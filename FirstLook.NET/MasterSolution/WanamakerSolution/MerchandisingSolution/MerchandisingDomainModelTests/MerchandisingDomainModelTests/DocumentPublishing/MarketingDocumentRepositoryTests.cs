﻿using System.Data.SqlClient;
using System.Linq;
using Autofac;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.PhotoServices;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.Repositories;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;
using FirstLook.Merchandising.ModelBuilder;
using FirstLook.Pricing.DomainModel;
using Merchandising.Messages;
using Moq;
using MvcMiniProfiler.Helpers.Dapper;
using NUnit.Framework;


namespace MerchandisingDomainModelTests.DocumentPublishing
{
	[TestFixture]
	public class MarketingDocumentRepositoryTests
	{
		private int _knownGoodInventory;
		private int _knownGoodBusinessUnit;
		private string _knownGoodXml;

		[SetUp]
		public void SetupMethod()
		{
			SetupAutofac();
			GetKnownGoodValues();
		}

        [Test]
        [Ignore] // Ignored because the build server does not have DB access
		public void GetXml_Returns_EmptyString_For_Bad_BusinessUnitId()
		{
            var searchAdapter = Registry.Resolve<IMarketListingSearchAdapter>();
            var mockLogger = new Mock<ILogger>();

			var repoUnderTest = new MarketingDocumentRepository(searchAdapter, mockLogger.Object);
			var output = repoUnderTest.GetMarketingXml(1, 1);
			Assert.AreEqual(string.Empty, output);
		}

        [Test]
        [Ignore] // Ignored because the build server does not have DB access
		public void GetXml_Returns_Matching_XML_For_Good_BusinessUnitId()
		{
            var searchAdapter = Registry.Resolve<IMarketListingSearchAdapter>();
            var mockLogger = new Mock<ILogger>();

			var repoUnderTest = new MarketingDocumentRepository(searchAdapter, mockLogger.Object);
			var output = repoUnderTest.GetMarketingXml(_knownGoodBusinessUnit, _knownGoodInventory);
			Assert.IsNotEmpty(output);
		}

		private static void SetupAutofac()
		{
			var builder = new ContainerBuilder();

			#region marketing XML Autofac registration
			/** The below are all required for MarketingXML generation **/
			//builder.RegisterModule(new CoreModule());
			//builder.RegisterType<DealerGeneralPreferenceDataAccess>().As<IDealerGeneralPreferenceDataAccess>();
			
			//// Photos
			//builder.RegisterType<PhotoLocatorDataAccess>().As<IPhotoLocatorDataAccess>();
			//builder.RegisterType<PhotoVehiclePreferenceDataAccess>().As<IPhotoVehiclePreferenceDataAccess>();
			//builder.RegisterModule(new PhotoServicesModule());
			////AdPreview
			//builder.RegisterType<AdPreviewVehiclePreferenceDataAccess>().As<IAdPreviewVehiclePreferenceDataAccess>();
			//builder.RegisterType<EquipmentDataAccess>().As<IEquipmentDataAccess>();
			//// Equipment
			//builder.RegisterType<EquipmentProviderAssignmentDataAccess>().As<IEquipmentProviderAssignmentDataAccess>();
			//builder.RegisterType<EquipmentDataAccess>().As<IEquipmentDataAccess>();
			//builder.RegisterType<EquipmentProviderDataAccess>().As<IEquipmentProviderDataAccess>();
			//// Vehicle summary
			//builder.RegisterType<VehicleSummaryDataAccess>().As<IVehicleSummaryDataAccess>();
			//// Consumer Highlights
			//builder.RegisterType<ConsumerHighlightsDealerPreferenceDataAccess>()
			//    .As<IConsumerHighlightsDealerPreferenceDataAccess>();
			//builder.RegisterType<ConsumerHighlightsVehiclePreferenceDataAccess>()
			//    .As<IConsumerHighlightsVehiclePreferenceDataAccess>();
			
			//// Certified
			//builder.RegisterType<CertifiedVehiclePreferenceDataAccess>().As<ICertifiedVehiclePreferenceDataAccess>();

			//// Market Analysis
			//builder.RegisterType<MarketAnalysisDealerPreferenceDataAccess>()
			//    .As<IMarketAnalysisDealerPreferenceDataAccess>();
			//builder.RegisterType<MarketAnalysisPricingDataAccess>().As<IMarketAnalysisPricingDataAccess>();
			//builder.RegisterType<MarketAnalysisVehiclePreferenceDataAccess>()
			//    .As<IMarketAnalysisVehiclePreferenceDataAccess>();

			//// MarketListing
			//builder.RegisterType<MarketListingVehiclePreferenceDataAccess>()
			//    .As<IMarketListingVehiclePreferenceDataAccess>();
			//builder.Register(c => new SearchAdapter(c.Resolve<IMarketListingVehiclePreferenceDataAccess>()))
			//    .As<ISearchAdapter>();

			//// Publishing
			//builder.RegisterType<ValueAnalyzerVehiclePreferenceDataAccess>()
			//    .As<IValueAnalyzerVehiclePreferenceDataAccess>();
			#endregion marketing XML

			builder.RegisterModule(new CoreModule());
			builder.RegisterModule(new OltpModule());
			builder.RegisterModule(new PhotoServicesModule());
			builder.RegisterModule(new MerchandisingMessagesModule());

			new PricingDomainRegister().Register(builder);
			new MerchandisingDomainRegistry().Register(builder);
            new MerchandisingModelBuilderRegistry().Register(builder);

			var mockLogger = new Mock<ILogger>().Object;
			builder.RegisterInstance(mockLogger).AsImplementedInterfaces();

			var container = builder.Build();
			Registry.RegisterContainer(container);
		}

		private void GetKnownGoodValues()
		{
			using (var conn = new SqlConnection("Data Source=intdb01;Initial Catalog=Merchandising;Type System Version=SQL Server 2008;Application Name=Merchandising;Integrated Security = True"))
			{
				conn.Open();

				const string query = @"
					SELECT TOP 1 BU.BusinessUnitID, I.InventoryID, DocumentXml
					FROM IMT.Marketing.InventoryDocuments D
					JOIN IMT..Inventory I ON I.InventoryID = D.VehicleEntityID
					JOIN Market.Pricing.Owner O ON O.OwnerID = D.OwnerID
					JOIN IMT.dbo.BusinessUnit BU ON BU.BusinessUnitID = O.OwnerEntityID AND BU.Active = 1
					WHERE BU.BusinessUnitId = 104452 -- AND I.InventoryId = 35376252
                ";


				var data = conn.Query<TestData>(query);
				var testDatas = data as TestData[] ?? data.ToArray();
				if (testDatas.Any())
				{
					_knownGoodBusinessUnit = testDatas.First().BusinessUnitId;
					_knownGoodInventory = testDatas.First().InventoryId;
					_knownGoodXml = testDatas.First().DocumentXml;
				}
			}
		}
	}


	class TestData
	{
		public int InventoryId { get; set; }
		public int BusinessUnitId { get; set; }
		public string DocumentXml { get; set; }
	}
}
