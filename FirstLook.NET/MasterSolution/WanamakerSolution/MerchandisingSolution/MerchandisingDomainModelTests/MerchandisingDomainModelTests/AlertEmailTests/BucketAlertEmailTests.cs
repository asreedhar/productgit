﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Core.Messaging;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.Dashboard.Entities;
using FirstLook.Merchandising.DomainModel.Release;
using FirstLook.Merchandising.DomainModel.Release.Tasks;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Workflow;
using MAX.Entities;
using Merchandising.Messages;
using Merchandising.Messages.BucketAlerts;
using Merchandising.Messages.QueueFactories;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.AlertEmailTests
{
    [TestFixture]
    public class BucketAlertEmailTests
    {
        // these tests are designed to make sure the text of the Alert Email does not accidently change
        // the exact email body test have been defined for each of the three email body situations:
        // Online Gaps only
        // Merchandising Alerts (low quality) only
        // Both Online Gaps and Merchandising Alerts

        #region Properties
        private const int BusinessUnitId = 1234;

        private IEmailQueueFactory _emailQueueFactory;

        private IEmail _emailProvider;

        private IDashboardRepository _dashboardRepository;
        private IEnumerable<Bucket> _newBuckets;
        private IEnumerable<Bucket> _usedBuckets;

        private INoSqlDbFactory _noSqlDbFactory;
        private INoSqlDb _noSqlDb;
        private IAdMessageSender _messageSender;

        private BusinessUnit _businessUnit;
        public BusinessUnit BusinessUnit {get { return _businessUnit; }}

        private GetBucketAlerts _getBucketAlerts;
        private List<String> _emailList;
        private List<String> _weeklyEmaiList;
        private List<BucketAlertThreshold> _thresholds;

        private BucketAlertTask _bucketAlertTask;
        #endregion

        #region Setup
        [SetUp]
        public void Setup()
        {
        }

        #region Email Queue Factory methods
        private void MockEmailQueueFactory()
        {
            var mock = new Mock<IEmailQueueFactory>() {CallBase = true};

            _emailQueueFactory = mock.Object;
        }
        #endregion

        #region Email Provider methods
        private void MockEmailProvider()
        {
            var mock = new Mock<IEmail>() { CallBase = true };

            mock.Setup(p => p.SendEmail(It.IsAny<MailMessage>()));
            mock.Setup(p => p.IsHardBounce(It.IsAny<Exception>())).Returns(true);

            _emailProvider = mock.Object;
        }

        private void MockEmailProviderForHardBounce()
        {
            var mock = new Mock<IEmail>() { CallBase = true };

            mock.Setup(p => p.SendEmail(It.IsAny<MailMessage>())).Throws(new Exception());
            mock.Setup(p => p.IsHardBounce(It.IsAny<Exception>())).Returns(true);

            _emailProvider = mock.Object;
        }

        private void MockEmailProviderForSendEmailException()
        {
            var mock = new Mock<IEmail>() { CallBase = true };

            mock.Setup(p => p.SendEmail(It.IsAny<MailMessage>())).Throws(new Exception());
            mock.Setup(p => p.IsHardBounce(It.IsAny<Exception>())).Returns(false);

            _emailProvider = mock.Object;
        }
        #endregion

        #region Dashboard Repository methods
        private void MockDashboardRepository()
        {
            HydrateBucketLists();

            var repositoryMock = new Mock<IDashboardRepository>() { CallBase = true };

            // override getting the buckets and their counts
            repositoryMock.Setup(c => c.GetBucketCounts(It.IsAny<int>(), It.Is<VehicleType>(v => v == VehicleType.New)))
                .Returns<int, VehicleType>((buId, type) => _newBuckets.ToList());
            repositoryMock.Setup(c => c.GetBucketCounts(It.IsAny<int>(), It.Is<VehicleType>(v => v == VehicleType.Used)))
                .Returns<int, VehicleType>((buId, type) => _usedBuckets.ToList());

            // override getting the bucket total
            repositoryMock.Setup(c => c.GetBucketTotal(It.IsAny<int>(), It.IsAny<VehicleType>(), It.IsAny<ISet<WorkflowType>>()))
                .Returns(50);

            _dashboardRepository = repositoryMock.Object;
        }

        private void HydrateBucketLists()
        {
            var workflowRepository = new WorkflowRepository();

            _newBuckets =
                workflowRepository.Workflows.Select(
                    wf => new Bucket() { BucketId = wf.Type, BucketDescription = wf.Label, BucketCount = 10 }).ToList();
            _usedBuckets =
                workflowRepository.Workflows.Select(
                    wf => new Bucket() { BucketId = wf.Type, BucketDescription = wf.Label, BucketCount = 15 }).ToList();
        }
        #endregion

        #region NoSqlDb Factory methods
        private void MockNoSqlDbFactory()
        {
            MockNoSqlDb();

            var mock = new Mock<INoSqlDbFactory>() { CallBase = true };

            mock.Setup(m => m.GetBucketValueDb()).Returns(() => _noSqlDb);

            _noSqlDbFactory = mock.Object;
        }

        private void MockNoSqlDb()
        {
            var mock = new Mock<INoSqlDb>();

            mock.Setup(m => m.Write(It.IsAny<Object>()));
            mock.Setup(m => m.Write(It.IsAny<IEnumerable<Object>>()));

            _noSqlDb = mock.Object;
        }
        #endregion

        #region MessageSender methods

        private void MockMessageSender()
        {
            var mock = new Mock<IAdMessageSender>();
            _messageSender = mock.Object;
        }
        #endregion

        #region BucketAlert methods
        private void MockGetBucketAlerts(int businessUnitId)
        {
            HydrateEmailLists();
            HydrateThresholds(businessUnitId);

            var mock = new Mock<GetBucketAlerts>(businessUnitId);

            mock.Setup(t => t.Active).Returns(true);
            mock.Setup(t => t.Emails).Returns(() => _emailList.ToArray());
            mock.Setup(t => t.WeeklyEmails).Returns(() => _weeklyEmaiList.ToArray());
            mock.Setup(t => t.NewUsed).Returns(0);
            mock.Setup(t => t.EmailBounced).Returns(false);
            mock.Setup(t => t.WeeklyEmailBounced).Returns(false);
            mock.Setup(t => t.Thresholds).Returns(() => _thresholds.ToArray());

            _getBucketAlerts = mock.Object;
        }

        private void MockGetBucketAlertsForEmptyEmailArrays(int businessUnitId)
        {
            HydrateThresholds(businessUnitId);

            var mock = new Mock<GetBucketAlerts>(businessUnitId);

            mock.Setup(t => t.Active).Returns(true);
            mock.Setup(t => t.Emails).Returns(() => new string[0]);
            mock.Setup(t => t.WeeklyEmails).Returns(() => new string[0]);
            mock.Setup(t => t.NewUsed).Returns(0);
            mock.Setup(t => t.EmailBounced).Returns(false);
            mock.Setup(t => t.WeeklyEmailBounced).Returns(false);
            mock.Setup(t => t.Thresholds).Returns(() => _thresholds.ToArray());

            _getBucketAlerts = mock.Object;
        }

        private void HydrateEmailLists()
        {
            _emailList = new List<string>() { String.Empty };
            _weeklyEmaiList = new List<string>() { String.Empty };
        }

        private void HydrateThresholds(int businessUnitId)
        {
            _thresholds = new List<BucketAlertThreshold>()
            {
                new BucketAlertThreshold() {Active = true, Bucket = WorkflowType.AllInventory, BusinessUnitId = businessUnitId, EmailActive = true, Id = 1, Limit = 5},
                new BucketAlertThreshold() {Active = true, Bucket = WorkflowType.Offline, BusinessUnitId = businessUnitId, EmailActive = true, Id = 2, Limit = 5},
                new BucketAlertThreshold() {Active = true, Bucket = WorkflowType.NeedsApprovalAll, BusinessUnitId = businessUnitId, EmailActive = true, Id = 3, Limit = 5},
                new BucketAlertThreshold() {Active = true, Bucket = WorkflowType.CreateInitialAd, BusinessUnitId = businessUnitId, EmailActive = true, Id = 4, Limit = 5},
                new BucketAlertThreshold() {Active = true, Bucket = WorkflowType.TrimNeeded, BusinessUnitId = businessUnitId, EmailActive = true, Id = 5, Limit = 5},
                new BucketAlertThreshold() {Active = true, Bucket = WorkflowType.OutdatedPrice, BusinessUnitId = businessUnitId, EmailActive = true, Id = 6, Limit = 5},
                new BucketAlertThreshold() {Active = true, Bucket = WorkflowType.NeedsAdReviewAll, BusinessUnitId = businessUnitId, EmailActive = true, Id = 7, Limit = 5},
                new BucketAlertThreshold() {Active = true, Bucket = WorkflowType.AdReviewNeeded, BusinessUnitId = businessUnitId, EmailActive = true, Id = 8, Limit = 5},
                new BucketAlertThreshold() {Active = true, Bucket = WorkflowType.NeedsPricingAll, BusinessUnitId = businessUnitId, EmailActive = true, Id = 9, Limit = 5},
                new BucketAlertThreshold() {Active = true, Bucket = WorkflowType.NoPrice, BusinessUnitId = businessUnitId, EmailActive = true, Id = 10, Limit = 5},
                new BucketAlertThreshold() {Active = true, Bucket = WorkflowType.DueForRepricing, BusinessUnitId = businessUnitId, EmailActive = true, Id = 11, Limit = 5},
                new BucketAlertThreshold() {Active = true, Bucket = WorkflowType.LowActivity, BusinessUnitId = businessUnitId, EmailActive = true, Id = 12, Limit = 5},
                new BucketAlertThreshold() {Active = true, Bucket = WorkflowType.NoCarfax, BusinessUnitId = businessUnitId, EmailActive = true, Id = 13, Limit = 5}
            };
        }
        #endregion

        #region BucketAlertTask methods
        private void MockBucketAlertTask()
        {
            MockEmailQueueFactory();
            MockEmailProvider();
            MockDashboardRepository();
            MockNoSqlDbFactory();
            MockMessageSender();

            var mock = new Mock<BucketAlertTask>(_emailQueueFactory, _emailProvider, _dashboardRepository, _noSqlDbFactory, _messageSender) {CallBase = true};

            HydrateBusinessUnit(BusinessUnitId);
            MockGetBucketAlerts(BusinessUnitId);

            mock.Setup(t => t.GetBusinessUnit(It.IsAny<int>())).Returns<int>(buId => _businessUnit);
            mock.Setup(t => t.RetrieveBucketAlerts(It.IsAny<int>())).Returns<int>(buId => _getBucketAlerts);
            mock.Setup(t => t.SetBounced(It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<bool>()));
            mock.Setup(t => t.GetCredentials(It.IsAny<int>())).Returns(false);

            _bucketAlertTask = mock.Object;
        }

        private void MockBucketAlertTaskForHardBounce()
        {
            MockEmailQueueFactory();
            MockEmailProviderForHardBounce();
            MockDashboardRepository();
            MockNoSqlDbFactory();

            var mock = new Mock<BucketAlertTask>(_emailQueueFactory, _emailProvider, _dashboardRepository,
                _noSqlDbFactory, _messageSender) { CallBase = true };

            HydrateBusinessUnit(BusinessUnitId);
            MockGetBucketAlerts(BusinessUnitId);

            mock.Setup(t => t.GetBusinessUnit(It.IsAny<int>())).Returns<int>(buId => _businessUnit);
            mock.Setup(t => t.RetrieveBucketAlerts(It.IsAny<int>())).Returns<int>(buId => _getBucketAlerts);
            mock.Setup(t => t.SetBounced(It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<bool>()));
            mock.Setup(t => t.GetCredentials(It.IsAny<int>())).Returns(false);

            _bucketAlertTask = mock.Object;
        }

        private void MockBucketAlertTaskForSendEmailException()
        {
            MockEmailQueueFactory();
            MockEmailProviderForSendEmailException();
            MockDashboardRepository();
            MockNoSqlDbFactory();

            var mock = new Mock<BucketAlertTask>(_emailQueueFactory, _emailProvider, _dashboardRepository,
                _noSqlDbFactory, _messageSender) { CallBase = true };

            HydrateBusinessUnit(BusinessUnitId);
            MockGetBucketAlerts(BusinessUnitId);

            mock.Setup(t => t.GetBusinessUnit(It.IsAny<int>())).Returns<int>(buId => _businessUnit);
            mock.Setup(t => t.RetrieveBucketAlerts(It.IsAny<int>())).Returns<int>(buId => _getBucketAlerts);
            mock.Setup(t => t.SetBounced(It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<bool>()));
            mock.Setup(t => t.GetCredentials(It.IsAny<int>())).Returns(false);

            _bucketAlertTask = mock.Object;
        }

        private void MockBucketalertTaskForEmptyEmailArrays()
        {
            MockEmailQueueFactory();
            MockEmailProviderForHardBounce();
            MockDashboardRepository();
            MockNoSqlDbFactory();

            var mock = new Mock<BucketAlertTask>(_emailQueueFactory, _emailProvider, _dashboardRepository,
                _noSqlDbFactory, _messageSender) { CallBase = true };

            HydrateBusinessUnit(BusinessUnitId);
            MockGetBucketAlertsForEmptyEmailArrays(BusinessUnitId);

            mock.Setup(t => t.GetBusinessUnit(It.IsAny<int>())).Returns<int>(buId => _businessUnit);
            mock.Setup(t => t.RetrieveBucketAlerts(It.IsAny<int>())).Returns<int>(buId => _getBucketAlerts);
            mock.Setup(t => t.SetBounced(It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<bool>()));
            mock.Setup(t => t.GetCredentials(It.IsAny<int>())).Returns(false);

            _bucketAlertTask = mock.Object;
        }

        private void HydrateBusinessUnit(int businessUnitId)
        {
            _businessUnit = new BusinessUnit
            {
                Id = businessUnitId,
                Name = "Test Dealer",
                ShortName = "Test",
                ZipCode = "78759",
                Active = true,
                BusinessUnitCode = "TD",
                BusinessUnitType = BusinessUnitType.Dealer
            };
        }
        #endregion

        private static BucketAlertEmailViewModel GetModel()
        {
            var model = new BucketAlertEmailViewModel("123456")
            {
                LowQualityLink = "http://google.com",
                OnlineGapLink = "http://yahoo.com",
                SettingsLink = "http://bing.com"
            };

            return model;
        }

        private static string Clean(string input)
        {
            // return a string without newlines and tabs
            return input.Replace("\r", string.Empty).Replace("\n", string.Empty).Replace("\t", string.Empty);
        }

        private static void CleanAndAssertAreEqual(string expected, string actual)
        {
            Debug.WriteLine(string.Format("Raw expected email body:{0}", expected));
            Debug.WriteLine(string.Format("Raw actual  email body:{0}", actual));
            Debug.WriteLine(string.Format("Clean expected email body:{0}", Clean(expected)));
            Debug.WriteLine(string.Format("Clean actual  email body:{0}", Clean(actual)));
            // ignore certain formatting characters during the equality assertion
            Assert.AreEqual(Clean(expected), Clean(actual));
        }
        #endregion

        [Test]
        public void BucketAlertEmail_With_OnlineGaps_and_Without_LowQuality()
        {
            //arrange
            var model = GetModel();
            model.Add(new BucketAlertEmailItem("gap1", 123, WorkflowType.NoPhotos));
            model.OnlineGapCount = 123;

            const string expectedEmailBody = "\r\n<div style=\"margin-bottom: 20px\">MAX analyzed your online inventory and found <span style=\"font-weight: bold\">123</span> vehicles with potential gaps online:\r\n<ul style=\"margin-top: 0; margin-bottom: 0\">\r\n\n<li>123 with gap1</li>\r\n\r\n</ul>\r\n</div>\r\nClick <a href=\"http://yahoo.com\">here</a> to go to these vehicles in MAX\r\n<br/>\r\n<br/>\r\n\r\n<div style=\"font-style:italic\">To modify your settings alerts, please click <a href=\"http://bing.com\">here</a>. Please call our Help Desk at 1-877-378-5665 with any questions or issues.</div>\r\n<br/>\r\n\r\n<div>Thank you</div>\r\n<br/>\r\n<div>The MAX Digital Operations Team</div>\r\n";

            //act
            var emailBody = new BucketAlertEmailTemplate(model).Run();

            //assert
            Assert.IsTrue(model.HasOnlineGaps);
            Assert.IsFalse(model.HasLowQuality);
            Assert.IsNotNullOrEmpty(emailBody);

            CleanAndAssertAreEqual(expectedEmailBody, emailBody);
        }

        [Test]
        public void BucketAlertEmail_Without_OnlineGaps_and_With_LowQuality()
        {
            //arrange
            var model = GetModel();

            model.Add(new BucketAlertEmailItem("low quality 1", 123, WorkflowType.LowActivity));
            model.LowQualityCount = 123;

            const string expectedEmailBody = "\r\n<div style=\"margin-bottom: 20px\">\r\n\t\tMAX analyzed your online inventory and found \r\n<span style=\"font-weight: bold\">123</span> vehicles with Merchandising Alerts:\r\n\r\n<ul style=\"margin-top: 0; margin-bottom: 0\">\r\n\n<li>123 with low quality 1</li>\r\n\r\n</ul>\r\n</div>\r\nClick <a href=\"http://yahoo.com\">here</a> to go to these vehicles in MAX\r\n<br/>\r\n<br/>\r\n\r\n<div style=\"font-style:italic\">To modify your settings alerts, please click <a href=\"http://bing.com\">here</a>. Please call our Help Desk at 1-877-378-5665 with any questions or issues.</div>\r\n<br/>\r\n\r\n<div>Thank you</div>\r\n<br/>\r\n<div>The MAX Digital Operations Team</div>\r\n";

            //act
            var emailBody = new BucketAlertEmailTemplate(model).Run();

            //assert
            Assert.IsFalse(model.HasOnlineGaps);
            Assert.IsTrue(model.HasLowQuality);
            Assert.IsNotNullOrEmpty(emailBody);

            CleanAndAssertAreEqual(expectedEmailBody, emailBody);
        }

        [Test]
        public void BucketAlertEmail_With_OnlineGaps_and_With_LowQuality()
        {
            //arrange
            var model = GetModel();

            model.Add(new BucketAlertEmailItem("gap1", 123, WorkflowType.NoPhotos));
            model.OnlineGapCount = 123;

            model.Add(new BucketAlertEmailItem("low quality 1", 99, WorkflowType.LowActivity));
            model.LowQualityCount = 99;

            const string expectedEmailBody = "\r\n<div style=\"margin-bottom: 20px\">MAX analyzed your online inventory and found <span style=\"font-weight: bold\">123</span> vehicles with potential gaps online:\r\n<ul style=\"margin-top: 0; margin-bottom: 0\">\r\n\n<li>123 with gap1</li>\r\n\r\n</ul>\r\n</div>\r\n<div style=\"margin-bottom: 20px\">\r\n\t\tThere are also  \r\n<span style=\"font-weight: bold\">99</span> vehicles with Merchandising Alerts:\r\n\r\n<ul style=\"margin-top: 0; margin-bottom: 0\">\r\n\n<li>99 with low quality 1</li>\r\n\r\n</ul>\r\n</div>\r\nClick <a href=\"http://yahoo.com\">here</a> to go to these vehicles in MAX\r\n<br/>\r\n<br/>\r\n\r\n<div style=\"font-style:italic\">To modify your settings alerts, please click <a href=\"http://bing.com\">here</a>. Please call our Help Desk at 1-877-378-5665 with any questions or issues.</div>\r\n<br/>\r\n\r\n<div>Thank you</div>\r\n<br/>\r\n<div>The MAX Digital Operations Team</div>\r\n";
            //act
            var emailBody = new BucketAlertEmailTemplate(model).Run();

            //assert
            Assert.IsTrue(model.HasOnlineGaps);
            Assert.IsTrue(model.HasLowQuality);
            Assert.IsNotNullOrEmpty(emailBody);

            CleanAndAssertAreEqual(expectedEmailBody, emailBody);
        }

        [TestCase("test@test.com", true)]
        [TestCase( "testtest.com", false )]
        [TestCase( "test@testcom", false )]
        [TestCase( "", true )]
        [TestCase( "test@test.com,test1@test.com,test1@test.com", true )]
        [TestCase( "test@test.com,test1test.com,test1@test.com", false )]
        [TestCase( "test@test.com;test1@test.com;test1@test.com", true )]
        [TestCase( "test@test.com, test1@test.com, test1@test.com", true )]
        [TestCase( "test@test.com , test1@test.com  ,  test1@test.com  ", true )]
        [TestCase( "test@test.com, test1@test.com,   test1@test.com  ", true )]
        [TestCase( "test   @test.com, test1@test.com,   test1@test.com  ", false )]
        [TestCase( "test@test.com, test1@test.com,,,   test1@test.com  ", true )]
        public void EmailValidatorRegularExpressionTest(string inputEmails, bool isValidExpected)
        {
            const string regExpression = @"^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*([\s,;])*)*$";
            var isValidResult = Regex.IsMatch(inputEmails, regExpression);
            Assert.AreEqual( isValidResult, isValidExpected );
        }

        [TestCase(null, false, false)]
        [TestCase("", false, false)]
        [TestCase("testtest.com", false, false)]
        [TestCase("test@test.com", false, false)]
        [TestCase(null, true, false)]
        [TestCase("", true, false)]
        [TestCase("testtest.com", true, false)]
        [TestCase("test@test.com", true, false)]
        [TestCase(null, false, true)]
        [TestCase("", false, true)]
        [TestCase("testtest.com", false, true)]
        [TestCase("test@test.com", false, true)]
        [TestCase(null, true, true)]
        [TestCase("", true, true)]
        [TestCase("testtest.com", true, true)]
        [TestCase("test@test.com", true, true)]
        public void TestEmailProcess(String inputEmail, Boolean sendDaily, Boolean sendWeekly)
        {
            MockBucketAlertTask();

            // test to verify that no exception is thrown
            _emailList[0] = inputEmail;
            _weeklyEmaiList[0] = inputEmail;

            var bucketAlertMessage = new BucketAlertMessage(BusinessUnitId)
            {
                SendDaily = sendDaily,
                SendWeekly = sendWeekly
            };

            _bucketAlertTask.Process(bucketAlertMessage);
            // We are expecting that there should be no email exceptions because we should have caught them.
            // Is there a way to check the logs for the error?
        }

        [Test]
        public void TestEmailProcessForEmptyEmailArrays()
        {
            MockBucketalertTaskForEmptyEmailArrays();

            var bucketAlertMessage = new BucketAlertMessage(BusinessUnitId) { SendDaily = true };

            _bucketAlertTask.Process(bucketAlertMessage);
            // We are expecting that there should be no email exceptions because we should have caught them.
            // Is there a way to check the logs for the error?
        }

        [Test]
        public void TestEmailProcessForHardBounce()
        {
            MockBucketAlertTaskForHardBounce();

            // test to verify that no exception is thrown
            _emailList[0] = "test@test.com";

            var bucketAlertMessage = new BucketAlertMessage(BusinessUnitId) {SendDaily = true};

            _bucketAlertTask.Process(bucketAlertMessage);
            // We are expecting that there should be no email exceptions because we should have caught them.
            // Is there a way to check the logs for the error?
        }

        [Test]
        [ExpectedException(typeof(Exception))]
        public void TestEmailProcessForSendEmailException()
        {
            MockBucketAlertTaskForSendEmailException();

            _emailList[0] = "test@test.com";

            var bucketAlertMessage = new BucketAlertMessage(BusinessUnitId) { SendDaily = true };

            _bucketAlertTask.Process(bucketAlertMessage);
        }
    }
}