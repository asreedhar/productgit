﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Helpers;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Vehicles
{
    [TestFixture]
    class InventoryDataSorterTests
    {

        private IInventoryDataSorter _inventorySorter;

        [TestFixtureSetUp]
        public void SetupFixture()
        {
            _inventorySorter = new InventoryDataSorter();
        }

        [Test]
        public void TestSortCertified()
        {
            var inventoryData = new List<IInventoryData>();
            inventoryData.Add(new InventoryData()
            {
                Certified = false
            });
            inventoryData.Add(new InventoryData()
            {
                Certified = true
            });
            inventoryData.Add(new InventoryData()
            {
                Certified = false
            });

            var sorted = _inventorySorter.SortInventory(inventoryData, SortType.Certified, SortDirection.Ascending);
            Assert.True(sorted.Last().Certified);

            sorted = _inventorySorter.SortInventory(inventoryData, SortType.Certified, SortDirection.Descending);
            Assert.True(sorted.First().Certified);
        }
        
        [Test]
        public void TestSortListPrice()
        {
            var inventoryData = new List<IInventoryData>();
            inventoryData.Add(new InventoryData()
            {
                ListPrice = 3
            });
            inventoryData.Add(new InventoryData()
            {
                ListPrice = 1
            });
            inventoryData.Add(new InventoryData()
            {
                ListPrice = 2
            });

            var sorted = _inventorySorter.SortInventory(inventoryData, SortType.ListPrice, SortDirection.Ascending);
            Assert.True(sorted[0].ListPrice == 1);
            Assert.True(sorted[1].ListPrice == 2);
            Assert.True(sorted[2].ListPrice == 3);

            sorted = _inventorySorter.SortInventory(inventoryData, SortType.ListPrice, SortDirection.Descending);
            Assert.True(sorted[0].ListPrice == 3);
            Assert.True(sorted[1].ListPrice == 2);
            Assert.True(sorted[2].ListPrice == 1);
        }

        private const string Chrysler = "Chrysler";
        private const string Chevrolet = "Chevrolet";
// ReSharper disable once InconsistentNaming
        private const string BMW = "BMW";

        [Test]
        public void TestSortMake()
        {
            var inventoryData = new List<IInventoryData>();
            inventoryData.Add(new InventoryData()
            {
                Make = Chrysler
            });
            inventoryData.Add(new InventoryData()
            {
                Make = Chevrolet
            });
            inventoryData.Add(new InventoryData()
            {
                Make = BMW
            });

            var sorted = _inventorySorter.SortInventory(inventoryData, SortType.Make, SortDirection.Ascending);
            Assert.True(sorted[0].Make.Equals(BMW));
            Assert.True(sorted[1].Make.Equals(Chevrolet));
            Assert.True(sorted[2].Make.Equals(Chrysler));

            sorted = _inventorySorter.SortInventory(inventoryData, SortType.Make, SortDirection.Descending);
            Assert.True(sorted[0].Make.Equals(Chrysler));
            Assert.True(sorted[1].Make.Equals(Chevrolet));
            Assert.True(sorted[2].Make.Equals(BMW));
        }

        private const string Aventador = "Aventador";
        private const string Gallardo = "Gallardo";
        private const string Murcielago = "Murcielago";

        [Test]
        public void TestSortModel()
        {
            var inventoryData = new List<IInventoryData>();
            inventoryData.Add(new InventoryData()
            {
                Model = Murcielago
            });
            inventoryData.Add(new InventoryData()
            {
                Model = Aventador
            });
            inventoryData.Add(new InventoryData()
            {
                Model = Gallardo
            });

            var sorted = _inventorySorter.SortInventory(inventoryData, SortType.Model, SortDirection.Ascending);
            Assert.True(sorted[0].Model.Equals(Aventador));
            Assert.True(sorted[1].Model.Equals(Gallardo));
            Assert.True(sorted[2].Model.Equals(Murcielago));

            sorted = _inventorySorter.SortInventory(inventoryData, SortType.Model, SortDirection.Descending);
            Assert.True(sorted[0].Model.Equals(Murcielago));
            Assert.True(sorted[1].Model.Equals(Gallardo));
            Assert.True(sorted[2].Model.Equals(Aventador));
        }

        [Test]
        public void TestSortMileageReceived()
        {
            var inventoryData = new List<IInventoryData>();
            inventoryData.Add(new InventoryData()
            {
                MileageReceived = 3
            });
            inventoryData.Add(new InventoryData()
            {
                MileageReceived = 1
            });
            inventoryData.Add(new InventoryData()
            {
                MileageReceived = 2
            });

            var sorted = _inventorySorter.SortInventory(inventoryData, SortType.MileageReceived, SortDirection.Ascending);
            Assert.True(sorted[0].MileageReceived == 1);
            Assert.True(sorted[1].MileageReceived == 2);
            Assert.True(sorted[2].MileageReceived == 3);

            sorted = _inventorySorter.SortInventory(inventoryData, SortType.MileageReceived, SortDirection.Descending);
            Assert.True(sorted[0].MileageReceived == 3);
            Assert.True(sorted[1].MileageReceived == 2);
            Assert.True(sorted[2].MileageReceived == 1);
        }

        [Test]
        public void TestSortUnitCost()
        {
            var inventoryData = new List<IInventoryData>();
            inventoryData.Add(new InventoryData()
            {
                UnitCost = 3
            });
            inventoryData.Add(new InventoryData()
            {
                UnitCost = 1
            });
            inventoryData.Add(new InventoryData()
            {
                UnitCost = 2
            });

            var sorted = _inventorySorter.SortInventory(inventoryData, SortType.UnitCost, SortDirection.Ascending);
            Assert.True(sorted[0].UnitCost == 1);
            Assert.True(sorted[1].UnitCost == 2);
            Assert.True(sorted[2].UnitCost == 3);

            sorted = _inventorySorter.SortInventory(inventoryData, SortType.UnitCost, SortDirection.Descending);
            Assert.True(sorted[0].UnitCost == 3);
            Assert.True(sorted[1].UnitCost == 2);
            Assert.True(sorted[2].UnitCost == 1);
        }

        [Test]
        public void TestSortStockNumber()
        {
            var inventoryData = new List<IInventoryData>();
            inventoryData.Add(new InventoryData()
            {
                StockNumber = "15-7083"
            });
            inventoryData.Add(new InventoryData()
            {
                StockNumber = "14-1782"
            });
            inventoryData.Add(new InventoryData()
            {
                StockNumber = "15-7088"
            });

            var sorted = _inventorySorter.SortInventory(inventoryData, SortType.StockNumber, SortDirection.Ascending);
            Assert.True(sorted[0].StockNumber.Equals("14-1782"));
            Assert.True(sorted[1].StockNumber.Equals("15-7083"));
            Assert.True(sorted[2].StockNumber.Equals("15-7088"));

            sorted = _inventorySorter.SortInventory(inventoryData, SortType.StockNumber, SortDirection.Descending);
            Assert.True(sorted[0].StockNumber.Equals("15-7088"));
            Assert.True(sorted[1].StockNumber.Equals("15-7083"));
            Assert.True(sorted[2].StockNumber.Equals("14-1782"));
        }
    }
}
