﻿using System;
using System.Collections.Generic;
using System.Text;
using FirstLook.Merchandising.DomainModel.DataSource;
using FirstLook.Merchandising.DomainModel.Vehicles;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Vehicles
{
    [TestFixture]
    public class displayEquipmentCollection_chromeFilterTest
    {

        [Test]
        public void Test()
        {

            var mockDisplayEquipmentCollection = new Mock<DisplayEquipmentCollection>() { CallBase = true }; 

            // create a generic choice collection for return from chrome equipment call
            GenericChoiceCollection myChromeEquipment = new GenericChoiceCollection();

            GenericChoice myChoice01 = new GenericChoice();
            myChoice01.ID = 1000;
            myChromeEquipment.Add(myChoice01);

            GenericChoice myChoice02 = new GenericChoice();
            myChoice02.ID = 1001;
            myChromeEquipment.Add(myChoice02);

            GenericChoice myChoice03 = new GenericChoice();
            myChoice03.ID = 1002;
            myChromeEquipment.Add(myChoice03);

            GenericChoice myChoice04 = new GenericChoice();
            myChoice04.ID = 1003;
            myChromeEquipment.Add(myChoice04);


            // return the above chrome equipt
            mockDisplayEquipmentCollection.Setup(x => x.getChromeEquipment(It.IsAny<int>())).Returns(myChromeEquipment);

            // create a generic equipment collection for the call from the lot equipment
            GenericEquipmentCollection myLotEquipment = new GenericEquipmentCollection();
            myLotEquipment.Add(new GenericEquipment(1000));
            myLotEquipment.Add(new GenericEquipment(1001));
            myLotEquipment.Add(new GenericEquipment(1002));
            myLotEquipment.Add(new GenericEquipment(1003));
            myLotEquipment.Add(new GenericEquipment(1004));

            // call filter
            GenericEquipmentCollection myFilteredEquipment = mockDisplayEquipmentCollection.Object.filterEquipment(myLotEquipment, 1);
            
            // may sure filtered equipment is less than of equal to lot equipment
            Assert.LessOrEqual(myFilteredEquipment.Count, myLotEquipment.Count);

            // make sure 1005 is not in the filtered collection because it is not in the chrome style equiptment mock below
            Assert.False(myFilteredEquipment.Contains(1004));

            // make sure 1001 is in the filtered collection because it is in the chrome style equiptment mock below
            Assert.True(myFilteredEquipment.Contains(1001));
        }
    }
}
