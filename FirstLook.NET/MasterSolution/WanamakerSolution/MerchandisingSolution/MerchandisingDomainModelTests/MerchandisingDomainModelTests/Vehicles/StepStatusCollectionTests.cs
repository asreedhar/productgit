﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Postings;
using FirstLook.Merchandising.DomainModel.Vehicles;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Vehicles
{
    [TestFixture]
    public class StepStatusCollectionTests
    {
        private class Repo : IStepStatusCollectionRepository
        {
            private readonly List<IStepStatus> StatusToFetch = new List<IStepStatus>();
            public int? BusinessUnitIdFetched;
            public int? InventoryIdFetched;
            public List<IStepStatus> SavedStatuses;
            public string SavedMemberLogin;

            public void AddStepStatusToFetch(
                StepStatusTypes typeId, int? level = null, int? defaultLevel = null, int businessUnitId = 0, int inventoryId = 0, byte[] version = null)
            {
                StatusToFetch.Add(new StepStatus(
                                      businessUnitId, inventoryId, typeId, level, defaultLevel, version ?? new byte[8]));
            }

            public IEnumerable<IStepStatus> Fetch(int businessUnitId, int inventoryId)
            {
                BusinessUnitIdFetched = businessUnitId;
                InventoryIdFetched = inventoryId;
                return StatusToFetch;
            }

            public void Save(IEnumerable<IStepStatus> stepsToSave, string memberLogin)
            {
                SavedStatuses = stepsToSave.ToList();
                SavedMemberLogin = memberLogin;
            }
        }


        [Test]
        public void Fetch_should_return_objects_from_repository()
        {
            var repo = new Repo();
            repo.AddStepStatusToFetch(StepStatusTypes.Photos, (int) ActivityStatusCodes.Complete);

            var statuses = StepStatusCollection.Fetch(repo, 12, 34);

            Assert.That(statuses.GetStatus(StepStatusTypes.Photos), Is.EqualTo(ActivityStatusCodes.Complete));
        }

        [Test]
        public void Fetch_should_pass_businessUnitId_to_repository_Fetch()
        {
            var repo = new Repo();
            repo.AddStepStatusToFetch(StepStatusTypes.Photos, (int)ActivityStatusCodes.Complete);

            StepStatusCollection.Fetch(repo, 12, 34);

            Assert.That(repo.BusinessUnitIdFetched, Is.EqualTo(12));
        }

        [Test]
        public void Fetch_should_pass_inventoryId_to_repository_Fetch()
        {
            var repo = new Repo();
            repo.AddStepStatusToFetch(StepStatusTypes.Photos, (int)ActivityStatusCodes.Complete);

            StepStatusCollection.Fetch(repo, 12, 34);

            Assert.That(repo.InventoryIdFetched, Is.EqualTo(34));
        }

        [TestCase(StepStatusTypes.Photos, ActivityStatusCodes.Complete)]
        [TestCase(StepStatusTypes.Photos, ActivityStatusCodes.Incomplete)]
        public void GetStatus_should_return_value_set_with_SetStatus(StepStatusTypes typeId, ActivityStatusCodes valueToSet)
        {
            var repo = new Repo();
            repo.AddStepStatusToFetch(StepStatusTypes.Photos);

            var statuses = StepStatusCollection.Fetch(repo, 12, 34);

            statuses.SetStatus(typeId, valueToSet);

            var status = statuses.GetStatus(typeId);

            Assert.That(status, Is.EqualTo(valueToSet));
        }

        [TestCase(ActivityStatusCodes.Complete)]
        [TestCase(ActivityStatusCodes.Incomplete)]
        [TestCase(ActivityStatusCodes.Untouched)]
        public void GetStatus_should_always_return_Untouched_for_type_id_not_returned_by_repository(ActivityStatusCodes statusToSet)
        {
            var repo = new Repo();
            repo.AddStepStatusToFetch(StepStatusTypes.Photos);
            var statuses = StepStatusCollection.Fetch(repo, 12, 34);

            statuses.SetStatus(StepStatusTypes.AdReview, statusToSet);

            var statusForItemNotReturnedFromRepository =
                statuses.GetStatus(StepStatusTypes.AdReview);

            Assert.That(statusForItemNotReturnedFromRepository, Is.EqualTo(ActivityStatusCodes.Untouched));
        }

        [TestCase(AdvertisementPostingStatus.NeedsAction)]
        [TestCase(AdvertisementPostingStatus.CurrentlyReleasing)]
        [TestCase(AdvertisementPostingStatus.PendingApproval)]
        public void SetPostingStatus_should_change_posting_status(AdvertisementPostingStatus postingStatus)
        {
            var repo = new Repo();
            repo.AddStepStatusToFetch(StepStatusTypes.Posting);
            var statuses = StepStatusCollection.Fetch(repo, 1, 2);

            statuses.SetPostingStatus(postingStatus);

            // Need to Save() to observe the value of the posting status
            statuses.Save(repo, "FOO");
            Assert.That(repo.SavedStatuses.Single(ss => ss.StatusTypeId == StepStatusTypes.Posting).StatusLevel,
                        Is.EqualTo((int) postingStatus));
        }

        [Test]
        public void Save_should_contain_changed_dirty_non_posting_steps()
        {
            var repo = new Repo();
            repo.AddStepStatusToFetch(StepStatusTypes.Posting);
            repo.AddStepStatusToFetch(StepStatusTypes.Photos);
            repo.AddStepStatusToFetch(StepStatusTypes.Pricing);
            var statuses = StepStatusCollection.Fetch(repo, 1, 2);

            statuses.SetStatus(StepStatusTypes.Photos, ActivityStatusCodes.Complete);

            statuses.Save(repo, "FOO");
            Assert.That(repo.SavedStatuses.Any(
                ss => ss.StatusTypeId == StepStatusTypes.Photos &&
                      ss.IsDirty), Is.True);
        }

        [Test]
        public void Save_should_not_contain_nonchanged_dirty_non_posting_steps()
        {
            var repo = new Repo();
            repo.AddStepStatusToFetch(StepStatusTypes.Posting);
            repo.AddStepStatusToFetch(StepStatusTypes.Photos);
            repo.AddStepStatusToFetch(StepStatusTypes.Pricing);
            var statuses = StepStatusCollection.Fetch(repo, 1, 2);

            statuses.SetStatus(StepStatusTypes.Photos, ActivityStatusCodes.Complete);

            statuses.Save(repo, "FOO");
            Assert.That(repo.SavedStatuses.Any(
                ss => ss.StatusTypeId == StepStatusTypes.Pricing &&
                      ss.IsDirty), Is.False);
        }

        [Test]
        public void Save_should_pass_username_to_repository()
        {
            var repo = new Repo();
            repo.AddStepStatusToFetch(StepStatusTypes.Posting);
            repo.AddStepStatusToFetch(StepStatusTypes.Photos);
            repo.AddStepStatusToFetch(StepStatusTypes.Pricing);
            var statuses = StepStatusCollection.Fetch(repo, 1, 2);

            statuses.Save(repo, "FOO");

            Assert.That(repo.SavedMemberLogin, Is.EqualTo("FOO"));
        }

        [TestCase(ActivityStatusCodes.Incomplete, ActivityStatusCodes.Complete)]
        [TestCase(ActivityStatusCodes.Complete, ActivityStatusCodes.Incomplete)]
        public void Save_should_NOT_update_posting_status_if_only_AdReview_changed(ActivityStatusCodes startingAdReviewStatus, ActivityStatusCodes statusToChangeTo)
        {
            var repo = new Repo();
            repo.AddStepStatusToFetch(StepStatusTypes.Posting);
            repo.AddStepStatusToFetch(StepStatusTypes.AdReview, (int)startingAdReviewStatus);
            var statuses = StepStatusCollection.Fetch(repo, 1, 2);

            statuses.SetStatus(StepStatusTypes.AdReview, statusToChangeTo);

            statuses.Save(repo, "FOO");

            Assert.That(repo.SavedStatuses.Any(
                ss =>
                    ss.IsDirty &&
                    ss.StatusTypeId == StepStatusTypes.Posting),
                        Is.False);
        }

        [TestCase(StepStatusTypes.Photos, ActivityStatusCodes.Complete)]
        [TestCase(StepStatusTypes.Pricing, ActivityStatusCodes.Incomplete)]
        [TestCase(StepStatusTypes.AdReview, ActivityStatusCodes.Incomplete)]
        public void Save_should_NOT_update_posting_status_when_any_non_posting_status_is_changed(
            StepStatusTypes nonPostingStatusToChange, ActivityStatusCodes newValue)
        {
            var repo = new Repo();
            repo.AddStepStatusToFetch(StepStatusTypes.Posting, (int)AdvertisementPostingStatus.Error);
            repo.AddStepStatusToFetch(StepStatusTypes.Photos, (int)ActivityStatusCodes.Incomplete);
            repo.AddStepStatusToFetch(StepStatusTypes.Pricing, (int)ActivityStatusCodes.Complete);
            repo.AddStepStatusToFetch(StepStatusTypes.AdReview, (int)ActivityStatusCodes.Incomplete);
            var statuses = StepStatusCollection.Fetch(repo, 1, 2);

            statuses.SetStatus(nonPostingStatusToChange, newValue);

            statuses.Save(repo, "FOO");

            Assert.That(repo.SavedStatuses.Any(ss => ss.IsDirty && ss.StatusTypeId == StepStatusTypes.Posting),
                        Is.False);
        }
    }
}