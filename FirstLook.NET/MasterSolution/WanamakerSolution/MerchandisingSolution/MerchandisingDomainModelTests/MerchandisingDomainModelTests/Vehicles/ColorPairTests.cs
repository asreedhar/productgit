﻿using FirstLook.Merchandising.DomainModel.Vehicles;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Vehicles
{
    [TestFixture]
    public class ColorPairTests
    {
        [Test]
        public void When_getting_the_codes_and_descriptions_string_it_should_return_the_information_for_both_colors_if_they_are_present()
        {
            var first = new ChromeColor("Metallic Silver", "silver", "47C", "silver");
            var second = new ChromeColor("Black Top", "black", "11", "black");
            var pair = new ColorPair(first, second);

            Assert.AreEqual("47C:Metallic Silver|11:Black Top", pair.CodesAndDescriptions);
        }

        [Test]
        public void When_getting_the_codes_and_descriptions_string_it_should_return_the_information_for_the_first_if_it_is_the_only_one_with_information()
        {
            var first = new ChromeColor("Metallic Silver", "silver", "47C", "silver");
            var second = new ChromeColor(string.Empty, string.Empty, string.Empty, string.Empty);
            var pair = new ColorPair(first, second);

            Assert.AreEqual("47C:Metallic Silver", pair.CodesAndDescriptions);
        }


        [Test]
        public void When_parsing_an_empty_string_it_should_return_an_empty_color_pair()
        {
            var result = ColorPair.Parse("");

            Assert.AreEqual(string.Empty, result.First.Description);
            Assert.AreEqual(string.Empty, result.First.ColorCode);
            Assert.AreEqual(string.Empty, result.Second.Description);
            Assert.AreEqual(string.Empty, result.Second.ColorCode);
        }

        [Test]
        public void When_parsing_a_single_code_and_description_pair_it_should_set_the_code_and_description_on_the_first_color()
        {
            var result = ColorPair.Parse("47C:Metallic Silver");

            Assert.AreEqual("Metallic Silver", result.First.Description);
            Assert.AreEqual("47C", result.First.ColorCode);
            Assert.AreEqual(string.Empty, result.Second.Description);
            Assert.AreEqual(string.Empty, result.Second.ColorCode);
        }

        [Test]
        public void When_parsing_multiple_color_and_description_codes_it_should_set_both_the_first_and_second_colors_in_the_pair()
        {
            var result = ColorPair.Parse("47C:Metallic Silver|11:Black Top");

            Assert.AreEqual("Metallic Silver", result.First.Description);
            Assert.AreEqual("47C", result.First.ColorCode);
            Assert.AreEqual("Black Top", result.Second.Description);
            Assert.AreEqual("11", result.Second.ColorCode);
            
        }
    }
}