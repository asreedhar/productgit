﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Merchandising.DomainModel.DataAccess;
using FirstLook.Merchandising.DomainModel.Vehicles;
using Moq;
using NUnit.Framework;
using VehicleDataAccess;

namespace MerchandisingDomainModelTests.Vehicles
{
    public abstract class LotLoadingTests
    {
        protected VehicleConfiguration Configuration;
        protected bool Loaded;

        protected const int StyleId = 324442;
        protected const int BusinessUnitId = 10000;
        protected const int InventoryId = 12345;
        protected const string Vin = "ASDFLKJ";

        protected Mock<IChromeOptionRepository> MockChromeOptionRepository;
        protected Mock<ILotRepository> MockLotRepository;

        protected Equipment AutomaticTransmission = new Equipment { OptionCode = "2TC", OptionKindID = 7, Description = "7-SPEED DOUBLE CLUTCH AUTOMATIC TRANSMISSION" };
        protected Equipment AlarmSystem = new Equipment { OptionCode = "302", OptionKindID = 0, Description = "ANTI-THEFT ALARM SYSTEM (Priority 1)" };
        protected Equipment KeylessEntry = new Equipment { OptionCode = "322", OptionKindID = 0, Description = "COMFORT ACCESS KEYLESS ENTRY (Priority 1)" };
        protected Equipment IpodAdapter = new Equipment { OptionCode = "6FL", OptionKindID = 0, Description = "IPOD & USB ADAPTER", ExtDescription = "USB interface"};
        protected Equipment GrayMetallic = new Equipment { OptionCode = "A52", OptionKindID = 68, Description = "SPACE GRAY METALLIC" };
        protected Equipment CrimsonRed = new Equipment { OptionCode = "A61", OptionKindID = 68, Description = "CRIMSON RED" };
        protected Equipment WhiteMetallic = new Equipment { OptionCode = "A96", OptionKindID = 68, Description = "MINERAL WHITE METALLIC" };
        protected Equipment BeigeSeatTrim = new Equipment { OptionCode = "KAG", OptionKindID = 27, Description = "CREAM BEIGE, LEATHERETTE SEAT TRIM" };
        protected Equipment BlackSeatTrim = new Equipment { OptionCode = "KAS", OptionKindID = 27, Description = "BLACK, LEATHERETTE SEAT TRIM" };
        protected Equipment ColdWeather = new Equipment { OptionCode = "ZCW", OptionKindID = 0, Description = "COLD WEATHER PKG" };
        protected Equipment BlackTop = new Equipment { OptionCode = "T42", OptionKindID = 69, Description = "BLACK" };
        protected Equipment WhiteTop = new Equipment { OptionCode = "T43", OptionKindID = 69, Description = "WHITE" };
        protected EquipmentCollection AllEquipment;

        protected AvailableChromeColors AvailableChromeColors = new AvailableChromeColors
        {
            ExteriorColors = new List<ColorPair> {
                 new ColorPair(new ChromeColor("Crimson Red", "", "A61", ""), new ChromeColor( "White", "", "T43", "")),
                 new ColorPair(new ChromeColor("Space Gray Metallic","", "A52", ""),new ChromeColor("Black", "","T42", ""))
             },

            InteriorColors = new List<ChromeColor> {
                 new ChromeColor("Cream Beige", "", "KAG", ""),
                 new ChromeColor("Black Leather", "", "KAS", "")
             }

        };

        protected List<ChromeStyle> AvailableStyles = new List<ChromeStyle> {
            new ChromeStyle(324440, "", "", ""),
            new ChromeStyle(324441, "", "", ""), 
            new ChromeStyle(324442, "", "", ""), 
            new ChromeStyle(324443, "", "", "") 
        };


        protected LotLoader LotLoader;
        protected LotVehicle VehicleInfo;

        [SetUp]
        public virtual void SetUp()
        {
            AllEquipment = new EquipmentCollection { 
                AutomaticTransmission, AlarmSystem, KeylessEntry, IpodAdapter, GrayMetallic, CrimsonRed, WhiteMetallic, BeigeSeatTrim, BlackSeatTrim, ColdWeather, BlackTop, WhiteTop
            };

            MockChromeOptionRepository = new Mock<IChromeOptionRepository>();
            MockChromeOptionRepository.Setup(x => x.FetchAllOptional(StyleId)).Returns(AllEquipment);
            MockChromeOptionRepository.Setup(x => x.FetchColors(StyleId)).Returns(AvailableChromeColors);

            Arrange();

            Act();
        }

        protected virtual void Arrange() { }

        protected virtual void Act() { }

        protected string GetPipeDelimitedOptionCodeString(params Equipment[] options)
        {
            var builder = new StringBuilder();

            foreach (var option in options)
            {
                builder.AppendFormat("{0}|", option.OptionCode);
            }

            return builder.ToString().TrimEnd('|');
        }

        protected VehicleConfiguration CreateConfiguration(
            int styleId = StyleId,
            string exteriorColorCode = "",
            string exteriorColorDescription = "",
            string secondaryExteriorColorCode = "",
            string secondaryExteriorColorDescription = "",
            string interiorColorCode = "",
            string interiorColorDescription = "",
            Equipment[] loadedEquipment = null)
        {
            if (loadedEquipment == null) loadedEquipment = new Equipment[] { };

            var detailedEquipment = new DetailedEquipmentCollection();
            foreach (var equipment in loadedEquipment.Select(e => new DetailedEquipment(e)))
            {
                detailedEquipment.Add(equipment);
            }

            var transfer = new VehicleDataTransfer
            {
                InventoryId = InventoryId,
                ChromeStyleID = styleId,
                ExteriorColor1 = exteriorColorDescription,
                ExteriorColorCode = exteriorColorCode,
                ExteriorColor2 = secondaryExteriorColorDescription,
                ExteriorColorCode2 = secondaryExteriorColorCode,
                InteriorColor = interiorColorDescription,
                InteriorColorCode = interiorColorCode,
                Options = detailedEquipment,
                Generics = new GenericEquipmentCollection(),
                Replacements = new GenericEquipmentReplacementCollection()
            };

            return new VehicleConfiguration(BusinessUnitId, transfer);
        }
    }

    [TestFixture]
    public class When_a_configuration_does_not_have_a_color_and_the_color_code_is_not_in_the_lot_data : LotLoadingTests
    {
        protected override void Arrange()
        {
            Configuration = CreateConfiguration();

            var lotInfo = new LotInfo
            {
                StyleId = StyleId,
                Vin = Vin,
                Codes = GetPipeDelimitedOptionCodeString(GrayMetallic),
                Color = "SPACE GRAY METALLIC"
            };

            MockLotRepository = new Mock<ILotRepository>();
            MockLotRepository.Setup(x => x.GetInfo(BusinessUnitId, InventoryId)).Returns(lotInfo);

            LotLoader = new LotLoader(MockChromeOptionRepository.Object, MockLotRepository.Object);
        }
        
        protected override void Act()
        {
            Loaded = LotLoader.Load(BusinessUnitId, Configuration);
        }

        [Test]
        public void It_should_set_the_color_code_based_on_the_description()
        {
            Assert.AreEqual(GrayMetallic.OptionCode, Configuration.ExteriorColorCode);
        }

        [Test]
        public void It_should_set_the_color_based_on_the_description()
        {
            Assert.AreEqual("Space Gray Metallic", Configuration.ExteriorColor1);
        }

    }

    [TestFixture]
    public class When_a_configuration_does_not_have_a_primary_exterior_color : LotLoadingTests
    {
        protected override void Arrange()
        {
            Configuration = CreateConfiguration();

            var lotInfo = new LotInfo
            {
                StyleId = StyleId,
                Vin = Vin,
                Codes = GetPipeDelimitedOptionCodeString(GrayMetallic)
            };

            MockLotRepository = new Mock<ILotRepository>();
            MockLotRepository.Setup(x => x.GetInfo(BusinessUnitId, InventoryId)).Returns(lotInfo);

            LotLoader = new LotLoader(MockChromeOptionRepository.Object, MockLotRepository.Object);
        }

        protected override void Act()
        {
            Loaded = LotLoader.Load(BusinessUnitId, Configuration);
        }

        [Test]
        public void It_should_load_the_correct_exterior_color_code()
        {
            Assert.AreEqual(GrayMetallic.OptionCode, Configuration.ExteriorColorCode);
        }

        [Test]
        public void It_should_load_the_correct_exterior_color_description()
        {
            Assert.AreEqual("Space Gray Metallic", Configuration.ExteriorColor1);
        }

        [Test]
        public void It_should_return_loaded()
        {
            Assert.AreEqual(true, Loaded);
        }
    }

    [TestFixture]
    public class When_a_configuration_has_a_primary_exterior_color : LotLoadingTests
    {
        protected override void Arrange()
        {
            Configuration = CreateConfiguration(
                exteriorColorCode: CrimsonRed.OptionCode,
                exteriorColorDescription: "Crimson Red"
            );

            var lotInfo = new LotInfo
            {
                StyleId = StyleId,
                Vin = Vin,
                Codes = GetPipeDelimitedOptionCodeString(GrayMetallic)
            };

            MockLotRepository = new Mock<ILotRepository>();
            MockLotRepository.Setup(x => x.GetInfo(BusinessUnitId, InventoryId)).Returns(lotInfo);

            LotLoader = new LotLoader(MockChromeOptionRepository.Object, MockLotRepository.Object);
        }

        protected override void Act()
        {
            Loaded = LotLoader.Load(BusinessUnitId, Configuration);
        }

        [Test]
        public void It_should_not_override_the_exterior_color_code()
        {
            Assert.AreEqual(CrimsonRed.OptionCode, Configuration.ExteriorColorCode);
        }

        [Test]
        public void It_should_not_override_the_exterior_color_description()
        {
            Assert.AreEqual("Crimson Red", Configuration.ExteriorColor1);
        }

        [Test]
        public void It_should_not_return_loaded()
        {
            Assert.AreEqual(false, Loaded);
        }
    }

    [TestFixture]
    public class When_a_configuration_does_not_have_a_seconday_exterior_color : LotLoadingTests
    {
        protected override void Arrange()
        {
            Configuration = CreateConfiguration();

            var lotInfo = new LotInfo
            {
                StyleId = StyleId,
                Vin = Vin,
                Codes = GetPipeDelimitedOptionCodeString(BlackTop)
            };

            MockLotRepository = new Mock<ILotRepository>();
            MockLotRepository.Setup(x => x.GetInfo(BusinessUnitId, InventoryId)).Returns(lotInfo);

            LotLoader = new LotLoader(MockChromeOptionRepository.Object, MockLotRepository.Object);
        }

        protected override void Act()
        {
            Loaded = LotLoader.Load(BusinessUnitId, Configuration);
        }

        [Test]
        public void It_should_load_the_correct_secondary_exterior_color_code()
        {
            Assert.AreEqual(BlackTop.OptionCode, Configuration.ExteriorColorCode2);
        }

        [Test]
        public void It_should_load_the_correct_secondary_exterior_color_description()
        {
            Assert.AreEqual("Black", Configuration.ExteriorColor2);
        }

        [Test]
        public void It_should_return_loaded()
        {
            Assert.AreEqual(true, Loaded);
        }
    }

    [TestFixture]
    public class When_a_configuration_has_a_secondary_exterior_color : LotLoadingTests
    {
        protected override void Arrange()
        {
            Configuration = CreateConfiguration(
                secondaryExteriorColorCode: WhiteTop.OptionCode,
                secondaryExteriorColorDescription: "White"
            );

            var lotInfo = new LotInfo
            {
                StyleId = StyleId,
                Vin = Vin,
                Codes = GetPipeDelimitedOptionCodeString(BlackTop)
            };

            MockLotRepository = new Mock<ILotRepository>();
            MockLotRepository.Setup(x => x.GetInfo(BusinessUnitId, InventoryId)).Returns(lotInfo);

            LotLoader = new LotLoader(MockChromeOptionRepository.Object, MockLotRepository.Object);
        }

        protected override void Act()
        {
            Loaded = LotLoader.Load(BusinessUnitId, Configuration);
        }

        [Test]
        public void It_should_not_override_the_exterior_color_code()
        {
            Assert.AreEqual(WhiteTop.OptionCode, Configuration.ExteriorColorCode2);
        }

        [Test]
        public void It_should_not_override_the_exterior_color_description()
        {
            Assert.AreEqual("White", Configuration.ExteriorColor2);
        }

        [Test]
        public void It_should_not_return_loaded()
        {
            Assert.AreEqual(false, Loaded);
        }
    }

    [TestFixture]
    public class When_a_configuration_does_not_have_an_interior_color : LotLoadingTests
    {
        protected override void Arrange()
        {
            Configuration = CreateConfiguration();

            var lotInfo = new LotInfo
            {
                StyleId = StyleId,
                Vin = Vin,
                Codes = GetPipeDelimitedOptionCodeString(BlackSeatTrim)
            };

            MockLotRepository = new Mock<ILotRepository>();
            MockLotRepository.Setup(x => x.GetInfo(BusinessUnitId, InventoryId)).Returns(lotInfo);

            LotLoader = new LotLoader(MockChromeOptionRepository.Object, MockLotRepository.Object);
        }

        protected override void Act()
        {
            Loaded = LotLoader.Load(BusinessUnitId, Configuration);
        }

        [Test]
        public void It_should_load_the_correct_exterior_color_code()
        {
            Assert.AreEqual(BlackSeatTrim.OptionCode, Configuration.InteriorColorCode);
        }

        [Test]
        public void It_should_load_the_correct_exterior_color_description()
        {
            Assert.AreEqual("Black Leather", Configuration.InteriorColor);
        }

        [Test]
        public void It_should_return_loaded()
        {
            Assert.AreEqual(true, Loaded);
        }
    }

    [TestFixture]
    public class When_a_configuration_has_an_interior_color : LotLoadingTests
    {
        protected override void Arrange()
        {
            Configuration = CreateConfiguration(
                interiorColorCode: BeigeSeatTrim.OptionCode,
                interiorColorDescription: "Cream Beige"
            );


            var lotInfo = new LotInfo
            {
                StyleId = StyleId,
                Vin = Vin,
                Codes = GetPipeDelimitedOptionCodeString(BlackSeatTrim)
            };

            MockLotRepository = new Mock<ILotRepository>();
            MockLotRepository.Setup(x => x.GetInfo(BusinessUnitId, InventoryId)).Returns(lotInfo);

            LotLoader = new LotLoader(MockChromeOptionRepository.Object, MockLotRepository.Object);
        }

        protected override void Act()
        {
            Loaded = LotLoader.Load(BusinessUnitId, Configuration);
        }

        [Test]
        public void It_should_not_override_the_exterior_color_code()
        {
            Assert.AreEqual(BeigeSeatTrim.OptionCode, Configuration.InteriorColorCode);
        }

        [Test]
        public void It_should_not_override_the_exterior_color_description()
        {
            Assert.AreEqual("Cream Beige", Configuration.InteriorColor);
        }

        [Test]
        public void It_should_not_return_loaded()
        {
            Assert.AreEqual(false, Loaded);
        }
    }

    [TestFixture]
    public class When_loading_a_configuration_with_that_already_has_information : LotLoadingTests
    {
        private VehicleConfiguration _configuration;
        private bool _loaded;

        protected override void Arrange()
        {
            var transfer = new VehicleDataTransfer
            {
                InventoryId = InventoryId,
                ChromeStyleID = StyleId,
                ExteriorColor1 = "Crimson Red",
                ExteriorColorCode = CrimsonRed.OptionCode,
                InteriorColor = "Beige Cream",
                InteriorColorCode = BeigeSeatTrim.OptionCode,
                ExteriorColor2 = "White",
                ExteriorColorCode2 = WhiteTop.OptionCode,
                Options = new DetailedEquipmentCollection { new DetailedEquipment(AlarmSystem), new DetailedEquipment(KeylessEntry), new DetailedEquipment(ColdWeather) },
                Generics = new GenericEquipmentCollection(),
                Replacements = new GenericEquipmentReplacementCollection()
            };

            _configuration = new VehicleConfiguration(BusinessUnitId, transfer);


            var lotInfo = new LotInfo
            {
                StyleId = StyleId,
                Vin = Vin,
                Codes = GetPipeDelimitedOptionCodeString(AlarmSystem, KeylessEntry, GrayMetallic, BlackSeatTrim, ColdWeather, BlackTop)
            };

            MockLotRepository = new Mock<ILotRepository>();
            MockLotRepository.Setup(x => x.GetInfo(BusinessUnitId, InventoryId)).Returns(lotInfo);

            LotLoader = new LotLoader(MockChromeOptionRepository.Object, MockLotRepository.Object);
        }

        protected override void Act()
        {
            _loaded = LotLoader.Load(BusinessUnitId, _configuration);
        }

        [Test]
        public void It_should_not_change_the_exterior_color_code()
        {
            Assert.AreEqual(CrimsonRed.OptionCode, _configuration.ExteriorColorCode);
        }

        [Test]
        public void It_should_not_chane_the_interior_color_code()
        {
            Assert.AreEqual(BeigeSeatTrim.OptionCode, _configuration.InteriorColorCode);
        }

        [Test]
        public void It_should_not_change_the_secondary_color_code()
        {
            Assert.AreEqual(WhiteTop.OptionCode, _configuration.ExteriorColorCode2);
        }

        [Test]
        public void It_should_not_change_the_exterior_color_description()
        {
            Assert.AreEqual("Crimson Red", _configuration.ExteriorColor1);
        }

        [Test]
        public void It_should_not_change_the_secondary_exterior_color_description()
        {
            Assert.AreEqual("White", _configuration.ExteriorColor2);
        }

        [Test]
        public void It_should_not_change_the_interior_color_description()
        {
            Assert.AreEqual("Beige Cream", _configuration.InteriorColor);
        }

        [Test]
        public void It_should_retain_the_old_packages()
        {
            var detailedEquipment = _configuration.VehicleOptions.options;
            Assert.AreEqual(3, detailedEquipment.Count);
            detailedEquipment.Single(d => (d as DetailedEquipment).OptionCode == KeylessEntry.OptionCode);
            detailedEquipment.Single(d => (d as DetailedEquipment).OptionCode == AlarmSystem.OptionCode);
            detailedEquipment.Single(d => (d as DetailedEquipment).OptionCode == ColdWeather.OptionCode);
        }

        [Test]
        public void It_should_return_not_loaded()
        {
            Assert.AreEqual(false, _loaded);
        }
    }

    [TestFixture]
    public class When_loading_a_configuration_that_already_has_packages_with_new_packages : LotLoadingTests
    {
        protected override void Arrange()
        {
            Configuration = CreateConfiguration(
                loadedEquipment: new[] { AlarmSystem, KeylessEntry, ColdWeather }
            );


            var lotInfo = new LotInfo
            {
                StyleId = StyleId,
                Vin = Vin,
                Codes = GetPipeDelimitedOptionCodeString(IpodAdapter)
            };

            MockLotRepository = new Mock<ILotRepository>();
            MockLotRepository.Setup(x => x.GetInfo(BusinessUnitId, InventoryId)).Returns(lotInfo);

            LotLoader = new LotLoader(MockChromeOptionRepository.Object, MockLotRepository.Object);
        }

        protected override void Act()
        {
            Loaded = LotLoader.Load(BusinessUnitId, Configuration);
        }

        [Test]
        public void It_should_append_new_packages()
        {
            var detailedEquipment = Configuration.VehicleOptions.options;
            Assert.AreEqual(4, detailedEquipment.Count);
            detailedEquipment.Single(d => (d as DetailedEquipment).OptionCode == KeylessEntry.OptionCode);
            detailedEquipment.Single(d => (d as DetailedEquipment).OptionCode == AlarmSystem.OptionCode);
            detailedEquipment.Single(d => (d as DetailedEquipment).OptionCode == ColdWeather.OptionCode);
            detailedEquipment.Single(d => (d as DetailedEquipment).OptionCode == IpodAdapter.OptionCode);
        }

        [Test]
        public void It_should_return_loaded()
        {
            Assert.AreEqual(true, Loaded);
        }
    }

    [TestFixture]
    public class When_loading_a_configuration_with_new_packages : LotLoadingTests
    {
        protected override void Arrange()
        {
            Configuration = CreateConfiguration();

            var lotInfo = new LotInfo
            {
                StyleId = StyleId,
                Vin = Vin,
                Codes = GetPipeDelimitedOptionCodeString(IpodAdapter)
            };

            MockLotRepository = new Mock<ILotRepository>();
            MockLotRepository.Setup(x => x.GetInfo(BusinessUnitId, InventoryId)).Returns(lotInfo);

            LotLoader = new LotLoader(MockChromeOptionRepository.Object, MockLotRepository.Object);
        }

        protected override void Act()
        {
            Loaded = LotLoader.Load(BusinessUnitId, Configuration);
        }

        [Test]
        public void It_should_append_new_packages()
        {
            var detailedEquipment = Configuration.VehicleOptions.options;
            Assert.AreEqual(1, detailedEquipment.Count);
            detailedEquipment.Single(d => (d as DetailedEquipment).OptionCode == IpodAdapter.OptionCode);
        }

        [Test]
        public void It_should_return_loaded()
        {
            Assert.AreEqual(true, Loaded);
        }

        [Test]
        public void It_should_set_the_description_of_the_package()
        {
            var detailedEquipment = Configuration.VehicleOptions.options.Single() as DetailedEquipment;
            Assert.AreEqual("IPOD & USB ADAPTER: USB interface", detailedEquipment.DetailText);
        }
    }

    [TestFixture]
    public class When_loading_a_configuration_that_already_with_duplicate_packages : LotLoadingTests
    {
        protected override void Arrange()
        {
            Configuration = CreateConfiguration(
                loadedEquipment: new[] { AlarmSystem, KeylessEntry, ColdWeather }
            );

            var lotInfo = new LotInfo
            {
                StyleId = StyleId,
                Vin = Vin,
                Codes = GetPipeDelimitedOptionCodeString(AlarmSystem, KeylessEntry)
            };

            MockLotRepository = new Mock<ILotRepository>();
            MockLotRepository.Setup(x => x.GetInfo(BusinessUnitId, InventoryId)).Returns(lotInfo);

            LotLoader = new LotLoader(MockChromeOptionRepository.Object, MockLotRepository.Object);
        }

        protected override void Act()
        {
            Loaded = LotLoader.Load(BusinessUnitId, Configuration);
        }

        [Test]
        public void It_should_not_add_new_packages()
        {
            var detailedEquipment = Configuration.VehicleOptions.options;
            Assert.AreEqual(3, detailedEquipment.Count);
            detailedEquipment.Single(d => (d as DetailedEquipment).OptionCode == KeylessEntry.OptionCode);
            detailedEquipment.Single(d => (d as DetailedEquipment).OptionCode == AlarmSystem.OptionCode);
            detailedEquipment.Single(d => (d as DetailedEquipment).OptionCode == ColdWeather.OptionCode);
        }

        [Test]
        public void It_should_not_return_loaded()
        {
            Assert.AreEqual(false, Loaded);
        }
    }

}
