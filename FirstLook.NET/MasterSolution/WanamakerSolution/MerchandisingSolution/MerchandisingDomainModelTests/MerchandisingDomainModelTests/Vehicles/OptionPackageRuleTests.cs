﻿using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Vehicles;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Vehicles
{
    public class OptionPackageRuleTests
    {
        [TestCase("MY LINK PACKAGE includes (UUI) AM/FM stereo with CD player, MP3 playback and USB port, (UPF) Bluetooth for phone, (K34) cruise control, (DCP) OnStar Directions and Connections service plan, 1 year of XM Radio Service, (UK3) leather-wrapped steering wheel mounted audio and cruise controls, leather-wrapped shift lever, (PFD) 16 ", "1 year of XM Radio Service, ", "", @"MY LINK PACKAGE includes (UUI) AM/FM stereo with CD player, MP3 playback and USB port, (UPF) Bluetooth for phone, (K34) cruise control, (DCP) OnStar Directions and Connections service plan, (UK3) leather-wrapped steering wheel mounted audio and cruise controls, leather-wrapped shift lever, (PFD) 16 ")]
        [TestCase("TRANSMISSION, [[%894-SPEED AUTOMATIC TRANSMISSION, 4-SPEED AUTOMATIC, ELECTRONICALLY 1 year of XM Radio Service, CONTROLLED WITH OVERDRIVE Custom ", "1 year of XM Radio Service, ", "", @"TRANSMISSION, [[%894-SPEED AUTOMATIC TRANSMISSION, 4-SPEED AUTOMATIC, ELECTRONICALLY CONTROLLED WITH OVERDRIVE Custom ")]
        public void CanReplaceStringCaseSensitively(string inputstring, string textToReplace, string textToReplaceWith, string expectedOutput)
        {
            var result = OptionPackageTextReplacementRule.ReplaceString(inputstring, textToReplace, textToReplaceWith);
            Assert.That(result.Equals(expectedOutput));
        }

        [TestCase("MY LINK PACKAGE includes (UUI) AM/FM stereo with CD player, MP3 playback and USB port, (UPF) Bluetooth for phone, (K34) cruise control, (DCP) OnStar Directions and Connections service plan, 1 year of XM Radio service, (UK3) leather-wrapped steering wheel mounted audio and cruise controls, leather-wrapped shift lever, (PFD) 16 ", "1 year of XM Radio Service, ", "", @"MY LINK PACKAGE includes (UUI) AM/FM stereo with CD player, MP3 playback and USB port, (UPF) Bluetooth for phone, (K34) cruise control, (DCP) OnStar Directions and Connections service plan, (UK3) leather-wrapped steering wheel mounted audio and cruise controls, leather-wrapped shift lever, (PFD) 16 ")]
        [TestCase("TRANSMISSION, [[%894-SPEED AUTOMATIC TRANSMISSION, 4-SPEED AUTOMATIC, ELECTRONICALLY 1 year of XM Radio service, CONTROLLED WITH OVERDRIVE Custom ", "1 year of XM Radio Service, ", "", @"TRANSMISSION, [[%894-SPEED AUTOMATIC TRANSMISSION, 4-SPEED AUTOMATIC, ELECTRONICALLY CONTROLLED WITH OVERDRIVE Custom ")]
        public void CanReplaceStringCaseInsensitively(string inputstring, string textToReplace, string textToReplaceWith, string expectedOutput)
        {
            var result = OptionPackageTextReplacementRule.ReplaceString(inputstring, textToReplace, textToReplaceWith);
            Assert.That(result.Equals(expectedOutput));
        }

        [TestCase("MY LINK PACKAGE includes (UUI) AM/FM stereo with CD player, MP3 playback and USB port, (UPF) Bluetooth for phone, (K34) cruise control, (DCP) OnStar Directions and Connections service plan, 1 year of XM Radio service, (UK3) leather-wrapped steering wheel mounted audio and cruise controls, leather-wrapped shift lever, (PFD) 16 ", "1 year of XM Radio Service, ", "", @"MY LINK PACKAGE includes (UUI) AM/FM stereo with CD player, MP3 playback and USB port, (UPF) Bluetooth for phone, (K34) cruise control, (DCP) OnStar Directions and Connections service plan, (UK3) leather-wrapped steering wheel mounted audio and cruise controls, leather-wrapped shift lever, (PFD) 16 ")]
        [TestCase("TRANSMISSION, [[%894-SPEED AUTOMATIC TRANSMISSION, 4-SPEED AUTOMATIC, ELECTRONICALLY 1 year of FM Radio service, CONTROLLED WITH OVERDRIVE Custom ", "1 year of XM Radio Service, ", "", "TRANSMISSION, [[%894-SPEED AUTOMATIC TRANSMISSION, 4-SPEED AUTOMATIC, ELECTRONICALLY 1 year of FM Radio service, CONTROLLED WITH OVERDRIVE Custom ")]
        public void ShouldNotReplaceStringThatsNotThere(string inputstring, string textToReplace, string textToReplaceWith, string expectedOutput)
        {
            var result = OptionPackageTextReplacementRule.ReplaceString(inputstring, textToReplace, textToReplaceWith);
            Assert.That(result.Equals(expectedOutput));
        }


        [TestCase("Test 1 year of XM Radio service2", "XM Radio service.", "", "Test 1 year of XM Radio service2")]
        public void ShouldNotInterpretTextToReplaceAsRegEx(string inputstring, string textToReplace, string textToReplaceWith, string expectedOutput)
        {
            var result = OptionPackageTextReplacementRule.ReplaceString(inputstring, textToReplace, textToReplaceWith);
            Assert.That(result.Equals(expectedOutput));
        }

        [TestCase(
            "EXECUTIVE PKG -inc: heated steering wheel, soft-close automatic door operation, active front seats, full LED lights, Head-Up Display, satellite radio w/1-year subscription, BMW Apps",
            "subscription", 
            "subscription from original in-service date",
            "EXECUTIVE PKG -inc: heated steering wheel, soft-close automatic door operation, active front seats, full LED lights, Head-Up Display, satellite radio w/1-year subscription from original in-service date, BMW Apps")]
        public void ShouldReplaceSubscriptionText(string inputstring, string textToReplace, string textToReplaceWith, string expectedOutput)
        {
            var result = OptionPackageTextReplacementRule.ReplaceString(inputstring, textToReplace, textToReplaceWith);
            Assert.That(result.Equals(expectedOutput));
        }


        /// <summary>
        /// testing the rules engine for the newly added wildcard option code "~" x 20
        /// </summary>
        [Test]
        public void RulesEngineTestForWildcardOptionCodes()
        {

            const string optionText = "NAVIGATION SYSTEM PACKAGE -inc: Bluetooth hands free phone and automatic phonebook download capabilities, HD Radio w/iTunes tagging, hard disk drive, Lexus Insider and advanced voice command, Lexus Enform, SafetyConnect, which includes automatic collision notification, stolen vehicle location, emergency assist button (SOS), enhanced roadside assistance, destination assist, eDestination and apps suite (complimentary 1-year trial subscription), Rear Back-Up Camera, XM NavTraffic/XM NavWeather, XM sports, stocks and fuel prices (complimentary 90-day trial subscription)";
            const string alteredText = "NAVIGATION SYSTEM PACKAGE -INC: BLUETOOTH HANDS FREE PHONE AND AUTOMATIC PHONEBOOK DOWNLOAD CAPABILITIES, HD RADIO W/ITUNES TAGGING, HARD DISK DRIVE, LEXUS INSIDER AND ADVANCED VOICE COMMAND, LEXUS ENFORM, SAFETYCONNECT, WHICH INCLUDES AUTOMATIC COLLISION NOTIFICATION, STOLEN VEHICLE LOCATION, EMERGENCY ASSIST BUTTON (SOS), ENHANCED ROADSIDE ASSISTANCE, DESTINATION ASSIST, EDESTINATION AND APPS SUITE , REAR BACK-UP CAMERA, XM NAVTRAFFIC/XM NAVWEATHER, XM SPORTS, STOCKS AND FUEL PRICES ";


            // create a rule or two
            var optionPackageRuleList = new List<OptionPackageRule>
                {
                    new OptionPackageTextReplacementRule
                        {
                            AppliesToInventoryType = 2,
                            OptionCode = new string('~', 20),
                            TextToReplace = "(complimentary 1-year trial subscription)",
                            TextToReplaceWith = ""
                        },
                    new OptionPackageTextReplacementRule
                        {
                            AppliesToInventoryType = 2,
                            OptionCode = new string('~', 20),
                            TextToReplace = "(complimentary 90-day trial subscription)",
                            TextToReplaceWith = ""
                        }
                };

            // set up mock rule repository to return the rules we created above
            var rulesRepoMock = new Mock<IOptionPackageRulesRepository>();
            rulesRepoMock.Setup(rrm => rrm.GetRulesForVehicle(It.IsAny<int>(), It.IsAny<int>())).Returns(optionPackageRuleList);

            // create the rules engine
            var rulesEngine = new OptionPackageRulesEngine(rulesRepoMock.Object);

            // create a few options
            var detailEquipmentList = new List<DetailedEquipment>
                {new DetailedEquipment("ZC1", optionText, "", optionText, false)};


            rulesEngine.Process(1, 1, detailEquipmentList);

            Assert.AreEqual(alteredText, detailEquipmentList[0].OptionText, "Option Text replacement engine failed!");

        }

        /// <summary>
        /// test to make sure only specific text replacement rules are applied to options
        /// </summary>
        [Test]
        public void RulesEngineTestForSpecificOptionCodes()
        {

            const string optionText = "NAVIGATION SYSTEM PACKAGE -inc: Bluetooth hands free phone and automatic phonebook download capabilities, HD Radio w/iTunes tagging, hard disk drive, Lexus Insider and advanced voice command, Lexus Enform, SafetyConnect, which includes automatic collision notification, stolen vehicle location, emergency assist button (SOS), enhanced roadside assistance, destination assist, eDestination and apps suite (complimentary 1-year trial subscription), Rear Back-Up Camera, XM NavTraffic/XM NavWeather, XM sports, stocks and fuel prices (complimentary 90-day trial subscription)";
            const string alteredText = "NAVIGATION SYSTEM PACKAGE -INC: BLUETOOTH HANDS FREE PHONE AND AUTOMATIC PHONEBOOK DOWNLOAD CAPABILITIES, HD RADIO W/ITUNES TAGGING, HARD DISK DRIVE, LEXUS INSIDER AND ADVANCED VOICE COMMAND, LEXUS ENFORM, SAFETYCONNECT, WHICH INCLUDES AUTOMATIC COLLISION NOTIFICATION, STOLEN VEHICLE LOCATION, EMERGENCY ASSIST BUTTON (SOS), ENHANCED ROADSIDE ASSISTANCE, DESTINATION ASSIST, EDESTINATION AND APPS SUITE , REAR BACK-UP CAMERA, XM NAVTRAFFIC/XM NAVWEATHER, XM SPORTS, STOCKS AND FUEL PRICES (COMPLIMENTARY 90-DAY TRIAL SUBSCRIPTION)";


            // create a rule or two
            var optionPackageRuleList = new List<OptionPackageRule>
                {
                    new OptionPackageTextReplacementRule
                        {
                            AppliesToInventoryType = 2,
                            OptionCode = "ABC",
                            TextToReplace = "(complimentary 1-year trial subscription)",
                            TextToReplaceWith = ""
                        },
                    new OptionPackageTextReplacementRule
                        {
                            AppliesToInventoryType = 2,
                            OptionCode = "EFG", // this rule should NOT apply!!!
                            TextToReplace = "(complimentary 90-day trial subscription)",
                            TextToReplaceWith = ""
                        }
                };

            // set up mock rule repository to return the rules we created above
            var rulesRepoMock = new Mock<IOptionPackageRulesRepository>();
            rulesRepoMock.Setup(rrm => rrm.GetRulesForVehicle(It.IsAny<int>(), It.IsAny<int>())).Returns(optionPackageRuleList);

            // create the rules engine
            var rulesEngine = new OptionPackageRulesEngine(rulesRepoMock.Object);

            // create a few options
            var detailEquipmentList = new List<DetailedEquipment> { new DetailedEquipment("ABC", optionText, "", optionText, false) };


            rulesEngine.Process(1, 1, detailEquipmentList);

            Assert.AreEqual(alteredText, detailEquipmentList[0].OptionText, "Option Text replacement engine failed!");

        }


    }
}
