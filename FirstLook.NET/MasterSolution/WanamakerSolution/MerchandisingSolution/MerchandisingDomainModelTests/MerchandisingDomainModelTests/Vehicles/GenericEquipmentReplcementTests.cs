﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Merchandising.DomainModel.Templating.Previews;
using FirstLook.Merchandising.DomainModel.Vehicles;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Vehicles
{
    [TestFixture(Description = "Tests for Euipment Group Replacements")]
    public class GenericEquipmentReplcementTests
    {
        [Test( Description = "Fogbugzid : 13743" )]
        public void Should_apply_longest_replacement_group_first ()
        {
            var rules = CreateTestRules();
            var generics = CreateGenerics();

            var duplicateRemover = new GenericEquipmentCollectionDuplicateRemover( rules, generics );

            var result = duplicateRemover.RemoveDuplicates();
            Assert.AreEqual( result.Count, 5 );
            Assert.AreEqual( ( (GenericEquipmentReplacement) result[0] ).Description, "Heated Leather Seats" );
            Assert.AreEqual( ( (GenericEquipment) result[1] ).Description, "Keyless Entry" );
            Assert.AreEqual( ( (GenericEquipment) result[4] ).Description, "Turbocharged" );
        }

        private static GenericEquipmentReplacementCollection CreateTestRules ()
        {
            var includedCategories1 = new CategoryCollection( "TestHeader1" )
                                          {
                                              new CategoryLink(4,  "", 0, "","Heated front seats"),
                                              new CategoryLink(5,  "", 0, "","Heated back seats")
                                          };
            var replacementRule1 = new GenericEquipmentReplacement() { Description = "Heated seats", IncludesCategories = includedCategories1 };

            var includedCategories2 = new CategoryCollection( "TestHeader2" )
                                          {
                                              new CategoryLink(4,  "", 0, "","Heated front seats"),
                                              new CategoryLink(5,  "", 0, "","Heated back seats"),
                                              new CategoryLink(6,  "", 0, "","Leather seats")
                                          };
            var replacementRule2 = new GenericEquipmentReplacement { Description = "Heated Leather Seats", IncludesCategories = includedCategories2 };

            return  new GenericEquipmentReplacementCollection { replacementRule1, replacementRule2 };
        }

        private static GenericEquipmentCollection CreateGenerics ()
        {
            return new GenericEquipmentCollection
                               {
                                   new GenericEquipment(new CategoryLink(7, "", 0, "", "Keyless Entry"), true),
                                   new GenericEquipment( new CategoryLink(4, "", 0, "", "Heated Driver Seats"), true),
                                   new GenericEquipment( new CategoryLink(5, "", 0, "", "Heated Passenger Seats"), true),
                                   new GenericEquipment(new CategoryLink(6, "", 0, "", "Leather interior"), true),
                                   new GenericEquipment(new CategoryLink(1, "", 0, "", "Satellite Radio"), true),
                                   new GenericEquipment(new CategoryLink(2, "", 0, "", "Heated Mirror"), true),
                                   new GenericEquipment(new CategoryLink(3, "", 0, "", "Turbocharged"), true)
                               };
        }


    }
}
