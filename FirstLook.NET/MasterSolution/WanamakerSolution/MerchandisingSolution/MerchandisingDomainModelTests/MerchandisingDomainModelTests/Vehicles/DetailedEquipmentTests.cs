﻿using FirstLook.Merchandising.DomainModel.Vehicles;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Vehicles
{
    [TestFixture]
    class DetailedEquipmentTests
    {

        [Test]
        public void ShortString()
        {
            string input = "abc";
            string output = DetailedEquipment.RemoveLeadingOptionCode(input);
            Assert.AreEqual(input, output);
        }

        [Test]
        public void LongString()
        {
            string input = "abcdefghi";
            string output = DetailedEquipment.RemoveLeadingOptionCode(input);
            Assert.AreEqual(input, output);
        }

        [Test]
        public void CodeOnly()
        {
            string input = "[abc]";
            string output = DetailedEquipment.RemoveLeadingOptionCode(input);
            Assert.IsEmpty(output);
        }

        [Test]
        public void CodeAtBeginning()
        {
            string input = "[abc] def";
            string output = DetailedEquipment.RemoveLeadingOptionCode(input);
            Assert.AreEqual(" def", output);
        }

    }
}
