﻿using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.MaxAnalytics;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using Moq;
using NUnit.Framework;
using FirstLook.Merchandising.DomainModel.Core;

namespace MerchandisingDomainModelTests.Vehicles
{
    [TestFixture]
    public class InventoryOnlineStateDataTests
    {
        [TestCase(false, false, 0)]
        [TestCase(true,  true, 1)]
        [TestCase(false, true,  0)]
        [TestCase(true, false, 2)]
        public void ConfirmedSentOnlineState(bool hasData, bool online, int expectedOnlineState) // online states: 0 - no data, 1 - online, 2 - offline
        {
            var maxAnalyticsMock = new Mock<IMaxAnalytics>();
            maxAnalyticsMock.Setup(x => x.GetOnlineVehicles(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())).Returns(VehiclesOnlineTestData(hasData, online));
            var invOnlineStateData = new InventoryOnlineStateData(maxAnalyticsMock.Object);
            var inventoryData = GetTestInventoryData();

            invOnlineStateData.CrossFill(1, 1, 1, inventoryData);

            Assert.That(inventoryData.Find(invData => invData.InventoryID == 1).OnlineState == expectedOnlineState);
        }

        private static IVehiclesOnline[] VehiclesOnlineTestData(bool hasData, bool online)
        {
            return new List<IVehiclesOnline>
            {
                new VehiclesOnline
                {
                    BusinessUnitId = 1,
                    InventoryId = 1,
                    Approved = true,
                    HasData = hasData,
                    Online = online,
                    Certified = false,
                    InventoryReceivedDate = DateTime.Now.AddDays(-5),
                    InventoryType = 1,
                    Make = "Make-1",
                    Model = "Model-1",
                    StockNumber = "stock-11",
                    Trim = "Trim-1",
                    VehicleYear = 1,
                    Vin = "vin-1",
                    ZipCode = "zipcode-1"
                }
            }.ToArray();
        }

        private static List<IInventoryData> GetTestInventoryData()
        {
            return new List<IInventoryData>
            {                                                                                                                                                                                                                                                                                                                                                                                // status bucket                                                                                                              
                new InventoryData("stock-11", 1, "vin-1", 1, false, "", null,1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2013", "Make-1", "Model-1", "MarketClass-1", 1, "Trim-1", ""                 , 1, false, 1, 1, 1, 1, 1, 1, "baseColor-1", "extCode-1", "extDesc-1", "ext2Code-1", "ext2Desc-1", "intCode-1", "intDesc-1", 1, null, "vehicleLocation-1", 1, DateTime.Now.AddDays(-7), null,              null, 1, 1, 1, false, null, false, false, null, null, false,"body style 1","Engine Option1", null, true)
            };
        }
    }
}
