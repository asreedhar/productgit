﻿using FirstLook.Merchandising.DomainModel.Vehicles;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Vehicles
{
    [TestFixture]
    public class ConsumerInfoTests
    {
        [TestCase("City 20/hwy 27 (3.0L engine/4-speed auto trans) (Estimated)", 20, 27)]
        [TestCase("City 9/hwy 15 (3.0L engine/4-speed auto trans) (Estimated)", 9, 15)]
        [TestCase("City 009/hwy 16 (3.0L engine/4-speed auto trans) (Estimated)", 9, 16)]
        [TestCase("City 2/hwy 5 (3.0L engine/4-speed auto trans) (Estimated)", 2, 5)]
        [TestCase("City 19/hwy 24", 19, 24)]
        [TestCase("City 9/hwy 14", 9, 14)]
        [TestCase("City 1/hwy 4", 1, 4)]
        [TestCase("EPA mileage - city 17/hwy 26 (5.0L STD engine/5-spd manual trans)", 17, 26)]
        [TestCase("EPA mileage - city 7/hwy 17 (5.0L STD engine/5-spd manual trans)", 7, 17)]
        [TestCase("EPA mileage - city 1/hwy 6 (5.0L STD engine/5-spd manual trans)", 1, 6)]
        [TestCase("16 city/21 hwy", 16, 21)]
        [TestCase("6 city/9 hwy", 6, 9)]
        [TestCase("16 city/100 hwy", 16, 100)]
        [TestCase("EPA mileage - 16 city/21 hwy (5-spd manual 4-spd auto trans)", 16, 21)]
        [TestCase("EPA mileage - 4 city/8 hwy (5-spd manual 4-spd auto trans)", 4, 8)]
        [TestCase("EPA mileage - 2 city/11 hwy (5-spd manual 4-spd auto trans)", 2, 11)]
        public void Should_return_correct_mileage(string description, int expectedCity, int expectedHighway)
        {
            var mileage = GasMileageParser.Parse(description);

            Assert.That(mileage.City, Is.EqualTo(expectedCity));
            Assert.That(mileage.Highway, Is.EqualTo(expectedHighway));
        }

        [Test]
        public void Should_return_0_if_invalid_string()
        {
            var mileage = GasMileageParser.Parse("invalid string");
            Assert.That(mileage.City, Is.EqualTo(0));
            Assert.That(mileage.Highway, Is.EqualTo(0));
        }

        [Test]
        public void Should_return_0_if_string_has_city_but_no_number()
        {
            var mileage = GasMileageParser.Parse("city / hwy");
            Assert.That(mileage.City, Is.EqualTo(0));
            Assert.That(mileage.Highway, Is.EqualTo(0));
        }

    }
}
