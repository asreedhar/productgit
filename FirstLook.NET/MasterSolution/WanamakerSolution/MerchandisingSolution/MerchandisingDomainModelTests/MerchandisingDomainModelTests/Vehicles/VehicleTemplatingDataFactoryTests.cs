﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using FirstLook.Merchandising.DomainModel.Vehicles.VehicleTemplatingDataDecorators;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Vehicles
{
    [TestFixture]
    public class VehicleTemplatingDataFactoryTests
    {

        [Test]
        public void CanConstruct()
        {
            var mockTemplateData = new Mock<IVehicleTemplatingData>();
            mockTemplateData.SetupGet(x => x.InventoryItem).Returns(FakeInventoryDatas.FirstOrDefault(data => data.BusinessUnitID == 1 && data.InventoryID == 1));

            var templatingDataFactory = new Mock<VehicleTemplatingDataFactory>();
            templatingDataFactory
                .Setup(x => x.GetUnderLyingTemplatingData(
                    It.IsAny<int>(),
                    It.IsAny<int>(),
                    It.IsAny<string>(),
                    It.IsAny<ITemplateMediator>()))
                .Returns(mockTemplateData.Object);


            var factory = templatingDataFactory.Object;
            var decorator = factory.GetVehicleTemplatingData(1, 1, "testUser");
            Assert.IsInstanceOf<VehicleTemplatingDataDecorator>(decorator);
        }


        [TestCase(102481, 24020806, true)]
        [TestCase(102481, 27896798, true)]
        [TestCase(1, 1, false)]
        public void FactoryReturnsLincoln(int businessUnitId, int inventoryId, bool expected)
        {
            var mockTemplateData = new Mock<IVehicleTemplatingData>();
            mockTemplateData.SetupGet(x => x.InventoryItem).Returns(FakeInventoryDatas.FirstOrDefault(data => data.BusinessUnitID == businessUnitId && data.InventoryID == inventoryId));

            var templatingDataFactory = new Mock<VehicleTemplatingDataFactory>();
            templatingDataFactory
                .Setup(x => x.GetUnderLyingTemplatingData(
                    It.IsAny<int>(),
                    It.IsAny<int>(),
                    It.IsAny<string>(),
                    It.IsAny<ITemplateMediator>()))
                .Returns(mockTemplateData.Object);

            var factory = templatingDataFactory.Object;
            
            var templatingData = factory.GetVehicleTemplatingData(businessUnitId, inventoryId, "testUser");

            Assert.IsTrue((templatingData.GetType() == typeof(LincolnVehicleTemplatingData)) == expected);
        }


        [TestCase(106578, 35932811, true)]
        [TestCase(106578, 33010399, true)]
        [TestCase(102481, 27896798, false)]
        public void FactoryReturnsMazda(int businessUnitId, int inventoryId, bool expected)
        {
            var mockTemplateData = new Mock<IVehicleTemplatingData>();
            mockTemplateData.SetupGet(x => x.InventoryItem).Returns(FakeInventoryDatas.FirstOrDefault(data => data.BusinessUnitID == businessUnitId && data.InventoryID == inventoryId));

            var templatingDataFactory = new Mock<VehicleTemplatingDataFactory>();
            templatingDataFactory
                .Setup(x => x.GetUnderLyingTemplatingData(
                    It.IsAny<int>(),
                    It.IsAny<int>(),
                    It.IsAny<string>(),
                    It.IsAny<ITemplateMediator>()))
                .Returns(mockTemplateData.Object);

            var factory = templatingDataFactory.Object;

            var templatingData = factory.GetVehicleTemplatingData(businessUnitId, inventoryId, "testUser");

            Assert.IsTrue((templatingData.GetType() == typeof(MazdaVehicleTemplatingData)) == expected);
        }

        #region Test Data
        private static IEnumerable<IInventoryData> FakeInventoryDatas
        {
            //Stock,BUID,VIN,UnitCost,Certified,CertifiedId,mileageReceived,TradeOrPurchase,ListPrice,ReceivedDate,vehicleYear,make,model,marketClass,segmentId,trim,afterMarketTrim,
            //InventoryId,lowActivityFlag,ExteriorStatus,pricingStatus,InteriorStatus,photoStatus,adReviewStatus,postingStatus,BaseColor,ExteriorColor,ExtColor1,ExteriorColorCode,ExtColor2,
            //InteriorColorCode,IntColor,ChromeStyleId,VehicleCatalogId,VehicleLocation,InventoryType,datePosted,StatusBucket,lastUpdateListPrice,desiredPhotoCountq,
            //LotPrice,MSRP,DueForRepricing,SpecialPrice,HasCurrentBookVal,HasCarFax,autoloadStatusType,autoloadEndTime,NoPackages,bodyStyleName,engineDescription,engineOptionCode,isStandardEngine, 
            //null, null, null,
            get
            {
                return new List<IInventoryData>
                {
                    new InventoryData("JL12018", 102481, "2LMHJ5FR3CBL53869", (decimal) 36100.61, false, null, null,2, 1, 45770, new DateTime(2012, 02, 23), "2012", "Lincoln", "MKT",
                        "2WD Sport Utility Vehicles", 6, null, null, 24020806, true, 2, 0, 2, 2, 2, 4, "White Platinum Tri-Coat", "UG", "White Platinum Tri-Coat", null, null, "TK",
                        "Light Stone", 333773, 716857, null, 1, new DateTime(2012, 11, 27, 14, 13, 0), 128, 43330, 12, 45770, 45770, true, 43330, false, false, null, null, false),
                    new InventoryData("JL13016", 102481, "2LMHJ5AT0DBL55003", (decimal) 50950.64, false, null, null, 2, 1, 55530, new DateTime(2012, 11, 13), "2013", "Lincoln", "MKT",
                        "4WD Sport Utility Vehicles", 6, "EcoBoost", null, 27896798, false, 2, 0, 2, 2, 2, 4, "Smoked Quartz Tinted Metallic Clearcoat", "TQ",
                        "Smoked Quartz Tinted Metallic Clearcoat", null, null, "6W", "Charcoal Black", 348568, 796280, null, 1, new DateTime(2013, 01, 29, 09, 47, 0), 128, 55530,
                        12, 55530, 55530, true, null, false, false, null, null, true),
                    new InventoryData("stock-11", 1, "vin-1", 1, false, null, null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2013", "Dodge", "Ram", "MarketClass-1", 1, null, null,
                        1, false, 1, 1, 1, 1, 1, 1, "baseColor-1", "extCode-1", "extDesc-1", "ext2Code-1", "ext2Desc-1", "intCode-1", "intDesc-1", 1, null, "vehicleLocation-1", 1,
                        DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false),
                    new InventoryData("stock-21", 1, "vin-2", 1, false, null, null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2012", "Ford", "Model-2", "MarketClass-2", 1, "Trim-2",
                        "afterMarketTrim-2", 2, false, 1, 1, 1, 1, 1, 1, "baseColor-2", "extCode-2", "extDesc-2", "ext2Code-2", "ext2Desc-2", "intCode-2", "intDesc-2", 1, null,
                        "vehicleLocation-2", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false),
                    new InventoryData("stock-21", 1, "vin-2", 1, false, null, null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2012", "Mercury", "Model-2", "MarketClass-2", 1,
                        "Trim-2", "afterMarketTrim-2", 3, false, 1, 1, 1, 1, 1, 1, "baseColor-2", "extCode-2", "extDesc-2", "ext2Code-2", "ext2Desc-2", "intCode-2", "intDesc-2", 1,
                        null, "vehicleLocation-2", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false),
                    new InventoryData("stock-21", 1, "vin-2", 1, false, null, null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2012", "Chevrolet", "Model-2", "MarketClass-2", 1,
                        "Trim-2", "afterMarketTrim-2", 4, false, 1, 1, 1, 1, 1, 1, "baseColor-2", "extCode-2", "extDesc-2", "ext2Code-2", "ext2Desc-2", "intCode-2", "intDesc-2", 1,
                        null, "vehicleLocation-2", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false),
                    new InventoryData("N8464", 106578, "JM1BM1V78E1140243", (decimal) 25100.40, false, null, null,2, 1, 29996, new DateTime(2014, 05, 23), "2014", "Mazda", "Mazda3",
                        "4-door Compact Passenger Car", 6, "i Touring", null, 33010399, true, 2, 0, 2, 2, 2, 4, "Blue Reflex Mica", "42B", "Blue Reflex Mica", null, null, "BV0",
                        "Black", 362686, 911815, null, 1, new DateTime(2012, 11, 27, 14, 13, 0), 128, 23989, 12, 29996, 29996, true, 43330, false, false, null, null, false),
                    new InventoryData("N8676T", 106578, "JM3KE4CY6F0462503", (decimal) 28100.40, false, null, null,2, 1, 29996, new DateTime(2014, 05, 23), "2014", "Mazda", "CX-5",
                        "4WD Sport Utility Vehicles", 6, "Touring", null, 35932811, true, 2, 0, 2, 2, 2, 4, "Crystal White Mica", "434K", "Crystal White Mica", null, null, "KC3",
                        "Black", 362686, 911815, null, 1, new DateTime(2012, 11, 27, 14, 13, 0), 128, 23989, 12, 29996, 29996, true, 43330, false, false, null, null, false)
                };
            }
        }
        #endregion Test Data
    }
}
