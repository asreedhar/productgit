﻿using FirstLook.Merchandising.DomainModel.Vehicles;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Vehicles
{
    [TestFixture]
    public class StepStatusTests
    {
        private IStepStatus MakeStepStatus(StepStatusTypes typeId, int? level = null, int? defaultLevel = null,
            int businessUnitId = 0, int inventoryId = 0, byte[] version = null)
        {
            return new StepStatus(
                businessUnitId, inventoryId, typeId, level, defaultLevel, version ?? new byte[8]);
        }

        [Test]
        public void New_StepStatus_should_be_dirty_after_StatusLevel_has_changed()
        {
            var status = MakeStepStatus(StepStatusTypes.Photos, (int)ActivityStatusCodes.Incomplete);
            Assert.That(status.IsDirty, Is.False);

            status.StatusLevel = (int)ActivityStatusCodes.Complete;
            Assert.That(status.IsDirty, Is.True);

            status.StatusLevel = (int) ActivityStatusCodes.Incomplete;
            Assert.That(status.IsDirty, Is.True);
        }

        [TestCase(2, Result = false)]
        [TestCase(null, Result = true)]
        public bool IsNew_should_return_true_if_StatusLevel_is_null(int? statusLevel)
        {
            var status = MakeStepStatus(StepStatusTypes.AdReview, statusLevel);
            return status.IsNew;
        }

        [Test]
        public void StatusLevel_should_return_Untouched_if_createed_with_null_value()
        {
            var status = MakeStepStatus(StepStatusTypes.Pricing);
            Assert.That(status.StatusLevel, Is.EqualTo((int) ActivityStatusCodes.Untouched));
        }


        [TestCase(1, 2, Result = 1)]
        [TestCase(1, null, Result = 1)]
        [TestCase(null, 2, Result = 2)]
        [TestCase(null, null, Result = (int) ActivityStatusCodes.Untouched)]
        public int UseDefaultLevelOverrideWhenPassedIn(int? level, int? defaultLevel)
        {
            var status = MakeStepStatus(StepStatusTypes.ExteriorEquipment, level: level, defaultLevel: defaultLevel);
            return status.StatusLevel;
        }
    }
}