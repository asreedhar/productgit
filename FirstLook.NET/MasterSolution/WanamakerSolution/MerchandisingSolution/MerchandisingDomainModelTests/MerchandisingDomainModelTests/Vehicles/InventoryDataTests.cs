﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Vehicles
{

    public class InventoryDataTests
    {
        [Test]
        public void Can_Sort_Inventory_By_Make_Z_A()
        {
            const int businessUnitId = 1234;
            var list = new List<InventoryData>{
                new InventoryData{ Make = "Chevrolet", Model = "Trailblazer", Year="2005"},
                new InventoryData{ Make = "Buick", Model = "Century", Year="2002"},
                new InventoryData{ Make = "Dodge", Model = "Dakota", Year="2001"},
                new InventoryData{ Make = "PT Cruiser", Model = "Chrysler", Year="2003"},
                 new InventoryData{ Make = "Chevrolet", Model = "Silverado", Year="1999"}
            };

            var newList = InventoryDataFilter.WorkflowSearch(businessUnitId, list, 0, 20, "make", new Bin(inv => true));

            Assert.AreEqual(newList[4].Make, "Buick");
            Assert.AreEqual(newList[3].Make, "Chevrolet");
            Assert.AreEqual(newList[2].Make, "Chevrolet");
            Assert.AreEqual(newList[1].Make, "Dodge");
            Assert.AreEqual(newList[0].Make, "PT Cruiser");
        }

        [Test]
        public void Can_Sort_Inventory_By_Make_A_Z()
        {
            const int businessUnitId = 1234;
            var list = new List<InventoryData>{
                new InventoryData{ Make = "Chevrolet", Model = "Trailblazer", Year="2005"},
                new InventoryData{ Make = "Buick", Model = "Century", Year="2002"},
                new InventoryData{ Make = "Dodge", Model = "Dakota", Year="2001"},
                new InventoryData{ Make = "PT Cruiser", Model = "Chrysler", Year="2003"},
                 new InventoryData{ Make = "Chevrolet", Model = "Silverado", Year="1999"}
            };

            var newList = InventoryDataFilter.WorkflowSearch(businessUnitId, list, 0, 20, "make asc", new Bin(inv => true));

            Assert.AreEqual(newList[0].Make, "Buick");
            Assert.AreEqual(newList[1].Make, "Chevrolet");
            Assert.AreEqual(newList[2].Make, "Chevrolet");
            Assert.AreEqual(newList[3].Make, "Dodge");
            Assert.AreEqual(newList[4].Make, "PT Cruiser");
        }

        [Test]
        public void Can_Sort_Inventory_By_Model_Z_A()
        {
            const int businessUnitId = 1234;
            var list = new List<InventoryData>{
                new InventoryData{ Make = "Chevrolet", Model = "Trailblazer", Year="2005"},
                new InventoryData{ Make = "Buick", Model = "Century", Year="2002"},
                new InventoryData{ Make = "Dodge", Model = "Dakota", Year="2001"},
                new InventoryData{ Make = "PT Cruiser", Model = "Chrysler", Year="2003"},
                 new InventoryData{ Make = "Chevrolet", Model = "Silverado", Year="1999"}
            };

            var newList = InventoryDataFilter.WorkflowSearch(businessUnitId, list, 0, 20, "model", new Bin(inv => true));

            Assert.AreEqual(newList[4].Model, "Century");
            Assert.AreEqual(newList[3].Model, "Chrysler");
            Assert.AreEqual(newList[2].Model, "Dakota");
            Assert.AreEqual(newList[1].Model, "Silverado");
            Assert.AreEqual(newList[0].Model, "Trailblazer");
        }

        [Test]
        public void Can_Sort_Inventory_By_Model_A_Z()
        {
            const int businessUnitId = 1234;
            var list = new List<InventoryData>{
                new InventoryData{ Make = "Chevrolet", Model = "Trailblazer", Year="2005"},
                new InventoryData{ Make = "Buick", Model = "Century", Year="2002"},
                new InventoryData{ Make = "Dodge", Model = "Dakota", Year="2001"},
                new InventoryData{ Make = "PT Cruiser", Model = "Chrysler", Year="2003"},
                 new InventoryData{ Make = "Chevrolet", Model = "Silverado", Year="1999"}
            };

            var newList = InventoryDataFilter.WorkflowSearch(businessUnitId, list, 0, 20, "model asc", new Bin(inv => true));

            Assert.AreEqual(newList[0].Model, "Century");
            Assert.AreEqual(newList[1].Model, "Chrysler");
            Assert.AreEqual(newList[2].Model, "Dakota");
            Assert.AreEqual(newList[3].Model, "Silverado");
            Assert.AreEqual(newList[4].Model, "Trailblazer");
        }
    }


}
