﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using FirstLook.Merchandising.DomainModel.Vehicles.VehicleTemplatingDataDecorators;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Vehicles
{
    [TestFixture]
    public class VehicleTemplatingDataDecoratorTests
    {

        [TestCase(102481, 24020806, true)]
        [TestCase(102481, 27896798, true)]
        public void Deocorator_Trim_Calls_Underlying_Template_Data(int businessUnitId, int inventoryId, bool expected)
        {
            var testInventory = FakeInventoryDatas.First(data => data.BusinessUnitID == businessUnitId && data.InventoryID == inventoryId);

            var mockTemplateData = new Mock<IVehicleTemplatingData>();
            var mockConfig = new Mock<IVehicleConfiguration>();

            mockConfig.SetupGet(x => x.AfterMarketTrim).Returns(testInventory.AfterMarketTrim);
            mockTemplateData.SetupGet(x => x.InventoryItem).Returns(testInventory);
            mockTemplateData.SetupGet(x => x.VehicleConfig).Returns(mockConfig.Object);
        
            var templatingData = new VehicleTemplatingDataDecorator(mockTemplateData.Object);


            // Loop through all the properties and "get" the values.
            var t = typeof (IVehicleTemplatingData).GetProperties();
            foreach (var x in t.Select(p => p.GetValue(templatingData, null)))
            {
            }

            // verify the values actually called "get" on the underlying mock
            mockTemplateData.Verify(x => x.AdditionalInformation, Times.Once());
            mockTemplateData.Verify(x => x.AfterMarketEquipment, Times.Once());
            mockTemplateData.Verify(x => x.AutoCheckAssured, Times.Once());
            mockTemplateData.Verify(x => x.AutoCheckInfo, Times.Once());
            mockTemplateData.Verify(x => x.AutoCheckOneOwner, Times.Once());
            mockTemplateData.Verify(x => x.AutoCheckScore, Times.Once());
            mockTemplateData.Verify(x => x.BasicWarranty, Times.Once());
            mockTemplateData.Verify(x => x.BestGasMileage, Times.Once());
            mockTemplateData.Verify(x => x.BestModelAward, Times.Once());
            mockTemplateData.Verify(x => x.Bookouts, Times.Once());
            mockTemplateData.Verify(x => x.BuybackGuarantee, Times.Once());
            mockTemplateData.Verify(x => x.Carfax1Owner, Times.Once());
            mockTemplateData.Verify(x => x.CarfaxInfo, Times.Once());
            mockTemplateData.Verify(x => x.CertificationDetails, Times.Once());
            mockTemplateData.Verify(x => x.CertificationPreview, Times.Once());
            mockTemplateData.Verify(x => x.CertificationTitle, Times.Once());
            mockTemplateData.Verify(x => x.Certified, Times.Once());
            mockTemplateData.Verify(x => x.CertifiedID, Times.Once());
            mockTemplateData.Verify(x => x.CleanAutoCheckReport, Times.Once());
            mockTemplateData.Verify(x => x.CleanCarfaxReport, Times.Once());
            mockTemplateData.Verify(x => x.Colors, Times.Once());
            mockTemplateData.Verify(x => x.ConditionSentence, Times.Once());
            mockTemplateData.Verify(x => x.ConditionSummary, Times.Once());
            mockTemplateData.Verify(x => x.ConsInfo, Times.Once());
            mockTemplateData.Verify(x => x.ConvenienceOptions, Times.Once());
            mockTemplateData.Verify(x => x.CountBelowBook, Times.Once());
            mockTemplateData.Verify(x => x.CountBelowEightThousandDollars, Times.Once());
            mockTemplateData.Verify(x => x.CountBelowFifteenThousandDollars, Times.Once());
            mockTemplateData.Verify(x => x.CountBelowNineThousandDollars, Times.Once());
            mockTemplateData.Verify(x => x.CountBelowSevenThousandDollars, Times.Once());
            mockTemplateData.Verify(x => x.CountBelowTenThousandDollars, Times.Once());
            mockTemplateData.Verify(x => x.CountBelowTwentyThousandDollars, Times.Once());
            mockTemplateData.Verify(x => x.CountInPriceRange, Times.Once());
            mockTemplateData.Verify(x => x.DisclaimerDate, Times.Once());
            mockTemplateData.Verify(x => x.DriverFrontCrash, Times.Once());
            mockTemplateData.Verify(x => x.DriverSideCrash, Times.Once());
            mockTemplateData.Verify(x => x.Drivetrain, Times.Once());
            mockTemplateData.Verify(x => x.DrivetrainOfInterest, Times.Once());
            mockTemplateData.Verify(x => x.DrivetrainWarranty, Times.Once());
            mockTemplateData.Verify(x => x.Engine, Times.Once());
            mockTemplateData.Verify(x => x.EngineDetails, Times.Once());
            mockTemplateData.Verify(x => x.EngineOfInterest, Times.Once());
            mockTemplateData.Verify(x => x.ExpirationDate, Times.Once());
            mockTemplateData.Verify(x => x.ExteriorColor, Times.Once());
            mockTemplateData.Verify(x => x.FuelCity, Times.Once());
            mockTemplateData.Verify(x => x.FuelHwy, Times.Once());
            mockTemplateData.Verify(x => x.FuelType, Times.Once());
            mockTemplateData.Verify(x => x.FuelTypeOfInterest, Times.Once());
            mockTemplateData.Verify(x => x.GetsGoodGasMileage, Times.Once());
            mockTemplateData.Verify(x => x.GoodCrashTests, Times.Once());
            mockTemplateData.Verify(x => x.GoodJdPowerRatings, Times.Once());
            mockTemplateData.Verify(x => x.GoodWarranties, Times.Once());
            mockTemplateData.Verify(x => x.HasAwards, Times.Once());
            mockTemplateData.Verify(x => x.HasBuybackGuarantee, Times.Once());
            mockTemplateData.Verify(x => x.HasCertificationDetails, Times.Once());
            mockTemplateData.Verify(x => x.HasCertificationPreview, Times.Once());
            mockTemplateData.Verify(x => x.HasColors, Times.Once());
            mockTemplateData.Verify(x => x.HasCondition, Times.Once());
            mockTemplateData.Verify(x => x.HasConditionDetails, Times.Once());
            mockTemplateData.Verify(x => x.HasConditionHighlights, Times.Once());
            mockTemplateData.Verify(x => x.HasConvenienceOptions, Times.Once());
            mockTemplateData.Verify(x => x.HasDisplayablePrice, Times.Once());
            mockTemplateData.Verify(x => x.HasEnoughTier1EquipmentAvailable, Times.Once());
            mockTemplateData.Verify(x => x.HasEnoughTier2EquipmentAvailable, Times.Once());
            mockTemplateData.Verify(x => x.HasFinancingAvailable, Times.Once());
            mockTemplateData.Verify(x => x.HasGoodAutoCheckScore, Times.Once());
            mockTemplateData.Verify(x => x.HasGoodCrashTestRatings, Times.Once());
            mockTemplateData.Verify(x => x.HasGoodJdPowerRatings, Times.Once());
            mockTemplateData.Verify(x => x.HasGoodPriceBelowBook, Times.Once());
            mockTemplateData.Verify(x => x.HasGoodPriceBelowMSRP, Times.Once());
            mockTemplateData.Verify(x => x.HasHighlightedPackages, Times.Once());
            mockTemplateData.Verify(x => x.HasInteriorType, Times.Once());
            mockTemplateData.Verify(x => x.HasKeyInformation, Times.Once());
            mockTemplateData.Verify(x => x.HasLuxuryOptions, Times.Once());
            mockTemplateData.Verify(x => x.HasMarketingData, Times.Once());
            mockTemplateData.Verify(x => x.HasMarketingPreviewSuperlative, Times.Once());
            mockTemplateData.Verify(x => x.HasMarketingSuperlative, Times.Once());
            mockTemplateData.Verify(x => x.HasMissionPreviewStatment, Times.Once());
            mockTemplateData.Verify(x => x.HasMostSearchedEquipmentAvailable, Times.Once());
            mockTemplateData.Verify(x => x.HasRoadsideAssistance, Times.Once());
            mockTemplateData.Verify(x => x.HasSafetyOptions, Times.Once());
            mockTemplateData.Verify(x => x.HasSportyOptions, Times.Once());
            mockTemplateData.Verify(x => x.HasThemedEquipment, Times.Once());
            mockTemplateData.Verify(x => x.HasTier1EquipmentAvailable, Times.Once());
            mockTemplateData.Verify(x => x.HasTier2EquipmentAvailable, Times.Once());
            mockTemplateData.Verify(x => x.HasTrim, Times.Once());
            mockTemplateData.Verify(x => x.HasTruckOptions, Times.Once());
            mockTemplateData.Verify(x => x.HasValuedOptions, Times.Once());
            mockTemplateData.Verify(x => x.HighInternetPrice, Times.Once());
            mockTemplateData.Verify(x => x.HighlightedPackageSummaries, Times.Once());
            mockTemplateData.Verify(x => x.HighlightedPackages, Times.Once());
            mockTemplateData.Verify(x => x.Horsepower, Times.Once());
            mockTemplateData.Verify(x => x.InteriorColor, Times.Once());
            mockTemplateData.Verify(x => x.InteriorType, Times.Once());
            mockTemplateData.Verify(x => x.InventoryItem, Times.Once());
            mockTemplateData.Verify(x => x.IsAutoCheckAssured, Times.Once());
            mockTemplateData.Verify(x => x.IsAutoCheckOneOwner, Times.Once());
            mockTemplateData.Verify(x => x.IsCertified, Times.Once());
            mockTemplateData.Verify(x => x.IsCleanAutoCheckReport, Times.Once());
            mockTemplateData.Verify(x => x.IsCleanCarfaxReport, Times.Once());
            mockTemplateData.Verify(x => x.IsLowMilesPerYear, Times.Once());
            mockTemplateData.Verify(x => x.IsNewVehicle, Times.Once());
            mockTemplateData.Verify(x => x.IsOneOwner, Times.Once());
            mockTemplateData.Verify(x => x.IsPriceReduced, Times.Once());
            mockTemplateData.Verify(x => x.IsPriceReducedEnough, Times.Once());
            mockTemplateData.Verify(x => x.JdPowerRatingPreview, Times.Once());
            mockTemplateData.Verify(x => x.JdPowerRatings, Times.Once());
            mockTemplateData.Verify(x => x.KBBBookValue, Times.Once());
            mockTemplateData.Verify(x => x.ListPrice, Times.Once());
            mockTemplateData.Verify(x => x.Loaded, Times.Once());
            mockTemplateData.Verify(x => x.LuxuryOptions, Times.Once());
            mockTemplateData.Verify(x => x.MajorEquipmentOfInterest, Times.Once());
            mockTemplateData.Verify(x => x.Make, Times.Once());
            mockTemplateData.Verify(x => x.MarketingCollection, Times.Once());
            mockTemplateData.Verify(x => x.MarketingList, Times.Once());
            mockTemplateData.Verify(x => x.MarketingPreview, Times.Once());
            mockTemplateData.Verify(x => x.MarketingSentence, Times.Once());
            mockTemplateData.Verify(x => x.MarketingSummary, Times.Once());
            mockTemplateData.Verify(x => x.MarketingSuperlative, Times.Once());
            mockTemplateData.Verify(x => x.Mediator, Times.Once());
            mockTemplateData.Verify(x => x.Mileage, Times.Once());
            mockTemplateData.Verify(x => x.MilesPerYearCategory, Times.Once());
            mockTemplateData.Verify(x => x.MissionPreviewStatement, Times.Once());
            mockTemplateData.Verify(x => x.Model, Times.Once());
            mockTemplateData.Verify(x => x.ModelAwards, Times.Once());
            mockTemplateData.Verify(x => x.NADABookValue, Times.Once());
            mockTemplateData.Verify(x => x.NewPreviewPreferences, Times.Once());
            mockTemplateData.Verify(x => x.OriginalMsrp, Times.Once());
            mockTemplateData.Verify(x => x.Preferences, Times.Once());
            mockTemplateData.Verify(x => x.PreferredBook, Times.Once());
            mockTemplateData.Verify(x => x.PreferredBookValue, Times.Once());
            mockTemplateData.Verify(x => x.PreviewEquipment, Times.Once());
            mockTemplateData.Verify(x => x.PreviewHighlights, Times.Once());
            mockTemplateData.Verify(x => x.PreviewPrefs, Times.Once());
            mockTemplateData.Verify(x => x.PriceBelowBook, Times.Once());
            mockTemplateData.Verify(x => x.PriceBelowMSRP, Times.Once());
            mockTemplateData.Verify(x => x.ReconditioningText, Times.Once());
            mockTemplateData.Verify(x => x.ReconditioningValue, Times.Once());
            mockTemplateData.Verify(x => x.RoadsideAssistance, Times.Once());
            mockTemplateData.Verify(x => x.RolloverRating, Times.Once());
            mockTemplateData.Verify(x => x.RoughPriceBelowBook, Times.Once());
            mockTemplateData.Verify(x => x.RoughPriceBelowMSRP, Times.Once());
            mockTemplateData.Verify(x => x.SafetyOptions, Times.Once());
            mockTemplateData.Verify(x => x.SpecialFinancing, Times.Once());
            mockTemplateData.Verify(x => x.SportyOptions, Times.Once());
            mockTemplateData.Verify(x => x.StockNumber, Times.Once());
            mockTemplateData.Verify(x => x.TargetPrice, Times.Once());
            mockTemplateData.Verify(x => x.TargetPriceDisplay, Times.Once());
            mockTemplateData.Verify(x => x.ThemedEquipment, Times.Once());
            mockTemplateData.Verify(x => x.Tier1Equipment, Times.Once());
            mockTemplateData.Verify(x => x.Tier2Equipment, Times.Once());
            mockTemplateData.Verify(x => x.Tier3Equipment, Times.Once());
            mockTemplateData.Verify(x => x.Transmission, Times.Once());
            mockTemplateData.Verify(x => x.TransmissionOfInterest, Times.Once());
            mockTemplateData.Verify(x => x.Trim, Times.Once());
            mockTemplateData.Verify(x => x.TruckOptions, Times.Once());
            mockTemplateData.Verify(x => x.UsedPreviewPreferences, Times.Once());
            mockTemplateData.Verify(x => x.ValuedOptions, Times.Once());
            mockTemplateData.Verify(x => x.VehicleAttributes, Times.Once());
            mockTemplateData.Verify(x => x.VehicleConfig, Times.Once());
            mockTemplateData.Verify(x => x.Year, Times.Once());
            mockTemplateData.Verify(x => x.YearMakeModel, Times.Once());
        }

        #region Test Data

        private static IEnumerable<IInventoryData> FakeInventoryDatas
        {
            //Stock,BUID,VIN,UnitCost,Certified,CertifiedId,mileageReceived,TradeOrPurchase,ListPrice,ReceivedDate,vehicleYear,make,model,marketClass,segmentId,trim,afterMarketTrim,InventoryId,lowActivityFlag,ExteriorStatus,pricingStatus,InteriorStatus,photoStatus,adReviewStatus,postingStatus,BaseColor,ExteriorColor,ExtColor1,ExteriorColorCode,ExtColor2,InteriorColorCode,IntColor,ChromeStyleId,VehicleCatalogId,VehicleLocation,InventoryType,datePosted,StatusBucket,lastUpdateListPrice,desiredPhotoCountq,LotPrice,MSRP,DueForRepricing,SpecialPrice,HasCurrentBookVal,HasCarFax,autoloadStatusType,autoloadEndTime,NoPackages,bodyStyleName,engineDescription,engineOptionCode,isStandardEngine, null, null, null,
            get
            {
                return new List<IInventoryData>
                {
                    new InventoryData("JL12018", 102481, "2LMHJ5FR3CBL53869", (decimal) 36100.61, false, null, null, 2, 1, 45770, new DateTime(2012, 02, 23), "2012", "Lincoln", "MKT",
                        "2WD Sport Utility Vehicles", 6, null, null, 24020806, true, 2, 0, 2, 2, 2, 4, "White Platinum Tri-Coat", "UG", "White Platinum Tri-Coat", null, null, "TK",
                        "Light Stone", 333773, 716857, null, 1, new DateTime(2012, 11, 27, 14, 13, 0), 128, 43330, 12, 45770, 45770, true, 43330, false, false, null, null, false),
                    new InventoryData("JL13016", 102481, "2LMHJ5AT0DBL55003", (decimal) 50950.64, false, null, null, 2, 1, 55530, new DateTime(2012, 11, 13), "2013", "Lincoln", "MKT",
                        "4WD Sport Utility Vehicles", 6, "EcoBoost", null, 27896798, false, 2, 0, 2, 2, 2, 4, "Smoked Quartz Tinted Metallic Clearcoat", "TQ",
                        "Smoked Quartz Tinted Metallic Clearcoat", null, null, "6W", "Charcoal Black", 348568, 796280, null, 1, new DateTime(2013, 01, 29, 09, 47, 0), 128, 55530,
                        12, 55530, 55530, true, null, false, false, null, null, true),
                    new InventoryData("stock-11", 1, "vin-1", 1, false, null, null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2013", "Dodge", "Ram", "MarketClass-1", 1, null, null,
                        1, false, 1, 1, 1, 1, 1, 1, "baseColor-1", "extCode-1", "extDesc-1", "ext2Code-1", "ext2Desc-1", "intCode-1", "intDesc-1", 1, null, "vehicleLocation-1", 1,
                        DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false),
                    new InventoryData("stock-21", 1, "vin-2", 1, false, null, null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2012", "Ford", "Model-2", "MarketClass-2", 1, "Trim-2",
                        "afterMarketTrim-2", 2, false, 1, 1, 1, 1, 1, 1, "baseColor-2", "extCode-2", "extDesc-2", "ext2Code-2", "ext2Desc-2", "intCode-2", "intDesc-2", 1, null,
                        "vehicleLocation-2", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false),
                    new InventoryData("stock-21", 1, "vin-2", 1, false, null, null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2012", "Mercury", "Model-2", "MarketClass-2", 1,
                        "Trim-2", "afterMarketTrim-2", 3, false, 1, 1, 1, 1, 1, 1, "baseColor-2", "extCode-2", "extDesc-2", "ext2Code-2", "ext2Desc-2", "intCode-2", "intDesc-2", 1,
                        null, "vehicleLocation-2", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false),
                    new InventoryData("stock-21", 1, "vin-2", 1, false, null, null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2012", "Chevrolet", "Model-2", "MarketClass-2", 1,
                        "Trim-2", "afterMarketTrim-2", 4, false, 1, 1, 1, 1, 1, 1, "baseColor-2", "extCode-2", "extDesc-2", "ext2Code-2", "ext2Desc-2", "intCode-2", "intDesc-2", 1,
                        null, "vehicleLocation-2", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false),
                };
            }
        }
        
        #endregion Test Data
    }
}
