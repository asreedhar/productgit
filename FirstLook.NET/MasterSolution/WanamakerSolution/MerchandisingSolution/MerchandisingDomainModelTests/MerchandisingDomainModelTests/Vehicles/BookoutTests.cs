﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Merchandising.DomainModel.Vehicles;
using NUnit.Framework;
using Moq;


namespace MerchandisingDomainModelTests.Vehicles
{
    [TestFixture]
    public class BookoutTests
    {
        // just some simple tests to check the two new bookout values are working correctly

        [Test]
        public void testBookoutCollectionGetValue()
        {
            BookoutCollection bookoutCollection = new BookoutCollection();

            bookoutCollection.Add(new Bookout(BookoutValueType.FinalValue, 25000, true, true, BookoutProvider.KBB, BookoutCategory.KbbTradeInRangeLow));
            bookoutCollection.Add(new Bookout(BookoutValueType.FinalValue, 26500, true, true, BookoutProvider.KBB, BookoutCategory.KbbTradeInRangeHigh));


            bookoutCollection.Add(new Bookout(BookoutValueType.FinalValue, 28000, true, true, BookoutProvider.BlackBook, BookoutCategory.KbbWholesale));
            bookoutCollection.Add(new Bookout(BookoutValueType.FinalValue, 24500, true, true, BookoutProvider.Galves, BookoutCategory.GalvesTradeIn));
            
            Assert.AreEqual(bookoutCollection.GetValue(BookoutCategory.KbbTradeInRangeLow, true, true), 25000);
            Assert.AreEqual(bookoutCollection.GetValue(BookoutCategory.KbbTradeInRangeHigh, true, true), 26500);
            Assert.AreEqual(bookoutCollection.GetValue(BookoutCategory.GalvesTradeIn, true, true), 24500);
        }

        [Test]
        public void testBookoutDescriptionTest()
        {
            Assert.AreEqual(Bookout.GetBookoutCategoryInternalDescription(BookoutCategory.KbbTradeInRangeLow), "KBB Trade-In + RangeLow", "Consumer name for bookout type has changed");
            Assert.AreEqual(Bookout.GetBookoutCategoryInternalDescription(BookoutCategory.KbbTradeInRangeHigh), "KBB Trade-In + RangeHigh", "Consumer name for bookout type has changed");
        }

        [Test]
        public void testBookoutProvider()
        {

            Assert.AreEqual(Bookout.GetProviderFromMap(BookoutCategory.KbbTradeInRangeLow), BookoutProvider.KBB, "The bookout provider for this bookout category is not correct");
            Assert.AreEqual(Bookout.GetProviderFromMap(BookoutCategory.KbbTradeInRangeHigh), BookoutProvider.KBB, "The bookout provider for this bookout category is not correct");
        }

        [Test] 
        public void testBookoutProviderDescription()
        {

            Assert.AreEqual(Bookout.GetBookoutCategoryConsumerDescription(BookoutCategory.KbbTradeInRangeLow), "Kelley Blue Book", "The bookout provider description for this bookout category is not correct");
            Assert.AreEqual(Bookout.GetBookoutCategoryConsumerDescription(BookoutCategory.KbbTradeInRangeHigh), "Kelley Blue Book", "The bookout provider description for this bookout category is not correct");

        }

    }
}
