﻿using System.Runtime.Caching;
using FirstLook.Common.Core;
using FirstLook.Merchandising.DomainModel.VehicleHistory;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.VehicleHistory
{
    [TestFixture]
    public class CachedCarfaxServicesTests
    {
        private ICache _cache;
        private Mock<ICarfaxServices> _innerMock;
        private ICarfaxServices _inner;

        [SetUp]
        public void Setup()
        {
            _cache = new FirstLook.Common.Core.Cache.DotnetMemoryCacheWrapper(new MemoryCache("Test"));
            _innerMock = new Mock<ICarfaxServices>();
            _inner = _innerMock.Object;
        }

        [TestCase(5000, Result = true)]
        [TestCase(5020, Result = false)]
        public bool HasAccount_should_call_inner_HasAccount(int businessUnitToCheck)
        {
            _innerMock.Setup(i => i.HasAccount(5000)).Returns(true);

            var services = new CachedCarfaxServices(_cache, _inner);

            var result = services.HasAccount(businessUnitToCheck);

            _innerMock.Verify(i => i.HasAccount(businessUnitToCheck), Times.Once());

            return result;
        }

        [TestCase(5000, Result = true)]
        [TestCase(5020, Result = false)]
        public bool HasAccount_called_twice_should_call_inner_HasAccount_once(int businessUnitToCheck)
        {
            _innerMock.Setup(i => i.HasAccount(5000)).Returns(true);

            var services = new CachedCarfaxServices(_cache, _inner);

            var result = services.HasAccount(businessUnitToCheck);
            var result2 = services.HasAccount(businessUnitToCheck);

            _innerMock.Verify(i => i.HasAccount(businessUnitToCheck), Times.Once());
            Assert.That(result, Is.EqualTo(result2));

            return result;
        }

        [Test]
        public void HasAccount_called_multiple_times_with_different_dealers_should_cache_results_separately()
        {
            _innerMock.Setup(i => i.HasAccount(5000)).Returns(true);
            _innerMock.Setup(i => i.HasAccount(5005)).Returns(true);
            // Everything else is false

            var services = new CachedCarfaxServices(_cache, _inner);

            var result1 = services.HasAccount(5000);
            var result2 = services.HasAccount(5005);
            var result3 = services.HasAccount(5000);
            var result4 = services.HasAccount(5020);
            var result5 = services.HasAccount(5020);

            Assert.That(result1, Is.True);
            Assert.That(result2, Is.True);
            Assert.That(result3, Is.True);
            Assert.That(result4, Is.False);
            Assert.That(result5, Is.False);

            _innerMock.Verify(i => i.HasAccount(5000), Times.Once());
            _innerMock.Verify(i => i.HasAccount(5005), Times.Once());
            _innerMock.Verify(i => i.HasAccount(5020), Times.Once());
        }
    }
}