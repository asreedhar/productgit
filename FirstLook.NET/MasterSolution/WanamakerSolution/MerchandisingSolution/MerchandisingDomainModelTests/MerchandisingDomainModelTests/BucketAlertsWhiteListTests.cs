﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Merchandising.DomainModel.Release;
using NUnit.Framework;

namespace MerchandisingDomainModelTests
{
    [TestFixture]
    public class BucketAlertsWhiteListTests
    {
        [Test]
        public void TestCanSend()
        {
            const string whiteList = "jsu@incisent.com;cnorton@incisent.com;tmuir@incisent.com;dbokor@incisent.com";
            var emailList = new[] {"Laura.Wild@hendrickauto.com","cnorton@incisent.com", "dbokor@incisent.com"};

            Assert.IsFalse(EmailJobBase.CanSend(whiteList, emailList));

            emailList = new[] { "cnorton@incisent.com", "dbokor@incisent.com" };
            Assert.IsTrue(EmailJobBase.CanSend(whiteList, emailList));

            var stringWhiteList = "cnorton@incisent.com;dbokor@incisent.com";
            Assert.IsTrue(EmailJobBase.CanSend(stringWhiteList, emailList));

            stringWhiteList = "dbokor@incisent.com";
            Assert.IsFalse(EmailJobBase.CanSend(stringWhiteList, emailList));
        }
    }
}
