﻿using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using MAX.Entities;
using Merchandising.Messages.AutoApprove;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Pricing.DiscountCampaigns
{
    [TestFixture]
    public class DiscountPricingCampaignManagerTest
    {
        private Mock<IDiscountPricingRepository> _pricingRepository;
        private Mock<IAdMessageSender> _messageSender;
        private Mock<IInventorySearch> _inventorySearch;
        private IDiscountPricingCampaignManager _manager;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            var mockPricingRepo = new Mock<IDiscountPricingRepository>();
            var mockMessageSender = new Mock<IAdMessageSender>();
            var mockInventorySearch = new Mock<IInventorySearch>();
            
            mockPricingRepo
                .Setup(x => x.FetchBusinessUnitPricingModels(It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<int?>())).Returns((int buid, bool includeInventory, int? campaignId) =>
                    TestDiscountCampaignModels(buid, campaignId)
                );
                
            mockPricingRepo.Setup(x => x.FetchCampaignsByInventory(It.IsInRange(1, 2, Range.Inclusive), It.IsAny<bool>()))
                .Returns((int inventory, bool active) => TestDiscountCampaignModels(1, inventory));

            mockPricingRepo.Setup(x => x.SaveCampaign(It.IsAny<DiscountPricingCampaignModel>()))
                .Returns(It.IsAny<DiscountPricingCampaignModel>)
                .Callback((DiscountPricingCampaignModel model) => Assert.That(model.HasBeenExpired.Equals(false)));

            mockInventorySearch.Setup(x => x.GetInventoryData(It.IsAny<int>())).Returns(FakeInventoryDatas());

            _pricingRepository = mockPricingRepo;
            _messageSender = mockMessageSender;
            _inventorySearch = mockInventorySearch;

        }

        
        
        [SetUp]
        public void TestSetup()
        {
            var managerMock = new Mock<DiscountPricingCampaignManager>(_pricingRepository.Object, _messageSender.Object, _inventorySearch.Object);
            managerMock.Setup(x => x.SetSpecialPrice(It.IsAny<int>(), It.IsAny<decimal>(), "testUser"));
            managerMock.Setup(x => x.RemoveSpecialPrice(It.IsAny<int>(), It.IsAny<decimal>()));
            managerMock.CallBase = true;

            _manager = managerMock.Object;
        }
    
        [Test]
        public void FetchCampaign_ReturnsCampaign_ShouldPass()
        {
            var campaign = _manager.FetchCampaign(1, 1);
            Assert.IsNotNull(campaign);
            _pricingRepository.Verify(x => x.FetchBusinessUnitPricingModels(It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<int?>()), Times.Once());
            Assert.IsTrue(campaign.CampaignId == 1);
            campaign.InventoryList.ForEach(inventory => Assert.That(inventory.CampaignId == 1));
            Assert.IsTrue(campaign.InventoryList.Count == 2);
        }

        [Test]
        public void FetchCampaign_ReturnsNull_ShouldPass()
        {
            var campaign = _manager.FetchCampaign(1, 5);
            Assert.IsNull(campaign);
        }

        /// <summary>
        /// Using the sample data, businessunit 1 has two campaigns.
        /// InventoryId 1 is in both campaigns.
        /// FetchCampaignForInventory should get the last campaign for this inventory
        /// </summary>
        [Test]
        public void FetchCampaignForInventory_ReturnsCampaign_ShouldPass()
        {
            var campaign = _manager.FetchCampaignForInventory(1, 1);
            
            Assert.IsNotNull(campaign);
            Assert.IsTrue(campaign.InventoryList.Count == 2);
            Assert.IsTrue(campaign.CampaignId == 2);
            campaign.InventoryList.ForEach(inventory => Assert.That(inventory.CampaignId == 2));

            _pricingRepository.Verify(x => x.FetchCampaignsByInventory(It.IsAny<int>(), It.IsAny<bool>()), Times.Once());
        }

        [Test]
        public void Method_ExpireDiscountCampaign_ShouldPass()
        {
            var campaign = _manager.FetchCampaign(1, 1);

            _pricingRepository.Setup(x => x.DeactivateInventoryForDiscountCampaign(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), null)).Returns(
                campaign.InventoryList);

            _manager.ExpireDiscountCampaign(campaign, "testFixture");

            _pricingRepository.Verify(x => x.DeactivateInventoryForDiscountCampaign(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), null), Times.Exactly(2));
            _pricingRepository.Verify(x=> x.ExpireDiscountCampaign(It.IsAny<DiscountPricingCampaignModel>()), Times.Once());

            _messageSender.Verify(x => x.SendBatchPriceChange(It.IsAny<BatchPriceChangeMessage>(), It.IsAny<string>()), Times.Once());
        }


        #region Test Data

        private static List<DiscountPricingCampaignModel> TestDiscountCampaignModels(int buid, int? campaignId)
        {
            var campaignList = new List<DiscountPricingCampaignModel>();

            if (buid == 1 && (campaignId == 1 || campaignId == 2))
            {
                return new List<DiscountPricingCampaignModel>
                {
                    new DiscountPricingCampaignModel
                    {
                        BusinessUnitId = 1,
                        CampaignFilter = GetTestCampaignFilter(1),
                        CampaignId = 1,
                        CreatedBy = "testFixture",
                        CreationDate = DateTime.Today.AddDays(1),
                        DealerDiscount = 1000,
                        ManufacturerRebate = 2000,
                        DiscountType = PriceBaseLine.Invoice,
                        ExpirationDate = DateTime.Today.AddMonths(1),
                        HasBeenExpired = false,
                        InventoryList = GetCampaignInventoryList(1)
                    },
                    new DiscountPricingCampaignModel
                    {
                        BusinessUnitId = 1,
                        CampaignFilter = GetTestCampaignFilter(2),
                        CampaignId = 2,
                        CreatedBy = "testFixture",
                        CreationDate = DateTime.Today.AddDays(1),
                        DealerDiscount = 1000,
                        ManufacturerRebate = 2000,
                        DiscountType = PriceBaseLine.Msrp,
                        ExpirationDate = DateTime.Today.AddMonths(1),
                        HasBeenExpired = false,
                        InventoryList = GetCampaignInventoryList(2)
                    }
                };
            }

            return campaignList;
        }

        private static CampaignFilter GetTestCampaignFilter(int incrementer)
        {
            return new CampaignFilter
            {
                VehicleType = VehicleType.New,
                Make = new List<string> { "Make-" + incrementer },
                Model = new List<string> { "Model-" + incrementer },
                Trim = new List<string> { "Trim-" + incrementer },
                Year = new List<string> { "2013, 2014" }
            };
        }

        private static List<InventoryDiscountPricingModel> GetCampaignInventoryList(int i)
        {
            return new List<InventoryDiscountPricingModel>
            {
                new InventoryDiscountPricingModel
                {
                    Active = true,
                    ActivatedBy = "testFixture",
                    CampaignId = i,
                    InventoryId = 1
                },
                new InventoryDiscountPricingModel
                {
                    Active = true,
                    ActivatedBy = "testFixture",
                    CampaignId = i,
                    InventoryId = 2,
                    OriginalNewCarPrice = 25000
                }
            };
        }




        private static IEnumerable<IInventoryData> FakeInventoryDatas()
        {
            return new List<IInventoryData>
            {
                new InventoryData("stock-11", 1, "vin-1", 1, false, "", null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2013", "Make-1", "Model-1", "MarketClass-1", 1, "Trim-1", "afterMarketTrim-1", 1, false, 1, 1, 1, 1, 1, 1, "baseColor-1", "extCode-1", "extDesc-1", "ext2Code-1", "ext2Desc-1", "intCode-1", "intDesc-1", 1, null, "vehicleLocation-1", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false),
                new InventoryData("stock-21", 1, "vin-2", 1, false, "", null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2012", "Make-2", "Model-2", "MarketClass-2", 1, "Trim-2", "afterMarketTrim-2", 1, false, 1, 1, 1, 1, 1, 1, "baseColor-2", "extCode-2", "extDesc-2", "ext2Code-2", "ext2Desc-2", "intCode-2", "intDesc-2", 1, null, "vehicleLocation-2", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false),
                new InventoryData("stock-31", 1, "vin-3", 1, false, "", null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2011", "Make-3", "Model-3", "MarketClass-3", 1, "Trim-3", "afterMarketTrim-3", 1, false, 1, 1, 1, 1, 1, 1, "baseColor-3", "extCode-3", "extDesc-3", "ext2Code-3", "ext2Desc-3", "intCode-3", "intDesc-3", 1, null, "vehicleLocation-3", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false),
                new InventoryData("stock-12", 1, "vin-4", 1, false, "", null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2010", "Make-1", "Model-2", "MarketClass-1", 1, "Trim-4", "afterMarketTrim-1", 1, false, 1, 1, 1, 1, 1, 1, "baseColor-1", "extCode-1", "extDesc-1", "ext2Code-1", "ext2Desc-1", "intCode-1", "intDesc-1", 1, null, "vehicleLocation-1", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false),
                new InventoryData("stock-22", 1, "vin-5", 1, false, "", null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2009", "Make-2", "Model-2", "MarketClass-2", 1, "Trim-5", "afterMarketTrim-2", 1, false, 1, 1, 1, 1, 1, 1, "baseColor-2", "extCode-2", "extDesc-2", "ext2Code-2", "ext2Desc-2", "intCode-2", "intDesc-2", 1, null, "vehicleLocation-2", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false),
                new InventoryData("stock-32", 1, "vin-6", 1, false, "", null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2008", "Make-3", "Model-3", "MarketClass-3", 1, "Trim-6", "afterMarketTrim-3", 1, false, 1, 1, 1, 1, 1, 1, "baseColor-3", "extCode-3", "extDesc-3", "ext2Code-3", "ext2Desc-3", "intCode-3", "intDesc-3", 1, null, "vehicleLocation-3", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false)
            };
        }

        #endregion Test Data

    }
}