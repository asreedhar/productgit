﻿
using FirstLook.Merchandising.DomainModel.Vehicles;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.CPO
{

	[TestFixture]
	public class CpoTests
	{
		[TestCase("", "General Motors", true)]
		[TestCase("12345", "General Motors", false)]
		[TestCase("123456", "General Motors", true)]
		[TestCase("1234567", "General Motors", true)]
		[TestCase("12345678", "General Motors", true)]
		[TestCase("123456789", "General Motors", false)]
		public void CertifiedIdValidationTests(string certifiedId, string manufacturerName, bool expectedResult)
		{
			Assert.AreEqual(expectedResult, VehicleCertification.ValidateCertifiedId(certifiedId, manufacturerName));
		}
	}
}
