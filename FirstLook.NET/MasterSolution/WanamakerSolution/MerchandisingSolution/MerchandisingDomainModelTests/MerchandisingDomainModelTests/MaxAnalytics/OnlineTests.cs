﻿using System;
using Autofac;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Extensions;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.Membership;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.Dashboard.Concrete;
using FirstLook.Merchandising.DomainModel.MaxAnalytics;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.Activity.InventoryCount;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel;
using MAX.Entities;
using MAX.Entities.Enumerations;
using Moq;
using NUnit.Framework;
using ICache = FirstLook.Common.Core.ICache;

namespace MerchandisingDomainModelTests.MaxAnalytics
{
    [TestFixture]
    public class OnlineTests
    {
        private static Mock<IDataOnlineRepository> onlineRepository = new Mock<IDataOnlineRepository>();
        private static Mock<IMemberContext> memberContext = new Mock<IMemberContext>();
        private static Mock<IDashboardBusinessUnitRepository> dealerServices = new Mock<IDashboardBusinessUnitRepository>();

        private const string USED_VIN = "12345678901234567";
        private const string NEW_VIN = "abcdefghijklmnopq";

        [TestFixtureSetUp]
        public void Setup()
        {
            var builder = new ContainerBuilder();
            builder.Register(c => new NullCache()).As<ICache>().SingleInstance();


            var container = builder.Build();
            Registry.RegisterContainer( container );
        }


        private static IVehiclesOnline[] Test(MaxDataOnline online)
        {
            int businessUnitId = 1;
            string businessunitcode = "HENDRICK02";

            dealerServices.Setup(x => x.GetBusinessUnit(businessUnitId)).Returns(new DashboardBusinessUnit()
                                                                                     {
                                                                                         BusinessUnitId = businessUnitId,
                                                                                         Code = businessunitcode
                                                                                     });

            onlineRepository.Setup(x => x.GetOnlineVehicles(businessunitcode)).Returns(online);

            onlineRepository.Setup(x => x.GetOnlineStaleDays()).Returns(12);
            onlineRepository.Setup(x => x.GetVehicleInfo(businessUnitId, VehicleType.Used, 14)).Returns( new IVehiclesOnline[] { 
                new VehiclesOnline { Vin = "a", InventoryType = 2 }, // used
                new VehiclesOnline { Vin = "b", InventoryType = 1 } // new
            });

	        var inventoryCountWs = new Mock<IInventoryCountWebService>();

			var analyticsWrapper = new AnalyticsWrapper(memberContext.Object, dealerServices.Object, onlineRepository.Object, inventoryCountWs.Object);

            return analyticsWrapper.GetOnlineVehicles(businessUnitId, 2, 14);
        }

        private static AnalyticsWrapper SetupMergeWithMaxDataTest(int businessUnitId, string businessUnitCode, 
            MaxDataOnline online, bool useMaxAnalytics = true)
        {

            dealerServices.Setup(x => x.GetBusinessUnit(businessUnitId)).Returns(new DashboardBusinessUnit()
            {
                BusinessUnitId = businessUnitId,
                Code = businessUnitCode
            });

            onlineRepository.Setup(x => x.GetOnlineVehicles(businessUnitCode)).Returns(online);

            onlineRepository.Setup(x => x.GetOnlineStaleDays()).Returns(12);

            // used
            onlineRepository.Setup(x => x.GetVehicleInfo(businessUnitId, VehicleType.Used, 14)).Returns(new IVehiclesOnline[] { 
                new VehiclesOnline { Vin = USED_VIN, InventoryType = 2 }
            });

            // new
            onlineRepository.Setup(x => x.GetVehicleInfo(businessUnitId, VehicleType.New, 14)).Returns(new IVehiclesOnline[] { 
                new VehiclesOnline { Vin = NEW_VIN, InventoryType = 1 } // new
            });

            // both
            onlineRepository.Setup(x => x.GetVehicleInfo(businessUnitId, VehicleType.Both, 14)).Returns(new IVehiclesOnline[] { 
                new VehiclesOnline { Vin = USED_VIN, InventoryType = 2 }, // used
                new VehiclesOnline { Vin = NEW_VIN, InventoryType = 1 } // new
            });

            var inventoryCountWs = new Mock<IInventoryCountWebService>();

            var analyticsWrapper = new AnalyticsWrapper(memberContext.Object, dealerServices.Object, onlineRepository.Object, inventoryCountWs.Object);

            return analyticsWrapper;
        }


        [Test]
        public void GetGidProvider()
        {
            Mock<IVehiclesOnline> vehicleMock = new Mock<IVehiclesOnline>();
            vehicleMock.SetupAllProperties();
            var vehicle = vehicleMock.Object;
            vehicle.InventoryType = 1; // new

            var newGidProvider = AnalyticsWrapper.GetGidProvider(vehicle, GidProvider.Aultec, GidProvider.eBiz);
            Assert.AreEqual(GidProvider.Aultec, newGidProvider);

            vehicle.InventoryType = 2;
            var usedGidProvider = AnalyticsWrapper.GetGidProvider(vehicle, GidProvider.Aultec, GidProvider.eBiz);
            Assert.AreEqual(GidProvider.eBiz, usedGidProvider);

            vehicle.InventoryType = 2;
            var noneGidProvider = AnalyticsWrapper.GetGidProvider(vehicle, GidProvider.None, GidProvider.None);
            Assert.AreEqual(GidProvider.None, noneGidProvider);
        }

        [Test][Ignore]
        public void TestNoData()
        {
            string businessunitcode = "HENDRICK02";

            var maxOnline = new MaxDataOnline();
            maxOnline.Stores = new[] { businessunitcode };
            maxOnline.SiteVins = new SiteVins[]{};

            var results = Test(maxOnline);

            Assert.IsTrue(results.Length == 2);

            foreach (var vehicle in results)
            {
                Assert.IsFalse(vehicle.Online);
                Assert.IsFalse(vehicle.AtOnline);
                Assert.IsFalse(vehicle.CarsOnline);
                Assert.IsFalse(vehicle.HasAtData);
                Assert.IsFalse(vehicle.HasCarsData);
            }
        }

        [Test]
        public void MergeWithMaxData_NoGid_Results_In_HasData_False()
        {
            int businessUnitId = 1;
            string businessUnitCode = "HENDRICK02";
            var maxOnline = new MaxDataOnline();
            maxOnline.Stores = new[] { businessUnitCode };
            maxOnline.SiteVins = new SiteVins[]{}; 
            var analyticsWrapper = SetupMergeWithMaxDataTest(businessUnitId, businessUnitCode, maxOnline, true);

            var onlineRepositoryObj = onlineRepository.Object;
            var inventory = onlineRepositoryObj.GetVehicleInfo(businessUnitId, VehicleType.Both, 14);

            analyticsWrapper.MergeWithMaxData(businessUnitId, inventory, maxOnline, GidProvider.None, GidProvider.None);

            foreach (var i in inventory)
            {
                Assert.IsFalse(i.HasData);                    
                Assert.IsFalse(i.Online);
            }                        
        }

        [Test]
        public void MergeWithMaxData_GidSpecified_Results_In_HasData_True()
        {
            int businessUnitId = 1;
            string businessUnitCode = "HENDRICK02";
            var maxOnline = new MaxDataOnline();
            maxOnline.Stores = new[] { businessUnitCode };
            maxOnline.SiteVins = new SiteVins[] {};

            var analyticsWrapper = SetupMergeWithMaxDataTest(businessUnitId, businessUnitCode, maxOnline, true);

            var onlineRepositoryObj = onlineRepository.Object;
            var inventory = onlineRepositoryObj.GetVehicleInfo(businessUnitId, VehicleType.Both, 14);

            analyticsWrapper.MergeWithMaxData(businessUnitId, inventory, maxOnline, GidProvider.None, GidProvider.eBiz, ReportDataSource.None, ReportDataSource.eBiz);

            foreach (var i in inventory)
            {
                if (i.InventoryType == (int) VehicleType.New)
                {
                    Assert.IsFalse(i.HasData);
                    Assert.IsFalse(i.Online);
                }
                else
                {
                    Assert.IsTrue(i.HasData);
                    Assert.IsFalse(i.Online);
                }
            }          
        }


        [Test]
        public void MergeWithMaxData_DataFoundFromGid_Used_Results_In_Online_True()
        {
            int businessUnitId = 1;
            string businessUnitCode = "HENDRICK02";
            var maxOnline = new MaxDataOnline();
            maxOnline.Stores = new[] { businessUnitCode };
            var siteVins = new[]
            {
                new SiteVins
                {
                    ObservationTime = DateTime.Now.AddHours(-1),
                    Provider = Provider.eBiz,
                    Site = Site.eBiz,
                    Vins = new[] {USED_VIN}
                }
            };
            maxOnline.SiteVins = siteVins;

            var analyticsWrapper = SetupMergeWithMaxDataTest(businessUnitId, businessUnitCode, maxOnline, true);
            var onlineRepositoryObj = onlineRepository.Object;
            var inventory = onlineRepositoryObj.GetVehicleInfo(businessUnitId, VehicleType.Both, 14);

            analyticsWrapper.MergeWithMaxData(businessUnitId, inventory, maxOnline, GidProvider.None, GidProvider.eBiz, ReportDataSource.None, ReportDataSource.eBiz);

            foreach (var i in inventory)
            {
                if (i.Vin == USED_VIN)
                {
                    Assert.IsTrue(i.Online);
                }
                else
                {
                    Assert.IsFalse(i.Online);                    
                }
            }          
        }

        [Test]
        public void MergeWithMaxData_DataFoundFromGid_New_Results_In_Online_True()
        {
            int businessUnitId = 1;
            string businessUnitCode = "HENDRICK02";
            var maxOnline = new MaxDataOnline();
            maxOnline.Stores = new[] { businessUnitCode };
            var siteVins = new[]
            {
                new SiteVins
                {
                    ObservationTime = DateTime.Now.AddHours(-1),
                    Provider = Provider.eBiz,
                    Site = Site.eBiz,
                    Vins = new[] {NEW_VIN}
                }
            };
            maxOnline.SiteVins = siteVins;

            var analyticsWrapper = SetupMergeWithMaxDataTest(businessUnitId, businessUnitCode, maxOnline, true);
            var onlineRepositoryObj = onlineRepository.Object;
            var inventory = onlineRepositoryObj.GetVehicleInfo(businessUnitId, VehicleType.Both, 14);

            analyticsWrapper.MergeWithMaxData(businessUnitId, inventory, maxOnline, GidProvider.eBiz, GidProvider.None, ReportDataSource.eBiz, ReportDataSource.None);

            foreach (var i in inventory)
            {
                if (i.Vin == NEW_VIN)
                {
                    Assert.IsTrue(i.Online);
                }
                else
                {
                    Assert.IsFalse(i.Online);
                }
            }
        }

        [Test]
        public void MergeWithMaxData_DataNotFoundFromGid_Used_Results_In_Online_False()
        {
            int businessUnitId = 1;
            string businessUnitCode = "HENDRICK02";
            var maxOnline = new MaxDataOnline();
            maxOnline.Stores = new[] { businessUnitCode };
            var siteVins = new[]
            {
                // the vin was found from another GID vendor - not ours, so the car is offline
                new SiteVins
                {
                    ObservationTime = DateTime.Now.AddHours(-1),
                    Provider = Provider.Fetch,
                    Site = Site.AutoTrader,
                    Vins = new[] {NEW_VIN}
                }
            };
            maxOnline.SiteVins = siteVins;

            var analyticsWrapper = SetupMergeWithMaxDataTest(businessUnitId, businessUnitCode, maxOnline, true);
            var onlineRepositoryObj = onlineRepository.Object;
            var inventory = onlineRepositoryObj.GetVehicleInfo(businessUnitId, VehicleType.Both, 14);

            analyticsWrapper.MergeWithMaxData(businessUnitId, inventory, maxOnline, GidProvider.eBiz, GidProvider.None);

            foreach (var i in inventory)
            {
                Assert.IsFalse(i.Online);
            }
        }

        [Test]
        public void MergeWithMaxData_DataNotFoundFromGid_New_Results_In_Online_False()
        {
            int businessUnitId = 1;
            string businessUnitCode = "HENDRICK02";
            var maxOnline = new MaxDataOnline();
            maxOnline.Stores = new[] { businessUnitCode };
            var siteVins = new[]
            {
                // the used & new vins were found from another provider - but not our GID vendor
                new SiteVins
                {
                    ObservationTime = DateTime.Now.AddHours(-1),
                    Provider = Provider.Aultec,
                    Site = Site.AutoTrader,
                    Vins = new[] {USED_VIN}
                },
                new SiteVins
                {
                    ObservationTime = DateTime.Now.AddHours(-1),
                    Provider = Provider.eBiz,
                    Site = Site.eBiz,
                    Vins = new[] {NEW_VIN}
                }
            };
            maxOnline.SiteVins = siteVins;

            var analyticsWrapper = SetupMergeWithMaxDataTest(businessUnitId, businessUnitCode, maxOnline, true);
            var onlineRepositoryObj = onlineRepository.Object;
            var inventory = onlineRepositoryObj.GetVehicleInfo(businessUnitId, VehicleType.Both, 14);

            // results are found, but they are from the wrong GID provider
            analyticsWrapper.MergeWithMaxData(businessUnitId, inventory, maxOnline, GidProvider.Aultec, GidProvider.eBiz);

            foreach (var i in inventory)
            {
                Assert.IsFalse(i.Online);
            }
        }

        private static SiteVins[] SetupAllSiteVinsAllSitesAllProviders()
        {
            var siteVins = new[]
            {
                new SiteVins
                {
                    ObservationTime = DateTime.Now.AddHours(-1),
                    Provider = Provider.Fetch,
                    Site = Site.Cars,
                    Vins = new[] {USED_VIN}
                },
                new SiteVins
                {
                    ObservationTime = DateTime.Now.AddHours(-1),
                    Provider = Provider.Fetch,
                    Site = Site.AutoTrader,
                    Vins = new[] {USED_VIN}
                },
                new SiteVins
                {
                    ObservationTime = DateTime.Now.AddHours(-1),
                    Provider = Provider.Aultec,
                    Site = Site.Cars,
                    Vins = new[] {USED_VIN}
                },
                new SiteVins
                {
                    ObservationTime = DateTime.Now.AddHours(-1),
                    Provider = Provider.Aultec,
                    Site = Site.AutoTrader,
                    Vins = new[] {USED_VIN}
                },
                new SiteVins
                {
                    ObservationTime = DateTime.Now.AddHours(-1),
                    Provider = Provider.eBiz,
                    Site = Site.eBiz,
                    Vins = new[] {USED_VIN}
                },
                new SiteVins
                {
                    ObservationTime = DateTime.Now.AddHours(-1),
                    Provider = Provider.AutoTrader,
                    Site = Site.AutoTrader,
                    Vins = new[] {USED_VIN}
                },
                new SiteVins
                {
                    ObservationTime = DateTime.Now.AddHours(-1),
                    Provider = Provider.Cars,
                    Site = Site.Cars,
                    Vins = new[] {USED_VIN}
                },
                new SiteVins
                {
                    ObservationTime = DateTime.Now.AddHours(-1),
                    Provider = Provider.Fetch,
                    Site = Site.Cars,
                    Vins = new[] {NEW_VIN}
                },
                new SiteVins
                {
                    ObservationTime = DateTime.Now.AddHours(-1),
                    Provider = Provider.Fetch,
                    Site = Site.AutoTrader,
                    Vins = new[] {NEW_VIN}
                },
                new SiteVins
                {
                    ObservationTime = DateTime.Now.AddHours(-1),
                    Provider = Provider.Aultec,
                    Site = Site.Cars,
                    Vins = new[] {NEW_VIN}
                },
                new SiteVins
                {
                    ObservationTime = DateTime.Now.AddHours(-1),
                    Provider = Provider.Aultec,
                    Site = Site.AutoTrader,
                    Vins = new[] {NEW_VIN}
                },
                new SiteVins
                {
                    ObservationTime = DateTime.Now.AddHours(-1),
                    Provider = Provider.eBiz,
                    Site = Site.eBiz,
                    Vins = new[] {NEW_VIN}
                },
                new SiteVins
                {
                    ObservationTime = DateTime.Now.AddHours(-1),
                    Provider = Provider.AutoTrader,
                    Site = Site.AutoTrader,
                    Vins = new[] {NEW_VIN}
                },
                new SiteVins
                {
                    ObservationTime = DateTime.Now.AddHours(-1),
                    Provider = Provider.Cars,
                    Site = Site.Cars,
                    Vins = new[] {NEW_VIN}
                }
            };

            return siteVins;
        }

        [Test][Ignore]
        public void TestVehicleOffLineCars()
        {
            string businessunitcode = "HENDRICK02";

            var maxOnline = new MaxDataOnline();
            maxOnline.Stores = new[] { businessunitcode };
            maxOnline.SiteVins = new[] {
                new SiteVins(){ObservationTime = DateTime.Now, Provider = Provider.Fetch, Site = Site.Cars, Vins = new []{"c"}}
            };

            var results = Test(maxOnline);            
            Assert.IsTrue(results.Length == 2);

            foreach (var vehicle in results)
            {
                Assert.IsFalse(vehicle.Online);
                Assert.IsFalse(vehicle.AtOnline);
                Assert.IsFalse(vehicle.CarsOnline);
                Assert.IsFalse(vehicle.HasAtData);
                Assert.IsFalse(vehicle.HasCarsData);
            }
        }

        [Test]
        public void TestJsonDeSerializationEnumUnknown()
        {
            string json = "{\"Stores\":[\"HENDRICK02\"],\"SiteVins\":[{ \"Fake\":\"5\",\"Site\":\"Noise\",\"Provider\":\"Carsoup\",\"Vins\":[\"1MEHM42W99G627377\"]}]}";

            var model = json.FromJson<MaxDataOnline>();

            Assert.IsTrue(model.SiteVins[0].Site == Site.Unknown);
            Assert.IsTrue(model.SiteVins[0].Provider == Provider.Unknown);
        }

        [Test]
        public void TestJsonDeSerialization()
        {
            string json = "{\"Stores\":[\"HENDRICK02\"],\"SiteVins\":[{ \"Fake\":\"5\",\"Site\":\"Cars\",\"Provider\":\"Fetch\",\"Vins\":[\"1MEHM42W99G627377\"]}]}";

            var model = json.FromJson<MaxDataOnline>();

            Assert.IsTrue(model.Stores.Length == 1);
            Assert.IsTrue(model.SiteVins.Length == 1);
            Assert.IsTrue(model.SiteVins[0].Vins.Length == 1);
            Assert.IsTrue(model.SiteVins[0].Vins[0] == "1MEHM42W99G627377");
            Assert.IsTrue(model.SiteVins[0].Site == Site.Cars);
            Assert.IsTrue(model.SiteVins[0].Provider == Provider.Fetch);
        }
    }
}
