﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.AdStatus;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.MaxAnalytics
{
	[TestFixture]
	public class AdStatusV2Tests
	{
		[TestCase("MerchandisingDomainModelTests.MaxAnalytics.TestData.AutoFairResults.json", 3, 3, 3)]
		public void CanParseFiles(string file, int providerCount, int siteCount, int adsParsed)
		{
			string value = null;
			var assembly = typeof(AdStatusV2Tests).Assembly;
			if (file != null)
			{
				using (var stream = assembly.GetManifestResourceStream(file))
					if (stream != null)
					{
						using (TextReader textReader = new StreamReader(stream))
							value = textReader.ReadToEnd();
					}
			}

			List<AdStatusResultV2> res = null;
			if (!string.IsNullOrWhiteSpace(value))
				res = value.FromJson<List<AdStatusResultV2>>();

			Assert.IsNotNull(res);
			
			var providers = res.Select(results => results.Provider);
			Assert.IsTrue(providerCount == providers.Count());

			var sites = res.Select(results => results.Site);
			Assert.IsTrue(siteCount == sites.Count());

			var totalAds = res.SelectMany(results => results.Ads, (v2, dates) => v2.Ads );
			Assert.IsTrue(adsParsed == totalAds.Count());
		}

		
	}
}
