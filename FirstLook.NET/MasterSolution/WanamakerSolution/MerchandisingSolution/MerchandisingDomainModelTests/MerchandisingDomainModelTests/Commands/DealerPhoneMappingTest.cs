﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings.Types.TransferObjects;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Commands
{
    /// <summary>
    ///This is a test class for DealerPhoneMappingTest and is intended
    ///to contain all DealerPhoneMappingTest Unit Tests
    ///</summary>
    [TestFixture]
    public class DealerPhoneMappingTest
    {
        #region Properties
        private const int BusinessUnitId = 101621;
        private Guid _ownerHandle = new Guid("11B4C338-B5C3-DC11-9377-0014221831B0");

        private DealerGeneralPreferenceTO _dealerPreference;
        public DealerGeneralPreferenceTO DealerPreference
        {
            get { return _dealerPreference; }
        }
        #endregion

        #region Setup/Teardown
        ////Use TestFixtureSetUp to run code before running the first test in the class
        //[TestFixtureSetUp]
        //public void MyTestFixtureSetUp()
        //{
        //}

        ////Use TestFixtureTearDown to run code after all tests in a class have run
        //[TestFixtureTearDown]
        //public void MyTestFixtureTearDown()
        //{
        //}
        
        //Use SetUp to run code before running each test
        [SetUp]
        public void MySetUp()
        {
            HydrateDealerPreference();
        }

        ////Use TearDown to run code after each test has run
        //[TearDown]
        //public void MyTearDown()
        //{
        //}
        

        private void HydrateDealerPreference()
        {
            _dealerPreference = new DealerGeneralPreferenceTO(
                "12422 Florida Blvd.", 
                "", 
                "Baton Rouge", 
                "LA", 
                "70815",
                "", 
                "", 
                "2252729330", 
                "", 
                0, 
                "", 
                "WC Pontiac Buick GMC", 
                _ownerHandle.ToString(), 
                false);
        }

        private Mock<IDataConnection> MockConnection(List<Dictionary<String, Object>> resultSet)
        {
            var mockParameters = new Mock<IDataParameterCollection>();
            mockParameters.Setup(x => x.Add(It.IsAny<Object>())).Returns(0);

            var mockParameter = new Mock<DbParameter>() { CallBase = true };

            var mockReader = new Mock<IDataReader>() { CallBase = true };
            var count = 0;
            mockReader.Setup(x => x.Read()).Returns(count < resultSet.Count).Callback(() => count++);

            var responseQueue = new Queue<Object>();
            resultSet.ForEach(row =>
            {
                for (var ordinal = 0; ordinal < resultSet[0].Keys.Count; ordinal++)
                {
                    var key = row.Keys.ElementAt(ordinal);
                    int ordinal1 = ordinal;

                    responseQueue.Enqueue(row[key]);
                    mockReader.Setup(x => x.GetOrdinal(It.Is<String>(y => y == key))).Returns(ordinal1);

                    switch (row[key].GetType().ToString())
                    {
                        case "System.Int16":
                            mockReader.Setup(x => x.GetInt16(It.Is<int>(y => y == ordinal1))).Returns(() => (Int16)responseQueue.Dequeue());
                            break;
                        case "System.Int32":
                            mockReader.Setup(x => x.GetInt32(It.Is<int>(y => y == ordinal1))).Returns(() => (Int32)responseQueue.Dequeue());
                            break;
                        case "System.Int64":
                            mockReader.Setup(x => x.GetInt64(It.Is<int>(y => y == ordinal1))).Returns(() => (Int64)responseQueue.Dequeue());
                            break;
                        case "System.DateTime":
                            mockReader.Setup(x => x.GetDateTime(It.Is<int>(y => y == ordinal1))).Returns(() => (DateTime)responseQueue.Dequeue());
                            break;
                        case "System.Boolean":
                            mockReader.Setup(x => x.GetBoolean(It.Is<int>(y => y == ordinal1))).Returns(() => (Boolean)responseQueue.Dequeue());
                            break;
                        default:
                            mockReader.Setup(x => x.GetString(It.Is<int>(y => y == ordinal1))).Returns(() => responseQueue.Dequeue().ToString());
                            break;
                    }
                }
            });

            var mockCommand = new Mock<IDbCommand>() { CallBase = true };
            mockCommand.Setup(x => x.Parameters).Returns(mockParameters.Object);
            mockCommand.Setup(x => x.CreateParameter()).Returns(mockParameter.Object);
            mockCommand.Setup(x => x.ExecuteReader()).Returns(mockReader.Object);
            mockCommand.Setup(x => x.ExecuteNonQuery()).Returns(1);

            var mockConnection = new Mock<IDataConnection>() { CallBase = true };
            mockConnection.Setup(x => x.CreateCommand()).Returns(mockCommand.Object);

            return mockConnection;
        }
        #endregion

        #region Tests
        /// <summary>
        ///A test for DealerPhoneMapping Constructor
        ///</summary>
        [Test]
        public void DealerPhoneMappingConstructorTest()
        {
            var mock = new Mock<DealerPhoneMapping>(BusinessUnitId) {CallBase = true};
            mock.Setup(x => x.DealerPreference).Returns(DealerPreference);

            var target = mock.Object;

            Assert.IsInstanceOf<DealerPhoneMapping>(target);
            Assert.AreEqual(BusinessUnitId, target.BusinessUnitId);
            Assert.IsFalse(target.HasSettings);
            Assert.AreEqual(target.PhoneToFriendlyName(DealerPreference.SalesPhoneNumber), target.MappedPhone);
            Assert.IsEmpty(target.ProvisionedPhone);
            Assert.IsFalse(target.Purchased);
        }

        /// <summary>
        ///A test for Fetch when there is a record returned
        ///</summary>
        [Test]
        public void FetchTestWithRecord()
        {
            var resultSet = new List<Dictionary<String, Object>>
            {
                new Dictionary<string, object> {{"MappedPhone", "(512) 555-1212"}, {"ProvisionedPhone", "(500) 555-0006"}, {"Purchased", true}}
            };

            var mock = new Mock<DealerPhoneMapping>(BusinessUnitId) {CallBase = true};
            mock.Setup(x => x.DealerPreference).Returns(DealerPreference);
            mock.Setup(x => x.Connection).Returns(MockConnection(resultSet).Object);

            var target = mock.Object;

            target.Fetch();

            Assert.AreEqual(BusinessUnitId, target.BusinessUnitId);
            Assert.AreEqual(resultSet[0]["MappedPhone"], target.MappedPhone);
            Assert.AreEqual(resultSet[0]["ProvisionedPhone"], target.ProvisionedPhone);
            Assert.IsTrue(target.HasSettings);
            Assert.IsTrue(target.Purchased);
        }

        /// <summary>
        ///A test for Fetch when there is not a record returned
        ///</summary>
        [Test]
        public void FetchTestWithNoRecord()
        {
            var resultSet = new List<Dictionary<String, Object>>();
            var mock = new Mock<DealerPhoneMapping>(BusinessUnitId) { CallBase = true };
            mock.Setup(x => x.DealerPreference).Returns(DealerPreference);
            mock.Setup(x => x.Connection).Returns(MockConnection(resultSet).Object);

            var target = mock.Object;

            target.Fetch();

            Assert.AreEqual(BusinessUnitId, target.BusinessUnitId);
            Assert.IsFalse(target.HasSettings);
            Assert.AreEqual(target.PhoneToFriendlyName(DealerPreference.SalesPhoneNumber), target.MappedPhone);
            Assert.IsEmpty(target.ProvisionedPhone);
            Assert.IsFalse(target.Purchased);
        }

        /// <summary>
        /// A test for Upsert
        /// </summary>
        [Test]
        public void UpsertTestWithNewPhone()
        {
            var resultSet = new List<Dictionary<String, Object>>
            {
                new Dictionary<string, object> {{"MappedPhone", "(512) 555-1212"}, {"ProvisionedPhone", "(500) 555-0006"}, {"Purchased", true}}
            };

            var mock = new Mock<DealerPhoneMapping>(BusinessUnitId) { CallBase = true };
            mock.Setup(x => x.DealerPreference).Returns(DealerPreference);
            mock.Setup(x => x.Connection).Returns(MockConnection(resultSet).Object);
            mock.Setup(t => t.ProvisionedPhone).Returns((String)resultSet[0]["ProvisionedPhone"]);

            var target = mock.Object;

            target.Upsert((String)resultSet[0]["MappedPhone"]);

            Assert.AreEqual(BusinessUnitId, target.BusinessUnitId);
            Assert.AreEqual(resultSet[0]["MappedPhone"], target.MappedPhone);
            Assert.AreEqual(resultSet[0]["ProvisionedPhone"], target.ProvisionedPhone);
            Assert.IsTrue(target.HasSettings);
            Assert.IsTrue(target.Purchased);
        }

        /// <summary>
        /// A test for Upsert
        /// </summary>
        [Test]
        public void UpsertTestWithPurchasedPhone()
        {
            const String newMappedPhone = "5125555555";

            var resultSet = new List<Dictionary<String, Object>>
            {
                new Dictionary<string, object> {{"MappedPhone", "(512) 555-1212"}, {"ProvisionedPhone", "(500) 555-0006"}, {"Purchased", true}}
            };

            var mock = new Mock<DealerPhoneMapping>(BusinessUnitId) { CallBase = true };
            mock.Setup(x => x.DealerPreference).Returns(DealerPreference);
            mock.Setup(x => x.Connection).Returns(MockConnection(resultSet).Object);

            var target = mock.Object;

            Assert.AreEqual(BusinessUnitId, target.BusinessUnitId);
            Assert.IsFalse(target.HasSettings);
            Assert.IsFalse(target.Purchased);

            target.Fetch(); // retrieve the mocked datbase row
            Assert.AreEqual(resultSet[0]["MappedPhone"], target.MappedPhone);
            Assert.AreEqual(resultSet[0]["ProvisionedPhone"], target.ProvisionedPhone);
            Assert.IsTrue(target.HasSettings);
            Assert.IsTrue(target.Purchased);

            target.Upsert(newMappedPhone);
            Assert.AreEqual(target.PhoneToFriendlyName(newMappedPhone), target.MappedPhone);
            Assert.AreEqual(resultSet[0]["ProvisionedPhone"], target.ProvisionedPhone);
            Assert.IsTrue(target.HasSettings);
            Assert.IsTrue(target.Purchased);
        }

        /// <summary>
        /// A test for Upsert
        /// </summary>
        [Test]
        public void UpsertTestWithFailedPurchaseAndNoSettings()
        {
            var resultSet = new List<Dictionary<String, Object>>
            {
                new Dictionary<string, object> {{"MappedPhone", "(512) 555-1212"}, {"ProvisionedPhone", "(500) 555-0006"}, {"Purchased", true}}
            };

            var mock = new Mock<DealerPhoneMapping>(BusinessUnitId) { CallBase = true };
            mock.Setup(x => x.DealerPreference).Returns(DealerPreference);
            mock.Setup(x => x.Connection).Returns(MockConnection(resultSet).Object);
            mock.Setup(x => x.ProvisionedPhone).Returns((String)resultSet[0]["ProvisionedPhone"]);
            mock.Setup(x => x.Purchased).Returns(false);

            var target = mock.Object;
            Assert.AreEqual(BusinessUnitId, target.BusinessUnitId);
            Assert.IsFalse(target.HasSettings);

            target.Upsert((String)resultSet[0]["MappedPhone"]);
            Assert.AreEqual(BusinessUnitId, target.BusinessUnitId);
            Assert.AreEqual(resultSet[0]["MappedPhone"], target.MappedPhone);
            Assert.IsFalse(target.HasSettings);
        }

        /// <summary>
        /// A test for Upsert
        /// </summary>
        [Test]
        public void UpsertTestWithEmptyMappedPhone()
        {
            var resultSet = new List<Dictionary<String, Object>>
            {
                new Dictionary<string, object> {{"MappedPhone", "(512) 555-1212"}, {"ProvisionedPhone", "(500) 555-0006"}, {"Purchased", true}}
            };

            var mock = new Mock<DealerPhoneMapping>(BusinessUnitId) { CallBase = true };
            mock.Setup(x => x.DealerPreference).Returns(DealerPreference);
            mock.Setup(x => x.Connection).Returns(MockConnection(resultSet).Object);
            mock.Setup(t => t.ProvisionedPhone).Returns((String)resultSet[0]["ProvisionedPhone"]);

            var target = mock.Object;

            try
            {
                target.Upsert(String.Empty);
            }
            catch (ApplicationException ex)
            {
                Assert.AreEqual(DealerPhoneMapping.ErrorMappedPhoneIsNullOrWhiteSpace, ex.Message);
            }
        }

        /// <summary>
        /// A test for Upsert
        /// </summary>
        [TestCase("(500) 555-0000")] // unavailable
        [TestCase("(500) 555-0001")] // invalid
        [ExpectedException(typeof(ApplicationException))]
        public void UpsertTestWithInvalidOrUnavailablePhone(String phone)
        {
            var resultSet = new List<Dictionary<String, Object>>
            {
                new Dictionary<string, object> {{"MappedPhone", "(512) 555-1212"}, {"ProvisionedPhone", phone}, {"Purchased", false}}
            };

            var mock = new Mock<DealerPhoneMapping>(BusinessUnitId) { CallBase = true };
            mock.Setup(x => x.DealerPreference).Returns(DealerPreference);
            mock.Setup(x => x.Connection).Returns(MockConnection(resultSet).Object);
            mock.Setup(t => t.ProvisionedPhone).Returns((String)resultSet[0]["ProvisionedPhone"]);

            var target = mock.Object;

            target.Upsert((String)resultSet[0]["MappedPhone"]);
        }

        /// <summary>
        ///A test for GetNewNumber when Purchased is true
        ///</summary>
        [Test]
        public void GetNewNumberTestWhenAlreadyPurchased()
        {
            var mock = new Mock<DealerPhoneMapping>(BusinessUnitId) { CallBase = true };
            mock.Setup(t => t.DealerPreference).Returns(DealerPreference);
            mock.Setup(t => t.Purchased).Returns(() => true);

            var target = mock.Object;

            try
            {
                target.GetNewNumber("");
            }
            catch (ApplicationException ex)
            {
                Assert.AreEqual(DealerPhoneMapping.ErrorAlreadyPurchased, ex.Message);
            }
        }

        /// <summary>
        ///A test for GetNewNumber when Area Code is null or whitespace
        ///</summary>
        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        [TestCase("\t")]
        [TestCase("\n")]
        [TestCase("\r")]
        [TestCase("\r\n")]
        public void GetNewNumberTestWhenAreaCodeIsNullOrWhiteSpace(String areaCode)
        {
            var mock = new Mock<DealerPhoneMapping>(BusinessUnitId) { CallBase = true };
            mock.Setup(t => t.DealerPreference).Returns(DealerPreference);

            var target = mock.Object;
            try
            {
                target.GetNewNumber(areaCode);
            }
            catch (ApplicationException ex)
            {
                Assert.AreEqual(DealerPhoneMapping.ErrorAreaCodeIsNullOrWhiteSpace, ex.Message);
            }
        }

        /// <summary>
        ///A test for GetNewNumber when Area Code is toll free
        ///</summary>
        [TestCase("800")]
        [TestCase("844")]
        [TestCase("855")]
        [TestCase("866")]
        [TestCase("877")]
        [TestCase("888")]
        public void GetNewNumberTestWhenAreaCodeIsTollFree(String areaCode)
        {
            var mock = new Mock<DealerPhoneMapping>(BusinessUnitId) { CallBase = true };
            mock.SetupGet(t => t.DealerPreference).Returns(DealerPreference);

            var target = mock.Object;
            try
            {
                target.GetNewNumber(areaCode);
            }
            catch (ApplicationException ex)
            {
                Assert.AreEqual(DealerPhoneMapping.ErrorAreaCodeIsTollFree, ex.Message);
            }
        }

        /// <summary>
        ///A test for GetNewNumber when Area Code is not numeric
        ///</summary>
        [TestCase("abc")]
        [TestCase("ABC")]
        [TestCase("ab1")]
        [TestCase("AB1")]
        [TestCase("1bc")]
        [TestCase("1BC")]
        public void GetNewNumberTestWhenAreaCodeIsNotNumeric(String areaCode)
        {
            var mock = new Mock<DealerPhoneMapping>(BusinessUnitId) { CallBase = true };
            mock.Setup(t => t.DealerPreference).Returns(DealerPreference);

            var target = mock.Object;
            try
            {
                target.GetNewNumber(areaCode);
            }
            catch (ApplicationException ex)
            {
                Assert.AreEqual(DealerPhoneMapping.ErrorAreaCodeIsNotNumeric, ex.Message);
            }
        }

        [Test]
        [ExpectedException(typeof(ApplicationException))] // Resource not accessible with Test Account Credentials
        public void GetNewNumberTest()
        {
            var mock = new Mock<DealerPhoneMapping>(BusinessUnitId) { CallBase = true };
            mock.Setup(t => t.DealerPreference).Returns(DealerPreference);

            var target = mock.Object;
            target.GetNewNumber("512");
        }

        /// <summary>
        ///A test for PurchaseNumber
        ///</summary>
        [Test]
        public void PurchaseNumberTest_ValidAndAvailable()
        {
            const string phone = "(500) 555-0006";
            var mock = new Mock<DealerPhoneMapping>(BusinessUnitId) { CallBase = true };
            mock.Setup(t => t.DealerPreference).Returns(DealerPreference);
            mock.Setup(t => t.ProvisionedPhone).Returns(phone);

            var target = mock.Object;
            target.PurchaseNumber();

            Assert.IsTrue(target.Purchased);
            Assert.AreEqual(phone, target.ProvisionedPhone);
        }

        /// <summary>
        ///A test for PurchaseNumber
        ///</summary>
        [Test]
        [ExpectedException(typeof(ApplicationException))]
        public void PurchaseNumberTest_Unavailable()
        {
            const string phone = "(500) 555-0000";
            var mock = new Mock<DealerPhoneMapping>(BusinessUnitId) { CallBase = true };
            mock.Setup(t => t.DealerPreference).Returns(DealerPreference);
            mock.Setup(t => t.ProvisionedPhone).Returns(phone);

            var target = mock.Object;
            target.PurchaseNumber();
        }

        /// <summary>
        ///A test for PurchaseNumber
        ///</summary>
        [Test]
        [ExpectedException(typeof(ApplicationException))]
        public void PurchaseNumberTest_Invalid()
        {
            const string phone = "(500) 555-0001";
            var mock = new Mock<DealerPhoneMapping>(BusinessUnitId) { CallBase = true };
            mock.Setup(t => t.DealerPreference).Returns(DealerPreference);
            mock.Setup(t => t.ProvisionedPhone).Returns(phone);

            var target = mock.Object;
            target.PurchaseNumber();
        }

        /// <summary>
        ///A test for MappedPhone
        ///</summary>
        [Test]
        public void MappedPhoneTest() // set via call to DealerGeneralPreferences or Upsert
        {
            var mock = new Mock<DealerPhoneMapping>(BusinessUnitId) {CallBase = true};
            mock.Setup(t => t.DealerPreference).Returns(DealerPreference);

            var target = mock.Object;

            Assert.AreEqual("(225) 272-9330", target.MappedPhone);
        }

        /// <summary>
        ///A test for MappedPhoneAreaCode
        ///</summary>
        [Test]
        public void MappedPhoneAreaCodeTest()
        {
            var mock = new Mock<DealerPhoneMapping>(BusinessUnitId) { CallBase = true };
            mock.Setup(t => t.DealerPreference).Returns(DealerPreference);

            var target = mock.Object;

            Assert.AreEqual("225", target.MappedPhoneAreaCode);
        }

        /// <summary>
        ///A test for PhoneToE164
        ///</summary>
        [TestCase("5125551212", "+15125551212")]
        [TestCase("15125551212", "+15125551212")]
        [TestCase("+15125551212", "+15125551212")]
        [TestCase("(512)5551212", "+15125551212")]
        [TestCase("1(512)5551212", "+15125551212")]
        [TestCase("+1(512)5551212", "+15125551212")]
        [TestCase("512 5551212", "+15125551212")]
        [TestCase("512 555 1212", "+15125551212")]
        [TestCase("1512 5551212", "+15125551212")]
        [TestCase("1512 555 1212", "+15125551212")]
        [TestCase("1 512 555 1212", "+15125551212")]
        [TestCase("+1512 5551212", "+15125551212")]
        [TestCase("+1512 555 1212", "+15125551212")]
        [TestCase("+1 512 555 1212", "+15125551212")]
        [TestCase("(512) 5551212", "+15125551212")]
        [TestCase("(512) 555 1212", "+15125551212")]
        [TestCase("1(512) 5551212", "+15125551212")]
        [TestCase("1(512) 555 1212", "+15125551212")]
        [TestCase("1 (512) 5551212", "+15125551212")]
        [TestCase("1 (512) 555 1212", "+15125551212")]
        [TestCase("+1(512) 5551212", "+15125551212")]
        [TestCase("+1(512) 555 1212", "+15125551212")]
        [TestCase("+1 (512) 5551212", "+15125551212")]
        [TestCase("+1 (512) 555 1212", "+15125551212")]
        [TestCase("512-5551212", "+15125551212")]
        [TestCase("512-555-1212", "+15125551212")]
        [TestCase("1512-5551212", "+15125551212")]
        [TestCase("1512-555-1212", "+15125551212")]
        [TestCase("1-512-555-1212", "+15125551212")]
        [TestCase("+1512-5551212", "+15125551212")]
        [TestCase("+1512-555-1212", "+15125551212")]
        [TestCase("+1-512-555-1212", "+15125551212")]
        [TestCase("(512)-5551212", "+15125551212")]
        [TestCase("(512)-555-1212", "+15125551212")]
        [TestCase("1(512)-5551212", "+15125551212")]
        [TestCase("1(512)-555-1212", "+15125551212")]
        [TestCase("1-(512)-5551212", "+15125551212")]
        [TestCase("1-(512)-555-1212", "+15125551212")]
        [TestCase("+1(512)-5551212", "+15125551212")]
        [TestCase("+1(512)-555-1212", "+15125551212")]
        [TestCase("+1-(512)-5551212", "+15125551212")]
        [TestCase("+1-(512)-555-1212", "+15125551212")]
        [TestCase("512.5551212", "+15125551212")]
        [TestCase("512.555.1212", "+15125551212")]
        [TestCase("1512.5551212", "+15125551212")]
        [TestCase("1512.555.1212", "+15125551212")]
        [TestCase("1.512.555.1212", "+15125551212")]
        [TestCase("+1512.5551212", "+15125551212")]
        [TestCase("+1512.555.1212", "+15125551212")]
        [TestCase("+1.512.555.1212", "+15125551212")]
        [TestCase("(512).5551212", "+15125551212")]
        [TestCase("(512).555.1212", "+15125551212")]
        [TestCase("1(512).5551212", "+15125551212")]
        [TestCase("1(512).555.1212", "+15125551212")]
        [TestCase("1.(512).5551212", "+15125551212")]
        [TestCase("1.(512).555.1212", "+15125551212")]
        [TestCase("+1(512).5551212", "+15125551212")]
        [TestCase("+1(512).555.1212", "+15125551212")]
        [TestCase("+1.(512).5551212", "+15125551212")]
        [TestCase("+1.(512).555.1212", "+15125551212")]
        [TestCase("512,5551212", "+15125551212")]
        [TestCase("512,555,1212", "+15125551212")]
        [TestCase("1512,5551212", "+15125551212")]
        [TestCase("1512,555,1212", "+15125551212")]
        [TestCase("1,512,555,1212", "+15125551212")]
        [TestCase("+1512,5551212", "+15125551212")]
        [TestCase("+1512,555,1212", "+15125551212")]
        [TestCase("+1,512,555,1212", "+15125551212")]
        [TestCase("(512),5551212", "+15125551212")]
        [TestCase("(512),555,1212", "+15125551212")]
        [TestCase("1(512),5551212", "+15125551212")]
        [TestCase("1(512),555,1212", "+15125551212")]
        [TestCase("1,(512),5551212", "+15125551212")]
        [TestCase("1,(512),555,1212", "+15125551212")]
        [TestCase("+1(512),5551212", "+15125551212")]
        [TestCase("+1(512),555,1212", "+15125551212")]
        [TestCase("+1,(512),5551212", "+15125551212")]
        [TestCase("+1,(512),555,1212", "+15125551212")]
        [TestCase("(512) 555-1212", "+15125551212")]
        public void PhoneToE164Test(String phoneNumber, String expectedNumber)
        {
            var target = new DealerPhoneMapping(BusinessUnitId);

            var actual = target.PhoneToE164(phoneNumber);
            Assert.AreEqual(expectedNumber, actual);
        }

        /// <summary>
        ///A test for PhoneToFriendlyName
        ///</summary>
        [TestCase("5125551212", "(512) 555-1212")]
        [TestCase("15125551212", "(512) 555-1212")]
        [TestCase("+15125551212", "(512) 555-1212")]
        [TestCase("(512)5551212", "(512) 555-1212")]
        [TestCase("1(512)5551212", "(512) 555-1212")]
        [TestCase("+1(512)5551212", "(512) 555-1212")]
        [TestCase("512 5551212", "(512) 555-1212")]
        [TestCase("512 555 1212", "(512) 555-1212")]
        [TestCase("1512 5551212", "(512) 555-1212")]
        [TestCase("1512 555 1212", "(512) 555-1212")]
        [TestCase("1 512 555 1212", "(512) 555-1212")]
        [TestCase("+1512 5551212", "(512) 555-1212")]
        [TestCase("+1512 555 1212", "(512) 555-1212")]
        [TestCase("+1 512 555 1212", "(512) 555-1212")]
        [TestCase("(512) 5551212", "(512) 555-1212")]
        [TestCase("(512) 555 1212", "(512) 555-1212")]
        [TestCase("1(512) 5551212", "(512) 555-1212")]
        [TestCase("1(512) 555 1212", "(512) 555-1212")]
        [TestCase("1 (512) 5551212", "(512) 555-1212")]
        [TestCase("1 (512) 555 1212", "(512) 555-1212")]
        [TestCase("+1(512) 5551212", "(512) 555-1212")]
        [TestCase("+1(512) 555 1212", "(512) 555-1212")]
        [TestCase("+1 (512) 5551212", "(512) 555-1212")]
        [TestCase("+1 (512) 555 1212", "(512) 555-1212")]
        [TestCase("512-5551212", "(512) 555-1212")]
        [TestCase("512-555-1212", "(512) 555-1212")]
        [TestCase("1512-5551212", "(512) 555-1212")]
        [TestCase("1512-555-1212", "(512) 555-1212")]
        [TestCase("1-512-555-1212", "(512) 555-1212")]
        [TestCase("+1512-5551212", "(512) 555-1212")]
        [TestCase("+1512-555-1212", "(512) 555-1212")]
        [TestCase("+1-512-555-1212", "(512) 555-1212")]
        [TestCase("(512)-5551212", "(512) 555-1212")]
        [TestCase("(512)-555-1212", "(512) 555-1212")]
        [TestCase("1(512)-5551212", "(512) 555-1212")]
        [TestCase("1(512)-555-1212", "(512) 555-1212")]
        [TestCase("1-(512)-5551212", "(512) 555-1212")]
        [TestCase("1-(512)-555-1212", "(512) 555-1212")]
        [TestCase("+1(512)-5551212", "(512) 555-1212")]
        [TestCase("+1(512)-555-1212", "(512) 555-1212")]
        [TestCase("+1-(512)-5551212", "(512) 555-1212")]
        [TestCase("+1-(512)-555-1212", "(512) 555-1212")]
        [TestCase("512.5551212", "(512) 555-1212")]
        [TestCase("512.555.1212", "(512) 555-1212")]
        [TestCase("1512.5551212", "(512) 555-1212")]
        [TestCase("1512.555.1212", "(512) 555-1212")]
        [TestCase("1.512.555.1212", "(512) 555-1212")]
        [TestCase("+1512.5551212", "(512) 555-1212")]
        [TestCase("+1512.555.1212", "(512) 555-1212")]
        [TestCase("+1.512.555.1212", "(512) 555-1212")]
        [TestCase("(512).5551212", "(512) 555-1212")]
        [TestCase("(512).555.1212", "(512) 555-1212")]
        [TestCase("1(512).5551212", "(512) 555-1212")]
        [TestCase("1(512).555.1212", "(512) 555-1212")]
        [TestCase("1.(512).5551212", "(512) 555-1212")]
        [TestCase("1.(512).555.1212", "(512) 555-1212")]
        [TestCase("+1(512).5551212", "(512) 555-1212")]
        [TestCase("+1(512).555.1212", "(512) 555-1212")]
        [TestCase("+1.(512).5551212", "(512) 555-1212")]
        [TestCase("+1.(512).555.1212", "(512) 555-1212")]
        [TestCase("512,5551212", "(512) 555-1212")]
        [TestCase("512,555,1212", "(512) 555-1212")]
        [TestCase("1512,5551212", "(512) 555-1212")]
        [TestCase("1512,555,1212", "(512) 555-1212")]
        [TestCase("1,512,555,1212", "(512) 555-1212")]
        [TestCase("+1512,5551212", "(512) 555-1212")]
        [TestCase("+1512,555,1212", "(512) 555-1212")]
        [TestCase("+1,512,555,1212", "(512) 555-1212")]
        [TestCase("(512),5551212", "(512) 555-1212")]
        [TestCase("(512),555,1212", "(512) 555-1212")]
        [TestCase("1(512),5551212", "(512) 555-1212")]
        [TestCase("1(512),555,1212", "(512) 555-1212")]
        [TestCase("1,(512),5551212", "(512) 555-1212")]
        [TestCase("1,(512),555,1212", "(512) 555-1212")]
        [TestCase("+1(512),5551212", "(512) 555-1212")]
        [TestCase("+1(512),555,1212", "(512) 555-1212")]
        [TestCase("+1,(512),5551212", "(512) 555-1212")]
        [TestCase("+1,(512),555,1212", "(512) 555-1212")]
        [TestCase("(512) 555-1212", "(512) 555-1212")]
        public void PhoneToFriendlyNameTest(String phoneNumber, String expectedNumber)
        {
            var target = new DealerPhoneMapping(BusinessUnitId);

            var actual = target.PhoneToFriendlyName(phoneNumber);
            Assert.AreEqual(expectedNumber, actual);
        }
        #endregion
    }
}
