﻿using System.Runtime.Caching;
using FirstLook.Common.Core.Cache;
using FirstLook.Merchandising.DomainModel.Chrome;
using FirstLook.Merchandising.DomainModel.Chrome.Model;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Chrome
{
    [TestFixture]
    public class CachedSharedChromeDataRepositoryTests
    {
        private DotnetMemoryCacheWrapper _cache;
        private Mock<ISharedChromeDataRepository> _innerMock;
        private ISharedChromeDataRepository _inner;
        private SharedChromeData _sampleData;

        [SetUp]
        public void Setup()
        {
            _cache =
                new DotnetMemoryCacheWrapper(
                    new System.Runtime.Caching.MemoryCache("CachedSharedChromeDataRepositoryTests"));

            _sampleData = new SharedChromeData();

            _innerMock = new Mock<ISharedChromeDataRepository>();
            _innerMock.Setup(r => r.GetData()).Returns(_sampleData);
            _inner = _innerMock.Object;
        }

        [Test]
        public void Call_GetData_once_should_access_inner()
        {
            var repository = new CachedSharedChromeDataRepository(_cache, _inner);

            var data = repository.GetData();

            Assert.That(data, Is.SameAs(_sampleData));
        }

        [Test]
        public void Call_GetData_twice_should_only_call_inner_once()
        {
            var repository = new CachedSharedChromeDataRepository(_cache, _inner);

            var data = repository.GetData();

            var data2 = repository.GetData();

            Assert.That(data, Is.SameAs(_sampleData));
            Assert.That(data2, Is.SameAs(_sampleData));
            _innerMock.Verify(r => r.GetData(), Times.Once());
        }
    }
}