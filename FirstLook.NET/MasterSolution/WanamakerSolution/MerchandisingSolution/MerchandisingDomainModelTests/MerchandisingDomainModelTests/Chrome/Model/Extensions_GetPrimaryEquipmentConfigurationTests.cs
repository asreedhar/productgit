﻿using FirstLook.Merchandising.DomainModel.Chrome.Model;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Chrome.Model
{
    [TestFixture]
    public class Extensions_GetPrimaryEquipmentConfigurationTests
    {
        [Test]
        public void Single_standard_engine_should_return_single_engine()
        {
            var style = new Style(styleGenericEquipment: new[]
                {
                    new StyleGenericEquipment(categoryID: 1051, styleAvailability: "Standard"),
                    new StyleGenericEquipment(categoryID: 1052, styleAvailability: "Optional"),
                    new StyleGenericEquipment(categoryID: 1103, styleAvailability: "Standard") // Throw in transmission "Standard" to throw it off the trail 
                });

            var chromeData = new SharedChromeData(
                categories: new[]
                    {
                        new Category(categoryID: 1051, categoryTypeFilter: "Engine"),
                        new Category(categoryID: 1052, categoryTypeFilter: "Engine"),
                        new Category(categoryID: 1103, categoryTypeFilter: "Transmission")
                    });

            var pec = style.GetPrimaryEquipmentConfiguration(chromeData);

            Assert.That(pec.Engine.TypeID, Is.EqualTo(1051));
        }

        [Test]
        public void No_standard_engine_should_return_negative_one()
        {
            var style = new Style(styleGenericEquipment: new[]
                {
                    new StyleGenericEquipment(categoryID: 1051, styleAvailability: "Optional"),
                    new StyleGenericEquipment(categoryID: 1052, styleAvailability: "Optional"),
                    new StyleGenericEquipment(categoryID: 1103, styleAvailability: "Standard") // Throw in transmission "Standard" to throw it off the trail 
                });

            var chromeData = new SharedChromeData(
                categories: new[]
                    {
                        new Category(categoryID: 1051, categoryTypeFilter: "Engine"),
                        new Category(categoryID: 1052, categoryTypeFilter: "Engine"),
                        new Category(categoryID: 1103, categoryTypeFilter: "Transmission")
                    });

            var pec = style.GetPrimaryEquipmentConfiguration(chromeData);

            Assert.That(pec.Engine.TypeID, Is.EqualTo(-1));
        }

        [Test]
        public void Two_standard_engines_should_return_negative_one()
        {
            var style = new Style(styleGenericEquipment: new[]
                {
                    new StyleGenericEquipment(categoryID: 1051, styleAvailability: "Standard"),
                    new StyleGenericEquipment(categoryID: 1052, styleAvailability: "Standard"),
                    new StyleGenericEquipment(categoryID: 1103, styleAvailability: "Standard") // Throw in transmission "Standard" to throw it off the trail 
                });

            var chromeData = new SharedChromeData(
                categories: new[]
                    {
                        new Category(categoryID: 1051, categoryTypeFilter: "Engine"),
                        new Category(categoryID: 1052, categoryTypeFilter: "Engine"),
                        new Category(categoryID: 1103, categoryTypeFilter: "Transmission")
                    });

            var pec = style.GetPrimaryEquipmentConfiguration(chromeData);

            Assert.That(pec.Engine.TypeID, Is.EqualTo(-1));
        }

        [Test]
        public void Single_standard_tranmission_should_return_single_transmission()
        {
            var style = new Style(styleGenericEquipment: new[]
                {
                    new StyleGenericEquipment(categoryID: 1051, styleAvailability: "Standard"),
                    new StyleGenericEquipment(categoryID: 1052, styleAvailability: "Optional"),
                    new StyleGenericEquipment(categoryID: 1103, styleAvailability: "Optional"),
                    new StyleGenericEquipment(categoryID: 1106, styleAvailability: "Standard")
                });

            var chromeData = new SharedChromeData(
                categories: new[]
                    {
                        new Category(categoryID: 1051, categoryTypeFilter: "Engine"),
                        new Category(categoryID: 1052, categoryTypeFilter: "Engine"),
                        new Category(categoryID: 1103, categoryTypeFilter: "Transmission"),
                        new Category(categoryID: 1106, categoryTypeFilter: "Transmission")
                    });

            var pec = style.GetPrimaryEquipmentConfiguration(chromeData);

            Assert.That(pec.Transmission.TypeID, Is.EqualTo(1106));
        }

        [Test]
        public void No_standard_transmission_should_return_negative_one()
        {
            var style = new Style(styleGenericEquipment: new[]
                {
                    new StyleGenericEquipment(categoryID: 1051, styleAvailability: "Optional"),
                    new StyleGenericEquipment(categoryID: 1052, styleAvailability: "Optional"),
                    new StyleGenericEquipment(categoryID: 1103, styleAvailability: "Optional"),
                    new StyleGenericEquipment(categoryID: 1106, styleAvailability: "Optional")
                });

            var chromeData = new SharedChromeData(
                categories: new[]
                    {
                        new Category(categoryID: 1051, categoryTypeFilter: "Engine"),
                        new Category(categoryID: 1052, categoryTypeFilter: "Engine"),
                        new Category(categoryID: 1103, categoryTypeFilter: "Transmission"),
                        new Category(categoryID: 1106, categoryTypeFilter: "Transmission")
                    });

            var pec = style.GetPrimaryEquipmentConfiguration(chromeData);

            Assert.That(pec.Transmission.TypeID, Is.EqualTo(-1));
        }

        [Test]
        public void Two_standard_transmission_should_return_negative_one()
        {
            var style = new Style(styleGenericEquipment: new[]
                {
                    new StyleGenericEquipment(categoryID: 1051, styleAvailability: "Standard"),
                    new StyleGenericEquipment(categoryID: 1052, styleAvailability: "Standard"),
                    new StyleGenericEquipment(categoryID: 1103, styleAvailability: "Standard"),
                    new StyleGenericEquipment(categoryID: 1106, styleAvailability: "Standard")
                });

            var chromeData = new SharedChromeData(
                categories: new[]
                    {
                        new Category(categoryID: 1051, categoryTypeFilter: "Engine"),
                        new Category(categoryID: 1052, categoryTypeFilter: "Engine"),
                        new Category(categoryID: 1103, categoryTypeFilter: "Transmission"),
                        new Category(categoryID: 1106, categoryTypeFilter: "Transmission")
                    });

            var pec = style.GetPrimaryEquipmentConfiguration(chromeData);

            Assert.That(pec.Transmission.TypeID, Is.EqualTo(-1));
        }

        [Test]
        public void Single_standard_drivetrain_should_return_single_drivetrain()
        {
            var style = new Style(styleGenericEquipment: new[]
                {
                    new StyleGenericEquipment(categoryID: 1041, styleAvailability: "STANDARD"),
                    new StyleGenericEquipment(categoryID: 1040, styleAvailability: "OPTIONAL"),
                    new StyleGenericEquipment(categoryID: 1103, styleAvailability: "OPTIONAL"),
                    new StyleGenericEquipment(categoryID: 1106, styleAvailability: "STANDARD")
                });

            var chromeData = new SharedChromeData(
                categories: new[]
                    {
                        new Category(categoryID: 1051, categoryTypeFilter: "ENGINE"),
                        new Category(categoryID: 1052, categoryTypeFilter: "ENGINE"),
                        new Category(categoryID: 1041, categoryTypeFilter: "DRIVETRAIN"),
                        new Category(categoryID: 1040, categoryTypeFilter: "DRIVETRAIN")
                    });

            var pec = style.GetPrimaryEquipmentConfiguration(chromeData);

            Assert.That(pec.Drivetrain.TypeID, Is.EqualTo(1041));
        }

        [Test]
        public void No_standard_drivetrain_should_return_negative_one()
        {
            var style = new Style(styleGenericEquipment: new[]
                {
                    new StyleGenericEquipment(categoryID: 1041, styleAvailability: "optional"),
                    new StyleGenericEquipment(categoryID: 1040, styleAvailability: "optional"),
                    new StyleGenericEquipment(categoryID: 1103, styleAvailability: "optional"),
                    new StyleGenericEquipment(categoryID: 1106, styleAvailability: "optional")
                });

            var chromeData = new SharedChromeData(
                categories: new[]
                    {
                        new Category(categoryID: 1041, categoryTypeFilter: "drivetrain"),
                        new Category(categoryID: 1040, categoryTypeFilter: "drivatrain"),
                        new Category(categoryID: 1103, categoryTypeFilter: "transmission"),
                        new Category(categoryID: 1106, categoryTypeFilter: "transmission")
                    });

            var pec = style.GetPrimaryEquipmentConfiguration(chromeData);

            Assert.That(pec.Drivetrain.TypeID, Is.EqualTo(-1));
        }

        [Test]
        public void Two_standard_drivetrain_should_return_negative_one()
        {
            var style = new Style(styleGenericEquipment: new[]
                {
                    new StyleGenericEquipment(categoryID: 1051, styleAvailability: "Standard"),
                    new StyleGenericEquipment(categoryID: 1052, styleAvailability: "Standard"),
                    new StyleGenericEquipment(categoryID: 1041, styleAvailability: "Standard"),
                    new StyleGenericEquipment(categoryID: 1040, styleAvailability: "Standard")
                });

            var chromeData = new SharedChromeData(
                categories: new[]
                    {
                        new Category(categoryID: 1051, categoryTypeFilter: "Engine"),
                        new Category(categoryID: 1041, categoryTypeFilter: "drivetrain"),
                        new Category(categoryID: 1040, categoryTypeFilter: "drivetrain"),
                        new Category(categoryID: 1106, categoryTypeFilter: "Transmission")
                    });

            var pec = style.GetPrimaryEquipmentConfiguration(chromeData);

            Assert.That(pec.Drivetrain.TypeID, Is.EqualTo(-1));
        }

        [Test]
        public void Single_standard_fuelsystem_should_return_single_fuelsystem()
        {
            var style = new Style(styleGenericEquipment: new[]
                {
                    new StyleGenericEquipment(categoryID: 1058, styleAvailability: "STANDARD"),
                    new StyleGenericEquipment(categoryID: 1059, styleAvailability: "OPTIONAL"),
                    new StyleGenericEquipment(categoryID: 1103, styleAvailability: "OPTIONAL"),
                    new StyleGenericEquipment(categoryID: 1106, styleAvailability: "STANDARD")
                });

            var chromeData = new SharedChromeData(
                categories: new[]
                    {
                        new Category(categoryID: 1058, categoryTypeFilter: "fuel system"),
                        new Category(categoryID: 1059, categoryTypeFilter: "fuel system"),
                        new Category(categoryID: 1041, categoryTypeFilter: "DRIVETRAIN"),
                        new Category(categoryID: 1040, categoryTypeFilter: "DRIVETRAIN")
                    });

            var pec = style.GetPrimaryEquipmentConfiguration(chromeData);

            Assert.That(pec.Fuel.TypeID, Is.EqualTo(1058));
        }

        [Test]
        public void No_standard_fuelsystem_should_return_negative_one()
        {
            var style = new Style(styleGenericEquipment: new StyleGenericEquipment[0]);

            var chromeData = new SharedChromeData(
                categories: new[]
                    {
                        new Category(categoryID: 1041, categoryTypeFilter: "drivetrain"),
                        new Category(categoryID: 1040, categoryTypeFilter: "drivatrain"),
                        new Category(categoryID: 1103, categoryTypeFilter: "transmission"),
                        new Category(categoryID: 1106, categoryTypeFilter: "transmission")
                    });

            var pec = style.GetPrimaryEquipmentConfiguration(chromeData);

            Assert.That(pec.Fuel.TypeID, Is.EqualTo(-1));
        }

        [Test]
        public void Two_standard_fuelsystem_should_return_negative_one()
        {
            var style = new Style(styleGenericEquipment: new[]
                {
                    new StyleGenericEquipment(categoryID: 1051, styleAvailability: "Standard"),
                    new StyleGenericEquipment(categoryID: 1052, styleAvailability: "Standard"),
                    new StyleGenericEquipment(categoryID: 1058, styleAvailability: "Standard"),
                    new StyleGenericEquipment(categoryID: 1059, styleAvailability: "Standard")
                });

            var chromeData = new SharedChromeData(
                categories: new[]
                    {
                        new Category(categoryID: 1051, categoryTypeFilter: "Engine"),
                        new Category(categoryID: 1041, categoryTypeFilter: "drivetrain"),
                        new Category(categoryID: 1058, categoryTypeFilter: "fuel system"),
                        new Category(categoryID: 1059, categoryTypeFilter: "FUEL SYSTEM")
                    });

            var pec = style.GetPrimaryEquipmentConfiguration(chromeData);

            Assert.That(pec.Fuel.TypeID, Is.EqualTo(-1));
        }
    }
}