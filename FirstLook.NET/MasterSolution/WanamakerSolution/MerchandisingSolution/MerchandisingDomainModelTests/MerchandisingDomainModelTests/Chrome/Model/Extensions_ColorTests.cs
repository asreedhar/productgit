﻿using System.Linq;
using FirstLook.Merchandising.DomainModel.Chrome.Model;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Chrome.Model
{
    [TestFixture]
    public class Extensions_ColorTests
    {
        [Test]
        public void GetExteriorColors_should_return_unique_color_pairs_for_style_976()
        {
            var raw = GetColorsForStyle976();
            var style = new Style(colors: raw);

            var exteriorColors = style.GetExteriorColors().ToArray();

            Assert.That(
                exteriorColors.Select(
                    c => string.Format("{0}|{1}|{2}|{3}", c.Ext1Desc, c.Ext2Desc, c.Ext1Simple, c.Ext2Simple)).ToArray(),
                Is.EquivalentTo(new[]
                    {
                        "Dark Cherry Red Metallic||Red|",
                        "Dark Cherry Red Metallic|Gold|Red|Gold",
                        "Dark Cherry Red Metallic|Onyx Black|Red|Black",
                        "Dark Cherry Red Metallic|Red Flames|Red|Red",
                        "Dark Cherry Red Metallic|Sparkle Silver|Red|Silver",
                        "Dark Green Metallic||Green|",
                        "Indigo Blue Metallic||Blue|",
                        "Indigo Blue Metallic|Competition Yellow|Blue|Yellow",
                        "Indigo Blue Metallic|Gold|Blue|Gold",
                        "Indigo Blue Metallic|Onyx Black|Blue|Black",
                        "Indigo Blue Metallic|Red Flames|Blue|Red",
                        "Indigo Blue Metallic|Sparkle Silver|Blue|Silver",
                        "Indigo Blue Metallic|Torch Red|Blue|Red",
                        "Light Pewter Metallic||Gold|",
                        "Onyx Black||Black|",
                        "Onyx Black|Competition Yellow|Black|Yellow",
                        "Onyx Black|Gold|Black|Gold",
                        "Onyx Black|Red Flames|Black|Red",
                        "Onyx Black|Sparkle Silver|Black|Silver",
                        "Onyx Black|Torch Red|Black|Red",
                        "Sandalwood Metallic||Gold|",
                        "Special Paint||Non-Color|",
                        "Summit White||White|",
                        "Summit White|Gold|White|Gold",
                        "Summit White|Onyx Black|White|Black",
                        "Summit White|Red Flames|White|Red",
                        "Summit White|Sparkle Silver|White|Silver",
                        "Summit White|Torch Red|White|Red",
                        "Victory Red||Red|",
                        "Victory Red|Competition Yellow|Red|Yellow",
                        "Victory Red|Onyx Black|Red|Black",
                        "Victory Red|Red Flames|Red|Red",
                        "Victory Red|Sparkle Silver|Red|Silver",
                        "Yellow||Yellow|",
                        "Yellow|Onyx Black|Yellow|Black",
                        "Yellow|Red Flames|Yellow|Red",
                        "Yellow|Sparkle Silver|Yellow|Silver",
                        "Yellow|Torch Red|Yellow|Red"
                    }));
        }

        [Test]
        public void GetExteriorColors_should_return_unique_color_pairs_for_style_332735()
        {
            var raw = GetColorsForStyle332735();
            var style = new Style(colors: raw);

            var exteriorColors = style.GetExteriorColors().ToArray();

            Assert.That(
                exteriorColors.Select(
                    c => string.Format("{0}|{1}|{2}|{3}", c.Ext1Desc, c.Ext2Desc, c.Ext1Simple, c.Ext2Simple)).ToArray(),
                Is.EquivalentTo(new[]
                    {
                        "Barcelona Red Metallic||Red|",
                        "Black||Black|",
                        "Magnetic Gray Metallic||Gray|",
                        "Nautical Blue Metallic||Blue|",
                        "Pyrite Mica||Gray|",
                        "Radiant Red||Red|",
                        "Silver Sky Metallic||Silver|",
                        "Spruce Mica||Green|",
                        "Super White||White|"
                    }));
        }

        [TestCase("39", "", "13", "", "39|39|Indigo Blue Metallic/13|13|Sparkle Silver", Description = "Should find when only code supplied")]
        [TestCase("", "Onyx Black", "", "Competition Yellow", "41|41|Onyx Black/59|59|Competition Yellow", Description = "Should find when only desc supplied")]
        [TestCase("", "onyx black", "", "COMPETITION YELLOW", "41|41|Onyx Black/59|59|Competition Yellow", Description = "Description matching should be case insensitive")]
        public void FirstOrDefault_on_exterior_color_list_should_match_by_code_and_then_by_description_for_style_976(
            string ext1Code, string ext1Desc, string ext2Code, string ext2Desc,
            string expectedColorKey)
        {
            var raw = GetColorsForStyle976();
            var style = new Style(colors: raw);
            var exteriorColors = style.GetExteriorColors().ToArray();

            var matchedColor = exteriorColors.FirstOrDefault(ext1Code, ext1Desc, ext2Code, ext2Desc);

            var matchedColorKey = matchedColor == null
                                      ? null
                                      : string.Format("{0}|{1}|{2}/{3}|{4}|{5}",
                                                      matchedColor.Ext1Code, matchedColor.Ext1MfgCode,
                                                      matchedColor.Ext1Desc,
                                                      matchedColor.Ext2Code, matchedColor.Ext2MfgCode,
                                                      matchedColor.Ext2Desc);
            Assert.That(matchedColorKey, Is.EqualTo(expectedColorKey));
        }

        [TestCase("1g3", "", "", "", "02|1G3|Magnetic Gray Metallic/||", Description = "Code matching should be case insensitive")]
        [TestCase("6V4", "Nautical Blue Metallic", "", "", "0P|6V4|Spruce Mica/||", Description = "Should prefer a code match over a description match")]
        public void FirstOrDefault_on_exterior_color_list_should_match_by_code_and_then_by_description_for_style_332735(
            string ext1Code, string ext1Desc, string ext2Code, string ext2Desc,
            string expectedColorKey)
        {
            var raw = GetColorsForStyle332735();
            var style = new Style(colors: raw);
            var exteriorColors = style.GetExteriorColors()
                .OrderBy(c => c.Ext1Code).ThenBy(c => c.Ext1MfgCode).ThenBy(c => c.Ext1Desc)
                .ThenBy(c => c.Ext2Code).ThenBy(c => c.Ext2MfgCode).ThenBy(c => c.Ext2MfgCode)
                .ToArray();

            var matchedColor = exteriorColors.FirstOrDefault(ext1Code, ext1Desc, ext2Code, ext2Desc);

            var matchedColorKey = matchedColor == null
                                      ? null
                                      : string.Format("{0}|{1}|{2}/{3}|{4}|{5}",
                                                      matchedColor.Ext1Code, matchedColor.Ext1MfgCode,
                                                      matchedColor.Ext1Desc,
                                                      matchedColor.Ext2Code, matchedColor.Ext2MfgCode,
                                                      matchedColor.Ext2Desc);
            Assert.That(matchedColorKey, Is.EqualTo(expectedColorKey));
        }

        [TestCase("31", "", "", "", "31||Red Flame Metallic/||", Description = "Should match on Code if MfgCode is empty")]
        [TestCase(null, "Red Flame Metallic", "", "", "31||Red Flame Metallic/||", Description = "Should work with null values 1")]
        [TestCase("31", null, "", "", "31||Red Flame Metallic/||", Description = "Should work with null values 2")]
        [TestCase("31", "", null, "", "31||Red Flame Metallic/||", Description = "Should work with null values 3")]
        [TestCase("31", "", "", null, "31||Red Flame Metallic/||", Description = "Should work with null values 4")]
        [TestCase("3L5", "Radiant Red", "", "", "0R|3L5|Radiant Red/||")]
        [TestCase("", "Radiant Red2", "", "", "||Radiant Red2/||")]
        [TestCase("", "", "3L5", "Radiant Red3", "||/0R|3L5|Radiant Red3")]
        [TestCase("", "", "", "Radiant Red4", "||/||Radiant Red4")]
        public void FirstOrDefault_on_exterior_color_list_should_match_by_code_and_then_by_description_for_style_1376(
            string ext1Code, string ext1Desc, string ext2Code, string ext2Desc,
            string expectedColorKey)
        {
            var raw = GetColorsForStyle1376();
            var style = new Style(colors: raw);
            var exteriorColors = style.GetExteriorColors().ToArray();

            var matchedColor = exteriorColors.FirstOrDefault(ext1Code, ext1Desc, ext2Code, ext2Desc);

            var matchedColorKey = matchedColor == null
                                      ? null
                                      : string.Format("{0}|{1}|{2}/{3}|{4}|{5}",
                                                      matchedColor.Ext1Code, matchedColor.Ext1MfgCode,
                                                      matchedColor.Ext1Desc,
                                                      matchedColor.Ext2Code, matchedColor.Ext2MfgCode,
                                                      matchedColor.Ext2Desc);
            Assert.That(matchedColorKey, Is.EqualTo(expectedColorKey));
        }

        [Test]
        public void GetInteriorColors_should_return_unique_colors_for_style_976()
        {
            var raw = GetColorsForStyle976();
            var style = new Style(colors: raw);

            var interiorColors = style.GetInteriorColors().ToArray();

            Assert.That(
                interiorColors.Select(c => c.Desc).ToArray(),
                Is.EquivalentTo(new[]
                    {
                        "Graphite",
                        "Medium Gray"
                    }));
        }

        [TestCase("12", "", "12|12|Graphite", Description = "Search by Code")]
        [TestCase("LA20", "", "1O|LA20|Black", Description = "Only use MfgCode if non-empty, otherwise Code")]
        [TestCase("M50", "", "|M50|Purple", Description = "Should match on MfgCode")]
        [TestCase("Z10", "", "Z10||Cyan", Description = "Should match on Code")]
        [TestCase("Z5", "", "Z5||Vols Orange", Description = "Null underlying MfgCode")]
        [TestCase("UT3", "", "|UT3|UT Orange", Description = "Null underlying Code")]
        [TestCase("NULLDESC", null, "NULLDESC|NULLDESC|")]
        [TestCase("NODESC", null, "NODESC|NODESC|")]
        [TestCase("ut3", "", "|UT3|UT Orange", Description = "Should match Code ignoring case")]
        [TestCase("", "Medium Gray", "92|92|Medium Gray", Description = "Match by Desc")]
        [TestCase("", "Sand Beige", "1P|LA44|Sand Beige", Description = "Match by Desc")]
        [TestCase("", "sand BEIGE", "1P|LA44|Sand Beige", Description = "Should match Desc ignoring case")]
        [TestCase("LA44", "Graphite", "1P|LA44|Sand Beige", Description = "Should prefer Code/MfgCode over Desc")]
        [TestCase("", "Vols Orange", "Z5||Vols Orange", Description = "Shouldn't match on empty codes")]
        [TestCase("FFF", "", null, Description = "Shouldn't match on empty desc")]
        public void FirstOrDefault_on_interior_color_list_should_match_by_code_and_then_by_description(
            string code, string desc, string expectedColorKey)
        {
            var interiorColors = new[]
                {
                    new Color(intCode: "12", intManCode: "12", intDesc: "Graphite"),
                    new Color(intCode: "92", intManCode: "92", intDesc: "Medium Gray"),
                    new Color(intManCode: "LA13", intCode: "1N", intDesc: "Graphite"),
                    new Color(intManCode: "LA20", intCode: "1O", intDesc: "Black"),
                    new Color(intManCode: "LA44", intCode: "1P", intDesc: "Sand Beige"),
                    new Color(intManCode: "M50", intCode: "", intDesc: "Purple"),
                    new Color(intManCode: "GRN", intCode: "GRN", intDesc: ""),
                    new Color(intDesc: "Red"),
                    new Color(intManCode: "", intCode: "Z10", intDesc: "Cyan"),
                    new Color(intManCode: null, intCode: "Z5", intDesc: "Vols Orange"),
                    new Color(intManCode: "UT3", intCode: null, intDesc: "UT Orange"),
                    new Color(intManCode: "NULLDESC", intCode: "NULLDESC"),
                    new Color(intManCode: "NODESC", intCode: "NODESC", intDesc: "")
                };

            var matchedColor = interiorColors.FirstOrDefault(code, desc);

            var matchedColorKey = matchedColor == null
                                      ? null
                                      : string.Format("{0}|{1}|{2}", matchedColor.Code, matchedColor.MfgCode,
                                                      matchedColor.Desc);
            Assert.That(matchedColorKey, Is.EqualTo(expectedColorKey));
        }

        private static Color[] GetColorsForStyle976()
        {
            return new[] {
                new Color(ext1Code: "01", ext1ManCode: "01", ext1Desc: "Special Paint", genericExtColor: "Non-Color", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "01", ext1ManCode: "01", ext1Desc: "Special Paint", genericExtColor: "Non-Color", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "11", ext1ManCode: "11", ext1Desc: "Light Pewter Metallic", genericExtColor: "Gold", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "11", ext1ManCode: "11", ext1Desc: "Light Pewter Metallic", genericExtColor: "Gold", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "39", ext1ManCode: "39", ext1Desc: "Indigo Blue Metallic", genericExtColor: "Blue", ext2Code: "13", ext2ManCode: "13", ext2Desc: "Sparkle Silver", genericExt2Color: "Silver", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "39", ext1ManCode: "39", ext1Desc: "Indigo Blue Metallic", genericExtColor: "Blue", ext2Code: "13", ext2ManCode: "13", ext2Desc: "Sparkle Silver", genericExt2Color: "Silver", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "39", ext1ManCode: "39", ext1Desc: "Indigo Blue Metallic", genericExtColor: "Blue", ext2Code: "38", ext2ManCode: "38", ext2Desc: "Red Flames", genericExt2Color: "Red", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "39", ext1ManCode: "39", ext1Desc: "Indigo Blue Metallic", genericExtColor: "Blue", ext2Code: "38", ext2ManCode: "38", ext2Desc: "Red Flames", genericExt2Color: "Red", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "39", ext1ManCode: "39", ext1Desc: "Indigo Blue Metallic", genericExtColor: "Blue", ext2Code: "41", ext2ManCode: "41", ext2Desc: "Onyx Black", genericExt2Color: "Black", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "39", ext1ManCode: "39", ext1Desc: "Indigo Blue Metallic", genericExtColor: "Blue", ext2Code: "41", ext2ManCode: "41", ext2Desc: "Onyx Black", genericExt2Color: "Black", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "39", ext1ManCode: "39", ext1Desc: "Indigo Blue Metallic", genericExtColor: "Blue", ext2Code: "54", ext2ManCode: "54", ext2Desc: "Gold", genericExt2Color: "Gold", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "39", ext1ManCode: "39", ext1Desc: "Indigo Blue Metallic", genericExtColor: "Blue", ext2Code: "54", ext2ManCode: "54", ext2Desc: "Gold", genericExt2Color: "Gold", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "39", ext1ManCode: "39", ext1Desc: "Indigo Blue Metallic", genericExtColor: "Blue", ext2Code: "59", ext2ManCode: "59", ext2Desc: "Competition Yellow", genericExt2Color: "Yellow", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "39", ext1ManCode: "39", ext1Desc: "Indigo Blue Metallic", genericExtColor: "Blue", ext2Code: "59", ext2ManCode: "59", ext2Desc: "Competition Yellow", genericExt2Color: "Yellow", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "39", ext1ManCode: "39", ext1Desc: "Indigo Blue Metallic", genericExtColor: "Blue", ext2Code: "70", ext2ManCode: "70", ext2Desc: "Torch Red", genericExt2Color: "Red", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "39", ext1ManCode: "39", ext1Desc: "Indigo Blue Metallic", genericExtColor: "Blue", ext2Code: "70", ext2ManCode: "70", ext2Desc: "Torch Red", genericExt2Color: "Red", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "39", ext1ManCode: "39", ext1Desc: "Indigo Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "39", ext1ManCode: "39", ext1Desc: "Indigo Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "39", ext1ManCode: "39", ext1Desc: "Indigo Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "39", ext1ManCode: "39", ext1Desc: "Indigo Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "41", ext1ManCode: "41", ext1Desc: "Onyx Black", genericExtColor: "Black", ext2Code: "13", ext2ManCode: "13", ext2Desc: "Sparkle Silver", genericExt2Color: "Silver", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "41", ext1ManCode: "41", ext1Desc: "Onyx Black", genericExtColor: "Black", ext2Code: "13", ext2ManCode: "13", ext2Desc: "Sparkle Silver", genericExt2Color: "Silver", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "41", ext1ManCode: "41", ext1Desc: "Onyx Black", genericExtColor: "Black", ext2Code: "38", ext2ManCode: "38", ext2Desc: "Red Flames", genericExt2Color: "Red", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "41", ext1ManCode: "41", ext1Desc: "Onyx Black", genericExtColor: "Black", ext2Code: "38", ext2ManCode: "38", ext2Desc: "Red Flames", genericExt2Color: "Red", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "41", ext1ManCode: "41", ext1Desc: "Onyx Black", genericExtColor: "Black", ext2Code: "54", ext2ManCode: "54", ext2Desc: "Gold", genericExt2Color: "Gold", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "41", ext1ManCode: "41", ext1Desc: "Onyx Black", genericExtColor: "Black", ext2Code: "54", ext2ManCode: "54", ext2Desc: "Gold", genericExt2Color: "Gold", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "41", ext1ManCode: "41", ext1Desc: "Onyx Black", genericExtColor: "Black", ext2Code: "59", ext2ManCode: "59", ext2Desc: "Competition Yellow", genericExt2Color: "Yellow", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "41", ext1ManCode: "41", ext1Desc: "Onyx Black", genericExtColor: "Black", ext2Code: "59", ext2ManCode: "59", ext2Desc: "Competition Yellow", genericExt2Color: "Yellow", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "41", ext1ManCode: "41", ext1Desc: "Onyx Black", genericExtColor: "Black", ext2Code: "70", ext2ManCode: "70", ext2Desc: "Torch Red", genericExt2Color: "Red", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "41", ext1ManCode: "41", ext1Desc: "Onyx Black", genericExtColor: "Black", ext2Code: "70", ext2ManCode: "70", ext2Desc: "Torch Red", genericExt2Color: "Red", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "41", ext1ManCode: "41", ext1Desc: "Onyx Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "41", ext1ManCode: "41", ext1Desc: "Onyx Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "41", ext1ManCode: "41", ext1Desc: "Onyx Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "41", ext1ManCode: "41", ext1Desc: "Onyx Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "43", ext1ManCode: "43", ext1Desc: "Yellow", genericExtColor: "Yellow", ext2Code: "13", ext2ManCode: "13", ext2Desc: "Sparkle Silver", genericExt2Color: "Silver", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "43", ext1ManCode: "43", ext1Desc: "Yellow", genericExtColor: "Yellow", ext2Code: "13", ext2ManCode: "13", ext2Desc: "Sparkle Silver", genericExt2Color: "Silver", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "43", ext1ManCode: "43", ext1Desc: "Yellow", genericExtColor: "Yellow", ext2Code: "38", ext2ManCode: "38", ext2Desc: "Red Flames", genericExt2Color: "Red", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "43", ext1ManCode: "43", ext1Desc: "Yellow", genericExtColor: "Yellow", ext2Code: "38", ext2ManCode: "38", ext2Desc: "Red Flames", genericExt2Color: "Red", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "43", ext1ManCode: "43", ext1Desc: "Yellow", genericExtColor: "Yellow", ext2Code: "41", ext2ManCode: "41", ext2Desc: "Onyx Black", genericExt2Color: "Black", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "43", ext1ManCode: "43", ext1Desc: "Yellow", genericExtColor: "Yellow", ext2Code: "41", ext2ManCode: "41", ext2Desc: "Onyx Black", genericExt2Color: "Black", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "43", ext1ManCode: "43", ext1Desc: "Yellow", genericExtColor: "Yellow", ext2Code: "70", ext2ManCode: "70", ext2Desc: "Torch Red", genericExt2Color: "Red", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "43", ext1ManCode: "43", ext1Desc: "Yellow", genericExtColor: "Yellow", ext2Code: "70", ext2ManCode: "70", ext2Desc: "Torch Red", genericExt2Color: "Red", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "43", ext1ManCode: "43", ext1Desc: "Yellow", genericExtColor: "Yellow", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "43", ext1ManCode: "43", ext1Desc: "Yellow", genericExtColor: "Yellow", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "47", ext1ManCode: "47", ext1Desc: "Dark Green Metallic", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "47", ext1ManCode: "47", ext1Desc: "Dark Green Metallic", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "50", ext1ManCode: "50", ext1Desc: "Summit White", genericExtColor: "White", ext2Code: "13", ext2ManCode: "13", ext2Desc: "Sparkle Silver", genericExt2Color: "Silver", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "50", ext1ManCode: "50", ext1Desc: "Summit White", genericExtColor: "White", ext2Code: "13", ext2ManCode: "13", ext2Desc: "Sparkle Silver", genericExt2Color: "Silver", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "50", ext1ManCode: "50", ext1Desc: "Summit White", genericExtColor: "White", ext2Code: "38", ext2ManCode: "38", ext2Desc: "Red Flames", genericExt2Color: "Red", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "50", ext1ManCode: "50", ext1Desc: "Summit White", genericExtColor: "White", ext2Code: "38", ext2ManCode: "38", ext2Desc: "Red Flames", genericExt2Color: "Red", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "50", ext1ManCode: "50", ext1Desc: "Summit White", genericExtColor: "White", ext2Code: "41", ext2ManCode: "41", ext2Desc: "Onyx Black", genericExt2Color: "Black", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "50", ext1ManCode: "50", ext1Desc: "Summit White", genericExtColor: "White", ext2Code: "41", ext2ManCode: "41", ext2Desc: "Onyx Black", genericExt2Color: "Black", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "50", ext1ManCode: "50", ext1Desc: "Summit White", genericExtColor: "White", ext2Code: "54", ext2ManCode: "54", ext2Desc: "Gold", genericExt2Color: "Gold", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "50", ext1ManCode: "50", ext1Desc: "Summit White", genericExtColor: "White", ext2Code: "54", ext2ManCode: "54", ext2Desc: "Gold", genericExt2Color: "Gold", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "50", ext1ManCode: "50", ext1Desc: "Summit White", genericExtColor: "White", ext2Code: "70", ext2ManCode: "70", ext2Desc: "Torch Red", genericExt2Color: "Red", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "50", ext1ManCode: "50", ext1Desc: "Summit White", genericExtColor: "White", ext2Code: "70", ext2ManCode: "70", ext2Desc: "Torch Red", genericExt2Color: "Red", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "50", ext1ManCode: "50", ext1Desc: "Summit White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "50", ext1ManCode: "50", ext1Desc: "Summit White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "50", ext1ManCode: "50", ext1Desc: "Summit White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "50", ext1ManCode: "50", ext1Desc: "Summit White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "58", ext1ManCode: "58", ext1Desc: "Sandalwood Metallic", genericExtColor: "Gold", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "58", ext1ManCode: "58", ext1Desc: "Sandalwood Metallic", genericExtColor: "Gold", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "74", ext1ManCode: "74", ext1Desc: "Victory Red", genericExtColor: "Red", ext2Code: "13", ext2ManCode: "13", ext2Desc: "Sparkle Silver", genericExt2Color: "Silver", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "74", ext1ManCode: "74", ext1Desc: "Victory Red", genericExtColor: "Red", ext2Code: "13", ext2ManCode: "13", ext2Desc: "Sparkle Silver", genericExt2Color: "Silver", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "74", ext1ManCode: "74", ext1Desc: "Victory Red", genericExtColor: "Red", ext2Code: "38", ext2ManCode: "38", ext2Desc: "Red Flames", genericExt2Color: "Red", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "74", ext1ManCode: "74", ext1Desc: "Victory Red", genericExtColor: "Red", ext2Code: "38", ext2ManCode: "38", ext2Desc: "Red Flames", genericExt2Color: "Red", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "74", ext1ManCode: "74", ext1Desc: "Victory Red", genericExtColor: "Red", ext2Code: "41", ext2ManCode: "41", ext2Desc: "Onyx Black", genericExt2Color: "Black", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "74", ext1ManCode: "74", ext1Desc: "Victory Red", genericExtColor: "Red", ext2Code: "41", ext2ManCode: "41", ext2Desc: "Onyx Black", genericExt2Color: "Black", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "74", ext1ManCode: "74", ext1Desc: "Victory Red", genericExtColor: "Red", ext2Code: "59", ext2ManCode: "59", ext2Desc: "Competition Yellow", genericExt2Color: "Yellow", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "74", ext1ManCode: "74", ext1Desc: "Victory Red", genericExtColor: "Red", ext2Code: "59", ext2ManCode: "59", ext2Desc: "Competition Yellow", genericExt2Color: "Yellow", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "74", ext1ManCode: "74", ext1Desc: "Victory Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "74", ext1ManCode: "74", ext1Desc: "Victory Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "74", ext1ManCode: "74", ext1Desc: "Victory Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "74", ext1ManCode: "74", ext1Desc: "Victory Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "94", ext1ManCode: "94", ext1Desc: "Dark Cherry Red Metallic", genericExtColor: "Red", ext2Code: "13", ext2ManCode: "13", ext2Desc: "Sparkle Silver", genericExt2Color: "Silver", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "94", ext1ManCode: "94", ext1Desc: "Dark Cherry Red Metallic", genericExtColor: "Red", ext2Code: "13", ext2ManCode: "13", ext2Desc: "Sparkle Silver", genericExt2Color: "Silver", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "94", ext1ManCode: "94", ext1Desc: "Dark Cherry Red Metallic", genericExtColor: "Red", ext2Code: "38", ext2ManCode: "38", ext2Desc: "Red Flames", genericExt2Color: "Red", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "94", ext1ManCode: "94", ext1Desc: "Dark Cherry Red Metallic", genericExtColor: "Red", ext2Code: "38", ext2ManCode: "38", ext2Desc: "Red Flames", genericExt2Color: "Red", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "94", ext1ManCode: "94", ext1Desc: "Dark Cherry Red Metallic", genericExtColor: "Red", ext2Code: "41", ext2ManCode: "41", ext2Desc: "Onyx Black", genericExt2Color: "Black", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "94", ext1ManCode: "94", ext1Desc: "Dark Cherry Red Metallic", genericExtColor: "Red", ext2Code: "41", ext2ManCode: "41", ext2Desc: "Onyx Black", genericExt2Color: "Black", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "94", ext1ManCode: "94", ext1Desc: "Dark Cherry Red Metallic", genericExtColor: "Red", ext2Code: "54", ext2ManCode: "54", ext2Desc: "Gold", genericExt2Color: "Gold", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "94", ext1ManCode: "94", ext1Desc: "Dark Cherry Red Metallic", genericExtColor: "Red", ext2Code: "54", ext2ManCode: "54", ext2Desc: "Gold", genericExt2Color: "Gold", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "94", ext1ManCode: "94", ext1Desc: "Dark Cherry Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "94", ext1ManCode: "94", ext1Desc: "Dark Cherry Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "12", intCode: "12", intDesc: "Graphite"),
                new Color(ext1Code: "94", ext1ManCode: "94", ext1Desc: "Dark Cherry Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "92", intCode: "92", intDesc: "Medium Gray"),
                new Color(ext1Code: "94", ext1ManCode: "94", ext1Desc: "Dark Cherry Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "", intManCode: "92", intCode: "92", intDesc: "Medium Gray")
            };
        }

        private static Color[] GetColorsForStyle332735()
        {
            return new[]
                {
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "02", ext1ManCode: "1G3", ext1Desc: "Magnetic Gray Metallic", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "02", ext1ManCode: "1G3", ext1Desc: "Magnetic Gray Metallic", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "02", ext1ManCode: "1G3", ext1Desc: "Magnetic Gray Metallic", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "02", ext1ManCode: "1G3", ext1Desc: "Magnetic Gray Metallic", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "02", ext1ManCode: "1G3", ext1Desc: "Magnetic Gray Metallic", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "02", ext1ManCode: "1G3", ext1Desc: "Magnetic Gray Metallic", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "02", ext1ManCode: "1G3", ext1Desc: "Magnetic Gray Metallic", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "02", ext1ManCode: "1G3", ext1Desc: "Magnetic Gray Metallic", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "02", ext1ManCode: "1G3", ext1Desc: "Magnetic Gray Metallic", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "02", ext1ManCode: "1G3", ext1Desc: "Magnetic Gray Metallic", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "02", ext1ManCode: "1G3", ext1Desc: "Magnetic Gray Metallic", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "02", ext1ManCode: "1G3", ext1Desc: "Magnetic Gray Metallic", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "02", ext1ManCode: "1G3", ext1Desc: "Magnetic Gray Metallic", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "02", ext1ManCode: "1G3", ext1Desc: "Magnetic Gray Metallic", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "02", ext1ManCode: "1G3", ext1Desc: "Magnetic Gray Metallic", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "02", ext1ManCode: "1G3", ext1Desc: "Magnetic Gray Metallic", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "02", ext1ManCode: "1G3", ext1Desc: "Magnetic Gray Metallic", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "02", ext1ManCode: "1G3", ext1Desc: "Magnetic Gray Metallic", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "02", ext1ManCode: "1G3", ext1Desc: "Magnetic Gray Metallic", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "02", ext1ManCode: "1G3", ext1Desc: "Magnetic Gray Metallic", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "02", ext1ManCode: "1G3", ext1Desc: "Magnetic Gray Metallic", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "08", ext1ManCode: "1D6", ext1Desc: "Silver Sky Metallic", genericExtColor: "Silver", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "08", ext1ManCode: "1D6", ext1Desc: "Silver Sky Metallic", genericExtColor: "Silver", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "08", ext1ManCode: "1D6", ext1Desc: "Silver Sky Metallic", genericExtColor: "Silver", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "08", ext1ManCode: "1D6", ext1Desc: "Silver Sky Metallic", genericExtColor: "Silver", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "08", ext1ManCode: "1D6", ext1Desc: "Silver Sky Metallic", genericExtColor: "Silver", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "08", ext1ManCode: "1D6", ext1Desc: "Silver Sky Metallic", genericExtColor: "Silver", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "08", ext1ManCode: "1D6", ext1Desc: "Silver Sky Metallic", genericExtColor: "Silver", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "08", ext1ManCode: "1D6", ext1Desc: "Silver Sky Metallic", genericExtColor: "Silver", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "08", ext1ManCode: "1D6", ext1Desc: "Silver Sky Metallic", genericExtColor: "Silver", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "08", ext1ManCode: "1D6", ext1Desc: "Silver Sky Metallic", genericExtColor: "Silver", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "08", ext1ManCode: "1D6", ext1Desc: "Silver Sky Metallic", genericExtColor: "Silver", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "08", ext1ManCode: "1D6", ext1Desc: "Silver Sky Metallic", genericExtColor: "Silver", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "08", ext1ManCode: "1D6", ext1Desc: "Silver Sky Metallic", genericExtColor: "Silver", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "08", ext1ManCode: "1D6", ext1Desc: "Silver Sky Metallic", genericExtColor: "Silver", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "08", ext1ManCode: "1D6", ext1Desc: "Silver Sky Metallic", genericExtColor: "Silver", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "08", ext1ManCode: "1D6", ext1Desc: "Silver Sky Metallic", genericExtColor: "Silver", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "08", ext1ManCode: "1D6", ext1Desc: "Silver Sky Metallic", genericExtColor: "Silver", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "08", ext1ManCode: "1D6", ext1Desc: "Silver Sky Metallic", genericExtColor: "Silver", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "08", ext1ManCode: "1D6", ext1Desc: "Silver Sky Metallic", genericExtColor: "Silver", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: "")
                };
        }

        private Color[] GetColorsForStyle1376()
        {
            return new[]
                {
                    new Color(ext1Code: "09", ext1ManCode: "", ext1Desc: "Black Sand Pearl", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "09", ext1ManCode: "", ext1Desc: "Black Sand Pearl", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "09", ext1ManCode: "", ext1Desc: "Black Sand Pearl", genericExtColor: "Black", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "16", ext1ManCode: "", ext1Desc: "Indigo Ink", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "16", ext1ManCode: "", ext1Desc: "Indigo Ink", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "16", ext1ManCode: "", ext1Desc: "Indigo Ink", genericExtColor: "Blue", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "17", ext1ManCode: "", ext1Desc: "Lunar Mist Metallic", genericExtColor: "Silver", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "17", ext1ManCode: "", ext1Desc: "Lunar Mist Metallic", genericExtColor: "Silver", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "20", ext1ManCode: "", ext1Desc: "Diamond White Pearl", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "20", ext1ManCode: "", ext1Desc: "Diamond White Pearl", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "20", ext1ManCode: "", ext1Desc: "Diamond White Pearl", genericExtColor: "White", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "29", ext1ManCode: "", ext1Desc: "Rainforest Pearl", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "29", ext1ManCode: "", ext1Desc: "Rainforest Pearl", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "29", ext1ManCode: "", ext1Desc: "Rainforest Pearl", genericExtColor: "Green", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "31", ext1ManCode: "", ext1Desc: "Red Flame Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "31", ext1ManCode: "", ext1Desc: "Red Flame Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "31", ext1ManCode: "", ext1Desc: "Red Flame Metallic", genericExtColor: "Red", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    new Color(ext1Code: "32", ext1ManCode: "", ext1Desc: "Gold Dust Metallic", genericExtColor: "Gold", ext2Code: "", ext2ManCode: "", ext2Desc: "", genericExt2Color: ""),
                    
                    // Extra item to test scenario with nulls
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red"),
                    new Color(ext1Code: null, ext1ManCode: null, ext1Desc: "Radiant Red2", genericExtColor: "Red"),
                    new Color(ext2Code: "0R", ext2ManCode: "3L5", ext2Desc: "Radiant Red3", genericExt2Color: "Red"),
                    new Color(ext2Code: null, ext2ManCode: null, ext2Desc: "Radiant Red4", genericExt2Color: "Red")
                };
        }
    }
}