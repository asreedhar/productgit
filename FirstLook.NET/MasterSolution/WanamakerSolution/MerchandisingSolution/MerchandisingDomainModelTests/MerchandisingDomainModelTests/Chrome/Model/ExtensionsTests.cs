﻿using FirstLook.Merchandising.DomainModel.Chrome.Model;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Chrome.Model
{
    [TestFixture]
    public class ExtensionsTests
    {
        [TestCase("4dr Sdn", "S", "S - 4dr Sdn")]
        [TestCase("4dr Sdn AWD", "", "base - 4dr Sdn AWD")]
        [TestCase("4dr Sdn AWD", null, "base - 4dr Sdn AWD")]
        [TestCase("", "S", "S")]
        [TestCase(null, "S", "S")]
        [TestCase(null, null, "base")]
        public void GetStyleDesc_should_return_expected_results(string styleNameWOTrim, string trim, string expectedDesc)
        {
            var style = new Style(styleNameWOTrim: styleNameWOTrim, trim: trim);

            var desc = style.GetStyleDesc();

            Assert.That(desc, Is.EqualTo(expectedDesc));
        }
    }
}