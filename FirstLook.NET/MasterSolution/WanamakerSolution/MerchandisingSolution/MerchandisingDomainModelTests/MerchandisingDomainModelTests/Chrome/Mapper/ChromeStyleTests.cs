﻿using System.Collections.Generic;
using System.Linq;
using Autofac;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Chrome.Mapper;
using FirstLook.Merchandising.DomainModel.Vehicles;
using Moq;
using NUnit.Framework;
using ICache = FirstLook.Common.Core.ICache;

namespace MerchandisingDomainModelTests.Chrome.Mapper
{
    [TestFixture]
    public class ChromeStyleTests
    {
        private Mock<IChromeMapper> ChromeMapperMock;
        // This VIN1 was chosen because the vin pattern resolves to two distinct chrome style ids which can only
        // be distinguished by the full style code
        private const string VIN1 = "WBAKB8C58BCY65872";
        private const string VIN2 = "WBAKB8C55BCY64727";
        private const string VIN3 = "WMWZF3C51BTX82242";
        private const int VinPatternId1 = 109300; // VinPatternId1 for the specified VIN1.
        private const int VinPatternId2 = 109300;
        private const int VinPatternId3 = 114608;
        private const int STYLE_ID = 324322;
        private const int NO_STYLE = -1;

        [SetUp]
        public void Setup()
        {
            ChromeMapperMock = new Mock<IChromeMapper>();
            ChromeMapperMock.Setup(m => m.LookupVinPatternIdByVin(VIN1)).Returns(VinPatternId1);
            ChromeMapperMock.Setup(m => m.LookupVinPatternIdByVin(VIN2)).Returns(VinPatternId2);
            ChromeMapperMock.Setup(m => m.LookupVinPatternIdByVin(VIN3)).Returns(VinPatternId3);

            var b = new ContainerBuilder();
            b.RegisterType<NullCache>().As<ICache>();
            b.Register(c => ChromeMapperMock.Object).As<IChromeMapper>();
            var container = b.Build();
            Registry.RegisterContainer(container);
        }

        [TearDown]
        public void Teardown()
        {
            Registry.Reset();
        }


        #region GetFullOemStyleCode

        [Test]
        public void GetFullOemStyleCodeReturnsEmptyWhenPassedNullOrEmpty()
        {
            var e = ChromeStyle.GetFullOemStyleCode(string.Empty);
            Assert.AreEqual(string.Empty, e);

            var n = ChromeStyle.GetFullOemStyleCode(null);
            Assert.AreEqual(string.Empty, n);
        }

        [Test]
        public void GetFullOemStyleCodeSplitsOnSpaces()
        {
            string input = "a b";
            var r = ChromeStyle.GetFullOemStyleCode(input);
            Assert.AreEqual("a", r);
        }

        [Test]
        public void GetFullOemStyleCodeSplitsOnDashes()
        {
            string input = "a-b";
            var r = ChromeStyle.GetFullOemStyleCode(input);
            Assert.AreEqual("a", r);            
        }

        [Test]
        public void GetFullOemStyleCodeSplitsOnDashesAboveSpaces()
        {
            string input = "a b - c";
            var r = ChromeStyle.GetFullOemStyleCode(input);
            Assert.AreEqual("a b", r);

            input = "a - b";
            r = ChromeStyle.GetFullOemStyleCode(input);
            Assert.AreEqual("a", r);
        }

        #endregion

        #region GetByStyleId

       
        [Test]
        public void GetByStyleIdBoundary()
        {
            var styles = ChromeStyle.GetByStyleId(NO_STYLE);
            Assert.AreEqual(0, styles.Count);

            styles = ChromeStyle.GetByStyleId(0);
            Assert.AreEqual(0, styles.Count);
        }

        #endregion

        #region InferStyleByModelAndOptions

        [Test]
        public void InferStyleByModelAndOptionsBoundary()
        {
            var val = ChromeStyle.InferStyleByModelAndOptions(null, null, null);
            Assert.IsNull(val);

            val = ChromeStyle.InferStyleByModelAndOptions(string.Empty, null, null);
            Assert.IsNull(val);

            val = ChromeStyle.InferStyleByModelAndOptions(string.Empty, string.Empty, null);
            Assert.IsNull(val);

            val = ChromeStyle.InferStyleByModelAndOptions(null, string.Empty, null);
            Assert.IsNull(val);
        }


        #endregion
      

        [Test]
        public void IsValid()
        {
            Assert.IsFalse(ChromeStyle.IsValid(-1));
            Assert.IsFalse(ChromeStyle.IsValid(0));
            Assert.IsTrue(ChromeStyle.IsValid(1));



            Assert.IsFalse(ChromeStyle.IsValid(null));

            var mockStyle_1 = new Mock<IChromeStyle>();
            mockStyle_1.SetupGet(s => s.ChromeStyleID).Returns(-1);
            Assert.IsFalse(ChromeStyle.IsValid(mockStyle_1.Object));

            var mockStyle0 = new Mock<IChromeStyle>();
            mockStyle0.SetupGet(s => s.ChromeStyleID).Returns(0);
            Assert.IsFalse(ChromeStyle.IsValid(mockStyle0.Object));

            var mockStyle1 = new Mock<IChromeStyle>();
            mockStyle0.SetupGet(s => s.ChromeStyleID).Returns(1);
            Assert.IsFalse(ChromeStyle.IsValid(mockStyle1.Object));

            // Mock an IVehicleConfiguration
            var mockUnknownConfig1 = new Mock<IChromeStyle>();
            mockUnknownConfig1.SetupGet(vc => vc.ChromeStyleID).Returns(0);
            Assert.IsFalse(ChromeStyle.IsValid(mockUnknownConfig1.Object));

            var mockUnknownConfig2 = new Mock<IChromeStyle>();
            mockUnknownConfig2.SetupGet(vc => vc.ChromeStyleID).Returns(-1);
            Assert.IsFalse(ChromeStyle.IsValid(mockUnknownConfig2.Object));

        }

        public void StylesMatch()
        {
            // Not sure how unknown styles should compare
            Assert.IsTrue(ChromeStyle.StylesMatch(0, 0));
            Assert.IsFalse(ChromeStyle.StylesMatch(-1, 0));

            Assert.IsFalse(ChromeStyle.StylesMatch(null, null));

            Mock<IChromeStyle> mockStyle = new Mock<IChromeStyle>();
            mockStyle.SetupGet(s => s.ChromeStyleID).Returns(0);
            Assert.IsTrue( ChromeStyle.StylesMatch(mockStyle.Object, 0));

            Assert.IsTrue(ChromeStyle.StylesMatch(0, 0));
            Assert.IsTrue(ChromeStyle.StylesMatch(-1, -1));
            Assert.IsTrue(ChromeStyle.StylesMatch(1, 1));
            Assert.IsFalse(ChromeStyle.StylesMatch(1, 2));
            Assert.IsFalse(ChromeStyle.StylesMatch(0, 1));
            Assert.IsFalse(ChromeStyle.StylesMatch(-1, 1));


        }

    }
    
}
