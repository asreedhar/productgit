﻿using FirstLook.Merchandising.DomainModel.Chrome.Mapper;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Chrome.Mapper
{
    [TestFixture]
    public class VinPartComparerTests
    {
        [TestCase(7, 2, 11)]
        [TestCase(9, 0, 9)]
        public void Equals_with_equivalent_byte_sequence_should_return_true(int length, int indexA, int indexB)
        {
            var bytes = new byte[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 0, 1, 2, 3, 4, 5, 6, 7, 8};

            var comparer = new VinPartComparer(length);

            var vinPartA = new VinPart(bytes, indexA);
            var vinPartB = new VinPart(bytes, indexB);

            Assert.That(comparer.Equals(vinPartA, vinPartB), Is.True);
        }

        [TestCase(7, 2, 4)]
        [TestCase(9, 4, 5)]
        public void Equals_with_inequivalent_byte_sequence_should_return_false(int length, int indexA, int indexB)
        {
            var bytes = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 0, 1, 2, 3, 4, 5, 6, 7, 8 };

            var comparer = new VinPartComparer(length);

            var vinPartA = new VinPart(bytes, indexA);
            var vinPartB = new VinPart(bytes, indexB);

            Assert.That(comparer.Equals(vinPartA, vinPartB), Is.False);
        }

        [TestCase(7, 0, 9)]
        [TestCase(7, 1, 10)]
        [TestCase(7, 2, 11)]
        [TestCase(9, 0, 9)]
        public void GetHashcode_of_equal_byte_sequences_should_return_equal_hash_codes(int length, int indexA, int indexB)
        {
            var bytes = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 0, 1, 2, 3, 4, 5, 6, 7, 8 };

            var comparer = new VinPartComparer(length);

            var vinPartA = new VinPart(bytes, indexA);
            var vinPartB = new VinPart(bytes, indexB);

            Assert.That(comparer.GetHashCode(vinPartA), Is.EqualTo(comparer.GetHashCode(vinPartB)));
        }

        [TestCase(7, 0, 5)]
        [TestCase(7, 3, 9)]
        [TestCase(9, 0, 6)]
        public void GetHashCode_of_inequal_byte_sequences_should_return_different_hash_codes(int length, int indexA, int indexB)
        {
            var bytes = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 0, 1, 2, 3, 4, 5, 6, 7, 8 };

            var comparer = new VinPartComparer(length);

            var vinPartA = new VinPart(bytes, indexA);
            var vinPartB = new VinPart(bytes, indexB);

            Assert.That(comparer.GetHashCode(vinPartA), Is.Not.EqualTo(comparer.GetHashCode(vinPartB)));
        }
    }
}