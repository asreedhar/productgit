﻿using System.Collections.Generic;
using FirstLook.Common.Core;
using FirstLook.Merchandising.DomainModel.Chrome.Mapper;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Chrome.Mapper
{
    [TestFixture]
    public class ChromeStyleMapCachedRepositoryTests
    {
        private Mock<IMemoryCache> CacheMock;
        private IMemoryCache Cache;
        private Mock<IChromeMapRepository> InnerMock;
        private IChromeMapRepository Inner;
        private Mock<IChromeMapData> DataMock;
        private IChromeMapData Data;

        [SetUp]
        public void Setup()
        {
            CacheMock = new Mock<IMemoryCache>();
            Cache = CacheMock.Object;

            InnerMock = new Mock<IChromeMapRepository>();
            Inner = InnerMock.Object;

            DataMock = new Mock<IChromeMapData>();
            Data = DataMock.Object;
        }

        [Test]
        public void GetMapperData_should_call_inner_GetMapperData_when_data_not_in_cache()
        {
            var repo = new ChromeMapCachedRepository(Cache, Inner);

            repo.GetMapperData();

            InnerMock.Verify(i => i.GetMapperData(), Times.Once());
        }

        [Test]
        public void GetMapperData_should_store_data_in_cache_when_data_not_in_cache()
        {
            InnerMock.Setup(i => i.GetMapperData()).Returns(Data);

            var repo = new ChromeMapCachedRepository(Cache, Inner);

            repo.GetMapperData();

            CacheMock.Verify(c => c.Set(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<int?>()), Times.Once());
        }

        [Test]
        public void GetMapperData_should_utilize_cache_when_called_multiple_times()
        {
            var miniCache = new Dictionary<string, object>();
            CacheMock.Setup(c => c.Set(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<int?>()))
                .Callback<string, object, int?>((k, v, t) => miniCache[k] = v);
            CacheMock.Setup(c => c.Get(It.IsAny<string>()))
                .Returns<string>(k =>
                                     {
                                         object val;
                                         miniCache.TryGetValue(k, out val);
                                         return val;
                                     });
            InnerMock.Setup(i => i.GetMapperData()).Returns(Data);

            var repo = new ChromeMapCachedRepository(Cache, Inner);

            repo.GetMapperData();
            repo.GetMapperData();
            var data = repo.GetMapperData();


            InnerMock.Verify(i => i.GetMapperData(), Times.Once());
            Assert.That(data, Is.SameAs(Data));
        }

        [Test]
        public void GetMapperData_should_not_call_inner_GetMapperData_when_data_already_in_cache()
        {
            CacheMock.Setup(c => c.Get(It.IsAny<string>())).Returns(Data);

            var repo = new ChromeMapCachedRepository(Cache, Inner);

            repo.GetMapperData();

            InnerMock.Verify(i => i.GetMapperData(), Times.Never());
        }
    }
}