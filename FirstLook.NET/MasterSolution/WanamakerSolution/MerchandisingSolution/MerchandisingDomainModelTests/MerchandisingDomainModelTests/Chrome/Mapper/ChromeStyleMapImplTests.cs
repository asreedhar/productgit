﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Chrome.Mapper;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Chrome.Mapper
{
    [TestFixture]
    public class ChromeStyleMapImplTests
    {
        private Mock<IChromeMapRepository> RepoMock;
        private Mock<IChromeMapData> DataMock;
        private IChromeMapData Data;
        private IChromeMapRepository Repo;

        [SetUp]
        public void Setup()
        {
            RepoMock = new Mock<IChromeMapRepository>();
            DataMock = new Mock<IChromeMapData>();
            Repo = RepoMock.Object;
            Data = DataMock.Object;

            RepoMock.Setup(r => r.GetMapperData()).Returns(Data);
        }

        [TestCase(89, 7)]
        [TestCase(170, 22)]
        [TestCase(null, null, Description = "Given a null, should return a null")]
        [TestCase(42, null, Description = "Should return null when key is not present in lookup")]
        public void LookupChromeStyleIdByVehicleCatalogId_should_return_expected_result_given_VehicleCatalogId(
            int? vehicleCatalogId, int? expectedChromeStyleId)
        {
            var vehicleCatalogLookup = new Dictionary<int, int>
                                           {
                                               { 89, 7 },
                                               { 170, 22}
                                           };
            DataMock.Setup(d => d.VehicleCatalogLookup).Returns(vehicleCatalogLookup);

            var mapper = new ChromeMapper(Repo);

            var style = mapper.LookupChromeStyleIdByVehicleCatalogId(vehicleCatalogId);

            Assert.That(style, Is.EqualTo(expectedChromeStyleId));
        }

        [TestCase(null, null)]
        [TestCase("NOT_17_CHARS", null)]
        [TestCase("ABCDEFGH*I7890123", 200)]
        [TestCase("ABCDEFGH*I1234Z67", 400)]
        [TestCase("ABCDEFGH*I0123456", 100)]
        [TestCase("ABCDEFGH*I4567890", null, Description = "Direct match, but invalid chrome style")]
        [TestCase("NOMATCHX*I0123456", null)]
        [TestCase("JKLMNOPQ_R0123456", 1000)]
        [TestCase("JKLMNOPQ_R-------", 1000)]
        [TestCase("STUVWXYZ-A00W0000", 2000)]
        [TestCase("STUVWXYZ-A0020000", null, Description = "If suffix character does not match non-wildcard character then pattern not matched.")]
        [TestCase("abcdefgh*i7890123", 200, Description = "Lowercase vin prefix should match")]
        [TestCase("ABCDEFGH*I1234z67", 400, Description = "Lowercase vin suffix should match")]
        public void LookupChromeStyleIdByVIN_should_return_expected_result_given_VIN(
            string vin, int? expectedChromeStyleId)
        {
            var vinPatternBuffer =
                "ABCDEFGHI0123456789012345678901234Z67JKLMNOPQR*******STUVWXYZA**W****".Select(r => (byte)r).ToArray();

            var vinSuffixList = new[]
                                    {
                                        new VinPatternSuffix(9, 50, 100),
                                        new VinPatternSuffix(16, 51, 200),
                                        new VinPatternSuffix(23, 52, -1),
                                        new VinPatternSuffix(30, 53, 400),
                                        new VinPatternSuffix(46, 54, 1000),
                                        new VinPatternSuffix(62, 55, 2000),
                                        new VinPatternSuffix(62, 56, -1)
                                    };  

            var vinPatternLookup = new Dictionary<VinPart, VinPatternSuffixes>(
                new VinPartComparer(9))
                    {
                        {
                            new VinPart(vinPatternBuffer, 0), 
                            new VinPatternSuffixes(0, 4)
                        }, 
                        {
                            new VinPart(vinPatternBuffer, 37), 
                            new VinPatternSuffixes(4, 1)
                        },
                        {
                            new VinPart(vinPatternBuffer, 53),
                            new VinPatternSuffixes(5, 2)
                        }
                    };

            DataMock.Setup(d => d.VinPatternBuffer).Returns(vinPatternBuffer);
            DataMock.Setup(d => d.VinPatternSuffixList).Returns(vinSuffixList);
            DataMock.Setup(d => d.VinPatternLookup).Returns(vinPatternLookup);

            var mapper = new ChromeMapper(Repo);

            var style = mapper.LookupChromeStyleIdByVin(vin);

            Assert.That(style, Is.EqualTo(expectedChromeStyleId));
        }

        [TestCase(null, null)]
        [TestCase("NOT_17_CHARS", null)]
        [TestCase("ABCDEFGH*I7890123", 51)]
        [TestCase("ABCDEFGH*I1234Z67", 53)]
        [TestCase("ABCDEFGH*I0123456", 50)]
        [TestCase("ABCDEFGH*I4567890", 52)]
        [TestCase("NOMATCHX*I0123456", null)]
        [TestCase("JKLMNOPQ_R0123456", 54)]
        [TestCase("JKLMNOPQ_R-------", 54)]
        [TestCase("STUVWXYZ-A00W0000", 55)]
        [TestCase("STUVWXYZ-A0020000", null, Description = "If suffix character does not match non-wildcard character then pattern not matched.")]
        [TestCase("abcdefgh*i7890123", 51, Description = "Lowercase vin prefix should match")]
        [TestCase("ABCDEFGH*I1234z67", 53, Description = "Lowercase vin suffix should match")]
        public void LookupVinPatternIdByVin_should_return_expected_result_given_VIN(
            string vin, int? expectedVinPatternId)
        {
            var vinPatternBuffer =
                "ABCDEFGHI0123456789012345678901234Z67JKLMNOPQR*******STUVWXYZA**W****".Select(r => (byte)r).ToArray();

            var vinSuffixList = new[]
                                    {
                                        new VinPatternSuffix(9, 50, 100),
                                        new VinPatternSuffix(16, 51, 200),
                                        new VinPatternSuffix(23, 52, -1),
                                        new VinPatternSuffix(30, 53, 400),
                                        new VinPatternSuffix(46, 54, 1000),
                                        new VinPatternSuffix(62, 55, 2000),
                                        new VinPatternSuffix(62, 56, -1)
                                    };

            var vinPatternLookup = new Dictionary<VinPart, VinPatternSuffixes>(
                new VinPartComparer(9))
                    {
                        {
                            new VinPart(vinPatternBuffer, 0), 
                            new VinPatternSuffixes(0, 4)
                        }, 
                        {
                            new VinPart(vinPatternBuffer, 37), 
                            new VinPatternSuffixes(4, 1)
                        },
                        {
                            new VinPart(vinPatternBuffer, 53),
                            new VinPatternSuffixes(5, 2)
                        }
                    };

            DataMock.Setup(d => d.VinPatternBuffer).Returns(vinPatternBuffer);
            DataMock.Setup(d => d.VinPatternSuffixList).Returns(vinSuffixList);
            DataMock.Setup(d => d.VinPatternLookup).Returns(vinPatternLookup);

            var mapper = new ChromeMapper(Repo);

            var vinPatternId = mapper.LookupVinPatternIdByVin(vin);

            Assert.That(vinPatternId, Is.EqualTo(expectedVinPatternId));
        }
    }
}