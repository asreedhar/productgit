﻿using System;
using FirstLook.Merchandising.DomainModel.Chrome.Mapper;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Chrome.Mapper
{
    [TestFixture]
    public class ChromeStyleMapDataAccumulatorTests
    {
        private Mock<IChromeMapRepository> RepoMock;
        private IChromeMapRepository Repo;
        private ChromeMapper Map;

        [SetUp]
        public void Setup()
        {
            RepoMock = new Mock<IChromeMapRepository>();
            Repo = RepoMock.Object;
            Map = new ChromeMapper(Repo);
        }

        [TestCase(50, 100)]
        [TestCase(90, 500)]
        [TestCase(42, null, Description = "Should return null when key is not present in lookup")]
        public void AccumulateVehicleCatalogEntry_should_generate_a_vehicle_catalog_mapping_entry(
            int vehicleCatalogIdToLookup, int? expectedChromeStyleId)
        {
            var accumulator = new ChromeMapDataAccumulator();

            accumulator.AccumulateVehicleCatalogEntry(50, 100);
            accumulator.AccumulateVehicleCatalogEntry(90, 500);

            RepoMock.Setup(r => r.GetMapperData()).Returns(
                accumulator.GetData());

            Assert.That(Map.LookupChromeStyleIdByVehicleCatalogId(vehicleCatalogIdToLookup),
                        Is.EqualTo(expectedChromeStyleId));
        }

        [TestCase("ABCDEFGH*I7890123", 200)]
        [TestCase("ABCDEFGH*I1234z67", 400)]
        [TestCase("ABCDEFGH*I0123456", 100)]
        [TestCase("ABCDEFGH*I4567890", null, Description = "Direct match, but invalid chrome style")]
        [TestCase("NOMATCHX*I0123456", null)]
        [TestCase("JKLMNOPQ_R0123456", 1000)]
        [TestCase("JKLMNOPQ_R-------", 1000)]
        [TestCase("STUVWXYZ-A00W0000", 2000)]
        [TestCase("abcdefgh*i7890123", 200, Description = "Lowercase vin prefix should match")]
        [TestCase("ABCDEFGH*I1234z67", 400, Description = "Lowercase vin suffix should match")]
        [TestCase("ABCDEFGH*I1234Z67", 400, Description = "Uppercase vin suffix should match")]
        public void AccumulateVinPatternEntry_should_generate_vin_pattern_lookup_data(
            string vin, int? expectedChromeStyleId)
        {
            var accumulator = new ChromeMapDataAccumulator();

            accumulator.AccumulateVinPatternEntry("ABCDEFGHI", "0123456", 100, 50);
            accumulator.AccumulateVinPatternEntry("abcdEFGHI", "7890123", 200, 51);
            accumulator.AccumulateVinPatternEntry("abcdefghi", "4567890", -1, 52);
            accumulator.AccumulateVinPatternEntry("ABCDEFGHI", "1234z67", 400, 53);
            accumulator.AccumulateVinPatternEntry("JKLMNOPQR", "*******", 1000, 54);
            accumulator.AccumulateVinPatternEntry("STUVWXYZA", "**W****", 2000, 55);

            RepoMock.Setup(r => r.GetMapperData()).Returns(
               accumulator.GetData());

            Assert.That(Map.LookupChromeStyleIdByVin(vin), Is.EqualTo(expectedChromeStyleId));
        }

        [TestCase("ABCDEFGH*I7890123", 51)]
        [TestCase("ABCDEFGH*I1234z67", 53)]
        [TestCase("ABCDEFGH*I0123456", 50)]
        [TestCase("ABCDEFGH*I4567890", 52)]
        [TestCase("NOMATCHX*I0123456", null)]
        [TestCase("JKLMNOPQ_R0123456", 54)]
        [TestCase("JKLMNOPQ_R-------", 54)]
        [TestCase("STUVWXYZ-A00W0000", 55)]
        [TestCase("abcdefgh*i7890123", 51, Description = "Lowercase vin prefix should match")]
        [TestCase("ABCDEFGH*I1234z67", 53, Description = "Lowercase vin suffix should match")]
        [TestCase("ABCDEFGH*I1234Z67", 53, Description = "Uppercase vin suffix should match")]
        public void AccumulateVinPatternEntry_should_generate_vin_pattern_lookup_data_for_VinPatternId_mapping(
            string vin, int? expectedVinPatternId)
        {
            var accumulator = new ChromeMapDataAccumulator();

            accumulator.AccumulateVinPatternEntry("ABCDEFGHI", "0123456", 100, 50);
            accumulator.AccumulateVinPatternEntry("abcdEFGHI", "7890123", 200, 51);
            accumulator.AccumulateVinPatternEntry("abcdefghi", "4567890", -1, 52);
            accumulator.AccumulateVinPatternEntry("ABCDEFGHI", "1234z67", 400, 53);
            accumulator.AccumulateVinPatternEntry("JKLMNOPQR", "*******", 1000, 54);
            accumulator.AccumulateVinPatternEntry("STUVWXYZA", "**W****", 2000, 55);

            RepoMock.Setup(r => r.GetMapperData()).Returns(
               accumulator.GetData());

            Assert.That(Map.LookupVinPatternIdByVin(vin), Is.EqualTo(expectedVinPatternId));
        }

        [TestCase("012345678X", "0123456", Description = "Prefix too long")]
        [TestCase("01234567", "0123456", Description = "Prefix too short")]
        [TestCase("012345678", "01234567", Description = "Suffix too long")]
        [TestCase("012345678", "012345", Description = "Suffix too short")]
        public void AccumulateVinPatternEntry_with_invalid_prefix_or_suffix_lengths_should_throw(string prefix, string suffix)
        {
            var accumulator = new ChromeMapDataAccumulator();

            Assert.Throws<ArgumentOutOfRangeException>(
                () => accumulator.AccumulateVinPatternEntry(prefix, suffix, -1, 60));
        }
    }
}