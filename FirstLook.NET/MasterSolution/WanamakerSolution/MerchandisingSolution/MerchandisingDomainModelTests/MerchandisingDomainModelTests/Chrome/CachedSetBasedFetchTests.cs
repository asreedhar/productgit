﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core;
using FirstLook.Merchandising.DomainModel.Chrome;
using NUnit.Framework;


namespace MerchandisingDomainModelTests.Chrome
{
    [TestFixture]
    public class CachedSetBasedFetchTests
    {
        private ICache _cache;
        private List<int[]> _getItemByIdCalls;

        [SetUp]
        public void Setup()
        {
            _cache = new FirstLook.Common.Core.Cache.DotnetMemoryCacheWrapper(
                new System.Runtime.Caching.MemoryCache(
                    "CachedVinPatternRepository"));
            _getItemByIdCalls = new List<int[]>();
        }
       
        private IEnumerable<object> GetItemById(IEnumerable<int> ids)
        {
            var input = ids.ToArray();
            _getItemByIdCalls.Add(input);
            return input.Select(id => (object)id).ToList();
        }

        private static int GetIdFromItem(object item)
        {
            return (int) item;
        }

        [Test]
        public void GetById_should_call_inner_with_full_set_of_ids_with_clean_cache()
        {
            var repository = new CachedSetBasedFetch<object>(_cache, 3600, "Prefix_",
                GetItemById, GetIdFromItem);

            var objects = repository.GetById(new[] {5, 10, 15});

            Assert.That(objects.Select(item => (int)item).ToArray(), Is.EquivalentTo(new[] {5, 10, 15}));
            Assert.That(_getItemByIdCalls.Count, Is.EqualTo(1));
        }

        [Test]
        public void GetById_twice_with_same_input_ids_should_only_call_inner_once()
        {
            var repository = new CachedSetBasedFetch<object>(_cache, 3600, "Prefix_",
                GetItemById, GetIdFromItem);

            var objects = repository.GetById(new[] { 5, 10, 15 });
            
            var objects2 = repository.GetById(new[] { 5, 10, 15 });

            Assert.That(objects.Select(item => (int)item).ToArray(), Is.EquivalentTo(new[] { 5, 10, 15 }));
            Assert.That(objects2.Select(item => (int)item).ToArray(), Is.EquivalentTo(new[] { 5, 10, 15 }));
            Assert.That(_getItemByIdCalls.Count, Is.EqualTo(1));
        }

        [Test]
        public void GetById_twice_with_similar_input_ids_but_one_added_should_call_inner_a_second_time_to_fetch_extra_item()
        {
            var repository = new CachedSetBasedFetch<object>(_cache, 3600, "Prefix_",
                                                             GetItemById, GetIdFromItem);

            var objects = repository.GetById(new[] { 5, 10, 15 });

            var objects2 = repository.GetById(new[] { 5, 10, 15, 35 });

            Assert.That(objects.Select(item => (int)item).ToArray(), Is.EquivalentTo(new[] { 5, 10, 15 }));
            Assert.That(objects2.Select(item => (int)item).ToArray(), Is.EquivalentTo(new[] { 5, 10, 15, 35 }));
            Assert.That(_getItemByIdCalls.Count, Is.EqualTo(2));
            Assert.That(_getItemByIdCalls[0], Is.EquivalentTo(new[] { 5, 10, 15 }));
            Assert.That(_getItemByIdCalls[1], Is.EquivalentTo(new[] { 35 }));
        }

        
        [Test]
        public void GetById_twice_with_similar_input_ids_should_call_inner_to_retrieve_extra_items()
        {
            var repository = new CachedSetBasedFetch<object>(_cache, 3600, "Prefix_",
                                                             GetItemById, GetIdFromItem);

            var objects = repository.GetById(new[] { 5, 10, 15 });

            var objects2 = repository.GetById(new[] { 5, 75, 35 });

            Assert.That(objects.Select(item => (int)item).ToArray(), Is.EquivalentTo(new[] { 5, 10, 15 }));
            Assert.That(objects2.Select(item => (int)item).ToArray(), Is.EquivalentTo(new[] { 5, 35, 75 }));
            Assert.That(_getItemByIdCalls.Count, Is.EqualTo(2));
            Assert.That(_getItemByIdCalls[0], Is.EquivalentTo(new[] { 5, 10, 15 }));
            Assert.That(_getItemByIdCalls[1], Is.EquivalentTo(new[] { 35, 75 }));
        }

        [Test]
        public void GetVinPatterById_should_collapse_duplicate_input_ids()
        {
            var repository = new CachedSetBasedFetch<object>(_cache, 3600, "Prefix_",
                                                             GetItemById, GetIdFromItem);

            var objects = repository.GetById(new[] { 5, 5, 5, 75, 75 });

            Assert.That(objects.Select(item => (int) item).ToArray(), Is.EquivalentTo(new[] { 5, 75 }));
        }
    }
}