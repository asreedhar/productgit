﻿using FirstLook.Merchandising.DomainModel.Chrome;
using FirstLook.Merchandising.DomainModel.Chrome.Model;
using Moq;
using NUnit.Framework;
using Extensions = FirstLook.Merchandising.DomainModel.Chrome.Extensions;

// ReSharper disable InvokeAsExtensionMethod

namespace MerchandisingDomainModelTests.Chrome
{
    [TestFixture]
    public class ExtensionsTests
    {
        private Mock<IVinPatternRepository> _vinPatternRepositoryMock;
        private IVinPatternRepository _vinPatternRepository;
        private Mock<IVinPattern> _vinPatternMock;
        private IVinPattern _vinPattern;
        private Mock<IStyleRepository> _styleRepositoryMock;
        private IStyleRepository _styleRepository;

        [SetUp]
        public void Setup()
        {
            _vinPatternMock = new Mock<IVinPattern>();
            _vinPattern = _vinPatternMock.Object;

            _vinPatternRepositoryMock = new Mock<IVinPatternRepository>();
            _vinPatternRepository = _vinPatternRepositoryMock.Object;

            _styleRepositoryMock = new Mock<IStyleRepository>();
            _styleRepository = _styleRepositoryMock.Object;
        }

        [Test]
        public void GetVinPatternById_should_pass_single_item_enumerable_to_inner()
        {
            Extensions.GetVinPatternById(_vinPatternRepository, 42);

            _vinPatternRepositoryMock.Verify(r => r.GetVinPatternById(new[] {42}), Times.Once());
        }

        [Test]
        public void GetVinPatternById_should_return_expected_VinPattern()
        {
            _vinPatternRepositoryMock.Setup(r => r.GetVinPatternById(new[] {42})).Returns(new[] {_vinPattern});

            var result = Extensions.GetVinPatternById(_vinPatternRepository, 42);

            Assert.That(result, Is.SameAs(_vinPattern));
        }

        [Test]
        public void GetVinPatternById_should_return_null_when_inner_returns_empty_list()
        {
            _vinPatternRepositoryMock.Setup(r => r.GetVinPatternById(new[] { 42 })).Returns(new IVinPattern[0]);

            var result = Extensions.GetVinPatternById(_vinPatternRepository, 42);

            Assert.That(result, Is.Null);
        }

        [Test]
        public void GetStyleById_should_pass_single_item_enumerable_to_inner()
        {
            Extensions.GetStyleById(_styleRepository, 42);

            _styleRepositoryMock.Verify(r => r.GetStyleById(new[] { 42 }), Times.Once());
        }

        [Test]
        public void GetStyleById_should_return_expected_Style()
        {
            _styleRepositoryMock.Setup(r => r.GetStyleById(new[] {42})).Returns(new[] {new Style(styleID: 42)});

            var result = Extensions.GetStyleById(_styleRepository, 42);

            Assert.That(result.StyleID, Is.EqualTo(42));
        }

        [Test]
        public void GetStyleById_should_return_null_when_inner_returns_empty_list()
        {
            _styleRepositoryMock.Setup(r => r.GetStyleById(new[] { 42 })).Returns(new Style[0]);

            var result = Extensions.GetStyleById(_styleRepository, 42);

            Assert.That(result, Is.Null);
        }
    }
}