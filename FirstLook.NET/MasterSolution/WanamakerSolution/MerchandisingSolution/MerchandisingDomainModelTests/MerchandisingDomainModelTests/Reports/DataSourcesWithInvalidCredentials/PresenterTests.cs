using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Postings;
using FirstLook.Merchandising.DomainModel.Reports;
using FirstLook.Merchandising.DomainModel.Reports.DataSourcesWithInvalidCredentials;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Reports.DataSourcesWithInvalidCredentials
{
    [TestFixture]
    public class PresenterTests
    {
        [Test, ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_with_null_getDataFunc_should_throw()
        {
            var destinations = new[] {EdtDestinations.AutoTrader};
            const double staleDataCutoff = 2.0;

            new Presenter(null, staleDataCutoff, destinations, DateTime.UtcNow);
        }

        [Test, ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_with_null_destinations_should_throw()
        {
            const double staleDataCutoff = 2.0;
            Func<int, IEnumerable<DataSourceStatus>> getDataFunc = businessUnitId => new DataSourceStatus[0];

            new Presenter(getDataFunc, staleDataCutoff, null, DateTime.UtcNow);
        }

        [Test, ExpectedException(typeof(ArgumentException))]
        public void Constructor_with_reference_time_not_in_UTC_should_throw()
        {
            const double staleDataCutoff = 2.0;
            Func<int, IEnumerable<DataSourceStatus>> getDataFunc = businessUnitId => new DataSourceStatus[0];
            var destinations = new[] {EdtDestinations.CarsDotCom};

            new Presenter(getDataFunc, staleDataCutoff, destinations,
                          new DateTime(2005, 5, 5, 5, 5, 5, DateTimeKind.Local));
        }

        [Test]
        public void DataSourceInformation_should_call_GetDataFunc_with_provided_businessUnitId()
        {
            var capturedBusinessUnitId = -1;

            var getDataFunc = (Func<int, IEnumerable<DataSourceStatus>>)
                              (buId =>
                                   {
                                       capturedBusinessUnitId = buId;
                                       return new DataSourceStatus[0];
                                   });
            var presenter = new Presenter(getDataFunc, 2.0, new[] { EdtDestinations.AutoTrader }, DateTime.UtcNow);
            
            presenter.SetBusinessUnitId(123);

            presenter.DataSourceInformation.Count();

            Assert.AreEqual(123, capturedBusinessUnitId);
        }

        [Test]
        public void DataSourceInformation_should_not_call_GetDataFunc_if_business_unit_set_to_null()
        {
            var callCount = 0;
            var getDataFunc = (Func<int, IEnumerable<DataSourceStatus>>)
                              (buId =>
                                   {
                                       callCount++;
                                       throw new Exception("GetDataFunc should not be called.");
                                   });
            var presenter = new Presenter(getDataFunc, 2.0, new[] {EdtDestinations.AutoTrader}, DateTime.UtcNow);
            presenter.SetBusinessUnitId(null);

            var statusCount = presenter.DataSourceInformation.Count();

            Assert.AreEqual(0, statusCount);
            Assert.AreEqual(0, callCount);
        }

        [Test]
        public void DataSourceInformation_should_return_default_DataSourceInfo_enumeration_if_GetDataFunc_returns_null()
        {
            var getDataFunc = (Func<int, IEnumerable<DataSourceStatus>>) (buId => null);
            var presenter = new Presenter(getDataFunc, 2.0, new[] {EdtDestinations.AutoTrader}, DateTime.UtcNow);
            presenter.SetBusinessUnitId(200);

            var statusCount = presenter.DataSourceInformation.Count();

            Assert.AreEqual(1, statusCount);
        }

        [Test]
        public void DataSourceInformation_should_only_return_info_for_specified_destinations()
        {
            var getDataFunc = (Func<int, IEnumerable<DataSourceStatus>>)
                (buId => new [] { 
                    CreateDataSourceStatus(buId, EdtDestinations.CarsDotCom, true, false, new DateTime(2005, 1, 5, 5, 40, 44)),
                    CreateDataSourceStatus(buId, EdtDestinations.AutoTrader, true, false, new DateTime(2006, 1, 5, 5, 40, 45))
                });
            var presenter = new Presenter(getDataFunc, 2.0, new[] {EdtDestinations.CarsDotCom}, DateTime.UtcNow);
            presenter.SetBusinessUnitId(200);

            var infos = presenter.DataSourceInformation.ToArray();

            Assert.AreEqual(1, infos.Length);
            Assert.AreEqual(EdtDestinations.CarsDotCom, infos[0].Destination);
        }

        [Test]
        public void DataSourceInformation_should_pass_thru_LastLoadedTime_from_getDataFunc_1()
        {
            Exercise_LastLoadTime_passthru(new DateTime(2001, 3, 11, 5, 55, 13));
        }

        [Test]
        public void DataSourceInformation_should_pass_thru_LastLoadedTime_from_getDataFunc_2()
        {
            Exercise_LastLoadTime_passthru(null);
        }

        private static void Exercise_LastLoadTime_passthru(DateTime? expectedLastLoadedTime)
        {
            var getDataFunc = (Func<int, IEnumerable<DataSourceStatus>>)
                              (buId =>
                               new[]
                                   {
                                       CreateDataSourceStatus(buId, EdtDestinations.DealerSpecialties, true, false,
                                                              expectedLastLoadedTime)
                                   });
            var presenter = new Presenter(getDataFunc, 2.0, new[] {EdtDestinations.DealerSpecialties}, DateTime.UtcNow);
            presenter.SetBusinessUnitId(215);

            var infos = presenter.DataSourceInformation.ToArray();

            Assert.AreEqual(1, infos.Length);
            Assert.AreEqual(EdtDestinations.DealerSpecialties, infos[0].Destination);
            Assert.AreEqual(expectedLastLoadedTime, infos[0].LastLoadedTime);
        }

        [Test]
        public void DataSourceInformation_should_return_default_LastLoadTime_for_destination_when_getDataFunc_does_not_return()
        {
            var getDataFunc = (Func<int, IEnumerable<DataSourceStatus>>)
                              (buId => new DataSourceStatus[0]);
            var presenter = new Presenter(getDataFunc, 2.0, new[] {EdtDestinations.CarsDotCom}, DateTime.UtcNow);
            presenter.SetBusinessUnitId(25);

            var infos = presenter.DataSourceInformation.ToArray();

            Assert.AreEqual(1, infos.Length);
            Assert.AreEqual(null, infos[0].LastLoadedTime);
        }

        [Test]
        public void DataSourceInformation_should_return_correct_Status_when_missing_credentials()
        {
            Exercise_Status_property(CreateDataSourceStatus(0, EdtDestinations.CarsDotCom, false, false,
                                                            new DateTime(2011, 9, 5, 0, 0, 0, DateTimeKind.Utc)),
                                     EdtDestinations.CarsDotCom, new DateTime(2011, 9, 5, 5, 55, 21, DateTimeKind.Utc), 2.0, 
                                     Status.MissingCredentials);
        }

        [Test]
        public void DataSourceInformation_should_return_correct_Status_when_having_invalid_credentials()
        {
            Exercise_Status_property(CreateDataSourceStatus(0, EdtDestinations.CarsDotCom, true, true,
                                                            new DateTime(2011, 9, 5, 0, 0, 0, DateTimeKind.Utc)),
                                     EdtDestinations.CarsDotCom, new DateTime(2011, 9, 5, 5, 55, 21, DateTimeKind.Utc), 2.0, 
                                     Status.InvalidCredentials);
        }

        [Test]
        public void DataSourceInformation_should_return_correct_Status_when_having_invalid_and_missing_credentials()
        {
            Exercise_Status_property(CreateDataSourceStatus(0, EdtDestinations.CarsDotCom, false, true,
                                                            new DateTime(2011, 9, 5, 0, 0, 0, DateTimeKind.Utc)),
                                     EdtDestinations.CarsDotCom, new DateTime(2011, 9, 5, 5, 55, 21, DateTimeKind.Utc), 2.0,
                                     Status.MissingCredentials);
        }

        [Test]
        public void DataSourceInformation_should_return_correct_Status_of_StaleData_when_having_null_load_date()
        {
            Exercise_Status_property(CreateDataSourceStatus(0, EdtDestinations.CarsDotCom, true, false, null),
                                     EdtDestinations.CarsDotCom, new DateTime(2011, 9, 5, 5, 55, 21, DateTimeKind.Utc),
                                     2.0,
                                     Status.StaleData);
        }

        [Test]
        public void DataSourceInformation_should_return_correct_Status_for_LastLoadedTime_barely_within_bounds()
        {
            Exercise_Status_property(CreateDataSourceStatus(0, EdtDestinations.CarsDotCom, true, false, new DateTime(2011, 9, 3, 5, 55, 21, DateTimeKind.Utc)),
                                     EdtDestinations.CarsDotCom, new DateTime(2011, 9, 5, 5, 55, 21, DateTimeKind.Utc),
                                     2.0,
                                     Status.Ok);
        }

        [Test]
        public void DataSourceInformation_should_return_correct_Status_for_LastLoadedTime_barely_outside_of_bounds()
        {
            Exercise_Status_property(CreateDataSourceStatus(0, EdtDestinations.CarsDotCom, true, false, new DateTime(2011, 9, 3, 5, 55, 20, DateTimeKind.Utc)),
                                     EdtDestinations.CarsDotCom, new DateTime(2011, 9, 5, 5, 55, 21, DateTimeKind.Utc),
                                     2.0,
                                     Status.StaleData);
        }

        [Test]
        public void DataSourceInformation_should_return_correct_Status_for_LastLoadedTime_barely_within_bounds_local_time()
        {
            var lastLoadedTimeInLocalTime = new DateTime(2011, 9, 3, 5, 55, 21, DateTimeKind.Utc).ToLocalTime();
            Exercise_Status_property(CreateDataSourceStatus(0, EdtDestinations.CarsDotCom, true, false, lastLoadedTimeInLocalTime),
                                     EdtDestinations.CarsDotCom, new DateTime(2011, 9, 5, 5, 55, 21, DateTimeKind.Utc),
                                     2.0,
                                     Status.Ok);
        }

        [Test]
        public void DataSourceInformation_should_return_correct_Status_for_LastLoadedTime_barely_outside_of_bounds_local_time()
        {
            var lastLoadedTimeInLocalTime = new DateTime(2011, 9, 3, 5, 55, 20, DateTimeKind.Utc).ToLocalTime();
            Exercise_Status_property(CreateDataSourceStatus(0, EdtDestinations.CarsDotCom, true, false, lastLoadedTimeInLocalTime),
                                     EdtDestinations.CarsDotCom, new DateTime(2011, 9, 5, 5, 55, 21, DateTimeKind.Utc),
                                     2.0,
                                     Status.StaleData);
        }

        [Test]
        public void DataSourceInformation_should_return_correct_Status_of_Ok_when_all_indicators_are_valid()
        {
            Exercise_Status_property(CreateDataSourceStatus(0, EdtDestinations.CarsDotCom, true, false, new DateTime(2011, 9, 4, 0, 0, 0, DateTimeKind.Utc)),
                                     EdtDestinations.CarsDotCom, new DateTime(2011, 9, 5, 5, 55, 21, DateTimeKind.Utc),
                                     2.0,
                                     Status.Ok);
        }

        [Test]
        public void DataSourceInformation_should_return_correct_Status_of_missing_credentials_and_stale_data_when_getDataFunc_does_not_return_data_for_destination()
        {
            Exercise_Status_property(null, EdtDestinations.AutoTrader, DateTime.UtcNow, 2.0, Status.MissingCredentials | Status.StaleData);
        }

        private static void Exercise_Status_property(DataSourceStatus status, EdtDestinations destinationToCheck, DateTime refTime, double staleThreshold, Status expectedStatus)
        {
            var getDataFunc = (Func<int, IEnumerable<DataSourceStatus>>)
                (buId => status == null ? new DataSourceStatus[0] : new[] { status });
            var presenter = new Presenter(getDataFunc, staleThreshold, new[] {destinationToCheck}, refTime);
            presenter.SetBusinessUnitId(0);

            var infos = presenter.DataSourceInformation.ToArray();

            Assert.AreEqual(expectedStatus, infos[0].Status);
        }

        [Test]
        public void SetState_should_restore_state_returned_by_GetState()
        {
            int? capturedBuId = null;
            var getDataFunc = (Func<int, IEnumerable<DataSourceStatus>>)
                              (buId =>
                                   {
                                       capturedBuId = buId;
                                       return new DataSourceStatus[0];
                                   });
            var presenter = new Presenter(getDataFunc, 2.0, new[] {EdtDestinations.AutoTrader}, DateTime.UtcNow);
            presenter.SetBusinessUnitId(31);

            var state = presenter.GetState();

            var anotherPresenter = new Presenter(getDataFunc, 2.0, new[] {EdtDestinations.AutoTrader}, DateTime.UtcNow);
            anotherPresenter.SetState(state);
            
            Assert.AreEqual(null, capturedBuId);
            anotherPresenter.DataSourceInformation.Count();

            Assert.AreEqual(31, capturedBuId);

        }

        private static DataSourceStatus CreateDataSourceStatus(int buId, EdtDestinations destination, bool hasCredentials, bool hasInvalidCredentials, DateTime? lastLoadedTime)
        {
            return new DataSourceStatus { BusinessUnitId = buId, Destination = destination, 
                                          HasCredentials = hasCredentials, HasInvalidCredentials = hasInvalidCredentials, 
                                          LastLoadedTime = lastLoadedTime };
        }
    }
}