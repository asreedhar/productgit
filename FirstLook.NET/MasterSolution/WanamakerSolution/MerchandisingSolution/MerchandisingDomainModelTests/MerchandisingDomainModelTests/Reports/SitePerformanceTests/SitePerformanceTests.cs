﻿using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using NUnit.Framework;
using FirstLook.Common.Core;
using System;

namespace MerchandisingDomainModelTests.Reports.SitePerformanceTests
{
    [TestFixture]
    public class SitePerformanceTests
    {
        private SitePerformance _performance;

        [SetUp]
        public void SetUp()
        {
            _performance = CreateObjectUnderTest();
        }

        public SitePerformance CreateObjectUnderTest()
        {
            return new SitePerformance
            {
                Site = new Site { Id = 1, Name = "AutoTrader.com" },
                SearchPageViewCount = 566,
                DetailPageViewCount = 250,
                EmailLeadsCount = 21,
                PhoneLeadsCount = 10,
                MapsViewedCount = 5,
                AdPrintedCount = 2,
                DateRange = DateRange.MonthOfDate(DateTime.Now),
            };
        }

        [Test]
        public void Total_leads_should_be_the_sum_of_the_email_and_phone_leads()
        {
            Assert.That(_performance.EmailLeadsCount + _performance.PhoneLeadsCount, Is.EqualTo(_performance.TotalLeads));
        }

        [Test]
        public void Total_actions_should_be_the_sum_of_email_leads_phone_leads_maps_viewed_and_ads_printed()
        {
            Assert.That(_performance.TotalLeads + _performance.MapsViewedCount + _performance.AdPrintedCount, Is.EqualTo(_performance.TotalActions));
        }

        [Test]
        public void Click_Through_Rate_should_be_details_divided_by_search()
        {
            Assert.AreEqual((decimal)_performance.DetailPageViewCount/_performance.SearchPageViewCount, _performance.ClickThroughRate);
        }

        [Test]
        public void Conversion_rate_from_detail_page_should_be_the_total_actions_divided_by_detail_page_view_count()
        {
            Assert.AreEqual((decimal)_performance.TotalActions / _performance.DetailPageViewCount, _performance.ConversionRateFromDetailPage);
        }
     
        [Test]
        public void Click_Through_Rate_should_return_zero_if_the_search_page_view_count_is_zero()
        {
            _performance.SearchPageViewCount = 0;

            Assert.AreEqual(0, _performance.ClickThroughRate);
        }

        [Test]
        public void Conversion_rate_from_detail_page_should_return_zero_if_the_detail_page_view_count_is_zero()
        {
            _performance.DetailPageViewCount = 0;

            Assert.AreEqual(0, _performance.ConversionRateFromDetailPage);
        }
    }
}