﻿using System;
using System.Linq;
using FirstLook.Common.Core;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.AdStatus;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket;
using MAX.Entities.Reports.TimeToMarket.Items;
using MerchandisingDomainModelTests.Reports.TimeToMarket.TestData;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Reports.TimeToMarket
{
    [TestFixture]
    public class TimeToMarketReportTests
    {
        #region TimeToMarketVehicleItem tests
        // The TimeToMarketVehicleItem is kind of the basis for everything on the Report site of TimeToMarket
        [Test]
        public void TimeToMarketVehicleItem_ElapsedDays_Measure_Days_Not_Hours()
        {
            var data = TimeToMarketTestData.MockDataTable(TimeToMarketTestData.GetTestDataList());

            var vehicleItems = new ReportVehicle(data.Rows[0]);
            // We want to use the difference in dates, not the difference in 24 hour periods.
            Assert.That(Math.Abs(1 - vehicleItems.ElapsedDaysPhoto) < Double.Epsilon);
            Assert.That(Math.Abs(4 - vehicleItems.ElapsedDaysCompleteAd) < Double.Epsilon);
        }
        #endregion TimeToMarketVehicleItem tests

        #region Business Unit Report Tests

        [Test]
        public void BusinessUnitReport_FirstPhoto_Days_Are_Correct()
        {
            var testDateRange = new DateRange(new DateTime(1, 1, 1), new DateTime(1, 1, 31));
            var repository = MockRepository();
            repository.Setup(x => x.GetBusinessUnitReportReader(1, 1, testDateRange)).Returns(() => TimeToMarketTestData.MockDataTable(TimeToMarketTestData.GetTestDataList()));

            var report = repository.Object.FetchReport(1, 1, testDateRange);
            
            // Total days to photos online / total vehicles ( 27 / 12) = 2.25

            Assert.That(Math.Abs(report.AverageDaysToPhotoOnline - 2.25) < Double.Epsilon);
            Assert.That(report.MaxDaysPhoto == 4);
            Assert.That(report.MinDaysPhoto == 1);
        }

        private static Mock<TimeToMarketRepository> MockRepository()
        {
            var analytics = new Mock<IAdStatusRepository>();
            analytics.Setup(a => a.GetAdStatusResults(It.IsAny<IAdStatusArgs>())).Returns(TimeToMarketTestData.GetAdStatusResults());

			var inventoryRepo = new Mock<IInventoryDataRepository>();

	        var mockBusinessUnitRepo = new Mock<IDashboardBusinessUnitRepository>();

            return new Mock<TimeToMarketRepository>(analytics.Object, inventoryRepo.Object, mockBusinessUnitRepo.Object);
        }

        [Test]
        public void BusinessUnitReport_CompleteAd_Days_Are_Correct ()
        {
            var testDateRange = new DateRange(new DateTime(1, 1, 1), new DateTime(1, 1, 31));
            var repository = MockRepository();
            repository.Setup(x => x.GetBusinessUnitReportReader(1, 1, testDateRange)).Returns(() => TimeToMarketTestData.MockDataTable(TimeToMarketTestData.GetTestDataList()));

            var report = repository.Object.FetchReport(1, 1, testDateRange);

            // Total days to complete ad / total vehicles ( 42 / 12) = 3.5

            Assert.That( Math.Abs(report.AverageDaysToCompleteAd - 3.5) < Double.Epsilon);
            Assert.That(report.MaxDaysComplete == 4);
            Assert.That(report.MinDaysComplete == 3);
        }

        [Test]
        public void BusnessUnitReport_Days_Have_Correct_Data()
        {
            var testDateRange = new DateRange(new DateTime(1, 1, 1), new DateTime(1, 1, 31));
            var repository = MockRepository();
            repository.Setup(x => x.GetBusinessUnitReportReader(1, 1, testDateRange)).Returns(() => TimeToMarketTestData.MockDataTable(TimeToMarketTestData.GetTestDataList()));

            var report = repository.Object.FetchReport(1, 1, testDateRange);
            Assert.That(report.DateRange == testDateRange);

            report.Days.Sort((a, b) => a.Date.CompareTo(b.Date));
            /***
             * Verify each day is calculated correctly... 
             ***/


        }

        [Test]
        public void BusnessUnitReport_Setup_Correct_Data()
        {
            var testDateRange = new DateRange(new DateTime(1, 1, 1), new DateTime(1, 1, 31));
            var repository = MockRepository();
            repository.Setup(x => x.GetBusinessUnitReportReader(1, 1, testDateRange)).Returns(() => TimeToMarketTestData.MockDataTable(TimeToMarketTestData.GetTestDataList()));

            var report = repository.Object.FetchReport(1, 1, testDateRange);

            Assert.That(report.BusinessUnitId == 1);
            Assert.That(report.DateRange == testDateRange);
            Assert.That(report.Vehicles.Count == 12);
            Assert.That(report.DaysCount == 5);
        }

        [Test]
        public void BusinessUnitReport_Day_Averages_Match()
        {
            var testDateRange = new DateRange(new DateTime(1, 1, 1), new DateTime(1, 1, 31));
            var repository = MockRepository();
            repository.Setup(x => x.GetBusinessUnitReportReader(1, 1, testDateRange)).Returns(() => TimeToMarketTestData.MockDataTable(TimeToMarketTestData.GetTestDataList()));

            var report = repository.Object.FetchReport(1, 1, testDateRange);

            var foundDays = 0;
            foreach (var found in testDateRange.AsDays().Select(day => report.Days.Find(d => d.Date == day.StartDate.Date)).Where(found => found != null))
            {
                foundDays++;

                if (found.Date == new DateTime(1, 1, 2))
                {
                    Assert.That(found.AvgElapsedDaysPhoto == 1);
                    Assert.That(found.AvgElapsedDaysAdComplete == 0);
                }
                else if (found.Date == new DateTime(1, 1, 5))
                {
                    Assert.That(found.AvgElapsedDaysPhoto == 3);
                    Assert.That(found.AvgElapsedDaysAdComplete == 4);
                }
                else if (found.Date == new DateTime(1, 1, 6))
                {
                    Assert.That(found.AvgElapsedDaysPhoto == 2);
                    Assert.That(found.AvgElapsedDaysAdComplete == 4);
                }
                else if (found.Date == new DateTime(1, 1, 7))
                {
                    Assert.That(found.AvgElapsedDaysPhoto == 3);
                    Assert.That(found.AvgElapsedDaysAdComplete == 4);
                }
                else if (found.Date == new DateTime(1, 1, 8))
                {
                    Assert.That(found.AvgElapsedDaysPhoto == 4);
                    Assert.That(found.AvgElapsedDaysAdComplete == 4);
                }
                else
                    Assert.Fail("Found a date we shouldn't have");
            }
            Assert.That(foundDays == report.DaysCount);
        }

        #endregion Business Unit Report Tests

        #region Group Report Tests
        [Test]
        public void Group_Report_Setup_Correctly()
        {
            var testDateRange = new DateRange(new DateTime(1, 1, 1), new DateTime(1, 1, 31));
            var repository = MockRepository();
            repository.Setup(x => x.GetGroupReportReader(4, 1, testDateRange)).Returns(() => TimeToMarketTestData.MockDataTable(TimeToMarketTestData.GetGroupTestDataList()));

            var report = repository.Object.FetchGroupReport(4, 1, testDateRange);

            Assert.That(report.GroupId == 4);
            Assert.That(report.DateRange == testDateRange);
            Assert.That(report.BusinessUnits.Count == 3);
            Assert.That(report.VehicleCount == 36);
        }

        [Test]
        public void Group_Report_Averages_Are_Correct()
        {
            var testDateRange = new DateRange(new DateTime(1, 1, 1), new DateTime(1, 1, 31));
            var repository = MockRepository();
            repository.Setup(x => x.GetGroupReportReader(4, 1, testDateRange)).Returns(() => TimeToMarketTestData.MockDataTable(TimeToMarketTestData.GetGroupTestDataList()));

            var report = repository.Object.FetchGroupReport(4, 1, testDateRange);

            // These averages should be averages of averages... so sum the averages for each business unit and divide by 3
            var avgPhotos = report.BusinessUnits.Sum(bu => bu.AverageDaysToPhotoOnline) / 3;
            var avgComplete = report.BusinessUnits.Sum(bu => bu.AverageDaysToCompleteAd) / 3;
            
            Assert.That( Math.Abs(report.AverageDaysToPhotoOnline - avgPhotos) < Double.Epsilon);
            Assert.That( Math.Abs(report.AverageDaysToCompleteAdOnline - avgComplete) < Double.Epsilon);
        }

        #endregion Group Report Tests

    }
}