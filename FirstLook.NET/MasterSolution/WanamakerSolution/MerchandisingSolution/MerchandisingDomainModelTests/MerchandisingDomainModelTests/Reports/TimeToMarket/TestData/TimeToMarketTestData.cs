﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.AdStatus;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket.Items;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using MAX.Entities.Enumerations;

namespace MerchandisingDomainModelTests.Reports.TimeToMarket.TestData
{
    class TimeToMarketTestData
    {
        #region TestData

        internal static DataTable MockDataTable(List<TestData> testData)
        {
            var dt = new DataTable();
            dt.Columns.Add("inventoryActive");
            dt.Columns.Add("BusinessUnitID");
            dt.Columns.Add("InventoryID");
            dt.Columns.Add("VehicleID");
            dt.Columns.Add("VIN");
            dt.Columns.Add("StockNumber");
            dt.Columns.Add("Year");
            dt.Columns.Add("Make");
            dt.Columns.Add("Model");
            dt.Columns.Add("Trim");
            dt.Columns.Add("DateAddedToInventory");
            dt.Columns.Add("PhotosFirstDate");
            dt.Columns.Add("CompleteAdFirstDate");


            foreach (var row in testData)
            {
                var dr = dt.NewRow();
                dr["inventoryActive"] = row.inventoryActive;
                dr["BusinessUnitID"] = row.BusinessUnitId;
                dr["InventoryID"] = row.InventoryId;
                dr["VehicleID"] = row.VehicleId;
                dr["VIN"] = row.VIN;
                dr["StockNumber"] = row.StockNumber;
                dr["Year"] = row.Year;
                dr["Make"] = row.Make;
                dr["Model"] = row.Model;
                dr["Trim"] = row.Trim;
                dr["DateAddedToInventory"] = row.DateAddedToInventory;
                dr["PhotosFirstDate"] = row.DateOfFirstPhoto;
                dr["CompleteAdFirstDate"] = row.DateOfFirstCompleteAd;

                dt.Rows.Add(dr);
            }

            return dt;
        }

        internal class TestData
        {
            public int BusinessUnitId { get; set; }
            public int InventoryId { get; set; }
            public int VehicleId { get; set; }
            public string VIN { get; set; }
            public string StockNumber { get; set; }
            public int Year { get; set; }
            public string Make { get; set; }
            public string Model { get; set; }
            public string Trim { get; set; }
            public DateTime DateAddedToInventory { get; set; }
            public DateTime DateOfFirstPhoto { get; set; }
            public DateTime DateOfFirstCompleteAd { get; set; }
            // ReSharper disable InconsistentNaming
            public byte inventoryActive { get; set; }
            // ReSharper restore InconsistentNaming
        }

        internal static List<TestData> GetTestDataList()
        {
            return new List<TestData>
            {
                new TestData { BusinessUnitId = 1, InventoryId = 01, VehicleId = 01, VIN = "VIN-01", StockNumber = "StockNumber-01", Year = 2013, Make = "Make-01", Model = "Model-01", Trim = "Trim-01", DateAddedToInventory = new DateTime(1,1,1), DateOfFirstPhoto = new DateTime(1,1,2), DateOfFirstCompleteAd = new DateTime(1,1,5), inventoryActive = 1}, // photo days 1    complete days 4
                new TestData { BusinessUnitId = 1, InventoryId = 02, VehicleId = 02, VIN = "VIN-02", StockNumber = "StockNumber-02", Year = 2013, Make = "Make-02", Model = "Model-02", Trim = "Trim-02", DateAddedToInventory = new DateTime(1,1,1), DateOfFirstPhoto = new DateTime(1,1,2), DateOfFirstCompleteAd = new DateTime(1,1,5), inventoryActive = 1}, // photo days 1    complete days 4
                new TestData { BusinessUnitId = 1, InventoryId = 03, VehicleId = 03, VIN = "VIN-03", StockNumber = "StockNumber-03", Year = 2013, Make = "Make-03", Model = "Model-03", Trim = "Trim-03", DateAddedToInventory = new DateTime(1,1,1), DateOfFirstPhoto = new DateTime(1,1,2), DateOfFirstCompleteAd = new DateTime(1,1,5), inventoryActive = 1}, // photo days 1    complete days 4
                new TestData { BusinessUnitId = 1, InventoryId = 04, VehicleId = 04, VIN = "VIN-04", StockNumber = "StockNumber-04", Year = 2013, Make = "Make-04", Model = "Model-04", Trim = "Trim-04", DateAddedToInventory = new DateTime(1,1,2), DateOfFirstPhoto = new DateTime(1,1,5), DateOfFirstCompleteAd = new DateTime(1,1,5), inventoryActive = 1}, // photo days 3    complete days 3
                new TestData { BusinessUnitId = 1, InventoryId = 05, VehicleId = 05, VIN = "VIN-05", StockNumber = "StockNumber-05", Year = 2013, Make = "Make-05", Model = "Model-05", Trim = "Trim-05", DateAddedToInventory = new DateTime(1,1,2), DateOfFirstPhoto = new DateTime(1,1,5), DateOfFirstCompleteAd = new DateTime(1,1,5), inventoryActive = 0}, // photo days 3    complete days 3
                new TestData { BusinessUnitId = 1, InventoryId = 06, VehicleId = 06, VIN = "VIN-06", StockNumber = "StockNumber-06", Year = 2013, Make = "Make-06", Model = "Model-06", Trim = "Trim-06", DateAddedToInventory = new DateTime(1,1,2), DateOfFirstPhoto = new DateTime(1,1,5), DateOfFirstCompleteAd = new DateTime(1,1,6), inventoryActive = 1}, // photo days 3    complete days 4
                new TestData { BusinessUnitId = 1, InventoryId = 07, VehicleId = 07, VIN = "VIN-07", StockNumber = "StockNumber-07", Year = 2013, Make = "Make-07", Model = "Model-07", Trim = "Trim-07", DateAddedToInventory = new DateTime(1,1,3), DateOfFirstPhoto = new DateTime(1,1,5), DateOfFirstCompleteAd = new DateTime(1,1,6), inventoryActive = 1}, // photo days 2    complete days 3
                new TestData { BusinessUnitId = 1, InventoryId = 08, VehicleId = 08, VIN = "VIN-08", StockNumber = "StockNumber-08", Year = 2013, Make = "Make-08", Model = "Model-08", Trim = "Trim-08", DateAddedToInventory = new DateTime(1,1,3), DateOfFirstPhoto = new DateTime(1,1,5), DateOfFirstCompleteAd = new DateTime(1,1,6), inventoryActive = 0}, // photo days 2    complete days 3
                new TestData { BusinessUnitId = 1, InventoryId = 09, VehicleId = 09, VIN = "VIN-09", StockNumber = "StockNumber-09", Year = 2013, Make = "Make-09", Model = "Model-09", Trim = "Trim-09", DateAddedToInventory = new DateTime(1,1,3), DateOfFirstPhoto = new DateTime(1,1,5), DateOfFirstCompleteAd = new DateTime(1,1,7), inventoryActive = 1}, // photo days 2    complete days 4
                new TestData { BusinessUnitId = 1, InventoryId = 10, VehicleId = 10, VIN = "VIN-10", StockNumber = "StockNumber-10", Year = 2013, Make = "Make-10", Model = "Model-10", Trim = "Trim-10", DateAddedToInventory = new DateTime(1,1,4), DateOfFirstPhoto = new DateTime(1,1,6), DateOfFirstCompleteAd = new DateTime(1,1,7), inventoryActive = 1}, // photo days 2    complete days 3
                new TestData { BusinessUnitId = 1, InventoryId = 11, VehicleId = 11, VIN = "VIN-11", StockNumber = "StockNumber-11", Year = 2013, Make = "Make-11", Model = "Model-11", Trim = "Trim-11", DateAddedToInventory = new DateTime(1,1,4), DateOfFirstPhoto = new DateTime(1,1,7), DateOfFirstCompleteAd = new DateTime(1,1,7), inventoryActive = 1}, // photo days 3    complete days 3
                new TestData { BusinessUnitId = 1, InventoryId = 12, VehicleId = 12, VIN = "VIN-12", StockNumber = "StockNumber-12", Year = 2013, Make = "Make-12", Model = "Model-12", Trim = "Trim-12", DateAddedToInventory = new DateTime(1,1,4), DateOfFirstPhoto = new DateTime(1,1,8), DateOfFirstCompleteAd = new DateTime(1,1,8), inventoryActive = 0}  // photo days 4    complete days 4
            };//                                                                                                                                                                                                                                                                                                                                           27                 42
        }

        internal static List<TestData> GetGroupTestDataList()
        {
            return new List<TestData>
            {
                // BU 1
                new TestData { BusinessUnitId = 1, InventoryId = 01, VehicleId = 01, VIN = "VIN-01", StockNumber = "StockNumber-01", Year = 2013, Make = "Make-01", Model = "Model-01", Trim = "Trim-01", DateAddedToInventory = new DateTime(1,1,01), DateOfFirstPhoto = new DateTime(1,1,2), DateOfFirstCompleteAd = new DateTime(1,1,5)}, // photo days 1    complete days 4
                new TestData { BusinessUnitId = 1, InventoryId = 02, VehicleId = 02, VIN = "VIN-02", StockNumber = "StockNumber-02", Year = 2013, Make = "Make-02", Model = "Model-02", Trim = "Trim-02", DateAddedToInventory = new DateTime(1,1,01), DateOfFirstPhoto = new DateTime(1,1,2), DateOfFirstCompleteAd = new DateTime(1,1,5)}, // photo days 1    complete days 4
                new TestData { BusinessUnitId = 1, InventoryId = 03, VehicleId = 03, VIN = "VIN-03", StockNumber = "StockNumber-03", Year = 2013, Make = "Make-03", Model = "Model-03", Trim = "Trim-03", DateAddedToInventory = new DateTime(1,1,01), DateOfFirstPhoto = new DateTime(1,1,2), DateOfFirstCompleteAd = new DateTime(1,1,5)}, // photo days 1    complete days 4
                new TestData { BusinessUnitId = 1, InventoryId = 04, VehicleId = 04, VIN = "VIN-04", StockNumber = "StockNumber-04", Year = 2013, Make = "Make-04", Model = "Model-04", Trim = "Trim-04", DateAddedToInventory = new DateTime(1,1,02), DateOfFirstPhoto = new DateTime(1,1,5), DateOfFirstCompleteAd = new DateTime(1,1,5)}, // photo days 3    complete days 3
                new TestData { BusinessUnitId = 1, InventoryId = 05, VehicleId = 05, VIN = "VIN-05", StockNumber = "StockNumber-05", Year = 2013, Make = "Make-05", Model = "Model-05", Trim = "Trim-05", DateAddedToInventory = new DateTime(1,1,02), DateOfFirstPhoto = new DateTime(1,1,5), DateOfFirstCompleteAd = new DateTime(1,1,5)}, // photo days 3    complete days 3
                new TestData { BusinessUnitId = 1, InventoryId = 06, VehicleId = 06, VIN = "VIN-06", StockNumber = "StockNumber-06", Year = 2013, Make = "Make-06", Model = "Model-06", Trim = "Trim-06", DateAddedToInventory = new DateTime(1,1,02), DateOfFirstPhoto = new DateTime(1,1,5), DateOfFirstCompleteAd = new DateTime(1,1,6), inventoryActive = 0}, // photo days 3    complete days 4
                new TestData { BusinessUnitId = 1, InventoryId = 07, VehicleId = 07, VIN = "VIN-07", StockNumber = "StockNumber-07", Year = 2013, Make = "Make-07", Model = "Model-07", Trim = "Trim-07", DateAddedToInventory = new DateTime(1,1,03), DateOfFirstPhoto = new DateTime(1,1,5), DateOfFirstCompleteAd = new DateTime(1,1,6)}, // photo days 2    complete days 3
                new TestData { BusinessUnitId = 1, InventoryId = 08, VehicleId = 08, VIN = "VIN-08", StockNumber = "StockNumber-08", Year = 2013, Make = "Make-08", Model = "Model-08", Trim = "Trim-08", DateAddedToInventory = new DateTime(1,1,03), DateOfFirstPhoto = new DateTime(1,1,5), DateOfFirstCompleteAd = new DateTime(1,1,6)}, // photo days 2    complete days 3
                new TestData { BusinessUnitId = 1, InventoryId = 09, VehicleId = 09, VIN = "VIN-09", StockNumber = "StockNumber-09", Year = 2013, Make = "Make-09", Model = "Model-09", Trim = "Trim-09", DateAddedToInventory = new DateTime(1,1,03), DateOfFirstPhoto = new DateTime(1,1,5), DateOfFirstCompleteAd = new DateTime(1,1,7)}, // photo days 2    complete days 4
                new TestData { BusinessUnitId = 1, InventoryId = 10, VehicleId = 10, VIN = "VIN-10", StockNumber = "StockNumber-10", Year = 2013, Make = "Make-10", Model = "Model-10", Trim = "Trim-10", DateAddedToInventory = new DateTime(1,1,04), DateOfFirstPhoto = new DateTime(1,1,6), DateOfFirstCompleteAd = new DateTime(1,1,7)}, // photo days 2    complete days 3
                new TestData { BusinessUnitId = 1, InventoryId = 11, VehicleId = 11, VIN = "VIN-11", StockNumber = "StockNumber-11", Year = 2013, Make = "Make-11", Model = "Model-11", Trim = "Trim-11", DateAddedToInventory = new DateTime(1,1,04), DateOfFirstPhoto = new DateTime(1,1,7), DateOfFirstCompleteAd = new DateTime(1,1,7), inventoryActive = 1}, // photo days 3    complete days 3
                new TestData { BusinessUnitId = 1, InventoryId = 12, VehicleId = 12, VIN = "VIN-12", StockNumber = "StockNumber-12", Year = 2013, Make = "Make-12", Model = "Model-12", Trim = "Trim-12", DateAddedToInventory = new DateTime(1,1,04), DateOfFirstPhoto = new DateTime(1,1,8), DateOfFirstCompleteAd = new DateTime(1,1,8)}, // photo days 4    complete days 4
                // Totals:                                                                                                                                                                                                                                                                                                                                  27                 42 
                // Averages:                                                                                                                                                                                                                                                                                                                                2.25              3.5

                // BU 2
                new TestData { BusinessUnitId = 2, InventoryId = 01, VehicleId = 01, VIN = "VIN-01", StockNumber = "StockNumber-01", Year = 2013, Make = "Make-01", Model = "Model-01", Trim = "Trim-01", DateAddedToInventory = new DateTime(1,1,03), DateOfFirstPhoto = new DateTime(1,1,03), DateOfFirstCompleteAd = new DateTime(1,1,04)}, // photo days 0    complete days 1
                new TestData { BusinessUnitId = 2, InventoryId = 02, VehicleId = 02, VIN = "VIN-02", StockNumber = "StockNumber-02", Year = 2013, Make = "Make-02", Model = "Model-02", Trim = "Trim-02", DateAddedToInventory = new DateTime(1,1,03), DateOfFirstPhoto = new DateTime(1,1,03), DateOfFirstCompleteAd = new DateTime(1,1,05)}, // photo days 0    complete days 2
                new TestData { BusinessUnitId = 2, InventoryId = 03, VehicleId = 03, VIN = "VIN-03", StockNumber = "StockNumber-03", Year = 2013, Make = "Make-03", Model = "Model-03", Trim = "Trim-03", DateAddedToInventory = new DateTime(1,1,03), DateOfFirstPhoto = new DateTime(1,1,03), DateOfFirstCompleteAd = new DateTime(1,1,06)}, // photo days 0    complete days 3
                new TestData { BusinessUnitId = 2, InventoryId = 04, VehicleId = 04, VIN = "VIN-04", StockNumber = "StockNumber-04", Year = 2013, Make = "Make-04", Model = "Model-04", Trim = "Trim-04", DateAddedToInventory = new DateTime(1,1,03), DateOfFirstPhoto = new DateTime(1,1,06), DateOfFirstCompleteAd = new DateTime(1,1,07)}, // photo days 3    complete days 4
                new TestData { BusinessUnitId = 2, InventoryId = 05, VehicleId = 05, VIN = "VIN-05", StockNumber = "StockNumber-05", Year = 2013, Make = "Make-05", Model = "Model-05", Trim = "Trim-05", DateAddedToInventory = new DateTime(1,1,03), DateOfFirstPhoto = new DateTime(1,1,06), DateOfFirstCompleteAd = new DateTime(1,1,05)}, // photo days 3    complete days 2
                new TestData { BusinessUnitId = 2, InventoryId = 06, VehicleId = 06, VIN = "VIN-06", StockNumber = "StockNumber-06", Year = 2013, Make = "Make-06", Model = "Model-06", Trim = "Trim-06", DateAddedToInventory = new DateTime(1,1,12), DateOfFirstPhoto = new DateTime(1,1,14), DateOfFirstCompleteAd = new DateTime(1,1,19)}, // photo days 2    complete days 7
                new TestData { BusinessUnitId = 2, InventoryId = 07, VehicleId = 07, VIN = "VIN-07", StockNumber = "StockNumber-07", Year = 2013, Make = "Make-07", Model = "Model-07", Trim = "Trim-07", DateAddedToInventory = new DateTime(1,1,12), DateOfFirstPhoto = new DateTime(1,1,14), DateOfFirstCompleteAd = new DateTime(1,1,19)}, // photo days 2    complete days 7
                new TestData { BusinessUnitId = 2, InventoryId = 08, VehicleId = 08, VIN = "VIN-08", StockNumber = "StockNumber-08", Year = 2013, Make = "Make-08", Model = "Model-08", Trim = "Trim-08", DateAddedToInventory = new DateTime(1,1,12), DateOfFirstPhoto = new DateTime(1,1,14), DateOfFirstCompleteAd = new DateTime(1,1,19)}, // photo days 2    complete days 7
                new TestData { BusinessUnitId = 2, InventoryId = 09, VehicleId = 09, VIN = "VIN-09", StockNumber = "StockNumber-09", Year = 2013, Make = "Make-09", Model = "Model-09", Trim = "Trim-09", DateAddedToInventory = new DateTime(1,1,13), DateOfFirstPhoto = new DateTime(1,1,14), DateOfFirstCompleteAd = new DateTime(1,1,21), inventoryActive = 0}, // photo days 1    complete days 8
                new TestData { BusinessUnitId = 2, InventoryId = 10, VehicleId = 10, VIN = "VIN-10", StockNumber = "StockNumber-10", Year = 2013, Make = "Make-10", Model = "Model-10", Trim = "Trim-10", DateAddedToInventory = new DateTime(1,1,13), DateOfFirstPhoto = new DateTime(1,1,16), DateOfFirstCompleteAd = new DateTime(1,1,20)}, // photo days 3    complete days 7
                new TestData { BusinessUnitId = 2, InventoryId = 11, VehicleId = 11, VIN = "VIN-11", StockNumber = "StockNumber-11", Year = 2013, Make = "Make-11", Model = "Model-11", Trim = "Trim-11", DateAddedToInventory = new DateTime(1,1,13), DateOfFirstPhoto = new DateTime(1,1,17), DateOfFirstCompleteAd = new DateTime(1,1,23)}, // photo days 4    complete days 10
                new TestData { BusinessUnitId = 2, InventoryId = 12, VehicleId = 12, VIN = "VIN-12", StockNumber = "StockNumber-12", Year = 2013, Make = "Make-12", Model = "Model-12", Trim = "Trim-12", DateAddedToInventory = new DateTime(1,1,13), DateOfFirstPhoto = new DateTime(1,1,18), DateOfFirstCompleteAd = new DateTime(1,1,20), inventoryActive = 1}, // photo days 5    complete days 7
                // Totals:                                                                                                                                                                                                                                                                                                                                    25                 65
                // Averages:                                                                                                                                                                                                                                                                                                                                  2.08333              5.416666 

                // BU 3
                new TestData { BusinessUnitId = 3, InventoryId = 01, VehicleId = 01, VIN = "VIN-01", StockNumber = "StockNumber-01", Year = 2013, Make = "Make-01", Model = "Model-01", Trim = "Trim-01", DateAddedToInventory = new DateTime(1,1,12), DateOfFirstPhoto = new DateTime(1,1,15), DateOfFirstCompleteAd = new DateTime(1,1,20)}, // photo days 3    complete days 8
                new TestData { BusinessUnitId = 3, InventoryId = 02, VehicleId = 02, VIN = "VIN-02", StockNumber = "StockNumber-02", Year = 2013, Make = "Make-02", Model = "Model-02", Trim = "Trim-02", DateAddedToInventory = new DateTime(1,1,12), DateOfFirstPhoto = new DateTime(1,1,15), DateOfFirstCompleteAd = new DateTime(1,1,20)}, // photo days 3    complete days 8
                new TestData { BusinessUnitId = 3, InventoryId = 03, VehicleId = 03, VIN = "VIN-03", StockNumber = "StockNumber-03", Year = 2013, Make = "Make-03", Model = "Model-03", Trim = "Trim-03", DateAddedToInventory = new DateTime(1,1,12), DateOfFirstPhoto = new DateTime(1,1,15), DateOfFirstCompleteAd = new DateTime(1,1,20), inventoryActive = 1}, // photo days 3    complete days 8
                new TestData { BusinessUnitId = 3, InventoryId = 04, VehicleId = 04, VIN = "VIN-04", StockNumber = "StockNumber-04", Year = 2013, Make = "Make-04", Model = "Model-04", Trim = "Trim-04", DateAddedToInventory = new DateTime(1,1,13), DateOfFirstPhoto = new DateTime(1,1,15), DateOfFirstCompleteAd = new DateTime(1,1,20)}, // photo days 2    complete days 7
                new TestData { BusinessUnitId = 3, InventoryId = 05, VehicleId = 05, VIN = "VIN-05", StockNumber = "StockNumber-05", Year = 2013, Make = "Make-05", Model = "Model-05", Trim = "Trim-05", DateAddedToInventory = new DateTime(1,1,13), DateOfFirstPhoto = new DateTime(1,1,15), DateOfFirstCompleteAd = new DateTime(1,1,20)}, // photo days 2    complete days 7
                new TestData { BusinessUnitId = 3, InventoryId = 06, VehicleId = 06, VIN = "VIN-06", StockNumber = "StockNumber-06", Year = 2013, Make = "Make-06", Model = "Model-06", Trim = "Trim-06", DateAddedToInventory = new DateTime(1,1,13), DateOfFirstPhoto = new DateTime(1,1,15), DateOfFirstCompleteAd = new DateTime(1,1,20), inventoryActive = 0}, // photo days 2    complete days 7
                new TestData { BusinessUnitId = 3, InventoryId = 07, VehicleId = 07, VIN = "VIN-07", StockNumber = "StockNumber-07", Year = 2013, Make = "Make-07", Model = "Model-07", Trim = "Trim-07", DateAddedToInventory = new DateTime(1,1,14), DateOfFirstPhoto = new DateTime(1,1,18), DateOfFirstCompleteAd = new DateTime(1,1,20)}, // photo days 4    complete days 6
                new TestData { BusinessUnitId = 3, InventoryId = 08, VehicleId = 08, VIN = "VIN-08", StockNumber = "StockNumber-08", Year = 2013, Make = "Make-08", Model = "Model-08", Trim = "Trim-08", DateAddedToInventory = new DateTime(1,1,14), DateOfFirstPhoto = new DateTime(1,1,18), DateOfFirstCompleteAd = new DateTime(1,1,20)}, // photo days 4    complete days 6
                new TestData { BusinessUnitId = 3, InventoryId = 09, VehicleId = 09, VIN = "VIN-09", StockNumber = "StockNumber-09", Year = 2013, Make = "Make-09", Model = "Model-09", Trim = "Trim-09", DateAddedToInventory = new DateTime(1,1,14), DateOfFirstPhoto = new DateTime(1,1,18), DateOfFirstCompleteAd = new DateTime(1,1,20)}, // photo days 4    complete days 6
                new TestData { BusinessUnitId = 3, InventoryId = 10, VehicleId = 10, VIN = "VIN-10", StockNumber = "StockNumber-10", Year = 2013, Make = "Make-10", Model = "Model-10", Trim = "Trim-10", DateAddedToInventory = new DateTime(1,1,15), DateOfFirstPhoto = new DateTime(1,1,18), DateOfFirstCompleteAd = new DateTime(1,1,20)}, // photo days 3    complete days 5
                new TestData { BusinessUnitId = 3, InventoryId = 11, VehicleId = 11, VIN = "VIN-11", StockNumber = "StockNumber-11", Year = 2013, Make = "Make-11", Model = "Model-11", Trim = "Trim-11", DateAddedToInventory = new DateTime(1,1,15), DateOfFirstPhoto = new DateTime(1,1,18), DateOfFirstCompleteAd = new DateTime(1,1,20)}, // photo days 3    complete days 5
                new TestData { BusinessUnitId = 3, InventoryId = 12, VehicleId = 12, VIN = "VIN-12", StockNumber = "StockNumber-12", Year = 2013, Make = "Make-12", Model = "Model-12", Trim = "Trim-12", DateAddedToInventory = new DateTime(1,1,15), DateOfFirstPhoto = new DateTime(1,1,18), DateOfFirstCompleteAd = new DateTime(1,1,20), inventoryActive = 0}  // photo days 3    complete days 5
                // Totals:                                                                                                                                                                                                                                                                                                                                    36                 78
                // Averages:                                                                                                                                                                                                                                                                                                                                     3                 3.5
            };
        }

        internal static IEnumerable<RowItem> GetTest_VinsToUpdate(int buid)
        {
            return new List<RowItem>
            {
                new RowItem { BusinessUnitId = buid, StockNumber = "StockNumber-1", VIN = "VIN-1", InventoryId = 1, InventoryReceivedDate = new DateTime(1,2,1), PhotosFirstDate = new DateTime(1,2,1) },
                new RowItem { BusinessUnitId = buid, StockNumber = "StockNumber-2", VIN = "VIN-2", InventoryId = 2, InventoryReceivedDate = new DateTime(1,2,2), AdApprovalFirstDate = new DateTime(1,2,6), MinimumPhotoFirstDate = new DateTime(1,2,3) },
                new RowItem { BusinessUnitId = buid, StockNumber = "StockNumber-3", VIN = "VIN-3", InventoryId = 3, InventoryReceivedDate = new DateTime(1,2,3), AdApprovalFirstDate = new DateTime(1,2,6), PriceFirstDate = new DateTime(1,2,4) },
                new RowItem { BusinessUnitId = buid, StockNumber = "StockNumber-4", VIN = "VIN-4", InventoryId = 4, InventoryReceivedDate = new DateTime(1,2,4), AdApprovalFirstDate = new DateTime(1,2,6), DescriptionFirstDate = new DateTime(1,2,5) },
                new RowItem { BusinessUnitId = buid, StockNumber = "StockNumber-5", VIN = "VIN-5", InventoryId = 5, InventoryReceivedDate = new DateTime(1,2,4), AdApprovalFirstDate = new DateTime(1,2,6) },
                // This one should always have a CompleteAdFirstDate after processing
                new RowItem { BusinessUnitId = buid, StockNumber = "StockNumber-6", VIN = "VIN-6", InventoryId = 6, InventoryReceivedDate = new DateTime(1,2,4), AdApprovalFirstDate = new DateTime(1,2,6), DescriptionFirstDate = new DateTime(1,2,6), PriceFirstDate = new DateTime(1,2,5), MinimumPhotoFirstDate = new DateTime(1,2,7) },

                // Time To Market GID test data
                new RowItem { BusinessUnitId = buid, StockNumber = "StockNumber-7", VIN = "AULTEC-AutoTrader-And-Cars", InventoryId = 7, InventoryReceivedDate = new DateTime(1,2,4)},
                new RowItem { BusinessUnitId = buid, StockNumber = "StockNumber-8", VIN = "EBIZ-Only", InventoryId = 8, InventoryReceivedDate = new DateTime(1,2,4)},
                new RowItem { BusinessUnitId = buid, StockNumber = "StockNumber-9", VIN = "AULTEC-AutoTrader-Only", InventoryId = 9, InventoryReceivedDate = new DateTime(1,2,4)},
                new RowItem { BusinessUnitId = buid, StockNumber = "StockNumber-10", VIN = "EBIZ-AutoTrader-Only", InventoryId = 10, InventoryReceivedDate = new DateTime(1,2,4)},
                new RowItem { BusinessUnitId = buid, StockNumber = "StockNumber-11", VIN = "AULTEC-Cars-Only", InventoryId = 11, InventoryReceivedDate = new DateTime(1,2,4)},
                new RowItem { BusinessUnitId = buid, StockNumber = "StockNumber-12", VIN = "EBIZ-Cars-Only", InventoryId = 12, InventoryReceivedDate = new DateTime(1,2,4)},

                // EBiz And AULTec
                new RowItem { BusinessUnitId = buid, StockNumber = "StockNumber-13", VIN = "EBIZ-And-AULTEC-AutoTrader-Only", InventoryId = 13, InventoryReceivedDate = new DateTime(1,2,4)},
                new RowItem { BusinessUnitId = buid, StockNumber = "StockNumber-14", VIN = "EBIZ-And-AULTEC-Cars-Only", InventoryId = 14, InventoryReceivedDate = new DateTime(1,2,4)},

            };
        }

        /// <summary>
        /// Ad Status Results for VINS
        ///     Aultec
        ///         AULTEC-AutoTrader-And-Cars
        ///         AULTEC-AutoTrader-Only
        ///     eBiz
        ///         AULTEC-AutoTrader-And-Cars
        /// </summary>
        /// <returns></returns>
        internal static IEnumerable<AdStatusResult> GetAdStatusResults()
        {
            return new List<AdStatusResult>
            {
                new AdStatusResult
                {
                    Provider = GidProvider.Aultec,
                    Site = Site.AutoTrader,
                    Ads = new List<IMaxAnalyticsAd>
                    {
                        new MaxAnalyticsAd
                        {
                            StatusDate = DateTime.Today.AddDays(-1),
                            VIN = "AULTEC-AutoTrader-And-Cars",
                            Price = 99999999,
                            PhotoUrls =
                                new[]
                                {
                                    "http://th07.deviantart.net/fs47/PRE/f/2009/210/c/8/best_car_EVER__WP__by_core17_design.jpg",
                                    "http://upload.wikimedia.org/wikipedia/commons/d/d1/Marussia_B2_photo03.jpg"
                                }.ToList(),
                            Description = "AutoTrader and Cars AULTec"
                        },
                        new MaxAnalyticsAd
                        {
                            StatusDate = DateTime.Today.AddDays(-1),
                            VIN = "AULTEC-AutoTrader-Only",
                            Price = 99999999,
                            PhotoUrls =
                                new[]
                                {
                                    "http://th07.deviantart.net/fs47/PRE/f/2009/210/c/8/best_car_EVER__WP__by_core17_design.jpg",
                                    "http://upload.wikimedia.org/wikipedia/commons/d/d1/Marussia_B2_photo03.jpg"
                                }.ToList(),
                            Description = "AutoTrader Only AULTec"
                        },
                        new MaxAnalyticsAd
                        {
                            StatusDate = DateTime.Today.AddDays(-1),
                            VIN = "EBIZ-And-AULTEC-AutoTrader-Only",
                            Price = 99999999,
                            PhotoUrls =
                                new[]
                                {
                                    "http://th07.deviantart.net/fs47/PRE/f/2009/210/c/8/best_car_EVER__WP__by_core17_design.jpg",
                                    "http://upload.wikimedia.org/wikipedia/commons/d/d1/Marussia_B2_photo03.jpg"
                                }.ToList(),
                            Description = "AutoTrader Only eBiz and AULTec"
                        }
                    }
                },
                new AdStatusResult
                {
                    Provider = GidProvider.Aultec,
                    Site = Site.Cars,
                    Ads = new List<IMaxAnalyticsAd>
                    {
                        new MaxAnalyticsAd
                        {
                            StatusDate = DateTime.Today.AddDays(-1),
                            VIN = "AULTEC-AutoTrader-And-Cars",
                            Price = 99999999,
                            PhotoUrls =
                                new[]
                                {
                                    "http://th07.deviantart.net/fs47/PRE/f/2009/210/c/8/best_car_EVER__WP__by_core17_design.jpg",
                                    "http://upload.wikimedia.org/wikipedia/commons/d/d1/Marussia_B2_photo03.jpg"
                                }.ToList(),
                            Description = "AutoTrader and Cars AULTec"
                        },
                        new MaxAnalyticsAd
                        {
                            StatusDate = DateTime.Today.AddDays(-1),
                            VIN = "AULTEC-Cars-Only",
                            Price = 99999999,
                            PhotoUrls =
                                new[]
                                {
                                    "http://th07.deviantart.net/fs47/PRE/f/2009/210/c/8/best_car_EVER__WP__by_core17_design.jpg",
                                    "http://upload.wikimedia.org/wikipedia/commons/d/d1/Marussia_B2_photo03.jpg"
                                }.ToList(),
                            Description = "Cars only AULTec"
                        },
                        new MaxAnalyticsAd
                        {
                            StatusDate = DateTime.Today.AddDays(-1),
                            VIN = "EBIZ-And-AULTec-Cars-Only",
                            Price = 99999999,
                            PhotoUrls =
                                new[]
                                {
                                    "http://th07.deviantart.net/fs47/PRE/f/2009/210/c/8/best_car_EVER__WP__by_core17_design.jpg",
                                    "http://upload.wikimedia.org/wikipedia/commons/d/d1/Marussia_B2_photo03.jpg"
                                }.ToList(),
                            Description = "Cars Only AULTec"
                        }
                    }
                },
                new AdStatusResult
                {
                    Provider = GidProvider.eBiz,
                    Site = Site.eBiz,
                    Ads = new List<IMaxAnalyticsAd>
                    {
                        new MaxAnalyticsAd
                        {
                            StatusDate = DateTime.Today.AddDays(-1),
                            VIN = "EBIZ-Only",
                            Price = 99999999,
                            PhotoUrls =
                                new[]
                                {
                                    "http://th07.deviantart.net/fs47/PRE/f/2009/210/c/8/best_car_EVER__WP__by_core17_design.jpg",
                                    "http://upload.wikimedia.org/wikipedia/commons/d/d1/Marussia_B2_photo03.jpg"
                                }.ToList(),
                            Description = "eBiz"
                        },
                        new MaxAnalyticsAd
                        {
                            StatusDate = DateTime.Today.AddDays(-1),
                            VIN = "EBIZ-AutoTrader-Only",
                            Price = 99999999,
                            PhotoUrls =
                                new[]
                                {
                                    "http://th07.deviantart.net/fs47/PRE/f/2009/210/c/8/best_car_EVER__WP__by_core17_design.jpg",
                                    "http://upload.wikimedia.org/wikipedia/commons/d/d1/Marussia_B2_photo03.jpg"
                                }.ToList(),
                            Description = "eBiz"
                        },
                        new MaxAnalyticsAd
                        {
                            StatusDate = DateTime.Today.AddDays(-1),
                            VIN = "EBIZ-And-AULTEC-AutoTrader-Only",
                            Price = 99999999,
                            PhotoUrls =
                                new[]
                                {
                                    "http://th07.deviantart.net/fs47/PRE/f/2009/210/c/8/best_car_EVER__WP__by_core17_design.jpg",
                                    "http://upload.wikimedia.org/wikipedia/commons/d/d1/Marussia_B2_photo03.jpg"
                                }.ToList(),
                            Description = "eBiz"
                        }
                    }
                }
            }.AsEnumerable();
        }


        internal static IEnumerable<IInventoryData> FakeInventoryData()
        {
            return new List<IInventoryData>
            {
                // new vehicle
                new InventoryData(vin: "AULTEC-AutoTrader-And-Cars", inventoryType: 1, businessUnitId: 102481, stockNumber: "JL12018", unitCost: (decimal) 36100.61, certified: false, certifiedId: null, certifiedProgramId: null, mileageReceived: 2, tradeOrPurchase: 1, listPrice: 45770, receiveDate: new DateTime(2012, 02, 23), year: "2012", make: "Lincoln", model: "MKT", marketClass: "2WD Sport Utility Vehicles", segmentId: 6, trim: null, afterMarketTrim: null, inventoryId: 24020806, isLowActivity: true, exteriorStatus: 2, pricingStatus: 0, interiorStatus: 2, photoStatus: 2, adReviewStatus: 2, postingStatus: 4, baseColor: "White Platinum Tri-Coat", ext1Code: "UG", ext1Desc: "White Platinum Tri-Coat", ext2Code: null, ext2Desc: null, intCode: "TK", intDesc: "Light Stone", chromeStyleId: 333773, vehicleCatalogId: 716857, vehicleLocation: null, lastDescriptionPublicationDate: new DateTime(2012,11,27,14,13,0), statusbucket: 128, lastUpdateListPrice: 43330, desiredPhotoCount: 12, lotPrice: 45770, msrp: 45770, isDueForRepricing: true, specialPrice: 43330, hasCurrentBookValue: false, hasCarfax: false, autoLoadStatusTypeId: null, autoloadStatusEndTime: null, noPackages: false),
                // New vehicle
                new InventoryData(vin: "AULTEC-AutoTrader-Only", inventoryType: 1, businessUnitId: 102481, stockNumber: "JL12018", unitCost: (decimal) 36100.61, certified: false, certifiedId: null, certifiedProgramId: null, mileageReceived: 2, tradeOrPurchase: 1, listPrice: 45770, receiveDate: new DateTime(2012, 02, 23), year: "2012", make: "Lincoln", model: "MKT", marketClass: "2WD Sport Utility Vehicles", segmentId: 6, trim: null, afterMarketTrim: null, inventoryId: 24020806, isLowActivity: true, exteriorStatus: 2, pricingStatus: 0, interiorStatus: 2, photoStatus: 2, adReviewStatus: 2, postingStatus: 4, baseColor: "White Platinum Tri-Coat", ext1Code: "UG", ext1Desc: "White Platinum Tri-Coat", ext2Code: null, ext2Desc: null, intCode: "TK", intDesc: "Light Stone", chromeStyleId: 333773, vehicleCatalogId: 716857, vehicleLocation: null, lastDescriptionPublicationDate: new DateTime(2012,11,27,14,13,0), statusbucket: 128, lastUpdateListPrice: 43330, desiredPhotoCount: 12, lotPrice: 45770, msrp: 45770, isDueForRepricing: true, specialPrice: 43330, hasCurrentBookValue: false, hasCarfax: false, autoLoadStatusTypeId: null, autoloadStatusEndTime: null, noPackages: false), 
                // New vehicle
                new InventoryData(vin: "AULTEC-Cars-Only", inventoryType: 1, businessUnitId: 102481, stockNumber: "JL12018", unitCost: (decimal) 36100.61, certified: false, certifiedId: null, certifiedProgramId: null, mileageReceived: 2, tradeOrPurchase: 1, listPrice: 45770, receiveDate: new DateTime(2012, 02, 23), year: "2012", make: "Lincoln", model: "MKT", marketClass: "2WD Sport Utility Vehicles", segmentId: 6, trim: null, afterMarketTrim: null, inventoryId: 24020806, isLowActivity: true, exteriorStatus: 2, pricingStatus: 0, interiorStatus: 2, photoStatus: 2, adReviewStatus: 2, postingStatus: 4, baseColor: "White Platinum Tri-Coat", ext1Code: "UG", ext1Desc: "White Platinum Tri-Coat", ext2Code: null, ext2Desc: null, intCode: "TK", intDesc: "Light Stone", chromeStyleId: 333773, vehicleCatalogId: 716857, vehicleLocation: null, lastDescriptionPublicationDate: new DateTime(2012,11,27,14,13,0), statusbucket: 128, lastUpdateListPrice: 43330, desiredPhotoCount: 12, lotPrice: 45770, msrp: 45770, isDueForRepricing: true, specialPrice: 43330, hasCurrentBookValue: false, hasCarfax: false, autoLoadStatusTypeId: null, autoloadStatusEndTime: null, noPackages: false), 
                // Used vehicle
                new InventoryData(vin: "EBIZ-Only", inventoryType: 2, businessUnitId: 102481, stockNumber: "JL12018", unitCost: (decimal) 36100.61, certified: false, certifiedId: null, certifiedProgramId: null, mileageReceived: 2, tradeOrPurchase: 1, listPrice: 45770, receiveDate: new DateTime(2012, 02, 23), year: "2012", make: "Lincoln", model: "MKT", marketClass: "2WD Sport Utility Vehicles", segmentId: 6, trim: null, afterMarketTrim: null, inventoryId: 24020806, isLowActivity: true, exteriorStatus: 2, pricingStatus: 0, interiorStatus: 2, photoStatus: 2, adReviewStatus: 2, postingStatus: 4, baseColor: "White Platinum Tri-Coat", ext1Code: "UG", ext1Desc: "White Platinum Tri-Coat", ext2Code: null, ext2Desc: null, intCode: "TK", intDesc: "Light Stone", chromeStyleId: 333773, vehicleCatalogId: 716857, vehicleLocation: null, lastDescriptionPublicationDate: new DateTime(2012,11,27,14,13,0), statusbucket: 128, lastUpdateListPrice: 43330, desiredPhotoCount: 12, lotPrice: 45770, msrp: 45770, isDueForRepricing: true, specialPrice: 43330, hasCurrentBookValue: false, hasCarfax: false, autoLoadStatusTypeId: null, autoloadStatusEndTime: null, noPackages: false), 
                // USED vehicle
                new InventoryData(vin: "EBIZ-AutoTrader-Only", inventoryType: 2, businessUnitId: 102481, stockNumber: "JL12018", unitCost: (decimal) 36100.61, certified: false, certifiedId: null, certifiedProgramId: null, mileageReceived: 2, tradeOrPurchase: 1, listPrice: 45770, receiveDate: new DateTime(2012, 02, 23), year: "2012", make: "Lincoln", model: "MKT", marketClass: "2WD Sport Utility Vehicles", segmentId: 6, trim: null, afterMarketTrim: null, inventoryId: 24020806, isLowActivity: true, exteriorStatus: 2, pricingStatus: 0, interiorStatus: 2, photoStatus: 2, adReviewStatus: 2, postingStatus: 4, baseColor: "White Platinum Tri-Coat", ext1Code: "UG", ext1Desc: "White Platinum Tri-Coat", ext2Code: null, ext2Desc: null, intCode: "TK", intDesc: "Light Stone", chromeStyleId: 333773, vehicleCatalogId: 716857, vehicleLocation: null, lastDescriptionPublicationDate: new DateTime(2012,11,27,14,13,0), statusbucket: 128, lastUpdateListPrice: 43330, desiredPhotoCount: 12, lotPrice: 45770, msrp: 45770, isDueForRepricing: true, specialPrice: 43330, hasCurrentBookValue: false, hasCarfax: false, autoLoadStatusTypeId: null, autoloadStatusEndTime: null, noPackages: false), 
                // Used vehicle
                new InventoryData(vin: "EBIZ-Cars-Only", inventoryType: 2, businessUnitId: 102481, stockNumber: "JL12018", unitCost: (decimal) 36100.61, certified: false, certifiedId: null, certifiedProgramId: null, mileageReceived: 2, tradeOrPurchase: 1, listPrice: 45770, receiveDate: new DateTime(2012, 02, 23), year: "2012", make: "Lincoln", model: "MKT", marketClass: "2WD Sport Utility Vehicles", segmentId: 6, trim: null, afterMarketTrim: null, inventoryId: 24020806, isLowActivity: true, exteriorStatus: 2, pricingStatus: 0, interiorStatus: 2, photoStatus: 2, adReviewStatus: 2, postingStatus: 4, baseColor: "White Platinum Tri-Coat", ext1Code: "UG", ext1Desc: "White Platinum Tri-Coat", ext2Code: null, ext2Desc: null, intCode: "TK", intDesc: "Light Stone", chromeStyleId: 333773, vehicleCatalogId: 716857, vehicleLocation: null, lastDescriptionPublicationDate: new DateTime(2012,11,27,14,13,0), statusbucket: 128, lastUpdateListPrice: 43330, desiredPhotoCount: 12, lotPrice: 45770, msrp: 45770, isDueForRepricing: true, specialPrice: 43330, hasCurrentBookValue: false, hasCarfax: false, autoLoadStatusTypeId: null, autoloadStatusEndTime: null, noPackages: false), 
                // Used vehicle
                new InventoryData(vin: "EBIZ-And-AULTEC-Cars-Only", inventoryType: 2, businessUnitId: 102481, stockNumber: "JL12018", unitCost: (decimal) 36100.61, certified: false, certifiedId: null, certifiedProgramId: null, mileageReceived: 2, tradeOrPurchase: 1, listPrice: 45770, receiveDate: new DateTime(2012, 02, 23), year: "2012", make: "Lincoln", model: "MKT", marketClass: "2WD Sport Utility Vehicles", segmentId: 6, trim: null, afterMarketTrim: null, inventoryId: 24020806, isLowActivity: true, exteriorStatus: 2, pricingStatus: 0, interiorStatus: 2, photoStatus: 2, adReviewStatus: 2, postingStatus: 4, baseColor: "White Platinum Tri-Coat", ext1Code: "UG", ext1Desc: "White Platinum Tri-Coat", ext2Code: null, ext2Desc: null, intCode: "TK", intDesc: "Light Stone", chromeStyleId: 333773, vehicleCatalogId: 716857, vehicleLocation: null, lastDescriptionPublicationDate: new DateTime(2012,11,27,14,13,0), statusbucket: 128, lastUpdateListPrice: 43330, desiredPhotoCount: 12, lotPrice: 45770, msrp: 45770, isDueForRepricing: true, specialPrice: 43330, hasCurrentBookValue: false, hasCarfax: false, autoLoadStatusTypeId: null, autoloadStatusEndTime: null, noPackages: false), 
                // New vehicle
                new InventoryData(vin: "EBIZ-And-AULTEC-AutoTrader-Only", inventoryType: 1, businessUnitId: 102481, stockNumber: "JL12018", unitCost: (decimal) 36100.61, certified: false, certifiedId: null, certifiedProgramId: null, mileageReceived: 2, tradeOrPurchase: 1, listPrice: 45770, receiveDate: new DateTime(2012, 02, 23), year: "2012", make: "Lincoln", model: "MKT", marketClass: "2WD Sport Utility Vehicles", segmentId: 6, trim: null, afterMarketTrim: null, inventoryId: 24020806, isLowActivity: true, exteriorStatus: 2, pricingStatus: 0, interiorStatus: 2, photoStatus: 2, adReviewStatus: 2, postingStatus: 4, baseColor: "White Platinum Tri-Coat", ext1Code: "UG", ext1Desc: "White Platinum Tri-Coat", ext2Code: null, ext2Desc: null, intCode: "TK", intDesc: "Light Stone", chromeStyleId: 333773, vehicleCatalogId: 716857, vehicleLocation: null, lastDescriptionPublicationDate: new DateTime(2012,11,27,14,13,0), statusbucket: 128, lastUpdateListPrice: 43330, desiredPhotoCount: 12, lotPrice: 45770, msrp: 45770, isDueForRepricing: true, specialPrice: 43330, hasCurrentBookValue: false, hasCarfax: false, autoLoadStatusTypeId: null, autoloadStatusEndTime: null, noPackages: false)
            };
        }


        #region Fill With Gid Tests
        internal static IEnumerable<AdStatusResult> FillTestAdStatusResults()
        {
            var updatableAdStatusResults = new List<AdStatusResult>();
            var aultecResult =
                new AdStatusResult
                {
                    Provider = GidProvider.Aultec,
                    Site = Site.AutoTrader,
                    Ads = new List<IMaxAnalyticsAd>
                    {
                        // Ad with everything.
                        new MaxAnalyticsAd
                        {
                            StatusDate = DateTime.Today.AddDays(-1),
                            VIN = "Everything",
                            Price = 99999999,
                            PhotoUrls =
                                new[]
                                {
                                    "http://th07.deviantart.net/fs47/PRE/f/2009/210/c/8/best_car_EVER__WP__by_core17_design.jpg",
                                    "http://upload.wikimedia.org/wikipedia/commons/d/d1/Marussia_B2_photo03.jpg"
                                }.ToList(),
                            Description = "AutoTrader and Cars AULTec"
                        },

                        // Ad with one photo url.
                        new MaxAnalyticsAd
                        {
                            StatusDate = DateTime.Today.AddDays(-1),
                            VIN = "OnePhoto",
                            PhotoUrls =
                                new[]
                                {
                                    "http://upload.wikimedia.org/wikipedia/commons/d/d1/Marussia_B2_photo03.jpg"
                                }.ToList()
                        },

                        // Ad with multiple photo urls.
                        new MaxAnalyticsAd
                        {
                            StatusDate = DateTime.Today.AddDays(-1),
                            VIN = "ManyPhotos",
                            PhotoUrls =
                                new[]
                                {
                                    "http://th07.deviantart.net/fs47/PRE/f/2009/210/c/8/best_car_EVER__WP__by_core17_design.jpg",
                                    "http://upload.wikimedia.org/wikipedia/commons/d/d1/Marussia_B2_photo03.jpg"
                                }.ToList()
                        },

                        // Ad with price.
                        new MaxAnalyticsAd
                        {
                            StatusDate = DateTime.Today.AddDays(-1),
                            VIN = "Price",
                            Price = 123
                        },

                        // Ad with negative price.
                        new MaxAnalyticsAd
                        {
                            StatusDate = DateTime.Today.AddDays(-1),
                            VIN = "NegPrice",
                            Price = -423
                        },

                        // Ad with Description.
                        new MaxAnalyticsAd
                        {
                            StatusDate = DateTime.Today.AddDays(-1),
                            VIN = "Description",
                            Description = "This is a description."
                        },

                        // Ad with Empty Description.
                        new MaxAnalyticsAd
                        {
                            StatusDate = DateTime.Today.AddDays(-1),
                            VIN = "EmptyDescription",
                            Description = ""
                        }
                    }
                };

            updatableAdStatusResults.Add(aultecResult);

            return updatableAdStatusResults;
        }

        internal static IEnumerable<IInventoryData> FillTestInventoryData()
        {
            return new List<IInventoryData>
            {
                // Everything
                new InventoryData(vin: "Everything", inventoryType: 1, businessUnitId: 102481, stockNumber: "JL12018", unitCost: (decimal) 36100.61,
                    certified: false, certifiedId: null, certifiedProgramId: null, mileageReceived: 2, tradeOrPurchase: 1, listPrice: 45770, receiveDate: new DateTime(2012, 02, 23), year: "2012",
                    make: "Lincoln", model: "MKT", marketClass: "2WD Sport Utility Vehicles", segmentId: 6, trim: null, afterMarketTrim: null, inventoryId: 24020806,
                    isLowActivity: true, exteriorStatus: 2, pricingStatus: 0, interiorStatus: 2, photoStatus: 2, adReviewStatus: 2, postingStatus: 4,
                    baseColor: "White Platinum Tri-Coat", ext1Code: "UG", ext1Desc: "White Platinum Tri-Coat", ext2Code: null, ext2Desc: null, intCode: "TK", intDesc: "Light Stone",
                    chromeStyleId: 333773, vehicleCatalogId: 716857, vehicleLocation: null, lastDescriptionPublicationDate: new DateTime(2012, 11, 27, 14, 13, 0), statusbucket: 128,
                    lastUpdateListPrice: 43330, desiredPhotoCount: 12, lotPrice: 45770, msrp: 45770, isDueForRepricing: true, specialPrice: 43330, hasCurrentBookValue: false,
                    hasCarfax: false, autoLoadStatusTypeId: null, autoloadStatusEndTime: null, noPackages: false),
                // One Photo
                new InventoryData(vin: "OnePhoto", inventoryType: 1, businessUnitId: 102481, stockNumber: "JL12018", unitCost: (decimal) 36100.61,
                    certified: false, certifiedId: null,certifiedProgramId: null, mileageReceived: 2, tradeOrPurchase: 1, listPrice: 45770, receiveDate: new DateTime(2012, 02, 23), year: "2012",
                    make: "Lincoln", model: "MKT", marketClass: "2WD Sport Utility Vehicles", segmentId: 6, trim: null, afterMarketTrim: null, inventoryId: 24020806,
                    isLowActivity: true, exteriorStatus: 2, pricingStatus: 0, interiorStatus: 2, photoStatus: 2, adReviewStatus: 2, postingStatus: 4,
                    baseColor: "White Platinum Tri-Coat", ext1Code: "UG", ext1Desc: "White Platinum Tri-Coat", ext2Code: null, ext2Desc: null, intCode: "TK", intDesc: "Light Stone",
                    chromeStyleId: 333773, vehicleCatalogId: 716857, vehicleLocation: null, lastDescriptionPublicationDate: new DateTime(2012, 11, 27, 14, 13, 0), statusbucket: 128,
                    lastUpdateListPrice: 43330, desiredPhotoCount: 12, lotPrice: 45770, msrp: 45770, isDueForRepricing: true, specialPrice: 43330, hasCurrentBookValue: false,
                    hasCarfax: false, autoLoadStatusTypeId: null, autoloadStatusEndTime: null, noPackages: false),

                // Multiple Photos
                new InventoryData(vin: "ManyPhotos", inventoryType: 1, businessUnitId: 102481, stockNumber: "JL12018", unitCost: (decimal) 36100.61,
                    certified: false, certifiedId: null,certifiedProgramId: null, mileageReceived: 2, tradeOrPurchase: 1, listPrice: 45770, receiveDate: new DateTime(2012, 02, 23), year: "2012",
                    make: "Lincoln", model: "MKT", marketClass: "2WD Sport Utility Vehicles", segmentId: 6, trim: null, afterMarketTrim: null, inventoryId: 24020806,
                    isLowActivity: true, exteriorStatus: 2, pricingStatus: 0, interiorStatus: 2, photoStatus: 2, adReviewStatus: 2, postingStatus: 4,
                    baseColor: "White Platinum Tri-Coat", ext1Code: "UG", ext1Desc: "White Platinum Tri-Coat", ext2Code: null, ext2Desc: null, intCode: "TK", intDesc: "Light Stone",
                    chromeStyleId: 333773, vehicleCatalogId: 716857, vehicleLocation: null, lastDescriptionPublicationDate: new DateTime(2012, 11, 27, 14, 13, 0), statusbucket: 128,
                    lastUpdateListPrice: 43330, desiredPhotoCount: 12, lotPrice: 45770, msrp: 45770, isDueForRepricing: true, specialPrice: 43330, hasCurrentBookValue: false,
                    hasCarfax: false, autoLoadStatusTypeId: null, autoloadStatusEndTime: null, noPackages: false),

                // Price
                new InventoryData(vin: "Price", inventoryType: 1, businessUnitId: 102481, stockNumber: "JL12018", unitCost: (decimal) 36100.61,
                    certified: false, certifiedId: null,certifiedProgramId: null, mileageReceived: 2, tradeOrPurchase: 1, listPrice: 45770, receiveDate: new DateTime(2012, 02, 23), year: "2012",
                    make: "Lincoln", model: "MKT", marketClass: "2WD Sport Utility Vehicles", segmentId: 6, trim: null, afterMarketTrim: null, inventoryId: 24020806,
                    isLowActivity: true, exteriorStatus: 2, pricingStatus: 0, interiorStatus: 2, photoStatus: 2, adReviewStatus: 2, postingStatus: 4,
                    baseColor: "White Platinum Tri-Coat", ext1Code: "UG", ext1Desc: "White Platinum Tri-Coat", ext2Code: null, ext2Desc: null, intCode: "TK", intDesc: "Light Stone",
                    chromeStyleId: 333773, vehicleCatalogId: 716857, vehicleLocation: null, lastDescriptionPublicationDate: new DateTime(2012, 11, 27, 14, 13, 0), statusbucket: 128,
                    lastUpdateListPrice: 43330, desiredPhotoCount: 12, lotPrice: 45770, msrp: 45770, isDueForRepricing: true, specialPrice: 43330, hasCurrentBookValue: false,
                    hasCarfax: false, autoLoadStatusTypeId: null, autoloadStatusEndTime: null, noPackages: false),

                // Negative Price
                new InventoryData(vin: "NegPrice", inventoryType: 1, businessUnitId: 102481, stockNumber: "JL12018", unitCost: (decimal) 36100.61,
                    certified: false, certifiedId: null,certifiedProgramId: null, mileageReceived: 2, tradeOrPurchase: 1, listPrice: 45770, receiveDate: new DateTime(2012, 02, 23), year: "2012",
                    make: "Lincoln", model: "MKT", marketClass: "2WD Sport Utility Vehicles", segmentId: 6, trim: null, afterMarketTrim: null, inventoryId: 24020806,
                    isLowActivity: true, exteriorStatus: 2, pricingStatus: 0, interiorStatus: 2, photoStatus: 2, adReviewStatus: 2, postingStatus: 4,
                    baseColor: "White Platinum Tri-Coat", ext1Code: "UG", ext1Desc: "White Platinum Tri-Coat", ext2Code: null, ext2Desc: null, intCode: "TK", intDesc: "Light Stone",
                    chromeStyleId: 333773, vehicleCatalogId: 716857, vehicleLocation: null, lastDescriptionPublicationDate: new DateTime(2012, 11, 27, 14, 13, 0), statusbucket: 128,
                    lastUpdateListPrice: 43330, desiredPhotoCount: 12, lotPrice: 45770, msrp: 45770, isDueForRepricing: true, specialPrice: 43330, hasCurrentBookValue: false,
                    hasCarfax: false, autoLoadStatusTypeId: null, autoloadStatusEndTime: null, noPackages: false),

                // Description
                new InventoryData(vin: "Description", inventoryType: 1, businessUnitId: 102481, stockNumber: "JL12018", unitCost: (decimal) 36100.61,
                    certified: false, certifiedId: null,certifiedProgramId: null, mileageReceived: 2, tradeOrPurchase: 1, listPrice: 45770, receiveDate: new DateTime(2012, 02, 23), year: "2012",
                    make: "Lincoln", model: "MKT", marketClass: "2WD Sport Utility Vehicles", segmentId: 6, trim: null, afterMarketTrim: null, inventoryId: 24020806,
                    isLowActivity: true, exteriorStatus: 2, pricingStatus: 0, interiorStatus: 2, photoStatus: 2, adReviewStatus: 2, postingStatus: 4,
                    baseColor: "White Platinum Tri-Coat", ext1Code: "UG", ext1Desc: "White Platinum Tri-Coat", ext2Code: null, ext2Desc: null, intCode: "TK", intDesc: "Light Stone",
                    chromeStyleId: 333773, vehicleCatalogId: 716857, vehicleLocation: null, lastDescriptionPublicationDate: new DateTime(2012, 11, 27, 14, 13, 0), statusbucket: 128,
                    lastUpdateListPrice: 43330, desiredPhotoCount: 12, lotPrice: 45770, msrp: 45770, isDueForRepricing: true, specialPrice: 43330, hasCurrentBookValue: false,
                    hasCarfax: false, autoLoadStatusTypeId: null, autoloadStatusEndTime: null, noPackages: false),

                // Description
                new InventoryData(vin: "EmptyDescription", inventoryType: 1, businessUnitId: 102481, stockNumber: "JL12018", unitCost: (decimal) 36100.61,
                    certified: false, certifiedId: null,certifiedProgramId: null, mileageReceived: 2, tradeOrPurchase: 1, listPrice: 45770, receiveDate: new DateTime(2012, 02, 23), year: "2012",
                    make: "Lincoln", model: "MKT", marketClass: "2WD Sport Utility Vehicles", segmentId: 6, trim: null, afterMarketTrim: null, inventoryId: 24020806,
                    isLowActivity: true, exteriorStatus: 2, pricingStatus: 0, interiorStatus: 2, photoStatus: 2, adReviewStatus: 2, postingStatus: 4,
                    baseColor: "White Platinum Tri-Coat", ext1Code: "UG", ext1Desc: "White Platinum Tri-Coat", ext2Code: null, ext2Desc: null, intCode: "TK", intDesc: "Light Stone",
                    chromeStyleId: 333773, vehicleCatalogId: 716857, vehicleLocation: null, lastDescriptionPublicationDate: new DateTime(2012, 11, 27, 14, 13, 0), statusbucket: 128,
                    lastUpdateListPrice: 43330, desiredPhotoCount: 12, lotPrice: 45770, msrp: 45770, isDueForRepricing: true, specialPrice: 43330, hasCurrentBookValue: false,
                    hasCarfax: false, autoLoadStatusTypeId: null, autoloadStatusEndTime: null, noPackages: false)
            };
        }

        internal static IEnumerable<RowItem> FillTestUpdatableVins(int buid)
        {
            return new List<RowItem>
            {
                new RowItem
                {
                    VIN = "Everything",
                    BusinessUnitId = buid,
                    StockNumber = "StockNumber-1",
                    InventoryId = 1,
                    InventoryReceivedDate = new DateTime(1, 2, 1)

                },
                new RowItem
                {
                    VIN = "OnePhoto",
                    BusinessUnitId = buid,
                    StockNumber = "StockNumber-2",
                    InventoryId = 2,
                    InventoryReceivedDate = new DateTime(1, 2, 2)
                },
                new RowItem
                {
                    VIN = "ManyPhotos",
                    BusinessUnitId = buid,
                    StockNumber = "StockNumber-3",
                    InventoryId = 3,
                    InventoryReceivedDate = new DateTime(1, 2, 3)
                },
                new RowItem
                {
                    VIN = "Price",
                    BusinessUnitId = buid,
                    StockNumber = "StockNumber-4",
                    InventoryId = 4,
                    InventoryReceivedDate = new DateTime(1, 2, 4)
                },
                new RowItem
                {
                    VIN = "NegPrice",
                    BusinessUnitId = buid,
                    StockNumber = "StockNumber-4",
                    InventoryId = 4,
                    InventoryReceivedDate = new DateTime(1, 2, 4)
                },
                new RowItem
                {
                    VIN = "Description",
                    BusinessUnitId = buid,
                    StockNumber = "StockNumber-4",
                    InventoryId = 4,
                    InventoryReceivedDate = new DateTime(1, 2, 4)
                },
                new RowItem
                {
                    VIN = "EmptyDescription",
                    BusinessUnitId = buid,
                    StockNumber = "StockNumber-4",
                    InventoryId = 4,
                    InventoryReceivedDate = new DateTime(1, 2, 4)
                }
            };
        }

        #endregion Fill With Gid Tests

        #endregion TestData
    }
}
