﻿using System;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket.Items;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Reports.TimeToMarket
{
    [TestFixture]
    public class RowItemTests
    {
        [Test]
        public void Nullable_Date_Fields_Cannot_Be_Overwritten()
        {
            // Get a time to market row item.
            // Set the fields
            // make sure they are not null
            // set the fields
            // make sure the values did not get overwritten.
            var rowItem = new RowItem();

            var yesterday = DateTime.Today.AddDays(-1);
            var today = DateTime.Today;


            // Set the nullable fields to yesterday.
            rowItem.AdApprovalFirstDate = yesterday;
            rowItem.CompleteAdFirstDate = yesterday;
            rowItem.DescriptionFirstDate = yesterday;
            rowItem.GidFirstDate = yesterday;
            rowItem.MinimumPhotoFirstDate = yesterday;
            rowItem.PhotosFirstDate = yesterday;
            rowItem.PriceFirstDate = yesterday;
            rowItem.MAXMinimumPhotoFirstDate = yesterday;
            rowItem.MAXPhotoFirstDate = yesterday;
            rowItem.MAXPriceFirstDate = yesterday;
            rowItem.MAXDescriptionFirstDate = yesterday;

            Assert.AreEqual(rowItem.AdApprovalFirstDate, yesterday);
            Assert.AreEqual(rowItem.CompleteAdFirstDate, yesterday);
            Assert.AreEqual(rowItem.DescriptionFirstDate, yesterday);
            Assert.AreEqual(rowItem.GidFirstDate, yesterday);
            Assert.AreEqual(rowItem.MinimumPhotoFirstDate, yesterday);
            Assert.AreEqual(rowItem.PhotosFirstDate, yesterday);
            Assert.AreEqual(rowItem.PriceFirstDate, yesterday);
            Assert.AreEqual(rowItem.MAXMinimumPhotoFirstDate, yesterday);
            Assert.AreEqual(rowItem.MAXPhotoFirstDate, yesterday);
            Assert.AreEqual(rowItem.MAXPriceFirstDate, yesterday);
            Assert.AreEqual(rowItem.MAXDescriptionFirstDate, yesterday);

            // Set the nullable fields to today.
            rowItem.AdApprovalFirstDate = today;
            rowItem.MAXMinimumPhotoFirstDate = today;
            rowItem.MAXPhotoFirstDate = today;
            rowItem.MAXPriceFirstDate = today;
            rowItem.MAXDescriptionFirstDate = today;

            // Re-test values.
            Assert.AreEqual(rowItem.AdApprovalFirstDate, yesterday);
            Assert.AreEqual(rowItem.CompleteAdFirstDate, yesterday);
            Assert.AreEqual(rowItem.DescriptionFirstDate, yesterday);
            Assert.AreEqual(rowItem.GidFirstDate, yesterday);
            Assert.AreEqual(rowItem.MinimumPhotoFirstDate, yesterday);
            Assert.AreEqual(rowItem.PhotosFirstDate, yesterday);
            Assert.AreEqual(rowItem.PriceFirstDate, yesterday);
            Assert.AreEqual(rowItem.MAXMinimumPhotoFirstDate, yesterday);
            Assert.AreEqual(rowItem.MAXPhotoFirstDate, yesterday);
            Assert.AreEqual(rowItem.MAXPriceFirstDate, yesterday);
            Assert.AreEqual(rowItem.MAXDescriptionFirstDate, yesterday);
        }

        [Test]
        public void Can_GetDataColumns()
        {
            var columns = RowItemHelpers.GetDataColumns();

            Assert.That(columns.Length == 14);
            Assert.That(columns.Count(dc => dc.ColumnName == "BusinessUnitId") == 1);
            Assert.That(columns.Count(dc => dc.ColumnName == "InventoryId") == 1);
            Assert.That(columns.Count(dc => dc.ColumnName == "VIN") == 1);
            Assert.That(columns.Count(dc => dc.ColumnName == "GIDFirstDate") == 1);
            Assert.That(columns.Count(dc => dc.ColumnName == "PhotosFirstDate") == 1);
            Assert.That(columns.Count(dc => dc.ColumnName == "DescriptionFirstDate") == 1);
            Assert.That(columns.Count(dc => dc.ColumnName == "PriceFirstDate") == 1);
            Assert.That(columns.Count(dc => dc.ColumnName == "AdApprovalFirstDate") == 1);
            Assert.That(columns.Count(dc => dc.ColumnName == "CompleteAdFirstDate") == 1);
            Assert.That(columns.Count(dc => dc.ColumnName == "MinimumPhotosFirstDate") == 1);
            Assert.That(columns.Count(dc => dc.ColumnName == "MAXMinimumPhotosFirstDate") == 1);
            Assert.That(columns.Count(dc => dc.ColumnName == "MAXPhotosFirstDate") == 1);
            Assert.That(columns.Count(dc => dc.ColumnName == "MAXPriceFirstDate") == 1);
            Assert.That(columns.Count(dc => dc.ColumnName == "MAXDescriptionFirstDate") == 1);
        }
    }
}
