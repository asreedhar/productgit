﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Enumerations;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.AdStatus;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket.Items;
using MAX.Entities;
using MAX.Entities.Enumerations;
using MAX.TimeToMarket.Queues;
using MAX.TimeToMarket.Tasks;
using Merchandising.Messages.TimeToMarket;
using Moq;
using NUnit.Framework;


namespace MerchandisingDomainModelTests.Reports.TimeToMarket
{
    [TestFixture]
    public class TimeToMarketTaskTests
    {
        private IAdStatusRepository _adStatusRepository;
        private IContainer _container;


        [TestFixtureSetUp]
        public void SetupFixture()
        {
            _adStatusRepository = new Mock<IAdStatusRepository>().Object;
        }
        [Test]
        public void Task_Calls_Repo_Methods()
        {
            var ttmRepo = new Mock<ITimeToMarketRepository>();
            var queueFactory = new Mock<ITimeToMarketQueueFactory>();
            var reportDates = new Mock<IReportStartDates>();

            var taskMessage = new TimeToMarketGenerationMessage(1);

            RegisterSettingsObject();

            reportDates.SetupGet(x => x.FirstPhotoStartDate).Returns(new DateTime(1, 2, 1));
            reportDates.SetupGet(x => x.CompleteStartDate).Returns(new DateTime(1, 2, 3));

            var testVinsToUpdate = TestData.TimeToMarketTestData.GetTest_VinsToUpdate(taskMessage.BusinessUnitId);
            
            ttmRepo.Setup(
                x =>
                    x.GetVinData(It.Is<int>(i => i == 1), It.Is<DateTime>(d => d == new DateTime(1, 2, 1)))
            ).Returns(testVinsToUpdate);

            var task = new TimeToMarketGenerationTask(queueFactory.Object, ttmRepo.Object, reportDates.Object);

            task.Process(taskMessage);

            ttmRepo.Verify(ttm => ttm.Initialize(It.IsAny<int>(), It.IsAny<GidProviders>()), Times.Once());
            ttmRepo.Verify(ttm => ttm.GetVinData(It.Is<int>(i => i == taskMessage.BusinessUnitId), It.Is<DateTime>(dt => dt == reportDates.Object.FirstPhotoStartDate)));

            ttmRepo.Verify(ttm => ttm.FillAdApprovalFirstDate(It.IsAny<IEnumerable<RowItem>>()), Times.Once());
            ttmRepo.Verify(ttm => ttm.FillCompleteAdFirstDate(It.IsAny<IEnumerable<RowItem>>()), Times.Once());

            // Time to Market (GID Return)
            ttmRepo.Verify(ttm => ttm.FillMinimumPhotosFirstDate(It.IsAny<IEnumerable<RowItem>>(), It.Is<DateTime>(dt => dt == new DateTime(1, 2, 1))), Times.Once());
            ttmRepo.Verify(ttm => ttm.FillPhotosFirstDate(It.IsAny<IEnumerable<RowItem>>(), It.Is<DateTime>(dt => dt == new DateTime(1, 2, 1))), Times.Once());
            ttmRepo.Verify(ttm => ttm.FillPriceFirstDate(It.IsAny<IEnumerable<RowItem>>()), Times.Once());
            ttmRepo.Verify(ttm => ttm.FillDescriptionFirstDate(It.IsAny<IEnumerable<RowItem>>()), Times.Once());
            
            // Time to Max (locally trackable)
            ttmRepo.Verify(ttm => ttm.FillMAXPhotosFirstDate(It.IsAny<IEnumerable<RowItem>>(), It.Is<DateTime>(dt => dt == new DateTime(1, 2, 1))), Times.Once());
            ttmRepo.Verify(ttm => ttm.FillMAXMinimumPhotosFirstDate(It.IsAny<IEnumerable<RowItem>>(), It.Is<DateTime>(dt => dt == new DateTime(1, 2, 3))), Times.Once());
            ttmRepo.Verify(ttm => ttm.FillMAXPriceFirstDate(It.IsAny<IEnumerable<RowItem>>()), Times.Once());
            ttmRepo.Verify(ttm => ttm.FillMAXDescriptionFirstDate(It.IsAny<IEnumerable<RowItem>>()), Times.Once());
            
            ttmRepo.Verify(ttm => ttm.UpdateVins(It.IsAny<IEnumerable<RowItem>>()), Times.Once());
            ttmRepo.Verify(ttm => ttm.FetchReport(It.IsAny<int>(), It.IsAny<VehicleType>(), It.IsAny<DateRange>()), Times.Never());
        }

        [Test]
        public void Task_Passes_Correct_Lists()
        {
            var ttmRepo = new Mock<ITimeToMarketRepository>();
            var queueFactory = new Mock<ITimeToMarketQueueFactory>();
            var reportDates = new Mock<IReportStartDates>();
            var taskMessage = new TimeToMarketGenerationMessage(1);

            int fillPhotosCount = 0,
                fillPriceCount = 0,
                fillDescriptionCount = 0,
                fillAdApprovalCount = 0,
                fillMinimumPhotosCount = 0,
                fillCompleteAdCount = 0,
                updateCount = 0;

            reportDates.SetupGet(x => x.FirstPhotoStartDate).Returns(new DateTime(1, 2, 1));
            reportDates.SetupGet(x => x.CompleteStartDate).Returns(new DateTime(1, 2, 3));

            ttmRepo
                .Setup(x => x.GetVinData(It.Is<int>(i => i == 1), It.Is<DateTime>(d => d == reportDates.Object.FirstPhotoStartDate)))
                .Returns(TestData.TimeToMarketTestData.GetTest_VinsToUpdate(taskMessage.BusinessUnitId));

            ttmRepo
                .Setup(x => x.FillPhotosFirstDate(It.IsAny<IEnumerable<RowItem>>(), It.Is<DateTime>(dt => dt == reportDates.Object.FirstPhotoStartDate)))
                .Callback<IEnumerable<RowItem>, DateTime>((list, d) => fillPhotosCount = list.Count());
            
            ttmRepo
                .Setup(x => x.FillMinimumPhotosFirstDate(It.IsAny<IEnumerable<RowItem>>(), It.Is<DateTime>(dt => dt == reportDates.Object.FirstPhotoStartDate)))
                .Callback<IEnumerable<RowItem>, DateTime>((l, d) => fillMinimumPhotosCount = l.Count());

            // Must use callbacks to get the values that are being passed into the repos
            ttmRepo
                .Setup(x => x.FillPriceFirstDate(It.IsAny<IEnumerable<RowItem>>()))
                .Callback<IEnumerable<RowItem>>(l => fillPriceCount = l.Count());
            ttmRepo
                .Setup(x => x.FillDescriptionFirstDate(It.IsAny<IEnumerable<RowItem>>()))
                .Callback<IEnumerable<RowItem>>(l => fillDescriptionCount = l.Count());
            ttmRepo
                .Setup(x => x.FillAdApprovalFirstDate(It.IsAny<IEnumerable<RowItem>>()))
                .Callback<IEnumerable<RowItem>>(l => fillAdApprovalCount = l.Count());
            ttmRepo
                .Setup(x => x.FillCompleteAdFirstDate(It.IsAny<IEnumerable<RowItem>>()))
                .Callback<IEnumerable<RowItem>>(l => fillCompleteAdCount = l.Count());
            ttmRepo
                .Setup(x => x.UpdateVins(It.IsAny<IEnumerable<RowItem>>()))
                .Callback<IEnumerable<RowItem>>(l => updateCount = l.Count());

            var task = new TimeToMarketGenerationTask(queueFactory.Object, ttmRepo.Object, reportDates.Object);


            RegisterSettingsObject();

            task.Process(taskMessage);

            Assert.AreEqual(11, fillPhotosCount);
            Assert.AreEqual(11, fillPriceCount);
            Assert.AreEqual(11, fillDescriptionCount);
            Assert.AreEqual(11, fillAdApprovalCount);
            Assert.AreEqual(11, fillMinimumPhotosCount);
            Assert.AreEqual(11, fillCompleteAdCount);
            Assert.AreEqual(14, updateCount);
        }

        [Test]
        [Ignore]
        public void Does_Set_Complete_Ad_Date()
        {
            var ttmRepo = new Mock<TimeToMarketRepository>(_adStatusRepository);
            var data = TestData.TimeToMarketTestData.GetTest_VinsToUpdate(1);

            ttmRepo.Setup(t => t.GetAdCompleteFlags(It.IsAny<int>())).Returns((AdCompleteFlags)7);

            var updatableVins = data as RowItem[] ?? data.ToArray();

            Assert.That(updatableVins.All(t => t.CompleteAdFirstDate.HasValue != true));
            ttmRepo.Object.FillCompleteAdFirstDate(updatableVins);
            
            Assert.That(updatableVins.Count(t => t.CompleteAdFirstDate.HasValue) == 1);
        }

        [Test]
        [Ignore]
        public void Set_Complete_Ad_Honors_AdCompleteFlags()
        {
            var ttmRepo = new Mock<TimeToMarketRepository>(_adStatusRepository);
            
            // 7 = Require Price, AdComplete, MinimumPhotos, Description
            ttmRepo.Setup(t => t.GetAdCompleteFlags(It.IsAny<int>())).Returns((AdCompleteFlags)7);

            var data = TestData.TimeToMarketTestData.GetTest_VinsToUpdate(1).ToArray();
            Assert.That(!data.Any(t => t.CompleteAdFirstDate.HasValue));
            ttmRepo.Object.FillCompleteAdFirstDate(data);
            Assert.AreEqual(1, data.Count(t => t.CompleteAdFirstDate.HasValue));

            data = TestData.TimeToMarketTestData.GetTest_VinsToUpdate(1).ToArray();
            ttmRepo.Setup(t => t.GetAdCompleteFlags(It.IsAny<int>())).Returns( AdCompleteFlags.Description );
            ttmRepo.Object.FillCompleteAdFirstDate(data);
            Assert.AreEqual(2, data.Count(t => t.CompleteAdFirstDate.HasValue));

            data = TestData.TimeToMarketTestData.GetTest_VinsToUpdate(1).ToArray();
            ttmRepo.Setup(t => t.GetAdCompleteFlags(It.IsAny<int>())).Returns(AdCompleteFlags.MinimumPhotos);
            ttmRepo.Object.FillCompleteAdFirstDate(data);
            Assert.AreEqual(2, data.Count(t => t.CompleteAdFirstDate.HasValue));

            data = TestData.TimeToMarketTestData.GetTest_VinsToUpdate(1).ToArray();
            ttmRepo.Setup(t => t.GetAdCompleteFlags(It.IsAny<int>())).Returns(AdCompleteFlags.Price);
            ttmRepo.Object.FillCompleteAdFirstDate(data);
            Assert.AreEqual(2, data.Count(t => t.CompleteAdFirstDate.HasValue));

            data = TestData.TimeToMarketTestData.GetTest_VinsToUpdate(1).ToArray();
            ttmRepo.Setup(t => t.GetAdCompleteFlags(It.IsAny<int>())).Returns(0);
            ttmRepo.Object.FillCompleteAdFirstDate(data);
            Assert.AreEqual(5, data.Count(t => t.CompleteAdFirstDate.HasValue));
        }






        #region Unit test methods
        private void RegisterSettingsObject()
        {
            // Register our mocks with the Registry and IoC container.
            var builder = new ContainerBuilder();
            var cacheMock = new Mock<ICache>();

            // Mock the ICache so I can use my fake settings.
            cacheMock.Setup(x => x.Get(It.IsAny<string>())).Returns(GetSettingsObj());

            builder.RegisterInstance(cacheMock.Object).As<ICache>();
            _container = builder.Build();
            Registry.RegisterContainer(_container);
        }

        private static object GetSettingsObj()
        {
            var settingsObj = new Mock<GetMiscSettings>(It.IsAny<int>());
            settingsObj.Setup(s => s.NewGidProvider).Returns(GidProvider.Aultec);
            settingsObj.Setup(s => s.UsedGidProvider).Returns(GidProvider.eBiz);

            return settingsObj.Object;
        }
        #endregion Unit test methods
    }
}
