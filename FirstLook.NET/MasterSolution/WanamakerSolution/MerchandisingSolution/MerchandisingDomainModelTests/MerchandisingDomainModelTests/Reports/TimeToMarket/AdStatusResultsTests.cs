﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.AdStatus;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel;
using MAX.Entities.Enumerations;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Reports.TimeToMarket
{
    [TestFixture]
    public class AdStatusResultsTests
    {
        [TestCase("MerchandisingDomainModelTests.Reports.TimeToMarket.TestData.jsonExampleALLSTARF01-Aultec-AutoTrader.json", 1, 1, 965)]
        [TestCase("MerchandisingDomainModelTests.Reports.TimeToMarket.TestData.jsonExampleAMERICAN02-Aultec-AutoTrader-Cars.json", 2, 2, 258)]
        [TestCase("MerchandisingDomainModelTests.Reports.TimeToMarket.TestData.jsonExampleEASTBAYB01_ebiz.json", 1, 1, 265)]
        public void CanParseFiles(string file, int providers, int sites, int adsParsed)
        {
            string value = null;
            var assembly = typeof(AdStatusResultsTests).Assembly;
            if (file != null)
            {
                using (var stream = assembly.GetManifestResourceStream(file))
                    if (stream != null)
                    {
                        using (TextReader textReader = new StreamReader(stream))
                            value = textReader.ReadToEnd();
                    }
            }

            List<AdStatusResult> res = null;
            if(!string.IsNullOrWhiteSpace(value))
                res = value.FromJson<List<AdStatusResult>>();

            Assert.IsNotNull(res);
            Assert.IsTrue(providers == res.Count());
            Assert.IsTrue(res.First().Ads.Count == adsParsed);
        }

        [TestCase("MerchandisingDomainModelTests.Reports.TimeToMarket.TestData.jsonExampleALLSTARF01-Aultec-AutoTrader.json", new[] { GidProvider.Aultec }, new[] { "AutoTrader" })]
        [TestCase("MerchandisingDomainModelTests.Reports.TimeToMarket.TestData.jsonExampleAMERICAN02-Aultec-AutoTrader-Cars.json", new[] { GidProvider.Aultec }, new[] { "AutoTrader", "Cars" })]
        [TestCase("MerchandisingDomainModelTests.Reports.TimeToMarket.TestData.jsonExampleEASTBAYB01_ebiz.json", new[] { GidProvider.eBiz }, new[] { "eBiz" })]
        public void CanParseProviders(string file, GidProvider[] providers, string[] siteNames)
        {
            // Site is an internal Enum and as such cannot be passed as an arg to a public method. So hack around that. 
            var sites = siteNames.Select(x => Enum.Parse(typeof (Site), x)).Cast<Site>().ToArray();

            string value = null;
            var assembly = typeof(AdStatusResultsTests).Assembly;
            if (file != null)
            {
                using (var stream = assembly.GetManifestResourceStream(file))
                    if (stream != null)
                    {
                        using (TextReader textReader = new StreamReader(stream))
                            value = textReader.ReadToEnd();
                    }
            }

            List<AdStatusResult> res = null;
            if (!string.IsNullOrWhiteSpace(value))
                res = value.FromJson<List<AdStatusResult>>();

            Assert.That(res != null, "res != null");

            var resProviders = res.Select(x => x.Provider).ToList();
            var resSites = res.Select(x => x.Site).ToList();

            Array.ForEach(providers, provider => Assert.That(resProviders.Contains(provider)));
            Array.ForEach(sites, site => Assert.That(resSites.Contains(site)));            
        }

        [TestCase("MerchandisingDomainModelTests.Reports.TimeToMarket.TestData.jsonExampleALLSTARF01-Aultec-AutoTrader.json")]
        public void AdStatusResults_Can_Serialize_Json(string file)
        {
            string value = null;
            var assembly = typeof(AdStatusResultsTests).Assembly;
            if (file != null)
            {
                using (var stream = assembly.GetManifestResourceStream(file))
                    if (stream != null)
                    {
                        using (TextReader textReader = new StreamReader(stream))
                            value = textReader.ReadToEnd();
                    }
            }

            List<AdStatusResult> res = null;
            if (!string.IsNullOrWhiteSpace(value))
                res = value.FromJson<List<AdStatusResult>>();

            Assert.IsNotNull(res);
            
            var backToJson = res.ToJson();

            var andBackToObj = backToJson.FromJson<List<AdStatusResult>>();
            Assert.That(res.Count == andBackToObj.Count);
            Assert.That(res[0].Provider == andBackToObj[0].Provider);
        }
    }
}
