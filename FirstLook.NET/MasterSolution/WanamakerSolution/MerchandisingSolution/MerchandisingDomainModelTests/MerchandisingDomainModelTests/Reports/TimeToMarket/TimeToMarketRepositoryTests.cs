﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.Dashboard.Concrete;
using FirstLook.Merchandising.DomainModel.Enumerations;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.AdStatus;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket;
using MAX.Entities;
using MAX.Entities.Enumerations;
using MerchandisingDomainModelTests.Reports.TimeToMarket.TestData;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Reports.TimeToMarket
{
    [TestFixture]
    public class TimeToMarketRepositoryTests
    {
        private Mock<IAdStatusRepository> _analytics;
        private GidProviders _settings;

        [TestFixtureSetUp]
        public void SetupFixture()
        {
            var analytics = new Mock<IAdStatusRepository>();
            analytics.Setup(a => a.GetAdStatusResults(It.IsAny<IAdStatusArgs>())).Returns(TimeToMarketTestData.GetAdStatusResults());
			
	        _settings = new GidProviders
	        {
		        {VehicleType.New, GidProvider.Aultec},
		        {VehicleType.Used, GidProvider.eBiz}
	        };
            _analytics = analytics;
        }

        [Test]
        public void GetGidCallsMaxAnalytics()
        {
            var repo = GetRepoWithInventory();

            // Repo must be initialized.
            repo.Initialize(1, _settings);

            // Get some data
            var invData = TimeToMarketTestData.GetTest_VinsToUpdate(1);
            
            // Call GetGidDataForVins
            repo.GetGidDataForVins(invData);

            // Make sure we called the analytics repo
            _analytics.Verify(x => x.GetAdStatusResults(It.IsAny<IAdStatusArgs>()), Times.Once());
        }

        [TestCase("VIN-1", false)]
        [TestCase("AULTEC-AutoTrader-And-Cars", true)]
        [TestCase("AULTEC-AutoTrader-Only", true)]
        [TestCase("AULTEC-Cars-Only", true)]
        [TestCase("EBIZ-Only", true)]
        [TestCase("EBIZ-And-AULTEC-Cars-Only", false)]// Used vehicle should not return data when it is only found in eBiz
        [TestCase("EBIZ-And-AULTEC-AutoTrader-Only", true)]
        public void GetGidData_For_Vehicles_ReturnsData_When_It_Should(string vin, bool expected)
        {
            var repo = GetRepoWithInventory();
            repo.Initialize(1, _settings);

            var invData = TimeToMarketTestData.GetTest_VinsToUpdate(1).Where(x => x.VIN == vin).ToList();
            repo.GetGidDataForVins(invData);

            var gidData = repo.GetGidDataForVehicle(vin);

            Assert.AreEqual(gidData.Any(), expected);
        }


        /// <summary>
        /// We want to make sure that new cars use AULTec and used cars use eBiz
        /// because that is how our test data is set up above
        /// </summary>
        [TestCase("AULTEC-AutoTrader-And-Cars", true, true)]
        [TestCase("AULTEC-AutoTrader-Only", true, true)]
        [TestCase("AULTEC-Cars-Only", true, true)]
        [TestCase("EBIZ-Only", false, true)]
        [TestCase("EBIZ-And-AULTEC-Cars-Only", false, false)]
        [TestCase("EBIZ-And-AULTEC-AutoTrader-Only", true, true)]
        public void GetGidDataForVehicles_UsesCorrectProvider(string vin, bool usesAultec, bool expectResults = false)
        {
            var repo = GetRepoWithInventory();
            repo.Initialize(1, _settings);

            var invData = TimeToMarketTestData.GetTest_VinsToUpdate(1).Where(x => x.VIN == vin).ToList();
            repo.GetGidDataForVins(invData);

            var data = repo.GetGidDataForVehicle(vin).ToArray();

            Assert.True( data.Any() == expectResults);

            if(data != null)
                Assert.That(data.All(ad => ((MaxAnalyticsAd)ad).Description.ToUpperInvariant().EndsWith("AULTEC") == usesAultec));
        }


        [Test]
        public void GidDataSetsEverything()
        {
            var repo = GetRepoWithFillData();

            repo.Initialize(1, _settings);

            const string vin = @"Everything";
            var invData = TimeToMarketTestData.FillTestUpdatableVins(1).Where(x => x.VIN == vin).ToList();
            repo.GetGidDataForVins(invData);

            repo.FillPhotosFirstDate(invData, new DateTime(1,1,1));
            repo.FillMinimumPhotosFirstDate(invData, new DateTime(1,1,1));
            repo.FillPriceFirstDate(invData);
            repo.FillDescriptionFirstDate(invData);

            var testRecord = invData.First(item => item.VIN == vin);
            var today = DateTime.Today.Date;
            Assert.NotNull(testRecord);

            Assert.That(testRecord.PhotosFirstDate.HasValue && testRecord.PhotosFirstDate.Value.Date == today.AddDays(-1));
            Assert.That(testRecord.MinimumPhotoFirstDate.HasValue && testRecord.MinimumPhotoFirstDate.Value.Date == today.AddDays(-1));
            Assert.That(testRecord.PriceFirstDate.HasValue && testRecord.PriceFirstDate.Value.Date == today.AddDays(-1));
            Assert.That(testRecord.DescriptionFirstDate.HasValue && testRecord.DescriptionFirstDate.Value.Date == today.AddDays(-1));
        }


        [Test]
        public void GidDataSetsOnlyPhoto()
        {
            var repo = GetRepoWithFillData();

            repo.Initialize(1, _settings);

            const string vin = @"OnePhoto";
            var invData = TimeToMarketTestData.FillTestUpdatableVins(1).Where(x => x.VIN == vin).ToList();
            repo.GetGidDataForVins(invData);

            repo.FillPhotosFirstDate(invData, new DateTime(1, 1, 1));
            repo.FillMinimumPhotosFirstDate(invData, new DateTime(1, 1, 1));
            repo.FillPriceFirstDate(invData);
            repo.FillDescriptionFirstDate(invData);

            var testRecord = invData.First(item => item.VIN == vin);
            var today = DateTime.Today.Date;
            Assert.NotNull(testRecord);

            Assert.That(testRecord.PhotosFirstDate.HasValue && testRecord.PhotosFirstDate.Value.Date == today.AddDays(-1));

            Assert.That(testRecord.MinimumPhotoFirstDate.HasValue == false);
            Assert.That(testRecord.PriceFirstDate.HasValue == false);
            Assert.That(testRecord.DescriptionFirstDate.HasValue == false);
        }

        /// <summary>
        /// If we have minimum photos, we must have at least one photo... so set them both
        /// </summary>
        [Test]
        public void GidDataSetsOnlyPhotosAndMinimumPhotos()
        {
            var repo = GetRepoWithFillData();

            repo.Initialize(1, _settings);

            const string vin = @"ManyPhotos";
            var invData = TimeToMarketTestData.FillTestUpdatableVins(1).Where(x => x.VIN == vin).ToList();
            repo.GetGidDataForVins(invData);

            repo.FillPhotosFirstDate(invData, new DateTime(1, 1, 1));
            repo.FillMinimumPhotosFirstDate(invData, new DateTime(1, 1, 1));
            repo.FillPriceFirstDate(invData);
            repo.FillDescriptionFirstDate(invData);

            var testRecord = invData.First(item => item.VIN == vin);
            var today = DateTime.Today.Date;
            Assert.NotNull(testRecord);

            Assert.That(testRecord.PhotosFirstDate.HasValue && testRecord.PhotosFirstDate.Value.Date == today.AddDays(-1));
            Assert.That(testRecord.MinimumPhotoFirstDate.HasValue && testRecord.MinimumPhotoFirstDate.Value.Date == today.AddDays(-1));
            
            Assert.That(testRecord.PriceFirstDate.HasValue == false);
            Assert.That(testRecord.DescriptionFirstDate.HasValue == false);
        }

        [Test]
        public void GidDataSetsOnlyPrice()
        {
            var repo = GetRepoWithFillData();

            repo.Initialize(1, _settings);

            const string vin = @"Price";
            var invData = TimeToMarketTestData.FillTestUpdatableVins(1).Where(x => x.VIN == vin).ToList();
            repo.GetGidDataForVins(invData);

            repo.FillPhotosFirstDate(invData, new DateTime(1, 1, 1));
            repo.FillMinimumPhotosFirstDate(invData, new DateTime(1, 1, 1));
            repo.FillPriceFirstDate(invData);
            repo.FillDescriptionFirstDate(invData);

            var testRecord = invData.First(item => item.VIN == vin);
            var today = DateTime.Today.Date;
            Assert.NotNull(testRecord);

            Assert.That(testRecord.PhotosFirstDate.HasValue == false);
            Assert.That(testRecord.MinimumPhotoFirstDate.HasValue == false);

            Assert.That(testRecord.PriceFirstDate.HasValue && testRecord.PriceFirstDate.Value.Date == today.AddDays(-1));
            Assert.That(testRecord.DescriptionFirstDate.HasValue == false);
        }

        [Test]
        public void GidDataDoesNotSetNegativePrice()
        {
            var repo = GetRepoWithFillData();

            repo.Initialize(1, _settings);

            const string vin = @"NegPrice";
            var invData = TimeToMarketTestData.FillTestUpdatableVins(1).Where(x => x.VIN == vin).ToList();
            repo.GetGidDataForVins(invData);

            repo.FillPhotosFirstDate(invData, new DateTime(1, 1, 1));
            repo.FillMinimumPhotosFirstDate(invData, new DateTime(1, 1, 1));
            repo.FillPriceFirstDate(invData);
            repo.FillDescriptionFirstDate(invData);

            var testRecord = invData.First(item => item.VIN == vin);
            Assert.NotNull(testRecord);

            Assert.That(testRecord.PhotosFirstDate.HasValue == false);
            Assert.That(testRecord.MinimumPhotoFirstDate.HasValue == false);

            Assert.That(testRecord.PriceFirstDate.HasValue == false);
            Assert.That(testRecord.DescriptionFirstDate.HasValue == false);
        }

        [Test]
        public void GidDataSetsOnlyDescription()
        {
            var repo = GetRepoWithFillData();

            repo.Initialize(1, _settings);

            const string vin = @"Description";
            var invData = TimeToMarketTestData.FillTestUpdatableVins(1).Where(x => x.VIN == vin).ToList();
            repo.GetGidDataForVins(invData);

            repo.FillPhotosFirstDate(invData, new DateTime(1, 1, 1));
            repo.FillMinimumPhotosFirstDate(invData, new DateTime(1, 1, 1));
            repo.FillPriceFirstDate(invData);
            repo.FillDescriptionFirstDate(invData);

            var testRecord = invData.First(item => item.VIN == vin);
            var today = DateTime.Today.Date;
            Assert.NotNull(testRecord);

            Assert.That(testRecord.PhotosFirstDate.HasValue == false);
            Assert.That(testRecord.MinimumPhotoFirstDate.HasValue == false);

            Assert.That(testRecord.PriceFirstDate.HasValue == false);
            Assert.That(testRecord.DescriptionFirstDate.HasValue && testRecord.DescriptionFirstDate.Value.Date == today.AddDays(-1));
        }

        [Test]
        public void GidDataDoesNotSetEmptyDescription()
        {
            var repo = GetRepoWithFillData();

            repo.Initialize(1, _settings);

            const string vin = @"EmptyDescription";
            var invData = TimeToMarketTestData.FillTestUpdatableVins(1).Where(x => x.VIN == vin).ToList();
            repo.GetGidDataForVins(invData);

            repo.FillPhotosFirstDate(invData, new DateTime(1, 1, 1));
            repo.FillMinimumPhotosFirstDate(invData, new DateTime(1, 1, 1));
            repo.FillPriceFirstDate(invData);
            repo.FillDescriptionFirstDate(invData);

            var testRecord = invData.First(item => item.VIN == vin);
            Assert.NotNull(testRecord);

            Assert.That(testRecord.PhotosFirstDate.HasValue == false);
            Assert.That(testRecord.MinimumPhotoFirstDate.HasValue == false);

            Assert.That(testRecord.PriceFirstDate.HasValue == false);
            Assert.That(testRecord.DescriptionFirstDate.HasValue == false);
        }

        [Test]
        public void CompleteAdFirstDateUsesMaxColumns()
        {
            var repo = GetRepoWithFillData();

            repo.Initialize(1, _settings);

            const string vin = @"EmptyDescription";
            var invData = TimeToMarketTestData.FillTestUpdatableVins(1).Where(x => x.VIN == vin).ToList();
            repo.GetGidDataForVins(invData);

            var testRecord = invData.First(item => item.VIN == vin);
            Assert.NotNull(testRecord);

            // Clear all the values.
            testRecord.PhotosFirstDate = null;
            testRecord.MinimumPhotoFirstDate = null;
            testRecord.PriceFirstDate = null;
            testRecord.DescriptionFirstDate = null;
            testRecord.MAXDescriptionFirstDate = null;
            testRecord.MAXMinimumPhotoFirstDate = null;
            testRecord.MAXPhotoFirstDate = null;
            testRecord.MAXPriceFirstDate = null;
            testRecord.AdApprovalFirstDate = null;
            testRecord.CompleteAdFirstDate = null;

            // Try setting the complete ad date
            repo.FillCompleteAdFirstDate(invData);
            
            Assert.That(testRecord.CompleteAdFirstDate.HasValue == false);
            
            var today = DateTime.Today;
            // Set the old fields with values
            testRecord.MAXDescriptionFirstDate = today;
            testRecord.MAXMinimumPhotoFirstDate = today;
            testRecord.MAXPhotoFirstDate = today;
            testRecord.MAXPriceFirstDate = today;

            // Must have an ad approval date.
            testRecord.AdApprovalFirstDate = today;

            // Try setting the complete ad date using the old columns
            repo.FillCompleteAdFirstDate(invData);

            Assert.That(testRecord.CompleteAdFirstDate.HasValue == false);

            // Set the new (GID) columns
            testRecord.PhotosFirstDate = today;
            testRecord.MinimumPhotoFirstDate = today;
            testRecord.PriceFirstDate = today;
            testRecord.DescriptionFirstDate = today;

            // Try setting the complete ad date using the new columns
            repo.FillCompleteAdFirstDate(invData);

            Assert.That(testRecord.CompleteAdFirstDate.HasValue && testRecord.CompleteAdFirstDate == today);
        }

        [TestCase(GidProvider.None, false)]
        [TestCase(GidProvider.Unknown, false)]
        [TestCase(GidProvider.Aultec, true)]
        [TestCase(GidProvider.eBiz, false)] // "Everything" data set is pointed at Aultec
        public void GidHonorsNoneProviderSetting(GidProvider provider, bool expected)
        {
            var settingsObj = new GidProviders
	        {
		        {VehicleType.New, provider},
		        {VehicleType.Used, provider}
	        };

            var repo = GetRepoWithFillData();

            repo.Initialize(1, settingsObj);

            // Everything returns GidData for all values, but since the GidProvider is set to None we should have no values.
            const string vin = @"Everything";
            var invData = TimeToMarketTestData.FillTestUpdatableVins(1).Where(x => x.VIN == vin).ToList();
            repo.GetGidDataForVins(invData);

            repo.FillPhotosFirstDate(invData, new DateTime(1, 1, 1));
            repo.FillMinimumPhotosFirstDate(invData, new DateTime(1, 1, 1));
            repo.FillPriceFirstDate(invData);
            repo.FillDescriptionFirstDate(invData);

            var testRecord = invData.First(item => item.VIN == vin);
            Assert.NotNull(testRecord);

            Assert.That(testRecord.PhotosFirstDate.HasValue == expected);
            Assert.That(testRecord.MinimumPhotoFirstDate.HasValue == expected);
            Assert.That(testRecord.PriceFirstDate.HasValue == expected);
            Assert.That(testRecord.DescriptionFirstDate.HasValue == expected);
        }

        #region Private methods

        private TimeToMarketRepository GetRepoWithInventory()
        {
            return GetRepo(TimeToMarketTestData.FakeInventoryData().ToList());
        }

        private TimeToMarketRepository GetRepoWithFillData()
        {
            var analytics = new Mock<IAdStatusRepository>();
            analytics
                .Setup(a => a.GetAdStatusResults(It.IsAny<IAdStatusArgs>()))
                .Returns(TimeToMarketTestData.FillTestAdStatusResults());

            return GetRepo(TimeToMarketTestData.FillTestInventoryData().ToList(), analytics.Object);
        }

        private TimeToMarketRepository GetRepo(List<IInventoryData> inventoryDatas, IAdStatusRepository analytics = null)
        {
            var useAnalytics = analytics ?? _analytics.Object;
			var inventoryRepository = new Mock<IInventoryDataRepository>();
			var businessUnitRepository = new Mock<IDashboardBusinessUnitRepository>();
	        businessUnitRepository.Setup(bur => bur.GetBusinessUnit(It.IsAny<int>())).Returns(new DashboardBusinessUnit {BusinessUnitId = 1, Code = "TESTBU"});

            var repoMock = new Mock<TimeToMarketRepository>(useAnalytics, inventoryRepository.Object, businessUnitRepository.Object);

            // Override the "GetInventoryData" method to return consistent results for testing.
            repoMock.Setup(r => r.GetInventoryData(It.IsAny<int>())).Returns(inventoryDatas);
            
            // The minimum photo count for tests is 2
            repoMock.Setup(r => r.GetMinimumPhotoCount(It.IsAny<int>())).Returns(2);

            // mock up the adcomplete flags
            repoMock.Setup(t => t.GetAdCompleteFlags(It.IsAny<int>())).Returns((AdCompleteFlags)7);
            
            // Except for the methods we have overridden here, call the base object methods.
            repoMock.CallBase = true;

            var repo = repoMock.Object;
            return repo;
        }

        #endregion Private methods
    }
}
