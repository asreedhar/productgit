using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using VehicleDataAccess;

namespace DomainModelTester
{
    [TestFixture()]
    public class TestOptions
    {
        [Test()]
        public void Can_Get_List_Of_Options_Using_ModelId()
        {
            EquipmentCollection ec = EquipmentCollection.FetchUnfilteredOptionsByModel(13727);

            Assert.AreNotEqual(0, ec.Count);
        }
    }
}
