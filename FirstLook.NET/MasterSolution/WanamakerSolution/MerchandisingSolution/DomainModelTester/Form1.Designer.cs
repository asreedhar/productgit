namespace DomainModelTester
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.resultsLbl = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.ConditionTestTB = new System.Windows.Forms.TextBox();
            this.condPop1 = new System.Windows.Forms.TextBox();
            this.condPop2 = new System.Windows.Forms.TextBox();
            this.styleInferBtn = new System.Windows.Forms.Button();
            this.VINTB = new System.Windows.Forms.TextBox();
            this.OptionCodesTB = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.styleResultTB = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.trimNameTB = new System.Windows.Forms.TextBox();
            this.transCombo = new System.Windows.Forms.ComboBox();
            this.drivetypeCombo = new System.Windows.Forms.ComboBox();
            this.useTrimCB = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(51, 32);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(156, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Test Marketing Snippets";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Test Results:";
            // 
            // resultsLbl
            // 
            this.resultsLbl.AutoSize = true;
            this.resultsLbl.Location = new System.Drawing.Point(33, 85);
            this.resultsLbl.Name = "resultsLbl";
            this.resultsLbl.Size = new System.Drawing.Size(51, 13);
            this.resultsLbl.TabIndex = 2;
            this.resultsLbl.Text = "run test...";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 346);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(156, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Test Condition Descriptor";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ConditionTestTB
            // 
            this.ConditionTestTB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ConditionTestTB.Location = new System.Drawing.Point(12, 375);
            this.ConditionTestTB.Multiline = true;
            this.ConditionTestTB.Name = "ConditionTestTB";
            this.ConditionTestTB.Size = new System.Drawing.Size(571, 32);
            this.ConditionTestTB.TabIndex = 4;
            // 
            // condPop1
            // 
            this.condPop1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.condPop1.Location = new System.Drawing.Point(12, 413);
            this.condPop1.Multiline = true;
            this.condPop1.Name = "condPop1";
            this.condPop1.Size = new System.Drawing.Size(571, 32);
            this.condPop1.TabIndex = 5;
            // 
            // condPop2
            // 
            this.condPop2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.condPop2.Location = new System.Drawing.Point(12, 452);
            this.condPop2.Multiline = true;
            this.condPop2.Name = "condPop2";
            this.condPop2.Size = new System.Drawing.Size(571, 32);
            this.condPop2.TabIndex = 6;
            // 
            // styleInferBtn
            // 
            this.styleInferBtn.Location = new System.Drawing.Point(357, 195);
            this.styleInferBtn.Name = "styleInferBtn";
            this.styleInferBtn.Size = new System.Drawing.Size(75, 23);
            this.styleInferBtn.TabIndex = 7;
            this.styleInferBtn.Text = "Test Style Inferer";
            this.styleInferBtn.UseVisualStyleBackColor = true;
            this.styleInferBtn.Click += new System.EventHandler(this.styleInferBtn_Click);
            // 
            // VINTB
            // 
            this.VINTB.Location = new System.Drawing.Point(137, 195);
            this.VINTB.Name = "VINTB";
            this.VINTB.Size = new System.Drawing.Size(199, 20);
            this.VINTB.TabIndex = 8;
            // 
            // OptionCodesTB
            // 
            this.OptionCodesTB.Location = new System.Drawing.Point(137, 221);
            this.OptionCodesTB.Name = "OptionCodesTB";
            this.OptionCodesTB.Size = new System.Drawing.Size(280, 20);
            this.OptionCodesTB.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(106, 200);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "VIN";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 224);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Option Codes (ABC, DEF)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(94, 276);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Result";
            // 
            // styleResultTB
            // 
            this.styleResultTB.Location = new System.Drawing.Point(151, 273);
            this.styleResultTB.Multiline = true;
            this.styleResultTB.Name = "styleResultTB";
            this.styleResultTB.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.styleResultTB.Size = new System.Drawing.Size(432, 67);
            this.styleResultTB.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(73, 250);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Trim Name";
            // 
            // trimNameTB
            // 
            this.trimNameTB.Location = new System.Drawing.Point(137, 247);
            this.trimNameTB.Name = "trimNameTB";
            this.trimNameTB.Size = new System.Drawing.Size(144, 20);
            this.trimNameTB.TabIndex = 15;
            // 
            // transCombo
            // 
            this.transCombo.FormattingEnabled = true;
            this.transCombo.Location = new System.Drawing.Point(462, 220);
            this.transCombo.Name = "transCombo";
            this.transCombo.Size = new System.Drawing.Size(121, 21);
            this.transCombo.TabIndex = 16;
            // 
            // drivetypeCombo
            // 
            this.drivetypeCombo.FormattingEnabled = true;
            this.drivetypeCombo.Location = new System.Drawing.Point(462, 248);
            this.drivetypeCombo.Name = "drivetypeCombo";
            this.drivetypeCombo.Size = new System.Drawing.Size(121, 21);
            this.drivetypeCombo.TabIndex = 17;
            // 
            // useTrimCB
            // 
            this.useTrimCB.AutoSize = true;
            this.useTrimCB.Checked = true;
            this.useTrimCB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.useTrimCB.Location = new System.Drawing.Point(287, 248);
            this.useTrimCB.Name = "useTrimCB";
            this.useTrimCB.Size = new System.Drawing.Size(108, 17);
            this.useTrimCB.TabIndex = 18;
            this.useTrimCB.Text = "use trim in search";
            this.useTrimCB.UseVisualStyleBackColor = true;
            this.useTrimCB.CheckedChanged += new System.EventHandler(this.useTrimCB_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(595, 528);
            this.Controls.Add(this.useTrimCB);
            this.Controls.Add(this.drivetypeCombo);
            this.Controls.Add(this.transCombo);
            this.Controls.Add(this.trimNameTB);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.styleResultTB);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.OptionCodesTB);
            this.Controls.Add(this.VINTB);
            this.Controls.Add(this.styleInferBtn);
            this.Controls.Add(this.condPop2);
            this.Controls.Add(this.condPop1);
            this.Controls.Add(this.ConditionTestTB);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.resultsLbl);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label resultsLbl;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox ConditionTestTB;
        private System.Windows.Forms.TextBox condPop1;
        private System.Windows.Forms.TextBox condPop2;
        private System.Windows.Forms.Button styleInferBtn;
        private System.Windows.Forms.TextBox VINTB;
        private System.Windows.Forms.TextBox OptionCodesTB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox styleResultTB;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox trimNameTB;
        private System.Windows.Forms.ComboBox transCombo;
        private System.Windows.Forms.ComboBox drivetypeCombo;
        private System.Windows.Forms.CheckBox useTrimCB;
    }
}

