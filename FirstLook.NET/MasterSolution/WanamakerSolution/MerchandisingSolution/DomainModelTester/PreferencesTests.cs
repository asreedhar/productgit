using System;
using System.Collections.Generic;
using System.Text;
using FirstLook.Merchandising.DomainModel.Templating;
using NUnit.Framework;

namespace DomainModelTester
{
    [TestFixture()]
    public class PreferencesTests
    {
        [Test()]
        public void Can_Fetch_Dealer_Preferences_Successfully()
        {
            DealerAdvertisementPreferences prefs = DealerAdvertisementPreferences.Fetch(102761); 

            Assert.AreEqual(8, prefs.GasMileageBySegment.Count);
        }

        [Test()]
        public void Can_Update_Dealer_Preferences_With_PreviewCallToAction_And_MinPriceReduce()
        {
            DealerAdvertisementPreferences prefs = DealerAdvertisementPreferences.Fetch(102761);
            prefs.MinGoodPriceReduction = 307;
            prefs.UseCallToActionInPreview = !prefs.UseCallToActionInPreview;

            DealerAdvertisementPreferences.UpdateOrCreate(prefs);

            DealerAdvertisementPreferences prefs2 = DealerAdvertisementPreferences.Fetch(102761);

            Assert.AreEqual(prefs.MinGoodPriceReduction, prefs2.MinGoodPriceReduction);

            Assert.AreEqual(prefs.UseCallToActionInPreview, prefs2.UseCallToActionInPreview);
        }
    }
}
