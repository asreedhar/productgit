using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using FirstLook.Merchandising.KapowOnDemand;
using FirstLook.Merchandising.Release.DomainModel;
using NUnit;
using NUnit.Framework;
namespace DomainModelTester
{
    [TestFixture()]
    public class KapowTests
    {

        public void Try_A_HUGE_URL_POST()
        {
            
            //Advertisement ad = new Advertisement();
            //crm.Release(new Advertisement());
        }

        [Test()]
        public void Try_A_REALLY_HUGE_URL_POST()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("username", "ADMIN");
            //parameters.Add("password", "");  //NOTE ADD PASSWORD
            parameters.Add("ListingSiteDealerId", "10199");
            parameters.Add("VIN","19UUA66277A001209");
            parameters.Add("Description", "Why not buy an Acura Certified, CARFAX 1-Owner, Clean, ONLY 40,509 Miles! GREAT DEAL $2,800 below Kelley Blue Book. EPA 28 MPG Hwy/20 MPG City! Leather Seats, NAV, Sunroof, Seat Heaters, Alloy Wheels, Overhead Airbag... Navigation CLICK IN TO SEE MORE! === Acura's Certified program comes with Additional 1-year/12,000-mile Non-Powertrain Warranty, Balance of original 7-year/100,000-Mile Powertrain Warranty, 150-point Inspection and Reconditioning, 24/7 Roadside Assistance ====== **It won't last long at $2,800 below Kelley Blue Book** ====== DRIVE WITH CONFIDENCE: CARFAX 1-Owner and a clean CARFAX Vehicle History Report; Qualifies for CARFAX Buyback Guarantee Acura Certified. Clean - ====== Comes LOADED with Heated Leather Seats, Navigation, Power Sunroof, Multi-CD Changer, and MP3 Player. Non-Smoker vehicle. ====== Navigation with Alabaster Silver Metallic exterior and Ebony Leather Seats features Auto Transmission w/Manual Mode and V6 Cylinder Engine with 258 HP at 6200 RPM*. ====== EXPERTS RAVE: Get Great Gas Mileage: 28 MPG Hwy ~~ 5 Star Driver Front Crash Rating. 4 Star Driver Side Crash Rating. \"If getting lots of bang for your buck is important, it's hard to beat the Acura TL, as few entry-level luxury cars pack its level of standard equipment. Well-built, reliable and exceedingly easy to live with as an everyday companion, the TL is a superb choice.\" -Edmunds.com ====== Call or Email Our Internet Department To Schedule Your Test Drive Today. ==================== Pricing analysis performed on 1/16/2010. Horsepower calculations based on trim engine configuration. Fuel economy calculations based on original manufacturer data for trim engine configuration. Please confirm the accuracy of the included equipment by calling us prior to purchase. ");
            RQLObjects ro = RobotHelper.ExecuteRobot("incisentsvc.kapow.net/cdmpostingrobot.xml", parameters);

            Assert.AreEqual(ro.Count, 1);

            RQLObject obj = ro.GetObject(0);
            string code = obj.Get("postStatus");
            Assert.AreEqual("1", code);
            
        }

        [Test()]
        public void Try_A_REALLY_HUGE_URL_POST_DealerSpecialties()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("username", "mmarkarian");
            parameters.Add("password", "");  //NOTE ADD PASSWORD

            parameters.Add("VIN", "JTMBK33V585062455");
            parameters.Add("Description", "Wait no more, grab this one before it is gone. Consumer Guide Recommended SUV. **PRICED TO MOVE - $1,600 below Kelley Blue Book!** Toyota Certified. 4X4, MP3 Player, iPod/MP3 Input, CD Player, Steel Wheels, Side Head Air Bag, and Rear Head Air Bag ~~ Approx. Original Base Sticker Price: $24,900* EXCELLENT SAFETY RATINGS: 5 Star Driver Front Crash Rating. 5 Star Driver Side Crash Rating. ALSO COMES WITH Rear Spoiler, Privacy Glass, Child Safety Locks, Electronic Stability Control, and Bucket Seats. Certification comes with 7-Years/100,000-Mile Powertrain Warranty from original in-service date, 3-Month/3,000-Mile Comprehensive Warranty from date of purchase, 160-Point Inspection and Reconditioning, 24-Hour Roadside Assistance, Buy Toyota Certified, because the best new cars make the best used cars.. We are committed to providing you the best possible service. Longo Toyota is the #1 Volume Toyota Certified Dealer in the country. ");
            RQLObjects ro = RobotHelper.ExecuteRobot("incisentsvc.kapow.net/dealerspecialtiesposting.xml", parameters);

            Assert.AreEqual(ro.Count, 1);

            RQLObject obj = ro.GetObject(0);
            string code = obj.Get("postStatus");
            Assert.AreEqual("1", code);

        }
        
    }
}
