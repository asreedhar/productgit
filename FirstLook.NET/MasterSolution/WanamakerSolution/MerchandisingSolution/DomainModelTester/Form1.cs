using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using FirstLook.Merchandising.DomainModel.MarketingData;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Vehicles;
using VehicleDataAccess;

namespace DomainModelTester
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            string[] transmissionTypes = Enum.GetNames(typeof (TransmissionType));
            string[] driveTypes = Enum.GetNames(typeof (DrivetrainType));

            transCombo.DataSource = transmissionTypes;
            drivetypeCombo.DataSource = driveTypes;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MarketingDataCollection mdc = MarketingDataCollection.Fetch(267538);
            mdc = mdc.FindByTag(MarketingTag.CompetitorComparison);
            if (mdc.Count > 0)
            {
                resultsLbl.Text = mdc[0].ToString();
            }
            else
            {
                resultsLbl.Text = mdc.Count.ToString();
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ConditionChecklist cc = new ConditionChecklist(true, true, false, false, false, true, true, false, true, true, false, false, false);
            VehicleConditionCollection vcc = new VehicleConditionCollection();
            vcc.Add(new VehicleCondition((int)VehicleConditionType.Exterior, VehicleCondition.ConditionLevelExcellent, "Excellent"));
            vcc.Add(new VehicleCondition((int)VehicleConditionType.Interior, VehicleCondition.ConditionLevelExcellent, "Good"));

            ConditionDescriber cd = new ConditionDescriber(cc, vcc, 80);
            ConditionTestTB.Text = cd.GetDescription(140);

            condPop1.Text = cd.PopDescription(5);
            condPop2.Text = cd.PopDescription(1);
        }

        private void styleInferBtn_Click(object sender, EventArgs e)
        {
            string vin = VINTB.Text;
            string opcodes = OptionCodesTB.Text;

            List<string> options = new List<string>();
            foreach (string option in opcodes.Split(",".ToCharArray()))
            {
                options.Add(option.Trim());
            }

            string trimName = trimNameTB.Text;
            if (!useTrimCB.Checked)
            {
                trimName = null;
            }
            List<ChromeStyle> styles = ChromeStyle.InferStyleId(vin, options, "", trimName,
                                                    (TransmissionType)
                                                    Enum.Parse(typeof (TransmissionType),
                                                               (string) transCombo.SelectedItem),
                                                    (DrivetrainType)
                                                    Enum.Parse(typeof (DrivetrainType),
                                                               (string) drivetypeCombo.SelectedItem));
                

            if (styles.Count == 1)
            {
                styleResultTB.Text = string.Format("{0} - style {1}, trim {2}",styles[0].StyleID, styles[0].StyleNameWOTrim, styles[0].Trim);
            }else
            {
                StringBuilder sb = new StringBuilder();
                foreach (ChromeStyle style in styles)
                {
                    sb.Append(style.StyleID).Append(": ").Append(style.Trim).Append(" - ").Append(style.StyleNameWOTrim).Append("\n\r");
                }
                styleResultTB.Text = sb.Insert(0, "No style match found: ").ToString();
            }


        }

        //private void TryTo

        private void useTrimCB_CheckedChanged(object sender, EventArgs e)
        {
            trimNameTB.Enabled = useTrimCB.Checked;
        }
    }
}