using System;
using System.Configuration;
using log4net;

namespace Wanamaker.WebApp
{

    public partial class ErrorPage : BasePage
    {
        #region Logging

        private static readonly ILog Log = LogManager.GetLogger(typeof(ErrorPage).FullName);

        #endregion

        public override string BulkUploadUrl
        {
            get { return ""; }
        }

        public override FirstLook.DomainModel.Oltp.BusinessUnit BusinessUnit
        {
            get { return null; }
        }

        public override int BusinessUnitId
        {
            get { return 100150; }
        }

        public override int InventoryId
        {
            get { return 0; }
        }

        public override bool IsMax30
        {
            get { return true; }
        }

        public override FirstLook.Common.Core.PhotoServices.IPhotoServices PhotoService
        {
            get { return null; }
            set { }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string err = Request["Error"];

            Log.ErrorFormat("Page_Load() has been called from sender {0} with EventArgs {1}. Error '{2}' was passed via the request.",
                sender, e, err);

            if (!string.IsNullOrEmpty(err))
            {
                var navigateUrl = ConfigurationManager.AppSettings["estock_host_name"] + "/support/Marketing_Pages/ContactUs.aspx";
                switch (err)
                {
                    case "404":
                        ErrorView.ActiveViewIndex = 1;
                        Header.Title = "Not Found";
                        break;
                    case "403":
                        ErrorView.ActiveViewIndex = 2;
                        errLnk2.NavigateUrl = navigateUrl;
                        Header.Title = "Access Denied";
                        break;
                    case "401":
                        ErrorView.ActiveViewIndex = 3;
                        errLnk3.NavigateUrl = navigateUrl;
                        Header.Title = "Access Denied";
                        break;
                        
                    default:
                        ErrorView.ActiveViewIndex = 0;
                        errLnk1.NavigateUrl = navigateUrl;
                        break;
                }
            }
        }
    }
}