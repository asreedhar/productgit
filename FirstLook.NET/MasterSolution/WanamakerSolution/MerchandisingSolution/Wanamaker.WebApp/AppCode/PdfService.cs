using System.Configuration;
using System.IO;


namespace Wanamaker.WebApp.AppCode
{

    public class PdfService
    {
        public PdfService()
        {
        }
        public byte[] GeneratePdf(string pagesHTML, string[] styleSheetPaths)
        {
            PrinceXmlWebServiceProxy service = new PrinceXmlWebServiceProxy();

            service.Url = ConfigurationManager.AppSettings["prince_xml_webservice_url"];
            Input input = new Input();

            input.Content = pagesHTML;
            input.ContentType = ContentType.Html;
            if (styleSheetPaths != null && styleSheetPaths.Length > 0)
            {
                input.StyleSheetPaths = styleSheetPaths;
            }
            OutputType outputType = new OutputType();
            outputType.OutputTypeCode = OutputTypeCode.ByteStream;
            outputType.Item = new SharedFileSystem();
            Output output = new Output();
            Status status = service.generate(input, outputType, out output);
            return (byte[])output.Item;
        }
        public byte[] GeneratePdf(string pagesHTML)
        {
            return GeneratePdf(pagesHTML, new string[0]);
        }

        public static string GetFileContents(string fileName)
        {
            StreamReader sr = null;
            string FileContents = "";
            try
            {
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new StreamReader(fs);
                FileContents = sr.ReadToEnd();
            }
            finally
            {
                if (sr != null) sr.Close();
            }
            return FileContents;
        }

    }

}