using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;

namespace Wanamaker.WebApp.AppCode
{
    public class UploadHelper
    {
        /// <summary>
        /// Helper to upload files to the gallery
        /// </summary>
        /// <param name="postedFile"></param>
        /// <param name="savePath"></param>
        /// <returns>string url of the image that was uploaded</returns>
        public static string ImageUploadProcessor(HttpPostedFile postedFile, string savePath)
        {
            string filepath = postedFile.FileName;

            int lastSlash = filepath.LastIndexOf('\\');

            string file = filepath;
            if (lastSlash > 0)
            {
                file = filepath.Substring(lastSlash + 1);
            }
            
            //save the file to the server
            
            postedFile.SaveAs(savePath + file);
            return file;
        }
    }
}