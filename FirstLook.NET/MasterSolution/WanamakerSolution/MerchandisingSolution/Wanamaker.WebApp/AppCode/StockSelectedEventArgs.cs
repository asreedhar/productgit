using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using VehicleDataAccess;

namespace Wanamaker.WebApp.AppCode
{

    /// <summary>
    /// Summary description for StyleSelectorEventArgs
    /// </summary>
    public class StockSelectedEventArgs : EventArgs
    {
        private int inventoryId;

        public int InventoryId
        {
            get { return inventoryId; }
            set { inventoryId = value; }
        }

        private bool cancel = false;
        public bool Cancel
        {
            get { return cancel; }
            set { cancel = value; }
        }
        public StockSelectedEventArgs(int inventoryId)
        {
       
            this.inventoryId = inventoryId;
            
        }
    }

}