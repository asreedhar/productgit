﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using FirstLook.Common.Security.CentralAuthenticationService.Client.Configuration;

namespace Wanamaker.WebApp.AppCode
{
    public class WebSettings
    {
        /// <summary>
        /// We are basing this solely on the url 
        /// </summary>
        public static bool IsMaxStandAlone
        {
            get
            {
                var standaloneurl = ConfigurationManager.AppSettings["max_standalone_url"];
                if (standaloneurl == null) return false;
                return HttpContext.Current.Request.Url.ToString().ToLower().IndexOf(standaloneurl.ToLower()) > 0;

            }
        }

        public static string CurrentBaseUrl
        {
         get {
             return HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.AbsolutePath, string.Empty);
            }
        }

        public static string CASLogoutUrl
        {
            get
            {
                CentralAuthenticationServiceConfigurationSection configuration =
                    (CentralAuthenticationServiceConfigurationSection)
                    ConfigurationManager.GetSection("centralAuthenticationService");
                return IsMaxStandAlone ? configuration.LogoutUrl.ToLower().Replace("/cas/", "/cas-maxad/") : configuration.LogoutUrl.ToLower().Replace("/cas-maxad/", "/cas/");
            }
        }

        public static string InventoryIdContextKey
        {
            get { return "InventoryIdContextKey"; }
        }
        public static string IncludeCurrentVehicleContextKey
        {
            get { return "IncludeCurrentVehicleContextKey"; }
        }

        public static string VinContextKey
        {
            get { return "VinContextKey"; }
        }
        public static string InventoryInfoContextKey
        {
            get { return "InventoryInfoContextKey"; }
        }
        public static string BusinessUnitIdContextKey
        {
            get { return "BusinessUnitIdContextKey"; }
        }
        public static string FacetsContextKey
        {
            get { return "FacetsContextKey"; }
        }
        public static string ProfitMaxSearchOptionsKey
        {
            get { return "ProfitMaxSearchOptionsKey"; }
        }

        public static string ProfitMaxSearchCriteriaKey
        {
            get { return "ProfitMaxSearchCriteriaKey"; }
        }

        public static string RecordCountWithZeroPricing
        {
            get { return "RecordCountWithZeroPricing"; }
        }

        public static string MarketListingContextKey
        {
            get { return "MarketListingContextKey"; }
        }
    }
}