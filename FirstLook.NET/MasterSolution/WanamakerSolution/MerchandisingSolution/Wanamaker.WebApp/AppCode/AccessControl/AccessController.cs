using System;
using System.Security.AccessControl;
using System.Web;
using System.Web.Security;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel;
using MvcMiniProfiler;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.Dashboard.Concrete;

namespace Wanamaker.WebApp.AppCode.AccessControl
{
    public class AccessController
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<AccessControlHttpModule>();
        private static readonly AccessController controller = new AccessController();
        
        public static AccessController Instance()
        {
            return controller;
        }

        public void VerifyPermissions(bool isDealer, string identityName, string path, out bool confirmLicense)
        {
            confirmLicense = false;

            //MembershipUser user = IdentityHelper.GetUserByMappingLogin(identityName);
            MembershipUser user = Membership.GetUser(identityName);
            if(user == null)
                throw new UnauthorizedAccessException(
                    string.Format("The user '{0}' is not configured in Max Ad.", identityName));


            bool allowAccess = false;

            if ("Admin/Admin.aspx".Equals(path) || "Admin/DealerSelector.aspx".Equals(path) || "Admin/AdminTemplateManager.aspx".Equals(path) || "Admin/BenchmarkEntry.aspx".Equals(path))
            {
                if (!Roles.IsUserInRole(user.UserName,"Administrator"))
                {
                    throw new PrivilegeNotHeldException("Administrator");
                }
            }
            else if ("ErrorPage.aspx".Equals(path) || "LogOff.aspx".Equals(path))

            {
                // you do not need to authenticate to get an error page
            }else if ("QuickReports/Approval.aspx".Equals(path) || "Workflow/ApprovalSummary.aspx".Equals(path))
            {
                if (!(Roles.IsUserInRole(user.UserName, "Administrator") || Roles.IsUserInRole(user.UserName, "Approver")))
                {
                    throw new PrivilegeNotHeldException("Approver");
                }
            }else if ("CustomizeMAX/TemplateManager.aspx".Equals(path))
            {
                if (!(Roles.IsUserInRole(user.UserName, "Administrator") || Roles.IsUserInRole(user.UserName, "Templater")))
                {
                    throw new PrivilegeNotHeldException("Templater");
                }
            }else if ("Certified.aspx".Equals(path))
            {
                if (!(Roles.IsUserInRole(user.UserName, "Certified Preowned Writer") || Roles.IsUserInRole(user.UserName, "Administrator")))
                {
                    throw new PrivilegeNotHeldException("Certified Preowned Writer");
                }
            }
        
            allowAccess = AllowAccess(identityName, allowAccess, out confirmLicense);

            if (!allowAccess)
            {
                throw new PrivilegeNotHeldException("WIP");
            }

            if (Roles.IsUserInRole(identityName, "DemoUser"))
            {
                // perhaps do some page access controlling or jimmy something up for /
                // Member because it resides in OLTP and we don't want that.
            }
            else
            {
                Member member = MemberFacade.FindByUserName(identityName);
                HttpContext.Current.Items.Add(MemberFacade.HttpContextKey, member);
            }

            HttpContext.Current.Items.Add(IdentityHelper.MembershipContextKey, user);
            if (Roles.IsUserInRole(user.UserName, ProfilerSetting.Permission)) //start the profiler if in role
                MiniProfiler.Start();
            
        }

        private static bool AllowAccess(string identityName, bool allowAccess, out bool confirmLicense)
        {
            confirmLicense = false;
            BusinessUnit dealer = null;

            // HERE Check for demo user before proceeding! demo dashboard users will not use this facade.
            if (Roles.IsUserInRole(identityName, "DemoUser"))
            {
                var profile = new ProfileCommon();
                DemoBusinessUnitRepository dbuRepo = new DemoBusinessUnitRepository();
                var dbu = dbuRepo.Fetch(profile.DemoBusinessUnitId);
                if (dbu != null)
                {
                    // Fake up an OLTP business unit for the app to use.
                    dealer = new BusinessUnit()
                    {
                        Id = dbu.BusinessUnitId,
                        BusinessUnitCode = dbu.Code,
                        BusinessUnitType = (BusinessUnitType)dbu.Type,
                        Name = dbu.Name
                    };
                    HttpContext.Current.Items.Add(DashboardBusinessUnit.HttpContextKey, dbu);
                }
            }
            else
            {
                SoftwareSystemComponentState state = SoftwareSystemComponentStateFacade.FindOrCreate(
                    identityName,
                    SoftwareSystemComponentStateFacade.DealerComponentToken);

                if (state != null)
                {
                    dealer = state.Dealer.GetValue();
                }
            }

            if (dealer != null)
            {
                //allowAccess = dealer.HasDealerUpgrade(Upgrade.PingTwo);
                //confirmLicense = dealer.HasDealerUpgrade(Upgrade.JDPowerUsedCarMarketData);

                HttpContext.Current.Items.Add(BusinessUnit.HttpContextKey, dealer);
                //for now, allow and confirm
                allowAccess = true;
                confirmLicense = true;
            }
                        
            return allowAccess;
        }
    }
}