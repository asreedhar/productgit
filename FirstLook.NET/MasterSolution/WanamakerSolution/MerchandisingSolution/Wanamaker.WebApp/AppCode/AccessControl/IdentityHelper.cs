using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;


namespace Wanamaker.WebApp.AppCode.AccessControl
{
    /// <summary>
    /// Summary description for IdentityHelper
    /// </summary>
    public static class IdentityHelper
    {
        public static string MembershipContextKey = "MerchandiserUser";
        
        public static MembershipUser GetUserByMappingLogin(string casLogin)
        {
			// Dave Speer: I had to refactor this table to no longer include a firstlookMemberLogin to enforce database RI,
			// in all cases the firstlookMemberLogin was equal to the merchandisingMemberLogin anyway but this lookup
			// is still required to test for existance of membership.  This query used to use firstlookMemberLogin as the where clause.
			string sql = @"SELECT merchandisingMemberLogin FROM settings.memberMapping where merchandisingMemberLogin = @Login";

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@Login", casLogin);
                    string login = Convert.ToString(cmd.ExecuteScalar());

                    MembershipUser user = Membership.GetUser(login);

                    if (user != null)
                    {
                        return user;
                    }
                    else
                    {
                        throw new MemberAccessException("Member could not be found in mapping");
                    }
                }
            }
        }

        public static int GetFirstlookId()
        {
			string sql = @"SELECT firstlookMemberId FROM settings.memberMapping where merchandisingMemberLogin = @FirstlookMemberLogin";

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@FirstlookMemberLogin", HttpContext.Current.User.Identity.Name);

                    try
                    {
                        return Convert.ToInt32(cmd.ExecuteScalar());
                    }
                    catch
                    {
                        throw new MemberAccessException("FirstLook ID could not be found in mapping");
                    }

                }
            }
            
        }
    }
}