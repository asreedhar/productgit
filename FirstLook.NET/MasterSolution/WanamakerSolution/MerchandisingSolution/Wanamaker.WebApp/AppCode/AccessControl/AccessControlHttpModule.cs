using System;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using FirstLook.Common.Core.Logging;
using FirstLook.DomainModel.Oltp;

namespace Wanamaker.WebApp.AppCode.AccessControl
{
    /// <summary>
    /// Summary description for AccessControlHttpModule
    /// </summary>
    public class AccessControlHttpModule : IHttpModule
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<AccessControlHttpModule>();

        public void Dispose()
        {
            // no state to destroy
        }

        public void Init(HttpApplication application)
        {
            application.PostAcquireRequestState += OnPostAcquireRequestState;
        }

        public void OnPostAcquireRequestState(object sender, EventArgs args)
        {
            HttpRequest request = HttpContext.Current.Request;

            int i = request.Path.IndexOf('/', 1) + 1;

            string path = request.Path.Substring(i, request.Path.Length - i);

            if (!(path.Contains(".aspx") || path.Contains(".asmx") || path.ToLower().Contains("pingone")))
            {
                return;
            }
            // the identity should have a name

            if (!string.IsNullOrEmpty(Thread.CurrentPrincipal.Identity.Name))
            {
                string identityName = HttpContext.Current.User.Identity.Name;

                bool isDealer = false;

                if (string.IsNullOrEmpty(path) || "Default.aspx".Equals(path, StringComparison.CurrentCultureIgnoreCase))
                {
                    string token = request.QueryString["token"];

                    isDealer = !string.IsNullOrEmpty(token) &&
                               token.Equals(SoftwareSystemComponentStateFacade.DealerComponentToken);
                }
                
                bool confirmLicense;

                try
                {
                    AccessController.Instance().VerifyPermissions(isDealer, identityName, path, out confirmLicense);
                }
                catch (UnauthorizedAccessException ex)
                {
                    Log.Warn("User not authorized to access Max Ad.", ex);

                    if(!RedirectTo403())
                        throw new HttpException(403, "Not authorized to access Max Ad.", ex);
                }
            }
        }

        private bool RedirectTo403()
        {
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);

            if (HttpContext.Current.Request.QueryString["Error"] != null)
                return false;
            
            HttpContext.Current.Response.Redirect("~/ErrorPage.aspx?Error=403", false);

            HttpContext.Current.ApplicationInstance.CompleteRequest();

            return true;
        }
    }
}