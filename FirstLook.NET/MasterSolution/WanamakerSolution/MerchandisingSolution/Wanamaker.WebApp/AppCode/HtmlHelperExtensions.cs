﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace Wanamaker.WebApp.AppCode
{
    
    public static class HtmlHelperExtensions
    {
        public static HtmlString RenderControl<T>(this HtmlHelper helper, string path)
           where T : UserControl
        {

            return RenderControl<T>(helper, path, null);

        }
        /// Helps us to reuse the webform user controls in the new MVC views.
        /// usage example @(Html.RenderControl<Wanamaker.WebApp.Workflow.Controls.WorkflowHeader>("~/Workflow/Controls/WorkflowHeader.ascx", x => x.SearchAction = Wanamaker.WebApp.Workflow.Controls.WorkflowHeader.SearchType.CurrentPage)
        public static HtmlString RenderControl<T>(this HtmlHelper helper, string path, Action<T> action)
            where T : UserControl
        {
            Page page = new Page();
            T control = (T)page.LoadControl(path);
            page.Controls.Add(control);
            if (action != null)
                action(control);

            using (StringWriter sw = new StringWriter())
            {

                HttpContext.Current.Server.Execute(page, sw, false);
                return new HtmlString(sw.ToString());
            }

        }
    }
}