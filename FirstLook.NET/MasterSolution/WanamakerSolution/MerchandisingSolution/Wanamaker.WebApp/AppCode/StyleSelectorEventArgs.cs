using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


namespace Wanamaker.WebApp.AppCode
{


    /// <summary>
    /// Summary description for StyleSelectorEventArgs
    /// </summary>
    public class StyleSelectorEventArgs : EventArgs
    {
        private int selectedStyle;
        public int SelectedStyle
        {
            get { return selectedStyle; }
            set { selectedStyle = value; }
        }

        private bool cancel = false;
        public bool Cancel
        {
            get { return cancel; }
            set { cancel = value; }
        }
        public StyleSelectorEventArgs(int selectedStyle)
        {
            this.selectedStyle = selectedStyle;
        }
    }
}