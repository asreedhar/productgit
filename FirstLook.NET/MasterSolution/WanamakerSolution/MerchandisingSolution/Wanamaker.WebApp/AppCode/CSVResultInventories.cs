﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wanamaker.WebApp.Areas.PricingSummary.Models;
using System.Text;

namespace Wanamaker.WebApp.AppCode
{
    public class CSVResultInventories : ActionResult
    {

        public CSVResultInventories(ExportData list, string fileName)
        {
            Inventories = list;
            FileName = fileName;
        }

        public string FileName { get; protected set; }
        public ExportData Inventories { get; protected set; }


        public override void ExecuteResult(ControllerContext context)
        {
            var csv = new StringBuilder(10 * (Inventories.Columns.Count == 0 ? 1 : Inventories.Columns.Count) * typeof(ExportDataColumns).GetProperties().Count());

            for (int c = 0; c < typeof(ExportDataColumns).GetProperties().Count(); c++)
            {
                if (c > 0)
                    csv.Append(",");
       
                string columnTitleCleaned = CleanCsvString(((System.Runtime.Serialization.DataMemberAttribute)typeof(ExportDataColumns).GetProperties().ElementAt(c).GetCustomAttributes(typeof(System.Runtime.Serialization.DataMemberAttribute), true)[0]).Name);
        
                
                csv.Append(columnTitleCleaned);
            }
            csv.Append(Environment.NewLine);
            foreach (var dr in Inventories.Columns)
            {
                var csvRow = new StringBuilder();
                for (int c = 0; c < Inventories.Columns[0].GetType().GetProperties().Count(); c++)
                {
                    if (c != 0)
                        csvRow.Append(",");

                    object columnValue = dr.GetType().GetProperties().ElementAt(c).GetValue(dr,null);
                    if (columnValue == null)
                        csvRow.Append("");
                    else
                    {
                        string columnStringValue = columnValue.ToString();

                        if ((columnStringValue.Contains("below")) || (columnStringValue.Contains("above")))
                        {
                            //do nothing
                        }
                        else 
                        {
                            columnStringValue = CleanCsvString(columnStringValue);
                        }
                       
                        csvRow.Append(columnStringValue);
                    }
                }
                csv.AppendLine(csvRow.ToString());
            }

            HttpResponseBase response = context.HttpContext.Response;
            response.ContentType = "text/csv";
            response.AppendHeader("Content-Disposition", "attachment;filename=" + this.FileName);
            response.Write(csv.ToString());
        }

         protected string CleanCsvString(string input)
        {
            string output = "\"" + input.Replace("\"", "\"\"").Replace("\r\n", " ").Replace("\r", " ").Replace("\n", "") + "\"";
            return output;
        }
    }
}