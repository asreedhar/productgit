using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Oltp;

namespace Wanamaker.WebApp.AppCode
{
    /// <summary>
    /// Summary description for DealerSelection
    /// </summary>
    public class DealerSelection
    {

        private static readonly string DealerGroupToken = SoftwareSystemComponentStateFacade.DealerGroupComponentToken;

        private static readonly string DealerToken = SoftwareSystemComponentStateFacade.DealerComponentToken;

        public DealerSelection()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static string SetSelection(int dealerId, string memberName)
        {

            BusinessUnit dealer = BusinessUnitFinder.Instance().Find(dealerId);

            if (dealer != null)
            {
                SetupSoftwareSystemComponentState(dealer, memberName);

                return "~/Default.aspx?token=DEALER_SYSTEM_COMPONENT&NoWait=true";
            }
            return "";
        }

        private static void SetupSoftwareSystemComponentState(BusinessUnit dealer, string memberName)
        {
            SoftwareSystemComponentState state = LoadState(DealerToken, memberName);
            state.DealerGroup.SetValue(dealer.DealerGroup());
            state.Dealer.SetValue(dealer);
            state.Member.SetValue(null);
            state.Save();
        }

        private static SoftwareSystemComponentState LoadState(string componentToken, string memberName)
        {
            SoftwareSystemComponentState state = SoftwareSystemComponentStateFacade.FindOrCreate(memberName, componentToken);

            if (state == null)
            {
                Member member = MemberFinder.Instance().FindByUserName(memberName);

                SoftwareSystemComponent component = SoftwareSystemComponentFinder.Instance().FindByToken(componentToken);

                state = new SoftwareSystemComponentState();
                state.AuthorizedMember.SetValue(member);
                state.SoftwareSystemComponent.SetValue(component);
            }

            return state;
        }
    }
}