using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Timers;
using System.Web.UI;
using System.Web.UI.Adapters;
using System.Web.UI.HtmlControls;
using System.Xml;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Workflow;
using log4net;
using MAX.Entities.Enumerations;
using Wanamaker.WebApp.Helpers;

namespace Wanamaker.WebApp.AppCode
{
    public class HtmlHeadAdapter : ControlAdapter
    {

        #region Logging

        private static readonly ILog Log = LogManager.GetLogger( typeof( HtmlHeadAdapter ).FullName );

        #endregion

        private delegate string ResolveUrlFunction ( string path );

        // Use this enum to indicate manufacturers that have css/images setup
        public enum OemBrandingSupport
        {
            BMW,
            Buick,
            Cadillac,
            Chevrolet,
            Chrysler,
            Ford,
            GMC,
            Honda
        }

        static HtmlHeadAdapter ()
        {
            //PageHasCss.Add( "dealerselector", false );
            //PageHasPrintCss.Add( "dealerselector", false );

            //PageHasPrintCss.Add( "home", false );
        }

        protected override void Render ( HtmlTextWriter writer )
        {
            HtmlHead head = ( HtmlHead )Control;

            writer.RenderBeginTag( HtmlTextWriterTag.Head );

            string pageName = Page.GetType().Name;
            int index = pageName.IndexOf( "_aspx" );

            if ( index >= 0 )
            {
                pageName = pageName.Substring( 0, index );
            }

            // Added 'if' statement to skip dynamic CSS insertion on a per page basis starting in 5.20.
            // Do this until codebase no longer requires dynamic CSS insertion (then delete).
            if ( !Page.Theme.Contains( "MAX60" ) && !Page.Theme.Contains( "MAX_2.0" ) && !Page.Theme.Contains( "None" ) )
            {
                string[] pageElements = pageName.Split( "_".ToCharArray() );
                if ( pageElements.Length > 1 )
                {
                    pageName = pageElements[ 1 ];
                }

                writer.AddAttribute( HtmlTextWriterAttribute.Href, 
                    HtmlHelpers.VersionedUrl(string.Format( "~/App_Themes/{0}/{1}.css", Page.Theme, pageName)));
                writer.AddAttribute( HtmlTextWriterAttribute.Rel, "stylesheet" );
                writer.AddAttribute( HtmlTextWriterAttribute.Type, "text/css" );
                writer.AddAttribute( "media", "screen" );
                writer.RenderBeginTag( HtmlTextWriterTag.Link );
                writer.RenderEndTag();

            }
            else
            {
                #region Theme CSS

                if (!Page.Theme.Contains("None"))
                {
                    string themeName = Page.Theme;

                    if (string.IsNullOrEmpty(themeName))
                    {
                        themeName = Page.StyleSheetTheme;
                    }

                    if (!string.IsNullOrEmpty(themeName))
                    {
                        CssControlCollection collection = GetThemeLinks(Page.Request.PhysicalApplicationPath, themeName,
                                                                        Page.ResolveUrl);
                        collection.LockObject.AcquireReaderLock(Timeout.Infinite);
                        try
                        {
                            foreach (Control ctrl in collection)
                            {
                                AppendVersionToCssLink(ctrl as HtmlLink);
                                ctrl.RenderControl(writer);
                                writer.WriteLine();
                            }
                        }
                        finally
                        {
                            collection.LockObject.ReleaseReaderLock();
                        }
                    }
                }

                #endregion
            }

            
            foreach(Control control in head.Controls)
            {
                var link = control as HtmlLink;
                if ( link != null )
                {
                    if (link.Href.StartsWith("~/App_Themes/", StringComparison.InvariantCultureIgnoreCase)) continue;

                    AppendVersionToCssLink(link);
                }

                control.RenderControl( writer );
                writer.WriteLine();
            }

            // OEM branding support
            if ( !Page.Theme.Equals( "MAX_2.0" ) && !Page.Theme.Equals( "MAX60" ) ) // If not Snippet Collector or admin pages
            {
                bool isMax30;

                try
                {
                    isMax30 = ( ( BasePage )Page ).IsMax30;

                    // MAX 2.0 Styles
                    if ( !isMax30 )
                    {
                        HtmlLink max20CssHtmlLink = new HtmlLink();
                        max20CssHtmlLink.Attributes[ "type" ] = "text/css";

                        writer.AddAttribute( HtmlTextWriterAttribute.Href, HtmlHelpers.VersionedUrl("~/Themes/MAX2.0/styles.css"));
                        writer.AddAttribute( HtmlTextWriterAttribute.Rel, "stylesheet" );
                        writer.AddAttribute( HtmlTextWriterAttribute.Type, "text/css" );
                        writer.AddAttribute( "media", "screen" );
                        writer.RenderBeginTag( HtmlTextWriterTag.Link );
                        writer.RenderEndTag();
                    }

                    var optionsCommand = GetMiscSettings.GetSettings(WorkflowState.BusinessUnitID);

                    if ( optionsCommand.Franchise != Franchise.None )
                    {
                        HtmlLink oemCssHtmlLink = new HtmlLink();
                        oemCssHtmlLink.Attributes[ "type" ] = "text/css";

                        if ( Enum.IsDefined( typeof( OemBrandingSupport ), optionsCommand.Franchise.ToString() ) )
                        {
                            writer.AddAttribute(HtmlTextWriterAttribute.Href,
                                                HtmlHelpers.VersionedUrl(
                                                    string.Format("~/App_Themes/OEM/{0}/styles{1}.css",
                                                                  optionsCommand.Franchise,
                                                                  isMax30 ? string.Empty : "20")));
                            writer.AddAttribute( HtmlTextWriterAttribute.Rel, "stylesheet" );
                            writer.AddAttribute( HtmlTextWriterAttribute.Type, "text/css" );
                            writer.AddAttribute( "media", "screen" );
                            writer.RenderBeginTag( HtmlTextWriterTag.Link );
                            writer.RenderEndTag();
                        }

                    }
                }
                catch (Exception exception)
                {
                    Log.Error( "OEM branding stylesheet failed", exception );
                }
            }

            writer.RenderEndTag();
        }

        private static readonly Dictionary<string, CssControlCollection> Themes = new Dictionary<string, CssControlCollection>();

        private static CssControlCollection GetThemeLinks ( string path, string themeName, ResolveUrlFunction resolveUrl )
        {
            CssControlCollection result;

            lock ( Themes )
            {
                if ( Themes.ContainsKey( themeName ) )
                {
                    result = Themes[ themeName ];
                }
                else
                {
                    string filename = string.Format( CultureInfo.InvariantCulture, "{0}App_Themes{1}{2}{1}css.xml", path, Path.DirectorySeparatorChar, themeName );

                    result = new CssControlCollection( new FileInfo( filename ), resolveUrl );

                    Themes.Add( themeName, result );
                }
            }

            return result;
        }

        sealed class CssControlCollection : CollectionBase
        {
            readonly ResolveUrlFunction _resolveUrl;
            private readonly FileInfo _fileInfo;
            private DateTime _lastWriteTime;
            private readonly System.Timers.Timer _timer;
            private readonly ReaderWriterLock _mutex = new ReaderWriterLock();

            public CssControlCollection ( FileInfo fileInfo, ResolveUrlFunction resolveUrl )
            {
                _fileInfo = fileInfo;
                _resolveUrl = resolveUrl;

                _timer = new System.Timers.Timer
                {
                    Interval = new TimeSpan( 0, 1, 0 ).TotalMilliseconds
                };
                _timer.Elapsed += OnElapsed;

                Load();
            }

            public Control this[ int index ]
            {
                get
                {
                    return ( Control )List[ index ];
                }
            }

            public ReaderWriterLock LockObject
            {
                get { return _mutex; }
            }

            protected override void OnValidate ( Object value )
            {
                bool isCorrectType = ( value.GetType() == typeof( HtmlLink ) );
                isCorrectType |= ( value.GetType() == typeof( ConditionalComment ) );
                if ( !isCorrectType )
                    throw new ArgumentException( "value must be of type HtmlLink or ConditionalComment.", "value" );
            }

            private void OnElapsed ( object source, ElapsedEventArgs e )
            {
                _timer.Stop();

                _mutex.AcquireReaderLock( Timeout.Infinite );
                try
                {
                    if ( File.GetLastWriteTime( _fileInfo.FullName ).CompareTo( _lastWriteTime ) > 0 )
                    {
                        LockCookie cookie = _mutex.UpgradeToWriterLock( Timeout.Infinite );
                        try
                        {
                            Load();
                        }
                        finally
                        {
                            _mutex.DowngradeFromWriterLock( ref cookie );
                        }
                    }
                }
                finally
                {
                    _mutex.ReleaseReaderLock();
                }
            }

            private void Load ()
            {
                List.Clear();

                XmlDocument document = new XmlDocument();

                document.Load( _fileInfo.FullName );

                _lastWriteTime = File.GetLastWriteTime( _fileInfo.FullName );

                XmlNodeList linksNodes = document.GetElementsByTagName( "links" )[ 0 ].ChildNodes;

                for ( int i = 0, l = linksNodes.Count; i < l; i++ )
                {
                    HtmlLink link = new HtmlLink();

                    XmlNode linkElement = linksNodes[ i ];

                    if ( _fileInfo.Directory != null )
                        link.Href = _resolveUrl( "~/App_Themes/"
                                                + _fileInfo.Directory.Name
                                                + "/"
                                                + linkElement.Attributes[ "href" ].Value );

                    XmlAttribute nameAtt = linkElement.Attributes[ "title" ];
                    if ( nameAtt != null )
                    {
                        link.Attributes.Add( "title", nameAtt.Value );
                    }

                    link.Attributes.Add( "type", linkElement.Attributes[ "type" ].Value );
                    link.Attributes.Add( "rel", linkElement.Attributes[ "rel" ].Value );
                    link.Attributes.Add( "media", linkElement.Attributes[ "media" ].Value );

                    XmlAttribute conditionAttr = linkElement.Attributes[ "condition" ];

                    if ( conditionAttr != null && !string.IsNullOrEmpty( conditionAttr.Value ) )
                    {
                        List.Add( new ConditionalComment( link, conditionAttr.Value ) );
                    }
                    else
                    {
                        List.Add( link );
                    }
                }

                _timer.Start();
            }
        }

        sealed class ConditionalComment : Control
        {
            private readonly HtmlLink _link;
            private readonly string _condition;

            public ConditionalComment ( HtmlLink link, string condition )
            {
                if ( string.IsNullOrEmpty( condition ) )
                    throw new ArgumentNullException( "condition" );

                _link = link;
                _condition = condition;
            }

            public override bool HasControls ()
            {
                return true;
            }

            protected override void CreateChildControls ()
            {
                Controls.Add( _link );
            }

            protected override void Render ( HtmlTextWriter writer )
            {
                writer.Write( "<!--[" );
                writer.Write( _condition );
                writer.Write( "]>" );
                writer.WriteLine();

                AppendVersionToCssLink(_link);
                _link.RenderControl( writer );

                writer.WriteLine();
                writer.Write( "<![endif]-->" );
            }
        }

        private static void AppendVersionToCssLink(HtmlLink ctrl)
        {
            if (ctrl != null && ctrl.Href.EndsWith(".css", StringComparison.InvariantCultureIgnoreCase))
                ctrl.Href = HtmlHelpers.VersionedUrl(ctrl.Href);
        }
    }
}