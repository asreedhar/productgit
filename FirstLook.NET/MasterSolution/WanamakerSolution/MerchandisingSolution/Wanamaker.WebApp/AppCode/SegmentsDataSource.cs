using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FirstLook.Merchandising.DomainModel.Vehicles;

namespace Wanamaker.WebApp.AppCode
{
    /// <summary>
    /// Summary description for SegmentsDataSource
    /// </summary>
    public class SegmentsDataSource
    {
        public SegmentsDataSource()
        {

        }
        public static Dictionary<int, string> FetchSegments()
        {

            Dictionary<int, string> retSegs = new Dictionary<int, string>();
            foreach (int val in Enum.GetValues(typeof(Segments)))
            {
                retSegs.Add(val, Enum.GetName(typeof(Segments), val));
            }
            return retSegs;
        }
    }
}