﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using MvcMiniProfiler;

namespace Wanamaker.WebApp
{
    public static class ProfilerSetting
    {
        public static string Permission = "MiniProfiler";

        public static bool HasProfilePermission()
        {
            string identityName = HttpContext.Current.User.Identity.Name;
            return Roles.IsUserInRole(identityName, Permission);
        }

        public static IHtmlString RenderIncludes()
        {
            IHtmlString includes = new HtmlString(String.Empty);
            if(HasProfilePermission())
                includes = MiniProfiler.RenderIncludes();

            return includes;
        }

    }
}