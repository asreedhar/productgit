﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace Wanamaker.WebApp.Helpers
{
    public static class HtmlHelpers
    {
#if DEBUG
        private static readonly string debugVersion = new System.Random().Next().ToString(CultureInfo.InvariantCulture);
#else
        private readonly static string cachedAppVersionString = FirstLook.Internal.FirstLookAssemblyInfo.Version.ToString(CultureInfo.InvariantCulture);
#endif

        public static string AppVersion
        {
            get
            {
#if DEBUG
                return debugVersion;
#else
                return cachedAppVersionString;
#endif
            }
        }

        public static string VersionedUrl(this UrlHelper helper, string relativeUrl)
        {
            return helper.Content(relativeUrl) + "?v=" + AppVersion;
        }

        public static string VersionedUrl(string relativeUrl)
        {
            return VirtualPathUtility.ToAbsolute(relativeUrl) + "?v=" + AppVersion;
        }

        public static bool IsDebugBuild()
        {
#if DEBUG
            return true;
#else
            return false;
#endif
        }

        // These APIs were inspired by jQuery.
        // See https://github.com/jquery/jquery/blob/d3320462df1253196e61b2daadc3cdfe1b4c3771/src/attributes.js#L39

        private static readonly char[] space = {' '};

        public static IAttributeAccessor AddClass(this IAttributeAccessor ctrl, string classes)
        {
            ctrl.SetAttribute("class",
                              classes
                                  .Split(space, StringSplitOptions.RemoveEmptyEntries)
                                  .Aggregate(" " + ctrl.GetAttribute("class") + " ",
                                             (current, cls) => current.Contains(cls) ? current : current + cls + " ")
                                  .Trim()
                );
            return ctrl;
        }

        public static IAttributeAccessor RemoveClass(this IAttributeAccessor ctrl, string classes)
        {
            ctrl.SetAttribute("class",
                              classes
                                  .Split(space, StringSplitOptions.RemoveEmptyEntries)
                                  .Aggregate(" " + ctrl.GetAttribute("class") + " ",
                                             (current, cls) => current.Replace(" " + cls + " ", " "))
                                  .Trim()
                );
            
            return ctrl;
        }

        public static bool HasClass(this IAttributeAccessor ctrl, string cls)
        {
            return (" " + ctrl.GetAttribute("class") + " ").Contains(" " + cls + " ");
        }

        public static IAttributeAccessor UpdateClass(this IAttributeAccessor ctrl, string classes, bool shouldAdd)
        {
            if (shouldAdd)
                ctrl.AddClass(classes);
            else
                ctrl.RemoveClass(classes);
            return ctrl;
        }

        public static bool IsRubyPageMatch(string url)
        {
            return Regex.IsMatch(url, @"https?://.*firstlook\.biz/home$");
        }

        //find out if they came from the ruby home page. Bugzid: 24167
        public static bool FromRubyHomePage(this HttpRequest request)
        {
            return (request.UrlReferrer != null && IsRubyPageMatch(request.UrlReferrer.AbsoluteUri));
        }

        //find out if they came from the ruby home page. Bugzid: 24167
        public static bool FromRubyHomePage(this HttpRequestBase request)
        {
            return (request.UrlReferrer != null && IsRubyPageMatch(request.UrlReferrer.AbsoluteUri));
        }
    }
}