using System.Web;
using System.Web.Routing;

namespace Wanamaker.WebApp.Helpers
{
    public class RedirectRouteHandler : IRouteHandler
    {
        private string RedirectUrl { get; set; }
        private bool AppendQueryString { get; set; }

        public RedirectRouteHandler(string redirectUrl, bool appendQuerystring)
        {
            RedirectUrl = redirectUrl;
            AppendQueryString = appendQuerystring;
        }

        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            return new RedirectHandler(this);
        }

        private class RedirectHandler : IHttpHandler
        {
            private RedirectRouteHandler RouteHandler { get; set; }

            public RedirectHandler(RedirectRouteHandler routeHandler)
            {
                RouteHandler = routeHandler;
            }

            public void ProcessRequest(HttpContext context)
            {
                var redirectUri = VirtualPathUtility.ToAbsolute(RouteHandler.RedirectUrl, context.Request.ApplicationPath);
                if(RouteHandler.AppendQueryString && !string.IsNullOrEmpty(context.Request.Url.Query))
                {
                    redirectUri += context.Request.Url.Query;
                }

                context.Response.RedirectPermanent(redirectUri);
            }

            public bool IsReusable
            {
                get { return true; }
            }
        }
    }
}