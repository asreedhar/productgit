﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.DomainModel.Oltp;
using FirstLook.Internal;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Workflow;

namespace Wanamaker.WebApp
{
    public class BasePage : Page
    {

        public string AssemblyVersion
        {
            get
            {
                return FirstLookAssemblyInfo.Version;
            }
        }

        public virtual string BulkUploadUrl
        {
            get
            {
                return BusinessUnit != null ? PhotoService.GetBulkUploadManagerUrl( BusinessUnit.Id, PhotoManagerContext.MAX ) : "";
            }
        }

        public virtual BusinessUnit BusinessUnit
        {
            get { return WorkflowState.BusinessUnit; }
        }

        public virtual int BusinessUnitId
        {
            get { return WorkflowState.BusinessUnitID; }
        }

        public virtual int InventoryId
        {
            get { return WorkflowState.InventoryId; }
        }

        public virtual bool IsMax30
        {
            get { return GetMiscSettings.GetSettings( BusinessUnitId ).MaxVersion == 3; }
        }

        public virtual IPhotoServices PhotoService { get; set; }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            ClientScript.RegisterHiddenField("BusinessUnitIdHidden", BusinessUnitId.ToString());
        }
    }
}