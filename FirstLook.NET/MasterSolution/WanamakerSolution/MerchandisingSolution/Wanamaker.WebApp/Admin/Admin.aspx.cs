using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.IOC;
using FirstLook.DomainModel.Oltp;
using FirstLook.LotIntegration.DomainModel;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Templating;
using log4net;
using Merchandising.Messages;
using Merchandising.Messages.AutoApprove;
using Merchandising.Messages.AutoLoad;

namespace Wanamaker.WebApp.Admin
{

    public partial class Admin : BasePage
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger( typeof( Admin ).FullName );
        #endregion

        AutoloadManager _autoloadManager = new AutoloadManager(Registry.Resolve<IAutoloadRepository>());

        #region Injected dependencies

        public IAdMessageSender MessageSender { get; set; }

        #endregion

        protected void Page_Load ( object sender, EventArgs e )
        {

            if ( !IsPostBack )
            {
                rolesCBL.DataSource = Roles.GetAllRoles();

                rolesCBL.DataBind();

                manufacturerGridView.DataSource = _autoloadManager.GetSupportedManufacturerList();

                manufacturerGridView.DataBind();

                usersList.DataSource = Membership.GetAllUsers();
                usersList.DataBind();
                
                vehicleSets.BusinessUnitId = 100150;

                var names = Enum.GetNames(typeof (Agent));
                foreach(var name in names)
                    AgentDropDown.Items.Add(new ListItem(name));

                AgentDropDown.SelectedIndex = 1;
            }
        }

        protected void BuidDDL_Bound ( object sender, EventArgs e )
        {
            BuidDDL.Items.Insert( 0, new ListItem( "FIRSTLOOK SYSTEM SETTINGS", "100150" ) );
            var bu = HttpContext.Current.Items[BusinessUnit.HttpContextKey] as BusinessUnit;
            if(bu != null)
            {
                var item = BuidDDL.Items.OfType<ListItem>().Where(x => int.Parse(x.Value) == bu.Id).SingleOrDefault();
                if (item != null)
                {
                    BuidDDL.SelectedValue = item.Value;
                    BUID_Changed();
                }
            }

            SetBusinessUnitName();
        }

        protected void rolesCBL_DataBound ( object sender, EventArgs e )
        {
            CheckBoxList list = sender as CheckBoxList;

            if ( list == null ) return;

            foreach ( ListItem item in list.Items )
            {
                const string ADMIN_LABEL = "(INTERNAL USERS ONLY!)";

                if ( item.Value.Equals( "Administrator" ) )
                {
                    item.Text = item.Text.Contains( ADMIN_LABEL ) ? item.Text : string.Format( "{0} {1}", item.Text, ADMIN_LABEL );
                    return;
                }
            }
        }

        protected void CreateUserClick ( object sender, EventArgs e )
        {
            string createUserName = userName.Text;
            if ( !string.IsNullOrEmpty( createUserName ) )
            {
                MerchandisingMember.CreateMerchandisingUser( createUserName );

                userName.Text = "";
                rolesCBL.DataBind();
                usersList.DataSource = Membership.GetAllUsers();
                usersList.DataBind();

                if ( usersList.Items.FindByValue( createUserName ) != null )
                {
                    usersList.SelectedValue = createUserName;
                    UserSelected( usersList, e );
                }
            }
        }

        protected void UsersBound ( object sender, EventArgs e )
        {
            usersList.Items.Insert( 0, "Select user..." );
        }

        protected void SetRoles_Click ( object sender, EventArgs e )
        {
            if ( usersList.SelectedIndex != 0 )
            {
                string user = usersList.SelectedValue;
                foreach ( ListItem cbRole in rolesCBL.Items )
                {
                    string role = cbRole.Value;
                    if ( cbRole.Selected )
                    {
                        if ( !Roles.IsUserInRole( user, role ) )
                        {
                            Roles.AddUserToRole( user, role );
                        }
                    }
                    else
                    {
                        if ( Roles.IsUserInRole( user, role ) )
                        {
                            Roles.RemoveUserFromRole( user, role );
                        }
                    }

                }
            }
        }

        protected void manufacturerGridView_RowDataBound ( object sender, GridViewRowEventArgs e )
        {
            var chk = e.Row.FindControl( "ManufacturerAutoLoadCheckBox" ) as CheckBox;
            if ( chk == null ) { return; }

            var isAutoLoadEnabled = e.Row.FindControl( "ManufacturerIsAutoLoadEnabledHiddenField" ) as HiddenField;
            if ( isAutoLoadEnabled == null ) { return; }

            chk.Checked = bool.Parse( isAutoLoadEnabled.Value );
        }

        protected void SaveAutoLoadSettingsButton_Click ( object sender, EventArgs e )
        {
            var idList = ( from GridViewRow row in manufacturerGridView.Rows
                           let chk = row.FindControl( "ManufacturerAutoLoadCheckBox" ) as CheckBox
                           where chk != null
                           let id = row.FindControl( "ManufacturerIdHiddenField" ) as HiddenField
                           where id != null
                           where chk.Checked
                           select int.Parse( id.Value ) ).ToList();

            _autoloadManager.UpdateAutoLoadSettings(idList);
        }

        protected void OnRequeueErrors ( object sender, EventArgs e )
        {
            Agent agent = (Agent)Enum.Parse(typeof(Agent), AgentDropDown.SelectedItem.Text, true);

            MessageSender.SendBatchAutoLoadInventory(new AutoLoadInventoryMessage(-1, AutoLoadInventoryType.AgentErrors) { Agent = agent }, GetType().FullName);
        }

        protected void UserSelected ( object sender, EventArgs e )
        {
            if ( usersList.SelectedIndex != 0 )
            {
                List<string> roles = new List<string>( Roles.GetRolesForUser( usersList.SelectedValue ) );

                foreach ( ListItem li in rolesCBL.Items )
                {
                    li.Selected = roles.Contains( li.Value );
                }
            }
            else
            {
                foreach ( ListItem li in rolesCBL.Items )
                {
                    li.Selected = false;
                }
            }
        }

        protected void DealerSetup_Click ( object sender, EventArgs e )
        {
            int buid = Int32.Parse( BuidDDL.SelectedValue );
            try
            {
                MerchandisingPreferences prefs = MerchandisingPreferences.Fetch( buid );
                //prefs exists, so don't override dealer setup
                Page.ClientScript.RegisterStartupScript( Page.GetType(), "alertDealer" + buid, "alert('dealer already setup');", true );
            }
            catch
            {
                Page.ClientScript.RegisterStartupScript( Page.GetType(), "alertDealerSuccess" + buid, "alert('dealer setup successful!');", true );
                MerchandisingPreferences.SetupDealer( buid );
                miscSettings.Refresh();
            }
        }

        private void BUID_Changed()
        {
            int buid = int.Parse(BuidDDL.SelectedValue);
            snippets.BusinessUnitID = buid;
            SetBusinessUnitName();
            vehicleSets.BusinessUnitId = buid;
            miscSettings.BusinessUnitId = buid;
            upgradeSettings.BusinessUnitId = buid;
        }

        protected void BUID_Changed ( object sender, EventArgs e )
        {
            BUID_Changed();
        }

        private void SetBusinessUnitName ()
        {
            lblBUName.Text = BuidDDL.SelectedItem == null
                                     ? string.Empty
                                     : string.Format( "These settings apply to the business unit '{0}'", BuidDDL.SelectedItem.Text );
        }

    }
}