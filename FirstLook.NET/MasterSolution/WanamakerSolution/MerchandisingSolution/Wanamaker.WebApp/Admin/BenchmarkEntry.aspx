﻿<%@ Page Title="" Language="C#" Theme="MAX_2.0" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="True" CodeBehind="BenchmarkEntry.aspx.cs" Inherits="Wanamaker.WebApp.Admin.BenchmarkEntry" %>
<%@ Register TagName="BenchmarkSettings" TagPrefix="bnchMrk" Src="~/Admin/Controls/BenchmarkSettings.ascx" %>
<%@ Import Namespace="Wanamaker.WebApp.Helpers" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/App_Themes/MAX60/MAX.Admin.debug.css") %>" />
    <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MaxMvc/jquery-ui-1.7.2.custom.css")%>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="Benchmarks">
        <div class="dateSelector">
            <asp:Label AssociatedControlID="month" runat="server">Date: </asp:Label>
            <asp:TextBox ID="month" name="month" CssClass="monthPicker" runat="server" AutoPostBack="true" OnTextChanged="Month_OnTextChanged" data-bind="value:selected_month, valueUpdate:'afterkeydown'"></asp:TextBox>
        </div>
        <bnchMrk:BenchmarkSettings ID="CarsBenchmarkSettings" ListingSite="CarsDotCom" SectionTitle="Cars.com" runat="server" />
        <bnchMrk:BenchmarkSettings ID="AutotraderBenchmarkSettings" ListingSite="AutoTrader" SectionTitle="AutoTrader" runat="server" />

        <div id="BtnSaveWrap">
            <asp:Button ID="BtnSave" Text="Save" runat="server" OnClick="BtnSave_OnClick"/>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
<script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Lib/jquery.ui.monthpicker.js") %>"></script>
<script type="text/javascript">
    $('.monthPicker').monthpicker({
        dateFormat: 'M yy'
    });
    $('#ui-monthpicker-div').css({ display: 'none' }); // fix for 3rd-party bug
</script>
</asp:Content>
