﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Merchandising.DomainModel.Postings;
using FirstLook.Common.Core.Utilities;
using Wanamaker.WebApp.Admin.Controls;
using System.Globalization;

namespace Wanamaker.WebApp.Admin
{
    public partial class BenchmarkEntry : System.Web.UI.Page
    {
        private DateTime _benchmarkMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        private List<EdtDestinations> SiteList = new List<EdtDestinations>();

        public DateTime BenchmarkMonth
        {
            get
            {
                return _benchmarkMonth;
            }
            set
            {
                _benchmarkMonth = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            if (!String.IsNullOrWhiteSpace(month.Text))
                BenchmarkMonth = DateTime.ParseExact(month.Text, "MMM yyyy", CultureInfo.CurrentCulture);
            else
                month.Text = BenchmarkMonth.ToString("MMM yyyy");

            CarsBenchmarkSettings.BenchmarkMonth = BenchmarkMonth;
            AutotraderBenchmarkSettings.BenchmarkMonth = BenchmarkMonth;
        }

        public void BtnSave_OnClick(object sender, EventArgs e)
        {
            CarsBenchmarkSettings.BenchmarkMonth = DateTime.Parse(month.Text);
            AutotraderBenchmarkSettings.BenchmarkMonth = DateTime.Parse(month.Text);

            CarsBenchmarkSettings.Save();
            AutotraderBenchmarkSettings.Save();
        }

        protected void Month_OnTextChanged(object sender, EventArgs e)
        {
            LoadData();
            CarsBenchmarkSettings.LoadData();
            AutotraderBenchmarkSettings.LoadData();
        }
    }
}