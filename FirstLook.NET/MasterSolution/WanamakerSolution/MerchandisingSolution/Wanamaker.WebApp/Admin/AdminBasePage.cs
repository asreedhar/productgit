﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FirstLook.Merchandising.DomainModel.Workflow;

namespace Wanamaker.WebApp.Admin
{
    public class AdminBasePage : BasePage
    {
        public int BusinessUnitId
        {
            get { return WorkflowState.BusinessUnitID; }
        }

        public int InventoryId
        {
            get { return WorkflowState.InventoryId; }
        }
    }
}