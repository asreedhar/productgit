<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="Admin.aspx.cs" Theme="MAX_2.0" MasterPageFile="~/Admin/Admin.Master" Title="Admin | MAX : Online Inventory. Perfected." Inherits="Wanamaker.WebApp.Admin.Admin"
    EnableTheming="true" EnableEventValidation="false" ValidateRequest="false" %>
<%@ Import Namespace="Wanamaker.WebApp.Helpers" %>

<%@ Register TagPrefix="carbuilder" TagName="SnippetEditor" Src="~/CustomizeMAX/Controls/SnippetEditor.ascx" %>
<%@ Register TagPrefix="carbuilder" TagName="VehicleSetCreator" Src="~/Admin/Controls/VehicleSetCreator.ascx" %>
<%@ Register TagPrefix="carbuilder" TagName="MiscSettings" Src="~/Admin/Controls/MiscSettings.ascx" %>
<%@ Register TagPrefix="carbuilder" tagname="upgradeSettings" src="~/Admin/Controls/upgradeSettings.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/App_Themes/MAX60/MAX.Admin.debug.css") %>" />

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <div class="adminContent clearfix">

            <div id="DealerSelector">
                <asp:SqlDataSource ID="DealersDS" runat="server" ConnectionString='<%$ ConnectionStrings:Merchandising %>' SelectCommandType="StoredProcedure"
                    SelectCommand="builder.Dealers#Fetch"></asp:SqlDataSource>
                <asp:DropDownList ID="BuidDDL" runat="server" DataSourceID="DealersDS" DataTextField="BusinessUnit" DataValueField="BusinessUnitId"
                    OnDataBound="BuidDDL_Bound" OnSelectedIndexChanged="BUID_Changed" AutoPostBack="true">
                </asp:DropDownList>
                <asp:Button ID="DealerSetupButton" runat="server" Text="Run Initial Dealer Setup" OnClick="DealerSetup_Click" />
            </div>


            <cwc:TabContainer runat="server" ID="Tabset" ActiveTabIndex="4" CssClass="clearfix">
                <cwc:TabPanel ID="snippetPanel" runat="server" Text="Snippets" CssClass="clearfix">
                    <ContentTemplate>
                        <carbuilder:SnippetEditor ID="snippets" runat="server" />
                    </ContentTemplate>
                </cwc:TabPanel>
                <cwc:TabPanel ID="membersPanel" runat="server" Text="Users" CssClass="clearfix">
                    <div id="CreateUserForm">
                        <h4>
                            Create New User</h4>
                        <label for="<%= userName.ClientID %>">
                            FirstLook UserName:</label>
                        <asp:TextBox ID="userName" runat="server"></asp:TextBox><br />
                        <asp:Button ID="CreateUserButton" runat="server" OnClick="CreateUserClick" Text="Create User" />
                    </div>
                    <div id="ExistingUsersForm">
                        <h4>
                            Existing Users</h4>
                        <asp:DropDownList ID="usersList" runat="server" DataTextField="UserName" DataValueField="UserName" AutoPostBack="true" OnSelectedIndexChanged="UserSelected"
                            OnDataBound="UsersBound" />
                        <div>
                            <asp:CheckBoxList ID="rolesCBL" runat="server" OnDataBound="rolesCBL_DataBound">
                            </asp:CheckBoxList>
                            <asp:Button ID="SetRoles" runat="server" Text="Set User Roles" OnClick="SetRoles_Click" />
                        </div>
                    </div>
                    <div class="clearfix">
                        &nbsp;</div>
                </cwc:TabPanel>
                <cwc:TabPanel ID="frameworkSet" runat="server" Text="Vehicle Framework Defaults">
                    <carbuilder:VehicleSetCreator runat="server" ID="vehicleSets"></carbuilder:VehicleSetCreator>
                </cwc:TabPanel>
                <cwc:TabPanel ID="AutoloadSettings" runat="server" Text="Autoload Settings" CssClass="explicitly_hidden">
                    <div id="SetManufacturerAutoloadSettings">
                        <h4>
                            Currently Supported Manufacturer List</h4>
                        <asp:GridView ID="manufacturerGridView" runat="server" AutoGenerateColumns="False"  OnRowDataBound="manufacturerGridView_RowDataBound">
                            <AlternatingRowStyle BackColor="#DDDDDD" />
                            <EmptyDataTemplate>
                                Manufacturer List could not be loaded.</EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Manufacturer" ControlStyle-Width="150px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="ManufacturerNameLabel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'></asp:Label>
                                        <asp:HiddenField ID="ManufacturerIdHiddenField" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Id") %>' />
                                        <asp:HiddenField ID="ManufacturerIsAutoLoadEnabledHiddenField" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.IsAutoLoadEnabled") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Auto Load">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="ManufacturerAutoLoadCheckBox" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:Button ID="SaveAutoLoadSettingsButton" runat="server" Text="Save Auto Load Settings" OnClick="SaveAutoLoadSettingsButton_Click" />
                        <div id="requeue">
                            <asp:DropDownList ID="AgentDropDown" runat="server"/> <asp:Button Text="Requeue Errors" runat="server" OnClick="OnRequeueErrors"/>
                        </div>
                    </div>
                </cwc:TabPanel>
                <cwc:TabPanel ID="miscSettingsTab" runat="server" Text="Miscellaneous Settings">
                    <h4><asp:Label ID="lblBUName" runat="server" Text=""></asp:Label></h4>
                    <carbuilder:MiscSettings runat="server" ID="miscSettings"></carbuilder:MiscSettings>
                </cwc:TabPanel>
                <cwc:TabPanel ID="upgradeSettingsTab" runat="server" Text="Upgrades">
                    <carbuilder:upgradeSettings ID="upgradeSettings" runat="server" />

                </cwc:TabPanel>

            </cwc:TabContainer>

        </div>

</asp:Content>

