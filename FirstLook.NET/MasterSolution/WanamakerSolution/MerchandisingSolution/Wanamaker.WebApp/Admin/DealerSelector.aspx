<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="DealerSelector.aspx.cs" Inherits="Wanamaker.WebApp.Admin.DealerSelector" %>
<%@ OutputCache Location="None" VaryByParam="None" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Choose Dealer | MAX : Online Inventory. Perfected.</title>
    <style type="text/css">
fieldset
{
    border: 1px solid #0066FF;
    float: left;
    padding: 5px;
    margin: 5px;
}
legend
{
    color: #0066FF;
    padding: 2px 6px
}
thead
{
    background-color: #0066FF;
}
table,td
{
    border: 1px solid black;
}
tr.table-row-selected
{
    background-color: #BBBBBB;
}
.table-row-pagination
{
    text-align: center;
}
    </style>
    <link rel="shortcut icon" href="../App_Themes/MAX60/images/max.ico" type="image/x-icon" />
</head>
<body>
    <form id="form1" runat="server" defaultfocus="DealerGroupFilter" defaultbutton="DealerGroupFilterButton">
        <fieldset>
            <legend>Dealer Groups</legend>
            <div>
                <asp:Label
                    ID="DealerGroupFilterLabel"
                    AssociatedControlID="DealerGroupFilter"
                    runat="server"
                    Text="Filter Name: ">
                </asp:Label>
                <asp:TextBox
                    ID="DealerGroupFilter"
                    runat="server"
                    AutoPostBack="true"
                    AccessKey="1"
                    TabIndex="1"
                    ToolTip="Dealer Group Filter"
                    OnTextChanged="DealerGroupFilter_TextChanged">
                </asp:TextBox>
                <asp:Button ID="DealerGroupFilterButton" runat="server" Text="Filter" TabIndex="2" />
                <br />
                <asp:CheckBox ID="chkWanamakerGroupsOnly" runat="server" Text="Show Only Wanamaker Groups" AutoPostBack="true" OnCheckedChanged="chkWanamakerGroupsOnly_CheckedChanged" />
            </div>
            <div>
                <asp:ObjectDataSource
                    ID="DealerGroupDataSource"
                    TypeName="FirstLook.Merchandising.DomainModel.MerchandisingBusinessUnits"
                    SelectMethod=""
                    runat="server">
                    <SelectParameters>
                        <owc:MemberParameter Name="member" Type="Object" AllowImpersonation="false" />
                        <asp:ControlParameter Name="filter" Type="string" ControlID="DealerGroupFilter" DefaultValue="" ConvertEmptyStringToNull="true" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:GridView ID="DealerGroupGridView" runat="server"
                        DataKeyNames="Id"
                        DataSourceID="DealerGroupDataSource"
                        AutoGenerateColumns="false"
                        AllowPaging="true"
                        PageSize="20"
                        OnPageIndexChanged="DealerGroupGridView_PageIndexChanged">
                    <Columns>
                        <asp:BoundField DataField="Name" HeaderText="Name" />
                        <asp:CommandField ButtonType="Button" SelectText="Show Dealers" ShowSelectButton="true" />
                    </Columns>
                </asp:GridView>
            </div>
        </fieldset>
        <fieldset>
            <legend>Dealers</legend>
            <div>
                <asp:Label
                    ID="DealerFilterLabel"
                    AssociatedControlID="DealerFilter"
                    runat="server"
                    Text="Filter Name: ">
                </asp:Label>
                <asp:TextBox
                    ID="DealerFilter"
                    runat="server"
                    AutoPostBack="true"
                    AccessKey="2"
                    TabIndex="3"
                    ToolTip="Dealer Filter"
                    OnTextChanged="DealerFilter_TextChanged">
                </asp:TextBox>
                <asp:Button ID="DealerFilterButton" runat="server" Text="Filter" TabIndex="4" />
                <br />
                <asp:CheckBox ID="chkWanamakerDealersOnly" runat="server" Text="Show Only Wanamaker Dealers" AutoPostBack="true" OnCheckedChanged="chkWanamakerDealersOnly_CheckedChanged" />
            </div>
            <div>
                <asp:ObjectDataSource
                    ID="DealerDataSource"
                    TypeName="FirstLook.Merchandising.DomainModel.MerchandisingBusinessUnits"
                    SelectMethod=""
                    runat="server">
                    <SelectParameters>
                        <owc:MemberParameter Name="member" Type="Object" AllowImpersonation="false" />
                        <asp:ControlParameter Name="dealerGroupId" Type="Int32" ControlID="DealerGroupGridView" DefaultValue="" ConvertEmptyStringToNull="true" />
                        <asp:ControlParameter Name="filter" Type="string" ControlID="DealerFilter" DefaultValue="" ConvertEmptyStringToNull="true" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:GridView ID="DealerGridView" runat="server"
                        DataKeyNames="Id"
                        DataSourceID="DealerDataSource"
                        AutoGenerateColumns="false"
                        AllowPaging="true"
                        PageSize="20"
                        UseAccessibleHeader="true"
                        OnRowCommand="DealerGridView_RowCommand"
                        OnRowDataBound="DealerGridView_RowDataBound"
                        OnPageIndexChanged="DealerGridView_PageIndexChanged">
                    <Columns>
                        <asp:BoundField DataField="Name" HeaderText="Name" />
                        <asp:ButtonField ButtonType="Button" Text="Enter" CommandName="EnterDealer" />
                        
                    </Columns>
                </asp:GridView>
            </div>
        </fieldset>
        
    </form>
</body>
</html>
