using System;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Utilities;
using BusinessUnit=FirstLook.DomainModel.Oltp.BusinessUnit;
using BusinessUnitFinder=FirstLook.DomainModel.Oltp.BusinessUnitFinder;
using SoftwareSystemComponentStateFacade=FirstLook.DomainModel.Oltp.SoftwareSystemComponentStateFacade;
using FirstLook.DomainModel.Oltp;
using Wanamaker.WebApp.AppCode.AccessControl;

namespace Wanamaker.WebApp.Admin
{

    public partial class DealerSelector : BasePage
    {
        //
        // Support Methods
        //

        private static readonly string DealerGroupToken = SoftwareSystemComponentStateFacade.DealerGroupComponentToken;

        private static readonly string DealerToken = SoftwareSystemComponentStateFacade.DealerComponentToken;

        protected SoftwareSystemComponentState SoftwareSystemComponentState
        {
            get { return (FirstLook.DomainModel.Oltp.SoftwareSystemComponentState)Context.Items[SoftwareSystemComponentStateFacade.HttpContextKey]; }
            set { Context.Items[SoftwareSystemComponentStateFacade.HttpContextKey] = value; }
        }

        //
        // Page Initialize
        //

        protected void Page_Init(object sender, EventArgs e)
        {
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();

            MembershipUser user = Context.Items[IdentityHelper.MembershipContextKey] as MembershipUser;
            if (user != null)
            {
                //if (User.IsInRole("Administrator") || User.IsInRole("AccountRepresentative"))
                if (Roles.IsUserInRole(user.UserName, "Administrator"))
                {
                    SoftwareSystemComponentState = LoadState(DealerGroupToken);
                }
            }
            else
            {
                Response.Redirect("~/Default.aspx", true);
            }

            chkWanamakerGroupsOnly.Checked = true;
            chkWanamakerDealersOnly.Checked = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (chkWanamakerGroupsOnly.Checked)
                DealerGroupDataSource.SelectMethod = "FindAllMerchandisingDealerGroups";
            else
                DealerGroupDataSource.SelectMethod = "FindAllDealerGroupsForMember";
   
            if (chkWanamakerDealersOnly.Checked)
                DealerDataSource.SelectMethod = "FindAllMerchandisingDealershipsByGroup";
            else
                DealerDataSource.SelectMethod = "FindAllDealersForMemberAndDealerGroup";
        }

        protected void DealerGroupFilter_TextChanged(object sender, EventArgs e)
        {
            DealerGroupGridView.SelectedIndex = -1;
            DealerFilter.Text = "";
            DealerFilter_TextChanged(sender, e);
        }

        protected void DealerGroupGridView_PageIndexChanged(object sender, EventArgs e)
        {
            DealerGroupFilter_TextChanged(sender, e);
        }

        protected void DealerFilter_TextChanged(object sender, EventArgs e)
        {
            DealerGridView.SelectedIndex = -1;
        }

        protected void DealerGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("EnterDealer"))
            {
                int selectedIndex = Int32Helper.ToInt32(e.CommandArgument);

                GridView grid = sender as GridView;

                if (grid != null)
                {
                    int dealerId = Int32Helper.ToInt32(grid.DataKeys[selectedIndex]["Id"]);

                    BusinessUnit dealer = BusinessUnitFinder.Instance().Find(dealerId);

                    if (dealer != null)
                    {
                        SetupSoftwareSystemComponentState(dealer);
                        Response.Redirect("~/Default.aspx?token=DEALER_SYSTEM_COMPONENT&NoWait=true", false);
                        Context.ApplicationInstance.CompleteRequest();
                    }
                }
            }
        }

        protected void DealerGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GridView view = sender as GridView;

                if (view != null)
                {
                    BusinessUnit dealer = e.Row.DataItem as BusinessUnit;

                    if (dealer != null)
                    {
                        //                    bool upgraded = dealer.HasDealerUpgrade(Upgrade.PingTwo);
                        e.Row.Cells[1].Enabled = true;
                    }
                }
            }
        }

        protected void DealerGridView_PageIndexChanged(object sender, EventArgs e)
        {
            DealerFilter_TextChanged(sender, e);
        }

        protected void chkWanamakerGroupsOnly_CheckedChanged(object sender, EventArgs e)
        {
            //CheckBox chkSender = (CheckBox)sender;
            //if (chkSender.Checked)
            //    DealerGroupDataSource.SelectMethod = "FindAllMerchandisingDealerGroups";
            //else
            //    DealerGroupDataSource.SelectMethod = "FindAllDealerGroupsForMember";
            DealerGroupGridView.SelectedIndex = -1;
            DealerGroupGridView.DataBind();
            //DealerGridView.DataBind();
        }

        protected void chkWanamakerDealersOnly_CheckedChanged(object sender, EventArgs e)
        {
            //CheckBox chkSender = (CheckBox)sender;
            //if (chkSender.Checked)
            //    DealerDataSource.SelectMethod = "FindAllMerchandisingDealershipsByGroup";
            //else
            //    DealerDataSource.SelectMethod = "FindAllDealersForMemberAndDealerGroup";
            DealerGridView.DataBind();
        }

        private void SetupSoftwareSystemComponentState(BusinessUnit dealer)
        {
            SoftwareSystemComponentState state = LoadState(DealerToken);
            state.DealerGroup.SetValue(dealer.DealerGroup());
            state.Dealer.SetValue(dealer);
            state.Member.SetValue(null);
            state.Save();
        }

        private SoftwareSystemComponentState LoadState(string componentToken)
        {
            SoftwareSystemComponentState state = SoftwareSystemComponentStateFacade.FindOrCreate(User.Identity.Name, componentToken);

            if (state == null)
            {
                Member member = MemberFinder.Instance().FindByUserName(User.Identity.Name);

                SoftwareSystemComponent component = SoftwareSystemComponentFinder.Instance().FindByToken(componentToken);

                state = new SoftwareSystemComponentState();
                state.AuthorizedMember.SetValue(member);
                state.SoftwareSystemComponent.SetValue(component);
            }

            return state;
        }
    }
}