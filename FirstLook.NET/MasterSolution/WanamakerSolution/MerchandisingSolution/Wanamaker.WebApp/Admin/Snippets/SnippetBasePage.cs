﻿using System;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Merchandising.DomainModel.SnippetCollector;
using Newtonsoft.Json;

namespace Wanamaker.WebApp.Admin.Snippets
{
    public abstract class SnippetBasePage : Page
    {
        public SnippetCollectorDataAccess DataAccess = new SnippetCollectorDataAccess();

        [WebMethod]
        public static string GetSnippets(string json)
        {
            SnippetCollectorDataAccess dataAccess = new SnippetCollectorDataAccess();
            return dataAccess.GetAllSnippets(json);
        }

        [WebMethod]
        public static string GetSnippetsByStyle(string json)
        {
            SnippetCollectorDataAccess dataAccess = new SnippetCollectorDataAccess();
            return dataAccess.GetSnippetsByStyle(json);
        }

        [WebMethod]
        public static string AddSnippet(string json)
        {
            SnippetCollectorDataAccess dataAccess = new SnippetCollectorDataAccess();
            return dataAccess.AddSnippet(json);
        }

        [WebMethod]
        public static string UpdateSnippet(string json)
        {
            SnippetCollectorDataAccess dataAccess = new SnippetCollectorDataAccess();
            return dataAccess.UpdateSnippet(json);
        }

        [WebMethod]
        public static string AddSnippetSource ( string json )
        {
            SnippetCollectorDataAccess dataAccess = new SnippetCollectorDataAccess();
            return dataAccess.AddSource( json );
        }

        [WebMethod]
        public static string UpdateSnippetSource ( string json )
        {
            SnippetCollectorDataAccess dataAccess = new SnippetCollectorDataAccess();
            return dataAccess.UpdateSource( json );
        }

        [WebMethod]
        public static string AddSnippetTag ( string json )
        {
            SnippetCollectorDataAccess dataAccess = new SnippetCollectorDataAccess();
            return dataAccess.AddTag( json );
        }

        [WebMethod]
        public static string UpdateSnippetTag ( string json )
        {
            SnippetCollectorDataAccess dataAccess = new SnippetCollectorDataAccess();
            return dataAccess.UpdateTag( json );
        }

        [WebMethod]
        public static string DeleteSnippet(string json)
        {
            SnippetCollectorDataAccess dataAccess = new SnippetCollectorDataAccess();
            return dataAccess.DeleteSnippet(json);
        }

        [WebMethod]
        public static string DeleteTag(string json)
        {
            SnippetCollectorDataAccess dataAccess = new SnippetCollectorDataAccess();
            return dataAccess.DeleteTag(json);
        }

        [WebMethod]
        public static string UndoTagDelete(string json)
        {
            SnippetCollectorDataAccess dataAccess = new SnippetCollectorDataAccess();
            return dataAccess.UndoTagDelete(json);
        }

        [WebMethod]
        public static string GetMakes()
        {
            SnippetCollectorDataAccess dataAccess = new SnippetCollectorDataAccess();
            var set = dataAccess.GetMakes();

            return JsonConvert.SerializeObject(set);
        }

        [WebMethod]
        public static string GetModels(string json)
        {
            SnippetCollectorDataAccess dataAccess = new SnippetCollectorDataAccess();
            return dataAccess.GetModels(json);
        }

        [WebMethod]
        public static string GetTrims(string json)
        {
            SnippetCollectorDataAccess dataAccess = new SnippetCollectorDataAccess();
            return dataAccess.GetTrims(json);
        }

        public void SetMultiSelectListBox(ListBox box, string csv)
        {
            if (string.IsNullOrEmpty(csv)) return;

            string[] values = csv.Split(new[] {','});

            foreach (string value in values)
            {
                foreach (ListItem li in box.Items)
                {
                    if (li.Value == value)
                    {
                        li.Selected = true;
                        break;
                    }
                }
            }

        }

    }
}
