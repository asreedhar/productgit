﻿<%@ Page Title="Manage Snippets | MAX Administration" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Wanamaker.WebApp.Admin.Snippets.Default" Theme="MAX_2.0" %>
<%@ Import Namespace="Wanamaker.WebApp.Helpers" %>
<%@ Register Src="~/Admin/Controls/MakeModelYearSelector.ascx" TagPrefix="max" TagName="MakeModelYearSelector" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/App_Themes/Max_2.0/Snippets.debug.css") %>" />
    
    <%-- jQuery snippet list template --%>
    <script id="SnippetsListTemplate" type="text/html">
        
        <div class="scrollTableHead">
            <table class="data">
                <colgroup>
                    <!-- Conditional comments support scrolling tbody -->
                    <!--[if gte IE 8]>
                        <col width="475" />
                    <![endif]-->
                    <!--[if lt IE 8]>
                        <col width="465" />
                    <![endif]-->
                    <!--[if !IE]>-->  
                        <col width="479" />
                    <!--<![endif]-->
                    
                    <!-- Conditional comments support scrolling tbody -->
                    <!--[if gte IE 8]>
                        <col width="160" />
                    <![endif]-->
                    <!--[if lt IE 8]>
                        <col width="119" />
                    <![endif]-->
                    <!--[if !IE]>-->
                        <col width="160" />
                    <!--<![endif]-->
                    
                    <col width="64" />
                </colgroup>
                <thead>
                    <tr>
                        <th class="first">Text</th>
                        <th>Source</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>

        <div class="scrollTableBody">
            <table class="data">
                <colgroup>
                    <!-- Conditional comments support scrolling tbody -->
                    <!--[if gte IE 8]>
                        <col width="475" />
                    <![endif]-->
                    <!--[if lt IE 8]>
                        <col width="465" />
                    <![endif]-->
                    <!--[if !IE]>-->  
                        <col width="479" />
                    <!--<![endif]-->
                    
                    <!-- Conditional comments support scrolling tbody -->
                    <!--[if gte IE 8]>
                        <col width="160" />
                    <![endif]-->
                    <!--[if lt IE 8]>
                        <col width="119" />
                    <![endif]-->
                    <!--[if !IE]>-->
                        <col width="158" />
                    <!--<![endif]-->
                    
                    <col width="64" />
                </colgroup>
                <tbody>
                
                {{each Snippets}}
                    <tr class="highlight">
                        <td class="first">
                            {{= MarketingText }}
                        </td>
                        <td>{{= SourceName }}</td>
                        <td>
                            <a href="AddSnippet.aspx?id={{= Id }}" class="edit" title="Edit"><span>Edit</span></a>
                            <a href="?id={{= Id }}" class="delete" title="Delete"><span>Delete</span></a>
                        </td>
                    </tr>
                {{/each}}

                </tbody>
            </table>
        </div>
    </script>
    
    <script id="EditSourceTemplate" type="text/x-jquery-tmpl">

        <div class="inlineEdit">
            <div class="shadow">
                <div class="shadowed">
                    <a href="#" class="close">x Close</a>
                    <div class="form">
                        <div class="formFields">
                            <label for="">Source Name</label>
                            <input id="Source{{= Id }}Text" class="text" type="text" value="{{= Name }}" />
                            <span class="checkbox">
                                <input id="Source{{= Id }}CopyCkb" type="checkbox" {{= Copyrighted ? "checked=\"checked\"" : "" }} />
                                <label for="Source{{= Id }}CopyCkb">Copyrighted</label>
                            </span>
                        </div>
                        <div class="formButtons">
                            <input value="Save" id="Source{{= Id }}Submit" class="button updateSource" type="submit">
                        </div>
                        <div class="error">
                            <span id="" style="color:Red;display:none;">Please enter the source name.</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </script>
    
    <script id="EditTagTemplate" type="text/x-jquery-tmpl">

        <div class="inlineEdit">
            <div class="shadow">
                <div class="shadowed">
                    <a href="#" class="close">x Close</a>
                    <div class="form">
                        <div class="formFields">
                            <label for="">Tag</label>
                            <input id="Tag{{= Id }}Text" class="text" type="text" value="{{= Name }}" />
                        </div>
                        <div class="formButtons">
                            <input value="Save" id="Tag{{= Id }}Submit" class="button updateTag" type="submit">
                        </div>
                        <div class="error">
                            <span id="" style="color:Red;display:none;">Please enter the tag text.</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="ManageSnippetsContent" class="pageContent">
    
        <div class="clearfix contentHeader">
            <h2><span>Manage Snippets</span></h2>
            <div class="contentHeaderButton">
                <a href="~/Admin/Snippets/AddSnippet.aspx" runat="server"><span>Add Snippet &raquo;</span></a>
            </div>
        </div>
        
        <div id="SnippetsList" class="clearfix panel">
            
            <%--<div id="SearchSnippets">
                <label for="<%= SnippetSearchTextbox.ClientID %>">Search snippets</label>
                <asp:TextBox ID="SnippetSearchTextbox" runat="server" CssClass="text" Enabled="false"></asp:TextBox>
                <asp:Button ID="SnippetSearchButton" runat="server" CssClass="button" Text="&gt;&gt;" ToolTip="Search snippets &gt;" OnClientClick="alert('Not yet implemented.');return false;" />
                <asp:Label ID="SearchStatusMessage" runat="server" Visible="false"></asp:Label>
            </div>--%>
            
            <div id="SearchSnippets" class="clearfix">
                <h3>Select Vehicle Make/Model/Trims</h3>
                <max:MakeModelYearSelector id="MakeModelYearSelector1" runat="server"></max:MakeModelYearSelector>
            </div>
            
            <div id="SnippetTableDiv">
            
                <%--<asp:Repeater ID="SnippetsRepeater" runat="server">
                
                    <ItemTemplate>
                        <tr class="highlight">
                            <td class="first">
                                <%# Eval("marketingText") %>
                            </td>
                            <td><%# Eval("sourceName") %></td>
                            <td>
                                <a href="AddSnippet.aspx?id=<%# Eval("marketingId") %>" class="edit" title="Edit"><span>Edit</span></a>
                                <a href="?id=<%# Eval("marketingId") %>" class="delete" title="Delete"><span>Delete</span></a>
                            </td>
                        </tr>
                    </ItemTemplate>
                
                    <AlternatingItemTemplate>
                        <tr>
                            <td class="first">
                                <%# Eval("marketingText") %>
                            </td>
                            <td><%# Eval("sourceName") %></td>
                            <td>
                                <a href="AddSnippet.aspx?id=<%# Eval("marketingId") %>" class="edit" title="Edit"><span>Edit</span></a>
                                <a href="?id=<%# Eval("marketingId") %>" class="delete" title="Delete"><span>Delete</span></a>
                            </td>
                        </tr>
                    </AlternatingItemTemplate>
                
                    <HeaderTemplate>
                        <div class="scrollTableHead">
                            <table class="data">
                                <colgroup>
                                    <!-- Conditional comments support scrolling tbody -->
                                    <!--[if gte IE 8]>
                                        <col width="458" />
                                    <![endif]-->
                                    <!--[if lt IE 8]>
                                        <col width="448" />
                                    <![endif]-->
                                    <!--[if !IE]>-->  
                                        <col width="458" />
                                    <!--<![endif]-->
                                    
                                    <!-- Conditional comments support scrolling tbody -->
                                    <!--[if gte IE 8]>
                                        <col width="160" />
                                    <![endif]-->
                                    <!--[if lt IE 8]>
                                        <col width="119" />
                                    <![endif]-->
                                    <!--[if !IE]>-->
                                        <col width="160" />
                                    <!--<![endif]-->
                                    
                                    <col width="64" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th class="first">Text</th>
                                        <th>Source</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                
                        <div class="scrollTableBody">
                            <table class="data">
                                <colgroup>
                                    <!-- Conditional comments support scrolling tbody -->
                                    <!--[if gte IE 8]>
                                        <col width="458" />
                                    <![endif]-->
                                    <!--[if lt IE 8]>
                                        <col width="448" />
                                    <![endif]-->
                                    <!--[if !IE]>-->  
                                        <col width="458" />
                                    <!--<![endif]-->
                                    
                                    <!-- Conditional comments support scrolling tbody -->
                                    <!--[if gte IE 8]>
                                        <col width="154" />
                                    <![endif]-->
                                    <!--[if lt IE 8]>
                                        <col width="119" />
                                    <![endif]-->
                                    <!--[if !IE]>-->
                                        <col width="158" />
                                    <!--<![endif]-->
                                    
                                    <col width="64" />
                                </colgroup>
                                <tbody>
                    </HeaderTemplate>
                    <FooterTemplate>
                                </tbody>
                            </table>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>--%>
                
                <div id="EmptySnippetsList" class="clearfix">
                
                    <p>Select Make, Models and Trims above to view associated marketing snippets.</p>
                
                </div>
            
            </div>
            
        </div>
        
        <div id="SnippetSources" class="clearfix panel secondary inlineAddContainer">
        
            <div class="clearfix panelHeader">
                <h3>Sources</h3>
                <a href="#" class="inlineAddLink">+ Add New &hellip;</a>
                
                <asp:Panel ID="AddSourcePanel" runat="server" DefaultButton="AddSourceButton">
                    <div class="inlineAdd" style="display:none;">
                        <a href="#" class="close">x Close</a>
                        <div class="form">
                            <div class="formFields">
                                <label for="<%= AddSourceTextbox.ClientID %>">Source Name</label>
                                <asp:TextBox ID="AddSourceTextbox" runat="server" CssClass="text" ValidationGroup="AddSourceValidationGroup"></asp:TextBox>
                                <asp:CheckBox ID="AddSourceCopyrightCkb" runat="server" CssClass="checkbox" Text="Copyrighted" TextAlign="Right" ValidationGroup="AddSourceValidationGroup" />
                            </div>
                            <div class="formButtons">
                                <asp:Button ID="AddSourceButton" runat="server" CssClass="button addSource" Text="+ Add" CausesValidation="true" ValidationGroup="AddSourceValidationGroup" />
                            </div>
                            <div class="error">
                                <asp:RequiredFieldValidator ID="AddSourceTextboxValidator" runat="server" ControlToValidate="AddSourceTextbox" ErrorMessage="Please enter the source name." Display="Dynamic" ValidationGroup="AddSourceValidationGroup"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                
            </div>
            
            <div id="SourcesTableDiv">
            
                <asp:Repeater ID="SourcesRepeater" runat="server" OnItemDataBound="SourcesTags_ItemDataBound">
                
                    <ItemTemplate>
                        <tr runat="server" id="TableRow">
                            <td class="first">
                                <asp:Label ID="CopyrightCheckmark" runat="server" CssClass="copyrighted" AlternateText="&copy;" ToolTip="&copy; Copyrighted" Visible='<%# bool.Parse(Eval("hasCopyright").ToString()) %>'></asp:Label>
                            </td>
                            <td><span class="sourceName"><%# Eval("sourceName") %></span></td>
                            <td>
                                <a href="?id=<%# Eval("id") %>" class="edit" title="Edit"><span>Edit</span></a>
                                <%--<a href="?id=<%# Eval("id") %>" class="delete" title="Delete"><span>Delete</span></a>--%>
                            </td>
                        </tr>
                    </ItemTemplate>
                
                    <HeaderTemplate>
                        <div class="scrollTableHead">
                            <table class="data">
                                <colgroup>
                                    <!--[if gte IE 8]>
                                        <col width="33" />
                                    <![endif]-->
                                    <!--[if lt IE 8]>
                                        <col width="10" />
                                    <![endif]-->
                                    <!--[if !IE]>-->  
                                        <col width="33" />
                                    <!--<![endif]-->
                                    
                                    <!--[if gte IE 8]>
                                        <col width="169" />
                                    <![endif]-->
                                    <!--[if lt IE 8]>
                                        <col width="150" />
                                    <![endif]-->
                                    <!--[if !IE]>-->
                                    <col width="169" />
                                    <!--<![endif]-->
                                    
                                    <col width="50" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th class="first">&copy;</th>
                                        <th>Name</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                
                        <div class="scrollTableBody">
                            <table class="data" id="SourcesDataTable">
                                <colgroup>
                                    <!--[if gte IE 8]>
                                        <col width="35" />
                                    <![endif]-->
                                    <!--[if lt IE 8]>
                                        <col width="10" />
                                    <![endif]-->
                                    <!--[if !IE]>-->  
                                        <col width="35" />
                                    <!--<![endif]-->
                                    
                                    <!--[if gte IE 8]>
                                        <col width="169" />
                                    <![endif]-->
                                    <!--[if lt IE 8]>
                                        <col width="150" />
                                    <![endif]-->
                                    <!--[if !IE]>-->  
                                    <col width="169" />
                                    <!--<![endif]-->
                                    
                                    <col width="40" />
                                </colgroup>
                                <tbody>
                    </HeaderTemplate>
                
                    <FooterTemplate>
                                </tbody>
                            </table>
                        </div>
                    </FooterTemplate>
                
                </asp:Repeater>
            
            </div>
        
        </div>
        
        <div id="SnippetTags" class="clearfix panel secondary inlineAddContainer">
        
            <div class="clearfix panelHeader">
                <h3>Tags</h3>
                <a href="#" class="inlineAddLink">+ Add New &hellip;</a>
                
                <asp:Panel ID="Panel1" runat="server" DefaultButton="AddTagButton">
                    <div class="inlineAdd" style="display:none;">
                        <a href="#" class="close">x Close</a>
                        <div class="form">
                            <div class="formFields">
                                <label for="<%= AddTagTextbox.ClientID %>">Tag</label>
                                <asp:TextBox ID="AddTagTextbox" runat="server" CssClass="text" MaxLength="30" ValidationGroup="AddTagValidationGroup"></asp:TextBox>
                            </div>
                            <div class="formButtons">
                                <asp:Button ID="AddTagButton" runat="server" CssClass="button addTag" Text="+ Add" ValidationGroup="AddTagValidationGroup" />
                            </div>
                            <div class="error">
                                <asp:RequiredFieldValidator ID="AddTagTextboxValidator" runat="server" ControlToValidate="AddTagTextbox" ErrorMessage="Please enter the tag text." Display="Dynamic" ValidationGroup="AddTagValidationGroup"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                
            </div>
            
            <div id="TagsTableDiv">
            
                <asp:Repeater ID="TagsRepeater" runat="server" OnItemDataBound="SourcesTags_ItemDataBound">
                
                    <ItemTemplate>
                        <tr runat="server" id="TableRow">
                            <td class="first"><span class="tagText"><%# Eval("description") %></span></td>
                            <td>
                                <a href="?id=<%# Eval("id") %>" class="edit" title="Edit"><span>Edit</span></a>
                                <a href="?id=<%# Eval("id") %>" name="<%# Eval("description") %>" class="delete" title="Delete"><span>Delete</span></a>
                            </td>
                        </tr>
                    </ItemTemplate>
                    
                    <HeaderTemplate>
                        <div class="scrollTableHead">
                            <table class="data">
                                <colgroup>
                                    <!--[if gte IE 8]>
                                        <col width="202" />
                                    <![endif]-->
                                    <!--[if lt IE 8]>
                                        <col width="176" />
                                    <![endif]-->
                                    <!--[if !IE]>-->  
                                        <col width="202" />
                                    <!--<![endif]-->
                                    
                                    <col width="50" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th class="first">Tag</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    
                        <div class="scrollTableBody">
                            <table class="data" id="TagsDataTable">
                                <colgroup>
                                    <!--[if gte IE 8]>
                                        <col width="205" />
                                    <![endif]-->
                                    <!--[if lt IE 8]>
                                        <col width="176" />
                                    <![endif]-->
                                    <!--[if !IE]>-->  
                                        <col width="205" />
                                    <!--<![endif]-->
                                    <col width="50" />
                                </colgroup>
                                <tbody>
                    </HeaderTemplate>
                    
                    <FooterTemplate>
                                </tbody>
                            </table>
                        </div>
                    </FooterTemplate>
                    
                </asp:Repeater>
            </div>
        
        </div>
    
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">

    <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Lib/jquery.datalink.js") %>"></script>
    <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Lib/jquery.tmpl.js") %>"></script>
    <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Admin/SnippetCollector/SnippetCollector.debug.js") %>"></script>
    
</asp:Content>
