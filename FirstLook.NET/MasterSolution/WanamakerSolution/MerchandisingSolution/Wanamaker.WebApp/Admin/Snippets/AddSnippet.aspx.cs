﻿using System;
using FirstLook.Merchandising.DomainModel.SnippetCollector;

namespace Wanamaker.WebApp.Admin.Snippets
{
    public partial class AddSnippet : SnippetBasePage
    {
        public enum PageModes { Insert, Update }
        public PageModes PageMode { get; set; }

        protected void Page_Init( object sender, EventArgs e )
        {
            // If id passed, then page is in edit mode
            PageMode = !string.IsNullOrEmpty( Request.QueryString[ "id" ] ) ? PageModes.Update : PageModes.Insert;
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            // Setup page
            if ( PageMode == PageModes.Update )
            {
                AddSnippetLink.Visible = true;
                PageTitleH2.InnerHtml = @"<span>Edit Snippet</span>";
                Page.Title = @"Edit Snippet | MAX Administration";
                AddSnippetButton.Text = @"Save Snippet";
            }

            BindData();
        }

        private void BindData()
        {

            SnippetTagsSelectBox.DataSource = DataAccess.GetAllTags();
            SnippetTagsSelectBox.DataTextField = "description";
            SnippetTagsSelectBox.DataValueField = "id";
            SnippetTagsSelectBox.DataBind();

            SnippetSourcesDDL.DataSource = DataAccess.GetAllSources();
            SnippetSourcesDDL.DataTextField = "sourceName";
            SnippetSourcesDDL.DataValueField = "id";
            SnippetSourcesDDL.DataBind();

            if ( PageMode == PageModes.Update )
            {
                BindSnippetData( int.Parse( Request.QueryString[ "id" ] ) );
            }

        }

        private void BindSnippetData(int snippetId)
        {
            // Get snippet
            SnippetCollectorDataAccess dataAccess = new SnippetCollectorDataAccess();
            var snippetByID = dataAccess.GetSnippetById(snippetId);

            // Set snippet text, verbatim, sources and tags
            SnippetTextTexbox.Text = snippetByID.MarketingText;
            SnippetSourcesDDL.SelectedValue = snippetByID.Source.ToString();
            SnippetIsVerbatimCkb.Checked = snippetByID.IsVerbatim;
            SetMultiSelectListBox(SnippetTagsSelectBox, snippetByID.Tags);

        }
    }
}
