﻿using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using FirstLook.Merchandising.DomainModel.SnippetCollector;

namespace Wanamaker.WebApp.Admin.Snippets
{
    public partial class Default : SnippetBasePage
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            SnippetCollectorDataAccess dataAccess = new SnippetCollectorDataAccess();

            //SnippetsRepeater.DataSource = dataAccess.GetAllSnippets();
            //SnippetsRepeater.DataBind();

            SourcesRepeater.DataSource = dataAccess.GetAllSources();
            SourcesRepeater.DataBind();

            TagsRepeater.DataSource = dataAccess.GetAllTags();
            TagsRepeater.DataBind();
        }

        protected void SourcesTags_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            // Add 'first' class to first row
            if (e.Item.ItemIndex == 0)
            {
                ((HtmlControl)e.Item.FindControl("TableRow")).Attributes.Add("class", "first");
            }
        }
    }
}
