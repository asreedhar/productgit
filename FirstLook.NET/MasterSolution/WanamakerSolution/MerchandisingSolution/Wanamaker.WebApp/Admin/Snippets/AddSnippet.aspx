﻿<%@ Page Title="Add Snippet | MAX Administration" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="AddSnippet.aspx.cs" Inherits="Wanamaker.WebApp.Admin.Snippets.AddSnippet" Theme="MAX_2.0" %>
<%@ Import Namespace="Wanamaker.WebApp.Helpers" %>
<%@ Register Src="~/Admin/Controls/MakeModelYearSelector.ascx" TagPrefix="max" TagName="MakeModelYearSelector" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/App_Themes/Max_2.0/Snippets.debug.css") %>" />

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="AddSnippetContent" class="pageContent">
    
        <div class="contentHeader clearfix">
            <h2 id="PageTitleH2" runat="server"><span>Add Snippet</span></h2>
            <a href="AddSnippet.aspx" id="AddSnippetLink" runat="server" visible="false" clientidmode="Static">Add Snippet</a>
            <div class="contentHeaderButton">
                <a href="~/Admin/Snippets/Default.aspx" runat="server"><span>View Snippets &raquo;</span></a>
            </div>
        </div>
        
        <div id="UserMessaging">
            <input type="hidden" id="UserMessageHiddenField" runat="server" />
            <asp:ValidationSummary ID="AddSnippetValidationSummry" runat="server" CssClass="error" DisplayMode="BulletList" HeaderText="Sorry! We were unable to add this snippet." ShowSummary="true" ValidationGroup="AddSnippetValidationGroup" />
        </div>
        
        <div id="SnippetForm" class="clearfix panel">
        
            <div id="SnippetFields" class="clearfix">
            
                <div id="SnippetTextFields">
                    <label for="<%= SnippetTextTexbox.ClientID %>" title="Required field">Text <span class="required">*</span></label>
                    <asp:TextBox ID="SnippetTextTexbox" runat="server" CssClass="text" MaxLength="500" TextMode="MultiLine"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="SnippetTextRequiredValidator" runat="server" ControlToValidate="SnippetTextTexbox" Display="None" ErrorMessage="Please enter the snippet text" ValidationGroup="AddSnippetValidationGroup"></asp:RequiredFieldValidator>
                </div>
                
                <div id="SnippetDetailsFields">
                    
                    <div id="SnippetSourceFields" class="inlineAddContainer">
                        <label for="<%= SnippetSourcesDDL.ClientID %>" title="Required field">Source <span class="required">*</span></label><br />
                        <asp:DropDownList ID="SnippetSourcesDDL" runat="server" AppendDataBoundItems="true">
                            <asp:ListItem Value="">-- Select Source --</asp:ListItem>
                        </asp:DropDownList>
                        <a href="#" id="AddSourceLink" class="inlineAddLink">+ Add New &hellip;</a>
                        <asp:RequiredFieldValidator ID="SnippetSourcesDDLRequiredValidator" runat="server" ControlToValidate="SnippetSourcesDDL" Display="None" ErrorMessage="Please select a snippet source" ValidationGroup="AddSnippetValidationGroup"></asp:RequiredFieldValidator>
                
                        <asp:Panel ID="AddSourcePanel" runat="server" DefaultButton="AddSourceButton">
                            <div class="inlineAdd" style="display:none;">
                                <a href="#" class="close">x Close</a>
                                <div class="form">
                                    <div class="formFields">
                                        <label for="<%= AddSourceTextbox.ClientID %>">Source Name</label>
                                        <asp:TextBox ID="AddSourceTextbox" runat="server" CssClass="text" ValidationGroup="AddSourceValidationGroup"></asp:TextBox>
                                        <asp:CheckBox ID="AddSourceCopyrightCkb" runat="server" CssClass="checkbox" Text="Copyrighted" TextAlign="Right" ValidationGroup="AddSourceValidationGroup" />
                                    </div>
                                    <div class="formButtons">
                                        <asp:Button ID="AddSourceButton" runat="server" CssClass="button addSource" Text="+ Add" ValidationGroup="AddSourceValidationGroup" />
                                    </div>
                                    <div class="error">
                                        <asp:RequiredFieldValidator ID="AddSourceTextboxValidator" runat="server" ControlToValidate="AddSourceTextbox" ErrorMessage="Please enter the source name." Display="Dynamic" ValidationGroup="AddSourceValidationGroup"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        
                    </div>
                    
                    <div id="VerbatimFields">
                        <asp:CheckBox ID="SnippetIsVerbatimCkb" runat="server" Checked="true" CssClass="checkbox" Text="Is Verbatim" TextAlign="Right" />
                    </div>
                    <%--
                    <div id="SnippetRatingFields">
                        <label>Rating</label>
                        <asp:RadioButtonList ID="SnippetRatingRadioList" runat="server" CssClass="radioList" RepeatColumns="5" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem>5</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    --%>
                </div>
                    
                <div id="SnippetTagsFields" class="inlineAddContainer">
                    <label>Tags</label>
                    <a href="#" id="AddTagLink" class="inlineAddLink">+ Add New &hellip;</a>
                    
                    <asp:Panel ID="AddTagPanel" runat="server" DefaultButton="AddTagButton">
                        <div class="inlineAdd" style="display:none;">
                            <a href="#" class="close">x Close</a>
                            <div class="form">
                                <div class="formFields">
                                    <label for="<%= AddTagTextbox.ClientID %>">Tag</label>
                                    <asp:TextBox ID="AddTagTextbox" runat="server" CssClass="text" ValidationGroup="AddTagValidationGroup"></asp:TextBox>
                                </div>
                                <div class="formButtons">
                                    <asp:Button ID="AddTagButton" runat="server" CssClass="button addTag" Text="+ Add" ValidationGroup="AddTagValidationGroup" />
                                </div>
                                <div class="error">
                                    <asp:RequiredFieldValidator ID="AddTagTextboxValidator" runat="server" ControlToValidate="AddTagTextbox" ErrorMessage="Please enter the tag text." Display="Dynamic" ValidationGroup="AddTagValidationGroup"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    
                    <asp:ListBox ID="SnippetTagsSelectBox" runat="server" SelectionMode="Multiple"></asp:ListBox>
                </div>
            
                <span class="helper"><span class="required">*</span> Required fields</span>

            </div>
        
            <%--<div id="ChildSnippetFields" class="clearfix">
            
                <div id="ChildSnippetTextFields">
                    <label for="<%= ChildSnippetTextTexbox.ClientID %>">Child Snippet Text</label>
                    <asp:TextBox ID="ChildSnippetTextTexbox" runat="server" TextMode="MultiLine" CssClass="text"></asp:TextBox>
                </div>
                
                <div id="ChildSnippetDetailsFields">
                    
                    <div id="ChildVerbatimFields">
                        <asp:CheckBox ID="ChildSnippetIsVerbatimCkb" runat="server" CssClass="checkbox" Text="Is Verbatim" TextAlign="Right" />
                    </div>
                    
                    <div id="ChildSnippetRatingFields">
                        <label>Rating</label>
                        <asp:RadioButtonList ID="ChildSnippetRatingRadioList" runat="server" CssClass="radioList" RepeatColumns="5" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem>5</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    
                </div>
                    
                <div id="ChildSnippetTagsFields" class="inlineAddContainer">
                    <label>Tags</label>
                    <a href="#" id="A1" class="inlineAddLink">+ Add New &hellip;</a>
                    <asp:ListBox ID="ChildSnippetTagsSelectBox" runat="server"></asp:ListBox>
                    
                    <div class="inlineAdd" style="display:none;">
                        <a href="#" class="close">x Close</a>
                        <div class="form">
                            <div class="formFields">
                                <label for="<%= AddChildTagTextbox.ClientID %>">Tag</label>
                                <asp:TextBox ID="AddChildTagTextbox" runat="server" CssClass="text" ValidationGroup="AddChildTagValidationGroup"></asp:TextBox>
                            </div>
                            <div class="formButtons">
                                <asp:Button ID="AddChildTagButton" runat="server" CssClass="button addTag" Text="+ Add" ValidationGroup="AddChildTagValidationGroup" />
                            </div>
                            <div class="error">
                                <asp:RequiredFieldValidator ID="AddChildTagTextboxValidator" runat="server" ControlToValidate="AddChildTagTextbox" ErrorMessage="Please enter the tag text." Display="Dynamic" ValidationGroup="AddChildTagValidationGroup"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                    
                </div>
            
            </div>--%>
        
        </div>
        
        <div id="SnippetVehicleForm" class="clearfix panel">
            
            <h3>Link to Vehicle(s)</h3>
            
            <max:MakeModelYearSelector id="MakeModelYearSelector1" runat="server"></max:MakeModelYearSelector>
            
        </div>
        
        <div id="AddSnippetPageButtons">
            <asp:Button ID="AddSnippetButton" runat="server" Text="Add Snippet" CssClass="pageButton" CausesValidation="true" Enabled="false" ValidationGroup="AddSnippetValidationGroup" />
            <a href="#" class="clearForm">Reset Form</a>
        </div>
    
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">

    <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Admin/SnippetCollector/SnippetCollector.debug.js") %>"></script>
    
</asp:Content>
