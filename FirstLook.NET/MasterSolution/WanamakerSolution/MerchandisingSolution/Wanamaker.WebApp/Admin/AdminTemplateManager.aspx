<%@ Page Language="C#" Theme="MAX_2.0" MasterPageFile="~/Admin/Admin.Master" Title="Template Manager | MAX : Online Inventory. Perfected." AutoEventWireup="True" CodeBehind="AdminTemplateManager.aspx.cs" Inherits="Wanamaker.WebApp.Admin.AdminTemplateManager" ClientIDMode="AutoID" %>
<%@ Import Namespace="Wanamaker.WebApp.Helpers" %>

<%@ Register TagPrefix="carbuilder" TagName="EditSnippetIntro" Src="~/CustomizeMAX/Controls/EditSnippetIntro.ascx" %>
<%@ Register TagPrefix="carbuilder" TagName="SnippetCreator" Src="~/CustomizeMAX/Controls/SnippetCreator.ascx" %>
<%@ Register TagPrefix="wanaworkflow" TagName="WorkflowHeader" Src="~/Workflow/Controls/WorkflowHeader.ascx" %>
<%@ Register TagPrefix="max" TagName="AdminNavigation" Src="~/Admin/Controls/NavigationMenu.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/App_Themes/MAX60/MAX.Admin.debug.css") %>" />
    <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/App_Themes/LazyBoy/admintemplatemanager.css") %>" />

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager ID="ScriptManager2" runat="server" LoadScriptsBeforeUI="true"></asp:ScriptManager>

    <div id="Div1" class="mainTemplateBox" runat="server">
        <div class="AvailTemplatesForBuilding">
            <div class="bodyTemplates">
                <span>Showing Dealer:</span>
                <asp:DropDownList ID="showingDealerDDL" runat="server" DataSourceID="InstalledDealerDS" DataTextField="businessUnit" DataValueField="businessUnitID" 
                                    AppendDataBoundItems="true" AutoPostBack="True" OnSelectedIndexChanged="DealerChanged">
                    <asp:ListItem Text="FIRSTLOOK SYSTEM" Value="100150"></asp:ListItem>
                </asp:DropDownList>
                <br/><br/>
                <span>Templates:</span>
                <asp:DropDownList ID="activeTemplatesDDL" runat="server" DataTextField="Value" DataValueField="Key" 
                                    OnDataBound="TemplateDDL1_Bound" OnDataBinding="DataBinding" OnSelectedIndexChanged="ActiveTemplateChanged" AutoPostBack="True">
                </asp:DropDownList>
                <br/><br/>
            </div>
        </div>
        <div class="templateContent">
            <div class="toolkitExt">
                <h4>
                    Add Snippets to Template</h4>
                <div class="toolkit">
                    <asp:Repeater runat="server" DataSourceID="BlurbXML">

                        <HeaderTemplate><ul></HeaderTemplate>

                        <ItemTemplate>
                            <li><h2><%# Eval("blurbTypeName") %></h2>
                                
                                <asp:Repeater runat="server" DataSource='<%# XPathSelect("./BlurbTemplate") %>' >

                                    <HeaderTemplate><ul></HeaderTemplate>

                                    <ItemTemplate>
                                        <li style='<%# IsBlurbVisible(XPathSelect(".")) ? "" : "display: none" %>'>
                                        <asp:LinkButton runat="server" CommandArgument='<%# XPath("./@blurbId") %>' OnClick="Snippet_Clicked">
                                            <%# XPath("./@blurbName") %>
                                        </asp:LinkButton>
                                        </li>
                                    </ItemTemplate>

                                    <FooterTemplate></ul></FooterTemplate>

                                </asp:Repeater>

                            </li>
                        </ItemTemplate>
                       
                        <FooterTemplate></ul></FooterTemplate>

                     </asp:Repeater>
                </div>
                <asp:Button ID="NewSnippet" runat="server" Text="Create New Snippet"></asp:Button>
                <asp:Panel ID="SnipCreator" runat="server" CssClass="SnipCreator" Style="display: none">
                    <asp:ImageButton CssClass="righty" ID="cancelCreate" runat="server" ImageUrl="../App_Themes/LazyBoy/images/closeX.gif" />
                    <p>
                        To create a new snippet, enter a title, select the proper category, specify when the snippet should be used, and add items
                        to the template body.</p>
                    <asp:UpdatePanel ID="Update3" runat="server">
                        <ContentTemplate>
                            <carbuilder:SnippetCreator ID="snip" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                <ajax:modalpopupextender id="ModalPopupExtender4" runat="server" popupcontrolid="SnipCreator" targetcontrolid="NewSnippet"
                    cancelcontrolid="cancelCreate" backgroundcssclass="modal">
    </ajax:modalpopupextender>
            </div>
            <asp:UpdatePanel ID="ReorderItemsPanel" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <div class="templateForm">
                        <asp:CheckBox CssClass="righty" ID="ActiveCB" runat="server" Text="Active?" />
                        <ul class="clearfix templateHead">
                            <li><span>Template Name:</span>
                                <asp:TextBox ID="TemplateName" runat="server" Text="" MaxLength="50"></asp:TextBox>
                            </li>
                            <li><span>Vehicle Profile:</span>
                                <asp:DropDownList ID="VehicleProfileDDL" runat="server" OnDataBound="ProfileDDL_Bound" DataSourceID="VehicleProfileDS" DataTextField="title"
                                    DataValueField="vehicleProfileId">
                                </asp:DropDownList>
                            </li>
                        </ul>
                        <div class="reorderableCategories">
                            <ajax:reorderlist id="ReorderList1" runat="server" postbackonreorder="false" datasourceid="ReorderTemplateDS" callbackcssstyle="callbackStyle"
                                draghandlealignment="Left" iteminsertlocation="Beginning" datakeyfield="BlurbID" sortorderfield="Sequence" ondatabound="ReorderList_Bound">
                        <EmptyListTemplate>
                        <h3>Please select a template to edit...</h3>
                        </EmptyListTemplate>
                        <ItemTemplate>
                            <div class="itemArea">
                                <asp:ImageButton ID="removeIt" runat="server" CommandArgument='<%# Eval("BlurbID") %>' ImageUrl="../App_Themes/LazyBoy/images/closeX.gif" OnClick="RemoveTemplateItem" />
                                <b><%# Eval("BlurbName") %></b>
                                <asp:LinkButton ID="editLB" runat="server" Text="edit..."></asp:LinkButton>
                               
                                <asp:Panel ID="EditPanel" runat="server" CssClass="DynamicEditor" style="display:none">
                                        <asp:ImageButton ID="closeButton" runat="server" ImageUrl="../App_Themes/LazyBoy/images/closeX.gif" />
                                        <carbuilder:EditSnippetIntro ID="editTag" runat="server" BlurbId='<%# Eval("BlurbID") %>' Title='<%# "Customize Text for " + Eval("BlurbName") %>' />                                                       
                                </asp:Panel>
                                <ajax:ModalPopupExtender ID="ModalPopupExtender3" runat="server" 
                                    PopupControlID="EditPanel"
                                    TargetControlID="editLB" 
                                    CancelControlID="closeButton" 
                                    BackgroundCssClass="modal">
                                </ajax:ModalPopupExtender>    
                                                     
                            </div>
                        </ItemTemplate>
                         <ReorderTemplate>
                            <asp:Panel ID="Panel3" runat="server" CssClass="reorderCue" />
                        </ReorderTemplate>
                        <DragHandleTemplate>
                            <div class="dragHandle"></div>
                        </DragHandleTemplate>
                        
        </ajax:reorderlist>
                        </div>
                        <div>
                            <a style="clear: left; float: right; color: #f8f8f8" href="#" onclick="document.getElementById('importance').style.display='block'">
                                advanced (edit importance)...</a>
                        </div>
                        <div id="importance" style="display: none; border-top: solid 1px #eee;" class="reorderableCategories">
                            <h2>
                                Importance:</h2>
                            <p>
                                This determines which pieces stay in the ad if we run out of space</p>
                            <ajax:reorderlist id="ReorderList2" runat="server" postbackonreorder="false" datasourceid="ReorderPriorityDS" callbackcssstyle="callbackStyle"
                                draghandlealignment="Left" iteminsertlocation="Beginning" datakeyfield="BlurbID" sortorderfield="Priority">
                        
                        <ItemTemplate>
                            <div class="itemArea">
                                <%# Eval("BlurbName") %>
                            </div>
                        </ItemTemplate>
                         <ReorderTemplate>
                            <asp:Panel ID="Panel2" runat="server" CssClass="reorderCue" />
                        </ReorderTemplate>
                        <DragHandleTemplate>
                            <div class="dragHandle"></div>
                        </DragHandleTemplate>
                        
        </ajax:reorderlist>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div>
                <ul class="butList">
                    <li>
                        <asp:Button ID="update" runat="server" Text="Save" OnClick="UpdateTemplate_Click" /></li>
                    <li>
                        <asp:Button ID="but1" runat="server" Text="Save As..." OnClientClick="document.getElementById('NewTemplateCopyNameTB').value = document.getElementById('TemplateName').value;return true;" />
                    </li>
                    <li>
                        <asp:Button ID="CreateNewTemplate" runat="server" Text="New" />
                    </li>
                </ul>
                <asp:Panel ID="CopyForm" runat="server" CssClass="modalBod" Style="display: none">
                    New Template's Nickname:
                    <asp:TextBox ID="NewTemplateCopyNameTB" runat="server"></asp:TextBox><br />
                    Select target dealership:
                    <asp:DropDownList ID="copyToDealerDDL" runat="server" DataSourceID="InstalledDealerDS" DataTextField="businessUnit" DataValueField="businessUnitID"
                        AppendDataBoundItems="true">
                        <asp:ListItem Text="FIRSTLOOK SYSTEM" Value="100150"></asp:ListItem>
                    </asp:DropDownList>
                    <br />
                    <asp:Button ID="doCopyBtn" runat="server" Text="save" OnClick="CopyTemplate_Click" />
                    <asp:Button ID="cancelCopyBtn" runat="server" Text="cancel" />
                </asp:Panel>
                <ajax:modalpopupextender id="ModalPopupExtender1" runat="server" popupcontrolid="CopyForm" targetcontrolid="but1" cancelcontrolid="cancelCopyBtn"
                    backgroundcssclass="modal">
    </ajax:modalpopupextender>
                <asp:Panel ID="NewForm" runat="server" CssClass="modalBod" Style="display: none">
                    New Template's Nickname:
                    <asp:TextBox ID="NewNameTB" runat="server" Text=""></asp:TextBox>
                    <asp:Button ID="Button1" runat="server" Text="create" OnClick="CreateNew_Click" />
                    <asp:Button ID="cancelCreateBtn" runat="server" Text="cancel" />
                </asp:Panel>
                <ajax:modalpopupextender id="ModalPopupExtender2" runat="server" popupcontrolid="NewForm" targetcontrolid="CreateNewTemplate"
                    cancelcontrolid="cancelCreateBtn" backgroundcssclass="modal">
    </ajax:modalpopupextender>
            </div>
            <asp:HiddenField ID="SelectedTemplateId" runat="server" Value="" />
            <div style="clear: left">
            </div>
        </div>
        <%-- Begin: this section was commented out but broke stuff in the code-behind --%>
        <div style="float: right; padding-top: 20px;">
            Example Vehicle:
            <asp:DropDownList ID="SelectedVehicle" runat="server" DataSourceID="InventoryDS" DataTextField="IdentifierText" DataValueField="InventoryId">
            </asp:DropDownList>
            <asp:CheckBox ID="Long" runat="server" Text="Go Long" />
            <asp:Button ID="refreshVehicle" runat="server" Text="Preview" OnClick="NewVehicle_Click" />
        </div>
        <div class="sampleDescription">
            <asp:ObjectDataSource ID="InventoryDS" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.Inventory.InventoryDataSource" SelectMethod="FetchList" OnSelecting="Inventory_Selecting">
                <SelectParameters>
                    <asp:Parameter Name="businessUnitId" Type="Int32" />
                    <asp:Parameter Name="usedNewFilter" Type="Int32" DefaultValue="2" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:Label ID="descriptionLB" runat="server"></asp:Label>
        </div>
        <%-- End: this section was commented out but broke stuff in the code-behind --%>
    </div>
    <asp:XmlDataSource ID="BlurbXML" runat="server" OnLoad="BlurbXml_Load" EnableCaching="false" XPath="ToolBox/Category"></asp:XmlDataSource>
    <asp:ObjectDataSource runat="server" ID="TemplateDS" TypeName="FirstLook.Merchandising.DomainModel.Templating.BlurbDataSource"
        SelectMethod="AllTemplatesSelect">
        <SelectParameters>
            <asp:ControlParameter ControlID="showingDealerDDL" Name="businessUnitId" Type="Int32" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ReorderTemplateDS" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Templating.BlurbDataSource"
        SelectMethod="TemplateItemsSelect" UpdateMethod="UpdateSequence" OnUpdating="TemplateContents_Updating">
        <SelectParameters>
            <asp:ControlParameter Name="templateID" ControlID="SelectedTemplateId" PropertyName="Value" />
            <asp:ControlParameter ControlID="showingDealerDDL" Name="businessUnitId" Type="Int32" PropertyName="SelectedValue" />
            <asp:Parameter Name="orderByPriority" Type="Boolean" DefaultValue="false" />
        </SelectParameters>
        <UpdateParameters>
            <asp:ControlParameter Name="templateID" ControlID="SelectedTemplateId" PropertyName="Value" />
            <asp:Parameter Name="blurbId" Type="Int32" />
            <asp:Parameter Name="sequence" Type="Int32" />
            <asp:Parameter Name="priority" Type="Int32" />
            <asp:Parameter Name="businessUnitId" Type="Int32" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="templateContentsDS" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Templating.BlurbDataSource"
        SelectMethod="TemplateSelect">
        <SelectParameters>
            <asp:ControlParameter Name="templateID" ControlID="SelectedTemplateId" PropertyName="Value" />
            <asp:ControlParameter ControlID="showingDealerDDL" Name="businessUnitId" Type="Int32" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ReorderPriorityDS" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Templating.BlurbDataSource"
        SelectMethod="TemplateItemsSelect" UpdateMethod="UpdateSequence" OnUpdating="TemplateContents_Updating">
        <SelectParameters>
            <asp:ControlParameter Name="templateID" ControlID="SelectedTemplateId" PropertyName="Value" />
            <asp:ControlParameter ControlID="showingDealerDDL" Name="businessUnitId" Type="Int32" PropertyName="SelectedValue" />
            <asp:Parameter Name="orderByPriority" Type="Boolean" DefaultValue="true" />
        </SelectParameters>
        <UpdateParameters>
            <asp:ControlParameter Name="templateID" ControlID="SelectedTemplateId" PropertyName="Value" />
            <asp:Parameter Name="blurbId" Type="Int32" />
            <asp:Parameter Name="sequence" Type="Int32" />
            <asp:Parameter Name="priority" Type="Int32" />
            <asp:Parameter Name="businessUnitId" Type="Int32" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    <asp:SqlDataSource ID="InstalledDealerDS" runat="server" ConnectionString='<%$ ConnectionStrings:Merchandising %>' SelectCommandType="Text"
        SelectCommand="Select bu.businessUnit, bu.businessUnitId FROM settings.Merchandising mr JOIN Interface.BusinessUnit bu ON bu.businessUnitId = mr.businessUnitId order by businessUnit ASC">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="VehicleProfileDS" runat="server" ConnectionString='<%$ ConnectionStrings:Merchandising %>' SelectCommand="Select vehicleProfileId, [title] FROM templates.vehicleProfiles WHERE businessUnitId = @BusinessUnitId OR businessUnitId = 100150">
        <SelectParameters>
            <asp:ControlParameter ControlID="showingDealerDDL" Name="businessUnitId" Type="Int32" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>
    <owc:HedgehogTracking ID="hh" runat="server" Bootstrap="false" EnhanceAll="true" />

</asp:Content>

<asp:Content ID="ScriptContent" ContentPlaceHolderID="scripts" runat="server">
    <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/MAX.Main.js") %>"></script>
</asp:Content>
