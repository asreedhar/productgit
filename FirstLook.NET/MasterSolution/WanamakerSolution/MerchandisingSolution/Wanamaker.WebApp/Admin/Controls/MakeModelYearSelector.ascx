﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MakeModelYearSelector.ascx.cs" Inherits="Wanamaker.WebApp.Admin.Controls.MakeModelYearSelector" %>

<div id="VehicleMakeFields" class="clearfix">
    <label for="<%= VehicleMakesSelectBox.ClientID %>">Make(s)<br /><span class="nobold">(select one)</span></label>
    <asp:ListBox ID="VehicleMakesSelectBox" runat="server"></asp:ListBox>
</div>

<div id="VehicleYearModelFields" class="clearfix">
    <label for="<%= VehicleModelsSelectBox.ClientID %>">Year/Model(s)</label>
    <asp:ListBox ID="VehicleModelsSelectBox" runat="server" SelectionMode="Multiple" Enabled="false"></asp:ListBox>
</div>

<div id="VehicleStyleTrimFields" class="clearfix">
    <label for="<%= VehicleTrimsSelectBox.ClientID %>" title="Required field">Style/Trim(s) <span class="required">*</span></label>
    <asp:ListBox ID="VehicleTrimsSelectBox" runat="server" SelectionMode="Multiple" Enabled="false"></asp:ListBox>
    <asp:RequiredFieldValidator ID="VehicleTrimsSelectBoxRequiredValidator" runat="server" ControlToValidate="VehicleTrimsSelectBox" Display="None" ErrorMessage="Please select the Style/Trim(s) associated with this snippet." ValidationGroup="AddSnippetValidationGroup"></asp:RequiredFieldValidator>
</div>