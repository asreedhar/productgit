<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="TemplateSelector.ascx.cs" Inherits="Wanamaker.WebApp.Admin.Controls.TemplateSelector" %>
<%@ Register TagPrefix="carbuilder" TagName="BusinessUnitUserControl" Src="~/Controls/BusinessUnitUserControl.ascx"  %>

<asp:DropDownList runat="server" ID="TemplateDDL" 
                    DataTextField="Value" DataValueField="Key"
                    DataSourceID="TemplateDS" OnDataBound="TemplateDDL_Bound"></asp:DropDownList>
<asp:ObjectDataSource runat="server"
    ID="TemplateDS"
    TypeName="FirstLook.Merchandising.DomainModel.Templating.BlurbDataSource"
    SelectMethod="AvailableTemplatesSelect"
    OnSelecting="Templates_Selecting">
    <SelectParameters>
        <asp:Parameter Name="businessUnitId" Type="Int32" />
    </SelectParameters>    
</asp:ObjectDataSource>