<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="VehicleSetCreator.ascx.cs" Inherits="Wanamaker.WebApp.Admin.Controls.VehicleSetCreator" %>
<%@ Register TagPrefix="carbuilder" TagName="TemplateSelector" Src="~/Admin/Controls/TemplateSelector.ascx"  %>
<%@ Register TagPrefix="carbuilder" TagName="ThemeSelector" Src="~/Admin/Controls/ThemeSelector.ascx"  %>
  
<div id="VehicleSet">
   
    <div class="selectors">    
        <div class="section">
        <h4>1. Choose Vehicle Set</h4>
        
        <asp:ObjectDataSource ID="GroupMakeDataSource" runat="server" 
            OldValuesParameterFormatString="original_{0}"
            SelectMethod="GetMake" 
            TypeName="FirstLook.Merchandising.DomainModel.MapVehicles.DataSourceMake">
             <SelectParameters>
                <asp:ControlParameter ControlID="MktClassDDL" Name="MktClassID" PropertyName="SelectedValue" Type="Int32" ConvertEmptyStringToNull="true" />
            </SelectParameters>
        </asp:ObjectDataSource>
        
        <asp:ObjectDataSource ID="GroupModelDataSource" runat="server" 
            OldValuesParameterFormatString="original_{0}"
            SelectMethod="GetModelNameByID" 
            TypeName="FirstLook.Merchandising.DomainModel.MapVehicles.DataSourceModel">
            <SelectParameters>
                <asp:ControlParameter ControlID="GroupMakeDropDownList" Name="divisionID" PropertyName="SelectedValue" Type="Int32" />
                <asp:ControlParameter ControlID="MktClassDDL" Name="MktClassID" PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
         <asp:ObjectDataSource ID="GroupTrimDataSource" runat="server" 
                OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetTrimByID" 
                TypeName="FirstLook.Merchandising.DomainModel.MapVehicles.DataSourceTrim">
                <SelectParameters>
                    <asp:ControlParameter ControlID="GroupMakeDropDownList" Name="divisionID" PropertyName="SelectedValue"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="GroupModelDropDownList" Name="modelName" PropertyName="SelectedValue"
                        Type="String" />                    
                    <asp:ControlParameter ControlID="MktClassDDL" Name="mktClassId" PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="GroupYearDataSource" runat="server" 
            SelectMethod="GetYear" 
            OnSelecting="NeedsTrim_Selecting"
            TypeName="FirstLook.Merchandising.DomainModel.MapVehicles.DataSourceYear">
            <SelectParameters>
                <asp:ControlParameter ControlID="GroupMakeDropDownList" Name="divisionId" PropertyName="SelectedValue"
                    Type="Int32" ConvertEmptyStringToNull="true" />
                <asp:ControlParameter ControlID="GroupModelDropDownList" Name="modelName" PropertyName="SelectedValue"
                    Type="String" />
                <asp:ControlParameter ControlID="GroupTrimDropDownList" Name="trim" PropertyName="SelectedValue" Type="String" ConvertEmptyStringToNull="false" />
            </SelectParameters>
        </asp:ObjectDataSource>
         <asp:ObjectDataSource ID="VehicleTypeDataSource" runat="server" 
            SelectMethod="GetVehicleTypes" 
            TypeName="FirstLook.Merchandising.DomainModel.MapVehicles.VehicleTypeDataSource">
        </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="GridViewDataSource" runat="server" 
            OldValuesParameterFormatString="original_{0}"
            SelectMethod="GroupGetStyleID"  
            OnSelecting="NeedsTrim_Selecting"
            TypeName="FirstLook.Merchandising.DomainModel.MapVehicles.GroupGridViewSource" >
            <SelectParameters>
                <asp:ControlParameter ControlID="MktClassDDL" Name="marketClassId" PropertyName="SelectedValue" ConvertEmptyStringToNull="true"
                    Type="Int32" />
                <asp:ControlParameter ControlID="GroupMakeDropDownList" Name="divisionId" PropertyName="SelectedValue"
                    Type="Int32" />
                <asp:ControlParameter ControlID="GroupModelDropDownList" Name="modelName"  PropertyName="SelectedValue"
                    Type="String" />
                <asp:ControlParameter ControlID="GroupYearSelectionList" Name="modelYear" PropertyName="SelectedValue"
                    Type="Int32" ConvertEmptyStringToNull="true" />
                <asp:ControlParameter ControlID="GroupTrimDropDownList" Name="trim" PropertyName="SelectedValue" Type="String" ConvertEmptyStringToNull="false" />
            </SelectParameters>
        </asp:ObjectDataSource>
         <!------------------------------------------------------------------------------------------------------------->
        <asp:SqlDataSource ID="mktClassDS" runat="server" ConnectionString="<%$ ConnectionStrings:Merchandising %>"
            SelectCommand="SELECT DISTINCT MktClassID, MarketClass FROM vehiclecatalog.chrome.mktClass ORDER BY MarketClass">
        </asp:SqlDataSource>
       <p><b>Market Class</b>        
            <asp:DropDownList ID="MktClassDDL" 
                OnDataBound="MktClassDDLBound" runat="server"
                DataSourceID="mktClassDS"  
                DataTextField="MarketClass" 
                DataValueField="MktClassID" 
                AutoPostBack="true"
                > 
            </asp:DropDownList>
       </p>
       <p><b>Make</b>
            <asp:DropDownList ID="GroupMakeDropDownList" runat="server" 
                AutoPostBack="True" 
                OnDataBound="MakeDDL_Bound"
                DataSourceID="GroupMakeDataSource" 
                DataTextField="Name" 
                DataValueField="MakeId" 
                TabIndex="1">
            </asp:DropDownList>
        </p>
        <p><b>Model</b>         
            <asp:DropDownList ID="GroupModelDropDownList"  
                OnDataBound="ModelDDLBound" 
                runat="server" 
                AutoPostBack="True" 
                DataSourceID="GroupModelDataSource" 
                DataTextField="Name"
                DataValueField="Name"
                >
            </asp:DropDownList>
        </p>        
        <p><b>Trim</b>
             <asp:DropDownList ID="GroupTrimDropDownList"  
                OnDataBound="TrimDDLBound" 
                runat="server"                     
                AutoPostBack="True" 
                DataSourceID="GroupTrimDataSource"                 
                >              
            </asp:DropDownList>
        </p>
        <p><b>Model Year</b>
        <asp:DropDownList ID="GroupYearSelectionList"  runat="server" 
            AutoPostBack="True" 
            DataSourceID="GroupYearDataSource"
            OnDataBound="YearDDL_Bound"
            >
        </asp:DropDownList>
        </p>
        <p><b>New or Used</b>
        <asp:DropDownList ID="VehicleTypeList"  
            runat="server" 
            AutoPostBack="True" 
            DataSourceID="VehicleTypeDataSource"
            >
        </asp:DropDownList>
        </p>
        </div>
        <div class="section">
            <h4>2. Map to Vehicle Set or Individual Styles</h4>
            <p><b>Framework:</b><carbuilder:TemplateSelector ID="templateSelector" runat="server" /></p>
            <p><b>Theme:</b><carbuilder:ThemeSelector ID="themeSelector" runat="server" /></p>
        
            <p><asp:Button ID="AddIndividualVehicles" runat="server" OnClick="AddIndividual_Click" Text="Map Checked Vehicles ONLY" /></p>
            <p>&mdash;OR&mdash;</p>         
            <p><asp:Button ID="MapEntireSet" runat="server" OnClick="MapEntireSet_Click" Text="Map All Matching Vehicles" /></p>
        </div>
    </div>
    <div class="setOperators">
        <div class="warning">
            <asp:Label ID="warningMessage" runat="server"></asp:Label>
        </div>
        <div class="section">
         <asp:GridView ID="GridView1" runat="server" 
                AllowPaging="True" 
                AutoGenerateColumns="False" 
                DataSourceID="GridViewDataSource"
                DataKeyNames="StyleId"              
                  CellPadding="4" ForeColor="#333333" GridLines="None">
                  <EmptyDataTemplate>
                        <p>No vehicle matches:  Select vehicles using drop downs</p>
                  </EmptyDataTemplate>
               <Columns>
                <asp:TemplateField HeaderText="Choose">
                        <ItemTemplate>
                           <asp:CheckBox ID="ChooseRow" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>               
                   <asp:BoundField DataField="DivisionName" HeaderText="Division Name" SortExpression="DivisionName" />
                   <asp:BoundField DataField="ModelName" HeaderText="Model Name" SortExpression="ModelName" /> 
                   <asp:BoundField DataField="ModelYear" HeaderText="Model Year" SortExpression="ModelYear" />               
                   <asp:BoundField DataField="Trim" HeaderText="Trim" SortExpression="Trim" /> 
                   <asp:BoundField DataField="StyleName" HeaderText="Style Name" SortExpression="StyleName" />
                </Columns>
                
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#999999" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
            <asp:Button ID="CheckAll" OnClick="OnClick_SelectAll" runat="server" Text="Check All" />
            <asp:Button ID="UnCheckAll" OnClick="OnClick_UnCheckAll" runat="server"  Text="UnCheck All" />
        </div>
        
        
     </div>           
     <div style="clear:both"></div>
</div>


