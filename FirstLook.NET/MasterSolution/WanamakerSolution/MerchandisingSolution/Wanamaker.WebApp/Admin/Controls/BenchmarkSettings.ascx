﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="BenchmarkSettings.ascx.cs" Inherits="Wanamaker.WebApp.Admin.Controls.BenchmarkSettings" %>

    <div class="benchmarkingHeader"><%= SectionTitle %></div>
    <table>
        <tr>
            <th colspan="2">Cost Per Impression</th>
            <th colspan="2">Cost Per Direct Lead</th>
        </tr>
    <asp:Repeater ID="ListingSiteVehicleType" runat="server">
        <ItemTemplate>
            <tr class="typeHeader" runat="server">
                <td class="vehType" colspan="2"><asp:Literal ID="ImpressionVehicleType" runat="server"></asp:Literal></td>
                <td class="vehType" colspan="2"><asp:Literal ID="LeadVehicleType" runat="server"></asp:Literal></td>
            </tr>
            <asp:Repeater ID="ListingSiteVehicleTypeDealershipSegment" runat="server">
                <ItemTemplate>
                    <tr>
                        <td><asp:Literal ID="ImpressionDealerSegment" runat="server"></asp:Literal></td>
                        <td><asp:TextBox ID="ImpressionCost" runat="server"></asp:TextBox></td>
                        <td><asp:Literal ID="LeadDealerSegment" runat="server"></asp:Literal></td>
                        <td><asp:TextBox ID="LeadCost" runat="server"></asp:TextBox></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </ItemTemplate>
    </asp:Repeater>

</table>