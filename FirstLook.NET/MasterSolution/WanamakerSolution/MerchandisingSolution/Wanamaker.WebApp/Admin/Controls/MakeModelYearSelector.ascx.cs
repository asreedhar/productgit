﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web.UI;
using FirstLook.Merchandising.DomainModel.SnippetCollector;
using Wanamaker.WebApp.Admin.Snippets;

namespace Wanamaker.WebApp.Admin.Controls
{
    public partial class MakeModelYearSelector : UserControl
    {
        public SnippetCollectorDataAccess DataAccess = new SnippetCollectorDataAccess();

        protected void Page_PreRender(object sender, EventArgs e)
        {
            BindData();
        }

        private void BindData()
        {
            VehicleMakesSelectBox.DataSource = DataAccess.GetMakes();
            VehicleMakesSelectBox.DataTextField = "DivisionName";
            VehicleMakesSelectBox.DataValueField = "DivisionId";
            VehicleMakesSelectBox.DataBind();

            // If id passed, then page is in edit mode
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                BindSnippetData(int.Parse(Request.QueryString["id"]));
            }
        }

        private void BindSnippetData(int snippetId)
        {
            // Get snippet
            SnippetCollectorDataAccess dataAccess = new SnippetCollectorDataAccess();
            var snippetByID = dataAccess.GetSnippetById(snippetId);

            // Set makes and populate/select models and styles list boxes
            SetMakeModelStyleListBoxes(snippetByID.Styles);
        }

        private void SetMakeModelStyleListBoxes(string styles)
        {
            // Get models from style Ids
            DataSet models = DataAccess.GetModelsFromChromeStyle(styles);
            string[] selectedModels = models.Tables[0].AsEnumerable().Select(x => x.Field<int>("modelId").ToString()).ToArray();

            // Get makes from model Ids
            DataSet makes = DataAccess.GetMakesFromModels(String.Join(",", selectedModels));
            string[] selectedMakes = makes.Tables[0].AsEnumerable().Select(x => x.Field<int>("DivisionId").ToString()).ToArray();

            // Bind models list box
            VehicleModelsSelectBox.DataSource = DataAccess.GetModels(selectedMakes);
            VehicleModelsSelectBox.DataTextField = "modelNameAndYear";
            VehicleModelsSelectBox.DataValueField = "modelId";
            VehicleModelsSelectBox.DataBind();

            // Bind style/trims list box
            VehicleTrimsSelectBox.DataSource = DataAccess.GetTrims(selectedModels);
            VehicleTrimsSelectBox.DataTextField = "styleName";
            VehicleTrimsSelectBox.DataValueField = "styleId";
            VehicleTrimsSelectBox.DataBind();

            // Enable list boxes
            VehicleModelsSelectBox.Enabled = true;
            VehicleTrimsSelectBox.Enabled = true;

            // Select items
            ((SnippetBasePage)Page).SetMultiSelectListBox(VehicleMakesSelectBox, String.Join(",", selectedMakes));
            ((SnippetBasePage)Page).SetMultiSelectListBox(VehicleModelsSelectBox, String.Join(",", selectedModels));
            ((SnippetBasePage)Page).SetMultiSelectListBox(VehicleTrimsSelectBox, styles);

        }
    }
}