﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavigationMenu.ascx.cs" Inherits="Wanamaker.WebApp.Admin.Controls.NavigationMenu" %>

<ul id="SiteMenuUL">
    <li><a href="~/Admin/Admin.aspx" runat="server">Admin Home</a></li>
    <%--<li class="menuHead">
        <a href="#">Merchandising</a>
        <div class="subMenu">
            <h5><a href="#">Merchandising</a></h5>
            <ul class="clearfix">
                <li><a href="#">Ad Frameworks</a></li>
                <li><a href="#">Ad Templates</a></li>
                <li><a href="#">CPO Programs</a></li>
                <li><a href="~/Admin/Snippets/AddSnippet.aspx" runat="server">Reviews &amp; Awards Snippets</a></li>
                <li><a href="#">Window Stickers</a></li>
            </ul>
        </div>
    </li>
    <li class="menuHead">
        <a href="#">Dealers</a>
        <div class="subMenu">
            <h5><a href="#">Dealers</a></h5>
            <ul class="clearfix">
                <li><a href="#">Activation</a></li>
                <li><a href="#">Third Party Accounts</a></li>
                <li><a href="#">Lot Providers</a></li>
                <li><a href="#">Distribution</a></li>
            </ul>
        </div>
    </li>
    <li class="menuHead">
        <a href="#">System</a>
        <div class="subMenu">
            <h5><a href="#">System</a></h5>
            <ul class="clearfix">
                <li><a href="#">Release Processor</a></li>
                <li><a href="#">Server/Network</a></li>
                <li><a href="#">Traffic/Current Users</a></li>
            </ul>
        </div>
    </li>
    <li><a href="#">Users</a></li>--%>
    <li><a href="~/Admin/BatchAutoLoadStatus.aspx" runat="server">Batch AutoLoad Status</a></li>
    <li><a href="~/Admin/AdminTemplateManager.aspx" runat="server">Template Manager</a></li>
    <li><a href="~/Admin/Snippets/AddSnippet.aspx" runat="server">Snippet Collector</a></li>
    <li><a id="A1" href="~/Admin/BenchmarkEntry.aspx" runat="server">Benchmark Entry</a></li>
</ul>

<ul id="SystemMenuUL">
    <li><a href="~/Workflow/Inventory.aspx" runat="server">Back to MAX Ad</a></li>
</ul>