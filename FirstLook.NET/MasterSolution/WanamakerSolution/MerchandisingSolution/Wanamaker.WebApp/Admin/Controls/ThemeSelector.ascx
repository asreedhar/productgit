<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ThemeSelector.ascx.cs" Inherits="Wanamaker.WebApp.Admin.Controls.ThemeSelector" %>


 <asp:DropDownList runat="server" ID="ThemeDDL" 
                    DataTextField="name" DataValueField="Id"
                    DataSourceID="ThemeDS" OnDataBound="ThemeDDL_Bound" AppendDataBoundItems="true">
                    <asp:ListItem Text="No theme..." Value=""></asp:ListItem>    
</asp:DropDownList>   

<asp:SqlDataSource ID="ThemeDS" runat="server" 
        SelectCommandType="StoredProcedure" 
        ConnectionString='<%$ ConnectionStrings:Merchandising %>'
        SelectCommand="templates.Themes#Fetch">
        
    <SelectParameters>
        <asp:Parameter Name="BusinessUnitID" DefaultValue="100150" ConvertEmptyStringToNull="true" Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource> 
                