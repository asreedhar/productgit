using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Wanamaker.WebApp.Admin.Controls
{
    public partial class ThemeSelector : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public event EventHandler ThemeSelectorDataBound;
        protected void ThemeDDL_Bound(object sender, EventArgs e)
        {
            if (ThemeSelectorDataBound != null)
            {
                ThemeSelectorDataBound(sender, e);
            }

        }

        public int? ThemeId
        {
            get
            {
                if (ThemeDDL.SelectedIndex == 0)
                {
                    return null;
                }
                return int.Parse(ThemeDDL.SelectedValue);
            }
            set
            {
                if (value.HasValue && ThemeDDL.Items.FindByValue(value.ToString()) != null)
                {
                    ThemeDDL.SelectedValue = value.ToString();
                }
                else
                {
                    ThemeDDL.SelectedIndex = 0;
                }
            }
        }
    }
}