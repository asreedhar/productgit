using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Logging;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Workflow.AutoOffline;
using MAX.Entities;
using MAX.Entities.Enumerations;
using MAX.ExternalCredentials;
using Merchandising.Messages;
using Merchandising.Messages.AutoLoad;
using Merchandising.Messages.GroupLevel;

namespace Wanamaker.WebApp.Admin.Controls
{
    public partial class MiscSettings : System.Web.UI.UserControl
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<MiscSettings>();

        #region Injected dependencies

        public IAdMessageSender MessageSender { get; set; }
        public IAutoOfflineProcessor AutoOfflineProcessor { get; set; }

        #endregion

        protected override void OnLoad(EventArgs e)
        {
            AutoOfflineMessage.InnerText = "";
            GroupLevelDashboardEnabled.CheckedChanged += GroupLevelDashboardEnabled_CheckedChanged;
            base.OnLoad(e);
        }

        /// <summary>
        /// BUGZID: 25225
        /// When anyone changes this checkbox, we need to make sure everyone gets the change.
        /// </summary>
        void GroupLevelDashboardEnabled_CheckedChanged(object sender, EventArgs e)
        {
            var dealerships = BusinessUnitFinder.Instance().Find(BusinessUnitId).DealerGroup().Dealerships();
            dealerships.ToList().ForEach(bu => GetMiscSettings.InvalidateCache(bu.Id));
        }

        public int BusinessUnitId
        {
            get
            {
                var o = ViewState["ctlbuid"];
                return (o == null) ? 100150 : (int)o;
            }
            set
            {
                ViewState["ctlbuid"] = value;
                LoadSettings(value);
            }
        }

        public void Refresh()
        {
            LoadSettings(BusinessUnitId);

        }

        private void LoadSettings(int businessUnitId)
        {
            var optionsCommand = GetMiscSettings.GetSettings(businessUnitId);

            settings.Visible = optionsCommand.HasSettings;
            noSettings.Visible = !optionsCommand.HasSettings;

            if (optionsCommand.HasSettings)
                LoadControlValues(optionsCommand);
        }

        private void PopulateMaxForOEMList()
        {
            MaxForOEMList.Items.Clear();
            MaxForOEMList.Items.AddRange(
                Enum
                    .GetValues(typeof (Franchise))
                    .Cast<Franchise>()
                    .Select(v => new ListItem(
                                     v.ToString(), ((int) v).ToString(CultureInfo.InvariantCulture)))
                    .ToArray()
                );
        }

        private void PopulateGidProvidersList(ListControl ddlControl, GidProvider selectedGidProvider)
        {
            ddlControl.Items.Clear();
            ddlControl.Items.AddRange(
                Enum.GetValues(typeof(GidProvider))
                .Cast<GidProvider>()
                .Select(provider => new ListItem
                {
                    Text = EnumHelper.GetEnumDescription(typeof(GidProvider), provider),
                    Value = ((int)provider).ToString(CultureInfo.InvariantCulture),
                    Selected = provider.Equals(selectedGidProvider)
                })
                .ToArray()
            );
        }

        private void PopulateReportDataSourceList(ListControl ddlControl, ReportDataSource selectedReportDataSource)
        {
            ddlControl.Items.Clear();
            ddlControl.Items.AddRange(
                Enum.GetValues(typeof(ReportDataSource))
                .Cast<ReportDataSource>()
                .Select(provider => new ListItem
                {
                    Text = EnumHelper.GetEnumDescription(typeof(ReportDataSource), provider),
                    Value = ((int)provider).ToString(CultureInfo.InvariantCulture),
                    Selected = provider.Equals(selectedReportDataSource)
                })
                .ToArray()
            );
        }

        private void LoadControlValues(GetMiscSettings values)
        {
            PopulateMaxForOEMList();

            chkOptimalFormat.Checked = values.OptimalFormat;
            chkNewCars.Checked = values.PriceNewCars;
            batchPriceNewCars.Checked = values.BulkPriceNewCars;

            Max30Upgrade.Checked = values.MaxVersion == 3;
            WebLoaderEnabled.Checked = values.WebLoaderEnabled;
            BatchAutoloadEnabled.Checked = values.BatchAutoload;
            DashboardEnabled.Checked = values.ShowDashboard;
            ModelSpecificAdsEnabled.Checked = values.ModelLevelFrameworksEnabled;
            SavedModelSpecificAdSetting.Value = ModelSpecificAdsEnabled.Checked.ToString(CultureInfo.InvariantCulture);
            GroupLevelDashboardEnabled.Checked = values.ShowGroupLevelDashboard;
            AnalyticsSuiteCheckBox.Checked = values.AnalyticsSuite;

            MaxForOEMList.SelectedValue = ((int)values.Franchise).ToString(CultureInfo.InvariantCulture);
            OEMBrandingEnabled.Checked = values.Franchise != Franchise.None;

            AutoOffline_WholesalePlanTrigger.Checked = values.AutoOffline_WholesalePlanTrigger;
            CtrGraph.Checked = values.ShowCtrGraph;

            chkShowOlineClassifiedOverview.Checked = values.showOnlineClassifiedOverview;
            chkShowTimeToMarket.Checked = values.ShowTimeToMarket;

            txtDMSName.Text = values.DMSName;
            txtDMSEmail.Text = values.DMSEmail;

            ShowAndEnableBatchAutoloadControls();
            ShowAndEnableOEMBrandingControls();

            DealershipSegment selectedSegment = values.DealershipSegment;
            GetSegmentList(selectedSegment);

            PopulateGidProvidersList(GIDProviderNew, values.NewGidProvider);
            PopulateGidProvidersList(GIDProviderUsed, values.UsedGidProvider);
            PopulateReportDataSourceList(ReportDataSourceNew, values.ReportDataSourceNew);
            PopulateReportDataSourceList(ReportDataSourceUsed, values.ReportDataSourceUsed);

            var bt = (byte) GidProvider.eBiz;

            var cmd = new GetPhotoTransfersEnabled(BusinessUnitId);
            AbstractCommand.DoRun(cmd);
            PhotoTransfersEnabled.Checked = cmd.PreferenceValue;
        }

        private void ShowAndEnableBatchAutoloadControls()
        {
            var hasValidCredentials = DoesDealerHaveAnyValidCredentials(BusinessUnitId);

            var isValid = BatchAutoloadEnabled.Checked || hasValidCredentials;
            
            BatchAutoloadEnabled.Enabled = isValid;
            autoloadDisabled.Visible = !isValid;
            autoloadInstructions.Visible = isValid;
        }

        private void ShowAndEnableOEMBrandingControls()
        {
            MaxForOEMList.Enabled = OEMBrandingEnabled.Checked;

            if (!OEMBrandingEnabled.Checked)
            {
                MaxForOEMList.SelectedValue = "0";
            }
        }

        private static bool DoesDealerHaveAnyValidCredentials(int businessUnitId)
        {
            return EncryptedSiteCredential.DoesDealerHaveAnyValidCredentials(businessUnitId);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadSettings(BusinessUnitId);
            }
            else
            {
                WebLoaderEnabled.Checked = Max30Upgrade.Checked;
            }
        }

        protected void OnBatchAutoloadValidation(object source, ServerValidateEventArgs args)
        {
            try
            {
                if (BatchAutoloadEnabled.Checked)
                {
                    args.IsValid = DoesDealerHaveAnyValidCredentials(BusinessUnitId);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Failed to test if dealer has any valid credentials.", ex);
                args.IsValid = false;
            }
        }

        protected void OnOEMBrandingValidation(object source, ServerValidateEventArgs args)
        {
            args.IsValid = !OEMBrandingEnabled.Checked || GetSelectedOEMFranchise() != Franchise.None;
        }

        protected void OnAnalyticsSuiteValidate(object source, ServerValidateEventArgs args)
        {
            if (AnalyticsSuiteCheckBox.Checked)
            {
                if ( GetSelectedDealershipSegment().Equals(DealershipSegment.Undefined))
                {
                    args.IsValid = false;
                    AnalyticsSuiteValidator.ErrorMessage = String.Format( AnalyticsSuiteValidator.ErrorMessage, GetSegmentNames());
                }
            }
        }

        protected void OnShowTimeToMarketValidate(object source, ServerValidateEventArgs args)
        {
            if (chkShowTimeToMarket.Checked == false)
            {
                args.IsValid = true;
                return;
            }

            var dataSourceNew = (ReportDataSource) Convert.ToInt16(ReportDataSourceNew.SelectedValue);
            var dataSourceUsed = (ReportDataSource) Convert.ToInt16(ReportDataSourceUsed.SelectedValue);

            if (dataSourceNew > ReportDataSource.None && dataSourceUsed > ReportDataSource.None)
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
                showTimeToMarketValidator.ErrorMessage = String.Format(showTimeToMarketValidator.ErrorMessage, GetValidReportDataSourceString());
            }
        }

        private static string GetValidGidProviderString()
        {
            var sb = new StringBuilder();
            sb.AppendFormat("{0}, ", EnumHelper.GetEnumDescription(typeof (GidProvider), GidProvider.Aultec));
            sb.AppendFormat("{0}", EnumHelper.GetEnumDescription(typeof(GidProvider), GidProvider.eBiz));

            return sb.ToString();
        }

        private static string GetValidReportDataSourceString()
        {
            var sb = new StringBuilder();
            sb.AppendFormat("{0}, ", EnumHelper.GetEnumDescription(typeof(ReportDataSource), ReportDataSource.Aultec));
            sb.AppendFormat("{0}, ", EnumHelper.GetEnumDescription(typeof(ReportDataSource), ReportDataSource.eBiz));
            sb.AppendFormat("{0}", EnumHelper.GetEnumDescription(typeof(ReportDataSource), ReportDataSource.CarsApi));

            return sb.ToString();
        }

        private Franchise GetSelectedOEMFranchise()
        {
            int val;
            return !int.TryParse(MaxForOEMList.SelectedValue, out val) ? Franchise.None : (Franchise) val;
        }

        protected void OnSave(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                LoadSettings(BusinessUnitId); // revert to saved values
                return;
            }

            DealershipSegment selectedType = GetSelectedDealershipSegment();
            // If they turn off Analytics, unset the DealershipSegment.
            if (AnalyticsSuiteCheckBox.Checked == false)
                selectedType = DealershipSegment.Undefined;

            var miscSettings = new SetMiscSettings(
                BusinessUnitId, 
                chkOptimalFormat.Checked, 
                GetSelectedOEMFranchise(), 
                chkNewCars.Checked, 
                DashboardEnabled.Checked, 
                GroupLevelDashboardEnabled.Checked,
                AnalyticsSuiteCheckBox.Checked,
                BatchAutoloadEnabled.Checked,
                Max30Upgrade.Checked ? (byte)3 : (byte)2,
                WebLoaderEnabled.Checked,
                ModelSpecificAdsEnabled.Checked,
                AutoOffline_WholesalePlanTrigger.Checked,
                CtrGraph.Checked,
                selectedType,
                batchPriceNewCars.Checked,
                chkShowOlineClassifiedOverview.Checked,
                chkShowTimeToMarket.Checked,
                (GidProvider)GIDProviderNew.SelectedIndex,
                (GidProvider)GIDProviderUsed.SelectedIndex,
                txtDMSName.Text,
                txtDMSEmail.Text,
                (ReportDataSource) Convert.ToInt16(ReportDataSourceNew.SelectedValue),
                (ReportDataSource) Convert.ToInt16(ReportDataSourceUsed.SelectedValue));
            AbstractCommand.DoRun(miscSettings);

            if (GroupLevelDashboardEnabled.Checked)
            {
                var finder = BusinessUnitFinder.Instance();
                var businessUnit = finder.Find(BusinessUnitId);
                var dealerGrouup = businessUnit.DealerGroup();
                MessageSender.BootStrapGroupLevelIfNecessary(new GroupLevelAggregationMessage(dealerGrouup.Id), GetType().FullName);
            }
            
            if(miscSettings.BatchAutoload)
                MessageSender.SendBatchAutoLoadInventory(new AutoLoadInventoryMessage(BusinessUnitId, AutoLoadInventoryType.NoStatus), GetType().FullName);

            if (SavedModelSpecificAdSetting.Value != ModelSpecificAdsEnabled.Checked.ToString(CultureInfo.InvariantCulture))
            {
                if (ModelSpecificAdsEnabled.Checked)
                {
                    AddModelLevelFrameworksToDealer();
                }

                SavedModelSpecificAdSetting.Value = ModelSpecificAdsEnabled.Checked.ToString(CultureInfo.InvariantCulture);
            }

            var cmd = new SetPhotoTransfersEnabled(BusinessUnitId, PhotoTransfersEnabled.Checked);
            AbstractCommand.DoRun(cmd);
            GetMiscSettings.InvalidateCache(BusinessUnitId);
        }

        private DealershipSegment GetSelectedDealershipSegment()
        {
            int ds = 0;
            if (DealerSegmentList.SelectedIndex > -1)
            {
                ListItem selected = DealerSegmentList.SelectedItem;
                Int32.TryParse(selected.Value, out ds);
            }
            return (DealershipSegment)ds;
        }

        private void AddModelLevelFrameworksToDealer()
        {
            if (BusinessUnitId == 100150) return;
            
            var service = new ModelLevelFrameworkService();
            service.PushFrameworks(BusinessUnitId);
        }

        protected void QueueAutoLoads(object sender, EventArgs e)
        {
            var miscSettings = GetMiscSettings.GetSettings(BusinessUnitId);

            if (miscSettings.BatchAutoload)
            {
                MessageSender.SendBatchAutoLoadInventory(new AutoLoadInventoryMessage(BusinessUnitId, AutoLoadInventoryType.NoStatus), GetType().FullName);
                MessageSender.SendBatchAutoLoadInventory(new AutoLoadInventoryMessage(BusinessUnitId, AutoLoadInventoryType.Errors), GetType().FullName);
            }
        }

        protected void ProcessAutoOffline(object sender, EventArgs e)
        {
            OnSave(sender, e);

            if (!Page.IsValid)
                return;

            try
            {
                AutoOfflineProcessor.SendProcessAutoOfflineMessage(BusinessUnitId);
                AutoOfflineMessage.InnerText = "Message has been queued.";
            }
            catch (Exception ex)
            {
                Log.Error("Failed to queue ProcessAutoOfflineMessage.", ex);
                AutoOfflineMessage.InnerText = "Failed to queue ProcessAutoOffline message: " + ex.Message;
            }
        }

        private string GetSegmentNames()
        {
            StringBuilder sb = new StringBuilder();
            string[] names = Enum.GetNames(typeof(DealershipSegment));
            for (var n = 0; n < names.Length; n++)
            {
                if (n == names.Length - 1)
                {
                    sb.AppendFormat(" or {0}", names[n]);
                }
                else if (n == 0)
                {
                }
                else
                {
                    sb.AppendFormat("{0}, ", names[n]);
                }
            }
            return sb.ToString().HumanizeString();
        }

        private void GetSegmentList(DealershipSegment selectedValue)
        {
            DealerSegmentList.Items.Clear();

            foreach (var li in from bt in EnumHelper.GetValues<DealershipSegment>() where !bt.Equals(DealershipSegment.Undefined) select new ListItem
            {
                Text = Enum.GetName(typeof (DealershipSegment), bt).HumanizeString(),
                Value = ((int) bt).ToString(CultureInfo.InvariantCulture),
                Selected = bt.Equals(selectedValue)
            })
            {
                DealerSegmentList.Items.Add(li);
            }
        }

        protected void ReportDataSourceUsed_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ReportDataSourceUsed.SelectedIndex == (int) ReportDataSource.CarsApi)
                ReportDataSourceNew.SelectedIndex = (int) ReportDataSource.CarsApi;
        }

        protected void ReportDataSourceNew_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ReportDataSourceNew.SelectedIndex == (int)ReportDataSource.CarsApi)
                ReportDataSourceUsed.SelectedIndex = (int)ReportDataSource.CarsApi;
        }
    }
}