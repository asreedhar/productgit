﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="upgradeSettings.ascx.cs" Inherits="Wanamaker.WebApp.Admin.Controls.upgradeSettings" %>
<div id="MiscSettings" class="clearfix">
    <br/>
    <div >
        <asp:Label runat="server" ID="lblMessage" CssClass="errorMessage" EnableViewState="False"></asp:Label>
    </div>
    <div id="noSettings" runat="server">
        There are no settings available for this dealer. Possible cause: The dealer has not been setup for MAX AD yet.
    </div>
    <h4>Max for Selling Upgrade</h4>
    <div id="settings" class="fieldLabelPair" runat="server">
        <asp:CheckBox runat="server" ID="chkMaxForSmartphone" />
        <label for="<%= chkMaxForSmartphone.ClientID %>">Max for SmartPhone</label>
    </div>
    <div id="settings1" class="fieldLabelPair" runat="server">
        Auto Batch Template : <asp:DropDownList runat="server" ID="ddlTemplate"/>
    </div>
    <div id="settings2" class="fieldLabelPair" runat="server">
        <asp:CheckBox runat="server" ID="chkMaxForWebsite20"/>
        <label for="<%= chkMaxForWebsite20.ClientID %>">Max for Website 2.0</label>
        <div class="indent">
            <div id="TwilioSettings" class="fieldLabelPair">
                <asp:CheckBox runat="server" ID="chkUsePhoneMapping"/>
                <label for="<%= chkUsePhoneMapping.ClientID %>">Use Twilio Number</label>
                <div class="indent">
                    <div class="fieldLabelPair">
                        <asp:TextBox runat="server" ID="txtAreaCode" Width="50"></asp:TextBox>
                        <label for="<%= txtAreaCode.ClientID %>">Area Code to Use.</label>
                    </div>
                    <div class="fieldLabelPair">
                        <asp:TextBox runat="server" ID="txtMappedPhone"></asp:TextBox>
                        <label for="<%= txtMappedPhone.ClientID %>">Phone Number to Map to.</label>
                    </div>
                    <div class="fieldLabelPair">
                        <asp:Button runat="server" ID="btnProvision" OnClick="btnProvision_OnClick" Text="Provision Number" Enabled="False"/>
                        &nbsp;
                        <asp:Button runat="server" ID="btnProvisionTollFree" OnClick="btnProvisionTollFree_OnClick" Text="Provision Toll Free Number"/>
                        <asp:HiddenField runat="server" ID="hdnPurchased" Value="false"/>
                        <asp:HiddenField runat="server" ID="hdnEnableTollFree" Value="false"/>
                    </div>
                    <div class="fieldLabelPair">
                        <asp:TextBox runat="server" ID="txtProvisionedPhone" ReadOnly="True"></asp:TextBox>
                        <label for="<%= txtProvisionedPhone.ClientID %>">Provisioned Phone Number.</label>
                    </div>
                </div>
            </div>
            <div id="DealerRaterSettings" class="fieldLabelPair">
                <asp:CheckBox runat="server" ID="chkUseDealerRater"/>
                <label for="<%= chkUseDealerRater.ClientID %>">Use DealerRater</label>
                <div class="indent">
                    <div class="fieldLabelPair">
                        <asp:TextBox runat="server" ID="txtDealerRaterId"></asp:TextBox>
                        <label for="<%= txtDealerRaterId.ClientID %>">DealerRater Id</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="fieldLabelPair" runat="server">
        <asp:CheckBox runat="server" ID="chkMaxDigitalShowroom"/>
        <label for="<%= chkMaxDigitalShowroom.ClientID %>">Max Digital Showroom</label>
    </div>
    <div class="fieldLabelPair">
        <asp:CheckBox runat="server" ID="chkEnableBookValuations"/>
        <asp:Label runat="server" AssociatedControlID="chkEnableBookValuations">Enable Book Valuations in Showroom</asp:Label>
    </div>
    <div class="fieldLabelPair">
        <asp:CheckBox runat="server" ID="chkEnableShowroomReports"/>
        <asp:Label runat="server" AssociatedControlID="chkEnableShowroomReports">Digital Showroom and Website 2.0 Reporting</asp:Label>
    </div>
    <div class="fieldLabelPair" runat="server">
        Cars available in Digital Showroom: <asp:DropDownList runat="server" ID="ddlVehiclesInShowroom"/>
    </div>

    <div class="formButtons">
        <asp:Button Text="Save" ID="SaveButton" runat="server" OnClick="OnSave" />
    </div>
</div>




