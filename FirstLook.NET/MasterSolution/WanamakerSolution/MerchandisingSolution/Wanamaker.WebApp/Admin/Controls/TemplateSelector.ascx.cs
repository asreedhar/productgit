using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FirstLook.Merchandising.DomainModel.Templating;
using Wanamaker.WebApp.Controls;

namespace Wanamaker.WebApp.Admin.Controls
{

    public partial class TemplateSelector : BusinessUnitUserControl
    {
        public event EventHandler TemplateSelectorDataBound;

        public void SetBusinessUnitId(int businessUnitId)
        {
            base.OverrideBusinessUnitId(businessUnitId);
            TemplateDDL.DataBind();
        }
        protected void TemplateDDL_Bound(object sender, EventArgs e)
        {
            TemplateDDL.Items.Insert(0, new ListItem("default text...", Constants.DEFAULT_TEMPLATE_ID.ToString()));
            if (TemplateSelectorDataBound != null)
            {
                TemplateSelectorDataBound(sender, e);
            }
        }

        protected void Templates_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["businessUnitId"] = BusinessUnitID;
        }

        public int TemplateId
        {
            get
            {
                return int.Parse(TemplateDDL.SelectedValue);
            }
            set
            {
                if (TemplateDDL.Items.FindByValue(value.ToString()) != null)
                {
                    TemplateDDL.SelectedValue = value.ToString();
                }
                else
                {
                    TemplateDDL.SelectedIndex = 0;
                }
            }
        }
    }
}