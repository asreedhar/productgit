<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="MiscSettings.ascx.cs" Inherits="Wanamaker.WebApp.Admin.Controls.MiscSettings" %>
<div id="MiscSettings" class="clearfix">
    <div id="noSettings" runat="server">
        There are no settings available for this dealer. Possible cause: The dealer has not been setup for MAX AD yet.
    </div>
    <div id="settings" runat="server">
        <div id="Max30Section">
            <h4>
                <asp:CheckBox ID="Max30Upgrade" runat="server" />
                <label for="<%= Max30Upgrade.ClientID %>">Max 3.0 Upgrade</label>
            </h4>
            <div class="indent">
                <div class="fieldLabelPair">
                    <asp:CheckBox ID="WebLoaderEnabled" runat="server" Enabled="False" />
                    <label for="<%= WebLoaderEnabled.ClientID %>">Webloader</label>
                </div>
            
                <div class="fieldLabelPair">
                    <asp:CheckBox ID="BatchAutoloadEnabled" runat="server" />
                    <asp:Label AssociatedControlID="BatchAutoloadEnabled" ID="lblBatchAutoload" runat="server">Batch Autoload</asp:Label>
                    <asp:CustomValidator ID="customValidatorBatchAutoload" runat="server" OnServerValidate="OnBatchAutoloadValidation" Display="Dynamic" 
                        ErrorMessage="<div class='error_desc'><p>Settings not saved. Site credentials not valid.</p></div>" />
                    <div class="desc">
                        <p id="autoloadInstructions" runat="server">Please verify that the dealer has valid site credentials for the supported makes, before you turn batch autoload on.</p>
                        <p id="autoloadDisabled" runat="server">Batch autoload cannot be turned on because dealer does not have any valid credentials.</p>
                        <p>These can be accessed by going to Max ad for the dealer. Settings -> Max Settings (Autoload tab)</p>
                    </div>
                </div>
                    
                <div class="fieldLabelPair">
                    <asp:CheckBox runat="server" ID="OEMBrandingEnabled" />
                    <label for="<%= OEMBrandingEnabled.ClientID %>">MAX for OEM</label>
                    <asp:DropDownList ID="MaxForOEMList" runat="server" />
                    <asp:CustomValidator ID="OEMBrandingValidator" runat="server" OnServerValidate="OnOEMBrandingValidation" Display="Dynamic"
                        ErrorMessage="<div class='error_desc'><p>Settings not saved. No valid OEM brand selected.</p></div>" />
                </div>

                <div class="fieldLabelPair">
                    <asp:CheckBox ID="DashboardEnabled" runat="server" />
                    <label for="<%= DashboardEnabled.ClientID %>">Dashboard</label>
                </div>
            
                <div class="fieldLabelPair">
                    <asp:CheckBox ID="chkShowOlineClassifiedOverview" runat="server" />
                    <label for="<%= chkShowOlineClassifiedOverview.ClientID %>">Show Online Classified Overview</label>
                </div>
                
                <div class="fieldLabelPair">
                    GID Provider: 
                        <asp:Label ID="Label1" runat="server" AssociatedControlID="GIDProviderUsed">Used</asp:Label>
                        <asp:DropDownList runat="server" ID="GIDProviderUsed"/>
                        <asp:Label ID="Label2" runat="server" AssociatedControlID="GIDProviderNew">New</asp:Label>
                        <asp:DropDownList runat="server" ID="GIDProviderNew"/>
                     <div class="desc">
                         <p id="gidProviderHelp">Who sends to AutoTrader or Cars.com?</p>
                     </div>
                </div>
                
                <div class="fieldLabelPair">
                    Report Data Source: 
                        <asp:Label ID="lblReportSourceUsed" runat="server" AssociatedControlID="ReportDataSourceUsed">Used</asp:Label>
                        <asp:DropDownList runat="server" ID="ReportDataSourceUsed" 
                        onselectedindexchanged="ReportDataSourceUsed_SelectedIndexChanged"/>
                        <asp:Label ID="lblReportSourceNew" runat="server" AssociatedControlID="ReportDataSourceNew">New</asp:Label>
                        <asp:DropDownList runat="server" ID="ReportDataSourceNew" 
                        onselectedindexchanged="ReportDataSourceNew_SelectedIndexChanged"/>
                     <div class="desc">
                         <p id="P1">Select the source of data to populate the TTM and Not Online reports. Note that if CarsApi is selected for either new or used, the description 
                             checkbox will be disabled and the photos and price checkboxes enabled in the TTM 
                             settings section under the Reports tab.</p>
                     </div>
                </div>

                <div class="fieldLabelPair">
                    <asp:CheckBox ID="chkShowTimeToMarket" runat="server" />
                    <label for="<%= chkShowTimeToMarket.ClientID %>">Show Time To Market</label>
                    <asp:CustomValidator ID="showTimeToMarketValidator" runat="server" OnServerValidate="OnShowTimeToMarketValidate" Display="Dynamic" 
                        ErrorMessage="<div class='error_desc'><p>Settings not saved. You must select a valid Report Data Source ({0}) from the settings above.</p></div>" />
                    <div class="desc">
                        <p id="ttmHelp" runat="server">In order to enable Time To Market you must select a Report Data Source for both new and use vehicles.</p>
                    </div>
                </div>
                
                <div class="indent">
                    <div class="fieldLabelPair">
                        <label for="<%= txtDMSName.ClientID %>">DMS Name</label>
                        <asp:TextBox runat="server" ID="txtDMSName"></asp:TextBox>
                    </div>
                    <div class="fieldLabelPair">
                        <label for="<%= txtDMSEmail.ClientID %>">DMS Email Address</label>
                        <asp:TextBox runat="server" ID="txtDMSEmail"></asp:TextBox>
                    </div>
                </div>

                <div class="fieldLabelPair">
                    <asp:CheckBox runat="server" ID="ModelSpecificAdsEnabled" />
                    <label for="<%= ModelSpecificAdsEnabled.ClientID %>">Model Specific Ads</label>
                </div>

                <div class="fieldLabelPair">
                    <asp:CheckBox runat="server" ID="AnalyticsSuiteCheckBox" />
                    <label for="<%= AnalyticsSuiteCheckBox.ClientID %>">Digital Performance Analytics</label>
                    
                        <asp:RadioButtonList ID="DealerSegmentList" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table"></asp:RadioButtonList>
                    
                    <asp:CustomValidator ID="AnalyticsSuiteValidator" runat="server" OnServerValidate="OnAnalyticsSuiteValidate" Display="Dynamic" 
                        ErrorMessage="<div class='error_desc'><p>Settings not saved. You must select one of {0}.</p></div>" />
                    <div class="desc">
                        <p id="AnalyticsInstructions" runat="server">Please select one value in order to enable Online Performance Analytics.</p>
                    </div>
                </div>

               <asp:HiddenField ID="SavedModelSpecificAdSetting" runat="server"/>

            </div>
        </div>
        
        <h4>Dealer Group</h4>
        <div class="fieldLabelPair">
            <asp:CheckBox runat="server" ID="GroupLevelDashboardEnabled" />
            <label for="<%= GroupLevelDashboardEnabled.ClientID %>">Group Level Dashboard</label>
            <div class="desc">
                <p>Modifying this value and clicking Save will change the setting group-wide; every dealer in this group will be affected.</p>
            </div>
        </div>


        <h4>Optimal Format</h4>
        <div class="fieldLabelPair">
            <asp:CheckBox ID="chkOptimalFormat" runat="server" />
            <label for="<%= chkOptimalFormat.ClientID %>">Send Optimal Format</label>
        </div>
        
        <h4>Requeue Failed and Empty Autoload Vehicles</h4>
        <div class="fieldLabelPair">
            <asp:Button Text="Requeue" ID="QueueAutoLoadButton" runat="server" OnClick="QueueAutoLoads" />
        </div>

        <h4>Price New Cars</h4>
        <div class="fieldLabelPair">
            <asp:CheckBox ID="chkNewCars" runat="server" />
            <label for="<%= chkNewCars.ClientID %>">Enable New Car Pricing</label>
            <div class="indent">
                <asp:CheckBox ID="batchPriceNewCars" runat="server" />      
                <label for="<%= batchPriceNewCars.ClientID %>">Enable Batch New Car Pricing</label>
            </div>
        </div>
        
        <h4>Auto Offline</h4>
        <div class="fieldLabelPair">
            <asp:CheckBox runat="server" ID="AutoOffline_WholesalePlanTrigger" />
            <label for="<%= AutoOffline_WholesalePlanTrigger.ClientID %>">Move vehicles offline that have a Wholesale plan in IMP</label>
            <asp:Button runat="server" Text="Process for this Dealer" OnClick="ProcessAutoOffline" />
            <label runat="server" id="AutoOfflineMessage" class="help"></label>
        </div>

        <h4>CTR Graph</h4>
        <div class="fieldLabelPair">
            <asp:CheckBox runat="server" ID="CtrGraph" />
            <label for="<%= CtrGraph.ClientID %>">Show CTR Graph</label>
        </div>

        <h4>Photo Transfers - Enables Photo Transfers for this dealer and all dealers in the same group.</h4>
        <div class="fieldLabelPair">
            <asp:CheckBox runat="server" ID="PhotoTransfersEnabled" />
            <label for="">Enable Photo Transfers</label>
        </div>
        
        <div class="formButtons">
            <asp:Button Text="Save" ID="SaveButton" runat="server" OnClick="OnSave" />
        </div>
        
    </div>
</div>