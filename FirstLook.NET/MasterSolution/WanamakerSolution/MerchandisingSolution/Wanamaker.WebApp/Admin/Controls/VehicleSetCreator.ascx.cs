using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FirstLook.Merchandising.DomainModel.Templating;
using MAX.Entities;

namespace Wanamaker.WebApp.Admin.Controls
{
    public partial class VehicleSetCreator : System.Web.UI.UserControl
    {
        public int BusinessUnitId
        {
            get
            {
                object o = ViewState["ctlbuid"];
                return (o == null) ? 100150 : (int)o;
            }
            set
            {
                ViewState["ctlbuid"] = value;
                templateSelector.SetBusinessUnitId(value);
            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            warningMessage.Text = string.Empty;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

            }

        }


        public enum SearchType
        {
            NotSet = -1,
            Individual = 1,
            Group = 0
        }

        protected void TrimDDLBound(object sender, EventArgs e)
        {
            GroupTrimDropDownList.Items.Insert(0, new ListItem("Select Trim...", "NOTRIM"));
        }

        protected void ModelDDLBound(object sender, EventArgs e)
        {
            GroupModelDropDownList.Items.Insert(0, new ListItem("Select Model...", ""));
        }

        protected void MktClassDDLBound(object sender, EventArgs e)
        {
            MktClassDDL.Items.Insert(0, new ListItem("Select Market Class...", ""));
        }

        protected void OnClick_SelectAll(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridView1.Rows)
            {
                CheckBox chkAll = (CheckBox)row.FindControl("ChooseRow");
                chkAll.Checked = true;
            }
        }

        protected void OnClick_UnCheckAll(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridView1.Rows)
            {
                CheckBox unChkAll = (CheckBox)row.FindControl("ChooseRow");
                unChkAll.Checked = false;
            }
        }


        protected void MapEntireSet_Click(object sender, EventArgs e)
        {
            int templateId = templateSelector.TemplateId;
            /*int.TryParse(templateDDL.SelectedValue, out templateId);
            if (templateId == 0)
            {
                warningMessage.Text = "You must select a valid framework";
                return;
            }*/

            /*int? themeId = null;
            if (themeDDL.SelectedIndex > 0)
            {
                themeId = int.Parse(themeDDL.SelectedValue);
            }*/
            int? themeId = themeSelector.ThemeId;

            int? mktClassId = null;
            if (MktClassDDL.SelectedIndex > 0)
            {
                mktClassId = int.Parse(MktClassDDL.SelectedValue);
            }

            int? makeId = null;
            if (GroupMakeDropDownList.SelectedIndex > 0)
            {
                makeId = int.Parse(GroupMakeDropDownList.SelectedValue);
            }

            string modelName = string.Empty;
            if (GroupModelDropDownList.SelectedIndex > 0)
            {
                modelName = GroupModelDropDownList.SelectedValue;
            }

            int? modelYear = null;
            if (GroupYearSelectionList.SelectedIndex > 0)
            {
                modelYear = int.Parse(GroupYearSelectionList.SelectedValue);
            }

            string trim = null;
            if (GroupTrimDropDownList.SelectedIndex > 0)
            {
                trim = GroupTrimDropDownList.SelectedValue;
            }

            var type = GetSelectedVehicleType();

            int retVal = AdTemplateSettings.CreateDefaultTemplateSet(BusinessUnitId, templateId, themeId, mktClassId, makeId, null, modelName, modelYear, trim, type);
            if (retVal == 1)
            {
                warningMessage.Text = "Successfully mapped!";
            }
        }

        private VehicleType GetSelectedVehicleType()
        {
            var vehicleType = VehicleType.Both;

            switch (VehicleTypeList.SelectedValue)
            {
                case "New":
                    vehicleType = VehicleType.New;
                    break;
                case "Used":
                    vehicleType = VehicleType.Used;
                    break;
            }

            return vehicleType;
        }

        protected void YearDDL_Bound(object sender, EventArgs e)
        {
            GroupYearSelectionList.Items.Insert(0, new ListItem("Select year...", ""));
        }



        protected void MakeDDL_Bound(object sender, EventArgs e)
        {
            GroupMakeDropDownList.Items.Insert(0, new ListItem("Select make...", ""));
        }

        protected void NeedsTrim_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            if (string.Compare((string)e.InputParameters["trim"], "NOTRIM") == 0)
            {
                e.InputParameters["trim"] = null;
            }
        }

        protected void AddIndividual_Click(object sender, EventArgs e)
        {
            var type = GetSelectedVehicleType();

            int ct = 0;
            foreach (GridViewRow gvr in GridView1.Rows)
            {
                CheckBox cb = gvr.FindControl("ChooseRow") as CheckBox;
                if (cb != null && cb.Checked)
                {
                    DataKey dk = GridView1.DataKeys[gvr.RowIndex];

                    if (dk == null) continue;

                    int styleId = (int)dk.Value;
                    AdTemplateSettings.SetAsDefaultForStyle(BusinessUnitId, templateSelector.TemplateId,
                                                            themeSelector.ThemeId, styleId, type);
                    ct++;
                }
            }

            if (ct > 0)
            {
                warningMessage.Text = string.Format("Successfully Mapped {0} vehicles", ct);
            }
            else
            {
                warningMessage.Text = "Please check boxes for vehicles to map them individually";
            }
        }
    }
}