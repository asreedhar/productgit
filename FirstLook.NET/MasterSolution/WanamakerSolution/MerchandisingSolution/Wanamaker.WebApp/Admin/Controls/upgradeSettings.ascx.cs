﻿using System;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web.Security;
using System.Web.UI.WebControls;
using BulkWindowSticker;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Commands;
using MAX.Entities;
using MAX.FirstLookServicesApiClient;
using Wanamaker.WebApp.AppCode.AccessControl;

namespace Wanamaker.WebApp.Admin.Controls
{
    public partial class upgradeSettings : System.Web.UI.UserControl
    {
        public IFirstLookBooksClient FirstLookBooksClient { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            // all loading handed from parent page
        }

        public int BusinessUnitId
        {
            get
            {
                var o = ViewState["ctlbuid"];
                return (o == null) ? 100150 : (int)o;
            }
            set
            {
                ViewState["ctlbuid"] = value;
                ViewState["ctlMapping"] = null; // release Dealer Phone Mapping
                ViewState["ctlRater"] = null; // release the Dealer Rater
                LoadSettings(value);
            }
        }

        public DealerPhoneMapping PhoneMapping
        {
            get
            {
                var phoneMapping = ViewState["ctlMapping"] as DealerPhoneMapping;
                return phoneMapping ?? (ViewState["ctlMapping"] = new DealerPhoneMapping(BusinessUnitId)) as DealerPhoneMapping;
            }
        }

        public DealerRater Rater
        {
            get
            {
                var rater = ViewState["ctlRater"] as DealerRater;
                return rater ?? (ViewState["ctlRater"] = new DealerRater(BusinessUnitId)) as DealerRater;
            }
        }

        protected void OnSave(object sender, EventArgs e)
        {
            /*
            if (!Page.IsValid)
            {
                LoadSettings(BusinessUnitId); // revert to saved values
                return;
            }
             */

            try
            {
                int? templateId = null;

                if (ddlTemplate.SelectedValue != "")
                    templateId = Convert.ToInt32(ddlTemplate.SelectedValue);

                if(templateId != null && !chkMaxForSmartphone.Checked)
                    throw new ApplicationException("Max for SmartPhone must be enabled to select an auto batch template");

                if (!chkMaxForWebsite20.Checked)
                {
                    chkUsePhoneMapping.Checked = false;
                    chkUseDealerRater.Checked = false;
                }

                if (chkEnableShowroomReports.Checked && !CanEnableShowroomReport())
                {
                    throw new ApplicationException("Max for Website 2.0 or Max Digital Showroom must be enabled to turn on Digital Showroom and Website 2.0 Reporting.");
                }

                if (chkUsePhoneMapping.Checked)
                {
                    PhoneMapping.Upsert(txtMappedPhone.Text);

                    btnProvision.Enabled = !PhoneMapping.Purchased;
                    btnProvisionTollFree.Enabled = !PhoneMapping.Purchased;
                    hdnPurchased.Value = PhoneMapping.Purchased ? "true" : "false";
                    txtAreaCode.Text = PhoneMapping.MappedPhoneAreaCode;
                    txtMappedPhone.Text = PhoneMapping.MappedPhone;
                    txtProvisionedPhone.Text = PhoneMapping.ProvisionedPhone;
                }
                if (chkUseDealerRater.Checked)
                {
                    Rater.Upsert(txtDealerRaterId.Text);

                    txtDealerRaterId.Text = Rater.DealerRaterId;
                }

                if (chkEnableBookValuations.Checked)
                {

                    var bu = BusinessUnitFinder.Instance().Find(BusinessUnitId);
                    // make sure the dealer has NADA books enabled.
                    // error if not
                    var books = FirstLookBooksClient.GetDealerBooks(BusinessUnitId,
                        ConfigurationManager.AppSettings["FirstLookServiceApiUser"], 
                        ConfigurationManager.AppSettings["FirstLookServiceApiPass"]);

                    if ( books is EmptyBooksList || !HasNada(bu, books))
                    {
                        chkEnableBookValuations.Checked = false;
                        throw new ApplicationException(
                            "The selected dealer does not have NADA books configured in FirstLook.");
                    }
                }

                VehicleType vehiclesInShowroom = null;
                vehiclesInShowroom = (VehicleType) Convert.ToInt16(ddlVehiclesInShowroom.SelectedValue);

                var setUpgradeSettings = new SetUpgradeSettings(
                    BusinessUnitId,
                    chkMaxForSmartphone.Checked,
                    templateId,
                    chkMaxForWebsite20.Checked,
					chkMaxDigitalShowroom.Checked,
                    chkUsePhoneMapping.Checked,
                    chkUseDealerRater.Checked,
                    chkEnableBookValuations.Checked,
                    vehiclesInShowroom,
                    chkEnableShowroomReports.Checked
                );

                AbstractCommand.DoRun(setUpgradeSettings);
            }
            catch (ApplicationException ex)
            {
                lblMessage.Text = ex.Message;
            }
            catch (System.Data.Common.DbException ex)
            {
                lblMessage.Text = ex.Message;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.ToString();
            }
        }

        /// <summary>
        /// Does the BU have the NadaValues upgrade or do they have nada books configured?
        /// </summary>
        /// <param name="bu"></param>
        /// <param name="books"></param>
        /// <returns></returns>
        private static bool HasNada(BusinessUnit bu, BooksList books)
        {
            return (bu.HasDealerUpgrade(Upgrade.NadaValues) || books.Books.Select(b => b.ToUpperInvariant()).Contains("NADA"));
        }

        private bool CanEnableShowroomReport()
        {
            return chkMaxDigitalShowroom.Checked || chkMaxForWebsite20.Checked;
        }

        private void LoadSettings(int businessUnitId)
        {
            try
            {
                var windowStickerRepository = new WindowStickerRepository();

                // load the possible templates for auto window sticker
                ddlTemplate.DataSource = windowStickerRepository.GetWindowStickerTemplates(businessUnitId);
                ddlTemplate.DataValueField = "Id";
                ddlTemplate.DataTextField = "Name";
                ddlTemplate.DataBind();
                ddlTemplate.Items.Insert(0, new ListItem("< none >", ""));

                var optionsCommand = GetUpgradeSettings.GetSettings(businessUnitId);
                PhoneMapping.Fetch();
                Rater.Fetch();

                settings.Visible = optionsCommand.HasSettings;
                noSettings.Visible = !optionsCommand.HasSettings;

                if (optionsCommand.HasSettings)
                    LoadControlValues(optionsCommand);

            }
            catch (ApplicationException ex)
            {
                lblMessage.Text = ex.Message;
            }
            catch (System.Data.Common.DbException ex)
            {
                lblMessage.Text = ex.Message;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.ToString();
            }

        }

        private void LoadControlValues(GetUpgradeSettings values)
        {
            chkMaxForSmartphone.Checked = values.MAXForSmartphone;
            if (values.AutoWindowStickerTemplateId != null)
                ddlTemplate.SelectedValue = values.AutoWindowStickerTemplateId.ToString();
            chkMaxForWebsite20.Checked = values.MAXForWebsite20;
	        chkMaxDigitalShowroom.Checked = values.MAXDigitalShowroom;

            // Dealer Phone Mapping
            chkUsePhoneMapping.Checked = values.UsePhoneMapping;
            hdnPurchased.Value = PhoneMapping.Purchased ? "true" : "false";
            hdnEnableTollFree.Value = "false";
            txtAreaCode.Text = PhoneMapping.MappedPhoneAreaCode;
            txtMappedPhone.Text = PhoneMapping.MappedPhone;
            txtProvisionedPhone.Text = PhoneMapping.ProvisionedPhone;

            // Dealer Rater
            chkUseDealerRater.Checked = values.UseDealerRater;
            txtDealerRaterId.Text = Rater.DealerRaterId;

            chkEnableBookValuations.Checked = values.EnableBookValuations;

            PopulateVehiclesInShowroomDropdown(ddlVehiclesInShowroom, values.VehiclesInShowroom);
            chkEnableShowroomReports.Checked = values.EnableShowroomReports;
        }

        private void PopulateVehiclesInShowroomDropdown(ListControl ddlControl, VehicleType selectedType)
        {
            ddlControl.Items.Clear();
            ddlControl.Items.AddRange(
                Enum.GetValues(typeof(VehicleTypeEnum))
                .Cast<VehicleTypeEnum>()
                .Select(type => new ListItem
                {
                    Text = EnumHelper.GetEnumDescription(typeof(VehicleTypeEnum), type),
                    Value = ((int)type).ToString(CultureInfo.InvariantCulture),
                    Selected = type.Equals((VehicleTypeEnum) selectedType.IntValue)
                })
                .ToArray()
            );
        }

        protected void btnProvision_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (chkUsePhoneMapping.Checked)
                {
                    if (PhoneMapping.GetNewNumber(txtAreaCode.Text))
                        txtProvisionedPhone.Text = PhoneMapping.ProvisionedPhone;
                    else
                    {
                        lblMessage.Text =
                            String.Format(@"There were no numbers available for area code {0}. Would you like to generate a toll-free number?", txtAreaCode.Text);
                        hdnEnableTollFree.Value = "true";
                    }
                }
                
            }
            catch (ApplicationException ex)
            {
                lblMessage.Text = ex.Message;
                hdnEnableTollFree.Value = "false";
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.ToString();
                hdnEnableTollFree.Value = "false";
            }
        }

        protected void btnProvisionTollFree_OnClick(object sender, EventArgs e)
        {
            if (chkUsePhoneMapping.Checked && PhoneMapping.GetNewTollFreeNumber())
                txtProvisionedPhone.Text = PhoneMapping.ProvisionedPhone;
            else
            {
                lblMessage.Text = @"No toll-free numbers were available.";
            }
        }
    }
}