﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Utilities;
using FirstLook.Merchandising.DomainModel.DataSource;
using FirstLook.Merchandising.DomainModel.Postings;
using MAX.Entities;
using MAX.Entities.Enumerations;
using BenchmarkingRecord = FirstLook.Merchandising.DomainModel.DataSource.BenchmarkingDataSource.BenchmarkingRecord;

namespace Wanamaker.WebApp.Admin.Controls
{
    public partial class BenchmarkSettings : System.Web.UI.UserControl
    {
        public EdtDestinations ListingSite { get; set; }
        public String SectionTitle { get; set; }
        public DateTime BenchmarkMonth { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
                LoadData();
        }

        public void LoadData()
        {
            BenchmarkingDataSource.BenchmarkDestinationObj data = BenchmarkingDataSource.LoadObject(ListingSite, BenchmarkMonth);

            ListingSiteVehicleType.DataSource = data.VehTypes;
            ListingSiteVehicleType.ItemDataBound += new RepeaterItemEventHandler(ListingSiteVehicleType_ItemDataBound);
            ListingSiteVehicleType.DataBind();
        }

        void ListingSiteVehicleType_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                BenchmarkingDataSource.BenchmarkVehTypes vehType = (BenchmarkingDataSource.BenchmarkVehTypes)e.Item.DataItem;
                Literal impressionType = (Literal)e.Item.FindControl("ImpressionVehicleType");
                if (impressionType != null)
                    impressionType.Text = EnumHelper.GetEnumDescription(typeof(VehicleType), vehType.VehType);

                Literal leadType = (Literal)e.Item.FindControl("LeadVehicleType");
                if (leadType != null)
                    leadType.Text = EnumHelper.GetEnumDescription(typeof(VehicleType), vehType.VehType);

                Repeater dsRepeater = (Repeater)e.Item.FindControl("ListingSiteVehicleTypeDealershipSegment");
                if (dsRepeater != null)
                {
                    try
                    {
                        List<BenchmarkingDataSource.BenchmarkSegments> segList = vehType.Segments;
                        dsRepeater.DataSource = vehType.Segments;
                        dsRepeater.ItemDataBound += new RepeaterItemEventHandler(dsRepeater_ItemDataBound);
                        dsRepeater.DataBind();
                    }
                    catch (Exception ex)
                    {
                        string mess = ex.Message;
                        Response.ClearContent();
                        Response.Write(mess);
                        Response.Flush();
                        Response.End();
                    }
                }
            }
        }

        void dsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                BenchmarkingDataSource.BenchmarkSegments segments = (BenchmarkingDataSource.BenchmarkSegments)e.Item.DataItem;
                Literal impressionSegment = (Literal)e.Item.FindControl("ImpressionDealerSegment");
                Literal leadSegment = (Literal)e.Item.FindControl("LeadDealerSegment");

                TextBox impressionCost = (TextBox)e.Item.FindControl("ImpressionCost");
                TextBox leadCost = (TextBox)e.Item.FindControl("LeadCost");

                impressionSegment.Text = segments.Segment.ToString();
                impressionCost.Text = String.Format("{0:0.00}", segments.Costs.Find(seg => seg.CostType.Equals(BenchmarkingDataSource.BenchmarkCostType.Impression)).Cost);

                leadSegment.Text = segments.Segment.ToString();
                leadCost.Text = String.Format("{0:0.00}", segments.Costs.Find(seg => seg.CostType.Equals(BenchmarkingDataSource.BenchmarkCostType.DirectLead)).Cost);
            }
        }

        internal void Save()
        {
            List<BenchmarkingRecord> rows = new List<BenchmarkingRecord>();
            
            BenchmarkingDataSource.BenchmarkDestinationObj saveData = new BenchmarkingDataSource.BenchmarkDestinationObj(ListingSite);
            foreach (RepeaterItem ri in ListingSiteVehicleType.Items)
            {
                Literal impressionType = (Literal)ri.FindControl("ImpressionVehicleType");
                VehicleTypeEnum vt = (VehicleTypeEnum)Enum.Parse(typeof(VehicleTypeEnum), impressionType.Text);

                Repeater segmentRepeater = (Repeater)ri.FindControl("ListingSiteVehicleTypeDealershipSegment");
                if (segmentRepeater != null)
                {
                    foreach (RepeaterItem segrep in segmentRepeater.Items)
                    {
                        Literal impressionSegment = (Literal)segrep.FindControl("ImpressionDealerSegment");
                        DealershipSegment dealerSegment = (DealershipSegment)Enum.Parse(typeof(DealershipSegment), impressionSegment.Text);

                        decimal directLeadCost = 0; 
                        decimal impressionCost = 0;
                        decimal.TryParse(((TextBox)segrep.FindControl("LeadCost")).Text, out directLeadCost);
                        decimal.TryParse(((TextBox)segrep.FindControl("ImpressionCost")).Text, out impressionCost);
                        
                        rows.Add( new BenchmarkingRecord()
                        {
                            SiteId = ListingSite,
                            DealershipSegmentType = dealerSegment,
                            UsedNew = vt,
                            Date = BenchmarkMonth,
                            ImpressionCost = impressionCost,
                            LeadCost = directLeadCost
                        });
                    }
                }
            }

            BenchmarkingDataSource.Save(rows);
        }
    }
}