<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="ErrorPage.aspx.cs" Theme="None" MasterPageFile="~/Workflow/Workflow.Master"
    Title="Site Error | MAX : Online Inventory. Prefected." Inherits="Wanamaker.WebApp.ErrorPage" %>
<%@ Import Namespace="Wanamaker.WebApp.Helpers" %>

<asp:Content ID="CssContent" ContentPlaceHolderID="CssPlaceHolder" runat="server">
    
    <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Main.debug.css") %>" />

    <style type="text/css">
        #Header .dealerSelector { display:none; }
        #Content { background:#f3f3f3; font-size:1.1em; margin-top:10px; padding:70px 0 90px; }
        #Content h3 { font-weight:bold; }
    </style>
</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="BodyPlaceHolder" runat="server">
    
    <asp:ScriptManager ID="ScriptManager2" runat="server"></asp:ScriptManager>
    
    <div id="Content">

        <div style="margin:0 auto; padding:1em; background:#fff; width:400px; border:solid 1px #ddd;">
            <asp:MultiView ID="ErrorView" runat="server">
                <asp:View ID="V400Error" runat="server">
                    <h3>MAX AD Issue Report</h3>
                    <br />
                    <p>
                        Sorry, we've experienced an unexpected error.
                        <br />
                        We have been notified and will fix the problem as soon as possible.
                    </p>
                    <br />
                    <p>
                        If the problem persists, please
                        <asp:HyperLink ID="errLnk1" runat="server">contact the helpdesk</asp:HyperLink>
                        to help us understand what you are trying to do in the system.
                    </p>
                    <br />
                    <a href="Workflow/Inventory.aspx">Return to Inventory List</a>
                </asp:View>
                <asp:View ID="V404Error" runat="server">
                    <h3>MAX AD 404 Not Found</h3>
                    <p>
                        Whoops! The resource you are looking for can not be found.<br />We have been notified of this problem.
                    </p>
                </asp:View>
                <asp:View ID="V403Error" runat="server">
                    <h3>
                        MAX AD Access Denied</h3>
                    <p>
                        You are not authorized to use MAX AD. If you think this restriction is improper, please close your browser completely and
                        attempt to login again.
                    </p>
                    <p>
                        If that is unsuccessful, please
                        <asp:HyperLink ID="errLnk2" runat="server">contact the helpdesk</asp:HyperLink>
                        to obtain access.
                    </p>
                </asp:View>
                <asp:View ID="V401Error" runat="server">
                    <h3>
                        MAX AD Access Denied</h3>
                    <p>
                        You are not authorized to use this feature of MAX AD. If you think this restriction is improper, please close your browser completely and
                        attempt to login again.
                    </p>
                    <p>
                        If that is unsuccessful, please
                        <asp:HyperLink ID="errLnk3" runat="server">contact the helpdesk</asp:HyperLink>
                        to obtain access.
                    </p>
                </asp:View>
            </asp:MultiView>
        </div>

    </div>

</asp:Content>

<asp:Content ID="ScriptContent" ContentPlaceHolderID="ScriptsPlaceHolder" runat="server">
</asp:Content>
