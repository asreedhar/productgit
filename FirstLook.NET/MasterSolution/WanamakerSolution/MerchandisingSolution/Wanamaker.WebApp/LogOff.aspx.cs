using System;
using System.Configuration;
using System.Web;
using System.Web.Security;
using Wanamaker.WebApp.AppCode;

namespace Wanamaker.WebApp
{
    public partial class LogOff : BasePage
    {
        /// <summary>
        /// We are basing this solely on the url 
        /// </summary>
        public bool IsMaxStandAlone
        {
            get
            {
                var standaloneurl = ConfigurationManager.AppSettings["max_standalone_url"];
                if (standaloneurl == null) return false;
                return Request.Url.ToString().ToLower().IndexOf(standaloneurl.ToLower()) > 0;

            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            // Delete the ASP.NET Authentication Token

            // ReSharper disable PossibleNullReferenceException
            if( Response.Cookies != null )
            {
                Response.Cookies.Add( new HttpCookie( FormsAuthentication.FormsCookieName ) { Expires = DateTime.Now.AddMinutes( -1 ) } );
                Response.Cookies.Add( new HttpCookie( "_session_id" ) { Expires = DateTime.Now.AddMinutes( -1 ) } );
                Response.Cookies.Add( new HttpCookie( "fltracker" ) { Expires = DateTime.Now.AddMinutes( -1 ) } );
                Response.Cookies.Add( new HttpCookie( "cas_filter_gateway" ) { Expires = DateTime.Now.AddMinutes( -1 ) } );
                Response.Cookies.Add(new HttpCookie("CASTGC") { Expires = DateTime.Now.AddMinutes(-1) });
                Response.Cookies.Add(new HttpCookie("CASPRIVACY") { Expires = DateTime.Now.AddMinutes(-1) });
                Response.Cookies.Add( new HttpCookie( ".ASPXAUTH" ) { Expires = DateTime.Now.AddMinutes( -1 ) } );
                Response.Cookies.Add( new HttpCookie( "ASP.NET_SessionId" ) { Expires = DateTime.Now.AddMinutes( -1 ) } );
                Response.Cookies.Add(new HttpCookie("JSESSIONID") { Expires = DateTime.Now.AddMinutes(-1) });
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId") { Expires = DateTime.Now.AddMinutes(-1) });
                Response.Cookies.Add( new HttpCookie( "dealerId" ) { Expires = DateTime.Now.AddMinutes( -1 ) } );
                Response.Cookies.Add( new HttpCookie( "WorkflowItemIndex" ) { Expires = DateTime.Now.AddMinutes( -1 ) } );   
                Response.Cookies.Add( new HttpCookie( "ActiveListType" ) { Expires = DateTime.Now.AddMinutes( -1 ) } );   
            }

            if( Session != null )
            {
                Session.Remove( "Inventory" );
                Session.Remove( "AgeBucket" );
                Session.Clear();
                Session.Abandon();
            }
            
            FormsAuthentication.SignOut();

            // Redirect to IMT Logout (which will send us onto CAS)

            var baseUrl = WebSettings.CurrentBaseUrl;
            string url = WebSettings.IsMaxStandAlone
                             ? string.Format("{0}?service={1}/merchandising/default.aspx", 
                             WebSettings.CASLogoutUrl, baseUrl) :
                              string.Format("{0}?service={1}/login",
                             WebSettings.CASLogoutUrl, baseUrl);

            
            Response.Redirect(url, true);
        }
    }
}