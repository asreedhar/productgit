﻿using System;
using System.Reflection;
using System.Web.Optimization;
using Autofac.Integration.Mvc;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Web;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.Common.PhotoServices;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.DomainModel.Oltp;
using FirstLook.LotIntegration.DomainModel;
using FirstLook.Merchandising.DomainModel.Vehicles;
using log4net.Config;
using MAX.BuildRequest;
using MAX.Caching;
using MAX.ExternalCredentials;
using MAX.FirstLookServicesApiClient;
using MAX.Market;
using VehicleTemplatingDomainModel;
using Wanamaker.WebApp.Areas.WanamakerGet.Controllers;
using Wanamaker.WebApp.Helpers;
using log4net;
using Merchandising.Messages;
using MerchandisingRegistry;
using MvcMiniProfiler;
using ICache = FirstLook.Common.Core.ICache;
using Module = FirstLook.Fault.DomainModel.Module;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;

namespace Wanamaker.WebApp
{
    public class Global : HttpApplication, IContainerProviderAccessor
    {
        private static ILog _log;
        private static IContainerProvider _containerProvider;

        protected void Application_Start(object sender, EventArgs e)
        {
            // Setup logging
            XmlConfigurator.Configure();
            _log = LogManager.GetLogger(typeof(Global).FullName);

            _log.InfoFormat("Application_Start() for Wanamaker.WebApp version {0} has been called.", Assembly.GetExecutingAssembly().GetName().Version);
            AreaRegistration.RegisterAllAreas();

            BundleConfig.RegisterBundles(BundleTable.Bundles);

            RegisterRoutes(RouteTable.Routes);

            _log.InfoFormat("MVC Routing setup complete.");

            // Setup injection.
            var builder = new ContainerBuilder();

            // register autofac modules here.  No silly Core.Registry stuff in this project please.

            // MVC 3 Registration
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterModelBinders(Assembly.GetExecutingAssembly());
            //Need to upgrade Autofac for Filter dependency injection
            //builder.RegisterFilterProvider();
            builder.RegisterModelBinderProvider();

            builder.RegisterModule(new CoreModule());
            builder.RegisterModule(new OltpModule());
            builder.RegisterModule(new PhotoServicesModule());

            builder.RegisterModule( new MerchandisingDomainModule() );
            builder.RegisterModule( new WebAppModule() );
            builder.RegisterModule( new MerchandisingMessagesModule()) ;

            builder.RegisterModule(new LotIntegrationServiceModule());

            builder.RegisterModule(new ExternalCredentialsModule());
			builder.RegisterModule(new BuildRequestModule());

            builder.RegisterModule(new FirstLookServicesApiModule());

            builder.RegisterType<DotnetMemoryCacheWrapper>().As<ICache>().SingleInstance();
            builder.RegisterType<WebContext>().As<IWebContext>();

            builder.RegisterType<VehicleTemplatingDataAccess>().As<IVehicleTemplatingDataAccess>();

            builder.RegisterType<DiscountPricingCampaignManager>().As<IDiscountPricingCampaignManager>();

	        builder.RegisterType<NullCacheWrapper>().As<ICacheWrapper>().AsSelf();
            builder.RegisterType<MemcachedClientWrapper>().As<ICacheWrapper>().AsSelf();
			builder.RegisterType<CacheKeyBuilder>().As<ICacheKeyBuilder>();

			// Do not cache pricing data in the UI.
			builder.Register(ctx => new NewCarPricingRepository(
				ctx.Resolve<ICacheKeyBuilder>(),
				ctx.Resolve<NullCacheWrapper>()
			));

            builder.RegisterModule(new MaxMarketModule());

            IContainer container = builder.Build();
            _containerProvider = new ContainerProvider( container );

            // Tell the registry about the container.   
            Registry.RegisterContainer( container );

            //Added for fault reporting (health montioring) integration

            // Set MVC's factory to AutoFac
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            IRegistry registry = RegistryFactory.GetRegistry();

            registry.Register<Module>();

            registry.Register<IDataSessionManager, DataSessionManager>(ImplementationScope.Shared);

        }
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.Add("OldHomeRedirect", 
                new Route("Workflow/Home.aspx", new RedirectRouteHandler("~/Workflow/Inventory.aspx", true)));
        }

        // Used for HTTP modules in Autofac.Integration.Web
        IContainerProvider IContainerProviderAccessor.ContainerProvider
        {
            get { return _containerProvider; }
        } 

        protected void Session_Start(object sender, EventArgs e)
        {
            _log.Debug("Session starting");
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            _log.Debug("Request beginning.");
        }

        protected void Application_EndRequest()
        {
            MiniProfiler.Stop();
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            _log.Debug("Authenticating request.");
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            // Get the error details
            HttpException lastErrorWrapper = Server.GetLastError() as HttpException;
            if (lastErrorWrapper != null)
            {
                Exception lastError = lastErrorWrapper;
                if (lastErrorWrapper.InnerException != null)
                    lastError = lastErrorWrapper.InnerException;

                string lastErrorTypeName = lastError.GetType().ToString();
                string lastErrorMessage = lastError.Message;
                string lastErrorStackTrace = lastError.StackTrace;

                _log.ErrorFormat(
                    "An unhandled exception has bubbled up to Global. LastServerError : [{0}] ({1}) {2}. The sender is {3} and the EventArgs are {4}.",
                    lastErrorTypeName, lastErrorMessage, lastErrorStackTrace, StandardObjectDumper.Write(sender, 1), StandardObjectDumper.Write(e, 1));
            }
            else
            {
                _log.ErrorFormat(
                    "An unhandled exception has bubbled up to Global.  The sender is {0} and the EventArgs are {1}.",
                    StandardObjectDumper.Write(sender, 1), StandardObjectDumper.Write(e, 1));
            }

            if (IsErrorPage(HttpContext.Current))
            {
                Server.ClearError();
            }
        }

        private bool IsErrorPage(HttpContext current)
        {
            return (current.Request.Url.ToString().ToLower().Contains("errorpage.aspx"));

        }

        protected void Session_End(object sender, EventArgs e)
        {
            _log.Debug("Session ending.");
        }

        protected void Application_End(object sender, EventArgs e)
        {
            _log.InfoFormat("Application_End() for Wanamaker.WebApp version {0} has been called.", Assembly.GetExecutingAssembly().GetName().Version);
        }
    }
}