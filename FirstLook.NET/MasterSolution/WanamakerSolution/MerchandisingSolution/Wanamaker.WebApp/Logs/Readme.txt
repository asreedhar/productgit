﻿This is where log4net log files will be written.  The IIS_WPG (in IIS 6) or IIS_IUSERS (in IIS 7) group must have sufficient permissions to
create, read, and write files in this folder.  In integration, we tested with Full Control.

See: http://stackoverflow.com/questions/1316039/why-is-log4net-not-creating-log-file-in-production