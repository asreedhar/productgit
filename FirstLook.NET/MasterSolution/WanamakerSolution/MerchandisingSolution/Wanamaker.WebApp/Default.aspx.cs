using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Workflow;
using Wanamaker.WebApp.AppCode;
using Wanamaker.WebApp.AppCode.AccessControl;

namespace Wanamaker.WebApp
{

    public partial class _Default : BasePage
    {
        private const string DealerToken = "DEALER_SYSTEM_COMPONENT";

        private const string InventoryUrl = "~/Workflow/Inventory.aspx";
        private const string DashboardUrl = "~/Dashboard.aspx";
        private const string GroupDashboardUrl = "~/GroupDashboard.aspx";
        private const string MobileInventoryUrl = "Mobile/Home.aspx";

        private const string SellerUrl = "~/DealerChooser.aspx";
        private const string MobileSellerUrl = "Mobile/DealerSelector.aspx";

        private const string CertifiedPreownedUrl = "~/Certified.aspx";

        private const string ErrorUrl = "~/ErrorPage.aspx?Error=410";

        private bool IsAdministrator;
        private bool IsReportViewer;

        private List<BusinessUnit> _DealerGroups;

        // list of dealers this login has access to for the chosen group id .
        private Dictionary<int,List<BusinessUnit>> _Dealers = new Dictionary<int,List<BusinessUnit>>();

        private bool HasMultipleDealerGroups
        {
            get { return (DealerGroups != null && DealerGroups.Count > 1); }
        }

        public List<BusinessUnit> DealerGroups
        {
            get
            {
                if (_DealerGroups == null)
                {
                    _DealerGroups = (List<BusinessUnit>)
                          BusinessUnitFinder.Instance().FindAllDealerGroupsForMember(MemberFinder.Instance().FindByUserName(User.Identity.Name), string.Empty);
                }
                return _DealerGroups;
            }
        }

        /// <summary>
        /// A Dictionary of lists of DealerGroups/Dealers this login has access to
        /// </summary>
        public Dictionary<int, List<BusinessUnit>> Dealers
        {
            get
            {
                if (_Dealers.Count == 0)
                {
                    if (DealerGroups.Count > 0)
                    {
                        DealerGroups.ForEach(dg =>
                        {
                            _Dealers.Add(dg.Id,
                                (List<BusinessUnit>)BusinessUnitFinder.Instance().FindAllDealershipsByDealerGroupAndMember(
                                    dg,
                                    MemberFinder.Instance().FindByUserName(User.Identity.Name)
                                )
                            );
                        });
                    }
                }
                return _Dealers;
            }
        }

        private bool GroupHasMultipleDealers(int parentGroupId)
        {
            bool result = false;
            if(Dealers.ContainsKey(parentGroupId))
                result = Dealers[parentGroupId].Count > 1;

            return result;
        }

        protected void Page_Init( object sender, EventArgs e )
        {
            Response.Cache.SetExpires( DateTime.UtcNow.AddMinutes( -1 ) );
            Response.Cache.SetCacheability( HttpCacheability.NoCache );
            Response.Cache.SetNoStore();
        }

        protected void Page_Load( object sender, EventArgs e )
        {
            IsAdministrator = Page.User.IsInRole( "Administrator" );
            IsReportViewer = Page.User.IsInRole( "Report Viewer" ) || IsAdministrator;

            // pass on knowlegde of coming from max. this is an example of how NOT to do something.

            if (!string.IsNullOrEmpty(Request.QueryString["HALSESSIONID"]))
            {
                if (Request.Cookies["_session_id"] == null)
                {
                    Response.Cookies.Add(new HttpCookie("_session_id", Request.QueryString["HALSESSIONID"]));
                }
            }
            // route the user to the correct page

            bool forbidden = true;

            string token = Request.QueryString["token"];
            string name = Context.User.Identity.Name;

            MembershipUser user = Context.Items[IdentityHelper.MembershipContextKey] as MembershipUser;
            if (user != null)
            {
                //use name to find our mapped user:
                if (!string.IsNullOrEmpty(token) && token.Equals(DealerToken))
                {
                    forbidden = EnterDealerTool();
                }
                else if ((Roles.IsUserInRole(user.UserName, "Administrator") || HasMultipleDealerGroups) && Request.QueryString["ref"] != "GLD")  
                {
                    forbidden = EnterSellerTool();
                }
                else
                {
                    forbidden = SetupDealerAndEnterDealerTool();
                }
            }
            if (forbidden)
            {
                Response.StatusCode = 403;
                Response.Status = "403 Forbidden";
                throw new HttpException(403, "User is not authorized for MAX AD");
            }

        }

        

        private bool EnterCertifiedPreownedTool()
        {
            //need to get manufacturer type here somehow...
            Response.Redirect(CertifiedPreownedUrl);

            return false;
        }
        private bool SetupDealerAndEnterDealerTool()
        {
            BusinessUnit dealer = Context.Items[BusinessUnit.HttpContextKey] as BusinessUnit;

            if (dealer == null || dealer.BusinessUnitType == BusinessUnitType.DealerGroup)
            {
                if (dealer == null) //if no dealer in context...get the groups
                {
                    if (DealerGroups != null && DealerGroups.Count > 0)
                    {
                        dealer = DealerGroups[0];
                    }
                    else
                    {
                        //no dealer groups to choose from...
                        return false;
                    }

                }

                if (dealer.BusinessUnitType == BusinessUnitType.DealerGroup)
                {
                    var member = MemberFinder.Instance().FindByUserName(User.Identity.Name);
                    List<BusinessUnit> dealerList =
                        (List<BusinessUnit>)
                        BusinessUnitFinder.Instance().FindAllDealersForMemberAndDealerGroup(member, dealer.Id, string.Empty);
                    if (dealerList.Count > 0)
                    {
                        SetupSoftwareSystemComponentState(dealerList[0]);
                        dealer = dealerList[0];
                    }
                    else
                    {
                        return false; //no dealer
                    }
                }
            }
            return EnterDealerTool(dealer);
        }
        private bool EnterDealerTool()
        {
            BusinessUnit dealer = Context.Items[BusinessUnit.HttpContextKey] as BusinessUnit;
            return EnterDealerTool(dealer);
        }
        private bool EnterDealerTool(BusinessUnit dealer)
        {


            if (dealer != null)
            {
                if (Request.Browser.IsMobileDevice)
                {
                    Response.Redirect(MobileInventoryUrl);
                }
                else if (WebSettings.IsMaxStandAlone && IsGroupDashboardEnabled && IsReportViewer && GroupHasMultipleDealers(WorkflowState.BusinessUnit.DealerGroup().Id) && Request.QueryString["ref"] != "GLD")
                {
                    Response.Redirect(GroupDashboardUrl);
                }
                //We are showing the Dashboard only for some dealers that is controlled by a web setting
                else if (IsDashboardEnabled && IsReportViewer)
                {
                    Response.Redirect(DashboardUrl);
                }
                else
                {
                    Response.Redirect(InventoryUrl);
                }
            }

            return false;
        }

        private readonly Lazy<bool> IsDashboardEnabledLazy = 
            new Lazy<bool>(() => GetMiscSettings.GetSettings(WorkflowState.BusinessUnitID).ShowDashboard);

        private bool IsDashboardEnabled { get { return IsDashboardEnabledLazy.Value; } }

        private readonly Lazy<bool> IsGroupDashboardEnabledLazy =
            new Lazy<bool>(() => GetMiscSettings.GetSettings(WorkflowState.BusinessUnitID).ShowGroupLevelDashboard);

        private bool IsGroupDashboardEnabled { get { return IsGroupDashboardEnabledLazy.Value; } }

        private bool EnterSellerTool()
        {
            if (Request.Browser.IsMobileDevice)
            {
                Response.Redirect(MobileSellerUrl);
            }
            else
            {
                Response.Redirect(SellerUrl);
            }

            return false;
        }

        private void SetupSoftwareSystemComponentState(BusinessUnit dealer)
        {
            SoftwareSystemComponentState state = LoadState(DealerToken);
            state.DealerGroup.SetValue(dealer.DealerGroup());
            state.Dealer.SetValue(dealer);
            state.Member.SetValue(null);
            state.Save();
        }
        private SoftwareSystemComponentState LoadState(string componentToken)
        {
            SoftwareSystemComponentState state = SoftwareSystemComponentStateFacade.FindOrCreate(User.Identity.Name, componentToken);

            if (state == null)
            {
                Member member = MemberFinder.Instance().FindByUserName(User.Identity.Name);

                SoftwareSystemComponent component = SoftwareSystemComponentFinder.Instance().FindByToken(componentToken);

                state = new SoftwareSystemComponentState();
                state.AuthorizedMember.SetValue(member);
                state.SoftwareSystemComponent.SetValue(component);
            }

            return state;
        }

    }
}