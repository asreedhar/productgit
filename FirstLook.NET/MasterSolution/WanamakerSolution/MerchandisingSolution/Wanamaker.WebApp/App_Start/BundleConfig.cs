﻿using System.Web.Optimization;

namespace Wanamaker.WebApp
{
    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            // {version} is a type of regex and will grab multiple versions of jQuery if they are in the same path
            // example: "~/Scripts/jquery-{version}.js"
            //

            // Scripts
            //
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Public/Scripts/Lib/jquery-1.11.2.js",

                "~/Public/Scripts/Lib/jquery-ui-1.11.3.js",
                "~/Public/Scripts/Lib/knockout-3.3.0.js"));

            bundles.Add(new ScriptBundle("~/bundles/pricing").Include(
                "~/Public/Scripts/PricingAnalysis/Base.js",
                "~/Public/Scripts/PricingAnalysis/Refinements.js",
                "~/Public/Scripts/PricingAnalysis/Gauge.js",
                "~/Public/Scripts/PricingAnalysis/MarketListings/grid.locale-en.js",
                "~/Public/Scripts/PricingAnalysis/MarketListings/jquery.jqGrid.src.js",
                "~/Public/Scripts/PricingAnalysis/MarketListings/marketlistings.js",
                "~/Public/Scripts/PricingAnalysis/VehicleHistoryScript.js",
                "~/Public/Scripts/PricingAnalysis/FacetsAndFilters.js",
                "~/Public/Scripts/PricingAnalysis/PricingComparison.js",
                "~/Public/Scripts/PricingAnalysis/PricingCalculator.js",
                "~/Public/Scripts/PricingAnalysis/raphael-min.js",
                "~/Public/Scripts/PricingAnalysis/drag-dealer.js",
                "~/Public/Scripts/PricingAnalysis/MarketPricingDto.js"));


            // Bundle: jQueryCombined
            // Referenced in Workflow.Master
            bundles.Add(new ScriptBundle("~/Public/Scripts/jQueryCombined").Include(
                "~/Public/Scripts/Lib/json2.js",
                "~/Public/Scripts/Lib/jquery-1.8.2.min.js",
                "~/Public/Scripts/Lib/jquery-ui-1.9.0.custom.min.js",
                "~/Public/Scripts/Lib/jquery.cookie.js",
                "~/Public/Scripts/Lib/jquery.tmpl.js",
                "~/Public/Scripts/Lib/jquery.tablesorter.min.js",
                "~/Public/Scripts/Lib/jquery.mCustomScrollbar.min.js",
                "~/Public/Scripts/Lib/jquery.mousewheel.min.js",
                "~/Public/Scripts/Lib/jquery.simpleplaceholder.min.js",
                "~/Public/Scripts/Lib/lodash.underscore.min.js",
                "~/Public/Scripts/Lib/date.js",
                "~/Public/Scripts/Lib/knockout-3.0.0beta.min.js",
                "~/Public/Scripts/Lib/knockout.mapping-2.4.1.min.js",
                "~/Public/Scripts/Lib/knockout-deferred-updates.js",
                "~/Public/Scripts/Lib/highcharts-4.1.7.src.js"));

            bundles.Add(new ScriptBundle("~/bundles/CTRGraph").Include(
                "~/Public/Scripts/PricingAnalysis/MarketListings/jquery.jqGrid.src.js",
                "~/Public/Scripts/Lib/jquery-1.8.2.js",
                "~/Public/Scripts/Lib/jquery-ui-1.9.0.custom.js",
                "~/Public/Scripts/Lib/underscore-min.js",
                "~/Public/Scripts/Lib/json2.js",
                "~/Public/Scripts/Lib/jquery.cookie.js",
                "~/Public/Scripts/Lib/jquery.tmpl.js",
                "~/Public/Scripts/Lib/jquery.dataTables.min.js",
                "~/Public/Scripts/Lib/lodash.underscore.min.js",
                "~/Public/Scripts/Lib/knockout-3.0.0beta.min.js",
                "~/Public/Scripts/Lib/highcharts-3.0.9.js",
                "~/Public/Scripts/MAX.Utils.debug.js",
                "~/Public/Scripts/Max/MAX.UI.Global.js",
                "~/Public/Scripts/ctr-graph.js",
                "~/Public/Scripts/PricingAnalysis/CTRGraphGen.js"));

            bundles.Add(new ScriptBundle("~/bundles/PWS").Include(
                "~/Public/Scripts/PricingAnalysis/PrintWindowSticker.js"));

            bundles.Add(new ScriptBundle("~/Scripts/Mileage").Include(
                "~/Public/Scripts/PricingAnalysis/MileageSlider.js",
                "~/Public/Scripts/PricingAnalysis/DefineCompetitiveSet.js",
                "~/Public/Scripts/PricingAnalysis/SearchCriteria.js"));

            bundles.Add(new ScriptBundle("~/Scripts/PricingSummary").Include(
                "~/Public/Scripts/PricingSummary/jquery-migrate-1.2.1.min.js",
                "~/Public/Scripts/PricingSummary/grid.locale-en.js",
                "~/Public/Scripts/PricingAnalysis/MarketListings/jquery.jqGrid.min.js",
                "~/Public/Scripts/PricingSummary/jquery.jqplot.min.js",
                "~/Public/Scripts/PricingSummary/jqplot.barRenderer.min.js",
                "~/Public/Scripts/PricingSummary/jqplot.categoryAxisRenderer.min.js",
                "~/Public/Scripts/PricingSummary/jqplot.pointLabels.min.js",
                "~/Public/Scripts/PricingSummary/UpdatePriceSummary.js",
                "~/Public/Scripts/PricingSummary/PriceSummary.js",
                "~/Public/Scripts/PricingSummary/InventoryGrid.js",
                "~/Public/Scripts/PricingSummary/PricingSummaryArgument.js",
                "~/Public/Scripts/Lib/highcharts-3.0.9.js" // this needs to be pulled out into a common bundle (used in CTRGraph bundle as well, un-necessary download size)
            ));

            bundles.Add(new ScriptBundle("~/bundles/crazyegg")
                .Include("~/Public/Scripts/CrazyEgg/CrazyEgg.js"));

            // Styles
            //
            bundles.Add(new StyleBundle("~/content/css").Include(
                "~/Public/Style/site.css"));

            bundles.Add(new StyleBundle("~/content/jqueryui_css").Include(
                "~/Public/Style/jquery-ui.css",
                "~/Public/Style/jquery-ui.structure.css",
                "~/Public/Style/jquery-ui.theme.css"));

            bundles.Add(new StyleBundle("~/content/pricing_css").Include(
                "~/Public/Style/vehicle-history.css",
                "~/Public/Style/mileage-slider.css",
                "~/Public/Style/MarketListings/ui.jqgrid.css",
                "~/Public/Style/MarketListings/grid-style.css",
                "~/Public/Style/inventory-header.css",
                "~/Public/Style/filter.css",
                "~/Public/Style/ping-layout.css",
                "~/Public/Style/define-competitive-set.css",
                "~/Public/Style/pricing-proof.css",
                "~/Public/Style/pricing-calculator.css",
                "~/Themes/MAX3.0/Main.debug.css",
                "~/Public/Style/gauge-component.css",
                "~/Themes/MAX3.0/Workflow.debug.css",
                "~/Themes/MAX3.0/Pricing.debug.css",
                "~/Public/Style/WorkflowHeader.css",
                "~/Public/Style/pws.css"));

            bundles.Add(new StyleBundle("~/content/pricingSummary_css").Include(
                "~/Themes/MAX3.0/Main.debug.css",
                "~/Themes/MaxMvc/DashboardKO.debug.css",
                "~/Public/Style/PricingSummary/summary-layout.css",
                "~/Public/Style/PricingSummary/bar-chart.css",
                "~/Public/Style/PricingSummary/InventoryGrid.css",
                "~/Public/Style/PricingSummary/ui.jqgrid.css"));
        }
    }

}