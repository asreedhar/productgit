using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FirstLook.DomainModel.Oltp;

namespace Wanamaker.WebApp.Controls
{

    public partial class BusinessUnitUserControl : System.Web.UI.UserControl
    {
        private int _businessUnitId;

        public int BusinessUnitID
        {
            get
            {
                if (_businessUnitId <= 0)
                {
                    BusinessUnit bu = Context.Items["BusinessUnit"] as BusinessUnit;
                    if (bu == null)
                    {
                        Response.Redirect("~/Default.aspx");
                    }
                    _businessUnitId = bu.Id;
                }
                return _businessUnitId;
            }
        }

        protected void OverrideBusinessUnitId(int businessUnitId)
        {
            if (Context.User.IsInRole("Administrator"))
            {
                _businessUnitId = businessUnitId;
            }
        }

        protected virtual void Page_Load(object sender, EventArgs e) { }
    }
}