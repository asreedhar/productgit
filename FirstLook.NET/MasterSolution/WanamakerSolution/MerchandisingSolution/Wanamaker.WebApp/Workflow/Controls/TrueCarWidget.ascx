<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TrueCarWidget.ascx.cs" Inherits="Wanamaker.WebApp.Workflow.Controls.TrueCarWidget" %>
<%@ Register TagPrefix="carbuilder" TagName="BusinessUnitUserControl" Src="~/Controls/BusinessUnitUserControl.ascx"  %>

<asp:MultiView ID="TrueCarView" runat="server">
<asp:View ID="needsTrimView" runat="server">
    
    <div style="padding:40px;">
        <div style="background:#F0F0F0;border:1px solid #AAAAAA;margin:0;padding:20px;width:350px;text-align:center;">
        <p style="margin-bottom:15px;border-bottom: solid 1px #aaa;">Select a trim to see TrueCar Pricing</p>
        <p>Trim: 
        <asp:DropDownList ID="StyleDDL" 
                    runat="server" 
                    DataSourceID="TrimStyleDS" 
                    AutoPostBack="true" 
                    DataTextField="Value" 
                    DataValueField="Key"
                    OnSelectedIndexChanged="Style_Changed" 
                    AppendDataBoundItems="true"
                    >
                    <asp:ListItem Text="Select trim..." Value="-1" />
                    </asp:DropDownList>
        </p>
        </div>
    </div>
    
    <asp:ObjectDataSource runat="server" ID="TrimStyleDS" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.ChromeStyle"  
    OnSelecting="Style_Selecting" SelectMethod="ChromeTrimStyleNamesSelect">
    <SelectParameters>
    
        <asp:Parameter Name="vin" Type="String" DefaultValue='' />
        
    </SelectParameters>
    </asp:ObjectDataSource>

</asp:View>
<asp:View ID="TureCarView" runat="server">
    <div>
      <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="573" height="684" id="tc_widget" name="tc_widget">
        <param name="movie" value="http://www.truecar.com/widgets/dealer/tc_dealer.swf" />
        <param name="wmode" value="opaque" />
        <param name="bgcolor" value="#ffffff" />
        <param name="allowscriptaccess" value="always" />
        <asp:Literal ID="flashVars1" runat="server" ></asp:Literal>
        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="http://www.truecar.com/widgets/consumer/ dealer/tc_dealer.swf" width="573" height="684">
          <param name="wmode" value="opaque" />
          <param name="bgcolor" value="#ffffff" />
          <param name="allowscriptaccess" value="always" />
          <asp:Literal ID="flashVars2" runat="server" ></asp:Literal>          
        <!--<![endif]-->
          <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
          <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></p>
        <!--[if !IE]>-->
        </object>
        <!--<![endif]-->
      </object>
    </div>
</asp:View>
</asp:MultiView>
