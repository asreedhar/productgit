﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Marketing.Commands;
using FirstLook.Merchandising.DomainModel.Marketing.ValueAnalyzer;
using FirstLook.Merchandising.DomainModel.Workflow;

namespace Wanamaker.WebApp.Workflow.Controls
{
    public class WorkflowBaseUserControl : UserControl
    {
        public int BusinessUnitId
        {
            get { return WorkflowState.BusinessUnitID; }
        }

        public int InventoryId
        {
            get { return WorkflowState.InventoryId; }
        }

        public BusinessUnit BusinessUnit
        {
            get { return WorkflowState.BusinessUnit; }
        }

        public string BulkUploadUrl
        {
            get
            {
                return BusinessUnit != null ? PhotoService.GetBulkUploadManagerUrl( BusinessUnit.Id, PhotoManagerContext.MAX ) : "";
            }
        }

        public static bool HasMarketingUpgrade
        {
            get
            {
                return WorkflowState.BusinessUnit.HasDealerUpgrade( Upgrade.Marketing );
            }
        }

        public IPhotoServices PhotoService { get; set; }

        public string GetMaxForWebsiteUrl()
        {
            var preference = GetValueAnalyzerPreference( InventoryId );
            return preference.LongUrl;
        }

        public ValueAnalyzerVehiclePreferenceTO GetValueAnalyzerPreference ( int inventoryId )
        {
            PricingContext context = PricingContext.FromMerchandising( WorkflowState.BusinessUnitID, inventoryId );

            var command = new GetValueAnalyzerVehiclePreference( context.OwnerHandle, context.VehicleHandle );
            AbstractCommand.DoRun( command );
            return command.Preference;
        }
    }
}