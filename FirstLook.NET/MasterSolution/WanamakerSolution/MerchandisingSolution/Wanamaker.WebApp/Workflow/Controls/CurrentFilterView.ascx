﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CurrentFilterView.ascx.cs" Inherits="Wanamaker.WebApp.Workflow.Controls.CurrentFilterView" %>
<%@ Register TagPrefix="cwc" TagName="AgeFilterView" Src="~/Workflow/Controls/AgeFilterView.ascx" %>

<asp:Repeater ID="Segments" runat="server">
    <ItemTemplate><span class="segment"><%# Container.DataItem %> &gt; </span></ItemTemplate>
</asp:Repeater>
<asp:Label runat="server" CssClass="range" ID="AgeBucket" />
<asp:Label runat="server" CssClass="count" ID="Count" />
<cwc:AgeFilterView ID="AgeFilter" runat="server" />

<asp:Label runat="server" ID="SearchText" Visible="false" />
<asp:LinkButton runat="server" OnClick="OnResetSearchClicked" ID="ResetSearchLink" Visible="false">Clear Search</asp:LinkButton>

