﻿using System;

namespace Wanamaker.WebApp.Workflow.Controls
{
    public partial class SearchInventory : WorkflowBaseUserControl
    {
        private const string HeaderSearchTextKey = "HeaderSearchText";

        /// <summary>
        /// A list of contextual actions available when searching for inventory.
        /// </summary>
        public enum SearchType
        {
            CurrentPage,
            HomePage
        }

        /// <summary>
        /// The action to perform upon performing an inventory search (Defaults to HomePage).
        /// </summary>
        public SearchType SearchAction
        {
            get
            {
                if ( ViewState[ "HeaderSearchAction" ] != null )
                {
                    return ( SearchType )ViewState[ "HeaderSearchAction" ];
                }

                ViewState[ "HeaderSearchAction" ] = SearchType.HomePage;
                return SearchType.HomePage;

            }
            set
            {
                ViewState[ "HeaderSearchAction" ] = value;
            }
        }

        /// <summary>
        /// The search keywords entered into the textbox by the user.
        /// </summary>
        public string SearchText
        {
            get { return (string)ViewState[HeaderSearchTextKey] ?? ""; }
            set
            {
                if (SearchText == value) return;
                ViewState[HeaderSearchTextKey] = value;
                HeaderSearchText.Text = value;
            }
        }

        protected void OnSearchClicked ( object sender, EventArgs e )
        {
            if (SearchText == HeaderSearchText.Text) return;
            SearchText = HeaderSearchText.Text;
            OnInventorySearching(EventArgs.Empty);
        }

        //Custom Events
        public event EventHandler InventorySearching;

        protected virtual void OnInventorySearching ( EventArgs e )
        {
            if ( InventorySearching != null )
                InventorySearching( this, e );
        }
    }
}