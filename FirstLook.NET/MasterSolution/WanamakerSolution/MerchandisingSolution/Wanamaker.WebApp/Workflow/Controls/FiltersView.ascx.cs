﻿using System;
using System.Text;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.Workflow.Presenter;
using MAX.Entities;

namespace Wanamaker.WebApp.Workflow.Controls
{
    public partial class FiltersView : System.Web.UI.UserControl, IFiltersView
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<FiltersView>();

        public IFiltersPresenter Presenter { get; private set; }

        protected IFiltersViewModel ViewModel { 
            get { return Presenter.GetViewModel(); }
        }

        protected override void OnInit(EventArgs e)
        {
            Presenter = new FiltersPresenter(this, (IFiltersModel) Page);
            Presenter.Initialize();
            base.OnInit(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            Presenter.Load();
            base.OnLoad(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            Presenter.PreRender();
            AuxFiltersRepeater.DataSource = ViewModel.AuxilaryFilters;
            AuxFiltersRepeater.DataBind();
            HighLevelFilters.DataSource = ViewModel.HighLevelFilters;
            HighLevelFilters.DataBind();
            base.OnPreRender(e);
        }
 
        protected void HighLevelItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var item = e.Item;
            if(item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                BindInnerHierarchicalRepeater(item);
            }
        }

        private static void BindInnerHierarchicalRepeater(RepeaterItem item)
        {
            var filtersRepeater = (Repeater) item.FindControl("Filters");
            var highLevelFilterItem = (IHighLevelFilterItem) item.DataItem;
            filtersRepeater.DataSource = highLevelFilterItem.DetailedFilters;
            filtersRepeater.DataBind();
        }

        protected void OnWorkflowClickedCommand(object sender, CommandEventArgs e)
        {
            WorkflowType workflow;
            if(!Enum.TryParse(e.CommandName, true, out workflow))
            {
                Log.ErrorFormat("ExecAction: Failed to parse enum {0} '{1}'.",
                    typeof(WorkflowType).FullName, e.CommandName);
                return;
            }

            if (WorkflowClicked != null)
                WorkflowClicked(workflow);
        }

        protected string CssClass(bool isFirst = false, bool isSelected = false, bool noCategory = false, string staticClasses = null)
        {
            var b = new StringBuilder(32);
            AppendIf(staticClasses != null, b, staticClasses);
            AppendIf(isFirst, b, "first");
            AppendIf(isSelected, b, "selected");
            AppendIf(noCategory, b, "noCategory");
            return b.ToString();
        }

        private static void AppendIf(bool condition, StringBuilder b, string className)
        {
            if (!condition) return;
            if (b.Length > 0) b.Append(" ");
            b.Append(className);
        }

        public Action<WorkflowType> WorkflowClicked { get; set; }

        protected static string GoogleAnalytics(string category, WorkflowType type)
        {
            return string.Format(
                "_gaq.push(['_trackEvent', 'Filters', 'Click', '{0}: {1}']);",
                category, type);
        }
    }
}