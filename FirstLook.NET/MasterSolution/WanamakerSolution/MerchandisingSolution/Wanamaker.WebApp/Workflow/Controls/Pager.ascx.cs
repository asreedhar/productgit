﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Logging;

namespace Wanamaker.WebApp.Workflow.Controls
{
    internal struct PageInfo
    {
        public int Start { get; private set; }
        public int Size { get; private set; }

        public PageInfo ( int start, int size )
            : this()
        {
            Start = start;
            Size = size;
        }

        public bool Equals ( PageInfo other )
        {
            return other.Start == Start && other.Size == Size;
        }

        public override bool Equals ( object obj )
        {
            if ( ReferenceEquals( null, obj ) ) return false;
            if ( obj.GetType() != typeof( PageInfo ) ) return false;
            return Equals( ( PageInfo )obj );
        }

        public override int GetHashCode ()
        {
            unchecked
            {
                return ( Start * 397 ) ^ Size;
            }
        }
    }

    internal class PageItem
    {
        public int PageId { get; private set; }
        public int PageNum { get; private set; }

        public PageItem ( int pageId, int pageNum )
        {
            PageId = pageId;
            PageNum = pageNum;
        }
    }

    public partial class Pager : System.Web.UI.UserControl
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<Pager>();

        private const int DefaultPageSize = 20;

        internal PageInfo GetCurrentPageInfo ()
        {
            return ShowAll
                ? new PageInfo( 0, ItemCount )
                : new PageInfo( PageNumber * PageSize, Math.Min( ItemCount - ( PageNumber * PageSize ), PageSize ) );
        }

        public void ResetPageNumberToZero ()
        {
            PageNumber = 0;
        }

        public int PageSize
        {
            get
            {
                return DefaultPageSize;
            }
        }

        public int PageNumber
        {
            get { return ( int? )ViewState[ "pageNum" ] ?? 0; }
            set
            {
                if ( Log.IsDebugEnabled )
                    Log.DebugFormat( "Setting PageNumber to {0}", value );
                ViewState[ "pageNum" ] = value;
            }
        }

        public int ItemCount
        {
            get { return ( int? )ViewState[ "itemCount" ] ?? 0; }
            set
            {
                if ( Log.IsDebugEnabled )
                    Log.DebugFormat( "Setting ItemCount to {0}", value );
                ViewState[ "itemCount" ] = value;
            }
        }

        public bool ShowAll
        {
            get { return ( bool? )ViewState[ "showAll" ] ?? false; }
            set
            {
                if ( Log.IsDebugEnabled )
                    Log.DebugFormat( "Setting ShowAll to {0}", value );
                ViewState[ "showAll" ] = value;
            }
        }

        public int PageNumberCount
        {
            get { return ShowAll ? 0 : ( ItemCount + PageSize - 1 ) / PageSize; }
        }

        public int NumberOfFirstItemOnPage
        {
            get { return GetCurrentPageInfo().Start + 1; }
        }

        public int NumberOfLastItemOnPage
        {
            get
            {
                var info = GetCurrentPageInfo();
                return info.Start + info.Size;
            }
        }

        protected override void OnPreRender ( EventArgs e )
        {
            base.OnPreRender( e );

            if (PageNumber >= PageNumberCount)
                PageNumber = Math.Max(0, PageNumberCount - 1);

            NumberOfFirstItem.Text = NumberOfFirstItemOnPage.ToString( CultureInfo.InvariantCulture );
            NumberOfLastItem.Text = NumberOfLastItemOnPage.ToString( CultureInfo.InvariantCulture );
            TotalItemCount.Text = ItemCount.ToString( CultureInfo.InvariantCulture );

            var showAll = ShowAll;
            ShowAllItems.Visible = !showAll && ItemCount > PageSize;
            UsePaging.Visible = showAll && ItemCount > PageSize;

            PageNumbers.DataSource =
                Enumerable
                    .Range( 0, PageNumberCount )
                    .Select( i => new PageItem( i, i + 1 ) )
                    .ToArray();
            PageNumbers.DataBind();

            PageNumbers.Visible = ItemCount > PageSize;

            CheckIfPageInfoChanged();
        }

        protected void ShowAll_Click ( object sender, EventArgs e )
        {
            ShowAll = true;
        }

        protected void UsePaging_Click ( object sender, EventArgs e )
        {
            ShowAll = false;
            ResetPageNumberToZero();
        }

        protected void PageClicked ( object sender, CommandEventArgs e )
        {
            if ( !( e.CommandArgument is string ) ) return;

            int num;
            int.TryParse( ( string )e.CommandArgument, out num );
            PageNumber = num;
        }

        private PageInfo lastPageInfo;
        private void CheckIfPageInfoChanged ()
        {
            var currentPageInfo = GetCurrentPageInfo();
            if ( !currentPageInfo.Equals( lastPageInfo ) )
                OnPageInfoChanged();
            lastPageInfo = currentPageInfo;
        }

        private void OnPageInfoChanged ()
        {
            PageNumberInfoChanged( this, EventArgs.Empty );
        }

        public event EventHandler PageNumberInfoChanged = ( sender, args ) => { };

        protected static string GoogleAnalytics ( string category, string action, string label )
        {
            return string.Format( "_gaq.push(['_trackEvent', '{0}', '{1}', '{2}']);",
                                 category, action, label );
        }
    }
}