﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FiltersView.ascx.cs" Inherits="Wanamaker.WebApp.Workflow.Controls.FiltersView" %>
<%@ Import Namespace="FirstLook.Merchandising.DomainModel.Workflow.Presenter" %>
<%@ Import Namespace="MAX.Entities" %>
<%@ Import Namespace="Merchandising.Messages" %>

<div id="AuxFilters" class="clearfix">

    <h2>Show:</h2>
    <asp:Repeater runat="server" ID="AuxFiltersRepeater">
        <HeaderTemplate><ul></HeaderTemplate>
        <ItemTemplate>
            <li class='<%# CssClass(isSelected: ((IFilterItem)Container.DataItem).WorkflowType == ViewModel.CurrentWorkflow) %>'>
                <asp:LinkButton runat="server"
                                OnCommand="OnWorkflowClickedCommand" CommandName='<%# Eval("WorkflowType") %>'
                                OnClientClick='<%# GoogleAnalytics("AuxilaryFilters", (WorkflowType)Eval("WorkflowType")) %>'>
                    <%# Eval("Label") %>
                </asp:LinkButton>
                (<%# Eval("Count") %>)
            </li>
        </ItemTemplate>
        <FooterTemplate></ul></FooterTemplate>
    </asp:Repeater>

</div>

<div id="MainFilters" class='<%# CssClass(staticClasses: "clearfix", noCategory: ViewModel.IsAuxiliaryFilterSelected) %>' runat="server" clientidmode="Static">

    <asp:Repeater runat="server" ID="HighLevelFilters" OnItemDataBound="HighLevelItemDataBound">
        <HeaderTemplate>
            <ul class="clearfix">
        </HeaderTemplate>
        <ItemTemplate>
            <li class='<%# CssClass(Container.ItemIndex == 0, 
                                    ((IFilterItem)Container.DataItem).WorkflowType == ViewModel.CurrentHighLevelWorkflow,
                                    staticClasses: ((IFilterItem)Container.DataItem).WorkflowType.ToString()) %>'>
                <asp:LinkButton runat="server"
                                OnCommand="OnWorkflowClickedCommand" CommandName='<%# Eval("WorkflowType") %>' 
                                OnClientClick='<%# GoogleAnalytics("HighLevelFilters", (WorkflowType)Eval("WorkflowType")) %>'>
                    <span class="icon"><span></span></span><%# Eval("Label") %> <span class="count"><%# Eval("Count") %></span>
                </asp:LinkButton>
                <div class="filterMenu">
                    <h2>Show:</h2>
                    <asp:Repeater runat="server" ID="Filters">
                        <HeaderTemplate>
                            <ul>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li class='<%# CssClass(Container.ItemIndex == 0,
                                                    ((IFilterItem)Container.DataItem).WorkflowType == ViewModel.CurrentWorkflow) %>'>
                                <asp:LinkButton runat="server"
                                                OnCommand="OnWorkflowClickedCommand" CommandName='<%# Eval("WorkflowType") %>'
                                                OnClientClick='<%# GoogleAnalytics("Filters", (WorkflowType)Eval("WorkflowType")) %>'>
                                    <%# Eval("Label") %> <span class="count">(<%# Eval("Count") %>)</span>
                                </asp:LinkButton>
                            </li>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
    </asp:Repeater>
</div>