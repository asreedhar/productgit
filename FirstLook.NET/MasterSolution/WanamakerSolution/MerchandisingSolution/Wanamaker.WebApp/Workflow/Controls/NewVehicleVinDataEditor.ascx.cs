using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Merchandising.DomainModel.VehicleVinData;


namespace Wanamaker.WebApp.Workflow.Controls
{
    public partial class NewVehicleVinDataEditor : UserControl
    {
        #region Page Variables
        private int _chromeStyleID;
        private EngineCollection _engineCollection;
        private TransmissionCollection _transmissionCollection;
        private DrivetrainCollection _drivetrainCollection;
        private FuelSystemCollection _fuelSystemCollection;
        private PrimaryEquipmentConfiguration _equipment;
        #endregion

        public List<FirstLook.Merchandising.DomainModel.Vehicles.GenericEquipment> MyGenericEquipment = new List<FirstLook.Merchandising.DomainModel.Vehicles.GenericEquipment>();

        public List<FirstLook.Merchandising.DomainModel.Vehicles.GenericEquipment> MyEquipment
        {
            get { return MyGenericEquipment; }
        }

        public void ClearDropDownLists()
        {
            EnginesDDL.Items.Clear();
            TransmissionsDDL.Items.Clear();
            DrivetrainsDDL.Items.Clear();
            FuelSystemsDDL.Items.Clear();
        }

        public void SetChromeStyle(int chromeStyleID, int inventoryID, int businessUnitID)
        {
            _chromeStyleID = chromeStyleID;

            ClearDropDownLists();

            _equipment =
                PrimaryEquipmentConfiguration.FetchVehicleConfig(businessUnitID, inventoryID)
                ?? PrimaryEquipmentConfiguration.Default;

            SubmitToDropDownLists(_chromeStyleID);

            SwitchToStandardsInDropDownLists();

            LoadSelectedValueIntoListControl(_equipment.Engine, EnginesDDL);
            LoadSelectedValueIntoListControl(_equipment.Transmission, TransmissionsDDL);
            LoadSelectedValueIntoListControl(_equipment.Drivetrain, DrivetrainsDDL);
            LoadSelectedValueIntoListControl(_equipment.Fuel, FuelSystemsDDL);
        }

        private static void LoadSelectedValueIntoListControl(EquipmentObject equipment, ListControl control)
        {
            if (equipment.TypeID != -1)
                control.SelectedValue =
                    control.Items.FindByValue(equipment.TypeID.ToString(CultureInfo.InvariantCulture)).Value;
        }

        private static void SwitchToStandard(ListControl ddl)
        {
            char[] p = {'['};
            const string SEP = "[";

            foreach (ListItem item in ddl.Items)
            {
                if (item.Text.Contains(SEP))
                {
                    var equip = item.Text.Split(p);
                    if (equip[1][0] == 'S')
                    {
                        ddl.SelectedValue = ddl.Items.FindByValue(item.Value).Value;
                    }
                }
            }
        }

        private void SwitchToStandardsInDropDownLists()
        {
            SwitchToStandard(EnginesDDL);
            SwitchToStandard(TransmissionsDDL);
            SwitchToStandard(DrivetrainsDDL);
            SwitchToStandard(FuelSystemsDDL);
        }

        protected void SubmitToDropDownLists(int chromeStyleId)
        {
            _engineCollection = EngineCollection.GetEngines(chromeStyleId);
            ViewState["engineCollection"] = _engineCollection;
            EnginesDDL.DataSource = GetListFromEquipmentCollection(_engineCollection);
            EnginesDDL.DataBind();

            _transmissionCollection = TransmissionCollection.GetTransmissions(chromeStyleId);
            ViewState["transmissionCollection"] = _transmissionCollection;
            TransmissionsDDL.DataSource = GetListFromEquipmentCollection(_transmissionCollection);
            TransmissionsDDL.DataBind();

            _drivetrainCollection = DrivetrainCollection.GetDrivetrains(chromeStyleId);
            ViewState["drivetrainCollection"] = _drivetrainCollection;
            DrivetrainsDDL.DataSource = GetListFromEquipmentCollection(_drivetrainCollection);
            DrivetrainsDDL.DataBind();

            _fuelSystemCollection = FuelSystemCollection.GetFuelSystems(chromeStyleId);
            ViewState["fuelSystemCollection"] = _fuelSystemCollection;
            FuelSystemsDDL.DataSource = GetListFromEquipmentCollection(_fuelSystemCollection);
            FuelSystemsDDL.DataBind();

        }

        public void SaveMapping(int chromeStyleID, int inventoryID, int businessUnitID, string member)
        {
            _engineCollection = (EngineCollection)ViewState["engineCollection"];
            _transmissionCollection = (TransmissionCollection)ViewState["transmissionCollection"];
            _drivetrainCollection = (DrivetrainCollection)ViewState["drivetrainCollection"];
            _fuelSystemCollection = (FuelSystemCollection)ViewState["fuelSystemCollection"];

            var engine = _engineCollection.GetEngine(int.Parse(EnginesDDL.SelectedValue)) ?? new Engine();
            var transmission = _transmissionCollection.GetTransmission(int.Parse(TransmissionsDDL.SelectedValue)) ??
                               new Transmission();
            var drivetrain = _drivetrainCollection.GetDrivetrain(int.Parse(DrivetrainsDDL.SelectedValue)) ??
                             new Drivetrain();
            var fuelSystem = _fuelSystemCollection.GetFuelSystem(int.Parse(FuelSystemsDDL.SelectedValue)) ??
                             new FuelSystem();

            // my upsert object, which prepares my record to be saved in the database
            UpsertVehicleConfiguration mapToDb = new UpsertVehicleConfiguration(chromeStyleID,
                                        inventoryID,
                                        businessUnitID,
                                        engine.CategoryId,
                                        transmission.CategoryId,
                                        drivetrain.CategoryId,
                                        fuelSystem.CategoryId,
                                        member);


            mapToDb.MapEquipment();

            List<IEquipmentOption> equipmentForGenerics = new List<IEquipmentOption>
                                            {
                                                engine,
                                                transmission,
                                                drivetrain,
                                                fuelSystem
                                            };

            foreach (IEquipmentOption equipment in equipmentForGenerics)
            {
                FirstLook.Merchandising.DomainModel.Vehicles.GenericEquipment generic = new FirstLook.Merchandising.DomainModel.Vehicles.GenericEquipment
                                                                                            {
                                                                                                Category =
                                                                                                    {
                                                                                                        Description =
                                                                                                            equipment.
                                                                                                            Description,
                                                                                                        CategoryID =
                                                                                                            equipment.
                                                                                                            CategoryId
                                                                                                    },
                                                                                                IsStandard =
                                                                                                    equipment.IsStandard
                                                                                            };
                generic.Category.TypeFilter = equipment.CategoryTypeFilter;
                MyGenericEquipment.Add(generic);
            }      
        }        

        private IList<DdlItem> GetListFromEquipmentCollection(IEnumerable<IEquipmentOption> equipment)
        {
            var list = new List<DdlItem>();
            foreach (var e in equipment)
            {
                if (list.Count <= 0)
                    list.Add(
                        new DdlItem(((EquipmentCollectionBaseClass)equipment).GetDefaultCallToAction(), 0));

                list.Add(
                    new DdlItem(e.Description +
                                 (e.IsStandard ? " [Std]" : "") +
                                 (e.IsOptional ? " [Opt]" : ""),
                                 e.CategoryId));
            }
            return list;
        }

        public class DdlItem
        {
            public string Description { get; private set; }
            public int CategoryId { get; private set; }

            public DdlItem(string description, int categoryId)
            {
                Description = description;
                CategoryId = categoryId;
            }
        }

        #region OnSelection Events for Object Data Sources
        protected void EngineDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["chromeStyleID"] = _chromeStyleID;
            if (EnginesDDL.Items.Count > 0)
                e.Cancel = true;
        }

        protected void TransmissionDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["chromeStyleID"] = _chromeStyleID;
            if (TransmissionsDDL.Items.Count > 0)
                e.Cancel = true;
        }

        protected void DrivetrainDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["chromeStyleID"] = _chromeStyleID;
            if (DrivetrainsDDL.Items.Count > 0)
                e.Cancel = true;
        }

        protected void FuelSystemDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["chromeStyleID"] = _chromeStyleID;
            if (FuelSystemsDDL.Items.Count > 0)
                e.Cancel = true;
        }
        #endregion
    }
}