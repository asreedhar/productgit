﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="PrintWindowSticker.ascx.cs" Inherits="Wanamaker.WebApp.Workflow.Controls.PrintWindowSticker" %>
<div id="PrintDiv" runat="server" class="clearfix">

    <p class="info"><asp:Label ID="LastPrintedMessage" runat="server"></asp:Label></p>

    <div class="field clearfix">
        <label for="<%= WindowStickerTemplateList.ClientID %>">Window Sticker</label>
        <asp:DropDownList ID="WindowStickerTemplateList" runat="server" DataTextField="Name" DataValueField="Id" AutoPostBack="true"
            OnSelectedIndexChanged="TemplateList_SelectedIndexChanged">
        </asp:DropDownList>
    </div>

    <div class="field clearfix">
        <label for="<%= BuyersGuideTemplateList.ClientID %>">Buyers Guide</label>
        <asp:DropDownList ID="BuyersGuideTemplateList" runat="server" DataTextField="Name" DataValueField="Id" AutoPostBack="true"
            OnSelectedIndexChanged="TemplateList_SelectedIndexChanged">
        </asp:DropDownList>
    </div>

    <asp:UpdateProgress ID="ProgressInidicator" runat="server" DynamicLayout="false">
        <ProgressTemplate>
            loading...</ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel ID="PromptControlsPanel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="WindowStickerTemplateList" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="BuyersGuideTemplateList" EventName="SelectedIndexChanged" />
        </Triggers>
        <ContentTemplate>
            <div id="PromptControls" runat="server">
                <div class="selectSheets">
                    <asp:RadioButton ID="WarrantyNo" Text="No Warranty" runat="server" GroupName="Warranty" Checked="false" Visible="false" />
                    <asp:RadioButton ID="WarrantyLimited" Text="Limited Warranty" runat="server" GroupName="Warranty" Checked="false" Visible="false" />
                    <asp:RadioButton ID="WarrantyFull" Text="Full Warranty" runat="server" GroupName="Warranty" Checked="false" Visible="false" />
                </div>
                <div class="selectSheets">
                    <asp:CheckBox ID="ServiceContract" Text="Service Contract" runat="server" Checked="false" Visible="false" />
                    <asp:CheckBox ID="KBBBookValue" Text="KBB Book Value" runat="server" Checked="false" Visible="false" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="selectSheets">
        <asp:CheckBox ID="PrintWindowStickerCheckbox" Text="Print Window Sticker" runat="server" Checked="false" />
        <asp:CheckBox ID="PrintBuyersGuideCheckbox" Text="Print Buyer's Guide" runat="server" Checked="false" />
    </div>
    
    <div class="sectionButtons clearfix">
        <asp:Button ID="PrintButton" runat="server" Text="Print" OnClick="PrintButton_Click" CssClass="button" />
    </div>

</div>

<div id="NoPrint" runat="server" class="noPrint">
    You have no templates saved for Window Stickers or Buyer's guides. Please contact your Account Manager.
</div>

<div id="ServiceError" runat="server" visible="false" class="error">
    Window stickers are not available due to system error. This has been logged and is being investigated.
</div>
