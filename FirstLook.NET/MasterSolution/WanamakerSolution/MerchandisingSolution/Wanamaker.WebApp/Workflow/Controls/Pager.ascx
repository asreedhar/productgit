﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Pager.ascx.cs" Inherits="Wanamaker.WebApp.Workflow.Controls.Pager" %>

<div id="PageCount">
    <asp:Label ID="NumberOfFirstItem" runat="server"></asp:Label>
    -
    <asp:Label ID="NumberOfLastItem" runat="server" Text="<%# NumberOfLastItemOnPage %>"></asp:Label>
    of
    <asp:Label ID="TotalItemCount" runat="server" Text="<%# ItemCount %>"></asp:Label>
    Vehicles&nbsp;&nbsp;
    
    <asp:LinkButton ID="ShowAllItems" runat="server" Text="Show All" OnClick="ShowAll_Click" OnClientClick='<%# GoogleAnalytics("Home Page", "Click", "Show All Inventory (remove paging)") %>' /> 
    <asp:LinkButton ID="UsePaging" runat="server" Text="Use Paging" Visible="false" OnClick="UsePaging_Click" OnClientClick='<%# GoogleAnalytics("Home Page", "Click", "Use Inventory Paging") %>' />

</div>
<div id="PageList">
    <asp:Repeater runat="server" ID="PageNumbers">
        <HeaderTemplate><ul></HeaderTemplate>
        <FooterTemplate></ul></FooterTemplate>
        <ItemTemplate>
            <li class="<%# ( (int)Eval("PageId") == PageNumber ) ? "selected" : string.Empty %>">
                <asp:Label ID="CurrentPage" 
                           Text='<%# Eval("PageNum") %>' 
                           Visible='<%# (int)Eval("PageId") == PageNumber %>' 
                           runat="server" />
                <asp:LinkButton ID="Page" 
                                Text='<%# Eval("PageNum") %>' 
                                CommandArgument='<%# Eval("PageId") %>' 
                                Visible='<%# (int)Eval("PageId") != PageNumber %>'
                                OnCommand="PageClicked" 
                                OnClientClick='<%# GoogleAnalytics("Home Page", "Click", "Inventory Paging (Page: " + Eval("PageNum") + ")") %>'
                                runat="server" />
            </li>
        </ItemTemplate>
    </asp:Repeater>
</div>