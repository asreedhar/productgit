using System;
using System.Configuration;
using System.Web.UI.WebControls;
using FirstLook.DomainModel.Oltp;
using Wanamaker.WebApp.Controls;

namespace Wanamaker.WebApp.Workflow.Controls
{
    public partial class TrueCarWidget : BusinessUnitUserControl
    {
        public delegate void ChromeStyleChangedEventHandler(object sender, EventArgs e);

        public event ChromeStyleChangedEventHandler ChromeStyleChanged;
        public string Vin
        {
            get
            {
                object o = ViewState["vin"];
                return (o == null) ? string.Empty : (string)o;
            }
            set
            {
                ViewState["vin"] = value;
            }
        }
        public decimal InternetPrice
        {
            get
            {
                object o = ViewState["price"];
                return (o == null) ? 0.0m : (decimal)o;
            }
            set
            {
                ViewState["price"] = value;
            }
        }
        public int ChromeStyleId
        {
            get
            {
                return int.Parse(StyleDDL.SelectedValue);
            }
        }
        protected void Style_Changed(object sender, EventArgs e)
        {

            int selectedStyleId;
            if (Int32.TryParse(StyleDDL.SelectedValue, out selectedStyleId))
            {
                SetTrueCarParameters(selectedStyleId, Vin, InternetPrice);
                if (ChromeStyleChanged != null)
                {
                    ChromeStyleChanged(this, new EventArgs());
                }
            }
        }


        protected void Style_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["vin"] = Vin;
        }

        public void SetTrueCarParameters(int chromeStyleId, string vin, decimal internetPrice)
        {
            Vin = vin;
            InternetPrice = internetPrice;
            if (chromeStyleId > 0)
            {
                BusinessUnit businessUnit = Context.Items[BusinessUnit.HttpContextKey] as BusinessUnit;
                if (businessUnit != null)
                {


                    string flashVars =
                        string.Format(
                            "<param name=\"flashvars\" value=\"partnerId={0}&trimId={1}&zipCode={2}&options_invoice={3}&options_msrp={4}&total_incentives={5}&dealer_name={6}&dealer_address={7}&dealer_city={8}&dealer_state={9}&vin={10}&dealer_price={11}\" />",
                            ConfigurationManager.AppSettings["TrueCarPartnerName"], chromeStyleId, businessUnit.ZipCode, "0", "0", "0", Server.HtmlEncode(businessUnit.Name), "", "", "", vin, string.Format("{0:#######}", internetPrice));
                    flashVars2.Text = flashVars;
                    flashVars1.Text = flashVars;
                    TrueCarView.ActiveViewIndex = 1;
                }
            }
            else
            {
                TrueCarView.ActiveViewIndex = 0;
            }

        }
    }
}