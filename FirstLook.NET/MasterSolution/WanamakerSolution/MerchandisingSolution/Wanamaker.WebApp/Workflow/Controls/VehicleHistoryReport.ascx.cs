﻿using System.Web;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;

namespace Wanamaker.WebApp.Workflow.Controls
{
    public partial class VehicleHistoryReport : WorkflowBaseUserControl
    {
        public InventoryData InventoryInfo { get; set; }

        public string EStockLink { get; set; }

        public bool IsComplete { get; set; }

        private CarfaxInterface _carfaxModel;

       
        public void LoadData()
        {
            LoadCarFaxInformation();
            ProcessOrdered();
            ProcessNotOrdered();
        }

        private void LoadCarFaxInformation()
        {
            _carfaxModel = new CarfaxInterface(BusinessUnitId, InventoryInfo.VIN, Page.User.Identity.Name);
            IsComplete = _carfaxModel.IsValidReport;
        }

        private void ProcessOrdered()
        {
            if (!IsComplete) return;
            VehicleHistoryLink.HRef = EStockLink;
            VehicleHistoryLink.InnerText = "View Report";

        }

        private void ProcessNotOrdered()
        {
            if (IsComplete) return;
            VehicleHistoryLink.HRef = EStockLink;
            VehicleHistoryLink.InnerText = "Order Report";
        }


        public static string HasCarfaxReport(int businessunitId, string vin, string name)
        {
            var model = new CarfaxInterface(businessunitId, vin, HttpContext.Current.User.Identity.Name);
            return "{ \"HasCarfax\": \"" + model.IsValidReport + "\" }";
        }
    }
}