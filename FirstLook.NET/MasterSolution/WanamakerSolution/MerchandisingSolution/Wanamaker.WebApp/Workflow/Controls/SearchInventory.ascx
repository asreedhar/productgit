﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchInventory.ascx.cs" Inherits="Wanamaker.WebApp.Workflow.Controls.SearchInventory" %>

<asp:Panel ID="SearchWidgetPanel" runat="server" DefaultButton="HeaderSearchButton">
    <div id="HeaderSearch">
        <asp:TextBox runat="server" ID="HeaderSearchText" ClientIDMode="Static"
                     ToolTip="Search inventory by Make, Model, Stock Number, VIN, Color, etc."
                     placeholder="Stock #, VIN, Make ..."></asp:TextBox>
        <asp:Button id="HeaderSearchButton" runat="server" Text="Search" clientidmode="Static" OnClick="OnSearchClicked" />
    </div>
</asp:Panel>