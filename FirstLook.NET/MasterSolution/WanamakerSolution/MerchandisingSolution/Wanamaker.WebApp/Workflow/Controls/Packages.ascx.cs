﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using FirstLook.Merchandising.DomainModel.VehicleVinData;
using log4net;
using Merchandising.Messages.AutoApprove;
using MvcMiniProfiler;
using VehicleDataAccess;

namespace Wanamaker.WebApp.Workflow.Controls
{
    public partial class Packages : WorkflowBaseUserControl
    {
        #region Logging

        private static readonly ILog Log = LogManager.GetLogger( typeof( Packages ).FullName );

        #endregion

        #region Injected dependencies

        public IAdMessageSender MessageSender { get; set; }
        public IInventorySearch InventorySearch { get; set; }
        public IDiscountPricingCampaignManager PricingCampaignManager { get; set; }

        #endregion

        private VehicleConfiguration _conf;
        private EquipmentCategoryIds _equipmentObjectCategoryIds;
        private string _currHeaderHighlighted = string.Empty;
        private string _currHeader = string.Empty;

        public event EventHandler SaveClicked;

        public bool ShowTrimSelector { get; set; }

        public string WorkflowSelectorID { get; set; }
        private WorkflowSelector WfsInventoryItem
        {
            get
            {
                return Page.Master.FindControl( "BodyPlaceHolder" ).FindControl( WorkflowSelectorID ) as WorkflowSelector;
            }
        }


        public string InventoryDataViewID { get; set; }
        private InventoryDataView InvDataViewer
        {
            get
            {
                return Page.Master.FindControl( "BodyPlaceHolder" ).FindControl( InventoryDataViewID ) as InventoryDataView;
            }
        }

        public EquipmentCategoryIds ObjectCategoryIds
        {
            get
            {
                object o = ViewState[ "EquipmentObjectCategoryIds" ];
                if ( o == null )
                {
                    if (InventoryInfo.ChromeStyleId != null)
                        return new EquipmentCategoryIds( InventoryInfo.ChromeStyleId.Value );
                }

                return ( EquipmentCategoryIds )o;

            }
            set
            {
                _equipmentObjectCategoryIds = value;
                ViewState[ "EquipmentObjectCategoryIds" ] = _equipmentObjectCategoryIds;
            }
        }

        public GenericEquipmentCollection GenericSelections
        {
            get
            {
                GenericEquipmentCollection de = new GenericEquipmentCollection();
                de.LoadViewState( ViewState[ "gens" ] );
                return de;
            }
            set
            {
                ViewState[ "gens" ] = value.SaveViewState();
            }
        }

        public DetailedEquipmentCollection Equipment
        {
            get
            {
                DetailedEquipmentCollection dec = new DetailedEquipmentCollection();
                dec.LoadViewState( ViewState[ "dec" ] );

                return dec;
            }
            set
            {
                ViewState[ "dec" ] = value.SaveViewState();
            }
        }

        private InventoryData InventoryInfo
        {
            get
            {
                return InvDataViewer.InventoryInfo;
            }
        }

        private VehicleConfiguration Conf
        {
            get
            {
                return _conf ?? (_conf = VehicleConfiguration.FetchOrCreateDetailed(BusinessUnitId, InventoryId,
                                                                                    Context.User.Identity.Name));
            }
        }

        public bool StepComplete
        {
            get
            {
                var statuses = StepStatusCollection.Fetch( BusinessUnitId, InventoryId );
                return statuses.GetStatus( StepStatusTypes.ExteriorEquipment ) == ActivityStatusCodes.Complete
                       && statuses.GetStatus( StepStatusTypes.InteriorEquipment ) == ActivityStatusCodes.Complete;
            }
        }

        public string Vin
        {
            get
            {
                string str = InventoryInfo.VIN;
                if ( string.IsNullOrEmpty( str ) )
                {
                    throw new ApplicationException( "No VIN found for vehicle!" );
                }
                return str;
            }
        }

        
        protected void Page_Load ( object sender, EventArgs e )
        {
            MessageSender.SendMessageAsync = CheckApprovalPage();
            if ( !IsPostBack )
            {
               SetupPage();
                EquipmentStepCompletedCkb.Checked = StepComplete;
            }
        }

        private bool CheckApprovalPage()
        {
            try
            {
                return System.IO.Path.GetFileName(Page.Request.Path).Contains("ApprovalSummary");
            }
            catch (Exception ex)
            {
                Log.Error("Error getting page name from CheckApprovalPage",ex);
            }
            return false;
        }
        private void SetupPage ()
        {
            //packages.BusinessUnitId = BusinessUnitId;
            //packages.InventoryId = InventoryID;
            GenericSelections = Conf.VehicleOptions.generics;

            SetupSelectedOptions();

            TrimWidget.Visible = ShowTrimSelector;
        }

        private void SetupSelectedOptions ()
        {
            DetailedEquipmentCollection tmp = DetailedEquipmentCollection.FetchSelections( BusinessUnitId, InventoryId );
            Equipment = tmp;
            tmp.ClearStandardHighlights();

            SelectedOptions.DataSource = tmp;
            SelectedOptions.DataBind();
        }

        private void Save ()
        {
            Log.Debug( "Save() called." );
            UpdateDescriptions();
            Equipment.Save( BusinessUnitId, InventoryId );
            GenericSelections.Save( BusinessUnitId, InventoryId );

            MessageSender.SendAutoApproval(new AutoApproveMessage(BusinessUnitId, InventoryId, Page.User.Identity.Name), InventoryInfo.IsNew(), GetType().FullName);
            PricingCampaignManager.ApplyDiscount(BusinessUnitId, InventoryId, Page.User.Identity.Name, GetType().FullName);
        }

        protected void OnSave()
        {
            Save();
            //Data bind only if the standard equipment is visible
            if (ContainsStandardEquipmentDisplay)
            {
                gvSelectedEquipment.DataBind();
                StandardEquipGV.DataBind();
            }
            if (SaveClicked != null)
                SaveClicked(this, EventArgs.Empty);
        }

        private void UpdateDescriptions ()
        {
            DetailedEquipmentCollection tmp = Equipment;
            foreach ( GridViewRow gvr in SelectedOptions.Rows )
            {
                DataKey dk = SelectedOptions.DataKeys[ gvr.DataItemIndex ];
                if ( dk == null ) throw new ApplicationException( "No OptionCode key found for options" );

                TextBox tbTitle = gvr.FindControl( "UserTitle" ) as TextBox;
                if ( tbTitle == null ) throw new ApplicationException( "User Title not found for options" );

                TextBox tb = gvr.FindControl( "UserDesc" ) as TextBox;
                if ( tb == null ) throw new ApplicationException( "User Description not found for options" );

                DetailedEquipment de = tmp.GetByCode( ( string )dk.Value );
                de.OptionText = tbTitle.Text.ToUpper();
                de.DetailText = tb.Text;
            }

            //Get the list of possible standard highlights, for comparison reasons
            EquipmentCollection origEquip = EquipmentCollection.StandardEquipmentSelect( Convert.ToInt32( StyleDDL.SelectedValue ) );

            //remove all standard highlights, b/c we're going to add them back here
            //tmp.ClearStandardHighlights();
            foreach ( GridViewRow gvr in StandardEquipGV.Rows )
            {
                CheckBox cb = gvr.FindControl( "highlight" ) as CheckBox;
                if ( cb != null && cb.Checked )
                {
                    DataKey dk = StandardEquipGV.DataKeys[ gvr.DataItemIndex ];
                    if ( dk == null ) throw new ApplicationException( "No sequence key found for standards" );

                    TextBox tb = gvr.FindControl( "UserDesc" ) as TextBox;
                    if ( tb == null ) throw new ApplicationException( "User Description not found for standards" );

                    DetailedEquipment de = new DetailedEquipment( DetailedEquipment.GetStandardCode( ( int )dk[ 0 ] ), string.Empty, dk[ 1 ].ToString(), tb.Text,
                                                                 true );
                    tmp.Add( de );

                    //Save any equipment descriptions that were changed
                    foreach ( Equipment item in origEquip )
                    {
                        if ( DetailedEquipment.GetStandardCode( ( int )dk[ 0 ] ) == de.OptionCode && DetailedEquipment.GetStandardCode( item.Sequence ) == de.OptionCode && item.NormalizedDescription.ToLower() != tb.Text.ToLower() )
                        {
                            CustomStandardEquipment custom = new CustomStandardEquipment( BusinessUnitId, InventoryInfo.InventoryType, de.OptionCode, tb.Text );
                            custom.Save();
                            break;
                        }
                    }
                }
            }

            foreach ( GridViewRow gvr in gvSelectedEquipment.Rows )
            {
                CheckBox cb = gvr.FindControl( "highlight" ) as CheckBox;

                if ( cb != null && !cb.Checked )
                {
                    //Remove when the user clicks the 'X'
                    DataKey dk = gvSelectedEquipment.DataKeys[ gvr.DataItemIndex ];
                    if ( dk == null ) throw new ApplicationException( "No sequence key found for standards" );

                    TextBox tb = gvr.FindControl( "UserDesc" ) as TextBox;
                    if ( tb == null ) throw new ApplicationException( "User Description not found for standards" );

                    DetailedEquipment de = new DetailedEquipment( DetailedEquipment.GetStandardCode( ( int )dk[ 0 ] ), string.Empty, dk[ 1 ].ToString(), tb.Text,
                                                                 true );
                    tmp.Remove( de.OptionCode );
                }
                else if ( cb != null && cb.Checked )
                {
                    //Update descriptions
                    DataKey dk = gvSelectedEquipment.DataKeys[ gvr.DataItemIndex ];
                    if ( dk == null ) throw new ApplicationException( "No sequence key found for standards" );

                    TextBox tb = gvr.FindControl( "UserDesc" ) as TextBox;
                    if ( tb == null ) throw new ApplicationException( "User Description not found for standards" );

                    DetailedEquipment de = new DetailedEquipment( DetailedEquipment.GetStandardCode( ( int )dk[ 0 ] ), string.Empty, dk[ 1 ].ToString(), tb.Text,
                                                                 true );
                    tmp.Remove( de.OptionCode );
                    tmp.Add( de );

                    //Save any equipment descriptions that were changed
                    foreach ( Equipment item in origEquip )
                    {
                        if ( DetailedEquipment.GetStandardCode( ( int )dk[ 0 ] ) == de.OptionCode && DetailedEquipment.GetStandardCode( item.Sequence ) == de.OptionCode && item.NormalizedDescription.ToLower() != tb.Text.ToLower() )
                        {
                            CustomStandardEquipment custom = new CustomStandardEquipment( BusinessUnitId, InventoryInfo.InventoryType, de.OptionCode, tb.Text );
                            custom.Save();
                            break;
                        }
                    }
                }
            }

            Equipment = tmp;

        }
        /// <summary>
        /// The packages control is shown on two different pages.  Standard equipment is hidden on Approval summary for performance reasons
        /// </summary>
        public void RemoveStandardEquipmentDisplay()
        {
            StandardTab.Visible = false;
            StandardEquipmentPlaceHolder.Controls.Clear();
        }

        private bool ContainsStandardEquipmentDisplay
        {
            get { return StandardEquipmentPlaceHolder.Controls.Count > 0; }
        }
        public void DataBindPackages()
        {
            SetupSelectedOptions();
            PossibleOptions.DataBind();
        }

        private void SetOptionSelectorChromeStyle ()
        {
            int csid;
            if ( int.TryParse( StyleDDL.SelectedValue, out csid ) )
            {
                //packages.ChromeStyleId = csid;
                // packages.DataBind();
                Conf.UpdateStyleId( BusinessUnitId, csid );
            }
        }

        protected void Style_Changed ( object sender, EventArgs e )
        {
            SetOptionSelectorChromeStyle();

            int numberOfOptions = SelectedOptions.DataKeys.Count;
            //int numberOfOptions = 0;
            //while(SelectedOptions.DataKeys.Count != 0)
            for ( int i = numberOfOptions; i > 0; i-- )
            {
                DataKey dk = SelectedOptions.DataKeys[ i - 1 ];
                if ( dk != null )
                {
                    string optionCode = ( string )dk.Value;
                    DetailedEquipment de = Equipment.GetByCode( optionCode );
                    DetailedEquipmentCollection tmp = Equipment;
                    tmp.Remove( optionCode );
                    Equipment = tmp;

                    CheckOptionSelectionChanged( false, de.categoryAdds, de.categoryRemoves );
                }
                //numberOfOptions++; 
            }
        }

        protected void AddToGenerics ( IEquipmentOption equipmentOption, GenericEquipmentCollection gen )
        {
            GenericEquipment equipmentToBeAdded = new GenericEquipment
            {
                Category =
                {
                    Description = equipmentOption.Description,
                    CategoryID = equipmentOption.CategoryId
                },
                IsStandard = equipmentOption.IsStandard
            };
            equipmentToBeAdded.Category.TypeFilter = equipmentOption.CategoryTypeFilter;
            gen.Add( equipmentToBeAdded );

        }

        protected void CheckOptionSelectionChanged ( bool selected, CategoryCollection categoryAdds, CategoryCollection categoryRemoves )
        {

            GenericEquipmentCollection gen = GenericSelections;

            //string type = FindEquipmentType(categoryAdds);
            foreach ( CategoryLink cl in categoryAdds )
            {
                if ( selected )
                {
                    gen.Add( new GenericEquipment( cl, false ) );
                }
                else
                {
                    gen.RemoveOptionsOnly( cl.CategoryID );
                }
            }

            foreach ( CategoryLink cl in categoryRemoves )
            {
                if ( selected )
                {
                    gen.RemoveOptionsOnly( cl.CategoryID );
                }
                else
                {
                    foreach ( CategoryLink c in categoryAdds )
                    {
                        if ( ObjectCategoryIds.ContainsCategoryId( c.CategoryID ) )
                        {
                            string type = EquipmentCategoryIds.FindTypeByCategoryID( c.CategoryID );
                            EquipmentCollectionBaseClass collection = ObjectCategoryIds.GetCollectionByType( type );
                            IEquipmentOption equipmentOption = collection.GetStandard();
                            AddToGenerics( equipmentOption, gen );
                            //ViewState["IdToSave"] = equipmentOption.CategoryId;
                            //ViewState["typeToSave"] = type;
                            UpsertVehicleConfiguration.UpdateEquipmentOption( type, InventoryInfo.InventoryID, BusinessUnitId, equipmentOption.CategoryId );
                            break;
                        }

                        gen.Add( new GenericEquipment( cl, false ) );

                    }

                }

            }
            GenericSelections = gen;

            DetailedEquipmentCollection tmp = Equipment;
            tmp.ClearStandardHighlights();

            SelectedOptions.DataSource = tmp;
            SelectedOptions.DataBind();
        }

        protected void LnkClearCustomStdSelectedClick ( object sender, EventArgs e )
        {
            LinkButton btnSender = ( LinkButton )sender;
            CustomStandardEquipment custom = new CustomStandardEquipment( BusinessUnitId, InventoryInfo.InventoryType, DetailedEquipment.GetStandardCode( Convert.ToInt32( btnSender.CommandArgument ) ), String.Empty );
            custom.Delete();

            EquipmentCollection temp = EquipmentCollection.StandardEquipmentSelect( Convert.ToInt32( StyleDDL.SelectedValue ) );
            foreach ( Equipment item in temp )
            {
                if ( item.Sequence == Convert.ToInt32( btnSender.CommandArgument ) )
                {
                    DetailedEquipmentCollection tmp = Equipment;
                    DetailedEquipment update = Equipment.GetStandardHighlight( item.Sequence );
                    update.DetailText = item.Description;
                    tmp.Remove( update.OptionCode );
                    tmp.Add( update );
                    Equipment = tmp;
                    Equipment.Save( BusinessUnitId, InventoryId );
                    break;
                }
            }

            gvSelectedEquipment.DataBind();
        }

        protected void BtnAddStandardClick ( object sender, EventArgs e )
        {
            try
            {
                Save();
                StandardEquipGV.DataBind();
                gvSelectedEquipment.DataBind();
            }
            catch ( Exception exception )
            {
                Log.Error( "Adding Option Exception", exception );
                throw;
            }
        }

        protected void LnkClearCustomStdClick ( object sender, EventArgs e )
        {
            LinkButton btnSender = ( LinkButton )sender;
            CustomStandardEquipment custom = new CustomStandardEquipment( BusinessUnitId, InventoryInfo.InventoryType, DetailedEquipment.GetStandardCode( Convert.ToInt32( btnSender.CommandArgument ) ), String.Empty );
            custom.Delete();

            StandardEquipGV.DataBind();
        }

        protected void TabComplete_CheckChanged ( object sender, EventArgs e )
        {


            CheckBoxPlus cbp = sender as CheckBoxPlus;
            if ( cbp == null ) return;

            SaveEquipmentStatus( cbp.Checked);

        }

        protected void Style_DataBound ( object sender, EventArgs e )
        {
            if ( StyleDDL.Items.Count > 0 && StyleDDL.Items[ 0 ].Text != "Select trim..." )
            {
                StyleDDL.Items.Insert(0, new ListItem("Select trim...", ""));
            }

            if (StyleDDL.Items.FindByValue(Conf.ChromeStyleID.ToString()) == null) return;
            StyleDDL.SelectedValue = Conf.ChromeStyleID.ToString();
            SetOptionSelectorChromeStyle();

        }

        protected void Option_Bound ( object sender, GridViewRowEventArgs e )
        {
            GridViewRow gvr = e.Row;
            DetailedEquipment equip = gvr.DataItem as DetailedEquipment;
            if ( equip == null ) return;

            TextBox customText = gvr.FindControl( "UserDesc" ) as TextBox;
            if ( customText == null ) throw new ApplicationException( "Custom User Textbox not found!" );

            customText.Rows = ( int )Math.Ceiling( ( ( double )customText.Text.Length ) / 105 );
        }

        protected void SaveEquipmentStatus (bool complete)
        {
            var statusCode = complete 
                ? ActivityStatusCodes.Complete 
                : ActivityStatusCodes.Incomplete;

            StepStatusCollection
                .Fetch( BusinessUnitId, InventoryId )
                .SetStatus( StepStatusTypes.InteriorEquipment, statusCode )
                .SetStatus( StepStatusTypes.ExteriorEquipment, statusCode )
                .Save(Context.User.Identity.Name);
        }

        protected void gvSelectedEquipment_SelectedIndexChanged ( object sender, EventArgs e )
        {
            CheckBox chkHighlight = ( CheckBox )gvSelectedEquipment.Rows[ gvSelectedEquipment.SelectedIndex ].FindControl( "highlight" );
            chkHighlight.Checked = false;
            Save();
            StandardEquipGV.DataBind();
            gvSelectedEquipment.DataBind();
        }

        protected void SelectedOptions_RowDeleting ( object sender, GridViewDeleteEventArgs e )
        {
            DataKey dk = SelectedOptions.DataKeys[ e.RowIndex ];
            if ( dk != null )
            {
                string optionCode = ( string )dk.Value;
                DetailedEquipment de = Equipment.GetByCode( optionCode );
                DetailedEquipmentCollection tmp = Equipment;
                tmp.Remove( optionCode );
                Equipment = tmp;

                CheckOptionSelectionChanged( false, de.categoryAdds, de.categoryRemoves );
            }
            
            OnSave();
        }

        protected void CustomizeForModel_Click ( object sender, EventArgs e )
        {
            CreateCustomization( sender, false );
        }

        protected void CustomizeForStyle_Click ( object sender, EventArgs e )
        {
            CreateCustomization( sender, true );
        }

        private void CreateCustomization ( object sender, bool forStyle )
        {
            LinkButton lb = sender as LinkButton;
            if ( lb == null ) throw new ApplicationException( "link button requried for customization of text" );

            TextBox optTitle = lb.NamingContainer.FindControl( "UserTitle" ) as TextBox;
            TextBox optDesc = lb.NamingContainer.FindControl( "UserDesc" ) as TextBox;
            if ( optTitle == null || optDesc == null ) throw new ApplicationException( "userTitle or userDesc not found for customization of option text" );

            //get the option
            DetailedEquipment de = Equipment.GetByCode( lb.CommandArgument );


            //if style checked...override for style
            if ( forStyle )
            {
                de.CreateDetailedEquipmentOverrideForStyle( BusinessUnitId, int.Parse( StyleDDL.SelectedValue ), optTitle.Text, optDesc.Text );
            }
            else
            {
                de.CreateDetailedEquipmentOverrideForModelByStyle( BusinessUnitId, int.Parse( StyleDDL.SelectedValue ), optTitle.Text, optDesc.Text );
            }
            lb.ForeColor = Color.ForestGreen;
            lb.Font.Bold = true;
        }


        protected void OptionSelectionChanged ( bool selected, CategoryCollection categoryAdds, CategoryCollection categoryRemoves )
        {
            GenericEquipmentCollection gen = GenericSelections;
            foreach ( CategoryLink cl in categoryAdds )
            {
                if ( selected )
                {
                    gen.Add( new GenericEquipment( cl, false ) );
                }
                else
                {
                    gen.RemoveOptionsOnly( cl.CategoryID );
                }
            }

            foreach ( CategoryLink cl in categoryRemoves )
            {
                if ( selected )
                {
                    gen.RemoveOptionsOnly( cl.CategoryID );
                }
                else
                {
                    gen.Add( new GenericEquipment( cl, false ) );
                }
            }
            GenericSelections = gen;

            DetailedEquipmentCollection tmp = Equipment;
            tmp.ClearStandardHighlights();

            SelectedOptions.DataSource = tmp;
            SelectedOptions.DataBind();

        }


        protected void BtnAddClick ( object sender, EventArgs e )
        {
            foreach ( GridViewRow row in PossibleOptions.Rows )
            {
                if ( row.RowType == DataControlRowType.DataRow )
                {
                    CheckBox chk = ( CheckBox )row.FindControl( "chkAdd" );
                    if ( chk != null && chk.Checked )
                    {
                        DataKey dk = PossibleOptions.DataKeys[ row.RowIndex ];

                        if ( dk != null )
                        {
                            string opCode = ( string )dk.Values[ "OptionCode" ];
                            string catList = ( string )dk.Values[ "CategoryList" ];
                            string titleTxt = ( string )dk.Values[ "NormalizedDescription" ];
                            string detailTxt = ( string )dk.Values[ "ExtDescription" ];
                            if ( noParensCB.Checked )
                            {
                                Regex rg = new Regex( @"\s{0,1}[\(].*?[\)]" );
                                titleTxt = rg.Replace( titleTxt, string.Empty );
                                detailTxt = rg.Replace( detailTxt, string.Empty );
                            }

                            Panel customPanel = row.FindControl( "customPanel" ) as Panel;

                            string useText;
                            if ( customPanel != null )
                            {
                                if ( customPanel.Visible )
                                {
                                    useText = detailTxt.Trim();
                                }
                                else
                                {
                                    useText = titleTxt.Trim().ToUpper();
                                    if ( !titleOnlyCB.Checked && detailTxt.Trim().Length > 0 )
                                        useText += ":" + linkText.Text.Trim() + " " + detailTxt.Trim();
                                }
                            }
                            else
                            {
                                useText = titleTxt;
                                if ( !titleOnlyCB.Checked && detailTxt.Trim().Length > 0 )
                                    useText += linkText.Text.Trim() + " " + detailTxt.Trim();
                            }

                            DetailedEquipment detailedEquipment = new DetailedEquipment( opCode, titleTxt.ToUpper(), catList, useText, false );
                            DetailedEquipmentCollection tmp = Equipment;
                            tmp.Add( detailedEquipment );
                            Equipment = tmp;

                            OptionSelectionChanged( true, detailedEquipment.categoryAdds, detailedEquipment.categoryRemoves );
                            GenericEquipmentCollection temp = GenericSelections;
                            foreach ( CategoryLink cl in detailedEquipment.categoryRemoves )
                            {
                                List<int> removes = CategoryCollection.GetCategoryRemovesMap( cl.CategoryID );
                                foreach ( int item2 in removes )
                                {
                                    temp.Remove( item2 );
                                }
                            }
                            GenericSelections = temp;
                            GenericSelections.Save( BusinessUnitId, InventoryId );
                            
                        }
                    }
                    if ( chk != null ) chk.Checked = false;
                }
            }

            OnSave();
        }

        protected void gvSelectedEquipment_RowDataBound ( object sender, GridViewRowEventArgs e )
        {
            GridViewRow gvr = e.Row;

            if ( gvr.RowType == DataControlRowType.DataRow )
            {
                TextBox customText = gvr.FindControl( "UserDesc" ) as TextBox;
                if ( customText == null ) throw new ApplicationException( "Custom User Textbox not found!" );

                customText.Rows = ( int )Math.Ceiling( ( ( double )customText.Text.Length ) / 102 );

                CheckBox cb = gvr.FindControl( "highlight" ) as CheckBox;
                if ( cb != null )
                {
                    if ( Equipment.ContainsStandardHighlight( ( ( Equipment )gvr.DataItem ).Sequence ) )
                    {
                        cb.Checked = true;
                        DetailedEquipment thisEquipment = Equipment.GetStandardHighlight( ( ( Equipment )gvr.DataItem ).Sequence );
                        if ( thisEquipment != null )
                            customText.Text = thisEquipment.DetailText;
                    }
                    else
                    {
                        cb.Checked = false;
                    }
                }

                string head = ( ( Equipment )gvr.DataItem ).Header;
                if ( _currHeaderHighlighted != head )
                {
                    _currHeaderHighlighted = head;
                    Literal literalCtl = gvr.FindControl( "headingLiteral" ) as Literal;
                    if ( literalCtl != null )
                    {
                        literalCtl.Text = @"<h4>" + head + @"</h4>";
                    }
                }
            }
        }

        protected void StandardDS_Selecting ( object sender, ObjectDataSourceSelectingEventArgs e )
        {
            e.InputParameters[ "dec" ] = Equipment;
            e.InputParameters[ "businessUnitId" ] = BusinessUnitId;
            e.InputParameters[ "usedNew" ] = InventoryInfo.InventoryType;
            if (StyleDDL.SelectedValue == string.Empty)
                e.InputParameters["chromeStyleId"] = -1;
            else
                e.InputParameters["chromeStyleId"] = StyleDDL.SelectedValue;
        }

        protected void Style_Selecting ( object sender, ObjectDataSourceSelectingEventArgs e )
        {
            e.InputParameters[ "vin" ] = Vin;
        }

        protected void Standard_Bound ( object sender, GridViewRowEventArgs e )
        {
            using (MiniProfiler.Current.Step("Packages.Standard_Bound"))
            {
                GridViewRow gvr = e.Row;

                if (gvr.RowType == DataControlRowType.DataRow)
                {
                    TextBox customText = gvr.FindControl("UserDesc") as TextBox;
                    if (customText == null) throw new ApplicationException("Custom User Textbox not found!");

                    customText.Rows = (int) Math.Ceiling(((double) customText.Text.Length)/102);

                    CheckBox cb = gvr.FindControl("highlight") as CheckBox;
                    if (cb != null)
                    {
                        if (Equipment.ContainsStandardHighlight(((Equipment) gvr.DataItem).Sequence))
                        {
                            cb.Checked = true;
                            DetailedEquipment thisEquipment =
                                Equipment.GetStandardHighlight(((Equipment) gvr.DataItem).Sequence);
                            if (thisEquipment != null)
                                customText.Text = thisEquipment.DetailText;
                        }
                        else
                        {
                            cb.Checked = false;
                        }
                    }

                    string head = ((Equipment) gvr.DataItem).Header;
                    if (_currHeader != head)
                    {
                        _currHeader = head;
                        Literal literalCtl = gvr.FindControl("headingLiteral") as Literal;
                        if (literalCtl != null)
                        {
                            literalCtl.Text = @"<h4>" + head + @"</h4>";
                        }
                    }
                }
            }
        }

        protected void DeleteCustomization_Click ( object sender, EventArgs e )
        {
            LinkButton ib = sender as LinkButton;
            if ( ib == null ) throw new ApplicationException( "Need the image button to delete custom text" );

            DetailedEquipment.DeleteDetailedEquipmentOverride( BusinessUnitId, int.Parse( StyleDDL.SelectedValue ), ib.CommandArgument );
            PossibleOptions.DataBind();
        }

        protected void OptionList_Selecting ( object sender, ObjectDataSourceSelectingEventArgs e )
        {
            StyleDDL.DataBind();

            if ( String.IsNullOrEmpty( StyleDDL.SelectedValue ) )
                e.Cancel = true;
            if(!e.InputParameters.Contains("businessUnitId"))
                e.InputParameters.Insert(0, "businessUnitId", BusinessUnitId);
            else
                e.InputParameters["businessUnitId"] = BusinessUnitId;

            if (!e.InputParameters.Contains("inventoryId"))
                e.InputParameters.Insert( 1, "inventoryId", InventoryId );
            else
                e.InputParameters[ "inventoryId" ] = InventoryId;
            e.InputParameters[ "chromeStyleId" ] = StyleDDL.SelectedValue;
            e.InputParameters[ "customizedDescriptionDealerId" ] = BusinessUnitId;
        }

    }
}