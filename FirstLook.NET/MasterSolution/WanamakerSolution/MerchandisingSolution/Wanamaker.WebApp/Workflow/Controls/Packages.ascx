﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Packages.ascx.cs" Inherits="Wanamaker.WebApp.Workflow.Controls.Packages" %>
<div id="PackageContent">
    <div id="StepCompleted" class='<%= StepComplete ? "complete" : string.Empty %>'>
        <asp:CheckBox ID="EquipmentStepCompletedCkb" runat="server" ClientIDMode="Static" StepStatusType="ExteriorEquipment" />
        <label for="EquipmentStepCompletedCkb">
            Packages Complete</label>
    </div>
    <%--
    Don't think we need this now that the label says 'Packages Completed' (and not 'Equipment Completed)
    <div id="StepCompletedHelpMsg">
        (check if no packages installed)
    </div>
    --%>
    <div id="TrimWidget" clientidmode="Static" runat="server">
        <label for="StyleDDL">
            Trim:</label>
        <asp:DropDownList ID="StyleDDL" runat="server" DataSourceID="TrimStyleDS" DataTextField="Value" DataValueField="Key" OnSelectedIndexChanged="Style_Changed"
            AutoPostBack="true" OnDataBound="Style_DataBound" CausesValidation="true" onchange="_gaq.push(['_trackEvent', 'Packages Control', 'Change', 'Trim']);">
        </asp:DropDownList>
        <asp:RequiredFieldValidator InitialValue="" ID="StyleDDL_RequiredFieldValidator" runat="server" ControlToValidate="StyleDDL"
            ErrorMessage="Please select a trim package."></asp:RequiredFieldValidator>
    </div>
    <div id="LoadPackagesTabset">
        <ul id="LoadPackagesTabs" class="clearfix">
            <li><a id="HighlightTab" class="selected tabLink" onclick="_gaq.push(['_trackEvent', 'Packages Control', 'Click', 'Packages Tab']);">
                Highlight Vehicle Packages</a></li>
            <li>
                <asp:HyperLink ID="StandardTab" CssClass="tabLink" runat="server" ClientIDMode="Static" onclick="_gaq.push(['_trackEvent', 'Packages Control', 'Click', 'Standard Equipment Tab']);">Standard Equipment</asp:HyperLink></li>
        </ul>
        <div id="HighlightPackages" class="tabContent">
            <asp:ObjectDataSource ID="OptionDS" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.EquipmentCollection"
                SelectMethod="FetchOptional" OnSelecting="OptionList_Selecting">
                <SelectParameters>
                    <asp:ControlParameter Name="chromeStyleId" Type="Int32" ControlID="StyleDDL" PropertyName="SelectedValue" />
                    <asp:ControlParameter Name="searchText" Type="String" ControlID="searchText" PropertyName="Text" />
                    <asp:Parameter Name="customizedDescriptionDealerId" Type="Int32" DefaultValue="" ConvertEmptyStringToNull="true" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <h3>
                Selected Packages</h3>
            <asp:GridView ID="SelectedOptions" runat="server" AutoGenerateColumns="false" OnRowDeleting="SelectedOptions_RowDeleting"
                DataKeyNames="OptionCode" OnRowDataBound="Option_Bound">
                <EmptyDataRowStyle CssClass="empty" />
                <EmptyDataTemplate>
                    <div class="info">
                        <p>
                            No Packages Selected</p>
                    </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="first">
                        <ItemTemplate>
                            <asp:LinkButton ID="ImageButton1" ToolTip="Remove Option From Vehicle" CssClass="deleteLink" runat="server" CommandName="Delete"
                                OnClientClick="_gaq.push(['_trackEvent', 'Packages Control', 'Click', 'Remove Packages']);">&nbsp;</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="OptionCode" HtmlEncode="false" HeaderText="Code" ItemStyle-HorizontalAlign="Center" />
                    <asp:TemplateField HeaderText="Ad Text for Package" ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="last">
                        <ItemTemplate>
                            <p class="opText">
                                <asp:TextBox ID="UserTitle" runat="server" Text='<%# Eval("OptionText").ToString().ToUpper() %>' CssClass="text"></asp:TextBox>
                            </p>
                            <asp:TextBox ID="UserDesc" runat="server" Text='<%# Eval("Description") %>' TextMode="MultiLine" ssClass="textarea"></asp:TextBox>
                            <br />
                            Make Reusable for:&nbsp;&nbsp;
                            <asp:LinkButton ID="StyleRb" runat="server" Text="Similar vehicles" OnClick="CustomizeForStyle_Click" CommandArgument='<%# Eval("OptionCode") %>'
                                OnClientClick="_gaq.push(['_trackEvent', 'Packages Control', 'Click', 'Make Package Reusable (Similar Vehicles)']);" />
                            &nbsp;&nbsp;
                            <asp:LinkButton ID="ModelRb" runat="server" Text="This model" OnClick="CustomizeForModel_Click" CommandArgument='<%# Eval("OptionCode") %>'
                                OnClientClick="_gaq.push(['_trackEvent', 'Packages Control', 'Click', 'Make Package Reusable (This Model)']);" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:Panel ID="srch" runat="server" CssClass="searchForm" DefaultButton="search">
                <h3>
                    Find Packages</h3>
                <div id="PackageSearchDiv" class="clearfix">
                    <label for="searchText">
                        Search for:</label>
                    <asp:TextBox ID="searchText" runat="server" CssClass="text"></asp:TextBox>
                    <asp:Button ID="search" runat="server" Text="Search" OnClientClick="_gaq.push(['_trackEvent', 'Packages Control', 'Click', 'Package Search Button']);" />
                </div>
                <div id="PackageList">
                    <h4>
                        Text Settings:</h4>
                    <div class="field">
                        <asp:CheckBox ID="titleOnlyCB" runat="server" Text="Use Title Only" onclick="_gaq.push(['_trackEvent', 'Packages Control', 'Click', 'Use Title Only checkbox']);" />
                    </div>
                    <div class="field">
                        <asp:CheckBox ID="noParensCB" runat="server" Text="Auto Remove Parentheses" onclick="_gaq.push(['_trackEvent', 'Packages Control', 'Click', 'Auto Remove Parentheses']);" />
                    </div>
                    <div class="field">
                        <asp:TextBox ID="linkText" runat="server" Text="" Columns="3" ToolTip="Provide text that links the package title to the package details (e.g. w/, with, includes, comes with)"></asp:TextBox>
                        <label>
                            Package-Linking Word</label>
                    </div>
                    <asp:GridView CssClass="possible" ID="PossibleOptions" AutoGenerateColumns="false" runat="server" DataSourceID="OptionDS"
                        GridLines="None" DataKeyNames="OptionCode,CategoryList,ExtDescription,NormalizedDescription" ShowFooter="true">
                        <EmptyDataRowStyle CssClass="empty" />
                        <EmptyDataTemplate>
                            <div class="info">
                                No Matches... Try a different option code or search term
                            </div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="first" FooterStyle-CssClass="first">
                                <HeaderTemplate>
                                    <asp:Button ID="BtnAdd" runat="server" Text="+ Add" OnClick="BtnAddClick" OnClientClick="_gaq.push(['_trackEvent', 'Packages Control', 'Click', 'Add Package(s) Button (Top)']);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="ChkAdd" runat="server" onclick="_gaq.push(['_trackEvent', 'Packages Control', 'Click', 'Select Package Checkbox']);" />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Button ID="BtnAddFooter" runat="server" Text="+ Add" OnClick="BtnAddClick" OnClientClick="_gaq.push(['_trackEvent', 'Packages Control', 'Click', 'Add Package(s) Button (Bottom)']);" />
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="OptionCode" HtmlEncode="false" HeaderText="Mfr. Code" ItemStyle-HorizontalAlign="Center" />
                            <asp:TemplateField ItemStyle-CssClass="opCol">
                                <ItemTemplate>
                                    <asp:Label ID="OpTitle" CssClass="OptionHead" Text='<%# Eval("NormalizedDescription").ToString().ToUpper() %>' runat="server"></asp:Label>
                                    <asp:Label ID="OpText" runat="server" Text='<%# Eval("ExtDescription") %>'></asp:Label>
                                    <asp:Panel ID="CustomPanel" runat="server" Visible='<%# Eval("IsCustomized") %>'>
                                        <p class="dealerOption">
                                            Custom
                                            <asp:LinkButton ToolTip="Remove Customization" ID="RemoveCustom" runat="server" CssClass="deleteLink" OnClick="DeleteCustomization_Click"
                                                CommandArgument='<%# Eval("OptionCode") %>' OnClientClick="_gaq.push(['_trackEvent', 'Packages Control', 'Click', 'Remove Custom Package Button']);">&nbsp;</asp:LinkButton>
                                        </p>
                                    </asp:Panel>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="MaxMSRP" HtmlEncode="false" DataFormatString="{0:c0}" HeaderText="Approx. Orig MSRP" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="last" FooterStyle-CssClass="last" />
                        </Columns>
                    </asp:GridView>
                </div>
            </asp:Panel>
        </div>
        <asp:PlaceHolder ID="StandardEquipmentPlaceHolder" runat="server">
            <div id="StandardEquipment" class="tabContent" style="display: none;">
                <asp:ObjectDataSource ID="odsHighlightedStandards" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.EquipmentCollection"
                    SelectMethod="StandardEquipmentSelectHighlighted" OnSelecting="StandardDS_Selecting"></asp:ObjectDataSource>
                <h3>Selected Standard Equipment</h3>
                <asp:GridView ID="gvSelectedEquipment" CssClass="StdList" AutoGenerateColumns="false" runat="server" DataSourceID="odsHighlightedStandards"
                    GridLines="None" OnRowDataBound="gvSelectedEquipment_RowDataBound" DataKeyNames="Sequence,CategoryList" OnSelectedIndexChanged="gvSelectedEquipment_SelectedIndexChanged">
                    <EmptyDataRowStyle CssClass="empty" />
                    <EmptyDataTemplate>
                        <div class="info">
                            No standard equipment selected
                        </div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="first">
                            <ItemTemplate>
                                <asp:LinkButton ID="ImageButton1" ToolTip="Remove Standard Equipment From Vehicle" CssClass="deleteLink" runat="server" CommandName="Select"
                                    OnClientClick="_gaq.push(['_trackEvent', 'Packages Control', 'Click', 'Remove Equipment Button']);">&nbsp;</asp:LinkButton>
                                <asp:CheckBox ID="Highlight" runat="server" Style="display: none;" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-CssClass="opCol last">
                            <ItemTemplate>
                                <asp:Literal ID="HeadingLiteral" runat="server"></asp:Literal>
                                <asp:TextBox ID="UserDesc" runat="server" Text='<%# Eval("NormalizedDescription") %>' TextMode="MultiLine" CssClass="textarea"></asp:TextBox>
                                <br />
                                <asp:LinkButton ID="LnkClearCustomStdSelected" runat="Server" Text="Clear Custom Description" OnClick="LnkClearCustomStdSelectedClick"
                                    CommandArgument='<%# Eval("Sequence") %>' Visible='<%# Eval("HasCustomDescription") %>' OnClientClick="_gaq.push(['_trackEvent', 'Packages Control', 'Click', 'Remove Custom Description (Packages)']);" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <asp:ObjectDataSource ID="StandardDS" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.EquipmentCollection"
                    SelectMethod="StandardEquipmentSelectNotHighlighted" OnSelecting="StandardDS_Selecting"></asp:ObjectDataSource>
                <div>
                    <h3>Available Standard Equipment</h3>
                    <asp:GridView ID="StandardEquipGV" CssClass="StdList" AutoGenerateColumns="false" runat="server" DataSourceID="StandardDS"
                        GridLines="None" OnRowDataBound="Standard_Bound" DataKeyNames="Sequence,CategoryList" ShowFooter="true">
                        <EmptyDataRowStyle CssClass="empty" />
                        <EmptyDataTemplate>
                            <div class="info">
                                No standard equipment details found...
                            </div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                                <HeaderTemplate>
                                    <asp:Button ID="BtnAdd" runat="server" Text="+ Add" OnClick="BtnAddStandardClick" OnClientClick="_gaq.push(['_trackEvent', 'Packages Control', 'Click', 'Add Equipment(s) Button (Top)']);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="Highlight" runat="server" />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Button ID="BtnAddFooter" runat="server" Text="+ Add" OnClick="BtnAddStandardClick" OnClientClick="_gaq.push(['_trackEvent', 'Packages Control', 'Click', 'Add Equipment(s) Button (Bottom)']);" />
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-CssClass="opCol">
                                <ItemTemplate>
                                    <asp:Literal ID="HeadingLiteral" runat="server"></asp:Literal>
                                    <asp:TextBox ID="UserDesc" runat="server" Text='<%# Eval("NormalizedDescription") %>' TextMode="MultiLine" Width="100%" CssClass="textarea"></asp:TextBox>
                                    <br />
                                    <asp:LinkButton ID="LnkClearCustomStd" runat="Server" Text="Clear Custom Description" OnClick="LnkClearCustomStdClick" CommandArgument='<%# Eval("Sequence") %>'
                                        Visible='<%# Eval("HasCustomDescription") %>' OnClientClick="_gaq.push(['_trackEvent', 'Packages Control', 'Click', 'Remove Custom Description (Equipment)']);" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </asp:PlaceHolder>
    </div>
</div>
<asp:ObjectDataSource runat="server" ID="TrimStyleDS" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.ChromeStyle"
    OnSelecting="Style_Selecting" SelectMethod="ChromeTrimStyleNamesSelect">
    <SelectParameters>
        <asp:Parameter Name="vin" Type="String" DefaultValue='' />
    </SelectParameters>
</asp:ObjectDataSource>
