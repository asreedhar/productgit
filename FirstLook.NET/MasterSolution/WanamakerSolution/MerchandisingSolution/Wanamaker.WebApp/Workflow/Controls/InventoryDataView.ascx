<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InventoryDataView.ascx.cs" Inherits="Wanamaker.WebApp.Workflow.Controls.InventoryDataView" %>
<%@ Register Src="~/Workflow/Controls/PrintWindowSticker.ascx" TagPrefix="Controls" TagName="PrintWindowSticker" %>
<%@ Register TagPrefix="cc1" Namespace="FirstLook.Merchandising.WebControls" Assembly="FirstLook.Merchandising.WebControls, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" %>

<asp:Panel ID="runningStatusPanel" CssClass="quickBuildRunningStatus" runat="server">
   
<asp:Panel runat="server" CssClass="inventoryData" >
    
    <asp:HiddenField ID="LastListPriceHidden" runat="server" ClientIDMode="Static" />
    
    <div id="PricingInfo" class="clearfix">

        <asp:Panel id="NewCarPricingInfoPanel" runat="server" Visible="false" CssClass="newCarPricing clearfix" DefaultButton="NewCarRepriceButton">
            <span class="msrp">Original MSRP: <asp:Label id="msrpPrice" runat="server"></asp:Label></span>
            <h3>Internet<br />Price:</h3>
            <div class="fieldLabelPair">
                <asp:TextBox ID="NewCarListPrice" runat="server" CssClass="text"></asp:TextBox>
                <asp:Button ID="NewCarRepriceButton" runat="server" Text="REPRICE" OnClick="OnReprice" CssClass="button small submit" />
                <a class="show_button" href="#show" ><div id="NewCarRepriceButtonWithCampaign" class="button small" style="display:none;">REPRICE</div></a>
                
                <div class="remodal jquimodal" data-remodal-id="show" id="formModal" style="display:none">
                    <div class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable ui-resizable"  tabindex="-1" role="dialog">
                        <div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">
                            <span class="ui-dialog-title">New Vehicle Repricing</span>
                            <a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button">
                                <span class="ui-icon ui-icon-closethick">close</span>
                            </a>
                        </div>
                        <div class="ui-dialog-content ui-widget-content" style="width: auto; min-height: 106px; height: auto;" scrolltop="0" scrollleft="0">
                            <br/>
                            <p runat="server" id="ModalRepriceWarnText">Empty</p>
                            <br/>
                            <a href="#"><div id="ConfirmReprice" class="button small">REPRICE</div></a>
                            <a href="#" style="margin-left:4px"><div id="repriceCancel" class="button-muted small">Cancel</div></a>
                        </div>
                    </div>
                </div>

                
                
                <script type="text/javascript">
                    $(function () {
                        var doRepriceWarn = <asp:Literal runat="server" id="DoRepriceWarn" EnableViewState="false" />;
                        if (doRepriceWarn) {

                            $("#NewCarRepriceButton").hide();
                            $('#NewCarRepriceButtonWithCampaign').show();

                            $('#ConfirmReprice').on("click", function() {
                               
                                $('#NewCarRepriceButtonWithCampaign').hide();
                                $("#NewCarRepriceButton").show();
                                $("#NewCarRepriceButton").click();

                            });

                        }
                    });
                    
                </script>
            </div>
            <div id="PricesSaving"></div>
            <asp:Panel id="NewCarPriceParseError" runat="server" Visible="false" CssClass="error">
                Value entered is not a valid price.
            </asp:Panel>
        </asp:Panel>

        <asp:Panel ID="UsedCarPricingPanel" runat="server" CssClass="clearfix" Visible="false">
            <h3>Internet<br />Price:</h3>
            <span class="price"><asp:Label ID="headerListPrice" runat="server" CssClass="headerListPrice"></asp:Label></span>
            <asp:HyperLink ID="priceLnk" runat="server" CssClass="newWindowLink" options="scrollbars=yes,resizable=yes,width=1024,height=768" onclick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Reprice (Used)']);">Reprice</asp:HyperLink>
        </asp:Panel>

    </div>
    
    <div id="InventoryDataWrap">
    
        <div id="YMMTrimWrap">

            <h2>
                <asp:Label ID="headerYearMakeModel" runat="server" Text='<%# InventoryInfo.Year + " " + InventoryInfo.Make + " " + InventoryInfo.Model %>'></asp:Label>
            </h2>

            <div id="TrimSelector">
                <asp:DropDownList ID="TrimStyleDropDown" runat="server" DataSourceID="TrimStyleDS" DataTextField="Value" DataValueField="Key" Enabled='<%# AllowTrimSelection %>'
                    CssClass="selectBox" OnSelectedIndexChanged="ChromeStyle_Changed" OnDataBound="StyleDDL_DataBound" onchange="_gaq.push(['_trackEvent', 'Approval Page', 'Change', 'Trim']);" />
                <asp:LinkButton ID="trimHelpLinkButton" CssClass="trimHelper" Visible='<%# AllowTrimSelection %>' OnClick="getDiffCategories" runat="server" OnClientClick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Trim Help']);">Trim Help</asp:LinkButton>
            </div>
     
        </div>

        <ul class="vehicle">
            <li><asp:Label ID="headerDoors" runat="server" Text='<%# VehicleAttributes.Doors > 0 ? string.Format("{0} Doors", VehicleAttributes.Doors) : string.Empty %>'></asp:Label></li>
            <li><asp:Label ID="headerMileage" runat="server" Text='<%# InventoryInfo.DisplayMileage %>'></asp:Label> mi.</li>
            <li>
                <a class="dialogLink" target="_blank" href='<%# GetEStockLink() %>'>
                    <asp:Label runat="server" ID="stockNumber" Text='<%# InventoryInfo.StockNumber %>'></asp:Label>
                </a>
            </li>
        </ul>
     
        <ul class="admin clearfix">
        
            <li runat="server" visible='<%# !string.IsNullOrEmpty(Color) %>'><%# Color %></li>
            <li><asp:Label runat="server" ID="day" Text='<%# InventoryInfo.Age %>'></asp:Label> Days</li>
            <li><asp:Label CssClass="smallLabel" ID="tradeOrPurchase" runat="server" Text='<%# InventoryInfo.TradeOrPurchase %>' ></asp:Label></li>
            <li>
                <asp:Label CssClass="vin" ID="vin" runat="server" Text='<%# InventoryInfo.VIN %>'></asp:Label>
                <asp:HiddenField ID="makeHidden" runat="server" />
            </li>
            
            <% if ( GetMaxForWebsiteUrl().Trim() != string.Empty ) { %>
            <li>
                <a href='<%= GetMaxForWebsiteUrl() %>' target="_blank">MAX For Website</a>
            </li>
            <% } %>

            <% if ( ShowLastAdLink ) { %>
            <li id="PreviousAdLink" runat="server">
                <a id="LastAdWindowLink" href="javascript:void(0)" runat="server" Visible='<%# ShowLastAdLink %>' options="width=640,height=480,scrollbars=yes,resizable=yes" onclick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Previous Ad']);">Last Ad Published</a>
                <asp:Label ID="DateAdPublished" runat="server" Visible='<%# InventoryInfo.HasBeenPublished %>' Text='<%# string.Format(" on {0}",InventoryInfo.LastDescriptionPublishedDateText()) %>'></asp:Label>
            </li>
            <li class="checkbox" id="CertifiedControlsItem" runat="server">
                <asp:HiddenField runat="server" ID="ProgramIsPersisted"/>
                <asp:DropDownList ID="ddlCertifiedPrograms" runat="server" AppendDataBoundItems="true" DataTextField="Name" DataValueField="Key" OnDataBound="ddlCertifiedPrograms_DataBound" OnSelectedIndexChanged="ddlCertifiedPrograms_SelectedIndexChanged">
                    <%-- If you change the Text values of these items, be sure to change the same values in the codebehind.  Some of these are dynamically removed based on their text values. --%>
                    <asp:ListItem Text="Not Certified" Value="-1|false"></asp:ListItem>
                    <asp:ListItem Text="Certified" Value="0|false"></asp:ListItem>
                </asp:DropDownList>
                <span runat="server" id="spanDisplayCertifiedID" style="display: none" Visible="False">
                    <span runat="server" id="spanCertifiedID"><asp:Label runat="server" ID="CertifiedIdLabel"></asp:Label>ID
                        <asp:TextBox ID="txtCertifiedID" runat="server" CssClass="certifiedID" MaxLength="15" Text='<%# InventoryInfo.CertifiedID %>' OnTextChanged="txtCertifiedID_TextChanged"></asp:TextBox>
                    </span>
                </span>
            </li>
            <% } %>
            
            <% if ( !DisablePrintWinowSticker ) { %>
                <li><a class="windowStickerToggle dialogLink">Print Window Sticker</a></li>
            <% } %>
     
        </ul>
         
    </div>

    <asp:Panel ID="discriminators" CssClass="trimHelperHolder" runat="server" Visible="false">

        <div class="form">
            <label for="CustomTrimTextbox">Use Custom Trim:</label>
            <input id="CustomTrimTextbox" type="text" runat="server" />
            <input type="button" class="button small" id="CustomTrimSubmit" value="Save" />
        </div>

        <p>Trim from Appraisals</p>
        <cc1:TrimHelper ID="discriminator" runat="server" CssClass="trimHelp" EvenRowCssClass="even" OddRowCssClass="odd" EquipmentIncludedImageUrl="~/App_Themes/LazyBoy/images/checkgreen.gif" />

    </asp:Panel>
    
    <asp:ObjectDataSource runat="server" ID="TrimStyleDS" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.ChromeStyle"
        OnSelecting="StylePieces_Selecting" OnSelected="StylePieces_Selected" SelectMethod="ChromeTrimStyleNamesSelect">
        <SelectParameters>
            <asp:Parameter Name="vin" Type="String" DefaultValue='' />
        </SelectParameters>
    </asp:ObjectDataSource>

</asp:Panel>
<div id="StickerControls" class="clearfix" style="display:none;">
    <Controls:PrintWindowSticker ID="PrintWindowStickerControl" runat="server" InventoryId='<%# InventoryId %>' BusinessUnitId='<%# BusinessUnitId %>' />
</div>
</asp:Panel>
  