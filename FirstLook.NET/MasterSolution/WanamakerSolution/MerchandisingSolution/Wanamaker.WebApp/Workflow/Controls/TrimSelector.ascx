﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TrimSelector.ascx.cs" Inherits="Wanamaker.WebApp.Workflow.Controls.TrimSelector" %>
<%--<asp:DropDownList CSSClass="trimSelect"  ClientIDMode="AutoID" ID="TrimSelect"  runat="server">                
        <asp:ListItem Text="Select trim &hellip;" Value="" Selected="True"/>
</asp:DropDownList>  --%>
<div id="TrimSelect" runat="server" clientidmode="AutoID" class="customSelect">
    <span class="selected">&nbsp;</span>
    <ul>
        <li><a>Select trim &hellip;</a></li>
    </ul>
</div>
<asp:HiddenField ID="HiddenSelectedTrim" runat="server" />
<asp:HiddenField ID="HiddenInventoryId" runat="server" />
