<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WorkflowHeader.ascx.cs" Inherits="Wanamaker.WebApp.Workflow.Controls.WorkflowHeader" %>

<div id="Masthead" class="clearfix">
    <p>MAX<span class="small">Intelligent Online Advertising System</span></p>
    <ul id="UserLinks">
        <% if (!IsMaxStandAlone){ %>
            <li class="first"><a href="../../home" onclick="_gaq.push(['_trackEvent', 'Header Navigation', 'Click', 'Return to FirstLook']);">Return to FirstLook MAX </a></li>
        <% } %>
        <li class="<%= IsMaxStandAlone ? "first" : "" %>"><a href="~/LogOff.aspx" runat="server" onclick="_gaq.push(['_trackEvent', 'Header Navigation', 'Click', 'Log Out']);">Log Out</a></li>
        <li>
            <asp:HyperLink ID="lnkDealerName" runat="server" class="dealerSelector" NavigateUrl="~/DealerChooser.aspx" onclick="_gaq.push(['_trackEvent', 'Header Navigation', 'Click', 'Dealer Selector']);">
                <asp:Label ID="DealerName" runat="server"></asp:Label>
            </asp:HyperLink>
        </li>
    </ul>
</div>

<div id="MainNavigation">
    <ul id="MainNavList">
        <li><asp:HyperLink ID="lnkHome" runat="server" Text="Home" NavigateUrl="~/Dashboard.aspx" onclick="_gaq.push(['_trackEvent', 'Header Navigation', 'Click', 'Home (Executive Dashboard)']);"></asp:HyperLink></li>
        <li><asp:HyperLink ID="HyperLink1" runat="server" Text="Inventory" NavigateUrl="~/Workflow/Inventory.aspx" onclick="_gaq.push(['_trackEvent', 'Header Navigation', 'Click', 'Inventory']);"></asp:HyperLink></li>
        <li class="menuHead" runat="server" id="ReportsMenu">
            <a href="#" onclick="return false;">Reports</a>
            <ul class="clearfix" style="display:none;">
                <li id="VehicleActivityReport" runat="server"><a href="~/Reports/VehicleActivityReport.aspx" runat="server" onclick="_gaq.push(['_trackEvent', 'Header Navigation', 'Click', 'Low Activity Report']);">Low Activity</a></li>
                <li><a href="~/Reports/VehicleOnlineStatusReport.aspx" runat="server" onclick="_gaq.push(['_trackEvent', 'Header Navigation', 'Click', 'Not Online Report']);">Not Online</a></li>
                <li><a href="~/Reports/PerformanceSummaryReport.aspx" runat="server" onclick="_gaq.push(['_trackEvent', 'Header Navigation', 'Click', 'Performance Summary']);">Performance Summary</a></li>
                <li id="liDashboard" runat="server"><a id="lnkDashboard" href="~/Dashboard.aspx" runat="server" onclick="_gaq.push(['_trackEvent', 'Header Navigation', 'Click', 'Executive Dashboard']);">MAX Dashboard</a></li>
                <li id="liGroupDashboard" runat="server"><a id="lnkGroupDashboard" href="~/GroupDashboard.aspx" runat="server" onclick="_gaq.push(['_trackEvent', 'Header Navigation', 'Click', 'Group Level Dashboard']);">Group Level Dashboard</a></li>
            </ul>
        </li>
        
        <li class="menuHead" runat="server" id="SettingsMenu">
            <a href="#" runat="server" onclick="return false;">Settings</a>
            <ul class="clearfix" style="display:none;">
                <li>
                    <asp:HyperLink ID="admin" runat="server" NavigateUrl="~/CustomizeMAX/DealerPreferences.aspx" Text="MAX Settings" onclick="_gaq.push(['_trackEvent', 'Header Navigation', 'Click', 'MAX Settings']);"></asp:HyperLink>
                </li>
                <li>
                <asp:HyperLink ID="wizard" runat="server" NavigateUrl="~/CustomizeMAX/DealerSetup.aspx" Text="Setup Wizard" onclick="_gaq.push(['_trackEvent', 'Header Navigation', 'Click', 'Setup Wizard']);"></asp:HyperLink>        
                </li>
                <% if ( IsAdministrator ) { %>
                <li>
                    <asp:HyperLink ID="adminAdvanced" runat="server" NavigateUrl="~/CustomizeMAX/AdvancedDealerPreferences.aspx" Text="Advanced Settings" onclick="_gaq.push(['_trackEvent', 'Header Navigation', 'Click', 'Advanced Dealer Preferences']);"></asp:HyperLink>
                </li>
                <li>
                    <asp:HyperLink ID="templates" runat="server" NavigateUrl="~/CustomizeMAX/TemplateManager.aspx" Text="Highlights Wizard" onclick="_gaq.push(['_trackEvent', 'Header Navigation', 'Click', 'Highlights Wizard']);"></asp:HyperLink>        
                </li>
                <% } %>
            </ul>
        </li>
        
        <li class="menuHead" runat="server" id="AdminMenu">
            <a href="#" onclick="return false;">Admin</a>
            <ul class="clearfix" style="display:none;">
                <li><a id="A1" href="~/Admin/Admin.aspx" runat="server">Home</a></li>
                <li><a id="A2" href="~/Admin/Snippets/AddSnippet.aspx" runat="server">Snippet Collector</a></li>
                <li><a id="A3" href="~/Admin/AdminTemplateManager.aspx" runat="server">Template Manager</a></li>
                <li><a id="A4" href="~/Admin/BenchmarkEntry.aspx" runat="server" >Benchmark Entry</a></li>
            </ul>
        </li>

    </ul>
    
    <div id="divSearch">
        <asp:TextBox ID="stockSearchTB" runat="server" ToolTip="Search for Make, Model, Stock Number, or VIN"></asp:TextBox>
        <asp:Button ID="stkBtn" runat="server" Text="Search" OnClick="StockSearch_Click" OnClientClick="_gaq.push(['_trackEvent', 'Header Navigation', 'Click', 'Search Button']);" />
        <mwc:DefaultButtonExtender ID="dbe" runat="server" TargetControlID="stkBtn" MonitorElementId="stockSearchTB" />
    </div>
    
    <asp:HyperLink CssClass="bulkUpload" ID="uploader" runat="server" Target="_blank" onclick="_gaq.push(['_trackEvent', 'Header Navigation', 'Click', 'Bulk Upload']);">Bulk<br />Upload</asp:HyperLink>

</div>

<asp:BulletedList 
    ID="notices" 
    runat="server" 
    CssClass="Notices"
    DataSourceID="NoticesDS"
    DataTextField="messageText"></asp:BulletedList>
<asp:SqlDataSource 
    ID="NoticesDS" 
    runat="server" 
    EnableCaching="true" 
    CacheExpirationPolicy="Absolute" 
    CacheDuration="3600" 
    ConnectionString='<%$ ConnectionStrings:Merchandising %>'
    SelectCommandType="StoredProcedure"
    SelectCommand="merchandising.SystemNotices#Fetch"
    ></asp:SqlDataSource>