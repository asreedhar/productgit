using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using FirstLook.Common.Core.Extensions;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using FirstLook.Merchandising.DomainModel.Workflow;
using log4net;
using MAX.Entities;
using MAX.Entities.Enumerations;
using MAX.Entities.Extensions;
using FirstLook.Merchandising.DomainModel.DataAccess;

namespace Wanamaker.WebApp.Workflow.Controls
{
    public partial class WorkflowSelector : WorkflowBaseUserControl
    {
        #region Logging

        private static readonly ILog Log = LogManager.GetLogger(typeof(WorkflowSelector).FullName);

        #endregion

        private const int USED_AND_NEW = 0;

        private readonly ProfileCommon _profile = new ProfileCommon();

        private List<IInventoryData> _currentWorkflowInventoryList;

        public IPhotoServices PhotoService { get; set; }

        /// <summary>
        /// A list of links that can be set as the active link for the current page.
        /// </summary>
        public enum SelectorLink
        {
            None,
            Equipment,
            OptionPackages,
            Photos,
            Pricing,
            Approval
        }


        private List<IInventoryData> CurrentWorkflowInventoryList
        {
            get
            {
                if (_currentWorkflowInventoryList == null)
                    _currentWorkflowInventoryList = GetList();

                return _currentWorkflowInventoryList;
            }
        }

        public int? AgeBucketId
        {
            get
            {
                return WorkflowState.AgeBucketId;
            }
        }

        public int UsedOrNewFilter
        {
            get
            {
                return WorkflowState.UsedOrNewFilter;
            }
        }
        

        public WorkflowType ActiveListType
        {
            get
            {
                return WorkflowState.ActiveListType;
            }
        }

        public SelectorLink ActiveLink { get; set; }

        public bool VehicleIsPreOwned
        {
            get
            {
                //New vehicles 
                ////Removed for the sake of TrueCar (don't want to blow away this code yet)
                if( ViewState["VehicleIsPreOwned"] != null )
                {
                    return (bool) ViewState["VehicleIsPreOwned"];
                }

                Log.Debug("Setting ViewState[VehicleIsPreOwned] is null. Setting it to true");
                ViewState["VehicleIsPreOwned"] = true;
                return true;
            }
            set
            {
                Log.DebugFormat("Setting ViewState[VehicleIsPreOwned] to {0}", value);
                ViewState["VehicleIsPreOwned"] = value;
            }
        }

        public bool HasMarketingUpgrade
        {
            get
            {
                if( ViewState["HasMarketingUpgrade"] != null )
                {
                    return (bool)ViewState["HasMarketingUpgrade"];
                }

                Log.Debug("Setting ViewState[HasMarketingUpgrade] is null. Setting it to false");
                ViewState["HasMarketingUpgrade"] = false;
                return true;
            }
            set
            {
                Log.DebugFormat("Setting ViewState[HasMarketingUpgrade] to {0}", value);
                ViewState["HasMarketingUpgrade"] = value;
            }
        }

        public string NextUrl { get; set; }

        public string BulkUploadUrl
        {
            get
            {
                BusinessUnit bu = Context.Items[ "BusinessUnit" ] as BusinessUnit;
                return bu != null ? PhotoService.GetBulkUploadManagerUrl( bu.Id, PhotoManagerContext.MAX ) : "";
            }
        }
       
        protected void Page_Load(object sender, EventArgs e)
        {
            Log.DebugFormat("Page_Load() called from sender {0} with EventArgs {1}", sender, e);

            SetDisplay();
        }

        private List<IInventoryData> GetList()
        {
            return GetList(BusinessUnitId, ActiveListType, AgeBucketId, UsedOrNewFilter);
        }
        
        //Public Methods
        /// <summary>
        /// Gets list of sorted inventory data
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <param name="wfType"></param>
        /// <param name="ageBucketId"></param>
        /// <param name="newUsed"></param>
        /// <returns></returns>
        private static List<IInventoryData> GetList(int businessUnitId, WorkflowType wfType, int? ageBucketId, int newUsed)
        {
            var sortedInventoryData = new List<IInventoryData>();
            Log.TraceFormat("GetList() called.");
            var sortDirection = HttpContext.Current.Session.GetOrSetDefault(SortDirection.Ascending, SessionKey.InventorySortDirection);
            var sortType = HttpContext.Current.Session.GetOrSetDefault(SortType.InventoryReceivedDate, SessionKey.InventorySortType);
            //For some odd reason, when the app is loaded as an admin, and the user is brought to DealerSelector.aspx,
            //this control is instaniated and this method being called.  DealerSelector has nothing to do with Inventory
            //so hacking it up to check if a BusinessUnitID exists first or not.  This should speed DealerSelector.aspx a lot.

            if (businessUnitId != 0)
            {
                var filteredInventory = InventoryDataFilter
                    .FetchListFromSession(businessUnitId)
                    .WorkflowFilter(wfType, ConditionFilters.CreateFromInt(newUsed),
                        AgeBucketFilters.CreateFromBucketFilterMode(ageBucketId)).ToList();

                var sorter = new InventoryDataSorter();
                sortedInventoryData = sorter.SortInventory(filteredInventory, sortType, sortDirection);
                
                Log.TraceFormat("BusinessUnitID is {0}. InventoryIdList size {1}.", businessUnitId, sortedInventoryData.Count);
            }

            return sortedInventoryData;
        }

        private LocateVehicleResults FindVehicle(int inventoryId, WorkflowType wfType, int? ageBucketId, int newUsed)
        {
            LocateVehicleResults result = new LocateVehicleResults() { FoundResult = false, ActiveListType = wfType, AgeBucketId = ageBucketId, UsedOrNewFilter = newUsed };
            var inventoryList = GetList(BusinessUnitId, result.ActiveListType, result.AgeBucketId, result.UsedOrNewFilter);

            if (inventoryList.Any(i => i.InventoryID.Equals(inventoryId)))
            {
                Log.DebugFormat("The InventoryIdList contains the specified inventoryId {0}.", inventoryId);

                result.FoundResult = true;
                return result;
            }

            return result;
        }

        public LocateVehicleResults LocateVehicle(int inventoryId)
        {
            //try current filter.
            LocateVehicleResults result = LocateVehicleCurrentFilter(inventoryId, ActiveListType, AgeBucketId, UsedOrNewFilter);
            if (result.FoundResult)
                return result;

            return LocateVehicleOpenFilter(inventoryId); //get it from all inventory
        }

        public LocateVehicleResults LocateVehicleCurrentFilter(int inventoryId, WorkflowType wfType, int? ageBucketId, int newUsed)
        {
            return FindVehicle(inventoryId, wfType, ageBucketId, newUsed);            
        }

        public LocateVehicleResults LocateVehicleOpenFilter(int inventoryId)
        {
            return FindVehicle(inventoryId, WorkflowType.AllInventory, null, USED_AND_NEW);
        }

        protected void SetWorkflowTabs()
        {
            Log.Debug("SetWorkflowTabs() called.");

            // Setup links
            ApprovalLink.NavigateUrl = WorkflowState.CreateUrl( "~/Workflow/ApprovalSummary.aspx" );
            LoadPackagesLink.NavigateUrl = WorkflowState.CreateUrl( "~/Workflow/LoadPackages.aspx" );
            LoadEquipmentLink.NavigateUrl = WorkflowState.CreateUrl( "~/Workflow/LoadEquipment.aspx" );
            LoadPhotosLink.NavigateUrl = WorkflowState.CreateUrl( "~/Workflow/Photos.aspx" );

            var result = LocateVehicle(InventoryId);
            PricingPageLink.NavigateUrl = DealerPingAccess.CheckDealerPingAccess(BusinessUnitId) ?
                WorkflowState.CreatePricingPingUrl("~/PricingAnalysis/pingone", InventoryId, BusinessUnitId,result.ActiveListType,result.UsedOrNewFilter,result.AgeBucketId)
                : WorkflowState.CreateUrl( "~/Workflow/Pricing.aspx" )  ;
            MAXSellingLink.NavigateUrl = InventoryData.GetPricingLink( BusinessUnitId, InventoryId, ConfigurationManager.AppSettings[ "marketing_page_from_host" ] );

            // Set active tab
            switch ( ActiveLink )
            {
                case SelectorLink.Equipment:
                    EquipmentMenuItem.Attributes[ "class" ] += " selected";
                    break;
                case SelectorLink.OptionPackages:
                    PackagesMenuItem.Attributes[ "class" ] += " selected";
                    break;
                case SelectorLink.Photos:
                    PhotosMenuItem.Attributes[ "class" ] += " selected";
                    break;
                case SelectorLink.Pricing:
                    PricingMenuItem.Attributes[ "class" ] += " selected";
                    break;
                case SelectorLink.Approval:
                    ApprovalMenuItem.Attributes[ "class" ] += " selected";
                    break;
            }

            // Set statuses
            var statuses = StepStatusCollection.Fetch(BusinessUnitId, InventoryId);

            var statusOfExteriorEquipment = statuses.GetStatus(StepStatusTypes.ExteriorEquipment);
            var statusOfInteriorEquipment = statuses.GetStatus(StepStatusTypes.InteriorEquipment);

            Log.DebugFormat("Status of ExteriorEquipment is {0}", statusOfExteriorEquipment);
            Log.DebugFormat("Status of InteriorEquipment is {0}", statusOfInteriorEquipment);

            if ( statusOfExteriorEquipment != ActivityStatusCodes.Complete || 
                 statusOfInteriorEquipment != ActivityStatusCodes.Complete )
            {
                Log.Debug( "Setting needs equipment and needs options images to visible." );
                LoadEquipmentLink.CssClass += " needsEquipment ";
                LoadPackagesLink.CssClass += " needsEquipment ";
                LoadEquipmentLink.ToolTip = @"Needs Equipment or Packages";
                LoadPackagesLink.ToolTip = @"Needs Equipment or Packages";
            }
            //else
            //{
            //    Log.Debug("Setting needs equipment and needs options images to NOT visible.");
            //    LoadEquipmentLink.CssClass.Replace(" needsEquipment ", string.Empty);
            //    LoadPackagesLink.CssClass.Replace( " needsEquipment ", string.Empty );
            //    LoadEquipmentLink.ToolTip = string.Empty;
            //    LoadPackagesLink.ToolTip = string.Empty;
            //}

            var statusOfPhotos = statuses.GetStatus(StepStatusTypes.Photos);
            Log.DebugFormat( "Status of Photos is {0}", statusOfPhotos );
            LoadPhotosLink.CssClass += statusOfPhotos != ActivityStatusCodes.Complete ? " needsPhotos " : string.Empty;
            LoadPhotosLink.ToolTip += statusOfPhotos != ActivityStatusCodes.Complete ? "Needs Photos" : string.Empty;

            var statusOfPricing = statuses.GetStatus(StepStatusTypes.Pricing);
            Log.DebugFormat("Status of Pricing is {0}", statusOfPricing);
            Log.DebugFormat("Vehicle is pre-owned is {0}", VehicleIsPreOwned);

            if (statusOfPricing != ActivityStatusCodes.Complete)
            {
                Log.Debug( "Setting needs pricing image to visible." );
                PricingPageLink.CssClass += " needsPricing ";
                PricingPageLink.ToolTip = @"Needs Pricing";
            }
            //else
            //{
            //    Log.Debug( "Setting needs pricing image to NOT visible." );
            //    PricingPageLink.CssClass.Replace( " needsPricing ", string.Empty );
            //    PricingPageLink.ToolTip = string.Empty;
            //}

            PricingMenuItem.Visible = VehicleIsPreOwned;
            MaxSEMenuItem.Visible = VehicleIsPreOwned && HasMarketingUpgrade;
            
        }

        private void OpenWorkflow()
        {
            var result = LocateVehicleOpenFilter(InventoryId);
            if (!result.FoundResult)
                throw new InvalidOperationException("Could not find inventory");

            string url = WorkflowState.CreateUrl(InventoryId, result.ActiveListType, result.UsedOrNewFilter, result.AgeBucketId);
            Response.Redirect(url);
        }

        private bool FindPrevious(out int inventoryId)
        {
            inventoryId = -1;
            int currentIndex = CurrentWorkflowInventoryList.FindIndex(x => x.InventoryID == InventoryId);

            if(currentIndex < 0) //Sometimes the car can't be found in the workflow. Copying links?
                OpenWorkflow();

            if (currentIndex == 0)
                return false;

            inventoryId = CurrentWorkflowInventoryList[currentIndex - 1].InventoryID;
            return true;
        }

        private bool FindNext(out int inventoryId)
        {
            inventoryId = -1;
            int currentIndex = CurrentWorkflowInventoryList.FindIndex(x => x.InventoryID == InventoryId);

            if (currentIndex < 0) //Sometimes the car can't be found in the workflow. Copying links?
                OpenWorkflow();

            if (currentIndex == (CurrentWorkflowInventoryList.Count - 1))
                return false;

            inventoryId = CurrentWorkflowInventoryList[currentIndex + 1].InventoryID;
            return true;
        }


        private bool GetPreviousUrl(out string url)
        {
            url = string.Empty;
            int inventoryid;
            bool nextFound = FindPrevious(out inventoryid);

            if (nextFound)
                url = WorkflowState.CreateUrl(HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath, inventoryid, ActiveListType, UsedOrNewFilter, AgeBucketId);

            return nextFound;
        }

        public bool GetNextUrl(out string url) // 20806 should use session sort order
        {
            url = string.Empty;
            int inventoryid;
            bool nextFound = FindNext( out inventoryid );

            if ( nextFound )
            {
                /* 
                   If we are in a pricing bucket & on the Approval page, make
                   the 'next' button go to the pricing page for the next car
                */
                string baseurl = ( ( ActiveListType == WorkflowType.DueForRepricing ||
                                  ActiveListType == WorkflowType.NoPrice ) &&
                                  HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath == "~/Workflow/ApprovalSummary.aspx" )
                                     ? "~/Workflow/Pricing.aspx"
                                     : HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath;

                url = WorkflowState.CreateUrl(baseurl, inventoryid, ActiveListType, UsedOrNewFilter, AgeBucketId);
            }

            return nextFound;
        }

        private void SetDisplay()
        {
            Log.Debug("SetDisplay() called.");

            string url;
            if (GetNextUrl(out url))
            {
                NextUrl = url;
                btnNext.NavigateUrl = url;
            }
            else
            {
                btnNext.Enabled = false;
            }

            if (GetPreviousUrl(out url))
                btnPrevious.NavigateUrl = url;
            else
                btnPrevious.Enabled = false;

            int currentIndex = CurrentWorkflowInventoryList.FindIndex(x => x.InventoryID == InventoryId);
            lblCurrentItem.Text = (currentIndex + 1) + @" of " + CurrentWorkflowInventoryList.Count;
            SetWorkflowTabs();
        }


        public event EventHandler InventorySearching;

        protected void OnInventorySearching(object sender, EventArgs e)
        {
            if (InventorySearching != null)
                InventorySearching(this, EventArgs.Empty);
            
        }

        public string SearchText
        {
            get { return InventorySearch.SearchText; }
            set { InventorySearch.SearchText = value; }
        }
    }
}