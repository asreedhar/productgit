﻿using System;
using System.Globalization;
using System.Text;
using FirstLook.Merchandising.DomainModel.Workflow.Aging;

namespace Wanamaker.WebApp.Workflow.Controls
{
    public partial class AgeFilterView : System.Web.UI.UserControl, IAgeFilterView
    {
        public IAgeFilterPresenter Presenter { get; private set; }

        protected IAgeFilterViewModel ViewModel { 
            get { return Presenter.GetViewModel(); }
        }

        protected override void OnInit(EventArgs e)
        {
            Presenter = new AgeFilterPresenter(this, (IAgeFilterModel) Page);
            Presenter.Initialize();
            base.OnInit(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            Presenter.Load();
            base.OnLoad(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            Presenter.PreRender();
            AgeSelector.DataSource = ViewModel.AgeBuckets;
            AgeSelector.DataBind();
            AgeSelector.SelectedValue = ViewModel.CurrentAgeBucketId.HasValue 
                ? ViewModel.CurrentAgeBucketId.Value.ToString(CultureInfo.InvariantCulture) 
                : "";
            base.OnPreRender(e);
        }
 
        protected void OnAgeSelectorChanged(object sender, EventArgs e)
        {
            int v;
            var ageBucketId = !string.IsNullOrWhiteSpace(AgeSelector.SelectedValue) &&
                               int.TryParse(AgeSelector.SelectedValue, out v)
                                   ? (int?) v
                                   : null;

            if (AgeBucketClicked != null)
                AgeBucketClicked(ageBucketId);
        }

        protected string CssClass(bool isFirst = false, bool isSelected = false, bool noCategory = false, string staticClasses = null)
        {
            var b = new StringBuilder(32);
            AppendIf(staticClasses != null, b, staticClasses);
            AppendIf(isFirst, b, "first");
            AppendIf(isSelected, b, "selected");
            AppendIf(noCategory, b, "noCategory");
            return b.ToString();
        }

        private static void AppendIf(bool condition, StringBuilder b, string className)
        {
            if (!condition) return;
            if (b.Length > 0) b.Append(" ");
            b.Append(className);
        }

        public Action<int?> AgeBucketClicked { get; set; }

        protected static string GoogleAnalytics(string dropDownElementId)
        {
            return string.Format(
                "_gaq.push(['_trackEvent', 'AgeFilter', 'Click', document.getElementById('{0}').options[document.getElementById('{0}').selectedIndex].text]);",
                dropDownElementId);
        }
    }
}