using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using FirstLook.Merchandising.DomainModel.Vehicles;
using VehicleDataAccess;

namespace Wanamaker.WebApp.Workflow.Controls
{
    public partial class QuickPackages : WorkflowBaseUserControl
    {
        public event EventHandler PackageAdded;
        public event EventHandler PackageDeleted;

        private DetailedEquipmentCollection _equipment;

        public int ChromeStyleId
        {
            get { return (int)ViewState["quickpackages_chromestyleid"]; }
            set { ViewState["quickpackages_chromestyleid"] = value; }
        }

        public DetailedEquipmentCollection Equipment
        {
            get
            {
                if (_equipment == null)
                {
                    DetailedEquipmentCollection tmp = DetailedEquipmentCollection.FetchSelections(BusinessUnitId, InventoryId);
                    tmp.ClearStandardHighlights();
                    _equipment = tmp;
                }
                return _equipment;
            }
             set { _equipment = value; }
        }
        public string ContextKey
        {
            get
            {
                return ChromeStyleId + "," + BusinessUnitId;
            }
        }

        public int SelectedCount { get; set; }

        protected void OnPackageAdded()
        {
            if(PackageAdded != null)
                PackageAdded(this, EventArgs.Empty);
        }

        protected void OnPackageDeleted()
        {
            if (PackageDeleted != null)
                PackageDeleted(this, EventArgs.Empty);
        }

        protected void Page_PreRender ( object sender, EventArgs e )
        {
            acePackages.ContextKey = ContextKey;
            SetupSelectedPackages();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //Setup page controls
            txtName.Text = String.Empty;
            hdnCatList.Value = String.Empty;
            txtDescription.Text = String.Empty;
            lblOptionCode.Text = String.Empty;
            pnlAddPackage.Visible = false;
        }
        protected void btnAddPackage_Click(object sender, EventArgs e)
        {
            EquipmentCollection possible = EquipmentCollection.FetchOptionalByOptionCode(BusinessUnitId, InventoryId, ChromeStyleId, String.Empty);
            bool isFound = false;

            string[] splits;
            if( txtPackages.Text.Contains( " -- " ) )
            {
                //Extract OptionCode from inputted text
                splits = txtPackages.Text.Split( new[] { " -- " }, StringSplitOptions.RemoveEmptyEntries );
            }
            else
            {
                //Multiple OptionCodes are separated by spaces.
                splits = txtPackages.Text.Split( new[] { " " }, StringSplitOptions.RemoveEmptyEntries );
                foreach( string option in splits )
                {
                    foreach( Equipment item in possible )
                    {
                        if( item.OptionCode.Equals( option, StringComparison.InvariantCultureIgnoreCase ) )
                        {
                            AddOptionToVehicleEquipment( item.OptionCode, item.NormalizedDescription, item.CategoryList, item.NormalizedDescription + @":" + item.ExtDescription );
                            txtPackages.Text = String.Empty;
                            isFound = true;
                        }
                    }
                }
                lblDoesNotExist.Visible = !isFound;
                return;
            }

            //Get the information for the package
            if (splits.Length > 0)
            {
                foreach (Equipment item in possible)
                {
                    if (item.OptionCode == splits[0])
                    {
                        txtName.Text = item.NormalizedDescription;
                        hdnCatList.Value = item.CategoryList;
                        txtDescription.Text = item.NormalizedDescription + @":" + item.ExtDescription;
                        lblOptionCode.Text = item.OptionCode;
                        isFound = true;
                    }
                }
            }

            if (isFound)
            {
                lblDoesNotExist.Visible = false;
                pnlAddPackage.Visible = true;
            }
            else
            {
                lblDoesNotExist.Visible = true;
            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            //Add new DetailedEquipment to the collection for this vehicle
            AddOptionToVehicleEquipment( lblOptionCode.Text, txtName.Text, hdnCatList.Value, txtDescription.Text );

            //Setup page controls
            txtName.Text = String.Empty;
            hdnCatList.Value = String.Empty;
            txtDescription.Text = String.Empty;
            lblOptionCode.Text = String.Empty;
            txtPackages.Text = String.Empty;
            pnlAddPackage.Visible = false;
        }

        protected void BtnDeleteClick(object sender, EventArgs e)
        {
            //Delete the selected package
            DetailedEquipmentCollection tmp = Equipment;
            var btnSender = (LinkButton)sender;
            string optionCode = btnSender.CommandArgument;
            tmp.Remove(optionCode);
            Equipment = tmp;
            Equipment.Save(BusinessUnitId, InventoryId);

            OnPackageDeleted();
        }

        public void RemoveAllPackages()
        {
            Equipment.Clear();
            Equipment.Save(BusinessUnitId, InventoryId);
            SetupSelectedPackages();

        }

        public void SetupSelectedPackages()
        {
            rptSelectedPackages.DataSource = Equipment;
            rptSelectedPackages.DataBind();

            if ( rptSelectedPackages.Items.Count == 0 )
            {
                HtmlGenericControl control = Page.FindControl( "QuickPackages" ) as HtmlGenericControl;
                if ( control != null )
                {
                    control.Attributes[ "class" ] += " attention";
                }
            }
        }

        private void AddOptionToVehicleEquipment( string optionCode, string shortDescription, string categoryList, string description )
        {
            //Add new DetailedEquipment to the collection for this vehicle
            Equipment.Add( new DetailedEquipment( optionCode, shortDescription, categoryList, description, false ) );
            Equipment.Save( BusinessUnitId, InventoryId );

            SetupSelectedPackages();
            OnPackageAdded();
        }
    }
}