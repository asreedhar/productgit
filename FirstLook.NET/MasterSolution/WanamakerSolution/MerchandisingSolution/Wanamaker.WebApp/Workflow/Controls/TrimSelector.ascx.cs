﻿using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using FirstLook.Merchandising.DomainModel.Workflow;
using log4net;
using MAX.Entities.Filters;
using Merchandising.Messages;

namespace Wanamaker.WebApp.Workflow.Controls
{
    public partial class TrimSelector : System.Web.UI.UserControl
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(TrimSelector).FullName);

        public int BusinessUnitId { get { return WorkflowState.BusinessUnitID; }}

        public int InventoryId { get; set; }

        public string Vin { get; set; }

        public UsedOrNewFilter UsedOrNewFilter { get; set; }

        public int? ChromeStyleId { get; set; }

        public IQueueFactory QueueFactory { get; set; }

        public void DataBind(InventoryData data)
        {
            Vin = data.VIN;
            ChromeStyleId = data.ChromeStyleId;
            InventoryId = data.InventoryID;
            HiddenInventoryId.Value = InventoryId.ToString();

            SetVisibility();
            AddTrimSelectAttributes();
        }

        private void SetVisibility()
        {
            Visible = !(ChromeStyleId.HasValue && ChromeStyleId.Value > 0);
        }

        private void AddTrimSelectAttributes()
        {
            if (string.IsNullOrWhiteSpace(Vin)) return;

            TrimSelect.Attributes[ "buid" ] = BusinessUnitId.ToString();
            TrimSelect.Attributes[ "vin" ] = Vin;
            TrimSelect.Attributes[ "inventoryId" ] = InventoryId.ToString();
            TrimSelect.Attributes[ "usedNewFilter" ] = ((int)UsedOrNewFilter).ToString();
        }
            
    }
}