﻿using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Workflow.Aging;

namespace Wanamaker.WebApp.Workflow.Controls
{
    public partial class CurrentFilterView : System.Web.UI.UserControl
    {
        public IList<string> CurrentWorkflowSegments
        {
            set
            {
                Segments.DataSource = value;
                Segments.DataBind();
            }
        }

        public string CurrentSearchText
        {
            set
            {
                SearchText.Text = string.IsNullOrEmpty(value) 
                    ? "" 
                    : string.Format("Inventory Search: {0}", value);
                UpdateVisibility();
            }
        }

        private void UpdateVisibility()
        {
            var isSearchVisible = !string.IsNullOrEmpty(SearchText.Text);
            Segments.Visible = !isSearchVisible;
            AgeBucket.Visible = !isSearchVisible;
            Count.Visible = !isSearchVisible;
            AgeFilter.Visible = !isSearchVisible;
            
            SearchText.Visible = isSearchVisible;
            ResetSearchLink.Visible = isSearchVisible;
        }

        public int CurrentWorkflowCount
        {
            set { Count.Text = "(" + value + ")"; }
        }

        public IAgeBucket CurrentAgeBucket
        {
            set { AgeBucket.Text = value != null ? value.Description : ""; }
        }

        public AgeFilterView AgeFilterControl
        {
            get { return AgeFilter; }
        }

        public event EventHandler ResetSearch;

        protected void OnResetSearchClicked(object sender, EventArgs e)
        {
            if (ResetSearch != null)
                ResetSearch(this, EventArgs.Empty);
        }
    }
}