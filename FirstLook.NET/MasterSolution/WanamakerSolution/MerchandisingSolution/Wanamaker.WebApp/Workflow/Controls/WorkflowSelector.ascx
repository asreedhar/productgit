<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="WorkflowSelector.ascx.cs" Inherits="Wanamaker.WebApp.Workflow.Controls.WorkflowSelector"%>
<%@ Import Namespace="Wanamaker.WebApp.Workflow" %>
<%@ Register TagPrefix="workflow" TagName="InventorySearch" Src="~/Workflow/Controls/SearchInventory.ascx" %>

<div id="WorkflowHeader" class="clearfix">

    <div class="clearfix">

        <div id="WorkflowMenu">
            <ul>
                <li class="first equipment" runat="server" id="EquipmentMenuItem"><asp:HyperLink id="LoadEquipmentLink" runat="server" onclick="_gaq.push(['_trackEvent', 'Workflow Selector', 'Click', 'Equipment']);">Equipment</asp:HyperLink></li>
                <li class="packages" runat="server" id="PackagesMenuItem"><asp:HyperLink id="LoadPackagesLink" runat="server" onclick="_gaq.push(['_trackEvent', 'Workflow Selector', 'Click', 'Option Packages']);">Option Packages</asp:HyperLink></li>
                <li class="photos" runat="server" id="PhotosMenuItem"><asp:HyperLink id="LoadPhotosLink" runat="server" onclick="_gaq.push(['_trackEvent', 'Workflow Selector', 'Click', 'Photos']);">Photos</asp:HyperLink></li>
                <li class="pricing" id="PricingMenuItem" runat="server"><asp:HyperLink id="PricingPageLink" runat="server" onclick="_gaq.push(['_trackEvent', 'Workflow Selector', 'Click', 'Pricing']);">Pricing</asp:HyperLink></li>
                <li class="approval" runat="server" id="ApprovalMenuItem"><asp:HyperLink ID="ApprovalLink" runat="server" CssClass="approval" onclick="_gaq.push(['_trackEvent', 'Workflow Selector', 'Click', 'Apprvoal']);">Approval </asp:HyperLink></li>
                <li class="maxSe" id="MaxSEMenuItem" runat="server"><asp:HyperLink ID="MAXSellingLink" runat="server" onclick="_gaq.push(['_trackEvent', 'Workflow Selector', 'Click', 'MAX SE']);">MAX Merchandising</asp:HyperLink></li>
            </ul>
        </div>

        <workflow:InventorySearch id="InventorySearch" runat="server" OnInventorySearching="OnInventorySearching"></workflow:InventorySearch>

        <div id="BulkUpload">
            <a href="<%= BulkUploadUrl %>" title="Upload photos for multiple vehicles" target="_blank">Bulk Upload</a>
        </div>

        <div id="WorkflowSelector">
            <ul>
                <li class="previous"><asp:HyperLink ID="btnPrevious" runat="server" onclick="_gaq.push(['_trackEvent', 'Workflow Selector', 'Click', 'Previous Vehicle']);">&lt; Previous</asp:HyperLink></li>
                <li class="count"><asp:Label ID="lblCurrentItem" runat="server"></asp:Label></li>
                <li class="next"><asp:HyperLink ID="btnNext" runat="server" onclick="_gaq.push(['_trackEvent', 'Workflow Selector', 'Click', 'Next Vehicle']);">Next &gt;</asp:HyperLink></li>
            </ul>
        </div>

    </div>

</div>