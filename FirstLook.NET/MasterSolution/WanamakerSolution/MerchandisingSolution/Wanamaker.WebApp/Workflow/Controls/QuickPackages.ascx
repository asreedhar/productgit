<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QuickPackages.ascx.cs" Inherits="Wanamaker.WebApp.Workflow.Controls.QuickPackages" %>

<asp:Panel ID="pnlPackages" runat="server" CssClass="config" DefaultButton="btnAddPackage">
    <div id="AddPackageContainer">
        <asp:Label ID="lblDoesNotExist" CssClass="red" runat="server" Text="The selected package is not available.<br>" Visible="false"></asp:Label>
        
        <label for="quickPackages1_txtPackages">Search</label>
        <ajax:AutoCompleteExtender ID="acePackages" runat="server" TargetControlID="txtPackages" ServiceMethod="GetPackages" ServicePath="~/WebServices/PackagesService.asmx"
            CompletionInterval="500" MinimumPrefixLength="1" CompletionListCssClass="quickPackageSearch">
        </ajax:AutoCompleteExtender>
        <asp:TextBox ID="txtPackages" runat="server" AutoCompleteType="none" CssClass="textbox" Wrap="false" ></asp:TextBox>

        <asp:Button ID="btnAddPackage" runat="server" Text="+ Add" OnClick="btnAddPackage_Click" CssClass="button small" OnClientClick="_gaq.push(['_trackEvent', 'Quick Packages', 'Click', 'Add Package']);" />
    </div>
    <%--<p style="font-size:0.75em; font-style:italic; color:#555; padding-left:10px;" title="Multiple package codes may be entered, separated by spaces.">Separate multiple package codes with spaces.</p>--%>
    <asp:Repeater ID="rptSelectedPackages" runat="server">
        <HeaderTemplate><ul id="PackagesUL" class="clearfix"></HeaderTemplate>
        <ItemTemplate>
            <li class="clearfix">
                <asp:LinkButton ID="BtnDelete" ToolTip="Remove this package" runat="server" CssClass="deleteLink" OnClick="BtnDeleteClick" CommandArgument='<%# Eval("OptionCode") %>' OnClientClick="_gaq.push(['_trackEvent', 'Quick Packages', 'Click', 'Remove Package']);" />
                <asp:Label ID="LblTitle" runat="server" Text='<%# Eval("OptionTextPreview") %>' ToolTip='<%# Eval("QuickPackagesToolTip") %>'></asp:Label>
            </li>
        </ItemTemplate>
        <FooterTemplate></ul></FooterTemplate>
    </asp:Repeater>
</asp:Panel>

<asp:Panel id="pnlAddPackage" runat="server" Visible="false">
    <div id="divAdd" runat="server">
        <div class="pkgCode">
            <label>Package code:</label>
            <asp:Label ID="lblOptionCode" runat="server" CssClass="bold"></asp:Label>
            <asp:HiddenField ID="hdnCatList" runat="server" />
        </div>
        <div class="pkgName pkgDesc">
            <asp:TextBox ID="txtName" runat="server" Width="442px"></asp:TextBox>
            <br />
            <asp:TextBox ID="txtDescription" runat="server" TextMode="multiline" Width="442px"></asp:TextBox>
        </div>
        <div class="modalButtons">
            <asp:Button ID="btnConfirm" runat="server" Text="+ Add" OnClick="btnConfirm_Click" OnClientClick="_gaq.push(['_trackEvent', 'Quick Packages', 'Click', 'Confirm Add Package']);" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" OnClientClick="_gaq.push(['_trackEvent', 'Quick Packages', 'Click', 'Cancel Add Package']);" />
        </div>
    </div>
</asp:Panel>