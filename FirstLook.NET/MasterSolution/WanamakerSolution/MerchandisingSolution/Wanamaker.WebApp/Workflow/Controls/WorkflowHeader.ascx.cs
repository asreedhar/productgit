using System;
using System.Configuration;
using System.Web.UI;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Properties;
using FirstLook.Merchandising.DomainModel.Workflow;
using Wanamaker.WebApp.AppCode;

namespace Wanamaker.WebApp.Workflow.Controls
{
    public partial class WorkflowHeader : UserControl
    {
        public bool IsAdministrator;

        public bool IsMaxStandAlone = WebSettings.IsMaxStandAlone;

        /// <summary>
        /// A list of contextual actions available when searching for inventory.
        /// </summary>
        public enum SearchType
        {
            CurrentPage,
            HomePage
        }

        /// <summary>
        /// The action to perform upon performing an inventory search (Defaults to HomePage).
        /// </summary>
        public SearchType SearchAction
        {
            get
            {
                if ( ViewState[ "HeaderSearchAction" ] != null )
                {
                    return ( SearchType )ViewState[ "HeaderSearchAction" ];
                }

                ViewState[ "HeaderSearchAction" ] = SearchType.HomePage;
                return SearchType.HomePage;

            }
            set
            {
                ViewState[ "HeaderSearchAction" ] = value;
            }
        }

        /// <summary>
        /// The search keywords entered into the textbox by the user.
        /// </summary>
        public string SearchText
        {
            get
            {
                return stockSearchTB.Text;
            }
        }

        public IPhotoServices PhotoService { get; set; }

        protected void Page_Load ( object sender, EventArgs e )
        {

            IsAdministrator = Page.User.IsInRole( "Administrator" );

            if ( !Page.IsPostBack )
            {
                BusinessUnit bu = Context.Items[ "BusinessUnit" ] as BusinessUnit;
                if ( bu != null )
                {
                    DealerName.Text = bu.Name;
                    uploader.NavigateUrl = PhotoService.GetBulkUploadManagerUrl( bu.Id, PhotoManagerContext.MAX );
                }

                // Hide/show menu items
                ReportsMenu.Visible = Page.User.IsInRole( "Report Viewer" ) || IsAdministrator;

                liDashboard.Visible = ReportsMenu.Visible && IsDashboardEnabled;
                liGroupDashboard.Visible = ReportsMenu.Visible && IsGroupDashboardEnabled;

                VehicleActivityReport.Visible =
                    WhiteListHelper.IsInWhiteList( bu == null ? "" : bu.BusinessUnitCode,
                                                  Settings.Default.VehicleActivityReport_BusinessUnit_Whitelist );

                AdminMenu.Visible = IsAdministrator;

            }
        }

        protected void StockSearch_Click ( object sender, EventArgs e )
        {
            OnInventorySearching( EventArgs.Empty );
        }

        //Custom Events
        public event EventHandler InventorySearching;
        protected virtual void OnInventorySearching ( EventArgs e )
        {
            if ( InventorySearching != null )
                InventorySearching( this, e );
        }

        private readonly Lazy<bool> IsDashboardEnabledLazy = new Lazy<bool>(
            () => GetMiscSettings.GetSettings(WorkflowState.BusinessUnitID).ShowDashboard);

        private bool IsDashboardEnabled { get { return IsDashboardEnabledLazy.Value;  } }

        private readonly Lazy<bool> IsGroupDashboardEnabledLazy =
            new Lazy<bool>(() => GetMiscSettings.GetSettings(WorkflowState.BusinessUnitID).ShowGroupLevelDashboard);

        private bool IsGroupDashboardEnabled { get { return IsGroupDashboardEnabledLazy.Value; } }
    }
}