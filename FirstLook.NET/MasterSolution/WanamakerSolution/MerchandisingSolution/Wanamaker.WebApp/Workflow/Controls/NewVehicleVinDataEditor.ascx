<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="NewVehicleVinDataEditor.ascx.cs" Inherits="Wanamaker.WebApp.Workflow.Controls.NewVehicleVinDataEditor" %>

    <%--  My Drop Down Menus  --%>

    <asp:DropDownList ID="EnginesDDL"  runat="server" DataTextField="Description" 
    DataValueField="CategoryID" onchange="_gaq.push(['_trackEvent', 'Equipment Page', 'Change', 'Engine']);">
    </asp:DropDownList>
               
    <asp:DropDownList ID="TransmissionsDDL"  runat="server" DataTextField="Description" 
    DataValueField="CategoryID" onchange="_gaq.push(['_trackEvent', 'Equipment Page', 'Change', 'Transmission']);">
    </asp:DropDownList>
               
    <asp:DropDownList ID="DrivetrainsDDL"  runat="server" DataTextField="Description" 
    DataValueField="CategoryID" onchange="_gaq.push(['_trackEvent', 'Equipment Page', 'Change', 'Drivetrain']);">
    </asp:DropDownList>
                
    <asp:DropDownList ID="FuelSystemsDDL"  runat="server" DataTextField="Description" 
    DataValueField="CategoryID" onchange="_gaq.push(['_trackEvent', 'Equipment Page', 'Change', 'Fuel System']);">
    </asp:DropDownList>