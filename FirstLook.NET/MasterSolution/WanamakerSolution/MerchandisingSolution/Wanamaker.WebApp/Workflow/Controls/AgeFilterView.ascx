﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AgeFilterView.ascx.cs" Inherits="Wanamaker.WebApp.Workflow.Controls.AgeFilterView" %>

<asp:DropDownList ID="AgeSelector" runat="server" DataValueField="Id" DataTextField="Description" 
    OnSelectedIndexChanged="OnAgeSelectorChanged" AutoPostBack="True"
    onchange='<%# GoogleAnalytics(AgeSelector.ClientID) %>' >
</asp:DropDownList>