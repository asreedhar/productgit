﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Command;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Audit;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.WindowSticker;
using VehicleTemplatingWebServiceClient.VehicleTemplatingService;
using VehicleData = VehicleTemplatingDomainModel.VehicleData;
using System.Collections;
using BulkWindowSticker;


namespace Wanamaker.WebApp.Workflow.Controls
{
    public partial class PrintWindowSticker : WorkflowBaseUserControl
    {

        public FirstLook.Common.Core.ILogger ExceptionLogger{get; set;}

        internal const string PrintPdfByteArrayCollection = "PRINT_PDF_BYTE_ARRAY_COLLECTION";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;

            try
            {
                PrintDiv.Attributes.Remove("disabled");

                // get the owner
                var owner = Owner.GetOwner(BusinessUnitId);

                try
                {
                    TemplateInfoSorter sorter = new TemplateInfoSorter();
                    // get basic info about the dealer's templates to bind the template dropdownlist
                    var windowStickers = TemplatingService.GetWindowStickerTemplateInfoForOwner(owner.Handle);
                    Array.Sort(windowStickers, sorter);

                    WindowStickerTemplateList.DataSource = windowStickers;
                    WindowStickerTemplateList.DataBind();

                    // get basic info about the dealer's templates to bind the template dropdownlist
                    var buyersGuides = TemplatingService.GetBuyersGuideTemplateInfoForOwner(owner.Handle);
                    Array.Sort(buyersGuides, sorter);

                    BuyersGuideTemplateList.DataSource = buyersGuides;
                    BuyersGuideTemplateList.DataBind();

                    if (WindowStickerTemplateList.SelectedItem != null)
                    {
                        // hide or show controls that depend on prompts.
                        TemplateTO wsTemplate = GetSelectedTemplate(WindowStickerTemplateList);
                        SetPromptControlVisibility(wsTemplate, PromptControls);
                    }

                    if (BuyersGuideTemplateList.SelectedItem != null)
                    {
                        // hide or show controls that depend on prompts.
                        TemplateTO bgTemplate = GetSelectedTemplate(BuyersGuideTemplateList);
                        SetPromptControlVisibility(bgTemplate, PromptControls);
                    }

                    SetupControlsBasedOnTemplateLists();

                    SetLastPrintDateMessage();
                }
                catch (WebException ex)
                {
                    ExceptionLogger.Log(ex);
                    ShowServiceError();
                }

            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                Visible = false;
            }
        }

        private void ShowServiceError()
        {
            ServiceError.Visible = true;
            NoPrint.Visible = false;
            PrintDiv.Attributes.Add("disabled", "disabled");
        }


        private TemplateTO GetSelectedTemplate(ListControl templateList)
        {
            var templateId = int.Parse(templateList.SelectedValue);

            TemplateTO template = null;
            try
            {
                template = TemplatingService.GetTemplate(templateId);
            }
            catch (Exception ex)
            {
                
                ExceptionLogger.Log(ex);
            }

            return template;
        }

        private void SetLastPrintDateMessage()
        {
            if (InventoryId == 0)
            {
                LastPrintedMessage.Text = string.Empty;
                return;
            }

            var getLastPrintDateCommand = new GetLastWindowStickerPrintDate(InventoryId);
            AbstractCommand.DoRun(getLastPrintDateCommand);

            LastPrintedMessage.Text = getLastPrintDateCommand.LastPrintDate == default(DateTime)
                                    ? "A Window Sticker has not yet been printed."
                                    : "Window Sticker Last printed: " + getLastPrintDateCommand.LastPrintDate;
        }

        private void SetupControlsBasedOnTemplateLists()
        {
            // Show the print div if we have at least one template of any type.
            PrintDiv.Visible = (WindowStickerTemplateList.Items.Count > 0) ||
                               (BuyersGuideTemplateList.Items.Count > 0);
            NoPrint.Visible = !PrintDiv.Visible;

            // Set up the other controls based on the visibility of the print div
            if (PrintDiv.Visible)
            {
                EnableOrDisableTemplateControls(WindowStickerTemplateList, PrintWindowStickerCheckbox, "Window Sticker");
                EnableOrDisableTemplateControls(BuyersGuideTemplateList, PrintBuyersGuideCheckbox, "Buyer's Guide");

                // this is clumbsy, but it works.
                WarrantyNo.Enabled = BuyersGuideTemplateList.Enabled;
                WarrantyLimited.Enabled = BuyersGuideTemplateList.Enabled;
                WarrantyFull.Enabled = BuyersGuideTemplateList.Enabled;
                WarrantyNo.ToolTip = BuyersGuideTemplateList.ToolTip;
                WarrantyLimited.ToolTip = BuyersGuideTemplateList.ToolTip;
                WarrantyFull.ToolTip = BuyersGuideTemplateList.ToolTip;
                ServiceContract.Visible = BuyersGuideTemplateList.Enabled;
                ServiceContract.Enabled = BuyersGuideTemplateList.Enabled;
                ServiceContract.ToolTip = BuyersGuideTemplateList.ToolTip;
            }
        }

        /// <summary>
        /// Set up a set of controls based on the number of items in a List Control that contains templates.
        /// </summary>
        /// <param name="templateList"></param>
        /// <param name="printCheckbox"></param>
        /// <param name="listDescription"></param>
        private static void EnableOrDisableTemplateControls(ListControl templateList, CheckBox printCheckbox, string listDescription)
        {
            if (templateList.Items.Count == 0)
            {
                templateList.Enabled = false;
                templateList.ToolTip = "No " + listDescription + " Templates have been uploaded.";
                printCheckbox.Checked = false;
                printCheckbox.Enabled = false;
            }
            else
            {
                templateList.Enabled = true;
                templateList.ToolTip = string.Empty;
                printCheckbox.Enabled = true;
            }

            printCheckbox.ToolTip = templateList.ToolTip;
        }

        /// <summary>
        /// Print the Window Sticker and/or Buyer's Guide
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PrintButton_Click(object sender, EventArgs e)
        {
            var pdfs = new Collection<byte[]>();

            if (PrintWindowStickerCheckbox.Checked)
            {
                var windowStickerPdf = GetPdfDocument(WindowStickerTemplateList);
                pdfs.Add(windowStickerPdf);
                TrackUsage(BusinessUnitEventType.Window_Sticker_Printed);
            }

            if (PrintBuyersGuideCheckbox.Checked)
            {
                var buyersGuidePdf = GetPdfDocument(BuyersGuideTemplateList);
                pdfs.Add(buyersGuidePdf);
                TrackUsage(BusinessUnitEventType.Buyers_Guide_Printed);
            }

            if (pdfs.Count > 0)
            {
                // store the pdf for the print page.
                Session.Add(PrintPdfByteArrayCollection, pdfs);
                RegisterPrintScript();
                LogPrintTime();
            }
        }

        private void LogPrintTime()
        {
            // old login code
            //var logPrintDateCommand = new SetLastWindowStickerPrintDate(InventoryId);
            //AbstractCommand.DoRun(logPrintDateCommand);

            WindowStickerRepository windowStickerRepository = new WindowStickerRepository();

            // save window sticker history
            if (PrintWindowStickerCheckbox.Checked)
            {
                bool isAutoTemplateId = false;
                var upgradeSettings = GetUpgradeSettings.GetSettings(BusinessUnitId);

                int? autoTemplateId = upgradeSettings.AutoWindowStickerTemplateId;
                int windowStickerTemplateId;
                if (Int32.TryParse(WindowStickerTemplateList.SelectedValue, out windowStickerTemplateId))
                {
                    // check for auto template so it does not print again in batch
                    if (autoTemplateId != null && windowStickerTemplateId == autoTemplateId)
                        isAutoTemplateId = true;

                    windowStickerRepository.SavePrintLog(InventoryId, 1, windowStickerTemplateId, isAutoTemplateId, null);
                }

            }

            // save buyers guide history
            if (PrintBuyersGuideCheckbox.Checked)
            {
                int buyersGuideTemplateId;
                if (Int32.TryParse(BuyersGuideTemplateList.SelectedValue, out buyersGuideTemplateId))
                {
                    windowStickerRepository.SavePrintLog(InventoryId, 2, buyersGuideTemplateId, false, null);
                }
            }



        }

        private void TrackUsage(BusinessUnitEventType eventType)
        {
            var Username = "unknown";
            if( HttpContext.Current != null )
                Username = HttpContext.Current.User.Identity.Name;

            var businessUnitEvent = new BusinessUnitEventLogEntry {
                BusinessUnitID = BusinessUnitId,
                EventType = (int) eventType,
                CreatedBy = Username
            };
            businessUnitEvent.Save();
        }

        private VehicleTemplatingService _templatingService;
        private VehicleTemplatingService TemplatingService
        {
            get 
            { 
                if (_templatingService == null)
                {
                    _templatingService = new VehicleTemplatingService();
                }
                return _templatingService;
            }
        }

        /// <summary>
        /// Get the pdf using the template and vehicle data, add the pdf to a cache for the print page.
        /// </summary>
        private byte[] GetPdfDocument(ListControl templateList)
        {

            // get the selected template
            TemplateTO template = GetSelectedTemplate(templateList);
            ApplyJustInTimeTransformations(BusinessUnitId, template);

            SelectContentAreasBasedOnUserInput(template, PromptControls);

            // get an owner handle
            var owner = Owner.GetOwner(BusinessUnitId);
            if (owner == null)
                throw new ApplicationException("No owner was found for the businessUnitId " + BusinessUnitId);

            // put together an inventory Id...this could be moved elsewhere
            var vehicleHandle = "1" + InventoryId;

            // get the vehicle data
            var legacyVehicleAdapter = new LegacyVehicleAdapter(Context.User.Identity.Name);
            VehicleData vtdmVehicleData = legacyVehicleAdapter.Adapt(owner.Handle, vehicleHandle);

            // convert it to a type tye service can use
            var wsVehicleData = GetWebServiceVehicleData(vtdmVehicleData);

            // get the pdf
            byte[] pdf = TemplatingService.GeneratePdf(wsVehicleData, template, owner.Handle);

            return pdf;
        }

        private void ApplyJustInTimeTransformations(int BusinessUnitId, TemplateTO template)
        {
            // if business unit does not have access to QR Codes app feature, remove them from the model before sending to pdf service
            var upgradeSettings = GetUpgradeSettings.GetSettings(BusinessUnitId, false);
            if( !upgradeSettings.MAXForSmartphone )
            {
                // business unit does not have "maxForSmartphone" permission; remove all QR codes
                if( template.ContentAreas != null && template.ContentAreas.Length > 0 )
                {
                    var ContentAreas_new = new List<ContentArea>( template.ContentAreas.Length );
                    for( var i = 0; i < template.ContentAreas.Length; ++i )
                    {
                        if( template.ContentAreas[i].DataPoint.Key != DynamicDataPointKeys.QRCodeURL )
                        {
                            // re-add all content areas that are not QR codes to a new collection
                            ContentAreas_new.Add( template.ContentAreas[i] );
                        }
                    }
                    template.ContentAreas = ContentAreas_new.ToArray();
                }
            }
        }


        #region REFACTOR
        
        private void HidePromptControls()
        {
            foreach (var control in PromptControls.Controls)
            {
                if ((control is WebControl) && (control is ICheckBoxControl))
                    ((WebControl) control).Visible = false;
            }            
        }

        private static void SetPromptControlVisibility(TemplateTO template, Control checkControlContainer)
        {

            foreach (var contentArea in template.ContentAreas)
            {
                DataPoint dp = contentArea.DataPoint;

                if (dp != null && !string.IsNullOrEmpty(dp.Prompt))
                {
                    ICheckBoxControl checkControl = GetPromptCheckControl(dp, checkControlContainer);

                    ((Control) checkControl).Visible = checkControl != null;
                }
            }
        }

        private static void SelectContentAreasBasedOnUserInput(TemplateTO template, Control checkControlContainer)
        {
            foreach (var contentArea in template.ContentAreas)
            {
                DataPoint dp = contentArea.DataPoint;

                if (dp.Prompt == null)
                {
                    continue;
                }

                ICheckBoxControl checkControl = GetPromptCheckControl(dp, checkControlContainer);
                
                // We will default to show/select a datapoint.  So, either we didn't have a checkbox 
                // to turn it off, or we had one that was turned on (checked)
                dp.Selected = (checkControl == null) || (checkControl.Checked);

            }
        }

        private static ICheckBoxControl GetPromptCheckControl(DataPoint dp, Control checkControlContainer)
        {
            if (dp.Prompt.Equals("No Warranty", StringComparison.CurrentCultureIgnoreCase))
            {
                return (ICheckBoxControl)checkControlContainer.FindControl("WarrantyNo");
            }
            if (dp.Prompt.Equals("Limited Warranty", StringComparison.CurrentCultureIgnoreCase))
            {
                return (ICheckBoxControl)checkControlContainer.FindControl("WarrantyLimited");
            }
            if (dp.Prompt.Equals("Full Warranty", StringComparison.CurrentCultureIgnoreCase))
            {
                return (ICheckBoxControl)checkControlContainer.FindControl("WarrantyFull");
            }
            if (dp.Prompt.Equals("Service Contract", StringComparison.CurrentCultureIgnoreCase))
            {
                return (ICheckBoxControl)checkControlContainer.FindControl("ServiceContract");
            }
            if (dp.Prompt.Equals("KBB Book Value", StringComparison.CurrentCultureIgnoreCase))
            {
                return (ICheckBoxControl)checkControlContainer.FindControl("KBBBookValue");
            }
            
            return null;
        }

        #endregion


        /// <summary>
        /// Register script to open the print page
        /// </summary>
        private void RegisterPrintScript()
        {
            // register a script to open the print page
            var scriptType = GetType();
            const string SCRIPT_NAME = "printPdf";
            if (!Page.ClientScript.IsStartupScriptRegistered(scriptType, SCRIPT_NAME))
            {
                var printPage = Request.ApplicationPath + "/Print/PrintPdf.aspx";
                var script = String.Format("<script type='text/javascript'>detailedresults=window.open('{0}');</script>", printPage);
                Page.ClientScript.RegisterStartupScript(scriptType, SCRIPT_NAME, script);
            }
        }
        
        /// <summary>
        /// Map the domain VehicleData type to the proxy VehicleData type
        /// </summary>
        /// <param name="vehicleDataIn"></param>
        /// <returns></returns>
        private static VehicleTemplatingWebServiceClient.VehicleTemplatingService.VehicleData GetWebServiceVehicleData(VehicleTemplatingDomainModel.VehicleData vehicleDataIn)
        {
            var vehicleDataOut = new VehicleTemplatingWebServiceClient.VehicleTemplatingService.VehicleData();

            vehicleDataOut.Model = vehicleDataIn.Model;
            vehicleDataOut.ModelYear = vehicleDataIn.ModelYear;
            vehicleDataOut.Make = vehicleDataIn.Make;
            vehicleDataOut.AgeInDays = vehicleDataIn.AgeInDays;
            vehicleDataOut.BodyStyle = vehicleDataIn.BodyStyle;
            vehicleDataOut.Certified = vehicleDataIn.Certified;
            vehicleDataOut.CertifiedID = vehicleDataIn.CertifiedID;
            vehicleDataOut.Class = vehicleDataIn.Class;
            vehicleDataOut.Description = vehicleDataIn.Description;
            vehicleDataOut.Drivetrain = vehicleDataIn.Drivetrain;
            vehicleDataOut.Engine = vehicleDataIn.Engine;
            vehicleDataOut.Equipment = vehicleDataIn.Equipment.ToArray();
            vehicleDataOut.Packages = vehicleDataIn.Packages.ToArray();
            vehicleDataOut.ExteriorColor = vehicleDataIn.ExteriorColor;
            vehicleDataOut.FuelType = vehicleDataIn.FuelType;
            vehicleDataOut.InteriorColor = vehicleDataIn.InteriorColor;
            vehicleDataOut.Mileage = vehicleDataIn.Mileage;
            vehicleDataOut.Price = vehicleDataIn.Price;
            vehicleDataOut.Series = vehicleDataIn.Series;
            vehicleDataOut.StockNumber = vehicleDataIn.StockNumber;
            vehicleDataOut.Transmission = vehicleDataIn.Transmission;
            vehicleDataOut.Trim = vehicleDataIn.Trim;
            vehicleDataOut.UnitCost = vehicleDataIn.UnitCost;
            vehicleDataOut.Vin = vehicleDataIn.Vin;
            vehicleDataOut.MpgCity = vehicleDataIn.MpgCity;
            vehicleDataOut.MpgHwy = vehicleDataIn.MpgHwy;
            vehicleDataOut.KBBBookValue = vehicleDataIn.KBBBookValue;
            vehicleDataOut.NADABookValue = vehicleDataIn.NADABookValue;
            vehicleDataOut.MSRP = vehicleDataIn.MSRP;

            return vehicleDataOut;
        }

        protected void TemplateList_SelectedIndexChanged(object sender, EventArgs e)
        {
            HidePromptControls();

            if (WindowStickerTemplateList.SelectedItem != null)
            {
                TemplateTO wsTemplate = GetSelectedTemplate(WindowStickerTemplateList);
                SetPromptControlVisibility(wsTemplate, PromptControls);
            }

            if (BuyersGuideTemplateList.SelectedItem != null)
            {
                TemplateTO bgTemplate = GetSelectedTemplate(BuyersGuideTemplateList);
                SetPromptControlVisibility(bgTemplate, PromptControls);
            }

        }

        private class TemplateInfoSorter : IComparer<TemplateInfo>
        {
            public int Compare(TemplateInfo x, TemplateInfo y)
            {
                return x.Name.CompareTo(y.Name);
            }
        }
    }
}