using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Runtime.ConstrainedExecution;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.WebPages;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Logging;
using FirstLook.LotIntegration.DomainModel.Tasks;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.DataAccess;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types.TransferObjects;
using FirstLook.Merchandising.DomainModel.Marketing.Commands;
using FirstLook.Merchandising.DomainModel.Pricing;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using FirstLook.Merchandising.DomainModel.Workflow;
using MAX.Entities;
using Merchandising.Messages;
using Merchandising.Messages.AutoApprove;
using System.Linq;

namespace Wanamaker.WebApp.Workflow.Controls
{
    public partial class InventoryDataView : WorkflowBaseUserControl
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<InventoryDataView>();

        #region Injected dependencies

        public IAdMessageSender MessageSender { get; set; }
        public IQueueFactory QueueFactory { get; set; }
        public IInventorySearch InventorySearch { get; set; }
        public IDiscountPricingCampaignManager PricingCampaignManager { get; set; }

        #endregion

		// If you change the Text values of these items, be sure to change the same values in the control (ascx file).
	    protected readonly string NotCertifiedText = "Not Certified";
		protected readonly string CertifiedText = "Certified";

        public bool AllowTrimSelection { get; set; }
        public bool DisablePrintWinowSticker { get; set; }
        public bool ShowLastAdLink { get; set; }
        public bool ShowCertified { get; set; }

	    public string Color 
        {
            get
            {
                return ColorPair.GetItemDescription(VehicleConfiguration.ExteriorColor1, VehicleConfiguration.ExteriorColor2, string.Empty);
            }
        }

        private InventoryData _inventoryInfo;
        private VehicleConfiguration _configuration;
        private VehicleAttributes _vehicleAttributes;
        

        public void getDiffCategories(object sender, EventArgs e)
        {
            discriminators.Visible = true;
        }

        public void closeDiscriminators(object sender, EventArgs e)
        {
            discriminators.Visible = false;
        }

        public int SelectedChromeStyle
        {
            get
            {
                if (string.IsNullOrEmpty(TrimStyleDropDown.SelectedValue))
                {
                    return -1;
                }

                return int.Parse(TrimStyleDropDown.SelectedValue);
            }
        }

        public string Trim
        {
            get
            {
                object o = ViewState["trim"];
                return ((o == null) ? String.Empty : (string)o);
            }
            set { ViewState["trim"] = value; }
        }

        public string Username
        {
            get { return Context.User.Identity.Name; }
        }

        public bool ChromeValueSet
        {
            get { return VehicleConfiguration.ChromeStyleID > 0; }
        }

        public VehicleAttributes VehicleAttributes
        {
            get
            {
                if (_vehicleAttributes == null)
                {
                    var styleId = VehicleConfiguration.ChromeStyleID;
                    _vehicleAttributes = VehicleAttributes.Fetch(styleId, InventoryInfo.VIN);
                }

                return _vehicleAttributes;
            }
            set { _vehicleAttributes = value; }
        }

        public VehicleConfiguration VehicleConfiguration
        {
            get
            {
                if (_configuration == null)
                {
                    _configuration = VehicleConfiguration.FetchOrCreateDetailed(BusinessUnitId, InventoryId, Username);
                }
                return _configuration;
            }
            set { _configuration = value; }
        }

        #region Trim Drop Down Code

        protected void StyleDDL_DataBound(object sender, EventArgs e)
        {
     
            switch (TrimStyleDropDown.Items.Count)
            {
                case 1:
                    VehicleConfiguration.UpdateStyleId(BusinessUnitId, Int32.Parse(TrimStyleDropDown.SelectedValue));
                    break;
                case 0:
                    break;
            }

            if (!ChromeValueSet)
            {
                TrimStyleDropDown.Items.Insert(0, new ListItem("Select trim/style...", ""));
            }
        
            string currId = VehicleConfiguration.ChromeStyleID.ToString();
            if (TrimStyleDropDown.Items.FindByValue(currId) != null)
            {
                TrimStyleDropDown.SelectedValue = currId;
            }

            foreach ( ListItem item in TrimStyleDropDown.Items )
            {
                if ( item.Text.Contains( "Select trim" ) ) continue;
                item.Attributes[ "title" ] = item.Text;
            }

            ReplaceTrimDisplayValueWithCustomTrim();

        }

        private void ReplaceTrimDisplayValueWithCustomTrim ()
        {
            var currId = VehicleConfiguration.ChromeStyleID.ToString();
            var selectedTrimItem = TrimStyleDropDown.Items.FindByValue(currId);
            if ( selectedTrimItem == null || string.IsNullOrEmpty(VehicleConfiguration.AfterMarketTrim)) return;
            selectedTrimItem.Text = AfterMarketTrim;
        }

        private void SetupTrimSelect()
        {
            // Make sure trim help does not load on subsequent postbacks
            discriminators.Visible = false;

            foreach ( ListItem item in TrimStyleDropDown.Items )
            {
                if ( item.Text.Contains( "Select trim" ) ) continue;
                item.Attributes[ "title" ] = item.Text;
            }
        }

        protected void ChromeStyle_Changed(object sender, EventArgs e)
        {
            var styleId = int.Parse(TrimStyleDropDown.SelectedValue);

            if (!ChromeValueSet)
            {
                TrimStyleDropDown.Items.Remove("");
            }

            if (styleId != VehicleConfiguration.ChromeStyleID)
            {

                VehicleConfiguration.SetChromeStyleID(BusinessUnitId, InventoryId, styleId);
                VehicleConfiguration.Save(BusinessUnitId, Username);
                TrimStyleDropDown.DataBind();

                PricingCampaignManager.ApplyDiscount(BusinessUnitId, InventoryId, Username, GetType().FullName, MessageSender);
            }

            InventoryData.Refresh(BusinessUnitId, InventoryInfo.InventoryID);
            InventoryInfo = null;

            // If we applied a discount campaign to the car, we may have also reset the price.
            NewCarListPrice.Text = string.Format("{0:n0}", InventoryInfo.ListPrice);
            LastListPriceHidden.Value = string.Format("{0:0}", InventoryInfo.ListPrice);

            RaiseBubbleEvent(this, new TrimChangedEventArgs(styleId));
        }


        #endregion

        // TrimDS
        protected void StylePieces_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception != null && e.Exception is VehicleCatalogException)
            {
                Response.Redirect("~/Workflow/Inventory.aspx?errorMsg=" + e.Exception.Message);
            }
        }

        // TrimDS
        protected void StylePieces_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["Vin"] = InventoryInfo.VIN;
        }

        public InventoryData InventoryInfo
        {
            get
            {
                if (_inventoryInfo == null)
                {
                    _inventoryInfo = InventoryId != 0 ? InventoryData.Fetch(BusinessUnitId, InventoryId) : new InventoryData();
                }
                return _inventoryInfo;
            }
            set { _inventoryInfo = value; }
        }

        public Control PrintWindowSticker 
        {
            get
            {
                return PrintWindowStickerControl;
            }
        }

        public decimal BookoutValue
        {
            get
            {
                object o = ViewState["bkVal"];
                return (o == null) ? 0.0M : (decimal)o;
            }
            set { ViewState["bkVal"] = value; }
        }

        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetNewVehicleData(InventoryInfo);
                SetupPricing();
                SetupCertification();

                discriminator.Vin = InventoryInfo.VIN;

                makeHidden.Value = InventoryInfo.Make;

                TrimStyleDropDown.DataBind();

            }
            SetupRepriceWarning();
            SetupTrimSelect();
            CustomTrimTextbox.Value = VehicleConfiguration.AfterMarketTrim;
            MessageSender.SendMessageAsync = CheckApprovalPage();
        }
        
	    protected void Page_PreRender(object sender, EventArgs e)
	    {
	    }

        // Check whether the current page is ApprovalSummary page
        private bool CheckApprovalPage()
        {
            try
            {
                return System.IO.Path.GetFileName(Page.Request.Path).Contains("ApprovalSummary");
            }
            catch (Exception ex)
            {
                Log.Error("Error getting page name from CheckApprovalPage", ex);
            }
            return false;
        }

        private void SetupRepriceWarning()
        {
            DoRepriceWarn.Text = "false";
            if (InventoryInfo.InventoryType == VehicleType.New)
            {
                var campaign = PricingCampaignManager.FetchCampaignForInventory(InventoryInfo.BusinessUnitID, InventoryInfo.InventoryID);
                
                if (campaign != null)
                {
                    NewCarListPrice.Attributes.Add("onkeydown", "if (event.keyCode==13) { $.remodal.lookup[$('#formModal').data('remodal')].open(); return false; };"); // disable enter doing autopostback on enter
                    DoRepriceWarn.Text = "true";
                    string campaignFilter = campaign.CampaignFilter.ToString();
                    string modalText =
                        "This vehicle is part of an existing discount. Manually setting a new price will remove it from ";
                    if (string.IsNullOrEmpty(campaignFilter))
                    {
                        modalText += "any existing discount it is in.";
                    }
                    else
                    {
                        modalText += "the following discount: \"" + campaignFilter + "\"";
                    }
                    ModalRepriceWarnText.InnerText = modalText;
                }
            }
        }

	    private int GetProgramCount()
	    {
		    var programCount = 0;

		    foreach (ListItem item in ddlCertifiedPrograms.Items)
		    {
			    var tokens = item.Value.Split('|');
			    if (tokens.Length > 0)
			    {
				    var programId = 0;
				    if (Int32.TryParse(tokens[0], out programId) && programId > 0) programCount++;
			    }
		    }
		    
			return programCount;
	    }

	    private void SetNewVehicleData(InventoryData data)
        {
            //is this needed?
            _configuration = VehicleConfiguration.FetchOrCreateDetailed(BusinessUnitId, InventoryId, Username);

            Trim = string.Empty;
        }

        public void SetNewVehicle(int inventoryId)
        {
            try
            {
                _inventoryInfo = InventoryData.Fetch( BusinessUnitId, inventoryId );
            }
            catch(Exception ex)
            {
                Response.Redirect("~/Workflow/Inventory.aspx?errorMsg=" + ex.Message);
            }
            SetNewVehicleData(_inventoryInfo);
        }

        public void SetNewVehicle(InventoryData data)
        {
            InventoryInfo = data;
            SetNewVehicleData(data);
        }

        protected string GetEStockLink()
        {
            return HttpUtility.HtmlEncode(InventoryInfo.GetEStockLink());
        }

        protected string GetGoogleLink()
        {
            return "javascript:window.open('" + InventoryInfo.GetGoogleLink() + "', 'vehicle_presence');return false;";
        }

        protected object GetPublishedDate(DateTime? date)
        {
            if (date.HasValue)
            {
                return string.Format("{0:d}", date);
            }

            return "-";
        }

        #region Vehicle Certification



	    private IEnumerable<CertifiedProgramInfoTO> _cpoPrograms;
	    private IEnumerable<CertifiedProgramInfoTO> CpoPrograms
	    {
		    get
		    {
			    if (_cpoPrograms == null)
			    {
					var certifiedProgamCommand = new CertifiedProgamsForDealerMakeCommand(BusinessUnitId, InventoryInfo.Make);
					AbstractCommand.DoRun(certifiedProgamCommand);
					_cpoPrograms = certifiedProgamCommand.Programs;
				}

			    return _cpoPrograms;

		    }
	    }


	    private bool ManufacturerReportedProgramSelected()
	    {

		    var selectedProgramId = ddlCertifiedPrograms.SelectedValue.Split('|')[0];

		    return InventoryInfo.CertifiedProgramId.HasValue &&
				  selectedProgramId == InventoryInfo.CertifiedProgramId.Value.ToString(CultureInfo.InvariantCulture) && 
				  CpoPrograms.Any(x=>x.ManufacturerReportsCertifications && x.Id.ToString(CultureInfo.InvariantCulture) == selectedProgramId);
	    }

	    private void SetupCertification()
        {
			ddlCertifiedPrograms.DataSource = CpoPrograms;
			ddlCertifiedPrograms.DataBind();
		    CertifiedIdLabel.Text = string.Format("{0} Certified", InventoryInfo.Make);
	        CertifiedControlsItem.Visible = !InventoryInfo.IsNew() && ShowCertified;
	        spanDisplayCertifiedID.Visible = true;
        }

        private VehicleCertification.Manufacturer _manufacturer;
        private VehicleCertification.Manufacturer Manufacturer
        {
            get
            {
                var o = ViewState["ManufacturerID"];
                if (o == null)
                {
                    if (_manufacturer.Id == 0)
                    {
                        try
                        {
							_manufacturer = VehicleCertification.GetManufacturer(InventoryId);
                        }
                        catch (VehicleCertification.FailedToGetManufacturer) { }
                    }
                    ViewState["ManufacturerID"] = _manufacturer;
                    return _manufacturer;
                }
                return (VehicleCertification.Manufacturer)o;
            }
        }

        #endregion


        #region Pricing

        protected void OnReprice(object sender, EventArgs e)
        {
            int newPrice;
            if (int.TryParse(NewCarListPrice.Text, NumberStyles.Number, CultureInfo.CurrentCulture, out newPrice))
            {
                NewCarPriceParseError.Visible = false;

                LastListPriceHidden.Value = string.Format("{0:0}", newPrice);

                PricingCampaignManager.ExpireAllCampaignsForInventory(InventoryId, HttpContext.Current.User.Identity.Name, false, InventoryExclusionType.ManualReprice);

                var priceCommand = new SetSpecialPrice(InventoryId, newPrice, HttpContext.Current.User.Identity.Name);
                AbstractCommand.DoRun(priceCommand);


                RaiseBubbleEvent(this, new PriceChangedEventArgs(newPrice));


                MessageSender.SendAutoApproval(new AutoApproveMessage(BusinessUnitId, InventoryId, Username), InventoryInfo.IsNew(), GetType().FullName);
            }
            else
            {
                NewCarPriceParseError.Visible = true;
            }
        }

        public void SetPricingLink(LocateVehicleResults result)
        {
            priceLnk.NavigateUrl = DealerPingAccess.CheckDealerPingAccess(BusinessUnitId) ?
                                WorkflowState.CreatePricingPingUrl("~/PricingAnalysis/pingone", InventoryId, BusinessUnitId, result.ActiveListType, result.UsedOrNewFilter, result.AgeBucketId)
                                : InventoryData.GetPricingLink(BusinessUnitId, InventoryId,
                                                                        ConfigurationManager.AppSettings[
                                                                            "pricing_page_from_host"]);
        }
        private void SetupPricing()
        {

            //pricing stuff
            try
            {
                var optionsCommand = GetMiscSettings.GetSettings(BusinessUnitId);

                var priceNewCars = optionsCommand.HasSettings && optionsCommand.PriceNewCars;

                if (!InventoryInfo.IsNew() || !priceNewCars) //if used vehicle always do OR do this if they don't want to price new cars
                {
                    UsedCarPricingPanel.Visible = true;

                    if (string.IsNullOrEmpty(priceLnk.NavigateUrl))
                    {
                        priceLnk.NavigateUrl = InventoryData.GetPricingLink(BusinessUnitId, InventoryId,
                            ConfigurationManager.AppSettings[
                                "pricing_page_from_host"]);
                    }
                    priceLnk.Visible = !string.IsNullOrEmpty(priceLnk.NavigateUrl) && !InventoryInfo.IsNew() && !DisableReprice;

                    headerListPrice.Text = string.Format("{0:c0}", InventoryInfo.ListPrice);

                    PricingData data = PricingData.GetMarketPricing(BusinessUnitId, InventoryId);

                    PricingData.PricingRisk pr = data.GetPricingRisk();

                    headerListPrice.CssClass = SetCssClass(headerListPrice.CssClass, "priceAlert",
                                                           pr == PricingData.PricingRisk.OverPriced ||
                                                           pr == PricingData.PricingRisk.Underpriced);

                }
                else
                {
                    NewCarPricingInfoPanel.Visible = true;
                    msrpPrice.Text = string.Format("{0:c0}", InventoryInfo.MSRP);
                    NewCarListPrice.Text = string.Format("{0:n0}", InventoryInfo.ListPrice);
                    LastListPriceHidden.Value = string.Format("{0:0}", InventoryInfo.ListPrice);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Failed to setup reprice link.", ex);
                priceLnk.Visible = false;
            }
        }

        public bool DisableReprice { get; set; }

        public string ListPriceText
        {
            get { return headerListPrice.Text; }
        }

        protected string AfterMarketTrim
        {
            get
            {
                var vc = VehicleConfiguration.FetchOrCreateDetailed(BusinessUnitId, InventoryId, Username);
                return vc.AfterMarketTrim;
            }
          
        }

        private static string SetCssClass(string cssClasses, string cssClass, bool include)
        {
            var newClasses = cssClasses
                .Split(' ')
                .Where(c => !string.IsNullOrWhiteSpace(c))
                .Where(c => !StringComparer.InvariantCulture.Equals(c, cssClass));

            if (include)
                newClasses = newClasses.Concat(new[] { cssClass });

            return string.Join(" ", newClasses);
        }

        public void MakeListPriceRed()
        {
            headerListPrice.CssClass += " red";
        }

        public void ClearListPriceColor()
        {
            headerListPrice.CssClass = headerListPrice.CssClass.Replace(" red", "");
        }

        #endregion

        [WebMethod]
        protected void UpdateCustomTrim(int businessUnitId, int inventoryId, string userName, string customTrim)
        {
            var configuration = VehicleConfiguration.FetchOrCreateDetailed(businessUnitId, inventoryId, userName);
            configuration.AfterMarketTrim = customTrim ?? string.Empty;
            configuration.Save(BusinessUnitId, Username);
            MessageSender.SendStyleSet(businessUnitId, inventoryId, Username, GetType().FullName);
        }

	    private void RemoveDropDownListItemByText(DropDownList list, string itemText)
	    {
		    var index = list.Items.IndexOf(list.Items.FindByText(itemText));
			if (index >= 0) list.Items.RemoveAt(index);
	    }

		private void SelectDropDownListItemByText(DropDownList list, string itemText)
		{
			foreach (ListItem i in list.Items)
			{
				i.Selected = i.Text == itemText;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="list">List to search for programId</param>
		/// <param name="programId">ProgramId to find</param>
		/// <returns>A value indicating whether or not the programId was found in the itemsList and selected.</returns>
		private bool SelectDropDownListItemByProgramId(DropDownList list, int programId)
		{
			/* 
			 * selectedValue = "[programId]|[requiresCertifiedId]"
			 * example: "95|True"
			 * split on a pipe and take the first value as the programId
			 */
	
			bool found = false;
			foreach (ListItem i in list.Items)
			{
				i.Selected = i.Value.Split('|')[0].Equals(programId.ToString(CultureInfo.InvariantCulture));
				if (i.Selected && ! found)
				{
					found = true;
				}
			}
			return found;
		}


	    protected void ddlCertifiedPrograms_DataBound(object sender, EventArgs e)
	    {
		    var list = (DropDownList) sender;
		    var selected = false;
			ProgramIsPersisted.Value = InventoryInfo.CertifiedProgramIsPersisted.ToString();			

			// We should have a program to select - either one persisted or one auto selected
			if (InventoryInfo != null && InventoryInfo.CertifiedProgramId.HasValue)
		    {
				var selectedIndexBefore = list.SelectedIndex;
				selected = SelectDropDownListItemByProgramId(list, InventoryInfo.CertifiedProgramId.Value);

				var selectionChanged = list.SelectedIndex != selectedIndexBefore;
				if (selectionChanged && ! InventoryInfo.CertifiedProgramIsPersisted)
					RaiseBubbleEvent(this, new CertificationAutoSelected());
		    }

		    if (!selected)
		    {
			    // We didn't select a program, so we need to select the best item.
			    SelectDropDownListItemByText(list, InventoryInfo.Certified ? CertifiedText : NotCertifiedText);
		    }
	    }

		

	    protected void ddlCertifiedPrograms_SelectedIndexChanged(object sender, EventArgs e)
	    {
		    UpdateCertifiedStatus();
	    }

		protected void txtCertifiedID_TextChanged(object sender, EventArgs e)
		{
			UpdateCertifiedStatus();
		}

	    public void UpdateCertifiedStatus()
	    {
			/* 
			 * selectedValue = "[programId]|[requiresCertifiedId]"
			 * example: "95|True"
			 * split on a pipe and take the first value as the programId
			 */

		    var certifiedProgramId = Int32.Parse(ddlCertifiedPrograms.SelectedValue.Split('|')[0]);
		    var certifiedId = string.Empty;
		    var isManufacturerProgram = CpoPrograms.Any(x => x.Id == certifiedProgramId && x.ProgramType == "MFR");

		    if (certifiedProgramId > 0)
		    {
			    if (isManufacturerProgram)
			    {
				    if (!VehicleCertification.ValidateCertifiedId(txtCertifiedID.Text, Manufacturer.Name)) return;
			    }

			    certifiedId = isManufacturerProgram ? txtCertifiedID.Text : string.Empty;
		    }

		    var certified = certifiedProgramId == 0 || isManufacturerProgram;
		    
			VehicleCertification.CreateOrUpdate(BusinessUnitId, InventoryId, certified, certifiedProgramId, certifiedId,Username);

			InventoryData.Refresh(BusinessUnitId, InventoryInfo.InventoryID);
			InventoryInfo = null;
			RaiseBubbleEvent(this, new CertificationStateChanged());

		    ProgramIsPersisted.Value = true.ToString();
	    }


    }


	public class CertificationAutoSelected : EventArgs
	{
	}
	public class CertificationStateChanged : EventArgs
	{
	}

    public class TrimChangedEventArgs : EventArgs
    {
        public int StyleId { get; set; }

        public TrimChangedEventArgs(int styleId)
        {
            StyleId = styleId;
        }
    }

    public class PriceChangedEventArgs : EventArgs
    {
        public int NewPrice { get; set; }

        public PriceChangedEventArgs(int newPrice)
        {
            NewPrice = newPrice;
        }
    }
}