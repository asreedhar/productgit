﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Release;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using MAX.Entities;

namespace Wanamaker.WebApp.Workflow
{
    public partial class Link : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string token = Request["link"];
            var entry = RedirectEntry.FromToken(token);

            // Persist BusinessUnitID.
            BusinessUnit bu = BusinessUnitFinder.Instance().Find(entry.BusinessUnitId);
            SetupSoftwareSystemComponentState(bu);

            BusinessUnit currentBusinessUnit = Context.Items["BusinessUnit"] as BusinessUnit;
            if (currentBusinessUnit != null && currentBusinessUnit.Id != bu.Id) //if different business unit, remove session and set new business unit in context
            {
                InventoryDataFilter.ClearListFromSession();
                HttpContext.Current.Items["BusinessUnit"] = bu;
            }

            Response.Redirect(entry.Url);
        }


        private void SetupSoftwareSystemComponentState(BusinessUnit dealer)
        {
            SoftwareSystemComponentState state = LoadState(SoftwareSystemComponentStateFacade.DealerComponentToken);
            state.DealerGroup.SetValue(dealer.DealerGroup());
            state.Dealer.SetValue(dealer);
            state.Member.SetValue(null);
            state.Save();
        }

        private SoftwareSystemComponentState LoadState(string componentToken)
        {
            SoftwareSystemComponentState state = SoftwareSystemComponentStateFacade.FindOrCreate(User.Identity.Name, componentToken);

            if (state == null)
            {
                Member member = MemberFinder.Instance().FindByUserName(User.Identity.Name);

                SoftwareSystemComponent component = SoftwareSystemComponentFinder.Instance().FindByToken(componentToken);

                state = new SoftwareSystemComponentState();
                state.AuthorizedMember.SetValue(member);
                state.SoftwareSystemComponent.SetValue(component);
            }

            return state;
        }
    }
}