﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Wanamaker.WebApp.Workflow
{
    internal class InteriorColorDropdownFilter
    {
        /// <summary>
        /// Given a drop down list of text=color description, value=color code, find the best
        /// match for the supplied code and color.
        /// </summary>
        /// <param name="ddl"></param>
        /// <param name="code"></param>
        /// <param name="color"></param>
        /// <returns>The ListItem that matches code/color. Null if no match found.</returns>
        internal ListItem FindInteriorColor(DropDownList ddl, string code, string color)
        {
            var codeMatch = ddl.Items.Cast<ListItem>().Where(i => i.Value == code).ToList();
            var colorMatch = ddl.Items.Cast<ListItem>()
                                .Where(
                                    i =>
                                    i.Text.Equals(color, StringComparison.InvariantCultureIgnoreCase)
                                 )
                                .ToList();

            var colorSpaceMatch =
                ddl.Items.Cast<ListItem>()
                   .Where(
                       i =>
                       i.Text.Replace(" ", "")
                        .Equals(color.Replace(" ", ""), StringComparison.InvariantCultureIgnoreCase)
                    )
                   .ToList();

            var partialMatch =
                ddl.Items.Cast<ListItem>()
                   .Where(
                       i =>
                       i.Text.ToLower().Contains(color.ToLower()) ||
                       color.ToLower().Contains(i.Text.ToLower())
                    )
                   .ToList();


            // Find the match
            if (codeMatch.Any())
            {
                return codeMatch.First();
            }
            if (colorMatch.Any())
            {
                return colorMatch.First();
            }
            if (colorSpaceMatch.Any())
            {
                // select the shortest one
                return colorSpaceMatch.OrderBy(i => i.Text.Length).First();
            }
            if (partialMatch.Any())
            {
                // select the shortest one
                return partialMatch.OrderBy(i => i.Text.Length).First();
            }
            return null;
        }
        /// <summary>
        /// Given a (possibly null) match, a color, code, and dropdown: select the match, then remove all
        /// duplicate descriptions (for both matches and non-matches) from the list. If the match is null, 
        /// a new entry will be added to the pulldown and it will be selected.
        /// </summary>
        /// <param name="match"></param>
        /// <param name="color"></param>
        /// <param name="code"></param>
        /// <param name="ddl"></param>
        internal void RemoveDuplicateColors(ListItem match, string color, string code, DropDownList ddl)
        {
            List<string> descriptions = new List<string>();
            List<ListItem> itemsToRemove = new List<ListItem>();

            // If we couldn't find a match, use the values provided.
            bool newEntry = false;
            if (match == null)
            {
                match = new ListItem(color, code);
                newEntry = true;
            }

            // First, remove any items that have the same description as the match, if one was found.
            descriptions.Add(match.Text);

            var dupes = ddl.Items.Cast<ListItem>().Where(i => i.Text == match.Text && i.Value != match.Value);
            dupes.ToList().ForEach(itemsToRemove.Add);

            foreach (ListItem item in ddl.Items)
            {
                if (item.Equals(match)) continue;

                if (descriptions.Contains(item.Text, StringComparer.InvariantCultureIgnoreCase))
                {
                    itemsToRemove.Add(item);
                }
                else
                {
                    descriptions.Add(item.Text);
                }
            }

            // Remove the duplicates from the dropdown.
            itemsToRemove.ForEach(i => ddl.Items.Remove(i));

            if (newEntry)
            {
                ddl.Items.Add(match);
                ddl.SelectedValue = match.Value;
            }
        }


    }
}