using System;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Command;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using FirstLook.Merchandising.DomainModel.Workflow;
using Merchandising.Messages;

namespace Wanamaker.WebApp.Workflow
{
    public partial class Pricing : WorkflowBasePage
    {
        
        private GetMiscSettings _miscSettings;
        private GetMiscSettings MiscSettings
        {
            get
            {
                if (_miscSettings == null)
                    _miscSettings = GetMiscSettings.GetSettings(BusinessUnitId);

                return _miscSettings;
            }
        }

        public InventoryData InventoryInfo
        {
            get
            {
                return invDataViewer.InventoryInfo;
            }
        }

        public bool StepComplete
        {
            get
            {
                return StepStatusCollection
                           .Fetch(BusinessUnitId, InventoryId)
                           .GetStatus(StepStatusTypes.Pricing) == ActivityStatusCodes.Complete;
            }
         }
        
        public string Vin
        {
            get
            {
                string str = InventoryInfo.VIN;
                if (string.IsNullOrEmpty(str))
                {
                    throw new ApplicationException("No VIN found for vehicle!");
                }
                return str;
            }
        }

        private VehicleConfiguration _conf;
        public VehicleConfiguration VehConfig
        {
            get
            {
                if (_conf == null)
                {
                    _conf = VehicleConfiguration.FetchOrCreateDetailed(BusinessUnitId, InventoryId,
                                                                                     Context.User.Identity.Name);
                }
                return _conf;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var businessUnit = BusinessUnitFinder.Instance().Find(BusinessUnitId);
            var hasFirstLook30Upgrade = businessUnit.HasDealerUpgrade(Upgrade.FirstLook30);

            var master = Master as Workflow;

            if (!IsPostBack)
            {
                if (InventoryId != 0)
                {
                    invDataViewer.SetNewVehicle(InventoryId);
                }
                invDataViewer.DataBind();
                invDataViewer.Visible = false;

            }

            wfsInventoryItem.VehicleIsPreOwned = InventoryInfo.IsPreOwned();
            wfsInventoryItem.HasMarketingUpgrade = HasMarketingUpgrade;

            if (!IsPostBack)
            {
                string url = InventoryData.GetPricingLink(BusinessUnitId, InventoryId,
                                                          ConfigurationManager.AppSettings["pricing_page_from_host"]);
                if (string.IsNullOrEmpty(url))
                {
                    if (InventoryInfo.IsNew())
                    {
                        //trueCar.SetTrueCarParameters(VehConfig.ChromeStyleID, InventoryInfo.VIN, InventoryInfo.ListPrice);
                        //mv.ActiveViewIndex = 2;
                    }
                    else
                    {
                        mv.ActiveViewIndex = 0;
                    }
                }
                else
                {
                    priceFrame.Attributes["src"] = url;
                }

                // Pass relevant data to client
                InventoryIdHidden.Value = InventoryId.ToString();
                BusinessUnitIdHidden.Value = BusinessUnitId.ToString();

                // CTR graph stuff
                CtrGraphLink.Visible = MiscSettings.ShowCtrGraph;
                CtrGraphLink.Attributes.Add("data-BusinessUnitId", BusinessUnitId.ToString());
                CtrGraphLink.Attributes.Add("data-inventoryid", InventoryInfo.InventoryID.ToString());
                CtrGraphLink.Attributes.Add("data-stockNum", InventoryInfo.StockNumber.ToString());
                var trim_str = GetTrimDisplay( InventoryInfo.ChromeStyleId.ToString(), InventoryInfo.Trim, InventoryInfo.Model, InventoryInfo.AfterMarketTrim );
                var ymmt_str = String.Format("{0} {1} {2} {3}", InventoryInfo.Year, InventoryInfo.Make, InventoryInfo.Model, trim_str);
                CtrGraphLink.Attributes.Add("data-ymmt", ymmt_str);
            }
          
        }

        protected void Page_PreRender ( object sender, EventArgs e )
        {
            // Set pricing page steps based on preapprove status
            var command = new GetAutoApproveSettings( WorkflowState.BusinessUnitID );
            AbstractCommand.DoRun( command );
            if ( !command.HasSettings || command.Status == AutoApproveStatus.Off )
            {
                ApprovalButton.NavigateUrl = WorkflowState.CreateUrl( "~/Workflow/ApprovalSummary.aspx" );
                PARPricingSteps.Visible = false;
                NonPARPricingSteps.Visible = true;
            }
            else
            {
                GoToNextButton.NavigateUrl = string.IsNullOrEmpty( wfsInventoryItem.NextUrl ) ?
                    WorkflowState.CreateUrl( "~/Workflow/Inventory.aspx" ) : wfsInventoryItem.NextUrl;
            }   
        }

        protected void wfhHeader_InventorySearching(object sender, EventArgs e)
        {
            //Get the matches for the search terms
            var allInventory = InventoryDataFilter.FetchListFromSession(BusinessUnitId);

            Bin bin = Bin.Create(wfsInventoryItem.SearchText);
            List<InventoryData> searchResults = InventoryDataFilter.WorkflowSearch(BusinessUnitId, allInventory, 0, 9999, String.Empty, bin);
            
            if (searchResults.Count > 1 || searchResults.Count == 0)
            {
                //More than 1 match, so send to the home page so they can click the desired vehicle
                //OR no results found
                Response.Redirect("~/Workflow/Inventory.aspx?search=" + wfsInventoryItem.SearchText);
            }
            else
            {
                FindSearchResult(searchResults[0].InventoryID);
            }
        }

        protected void FindSearchResult(int inventoryId)
        {
            var results = wfsInventoryItem.LocateVehicle(inventoryId);
            SetSearchResult(inventoryId, results);
        }

        protected void SetSearchResult(int inventoryId, LocateVehicleResults result)
        {
            string queryUrl = WorkflowState.CreateUrl(HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath, inventoryId, 
                result.ActiveListType, result.UsedOrNewFilter, result.AgeBucketId);
            Response.Redirect(queryUrl);
        }
    }
}