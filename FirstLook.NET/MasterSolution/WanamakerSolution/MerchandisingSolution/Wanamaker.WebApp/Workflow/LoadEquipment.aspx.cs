using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Command;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using FirstLook.Merchandising.DomainModel.Workflow;
using FirstLook.Merchandising.WebControls.UI;
using Wanamaker.WebApp.Workflow.Controls;
using log4net;
using Merchandising.Messages.AutoApprove;

namespace Wanamaker.WebApp.Workflow
{
    public partial class LoadEquipment : WorkflowBasePage
    {
        #region Logging

        private static readonly ILog Log = LogManager.GetLogger( typeof( LoadEquipment ).FullName );

        #endregion

        private bool _justUpdatedStyleIdWorkaround;

        private GenericEquipmentCollection _checkedEquipment;

        private GenericEquipmentCollection _selectedEquipment;

        #region Injected dependencies

        public IAdMessageSender MessageSender { get; set; }
        public IInventorySearch InventorySearch { get; set; }
        public IDiscountPricingCampaignManager PricingCampaignManager { get; set; }

        #endregion

        public GenericEquipmentCollection SelectedEquipment
        {
            get
            {
                return _selectedEquipment ?? ( _selectedEquipment = GenericEquipmentCollection.FetchSelections( BusinessUnitId, InventoryId ) );
            }
        }

        private VehicleConfiguration _conf;
        public VehicleConfiguration Conf
        {
            get
            {
                return _conf ??
                       ( _conf = VehicleConfiguration.FetchOrCreateDetailed( BusinessUnitId, InventoryId, Context.User.Identity.Name ) );
            }
            set
            {
                _conf = value;
            }
        }

        public InventoryData InventoryInfo
        {
            get
            {
                return invDataViewer.InventoryInfo;
            }
        }

        public bool StepComplete
        {
            get
            {
                var statuses = StepStatusCollection.Fetch( BusinessUnitId, InventoryId );
                return statuses.GetStatus( StepStatusTypes.ExteriorEquipment ) == ActivityStatusCodes.Complete
                       && statuses.GetStatus( StepStatusTypes.InteriorEquipment ) == ActivityStatusCodes.Complete;
            }
        }

        public string Vin
        {
            get
            {
                string str = InventoryInfo.VIN;
                if ( string.IsNullOrEmpty( str ) )
                {
                    throw new ApplicationException( "No VIN found for vehicle!" );
                }
                return str;
            }
        }

        protected void Page_Init ( object sender, EventArgs e )
        {
            _checkedEquipment = new GenericEquipmentCollection();
        }

        protected void Page_Load ( object sender, EventArgs e )
        {
            if ( !IsPostBack )
            {
                if ( InventoryId != 0 )
                {
                    invDataViewer.SetNewVehicle( InventoryId );
                }

                invDataViewer.DataBind();
                NewVinDataEditor.SetChromeStyle(invDataViewer.SelectedChromeStyle, InventoryId, BusinessUnitId);
            }

            
            wfsInventoryItem.VehicleIsPreOwned = InventoryInfo.IsPreOwned();
            wfsInventoryItem.HasMarketingUpgrade = HasMarketingUpgrade;

            if ( !IsPostBack )
            {

                //get after market and bind it
                AdditionalItems = Conf.AfterMarketEquipment;
                ExtraEquipment.DataSource = AdditionalItems;
                ExtraEquipment.DataBind();

                StepCompletedCkb.Checked = StepComplete;

            }
            //Set inventory pricing link
            SetPricingLinkForInventory();
            // Pass relevant data to client
            InventoryIdHidden.Value = InventoryId.ToString();
            BusinessUnitIdHidden.Value = BusinessUnitId.ToString();
        }

        protected void Page_PreRender ( object sender, EventArgs e )
        {
            if ( !IsPostBack )
            {
                SetVehicleData( InventoryId );
            }
            if ( Conf.ChromeStyleID < 1 )
            {
                NewVinDataEditor.ClearDropDownLists();
            }
        }



        protected void Style_Selecting ( object sender, ObjectDataSourceSelectingEventArgs e )
        {
            e.InputParameters[ "vin" ] = Vin;
        }

        protected void Save_Clicked ( object sender, EventArgs e )
        {
            //should have already processed all the feature changed event args...
            Save();

            HandleNotificationsDisplay();
        }

        private void HandleNotificationsDisplay()
        {
            if (IsPreApproved(BusinessUnitId) == false
                || InventoryInfo.StatusBucket == MerchandisingBucket.Online
                || InventoryInfo.StatusBucket == MerchandisingBucket.OnlineNeedsAttention
                || InventoryInfo.StatusBucket == MerchandisingBucket.AllOnline)
            {
                WarningPanel.Visible = true;
                AdApprovalWarning.Visible = true;
            }
            else
            {
                WarningPanel.Visible = false;
                AdApprovalWarning.Visible = false;
            }
        }

        private void Save ()
        {
            //
            //From LoadBasics.aspx
            //
            if ( _justUpdatedStyleIdWorkaround )
            {
                _conf = null;
            }
          
            var colorPair = ColorPair.Parse(colorSelection.SelectedValue);

            if (colorSelection.Items.Count > 0)
            {
                Conf.ExteriorColor1 = colorPair.First.Description;
                Conf.ExteriorColor2 = colorPair.Second.Description;

                Conf.ExteriorColorCode = colorPair.First.ColorCode;
                Conf.ExteriorColorCode2 = colorPair.Second.ColorCode;
            }

            if (intColorSelection.Items.Count > 0)
            {
                if (string.IsNullOrEmpty(intColorSelection.SelectedValue))
                {
                    Conf.InteriorColor = string.Empty;
                    Conf.InteriorColorCode = string.Empty;
                }
                else
                {
                    Conf.InteriorColorCode = intColorSelection.SelectedValue;
                    Conf.InteriorColor = intColorSelection.SelectedItem.Text;
                }

            }

            Conf.KeyInformationList.Clear();
            foreach ( ListItem infoItem in AdditionalInfoItemsList.Items )
            {
                if ( infoItem.Selected )
                {
                    Conf.KeyInformationList.Add( new KeyInformation( int.Parse( infoItem.Value ), string.Empty ) );
                }
            }

            //
            //Originally from this page
            //
           
            NewVinDataEditor.SaveMapping( invDataViewer.SelectedChromeStyle, InventoryId, BusinessUnitId, Context.User.Identity.Name );
          
            VehicleConfiguration tmpConfig = Conf;

            //alter equipment based on selections, was selected equipment
            GenericEquipmentCollection tmpSelections = tmpConfig.VehicleOptions.generics;
            tmpSelections.Clear();
            tmpSelections.AddRange( _checkedEquipment );

            // Returning a list of generic equipment from the override selector user control.
            List<GenericEquipment> equipmentOverrides = NewVinDataEditor.MyEquipment;
            foreach ( GenericEquipment equipmentOverride in equipmentOverrides )
            {
                tmpSelections.Add( equipmentOverride );
            }
            tmpConfig.AfterMarketEquipment = AdditionalItems;
            tmpConfig.Save( BusinessUnitId, User.Identity.Name );
            //tmpSelections.Save(BusinessUnitId, InventoryId);
            Clean();

            PricingCampaignManager.ApplyDiscount(BusinessUnitId, InventoryId, User.Identity.Name, GetType().FullName);

            MessageSender.SendAutoApproval(new AutoApproveMessage(BusinessUnitId, InventoryId, Context.User.Identity.Name), InventoryInfo.IsNew(), GetType().FullName);
        }

        protected void OnCheckedEquipment ( object sender, DataItemEventArgs e )
        {
            GenericEquipment ge = new GenericEquipment
            {
                Category = { Description = e.Item.Name, CategoryID = e.Item.Id },
                IsStandard = e.IsStandard
            };

            _checkedEquipment.Add( ge );
        }

        protected void Item_Bound ( object sender, DataItemEventArgs e )
        {
            if ( SelectedEquipment.Contains( e.Item.Id ) )
            {
                e.Item.Selected = true;
            }
        }

        private void Dirtify ()
        {
            //dirtify the save button
            saveBtn.ForeColor = Color.Red;
            saveBtn.Font.Bold = true;
        }
        private void Clean ()
        {
            //clean the save button
            saveBtn.ForeColor = Color.Black;
            saveBtn.Font.Bold = false;
        }

        #region afterMarketEquipment

        protected void RemoveExtraEquipment ( object sender, ImageClickEventArgs e )
        {
            ImageButton sendingButton = sender as ImageButton;
            if ( sendingButton == null ) return;

            string textToRemove = sendingButton.CommandArgument;
            List<string> addItemsTemp = AdditionalItems;
            addItemsTemp.Remove( textToRemove );
            AdditionalItems = addItemsTemp;

            ExtraEquipment.DataSource = AdditionalItems;
            ExtraEquipment.DataBind();
            Dirtify();

        }

        protected void Stickies_Selecting ( object sender, SqlDataSourceSelectingEventArgs e )
        {
            e.Command.Parameters[ "@BusinessUnitID" ].Value = BusinessUnitId;
        }

        protected void EquipmentSearch_Click ( object sender, EventArgs e )
        {
            Button sendingButton = sender as Button;
            if ( sendingButton == null )
            {
                return;
            }

            string eq = EquipmentFinder.Text.Trim( " ".ToCharArray() );
            if ( !string.IsNullOrEmpty( eq ) )
            {
                List<string> addItemsTemp = AdditionalItems;
                addItemsTemp.Add( eq );
                AdditionalItems = addItemsTemp;

                ExtraEquipment.DataSource = AdditionalItems;
                ExtraEquipment.DataBind();
                Dirtify();
            }
        }

        public List<string> AdditionalItems
        {
            get
            {
                List<string> o = ViewState[ "adds" ] as List<string>;
                return o ?? new List<string>();
            }
            set
            {

                ViewState[ "adds" ] = value;
            }
        }
        #endregion

        protected void wfhHeader_InventorySearching ( object sender, EventArgs e )
        {
            //Get the matches for the search terms
            var allInventory = InventoryDataFilter.FetchListFromSession(BusinessUnitId);

            Bin bin = Bin.Create( wfsInventoryItem.SearchText );
            List<InventoryData> searchResults = InventoryDataFilter.WorkflowSearch( BusinessUnitId, allInventory, 0, 9999, String.Empty, bin );

            if ( searchResults.Count > 1 || searchResults.Count == 0 )
            {
                //More than 1 match, so send to the home page so they can click the desired vehicle
                //OR no results found
                Response.Redirect( "~/Workflow/Inventory.aspx?search=" + wfsInventoryItem.SearchText );
            }
            else
            {
                FindSearchResult( searchResults[ 0 ].InventoryID );
            }
        }

        protected void FindSearchResult ( int inventoryId )
        {
            var results = wfsInventoryItem.LocateVehicle( inventoryId );
            SetSearchResult( inventoryId, results );
        }

        protected void SetSearchResult ( int inventoryId, LocateVehicleResults result )
        {
            string queryUrl = WorkflowState.CreateUrl( HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath, 
                inventoryId, result.ActiveListType, result.UsedOrNewFilter, result.AgeBucketId );
            Response.Redirect( queryUrl );
        }

        protected void ExtColor_Bound(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;
            if (ddl == null) return;

            ddl.Items.Insert(0, new ListItem("Choose a color...", ""));
            var first = new ChromeColor(Conf.ExteriorColor1, string.Empty, Conf.ExteriorColorCode, string.Empty);
            var second = new ChromeColor(Conf.ExteriorColor2, string.Empty, Conf.ExteriorColorCode2, string.Empty);
            var pair = new ColorPair(first, second);

            if (!string.IsNullOrEmpty(pair.GetPairCode()))
            {
                foreach (ListItem listItem in ddl.Items)
                {
                    var selectedPair = ColorPair.Parse(listItem.Value);
                    if (string.IsNullOrEmpty(selectedPair.GetPairCode()) || selectedPair.GetPairCode() != pair.GetPairCode()) continue;

                    ddl.SelectedValue = listItem.Value;
                    return;
                }
            }

            if (!string.IsNullOrEmpty(pair.GetPairDescription()))
            {
                foreach (ListItem listItem in ddl.Items)
                {
                    var selectedPair = ColorPair.Parse(listItem.Value);
                    if (selectedPair.GetPairDescription().ToLower() != pair.GetPairDescription().ToLower()) continue;

                    ddl.SelectedValue = listItem.Value;
                    return;
                }
            }

            ddl.SelectedIndex = 0;
        }

        protected void IntColorBound(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;
            if (ddl == null) return;

            ddl.Items.Insert(0, new ListItem("Choose a color...", ""));

            string color = Conf.InteriorColor;
            string code = Conf.InteriorColorCode;

            var filter = new InteriorColorDropdownFilter();
            ListItem match = filter.FindInteriorColor(ddl, code, color);

            // remove duplicate descriptions from the list, keeping the vehicle's color/code.
            filter.RemoveDuplicateColors(match, color, code, ddl);

            // select the match, if one was found
            if (match != null)
            {
                if (ddl.Items.Cast<ListItem>().Any(i => i.Value == match.Value))
                {
                    ddl.Items.Cast<ListItem>().First(i => i.Value == match.Value).Selected = true;
                    return;
                }
            }

            // If we couldn't match anything ...
            var selected = ddl.Items.Cast<ListItem>().Where(i => i.Selected);
            if (!selected.Any() && ddl.Items.Count > 0)
            {
                ddl.SelectedIndex = 0;
            }
        }

        protected string GetColorSelectJs ( string chromeColorDesc, string chromeColorPair )
        {
            string js = "javascript:setExteriorColor('" + colorSelection.ClientID + "','" + chromeColorDesc + "','" + chromeColorPair + "');";
            return js;
        }

        protected void AddColor_Click ( object sender, EventArgs e )
        {
            colorSelection.Items.Add(new ListItem(CustomColor.Text, ":" + CustomColor.Text));
            colorSelection.SelectedValue = ":" + CustomColor.Text;
            saveBtn.ForeColor = Color.Red;
        }

        protected object GetCustomColorJs ()
        {
            return "setExteriorColor('" + colorSelection.ClientID + "', document.getElementById('customExterior').value, document.getElementById('customExterior').value);return false;";
        }

        protected void ChromeColorsSelecting ( object sender, ObjectDataSourceSelectingEventArgs e )
        {
            if (
                string.IsNullOrEmpty( e.InputParameters[ "chromeStyleId" ].ToString() )
                || e.InputParameters[ "chromeStyleId" ].ToString() == "-1" )
            {
                e.Cancel = true;
                return;
            }
            e.InputParameters[ "currentExtColorDesc1" ] = Conf.ExteriorColor1;
            e.InputParameters[ "currentExtColorDesc2" ] = Conf.ExteriorColor2;
        }

        protected void ChromeInteriorColorsSelecting ( object sender, ObjectDataSourceSelectingEventArgs e )
        {
            if (
                string.IsNullOrEmpty( e.InputParameters[ "chromeStyleId" ].ToString() )
                || e.InputParameters[ "chromeStyleId" ].ToString() == "-1" )
            {
                e.Cancel = true;
                return;
            }
        }

        protected void AdditionalInfoItemsList_DataBound ( object sender, EventArgs e )
        {
            // Appends Google Analytics click handler
            try
            {
                foreach ( ListItem item in AdditionalInfoItemsList.Items )
                {
                    item.Attributes.Add("onclick", "_gaq.push(['_trackEvent', 'Equipment Page', 'Click', 'Key Information Item']);");
                }
            }
            catch ( Exception exception )
            {
                Log.Error( "Failed to append google analytics to Key Information checkbox", exception );
            }
        }

        protected void AdditionalInfo_Selecting ( object sender, ObjectDataSourceSelectingEventArgs e )
        {
            e.InputParameters[ "BusinessUnitID" ] = BusinessUnitId;
        }

        protected void OnLotEquipmentSelecting ( object sender, ObjectDataSourceSelectingEventArgs e )
        {
            e.InputParameters[ "BusinessUnitID" ] = BusinessUnitId;
            e.InputParameters[ "InventoryID" ] = InventoryId;
        }

        protected void SetPricingLinkForInventory()
        {
            var results = wfsInventoryItem.LocateVehicle(InventoryId);
            invDataViewer.SetPricingLink(results);

        }

        public void SetVehicleData ( int inventoryId )
        {
            try
            {
                discriminator.Vin = Vin;

                AdditionalInfoItemsList.DataBind();
                foreach ( ListItem infoItem in AdditionalInfoItemsList.Items )
                {
                    ListItem item = infoItem;
                    infoItem.Selected = Conf.KeyInformationList.Exists( info => info.KeyInformationId ==
                                                                               int.Parse( item.Value ) );
                }
            }
            catch
            {
                //show not found alert
                Page.ClientScript.RegisterClientScriptBlock( Page.GetType(), "alertMe", "alert('Vehicle Not Found')", true );
                Response.Redirect( "~/Workflow/Inventory.aspx" );
            }
        }

        // This override added for FB 17805
        public override void VerifyRenderingInServerForm ( Control control ) { }

        //This override added for FB 18210. Needs to be override in tandum with VerifyRenderingInServerForm above
        public override bool EnableEventValidation
        {
            get { return false; }
            set
            {
                base.EnableEventValidation = value;
            }
        }


        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            if (args is TrimChangedEventArgs)
            {
                HandleTrimChanged((args as TrimChangedEventArgs).StyleId);
                return true;
            }

            return false;
        }

        private void HandleTrimChanged(int styleId)
        {
            Conf = invDataViewer.VehicleConfiguration;
            _justUpdatedStyleIdWorkaround = true; //we want to make sure that we reload VehConfig during same page cycle due to version code
            NewVinDataEditor.SetChromeStyle(styleId, InventoryId, BusinessUnitId);

            MessageSender.SendAutoApproval(new AutoApproveMessage(BusinessUnitId, InventoryId, Context.User.Identity.Name), InventoryInfo.IsNew(), GetType().FullName);
        }
    }
}