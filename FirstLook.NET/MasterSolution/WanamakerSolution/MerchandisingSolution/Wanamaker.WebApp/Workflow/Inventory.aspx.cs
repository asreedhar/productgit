﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using BulkWindowSticker;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.Utilities;
using FirstLook.LotIntegration.DomainModel;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.DataAccess;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using FirstLook.Merchandising.DomainModel.Workflow;
using FirstLook.Merchandising.DomainModel.Workflow.Aging;
using FirstLook.Merchandising.DomainModel.Workflow.Presenter;
using log4net;
using MAX.BuildRequest;
using MAX.Entities;
using MAX.Entities.Enumerations;
using MAX.Entities.Extensions;
using MAX.Entities.Filters;
using Merchandising.Messages;
using MvcMiniProfiler;
using Wanamaker.WebApp.Helpers;
using Wanamaker.WebApp.Workflow.Controls;

namespace Wanamaker.WebApp.Workflow
{
    public partial class Inventory : WorkflowBasePage, IFiltersModel, IAgeFilterModel
    {
        #region Logging

        private static readonly ILog Log = LogManager.GetLogger( typeof( Inventory ).FullName );

        #endregion

        private readonly IWorkflowRepository WorkflowRepository = Registry.Resolve<IWorkflowRepository>();
        private readonly IAgeBucketRepository AgeBucketRepository = Registry.Resolve<IAgeBucketRepository>();
        private readonly IDealerServices DealerServices = Registry.Resolve<IDealerServices>();
        private readonly IUserServices UserServices = Registry.Resolve<IUserServices>();

		#region Injected Dependencies
		
		public IBuildRequestRepository BuildRequestRepository { get; set; }
		
		#endregion Injected Dependencies
		
		
		private Dictionary<int, bool> AsynchronousAutoloadSettings
	    {
		    get { return _lazyDict.Value; }
	    }

	    private readonly Lazy<Dictionary<int, bool>> _lazyDict;

	    readonly ProfileCommon _profile = new ProfileCommon();

        private readonly AutoloadManager _autoloadManager = new AutoloadManager( Registry.Resolve<IAutoloadRepository>() );

        private GetMiscSettings _miscSettings;

        private GetMiscSettings MiscSettings
        {
            get
            {
                if (_miscSettings == null)
                    _miscSettings = GetMiscSettings.GetSettings(BusinessUnitId);

                return _miscSettings;
            }
        }

        private int? AgeBucketId
        {
            get { return CurrentFilterView.AgeFilterControl.Presenter.CurrentAgeBucketId; }
        }

        public UsedOrNewFilter UsedOrNewFilter
        {
            get { return ( UsedOrNewFilter )int.Parse( InventoryTypeDDL.SelectedValue ); }
        }

        private IWorkflow CurrentWorkflow
        {
            get { return Filters.Presenter.CurrentWorkflow; }
            set { Filters.Presenter.CurrentWorkflow = value; }
        }

        private WorkflowType CurrentWorkflowType
        {
            get { return CurrentWorkflow.Type; }
            set { CurrentWorkflow = WorkflowRepository.GetWorkflow( value ); }
        }

        private bool? _HasAutoWindowSticker;
        public bool HasAutoWindowSticker
        {
            get
            {
                if(!_HasAutoWindowSticker.HasValue)
                {
                    var windowStickerRepository = new WindowStickerRepository();
                    _HasAutoWindowSticker = windowStickerRepository.HasAutoWindowSticker(BusinessUnitId);
                }
                return _HasAutoWindowSticker.Value;
            }
        }

	    public Inventory ()
	    {
			_lazyDict = new Lazy<Dictionary<int, bool>>(() => BuildRequestRepository.AsynchronousSiteSupportSettings(BusinessUnitId));
	    }

        protected void Page_Init ( object sender, EventArgs e )
        {
            Log.TraceFormat( "Page_Init() called for dealer {0} from sender {1} with EventArgs {2}", BusinessUnitId, sender, e );

            if (!IsPostBack)
            {
                Log.Trace("Not a postback.");

                //Expire the dealer's inventory cache so it will be grabbed fresh
                Log.Trace("Removing inventory from Session.");
                InventoryDataFilter.ClearListFromSession();
            }
            else
            {
                Log.Trace("This is a postback.");
            }
        }

        protected override void OnLoad ( EventArgs e )
        {

            base.OnLoad( e );

            //Tomcat session is set by reuest to two images to NextGen and IMT.  In some cases e.g., it might not be set so set it here.
            var tomcatSessionExists = Request.Cookies[ "ts_dealer" ] != null;
            tomcatSessionImages.Visible = !tomcatSessionExists;

            if (Request.FromRubyHomePage())
                _profile.NewUsed = (int)UsedOrNewFilter.Used;
            
            using ( MiniProfiler.Current.Step( "Page Load" ) )
            {

                Log.TraceFormat( "OnLoad() called for dealer {0} with EventArgs {1}", BusinessUnitId, e );

                errors.Visible = false;

                if ( !IsPostBack )
                {
                    InventoryTypeDDL.SelectedValue = _profile.NewUsed.ToString( CultureInfo.InvariantCulture );
                    SetSearchTextFromSession();
                    ParseQueryString();

                    Pager1.ResetPageNumberToZero();
                    incentives.Visible = MiscSettings.BulkPriceNewCars;
                    viewCampaigns.Visible = MiscSettings.BulkPriceNewCars;
                }
            }
        }

        private void SetupExportHyperlink()
        {
            ExportToExcel.Visible = true;
            SetupExportToExcelLink();
        }

        private void SetupExportToExcelLink()
        {
            var dealer = DealerServices.GetDealerById(BusinessUnitId);
            ExportToExcel.NavigateUrl = 
                string.Format("~/Tabular.aspx/{0}/inventory.csv?filter={1}&newused={2}&agebucket={3}&search={4}",
                dealer.Code, CurrentWorkflowType, (int)UsedOrNewFilter, AgeBucketId, Uri.EscapeDataString(InventorySearch.SearchText));
        }

        //This can be used coming from other urls.
        private void SetSearchTextFromSession ()
        {
            var searchText = Session[ "SearchText" ];
            if ( searchText != null )
            {
                LoadSearch( searchText.ToString() );
                Session.Remove( "SearchText" );
            }

        }

        //This can be used coming from other urls.
        private void ParseQueryString ()
        {
            if ( Page.IsPostBack )
                return;

            ParseQueryString_Filter();
            ParseQueryString_UsedNew();
            ParseQueryString_ErrorMsg();
            ParseQueryString_Search();
        }

        private void ParseQueryString_Filter ()
        {
            var filter = Request.QueryString[ "filter" ];
            if ( filter != null )
            {
                WorkflowType bucket;
                if ( Enum.TryParse( filter, out bucket ) )
                    CurrentWorkflowType = bucket;
            }
        }

        private void ParseQueryString_UsedNew ()
        {
            var usednew = Request.QueryString[ "usednew" ];
            if ( usednew != null &&
                InventoryTypeDDL.Items.FindByValue( usednew ) != null )
            {
                InventoryTypeDDL.SelectedValue = usednew;
            }
        }

        private void ParseQueryString_ErrorMsg ()
        {
            var errorMessage = Request[ "errorMsg" ];

            if ( !string.IsNullOrEmpty( errorMessage ) )
            {
                Log.DebugFormat( "The following error message was passed on the HttpRequest: {0}", errorMessage );
                errorLbl.Text = errorMessage;
                errors.Visible = true;
            }
        }

        private void ParseQueryString_Search ()
        {
            var search = Request[ "search" ];
            Log.TraceFormat( "Search value {0} was found in the request.", search );

            if ( !string.IsNullOrEmpty( search ) )
                LoadSearch( search );
        }

        protected void OnResetSearch(object sender, EventArgs e)
        {
            ResetSearch();
        }

        private void ResetSearch ()
        {
            SetSearchText("");
            Pager1.ResetPageNumberToZero();
        }

        private void SetSearchText(string val)
        {
            InventorySearch.SearchText = val;
            CurrentFilterView.CurrentSearchText = val;
        }

        private string GetMaxSellingAndEmailUrl ( int invid )
        {
            return InventoryData.GetPricingLink( BusinessUnitId, invid, ConfigurationManager.AppSettings[ "marketing_page_from_host" ] );
        }
       

        protected void OnSellingAndEmailClicked ( object sender, CommandEventArgs e )
        {
            var inventoryId = Convert.ToInt32( e.CommandArgument );
            var url = GetMaxSellingAndEmailUrl( inventoryId );
            Response.Redirect( url );
        }

        private void FindVehicleAndRedirectToUrl ( int invid, string url )
        {
            Log.TraceFormat( "FindVehicleAndRedirectTo() called for inventoryId {0} and url {1}.", invid, url );

            var result = FindSearchResult( invid );

            if ( !result.FoundResult )
                throw new ApplicationException( "Could not find inventory" );

            Log.Trace( "FindVehicleAndRedirectToUrl() found the correct vechicle." );
            Log.TraceFormat( "Redirecting to url {0}", url );
            
            url = WorkflowState.CreateUrl(url, invid, result.ActiveListType, result.UsedOrNewFilter, result.AgeBucketId);
            Response.Redirect(url);
        }

        private void FindVehicleAndRedirectToNewPingUrl(int invid, string url)
        {
            Log.TraceFormat("FindVehicleAndRedirectToNewPingUrl() called for inventoryId {0} and url {1}.", invid, url);

            var result = FindSearchResult(invid);

            if (!result.FoundResult)
                throw new ApplicationException("Could not find inventory");

            url = WorkflowState.CreatePricingPingUrl(url,invid, BusinessUnitId, result.ActiveListType, result.UsedOrNewFilter, result.AgeBucketId);

            Log.Trace("FindVehicleAndRedirectToNewPingUrl() found the correct vechicle.");
            Log.TraceFormat("Redirecting to url {0}", url);

            Response.Redirect(url);
        }
        protected void BtnInventoryItemClick ( object sender, CommandEventArgs e )
        {
            string oldPricingPage = "Pricing.aspx";
            string newPricingPage = "~/PricingAnalysis/pingone";
            int invid = Convert.ToInt32( e.CommandArgument );
            var url = CurrentWorkflow.GetNavigationUrl();

            Log.TraceFormat( "BtnInventoryItemClick() called.  We should be redirecting to {0} for inventoryId {1}", url, invid );
            if (url == oldPricingPage && DealerPingAccess.CheckDealerPingAccess(BusinessUnitId))
            {
                FindVehicleAndRedirectToNewPingUrl(invid, newPricingPage);
            }
            else
            {
                FindVehicleAndRedirectToUrl(invid, url);     
            }
        }


        protected void BtnDocumentIconClick ( object sender, CommandEventArgs e )
        {
            int invid = Convert.ToInt32( e.CommandArgument );
            const string URL = "~/Workflow/LoadEquipment.aspx";

            Log.TraceFormat( "BtnDocumentIconClick() called.  We should be redirecting to {0} for inventoryId {1}", URL, invid );
            FindVehicleAndRedirectToUrl( invid, URL );
        }

        protected void BtnPhotoIconClick ( object sender, CommandEventArgs e )
        {
            int invid = Convert.ToInt32( e.CommandArgument );
            const string URL = "Photos.aspx";

            Log.TraceFormat( "BtnPhotoIconClick() called.  We should be redirecting to {0} for inventoryId {1}", URL, invid );
            FindVehicleAndRedirectToUrl( invid, URL );
        }

        protected void BtnPricingIconClick ( object sender, CommandEventArgs e )
        {
            int invid = Convert.ToInt32( e.CommandArgument );
            string url;
            if (DealerPingAccess.CheckDealerPingAccess(BusinessUnitId))
            {
                url = "~/PricingAnalysis/pingone";
                Log.TraceFormat("BtnPricingIconClick() called.  We should be redirecting to {0} for inventoryId {1}", url, invid);
                FindVehicleAndRedirectToNewPingUrl(invid, url);
            }
            else
            {
                url = GetPricingIconUrl(invid);
                Log.TraceFormat("BtnPricingIconClick() called.  We should be redirecting to {0} for inventoryId {1}", url, invid);
                FindVehicleAndRedirectToUrl(invid, url);
            }
        }

        private string GetPricingIconUrl (int invid)
        {
            var invData = InventoryData.Fetch(BusinessUnitId, invid);
            return invData.IsNew() ? "ApprovalSummary.aspx" : "Pricing.aspx";
        }

        protected void BtnPendingApprovalIconClick ( object sender, CommandEventArgs e )
        {
            int invid = Convert.ToInt32( e.CommandArgument );
            const string URL = "ApprovalSummary.aspx";

            Log.TraceFormat( "BtnPendingApprovalIconClick() called.  We should be redirecting to {0} for inventoryId {1}", URL, invid );
            FindVehicleAndRedirectToUrl( invid, URL );
        }

        protected void BtnOutdatedPriceClick ( object sender, CommandEventArgs e )
        {
            int invid = Convert.ToInt32( e.CommandArgument );
            const string URL = "ApprovalSummary.aspx";

            Log.TraceFormat( "BtnOutdatedPriceClick() called.  We should be redirecting to {0} for inventoryId {1}", URL, invid );
            FindVehicleAndRedirectToUrl( invid, URL );
        }

        public string SortExpression
        {
            get 
            {
                var sortDirection = (SortDirection.Ascending.Equals((SortDirection) Session.GetOrSetDefault(SortDirection.Ascending, SessionKey.InventorySortDirection)) ? "ASC" : "DESC");
                var sortType = Session.GetOrSetDefault(SortType.InventoryReceivedDate, SessionKey.InventorySortType).ToString();
                
                return (sortType + " " + sortDirection).Replace("DESC", string.Empty).Trim();
            }
        }

        private void LoadSearch ( string searchText )
        {
            Log.TraceFormat( "Loading search: {0}", searchText );

            SetSearchText(searchText);
            LoadSearch();
        }

        private void LoadSearch ()
        {
            Log.Trace( "LoadSearch() called. Setting Bucket to All and PageNumber to 0." );
            Pager1.ResetPageNumberToZero();
        }

        protected void Page_PreRender ( object sender, EventArgs e )
        {
            if ( Log.IsDebugEnabled )
                Log.TraceFormat( "Page_PreRender() called from sender {0} with EventArgs {1}", sender, e );

            SetPagerOncePerRequest();

            CurrentFilterView.CurrentAgeBucket =
                AgeBucketRepository.GetBucketsForBusinessUnit(BusinessUnitId).GetBucket(AgeBucketId);
            CurrentFilterView.CurrentWorkflowSegments = Filters.Presenter.GetViewModel().CurrentWorkflowLabels;

            SetupExportHyperlink();
        }

        protected void BulkActionsSelectPreRender(object sender, EventArgs e)
        {
            UpdateAvailableBulkActions((ListControl)sender);
        }

        private bool pagerSet;
        private void SetPagerOncePerRequest ()
        {
            if ( pagerSet ) return;
            pagerSet = true;

            Log.Trace( "SetPagerOncePerRequest() called." );

            var count = InventoryDataFilter
                .FetchListFromSession( BusinessUnitId )
                .WorkflowFilter( CurrentWorkflowType, 
                    ConditionFilters.CreateFromInt((int)UsedOrNewFilter),
                    AgeBucketFilters.CreateFromBucketFilterMode(AgeBucketId),
                    InventorySearch.SearchText )
                .Count();

            
            Log.TraceFormat( "Count is {0}", count );

            Pager1.ItemCount = count;
            CurrentFilterView.CurrentWorkflowCount = count;

            var pagerVisible = count > 0;
            Pager.Visible = pagerVisible;
        }

        protected void InventoryDSList_Selecting ( object sender, ObjectDataSourceSelectingEventArgs e )
        {
            if ( Log.IsDebugEnabled )
            {
                Log.TraceFormat( "InventoryDSList_Selecting() called from sender {0} with ObjectDataSourceSelectingEventArgs {1}", sender, e );
                Log.TraceFormat( "Setting e.InputParameters[businessUnitId] to {0}", BusinessUnitId );
            }

            e.InputParameters[ "BusinessUnitId" ] = BusinessUnitId;

            var newUsedFilter = ( int )e.InputParameters[ "usedOrNewFilter" ];
            e.InputParameters.Remove( "usedOrNewFilter" );

            var searchText = ( string )e.InputParameters[ "searchText" ];
            e.InputParameters.Remove( "searchText" );

            e.InputParameters[ "bin" ] = CurrentWorkflow.CreateBin(
                ( UsedOrNewFilter )newUsedFilter, AgeBucketId, searchText );
            e.Arguments.SortExpression = SortExpression;

            var pageInfo = Pager1.GetCurrentPageInfo();
            e.Arguments.StartRowIndex = pageInfo.Start;
            e.Arguments.MaximumRows = pageInfo.Size;
        }

        protected void UsedNew_Selected ( object sender, EventArgs e )
        {
            _profile.NewUsed = int.Parse( InventoryTypeDDL.SelectedValue );

            ResetSearch();
            Log.TraceFormat( "UsedNew_Selected() called from sender {0} with EventArgs {1}", sender, e );

            _countCollection = null;
            SetListData();
        }

        private void SetListData ()
        {
            Log.Trace( "SetListData() called. Setting PageNumber to 0 and calaling InventoryTable.DataBind()." );

            Pager1.ResetPageNumberToZero();
            InventoryTableRepeater.DataBind();
        }

        protected void BulkChangeStatus(string changeStatusTo)
        {
            List<string> inventoryIds = GetSelectedInventoryItems();

            if (inventoryIds == null) return;

            Log.TraceFormat("Start BulkChangeStatus to {0} for Businessunit {1} InvnetoryCount {2}", changeStatusTo,
                            BusinessUnitId, inventoryIds.Count);

            foreach (string inventoryId in inventoryIds)
            {
                int invId;

                if (!int.TryParse(inventoryId, out invId)) continue;

                Log.TraceFormat("Status changing for InventoryId {0}.", invId);

                Log.TraceFormat("Fetching InventoryData for BusinessUnitId {0} and InventoryId {1}", BusinessUnitId,
                                invId);
                InventoryData inventoryData = InventoryData.Fetch(BusinessUnitId, invId);

                if (inventoryData != null)
                {
                    MerchandisingBucket inventoryStatusBucket = inventoryData.StatusBucket ??
                                                                MerchandisingBucket.Invalid;

                    Log.TraceFormat("The fetched InventoryData has StatusBucket {0}", inventoryStatusBucket);

                    if (inventoryStatusBucket == MerchandisingBucket.DoNotPost && changeStatusTo == "online")
                    {
                        Log.Trace("StatusBucket is DoNotPost. Moving to 'releasable'.");
                        InventoryDataSource.MoveToReleasable(BusinessUnitId, inventoryData.InventoryID, User.Identity.Name);
                    }
                    else if (changeStatusTo == "offline")
                    {
                        Log.Trace("StatusBucket is not DoNotPost. Moving offline.");
                        InventoryDataSource.MoveOffline(BusinessUnitId, invId, User.Identity.Name);
                    }
                }
            }

            // Expire the dealer's inventory cache so it will be grabbed fresh
            InventoryDataFilter.ClearListFromSession();

            Log.Trace("Calling WorkflowSelector.ClearCounts()");

            _countCollection = null;
            DataBind();
        }

        // search for an individual vehicle
        protected void StockSearch_Click ( object sender, EventArgs e )
        {
            Log.TraceFormat( "StockSearch_CLick() called from sender {0} with EventArgs {1}.", sender, e );
            LoadSearch();
        }

        protected void InventoryRow_DataBound ( object sender, ListViewItemEventArgs e )
        {
            using ( MiniProfiler.Current.Step( "Home.InventoryRow_DataBound" ) )
            {
                Log.TraceFormat( "InventoryRow_DataBound() called with sender {0} and GridViewRowEventArgs {1}", sender,
                                e );

                var listItem = e.Item;

                if (listItem == null)
                {
                    Log.Warn("The ListViewItemEventArgs.ListItem is null.");

                    // Is there any point in continuing further?
                    Log.Warn( "Returning early from InventoryRow_DataBound()." );
                    return;
                }

                var dataItem = e.Item.DataItem;
                if (dataItem == null)
                {
                    Log.Warn("The ListViewItemEventArgs.DataItem is null.");

                    // Is there any point in continuing further?
                    Log.Warn("Returning early from InventoryRow_DataBound().");
                    return;
                }

                var inventoryData = ( InventoryData )dataItem;

                Log.Trace("Row.DataItem is an InventoryData.");

                // Handle invalid VINs
                var validVinHidden = listItem.FindControl( "ValidVinHidden" ) as HtmlInputHidden;
                if ( validVinHidden != null )
                {
                    validVinHidden.Value = GetHasValidVinPattern( inventoryData ).ToString( CultureInfo.InvariantCulture ).ToLower();
                }


                var trimSelector = listItem.FindControl("InventoryTrimSelector") as TrimSelector;

                if (trimSelector != null)
                {
                    trimSelector.DataBind(listItem.DataItem as InventoryData);
                }

                var ctrGraphlink = listItem.FindControl("CtrGraphLink") as HyperLink;
                if (ctrGraphlink != null)
                    ctrGraphlink.Visible = MiscSettings.ShowCtrGraph;
            

                object objCertLb = listItem.FindControl("certifiedLbl");
                var certLb = objCertLb as Label;

                if ( objCertLb == null )
                {
                    Log.Trace("Cound not find the control 'certifiedLbl' in the current row.");
                }

                if ( certLb == null )
                {
                    Log.Trace("Could not cast the control returned by e.Row.FindControl('certifiedLbl') to a Label.");
                }

                if ( certLb != null )
                {
                    // This should be safe at this point b/c if the row.DataItem is not an InventoryData, we returned early.
                    bool cert = inventoryData.Certified;
                    if ( cert )
                    {
                        Log.Trace("The inventory is certified.");
                        certLb.Text = @"C";
                        certLb.ToolTip = inventoryData.CertificationTitle;
                    }
                    else
                    {
                        Log.Trace("The inventory is not certified.");
                    }
                }
            }
        }

        protected string GetThumbnailUrl ( object evalObj )
        {
            var urlOnFile = evalObj as string;
            if ( string.IsNullOrEmpty( urlOnFile ) )
            {
                return "~/App_Themes/MAX60/images/noImageAvailable.png";
            }
            return urlOnFile;
        }

        protected void InventoryTable_Sorting ( object sender, GridViewSortEventArgs e )
        {
        }

        private List<string> GetSelectedInventoryItems ()
        {
            return (from item in InventoryTableRepeater.Items
                                let ctrl = item.FindControl("BulkActionsCheckbox") as HtmlInputCheckBox
                                where ctrl != null && ctrl.Checked
                                select InventoryTableRepeater.DataKeys[item.DataItemIndex]
                                into dataKey where dataKey != null select dataKey.Value.ToString()).ToList();
           
        }

        
        protected void BuildWorkBook_Create ( object sender, EventArgs e )
        {
            //create build workbook with selected stock items from the list

            string invIds = string.Join(",", GetSelectedInventoryItems());

            if ( !string.IsNullOrEmpty( invIds ) )
            {
                Page.ClientScript.RegisterStartupScript( Page.GetType(), "buildsheets", "ShowWorkBook('" + invIds + "');", true );
            }

        }

        protected void Sort_Selected ( object sender, EventArgs e )
        {
            Pager1.ResetPageNumberToZero();
            InventoryTableRepeater.DataBind();
        }

        protected void SelectedGridViewRow_InventoryTable ( object sender, EventArgs e )
        {
            //GridViewRow gvr = InventoryTableRepeater.SelectedRow;
            //DataKey dk = InventoryTableRepeater.DataKeys[ gvr.DataItemIndex ];
            //if ( dk != null )
            //{
            //    string make = dk[ "Make" ].ToString().ToLower();
            //    idVinMakeCombos.Add( new BuildDataVehicleRequest( ( int )dk.Value, make, dk[ "Vin" ].ToString() ) );
            //}
        }

        protected bool GetMarketingVisibility ( InventoryData item )
        {
            return HasMarketingUpgrade && item.InventoryType == 2;
        }

        protected bool GetPricingVisibility ( InventoryData item, bool isNeededIcon )
        {
            //We only apply pricing icon for preowned...otherwise, it's hidden
            //we also only apply the right icon based on needed or notNeeded state

            return ( item.IsPreOwned() && ( item.NeedsPricing == isNeededIcon ) );
        }

        protected bool GetHasValidVinPattern ( InventoryData item )
        {
            return ChromeStyle.HasValidVinPattern( item.VIN );
        }

        protected void wfhHeader_InventorySearching ( object sender, EventArgs e )
        {
            LoadSearch( InventorySearch.SearchText );
        }

        private LocateVehicleResults FindSearchResult ( int inventoryId )
        {
            Log.TraceFormat("FindSearchResult() called for inventoryId {0}", inventoryId);

            var selector = new WorkflowSelector();
            var results = selector.LocateVehicleCurrentFilter( inventoryId, CurrentWorkflowType, AgeBucketId, ( int )UsedOrNewFilter ); //try current workflow
            if ( results.FoundResult )
                return results;

            return selector.LocateVehicleOpenFilter( inventoryId ); //look in entire inventory
        }

        WorkflowType? IFiltersModel.LoadWorkflow ()
        {
            return ( WorkflowType? )ViewState[ "CurrentWorkflow" ];
        }

        void IFiltersModel.SaveWorkflow (WorkflowType? workflow, bool clicked)
        {
            Log.TraceFormat("Workflow changed to {0}.", workflow);
            ViewState[ "CurrentWorkflow" ] = workflow;

            if(clicked)
                OnWorkflowClicked();
        }

        AutoApproveStatus IFiltersModel.GetAutoApproveStatus ()
        {
            return GetAutoApproveSettings.GetSettings( BusinessUnitId ).Status;
        }

        UsedOrNewFilter IFiltersModel.GetUsedOrNewFilter ()
        {
            return UsedOrNewFilter;
        }

        private IWorkflowCounts _countCollection;
        IWorkflowCounts IFiltersModel.GetWorkflowCounts (ISet<WorkflowType> workflowsToCount)
        {
            return _countCollection ?? (_countCollection = WorkflowRepository.CalculateCounts(
                InventoryDataFilter
                    .FetchListFromSession(BusinessUnitId)
                    .FilterBy(UsedOrNewFilter)));
        }

        IWorkflowRepository IFiltersModel.GetWorkflowRepository ()
        {
            return WorkflowRepository;
        }

        IAgeBuckets IAgeFilterModel.GetAgeBuckets()
        {
            return AgeBucketRepository.GetBucketsForBusinessUnit(BusinessUnitId);
        }

        int? IAgeFilterModel.LoadAgeBucket()
        {
            return (int?)ViewState["CurrentAgeBucket"];
        }

        void IAgeFilterModel.SaveAgeBucket(int? id)
        {
            
            Log.TraceFormat("Age bucket changed to {0}.", id);
            ViewState["CurrentAgeBucket"] = id;

            OnAgeBucketChanged();
        }

        private void OnAgeBucketChanged()
        {
            OnFilterCriteriaChanged();
        }

        private void OnWorkflowClicked ()
        {
            OnFilterCriteriaChanged();
        }

        private void OnFilterCriteriaChanged()
        {
            ResetSearch();
            SetPagerOncePerRequest();
            DataBind();
        }

        protected static string GoogleAnalytics ( string category, string action, string label )
        {
            return string.Format( "_gaq.push(['_trackEvent', '{0}', '{1}', '{2}']);",
                                 category, action, label );
        }

        protected void OnPageNumberChanged ( object sender, EventArgs e )
        {
            InventoryTableRepeater.DataBind();
        }


        
        protected void SortInventory(object sender, CommandEventArgs e)
        {
            if (! "SortInventory".Equals(e.CommandName)) return;
            var sortCommandArg = EnumHelper.GetEnumFromDescription<SortType>((string)e.CommandArgument);
            var currentSortType = Session.GetObject(SessionKey.InventorySortType);
            var doSwitchDirection = (currentSortType != null && sortCommandArg.Equals(currentSortType));
            var sortDirection = Session.GetOrSetDefault(SortDirection.Ascending, SessionKey.InventorySortDirection);

            Session.SetObject(SessionKey.InventorySortType, sortCommandArg);

            if (doSwitchDirection)
            { 

                switch (sortDirection)
                {
                    case SortDirection.Ascending:
                        Session.SetObject(SessionKey.InventorySortDirection, SortDirection.Descending);
                        break;
                    case SortDirection.Descending:
                        // Session[SortDirectionKey] = SortDirection.Ascending; // old
                        Session.SetObject(SessionKey.InventorySortDirection, SortDirection.Ascending);
                        break;
                }

            }

            InventoryTableRepeater.DataBind();
        }

        protected void BulkActionsSelectedIndexChanged(object sender, EventArgs e)
        {
            var bulkActions = sender as DropDownList;
            if (bulkActions == null || string.IsNullOrEmpty(bulkActions.SelectedValue)) return;
            if (bulkActions.SelectedValue == "workbook") BuildWorkBook_Create(sender, e);
            if (bulkActions.SelectedValue == "offline") BulkChangeStatus("offline");
            if (bulkActions.SelectedValue == "online") BulkChangeStatus("online");
            bulkActions.SelectedValue = string.Empty;

        }

        [Flags]
        private enum OfflineStatus
        {
            None = 0,
            NotOffline = 1,
            Offline = 2,
            Both = NotOffline | Offline
        }

        private OfflineStatus GetOfflineStatusForCurrentFilter()
        {
            return InventoryDataFilter
                .FetchListFromSession(BusinessUnitId)
                .WorkflowFilter(CurrentWorkflowType, 
                    ConditionFilters.CreateFromInt((int) UsedOrNewFilter), 
                    AgeBucketFilters.CreateFromBucketFilterMode(AgeBucketId), 
                    InventorySearch.SearchText)
                .Aggregate(OfflineStatus.None, 
                    (current, vehicle) => current 
                        | (WorkflowFilters.DoNotPost(vehicle) ? OfflineStatus.Offline : OfflineStatus.NotOffline));
        }

        private void UpdateAvailableBulkActions(ListControl bulkActionsSelect)
        {
            bulkActionsSelect.Items.Clear();
            bulkActionsSelect.Items.Add(new ListItem("Bulk Actions...", string.Empty));
            bulkActionsSelect.Items.Add(new ListItem("Create Workbook", "workbook"));

            var offlineStatus = GetOfflineStatusForCurrentFilter();
            if((offlineStatus & OfflineStatus.Offline) > 0)
                bulkActionsSelect.Items.Add(new ListItem("Mark As Online", "online"));
            if((offlineStatus & OfflineStatus.NotOffline) > 0)
                bulkActionsSelect.Items.Add(new ListItem("Mark As Offline", "offline"));
        }


    }
}