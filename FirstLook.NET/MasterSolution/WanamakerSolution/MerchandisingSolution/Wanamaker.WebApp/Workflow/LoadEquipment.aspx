﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoadEquipment.aspx.cs" Theme="None" MasterPageFile="~/Workflow/Workflow.Master" Title="Load Equipment | MAX : Online Inventory. Perfected." Inherits="Wanamaker.WebApp.Workflow.LoadEquipment" %>

<%@ Import Namespace="FirstLook.Merchandising.DomainModel.Vehicles" %>
<%@ Import Namespace="Wanamaker.WebApp.Helpers" %>
<%@ Register Assembly="Firstlook.Merchandising.WebControls" Namespace="FirstLook.Merchandising.WebControls" TagPrefix="fwc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="cwc" TagName="WorkflowSelector" Src="~/Workflow/Controls/WorkflowSelector.ascx" %>
<%@ Register TagPrefix="carbuilder" TagName="InventoryDataView" Src="~/Workflow/Controls/InventoryDataView.ascx" %>
<%@ Register TagPrefix="newVinMapper" TagName="NewVehicleVinDataEditor" Src="~/Workflow/Controls/NewVehicleVinDataEditor.ascx" %>

<asp:Content ID="CssContent" ContentPlaceHolderID="CssPlaceHolder" runat="server">

    <% if(HtmlHelpers.IsDebugBuild()) { %>
        <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Main.debug.css") %>" />
        <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Workflow.debug.css") %>" />
        <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Equipment.debug.css") %>" />
    <% } else { %>
        <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Equipment.Combined.css") %>" />
    <% } %>
    <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/MediaQueries.css") %>" />

</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="BodyPlaceHolder" runat="server">

    <asp:HiddenField ID="InventoryIdHidden" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="BusinessUnitIdHidden" runat="server" ClientIDMode="Static" />

    <asp:ScriptManager ID="ScriptManager2" runat="server"></asp:ScriptManager>

    <cwc:WorkflowSelector ID="wfsInventoryItem" runat="server" EnableViewState="true" ActiveLink="Equipment" 
                          OnInventorySearching="wfhHeader_InventorySearching"></cwc:WorkflowSelector>

    <div id="VehicleSummary" class="clearfix">
        <carbuilder:InventoryDataView ID="invDataViewer" runat="server" InventoryId='<%# InventoryId %>' BusinessUnitId='<%# BusinessUnitId %>' AllowTrimSelection="True" ShowLastAdLink="false" />
    </div>

    <div id="Content">

        <div id="StepCompleted" class='<%= StepComplete ? "complete" : string.Empty %>'>
            <asp:CheckBox ID="StepCompletedCkb" runat="server" ClientIDMode="Static" StepStatusType="ExteriorEquipment" onclick="_gaq.push(['_trackEvent', 'Equipment Page', 'Click', 'Equipment Complete']);" />
            <label for="StepCompletedCkb">Equipment Complete</label>
        </div>

        <asp:Panel ID="WarningPanel" runat="server" Visible="false" CssClass="warning">
            <p id="AdApprovalWarning" runat="server" Visible="false">
                Action Required: Changes have been made to this vehicle. You must Approve the Ad for your changes to appear online.
                <a href="ApprovalSummary.aspx?<%= Request.QueryString %>">Click here to go to Approval page</a>
            </p>
        </asp:Panel>

        <div id="EquipmentContent">

            <asp:Button ID="saveBtn" Text="Save" runat="server" CssClass="button save" OnClick="Save_Clicked" OnClientClick="_gaq.push(['_trackEvent', 'Equipment Page', 'Click', 'Save Page']);" />
                


            <div id="Colors" class="clearfix">
                <h3>Colors</h3>
                <div class="section gray clearfix">
                        
                    <div class="field">
                        <label for="colorSelection">Exterior Color:</label>
                        <asp:DropDownList ID="colorSelection" runat="server" OnDataBound="ExtColor_Bound" DataSourceID="ExtColorsDS" DataTextField="ItemDescription" DataValueField="CodesAndDescriptions" onchange="_gaq.push(['_trackEvent', 'Equipment Page', 'Change', 'Exterior Color']);" />
                        
                        <div id="ColorWidget">

                            <a id="ColorPaletteLink" class="dialogLink" onclick="_gaq.push(['_trackEvent', 'Equipment Page', 'Click', 'Custom Exterior Color']);">Palette/Custom</a>
                        
                            <ul id="extColorDDL" class="colorDropDownList" style="display:none;">
                                <li>Available Colors:</li>
                                <asp:Repeater ID="colorListRptr" runat="server" DataSourceID="ExtColorsDS">
                                    <ItemTemplate>
                                        <li><span style="background: <%# ((ColorPair)Container.DataItem).First.Color %>">&nbsp;&nbsp;&nbsp; </span>&nbsp; <span style="background: <%# ((ColorPair)Container.DataItem).Second.Color %>">
                                            &nbsp;&nbsp;&nbsp; </span>&nbsp; <a href="<%# GetColorSelectJs(((ColorPair)Container.DataItem).GetItemDescription(), ((ColorPair)Container.DataItem).GetPairDescription()) %>">
                                                <%# ((ColorPair)Container.DataItem).GetItemDescription() %></a> </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <li>Custom:
                                    <asp:TextBox ID="CustomColor" runat="server"></asp:TextBox><asp:Button ID="AddColor" runat="server" OnClick="AddColor_Click"
                                        Text="+ Add" />
                                </li>
                            </ul>
                        
                        </div>
                        
                    </div>
                        
                    <div class="field">
                        <label for="intColorSelection">Interior Color:</label>
                        <asp:DropDownList ID="intColorSelection" runat="server" DataSourceID="IntColorsDS" DataTextField="Description" DataValueField="ColorCode" OnDataBound="IntColorBound" onchange="_gaq.push(['_trackEvent', 'Equipment Page', 'Change', 'Interior Color']);"></asp:DropDownList>
                    </div>
                        
                </div>
            </div>

            <div id="KeyInformation" class="clearfix">

                <h3>Key Information</h3>
                <div class="Features section gray clearfix">
                    <asp:CheckBoxList RepeatColumns="4" ID="AdditionalInfoItemsList" runat="server" Width="95%" DataSourceID="AdditionalInfoItems"
                        DataTextField="QuestionText" DataValueField="InfoId" OnDataBound="AdditionalInfoItemsList_DataBound">
                    </asp:CheckBoxList>
                </div>

            </div>

            <div id="VehicleConfiguration" class="clearfix">

                <h3>Vehicle Configuration</h3>
                <div class="VinData section gray clearfix">
                    <newVinMapper:NewVehicleVinDataEditor ID="NewVinDataEditor" runat="server"></newVinMapper:NewVehicleVinDataEditor>
                </div>

                <h3>Exterior</h3>
                <div class="gray">
                    <fwc:EquipmentGroups ID="equipExt" runat="server" DataSourceID="ExtEquipDS" OnItemDataBound="Item_Bound" OnCheckedEquipment="OnCheckedEquipment"
                        CssClass="Features section clearfix" ChildDivCssClass="standardEquipment" ChildDivID="ExtStd" ChildDivTitle="Standard Equipment" />
                </div>

                <h3>Interior</h3>
                <div class="gray">
                    <fwc:EquipmentGroups ID="equipInt" runat="server" DataSourceID="IntEquipDS" OnItemDataBound="Item_Bound" OnCheckedEquipment="OnCheckedEquipment"
                        CssClass="Features section clearfix" ChildDivCssClass="standardEquipment" ChildDivID="IntStd" ChildDivTitle="Standard Equipment" />
                </div>

                <h3>Other</h3>
                <div class="Features section gray clearfix">
                    <fwc:EquipmentGroups ID="lotEquip" runat="server" DataSourceID="lotEquipDS" OnItemDataBound="Item_Bound" OnCheckedEquipment="OnCheckedEquipment"
                        ChildDivCssClass="standardEquipment" ChildDivID="IntStd" ChildDivTitle="Standard Equipment" />
                </div>

                <div class="plain clearfix">
                    <asp:Panel ID="CustomEquipmentForm" runat="server" DefaultButton="stockSearchButton">
                        <asp:Label ID="lb99" runat="server" Text="Enter Description:"></asp:Label>
                        <asp:TextBox ID="EquipmentFinder" runat="server" CssClass="autocompleteTB"></asp:TextBox>
                        <asp:Button ID="stockSearchButton" runat="server" CssClass="button small add" CommandArgument="Exterior" OnClick="EquipmentSearch_Click" Text="+ Add" />
                    </asp:Panel>
                    <ul>
                        <asp:Repeater ID="ExtraEquipment" runat="server">
                            <ItemTemplate>
                                <li>
                                    <asp:Label ID="ExtraEquipmentLabel" Text='<%# Container.DataItem %>' runat="server"></asp:Label>
                                    <asp:ImageButton ID="RemoveEquipmentButton" runat="server" ImageUrl="~/App_Themes/LazyBoy/images/confirm-16x16-cancel.gif"
                                        OnClick="RemoveExtraEquipment" CommandArgument='<%# Container.DataItem %>' /></li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" ServicePath="~/WebServices/InventoryStockService.asmx"
                        ServiceMethod="GetEquipmentCategories" TargetControlID="EquipmentFinder" MinimumPrefixLength="3" CompletionInterval="500"
                        EnableCaching="true" CompletionSetCount="5" CompletionListCssClass="autoComplete" CompletionListItemCssClass="StockSearchFlyoutListItem"
                        CompletionListHighlightedItemCssClass="StockSearchFlyoutListItemSelected" UseContextKey="false">
                    </cc1:AutoCompleteExtender>
                </div>

                <asp:Panel ID="discriminators" CssClass="trimHelperHolder" runat="server" Visible="false">
                    <p>Trim from Appraisals:&nbsp;<asp:Label ID="AppraisalTrim" runat="server"></asp:Label></p>
                    <fwc:TrimHelper ID="discriminator" runat="server" CssClass="trimHelp" EvenRowCssClass="even" OddRowCssClass="odd" EquipmentIncludedImageUrl="App_Themes/LazyBoy/images/checkgreen.gif" />
                </asp:Panel>

            
                <asp:ObjectDataSource ID="IntEquipDS" runat="server" TypeName="FirstLook.Merchandising.DomainModel.DataSource.GenericChoicesDataSource"
                    SelectMethod="Fetch">
                    <SelectParameters>
                        <asp:ControlParameter Name="chromeStyleID" ControlID="invDataViewer" PropertyName="SelectedChromeStyle"  />
                        <asp:Parameter Name="categoryCategory" Type="Int32" DefaultValue="1" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ExtEquipDS" runat="server" TypeName="FirstLook.Merchandising.DomainModel.DataSource.GenericChoicesDataSource"
                    SelectMethod="Fetch">
                    <SelectParameters>
                        <asp:ControlParameter Name="chromeStyleID" ControlID="invDataViewer" PropertyName="SelectedChromeStyle"  />
                        <asp:Parameter Name="categoryCategory" Type="Int32" DefaultValue="2" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="lotEquipDS" runat="server" TypeName="FirstLook.Merchandising.DomainModel.DataSource.OrphanedLotOptionsDataSource"
                    SelectMethod="Fetch" OnSelecting="OnLotEquipmentSelecting">
                    <SelectParameters>
                        <asp:ControlParameter Name="chromeStyleID" ControlID="invDataViewer" PropertyName="SelectedChromeStyle" />
                        <asp:Parameter Name="BusinessUnitId" Type="Int32" />
                        <asp:Parameter Name="InventoryId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:SqlDataSource ID="StickiesDS" runat="server" SelectCommand="SELECT displayDescription from settings.equipmentdisplayrankings 
                                where businessUnitId = @BusinessUnitId
                                AND isSticky = 1" ConnectionString='<%$ ConnectionStrings:Merchandising %>' OnSelecting="Stickies_Selecting">
                    <SelectParameters>
                        <asp:Parameter Name="BusinessUnitId" Type="Int32" DefaultValue="100150" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:ObjectDataSource runat="server" ID="ExtColorsDS" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.AvailableChromeColors"
                    SelectMethod="FetchExterior" OnSelecting="ChromeColorsSelecting">
                    <SelectParameters>
                        <asp:ControlParameter Name="chromeStyleId" Type="Int32" ControlID="invDataViewer" PropertyName="SelectedChromeStyle"  />
                        <asp:Parameter Name="currentExtColorDesc1" Type="String" />
                        <asp:Parameter Name="currentExtColorDesc2" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource runat="server" ID="IntColorsDS" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.AvailableChromeColors"
                    SelectMethod="FetchInterior" OnSelecting="ChromeInteriorColorsSelecting">
                    <SelectParameters>
                        <asp:ControlParameter Name="chromeStyleId" Type="Int32" ControlID="invDataViewer" PropertyName="SelectedChromeStyle" DefaultValue="-1" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="AdditionalInfoItems" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.VehicleConfigurationDataSource"
                    SelectMethod="GetAdditionalInfoItems" OnSelecting="AdditionalInfo_Selecting">
                    <SelectParameters>
                        <asp:Parameter Name="BusinessUnitId" Type="Int32" DefaultValue="100150" />
                    </SelectParameters>
                </asp:ObjectDataSource>

            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="ScriptContent" ContentPlaceHolderID="ScriptsPlaceHolder" runat="server">

    <% if(HtmlHelpers.IsDebugBuild()) { %>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/MAX.Utils.debug.js") %>"></script>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Max/MAX.UI.Global.debug.js") %>"></script>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Workflow/Workflow.debug.js") %>"></script>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Workflow/LoadEquipment.debug.js") %>"></script>
    <% } else { %>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Workflow/LoadEquipment.Combined.js") %>"></script>
    <% } %>

</asp:Content>
