using System;
using System.Collections.Generic;
using System.Web;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using FirstLook.Merchandising.DomainModel.Workflow;

namespace Wanamaker.WebApp.Workflow
{
    public partial class Photos : WorkflowBasePage
    {
        #region Injected dependencies

        public IPhotoServices PhotoService { get; set; }

        #endregion

        public InventoryData InventoryInfo
        {
            get
            {
                return invDataViewer.InventoryInfo;
            }
        }

        public bool StepComplete
        {
            get
            {
                return StepStatusCollection
                           .Fetch(BusinessUnitId, InventoryId)
                           .GetStatus(StepStatusTypes.Photos) == ActivityStatusCodes.Complete;
            }
        }

        public string Vin
        {
            get
            {
                string str = InventoryInfo.VIN;
                if ( string.IsNullOrEmpty( str ) )
                {
                    throw new ApplicationException( "No VIN found for vehicle!" );
                }
                return str;
            }
        }

        protected void Page_Load ( object sender, EventArgs e )
        {
            // Allows the p3p cookies to be set
            // https://incisent.fogbugz.com/default.asp?16674
            HttpContext.Current.Response.AddHeader( "p3p", "CP=\"IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT\"" );

            if ( !IsPostBack )
            {
                if ( InventoryId != 0 )
                {
                    invDataViewer.SetNewVehicle( InventoryId );
                }
                invDataViewer.DataBind();
                StepCompletedCkb.Checked = StepComplete;
            }

            wfsInventoryItem.VehicleIsPreOwned = InventoryInfo.IsPreOwned();
            wfsInventoryItem.HasMarketingUpgrade = HasMarketingUpgrade;

            // Pass relevant data to client
            InventoryIdHidden.Value = InventoryId.ToString();
            BusinessUnitIdHidden.Value = BusinessUnitId.ToString();

            //Set inventory pricing link
            SetPricingLinkForInventory();

        }

        protected void Page_PreRender ( object sender, EventArgs e )
        {
            photosFrame.Attributes[ "src" ] = PhotoService.GetPhotoManagerUrl( BusinessUnitId, Vin, InventoryInfo.StockNumber, PhotoManagerContext.MAX );
        }

        protected void wfhHeader_InventorySearching ( object sender, EventArgs e )
        {
            //Get the matches for the search terms
            var allInventory = InventoryDataFilter.FetchListFromSession(BusinessUnitId);

            Bin bin = Bin.Create( wfsInventoryItem.SearchText );
            List<InventoryData> searchResults = InventoryDataFilter.WorkflowSearch( BusinessUnitId, allInventory, 0, 9999, String.Empty, bin );

            if ( searchResults.Count > 1 || searchResults.Count == 0 )
            {
                //More than 1 match, so send to the home page so they can click the desired vehicle
                //OR no results found
                Response.Redirect( "~/Workflow/Inventory.aspx?search=" + wfsInventoryItem.SearchText );
            }
            else
            {
                FindSearchResult( searchResults[ 0 ].InventoryID );
            }
        }

        protected void FindSearchResult ( int inventoryId )
        {
            var results = wfsInventoryItem.LocateVehicle( inventoryId );
            SetSearchResult( inventoryId, results );
        }

        protected void SetSearchResult ( int inventoryId, LocateVehicleResults result )
        {
            string queryUrl = WorkflowState.CreateUrl( HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath, inventoryId, 
                result.ActiveListType, result.UsedOrNewFilter, result.AgeBucketId );
            Response.Redirect( queryUrl );
        }

        protected void SetPricingLinkForInventory()
        {
            var results = wfsInventoryItem.LocateVehicle(InventoryId);
            invDataViewer.SetPricingLink(results);

        }
    }
}