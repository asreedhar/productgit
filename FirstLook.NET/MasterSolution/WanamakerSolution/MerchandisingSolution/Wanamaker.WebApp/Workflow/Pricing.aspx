﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="Pricing.aspx.cs" Theme="None" MasterPageFile="~/Workflow/Workflow.Master" Title="Pricing | MAX : Online Inventory. Perfected." Inherits="Wanamaker.WebApp.Workflow.Pricing" %>
<%@ Import Namespace="Wanamaker.WebApp.Helpers" %>

<%@ Register TagPrefix="cwc" TagName="WorkflowSelector" Src="~/Workflow/Controls/WorkflowSelector.ascx" %>
<%@ Register TagPrefix="cwc" TagName="PrintWindowSticker" Src="~/Workflow/Controls/PrintWindowSticker.ascx" %>
<%@ Register TagPrefix="carbuilder" TagName="InventoryDataView" Src="~/Workflow/Controls/InventoryDataView.ascx" %>

<asp:Content ID="CssContent" ContentPlaceHolderID="CssPlaceHolder" runat="server">

    <% if(HtmlHelpers.IsDebugBuild()) { %>
        <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Main.debug.css") %>" />
        <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Workflow.debug.css") %>" />
        <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Pricing.debug.css") %>" />
    <% } else { %>
        <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Pricing.Combined.css") %>" />
    <% } %>
    <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/MediaQueries.css") %>" />

</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="BodyPlaceHolder" runat="server">

    <asp:ScriptManager ID="ScriptManager2" runat="server"></asp:ScriptManager>

    <cwc:WorkflowSelector ID="wfsInventoryItem" runat="server" EnableViewState="true" ActiveLink="Pricing"
                          OnInventorySearching="wfhHeader_InventorySearching"></cwc:WorkflowSelector>
          
    <asp:HiddenField ID="InventoryIdHidden" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="BusinessUnitIdHidden" runat="server" ClientIDMode="Static" />

    <div id="Content" class="clearfix">
    
 
        <div id="PricingWorkflow" class="clearfix">
            <h3><span>How to Use This Page</span></h3>

            <asp:Panel ID="PARPricingSteps" runat="server">
                <ul>
                    <li id="Step1"><span>Reprice Your Vehicle</span></li>
                    <li id="Step2"><span>Click SAVE PRICE below</span></li>
                    <li id="Step3"><span><asp:HyperLink ID="GoToNextButton" runat="server">CLICK HERE TO UPDATE AND GO TO NEXT</asp:HyperLink></span></li>
                </ul>
            </asp:Panel>
                
            <asp:Panel ID="NonPARPricingSteps" runat="server" Visible="false">
                <ul>
                    <li id="Step1"><span>Reprice Your Vehicle</span></li>
                    <li id="Step2"><span>Click SAVE PRICE below</span></li>
                    <li id="Step3"><span><asp:HyperLink ID="ApprovalButton" runat="server">CLICK HERE TO UPDATE AND APPROVE</asp:HyperLink></span></li>
                </ul>
            </asp:Panel>


        </div>
        <div id="InventoryAndCompleted">
            <a class="windowStickerToggle dialogLink">Print Window Sticker</a>
            <%--<div id="StepCompleted" class='<%= StepComplete ? "complete" : string.Empty %>'>
                <asp:CheckBox ID="StepCompletedCkb" runat="server" ClientIDMode="Static" StepStatusType="Pricing" />
                <label for="StepCompletedCkb">Pricing Complete</label>
            </div>--%>
        </div>
        
        <asp:MultiView ID="mv" runat="server" ActiveViewIndex="1">
            <asp:View ID="View1" runat="server">
                <div style="padding: 20px">
                    <h4 style="padding: 50px 20px">
                        No pricing information available at this time.
                    </h4>
                </div>
            </asp:View>
            <asp:View ID="View2" runat="server">
                <iframe frameborder="0" id="priceFrame" style="border:0;" runat="server" width="1010px" height="1000px"></iframe>
            </asp:View>
        </asp:MultiView>
        <div id="StickerControls" class="clearfix" style="display:none;">
            <cwc:PrintWindowSticker ID="PrintWindowStickerControl" runat="server" InventoryId='<%# InventoryId %>' BusinessUnitId='<%# BusinessUnitId %>' />
        </div>
        <div id="VehicleSummary">
            <carbuilder:InventoryDataView ID="invDataViewer" runat="server" InventoryId='<%# InventoryId %>'
                BusinessUnitId='<%# BusinessUnitId %>' ShowLastAdLink="false" />
        </div>
    </div>

    <div id="ctr-graph" class="dialog" style="display: none">                
        
        <ul class="carinfo"></ul>

        <script id="ctrGraphHeader" type="text/x-jquery-tmpl">
          <li><span class="yearmakemodel data">${YearMakeModel}</span></li>
          <li>Ext. Color: <span class="data color">${ExteriorColor}</span></li>
          <li>Photos: <span class="data photos">${PhotoCount}</span></li>
          <li>Miles: <span class="data mileage">${Mileage}</span></li>
          <li>Certified: <span class="data cert">{{if Certfied===true}}Yes{{else}}No{{/if}}</span></li>
        </script>

        <div id="chart-line"></div>

        <div class="loading"></div>

    </div>

    <asp:HyperLink ID="CtrGraphLink" CssClass="ctr-graph-link" Text="Graph"  runat="server" href="#" ><img src="/merchandising/Themes/MAX3.0/Images/1343832346_pie-chart_graph.png" width="16" title="CTR graph" border="0" />CTR Graph</asp:HyperLink>
    

</asp:Content>

<asp:Content ID="ScriptContent" ContentPlaceHolderID="ScriptsPlaceHolder" runat="server">

    <% if(HtmlHelpers.IsDebugBuild()) { %>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/ctr-graph.js") %>"></script>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/MAX.Utils.debug.js") %>"></script>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Max/MAX.UI.Global.debug.js") %>"></script>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Workflow/Workflow.debug.js") %>"></script>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Workflow/Pricing.debug.js") %>"></script>
    <% } else { %>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Workflow/Pricing.Combined.js") %>"></script>
    <% } %>

</asp:Content>

