﻿using System;
using System.Web;
using System.Web.Services;
using FirstLook.Common.Core.Command;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Marketing.Commands;
using FirstLook.Merchandising.DomainModel.Marketing.ValueAnalyzer;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Workflow;
using log4net;
using Merchandising.Messages;
using Newtonsoft.Json;
using ApplicationException = Elmah.ApplicationException;

namespace Wanamaker.WebApp.Workflow
{
    public abstract class WorkflowBasePage : BasePage
    {
        #region Logging

        private static readonly ILog Log = LogManager.GetLogger(typeof(Inventory).FullName);

        #endregion

        private Bookout _bookout;
        private Bookout _secondaryBookout;
        private Bookout _KbbConsumerBookout;
        private Bookout _edmundsBookout;
        private DealerAdvertisementPreferences _prefs;
        private Bookout _kbbRetail;
        private Bookout _nadaCleanRetail;


        protected DealerAdvertisementPreferences DealerPreference
        {
            get{return _prefs?? DealerAdvertisementPreferences.Fetch(WorkflowState.BusinessUnitID);}
        }

        protected Bookout Bookout
        {

            get
            {
                return _bookout ?? (_bookout = Bookout.Fetch(WorkflowState.BusinessUnitID, WorkflowState.InventoryId,
                                                             PreferredBookCategory));
            }
        }

        protected Bookout SecondaryBookout
        {

            get
            {
                return _secondaryBookout ?? (_secondaryBookout = Bookout.Fetch(WorkflowState.BusinessUnitID, WorkflowState.InventoryId,
                                                             SecondaryBookCategory));
            }
        }

        protected Bookout EdmundsBookout
        {

            get
            {
                return _edmundsBookout ?? (_edmundsBookout = Bookout.Fetch(WorkflowState.BusinessUnitID, WorkflowState.InventoryId));

            }
        }

        protected Bookout KbbConsumerBookout
        {

            get
            {
                return _KbbConsumerBookout ?? (_KbbConsumerBookout = Bookout.FetchKbbConsumer(WorkflowState.BusinessUnitID, WorkflowState.InventoryId));
            }
        }

        protected Bookout KbbRetail
        {

            get
            {
                return _kbbRetail ?? (_kbbRetail = Bookout.Fetch(WorkflowState.BusinessUnitID, WorkflowState.InventoryId,
                                                             BookoutCategory.KbbRetail));
            }
        }

        protected Bookout NadaCleanRetail
        {

            get
            {
                return _nadaCleanRetail ?? (_nadaCleanRetail = Bookout.Fetch(WorkflowState.BusinessUnitID, WorkflowState.InventoryId,
                                                             BookoutCategory.NadaCleanRetail));
            }
        }

        public bool NeedsBookoutAttention
        {
            get { return !(Bookout.IsAccurate && Bookout.IsValid); }
        }

        public bool NeedsSecondaryBookoutAttention
        {
            get { return !(SecondaryBookout.IsAccurate && SecondaryBookout.IsValid); }
        }

        public bool NeedsKbbConsumerAttention
        {
            get { return !(KbbConsumerBookout.IsAccurate && KbbConsumerBookout.IsValid); }
        }

        public bool NeedsTmvBookoutAttention
        {
            get
            {
                return false;//!(                EdmundsBookout.IsAccurate && EdmundsBookout.IsValid);
            }
        }

        public BookoutCategory PreferredBookCategory
        {
            get
            {
                if (ViewState["bkCat"] != null)
                {
                    return (BookoutCategory)ViewState["bkCat"];
                }

                //DealerAdvertisementPreferences prefs = DealerAdvertisementPreferences.Fetch(WorkflowState.BusinessUnitID);
                ViewState["bkCat"] = DealerPreference.PreferredBookCategory;
                return DealerPreference.PreferredBookCategory;
            }
            set { ViewState["bkCat"] = value; }
        }

        public BookoutCategory SecondaryBookCategory
        {
            get
            {
                if (ViewState["bkSecCat"] != null)
                {
                    return (BookoutCategory)ViewState["bkSecCat"];
                }

                //DealerAdvertisementPreferences prefs = DealerAdvertisementPreferences.Fetch(WorkflowState.BusinessUnitID);
                ViewState["bkSecCat"] = DealerPreference.SecondaryBookCategory;
                return DealerPreference.SecondaryBookCategory;
            }
            set { ViewState["bkSecCat"] = value; }
        }

        public bool EdumudsBook
        {
            get
            {
                if (ViewState["edmundsbook"] != null)
                {
                    return (bool)ViewState["edmundsbook"];
                }

                //DealerAdvertisementPreferences prefs = DealerAdvertisementPreferences.Fetch(WorkflowState.BusinessUnitID);
                ViewState["edmundsbook"] = DealerPreference.IsEdmundsActive;
                return DealerPreference.IsEdmundsActive;
            }
            set { ViewState["edmundsbook"] = value; }
        }

        public bool EdmundsActive
        {
            get
            {
                if (ViewState["edmunds"] != null)
                {
                    return (bool)ViewState["edmunds"];
                }

                //DealerAdvertisementPreferences prefs = DealerAdvertisementPreferences.Fetch(WorkflowState.BusinessUnitID);
                ViewState["edmunds"] = DealerPreference.IsEdmundsActive;
                return DealerPreference.IsEdmundsActive;
            }
            set { ViewState["edmunds"] = value; }
        }

        public bool IsKbbConsumerEnabled
        {
            get
            {
                if (ViewState["kbbconsumer"] != null)
                {
                    return (bool)ViewState["kbbconsumer"];
                }

                //DealerAdvertisementPreferences prefs = DealerAdvertisementPreferences.Fetch(WorkflowState.BusinessUnitID);
                ViewState["kbbconsumer"] = DealerPreference.IsKbbConsumerActive;
                return DealerPreference.IsKbbConsumerActive;
            }
            set { ViewState["kbbconsumer"] = value; }
        }

        public static bool HasMarketingUpgrade
        {
            get
            {
                return WorkflowState.BusinessUnit.HasDealerUpgrade(Upgrade.Marketing);
            }
        }

        private TemplateMediator _mediator;
        public TemplateMediator Mediator
        {
            get
            {
                return _mediator ?? (_mediator = new TemplateMediator(BusinessUnitId, InventoryId, Context.User.Identity.Name));
            }
            set { _mediator = value; }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();

            CheckForInactivity();

        }

        /// <summary>
        /// Updates workflow ActivityStatusCode (completed state).
        /// Consumed by jQuery AJAX request in response to a '[STEP] COMPLETE' checkbox click.
        /// </summary>
        /// <param name="json">JSON object from jQuery AJAX request</param>
        [WebMethod]
        public static string UpdateStepComplete(string json)
        {
            try
            {
                var stepStatus = (WorkflowStepTo)JsonConvert.DeserializeObject(json, typeof(WorkflowStepTo));

                var statuses = StepStatusCollection.Fetch(WorkflowState.BusinessUnitID, WorkflowState.InventoryId);

                if (stepStatus.StepStatusType == StepStatusTypes.ExteriorEquipment ||
                    stepStatus.StepStatusType == StepStatusTypes.InteriorEquipment)
                {
                    // Tie tow equipment statuses to one checkbox
                    statuses
                        .SetStatus(StepStatusTypes.ExteriorEquipment, stepStatus.ActivityStatusCode)
                        .SetStatus(StepStatusTypes.InteriorEquipment, stepStatus.ActivityStatusCode);
                }
                else
                {
                    statuses.SetStatus(stepStatus.StepStatusType, stepStatus.ActivityStatusCode);
                }

                statuses.Save(HttpContext.Current.User.Identity.Name);

                return "{ \"success\":\"Status updated!\" }";
            }
            catch (Exception exception)
            {
                return "{ \"error\":\"" + Uri.EscapeDataString(exception.Message) + "\" }";
            }
        }

        protected void CheckForInactivity()
        {

            // Don't do this if on inventory list page and/or session is new or null
            if (Context.Session == null) return;
            if (!Session.IsNewSession) return;
            if (Page.GetType().ToString() == "ASP.workflow_inventory_aspx") return;


            var szCookieHeader = Request.Headers["Cookie"];
            if ((null != szCookieHeader) && (szCookieHeader.IndexOf("ASP.NET_SessionId") >= 0))
            {
                Response.Redirect("~/Workflow/Inventory.aspx?errorMsg=Returned to Home page due to inactivity.  Please select your vehicle again.");
            }

        }

        protected bool IsPreApproved(int businessUnitId)
        {
            var preApproved = true;
            var command = new GetAutoApproveSettings(businessUnitId);

            AbstractCommand.DoRun(command);

            if (command.Status == AutoApproveStatus.Off)
                preApproved = false;

            return preApproved;
        }
        public string GetMaxForWebsiteUrl(int inventoryId)
        {
            var preference = GetValueAnalyzerPreference(inventoryId);
            return preference.LongUrl;
        }

        public ValueAnalyzerVehiclePreferenceTO GetValueAnalyzerPreference(int inventoryId)
        {
            PricingContext context = PricingContext.FromMerchandising(WorkflowState.BusinessUnitID, inventoryId);

            var command = new GetValueAnalyzerVehiclePreference(context.OwnerHandle, context.VehicleHandle);
            AbstractCommand.DoRun(command);
            return command.Preference;
        }

        public string GetTrimDisplay(string chromeStyleId, string trimFromData, string modelFromData, string afterMarketTrim)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(afterMarketTrim)) return afterMarketTrim;

                int chromeId = int.Parse(chromeStyleId);

                string trimString = trimFromData;

                //replace the trim string with empty string if the trim starts with the model value. Bugzid: 20561
                if (trimString.StartsWith(modelFromData))
                    trimString = trimString.Replace(modelFromData, String.Empty).Trim();

                if (chromeId > 0 && !modelFromData.Contains(trimString))
                {
                    return trimString;
                }

                return string.Empty;
            }
            catch (Exception exception)
            {
                return string.Empty;
            }
        }

        [WebMethod]
        public static string GetBookValue(string json)
        {
            var requestParams = (BookValueRequest)JsonConvert.DeserializeObject(json, typeof(BookValueRequest));

            if (requestParams == null) return "{ \"Error\": \"Error: Could not cast JSON as BookValueRequest.\" }";

            BookoutCategory bookoutCategory = (BookoutCategory)Enum.Parse(typeof(BookoutCategory), requestParams.BookoutCategory.ToString());

            Bookout bookout = null;
            string bookValue = String.Empty;
            bool needsAttention = true;

            try
            {
                bookout = Bookout.Fetch(requestParams.BusinessUnitId, requestParams.InventoryId, bookoutCategory);
                bookValue = string.Format("{0:C0}", bookout.BookValue);
                needsAttention = !(bookout.IsAccurate && bookout.IsValid);

            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("No bookout"))
                {
                    bookValue = String.Empty;
                    needsAttention = true;
                }
                else throw;
            }

            return "{ \"BookValue\": \"" + bookValue + "\", \"NeedsAttention\": " + needsAttention.ToString().ToLower() + " }";
        }

        [WebMethod]
        public static string GetSecondaryBookValue(string json)
        {
            var requestParams = (BookValueRequest)JsonConvert.DeserializeObject(json, typeof(BookValueRequest));

            if (requestParams == null) return "{ \"Error\": \"Error: Could not cast JSON as BookValueRequest.\" }";

            BookoutCategory bookoutCategory = (BookoutCategory)Enum.Parse(typeof(BookoutCategory), requestParams.BookoutCategory.ToString());

            Bookout bookout = null;
            string bookValue = String.Empty;
            bool needsAttention = true;

            try
            {
                bookout = Bookout.Fetch(requestParams.BusinessUnitId, requestParams.InventoryId, bookoutCategory);
                bookValue = string.Format("{0:C0}", bookout.BookValue);
                needsAttention = !(bookout.IsAccurate && bookout.IsValid);

            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("No bookout"))
                {
                    bookValue = String.Empty;
                    needsAttention = true;
                }
                else throw;
            }

            return "{ \"BookValue\": \"" + bookValue + "\", \"NeedsAttention\": " + needsAttention.ToString().ToLower() + " }";
        }

        [WebMethod]
        public static string GetEdmundsBookValue(string json)
        {
            var requestParams = (EdmundsRequest)JsonConvert.DeserializeObject(json, typeof(EdmundsRequest));

            if (requestParams == null) return "{ \"Error\": \"Error: Could not cast JSON as BookValueRequest.\" }";

            Bookout bookout = null;
            string bookValue = String.Empty;
            bool needsAttention = true;

            try
            {
                bookout = Bookout.Fetch(requestParams.BusinessUnitId, requestParams.InventoryId);
                bookValue = string.Format("{0:C0}", bookout.EdmundsValue);
                needsAttention = bookout.EdmundsValue==0;

            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("No bookout"))
                {
                    bookValue = String.Empty;
                    needsAttention = true;
                }
                else throw;
            }

            return "{ \"BookValue\": \"" + bookValue + "\", \"NeedsAttention\": " + needsAttention.ToString().ToLower() + " }";
        }

        [WebMethod]
        public static string GetKbbConsumerValue(string json)
        {
            var requestParams = (KbbConsumerRequest)JsonConvert.DeserializeObject(json, typeof(KbbConsumerRequest));

            if (requestParams == null) return "{ \"Error\": \"Error: Could not cast JSON as BookValueRequest.\" }";

            BookoutCategory bookoutCategory = BookoutCategory.KbbRetail;

            Bookout bookout = null;
            string bookValue = String.Empty;
            bool needsAttention = true;

            try
            {
                bookout = Bookout.FetchKbbConsumer(requestParams.BusinessUnitId, requestParams.InventoryId);
                bookValue = string.Format("{0:C0}", bookout.BookValue);
                needsAttention = !(bookout.IsAccurate && bookout.IsValid);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("No bookout"))
                {
                    bookValue = String.Empty;
                    needsAttention = true;
                }
                else throw;
            }

            return "{ \"BookValue\": \"" + bookValue + "\", \"NeedsAttention\": " + needsAttention.ToString().ToLower() + " }";
        }
        // 'Transfer Object' for ajax book value request
        public class BookValueRequest
        {
            public string BookoutCategory;
            public int BusinessUnitId;
            public int InventoryId;
        }

        // 'Transfer Object' for ajax edmunds book value request
        public class EdmundsRequest
        {
            public bool IsEdmundsActive;
            public int BusinessUnitId;
            public int InventoryId;
        }
        public class KbbConsumerRequest
        {  
            public int BusinessUnitId;
            public int InventoryId;
        }

        public class WorkflowStepTo
        {
            public StepStatusTypes StepStatusType;
            public ActivityStatusCodes ActivityStatusCode;
        }

    }
}