﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.Core;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Oltp;
using FirstLook.Internal;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Dashboard;
using FirstLook.Merchandising.DomainModel.Properties;
using FirstLook.Merchandising.DomainModel.Workflow;
using MAX.Entities.Enumerations;
using Wanamaker.WebApp.AppCode;
using Wanamaker.WebApp.Reports;
using FirstLook.Merchandising.DomainModel.DataAccess;

namespace Wanamaker.WebApp.Workflow
{
    public partial class Workflow : System.Web.UI.MasterPage
    {
        //property injected
        public bool hasPricingSummary = false;
        public bool IsReportViewer;

        public bool IsAdministrator;

        public bool IsMaxStandAlone = WebSettings.IsMaxStandAlone;

        public bool IsMax30;

        public string XUACompatibleContentTag
        {
            get { return XUACompatibleTag.Attributes["content"]; }
            set { XUACompatibleTag.Attributes["content"] = value; }
        }

        public string AssemblyVersion
        {
            get
            {
                return FirstLookAssemblyInfo.Version;
            }
        }

        public string BuCode
        {
            get
            {
                var bu = Context.Items[ "BusinessUnit" ] as BusinessUnit;
                if (bu != null)
                    return bu.BusinessUnitCode;

                return string.Empty;
            }
        }

        protected void Page_Load ( object sender, EventArgs e )
        {
            IsAdministrator = Page.User.IsInRole( "Administrator" );
            IsReportViewer = Page.User.IsInRole( "Report Viewer" ) || IsAdministrator;

            IsMax30 = ( Page as BasePage ) == null ? true : ( Page as BasePage ).IsMax30;

            BusinessUnit bu = Context.Items["BusinessUnit"] as BusinessUnit;

            if(bu!=null)
            hasPricingSummary = DealerPingAccess.CheckDealerPingAccess(bu.Id);

            if ( !Page.IsPostBack )
            {
               
                if (bu != null)
                {
                    DealerName.Text = bu.Name;
                    DealerName.ToolTip = bu.Name;
                    // get settings to control UI
                    GetMiscSettings _getMiscSettings = GetMiscSettings.GetSettings(bu.Id);
                    ViewState["showDashboard"] = _getMiscSettings.ShowDashboard;
                    ViewState["IsOemBranded"] = _getMiscSettings.Franchise != Franchise.None;

                    // needed for postbacks on .aspx objects that don't have viewstate and need this setting
                    // Hide PerformanceSummaryReport FB: 28329
                    liPerformanceSummaryReport.Visible = _getMiscSettings.showOnlineClassifiedOverview;

                    liDashboard.Visible = IsReportViewer && _getMiscSettings.ShowDashboard;
                    liGroupDashboard.Visible = IsReportViewer && _getMiscSettings.ShowGroupLevelDashboard;
                    lnkPricingSummary.Attributes.Add("href", "~/PricingSummary/PriceSummary?BusinessunitId=" + bu.Id);
                }
                else
                {
                    DealerName.Text = "";
                    // get settings to control UI

                    ViewState["showDashboard"] = false;
                    // needed for postbacks on .aspx objects that don't have viewstate and need this setting
                    ViewState["IsOemBranded"] = false;

                    // Hide PerformanceSummaryReport FB: 28329
                    liPerformanceSummaryReport.Visible = false;

                    liDashboard.Visible = false;
                    liGroupDashboard.Visible = false;
                }

                

                // Hide/show menu items
                ReportsMenu.Visible = IsReportViewer;

                

                liVehicleActivityReport.Visible =
                    WhiteListHelper.IsInWhiteList( bu == null ? "" : bu.BusinessUnitCode,
                                                  Settings.Default.VehicleActivityReport_BusinessUnit_Whitelist );

                AdminMenu.Visible = IsAdministrator;
            }

        }

        public bool IsDashboardEnabled
        {
            get { return Convert.ToBoolean(ViewState["showDashboard"]); }

        }

        public bool IsOemBranded { get { return Convert.ToBoolean(ViewState["IsOemBranded"]); } }
    }
}