using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Autofac;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Core.AdvertisementModel.Converters;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.GroupLevelDashboard;
using FirstLook.Merchandising.DomainModel.Marketing.Commands;
using FirstLook.Merchandising.DomainModel.Marketing.ValueAnalyzer;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.VehicleHistory;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using FirstLook.Merchandising.DomainModel.Workflow;
using log4net;
using MAX.BuildRequest;
using Merchandising.Messages;
using Merchandising.Messages.AutoApprove;
using MvcMiniProfiler;
using VehicleDataAccess;
using Wanamaker.WebApp.AppCode.AccessControl;
using Wanamaker.WebApp.Helpers;
using Wanamaker.WebApp.Workflow.Controls;

namespace Wanamaker.WebApp.Workflow
{
    public partial class ApprovalSummary : WorkflowBasePage
    {

        #region Logging

        private static readonly ILog Log = LogManager.GetLogger(typeof(ApprovalSummary).FullName);

        #endregion

        private AdvertisementConverter _advertisementConverter;

        #region Injected dependencies

        public IPhotoServices PhotoService { get; set; }
        public IAdMessageSender MessageSender { get; set; }
        public IQueueFactory QueueFactory { get; set; }
        public ICarfaxServices CarfaxServices { get; set; }
        public IDiscountPricingCampaignManager PricingCampaignManager { get; set; }
        public IVehicleTemplatingFactory VehicleTemplatingFactory { get; set; }
        public IBuildRequestRepository BuildRequestRepository { get; set; }

        #endregion

        private bool IsDirty { get; set; }

        public ApprovalSummary()
        {
            _advertisementConverter = new AdvertisementConverter();
        }

        public BookoutProvider BookProvider
        {
            get
            {
                return Bookout.GetProviderFromMap(PreferredBookCategory);
            }
        }

        public BookoutProvider SecondaryBookProvider
        {
            get
            {
                return SecondaryBookCategory != BookoutCategory.Undefined ? Bookout.GetProviderFromMap(SecondaryBookCategory) : BookoutProvider.Undefined;
            }
        }

        public BookoutProvider TmvBookProvider
        {
            get
            {
                return BookoutProvider.Edmunds;
            }
        }

        public int? DefaultTemplateId
        {
            get
            {
                var vehicleTempData = VehicleTemplatingFactory.GetVehicleTemplatingData(BusinessUnitId, InventoryId, Context.User.Identity.Name);
                return vehicleTempData.Preferences.DefaultTemplateId;
            }
        }

        private string _eStockCardUrl;
        public string EStockCardUrl
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_eStockCardUrl))
                {
                    _eStockCardUrl = Mediator.VehicleData.InventoryItem.GetEStockLink();
                }
                return _eStockCardUrl;
            }
        }

        public bool CertifiedProgramChangedBySystem
        {
            get
            {
                var o = ViewState["CPODirty"];
                return o == null ? false : (bool)o;
            }
            set { ViewState["CPODirty"] = value; }

        }

        public int ChromeStyleID
        {
            get
            {
                object o = ViewState["styleID"];
                return ((o == null) ? -1 : (int)o);
            }
            set { ViewState["styleID"] = value; }
        }

        private VehicleCertification.Manufacturer _manufacturer;
        private VehicleCertification.Manufacturer Manufacturer
        {
            get
            {
                var o = ViewState["ManufacturerID"];
                if (o == null)
                {
                    if (_manufacturer.Id == 0)
                    {
                        try
                        {
                            _manufacturer = VehicleCertification.GetManufacturer(InventoryId);
                        }
                        catch (VehicleCertification.FailedToGetManufacturer) { }
                    }
                    ViewState["ManufacturerID"] = _manufacturer;
                    return _manufacturer;
                }
                return (VehicleCertification.Manufacturer)o;
            }
        }

        public InventoryData InventoryInfo
        {
            get
            {
                return invDataViewer.InventoryInfo;
            }
        }

        public bool StepComplete
        {
            get
            {
                return StepStatusCollection
                           .Fetch(BusinessUnitId, InventoryId)
                           .GetStatus(StepStatusTypes.AdReview) == ActivityStatusCodes.Complete;
            }
        }

        public string Vin
        {
            get
            {
                string str = InventoryInfo.VIN;
                if (string.IsNullOrEmpty(str))
                {
                    throw new ApplicationException("No VIN found for vehicle!");
                }
                return str;
            }
        }

        private bool FromPricingWorkflow
        {
            get
            {
                object o = ViewState["FromPricingWorkflow"];
                if (o != null)
                {
                    return (bool)ViewState["FromPricingWorkflow"];
                }
                return false;
            }
            set
            {
                ViewState["FromPricingWorkflow"] = value;
            }
        }


        private bool AdAndVehicleInSync
        {
            set
            {
                AdAndVehicleInSyncHidden.Value = value.ToString();
            }

        }

        /*PAGE LIFE CYCLE FUNCTIONS */

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //var obj = Registry.BuildUp(new FirstLook.Merchandising.DomainModel.GroupLevelDashboardGroupLevelS3DataCache(Registry.Resolve<IFileStoreFactory>(), true));
            var builder = new ContainerBuilder();
            //Registry.BuildUp((new AsyncServiceRegister()).Register());

            new AsyncServiceRegister().Register(builder);
            //builder.Build();
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            using (MiniProfiler.Current.Step("Page Init"))
            {
                //We don't need the printwindow sticker in the approval summary
                invDataViewer.PrintWindowSticker.Parent.Controls.Remove(invDataViewer.PrintWindowSticker);

                //We don't need the standard equipement on the packages for the approval page. Taking too much time to show
                OptionPackagesControl.RemoveStandardEquipmentDisplay();
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            using (MiniProfiler.Current.Step("Page Load"))
            {
                if (!IsPostBack)
                {
                    FromPricingWorkflow = false;
                    AdAndVehicleInSync = true;

                    if (Request.UrlReferrer != null)
                        FromPricingWorkflow = Regex.IsMatch(Request.UrlReferrer.LocalPath, "/Workflow/Pricing.aspx");

                    if (InventoryId != 0)
                    {
                        invDataViewer.SetNewVehicle(InventoryId);
                    }

                    invDataViewer.DataBind();
                }

                ApprovalAgreement.Text =
                  @"I hereby acknowledge that I have confirmed the content of the 
                                        advertisement(s) I am approving.  I assume responsibility
                                        for the factual correctness of the content, and I approve its submission
                                        to the internet for public distribution.";

                wfsInventoryItem.VehicleIsPreOwned = InventoryInfo.IsPreOwned();
                wfsInventoryItem.HasMarketingUpgrade = HasMarketingUpgrade;

                //next car link for after approval
                string url;
                if (wfsInventoryItem.GetNextUrl(out url))
                    btnApproveSuccessfulYes.NavigateUrl = url;
                else
                    btnApproveSuccessfulYes.Visible = false;

                btnApproveSuccessfulNo.NavigateUrl = WorkflowState.CreateUrl();

                if (!IsPostBack)
                {
                    LoadVehicle();
                }

                StepCompletedCkb.Checked = StepComplete;

                quickPackages1.PackageAdded += OnPackageChanged;
                quickPackages1.PackageDeleted += OnPackageChanged;

                // mark as not dirty
                IsDirty = false;

                // Pass relevant data to client
                InventoryIdHidden.Value = InventoryId.ToString();
                BusinessUnitIdHidden.Value = BusinessUnitId.ToString();
                ManufacturerIdHidden.Value = Manufacturer.Id.ToString();
                ManufacturerNameHidden.Value = Manufacturer.Name;
                VinHidden.Value = Vin;
                PreviewLengthLimit.Value = Mediator.VehicleData.Preferences.DesiredPreviewLength.ToString();

                //Set inventory pricing link
                SetPricingLinkForInventory();
                //Set Async status of AdMessageSender so that sendMessage calls are async
                MessageSender.SendMessageAsync = true;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (MiniProfiler.Current.Step("Page PreRender"))
            {
                MembershipUser user = Context.Items[IdentityHelper.MembershipContextKey] as MembershipUser;
                if (user == null)
                {
                    Response.Redirect("~/Workflow/Inventory.aspx");
                }

                // Setup micro-workflow
                SetupQuickPackages(Mediator.VehicleData.InventoryItem.ChromeStyleId ?? -1);
                SetupBookValues();
                SetupVehicleHistory();
                SetupKeyInformation();
                SetupColors();
                SetupCondition();

                // Disable approve button if price is less than $200
                if (Mediator.VehicleData.InventoryItem.ListPrice > 0 &&
                    Mediator.VehicleData.InventoryItem.ListPrice < 200)
                {
                    approveButton.CssClass = "disabled";
                    approveButton.Enabled = false;

                    ApprovalAgreement.AddClass("red");
                    ApprovalAgreement.Text = @"You must reprice this vehicle before approving the advertisement. (Current Price: "
                                             + string.Format("{0:C}", Mediator.VehicleData.InventoryItem.ListPrice) +
                                             @")";
                }
                else
                {
                    approveButton.CssClass = "";
                    approveButton.Enabled = true;
                    ApprovalAgreement.CssClass = "";
                }

                Master.html.AddClass("ApprovalSummary");
            }
            //For Regen Ad using Bookout
            if (Request.QueryString["regenad"] != null && Request.QueryString["regenad"] == "true")
            {
                RewriteAdButton.AddClass("bold red");
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);

            ClientScript.RegisterForEventValidation(
                  AdModelHidden.UniqueID,
                  "<b>"
               );
            ClientScript.RegisterForEventValidation(
                  AdModelHidden.UniqueID,
                  "</b>"
               );

        }

        private Bookout GetBookout(BookoutProvider provider)
        {
            if (provider == BookoutProvider.KBB)
                return KbbRetail;
            else if (provider == BookoutProvider.NADA)
                return NadaCleanRetail;
            else
                return null;

        }

        private bool NeedBookoutAttention(Bookout bookout)
        {
            return !(bookout.IsAccurate && bookout.IsValid);
        }

        private void SetupQuickPackages(int styleId)
        {

            quickPackages1.ChromeStyleId = styleId;

            DetailedEquipmentCollection tmp = DetailedEquipmentCollection.FetchSelections(BusinessUnitId, InventoryId);
            tmp.ClearStandardHighlights();
            quickPackages1.Equipment = tmp;

            var needsAttention = WorkflowFilters.IsPostableAndNoPackages(InventoryInfo);
            QuickPackages.UpdateClass("attention", needsAttention);
            QuickPackagesHeader.InnerText = needsAttention ? "NO PACKAGES" : "Packages";
        }

        private void SetupBookValues()
        {
            BookoutPrimary.Visible = false;
            BookoutSecondary.Visible = false;
            BookoutTmv.Visible = false;
            KbbConsumer.Visible = false;
            BookValues.Visible = false;
            BookValues.RemoveClass("attention");
            Bookout _bookout=null;
            // If new car, hide section and return
            if (InventoryInfo.InventoryType == 1)
            {
                BookValues.Visible = false;
                return;
            }

            #region PrefferedBookout
            // Bookout.Fetch throws an exception if no value is found
            try
            {
                _bookout = GetBookout(BookProvider);
                if (BookProvider == BookoutProvider.KBB || BookProvider == BookoutProvider.NADA)
                {
                    BookValues.Visible = true;
                    BookoutPrimary.Visible = true;

                    BookProviderLogo.AddClass(BookProvider.ToString().ToLower());
                    BookoutCategoryHidden.Value = PreferredBookCategory.ToString();

                    BookOutLink.Visible = _bookout.BookValue == 0;
                    BookAttentionLink.Visible = NeedBookoutAttention(_bookout);

                    if (_bookout.BookValue == 0 || NeedBookoutAttention(_bookout))
                    {
                        BookValues.AddClass("attention");
                    }

                    if (_bookout.BookValue > 0)
                    {
                        BookValueSpan.InnerText = "$" + _bookout.BookValue.ToString("#,##0");
                        BookValueSpan.Visible = true;
                    }

                }
            }
            catch (Exception exception)
            {
                BookValues.Visible = true;
                BookoutPrimary.Visible = true;
                BookProviderLogo.AddClass(BookProvider.ToString().ToLower());
                   
                BookValues.AddClass("attention");
                BookOutLink.Visible = true;
                BookAttentionLink.Visible = false;

                Log.Info(string.Format("{0} BuId:{1}, InvId:{2}", exception.Message, BusinessUnitId, InventoryId));
            }
            #endregion

            #region SecondaryBookout
            // Bookout.Fetch throws an exception if no value is found
            try
            {

                _bookout = GetBookout(SecondaryBookProvider);
                if (SecondaryBookProvider == BookoutProvider.KBB || SecondaryBookProvider == BookoutProvider.NADA)
                {
                    BookValues.Visible = true;
                    BookoutSecondary.Visible = true;
                    // If new car, hide section and return
                    if (SecondaryBookProvider.ToString().Trim().Length != 0 && !string.IsNullOrEmpty(SecondaryBookCategory.ToString()))
                    {

                        BookSecondaryProviderLogo.AddClass(SecondaryBookProvider.ToString().ToLower());
                        BookoutSecondaryCategoryHidden.Value = SecondaryBookCategory.ToString();

                        BookOutSecondaryLink.Visible = _bookout.BookValue == 0;
                        BookSecondaryAttentionLink.Visible = NeedBookoutAttention(_bookout);

                        if (_bookout.BookValue == 0 || NeedBookoutAttention(_bookout))
                        {
                            BookValues.AddClass("attention");
                        }

                        if (_bookout.BookValue > 0)
                        {
                            BookSecondaryValueSpan.InnerText = "$" + _bookout.BookValue.ToString("#,##0");
                            BookSecondaryValueSpan.Visible = true;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                BookValues.Visible = true;
                BookoutSecondary.Visible = true;
                BookSecondaryProviderLogo.AddClass(SecondaryBookProvider.ToString().ToLower());
                        
                BookValues.AddClass("attention");
                BookOutSecondaryLink.Visible = true;
                BookSecondaryAttentionLink.Visible = false;

                Log.Info(string.Format("{0} BuId:{1}, InvId:{2}", exception.Message, BusinessUnitId, InventoryId));
            }

            

            #endregion

            #region KBBConsumer

            if (BookProvider != BookoutProvider.KBB && SecondaryBookProvider != BookoutProvider.KBB && IsKbbConsumerEnabled)
            {
                BookValues.Visible = true;
                KbbConsumer.Visible = true;
                // If new car, hide section and return

                KbbConsumerLogo.AddClass(BookoutProvider.KBB.ToString().ToLower());
                KbbConsumerHidden.Value = IsKbbConsumerEnabled.ToString();

                // Bookout.Fetch throws an exception if no value is found
                try
                {
                    KbbConsumerBookoutLink.Visible = KbbConsumerBookout.BookValue == 0;
                    KbbConsumerAttentionLink.Visible = NeedsKbbConsumerAttention;

                    if (KbbConsumerBookout.BookValue == 0 || NeedsKbbConsumerAttention)
                    {
                        BookValues.AddClass("attention");
                    }

                    if (KbbConsumerBookout.BookValue > 0)
                    {
                        KbbConsumerValueSpan.InnerText = "$" + KbbConsumerBookout.BookValue.ToString("#,##0");
                        KbbConsumerValueSpan.Visible = true;
                    }
                }
                catch (Exception exception)
                {
                    BookValues.Visible = true;
                    KbbConsumer.Visible = true;
                    KbbConsumerLogo.AddClass(BookoutProvider.KBB.ToString().ToLower());
                    BookValues.AddClass("attention");
                    KbbConsumerBookoutLink.Visible = true;
                    KbbConsumerAttentionLink.Visible = false;

                    Log.Info(string.Format("{0} BuId:{1}, InvId:{2}", exception.Message, BusinessUnitId, InventoryId));
                }

            }

            #endregion

            #region Edmunds

            if (EdmundsActive)
            {
                BookValues.Visible = true;
                BookoutTmv.Visible = true;

                BookTmvProviderLogo.AddClass(TmvBookProvider.ToString().ToLower());
                BookoutTmvCategoryHidden.Value = EdumudsBook.ToString();

                // Bookout.Fetch throws an exception if no value is found
                try
                {  

                    BookOutTmvLink.Visible = EdmundsBookout.EdmundsValue == 0;
                    BookTmvAttentionLink.Visible = EdmundsBookout.EdmundsValue == 0;

                    if (EdmundsBookout.EdmundsValue == 0)
                    {
                        BookValues.AddClass("attention");
                    }

                    if (EdmundsBookout.EdmundsValue > 0)
                    {
                        BookTmvValueSpan.InnerText = "$" + EdmundsBookout.EdmundsValue.ToString("#,##0");
                        BookTmvValueSpan.Visible = true;
                    }
                }
                catch (Exception exception)
                {
                    BookValues.Visible = true;
                    BookoutTmv.Visible = true;
                    BookTmvProviderLogo.AddClass(TmvBookProvider.ToString().ToLower());
                    BookValues.AddClass("attention");
                    BookOutTmvLink.Visible = true;
                    BookTmvAttentionLink.Visible = false;

                    Log.Info(string.Format("{0} BuId:{1}, InvId:{2}", exception.Message, BusinessUnitId, InventoryId));
                }
            }

            #endregion

            BookValuesHeader.InnerText = BookValues.HasClass("attention") ? "NO BOOK VALUE" : "Book Value";
        }

        private void SetupVehicleHistory()
        {
            // Hide vehicle history under certain situations
            if (!ShouldShowVehicleHistory())
            {
                VehicleHistory.Visible = false;
                return;
            }

            VehicleHistoryReportControl.InventoryInfo = InventoryInfo;
            VehicleHistoryReportControl.EStockLink = EStockCardUrl;
            VehicleHistoryReportControl.LoadData();
            var needsAttention = !VehicleHistoryReportControl.IsComplete;
            VehicleHistory.UpdateClass("attention", needsAttention);
            VehicleHistoryReportHeader.InnerText = needsAttention ? "NO CARFAX" : "Carfax";
        }

        private bool ShouldShowVehicleHistory()
        {
            return InventoryInfo != null
                && InventoryInfo.IsPreOwned()
                && CarfaxServices.HasAccount(BusinessUnitId);
        }

        private void SetupKeyInformation()
        {
            var items = AdditionalInfoItemsList.Items.Cast<ListItem>().ToArray();
            var needsAttention = items.Any() && !items.Any(t => t.Selected);
            KeyInformation.UpdateClass("attention", needsAttention);
            KeyInformationHeader.InnerText = needsAttention ? "NO KEY INFORMATION" : "Key Information";
        }

        private void SetupColors()
        {
            var needsAttention = ExteriorColorDropDown.SelectedValue == string.Empty || InteriorColorDropDown.SelectedValue == string.Empty;
            DropDownLists.UpdateClass("attention", needsAttention);
            ColorsHeader.InnerText = needsAttention ? "NO COLORS" : "Colors";
        }

        private void SetupCondition()
        {
            var needsAttention = ConditionDropDown.SelectedValue == string.Empty;
            DropDownLists2.UpdateClass("attention", needsAttention);
            ConditionHeader.InnerText = needsAttention ? "NO CONDITION" : "Condition";
        }

        protected bool GetMarketingVisibility()
        {
            return HasMarketingUpgrade && InventoryInfo.InventoryType == 2;
        }

        protected static string GetImageOpenerJs(object evalObj)
        {
            string url = evalObj as string;
            url = Regex.Replace(url, @"'", @"\'"); //escape '
            return string.Format("javascript:openindex('{0}')", url);
        }

        protected void ChooseDesc_Click(object sender, EventArgs e)
        {
            Button but = sender as Button;

            string choice = "IMM";
            if (but != null)
            {
                choice = but.CommandArgument;
            }

            MerchandisingDescription desc = Mediator.GetOrCreateMerchandisingDescription();

            //use choice to select one of the descriptions
            if (choice != "IMM")
            {
                desc.Description = IAADesc.Text;
                desc.Footer = IAAFoot.Text;
                desc.IsLongForm = false;
                desc.IsAutoPilot = false;
                desc.LastUpdatedOn = DateTime.Parse(IaaEdit.Text);
                desc.TemplateId = null;
                desc.ThemeId = null;
                desc.AdvertisementModel = _advertisementConverter.FromText(IAADesc.Text);
            }
            else
            {
                var advertisementModel = _advertisementConverter.FromJson(AdModelHidden.Value);

                desc.Description = _advertisementConverter.ToText(advertisementModel);
                desc.Footer = IMMFoot.Text;
                desc.AdvertisementModel = advertisementModel;
            }

            desc.AdvertisementId = string.IsNullOrEmpty(iaaAdId.Value) ? 1 : Int32.Parse(iaaAdId.Value);

            ChoicePanel.Visible = false;
            BindControlsFromDescription(desc);

            //we save the add to persist the advertisementID
            desc.Save(BusinessUnitId, InventoryId, User.Identity.Name);
        }

        private void BindControlsFromDescription(MerchandisingDescription desc)
        {
            var advertisementModel = desc.AdvertisementModel;

            AdModelHidden.Value = _advertisementConverter.ToJson(advertisementModel);
            OptimalFooterLiteral.Text = desc.Footer;
            AdFooterPanel.Visible = desc.Footer.Trim().Length > 0;

            if (desc.LastUpdatedOn.HasValue)
            {
                CreatedOn.Text = desc.LastUpdatedOn.Value.ToShortDateString();
                if (DateTime.Today.Subtract(desc.LastUpdatedOn.Value).Days > 7)
                {
                    CreatedOn.AddClass("alert");
                }
            }
            else
            {
                CreatedOn.Text = DateTime.Now.ToShortDateString();
            }


            ExpiresOn.Text = desc.ExpiresOn.HasValue ? desc.ExpiresOn.Value.ToShortDateString() : string.Empty;

            var autoApproveSettings = GetAutoApproveSettings.GetSettings(BusinessUnitId);
            var hasAutoApprove = autoApproveSettings.HasSettings && autoApproveSettings.Status != AutoApproveStatus.Off;

            if (WorkflowFilters.HasOutdatedPrice(desc.LastUpdateListPrice, Mediator.VehicleData.InventoryItem.ListPrice) && !hasAutoApprove)
            {
                invDataViewer.MakeListPriceRed();
                warning.Visible = true;
                descPrice.Text = String.Format("{0:c0}", desc.LastUpdateListPrice ?? 0m);
                RewriteAdButton.AddClass("bold red");
            }
            else
            {
                invDataViewer.ClearListPriceColor();
                RewriteAdButton.RemoveClass("bold red");
                warning.Visible = false;
                descPrice.Text = "";
            }


            if (desc.TemplateId.HasValue && FrameworkSelectorDDL.Items.FindByValue(desc.TemplateId.Value.ToString()) != null)
            {
                FrameworkSelectorDDL.SelectedValue = desc.TemplateId.Value.ToString();
            }
            else
            {
                FrameworkSelectorDDL.DataBind();
            }

            //autoPilot.Checked = desc.IsAutoPilot;
        }

        private MerchandisingDescription CreateDescriptionFromControls()
        {
            MerchandisingDescription desc = MerchandisingDescription.Fetch(BusinessUnitId, InventoryId);

            var advertisementModel = _advertisementConverter.FromJson(AdModelHidden.Value);

            desc.Description = _advertisementConverter.ToText(advertisementModel);
            desc.Footer = OptimalFooterLiteral.Text;
            desc.AdvertisementModel = advertisementModel;

            DateTime expiry; //DateTime expiry = DateTime.MaxValue;
            if (!string.IsNullOrEmpty(ExpiresOn.Text) && DateTime.TryParse(ExpiresOn.Text, out expiry))
            {
                desc.ExpiresOn = expiry;
                desc.AddExpiresDisclaimer();
            }
            else
            {
                desc.ExpiresOn = null;
                ExpiresOn.Text = string.Empty;
                desc.RemoveExpiresDisclaimer();
            }
            //reset the footer based on whatever changed here...
            OptimalFooterLiteral.Text = desc.Footer;


            decimal tmpPrice;
            if (decimal.TryParse(invDataViewer.ListPriceText, out tmpPrice))
            {
                desc.LastUpdateListPrice = tmpPrice;
            }


            return desc;
        }

        private void LoadVehicle()
        {
            try
            {

                MerchandisingDescription descrip;
                if (invDataViewer.ChromeValueSet && FromPricingWorkflow)
                    descrip = Mediator.GenerateDescription();
                else
                    descrip = Mediator.GetOrCreateMerchandisingDescription(invDataViewer.ChromeValueSet);


                PreviousAdModelHidden.Value = _advertisementConverter.ToJson(descrip.OldAdvertisementModel);

                //Get the Accelerator description
                int? accelId = MerchandisingDescription.FetchLatestAcceleratorAdvertisementId(BusinessUnitId, InventoryId);

                bool descriptionChoiceHandled = false;

                if (accelId.HasValue //an I2A exists                
                    && (!descrip.AdvertisementId.HasValue || //AND either we don't have an adId OR
                        accelId.Value != descrip.AdvertisementId.Value))
                //the ids do not match (we only get IDs from them, so they are always >= us
                {
                    try
                    {
                        //present options to user
                        MerchandisingDescription accelDescription =
                            MerchandisingDescription.FetchAcceleratorDescription(BusinessUnitId, InventoryId);
                        IAADesc.Text = accelDescription.Description;
                        IAAFoot.Text = accelDescription.Footer;
                        IMMDesc.Text = descrip.Description;
                        IMMFoot.Text = descrip.Footer;
                        IaaEdit.Text = accelDescription.BasicProperties.Modified.ToShortDateString() + @" " +
                                       accelDescription.BasicProperties.Modified.ToShortTimeString();
                        if (descrip.LastUpdatedOn.HasValue)
                        {
                            ImmEdit.Text = descrip.LastUpdatedOn.Value.ToShortDateString() + " " +
                                           descrip.LastUpdatedOn.Value.ToShortTimeString();
                        }
                        else
                        {
                            ImmEdit.Text = @"now";
                        }

                        AdModelHidden.Value = _advertisementConverter.ToJson(descrip.AdvertisementModel);
                        iaaAdId.Value = accelId.Value.ToString();
                        ChoicePanel.Visible = true;
                        descriptionChoiceHandled = true;
                    }
                    catch (Exception exception)
                    {
                        Log.Error("previously ignored exception", exception);
                    }
                }

                if (!descriptionChoiceHandled)
                {
                    BindControlsFromDescription(descrip);
                    ChoicePanel.Visible = false;
                }

                BookOutLink.NavigateUrl = EStockCardUrl;
                BookAttentionLink.Attributes["href"] = EStockCardUrl;
                BookOutSecondaryLink.NavigateUrl = EStockCardUrl;
                BookSecondaryAttentionLink.Attributes["href"] = EStockCardUrl;
                BookOutTmvLink.NavigateUrl = EStockCardUrl;
                BookTmvAttentionLink.Attributes["href"] = EStockCardUrl;
                KbbConsumerBookoutLink.NavigateUrl = EStockCardUrl;
                KbbConsumerAttentionLink.Attributes["href"] = EStockCardUrl;
                //check the database for the currently saved style for this vehicle
                ChromeStyleID = Mediator.VehicleData.VehicleConfig.ChromeStyleID;

                //BIND STORYBOARD AND IMAGES

                OptionsRepeater.DataBind();

                //BIND the lot provider data
                LoadLotData();

                // Bind micro-workflow controls
                AdditionalInfoItemsList.DataBind();
                ExteriorColorDropDown.DataBind();
                InteriorColorDropDown.DataBind();
                ConditionDropDown.DataBind();

            }
            catch (InventoryException ex)
            {
                Response.Redirect("~/Workflow/Inventory.aspx?errorMsg=" + ex.Message);
            }
        }

        //binds the data pulled from the lot provider
        private void LoadLotData()
        {
            FormView fv = ops.FindControl("LotDataFV") as FormView;
            if (fv != null)
                fv.DataBind();

            CheckBoxList cbl = ops.FindControl("ListingOpts") as CheckBoxList;
            if (cbl != null) cbl.DataBind();
        }

        protected void Mapped_Bound(object sender, EventArgs e)
        {
            CheckBoxList cbl = ops.FindControl("ListingOpts") as CheckBoxList;

            if (cbl != null && cbl.Items.Count <= 0)
            {
                cbl.Items.Add("No Lot Provider Equipment Info Found");
            }
        }


        protected void ConditionAdjectives_Bound(object sender, EventArgs e)
        {
            ConditionDropDown.Items.Insert(0, new ListItem("Select condition...", ""));

            string condition = Mediator.VehicleData.VehicleConfig.VehicleCondition;
            if (!string.IsNullOrEmpty(condition))
            {
                if (ConditionDropDown.Items.FindByValue(condition) != null)
                {
                    ConditionDropDown.SelectedValue = condition;
                    return;
                }

                condition = condition.ToLower();
                foreach (ListItem listItem in ConditionDropDown.Items)
                {
                    string item = listItem.Text.ToLower();
                    if (item.Contains(condition) || condition.Contains(item))
                    {
                        ConditionDropDown.SelectedValue = listItem.Value;
                        return;
                    }
                }
            }

            ConditionDropDown.SelectedIndex = 0;
        }

        protected void ExtColor_Bound(object sender, EventArgs e)
        {
            var ddl = sender as DropDownList;
            if (ddl == null) return;

            var choose = "Choose a color...";
            if (ddl.Items.Count > 0 && ddl.Items[0].Text != choose)
            {
                ddl.Items.Insert(0, new ListItem(choose, ""));
            }

            //We need to fetch a new vehicle configuration in order to get new color from autoload.
            var vehConfig = VehicleConfiguration.FetchOrCreateDetailed(BusinessUnitId, InventoryId, User.Identity.Name);

            var first = new ChromeColor(vehConfig.ExteriorColor1, string.Empty, vehConfig.ExteriorColorCode, string.Empty);
            var second = new ChromeColor(vehConfig.ExteriorColor2, string.Empty, vehConfig.ExteriorColorCode2, string.Empty);
            var pair = new ColorPair(first, second);

            var selectedValue = SelectExteriorColor(ddl, pair);

            if (string.IsNullOrWhiteSpace(selectedValue) && ddl.Items.Count > 0)
            {
                ddl.SelectedIndex = 0;
            }
        }

        protected void IntColorBound(object sender, EventArgs e)
        {
            var ddl = sender as DropDownList;
            if (ddl == null) return;

            // choose a color
            var choose = "Choose a color...";
            if (ddl.Items.Count > 0 && ddl.Items[0].Text != choose)
            {
                ddl.Items.Insert(0, new ListItem(choose, ""));
            }

            //We need to fetch a new vehicle configuration in order to get new color from autoload.
            var vehConfig = VehicleConfiguration.FetchOrCreateDetailed(BusinessUnitId, InventoryId, User.Identity.Name);
            var color = vehConfig.InteriorColor;
            var code = vehConfig.InteriorColorCode;

            var filter = new InteriorColorDropdownFilter();

            // Find matching color
            ListItem match = filter.FindInteriorColor(ddl, code, color);

            // remove duplicate descriptions from the list, keeping the vehicle's color/code.
            filter.RemoveDuplicateColors(match, color, code, ddl);

            // select the match, if one was found
            if (match != null)
            {
                if (ddl.Items.Cast<ListItem>().Any(i => i.Value == match.Value))
                {
                    ddl.Items.Cast<ListItem>().First(i => i.Value == match.Value).Selected = true;
                    return;
                }
            }

            // If we couldn't match anything ...
            var selected = ddl.Items.Cast<ListItem>().Where(i => i.Selected);
            if (!selected.Any() && ddl.Items.Count > 0)
            {
                ddl.SelectedIndex = 0;
            }
        }




        private static string SelectExteriorColor(ListControl ddl, ColorPair pair)
        {
            if (!string.IsNullOrEmpty(pair.PairCode))
            {
                foreach (ListItem listItem in ddl.Items)
                {
                    var itemPair = ColorPair.Parse(listItem.Value);

                    if (itemPair.PairCode != pair.PairCode) continue;

                    ddl.SelectedValue = listItem.Value;
                    return ddl.SelectedValue;
                }
            }

            foreach (ListItem listItem in ddl.Items)
            {
                var itemPair = ColorPair.Parse(listItem.Value);

                if (itemPair.PairDescription.ToLower() != pair.PairDescription.ToLower()) continue;

                ddl.SelectedValue = listItem.Value;
                return ddl.SelectedValue;
            }


            foreach (ListItem listItem in ddl.Items)
            {
                var itemPair = ColorPair.Parse(listItem.Value);

                if (itemPair.PairDescription.ToLower().Replace(" ", "") != pair.PairDescription.ToLower().Replace(" ", "")) continue;

                ddl.SelectedValue = listItem.Value;
                return ddl.SelectedValue;
            }

            return string.Empty;
        }

        protected void Remap_Click(object sender, EventArgs e)
        {
            var vc = Mediator.VehicleData.VehicleConfig;
            vc.VehicleOptions.LoadMappedEquipment(BusinessUnitId, InventoryId, true, vc.ChromeStyleID);
            vc.Save(BusinessUnitId, User.Identity.Name);
        }

        protected void Conditions_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@BusinessUnitId"].Value = BusinessUnitId;
        }

        protected void StylePieces_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["Vin"] = Vin;
        }

        protected void ChromeColorsSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["chromeStyleId"] = invDataViewer.SelectedChromeStyle;

            if (
                string.IsNullOrEmpty(e.InputParameters["chromeStyleId"].ToString())
                || e.InputParameters["chromeStyleId"].ToString() == "-1")
            {
                e.Cancel = true;
                return;
            }
            VehicleConfiguration vehConfig = VehicleConfiguration.FetchOrCreateDetailed(BusinessUnitId, InventoryId, User.Identity.Name);
            //VehicleConfiguration vehConfig = Mediator.VehicleData.VehicleConfig;
            e.InputParameters["currentExtColorDesc1"] = vehConfig.ExteriorColor1;
            e.InputParameters["currentExtColorDesc2"] = vehConfig.ExteriorColor2;
        }
        protected void ChromeInteriorColorsSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["chromeStyleId"] = invDataViewer.SelectedChromeStyle;

            if (
                string.IsNullOrEmpty(e.InputParameters["chromeStyleId"].ToString())
                || e.InputParameters["chromeStyleId"].ToString() == "-1")
            {
                e.Cancel = true;
                return;
            }
        }


        private int? ChooseTemplateOver15()
        {
            string value = (from ListItem item in FrameworkSelectorDDL.Items
                            where Regex.IsMatch(item.Text, "Luxury/Key Features Enhanced\\+", RegexOptions.IgnoreCase)
                            select item.Value).FirstOrDefault() ?? (from ListItem item in FrameworkSelectorDDL.Items
                                                                    where Regex.IsMatch(item.Text, "feature", RegexOptions.IgnoreCase)
                                                                    select item.Value).FirstOrDefault();

            if (value == null)
            {
                return new int?();
            }

            return int.Parse(value);
        }

        private int? ChooseTemplateUnder15()
        {
            string value = (from ListItem item in FrameworkSelectorDDL.Items
                            where Regex.IsMatch(item.Text, "Aggressive Pricing Enhanced\\+", RegexOptions.IgnoreCase)
                            select item.Value).FirstOrDefault() ?? (from ListItem item in FrameworkSelectorDDL.Items
                                                                    where Regex.IsMatch(item.Text, "pric", RegexOptions.IgnoreCase)
                                                                    select item.Value).FirstOrDefault();

            if (value == null)
            {
                return new int?();
            }

            return int.Parse(value);
        }

        protected void TemplateDDL_Bound(object sender, EventArgs e)
        {
            int? templateID;

            if (Mediator.TemplateId != Constants.DEFAULT_TEMPLATE_ID)
            {
                MerchandisingDescription desc = MerchandisingDescription.Fetch(BusinessUnitId, InventoryId);
                if (desc != null && desc.AdvertisementModel != null)
                    templateID = desc.AdvertisementModel.AdTemplateID;
                else
                    templateID = Mediator.TemplateId;
            }
            else /*haven't saved template and no default*/
            {
                templateID = Mediator.VehicleData.InventoryItem.ListPrice >= 15000 ? ChooseTemplateOver15() : ChooseTemplateUnder15();
            }


            if (templateID.HasValue && FrameworkSelectorDDL.Items.FindByValue(templateID.ToString()) != null)
            {
                FrameworkSelectorDDL.SelectedValue = templateID.ToString();
            }
            else
            {
                var settings = AdTemplateSettings.FetchDefaultTemplateSettings(BusinessUnitId, invDataViewer.SelectedChromeStyle, InventoryInfo);
                if (settings.TemplateId.HasValue && FrameworkSelectorDDL.Items.FindByValue(settings.TemplateId.Value.ToString()) != null)
                {
                    FrameworkSelectorDDL.SelectedValue = settings.TemplateId.ToString();
                }
            }

        }
        protected void Templates_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["businessUnitId"] = BusinessUnitId;
            e.InputParameters["inventoryData"] = Mediator.VehicleData.InventoryItem;
        }

        protected void Pricing_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["businessUnitId"] = BusinessUnitId;
            e.InputParameters["inventoryId"] = InventoryId;
        }

        protected void AdditionalInfo_Bound(object sender, EventArgs e)
        {
            int selectedCount = 0;

            foreach (ListItem li in AdditionalInfoItemsList.Items)
            {
                //split out the id
                int itemid = AdditionalVehicleInfo.GetId(li.Value);
                li.Selected = Mediator.VehicleData.VehicleConfig.KeyInformationList.Exists(
                    info => info.KeyInformationId == itemid);

                if (li.Selected) selectedCount++;

                // Appends Google Analytics click handler
                try
                {
                    li.Attributes.Add("onclick", "_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Key Information Item']);");
                }
                catch (Exception exception)
                {
                    Log.Error("Failed to append google analytics to Key Information checkbox", exception);
                }

            }

        }

        public bool ShowItem(object itemText)
        {
            string item = itemText as string;
            if (item == null) return false;

            return Mediator.VehicleData.VehicleConfig.AdditionalInfoText.Contains(item);
        }

        protected IEnumerable<CategoryCollection> GetOptionsDs()
        {
            using (MiniProfiler.Current.Step("Home.GetOptionsDs"))
            {
                Mediator.VehicleData.VehicleConfig.OrderAndFilterEquipment(BusinessUnitId,
                                                                           Mediator.VehicleData.Preferences.
                                                                               MaxOptionsByTier);
                List<CategoryCollection> catList =
                    Mediator.VehicleData.VehicleConfig.VehicleOptions.GetCategoryCollections();
                catList.Sort(new CategoryCollectionListLengthOrderer());
                return catList;
            }
        }

        protected bool ScheduleStockItem(DateTime release)
        {
            // Getting these destinations back from the UI is ridiculous!
            List<int> destinations = (from edtCb in
                                          (from RepeaterItem item in EdtRptr.Items select item.FindControl("itemCB")).OfType<CheckBoxPlus>()
                                      where edtCb.Checked
                                      select Int32.Parse(edtCb.CheckBoxArgument)).ToList();

            MerchandisingDescription desc = CreateDescriptionFromControls();

            int? tempId = null;
            if (!string.IsNullOrEmpty(FrameworkSelectorDDL.SelectedValue))
            {
                tempId = int.Parse(FrameworkSelectorDDL.SelectedValue);
            }

            desc.TemplateId = tempId;

            //save the description
            string user = Context.User.Identity.Name;
            desc.Save(BusinessUnitId, InventoryId, user);


            // Find out if there are any active discounts for this inventory
            int? manufacturerRebate = null;
            int? dealerDiscount = null;

            var discountCampaign = PricingCampaignManager.FetchCampaignForInventory(BusinessUnitId, InventoryId);

            // Only negative MSRP discounts need to have the rebate and incentive fields set.
            if (discountCampaign != null && discountCampaign.DiscountType == PriceBaseLine.Msrp)
            {
                manufacturerRebate = (discountCampaign.ManufacturerRebate < 0) ? Convert.ToInt32(discountCampaign.ManufacturerRebate) : (int?)null;
                dealerDiscount = (discountCampaign.DealerDiscount < 0) ? Convert.ToInt32(discountCampaign.DealerDiscount) : (int?)null;
            }

            // Approve it!
            Log.InfoFormat("Ad approved for release. BusinessUnitId:{0}, InventoryId:{1}", BusinessUnitId, InventoryId);
            ApproveForRelease.Run(
                                            BusinessUnitId, InventoryId,
                                            release, user,
                                            desc.Description, desc.Footer,
                                            Mediator.VehicleData.VehicleConfig.VehicleOptions.GetOrderedDescriptionText(BusinessUnitId),
                                            Mediator.VehicleData.InventoryItem.FlListPrice,
                                            Mediator.VehicleData.InventoryItem.SpecialPrice,
                                            desc.ExpiresOn,
                                            Mediator.VehicleData.VehicleConfig.GetExteriorColorDescription(),
                                            Mediator.VehicleData.VehicleConfig.InteriorColor,
                                            Mediator.VehicleData.VehicleConfig.InteriorType,
                                            AutoRevoMapper.Instance().ExecuteMapping(BusinessUnitId, Mediator.VehicleData),
                                            Mediator.VehicleData.VehicleConfig.VehicleOptions.options.GetOptionCodes(","),
                                            false,
                                            Convert.ToInt32(Mediator.VehicleData.InventoryItem.MSRP),
                                            manufacturerRebate,
                                            dealerDiscount
                                           );


            MessageSender.SendApproval(new ApproveMessage(BusinessUnitId, InventoryId, user), GetType().FullName);

            return true;
        }

        protected void MappedOpts_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@BusinessUnitId"].Value = BusinessUnitId;

            if (InventoryId < 0)
            {
                e.Cancel = true;
            }

            e.Command.Parameters["@InventoryId"].Value = InventoryId;
        }

        protected void EdtPostings_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@BusinessUnitId"].Value = BusinessUnitId;
            e.Command.Parameters["@InventoryId"].Value = InventoryId;
        }

        protected void EdtItem_DataBound(object sender, RepeaterItemEventArgs e)
        {
            Label lb = e.Item.FindControl("dateLbl") as Label;
            if (lb != null)
            {
                if (String.IsNullOrEmpty(lb.Text))
                {
                    lb.Text = @"--";
                }
            }

            Label lb2 = e.Item.FindControl("priceLbl") as Label;
            if (lb2 != null)
            {
                if (String.IsNullOrEmpty(lb2.Text))
                {
                    lb2.Text = @"--";
                }
            }
        }

        protected void EdtList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["businessUnitID"] = BusinessUnitId;
        }

        protected void AdditionalInfo_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["BusinessUnitId"] = BusinessUnitId;
        }

        protected void ApproveVehicle_Click(object sender, EventArgs e)
        {
            if (IsDirty)
            {
                UpdateVehicle();
            }

            AdAndVehicleInSync = true;
            DateTime release = cal1.SelectedDate;

            // Mark Ad Review as completed
            var statuses = StepStatusCollection.Fetch(WorkflowState.BusinessUnitID, WorkflowState.InventoryId);
            statuses.SetStatus(StepStatusTypes.AdReview, ActivityStatusCodes.Complete);
            statuses.Save(HttpContext.Current.User.Identity.Name);

            if (ScheduleStockItem(release))
            {
                Page.MaintainScrollPositionOnPostBack = false;
                ApprovalModalPanel.Visible = true;
                btnApproveSuccessfulYes.Focus();
            }
        }

        protected void Specials_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@OwnerEntityId"].Value = BusinessUnitId;
        }

        protected void SpecialDDL_Bound(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;
            if (ddl == null) return;

            ddl.Items.Insert(0, new ListItem("Auto Select Financing...", FinancingSpecial.AutomatedFinancingSpecialSelectionId.ToString()));
            ddl.Items.Insert(1, new ListItem("Do Not Show Special", FinancingSpecial.DoNotUseFinancingSpecialId.ToString()));

            ListItem theOne = SpecialDDL.Items.FindByValue(Mediator.VehicleData.VehicleConfig.SpecialID.ToString());
            if (theOne != null)
            {
                SpecialDDL.SelectedValue = theOne.Value;
            }
        }

        protected void Special_Changed(object sender, EventArgs e)
        {
            Mediator.VehicleData.SetFinancingSpecial(int.Parse(SpecialDDL.SelectedValue), Context.User.Identity.Name);

        }

        protected static string NullProof(object evalObj)
        {
            string obj = evalObj as string;
            if (string.IsNullOrEmpty(obj))
            {
                return "--";
            }
            return (string)evalObj;
        }

        private int MaxThemeSentenceCount
        {
            get
            {
                object o = ViewState["mtsc"];
                return (o == null) ? 2 : (int)o;
            }
        }


        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            if (args is TrimChangedEventArgs)
            {
                HandleTrimChangedEvent((args as TrimChangedEventArgs).StyleId);
                return true;
            }

            if (args is CertificationAutoSelected)
            {
                IsDirty = true;
                CertifiedProgramChangedBySystem = true;
                return true;
            }

            if (args is CertificationStateChanged)
            {
                IsDirty = true;
            }

            return false;
        }


        private void HandleTrimChangedEvent(int styleId)
        {
            var vc = Mediator.VehicleData.VehicleConfig;

            if (vc.ChromeStyleID <= 0)
            {
                Mediator.ResetDefaultTemplateSettings(styleId);
                if (FrameworkSelectorDDL.Items.FindByValue(Mediator.TemplateId.ToString()) != null)
                {
                    FrameworkSelectorDDL.SelectedValue = Mediator.TemplateId.ToString();
                }
            }

            AdModelHidden.Value = _advertisementConverter.ToJson(_advertisementConverter.EmptyModel);

            if (invDataViewer.VehicleConfiguration.ChromeStyleID != vc.ChromeStyleID)
            {
                Mediator.VehicleData.VehicleConfig = invDataViewer.VehicleConfiguration;

                SetupQuickPackages(invDataViewer.VehicleConfiguration.ChromeStyleID);
                quickPackages1.RemoveAllPackages();

                OptionPackagesControl.DataBindPackages();
            }

            ExteriorColorDropDown.DataBind();
            InteriorColorDropDown.DataBind();

            ResetConfigurationControls();
            UpdateVehicle();

            FrameworkSelectorDDL.DataBind();

            btn_RewriteFormat_OnClick(this, new EventArgs());
        }



        private void ResetConfigurationControls()
        {
            ConditionDropDown.SelectedIndex = 0;

            //This should unselect the drop down.
            //Setting to 0 causes an exception because the Databinding happens 
            //after this method is called and throws an argumentoutofrangeexception in some edge cases,
            //where there are no colors for a chrome style id
            //https://incisent.fogbugz.com/default.asp?19265

            ExteriorColorDropDown.SelectedIndex = -1;

            InteriorColorDropDown.SelectedIndex = -1;

            foreach (ListItem infoItem in AdditionalInfoItemsList.Items)
                infoItem.Selected = false;

        }

        protected void chkCertified_OnCheckedChanged(object sender, EventArgs e)
        {
            IsDirty = true;
        }

        protected void ExteriorColorChanged(object sender, EventArgs e)
        {
            IsDirty = true;
        }

        protected void InteriorColorChanged(object sender, EventArgs e)
        {
            IsDirty = true;
        }

        protected void ConditionChanged(object sender, EventArgs e)
        {
            IsDirty = true;
        }

        protected void AdditionalInfoItemsChanged(object sender, EventArgs e)
        {
            IsDirty = true;
        }

        /*BEGIN Save Callback FUNCTIONS */

        private void UpdateVehicle()
        {
            var vehConfig = VehicleConfiguration.FetchOrCreateDetailed(BusinessUnitId, InventoryId,
                   User.Identity.Name);

            vehConfig.VehicleCondition = ConditionDropDown.SelectedValue;

            var colorPair = ColorPair.Parse(ExteriorColorDropDown.SelectedValue);

            vehConfig.ExteriorColor1 = colorPair.First.Description;
            vehConfig.ExteriorColor2 = colorPair.Second.Description;

            vehConfig.ExteriorColorCode = colorPair.First.ColorCode;
            vehConfig.ExteriorColorCode2 = colorPair.Second.ColorCode;

            vehConfig.InteriorColorCode = InteriorColorDropDown.SelectedValue;
            vehConfig.InteriorColor = string.IsNullOrEmpty(InteriorColorDropDown.SelectedValue) ? "" : InteriorColorDropDown.SelectedItem.Text;


            vehConfig.KeyInformationList.Clear();
            foreach (ListItem infoItem in AdditionalInfoItemsList.Items)
            {
                if (infoItem.Selected)
                {
                    vehConfig.KeyInformationList.Add(new KeyInformation(AdditionalVehicleInfo.GetId(infoItem.Value), AdditionalVehicleInfo.GetAdText(infoItem.Value)));
                }
            }

            vehConfig.Save(BusinessUnitId, User.Identity.Name);

            //Reset Mediator to reflect changes to vehicle information entered by user.
            Mediator = new TemplateMediator(BusinessUnitId, InventoryId, Context.User.Identity.Name);
            
            MessageSender.SendAutoApproval(new AutoApproveMessage(BusinessUnitId, InventoryId, User.Identity.Name), InventoryInfo.IsNew(), GetType().FullName);
        }

        protected void TemplateList_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            //set the parameter for the declarative sqldatasource to the busienssunitid
            e.Command.Parameters["@BusinessUnitId"].Value = BusinessUnitId;
        }

        protected void CancelEdit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Workflow/Inventory.aspx?bucket=" + Request["bucket"]);
        }


        protected void MatchingSpecials_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["OwnerEntityId"] = BusinessUnitId;
            e.InputParameters["inventoryData"] = Mediator.VehicleData.InventoryItem;
        }

        protected void StylePieces_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception != null && e.Exception is VehicleCatalogException)
            {
                Response.Redirect("~/Workflow/Inventory.aspx?errorMsg=" + e.Exception.Message);
            }
        }

        protected void SetDefault_Click(object sender, EventArgs e)
        {
            //test the chrome styleId
            int chromeStyleId = Mediator.VehicleData.VehicleConfig.ChromeStyleID;
            if (chromeStyleId <= 0)
            {
                return;
            }

            int? themeSetId = null;
            AdTemplateSettings.SetAsDefaultForStyle(BusinessUnitId, int.Parse(FrameworkSelectorDDL.SelectedValue), themeSetId, chromeStyleId, Mediator.VehicleData.InventoryItem.InventoryType);
        }

        protected void wfhHeader_InventorySearching(object sender, EventArgs e)
        {
            //Get the matches for the search terms
            var allInventory = InventoryDataFilter.FetchListFromSession(BusinessUnitId);

            Bin bin = Bin.Create(wfsInventoryItem.SearchText);
            List<InventoryData> searchResults = InventoryDataFilter.WorkflowSearch(BusinessUnitId, allInventory, 0, 9999, String.Empty, bin);

            if (searchResults.Count > 1)
            {
                //More than 1 match, so send to the home page so they can click the desired vehicle
                Response.Redirect("~/Workflow/Inventory.aspx?search=" + Uri.EscapeDataString(wfsInventoryItem.SearchText));
            }
            else if (searchResults.Count == 0)
            {
                //Alert that no vehicle exists
            }
            else
            {
                FindSearchResult(searchResults[0].InventoryID);
            }
        }

        protected void FindSearchResult(int inventoryId)
        {
            var results = wfsInventoryItem.LocateVehicle(inventoryId);
            SetSearchResult(inventoryId, results);
        }

        protected void SetPricingLinkForInventory()
        {
            var results = wfsInventoryItem.LocateVehicle(InventoryId);
            invDataViewer.SetPricingLink(results);

        }
        protected void SetSearchResult(int inventoryId, LocateVehicleResults result)
        {
            string queryUrl = WorkflowState.CreateUrl(HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath, inventoryId, result.ActiveListType, result.UsedOrNewFilter, result.AgeBucketId);
            Response.Redirect(queryUrl);
        }

        private MerchandisingDescription RegenerateAd()
        {
            int? tempId = null;
            if (!string.IsNullOrEmpty(FrameworkSelectorDDL.SelectedValue))
            {
                tempId = int.Parse(FrameworkSelectorDDL.SelectedValue);
            }

            int? themeId = null;

            var editedModel = _advertisementConverter.FromJson(AdModelHidden.Value);
            return Mediator.ReGenerateTemplateText(editedModel, tempId, themeId, MaxThemeSentenceCount);
        }

        protected void OnPackageChanged(object sender, EventArgs e)
        {
            OptionPackagesControl.DataBindPackages();
            AdAndVehicleInSync = false;

            MessageSender.SendAutoApproval(new AutoApproveMessage(BusinessUnitId, InventoryId, User.Identity.Name), InventoryInfo.IsNew(), GetType().FullName);
        }

        protected void btn_RewriteFormat_OnClick(object sender, EventArgs e)
        {
            if (CertifiedProgramChangedBySystem)
            {
                invDataViewer.UpdateCertifiedStatus();
                CertifiedProgramChangedBySystem = false;
            }


            if (IsDirty)
            {
                UpdateVehicle();
            }


            AdAndVehicleInSync = true;
            MerchandisingDescription desc = RegenerateAd();

            // Remove regenerate button highlight
            RewriteAdButton.RemoveClass("red");

            BindControlsFromDescription(desc);
        }

        public ValueAnalyzerVehiclePreferenceTO GetValueAnalyzerPreference()
        {
            PricingContext context = PricingContext.FromMerchandising(BusinessUnitId, InventoryId);

            var command = new GetValueAnalyzerVehiclePreference(context.OwnerHandle, context.VehicleHandle);
            AbstractCommand.DoRun(command);
            return command.Preference;
        }

        [WebMethod]
        public static string HasCarfaxReport(int businessunitId, string vin)
        {
            return VehicleHistoryReport.HasCarfaxReport(businessunitId, vin, HttpContext.Current.User.Identity.Name);
        }

    }
}