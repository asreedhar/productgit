﻿<%@ Page Title="Current Inventory Summary | MAX : Online Inventory. Perfected." Language="C#" MasterPageFile="~/Workflow/Workflow.Master" Theme="None" AutoEventWireup="true" CodeBehind="Inventory.aspx.cs" Inherits="Wanamaker.WebApp.Workflow.Inventory" EnableEventValidation="false" %>
<%@ Import Namespace="FirstLook.Merchandising.DomainModel.Core" %>
<%@ Import Namespace="FirstLook.Merchandising.DomainModel.Vehicles" %>
<%@ Import Namespace="FirstLook.Merchandising.DomainModel.Vehicles.Inventory" %>
<%@ Import Namespace="FirstLook.Merchandising.DomainModel.Workflow" %>
<%@ Import Namespace="Wanamaker.WebApp.Helpers" %>
<%@ Register Src="~/Workflow/Controls/SearchInventory.ascx" TagPrefix="workflow" TagName="InventorySearch"  %>
<%@ Register TagPrefix="cwc" TagName="FiltersView" Src="~/Workflow/Controls/FiltersView.ascx" %>
<%@ Register TagPrefix="cwc" TagName="Pager" Src="~/Workflow/Controls/Pager.ascx" %>

<%@ Register TagPrefix="cwc" TagName="TrimSelector" Src="~/Workflow/Controls/TrimSelector.ascx" %>
<%@ Register tagPrefix="cwc" tagName="CurrentFilterView" src="~/Workflow/Controls/CurrentFilterView.ascx" %>

<asp:Content ID="CssContent" ContentPlaceHolderID="CssPlaceHolder" runat="server">

    <% if(HtmlHelpers.IsDebugBuild()) { %>
        <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Main.debug.css") %>" />
        <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Home.debug.css") %>" />
        <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MaxMvc/CampaignDataTable.css") %>" />
    <% } else { %>
        <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Inventory.Combined.css") %>" />
    <% } %>

</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="BodyPlaceHolder" runat="server">
    <!--These should move to MAX Stand alone login dashboard once we have that running-->
    <!--They start your session in both the IMT and NextGen -->
    <div id="tomcatSessionImages" runat="server">
        <img  height="1" width="1" src='/NextGen/common/_images/d.gif?< %= DateTime.Now.ToString("yyyyMMddhhmmss") % >' style="visibility: hidden;"/>
        <img height="1" width="1" src='/IMT/common/_images/d.gif?< %= DateTime.Now.ToString("yyyyMMddhhmmss") % >' style="visibility: hidden;"/>
    </div>

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <div id="BulkUpload">
        <a href="<%= BulkUploadUrl %>" title="Upload photos for multiple vehicles" target="_blank">Bulk Upload</a>
    </div>

    <workflow:InventorySearch id="InventorySearch" OnInventorySearching="wfhHeader_InventorySearching" runat="server"></workflow:InventorySearch>

    <div id="InventoryFilter" class="clearfix">

        <h1>Current Inventory Summary</h1>

        <asp:DropDownList ID="InventoryTypeDDL" ClientIDMode="Static" runat="server" AutoPostBack="true" OnSelectedIndexChanged="UsedNew_Selected" onchange="_gaq.push(['_trackEvent', 'Bucket Selector', 'Change', 'Inventory Type']);">
            <asp:ListItem Text="Used and New Vehicles" Value="0"></asp:ListItem>
            <asp:ListItem Text="Used Vehicles Only" Value="2"></asp:ListItem>
            <asp:ListItem Text="New Vehicles Only" Value="1"></asp:ListItem>
        </asp:DropDownList>

    </div>
    
    <div id="Content" class="clearfix">
        
        <asp:Panel ID="errors" runat="server" Visible="false" CssClass="info">
            <asp:Label ID="errorLbl" runat="server"></asp:Label>
        </asp:Panel>

        <div id="FiltersDiv">
            <cwc:FiltersView ID="Filters" runat="Server" Visible="true" />
            <h2 id="CurrentFilter">
                <cwc:CurrentFilterView ID="CurrentFilterView" runat="server" OnResetSearch="OnResetSearch" />
                <asp:Button ID="incentives" runat="server" Text="" CssClass="pricing_button"></asp:Button>
                <asp:LinkButton ID="viewCampaigns" runat="server" Text="View Active Discounts"></asp:LinkButton>
                <asp:HyperLink ID="ExportToExcel" Text="Export to Excel" CssClass="export" runat="server" />
            </h2>
        </div>

        <div id="InventoryList">
            
            <asp:ListView ID="InventoryTableRepeater" runat="server" DataKeyNames="InventoryId, Make, Vin" OnItemDataBound="InventoryRow_DataBound"  
                                     OnSelectedIndexChanged="SelectedGridViewRow_InventoryTable" DataSourceID="InventoryDSList">
                
                <EmptyDataTemplate>
                
                    <div class="empty"><p>There are currently no vehicles in the selected list.</p></div>

                </EmptyDataTemplate>

                <LayoutTemplate>
                    <table id="InventoryTable">
                        <colgroup>
                            <col style="width:20px;" />
                            <col style="width:110px;" />
                            <col style="width:50px;" />
	                        <col style="width:424px;" />
                            <col style="width:60px;" />
                            <col style="width:68px;" />
                            <col style="width:68px;" />
                            <!--[if IE 7]>
	                            <col style="width:145px;" />
                            <![endif]-->
                            <!--[if gte IE 8]>
                                <col style="width:170px;" />
                            <![endif]-->
                            <!--[if !IE]>
                                <col style="width:170px;" />
                            <![endif]-->
                        </colgroup>
                        <thead>
                            <tr>
                                
                                <th colspan="2">
                                    <div id="BulkActions" class="field">
                                        <input type="checkbox" id="BulkActionsCkb" />
                                        
                                        <asp:DropDownList ClientIDMode="Static" ID="BulkActionsSelect" OnSelectedIndexChanged="BulkActionsSelectedIndexChanged" OnPreRender="BulkActionsSelectPreRender" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </th>
                                <th>
                                    <asp:LinkButton ID="lnkBtnAge" CommandName="SortInventory" CommandArgument="InventoryReceivedDate" OnCommand="SortInventory" runat="server">Age</asp:LinkButton>
                                </th>
                                <th class="alignLeft ymm">
                                    <asp:LinkButton ID="lnkBtnYear" CommandName="SortInventory" CommandArgument="VehicleYear" OnCommand="SortInventory" runat="server">Year</asp:LinkButton>
                                    <asp:LinkButton ID="lnkBtnMake" CommandName="SortInventory" CommandArgument="Make" OnCommand="SortInventory"
                                        runat="server">Make</asp:LinkButton>
                                    <asp:LinkButton ID="lnkBtnModel" CommandName="SortInventory" CommandArgument="Model" OnCommand="SortInventory"
                                        runat="server">Model</asp:LinkButton>
                                    <asp:LinkButton ID="lnkBtnTrim" CommandName="SortInventory" CommandArgument="Trim" OnCommand="SortInventory"
                                        runat="server">Trim</asp:LinkButton>
                                    <asp:LinkButton ID="lnkBtnMileage" CommandName="SortInventory" CommandArgument="MileageReceived" OnCommand="SortInventory" runat="server">Mileage</asp:LinkButton>
                                    <asp:LinkButton ID="lnkBtnStockNumber" CommandName="SortInventory" CommandArgument="StockNumber" OnCommand="SortInventory" runat="server">Stock #</asp:LinkButton>
                                </th>
                                <th>
                                    <asp:LinkButton ID="lnkBtnCertified" CommandName="SortInventory" CommandArgument="Certified" OnCommand="SortInventory" runat="server">Certified</asp:LinkButton>
                                </th>
                                <th class="alignRight">
                                    <asp:LinkButton ID="lnkBtnListPrice" CommandName="SortInventory" CommandArgument="ListPrice" OnCommand="SortInventory" runat="server">Internet Price</asp:LinkButton>
                                </th>
                                <th class="alignRight">
                                    <asp:LinkButton ID="lnkBtnUnitCost" CommandName="SortInventory" CommandArgument="UnitCost" OnCommand="SortInventory" runat="server">Unit Cost</asp:LinkButton>
                                </th>
                                <th class="alignLeft actions">
                                    Actions Required
                                </th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <td colspan="8">
                                    <div class="helper">
                                        <img src="../Themes/MAX3.0/Images/icon_Checkbox.png" width="16" height="16" alt="" />
                                        Select checkboxes to perform Bulk Actions on selected vehicles
                                    </div>
                                </td>
                            </tr>
                        </tfoot>
                        <tbody>
                            <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                        </tbody>
                    </table>
                </LayoutTemplate>

                <ItemTemplate>
                    <tr>
                        <td class="valignTop">
                            <input type="hidden" id="ValidVinHidden" runat="server" class="validVIN" clientidmode="AutoID" />
                            <div class="field">
                                <input type="checkbox" id="BulkActionsCheckbox" class="bulkActions" runat="server" clientidmode="AutoID" />
                                <input type="hidden" id="InventoryId" runat="server" value='<%# Eval("InventoryId") %>' clientidmode="AutoID" />
                            </div>
                        </td>
                        <td class="valignMiddle">
                            <div class='photos <%# WorkflowFilters.NoOrLowPhotos((IInventoryData) Container.DataItem)  ? "attention" : string.Empty %>'>
                                <asp:LinkButton ToolTip='<%# WorkflowFilters.NoOrLowPhotos((IInventoryData) Container.DataItem)   ? "No/Low Photos" : string.Empty %>' ID="PhotoLink" runat="server" OnCommand="BtnPhotoIconClick" CssClass="needsPhotos" CommandArgument='<%# Eval("InventoryId") %>' OnClientClick="_gaq.push(['_trackEvent', 'Home Page', 'Click', 'Photo Link']);">
                                    <%--TODO: Add Photo status to alt attribute and attention class --%>
                                    <img src="<%# GetThumbnailUrl(Eval("ThumbnailUrl")) %>" width="80" height="60" alt="" />
                                    <span class="count"><%# Eval( "PhotoCount" )%></span>
                                </asp:LinkButton>
                            </div>
                        </td>
                        <td class="age valignMiddle"><%# Eval( "Age" ) %> <span class="small">Days</span></td>
                        <td class="alignLeft ymm">
                            <h3>
                                <asp:LinkButton ID="BtnInventoryItem" runat="server" CssClass="vehicleLink" OnCommand="BtnInventoryItemClick" Text='<%# Eval("Year") + " " + Eval("Make") + " " + Eval("Model") + " " + GetTrimDisplay( Eval("ChromeStyleId").ToString(), Eval("Trim").ToString(), Eval("Model").ToString(), Eval("AfterMarketTrim").ToString() ) %>' CommandArgument='<%# Eval("InventoryId") %>' OnClientClick="_gaq.push(['_trackEvent', 'Home Page', 'Click', 'Year/Make/Model Link']);" />
                                <cwc:TrimSelector ID="InventoryTrimSelector" Vin='<%# Eval("VIN") %>' BusinessUnitId="<%#BusinessUnitId%>" InventoryId='<%# Eval("InventoryId") %>' UsedOrNewFilter="<%#UsedOrNewFilter%>" runat="Server"  /> 
                            </h3>
                            <ul class="vehicleSubInfo">
                                <li runat="server" visible='<%# Eval("BaseColor").ToString().Trim().Length > 0 %>'><%# Eval("BaseColor").ToString().ToLower() %></li>
                                <li class="mileage" runat="server" visible='<%# Eval("DisplayMileage").ToString().Trim() != "--" %>'><%# Eval("DisplayMileage") %> mi.</li>
                                <li><a href="<%# ((InventoryData)Container.DataItem).GetEStockLink() %>" target="_blank"><%# Eval("StockNumber") %></a></li>
                                <li><asp:HyperLink ID="CtrGraphLink" CssClass="ctr-graph-link" Text="Graph" data-BusinessUnitId="<%#BusinessUnitId%>" data-inventoryid="<%# ((InventoryData)Container.DataItem).InventoryID %>" data-stockNum="<%# ((InventoryData)Container.DataItem).StockNumber %>" data-ymmt='<%# Eval("Year") + " " + Eval("Make") + " " + Eval("Model") + " " + GetTrimDisplay( Eval("ChromeStyleId").ToString(), Eval("Trim").ToString(), Eval("Model").ToString(), Eval("AfterMarketTrim").ToString() ) %>' runat="server"><img src="../Themes/MAX3.0/Images/1343832346_pie-chart_graph.png" width="16" title="CTR graph" border="0" /></asp:HyperLink></li>
                                <!-- auto window sticker -->
                                <li>
                                    <asp:HyperLink ID="lnkMmsSticker" runat="server" Visible='<%#HasAutoWindowSticker && (bool)DataBinder.Eval(Container, ("dataItem.AutoWindowStickerPrinted"))%>' Target="_blank" NavigateUrl='<%# "~/Print/AutoWindowStickerPrint.aspx?InventoryId=" + Eval("InventoryId")%>' >
                                        <img src="../Themes/MAX3.0/Images/smartphoneQr.png" width="16" title="Mobile Showroom Sticker" border="0" />                                        
                                    </asp:HyperLink>
                                </li>
                                <li runat="server" visible='<%# Request.QueryString.ToString().Contains("MAXQA=true") %>'><%# Eval("Vin") %></li>
                            </ul>
                        </td>
                        <td class="valignMiddle"><%# (bool)Eval("Certified") ? "C" : string.Empty %></td>
                        <td class="price valignMiddle alignRight">
                            <asp:LinkButton ToolTip='<%# WorkflowFilters.NeedsPricingAll((IInventoryData) Container.DataItem) ? "Needs Pricing" : string.Empty %>' ID="ListPriceLink1" runat="server" OnCommand="BtnPricingIconClick" CssClass='<%# WorkflowFilters.NeedsPricingAll((IInventoryData) Container.DataItem) ? "attention" : string.Empty %>' CommandArgument='<%# Eval("InventoryId") %>' OnClientClick="_gaq.push(['_trackEvent', 'Home Page', 'Click', 'List Price Link']);"><%# string.Format( "{0:C0}", Eval( "ListPrice" ) )%></asp:LinkButton>
                        </td>
                        <td class="valignMiddle alignRight"><%# string.Format( "{0:C0}", Eval( "UnitCost" ) )%></td>
                        <td class="alignLeft valignMiddle">
                            <ul class="actions">
                                <li class="approval" runat="server" visible='<%# WorkflowFilters.NeedsApprovalAll((IInventoryData) Container.DataItem) || WorkflowFilters.NeedsAdReviewAll((IInventoryData) Container.DataItem)%>'>
                                    <asp:LinkButton ToolTip="Needs Ad Review/Approval" ID="BtnPendingApprovalIcon" runat="server" OnCommand="BtnPendingApprovalIconClick" CommandArgument='<%# Eval("InventoryId") %>' OnClientClick="_gaq.push(['_trackEvent', 'Home Page', 'Click', 'Item Link (Approval/Ad Review Needed)']);"><span>Approval</span></asp:LinkButton>
                                </li>
                                <li class="pricing" runat="server" visible='<%# WorkflowFilters.NeedsPricingAll((IInventoryData) Container.DataItem) %>'>
                                    <asp:LinkButton ToolTip="Needs Pricing" ID="BtnPricingIcon" runat="server" OnCommand="BtnPricingIconClick" CssClass="needsPricing" CommandArgument='<%# Eval("InventoryId") %>' OnClientClick="_gaq.push(['_trackEvent', 'Home Page', 'Click', 'Item Link (Pricing Needed)']);"><span>Pricing</span></asp:LinkButton>
                                </li>
                                <li class="photos" runat="server" visible='<%# !WorkflowFilters.DoNotPost((IInventoryData) Container.DataItem) && WorkflowFilters.NoOrLowPhotos((IInventoryData) Container.DataItem)   %>'>
                                    <asp:LinkButton ToolTip="No/Low Photos" ID="BtnPhotosIcon" runat="server" OnCommand="BtnPhotoIconClick" CssClass="needsPhotos" CommandArgument='<%# Eval("InventoryId") %>' OnClientClick="_gaq.push(['_trackEvent', 'Home Page', 'Click', 'Item Link (Photos Needed)']);"><span>Photos</span></asp:LinkButton>
                                </li>
                                <li class="equipment" runat="server" visible='<%#  !WorkflowFilters.DoNotPost((IInventoryData) Container.DataItem) && WorkflowFilters.EquipmentNeeded((IInventoryData) Container.DataItem)   %>'>
                                    <asp:LinkButton ToolTip="Needs Equipment" ID="BtnMerchandisingIcon" runat="server" OnCommand="BtnDocumentIconClick" CommandArgument='<%# Eval("InventoryId") %>'  OnClientClick="_gaq.push(['_trackEvent', 'Home Page', 'Click', 'Item Link (Equipment Needed)']);"><span>Equipment</span></asp:LinkButton>
                                </li>
                            </ul>
                        </td>
                    </tr>
                </ItemTemplate>

            </asp:ListView>

            <asp:Panel ID="Pager" CssClass="clearfix" runat="server" ClientIDMode="Static">
                <cwc:Pager ID="Pager1" OnPageNumberInfoChanged="OnPageNumberChanged" runat="server" />
            </asp:Panel>

        </div>

    </div>
    
    

    <asp:ObjectDataSource runat="server" ID="InventoryDSList" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.Inventory.InventoryDataSource"
        OnSelecting="InventoryDSList_Selecting" SelectMethod="WorkflowInventorySubsetSelect"
        EnablePaging="True" EnableCaching="false" StartRowIndexParameterName="startIndex" MaximumRowsParameterName="pageSize"
        SortParameterName="sortExpression">
        <SelectParameters>
            <asp:Parameter Name="businessUnitID" Type="Int32" />
            <asp:ControlParameter Name="usedOrNewFilter" Type="Int32" ControlID="InventoryTypeDDL" PropertyName="SelectedValue" />
            <asp:ControlParameter Name="searchText" ControlID="InventorySearch" PropertyName="SearchText" />
        </SelectParameters>
    </asp:ObjectDataSource>


    <div id="new-car-pricing" class="dialog" style="display: none">
        <h3>Select Vehicles</h3>
        <div id="new-car-select">
            <div style="display: inline-block; margin: 0px 5px">
                <div>Year</div>
                <select id="yearSelectBox" multiple="multiple" size="4" data-allowMultiSelect="false" data-selector-order="0"></select>
            </div>
            <div style="display: inline-block; margin: 5px 5px">
                <div>Make</div>
                <select id="makeSelectBox" multiple="multiple" size="4" data-allowMultiSelect="false" data-selector-order="1" data-selector-parent="0"></select>
            </div>
            <div style="display: inline-block; margin: 5px 5px">
                <div>Models</div>
                <select id="modelSelectBox" multiple="multiple" size="4" data-allowMultiSelect="false" data-selector-order="2" data-selector-parent="1"></select>
            </div>
            <div style="display: inline-block; margin: 5px 5px">
                <div>Trims</div>
                <select id="trimSelectBox" multiple="multiple" size="4" data-allowMultiSelect="false" data-selector-order="3" data-selector-parent="2"></select>
            </div>
        </div>
        <div id="new-car-body-select">
            <div style="display: inline-block; margin: 0px 5px">
                <div>Body</div>
                <select id="bodyStyleSelectBox" multiple="multiple" size="8" data-allowMultiSelect="true" data-selector-order="4" data-selector-parent="3"></select>
            </div>
            <div style="display: inline-block; margin: 5px 5px">
                <div>Engine</div>
                <select id="engineDescriptionSelectBox" multiple="multiple" size="8" data-allowMultiSelect="true" data-selector-order="5" data-selector-parent="3"></select>
            </div>
        </div>
        <div>
            <span id="new-car-vehicles">0</span> Vehicles have been selected.
        </div>
        
        <div style="float: left; width:500px;">
            <h3>Apply Discounts</h3>
            <select id="priceBaseLine" style="float:left">
                <option value="Msrp">MSRP</option>
                <option value="Invoice">Invoice</option>
            </select>
            <div style="float:left; margin: 0px 5px" id="price-buttons">
                <input type="radio" name="priceDelta" value="add"/> Add<br />
                <input type="radio" name="priceDelta" value="subtract"/> Subtract
            </div>
            <div style="float:left; margin: 0px 5px">
                <table>
                    <tr>
                        <td><label for="rebate-price">Manufacturer Rebate</label></td>
                        <td style="padding: 0px 0px 0px 5px"><input id="rebate-price" type="text" value="0"/></td>
                    </tr>
                    <tr>
                        <td><label for="discount-price">Dealer Discount</label></td>
                        <td style="padding: 0px 0px 0px 5px"><input id="discount-price" type="text" value="0"/></td>
                    </tr>
                </table>
            </div>
        </div>
        <div style="float: left">
            <h3>Expiration Date</h3>
            <input id="newCar-Expiration" type="text" />
        </div>
        <div id="apply-buttons" style="clear: both; padding: 10px 0px;">
            <button type="button">Cancel</button>
            <button type="button" id="apply-prices">Apply</button>
            <div class="loading" style="width:30px; height:30px; position: relative; top:10px"></div>
            <div class="checkmark" style="min-width:30px; height:20px; padding: 0px 0px 0px 20px">Changes Applied Successfully.</div>
            <div class="error"></div>
        </div>
        
    </div>

    <div id="activeCampaignsDialog" class="dialog" style="display: none">
        
        <script id="activeCampaignTemplate" type="text/x-jquery-tmpl">   
            <table class="display">
                <thead>
                    <tr>
                    <th>Discount</th>
                    <th>Start Date</th>
                    <th>Expire Date</th>
                    <th>Discount Type</th>
                    <th>Manufacturer Rebate</th>
                    <th>Dealer Discount</th>
                    <th>User</th>
                    <th>Cancel</th>
                    </tr>
                </thead>
                <tbody >
                    {{each data}}
                    <tr id="${CampaignId}">
                    <td>${CampaignName}</td>
                    <td>${CreationDate}</td>
                    <td>${ExpirationDate}</td>
                    <td>${PriceBaseLine}</td>
                    <td>${RebateAmount}</td>
                    <td>${IncentiveAmount}</td>
                    <td>${CreatedBy}</td>
                    <td><button type="button">Cancel</button></td>
                    </tr>
                    {{/each}}
                </tbody>
            </table>
        </script>

        <script id="campaignDetailsTemplate" type="text/x-jquery-tmpl">   
            <table class="display">
                <thead>
                    <tr>
                    <th>Stock Number</th>
                    <th>MSRP</th>
                    <th>Unit Cost</th>
                    <th>Original Internet Price</th>
                    <th>New Internet Price</th>
                    </tr>
                </thead>
                <tbody >
                    {{each InventoryList}}
                    <tr>
                    <td>${StockNumber}</td>
                    <td>${MSRP}</td>
                    <td>${UnitCost}</td>
                    <td>${OriginalPrice}</td>
                    <td>${NewInternetPrice}</td>
                    </tr>
                    {{/each}}
                </tbody>
            </table>
        </script>
        
        <div id="campaigns">
            <h3>Active Discounts</h3>
            <div id="campaignContent" style="padding: 15px">
            </div>
            <h3>Discount Details</h3>
            <div id="campaignDetailsContent" style="padding: 15px">
            </div>
        </div>
    </div>

    <div id="ctr-graph" class="dialog" style="display: none">                
        
        <ul class="carinfo"></ul>

        <script id="ctrGraphHeader" type="text/x-jquery-tmpl">
          <li><span class="yearmakemodel data">${YearMakeModel}</span></li>
          <li>Ext. Color: <span class="data color">${ExteriorColor}</span></li>
          <li>Photos: <span class="data photos">${PhotoCount}</span></li>
          <li>Miles: <span class="data mileage">${Mileage}</span></li>
          <li>Certified: <span class="data cert">{{if Certfied===true}}Yes{{else}}No{{/if}}</span></li>
        </script>

        <div id="chart-line"></div>

        <div class="loading"></div>

    </div>

</asp:Content>

<asp:Content ID="ScriptContent" ContentPlaceHolderID="ScriptsPlaceHolder" runat="server">
    
    <% if(HtmlHelpers.IsDebugBuild()) { %>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/ctr-graph.js") %>"></script>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/MAX.Utils.debug.js") %>"></script>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Max/MAX.UI.Global.debug.js") %>"></script>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Home.debug.js") %>"></script>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Lib/date.js") %>"></script>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Lib/jquery.dataTables.min.js") %>"></script>
    <% } else { %>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Workflow/Home.Combined.js") %>"></script>
    <% } %>

<script>
$("#incentives").on("mousedown",function(){ $(this).addClass("down"); });
$(document).on("mouseup",function(){ $("#incentives").removeClass("down"); });    
</script>

</asp:Content>