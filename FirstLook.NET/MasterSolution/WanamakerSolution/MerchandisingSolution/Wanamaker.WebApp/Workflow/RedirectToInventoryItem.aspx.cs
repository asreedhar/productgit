﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.DataAccess;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using FirstLook.Merchandising.DomainModel.Workflow;
using Wanamaker.WebApp.AppCode.AccessControl;
using log4net;
using Wanamaker.WebApp.Workflow.Controls;

namespace Wanamaker.WebApp.Workflow
{
    public partial class RedirectToInventoryItem : Page
    {
        private static readonly ILog Log = LogManager.GetLogger( typeof( RedirectToInventoryItem ).FullName );

        protected void Page_Init( object sender, EventArgs e )
        {
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
        }

        protected void Page_Load( object sender, EventArgs e )
        {
            MembershipUser user = Context.Items[IdentityHelper.MembershipContextKey] as MembershipUser;
            if (user == null)
                Response.Redirect("~/Default.aspx", true);

            int dealerId = Int32Helper.ToInt32( Request["bu"] );
            int inventoryId = Int32Helper.ToInt32( Request["inv"] );
            string pageType = Convert.ToString(Request["pg"]);
            // Persist BusinessUnitID.
            BusinessUnit bu = BusinessUnitFinder.Instance().Find( dealerId );
            SetupSoftwareSystemComponentState( bu );
             
            BusinessUnit currentBusinessUnit = Context.Items["BusinessUnit"] as BusinessUnit;
            if (currentBusinessUnit != null && currentBusinessUnit.Id != bu.Id) //if different business unit, remove session and set new business unit in context
            {
                InventoryDataFilter.ClearListFromSession();
                HttpContext.Current.Items["BusinessUnit"] = bu;
            }

            var selector = new WorkflowSelector();
            var result = selector.LocateVehicleOpenFilter(inventoryId);

            var url = CalculateWorkflowPage();

            string queryUrl;
            if (!string.IsNullOrEmpty(pageType) && pageType.ToLower() == "pricing" && DealerPingAccess.CheckDealerPingAccess(dealerId))
            {
                queryUrl = WorkflowState.CreatePricingPingUrl("~/PricingAnalysis/pingone",inventoryId, dealerId,result.ActiveListType, result.UsedOrNewFilter, result.AgeBucketId);
            }
            else
            {
                queryUrl = WorkflowState.CreateUrl(url, inventoryId, result.ActiveListType, result.UsedOrNewFilter, result.AgeBucketId);
            }
             
            Response.Redirect(queryUrl);
        }

        private string CalculateWorkflowPage()
        {
            var pg = Request.QueryString["pg"];
            switch((pg ?? "").ToLowerInvariant())
            {
                case "photos":  return "Photos.aspx";
                case "pricing": return "Pricing.aspx";
                default:
                    return "ApprovalSummary.aspx";
            }
        }

        private void SetupSoftwareSystemComponentState( BusinessUnit dealer )
        {
            SoftwareSystemComponentState state = LoadState( SoftwareSystemComponentStateFacade.DealerComponentToken );
            state.DealerGroup.SetValue( dealer.DealerGroup() );
            state.Dealer.SetValue( dealer );
            state.Member.SetValue( null );
            state.Save();
        }

        private SoftwareSystemComponentState LoadState( string componentToken )
        {
            SoftwareSystemComponentState state = SoftwareSystemComponentStateFacade.FindOrCreate( User.Identity.Name, componentToken );

            if( state == null )
            {
                Member member = MemberFinder.Instance().FindByUserName( User.Identity.Name );

                SoftwareSystemComponent component = SoftwareSystemComponentFinder.Instance().FindByToken( componentToken );

                state = new SoftwareSystemComponentState();
                state.AuthorizedMember.SetValue( member );
                state.SoftwareSystemComponent.SetValue( component );
            }

            return state;
        }
    }
}
