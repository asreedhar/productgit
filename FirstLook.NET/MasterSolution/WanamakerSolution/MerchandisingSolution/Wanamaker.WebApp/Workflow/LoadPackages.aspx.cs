using System;
using System.Collections.Generic;
using System.Web;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using FirstLook.Merchandising.DomainModel.Workflow;
using log4net;

namespace Wanamaker.WebApp.Workflow
{
    public partial class LoadPackages : WorkflowBasePage
    {
        #region Logging

        private static readonly ILog Log = LogManager.GetLogger(typeof(LoadPackages).FullName);

        #endregion

        private InventoryData InventoryInfo
        {
            get
            {
                return invDataViewer.InventoryInfo;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (InventoryId != 0)
                {
                    invDataViewer.SetNewVehicle(InventoryId);
                }
                //DataBind();
                invDataViewer.DataBind();
            }

            wfsInventoryItem.VehicleIsPreOwned = InventoryInfo.IsPreOwned();
            wfsInventoryItem.HasMarketingUpgrade = HasMarketingUpgrade;

            // Pass relevant data to client
            InventoryIdHidden.Value = InventoryId.ToString();
            BusinessUnitIdHidden.Value = BusinessUnitId.ToString();

            //Set inventory pricing link
            SetPricingLinkForInventory();
        }

        protected void OnPackagesSaveClicked(object sender, EventArgs e)
        {
            HandleNotificationDisplay();
        }

        private void HandleNotificationDisplay()
        {
            // if ad is already approved show warning
            if (IsPreApproved(BusinessUnitId) == false
                || InventoryInfo.StatusBucket == MerchandisingBucket.Online
                || InventoryInfo.StatusBucket == MerchandisingBucket.OnlineNeedsAttention
                || InventoryInfo.StatusBucket == MerchandisingBucket.AllOnline)
            {
                WarningPanel.Visible = true;
                AdApprovalWarning.Visible = true;
            }
            else
            {
                WarningPanel.Visible = false;
                AdApprovalWarning.Visible = false;
            }
        }

        protected void wfhHeader_InventorySearching(object sender, EventArgs e)
        {
            //Get the matches for the search terms
            var allInventory = InventoryDataFilter.FetchListFromSession(BusinessUnitId);

            Bin bin = Bin.Create(wfsInventoryItem.SearchText);
            List<InventoryData> searchResults = InventoryDataFilter.WorkflowSearch(BusinessUnitId, allInventory, 0, 9999, String.Empty, bin);

            if (searchResults.Count > 1 || searchResults.Count == 0)
            {
                //More than 1 match, so send to the home page so they can click the desired vehicle
                //OR no results found
                Response.Redirect("~/Workflow/Inventory.aspx?search=" + wfsInventoryItem.SearchText);
            }
            else
            {
                FindSearchResult(searchResults[0].InventoryID);
            }
        }

        protected void SetPricingLinkForInventory()
        {
            var results = wfsInventoryItem.LocateVehicle(InventoryId);
            invDataViewer.SetPricingLink(results);

        }

        private void SetSearchResult(int inventoryId, LocateVehicleResults result)
        {
            string queryUrl = WorkflowState.CreateUrl(HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath, 
                inventoryId, result.ActiveListType, result.UsedOrNewFilter, result.AgeBucketId);
            Response.Redirect(queryUrl);
        }

        private void FindSearchResult(int inventoryId)
        {
            var results = wfsInventoryItem.LocateVehicle(inventoryId);
            SetSearchResult(inventoryId, results);
        }
    }
}
