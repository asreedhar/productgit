<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoadPackages.aspx.cs" Inherits="Wanamaker.WebApp.Workflow.LoadPackages" Theme="None" MasterPageFile="~/Workflow/Workflow.Master" Title="Load Packages | MAX : Online Inventory. Perfected." MaintainScrollPositionOnPostback="False" %>
<%@ Import Namespace="Wanamaker.WebApp.Helpers" %>

<%@ Register TagPrefix="cwc" TagName="WorkflowSelector" Src="~/Workflow/Controls/WorkflowSelector.ascx" %>
<%@ Register TagPrefix="carbuilder" TagName="InventoryDataView" Src="~/Workflow/Controls/InventoryDataView.ascx" %>
<%@ Register TagPrefix="pkg" TagName="Packages" Src="~/Workflow/Controls/Packages.ascx" %>

<asp:Content ID="CssContent" ContentPlaceHolderID="CssPlaceHolder" runat="server">

    <% if(HtmlHelpers.IsDebugBuild()) { %>
        <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Main.debug.css") %>" />
        <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Workflow.debug.css") %>" />
        <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Packages.debug.css") %>" />
    <% } else { %>
        <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Packages.Combined.css") %>" />
    <% } %>
    <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/MediaQueries.css") %>" />

</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="BodyPlaceHolder" runat="server">

    <asp:HiddenField ID="InventoryIdHidden" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="BusinessUnitIdHidden" runat="server" ClientIDMode="Static" />

    <asp:ScriptManager ID="ScriptManager2" runat="server"></asp:ScriptManager>
    
    <cwc:WorkflowSelector ID="wfsInventoryItem" runat="server" EnableViewState="true" ActiveLink="OptionPackages"
                          OnInventorySearching="wfhHeader_InventorySearching"></cwc:WorkflowSelector>

    <div id="VehicleSummary" class="clearfix">

        <carbuilder:InventoryDataView ID="invDataViewer" runat="server" InventoryId='<%# InventoryId %>' BusinessUnitId='<%# BusinessUnitId %>' ShowLastAdLink="false" />
    
    </div>
    
    <div id="Content">

        <asp:Panel ID="WarningPanel" runat="server" Visible="false" CssClass="warning">
            <p id="AdApprovalWarning" runat="server" Visible="false">
                Action Required: Changes have been made to this vehicle. You must Approve the Ad for your changes to appear online.
                <a href="ApprovalSummary.aspx?<%= Request.QueryString %>">Click here to go to Approval page</a>
            </p>
        </asp:Panel>

        <pkg:Packages ID="PackagesControl" WorkflowSelectorID="wfsInventoryItem" InventoryDataViewID="invDataViewer" OnSaveClicked="OnPackagesSaveClicked" runat="server"/>

    </div>

</asp:Content>

<asp:Content ID="ScriptContent" ContentPlaceHolderID="ScriptsPlaceHolder" runat="server">

    <% if(HtmlHelpers.IsDebugBuild()) { %>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/MAX.Utils.debug.js") %>"></script>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Max/MAX.UI.Global.debug.js") %>"></script>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Workflow/Workflow.debug.js") %>"></script>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Workflow/LoadPackages.debug.js") %>"></script>
    <% } else { %>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Workflow/LoadPackages.Combined.js") %>"></script>
    <% } %>

</asp:Content>
