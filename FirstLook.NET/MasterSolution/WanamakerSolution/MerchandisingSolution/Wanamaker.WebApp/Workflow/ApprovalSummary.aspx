﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="ApprovalSummary.aspx.cs" Inherits="Wanamaker.WebApp.Workflow.ApprovalSummary"
    Theme="None" Title="Ad Approval | MAX : Online Inventory. Perfected." EnableEventValidation="false" MaintainScrollPositionOnPostback="true"
    MasterPageFile="~/Workflow/Workflow.Master" %>
<%@ MasterType virtualpath="~/Workflow/Workflow.Master" %>
<%@ Import Namespace="Wanamaker.WebApp.Helpers" %>

<%@ Register TagPrefix="cwc" TagName="WorkflowSelector" Src="~/Workflow/Controls/WorkflowSelector.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="Firstlook.Merchandising.WebControls" Namespace="FirstLook.Merchandising.WebControls" TagPrefix="cc1" %>
<%@ Register TagPrefix="cwc" TagName="WorkflowHeader" Src="~/Workflow/Controls/WorkflowHeader.ascx" %>
<%@ Register TagPrefix="carbuilder" TagName="InventoryDataView" Src="~/Workflow/Controls/InventoryDataView.ascx" %>
<%@ Register TagPrefix="cwc" TagName="QuickPackages" Src="~/Workflow/Controls/QuickPackages.ascx" %>
<%@ Register TagPrefix="pkg" TagName="Packages" Src="~/Workflow/Controls/Packages.ascx" %>
<%@ Register TagPrefix="Controls" TagName="VehicleHistoryReport" Src="~/Workflow/Controls/VehicleHistoryReport.ascx" %>

<asp:Content ID="CssContent" ContentPlaceHolderID="CssPlaceHolder" runat="server">

    <% if(HtmlHelpers.IsDebugBuild()) { %>
        <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Main.debug.css") %>" />
        <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Workflow.debug.css") %>" />
        <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Approval.debug.css") %>" />
        <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Packages.debug.css") %>" />
    <% } else { %>
        <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Approval.Combined.css") %>" />
    <% } %>
    <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/MediaQueries.css") %>" />
    
</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="BodyPlaceHolder" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <%= Wanamaker.WebApp.ProfilerSetting.RenderIncludes() %>
    
    <asp:HiddenField ID="AdModelHidden" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="PreviousAdModelHidden" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="InventoryIdHidden" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="VinHidden" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="BusinessUnitIdHidden" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="ManufacturerIdHidden" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="ManufacturerNameHidden" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="LastListPriceHidden" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="PreviewLengthLimit" runat="server" ClientIDMode="Static" />

    <asp:UpdatePanel ID="IsDirtyUpdatePanel" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="AdAndVehicleInSyncHidden" ClientIDMode="Static" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
        
    <cwc:WorkflowSelector ID="wfsInventoryItem" runat="server" EnableViewState="true" ActiveLink="Approval" 
                          OnInventorySearching="wfhHeader_InventorySearching"></cwc:WorkflowSelector>
   
    <div id="VehicleSummary" class="clearfix">
    
        <carbuilder:InventoryDataView ID="invDataViewer" runat="server" InventoryId='<%# InventoryId %>' BusinessUnitId='<%# BusinessUnitId %>' ShowCertified="true" ShowLastAdLink="True" DisablePrintWinowSticker="True" AllowTrimSelection="True"/>
    
        <div id="StepCompleted" class='<%= StepComplete ? "complete" : string.Empty %>'>
            <asp:CheckBox ID="StepCompletedCkb" runat="server" ClientIDMode="Static" StepStatusType="AdReview" onclick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Ad Review Complete']);" />
            <label for="StepCompletedCkb">Ad Review Complete</label>
        </div>

        <asp:Panel ID="noVehiclesPanel" runat="server" Visible="false">
            <span class="noVehiclesLbl">No vehicles remaining in list - Return to <a href="~/Workflow/Inventory.aspx">Home</a> or navigate to other vehicles on the right. </span>
        </asp:Panel>

    </div>

    <div id="Content" class="clearfix">
    
        <asp:Panel ID="warning" ClientIDMode="Static" runat="server" Visible="false">
            <span class="message">WARNING: Ad copy created with outdated price (<asp:Label ID="descPrice" runat="server"></asp:Label>).
                Click the REGENERATE AD button to update.</span>
        </asp:Panel>

        <div id="AdQuality">
        
            <h3>Needs Action</h3>

            <div id="AdConfigForm" class="clearfix">

                <div id="QuickPackages" class="clearfix" runat="server" clientidmode="Static">

                    <h4 class="first accordionHead"><a href="#"><span id="QuickPackagesHeader" runat="server">Packages</span><span class="complete"></span></a></h4>
                    <div class="clearfix accordion">

                        <asp:UpdatePanel ID="OptionPackagesListUpdatePanel" runat="server">
                            <ContentTemplate>
                                <cwc:QuickPackages ID="quickPackages1" runat="server"></cwc:QuickPackages>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <span class="link small editLink dialogLink" onclick="_gaq.push(['_trackEvent', 'Quick Packages', 'Click', 'View Available Packages']);">View Available Packages</span>

                    </div>
                </div>
                        
                <div id="BookValues" class="clearfix" runat="server" clientidmode="Static">

                    <h4 class="accordionHead"><a href="#" ><span id="BookValuesHeader" runat="server">Book Value</span><span class="complete"></span></a></h4>

                    <div class="clearfix accordion" id="BookValueAccordionDiv" runat="server">

                        <div class="source table-layout book-value" id="BookoutPrimary" runat="server">
                            <asp:HiddenField ID="BookoutCategoryHidden" runat="server" ClientIDMode="Static" />
                            <span id="BookProviderLogo" class="bookProvider" runat="server" clientidmode="Static">&nbsp;</span>
                            <asp:HyperLink ID="BookOutLink" runat="server" clientidmode="Static" CssClass="link dialogLink" onclick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Bookout Link']);">Book Out Vehicle ...</asp:HyperLink>
                            <span id="BookValueSpan" class="bookValue" runat="server" clientidmode="Static" visible="true"></span>
                            <a id="BookAttentionLink" runat="server" clientidmode="Static" class="warning" visible="false" onclick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Bookout Attention Link']);">Update Book Value</a>
                        </div>
                        
                        <div class="source table-layout book-value non-primary-book" id="BookoutSecondary" runat="server" >
                            <asp:HiddenField ID="BookoutSecondaryCategoryHidden" runat="server" ClientIDMode="Static" />
                            <span id="BookSecondaryProviderLogo" class="bookProvider" runat="server" clientidmode="Static">&nbsp;</span>
                            <asp:HyperLink ID="BookOutSecondaryLink" runat="server" clientidmode="Static" CssClass="link dialogLink" onclick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Bookout Link']);">Book Out Vehicle ...</asp:HyperLink>
                            <span id="BookSecondaryValueSpan" class="bookValue" runat="server" clientidmode="Static" visible="true"></span>
                            <a id="BookSecondaryAttentionLink" runat="server" clientidmode="Static" class="warning" visible="false" onclick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Bookout Attention Link']);">Update Book Value</a>
                        </div>
                        <div class="source table-layout book-value non-primary-book" id="KbbConsumer" runat="server">
                            <asp:HiddenField ID="KbbConsumerHidden" runat="server" ClientIDMode="Static" />
                            <span id="KbbConsumerLogo" class="bookProvider" runat="server" clientidmode="Static">&nbsp;</span>
                            <asp:HyperLink ID="KbbConsumerBookoutLink" runat="server" clientidmode="Static" CssClass="link dialogLink" onclick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Bookout Link']);">Book Out Vehicle ...</asp:HyperLink>
                            <span id="KbbConsumerValueSpan" class="bookValue" runat="server" clientidmode="Static" visible="true"></span>
                            <a id="KbbConsumerAttentionLink" runat="server" clientidmode="Static" class="warning" visible="false" onclick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Bookout Attention Link']);">Update Book Value</a>
                        </div>

                        <div class="source table-layout book-value non-primary-book" id="BookoutTmv" runat="server">
                            <asp:HiddenField ID="BookoutTmvCategoryHidden" runat="server" ClientIDMode="Static" />
                            <span id="BookTmvProviderLogo" class="bookProvider" runat="server" clientidmode="Static">&nbsp;</span>
                            <asp:HyperLink ID="BookOutTmvLink" runat="server" clientidmode="Static" CssClass="link dialogLink" onclick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Bookout Link']);">Book Out Vehicle ...</asp:HyperLink>
                            <span id="BookTmvValueSpan" class="bookValue" runat="server" clientidmode="Static" visible="true"></span>
                            <a id="BookTmvAttentionLink" runat="server" clientidmode="Static" class="warning" visible="false" onclick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Bookout Attention Link']);">Update Book Value</a>
                        </div>
                         
                    </div>

                </div>

                <div id="VehicleHistory" class="clearfix attention" runat="server" clientidmode="Static">

                    <h4 class="accordionHead"><a href="#"><span id="VehicleHistoryReportHeader" runat="server">Carfax</span><span class="complete"></span></a></h4>

                    <div class="clearfix accordion">

                        <Controls:VehicleHistoryReport ID="VehicleHistoryReportControl" runat="server"/>

                    </div>

                </div>

                <div id="KeyInformation" class="clearfix" runat="server" clientidmode="Static">

                    <h4 class="accordionHead"><a href="#"><span id="KeyInformationHeader" runat="server">Key Information</span><span class="complete"></span></a></h4>

                    <div class="clearfix accordion">
                        <asp:CheckBoxList ID="AdditionalInfoItemsList" runat="server" ClientIDMode="Static" DataSourceID="AdditionalInfoItems" RepeatLayout="Table" RepeatDirection="Horizontal"
                            OnDataBound="AdditionalInfo_Bound" DataTextField="QuestionText" DataValueField="IdAndAdText" RepeatColumns="2" OnSelectedIndexChanged="AdditionalInfoItemsChanged"
                            AutoPostBack="false" />
                    </div>

                </div>

                <div id="DropDownLists" class="clearfix" runat="server" clientidmode="Static">

                    <h4 class="accordionHead"><a href="#"><span id="ColorsHeader" runat="server">Colors</span><span class="complete"></span></a></h4>

                    <div class="clearfix accordion">

                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <label id="ExteriorColorSelectionLabel" for="ExteriorColorDropDown">Exterior</label>
                                <asp:DropDownList ID="ExteriorColorDropDown" runat="server"  ClientIDMode="Static" DataSourceID="ExtColorsDS" DataTextField="ItemDescription" DataValueField="CodesAndDescriptions"
                                    CssClass="itemSelection" OnSelectedIndexChanged="ExteriorColorChanged" OnDataBound="ExtColor_Bound" AutoPostBack="false" onchange="_gaq.push(['_trackEvent', 'Approval Page', 'Change', 'Exterior Color']);" />
                                <label id="InteriorColorSelectionLabel" for="InteriorColorDropDown">Interior</label>
                                <asp:DropDownList ID="InteriorColorDropDown" runat="server" ClientIDMode="Static" DataSourceID="IntColorsDS" DataTextField="Description" DataValueField="ColorCode"
                                    CssClass="itemSelection" OnSelectedIndexChanged="InteriorColorChanged" OnDataBound="IntColorBound" AutoPostBack="false" onchange="_gaq.push(['_trackEvent', 'Approval Page', 'Change', 'Interior Color']);" />
                                    
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>

                </div>

                <div id="DropDownLists2" class="clearfix" runat="server" clientidmode="Static">

                    <h4 class="accordionHead"><a href="#"><span id="ConditionHeader" runat="server">Condition</span><span class="complete"></span></a></h4>
                            
                    <div class="clearfix accordion last">

                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="ConditionDDLLabel" AssociatedControlID="ConditionDropDown" Text="Condition:" runat="server" />
                                <asp:DropDownList ID="ConditionDropDown" runat="server" DataSourceID="ConditionAdjectivesDS" DataTextField="conditionDescription"
                                    DataValueField="conditionDescription" CssClass="itemSelection" OnSelectedIndexChanged="ConditionChanged" OnDataBound="ConditionAdjectives_Bound"
                                    AutoPostBack="false" onchange="_gaq.push(['_trackEvent', 'Approval Page', 'Change', 'Condition']);" />
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>

                </div>

            </div>

        </div>
    
        <div id="AdPanel">
        
            <div class="sectionHeader">
            
                <div id="FrameworkSelector">
                    <label for="TemplateDDL">Framework</label>
                    <asp:DropDownList runat="server" ID="FrameworkSelectorDDL" ClientIDMode="Static" DataTextField="Value" DataValueField="Key" DataSourceID="TemplateDS" OnDataBound="TemplateDDL_Bound" onchange="_gaq.push(['_trackEvent', 'Approval Page', 'Change', 'Framework Template']);">
                    </asp:DropDownList>
                </div>

                <asp:Button ID="RewriteAdButton" ClientIDMode="Static" OnClick="btn_RewriteFormat_OnClick" Text="REGENERATE AD" runat="server" OnClientClick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Regenerate Ad']);" />

            </div>
            
            <div id="Advertisement">
            
                <div id="AdModulesHeader" class="clearfix">
                
                    <span class="helper">Mouseover a section to edit or delete</span>
                    <a id="AddModuleLink" title="Add a section to this ad" onclick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Add module']);">Add Section</a>
                    
                    <div id="AdPreviewHighlighter">
                        <label>Highlight Preview Length</label>
                        <input type="radio" id="Preview150RadioButton" name="highlightPreview" value="150" onclick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Highlight Preview (150)']);" /><label for="Preview150RadioButton">150</label>
                        <input type="radio" id="Preview250RadioButton" name="highlightPreview" value="250" onclick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Highlight Preview (250)']);" /><label for="Preview250RadioButton">250</label>
                    </div>
                    
                </div>
                
                <div id="AdTemplate" class="clearfix"></div>
                
                <div class="adFooter" id="AdFooterPanel" runat="server">
                    <asp:Literal ID="OptimalFooterLiteral" runat="server" />
                </div>
                
                <%--TODO: Remove this--%>
                <div id="edt" class="clearfix" style="display: none">
                    <p>
                        <strong>Ad Destinations:</strong> <span>Publishing to:
                            <asp:Repeater ID="EdtRptr" runat="server" DataSourceID="edtSettingsDS" OnItemDataBound="EdtItem_DataBound">
                                <ItemTemplate>
                                    <em><%# (Container.ItemIndex+1) + ") " + Eval("Description") %></em>
                                    <vda:CheckBoxPlus runat="server" ID="ItemCB" Checked="true" CheckBoxArgument='<%# Eval("DestinationID") %>' Text='<%# Eval("Description") %>'
                                        Visible="false" />
                                </ItemTemplate>
                            </asp:Repeater>
                        </span>
                    </p>
                </div>
            
            </div>

        </div>
        
        <div id="OptionsPanel" class="clearfix">
            <div id="AdPanelFooterTools">
                <span class="dateCreated">Created:
                    <asp:Label ID="CreatedOn" runat="server"></asp:Label></span>
                    <a class="calendar" title="Scheduler" href="javascript:toggleScheduler(true, $get('options'), $get('photos'));" onclick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Scheduler Link']);">
                        Schedule... </a>
                <div id="Scheduler" class="DateSelect" style="display: none;">
                    <h4>
                        Release Date and Time:
                        <asp:DropDownList ID="ReleaseTimeDDL" runat="server" onchange="_gaq.push(['_trackEvent', 'Approval Page', 'Change', 'Schedule Time']);">
                            <asp:ListItem Text="1:00 AM" Value="1"></asp:ListItem>
                            <asp:ListItem Text="2:00 AM" Value="2"></asp:ListItem>
                            <asp:ListItem Text="3:00 AM" Value="3"></asp:ListItem>
                            <asp:ListItem Text="4:00 AM" Value="4"></asp:ListItem>
                            <asp:ListItem Text="5:00 AM" Value="5"></asp:ListItem>
                            <asp:ListItem Text="6:00 AM" Value="6"></asp:ListItem>
                            <asp:ListItem Text="7:00 AM" Value="7"></asp:ListItem>
                            <asp:ListItem Text="8:00 AM" Value="8"></asp:ListItem>
                            <asp:ListItem Text="9:00 AM" Value="9"></asp:ListItem>
                            <asp:ListItem Text="10:00 AM" Value="10"></asp:ListItem>
                            <asp:ListItem Text="11:00 AM" Value="11"></asp:ListItem>
                            <asp:ListItem Text="12:00 PM" Value="12"></asp:ListItem>
                            <asp:ListItem Text="1:00 PM" Value="13"></asp:ListItem>
                            <asp:ListItem Text="2:00 PM" Value="14"></asp:ListItem>
                            <asp:ListItem Text="3:00 PM" Value="15"></asp:ListItem>
                            <asp:ListItem Text="4:00 PM" Value="16"></asp:ListItem>
                            <asp:ListItem Text="5:00 PM" Value="17"></asp:ListItem>
                            <asp:ListItem Text="6:00 PM" Value="18"></asp:ListItem>
                            <asp:ListItem Text="7:00 PM" Value="19"></asp:ListItem>
                            <asp:ListItem Text="8:00 PM" Value="20"></asp:ListItem>
                            <asp:ListItem Text="9:00 PM" Value="21"></asp:ListItem>
                            <asp:ListItem Text="10:00 PM" Value="22"></asp:ListItem>
                            <asp:ListItem Text="11:00 PM" Value="23"></asp:ListItem>
                            <asp:ListItem Text="12:00 AM" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                    </h4>
                    <asp:Calendar ID="cal1" CssClass="ReleaseCalendar" runat="server" BackColor="White" BorderColor="#999999" CellPadding="4"
                        DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="100px" Width="150px">
                        <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                        <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                        <SelectorStyle BackColor="#CCCCCC" />
                        <WeekendDayStyle BackColor="#FFFFCC" />
                        <OtherMonthDayStyle ForeColor="#808080" />
                        <NextPrevStyle VerticalAlign="Bottom" />
                        <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                        <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                    </asp:Calendar>
                </div>
                <div id="expiresDiv" style="display: inline">
                    Expires:
                    <asp:TextBox ID="ExpiresOn" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="ExpiresOn" Display="Dynamic" runat="server" ErrorMessage="RegularExpressionValidator"
                        Text="(valid date required - mm/dd/yyyy)" ValidationExpression="^(((0?[1-9]|1[012])/(0?[1-9]|1\d|2[0-8])|(0?[13456789]|1[012])/(29|30)|(0?[13578]|1[02])/31)/(19|[2-9]\d)\d{2}|0?2/29/((19|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(([2468][048]|[3579][26])00)))$"></asp:RegularExpressionValidator>
                </div>
                <span>
                    <asp:DropDownList ID="SpecialDDL" DataValueField="Id" DataTextField="Title" DataSourceID="MatchingSpecialsDS" runat="server"
                        OnSelectedIndexChanged="Special_Changed" OnDataBound="SpecialDDL_Bound" onchange="_gaq.push(['_trackEvent', 'Approval Page', 'Change', 'Specials']);" />
                </span>
            </div>
            <div id="options" class="clearfix exp">
                <div class="CollapsiblePanel">
                    <h3 class="closed">
                        <asp:Label ID="lblConditionReport" runat="server" Text="Options & Standard Equipment" />
                    </h3>
                </div>
                <asp:Panel ID="ops" runat="server" ClientIDMode="Static">
                    <div id="optionList">
                        <asp:Repeater ID="OptionsRepeater" runat="server" DataSource='<%# GetOptionsDs() %>'>
                            <ItemTemplate>
                                <div class="optionset">
                                    <h4><%# Eval("headerText") %></h4>
                                    <ul>
                                        <asp:Repeater runat="server" ID="InnerRepeater" DataSource='<%# Container.DataItem %>'>
                                            <ItemTemplate>
                                                <li>
                                                    <%# Eval("Description") %></li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div id="mapped">
                        <asp:LinkButton ID="Remap" CssClass="Remap" runat="server" OnClick="Remap_Click" Text="Map Equipment" OnClientClick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Map Equipment Link']);"></asp:LinkButton>
                        <h3>Data From Lot Provider</h3>
                        <div class="LotDetails">
                            <asp:FormView ID="LotDataFV" runat="server" DataSourceID="LotDataDS">
                                <ItemTemplate>
                                    <%# Eval("ModelYear") %>
                                    &nbsp;<%# Eval("Make") %>&nbsp;<%# Eval("Model") %><span>Trim:</span><%# NullProof(Eval("Trim")) %><span>Milage:</span><%# NullProof(String.Format("{0:n0}", Eval("Mileage"))) %><span>List
                                        Price:</span><%# NullProof(String.Format("{0:c0}", Eval("ListPrice"))) %>
                                </ItemTemplate>
                            </asp:FormView>
                        </div>
                        <asp:CheckBoxList CssClass="ListedOptions" ID="ListingOpts" runat="server" DataSourceID="MappedDS" DataTextField="description"
                            RepeatColumns="5" RepeatLayout="Table" RepeatDirection="Vertical" OnDataBound="Mapped_Bound" />
                    </div>
                </asp:Panel>
            </div>
            <asp:SqlDataSource ID="MappedDS" runat="server" ConnectionString='<%$ ConnectionStrings:Merchandising %>' SelectCommandType="StoredProcedure"
                SelectCommand="builder.VehicleOptionsMapped#Fetch" OnSelecting="MappedOpts_Selecting">
                <SelectParameters>
                    <asp:Parameter Name="BusinessUnitId" Type="Int32" />
                    <asp:Parameter Name="InventoryId" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="LotDataDS" runat="server" ConnectionString='<%$ ConnectionStrings:Merchandising %>' SelectCommandType="StoredProcedure"
                SelectCommand="builder.LotData#Fetch" OnSelecting="MappedOpts_Selecting">
                <SelectParameters>
                    <asp:Parameter Name="BusinessUnitId" Type="Int32" />
                    <asp:Parameter Name="InventoryId" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
        </div>
        
        <div id="AdApproval" class="clearfix">
                    
            <h4>Approval Terms &amp; Conditions</h4>
            <p><asp:Label ID="ApprovalAgreement" runat="server" Text=""></asp:Label></p>
            <asp:Button ID="approveButton" runat="server" ClientIDMode="Static" OnClick="ApproveVehicle_Click" OnClientClick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Approve Ad Button']);" />

        </div>

    </div>
    <asp:ObjectDataSource runat="server" ID="TrimStyleDS" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.ChromeStyle"
        OnSelecting="StylePieces_Selecting" OnSelected="StylePieces_Selected" SelectMethod="ChromeTrimStyleNamesSelect">
        <SelectParameters>
            <asp:Parameter Name="vin" Type="String" DefaultValue='' />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="AdditionalInfoItems" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.VehicleConfigurationDataSource"
        SelectMethod="GetAdditionalInfoItems" OnSelecting="AdditionalInfo_Selecting">
        <SelectParameters>
            <asp:Parameter Name="BusinessUnitId" Type="Int32" DefaultValue="100150" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:SqlDataSource ID="ConditionAdjectivesDS" runat="server" SelectCommandType="StoredProcedure" ConnectionString='<%$ ConnectionStrings:Merchandising %>'
        SelectCommand="builder.getConditionAdjectives" OnSelecting="Conditions_Selecting">
        <SelectParameters>
            <asp:Parameter Name="BusinessUnitId" DefaultValue="-1" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="EdtPostingDS" runat="server" ConnectionString='<%$ ConnectionStrings:Merchandising %>' SelectCommand="postings.getVehicleEdtSettings"
        SelectCommandType="StoredProcedure" OnSelecting="EdtPostings_Selecting">
        <SelectParameters>
            <asp:Parameter Name="BusinessUnitId" DefaultValue="-1" Type="Int32" />
            <asp:Parameter Name="InventoryId" DefaultValue="-1" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:ObjectDataSource ID="edtSettingsDS" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.Inventory.InventoryDataSource"
        SelectMethod="EdtDestinationsSelect" OnSelecting="EdtList_Selecting">
        <SelectParameters>
            <asp:Parameter Name="businessUnitID" Type="Int32" DefaultValue="100150" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource runat="server" ID="MatchingSpecialsDS" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.SpecialsCollection"
        SelectMethod="FetchValidSpecials" OnSelecting="MatchingSpecials_Selecting">
        <SelectParameters>
            <asp:Parameter Name="OwnerEntityId" Type="Int32" />
            <asp:Parameter Name="OwnerEntityTypeId" DefaultValue="1" Type="Int32" />
            <asp:Parameter Name="inventoryData" Type="Object" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:SqlDataSource runat="server" ID="NotExpiredSpecialsDS" ConnectionString="<%$ ConnectionStrings:Merchandising %>" SelectCommand="builder.FinancingSpecials#Fetch"
        SelectCommandType="StoredProcedure" OnSelecting="Specials_Selecting">
        <SelectParameters>
            <asp:Parameter Name="OwnerEntityId" Type="Int32" />
            <asp:Parameter Name="OwnerEntityTypeId" DefaultValue="1" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:ObjectDataSource runat="server" ID="ExtColorsDS" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.AvailableChromeColors"
        SelectMethod="FetchExterior" OnSelecting="ChromeColorsSelecting">
        <SelectParameters>
            <asp:Parameter Name="chromeStyleId" Type="Int32" />
            <asp:Parameter Name="currentExtColorDesc1" Type="String" />
            <asp:Parameter Name="currentExtColorDesc2" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource runat="server" ID="IntColorsDS" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.AvailableChromeColors"
        SelectMethod="FetchInterior" OnSelecting="ChromeInteriorColorsSelecting">
        <SelectParameters>
            <asp:Parameter Name="chromeStyleId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource runat="server" ID="PricingDS" TypeName="FirstLook.Merchandising.DomainModel.DataSource.PricingDataSource"
        SelectMethod="FetchBasicPricing" OnSelecting="Pricing_Selecting">
        <SelectParameters>
            <asp:Parameter Name="businessUnitId" Type="Int32" />
            <asp:Parameter Name="inventoryId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource runat="server" ID="TemplateDS" TypeName="FirstLook.Merchandising.DomainModel.Templating.TemplateFilter"
        SelectMethod="SelectTemplates" OnSelecting="Templates_Selecting">
        <SelectParameters>
            <asp:Parameter Name="businessUnitId" Type="Int32" />
            <asp:Parameter Name="inventoryData" Type="Object" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <div id="AddModuleFormDiv" style="display: none;">
        <fieldset>
            <label for="AddModuleHeaderText">
                Header</label>
            <input type="text" id="AddModuleHeaderText" />
            <label for="AddModuleParagraphText">
                Text</label>
            <textarea id="AddModuleParagraphText"></textarea>
        </fieldset>
    </div>
    <asp:Panel ID="ChoicePanel" runat="server" Visible="false">
        <div class="choiceForward">
            <asp:HiddenField ID="iaaAdId" runat="server" />
            <h2>
                Your description was edited in the Accelerator<br />
                Which description would you like to use?</h2>
            <h3>
                <asp:Label CssClass="EditTime" ID="ImmEdit" runat="server"></asp:Label>
                <asp:Button ID="Button2" runat="server" Text="Select" OnClick="ChooseDesc_Click" CommandArgument="IMM" OnClientClick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Choose Description (IMM)']);" />
                Internet Merchandising Manager Description
            </h3>
            <asp:Label ID="IMMDesc" runat="server"></asp:Label>
            <asp:Label ID="IMMFoot" runat="server"></asp:Label>
            <h3>
                <asp:Label CssClass="EditTime" ID="IaaEdit" runat="server"></asp:Label>
                <asp:Button ID="chooseIaa" runat="server" Text="Select" OnClick="ChooseDesc_Click" CommandArgument="IAA" OnClientClick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Choose Description (IAA)']);" />
                Internet Advertising Accelerator Description
            </h3>
            <asp:Label ID="IAADesc" runat="server"></asp:Label>
            <asp:Label ID="IAAFoot" runat="server"></asp:Label>
        </div>
    </asp:Panel>
    <asp:Panel ID="ApprovalModalPanel" CssClass="clearfix" runat="server" Visible="false">
        <div id="divApproveSuccessful" class="clearfix">
            <asp:Label ID="lblApproveSuccessfulMessage" runat="server" Text="<h1>This Optimized Ad has been approved.</h1><p>Would you like to move to the next vehicle?</p>"></asp:Label>
            <asp:HyperLink ID="btnApproveSuccessfulYes" runat="server" onclick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Approved: Move to Next (Yes)']);">Yes</asp:HyperLink>
            <asp:HyperLink ID="btnApproveSuccessfulNo" runat="server" onclick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Approved: Move to Next (No)']);">No</asp:HyperLink>
        </div>
    </asp:Panel>
    <div id="OptionPackagesModal" clientidmode="Static" runat="server" style="display: none;">
        <asp:UpdatePanel ID="OptionsModalUpdatePanel" runat="server">
            <ContentTemplate>
                <pkg:Packages ID="OptionPackagesControl" runat="server" InventoryDataViewID="invDataViewer" ShowTrimSelector="false" WorkflowSelectorID="wfsInventoryItem" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>


</asp:Content>

<asp:Content ID="ScriptContent" ContentPlaceHolderID="ScriptsPlaceHolder" runat="server">

    <% if(HtmlHelpers.IsDebugBuild()) { %>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/MAX.Utils.debug.js") %>"></script>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Max/MAX.UI.Global.debug.js") %>"></script>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Workflow/Workflow.debug.js") %>"></script>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Workflow/ApprovalSummary.debug.js") %>"></script>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Workflow/AdvertisementPanel.debug.js") %>"></script>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Workflow/LoadPackages.debug.js") %>"></script>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Lib/jquery.remodal.js") %>"></script>
    <% } else { %>
        <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Workflow/ApprovalSummary.Combined.js") %>"></script>
    <% } %>

    <script id="ModuleTemplate" type="text/x-jquery-tmpl">
        {{each Modules}}
            <div class="module clearfix ${Type === 'Preview' ? 'adPreview' : ''}" id="Module${ $index }">
                <div class="moduleLinks">
                    {{if Type !== "Preview"}}
                        <a class="deleteModuleLink" id="Module${ $index }DeleteLink" title="Delete this section" onclick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Delete Module']);"></a>
                    {{/if}}
                    <a class="editModuleLink" id="Module${ $index }EditLink" title="Edit this section" onclick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Edit Module']);"></a>
                </div>
                <div class="moduleContent">
                    {{if Header.Text !== ""}} <span class="header">${Header.Text}</span>
                        {{if Type !== "Preview"}}
                            <input type="text" module="${$index}" onkeydown="setTimeout(AdPanel.LockEditedBlurb,0)" oninput="AdPanel.LockEditedBlurb( event )" onpropertychange="AdPanel.LockEditedBlurb( event )" class="text header ${Header.Edited === 'false' ? '' : 'locked' }" maxlength="80" id="Module${ $index }HeaderTextbox" name="Module${ $index }HeaderTextbox" value="${Header.Text}" style="display:none;" />
                            {{if Header.Edited === "true"}}
                                <a class="unlock header" style="display:none;" module="${$index}" blurb="header" onclick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', this.className]);"></a>
                            {{else}}
                                <a class="lock header" style="display:none;" module="${$index}" blurb="header" onclick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', this.className]);"></a>
                            {{/if}}
                        {{/if}}
                    {{/if}}
                    {{tmpl(Paragraph, { index: $index }) "#ModuleItemsTemplate"}}
                </div>
                <div class="buttons clearfix">
                    {{if Type === "Preview"}}
                        <span id="PreviewLengthWarning"></span>
                    {{/if}}
                    <input type="button" class="button small cancel" value="Cancel" id="EditModule${ $index }CancelButton" style="display:none;" onclick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Cancel Module Edit']);" />
                    <input type="button" class="button small done" value="Done" id="EditModule${ $index }DoneButton" style="display:none;" onclick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', 'Confirm Module Edit (Done)']);" />
                </div>
            </div>
        {{/each}}
    </script>
    <script id="ModuleItemsTemplate" type="text/x-jquery-tmpl">
        {{if Text !== ""}}
            <span class="paragraph blurb ${Type === 'Item' ? 'notPrice' : 'price' }" id="Module${$item.index}Blurb${$index}">${Text}</span>
            
            <div class="blurbEditor clearfix" style="display:none;clear:both;">
                {{if $item.index === 0}}
                    <input type="text" onkeydown="AdPanel.MonitorPreviewLength( event );AdPanel.LockEditedBlurb( event )" oninput="AdPanel.MonitorPreviewLength( event );AdPanel.LockEditedBlurb( event )" onpropertychange="AdPanel.MonitorPreviewLength( event );AdPanel.LockEditedBlurb( event )" module="${$item.index}" blurb="${$index}" class="text blurb ${Type === 'Item' ? 'notPrice' : 'price' } ${Edited === 'false' ? '' : 'locked' }" value="${Text}" id="Module${$item.index}Blurb${ $index }Textbox" name="Module${$item.index}Blurb${ $index }Textbox" />
                {{else}}
                    <input type="text" onkeydown="AdPanel.LockEditedBlurb( event )"                                       oninput="AdPanel.LockEditedBlurb( event )"                                       onpropertychange="AdPanel.LockEditedBlurb( event )"                                       module="${$item.index}" blurb="${$index}" class="text blurb ${Type === 'Item' ? 'notPrice' : 'price' } ${Edited === 'false' ? '' : 'locked' }" value="${Text}" id="Module${$item.index}Blurb${ $index }Textbox" name="Module${$item.index}Blurb${ $index }Textbox" />
                {{/if}}
                {{if Type !== "PriceItem"}}
                    {{if Edited === "false"}}
                        <a href="#" class="lock" module="${$item.index}" blurb="${$index}" onclick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', this.className + ' Blurb']);"></a>
                    {{else}}
                        <a href="#" class="unlock" module="${$item.index}" blurb="${$index}" onclick="_gaq.push(['_trackEvent', 'Approval Page', 'Click', this.className + ' Blurb']);"></a>
                    {{/if}}
                {{/if}}
            </div>
            
        {{/if}}
    </script>
    <script id="PreviousAdTemplate" type="text/x-jquery-tmpl">
        <div id="PreviousAdContainer">
            {{each Modules}}
                <div class="module clearfix">
                    {{if Header.Text !== ""}} <div class="header">${Header.Text}</div> {{/if}}
                    {{tmpl(Paragraph, { index: $index }) "#ModuleItemsTemplate"}}
                </div>
            {{/each}}
        </div>
    </script>

</asp:Content>