﻿using System.Web.Mvc;
using log4net;

namespace Wanamaker.WebApp.Areas.Dashboard.Filters
{
    public class HandleMvcExceptionAttribute : ActionFilterAttribute
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(HandleMvcExceptionAttribute).FullName);

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.Exception != null)
            {

                Log.ErrorFormat("Exception thrown for an MVC request. {5} Url : {4}, Action : {0}, Controller : {1}, {5} Message : {2}, StackTrace : {3}",
                filterContext.Controller, filterContext.ActionDescriptor, filterContext.Exception.Message, filterContext.Exception.StackTrace, filterContext.RequestContext.HttpContext.Request.Url, System.Environment.NewLine);

                filterContext.HttpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;

                //We still want to bubble the error up so the emails go out to the stack trace alert.
                filterContext.ExceptionHandled = false;
            }

        }
    }
}