﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Wanamaker.WebApp.Areas.Dashboard.Filters
{
    //Exception message: Error during serialization or deserialization using the JSON JavaScriptSerializer. The length of the string exceeds the value set on the maxJsonLength property.
    //the setting in the config <jsonSerialization maxJsonLength is not used in the controller Json method
    //We are not using MVC 4.0 either so we can't override 
    // protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
    //and set the  MaxJsonLength property on json result.
    public class BigJsonResult : ContentResult
    {
        public BigJsonResult(object data)
        {
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;

            Content = serializer.Serialize(data);
            ContentType = "application/json";
        }
    }
}