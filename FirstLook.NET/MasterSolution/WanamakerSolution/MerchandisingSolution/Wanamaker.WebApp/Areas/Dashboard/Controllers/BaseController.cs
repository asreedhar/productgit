﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Dashboard;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.Dashboard.Concrete;
using FirstLook.Merchandising.DomainModel.Reports.DashboardHelpers;
using FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Workflow;
using MAX.Entities;
using MAX.Entities.Enumerations;
using MAX.Entities.Helpers.DashboardHelpers;
using Merchandising.Messages;
using MvcMiniProfiler;
using FirstLook.Common.Core.IOC;

namespace Wanamaker.WebApp.Areas.Dashboard.Controllers
{
    public class BaseController : Controller
    {
        protected  ProfileCommon UserProfile = new ProfileCommon();
        public IPhotoServices PhotoService { get; set; }

        public const string InventoryUrl = "~/Workflow/Inventory.aspx";

        public BaseController()
        {
            ViewBag.CustomMessage = CustomMessage.Empty;
            ViewBag.ViewingGroupDashboard = false;
            ViewBag.IsOemBranded = false;
        }

        public bool IsMax30
        {
            get { return GetMiscSettings.GetSettings( BusinessUnit.BusinessUnitId ).MaxVersion == 3; }
        }

        public bool IsOemBranded
        {
            get
            {
                return GetMiscSettings.GetSettings(BusinessUnit.BusinessUnitId).Franchise != Franchise.None;
            }
        }

        public bool IsMaxStandAlone
        {
            get
            {
                var standaloneurl = ConfigurationManager.AppSettings["max_standalone_url"];
                if (standaloneurl == null) return false;
                if (Request == null) return false;
                return Request.Url != null  && Request.Url.ToString().ToLower().IndexOf(standaloneurl.ToLower()) > 0;

            }
        }

        public bool IsReportViewer
        {
            get { return Helper.IsReportViewer; }
        }

        private readonly Lazy<bool> IsGroupDashboardEnabledLazy =
            new Lazy<bool>(() => GetMiscSettings.GetSettings(WorkflowState.BusinessUnitID).ShowGroupLevelDashboard);

        private bool IsGroupDashboardEnabled 
        { 
            get 
            {
                if (HttpContext == null)
                    return true;

                return IsGroupDashboardEnabledLazy.Value; 
            } 
        }
        
        // Use this enum to indicate manufacturers that have css/images setup
        public enum OemBrandingSupport
        {
            BMW,
            Buick,
            Cadillac,
            Chevrolet,
            Chrysler,
            Ford,
            GMC,
            Honda
        }

        public virtual void SetupViewBag ()
        {
            ViewBag.BusinessUnit = BusinessUnit;
            ViewBag.IsAdministrator = IsAdministrator;
            ViewBag.IsReportViewer = IsReportViewer;
            ViewBag.BulkUploadUrl = GetBulkUploadUrl();
            ViewBag.IsMaxStandAlone = IsMaxStandAlone;
            ViewBag.IsGldEnabled = IsGroupDashboardEnabled;
            ViewBag.showOnlineClassifiedOverview = GetMiscSettings.GetSettings(WorkflowState.BusinessUnitID).showOnlineClassifiedOverview; // FB: 28329 - disable performance summary option
            ViewBag.IsOemBranded = IsOemBranded;
        }

        public string GetBulkUploadUrl()
        {
            if (PhotoService == null) PhotoService = Registry.Resolve<IPhotoServices>();
            return PhotoService.GetBulkUploadManagerUrl(BusinessUnit.BusinessUnitId, PhotoManagerContext.MAX);
        }

        public IDashboardBusinessUnit BusinessUnit
        {
            get
            {
                var businessUnit = this.HttpContext == null ? null : this.HttpContext.Items["BusinessUnit"] as BusinessUnit;

                if (businessUnit != null)
                    return new DashboardBusinessUnit() {BusinessUnitId = businessUnit.Id, Code = businessUnit.BusinessUnitCode, Name = businessUnit.Name};

                return null;
            }
        }


        public bool IsAdministrator
        {
            get { return Helper.IsAdministrator; }
        }

        public void SetMessage(string messageText, MessageType type)
        {
            //For requests between actions
            TempData["CustomMessage"] = new CustomMessage { MessageText = messageText, MessageType = type };

        }

        protected string GetVehicleTypeFromUserProfile()
        {
            var usedNew = UserProfile.NewUsed;
            VehicleType usedNewFilter = usedNew;
            return usedNewFilter.ToString();

        }

        //For the first 3 days of the month we use the last day of previous month as the end date.
        //This is because we might not get the data for a couple of days at the beginning of the month.
        //FB 19756
        public DateTime GetEndDate(IClock clock)
        {
            var endDate = clock.CurrentTime;
            if (endDate.Day > 3) return endDate;
            return endDate.AddDays(-1 * endDate.Day);
        }

        public DateTime GetStartOfMonth(DateTime date)
        {
            var startOfMonth = new DateTime(date.Year, date.Month, 1);
            return startOfMonth;
        }

        protected void HandleMessages()
        {
            if (TempData["CustomMessage"] != null)
            {
                ViewBag.CustomMessage = TempData["CustomMessage"] as CustomMessage;
                TempData.Remove("CustomMessage");
            }else
            {
                ViewBag.CustomMessage = CustomMessage.Empty;
            }
        }
    }
}