﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using FirstLook.Common.Core;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Dashboard;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.Dashboard.Entities;
using FirstLook.Merchandising.DomainModel.DataAccess;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics;
using FirstLook.Merchandising.DomainModel.MaxAnalytics;
using FirstLook.Merchandising.DomainModel.Properties;
using FirstLook.Merchandising.DomainModel.Reports;
using FirstLook.Merchandising.DomainModel.Reports.DashboardHelpers;
using FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using FirstLook.Merchandising.DomainModel.Workflow;
using log4net;
using MAX.Entities;
using MAX.Entities.Enumerations;
using MAX.Entities.Helpers.DashboardHelpers;
using MvcMiniProfiler;
using Newtonsoft.Json;
using Wanamaker.WebApp.AppCode;
using Wanamaker.WebApp.Areas.Dashboard.Filters;
using Wanamaker.WebApp.Areas.Results;
using Wanamaker.WebApp.Helpers;


namespace Wanamaker.WebApp.Areas.Dashboard.Controllers
{
    [HandleMvcException]
    public class HomeController : BaseController
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(HomeController).FullName);

        private readonly IDashboardManager _manager;
        private readonly IDashboardBusinessUnitRepository _businessUnitRepository;

        public HomeController(IDashboardManager manager, IDashboardBusinessUnitRepository businessUnitRepository)
        {
            _manager = manager;
            _businessUnitRepository = businessUnitRepository;
        }

        [CompressionFilter]
        [HttpGet]
        public ActionResult Index(string usedOrNewFilter)
        {
            using (MiniProfiler.Current.Step("Index"))
            {
                if (Request.FromRubyHomePage())
                {
                    UserProfile.NewUsed = VehicleType.Used.IntValue;
                    usedOrNewFilter = VehicleType.Used;
                }
                else if (string.IsNullOrWhiteSpace(usedOrNewFilter))
                {
                    usedOrNewFilter = GetVehicleTypeFromUserProfile();
                }

                SetupViewBag();
                
                ViewBag.HasMaxSE = _businessUnitRepository.HasDealerUpgrade(BusinessUnit.BusinessUnitId, (int)Upgrade.Marketing);

                var inventoryCounts = _manager.GetActiveInventoryCounts(BusinessUnit.BusinessUnitId);

                var miscSettings = GetMiscSettings.GetSettings(BusinessUnit.BusinessUnitId);
                var model = new DashboardModel
                {

                    SelectedUsedNewFilter = usedOrNewFilter,
                    BusinessUnit = BusinessUnit,
                    HasAnalyticsSuite = miscSettings.AnalyticsSuite,
                    UsedNewFilters = inventoryCounts.Select(x => new SelectListItem
                    {
                        Value =
                            (x.Key).ToString(),
                        Text =
                            string.Format("{0} ({1})", x.Key.ToString().Replace("Both", "Used & New"), x.Value)
                    }).OrderByDescending(x => x.Text)
                };

                //e.g., value = 0   text = Used(250) 

                model.SelectedFilterCount =
                    inventoryCounts.Single(x => x.Key == model.SelectedUsedNewFilter).Value;

                model.PerformanceData = new SitePerformanceData();

                UserProfile.NewUsed = model.SelectedUsedNewFilter;

                model.NotOnlineUrl = Helper.GetNotOnlineUrl(model.SelectedUsedNewFilter);

                HandleMessages();

                return View(model);
            }
        }

        [CompressionFilter]
        [HttpGet]
        public ActionResult IndexKO(string usedOrNewFilter)
        {
            using (MiniProfiler.Current.Step("Index"))
            {
                if (Request.FromRubyHomePage())
                {
                    UserProfile.NewUsed = VehicleType.Used.IntValue;
                    usedOrNewFilter = VehicleType.Used;
                }
                else if (string.IsNullOrWhiteSpace(usedOrNewFilter))
                {
                    usedOrNewFilter = GetVehicleTypeFromUserProfile();
                }

                SetupViewBag();
                ViewBag.CssLinks = new List<string> { Url.VersionedUrl( "~/Themes/MaxMvc/DashboardKO.debug.css") };
                ViewBag.JavaScripts = new List<string>();

                ViewBag.HasMaxSE = _businessUnitRepository.HasDealerUpgrade(BusinessUnit.BusinessUnitId, (int)Upgrade.Marketing);
                ViewBag.HasPricingSummary = DealerPingAccess.CheckDealerPingAccess(BusinessUnit.BusinessUnitId);
                var inventoryCounts = _manager.GetActiveInventoryCounts(BusinessUnit.BusinessUnitId);

                var miscSettings = GetMiscSettings.GetSettings(BusinessUnit.BusinessUnitId);
                var reportSettings = GetReportSettings.GetSettings(BusinessUnit.BusinessUnitId);
                var upgradeSettings = GetUpgradeSettings.GetSettings(BusinessUnit.BusinessUnitId);
                var model = new DashboardModel
                {

                    SelectedUsedNewFilter = usedOrNewFilter,
                    BusinessUnit = BusinessUnit,
                    HasAnalyticsSuite = miscSettings.AnalyticsSuite,
                    DashboardSettings = new DashboardSettingsModel
                    {
                        BusinessUnitId = BusinessUnit.BusinessUnitId,
                        HasAnalyticsSuite = miscSettings.AnalyticsSuite,
                        ShowTimeToMarket = miscSettings.ShowTimeToMarket,
                        ShowOnlineClassifiedOverview = miscSettings.showOnlineClassifiedOverview,
                        MerchandisingAlertSelectionBucket = reportSettings.MerchandisingGraphDefault.ToString(),
                        InventoryBaseUrl = Request.ApplicationPath + "/Workflow/Inventory.aspx",
                        HasDigitalShowroom = upgradeSettings.MAXDigitalShowroom,
                        HasWebsite20 = upgradeSettings.MAXForWebsite20,
                        HasShowroomReporting = upgradeSettings.EnableShowroomReports
                    },
                };

                //e.g., value = 0   text = Used(250) 
                model.UsedNewFilters = inventoryCounts.Select(x => new SelectListItem
                {
                    Value =
                        (x.Key).ToString(),
                    Text =
                        string.Format("{0} ({1})", x.Key.ToString().Replace("Both", "Used & New"), x.Value)
                }).OrderByDescending(x => x.Text);

                model.SelectedFilterCount =
                    inventoryCounts.Single(x => x.Key == model.SelectedUsedNewFilter).Value;

                model.PerformanceData = new SitePerformanceData();

                UserProfile.NewUsed = model.SelectedUsedNewFilter;

                model.NotOnlineUrl = Helper.GetNotOnlineUrl(model.SelectedUsedNewFilter);

                var bucket = MerchandisingGraphSettingsHelper.GetWorkflowType(reportSettings.MerchandisingGraphDefault);
                model.LowActivityReportUrl = Helper.GetBucketNavigationUrl(bucket, model.SelectedUsedNewFilter);

                HandleMessages();

                return View(model);
            }
        }

        [CompressionFilter]
        [HttpGet]
        public JsonResult GoogleAnalyticsDataByDateAndLimit(int businessUnitId, int gaProfileType, int month, int year, int endMonth, int endYear, int limit)
        {
            using (MiniProfiler.Current.Step("Google Analytics Data"))
            {
                var dateRange = new DateRange(year, month, endYear, endMonth);

                var data = new Object();
                
                try
                {
                    GoogleAnalyticsType gaType = (GoogleAnalyticsType)gaProfileType;
                    data = _manager.GetAllGoogleAnalyticsData(businessUnitId, gaType, dateRange);
                }
                catch(Exception ex)
                {
                    if (ex.InnerException != null && ex.InnerException.Message != null)
                    {
                        data = new { errorMessage = ex.Message, errorData = ex.InnerException.Message };
                    }
                    else
                    {
                        data = new { errorMessage = ex.Message };
                    }
                }

                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }

        [CompressionFilter]
        [HttpGet]
        public JsonResult GoogleAnalyticsDataByDateAndLimitUngrouped(int businessUnitId, int gaProfileType, int month, int year, int endMonth, int endYear, int limit)
        {
            using (MiniProfiler.Current.Step("Google Analytics Data (Ungrouped)"))
            {
                var dateRange = new DateRange(year, month, endYear, endMonth);

                Object data;
                
                try
                {
                    GoogleAnalyticsType gaType = (GoogleAnalyticsType)gaProfileType;
                    data = _manager.GetAllGoogleAnalyticsDataUngrouped(businessUnitId, gaType, dateRange, new[]{"Trends"});
                    var row_count = 0;
                    foreach( var r in (data as List<GoogleAnalyticsResult>) )
                    {
                        if( r != null && r.Referrals.ContainsKey("Trends"))
                            row_count += r.Referrals["Trends"].Rows.Count;
                    }
                    if( row_count == 0 )
                        data = null; // nothing of note to send back, so be clear about it
                }
                catch(Exception ex)
                {
                    if (ex.InnerException != null && ex.InnerException.Message != null)
                    {
                        data = new { errorMessage = ex.Message, errorData = ex.InnerException.Message };
                    }
                    else
                    {
                        data = new { errorMessage = ex.Message };
                    }
                }

                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }

        [CompressionFilter]
        [HttpGet]
        public JsonResult DashboardData(int businessUnitId, string usedNewFilter)
        {
            using (MiniProfiler.Current.Step("Dashboard Data"))
            {
                VehicleType vehicleType = usedNewFilter;
                
                var model = new DashboardModel
                {
                    BucketsData = _manager.GetBucketsData(businessUnitId, vehicleType),
                    NotOnlineUrl = Helper.GetNotOnlineUrl(vehicleType),
                };

                return Json(model, JsonRequestBehavior.AllowGet);
            }
        }

        [CompressionFilter]
        [HttpGet]
        public JsonResult TimeToMarketData(int businessUnitId, string usedNewFilter, int startMonth, int startYear, int endMonth, int endYear)
        {
            var dateRange = new DateRange(startYear, startMonth, endYear, endMonth);

            var data = _manager.GetTimeToMarket(businessUnitId, usedNewFilter, dateRange);

            for (int i = 0; i < data.Vehicles.Count; i++)
            {
                // hacking the date so all USA timezones show the same day :/
                // We should be setting these dates based on the dealership's timezones so it is relevant to them when we view their data
                // in UTC
                if (data.Vehicles[i].DateAddedToInventory != DateTime.MinValue)
                    data.Vehicles[i].DateAddedToInventory = data.Vehicles[i].DateAddedToInventory.AddHours(6);
                if (data.Vehicles[i].DateOfFirstPhoto != DateTime.MinValue)
                    data.Vehicles[i].DateOfFirstPhoto = data.Vehicles[i].DateOfFirstPhoto.AddHours(6);
                if (data.Vehicles[i].DateOfFirstCompleteAd != DateTime.MinValue)
                    data.Vehicles[i].DateOfFirstCompleteAd = data.Vehicles[i].DateOfFirstCompleteAd.AddHours(6);
            }

            var result = new JsonNetResult(); // properly serialize DateTime to a javascript friendly string
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = data;
            return result;
        }

        [CompressionFilter]
        [HttpGet]
        public JsonResult BucketsData(int businessUnitId, string usedNewFilter)
        {
            try
            {
                using (MiniProfiler.Current.Step("Dashboard Data"))
                {
                    var model = _manager.GetBucketsData(businessUnitId, usedNewFilter);

                    return Json(model, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Log.ErrorFormat(
                    "Exception thrown for an MVC request BucketsData.  BusinessUnitId : {0} , usedNewFilter : {1} , Message : {2}, StackTrace : {3}",
                    businessUnitId, usedNewFilter, ex.Message, ex.StackTrace);
                var data = new { errorMessage = ex.Message };

                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, ActionName("ChangeInventoryTypeFilter")]
        public ActionResult ClassicChangeInventoryTypeFilter(string selectedUsedNewFilter)
        {
            return RedirectToAction("Index", new { usedOrNewFilter = selectedUsedNewFilter });
        }

        [HttpGet]
        public ActionResult ChangeInventoryTypeFilter(string usedNewFilter)
        {
            UserProfile.NewUsed = (VehicleType) usedNewFilter;
            return Json(UserProfile.NewUsed, JsonRequestBehavior.AllowGet);
        }

        private SitePerformanceData GetSitePerformanceData(int businessUnitId, string usedNewFilter, DateTime startDate, DateTime endDate)
        {
            var sitePerformancesModel = _manager.GetSitePerformanceData(businessUnitId, usedNewFilter, startDate, endDate);

            return new SitePerformanceData
            {
                StartDate = startDate,
                EndDate = endDate,
                Sites = SitePerformanceMapper.Map(sitePerformancesModel)
            };
        }

        [CompressionFilter]
        [HttpGet]
        public JsonResult SitePerformanceDataForMonth(int businessUnitId, string usedNewFilter, int month, int year, int endMonth, int endYear)
        {
            var startDate = new DateTime(year, month, 1);
            var endDate = new DateTime(endYear, endMonth, 1).AddMonths(1).AddSeconds(-1); //Need to subtract 1 second here for db to not round to next month

            var data = GetSitePerformanceData(businessUnitId, usedNewFilter, startDate, endDate);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public RedirectResult SearchMaxAd(string searchText)
        {
            Session["SearchText"] = searchText;
            return Redirect(InventoryUrl);
        }

        [HttpGet]
        public ActionResult SearchMaxForSelling(string stockNumber)
        {
            var result = new SearchMaxSEResult();
            try
            {

                SetupViewBag();

                result.StatusMessage = new CustomMessage();

                var message = string.Empty;

                var url = _manager.GetMaxSeLink(BusinessUnit, stockNumber, ref message);

                if (url == null)
                {
                    result.StatusMessage.MessageType = MessageType.Error;
                    result.StatusMessage.MessageText = message;
                }
                else
                {
                    result.MaxSEUrl = url;
                    result.StatusMessage.MessageType = MessageType.Success;
                }
            }
            catch (Exception ex)
            {
                result.StatusMessage.MessageType = MessageType.Error;
                result.StatusMessage.MessageText = "Error in search : " + ex.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);

        }

        [CompressionFilter]
        [HttpGet]
        public ActionResult SitePerformanceTrend( int businessUnitId, string vehicleType, int startMonth, int startYear, int endMonth, int endYear )
        {
            var dateRange = new DateRange(startYear, startMonth, endYear, endMonth);
            var trendData = _manager.GetMonthlySiteTrends(businessUnitId, vehicleType, dateRange);

            var totals = new List<SitePerformance>();
            trendData.Values.ToList().ForEach(tdv =>
            {
                if (tdv.Any())
                {
                    var total = tdv.Summation();
                    totals.Add(total);
                }
            });

            var mappedTotals = new SitePerformanceData
            {
                StartDate = dateRange.StartDate,
                EndDate = dateRange.EndDate,
                Sites = SitePerformanceMapper.Map(totals)
            };
            
            foreach(var kvp in mappedTotals.Sites)
            {
                // Set the Months datapoint
                var monthCount = trendData[kvp.Key].Count();
                kvp.Value.Months = monthCount;

                // manually calculate the benchmark costs FB: 27552 
                if (! trendData[kvp.Key].TrueForAll(x => x.BenchMarkImpressionCost.HasValue.Equals(false)))
                    kvp.Value.BenchMarkImpressionCost = trendData[kvp.Key].Sum(x => x.BenchMarkImpressionCost) / trendData[kvp.Key].Count(x => x.BenchMarkImpressionCost.HasValue);

                if (!trendData[kvp.Key].TrueForAll(x => x.BenchMarkLeadCost.HasValue.Equals(false)))
                    kvp.Value.BenchMarkLeadCost = trendData[kvp.Key].Sum(x => x.BenchMarkLeadCost) / trendData[kvp.Key].Count(x => x.BenchMarkLeadCost.HasValue);
            }

            var data = new SitePerformanceTrendViewModel(trendData, mappedTotals);

            return Json( data, JsonRequestBehavior.AllowGet );
        }

        [HttpGet]
        public ActionResult MaxDataVersion()
        {
            string url = string.Format("{0}about?format=json", Settings.Default.MaxAnalytics);
            var model = MaxAnalyticsClient.GetModel<AboutModel>(url);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public override void SetupViewBag()
        {
            if (HtmlHelpers.IsDebugBuild()) {
                ViewBag.CssLinks = new List<string> { Url.VersionedUrl( "~/Themes/MaxMvc/Dashboard.debug.css") };
                ViewBag.JavaScripts = new List<string> { Url.VersionedUrl("~/Public/Scripts/Max/MAX.Dashboard.debug.js"), Url.VersionedUrl("~/Public/Scripts/Max/MAX.Dashboard.render.js") };
            } else {
            ViewBag.CssLinks = new List<string> { Url.VersionedUrl("~/Themes/MaxMvc/Dashboard.css") };
                ViewBag.JavaScripts = new List<string> { Url.VersionedUrl("~/Public/Scripts/Max/MAX.Dashboard.js"), Url.VersionedUrl("~/Public/Scripts/Max/MAX.Dashboard.render.js") };
            }
            ViewBag.HeaderSearchClass = "invisible";

            // Add 2.0 styles if not 3.0
            if (!IsMax30)
            {
                ViewBag.CssLinks.Add(Url.VersionedUrl("~/Themes/MAX2.0/styles.css"));
            }

            // Add OEM logo
            var optionsCommand = GetMiscSettings.GetSettings(WorkflowState.BusinessUnitID);
            if (optionsCommand.Franchise != Franchise.None)
            {
                if (Enum.IsDefined(typeof(OemBrandingSupport), optionsCommand.Franchise.ToString()))
                {
                    ViewBag.CssLinks.Add(
                        Url.VersionedUrl(string.Format("~/App_Themes/OEM/{0}/styles{1}.css", optionsCommand.Franchise, (IsMax30 ? string.Empty : "20"))));
                }
            }

            base.SetupViewBag();
        }

        private class AboutModel
        {
            public string EnvironmentName { get; set; }
            public string Version { get; set; }
        }
    }
}