﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Configuration;
using FirstLook.Common.Core;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Dashboard;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.GroupLevelDashboard;
using FirstLook.Merchandising.DomainModel.Release;
using FirstLook.Merchandising.DomainModel.Reports.DashboardHelpers;
using FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using FirstLook.Merchandising.DomainModel.Templating;
using MAX.Entities;
using Merchandising.Messages;
using Merchandising.Messages.GroupLevel;
using Wanamaker.WebApp.Helpers;
using Wanamaker.WebApp.Areas.Dashboard.Filters;
using MvcMiniProfiler;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics;
using Wanamaker.WebApp.AppCode;

namespace Wanamaker.WebApp.Areas.Dashboard.Controllers
{
    [HandleMvcException]
    public class GroupController : BaseController
    {
        private readonly IGroupDashboardManager _manager;
        private IQueueFactory _queueFactory;
        private IDashboardBusinessUnitRepository _businessUnitRepository;

        private List<FirstLook.Merchandising.DomainModel.Dashboard.Entities.Bucket> _available_buckets = new List<FirstLook.Merchandising.DomainModel.Dashboard.Entities.Bucket>();

        public GroupController(IGroupDashboardManager manager, IQueueFactory queueFactory, IDashboardBusinessUnitRepository businessUnitRepository)
        {
            _manager = manager;
            _queueFactory = queueFactory;
            ViewBag.ViewingGroupDashboard = true;
            _businessUnitRepository = businessUnitRepository;
        }
        
        [CompressionFilter]
        public ActionResult Index(string usedOrNewFilter)
        {

            if (string.IsNullOrWhiteSpace(usedOrNewFilter))
                usedOrNewFilter = GetVehicleTypeFromUserProfile();

			SetupViewBag();

            var model = new GroupDashboardHomeModel
            {
                SelectedUsedNewFilter = usedOrNewFilter,
                GroupId = _businessUnitRepository.GetGroup(BusinessUnit.BusinessUnitId).BusinessUnitId,
                GroupName = _businessUnitRepository.GetGroup(BusinessUnit.BusinessUnitId).Name
            };

            VehicleType[] vehicleTypes = new[] {VehicleType.Used, VehicleType.Both, VehicleType.New};

            //e.g., value = 0   text = Used(250)
            model.UsedNewFilters = vehicleTypes.Select(x => new SelectListItem
            {
                Value = x.ToString(),
                Text =
                    string.Format("{0}", x.ToString().Replace("Both", "Used & New"))
            }).OrderByDescending(x => x.Text);

            model.SelectedFilterCount = vehicleTypes.Single(x => x == model.SelectedUsedNewFilter);
            
            UserProfile.NewUsed = model.SelectedUsedNewFilter;

            return View(model);
        }

        public ActionResult IndexKO(string usedOrNewFilter)
        {
            var Group = _businessUnitRepository.GetGroup(BusinessUnit.BusinessUnitId);

            // business-unit-specific redirect to classic group dashboard if included in an xml configured list
            // only applies if system setting is not set to classic group dashboard globally
            bool useClassicDashboard = false;
            bool.TryParse(ConfigurationManager.AppSettings["UseClassicDashboard"], out useClassicDashboard);
            if( !useClassicDashboard ) {
                // can assume url would have been "[..]GroupDashboard.aspx[..]"
                // get business units
                string classicBusinessUnitList = ConfigurationManager.AppSettings["ClassicBusinessUnitList"];
                var list = classicBusinessUnitList.Split(',');
                foreach( string bu in list ) {
                    int bu_id = 0;
                    int.TryParse( bu, out bu_id );
                    if( Group.BusinessUnitId == bu_id ) {
                        // REDIRECT request to CLASSIC group-level dashboard
                        return RedirectToAction("Index"); 
                    }
                }
            }
            

            if (string.IsNullOrWhiteSpace(usedOrNewFilter))
                usedOrNewFilter = GetVehicleTypeFromUserProfile();

			SetupViewBag();
            ViewBag.CssLinks = new List<string> { 
                Url.VersionedUrl( "~/Themes/MaxMvc/DashboardKO.debug.css"),
                Url.VersionedUrl( "~/Themes/MaxMvc/GroupDashboard.debug.css")
            };
            ViewBag.JavaScripts = new List<string>();


            var model = new GroupDashboardHomeModel
            {
                SelectedUsedNewFilter = usedOrNewFilter,
                GroupId = Group.BusinessUnitId,
                GroupName = Group.Name
            };

            VehicleType[] vehicleTypes = new[] { VehicleType.Used, VehicleType.Both, VehicleType.New };

            //e.g., value = 0   text = Used(250)
            model.UsedNewFilters = vehicleTypes.Select(x => new SelectListItem
            {
                Value = x.ToString(),
                Text =
                    string.Format("{0}", x.ToString().Replace("Both", "Used & New"))
            }).OrderByDescending(x => x.Text);

            model.SelectedFilterCount = vehicleTypes.Single(x => x == model.SelectedUsedNewFilter);

            UserProfile.NewUsed = model.SelectedUsedNewFilter;

            return View(model);
        }
        
        private UserPermissions FetchPermissions(int groupId)
        {
            var login = HttpContext.User.Identity.Name;
            var bufinder = BusinessUnitFinder.Instance();
            var permittedDealers = bufinder.FindAllDealershipsByDealerGroupAndMember(groupId, login)
                .Select(x => x.Id)
                .ToArray();

            //This could be one proc but perf doesn't seem too bad.
            var units = bufinder.FindAllDealershipsByDealerGroup(groupId);
            var DPA_Units = new List<int>();
            var TTM_Units = new List<int>();
            var OCO_Units = new List<int>();
            
            foreach (var unit in units)
            {
                var miscSettings = GetMiscSettings.GetSettings(unit.Id);

                if (miscSettings.AnalyticsSuite)
                    DPA_Units.Add(unit.Id);
                if (miscSettings.ShowTimeToMarket)
                    TTM_Units.Add(unit.Id);
                if (miscSettings.showOnlineClassifiedOverview)
                    OCO_Units.Add(unit.Id);
            }

            var groupUpgradeSettings = GetUpgradeSettings.GetGroupUpgradeSettings(groupId);

            return new UserPermissions { UserBusinessUnits = permittedDealers,
                AnalyticsSuiteBusinessUnits = DPA_Units.ToArray(),
                TimeToMarketBusinessUnits = TTM_Units.ToArray(),
                OnlineClassifiedOverviewBusinessUnits = OCO_Units.ToArray(),
                IsReportViewer = IsReportViewer,
                ShowroomBusinessUnits = groupUpgradeSettings.Where(gus => gus.MAXDigitalShowroom && permittedDealers.Contains(gus.BusinessUnitId)).Select(gus => gus.BusinessUnitId).ToArray(),
                ShowroomReportsBusinessUnits = groupUpgradeSettings.Where(gus => gus.EnableShowroomReports && permittedDealers.Contains(gus.BusinessUnitId)).Select(gus => gus.BusinessUnitId).ToArray(),
                Website20BusinessUnits = groupUpgradeSettings.Where(gus => gus.MAXForWebsite20 && permittedDealers.Contains(gus.BusinessUnitId)).Select(gus => gus.BusinessUnitId).ToArray()
            };
        }

        [CompressionFilter]
        [HttpGet]
        public ActionResult GroupDashboardData(int groupId, string usedNewFilter)
        {
            var model = _manager.FetchDashboardData(groupId, usedNewFilter);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CacheStatus(int groupId)
        {
            var state = _manager.FetchCachedStatus(groupId);
            return Json(state, JsonRequestBehavior.AllowGet);
        }

        [ActionName("CacheStatus")]
        [HttpPost]
        public ActionResult StartCacheStatus(int groupId)
        {
            var queue = _queueFactory.CreateGroupLevelAggregation();
            queue.SendMessage(new GroupLevelAggregationMessage(groupId), GetType().FullName);

            _manager.SaveCachedState(groupId, State.Pending);
            return new EmptyResult();
        }
       
        [CompressionFilter]
        [HttpGet]
        public ActionResult UserPermissions(int groupId)
        {
            var model = FetchPermissions(groupId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [CompressionFilter]
        [HttpGet]
        public ActionResult GroupSitePerformanceDataForMonth(int groupId, string usedNewFilter, int month, int year, int endMonth, int endYear)
        {
            var startDate = new DateTime(year, month, 1);
            var endDate = new DateTime(endYear, endMonth, 1).AddMonths(1).AddSeconds(-1);

            var model = _manager.FetchSitePerformance(groupId, usedNewFilter, startDate, endDate);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
		
		[HttpGet]
        public void SaveDealerSiteBudget(int businessUnitId, string siteName, int budgetValue, int month, int year)
        {
            _manager.SaveDealerSiteBudget(budgetValue, siteName, budgetValue, month, year);
        }

        [HttpPost]
        public ActionResult ChangeInventoryTypeFilter(string selectedUsedNewFilter)
        {
            return RedirectToAction("Index", new { usedOrNewFilter = selectedUsedNewFilter });
        }

        [CompressionFilter]
        [HttpGet]
        public ActionResult GoogleAnalyticsDataByDateAndLimit(int groupId, int gaProfileType, int month, int year, int endMonth, int endYear, int limit)
        {
            using (MiniProfiler.Current.Step("Google Analytics Data"))
            {
                var dateRange = new DateRange(year, month, endYear, endMonth);
                var data = new Object();

                try
                {
                    GoogleAnalyticsType gaType = (GoogleAnalyticsType)gaProfileType;
                    data = _manager.GetAllGoogleAnalyticsData(groupId, gaType, dateRange);
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null && ex.InnerException.Message != null)
                    {
                        data = new { errorMessage = ex.Message, errorData = ex.InnerException.Message };
                    }
                    else
                    {
                        data = new { errorMessage = ex.Message };
                    }
                }

                return new BigJsonResult(data);
            }
        }

        [CompressionFilter]
        [HttpGet]
        public ActionResult GroupTimeToMarketData(int groupId, string usedNewFilter, int startMonth, int startYear, int endMonth, int endYear)
        {
            var dateRange = new DateRange(startYear, startMonth, endYear, endMonth);

            var data = _manager.FetchGroupReport(groupId, usedNewFilter, dateRange);
            return new BigJsonResult(new GroupTimeToMarketReportVm( data ));
        }

        public override void SetupViewBag()
        {
            if (HtmlHelpers.IsDebugBuild()) {
                ViewBag.CssLinks = new List<string> { Url.VersionedUrl("~/Themes/MaxMvc/Dashboard.debug.css"), Url.VersionedUrl("~/Themes/MaxMvc/GroupDashboard.debug.css") };
                ViewBag.JavaScripts = new List<string> { Url.VersionedUrl("~/Public/Scripts/Max/MAX.Dashboard.debug.js"), Url.VersionedUrl("~/Public/Scripts/Max/MAX.Dashboard.Group.render.js") };
            } else {
                ViewBag.CssLinks = new List<string> { Url.VersionedUrl("~/Themes/MaxMvc/Dashboard.css"), Url.VersionedUrl("~/Themes/MaxMvc/GroupDashboard.css") };
                ViewBag.JavaScripts = new List<string> { Url.VersionedUrl("~/Public/Scripts/Max/MAX.Dashboard.js"), Url.VersionedUrl("~/Public/Scripts/Max/MAX.Dashboard.Group.render.js") };
            }
            ViewBag.HeaderSearchClass = "invisible";

            // Add 2.0 styles if not 3.0
            if (!IsMax30)
            {
                ViewBag.CssLinks.Add(Url.VersionedUrl("~/Themes/MAX2.0/styles.css"));
            }

            // Add OEM logo
            /*var optionsCommand = GetMiscSettings.GetSettings(WorkflowState.BusinessUnitID);
            if (optionsCommand.Franchise != Franchise.None)
            {
                if (Enum.IsDefined(typeof(OemBrandingSupport), optionsCommand.Franchise.ToString()))
                {
                    ViewBag.CssLinks.Add(
                        Url.VersionedUrl(string.Format("~/App_Themes/OEM/{0}/styles{1}.css", optionsCommand.Franchise, (IsMax30 ? string.Empty : "20"))));
                }
            }*/

            base.SetupViewBag();

            // I don't want to override the business unit used anywhere else, so just override what's set in the ViewBag.
            ViewBag.BusinessUnit = _businessUnitRepository.GetGroup(BusinessUnit.BusinessUnitId);
        }

    }
}
