﻿
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using System.Web.WebPages;
using FirstLook.Merchandising.DomainModel.Commands;
using MAX.Entities.Reports.Dashboard;
using Wanamaker.WebApp.Areas.Dashboard.Filters;
using Wanamaker.WebApp.Areas.Results;

namespace Wanamaker.WebApp.Areas.Dashboard.Controllers
{
    [HandleMvcException]
    public class TrafficController : BaseController
    {
        /// <summary>
        /// Digital Showroom Page Views & Traffic
        /// </summary>
        /// <returns></returns>
        public JsonResult ShowroomViews(int businessUnitId, string startDate, string endDate)
        {
            // Validation:

            // 1) Showroom upgrade available?

            // 2) user is able to view businessUnitID's data?
            
            var trafficReports = new GoogleAnalyticsShowroomTrafficReport(businessUnitId);
            trafficReports.FetchMonths(DateTime.Parse(startDate), DateTime.Parse(endDate));
            var traffic = trafficReports.ShowroomTrafficDto;

            var success = true;
            var result = new JsonNetResult();

            if (!success)
            {
                Response.SetStatus(HttpStatusCode.InternalServerError);
                // or
                Response.SetStatus(HttpStatusCode.ExpectationFailed);
                // or
                Response.SetStatus(HttpStatusCode.Forbidden);

                traffic.Errors.Add("A useful description of what went wrong.");
            }
            result.Data = traffic;
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return result;
        }

        public JsonResult GroupShowroomViews(int businessUnitId, string startDate, string endDate)
        {
            var trafficReports = new GoogleAnalyticsGroupTrafficReport(businessUnitId);
            trafficReports.FetchShowroomTrafficByMonths(DateTime.Parse(startDate), DateTime.Parse(endDate));

            var groupTraffic = trafficReports.GroupTrafficDto;

            var result = new JsonNetResult();
            result.Data = groupTraffic;
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return result;
        }

        /// <summary>
        /// Website 2.0 Traffic
        /// </summary>
        /// <returns></returns>
        public JsonResult WebTraffic(int businessUnitId, string startDate, string endDate)
        {
            // Validation:

            // 1) Website 2.0 upgrade available?

            // 2) user is able to view businessUnitID's data?
            var trafficReport = new GoogleAnalyticsWebTrafficReport(businessUnitId);
            trafficReport.FetchMonths(DateTime.Parse(startDate), DateTime.Parse(endDate));

            var traffic = trafficReport.WebTrafficDto;

            var result = new JsonNetResult();
            result.Data = traffic;
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return result;
        }

        public JsonResult GroupWebViews(int businessUnitId, string startDate, string endDate)
        {
            var trafficReports = new GoogleAnalyticsGroupTrafficReport(businessUnitId);
            trafficReports.FetchWebTrafficByMonths(DateTime.Parse(startDate), DateTime.Parse(endDate));

            var groupTraffic = trafficReports.GroupTrafficDto;

            var result = new JsonNetResult();
            result.Data = groupTraffic;
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return result;
        }
    }
}