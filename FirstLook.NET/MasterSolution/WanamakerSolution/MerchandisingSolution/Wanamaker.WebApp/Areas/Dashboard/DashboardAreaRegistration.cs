﻿using System;
using System.Configuration;
using System.Web.Mvc;
using FirstLook.Common.Core.PhotoServices.WebService.V1.Model;

namespace Wanamaker.WebApp.Areas.Dashboard
{
    public class DashboardAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Dashboard";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            bool useClassicDashboard = false;
            bool.TryParse(ConfigurationManager.AppSettings["UseClassicDashboard"], out useClassicDashboard);

            if( !useClassicDashboard ) {
                // Group (new setup)
                context.MapRoute( "GroupDashboardHomeKO","GroupDashboard.aspx",       new { controller = "Group", action = "IndexKO" });
                context.MapRoute( "GroupDashboardHome",  "LegacyGroupDashboard.aspx", new { controller = "Group", action = "Index" });
            } else {
                // Group (classic setup)
                context.MapRoute( "GroupDashboardHomeKO","GroupDashboardKO.aspx",     new { controller = "Group", action = "IndexKO" });
                context.MapRoute( "GroupDashboardHome",  "GroupDashboard.aspx",       new { controller = "Group", action = "Index" });
            }
            // Exec
            context.MapRoute( "DashboardHomeKO",     "Dashboard.aspx",            new { controller = "Home",  action = "IndexKO" });
            context.MapRoute( "DashboardHome",       "LegacyDashboard.aspx",      new { controller = "Home",  action = "Index" });

            context.MapRoute(
                "DigitalPerformanceAnalytics",
                "Dashboard/{controller}/{action}",
                new { }
            );

            context.MapRoute(
                 "BucketDataRoute",
                 "InventoryFilters.aspx/{businessUnitId}/{usedNewFilter}",
                 new { controller = "Home", action = "BucketsData", usedNewFilter = "Both" }
             );

            context.MapRoute(
                "AllGoogleAnalyticsDataRouteByDateAndLimit",
                "GoogleAnalyticsDataAll.aspx/{businessUnitId}/{gaProfileType}/{month}/{year}/{endMonth}/{endYear}/{limit}",
                new { controller = "Home", action = "GoogleAnalyticsDataByDateAndLimit" }
            );

            context.MapRoute(
                "AllGoogleAnalyticsDataRouteByDateAndLimitUngrouped",
                "GoogleAnalyticsDataAllUngrouped.aspx/{businessUnitId}/{gaProfileType}/{month}/{year}/{endMonth}/{endYear}/{limit}",
                new { controller = "Home", action = "GoogleAnalyticsDataByDateAndLimitUngrouped" }
            );

            context.MapRoute(
                 "DashboardDataRoute",
                 "DashboardData.aspx/{businessUnitId}/{usedNewFilter}",
                 new { controller = "Home", action = "DashboardData" }
             );

            context.MapRoute(
                "GroupAllGoogleAnalyticsDataByDateAndLimitRoute",
                "GroupGoogleAnalyticsDataAll.aspx/{groupId}/{gaProfileType}/{month}/{year}/{endMonth}/{endYear}/{limit}",
                new { controller = "Group", action = "GoogleAnalyticsDataByDateAndLimit" }
            );

            context.MapRoute(
                "GroupDashboardDataRoute",
                "GroupDashboardData.aspx/{groupId}/{usedNewFilter}",
                new { controller = "Group", action = "GroupDashboardData" }
            );

            context.MapRoute(
               "UserPermissionsRoute",
               "User.aspx/permissions/{groupId}",
               new { controller = "Group", action = "UserPermissions" }
            );

            context.MapRoute(
                   "GroupDashboardDefault",
                   "GroupDashboard.aspx/{action}",
                   new { controller = "Group" }
               );

            context.MapRoute(
                   "CacheStatus",
                   "GroupDashboard.aspx/CacheStatus/{groupId}",
                   new { controller = "Group", action = "CacheStatus" }
               );

            context.MapRoute(
                   "StartCacheStatus",
                   "GroupDashboard.aspx/StartCacheStatus/{groupId}",
                   new { controller = "Group", action = "CacheStatus" }
               );

            context.MapRoute(
                 "SitePerformanceForMonthRoute",
                 "SitePerformanceForMonth.aspx/{businessUnitId}/{usedNewFilter}/{month}/{year}/{endMonth}/{endYear}",
                 new { controller = "Home", action = "SitePerformanceDataForMonth"}
             );

            context.MapRoute(
                "GroupSitePerformanceForMonthRoute",
                "GroupSitePerformanceForMonth.aspx/{groupId}/{usedNewFilter}/{month}/{year}/{endMonth}/{endYear}",
                new { controller = "Group", action = "GroupSitePerformanceDataForMonth"}
            );

            context.MapRoute(
                   "SearchMaxSE",
                   "SearchMaxSE.aspx/{stockNumber}",
                   new { controller = "Home", action = "SearchMaxForSelling" }
               );

            context.MapRoute(
                   "DashboardDefault",
                   "Dashboard.aspx/{action}",
                   new { controller = "Home" }
               );

            context.MapRoute(
                "SaveDealerSiteBudget",
                "SaveDealerSiteBudget.aspx/{businessUnitId}/{siteName}/{budgetValue}/{month}/{year}",
                new { controller = "Group", action = "SaveDealerSiteBudget" }
            );

            context.MapRoute(
                "TimeToMarket",
                "TimeToMarketReport.aspx/{businessUnitId}/{usedNewFilter}/{startMonth}/{startYear}/{endMonth}/{endYear}",
                new { controller = "Home", action = "TimeToMarketData" }
            );

            context.MapRoute(
                "GroupTimeToMarket",
                "GroupTimeToMarketReport.aspx/{groupId}/{usedNewFilter}/{startMonth}/{startYear}/{endMonth}/{endYear}",
                new { controller = "Group", action = "GroupTimeToMarketData" }
            );

            context.MapRoute(
                "SitePerformanceTrend",
                "SitePerformanceTrend.aspx/{businessUnitId}/{vehicleType}/{startMonth}/{startYear}/{endMonth}/{endYear}",
                new {
                    controller = "Home",
                    action = "SitePerformanceTrend"
                });

            context.MapRoute(
                "ChangeInventoryTypeFilter",
                "ChangeInventoryTypeFilter.aspx/{usedNewFilter}", 
                new {
                    controller = "Home", 
                    action = "ChangeInventoryTypeFilter"
                });

            context.MapRoute("MaxDataVersion",
                "MaxData.aspx/Version",
                new {
                    controller = "Home",
                    action = "MaxDataVersion"
                });

            context.MapRoute("DashboardSettngs",
                "DashboardSettings.aspx/{businessUnitId}",
                new
                {
                    controller = "Home",
                    action = "GetDashboardSettings"
                });
        }
    }
}
