﻿using System.Web.Mvc;

namespace Wanamaker.WebApp.Areas.Router
{
    public class RouterAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Router";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Router_default",
                "Router.aspx/{destination_name}",
                new { controller = "Router", action = "Index" }
            );
        }
    }
}
