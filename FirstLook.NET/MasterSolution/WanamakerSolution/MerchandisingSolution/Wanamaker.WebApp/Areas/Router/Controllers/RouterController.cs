﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Reports.DashboardHelpers;
using FirstLook.DomainModel.Oltp;
using MAX.Entities.Helpers.DashboardHelpers;
using Wanamaker.WebApp.Helpers;

namespace Wanamaker.WebApp.Areas.Router.Controllers
{
    public class RouterController : Controller
    {
        public ActionResult Index( string destination_name )
        {
            switch( destination_name.ToLowerInvariant() )
            {
                case "max_digital_performance_system":
                    var businessUnit = this.HttpContext == null ? null : this.HttpContext.Items["BusinessUnit"] as BusinessUnit;
                    var settings = GetMiscSettings.GetSettings( businessUnit.Id );
                    if( settings.ShowDashboard && Helper.IsReportViewer ) { // show dashboard
                        if( Request.FromRubyHomePage() ) {
                            return new RedirectResult( "~/Dashboard.aspx?usedOrNewFilter=Used", true );
                        } else {
                            return new RedirectResult( "~/Dashboard.aspx", true );
                        }
                    } else { // do not show dashboard
                        return new RedirectResult( "~/Workflow/Inventory.aspx", true );
                    }
                    break;

                case "needs_repricing":
                    return new RedirectResult( "~/merchandising/workflow/inventory.aspx?usednew=2&filter=DueForRepricing", true );
                    break;

                case "max_dashboard":
                    return new RedirectResult( "~/Dashboard.aspx", true );
                    break;

                case "max_for_selling_elite":
                    break;

                case "online_inventory_status":
                    return new RedirectResult( "~/Dashboard.aspx", true );
                    break;
            }
            throw new Exception("Invalid Router Destination: "+destination_name);
        }

    }
}
