﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FirstLook.Merchandising.DomainModel.Reports.InventoryPerformace;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using Wanamaker.WebApp.Areas.Inventory.ViewModels;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Controllers
{
    public class PerformanceController : Controller
    {
        private IPerformanceRepository _performanceRepository;
       
        public PerformanceController(IPerformanceRepository performanceRepository)
        {
            _performanceRepository = performanceRepository;
        }

        [HttpGet]
        public JsonResult ClickThroughRatePrice(int businessUnitId, int inventoryId)
        {
            var vehiclePerformance = new VehiclePerformance();

            var inventoryData = InventoryData.Fetch(businessUnitId, inventoryId);
            var vehicleConfiguration = VehicleConfiguration.FetchOrCreateDetailed(businessUnitId, inventoryId, User.Identity.Name);

            vehiclePerformance.ExteriorColor = ColorPair.GetItemDescription(vehicleConfiguration.ExteriorColor1, vehicleConfiguration.ExteriorColor2, string.Empty);
            vehiclePerformance.PhotoCount = inventoryData.PhotoCount;
            vehiclePerformance.YearMakeModel = inventoryData.YearMakeModel;
            vehiclePerformance.Mileage = inventoryData.DisplayMileage;
            vehiclePerformance.Certfied = inventoryData.Certified;

            var priceData = _performanceRepository.GetPriceEvents(inventoryId);
            foreach(var data in priceData)
            {
                vehiclePerformance.PriceEvents.Add(new RepriceEvent(){Price = data.Price, Date = data.Date.ToShortDateString()});
            }

            var perfData = _performanceRepository.GetAdPerformace(businessUnitId, inventoryId);
            perfData = AdPerformanceProcessor.Process(perfData);
            foreach(var data in perfData)
            {
                IList<ClickThroughRate> eventSource = null;
                if (!vehiclePerformance.Sources.ContainsKey(data.Source))
                {
                    eventSource = new List<ClickThroughRate>();
                    vehiclePerformance.Sources.Add(data.Source, eventSource);
                }

                eventSource = vehiclePerformance.Sources[data.Source];

                eventSource.Add(new ClickThroughRate(){
                    Rate = (float)data.ClickThrough,
                    Date = data.Date.ToShortDateString(),
                    StoredValue = data.RealPoint
                });
            }

            return Json(vehiclePerformance, JsonRequestBehavior.AllowGet);
        }

    }
}
