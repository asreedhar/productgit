﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Audit;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.WindowSticker;
using VehicleTemplatingWebServiceClient.VehicleTemplatingService;
using VehicleData = VehicleTemplatingDomainModel.VehicleData;
using System.Net;
using System.Web.Script.Serialization;
using Wanamaker.WebApp.Areas.PricingAnalysis.Models;
using FirstLook.Common.Core.Command;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using BulkWindowSticker;
using System.IO;
using FirstLook.Common.Core.Utilities;


namespace Wanamaker.WebApp.Areas.PricingAnalysis.Controllers
{
    public class PwsController: Controller
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        internal const string PrintPdfByteArrayCollection = "PRINT_PDF_BYTE_ARRAY_COLLECTION";
        VehicleTemplatingService templatingService = new VehicleTemplatingService();
        private int BusinessUnitId;
        private int InventoryId;

        public ActionResult Index(int? BusinessUnitId, int? inv)
        {
            // get the owner
            var owner = Owner.GetOwner(BusinessUnitId.Value);
            PrintWindowSticker pws = new PrintWindowSticker();
            try
            {
                pws.PageTitle = "Print Window Sticker";
                TemplateInfoSorter sorter = new TemplateInfoSorter();
                // get basic info about the dealer's templates to bind the template dropdownlist
                
                var windowStickers = templatingService.GetWindowStickerTemplateInfoForOwner(owner.Handle);
                Array.Sort(windowStickers, sorter);

                pws.WindowStickers = new List<SelectListItem>();
                //pws.BusinessUnitId = BusinessUnitId;
                foreach (var ws in windowStickers)
                {
                    SelectListItem sellist = new SelectListItem();
                    sellist.Value = ws.Id.ToString();
                    sellist.Text = ws.Name;
                    pws.WindowStickers.Add(sellist);
                }

                // get basic info about the dealer's templates to bind the template dropdownlist
                var buyersGuides = templatingService.GetBuyersGuideTemplateInfoForOwner(owner.Handle);
                Array.Sort(buyersGuides, sorter);

                pws.BuyersGuides = new List<SelectListItem>();

                foreach (var bg in buyersGuides)
                {
                    SelectListItem sellist = new SelectListItem();
                    sellist.Value = bg.Id.ToString();
                    sellist.Text = bg.Name;

                    pws.BuyersGuides.Add(sellist);
                }
                ViewData["postUrl"] = "/merchandising/PricingAnalysis/Pws/PrintPdf?inv=" + inv + "&businessUnitId=" + BusinessUnitId;
                
                //if (pws.WindowStickers != null)
                //{
                //    ShowControls(Int32.Parse(pws.WindowStickers[0].Value));
                //}

                if (pws.BuyersGuides != null)
                {
                    pws.BuyerGuidesJsonStatus = ShowControls(Int32.Parse(pws.BuyersGuides[0].Value));
                }

                if (pws.BuyersGuides.Count > 0)
                    pws.ServiceContractStatus = true;

                pws.LastPrintDateMessage = SetLastPrintDateMessage(inv.Value);
            }
            catch (WebException ex)
            {
                Log.Warn(ex);
            }
            return View(pws);
        }

        public string PWSLastPrintDateMessage(int? inv)
        {
            return SetLastPrintDateMessage(inv.Value);
        }

        private string SetLastPrintDateMessage(int inv)
        {
            string LastPrintedMessage = string.Empty;
            if (inv == 0)
                return LastPrintedMessage;

            var getLastPrintDateCommand = new GetLastWindowStickerPrintDate(inv);
            AbstractCommand.DoRun(getLastPrintDateCommand);

           return LastPrintedMessage = getLastPrintDateCommand.LastPrintDate == default(DateTime)
                                    ? "A Window Sticker has not yet been printed."
                                    : "Window Sticker Last printed: " + getLastPrintDateCommand.LastPrintDate;
        }

        public string ShowControls(int? selectedValue)
        {
            TemplateTO wsTemplate = GetSelectedTemplate(selectedValue.Value);
            DisplayElements dispEle = new DisplayElements();
            if (wsTemplate != null) dispEle = SetPromptControlVisibility(wsTemplate);
            JavaScriptSerializer json = new JavaScriptSerializer();
            return json.Serialize(dispEle);
        }

        private TemplateTO GetSelectedTemplate(int templateId)
        {
            TemplateTO template = null;
            try
            {
                template = templatingService.GetTemplate(templateId);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

            return template;
        }

        private static DisplayElements SetPromptControlVisibility(TemplateTO template)
        {
            DisplayElements de = new DisplayElements();
            foreach (var contentArea in template.ContentAreas)
            {
                DataPoint dp = contentArea.DataPoint;

                if (dp != null && !string.IsNullOrEmpty(dp.Prompt))
                {
                    de = GetPromptCheckControl(dp, de);
                }
            }

            return de;
        }

        private static DisplayElements GetPromptCheckControl(DataPoint dp, DisplayElements de)
        {
            if (dp.Prompt.Equals("No Warranty", StringComparison.CurrentCultureIgnoreCase))
            {
                de.NoWarranty = true;
                return de;
            }
            if (dp.Prompt.Equals("Limited Warranty", StringComparison.CurrentCultureIgnoreCase))
            {
                de.LimitedWarranty = true;
                return de;
            }
            if (dp.Prompt.Equals("Full Warranty", StringComparison.CurrentCultureIgnoreCase))
            {
                de.FullWarranty = true;
                return de; 
            }
            if (dp.Prompt.Equals("Service Contract", StringComparison.CurrentCultureIgnoreCase))
            {
                de.ServiceContract = true;
                return de;
            }
            if (dp.Prompt.Equals("KBB Book Value", StringComparison.CurrentCultureIgnoreCase))
            {
                de.KbbBookValue = true;
                return de;
            }

            return null;
        }
        private static bool? GetPromptCheckControlStatus(DataPoint dp, DisplayElements de)
        {
            if (dp.Prompt.Equals("No Warranty", StringComparison.CurrentCultureIgnoreCase))
            {
                return de.NoWarranty;
            }
            if (dp.Prompt.Equals("Limited Warranty", StringComparison.CurrentCultureIgnoreCase))
            {
                return de.LimitedWarranty ;
            }
            if (dp.Prompt.Equals("Full Warranty", StringComparison.CurrentCultureIgnoreCase))
            {
                return de.FullWarranty ; 
            }
            if (dp.Prompt.Equals("Service Contract", StringComparison.CurrentCultureIgnoreCase))
            {
                return de.ServiceContract ;
            }
            if (dp.Prompt.Equals("KBB Book Value", StringComparison.CurrentCultureIgnoreCase))
            {
                return de.KbbBookValue;
            }

            return null;
        }
         
        public FileStreamResult PrintPdf(int? inv, int? businessUnitId, int? pwsSelectedTemp, int? bgtSelectedTemp, string de)
        {
            BusinessUnitId = businessUnitId.Value;
            InventoryId = inv.Value;

            var pdfs = new Collection<byte[]>();
            
            DisplayElements displayElements = new DisplayElements();
            if (!string.IsNullOrEmpty(de))
            {
                //Set all the prompt controls checked status with DisplayElements property 
                displayElements = GetDisplayElementData(displayElements, de.Split(",".ToCharArray()));
            }

            if (pwsSelectedTemp != null && pwsSelectedTemp > 0)
            {
                var windowStickerPdf = GetPdfDocument(pwsSelectedTemp, displayElements);
                pdfs.Add(windowStickerPdf);
                TrackUsage(BusinessUnitEventType.Window_Sticker_Printed);
            }

            if (bgtSelectedTemp != null && bgtSelectedTemp > 0)
            {
                var buyersGuidePdf = GetPdfDocument(bgtSelectedTemp, displayElements);
                pdfs.Add(buyersGuidePdf);
                TrackUsage(BusinessUnitEventType.Buyers_Guide_Printed);
            }

            if (pdfs.Count > 0)
            {
                LogPrintTime( pwsSelectedTemp, bgtSelectedTemp);
            }

            MemoryStream ms = new MemoryStream();

            // add the print script to the pdf
            string printScript = "this.print(true);";
            var pdf = PdfDocumentWrapper.Merge(pdfs, 0, 0, 0, 0, printScript);

            ms.Write(pdf, 0, pdf.Length);
            ms.Position = 0;
            return new FileStreamResult(ms, "application/pdf");
        }

        private DisplayElements GetDisplayElementData(DisplayElements de, string[] dataElements)
        {
            foreach (string str in dataElements)
            {
                switch (str)
                {
                    case "WarrantyNo":
                        de.NoWarranty = true;
                        break;
                    case "WarrantyLimited":
                        de.FullWarranty = true;
                        break;
                    case "WarrantyFull":
                        de.LimitedWarranty = true;
                        break;
                    case "ServiceContract":
                        de.ServiceContract = true;
                        break;
                    case "KbbBookValue":
                        de.KbbBookValue = true;
                        break;
                }
            }
            return de;
        }

        /// <summary>
        /// Get the pdf using the template and vehicle data, add the pdf to a cache for the print page.
        /// </summary>
        private byte[] GetPdfDocument(int? selecttedTemplate,DisplayElements displayElements)
        {

            // get the selected template
            TemplateTO template = GetSelectedTemplate(selecttedTemplate.Value);
            ApplyJustInTimeTransformations(BusinessUnitId, template);

            SelectContentAreasBasedOnUserInput(template, displayElements);

            // get an owner handle
            var owner = Owner.GetOwner(BusinessUnitId);
            if (owner == null)
                throw new ApplicationException("No owner was found for the businessUnitId " + BusinessUnitId);

            // put together an inventory Id...this could be moved elsewhere
            var vehicleHandle = "1" + InventoryId;

            // get the vehicle data
            var legacyVehicleAdapter = new LegacyVehicleAdapter("mobileapp");
            VehicleData vtdmVehicleData = legacyVehicleAdapter.Adapt(owner.Handle, vehicleHandle);

            // convert it to a type tye service can use
            var wsVehicleData = GetWebServiceVehicleData(vtdmVehicleData);

            // get the pdf
            byte[] pdf = templatingService.GeneratePdf(wsVehicleData, template, owner.Handle);

            return pdf;
        }

        private static void SelectContentAreasBasedOnUserInput(TemplateTO template, DisplayElements checkControlContainer)
        {
            foreach (var contentArea in template.ContentAreas)
            {
                DataPoint dp = contentArea.DataPoint;

                if (dp.Prompt == null)
                {
                    continue;
                }

                bool? checkControl = GetPromptCheckControlStatus(dp, checkControlContainer);

                // We will default to show/select a datapoint.  So, either we didn't have a checkbox 
                // to turn it off, or we had one that was turned on (checked)
                dp.Selected = (checkControl == null) || (checkControl.Value);

            }
        }

        private void ApplyJustInTimeTransformations(int BusinessUnitId, TemplateTO template)
        {
            // if business unit does not have access to QR Codes app feature, remove them from the model before sending to pdf service
            var upgradeSettings = GetUpgradeSettings.GetSettings(BusinessUnitId, false);
            if (!upgradeSettings.MAXForSmartphone)
            {
                // business unit does not have "maxForSmartphone" permission; remove all QR codes
                if (template.ContentAreas != null && template.ContentAreas.Length > 0)
                {
                    var ContentAreas_new = new List<ContentArea>(template.ContentAreas.Length);
                    for (var i = 0; i < template.ContentAreas.Length; ++i)
                    {
                        if (template.ContentAreas[i].DataPoint.Key != DynamicDataPointKeys.QRCodeURL)
                        {
                            // re-add all content areas that are not QR codes to a new collection
                            ContentAreas_new.Add(template.ContentAreas[i]);
                        }
                    }
                    template.ContentAreas = ContentAreas_new.ToArray();
                }
            }
        }

        private static VehicleTemplatingWebServiceClient.VehicleTemplatingService.VehicleData GetWebServiceVehicleData(VehicleTemplatingDomainModel.VehicleData vehicleDataIn)
        {
            var vehicleDataOut = new VehicleTemplatingWebServiceClient.VehicleTemplatingService.VehicleData();

            vehicleDataOut.Model = vehicleDataIn.Model;
            vehicleDataOut.ModelYear = vehicleDataIn.ModelYear;
            vehicleDataOut.Make = vehicleDataIn.Make;
            vehicleDataOut.AgeInDays = vehicleDataIn.AgeInDays;
            vehicleDataOut.BodyStyle = vehicleDataIn.BodyStyle;
            vehicleDataOut.Certified = vehicleDataIn.Certified;
            vehicleDataOut.CertifiedID = vehicleDataIn.CertifiedID;
            vehicleDataOut.Class = vehicleDataIn.Class;
            vehicleDataOut.Description = vehicleDataIn.Description;
            vehicleDataOut.Drivetrain = vehicleDataIn.Drivetrain;
            vehicleDataOut.Engine = vehicleDataIn.Engine;
            vehicleDataOut.Equipment = vehicleDataIn.Equipment.ToArray();
            vehicleDataOut.Packages = vehicleDataIn.Packages.ToArray();
            vehicleDataOut.ExteriorColor = vehicleDataIn.ExteriorColor;
            vehicleDataOut.FuelType = vehicleDataIn.FuelType;
            vehicleDataOut.InteriorColor = vehicleDataIn.InteriorColor;
            vehicleDataOut.Mileage = vehicleDataIn.Mileage;
            vehicleDataOut.Price = vehicleDataIn.Price;
            vehicleDataOut.Series = vehicleDataIn.Series;
            vehicleDataOut.StockNumber = vehicleDataIn.StockNumber;
            vehicleDataOut.Transmission = vehicleDataIn.Transmission;
            vehicleDataOut.Trim = vehicleDataIn.Trim;
            vehicleDataOut.UnitCost = vehicleDataIn.UnitCost;
            vehicleDataOut.Vin = vehicleDataIn.Vin;
            vehicleDataOut.MpgCity = vehicleDataIn.MpgCity;
            vehicleDataOut.MpgHwy = vehicleDataIn.MpgHwy;
            vehicleDataOut.KBBBookValue = vehicleDataIn.KBBBookValue;
            vehicleDataOut.NADABookValue = vehicleDataIn.NADABookValue;

            return vehicleDataOut;
        }

        private void TrackUsage(BusinessUnitEventType eventType)
        {
            var Username = "unknown";
            //if (HttpContext.Current != null)
            //    Username = HttpContext.Current.User.Identity.Name;

            var businessUnitEvent = new BusinessUnitEventLogEntry
            {
                BusinessUnitID = BusinessUnitId,
                EventType = (int)eventType,
                CreatedBy = Username
            };
            businessUnitEvent.Save();
        }

        private void LogPrintTime(int? pwsSelectedTemp, int? bgtSelectedTemp)
        {
            // old login code
            //var logPrintDateCommand = new SetLastWindowStickerPrintDate(InventoryId);
            //AbstractCommand.DoRun(logPrintDateCommand);

            WindowStickerRepository windowStickerRepository = new WindowStickerRepository();

            // save window sticker history
            if (pwsSelectedTemp != null && pwsSelectedTemp > 0)
            {
                bool isAutoTemplateId = false;
                var upgradeSettings = GetUpgradeSettings.GetSettings(BusinessUnitId);

                int? autoTemplateId = upgradeSettings.AutoWindowStickerTemplateId;
                int windowStickerTemplateId;
                if (Int32.TryParse(pwsSelectedTemp.Value.ToString(), out windowStickerTemplateId))
                {
                    // check for auto template so it does not print again in batch
                    if (autoTemplateId != null && windowStickerTemplateId == autoTemplateId)
                        isAutoTemplateId = true;

                    windowStickerRepository.SavePrintLog(InventoryId, 1, windowStickerTemplateId, isAutoTemplateId, null);
                }

            }

            // save buyers guide history
            if (bgtSelectedTemp != null && bgtSelectedTemp > 0)
            {
                int buyersGuideTemplateId;
                if (Int32.TryParse(bgtSelectedTemp.Value.ToString(), out buyersGuideTemplateId))
                {
                    windowStickerRepository.SavePrintLog(InventoryId, 2, buyersGuideTemplateId, false, null);
                }
            }

        }

    }


    public class TemplateInfoSorter : IComparer<VehicleTemplatingWebServiceClient.VehicleTemplatingService.TemplateInfo>
    {
        public int Compare(VehicleTemplatingWebServiceClient.VehicleTemplatingService.TemplateInfo x, VehicleTemplatingWebServiceClient.VehicleTemplatingService.TemplateInfo y)
        {
            return x.Name.CompareTo(y.Name);
        }
    }

}