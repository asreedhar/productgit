﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.UI.MobileControls;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.DataAccess;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using FirstLook.Merchandising.DomainModel.Workflow;
using FirstLook.Merchandising.ModelBuilder.BusinessUnit;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.MarketListings;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.PricingAnalysis;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.MarketListings;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.SaveApproveAd;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.SearchCriteria;
using MAX.Entities;
using MAX.Entities.Enumerations;
using MAX.Entities.Helpers.DashboardHelpers;
using MAX.Market;
using Merchandising.Messages;
using MvcMiniProfiler;
using Newtonsoft.Json;
using Wanamaker.WebApp.AppCode;
using Wanamaker.WebApp.Areas.PricingAnalysis.Builders;
using Wanamaker.WebApp.Areas.PricingAnalysis.Models;
using Wanamaker.WebApp.Areas.PricingAnalysis.Models.MarketListingsTable;
using WebGrease.Css.Extensions;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Controllers
{
    public class PingOneController : Controller
    {
        private readonly IMappingClient _mappingClient;
        private readonly IPhotoServices _photoServices;
        private const string InventoryUrl = "~/Workflow/Inventory.aspx";

        public PingOneController(IPhotoServices injectedPhotoServiceses, IMappingClient mappingClient)
        {
            _mappingClient = mappingClient;
            _photoServices = injectedPhotoServiceses;
        }

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        SearchInputs searchOptionsList;

        private Dealer Dealer { get { return HttpContext.Session[Dealer.HttpContextKey] as Dealer; } set { HttpContext.Session[Dealer.HttpContextKey] = value; } }

        private int InventoryId { get { return Convert.ToInt32(HttpContext.Session[WebSettings.InventoryIdContextKey]); } set { HttpContext.Session[WebSettings.InventoryIdContextKey] = value; } }

        private FacetAndFilter Facets { get { return HttpContext.Session[WebSettings.FacetsContextKey] as FacetAndFilter; } set { HttpContext.Session[WebSettings.FacetsContextKey] = value; } }

        private SearchInputs SearchOption { get { return HttpContext.Session[WebSettings.ProfitMaxSearchOptionsKey] as SearchInputs; } set { HttpContext.Session[WebSettings.ProfitMaxSearchOptionsKey] = value; } }

        private SearchCriteriaViewModel SearchCriteria { get { return HttpContext.Session[WebSettings.ProfitMaxSearchCriteriaKey] as SearchCriteriaViewModel; } set { HttpContext.Session[WebSettings.ProfitMaxSearchCriteriaKey] = value; } }

        private List<MarketListingData> MarketListing { get { return HttpContext.Session[WebSettings.MarketListingContextKey] as List<MarketListingData>; } set { HttpContext.Session[WebSettings.MarketListingContextKey] = value; } }

        private string Vin
        {
            get
            {
                return Convert.ToString(HttpContext.Session[WebSettings.VinContextKey]);
            }
            set
            {
                HttpContext.Session[WebSettings.VinContextKey] = value;
            }
        }

        private InventoryInfo InventoryDetails
        {
            get
            {
                if (HttpContext.Session[WebSettings.InventoryInfoContextKey] == null)
                {
                    return new InventoryInfo();
                }
                return HttpContext.Session[WebSettings.InventoryInfoContextKey] as InventoryInfo;
            }
            set
            {
                HttpContext.Session[WebSettings.InventoryInfoContextKey] = value;
            }
        }

        public int PingBusinessUnitId
        {
            get
            {
                var businessUnit = this.HttpContext == null ? null : this.HttpContext.Items["BusinessUnit"] as BusinessUnit;

                if (businessUnit != null)
                    return businessUnit.Id;
                else
                {
                    return WorkflowState.BusinessUnit.Id;
                }
            }
        }
        private bool IsIncludeCurrentVehicle { get { return Convert.ToBoolean(HttpContext.Session[WebSettings.IncludeCurrentVehicleContextKey]); } set { HttpContext.Session[WebSettings.IncludeCurrentVehicleContextKey] = value; } }

        private int RecordCountWithZeroPricing { get { return Convert.ToInt32(HttpContext.Session[WebSettings.RecordCountWithZeroPricing]); } set { HttpContext.Session[WebSettings.RecordCountWithZeroPricing] = value; } }

        // GET: /PricingAnalysis/PingControllerOne/
        //
        public ActionResult Index(int inv, int? businessUnitId, string filter, int? usednew, int? agebucket, string searchText)
        {
            using (MiniProfiler.Current.Step("Index"))
            {
                if (string.IsNullOrEmpty(filter)) Response.Redirect(InventoryUrl);

                var model = new ViewOne();

                model.PageTitle = "PingOne | MAX : Online Inventory. Perfected.";
                int nextInventoryId = 0, previousInventoryId = 0;
                int selectedDistance = 0, minMileage = 0;
                int? maxMileage = 0;

                #region BusinessUnitDetails
                //Get BusinessUnit details
                int businessUnitIdLocal = Convert.ToInt32(businessUnitId) > 0 ? Convert.ToInt32(businessUnitId) : PingBusinessUnitId;
                var businessUnitBuilder = new BusinessUnitBuilder(businessUnitIdLocal);
                FirstLook.Merchandising.DomainModel.Dashboard.Abstract.IDashboardBusinessUnit BusinessUnit =
                    new FirstLook.Merchandising.DomainModel.Dashboard.Concrete.DashboardBusinessUnit();
                using (MiniProfiler.Current.Step("BusinessUnitBuilder"))
                {
                    Dealer = businessUnitBuilder.Build();
                }
                BusinessUnit.BusinessUnitId = Dealer.DealerId;
                BusinessUnit.Name = Dealer.OwnerName;
                //Used in the Header.cshtml
                ViewBag.BusinessUnit = BusinessUnit;
                ViewBag.IsReportViewer = Helper.IsReportViewer;
                ViewBag.IsMaxStandAlone = WebSettings.IsMaxStandAlone;
                ViewBag.IsAdministrator = Helper.IsAdministrator;
                ViewBag.IsSupportEnabled = false;
                ViewBag.HasPricingSummary = DealerPingAccess.CheckDealerPingAccess(BusinessUnit.BusinessUnitId);
                ViewBag.IsGldEnabled = GetMiscSettings.GetSettings(WorkflowState.BusinessUnitID).ShowGroupLevelDashboard;
                ViewBag.ShowOnlineClassifiedOverview = GetMiscSettings.GetSettings(BusinessUnit.BusinessUnitId).showOnlineClassifiedOverview;
                ViewBag.IsOemBranded = GetMiscSettings.GetSettings(BusinessUnit.BusinessUnitId).Franchise != Franchise.None;
                bool hasDealerupgrade = false, isPreOwned = false;
                //Update the present Inventory Id.
                InventoryId = inv;
                #endregion

                #region InventoryFilters
                WorkFlowHeaderLinks links = new WorkFlowHeaderLinks();
                //When Filter,useedNew and agebucket are passed
                if (!string.IsNullOrEmpty(filter))
                {
                    WorkflowType wFfilter = (WorkflowType)Enum.Parse(typeof(WorkflowType), filter);

                    if (!string.IsNullOrEmpty(searchText))
                    {
                        //Get the matches for the search terms
                        var allInventory = InventoryDataFilter.FetchListFromSession(Dealer.DealerId);

                        Bin bin = Bin.Create(searchText);
                        List<InventoryData> searchResults = new List<InventoryData>();

                        searchResults = InventoryDataFilter.WorkflowSearch(Dealer.DealerId, allInventory, 0, 9999, String.Empty, bin);

                        if (searchResults.Count > 1 || searchResults.Count == 0)
                        {
                            //More than 1 match, so send to the home page so they can click the desired vehicle
                            //OR no results found
                            Response.Redirect("~/Workflow/Inventory.aspx?search=" + searchText);
                        }
                        else
                        {
                            InventoryId = searchResults[0].InventoryID; // Set InventoryId it to the searched one.
                            inv = searchResults[0].InventoryID; // Set InventoryId it to the searched one.
                            LocateVehicleResults result = new LocateVehicleResults() { FoundResult = false, ActiveListType = wFfilter, AgeBucketId = agebucket, UsedOrNewFilter = Convert.ToInt32(usednew) };
                            //var inventoryList = GetList(BusinessUnitId, result.ActiveListType, result.AgeBucketId, result.UsedOrNewFilter);
                            var sortedInventoryData = new List<IInventoryData>();

                            sortedInventoryData = InventoryDataFilter
                                .FetchListFromSession(Dealer.DealerId)
                                .WorkflowFilter(wFfilter, ConditionFilters.CreateFromInt(Convert.ToInt32(usednew)),
                                    AgeBucketFilters.CreateFromBucketFilterMode(agebucket)).ToList();

                            //May be needed for the future use
                            //var sorter = new InventoryDataSorter();
                            //sortedInventoryData = sorter.SortInventory(filteredInventory, SortType.InventoryReceivedDate, SortDirection.Ascending);

                            int currentIndex = sortedInventoryData.FindIndex(x => x.InventoryID == InventoryId);

                            if (currentIndex == (sortedInventoryData.Count - 1))
                                links.NextUrl = "#";
                            if (currentIndex < 1)
                                links.PreviousUrl = "#";

                            links.PresentOutofTotal = (currentIndex + 1) + " of " + sortedInventoryData.Count;
                            if ((sortedInventoryData.Count - 1) > currentIndex)
                                nextInventoryId = sortedInventoryData[currentIndex + 1].InventoryID; //Next Inventory Id

                            if (currentIndex >= 1 && currentIndex != (sortedInventoryData.Count - 1))
                                previousInventoryId = sortedInventoryData[currentIndex - 1].InventoryID; //Previous Inventory Id

                            if (sortedInventoryData.Any(i => i.InventoryID.Equals(searchResults[0].InventoryID)))
                            {
                                Log.DebugFormat("The InventoryIdList contains the specified inventoryId {0}.", searchResults[0].InventoryID);

                                result.FoundResult = true;
                            }

                            if (result.FoundResult)
                            {
                                InventoryId = searchResults[0].InventoryID;
                                inv = searchResults[0].InventoryID; // Set InventoryId it to the searched one.
                                Vin = searchResults[0].VIN;
                                wFfilter = result.ActiveListType;
                                usednew = result.UsedOrNewFilter;
                                agebucket = result.AgeBucketId;

                                //MAXSelling Link which depends on these bool values.
                                isPreOwned = searchResults[0].IsPreOwned();
                                hasDealerupgrade = WorkflowState.BusinessUnit.HasDealerUpgrade(Upgrade.Marketing);
                            }
                        }
                    }
                    else
                    {
                        LocateVehicleResults result = new LocateVehicleResults() { FoundResult = false, ActiveListType = wFfilter, AgeBucketId = agebucket, UsedOrNewFilter = Convert.ToInt32(usednew) };
                        var sortedInventoryData = new List<IInventoryData>();

                        sortedInventoryData = InventoryDataFilter
                            .FetchListFromSession(Dealer.DealerId)
                            .WorkflowFilter(wFfilter, ConditionFilters.CreateFromInt(Convert.ToInt32(usednew)),
                                AgeBucketFilters.CreateFromBucketFilterMode(agebucket)).ToList();

                        //May be needed for the future use
                        //var sorter = new InventoryDataSorter();
                        //sortedInventoryData = sorter.SortInventory(filteredInventory, SortType.InventoryReceivedDate, SortDirection.Ascending);

                        int currentIndex = sortedInventoryData.FindIndex(x => x.InventoryID == InventoryId);

                        if (currentIndex == (sortedInventoryData.Count - 1))
                            links.NextUrl = "#";
                        if (currentIndex < 1)
                            links.PreviousUrl = "#";

                        links.PresentOutofTotal = (currentIndex + 1) + " of " + sortedInventoryData.Count;
                        if ((sortedInventoryData.Count - 1) > currentIndex)
                            nextInventoryId = sortedInventoryData[currentIndex + 1].InventoryID; //Next Inventory Id

                        if (currentIndex >= 1 && currentIndex != (sortedInventoryData.Count - 1))
                            previousInventoryId = sortedInventoryData[currentIndex - 1].InventoryID; //Previous Inventory Id

                        if (currentIndex != -1)
                        {
                            //MAXSelling Link which depends on these bool values.
                            isPreOwned = sortedInventoryData[currentIndex].IsPreOwned();
                            hasDealerupgrade = WorkflowState.BusinessUnit.HasDealerUpgrade(Upgrade.Marketing);
                        }
                    }

                    links.LoadEquipmentLink = WorkflowState.CreateUrl("~/Workflow/LoadEquipment.aspx", InventoryId, wFfilter, Convert.ToInt32(usednew), agebucket);
                    links.LoadPackagesLink = WorkflowState.CreateUrl("~/Workflow/LoadPackages.aspx", InventoryId, wFfilter, Convert.ToInt32(usednew), agebucket);
                    links.LoadPhotosLink = WorkflowState.CreateUrl("~/Workflow/Photos.aspx", InventoryId, wFfilter, Convert.ToInt32(usednew), agebucket);
                    links.PricingPageLink = String.Format("~/PricingAnalysis/PingOne?inv={0}&businessUnitId={1}&filter={2}&usednew={3}&agebucket={4}", InventoryId, businessUnitIdLocal, wFfilter, Convert.ToInt32(usednew), agebucket);
                    links.ApprovalLink = WorkflowState.CreateUrl("~/Workflow/ApprovalSummary.aspx", InventoryId, wFfilter, Convert.ToInt32(usednew), agebucket);
                    links.PrintWindowSticker = string.Format("~/PricingAnalysis/PWS?businessUnitId={0}&inv={1}", businessUnitIdLocal, InventoryId);

                    links.BulkUpload = _photoServices.GetBulkUploadManagerUrl(businessUnitIdLocal, PhotoManagerContext.MAX);

                    if (isPreOwned && hasDealerupgrade)
                        links.MAXSellingLink = InventoryData.GetPricingLink(businessUnitIdLocal, InventoryId, ConfigurationManager.AppSettings["marketing_page_from_host"]);

                    if (links.NextUrl == null)
                        links.NextUrl = Url.Content("~/PricingAnalysis/PingOne?inv=" + nextInventoryId + "&businessUnitId=" + businessUnitIdLocal + "&filter=" + filter + "&usednew=" + usednew + "&agebucket=" + agebucket);
                    if (links.PreviousUrl == null)
                        links.PreviousUrl = Url.Content("~/PricingAnalysis/PingOne?inv=" + previousInventoryId + "&businessUnitId=" + businessUnitIdLocal + "&filter=" + filter + "&usednew=" + usednew + "&agebucket=" + agebucket);
                    links.InventoryId = InventoryId;
                    links.BusinessUnitId = businessUnitIdLocal;
                    links.Filter = filter;
                    links.UsedNew = usednew;
                    links.AgeBucket = agebucket;
                    model.links = links;
                }

                #endregion

                #region SearchCriteria

                using (MiniProfiler.Current.Step("SearchCriteria"))
                {
                    SearchCriteria = GetSearchCriteria(businessUnitIdLocal, inv);
                }

                #endregion

                #region Get Inventory Details

                var builder = new InventoryHeaderModelBuilder(Dealer.DealerId, inv);
                InventoryHeader inventoryHeader;
                using (MiniProfiler.Current.Step("InventoryHeaderModelBuilder"))
                {
                    inventoryHeader = builder.Build();
                }
                Vin = inventoryHeader.Vin;
                model.InventoryHeader = inventoryHeader;
                model.InventoryHeader.Color = model.InventoryHeader.Color;

                var vehicleMarketInfo = _mappingClient.GetVehicleMarketInfo(Convert.ToInt32(inventoryHeader.ChromeStyleId));

                InventoryInfo InventoryInfo = new InventoryInfo();
                InventoryInfo.InventoryId = InventoryId;
                InventoryInfo.ListPrice = inventoryHeader.ListPrice;
                InventoryInfo.Vin = inventoryHeader.Vin;
                InventoryInfo.Make = vehicleMarketInfo.Make;
                InventoryInfo.Model = vehicleMarketInfo.Model;
                InventoryInfo.Year = vehicleMarketInfo.Year;
                InventoryInfo.Age = inventoryHeader.AgeInDays;
                InventoryInfo.Color = inventoryHeader.Color;
                InventoryInfo.Distance = Dealer.PingIIDefaultSearchRadius;
                InventoryInfo.Mileage = inventoryHeader.Mileage;
                InventoryInfo.IsCertified = Convert.ToBoolean(inventoryHeader.Certified);
                InventoryInfo.Trim = inventoryHeader.Trim;
                InventoryInfo.ChromeStyleId = Convert.ToInt32(inventoryHeader.ChromeStyleId);
                InventoryInfo.StockNumber = inventoryHeader.StockNumber;
                InventoryInfo.InternetPrice = Convert.ToInt32(inventoryHeader.ListPrice);
                InventoryInfo.UnitCost = Convert.ToInt32(inventoryHeader.UnitCost);
                InventoryDetails = InventoryInfo;

                #endregion

                model.SearchOption = new SearchInputs();
                model.SearchOption.Listings = GetListings(0, 0);
                model.SearchOption.UpdateOverall =true;
                if (SearchCriteria != null)
                {
                    selectedDistance = SearchCriteria.Distance;
                    minMileage = 0;
                    maxMileage = null;
                    //minMileage = SearchCriteria.MinMileage;
                    //maxMileage = SearchCriteria.MaxMileage;
                    model.SearchOption.Listings.FirstOrDefault(lst => lst.DisplayId == PricingAnalysisEnums.RecentActiveDisplayId).IsSelected = SearchCriteria.Listings.Contains(PricingAnalysisEnums.RecentActiveDisplayName);
                    model.SearchOption.Listings.FirstOrDefault(lst => lst.DisplayId == PricingAnalysisEnums.ActiveDisplayId).IsSelected = SearchCriteria.Listings.Contains(PricingAnalysisEnums.ActiveDisplayName);
                }
                else
                {
                    selectedDistance = Dealer.PingIIDefaultSearchRadius;
                    minMileage = 0;
                    maxMileage = null;
                }

              
                List<FilterItem> distanceList = new List<FilterItem>();
                if (Dealer.DistanceList != null || Dealer.DistanceList.Count > 0)
                {
                    Dealer.DistanceList.ForEach(dl => distanceList.Add(new FilterItem() { DisplayId = Convert.ToString(dl), DisplayName = Convert.ToString(dl), IsSelected = selectedDistance == dl }));
                }
                else
                {
                    model.SearchOption.DefaultDistanceList.ForEach(dl => distanceList.Add(new FilterItem() { DisplayId = Convert.ToString(dl), DisplayName = Convert.ToString(dl), IsSelected = selectedDistance == dl }));
                    Dealer.DistanceList = model.SearchOption.DefaultDistanceList;
                }

                model.SearchOption.Distance = distanceList;

                model.SearchOption.MinMileageValue = minMileage;
                model.SearchOption.MaxMileageValue = maxMileage;

                //Check for save price only link to show or hide
                model.ShowSavePriceOnly = GetAutoApproveSettings.GetSettings(businessUnitIdLocal).Status == AutoApproveStatus.Off ? true : false;

                MysteryLinks mysteryLinks = new MysteryLinks()
                {

                    Make = inventoryHeader.Make,
                    StartYear = inventoryHeader.Year,
                    EndYear = inventoryHeader.Year,
                    Model = inventoryHeader.Model,
                    ListingCount = 0,
                    Radius = Dealer.PingIIDefaultSearchRadius,
                    Zipcode = Dealer.ZipCode,
                    CarsDotComMakeID = inventoryHeader.CarsDotComMakeID,
                    CarsDotComModelId = inventoryHeader.CarsDotComModelId,
                    AutoTraderMake = inventoryHeader.AutoTraderMake,
                    AutoTraderModel = inventoryHeader.AutoTraderModel
                };
                String autotrader = "http://www.autotrader.com/fyc/searchresults.jsp?advanced=&num_records=25&certified=&isp=y&search=y&lang=en&search_type=used&min_price=&max_price=&rdpage=100&make=" + mysteryLinks.Make + "&model=" + mysteryLinks.Model + "&start_year=" + mysteryLinks.StartYear + "&end_year=" + mysteryLinks.EndYear + "&Distance=" + mysteryLinks.Radius + "&Address=" + mysteryLinks.Zipcode;
                mysteryLinks.AutoTraderUrl = autotrader;
                model.MysteryLink = mysteryLinks;

                SearchOption = model.SearchOption;

                GetMiscSettings MiscSettings = GetMiscSettings.GetSettings(businessUnitIdLocal);
                model.ShowCtrGraph = MiscSettings.ShowCtrGraph;

                return View(model);

            }
        }

        // POST: /PricingAnalysis/PingControllerOne/Create
        //
        [HttpPost]
        public JsonResult Refine(string refinement)
        {
            try
            {
                return Json(new CorePostResponse() { Success = true, Info = refinement });
            }
            catch (Exception e)
            {
                Log.Error("An unexpected error occured.", e);
                Response.StatusCode = HttpStatusCode.InternalServerError.ToCode();
            }
            return Json(new CorePostResponse() { Success = false, Info = "Something went wrong." });
        }

        [HttpPost]
        public JsonResult Marketlistings(GridDataRequest gridDataRequest)
        {
            using (MiniProfiler.Current.Step("Marketlistings"))
            {
                JqGridDataModel objModel = null;
                List<SearchMarketListings> marketListingsInput = new List<SearchMarketListings>();
                SearchOption = gridDataRequest.SearchInputsObject;
                //marketListingsInput.Add(GetFilter(gridDataRequest.SearchInputsObject, gridDataRequest.sidx, gridDataRequest.sord, gridDataRequest.page, true));
                bool isIncluded = IncludeCurrentVehicle(gridDataRequest.SearchInputsObject);

                marketListingsInput.Add(GetFilter(gridDataRequest.SearchInputsObject, gridDataRequest.sidx, gridDataRequest.sord, gridDataRequest.page, true));

                if (gridDataRequest.page == 1 && isIncluded)
                    marketListingsInput[0].Size = PricingAnalysisEnums.DefaultPageSize - 1;
                else
                    marketListingsInput[0].Size = PricingAnalysisEnums.DefaultPageSize;

                marketListingsInput[0].Comment = "Market Listing grid";//Set Comment as Market listing Grid
                MarketListingsArgumentDto input = new MarketListingsArgumentDto()
                                                  {
                                                      RequestData =
                                                          new MarketListingRequestDto()
                                                          {
                                                              SearchMarketListings = marketListingsInput
                                                          }
                                                  };
                List<MarketListingData> listings;
                using (MiniProfiler.Current.Step("MarketListingsModelBuilder"))
                {
                    var builder = new MarketListingsModelBuilder(input, Dealer.Latitude, Dealer.Longitude);
                    listings = builder.Build();
                }
                this.MarketListing = listings;
                JsonResult jr = new JsonResult();
                int mileageAvg = 0, avgPrice = 0, precisionCount = 0;
                decimal? avgInternetPrice = 0;
                var resultListing = listings.FirstOrDefault();
                if (listings != null && listings.Count > 0)
                {
                    if (resultListing.MarketlistingsColumns.Count > 0)
                    {
                        RecordCountWithZeroPricing = resultListing.RecordCountWithZero;

                        avgInternetPrice = resultListing.MarketlistingsColumns.Average(mlc => mlc.InternetPrice);

                        if (isIncluded)
                        {
                            avgInternetPrice = InventoryDetails.InternetPrice == 0
                                ? avgInternetPrice
                                : (avgInternetPrice + InventoryDetails.InternetPrice);
                        }
                        avgInternetPrice = avgInternetPrice / resultListing.RecordCount;

                        mileageAvg = isIncluded ?
                                     Convert.ToInt32(Convert.ToInt32((resultListing.MileageSum + InventoryDetails.Mileage)) / (resultListing.MileageCount + 1))
                                     : resultListing.MileageAvg;

                        resultListing.MarketlistingsColumns[0].MileageAvg = mileageAvg == null ? 0 : mileageAvg;

                        avgPrice = resultListing.PriceAvg;

                        InventoryDetails.MarketAverage = avgPrice;

                        Facets = FacetsAndFilters(resultListing.Facets);

                        Dictionary<string, string> subGridData = new Dictionary<string, string>();
                        resultListing.MarketlistingsColumns.ForEach(li => subGridData.Add(li.VIN, li.AdText));

                        float recCount = float.Parse(resultListing.RecordCount.ToString()) /
                                         float.Parse(PricingAnalysisEnums.DefaultPageSize.ToString());
                        var jsubGridData = JsonConvert.SerializeObject(subGridData);
                        precisionCount = isIncluded ? Convert.ToInt32(resultListing.RecordCount) + 1 : Convert.ToInt32(resultListing.RecordCount);
                        objModel = new JqGridDataModel()
                                   {
                                       page = gridDataRequest.page,
                                       total = Convert.ToInt32(Math.Ceiling(recCount)),
                                       records = precisionCount,
                                       rows = resultListing.MarketlistingsColumns,
                                       sub = jsubGridData
                                   };

                        jr.Data = objModel;
                        jr.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                    }
                    else
                    {

                        Facets = FacetsAndFilters(resultListing.Facets != null ? resultListing.Facets : new List<FacetEquipments>());

                        precisionCount = isIncluded ? 1 : 0;
                        InventoryDetails.MarketAverage = isIncluded ? Convert.ToInt32(InventoryDetails.InternetPrice) : 0;
                        mileageAvg = isIncluded
                                ? Convert.ToInt32(
                                    Convert.ToInt32((resultListing.MileageSum + InventoryDetails.Mileage)) /
                                    (resultListing.MileageCount + 1))
                                : resultListing.MileageAvg;

                        objModel = new JqGridDataModel()
                        {
                            page = 1,
                            total = precisionCount,
                            records = precisionCount,
                            rows = resultListing.MarketlistingsColumns,
                        };
                    }

                    if (isIncluded && gridDataRequest.page == 1)
                    {
                        resultListing.MarketlistingsColumns.Insert(0, new MarketListingsColumnsModel()
                        {
                            Age = InventoryDetails.Age,
                            Seller = Dealer.OwnerName,
                            Color = InventoryDetails.Color,
                            Distance = 0,
                            MileageAvg = mileageAvg,
                            InternetPrice =
                                Convert.ToInt32(
                                    InventoryDetails.ListPrice),
                            IsCertified =
                                InventoryDetails.IsCertified,
                            Mileage = InventoryDetails.Mileage,
                            VIN = InventoryDetails.Vin,
                            VehicleDescription =
                                string.Format("{0} {1} {2}",
                                    InventoryDetails.Make,
                                    InventoryDetails.Model,
                                    InventoryDetails.Trim)
                        });
                    }
                }
                objModel.PricingcalculatorModel = GetPricingCalculator(listings, isIncluded, gridDataRequest.SearchInputsObject.InternetPrice);
                InventoryDetails.MarketAverage = objModel.PricingcalculatorModel.AvgListPrice;
                //Update Market Avg
                if (isIncluded && gridDataRequest.page == 1)
                {
                    resultListing.MarketlistingsColumns[0].PMarketAverage = Convert.ToInt32(CalculateMarketPercentage(Convert.ToDecimal(InventoryDetails.InternetPrice), Convert.ToDecimal(InventoryDetails.MarketAverage)));
                }

                jr.Data = objModel;
                jr.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

                //Save Criteria if it is updated
                if (gridDataRequest.SearchInputsObject.CriteriaUpdated)
                {
                    SearchCriteriaBuilder searchCriteriaBuilder = new SearchCriteriaBuilder(Dealer.DealerId, InventoryDetails.InventoryId);

                    searchCriteriaBuilder.SearchCriteria = new SearchCriteriaDto()
                                                         {
                                                             AvgMileage = mileageAvg,
                                                             AvgPrice = objModel.PricingcalculatorModel.AvgListPrice,
                                                             Certified = gridDataRequest.SearchInputsObject.Certified.FirstOrDefault(c => c.DisplayId == "Certified") == null ? false : gridDataRequest.SearchInputsObject.Certified.FirstOrDefault(c => c.DisplayId == "Certified").IsSelected,
                                                             NotCertified = gridDataRequest.SearchInputsObject.Certified.FirstOrDefault(c => c.DisplayId == "NonCertified") == null ? false : gridDataRequest.SearchInputsObject.Certified.FirstOrDefault(c => c.DisplayId == "NonCertified").IsSelected,
                                                             DealerId = Dealer.DealerId,
                                                             Distance = Convert.ToInt32(gridDataRequest.SearchInputsObject.Distance.FirstOrDefault().DisplayName),
                                                             InventoryId = InventoryDetails.InventoryId,
                                                             MaxMileage = Convert.ToInt32(gridDataRequest.SearchInputsObject.MaxMileageValue),
                                                             MaxPrice = objModel.PricingcalculatorModel.MaxListPrice,
                                                             MinMileage = Convert.ToInt32(gridDataRequest.SearchInputsObject.MinMileageValue),
                                                             MinPrice = objModel.PricingcalculatorModel.MinListPrice,
                                                             Vin = InventoryDetails.Vin,
                                                             OverallCount = InventoryDetails.OverallCount,
                                                             PrecisionCount = precisionCount,
                                                         };
                    searchCriteriaBuilder.SearchCriteria.DriveTrian = new List<string>();
                    searchCriteriaBuilder.SearchCriteria.Engine = new List<string>();
                    searchCriteriaBuilder.SearchCriteria.Fuel = new List<string>();
                    searchCriteriaBuilder.SearchCriteria.Listings = new List<string>();
                    searchCriteriaBuilder.SearchCriteria.Transmission = new List<string>();
                    searchCriteriaBuilder.SearchCriteria.Trim = new List<string>();

                    gridDataRequest.SearchInputsObject.DriveTrain.ForEach(dt => searchCriteriaBuilder.SearchCriteria.DriveTrian.Add(dt.DisplayName));
                    gridDataRequest.SearchInputsObject.Engine.ForEach(eg => searchCriteriaBuilder.SearchCriteria.Engine.Add(eg.DisplayName));
                    gridDataRequest.SearchInputsObject.Fuel.ForEach(fl => searchCriteriaBuilder.SearchCriteria.Fuel.Add(fl.DisplayName));
                    gridDataRequest.SearchInputsObject.Listings.ForEach(lt => searchCriteriaBuilder.SearchCriteria.Listings.Add(lt.DisplayName));
                    gridDataRequest.SearchInputsObject.Transmission.ForEach(ts => searchCriteriaBuilder.SearchCriteria.Transmission.Add(ts.DisplayName));
                    gridDataRequest.SearchInputsObject.Trim.ForEach(t => searchCriteriaBuilder.SearchCriteria.Trim.Add(t.DisplayName));

                    using (MiniProfiler.Current.Step("SearchCriteriaBuilder"))
                    {
                        searchCriteriaBuilder.Rebuild(new SearchCriteriaViewModel());
                    }
                }
                return jr;
            }
        }

        public ActionResult MileageBar(SearchInputs searchOption)
        {
            using (MiniProfiler.Current.Step("MileageBar"))
            {
                PriceMileageInputs model = new PriceMileageInputs();

                bool includeCurrentVehicle = IncludeCurrentVehicleInOverall(searchOption);

                model.MaxValue = searchOption.MaxMileageValue == null ? Convert.ToInt32(searchOption.MileageEndValue) : Convert.ToInt32(searchOption.MaxMileageValue);

                model.MinValue = Convert.ToInt32(searchOption.MinMileageValue);
                model.AveragePrice = Convert.ToInt32(InventoryDetails.Mileage);
                model.EndValue = Convert.ToInt32(searchOption.MileageEndValue);
                if (includeCurrentVehicle)
                    model.EndValue = Convert.ToInt32(searchOption.MileageEndValue) < Convert.ToInt32(InventoryDetails.Mileage) ? Convert.ToInt32(InventoryDetails.Mileage) : Convert.ToInt32(searchOption.MileageEndValue);

                model.StartValue = 0;

                model.OverallCount = includeCurrentVehicle ? searchOption.OverallCount + 1 : searchOption.OverallCount;

                if (InventoryDetails.ShowAlert)
                {
                    model.ShowAlert = true;
                }
                model.Mileage = Convert.ToInt32(InventoryDetails.Mileage);
                return PartialView("PriceMileagePartial", model);
            }
        }

        public CSVResult ExportListings(FormCollection frmCollection)
        {
            using (MiniProfiler.Current.Step("ExportListings"))
            {
                if (frmCollection.Get(0) != null || frmCollection.Get(0) != "")
                {
                    SearchInputs searchOption = JsonConvert.DeserializeObject<SearchInputs>(frmCollection.Get(0));
                    // (SearchInputs) frmCollection.GetKey(0);
                    List<SearchMarketListings> marketListingsInput = new List<SearchMarketListings>();
                    marketListingsInput.Add(GetFilter(searchOption, "", ""));
                    var defaultMarketListing = marketListingsInput.FirstOrDefault();
                    defaultMarketListing.Aggregations.Remove(PricingAnalysisEnums.Aggregations.equipment_terms.ToString());
                    defaultMarketListing.Comment = "Export Market Listing";

                    //update the size to receive proper market listings
                    defaultMarketListing.Size = RecordCountWithZeroPricing;

                    MarketListingsArgumentDto input = new MarketListingsArgumentDto()
                                                      {
                                                          RequestData =
                                                              new MarketListingRequestDto()
                                                              {
                                                                  SearchMarketListings =
                                                                      marketListingsInput
                                                              }
                                                      };
                    var builder = new MarketListingsModelBuilder(input, Dealer.Latitude, Dealer.Longitude);
                    List<MarketListingData> listings;
                    using (MiniProfiler.Current.Step("MarketListingsModelBuilder"))
                    {
                        listings = builder.Build();
                    }
                    List<ExportListingsColumnsModel> exportData = new List<ExportListingsColumnsModel>();

                    if (listings.FirstOrDefault().MarketlistingsColumns != null &&
                        listings.FirstOrDefault().MarketlistingsColumns.Count > 0)
                    {


                        listings.FirstOrDefault()
                            .MarketlistingsColumns.ForEach(mlc => exportData.Add(new ExportListingsColumnsModel()
                                                                                 {
                                                                                     Age = mlc.Age,
                                                                                     Seller = mlc.Seller,
                                                                                     VehicleDescription =
                                                                                         mlc.VehicleDescription,
                                                                                     Certified =
                                                                                         mlc.IsCertified == true
                                                                                             ? "Certified"
                                                                                             : "",
                                                                                     Color = mlc.Color,
                                                                                     Mileage = mlc.Mileage,
                                                                                     InternetPrice =
                                                                                         mlc.InternetPrice == null
                                                                                             ? "-"
                                                                                             : mlc.InternetPrice
                                                                                         .ToString(),
                                                                                     PMarketAverage =
                                                                                         Convert.ToString(
                                                                                             CalculateMarketPercentage(
                                                                                                 Convert.ToDecimal(
                                                                                                     mlc.InternetPrice),
                                                                                                 Convert.ToDecimal(
                                                                                                     InventoryDetails
                                                                                         .MarketAverage))),
                                                                                     // Convert.ToInt32(mlc.PMarketAverage),
                                                                                     Distance = mlc.Distance,
                                                                                     VIN = mlc.VIN,
                                                                                     Comments = mlc.AdText
                                                                                 }));


                    }
                    if (IsIncludeCurrentVehicle)
                    {
                        
                        exportData.Insert(0, new ExportListingsColumnsModel()
                                             {
                                                 Age = InventoryDetails.Age,
                                                 Color = InventoryDetails.Color,
                                                 Seller = Dealer.OwnerName,
                                                 Distance = 0,
                                                 InternetPrice =
                                                     InventoryDetails.ListPrice == null
                                                         ? "-"
                                                         : InventoryDetails.ListPrice.ToString(),
                                                 Certified = InventoryDetails.IsCertified == true ? "Certified" : "",
                                                 Mileage = InventoryDetails.Mileage,
                                                 PMarketAverage =InventoryDetails.ShowAlert?"N/A":Convert.ToString(CalculateMarketPercentage(Convert.ToDecimal(InventoryDetails.InternetPrice),Convert.ToDecimal(InventoryDetails.MarketAverage))),
                                                 VIN = InventoryDetails.Vin,
                                                 VehicleDescription =
                                                     string.Format("{0} {1} {2}", InventoryDetails.Make,
                                                         InventoryDetails.Model, InventoryDetails.Trim),
                                                 Comments = ""

                                             });
                    }
                    return new CSVResult(exportData, "MarketListngs.csv");
                    //return new CSVResult(new List<ExportListingsColumnsModel>(), "MarketListngs.csv");
                }
                else
                {
                    return new CSVResult(new List<ExportListingsColumnsModel>(), "MarketListngs.csv");
                }
            }
        }

        public ActionResult VehicleHistoryReport(bool pullAutoCheckReport = false, bool pullCarfaxReport = false)
        {
            using (MiniProfiler.Current.Step("VehicleHistoryReport"))
            {
                VehicleHistoryReport vehicleHistoryReport;
                var builder = new CarfaxModelBuilder(Dealer.DealerId, Vin, HttpContext.User.Identity.Name,
                    pullCarfaxReport);
                CarfaxVehicleHistory carfaxVehicleHistory;
                using (MiniProfiler.Current.Step("CarfaxVehicleHistoryBuilder"))
                {
                    carfaxVehicleHistory = builder.Build();
                }
                var autocheckBuilder = new AutocheckReportBuilder(Dealer.DealerId, Vin, HttpContext.User.Identity.Name,
                    pullAutoCheckReport);
                AutocheckVehicleHistory autocheckVehicleHistory;
                using (MiniProfiler.Current.Step("AutocheckVehicleHistoryBuilder"))
                {
                    autocheckVehicleHistory = autocheckBuilder.Build();
                }
                vehicleHistoryReport = new VehicleHistoryReport()
                                       {
                                           AutocheckVehicleHistoryReport =
                                               autocheckVehicleHistory,
                                           CarfaxVehicleHistoryReport = carfaxVehicleHistory,
                                       };

                return PartialView("VehicleHistoryPartialView", vehicleHistoryReport);
            }
        }

        public ActionResult LoadFacets(bool pullAutoCheckReport = false, bool pullCarfaxReport = false)
        {
            return PartialView("FilterPartialView", Facets);
        }

        public JsonResult PricingHistory()
        {
            using (MiniProfiler.Current.Step("PricingHistory"))
            {
                var pricingBuilder = new PricingHistoryModelBuilder(Dealer.DealerId, InventoryDetails.InventoryId);
                IList<PricingHistory> pricingHistoryList = pricingBuilder.Build();
                pricingHistoryList.OrderBy(phl => phl.historydate);
                JsonResult j = new JsonResult();
                j.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                j.Data = pricingHistoryList;
                return j;
            }
        }

        public bool SaveTrim(string chromeStyleId)
        {
            using (MiniProfiler.Current.Step("SaveTrim"))
            {
                var builder = new SaveTrimModelBuilder(Dealer.DealerId, InventoryDetails.InventoryId,
                    Convert.ToInt32(chromeStyleId));
                SaveSelectedTrimModel response;
                using (MiniProfiler.Current.Step("SaveSelectedTrimModel"))
                {
                    response = builder.Build();

                }
                
                var vehicleConfig = VehicleConfiguration.FetchOrCreateDetailed(Dealer.DealerId, InventoryId,
                    User.Identity.Name);
                vehicleConfig.Save(Dealer.DealerId, HttpContext.User.Identity.Name);
                var PricingCampaignManager = Registry.Resolve<IDiscountPricingCampaignManager>();
                var MessageSender = Registry.Resolve<IAdMessageSender>();
                PricingCampaignManager.ApplyDiscount(Dealer.DealerId, InventoryId, User.Identity.Name,
                    GetType().FullName, MessageSender);

                return response.Flag;
            }
        }

        public bool SaveInternetPrice(string internetprice)
        {
            using (MiniProfiler.Current.Step("SaveSelectedTrimModel"))
            {
                decimal InternetPrice = Convert.ToDecimal(internetprice);
                var builder = new SaveInternetPriceModelbuilder(Dealer.DealerId, InventoryDetails.InventoryId,
                    InternetPrice);
                UpdateInternetPriceModel response;
                using (MiniProfiler.Current.Step("SaveSelectedTrimModel"))
                {
                    response = builder.Build();
                }
                InventoryDetails.InternetPrice = Convert.ToInt32(InternetPrice);
                return true;
            }
        }

        public ActionResult PricingProof(bool? showAlert = false)
        {
            using (MiniProfiler.Current.Step("PricingProof"))
            {
                PricingProof PriceComparison;
                try
                {
                    List<int> distanceList = new List<int>() { 50, 100, 150, 200, 250, 500 };
                    MarketListingsArgumentDto input2 = new MarketListingsArgumentDto();
                    List<SearchMarketListings> lll = new List<SearchMarketListings>();
                    foreach (var dist in distanceList)
                    {
                        SearchMarketListings searchMarketListings = GetFilter(SearchOption, "", "");
                        searchMarketListings.Aggregations.Remove(
                            PricingAnalysisEnums.Aggregations.equipment_terms.ToString());

                        searchMarketListings.AggregateListings = null;
                        searchMarketListings.Distance = dist;
                        searchMarketListings.Size = 0;
                        searchMarketListings.From = 0;
                        searchMarketListings.Comment = "Pricing Proof";
                        lll.Add(searchMarketListings);
                    }

                    input2.RequestData = new MarketListingRequestDto();
                    input2.RequestData.SearchMarketListings = lll;
                    var selectedDistanceList = SearchOption.Distance.Where(a => a.IsSelected == true);
                    int selecDist = Dealer.PingIIDefaultSearchRadius;

                    if (selectedDistanceList != null)
                        selecDist = Convert.ToInt32(selectedDistanceList.FirstOrDefault().DisplayName);

                    var builder = new PricingProofBuilder(Dealer.DealerId,
                        new List<int>() { 
                            InventoryId },
                            (int)InventoryDetails.ListPrice,
                            input2,
                            selecDist,
                            IsIncludeCurrentVehicle);

                    using (MiniProfiler.Current.Step("PricingProofBuilder"))
                    {
                        PriceComparison = builder.Build();
                    }
                    PriceComparison.PriceComparisons.Insert(0,
                        new PriceComparison()
                        {
                            DisplayId = "marketAvg",
                            DisplayName = "Market Avg",
                            Threshold = InventoryDetails.MarketAverage,
                            DealerThreshold = Dealer.MarketAvgInternetPrice
                        });

                    foreach (var book in PriceComparison.PriceComparisons)
                    {
                        if (book.DisplayId.ToString().ToLower() == "kbb")
                        {
                            book.DealerThreshold = Dealer.KBBRetailValue;
                            book.DisplayName = book.DisplayName.ToUpper() + " Retail";
                        }
                        if (book.DisplayId.ToString().ToLower() == "nada")
                        {
                            book.DealerThreshold = Dealer.NADARetailValue;
                            book.DisplayName = book.DisplayName.ToUpper() + " Retail";
                        }
                        if (book.DisplayId.ToString().ToLower() == "edmunds")
                        {
                            book.DealerThreshold = Dealer.EdmundsTrueMarketValue;
                            book.DisplayName = "Edmunds Retail";
                        }
                        if (book.DisplayId.ToString().ToLower() == "msrp")
                        {
                            book.DealerThreshold = Dealer.OriginalMSRP;
                        }
                    }
                }
                catch (Exception ex)
                {
                    return Json("Error Loading Pricing Proof: " + ex.Message, JsonRequestBehavior.AllowGet);
                }
                ViewBag.Make = InventoryDetails.Make;
                ViewBag.StockNumber = InventoryDetails.StockNumber;
                ViewBag.ShowAlert = showAlert.Value;
                return PartialView("PricingProof", PriceComparison);
            }
        }

        public FacetAndFilter FacetsAndFilters(List<FacetEquipments> facetEquipments)
        {
            FacetAndFilter facetsFilter = new FacetAndFilter();
            List<FilterItem> filterList = new List<FilterItem>();

            foreach (var option in facetEquipments)
            {
                FilterItem filter = new FilterItem();
                filter.DisplayId = RemoveSpecialCharacters(option.Key);
                filter.DisplayName = option.Key;
                filter.Level = option.Level;
                filter.Count = IsIncludeCurrentVehicle ? (option.DocCount + 1) : option.DocCount;
                filter.IsSelected = false;
                filterList.Add(filter);
            }

            facetsFilter.TopLevelEquipments = filterList;
            return facetsFilter;
        }

        public SearchInputs DefineCompetitiveSet(List<MarketListingData> model, int chromeStyleId, int dealerId, int inventoryId, SearchInputs searchOption)
        {
            SearchInputs searchInputs = new SearchInputs()
            {
                AutoTraderMake = searchOption.AutoTraderMake,
                AutoTraderModel = searchOption.AutoTraderModel,
                AvgMileageValue = searchOption.AvgMileageValue,
                CarsDotComMakeID = searchOption.CarsDotComMakeID,
                CarsDotComModelId = searchOption.CarsDotComModelId,
                CriteriaUpdated = searchOption.CriteriaUpdated,
                EndYear = searchOption.EndYear,
                InternetPrice = searchOption.InternetPrice,
                ListingCount = searchOption.ListingCount,
                MatchingCount = searchOption.MatchingCount,
                MaxMileageValue = searchOption.MaxMileageValue,
                MileageUpdated = searchOption.MileageUpdated,
                MinMileageValue = searchOption.MinMileageValue,
                OverallCount = searchOption.OverallCount,
                PageNo = searchOption.PageNo,
                PageSize = searchOption.PageSize,
                RecordCount = searchOption.RecordCount,
                SelectedEquipment = searchOption.SelectedEquipment,
                ShowZeroListing = searchOption.ShowZeroListing,
                Size = searchOption.Size,
                StartYear = searchOption.StartYear,
                UpdateOverall = searchOption.UpdateOverall,
                Vin = searchOption.Vin,
                Zipcode = searchOption.Zipcode,
                MileageEndValue = searchOption.MileageEndValue,
                MileageStartValue = searchOption.MileageStartValue

            };
            var defaultMarketListing = model.FirstOrDefault();
            int allTrimCounts = defaultMarketListing.TrimTerms.Sum(t => t.DocCount);//defaultMarketListing.RecordCount;
            List<FilterItem> DriveTrain = new List<FilterItem>();
            List<FilterItem> Engine = new List<FilterItem>();
            List<FilterItem> Fuel = new List<FilterItem>();
            List<FilterItem> Transmission = new List<FilterItem>();
            List<FilterItem> Trim = new List<FilterItem>();
            List<FilterItem> Distance = new List<FilterItem>();
            List<FilterItem> Certified = new List<FilterItem>();
            List<FilterItem> Listings = new List<FilterItem>();

            //Add All trims value
            Trim.Add(new FilterItem()
            {
                Count = allTrimCounts,
                DisplayId = PricingAnalysisEnums.AllTrimsDisplayId,
                DisplayName = PricingAnalysisEnums.AllTrimsDisplayName,
                IsSelected = true
            }
            );

            #region Get Filters

            var response = _mappingClient.GetVehicleMarketInfo(chromeStyleId);
            Log.Debug("Elastic search Data:" + JsonConvert.SerializeObject(response));
            CultureInfo currentCulture = System.Threading.Thread.CurrentThread.CurrentCulture;
            TextInfo currentInfo = currentCulture.TextInfo;
            using (MiniProfiler.Current.Step("Loading Define Comp set"))
            {
                foreach (var returnvalue in response.VehicleMarketAttributeList)
                {
                    #region DriveTrain

                    DriveTrain = GetFilterItems(defaultMarketListing.DriveTrainTerms, returnvalue.DriveTrains, returnvalue.MatchesTargetVehicleTrim, DriveTrain);

                    #endregion

                    #region Engines

                    Engine = GetFilterItems(defaultMarketListing.EngineTerms, returnvalue.Engines, returnvalue.MatchesTargetVehicleTrim, Engine);

                    #endregion

                    #region Transmissions
                    Transmission = GetFilterItems(defaultMarketListing.TransmissionTerms, returnvalue.Transmissions, returnvalue.MatchesTargetVehicleTrim, Transmission);

                    #endregion

                    #region Trims

                    List<string> Trimlist = new List<string>() { returnvalue.Trim };

                    Trim = GetFilterItems(defaultMarketListing.TrimTerms, Trimlist, returnvalue.MatchesTargetVehicleTrim, Trim);

                    #endregion

                    #region FuelTypes

                    Fuel = GetFilterItems(defaultMarketListing.FuelTypeTerms, returnvalue.FuelTypes, returnvalue.MatchesTargetVehicleTrim, Fuel);

                    #endregion
                }
            }
            #endregion

            //Add Undefined Trim option only if Sum of trim aggregations is less than overall count
            var nullTrims = defaultMarketListing.TrimTerms.Where(t => t.Key == null); //Trim.Where(t => t.DisplayId != PricingAnalysisEnums.AllTrimsDisplayId).Sum(t => t.Count);
            if (nullTrims !=null && nullTrims.Count()>0)
            {
                Trim.Add(new FilterItem()
                {
                    Count = nullTrims.FirstOrDefault().DocCount,
                    DisplayId = PricingAnalysisEnums.UndefinedDisplayId,
                    DisplayName = PricingAnalysisEnums.UndefinedDisplayName,
                    IsSelected = false
                }
            );
            }
            //If a trim is selected deselect all
            var selectedTrims = Trim.Where(t => t.IsSelected == true);
            if (selectedTrims.Count() > 1)
            {
                Trim.FirstOrDefault(t => t.DisplayId == PricingAnalysisEnums.AllTrimsDisplayId).IsSelected = false;
            }

            #region Distance and Certified
            List<int> distanceList = Dealer.DistanceList.ToList();

            foreach (var distance in distanceList)
            {
                FilterItem item = new FilterItem();
                item.DisplayId = distance.ToString();
                item.DisplayName = distance.ToString();
                item.IsSelected = Convert.ToInt32(item.DisplayId) == Dealer.PingIIDefaultSearchRadius ? true : false;
                Distance.Add(item);
            }

            var certified = defaultMarketListing.CertifiedTerms.FirstOrDefault(ct => ct.Key.ToLower() == "t");
            Certified.Add(new FilterItem() { DisplayId = "Certified", DisplayName = "Certified", IsSelected = true, Count = certified == null ? 0 : certified.DocCount });

            var notCertified = defaultMarketListing.CertifiedTerms.FirstOrDefault(ct => ct.Key.ToLower() == "f");
            Certified.Add(new FilterItem()
                          {
                              DisplayId = "NonCertified",
                              DisplayName = "Not Certified",
                              IsSelected = true,
                              Count = notCertified == null ? 0 : notCertified.DocCount
                          });
            if (searchInputs.UpdateOverall)
            {
                Listings = GetListings(defaultMarketListing.ActiveCount, defaultMarketListing.RecentActiveCount);
            }
            else
            {
                Listings = searchOption.AllListings;
            }
            searchInputs.AllListings = Listings;
            #endregion

            #region Select Filters from search criteria
            if (SearchCriteria != null && !searchOption.CriteriaUpdated)
            {
                Certified.FirstOrDefault(c => c.DisplayId == "Certified").IsSelected = SearchCriteria.Certified;
                Certified.FirstOrDefault(c => c.DisplayId == "NonCertified").IsSelected = SearchCriteria.NotCertified;

                Listings.FirstOrDefault(lst => lst.DisplayId == PricingAnalysisEnums.RecentActiveDisplayId).IsSelected = SearchCriteria.Listings.Contains(PricingAnalysisEnums.RecentActiveDisplayName);
                Listings.FirstOrDefault(lst => lst.DisplayId == PricingAnalysisEnums.ActiveDisplayId).IsSelected = SearchCriteria.Listings.Contains(PricingAnalysisEnums.ActiveDisplayName);

                DriveTrain.ForEach(dt => dt.IsSelected = SearchCriteria.DriveTrian.FirstOrDefault(dtt => dtt.ToLower() == dt.DisplayName.ToLower()) != null && SearchCriteria.DriveTrian.FirstOrDefault(dtt => dtt.ToLower() == dt.DisplayName.ToLower()).Count() > 0);
                Engine.ForEach(dt => dt.IsSelected = SearchCriteria.Engine.FirstOrDefault(dtt => dtt.ToLower() == dt.DisplayName.ToLower()) != null && SearchCriteria.Engine.FirstOrDefault(dtt => dtt.ToLower() == dt.DisplayName.ToLower()).Count() > 0);
                Fuel.ForEach(dt => dt.IsSelected = SearchCriteria.Fuel.FirstOrDefault(dtt => dtt.ToLower() == dt.DisplayName.ToLower()) != null && SearchCriteria.Fuel.FirstOrDefault(dtt => dtt.ToLower() == dt.DisplayName.ToLower()).Count() > 0);
                Transmission.ForEach(dt => dt.IsSelected = SearchCriteria.Transmission.FirstOrDefault(dtt => dtt.ToLower() == dt.DisplayName.ToLower()) != null && SearchCriteria.Transmission.FirstOrDefault(dtt => dtt.ToLower() == dt.DisplayName.ToLower()).Count() > 0);
                Trim.ForEach(dt => dt.IsSelected = SearchCriteria.Trim.FirstOrDefault(dtt => dtt.ToLower() == dt.DisplayName.ToLower()) != null && SearchCriteria.Trim.FirstOrDefault(dtt => dtt.ToLower() == dt.DisplayName.ToLower()).Count() > 0);
                Distance.ForEach(dt => dt.IsSelected = SearchCriteria.Distance == Convert.ToInt32(dt.DisplayName));
                searchInputs.MinMileageValue = SearchCriteria.MinMileage;
                if (SearchCriteria.MaxMileage == 0)
                    searchInputs.MaxMileageValue = null;
                else
                    searchInputs.MaxMileageValue = SearchCriteria.MaxMileage;

            }
            #endregion

            #region Reselect Filters which User selected before updating
            if (searchOption.Certified != null && searchOption.Certified.Count > 0)
            {
                Certified.ForEach(d => d.IsSelected = false);
                searchOption.Certified.ForEach(so => Certified.Where(dcs => dcs.DisplayName.ToLower()==so.DisplayName.ToLower()).ForEach(dc => dc.IsSelected = so.IsSelected));
            }
            if (searchOption.DriveTrain != null && searchOption.DriveTrain.Count > 0)
            {
                DriveTrain.ForEach(dt => dt.IsSelected = false);
                searchOption.DriveTrain.ForEach(so => DriveTrain.Where(dcs => dcs.DisplayName.ToLower() == so.DisplayName.ToLower()).ForEach(dc => dc.IsSelected = so.IsSelected));
            }
            if (searchOption.Engine != null && searchOption.Engine.Count > 0)
            {
                Engine.ForEach(e => e.IsSelected = false);
                searchOption.Engine.ForEach(so => Engine.Where(dcs => dcs.DisplayName.ToLower() == so.DisplayName.ToLower()).ForEach(dc => dc.IsSelected = so.IsSelected));
            }
            if (searchOption.Fuel != null && searchOption.Fuel.Count > 0)
            {
                Fuel.ForEach(f => f.IsSelected = false);
                searchOption.Fuel.ForEach(so => Fuel.Where(dcs => dcs.DisplayName.ToLower() == so.DisplayName.ToLower()).ForEach(dc => dc.IsSelected = so.IsSelected));
            }
            if (searchOption.Transmission != null && searchOption.Transmission.Count > 0)
            {
                Transmission.ForEach(t => t.IsSelected = false);
                searchOption.Transmission.ForEach(so => Transmission.Where(dcs => dcs.DisplayName.ToLower() == so.DisplayName.ToLower()).ForEach(dc => dc.IsSelected = so.IsSelected));
            }
            if (searchOption.Trim != null && searchOption.Trim.Count > 0)
            {
                Trim.ForEach(t => t.IsSelected = false);
                searchOption.Trim.ForEach(so => Trim.Where(dcs => dcs.DisplayName.ToLower() == so.DisplayName.ToLower()).ForEach(dc => dc.IsSelected = so.IsSelected));
            }
            if (searchOption.Distance != null && searchOption.Distance.Count > 0)
            {
                Distance.ForEach(d => d.IsSelected = false);
                searchOption.Distance.FirstOrDefault(so => Distance.FirstOrDefault(dcs => dcs.DisplayName.ToLower() == so.DisplayName.ToLower()).IsSelected = true);
            }
            if (searchOption.Listings != null && searchOption.Listings.Count > 0)
            {
                Listings.ForEach(l => l.IsSelected = false);
                searchOption.Listings.ForEach(so => Listings.FirstOrDefault(dcs => dcs.DisplayName.ToLower() == so.DisplayName.ToLower()).IsSelected = true);
            }

            #endregion

            searchInputs.DriveTrain = DriveTrain;
            searchInputs.Engine = Engine;
            searchInputs.Fuel = Fuel;
            searchInputs.Transmission = Transmission;
            searchInputs.Trim = Trim;

            searchInputs.Listings = Listings;
            searchInputs.Distance = Distance;
            searchInputs.Certified = Certified;

            if (model != null && model.Count > 0)
                searchInputs.RecordCount = model[0].RecordCount;

            return searchInputs;
        }

        public ActionResult DefineSearchFilters(int chromeStyleId, SearchInputs searchOption)
        {
            using (MiniProfiler.Current.Step("DefineSearchFilters"))
            {
                List<SearchMarketListings> searchMarketListings = new List<SearchMarketListings>();
                 SearchInputs searchInput=null; 
                if (searchOption.UpdateOverall)
                {
                    searchMarketListings.Add(GetOverallCriteria(InventoryDetails, Dealer, searchOption));

                    MarketListingsArgumentDto input = new MarketListingsArgumentDto() { RequestData = new MarketListingRequestDto() { SearchMarketListings = searchMarketListings } };

                    MarketListingsModelBuilder listingBuilder = new MarketListingsModelBuilder(input,Dealer.Latitude,Dealer.Longitude);
                    List<MarketListingData> overallList = new List<MarketListingData>();
                    using (MiniProfiler.Current.Step("MarketListingBuilder-Overall"))
                    {
                        overallList = listingBuilder.Build();
                    }
                    searchInput = DefineCompetitiveSet(overallList, Convert.ToInt32(chromeStyleId),
                    Dealer.DealerId, InventoryDetails.InventoryId, searchOption);

                    if (overallList != null && overallList.Count()>0)
                    {
                        searchInput.OverallCount = Convert.ToInt32(overallList.FirstOrDefault().RecordCount);

                        searchInput.MileageEndValue = Convert.ToInt32(overallList.FirstOrDefault().OdometerStats.Max);
                        searchInput.MileageStartValue = 0;
                        searchInput.MinMileageValue = 0;
                        searchInput.MaxMileageValue = searchOption.MaxMileageValue;
                    }
                    else
                    {
                        searchInput.OverallCount = 0;
                        searchInput.MileageEndValue = 0;
                        searchInput.MileageStartValue = 0;
                        searchInput.MinMileageValue = 0;
                        searchInput.MaxMileageValue = 0;
                        InventoryDetails.ShowAlert = true;
                    }
                }
                else
                {   
                    searchInput = DefineCompetitiveSet(this.MarketListing, Convert.ToInt32(chromeStyleId),
                    Dealer.DealerId, InventoryDetails.InventoryId, searchOption);
                }
                searchInput.UpdateOverall = false;
                this.SearchOption = searchInput;
                return PartialView("DefineCompetitiveSet", searchInput);
            }
        }

        public bool SaveInternetPriceNApproveAd(string internetprice, string oldprice = "0", string specialprice = "0", string pMarketAvg = "0")
        {
            using (MiniProfiler.Current.Step("MarketListingBuilder-Overall"))
            {
                decimal internetPrice = Convert.ToDecimal(internetprice);
                decimal oldPrice = Convert.ToDecimal(oldprice);
                decimal specialPrice = Convert.ToDecimal(specialprice);
                decimal percentMarketAvg = CalculateMarketPercentage(internetPrice, Convert.ToDecimal(InventoryDetails.MarketAverage));
                var builder = new SaveInternetPricenApproveAdModelbuilder(Dealer.DealerId, InventoryDetails.InventoryId,
                    oldPrice, internetPrice, specialPrice, HttpContext.User.Identity.Name);
                UpdateInternetPriceNApproveAdModel response;
                using (MiniProfiler.Current.Step("MarketListingBuilder-Overall"))
                {
                    response = builder.Build();
                }
                if (response.AdApproved)
                {
                    InventoryDetails.ListPrice = (int)internetPrice;
                    InventoryDetails.PMarketAverage = percentMarketAvg;
                    InventoryDetails.InternetPrice = Convert.ToInt32(internetPrice);
                    return true;
                }
                return false;
            }
        }

        #region Private Methods
        private SearchMarketListings GetFilter(SearchInputs searchOption, string sortColumn, string sortOrder, int pageNo = 0, bool getRanks = false)
        {
            int selectedDistance = 0;
            string selectedListing;
            bool certification = false;
            List<string> driveTrain = new List<string>();
            List<string> Engine = new List<string>();
            List<string> Trim = new List<string>();
            List<string> Transmission = new List<string>();
            List<string> Fuel = new List<string>();
            List<string> Listing = new List<string>();
            List<string> Distance = new List<string>();

            foreach (var obj in searchOption.DriveTrain)
            {
                if (obj.IsSelected == true)
                    driveTrain.Add(obj.DisplayName);
            }
            foreach (var obj in searchOption.Engine)
            {
                if (obj.IsSelected == true)
                    Engine.Add(obj.DisplayName);
            }
            foreach (var obj in searchOption.Fuel)
            {
                if (obj.IsSelected == true)
                    Fuel.Add(obj.DisplayName);
            }
            foreach (var obj in searchOption.Transmission)
            {
                if (obj.IsSelected == true)
                    Transmission.Add(obj.DisplayName);
            }
            foreach (var obj in searchOption.Trim)
            {
                if (obj.IsSelected == true && obj.DisplayId != PricingAnalysisEnums.AllTrimsDisplayId)
                {
                    if (obj.DisplayId != PricingAnalysisEnums.UndefinedDisplayId)
                        Trim.Add(obj.DisplayName);
                    else
                        Trim.Add(null);
                }
            }
            foreach (var obj in searchOption.Distance)
            {
                if (obj.IsSelected == true)
                    selectedDistance = Convert.ToInt32(obj.DisplayName);
            }

            foreach (var obj in searchOption.Certified)
            {
                if (obj.IsSelected == true && obj.DisplayName == "Certified")
                    certification = true; ;
            }


            if (pageNo == 1)
                pageNo = 0;
            else
                pageNo = (pageNo - 1) * (PricingAnalysisEnums.DefaultPageSize - 1);

            SearchMarketListings objt = new SearchMarketListings()
            {
                BusinessUnitId = Dealer.DealerId,
                InventoryId = InventoryDetails.InventoryId,
                Year = InventoryDetails.Year,
                Make = InventoryDetails.Make,
                Model = InventoryDetails.Model,
                Size = searchOption.RecordCount,
                IncludeZeroPrice = searchOption.ShowZeroListing,
                From = pageNo,
                GetRanks = getRanks,
                Aggregations = new List<string> { PricingAnalysisEnums.Aggregations.equipment_terms.ToString(),
                    PricingAnalysisEnums.Aggregations.price_stats.ToString(),
                    PricingAnalysisEnums.Aggregations.trim_terms.ToString(),
                    PricingAnalysisEnums.Aggregations.transmission_terms.ToString(),
                    PricingAnalysisEnums.Aggregations.drive_train_terms.ToString(),
                    PricingAnalysisEnums.Aggregations.oem_certified_terms.ToString(),
                    PricingAnalysisEnums.Aggregations.engine_terms.ToString(),
                    PricingAnalysisEnums.Aggregations.fuel_type_terms.ToString(),
                    PricingAnalysisEnums.Aggregations.odometer_stats.ToString()
                },

            };

            if (Dealer.Latitude != null && Dealer.Longitude != null)
            {
                objt.Latitude = Convert.ToString(Dealer.Latitude);
                objt.Longitude = Convert.ToString(Dealer.Longitude);
                objt.Distance = selectedDistance;
            }

            if (searchOption.Listings != null && searchOption.Listings.Count > 0)
            {
                if (searchOption.Listings.FirstOrDefault(lt => lt.IsSelected == true).DisplayId.ToLower() == PricingAnalysisEnums.RecentActiveDisplayId)
                {
                    objt.RecentActive = true;
                }
                else
                {
                    objt.ActiveOnly = true;
                }
            }

            if (driveTrain != null && driveTrain.Count != 0)
            {
                objt.DriveTrains = driveTrain;
            }
            if (Engine != null && Engine.Count != 0)
            {
                objt.Engines = Engine;
            }
            if (Fuel != null && Fuel.Count != 0)
            {
                objt.FuelTypes = Fuel;
            }
            if (Transmission != null && Transmission.Count != 0)
            {
                objt.Transmissions = Transmission;
            }
            if (Trim != null && Trim.Count != 0)
            {
                objt.Trims = Trim;
            }
            if (selectedDistance != null)
            {
                objt.Distance = selectedDistance;
            }
            if (searchOption.MinMileageValue != null && searchOption.MinMileageValue != 0)
            {
                objt.MinMileage = searchOption.MinMileageValue;
            }
            if (searchOption.MaxMileageValue != null)
            {
                objt.MaxMileage = searchOption.MaxMileageValue;
            }
            if (searchOption.SelectedEquipment != null && searchOption.SelectedEquipment.Count != 0)
            {
                objt.Equipment = searchOption.SelectedEquipment;
            }
            if (searchOption.Certified.Where(c => c.IsSelected).Count() == 1)
            {
                objt.OemCertified = certification;
            }

            if (!string.IsNullOrEmpty(sortColumn))
            {
                if (sortColumn.ToLower() == "vehicledescription")
                {
                    objt.Sorts = new List<Sorts>();
                    objt.Sorts.Add(new Sorts() { Field = "year", Order = sortOrder });
                    objt.Sorts.Add(new Sorts() { Field = "make", Order = sortOrder });
                    objt.Sorts.Add(new Sorts() { Field = "model", Order = sortOrder });
                    objt.Sorts.Add(new Sorts() { Field = "trim", Order = sortOrder });
                }
                else
                {
                    objt.Sorts = new List<Sorts>();
                    objt.Sorts.Add(new Sorts() { Field = sortColumn, Order = sortOrder });
                }
            }

            return objt;
        }

        private bool IncludeCurrentVehicle(SearchInputs searchInputsObject)
        {
            bool isInclude = false;

            if ((searchInputsObject.Trim == null || searchInputsObject.Trim.Count == 0 || (searchInputsObject.Trim.Count == 1 && searchInputsObject.Trim[0].DisplayId == "AllTrims") || searchInputsObject.Trim.Any(str => InventoryDetails.Trims.Contains(str.DisplayName.ToLower())))
                        && (searchInputsObject.Transmission == null || searchInputsObject.Transmission.Count == 0 || searchInputsObject.Transmission.Any(str => InventoryDetails.Transmission.Contains(str.DisplayName.ToLower())))
                        && (searchInputsObject.DriveTrain == null || searchInputsObject.DriveTrain.Count == 0 || searchInputsObject.DriveTrain.Any(str => InventoryDetails.DriveTrains.Contains(str.DisplayName.ToLower())))
                        && (searchInputsObject.Fuel == null || searchInputsObject.Fuel.Count == 0 || searchInputsObject.Fuel.Any(str => InventoryDetails.FuelTypes.Contains(str.DisplayName.ToLower())))
                        && (searchInputsObject.Engine == null || searchInputsObject.Engine.Count == 0 || searchInputsObject.Engine.Any(str => InventoryDetails.Engines.Contains(str.DisplayName.ToLower())))
                      )
            {
                if (searchInputsObject.MaxMileageValue != null && searchInputsObject.MaxMileageValue != 0)
                {
                    if (searchInputsObject.MinMileageValue <= InventoryDetails.Mileage && searchInputsObject.MaxMileageValue >= InventoryDetails.Mileage)
                    {
                        if (InventoryDetails.InternetPrice != 0 && InventoryDetails.InternetPrice != null)
                        {
                            isInclude = true;
                        }
                    }
                    else
                    {
                        isInclude = false;
                    }
                }
                else
                {
                    if (InventoryDetails.InternetPrice != 0 && InventoryDetails.InternetPrice != null)
                    {
                        isInclude = true;
                    }
                }
            }



            IsIncludeCurrentVehicle = isInclude;
            return isInclude;
        }

        private SearchMarketListings GetOverallCriteria(InventoryInfo inventory, Dealer dealer, SearchInputs searchInput = null)
        {
            SearchMarketListings searchMarketListings =
            new SearchMarketListings()
            {
                BusinessUnitId = dealer.DealerId,
                InventoryId = inventory.InventoryId,
                Year = inventory.Year,
                Make = inventory.Make,
                Model = inventory.Model,
                IncludeZeroPrice = false,
                Size = 0,
                AggregateListings = true,
                GetRanks = false,
                Comment = "Overall Count",
                Aggregations = new List<string> { 
                    PricingAnalysisEnums.Aggregations.price_stats.ToString(),
                    PricingAnalysisEnums.Aggregations.trim_terms.ToString(),
                    PricingAnalysisEnums.Aggregations.transmission_terms.ToString(),
                    PricingAnalysisEnums.Aggregations.drive_train_terms.ToString(),
                    PricingAnalysisEnums.Aggregations.oem_certified_terms.ToString(),
                    PricingAnalysisEnums.Aggregations.engine_terms.ToString(),
                    PricingAnalysisEnums.Aggregations.fuel_type_terms.ToString(),
                    PricingAnalysisEnums.Aggregations.odometer_stats.ToString()}

            };
            if (Dealer.Latitude != null && Dealer.Longitude != null)
            {
                searchMarketListings.Latitude = Convert.ToString(Dealer.Latitude);
                searchMarketListings.Longitude = Convert.ToString(Dealer.Longitude);
                searchMarketListings.Distance = searchInput == null ? dealer.PingIIDefaultSearchRadius : Convert.ToInt32(searchInput.Distance.Where(d => d.IsSelected).FirstOrDefault().DisplayName);
            }

                        if (searchInput != null)
            {
               var listings=searchInput.Listings.Where(si => si.DisplayId == PricingAnalysisEnums.ActiveDisplayId);
               if (listings != null && listings.Count()>0)
                   searchMarketListings.ActiveOnly = true;                
               else
                   searchMarketListings.RecentActive = true;
                            }
                       

            if (searchInput != null)
            {
                var listings=searchInput.Listings.Where(si => si.DisplayId == PricingAnalysisEnums.ActiveDisplayId);
                if (listings != null && listings.Count()>0)
                    searchMarketListings.ActiveOnly = true;
                else
                    searchMarketListings.RecentActive = true;
                       
            }
            return searchMarketListings;
        }

        private decimal CalculateMarketPercentage(decimal internetPrice, decimal avgListPrice)
        {
            return avgListPrice == 0 ? 0 : (int)Math.Round(((decimal)internetPrice / (decimal)avgListPrice) * 100);
        }

        private SearchCriteriaViewModel GetSearchCriteria(int dealerId, int inventoryId)
        {
            SearchCriteriaViewModel viewModel = null;
            try
            {

                SearchCriteriaBuilder builder = new SearchCriteriaBuilder(dealerId, inventoryId);
                viewModel = builder.Build();
            }
            catch
            {
                return null;
            }
            return viewModel;
        }

        private bool IncludeCurrentVehicleInOverall(SearchInputs searchOption)
        {
            return !(InventoryDetails.ListPrice == 0);
        }

        private PricingCalculator GetPricingCalculator(List<MarketListingData> listings, bool includeCurrentVehicle, int internetPrice)
        {
            PricingCalculator PricingCalaculatorInput = new PricingCalculator();


            if (listings != null && listings.Count > 0)
            {
                var defaultListing = listings.FirstOrDefault();
                PricingCalaculatorInput.OverallMarketAvgCount = defaultListing.OverallMarketDaysSupply;
                PricingCalaculatorInput.MatchingMarketAvgCount = defaultListing.MatchingMarketDaysSupply;
                PricingCalaculatorInput.MinListPrice = defaultListing.MinPrice;
                PricingCalaculatorInput.MaxListPrice = defaultListing.MaxPrice;


                if (listings.FirstOrDefault().MarketlistingsColumns == null)
                {
                    PricingCalaculatorInput.ListingsInternetPrice = new List<int>();
                    if (PricingCalaculatorInput.InternetPrice != 0 && includeCurrentVehicle == true)
                    {
                        PricingCalaculatorInput.ListingsInternetPrice.Add((int)internetPrice);
                        PricingCalaculatorInput.AvgListPrice = Convert.ToInt32(internetPrice);
                    }
                    else
                    {
                        PricingCalaculatorInput.AvgListPrice = 0;
                    }
                    PricingCalaculatorInput.Count = IsIncludeCurrentVehicle ? 1 : 0;
                }
                else
                {
                    PricingCalaculatorInput.ListingsInternetPrice = defaultListing.InternetPrices;
                    PricingCalaculatorInput.ListingsInternetPrice.Add((int)internetPrice);
                    if (internetPrice != 0 && includeCurrentVehicle == true)
                    {
                        PricingCalaculatorInput.AvgListPrice = (defaultListing.SumInternetPrices + (int)internetPrice) / (listings.FirstOrDefault().RecordCount + 1);
                    }
                    else
                    {
                        PricingCalaculatorInput.AvgListPrice = defaultListing.RecordCount == 0 ? 0 : listings.FirstOrDefault().SumInternetPrices / listings.FirstOrDefault().RecordCount;
                    }
                    PricingCalaculatorInput.Count = IsIncludeCurrentVehicle ? defaultListing.ListingsCount + 1 : defaultListing.ListingsCount;
                }
                PricingCalaculatorInput.MarketAvg = (int)CalculateMarketPercentage(internetPrice, PricingCalaculatorInput.AvgListPrice);
                PricingCalaculatorInput.ListingsInternetPrice.Sort();
                PricingCalaculatorInput.PotentialGrossProfit = internetPrice - InventoryDetails.UnitCost;
                PricingCalaculatorInput.UnitCost = InventoryDetails.UnitCost;
                PricingCalaculatorInput.Rank = Array.IndexOf(PricingCalaculatorInput.ListingsInternetPrice.ToArray(), (int)internetPrice);
                PricingCalaculatorInput.Rank += 1;
                if (internetPrice <= 0)
                {
                    PricingCalaculatorInput.Rank = 0;
                }
                PricingCalaculatorInput.HighPercentage = (int)CalculateMarketPercentage(PricingCalaculatorInput.MaxListPrice, PricingCalaculatorInput.AvgListPrice);
                PricingCalaculatorInput.LowPercantage = (int)CalculateMarketPercentage(PricingCalaculatorInput.MinListPrice, PricingCalaculatorInput.AvgListPrice);


                if (PricingCalaculatorInput.Count < PricingCalaculatorInput.ListingCountThreshold)
                {
                    PricingCalaculatorInput.ShowAlert = true;
                    InventoryDetails.ShowAlert = true;
                }
                else
                {
                    InventoryDetails.ShowAlert = false;
                }

            }
            #region RiskIndicators
            List<RiskValues> redRisk = new List<RiskValues>();
            List<RiskValues> greenRisk = new List<RiskValues>();
            List<RiskValues> yellowRisk = new List<RiskValues>();

            redRisk.Add(new RiskValues() { MinValue = 0, Maxvalue = Dealer.PingIIRedRange1Value2 });
            redRisk.Add(new RiskValues() { MinValue = Dealer.PingIIRedRange1Value1, Maxvalue = 9999999 });
            greenRisk.Add(new RiskValues() { MinValue = Dealer.PingIIGreenRange1Value1, Maxvalue = Dealer.PingIIGreenRange1Value2 });
            yellowRisk.Add(new RiskValues() { MinValue = Dealer.PingIIYellowRange2Value1, Maxvalue = Dealer.PingIIYellowRange2Value2 });
            yellowRisk.Add(new RiskValues() { MinValue = Dealer.PingIIYellowRange1Value1, Maxvalue = Dealer.PingIIYellowRange1Value2 });

            PricingCalaculatorInput.RedRisk = redRisk;
            PricingCalaculatorInput.GreenRisk = greenRisk;
            PricingCalaculatorInput.YellowRisk = yellowRisk;
            #endregion

            return PricingCalaculatorInput;
        }

        private List<FilterItem> GetFilterItems(List<FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.MarketListings.EquipmentCount> terms, IList<string> elasticSearchEquipments, bool isCurrentVehicle, List<FilterItem> items)
        {
            try
            {

                CultureInfo currentCulture = System.Threading.Thread.CurrentThread.CurrentCulture;
                TextInfo currentInfo = currentCulture.TextInfo;

                var termsList = terms.Where(
                                dt =>
                                    elasticSearchEquipments.ToList()
                                        .Any(
                                            dt2 =>
                                                dt2.ToLower()
                                                    .Equals(dt.Key.ToLower(),
                                                        StringComparison.InvariantCultureIgnoreCase)));


                foreach (var term in termsList)
                {
                    items = AddFilterItem(items, term.Key, term.DocCount, isCurrentVehicle);
                }
                
               var unmatchedDataList =
                        elasticSearchEquipments.ToList()
                            .Where(
                                dt =>
                                    !termsList.Any(
                                        dtt =>
                                            (dtt.Key.ToLower()).Equals(dt.ToLower(),
                                                StringComparison.InvariantCultureIgnoreCase)))
                            .ToList();
               foreach (var unmatchedData in unmatchedDataList)
               {
                   items = AddFilterItem(items,unmatchedData, 0, isCurrentVehicle);
               }
               
            }
            catch
            {

            }
            return items;
        }

        private List<FilterItem> AddFilterItem(List<FilterItem> items, string key, int docCount, bool isCurrentVehicle)
        {

            CultureInfo currentCulture = System.Threading.Thread.CurrentThread.CurrentCulture;
            TextInfo currentInfo = currentCulture.TextInfo;

            if (items.Where(i => key.ToLower()
                                                    .Equals(i.DisplayName.ToLower(),
                                                        StringComparison.InvariantCultureIgnoreCase)).Count() == 0)
            {
                items.Add(new FilterItem()
                {
                    Count = docCount,
                    DisplayId =
                        Regex.Replace(key, "[^a-zA-Z0-9]", ""),
                    DisplayName = currentInfo.ToTitleCase(key),
                    IsSelected = isCurrentVehicle
                });
            }
            else
            {
                //if current Filter exists then remove previous entry and add new one
                if (isCurrentVehicle)
                {
                    items.Remove(items.Where(i => key.ToLower()
                                        .Equals(i.DisplayName.ToLower(),
                                            StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault());

                    items.Add(new FilterItem()
                    {
                        Count = docCount,
                        DisplayId =
                            Regex.Replace(key, "[^a-zA-Z0-9]", ""),
                        DisplayName = currentInfo.ToTitleCase(key),
                        IsSelected = isCurrentVehicle
                    });
                }
            }

            return items;
        }

        private List<FilterItem> GetListings(int activeCount, int activeRecentCount)
        {
            List<FilterItem> listings = new List<FilterItem>();
            listings.Add(new FilterItem() { DisplayId = PricingAnalysisEnums.RecentActiveDisplayId, DisplayName = PricingAnalysisEnums.RecentActiveDisplayName, IsSelected = false, Count = activeRecentCount });
            listings.Add(new FilterItem() { DisplayId = PricingAnalysisEnums.ActiveDisplayId, DisplayName = PricingAnalysisEnums.ActiveDisplayName, IsSelected = true, Count = activeCount });

            return listings;
        }

        private string RemoveSpecialCharacters(string str)
        {
            char[] buffer = new char[str.Length];
            int idx = 0;

            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z')
                    || (c >= 'a' && c <= 'z'))
                {
                    buffer[idx] = c;
                    idx++;
                }
            }

            return new string(buffer, 0, idx);
        }
        #endregion

    }

    public static class WebExtensions
    {
        public static int ToCode(this HttpStatusCode code)
        {
            return Convert.ToInt32(code);
        }
    }

}
