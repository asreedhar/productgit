﻿using System.Web.Mvc;

namespace Wanamaker.WebApp.Areas.PricingAnalysis
{
    public class PricingAnalysisAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PricingAnalysis";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PricingAnalysis",
                "PricingAnalysis/{controller}/{action}",
                new { action = "Index"}
            );

            context.MapRoute(
                 "PACtrPrice",
                 "InventoryMVCGraph.aspx/CtrPrice/{businessUnitId}/{inventoryId}",
                 new { controller = "Performance", action = "ClickThroughRatePrice" }
             );
        }
    }
}
