﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models.MarketListingsTable
{
    [DataContract]
    public class JqGridDataModel
    {
        [DataMember(Name = "PricingCalculator")]
        public PricingCalculator PricingcalculatorModel { get; set; }
        [DataMember(Name="total")]
        public int total { get; set; }
        [DataMember(Name = "page")]
        public int page { get; set; }
        [DataMember(Name = "records")]
        public int records { get; set; }
        [DataMember(Name = "rows")]
        public List<MarketListingsColumnsModel> rows { get; set; }
        [DataMember(Name = "total")]
        public string sub { get; set; }        
    }
}