﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    public class PricingHistory
    {
        public string historydate { get; set; }
        public string appraisername { get; set; }
        public int money { get; set; }
    }
}