﻿using System.Collections.Generic;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    public class MarketListingArgumentDto
    {
        //public List<FilterItem> Listings { get; set; }
        //public List<FilterItem> Distance { get; set; }
        //public List<FilterItem> Certified { get; set; }
        //public List<FilterItem> Trim { get; set; }
        //public List<FilterItem> Transmission { get; set; }
        //public List<FilterItem> DriveTrain { get; set; }
        //public List<FilterItem> Engine { get; set; }
        //public List<FilterItem> Fuel { get; set; }
        public string Vin { get; set; }
        public bool ShowZeroListing { get; set; }
    }
}