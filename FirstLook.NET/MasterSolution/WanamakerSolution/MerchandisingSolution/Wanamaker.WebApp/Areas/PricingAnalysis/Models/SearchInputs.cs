﻿
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    [DataContract]

    public class SearchInputs
    {
        [DataMember(Name = "Listings")]
        [JsonProperty(PropertyName = "Listings")]
        public List<FilterItem> Listings { get; set; }

        [DataMember(Name = "AllListings")]
        [JsonProperty(PropertyName = "AllListings")]
        public List<FilterItem> AllListings { get; set; }

        [JsonProperty(PropertyName = "Distance")]
        public List<FilterItem> Distance { get; set; }
        [DataMember]
        [JsonProperty(PropertyName = "Certified")]
        public List<FilterItem> Certified { get; set; }
        [DataMember]
        [JsonProperty(PropertyName = "Trim")]
        public List<FilterItem> Trim { get; set; }
        [DataMember]
        [JsonProperty(PropertyName = "Transmission")]
        public List<FilterItem> Transmission { get; set; }
        [DataMember]
        [JsonProperty(PropertyName = "DriveTrain")]
        public List<FilterItem> DriveTrain { get; set; }
        [DataMember]
        [JsonProperty(PropertyName = "Engine")]
        public List<FilterItem> Engine { get; set; }
        [DataMember]
        [JsonProperty(PropertyName = "Fuel")]
        public List<FilterItem> Fuel { get; set; }
        [JsonProperty(PropertyName = "Vin")]
        public string Vin { get; set; }
        [JsonProperty(PropertyName = "ShowZeroListing")]
        public bool ShowZeroListing { get; set; }
        [JsonProperty(PropertyName = "SelectedEquipment")]
        public List<string> SelectedEquipment { get; set; }
        [JsonProperty(PropertyName = "Size")]
        public int Size { get; set; }
        [JsonProperty(PropertyName = "MinMileageValue")]
        public int MinMileageValue { get; set; }
        [JsonProperty(PropertyName = "MaxMileageValue")]
        public int? MaxMileageValue { get; set; }
        [JsonProperty(PropertyName = "AvgMileageValue")]
        public int AvgMileageValue { get; set; } 
        [JsonProperty(PropertyName = "ListingCount")]
        public int ListingCount { get; set; }
        [JsonProperty(PropertyName = "PageSize")]
        public int PageSize { get; set; }
        [JsonProperty(PropertyName = "PageNo")]
        public int PageNo { get; set; }
        [JsonProperty(PropertyName = "RecordCount")]
        public int RecordCount { get; set; }
        [JsonProperty(PropertyName = "CriteriaUpdated")]
        public bool CriteriaUpdated { get; set; }
        [JsonProperty(PropertyName = "StartYear")]
        public int StartYear { get; set; }
        [JsonProperty(PropertyName = "EndYear")]
        public int EndYear { get; set; }
        [JsonProperty(PropertyName = "Zipcode")]
        public string Zipcode { get; set; }
        [JsonProperty(PropertyName = "CarsDotComMakeID")]
        public int? CarsDotComMakeID { get; set; }
        [JsonProperty(PropertyName = "CarsDotComModelId")]
        public int? CarsDotComModelId { get; set; }
        [JsonProperty(PropertyName = "AutoTraderMake")]
        public string AutoTraderMake { get; set; }
        [JsonProperty(PropertyName = "AutoTraderModel")]
        public string AutoTraderModel { get; set; }
        [JsonProperty(PropertyName = "UpdateOverall")]
        public bool UpdateOverall { get; set; }
        public int ListingCountThreshold { get { return FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.MaxProfitLowCountThreshold; } }
        [JsonProperty(PropertyName = "OverallCount")]
        public int OverallCount { get; set; }
        [JsonProperty(PropertyName = "MatchingCount")]
        public int MatchingCount { get; set; }
        [JsonProperty(PropertyName = "MileageUpdated")]
        public bool MileageUpdated { get; set; }
        [JsonProperty(PropertyName = "InternetPrice")]
        public int InternetPrice { get; set; }
        [JsonProperty(PropertyName = "MileageEndValue")]
        public int? MileageEndValue { get; set; }
        [JsonProperty(PropertyName = "MileageStartValue")]
        public int? MileageStartValue { get; set; }

        public SearchInputs()
        {
            Listings = new List<FilterItem>();
            Distance = new List<FilterItem>();
            Certified = new List<FilterItem>();
            Trim = new List<FilterItem>();
            Transmission = new List<FilterItem>();
            DriveTrain = new List<FilterItem>();
            Fuel = new List<FilterItem>();
            Engine = new List<FilterItem>();
        }

        public List<int> DefaultDistanceList
        {
            get
            {
                return new List<int>() {10,25,50,75,100,150,250,500,750,1000 };
            }
            internal set
            {

            }
        }
    }
}