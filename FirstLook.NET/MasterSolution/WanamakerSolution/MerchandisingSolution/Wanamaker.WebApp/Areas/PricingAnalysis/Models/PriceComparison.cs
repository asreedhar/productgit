﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    public class PriceComparison
    {
        public int Threshold { get; set; }
        public string DisplayName { get; set; }
        public string DisplayId { get; set; }
        public int BookId { get; set; }
        public int DealerThreshold { get; set; }
        public bool IsAccurate { get; set; }

    }
}