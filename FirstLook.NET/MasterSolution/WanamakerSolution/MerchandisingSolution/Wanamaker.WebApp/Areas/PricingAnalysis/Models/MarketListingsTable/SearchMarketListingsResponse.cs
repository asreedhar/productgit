﻿using System.Collections.Generic;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models.MarketListingsTable
{
    public class SearchMarketListingsResponse
    {
        public List<MarketListingsColumnsModel> SearchMarketListings { get; set; }

        public List<EquipmentCount> TierOneEquipments { get; set; }

        public PriceStat PriceStats { get; set; }

        public List<EquipmentCount> CertifiedTerms { get; set; }

      
        public List<EquipmentCount> DriveTrainTerms { get; set; }

       
        public List<EquipmentCount> TransmissionTerms { get; set; }

       
        public List<EquipmentCount> TrimTerms { get; set; }

  
        public List<EquipmentCount> EngineTerms { get; set; }

       
        public List<EquipmentCount> FuelTypeTerms { get; set; }
    }
}