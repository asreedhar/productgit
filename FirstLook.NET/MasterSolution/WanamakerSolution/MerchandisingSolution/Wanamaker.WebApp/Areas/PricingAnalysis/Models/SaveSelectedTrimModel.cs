﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    public class SaveSelectedTrimModel
    {
        
        public int ChromeStyleId { get; set; }
        
        public bool Flag { get; set; }
    }
}