﻿using System;
using System.ComponentModel;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Utilities;
using FirstLook.VehicleHistoryReport.WebService.Client.Carfax;
using FirstLook.VehicleHistoryReport.WebService.Client.AutoCheck;
using CarfaxUserIdentity = FirstLook.VehicleHistoryReport.WebService.Client.Carfax.UserIdentity;
using AutoCheckUserIdentity = FirstLook.VehicleHistoryReport.WebService.Client.AutoCheck.UserIdentity;
//using CarfaxReportType = FirstLook.VehicleHistoryReport.WebControls.Carfax.CarfaxReportType;
using CarfaxTrackingCode = FirstLook.VehicleHistoryReport.WebService.Client.Carfax.CarfaxTrackingCode;
using CarfaxVehicleEntityType = FirstLook.VehicleHistoryReport.WebService.Client.Carfax.VehicleEntityType;
//using AutoCheckDealerAccount = FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.DealerAccount;


namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    public class VehicleHistoryReport
    {
        public CarfaxVehicleHistory CarfaxVehicleHistoryReport { get; set; }
        public AutocheckVehicleHistory AutocheckVehicleHistoryReport { get; set; }


        private int DealerId
        {
            get { return 101590; }//  int.Parse(Request.QueryString["dealerId"]); }
        }

        private int InventoryId
        {
            get { return int.Parse("11222333"); }
        }


    }
}