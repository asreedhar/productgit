﻿
namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models.MarketListingsTable
{
    public class MarketListingsColumnsModel
    {
        public int? Age { get; set; }
        public string Seller { get; set; }
        public string VehicleDescription { get; set; }
        public bool IsCertified { get; set; }
        public string Color { get; set; }
        public double? Mileage { get; set; }
        public decimal? InternetPrice { get; set; }
        public decimal? PMarketAverage{get;set;}
        public int? Distance { get; set; }
        public string VIN { get; set; }
        public int MileageAvg { get; set; }
        public string AdText { get; set; }
        public string StockNumber { get; set; }
    }
}