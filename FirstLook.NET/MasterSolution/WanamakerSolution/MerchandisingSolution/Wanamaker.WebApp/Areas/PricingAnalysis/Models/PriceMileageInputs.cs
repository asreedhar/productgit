﻿
namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    public class PriceMileageInputs
    {
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
        public int Mileage { get; set; }
        public int AveragePrice { get; set; }
        public int StartValue { get; set; }
        public int EndValue { get; set; }
        public int OverallCount { get; set; }
        public int MatchingCount { get; set; }
        public bool ShowAlert { get; set; }
    }
}