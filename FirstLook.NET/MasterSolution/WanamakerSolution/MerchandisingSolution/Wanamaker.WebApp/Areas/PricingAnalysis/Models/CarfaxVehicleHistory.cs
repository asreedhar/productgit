﻿
namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    public class CarfaxVehicleHistory
    {
        public bool HasCarfax { get; set; }

        public bool  CanPurchaseCarfax { get; set; }

        public bool HasPurchasedCarfax { get; set; }

        public string CarfaxUrl { get; set; }

        public bool HasPurchaseReportRights { get; set; }

        public bool HasOneOwner { get; set; }

        public bool HasBuyBackGurantee { get; set; }

        public bool HasNoTotalLossReported { get; set; }

        public bool HasNoFrameDamageReported { get; set; }

        public bool HasNoAirbagDeploymentReported { get; set; }

        public bool HasNoOdometerRollbackReported { get; set; }

        public bool HasNoAccidentReported { get; set; }

        public bool HasNoManufacturerRecallsReported { get; set; }

        public string Error { get; set; }     
    }
}