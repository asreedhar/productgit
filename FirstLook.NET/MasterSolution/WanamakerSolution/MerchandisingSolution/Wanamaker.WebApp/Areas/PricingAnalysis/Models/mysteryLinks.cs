﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    public class MysteryLinks
    {
        public int MakeId { get; set; }
        public int ModelId { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int StartYear { get; set; }
        public int EndYear { get; set; }
        public int Radius { get; set; }
        public string Zipcode { get; set; }
        public string AutoTraderUrl { get; set; }
        public string CarsUrl { get; set; }
        public int ListingCount { get; set; }
        public int? CarsDotComMakeID { get; set; }

        public int? CarsDotComModelId { get; set; }

        public string AutoTraderMake { get; set; }

        public string AutoTraderModel { get; set; }
    
    }
}