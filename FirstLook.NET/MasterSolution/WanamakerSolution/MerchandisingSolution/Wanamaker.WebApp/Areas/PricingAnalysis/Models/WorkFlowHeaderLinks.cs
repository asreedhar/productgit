﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    public class WorkFlowHeaderLinks
    {
        public string LoadEquipmentLink { get; set; }
        public string LoadPackagesLink { get; set; }
        public string LoadPhotosLink { get; set; }
        public string PricingPageLink { get; set; }
        public string ApprovalLink { get; set; }
        public string BulkUpload { get; set; }
        public string MAXSellingLink { get; set; }
        public string NextUrl { get; set; }
        public string PreviousUrl { get; set; }
        public string PresentOutofTotal { get; set; }
        public string PrintWindowSticker { get; set; }

        public int InventoryId { get; set; }
        public int BusinessUnitId { get; set; }
        public string Filter { get; set; }
        public int? UsedNew { get; set; }
        public int? AgeBucket { get; set; }
    }
}