﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Text;
using System.Runtime.Serialization;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models.MarketListingsTable
{
    public class GeonameDetailsModel
    {
        [DataMember(Name = "fcodeName")]
        public string FcodeName { get; set; }

        [DataMember(Name = "toponymName")]
        public string ToponymName { get; set; }

        [DataMember(Name = "countrycode")]
        public string CountryCode { get; set; }

        [DataMember(Name = "fcl")]
        public string Fcl { get; set; }

        [DataMember(Name = "fclName")]
        public string FclName { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "wikipedia")]
        public string WikiPedia { get; set; }

        [DataMember(Name = "lng")]
        public decimal lng { get; set; }
        [DataMember(Name = "fcode")]
        public string Fcode { get; set; }

        [DataMember(Name = "geonameId")]
        public int GeonameId { get; set; }

        [DataMember(Name = "lat")]
        public decimal Lat { get; set; }

        [DataMember(Name = "population")]
        public double Population { get; set; }


    }
}