﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wanamaker.WebApp.Areas.PricingAnalysis.Models.MarketListingsTable;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    public class ViewOne : CoreView
    {
        public PriceMileageInputs MileageInputs { get; set; }
        public MarketListingsInput MarketlistingsInput { get; set; }
        public VehicleHistoryReport VehicleHistoryReport { get; set; }
        public InventoryHeader InventoryHeader { get; set; }
        public FacetAndFilter FacetAndFilter { get; set; }
        public SearchInputs SearchOption { get; set; }       
        public PricingProof PriceProof { get; set; }
        public PricingCalculator PricingCalc { get; set; }
        public MysteryLinks MysteryLink { get; set; }
        
        public int OverallCount { get; set; }
        public int MatchingCount { get; set; }
        public int OverallMarketAvgCount { get; set; }
        public int MatchingMarketAvgCount { get; set; }
        public bool ShowSavePriceOnly { get; set; }
        public int AverageCount { get; set; }
        public int LowValue { get; set; }
        public int AvgValue { get; set; }
        public int HighValue { get; set; }
        public int LowPercentage { get; set; }
        public int HighPercentage { get; set; }
        public WorkFlowHeaderLinks links { get; set; }
        public bool ShowCtrGraph { get; set; }
    }
}
