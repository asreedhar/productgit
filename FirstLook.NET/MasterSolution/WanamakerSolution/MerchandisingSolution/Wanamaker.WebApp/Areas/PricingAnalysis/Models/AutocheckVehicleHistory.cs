﻿

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    public class AutocheckVehicleHistory
    {
        public string AutocheckUrl { get; set; }
        public bool HasPurchaseReportRights { get; set; }
        public bool HasAutocheckReport { get; set; }
        public bool HasSingleOwner { get; set; }
        public bool IsAssured { get; set; }
        public bool HasTotalLossReported { get; set; }
        public bool HasNoFrameDamageReported { get; set; }
        public bool HasNoAirbagDeploymentReported { get; set; }
        public bool HasNoOdometerRollbackReported { get; set; }
        public bool HasNoAccidentReported { get; set; }
        public bool HasNoManufacturerRecallsReported { get; set; }
        public bool HasPurchasedAutocheck { get; set; }
        public string Error { get; set; }     
    }
}