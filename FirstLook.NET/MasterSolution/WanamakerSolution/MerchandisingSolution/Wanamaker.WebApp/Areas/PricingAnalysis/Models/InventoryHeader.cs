﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    public class InventoryHeader
    {
        public int InventoryId { get; set; }

        public int BusinessUnitId { get; set; }

        public string Vin { get; set; }

        public string StockNumber { get; set; }

        public int? StatusBucket { get; set; }

        public DateTime InventoryReceivedDate { get; set; }

        public int Year { get; set; }

        public decimal UnitCost { get; set; }

        public decimal? AcquisitionPrice { get; set; }

        public int? Certified { get; set; }

        public string CertifiedId { get; set; }

        public string Make { get; set; }

        public string Model { get; set; }

        public int? ChromeStyleId { get; set; }

        public string Trim { get; set; }

        public int TradePurchase { get; set; }

        public int? Mileage { get; set; }

        public decimal? ListPrice { get; set; }

        public string BaseColor { get; set; }

        public string ExtColor1 { get; set; }

        public string ExteriorColorCode { get; set; }

        public string ExtColor2 { get; set; }

        public string ExteriorColorCode2 { get; set; }

        public string InteriorColor { get; set; }

        public string InteriorColorCode { get; set; }

        public string VehicleLocation { get; set; }

        public int InventoryStatusCD { get; set; }

        public string Description { get; set; }

        public decimal LastUpdateListPrice { get; set; }

        public DateTime DatePosted { get; set; }

        public int ExteriorStatus { get; set; }

        public int InteriorStatus { get; set; }

        public int PhotoStatus { get; set; }

        public int AdReviewStatus { get; set; }

        public int PricingStatus { get; set; }

        public int PostingStatus { get; set; }

        public int OnlineFlag { get; set; }

        public int InventoryType { get; set; }

        public int DesiredPhotoCount { get; set; }

        public decimal LotPrice { get; set; }

        public decimal Msrp { get; set; }

        public decimal SpecialPrice { get; set; }

        public decimal DueForReprice { get; set; }

        public decimal LowActivityFlag { get; set; }

        public decimal HasCurrentBookValue { get; set; }

        public decimal HasKeyInformation { get; set; }

        public decimal HasCarfax { get; set; }

        public decimal HasCarfaxAdmin { get; set; }

        public string CertificationName { get; set; }

        public int CertifiedProgramId { get; set; }

        public int MakeId { get; set; }

        public int ModelId { get; set; }
        public List<SelectListItem> TrimsList { get; set; }

        public string Color { get; set; }     
        
        public int AgeInDays { get; set; }

        public int? CarsDotComMakeID { get; set; }

        public int? CarsDotComModelId { get; set; }

        public string AutoTraderMake { get; set; }

        public string AutoTraderModel { get; set; }
    }
}