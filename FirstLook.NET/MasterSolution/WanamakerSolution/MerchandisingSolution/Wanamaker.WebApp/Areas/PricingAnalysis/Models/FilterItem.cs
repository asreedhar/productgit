﻿using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    [DataContract]
    public class FilterItem
    {
        [DataMember]
        [JsonProperty(PropertyName = "DisplayName")]
        public string DisplayName { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "Level")]
        public int Level { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "Count")]
        public int Count { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "DisplayId")]
        public string DisplayId { get; set; }

        [DataMember]
        [JsonProperty(PropertyName = "IsSelected")]
        public bool IsSelected { get; set; }
    }
}