﻿using System;
using System.Web.Mvc;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    public abstract class CoreView : Controller
    {
        public String PageTitle { get; set; }
    }
}