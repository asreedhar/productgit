﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models.MarketListingsTable
{
    public class MarketListings
    {
        public List<SearchMarketListingsResponse> SearchMarketListingsResponse { get; set; }
    }
}