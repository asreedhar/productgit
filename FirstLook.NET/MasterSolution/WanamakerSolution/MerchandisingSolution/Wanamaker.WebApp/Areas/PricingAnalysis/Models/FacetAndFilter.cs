﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    public class FacetAndFilter
    {
       public List<FilterItem> Equipments { get; set; }
       public List<FilterItem> TopLevelEquipments { get; set; }
    }
}