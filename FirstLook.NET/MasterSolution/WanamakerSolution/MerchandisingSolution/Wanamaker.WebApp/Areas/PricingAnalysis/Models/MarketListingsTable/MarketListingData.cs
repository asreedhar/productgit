﻿using System.Collections.Generic;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.MarketListings;
using ModelBuilder=FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.MarketListings;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models.MarketListingsTable
{
    public class MarketListingData
    {
        public MarketListingData()
        {
            MarketlistingsColumns=new List<MarketListingsColumnsModel>();
        }

        public List<MarketListingsColumnsModel> MarketlistingsColumns{ get; set; }
        public int RecordCount { get; set; }
        public int PriceAvg { get; set; }
        public int MileageAvg { get; set; }
        public int MileageSum { get; set; }
        public int MileageCount { get; set; }
        public int AvgPrice { get; set; }
        public int MinPrice { get; set; }
        public int MaxPrice { get; set; }
        public int OverallMarketDaysSupply { get; set; }
        public int MatchingMarketDaysSupply { get; set; }
        public int ListingsCount { get; set; }
        public int SumInternetPrices { get; set; }
        public List<int> InternetPrices { get; set; }
        public List<FacetEquipments> Facets { get; set; }
        public int RecordCountWithZero { get; set; }

        public int ActiveCount { get; set; }
        public int RecentActiveCount { get; set; }
        public List<ModelBuilder.EquipmentCount> CertifiedTerms { get; set; }
        public List<ModelBuilder.EquipmentCount> DriveTrainTerms { get; set; }
        public List<ModelBuilder.EquipmentCount> TransmissionTerms { get; set; }
        public List<ModelBuilder.EquipmentCount> TrimTerms { get; set; }
        public List<ModelBuilder.EquipmentCount> EngineTerms { get; set; }
        public List<ModelBuilder.EquipmentCount> FuelTypeTerms { get; set; }
        public ModelBuilder.OdometerStat OdometerStats { get; set; }
        public ModelBuilder.PriceStat PriceStats { get; set; }
    }
}