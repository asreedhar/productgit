﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Mvc;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    [DataContract]
    public class PrintWindowSticker
    {
        public string PageTitle { get; set; }
        [DataMember(Name = "Window Stickers")]
        public List<SelectListItem> WindowStickers { get; set; }
        [DataMember(Name = "Buyers Guides")]
        public List<SelectListItem> BuyersGuides { get; set; }

        public string WindowstickerJsonStatus { get; set; }

        public string BuyerGuidesJsonStatus { get; set; }

        public string LastPrintDateMessage { get; set; }

        public bool ServiceContractStatus { get; set; }
    }

    [DataContract]
    public class DisplayElements
    {
        [DataMember]
        public bool NoWarranty { get; set; }

        [DataMember]
        public bool LimitedWarranty { get; set; }

        [DataMember]
        public bool FullWarranty { get; set; }

        [DataMember]
        public bool ServiceContract { get; set; }

        [DataMember]
        public bool KbbBookValue { get; set; }
    }
}