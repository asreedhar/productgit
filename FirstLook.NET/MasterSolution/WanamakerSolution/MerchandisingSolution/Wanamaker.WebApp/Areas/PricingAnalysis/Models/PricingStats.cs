﻿
namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    public class PricingStats
    {
        public int OverallCount { get; set; }

        public int MatchingCount { get; set; }

        public int AvgPrice { get; set; }

        public int PriceSum { get; set; }

        public int PriceCount { get; set; }

        public int PriceMin { get; set; }

        public int PriceMax { get; set; }

    }
}