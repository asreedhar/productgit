﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models.MarketListingsTable
{
    public class PriceStat
    {
        
        public int? Count { get; set; }

        
        public decimal? Sum { get; set; }

        
        public decimal? Avg { get; set; }

        
        public decimal? Max { get; set; }

        
        public bool? Min { get; set; }

        
        public string SumAsString { get; set; }

        
        public string AvgAsString { get; set; }

        
        public string MaxAsString { get; set; }

        
        public string MinAsString { get; set; }
    }
}