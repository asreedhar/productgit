﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    public class UpdateInternetPriceModel
    {
        
        public int InventoryId { get; set; }

        
        public decimal OldListPrice { get; set; }

       
        public decimal NewListPrice { get; set; }
    }
}