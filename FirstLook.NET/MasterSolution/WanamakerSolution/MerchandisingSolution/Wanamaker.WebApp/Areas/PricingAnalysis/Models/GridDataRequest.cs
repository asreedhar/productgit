﻿using Newtonsoft.Json;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    
    public class GridDataRequest
    {
        public string sidx { get; set; }
        public string sord { get; set; }
        public int page { get; set; }
        public int rows { get; set; }
        public string rowList { get; set; }
        public string SearchInputs { get; set; }
        public SearchInputs SearchInputsObject {
            get { return JsonConvert.DeserializeObject<SearchInputs>(SearchInputs); } 
        }

    }
}