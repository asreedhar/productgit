﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    public class PricingProof
    {
        public List<PriceComparison> PriceComparisons { get; set; }
        public int InternetPrice { get; set; }
        public List<RadioItem> RadioList { get; set; }
    }
}