﻿
namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models.MarketListingsTable
{
    public class MarketListingsInput
    {
        public string VIN { get; set; }
    }
}