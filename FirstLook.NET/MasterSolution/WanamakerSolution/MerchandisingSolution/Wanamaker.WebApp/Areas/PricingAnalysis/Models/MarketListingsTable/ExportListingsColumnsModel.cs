﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models.MarketListingsTable
{
    public class ExportListingsColumnsModel
    {
        public int? Age { get; set; }
        public string Seller { get; set; }
        public string VehicleDescription { get; set; }
        public string Certified { get; set; }
        public string Color { get; set; }
        public double? Mileage { get; set; }
        public string InternetPrice { get; set; }
        public string PMarketAverage { get; set; }
        public int? Distance { get; set; }
        public string VIN { get; set; }
        public string Comments { get; set; }
    }
}