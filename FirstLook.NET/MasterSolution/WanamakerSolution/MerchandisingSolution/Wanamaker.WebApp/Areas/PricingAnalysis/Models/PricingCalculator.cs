﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wanamaker.WebApp.Areas.PricingAnalysis.Models.MarketListingsTable;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    public class PricingCalculator
    {
        public decimal? UnitCost { get; set; }
        public List<int> ListingsInternetPrice { get; set; }
        public int Rank { get; set; }
        public int MinListPrice { get; set; }
        public int MaxListPrice { get; set; }
        public int AvgListPrice { get; set; }
        public decimal InternetPrice { get; set; }
        public decimal? PotentialGrossProfit { get; set; }
        public int MarketAvg { get; set; }
        public int[] Scores { get; set; }
        public int Count { get; set; }
        public IList<PricingHistory> Pricinghistory { get; set; }
        public List<RiskValues> RedRisk{ get; set; }
        public List<RiskValues> GreenRisk{ get; set; }
        public List<RiskValues> YellowRisk{ get; set; }    
        public int HighPercentage { get; set; }
        public int LowPercantage { get; set; }
        public int AverageMileage { get; set; }
        public int OverallMarketAvgCount { get; set; }
        public int MatchingMarketAvgCount { get; set; }
        public int ListingCountThreshold { get { return FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.MaxProfitLowCountThreshold; } }
        public bool ShowAlert { get; set; }
    }
    public class RiskValues{
        public int MinValue { get; set; }
        public int Maxvalue{ get; set; }
    }    

}