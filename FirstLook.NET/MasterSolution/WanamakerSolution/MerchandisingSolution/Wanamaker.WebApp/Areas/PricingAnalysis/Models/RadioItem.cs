﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    public class RadioItem
    {
        public string Mileage { get; set; }
        public int AvgPrice { get; set; }
        public bool IsSelected { get; set; }
        public bool ShowAlert { get; set; }
    }
}