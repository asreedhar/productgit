﻿
using System.Collections.Generic;
namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    public class InventoryInfo
    {
        public decimal? ListPrice { get; set; }
        public int UnitCost { get; set; }
        public string Vin  { get; set; }
        public int InventoryId { get; set; }
        public int Year { get; set; }
        public string Model { get; set; }
        public string Make { get; set; }
        public string Trim { get; set; }
        public int MarketAverage { get; set; }
        public string StockNumber { get; set; }
        public int Distance { get; set; }
        public int? Age { get; set; }
        public string Seller { get; set; }
        public string VehicleDescription { get; set; }
        public bool IsCertified { get; set; }
        public string Color { get; set; }
        public double? Mileage { get; set; }
        public int? InternetPrice { get; set; }
        public decimal? PMarketAverage { get; set; }
        public int OverallCount { get; set; }
        public bool ShowAlert { get; set; }
        public int ChromeStyleId { get; set; }
        public List<string> DriveTrains = new List<string>();
        public List<string> Engines = new List<string>();
        public List<string> Transmission = new List<string>();
        public List<string> Trims = new List<string>();
        public List<string> FuelTypes = new List<string>();
    }
}