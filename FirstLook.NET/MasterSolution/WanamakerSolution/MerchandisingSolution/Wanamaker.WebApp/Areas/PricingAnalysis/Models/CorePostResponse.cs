﻿using System;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    public class CorePostResponse
    {
        public bool Success { get; set; }
        public String Info { get; set; }
    }
}