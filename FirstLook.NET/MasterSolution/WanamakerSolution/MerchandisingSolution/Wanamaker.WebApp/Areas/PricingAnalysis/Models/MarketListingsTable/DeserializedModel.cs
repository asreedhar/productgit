﻿using System.Collections.Generic;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models.MarketListingsTable
{
    public class DeserializedModel
    {   
        public List<MarketListingsColumnsModel> Listings { get; set; }
    }
}