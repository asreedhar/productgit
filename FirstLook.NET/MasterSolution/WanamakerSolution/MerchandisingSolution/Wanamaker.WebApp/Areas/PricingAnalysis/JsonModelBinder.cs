﻿using System;
using System.IO;
using System.Web.Mvc;
using Newtonsoft.Json;


namespace Wanamaker.WebApp.Areas.PricingAnalysis
{
    public class JsonModelBinder : IModelBinder
    {
        #region IModelBinder Members

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (controllerContext == null)
                throw new ArgumentNullException("controllerContext");
            if (bindingContext == null)
                throw new ArgumentNullException("bindingContext");

            StreamReader reader = new StreamReader(controllerContext.HttpContext.Request.InputStream);
            string data = reader.ReadToEnd();
            return JsonConvert.DeserializeObject(data, bindingContext.ModelType);
        }

        #endregion
    }
}