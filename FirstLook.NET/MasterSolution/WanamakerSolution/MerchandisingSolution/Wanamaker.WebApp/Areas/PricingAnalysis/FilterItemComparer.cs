﻿using System.Collections.Generic;
using Wanamaker.WebApp.Areas.PricingAnalysis.Models;

namespace Wanamaker.WebApp.Areas.PricingAnalysis
{
    public class FilterItemComparer : IEqualityComparer<FilterItem>
    {
        public bool Equals(FilterItem x, FilterItem y)
        {
            return (x.DisplayId.ToLower().Equals(y.DisplayId.ToLower(),System.StringComparison.InvariantCultureIgnoreCase));
        }

        public int GetHashCode(FilterItem obj)
        {
            return obj.DisplayId.GetHashCode();
        }
    }
}