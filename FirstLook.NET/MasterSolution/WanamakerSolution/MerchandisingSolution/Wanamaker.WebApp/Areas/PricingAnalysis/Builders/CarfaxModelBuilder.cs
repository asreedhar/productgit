﻿using System;
using System.Configuration;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Builder;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.VehicleHistoryReport;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.VehicleHistoryReport;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository.VehicleHistoryReport;
using Wanamaker.WebApp.Areas.PricingAnalysis.Models;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Builders
{
    public class CarfaxModelBuilder : IModelBuilder<CarfaxVehicleHistory>
    {
        public int BusinessUnitId { get; set; }
        public string Vin { get; set; }
        public bool PullReport { get; set; }
        public string UserName { get; set; }

        public const string VhrApi = "/v1/MaxPricingToolReports/carfax/dealers/{0}/vins/{1}/user/{2}";

        public string VhrApiUrl
        {
            get
            {
                string vhrapi = ConfigurationManager.AppSettings["VhrCarfaxApiUrl"];
                return vhrapi == null ? VhrApi : vhrapi.TrimEnd(new[] { '/', '\\' });
            }
        }

        #region IModelBuilder<CarfaxVehicleHistory,CarfaxModel> Members

        public CarfaxModelBuilder(int businessunitId, string vin, string userName, bool pullReport = false)
        {
            this.BusinessUnitId = businessunitId;
            this.Vin = vin;
            this.PullReport = pullReport;
            this.UserName = userName;
        }

        /// <summary>
        /// This method will build Vehicle History ViewModel from Repository.
        /// </summary>
        /// <returns> CarfaxVehicleHistory ViewModel</returns>
        public CarfaxVehicleHistory Build()
        {
            CarfaxVehicleHistory carfaxVehicleHistory = new CarfaxVehicleHistory();
            CarfaxModel model = null;
            try
            {
                var repository = Registry.Resolve<IRepository<CarfaxModel, CarfaxArgumentDto>>();

                if (PullReport)
                {
                    model = repository.Update(new CarfaxArgumentDto()
                                              {
                                                  ApiPass =
                                                      FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers
                                                      .FlServiceApiPass,
                                                  ApiUser =
                                                      FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers
                                                      .FlServiceApiUser,
                                                  Method = "POST",
                                                  Url =
                                                      FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers
                                                      .FlServiceBaseUrl + string.Format(VhrApiUrl, BusinessUnitId, Vin, UserName)
                                              });
                }
                else
                {
                    model = repository.Fetch(new CarfaxArgumentDto()
                                             {
                                                 ApiPass =
                                                     FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers
                                                     .FlServiceApiPass,
                                                 ApiUser =
                                                     FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers
                                                     .FlServiceApiUser,
                                                 Method = "GET",
                                                 Url =
                                                     FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers
                                                     .FlServiceBaseUrl + string.Format(VhrApiUrl, BusinessUnitId, Vin, UserName)
                                             });
                }

                carfaxVehicleHistory.CanPurchaseCarfax = model.HasPurchaseReportRights;

                if (model.Inspections != null)
                {
                    carfaxVehicleHistory.HasOneOwner = model.Inspections[CarfaxItemsKeys.SingleOwner];
                    carfaxVehicleHistory.HasBuyBackGurantee = model.Inspections[CarfaxItemsKeys.BuyBackGurantee];
                    carfaxVehicleHistory.HasNoTotalLossReported = model.Inspections[CarfaxItemsKeys.TotalLoss];
                    carfaxVehicleHistory.HasNoFrameDamageReported = model.Inspections[CarfaxItemsKeys.FrameDamage];
                    carfaxVehicleHistory.HasNoAirbagDeploymentReported =
                        model.Inspections[CarfaxItemsKeys.AirbagDeployment];
                    carfaxVehicleHistory.HasNoOdometerRollbackReported =
                        model.Inspections[CarfaxItemsKeys.OdometerRollback];
                    carfaxVehicleHistory.HasNoAccidentReported = model.Inspections[CarfaxItemsKeys.Accidents];
                    carfaxVehicleHistory.HasNoManufacturerRecallsReported =
                        model.Inspections[CarfaxItemsKeys.ManufacturerRecall];
                    carfaxVehicleHistory.CarfaxUrl = model.Href;
                    carfaxVehicleHistory.HasPurchasedCarfax = true;
                    model.HasCarfax = true;
                }
                else
                {
                    carfaxVehicleHistory.HasPurchasedCarfax = false;
                }

            }
            catch (Exception ex)
            {
                carfaxVehicleHistory.Error = ex.Message;
            }
            finally
            {

                carfaxVehicleHistory.HasCarfax = model == null || model.HasCarfax;
            }

            return carfaxVehicleHistory;
        }

        /// <summary>
        /// This method will update CarfaxVehicleHistory View model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public CarfaxVehicleHistory Rebuild(CarfaxVehicleHistory model)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}