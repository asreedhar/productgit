﻿using System;
using System.Configuration;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Builder;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.VehicleHistoryReport;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.VehicleHistoryReport;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository.VehicleHistoryReport;
using Wanamaker.WebApp.Areas.PricingAnalysis.Models;
using System.Web;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Builders
{
    public class AutocheckReportBuilder : IModelBuilder<AutocheckVehicleHistory>
    {
        public int BusinessUnitId { get; set; }
        public string Vin { get; set; }
        public bool PullReport { get; set; }
        public string UserName { get; set; }

        public const string VhrApi = "/v1/MaxPricingToolReports/autocheck/dealers/{0}/vins/{1}/user/{2}";

        public string VhrApiUrl
        {
            get
            {
                string vhrapi = ConfigurationManager.AppSettings["VhrAutocheckApiUrl"];
                return vhrapi == null ? VhrApi : vhrapi.TrimEnd(new[] { '/', '\\' });
            }
        }

        public AutocheckReportBuilder(int businessunitId, string vin, string userName, bool pullReport)
        {
            this.BusinessUnitId = businessunitId;
            this.Vin = vin;
            this.PullReport = pullReport;
            this.UserName = userName;
        }

        /// <summary>
        /// This method will build Vehicle History ViewModel from Repository.
        /// </summary>
        /// <returns> CarfaxVehicleHistory ViewModel</returns>
        #region IModelBuilder<AutocheckVehicleHistory,AutocheckModel> Members

        public AutocheckVehicleHistory Build()
        {
            AutocheckVehicleHistory autocheckVehicleHistory = new AutocheckVehicleHistory();
            AutocheckModel model = null;

            try
            {
                var repository = Registry.Resolve<IRepository<AutocheckModel, AutocheckArgumentDto>>();

                if (PullReport)
                {
                    model = repository.Update(new AutocheckArgumentDto()
                                              {
                                                  ApiPass =
                                                      FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers
                                                      .FlServiceApiPass,
                                                  ApiUser =
                                                      FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers
                                                      .FlServiceApiUser,
                                                  Method = "POST",
                                                  Url =
                                                      FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers
                                                      .FlServiceBaseUrl + string.Format(VhrApiUrl, BusinessUnitId, Vin, UserName)
                                              });
                }
                else
                {
                    model = repository.Fetch(new AutocheckArgumentDto()
                                             {
                                                 ApiPass =
                                                     FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers
                                                     .FlServiceApiPass,
                                                 ApiUser =
                                                     FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers
                                                     .FlServiceApiUser,
                                                 Method = "GET",
                                                 Url =
                                                     FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers
                                                     .FlServiceBaseUrl + string.Format(VhrApiUrl, BusinessUnitId, Vin, UserName)
                                             });
                }

                autocheckVehicleHistory.HasPurchaseReportRights = model.HasPurchaseReportRights;

                if (model.Inspections != null)
                {
                    model.HasAutocheck = true;
                    autocheckVehicleHistory.HasAutocheckReport = true;
                    autocheckVehicleHistory.HasPurchasedAutocheck = true;
                    autocheckVehicleHistory.HasSingleOwner = model.Inspections[AutocheckItemsKeys.SingleOwner];
                    autocheckVehicleHistory.IsAssured = model.Inspections[AutocheckItemsKeys.Assured];
                    autocheckVehicleHistory.HasTotalLossReported = model.Inspections[AutocheckItemsKeys.TotalLoss];
                    autocheckVehicleHistory.HasNoFrameDamageReported = model.Inspections[AutocheckItemsKeys.FrameDamage];
                    autocheckVehicleHistory.HasNoAirbagDeploymentReported =
                        model.Inspections[AutocheckItemsKeys.AirbagDeployment];
                    autocheckVehicleHistory.HasNoOdometerRollbackReported =
                        model.Inspections[AutocheckItemsKeys.OdometerRollback];
                    autocheckVehicleHistory.HasNoAccidentReported = model.Inspections[AutocheckItemsKeys.Accidents];
                    autocheckVehicleHistory.HasNoManufacturerRecallsReported =
                        model.Inspections[AutocheckItemsKeys.ManufacturerRecall];
                    autocheckVehicleHistory.AutocheckUrl =
                        (string.Format(FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.AutocheckUrl,
                            BusinessUnitId, Vin));
                }
                else
                {
                    autocheckVehicleHistory.HasAutocheckReport = true;
                    autocheckVehicleHistory.HasPurchasedAutocheck = false;
                    if (autocheckVehicleHistory.HasPurchaseReportRights)
                    {
                        model.HasAutocheck = true;
                        autocheckVehicleHistory.AutocheckUrl =
                            (string.Format(FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.AutocheckUrl,
                                BusinessUnitId, Vin));
                    }
                }

            }
            catch (Exception ex)
            {

                autocheckVehicleHistory.Error = ex.Message;
            }
            finally
            {
                autocheckVehicleHistory.HasAutocheckReport = model == null || model.HasAutocheck;
            }

            return autocheckVehicleHistory;
        }

        public AutocheckVehicleHistory Rebuild(AutocheckVehicleHistory model)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}