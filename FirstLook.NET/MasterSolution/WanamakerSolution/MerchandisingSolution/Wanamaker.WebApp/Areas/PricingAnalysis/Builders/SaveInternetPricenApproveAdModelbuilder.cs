﻿using System;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Builder;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.ApproveAd;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.SaveApproveAd;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Builders
{
    public class SaveInternetPricenApproveAdModelbuilder:IModelBuilder<UpdateInternetPriceNApproveAdModel>
    {
        int BusinessUnitId { get; set; }
        int InventoryId { get; set; }
        decimal InternetPrice { get; set; }
        decimal OldListPrice { get; set; }
        string saveInternetPriceNApproveAdApi = "/v1/approveAd/dealers/{0}/inventory/{1}";
        decimal? SpecialPrice { get; set; }
        string UserName { get; set; }
        public SaveInternetPricenApproveAdModelbuilder(int businessunitid, int inventoryid, decimal oldlistprice, decimal internetprice, decimal? specialprice,string userName)
        {
            BusinessUnitId = businessunitid;
            InventoryId = inventoryid;
            InternetPrice = internetprice;
            OldListPrice = oldlistprice;
            SpecialPrice = specialprice;
            UserName = userName;
        }


        public UpdateInternetPriceNApproveAdModel Build()
        {

            UpdateInternetPriceNApproveAdModel SaveInternetPriceNApproveAdModel = new UpdateInternetPriceNApproveAdModel();
            try
            {
                var repository = Registry.Resolve<IRepository<UpdateInternetPriceNApproveAdModel, SaveInternetPriceNApproveAdArgumentDto>>();
                var model = repository.Update(new SaveInternetPriceNApproveAdArgumentDto()
                {
                    ApiPass =
                        FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceApiPass,
                    ApiUser =
                        FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceApiUser,
                    Method = "POST",
                    Url = FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceBaseUrl + string.Format(saveInternetPriceNApproveAdApi, BusinessUnitId, InventoryId),
                    NewPrice=InternetPrice,
                    OldPrice = OldListPrice,
                    SpecialPrice = SpecialPrice,
                    UserName = UserName
                });

                SaveInternetPriceNApproveAdModel.AdApproved = model.AdApproved;
            }
            catch(Exception ex) {
                throw ex;
            }

            return SaveInternetPriceNApproveAdModel;
        }

        public UpdateInternetPriceNApproveAdModel Rebuild(UpdateInternetPriceNApproveAdModel model)
        {
            throw new NotImplementedException();
        }
    }
}