﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Builder;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.MarketListings;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.MarketListings;
using Wanamaker.WebApp.Areas.PricingAnalysis.Models.MarketListingsTable;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Builders
{
    public class MarketListingBuilder : IModelBuilder<MarketListingsModel>
    
    {
        MarketListingsArgumentDto PostData { get; set; }
        public const string listingsApi = "/v1/marketListing/search";
        public MarketListingBuilder(MarketListingsArgumentDto postData)
        {
           
            PostData = postData;
        }
        
        public MarketListingsModel Build()
        {
            MarketListingsModel ListingsResponse = new MarketListingsModel();
            try
            {
                var repository = Registry.Resolve<IRepository<MarketListingsModel, MarketListingsArgumentDto>>();
                
                ListingsResponse = repository.Fetch(new MarketListingsArgumentDto()
                {
                    ApiPass =
                                FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceApiPass,
                    ApiUser =
                        FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceApiUser,
                    Method = "POST",
                    Url = FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceBaseUrl + string.Format(listingsApi),                   
                    RequestData = PostData.RequestData

                });
                
            }
            catch (Exception ex)
            {
                throw ex;
            }


            return ListingsResponse;
        }

        public MarketListingsModel Rebuild(MarketListingsModel model)
        {
            throw new NotImplementedException();
        }
    }
}