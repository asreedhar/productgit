﻿using System;
using System.Configuration;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.ModelBuilder.BusinessUnit;
using FirstLook.Merchandising.ModelBuilder.BusinessUnit.BusinessUnitModel;
using FirstLook.Merchandising.ModelBuilder.BusinessUnit.Dto;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Builder;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Builders
{
    public class BusinessUnitBuilder : IModelBuilder<Dealer>
    {
        public int BusinessUnitId { get; set; }

        public const string VhrApi = "/v1/dealerInfo/dealerId/{0}";

        public string BusinessUnitApiUrl
        {
            get
            {
                string vhrapi = ConfigurationManager.AppSettings["BusinessUnitApi"];
                return vhrapi == null ? VhrApi : vhrapi.TrimEnd(new[] { '/', '\\' });
            }
        }

        public BusinessUnitBuilder(int businessunitId)
        {
            this.BusinessUnitId = businessunitId;
        }

        public Dealer Build()
        {
            Dealer businessUnit = new Dealer();
            try
            {
                var repository = Registry.Resolve<IRepository<BusinessUnitModel, BusinessUnitDto>>();

                BusinessUnitModel model = repository.Fetch(new BusinessUnitDto()
                {
                    ApiPass =
                        FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceApiPass,
                    ApiUser =
                        FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceApiUser,
                    Method = "GET",
                    Url = FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceBaseUrl + string.Format(BusinessUnitApiUrl, BusinessUnitId)
                });

                businessUnit.BookOut = model.DealerInfo.BookOut;
                businessUnit.DealerId = model.DealerInfo.DealerId;
                businessUnit.DistanceList = model.DealerInfo.DistanceList;
                businessUnit.EdmundsTrueMarketValue = model.DealerInfo.EdmundsTrueMarketValue;
                businessUnit.FavourableThreshold= model.DealerInfo.FavourableThreshold;
                businessUnit.GuideBook2Id= model.DealerInfo.GuideBook2Id;
                businessUnit.GuideBookID = model.DealerInfo.GuideBookID;
                businessUnit.IsNewPing = model.DealerInfo.IsNewPing;
                businessUnit.KBBRetailValue = model.DealerInfo.KBBRetailValue;
                businessUnit.Latitude = model.DealerInfo.Latitude;
                businessUnit.Longitude = model.DealerInfo.Longitude;
                businessUnit.MarketAvgInternetPrice = model.DealerInfo.MarketAvgInternetPrice;
                businessUnit.NADARetailValue = model.DealerInfo.NADARetailValue;
                businessUnit.OriginalMSRP = model.DealerInfo.OriginalMSRP;
                businessUnit.OwnerName = model.DealerInfo.OwnerName;
                businessUnit.PingIIDefaultSearchRadius= model.DealerInfo.PingIIDefaultSearchRadius;
                businessUnit.PingIIGreenRange1Value1 = model.DealerInfo.PingIIGreenRange1Value1;
                businessUnit.PingIIGreenRange1Value2= model.DealerInfo.PingIIGreenRange1Value2;
                businessUnit.PingIIGuideBookId = model.DealerInfo.PingIIGuideBookId;
                businessUnit.PingIIMarketListing= model.DealerInfo.PingIIMarketListing;
                businessUnit.PingIIMaxSearchRadius = model.DealerInfo.PingIIMaxSearchRadius;
                businessUnit.PingIIRedRange1Value1 = model.DealerInfo.PingIIRedRange1Value1;
                businessUnit.PingIIRedRange1Value2 = model.DealerInfo.PingIIRedRange1Value2;
                businessUnit.PingIIYellowRange1Value1 = model.DealerInfo.PingIIYellowRange1Value1;
                businessUnit.PingIIYellowRange1Value2= model.DealerInfo.PingIIYellowRange1Value2;
                businessUnit.PingIIYellowRange2Value1 = model.DealerInfo.PingIIYellowRange2Value1;
                businessUnit.PingIIYellowRange2Value2 = model.DealerInfo.PingIIYellowRange2Value2;
                businessUnit.UnitCostThreshold = model.DealerInfo.UnitCostThreshold;
                businessUnit.UnitCostThresholdLower = model.DealerInfo.UnitCostThresholdLower;
                businessUnit.UnitCostThresholdUpper = model.DealerInfo.UnitCostThresholdUpper;
                businessUnit.ZipCode = model.DealerInfo.ZipCode;
                
                return businessUnit;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Dealer Rebuild(Dealer model)
        {
            throw new NotImplementedException();
        }
    }
}