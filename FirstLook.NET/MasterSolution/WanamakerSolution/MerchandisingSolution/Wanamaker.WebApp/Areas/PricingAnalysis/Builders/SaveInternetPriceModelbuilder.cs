﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Builder;
using Wanamaker.WebApp.Areas.PricingAnalysis.Models;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.SaveInternetPrice;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.SaveInternetPrice;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Builders
{
    public class SaveInternetPriceModelbuilder:IModelBuilder<UpdateInternetPriceModel>
    {
        public int BusinessUnitId { get; set; }
        public int InventoryId { get; set; }
        public decimal InternetPrice { get; set; }
        public string saveInternetPriceApi = "/v1/vehicles/dealers/{0}/inventory/{1}";
        public SaveInternetPriceModelbuilder(int businessunitid,int inventoryid,decimal internetprice)
        {
            BusinessUnitId = businessunitid;
            InventoryId = inventoryid;
            InternetPrice = internetprice;
        }

        
        public UpdateInternetPriceModel Build()
        {
            
            UpdateInternetPriceModel SaveInternetPriceModel = new UpdateInternetPriceModel();
            try
            {
                var repository = Registry.Resolve<IRepository<SaveInternetPriceModel, SaveInternetPriceArgumentDto>>();
                var model = repository.Update(new SaveInternetPriceArgumentDto() {
                    ApiPass =
                        FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceApiPass,
                    ApiUser =
                        FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceApiUser,
                    Method = "POST",
                    Url = FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceBaseUrl + string.Format(saveInternetPriceApi, BusinessUnitId, InventoryId),
                    NewListPrice=InternetPrice,
                    UserName = HttpContext.Current.User.Identity.Name
                });

                SaveInternetPriceModel.InventoryId = model.InventoryId;
                SaveInternetPriceModel.OldListPrice = model.OldListPrice;
                SaveInternetPriceModel.NewListPrice = model.NewListPrice;                

            }
            catch(Exception ex) {
                throw ex;
            }

            return SaveInternetPriceModel;
        }

        public UpdateInternetPriceModel Rebuild(UpdateInternetPriceModel model)
        {
            throw new NotImplementedException();
        }
    }
}