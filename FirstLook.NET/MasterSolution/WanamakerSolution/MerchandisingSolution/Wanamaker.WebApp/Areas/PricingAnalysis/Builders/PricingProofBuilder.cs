﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wanamaker.WebApp.Areas.PricingAnalysis.Models;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Builder;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.PricingProof;
using System.Configuration;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.PricingProof;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.MarketListings;
using Wanamaker.WebApp.Areas.PricingAnalysis.Models.MarketListingsTable;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.MarketListings;


namespace Wanamaker.WebApp.Areas.PricingAnalysis.Builders
{
    public class PricingProofBuilder : IModelBuilder<PricingProof>
    {
        public string Ver { get; set; }
        public int DealerId { get; set; }
        public List<int> InventoryId { get; set; }
        public int InternetPrice { get; set; }
        public MarketListingsArgumentDto postData { get; set; }
        public int DefaultDealerRadii { get; set; }
        public bool IncludeCurrentVehicle { get; set; }

        public const string PricingProofApi = "/v1/PriceComparisons/dealer/{0}";

        public string PricingProofApiUrl
        {
            get
            {
                string vhrapi = ConfigurationManager.AppSettings["VhrAutocheckApiUrl"];
                return vhrapi == null ? PricingProofApi : vhrapi.TrimEnd(new[] { '/', '\\' });
            }
        }


        public PricingProofBuilder(int dealerId, List<int> inventoryId, int internetPrice, MarketListingsArgumentDto postData,int defaultRadius,bool includeCurrentVehicle)
        {
            this.DealerId = dealerId;
            this.InventoryId = inventoryId;
            this.InternetPrice = internetPrice;
            this.postData = postData;
            this.DefaultDealerRadii = defaultRadius;
            this.IncludeCurrentVehicle = includeCurrentVehicle;
        }

        public PricingProof Build()
        {
            PricingProof pricingProof = new PricingProof();
            PricingProofModel model = new PricingProofModel();
            try
            {
                var repository = Registry.Resolve<IRepository<PricingProofModel, PricingProofArgumentDto>>();

                model = repository.Fetch(new PricingProofArgumentDto()
                {
                    ApiPass =
                        FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceApiPass,
                    ApiUser =
                        FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceApiUser,
                    Method = "POST",
                    Url = FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceBaseUrl + string.Format(PricingProofApiUrl, DealerId),
                    pricingCompare = new PriceComaprison() { InventoryIds = this.InventoryId }
                });

                List<PriceComparison> comparisonList = new List<PriceComparison>();
                foreach (var mo in model.PriceComparisonList)
                {
                    if (mo.PriceComparisonDetail != null)
                    {
                        foreach (var inv in mo.PriceComparisonDetail)
                        {
                            PriceComparison priceComparison = new PriceComparison();
                            priceComparison.DisplayId = inv.BookName;
                            priceComparison.DisplayName = inv.BookName;
                            priceComparison.Threshold = inv.BookValue;
                            priceComparison.BookId = inv.BookId;
                        priceComparison.IsAccurate = inv.IsAccurate;
                            comparisonList.Add(priceComparison);
                        }
                    }

                }

                //   searchOptions.Listings = Model.SearchOption.Listings.Where(ls => ls.IsSelected == true).ToList();
                pricingProof.PriceComparisons = comparisonList;
                pricingProof.InternetPrice = InternetPrice;

                MarketListingBuilder builder = new MarketListingBuilder(postData);
                MarketListingsModel response = new MarketListingsModel();
                SearchInputs searchInput=new SearchInputs();
                response = builder.Build();

                List<RadioItem> radioList = new List<RadioItem>();
                int i = 0;
                foreach (var obj in response.MarketListingsResponse)
                {
                    var radioItem = new RadioItem()
                        {
                            Mileage = string.Format("{0} Miles",
                            postData.RequestData.SearchMarketListings[i].Distance.ToString()),
                            AvgPrice = Convert.ToInt32(obj.PriceStats.Avg),
                            IsSelected = postData.RequestData.SearchMarketListings[i].Distance == DefaultDealerRadii ? true : false,
                            ShowAlert=obj.RecordCount < searchInput.ListingCountThreshold
                        };
                    if (InternetPrice != 0 && IncludeCurrentVehicle == true)
                    {
                        radioItem.AvgPrice = obj.RecordCount == 0 ? 0 : Convert.ToInt32((obj.PriceStats.Sum + (int)InternetPrice) / (obj.RecordCount + 1));
                    }
                    else
                    {
                        radioItem.AvgPrice = obj.RecordCount==0?0: Convert.ToInt32(Convert.ToInt32(obj.PriceStats.Sum)  / (obj.RecordCount));
                    }
                    radioList.Add(radioItem);
                    i++;
                }

                pricingProof.RadioList = radioList;

            }
            catch (Exception ex)
            {
                throw ex;
            }


            return pricingProof;
        }

        public PricingProof Rebuild(PricingProof model)
        {
            throw new NotImplementedException();
        }
    }
}