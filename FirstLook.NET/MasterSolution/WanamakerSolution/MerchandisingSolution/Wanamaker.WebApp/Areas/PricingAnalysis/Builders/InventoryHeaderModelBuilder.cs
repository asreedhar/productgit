﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FirstLook.Common.Core.Command;
using FirstLook.Merchandising.DomainModel.Marketing.Commands;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Builder;
using Wanamaker.WebApp.Areas.PricingAnalysis.Models;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository;
using FirstLook.Common.Core.IOC;
using System.Web.Mvc;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.InventoryHeader;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.InventoryHeader;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types.TransferObjects;
using WebGrease.Css.Extensions;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Builders
{
    public class InventoryHeaderModelBuilder : IModelBuilder<InventoryHeader>
    {
        public int BusinessUnitId { get; set; }
        public double InventoryId { get; set; }
        public const string inventoryHeaderApi = "/v1/pinginventory/{0}/{1}";

        public InventoryHeaderModelBuilder(int businessunitId,int inventoyrId)
        {
            BusinessUnitId = businessunitId;
            InventoryId = inventoyrId;
        }

        public InventoryHeader Build()
        {
            InventoryHeader InventoryDetails = new InventoryHeader();
            var repository = Registry.Resolve<IRepository<InventoryHeaderModel, InventoryHeaderArgumentDto>>();
            
            InventoryHeaderModel model = repository.Fetch(new InventoryHeaderArgumentDto()
            {
                ApiPass =
                           FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceApiPass,
                ApiUser =
                    FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceApiUser,
                Method = "GET",
                Url = FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceBaseUrl + string.Format(inventoryHeaderApi, BusinessUnitId, InventoryId),
                
            }
            );
            List<SelectListItem> trimList = new List<SelectListItem>() { };
            if (model.TrimList != null)
            {
                if (model.InventoryInfo.ChromeStyleId == 0)
                {
                    model.TrimList.ToList().ForEach(data => trimList.Add(new SelectListItem() { Selected = false, Text = data.Trim, Value = Convert.ToString(data.ChromeStyleId) }));
                }
                else
                {
                    TrimInfo trimData=model.TrimList.Where(tl=>tl.ChromeStyleId==model.InventoryInfo.ChromeStyleId).FirstOrDefault();

                    if (trimData != null)
                    {
                        trimList.Add(new SelectListItem() { Selected = true, Text = trimData.Trim, Value = Convert.ToString(trimData.ChromeStyleId) });
                    }

                    model.TrimList.ToList().Where(tl => tl.ChromeStyleId != model.InventoryInfo.ChromeStyleId).ForEach(data => trimList.Add(new SelectListItem() { Selected = false, Text = data.Trim, Value = Convert.ToString(data.ChromeStyleId) }));
                    
                       
                }
            }

            //Set certification
            if (!string.IsNullOrEmpty(model.InventoryInfo.Make))
            {
                var certification = CpoPrograms(model.InventoryInfo.Make);
                var cpt = from x in certification
                          where x.RequiresCertifiedId == true
                          select x;
                if (cpt.FirstOrDefault() != null)
                {
                    CertifiedProgramInfoTO cpTo = cpt.FirstOrDefault();
                    InventoryDetails.CertificationName = "Certified";
                    InventoryDetails.CertifiedProgramId = cpTo.Id;
                }
                else if (model.InventoryInfo.Certified == 1)
                {
                    InventoryDetails.CertificationName = "Certified";
                }
                else
                {
                    InventoryDetails.CertificationName = "";
                }
            }
            else
            {
                InventoryDetails.CertificationName = "";
            }
            
            InventoryDetails.StatusBucket = model.InventoryInfo.StatusBucket;
            InventoryDetails.Certified = model.InventoryInfo.Certified;
            InventoryDetails.Color = ColorPair.GetItemDescription(model.InventoryInfo.ExtColor1,model.InventoryInfo.ExtColor2,string.Empty);
            InventoryDetails.Mileage = model.InventoryInfo.Mileage;
            InventoryDetails.Make = model.InventoryInfo.Make;
            InventoryDetails.Model = model.InventoryInfo.Model;
            InventoryDetails.StockNumber = model.InventoryInfo.StockNumber;
            InventoryDetails.ChromeStyleId = model.InventoryInfo.ChromeStyleId;
            InventoryDetails.UnitCost = model.InventoryInfo.UnitCost;
            InventoryDetails.Year = model.InventoryInfo.Year;
            InventoryDetails.TrimsList = trimList;
            InventoryDetails.Vin = model.InventoryInfo.Vin;
            InventoryDetails.ListPrice = model.InventoryInfo.ListPrice;
            InventoryDetails.AgeInDays =
                Convert.ToInt32((DateTime.Now - model.InventoryInfo.InventoryReceivedDate).TotalDays);
            InventoryDetails.CarsDotComMakeID = model.InventoryInfo.CarsDotComMakeID;
            InventoryDetails.CarsDotComModelId = model.InventoryInfo.CarsDotComModelId;
            InventoryDetails.AutoTraderMake = model.InventoryInfo.AutoTraderMake;
            InventoryDetails.AutoTraderModel = model.InventoryInfo.AutoTraderModel;
           return InventoryDetails;

        }

        private IEnumerable<CertifiedProgramInfoTO> CpoPrograms(string make)
        {
            IEnumerable<CertifiedProgramInfoTO> cpoPrograms = null;

            var certifiedProgamCommand = new CertifiedProgamsForDealerMakeCommand(BusinessUnitId, make);
            AbstractCommand.DoRun(certifiedProgamCommand);
            cpoPrograms = certifiedProgamCommand.Programs;
               
            return cpoPrograms;

            
        }
        public InventoryHeader Rebuild(InventoryHeader model)
        {
            throw new NotImplementedException();
        }
    }
}