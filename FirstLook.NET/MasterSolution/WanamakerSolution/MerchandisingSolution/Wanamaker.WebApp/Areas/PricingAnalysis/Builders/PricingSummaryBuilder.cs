﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Builder;
using Wanamaker.WebApp.Areas.PricingSummary.Models;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.PricingSummary;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.PricingSummary;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository;
using System.Web.Mvc;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Builders
{
    public class PricingSummaryBuilder : IModelBuilder<Summary>
    {
        PricingSummaryArgumentDto PostData;

        public const string pricingSummaryApi = "/v1/pricingSummary/DealerId/{0}";
        public PricingSummaryBuilder(PricingSummaryArgumentDto postData)
        {
            PostData = postData;
        }

        public Summary Build()
        {
            var pricingSummary = new Summary();
            var model = new PricingSummaryModel();

            try
            {
                var repository = Registry.Resolve<IRepository<PricingSummaryModel, PricingSummaryArgumentDto>>();

                model = repository.Fetch(new PricingSummaryArgumentDto()
                {
                    ApiPass =
                                FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceApiPass,
                    ApiUser =
                        FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceApiUser,
                    Method = "POST",
                    Url = FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceBaseUrl + string.Format(pricingSummaryApi, PostData.PricingSummaryArguments.DealerId),
                    PricingSummaryArguments = PostData.PricingSummaryArguments
                });

            }
            catch (Exception ex)
            {
                throw ex;
            }

            var filter = new Filters()
            {
                UnitsInStock = model.PringGraphSummary.UnitsInStock,
                MarketAvg = model.PringGraphSummary.PctMarketAverage,
                MarketDaysSupply = model.PringGraphSummary.MarketDaysSupply,
                AvgInternetPrice = model.PringGraphSummary.AvgInternetPrice,

            };

            var xAxisText = new List<string>();
            var yAxisValues = new List<int>();
            var yAxisPercentage = new List<int>();
            var xAxisId = new List<int>();

            foreach (var obj in model.PricingGraph.Buckets)
            {
                xAxisText.Add(obj.BucketName);
                yAxisValues.Add(obj.Bucketunits);
                yAxisPercentage.Add(obj.BucketPercentage);
                xAxisId.Add(obj.BucketId);
            }

            filter.XaxisText = xAxisText;
            filter.YaxisValues = yAxisValues;
            filter.YaxisPercentage = yAxisPercentage;
            filter.XaxisId = xAxisId;

            var invData = new List<InventoryColumnsModels>();

            foreach (var obj in model.PricingInventoryTable.Inventory)
            {
                var inventoryModel = new InventoryColumnsModels
                {
                    age = obj.Age,
                    color = obj.VehicleColor,
                    internetPrice = (Int32)obj.InternetPrice,
                    invId = obj.InventoryId,
                    marketdayssupply = obj.MarketDaysSupply,
                    mileage = obj.VehicleMileage
                };

                if (obj.PctMarketAverage != null) inventoryModel.pMarketAverage = (Int32)obj.PctMarketAverage;
                inventoryModel.StockNumber = obj.StockNumber;
                inventoryModel.unitcost = (Int32)obj.UnitCost;
                inventoryModel.vehicleDescription = obj.VehicleDescription;
                inventoryModel.make = obj.Make;
                inventoryModel.settingCount = model.PringGraphSummary.DealerFavourableThreshold;
                inventoryModel.ComparisonColor = obj.ComparisonColor;
                inventoryModel.AgeBucket = obj.AgeBucket;

                var books = obj.PriceComparisons.Select(ob => new Book { BookName = ob.BookName, Bookvalue = ob.Bookvalue }).ToList();
                inventoryModel.PriceComparison = books;

                var trims = obj.TrimList.Select(ob => new TrimInfo { ChromeStyleId = ob.ChromeStyleId, Trim = ob.Trim }).ToList();
                inventoryModel.TrimInfo = trims;

                invData.Add(inventoryModel);

            }
            var InventoryType = new List<SelectListItem>() { new SelectListItem() { Selected = true, Text = "All Inventory", Value = "A" }, new SelectListItem() { Selected = false, Text = "Retail Inventory", Value = "R" } };
            var CurrentInventoryType = new List<SelectListItem>() { new SelectListItem() { Selected = true, Text = "Pricing Risk", Value = "R" }, new SelectListItem() { Selected = false, Text = "Age", Value = "A" }, new SelectListItem() { Selected = false, Text = "All", Value = "N" } };
            filter.CurrentInventoryType = CurrentInventoryType;
            filter.InventoryType = InventoryType;
            var gridModel = new InventoryGridModel { GridData = invData };

            pricingSummary.ColModels = gridModel;

            pricingSummary.FilterOption = filter;

            pricingSummary.OwnerName = model.OwnerName;
            if (PostData.PricingSummaryArguments.Mode == "R")
            {
                pricingSummary.BucketName = model.BucketName + " by Age";
            }
            else if (PostData.PricingSummaryArguments.Mode == "A")
            {
                pricingSummary.BucketName = model.BucketName + " by Risk";
            }
            else if (PostData.PricingSummaryArguments.Mode == "N")
            {
                pricingSummary.BucketName = "All Inventory";
            }

            return pricingSummary;
        }

        public Summary Rebuild(Summary model)
        {
            throw new NotImplementedException();
        }
    }
}