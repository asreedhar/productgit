﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Builder;
using Wanamaker.WebApp.Areas.PricingAnalysis.Models;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.SaveTrim;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.SaveTrim;
namespace Wanamaker.WebApp.Areas.PricingAnalysis.Builders
{
    public class SaveTrimModelBuilder : IModelBuilder<SaveSelectedTrimModel>
    {
        public int BusinessUnitId { get; set; }
        public int InventoryId { get; set; }
        public int ChromeStyleId { get; set; }

        public const string saveTrimApi = "/v1/trim/dealers/{0}/inventory/{1}";
        public SaveTrimModelBuilder(int businessunitid, int inventoryid, int chromestyleid)
        {
            BusinessUnitId = businessunitid;
            InventoryId = inventoryid;
            ChromeStyleId = chromestyleid;
        }

        public SaveSelectedTrimModel Build()
        {
            SaveSelectedTrimModel SaveSelectedTrimModel = new SaveSelectedTrimModel();
            try
            {
                var repository = Registry.Resolve<IRepository<SaveTrimModel, SaveTrimArgumentDto>>();
                
                SaveTrimModel model = repository.Update(new SaveTrimArgumentDto()
                 {
                     ApiPass =
                         FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceApiPass,
                     ApiUser =
                         FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceApiUser,
                     Method = "POST",
                     Url = FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceBaseUrl + string.Format(saveTrimApi, BusinessUnitId, InventoryId),
                     ChromeStyleId=ChromeStyleId

                 });

                SaveSelectedTrimModel.ChromeStyleId = model.ChromeStyleId;
                SaveSelectedTrimModel.Flag = model.Flag;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return SaveSelectedTrimModel;
        }

        public SaveSelectedTrimModel Rebuild(SaveSelectedTrimModel model)
        {
            throw new NotImplementedException();
        }
    }
}