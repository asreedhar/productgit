﻿using System;
using System.Configuration;
using System.Collections.Generic;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Builder;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.PricingHistory;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.PricingHistory;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository.PricingHistory;
using Wanamaker.WebApp.Areas.PricingAnalysis.Models;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Builders
{
    public class PricingHistoryModelBuilder:IModelBuilder<IList<PricingHistory>>
    {
        public int BusinessUnitId { get; set; }
        public int Inventoryid { get; set; }

        public const string PhistoryApiUrl = "/v1/showPricingHistory/{0}/{1}";

        public PricingHistoryModelBuilder(int businessunitId, int inventoryid)
        {
            this.BusinessUnitId = businessunitId;
            this.Inventoryid = inventoryid;
        }

        /// <summary>
        /// This method will build Vehicle History ViewModel from Repository.
        /// </summary>
        /// <returns> CarfaxVehicleHistory ViewModel</returns>
        #region IModelBuilder<PricingHistory,PricingHistoryModel> Members
        public IList<PricingHistory> Build()
        {
            IList<PricingHistory> pricingHistory = new List<PricingHistory>();
            try
            {
                var repository = Registry.Resolve<IRepository<Rsponse.PricingHistoryModel, PricingHistoryArgumentDto>>();
                Rsponse.PricingHistoryModel model = repository.Fetch(new PricingHistoryArgumentDto()
                                                                         {
                                                                             ApiPass =
                                                                                 FirstLook.Merchandising.ModelBuilder.
                                                                                 MaxPricingTool.
                                                                                 Helpers.FlServiceApiPass,
                                                                             ApiUser =
                                                                                 FirstLook.Merchandising.ModelBuilder.
                                                                                 MaxPricingTool.
                                                                                 Helpers.FlServiceApiUser,
                                                                             Method = "GET",
                                                                             Url =
                                                                                 FirstLook.Merchandising.ModelBuilder.
                                                                                     MaxPricingTool.
                                                                                     Helpers.FlServiceBaseUrl +
                                                                                 string.Format(PhistoryApiUrl,
                                                                                               BusinessUnitId,
                                                                                               Inventoryid)
                                                                         });

                

                if(model.PricingHistory!=null)
                {

                    pricingHistory = MapPricingHistory(model);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return pricingHistory;
        }
        #endregion

        public PricingHistory Rebuild(PricingHistory model)
        {
            throw new NotImplementedException();
        }
        public IList<PricingHistory> MapPricingHistory(Rsponse.PricingHistoryModel pricingModel)
        {
            IList<PricingHistory> pList = new List<PricingHistory>();
            foreach(var v in pricingModel.PricingHistory)
            {
                PricingHistory pricingHistory = new PricingHistory();
                pricingHistory.appraisername = v.Login;
                pricingHistory.historydate = DateTime.Parse(v.UpdatedDate).ToString("MM/dd/yy"); 
                pricingHistory.money = v.ListPrice;
                pList.Add(pricingHistory);
            }
            return pList;
        }


        public IList<PricingHistory> Rebuild(IList<PricingHistory> model)
        {
            throw new NotImplementedException();
        }
    }
}