﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Builder;
using Wanamaker.WebApp.Areas.PricingAnalysis.Models.MarketListingsTable;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.MarketListings;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.MarketListings;
namespace Wanamaker.WebApp.Areas.PricingAnalysis.Builders
{
    public class MarketListingsModelBuilder : IModelBuilder<List<MarketListingData>>
    {
        MarketListingsArgumentDto PostData { get; set; }

        public const string ListingsApi = "/v1/marketListing/search";

        public decimal DealerLatitude { get; set; }
        public decimal DealerLongitude { get; set; }

        public List<MarketListingData> MapListings(MarketListingsModel listings, MarketListingsModel nonzeroListingsModel)
        {

            List<MarketListingData> marketListingData = new List<MarketListingData>();
            int i=0;
            foreach (var v in listings.MarketListingsResponse)
            {   
                MarketListingData makertListingData = new MarketListingData();
                var overallMarketSupply=listings.MarketListingsResponse.FirstOrDefault().MarketDaysSupplyInfo.FirstOrDefault(mkr => mkr.SearchTypeId == 4);
                makertListingData.OverallMarketDaysSupply=overallMarketSupply==null?0:overallMarketSupply.MarketDaysSupplyCount;

                var matchingMarketSupply = listings.MarketListingsResponse.FirstOrDefault().MarketDaysSupplyInfo.FirstOrDefault(mkr => mkr.SearchTypeId == 1);
                makertListingData.OverallMarketDaysSupply = matchingMarketSupply == null ? 0 : matchingMarketSupply.MarketDaysSupplyCount;

                makertListingData.RecordCountWithZero = listings.MarketListingsResponse[i].RecordCount;
                
                makertListingData.CertifiedTerms = listings.MarketListingsResponse[i].CertifiedTerms;
                makertListingData.DriveTrainTerms = listings.MarketListingsResponse[i].DriveTrainTerms;
                makertListingData.EngineTerms= listings.MarketListingsResponse[i].EngineTerms;
                makertListingData.FuelTypeTerms = listings.MarketListingsResponse[i].FuelTypeTerms;
                makertListingData.TransmissionTerms = listings.MarketListingsResponse[i].TransmissionTerms;
                makertListingData.TrimTerms = listings.MarketListingsResponse[i].TrimTerms;
                makertListingData.OdometerStats = listings.MarketListingsResponse[i].OdometerStats;
                makertListingData.PriceStats = listings.MarketListingsResponse[i].PriceStats;

                makertListingData.RecentActiveCount = listings.MarketListingsResponse[i].RecentActiveCount;
                makertListingData.ActiveCount = listings.MarketListingsResponse[i].ActiveCount;
                makertListingData.RecordCount = nonzeroListingsModel.MarketListingsResponse[i].RecordCount;
                makertListingData.AvgPrice = Convert.ToInt32(nonzeroListingsModel.MarketListingsResponse[i].PriceStats!=null ? nonzeroListingsModel.MarketListingsResponse[i].PriceStats.Avg:0);
                makertListingData.MinPrice = Convert.ToInt32(nonzeroListingsModel.MarketListingsResponse[i].PriceStats != null ? nonzeroListingsModel.MarketListingsResponse[i].PriceStats.Min : 0);
                makertListingData.MaxPrice = Convert.ToInt32(nonzeroListingsModel.MarketListingsResponse[i].PriceStats != null ? nonzeroListingsModel.MarketListingsResponse[i].PriceStats.Max : 0);
                makertListingData.ListingsCount = Convert.ToInt32(nonzeroListingsModel.MarketListingsResponse[i].PriceStats != null ? nonzeroListingsModel.MarketListingsResponse[i].PriceStats.Count : 0);
                makertListingData.SumInternetPrices = Convert.ToInt32(nonzeroListingsModel.MarketListingsResponse[i].PriceStats != null ? nonzeroListingsModel.MarketListingsResponse[i].PriceStats.Sum : 0);
                makertListingData.InternetPrices = nonzeroListingsModel.MarketListingsResponse[i].MarketPrices==null? new List<int>(): nonzeroListingsModel.MarketListingsResponse[i].MarketPrices.ToList();
                makertListingData.MileageAvg = Convert.ToInt32(nonzeroListingsModel.MarketListingsResponse[i].OdometerStats != null ? nonzeroListingsModel.MarketListingsResponse[i].OdometerStats.Avg : 0);
                makertListingData.MileageSum = Convert.ToInt32(nonzeroListingsModel.MarketListingsResponse[i].OdometerStats != null ? nonzeroListingsModel.MarketListingsResponse[i].OdometerStats.Sum : 0);
                makertListingData.MileageCount = Convert.ToInt32(nonzeroListingsModel.MarketListingsResponse[i].OdometerStats != null ? nonzeroListingsModel.MarketListingsResponse[i].OdometerStats.Count : 0);
                decimal avg = Convert.ToDecimal(v.PriceStats == null ? 0 : v.PriceStats.Avg);
                makertListingData.Facets = v.TierOneEquipments;

                foreach (var v1 in v.DealerMarketListings)
                {
                    string[] location = Regex.Split(v1.Location, ","); 
                    

                    makertListingData.MarketlistingsColumns.Add(new MarketListingsColumnsModel()
                                                    {
                                                        Age = v1.Age,
                                                        Color = v1.Color,
                                                        Distance = Convert.ToInt32(GeoLocations.Distance(Convert.ToDouble(DealerLatitude), Convert.ToDouble(DealerLongitude), Convert.ToDouble(location[0]), Convert.ToDouble(location[1]), 'N')),
                                                        InternetPrice = v1.InternetPrice,
                                                        IsCertified = v1.Certified,
                                                        Mileage = v1.Mileage,
                                                        PMarketAverage = avg == 0 ? 0 : (v1.InternetPrice / avg) * 100,
                                                        Seller = v1.Seller,
                                                        VIN = v1.Vin,
                                                        VehicleDescription = string.Format("{0} {1} {2} {3}", v1.Year, v1.Make, v1.Model, v1.Trim),
                                                        MileageAvg = Convert.ToInt32(v.OdometerStats != null ? v.OdometerStats.Avg : 0),
                                                        AdText=v1.AdText
                                                    });

                }
                marketListingData.Add(makertListingData);
            }

            return marketListingData;
        }

        public MarketListingsModelBuilder(MarketListingsArgumentDto postData, decimal dealerLatitude, decimal dealerLongitude)
        {
            DealerLatitude = dealerLatitude;
            DealerLongitude = dealerLongitude;
            PostData = postData;
        }

        public List<MarketListingData> Build()
        {
            List<MarketListingData> marketListings = new List<MarketListingData>();
            try
            {
                var repository = Registry.Resolve<IRepository<MarketListingsModel, MarketListingsArgumentDto>>();

                MarketListingsModel model = repository.Fetch(new MarketListingsArgumentDto()
                {
                    ApiPass =
                                FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceApiPass,
                    ApiUser =
                        FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceApiUser,
                    Method = "POST",
                    Url = FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceBaseUrl + ListingsApi,
                    RequestData = PostData.RequestData
                });

                MarketListingsModel nonzeroMarketListingsModel=null;

                if (PostData.RequestData.SearchMarketListings.Any(sm => sm.IncludeZeroPrice == true))
                {
                    PostData.RequestData.SearchMarketListings.ForEach(sml=>sml.IncludeZeroPrice=false);
                    nonzeroMarketListingsModel = repository.Fetch(new MarketListingsArgumentDto()
                    {
                        ApiPass =
                                    FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceApiPass,
                        ApiUser =
                            FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceApiUser,
                        Method = "POST",
                        Url = FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceBaseUrl + ListingsApi,
                        RequestData = PostData.RequestData
                    });
                }
                if (nonzeroMarketListingsModel != null)
                {
                    marketListings = MapListings(model, nonzeroMarketListingsModel);
                }
                else
                {
                    marketListings = MapListings(model, model);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return marketListings;
        }

        public List<MarketListingData> Rebuild(List<MarketListingData> model)
        {
            throw new NotImplementedException();
        }

      
    }
}