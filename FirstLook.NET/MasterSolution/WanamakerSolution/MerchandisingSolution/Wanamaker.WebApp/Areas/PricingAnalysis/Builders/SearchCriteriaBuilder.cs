﻿using System;
using System.Configuration;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Builder;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.SearchCriteriaDto;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.SearchCriteria;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository;
using Wanamaker.WebApp.Areas.PricingAnalysis.Models;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Builders
{
    public class SearchCriteriaBuilder : IModelBuilder<SearchCriteriaViewModel>
    {
        public string Ver { get; set; }
        public int DealerId { get; set; }
        public int InventoryId { get; set; }
        public int Vin { get; set; }

        public SearchCriteriaDto SearchCriteria { get; set; }

        public const string SeachCriteriaUrl = "/v1/amazonSearchCriteria/dealerId/{0}/inventoryId/{1}";
        public const string SeachCriteriaPostUrl = "/v1/searchCriteria/dealerId/{0}";

        public string SeachCriteriaApiurl
        {
            get
            {
                string apiUrl = ConfigurationManager.AppSettings["SearchCriteriaApiUrl"];
                return apiUrl == null ? SeachCriteriaUrl : apiUrl.TrimEnd(new[] { '/', '\\' });
            }
        }

        public string SeachCriteriaApiPosturl
        {
            get
            {
                string apiUrl = ConfigurationManager.AppSettings["SearchCriteriaApiPostUrl"];
                return apiUrl == null ? SeachCriteriaPostUrl : apiUrl.TrimEnd(new[] { '/', '\\' });
            }
        }

        public SearchCriteriaBuilder(int dealerId, int inventoryId)
        {
            this.DealerId = dealerId;
            this.InventoryId = inventoryId;
        }

        #region IModelBuilder<SearchCriteriaModel> Members

        public SearchCriteriaViewModel Build()
        {
            SearchCriteriaViewModel model=null;
            SearchCriteriaModel searchCriteriaModel;
            try
            {
                var repository = Registry.Resolve<IRepository<SearchCriteriaModel, SearchCriteriaArgumentDto>>();

                searchCriteriaModel = repository.Fetch(new SearchCriteriaArgumentDto()
                {
                    ApiPass =
                        FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceApiPass,
                    ApiUser =
                        FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceApiUser,
                    Method = "GET",
                    Url = FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceBaseUrl + string.Format(SeachCriteriaApiurl, DealerId,InventoryId)
                });

            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (searchCriteriaModel != null && searchCriteriaModel.Criteria!=null)
            {
                model=new SearchCriteriaViewModel()
                      {
                          AvgMileage = searchCriteriaModel.Criteria.AvgMileage,
                          AvgPrice = searchCriteriaModel.Criteria.AvgPrice,
                          Certified = searchCriteriaModel.Criteria.Certified,
                          DealerId = searchCriteriaModel.Criteria.DealerId,
                          Distance = searchCriteriaModel.Criteria.Distance,
                          DriveTrian = searchCriteriaModel.Criteria.DriveTrian,
                          Engine = searchCriteriaModel.Criteria.Engine,
                          Facet = searchCriteriaModel.Criteria.Facet,
                          Fuel = searchCriteriaModel.Criteria.Fuel,
                          InventoryId = searchCriteriaModel.Criteria.InventoryId,
                          Listings = searchCriteriaModel.Criteria.Listings,
                          MaxMileage = searchCriteriaModel.Criteria.MaxMileage,
                          MaxPrice = searchCriteriaModel.Criteria.MaxPrice,
                          MinMileage = searchCriteriaModel.Criteria.MinMileage,
                          MinPrice = searchCriteriaModel.Criteria.MinPrice,
                          NotCertified = searchCriteriaModel.Criteria.NotCertified,
                          OverallCount = searchCriteriaModel.Criteria.OverallCount,
                          PrecisionCount = searchCriteriaModel.Criteria.PrecisionCount,
                          Transmission = searchCriteriaModel.Criteria.Transmission,
                          Trim = searchCriteriaModel.Criteria.Trim,
                          Vin = searchCriteriaModel.Criteria.Vin
                      };
            }

            return model;
        }


        public SearchCriteriaViewModel Rebuild(SearchCriteriaViewModel model)
        {
            SearchCriteriaViewModel searchCriteriaViewModel = new SearchCriteriaViewModel();
            SearchCriteriaModel searchCriteriaModel = new SearchCriteriaModel();
            try
            {
                var repository = Registry.Resolve<IRepository<SearchCriteriaModel, SearchCriteriaArgumentDto>>();

                searchCriteriaModel = repository.Update(new SearchCriteriaArgumentDto()
                {
                    ApiPass =
                        FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceApiPass,
                    ApiUser =
                        FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceApiUser,
                    Method = "POST",
                    Url = FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Helpers.FlServiceBaseUrl + string.Format(SeachCriteriaApiPosturl, DealerId),
                    SearchCriteriaDto = SearchCriteria
                });

            }
            catch (Exception ex)
            {
                throw ex;
            }


            return searchCriteriaViewModel;
        }

        #endregion
    }
}