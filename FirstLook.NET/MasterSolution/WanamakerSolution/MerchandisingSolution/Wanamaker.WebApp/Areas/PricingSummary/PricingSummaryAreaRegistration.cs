﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Wanamaker.WebApp.Areas.PricingSummary
{
    public class PricingSummaryAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PricingSummary";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PricingSummary",
                "PricingSummary/{controller}/{action}",
                new { action = "Index" }
            );
        }

    }
}