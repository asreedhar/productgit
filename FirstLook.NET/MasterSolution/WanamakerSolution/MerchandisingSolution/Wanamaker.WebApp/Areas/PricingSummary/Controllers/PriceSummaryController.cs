﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.DataAccess;
using FirstLook.Merchandising.DomainModel.Workflow;
using MAX.Entities.Enumerations;
using Wanamaker.WebApp.Areas.PricingSummary.Models;
using Wanamaker.WebApp.Areas.PricingAnalysis.Builders;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.PricingSummary;
using MAX.Entities.Helpers.DashboardHelpers;
using Wanamaker.WebApp.AppCode;
using Wanamaker.WebApp.Areas.PricingAnalysis.Models;
using System.Text.RegularExpressions;
using FirstLook.Merchandising.ModelBuilder.BusinessUnit;
using Newtonsoft.Json;
using FirstLook.DomainModel.Oltp;

namespace Wanamaker.WebApp.Areas.PricingSummary.Controllers
{
    public class PriceSummaryController : Controller
    {
        public int PingBusinessUnitId
        {
            get
            {
                var businessUnit = this.HttpContext == null ? null : this.HttpContext.Items["BusinessUnit"] as BusinessUnit;

                if (businessUnit != null)
                    return businessUnit.Id;
                else
                {
                    return WorkflowState.BusinessUnit.Id;
                }
            }
        }
        //
        // GET: /PricingSummary/PriceSummary/
        public Summary Summary { get; set; }
        /// <summary>
        /// WARNING: If the session expires with the page still open, Dealer can be null for AJAX requests which will throw exceptions. TODO: fix this.
        /// </summary>
        private Dealer Dealer { get { return HttpContext.Session[Dealer.HttpContextKey] as Dealer; } set { HttpContext.Session[Dealer.HttpContextKey] = value; } }
        private int BusinessUnitId { get { return Convert.ToInt32(HttpContext.Session[WebSettings.BusinessUnitIdContextKey]); } set { HttpContext.Session[WebSettings.BusinessUnitIdContextKey] = value; } }

        public void ReloadBusinessUnitDetails(int businessUnitId)
        {
            #region BusinessUnitDetails
            //Get BusinessUnit details

            var businessUnitBuilder = new BusinessUnitBuilder(businessUnitId);
            FirstLook.Merchandising.DomainModel.Dashboard.Abstract.IDashboardBusinessUnit businessUnit = new FirstLook.Merchandising.DomainModel.Dashboard.Concrete.DashboardBusinessUnit();
            Dealer = businessUnitBuilder.Build();
            businessUnit.BusinessUnitId = Dealer.DealerId;
            businessUnit.Name = Dealer.OwnerName;
            //Used in the Header.cshtml
            ViewBag.BusinessUnit = businessUnit;
            ViewBag.IsReportViewer = Helper.IsReportViewer;
            ViewBag.IsMaxStandAlone = WebSettings.IsMaxStandAlone;
            ViewBag.IsAdministrator = Helper.IsAdministrator;
            ViewBag.IsSupportEnabled = false;
            ViewBag.HasPricingSummary = DealerPingAccess.CheckDealerPingAccess(businessUnit.BusinessUnitId);
            ViewBag.IsGldEnabled = GetMiscSettings.GetSettings(WorkflowState.BusinessUnitID).ShowGroupLevelDashboard;
            ViewBag.ShowOnlineClassifiedOverview = GetMiscSettings.GetSettings(businessUnit.BusinessUnitId).showOnlineClassifiedOverview;
            ViewBag.IsOemBranded = GetMiscSettings.GetSettings(businessUnit.BusinessUnitId).Franchise != Franchise.None;
            #endregion
        }

        public ActionResult Index(int? businessUnitId)
        {
            int currentBusinessUnitId = Convert.ToInt32(businessUnitId) > 0 ? Convert.ToInt32(businessUnitId) : PingBusinessUnitId;
            ReloadBusinessUnitDetails(currentBusinessUnitId);
            BusinessUnitId = currentBusinessUnitId;

            var model = new PriceView();
            
            var arg = new PricingSummaryArgumentDto()
                {
                    PricingSummaryArguments = new PricingSummaryDto()
                    {
                        DealerId = Dealer.DealerId,
                        Mode = "R",
                        SalesStrategy = "A",
                        ColumnIndex = 2,
                        MaximumRows = 25,
                        StartRowIndex = 1,
                        InventoryFilter = null,
                        IsAggregateAndDetail = true

                    }
                };

            
            var builder = new PricingSummaryBuilder(arg);
            Summary = builder.Build();
            model.FilterOptions = Summary.FilterOption;
            for (int i = 0; i < model.FilterOptions.XaxisText.Count();i++)
            {
                if (model.FilterOptions.XaxisText[i] == "Priced at Market" )
                {
                    model.FilterOptions.XaxisText[i] = "Priced at Market" + "<br>" + "(" + Dealer.PingIIGreenRange1Value1 + "%-" + Dealer.PingIIGreenRange1Value2 + "%)";
                }
                if (model.FilterOptions.XaxisText[i] == "No Favorable Price Comparisons")
                {
                    model.FilterOptions.XaxisText[i] = "No Favorable" + "<br>" + "Price Comparisons";
                }
            }           
            model.GridData = Summary.ColModels.GridData;
            model.OwnerName = Summary.OwnerName;            
            model.Input = arg.PricingSummaryArguments;
            model.GreenValue1 = Dealer.PingIIGreenRange1Value1;
            model.GreenValue2 = Dealer.PingIIGreenRange1Value2;
            model.BucketName = Summary.BucketName;
            model.PageTitle = "Profit MAX | Converting Browsers into Buyers.";
            model.BusinessUnitId = arg.PricingSummaryArguments.DealerId;
            return View(model);
        }
        
        //Update Price summary
        
        public JsonResult UpdatePricingSummary(PricingSummaryDto input)
        {
            var arg = new PricingSummaryArgumentDto()
            {
                PricingSummaryArguments = input
            };

            var builder = new PricingSummaryBuilder(arg);
            Summary = builder.Build();
            for (int i = 0; i < Summary.FilterOption.XaxisText.Count(); i++)
            {
                if (Summary.FilterOption.XaxisText[i] == "Priced at Market")
                {
                    Summary.FilterOption.XaxisText[i] = "Priced at Market" + "<br>" + "(" + Dealer.PingIIGreenRange1Value1 + "%-" + Dealer.PingIIGreenRange1Value2 + "%)";
                }
                if (Summary.FilterOption.XaxisText[i] == "No Favorable Price Comparisons")
                {
                    Summary.FilterOption.XaxisText[i] = "No Favorable" + "<br>" + "Price Comparisons";
                }
            }       
            var jr = new JsonResult { Data = Summary, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            return jr;
        }

        //Update InternetPrice        
        public JsonResult SaveInternetPrice(string internetPrice, int inventoryId)
        {
            ReloadBusinessUnitDetails(BusinessUnitId);

            decimal internetprice = Convert.ToDecimal(Regex.Replace(internetPrice, @"[^\d]", ""));
            var builder = new SaveInternetPriceModelbuilder(BusinessUnitId, inventoryId, internetprice);
            UpdateInternetPriceModel response = builder.Build();
            JsonResult jr = new JsonResult();
            jr.Data = Dealer.DealerId;
            jr.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return jr;

        }

        //Save Trim
        public bool SaveTrim(string chromeStyleId, int inventoryId)
        {
            ReloadBusinessUnitDetails(BusinessUnitId);
            var builder = new SaveTrimModelBuilder(BusinessUnitId, inventoryId, Convert.ToInt32(chromeStyleId));
            SaveSelectedTrimModel response = builder.Build();
            return response.Flag;
        }

        //Export
        public CSVResultInventories ExportInventories(FormCollection frmCollection)
        {
            if (frmCollection.Get(0) != null || frmCollection.Get(0) != "")
            {
                var input = JsonConvert.DeserializeObject<PricingSummaryDto>(frmCollection.Get(0));

                var arg = new PricingSummaryArgumentDto
                {
                    PricingSummaryArguments = input
                };

                var builder = new PricingSummaryBuilder(arg);
                Summary = builder.Build();



                if (Summary.ColModels.GridData != null && Summary.ColModels.GridData.Count > 0)
                {
                    var export = new ExportData();
                    var exportList = new List<ExportDataColumns>();
                    for (int i = 0; i < Summary.ColModels.GridData.Count(); i++)
                    {
                        var column = new ExportDataColumns
                                     {
                                         Age = Summary.ColModels.GridData[i].age,
                                         VehicleDescription =
                                             Summary.ColModels.GridData[i].vehicleDescription,
                                         Color = Summary.ColModels.GridData[i].color,
                                         Mileage = Summary.ColModels.GridData[i].mileage,
                                         UnitCost = Summary.ColModels.GridData[i].unitcost,
                                         InternetPrice = Summary.ColModels.GridData[i].internetPrice,
                                         PMarketAverage = Summary.ColModels.GridData[i].pMarketAverage,
                                         Marketdayssupply =
                                             Summary.ColModels.GridData[i].marketdayssupply,
                                         PriceComparisons = null
                                     };
                        for (var j = 0; j < Summary.ColModels.GridData[i].PriceComparison.Count(); j++)
                        {
                            if (Summary.ColModels.GridData[i].PriceComparison[j].Bookvalue > 0)
                            {
                                column.PriceComparisons += "$" + String.Format("{0:#,##0}", Summary.ColModels.GridData[i].PriceComparison[j].Bookvalue) +
                                                       " below " + Summary.ColModels.GridData[i].PriceComparison[j].BookName +
                                                       ", ";
                            }
                            else if (Summary.ColModels.GridData[i].PriceComparison[j].Bookvalue < 0)
                            {
                                column.PriceComparisons += "$" + String.Format("{0:#,##0}", Summary.ColModels.GridData[i].PriceComparison[j].Bookvalue) +
                                                       " above " + Summary.ColModels.GridData[i].PriceComparison[j].BookName +
                                                       ", ";
                            }
                        }

                        if (column.PriceComparisons != null)
                        {
                            column.PriceComparisons = column.PriceComparisons.Remove(column.PriceComparisons.Length - 2);
                            column.PriceComparisons = string.Format("\"" + "{0}" + "\"", column.PriceComparisons);
                        }
                        if (column.PriceComparisons == null)
                        {
                            column.PriceComparisons = "No Favorable Price Comparisons";
                        }

                        exportList.Add(column);
                    }
                    export.Columns = exportList;

                    return new CSVResultInventories(export, "Inventories.csv");
                }
                return new CSVResultInventories(new ExportData() { Columns=new List<ExportDataColumns>()}, "Inventories.csv");
            }
            else
            {
                return new CSVResultInventories(new ExportData() { Columns = new List<ExportDataColumns>() }, "Inventories.csv");

            }
        }

    }
}
