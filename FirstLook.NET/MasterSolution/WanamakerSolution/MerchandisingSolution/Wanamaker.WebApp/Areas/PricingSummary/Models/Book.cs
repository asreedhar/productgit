﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wanamaker.WebApp.Areas.PricingSummary.Models
{
    public class Book
    {
        public string BookName { get; set; }
        public int Bookvalue { get; set; }
    }
}