﻿
using System.Runtime.Serialization;

namespace Wanamaker.WebApp.Areas.PricingSummary.Models
{
    [DataContract]
    public class ExportDataColumns
    {
        [DataMember(Name = "Age")]
        public int Age { get; set; }

        [DataMember(Name = "Vehicle Description")]
        public string VehicleDescription { get; set; }

        [DataMember(Name = "Color")]
        public string Color { get; set; }

        [DataMember(Name = "Mileage")]
        public double Mileage { get; set; }

        [DataMember(Name = "Unit Cost")]
        public int UnitCost { get; set; }

        [DataMember(Name = "Internet Price")]
        public int InternetPrice { get; set; }

        [DataMember(Name = "% of Market Avg")]
        public int PMarketAverage { get; set; }

        [DataMember(Name = "Market Days Supply")]
        public int Marketdayssupply { get; set; }

        [DataMember(Name = "Price Comparison(s)")]
        public string PriceComparisons { get; set; }
    }
}