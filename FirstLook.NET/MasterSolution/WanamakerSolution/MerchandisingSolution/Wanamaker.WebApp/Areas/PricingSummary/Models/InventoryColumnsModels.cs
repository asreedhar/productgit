﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wanamaker.WebApp.Areas.PricingSummary.Models
{
    public class InventoryColumnsModels
    {
        public int invId { get; set; }
        public int age { get; set; }
        public string vehicleDescription { get; set; }
        public string make { get; set; }
        public string color { get; set; }
        public double mileage { get; set; }
        public int unitcost { get; set; }
        public int internetPrice { get; set; }
        public int pMarketAverage { get; set; }
        public string pricing { get; set; }
        public int marketdayssupply { get; set; }
        public string StockNumber { get; set; }
        public List<Book> PriceComparison { get; set; }
        public List<TrimInfo> TrimInfo { get; set; }
        public int settingCount { get; set; }
        public int ComparisonColor { get; set; }
        public int AgeBucket { get; set; }
    }
}