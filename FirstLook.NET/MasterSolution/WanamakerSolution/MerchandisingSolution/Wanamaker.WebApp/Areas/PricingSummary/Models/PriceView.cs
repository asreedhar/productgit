﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.PricingSummary;

namespace Wanamaker.WebApp.Areas.PricingSummary.Models
{
    public class PriceView : PriceCoreView
    {
        public string OwnerName { get; set; }
        public Filters FilterOptions { get; set; }
        public List<InventoryColumnsModels> GridData { get; set; }
        public PricingSummaryDto Input { get; set; }
        public int BusinessUnitId { get; set; }
        public int GreenValue1 { get; set; }
        public int GreenValue2 { get; set; }
        public string BucketName { get; set; }
    }
}