﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Wanamaker.WebApp.Areas.PricingSummary.Models
{
    public class Filters
    {
        public List<SelectListItem> InventoryType { get; set; }
        public List<SelectListItem> CurrentInventoryType { get; set; }
        public int UnitsInStock { get; set; }
        public int AvgInternetPrice { get; set; }
        public int MarketAvg { get; set; }
        public int MarketDaysSupply { get; set; }
        public List<int> YaxisValues { get; set; }
        public List<string> XaxisText{ get; set; }
        public List<int> YaxisPercentage { get; set; }
        public List<int> XaxisId { get; set; }

    }
}