﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wanamaker.WebApp.Areas.PricingSummary.Models
{
    public class Summary
    {
        public string OwnerName { get; set; }
        public Filters FilterOption{ get; set; }
        public InventoryGridModel ColModels { get; set; }
        public string BucketName { get; set; }
    }
}