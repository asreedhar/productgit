﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wanamaker.WebApp.Areas.PricingSummary.Models
{
    public class TrimInfo
    {        
        public int ChromeStyleId { get; set; }        
        public string Trim { get; set; }
    }
}