﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wanamaker.WebApp.Areas.PricingSummary.Models
{
    public class ExportData
    {
        public List<ExportDataColumns> Columns { get; set; }
    }
}