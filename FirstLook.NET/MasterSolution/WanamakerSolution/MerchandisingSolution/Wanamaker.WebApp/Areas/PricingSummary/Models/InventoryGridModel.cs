﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wanamaker.WebApp.Areas.PricingSummary.Models
{
    public class InventoryGridModel
    {
        public List<InventoryColumnsModels> GridData { get; set; }
    }
}