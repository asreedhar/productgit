﻿using System.Web.Mvc;

namespace Wanamaker.WebApp.Areas.ExternalCredentials
{
    public class ExternalCredentialsAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExternalCredentials";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Home",
                "ExternalCredentials/{businessUnitId}",
                new {action = "Index", controller = "Home"}
                );

            context.MapRoute(
                "CredentialData",
                "ExternalCredentials.aspx/get/{businessUnitId}",
                new {controller = "Home", action = "CredentialData"}
                );

            context.MapRoute(
                "SaveCredential",
                "ExternalCredentials.aspx/set",
                new { controller = "Home", action = "SaveCredential"}
                );

            context.MapRoute(
                "GetDealerList",
                "ExternalCredentials.aspx/dealers/{businessUnitId}",
                new { controller = "Home", action = "GetDealers" }
            );

        }
    }
}
