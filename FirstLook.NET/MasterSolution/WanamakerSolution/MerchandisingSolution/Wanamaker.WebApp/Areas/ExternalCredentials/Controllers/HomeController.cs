﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using MAX.ExternalCredentials;
using Wanamaker.WebApp.Areas.ExternalCredentials.Models;
using FirstLook.Merchandising.DomainModel.Dashboard;
using log4net;
using ICache = FirstLook.Common.Core.ICache;

namespace Wanamaker.WebApp.Areas.ExternalCredentials.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICache _cache;
        private readonly ICredentialRepository _credentialRepository;
        private readonly ILog _log = LogManager.GetLogger(typeof(HomeController).FullName);

        public HomeController(ICredentialRepository credentialRepository, ICache cache)
        {
            _credentialRepository = credentialRepository;
            _cache = cache;
        }

        public ActionResult Index(int businessUnitId)
        {
            if (!IsAdministrator) return View(GetModel("User is not authorized."));

            var model = GetModel(businessUnitId);
            return View(model);
        }

        [HttpGet]
        public JsonResult CredentialData(int businessUnitId)
        {
            var model = GetModel(businessUnitId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        public JsonResult GetDealers(int businessUnitId)
        {
            JsonResult res = Json(new Object(), JsonRequestBehavior.AllowGet);
            
            if (businessUnitId > 0)
            {
                var units = new UserBusinessUnits("/merchandising/Default.aspx?ref=GLD", _cache, IsAdministrator, businessUnitId);
                res = Json(units.GenerateJson(HttpContext.User.Identity.Name), JsonRequestBehavior.AllowGet);
            }
            return res;
        }

        [HttpPost]
        public JsonResult SaveCredential(InputModel model)
        {
            try
            {
                foreach (var inputCredential in model.Credentials)
                {
                    switch (inputCredential.GroupId)
                    {
                        case (int)CredentialGroup.OEM:
                            _credentialRepository.SaveCredential(model.BusinessUnitId, inputCredential.ToOemSiteCredential());
                            break;
                        case (int)CredentialGroup.ListingSite:
                            _credentialRepository.SaveCredential(model.BusinessUnitId, inputCredential.ToListingSiteCredential());
                            break;
                        default:
                            throw new Exception(String.Format("Unexpected groupId {0}",inputCredential.GroupId));
                    }
                    
                }

                return Json(new { success = true });
            }
            catch (SqlException ex)
            {
                _log.Error("Error save credentials from nag dialog", ex);

                Response.StatusCode = (int)HttpStatusCode.InternalServerError;

                return Json(new { success = false, message = ex.Message });
            }
            catch (Exception ex)
            {
                _log.Error("Error save credentials from nag dialog", ex);

                Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                
                return Json(new { success = false, message = "An error has occurred while saving your credentials." });
            }
        }



        private OutputModel GetModel(int businessUnitId)
        {
            var credentials = _credentialRepository.GetCredentials(businessUnitId);

            return new OutputModel
            {
                BusinessUnitId = businessUnitId,
                HasInvalidCredentials = credentials.Any(c=>!c.IsValid),
                OEMCredentials = credentials.Where(c => c.Group == CredentialGroup.OEM).Select(CredentialAdapter.Adapt).ToList(),
                SiteCredentials = credentials.Where(c => c.Group == CredentialGroup.ListingSite).Select(CredentialAdapter.Adapt).ToList()
            };
        }
        
        private OutputModel GetModel(string message)
        {
            return new OutputModel
            {
                Message = message
            };
        }

        public bool IsAdministrator
        {
            get { return HttpContext != null && HttpContext.User.IsInRole("Administrator"); }
        }
    }
}
