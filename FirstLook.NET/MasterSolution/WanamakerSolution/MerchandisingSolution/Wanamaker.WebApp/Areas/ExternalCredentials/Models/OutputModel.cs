﻿using System;
using System.Collections.Generic;
using Microsoft.AnalysisServices.AdomdClient;


namespace Wanamaker.WebApp.Areas.ExternalCredentials.Models
{
    public class OutputModel
    {
        public int BusinessUnitId { get; set; }

        public bool HasInvalidCredentials { get; set; }

        public IEnumerable<Credential> OEMCredentials { get; set; }

        public IEnumerable<Credential> SiteCredentials { get; set; }

        public string Message { get; set; }


        public class Credential
        {
            public string GroupDescription { get; set; }
            public int GroupId { get; set; }
            public string TypeDescription { get; set; }
            public int TypeId { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
            public bool? IsValid { get; set; }
            public string InvalidMessage { get; set; }
            public string CredentialVersion { get; set; }
        }
    }
}