using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Postings;
using MAX.ExternalCredentials;
using Merchandising.Messages;

namespace Wanamaker.WebApp.Areas.ExternalCredentials.Models
{
    public class InputModel
    {
        public int BusinessUnitId { get; set; }
        public IEnumerable<InputCredential> Credentials { get; set; }

        public class InputCredential
        {
            public int GroupId { get; set; }
            public int TypeId { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
            public string CredentialVersion { get; set; }
            public OEMSiteCredential ToOemSiteCredential()
            {
                UInt64 version;
                if (! UInt64.TryParse(CredentialVersion, out version))
                {
                    throw new Exception("Unable to parse input CredentialVersion as UInt64");
                }
                return new OEMSiteCredential((Agent)TypeId, Username ?? string.Empty, Password ?? string.Empty, true, BitConverter.GetBytes(version));
            }
            public ListingSiteCredential ToListingSiteCredential()
            {
                return new ListingSiteCredential((EdtDestinations)TypeId, Username ?? string.Empty, Password ?? string.Empty, true);
            }
        }

        
    }
}