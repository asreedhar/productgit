using System;
using MAX.ExternalCredentials;

namespace Wanamaker.WebApp.Areas.ExternalCredentials.Models
{
    
    public static class CredentialAdapter
    {

        public static OutputModel.Credential Adapt(ICredential credential)
        {
            var value = new OutputModel.Credential
                       {
                           GroupId = (int) credential.Group,
                           GroupDescription = credential.Group.ToString(),
                           TypeId = credential.TypeId,
                           TypeDescription = credential.Description,
                           Username = credential.UserName,
                           Password = credential.Password,
                           IsValid = credential.IsValid,
                           InvalidMessage = credential.IsInsufficientPermissions ? "Insufficient Privileges" : "Invalid"
                       };
            if (credential.CredentialVersion != null)
            {
                value.CredentialVersion = Convert.ToString(BitConverter.ToUInt64(credential.CredentialVersion, 0));
            }
            return value;
        }

    }
}