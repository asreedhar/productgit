﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wanamaker.WebApp.Areas.Inventory.ViewModels
{
    public class ClickThroughRate
    {
        public string Date { get; set; }
        public float Rate { get; set; }
        public bool StoredValue { get; set; }
    }
}