using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Workflow;

namespace Wanamaker.WebApp.Areas.Inventory.ViewModels
{
    public class InventoryAdapter
    {
        private readonly IInventoryData _inner;

        public InventoryAdapter(IInventoryData inner)
        {
            _inner = inner;
        }

        public int Id { get { return _inner.InventoryID; } }
        public string VIN { get { return _inner.VIN; } }
        public string StockNumber { get { return _inner.StockNumber; } }

        public int Age { get { return _inner.Age; } }
        public string Year { get { return _inner.Year; } }
        public string Make { get { return _inner.Make; } }
        public string Model { get { return _inner.Model; } }
        public string Trim { get { return _inner.Trim; } }
        public string AfterMarketTrim { get { return _inner.AfterMarketTrim; } }
        public string MarketClass { get { return _inner.MarketClass; } }

        public decimal InternetPrice { get { return _inner.ListPrice; } }
        public int PhotoCount { get { return _inner.PhotoCount; } }
        public string NewOrUsed { get { return _inner.IsNew() ? "New" : "Used"; } }
        public string ExtColor
        {
            get
            {
                var ext1Desc = (_inner.Ext1Desc ?? "").Trim();
                var ext2Desc = (_inner.Ext2Desc ?? "").Trim();
                return ext1Desc + (ext1Desc != "" && ext2Desc != "" ? " / " : "") + ext2Desc;
            }
        }
        public string IntColor { get { return (_inner.IntDesc ?? "").Trim(); } }
        public string Certified { get { return FormatBool(_inner.Certified); } }
        public string TradeOrPurchase { get { return _inner.TradeOrPurchase; } }
        public string Mileage { get { return _inner.DisplayMileage; } }

        public string Approved { get { return FormatBool(WorkflowFilters.Online(_inner)); } }
        public string Offline { get { return FormatBool(WorkflowFilters.DoNotPost(_inner)); } }

        public string NoDescription { get { return FormatBool(WorkflowFilters.NotPosted(_inner)); } }
        public string NoTrim { get { return FormatBool(WorkflowFilters.IsPostableAndNoTrim(_inner)); } }
        public string OutdatedPriceInAd { get { return FormatBool(WorkflowFilters.OnlineAndOutdatedPrice(_inner)); } }
        public string AdReviewNeeded { get { return FormatBool(WorkflowFilters.IsPostableAndAdReviewNeeded(_inner)); } }

        public string NoPrice { get { return FormatBool(WorkflowFilters.IsPostableAndNoListPrice(_inner)); } }
        public string NeedsRepricing { get { return FormatBool(WorkflowFilters.IsPostableAndUsedAndDueForRepricing(_inner)); } }

        public string NoPackages { get { return FormatBool(WorkflowFilters.IsPostableAndNoPackages(_inner)); } }
        public string NoBookValue { get { return FormatBool(WorkflowFilters.IsPostableAndUsedAndNoBookValue(_inner)); } }
        public string NoCarfax { get { return FormatBool(WorkflowFilters.IsPostableAndUsedAndNoCarfax(_inner)); } }
        public string LowOnlineActivity { get { return FormatBool(WorkflowFilters.IsPostableAndLowActivity(_inner)); } }

        public string EquipmentReview { get { return FormatBool(WorkflowFilters.IsPostableAndEquipmentNeeded(_inner)); } }
        public string LowPhotos { get { return FormatBool(WorkflowFilters.IsPostableAndLowPhotos(_inner)); } }
        public string NoPhotos { get { return FormatBool(WorkflowFilters.IsPostableAndNoPhotos(_inner)); } }

        private static string FormatBool(bool val)
        {
            return val ? "1" : "0";
        }
    }
}