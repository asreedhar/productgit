﻿using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;

namespace Wanamaker.WebApp.Areas.Inventory.ViewModels
{
    public class NewCarPricingModel
    {
        public int RebateAmount { get; set; }
        public int DiscountAmount { get; set; }
        public PriceBaseLine PriceBaseLine { get; set; }
        public string ExpirationDate { get; set; }
        public List<int> InventoryIds { get; set; }
        public List<string> Year { get; set; }
        public List<string> Make { get; set; }
        public List<string> Model { get; set; }
        public List<string> Trim { get; set; }
        public List<string> BodyStyle { get; set; }
        public List<CampaignEngineDescriptionInfo> EngineDescription { get; set; }

        public static bool Validate(NewCarPricingModel priceInfo, out string[] errorMessage)
        {
            var valid = true;
            var errors = new List<string>();

            if(priceInfo.RebateAmount == 0 && priceInfo.DiscountAmount == 0)
            {
                errors.Add("You must set at least one discount value");
                valid = false;
            }

            if(DateTime.Parse(priceInfo.ExpirationDate) < DateTime.Today)
            {
                errors.Add("The expiration date must be in the future");
                valid = false;
            }

            errorMessage = errors.ToArray();
            return valid;
        }

        public static void PostProcess(NewCarPricingModel priceInfo)
        {
            // mostly guarantees initialization of IEnumerables
            if( priceInfo.InventoryIds == null )
                priceInfo.InventoryIds = new List<int>();
            
            if( priceInfo.Year == null )
                priceInfo.Year = new List<string>();
            if( priceInfo.Make == null )
                priceInfo.Make = new List<string>();
            if( priceInfo.Model == null )
                priceInfo.Model = new List<string>();
            if( priceInfo.Trim == null )
                priceInfo.Trim = new List<string>();
            if( priceInfo.BodyStyle == null )
                priceInfo.BodyStyle = new List<string>();
            if( priceInfo.EngineDescription == null )
                priceInfo.EngineDescription = new List<CampaignEngineDescriptionInfo>();
        }
    }
}