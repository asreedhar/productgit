﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wanamaker.WebApp.Areas.Inventory.ViewModels
{
    public class RepriceEvent
    {
        public string Date { get; set; }
        public decimal Price { get; set; }
    }
}