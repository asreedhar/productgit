﻿using System.Collections.Generic;

namespace Wanamaker.WebApp.Areas.Inventory.ViewModels
{
    public class PricingCampaignViewModel
    {
        public int CampaignId { get; set; }
        public string CampaignName { get; set; }
        public string CreationDate { get; set; }
        public string PriceBaseLine { get; set; }
        public decimal RebateAmount { get; set; }
        public decimal IncentiveAmount { get; set; }
        public string ExpirationDate { get; set; }
        public string CreatedBy { get; set; }

        public List<PricingCampaignInventoryViewModel> InventoryList { get; set; }
    }
}