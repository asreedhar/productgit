﻿

namespace Wanamaker.WebApp.Areas.Inventory.ViewModels
{
    public class ChromeStyleVm
    {
        public string Trim { get; set; }

        public int ChromeStyleId { get; set; }

        public string ChromeStyleName { get; set; }
    }
}