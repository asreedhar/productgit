﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wanamaker.WebApp.Areas.Inventory.ViewModels
{
    public class VehiclePerformance
    {
        public VehiclePerformance()
        {
            PriceEvents = new List<RepriceEvent>();
            Sources = new Dictionary<string, IList<ClickThroughRate>>();
        }

        public int PhotoCount { get; set; }
        public string YearMakeModel { get; set; }
        public string ExteriorColor { get; set; }
        public string Mileage { get; set; }
        public bool Certfied { get; set; }

        public IList<RepriceEvent> PriceEvents { get; set; }
        public Dictionary<string, IList<ClickThroughRate>> Sources { get; set; }
    }
}