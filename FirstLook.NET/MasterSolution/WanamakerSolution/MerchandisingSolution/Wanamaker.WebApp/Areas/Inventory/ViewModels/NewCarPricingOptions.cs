﻿using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;

namespace Wanamaker.WebApp.Areas.Inventory.ViewModels
{
    public class NewCarPricingOptions
    {
        public List<int> InventoryIds { get; set; }
        public string SelectedValue { get; set; }
        public Dictionary<string, string> Options { get; set; } 
        public CampaignFilter CampaignFilter { get; set; }
    }
}