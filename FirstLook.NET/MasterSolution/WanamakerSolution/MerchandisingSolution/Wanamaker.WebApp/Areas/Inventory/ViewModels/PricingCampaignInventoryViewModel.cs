﻿namespace Wanamaker.WebApp.Areas.Inventory.ViewModels
{
    public class PricingCampaignInventoryViewModel
    {
        public string StockNumber { get; set; }
        public string OriginalPrice { get; set; }
        public string NewInternetPrice { get; set; }
        public string ActivatedBy { get; set; }
        public string DeactivatedBy { get; set; }
        public string Active { get; set; }
        // FB: 27649 - add MSRP and unit cost, tureen 10/25/2013
        public decimal MSRP { get; set; }
        public decimal UnitCost { get; set; }

    }
}