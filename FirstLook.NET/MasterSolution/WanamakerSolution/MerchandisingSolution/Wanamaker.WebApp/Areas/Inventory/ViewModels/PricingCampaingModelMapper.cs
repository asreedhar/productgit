﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;

namespace Wanamaker.WebApp.Areas.Inventory.ViewModels
{
    public static class PricingCampaingModelMapper
    {

        public static List<PricingCampaignViewModel> Map(IEnumerable<DiscountPricingCampaignModel> campaignModels, IEnumerable<IInventoryData> inventoryData)
        {
            var viewModels = new List<PricingCampaignViewModel>();

            campaignModels.ToList().ForEach(model => 
            {
                var calculator = CampaignPriceCalculatorFactory.GetCalculator(model);
                viewModels.Add(new PricingCampaignViewModel
                {
                    CampaignId = model.CampaignId,
                    CampaignName = model.CampaignFilter.ToString(),
                    CreationDate = model.CreationDate.ToString("d"),
                    PriceBaseLine = model.DiscountType.ToString(),
                    RebateAmount = model.ManufacturerRebate,
                    IncentiveAmount = model.DealerDiscount,
                    ExpirationDate = model.ExpirationDate.ToString("d"),
                    CreatedBy = model.CreatedBy,
                    InventoryList = Map(model.InventoryList, inventoryData, calculator)
                });
            });

            return viewModels;
        }

        private static List<PricingCampaignInventoryViewModel> Map(IEnumerable<InventoryDiscountPricingModel> inventoryList, IEnumerable<IInventoryData> inventoryData, ICampaignPriceCalculator calculator)
        {
            var iList = new List<PricingCampaignInventoryViewModel>();
            inventoryList.ToList().ForEach(inventory =>
            {
                var id = inventoryData.FirstOrDefault(x => x.InventoryID.Equals(inventory.InventoryId));
                if (id != null)
                {
                    iList.Add( new PricingCampaignInventoryViewModel
                    {
                        NewInternetPrice = id.SpecialPrice.HasValue ? string.Format("{0:n0}",id.SpecialPrice.Value) : string.Format("{0:n0}",calculator.CalculatePrice(id)),
                        OriginalPrice = inventory.OriginalNewCarPrice.HasValue ?  string.Format("{0:n0}", inventory.OriginalNewCarPrice.Value) : string.Format("{0:n0}",id.FlListPrice),
                        StockNumber = id.StockNumber,
                        ActivatedBy = inventory.ActivatedBy,
                        DeactivatedBy = inventory.DeactivatedBy ?? "",
                        Active = inventory.Active.ToString(),
                        // FB: 27649 - add MSRP and unit cost
                        UnitCost = id.UnitCost,
                        MSRP = id.MSRP

                    });
                }
            });

            return iList;
        }
        
    }
}