﻿using System.Web.Mvc;

namespace Wanamaker.WebApp.Areas.Inventory
{
    public class InventoryAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Inventory";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
          
            context.MapRoute(
                 "ChromeStyleRoute",
                 "InventoryMVC.aspx/ChromeStyles/{vin}",
                 new { controller = "SessionlessHome", action = "ChromeStyles" }
             );

            context.MapRoute(
                 "SetChromeStyleRoute",
                 "InventoryMVC.aspx/SetChromeStyle",
                 new { controller = "Home", action = "SetChromeStyle" }
             );

             context.MapRoute(
                 "SetCustomTrim",
                 "InventoryMVC.aspx/SetCustomTrim",
                 new { controller = "Home", action = "SetCustomTrim" }
             );

             context.MapRoute(
                  "CtrPrice",
                  "InventoryMVC.aspx/CtrPrice/{businessUnitId}/{inventoryId}",
                  new { controller = "Performance", action = "ClickThroughRatePrice" }
              );

            context.MapRoute("inventory.csv", "Tabular.aspx/{dealer}/inventory.csv",
                             new
                                 {
                                     controller = "TabularData",
                                     action = "GetInventoryCsv"
                                 });

            context.MapRoute("GetSearchableYears",
                "InventorySearch.aspx/Years",
                new {controller = "NewCarPricing", action = "GetSearchableYears"});

            context.MapRoute("GetSearchableMakes",
                "InventorySearch.aspx/Makes",
                new { controller = "NewCarPricing", action = "GetSearchableMakes" });

            context.MapRoute("GetSearchableModels",
                "InventorySearch.aspx/Models",
                new { controller = "NewCarPricing", action = "GetSearchableModels" });

            context.MapRoute("GetSearchableTrims",
                "InventorySearch.aspx/Trims",
                new { controller = "NewCarPricing", action = "GetSearchableTrims" });

            context.MapRoute("GetSearchableBodys",
                "InventorySearch.aspx/BOdys",
                new { controller = "NewCarPricing", action = "GetSearchableBodys" });

            context.MapRoute("GetSearchableEngines",
                "InventorySearch.aspx/Engines",
                new { controller = "NewCarPricing", action = "GetSearchableEngines" });

            context.MapRoute("GetInventoryList",
                "InventorySearch.aspx/Filter",
                new {controller = "NewCarPricing", action = "GetSelectedVehicles"});

            context.MapRoute("SaveNewCarPricing",
                "NewCarPricing.aspx/Save",
                new { controller = "NewCarPricing", action = "SavePricing" });

            context.MapRoute("CancelCampaign",
                "NewCarPricing.aspx/Cancel",
                new { controller = "NewCarPricing", action = "CancelCampaign" });

            // The 'onlyActive' boolean parameter is optional.
            // /merchandising/NewCarPricing.aspx/ListCampaigns/1234 is a perfectly valid route.
            context.MapRoute("ListCampaigns",
                "NewCarPricing.aspx/ListCampaigns/{businessUnitId}/{onlyActive}",
                new { controller = "NewCarPricing", action = "ListCampaigns", onlyActive = UrlParameter.Optional  });
        }
    }
}
