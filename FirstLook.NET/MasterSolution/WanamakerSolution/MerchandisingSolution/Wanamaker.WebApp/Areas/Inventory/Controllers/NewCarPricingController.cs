﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Vehicles;
using Google.Apis.Util;
using log4net;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using MAX.Entities;
using Wanamaker.WebApp.AppCode;
using Wanamaker.WebApp.Areas.Dashboard.Filters;
using Wanamaker.WebApp.Areas.Inventory.ViewModels;

namespace Wanamaker.WebApp.Areas.Inventory.Controllers
{
    [HandleMvcException]
    public class NewCarPricingController : Controller
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(NewCarPricingController).FullName);
        private readonly IInventorySearch _inventorySearch;
        private readonly IDiscountPricingCampaignManager _pricingCampaignManager;

        public NewCarPricingController(IInventorySearch inventorySearch, IDiscountPricingCampaignManager pricingCampaignManager)
        {
            _inventorySearch = inventorySearch;
            _pricingCampaignManager = pricingCampaignManager;
        }

        [HttpPost]
        [CompressionFilter]
        public JsonResult GetSearchableYears(int businessUnitId)
        {
            try
            {
                _inventorySearch.Initialize(businessUnitId, VehicleType.New);

                var yearOptions = _inventorySearch.InventoryList.Select(x => x.Year.ToString(CultureInfo.InvariantCulture).Trim()).Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
                yearOptions.Sort();

                var options = new List<string>();
                options.AddRange(yearOptions);

                var optDict = options.ToDictionary(year => year);

                var data = new NewCarPricingOptions
                {
                    InventoryIds = new List<int>(),
                    Options = optDict,
                    CampaignFilter = new CampaignFilter {Year = options}
                };
                return Json(data);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat(
                    "Exception thrown for an MVC request GetSearchableYears.  BusinessUnitId : {0} , Message : {1}, StackTrace : {2}",
                    businessUnitId, ex.Message, ex.StackTrace);
                var data = new { errorMessage = ex.Message };

                return Json(data);
            }
        }

        [HttpPost]
        [CompressionFilter]
        public JsonResult GetSearchableMakes(int businessUnitId, CampaignFilter filter)
        {
            try
            {
                _inventorySearch.Initialize(businessUnitId, filter.VehicleType);

                var makes = _inventorySearch.ByCampaignFilter(filter).ToList();
                var options = makes.Select(x => x.Make.Trim()).Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
                options.Sort();

                filter.Make = options;

                var data = new NewCarPricingOptions
                {
                    InventoryIds = makes.Select(x => x.InventoryID).ToList(),
                    Options = options.ToDictionary(make => make),
                    SelectedValue = GetDefaultMake(options),
                    CampaignFilter = filter
                };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat(
                    "Exception thrown for an MVC request GetSearchableYears.  BusinessUnitId : {0} , CampaignFilter : {1} , Message : {2}, StackTrace : {3}",
                    businessUnitId, filter, ex.Message, ex.StackTrace);
                var data = new { errorMessage = ex.Message };

                return Json(data);
            }
        }

        [HttpPost]
        [CompressionFilter]
        public JsonResult GetSearchableModels(int businessUnitId, CampaignFilter filter)
        {
            try
            {
                _inventorySearch.Initialize(businessUnitId, filter.VehicleType);
                
                var models = _inventorySearch.ByCampaignFilter(filter).ToList();

                var options = models.Where(x => x.ChromeStyleId.HasValue && x.ChromeStyleId != -1 && x.Model.IsNotNullOrEmpty()).Select(x => x.Model.Trim()).Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
                options.Sort();
                filter.Model = options;

                var data = new NewCarPricingOptions
                {
                    InventoryIds = models.Select(x => x.InventoryID).ToList(),
                    Options = options.ToDictionary(model => model),
                    SelectedValue = options.FirstOrDefault(),
                    CampaignFilter = filter
                };

                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat(
                    "Exception thrown for an MVC request GetSearchableModels.   BusinessUnitId : {0} , CampaignFilter : {1} , Message : {2}, StackTrace : {3}",
                    businessUnitId, filter, ex.Message, ex.StackTrace);
                var data = new { errorMessage = ex.Message };

                return Json(data);
            }
        }

        [HttpPost]
        [CompressionFilter]
        public JsonResult GetSearchableTrims(int businessUnitId, CampaignFilter filter)
        {
            try
            {
                _inventorySearch.Initialize(businessUnitId, filter.VehicleType);

                var cars = _inventorySearch.ByCampaignFilter(filter).ToList();
                
                // Get all the trims that have a value
                var styles = cars.Select(
                    x => (!String.IsNullOrEmpty(x.AfterMarketTrim)) ? x.AfterMarketTrim.FirstCharacterToUpper().Trim() : x.Trim.FirstCharacterToUpper().Trim())
                        .Distinct(StringComparer.InvariantCultureIgnoreCase)
                        .Where(x => !String.IsNullOrWhiteSpace(x.Trim()))
                        .ToList();
                
                // For the trims that don't have a value, try to get the chromestyle trim
                cars.Where(x => String.IsNullOrWhiteSpace(x.Trim)).ToList().ForEach(x => {
                    var chromeStyles = ChromeStyle.ChromeTrimStylesSelect(x.VIN);
                    // if the car has a chrome style id set, only add that one; else add them all
                    int styleId = x.ChromeStyleId ?? -1;
                    if (chromeStyles.ContainsKey(styleId))
                    {
                        styles.Add(chromeStyles[styleId].Trim.FirstCharacterToUpper().Trim());
                    }
                    else
                    {
                        var trims = chromeStyles.Select(y => y.Value.Trim.FirstCharacterToUpper().Trim()).ToArray();
                        styles.AddRange(trims);
                    }
                });

                var options = styles.Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
                options.Sort();

                filter.Trim = options;

                var data = new NewCarPricingOptions
                {
                    InventoryIds = cars.Select(x => x.InventoryID).ToList(),
                    Options = options.ToDictionary(trim => trim),
                    SelectedValue = options.FirstOrDefault(),
                    CampaignFilter = filter
                };

                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat(
                    "Exception thrown for an MVC request GetSearchableTrims.  BusinessUnitId : {0} , CampaignFilter : {1} , Message : {2}, StackTrace : {3}",
                    businessUnitId, filter, ex.Message, ex.StackTrace);
                var data = new { errorMessage = ex.Message };

                return Json(data);
            }
        }

        [HttpPost]
        [CompressionFilter]
        public JsonResult GetSearchableBodys(int businessUnitId, CampaignFilter filter)
        {
            try
            {
                _inventorySearch.Initialize(businessUnitId, filter.VehicleType);
                
                var bodys = _inventorySearch.ByCampaignFilter(filter).ToList();
                var inventoryIds = bodys.Select(x => x.InventoryID).ToList();
                
                //BUGZID: 27992 - bodystyle and engine options need to only use the intersection.
                // group by body style
                // foreach body style, get a list of available engines.
                // keep only the engines that are in all the groups.
                if (filter.CampaignEngineDescription.Any() && bodys.Count > 1)
                {
                    var eDesc = filter.CampaignEngineDescription.ToArray();

                    var sharedBodies = new List<string>();
                    for (var engCount = 0; engCount < eDesc.Length; engCount++)
                    {
                        var currentEngineDesc = eDesc[engCount];
                        var engDescriptions = bodys
                            .Where(x => currentEngineDesc.IsStandard
                                ? (x.EngineDescription??"").Contains(currentEngineDesc.Description)
                                : (x.EngineOptionCode??"").Equals(currentEngineDesc.OptionCode))
                            .Select(x => x.BodyStyleName)
                            .Distinct(StringComparer.InvariantCultureIgnoreCase)
                            .ToList();
                        if (engCount == 0)
                        {
                            sharedBodies.AddRange(engDescriptions.Select(x => x));
                        }
                        else
                        {
                            sharedBodies = engDescriptions.Intersect(sharedBodies).ToList();
                        }
                    }

                    bodys = bodys.Where(id => sharedBodies.Contains(id.BodyStyleName, StringComparer.InvariantCultureIgnoreCase)).Select(x => x).ToList();
                }

                //BUGZID: 27992 - bodystyle and engine options need to only use the intersection.
                var options = bodys
                    .Where(x => x.BodyStyleName.IsNotNullOrEmpty())
                    .Select(x => x.BodyStyleName.Trim())
                    .Distinct(StringComparer.InvariantCultureIgnoreCase)
                    .ToList();

                options.Sort();
                
                filter.BodyStyle = options;

                var data = new NewCarPricingOptions
                {
                    InventoryIds = inventoryIds,
                    Options = options.ToDictionary(body => body),
                    SelectedValue = options.FirstOrDefault(),
                    CampaignFilter = filter
                };

                return Json(data);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat(
                    "Exception thrown for an MVC request GetSearchableBodys.   BusinessUnitId : {0} , CampaignFilter : {1} , Message : {2}, StackTrace : {3}",
                    businessUnitId, filter, ex.Message, ex.StackTrace);
                var data = new { errorMessage = ex.Message };

                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [CompressionFilter]
        public JsonResult GetSearchableEngines(int businessUnitId, CampaignFilter filter)
        {
            try
            {
                _inventorySearch.Initialize(businessUnitId, filter.VehicleType);

                var engines = _inventorySearch.ByCampaignFilter(filter).ToList();
                var inventoryIds = engines.Select(x => x.InventoryID).ToList();
                //BUGZID: 27992 - bodystyle and engine options need to only use the intersection.
                // group by body style
                // foreach body style, get a list of available engines.
                // keep only the engines that are in all the groups.
                if (filter.BodyStyle.Any() && engines.Count > 1)
                {
                    var bStyles = filter.BodyStyle.ToArray();

                    var sharedEngines = new List<string>();
                    for(var bsCount = 0; bsCount < bStyles.Length; bsCount++)
                    {
                        var currentBodyStyle = bStyles[bsCount];
                        var bsEngines = engines.Where(x => x.BodyStyleName.Contains(currentBodyStyle)).Select(x => x.EngineDescription).Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
                        if (bsCount == 0)
                        {
                            sharedEngines.AddRange(bsEngines.Select(x => x));
                        }
                        else
                        {
                            sharedEngines = bsEngines.Intersect(sharedEngines).ToList();
                        }
                    }

                    engines = engines.Where(id => sharedEngines.Contains(id.EngineDescription, StringComparer.InvariantCultureIgnoreCase))
                        .Select(x => x)
                        .ToList();
                }

                var enginedescriptions = engines
                    .Where(x => x.EngineDescription.IsNotNullOrEmpty())
                    .Select(x => x.EngineDescription.Trim())
                    .Distinct(StringComparer.CurrentCultureIgnoreCase)
                    .ToList();

                enginedescriptions.Sort();
                filter.BodyStyle = enginedescriptions;

                var options_list = engines
                    .Where(x => x.EngineDescription.IsNotNullOrEmpty())
                    .Select(x => new { Key = (x.IsStandardEngine) ? "maxchromestandard" : x.EngineOptionCode.Trim(), Value = x.EngineDescription.Trim() })
                    .ToList();
                var options = new Dictionary<string,string>();
                foreach( var item in options_list )
                    if( !options.ContainsKey( item.Key ))
                        options.Add( item.Key, item.Value );

                var data = new NewCarPricingOptions
                {
                    InventoryIds = inventoryIds,
                    Options = options,
                    SelectedValue = enginedescriptions.FirstOrDefault(),
                    CampaignFilter = filter
                };

                return Json(data, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                Log.ErrorFormat(
                    "Exception thrown for an MVC request GetSearchableBodys.   BusinessUnitId : {0} , CampaignFilter : {1} , Message : {2}, StackTrace : {3}",
                    businessUnitId, filter, ex.Message, ex.StackTrace);
                var data = new { errorMessage = ex.Message };

                return Json(data);
            }
        }

        [HttpPost]
        [CompressionFilter]
        public JsonResult GetSelectedVehicles(int businessUnitId, CampaignFilter filter)
        {
            try
            {
                _inventorySearch.Initialize(businessUnitId, filter.VehicleType);

                var trims = _inventorySearch.ByCampaignFilter(filter).ToList();

                var data = new NewCarPricingOptions
                {
                    InventoryIds = trims.Select(x => x.InventoryID).ToList(),
                    CampaignFilter = filter
                };

                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat(
                    "Exception thrown for an MVC request GetSelectedVehicles.  BusinessUnitId : {0} , CampaignFilter : {1} , Message : {2}, StackTrace : {3}",
                    businessUnitId, filter, ex.Message, ex.StackTrace);
                var data = new { errorMessage = ex.Message };

                return Json(data);
            }
        }

        [HttpPost]
        public JsonResult CancelCampaign(int businessunitId, int campaignId, string canceledByUser = null)
        {
            try
            {
                if (canceledByUser.IsNullOrEmpty() && System.Web.HttpContext.Current != null)
                    canceledByUser = System.Web.HttpContext.Current.User.Identity.Name;

                if(canceledByUser.IsNullOrEmpty())
                    throw new Exception("You must provide a user to cancel a campaign");

                var model = _pricingCampaignManager.FetchCampaign(businessunitId, campaignId);
                _pricingCampaignManager.ExpireDiscountCampaign(model, canceledByUser);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat(
                    "Exception thrown for an MVC request Cancel Campaign.  BusinessUnitId : {0} , CampaignId : {1} , Message : {2}, StackTrace : {3}",
                    businessunitId, campaignId.ToJson(), ex.Message, ex.StackTrace);

                return Json(new { errorMessage = ex.Message });
            }

            return Json(new { result = "success" });
        }

        [HttpPost]
        [CompressionFilter]
        public JsonResult SavePricing(int businessUnitId, NewCarPricingModel priceInfo, string createdByUser = null)
        {
            try
            {
                // validate model before continuing.
                string[] errorMessages;
                if (!NewCarPricingModel.Validate(priceInfo, out errorMessages))
                {
                    return Json(new { result = "fail", errorMessage = errorMessages });
                }
                // perform post-processing on model to reduce edge cases
                NewCarPricingModel.PostProcess(priceInfo);

                if (createdByUser.IsNullOrEmpty() && System.Web.HttpContext.Current != null)
                    createdByUser = System.Web.HttpContext.Current.User.Identity.Name;

                var dcp = DiscountPricingModelMapper.MapNewCarPricing(businessUnitId, priceInfo, VehicleType.New, createdByUser);
                dcp = _pricingCampaignManager.SaveCampaign(dcp, priceInfo.InventoryIds);

                if (dcp != null && dcp.CampaignId > 0)
                    return Json(new {result = "success"});

                throw new Exception("unknown failure");
            }
            catch (Exception ex)
            {
                Log.ErrorFormat(
                    "Exception thrown for an MVC request SavePricing.  BusinessUnitId : {0} , PriceInfo : {1} , Message : {2}, StackTrace : {3}",
                    businessUnitId, priceInfo.ToJson(), ex.Message, ex.StackTrace);
                var data = new { errorMessage = ex.Message };

                return Json(data);
            }
        }


        [HttpGet]
        [CompressionFilter]
        public JsonResult ListCampaigns(int businessUnitId, bool onlyActive = true)
        {
            try
            {
                var campaigns = _pricingCampaignManager.FetchAllCampaigns(businessUnitId, onlyActive);

                _inventorySearch.Initialize(businessUnitId, VehicleType.Both);

                var json = PricingCampaingModelMapper.Map(campaigns, _inventorySearch.InventoryList);

                return Json(json, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat(
                    "Exception thrown for an MVC request GetCampaigns.  BusinessUnitId : {0} , OnlyActive : {1} , Message : {2}, StackTrace : {3}",
                    businessUnitId, onlyActive, ex.Message, ex.StackTrace);
                var data = new { errorMessage = ex.Message };

                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }

        #region Private Methods
        private static string GetDefaultMake(List<string> makes)
        {
            // Figure out what the default make is for this dealer.
            if (makes.Count == 1)
                return makes.First();

            if (makes.Count <= 1) return String.Empty;

            makes.Sort();
            return makes.First();
        }

        #endregion Private Methods
    }
}
