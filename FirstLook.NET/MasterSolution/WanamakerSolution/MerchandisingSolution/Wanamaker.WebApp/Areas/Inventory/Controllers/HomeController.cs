﻿
using System;
using System.Linq;
using System.Web.Mvc;
using FirstLook.LotIntegration.DomainModel.Tasks;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using FirstLook.Merchandising.DomainModel.Workflow;
using log4net;
using MAX.Entities;
using MAX.Entities.Filters;
using Merchandising.Messages;

namespace Wanamaker.WebApp.Areas.Inventory.Controllers
{
    public class HomeController : Controller
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(HomeController).FullName);

        private IQueueFactory QueueFactory { get; set; }

        private IWorkflowRepository WorkflowRepository { get; set; }
        private IAdMessageSender _messageSender;

        public HomeController(IWorkflowRepository workflowRepository, IQueueFactory queueFactory, IAdMessageSender messageSender)
        {
            WorkflowRepository = workflowRepository;
            QueueFactory = queueFactory;
            _messageSender = messageSender;
        }

       
        [HttpPost]
        public JsonResult SetCustomTrim(int businessUnitId, int inventoryId, string customTrim)
        {
            try
            {
                var vc = VehicleConfiguration.FetchOrCreateDetailed(businessUnitId, inventoryId, User.Identity.Name);
                vc.AfterMarketTrim = customTrim;
                vc.Save(businessUnitId, User.Identity.Name);
                _messageSender.SendStyleSet(businessUnitId, inventoryId, User.Identity.Name, GetType().FullName);
                return Json(new {success = true});
            }
            catch (Exception)
            {
                return Json(new {success = false});
            }
  
        }

        [HttpPost]
        public JsonResult SetChromeStyle(int businessUnitId, int inventoryId, int chromeStyleId, int usedNewFilter)
        {
            try
            {
                var vc = VehicleConfiguration.FetchOrCreateDetailed(businessUnitId, inventoryId, User.Identity.Name);

                var inventoryType = (UsedOrNewFilter) usedNewFilter;

                //Update DB
                vc.UpdateStyleId(businessUnitId, chromeStyleId);


                //We just update the inventory in the session so we don't have to get it from the DB again.
                var cachedData = InventoryDataFilter
                    .FetchListFromSession(businessUnitId).Where(inv => inv.InventoryID == inventoryId).SingleOrDefault();

                if (cachedData != null)
                {
                    cachedData.ChromeStyleId = chromeStyleId;
                }

                _messageSender.SendStyleSet(businessUnitId, inventoryId, User.Identity.Name, GetType().FullName);
                
                //Return the new No Trim bucket count as only that should have changed.
                var counts = WorkflowRepository.CalculateCounts(
                InventoryDataFilter
                    .FetchListFromSession(businessUnitId)
                    .FilterBy(inventoryType));
               return  Json(counts.GetCount(WorkflowType.TrimNeeded));
            }
            catch (Exception ex)
            {
                Log.ErrorFormat(
                    "Exception thrown for an MVC SetChromeStyle.  businessUnitId : {0} , inventoryId : {1},  chromeStyleId : {2} , Message : {3}, StackTrace : {4}",
                    businessUnitId, inventoryId, chromeStyleId, ex.Message, ex.StackTrace);

                var data = new { errorMessage = ex.Message };

                return Json(data, JsonRequestBehavior.AllowGet);
            }

        }

    }
}
