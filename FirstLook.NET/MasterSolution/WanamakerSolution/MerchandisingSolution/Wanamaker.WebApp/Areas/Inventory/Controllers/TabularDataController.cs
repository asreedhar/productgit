﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.SessionState;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using FirstLook.Merchandising.DomainModel.Core.Authorization.Mvc;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Core.IO;
using FirstLook.Merchandising.DomainModel.Workflow;
using MAX.Entities;
using Wanamaker.WebApp.Areas.Inventory.ViewModels;

namespace Wanamaker.WebApp.Areas.Inventory.Controllers
{
    [SessionState(SessionStateBehavior.Disabled)]
    public class TabularDataController : Controller
    {
        private readonly IInventoryDataRepository _inventoryRepository;
        private readonly IDealerServices _dealerServices;
        private readonly Func<IInventoryQueryBuilder> _queryBuilderFactory;

        public TabularDataController(IInventoryDataRepository inventoryRepository, 
            IDealerServices dealerServices, Func<IInventoryQueryBuilder> queryBuilderFactory)
        {
            _inventoryRepository = inventoryRepository;
            _dealerServices = dealerServices;
            _queryBuilderFactory = queryBuilderFactory;
        }

        [HttpGet]
        [AuthorizeUser]
        [AuthorizeDealer(AssertDealerUpgrades = "Merchandising")]
        public CsvResult<InventoryAdapter> GetInventoryCsv(GetInventoryParameters parameters)
        {
            var dealer = _dealerServices.GetDealerByCode(parameters.dealer);

            var query = _queryBuilderFactory();
            parameters.ApplyToQuery(query);

            var data = _inventoryRepository
                .GetAllInventory(dealer.Id)
                .WorkflowFilter(query)
                .Select(inv => new InventoryAdapter(inv));
            
            return new CsvResult<InventoryAdapter>(data);
        }

        public class GetInventoryParameters
        {
            public string dealer { get; set; }
            public WorkflowType? filter { get; set; }
            public int? newused { get; set; }
            public string search { get; set; }
            public int? agebucket { get; set; }

            public void ApplyToQuery(IInventoryQueryBuilder query)
            {
                if(filter != null)
                    query.FilterByWorkflow(filter.Value);
                if(newused != null)
                    query.FilterByNewOrUsed(newused.Value);
                if(agebucket != null)
                    query.FilterByAgeBucket(agebucket.Value);
                if(search != null)
                    query.FilterByKeywords(search);
            }
        }
    }
}
