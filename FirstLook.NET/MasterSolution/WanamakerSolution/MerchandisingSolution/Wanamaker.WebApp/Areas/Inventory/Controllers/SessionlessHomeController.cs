﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.SessionState;
using FirstLook.Merchandising.DomainModel.Vehicles;
using log4net;
using Wanamaker.WebApp.Areas.Inventory.ViewModels;

namespace Wanamaker.WebApp.Areas.Inventory.Controllers
{
    //This controller is to allow multiple requests (in an ajax scenario) to be processed concurrently rather than
    //being processed sequentially
    [SessionState(SessionStateBehavior.Disabled)]
    public class SessionlessHomeController : Controller
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(HomeController).FullName);

        [HttpGet]
        public JsonResult ChromeStyles(string vin)
        {
            try
            {
                var model = ChromeStyle.ChromeTrimStylesSelect(vin);
                var list =
                    model.Keys.Select(
                        chromeStyleId =>
                        new ChromeStyleVm { ChromeStyleId = chromeStyleId, ChromeStyleName = model[chromeStyleId].Trim + " - " + model[chromeStyleId].StyleNameWoTrim, Trim = model[chromeStyleId].Trim }).
                        ToList();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat(
                    "Exception thrown for an MVC request Area Inventory, Home Controller , action ChromeStyles.  Vin : {0} ,  Message : {1}, StackTrace : {2}",
                    vin, ex.Message, ex.StackTrace);

                var data = new { errorMessage = ex.Message };

                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }
    }
}