﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FirstLook.Merchandising.DomainModel.Pricing;
using Wanamaker.WebApp.Areas.Inventory.ViewModels;

namespace Wanamaker.WebApp.Areas.Inventory.Controllers
{
    public class DiscountPricingModelMapper
    {
        public static DiscountPricingCampaignModel MapNewCarPricing(int businessUnitId, NewCarPricingModel priceInfo)
        {
            var dpcm = new DiscountPricingCampaignModel
            {
                BusinessUnitId = businessUnitId,
                CampaignFilter =
                    new CampaignFilter
                    {
                        Make = priceInfo.Make,
                        Model = priceInfo.Model,
                        Trim = priceInfo.Trim,
                        Year = priceInfo.Year
                    },
                CreatedBy = HttpContext.Current.User.Identity.Name,
                DiscountType = (int) priceInfo.PriceBaseLine,
                DiscountValue = priceInfo.RebateAmount,
                ExpirationDate = DateTime.Parse(priceInfo.ExpirationDate)
            };

            return dpcm;
        }

        public static IEnumerable<InventoryDiscountPricingModel> MapCampaignInventory(int campaignId, List<int> inventoryIds)
        {
            var inventoryList =
                inventoryIds.Select(
                    x =>
                        new InventoryDiscountPricingModel
                        {
                            Active = true,
                            CampaignId = campaignId,
                            InventoryId = x
                        });

            return inventoryList;
        }
    }
}