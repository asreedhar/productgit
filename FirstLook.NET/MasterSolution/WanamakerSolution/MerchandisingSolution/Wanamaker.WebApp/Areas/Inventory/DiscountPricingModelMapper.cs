﻿using System;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;
using MAX.Entities;
using Wanamaker.WebApp.Areas.Inventory.ViewModels;

namespace Wanamaker.WebApp.Areas.Inventory
{
    public class DiscountPricingModelMapper
    {
        public static DiscountPricingCampaignModel MapNewCarPricing(int businessUnitId, NewCarPricingModel priceInfo, VehicleType vehicleType, string createdByUser)
        {
            var dpcm = new DiscountPricingCampaignModel
            {
                BusinessUnitId = businessUnitId,
                CampaignFilter = new CampaignFilter
                {
                    VehicleType = vehicleType,
                    Make = priceInfo.Make,
                    Model = priceInfo.Model,
                    Trim = priceInfo.Trim,
                    Year = priceInfo.Year,
                    BodyStyle = priceInfo.BodyStyle,
                    CampaignEngineDescription = priceInfo.EngineDescription
                },
                CreatedBy = createdByUser,
                DiscountType = priceInfo.PriceBaseLine,
                DealerDiscount = priceInfo.DiscountAmount,
                ManufacturerRebate = priceInfo.RebateAmount,
                ExpirationDate = DateTime.Parse(priceInfo.ExpirationDate)
            };

            return dpcm;
        }
    }
}