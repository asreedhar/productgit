﻿using System.Web.Mvc;

namespace Wanamaker.WebApp.Areas.WanamakerGet
{
    public class WanamakerGetAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "WanamakerGet";
            }
        }

        public override void RegisterArea( AreaRegistrationContext context )
        {
            context.MapRoute(
                "WanamakerGet_default",
                "DigitalPerformanceAnalysis.aspx",
                new { controller = "WanamakerGet", action = "WanamakerGet" }
            );

            context.MapRoute(
                "WanamakerGet_API_Load",
                "DigitalPerformanceAnalysis.aspx/Load",
                new { controller = "WanamakerGet", action = "Load" }
            );

            context.MapRoute(
                "WanamakerGet_API_Save",
                "DigitalPerformanceAnalysis.aspx/Save",
                new { controller = "WanamakerGet", action = "Save" }
            );
        }
    }
}
