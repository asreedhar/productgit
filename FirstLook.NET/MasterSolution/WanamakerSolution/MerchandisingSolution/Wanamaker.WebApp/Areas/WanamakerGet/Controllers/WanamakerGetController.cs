﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.DataSource;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics;
using FirstLook.Merchandising.DomainModel.MaxAnalytics;
using FirstLook.Merchandising.DomainModel.Postings;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using MAX.Entities.Enumerations;
using MAX.Entities.Helpers.DashboardHelpers;
using Wanamaker.WebApp.Areas.WanamakerGet.Models;
// type aliases
using SiteBudgetCollection = System.Collections.Generic.Dictionary<int, System.Collections.Generic.List<FirstLook.Merchandising.DomainModel.Reports.SitePerformance.SiteBudget>>;

namespace Wanamaker.WebApp.Areas.WanamakerGet.Controllers
{
    public class WanamakerGetController : Controller
    {
        public static readonly int ASSUMED_TIMESPAN_MONTHS = 13; // backwards, from now

        #region Autofac

        protected IWebContext WebContext;
        protected ISiteBudgetRepository SiteBudgetRepository;
        protected IGoogleAnalyticsWebAuthorization GoogleAnalyticsWebAuthorization;
        protected IGoogleAnalyticsQuery GoogleAnalyticsQuery;
        protected IGoogleAnalyticsRepository GoogleAnalyticsRepository;
        protected IWebsiteProviderMetricsModel WebsiteProviderMetricsModel;
        protected IDealerListingSiteCredentialRepository DealerListingSiteCredentialRepository;
        protected IDealershipSegmentModel DealershipSegmentModel;

        public WanamakerGetController(
            IWebContext WebContext,
            ISiteBudgetRepository SiteBudgetRepository,
            IGoogleAnalyticsWebAuthorization GoogleAnalyticsWebAuthorization,
            IGoogleAnalyticsQuery GoogleAnalyticsQuery,
            IGoogleAnalyticsRepository GoogleAnalyticsRepository,
            IWebsiteProviderMetricsModel WebsiteProviderMetricsModel,
            IDealerListingSiteCredentialRepository DealerListingSiteCredentialRepository,
            IDealershipSegmentModel DealershipSegmentModel )
        {
            this.WebContext = WebContext;
            this.SiteBudgetRepository = SiteBudgetRepository;
            this.GoogleAnalyticsWebAuthorization = GoogleAnalyticsWebAuthorization;
            this.GoogleAnalyticsQuery = GoogleAnalyticsQuery;
            this.GoogleAnalyticsRepository = GoogleAnalyticsRepository;
            this.WebsiteProviderMetricsModel = WebsiteProviderMetricsModel;
            this.DealerListingSiteCredentialRepository = DealerListingSiteCredentialRepository;
            this.DealershipSegmentModel = DealershipSegmentModel;
        }

        #endregion

        /////////

        public ActionResult WanamakerGet()
        {
            // for this implementation, our API Token is simply a business unit ID
            var bu = WebContext.GetBusinessUnit();
            VerifyBusinessUnit( bu );
            
            ViewBag.title = "Digital Performance Analysis | MAX : Intelligent Online Advertising Systems";
            ViewBag.api_token = bu.Id;
            ViewBag.context_name = bu.Name;
            ViewBag.IsOemBranded = GetMiscSettings.GetSettings(bu.Id).Franchise != Franchise.None;

            return View();
        }
        
        public string Load( string api_token )
        {
            var bu = WebContext.GetBusinessUnit();
            VerifyBusinessUnit( bu, api_token );

            var data = new WanamakerDataCollectionFormModel();

            // only the following explicit dealer listing sites are supported in the UI; hardcode them
            var supported_dealer_listing_sites = new List<DealerListingSite> {
                new DealerListingSite { site_id = 2, site_name = "AutoTrader" },
                new DealerListingSite { site_id = 1, site_name = "Cars.com" }
            };
            
            // get site budgets
            var budgets = SiteBudgetRepository.GetAllBudgetsForBusinessUnit( bu.Id );
            if( budgets != null )
            {
                foreach( var site in supported_dealer_listing_sites )
                {
                    if( budgets.ContainsKey( site.site_id )) 
                    {
                        data.dealer_listing_sites.Add( site );
                        foreach( var budget in budgets[ site.site_id ])
                        {
                            if( budget.Budget != null )
                                site.monthly_cost.Add( new LinkedBudget {
                                    month = budget.MonthApplicable,
                                    value = (int) budget.Budget,
                                    soft = false
                                });
                        }
                    }
                }
            }

            // get credentials + dealer ids
            var credentials = DealerListingSiteCredentialRepository.Load( bu.Id );
            if( credentials != null )
            {
                foreach( var site in supported_dealer_listing_sites )
                {
                    var cred = credentials.Find( c => c.DestinationId == site.site_id );
                    if( cred == null )
                        continue;

                    if( ! data.dealer_listing_sites.Contains( site ))
                        data.dealer_listing_sites.Add( site );

                    site.username = cred.Username;
                    site.password = cred.Password;
                    site.dealer_id = cred.ExternalID;
                }
            }

            // get google analytics profiles so they can select one
            try
            {
                if( GoogleAnalyticsWebAuthorization.IsAuthorized( bu.Id )) // check active
                {
                    GoogleAnalyticsQuery.Init( bu.Id );
                    var profiles = GoogleAnalyticsQuery.GetProfiles();
                    if( profiles != null && profiles.Length > 0 )
                    {
                        data.google_analytics = new GoogleAnalyticsProfileWrapper();
                        // determine the selected profile
                        var selectedProfile = GoogleAnalyticsRepository.FetchProfile( bu.Id, GoogleAnalyticsType.PC );
                        if( selectedProfile != null )
                        {
                            data.google_analytics.selected_profile_id = selectedProfile.ProfileId;
                            data.google_analytics.selected_profile_name = selectedProfile.ProfileName;
                            data.google_analytics.associated_provider_id = (int)selectedProfile.SiteProvider;
                        }
                        // determine whether the user should be shown all of the possible profiles, or just the selected one
                        // to be shown all profiles, one must be either an admin, or have no refresh tokens set and not be locked
                        bool is_admin = Helper.IsAdministrator;
                        bool admin_lock = GoogleAnalyticsRepository.Read_AdminLock( bu.Id );
                        data.google_analytics.is_admin = is_admin;
                        data.google_analytics.admin_lock = admin_lock;
                        if( is_admin || !admin_lock )
                            data.google_analytics.google_analytics_profiles.AddRange( profiles );
                        else
                            data.google_analytics.google_analytics_profiles.Add( new ProfileResult( selectedProfile.ProfileId, selectedProfile.ProfileName ));
                    
                        var google_providers = WebsiteProviderMetricsModel.GetGoogleVdpSites();
                        if( google_providers != null )
                            foreach( var p in google_providers )
                            {
                                data.google_analytics.compatible_providers.Add(
                                    new Provider { id = (int)p, name = EnumHelper.GetEnumDescription(typeof(EdtDestinations), p) });
                            }
                        data.google_analytics.compatible_providers.Add(
                            new Provider { id = 0, name = "Other" });
                    }
                }
            }
            catch( AccessViolationException e )
            {
                GoogleAnalyticsRepository.SetRefreshTokenInactive( bu.Id );
                data.google_analytics = null;
            }
            catch( InvalidOperationException e )
            {
                data.google_analytics = null;
            }
            
            // get dealer website vdps, providers and credentials
            data.dealer_vdps = new DealerVehicleDetailPage();
            int p_id = WebsiteProviderMetricsModel.GetSelectedProviderID( bu.Id ) ?? 0;
            data.dealer_vdps.dealer_website_provider_id = p_id;
            var now = DateTime.Now;
            var monthRangeEnd = new DateTime( now.Year, now.Month, 1, 0, 0, 0, 0, DateTimeKind.Utc );
            var monthRangeBegin = monthRangeEnd.AddMonths( -ASSUMED_TIMESPAN_MONTHS );
            var vdps = WebsiteProviderMetricsModel.GetVDPs( bu.Id, monthRangeBegin, monthRangeEnd );
            if( vdps != null && vdps.Count > 0 )
            {
                foreach( var row in vdps )
                {
                    var vdp_row = new DatedVDP {
                        month = row.MonthApplicable,
                        new_value = row.NewVDPs,
                        used_value = row.UsedVDPs
                    };
                    var vdp_collection = data.dealer_vdps.vdp_data.Find( v => v.website_provider_id == row.WebsiteProviderID );
                    if( vdp_collection == null )
                    {
                        vdp_collection = new DealerVDPData {
                            website_provider_id = row.WebsiteProviderID
                        };
                        data.dealer_vdps.vdp_data.Add( vdp_collection );
                    }
                    vdp_collection.vdps.Add( vdp_row );
                }
            }
            List<int> additional_ids = null;
            if( p_id < 0 ) // freeform
                additional_ids = new List<int>{ p_id };
            var providers = WebsiteProviderMetricsModel.GetProviders( additional_ids );
            if( providers != null && providers.Count > 0 )
            {
                data.dealer_vdps.dealer_website_providers = new List<Provider>();
                foreach( var p in providers )
                {
                    data.dealer_vdps.dealer_website_providers.Add( new Provider {
                        id = p.WebsiteProviderID,
                        name = p.Name
                    });
                    // even if the freeform website provider was entered previously, it will be treated as a first-class citizen of the system
                    // for this dealer only, and will appear in the dropdown after the curated providers.
                }
            }
            if( credentials != null && credentials.Count > 0
            &&  providers != null && providers.Count > 0 )
            {
                foreach( var provider in providers )
                {
                    var dw_cred = credentials.Find( c => c.DestinationId == provider.WebsiteProviderID );
                    if( dw_cred != null )
                    {
                        var vdp_collection = data.dealer_vdps.vdp_data.Find( v => v.website_provider_id == provider.WebsiteProviderID );
                        if( vdp_collection == null )
                        {
                            vdp_collection = new DealerVDPData {
                                website_provider_id = provider.WebsiteProviderID
                            };
                            data.dealer_vdps.vdp_data.Add( vdp_collection );
                        }
                        vdp_collection.username = dw_cred.Username;
                        vdp_collection.password = dw_cred.Password;
                    }
                }
            }
            
            // get segment
            var segment = DealershipSegmentModel.get( bu.Id );
            if( segment != DealershipSegment.Undefined )
            {
                data.dealership_segment = new DealershipSegmentWrapper {
                    segment = segment
                };
            }

            var data_str = data.toJSON();
            return data_str;
        }

        public void Save( string api_token, string data )
        {
            var bu = WebContext.GetBusinessUnit();
            if( int.Parse( api_token ) != bu.Id )
                throw new Exception("Logged-in user's business unit ID does not match given API token; for this implementation, it must.");

            var data_obj = WanamakerDataCollectionFormModel.fromJSON( data );

            var now = DateTime.Now;
            var monthRangeEnd = new DateTime( now.Year, now.Month, 1, 0, 0, 0, 0, DateTimeKind.Utc );
            var monthRangeBegin = monthRangeEnd.AddMonths( -ASSUMED_TIMESPAN_MONTHS );

            if( data_obj.dealer_listing_sites != null && data_obj.dealer_listing_sites.Count > 0 )
            {
                // save site budgets
                var site_budgets = new SiteBudgetCollection();
                foreach( var site in data_obj.dealer_listing_sites )
                {
                    var budget_list = new List<SiteBudget>();
                    foreach( var budget in site.monthly_cost )
                    {
                        if( budget.soft )
                            continue;
                        //
                        budget_list.Add( new SiteBudget {
                            BusinessUnitId = bu.Id,
                            DestinationId = site.site_id,
                            Budget = budget.value,
                            MonthApplicable = budget.month,
                            DescriptionText = null
                        });
                    }
                    if( budget_list.Count > 0 )
                        site_budgets.Add( site.site_id, budget_list );
                }
                SiteBudgetRepository.ReplaceBudgetsForBusinessUnitId( bu.Id, site_budgets, monthRangeBegin, monthRangeEnd );

                // save credentials + dealer ids
                foreach( var site in data_obj.dealer_listing_sites )
                {
                    if( site.username == null )
                        site.username = "";
                    if( site.password == null )
                        site.password = "";
                    DealerListingSiteCredentialRepository.Save( new DealerListingSiteCredential {
                        BusinessUnitId = bu.Id,
                        DestinationId = site.site_id,
                        Username = site.username,
                        Password = site.password,
                        ExternalID = site.dealer_id
                    });
                }
            }
            if( data_obj.google_analytics != null )
            {
                GoogleAnalyticsRepository.CreateUpdateProfileAccount(
                    new GoogleAnalyticsProfile( 
                        bu.Id, GoogleAnalyticsType.PC, 
                        data_obj.google_analytics.selected_profile_id, 
                        data_obj.google_analytics.selected_profile_name,
                        data_obj.google_analytics.associated_provider_id ));
                if( Helper.IsAdministrator )
                    GoogleAnalyticsRepository.Write_AdminLock( bu.Id, data_obj.google_analytics.admin_lock );
            }
            if( data_obj.dealer_vdps != null )
            {
                // save dealer website selection, vdp values and credentials
                // save selection
                var provider_id = data_obj.dealer_vdps.dealer_website_provider_id;
                if( data_obj.dealer_vdps.dealer_website_provider_id == -1 )
                {
                    if( data_obj.dealer_vdps.dealer_website_provider_freeform_name != null )
                        provider_id = WebsiteProviderMetricsModel.FindOrCreateFreeformProvider( data_obj.dealer_vdps.dealer_website_provider_freeform_name );
                    else
                        throw new WanamakerGetException( "Selected website provider was 'Other', but no freeform input name was given." );
                }
                WebsiteProviderMetricsModel.SetSelectedProviderID( bu.Id, provider_id );
                if( data_obj.dealer_vdps.vdp_data != null && data_obj.dealer_vdps.vdp_data.Count > 0 )
                {
                    foreach( var vdp_collection in data_obj.dealer_vdps.vdp_data )
                    {
                        if( vdp_collection.website_provider_id == -1 )
                        {
                            if( provider_id < 0 )
                                vdp_collection.website_provider_id = provider_id; // should have been previously set
                            else
                                continue; // no provider id found or created? (should not happen); should this be an exception?
                        }

                        // save vdps
                        var values = new List<MonthlyWebsiteMetric>();
                        foreach( var vdp in vdp_collection.vdps )
                        {
                            values.Add( new MonthlyWebsiteMetric {
                                BusinessUnitID = bu.Id,
                                WebsiteProviderID = vdp_collection.website_provider_id,
                                MonthApplicable = vdp.month,
                                NewVDPs = vdp.new_value,
                                UsedVDPs = vdp.used_value
                            });
                        }
                        WebsiteProviderMetricsModel.SetVDPs( 
                            bu.Id,
                            vdp_collection.website_provider_id, 
                            monthRangeBegin,
                            monthRangeEnd,
                            values );
                        
                        // save credentials
                        if( vdp_collection.username == null )
                            vdp_collection.username = "";
                        if( vdp_collection.password == null )
                            vdp_collection.password = "";
                        DealerListingSiteCredentialRepository.Save( new DealerListingSiteCredential {
                            BusinessUnitId = bu.Id,
                            DestinationId = vdp_collection.website_provider_id,
                            Username = vdp_collection.username,
                            Password = vdp_collection.password
                        });
                    }
                }
            }
            if( data_obj.dealership_segment != null )
            {
                // save dealership segment
                DealershipSegmentModel.set( bu.Id, data_obj.dealership_segment.segment );
            }
        }
        
        private void VerifyBusinessUnit( BusinessUnit bu, string api_token = null )
        {
            if( bu == null )
                throw new WanamakerGetException("Could not retrieve logged-in user's business unit ID from the HTTP Context.");

            if( api_token != null )
            {
                try
                {
                    if( int.Parse( api_token ) != bu.Id )
                        throw new WanamakerGetException("Logged-in user's business unit ID does not match given API token; for this implementation, it must.");
                }
                catch( FormatException ex )
                {
                    throw new WanamakerGetException("API token must be an integer.", ex);
                }
            }
        }
   }

    public interface IWebContext
    {
        BusinessUnit GetBusinessUnit();
    }

    public class WebContext : IWebContext
    {
        public BusinessUnit GetBusinessUnit()
        {
            return HttpContext.Current == null ? null : HttpContext.Current.Items["BusinessUnit"] as BusinessUnit;
        }
    }

    public class WanamakerGetException : Exception
    {
        public WanamakerGetException() {}
        public WanamakerGetException( string message ) : base( message ) {}
        public WanamakerGetException( string message, Exception inner ) : base( message, inner ) {}
    }
}
