﻿using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics;
using MAX.Entities.Enumerations;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Wanamaker.WebApp.Areas.WanamakerGet.Models
{
    public class WanamakerDataCollectionFormModel
    {
        public List<DealerListingSite> dealer_listing_sites = new List<DealerListingSite>();
        public GoogleAnalyticsProfileWrapper google_analytics;
        public DealerVehicleDetailPage dealer_vdps = new DealerVehicleDetailPage();
        public DealershipSegmentWrapper dealership_segment;

        public string toJSON()
        {
            return JsonConvert.SerializeObject( this, new IsoDateTimeConverter() );
        }

        public static WanamakerDataCollectionFormModel fromJSON( string json_data )
        {
            return JsonConvert.DeserializeObject<WanamakerDataCollectionFormModel>( json_data );
        }
    }
    
    public class DealerListingSite
    {
        public int site_id;
        public string site_name;
        public string username;
        public string password;
        public string dealer_id;
        public List<LinkedBudget> monthly_cost = new List<LinkedBudget>();
    }

    public class GoogleAnalyticsProfileWrapper
    {
        public List<ProfileResult> google_analytics_profiles = new List<ProfileResult>();
        public List<Provider> compatible_providers = new List<Provider>();
        public int selected_profile_id = -1;
        public string selected_profile_name;
        public int associated_provider_id = -1;
        public bool is_admin;
        public bool admin_lock;
    }

    public class DealerVehicleDetailPage
    {
        public List<Provider> dealer_website_providers;
        public int dealer_website_provider_id;
        public string dealer_website_provider_freeform_name;
        public List<DealerVDPData> vdp_data = new List<DealerVDPData>();
    }

    public class DealershipSegmentWrapper
    {
        public DealershipSegment segment;
    }

    public class LinkedBudget
    {
        public DateTime month;
        public int value;
        public bool soft;
    }

    public class Provider
    {
        public int id;
        public string name;
    }

    public class DealerVDPData
    {
        public int website_provider_id;
        public string username;
        public string password;
        public List<DatedVDP> vdps = new List<DatedVDP>();
    }

    public class DatedVDP
    {
        public DateTime month;
        public int used_value;
        public int new_value;
    }
}