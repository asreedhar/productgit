﻿using System.Web.Mvc;

namespace Wanamaker.WebApp.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "AutoLoadStatus",
                "Admin/BatchAutoLoadStatus.aspx",
                new { controller = "Home", action = "AutoLoadStatus" }
            );

            context.MapRoute(
                "AutoLoadDetails",
                "Admin/AutoLoadDetails.aspx/{businessUnitId}",
                new { controller = "Home", action = "Details" }
            );

            context.MapRoute(
                "FetchAudit",
                "Admin/FetchAudit.aspx/{vin}",
                new { controller = "Home", action = "FetchAudit" }
            );

            context.MapRoute(
               "WhiteList",
               "Admin/WhiteList.aspx/{manufacturerId}/{divisionId}",
               new { controller = "Home", action = "WhiteList" }
           );
        }
    }
}
