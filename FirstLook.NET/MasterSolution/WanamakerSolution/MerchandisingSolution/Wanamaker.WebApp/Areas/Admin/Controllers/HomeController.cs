﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using Core.Messaging;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Admin;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using MAX.ExternalCredentials;
using log4net;
using Merchandising.Messages;
using Wanamaker.WebApp.Areas.Admin.ViewModels;

namespace Wanamaker.WebApp.Areas.Admin.Controllers
{
    public class HomeController : Controller
    {
        private readonly IAutoLoadAuditRepository _repository;
        private readonly ICredentialRepository _credentialRepository;
	    private readonly IFileStorage _auditStore;
        //private IFileStorage _fileStore;

	    #region Logging

	    protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

	    #endregion Logging


        public HomeController(IAutoLoadAuditRepository repository, IFileStoreFactory fileStoreFactory, ICredentialRepository credentialedSiteRepository)
        {
            _repository = repository;
	        _auditStore = fileStoreFactory.CreateAutoLoadAudit();
            //_fileStore = fileStoreFactory.CreateFileStore();
            _credentialRepository = credentialedSiteRepository;
        }
        
        [HttpGet]
        public ActionResult AutoLoadStatus()
        {
            var audits = _repository.FetchAudit();
            var model = new AutoLoadViewModel {Rows = audits};

            return View(model);
        }

        [HttpGet]
        public ActionResult Details(int businessUnitId)
        {   
            var details = _repository.FetchBusinessUnitDetails(businessUnitId);

            var detailRows = new List<AutoLoadDetailsRow>();
            var credCache = new Dictionary<string, EncryptedSiteCredential>();

            foreach (var detail in details)
            {
                var detailsViewModel = new AutoLoadDetailsRow
                                           {
                                               Vin = detail.Vin,
                                               Start = detail.Start.ToString("g"),
                                               End = detail.End == DateTime.MinValue ? "pending" : detail.End.ToString("g"),
                                               Status = detail.Status,
                                               Make = detail.Make,
                                               Manufacturer =  detail.Manufacturer,
                                               ManufacturerId = detail.ManufacturerId,
                                               DivisionId = detail.DivisionId
                                           };



                // find the credential status
                var inventoryData = InventoryData.Fetch(businessUnitId, detail.InventoryId);
                EncryptedSiteCredential creds;
                
                if(!credCache.TryGetValue(detail.Make, out creds))  // just caching to speed things up
                {
                    creds = _credentialRepository.GetSiteCredentials(inventoryData, true);  // actual call to get creds
                    credCache.Add(detail.Make, creds);
                }

                if(creds != null)
                {
                    if (!String.IsNullOrEmpty(creds.Password) && !String.IsNullOrEmpty(creds.UserName))
                        detailsViewModel.HasCredentials = true;
                    detailsViewModel.AccountLocked = creds.IsLockedOut;

                }


                detailRows.Add(detailsViewModel);
            }

            var finder = new BusinessUnitFinder();
            var businessUnit = finder.Find(businessUnitId);

            var model = new AutoLoadDetailsViewModel {BusinessUnit = businessUnit.Name, Rows = detailRows.ToArray()};

            return View(model);
        }

        [HttpGet]
        public ActionResult WhiteList(int manufacturerId, int divisionId)
        {
            var list = _repository.GetWhiteList(manufacturerId, divisionId);
            return View(list);
        }

        [HttpGet]
        public ActionResult FetchAudit(string vin)
        {
            bool exists;
            string xml = "<NoRecord/>";
            using (Stream stream = _auditStore.Exists(vin, out exists))
            {
                if(exists)
                {
                    TextReader reader = new StreamReader(stream);
                    xml = reader.ReadToEnd();
                }
            }

            return new ContentResult { Content = xml, ContentEncoding = Encoding.UTF8, ContentType = "text/xml" };
        }
    }
}
