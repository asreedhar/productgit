﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FirstLook.Merchandising.DomainModel.Admin;

namespace Wanamaker.WebApp.Areas.Admin.ViewModels
{
    public class AutoLoadViewModel
    {
        public AutoLoadViewModel()
        {
            StatusHeaders = new string[] { "Pending", "Invalid Credentials", "Insufficient Permissions", "No Vehicle Found", "Network Error", "Unknown Error", "Success" };
        }

        public string[] StatusHeaders { get; set; }
        public AutoLoadBusinessAudit[] Rows;
    }
}