﻿namespace Wanamaker.WebApp.Areas.Admin.ViewModels
{
    public class AutoLoadDetailsRow
    {
        public AutoLoadDetailsRow()
        {
            HasCredentials = false;
            AccountLocked = false;
        }

        public string Vin { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        public string Status { get; set; }
        public string Make { get; set; }

        public bool HasCredentials { get; set; }
        public string Manufacturer { get; set; }
        public bool AccountLocked { get; set; }
        public int ManufacturerId { get; set; }
        public int DivisionId { get; set; }
    }
}