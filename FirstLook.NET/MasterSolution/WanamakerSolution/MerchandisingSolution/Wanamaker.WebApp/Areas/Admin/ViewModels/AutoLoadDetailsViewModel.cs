﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wanamaker.WebApp.Areas.Admin.ViewModels
{
    public class AutoLoadDetailsViewModel
    {
        public string BusinessUnit { get; set; }
        public AutoLoadDetailsRow[] Rows { get; set; }
    }

}