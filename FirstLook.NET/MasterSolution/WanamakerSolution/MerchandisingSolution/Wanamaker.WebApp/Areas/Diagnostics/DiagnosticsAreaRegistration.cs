﻿using System.Web.Mvc;

namespace Wanamaker.WebApp.Areas.Diagnostics
{
    public class DiagnosticsAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Diagnostics";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            // Disabled routing until we have content and oauth configured
            //
            /*
            context.MapRoute(
                "Diagnostics",
                "Diagnostics.aspx/{countPerPage}",
                new { controller = "Diagnostics", action = "index", countPerPage = UrlParameter.Optional }
            );
            */
        }
    }
}
