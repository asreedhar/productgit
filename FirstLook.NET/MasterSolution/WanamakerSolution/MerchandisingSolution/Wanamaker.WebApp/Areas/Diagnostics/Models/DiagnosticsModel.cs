﻿using System;
using System.Collections.Generic;

namespace Wanamaker.WebApp.Areas.Diagnostics.Models
{
    public class DiagnosticsModel
    {
        
        public List<Entry> Entries { get; set; }
        
        public DateTime StartDate { get; set; }
        
        public int MaxInitialEntryCount { get; set; }

        /// <summary>
        /// Diagnostics view model
        /// </summary>
        /// <param name="entryCount">How many entries to display initially, default 30</param>
        public DiagnosticsModel(DateTime startDate, int maxInitialEntryCount = 30)
        {
            StartDate = startDate;
            MaxInitialEntryCount = maxInitialEntryCount;
        }
    }
}