﻿using System;

namespace Wanamaker.WebApp.Areas.Diagnostics.Models
{
    public class Entry
    {
        public enum EntryType
        {
            Car, /* online, offline, price change, price export, removed from campaign, added to campaign */
            Campaign, /* created, expired */
            Autoload
        }

        public EntryType Type { get; private set; }

        public DateTime EventTime { get; private set; }

        public String Invoker { get; set; }

        public Entry(EntryType type, DateTime eventTime)
        {
            this.Type = type;
            this.EventTime = eventTime;
        }
    }
}