﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Wanamaker.WebApp.Areas.Diagnostics.Models;
using Wanamaker.WebApp.Helpers;

namespace Wanamaker.WebApp.Areas.Diagnostics.Controllers
{
    public class DiagnosticsController : Controller
    {
        
        // GET: /Diagnostics/
        // optional: initial count
        //
        [HttpGet]
        public ActionResult Index(int countPerPage = 30)
        {
            countPerPage = countPerPage.Clamp(10, 100);

            var startDate = DateTime.UtcNow;

            var model = new DiagnosticsModel(startDate, countPerPage);
            var entries = GetEntries(startDate, countPerPage, 1);
            
            model.Entries = entries;

            return View(model);
        }

        // dynamo limits queries to 1mb of results and will indicate partial results
        // with a key to retrieve the next megabyte of data
        //
        private List<Entry> GetEntries(DateTime startDate, int pageSize, int page)
        {
            // TODO: get real data
            return new List<Entry>();
        }
    }
}
