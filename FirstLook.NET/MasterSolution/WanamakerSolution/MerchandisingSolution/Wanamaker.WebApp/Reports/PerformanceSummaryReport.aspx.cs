﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.Membership;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Reports;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using MAX.Entities;
using Microsoft.Reporting.WebForms;

namespace Wanamaker.WebApp.Reports
{
    public partial class PerformanceSummaryReport : ReportBasePage
    {
        private static readonly SitePerformanceViewModel Overall = new SitePerformanceViewModel(new SitePerformance { Site = new Site { Id = -1, Name = "Overall" } });

        private readonly ISitePerformanceRepository _sitePerformanceRepository;
        private readonly IBusinessUnitRepository _businessUnitRepository;
        private readonly IMemberContext _memberContext;
        protected  ProfileCommon UserProfile = new ProfileCommon();

        public PerformanceSummaryReport()
        {
            _memberContext = Registry.Resolve<IMemberContext>();
            _businessUnitRepository = new BusinessUnitRepository();
            _sitePerformanceRepository = Registry.Resolve<ISitePerformanceRepository>();
        }

        protected void Page_Init(object sender, EventArgs e)
        {
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;

            BindDealers();
            BindDateRanges();

            inventoryTypes.Attributes.Add("onchange", "return ClientValidate(this);");
            sites.Attributes.Add("onchange", "return ClientValidate(this);");
            dealers.Attributes.Add("onchange", "return ClientValidate(this);");

            viewer.LocalReport.ReportPath = @"Reports\PerformanceSummary.rdlc";

            inventoryTypes.SelectedValue = ((VehicleType)UserProfile.NewUsed).IntValue.ToString(); // "0", "1", "2"
        }

        private void BindDateRanges()
        {
            var today = DateTime.Now;
            var years = GetYearsFrom2010();

            FromMonth.SelectedValue = BeginningOfMonth(today) ? PreviousMonth(today) : today.Month.ToString();
            ToMonth.SelectedValue = today.Month.ToString();

            FromYear.DataSource = years;
            FromYear.DataBind();

            ToYear.DataSource = years;
            ToYear.DataBind();
        }

        private string PreviousMonth(DateTime today)
        {
            return today.AddMonths(-1).Month.ToString();
        }

        private bool BeginningOfMonth(DateTime today)
        {
            return (today.Day < 4);
        }


        private List<int> GetYearsFrom2010()
        {
            var yearsSince2010 = DateTime.Now.Year - 2010;
            var years = new List<int>();

            for (int i = yearsSince2010; i >= 0; i--)
            {
                years.Add(2010 + i);
            }

            return years;
        }

        private DateTime ToDate
        {
            get
            {
                var toYear = int.Parse(ToYear.SelectedValue);
                var toMonth = int.Parse(ToMonth.SelectedValue);

                var daysInMonth = DateTime.DaysInMonth(toYear, toMonth);

                return new DateTime(toYear, toMonth, daysInMonth);
            }
        }

        private DateTime FromDate
        {
            get
            {
                var fromYear = int.Parse(FromYear.SelectedValue);
                var fromMonth = int.Parse(FromMonth.SelectedValue);

                return new DateTime(fromYear, fromMonth, 1);
            }
        }

        private VehicleType CurrentVehicleType
        {
            get { return int.Parse(inventoryTypes.SelectedValue); }
        }

        private int? CurrentBusinessUnitId
        {
            get
            {
                int value;
                return !int.TryParse(dealers.SelectedValue, out value) ? (int?)null : value;
            }
            set
            {
                if (value.HasValue)
                {
                    dealers.SelectedValue = value.Value.ToString();
                }
                else
                {
                    dealers.SelectedIndex = 0;
                }
            }
        }

        protected void RebindReport(object sender, EventArgs e)
        {
            BindReport();
        }

        protected void OnLoadReportData(object sender, EventArgs e)
        {
            if (FromDate > ToDate) return;

            BindReport();
        }

        private void BindReport()
        {
            viewer.Visible = true;
            viewer_spinner.Visible = false;

            if (FromDate > ToDate) return;

            var businessUnitId = CurrentBusinessUnitId;
            if (!businessUnitId.HasValue) return;

            var siteTrends = _sitePerformanceRepository.GetMonthlySiteTrends(businessUnitId.Value, CurrentVehicleType, new DateRange(FromDate, ToDate));
            List<SitePerformance> totals = new List<SitePerformance>();
            siteTrends.Values.ToList().ForEach(tdv =>
            {
                if (tdv.Count() > 0)
                {
                    var total = tdv.First();
                    total = tdv.Summation();
                    totals.Add(total);
                }
            });

            var sitePerformances = totals
                .Select(p => new SitePerformanceViewModel(p))
                .ToList();

            var anyMissingData = false;

            foreach (var sitePerformanceViewModel in sitePerformances)
            {
                var earliestDataForSite = _sitePerformanceRepository.GetEarliestDataPoint(businessUnitId.Value, sitePerformanceViewModel.Id);

                if (earliestDataForSite.HasValue && FromDate < earliestDataForSite.Value)
                {
                    anyMissingData = true;
                }
            }

            DataMessage.Visible = anyMissingData;

            BindSiteSelector(sitePerformances);
            BindReportDataToViewer(sitePerformances);

        }

        private class BusinessUnitComparer : IComparer<BusinessUnit>
        {
            public int Compare(BusinessUnit first, BusinessUnit second)
            {
                return first.Name.CompareTo(second.Name);
            }
        }

        private void BindDealers()
        {
            var businessUnits = _businessUnitRepository.GetBusinessUnitsByLogin(_memberContext.Current.UserName).ToList();
            businessUnits.Sort(new BusinessUnitComparer());
            dealers.DataSource = businessUnits;
            dealers.DataTextField = "Name";
            dealers.DataValueField = "Id";
            dealers.DataBind();

            var businessUnit = (BusinessUnit)Context.Items["BusinessUnit"];
            CurrentBusinessUnitId = businessUnit == null ? (int?)null : businessUnit.Id;
        }

        protected void SiteSelectionChanged(object sender, EventArgs e)
        {
            if (FromDate > ToDate) return;

            var businessUnit = CurrentBusinessUnitId;
            if (!businessUnit.HasValue) return;

            //perhaps cache this data instead of calling for it again
            var siteTrends = _sitePerformanceRepository.GetMonthlySiteTrends( businessUnit.Value, CurrentVehicleType, new DateRange(FromDate, ToDate));
            List<SitePerformance> totals = new List<SitePerformance>();
            siteTrends.Values.ToList().ForEach(tdv =>
            {
                if (tdv.Count() > 0)
                {
                    var total = tdv.First();
                    total = tdv.Summation();
                    totals.Add(total);
                }
            });

            var sitePerformances = totals
                .Select(p => new SitePerformanceViewModel(p))
                .ToList();

            BindReportDataToViewer(sites.SelectedValue == "Overall" ? sitePerformances : sitePerformances.Where(p => p.Name == sites.SelectedValue));
        }


        private void BindSiteSelector(IEnumerable<SitePerformanceViewModel> sitePerformances)
        {
            var performanceStats = sitePerformances.ToList();
            if (performanceStats.Count > 1)
            {
                performanceStats.Insert(0, Overall);
            }

            sites.DataSource = performanceStats;
            sites.DataTextField = "Name";
            sites.DataBind();

        }

        private void BindReportDataToViewer(IEnumerable<SitePerformanceViewModel> reportData)
        {
            var dataSource = viewer.LocalReport.DataSources.SingleOrDefault(rds => rds.Name == "SitePerformanceViewModel");
            if (dataSource == null)
            {
                viewer.LocalReport.DataSources.Add(new ReportDataSource("SitePerformanceViewModel", reportData));
            }
            else
            {
                dataSource.Value = reportData;
            }

            viewer.DataBind();
        }

        protected void ValidateFromAndToDate(object source, ServerValidateEventArgs args)
        {
            if (FromDate > ToDate)
            {
                args.IsValid = false;
            }
        }

        protected void OnInventorySearching ( object sender, EventArgs e )
        {
            Response.Redirect( "~/Workflow/Inventory.aspx?search=" + InventorySearch.SearchText );
        }
    }
}