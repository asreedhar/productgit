﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.Membership;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.MaxAnalytics;
using FirstLook.Merchandising.DomainModel.Postings;
using FirstLook.Merchandising.DomainModel.Reports.VehicleActivity;
using FirstLook.Merchandising.DomainModel.Reports.VehicleActivity.Client;
using Microsoft.Reporting.WebForms;
using MvcMiniProfiler;

namespace Wanamaker.WebApp.Reports
{
    public partial class VehicleActivityReport : ReportBasePage
    {
        private IMemberContext MemberContext { get; set; }
        private ReportModel ReportModel { get; set; }
        private IPhotoServices PhotoServices { get; set; }

        public VehicleActivityReport()
        {
            PhotoServices = Registry.Resolve<IPhotoServices>();
            MemberContext = Registry.Resolve<IMemberContext>();
            var maxAnalytics = Registry.Resolve<IMaxAnalytics>();

            ReportModel = new ReportModel(MemberContext, PhotoServices, maxAnalytics);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                InitialPageLoad();
        }

      
        private void InitialPageLoad()
        {
            BindDealers();

            SetInventoryTypeFilterFromQueryString();

            viewer.LocalReport.ReportPath = @"Reports\VehicleActivityReport.rdlc";
            viewer.LocalReport.EnableHyperlinks = true;
        }

        private void BindDealers()
        {
            using(MiniProfiler.Current.Step("GetBusinessUnitList()"))
                dealers.DataSource = ReportModel.GetBusinessUnitList().ToList();
            dealers.DataTextField = "Name";
            dealers.DataValueField = "BusinessUnitID";
            dealers.DataBind();

            SetDealerComboToCurrentBusinessUnit();
        }

        private void SetDealerComboToCurrentBusinessUnit()
        {
            var bu = (BusinessUnit) Context.Items["BusinessUnit"];
            CurrentBusinessUnitId = bu == null ? (int?) null : bu.Id;
        }

        private int? CurrentBusinessUnitId
        {
            get
            {
                int v;
                return !int.TryParse(dealers.SelectedValue, out v) ? (int?) null : v;
            }
            set
            {
                if (value.HasValue)
                    dealers.SelectedValue = value.Value.ToString(CultureInfo.InvariantCulture);
                else
                    dealers.SelectedIndex = 0;
            }
        }

        protected void OnDelearChanged(object sender, EventArgs e)
        {
            RebindReport(sender, e);
        }

        protected void RebindReport(object sender, EventArgs e)
        {
            BindReport();
        }

        protected void OnLoadReportData(object sender, EventArgs e)
        {
            BindReport();
        }

        private void BindReportWithNoData()
        {
            BindReportDataToViewer(new FlatVehicleActivity[0]);
        }

        private void BindReport()
        {
            viewer.Visible = true;
            viewer_spinner.Visible = false;

            var businessUnitId = CurrentBusinessUnitId;
            if(businessUnitId.HasValue)
            {
                BindReportDataToViewer(
                    ReportModel.GetVehicleActivity(businessUnitId.Value, GetInventoryTypeFilter(), GetTextFilter(),
                                                   GetMinAgeFilter(), GetActivityFilter()));
            }
            else
            {
                BindReportWithNoData();
            }
        }

        private string GetTextFilter()
        {
            return string.IsNullOrWhiteSpace( InventorySearch.SearchText )
                       ? null
                       : InventorySearch.SearchText;
        }

        private int? GetInventoryTypeFilter()
        {
            int inventoryTypeVal;
            return
                int.TryParse(inventoryTypes.SelectedValue, out inventoryTypeVal) && inventoryTypeVal != 0
                    ? inventoryTypeVal
                    : (int?)null;
        }

        private void SetInventoryTypeFilterFromQueryString()
        {
            if (!Request.QueryString.AllKeys.Contains("usednew")) return;

            object usedNewFilter = Request.QueryString["usednew"];
            if (usedNewFilter == null) return;
            if (inventoryTypes.Items.FindByValue(usedNewFilter.ToString()) == null) return;
            inventoryTypes.SelectedValue = usedNewFilter.ToString();
        }

        private int? GetMinAgeFilter()
        {
            int minAgeVal;
            return
                int.TryParse(ageInDays.SelectedValue, out minAgeVal) && minAgeVal > 0
                    ? minAgeVal
                    : (int?)null;
        }

        private string GetActivityFilter()
        {
            return activityFilter.SelectedValue;
        }

        private void BindReportDataToViewer(IEnumerable<FlatVehicleActivity> reportData)
        {
            var dataSource = viewer.LocalReport.DataSources.SingleOrDefault(rds => rds.Name == "VehicleActivity");
            if (dataSource == null)
            {
                viewer.LocalReport.DataSources.Add(
                    new ReportDataSource("VehicleActivity", reportData));
            }
            else
            {
                dataSource.Value = reportData;
            }
            viewer.DataBind();
        }
    }
}