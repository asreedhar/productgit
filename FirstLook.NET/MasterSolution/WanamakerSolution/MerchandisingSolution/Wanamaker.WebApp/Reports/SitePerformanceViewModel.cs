﻿using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;

namespace Wanamaker.WebApp.Reports
{
    public class SitePerformanceViewModel
    {
        private readonly SitePerformance _performance;

        public SitePerformanceViewModel(SitePerformance performance)
        {
            _performance = performance;
        }

        public int Id
        {
            get { return _performance.Site.Id; }
        }

        public string Name
        {
            get { return _performance.Site.Name; }
        }

        public int SearchPageViews
        {
            get { return _performance.SearchPageViewCount; }
        }

        public int DetailPageViews
        {
            get { return _performance.DetailPageViewCount; }
        }

        public int EmailLeads
        {
            get { return _performance.EmailLeadsCount; }
        }

        public int PhoneLeads
        {
            get { return _performance.PhoneLeadsCount; }
        }

        public int MapsAndPrints
        {
            get { return _performance.MapsViewedCount + _performance.AdPrintedCount; }
        }

        public int TotalLeads
        {
            get { return _performance.TotalLeads; }
        }

        public int TotalActions
        {
            get { return _performance.TotalActions; }
        }

        public decimal ClickThroughRate
        {
            get { return _performance.ClickThroughRate; }
        }

        public decimal ConversionRateFromDetailPage
        {
            get { return _performance.ConversionRateFromDetailPage; }
        }

    }
}