﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VehicleOnlineStatusReport.aspx.cs" MasterPageFile="~/Workflow/Workflow.Master"
    Theme="None" Title="Not Online Report | MAX : Online Inventory. Perfected." Inherits="Wanamaker.WebApp.Reports.VehicleOnlineStatusReport"
    ClientIDMode="AutoID" %>
<%@ Import Namespace="Wanamaker.WebApp.Helpers" %>

<%@ Register TagPrefix="rsweb" Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a, processorArchitecture=MSIL"
    Namespace="Microsoft.Reporting.WebForms" %>
<%@ Register Src="~/Workflow/Controls/SearchInventory.ascx" TagPrefix="workflow" TagName="InventorySearch"  %>

<asp:Content ID="CssContent" ContentPlaceHolderID="CssPlaceHolder" runat="server">

    <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Main.debug.css") %>" />
    <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Reports.debug.css") %>" />

</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="BodyPlaceHolder" runat="server">

    <asp:ScriptManager ID="ScriptManager2" runat="server"></asp:ScriptManager>
    
    <%= Wanamaker.WebApp.ProfilerSetting.RenderIncludes() %>

    <div id="BulkUpload">
        <a href="<%= BulkUploadUrl %>" title="Upload photos for multiple vehicles" target="_blank">Bulk Upload</a>
    </div>

    <workflow:InventorySearch id="InventorySearch" runat="server" OnInventorySearching="RebindReport"></workflow:InventorySearch>

    <div id="Content" class="clearfix notOnlineReport">

        <div id="ReportTitle"><h1>Not Online Report</h1></div>

        <div id="ReportFilterForm">

            <div id="SelectDealerFields">
                <label for="<%= dealers.ClientID %>">
                    Select Dealer:
                </label>
                <asp:DropDownList ID="dealers" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dealers_SelectedIndexChanged">
                </asp:DropDownList>
            </div>

            <div id="SelectStatus">
                <label for="<%= inventoryTypes.ClientID %>">
                    Status:
                </label>
                <asp:DropDownList ID="status" runat="server" AutoPostBack="true" OnSelectedIndexChanged="RebindReport">
                    <asp:ListItem Text="Not Online" Value="2" />
                    <asp:ListItem Text="Online" Value="1" />
                    <asp:ListItem Text="All" Value="0" />
                </asp:DropDownList>
            </div>

            <div id="SelectUsedOrNewFields">
                <label for="<%= inventoryTypes.ClientID %>">
                    New or Used:
                </label>
                <asp:DropDownList ID="inventoryTypes" runat="server" AutoPostBack="true" OnSelectedIndexChanged="RebindReport">
                    <asp:ListItem Text="New & Used" Value="0" />
                    <asp:ListItem Text="Used Only" Value="2" />
                    <asp:ListItem Text="New Only" Value="1" />
                </asp:DropDownList>
            </div>

            <div id="SelectMinAgeInDays">
                <label for="<%= ageInDays.ClientID %>">
                    Min Age (days):
                </label>
                <asp:DropDownList ID="ageInDays" runat="server" AutoPostBack="true" OnSelectedIndexChanged="RebindReport">
                    <asp:ListItem Text="ALL" Value="0" />
                    <asp:ListItem Value="1" />
                    <asp:ListItem Value="2" />
                    <asp:ListItem Value="3" />
                    <asp:ListItem Value="4" />
                    <asp:ListItem Value="5" />
                    <asp:ListItem Value="6" />
                    <asp:ListItem Value="7" />
                    <asp:ListItem Value="8" />
                    <asp:ListItem Value="9" />
                    <asp:ListItem Value="10" />
                    <asp:ListItem Value="11" />
                    <asp:ListItem Value="12" />
                    <asp:ListItem Value="13" />
                    <asp:ListItem Value="14" />
                </asp:DropDownList>
            </div>
        </div>

        <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="ReportUpdatePanel">
            <ContentTemplate>
                <div id="ReportDiv">
                    <rsweb:ReportViewer ID="viewer" Visible="false" runat="server" Width="979" ProcessingMode="Local" AsyncRendering="True" InteractivityPostBackMode="AlwaysAsynchronous"
                        SizeToReportContent="True" Font-Names="Verdana" Font-Size="8pt" ShowBackButton="false" ShowRefreshButton="false" ShowWaitControlCancelLink="false"
                        WaitControlDisplayAfter="500" PageCountMode="Actual" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt"
                        ShowFindControls="False" ShowZoomControl="False">
                    </rsweb:ReportViewer>
                    <%-- Following two controls delay loading the data and display a spinner until the page is "ready". --%>
                    <div id="viewer_spinner" visible="true" runat="server" clientidmode="Static" />
                    <asp:Button ID="TriggerLoadReportData" runat="server" Style="display: none;" OnClick="OnLoadReportData" />
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="InventorySearch" EventName="InventorySearching" />
                <asp:AsyncPostBackTrigger ControlID="dealers" />
                <asp:AsyncPostBackTrigger ControlID="inventoryTypes" />
                <asp:AsyncPostBackTrigger ControlID="status" />
                <asp:AsyncPostBackTrigger ControlID="ageInDays" />
            </Triggers>
        </asp:UpdatePanel>
    </div>


</asp:Content>

<asp:Content ID="ScriptContent" ContentPlaceHolderID="ScriptsPlaceHolder" runat="server">

    <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Max/MAX.UI.Global.debug.js") %>"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            <% if(!IsPostBack)
               { %>
            // Kick off the report data load...
            __doPostBack( '<%= TriggerLoadReportData.UniqueID %>', '' );
            <% } %>
        } );
    </script>

</asp:Content>

