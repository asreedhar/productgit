﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PerformanceSummaryReport.aspx.cs" MasterPageFile="~/Workflow/Workflow.Master"
    Theme="None" Title="Performance Summary Report | MAX : Online Inventory. Perfected." Inherits="Wanamaker.WebApp.Reports.PerformanceSummaryReport"
    ClientIDMode="AutoID" %>
<%@ Import Namespace="Wanamaker.WebApp.Helpers" %>

<%@ Register TagPrefix="rsweb" Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a, processorArchitecture=MSIL"
    Namespace="Microsoft.Reporting.WebForms" %>
<%@ Register Src="~/Workflow/Controls/SearchInventory.ascx" TagPrefix="workflow" TagName="InventorySearch"  %>

<asp:Content ID="CssContent" ContentPlaceHolderID="CssPlaceHolder" runat="server">
    
    <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Main.debug.css") %>" />
    <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Reports.debug.css") %>" />
    
</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="BodyPlaceHolder" runat="server">
    
    <asp:ScriptManager ID="ScriptManager2" runat="server"></asp:ScriptManager>
    
    <%= Wanamaker.WebApp.ProfilerSetting.RenderIncludes() %>

    <div id="BulkUpload">
        <a href="<%= BulkUploadUrl %>" title="Upload photos for multiple vehicles" target="_blank">Bulk Upload</a>
    </div>

    <workflow:InventorySearch id="InventorySearch" runat="server" OnInventorySearching="OnInventorySearching"></workflow:InventorySearch>
    
    <div id="Content" class="clearfix">

        <div id="ReportTitle">
            <h1>Performance Summary Report</h1>
        </div>
    
        <div id="ReportFilterForm" class="clearfix">

            <div id="DateRangeSelector">

                <div id="DateRangeFields" class="clearfix">

                    <div id="FromDateFields">
                        <label for="<%= FromMonth.ClientID %>">From:</label>
                            <asp:DropDownList ID="FromMonth" class="MonthDropDown" runat="server">
                                <asp:ListItem Text="January" Value="1" />
                                <asp:ListItem Text="February" Value="2" />
                                <asp:ListItem Text="March" Value="3" />
                                <asp:ListItem Text="April" Value="4" />
                                <asp:ListItem Text="May" Value="5" />
                                <asp:ListItem Text="June" Value="6" />
                                <asp:ListItem Text="July" Value="7" />
                                <asp:ListItem Text="August" Value="8" />
                                <asp:ListItem Text="September" Value="9" />
                                <asp:ListItem Text="October" Value="10" />
                                <asp:ListItem Text="November" Value="11" />
                                <asp:ListItem Text="December" Value="12" />
                            </asp:DropDownList>

                            <asp:DropDownList ID="FromYear" runat="server"></asp:DropDownList>
                    </div>

                    <div id="ToDateFields">
                        <label for="<%= ToMonth.ClientID %>">To:</label>
                
                        <asp:DropDownList ID="ToMonth" class="MonthDropDown" runat="server">
                            <asp:ListItem Text="January" Value="1" />
                            <asp:ListItem Text="February" Value="2" />
                            <asp:ListItem Text="March" Value="3" />
                            <asp:ListItem Text="April" Value="4" />
                            <asp:ListItem Text="May" Value="5" />
                            <asp:ListItem Text="June" Value="6" />
                            <asp:ListItem Text="July" Value="7" />
                            <asp:ListItem Text="August" Value="8" />
                            <asp:ListItem Text="September" Value="9" />
                            <asp:ListItem Text="October" Value="10" />
                            <asp:ListItem Text="November" Value="11" />
                            <asp:ListItem Text="December" Value="12" />
                        </asp:DropDownList>

                        <asp:DropDownList ID="ToYear" runat="server"></asp:DropDownList>
                        
                    </div>
                    
                    <asp:Button ID="UpdateButton" Text="Update" runat="server" OnClick="RebindReport" CssClass="button" OnClientClick="return ClientValidate()" />
                    <span id="DateRangeError">To Date Must Be Earlier Than From Date</span>

                </div>
               
             </div>

            <div id="ReportFilters">

                <div id="DealerSelector">
                    <label for="<%= dealers.ClientID %>">Select Dealer:</label>
                    <asp:DropDownList ID="dealers" runat="server" OnSelectedIndexChanged="RebindReport" />
                </div>

                <div id="SiteSelector">
                    <asp:UpdatePanel ID="SiteSelection" runat="server">
                        <ContentTemplate>
                            <label for="<%= sites.ClientID %>">Select Site:</label>
                            <asp:DropDownList ID="sites" runat="server" OnSelectedIndexChanged="SiteSelectionChanged" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>

                <div id="NewOrUsedSelector">
                    <label for="<%= inventoryTypes.ClientID %>">
                        New or Used:</label>
                    <asp:DropDownList ID="inventoryTypes" runat="server" OnSelectedIndexChanged="RebindReport">
                        <asp:ListItem Text="Both" Value="0" />
                        <asp:ListItem Text="New" Value="1" />
                        <asp:ListItem Text="Used" Value="2" />
                    </asp:DropDownList>
                </div>

            </div>
    
            <div id="DataMessage" visible="false" runat="server">Note: System may not contain data for entire date range.</div>
    
        </div>

        <asp:UpdatePanel ID="ReportControlUpdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="ReportDiv">
                    <rsweb:ReportViewer ID="viewer" Visible="false" runat="server" Width="979" ProcessingMode="Local" AsyncRendering="True" InteractivityPostBackMode="AlwaysAsynchronous"
                        SizeToReportContent="True" Font-Names="Verdana" Font-Size="8pt" ShowBackButton="false" ShowRefreshButton="false" ShowWaitControlCancelLink="false"
                        WaitControlDisplayAfter="500" PageCountMode="Actual" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt"
                        ShowZoomControl="False" ShowFindControls="False">
                    </rsweb:ReportViewer>
                    <%-- Following two controls delay loading the data and display a spinner until the page is "ready". --%>
                    <div id="viewer_spinner" visible="true" runat="server" clientidmode="Static" />
                    <asp:Button ID="TriggerLoadReportData" runat="server" Style="display: none;" OnClick="OnLoadReportData" />
                </div>
            </ContentTemplate>
            <Triggers>
                <%--<asp:AsyncPostBackTrigger ControlID="wfhHeader" EventName="InventorySearching" />--%>
                <asp:AsyncPostBackTrigger ControlID="dealers" />
                <asp:AsyncPostBackTrigger ControlID="inventoryTypes" />
            </Triggers>
        </asp:UpdatePanel>
        
    </div>

</asp:Content>

<asp:Content ID="ScriptContent" ContentPlaceHolderID="ScriptsPlaceHolder" runat="server">
    
    <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Max/MAX.UI.Global.debug.js") %>"></script>

    <script type="text/javascript">

        // TODO: Move this script to script file
        function ClientValidate(elementRef) {

            var toMonthElement = document.getElementById("<%= ToMonth.ClientID %>");
            var toMonth = parseInt(toMonthElement.options[toMonthElement.selectedIndex].value);

            var fromMonthElement = document.getElementById("<%= FromMonth.ClientID %>");
            var fromMonth = parseInt(toMonthElement.options[fromMonthElement.selectedIndex].value);

            var toYearElement = document.getElementById("<%= ToYear.ClientID %>");
            var toYear = parseInt(toYearElement.options[toYearElement.selectedIndex].text);

            var fromYearElement = document.getElementById("<%= FromYear.ClientID %>");
            var fromYear = parseInt(fromYearElement.options[fromYearElement.selectedIndex].text);

            var errorElement = document.getElementById("DateRangeError");
        
            if (fromYear > toYear) {
                errorElement.style.display = 'block';
                return false;
            }

            if ((fromYear == toYear) && (fromMonth > toMonth)) {
                errorElement.style.display = 'block';
                return false;
            }

            errorElement.style.display = 'none';
        
            if (elementRef.id == "<%= inventoryTypes.ClientID %>") {
                <%= ClientScript.GetPostBackEventReference(inventoryTypes, string.Empty) %> ;
            }
         
            if (elementRef.id == "<%= sites.ClientID %>") {
                <%= ClientScript.GetPostBackEventReference(sites, string.Empty) %> ;
            }
         
            if (elementRef.id == "<%= dealers.ClientID %>") {
                <%= ClientScript.GetPostBackEventReference(dealers, string.Empty) %> ;
            }
        
            return true;
        };
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            <% if(!IsPostBack)
               { %>
                // Kick off the report data load...
                __doPostBack( '<%= TriggerLoadReportData.UniqueID %>', '' );
            <% } %>
        } );
    </script>
</asp:Content>
