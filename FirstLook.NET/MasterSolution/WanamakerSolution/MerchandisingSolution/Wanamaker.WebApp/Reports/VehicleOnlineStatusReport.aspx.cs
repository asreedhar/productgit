﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.Membership;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.MaxAnalytics;
using FirstLook.Merchandising.DomainModel.Postings;
using FirstLook.Merchandising.DomainModel.Reports.VehiclesOnline;
using Microsoft.Reporting.WebForms;
using MvcMiniProfiler;
using FirstLook.Merchandising.DomainModel.Commands;

namespace Wanamaker.WebApp.Reports
{
    public partial class VehicleOnlineStatusReport : ReportBasePage
    {
        //Injected
        private IMaxAnalytics MaxAnalytics { get; set; }

        private IMemberContext MemberContext { get; set; }
        private ReportModel ReportModel { get; set; }
        private IPhotoServices PhotoServices { get; set; }
        
        public VehicleOnlineStatusReport()
        {
            PhotoServices = Registry.Resolve<IPhotoServices>();
            MemberContext = Registry.Resolve<IMemberContext>();
            MaxAnalytics = Registry.Resolve<IMaxAnalytics>();

            ReportModel = new ReportModel(MemberContext, PhotoServices, MaxAnalytics);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                InitialPageLoad();
        }

        private void InitialPageLoad()
        {
            BindDealers();

            SetInventoryTypeFilterFromQueryString();

            viewer.LocalReport.ReportPath = @"Reports\VehicleOnlineStatusReport.rdlc";
            viewer.LocalReport.EnableHyperlinks = true;
            viewer.LocalReport.EnableExternalImages = true;
            
            ApplyDealerPreferences();
        }

        private void SetInventoryTypeFilterFromQueryString()
        {
            if (!Request.QueryString.AllKeys.Contains("usednew")) return;

            object usedNewFilter = Request.QueryString["usednew"];
            if (usedNewFilter == null) return;
            if (inventoryTypes.Items.FindByValue(usedNewFilter.ToString()) == null) return;
            inventoryTypes.SelectedValue = usedNewFilter.ToString();
        }

        private void BindDealers()
        {
            using (MiniProfiler.Current.Step("GetBusinessUnitList()"))
                dealers.DataSource = ReportModel.GetBusinessUnitList().ToList();
            dealers.DataTextField = "Name";
            dealers.DataValueField = "BusinessUnitID";
            dealers.DataBind();

            SetDealerComboToCurrentBusinessUnit();
        }

        private void SetDealerComboToCurrentBusinessUnit()
        {
            var bu = (BusinessUnit)Context.Items["BusinessUnit"];
            CurrentBusinessUnitId = bu == null ? (int?)null : bu.Id;
        }

        private int? CurrentBusinessUnitId
        {
            get
            {
                int v;
                return !int.TryParse(dealers.SelectedValue, out v) ? (int?)null : v;
            }
            set
            {
                if (value.HasValue)
                    dealers.SelectedValue = value.Value.ToString(CultureInfo.InvariantCulture);
                else
                    dealers.SelectedIndex = 0;
            }
        }


        protected void RebindReport(object sender, EventArgs e)
        {
            BindReport();
        }

        protected void OnLoadReportData(object sender, EventArgs e)
        {
            BindReport();
        }

        private void BindReportWithNoData()
        {
            BindReportData(new VehicleOnlineBinder[0].ToList());
        }

        private void BindReport()
        {
            viewer.Visible = true;
            viewer_spinner.Visible = false;

            var businessUnitId = CurrentBusinessUnitId;
            if (businessUnitId.HasValue)
            {
                BindReportData(ReportModel.GetVehiclesOnline(businessUnitId.Value, GetInventoryTypeFilter(), GetMinAgeFilter()));
            }
            else
            {
                BindReportWithNoData();
            }
        }

        private void BindReportData(IEnumerable<VehicleOnlineBinder> data)
        {
            var vehicleOnlineBinders = data as VehicleOnlineBinder[] ?? data.ToArray();

            var dataFilteredForGrid = ReportModel.FilterGridData(vehicleOnlineBinders, GetStatusFilter(), GetTextFilter());

            BindReportDataToViewer(vehicleOnlineBinders, "DataNoStatusFilter");
            BindReportDataToViewer(dataFilteredForGrid, "VehicleOnlineStatus");
        }
                
        private int? GetInventoryTypeFilter()
        {
            int inventoryTypeVal;
            return
                int.TryParse(inventoryTypes.SelectedValue, out inventoryTypeVal) && inventoryTypeVal != 0
                    ? inventoryTypeVal
                    : (int?)null;
        }

        private string GetTextFilter()
        {
            return string.IsNullOrWhiteSpace(InventorySearch.SearchText)
                       ? null
                       : InventorySearch.SearchText;
        }

        private int? GetMinAgeFilter()
        {
            int minAgeVal;
            return int.TryParse(ageInDays.SelectedValue, out minAgeVal) ? minAgeVal : (int?)null;
        }

        private OnlineStatus GetStatusFilter()
        {
            int statusVal;
            return int.TryParse(status.SelectedValue, out statusVal)
                       ? (OnlineStatus) statusVal
                       : OnlineStatus.All;
        }

        private void BindReportDataToViewer(IEnumerable<VehicleOnlineBinder> reportData, string dataSourceName)
        {
            var dataSource = viewer.LocalReport.DataSources.SingleOrDefault(rds => rds.Name == dataSourceName);
            if (dataSource == null)
            {
                viewer.LocalReport.DataSources.Add(
                    new ReportDataSource(dataSourceName, reportData));
            }
            else
            {
                dataSource.Value = reportData;
            }
            viewer.DataBind();
        }

        protected void dealers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ApplyDealerPreferences();
            RebindReport(sender, e);
        }
                        
        private void ApplyDealerPreferences()
        {
            int? buid = CurrentBusinessUnitId;
            if (buid.HasValue)
            {
                var settings = new GetReportSettings(buid.Value);
                AbstractCommand.DoRun(settings);
                ageInDays.SelectedValue = settings.VehicleOnlineStatusReport_MinAgeDays.ToString(CultureInfo.InvariantCulture);
            }
        }
                
    }
}
