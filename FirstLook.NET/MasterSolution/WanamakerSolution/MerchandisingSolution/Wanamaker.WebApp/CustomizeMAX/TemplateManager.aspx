<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/Workflow/Workflow.Master" Theme="None" Title="Template Manager | MAX : Online Inventory. Perfected."
    CodeBehind="TemplateManager.aspx.cs" Inherits="Wanamaker.WebApp.CustomizeMAX.TemplateManager" ValidateRequest="false"
    EnableEventValidation="false" ClientIdMode="AutoID" %>
<%@ Import Namespace="Wanamaker.WebApp.Helpers" %>

<%@ Register TagPrefix="carbuilder" TagName="EditSnippetIntro" Src="~/CustomizeMAX/Controls/EditSnippetIntro.ascx" %>
<%@ Register TagPrefix="carbuilder" TagName="SnippetCreator" Src="~/CustomizeMAX/Controls/SnippetCreator.ascx" %>
<%@ Register TagPrefix="wanaworkflow" TagName="WorkflowHeader" Src="~/Workflow/Controls/WorkflowHeader.ascx" %>
<%@ Register TagPrefix="workflow" TagName="InventorySearch" Src="~/Workflow/Controls/SearchInventory.ascx"  %>

<asp:Content ID="CssContent" ContentPlaceHolderID="CssPlaceHolder" runat="server">

    <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Main.debug.css") %>" />
    <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Settings/templateManager.css") %>" />

</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="BodyPlaceHolder" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <div id="BulkUpload">
        <a href="<%= BulkUploadUrl %>" title="Upload photos for multiple vehicles" target="_blank">Bulk Upload</a>
    </div>

    <workflow:InventorySearch id="InventorySearch" runat="server"></workflow:InventorySearch>

    <h1>Template Manager</h1>

    <div id="Content" class="mainTemplateBox" runat="server">
        <div class="AvailTemplatesForBuilding">
            <div class="bodyTemplates">
                Active Templates:
                <asp:DropDownList ID="activeTemplatesDDL" runat="server" DataTextField="Value" DataValueField="Key" DataSourceID="TemplateDS"
                    OnDataBound="TemplateDDL1_Bound">
                </asp:DropDownList>
                <asp:Button ID="Go1" runat="server" CssClass="button small" Text="Edit" OnClick="Edit1_Click" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Inactive Templates:
                <asp:DropDownList ID="inactiveTemplatesDDL" runat="server" DataTextField="Value" DataValueField="Key" DataSourceID="InactiveDS">
                </asp:DropDownList>
                <asp:Button ID="Go2" runat="server" CssClass="button small" Text="Edit" OnClick="Edit2_Click" />
            </div>
        </div>
        <div class="templateContent">
            <div class="toolkitExt">
                <h4>
                    Add Phrases to Template</h4>
                <div class="toolkit">
                    <asp:Repeater runat="server" DataSourceID="BlurbXML">

                        <HeaderTemplate><ul></HeaderTemplate>

                        <ItemTemplate>
                            <li><h2><%# Eval("blurbTypeName") %></h2>
                                
                                <asp:Repeater runat="server" DataSource='<%# XPathSelect("./BlurbTemplate") %>' >

                                    <HeaderTemplate><ul></HeaderTemplate>

                                    <ItemTemplate>
                                        <li style='<%# IsBlurbVisible(XPathSelect(".")) ? "" : "display: none" %>'>
                                        <asp:LinkButton runat="server" CommandArgument='<%# XPath("./@blurbId") %>' OnClick="Snippet_Clicked">
                                            <%# XPath("./@blurbName") %>
                                        </asp:LinkButton>
                                        </li>
                                    </ItemTemplate>

                                    <FooterTemplate></ul></FooterTemplate>

                                </asp:Repeater>

                            </li>
                        </ItemTemplate>
                       
                        <FooterTemplate></ul></FooterTemplate>

                    </asp:Repeater>
                    
                </div>
                <asp:Button ID="NewSnippet" runat="server" Text="Create New Snippet"></asp:Button>
                <asp:HyperLink ID="lnk" runat="server" Text="Customize Phrases" NavigateUrl="~/CustomizeMAX/CustomizeMax.aspx"></asp:HyperLink>
                <asp:Panel ID="SnipCreator" runat="server" CssClass="SnipCreator" Style="display: none">
                    <asp:ImageButton ID="cancelCreate" runat="server" CssClass="deleteLink" />
                    <p>
                        To create a new snippet, enter a title, select the proper category, specify when the snippet should be used, and add items
                        to the template body.</p>
                    <asp:UpdatePanel ID="Update3" runat="server">
                        <ContentTemplate>
                            <carbuilder:SnippetCreator ID="snip" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                <ajax:modalpopupextender id="ModalPopupExtender4" runat="server" popupcontrolid="SnipCreator" targetcontrolid="NewSnippet"
                    cancelcontrolid="cancelCreate" backgroundcssclass="modal">
                </ajax:modalpopupextender>
            </div>
            <asp:UpdatePanel ID="ReorderItemsPanel" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <div class="templateForm">
                        <asp:CheckBox CssClass="righty" ID="ActiveCB" runat="server" Text="Active?" />
                        <ul class="clearfix templateHead">
                            <li><span>Template Name:</span>
                                <asp:TextBox ID="TemplateName" runat="server" class="text"></asp:TextBox>
                            </li>
                            <li><span>Vehicle Profile:</span>
                                <asp:DropDownList ID="VehicleProfileDDL" runat="server" OnDataBound="ProfileDDL_Bound" DataSourceID="VehicleProfileDS" DataTextField="title"
                                    DataValueField="vehicleProfileId">
                                </asp:DropDownList>
                            </li>
                        </ul>
                        <div class="reorderableCategories">
                            <ajax:reorderlist id="ReorderList1" runat="server" postbackonreorder="false" datasourceid="ReorderTemplateDS" callbackcssstyle="callbackStyle"
                                draghandlealignment="Left" iteminsertlocation="Beginning" datakeyfield="BlurbID" sortorderfield="Sequence" ondatabound="ReorderList_Bound">
                    <EmptyListTemplate>
                    <h3>Please select a template to edit...</h3>
                    </EmptyListTemplate>
                    <ItemTemplate>
                        <div class="itemArea">
                            <asp:ImageButton ID="removeIt" runat="server" CommandArgument='<%# Eval("BlurbID") %>' CssClass="deleteLink" OnClick="RemoveTemplateItem" />
                            <b><%# Eval("BlurbName") %></b>
                                                     
                        </div>
                    </ItemTemplate>
                        <ReorderTemplate>
                        <asp:Panel ID="Panel3" runat="server" CssClass="reorderCue" />
                    </ReorderTemplate>
                    <DragHandleTemplate>
                        <div class="dragHandle"></div>
                    </DragHandleTemplate>
                        
                    </ajax:reorderlist>
                        </div>
                        <div>
                            <a style="clear: left; float: right;" href="#" onclick="$( '#importance' ).toggle();return false;">advanced (edit importance)...</a>
                        </div>
                        <div id="importance" style="display: none; border-top: solid 1px #eee;" class="reorderableCategories">
                            <h2>
                                Importance:</h2>
                            <p>
                                This determines which pieces stay in the ad if we run out of space</p>
                            <ajax:reorderlist id="ReorderList2" runat="server" postbackonreorder="false" datasourceid="ReorderPriorityDS" callbackcssstyle="callbackStyle"
                                draghandlealignment="Left" iteminsertlocation="Beginning" datakeyfield="BlurbID" sortorderfield="Priority">
                        
                    <ItemTemplate>
                        <div class="itemArea">
                            <%# Eval("BlurbName") %>
                        </div>
                    </ItemTemplate>
                        <ReorderTemplate>
                        <asp:Panel ID="Panel2" runat="server" CssClass="reorderCue" />
                    </ReorderTemplate>
                    <DragHandleTemplate>
                        <div class="dragHandle"></div>
                    </DragHandleTemplate>
                        
    </ajax:reorderlist>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div>
                <ul class="butList">
                    <li>
                        <asp:Button ID="update" runat="server" CssClass="button" Text="Save" OnClick="UpdateTemplate_Click" /></li>
                    <li>
                        <asp:Button ID="but1" runat="server" CssClass="button" Text="Save As..." />
                    </li>
                    <li>
                        <asp:Button ID="CreateNewTemplate" CssClass="button" runat="server" Text="New" />
                    </li>
                </ul>
                <asp:Panel ID="CopyForm" runat="server" CssClass="modalBod" Style="display: none">
                    New Template's Nickname:
                    <asp:TextBox ID="NewTemplateCopyNameTB" runat="server"></asp:TextBox>
                    <asp:Button ID="doCopyBtn" runat="server" Text="save" OnClick="CopyTemplate_Click" />
                    <asp:Button ID="cancelCopyBtn" runat="server" Text="cancel" />
                </asp:Panel>
                <ajax:modalpopupextender id="ModalPopupExtender1" runat="server" popupcontrolid="CopyForm" targetcontrolid="but1" cancelcontrolid="cancelCopyBtn"
                    backgroundcssclass="modal">
</ajax:modalpopupextender>
                <asp:Panel ID="NewForm" runat="server" CssClass="modalBod" Style="display: none">
                    New Template's Nickname:
                    <asp:TextBox ID="NewNameTB" runat="server" Text=""></asp:TextBox>
                    <asp:Button ID="Button1" runat="server" Text="create" OnClick="CreateNew_Click" />
                    <asp:Button ID="cancelCreateBtn" runat="server" Text="cancel" />
                </asp:Panel>
                <ajax:modalpopupextender id="ModalPopupExtender2" runat="server" popupcontrolid="NewForm" targetcontrolid="CreateNewTemplate"
                    cancelcontrolid="cancelCreateBtn" backgroundcssclass="modal">
</ajax:modalpopupextender>
            </div>
            <asp:HiddenField ID="busID" runat="server" Value="100150" />
            <asp:HiddenField ID="SelectedTemplateId" runat="server" Value="" />
            <div style="clear: left">
            </div>
        </div>
        <div style="float: right; padding-top: 20px;">
            Example Vehicle:
            <asp:DropDownList ID="SelectedVehicle" runat="server" DataSourceID="InventoryDS" DataTextField="IdentifierText" DataValueField="InventoryId">
            </asp:DropDownList>
            <asp:CheckBox ID="Long" runat="server" Text="Go Long" />
            <asp:Button ID="refreshVehicle" runat="server" Text="Preview" OnClick="NewVehicle_Click" />
        </div>
        <div class="sampleDescription">
            <asp:ObjectDataSource ID="InventoryDS" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.Inventory.InventoryDataSource"
                SelectMethod="FetchList" OnSelecting="Inventory_Selecting">
                <SelectParameters>
                    <asp:Parameter Name="businessUnitId" Type="Int32" />
                    <asp:Parameter Name="usedNewFilter" Type="Int32" DefaultValue="2" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:Label ID="descriptionLB" runat="server"></asp:Label>
        </div>
    </div>
    <asp:XmlDataSource ID="BlurbXML" runat="server" OnLoad="BlurbXml_Load" EnableCaching="false" XPath="ToolBox/Category"></asp:XmlDataSource>
    <asp:ObjectDataSource runat="server" ID="TemplateDS" TypeName="FirstLook.Merchandising.DomainModel.Templating.BlurbDataSource"
        SelectMethod="AvailableTemplatesSelect" OnSelecting="Templates_Selecting">
        <SelectParameters>
            <asp:Parameter Name="businessUnitId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource runat="server" ID="InactiveDS" TypeName="FirstLook.Merchandising.DomainModel.Templating.BlurbDataSource"
        SelectMethod="InactiveTemplatesSelect" OnSelecting="Templates_Selecting">
        <SelectParameters>
            <asp:Parameter Name="businessUnitId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ReorderTemplateDS" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Templating.BlurbDataSource"
        SelectMethod="TemplateItemsSelect" UpdateMethod="UpdateSequence" OnUpdating="TemplateContents_Updating">
        <SelectParameters>
            <asp:ControlParameter Name="templateID" ControlID="SelectedTemplateId" PropertyName="Value" />
            <asp:ControlParameter Name="businessUnitID" ControlID="busID" Type="Int32" DefaultValue="100150" />
            <asp:Parameter Name="orderByPriority" Type="Boolean" DefaultValue="false" />
        </SelectParameters>
        <UpdateParameters>
            <asp:ControlParameter Name="templateID" ControlID="SelectedTemplateId" PropertyName="Value" />
            <asp:Parameter Name="blurbId" Type="Int32" />
            <asp:Parameter Name="sequence" Type="Int32" />
            <asp:Parameter Name="priority" Type="Int32" />
            <asp:Parameter Name="businessUnitId" Type="Int32" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="templateContentsDS" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Templating.BlurbDataSource"
        SelectMethod="TemplateSelect">
        <SelectParameters>
            <asp:ControlParameter Name="templateID" ControlID="SelectedTemplateId" PropertyName="Value" />
            <asp:ControlParameter Name="businessUnitID" ControlID="busID" Type="Int32" DefaultValue="100150" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ReorderPriorityDS" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Templating.BlurbDataSource"
        SelectMethod="TemplateItemsSelect" UpdateMethod="UpdateSequence" OnUpdating="TemplateContents_Updating">
        <SelectParameters>
            <asp:ControlParameter Name="templateID" ControlID="SelectedTemplateId" PropertyName="Value" />
            <asp:ControlParameter Name="businessUnitID" ControlID="busID" Type="Int32" DefaultValue="100150" />
            <asp:Parameter Name="orderByPriority" Type="Boolean" DefaultValue="true" />
        </SelectParameters>
        <UpdateParameters>
            <asp:ControlParameter Name="templateID" ControlID="SelectedTemplateId" PropertyName="Value" />
            <asp:Parameter Name="blurbId" Type="Int32" />
            <asp:Parameter Name="sequence" Type="Int32" />
            <asp:Parameter Name="priority" Type="Int32" />
            <asp:Parameter Name="businessUnitId" Type="Int32" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    <asp:SqlDataSource ID="VehicleProfileDS" runat="server" ConnectionString='<%$ ConnectionStrings:Merchandising %>' SelectCommand="Select vehicleProfileId, [title] FROM templates.vehicleProfiles WHERE businessUnitId = @BusinessUnitId OR businessUnitId = 100150">
        <SelectParameters>
            <asp:ControlParameter ControlID="BusId" Name="BusinessUnitId" DefaultValue="100150" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
<asp:Content ID="ScriptContent" ContentPlaceHolderID="ScriptsPlaceHolder" runat="server">
    <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/MAX.Main.js") %>"></script>
</asp:Content>
