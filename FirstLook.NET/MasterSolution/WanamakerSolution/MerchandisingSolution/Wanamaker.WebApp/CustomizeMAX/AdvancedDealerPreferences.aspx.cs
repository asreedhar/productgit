using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Alerts;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Vehicles;
using VehicleDataAccess;
using Wanamaker.WebApp.AppCode.AccessControl;
using TabPanel = FirstLook.Common.WebControls.UI.TabPanel;
using FirstLook.Merchandising.DomainModel;


namespace Wanamaker.WebApp.CustomizeMAX
{
    public partial class AdvancedDealerPreferences : BasePage
    {
        public int BusinessUnitID
        {
            get
            {
                object o = ViewState["BusinessUnitID"];
                return (o == null) ? -1 : (int)o;
            }
            set
            {
                ViewState["BusinessUnitID"] = value;
            }
        }
        public bool HasJDPowerUpgrade
        {
            get
            {
                object o = ViewState["HasJDPowerUpgrade"];

                bool hasJDPowerUpgrade = false;
                if (o == null)
                {
                    hasJDPowerUpgrade = false;
                }
                else
                {
                    try
                    {
                        hasJDPowerUpgrade = Convert.ToBoolean(o);
                    }
                    catch
                    {
                        hasJDPowerUpgrade = false;
                    }
                }

                return hasJDPowerUpgrade;
            }
            set
            {
                ViewState["HasJDPowerUpgrade"] = value;
            }
        }

        private DropDownList lotProviderDDL
        {
            get
            {
                DropDownList ddl = DataSourceDetailsView.FindControl("lotProviderDDL") as DropDownList;
                if (ddl == null) throw new ApplicationException("LotProvider DDL Not Found");
                return ddl;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BusinessUnit bu = Context.Items["BusinessUnit"] as BusinessUnit;
                if (bu == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                else
                {
                    HasJDPowerUpgrade = bu.HasDealerUpgrade(Upgrade.JDPowerUsedCarMarketData);
                }
                BusinessUnitID = bu.Id;

                CheckQueryString();
            }
        }

        private void CheckQueryString()
        {
            ProcessInitialTabQueryStringParameter(Request.QueryString["initialTab"]);
        }

        private void ProcessInitialTabQueryStringParameter(string initialTab)
        {
            TabPanel tab;

            switch ((initialTab ?? "").ToLowerInvariant())
            {
                case "equipmentgroups":
                    tab = TabPanel1;
                    break;
                case "adsettings":
                    tab = AdPreferences;
                    break;
                case "alerting":
                    tab = AlertingTab;
                    break;
                case "datasourcesettings":
                    tab = Merchandising;
                    break;
                default:
                    return;
            }

            for (var index = 0; index < advancedTabSet.Tabs.Count; ++index)
            {
                if (ReferenceEquals(tab, advancedTabSet.Tabs[index]))
                {
                    advancedTabSet.ActiveTabIndex = index;
                    break;
                }
            }
        }

        protected void Edt_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@BusinessUnitId"].Value = BusinessUnitID;
        }

        protected void AdPref_Bound(object sender, EventArgs e)
        {
            BookType book;
            if (PrefsDV.CurrentMode == DetailsViewMode.Edit)
            {
                var ddlJdPower = PrefsDV.FindControl("ddlMinValidJdPowerRating") as DropDownList;
                if (ddlJdPower != null)
                {
                    ddlJdPower.SelectedValue = String.Format("{0:F1}", ((DealerAdvertisementPreferences)PrefsDV.DataItem).MinValidJdPowerRating);
                }

                var bookDDL = PrefsDV.FindControl("PreferredBookCategory") as DropDownList;
                if (bookDDL != null)
                {
                    bookDDL.DataSource = Bookout.FetchValidDisplayCategories();
                    bookDDL.DataTextField = "Key";
                    bookDDL.DataValueField = "Value";
                    bookDDL.DataBind();

                    bookDDL.SelectedValue = ((DealerAdvertisementPreferences)PrefsDV.DataItem).PrefferedBookoutCategory.ToString();
                }

                var ddl = PrefsDV.FindControl("defaultTemplateDDL") as DropDownList;
                if (ddl != null)
                {
                    int id = (int)PrefsDV.DataKey["DefaultTemplateId"];
                    if (ddl.Items.FindByValue(id.ToString()) != null)
                    {
                        ddl.SelectedValue = id.ToString();
                    }
                }

                var thresholdDdl = PrefsDV.FindControl("ddlThresholdColor") as DropDownList;
                if (thresholdDdl != null)
                {
                    foreach (int val in Enum.GetValues(typeof(ThresholdColors)))
                    {

                        thresholdDdl.Items.Add(new ListItem(((ThresholdColors)val).DescriptionAttr()));
                    }
                    ThresholdColors colors = (ThresholdColors)((DealerAdvertisementPreferences)PrefsDV.DataItem).ThresholdColor;
                    thresholdDdl.SelectedValue = colors.DescriptionAttr();
                }

                var kbbDdl = PrefsDV.FindControl("ddlKbb") as DropDownList;
                if (kbbDdl != null)
                {
                    foreach (int val in Enum.GetValues(typeof(BookType)))
                    {
                        kbbDdl.Items.Add(new ListItem(((BookType)val).DescriptionAttr()));
                    }

                    book = (BookType)((DealerAdvertisementPreferences)PrefsDV.DataItem).Kbb;
                    kbbDdl.SelectedValue = book.DescriptionAttr();
                }

                var nadaDdl = PrefsDV.FindControl("ddlNada") as DropDownList;
                if (nadaDdl != null)
                {
                    foreach (int val in Enum.GetValues(typeof(BookType)))
                    {
                        nadaDdl.Items.Add(new ListItem(((BookType)val).DescriptionAttr()));
                    }

                    book = (BookType)((DealerAdvertisementPreferences)PrefsDV.DataItem).Nada;
                    nadaDdl.SelectedValue = book.DescriptionAttr();
                }

                var marketAvgDdl = PrefsDV.FindControl("ddlMarketAverage") as DropDownList;
                if (marketAvgDdl != null)
                {
                    foreach (int val in Enum.GetValues(typeof(BookType)))
                    {
                        marketAvgDdl.Items.Add(new ListItem(((BookType)val).DescriptionAttr()));
                    }

                    BookType bookMktAvg = (BookType)((DealerAdvertisementPreferences)PrefsDV.DataItem).MarketAverage;
                    marketAvgDdl.SelectedValue = bookMktAvg.DescriptionAttr();
                }

                var msrpDdl = PrefsDV.FindControl("ddlMsrp") as DropDownList;
                if (msrpDdl != null)
                {

                    foreach (int val in Enum.GetValues(typeof(BookType)))
                    {
                        msrpDdl.Items.Add(new ListItem(((BookType)val).DescriptionAttr()));
                    }

                    BookType bookMsrp = (BookType)((DealerAdvertisementPreferences)PrefsDV.DataItem).Msrp;
                    msrpDdl.SelectedValue = bookMsrp.DescriptionAttr();
                }

                var edmundsDdl = PrefsDV.FindControl("ddlEdmunds") as DropDownList;
                if (edmundsDdl != null)
                {
                    foreach (int val in Enum.GetValues(typeof(BookType)))
                    {
                        edmundsDdl.Items.Add(new ListItem(((BookType)val).DescriptionAttr()));
                    }

                    BookType bookEdmunds = (BookType)((DealerAdvertisementPreferences)PrefsDV.DataItem).Edmunds;
                    edmundsDdl.SelectedValue = bookEdmunds.DescriptionAttr();
                }
            }
        }

        protected void Preferences_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["businessUnitId"] = BusinessUnitID;
        }




        protected void DataSourceDetailsView_Bound(object sender, EventArgs e)
        {
            DropDownList ddl = lotProviderDDL;

            string lotProvID = ((DataRowView)DataSourceDetailsView.DataItem)["lotProviderId"].ToString();
            if (ddl.Items.FindByValue(lotProvID) == null)
            {
                ddl.SelectedValue = "";
            }
            else
            {
                ddl.SelectedValue = lotProvID;
            }
        }
        protected void PreferencesUpdating(object sender, ObjectDataSourceMethodEventArgs e)
        {
            int[] arr = new int[5];
            DealerAdvertisementPreferences prefs = e.InputParameters["preferences"] as DealerAdvertisementPreferences;
            if (prefs == null) return;

            prefs.BusinessUnitId = BusinessUnitID;

            TextBox t0 = PrefsDV.FindControl("Tier0Count") as TextBox;
            TextBox t1 = PrefsDV.FindControl("Tier1Count") as TextBox;
            TextBox t2 = PrefsDV.FindControl("Tier2Count") as TextBox;
            TextBox t3 = PrefsDV.FindControl("Tier3Count") as TextBox;
            DropDownList defTemplate = PrefsDV.FindControl("defaultTemplateDDL") as DropDownList;

            //all the tier vals default to zero
            if (t0 != null && !String.IsNullOrEmpty(t0.Text))
            {
                int maxOptionsByTierIndex0;
                if (int.TryParse(t0.Text, out maxOptionsByTierIndex0))
                {
                    prefs.MaxOptionsByTier[0] = maxOptionsByTierIndex0;
                }
            }
            if (t1 != null && !String.IsNullOrEmpty(t1.Text))
            {
                int maxOptionsByTierIndex1;
                if (int.TryParse(t1.Text, out maxOptionsByTierIndex1))
                {
                    prefs.MaxOptionsByTier[1] = maxOptionsByTierIndex1;
                }
            }
            if (t2 != null && !String.IsNullOrEmpty(t2.Text))
            {
                int maxOptionsByTierIndex2;
                if (int.TryParse(t2.Text, out maxOptionsByTierIndex2))
                {
                    prefs.MaxOptionsByTier[2] = maxOptionsByTierIndex2;
                }
            }
            if (t3 != null && !String.IsNullOrEmpty(t3.Text))
            {
                int maxOptionsByTierIndex3;
                if (int.TryParse(t3.Text, out maxOptionsByTierIndex3))
                {
                    prefs.MaxOptionsByTier[3] = maxOptionsByTierIndex3;
                }
            }
            if (defTemplate != null)
            {
                int defaultTemplateId;
                if (int.TryParse(defTemplate.SelectedValue, out defaultTemplateId))
                {
                    prefs.DefaultTemplateId = defaultTemplateId;
                }
            }

            prefs.GasMileageBySegment = new List<GasMileagePreference>();
            GridView gasGV = (GridView)PrefsDV.FindControl("GasMileages");
            foreach (GridViewRow gvr in gasGV.Rows)
            {
                DataKey dk = gasGV.DataKeys[gvr.DataItemIndex];
                int segmentId = (int)dk.Value;

                TextBox cityTB = gvr.FindControl("City") as TextBox;
                TextBox hwyTB = gvr.FindControl("Highway") as TextBox;
                int city = 0;
                int hwy = 0;
                if (cityTB != null) Int32.TryParse(cityTB.Text, out city);
                if (hwyTB != null) Int32.TryParse(hwyTB.Text, out hwy);

                prefs.GasMileageBySegment.Add(new GasMileagePreference(segmentId, "", city, hwy));
            }

            var ddlJdPower = PrefsDV.FindControl("ddlMinValidJdPowerRating") as DropDownList;
            if (ddlJdPower != null)
            {
                prefs.MinValidJdPowerRating = (float)Convert.ToDouble(ddlJdPower.SelectedValue);
            }

            var bookDDL = PrefsDV.FindControl("PreferredBookCategory") as DropDownList;
            if (bookDDL != null)
            {
                prefs.PrefferedBookoutCategory = (BookoutCategory)Enum.Parse(typeof(BookoutCategory), bookDDL.SelectedValue);
            }

            var thresholdDDl = PrefsDV.FindControl("ddlThresholdColor") as DropDownList;
            if (thresholdDDl != null)
            {
                prefs.ThresholdColor = (int)(Enum.GetValues(typeof(ThresholdColors))
                .Cast<ThresholdColors>()
                .FirstOrDefault(t => t.DescriptionAttr() == thresholdDDl.SelectedValue));
            }

            var kbbDDL = PrefsDV.FindControl("ddlKbb") as DropDownList;
            if (kbbDDL != null)
            {
                prefs.Kbb = (int)(Enum.GetValues(typeof(BookType))
                .Cast<BookType>()
                .FirstOrDefault(k => k.DescriptionAttr() == kbbDDL.SelectedValue));

            }

            var nadaDDL = PrefsDV.FindControl("ddlNada") as DropDownList;
            if (nadaDDL != null)
            {
                prefs.Nada = (int)(Enum.GetValues(typeof(BookType))
                .Cast<BookType>()
                .FirstOrDefault(n => n.DescriptionAttr() == nadaDDL.SelectedValue));

            }

            var mktAvgDDL = PrefsDV.FindControl("ddlMarketAverage") as DropDownList;
            if (mktAvgDDL != null)
            {
                prefs.MarketAverage = (int)(Enum.GetValues(typeof(BookType))
                .Cast<BookType>()
                .FirstOrDefault(m => m.DescriptionAttr() == mktAvgDDL.SelectedValue));

            }

            var msrpDDL = PrefsDV.FindControl("ddlMsrp") as DropDownList;
            if (msrpDDL != null)
            {
                prefs.Msrp = (int)(Enum.GetValues(typeof(BookType))
                .Cast<BookType>()
                .FirstOrDefault(ms => ms.DescriptionAttr() == msrpDDL.SelectedValue));

            }
            var edmundsDDL = PrefsDV.FindControl("ddlEdmunds") as DropDownList;
            if (edmundsDDL != null)
            {
                prefs.Edmunds = (int)(Enum.GetValues(typeof(BookType))
                .Cast<BookType>()
                .FirstOrDefault(ed => ed.DescriptionAttr() == edmundsDDL.SelectedValue));

            }

        }

        public static int GetTierCount(object evalObj, int tierNumber)
        {
            return ((int[])evalObj)[tierNumber];
        }
        public static int GetGasMileageBySegment(object evalObj, int segmentId)
        {
            return ((int[])evalObj)[segmentId];
        }

        protected void LotLocations_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@BusinessUnitId"].Value = BusinessUnitID;
        }


        protected void Alerting_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@BusinessUnitId"].Value = BusinessUnitID;
        }



        protected void Alerting_InsertUpdating(object sender, SqlDataSourceCommandEventArgs e)
        {
            e.Command.Parameters["@BusinessUnitId"].Value = BusinessUnitID;
        }


        protected void AlertRow_Bound(object sender, RepeaterItemEventArgs e)
        {
            DropDownList timeDDL = e.Item.FindControl("TimeDDL") as DropDownList;
            if (timeDDL == null) return;

            if (e.Item.ItemIndex != 5)
            {
                e.Item.Visible = false;
                return;
            }

            HiddenField reportID = e.Item.FindControl("reportIDField") as HiddenField;
            if (reportID == null) return;
            int reportId = Int32.Parse(reportID.Value);

            Subscription alerts = Subscription.Fetch(reportId, BusinessUnitID, IdentityHelper.GetFirstlookId());
            //loop over all the controls and only look at the checkboxes
            timeDDL.SelectedValue = alerts.TimeOfDay.ToString();
            foreach (Control ctl in e.Item.Controls)
            {
                CheckBoxPlus cbp = ctl as CheckBoxPlus;
                if (cbp == null) continue;

                cbp.Checked = alerts.SubscribesOnDay(Int32.Parse(cbp.CheckBoxArgument));
            }

            TextBox emailTB = e.Item.FindControl("email") as TextBox;
            if (emailTB == null) return;

            emailTB.Text = alerts.Email;
        }

        protected void Subscriptions_Save(object sender, EventArgs e)
        {
            int memberId = IdentityHelper.GetFirstlookId();
            string memberName = Context.User.Identity.Name;

            //first remove old subscriptions
            Subscription.DeleteSubscriptions(BusinessUnitID, memberId);

            foreach (RepeaterItem item in SubscriptionsTableRptr.Items)
            {
                if (!(item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)) continue;

                HiddenField reportID = item.FindControl("reportIDField") as HiddenField;
                if (reportID == null) continue;

                DropDownList theTime = item.FindControl("TimeDDL") as DropDownList;
                if (theTime == null) continue;

                TextBox emailTB = item.FindControl("email") as TextBox;
                if (emailTB == null) continue;

                int reportId = Int32.Parse(reportID.Value);

                //default to 4 if selected value is null/empty
                int timeVal = string.IsNullOrEmpty(theTime.SelectedValue) ? 4 : Int32.Parse(theTime.SelectedValue);

                //loop over all the controls and only look at the checkboxes
                foreach (Control ctl in item.Controls)
                {
                    CheckBoxPlus cbp = ctl as CheckBoxPlus;
                    if (cbp == null) continue;

                    if (cbp.Checked)
                    {
                        int day = Convert.ToInt32(cbp.CheckBoxArgument);
                        Subscription.AddSubscription(reportId, BusinessUnitID, memberId,
                            memberName, emailTB.Text, day, timeVal);
                    }
                }
            }
        }



        protected void Settings_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@BusinessUnitId"].Value = BusinessUnitID;
        }
        protected void Profiles_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@BusinessUnitId"].Value = BusinessUnitID;
        }

        protected void Settings_Updating(object sender, SqlDataSourceCommandEventArgs e)
        {
            e.Command.Parameters["@BusinessUnitId"].Value = BusinessUnitID;

            DropDownList ddl = lotProviderDDL;
            if (!string.IsNullOrEmpty(ddl.SelectedValue))
            {
                e.Command.Parameters["@LotProviderId"].Value = ddl.SelectedValue;
            }
        }

        protected void Profile_Bound(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;
            if (ddl != null)
            {
                ddl.Items.Insert(0, new ListItem("Any Vehicle...", ""));
            }
        }

        protected void Templates_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["BusinessUnitId"] = BusinessUnitID;
        }

        #region equipGroups

        protected void EquipGroups_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["BusinessUnitId"] = BusinessUnitID;
            e.InputParameters["generics"] = new GenericEquipmentCollection();
        }



        protected void GroupEdit_Bound(object sender, EventArgs e)
        {
            ListBox lb = GroupEditFV.FindControl("GroupItems") as ListBox;
            GenericEquipmentReplacement tmp = (GenericEquipmentReplacement)GroupEditFV.DataItem;
            if (lb != null && tmp != null)
            {
                foreach (ListItem li in lb.Items)
                {
                    li.Selected = tmp.IncludesCategories.Contains(int.Parse(li.Value));
                }

            }
        }

        protected void EquipGroup_Updating(object sender, FormViewUpdateEventArgs e)
        {
            ListBox lb = GroupEditFV.FindControl("GroupItems") as ListBox;

            //e.CommandArgument

            if (lb != null)
            {
                CategoryCollection validCategories = new CategoryCollection("included");
                foreach (ListItem li in lb.Items)
                {
                    if (li.Selected)
                    {
                        validCategories.Add(new CategoryLink(int.Parse(li.Value), string.Empty));
                    }
                }

                GenericEquipmentReplacementType replacementType =
                    (GenericEquipmentReplacementType)
                    Enum.Parse(typeof(GenericEquipmentReplacementType), e.NewValues["ReplacementType"].ToString());


                GenericEquipmentReplacement ger = new GenericEquipmentReplacement(
                    (int)GroupEditFV.DataKey.Value,
                    validCategories,
                    new CategoryCollection("excluded"),
                    (string)e.NewValues["Description"],
                    replacementType
                    );
                ger.UpdateOrOverride(BusinessUnitID, Context.User.Identity.Name);
                GroupsGV.DataBind();
                e.Cancel = true;

            }
        }

        protected void CreateGroup_Click(object sender, EventArgs e)
        {
            GroupEditFV.ChangeMode(FormViewMode.Insert);
            GroupsGV.SelectedIndex = -1;
        }

        protected void EquipGroup_Inserting(object sender, FormViewInsertEventArgs e)
        {
            ListBox lb = GroupEditFV.FindControl("GroupItems") as ListBox;
            if (lb != null)
            {
                List<int> validCategories = new List<int>();
                foreach (ListItem li in lb.Items)
                {
                    if (li.Selected)
                    {
                        validCategories.Add(int.Parse(li.Value));
                    }
                }
                GenericEquipmentReplacement.Create(BusinessUnitID,
                                                   (string)e.Values["Description"],
                                                   validCategories,
                                                   GenericEquipmentReplacementType.Dealer,
                                                   Context.User.Identity.Name);
                GroupsGV.DataBind();
                e.Cancel = true;
            }
        }

        protected void SelectedGroup_Changed(object sender, EventArgs e)
        {
            GroupEditFV.ChangeMode(FormViewMode.Edit);
            GroupEditFV.PageIndex = (int)GroupsGV.SelectedIndex;

        }


        protected void EquipGroup_Deleting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            e.InputParameters["businessUnitId"] = BusinessUnitID;
            e.InputParameters["memberLogin"] = Context.User.Identity.Name;
        }
        #endregion

        protected void PreviewPref_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["businessUnitId"] = BusinessUnitID;
        }

        protected void PreviewPref_Updating(object sender, ObjectDataSourceMethodEventArgs e)
        {
        }

    }
}