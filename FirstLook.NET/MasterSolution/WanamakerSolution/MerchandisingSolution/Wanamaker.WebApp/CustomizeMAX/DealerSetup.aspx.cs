using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Web.UI.WebControls;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Vehicles;

namespace Wanamaker.WebApp.CustomizeMAX
{

    public partial class DealerSetup : BasePage
    {
        public bool IsAdministrator;

        private int _businessUnitId;
        public int BusinessUnitID
        {
            get
            {
                if ( _businessUnitId <= 0 )
                {
                    BusinessUnit bu = Context.Items[ "BusinessUnit" ] as BusinessUnit;
                    if ( bu == null )
                    {
                        Response.Redirect( "~/Default.aspx" );
                    }
                    _businessUnitId = bu.Id;
                }
                return _businessUnitId;
            }
        }

        private void ConfigureSetupState ( bool isSetup )
        {
            if ( !isSetup )
            {
                setupStatusLbl.Text = "DEALER NOT SETUP!!!";
                setupStatusLbl.ForeColor = Color.Red;
                DealerSetupButton.Visible = true;
                setupWizard.Visible = false;
            }
            else
            {
                setupStatusLbl.Text = "Initial Dealer Setup OK - Proceed with Setup Wizard";
                setupStatusLbl.ForeColor = Color.Black;
                DealerSetupButton.Visible = false;
                setupWizard.Visible = true;
            }

        }

        protected void Page_Load ( object sender, EventArgs e )
        {

            IsAdministrator = User.IsInRole( "Administrator" );

            if ( !IsPostBack )
            {
                if ( IsAdministrator )
                {
                    DealerSetupStatus.Visible = true;
                    bool isSetup = DealerHelper.IsDealerSetup( BusinessUnitID );
                    ConfigureSetupState( isSetup );
                }

            }
        }

        protected void Preferences_Selecting ( object sender, ObjectDataSourceSelectingEventArgs e )
        {
            e.InputParameters[ "businessUnitId" ] = BusinessUnitID;
        }

        protected void NextWizardStep_Click ( object sender, WizardNavigationEventArgs e )
        {
            DealerAdvertisementPreferences prefs = DealerAdvertisementPreferences.Fetch( BusinessUnitID );

            if ( prefs == null ) return;

            //set the equipment importance for this dealer depending on the value in the slider chosen
            switch ( setupWizard.ActiveStepIndex )
            {
                case 0:
                    LotProviderDV.UpdateItem( false );
                    break;
                case 2:
                    //set crash test info
                    if ( crashLow.Checked )
                    {
                        prefs.MaxCrashTestRatingsToDisplay = 1;
                    }
                    else if ( crashMed.Checked )
                    {
                        prefs.MaxCrashTestRatingsToDisplay = 2;
                    }
                    else if ( crashHi.Checked )
                    {
                        prefs.MaxCrashTestRatingsToDisplay = 3;
                    }
                    else
                    {
                        prefs.MaxCrashTestRatingsToDisplay = 0;
                    }
                    prefs.GoodCrashTestRating = int.Parse( crashCount.Text );

                    //set warranty info
                    if ( warrLow.Checked )
                    {
                        prefs.MaxWarrantiesToDisplay = 1;
                    }
                    else if ( warrMed.Checked )
                    {
                        prefs.MaxWarrantiesToDisplay = 2;
                    }
                    else if ( warrHi.Checked )
                    {
                        prefs.MaxWarrantiesToDisplay = 3;
                    }
                    else
                    {
                        prefs.MaxWarrantiesToDisplay = 0;
                    }
                    prefs.GoodWarrantyMileageRemaining = int.Parse( warrMiles.Text );


                    prefs.GasMileageBySegment = new List<GasMileagePreference>();
                    foreach ( GridViewRow gvr in GasMileages.Rows )
                    {
                        DataKey dk = GasMileages.DataKeys[ gvr.DataItemIndex ];
                        int segmentId = ( int )dk.Value;

                        TextBox cityTB = gvr.FindControl( "City" ) as TextBox;
                        TextBox hwyTB = gvr.FindControl( "Highway" ) as TextBox;
                        int city = 0;
                        int hwy = 0;
                        if ( cityTB != null ) Int32.TryParse( cityTB.Text, out city );
                        if ( hwyTB != null ) Int32.TryParse( hwyTB.Text, out hwy );

                        prefs.GasMileageBySegment.Add( new GasMileagePreference( segmentId, "", city, hwy ) );



                    }

                    break;
                case 3:
                    prefs.PrefferedBookoutCategory = (BookoutCategory)Enum.Parse(typeof(BookoutCategory), PreferredBookCategory.SelectedValue);
                    prefs.GoodBookDifferential = int.Parse( goodBookDiffTB.Text );


                    break;
                case 5:
                    prefs.DefaultTemplateId = int.Parse( defaultTemplateDDL.SelectedValue );
                    prefs.TextSeparator = separatorTB.Text;

                    //set preview length
                    int previewLen = 150;
                    if ( preview250.Checked )
                    {
                        previewLen = 250;
                    }
                    else if ( previewCustom.Checked )
                    {
                        int.TryParse( CustomPreviewLen.Text, out previewLen );
                    }
                    prefs.DesiredPreviewLength = previewLen;
                    break;
            }

            DealerAdvertisementPreferences.UpdateOrCreate( prefs );

            if ( setupWizard.ActiveStepIndex == setupWizard.WizardSteps.Count - 1 ) Response.Redirect( "~/CustomizeMAX/DealerPreferences.aspx" );
        }

        private DealerAdvertisementPreferences _origPrefs;
        public DealerAdvertisementPreferences OrigPrefs
        {
            get { return _origPrefs ?? ( _origPrefs = DealerAdvertisementPreferences.Fetch( BusinessUnitID ) ); }
        }

        protected void Wizard_Load ( object sender, EventArgs e )
        {
            if ( !IsPostBack )
            {
                //gas mileages
                DealerAdvertisementPreferences prefs = OrigPrefs;
                GasMileages.DataSource = prefs.GasMileageBySegment;
                GasMileages.DataBind();

                //consumer info
                if ( prefs.MaxWarrantiesToDisplay >= 3 )
                {
                    warrHi.Checked = true;
                }
                else if ( prefs.MaxWarrantiesToDisplay == 1 )
                {
                    warrLow.Checked = true;
                }
                else if ( prefs.MaxWarrantiesToDisplay == 0 )
                {
                    warrNone.Checked = true;
                }
                else
                {
                    warrMed.Checked = true;
                }
                warrMiles.Text = prefs.GoodWarrantyMileageRemaining.ToString();

                if ( prefs.MaxCrashTestRatingsToDisplay >= 3 )
                {
                    crashHi.Checked = true;
                }
                else if ( prefs.MaxCrashTestRatingsToDisplay == 1 )
                {
                    crashLow.Checked = true;
                }
                else if ( prefs.MaxCrashTestRatingsToDisplay == 0 )
                {
                    crashNone.Checked = true;
                }
                else
                {
                    crashMed.Checked = true;
                }
                crashCount.Text = prefs.GoodCrashTestRating.ToString();

                //book info
                PreferredBookCategory.DataBind();
                string book = prefs.PrefferedBookoutCategory.ToString();
                ListItem li = PreferredBookCategory.Items.FindByValue( book );
                if ( li != null )
                {
                    PreferredBookCategory.SelectedValue = book;
                }
                goodBookDiffTB.Text = prefs.GoodBookDifferential.ToString();

                //set default template
                SetupDefaultTemplates();
                separatorTB.Text = prefs.TextSeparator;
                if ( prefs.DesiredPreviewLength == 150 )
                {
                    preview150.Checked = true;
                }
                else if ( prefs.DesiredPreviewLength == 250 )
                {
                    preview250.Checked = true;
                }
                else
                {
                    previewCustom.Checked = true;
                    CustomPreviewLen.Text = prefs.DesiredPreviewLength.ToString();
                }
            }
        }

        protected void InsertNew_Click ( object sender, EventArgs e )
        {
            Button send = sender as Button;
            if ( send != null )
            {
                switch ( send.CommandArgument )
                {
                    /*    case "Excellent":
                            ExcellentAdjectivesDS.Insert();
                            break;
                        case "Good":
                            GoodAdjectivesDS.Insert();
                            break;
                        case "Dependable":
                            DependableAdjectivesDS.Insert();
                            break;*/
                    case "AdditionalInfo":
                        AdditionalInfoDS.Insert();
                        break;
                    default:
                        throw new ApplicationException( "error in adjective insert script arg: " + send.CommandArgument );
                }
            }
        }

        protected void Additional_Inserting ( object sender, SqlDataSourceCommandEventArgs e )
        {

            if (string.IsNullOrEmpty(NewAdditionalBuilder.Text) || string.IsNullOrEmpty(NewAdditionalAd.Text))
            {
                e.Cancel = true;
                return;
            }

            e.Command.Parameters[ "@InfoBuilderDisplay" ].Value = NewAdditionalBuilder.Text;
            e.Command.Parameters[ "@InfoAdDisplay" ].Value = NewAdditionalAd.Text;
            e.Command.Parameters[ "@BusinessUnitId" ].Value = BusinessUnitID;
            e.Command.Parameters[ "@Priority" ].Value = 1;
        }

        protected void Additional_Selecting ( object sender, SqlDataSourceSelectingEventArgs e )
        {
            e.Command.Parameters[ "@BusinessUnitId" ].Value = BusinessUnitID;
        }

        protected void Additional_Updating ( object sender, SqlDataSourceCommandEventArgs e )
        {
            CheckBox cb = AdditionalInfoGV.Rows[ AdditionalInfoGV.EditIndex ].FindControl( "Enabled" ) as CheckBox;
            if ( cb == null ) return;

            e.Command.Parameters[ "@Enabled" ].Value = Convert.ToInt32( cb.Checked );
        }

        protected void Settings_Selecting ( object sender, SqlDataSourceSelectingEventArgs e )
        {
            e.Command.Parameters[ "@BusinessUnitId" ].Value = BusinessUnitID;
        }

        protected void Settings_Updating ( object sender, SqlDataSourceCommandEventArgs e )
        {
            e.Command.Parameters[ "@BusinessUnitId" ].Value = BusinessUnitID;

            DropDownList ddl = lotProviderDDL;
            if ( !string.IsNullOrEmpty( ddl.SelectedValue ) )
            {
                e.Command.Parameters[ "@LotProviderId" ].Value = ddl.SelectedValue;
            }

        }

        protected string GetProvider ( object evalObj )
        {
            if ( evalObj != DBNull.Value )
            {
                int providerId = ( int )evalObj;
                switch ( providerId )
                {
                    case 2:
                        return "Dealer Specialties";
                    case 5:
                        return "eBizAutos";
                    default:
                        return "Contact FirstLook";
                }
            }

            return "None";

        }
        private void SetupDefaultTemplates()
        {
            this.defaultTemplateDDL.Items.Clear();
            var availableTemplates = new BlurbDataSource().AvailableTemplatesSelect(BusinessUnitID);
            foreach (var templateId in availableTemplates.Keys)
            {
                defaultTemplateDDL.Items.Add(new ListItem(availableTemplates[templateId], templateId.ToString()));
            }
            var tempId = OrigPrefs.DefaultTemplateId.ToString();
            if (defaultTemplateDDL.Items.FindByValue(tempId) != null)
            {
                defaultTemplateDDL.SelectedValue = tempId;
            }

        }
        protected void SelectAdType_Click ( object sender, EventArgs e )
        {
            DealerAdvertisementPreferences prefs = DealerAdvertisementPreferences.Fetch( BusinessUnitID );
            Button btn = sender as Button;
            if ( btn == null ) throw new ApplicationException( "sender is no longer a Button" );

            if ( btn.CommandArgument == "0" && prefs.UseLongDescriptionAsDefault ) //if they asked for short and it is currently long
            {
                prefs.UseLongDescriptionAsDefault = false;
                DealerAdvertisementPreferences.UpdateOrCreate( prefs );
            }
            else if ( btn.CommandArgument == "1" && !prefs.UseLongDescriptionAsDefault ) //or vice versa
            {
                prefs.UseLongDescriptionAsDefault = true;
                DealerAdvertisementPreferences.UpdateOrCreate( prefs );
            }
            //need to set merchandising description to be long for or short form by default
            setupWizard.ActiveStepIndex = setupWizard.ActiveStepIndex + 1;
        }

        protected void Templates_Selecting ( object sender, ObjectDataSourceSelectingEventArgs e )
        {
            e.InputParameters[ "businessUnitId" ] = BusinessUnitID;
        }

        protected void LotProvider_Binding ( object sender, EventArgs e )
        {
            DropDownList ddl = lotProviderDDL;
            if ( ddl.Items.FindByValue( ddl.SelectedValue ) == null )
            {
                ddl.SelectedValue = "";
            }

        }

        private DropDownList lotProviderDDL
        {
            get
            {
                DropDownList ddl = LotProviderDV.FindControl( "lotProviderDDL" ) as DropDownList;
                if ( ddl == null ) throw new ApplicationException( "LotProvider DDL Not Found" );
                return ddl;
            }
        }

        protected void LotProviderDV_Bound ( object sender, EventArgs e )
        {

            DropDownList ddl = lotProviderDDL;

            string lotProvID = ( ( DataRowView )LotProviderDV.DataItem )[ "lotProviderId" ].ToString();
            ddl.SelectedValue = ddl.Items.FindByValue( lotProvID ) == null ? "" : lotProvID;
        }

        protected void DealerSetup_Click ( object sender, EventArgs e )
        {

            try
            {
                MerchandisingPreferences prefs = MerchandisingPreferences.Fetch( BusinessUnitID );
                //prefs exists, so don't override dealer setup
                this.Page.ClientScript.RegisterStartupScript( Page.GetType(), "alertDealer" + BusinessUnitID, "alert('dealer already setup');", true );
            }
            catch
            {
                this.Page.ClientScript.RegisterStartupScript( Page.GetType(), "alertDealerSuccess" + BusinessUnitID, "alert('dealer setup successful!');", true );
                MerchandisingPreferences.SetupDealer( BusinessUnitID );
                ConfigureSetupState( true );
            }
        }
    }
}