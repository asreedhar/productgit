<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="DealerPreferences.aspx.cs" MasterPageFile="~/Workflow/Workflow.Master"
    Theme="None" Title="Dealer Preferences | MAX : Online Inventory. Perfected." Inherits="Wanamaker.WebApp.CustomizeMAX.DealerPreferences" EnableEventValidation="false" %>

<%@ Import Namespace="Wanamaker.WebApp.Helpers" %>

<%@ Register TagPrefix="wanaworkflow" TagName="WorkflowHeader" Src="~/Workflow/Controls/WorkflowHeader.ascx" %>
<%@ Register TagPrefix="carbuilder" TagName="SpecialsEditor" Src="~/CustomizeMAX/Controls/SpecialsEditor.ascx" %>
<%@ Register TagPrefix="carbuilder" TagName="VehicleProfiler" Src="~/CustomizeMAX/Controls/VehicleProfiler.ascx" %>
<%@ Register TagPrefix="carbuilder" TagName="ConditionSettings" Src="~/CustomizeMAX/Controls/ConditionSettings.ascx" %>
<%@ Register TagPrefix="carbuilder" TagName="EditSnippetIntro" Src="~/CustomizeMAX/Controls/EditSnippetIntro.ascx" %>
<%@ Register TagPrefix="carbuilder" TagName="SiteCollectionSetup" Src="~/CustomizeMAX/Controls/SiteCollectionSetup.ascx" %>
<%@ Register TagPrefix="carbuilder" TagName="CampaignSettings" Src="~/CustomizeMAX/Controls/CampaignSettings.ascx" %>
<%@ Register TagPrefix="carbuilder" TagName="ReportSettings" Src="~/CustomizeMAX/Controls/ReportSettings.ascx" %>
<%@ Register TagPrefix="carbuilder" TagName="AutoApproveSettings " Src="~/CustomizeMAX/Controls/AutoApproveSettings.ascx" %>
<%@ Register TagPrefix="carbuilder" TagName="BucketAlerts" Src="~/CustomizeMAX/Controls/BucketAlerts.ascx" %>
<%@ Register TagPrefix="carbuilder" TagName="Analytics" Src="~/CustomizeMAX/Controls/Analytics.ascx" %>
<%@ Register TagPrefix="workflow" TagName="InventorySearch" Src="~/Workflow/Controls/SearchInventory.ascx" %>
<%@ Register TagPrefix="carbuilder" Namespace="Wanamaker.WebApp.CustomizeMAX.Controls" %>
<%@ Register tagprefix="carbuilder" src="~/CustomizeMAX/Controls/AutoWindowSticker.ascx" tagname="AutoWindowSticker" %>

<asp:Content ID="CssContent" ContentPlaceHolderID="CssPlaceHolder" runat="server">
    <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Main.debug.css") %>" />
    <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Settings/dealerpreferences.css") %>" />
</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="BodyPlaceHolder" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <div id="BulkUpload">
        <a href="<%= BulkUploadUrl %>" title="Upload photos for multiple vehicles" target="_blank">Bulk Upload</a>
    </div>

    <workflow:InventorySearch ID="InventorySearch" runat="server"></workflow:InventorySearch>
        
    <h1>Dealer Preferences</h1>

    <div class="preferencesBody">
        
        <cwc:TabContainer ID="preferencesTabs" runat="server" CssClass="block_tabs" ActiveTabIndex="0" OnActiveTabChanged="OnTabChanged">
            <cwc:TabPanel Text="Tagline" runat="server" ID="TaglineTab" OnClientClick="_gaq.push(['_trackEvent', 'Dealer Preferences', 'Click', 'Tagline Tab']);">
                <h2>
                    Taglines</h2>
                <carbuilder:EditSnippetIntro BlurbId="30" Title="Enter your taglines" ID="editTag" runat="server" />
            </cwc:TabPanel>
            <cwc:TabPanel ID="Specials" Text="Specials" runat="server" OnClientClick="_gaq.push(['_trackEvent', 'Dealer Preferences', 'Click', 'Specials Tab']);">
                <carbuilder:SpecialsEditor ID="specialsTool" runat="server" IsInCpoManagerMode="false" BusinessUnitID="<%# BusinessUnitID %>">
                </carbuilder:SpecialsEditor>
            </cwc:TabPanel>
            <cwc:TabPanel ID="VehicleTargets" runat="server" Text="Vehicle Profiles" OnClientClick="_gaq.push(['_trackEvent', 'Dealer Preferences', 'Click', 'Vehicle Profiles Tab']);">
                <h2>
                    Create and Edit Vehicle Profiles</h2>
                <carbuilder:VehicleProfiler ID="profiler" runat="server" OnVehicleProfile_Updated="VehicleProfileNames_Updated" />
            </cwc:TabPanel>
            <cwc:TabPanel CssClass="EquipmentDisplayPriority" ID="EquipmentDisplayPriority" runat="server" Text="Equipment Priority"
                OnClientClick="_gaq.push(['_trackEvent', 'Dealer Preferences', 'Click', 'Equipment Priority Tab']);">
                <h2>
                    Reorder Equipment Importance</h2>
                <p>
                    Drag the most important equipment at the top of this list - these will appear first in your advertisements online.</p>
                <asp:UpdatePanel ID="Updatepanel1" runat="server">
                    <ContentTemplate>
                        <div id="categoryList" class="reorderable">
                            <asp:DropDownList ID="TierDDL" runat="server" AutoPostBack="true">
                                <asp:ListItem Text="Tier1" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Tier2" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Tier3" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Tier4" Value="4"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:Button ID="ReorderButton" runat="server" Text="Save Equipment Display Descriptions" />
                            <br />
                            <br />
                            <span style="padding-left: 28px; font-weight: bold;">Equipment Name</span> <span style="margin-left: 288px; font-weight: bold;">
                                Equipment Display Description</span> <span style="margin-left: 142px; font-weight: bold;">Tier</span>
                            <ajax:reorderlist id="PriorityList" runat="server" postbackonreorder="false" datasourceid="PriorityDS" callbackcssstyle="callbackStyle"
                                draghandlealignment="Left" iteminsertlocation="Beginning" datakeyfield="rankingID" sortorderfield="displayPriority">
                                <ItemTemplate>
                                    <div class="itemArea">
                                        <vda:CheckBoxPlus CheckBoxArgument='<%# Eval("rankingId") %>' ID="sticky" runat="server" Checked='<%#Eval("isSticky") %>' OnCheckedChanged="StickyCB_Changed" Visible="false" />
                                        <asp:TextBox CssClass="redescribe" OnTextChanged="ReplaceText_Changed" AutoPostBack="false" ID="ReplaceText" runat="server" Text='<%# HttpUtility.HtmlEncode((string)Eval("displayDescription")) %>' ></asp:TextBox>
                                        <asp:Label ID="Label1" runat="server"
                                            Text='<%# HttpUtility.HtmlEncode(Convert.ToString(Eval("categoryDesc"))) %>' />
                                                        
                                    </div>
                                    <div class="itemActions">
                                        <asp:ImageButton ID="Up" runat="server" ImageUrl="~/Themes/MAX3.0/Settings/Images/promote.png" OnClick="UpTier_Click" ToolTip="Promote" CommandArgument='<%# Eval("rankingId") + "|" + Eval("displayPriority") %>' Visible='<%# TierDDL.SelectedValue != "1" %>' />
                                        <asp:ImageButton ID="Down" runat="server" ImageUrl="~/Themes/MAX3.0/Settings/Images/demote.png" OnClick="DownTier_Click" ToolTip="Demote" CommandArgument='<%# Eval("rankingId") + "|" + Eval("displayPriority") %>' Visible='<%# TierDDL.SelectedValue != "4" %>' />                            
                                    </div>
                                </ItemTemplate>
                                <ReorderTemplate>
                                    <asp:Panel ID="Panel2" runat="server" CssClass="reorderCue" />
                                </ReorderTemplate>
                                <DragHandleTemplate>
                                    <div class="dragHandle"></div>
                                </DragHandleTemplate>
                            </ajax:reorderlist>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </cwc:TabPanel>
            <cwc:TabPanel ID="CertifiedPreOwned" runat="server" Text="Certified" OnClientClick="_gaq.push(['_trackEvent', 'Dealer Preferences', 'Click', 'Certified Tab']);">
                <h2>Certified Pre-Owned Vehicle Settings</h2>
                
                <div>
                <asp:HyperLink ID="ManufacturerProgramsLink" runat="server" Target="_blank">Manufacturer Programs Management Page</asp:HyperLink>
                </div>
                
                <div>
                <asp:HyperLink ID="DealerProgramsLink" runat="server" Target="_blank">Dealer Programs Management Page</asp:HyperLink>
                </div>

            </cwc:TabPanel>
            <cwc:TabPanel ID="Campaigns" runat="server" Text="Campaigns" OnClientClick="_gaq.push(['_trackEvent', 'Dealer Preferences', 'Click', 'Campaigns Tab']);">
                <h2>
                    Create and Edit Campaign Actions</h2>
                <p>
                    Campaign actions allow you to schedule events based on the age of a vehicle. As it ages, new descriptions are automatically
                    drafted for the vehicle.
                </p>
                <div>
                    <carbuilder:CampaignSettings ID="campaign" runat="server" />
                </div>
            </cwc:TabPanel>
            <cwc:TabPanel ID="tabDisclaimer" runat="server" Text="Disclaimer" OnClientClick="_gaq.push(['_trackEvent', 'Dealer Preferences', 'Click', 'Disclaimer Tab']);">
                <h2>
                    Customize Your Optimized Ad Disclaimer</h2>
                <table id="DisclaimerTable">
                    <colgroup>
                        <col style="text-align: center" />
                        <col />
                    </colgroup>
                    <tbody>
                        <tr>
                            <td>
                                <asp:RadioButton ID="rdoDefault" runat="server" Checked="True" GroupName="disclaimer" />
                            </td>
                            <td>
                                <label for="<%= rdoDefault.ClientID %>">
                                    Use the default Disclaimer for your Optimized Ads</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RadioButton ID="rdoAppend" runat="server" GroupName="disclaimer" />
                            </td>
                            <td>
                                <label for="<%= rdoAppend.ClientID %>">
                                    Add custom text to the beginning of the default Disclaimer</label>
                                <div class="collapsiblePanel">
                                    <ajax:collapsiblepanelextender id="cpeAppend" runat="server" collapsecontrolid="rdoReplace" collapsedsize="0" expandcontrolid="rdoAppend"
                                        targetcontrolid="pnlAppend" scrollcontents="false" />
                                    <asp:Panel ID="pnlAppend" runat="server">
                                        <asp:TextBox ID="txtAppend" runat="server" TextMode="multiLine" Rows="3" CssClass="disclaimerTextBox" MaxLength="255"></asp:TextBox>
                                    </asp:Panel>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RadioButton ID="rdoReplace" runat="server" GroupName="disclaimer" />
                            </td>
                            <td>
                                <label for="<%= rdoReplace.ClientID %>">
                                    Use custom text as your Disclaimer</label>
                                <div class="collapsiblePanel">
                                    <ajax:collapsiblepanelextender id="cpeReplace" runat="server" collapsecontrolid="rdoAppend" collapsedsize="0" expandcontrolid="rdoReplace"
                                        targetcontrolid="pnlReplace" scrollcontents="false" />
                                    <asp:Panel ID="pnlReplace" runat="server">
                                        <asp:TextBox ID="txtReplace" runat="server" TextMode="multiLine" CssClass="disclaimerTextBox" MaxLength="255"></asp:TextBox>
                                    </asp:Panel>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="formButtons">
                    <asp:Button ID="btnSaveDisclaimer" runat="server" Text="Save" CssClass="submit button" OnClick="btnSaveDisclaimer_Click" />
                    <p>
                        <asp:Label ID="lblDisclaimerDescription" runat="server" Text="MAX Merchandising automatically generates a Disclaimer for each of your Optimized Ads.  You can customize this Disclaimer by selecting one of the options above and entering your custom text in the space provided." /></p>
                </div>
            </cwc:TabPanel>
            <cwc:TabPanel ID="BuildAutomater" runat="server" CssClass="clearfix" Text="AutoLoad" OnClientClick="_gaq.push(['_trackEvent', 'Dealer Preferences', 'Click', 'AutoLoad Tab']);">
                <h2>
                    Automate Loading Of Vehicle Equipment</h2>
                <p>
                    MAX can automatically load vehicle equipment for <em>some</em> Vehicle Manufacturers. If your core manufacturers are not shown below,
                    contact us to find out how we can help.</p>
                <div id="AutoLoadMakes">
                    <label for="<%= AutomaterMakesDDL.ClientID %>">
                        Supported Manufacturers</label>
                    <asp:DropDownList ID="AutomaterMakesDDL" runat="server" />
                    <asp:Button ID="EditAutomaterMakeCredentials" runat="server" OnClick="EditAutomaterMakeCredentials_Click" Text="Edit" />
                </div>
                <asp:Panel ID="EditSiteCredentialsPanel" runat="server" Visible="false" CssClass="EditCreds controlBox" defaultbutton="CreateAutomaterCredentials">
                    <asp:Panel ID="CredentialLockedOutPanel" runat="server" CssClass="LockedOut">
                        <asp:Label ID="credentialWarningLabel" runat="server" CssClass="warning" Text="Warning! Your credentials have become invalid. Please check that you have valid credentials for your 3rd party site and enter them below."></asp:Label>
                    </asp:Panel>
                    <asp:Label ID="lblCredentialWarning" runat="server" CssClass="warning" Text="" EnableViewState="False"></asp:Label>
                    <p>
                        I am an authorized
                        <asp:Label ID="editingSiteLabel" runat="server"></asp:Label>
                        user with the following valid user name and password. I would like to automate the look up of equipment using
                        <asp:Label ID="editingSiteLabel2" runat="server"></asp:Label>
                        for the purpose of creating online inventory ads.</p>
                    <br style="line-height: 0;" />
                    <p>
                        <label for="<%= usernameTB.ClientID %>">
                            Username</label><asp:TextBox ID="usernameTB" runat="server"></asp:TextBox></p>
                    <p>
                        <label for="<%= passwordTB.ClientID %>">
                            Password</label><asp:TextBox ID="passwordTB" TextMode="Password" runat="server"></asp:TextBox></p>
                            <asp:HiddenField runat="server" ID="hfRowVersion"/>
                            <asp:HiddenField runat="server" ID="hfSiteId"/>
                    <p>
                        <asp:Panel ID="dealerCodePanel" runat="server">
                            <label id="lbDealerCodeOptional" runat="server">DealerCode</label>
                            <asp:TextBox ID="dealercodeTB" runat="server"></asp:TextBox>
                        </asp:Panel>
                    </p>
                    <br style="line-height: 0;" />
                    <p>
                        <asp:Button ID="ClearAutomaterCredentials" CssClass="button" runat="server" Text="Clear" OnClick="ClearCredentials_Click" />
                        <asp:Button ID="CreateAutomaterCredentials" CssClass="button" runat="server" Text="Save" OnClick="SaveCredentials_Click" /></p>
                </asp:Panel>
                
                <asp:Panel runat="server" ID="FordDirectAutoLoadsPanel" Visible="false" CssClass="EditCreds controlBox" defaultbutton="CreateAutomaterCredentials">
                    <p>
                        I am an authorized Ford user with the following PACode. I would like to automate the look up of equipment
                        for the purpose of creating online inventory ads.</p>
                    <br style="line-height: 0;" />
                    <p>
                        <label for="<%= PACodeTB.ClientID %>">PACode</label>
                        <asp:TextBox ID="PACodeTB" runat="server"></asp:TextBox>
                        <asp:Button runat="server" ID="VerifyDealerPaCode" CssClass="button" Text="Verify" OnClick="VerifyDealerPaCode_Click"/>
                    </p>
                    
                    <p>
                        <label for="<%= DealerNameTB.ClientID %>">Dealer</label>
                        <asp:TextBox ID="DealerNameTB" runat="server" ReadOnly="True" Enabled="False"></asp:TextBox>                                                
                    </p>
                    <br style="line-height: 0;" />
                    <p>
                        <asp:Button ID="FordDirectClearCredentials" CssClass="button" runat="server" Text="Clear" OnClick="FordDirectClearCredentials_Click" />
                        <asp:Button ID="FordDirectSaveCredentials" CssClass="button" runat="server" Text="Save" OnClick="FordDirectSaveCredentials_Click" /></p>

                </asp:Panel>

                <div style="clear: both; line-height: 0;">
                    &nbsp;</div>
            </cwc:TabPanel>
            <cwc:TabPanel ID="PreApprove" runat="server" Text="Auto-Approve" CssClass="preApproveSettings" OnClientClick="_gaq.push(['_trackEvent', 'Dealer Preferences', 'Click', 'Pre-Approve Tab']);">
                <h2>Auto-Approve</h2>
                <carbuilder:AutoApproveSettings ID="PreApproveSettings1" runat="server" />
            </cwc:TabPanel>
            <cwc:TabPanel ID="Reports" runat="server" Text="Reports" CssClass="reportSettings" OnClientClick="_gaq.push(['_trackEvent', 'Dealer Preferences', 'Click', 'Reports Tab']);">
                <carbuilder:ReportSettings ID="ReportSettings" runat="server" />
            </cwc:TabPanel>
            <cwc:TabPanel ID="BucketAlerts" runat="server" Text="Alerts" CssClass="bucketAlertsSettings" OnClientClick="_gaq.push(['_trackEvent', 'Dealer Preferences', 'Click', 'Quality Alerts Tab']);">
                <carbuilder:BucketAlerts ID="BucketAlertsSettings" runat="server" />
            </cwc:TabPanel>
             <cwc:TabPanel ID="Analytics" runat="server" Text="Analytics" CssClass="analyticsSettings" OnClientClick="_gaq.push(['_trackEvent', 'Dealer Preferences', 'Click', 'Analytics Tab']);">
                <carbuilder:Analytics ID="AnalyticsSettings" runat="server"/>
            </cwc:TabPanel>
            <cwc:TabPanel runat="server" ID="AutoWindowSticker" Text="Window Sticker">
                <carbuilder:AutoWindowSticker ID="AutoWindowStickerSettings" runat="server" />
            </cwc:TabPanel>
        </cwc:TabContainer>
    </div>
    <asp:ObjectDataSource ID="PriorityDS" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Templating.DisplayPriorityDataSource"
        SelectMethod="Fetch" UpdateMethod="Update" OnSelecting="Priority_Selecting" OnUpdating="Priority_Updating">
        <SelectParameters>
            <asp:Parameter Name="businessUnitID" Type="Int32" />
            <asp:ControlParameter ControlID="TierDDL" PropertyName="SelectedValue" Name="tierNumber" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="rankingID" Type="Int32" />
            <asp:Parameter Name="displayPriority" Type="Int32" />
            <asp:Parameter Name="displayDescription" Type="String" DefaultValue="" />
            <asp:Parameter Name="categoryDesc" Type="String" DefaultValue="" />
        </UpdateParameters>
    </asp:ObjectDataSource>
        
</asp:Content>

<asp:Content ID="ScriptContent" ContentPlaceHolderID="ScriptsPlaceHolder" runat="server">

    <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/MAX/MAX.UI.Global.debug.js") %>"></script>
    <script type="text/javascript" src="<%= HtmlHelpers.VersionedUrl("~/Public/Scripts/Settings/DealerPreferences.debug.js") %>"></script>


</asp:Content>
