﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Command;
using FirstLook.Merchandising.DomainModel.Commands;
using MAX.Entities;
using Wanamaker.WebApp.Controls;

namespace Wanamaker.WebApp.CustomizeMAX.Controls
{
    public partial class BucketAlerts : BusinessUnitUserControl
    {
        protected alertDisplayDriver[] _alertDisplayDriverList;

        protected override void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                // save the business unit id from the context to make it page level, 
                // if the users switches business units (new page), it will save the settings on this page to the dealer from which the settings 
                // came from and not to the dealer in the other page (which sets the context)
                // FB: 28432, tureen 12/10/2013

                ViewState["BU"] = BusinessUnitID;

                SetInitalState();
                LoadSettings();
            }
        }

        private void SetInitalState()
        {
            // FB: 27993 - separate dashboard and email alerts
            // (did not want to change interface ... so made this array to mimic the 12 hard coded panels
            _alertDisplayDriverList = new alertDisplayDriver[]
                {
                    new alertDisplayDriver{bucket = (int)WorkflowType.NotOnline, sort = 1, postNumberText="vehicles Not Online"},
                    new alertDisplayDriver{bucket = (int)WorkflowType.NoPrice, sort = 2, postNumberText="vehicles with No Price"},
                    new alertDisplayDriver{bucket = (int)WorkflowType.NoPhotos, sort = 3, postNumberText="vehicles with No Photos"},
                    new alertDisplayDriver{bucket = (int)WorkflowType.CreateInitialAd, sort = 4, postNumberText="vehicles No Description"},
                    new alertDisplayDriver{bucket = (int)WorkflowType.TrimNeeded, sort = 5, postNumberText="vehicles with No Trim"},
                    new alertDisplayDriver{bucket = (int)WorkflowType.DueForRepricing, sort = 6, postNumberText="vehicles that Need Re-pricing"},
                    new alertDisplayDriver{bucket = (int)WorkflowType.EquipmentNeeded, sort = 7, postNumberText="vehicles that need Equipment Review"},
                    new alertDisplayDriver{bucket = (int)WorkflowType.LowActivity, sort = 8, postNumberText="with Low Online Activity"},
                    new alertDisplayDriver{bucket = (int)WorkflowType.LowPhotos, sort = 9, postNumberText="vehicles with Low Photos"},
                    new alertDisplayDriver{bucket = (int)WorkflowType.NoPackages, sort = 10, postNumberText="vehicles with No Packages"},
                    new alertDisplayDriver{bucket = (int)WorkflowType.BookValueNeeded, sort = 11, postNumberText="vehicles with No Book Value"},
                    new alertDisplayDriver{bucket = (int)WorkflowType.NoCarfax, sort = 12, postNumberText="vehicles with No CarFax"},
                    new alertDisplayDriver{bucket = (int)WorkflowType.NoFavourablePriceComparisons, sort = 13, postNumberText="vehicles with No Price Comparison"}
                };
            
            var settings = GetMiscSettings.GetSettings(buID);
            if (settings.HasSettings && settings.ShowDashboard)
                SettingsPlaceHolder.Visible = true;
            else
                DashboardWarningPlaceHolder.Visible = true;

        }

        private void LoadSettings()
        {
            // get bucket alert thresholds
            var settingsCommand = new GetBucketAlerts(buID);
            AbstractCommand.DoRun(settingsCommand);

            // combine with driver for proper sort and post textbox text
            var thresholdView = from t in settingsCommand.Thresholds
                                   join d in _alertDisplayDriverList
                                   on (int)t.Bucket equals d.bucket
                                   orderby d.sort
                                   select new {d.bucket, d.postNumberText, t.Id, t.Limit, t.Active, t.EmailActive };

            // display in repeater
            rptBuckets.DataSource = thresholdView;
            rptBuckets.DataBind();

            // get bucket alert settings
            NewUsedDropDown.SelectedValue = settingsCommand.NewUsed.ToString(CultureInfo.InvariantCulture);
            EmailTextBox.Text = String.Join(",", settingsCommand.Emails);
            WeeklyEmailTextBox.Text = String.Join(",", settingsCommand.WeeklyEmails);
            BionicHealthReportEmailTextBox.Text = String.Join(",", settingsCommand.BionicHealthReportEmails);
            GroupBionicHealthReportEmailTextBox.Text = String.Join(",", settingsCommand.GroupBionicHealthReportEmails);

            BouncedLabel.Visible = settingsCommand.EmailBounced;
            WeeklyBouncedLabel.Visible = settingsCommand.WeeklyEmailBounced;
            BionicHealthReportBouncedLabel.Visible = settingsCommand.BionicHealthReportEmailBounced;
            GroupBionicHealthReportBouncedLabel.Visible = settingsCommand.GroupBionicHealthReportEmailBounced;
        }

        protected void OnSave ( object sender, EventArgs e )
        {
            // retrieve thresholds values from repeater
            var thresholdList = new List<BucketAlertThreshold>();

            foreach (RepeaterItem rItem in rptBuckets.Items)
            {

                int thresholdID;
                bool alertIsActive;
                int bucketID;
                int limit;
                bool emailActive;

                // threshold ID
                var ctlThresholdID = rItem.FindControl("hfThresholdID");
                int.TryParse(((HiddenField)ctlThresholdID).Value, out thresholdID);

                // bucket
                var ctlBucket = rItem.FindControl("hfBucket");
                int.TryParse(((HiddenField)ctlBucket).Value, out bucketID);

                // limit
                var ctlLimit = rItem.FindControl("txtLimit");
                if (!int.TryParse(((TextBox)ctlLimit).Text, out limit))
                {
                    limit = 0;
                    ((TextBox)ctlLimit).Text = limit.ToString();
                }

                // dashboard active
                var ctlActive = rItem.FindControl("chkActive");
                alertIsActive = ((CheckBox)ctlActive).Checked;

                // email active
                var ctlEmailActive = rItem.FindControl("chkEmail");
                emailActive = ((CheckBox)ctlEmailActive).Checked;

                // add to bucket
                thresholdList.Add(new BucketAlertThreshold { Active = alertIsActive, Bucket = (WorkflowType)bucketID, BusinessUnitId = buID, Id = thresholdID, Limit = limit, EmailActive = emailActive });
            
            }

            // save bucket settings
            int newUsed = int.Parse(NewUsedDropDown.SelectedValue);
            string emails = string.Join( ",", Regex.Replace(EmailTextBox.Text, @"\s*", string.Empty).Split(',',';').Where( x => x != "" )); // throw away empties
            string weeklyEmails = string.Join(",", Regex.Replace(WeeklyEmailTextBox.Text, @"\s*", string.Empty).Split(',', ';').Where(x => x != "")); // throw away empties
            string bionicHealthReportEmails = string.Join(",", Regex.Replace(BionicHealthReportEmailTextBox.Text, @"\s*", string.Empty).Split(',', ';').Where(x => x != "")); // throw away empties
            string groupBionicHealthReportEmails = string.Join(",", Regex.Replace(GroupBionicHealthReportEmailTextBox.Text, @"\s*", string.Empty).Split(',', ';').Where(x => x != "")); // throw away empties
            var active = !(String.IsNullOrWhiteSpace(emails) && String.IsNullOrWhiteSpace(weeklyEmails) && String.IsNullOrWhiteSpace(bionicHealthReportEmails) && String.IsNullOrWhiteSpace(groupBionicHealthReportEmails));
            var settingsCommand = new SetBucketAlerts(buID, active, emails, weeklyEmails, bionicHealthReportEmails, groupBionicHealthReportEmails, newUsed, Context.User.Identity.Name, false, false, false, false, thresholdList.ToArray());
            AbstractCommand.DoRun(settingsCommand);
            BouncedLabel.Visible = false;
            WeeklyBouncedLabel.Visible = false;
        }
        
        protected class alertDisplayDriver
        {
            public int bucket { get; set; }
            public int sort { get; set; }
            public string postNumberText { get; set; }
        }

        protected int buID 
        { 
            get{ return Convert.ToInt32(ViewState["BU"]);}
        }
    }
}