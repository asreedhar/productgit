using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Templating;
using MerchandisingLibrary;
using Wanamaker.WebApp.Controls;

namespace Wanamaker.WebApp.CustomizeMAX.Controls
{

    public partial class EditSnippetIntro : BusinessUnitUserControl
    {

        public int BlurbId
        {
            get
            {

                object o = ViewState["bId"];
                //if (o == null) throw new ApplicationException("You must have a valid blurbId");
                return (o == null) ? 1 : (int)o;
            }
            set
            {
                ViewState["bId"] = value;
            }
        }
        public int? ThemeId
        {
            get
            {
                object o = ViewState["tId"];
                return (int?)o;
            }
            set
            {
                ViewState["tId"] = value;
            }
        }

        private string _title;
        public string Title
        {
            get
            {
                if (string.IsNullOrEmpty(_title))
                {
                    _title = string.Empty;
                }
                return _title;
            }
            set
            {
                _title = value;
            }
        }

        public bool ShowFirstLookLibrary
        {
            get
            {
                object o = ViewState["useFL"];
                return (o == null) ? false : (bool)o;
            }
            set
            {
                ViewState["useFL"] = value;
            }
        }
        protected void Tagline_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@BusinessUnitId"].Value = BusinessUnitID;
            if (ShowFirstLookLibrary)
            {
                e.Command.Parameters["@LibraryBusinessUnitId"].Value = 100150;
            }
            else
            {
                e.Command.Parameters["@LibraryBusinessUnitId"].Value = BusinessUnitID;
            }
            e.Command.Parameters["@BlurbId"].Value = BlurbId;
            if (ThemeId.HasValue)
            {
                e.Command.Parameters["@ThemeId"].Value = ThemeId.Value;
            }
            else
            {
                e.Command.Parameters["@ThemeId"].Value = DBNull.Value;
            }

            TaglineGV.ShowFooter = !ShowFirstLookLibrary;

        }
        protected void Tagline_Command(object sender, SqlDataSourceCommandEventArgs e)
        {
            e.Command.Parameters["@BusinessUnitId"].Value = BusinessUnitID;
            e.Command.Parameters["@BlurbId"].Value = BlurbId;

            TextBox tagTb = TaglineGV.FooterRow.FindControl("BlurbPreText") as TextBox;
            if (tagTb == null) throw new ApplicationException("No tagline tb");

            TextBox tagEndTb = TaglineGV.FooterRow.FindControl("BlurbPostText") as TextBox;
            if (tagEndTb == null) throw new ApplicationException("No tagline end tb");

            DropDownList profileDDL = TaglineGV.FooterRow.FindControl("ProfileDDL") as DropDownList;
            if (profileDDL == null) throw new ApplicationException("No profile ddl");

            DropDownList themeDDL = TaglineGV.FooterRow.FindControl("MissionDDL") as DropDownList;
            if (themeDDL == null) throw new ApplicationException("No theme/mission ddl");


            e.Command.Parameters["@BlurbPreText"].Value = tagTb.Text;
            e.Command.Parameters["@BlurbPostText"].Value = tagEndTb.Text;

            int profileID;
            if (int.TryParse(profileDDL.SelectedValue, out profileID))
            {
                e.Command.Parameters["@VehicleProfileId"].Value = profileID;
            }

            int themeId;
            if (int.TryParse(themeDDL.SelectedValue, out themeId))
            {
                e.Command.Parameters["@ThemeId"].Value = themeId;
            }

        }
        protected void Tagline_Selected(object sender, EventArgs e)
        {
            //DataKey dk = TaglineGV.DataKeys[TaglineGV.SelectedIndex];
            //set active tagline to this one!
        }
        protected void NewTagline_Click(object sender, EventArgs e)
        {
            TaglineDS.Insert();
            TaglineGV.DataBind();
        }

        protected void Profile_Bound(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;
            if (ddl != null)
            {
                ddl.Items.Insert(0, new ListItem("Any Vehicle...", ""));
            }
        }

        protected void Profiles_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@BusinessUnitId"].Value = BusinessUnitID;
        }


        //called when the add button on bottom is clicked
        protected void AddSnippet_Click(object sender, EventArgs e)
        {
            TaglineDS.Insert();

        }

        protected static string GetIsPostVisibleClass()
        {
            return "invisible";
        }

        protected void Customize_Clicked(object sender, EventArgs e)
        {
            BlurbDataSource bds = new BlurbDataSource();
            bds.TemplateSampleCreatePlaceholder(BusinessUnitID, BlurbId);
            TaglineGV.DataBind();
            TaglineGV.EditIndex = 0;
        }

        protected void GV_Binding(object sender, EventArgs e)
        {

        }

        protected void Tag_Sel(object sender, SqlDataSourceStatusEventArgs e)
        {
            int a = e.AffectedRows;
        }

        protected void CopyToDealer_Click(object sender, EventArgs e)
        {
            LinkButton lb = sender as LinkButton;
            if (lb == null) throw new ApplicationException("Link button for copy not found");
            int blurbId = int.Parse(lb.CommandArgument);


            throw new NotImplementedException();
        }

        protected void Tagline_Deleting(object sender, SqlDataSourceCommandEventArgs e)
        {
            e.Command.Parameters["@BusinessUnitId"].Value = BusinessUnitID;
        }

        protected string GetDeleteText(bool isActive)
        {
            if (ShowFirstLookLibrary)
            {
                if (isActive)
                {
                    return disableText;
                }
                else
                {
                    return enableText;
                }
            }
            else
            {
                return "Delete";
            }
        }

        private const string disableText = "Disable";
        private const string enableText = "Enable";
        protected void ToggleEnabled_Click(object sender, EventArgs e)
        {
            LinkButton lb = sender as LinkButton;
            if (lb == null) throw new ApplicationException("Link button for copy not found");
            int blurbId = int.Parse(lb.CommandArgument);

            //we enable it whenever the text is enable
            BlurbText.SetEnabled(BusinessUnitID, blurbId, lb.Text == enableText);
            TaglineGV.DataBind();
        }

        protected void Tagline_Updating(object sender, SqlDataSourceCommandEventArgs e)
        {
            e.Command.Parameters["@BusinessUnitID"].Value = BusinessUnitID;
            e.Command.Parameters["@BlurbId"].Value = BlurbId;
        }

        protected void Tagline_Inserted(object sender, SqlDataSourceStatusEventArgs e)
        {
            TaglineGV.DataBind();
        }
    }
}