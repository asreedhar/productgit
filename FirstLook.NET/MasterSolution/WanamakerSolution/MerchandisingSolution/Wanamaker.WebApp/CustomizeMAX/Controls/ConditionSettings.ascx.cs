using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FirstLook.DomainModel.Oltp;
using Wanamaker.WebApp.Controls;

namespace Wanamaker.WebApp.CustomizeMAX.Controls
{
    public partial class ConditionSettings : BusinessUnitUserControl
    {
        protected void ExcellentAdjectives_Inserting(object sender, SqlDataSourceCommandEventArgs e)
        {
            if (string.IsNullOrEmpty(NewExcellentConditionDescription.Text))
            {
                e.Cancel = true;
            }
                
            e.Command.Parameters["@BusinessUnitId"].Value = BusinessUnitID;
            e.Command.Parameters["@ConditionLevel"].Value = 1;
            e.Command.Parameters["@ConditionDescription"].Value = NewExcellentConditionDescription.Text;

        }

        protected void GoodAdjectives_Inserting(object sender, SqlDataSourceCommandEventArgs e)
        {
            if (string.IsNullOrEmpty(NewGoodConditionDescription.Text))
            {
                e.Cancel = true;
            }

            e.Command.Parameters["@BusinessUnitId"].Value = BusinessUnitID;
            e.Command.Parameters["@ConditionLevel"].Value = 2;
            e.Command.Parameters["@ConditionDescription"].Value = NewGoodConditionDescription.Text;

        }

        protected void DependableAdjectives_Inserting(object sender, SqlDataSourceCommandEventArgs e)
        {
            if (string.IsNullOrEmpty(NewDependableConditionDescription.Text))
            {
                e.Cancel = true;
            }

            e.Command.Parameters["@BusinessUnitId"].Value = BusinessUnitID;
            e.Command.Parameters["@ConditionLevel"].Value = 3;
            e.Command.Parameters["@ConditionDescription"].Value = NewDependableConditionDescription.Text;
        }

        protected void Adjectives_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@BusinessUnitId"].Value = BusinessUnitID;
        }

        protected void InsertNew_Click(object sender, EventArgs e)
        {
            Button send = sender as Button;
            if (send != null)
            {
                switch (send.CommandArgument)
                {
                    case "Excellent":
                        ExcellentAdjectivesDS.Insert();
                        break;
                    case "Good":
                        GoodAdjectivesDS.Insert();
                        break;
                    case "Dependable":
                        DependableAdjectivesDS.Insert();
                        break;
                    default:
                        throw new ApplicationException("error in adjective insert script arg: " + send.CommandArgument);
                }
            }
        }

    }
}