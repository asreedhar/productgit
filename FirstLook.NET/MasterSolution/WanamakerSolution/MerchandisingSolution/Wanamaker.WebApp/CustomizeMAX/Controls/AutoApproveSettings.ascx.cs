﻿using System;
using System.Web.Security;
using FirstLook.Common.Core.Command;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Commands;
using MAX.Entities;
using Merchandising.Messages;
using Merchandising.Messages.AutoApprove;
using Wanamaker.WebApp.AppCode.AccessControl;
using Wanamaker.WebApp.Controls;

namespace Wanamaker.WebApp.CustomizeMAX.Controls
{
    public partial class AutoApproveSettings : BusinessUnitUserControl
    {
        #region Injected dependencies

        public IAdMessageSender MessageSender { get; set; }

        #endregion

        protected void Page_PreRender ( object sender, EventArgs e )
        {   
            LoadAutoApprove();
        }
       
        protected void OnReapproveClick(object sender, EventArgs e)
        {
            var status = (AutoApproveStatus)Enum.Parse(typeof(AutoApproveStatus), preapprove_Status.SelectedValue);
            MessageSender.SendAutoApprovalInventory(new AutoApproveInventoryMessage(BusinessUnitID, status, WorkflowType.AllInventory), GetType().FullName);
        }

        protected void btn_PreApproveSettings_Click ( object sender, EventArgs e )
        {
            MembershipUser user = Context.Items[ IdentityHelper.MembershipContextKey ] as MembershipUser;
            if ( user == null ) throw new Exception( "Unable to determine current user." );

            var status = (AutoApproveStatus)Enum.Parse(typeof(AutoApproveStatus), preapprove_Status.SelectedValue);    

            var command = new SetAutoApproveSettings( BusinessUnitID, preapprove_FirstName.Text,
                preapprove_LastName.Text, preapprove_Email.Text, status, user.UserName, false);
            AbstractCommand.DoRun( command );

            if (status != AutoApproveStatus.Off)
                ApprovalAllInventoryButton.Visible = true;

            MessageSender.SendAutoApprovalInventory(new AutoApproveInventoryMessage(BusinessUnitID, status, WorkflowType.CreateInitialAd), GetType().FullName);
        }

        private void LoadAutoApprove()
        {
            var command = new GetAutoApproveSettings(BusinessUnitID);
            AbstractCommand.DoRun(command);

            if (!command.HasSettings)
                return;

            TermsAndConditions.Visible = false;
            btn_PreApproveSettings.Text = @"Save";
            btn_PreApproveSettings.OnClientClick =
                "_gaq.push(['_trackEvent', 'Dealer Preferences', 'Click', 'Pre-Approve Save Button']);";

            preapprove_FirstName.Text = command.FirstName;
            preapprove_LastName.Text = command.LastName;
            preapprove_Email.Text = command.Email;
            preapprove_Status.SelectedValue = command.Status.ToString();
            BouncedLabel.Visible = command.EmailBounced;

            if (command.Status != AutoApproveStatus.Off)
                ApprovalAllInventoryButton.Visible = true;
        }
    }
}