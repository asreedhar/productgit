<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="SnippetEditor.ascx.cs" Inherits="Wanamaker.WebApp.CustomizeMAX.Controls.SnippetEditor" %>
<%@ Import Namespace="FirstLook.Merchandising.DomainModel.Templating" %>
<asp:SqlDataSource ID="ThemeDS" runat="server" SelectCommandType="StoredProcedure" ConnectionString='<%$ ConnectionStrings:Merchandising %>'
    SelectCommand="templates.Themes#Fetch">
    <SelectParameters>
        <asp:Parameter Name="BusinessUnitID" DefaultValue="100150" ConvertEmptyStringToNull="true" Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="VehicleProfileDS" runat="server" ConnectionString='<%$ ConnectionStrings:Merchandising %>' SelectCommand="Select vehicleProfileId, [title] FROM templates.vehicleProfiles WHERE businessUnitId = @BusinessUnitId OR businessUnitId = 100150">
    <SelectParameters>
        <asp:ControlParameter ControlID="BusId" Name="BusinessUnitId" DefaultValue="100150" />
    </SelectParameters>
</asp:SqlDataSource>
<div style="float: left; width: 640px">
    
    <asp:HiddenField ID="BusID" runat="server" Value="-1" />
    
    <h3><span style="float: right"><asp:Button ID="NewSnip" runat="server" OnClick="NewSnippet_Click" Text="Create New Snippet" /></span>Snippet Information</h3>
    
    <asp:FormView ID="BlurbCreatorFV" runat="server" DefaultMode="Insert" DataSourceID="BlurbDS" DataKeyNames="BlurbID" OnDataBound="BlurbCreatorFV_DataBound"
        OnModeChanging="BlurbMode_Changing" OnItemInserted="Blurb_Inserted">
        <EditItemTemplate>
            <asp:Label CssClass="warning" runat="server" ID="NotOwnerLbl" Text="FirstLook Snippet - Template cannot be modified - Go to Ad Text Editor to Customize Your Text"></asp:Label>
            <asp:HiddenField ID="ownerId" runat="server" Value='<%# Eval("OwnerId") %>' />
            <ul class="clearfix snippetHead">
                <li>Category:
                    <asp:DropDownList ID="BlurbCategoryDDL" runat="server" DataSourceID="BlurbTypesDS" DataTextField="BlurbTypeName" DataValueField="BlurbTypeId">
                    </asp:DropDownList>
                </li>
                <li>Title:
                    <asp:TextBox ID="BlurbNameTB" runat="server" Text='<%# Bind("blurbName") %>'></asp:TextBox></li>
            </ul>
            <asp:Table ID="Table1" CssClass="BlurbBuilderTable" runat="server">
                <asp:TableRow>
                    <asp:TableCell>
                        Use When:
                        <asp:LinkButton ID="andButton" runat="server" Text="AND" OnClientClick='<%# getAddTextJS("&") %>'></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="OR" OnClientClick='<%# getAddTextJS(",") %>'></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton2" runat="server" Text="NOT" OnClientClick='<%# getAddTextJS("!") %>'></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton3" runat="server" Text="Group" OnClientClick='<%# getAddTextJS("()") %>'></asp:LinkButton>
                        <br />
                        <asp:TextBox ID="ConditionTB" runat="server" TextMode="MultiLine" CssClass="ConditionField" Text='<%# Bind("TemplateCondition") %>'></asp:TextBox></asp:TableCell>
                    <asp:TableCell CssClass="TemplateMergeFields">
                        <asp:DropDownList ID="RulesDDL" runat="server" DataSourceID="RulesDS" OnDataBound="MergeRules_Bound" OnSelectedIndexChanged="Condition_Selected"
                            AutoPostBack="true">
                        </asp:DropDownList>
                        <asp:ObjectDataSource ID="RulesDS" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Templating.TemplateConditionHelper"
                            SelectMethod="GetConditionDisplayNames"></asp:ObjectDataSource>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        Template Body:<br />
                        <asp:TextBox ID="TemplateBodyTB" runat="server" TextMode="MultiLine" CssClass="TemplateBodyField" Text='<%# Bind("TemplateText") %>'></asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell CssClass="TemplateMergeFields">
                        <asp:DropDownList ID="BasicInfoDDL" runat="server" DataSourceID="otherMerchPointsDS" DataTextField="templateDisplayText"
                            DataValueField="pointID" OnSelectedIndexChanged="Rule_Selected" AutoPostBack="true">
                        </asp:DropDownList>
                        <br />
                        <asp:DropDownList ID="OptionsRulesDDL" runat="server" DataSourceID="basicInfoDS" DataTextField="templateDisplayText" DataValueField="pointID"
                            OnSelectedIndexChanged="Rule_Selected" AutoPostBack="true">
                        </asp:DropDownList>
                        <br />
                        <asp:DropDownList ID="ConsumerInfoDDL" runat="server" DataSourceID="consInfoDS" DataTextField="templateDisplayText" DataValueField="pointID"
                            OnSelectedIndexChanged="Rule_Selected" AutoPostBack="true">
                        </asp:DropDownList>
                        <br />
                        <asp:DropDownList ID="CarfaxInfoDDL" runat="server" DataSourceID="carfaxInfoDS" DataTextField="templateDisplayText" DataValueField="pointID"
                            OnSelectedIndexChanged="Rule_Selected" AutoPostBack="true">
                        </asp:DropDownList>
                        <br />
                        <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="categoriesDS" DataTextField="Description" DataValueField="CategoryID"
                            OnSelectedIndexChanged="Rule_Selected" AutoPostBack="true">
                        </asp:DropDownList>
                        <br />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <asp:Button ID="CreateButton" runat="server" CommandName="Insert" Text="Save" />
            <asp:Button ID="SaveButton" runat="server" CommandName="Update" Text="Save" Visible="false" />
            <asp:Button ID="Button4" runat="server" CommandName="Delete" Text="Delete" />
        </EditItemTemplate>
    </asp:FormView>
    
</div>

<asp:TreeView ID="blurbTree" CssClass="AvailableBlurbTree" runat="server" 
    NodeStyle-CssClass="templateCategory" LeafNodeStyle-CssClass="templateBlurb" 
    EnableClientScript="true"
    PopulateNodesFromClient="True"
    SelectedNodeStyle-CssClass="SelectedBlurb" 
    OnSelectedNodeChanged="TemplateBlurb_Selected"
    OnTreeNodePopulate="TemplateNodeExpanding"
    NodeIndent="2">
</asp:TreeView>

<div id="CustomSnippets" class="clearfix">
    <h3>Custom Snippet Text</h3>
    <p>Enter custom text below that should be used with this snippet. The text will only be in ads for the vehicle type you specify. Check "long form" if the text should only appear in longer ads.</p>
    <asp:GridView ID="SampleTextGV" runat="server" DataSourceID="TextSamplesDS" AutoGenerateColumns="False" DataKeyNames="BlurbTextID" OnRowEditing="TextRow_Editing" ShowFooter="false" ShowHeader="true" CssClass="SnippetEditorTable"
        AutoGenerateDeleteButton="true" AutoGenerateEditButton="true" >
        <Columns>
            <asp:TemplateField HeaderText="Your Custom Text">
                <ItemTemplate>
                    <div>
                        <p style="width: 30%; float: left">
                            Vehicle:&nbsp;<span class="profile"><%# ((BlurbText)Container.DataItem).Match.Title %></span>
                        </p>
                        <p style="width: 30%; float: left">
                            Theme:&nbsp;<asp:Label ID="ThemeLbl" CssClass="theme" runat="server" Text='<%# ((BlurbText)Container.DataItem).Theme %>'></asp:Label>
                        </p>
                    </div>
                    <div style="clear: left; padding-left: 10px;">
                        <%# Eval("PreGenerateText") %>
                        <br />
                        ...body...
                        <br />
                        <%# Eval("PostGenerateText") %>
                    </div>
                </ItemTemplate>
                <EditItemTemplate>
                    <div>
                        <p style="width: 30%; float: left">
                            Vehicle:
                            <asp:DropDownList ID="VehicleProfileDDL" runat="server" DataSourceID="VehicleProfileDS" DataTextField="title" DataValueField="vehicleProfileId"
                                OnDataBound="EditProfiles_Bound">
                            </asp:DropDownList>
                        </p>
                        <p style="width: 30%; float: left">
                            Theme:
                            <asp:DropDownList ID="ThemeDDL" runat="server" DataTextField="name" DataValueField="Id" DataSourceID="ThemeDS" AppendDataBoundItems="true"
                                SelectedValue='<%# GetThemeVal(Eval("ThemeId")) %>'>
                                <asp:ListItem Text="None" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </p>
                    </div>
                    <div style="clear: left; padding-left: 10px;">
                        <asp:TextBox TextMode="MultiLine" CssClass="PreTextInsertBox" ID="PreBlurbTB" runat="server" Text='<%# Bind("PreGenerateText") %>'></asp:TextBox>
                        <br />
                        ...body...
                        <br />
                        <asp:TextBox TextMode="MultiLine" CssClass="PreTextInsertBox" ID="PostBlurbTB" runat="server" Text='<%# Bind("PostGenerateText") %>'></asp:TextBox>
                    </div>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:CheckBoxField HeaderText="Long Form?" DataField="isLongForm" ItemStyle-CssClass="Active" />
            <asp:CheckBoxField HeaderText="Active?" DataField="active" ItemStyle-CssClass="Active" />
            <asp:CheckBoxField HeaderText="Is Header?" DataField="isHeader" ItemStyle-CssClass="Active" />
        </Columns>
        <EditRowStyle BackColor="#485775" ForeColor="#EEEEEE" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle BackColor="Transparent" Font-Bold="True" ForeColor="#555555" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <div class="addDataForm">
        <div>
            Vehicle Type:
            <asp:DropDownList ID="VehicleProfileDDL" runat="server" DataSourceID="VehicleProfileDS" DataTextField="title" DataValueField="vehicleProfileId"
                OnDataBound="Profiles_Bound">
            </asp:DropDownList>
            <asp:DropDownList ID="ThemeDDL" runat="server" DataTextField="name" DataValueField="Id" DataSourceID="ThemeDS" AppendDataBoundItems="true">
                <asp:ListItem Text="None" Value=""></asp:ListItem>
            </asp:DropDownList>
        </div>
        <asp:TextBox TextMode="MultiLine" CssClass="PreTextInsertBox" ID="PreBlurbTBInsert" runat="server" Text=""></asp:TextBox>
        <br />
        ...body...
        <br />
        <asp:TextBox TextMode="MultiLine" CssClass="PreTextInsertBox" ID="PostBlurbTBInsert" runat="server" Text=""></asp:TextBox>
        <br />
        <asp:CheckBox ID="isLongCB" runat="server" Text="Long Form?" />
        <asp:CheckBox ID="isHeaderCheckBox" runat="server" Text="Is Header?" />
        <asp:Button ID="addSamp" runat="server" OnClick="AddNew_Click" Text="Add New..." />
    </div>
</div>
<asp:SqlDataSource runat="server" ID="BlurbTypesDS" SelectCommandType="StoredProcedure" ConnectionString="<%$ ConnectionStrings:Merchandising %>"
    SelectCommand="templates.getAllSnippetTypes"></asp:SqlDataSource>
<asp:SqlDataSource runat="server" ID="otherMerchPointsDS" SelectCommandType="StoredProcedure" ConnectionString="<%$ ConnectionStrings:Merchandising %>"
    SelectCommand="templates.getPossibleMerchandisingPoints">
    <SelectParameters>
        <asp:Parameter Name="BusinessUnitID" DefaultValue="100150" />
        <asp:Parameter Name="RuleType" Type="Int32" DefaultValue="4" />
    </SelectParameters>
</asp:SqlDataSource>
<asp:SqlDataSource runat="server" ID="basicInfoDS" SelectCommandType="StoredProcedure" ConnectionString="<%$ ConnectionStrings:Merchandising %>"
    SelectCommand="templates.getPossibleMerchandisingPoints">
    <SelectParameters>
        <asp:Parameter Name="BusinessUnitID" DefaultValue="100150" />
        <asp:Parameter Name="RuleType" Type="Int32" DefaultValue="3" />
    </SelectParameters>
</asp:SqlDataSource>
<asp:SqlDataSource runat="server" ID="consInfoDS" SelectCommandType="StoredProcedure" ConnectionString="<%$ ConnectionStrings:Merchandising %>"
    SelectCommand="templates.getPossibleMerchandisingPoints">
    <SelectParameters>
        <asp:Parameter Name="BusinessUnitID" DefaultValue="100150" />
        <asp:Parameter Name="RuleType" Type="Int32" DefaultValue="5" />
    </SelectParameters>
</asp:SqlDataSource>
<asp:SqlDataSource runat="server" ID="carfaxInfoDS" SelectCommandType="StoredProcedure" ConnectionString="<%$ ConnectionStrings:Merchandising %>"
    SelectCommand="templates.getPossibleMerchandisingPoints">
    <SelectParameters>
        <asp:Parameter Name="BusinessUnitID" DefaultValue="100150" />
        <asp:Parameter Name="RuleType" Type="Int32" DefaultValue="6" />
    </SelectParameters>
</asp:SqlDataSource>
<asp:ObjectDataSource runat="server" ID="categoriesDS" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.VINStyleDataSource"
    SelectMethod="GetGlobalCategories2"></asp:ObjectDataSource>
<asp:ObjectDataSource ID="BlurbDS" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Templating.BlurbDataSource"
    SelectMethod="TemplateBlurbSelect" OnSelecting="Blurb_Selecting" UpdateMethod="TemplateBlurbUpdate" OnUpdating="Blurb_InsUpdating"
    InsertMethod="TemplateBlurbCreate" OnInserting="Blurb_InsUpdating" DeleteMethod="TemplateBlurbDelete" OnDeleting="Blurb_Deleting">
    <SelectParameters>
        <asp:Parameter Name="businessUnitID" Type="Int32" DefaultValue="100150" />
        <asp:ControlParameter Name="blurbID" ControlID="blurbTree" PropertyName="SelectedValue" />
    </SelectParameters>
    <UpdateParameters>
        <asp:Parameter Type="Object" Name="blurb" />
        <asp:Parameter Type="Int32" Name="BusinessUnitId" DefaultValue="100150" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Type="Object" Name="blurb" />
        <asp:Parameter Type="Int32" Name="BusinessUnitId" DefaultValue="100150" />
    </InsertParameters>
    <DeleteParameters>
        <asp:Parameter Name="businessUnitID" Type="Int32" DefaultValue="100150" />
        <asp:ControlParameter Name="blurbID" ControlID="blurbTree" PropertyName="SelectedValue" />
    </DeleteParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="TextSamplesDS" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Templating.BlurbDataSource"
    SelectMethod="TemplateTextSamplesSelect" OnSelecting="TextSamples_Selecting" UpdateMethod="TemplateSampleCreateOrUpdate"
    OnUpdating="TextSamples_Updating" InsertMethod="TemplateSampleCreateOrUpdate" OnInserting="TextSamples_Updating" DeleteMethod="TemplateSampleDelete"
    OnDeleting="TextSamples_Deleting">
    <SelectParameters>
        <asp:Parameter Name="businessUnitID" Type="Int32" DefaultValue="100150" />
        <asp:ControlParameter ControlID="blurbTree" PropertyName="SelectedValue" Name="blurbID" Type="Int32" DefaultValue="0" />
    </SelectParameters>
    <InsertParameters>
        <asp:Parameter Name="businessUnitID" Type="Int32" DefaultValue="100150" />
        <asp:ControlParameter ControlID="blurbTree" PropertyName="SelectedValue" Name="blurbID" Type="Int32" DefaultValue="0" />
        <asp:Parameter Name="BlurbTextID" Type="Int32" DefaultValue="0" />
        <asp:Parameter Name="vehicleProfileId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="themeId" Type="Int32" DefaultValue="-1" />
    </InsertParameters>
    <UpdateParameters>
        <asp:Parameter Name="businessUnitID" Type="Int32" DefaultValue="100150" />
        <asp:ControlParameter ControlID="blurbTree" PropertyName="SelectedValue" Name="blurbID" Type="Int32" DefaultValue="0" />
        <asp:Parameter Name="BlurbTextID" Type="Int32" DefaultValue="0" />
        <asp:Parameter Name="vehicleProfileId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="themeId" Type="Int32" DefaultValue="-1" />
    </UpdateParameters>
    <DeleteParameters>
        <asp:Parameter Name="businessUnitID" Type="Int32" DefaultValue="0" />
        <asp:Parameter Name="BlurbTextID" Type="Int32" DefaultValue="0" />
    </DeleteParameters>
</asp:ObjectDataSource>
