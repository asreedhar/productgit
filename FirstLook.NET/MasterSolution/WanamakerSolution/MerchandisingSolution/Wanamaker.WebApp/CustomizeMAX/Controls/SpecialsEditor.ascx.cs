using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Vehicles;

namespace Wanamaker.WebApp.CustomizeMAX.Controls
{

    public partial class SpecialsEditor : System.Web.UI.UserControl
    {
        public int OwnerEntityId
        {
            get
            {
                if (IsInCpoManagerMode)
                {
                    return CpoGroupId;
                }
                else
                {
                    object o = ViewState["busId"];
                    return (o == null) ? -1 : (int)o;
                }
            }
            set
            {
                ViewState["busId"] = value;
            }
        }
        public int OwnerEntityTypeId
        {
            get
            {
                if (IsInCpoManagerMode)
                {
                    return 3;
                }
                else
                {
                    return 1;
                }
            }
        }


        public int CpoGroupId
        {
            get
            {
                object o = ViewState["cpoId"];
                return (o == null) ? -1 : (int)o;
            }
            set
            {
                ViewState["cpoId"] = value;
            }
        }



        public bool IsInCpoManagerMode
        {
            get
            {
                object o = ViewState["isCpo"];
                return (o == null) ? false : (bool)o;
            }
            set { ViewState["isCpo"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void CertDDL_Bound(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;
            if (ddl == null) return;

            ddl.Items.Insert(0, "None selected...");


        }

        protected void CreateNew_Click(object sender, EventArgs e)
        {
            SpecialsDV.ChangeMode(DetailsViewMode.Insert);
        }

        protected void Specials_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@OwnerEntityId"].Value = OwnerEntityId;
            e.Command.Parameters["@OwnerEntityTypeId"].Value = OwnerEntityTypeId;
        }


        protected void Specials_InsertingUpdating(object sender, SqlDataSourceCommandEventArgs e)
        {
            e.Command.Parameters["@OwnerEntityId"].Value = OwnerEntityId;
            e.Command.Parameters["@OwnerEntityTypeId"].Value = OwnerEntityTypeId;

            DropDownList profDDL = SpecialsDV.FindControl("ProfileDDL") as DropDownList;
            if (profDDL.SelectedIndex > 0)
            {
                e.Command.Parameters["@VehicleProfileId"].Value = profDDL.SelectedValue;
            }
        }

        protected void Specials_Inserted(object sender, EventArgs e)
        {
            DataBind();
        }
        protected void SpecialsDV_Binding(object sender, EventArgs e)
        {

        }

        protected void SpecialsDV_Bound(object sender, EventArgs e)
        {
            //first get the id and name of the current item
            int? profileId = null;

            object o = SpecialsDV.DataItem;
            DataRowView drv = o as DataRowView;
            if (drv != null)
            {


                object ct = drv["vehicleProfileId"];
                if (ct != DBNull.Value)
                {
                    try
                    {
                        profileId = Convert.ToInt32(ct);
                    }
                    catch
                    {
                        profileId = null;
                    }
                }
            }


            if (SpecialsDV.CurrentMode != DetailsViewMode.ReadOnly)
            {

                DropDownList profileDDL = SpecialsDV.FindControl("ProfileDDL") as DropDownList;
                if (profileDDL != null)
                {
                    profileDDL.DataBind();
                    if (profileId.HasValue)
                    {
                        profileDDL.SelectedValue = profileId.ToString();
                    }
                    else
                    {
                        profileDDL.SelectedIndex = 0;
                    }
                }
            }





        }

        protected void Special_Changed(object sender, EventArgs e)
        {
            SpecialsDV.ChangeMode(DetailsViewMode.ReadOnly);
        }

        protected void ProfileDDL_Bound(object sender, EventArgs e)
        {
            DropDownList snd = sender as DropDownList;
            if (snd != null)
            {
                snd.Items.Insert(0, "Any vehicle...");
            }
        }

        protected void Profiles_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@BusinessUnitId"].Value = OwnerEntityId;

        }

        public delegate void EventHandler(object sender, EventArgs e);
        public event EventHandler SpecialDeleted;

        protected void Special_Deleted(object sender, SqlDataSourceStatusEventArgs e)
        {
            if (SpecialDeleted != null)
            {
                SpecialDeleted(this, new EventArgs());
            }
            SpecialDDL.DataBind();
        }
    }
}