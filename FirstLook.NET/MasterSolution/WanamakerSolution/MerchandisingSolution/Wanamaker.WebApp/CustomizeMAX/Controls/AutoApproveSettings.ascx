﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AutoApproveSettings.ascx.cs" Inherits="Wanamaker.WebApp.CustomizeMAX.Controls.AutoApproveSettings" %>
<%@ Import Namespace="Wanamaker.WebApp.CustomizeMAX.Controls" %>

<ul>
    <li>Auto-Approve will automatically send out ads for every vehicle without requiring you to click the &quot;Approval&quot;
        button. Any changes to the vehicle like price updates will not require you to re-approve the ad - the system will add the
        data to the ad for you and re-approve it automatically. At any time you can customize the ad and the system will replace
        its ad with your ad.</li>
    <li>The system cannot create an ad for any vehicles found in the "No Trim" bucket.  
        If a Lot Provider is sending us a feed and there's an ad for the vehicle in the feed, 
        then the system will send that ad to 3rd party sites. 
        Once the trim is selected in MAX, then a MAX Ad will be created automatically and sent to 3rd party sites.</li>
    <li>Once a day you'll receive an email with all the ads that the system created so you can review them.</li>
    <li>You can keep track of ads that you have customized and/or reviewed by checking the &quot;Ad Review Complete&quot; check
        box in the approval screen. At anytime, you can see the ads which still need customization and/or review by clicking the
        &quot;Ad Review Needed&quot; bucket on the home page. It will show you all the vehicles which do not have a check for the
        &quot;Ad Review Complete&quot; check box.</li>
</ul>
<asp:Panel ID="PreApproveSettingsForm" CssClass="preApproveSettingsForm" runat="server" DefaultButton="btn_PreApproveSettings">
    <div class="fieldLabelPair">
        <label for="<%= preapprove_FirstName.ClientID %>">
            First Name</label>
        <asp:TextBox ID="preapprove_FirstName" runat="server" Width="150px"></asp:TextBox>
        <asp:RequiredFieldValidator CssClass="error" ID="preapprove_FirstName_RequiredValidator" runat="server" ControlToValidate="preapprove_FirstName"
            ErrorMessage="First Name is required."></asp:RequiredFieldValidator>
    </div>
    <div class="fieldLabelPair">
        <label for="<%= preapprove_LastName.ClientID %>">
            Last Name</label>
        <asp:TextBox ID="preapprove_LastName" runat="server" Width="150px"></asp:TextBox>
        <asp:RequiredFieldValidator CssClass="error" ID="preapprove_LastName_RequiredValidator" runat="server" ControlToValidate="preapprove_LastName"
            ErrorMessage="Last Name is required."></asp:RequiredFieldValidator>
    </div>
    <div class="fieldLabelPair">
        <label for="<%= preapprove_Email.ClientID %>">
            Email</label>
        <asp:TextBox ID="preapprove_Email" runat="server" Width="200px"></asp:TextBox>
        <asp:Label Text="Email Listed Doesn't Exist" ID="BouncedLabel" runat="server" Visible="false" CssClass="error"/>
        <asp:RequiredFieldValidator CssClass="error" ID="preapprove_Email_RequiredValidator" runat="server" Display="Dynamic" ControlToValidate="preapprove_Email"
            ErrorMessage="Email is required."></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator CssClass="error" ID="preapprove_Email_RegexValidator" runat="server" ValidationExpression="^([a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]){1,70}$"
            ControlToValidate="preapprove_Email" ErrorMessage="Please enter a valid email address."></asp:RegularExpressionValidator>
    </div>
    <div class="fieldLabelPair">
        <label for="<%= preapprove_LastName.ClientID %>">Auto-Approve is</label>
        <asp:DropDownList ID="preapprove_Status" runat="server">
            <asp:ListItem Value="Off" Text="Off" />
            <asp:ListItem Value="OnForAll" Text="On (New &amp; Used)" />
        </asp:DropDownList>
    </div>
    <div class="formButtons">
        <asp:Panel ID="TermsAndConditions" CssClass="terms" runat="server">
            <h3>Terms of Service</h3>
            <p class="disclaimer">
                a. I hereby authorize the automated generation, updating and distribution of advertisements for my [new|used|used & new] vehicles. 
                I understand that these advertisements will be automatically updated and published for distribution. 
                I also hereby acknowledge that I am solely responsible for the accuracy of these advertisements, correcting 
                any inaccuracies in the advertisements immediately, and  ensuring that these advertisements are in 
                compliance with all applicable laws and regulations.</p>
            <p class="disclaimer">b. Please send a daily email of updated inventory advertisements for my review to the address listed above.</p>
            <p class="disclaimer">By clicking on 'I accept' below you are agreeing to the Terms of Service above.</p>
        </asp:Panel>
        <asp:Button ID="btn_PreApproveSettings" runat="server" CssClass="button submit" OnClick="btn_PreApproveSettings_Click" OnClientClick="_gaq.push(['_trackEvent', 'Dealer Preferences', 'Click', 'Pre-Approve Accept Button']);" Text="&nbsp;&nbsp;&nbsp;I ACCEPT&nbsp;&nbsp;(Activate Auto-Approve)&nbsp;&nbsp;&nbsp;" />
        
    </div>
    
</asp:Panel>
<asp:Button ID="ApprovalAllInventoryButton" runat="server" CssClass="button" Text="Re-Approve All Inventory" OnClick="OnReapproveClick" Visible="false"/>