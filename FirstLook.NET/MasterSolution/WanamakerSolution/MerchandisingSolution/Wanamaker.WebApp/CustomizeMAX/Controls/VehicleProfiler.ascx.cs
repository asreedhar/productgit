using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Vehicles;

namespace Wanamaker.WebApp.CustomizeMAX.Controls
{

    public partial class VehicleProfiler : System.Web.UI.UserControl
    {

        public event EventHandler VehicleProfileNames_Updated;

        public int BusinessUnitId
        {
            get
            {
                if (IsInCpoManagerMode)
                {
                    return CpoGroupId;
                }
                else
                {
                    object o = ViewState["busId"];
                    return (o == null) ? -1 : (int)o;
                }
            }
            set
            {
                ViewState["busId"] = value;
            }
        }

        public int CpoGroupId
        {
            get
            {
                object o = ViewState["cpoId"];
                return (o == null) ? -1 : (int)o;
            }
            set
            {
                ViewState["cpoId"] = value;
            }
        }



        public bool IsInCpoManagerMode
        {
            get
            {
                object o = ViewState["isCpo"];
                return (o == null) ? false : (bool)o;
            }
            set { ViewState["isCpo"] = value; }
        }

        public VehicleProfile profile
        {
            get
            {
                object o = ViewState["prof"];
                return (o == null) ? new VehicleProfile() : (VehicleProfile)o;
            }
            set
            {
                ViewState["prof"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ControlPanel.Visible = false;
                Summary.Visible = false;
            }
        }

        protected void CreateNew_Click(object sender, EventArgs e)
        {
            titleTB.Text = "New Profile";
            profile = new VehicleProfile();
            SetControls();
        }
        protected void Available_Bound(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;
            if (ddl == null) return;

            ddl.Items.Insert(0, "Choose Profile to edit...");
        }
        private void SetControls()
        {
            ControlPanel.Visible = true;
            SetSummary();
            SetModelList();
            SetTrimList();
        }
        private void SetSummary()
        {
            Summary.Visible = true;
            SummaryBullets.DataSource = profile.getSummaryList();
            titleLbl.Text = profile.Title;
            SummaryBullets.DataBind();

        }
        private void SetModelList()
        {
            ModelDDL.DataSource = profile.GetPossibleModels();
            ModelDDL.DataBind();
        }
        private void SetTrimList()
        {
            TrimDDL.DataSource = profile.GetPossibleTrims();
            TrimDDL.DataBind();
        }


        protected void Profiles_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {

            e.Command.Parameters["@BusinessUnitId"].Value = BusinessUnitId;

        }

        protected void CurrentProfile_Changed(object sender, EventArgs e)
        {
            if (VehicleProfileDDL.SelectedIndex > 0)
            {
                profile = VehicleProfile.Fetch(Int32.Parse(VehicleProfileDDL.SelectedValue));
                titleTB.Text = profile.Title;
                SetControls();
            }
        }


        protected void ChangeParam_Click(object sender, EventArgs e)
        {
            Button sndr = sender as Button;
            if (sndr == null) return;

            switch (sndr.CommandArgument)
            {

                case "Name":
                    profile.Title = titleTB.Text;
                    titleLbl.Text = profile.Title;
                    break;
                case "Year":


                    if (!string.IsNullOrEmpty(startYear.Text))
                    {
                        profile.StartYear = Int32.Parse(startYear.Text);
                    }
                    else
                    {
                        profile.StartYear = null;
                    }

                    if (!string.IsNullOrEmpty(endYear.Text))
                    {
                        profile.EndYear = Int32.Parse(endYear.Text);
                    }
                    else
                    {
                        profile.EndYear = null;
                    }


                    break;
                case "Age":
                    if (!string.IsNullOrEmpty(minAge.Text))
                    {
                        profile.MinAge = Int32.Parse(minAge.Text);
                    }
                    else
                    {
                        profile.MinAge = null;
                    }

                    if (!string.IsNullOrEmpty(maxAge.Text))
                    {
                        profile.MaxAge = Int32.Parse(maxAge.Text);
                    }
                    else
                    {
                        profile.MaxAge = null;
                    }
                    break;
                case "Price":
                    if (!string.IsNullOrEmpty(lowPriceTB.Text))
                    {
                        profile.MinPrice = Decimal.Parse(lowPriceTB.Text);
                    }
                    else
                    {
                        profile.MinPrice = null;
                    }

                    if (!string.IsNullOrEmpty(hiPriceTB.Text))
                    {
                        profile.MaxPrice = Decimal.Parse(hiPriceTB.Text);
                    }
                    else
                    {
                        profile.MaxPrice = null;
                    }
                    break;
                case "Certified":
                    int val = Int32.Parse(cert.SelectedValue);
                    if (val == 0)
                    {
                        profile.CertifiedStatusFilter = null;
                    }
                    else if (val == 1)
                    {
                        profile.CertifiedStatusFilter = true;
                    }
                    else if (val == 2)
                    {
                        profile.CertifiedStatusFilter = false;
                    }
                    break;
                case "Type":
                    profile.VehicleType = Int32.Parse(typeDropDown.SelectedValue);
                    break;
                case "Make":
                    if (MakeDDL.SelectedIndex != 0 &&
                        !profile.Makes.Contains(MakeDDL.SelectedValue))
                    {
                        profile.Makes.Add(MakeDDL.SelectedValue);
                    }

                    break;
                case "Model":
                    if (ModelDDL.SelectedIndex != 0 &&
                        !profile.Models.Contains(ModelDDL.SelectedValue))
                    {
                        profile.Models.Add(ModelDDL.SelectedValue);
                    }
                    break;
                case "Trim":
                    if (TrimDDL.SelectedIndex != 0 &&
                        !profile.Trims.Contains(TrimDDL.SelectedValue))
                    {
                        profile.Trims.Add(TrimDDL.SelectedValue);
                    }
                    break;
                case "Segment":
                    Segments segval = (Segments)Enum.Parse(typeof(Segments), SegmentDDL.SelectedValue);

                    if (SegmentDDL.SelectedIndex != 0 &&
                        !profile.Segments.Contains(segval))
                    {
                        profile.Segments.Add(segval);
                    }
                    break;

                case "MarketClass":
                    if (MktClassDDL.SelectedIndex != 0 &&
                        !profile.MktClasses.Contains(MktClassDDL.SelectedValue))
                    {
                        profile.MktClasses.Add(MktClassDDL.SelectedValue);
                    }
                    break;
                case "ClearMake":
                    profile.Makes.Clear();

                    break;
                case "ClearModel":
                    profile.Models.Clear();
                    break;
                case "ClearTrim":
                    profile.Trims.Clear();
                    break;
                case "ClearSegment":
                    profile.Segments.Clear();
                    break;
                case "ClearMarketClass":
                    profile.MktClasses.Clear();
                    break;
                default:
                    break;
            }
            profile.Save(BusinessUnitId);

            if (sndr.CommandArgument == "Name")
            {
                VehicleProfileDDL.DataBind();
                if (VehicleProfileNames_Updated != null)
                {
                    VehicleProfileNames_Updated(this, new EventArgs());
                }
            }

            //if we updated the year, update the lists
            if (sndr.CommandArgument == "Year")
            {
                SetModelList();
                SetTrimList();
            }

            if (sndr.CommandArgument != "Name"
                && sndr.CommandArgument != "Certified"
                && sndr.CommandArgument != "Price"
                && sndr.CommandArgument != "Age"
                && sndr.CommandArgument != "Type")
            {
                SetModelList();
                SetTrimList();
            }

            SetSummary();


        }

        protected void DDL_Bound(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;
            if (ddl != null)
            {
                ddl.Items.Insert(0, "Select...");
            }
        }

    }
}