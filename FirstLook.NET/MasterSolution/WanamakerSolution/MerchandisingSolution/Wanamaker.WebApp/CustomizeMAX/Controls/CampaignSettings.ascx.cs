using System;
using System.Web.UI.WebControls;
using FirstLook.Merchandising.DomainModel.Postings;
using Wanamaker.WebApp.Controls;

namespace Wanamaker.WebApp.CustomizeMAX.Controls
{

    public partial class CampaignSettings : BusinessUnitUserControl
    {
        protected void CreateDefault_Campaign(object sender, EventArgs e)
        {
            CampaignEventDataSource.Upsert(new CampaignEvent(BusinessUnitID));
            CampaignGV.DataBind();
            CampaignGV.EditIndex = 0;

        }

        protected void Campaigns_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["businessUnitId"] = BusinessUnitID;
        }

        protected void AddNew_Click(object sender, EventArgs e)
        {
            CampaignDS.Insert();
        }

        protected static string GetNewDescriptionText(object o, string templateName, string themeName)
        {
            if (!(o is bool)) 
                throw new ArgumentException("must be a boolean");

            if ((bool)o)
            {
                if (string.IsNullOrEmpty(themeName))
                {
                    themeName = "no";
                }
                return string.Format("Create new highlights using {0} ({1} theme)", templateName, themeName);
            }
            return "Do not create new highlights";

        }
        protected void Templates_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["businessUnitId"] = BusinessUnitID;
        }

        protected void Row_Bound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow gvr = e.Row;
            if (gvr.RowType == DataControlRowType.DataRow)
            {
                DropDownList mission = gvr.FindControl("MissionDDL") as DropDownList;
                DropDownList template = gvr.FindControl("TemplateDDL") as DropDownList;
                CampaignEvent evt = ((CampaignEvent)gvr.DataItem);
                if (mission != null && evt.ThemeId.HasValue)
                {
                    mission.SelectedValue = evt.ThemeId.Value.ToString();
                }

                if (template != null && evt.TemplateId.HasValue)
                {
                    template.SelectedValue = evt.TemplateId.Value.ToString();
                }
            }
        }


        protected void Campaign_Updating(object sender, ObjectDataSourceMethodEventArgs e)
        {
            GridViewRow gvr = CampaignGV.Rows[CampaignGV.EditIndex];

            CampaignEvent ce = new CampaignEvent(BusinessUnitID);

            ce.VehicleAgeInDaysForAction = int.Parse(e.InputParameters["VehicleAgeInDaysForAction"].ToString());

            //apply the theme/mission
            DropDownList ddl = gvr.FindControl("MissionDDL") as DropDownList;
            if (ddl == null) throw new ApplicationException("Could not find MissionDDL");

            if (!string.IsNullOrEmpty(ddl.SelectedValue))
            {
                ce.ThemeId = int.Parse(ddl.SelectedValue);
            }

            DropDownList templateDDL = gvr.FindControl("TemplateDDL") as DropDownList;
            if (templateDDL == null) throw new ApplicationException("Could not find TemplateDDL");

            if (!string.IsNullOrEmpty(templateDDL.SelectedValue))
            {
                ce.TemplateId = int.Parse(templateDDL.SelectedValue);
            }

            //set the event id
            DataKey eventIdDK = CampaignGV.DataKeys[CampaignGV.EditIndex];
            if (eventIdDK == null) throw new ApplicationException("Data key must include campaign event Id");

            ce.CampaignEventId = (int)eventIdDK.Value;


            ce.RegenerateDescription = (bool)e.InputParameters["RegenerateDescription"];

            /*future:  add fields for pricing events*/

            ce.Upsert();
            
            e.Cancel = true;


        }


        protected void Campaign_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            CampaignEvent ce = new CampaignEvent(BusinessUnitID);
            DropDownList mission = CampaignGV.FooterRow.FindControl("MissionDDL") as DropDownList;
            DropDownList template = CampaignGV.FooterRow.FindControl("TemplateDDL") as DropDownList;

            if (mission != null && !string.IsNullOrEmpty(mission.SelectedValue))
            {
                ce.ThemeId = int.Parse(mission.SelectedValue);
            }

            if (template != null && !string.IsNullOrEmpty(template.SelectedValue))
            {
                ce.TemplateId = int.Parse(template.SelectedValue);
            }

            ce.RegenerateDescription = ce.TemplateId.HasValue || ce.ThemeId.HasValue;

            TextBox ageTB = CampaignGV.FooterRow.FindControl("ageInDaysF") as TextBox;
            if (ageTB != null && !string.IsNullOrEmpty(ageTB.Text))
            {
                ce.VehicleAgeInDaysForAction = int.Parse(ageTB.Text);
            }
            e.InputParameters["campaignEvent"] = ce;

        }

        protected void Campaign_Deleting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            e.InputParameters["businessUnitId"] = BusinessUnitID;
        }
    }
}