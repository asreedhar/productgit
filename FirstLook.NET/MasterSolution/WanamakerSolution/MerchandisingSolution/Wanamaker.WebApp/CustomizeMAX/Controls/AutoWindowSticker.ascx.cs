﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using BulkWindowSticker;
using Wanamaker.WebApp.Controls;

namespace Wanamaker.WebApp.CustomizeMAX.Controls
{
    public partial class AutoWindowSticker : BusinessUnitUserControl
    {

        protected override void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                // save the business unit id from the context to make it page level, 
                // if the users switches business units (new page), it will save the settings on this page to the dealer from which the settings 
                // came from and not to the dealer in the other page (which sets the context)

                ViewState["BU"] = BusinessUnitID;
                ViewState["s3url"] = ConfigurationManager.AppSettings["window_stickers_external_url"];


                try
                {
                    var windowStickerRepository = new WindowStickerRepository();

                    var windowStickerSetting = windowStickerRepository.GetAutoWindowSticker(Convert.ToInt32(ViewState["BU"]));

                    // set optional email
                    if(!String.IsNullOrEmpty(windowStickerSetting.Email))
                        txtEmail.Text = windowStickerSetting.Email;

                    ddlMinimumDays.SelectedValue = windowStickerSetting.MinimumDays.ToString(CultureInfo.InvariantCulture);

                    // show history
                    rptHistory.DataSource =
                        windowStickerRepository.GetAutoWindowStickerHistory(Convert.ToInt32(ViewState["BU"]),
                                                                            DateTime.Now.AddDays(-14));
                    rptHistory.DataBind();

                }
                catch (Exception ex)
                {
                    lblMessage.Text = ex.ToString();
                }

            }

        }
        protected string GetPdfUrl(object s3Object)
        {
            string url = @"~/print/BulkWindowStickerDisplay.aspx?PrintBatchId=" + s3Object;

            return url;

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                // check the validitiy of the emails
                string emails = "";
                if (!string.IsNullOrEmpty(txtEmail.Text.Trim()))
                {
                    string[] potencialAdressList = txtEmail.Text.Trim().Split(',', ';');
                    var validAddressList = new List<string>();

                    foreach (string email in potencialAdressList)
                    {
                        if (!string.IsNullOrWhiteSpace(email))
                        {
                            if (!IsValidEmail(email))
                                throw new ApplicationException(string.Format("The email address: {0} is not valid",email));

                            // add it to the list
                            validAddressList.Add(email);
                        }
                    }

                    if(validAddressList.Any())
                        emails = string.Join(",", validAddressList);
                }

                txtEmail.Text = emails;

                // save entry                
                var windowStickerRepository = new WindowStickerRepository();

                windowStickerRepository.SaveAutoWindowSticker(Convert.ToInt32(ViewState["BU"]),
                                                                    emails,
                                                                    false,
                                                                    Context.User.Identity.Name,
                                                                    Convert.ToInt16(ddlMinimumDays.SelectedValue)
                                                                );

            }
            catch (ApplicationException ex)
            {
                lblMessage.Text = ex.Message;
            }
            catch (SqlException ex)
            {
                lblMessage.Text = ex.Message;
            }
            catch(Exception ex)
            {
                lblMessage.Text = ex.ToString();

            }

        }
        protected bool IsValidEmail(string email)
        {
            bool isValid = true;

            try
            {
                // give it a try
                var mailAddress = new MailAddress(email);
            }
            catch (Exception)
            {
                isValid = false;
            }

            return isValid;

        }
    }
}