<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="EditSnippetIntro.ascx.cs" Inherits="Wanamaker.WebApp.CustomizeMAX.Controls.EditSnippetIntro" %>
<%@ Register TagPrefix="carbuilder" TagName="BusinessUnitUserControl" Src="~/Controls/BusinessUnitUserControl.ascx" %>
<asp:SqlDataSource ID="ThemeDS" runat="server" SelectCommandType="StoredProcedure" ConnectionString='<%$ ConnectionStrings:Merchandising %>'
    SelectCommand="templates.Themes#Fetch">
    <SelectParameters>
        <asp:Parameter Name="BusinessUnitID" DefaultValue="100150" ConvertEmptyStringToNull="true" Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>
<script type="text/javascript">
    function ValidateJQ(x, y) {
        var text = (document.getElementById('lb')).value;
        try {
            if ($(text).length > 0) {
                y.IsValid = false;
            } else {
                y.IsValid = true;
            }
        } catch (e) {
            y.IsValid = true;
        }
    }
</script>
<asp:SqlDataSource runat="server" ID="TaglineDS" ConnectionString='<%$ ConnectionStrings:Merchandising %>' SelectCommand="templates.SampleTextLibrary#Fetch"
    SelectCommandType="StoredProcedure" OnSelecting="Tagline_Selecting" OnSelected="Tag_Sel" InsertCommand="templates.updateOrCreateSampleText"
    InsertCommandType="StoredProcedure" UpdateCommand="templates.updateOrCreateSampleText" UpdateCommandType="StoredProcedure"
    DeleteCommand="templates.SampleText#Delete" DeleteCommandType="StoredProcedure" OnInserting="Tagline_Command" OnInserted="Tagline_Inserted"
    OnUpdating="Tagline_Updating" OnDeleting="Tagline_Deleting">
    <SelectParameters>
        <asp:Parameter Name="BusinessUnitId" Type="Int32" />
        <asp:Parameter Name="LibraryBusinessUnitId" Type="Int32" />
        <asp:Parameter Name="BlurbId" Type="Int32" />
        <asp:Parameter Name="ThemeId" Type="Int32" DefaultValue="" ConvertEmptyStringToNull="true" />
    </SelectParameters>
    <UpdateParameters>
        <asp:Parameter Name="BusinessUnitId" Type="Int32" />
        <asp:Parameter Name="BlurbId" Type="Int32" />
        <asp:Parameter Name="BlurbTextId" Type="Int32" />
        <asp:Parameter Name="BlurbPreText" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
        <asp:Parameter Name="BlurbPostText" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
        <asp:Parameter Name="Active" Type="Boolean" DefaultValue="True" />
        <asp:Parameter Name="VehicleProfileId" Type="Int32" />
        <asp:Parameter Name="ThemeId" Type="Int32" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Name="BusinessUnitId" Type="Int32" />
        <asp:Parameter Name="BlurbPreText" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
        <asp:Parameter Name="BlurbPostText" Type="String" DefaultValue="" ConvertEmptyStringToNull="false" />
        <asp:Parameter Name="VehicleProfileId" Type="Int32" />
        <asp:Parameter Name="ThemeId" Type="Int32" />
        <asp:Parameter Name="BlurbId" Type="Int32" />
    </InsertParameters>
    <DeleteParameters>
        <asp:Parameter Name="BlurbTextId" Type="Int32" />
        <asp:Parameter Name="BusinessUnitId" Type="Int32" />
    </DeleteParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="ProfilesDS" runat="server" ConnectionString='<%$ ConnectionStrings:Merchandising %>' SelectCommand="Select vehicleProfileId, [title] FROM templates.vehicleProfiles WHERE businessUnitId = @BusinessUnitId OR businessUnitId = 100150"
    OnSelecting="Profiles_Selecting">
    <SelectParameters>
        <asp:Parameter Name="BusinessUnitId" DefaultValue="100150" />
    </SelectParameters>
</asp:SqlDataSource>
<asp:GridView ID="TaglineGV" runat="server" CssClass="taglinesTable" DataSourceID="TaglineDS" DataKeyNames="blurbTextId" OnSelectedIndexChanged="Tagline_Selected"
    OnDataBinding="GV_Binding" AutoGenerateColumns="false" ShowFooter="true" BorderStyle="None" BorderWidth="0" GridLines="None">
    <EmptyDataTemplate>
        <h3>No custom dealer phrases saved for this item</h3>
        <asp:Button ID="createOne" runat="server" Text="Customize Now" OnClick="Customize_Clicked" />
    </EmptyDataTemplate>
    <Columns>
        <asp:TemplateField HeaderText="Advertising Phrase" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle">
            <ItemTemplate>
                <asp:Label ID="lb" runat="server" Text='<%# Bind("BlurbPreText") %>' Width="380px"></asp:Label>
                <span id="sp1" runat="server" class='<%# GetIsPostVisibleClass() %>'>...
                    <asp:Label ID="lb2" runat="server" Text='<%# Bind("BlurbPostText") %>'></asp:Label>
                </span>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox ID="lb" runat="server" Text='<%# Bind("BlurbPreText") %>' TextMode="MultiLine" Rows="3" Width="390px"></asp:TextBox>
                <asp:CustomValidator ID="CustomValidator2" runat="server" ErrorMessage="HTML Tags Not Allowed" ControlToValidate="lb" ClientValidationFunction="ValidateJQ"></asp:CustomValidator>
                <span id="sp1" runat="server" class='<%# GetIsPostVisibleClass() %>'>...
                    <asp:TextBox ID="lb1" runat="server" Text='<%# Bind("BlurbPostText") %>' Width="80px"></asp:TextBox>
                </span>
            </EditItemTemplate>
            <FooterTemplate>
                <asp:TextBox ID="BlurbPreText" runat="server" Text=""
                    TextMode="MultiLine" Width="380px"></asp:TextBox>
                <span id="sp2" runat="server" class='<%# GetIsPostVisibleClass() %>'>...
                    <asp:TextBox ID="BlurbPostText" runat="server" Text="" TextMode="MultiLine" Width="80px"></asp:TextBox>
                </span>
            </FooterTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Vehicle" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
            <EditItemTemplate>
                <asp:DropDownList ID="TagVP" runat="server" DataSourceID="ProfilesDS" DataTextField="Title" DataValueField="VehicleProfileId"
                    OnDataBound="Profile_Bound" SelectedValue='<%# Bind("VehicleProfileId") %>' Width="140">
                </asp:DropDownList>
            </EditItemTemplate>
            <ItemTemplate>
                <%# Eval("Title") %>
            </ItemTemplate>
            <FooterTemplate>
                <asp:DropDownList ID="ProfileDDL" runat="server" DataSourceID="ProfilesDS" DataTextField="Title" DataValueField="VehicleProfileId"
                    OnDataBound="Profile_Bound" Width="140">
                </asp:DropDownList>
            </FooterTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Theme" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
            <EditItemTemplate>
                <asp:DropDownList ID="MissionDDL" runat="server" DataSourceID="ThemeDS" DataTextField="name" DataValueField="id" SelectedValue='<%# Bind("ThemeId") %>'
                    AppendDataBoundItems="true" Width="140">
                    <asp:ListItem Text="No theme..." Value=""></asp:ListItem>
                </asp:DropDownList>
            </EditItemTemplate>
            <ItemTemplate>
                <%# Eval("ThemeName") %>
            </ItemTemplate>
            <FooterTemplate>
                <asp:DropDownList ID="MissionDDL" runat="server" DataSourceID="ThemeDS" DataTextField="name" DataValueField="id" AppendDataBoundItems="true" Width="140">
                    <asp:ListItem Text="Any theme..." Value=""></asp:ListItem>
                </asp:DropDownList>
            </FooterTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Active?" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
            <ItemTemplate>
                <asp:CheckBox ID="cb1" runat="server" Enabled="false" Checked='<%# Bind("Active") %>' />
            </ItemTemplate>
            <EditItemTemplate>
                <asp:CheckBox ID="cb1" runat="server" Checked='<%# Bind("Active") %>' />
            </EditItemTemplate>
            <FooterTemplate>
            </FooterTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:LinkButton ID="linkb1" runat="server" Text="Edit" Visible='<%# !ShowFirstLookLibrary %>' CommandName="Edit"></asp:LinkButton>
                <asp:LinkButton ID="linkb2" runat="server" Visible='<%# !ShowFirstLookLibrary %>' Text='Delete' CommandName="Delete"></asp:LinkButton>
                <asp:LinkButton ID="linkb3" runat="server" Visible='<%# ShowFirstLookLibrary %>' Text='<%# GetDeleteText((bool)Eval("Active")) %>'
                    OnClick="ToggleEnabled_Click" CommandArgument='<%# Eval("BlurbTextId") %>'></asp:LinkButton>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:LinkButton ID="linkb1" runat="server" Text="Update" CommandName="Update"></asp:LinkButton>
                <asp:LinkButton ID="linkb2" runat="server" Text="Cancel" CommandName="Cancel"></asp:LinkButton>
            </EditItemTemplate>
            <FooterTemplate>
                <asp:Button ID="AddSnippet" runat="server" CssClass="button" Text="Add New" OnClick="AddSnippet_Click" />
            </FooterTemplate>
        </asp:TemplateField>
    </Columns>
    <FooterStyle CssClass="tableFooter" />
    <RowStyle BackColor="#f4f4f4" />
    <EditRowStyle BackColor="#ffffcc" />
    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
    <HeaderStyle BackColor="#DDDDDD" Font-Bold="True" ForeColor="#555555" />
</asp:GridView>
