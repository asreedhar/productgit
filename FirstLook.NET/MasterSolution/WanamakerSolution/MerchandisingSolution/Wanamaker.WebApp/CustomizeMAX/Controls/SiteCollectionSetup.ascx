<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="SiteCollectionSetup.ascx.cs" Inherits="Wanamaker.WebApp.CustomizeMAX.Controls.SiteCollectionSetup" %>
<%@ Register TagPrefix="carbuilder" TagName="BusinessUnitUserControl" Src="~/Controls/BusinessUnitUserControl.ascx" %>
<div class="clearfix">
    <asp:SqlDataSource ID="DestDS" runat="server" ConnectionString="<%$ ConnectionStrings:Merchandising %>" SelectCommand="SELECT description, destinationID FROM settings.EdtDestinations WHERE active = 1">
    </asp:SqlDataSource>
    <asp:GridView ID="CollectionGV" runat="server" AutoGenerateColumns="False" DataSourceID="collectionSettingsDS" DataKeyNames="DestinationID"
        OnRowEditing="Destination_Editing" OnRowUpdating="Destination_Updating" OnRowDataBound="Destination_Bound" ShowFooter="true">
        <EmptyDataTemplate>
            <asp:DropDownList ID="DestDDL" runat="server" DataSourceID="DestDS" DataTextField="description" DataValueField="destinationID">
            </asp:DropDownList>
            <asp:Button ID="startCollections" runat="server" Text="Setup Data Input/Ouput" OnClick="InitialDataInOut_Click" />
        </EmptyDataTemplate>
        <Columns>
            <asp:CheckBoxField DataField="activeFlag" HeaderText="Active?" />
            <asp:TemplateField HeaderText="Send Ads?">
                <ItemTemplate>
                    <asp:CheckBox ID="releaseCB" runat="server" Checked='<%# Eval("releaseEnabled") %>' Enabled="False" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:CheckBox ID="releaseCB" runat="server" Checked='<%# Bind("releaseEnabled") %>' />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Use for Analytics?">
                <ItemTemplate>
                    <asp:CheckBox ID="collectCB" runat="server" Checked='<%# Eval("collectionEnabled") %>' Enabled="False" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:CheckBox ID="collectCB" runat="server" Checked='<%# Bind("collectionEnabled") %>' />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Types&nbspListed on&nbspSite">
                <ItemTemplate>
                    <ul>
                        <li><asp:CheckBox runat="server" ID="ListNew" Checked='<%# Eval("ListNew") %>' Enabled="False"/> <asp:Label runat="server" AssociatedControlID="ListNew">New</asp:Label></li>
                        <li><asp:CheckBox runat="server" ID="ListUsed" Checked='<%# Eval("ListUsed") %>' Enabled="False"/> <asp:Label runat="server" AssociatedControlID="ListUsed">Used</asp:Label></li>
                        <li><asp:CheckBox runat="server" ID="ListCertified" Checked='<%# Eval("ListCertified") %>' Enabled="False" /> <asp:Label runat="server" AssociatedControlID="ListCertified">Certified</asp:Label></li>
                    </ul>
                </ItemTemplate>
                <EditItemTemplate>
                    <ul>
                        <li><asp:CheckBox runat="server" ID="ListNew" Checked='<%# Bind("ListNew") %>'/> <asp:Label runat="server" AssociatedControlID="ListNew">New</asp:Label></li>
                        <li><asp:CheckBox runat="server" ID="ListUsed" Checked='<%# Bind("ListUsed") %>'/> <asp:Label runat="server" AssociatedControlID="ListUsed">Used</asp:Label></li>
                        <li><asp:CheckBox runat="server" ID="ListCertified" Checked='<%# Bind("ListCertified") %>'/> <asp:Label runat="server" AssociatedControlID="ListCertified">Certified</asp:Label></li>
                    </ul>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Source Name" SortExpression="description" ItemStyle-CssClass="relative" ItemStyle-VerticalAlign="Top">
                <EditItemTemplate>
                    <asp:DropDownList ID="DestDDL" runat="server" DataSourceID="DestDS" DataTextField="description" DataValueField="destinationID"
                        OnDataBound="EditDest_Bound" CssClass="destinationSelect">
                    </asp:DropDownList>
                    <asp:HiddenField ID="v1" runat="server" Value='<%# Bind("PushFrequency") %>' />
                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("SendPhotos") %>' />
                    <asp:HiddenField ID="HiddenField2" runat="server" Value='<%# Bind("SendOptions") %>' />
                    <asp:HiddenField ID="HiddenField3" runat="server" Value='<%# Bind("SendDescription") %>' />
                    <asp:HiddenField ID="HiddenField4" runat="server" Value='<%# Bind("SendPricing") %>' />
                    <asp:HiddenField ID="HiddenField5" runat="server" Value='<%# Bind("SendVideos") %>' />
                    <asp:HiddenField ID="HiddenField6" runat="server" Value='<%# Bind("SendNewCars") %>' />
                    <asp:HiddenField ID="HiddenField7" runat="server" Value='<%# Bind("SendUsedCars") %>' />
                    <asp:HiddenField ID="HiddenField8" runat="server" Value='<%# Bind("PushFrequency") %>' />
                    <p class="credentialDisclaimer">
                        I am an authorized
                        <%# Eval("description") %>
                        user with a valid username and password. I would like to automate the retrieval of my dealership�s online performance data
                        daily from
                        <%# Eval("description") %>
                        for online inventory performance analysis.</p>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("description") %>'></asp:Label>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:DropDownList ID="DestDDL" runat="server" DataSourceID="DestDS" DataTextField="description" DataValueField="destinationID">
                    </asp:DropDownList>
                    <p id="AddSourceCredentialDisclaimer" class="credentialDisclaimer addCredentialDisclaimer" runat="server"></p>
                    
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Username" ItemStyle-VerticalAlign="Top">
                <ItemTemplate>
                    <%# Eval("username") %>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="username" runat="server" Text='<%# Bind("username") %>'></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="usernameTB" Width="120px" runat="server" Text=""></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Password" ItemStyle-VerticalAlign="Top">
                <ItemTemplate>
                    <asp:Literal ID="lit1" runat="server" Text='<%# GetPWDisplay((string)Eval("username"), (string)Eval("Password")) %>'></asp:Literal>
                    <asp:Panel ID="failedLoginPanel" runat="server" Visible="false" CssClass="warning">
                        Last login failed.
                        <asp:LinkButton ID="clearLoginFailure" runat="server" OnClick="ClearLoginFailure">Reset</asp:LinkButton>
                    </asp:Panel>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox TextMode="Password" ID="pwBox" runat="server" Text='<%# Bind("Password") %>'></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox TextMode="Password" ID="passwordTB" Width="120px" runat="server" Text='<%# Bind("Password") %>'></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="ed" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Delete" Text="Delete"></asp:LinkButton>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:LinkButton ID="ed" runat="server" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:Button ID="newCollect" runat="server" CssClass="button small" OnClick="AddNewCollectionSource_Click" Text="+ Add" />
                </FooterTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="collectionSettingsDS" runat="server" ConnectionString="<%$ ConnectionStrings:Merchandising %>" SelectCommandType="StoredProcedure"
        UpdateCommandType="StoredProcedure" InsertCommandType="StoredProcedure" SelectCommand="Release.ListingSitePreferences#Fetch"
        UpdateCommand="Release.ListingSitePreferences#Upsert" InsertCommand="Release.ListingSitePreferences#Upsert" DeleteCommand="DELETE FROM settings.DealerListingSites WHERE businessUnitId = @BusinessUnitId AND destinationId = @DestinationId"
        OnInserting="Collection_Inserting" OnInserted="Collection_Inserted" OnUpdating="Collection_Updating" OnSelecting="Collection_Selecting"
        OnDeleting="Collection_Deleting">
        <UpdateParameters>
            <asp:Parameter Name="Username" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="Password" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="ActiveFlag" Type="Boolean" />
            <asp:Parameter Name="BusinessUnitID" />
            <asp:Parameter Name="DestinationID" />
            <asp:Parameter Name="CollectionEnabled" Type="Boolean" />
            <asp:Parameter Name="ReleaseEnabled" Type="Boolean" />
            <asp:Parameter Name="NewDestinationID" />
            <asp:Parameter Name="ListNew" Type="Boolean" />
            <asp:Parameter Name="ListUsed" Type="Boolean" />
            <asp:Parameter Name="ListCertified" Type="Boolean" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="Username" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="Password" DefaultValue="" ConvertEmptyStringToNull="false" />
            <asp:Parameter Name="ActiveFlag" DefaultValue="1" />
            <asp:Parameter Name="BusinessUnitID" />
            <asp:Parameter Name="DestinationID" />
        </InsertParameters>
        <SelectParameters>
            <asp:Parameter Name="BusinessUnitID" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="BusinessUnitID" />
            <asp:Parameter Name="DestinationID" />
        </DeleteParameters>
    </asp:SqlDataSource>
</div>
