using System;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Command;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Dashboard.Entities;
using FirstLook.Merchandising.DomainModel.Enumerations;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using Wanamaker.WebApp.Controls;

namespace Wanamaker.WebApp.CustomizeMAX.Controls
{

    public partial class ReportSettings : BusinessUnitUserControl
    {
        public const string LowActivityThresholdHlep =
            "This relates to the percent of Search Result Pages that get a click thru to the Vehicle Detail Page.  If below this percentage, the vehicle is considered Low Activity.  For example, if a car shows up in search results 100 times, and the Low Activity Threshold is 2%, then if users clicked through to the detail page 2 or fewer times, the car would be Low Activity.";

        public const string HighActivityThresholdHlep =
            "This relates to the percent of Search Result Pages that get a click thru to the Vehicle Detail Page.  If above this percentage, the vehicle is considered High Activity.  For example, if a car shows up in search results 100 times, and the High Activity Threshold is 4%, then if users clicked through to the detail page 4 or more times, the car would be High Activity.";

        public const string MinSearchCountThresholdHelp =
            "We won�t consider a car to be Low Activity or High Activity until it has shown up in search results at least this many times.";

        
        protected void Page_Init(object sender, EventArgs e)
        {
        }

        protected override void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadSettings();
            }
        }

        public void Refresh()
        {
            LoadSettings();
        }

        private void LoadSettings()
        {
            string default_month = null;

            // load available budgets for specified month
            default_month = Request.QueryString["d"];
            InitChartSelect( default_month );

            var optionsCommand = new GetReportSettings(BusinessUnitID);
            AbstractCommand.DoRun(optionsCommand);

            if (optionsCommand.HasSettings)
            {
                txtLowActivityThreshold.Text = string.Format("{0:0.00}", optionsCommand.LowActivityThreshold * 100);
                txtHighActivityThreshold.Text = string.Format("{0:0.00}", optionsCommand.HighActivityThreshold * 100);
                txtMinSearchCountThreshold.Text = optionsCommand.MinSearchCountThreshold.ToString();
                var ttmDaysTest = optionsCommand.TimeToMarketGoalDays.ToString();
                txtTimeToMarketGoalDays.Text = String.IsNullOrWhiteSpace(ttmDaysTest) ? "5" : ttmDaysTest;

                ageInDays.SelectedValue = optionsCommand.VehicleOnlineStatusReport_MinAgeDays.ToString();
                var adCompleteSettings = (AdCompleteFlags)optionsCommand.AdCompleteFlags;

                
                TTMDescription.Checked = adCompleteSettings.HasFlag(AdCompleteFlags.Description);
                TTMPhotos.Checked = adCompleteSettings.HasFlag(AdCompleteFlags.MinimumPhotos);
                TTMPrice.Checked = adCompleteSettings.HasFlag(AdCompleteFlags.Price);
               
                settings.Visible = true;
                noSettings.Visible = false;
            }
            else
            {
                settings.Visible = false;
                noSettings.Visible = true;
            }

            MerchandisingGraphDefault.DataSource = GetMerchandisingGraphOptions();
            MerchandisingGraphDefault.DataValueField = "Value";
            MerchandisingGraphDefault.DataTextField = "Text";
            MerchandisingGraphDefault.DataBind();
            MerchandisingGraphDefault.Items.FindByValue(((int) optionsCommand.MerchandisingGraphDefault).ToString(CultureInfo.InvariantCulture)).Selected = true;
        }

        private object GetMerchandisingGraphOptions()
        {
            var collection = new ListItemCollection();

            foreach (var li in from object graphSetting in Enum.GetValues(typeof (MerchandisingGraphSettings)) 
                               select new ListItem(
                                   Enum.GetName(typeof (MerchandisingGraphSettings), graphSetting).HumanizeString(), 
                                   Convert.ToString((int)graphSetting)
                                ))
            {
                collection.Add(li);
            }

            return collection;
        }
        
        protected void OnSave(object sender, EventArgs e)
        {
            var lowActivityThreshold = Single.Parse(txtLowActivityThreshold.Text) / 100;
            var highActivityThreshold = Single.Parse(txtHighActivityThreshold.Text) / 100;
            var minSearchCountThreshold = int.Parse(txtMinSearchCountThreshold.Text);
            var notOnlineReportMinAgeDays = int.Parse(ageInDays.SelectedValue);
            var adCompleteFlags = GetAdCompleteFlagsSetting();
            var merchandisingGraphDefault = int.Parse(MerchandisingGraphDefault.SelectedValue);
            var timeToMarketGoalDays = int.Parse(String.IsNullOrEmpty(txtTimeToMarketGoalDays.Text) ? "0" : txtTimeToMarketGoalDays.Text);

            var reportSettings = new SetReportSettings(BusinessUnitID, lowActivityThreshold, highActivityThreshold, minSearchCountThreshold, notOnlineReportMinAgeDays, adCompleteFlags, merchandisingGraphDefault, timeToMarketGoalDays);
            AbstractCommand.DoRun(reportSettings);
        }

        private int GetAdCompleteFlagsSetting()
        {
            AdCompleteFlags setting = AdCompleteFlags.MinimumPhotos | AdCompleteFlags.Description | AdCompleteFlags.Price;

            if (TTMDescription.Checked == false)
                setting &= ~AdCompleteFlags.Description;
            if (TTMPhotos.Checked == false)
                setting &= ~AdCompleteFlags.MinimumPhotos;
            if (TTMPrice.Checked == false)
                setting &= ~AdCompleteFlags.Price;

            return (int)setting;
        }
        
        protected void HistoricalDateDDL_Changed(object sender, EventArgs e)
        {
            BudgetSource.SelectParameters.SetDirty();
            BudgetInfo.DataBind();
            BudgetSource.DataBind();
        }

        protected void OnSelectBudgetData(object sender, ObjectDataSourceSelectingEventArgs e)
        {	
			e.InputParameters["businessUnitId"] = BusinessUnitID;
            e.InputParameters["monthApplicable"] = HistoricalDateDDL.SelectedValue;

        }

        protected void SetDestination(object sender, EventArgs e)
        {
            var ddl = sender as DropDownList;
            if (ddl == null) return;

            var gvr = ddl.NamingContainer as GridViewRow;
            if (gvr == null) return;

            var sitebudget = gvr.DataItem as SiteBudget;
            if (sitebudget == null) return;

            var destination = sitebudget.DestinationId;
            ddl.SelectedValue = destination.ToString();

        }


        protected void UpdateBudgetRow(object sender, GridViewUpdateEventArgs e)
        {
            if (e.OldValues["Budget"] == e.NewValues["Budget"])
                return;

            var destinationId = BudgetInfo.DataKeys[e.RowIndex]["DestinationId"];
            var monthApplicable = DateTime.Parse(HistoricalDateDDL.SelectedValue);
            var updatedOn = DateTime.Now;
            
            e.NewValues.Add("BusinessUnitId", BusinessUnitID);
            e.NewValues.Add("DestinationId", destinationId);
            e.NewValues.Add("MonthApplicable", monthApplicable);
            // e.NewValues["Budget"]
            // e.NewValues["DescriptionText"]
            e.NewValues.Add("UpdatedOn", updatedOn);
        }

        protected void CreateNewBudget(object sender, EventArgs e)
        {
            var test = BudgetInfo.FooterRow;

            var businessUnitId = BusinessUnitID;
            
            var monthApplicable = DateTime.Parse(HistoricalDateDDL.SelectedValue);

            var button = sender as Button;
            if (button == null) return;

            var destinationList = BudgetInfo.FooterRow.FindControl("DestDDL") as DropDownList;
            if (destinationList == null) return;

            int destinationId;
            if (!int.TryParse(destinationList.SelectedValue, out destinationId))
            {
                return;
            }

            foreach (var row in BudgetInfo.DataKeys)
            {
                if ((int)(row as DataKey).Value == destinationId)
                {
                    return;
                }
            }

            var textBox = BudgetInfo.FooterRow.FindControl("budget_insert") as TextBox;
            if (textBox == null) return;
            
            int budget;
            if (!int.TryParse(textBox.Text, out budget))
            {
                return;
            }

            var package_input = button.NamingContainer.FindControl("package_insert") as TextBox;
            if (package_input == null) return;

            var newBudget = new SiteBudget
            {
                BusinessUnitId = businessUnitId,
                DestinationId = destinationId,
                MonthApplicable = monthApplicable,
                Budget = budget,
                DescriptionText = package_input.Text
            };

            new SiteBudgetRepository().InsertOrUpdate(newBudget);

            BudgetInfo.DataBind();
        }

        protected void CreateInitialBudget(object sender, EventArgs e)
        {
            var businessUnitId = BusinessUnitID;

            var monthApplicable = DateTime.Parse(HistoricalDateDDL.SelectedValue);

            var button = sender as Button;
            if (button == null) return;

            
            var destinationList = button.NamingContainer.FindControl("DestDDL") as DropDownList;
            if (destinationList == null) return;

            int destinationId;
            if (!int.TryParse(destinationList.SelectedValue, out destinationId))
            {
                return;
            }

            var textBox = button.NamingContainer.FindControl("budget_insert") as TextBox;
            if (textBox == null) return;

            int budget;
            if (!int.TryParse(textBox.Text, out budget))
            {
                return;
            }

            var package_input = button.NamingContainer.FindControl("package_insert") as TextBox;
            if (package_input == null) return;
            
            var newBudget = new SiteBudget
            {
                BusinessUnitId = businessUnitId,
                DestinationId = destinationId,
                MonthApplicable = monthApplicable,
                Budget = budget,
                DescriptionText = package_input.Text
            };

            new SiteBudgetRepository().InsertOrUpdate(newBudget);

            BudgetInfo.DataBind();
        }

        protected void ClearBudget(object sender, GridViewDeleteEventArgs e)
        {
            var destinationId = BudgetInfo.DataKeys[e.RowIndex]["DestinationId"];

            e.Values.Add("BusinessUnitId", BusinessUnitID);
            e.Keys.Add("BusinessUnitId", BusinessUnitID);

            e.Values.Add("MonthApplicable", HistoricalDateDDL.SelectedValue);
            e.Keys.Add("MonthApplicable", HistoricalDateDDL.SelectedValue);
        }

        protected object GetDestinationName(object destinationId)
        {
            var id = (int)destinationId;

            var destinations = DestDS.Select(new DataSourceSelectArguments("destinationId")) as DataView;
            var rowIndex = destinations.Find(id);

            if (rowIndex < 0) return "Destination Unavailable";

            var row = destinations.Table.Rows[rowIndex];
            return row["description"];
        }

        protected string GetVisibility()
        {
            return "visibility:hidden";
        }

        protected string GetDashboardVisibility()
        {
            var settings = GetMiscSettings.GetSettings(BusinessUnitID);
            var visible = settings.ShowDashboard;

            var visibility = visible ? "visibility:visible" : "visibility:hidden";
            return visibility;
        }

        protected string ToDollars(object ammount)
        {
            return ((int)ammount).ToString("C0");
        }


        /// <remarks>
        /// Ported from
        /// FirstLook.NET\MasterSolution\WanamakerSolution\MerchandisingSolution\Wanamaker.WebApp\Public\Scripts\Max\
        ///   MAX.Dashboard.debug.js : InitChartSelect()
        /// </remarks>
        protected void InitChartSelect( string default_month ) // fill the drop down with the past 6 months
        {
            const int length = 13;
            const int month_cutoff_day = 12;
            
            // construct a list of months in reverse chronological order
            // starting with the current month if it is the 5th or later; otherwise, with the previous month.
            HistoricalDateDDL.Items.Clear();
            var now = DateTime.Now;
            bool first_attempt = true;
            while( HistoricalDateDDL.Items.Count < length )
            {
                if( first_attempt && now.Day < month_cutoff_day )
                {
                    first_attempt = false;
                    continue;
                }
                HistoricalDateDDL.Items.Add( 
                    new ListItem( 
                        String.Format( "{0:MMMM yyyy}", now ), 
                        String.Format( "{0}/{1}", now.Month, now.Year )));
                now = now.AddMonths( -1 );
            }

            if( default_month != null )
            {
                try
                {
                    HistoricalDateDDL.SelectedValue = default_month;
                }
                catch (ArgumentOutOfRangeException)
                {
                    // it's fine.
                }
            }
        }

        protected void DestDDL_OnDataBound(object sender, EventArgs e)
        {
            var DestDDL = sender as DropDownList;
            if( DestDDL != null )
                DestDDL.SelectedValue = Request.QueryString["DestDDL"];
        }
    }
}