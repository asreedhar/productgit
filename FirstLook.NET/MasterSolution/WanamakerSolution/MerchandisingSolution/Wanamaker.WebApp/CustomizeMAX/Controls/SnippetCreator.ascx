<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="SnippetCreator.ascx.cs" Inherits="Wanamaker.WebApp.CustomizeMAX.Controls.SnippetCreator" %>
<%@ Register TagPrefix="carbuilder" TagName="BusinessUnitUserControl" Src="~/Controls/BusinessUnitUserControl.ascx"  %>

<asp:FormView ID="BlurbCreatorFV" runat="server" DefaultMode="Insert" DataSourceID="BlurbDS" 
    DataKeyNames="BlurbID" OnDataBound="BlurbCreatorFV_DataBound">
    
    <EditItemTemplate>
    <asp:Label CssClass="warning" runat="server" ID="NotOwnerLbl" Text="FirstLook Snippet - Template cannot be modified - Go to Ad Text Editor to Customize Your Text"></asp:Label>
    <asp:HiddenField ID="ownerId" runat="server" Value='<%# Eval("OwnerId") %>' />
    <asp:Table ID="Table1" CssClass="BlurbBuilderTable" runat="server">
    <asp:TableRow>
    <asp:TableCell>
        Title:  <asp:TextBox ID="BlurbNameTB" runat="server" Text='<%# Bind("blurbName") %>' ></asp:TextBox>
    </asp:TableCell>
    <asp:TableCell>
        Category: <asp:DropDownList ID="BlurbCategoryDDL" runat="server" DataSourceID="BlurbTypesDS" DataTextField="BlurbTypeName" DataValueField="BlurbTypeId"></asp:DropDownList>
    </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
    <asp:TableCell ColumnSpan="2">
    <hr />
    </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            Use When:   
            
            <br /><asp:TextBox ID="ConditionTB" runat="server" TextMode="MultiLine"
            CssClass="ConditionField"
            Text='<%# Bind("TemplateCondition") %>' ></asp:TextBox>
            
        </asp:TableCell>
       <asp:TableCell CssClass="TemplateMergeFields">
            <asp:DropDownList ID="RulesDDL" runat="server" 
                    DataSourceID="RulesDS" OnDataBound="MergeRules_Bound"
                    OnSelectedIndexChanged="Condition_Selected" AutoPostBack="true">
                    
            </asp:DropDownList>
            <div style="text-align:right">
            <asp:LinkButton ID="andButton" runat="server" Text="AND" OnClientClick='<%# getAddTextJS("&") %>'></asp:LinkButton>
            <asp:LinkButton ID="LinkButton1" runat="server" Text="OR" OnClientClick='<%# getAddTextJS(",") %>'></asp:LinkButton>
            <asp:LinkButton ID="LinkButton2" runat="server" Text="NOT" OnClientClick='<%# getAddTextJS("!") %>'></asp:LinkButton>
            <asp:LinkButton ID="LinkButton3" runat="server" Text="Group" OnClientClick='<%# getAddTextJS("()") %>'></asp:LinkButton>    
            </div>
            <asp:ObjectDataSource ID="RulesDS" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Templating.TemplateConditionHelper"
                SelectMethod="GetConditionDisplayNames">
            
            </asp:ObjectDataSource>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>Template Body:<br /><asp:TextBox ID="TemplateBodyTB" runat="server" TextMode="MultiLine"
            CssClass="TemplateBodyField"
        Text='<%# Bind("TemplateText") %>' ></asp:TextBox></asp:TableCell>
        
         <asp:TableCell CssClass="TemplateMergeFields">
            
            <asp:DropDownList ID="BasicInfoDDL" runat="server" 
                    DataSourceID="otherMerchPointsDS" DataTextField="templateDisplayText" DataValueField="pointID" 
                    OnSelectedIndexChanged="Rule_Selected" AutoPostBack="true">
            </asp:DropDownList>
            
            <asp:DropDownList ID="OptionsRulesDDL" runat="server" 
                DataSourceID="basicInfoDS" DataTextField="templateDisplayText" DataValueField="pointID" 
                OnSelectedIndexChanged="Rule_Selected" AutoPostBack="true">
            </asp:DropDownList>
            
            <asp:DropDownList ID="ConsumerInfoDDL" runat="server"
                DataSourceID="consInfoDS" DataTextField="templateDisplayText" DataValueField="pointID" 
                OnSelectedIndexChanged="Rule_Selected" AutoPostBack="true">
            </asp:DropDownList>
            
            <asp:DropDownList ID="CarfaxInfoDDL" runat="server"
                DataSourceID="carfaxInfoDS" DataTextField="templateDisplayText" DataValueField="pointID" 
                OnSelectedIndexChanged="Rule_Selected" AutoPostBack="true">
            </asp:DropDownList>
            
            <asp:DropDownList ID="DropDownList1" runat="server"
                DataSourceID="categoriesDS" DataTextField="Description" DataValueField="CategoryID" 
                OnSelectedIndexChanged="Rule_Selected" AutoPostBack="true">
            </asp:DropDownList>
        </asp:TableCell>
    </asp:TableRow>
    </asp:Table>
    <asp:Button ID="CreateButton" runat="server" CommandName="Insert" Text="Create New Snippet" />
    </EditItemTemplate>
</asp:FormView>


<asp:ObjectDataSource ID="BlurbDS" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Templating.BlurbDataSource"    
    InsertMethod="TemplateBlurbCreate" OnInserting="Blurb_InsUpdating"
    >    
    <InsertParameters>
        <asp:Parameter Type="Object" Name="blurb" />
        <asp:Parameter Type="Int32" Name="BusinessUnitId" DefaultValue="100150" />
    </InsertParameters>        
</asp:ObjectDataSource>


<asp:SqlDataSource runat="server" ID="BlurbTypesDS" SelectCommandType="StoredProcedure" ConnectionString="<%$ ConnectionStrings:Merchandising %>"    
    SelectCommand="templates.getAllSnippetTypes">
    
</asp:SqlDataSource>


<asp:SqlDataSource runat="server" ID="otherMerchPointsDS" SelectCommandType="StoredProcedure" ConnectionString="<%$ ConnectionStrings:Merchandising %>"    
SelectCommand="templates.getPossibleMerchandisingPoints">
<SelectParameters>
    <asp:Parameter Name="BusinessUnitID" DefaultValue="100150" />
    <asp:Parameter Name="RuleType" Type="Int32" DefaultValue="4" />
</SelectParameters>        
</asp:SqlDataSource>


<asp:SqlDataSource runat="server" ID="basicInfoDS" SelectCommandType="StoredProcedure" ConnectionString="<%$ ConnectionStrings:Merchandising %>"    
    SelectCommand="templates.getPossibleMerchandisingPoints">
<SelectParameters>
<asp:Parameter Name="BusinessUnitID" DefaultValue="100150" />
<asp:Parameter Name="RuleType" Type="Int32" DefaultValue="3" />
</SelectParameters>        
</asp:SqlDataSource>

<asp:SqlDataSource runat="server" ID="consInfoDS" SelectCommandType="StoredProcedure" ConnectionString="<%$ ConnectionStrings:Merchandising %>"    
                        SelectCommand="templates.getPossibleMerchandisingPoints">
<SelectParameters>
    <asp:Parameter Name="BusinessUnitID" DefaultValue="100150" />
    <asp:Parameter Name="RuleType" Type="Int32" DefaultValue="5" />
</SelectParameters>        
</asp:SqlDataSource>

<asp:SqlDataSource runat="server" ID="carfaxInfoDS"  SelectCommandType="StoredProcedure" ConnectionString="<%$ ConnectionStrings:Merchandising %>"    
    SelectCommand="templates.getPossibleMerchandisingPoints">
<SelectParameters>
    <asp:Parameter Name="BusinessUnitID" DefaultValue="100150" />
    <asp:Parameter Name="RuleType" Type="Int32" DefaultValue="6" />
</SelectParameters>        
</asp:SqlDataSource>

<asp:ObjectDataSource runat="server" ID="categoriesDS" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.VINStyleDataSource" SelectMethod="GetGlobalCategories2" >

</asp:ObjectDataSource>