﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AutoWindowSticker.ascx.cs" Inherits="Wanamaker.WebApp.CustomizeMAX.Controls.AutoWindowSticker" %>
<div id="windowSticker">
    <div class="normal">
        <asp:Label runat="server" ID="lblMessage" CssClass="error" EnableViewState="False"/>
    </div>
    <div class="normal">
        Please specify the e-mail address(es) to send Window Stickers to
    </div>
    <div class="normal">
        <asp:TextBox runat="server" ID="txtEmail" MaxLength="1000" Columns="75"></asp:TextBox>
    </div>
    <div class="normal">
        Please select  minimum days in inventory:&nbsp
        <asp:DropDownList runat="server" ID="ddlMinimumDays" >
            <asp:ListItem Selected="True" Value="0">0</asp:ListItem>
            <asp:ListItem Value="1">1</asp:ListItem>
            <asp:ListItem Value="2">2</asp:ListItem>
            <asp:ListItem Value="3">3</asp:ListItem>
            <asp:ListItem Value="4">4</asp:ListItem>
            <asp:ListItem Value="5">5</asp:ListItem>
            <asp:ListItem Value="6">6</asp:ListItem>
            <asp:ListItem Value="7">7</asp:ListItem>
            <asp:ListItem Value="8">8</asp:ListItem>
            <asp:ListItem Value="9">9</asp:ListItem>
            <asp:ListItem Value="10">10</asp:ListItem>
            <asp:ListItem Value="11">11</asp:ListItem>
            <asp:ListItem Value="12">12</asp:ListItem>
            <asp:ListItem Value="13">13</asp:ListItem>
            <asp:ListItem Value="14">14</asp:ListItem>
        </asp:DropDownList>
        
    </div>
    <div class="normal">
        <asp:Button runat="server" ID="btnSave" Text="Save" onclick="btnSave_Click"/>
    </div>
    <div  class="normal">
        <br/>
    </div>
    <div class="normal" align="center">
        <table width="95%" class="windowStickerHistory">
            <asp:Repeater runat="server" ID="rptHistory">
                <HeaderTemplate>
                    <tr>
                        <th style="text-align: left" colspan="2">Date Created</th>
                        <th style="text-align: left">PDF File</th>
                        <th style="text-align: left" colspan="2">Email Sent</th>
                        <th style="text-align: left" colspan="2">Last Access</th>
                        <th style="text-align: right">Stickers</th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="windowStickerHistory">
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.CreatedOn", "{0:ddd}")%>
                        </td>
                        <td style="text-align: left" class="windowStickerHistory">
                            <%#DataBinder.Eval(Container, "DataItem.CreatedOn", "{0:MMM dd, yyyy}")%>
                        </td>
                        <td style="text-align: left">
                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl=<%#String.Format("~/print/BulkWindowStickerDisplay.aspx?PrintBatchId={0}", Eval("PrintBatchId")) %>  Target="blank">
                                [ view ]
                            </asp:HyperLink>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.LastEmailSent", "{0:ddd}")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.LastEmailSent", "{0:MMM dd, yyyy}")%>&nbsp;&nbsp;<%#DataBinder.Eval(Container, "DataItem.LastEmailSent", "{0:hh:mm tt}")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.LastAccess", "{0:ddd}")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.LastAccess", "{0:MMM dd, yyyy}")%>&nbsp;&nbsp;<%#DataBinder.Eval(Container, "DataItem.LastAccess", "{0:hh:mm tt}")%>
                        </td>
                        <td style="text-align: right"><%#DataBinder.Eval(Container, "DataItem.VehicleCount")%></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
</div>