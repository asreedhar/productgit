<%@ Import Namespace="FirstLook.Merchandising.DomainModel.Templating" %>
<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="VehicleProfiler.ascx.cs" Inherits="Wanamaker.WebApp.CustomizeMAX.Controls.VehicleProfiler" %>
<asp:SqlDataSource ID="VehicleProfileDS" runat="server" ConnectionString='<%$ ConnectionStrings:Merchandising %>' SelectCommand="Select vehicleProfileId, [title] FROM templates.vehicleProfiles WHERE businessUnitId = @BusinessUnitId OR businessUnitId = 100150"
    OnSelecting="Profiles_Selecting">
    <SelectParameters>
        <asp:Parameter Name="BusinessUnitId" DefaultValue="100150" />
    </SelectParameters>
</asp:SqlDataSource>
<h3>
    Profile:
    <asp:DropDownList ID="VehicleProfileDDL" runat="server" DataSourceID="VehicleProfileDS" DataTextField="title" DataValueField="vehicleProfileId"
        AutoPostBack="true" OnSelectedIndexChanged="CurrentProfile_Changed" OnDataBound="Available_Bound">
    </asp:DropDownList>
    <asp:Button ID="Button2" Text="Create New" runat="server" OnClick="CreateNew_Click" />
</h3>
<div class="clearfix main">
    <asp:Panel ID="Summary" CssClass="summary" runat="server">
        Name:&nbsp;&quot;<asp:Label ID="titleLbl" runat="server" Text='<%# Eval("Title") %>'></asp:Label>&quot;
        <asp:Repeater ID="SummaryBullets" runat="server">
            <HeaderTemplate>
                <table>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td class="header">
                        <%# Eval("Key") %>
                    </td>
                    <td>
                        <%# Eval("Value") %>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>
    <asp:Panel ID="ControlPanel" CssClass="controls" runat="server">
        <h3>
            Modify Settings</h3>
        <ul class="yearPriceDiv">
            <li><span class="rangeLabel">Title: </span>
                <asp:TextBox ID="titleTB" runat="server" Columns="30" Text='<%# Bind("Title") %>'></asp:TextBox>
                <asp:Button ID="Button4" runat="server" Text="Set" OnClick="ChangeParam_Click" CommandArgument="Name" />
            </li>
            <li><span class="rangeLabel">Model Year: </span>
                <asp:TextBox ID="startYear" runat="server" Columns="4" Text='<%# Bind("StartYear") %>'></asp:TextBox>
                to
                <asp:TextBox ID="endYear" runat="server" Columns="4" Text='<%# Bind("EndYear") %>'></asp:TextBox>
                <asp:Button ID="ChangeYear" runat="server" Text="Set" OnClick="ChangeParam_Click" CommandArgument="Year" />
            </li>
            <li><span class="rangeLabel">Price:</span>
                <asp:TextBox ID="lowPriceTB" runat="server" Columns="4" Text='<%# Bind("MinPrice") %>'></asp:TextBox>
                to
                <asp:TextBox ID="hiPriceTB" runat="server" Columns="4" Text='<%# Bind("MaxPrice") %>'></asp:TextBox>
                <asp:Button ID="Button1" runat="server" Text="Set" OnClick="ChangeParam_Click" CommandArgument="Price" />
            </li>
            <li><span class="rangeLabel">Age in days:</span>
                <asp:TextBox ID="minAge" runat="server" Columns="4" Text='<%# Bind("MinAge") %>'></asp:TextBox>
                to
                <asp:TextBox ID="maxAge" runat="server" Columns="4" Text='<%# Bind("MaxAge") %>'></asp:TextBox>
                <asp:Button ID="Button3" runat="server" Text="Set" OnClick="ChangeParam_Click" CommandArgument="Age" />
            </li>
            <li><span class="rangeLabel">Certified:</span>
                <asp:DropDownList ID="cert" runat="server">
                    <asp:ListItem Text="Any Certified Status" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Only Certified" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Only NON-Certified" Value="2"></asp:ListItem>
                </asp:DropDownList>
                <asp:Button ID="Button11" runat="server" Text="Set" OnClick="ChangeParam_Click" CommandArgument="Certified" />
            </li>
            <li><span class="rangeLabel">New or Used:</span>
                <asp:DropDownList ID="typeDropDown" runat="server">
                    <asp:ListItem Text="Both" Value="0"></asp:ListItem>
                    <asp:ListItem Text="New" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Used" Value="2"></asp:ListItem>
                </asp:DropDownList>
                <asp:Button ID="Button16" runat="server" Text="Set" OnClick="ChangeParam_Click" CommandArgument="Type" />
            </li>
        </ul>
        <div class="templateRuleSectionBody">
            <div>
                <span class="rangeLabel">Make:</span>
                <asp:DropDownList ID="MakeDDL" runat="server" DataSourceID="makeDS" DataTextField="DivisionName" DataValueField="DivisionName"
                    OnDataBound="DDL_Bound">
                </asp:DropDownList>
                <asp:Button ID="Button5" runat="server" Text="+ Add" OnClick="ChangeParam_Click" CommandArgument="Make" />
                <asp:Button ID="Button6" runat="server" Text="Clear" OnClick="ChangeParam_Click" CommandArgument="ClearMake" />
            </div>
            <div>
                <span class="rangeLabel">Model:</span>
                <asp:DropDownList ID="ModelDDL" runat="server" OnDataBound="DDL_Bound">
                </asp:DropDownList>
                <asp:Button ID="Button7" runat="server" Text="+ Add" OnClick="ChangeParam_Click" CommandArgument="Model" />
                <asp:Button ID="Button8" runat="server" Text="Clear" OnClick="ChangeParam_Click" CommandArgument="ClearModel" />
            </div>
            <div>
                <span class="rangeLabel">Trim:</span>
                <asp:DropDownList ID="TrimDDL" runat="server" OnDataBound="DDL_Bound">
                </asp:DropDownList>
                <asp:Button ID="Button12" runat="server" Text="+ Add" OnClick="ChangeParam_Click" CommandArgument="Trim" />
                <asp:Button ID="Button13" runat="server" Text="Clear" OnClick="ChangeParam_Click" CommandArgument="ClearTrim" />
            </div>
            <div>
                <span class="rangeLabel">Segment:</span>
                <asp:DropDownList ID="SegmentDDL" runat="server" DataSourceID="segmentDS" DataTextField="Value" DataValueField="Key" OnDataBound="DDL_Bound">
                </asp:DropDownList>
                <asp:Button ID="Button14" runat="server" Text="+ Add" OnClick="ChangeParam_Click" CommandArgument="Segment" />
                <asp:Button ID="Button15" runat="server" Text="Clear" OnClick="ChangeParam_Click" CommandArgument="ClearSegment" />
            </div>
            <div>
                <span class="rangeLabel">Mkt Class:</span>
                <asp:DropDownList ID="MktClassDDL" runat="server" DataSourceID="mktClassDS" DataTextField="MarketClass" DataValueField="MarketClass"
                    OnDataBound="DDL_Bound">
                </asp:DropDownList>
                <asp:Button ID="Button9" runat="server" Text="+ Add" OnClick="ChangeParam_Click" CommandArgument="MarketClass" />
                <asp:Button ID="Button10" runat="server" Text="Clear" OnClick="ChangeParam_Click" CommandArgument="ClearMarketClass" />
            </div>
        </div>
    </asp:Panel>
</div>
<asp:ObjectDataSource ID="segmentDS" runat="server" TypeName="Wanamaker.WebApp.AppCode.SegmentsDataSource" SelectMethod="FetchSegments">
</asp:ObjectDataSource>
<asp:SqlDataSource ID="makeDS" runat="server" ConnectionString="<%$ ConnectionStrings:chrome %>" SelectCommand="SELECT DISTINCT DivisionID, DivisionName FROM chrome.Divisions WHERE countrycode = 1 ORDER BY DivisionName">
</asp:SqlDataSource>
<asp:SqlDataSource ID="modelDS" runat="server" ConnectionString="<%$ ConnectionStrings:chrome %>" SelectCommand="SELECT DISTINCT ModelName FROM chrome.models ORDER BY ModelName">
</asp:SqlDataSource>
<asp:SqlDataSource ID="mktClassDS" runat="server" ConnectionString="<%$ ConnectionStrings:chrome %>" SelectCommand="SELECT DISTINCT MktClassID, MarketClass FROM chrome.mktClass ORDER BY MarketClass">
</asp:SqlDataSource>
