using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FirstLook.Merchandising.DomainModel.Templating;
using MerchandisingLibrary;
using Wanamaker.WebApp.Controls;


namespace Wanamaker.WebApp.CustomizeMAX.Controls
{

    public partial class SnippetCreator : BusinessUnitUserControl
    {
        protected void BlurbCreatorFV_DataBound(object sender, EventArgs e)
        {
            DropDownList ddlBI = BlurbCreatorFV.FindControl("BasicInfoDDL") as DropDownList;
            DropDownList ddlOR = BlurbCreatorFV.FindControl("OptionsRulesDDL") as DropDownList;
            DropDownList ddlConsInf = BlurbCreatorFV.FindControl("ConsumerInfoDDL") as DropDownList;
            DropDownList ddlCarfInf = BlurbCreatorFV.FindControl("CarfaxInfoDDL") as DropDownList;
            if (ddlBI == null || ddlOR == null || ddlConsInf == null || ddlCarfInf == null)
            {
                return;
            }
            ddlConsInf.DataBind();
            ddlConsInf.Items.Insert(0, "Select consumer info...");

            ddlCarfInf.DataBind();
            ddlCarfInf.Items.Insert(0, "Select carfax info...");

            ddlOR.DataBind();
            ddlOR.Items.Insert(0, "Select rule...");

            ddlBI.DataBind();
            ddlBI.Items.Insert(0, "Select vehicle basics...");


            Label lb = BlurbCreatorFV.FindControl("NotOwnerLbl") as Label;

            if (BlurbCreatorFV.CurrentMode == FormViewMode.Edit)
            {
                DropDownList blurbCategory = BlurbCreatorFV.FindControl("BlurbCategoryDDL") as DropDownList;
                TemplateBlurb tb = ((TemplateBlurb)BlurbCreatorFV.DataItem);
                int btId = tb.BlurbTypeId;
                if (blurbCategory != null && blurbCategory.Items.FindByValue(btId.ToString()) != null)
                    blurbCategory.SelectedValue = btId.ToString();
                if (lb != null)
                {
                    if (tb.IsOwner(BusinessUnitID))
                    {
                        lb.Visible = false;
                        BlurbCreatorFV.Enabled = true;

                    }
                    else
                    {
                        lb.Visible = true;
                        BlurbCreatorFV.Enabled = false;

                    }
                }
            }
            else
            {
                if (lb != null) lb.Visible = false;
            }



        }


        protected void Blurb_InsUpdating(object sender, ObjectDataSourceMethodEventArgs e)
        {


            TemplateBlurb updateMe = new TemplateBlurb();

            if (e.InputParameters.Contains("BlurbID"))
            {
                updateMe.BlurbID = (int)e.InputParameters["BlurbID"];
                e.InputParameters.Remove("BlurbID");

            }

            e.InputParameters["BusinessUnitId"] = BusinessUnitID;

            DropDownList typeDDL = BlurbCreatorFV.FindControl("BlurbCategoryDDL") as DropDownList;
            TextBox titleTB = BlurbCreatorFV.FindControl("BlurbNameTB") as TextBox;
            TextBox condTB = BlurbCreatorFV.FindControl("ConditionTB") as TextBox;
            TextBox bodyTB = BlurbCreatorFV.FindControl("TemplateBodyTB") as TextBox;
            HiddenField owner = BlurbCreatorFV.FindControl("ownerId") as HiddenField;

            if (titleTB != null)
            {
                updateMe.BlurbName = titleTB.Text;
            }

            if (condTB != null)
            {
                updateMe.TemplateCondition = condTB.Text;
            }

            if (bodyTB != null)
            {
                updateMe.TemplateText = bodyTB.Text;
            }

            if (typeDDL != null)
            {
                updateMe.BlurbTypeId = Int32.Parse(typeDDL.SelectedValue);
            }
            if (owner != null && !string.IsNullOrEmpty(owner.Value))
            {
                updateMe.OwnerId = Int32.Parse(owner.Value);
            }
            else
            {
                updateMe.OwnerId = BusinessUnitID;
            }

            e.InputParameters["blurb"] = updateMe;
            if (e.InputParameters.Contains("blurbName"))
            {
                e.InputParameters.Remove("blurbName");
            }

            if (e.InputParameters.Contains("TemplateText"))
            {
                e.InputParameters.Remove("TemplateText");
            }

            if (e.InputParameters.Contains("TemplateCondition"))
            {
                e.InputParameters.Remove("TemplateCondition");
            }

        }

        protected void Condition_Selected(object sender, EventArgs e)
        {
            DropDownList theDDL = (DropDownList)sender;
            if (theDDL.SelectedIndex == 0)
            {
                return;
            }
            addRuleToCondition(theDDL.SelectedItem.Text);
        }
        private void addRuleToCondition(string newPoint)
        {

            ((TextBox)BlurbCreatorFV.FindControl("ConditionTB")).Text += AdTemplate.CODE_TEMPLATE_START_TAG + newPoint + AdTemplate.CODE_TEMPLATE_END_TAG;
        }

        protected void MergeRules_Bound(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;
            if (ddl == null) return;

            ddl.Items.Insert(0, new ListItem("Select Condition...", ""));
        }

        public string getAddTextJS(string textToAdd)
        {
            TextBox tb = BlurbCreatorFV.FindControl("ConditionTB") as TextBox;
            if (tb == null) return string.Empty;
            return "javascript:document.getElementById('" + tb.ClientID + "').value += '" + textToAdd + "';return false;";
        }

        protected void Rule_Selected(object sender, EventArgs e)
        {

            DropDownList theDDL = (DropDownList)sender;
            if (theDDL.SelectedIndex == 0)
            {
                return;
            }
            ((TextBox)BlurbCreatorFV.FindControl("TemplateBodyTB")).Text += " " + AdTemplate.CODE_TEMPLATE_START_TAG + theDDL.SelectedItem.Text + AdTemplate.CODE_TEMPLATE_END_TAG;

        }

    }
}