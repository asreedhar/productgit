<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="CampaignSettings.ascx.cs" Inherits="Wanamaker.WebApp.CustomizeMAX.Controls.CampaignSettings" %>
<%@ Register TagPrefix="carbuilder" TagName="BusinessUnitUserControl" Src="~/Controls/BusinessUnitUserControl.ascx"  %>

<asp:ObjectDataSource ID="CampaignDS" runat="server" 
    TypeName="FirstLook.Merchandising.DomainModel.Postings.CampaignEventDataSource" 
    SelectMethod="FetchList"    
    InsertMethod="Upsert"
    UpdateMethod="Upsert"
    DeleteMethod="Delete"
    OnUpdating="Campaign_Updating"
    OnDeleting="Campaign_Deleting"
    OnSelecting="Campaigns_Selecting"
    OnInserting="Campaign_Inserting"
     >
     <SelectParameters>
        <asp:Parameter Name="BusinessUnitId" Type="Int32" />
     </SelectParameters>
     <InsertParameters>
        <asp:Parameter Name="campaignEvent" Type="Object" />
     </InsertParameters>
     <UpdateParameters>
        <asp:Parameter  Name="campaignEvent" Type="Object" />
     </UpdateParameters>
     <DeleteParameters>        
        <asp:Parameter Name="BusinessUnitId" Type="Int32" />
     </DeleteParameters>
    </asp:ObjectDataSource>
    
    
    <asp:GridView ID="CampaignGV" runat="server" 
        DataSourceID="CampaignDS" 
        AutoGenerateColumns="false"
        ShowFooter="true"
        OnRowDataBound="Row_Bound"
        DataKeyNames="CampaignEventId"
        GridLInes="None"
        >
    <EmptyDataTemplate>
        <asp:Button ID="Create" runat="server" OnClick="CreateDefault_Campaign" Text="Create a Campaign" />
    </EmptyDataTemplate>
    <Columns>
    <asp:TemplateField HeaderText="Action at Age" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
        <ItemTemplate>
            <%# Eval("VehicleAgeInDaysForAction") %> days
        </ItemTemplate>
        <EditItemTemplate>
            <asp:TextBox ID="ageInDays" runat="server" MaxLength="3" Columns="5" Text='<%# Bind("VehicleAgeInDaysForAction") %>' ></asp:TextBox> days
        </EditItemTemplate>
        <FooterTemplate>
            <asp:TextBox ID="ageInDaysF" runat="server" MaxLength="3" Columns="5"></asp:TextBox> days
        </FooterTemplate>
    </asp:TemplateField>
    <asp:TemplateField HeaderText="Descriptions" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="left">
    <ItemTemplate>
        <%# GetNewDescriptionText(Eval("RegenerateDescription"), (string)Eval("TemplateName"), (string)Eval("ThemeName")) %>
    </ItemTemplate>
    <EditItemTemplate>
        <asp:CheckBox ID="newDesc" runat="server" Checked='<%# Bind("RegenerateDescription") %>' Text="Create new highlights" />
        <p><asp:DropDownList ID="MissionDDL" runat="server" DataSourceID="ThemeDS"
                DataTextField="name" DataValueField="id"
                AppendDataBoundItems="true">
                <asp:ListItem Text="No theme..." Value=""></asp:ListItem>
        </asp:DropDownList></p>
        <p><asp:DropDownList runat="server" ID="TemplateDDL" 
                DataTextField="Value" DataValueField="Key"
                DataSourceID="TemplateDS" AppendDataBoundItems="true">
                <asp:ListItem Text="Select framework..." Value=""></asp:ListItem>
                </asp:DropDownList></p>
            
    </EditItemTemplate>
    <FooterTemplate>
        <asp:CheckBox ID="newDescF" runat="server" Checked='<%# Bind("RegenerateDescription") %>' Text="Create new highlights" />
        <p><asp:DropDownList ID="MissionDDL" runat="server" DataSourceID="ThemeDS"
                DataTextField="name" DataValueField="id"
                AppendDataBoundItems="true">
                <asp:ListItem Text="No theme..." Value=""></asp:ListItem>
        </asp:DropDownList></p>
        <p><asp:DropDownList runat="server" ID="TemplateDDL" 
                DataTextField="Value" DataValueField="Key"
                DataSourceID="TemplateDS" AppendDataBoundItems="true">
                <asp:ListItem Text="Select framework..." Value=""></asp:ListItem>
                </asp:DropDownList></p>
            
    </FooterTemplate>
    </asp:TemplateField>
    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" FooterStyle-HorizontalAlign="Center" FooterStyle-VerticalAlign="Middle">
        <ItemTemplate>
            <asp:LinkButton ID="lb1" runat="server" Text="Edit" CommandName="Edit" CssClass="tableButton"></asp:LinkButton>
            <asp:LinkButton ID="lb2" runat="server" Text="Delete" CommandName="Delete" CssClass="tableButton"></asp:LinkButton>
        </ItemTemplate>
        <EditItemTemplate>
            <asp:LinkButton ID="lb1" runat="server" Text="Update" CommandName="Update" CssClass="tableButton"></asp:LinkButton>
            <asp:LinkButton ID="lb2" runat="server" Text="Cancel" CommandName="Cancel" CssClass="tableButton"></asp:LinkButton>
        </EditItemTemplate>
        <FooterTemplate>
            <asp:Button ID="newItemF" runat="server" Text="+ Add" OnClick="AddNew_Click" CssClass="button" />
        </FooterTemplate>
    </asp:TemplateField>
    </Columns>
    </asp:GridView>



<asp:SqlDataSource ID="ThemeDS" runat="server" 
        SelectCommandType="StoredProcedure" 
        ConnectionString='<%$ ConnectionStrings:Merchandising %>'
        SelectCommand="templates.Themes#Fetch">
        
    <SelectParameters>
        <asp:Parameter Name="BusinessUnitID" DefaultValue="100150" ConvertEmptyStringToNull="true" Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>
<asp:ObjectDataSource runat="server"
    ID="TemplateDS"
    TypeName="FirstLook.Merchandising.DomainModel.Templating.BlurbDataSource"
    SelectMethod="AvailableTemplatesSelect"
    OnSelecting="Templates_Selecting">
    <SelectParameters>
        <asp:Parameter Name="businessUnitId" Type="Int32" />
    </SelectParameters>    
</asp:ObjectDataSource>
