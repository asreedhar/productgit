﻿using System;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Utilities;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics;
using FirstLook.Merchandising.DomainModel.Postings;
using Wanamaker.WebApp.Controls;
using Merchandising.Messages;

namespace Wanamaker.WebApp.CustomizeMAX.Controls
{
    public partial class Analytics : BusinessUnitUserControl
    {
        //property injected
        public IGoogleAnalyticsWebAuthorization WebAuthorization { get; set; }
        public IGoogleAnalyticsQuery Query { get; set; }
        public IGoogleAnalyticsRepository Repository { get; set; }
        public IQueueFactory _queueFactory { get; set; }

        protected override void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!WebAuthorization.IsAuthorized(BusinessUnitID))
                {
                    if (HttpContext.Current.Request["code"] != null)
                    {
                        bool authorized = WebAuthorization.Authorize(BusinessUnitID);
                        if (authorized)
                        {
                            // Force a redirect to clear the url.
                            Response.Redirect(Request.Url.AbsolutePath + "?t=Analytics");
                        }
                    }
                }
                else //we are authorized
                {
                    SetUpButtonSection.Visible = false;
                    ProfilePanel.Visible = true;
                    PopulateDropDowns();
                }

                SetupErrorMessages();
            }
        }

        private void PopulateDropDowns()
        {
            try
            {
                adminLock.Checked = Repository.Read_AdminLock( BusinessUnitID );
                if( !Context.User.IsInRole("Administrator"))
                    lockbox.Visible = false; // not admin; hide lock box (but still possible to use its value)

                Query.Init(BusinessUnitID);
                var profiles = Query.GetProfiles();

                if (profiles.Count() == 0)
                {
                    GoogleErrorRedirect("User does not have any Analytics API profiles configured");
                }

                foreach (var profile in profiles)
                {
                    personalComputerDropDown.Items.Add(new ListItem(profile.Name, profile.Id.ToString()));
                }

                // only use Dealer.com right now.
                var siteProviders = EdtDestinationHelper.GetGoogleVdpSites().ToList();
                if (!siteProviders.Contains(EdtDestinations.Undefined))
                    siteProviders.Add(EdtDestinations.Undefined);

                foreach (var site in siteProviders)
                {
                    siteProviderDropDown.Items.Add(new ListItem(EnumHelper.GetEnumDescription(typeof(EdtDestinations), site), site.ToString()));
                }
                
                var selectedProfile = Repository.FetchProfile(BusinessUnitID, GoogleAnalyticsType.PC);
                if (selectedProfile != null)
                {
                    int hasProfile = personalComputerDropDown.Items.IndexOf(personalComputerDropDown.Items.FindByValue(selectedProfile.ProfileId.ToString()));
                    personalComputerDropDown.SelectedIndex = hasProfile;

                    siteProviderDropDown.SelectedIndex = siteProviderDropDown.Items.IndexOf(siteProviderDropDown.Items.FindByValue(selectedProfile.SiteProvider.ToString()));
                }

                updateFormState();
            }
            catch (System.AccessViolationException ave)
            {
                GoogleErrorRedirect(ave.Message);
            }
        }

        public void updateFormState()
        {
            bool can_change_profile = ShouldEnableGoogleAnalytics();
            personalComputerDropDown.Enabled = can_change_profile;
            personalComputerDropDown_readOnly.Text = personalComputerDropDown.SelectedItem != null ? personalComputerDropDown.SelectedItem.Text : "";
            profile.Visible = can_change_profile;
            profile_readOnly.Visible = !can_change_profile;
            mustClearBlurb.Visible = !can_change_profile;
        }

        private void GoogleErrorRedirect(string message)
        {
            Repository.SetRefreshTokenInactive(BusinessUnitID);
            Session["googleAuthErrors"] = message;

            // we have to redirect to make sure we have a clean url in case we try to authenticate again.
            Response.Redirect(Request.Url.AbsolutePath + "?t=Analytics");
        }

        private bool ShouldEnableGoogleAnalytics()
        {
            bool is_admin = Context.User.IsInRole("Administrator");
            bool admin_lock = adminLock.Checked;
            
            return is_admin || !admin_lock;
        }

        private void SetupErrorMessages()
        {
            if(Session["googleAuthErrors"] != null)
            {
                String aex = Session["googleAuthErrors"].ToString();
                System.Web.UI.HtmlControls.HtmlGenericControl paragraph = new System.Web.UI.HtmlControls.HtmlGenericControl("p");
                paragraph.Attributes.Add("class", "message error");
                paragraph.InnerText = Uri.UnescapeDataString(aex);

                ApiError.Controls.Add(paragraph);
                ApiError.Visible = true;

                Session.Remove("googleAuthErrors");
            }
            if (!String.IsNullOrEmpty(Request.Params["error"]))
            {
                System.Web.UI.HtmlControls.HtmlGenericControl paragraph = new System.Web.UI.HtmlControls.HtmlGenericControl("p");
                paragraph.Attributes.Add("class", "message error");
                paragraph.InnerText = Uri.UnescapeDataString("We require access to your Google Analytics account in order to complete this configuration.");

                ApiError.Controls.Add(paragraph);
                ApiError.Visible = true;
            }
        }

        protected void SetUpButton_Click(object sender, EventArgs e)
        {
            WebAuthorization.SendRedirect();
        }

        protected void saveButton_Click(object sender, EventArgs e)
        {
            int profileId = int.Parse(personalComputerDropDown.SelectedValue);
            string profileName = personalComputerDropDown.SelectedItem.Text;
            EdtDestinations destination = (EdtDestinations)Enum.Parse(typeof(EdtDestinations), siteProviderDropDown.SelectedValue);
            int destId = (int)destination;
            Repository.CreateUpdateProfileAccount(new GoogleAnalyticsProfile(BusinessUnitID, GoogleAnalyticsType.PC, profileId, profileName, destination));

            _queueFactory.CreateGoogleAnalyticsQueue().SendMessage(
                new Merchandising.Messages.GoogleAnalytics.GoogleAnalyticsGenerateMessage(
                    BusinessUnitID, GoogleAnalyticsType.PC.ToString(), FirstLook.Common.Core.DateRange.MonthsFromDate(-13, DateTime.Today), true)
                , GetType().FullName);

            updateFormState();

            if( lockbox.Visible )
            {
                Repository.Write_AdminLock( BusinessUnitID, adminLock.Checked );
            }
        }

        protected void ClearClick(object sender, EventArgs e)
        {
            Repository.SetRefreshTokenInactive(BusinessUnitID);
            Repository.DeleteProfile(BusinessUnitID, GoogleAnalyticsType.PC);
            Repository.Write_AdminLock( BusinessUnitID, false ); // if the lock was enabled and left on, they would have trouble selecting their own profile
            // we have to redirect. Need to start process
            Response.Redirect(Request.Url.AbsolutePath + "?t=Analytics");
        }


    }
}