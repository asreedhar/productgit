﻿    <%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BucketAlerts.ascx.cs" Inherits="Wanamaker.WebApp.CustomizeMAX.Controls.BucketAlerts" %>
    <div id="bucketAlerts">
    <asp:PlaceHolder id="DashboardWarningPlaceHolder" runat="server" Visible="false">
        You must have dashboard enabled to use this feature.
    </asp:PlaceHolder>
    <asp:PlaceHolder id="SettingsPlaceHolder" runat="server" Visible="false">
        
        <div class="emailSettingsSection">
            <div class="dropDown">
                These alerts should apply to 
                <asp:DropDownList id="NewUsedDropDown" runat="server">
                    <asp:ListItem Text="New And Used" Value="0" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Used" Value="2"></asp:ListItem>
                    <asp:ListItem Text="New" Value="1"></asp:ListItem>
                </asp:DropDownList>
                vehicles.
            </div>
            <div class="alertBuckets">
                <div class="alertHeader">
                    <span class="col1">
                        Display on Dashboard
                    </span>
                    <span class="col3">
                        Alert in E-mail
                    </span>
                    <span class="col2">
                        I would like to receive email alerts when I have:
                    </span>
                </div>
                <asp:Repeater ID="rptBuckets" runat="server">
                    <ItemTemplate>
                        <div>
                            <p >
                            <asp:HiddenField ID='hfBucket' runat='server' Value='<%#Eval("bucket") %>' />
                            <asp:HiddenField ID='hfThresholdID' runat='server' Value='<%#Eval("Id") %>' />
                            <span class="col1">
                                    <asp:CheckBox ID='chkActive' runat='server' Checked='<%#Eval("Active") %>'/>
                            </span> 
                            <span class="col3">
                                    <asp:CheckBox ID='chkEmail' runat='server' Checked='<%#Eval("EmailActive") %>' />
                            </span> 
                            <span class="col2">
                                At least <asp:TextBox ID="txtLimit" runat='server' Text='<%#Eval("Limit") %>' Width='40px' style=" text-align:right" ></asp:TextBox> <%#Eval("postNumberText") %>
                            </span>
                            </p>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>

        <div class="emailSettingsSection">
            <h2>Alerts</h2>
            <div>
                E-mail address(es) you would like to send <span class="bold">daily</span> alerts to:
            </div>
            <div>
                <asp:TextBox ID="EmailTextBox" runat="server" Width="800px"/>
                <div>
                    <asp:Label Text="Email Listed Doesn't Exist" ID="BouncedLabel" runat="server" Visible="false" CssClass="error"/>
                    <asp:RegularExpressionValidator runat="server"  Display="Dynamic" id="emailValidator" controltovalidate="EmailTextBox" validationexpression="^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)+([\s,;])*)*$" errormessage="Invalid email addresses!" />
                </div>
            </div>
            <div>
                E-mail address(es) you would like to send <span class="bold">weekly</span> alerts to:
            </div>
            <div>
                <asp:TextBox ID="WeeklyEmailTextBox" runat="server" Width="800px"/>
                <div>
                    <asp:Label Text="Email Listed Doesn't Exist" ID="WeeklyBouncedLabel" runat="server" Visible="false" CssClass="error"/>
                    <asp:RegularExpressionValidator runat="server"  Display="Dynamic" id="weeklyEmailValidator" controltovalidate="WeeklyEmailTextBox" validationexpression="^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)+([\s,;])*)*$" errormessage="Invalid email addresses!" />
                </div>
            </div>
            <div class="emailHelp" style="margin-bottom: 30px;">
                To send to multiple recipients, type e-mail addresses separated with commas
            </div>
        </div>

        <div class="emailSettingsSection">
            <h2>Digitial Marketing Analysis Emails</h2>
            <div>
                E-mail address(es) you would like to send Digital Marketing Analysis emails to for <span class="bold">individual</span> stores:
            </div>
            <div>
                <asp:TextBox ID="BionicHealthReportEmailTextBox" runat="server" Width="800px"/>
                <div>
                    <asp:Label Text="Email Listed Doesn't Exist" ID="BionicHealthReportBouncedLabel" runat="server" Visible="false" CssClass="error"/>
                    <asp:RegularExpressionValidator runat="server"  Display="Dynamic" id="BionicHealthReportEmailValidator" controltovalidate="BionicHealthReportEmailTextBox" validationexpression="^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)+([\s,;])*)*$" errormessage="Invalid email addresses!" />
                </div>
            </div>
            <div>
                E-mail address(es) you would like to send Digital Marketing Analysis emails to for a <span class="bold">group</span>*:
            </div>
            <div>
                <asp:TextBox ID="GroupBionicHealthReportEmailTextBox" runat="server" Width="800px"/>
                <div>
                    <asp:Label Text="Email Listed Doesn't Exist" ID="GroupBionicHealthReportBouncedLabel" runat="server" Visible="false" CssClass="error"/>
                    <asp:RegularExpressionValidator runat="server"  Display="Dynamic" id="GroupBionicHealthReportEmailValidator" controltovalidate="GroupBionicHealthReportEmailTextBox" validationexpression="^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)+([\s,;])*)*$" errormessage="Invalid email addresses!" />
                </div>
            </div>
            <div class="emailHelp">
                * If you have access to multiple stores, the Digital Market Analysis can also be generated at the group level.
            </div>
        </div>

        <div class="formButtons">
            <asp:Button ID="SaveButton" Text="Save" OnClick="OnSave" runat="server" CssClass="button submit"/>
        </div>
    </asp:PlaceHolder>
</div>
    