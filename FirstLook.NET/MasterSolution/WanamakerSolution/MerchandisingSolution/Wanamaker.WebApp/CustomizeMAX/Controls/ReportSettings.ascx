<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ReportSettings.ascx.cs" Inherits="Wanamaker.WebApp.CustomizeMAX.Controls.ReportSettings" %>
<%@ Register TagPrefix="carbuilder" TagName="BusinessUnitUserControl" Src="~/Controls/BusinessUnitUserControl.ascx"  %>

<div id="MiscSettings" class="clearfix">
    <asp:ValidationSummary ID="validationSummary" DisplayMode="BulletList" runat="server" />
    <div id="noSettings" runat="server">
        There are no settings available for this dealer. Possible cause: The dealer has
        not been setup for MAX AD yet.
    </div>
    <div id="settings" runat="server">
        
         <asp:SqlDataSource ID="DestDS" runat="server" ConnectionString="<%$ ConnectionStrings:Merchandising %>" 
            SelectCommand="SELECT description, destinationID FROM settings.EdtDestinations WHERE active = 1 and (destinationID = 1 or destinationID = 2)">
        </asp:SqlDataSource>
        
        <asp:ObjectDataSource ID="BudgetSource" runat="server" 
            TypeName="FirstLook.Merchandising.DomainModel.Reports.SitePerformance.SiteBudgetRepository" 
            DataObjectTypeName="FirstLook.Merchandising.DomainModel.Reports.SitePerformance.SiteBudget"
            SelectMethod="GetBudgetsByDate" OnSelecting="OnSelectBudgetData" 
            UpdateMethod="InsertOrUpdate"
            DeleteMethod="ClearCurrentBudget"
            InsertMethod="InsertOrUpdate" >
            <SelectParameters>
                <asp:Parameter Name="businessUnitId" Type="Int32" />
                <asp:Parameter  Name="monthApplicable" Type="DateTime" />
            </SelectParameters>
        </asp:ObjectDataSource>
        
        <div class="report_settings_section" id="dashboard_report_settings"  style='<%= GetDashboardVisibility() %>'>
                
            <h2>Executive Dashboard</h2>
            
            <asp:DropDownList ID="HistoricalDateDDL" DataTextField="display" DataValueField="value" runat="server" AutoPostBack="True" OnSelectedIndexChanged="HistoricalDateDDL_Changed"></asp:DropDownList>
            
            <asp:GridView ID="BudgetInfo" AutoGenerateColumns="False" DataSourceID="BudgetSource" runat="server" ShowFooter="True" ShowHeaderWhenEmpty="True" 
                OnRowUpdating="UpdateBudgetRow" DataKeyNames="DestinationId" OnRowDeleting="ClearBudget">
              
                <EmptyDataTemplate>
                    <asp:DropDownList ID="DestDDL" runat="server" DataSourceID="DestDS" DataTextField="description" DataValueField="destinationID" OnDataBound="DestDDL_OnDataBound"></asp:DropDownList>
                    <asp:TextBox ID="budget_insert" runat="server" Text=""></asp:TextBox>
                    <asp:TextBox ID="package_insert" runat="server" Text="" MaxLength="32"></asp:TextBox>
                    <asp:RangeValidator  id="CustomValidator3" 
                        runat="server" ControlToValidate="budget_insert" 
                        ErrorMessage="Budget must be a whole number greater than 0 (Do not include cents)."
                        Display="None" ValidationGroup="InsertBudget" 
                        MinimumValue="1" MaximumValue="1000000000" Type="Integer">
                    </asp:RangeValidator>
                    <asp:Button ID="newBudget" runat="server" CssClass="button small" Text="+ Add" CommandName="Insert" ValidationGroup="InsertBudget" OnClick="CreateInitialBudget" />
                </EmptyDataTemplate>
            
            <Columns>
                
                 <asp:TemplateField HeaderText="Source" ItemStyle-VerticalAlign="Top">
                    <ItemTemplate>
                        <asp:Literal ID="site" runat="server" Text='<%# GetDestinationName(Eval("DestinationId")) %>'></asp:Literal>
                    </ItemTemplate>
                     <FooterTemplate>
                        <asp:DropDownList ID="DestDDL" runat="server" DataSourceID="DestDS" DataTextField="description" DataValueField="destinationID" OnDataBound="DestDDL_OnDataBound"></asp:DropDownList>
                    </FooterTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Monthly Cost" ItemStyle-VerticalAlign="Top">
                    <ItemTemplate>
                        <asp:Literal ID="budget" runat="server" Text='<%# ToDollars(Eval("Budget")) %>'></asp:Literal>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="budget_edit" runat="server" Text='<%# Bind("Budget") %>' ></asp:TextBox>
                          <asp:RangeValidator id="RangeValidator1" 
                                runat="server" ControlToValidate="budget_edit"
                                ErrorMessage="Budget must be a whole number greater than 0 (Do not include cents)."
                                Display="None"  ValidationGroup="EditBudget"
                                MinimumValue="1" MaximumValue="1000000000" Type="Integer" >
                        </asp:RangeValidator>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="budget_insert" runat="server" Text=""></asp:TextBox>
                    </FooterTemplate>
                    <InsertItemTemplate>
                         <asp:TextBox ID="budget_add" runat="server" Text='<%# Bind("Budget") %>' ></asp:TextBox>
                    </InsertItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Dealer Subscription Package" ItemStyle-VerticalAlign="Top">
                    <ItemTemplate>
                        <asp:Literal ID="package" runat="server" Text='<%# Eval("DescriptionText") %>'></asp:Literal>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="package_edit" runat="server" Text='<%# Bind("DescriptionText") %>' MaxLength="32"></asp:TextBox>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="package_insert" runat="server" Text="" MaxLength="32"></asp:TextBox>
                    </FooterTemplate>
                    <InsertItemTemplate>
                         <asp:TextBox ID="package_add" runat="server" Text='<%# Bind("DescriptionText") %>' MaxLength="32"></asp:TextBox>
                    </InsertItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="edit_link" runat="server" Text="Edit" Visible='true' CommandName="Edit"></asp:LinkButton>
                        <asp:LinkButton ID="delete_link" runat="server" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:LinkButton ID="update_link" runat="server" Text="Update" CommandName="Update" ValidationGroup="EditBudget"></asp:LinkButton>
                        <asp:LinkButton ID="cancel_link" runat="server" Text="Cancel" CommandName="Cancel" CausesValidation="False"></asp:LinkButton>
                    </EditItemTemplate>
                     <FooterTemplate>
                        <asp:Button ID="newBudget" runat="server" CssClass="button small" Text="+ Add" CommandName="Insert"  ValidationGroup="InsertBudget" OnClick="CreateNewBudget" />
                             <asp:RangeValidator  id="RangeValidator2" 
                                runat="server" ControlToValidate="budget_insert" 
                                ErrorMessage="Budget must be a whole number greater than 0 (Do not include cents)."
                                Display="None" ValidationGroup="InsertBudget" 
                                MinimumValue="1" MaximumValue="1000000000" Type="Integer">
                        </asp:RangeValidator>
                    </FooterTemplate>
                </asp:TemplateField>
                

            </Columns>
       
            </asp:GridView>

            <h2><asp:Label runat="server" ID="MerchandisingGraphLabel" AssociatedControlID="MerchandisingGraphDefault" >Merchandising Alert Graph</asp:Label></h2>
            <asp:RadioButtonList runat="server" ID="MerchandisingGraphDefault" RepeatDirection="Horizontal" />

                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="InsertBudget" DisplayMode="SingleParagraph"  />
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="EditBudget" DisplayMode="SingleParagraph"  />
        </div>
        
        
        <div class="report_settings_section" id="low_activity_report_settings">

            <h2>Low Activity Report</h2>
            <div class="fieldLabelPair">
                <label for="<%= txtLowActivityThreshold.ClientID %>">
                    Low Activity Threshold</label>
                <asp:TextBox ID="txtLowActivityThreshold" runat="server" />
                %
                <a class="help" title="<%=LowActivityThresholdHlep %>">What's this?</a>
                <asp:RangeValidator ID="LowActivityValidator"
                    ControlToValidate="txtLowActivityThreshold" MinimumValue="0" MaximumValue="100"
                    Type="Double" ErrorMessage="Low Activity Threshold should be between 0 and 100"
                    Text="Low Activity Threshold should be between 0 and 100" runat="server">
                </asp:RangeValidator>
            </div>
            <div class="fieldLabelPair">
                <label  for="<%= txtHighActivityThreshold.ClientID %>">
                    High Activity Threshold</label>
                <asp:TextBox ID="txtHighActivityThreshold" runat="server" />
                % 
                <a class="help" title="<%=LowActivityThresholdHlep %>">What's this?</a>
                <asp:RangeValidator ID="HighActivityValidator" ControlToValidate="txtHighActivityThreshold"
                    MinimumValue="0" MaximumValue="100" Type="Double" ErrorMessage="High Activity Threshold should be between 0 and 100"
                    Text="High Activity Threshold should be between 0 and 100" runat="server">
                </asp:RangeValidator>
            </div>
            <div class="fieldLabelPair">
                <label  for="<%= txtMinSearchCountThreshold.ClientID %>">
                    Minimum Search Count</label>
                <asp:TextBox ID="txtMinSearchCountThreshold" runat="server" />
                (20 - 100000) 
                <a class="help" title="<%=LowActivityThresholdHlep %>">What's this?</a>
                <asp:RangeValidator ID="MinSearchCountValidator" ControlToValidate="txtMinSearchCountThreshold"
                    MinimumValue="20" MaximumValue="100000" Type="Integer" ErrorMessage="Min search count should be between 20 and 100000" Text="Min search count should be between 20 and 100000"
                    runat="server">
                </asp:RangeValidator>
            </div>
        
        </div>
        <div class="report_settings_section" id="not_online_report_settings">
            <h2>Not Online Report</h2>
            <div class="fieldLabelPair">
               <label for="<%= ageInDays.ClientID %>">Min Age (days):</label>
                    <asp:DropDownList ID="ageInDays" runat="server">
                        <asp:ListItem Text="ALL" Value="0" />
                        <asp:ListItem Value="1" />
                        <asp:ListItem Value="2" />
                        <asp:ListItem Value="3" />
                        <asp:ListItem Value="4" />
                        <asp:ListItem Value="5" />
                        <asp:ListItem Value="6" />
                        <asp:ListItem Value="7" />
                        <asp:ListItem Value="8" />
                        <asp:ListItem Value="9" />
                        <asp:ListItem Value="10" />
                        <asp:ListItem Value="11" />
                        <asp:ListItem Value="12" />
                        <asp:ListItem Value="13" />
                        <asp:ListItem Value="14" />
                    </asp:DropDownList>
            </div>
        </div>


        <script type="text/javascript" id="TtmClientValidator">
            function ValidateTtmCompleteSelection(sender, args) {
                args.IsValid = false;
                $('.ttmComplete').each(function () {
                    if ($(this).find(":checkbox").attr("checked")) {
                        args.IsValid = true;
                        return;
                    }
                });
            }
        </script>

        <asp:CustomValidator 
            ID="TtmSettingsValidator" 
            runat="server" 
            ErrorMessage="You must select at least one Time to Market property to consider an ad complete" Display="None" ClientValidationFunction="ValidateTtmCompleteSelection" ClientIDMode="Predictable">
             
        </asp:CustomValidator>
        <div class="report_settings_section" id="ttm_report_settings">
            <h2>Time to Market</h2>
            <div class="fieldLabelPair">
                <asp:CheckBox ID="TTMPhotos" CssClass="ttmComplete" runat="server" />
                <label  for="<%= TTMPhotos.ClientID %>">
                    Photos</label>
            </div>
            <div class="fieldLabelPair">
                <asp:CheckBox ID="TTMDescription" CssClass="ttmComplete" runat="server" />
                <label  for="<%= TTMDescription.ClientID %>">
                    Description</label>
            </div>
            <div class="fieldLabelPair">
                <asp:CheckBox ID="TTMPrice" CssClass="ttmComplete" runat="server" />
                <label  for="<%= TTMPrice.ClientID %>">
                    Price</label>
            </div>
        </div>
        
        <div class="report_settings_section" id="ttm_report_settings_goaldays">
            <div class="fieldLabelPair">
                <label for="<%= txtTimeToMarketGoalDays.ClientID %>">Goal (days):</label>
                <asp:TextBox runat="server" ID="txtTimeToMarketGoalDays"/>
            </div>
            <asp:RegularExpressionValidator runat="server" ID="testTimeToMarketGoalDays" ControlToValidate="txtTimeToMarketGoalDays" ValidationExpression="^([1-9]\d*|0)" ErrorMessage="Goal days must be numeric"/>
        </div>

        
        <div class="formButtons">
            <asp:Button Text="Save" ID="SaveButton" runat="server" OnClick="OnSave" CssClass="button submit" />
        </div>
         
    </div>
</div>
