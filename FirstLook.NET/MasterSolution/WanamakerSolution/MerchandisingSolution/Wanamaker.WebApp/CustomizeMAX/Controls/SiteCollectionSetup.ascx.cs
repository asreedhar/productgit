using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using FirstLook.Merchandising.DomainModel.Commands;
using Wanamaker.WebApp.Controls;


namespace Wanamaker.WebApp.CustomizeMAX.Controls
{

    public partial class SiteCollectionSetup : BusinessUnitUserControl
    {
        protected void Collection_Deleting(object sender, SqlDataSourceCommandEventArgs e)
        {
            e.Command.Parameters["@BusinessUnitID"].Value = BusinessUnitID;
        }

        protected void EditDest_Bound(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;
            if (ddl == null) return;

            GridViewRow gvr = ddl.NamingContainer as GridViewRow;
            if (gvr == null) return;

            DataKey dk = CollectionGV.DataKeys[gvr.DataItemIndex];
            if (dk != null && ddl.Items.FindByValue(dk.Value.ToString()) != null)
            {
                ddl.SelectedValue = dk.Value.ToString();
            }
        }

        protected void Destination_Editing(object sender, GridViewEditEventArgs e)
        {

            DropDownList currDDL = CollectionGV.Rows[e.NewEditIndex].FindControl("DestDDL") as DropDownList;
            if (currDDL == null)
            {
                return;
            }
            DataKey dk = CollectionGV.DataKeys[e.NewEditIndex];
            if (dk != null)
            {
                currDDL.SelectedValue = dk.Value.ToString();
            }
        }
        protected void Destination_Updating(object sender, GridViewUpdateEventArgs e)
        {

            DropDownList currDDL = CollectionGV.Rows[e.RowIndex].FindControl("DestDDL") as DropDownList;
            if (currDDL != null)
            {
                int destID = Int32.Parse(currDDL.SelectedValue);
                e.NewValues["NewDestinationID"] = destID;

            }
            CheckBox cb = CollectionGV.Rows[e.RowIndex].FindControl("enabledCB") as CheckBox;
            if (cb != null)
            {
                e.NewValues["activeFlag"] = cb.Checked;
            }

        }

        protected void Collection_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@BusinessUnitID"].Value = BusinessUnitID;
        }
        protected void Collection_Updating(object sender, SqlDataSourceCommandEventArgs e)
        {
            DropDownList ddl = CollectionGV.Rows[CollectionGV.EditIndex].FindControl("DestDDL") as DropDownList;
            if (ddl == null) return;

            e.Command.Parameters["@NewDestinationID"].Value = ddl.SelectedValue;
            e.Command.Parameters["@BusinessUnitID"].Value = BusinessUnitID;

        }


        protected void AddNewCollectionSource_Click(object sender, EventArgs e)
        {
            collectionSettingsDS.Insert();
        }

        protected void Destination_Bound(object sender, GridViewRowEventArgs e)
        {
            var row = e.Row;
            if (row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = row.DataItem as DataRowView;
                if (drv == null) throw new ApplicationException("You changed the data source...it needs a datarowview");

                BindReleaseAndCollectionCheckboxes(row, drv);
                DisplayLoginFailurePanel(row, drv);
            }else if(row.RowType == DataControlRowType.Footer)
            {
                BindAddNewCollectionSourceRow(sender, e);
            }
        }

        private static void BindAddNewCollectionSourceRow(object sender, GridViewRowEventArgs e)
        {
            var ddl = GetControl<DropDownList>(e.Row, "DestDDL");
            var selectedSource = ddl.SelectedItem.Text;

            var label = GetControl<HtmlGenericControl>(e.Row, "AddSourceCredentialDisclaimer");
            label.InnerText =
                string.Format(
                    "I am an authorized {0}  user with a valid username and password. I would like to automate the retrieval of my dealership�s online performance data daily from {0} for online inventory performance analysis.",
                    selectedSource);
        }

        private static void BindReleaseAndCollectionCheckboxes(Control row, DataRowView drv)
        {
            var rcb = GetControl<CheckBox>(row, "releaseCB");
            var ccb = GetControl<CheckBox>(row, "collectCB");

            try {
                ccb.Visible = (bool)drv["canCollectMetricsFrom"];
                rcb.Visible = (bool)drv["canReleaseTo"];
            } catch( System.InvalidCastException e ) {
                row.Visible = false; // freeform provider; do not show settings for
            }
        }

        private static T GetControl<T>(Control parent, string id) where T : class
        {
            T ctrl;
            if(!TryGetControl(parent, id, out ctrl))
                throw new ApplicationException("Failed to find child control with ID '" + id + "'.");
            return ctrl;
        }

        private static bool TryGetControl<T>(Control parent, string id, out T foundControl) where T : class
        {
            if (parent == null) throw new ArgumentNullException("parent");
            if (id == null) throw new ArgumentNullException("id");
            foundControl = parent.FindControl(id) as T;
            return foundControl != null;
        }

        //forgive me - this is heinous, but I need to do it to make this mangled grid view work
        private DropDownList newDataIODDL = null;
        protected void InitialDataInOut_Click(object sender, EventArgs e)
        {
            Button b = sender as Button;
            if (b == null) throw new ApplicationException("Button not found");

            newDataIODDL = b.NamingContainer.FindControl("DestDDL") as DropDownList;
            collectionSettingsDS.Insert();
        }

        protected void Collection_Inserting(object sender, SqlDataSourceCommandEventArgs e)
        {

            e.Command.Parameters["@BusinessUnitID"].Value = BusinessUnitID;
            if (newDataIODDL != null)  //it was an empty row insert
            {
                e.Command.Parameters["@DestinationID"].Value = newDataIODDL.SelectedValue;
                e.Command.Parameters["@Username"].Value = string.Empty;
                e.Command.Parameters["@Password"].Value = string.Empty;
            }
            else
            {
                DropDownList ddl = CollectionGV.FooterRow.FindControl("DestDDL") as DropDownList;
                if (ddl == null) throw new ApplicationException("Destination DDL not found in footer!");

                TextBox userTB = CollectionGV.FooterRow.FindControl("usernameTB") as TextBox;
                TextBox passwordTB = CollectionGV.FooterRow.FindControl("passwordTB") as TextBox;
                if (userTB == null || passwordTB == null)
                    throw new ApplicationException("username/password textbox now found in footer!");

                e.Command.Parameters["@DestinationID"].Value = ddl.SelectedValue;
                e.Command.Parameters["@Username"].Value = userTB.Text;
                e.Command.Parameters["@Password"].Value = passwordTB.Text;
            }


        }

        protected string GetPWDisplay(string username, string pw)
        {
            if (!string.IsNullOrEmpty(pw))
            {
                return "******";
            }
            if (!string.IsNullOrEmpty(username))
            {
                return "None provided!";
            }
            return string.Empty;
        }

        protected void Collection_Inserted(object sender, SqlDataSourceStatusEventArgs e)
        {
            CollectionGV.DataBind();
        }

        #region Clear Failed Login routines

        protected void ClearLoginFailure(object sender, EventArgs e)
        {
            var button = (LinkButton) sender;

            var destinationId = int.Parse(button.Attributes["destinationID"]);

            ListingSitePreferences_ClearLoginFailure_Command.Execute(BusinessUnitID, destinationId);

            DataBind();
        }

        private static void DisplayLoginFailurePanel(Control row, DataRowView drv)
        {
            Panel panel;
            if (TryGetControl(row, "failedLoginPanel", out panel))
                panel.Visible = (bool)drv["hasFailedLogin"];

            LinkButton linkButton;
            if (TryGetControl(row, "clearLoginFailure", out linkButton))
                linkButton.Attributes.Add("destinationID", drv["destinationID"].ToString());
        }

        #endregion
    }
}