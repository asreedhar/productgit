﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Analytics.ascx.cs" Inherits="Wanamaker.WebApp.CustomizeMAX.Controls.Analytics" %>

<div id="analytics">
	<asp:Panel ID="ApiError" runat="server" Text="" Visible="false"></asp:Panel>
    <asp:Panel ID="SetUpButtonSection" runat="server">
        <p>MAX Systems is requesting access to your Google analytics data for<br />
        Digital Performance Management Dashboard and other analytics.</p>
        <p><asp:Button ID="SetUpButton" Text="Setup Google Account" runat="server" onclick="SetUpButton_Click"/></p>
    </asp:Panel>
	<asp:Panel ID="ProfilePanel" runat="server" Visible="false">

        <p id="profileInstructions" runat="server">Select the correct profiles and click the save button.</p>
        <p id="mustClearBlurb" runat="server" visible="false">To choose a different profile you must first click Clear Account</p>

        <fieldset>
            <asp:Panel ID="profile" runat="server"><label class="analyticsLabel" for="personalComputerDropDown">PC Profile</label> <asp:DropDownList ID="personalComputerDropDown" runat="server"/><br /></asp:Panel>
            <asp:Panel ID="profile_readOnly" runat="server"><label class="analyticsLabel" for="personalComputerDropDown_readOnly">PC Profile</label> <asp:Label CssClass="readOnly_settings_value" ID="personalComputerDropDown_readOnly" runat="server"/><br /></asp:Panel>

            <label class="analyticsLabel" for="siteProviderDropDown">Site Provider</label> <asp:DropDownList ID="siteProviderDropDown" runat="server"/><br />
            
            <asp:Panel ID="lockbox" runat="server"><label class="analyticsLabel" for="adminLock">Admin Lock</label> <asp:CheckBox ID="adminLock" runat="server" /></asp:Panel>
        </fieldset>
        
	    <p><asp:Button ID="saveButton" runat="server" Text="Save" CssClass="button" onclick="saveButton_Click" /></p>
        <p><asp:Button ID="clearAccountButton" runat="server" Text="Clear Account" CssClass="button" OnClick="ClearClick"/></p>

    </asp:Panel>
</div>

<script type="text/javascript">
    // wait for jQuery to become available (this feels kind of ridiculous)
    var $_wait = setInterval(function () {
        if (typeof window.$ == "undefined")
            return;
        clearInterval($_wait);
        $("#personalComputerDropDown").on("change", function () {
            var adminLock = $("#adminLock");
            if (adminLock.length > 0)
                adminLock.prop("checked", true);
        });
    }, 250);
</script>

