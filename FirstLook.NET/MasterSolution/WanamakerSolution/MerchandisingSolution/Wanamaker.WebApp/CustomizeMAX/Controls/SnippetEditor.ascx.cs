using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Templating;
using MerchandisingLibrary;
using log4net;

namespace Wanamaker.WebApp.CustomizeMAX.Controls
{

    public partial class SnippetEditor : System.Web.UI.UserControl
    {
        #region Logging

        private static readonly ILog Log = LogManager.GetLogger(typeof(SnippetEditor).FullName);

        #endregion

        public int BusinessUnitID
        {
            set
            {
                if (BusID.Value != value.ToString())
                {
                    BusID.Value = value.ToString();
                    loadNodeBlurbCategory();
                }
            }
            get
            {

                return Int32.Parse(BusID.Value);
            }
        }
        public int BlurbID
        {
            get
            {
                object o = ViewState["BlurbID"];
                return (o == null) ? -1 : (int)o;
            }
            set
            {
                ViewState["BlurbID"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected void TextRow_Editing(object sender, GridViewEditEventArgs e)
        {

        }
        protected void TextSamples_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["businessUnitID"] = BusinessUnitID;
        }
        protected void TextSamples_Deleting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            if (e.InputParameters.Contains("blurbID"))
            {
                e.InputParameters["blurbID"] = BlurbID;
            }
            e.InputParameters["businessUnitID"] = BusinessUnitID;

        }
        protected void TextSamples_Updating(object sender, ObjectDataSourceMethodEventArgs e)
        {
            if (e.InputParameters.Contains("blurbID"))
            {
                e.InputParameters["blurbID"] = BlurbID;
            }
            e.InputParameters["businessUnitID"] = BusinessUnitID;


            DropDownList prof = SampleTextGV.Rows[SampleTextGV.EditIndex].FindControl("VehicleProfileDDL") as DropDownList;
            if (prof == null) throw new ApplicationException("No profile DDL");

            e.InputParameters["vehicleProfileId"] = prof.SelectedValue;

            DropDownList thm = SampleTextGV.Rows[SampleTextGV.EditIndex].FindControl("ThemeDDL") as DropDownList;
            if (thm == null) throw new ApplicationException("No theme DDL");
            e.InputParameters["themeId"] = thm.SelectedValue;
        }
        protected void Profiles_Bound(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;
            if (ddl == null) return;

            ddl.Items.Insert(0, new ListItem("Any vehicle...", "-1"));


        }
        protected void EditProfiles_Bound(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;
            if (ddl == null) return;

            ddl.Items.Insert(0, new ListItem("Any vehicle...", "-1"));


            int profileId = ((BlurbText)((GridViewRow)ddl.NamingContainer).DataItem).Match.Id;
            if (ddl.Items.FindByValue(profileId.ToString()) != null)
            {
                ddl.SelectedValue = profileId.ToString();
            }

        }
        public void UpdateProfileNames()
        {
            VehicleProfileDDL.DataBind();
        }
        protected void TemplateNodeExpanding(Object sender, TreeNodeEventArgs e)
        {
            TreeNode tnSelected = e.Node;

            if(tnSelected.Depth == 1) // blurb category (blurb type)
                loadNodeBlurb(tnSelected);
            
        }
        protected void loadNodeBlurbCategory()
        {
            // clear it
            blurbTree.Nodes.Clear();

            // load blub tree 
            TreeNode tnRoot = new TreeNode("ToolBox", "0");
            blurbTree.Nodes.Add(tnRoot);
            tnRoot.SelectAction = TreeNodeSelectAction.None;

            List<TemplateCategory> blurbCategoryList = TemplateCategory.FetchCategoryList();

            foreach (TemplateCategory myCatagory in blurbCategoryList)
            {
                TreeNode myChild = new TreeNode(myCatagory.CategoryName, myCatagory.CategoryID.ToString());
                myChild.PopulateOnDemand = true;
                myChild.SelectAction = TreeNodeSelectAction.Expand;
                tnRoot.ChildNodes.Add(myChild);
            }
            tnRoot.Expand();


        }
        protected void loadNodeBlurb(TreeNode tnCategory)
        {
            // loads the category (blurb type) with associated snippets (blurbs) for this bu and firstLook (100150)

            int iCategory;

            Int32.TryParse(tnCategory.Value, out iCategory );  // this is the blurbs type in the database (confusing)

            List<TemplateBlurb> blurbs = TemplateBlurb.FetchBlurbsByType(this.BusinessUnitID, iCategory);

            foreach (TemplateBlurb blurb in blurbs)
            {
                string blurbName = blurb.BlurbName;
                string blurbID = blurb.BlurbID.ToString();

                if(String.IsNullOrEmpty(blurbName)) // make sure the description is not null
                    blurbName = blurbID;

                TreeNode tnChild = new TreeNode(blurbName, blurbID);
                    tnChild.SelectAction = TreeNodeSelectAction.Select;
                    tnCategory.ChildNodes.Add(tnChild);
            }
        }
        protected void TemplateBlurb_Selected(object sender, EventArgs e)
        {
            TreeNode tn = blurbTree.SelectedNode;
            if (tn.Depth > 1) //it is a category, do nothing
            {

                BlurbCreatorFV.ChangeMode(FormViewMode.Edit);
                BlurbID = Int32.Parse(tn.Value);
                BlurbCreatorFV.DataBind();
                setSaveButton();
            }

        }
        private void setSaveButton()
        {
            Button but = BlurbCreatorFV.FindControl("SaveButton") as Button;
            Button but2 = BlurbCreatorFV.FindControl("CreateButton") as Button;
            if (but != null && but2 != null)
            {
                but.Visible = true;
                but2.Visible = false;
            }
        }
        private void setCreateButton()
        {
            Button but = BlurbCreatorFV.FindControl("SaveButton") as Button;
            Button but2 = BlurbCreatorFV.FindControl("CreateButton") as Button;
            if (but != null && but2 != null)
            {
                but.Visible = true;
                but2.Visible = false;
            }
        }
        protected void BlurbMode_Changing(object sender, FormViewModeEventArgs e)
        {
            if (e.NewMode == FormViewMode.Edit)
            {
                setSaveButton();
            }
            else
            {
                setCreateButton();
            }

        }
        protected void Blurb_Inserted(object sender, FormViewInsertedEventArgs e)
        {
            // todo: need to refresh the tree
            blurbTree.DataBind();
        }
        protected void Blurb_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            TreeNode tn = blurbTree.SelectedNode;
            if (tn == null)
            {
                return;
            }
            if (tn.Depth == 1)
            {
                //it is a category, do nothing
                e.Cancel = true;
                //EditorTabView.ActiveTabIndex = -1;
            }
            e.InputParameters["BusinessUnitId"] = BusinessUnitID;

        }
        protected void MergeRules_Bound(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;
            if (ddl == null) return;

            ddl.Items.Insert(0, new ListItem("Select Condition...", ""));
        }
        protected void BlurbCreatorFV_DataBound(object sender, EventArgs e)
        {
            DropDownList ddlBI = BlurbCreatorFV.FindControl("BasicInfoDDL") as DropDownList;
            DropDownList ddlOR = BlurbCreatorFV.FindControl("OptionsRulesDDL") as DropDownList;
            DropDownList ddlConsInf = BlurbCreatorFV.FindControl("ConsumerInfoDDL") as DropDownList;
            DropDownList ddlCarfInf = BlurbCreatorFV.FindControl("CarfaxInfoDDL") as DropDownList;
            if (ddlBI == null || ddlOR == null || ddlConsInf == null || ddlCarfInf == null)
            {
                return;
            }
            ddlConsInf.DataBind();
            ddlConsInf.Items.Insert(0, "Select consumer info...");

            ddlCarfInf.DataBind();
            ddlCarfInf.Items.Insert(0, "Select carfax info...");

            ddlOR.DataBind();
            ddlOR.Items.Insert(0, "Select rule...");

            ddlBI.DataBind();
            ddlBI.Items.Insert(0, "Select vehicle basics...");


            Label lb = BlurbCreatorFV.FindControl("NotOwnerLbl") as Label;

            if (BlurbCreatorFV.CurrentMode == FormViewMode.Edit)
            {
                DropDownList blurbCategory = BlurbCreatorFV.FindControl("BlurbCategoryDDL") as DropDownList;
                TemplateBlurb tb = ((TemplateBlurb)BlurbCreatorFV.DataItem);
                int btId = tb.BlurbTypeId;
                if (blurbCategory != null && blurbCategory.Items.FindByValue(btId.ToString()) != null)
                    blurbCategory.SelectedValue = btId.ToString();
                if (lb != null)
                {
                    if (tb.IsOwner(BusinessUnitID))
                    {
                        lb.Visible = false;
                        BlurbCreatorFV.Enabled = true;

                    }
                    else
                    {
                        lb.Visible = true;
                        BlurbCreatorFV.Enabled = false;

                    }
                }
            }
            else
            {
                if (lb != null) lb.Visible = false;
            }



        }
        protected void Blurb_Deleting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            e.InputParameters["BusinessUnitID"] = BusinessUnitID;


        }
        protected void Blurb_InsUpdating(object sender, ObjectDataSourceMethodEventArgs e)
        {


            TemplateBlurb updateMe = new TemplateBlurb();

            if (e.InputParameters.Contains("BlurbID"))
            {
                updateMe.BlurbID = (int)e.InputParameters["BlurbID"];
                e.InputParameters.Remove("BlurbID");

            }

            e.InputParameters["BusinessUnitId"] = BusinessUnitID;

            DropDownList typeDDL = BlurbCreatorFV.FindControl("BlurbCategoryDDL") as DropDownList;
            TextBox titleTB = BlurbCreatorFV.FindControl("BlurbNameTB") as TextBox;
            TextBox condTB = BlurbCreatorFV.FindControl("ConditionTB") as TextBox;
            TextBox bodyTB = BlurbCreatorFV.FindControl("TemplateBodyTB") as TextBox;
            HiddenField owner = BlurbCreatorFV.FindControl("ownerId") as HiddenField;

            if (titleTB != null)
            {
                updateMe.BlurbName = titleTB.Text;
            }

            if (condTB != null)
            {
                updateMe.TemplateCondition = condTB.Text;
            }

            if (bodyTB != null)
            {
                updateMe.TemplateText = bodyTB.Text;
            }

            if (typeDDL != null)
            {
                updateMe.BlurbTypeId = Int32.Parse(typeDDL.SelectedValue);
            }
            if (owner != null && !string.IsNullOrEmpty(owner.Value))
            {
                updateMe.OwnerId = Int32.Parse(owner.Value);
            }
            else
            {
                updateMe.OwnerId = BusinessUnitID;
            }

            e.InputParameters["blurb"] = updateMe;
            if (e.InputParameters.Contains("blurbName"))
            {
                e.InputParameters.Remove("blurbName");
            }
            

            if (e.InputParameters.Contains("TemplateText"))
            {
                e.InputParameters.Remove("TemplateText");
            }

            if (e.InputParameters.Contains("TemplateCondition"))
            {
                e.InputParameters.Remove("TemplateCondition");
            }
            string user = HttpContext.Current.User.Identity.Name ?? String.Empty;
            Log.Warn(string.Format("Updating BlurbTemplate: User = {0} , BlurbName = {1}, BlurbID = {2}", user, updateMe.BlurbName, updateMe.BlurbID));
        }
        private void addRuleToCondition(string newPoint)
        {
            ((TextBox)BlurbCreatorFV.FindControl("ConditionTB")).Text += "<" + newPoint + ">";
        }
        private void addPointToTemplate(string newPoint)
        {
            ((TextBox)BlurbCreatorFV.FindControl("TemplateBodyTB")).Text += " <" + newPoint + ">";
            //BlurbCreatorFV.DataBind();
        }
        protected void Condition_Selected(object sender, EventArgs e)
        {
            DropDownList theDDL = (DropDownList)sender;
            if (theDDL.SelectedIndex == 0)
            {
                return;
            }
            addRuleToCondition(theDDL.SelectedItem.Text);

        }
        protected void Rule_Selected(object sender, EventArgs e)
        {

            DropDownList theDDL = (DropDownList)sender;
            if (theDDL.SelectedIndex == 0)
            {
                return;
            }

            addPointToTemplate(theDDL.SelectedItem.Text);


        }
        protected void AddNew_Click(object sender, EventArgs e)
        {
            int? theme = null;
            if (!string.IsNullOrEmpty(ThemeDDL.SelectedValue))
            {
                theme = int.Parse(ThemeDDL.SelectedValue);
            }
            BlurbText.Create(BusinessUnitID, BlurbID, PreBlurbTBInsert.Text, PostBlurbTBInsert.Text, isLongCB.Checked, int.Parse(VehicleProfileDDL.SelectedValue), theme, isHeaderCheckBox.Checked);

            SampleTextGV.DataBind();
        }
        public string getAddTextJS(string textToAdd)
        {
            TextBox tb = BlurbCreatorFV.FindControl("ConditionTB") as TextBox;
            if (tb == null) return string.Empty;
            return "javascript:document.getElementById('" + tb.ClientID + "').value += '" + textToAdd + "';return false;";
        }
        protected void NewSnippet_Click(object sender, EventArgs e)
        {
            BlurbCreatorFV.Enabled = true;
            BlurbCreatorFV.ChangeMode(FormViewMode.Insert);
        }
        protected string GetThemeVal(object obj)
        {
            int? myVal = (int?)obj;
            if (myVal.HasValue)
            {
                return myVal.Value.ToString();
            }
            return string.Empty;
        }
    }
}