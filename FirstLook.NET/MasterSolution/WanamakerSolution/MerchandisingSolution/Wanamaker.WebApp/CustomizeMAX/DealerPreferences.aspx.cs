using System;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using AutoLoad;
using BulkWindowSticker;
using FirstLook.DomainModel.Oltp;
using FirstLook.LotIntegration.DomainModel;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Alerts;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.WindowSticker;
using MAX.ExternalCredentials;
using Merchandising.Messages;
using Merchandising.Messages.AutoLoad;
using VehicleDataAccess;
using Wanamaker.WebApp.AppCode.AccessControl;
using Wanamaker.WebApp.CustomizeMAX.Controls;
using Wanamaker.WebApp.Workflow;

namespace Wanamaker.WebApp.CustomizeMAX
{

    public partial class DealerPreferences : BasePage
    {
        #region Injected dependencies

        public IFileStoreFactory FileStoreFactory { get; set; }
        public IAdMessageSender MessageSender { get; set; }

        #endregion
        
        public bool IsAdministrator;

        public int BusinessUnitID
        {
            get
            {
                object o = ViewState[ "BusinessUnitID" ];
                return ( o == null ) ? -1 : ( int )o;
            }
            set
            {
                ViewState[ "BusinessUnitID" ] = value;
            }
        }

        private Agent EditingCredentialedSite
        {
            get
            {
                return (Agent)Convert.ToInt32(AutomaterMakesDDL.SelectedValue);
            }
        }

        protected void Page_Init ( object sender, EventArgs e )
        {
            IsAdministrator = User.IsInRole( "Administrator" );

            if ( !IsAdministrator )
            {
                preferencesTabs.Controls.Remove( Specials );
                preferencesTabs.Controls.Remove( VehicleTargets );
                preferencesTabs.Controls.Remove( Campaigns );
                preferencesTabs.Controls.Remove( PreApprove );
            }


        }

        protected void Page_Load ( object sender, EventArgs e )
        {
            if ( !IsPostBack )
            {
                //used to switch default tabs.
                string tab = Request.QueryString["t"];
                if (tab != null)
                {
                    var tabPanel = preferencesTabs.Tabs.Cast<FirstLook.Common.WebControls.UI.TabPanel>().Where(x => x.Text == tab).SingleOrDefault();
                    if (tabPanel != null)
                        preferencesTabs.ActiveTabIndex = preferencesTabs.Tabs.IndexOf(tabPanel);
                    
                    // used when referrer wishes to immediately be sent to the "Setup Google Account" action of the "Analytics" tab.
                    // this would have been a URL parameter except that the Google Analytics API has to know about the referring URL
                    if( tab == "Analytics"
                    &&  Request.UrlReferrer != null 
                    &&  Request.UrlReferrer.LocalPath.ToLower() == "/merchandising/digitalperformanceanalysis.aspx" )
                    {
                        AnalyticsSettings.WebAuthorization.SendRedirect();
                    }
                }

                BusinessUnit bu = Context.Items[ "BusinessUnit" ] as BusinessUnit;
                if ( bu == null )
                {
                    Response.Redirect( "~/Default.aspx" );
                }
                BusinessUnitID = bu.Id;

                // check to display auto window sticker
                var windowStickerRepository = new WindowStickerRepository();
                ViewState["WINDOWSTICKER"] = windowStickerRepository.HasAutoWindowSticker(BusinessUnitID);

                SetupDisclaimer();
	            SetUpCertified();

                // load the autoload provider drop down
                var credentialSiteRepo = new CredentialedSiteRepository();
                AutomaterMakesDDL.DataSource = credentialSiteRepo.FetchAutoLoadSettingSites();
                AutomaterMakesDDL.DataValueField = "SiteId";
                AutomaterMakesDDL.DataTextField = "ManufacturerName";
                AutomaterMakesDDL.DataBind();
            }

            specialsTool.OwnerEntityId = BusinessUnitID;
            profiler.BusinessUnitId = BusinessUnitID;

            if(!Convert.ToBoolean((ViewState["WINDOWSTICKER"])))
                preferencesTabs.Controls.Remove(AutoWindowSticker);

        }

	    private void SetUpCertified()
	    {
		    var pricingContext = PricingContext.FromMerchandising(BusinessUnitID);

			ManufacturerProgramsLink.NavigateUrl = string.Format("{0}?oh={1}", ConfigurationManager.AppSettings["certified_manufacturer_admin_url"], pricingContext.OwnerHandle);
			DealerProgramsLink.NavigateUrl = string.Format("{0}?oh={1}", ConfigurationManager.AppSettings["certified_dealer_admin_url"], pricingContext.OwnerHandle);

		}

        protected void OnTabChanged(object sender, EventArgs args)
        {
            var tabPanel = preferencesTabs.Tabs.Cast<FirstLook.Common.WebControls.UI.TabPanel>().Where(x => x.Text == "Analytics").SingleOrDefault();
            if (tabPanel != null)
            {
                int analyticsIndex = preferencesTabs.Tabs.IndexOf(tabPanel);
                if(analyticsIndex == preferencesTabs.ActiveTabIndex && Request.QueryString["t"] == null)
                    Response.Redirect(Request.RawUrl + "?t=Analytics");
            }
        }

        protected void Edt_Selecting ( object sender, SqlDataSourceSelectingEventArgs e )
        {
            e.Command.Parameters[ "@BusinessUnitId" ].Value = BusinessUnitID;
        }

        protected void PhotoTemplate_Selecting ( object sender, ObjectDataSourceSelectingEventArgs e )
        {
            e.InputParameters[ "BusinessUnitId" ] = BusinessUnitID;
        }

        protected void PhotoTemplate_Updating ( object sender, ObjectDataSourceMethodEventArgs e )
        {
            int photoTemplateItemId = ( int )e.InputParameters[ "PhotoTemplateItemId" ];
            int sequenceNumber = ( int )e.InputParameters[ "SequenceNumber" ];

            e.InputParameters.Clear();

            e.InputParameters.Add( "businessUnitId", BusinessUnitID );
            e.InputParameters.Add( "sequenceNumber", sequenceNumber );
            e.InputParameters.Add( "photoTemplateItemId", photoTemplateItemId );
        }

        protected void PhotoTemplate_Deleting ( object sender, ObjectDataSourceMethodEventArgs e )
        {
            e.InputParameters[ "BusinessUnitId" ] = BusinessUnitID;
        }

        protected void UpTier_Click ( object sender, EventArgs e )
        {
            ImageButton but = sender as ImageButton;
            if ( but == null ) return;

            int tier = Int32.Parse( TierDDL.SelectedValue );
            if ( tier != 1 )
            {
                processNewTierArgs( but.CommandArgument, tier - 1 );
            }
            PriorityList.DataBind();
        }

        protected void DownTier_Click ( object sender, EventArgs e )
        {
            ImageButton but = sender as ImageButton;
            if ( but == null ) return;

            int tier = Int32.Parse( TierDDL.SelectedValue );
            if ( tier != 4 )
            {
                processNewTierArgs( but.CommandArgument, tier + 1 );
            }
            PriorityList.DataBind();
        }

        private static void processNewTierArgs ( string newTierArgs, int newTier )
        {
            //move tier to next tier up...
            string[] newTierArgsAra = newTierArgs.Split( "|".ToCharArray() );
            if ( newTierArgsAra.Length >= 2 )
            {
                int displayRanking = Int32.Parse( newTierArgsAra[ 1 ] );
                int rankingId = Int32.Parse( newTierArgsAra[ 0 ] );
                new DisplayPriorityDataSource().Update( rankingId, displayRanking, newTier );
            }

        }

        protected void Priority_Selecting ( object sender, ObjectDataSourceSelectingEventArgs e )
        {
            e.InputParameters[ "businessUnitId" ] = BusinessUnitID;
        }

        protected void Priority_Updating ( object sender, ObjectDataSourceMethodEventArgs e )
        {
            int tmpId = ( int )e.InputParameters[ "rankingID" ];
            int tmpRnk = ( int )e.InputParameters[ "displayPriority" ];
            e.InputParameters.Clear();

            e.InputParameters.Add( "rankingID", tmpId );
            e.InputParameters.Add( "displayPriority", tmpRnk );
            e.InputParameters.Add( "tier", Int32.Parse( TierDDL.SelectedValue ) );
        }

        public static int GetTierCount ( object evalObj, int tierNumber )
        {
            return ( ( int[] )evalObj )[ tierNumber ];
        }

        public static int GetGasMileageBySegment ( object evalObj, int segmentId )
        {
            return ( ( int[] )evalObj )[ segmentId ];
        }

        protected void LotLocations_Selecting ( object sender, SqlDataSourceSelectingEventArgs e )
        {
            e.Command.Parameters[ "@BusinessUnitId" ].Value = BusinessUnitID;
        }

        protected void Alerting_Selecting ( object sender, SqlDataSourceSelectingEventArgs e )
        {
            e.Command.Parameters[ "@BusinessUnitId" ].Value = BusinessUnitID;
        }

        protected void Alerting_InsertUpdating ( object sender, SqlDataSourceCommandEventArgs e )
        {
            e.Command.Parameters[ "@BusinessUnitId" ].Value = BusinessUnitID;
        }

        protected void AlertRow_Bound ( object sender, RepeaterItemEventArgs e )
        {
            DropDownList timeDDL = e.Item.FindControl( "TimeDDL" ) as DropDownList;
            if ( timeDDL == null ) return;

            if ( e.Item.ItemIndex != 5 )
            {
                e.Item.Visible = false;
                return;
            }

            HiddenField reportID = e.Item.FindControl( "reportIDField" ) as HiddenField;
            if ( reportID == null ) return;
            int reportId = Int32.Parse( reportID.Value );

            Subscription alerts = Subscription.Fetch( reportId, BusinessUnitID, IdentityHelper.GetFirstlookId() );
            //loop over all the controls and only look at the checkboxes
            timeDDL.SelectedValue = alerts.TimeOfDay.ToString();
            foreach ( Control ctl in e.Item.Controls )
            {
                CheckBoxPlus cbp = ctl as CheckBoxPlus;
                if ( cbp == null ) continue;

                cbp.Checked = alerts.SubscribesOnDay( Int32.Parse( cbp.CheckBoxArgument ) );

            }

            TextBox emailTB = e.Item.FindControl( "email" ) as TextBox;
            if ( emailTB == null ) return;

            emailTB.Text = alerts.Email;
        }

        protected void Settings_Selecting ( object sender, SqlDataSourceSelectingEventArgs e )
        {
            e.Command.Parameters[ "@BusinessUnitId" ].Value = BusinessUnitID;
        }
        
        protected void Profiles_Selecting ( object sender, SqlDataSourceSelectingEventArgs e )
        {
            e.Command.Parameters[ "@BusinessUnitId" ].Value = BusinessUnitID;
        }
        
        protected void Settings_Updating ( object sender, SqlDataSourceCommandEventArgs e )
        {
            e.Command.Parameters[ "@BusinessUnitId" ].Value = BusinessUnitID;
        }
        
        protected void Profile_Bound ( object sender, EventArgs e )
        {
            DropDownList ddl = sender as DropDownList;
            if ( ddl != null )
            {
                ddl.Items.Insert( 0, new ListItem( "Any Vehicle...", "" ) );
            }
        }
        
        protected void ReplaceText_Changed ( object sender, EventArgs e )
        {
            TextBox changedTB = sender as TextBox;
            if ( changedTB == null ) return;

            ReorderListItem rli = changedTB.NamingContainer as ReorderListItem;
            if ( rli == null ) return;

            object dk = PriorityList.DataKeys[ rli.DataItemIndex ];
            if ( dk != null )
            {
                int reorderId = ( int )dk;
                DisplayPriorityDataSource.UpdateDisplayDescription( reorderId, changedTB.Text );
            }
        }
        
        protected void StickyCB_Changed ( object sender, EventArgs e )
        {
            CheckBoxPlus cbp = sender as CheckBoxPlus;
            if ( cbp == null ) return;

            int id = Int32.Parse( cbp.CheckBoxArgument );
            DisplayPriorityDataSource.UpdateSticky( id, cbp.Checked );
        }
        
        protected string GetProvider ( object evalObj )
        {
            if ( evalObj != DBNull.Value )
            {
                int providerId = ( int )evalObj;
                switch ( providerId )
                {
                    case 2:
                        return "Dealer Specialties";
                    case 5:
                        return "eBizAutos";
                    default:
                        return "Contact FirstLook";
                }
            }

            return "None";
        }

        protected void VehicleProfileNames_Updated ( object sender, EventArgs e )
        {
            //update the tagline vehicle profile id...
            //editTag.UpdateProfiles();        
        }

        protected void Templates_Selecting ( object sender, ObjectDataSourceSelectingEventArgs e )
        {
            e.InputParameters[ "BusinessUnitId" ] = BusinessUnitID;
        }

        protected void EquipGroups_Selecting ( object sender, ObjectDataSourceSelectingEventArgs e )
        {
            e.InputParameters[ "BusinessUnitId" ] = BusinessUnitID;
            e.InputParameters[ "generics" ] = new GenericEquipmentCollection();
        }

        protected void EquipGroup_Deleting ( object sender, ObjectDataSourceMethodEventArgs e )
        {
            e.InputParameters[ "businessUnitId" ] = BusinessUnitID;
            e.InputParameters[ "memberLogin" ] = Context.User.Identity.Name;
        }

        private void SetupDisclaimer ()
        {
            CustomDisclaimer disclaimer = CustomDisclaimer.FetchCustomDisclaimer( BusinessUnitID );
            txtAppend.Text = disclaimer.CustomText;
            txtReplace.Text = disclaimer.CustomText;
            if ( disclaimer.IsAppend )
            {
                rdoAppend.Checked = true;
                cpeAppend.Collapsed = false;
                cpeReplace.Collapsed = true;
            }
            else if ( disclaimer.IsDefault )
            {
                rdoDefault.Checked = true;
                cpeAppend.Collapsed = true;
                cpeReplace.Collapsed = true;
            }
            else if ( disclaimer.IsReplace )
            {
                rdoReplace.Checked = true;
                cpeReplace.Collapsed = false;
                cpeAppend.Collapsed = true;
            }
        }

        protected void btnSaveDisclaimer_Click ( object sender, EventArgs e )
        {
            CustomDisclaimer disclaimer = new CustomDisclaimer( BusinessUnitID ) { IsAppend = rdoAppend.Checked };
            if ( rdoAppend.Checked )
            {
                disclaimer.CustomText = txtAppend.Text;
            }
            else if ( rdoReplace.Checked )
            {
                disclaimer.CustomText = txtReplace.Text;
            }
            disclaimer.Save();
        }

        protected void EditAutomaterMakeCredentials_Click ( object sender, EventArgs e )
        {
            LoadDealerSiteCredentials();
        }

        protected void SaveCredentials_Click ( object sender, EventArgs e )
        {

            // FB: 31286 - changes for concurrency checks and not to re-load the autoload queues if not update has been performed
            int rowsUpdated;

            try
            {
                var credSite = new CredentialedSiteRepository();
                var site = credSite.Fetch(Convert.ToInt32(hfSiteId.Value));
                
                // validate user input
                if(string.IsNullOrWhiteSpace(usernameTB.Text))
                    throw new ApplicationException(string.Format("{0} requires your user name to access vehicle information", site.SiteName));

                if (string.IsNullOrWhiteSpace(passwordTB.Text))
                    throw new ApplicationException(string.Format("{0} requires your password to access vehicle information", site.SiteName));

                if (string.IsNullOrWhiteSpace(dealercodeTB.Text) && site.DealerCodeRequired == 1)
                    throw new ApplicationException(string.Format("{0} requires your dealer code to access vehicle information", site.SiteName));

                // put into a try block to catch database, specially concurrency, errors and display them to the user
                var creds = new EncryptedSiteCredential(BusinessUnitID, (Agent)Convert.ToInt32(hfSiteId.Value), dealercodeTB.Text,
                                                        usernameTB.Text, passwordTB.Text, false);

                // set the row version from the hidden field on the page FB: 31286
                if (!String.IsNullOrWhiteSpace(hfRowVersion.Value))
                {
                    creds.CredentialVersion = BitConverter.GetBytes(Convert.ToUInt64(hfRowVersion.Value));
                }
                rowsUpdated = creds.Save();

            }
            catch (ApplicationException ex)
            {
                lblCredentialWarning.Text = ex.Message;
                return;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                lblCredentialWarning.Text = ex.Message;
                return;
            }
            

            // FB: 30552 - autoload credential lockout
            //var fileStore = FileStoreFactory.CreateFileStore();
            //FetchAgentCredentials.UnLock(fileStore, BusinessUnitID, EditingCredentialedSite);

            var settings = GetMiscSettings.GetSettings(BusinessUnitID);
            if (settings.HasSettings && rowsUpdated > 0) // only call if the row has been updated, removed batch autoload requiremnt
            {
                MessageSender.SendBatchAutoLoadInventory(new AutoLoadInventoryMessage(BusinessUnitID, AutoLoadInventoryType.Errors), GetType().FullName);
                MessageSender.SendBatchAutoLoadInventory(new AutoLoadInventoryMessage(BusinessUnitId, AutoLoadInventoryType.NoStatus), GetType().FullName);
            }

            // read the credentials to update the GUI and reset the new rowVersion from the table
            LoadDealerSiteCredentials();

            //CredentialLockedOutPanel.Visible = false;
            //passwordTB.Attributes.Add("value", passwordTB.Text);


        }

        protected void ClearCredentials_Click ( object sender, EventArgs e )
        {
            var credentialVersion = new byte[8];

            // set the row version from the hidden field on the page FB: 31286
             if (!String.IsNullOrWhiteSpace(hfRowVersion.Value))
                credentialVersion = BitConverter.GetBytes(Convert.ToUInt64(hfRowVersion.Value));

            try
            {
                EncryptedSiteCredential.Delete(BusinessUnitID, (Agent)Convert.ToInt32(hfSiteId.Value) , credentialVersion);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                lblCredentialWarning.Text = ex.Message;
                return;
            }
            
            EditSiteCredentialsPanel.Visible = false;
        }

        protected void FordDirectClearCredentials_Click(object sender, EventArgs e)
        {
            var subscription = new FordNewAutoLoadSubscription();
            subscription.Unsubscribe(BusinessUnitID, User.Identity.Name);
            PACodeTB.Text = string.Empty;
            DealerNameTB.Text = "ENTER A VALID PA CODE";
        }

        protected void FordDirectSaveCredentials_Click(object sender, EventArgs e)
        {
            var subscription = new FordNewAutoLoadSubscription();
            string name = subscription.Subscribe(BusinessUnitID, PACodeTB.Text, User.Identity.Name);
            DealerNameTB.Text = name.Length > 0 ? name : "INVALID PA CODE!!!";
        }

        protected void VerifyDealerPaCode_Click(object sender, EventArgs e)
        {
            var subscription = new FordNewAutoLoadSubscription();
            string name = subscription.GetDealerName(PACodeTB.Text);
            DealerNameTB.Text = name.Length > 0 ? name : "INVALID PA CODE!!!";
        }

        protected void LoadDealerSiteCredentials()
        {
            EncryptedSiteCredential creds = EncryptedSiteCredential.Fetch(BusinessUnitID, EditingCredentialedSite, true) ??
                                            new EncryptedSiteCredential(BusinessUnitID, EditingCredentialedSite, "", "", "", false);

            // Ford direct has it's own panel.
            if (AutomaterMakesDDL.SelectedValue == "6") // ford direct
            {
                FordDirectAutoLoadsPanel.Visible = true;
                EditSiteCredentialsPanel.Visible = false;

                var subscription = new FordNewAutoLoadSubscription();
                var dealerPaCode = subscription.GetDealerPaCode(BusinessUnitID);
                if (!string.IsNullOrWhiteSpace(dealerPaCode))
                {
                    PACodeTB.Text = dealerPaCode;
                    DealerNameTB.Text = subscription.GetDealerName(dealerPaCode);
                }
            }
            else
            {
                var credSite = new CredentialedSiteRepository();
                var site = credSite.Fetch(Convert.ToInt32(AutomaterMakesDDL.SelectedValue));
                
                
                FordDirectAutoLoadsPanel.Visible = false;
                EditSiteCredentialsPanel.Visible = true;

                dealercodeTB.Text = creds.DealerCode;

                // do we need dealer code?
                switch (site.DealerCodeRequired)
                {
                    case 0:
                        dealerCodePanel.Visible = false;
                        break;
                    case 1:
                        lbDealerCodeOptional.InnerText = "Dealer Code";
                        dealerCodePanel.Visible = true;
                        break;
                    case 2:
                        lbDealerCodeOptional.InnerText = "Dealer Code (Optional)";
                        dealerCodePanel.Visible = true;
                        break;
                }

                editingSiteLabel.Text = site.SiteName;
                editingSiteLabel2.Text = site.SiteName;

                CredentialLockedOutPanel.Visible = creds.IsLockedOut;

                usernameTB.Text = creds.UserName;
                passwordTB.Attributes.Add("value", creds.Password);

                // hide the row version on the page FB: 31286
                if (creds.CredentialVersion != null) // don't need a row version on an add
                {
                    var u64RowVersion = BitConverter.ToUInt64(creds.CredentialVersion, 0);
                    hfRowVersion.Value = u64RowVersion.ToString(CultureInfo.InvariantCulture);
                }
                else
                {
                    hfRowVersion.Value = null;
                }

                // hide the correct site id for round trip FB: 31382
                hfSiteId.Value = AutomaterMakesDDL.SelectedValue;

            }

        }
    }
}
