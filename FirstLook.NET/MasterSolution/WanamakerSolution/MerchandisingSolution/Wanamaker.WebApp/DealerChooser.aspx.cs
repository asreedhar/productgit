using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Oltp;

namespace Wanamaker.WebApp
{
    public partial class DealerChooser : BasePage
    {
        //
        // Support Methods
        //
        private static readonly string DealerGroupToken = SoftwareSystemComponentStateFacade.DealerGroupComponentToken;

        private static readonly string DealerToken = SoftwareSystemComponentStateFacade.DealerComponentToken;

        protected FirstLook.DomainModel.Oltp.SoftwareSystemComponentState SoftwareSystemComponentState
        {
            get { return (FirstLook.DomainModel.Oltp.SoftwareSystemComponentState)Context.Items[SoftwareSystemComponentStateFacade.HttpContextKey]; }
            set { Context.Items[SoftwareSystemComponentStateFacade.HttpContextKey] = value; }
        }

        protected void Page_Init( object sender, EventArgs e )
        {
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SoftwareSystemComponentState = LoadState(DealerGroupToken);
        }

        protected void DealerGroupFilter_TextChanged(object sender, EventArgs e)
        {
            DealerGroupGridView.SelectedIndex = -1;
            DealerFilter.Text = "";
            DealerFilter_TextChanged(sender, e);
        }

        protected void DealerGroupGridView_PageIndexChanged(object sender, EventArgs e)
        {
            DealerGroupFilter_TextChanged(sender, e);
        }

        protected void DealerFilter_TextChanged(object sender, EventArgs e)
        {
            DealerGridView.SelectedIndex = -1;


        }

        protected void DealerGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("EnterDealer"))
            {
                int selectedIndex = Int32Helper.ToInt32(e.CommandArgument);

                GridView grid = sender as GridView;

                if (grid != null)
                {
                    int dealerId = Int32Helper.ToInt32(grid.DataKeys[selectedIndex]["Id"]);

                    BusinessUnit dealer = BusinessUnitFinder.Instance().Find(dealerId);

                    if (dealer != null)
                    {


                        SetupSoftwareSystemComponentState(dealer);

                        SetDealerCookies(dealer);
                        Response.Redirect("~/Default.aspx?token=DEALER_SYSTEM_COMPONENT&NoWait=true", true);
                    }
                }
            }
        }

        private void SetDealerCookies(BusinessUnit dealer)
        {
            Response.Cookies.Add(new HttpCookie("dealerId", dealer.Id.ToString()));
            var dealerName = CleanCookieVal(dealer.Name);
            Response.Cookies.Add(new HttpCookie("ts_dealer" , dealerName));
            var dealerGroup = CleanCookieVal(dealer.DealerGroup().Name);
            Response.Cookies.Add(new HttpCookie("ts_dealergroup" , dealerGroup));
        }


        private string CleanCookieVal(string val)
        {
            return Regex.Replace(val, @"\s|\[|\]|\{|\}|\(|\)|\=|\,|\""|\?|\\|\/|@|:|;", "");
         
        }

      

        protected void DealerGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GridView view = sender as GridView;

                if (view != null)
                {
                    BusinessUnit dealer = e.Row.DataItem as BusinessUnit;

                    if (dealer != null)
                    {
                        //                    bool upgraded = dealer.HasDealerUpgrade(Upgrade.PingTwo);
                        e.Row.Cells[1].Enabled = true;

                    }
                }
            }
        }

        protected void DealerGridView_PageIndexChanged(object sender, EventArgs e)
        {
            DealerFilter_TextChanged(sender, e);
        }



        private void SetupSoftwareSystemComponentState(BusinessUnit dealer)
        {
            SoftwareSystemComponentState state = LoadState(DealerToken);
            state.DealerGroup.SetValue(dealer.DealerGroup());
            state.Dealer.SetValue(dealer);
            state.Member.SetValue(null);
            state.Save();
        }

        private SoftwareSystemComponentState LoadState(string componentToken)
        {
            SoftwareSystemComponentState state = SoftwareSystemComponentStateFacade.FindOrCreate(User.Identity.Name, componentToken);

            if (state == null)
            {
                Member member = MemberFinder.Instance().FindByUserName(User.Identity.Name);

                SoftwareSystemComponent component = SoftwareSystemComponentFinder.Instance().FindByToken(componentToken);

                state = new SoftwareSystemComponentState();
                state.AuthorizedMember.SetValue(member);
                state.SoftwareSystemComponent.SetValue(component);
            }

            return state;
        }
    }
}
