﻿//  ===========================================================================
//  This script is intended for use with the all MAX (merchandising) pages
//  ===========================================================================
//  Dependencies: 
//      - jQuery.1.4.3.min.js
//  ---------------------------------------------------------------------------

var MAXMain = function () { };

MAXMain.IsIE6 = function () {
    return navigator.appName === "Microsoft Internet Explorer" && navigator.appVersion.indexOf( "MSIE 6.0" ) > -1;
};

MAXMain.IsIE7 = function () {
    return navigator.appName === "Microsoft Internet Explorer" && navigator.appVersion.indexOf( "MSIE 7." ) > -1;
};

MAXMain.IsIE = function () {
    return navigator.appName === "Microsoft Internet Explorer";
};

MAXMain.prototype = {

    /// Initializes client-side page functionality
    onReady: function ( e ) {

        if ( MAXMain.IsIE6() ) {
            this.initializeIE6CssMenus();
        }
        this.initializeNewWindowLinks();
        this.initNavMenu();

    },

    initializeIE6CssMenus: function () {

        $( "#CustomizeMax" ).hover( function () { $( "#config" ).show(); }, function () { $( "#config" ).hide(); } );

    },

    /// Automatically sets up links with the 'newWindowLink' class to open in a new window
    /// (optionally populates and/or polls the child window)
    initializeNewWindowLinks: function () {

        $( ".newWindowLink" ).click( function ( e ) {

            var linkId = $( this ).attr( "id" ) === undefined ? "NEWWIN" : $( this ).attr( "id" ),
                linkHref = $( this ).attr( "href" ) === undefined || $( this ).attr( "href" ) === "" ? window.location : $( this ).attr( "href" ),
                newWin = window.open( linkHref, linkId.replace( "Link", "" ), $( this ).attr( "options" ) ),
                poller;

            // Handle child window polling (Replace 'if' with 'switch' to scale)
            if ( linkId === "priceLnk" ) {
                SetChildWindowPolling( function () { Page.maskWindow( "Loading ..." ); window.location.reload(); } );
            }

            // Child window polling helper
            function SetChildWindowPolling( callback ) {
                poller = window.setInterval( PollChildWindow, 500 );
                function PollChildWindow() {
                    if ( newWin.closed ) {
                        clearInterval( poller );
                        callback();
                    }
                }
            }

            e.preventDefault();
            e.stopPropagation();

        } );

    },

    initNavMenu: function () {

        // Setup menu headers to open menus on mouseover
        $( "li.menuHead" ).hover( function ( e ) { $( this ).find( "ul" ).show(); }, function ( e ) { $( this ).find( "ul" ).hide(); } );

        // Set z-index so drop-downs cover adjacent menu items
        $( "li.menuHead" ).each( function ( index ) {
            $( this ).css( "z-index", 9999 - index );
        } );

        if ( MAXMain.IsIE() ) {
            $.each( $( "#MainNavList li.menuHead" ), function () {

                var menu = $( this ).find( "ul:first" ),
                    menuWidth = $( menu ).width();

                $.each( $( menu ).find( "li a" ), function () {
                    $( this ).width( menuWidth );
                } );

            } );
        }

    },

    /// Removes empty elements and empty child elements to correct layout issues
    hideEmptyElements: function ( selector ) {
        if ( selector === undefined ) { return; }
        $( selector + ":empty" ).remove();
    }

};

var MAXPage;
$( document ).ready( function () {
    MAXPage = new MAXMain();
    MAXPage.onReady();
} );
