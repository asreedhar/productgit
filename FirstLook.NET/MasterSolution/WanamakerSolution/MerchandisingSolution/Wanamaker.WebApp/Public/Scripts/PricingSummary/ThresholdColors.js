﻿
$('#imgColor').hover(function () {

    $('#displayInfo').addClass('info-div');
    $('#displayInfo').addClass('callout-style');
    $('#displayInfo').removeClass('hide');
}
,
function () {

    $('#displayInfo').addClass('hide');
    $('#displayInfo').removeClass('info-div');
}
);

function ValidatePrimarySource() {

    var count = 0;
    var arr = new Array();
    arr[0] = $('#ddlKbb').val();
    arr[1] = $('#ddlNada').val();
    arr[2] = $('#ddlMarketAverage').val();
    arr[3] = $('#ddlMsrp').val();
    arr[4] = $('#ddlEdmunds').val();

    for (var i = 0; i < 5; i++) {
        if (arr[i] == 'Primary')
            count++;
    }
    if (count < 1) {

        $('#validationDiv').html('<div id="message">Please select atleast 1 Primary Source</div>');
        $('#validationDiv').dialog({
            modal: true,
            buttons: {
                "OK": function () {
                    $('.ui-dialog').hide();
                    $(".ui-widget-overlay").hide();

                }
            }

        });
        $('.ui-dialog').addClass('center-align');

        $('.ui-dialog-titlebar-close').unbind("click").click(function () {
            $('.ui-dialog').hide();
            $(".ui-widget-overlay").hide();


        });
    }
    else if (count > 1) {

        $('#validationDiv').html('<div id="msg">Please select only 1 Primary Source</div>');
        $('#validationDiv').dialog({
            modal: true,
            buttons: {
                "OK": function () {
                    $('.ui-dialog').hide();
                    $(".ui-widget-overlay").hide();

                }

            }
        });

        $('.ui-dialog-titlebar-close').unbind("click").click(function () {
            $('.ui-dialog').hide();
            $(".ui-widget-overlay").hide();




        });

    }
}


