﻿var makesArray = ["acura", "audi", "bmw", "ford", "honda", "infiniti", "kia", "mini", "scion", "volkswagen", "toyota"];
var businessUnitId = 0;
function InventoryGrid() {

    var self = this;
    self.CreateInventoryGrid = function CreateInventoryGrid(Data, BusinessUnitId) {
        businessUnitId = BusinessUnitId;
        $('#invGrid').jqGrid('clearGridData');
        $("#invGrid").jqGrid({
            datatype: 'local',
            data: Data,
            height: '100%',
            colNames: ["Inventory Id", "Trim", "StockNumber", "settingCount", "AgeBucket", "Age", "Vehicle Description", "Color", "Mileage", "  Unit<br>Cost", "Internet<br>Price", "% of<br>Market<br>Average", "Pricing", "Market<br>Days<br>Supply", "Price Comparison(s) Displaying", "ComparisonColor"],
            colModel: [
                 { name: 'invId', hidden: true, key: true },
                 { name: 'Trim', hidden: true, key: false },
                 { name: 'StockNumber', hidden: true, key: false },
                 { name: 'settingCount', hidden: true, key: false },
                 { name: 'ageBucket', hidden: true, key: false },
                 { name: 'age', index: 'age', align: "center", hidden: false, sortable: true, sorttype: "int", width: 35, height: 10, cellattr: function (rowId, cellValue, rawObject, cm, rdata) { return ' class="blue"'; } },
                 { name: 'vehicleDescription', sortable: true, hidden: false, width: 158, height: 10, formatter: DisplayTrim, unformat: function (cellValue, options, rowObject) { return $(rowObject).html(); }, cellattr: function (rowId, cellValue, rawObject, cm, rdata) { return ' class="breakwordclass greycolor"'; } },
                 { name: 'color', sortable: true, hidden: false, width: 40, height: 10, cellattr: function (rowId, cellValue, rawObject, cm, rdata) { return ' class="breakwordclass greycolor colordescription"'; } },
                 { name: 'mileage', sortable: true, sorttype: "int", hidden: false, width: 50, height: 10, formatter: function (cellValue, option, rowOpt) { return FormatNumber(cellValue); }, cellattr: function (rowId, cellValue, rawObject, cm, rdata) { return ' class="greycolor"'; } },
                 { name: 'unitcost', sortable: true, sorttype: "int", hidden: false, width: 55, height: 10, formatter: function (cellValue, option, rowOpt) { return FormatNumberAsCurrency(cellValue); }, cellattr: function (rowId, cellValue, rawObject, cm, rdata) { return ' class="greycolor"'; } },
                   { name: 'internetPrice', sortable: true, sorttype: "int", width: 78, editable: true, editrules: { required: true }, hidden: false, formatter: InternetPrice },

               { name: 'pMarketAverage', sortable: true, sorttype: "int", hidden: false, width: 53, height: 10, formatter: function (cellValue, option, rowOpt) {
                   if (cellValue == 0) {
                       return 'N/A';
                   } return cellValue + '%';
               }, cellattr: function (rowId, cellValue, rawObject, cm, rdata) { if ((parseInt(cellValue.replace("%", "")) >= parseInt($('#GreenValue1').val())) && (parseInt(cellValue.replace("%", "")) <= parseInt($('#GreenValue2').val()))) { return 'class="greenclass"'; } else { return 'class=redclass'; } }
               },
                 { name: 'pricing', sortable: false, width: 47, height: 10, editable: true, editrules: { required: true }, hidden: false, formatter: FormatUrl
                 },
                 { name: 'marketdayssupply', key: false, sortable: true, sorttype: "int", hidden: false, width: 45, height: 10 },
                 { name: 'PriceComparison', sortable: true, hidden: false, width: 326, height: 10, formatter: FormatPriceComparison, cellattr: GetBackgroudColor },
                 { name: 'ComparisonColor', hidden: true, key: false }
                ],
            rowNum: 5000,
            viewrecords: true,
            rowList: [10, 20, 30, 40],
            sortorder: 'asc',
            hoverrows: false,
            scroll: false,
            sortable: true,
            loadonce: true,
            subGrid: false,
            pager: '#InvGridPager',
            gridComplete: PriceComparisonSorthandler,
            viewsortcols: [true, 'vertical', true],
            gridview: true,
            loadError: function (xhr, st, err) {
                alert("Error Loading Grid ");
            }
        });
        jQuery('#invGrid').jqGrid('setGridParam', { data: Data, page: 1 });
        jQuery('#invGrid').trigger('reloadGrid');



        jQuery(".txt-internetprice").tooltip({
            position: {
                my: "center bottom",
                at: "center center-18",
                collision: "fit"
            },
            content: function () {
                $(this).attr('title', $('#callout').html());
                return $(this).attr('title');
            },
            tooltipClass: "arrow_box"
        });

        PriceComparisonSorthandler();

    };
};

function PriceComparisonSorthandler() {
    jQuery("li.width").each(function () {

        var wdth = jQuery(this).outerWidth();
        if (wdth > 157) {
            jQuery(this).attr("isLong", true);
        } else {
            jQuery(this).attr("isLong", false);
        }


    });

    jQuery("ul.unorderedlistclass").each(function () {

        //Iterate over children and see if islong

        convertPricingComparisonsIntoColumns(jQuery(this).children(), 0);
        
    });
}

function convertPricingComparisonsIntoColumns(arrayofElems, startingIndex) {

    if (startingIndex < arrayofElems.length) {
        var child1;
        var isLong1;
        if (startingIndex + 1 < arrayofElems.length) {
            child1 = arrayofElems[startingIndex];
            var child2 = arrayofElems[startingIndex + 1];
            isLong1 = jQuery(child1).attr("isLong");
            var isLong2 = jQuery(child2).attr("isLong");

            if (isLong1 == "false" & isLong2 == "false") {
                convertPricingComparisonsIntoColumns(arrayofElems, startingIndex + 2);
            } else if (isLong1 == "true" & isLong2 == "false") {
                jQuery(child1).removeClass("width");
                jQuery(child1).addClass("maxwidth");
                convertPricingComparisonsIntoColumns(arrayofElems, startingIndex + 1);
            } else if (isLong1 == "false" & isLong2 == "true") {
                jQuery(child1).removeClass("width");
                jQuery(child1).addClass("maxwidth");
                convertPricingComparisonsIntoColumns(arrayofElems, startingIndex + 1);
            } else if (isLong1 == "true" & isLong2 == "true") {
                jQuery(child1).removeClass("width");
                jQuery(child1).addClass("maxwidth");
                jQuery(child2).removeClass("width");
                jQuery(child2).addClass("maxwidth");
                convertPricingComparisonsIntoColumns(arrayofElems, startingIndex + 2);
            } else {
                return;
            }

        } else {

            if (startingIndex > 0) {
                var child0, isLong0;
                child0 = arrayofElems[startingIndex - 1];
                isLong0 = jQuery(child0).attr("isLong");
                child1 = arrayofElems[startingIndex];
                if (isLong0 == "true") {
                    jQuery(child1).removeClass("width");
                    jQuery(child1).addClass("maxwidth");
                } else {
                    var countSid = 0;
                    jQuery(arrayofElems).each(function(index,data) {
                            if (jQuery(data).hasClass("width")) {
                                countSid++;
                            };
                    });
                    
                    if (countSid % 2 != 0) {
                        jQuery(child1).removeClass("width");
                        jQuery(child1).addClass("maxwidth");
                    }
                }

            } else {
                child1 = arrayofElems[startingIndex];
                isLong1 = jQuery(child1).attr("isLong");
                if (isLong1 == "true") {
                    jQuery(child1).removeClass("width");
                    jQuery(child1).addClass("maxwidth");
                }
            }
        }
    }
};

function ToInt(value) {
    return parseInt(value.replace(/[^\d]*/g, ""), 10) || 0;
}

function IsNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}
function ParseMoney(moneyStr) {
    if (IsNumber(moneyStr))
        return parseInt(moneyStr);
    if (moneyStr == 'N/A')
        return 0;
    else
        return (
                    (moneyStr.indexOf("-") <= 1 && moneyStr.indexOf("-") >= 0) ||
                        (moneyStr.indexOf("(") == 0 && moneyStr.indexOf(")") == moneyStr.length - 1)) ?
                    -1 * ToInt(moneyStr) :
                   ToInt(moneyStr);
}

function DisplayTrim(cellValue, options, rowObject) {
    var displayText = '<div>';
    //debugger;
    var count = rowObject.TrimInfo.length;
    var trim;
    if (count == 1) {
        trim = "";  //rowObject.TrimInfo[0].Trim;//dont display any thing.//Taken care by vehicle description.
    } else {
        //debugger; 
        var singlechromeStyleId;
        var singletrim;
        trim = '<br />';
        trim += '<select  name="ddlSelectTrim" id="ddlSelectTrim' + options.rowId + '" class="ddl-select-trim selectList alert-trim" onChange="SaveTrim(' + options.rowId + ')"><option value="-1" > Select Trim</option>';
        $.each(rowObject.TrimInfo, function (index) {
            $.each(rowObject.TrimInfo[index], function (key, value) {
                //debugger;
                if (key == "ChromeStyleId") {
                    singlechromeStyleId = value;
                }
                if (key == "Trim") {
                    singletrim = value;
                }

            });
            trim += '<option value="' + singlechromeStyleId + '" > ' + singletrim + '</option>';
        });
        trim += '</select>';
    }

    displayText += cellValue + trim + '<br /><a class="blue stocknumber" id="' + options.rowId + '"  href= "/IMT/EStock.go?stockNumberOrVin=' + rowObject.StockNumber + '&isPopup=true" target="_blank" >Stock #' + rowObject.StockNumber + '</a></div>';
    return displayText;
}



function FormatPriceComparison(cellvalue, options, rowobject) {
    var string = '<div>';
    string += '<ul class="unorderedlistclass">';
    var make = rowobject.make;
    make = make.toString().toLowerCase();
    make = ($.inArray(make, makesArray) != -1 ? make : "");
    var isSingleElem = cellvalue.length == 1 ? true : false;
    $.each(cellvalue, function (key, value) {
        if (!isSingleElem)
            string += '<li class="width">';
        else
            string += '<li class="maxwidth">';
        var text;
        if (parseInt(value.Bookvalue) <= 0) {
            text = FormatNumberAsCurrency(Math.abs(value.Bookvalue)) + ' above ' + value.BookName;
        } else {
            text = FormatNumberAsCurrency(value.Bookvalue) + ' below ' + value.BookName;
        }
        string += '<span class="bookName ' + (value.BookName.toLowerCase() == 'msrp' ? make + "-" : "") + value.BookName.toLowerCase().replace(" ", "").replace(" ", "").replace(".", "") + '"><span class="inner-span">' + text + '</span></span>';
        string += '</li>';
    });
    string += '</ul>';
    string += '</div>';
    return string;
}

function InternetPrice(cellValue, option) {
    return '<table id="tickcross-table" cellspacing="0" cellpadding="0"> <tbody> <tr> <td colspan="2" style="vertical-align: middle;height:auto;"><input id="txt' + option.rowId + '" value="' + FormatNumberAsCurrency(cellValue) + '" type="text" onkeypress = "return isNumber(this,event)" class="txt-internetprice" onfocus="ChangeInternetPrice(' + option.rowId + ',' + cellValue + ')"></td></tr> <tr id="td' + option.rowId + '" class="show-hide"> <td class="align-top" align="right"><span class="span-cursor" id="a-cross' + option.rowId + '" onClick="Close(' + option.rowId + ',' + cellValue + ');"><img src="../Public/Images/redx.png" class="img-tick"></span></td><td class="align-top" align="left"><span class="span-cursor" id="a-tick' + option.rowId + '" onClick="SaveInterNetPrice(' + option.rowId + ');"><img src="../Public/Images/smgreencheck.png" class="img-cross"></span></td></tr> </tbody></table>';
}

function FormatUrl(cellvalue, options, rowobject) {
    return '<div class="pricing" onclick="OpenMAXPricingTool(' + rowobject.invId + ',' + businessUnitId + ',' + rowobject.AgeBucket + ')"></div>';

}

function OpenMAXPricingTool(invId, businessUnitId, ageBucket) {

    window.open('../PricingAnalysis/PingOne?inv=' + invId + '&businessUnitId=' + businessUnitId + '&ageBucket=' + ageBucket + '&filter=AllInventory&usedNew=2');
}



function GetBackgroudColor(cellvalue, options, rowobject) {
    if (rowobject.ComparisonColor == 1) {
        return 'class="green"';
    }
    else if (rowobject.ComparisonColor == 2)
        return 'class="yellow"';
    else
        return 'class="red"';
}

function isNumber(source, evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    var intprice = source.value.toString().replace(/\$/g, '').replace(/,/g, '').replace(/\(|\)/g, '');
    if (charCode != 44 && charCode != 188 && charCode != 36 && charCode != 8 && charCode != 0 && (charCode < 48 || charCode > 57) || intprice.length >= 6) {
        return false;
    }
    return true;
}

function FormatNumber(number) {
    return (number.toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ","));
}
function FormatNumberAsCurrency(number) {
    return '$' + FormatNumber(number);
}

function ChangeInternetPrice(inventoryId, newPrice) {
    $('.show-hide').hide();
    $('.arrow_box').hide();
    $('#td' + inventoryId).show();
}

function Close(inventoryId, oldValue) {
    $('#td' + inventoryId).hide();
    $('#txt' + inventoryId).val(FormatNumberAsCurrency(oldValue));
}


function SaveInterNetPrice(inventoryId, oldValue) {
    var url = $('#Index').val();
    $('#td' + inventoryId).hide();
    var internetPrice = ParseMoney( $('#txt' + inventoryId).val());
    if (internetPrice != "" && internetPrice!= undefined) {
        $.ajax({
            url: $('#SaveInertnetPriceUrl').val(),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({ internetPrice: internetPrice, inventoryId: inventoryId }),
            dataType: 'html',
            cache: false,
            success: function (data) {
                window.location = url + "?BusinessunitId=" + data;
            },
            error: function (jqXHR, textStatus, errorThrown) {
               
            }
        });
                            }
    else {
        $('#txt' + inventoryId).val(FormatNumberAsCurrency(oldValue));
    }
}


function SaveTrim(inventoryId) {
    $.ajax({
        url: $('#SaveTrimUrl').val(),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({ chromeStyleId: $('#ddlSelectTrim' + inventoryId).val(), inventoryId: inventoryId }),
        cache: false,
        success: function (data) {
            if (data) {
                $('#ddlSelectTrim' + inventoryId).hide();
                $('#ddlSelectTrim' + inventoryId).before($('#ddlSelectTrim' + inventoryId).find(":selected").text());
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    });

}





