﻿var Max = Max || {};
Max.PriceSummary = Max.PriceSummary || {};
Max.PriceSummary = $.extend({}, Max.PriceSummary, {
    RedGradient: {
        linearGradient: {
            x1: 0,
            y1: 0,
            x2: 1,
            y2: 0
        },
        stops: [
            [0.00, "#DB2122"],
            [0.50, "#D81E1F"],
            [1.00, "#AB1919"]
        ]
    },
    BlueGradient: {
        linearGradient: {
            x1: 0,
            y1: 0,
            x2: 1,
            y2: 0
        },
        stops: [
            [0.00, "#245BD9"],
            [0.50, "#2059DA"],
            [1.00, "#1243AC"]
        ]
    },
    ChartConfiguration: {
        chart: {
            type: 'column',
            marginLeft: 0,
            marginRight: 0,
            marginTop: 25,
            spacingBottom: 0
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },
        legend: {
            enabled: false
        },
        xAxis: {
            type: 'category',
            labels: {
                style: { 'fontWeight': 'bold', 'color': 'black', 'fontSize': '1.02em', 'lineHeight': '12' },
                padding: 0
            },
            minPadding: 0
        },
        yAxis: [
            {
                min: 0,
                tickInterval: 14.3,
                gridLineColor: '#EAEAEA',
                title: {
                    text: null
                },
                labels: {
                    enabled: false
                }
            }
        ],
        plotOptions: {
            series: {
                borderWidth: 1,
                borderColor: 'rgba(255,255,255,0.2)',
                dataLabels: {
                    enabled: true,
                    format: '<p style="text-align:center">{point.valueCount} units<br/>{point.y}%</p>',
                    crop: false,
                    overflow: 'none',
                    useHTML: true

                }
            },
            column: {
                colorByPoint: true
            }
        },
        tooltip: {
            enabled: false,
            pointFormat: 'Count: <b>{point.valueCount}</b>',
        },
        /*series: [
            {
                name: "none",
                data: dataSet
            }
        ],*/
        credits: {
            enabled: false
        }
    },
    RedMappings: ["overpricing risk", "no favorable<br>price comparisons", "due for repricing"],
    ClickFunction: function clickFunction() {
        /* expects series object context, invoke with $.proxy if necessary */
        var input = JSON.parse($('#PriceSummaryInput').val());
        if (input.Mode == "R") {
            $('#ColumnIndex').val(this.dataIndex + (+1));
        } else if (input.Mode == "A") {
            $('#ColumnIndex').val(this.dataIndex);
        }
        var updateSummary = new UpdateSummary();
        //Update only below table                 
        updateSummary.UpdateGrid();
    },
    Highchart: function PriceSummaryHighchart(jqSelector, yAxisValues, yAxisMaxValue, xAxisLabels) {
        if (!jqSelector || !yAxisValues || !yAxisMaxValue || !xAxisLabels) {
            if (console && console.log) {
                console.log("Max.PricingSummary.Highchart() missing required parameters.");
            }
            return null;
        }
        
        var self = this;
        var dataSet = [];
        for (var i = 0; i < yAxisValues.length; i++) {
            var computedValue = Math.round((yAxisValues[i] / yAxisMaxValue) * 100);
            dataSet.push({
                dataIndex: i,
                name: xAxisLabels[i],
                y: computedValue,
                colorByPoint: true,
                color: (self.RedMappings.indexOf(xAxisLabels[i].trim().toLowerCase()) > -1 ? self.RedGradient : self.BlueGradient),
                valueCount: yAxisValues[i],
                events: {
                    click: Max.PriceSummary.ClickFunction
                    }
            });
        }
        var newChart = $.extend({}, self.ChartConfiguration);
        newChart.series = [
            {
                name: "none",
                cursor: "pointer",
                data: dataSet
            }
        ];
        var highcharts = $(jqSelector).highcharts(newChart, function(chartObj) {
            /* do click bindings */
            $.each(chartObj.series, function(k, seriesObj) {
                $.each(seriesObj.data, function(k, dataObj) {
                    $(dataObj.dataLabel.div.lastChild).css({ "cursor": "pointer" });
                    $(dataObj.dataLabel.div.lastChild).on("click", function() { Max.PriceSummary.ClickFunction.call(dataObj) });
                });
            });
        });

        jQuery("g.highcharts-axis path").each(function(index, elem){
            jQuery(elem).attr("stroke-width",0);

        });

        return highcharts;
    }
});