﻿var Max = Max || {};
Max.PriceSummary = Max.PriceSummary || {};
Max.PriceSummary = $.extend({}, Max.PriceSummary, {
    RequestCache: {}
});

function ApplyGridUpdate(json) {
    //Only update Grid
    var grid = new InventoryGrid();
    grid.CreateInventoryGrid(json.ColModels.GridData);

    //Clear InventoryFilter
    $('#inventoryFilter').val("");

    //Table Header
    $('#lblRiskType').text(json.BucketName);

}
function UpdateSummary() {
    var self = this;

    self.Update = function Update() {        
        self.url = $('#PriceSummaryUrl').val();
        var input = new InputDto();
        input = JSON.parse($('#PriceSummaryInput').val());
        input.SalesStrategy = $('#showsSelectBox').val();
        input.Mode = $('#currentSelectBox').val();
        input.InventoryFilter = null;
        //set column index for first time
        if ($('#currentSelectBox').val() == "R") {
            $('#ColumnIndex').val(2);
        }
        else if ($('#currentSelectBox').val() == "A") {
            $('#ColumnIndex').val(0);
        }
        input.ColumnIndex = $('#ColumnIndex').val();

        $('#PriceSummaryInput').val(JSON.stringify(input));
        $('#duplPriceSummaryInput').val(JSON.stringify(input));
        $("#invBarChart").highcharts().showLoading();
        
        $.ajax({
            type: "POST",
            url: self.url,
            data: JSON.stringify(input),
            contentType: "application/json",
            dataType: "json",
            success: function (json) {

                //Refresh entire page
                $('#chart-container').show();                
                if ($('#currentSelectBox').val() == "N") {
                    $('#chart-container').hide();
                }
                var chart = Max.PriceSummary.Highchart("#invBarChart", json.FilterOption.YaxisValues, json.FilterOption.UnitsInStock, json.FilterOption.XaxisText);

                var grid = new InventoryGrid();
                grid.CreateInventoryGrid(json.ColModels.GridData);

                //Clear InventoryFilter
                $('#inventoryFilter').val("");

                //Table Header
                $('#lblRiskType').text(json.BucketName);
                

                $('#unitsInStock').text(json.FilterOption.UnitsInStock);
                $('#avgInternetPrice').text('$' + FormatNumber(json.FilterOption.AvgInternetPrice));
                $('#marketAvg').text(json.FilterOption.MarketAvg + '%');
                $('#marketDaysSupply').text(json.FilterOption.MarketDaysSupply);


            },
            error: function () {
                alert('Error:Update');
            }
        });
    };
    self.UpdateForAllInventory = function UpdateForAllInventory() {        
        self.url = $('#PriceSummaryUrl').val();
        var input = new InputDto();

        input = JSON.parse($('#PriceSummaryInput').val());
        input.SalesStrategy = $('#showsSelectBox').val();
        input.Mode = $('#currentSelectBox').val();
        input.InventoryFilter = null;
        input.ColumnIndex = $('#ColumnIndex').val();

        $('#PriceSummaryInput').val(JSON.stringify(input));
        $('#duplPriceSummaryInput').val(JSON.stringify(input));

        $.ajax({
            type: "POST",
            url: self.url,
            data: JSON.stringify(input),
            contentType: "application/json",
            dataType: "json",
            success: function (json) {

                //Hide barChart and Only update Grid
                $('#chart-container').hide();

                //Clear InventoryFilter
                $('#inventoryFilter').val("");

                //Table Header
                $('#lblRiskType').text(json.BucketName);

                var grid = new InventoryGrid();
                grid.CreateInventoryGrid(json.ColModels.GridData);

            },
            error: function () {
                alert('Error:UpdateForAllInventory');
            }
        });
    };
    //Search filter
    self.UpdateForInventoryFilter = function UpdateForInventoryFilter() {
        self.url = $('#PriceSummaryUrl').val();
        var input = new InputDto();
        
        input = JSON.parse($('#PriceSummaryInput').val());
        input.SalesStrategy = $('#showsSelectBox').val();
        input.Mode = $('#currentSelectBox').val();
        input.InventoryFilter = $('#inventoryFilter').val();
        input.ColumnIndex = $('#ColumnIndex').val();

        $('#PriceSummaryInput').val(JSON.stringify(input));
        $('#duplPriceSummaryInput').val(JSON.stringify(input));

        $.ajax({
            type: "POST",
            url: self.url,
            data: JSON.stringify(input),
            contentType: "application/json",
            dataType: "json",
            success: function (json) {
                
                //Only update Grid
                var grid = new InventoryGrid();
                grid.CreateInventoryGrid(json.ColModels.GridData);

                //Table Header
                $('#lblRiskType').text(json.BucketName);

            },
            error: function () {
                alert('Error: UpdateForInventoryFilter ');
            }
        });
    };
    //Column Click
    self.UpdateGrid = function UpdateGrid() {
        self.url = $('#PriceSummaryUrl').val();
        var input = new InputDto();        
        input = JSON.parse($('#PriceSummaryInput').val());
        input.SalesStrategy = $('#showsSelectBox').val();
        input.Mode = $('#currentSelectBox').val();
        input.InventoryFilter =null;
        input.ColumnIndex = $('#ColumnIndex').val();

        $('#PriceSummaryInput').val(JSON.stringify(input));
        $('#duplPriceSummaryInput').val(JSON.stringify(input));
        var request = JSON.stringify(input);
        if (request in Max.PriceSummary.RequestCache) {
            ApplyGridUpdate(Max.PriceSummary.RequestCache[request]);
        } else {
            $('#ExportExcel').fadeTo(225, 0.5);
            $.ajax({
                type: "POST",
                url: self.url,
                data: request,
                contentType: "application/json",
                dataType: "json",
                success: function (json) {
                    Max.PriceSummary.RequestCache[request] = json;
                    ApplyGridUpdate(json);
                },
                error: function () {
                    alert('Error:UpdateGrid ');
                }
            }).always(function () {
                $('#ExportExcel').fadeTo(100, 1);
            });
        }
    };
};
  