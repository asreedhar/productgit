﻿var Preferences = function () {};

Preferences.prototype = {

    onReady: function () {
        this.initDOM();
        this.initAlerts();
        this.initValidation();
    },

    initDOM: function () {
        this.$DOM = {
            alertTextboxes: $("#bucketAlerts .alertBuckets .col2 :text"),
            dashboardCheckBoxes: $("#bucketAlerts .alertBuckets .col1 input[type=checkbox]"),
            emailCheckBoxes: $("#bucketAlerts .alertBuckets .col3 input[type=checkbox]"),
            alertEmail: $("#bucketAlerts .email :text")
        };

    },

    initAlerts: function () {
        this.$DOM.emailCheckBoxes.click(function () {
            var checked = $(this).is(':checked');
            var selector = $(this).parents('div:first').find('span.col2').children('input[type=text]');

            if (checked) {
                selector.removeAttr('readonly');
                selector.removeClass('readonly');
            }
            else {
                selector.attr('readonly', 'readonly');
                selector.addClass('readonly');
            }
        });

        // initialize the correct state of the alert textboxes without actually simulating a user click
        this.$DOM.emailCheckBoxes.each(function () { $(this).triggerHandler("click"); });
    },

    initValidation: function () {
        $("form").submit($.proxy(function () {
            this.$DOM.alertEmail.removeClass("error");

            //get the texboxValues
            var textboxValues = [];
            this.$DOM.alertTextboxes.each(function () {
                textboxValues.push($(this));
            });

            for (var i = 0; i < textboxValues.length; i++) {
                if (isNaN(textboxValues[i].val())) {
                    textboxValues[i].addClass("error");
                    return false;
                }
            }
        }, this));


        this.$DOM.alertTextboxes.keyup(function () {
            var val = $(this).val();
            if (isNaN(val)) {
                $(this).addClass("error");
            }
            else {
                $(this).removeClass("error");
            }
        });
    }
};

$(document).ready(function () {
    Preferences.Page = new Preferences();
    Preferences.Page.onReady();
});