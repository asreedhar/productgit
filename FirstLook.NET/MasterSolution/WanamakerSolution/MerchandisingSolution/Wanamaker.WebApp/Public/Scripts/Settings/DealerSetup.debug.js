﻿
//  ===========================================================================
//  This script is intended for use with the CustomizeMAX/DealerSetup.aspx page
//  ===========================================================================
//  Dependencies:
//      - jQuery.1.4.3.min.js
//  ---------------------------------------------------------------------------

var DealerSetupPage = function () { };

DealerSetupPage.prototype = {

    InitPreviewLength: function () {

        $( "#setupWizard_CustomPreviewLen" ).unbind( "focus" ).bind( "focus", function () {

            $( "#setupWizard_previewCustom" ).attr( "checked", "checked" );

        } );

    },

    InitCredentialDisclaimer: function () {

        $( "#CollectionGV select" ).die( "change" ).live( "change", function () {

            var selectedVal = $("#" + this.id + " option:selected").text(); 

            if ( selectedVal == "" || selectedVal == null ) return;

            disclaimer = "I am an authorized " + selectedVal + " user with a valid username and password. I would like to automate the retrieval of my dealership’s online performance data daily from " + selectedVal + " for online inventory performance analysis.";

            $( this ).siblings( ".credentialDisclaimer" ).text( disclaimer );

        } );

    },

    /// Initializes client-side page functionality on page load
    onReady: function () {

        this.InitPreviewLength();
        this.InitCredentialDisclaimer();

    }

};

var Page;
$( document ).ready( function () {
    Page = new DealerSetupPage();
    Page.onReady();
} );
