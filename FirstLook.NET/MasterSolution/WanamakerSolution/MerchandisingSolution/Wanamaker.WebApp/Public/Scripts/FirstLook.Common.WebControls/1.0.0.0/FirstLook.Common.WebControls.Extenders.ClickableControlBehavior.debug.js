Type.registerNamespace('FirstLook.Common.WebControls.Extenders');

FirstLook.Common.WebControls.Extenders.ClickableControlBehavior = function(element) {
    FirstLook.Common.WebControls.Extenders.ClickableControlBehavior.initializeBase(this, [element]);
    this._buttonID = null;
    this._onclickDelegate = null;
};

FirstLook.Common.WebControls.Extenders.ClickableControlBehavior.prototype = {
    
    initialize : function() {
        FirstLook.Common.WebControls.Extenders.ClickableControlBehavior.callBaseMethod(this, 'initialize');
        this._onclickDelegate = Function.createDelegate(this, this._onclick);
    },

    updated: function() {
        $addHandler(this.get_element(), 'click', this._onclickDelegate);
        this.get_element().style.cursor = 'pointer';
        FirstLook.Common.WebControls.Extenders.ClickableControlBehavior.callBaseMethod(this, 'updated');
    },

    dispose : function() {
        if (this._onclickDelegate) {
            $removeHandler(this.get_element(), 'click', this._onclickDelegate);
            this._onclickDelegate = null;
        }
        FirstLook.Common.WebControls.Extenders.ClickableControlBehavior.callBaseMethod(this, 'dispose');
    },

    _onclick : function(e) {
        if (this._buttonID) {
            $get(this._buttonID).click();
        }
    },
    
    get_buttonID : function() {
        return this._buttonID;
    },
    set_buttonID : function(value) {
        this._buttonID = value;
    }
};

FirstLook.Common.WebControls.Extenders.ClickableControlBehavior.registerClass('FirstLook.Common.WebControls.Extenders.ClickableControlBehavior', Sys.UI.Behavior);

Sys.Application.notifyScriptLoaded();