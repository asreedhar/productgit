Type.registerNamespace('FirstLook.Common.WebControls.UI');
FirstLook.Common.WebControls.UI.Dialog = function(element) {
    FirstLook.Common.WebControls.UI.Dialog.initializeBase(this, [element]);
    this._ClientStateField = null;
    this._ButtonCollection = [];
    this._AllowCancelPostBack = false;
    this._Hidden = false;
    this._isIE6 = false /*@cc_on || @_jscript_version < 5.7 @*/;
};
FirstLook.Common.WebControls.UI.Dialog.prototype = {
    initialize: function() {
        FirstLook.Common.WebControls.UI.Dialog.callBaseMethod(this, 'initialize');        
    },
    updated: function() {
        if (this._clickDelegate == undefined) {
            this._clickDelegate = Function.createDelegate(this, this._clickHandler);
        }
        for (var i = 0; i < this._ButtonCollection.length; i++) {
        	var el = $get(this._ButtonCollection[i][0]);
            Sys.UI.DomEvent.addHandler($get(this._ButtonCollection[i][0]), 'click', this._clickDelegate);
        }
        if (this._isIE6) {
        	this._protectSelects();
        	this._fixIE6Buttons();
        };
    },
    dispose: function() {
        if (this._clickDelegate) {
            for (var i = 0; i < this._ButtonCollection.length; i++) {
                Sys.UI.DomEvent.removeHandler($get(this._ButtonCollection[i][0]), 'click', this._clickDelegate);
            };
            delete this._clickDelegate;
        }
        if (this._mouseoverDelegate) {
            Sys.UI.DomEvent.removeHandler(this._element, 'mouseover', this._mouseoverDelegate);
            delete this._mouseoverDelegate;
        }
        if (this._mouseoutDelegate) {
            Sys.UI.DomEvent.removeHandler(this._element, 'mouseout', this._mouseoutDelegate);        
            delete this._mouseoutDelegate;
        }
        FirstLook.Common.WebControls.UI.Dialog.callBaseMethod(this, 'dispose');
    },
    get_ButtonCollection: function() {
        return this._ButtonCollection;
    },
    set_ButtonCollection: function(value) {
        this._ButtonCollection = value;
    },
    get_Hidden: function() {
        return this._Hidden;
    },
    set_Hidden: function(value) {
        this._Hidden = value;
        this.set_clientState({
            'hidden': value
        });
    },
    get_AllowCancelPostBack: function() {
        return this._AllowCancelPostBack;
    },
    set_AllowCancelPostBack: function(value) {
        this._AllowCancelPostBack = value;
    },
    get_ClientStateField: function() {
        return this._ClientStateField;
    },
    set_ClientStateField: function(value) {
        this._ClientStateField = value;
    },
    show: function() {;
        this.get_element().style.display = '';
        this.set_Hidden(false);
    },
    hide: function() {;
        this.get_element().style.display = 'none';
        this.set_Hidden(true);
    },
    toggle: function() {
        if (this.get_Hidden()) {
            this.show();
        } else {
            this.hide();
        }
    },
    add_click: function(handler) {
        this.get_events().addHandler('click', handler);
    },
    remove_click: function(handler) {
        this.get_events().removeHandler('click', handler);
    },
    _clickHandler: function(event) {    
        var h = this.get_events().getHandler('click');
        if (h) {
            var args = new Sys.ButtonEventArgs();
            for (var i = 0; i < this._ButtonCollection.length; i++) {
                if (this._ButtonCollection[i][0] == event.target.id) {
                    args.set_command(this._ButtonCollection[i][1]);
                }
            };
            h(this, args);
            if (args.get_cancel()) {
                if (!this.get_AllowCancelPostBack()) {
                    this.hide();
                }
                event.preventDefault();
            }
        }
    },
    _protectSelects: function() { 
    	// ========================================================================
    	// = This form of insert is neccissary to avoid 404's and unsecure errors =
    	// ========================================================================
    	var html = '<iframe class="bgiframe"frameborder="0"tabindex="-1"src="javascript:false;"style="display:block;position:absolute;z-index:-1;filter:Alpha(Opacity=\'0\');top:0;left:0;bottom:0;right:0;"/>';    	
    	var bgiframe = document.createElement(html);	
    	this._element.insertBefore(bgiframe, this._element.firstChild);
    },
    _fixIE6Buttons: function() {
    	var domButtons = [];
    	for (var i=0, l = this._ButtonCollection.length; i < l; i++) {
    		domButtons.push($get(this._ButtonCollection[i][0]));
    	};
    	var _mouseoverHandler = function(e) {
    		for (var i=0, l=domButtons.length; i < l; i++) {
    			domButtons[i].disabled = true;
    		};
    		if (e.target && e.target.tagName.toUpperCase() == "BUTTON") {
    			e.target.disabled = false;
    		};    		
    	};
    	var _mouseoutHandler = function(e) {
    		for (var i=0, l=domButtons.length; i < l; i++) {
    			domButtons[i].disabled = true;
    		};
    	};
    	if (this._mouseoverDelegate == undefined) {
    		this._mouseoverDelegate = Function.createDelegate(this, _mouseoverHandler);
    	};
    	if (this._mouseoutDelegate == undefined) {
    		this._mouseoutDelegate = Function.createDelegate(this, _mouseoutHandler);
    	};
		Sys.UI.DomEvent.addHandler(this._element, 'mouseover', this._mouseoverDelegate);
		Sys.UI.DomEvent.addHandler(this._element, 'mouseout', this._mouseoutDelegate);
		_mouseoutHandler(new Sys.EventArgs());
    },
    get_clientState: function() {
        if (this._ClientStateField) {
            var input = this._ClientStateField;
            if (input) {
                if (input.value.length > 4) {
                    return eval('(' + input.value + ')');
                }
                return {};
            }
        }
        return null;
    },
    set_clientState: function(value) {
        if (this._ClientStateField) {
            var input = this._ClientStateField;
            if (input) {
                var state = this.get_clientState();
                for (var property in value) {
                    state[property] = value[property];
                }
                value = state;
                var results = [];
                for (var property in value) {
                    if (typeof(value[property]) != "undefined") {
                        results.push(property + ': ' + value[property]);
                    }
                }
                input.value = '{' + results.join(', ') + '}';
            }
        }
    }
};
FirstLook.Common.WebControls.UI.Dialog.registerClass('FirstLook.Common.WebControls.UI.Dialog', Sys.UI.Behavior);
FirstLook.Common.WebControls.UI.InputDialog = function(element) {
    FirstLook.Common.WebControls.UI.InputDialog.initializeBase(this, [element]);
    this._InputBox = null;
    this._CurrentText = "";
    this._MaxLength = null;
    this._Dialog = null;
};
FirstLook.Common.WebControls.UI.InputDialog.prototype = {
    initialize: function() {
        FirstLook.Common.WebControls.UI.InputDialog.callBaseMethod(this, 'initialize');
    },
    updated: function() {
        if (this._clickDelegate == undefined) {
            this._clickDelegate = Function.createDelegate(this, this._clickHandler);
        }
        this.get_Dialog().add_click(this._clickDelegate);
    },
    dispose: function() {
        if (this._clickDelegate) {
            this.get_Dialog().remove_click(this._clickHandler);
            delete this._clickDelegate;
        }
        FirstLook.Common.WebControls.UI.InputDialog.callBaseMethod(this, 'dispose');
    },
    get_InputBox: function() {
        return this._InputBox;
    },
    set_InputBox: function(value) {
        this._InputBox = value;
    },
    get_MaxLength: function() {
        var input = this.get_InputBox();
        if (input.tagName.toLowerCase() == 'input') {
            return this.get_InputBox().maxlength || null;
        } else {
            return this._MaxLength;
        }
    },
    set_MaxLength: function(value) {
        var input = this.get_InputBox();
        if (input.tagName.toLowerCase() == 'input') {
            this.get_InputBox().maxlength = value;
        } else {
            this._MaxLength = value;
        }
    },
    get_CurrentText: function() {
        return this._CurrentText || '';
    },
    set_CurrentText: function(value) {
        this._CurrentText = value;
    },
    get_Dialog: function() {
        return this._Dialog;
    },
    set_Dialog: function(value) {
        this._Dialog = value;
    },
    _ValidateLength: function() {
        if (this.get_MaxLength() != undefined) {
            if (this.get_InputBox().value.length > this.get_MaxLength()) {
                alert("Your input must be less then " + this.get_MaxLength() + " characters.");
                this.get_InputBox().focus();
                return false;
            };
        }
        return true;
    },
    _CancelTextChange: function() {
        this.get_InputBox().value = this.get_CurrentText();
    },
    _clickHandler: function(sender, eventArgs) {
        if (eventArgs.get_command() == FirstLook.Common.WebControls.UI.DialogButtonCommand.Ok) {
            if (this._ValidateLength() == false) {
                eventArgs.set_cancel(true);
            };
        } else if (eventArgs.get_command() == FirstLook.Common.WebControls.UI.DialogButtonCommand.Cancel) {
            this._CancelTextChange();
            eventArgs.set_cancel(true);
        }
    }
};
FirstLook.Common.WebControls.UI.InputDialog.registerClass('FirstLook.Common.WebControls.UI.InputDialog', Sys.UI.Behavior);
function Get_ClientState() {
    if (this._ClientStateField) {
        var input = this._ClientStateField;
        if (input) {
            if (input.value.length > 4) {
                return eval('(' + input.value + ')');
            }
            return {};
        }
    }
    return null;
};
function Set_ClientState(value) {
    if (this._ClientStateField) {
        var input = this._ClientStateField;
        if (input) {
            var state = this.get_clientState();
            for (var property in value) {
                state[property] = value[property];
            }
            value = state;
            var results = [];
            for (var property in value) {
                if (typeof(value[property]) != "undefined") {
                    results.push(property + ': ' + value[property]);
                }
            }
            input.value = '{' + results.join(', ') + '}';
        }
    }
};
FirstLook.Common.WebControls.UI.DialogButtonCommand = function() {};
FirstLook.Common.WebControls.UI.DialogButtonCommand.prototype = {
    NoButton: 0,
    Ok: 1,
    Cancel: 2,
    Yes: 3,
    No: 4,
    Abort: 5,
    Retry: 6,
    Ignore: 7,
    Escape: 8
};
FirstLook.Common.WebControls.UI.DialogButtonCommand.registerEnum("FirstLook.Common.WebControls.UI.DialogButtonCommand");
Sys.ButtonEventArgs = function Sys$ButtonEventArgs() {
    if (arguments.length !== 0) {
        throw Error.parameterCount();
    };
    Sys.ButtonEventArgs.initializeBase(this);
    this._command = FirstLook.Common.WebControls.UI.DialogButtonCommand.NoButton;
};
function Sys$ButtonEventArgs$get_command() {
    if (arguments.length !== 0) throw Error.parameterCount();
    return this._command;
}
function Sys$ButtonEventArgs$set_command(value) {
    var e = Function._validateParams(arguments, [{
        name: "value",
        type: FirstLook.Common.WebControls.UI.DialogButtonCommand
    }]);
    if (e) throw e;
    this._command = value;
}
Sys.ButtonEventArgs.prototype = {
    get_command: Sys$ButtonEventArgs$get_command,
    set_command: Sys$ButtonEventArgs$set_command
};
Sys.ButtonEventArgs.registerClass('Sys.ButtonEventArgs', Sys.CancelEventArgs);
Sys.Application.notifyScriptLoaded();