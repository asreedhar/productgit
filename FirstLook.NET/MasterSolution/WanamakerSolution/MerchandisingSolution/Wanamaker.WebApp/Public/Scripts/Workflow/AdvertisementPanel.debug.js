﻿//  ===========================================================================
//  This script is intended for use with the ApprovalSummary.aspx page
//  ===========================================================================
//  Dependencies:
//      - jQuery.1.4.3.min.js
//      - jquery-ui-1.8.5.min.js
//      - jquery.tmpl.js
//      - jquery.datalink.js  NOTE: NOT CURRENTLY USING THIS
//      - json2.js
//  ---------------------------------------------------------------------------

var AdvertisementPanel = function () { };

AdvertisementPanel.prototype = {

    onReady: function () {

        // Set properties
        this.ModelHiddenField = $( "#AdModelHidden" )[0];
        this.Advertisement = JSON.parse( $( this.ModelHiddenField ).val().trim() );
        this.Panel = $( "#AdTemplate" )[0];
        this.Modules = $( this.Panel ).find( "div.module" );

        // Initialize UI
        this.BindTemplate();
        this.InitModuleEditing();
        this.InitPreviewHighlighter();

    },

    /// Properties

    Advertisement: {},
    IsDirty: false,
    ModelHiddenField: null,
    Modules: null,
    Panel: null,

    /// Functions

    BindTemplate: function () {

        $( this.Panel ).html( $( "#ModuleTemplate" ).tmpl( this.Advertisement ) );

    },

    CancelModuleEdit: function ( module ) {

        if ( !module ) { return; }

        // Reset model & rebind template
        this.Advertisement = JSON.parse( $( this.ModelHiddenField ).val().trim() );
        this.BindTemplate();

    },

    HighlightPreview: function ( count ) {

        var blurbText,
            charCount = 0,
            module = $( "div.module.adPreview" ),
            remainder = 0,
            remainderSpan = $( "<span />" );

        $( module ).find( "span" ).removeClass( "highlight" );

        $.each( $( module ).find( "span.blurb" ), function ( index, value ) {

            blurbText = $( this ).text();

            if ( blurbText.length + charCount < count ) {

                // We're still within the character limit,
                // so just add highlight class.
                $( this ).addClass( "highlight" );

            } else {

                if ( charCount < count ) {

                    // This span broke the limit,
                    // so calculate count and add highlight span.

                    remainder = count - charCount;

                    // Setup child highlight span
                    $( remainderSpan ).addClass( "highlight" ).html( blurbText.substr( 0, remainder ) );

                    // Add highlight span to this blurb
                    $( this ).html( 
                        $( remainderSpan ).outerHtml() +
                        blurbText.substring( remainder, blurbText.length )
                    );

                } else {

                    // We have highlighted the preview ( break )

                    return false;

                }

            }

            charCount = charCount + blurbText.length;

        } );

    },

    InitModuleEditing: function () {

        var doneBtnSelector, moduleTextboxes;

        this.InitBlurbLocking();
        //this.InitModuleHover();

        // Setup module edit links
        $( ".editModuleLink" ).die( "click" ).live( "click", { instance: this }, function ( e ) {

            var instance = e.data.instance;

            instance.ShowEditModuleForm( $( this ).parents( ".module" )[0] );

            e.preventDefault();
            e.stopPropagation();

        } );

        // Setup module delete links
        $( "#AdTemplate .deleteModuleLink" ).die( "click" ).live( "click", { instance: this }, function ( e ) {

            var instance = e.data.instance,
                indexToDelete = $( this ).parents( ".module:first" ).index();

            if ( confirm( "Are you sure you want to remove this section from the ad?" ) ) {

                instance.Advertisement.Modules.remove( indexToDelete );

                // Update JSON hidden text field
                instance.SaveModel();

                // Mark as dirty
                instance.IsDirty = true;

                // Rebind template
                instance.BindTemplate();

            }

            e.preventDefault();
            e.stopPropagation();

        } );

        // Setup edit cancel buttons
        $( "#AdTemplate .editMode input.button.cancel" ).die( "click" ).live( "click", { instance: this }, function ( e ) {

            var instance = e.data.instance;

            instance.CancelModuleEdit( $( this ).parents( ".module" )[0] );

            e.preventDefault();
            e.stopPropagation();

        } );

        // Setup edit done buttons
        doneBtnSelector = MAXUI.IsIE() ? ".editMode input.button.done[ disabled!=true ]" : ".editMode input.button.done";
        $( doneBtnSelector ).die( "click" ).live( "click", { instance: this }, function ( e ) {

            var instance = e.data.instance;

            instance.SaveModuleEdit( $( this ).parents( ".module" )[0] );

            e.preventDefault();
            e.stopPropagation();

        } );

        // Setup preview character length warning/functionality
        this.SetPreviewMaxLength();

        // Setup 'Add Section' link
        $( "#AddModuleLink" ).unbind( "click" ).bind( "click", { instance: this }, this.ShowAddModuleForm );

        // Update JSON hidden text field on 'Approve Ad' click
        $( "#approveButton" ).unbind( "click" ).bind( "click", { instance: this }, function ( e ) {
            var instance = e.data.instance;
            instance.IsDirty = false;
            instance.SaveModel();
        } );

    },

    InitBlurbLocking: function () {

        var advertisementDiv = $( "#Advertisement" );

        $( "a.lock, a.unlock", advertisementDiv ).die( "click" ).live( "click", { instance: this }, function ( e ) {

            var instance = e.data.instance,
                moduleIndex = $( this ).attr( "module" ),
                blurbIndex = $( this ).attr( "blurb" ),
                isHeader = blurbIndex === "header";

            if ( $( this ).hasClass( "lock" ) ) {

                $( this ).removeClass( "lock" ).addClass( "unlock" ).prev( "input" ).addClass( "locked" );

                if ( isHeader ) {
                    instance.Advertisement.Modules[moduleIndex].Header.Edited = "true";
                } else {
                    instance.Advertisement.Modules[moduleIndex].Paragraph[blurbIndex].Edited = "true";
                }

            } else {

                $( this ).removeClass( "unlock" ).addClass( "lock" ).prev( "input" ).removeClass( "locked" );

                if ( isHeader ) {
                    instance.Advertisement.Modules[moduleIndex].Header.Edited = "false";
                } else {
                    instance.Advertisement.Modules[moduleIndex].Paragraph[blurbIndex].Edited = "false";
                }

            }

            e.preventDefault();
            e.stopPropagation();

        } );

        // Setup blurb and header textboxes to lock when edited
        //$( ".module input[type=text]", advertisementDiv ).die( "keyup" ).live( "keyup", { instance: this }, this.LockEditedBlurb )
        //                                                    .die( "click" ).live( "click", { instance: this }, this.LockEditedBlurb );

    },

    LockEditedBlurb: function ( e ) {

        var element = e.target || e.srcElement,
            blurb = $( element ).attr( "blurb" ),
            hasChanged = false,
            isHeader = $( element ).hasClass( "header" ),
            module = parseInt( $( element ).attr( "module" ), 10 );

        if ( isHeader ) {

            if ( this.Advertisement.Modules[module].Header.Text.trim() !== $( element ).val().trim() ) {
                hasChanged = true;
            }

        } else {

            blurb = parseInt( blurb, 10 );
            if ( this.Advertisement.Modules[module].Paragraph[blurb].Text.trim() !== $( element ).val().trim() ) {
                hasChanged = true;
            }
        }

        if ( hasChanged ) {

            this.IsDirty = true;
            $( element ).next( "a.lock" ).click();

        }
    },

    InitModuleHover: function () {

        $( this.Modules ).die( "mouseover mouseout" ).live( "mouseover mouseout", function ( e ) {

            if ( $( this ).hasClass( "editMode" ) ) { return; }

            if ( e.type === "mouseover" ) {
                $( this ).css( "border", "dashed #ccc 1px" );
                $( this ).find( "a.editModuleLink, a.deleteModuleLink" ).css( "visibility", "visible" );
            } else {
                $( this ).css( "border", $( this ).hasClass( "adPreview" ) ? "dotted #A4C6CF 1px" : "solid #fff 1px" );
                $( this ).find( "a.editModuleLink, a.deleteModuleLink" ).css( "visibility", "hidden" );
            }

        } );

    },

    InitPreviewHighlighter: function () {

        $( "#AdPreviewHighlighter input[type=radio]" ).unbind( "click" ).bind( "click", { instance: this }, function ( e ) {

            var count = parseInt( $( this ).val(), 10 ),
                instance = e.data.instance;

            instance.HighlightPreview( count );

        } );

    },

    MonitorPreviewLength: function ( e ) {

        var element = e.target || e.srcElement,
            length = 0,
            limit = parseInt( $( "#PreviewLengthLimit" ).val(), 10 ),
            preview = "",
            module = $( element ).parents( "div.module:first" ),
            cancelButton = $( module ).find( "div.buttons input.cancel" ),
            doneButton = $( module ).find( "div.buttons input.done" ),
            warningDiv = $( "#PreviewLengthWarning" );

        // Get current length
        $( "input.text", module ).each( function () {
            var textboxValue = $( this ).val();

            //take the spaces into consideration while calculating the length
            var blurbText = textboxValue.trim() == "" ? textboxValue.trim() : textboxValue.trim() + " ";

            preview += blurbText;
        } );

        length = preview.length;

        $( "#CharCounter" ).html( length.toString() );

        if ( length > limit ) {
            $( doneButton ).attr( "disabled", "disabled" );
            $( warningDiv ).html( "<p>Your preview has exceeded the character limit (" + limit + ") set in your preferences. Please correct this before clicking 'Done.'</p>" );
        } else {
            $( warningDiv ).html( "" );
            $( doneButton ).removeAttr( "disabled" );
        }

    },

    SaveModel: function () {

        // Save current model JSON to hidden field
        $( this.ModelHiddenField ).val( JSON.stringify( this.Advertisement ) );

    },

    SaveModuleEdit: function ( module ) {

        var blurbSpans = $( module ).find( "span.blurb" ),
            blurbTextboxes = $( module ).find( "div.blurbEditor input[type=text]" ),
            headerTextbox = $( module ).find( "input[type=text].header" ),
            moduleIndex = $( module ).index(),
            i, modelIndex = 0, modelText, uiText, modelHeaderText, textboxValue, uiHeaderText;

        // Check for invalid values
        if ( moduleIndex === -1 ) { return; }

        // Update blurbs
        for ( i = 0; i < blurbSpans.length; i++ ) {

            textboxValue = $( blurbTextboxes[i] ).val();

            //don't add spaces to blank blurbs FB 18531
            var blurbText = textboxValue.trim() == "" ? textboxValue.trim() : textboxValue.trim() + " ";

            $( blurbSpans[i] ).text( blurbText );

            // Update model
            modelText = this.Advertisement.Modules[moduleIndex].Paragraph[modelIndex].Text.trim();
            uiText = textboxValue.trim();

            if ( modelText !== uiText ) {
                this.Advertisement.Modules[moduleIndex].Paragraph[modelIndex].Text = blurbText;
            }

            modelIndex++;


        }

        // Update header ( & model )
        if ( headerTextbox.length > 0 ) {

            modelHeaderText = this.Advertisement.Modules[moduleIndex].Header.Text.trim();
            uiHeaderText = $( module ).find( "input[type=text].header" ).val().trim();

            if ( modelHeaderText !== uiHeaderText ) {
                $( module ).find( "span.header" ).text( uiHeaderText );
                $( module ).find( "input[type=text]" ).change();
                this.Advertisement.Modules[moduleIndex].Header.Text = uiHeaderText.trim();
                this.Advertisement.Modules[moduleIndex].Header.Edited = "true";
            }

        }

        $( module ).removeClass( "editMode" );

        // Hide/show relevant items
        $( module ).find( "div.buttons input" ).hide();
        $( module ).find( "a.lock, a.unlock" ).hide();
        $( module ).find( ".blurbEditor" ).hide();
        if ( !$( module ).hasClass( "adPreview" ) ) { $( module ).find( "input.text.header" ).hide(); $( module ).find( "span.header" ).show(); }
        $( module ).find( "span.blurb" ).css( "display", "inline" );

        // Update JSON hidden text field
        this.SaveModel();

    },

    SetPreviewMaxLength: function () {

        var previewLength = $( "#PreviewLengthLimit" ).val();

        if ( parseInt( previewLength, 10 ).toString() !== "NaN" ) {
            $( "#AdTemplate div.adPreview input:text" ).attr( "maxlength", parseInt( previewLength, 10 ) - 1 );
        } else {
            $( "#AdTemplate div.adPreview input:text" ).attr( "maxlength", "249" );
        }

    },

    ShowEditModuleForm: function ( module ) {

        // Cancel any open module edit forms
        this.CancelModuleEdit( $( ".module.editMode" )[0] );

        $( module ).addClass( "editMode" );

        // Hide/show relevant items
        $( module ).find( "span.blurb" ).hide();
        if ( !$( module ).hasClass( "adPreview" ) ) { $( module ).find( "span.header" ).hide(); $( module ).find( "input.text.header" ).show(); }
        $( module ).find( ".blurbEditor, a.lock, a.unlock, div.buttons, div.buttons input" ).show();

        $( module ).removeClass( "editMode" ).addClass( "editMode" );

    },

    ShowAddModuleForm: function ( e ) {

        var instance = e.data.instance, newModule;

        // Cancel any open module edit forms
        instance.CancelModuleEdit( $( ".module.editMode" )[0] );

        // IE6 hack
        $( "select" ).css( "visibility", "hidden" );

        $( "#AddModuleFormDiv" ).dialog( { buttons: {

            "+ Add": function () {

                // IE6 hack
                $( "select" ).css( "visibility", "visible" );

                var headerText = $( this ).find( "input[type=text]" ).val(),
                    paragraphText = $( this ).find( "textarea" ).val();

                // Handle 'Add' button click with nothing entered
                if ( headerText === "" && paragraphText === "" ) {
                    if ( confirm( "There is no text in the header or text fields.\r\n\r\nNothing will be added.\r\n\r\n" ) ) {
                        $( this ).dialog( "close" );
                    }
                    return;
                }

                // Add new section
                newModule = { "Type": "Module",
                    "Header": { "Type": "Item", "Edited": "true", "Text": headerText },
                    "Paragraph": [{ "Type": "Item", "Edited": "true", "Text": paragraphText}]
                };
                instance.Advertisement.Modules.push( newModule );

                // Update JSON hidden text field
                instance.SaveModel();

                // Mark as dirty
                instance.IsDirty = true;

                // Rebind template
                instance.BindTemplate();

                $( this ).dialog( "close" );

            },

            "Cancel": function () {

                $( this ).find( "input[type=text]" ).val( "" );
                $( "select" ).css( "visibility", "visible" );
                $( this ).dialog( "close" );

            }

        }, height: 420, modal: true, title: "Add Section", width: 737

        } );

    }

};

var AdPanel;
$( document ).ready( function () {
    AdPanel = new AdvertisementPanel();
    AdPanel.onReady();
} );