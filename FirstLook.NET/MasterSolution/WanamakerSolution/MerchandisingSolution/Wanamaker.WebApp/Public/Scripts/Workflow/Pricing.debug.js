$(function () {

    var ctricon = $('.ctr-graph-link'),
        ctrChart;

    $("#ie7upgrade").hide(); // don't show the IE7 upgrade warning, since this page forces IE7 rendering engine

    ctricon.appendTo("#InventoryAndCompleted");
    ctrChart = new CTRChart();

    $("#ctr-graph").dialog({
        autoOpen: false,
        width: 840,
        modal: true,
        close: function () {}
    });

    CTRChart.prototype.LoadChart = function (args) {
        var _self = this;
        // args should contain vehicle info needed to request ctr data (buid, invid), optional: loading div
        $.ajax({
            url: "/merchandising/InventoryMVC.aspx/CtrPrice/"+args.buid+"/"+args.invId,
            dataType: "text",
            cache: false,
            success: function(data) {
                data = JSON.parse(data);
                if(_.isEmpty(data.Sources)) {
                    args.loading.html("<div class='error'>We're sorry, but there is not enough data for this vehicle.</div>");
                } else {
                    args.loading.hide();
                    _self.VehicleData = data;
                    _self.Init();
                    if(args.onReady) { args.onReady(data); }
                }
            },
            error: function() {
                args.loading.html("<div class='error'>We're sorry, but we can't get data for that vehicle right now.</div>");
            }
        });
    };

    
    $(".ctr-graph-link").on("click", function (event) {
        var myargs = {
            invId: $(this).attr("data-inventoryId"),
            ymmt: $(this).attr("data-ymmt"),
            buid: $(this).attr("data-businessunitid"),
            stockNum: $(this).attr("data-stockNum"),
            loading: $("#ctr-graph .loading"),
            onReady: function (data) {
                $( "#ctr-graph .carinfo" ).html( $( "#ctrGraphHeader" ).tmpl( data ) );
            }
        };

        myargs.loading.html('');
        myargs.loading.show();
        $("#ctr-graph").dialog('option','title',myargs.ymmt+" ("+myargs.stockNum+")");
        $("#ctr-graph").dialog('open');
        ctrChart.LoadChart(myargs);
        
    });

});