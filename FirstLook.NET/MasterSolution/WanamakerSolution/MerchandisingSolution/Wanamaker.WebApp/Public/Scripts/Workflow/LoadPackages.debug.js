﻿//  ===========================================================================
//  This script is intended for use with the LoadPackages.aspx page
//  ===========================================================================
//  Dependencies:
//      - jQuery.1.4.3.min.js
//  ---------------------------------------------------------------------------

var MaxAdOptionsPage = function () { };

MaxAdOptionsPage.prototype = {

    initalizeTabLinks: function () {

        $( ".tabLink" ).die( "click" ).live( "click", function () {

            var clickedLink = $( this );

            // If selected tab is open, do nothing
            if ( clickedLink.hasClass( "selected" ) ) { return; }

            // If Approval page, set selected tab
            if ( typeof ApprovalSummary !== "undefined" ) {
                ApprovalSummary.OptionsTab = $( this ).attr( "id" );
            }

            $( "#LoadPackagesTabs *" ).removeClass( "selected" );
            clickedLink.addClass( "selected" );

            switch ( clickedLink.attr( "id" ) ) {

                case "HighlightTab":
                    $( "#StandardEquipment" ).hide();
                    $( "#HighlightPackages" ).show();
                    break;

                case "StandardTab":
                    $( "#HighlightPackages" ).hide();
                    $( "#StandardEquipment" ).show();
                    break;

            }

        } );

    },

    /// Initializes client-side page functionality on page load
    onReady: function () {

        this.initalizeTabLinks();

    }

};

var OptionsPage;
$( document ).ready( function () {
    OptionsPage = new MaxAdOptionsPage();
    OptionsPage.onReady();
} );
