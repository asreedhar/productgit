﻿//  ===========================================================================
//  This script is intended for use with the ApprovalSummary.aspx page
//  ===========================================================================
//  Dependencies:
//      - jQuery.1.4.3.min.js
//      - jquery-ui-1.8.5.min.js
//      - MAX.Main.debug.js
//  ---------------------------------------------------------------------------

var ApprovalSummary = function () { };
debugger;
ApprovalSummary.IsVehicleDirty = false;
ApprovalSummary.TrimSelectPostback = function () { __doPostBack( "TrimStyleDropDown", "" ); };

ApprovalSummary.ModalWindows = { Options: "OptionPackages", AdChoice: "ChoicePanel", AdApproval: "ApprovalDialog", AutoLoad: "AutoLoad" };
ApprovalSummary.PageRequestManager = Sys.WebForms.PageRequestManager.getInstance();

ApprovalSummary.OptionsTab = "HighlightTab";
ApprovalSummary.CurrentModal = null;

ApprovalSummary.CurrentBookValue = "";
ApprovalSummary.SecondaryBookValue = "";
ApprovalSummary.EdmundsBookValue = "";

ApprovalSummary.OnEndRequest = function ( sender, args ) {

    // If Packages lightbox
    if ( ApprovalSummary.CurrentModal === ApprovalSummary.ModalWindows.Options ) {

        // Reset modal window
        var modal = $( "#OptionsModalUpdatePanel:first" ),
            content = $( modal ).find( "#Content" ),
            packageContent = $( modal ).find( "#PackageContent" );

        $( modal ).removeClass( "loading" );
        $( modal ).parents( ".ui-dialog" ).find( ".ui-dialog-titlebar-close" ).css( "visibility", "visible" );
        $( content ).css( "background-color", "#fff" );
        $( packageContent ).css( "display", "block" );

        if ( $( sender._activeElement ).attr( "id" ) !== "" ) {
            Page.HighlightRegenerate();
        }

    } else {
        // Other UpdatePanel postbacks
        if ( $( "#AdAndVehicleInSyncHidden" ).val().toLowerCase() === "false" ) {
            Page.HighlightRegenerate();
        }
    }

};

ApprovalSummary.prototype = {

    /// Initializes client-side page functionality
    OnReady: function () {

        var modalsSelector, panelPosition, panelPosition2;

        this.InitTrimSelect();
        this.InitIsDirtyAlert();
        this.InitAddPackagesModal();
        this.InitPackagesModal();
        this.InitAutoLoad();
        this.InitPriceCheck();
        this.InitPackageDelete();
        this.InitAccordions();
        this.InitBookValue();
        this.InitVHR();
        this.InitAdChoice();
        this.InitAdApproval();
        this.InitLastAdLink();
        this.InitSecondaryBookValue();
        this.InitEdmundsBookValue();
        this.InitKbbConsumer();
        ApprovalSummary.PageRequestManager.add_endRequest(ApprovalSummary.OnEndRequest);

        if ($("#AdAndVehicleInSyncHidden").val().toLowerCase() === "false") this.HighlightRegenerate();

        if ($("#ProgramIsPersisted").length > 0 && $("#ProgramIsPersisted").val() != undefined && $("#ProgramIsPersisted").val().toLowerCase() !== 'true') Page.HighlightRegenerate();


        // Store original select value because IE sux
        $("#TrimStyleDropDown").data("TrimSelect", { initialValue: $("#TrimStyleDropDown").val() });

        // Store book value
        ApprovalSummary.CurrentBookValue = $("#BookValueSpan").text();
        ApprovalSummary.SecondaryBookValue = $("#BookSecondaryValueSpan").text();
        ApprovalSummary.EdmundsBookValue = $("#BookTmvValueSpan").text();
    },

    IsMAX30: function () {

        return $("#IsMAX30Hidden").val();

    },

    CheckInternetPriceDelta: function () {

        var price = parseInt($("#NewCarPricingInfoPanel #NewCarListPrice").val().replace(/[^\d\.]/gi, ""), 10),
            orignalPrice = parseInt($("#LastListPriceHidden").val().replace(/[^\d\.]/gi, ""), 10),
            dialogDiv = document.createElement("div"),
            warningTextSnippet;

        if ((price > orignalPrice * 1.20) || (price < orignalPrice * 0.80)) {

            if (price > orignalPrice * 1.20) {
                warningTextSnippet = 'more than 120% of';
            }
            else {
                warningTextSnippet = 'less than 80% of';
            }

            dialogDiv.setAttribute("id", "OfferPriceWarningDialog");
            dialogDiv.innerHTML = "WARNING: New List Price <strong><em>$" + price +
                                 "</em></strong> is <strong><em>" + warningTextSnippet + "</em></strong> the original List Price of <strong><em>$" +
                                 orignalPrice + "</em>.</strong>";

            $("body").append(dialogDiv);

            $("#OfferPriceWarningDialog").dialog(
            {
                closeText: "",
                dialogClass: "warningDialog",
                title: "Large Price Change Warning",
                modal: true,
                buttons: {
                    "Ok": function () {
                        $(this).dialog("close");
                        $("#NewCarPricingInfoPanel :submit").hide();
                        $("#PricesSaving").show();
                        $("#OfferPriceWarningDialog").remove();
                        var name = $("#NewCarPricingInfoPanel :submit").attr("name");
                        WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(name, "", true, "", "", false, true));
                    },
                    "Cancel": function () {
                        $("#NewCarPricingInfoPanel #NewCarListPrice").val(orignalPrice.toString());
                        $(this).dialog("close");
                        $("#OfferPriceWarningDialog").remove();
                    }
                }
            });

            return false;
        } else {
            $("#NewCarPricingInfoPanel :submit").hide();
            $("#PricesSaving").show();
        }

        return true;
    },

    SetBookValue: function () {

        $("#BookValueSpan").after("<div class=\"loading\">&nbsp;</div>");

        var postData = "{ \"json\": \"{ 'BusinessUnitId':" + $("#BusinessUnitIdHidden").val()
                                + ", 'InventoryId':" + $("#InventoryIdHidden").val()
                                + ", 'BookoutCategory':'" + $("#BookoutCategoryHidden").val()
                                + "' }\" }";

        // Post request
        $.ajax({
            type: "POST",
            url: "ApprovalSummary.aspx/GetBookValue",
            data: postData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {

                var bookValues = $("#BookValues:first"),
                    message = JSON.parse(msg.d);

                if (message.BookValue.length > 0) {
                    $("#BookValueSpan", bookValues).text(message.BookValue);
                    if (!message.NeedsAttention)
                        $("#BookValuesHeader").text("Book Value");
                    $("#BookOutLink, #BookAttentionLink").hide().after("<div class=\"loading\">&nbsp;</div>");
                }


                if (message.NeedsAttention) {
                    $(bookValues).addClass("attention");
                    $("#BookAttentionLink").show();
                } else {
                    $(bookValues).removeClass("attention");
                    $("#BookOutLink, #BookAttentionLink", bookValues).hide();
                }

                if (message.BookValue != ApprovalSummary.CurrentBookValue) Page.HighlightRegenerate();

                $("#BookValues div.source div.loading").remove();
            },
            error: function () {
                $("#BookValueSpan").text(" -- ");
                $("#BookValues div.source div.loading").remove();
            }

        });

    },

    SetSecondaryBookValue: function () {

        $("#BookSecondaryValueSpan").after("<div class=\"loading\">&nbsp;</div>");

        var postData = "{ \"json\": \"{ 'BusinessUnitId':" + $("#BusinessUnitIdHidden").val()
                                + ", 'InventoryId':" + $("#InventoryIdHidden").val()
                                + ", 'BookoutCategory':'" + $("#BookoutSecondaryCategoryHidden").val()
                                + "' }\" }";

        // Post request
        $.ajax({
            type: "POST",
            url: "ApprovalSummary.aspx/GetSecondaryBookValue",
            data: postData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {

                var bookValues = $("#BookValues:first"),
                    message = JSON.parse(msg.d);

                if (message.BookValue.length > 0) {
                    $("#BookSecondaryValueSpan").text(message.BookValue);
                    if (!message.NeedsAttention)
                        $("#BookValuesHeader").text("Book Value");
                    $("#BookOutSecondaryLink, #BookSecondaryAttentionLink").hide().after("<div class=\"loading\">&nbsp;</div>");
                }

                if (message.NeedsAttention) {
                    $(bookValues).addClass("attention");
                    $("#BookSecondaryAttentionLink").show();
                } else {
                    $(bookValues).removeClass("attention");
                    $("#BookOutSecondaryLink, #BookSecondaryAttentionLink", bookValues).hide();
                }

                if (message.BookValue != ApprovalSummary.SecondaryBookValue) Page.HighlightRegenerate();

                $("#BookValues div.source div.loading").remove();
            },
            error: function () {
                $("#BookSecondaryValueSpan").text(" -- ");
                $("#BookValues div.source div.loading").remove();
            }

        });

    },

    SetEdmundsValue: function () {

        $("#BookTmvValueSpan").after("<div class=\"loading\">&nbsp;</div>");

        var postData = "{ \"json\": \"{ 'BusinessUnitId':" + $("#BusinessUnitIdHidden").val()
                                + ", 'InventoryId':" + $("#InventoryIdHidden").val()
                                + ", 'IsEdmundsActive':'" + $("#BookoutTmvCategoryHidden").val()
                                + "' }\" }";

        // Post request
        $.ajax({
            type: "POST",
            url: "ApprovalSummary.aspx/GetEdmundsBookValue",
            data: postData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {

                var bookValues = $("#BookValues:first"),
                    message = JSON.parse(msg.d);

                if (message.BookValue.length > 0) {
                    $("#BookTmvValueSpan").text(message.BookValue);
                    if (!message.NeedsAttention)
                        $("#BookValuesHeader").text("Book Value");
                    $("#BookOutTmvLink, #BookTmvAttentionLink").hide().after("<div class=\"loading\">&nbsp;</div>");
                }

                if (message.NeedsAttention) {
                    $(bookValues).addClass("attention");
                    $("#BookTmvValueSpan").text('');
                    $("#BookTmvAttentionLink").show();
                    $("#BookOutTmvLink").show();
                } else {
                    $(bookValues).removeClass("attention");
                    $("#BookOutTmvLink, #BookTmvAttentionLink", bookValues).hide();
                }

                if (message.BookValue != ApprovalSummary.EdmundsBookValue) Page.HighlightRegenerate();

                $("#BookValues div.source div.loading").remove();
            },
            error: function () {
                $("#BookValues div.source div.loading").remove();
                window.location.reload(); //Reload window for error.
            }
        });

    },
    SetKbbConsumerValue: function () {

        $("#KbbConsumerValueSpan").after("<div class=\"loading\">&nbsp;</div>");

        var postData = "{ \"json\": \"{ 'BusinessUnitId':" + $("#BusinessUnitIdHidden").val()
                                + ", 'InventoryId':" + $("#InventoryIdHidden").val()
                                + " }\" }";

        // Post request
        $.ajax({
            type: "POST",
            url: "ApprovalSummary.aspx/GetKbbConsumerValue",
            data: postData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {

                var bookValues = $("#BookValues:first"),
                    message = JSON.parse(msg.d);

                if (message.BookValue.length > 0) {
                    $("#KbbConsumerValueSpan").text(message.BookValue);
                    if (!message.NeedsAttention)
                        $("#BookValuesHeader").text("Book Value");
                    $("#KbbConsumerBookoutLink, #KbbConsumerAttentionLink").hide().after("<div class=\"loading\">&nbsp;</div>");
                }

                if (message.NeedsAttention) {
                    $(bookValues).addClass("attention");
                    $("#KbbConsumerAttentionLink").show();
                } else {
                    $(bookValues).removeClass("attention");
                    $("#KbbConsumerBookoutLink, #KbbConsumerAttentionLink", bookValues).hide();
                }

                if (message.BookValue != ApprovalSummary.SecondaryBookValue) Page.HighlightRegenerate();

                $("#BookValues div.source div.loading").remove();
            },
            error: function () {
                $("#KbbConsumerValueSpan").text(" -- ");
                $("#BookValues div.source div.loading").remove();
            }

        });

    },
    SetVehicleHistory: function () {

        $.ajax({
            type: 'POST',
            url: 'ApprovalSummary.aspx/HasCarfaxReport',
            data: "{ businessunitId:" + $("#BusinessUnitIdHidden").val() + ", vin:'" + $("#VinHidden").val() + "' }",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {

                var message = JSON.parse(msg.d);

                var text = message.HasCarfax == "True" ? "View Report" : "Order Report";

                $("#VehicleHistoryLink").html(text);
                if (message.HasCarfax == "True") {
                    $("#VehicleHistory").removeClass("attention");
                    //Car fax changed highlight the regen button
                    Page.HighlightRegenerate();
                }
            },
            error: function () {

            }
        });
    },

    HighlightRegenerate: function () {
        $("#RewriteAdButton").addClass("red");
    },

    InitAccordions: function () {

        var adConfigForm = $("#AdConfigForm"),
            options = $("#options");

        $(".CollapsiblePanel h3", options).off("click").on("click", function () {

            $(this).toggleClass("open");
            $("#ops", options).toggle();

        });

        $("h4.accordionHead", adConfigForm).off("click").on("click", function (e) {

            $(this).toggleClass("open").toggleClass("closed");
            $(this).siblings("div.accordion").toggle();

            e.preventDefault();
            e.stopPropagation();

        });

    },

    InitAdApproval: function () {

        var approvalDialog = $("#ApprovalModalPanel").dialog({ height: "auto", closeOnEscape: false, closeText: "", dialogClass: "approval", modal: true, title: "Optimized Ad Approved", width: 600 });

        ApprovalSummary.CurrentModal = ApprovalSummary.ModalWindows.AdApproval;

        // IE6 Approval Panel
        if (MAXUI.IsIE6()) {

            panelPosition = document.documentElement.clientHeight -
                    $("#Header").height() - $("#InventoryItem").height() -
                    61 + document.documentElement.scrollTop;

            $("#ApprovalPanel").css("top", panelPosition);

            $(window).scroll(function () {
                panelPosition2 = document.documentElement.clientHeight - $("#Header").height() - $("#InventoryItem").height() - 61 + document.documentElement.scrollTop;
                $("#ApprovalPanel").css("top", panelPosition2);
            });

        }

    },

    InitAdChoice: function () {

        var adChoiceDialog = $("#ChoicePanel").dialog({ height: "auto", closeOnEscape: false, closeText: "", modal: true, title: "Choose Description", width: 750 });

        adChoiceDialog.parent().appendTo("form:first");

        ApprovalSummary.CurrentModal = ApprovalSummary.ModalWindows.AdChoice;

    },

    InitPriceCheck: function () {
        $("#NewCarPricingInfoPanel :submit").click(function () {
            return Page.CheckInternetPriceDelta();
        });
    },

    InitAddPackagesModal: function () {

        $("#btnAddPackage").off("click").on("click", function (e) {

            $(this).val("").addClass("loading");

        });

    },

    /// Initializes UI for Auto Load button postback
    InitAutoLoad: function () {

        // Redisplay modal using jQuery
        var autoLoadDialogDiv = $("#NeededLoginInfo:first"),
            autoLoadDialog = $(autoLoadDialogDiv).dialog({ closeOnEscape: false, dialogClass: "autoLoad hideClose", modal: true, position: ['center', 200], title: "Auto Load", width: 600 });
        autoLoadDialog.parent().appendTo("form:first");

        ApprovalSummary.CurrentModal = ApprovalSummary.ModalWindows.AutoLoad;

        $("#AutoLoad .button").unbind("click").bind("click", function (e) {

            $(this).css("display", "none");
            $(this).next(".autoLoadWorking").show();
            $(this).next(".status").hide();

            $("a, input[class*='autoLoadButton'], #DropDownLists select, #TrimStyleDropDown, textarea, button").attr("disabled", "disabled").css("cursor", "default");

        });

        // Show 'please wait' panel when user clicks cancel on autoload login screen
        $("input.cancelBtn, #automator_xBtn", autoLoadDialogDiv).off("click").on("click", function (e) {

            var modalBody = $(".modalBod:first", autoLoadDialogDiv);
            $(modalBody).append("<div class=\"pleaseWaitPanel\" style=\"height:"
                + ($(autoLoadDialogDiv).height() + 24) + "px;\"><p style=\"margin-top:"
                + parseInt(($(autoLoadDialogDiv).height() * 0.4), 10)
                + "px\">Please wait ...</p></div>");

        });

    },

    /// Checks chrome (trim) selection and updates UI (red text, grey-out ad panel)
    InitTrimSelect: function () {

        var trimSelectContainer = $("#TrimSelector:first"),
            trimSelect = $("select:first", trimSelectContainer),
            newVal;

        if (trimSelect.length < 1 || trimSelectContainer.length < 1) { return; }

        if ($(trimSelect).val().trim() === "") {

            $(trimSelectContainer).addClass($(trimSelectContainer).hasClass("red") ? "" : "red");
            this.MaskAdPanel();

            $(trimSelect).change(function () {
                ApprovalSummary.TrimSelectPostback();
            });

        } else {

            $(trimSelect).change(function () {
                newVal = $(this).val();
                if (confirm("This will clear all elements of your ad. Are you sure?")) {
                    ApprovalSummary.TrimSelectPostback();
                    return;
                }
                $(this).val($("#TrimStyleDropDown").data("TrimSelect").initialValue);
                return false;
            });

        }

    },

    /// Sets up IsDirty flag and alert
    InitIsDirtyAlert: function () {

        // Sets IsVehicleDirty flag on config form change
        $("#DropDownLists select, #DropDownLists2 select, #KeyInformation input:checkbox, #FrameworkSelectorDDL, #ddlCertifiedPrograms, #txtCertifiedID").bind("change", { instance: this }, function (e) {

            var instance = e.data.instance;

            ApprovalSummary.IsVehicleDirty = true;
            instance.HighlightRegenerate();

            $("#RewriteAdButton").addClass("red").bind("click", function () {
                AdPanel.IsDirty = false;
                ApprovalSummary.IsVehicleDirty = false;
            });

        });

        // Sets all dirty flags to false when 'REGENERATE AD' is clicked
        $("#RewriteAdButton").click(function () {

            // validate certified id
            if ($("#ddlCertifiedPrograms").length > 0 && $("#ddlCertifiedPrograms").val() != undefined && $("#ddlCertifiedPrograms").val() !== null && $("#ddlCertifiedPrograms").val().split("|")[1].toLowerCase() === "true") //requires certified id
            {
                var certifiedId = $("#txtCertifiedID").val();

                if (certifiedId.length != 0 && certifiedId.match("^[0-9]{6,8}$") == null) {
                    alert("CertifiedId is not a valid format. A Certified Id should be a number 6 to 8 digits long.");
                    return false;
                }
            }

            AdPanel.IsDirty = false;
            ApprovalSummary.IsVehicleDirty = false;
        });

        // Checks for dirty vehicle info and sets IsAdDirty dirty flag to false when 'APPROVE VEHICLE' is clicked
        $("#approveButton").click(function () {

            var newModule,
                message = "Changes have been made to this vehicle since the ad was last generated." +
                          "The ad copy may not reflect the new information about the vehicle.\r\n\r\n" +
                          "Would you like to approve this vehicle anyway?";

            if (ApprovalSummary.IsVehicleDirty || $("#AdAndVehicleInSyncHidden").val().toLowerCase() === "false") {

                if (!confirm(message)) { return false; }

            }

            // If only the preview module is left, add a blank module (for server-side compliance)
            if (AdPanel.Advertisement.Modules.length === 1) {
                newModule = { "Type": "Module", "Header": { "Type": "Item", "Edited": "false", "Text": "" },
                    "Paragraph": [{ "Type": "Item", "Edited": "false", "Text": ""}]
                };
                AdPanel.Advertisement.Modules.push(newModule);
                AdPanel.SaveModel();
            }

            AdPanel.IsDirty = false;

            return true;

        });

        // Alerts user on page unload if dirty
        $(window).bind("beforeunload", function () {

            // Update JSON hidden text field
            AdPanel.SaveModel();

            if (AdPanel.IsDirty) {
                return "You have made unsaved changes to this ad. Navigating away from this page will cause you to lose these changes.";
            }

        });

    },

    InitPackagesModal: function () {

        $("#QuickPackages span.editLink").off("click").on("click", function (e) {

            var optionPackagesDialog = $("#OptionsModalUpdatePanel").dialog({ height: "auto", modal: true, title: "Option Packages", width: 960 });

            optionPackagesDialog.parent().appendTo("form:first");

            ApprovalSummary.CurrentModal = ApprovalSummary.ModalWindows.Options;

            e.preventDefault();
            e.stopPropagation();

        });

        $("#PackageContent input:submit, #PackageContent input:image").off("click").on("click", function (e) {

            var modal = $(this).parents("#OptionsModalUpdatePanel:first"),
                content = $(modal).find("#Content"),
                packageContent = $(modal).find("#PackageContent");

            $(modal).addClass("loading");
            $(modal).parents(".ui-dialog").find(".ui-dialog-titlebar-close").css("visibility", "hidden");
            $(content).css("background-color", "transparent");
            $(packageContent).css("display", "none");

        });

    },

    InitPackageDelete: function () {

        $("#PackagesUL li a").off("click").on("click", function (e) {

            var li = $(this).parent();

            $(li).children().css("visibility", "hidden");
            $(li).addClass("loading");

        });

    },

    InitVHR: function () {

        $("#VehicleHistoryLink").off("click").on("click", function (e) {

            var vehicleHistoryWindow = window.open($(this).attr("href"), "VehicleHistoryWindow", "scrollbars=1,width=956,height=546"),
                poller;

            // Ensure focus
            vehicleHistoryWindow.focus();

            //poll for change only if there is no vehicle history report
            if ($("#VehicleHistory").hasClass("attention")) {

                $("#VehicleHistoryLink").html("<div class=\"loading\">&nbsp;</div>");

                // Listen for window's close event
                poller = window.setInterval(function () {

                    if (vehicleHistoryWindow.closed) {

                        window.setTimeout(function () {

                            var vehicleHistory = $("#VehicleHistory");

                            Page.SetVehicleHistory();

                        }, 1000);

                        window.clearInterval(poller);
                    }

                }, 500);
            }
            e.preventDefault();
            e.stopPropagation();

        });
    },

    InitBookValue: function () {

        $("#BookOutLink, #BookAttentionLink").off("click").on("click", function (e) {

            var bookOutWindow = window.open($(this).attr("href"), "BookOutWindow", "scrollbars=1,width=956,height=546"),
                poller;

            // Ensure focus
            bookOutWindow.focus();

            // Listen for window's close event
            poller = window.setInterval(function () {

                if (bookOutWindow.closed) {

                    window.setTimeout(function () {
                        Page.SetBookValue();

                    }, 3000);

                    window.clearInterval(poller);
                }

            }, 500);

            e.preventDefault();
            e.stopPropagation();

        });

    },
    InitSecondaryBookValue: function () {

        $("#BookOutSecondaryLink, #BookSecondaryAttentionLink").off("click").on("click", function (e) {

            var bookOutWindow = window.open($(this).attr("href"), "BookOutWindow", "scrollbars=1,width=956,height=546"),
                poller;

            // Ensure focus
            bookOutWindow.focus();

            // Listen for window's close event
            poller = window.setInterval(function () {

                if (bookOutWindow.closed) {

                    window.setTimeout(function () {
                        Page.SetSecondaryBookValue();
                    }, 3000);

                    window.clearInterval(poller);
                }

            }, 500);

            e.preventDefault();
            e.stopPropagation();

        });

    },
    InitEdmundsBookValue: function () {

        $("#BookOutTmvLink, #BookTmvAttentionLink").off("click").on("click", function (e) {

            var bookOutWindow = window.open($(this).attr("href"), "BookOutWindow", "scrollbars=1,width=956,height=546"),
                poller;

            // Ensure focus
            bookOutWindow.focus();

            // Listen for window's close event
            poller = window.setInterval(function () {

                if (bookOutWindow.closed) {

                    window.setTimeout(function () {
                        Page.SetEdmundsValue();
                    }, 3000);

                    window.clearInterval(poller);
                }

            }, 500);

            e.preventDefault();
            e.stopPropagation();

        });

    },
    InitKbbConsumer: function () {

        $("#KbbConsumerBookoutLink, #KbbConsumerAttentionLink").off("click").on("click", function (e) {

            var bookOutWindow = window.open($(this).attr("href"), "BookOutWindow", "scrollbars=1,width=956,height=546"),
                poller;

            // Ensure focus
            bookOutWindow.focus();

            // Listen for window's close event
            poller = window.setInterval(function () {

                if (bookOutWindow.closed) {

                    window.setTimeout(function () {
                        Page.SetKbbConsumerValue();
                    }, 3000);

                    window.clearInterval(poller);
                }

            }, 500);

            e.preventDefault();
            e.stopPropagation();

        });

    },
    InitLastAdLink: function () {
        var dialogMarkup = $('<div class="previousAd"></div>');

        dialogMarkup.dialog({
            autoOpen: false,
            buttons: { "Close": function () { $(this).dialog('close'); } },
            width: 900,
            maxHeight: 700,
            modal: true
        });

        function GenerateDialogContent() {
            var data, parsedData;
            data = $("#PreviousAdModelHidden").val();
            parsedData = JSON.parse(data);
            return $("#PreviousAdTemplate").tmpl(parsedData).html();
        }

        function SetupAndOpenDialog() {
            var stockNumber = $("#stockNumber").text();
            dialogMarkup.dialog('option', 'title', 'Previous Ad (Stock #' + stockNumber + ')');
            dialogMarkup.html(GenerateDialogContent());
            dialogMarkup.dialog('open');
        }

        $('#LastAdWindowLink').click(function () {
            SetupAndOpenDialog();
            return false;
        });
    },

    /// Adds grey box over advertisement panels
    MaskAdPanel: function () {

        var adPanel = $("#Content:first"),
            maskHeight = $(adPanel).height() - 2,
            maskWidth = $(adPanel).width() - 2;

        $("#OptionsPanel, #AdApproval").hide();

        $(adPanel).find("a, input, select, textarea").attr("disabled", "disabled");

        $("<div style=\"height:" + maskHeight + "px;left:0;top:0;width:" + maskWidth + "px;\" class=\"mask\"><p style=\"margin-top:" +
                (maskHeight * 0.4) + "px\">Please select trim above to continue.<p></div>").appendTo(adPanel);

    }

};

var Page;
$( document ).ready( function () {
    Page = new ApprovalSummary();
    Page.OnReady();
} );