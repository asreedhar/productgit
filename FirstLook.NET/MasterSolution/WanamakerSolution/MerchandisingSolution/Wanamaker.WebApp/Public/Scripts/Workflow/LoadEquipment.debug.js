﻿//  ===========================================================================
//  This script is intended for use with the LoadEquipment.aspx page
//  ===========================================================================
//  Dependencies:
//      - jQuery.1.4.3.min.js
//  ---------------------------------------------------------------------------

var EquipmentPage = function () { };

EquipmentPage.TrimSelectPostback = function () { __doPostBack("TrimStyleDropDown", ""); };

EquipmentPage.prototype = {

    /// Initializes client-side page functionality on page load
    onReady: function () {

        this.InitPanelExpanders();
        this.InitColorPalette();
        this.InitTrimSelect();
        this.InitEquipmentChecked();
        this.InitCustomEquipment();
    },

    CheckEquipmentComplete: function () {
        if (!$("#StepCompleted input:checkbox").is(':checked') && WorkflowPage) {
            $("#StepCompleted input:checkbox").attr('checked', 'checked');
            WorkflowPage.SetComplete.call($("#StepCompleted input:checkbox").get(0), { instance: WorkflowPage });
        }
    },

    InitCustomEquipment: function () {
        var that = this;
        $("#CustomEquipmentForm input#stockSearchButton").click(function () {
            that.CheckEquipmentComplete();
        });
    },

    InitEquipmentChecked: function () {
        var that = this;
        $("div#equipInt input:checkbox, div#equipExt input:checkbox, div#lotEquip input:checkbox").click(function () {
            if ($(this).is(':checked')) {
                that.CheckEquipmentComplete();
            }
        });
    },

    InitColorPalette: function () {

        $("#ColorPaletteLink").die("click").live("click", function (e) {

            $("#extColorDDL").toggle();

            e.preventDefault();
            e.stopPropagation();

        });

    },

    /// Checks chrome (trim) selection and updates UI (red text, grey-out ad panel)
    InitTrimSelect: function () {

        var trimSelectContainer = $("#TrimSelector:first"),
            trimSelect = $("select:first", trimSelectContainer),
            newVal;

        if (trimSelect.length < 1 || trimSelectContainer.length < 1) { return; }


        $(trimSelect).change(function () {
            EquipmentPage.TrimSelectPostback();
        });

    },

    InitPanelExpanders: function () {

        $(".standardEquipment").unbind("click").bind("click", function () {

            $(this).toggleClass("open");

        });

    }

};

var Page;
$( document ).ready( function () {
    Page = new EquipmentPage();
    Page.onReady();
} );
