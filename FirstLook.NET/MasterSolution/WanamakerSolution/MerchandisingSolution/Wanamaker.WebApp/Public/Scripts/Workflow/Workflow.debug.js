
//  ===========================================================================
//  This script is intended for use with all Workflow pages
//  ===========================================================================
//  Dependencies:
//      - jQuery.1.4.3.min.js
//  ---------------------------------------------------------------------------

var MaxWorkflowPage = function () { };

MaxWorkflowPage.WorkflowStatuses = {
    Equipment: ["needsEquipment"],
    Photos: ["needsPhotos"],
    Pricing: ["needsPricing"],
    AdReview: ["needsAdReview"]
};

MaxWorkflowPage.prototype = {

    OnReady: function () {

        this.InitCompletedCheckbox();
        this.InitWindowSticker();
        this.InitTrimHelper();
        this.InitCustomTrim();
        this.InitCertifiedControls();

    },

    SetComplete: function (e) {

        var container = $(this).parents("#StepCompleted:first"),
                instance = e.instance,
                isChecked = $(this).is(":checked"),
                label = $(container).find("label"),
                stepStatusType = $(container).find("span[stepstatustype]").attr("stepstatustype");

        // Show spinner
        $(container).append("<div class=\"loading\">&nbsp;</div>");

        window.setTimeout(function () {

            $.ajax({
                type: "POST",
                url: location.pathname + "/UpdateStepComplete" + location.search,
                data: "{ \"json\": \"{ 'StepStatusType' : '" + stepStatusType + "', 'ActivityStatusCode' : '" + (isChecked ? "Complete" : "Incomplete") + "' }\" } ",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {

                    // Hide loading animation
                    $(container).find("div.loading").remove();

                    // Check for error message in return json
                    var json = JSON.parse(msg.d);
                    if (json.error && json.error.length > 0) {
                        // TODO: Handle error
                        $(container).html("<p class=\"error\" title=\"\">Sorry, we were unable to update the status at this time.</div>");
                    } else {
                        // TODO: Handle success
                        $(container).toggleClass("complete");
                        instance.UpdateStatusIcons(stepStatusType, isChecked);
                    }

                },

                error: function () {
                    // TODO: Handle error
                    $(container).html("<p class=\"error\">Sorry, we were unable to update the status at this time.</div>");
                }

            });

        }, 250);

    },

    InitCompletedCheckbox: function () {

        $("#StepCompleted input:checkbox").die("click").live("click", { instance: this }, function (e) {
            e.data.instance.SetComplete.call(this, { instance: e.data.instance });
        });

    },

    InitCertifiedId: function () {
        var showIdBox = "false";
        if ( $("#ddlCertifiedPrograms").val() &&
             ($("#ddlCertifiedPrograms").val().indexOf("|") > -1)) {
            showIdBox = $("#ddlCertifiedPrograms").val().split('|')[1].toLowerCase();
        }
        if (showIdBox == 'true') {
            $("#spanDisplayCertifiedID").show();
        } else {
            $("#spanDisplayCertifiedID").hide();
        }
    },

    InitCertifiedControls: function () {
        
        WorkflowPage.InitCertifiedId();

        $("#ddlCertifiedPrograms").bind("change", function () {
            WorkflowPage.InitCertifiedId();
            ApprovalSummary.IsVehicleDirty = true;
        });

        $("#txtCertifiedID").bind("change", function () {
            ApprovalSummary.IsVehicleDirty = true;
        });

    },

    InitCustomTrim: function () {

        $("#CustomTrimSubmit").die("click").live("click", function (e) {

            var buid = $("#BusinessUnitIdHidden").val(),
                inventoryId = $("#InventoryIdHidden").val(),
                customTrim = $('#CustomTrimTextbox').val();

            $.ajax({
                type: "POST",
                url: "/merchandising/InventoryMVC.aspx/SetCustomTrim",
                contentType: 'application/json; charset=utf-8',
                data: "{ businessUnitId: " + buid + ", inventoryId: " + inventoryId + ", customTrim: '" + customTrim + "' }",
                error: function (jqXHR, textStatus, errorThrown) {
                },
                success: function (data) {
                    if (data.success) {
                        //notify success
                        window.location.assign(window.location);
                    } else {
                        alert("failure");
                        //problem occured
                    }
                },
                complete: function () {

                }
            });

            e.preventDefault();
            e.stopPropagation();

        });


    },

    InitTrimHelper: function () {

        $("#discriminators").dialog({ height: "auto", modal: true, title: "Trim Helper", width: 999 });

    },

    UpdateStatusIcons: function (type, complete) {

        // Map 'ExteriorEquipment' to 'Equipment'
        type = type.replace("Exterior", "");

        var cssClass = MaxWorkflowPage.WorkflowStatuses[type][0],
            icons, listItem;

        if (complete) {

            // Workflow icons on equipment, options & photos
            $("#WorkflowStatusIcons").find("li." + cssClass).remove();

            // Workflow menu icons
            $("#WorkflowMenu a[class*='" + cssClass + "']").removeClass(cssClass);

            return;

        }

        // Workflow icons on equipment, options & photos
        listItem = $("<li />").addClass(cssClass).html("&nbsp;");
        $("#WorkflowStatusIcons ul").append(listItem);

        // Workflow menu icons
        $("#WorkflowMenu a:contains(" + type + ")").addClass(cssClass);

        // Because options and equipment share status
        if (type === "Equipment") {
            $("#WorkflowMenu a:contains(Packages)").addClass(cssClass);
        }

    },

    InitWindowSticker: function () {
        $(".windowStickerToggle").die("click").live("click", function (e) {

            var optionPackagesDialog = $("#StickerControls").dialog({ modal: true, title: "Print Window Sticker", width: 600 });

            optionPackagesDialog.parent().appendTo("form:first");

            e.preventDefault();
            e.stopPropagation();

        });

    }

};

var WorkflowPage;
$( document ).ready( function () {
    WorkflowPage = new MaxWorkflowPage();
    WorkflowPage.OnReady();
} );
