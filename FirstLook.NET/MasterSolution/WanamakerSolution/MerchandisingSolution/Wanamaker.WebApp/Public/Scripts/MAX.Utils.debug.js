﻿
var MAXUtils = function () { };

/// Returns querystring value from supplied location.href and key name
MAXUtils.parseQuery = function ( href, key ) {

    var parsedKey, parsedValue;
    var query = href.substr( ( href.lastIndexOf( "?" ) + 1 ), href.length );
    var pairs = query.split( "&" );

    for ( var i = 0; i < pairs.length; i++ ) {

        parsedKey = pairs[i].split( "=" )[0];

        if ( $.trim( key ) === parsedKey ) { return parsedValue = pairs[i].split( "=" )[1]; }

    }

    return "";

};

/// ----------------------------------------
/// Native JavaScript modifications
/// ----------------------------------------

/// Removes items from array from supplied from/to index
Array.prototype.remove = function ( from, to ) {
    var rest = this.slice( ( to || from ) + 1 || this.length );
    this.length = from < 0 ? this.length + from : from;
    return this.push.apply( this, rest );
};

/// Capitalizes the first letter of a string
String.prototype.capitalize = function () {

    return this.substr( 0, 1 ).toUpperCase() + this.substr( 1, this.length - 1 );

};

/// ----------------------------------------
/// jQuery modifications
/// ----------------------------------------

jQuery.fn.outerHtml = function ( s ) {
    return ( s )
        ? this.before( s ).remove()
        : jQuery( "<p>" ).append( this.eq( 0 ).clone() ).html();
}
