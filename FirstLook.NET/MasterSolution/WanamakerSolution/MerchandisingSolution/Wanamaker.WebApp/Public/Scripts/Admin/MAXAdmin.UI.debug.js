﻿
var MAXUIGlobal = function() {};

MAXUIGlobal.prototype = {

    /// Initialize
    onReady: function () {

        this.initNavMenu();
        this.initPanelShadow();
        this.initMax30UpgradeLogic();
        this.initNewCarPricing();
        this.initMaxForWebsite20();
        this.initUseTwilio();
        this.initUseDealerRater();

        $(".pageContent").css("visibility", "visible");

    },

    initNewCarPricing: function () {
        $('#batchPriceNewCars').click(function () {
            var isChecked = $(this).is(":checked");
            if (isChecked) {
                $('#chkNewCars').attr('checked', 'checked');
            }
        });
        $('#chkNewCars').click(function () {
            var isChecked = $(this).is(":checked");
            if (!isChecked) {
                $('#batchPriceNewCars').removeAttr("checked");
            }
        });
    },

    initMax30UpgradeLogic: function () {
        var updateOEMBrandingEnabled = function () {
            if ($("#OEMBrandingEnabled").is(":checked")) {
                $("#MaxForOEMList").removeAttr("disabled");
            } else {
                $("#MaxForOEMList").attr("disabled", true);
                $("#MaxForOEMList").val("0");
            }
        };
        updateOEMBrandingEnabled();

        $("#Max30Upgrade").click(function () {
            var isChecked = $(this).is(":checked");
            var isAutoloadEnabled = !($("#BatchAutoloadEnabled").attr("disabled"));

            if (isChecked) {
                var elementsToCheck = $("#Max30Section :checkbox").not("#Max30Upgrade").not("#ModelSpecificAdsEnabled").not("#AnalyticsSuiteCheckBox").not("#chkShowOlineClassifiedOverview");
                if (!isAutoloadEnabled)
                    elementsToCheck = elementsToCheck.not("#BatchAutoloadEnabled");
                elementsToCheck.attr('checked', 'checked');
            } else {
                $("#WebLoaderEnabled").removeAttr("checked");
            }

            updateOEMBrandingEnabled();
        });

        $("#OEMBrandingEnabled").change(function () {
            updateOEMBrandingEnabled();
        });
    },

    initMaxForWebsite20: function() {
        var updatechkMaxForWebsite20 = function () {
            if ($("#chkMaxForWebsite20").is(":checked")) {
                $("#chkUsePhoneMapping").removeAttr("disabled");
                $("#chkUseDealerRater").removeAttr("disabled");
            } else {
                $("#chkUsePhoneMapping").removeAttr("checked");
                $("#chkUsePhoneMapping").attr("disabled", true);
                $("#txtAreaCode").attr("disabled", true);
                $("#txtMappedPhone").attr("disabled", true);
                $("#btnProvision").attr("disabled", true);
                $("#btnProvisionTollFree").attr("disabled", true);
                $("#txtProvisionedPhone").attr("disabled", true);

                $("#chkUseDealerRater").removeAttr("checked");
                $("#chkUseDealerRater").attr("disabled", true);
                $("#txtDealerRaterId").attr("disabled", true);
            }
        };
        updatechkMaxForWebsite20();

        $("#chkMaxForWebsite20").click(function () {
            updatechkMaxForWebsite20();
        });
    },

    initUseTwilio: function () {
        var updatechkUsePhoneMapping = function() {
            if ($("#chkUsePhoneMapping").is(":checked")) {
                $("#txtAreaCode").removeAttr("disabled");
                $("#txtMappedPhone").removeAttr("disabled");
                if ($("#hdnPurchased").attr("value") == "false") {
                    $("#btnProvision").removeAttr("disabled");
                    $("#btnProvisionTollFree").removeAttr("disabled");
                }
                if ($("#hdnEnableTollFree").attr("value") == "true")
                    $("#btnProvisionTollFree").css("visibility", "visible");
                else
                    $("#btnProvisionTollFree").css("visibility", "hidden");
                $("#txtProvisionedPhone").removeAttr("disabled");
            } else {
                $("#txtAreaCode").attr("disabled", true);
                $("#txtMappedPhone").attr("disabled", true);
                $("#btnProvision").attr("disabled", true);
                $("#txtProvisionedPhone").attr("disabled", true);
                if ($("#hdnEnableTollFree").attr("value") == "true")
                    $("#btnProvisionTollFree").css("visibility", "visible");
                else
                    $("#btnProvisionTollFree").css("visibility", "hidden");
                $("#btnProvisionTollFree").attr("disabled", true);
            }
        };
        updatechkUsePhoneMapping();

        $("#chkUsePhoneMapping").click(function () {
            updatechkUsePhoneMapping();
        });
    },

    initUseDealerRater: function() {
        var updatechkUseDealerRater = function() {
            if ($("#chkUseDealerRater").is(":checked")) {
                $("#txtDealerRaterId").removeAttr("disabled");
            } else {
                $("#txtDealerRaterId").attr("disabled", true);
            }
        };
        updatechkUseDealerRater();

        $("#chkUseDealerRater").click(function() {
            updatechkUseDealerRater();
        });
    },

    /// Setup main navigation drop-down-menus
    initNavMenu: function () {

        // Setup menu headers to open menus on click
        $("li.menuHead > a").bind("click", function (e) {
            $(".subMenu").hide();
            $(this).parent().find(".subMenu").show("blind", 200);
            $(this).parent().find(".subMenu h5 a").unbind().bind("click", function () {
                $(this).parents(".subMenu").hide("blind", 250);
                e.preventDefault();
            });
            e.preventDefault();
        });

        // Setup menu headers to open menus on mouseover
        //        $("li.menuHead").bind("mouseover", function(e) {
        //            $(this).find(".subMenu").show();
        //            e.preventDefault();
        //        });
        //        
        //        // Setup menu headers to hide menus on mouseout
        //        $("li.menuHead").bind("mouseout", function(e) {
        //            $(this).find(".subMenu").hide();
        //            e.preventDefault();
        //        });

        // Set z-index so drop-downs cover adjacent menu items
        $("li.menuHead").each(function (index) {
            $(this).css("z-index", 500 - index);
        });

        // Close menus on unrelated click
        //        $("*").bind("click", function(e) {
        //            alert($(this).parents("li.menuHead").length);
        //            if ($(this).parents("li.menuHead").length < 1) {
        //                $(".subMenu").hide();
        //            }
        //        });

        // Add drop shadow effect
        $(".subMenu, .inlineAdd").each(function () {
            var children = $(this).html();
            $(this).html(
                "<div class=\"shadow\">" +
                "\t<div class=\"shadowed\">" +
                children +
                "\t</div>" +
                "</div>"
            );
        });

    },

    /// Setup panel drop shadow
    initPanelShadow: function () {

        $(".panel").each(function () {

            // Store positioning CSS
            var positionCss = "margin-top:" + $(this).css("margin-top") + ";" +
                              "margin-right:" + $(this).css("margin-right") + ";" +
                              "margin-bottom:" + $(this).css("margin-bottom") + ";" +
                              "margin-left:" + $(this).css("margin-left") + ";" +
                              "clear:" + $(this).css("clear") + ";" +
                              "float:" + $(this).css("float") + ";" +
                              "position:" + $(this).css("position") + ";" +
                              "top:" + $(this).css("top") + ";" +
                              "right:" + $(this).css("right") + ";" +
                              "bottom:" + $(this).css("bottom") + ";" +
                              "left:" + $(this).css("left") + ";";

            // Reset positioning CSS
            $(this).css("margin-top", "0").css("margin-right", "0").css("margin-bottom", "0").css("margin-left", "0").css("float", "none");
            $(this).css("position", "static").css("top", "auto").css("right", "auto").css("bottom", "auto").css("left", "auto");

            $(this).wrap("<div style='border:solid #fafafa 1px;border-radius:4px;-moz-border-radius:4px;" + positionCss + "'>").wrap("<div style='border:solid #f4f4f4 1px;border-radius:3px;-moz-border-radius:3px;' />").wrap("<div style='border:solid #e8e8e8 1px;border-radius:2px;-moz-border-radius:2px;'>").wrap("<div style='border:solid #d6d6d6 1px;' />");

        });

    }

};

var MaxUiPage;
$(document).ready(function() {
    MaxUiPage = new MAXUIGlobal();
    MaxUiPage.onReady();
});