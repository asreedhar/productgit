﻿
var AutoLoadStatus = {};

 (function () {

    this.init = function(){
        this.initTable(AutoLoadStatus.$DOM.statusTable);
        this.initRow();

        //IE Redraw table headers
        $('#Container').keyup(function(){
            $('#autoLoadStatus thead tr, #details table thead tr').addClass('reDraw1');
        });

        //IE Redraw table headers
        $('#Container').change(function(){
            $('#autoLoadStatus thead tr, #details table thead tr').addClass('reDraw1');
        });
    };

    this.initTable = function(table){
        table.dataTable( {
            "bJQueryUI": true,
            "sPaginationType": "full_numbers"
        });

        //IE Redraw table headers
        table.on('page', function (){
            $('#autoLoadStatus thead tr, #details table thead tr').addClass('reDraw1');
        });
    };

    this.initRow = function(){
        var that = this;
        AutoLoadStatus.$DOM.statusTableRows.click(function(){
           AutoLoadStatus.$DOM.statusTableRows.removeClass('selected');
           $(this).addClass('selected');
           AutoLoadStatus.$DOM.details.empty();
           AutoLoadStatus.$DOM.loading.show();
           var businessUnitid = $(this).attr("id");
           that.loadDetails(businessUnitid); 
        });
    };

    this.loadDetails = function(businessUnitId){
        $.ajax({
            type: "GET",
            dataType: "html",
            cache: false,
            url: 'AutoLoadDetails.aspx/' + businessUnitId,
            success: function(data){
                AutoLoadStatus.$DOM.loading.hide();
                AutoLoadStatus.$DOM.details.html(data);
                AutoLoadStatus.initTable($('#details table'));
                $('#details table').addClass('display');
            },
            error: function(){
                AutoLoadStatus.$DOM.loading.hide();
                AutoLoadStatus.$DOM.details.html("ERROR");
            }
        });
    };

     $(document).ready(function () {
        AutoLoadStatus.$DOM = {
            statusTable: $('#autoLoadStatus'),
            loading: $('#loading'),
            statusTableRows: $('#autoLoadStatus tbody tr'),
            details: $('#details')
        };
        AutoLoadStatus.init();
    });

 }).apply(AutoLoadStatus);
