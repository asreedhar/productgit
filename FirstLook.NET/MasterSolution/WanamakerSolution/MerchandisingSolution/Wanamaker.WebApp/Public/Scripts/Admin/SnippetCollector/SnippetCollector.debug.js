﻿
var MAXSnippetCollector = function () { };

MAXSnippetCollector.Messages = {

    GenericAjaxError: "We're sorry, an error has occurred. Please <a href=\"#\" onclick=\"window.location.reload()\">reload the page</a> and try again.",
    DefaultListHelper: "Select Make, Models and Trims above to view associated marketing snippets.",
    NoSnippetsFound: "No snippets found for the selected vehicles (trims).",
    SnippetAdded: "", // Currently this message is dynamic. TODO: Handle this
    SnippetUpdated: "Snippet updated!"

};

MAXSnippetCollector.MessageTypes = {
    Information: "info",
    Warning: "warning",
    Error: "error"
};

/// Adds new (or 'undo delete') sources/tags to DOM
MAXSnippetCollector.AddRecord = function ( source, panel, type ) {

    var json = JSON.parse( source ), option, row, selectBox, selected, tableBody;

    if ( $( panel ).find( ".scrollTableBody" ).length > 0 ) { // If panel has a table.scrollTableBody then add to table ...

        // Get table body to append to
        tableBody = $( panel ).find( ".scrollTableBody table tbody" );

        // Create new row and add cells
        row = $( "<tr></tr>" );
        if ( type === "source" ) {
            $( row ).append( "<td class=\"first\">" + ( json.HasCopyright === "True" ? "<span class=\"copyrighted\">Y</span>" : "" ) + "</td>" );
            $( row ).append( "<td>" + json.SourceName + "</td>" );
            $( row ).append( "<td><a name=\"" + json.SourceName + "\" title=\"Edit\" class=\"edit\" href=\"?id=" + json.id + "\"><span>Edit</span></a></td>" );
        } else if ( type === "tag" ) {
            $( row ).append( "<td class=\"first\">" + json.Tag + "</td>" );
            $( row ).append( "<td><a name=\"" + json.Tag + "\" title=\"Edit\" class=\"edit\" href=\"?id=" + json.id + "\"><span>Edit</span></a><a name=\"" + json.Tag + "\" title=\"Delete\" class=\"delete\" href=\"?id=" + json.id + "\"><span>Delete</span></a></td>" );
        } else {
            // TODO: What happens here?
        }

        // Add new row to table body
        $( row ).appendTo( $( tableBody ) );

        // Scroll to new item
        $( location ).attr( "href", "#" + ( type === "tag" ? json.Tag : json.SourceName ) );

        // Highlight new item (Had to hack this a bit due to lack of RGBa support in jQuery animate)
        $( row ).css( "background-color", "#ffff66" );
        $( row ).animate( { backgroundColor: "#f5f5f6" }, 5000, function () {
            $( row ).css( "background-color", "transparent" );
        } );

    } else { // ... else append to select box

        selectBox = ( type === "tag" ? $( panel ).find( "#SnippetTagsFields select" ) : $( panel ).find( "#SnippetDetailsFields select" ) );

        // Build option
        option = $( "<option />" );
        $( option ).attr( "value", json.id );
        $( option ).text( ( type === "tag" ? json.Tag : json.SourceName ) );

        // Add option to select box
        $( selectBox ).append( option );

        // Select new option (and retain previous selections)
        selected = $( selectBox ).val();

        if ( selected === undefined ) {
            selected = json.id;
        } else if ( $.isArray( selected ) ) {
            selected.push( json.id );
        } else {
            selected = [selected, json.id];
        }

        $( selectBox ).val( selected );

        // Scroll to new item
        $( selectBox ).scrollTop( 9999 );

        // Highlight new item (Had to hack this a bit due to lack of RGBa support in jQuery animate)
        $( selectBox ).css( "background-color", "#ffffcc" );
        $( selectBox ).animate( { backgroundColor: "#f5f5f6" }, 5000, function () {
            $( selectBox ).css( "background-color", "transparent" );
        } );

    }


};

MAXSnippetCollector.prototype = {

    /// Initialize
    onReady: function () {

        // Setup tag/source inline forms
        this.initInlineAddLinks();

        // Setup inline source/tag form AJAX handlers
        this.initAddRecordAjax( "source" );
        this.initAddRecordAjax( "tag" );
        this.initEditRecordAjax( "source" );
        this.initEditRecordAjax( "tag" );

        // Setup make/model/trim selector
        this.initFillModelsSelectAjax();
        this.initFillTrimsSelectAjax();

        // Page specific
        if ( $( "#AddSnippetContent" ).length > 0 ) {

            // This is add/edit page
            this.initAddEditSnippetAjax();
            this.initClearFormLinks();
            this.initSubmitEnable();

        } else {

            // This is list page
            this.initDeleteSnippetAjax();
            this.initDeleteTagAjax();
            this.initSnippetListFilter();
            this.initUndoDeleteTagAjax();

        }

    },

    /// Setup source/tag 'Add' button click handler
    initAddRecordAjax: function ( type ) {

        $( ".button.add" + type.capitalize() ).die( "click" ).live( "click", function ( e ) {

            // Get relevant 'inline add' panel
            var panel = $( this ).parents( ".inlineAdd:first" ).find( ".shadowed:first" );

            // Reset validation error message
            $( panel ).find( ".error span" ).hide();

            // Validate form and display error / else process request
            if ( $( panel ).find( "input:text" ).val() === "" ) {
                $( panel ).find( ".error span" ).show();
            } else {

                $( panel ).append( '<div class="loading" style="height:' + $( panel ).outerHeight().toString() + 'px;width:' + $( panel ).outerWidth().toString() + 'px;"></div>' );

                $.ajax( {
                    type: "POST",
                    url: ( type === "tag" ? "Default.aspx/AddSnippetTag" : " Default.aspx/AddSnippetSource" ), /* Having the page filename in here sucks */
                    data: ( type === "tag" ? "{ \"json\": \"{ 'Tag' : '" + $( panel ).find( "input:text" ).val() + "'}\" } " : "{ \"json\": \"{ 'SourceName':'" + $( panel ).find( "input:text" ).val() + "', 'HasCopyright':'" + ( $( panel ).find( "input:checked" ).length > 0 ) + "'}\" }" ),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function ( msg ) {

                        // Hide loading animation
                        $( panel ).find( "div.loading" ).remove();

                        // If source json not returned, then error occured
                        if ( msg.d === "" ) {
                            $( panel ).append( "<div class='error'><span>" + MAXSnippetCollector.Messages.GenericAjaxError + "</span></div>" );
                        } else {

                            // Check for error message in return json
                            var json = JSON.parse( msg.d );
                            if ( json.error && json.error.length > 0 ) {
                                $( panel ).append( "<div class='error'><span>" + unescape( json.error ) + "</span></div>" );
                            } else {
                                $( panel ).find( "a.close" ).trigger( "click" );
                                MAXSnippetCollector.AddRecord( msg.d, panel.parents( ".panel" ), type );
                            }

                        }

                    },

                    error: function () {

                        MaxSnippetCollectorPage.showUserMessage( MAXSnippetCollector.MessageTypes.Error, MAXSnippetCollector.Messages.GenericAjaxError );

                    }
                } );

            }

            // We're all done
            e.preventDefault();
            e.stopPropagation();

        } );

    },

    /// Setup add/edit snippet form submit handler
    initAddEditSnippetAjax: function () {

        $( "#AddSnippetPageButtons input[type=submit]" ).die( "click" ).live( "click", function ( e ) {

            // Reset message container
            $( "#UserMessaging" ).html( "" );

            var submitButton = $( "#AddSnippetPageButtons input:submit" ).get( 0 ),
                snippetId = MAXUtils.parseQuery( window.location.href, "id" ),
                isNewSnippet = ( snippetId.length === 0 ),
                snippetText = $( "#SnippetTextFields textarea" ).val(),
                sourceId = $( "#SnippetSourceFields select option:selected" ).val(),
                isVerbatim = $( "#VerbatimFields input:checked" ).length > 0,
                rating = 0,
                tagIds = $( "#SnippetTagsFields select:first" ).val(),
                styleIds = $( "#VehicleStyleTrimFields select:first" ).val(),
                postData, requestUrl;

            // Construct post data variable
            postData = "{ \"json\": \"{ 'Id':" + ( snippetId === "" ? -1 : snippetId ) + ", 'MarketingText':'" + encodeURI( escape ( snippetText ) ) + "', 'Source':'" + sourceId + "', 'IsVerbatim':'"
                + isVerbatim + "', 'Rating':'" + rating + "', 'Tags':'" + tagIds + "', 'Styles':'" + styleIds + "' }\" }";

            // Set web mthod url
            requestUrl = isNewSnippet ? "AddSnippet.aspx/AddSnippet" : "AddSnippet.aspx/UpdateSnippet";

            // Disable submit and show loading icon
            $( submitButton ).attr( "disabled", "disabled" ).addClass( "disabled" );
            $( submitButton ).after( "<div class=\"loading\">&nbsp;</div>" );

            // Post request
            $.ajax( {
                type: "POST",
                url: requestUrl,
                data: postData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function ( msg ) {

                    // If source json not returned, then error occured
                    if ( msg.d === "" ) {
                        MaxSnippetCollectorPage.showUserMessage( MAXSnippetCollector.MessageTypes.Error, MAXSnippetCollector.Messages.GenericAjaxError );
                    } else {

                        // Check for error message in return json
                        var json = JSON.parse( msg.d );
                        if ( json.error && json.error.length > 0 ) {
                            MaxSnippetCollectorPage.showUserMessage( MAXSnippetCollector.MessageTypes.Error, unescape( json.error ) );
                        } else {

                            if ( isNewSnippet ) {
                                $( ".clearForm" ).click();
                                MaxSnippetCollectorPage.showUserMessage( MAXSnippetCollector.MessageTypes.Information, "Snippet added! Complete the form to add another snippet or <a href=\"?id=" + json.id + "\">edit new snippet</a>." );
                            } else {
                                MaxSnippetCollectorPage.showUserMessage( MAXSnippetCollector.MessageTypes.Information, MAXSnippetCollector.Messages.SnippetUpdated );
                            }

                            $( "#AddSnippetPageButtons div.loading" ).remove();

                        }

                    }

                },

                error: function () {

                    MaxSnippetCollectorPage.showUserMessage( MAXSnippetCollector.MessageTypes.Error, MAXSnippetCollector.Messages.GenericAjaxError );

                }

            } );

            // We're all done
            e.preventDefault();
            e.stopPropagation();

        } );

    },

    /// Setup reset form link click handler
    initClearFormLinks: function () {

        if ( MAXUtils.parseQuery( window.location.href, "id" ) !== "" ) {

            $( ".clearForm" ).bind( "click", function ( e ) {

                $( ".pageContent" ).css( "visibility", "hidden" );

                window.location.reload( true );

                e.preventDefault();
                e.stopPropagation();

            } );

        } else {

            $( ".clearForm" ).bind( "click", function ( e ) {

                $( "form:first" )[0].reset();
                $( "#VehicleYearModelFields select, #VehicleStyleTrimFields select" ).html( "" );

                $( "#AddSnippetPageButtons input:submit" ).attr( "disabled", "disabled" );

                e.preventDefault();
                e.stopPropagation();

            } );

        }

    },

    /// Setup delete snippet button click handler
    initDeleteSnippetAjax: function () {

        $( "#SnippetsList a.delete" ).die( "click" ).live( "click", function ( e ) {

            if ( !confirm( "This action will permanently delete this snippet.\r\n\r\nClick OK to continue." ) ) { return; }

            // Get item id from link href
            var itemID = MAXUtils.parseQuery( $( this ).attr( "href" ), "id" ),
                tr = $( this ).parents( "tr:first" ),
                postData;

            $( tr ).html( "<td colspan=\"3\" class=\"first\"><div style=\"position:relative;height:" + $( tr ).find( "td" ).height() + "px;\"><div class=\"loading\">&nbsp;</div></div></td>" );

            // Construct post data variable
            postData = "{ \"json\": \"{ 'id':" + itemID + " }\" }";

            // Post request
            $.ajax( {
                type: "POST",
                url: "Default.aspx/DeleteSnippet",
                data: postData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function ( msg ) {

                    // If source json not returned, then error occured
                    if ( msg.d === "" ) {
                        $( tr ).find( "td" ).html( "<div class='error'><span>" + MAXSnippetCollector.Messages.GenericAjaxError + "</span></div>" );
                    } else {

                        // Check for error message in return json
                        var json = JSON.parse( msg.d );
                        if ( json.error && json.error.length > 0 ) {
                            $( tr ).find( "td" ).html( "<div class='error'><span>" + unescape( json.error ) + "</span></div>" );
                        } else {

                            // Animate row removal
                            $( tr ).find( "td" ).html( "&nbsp;" ).css( "background-color", "#ffcccc" );
                            $( tr ).find( "td" ).slideUp( 500, function () {

                                $( this ).remove();

                            } );

                        }

                    }

                },

                error: function () {

                    MaxSnippetCollectorPage.showUserMessage( MAXSnippetCollector.MessageTypes.Error, MAXSnippetCollector.Messages.GenericAjaxError );

                }
            } );

            // We're all done
            e.preventDefault();
            e.stopPropagation();

        } );

    },

    /// Setup delete tag button click handler
    initDeleteTagAjax: function () {

        $( "#SnippetTags a.delete" ).die( "click" ).live( "click", function ( e ) {

            // Get item id from link href
            var itemID = MAXUtils.parseQuery( $( this ).attr( "href" ), "id" ),
            tagName = $( this ).attr( "name" ),
            tr = $( this ).parents( "tr:first" ),
            postData;

            $( tr ).html( "<td colspan=\"3\" class=\"first\"><div style=\"position:relative;height:16px;\"><div class=\"loading\">&nbsp;</div></div></td>" );

            // Construct post data variable
            postData = "{ \"json\": \"{ 'id':'" + itemID + "', 'tag':'" + tagName + "' }\" }";

            // Post request
            $.ajax( {
                type: "POST",
                url: "Default.aspx/DeleteTag",
                data: postData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function ( msg ) {

                    // If source json not returned, then error occured
                    if ( msg.d === "" ) {
                        $( tr ).find( "td" ).html( "<div class='error'><span>" + MAXSnippetCollector.Messages.GenericAjaxError + "</span></div>" );
                    } else {

                        // Check for error message in return json
                        var json = JSON.parse( msg.d );
                        if ( json.error && json.error.length > 0 ) {
                            $( tr ).find( "td" ).html( "<div class='error'><span>" + unescape( json.error ) + "</span></div>" );
                        } else {

                            // If no id returned then there were no mappings (so no need to 'UNDO')
                            if ( json.id === "" ) {
                                $( tr ).find( "td" ).html( "<div class=\"undo\"><span>" + json.Tag + "</span></div>" );
                                $( tr ).addClass( "undo" );
                            } else { // else add 'UNDO' link
                                $( tr ).find( "td" ).html( "<div class=\"undo\"><span>" + json.Tag + "</span><a href=\"?id=" + json.id
                                  + "\" name=\"" + json.Tag + "\" class=\"undo\">UNDO</a></div>" );
                                $( tr ).addClass( "undo" );
                            }

                        }

                    }

                },

                error: function () {

                    MaxSnippetCollectorPage.showUserMessage( MAXSnippetCollector.MessageTypes.Error, MAXSnippetCollector.Messages.GenericAjaxError );

                }
            } );

            // We're all done
            e.preventDefault();
            e.stopPropagation();

        } );

    },

    initEditRecordAjax: function () {

        $( "#SourcesDataTable a.edit" ).die( "click" ).live( "click", function ( e ) {

            var thisRow = $( this ).parents( "tr:first" ),
                source = new Source( $( "span.copyrighted", thisRow ).length > 0, MAXUtils.parseQuery( $( "a.edit", thisRow ).attr( "href" ), "id" ), $( "span.sourceName", thisRow ).text() );

            showEditMode( thisRow, source );

            e.preventDefault();
            e.stopPropagation();

        } );

        $( "#TagsDataTable a.edit" ).die( "click" ).live( "click", function ( e ) {

            var thisRow = $( this ).parents( "tr:first" ),
                tag = new Tag( MAXUtils.parseQuery( $( "a.edit", thisRow ).attr( "href" ), "id" ), $( "span.tagText", thisRow ).text() );

            showEditMode( thisRow, tag );

            e.preventDefault();
            e.stopPropagation();

        } );

        $( ".inlineEdit a.close" ).die( "click" ).live( "click", function ( e ) {

            $( this ).parents( ".inlineEdit" ).remove();

            e.preventDefault();
            e.stopPropagation();

        } );

        $( ".inlineEdit input:submit" ).die( "click" ).live( "click", function ( e ) {

            var dataType = $( this ).hasClass( "updateSource" ) ? "Source" : "Tag",
                dataUrl = ( dataType === "Source" ? "Default.aspx/UpdateSnippetSource" : "Default.aspx/UpdateSnippetTags" ),
                itemId = parseInt( $( this ).attr( "id" ).replace( dataType, "" ).replace( "Submit", "" ), 10 ),
                itemText = $( this ).parents( ".form:first" ).find( "input:text" ).val(),
                panel = $( this ).parents( ".inlineEdit:first" ).find( ".shadowed:first" ),
                hasCopyright, postData;

            // Validate form
            if ( itemText.replace( " ", "" ).length === 0 ) {
                $( this ).parents( ".form:first" ).find( "div.error" ).html( "<span>Please enter the " + ( dataType === "Source" ? "source name." : "tag text." ) + "</span>" );
                return false;
            }

            // Show loading animation
            $( panel ).append( '<div class="loading" style="height:' + $( panel ).outerHeight().toString() + 'px;width:' + $( panel ).outerWidth().toString() + 'px;"></div>' );

            // Build post json
            if ( dataType === "Source" ) {
                hasCopyright = ( $( this ).parents( ".form:first" ).find( "input:checkbox:checked" ).length > 0 );
                postData = "{ \"json\": \"{ 'HasCopyright':'" + hasCopyright + "', 'Id':'" + itemId + "', 'SourceName':'" + itemText + "' }\" }";
            } else {
                postData = "{ \"json\": \"{ 'Id\': '" + itemId + "', 'Tag': '" + itemText + "' }\" }";
            }

            $.ajax( {
                type: "POST",
                url: dataUrl,
                data: postData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function ( msg ) {

                    var form = $( ".inlineEdit" )[0],
                        checkHtml, json, row;

                    // If source json not returned, then error occured
                    if ( msg.d === "" ) {
                        $( modelsSelect ).before( "<div class='error'><span>" + MAXSnippetCollector.Messages.GenericAjaxError + "</span></div>" );
                    } else {

                        // Check for error message in return json
                        json = JSON.parse( msg.d );
                        if ( json.error && json.error.length > 0 ) {
                            $( ".inlineEdit div.error" ).replace( "<div class='error'><span>" + unescape( json.error ) + "</span></div>" );
                        } else {

                            setTimeout( function () {

                                row = $( form ).parents( "tr:first" );

                                // Update span
                                $( form ).siblings( "span" ).text( itemText );

                                // Update copyright checkmark
                                if ( dataType === "Source" ) {
                                    checkHtml = ( hasCopyright ? "<span alternatetext=\"©\" class=\"copyrighted\" title=\"© Copyrighted\"></span>" : "" );
                                    $( row ).find( "td:first" ).html( checkHtml );
                                }

                                // Hide form
                                $( ".inlineEdit" ).remove();

                                // Highlight row
                                $( row ).css( "background-color", "#ffff66" );
                                $( row ).animate( { backgroundColor: "#f5f5f6" }, 5000, function () {
                                    $( row ).css( "background-color", "transparent" );
                                } );

                            }, 250 );

                        }

                    }

                },

                error: function () {

                    $( "div.loading", panel ).remove();
                    $( "div.error", panel ).html( "<span>" + MAXSnippetCollector.Messages.GenericAjaxError + "</span>" );

                }

            } );

            // We're all done
            e.preventDefault();
            e.stopPropagation();

        } );

        // Ensure ENTER key submits form
        $( ".inlineEdit" ).die( "keypress" ).live( "keypress", function ( e ) {

            var keyCode = e.keyCode || e.charCode;

            if ( keyCode === 13 ) {

                $( "input:submit", this ).click();

                e.preventDefault();
                e.stopPropagation();

            }

        } );

        function showEditMode( row, object ) {

            var dataCell = ( object.Type === "Source" ) ? $( "td", row )[1] : $( "td", row )[0];

            // Remove open inline edit widgets
            $( ".inlineEdit" ).remove();
            $( ".inlineAdd" ).hide();
            $( ".inlineAddLink" ).css( "visibility", "visible" );

            $( dataCell ).append( $( "#Edit" + object.Type + "Template" ).tmpl( object ) );

            $( ".inlineEdit", dataCell ).css( "top", dataCell.offsetTop + $( dataCell ).parents( "table:first" )[0].offsetTop - $( dataCell ).parents( ".scrollTableBody" )[0].scrollTop ).css( "right", 30 );

        }

    },

    /// Setup make list box change handler (populates models list box)
    initFillModelsSelectAjax: function () {

        $( "#VehicleMakeFields select:first" ).die( "change" ).live( "change", function ( e ) {

            var makeId = $( this ).val(),
                modelsSelect = $( "#VehicleYearModelFields select" ),
                trimsSelect = $( "#VehicleStyleTrimFields select" ),
                json, postData;

            // Clear fields
            $( modelsSelect ).html( "" );
            $( trimsSelect ).html( "" );

            // Handle deselection
            if ( !makeId ) { $( modelsSelect ).attr( "disabled", "disabled" ); $( trimsSelect ).attr( "disabled", "disabled" ); return; }

            // Show loading icon
            $( modelsSelect ).addClass( "loading" );

            // Build post data string
            postData = "{ \"json\": \"{ 'id':'" + makeId + "' }\" }";

            $.ajax( {
                type: "POST",
                url: "Default.aspx/GetModels",
                data: postData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function ( msg ) {

                    var i, options = "";

                    // Hide loading animation
                    $( modelsSelect ).removeClass( "loading" );

                    // If source json not returned, then error occured
                    if ( msg.d === "" ) {
                        $( modelsSelect ).before( "<div class='error'><span>" + MAXSnippetCollector.Messages.GenericAjaxError + "</span></div>" );
                    } else {

                        // Check for error message in return json
                        json = JSON.parse( msg.d );
                        if ( json.error && json.error.length > 0 ) {
                            $( modelsSelect ).before( "<div class='error'><span>" + unescape( json.error ) + "</span></div>" );
                        } else {

                            // Clear Styles/Trims select box
                            $( "#VehicleStyleTrimFields select" ).html( "" );

                            //Enable select box
                            $( modelsSelect ).removeAttr( "disabled" );

                            // Populate select box
                            for ( i = 0; i < json.Table.length; i++ ) {

                                options += "<option value=\"" + json.Table[i].modelId + "\">" + json.Table[i].modelNameAndYear + "</option>";

                            }
                            $( modelsSelect ).html( options );
                            $( trimsSelect ).change();
                        }

                    }

                },

                error: function () {

                    MaxSnippetCollectorPage.showUserMessage( MAXSnippetCollector.MessageTypes.Error, MAXSnippetCollector.Messages.GenericAjaxError );

                }
            } );

            // We're all done
            e.preventDefault();
            e.stopPropagation();
        } );

    },

    /// Setup model list box change handler (populates trims list box)
    initFillTrimsSelectAjax: function () {

        $( "#VehicleYearModelFields select:first" ).die( "change" ).live( "change", function ( e ) {

            var modelIds = $( this ).val(),
                trimSelect, postData;

            // Handle deselection
            if ( !modelIds ) { $( "#VehicleStyleTrimFields select" ).attr( "disabled", "disabled" ).html( "" ); return; }

            // Show loading icon
            trimsSelect = $( "#VehicleStyleTrimFields select" );
            $( trimsSelect ).html( "" );
            $( trimsSelect ).addClass( "loading" );

            // Build post data string
            postData = "{ \"json\": \"{ 'Id':'" + modelIds + "' }\" }";

            $.ajax( {
                type: "POST",
                url: "Default.aspx/GetTrims",
                data: postData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function ( msg ) {

                    var i, j, json, obj, options = "";

                    // Hide loading animation
                    $( trimsSelect ).removeClass( "loading" );

                    // If source json not returned, then error occured
                    if ( msg.d === "" ) {
                        $( trimsSelect ).before( "<div class='error'><span>" + MAXSnippetCollector.Messages.GenericAjaxError + "</span></div>" );
                    } else {

                        // Check for error message in return json
                        json = JSON.parse( msg.d );
                        if ( json.error && json.error.length > 0 ) {
                            $( trimsSelect ).before( "<div class='error'><span>" + unescape( json.error ) + "</span></div>" );
                        } else {

                            if ( $.isArray( json ) ) { // Should always be an array if not an error

                                //Enable select box
                                $( trimsSelect ).removeAttr( "disabled" );

                                for ( j = 0; j < json.length; j++ ) {

                                    //var obj = JSON.parse(json[j]);
                                    obj = json[j];
                                    options += "<option value=\"" + obj.styleID + "\">" + obj.styleName + "</option>";

                                    $( trimsSelect ).html( options );
                                }
                            }

                        }

                    }

                },

                error: function () {

                    MaxSnippetCollectorPage.showUserMessage( MAXSnippetCollector.MessageTypes.Error, MAXSnippetCollector.Messages.GenericAjaxError );

                }
            } );

            // We're all done
            e.preventDefault();
            e.stopPropagation();
        } );

    },

    /// Setup tag undo delete link click handler
    initUndoDeleteTagAjax: function () {

        $( "#SnippetTags a.undo" ).die( "click" ).live( "click", function ( e ) {

            var itemId, itemText, tr, postData;

            itemId = MAXUtils.parseQuery( $( this ).attr( "href" ), "id" );
            itemText = $( this ).attr( "name" );

            // Show loading icon
            tr = $( this ).parents( "tr:first" );
            $( tr ).html( "<td colspan=\"2\"><div style=\"position:relative;height:16px;\"><div class=\"loading\">&nbsp;</div></div></td>" );

            // Build post data string
            postData = "{ \"json\": \"{ 'id':'" + itemId + "', 'tag':'" + itemText + "' }\" }";

            $.ajax( {
                type: "POST",
                url: "Default.aspx/UndoTagDelete",
                data: postData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function ( msg ) {

                    // Hide loading animation
                    $( tr ).find( "div.loading" ).remove();

                    // If source json not returned, then error occured
                    if ( msg.d === "" ) {
                        $( tr ).find( "td" ).append( "<div class='error'><span>" + MAXSnippetCollector.Messages.GenericAjaxError + "</span></div>" );
                    } else {

                        // Check for error message in return json
                        var json = JSON.parse( msg.d );
                        if ( json.error && json.error.length > 0 ) {
                            $( tr ).find( "td" ).append( "<div class='error'><span>" + unescape( json.error ) + "</span></div>" );
                        } else {
                            MAXSnippetCollector.AddRecord( msg.d, $( tr ).parents( ".panel" ), "tag" );
                            $( tr ).remove();
                        }

                    }

                },

                error: function () {

                    MaxSnippetCollectorPage.showUserMessage( MAXSnippetCollector.MessageTypes.Error, MAXSnippetCollector.Messages.GenericAjaxError );

                }
            } );

            // We're all done
            e.preventDefault();
            e.stopPropagation();

        } );

    },

    /// Setup source/tag inline forms
    initInlineAddLinks: function () {

        // Setup inline add '+ Add New' links
        $( ".inlineAddLink" ).bind( "click", function ( e ) {

            // Hide other inlinePanels (and show link)
            $( ".inlineEdit" ).remove();
            $( ".inlineAdd" ).hide();
            $( ".inlineAddLink" ).css( "visibility", "visible" );

            // Hide target link and show inlineAdd panel
            $( this ).css( "visibility", "hidden" );
            $( this ).parents( ".inlineAddContainer" ).find( ".inlineAdd" ).show();

            // Set focus
            $( this ).parents( ".inlineAddContainer" ).find( ".inlineAdd input:text:first" ).focus();

            // We're all done
            e.preventDefault();
            e.stopPropagation();

        } );

        // Setup inline add '+ Close' links
        $( ".inlineAdd a.close" ).die( "click" ).live( "click", function ( e ) {

            var panel = $( this ).parents( ".inlineAddContainer" );

            // Show link, hide inlineAdd panel and reset form
            $( panel ).find( ".inlineAddLink" ).css( "visibility", "visible" );
            $( panel ).find( ".inlineAdd, .error span" ).hide();
            $( panel ).find( "input:text" ).val( "" );
            $( panel ).find( "input:checkbox" ).removeAttr( "checked" );

            // We're all done
            e.preventDefault();
            e.stopPropagation();

        } );

    },

    /// Setup trim list box change handler (populates snippets list)
    initSnippetListFilter: function () {

        // Clear make/model/trim selections
        $( "#SearchSnippets select" ).val( "" );

        $( "#VehicleStyleTrimFields select" ).die( "change" ).live( "change", function () {

            var styleIds = $( this ).val(),
                postData = "{ \"json\": \"{ 'ids':'" + styleIds + "' }\" }",
                emptyListDiv = $( "#EmptySnippetsList" ),
                snippetTableDiv = $( "#SnippetTableDiv" ),
                results;

            // Remove current results
            $( snippetTableDiv ).find( "div.scrollTableHead, div.scrollTableBody" ).remove();

            // Handle trim deselection
            if ( styleIds === null || styleIds.length === 0 ) {
                $( emptyListDiv ).html( "<p>" + MAXSnippetCollector.Messages.DefaultListHelper + "</p>" );
                $( emptyListDiv ).show();
                return;
            }

            // Show loading animation
            $( emptyListDiv ).html( "" );
            $( snippetTableDiv ).css( "background", "url(/merchandising/App_Themes/MAX_2.0/Images/icon_Loading.gif) no-repeat center 120px transparent" );

            $.ajax( {
                type: "POST",
                url: "Default.aspx/GetSnippetsByStyle",
                data: postData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function ( msg ) {

                    // If source json not returned, then error occured
                    if ( msg.d === "" ) {
                        $( emptyListDiv ).html( "<div class='error'><span>" + MAXSnippetCollector.Messages.GenericAjaxError + "</span></div>" );
                    } else {

                        // Check for error message in return json
                        var json = JSON.parse( msg.d );
                        if ( json.error && json.error.length > 0 ) {
                            $( emptyListDiv ).html( "<div class='error'><span>" + unescape( json.error ) + "</span></div>" );
                        } else {

                            // Hide loading icon
                            $( snippetTableDiv ).css( "background", "none" );

                            // Display results or 'no results' message
                            if ( json === null || json.Snippets === undefined || json.Snippets.length === 0 ) {
                                $( emptyListDiv ).html( "<p>" + MAXSnippetCollector.Messages.NoSnippetsFound + "</p>" );
                            } else {
                                $( emptyListDiv ).hide();
                                $( emptyListDiv ).before( $( "#SnippetsListTemplate" ).tmpl( json ) );
                            }
                        }

                    }

                }
            } );

        } );

    },

    /// Setup add/edit snippet submit button enabling
    initSubmitEnable: function () {

        $( "form" ).unbind( "change" ).bind( "change", function () {

            var submitButton = $( "#AddSnippetPageButtons input:submit" ),
                snippetEntered = $( "#SnippetTextFields textarea" ).val().length > 3,
                sourceSelected = $( "#SnippetSourceFields select option:selected" ).val() === undefined ? false : $( "#SnippetSourceFields select option:selected" ).attr( "value" ).length > 0,
                styleSelected = $( "#VehicleStyleTrimFields select option:selected" ).length > 0;

            if ( snippetEntered && sourceSelected && styleSelected ) {
                $( submitButton ).removeAttr( "disabled" );
            } else {
                $( submitButton ).attr( "disabled", "disabled" );
            }

        } );

    },

    /// Displays message in user messaging area above form.
    /// Requires a message type (MAXSnippetCollector.MessageTypes or string)
    /// and a message (MAXSnippetCollector.Messages or string)
    showUserMessage: function ( type, text ) {

        var messageContainer = $( "#UserMessaging" );

        // Reset message container
        $( messageContainer ).html("");

        if ( text.length > 0 ) {

            $( "<div />" ).addClass( type ).append( "<span>" + text + "</span>" ).appendTo( messageContainer );

        }

    }

};

var MaxSnippetCollectorPage;
$( document ).ready( function () {
    MaxSnippetCollectorPage = new MAXSnippetCollector();
    MaxSnippetCollectorPage.onReady();
} );

var Source = function ( copyrighted, id, name ) {

    this.Copyrighted = copyrighted;
    this.Id = id;
    this.Name = name;
    this.Type = "Source";

};

var Tag = function ( id, name ) {

    this.Id = id;
    this.Name = name;
    this.Type = "Tag";

};