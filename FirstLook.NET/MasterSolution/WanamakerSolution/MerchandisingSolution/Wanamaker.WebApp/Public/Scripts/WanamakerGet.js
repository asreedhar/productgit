/////////////////////////////
// DEPENDENCIES
// + LIBRARIES
//     jQuery
//     jQuery UI
//     jQuery UI plugin: monthpicker
//     KnockoutJS
//     underscore
//     date.format
//     json2
// + GLOBAL VARS
//     api_token : string (UUID)
/////////////////////////////


// knockout extensions

ko.extenders.toggle = function( ko_obsv, state ) {
	ko_obsv( state );
	ko_obsv.toggle = function() {
		ko_obsv( ! ko_obsv() );
	}
	return ko_obsv;
}

// view models

var LinkedBudget = function( month, value ) {
	var self = this;
	self.month = month;
	self.value = ko.observable( value );

	self.soft = ko.observable( true ); // true: system-implied, false: user-specified
	///
	self.next = ko.observable( null ); // optional; set externally
	self.previous = ko.observable( null ); // optional; set externally
	self.lock = ko.observable( false ); // true: ignore next value modification with respect to soft status
	self.value.subscribe( function( new_value ) {
		if( new_value == '' ) { // value reset
			var previous = self.previous();
			if( previous ) {
				new_value = previous.value();
				self.value( new_value );
			}
			self.soft( true ); // enable soft status
			self.lock( true );
		}
		if( ! self.lock() )
			self.soft( false ); // soft flag disabled (value set by user)
		else // value set by system via implicit-chain
			self.lock( false );
		var next = self.next(); // implicit value propagation
		if( next && next.soft() == true ) {
			next.lock( true );
			next.value( new_value );
		}
	});
}

var DatedVDP = function( month, used_value, new_value ) {
	var self = this;
	self.month = month;
	self.used_value = ko.observable( used_value );
	self.new_value = ko.observable( new_value );
}

var DealerVDPData = function( website_provider_id ) {
	var self = this;
	
	self.website_provider_id = website_provider_id;
	self.placeholder = ( website_provider_id == null );
	
	self.username = ko.observable( '' );
	self.password = ko.observable( '' );

	self.editing_password = ko.observable( false );
	self.editing_password.subscribe( function( new_value ) {
		if( new_value === true )
			self.password( '' );
	});

	self.vdps = get_applicable_months();
	for( var i = 0; i < self.vdps.length; ++i )
		self.vdps[i] = new DatedVDP( self.vdps[i], '', '' );

	self.clear_providers = function() {
		self.dealer_website_providers( [] );
	}

	self.clear_vdps = function() {
		for( var i = 0; i < self.vdps.length; ++i ) {
			var v = self.vdps[i];
			v.used_value( '' );
			v.new_value( '' );
		}
	}

	self.data_payload = ko.computed( function() {
		var filtered_vdps = _.filter( self.vdps, function( item ) {
			return item.used_value() != '' || item.new_value() != '';
		});
		var data = {
			website_provider_id: self.website_provider_id,
			username: self.username(),
			password: self.password(),
			vdps: _.map( filtered_vdps, function( item ) {
				return {
					month: item.month,
					used_value: parse_integer( item.used_value() ),
					new_value: parse_integer( item.new_value() )
				}
			})
		};
		return data;
	});

	self.data_changed = ko.observable( false );
	self.data_payload.subscribe( function( new_value ) {
		self.data_changed( true );
	});
}

////

var DealerListingSite = function( id, name ) {
	var self = this;
	self.site_id = id;
	self.site_name = name;

	self.expanded = ko.observable().extend({ toggle:false });

	self.username = ko.observable( '' );
	self.password = ko.observable( '' );
	self.dealer_id = ko.observable( '' );

	self.editing_password = ko.observable( false );
	self.editing_password.subscribe( function( new_value ) {
		if( new_value === true )
			self.password( '' );
	});

	self.monthly_cost = get_applicable_months();
	for( var i = 0; i < self.monthly_cost.length; ++i ) {
		self.monthly_cost[i] = new LinkedBudget( self.monthly_cost[i], '' );
		if( i > 0 ) { // display order is chronologically reversed, so next means previous in sequence (and vice-versa)
			self.monthly_cost[i].next( self.monthly_cost[i-1] );
			self.monthly_cost[i-1].previous( self.monthly_cost[i] );
		}
	}
	self.monthly_cost_seed = new LinkedBudget( null, '' ); // no timestamp association
	var oldest = self.monthly_cost[self.monthly_cost.length-1];
	self.monthly_cost_seed.next( oldest ); // links forward to oldest displayed budget item
	oldest.previous( self.monthly_cost_seed );

	self.clear_monthly_cost = function() {
		for( var i = 0; i < self.monthly_cost.length; ++i ) {
			var mc = self.monthly_cost[i];
			mc.soft( true );
			mc.lock( true );
			mc.value( '' );
		}
	}

	// self.selected_month = ko.observable( null );
	self.selected_month = ko.observable( self.monthly_cost[0].month.format('mmm yyyy') );
	
	self.selected_month_obj = ko.computed( function() {
		var selected_month = parse_dateFormat_month_ts( self.selected_month() );
		if( !selected_month )
			return null;
		var dv_pair = _.detect( self.monthly_cost, function( item ) {
			return selected_month.getTime() == item.month.getTime();
		});
		if( dv_pair )
			return dv_pair;
		else
			return null;
	});

	self.selected_month_value = ko.computed({
		read: function() {
			var selected_month_obj = self.selected_month_obj();
			if( selected_month_obj )
				return selected_month_obj.value();
			else
				return null;
		},
		write: function( new_value ) {
			var selected_month_obj = self.selected_month_obj();
			if( selected_month_obj )
				selected_month_obj.value( new_value );
		}
	});

	self.selected_month_soft = ko.computed( function() {
		var selected_month_obj = self.selected_month_obj();
		if( selected_month_obj )
			return selected_month_obj.soft();
		else
			return false; // default: user-specified
	});

	self.data_payload = ko.computed( function() {
		var monthly_cost_filtered = _.filter( self.monthly_cost, function( item ) {
			return (!item.soft());
		});
		return {
			site_id: self.site_id,
			site_name: self.site_name,
			username: self.username(),
			password: self.password(),
			dealer_id: self.dealer_id(),
			monthly_cost: _.map( monthly_cost_filtered, function( item ) {
				return {
					month: item.month,
					value: parse_currency( item.value() ),
					soft: false
				}
			})
		};
	});

	self.data_valid = ko.computed( function() {
		return true;
	})
}

var GoogleAnalyticsProfile = function() {
	var self = this;

	self.show_setup_button = ko.observable( true );

	self.is_admin = ko.observable( false );
	self.admin_lock = ko.observable( false );

	self.google_analytics_profiles = ko.observableArray( [] );
	self.compatible_providers = ko.observableArray( [] );
	self.selected_profile_id = ko.observable( -1 );
	self.associated_provider_id = ko.observable( -1 );
	self.selected_profile_name = ko.computed( function() {
		var item = _.find( self.google_analytics_profiles(), function( item ) {
			return item.Id == self.selected_profile_id();
		});
		if( item )
			return item.Name;
		else
			return null;
	});

	self.selected_profile_id.subscribe( function() {
		if( self.is_admin() )
			self.admin_lock( true ); // profile changed, lock it (if admin)
	});

	self.data_payload = ko.computed( function() {
		if( self.show_setup_button() )
			return null;
		else // showing profile select
			return {
				selected_profile_id: self.selected_profile_id(),
				selected_profile_name: self.selected_profile_name(),
				associated_provider_id: self.associated_provider_id(),
				admin_lock: self.admin_lock()
			}
	});

	self.data_valid = ko.computed( function() {
		return true;
	});
}

var DealerVehicleDetailPage = function() {
	var self = this;

	self.expanded = ko.observable().extend({ toggle:false });

	self.dealer_website_providers = ko.observableArray();
	self.dealer_website_provider_id = ko.observable( 0 );
	self.dealer_website_provider_freeform_name = ko.observable( '' );

	self.vdp_data = ko.observableArray();

	self.selected_vdp_data = ko.computed( function() {
		var vdp_data = _.find( self.vdp_data(), function( item ) {
			return item.website_provider_id == self.dealer_website_provider_id();
		});
		if( vdp_data )
			return vdp_data;
		else
			return null;
	});
	self.selected_vdp_data.subscribe( function( new_value ) {
		setTimeout( position_custom_components, 1 );
	});

	self.data_payload = ko.computed( function() {
		var freeform_name = self.dealer_website_provider_id() == -1 ? self.dealer_website_provider_freeform_name() : null;
		if( freeform_name != null )
			freeform_name = $.trim( freeform_name );
		if( freeform_name == '' )
			freeform_name = null;
		var data = {
			dealer_website_provider_id: self.dealer_website_provider_id(),
			dealer_website_provider_freeform_name: freeform_name,
			vdp_data: []
		};
		_.each( self.vdp_data(), function( item ) {
			if( item.data_changed() )
				data.vdp_data.push( item.data_payload() );
		});
		return data;
	});

	self.data_valid = ko.computed( function() {
		var id = self.dealer_website_provider_id();
		var ffn = self.dealer_website_provider_freeform_name();
		return id != -1 || (ffn != null && ffn != '');
	})
}

var DealershipSegment = function() {
	var self = this;

	self.segment = ko.observable( 0 );

	self.data_payload = ko.computed( function() {
		return {
			segment: parseInt( self.segment() )
		}
	});

	self.data_valid = ko.computed( function() {
		return true;
	})
}

// root view model

var ViewModel = function( api_token ) {
	var self = this;

	self.api_token = api_token;

	self.loading = ko.observable( false );
	
	// sections
	self.dealer_listing_sites = [ 
		new DealerListingSite( 2, 'AutoTrader.com' ),
		new DealerListingSite( 1, 'Cars.com' )
	];
	self.google_analytics = new GoogleAnalyticsProfile();
	self.dealer_vdps = new DealerVehicleDetailPage();
	self.dealership_segment = new DealershipSegment();
	
	// meta
	self.busy = ko.observable( false );
	self.data_changed = ko.observable( false );
	self.data_valid = ko.computed( function() {
		var is_valid = true;
		_.each( self.dealer_listing_sites, function( site ) {
			is_valid = is_valid && site.data_valid();
		});
		is_valid = is_valid && self.google_analytics.data_valid();
		is_valid = is_valid && self.dealer_vdps.data_valid();
		is_valid = is_valid && self.dealership_segment.data_valid();
		return is_valid;
	});
	self.can_save = ko.computed( function() {
		return self.data_changed() && self.data_valid() && !self.busy();
	});
	// self.can_save.subscribe( function( new_value ) {
	// 	console.debug( 'can_save => '+new_value );
	// 	console.trace();
	// });

	_.each( self.dealer_listing_sites, function( site ) {
		site.data_payload.subscribe( function( new_value ) {
			self.data_changed( true );
		});
	});
	self.google_analytics.data_payload.subscribe( function( new_value ) {
		self.data_changed( true );
	});
	self.dealer_vdps.data_payload.subscribe( function( new_value ) {
		self.data_changed( true );
	});
	self.dealership_segment.data_payload.subscribe( function( new_value ) {
		self.data_changed( true );
	});

	self.save = function() {
		if( ! self.can_save() )
			return;
		self.busy( true );
		var transmit_data = {
			dealer_listing_sites: [],
			google_analytics: null,
			dealer_vdps: null,
			dealership_segment: null
		};
		_.each( self.dealer_listing_sites, function( site ) {
			transmit_data.dealer_listing_sites.push( site.data_payload() );
		});
		transmit_data.google_analytics = self.google_analytics.data_payload();
		transmit_data.dealer_vdps = self.dealer_vdps.data_payload();
		transmit_data.dealership_segment = self.dealership_segment.data_payload();
		var transmit_data_json = JSON.stringify( transmit_data );
		////
		$.ajax({
			url: '/merchandising/DigitalPerformanceAnalysis.aspx/Save' + '?api_token='+self.api_token,
			type: 'POST',
			data: {
				data: transmit_data_json
			},
			success: function( data, textStatus, jqXHR ) {
				// presence and ordering of the following is important
				_.each( self.dealer_vdps.vdp_data(), function( vm_vdp ) {
					vm_vdp.data_changed( false );
				});
				self.data_changed( false );
			},
			error: function( jqXHR, textStatus, errorThrown ) {
			},
			complete: function( jqXHR, textStatus ) {
				self.busy( false );
			}
		});
	}

	self.load = function() {
		self.loading( true );
		$.ajax({
			url: '/merchandising/DigitalPerformanceAnalysis.aspx/Load' + '?api_token='+self.api_token,
			type: 'GET',
			dataType: 'json',
			success: function( data, textStatus, jqXHR ) {
				if( data ) {
					if( data.dealer_listing_sites ) {
						_.each( data.dealer_listing_sites, function( load_dls ) {
							var vm_dls = _.find( self.dealer_listing_sites, function( vm_dls ) {
								return vm_dls.site_id == load_dls.site_id;
							});
							if( !vm_dls )
								return;
							vm_dls.username( load_dls.username );
							vm_dls.password( load_dls.password );
							vm_dls.dealer_id( load_dls.dealer_id );
							vm_dls.clear_monthly_cost();
							if( load_dls.monthly_cost ) {
								_.each( load_dls.monthly_cost, function( load_mc ) {
									var load_t = parse_iso_ts( load_mc.month );
									var vm_mc_slot = _.find( vm_dls.monthly_cost, function( vm_mc ) {
										return vm_mc.month.getTime() == load_t.getTime();
									});
									if( ! vm_mc_slot )
										return;
									vm_mc_slot.lock( load_mc.soft );
									vm_mc_slot.value( load_mc.value );
								});
								var oldest_slot_ts = vm_dls.monthly_cost[vm_dls.monthly_cost.length-1].month;
								var potential_seeds = _.filter( load_dls.monthly_cost, function( load_mc ) {
									var load_t = parse_iso_ts( load_mc.month );
									if( load_t.getTime() < oldest_slot_ts.getTime() )
										return true;
								});
								if( potential_seeds.length > 0 ) {
									var most_recent_seed_budget = _.max( potential_seeds, function( load_mc ) {
										var load_t = parse_iso_ts( load_mc.month );
										return load_t.getTime();
									});
									vm_dls.monthly_cost_seed.value( most_recent_seed_budget.value );
								}
							}
						});
					}
					if( data.google_analytics ) {
						self.google_analytics.google_analytics_profiles( data.google_analytics.google_analytics_profiles );
						self.google_analytics.compatible_providers( data.google_analytics.compatible_providers );
						self.google_analytics.selected_profile_id( data.google_analytics.selected_profile_id );
						self.google_analytics.associated_provider_id( data.google_analytics.associated_provider_id );
						self.google_analytics.is_admin( data.google_analytics.is_admin );
						self.google_analytics.admin_lock( data.google_analytics.admin_lock );
						self.google_analytics.show_setup_button( false ); // profiles loaded! change mode
					}
					if( data.dealer_vdps ) {
						var load_vdps = data.dealer_vdps;
						var vm_vdps = self.dealer_vdps;
						if( load_vdps.dealer_website_providers ) {
							_.each( load_vdps.dealer_website_providers, function( load_p ) {
								if( load_p.id > 0 ) { // curated
									vm_vdps.dealer_website_providers.push( load_p );
									vm_vdps.vdp_data.push( new DealerVDPData( load_p.id ));
								} else { // freeform
									vm_vdps.dealer_website_provider_freeform_name( load_p.name );
								}
							});
							vm_vdps.dealer_website_providers.push({ name: 'Other', id: -1 });
							vm_vdps.vdp_data.push( new DealerVDPData( -1 ));
						}
						if( load_vdps.dealer_website_provider_id != 0 )
							vm_vdps.dealer_website_provider_id( load_vdps.dealer_website_provider_id > 0 ? load_vdps.dealer_website_provider_id : -1 );
						_.each( load_vdps.vdp_data, function( vdp_data ) {
							vm_vdp_data = _.find( vm_vdps.vdp_data(), function( vm_vdp ) {
								return (vm_vdp.website_provider_id == vdp_data.website_provider_id)
									||   (vdp_data.website_provider_id < 0 && vm_vdp.website_provider_id == -1);
							});
							if( vm_vdp_data == null )
								return; // skip
							vm_vdp_data.username( vdp_data.username );
							vm_vdp_data.password( vdp_data.password );
							if( vdp_data.vdps ) {
								_.each( vdp_data.vdps, function( load_vdp ) {
									var load_t = parse_iso_ts( load_vdp.month );
									var vm_vdp_slot = _.find( vm_vdp_data.vdps, function( vm_mc ) {
										return vm_mc.month.getTime() == load_t.getTime();
									});
									if( vm_vdp_slot ) {
										vm_vdp_slot.used_value( load_vdp.used_value );
										vm_vdp_slot.new_value( load_vdp.new_value );
									}
								});
							}
						});
					}
					if( data.dealership_segment ) {
						self.dealership_segment.segment( data.dealership_segment.segment );
					}
					// presence and ordering of the following is important
					_.each( self.dealer_vdps.vdp_data(), function( vm_vdp ) {
						vm_vdp.data_changed( false );
					});
					self.data_changed( false );
				}
			},
			error: function( jqXHR, textStatus, errorThrown ) {
			},
			complete: function( jqXHR, textStatus ) {
				self.loading( false );
				position_custom_components();
			}
		});
	}

};

// utility functions

function init_jqui_components() {
	$('.monthpicker').monthpicker({
		dateFormat: 'M yy'
	});
	$('#ui-monthpicker-div').css({ display: 'none' });// fix for 3rd-party bug
}

function position_custom_components() {
}

function get_applicable_months() {
	var now = new Date();
	var m = [];
	for( var i = 0; i < 13; ++i )
		m.push( new Date( now.getFullYear(), now.getMonth() - i, 1 ));
	return m;
}

function is_number(n) {
  return !isNaN(parseFloat(n)) && isFinite(n)
}

function parse_iso_ts( ts_str ) {
	return new Date( ts_str.substring(0,10).replace(/-/g,'/'));
}

function parse_dateFormat_month_ts( m_ts_str ) {
	return new Date( m_ts_str.substring(4,8)+'/'+(_.indexOf(dateFormat.i18n.monthNames,m_ts_str.substring(0,3))+1)+'/01' );
}

function parse_integer( str_value ) {
	if( ! str_value )
		return 0;
	return is_number( str_value ) ? parseInt( str_value ) : 0;
}

function parse_currency( str_value ) {
	if( typeof str_value == 'undefined' || str_value == null )
		return 0;
	if( typeof str_value != 'string' )
		str_value = str_value.toString();
	var sanitized_value = str_value.replace( '$', '' );
	return is_number( sanitized_value ) ? Math.floor( parseFloat( sanitized_value )) : 0;
}

//////////////////////////
// main 

var ko_vm = new ViewModel( api_token );
ko.applyBindings( ko_vm );

$.ajaxSetup({
	cache: false
});
init_jqui_components();
position_custom_components();

ko_vm.load();

