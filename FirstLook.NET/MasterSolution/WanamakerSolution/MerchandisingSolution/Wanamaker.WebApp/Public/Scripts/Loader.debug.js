﻿// JScript File
window.onload = function initialLoad(){  
     updateOrientation();  
} 

window.onbeforeunload = confirmExit;

//quick hack, IE doesn't support addEventListener
if(window.addEventListener) {
    window.addEventListener("load", function() { setTimeout(loaded, 100) }, false);
} else {
    //the IE part
    window.attachEvent("onload", function() { setTimeout(loaded, 100) });
}

function loaded() {
    var obj = $get("mobileBuilderBody");
    if(obj) {
        obj.style.visibility = "visible";
    }   
    //$get("mobileBuilderBody").style.visibility = "visible";
    window.scrollTo(0, 1); // pan to the bottom, hides the location bar  
}

function updateOrientation() {  

   var contentType = "show_";  
   switch(window.orientation){  
       case 0:  
       
       contentType += "normal";  
       break;  
 
       case -90:  
       contentType += "side";  
       break;  
 
       case 90:  
       contentType += "side";  
       break;  
 
       case 180:  
       contentType += "normal";  
       break;  
   }
   
   var obj = $get("mobileBuilderBody");
   if(obj) {
    obj.setAttribute("class", contentType);  
   }
    
}  


var isMenuOpen = false;
function menuOpen() {

    /*if (isMenuOpen) {
        window.scrollBy(20,0);
        if (window.scrollX < 320) {
            setTimeout('menuOpen()',20);
        }
    }*/
}
function menuClose() {
    /*if (!isMenuOpen) {
        window.scrollBy(-20,0);
        if (window.scrollX > 0) {
            setTimeout('menuClose()',50);
        }
    }*/
}
function toggleStdMenu(retVal) {
    var menu = $get('MenuPanel');
    var mainPanel = $get('LoadUpdatePanel');
    if (menu.style.display != 'block') {
        isMenuOpen = true;
        menu.style.display = 'block';
        mainPanel.className = 'menuOpen';
        menuOpen();
    }else {
        isMenuOpen = false;
        menu.style.display = 'none';
        mainPanel.className = '';
        menuClose();
    }
    return retVal;

}

function setAllCondValue(conditionValue) {
    var el = $get('conditions');
    var divs = el.getElementsByTagName("DIV");
    var inpNdx;
    switch (conditionValue) {
        case 1:
            inpNdx = 4;
            break;
        case 2:
            inpNdx = 3;
            break;
        case 3:
            inpNdx = 2;
            break;
    }
        
    for (var i=0; i<divs.length; i++) {
        var inps = divs[i].getElementsByTagName("INPUT");
        if (inps.length < 5) {
            return;
        }
        
        var typeVal = inps[1].value;
        var condVal = 1;
        var inpToSelect;
        
        setConditionValue(inps[inpNdx], conditionValue, typeVal);
        
    }
}

function setConditionValue(theImage, conditionValue, conditionId) {
    
    //reset the images in the container
    var container = theImage.parentNode.parentNode;
    for (var i=0; i<container.childNodes.length; i++) {
        var node = container.childNodes[i];
        
        if (node.id != undefined && node.id.indexOf('ConditionLevel') > -1) {
            node.value = conditionValue;
        }
        else if (node.nodeName == "A") {
            
            node.getElementsByTagName("INPUT")[0].className = "";
            
        }
    }
    theImage.className = 'Active';
    
    //setDirty();
    return false;
    
}
function setDirty() {
    var but = $get('ButtonsDiv');
    but.style.display = '';
    var img = $get('SaveOKPlaceholder');
    img.style.display = 'none';
    $get('dirtyFlag').value = '1';
}
function checkItem(itemId, displayItemId) {
    var item = document.getElementById(itemId);
    var displayItem = document.getElementById(displayItemId);
    
    if (item != 'undefined' && displayItem != 'undefined') {
        item.checked = !item.checked;
        if (item.checked) {
            displayItem.className = 'checkedHolder';
        }else {
            displayItem.className = 'checkHolder';
        }
        //setDirty();
    }
    return false;
}

  function confirmExit(evt)
  {
    
      // check to see if any changes to the data entry fields have been made
      var el = $get('dirtyFlag');
      if (el.value == '1') {
        return 'Any changes made without clicking the Save button will be lost.  \n\nDiscard changes?';
      }
      
      // no changes - return nothing      
    
  }
      

  function cleanRecord() {
    // check to see if any changes to the data entry fields have been made
    var el = $get('dirtyFlag');
    el.value = '0';
    return true;
  }
  