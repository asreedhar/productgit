﻿function Gauge() {
    var self = this;

    var barWidth = 7;
    var barHeight = 55;
    var riskHeight = 6;
    var baseColour = "#0186BE";

    var container = document.getElementById('gauge');
    var riskContainer = document.getElementById('gaugeRiskIndicator');
    
    var handle = $('.dragdealer#gauge');
    handle.top = container.top;
    self.SliderMoved = [];
    self.ColorLuminance= function (hex, lum) {

        // validate hex string
        hex = String(hex).replace(/[^0-9a-f]/gi, '');
        if (hex.length < 6) {
            hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
        }
        lum = lum || 0;

        // convert to decimal and change luminosity
        var rgb = "#", c, i;
        for (i = 0; i < 3; i++) {
            c = parseInt(hex.substr(i * 2, 2), 16);
            c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
            rgb += ("00" + c).substr(c.length);
        }

        return rgb;
    }

    self.createGauge = function (data) {
        
        self.Destroy();
        self.container = container;
        self.MaxValue = data.MaxValue;
        self.MinValue = data.MinValue;
        self.AvgValue = data.AvgValue;
        self.LowPercentage = data.LowPercentage;
        self.HighPercentage = data.HighPercentage;


        self.ContainerWidth = parseInt(container.style.width.replace("px", "")) - 10;
        self.PerPercentageValue = ((self.MaxValue - self.MinValue) / 100);
        self.PixelPercentage = ((self.ContainerWidth) / 100);
        self.ValuePerPixel = (parseFloat(self.MaxValue - self.MinValue) / parseFloat(self.ContainerWidth - 10));  //Math.round((self.PerPercentageValue / self.PixelPercentage));


        var paper = new Raphael(container, self.ContainerWidth - 2, barHeight);
        var colour;
        var colourstack = [];
        for (var i = 10; i < self.ContainerWidth; i = i + barWidth) {
            var path = "M " + i.toString() + " " + barWidth.toString() + " L" + i.toString() + " " + barHeight.toString() + " L" + (parseInt(i) + parseInt(barWidth)).toString() + " " + barHeight.toString() + " L" + (parseInt(i) + parseInt(barWidth)).toString() + " " + barWidth.toString() + " Z";
            if (i < (self.ContainerWidth / 3)) {
                colour = self.ColorLuminance(baseColour, i / barWidth / 100);
                colourstack.push(colour);
            } else {
                if (i < (self.ContainerWidth / 2)) {
                    colour = self.ColorLuminance(baseColour, i / barWidth / 100);
                    colourstack.push(colour);
                } else {
                    colour = colourstack.pop();
                }
            }

            if (colour == null || colour == undefined)
                colour = baseColour;

            paper.path(path).attr({ "fill": colour, "stroke": "#1272CE", "stroke-width": "1" });

        }

        var riskPaper = new Raphael(riskContainer, self.ContainerWidth - 11, riskHeight);

        self.RiskValue = self.ContainerWidth / (self.HighPercentage - self.LowPercentage);


        for (var i = 0; i < data.RedRisk.length; i++) {
            self.StartPoint = self.PixelFromPercent(data.RedRisk[i].MinValue - 1);
            self.RiskWidth = self.PixelFromPercent(data.RedRisk[i].Maxvalue) - self.StartPoint;
            riskPaper.rect(self.StartPoint, 0, self.RiskWidth, riskHeight).attr({ "fill": "#EA2B2B", "stroke": "#EA2B2B", "stroke-width": "1" });
        }
        for (var i = 0; i < data.GreenRisk.length; i++) {
            self.StartPoint = self.PixelFromPercent(data.GreenRisk[i].MinValue - 1);
            self.RiskWidth = self.PixelFromPercent(data.GreenRisk[i].Maxvalue) - self.StartPoint;
            riskPaper.rect(self.StartPoint, 0, self.RiskWidth, riskHeight).attr({ "fill": "#2BC32B", "stroke": "#2BC32B", "stroke-width": "1" });
        }
        for (var i = 0; i < data.YellowRisk.length; i++) {
            self.StartPoint = self.PixelFromPercent(data.YellowRisk[i].MinValue - 1);
            self.RiskWidth = self.PixelFromPercent(data.YellowRisk[i].Maxvalue) - self.StartPoint;
            riskPaper.rect(self.StartPoint, 0, self.RiskWidth, riskHeight).attr({ "fill": "#EAEA2B", "stroke": "#EAEA2B", "stroke-width": "1" });
        }

        self.dragdealer = new Dragdealer($('.dragdealer#gauge')[0], {
            slide: false,
            speed: 100, // speed must be 100 if slide is false for the setValue() function to work, for some reason                        
            animationCallback: function (x, y) {
                if (!this.dragging) return; // suppress normal behavior if value was changed via dragdealer API
                var xpos = $('#handle').position().left;
                var price = self.price_from_position(xpos);
                self.update_handle_label_position(this, xpos);
                self.update_label_text(price);
                $('#ListPrice').val(price);
                for (var i = 0; i < self.SliderMoved.length; i++) {
                    self.SliderMoved[i](self.price_from_position(xpos));
                }
            }
        });

    }
    
    if (!self.dragdealer) {
        self.price_from_position = function (x) {
            if (x == 0)
                return self.MinValue;
            if (x == self.ContainerWidth)
                return self.MaxValue;
            //x = x == 0 ? 1 : x;
            return Math.round((x * self.ValuePerPixel) + parseFloat(self.MinValue)); // round to one dollar less than nearest ten dollar increment
        }
        self.position_from_price = function (price) {            
            return (price - self.MinValue)<0?0:Math.round((price-self.MinValue) / self.ValuePerPixel);
        }
        self.update_handle_label_position = function (dragdealer_obj, x) {

            var gaugeContainer = document.getElementById('handle');
            if (x > self.ContainerWidth) {
                x = self.ContainerWidth;
            }
            if (x < 0) {
                x = 0;
            }

            var label_offset = $('#handle').position().left - (parseInt($('#sliderLabel').width()) / 2) + 23; //x - (parseInt($('#sliderLabel').width()) / 2) + 17;
            if (label_offset < 0) {

                if (!$("#sliderLabel").hasClass("gauge-callout-style-el")) {
                    $("#sliderLabel").toggleClass("gauge-callout-style-el");
                }

                label_offset = parseInt(gaugeContainer.style.left.replace("px", ""));

            }
            else if ((label_offset + 160) > (self.ContainerWidth + 26)) {
                if (!$("#sliderLabel").hasClass("gauge-callout-style-er")) {
                    $("#sliderLabel").toggleClass("gauge-callout-style-er");
                }
             
                label_offset = parseInt(gaugeContainer.style.left.replace("px", "")) - 136;

            }
            else {
                if ($("#sliderLabel").hasClass("gauge-callout-style-el")) {
                    $("#sliderLabel").toggleClass("gauge-callout-style-el");
                }
                else if ($("#sliderLabel").hasClass("gauge-callout-style-er")) {
                    $("#sliderLabel").toggleClass("gauge-callout-style-er");
                }
                else {
                }

            }
            $('#sliderLabel').css({ 'left': label_offset + 'px' });

        }
        self.UpdateHandlePosition=function(xpos){
            var handle_offset = xpos>self.ContainerWidth-10?self.ContainerWidth-10:xpos;
            $('.dragdealer .handle').css({ 'left': handle_offset + 'px' });
        }
        self.update_label_text=function(price) {
            $('#gaugeCalloutText').text('$' + self.numberWithCommas(price));
            $('#gaugeCalloutPercentage').text('(' + self.SetMarketAverage(price) + '%)');
        }


        self.clicked = function () {
            var x = $('#handle').position().left; //- ( parseFloat($('#handle').width())/2);
            var price = self.price_from_position(x);
            if (price >= self.MinValue && price <= self.MaxValue) {
                self.update_handle_label_position(self.dragdealer, x);
                self.update_label_text(price);
            }
            return price;
        };

        self.update_slider = function (price) {
            var x;
            if (price < self.MinValue) {  //For Low
                $('#ListPrice').val(self.MinValue);
                 x = self.position_from_price(self.MinValue);
                self.UpdateHandlePosition(x);
                self.update_handle_label_position(self.dragdealer, x);
                if ($('#internetPriceEdit').val() == 'Y')
                    self.update_label_text(price);
                else
                    self.update_label_text(self.MinValue);
            } else if (price > self.MaxValue) {
                $('#ListPrice').val(self.MaxValue); //For High
                 x = self.position_from_price(self.MaxValue);
                self.UpdateHandlePosition(x);
                self.update_handle_label_position(self.dragdealer, x);
                if ($('#internetPriceEdit').val() == 'Y')
                    self.update_label_text(price);
                else
                    self.update_label_text(self.MaxValue);
            } else {
                $('#ListPrice').val(price);
                 x = self.position_from_price(price);
                self.UpdateHandlePosition(x);
                self.update_handle_label_position(self.dragdealer, x);
                self.update_label_text(price);
            }
           
           };

        self.SetMarketAverage = function(price) {
            percent = self.AvgValue==0?0:(price / self.AvgValue) * 100;
            return Math.round(percent);
        };

        self.PriceFromPercent = function (percent) {            
            return Math.round((percent * self.AvgValue)/100);
        };

        self.PixelFromPrice = function (price) {
            if((price - self.MinValue)==0) return 0;
            return Math.round((price - self.MinValue) / self.ValuePerPixel); // round to one dollar less than nearest ten dollar increment
        };
        self.PixelFromPercent = function (percent) {
            return self.PixelFromPrice(self.PriceFromPercent(percent));
        };

        self.enable_slider = function() {
            self.dragdealer.enable();
        };
        self.disable_slider = function() {

            self.dragdealer.disable();

        };
		self.numberWithCommas = function(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        };
        self.Destroy = function () {
            // cleanup
            $('#gauge .handle').css({ 'left': '0px' });
            $('#gauge').contents(':not(.handle)').remove();
            //$(' > *').remove();
            $('#gaugeRiskIndicator > *').remove();
            self.dragdealer = null;
        }
    }


}
