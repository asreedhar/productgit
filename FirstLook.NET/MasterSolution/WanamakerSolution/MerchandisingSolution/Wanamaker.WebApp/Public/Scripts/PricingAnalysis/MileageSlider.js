﻿function SliderInputs() {
    this.StartValue = jQuery("#StartValue").val();
    this.EndValue = jQuery("#EndValue").val();
    this.MinValue = jQuery("#MinValue").val();
    this.MaxValue = jQuery("#MaxValue").val();
    this.Mileage = jQuery("#Mileage").val();
    this.AveragePrice = jQuery("#AveragePrice").val();

    this.CreateSlider = function CreateSlider() {
        //jQuery.noConflict();
        var self = this;
        var width = $("#rangeSlider").width();
        var valuePerPixel = (self.EndValue - self.StartValue) / width;
        var leftPixels = self.Mileage / valuePerPixel;
        leftPixels = leftPixels - 48;
        var marginTop = $("#rangeSlider").outerHeight();
        jQuery('#callout').css("margin-top", marginTop);
        if (parseInt(self.Mileage) < 1) {
            var leftPos = parseInt(18 - ($('#callout').width() / 2));
            $('#callout').css("left", leftPos + "px");
        } else {
            $('#callout').css("left", leftPixels+ "px");
        }
        jQuery('#calloutText').text(parseInt(self.Mileage) < 1 ? 'N/A' : FormatNumber(self.Mileage));
        jQuery('#lblAvgPrice').text(FormatNumber(self.AveragePrice));

        var minSlide = parseInt(self.MinValue), maxSlide = parseInt(self.MaxValue);

        //        if (self.MaxValue == null) {
        //            self.MaxValue = self.EndValue;
        //        }

        if (self.MaxValue == self.EndValue) {
            $('#lblMaxValue').text("Unlimited");
        }
        else {
            $('#lblMaxValue').text(FormatNumber(self.MaxValue));
        }
        jQuery('#lblMinValue').text(FormatNumber(self.MinValue));

        jQuery('#lblSliderRange').text(jQuery("#lblMinValue").text() + ' - ' + jQuery("#lblMaxValue").text());

        jQuery('#rangeSlider').slider({
            range: true,
            min: parseInt(self.StartValue),
            max: parseInt(self.EndValue),
            values: [parseInt(self.MinValue), parseInt(self.MaxValue)],
            slide: changeMileage,
            create: function (event, ui) {
                handle = jQuery('#rangeSlider .ui-slider-handle');
                handle.eq(0).addClass('first-handle');
                handle.eq(1).addClass('second-handle');
            },
            stop: function (event, ui) {
                maxSlide = ui.values[1];
                minSlide = ui.values[0];
                var searchOptions = new SearchCriteria();
                searchOptions = JSON.parse($('#SearchOptions').val());
                if (ui.values[1] == self.EndValue) {
                    searchOptions.MaxMileageValue = "";
                }
                else {
                    searchOptions.MaxMileageValue = ui.values[1];
                }
                searchOptions.MinMileageValue = ui.values[0];
                $('#SearchOptions').val(JSON.stringify(searchOptions));
                searchOptions.CriteriaUpdated = true;
                searchOptions.UpdateFacets = true;
                searchOptions.MileageUpdated = true;

                var gridObject = new Markelistingsinput(searchOptions.Vin);
                gridObject.fillGrid(searchOptions, false);
            }
        });

        function changeMileage(event, ui) {
            if (minSlide != parseInt(ui.values[0])) {
                handle.eq(0).addClass('increase-index');
                handle.eq(1).removeClass('increase-index');
            }
            else if (maxSlide != parseInt(ui.values[1])) {
                handle.eq(1).addClass('increase-index');
                handle.eq(0).removeClass('increase-index');
            }
            if (ui.values[1] == self.EndValue) {
                $('#lblMaxValue').text("Unlimited");
            }
            else {
                $('#lblMaxValue').text(FormatNumber(ui.values[1]));
            }
            jQuery('#lblMinValue').text(FormatNumber(ui.values[0]));
            jQuery('#lblSliderRange').text(jQuery("#lblMinValue").text() + ' - ' + jQuery("#lblMaxValue").text());
        }

        function FormatNumber(number) {
            if (number != undefined && number != null)
                return (number.toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            else
                return 0;
        }

    }
};
