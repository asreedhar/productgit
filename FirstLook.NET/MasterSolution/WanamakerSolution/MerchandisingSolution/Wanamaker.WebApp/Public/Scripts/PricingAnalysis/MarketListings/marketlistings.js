﻿
function Markelistingsinput(vin) {
    this.url = $('#MarketListingUrl').val();
    this.vin = vin;

    this.fillGrid = function fillGrid(searchOption, updateCompetitiveFilters) {
        try {

            if ($('#showdollar0listings').is(":checked") == false) {
                searchOption.ShowZeroListing = false;
            }
            else {
                searchOption.ShowZeroListing = true;
            }

            searchOption.InternetPrice = parseInt($('#ListPrice').val());

            $('#overallCriteria').val(JSON.stringify(searchOption));

            if (updateCompetitiveFilters) {
                Max.PricingAnalysis.LoadDefineCompetitivePartial($('#ddlChromeStyle').val(), $('#overallCriteria').val(),true);
                return;
            }


            $('#DuplSearchOptions').val(JSON.stringify(searchOption));

            if ($('#hdJqGridPageNo') != undefined)
                searchOption.PageNo = parseInt($('#hdJqGridPageNo').val()) + parseInt(1);

            $("#jqGrid").jqGrid("GridUnload");
            $("#jqGrid").jqGrid('GridDestroy');
            var mySubgridData;
            var vin = this.vin;
            Max.PricingAnalysis.ShowLoader();
            $("#jqGrid").jqGrid({
                url: this.url,
                postData: {
                    SearchInputs: JSON.stringify(searchOption)
                },
                mtype: 'POST',
                datatype: 'json',
                contentType: "application/json",
                colNames: ["Age", "Seller", "Vehicle Description", "Certified", "Color", "Mileage", "Internet<br>Price", "% of<br>Market<br>Avg", "Distance", "VIN"],
                colModel: [
                    { name: 'Age', align: "center", hidden: false, sortable: true, sorttype: "int", width: 95, cellattr: function (rowId, cellValue, rawObject, cm, rdata) { return ' class="rightalign"'; } },
                            { name: 'Seller', sortable: true, hidden: false, width: 146, cellattr: function (rowId, cellValue, rawObject, cm, rdata) { return ' class="txtOverflow"'; } },
                            { name: 'VehicleDescription', sortable: true, hidden: false, width: 290, cellattr: function (rowId, cellValue, rawObject, cm, rdata) { return ' class="txtOverflow"'; } },
                            { name: 'IsCertified', sortable: true, hidden: false, width: 100, formatter: function (cellValue) {
                                if (cellValue == true) {
                                    return "<img src='../Public/Images/16x16-smgreycheck.png' align='middle'/>";
                                } else {
                                    return "";
                                }
                            }, cellattr: function (rowId, cellValue, rawObject, cm, rdata) { return ' class="middlealign"'; }
                            },
                            { name: 'Color', sortable: true, hidden: false, width: 120, cellattr: function (rowId, cellValue, rawObject, cm, rdata) { return ' class="leftalign"'; } },
                            { name: 'Mileage', sortable: true, hidden: false, width: 100, formatter: function (cellValue, option, rowOpt) { return FormatNumberWithComma(cellValue); }, cellattr: function (rowId, cellValue, rawObject, cm, rdata) { return ' class="rightalign"'; } },
                            { name: 'InternetPrice', sortable: true, hidden: false, width: 110, formatter: function (cellValue, option, rowOpt) { return FormatCurrencyNumber(cellValue); }, cellattr: function (rowId, cellValue, rawObject, cm, rdata) { return ' class="rightalign internet-price"'; } },
                            { name: 'PMarketAverage', sortable: true, hidden: false, width: 110, formatter: function (cellValue, option, rowOpt) { return Math.round(cellValue); }, cellattr: function (rowId, cellValue, rawObject, cm, rdata) { return ' class="rightalign market-avg"'; } },
                            { name: 'Distance', sortable: true, hidden: false, width: 110, cellattr: function (rowId, cellValue, rawObject, cm, rdata) { return ' class="rightalign"'; } },
                            { name: 'VIN', key: true, sortable: true, hidden: false, width: 270 }
                ],
                autowidth: true,
                rowNum: 10, //defines the number of rows s
                viewrecords: true,
                rowList: [10, 20, 30, 40],
                sortorder: 'asc',
                hoverrows: false,
                scroll: true,
                sortable: true,
                subGrid: true,
                viewsortcols: [true, 'vertical', true],
                subGridOptions: { openicon: "ui-helper-hidden" },
                gridview: true,
                beforeRequest: function (e) {
                    if (Max.PricingAnalysis.IsLoaderHidden()) {
                        $(".market-listing-overlay").show();
                        Max.PricingAnalysis.ShowLoaderForMarketListings();
                    }
                },
                loadComplete: function (data) {
                    if (data != undefined) {
                        if (mySubgridData != null && mySubgridData != undefined)
                            mySubgridData = $.extend(mySubgridData, JSON.parse(data.sub));
                        else {
                            mySubgridData = JSON.parse(data.sub);
                        }
                    }
                    var allRowsOnCurrentPage = $('#jqGrid').jqGrid('getDataIDs');

                    //suppose the column you want tho check in dropdown and in grid is "Name"

                    if (data.rows.length > 0) {
                        $('#lblAvgPrice').text(FormatNumberWithComma(data.rows[0].MileageAvg));
                        $('#lblAvgMileagePricInGame').text(FormatNumberWithComma(data.rows[0].MileageAvg));
                        searchOption.AvgMileageValue = data.rows[0].MileageAvg;
                        $('#listingCount').text(data.records);
                        searchOption.RecordCount = data.records;
                        $('#DuplSearchOptions').val(JSON.stringify(searchOption));
                    }
                    else {
                        $('#lblAvgPrice').text("0");
                        $('#lblAvgMileagePricInGame').text("0");
                        searchOption.AvgMileageValue = 0;
                        $('#listingCount').text("0");
                        searchOption.RecordCount = 0;
                        $('#DuplSearchOptions').val(JSON.stringify(searchOption));
                    }
                    if (data != null && data != undefined) {
                        Max.PricingAnalysis.LoadPricingBar(data.PricingcalculatorModel);

                        if (data.page != null && data.page != undefined) {
                            if ($('#hdJqGridPageNo') != undefined)
                                searchOption.PageNo = data.page;
                            $('#hdJqGridPageNo').val(data.page);
                        }

                        if ($('#hdJqGridPageSize') != undefined)
                            $('#hdJqGridPageNo').val(10);

                        if (data.records != null) {
                            Max.PricingAnalysis.UpdateMatchingCount(data.records, data.PricingcalculatorModel.ShowAlert);
                        }
                    }
                    for (var i = 0; i < allRowsOnCurrentPage.length; i++) {
                        var Name = $('#jqGrid').getCell(allRowsOnCurrentPage[i], 'VIN');
                        if (Name === vin) {
                            jQuery("#jqGrid").setSelection(allRowsOnCurrentPage[i], true);
                        }
                    }

//                    if (searchOption.UpdateFacets) {
                        Max.PricingAnalysis.LoadFacets(searchOption.SelectedEquipment);
//                    }
                    $('#ListPrice').val(searchOption.InternetPrice);
                    searchOption.UpdateFacets = false;
                    searchOption.CriteriaUpdated = false;
                    $('#SearchOptions').val(JSON.stringify(searchOption));
                    $('#overallCriteria').val(JSON.stringify(searchOption));
                    Max.PricingAnalysis.LoadDefineCompetitivePartial($('#ddlChromeStyle').val(), $('#SearchOptions').val(), false);
                    Max.PricingAnalysis.LoadMileagePartial(JSON.stringify(searchOption));
                    
                    var searchOptionsInetPrice = $.parseJSON($('#SearchOptions').val());

                    if (searchOptionsInetPrice.InternetPrice != undefined)
                        $("#PPInternetPrice").val(searchOptionsInetPrice.InternetPrice);

                    if (data.PricingcalculatorModel.ShowAlert != undefined)
                        $("#PPShowAlert").val(data.PricingcalculatorModel.ShowAlert);

                    if (data.PricingcalculatorModel.AvgListPrice != undefined)
                        $("#PPMarketAvg").val(data.PricingcalculatorModel.AvgListPrice);

                    Max.PricingAnalysis.LoadPartialView4PricingProof();
                    if (searchOptionsInetPrice.InternetPrice != undefined && data.PricingcalculatorModel.ShowAlert != undefined && data.PricingcalculatorModel.AvgListPrice != undefined)
                        UpdateMarketAvg(searchOptionsInetPrice.InternetPrice, data.PricingcalculatorModel.ShowAlert, data.PricingcalculatorModel.AvgListPrice);
                    if (data.PricingcalculatorModel.ShowAlert) {
                        $('table#jqGrid.ui-jqgrid-btable tbody tr.ui-widget-content.jqgrow.ui-row-ltr.ui-state-highlight td.market-avg').html("N/A");
                    }
                    
                    Max.PricingAnalysis.HideLoader();
                },
                loadError: function (xhr, st, err) {
                    Max.PricingAnalysis.HideLoader();
                },
                beforeSelectRow: function (rowid, e) {
                    return false;
                },
                subGridRowExpanded: function (subgridDivId, rowId) {

                    var subgridTableId = subgridDivId + "_t";

                    if (mySubgridData == null || mySubgridData[rowId] == null || mySubgridData[rowId].trim() == "") {
                        $("#" + subgridDivId).html('<span class="subgrid-style"> - </span>');
                    } else {
                        $("#" + subgridDivId).html('<span class="subgrid-style">' + mySubgridData[rowId] + '</span>');
                    }

                }
            });
        } catch (ex) {
            Max.PricingAnalysis.HideLoader();
        }
    }
   


};

function FormatCurrencyNumber(number) {
    return Max.PricingAnalysis.currencyFormat(number);
}

function FormatNumberWithComma(number) {
    return number == null ? '-' : number.toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

String.prototype.format = String.prototype.f = function () {
    var s = this,
        i = arguments.length;

    while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
    }
    return s;
};

function updateLinkCarsCom(values)
{
    var url = 'http://www.cars.com/go/search/search_results.jsp?NumResultsPerPage=25&makeid={0}&dgid=&pageNumber=0&zc={1}&cid=&amid=&certifiedOnly=false&sortorder=descending&SearchType=22&criteria=K-%7CE-ALL%7CM-_{2}_%7CH-%7CD-_{3}_%7CN-N%7CR-{4}%7CI-1%7CP-PRICE+descending%7CQ-descending%7CY-_{5}_%7CX-popular%7CZ-{6}&modelid={7}&tracktype=usedcc&sortfield=PRICE+descending&aff=national&dlidF=&Cname=&LargeNumResultsPerPage=500&paId=&aff=national'.format($(values).attr("CarsDotComMakeID"), $(values).attr("Zipcode"), $(values).attr("CarsDotComMakeID"), $(values).attr("ModelId"), $('span#distanceSpan>ul>li').text(), $(values).attr("StartYear"), $(values).attr("Zipcode"), $(values).attr("CarsDotComModelId"));
    window.open(url, '_blank');
    return false;
}
function updateLinkAutoTraderCom(values) {
    var url = 'http://www.autotrader.com/fyc/searchresults.jsp?advanced=&num_records=25&certified=&isp=y&search=y&lang=en&search_type=used&min_price=&max_price=&rdpage=100&make={0}&model={1}&start_year={2}&end_year={3}&distance={4}&address={5}'.format($(values).attr("AutoTraderMake"), $(values).attr("AutoTraderModel"), $(values).attr("StartYear"), $(values).attr("EndYear"), $('span#distanceSpan>ul>li').text(), $(values).attr('Zipcode'));
    window.open(url, '_blank');
    return false;
}