﻿var Max = Max || {};
Max.PricingAnalysis = Max.PricingAnalysis || {};
Max.PricingAnalysis.Refinements = Max.PricingAnalysis.Refinements || {};
Max.PricingAnalysis.Refinements = $.extend({}, Max.PricingAnalysis.Refinements, {

    Refine: function Refine(refinement, onComplete) {
        var self = this;
        $.ajax({
            type: "POST",
            url: "/merchandising/PricingAnalysis/PingOne/Refine",
            data: { refinement: refinement }
        }).done(function (response) {
            alert("Success: " + response.Info);
        }).fail(function (response) {
            alert("Failure! " + response.Info);
        }).always(function () {
            if (Max.PricingAnalysis.IsFunction(onComplete)) {
                onComplete();
            }
        });
    },

    BindEvents: function BindEvents() {
        var self = this;
        $("button.refinement").each(function() {
            var $button = $(this);
            var transactComplete = "transactComplete";

            $button.on(transactComplete, function (e) {
                $button.prop("disabled", false);
            });

            $button.on("click", function() {
                // Disable the button until transaction is complete
                //
                $button.prop("disabled", true);

                // Completed action
                //
                var onComplete = function onComplete() {
                    $button.trigger(transactComplete);
                };

                // Do work
                //
                self.Refine("trim", onComplete);
            });
        });
    },

    Init: function Init() {
        var self = this;
        $(function() {
            self.BindEvents();
        });
    }
});

Max.PricingAnalysis.Refinements.Init();