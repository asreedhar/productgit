﻿


    function btnCarfax_onclick() {

        document.getElementsByClassName("carfax-main")[0].setAttribute('style', 'display:block');

        if (document.getElementsByClassName("autocheck-main")[0] != null && document.getElementsByClassName("autocheck-main")[0] != undefined)
            document.getElementsByClassName("autocheck-main")[0].setAttribute('style', 'display:none');

        $("#btnAutocheck").addClass("change-color");
        $("#btnCarfax").addClass("btn-carfax");
        $("#btnCarfax").removeClass("change-color");

    }

    function btnAutocheck_onclick() {

        document.getElementsByClassName("autocheck-main")[0].setAttribute('style', 'display:block');
        if (document.getElementsByClassName("carfax-main")[0] != null && document.getElementsByClassName("carfax-main")[0]!=undefined)
         document.getElementsByClassName("carfax-main")[0].setAttribute('style', 'display:none');
        $("#btnCarfax").addClass("change-color");
        $("#btnAutocheck").addClass("btn-autocheck");
        $("#btnAutocheck").removeClass("btn-autocheck-default");

        $("#btnAutocheck").removeClass("change-color");

    }

    function PullAutocheck(btn) {
        postData = { "pullAutoCheckReport": true };
        var url = $(btn).attr('post-url');
        Max.PricingAnalysis.PostData(url, postData, function (data) { $('#veicleHistoryReport').html(data); btnAutocheck_onclick(); }, function (message) { alert(message); });
    }

    function PullCarfax(btn) {
        postData = { "pullCarfaxReport": true };
        var url = $(btn).attr('post-url');
        Max.PricingAnalysis.PostData(url, postData, function (data) { $('#veicleHistoryReport').html(data); }, function (message) { alert(message); });
    }


function openReport(linkText) {
    window.open(linkText, '', 'top=0, left=0, width=' + screen.width + ',height=' + screen.height+"'");
}

