﻿var minBookHeight = 376;
var maxBookHeight = 454;
function OpenDiv(id, disp) {

    ////    $('.main').attr('class', 'main');
    ////    $('.new-class').attr('class', 'mainew-class');
    ////    $('.ping-content').attr('class', 'ping-contentn');
    ////    $('.pricing-inner').attr('class', 'pricing-inner');
    //    $("html, body").animate({ scrollTop: 225 }, 225);
    $('.main').removeClass('enlarged');
    $('.new-class').removeClass('enlarge');
    $('.ping-content').removeClass('enlarge-content');
    $('.new-class').removeClass('enlarge-main');
    $('.pricing-inner').removeClass('enlarge-pricing-inner');
    $('.main').removeClass('enlarged-main');
    if ($("#" + id.getAttribute('id')).hasClass("enlarged-subdiv")) {
        $('#marketAvgLogo').removeClass("shift-msrp-logo");
        $('#tickmarketAvg').removeClass("shift-down-tick");
        $("#" + id.getAttribute('id')).addClass("subdiv");
        $("#" + id.getAttribute('id')).removeClass("enlarged-subdiv");
        $(".show-comparisons").addClass("comparisons");
        $(".show-comparisons").removeClass("show-comparisons");
        $("#lnkView").removeClass("lnk-view");
        $("#lnkView").addClass("view-link");
        $("#arr").addClass("side-arrow");
        $("#arr").removeClass("down-arrow");
        $("#" + disp.getAttribute('id')).show();
    } else {
        if ($('.pricing-proof-main').height() >= minBookHeight && $('.pricing-proof-main').height() <= (maxBookHeight - 1)) {
            $('.main').addClass('enlarged');
            $('.new-class').addClass('enlarge');
        }
        else if ($('.pricing-proof-main').height() >= maxBookHeight) {
            $('.ping-content').addClass('enlarge-content');
            $('.new-class').addClass('enlarge-main');
            $('.pricing-inner').addClass('enlarge-pricing-inner');
            $('.main').addClass('enlarged-main');
        }
        $('#tickmarketAvg').addClass("shift-down-tick");
        $('#marketAvgLogo').addClass("shift-msrp-logo");
        //        $("html, body").animate({ scrollTop: 400 }, 10);
        $("#lnkView").removeClass("view-link");
        $("#lnkView").addClass("lnk-view");
        $("#arr").addClass("down-arrow");
        $("#arr").removeClass("side-arrow");
        $("#" + id.getAttribute('id')).addClass("enlarged-subdiv");
        $("#" + id.getAttribute('id')).removeClass("subdiv");
        $(".comparisons").addClass("show-comparisons");
        $(".comparisons").removeClass("comparisons");
        $("#" + disp.getAttribute('id')).hide();
    }
}

function Change(selectedItem) {
    $('#lblmarketAvg').text("$" + FormatPricingProofNumber($(selectedItem).next().val()));
    var value = parseInt($(selectedItem).parent().next().children().text().replace(/[^0-9\.]+/g, ""));
    $("#spnmarketAvg").text("$" + FormatPricingProofNumber(value));
    if ($(selectedItem).parent().next().children().hasClass('warning')) {
        if (!$('#tickmarketAvg').hasClass("alert-image")) {
            $("#tickmarketAvg").removeClass();
            $('#tickmarketAvg').addClass("failure-image");
            $('#statusmarketAvg').text("above Market Avg.");
        }
    }
    else if ($(selectedItem).parent().next().children().hasClass('success')) {
        if (!$('#tickmarketAvg').hasClass("alert-image")) {
            $("#tickmarketAvg").removeClass();
            $('#tickmarketAvg').addClass("success-image");
            $('#statusmarketAvg').text("below Market Avg.");
        }
    }
}

function FormatPricingProofNumber(number) {
    return (number.toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ","));
}


function UpdateMarketAvg(internetPrice, showAlert, MarketAvg) {
    var bookList = ["marketAvg", "MSRP", "Nada", "KBB", "Edmunds"];
    var DisplayNames = ["Market Avg.", "MSRP", "NADA Retail", "KBB Retail", "Edmunds Retail"];
    if (showAlert === undefined) showAlert = false;
    if (MarketAvg === undefined) MarketAvg = 0;

    $(".pricing-proof-hidden-fileds").each(function () {
        var hiddenFieldId = $(this).attr('id').replace('hd_', '').trim();
        var hiddenAlertFieldId = $(this).attr('id').replace('hd_', 'hd_Alert_').trim();

        marketAvgValue = $(this).val() - internetPrice;

        var priceComparisonData = "";
        if ($('#' + hiddenAlertFieldId).val()!=0) {
            $('.market-avg-options #' + hiddenFieldId).html('-');
        } else {
            if (marketAvgValue >= $('#hiddenmarketAvg').val()) {
                $('.market-avg-options #' + hiddenFieldId).removeClass('warning radio-width');
                $('.market-avg-options #' + hiddenFieldId).addClass('success radio-width');
                priceComparisonData = FormatPricingProofNumber(marketAvgValue * -1) + " Under";
            } else {
                $('.market-avg-options #' + hiddenFieldId).removeClass('success radio-width');
                $('.market-avg-options #' + hiddenFieldId).addClass('warning radio-width');
                priceComparisonData = FormatPricingProofNumber(marketAvgValue) + "  Over";
            }
            $('.market-avg-options #' + hiddenFieldId).html('$' + priceComparisonData);
        }

        

        for (var i = 0; i < bookList.length; i++) {
            UpdatePrice(bookList[i], internetPrice, DisplayNames[i], showAlert, MarketAvg);
        }
    });
}

function UpdatePrice(bookname, internetPrice, displayName, showAlert, MarketAvg) {
    var value;
    if (showAlert === undefined) showAlert = false;
    if (MarketAvg === undefined) MarketAvg = 0;

    if (!($('#lbl' + bookname).text() == "N/A")) {
        value = parseInt($('#lbl' + bookname).text().replace(/[^0-9\.]+/g, ""));
    }
    else {
        value = 0;
    }
    var difff = internetPrice - value;
    if (difff >= $('#hidden' + bookname).val()) {
        var status = "above " + displayName;
        $('#spn' + bookname).text("$" + FormatPricingProofNumber(difff));

        if (!$('#tick' + bookname).hasClass("alert-image")) {
            $('#lbl' + bookname).addClass("warning");
            $('#lbl' + bookname).removeClass("success");
            $('#tick' + bookname).addClass("failure-image");
            $('#tick' + bookname).removeClass("success-image");
        }

    }
    else {
        var status = "below " + displayName;
        $('#spn' + bookname).text("$" + FormatPricingProofNumber((difff * -1)));

        //if (!$('#tick' + bookname).hasClass("alert-image")) {
            $('#lbl' + bookname).removeClass("warning");
            $('#lbl' + bookname).addClass("success");
            $('#tick' + bookname).removeClass("failure-image");
            $('#tick' + bookname).addClass("success-image");
        //}
    }
    $('#status' + bookname).text(status);

    if (bookname.toLowerCase() == "marketavg") {
        if ($('#lbl' + bookname).length > 0 && MarketAvg != 0 ) {
            $('#lbl' + bookname).text("$" + FormatPricingProofNumber(MarketAvg)).removeClass("black-font");
            //var variable = $('#lbl' + bookname).text();
        }
        else
            $('#hdnmarketAvg').val("$" + FormatPricingProofNumber(MarketAvg));
    }

    if ((showAlert == "true" || showAlert == true) && (bookname.toLowerCase() == "marketavg" || bookname.toLowerCase() == "msrp")) {
        $('#spn' + bookname).text("N/A").addClass("black-font");
        $('#lbl' + bookname).text("N/A").addClass("black-font");
        $('#tick' + bookname).removeClass('failure-image').removeClass('success-image').addClass('alert-image');
    }

    if (bookname.toLowerCase() != "marketavg" && bookname.toLowerCase() != "msrp" && bookname.toLowerCase() != "edmunds") {
        if ($('#tick' + bookname).hasClass('alert-image')) {
            $('#lbl' + bookname).text("N/A").addClass('black-font');
            $('#spn' + bookname).text("-");
            $('#status' + bookname).text('below ' + bookname + ' Retail');
        }
    }

    if (($('#spn' + bookname).text() == "N/A") && (bookname.toLowerCase() == "marketavg" || bookname.toLowerCase() == "msrp")) {
        $('#spn' + bookname).text("N/A").addClass("black-font");
        $('#lbl' + bookname).text("N/A").addClass("black-font");
        //$('#tick' + bookname).removeClass('failure-image').removeClass('success-image').addClass('alert-image');
    }

    if (($('#lbl' + bookname).text() == "N/A") && (bookname.toLowerCase() == "edmunds")) {
        $('#spn' + bookname).text("-").addClass("black-font");
    }

}

function openEstock(lmn) {

    var PricingProofWindow = window.open($(lmn).attr("href"), "PricingProofWindow"),
      poller;

    // Ensure focus
    PricingProofWindow.focus();

    //poll for change only if there is no vehicle history report

    // Listen for window's close event
    poller = window.setInterval(function () {

        if (PricingProofWindow.closed) {

            window.setTimeout(function () {

                //Max.PricingAnalysis.LoadPartialView($('#pricingproofDisplay').attr('action-url'), $('#pricingproofDisplay'));
                Max.PricingAnalysis.LoadPartialView4PricingProof();
            }, 1000);

            window.clearInterval(poller);
        }

    }, 500);


}
