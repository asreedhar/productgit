﻿function ChangeColor(btn, refresh) {
  
    var thisId = "#btnFct" + Max.PricingAnalysis.RemoveSpecialCharacters(btn);
    if ($(thisId).hasClass('filter')) {
        $(thisId).removeClass('filter');
        $(thisId).addClass('selected-button');
        var searchOption = JSON.parse($('#SearchOptions').val());
        searchOption.SelectedEquipment =[] ;
        $('#filter-div .selected-button').each(function () {                        
            searchOption.SelectedEquipment.push($(this).val());
        });
        $('#SearchOptions').val(JSON.stringify(searchOption));
        if (refresh) {
            var gridObject = new Markelistingsinput(searchOption.Vin);
            searchOption.CriteriaUpdated = true;
            searchOption.UpdateFacets = false;
            gridObject.fillGrid(searchOption, false);
        }
    } else if ($(thisId).hasClass('selected-button')) {
        $(thisId).removeClass('selected-button');
        $(thisId).addClass('filter');
        var searchOption = JSON.parse($('#SearchOptions').val());
        var data = btn;
        searchOption.SelectedEquipment = $.grep(searchOption.SelectedEquipment,function (o, i) { return o == data; },true);
        $('#SearchOptions').val(JSON.stringify(searchOption));
        var gridObject = new Markelistingsinput(searchOption.Vin);
        searchOption.CriteriaUpdated = true;
        searchOption.UpdateFacets = true;
        gridObject.fillGrid(searchOption,false);             
    }
}

function GetSelectedItems() {

    $("input:checkbox[name=addFilterCheckbox]:checked").each(function () {
        var btn = document.getElementById('btn' + $(this).attr('id'));
        if (btn == null || btn == 'undefined') {
            var b = document.createElement('button');
            b.setAttribute('class', 'filter');
            b.setAttribute('id', 'btn' + $(this).attr('id'));
            b.innerHTML = $(this).val();
            b.setAttribute('onclick', 'ChangeColor(this.id,false)');
            $('.add-more').before(b);
        }

    }
        );



    $("input:checkbox[name=addFilterCheckbox]:not(:checked)").each(function () {
        var btn = document.getElementById('btn' + $(this).attr('id'));
        if (btn != null && btn != 'undefined') {
            $('#btn' + $(this).attr('id')).remove();
        }
    }
        );
    $('.ui-dialog').hide();
    $(".ui-widget-overlay").hide();
}


function PopulateList(btn) {
    $('.facet-hidden-buttons').removeClass('hide-control');
    $('.facet-hidden-buttons').addClass('show-control');
    $(btn).hide();
//    $('.filter-facet-div').removeClass('filter-facet-div');
//    $('#filter-facet-div').addClass('show-filter-list');
//    $('.ui-dialog').show();
//    $(".ui-widget-overlay").hide();
//    $('#filter-facet-div').dialog({
//        modal: true,
//        buttons: {
//            "Save": GetSelectedItems,
//            Cancel: function () {
//                $('.ui-dialog').hide();
//               $(".ui-widget-overlay").hide();
//            }

//        },
//        close: function (ev, ui) {
//            $('.ui-dialog').hide();
//            $(".ui-widget-overlay").hide();
//        }
    //    });

}