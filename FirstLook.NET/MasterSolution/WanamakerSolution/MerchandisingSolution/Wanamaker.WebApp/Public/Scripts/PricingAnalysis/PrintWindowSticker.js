﻿$(document).ready(function () {
    $('#pws').on("click", function (e) {
        if ($('#PrintWindowStickerDlg').children().length > 0) {
            $('#pwsOverlay').addClass('pws-overlay');
            $('#PrintWindowStickerDlg').css('display', 'inline');
            var inventoryId = $(this).attr('data-inv');
            var url = $('#LastPrintedMessage').attr('action-url');
            url += '?inv=' + inventoryId;
            $.ajax({
                type: "GET",
                contentType: 'application/html',
                url: url,
                cache: false,
                success: function (data) {
                    if (data != undefined && data != '') {
                        $('#LastPrintedMessage').text(data);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jqXHR.responseText);
                }
            });
            return;
        }
        var url = $(this).attr("data-url");
        var $this = $(this);
        var $body = $('body');
        $.ajax({
            type: "GET",
            contentType: 'application/html',
            url: url,
            cache: false,
            beforeSend: function () {
                //$this.css({ 'cursor': 'wait' });
                $body.css({ 'cursor': 'wait' });
            },
            success: function (data) {
                if (data != undefined && data != '') {
                    $('#PrintWindowStickerDlg').css('display', 'inline');
                    $('#PrintWindowStickerDlg').addClass('pws-dialog').html(data);
                    $('#pwsOverlay').addClass('pws-overlay');
                }
            },
            complete: function () {
                var buyerGuidesJsonStatus = JSON.parse($("#BuyerGuidesJsonStatus").val());
                $(".noWarranty").addClass("hideMe");
                $(".lmtWarranty").addClass("hideMe");
                $(".fullWarranty").addClass("hideMe");
                $(".serContract").addClass("hideMe");
                $(".kbbBookVal").addClass("hideMe");
                if (buyerGuidesJsonStatus && buyerGuidesJsonStatus.NoWarranty) {
                    $('.noWarranty').removeClass('hideMe').addClass('showMe');
                }
                if (buyerGuidesJsonStatus && buyerGuidesJsonStatus.LimitedWarranty) {
                    $('.lmtWarranty').removeClass('hideMe').addClass('showMe');
                }
                if (buyerGuidesJsonStatus && buyerGuidesJsonStatus.FullWarranty) {
                    $('.fullWarranty').removeClass('hideMe').addClass('showMe');
                }
                if (buyerGuidesJsonStatus && buyerGuidesJsonStatus.ServiceContract) {
                    $('.serContract').removeClass('hideMe').addClass('showMe');
                }
                if (buyerGuidesJsonStatus && buyerGuidesJsonStatus.KbbBookValue) {
                    $('.kbbBookVal').removeClass('hideMe').addClass('showMe');
                }
                //Final Validation Check
                if ($('#WindowStickers option:selected').val() == undefined) {
                    $("#WindowStickers").prop('disabled', 'disabled');
                    $("#PrintWindowStickerCheckbox").prop('disabled', 'disabled');
                }
                if ($('#BuyersGuides option:selected').val() == undefined) {
                    $("#BuyersGuides").prop('disabled', 'disabled');
                    $("#PrintBuyersGuideCheckbox").prop('disabled', 'disabled');
                }

                //$this.css({ 'cursor': 'default' });
                $body.css({ 'cursor': 'default' });
                $('#closeWps').attr('style', 'background:transparent url(../Themes/MAX3.0/Images/sprite_jQueryUI.png?v=3.0) no-repeat scroll 0 1px !important');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseText);
                //$this.css({ 'cursor': 'default' });
                $body.css({ 'cursor': 'default' });
            }

        });
    });
    $('body').on('change', 'select.wSticker', function () {
        var url = $(this).attr("data-url") + '?selectedValue=' + $(this).val();
        callcssshowControls(url);
    });

    function callcssshowControls(url) {
        $.ajax({
            type: "GET",
            contentType: 'application/html',
            url: url,
            cache: false,
            beforeSend: function () {
                $('#ProgressInidicator').css('display', 'inline');
            },
            success: function (data) {
                var buyerGuidesJsonStatus = JSON.parse(data);
                $(".noWarranty").removeClass('showMe').addClass("hideMe");
                $(".lmtWarranty").removeClass('showMe').addClass("hideMe");
                $(".fullWarranty").removeClass('showMe').addClass("hideMe");
                $(".serContract").removeClass('showMe').addClass("hideMe");
                $(".kbbBookVal").removeClass('showMe').addClass("hideMe");
                if (buyerGuidesJsonStatus && buyerGuidesJsonStatus.NoWarranty) {
                    $('.noWarranty').removeClass('hideMe').addClass('showMe');
                    $('.noWarranty').prop('checked', true);
                }
                if (buyerGuidesJsonStatus && buyerGuidesJsonStatus.LimitedWarranty) {
                    $('.lmtWarranty').removeClass('hideMe').addClass('showMe');
                }
                if (buyerGuidesJsonStatus && buyerGuidesJsonStatus.FullWarranty) {
                    $('.fullWarranty').removeClass('hideMe').addClass('showMe');
                }
                if (buyerGuidesJsonStatus && buyerGuidesJsonStatus.ServiceContract) {
                    $('.serContract').removeClass('hideMe').addClass('showMe');

                }
                if (buyerGuidesJsonStatus && buyerGuidesJsonStatus.KbbBookValue) {
                    $('.kbbBookVal').removeClass('hideMe').addClass('showMe');
                }
                $('#ProgressInidicator').hide();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseText);
            }
        });
    }

    $('body').on('change', 'select.bGuides', function () {
        var url = $(this).attr("data-url") + '?selectedValue=' + $(this).val();
        callcssshowControls(url);

    });
    $('body').on('click', 'a.ui-dialog-titlebar-close', function () {
        $('#PrintWindowStickerDlg').css('display', 'none');
        $('#pwsOverlay').removeClass('pws-overlay');
    });
    $('body').on('click', 'input.postPws', function () {
        var pdfCnt = 0;
        var url = $(this).attr('data-posturl');
        if ($('#PrintWindowStickerCheckbox').is(':checked')) {
            url = url + '&pwsSelectedTemp=' + $("select.wSticker").val();
            pdfCnt++;
        }
        if ($('#PrintBuyersGuideCheckbox').is(':checked')) {
            url = url + '&bgtSelectedTemp=' + $("select.bGuides").val();
            pdfCnt++;
        }
        if (pdfCnt <= 0) {
            $('#pwsOverlay').removeClass('pws-overlay');
            $('#PrintWindowStickerDlg').css('display', 'none');
            return;
        }
        var checkedStatus = getPromptControlsCheckedStatus();
        if (checkedStatus != '') {
            url = url + '&de=' + checkedStatus;
        }
        var win = window.open(url, '_blank');
        if (win) {
            $('#pwsOverlay').removeClass('pws-overlay');
            $('#PrintWindowStickerDlg').css('display', 'none');
            //Browser has allowed it to be opened
            win.focus();

        } else {
            //Broswer has blocked it
            alert('Please allow popups for this site');
        }

    });

    function getPromptControlsCheckedStatus() {
        var checkedStatus = "";
        if ($('#WarrantyNo').is(':checked')) {
            checkedStatus = 'WarrantyNo';
        }
        if ($('#WarrantyLimited').is(':checked')) {
            checkedStatus = ',WarrantyLimited';
        }
        if ($('#WarrantyFull').is(':checked')) {
            checkedStatus = ',WarrantyFull';
        }
        if ($('#ServiceContract').is(':checked')) {
            checkedStatus = ',ServiceContract';
        }
        if ($('#KBBBookValue').is(':checked')) {
            checkedStatus = ',KBBBookValue';
        }
        return checkedStatus;
    }

});