﻿//function searchOptions() {

function searchOptions(searchOptionList) {
    var searchCriteria = new SearchCriteria();
    searchCriteria.Listings = searchOptionList.Listings;
    searchCriteria.Distance = searchOptionList.Distance;
    searchCriteria.Certified = searchOptionList.Certified;
    searchCriteria.Trim = searchOptionList.Trim;
    searchCriteria.Transmission = searchOptionList.Transmission;
    searchCriteria.DriveTrain = searchOptionList.DriveTrain;
    searchCriteria.Engine = searchOptionList.Engine;
    searchCriteria.Fuel = searchOptionList.Fuel;

    var self = this;

    self.SetSearchCriteria = function SetSearchCriteria() {
        var searchOption = JSON.parse($('#SearchOptions').val());
        searchCriteria.Vin = searchOptionList.Vin;
        searchCriteria.ShowZeroListings = searchOption.ShowZeroListing;
        searchCriteria.SelectedEquipment = searchOption.SelectedEquipment;
        searchCriteria.MinMileageValue = searchOption.MinMileageValue;
        searchCriteria.MaxMileageValue = searchOption.MaxMileageValue;
        searchCriteria.Size = searchOption.Size;
        searchCriteria.PageNo = searchOption.PageNo;
        searchCriteria.PageSize = searchOption.PageSize;
        searchCriteria.RecordCount = searchOption.RecordCount;
        searchCriteria.CriteriaUpdated = searchOption.CriteriaUpdated;
        searchCriteria.UpdateFacets = searchOption.UpdateFacets;
        searchCriteria.StartYear = searchOption.StartYear;
        searchCriteria.EndYear = searchOption.EndYear;
        searchCriteria.Zipcode = searchOption.ZipCode;
        searchCriteria.CarsDotComMakeID = searchOption.CarsDotComMakeID;
        searchCriteria.CarsDotComModelId = searchOption.CarsDotComModelId;
        searchCriteria.AutoTraderMake = searchOption.AutoTraderMake;
        searchCriteria.AutoTraderModel = searchOption.AutoTraderModel;
        searchCriteria.UpdateOverall = searchOption.UpdateOverall;
        searchCriteria.ListingCountThreshold = searchOption.ListingCountThreshold;
        searchCriteria.OverallCount = searchOption.OverallCount;
        searchCriteria.MatchingCount = searchOption.MatchingCount;
        searchCriteria.MileageUpdated = searchOption.MileageUpdated;
        searchCriteria.AvgMileageValue = searchOption.AvgMileageValue;
        searchCriteria.MileageEndValue = searchOption.MileageEndValue;
        searchCriteria.MileageStartValue = searchOption.MileageStartValue;
        searchCriteria.AllListings = searchOption.AllListings;
    }


    self.Show = function Show(divname) {

        $('.dcs-trims-checkbox').off("change").on("change", function () {
            if ($(this)[0].checked) {
                if ($(this)[0].id == "AllTrims") {
                    $('.dcs-trims-checkbox').each(function () {
                        this.checked = false;
                    });
                    $('.dcs-trims-checkbox#AllTrims')[0].checked = true; ;
                }
                else {
                    $('.dcs-trims-checkbox#AllTrims')[0].checked = false ;
                }
            }
        });

        $('#overlay').show();
        $(".toggleable").hide();
        $('#' + divname).next().toggle();
        $('.error').hide();
        var clickedDiv = $('#' + divname).next().attr("id");
        if (clickedDiv.toLowerCase() == "list") {
            $('#list').find('input[type=radio]:checked').removeAttr('checked');
            for (var i = 0; i < searchCriteria.Listings.length; i++) {
                $('#' + searchCriteria.Listings[i].DisplayId).prop('checked', true);
            }
        }
        else if (clickedDiv.toLowerCase() == "distance") {
            $('#distance').find('input[type=radio]:checked').removeAttr('checked');
            for (var i = 0; i < searchCriteria.Distance.length; i++) {
                $('#' + searchCriteria.Distance[i].DisplayId).prop('checked', true);
            }
        }
        else if (clickedDiv.toLowerCase() == "certified") {
            $('#certified').find('input[type=checkbox]:checked').removeAttr('checked');
            for (var i = 0; i < searchCriteria.Certified.length; i++) {
                $('#' + searchCriteria.Certified[i].DisplayId).prop('checked', true);
            }
        }
        else if (clickedDiv.toLowerCase() == "trim") {
            $('#trim').find('input[type=checkbox]').removeAttr('checked');
            for (var i = 0; i < searchCriteria.Trim.length; i++) {

                $('#' + searchCriteria.Trim[i].DisplayId).prop('checked', true);
            }
        }
        else if (clickedDiv.toLowerCase() == "transmission") {
            $('#transmission').find('input[type=checkbox]:checked').removeAttr('checked');
            for (var i = 0; i < searchCriteria.Transmission.length; i++) {
                $('#' + searchCriteria.Transmission[i].DisplayId).prop('checked', true);
            }
        }
        else if (clickedDiv.toLowerCase() == "engine") {
            $('#engine').find('input[type=checkbox]:checked').removeAttr('checked');
            for (var i = 0; i < searchCriteria.Engine.length; i++) {

                $('#' + searchCriteria.Engine[i].DisplayId).prop('checked', true);
            }
        }
        else if (clickedDiv.toLowerCase() == "drivetrain") {
            $('#driveTrain').find('input[type=checkbox]:checked').removeAttr('checked');
            for (var i = 0; i < searchCriteria.DriveTrain.length; i++) {
                //  alert(self.DriveTrain[i].DisplayId);
                $('#' + searchCriteria.DriveTrain[i].DisplayId).prop('checked', true);
            }
        }
        else if (clickedDiv.toLowerCase() == "fuel") {
            $('#fuel').find('input[type=checkbox]:checked').removeAttr('checked');
            for (var i = 0; i < searchCriteria.Fuel.length; i++) {
                $('#' + searchCriteria.Fuel[i].DisplayId).prop('checked', true);
            }
        }
    }                        //end of show function

    self.Close = function Close(divname) {
        $('#' + divname).parent().toggle();
        $('#overlay').hide();
    }

    self.Update = function Update(divname, refreshGrid, showError) {
        //$("#" + divname).find('.close-box').css("pointer-events", "auto");
        var displayText = ''; 
        var isValid = false;
        var count = 0;

        //Check If anything is selected and We need to show Error
        if ($('#' + divname + ' :input:checkbox:checked').length > 0 || $('#' + divname + ' :input:radio:checked').length > 0 || !showError) {
            displayText += '<ul class="display-style">';
            $('#' + divname + ' :input').not(':button').each(function () {
                if ($(this).is(':checked')) {
                    count = count + 1;
                    PushToArray(divname, $(this).val(), $(this).attr('id'), true, 0);

                    if (count >= 3) {

                        displayText = '<ul class="display-style"><li>' + count + ' Selected</li>';
                    }
                    else {
                        displayText += '<li>' + $(this).val() + '</li>';
                    }
                    if ((divname == "certified") && count > 1) {
                        displayText = '<ul class="display-style"><li> Certified & </li> <li>Not Certified </li>'
                    }

                }
                else {

                    PushToArray(divname, $(this).val(), $(this).attr('id'), false, 1);
                }
            });
            displayText += '</ul>';
            isValid = true;
        }


        if (isValid) {
            $('#' + divname).next().text('');
            $('#' + divname).next().html('');
            $('#' + divname).next().html(displayText);
            $('#' + divname).toggle();
            $('#' + divname).find('.error').hide();
            $('#overlay').hide();

            if (refreshGrid) {
                var gridObject = new Markelistingsinput(searchCriteria.Vin);
                self.SetSearchCriteria();
                searchCriteria.CriteriaUpdated = true;
                searchCriteria.UpdateFacets = true;
                if (divname.toLowerCase() == 'distance' || divname.toLowerCase() == 'list') {
                    searchCriteria.UpdateOverall = true;
                    searchCriteria.MaxMileageValue = null;
                    gridObject.fillGrid(searchCriteria, true);
                } else {
                    searchCriteria.UpdateOverall = false;
                    gridObject.fillGrid(searchCriteria, false);
                }
            }

        }
        else {
            if (showError && $('#' + divname + ' :input').not(':button').length > 0) {
                $('#' + divname).find('.error').show();
                //$("#" + divname).find('.close-box').css("pointer-events", "none");
            } else {
                $('#' + divname).toggle();
                $('#' + divname).find('.error').hide();
                $('#overlay').hide();
            }
        }

    }                                      //end of update function

    function PushToArray(divname, value, id,isChecked, opreration) {
        var count = 0;
        var filterObject = new FilterItem(value, count, id, isChecked);

        if (divname.toLowerCase() == "list") {

            searchCriteria.Listings = Operation(searchCriteria.Listings, filterObject, opreration);

        }
        else if (divname == "trim") {
            searchCriteria.Trim = Operation(searchCriteria.Trim, filterObject, opreration);
        }

        else if (divname == "distance") {
            searchCriteria.Distance = Operation(searchCriteria.Distance, filterObject, opreration);
        }

        else if (divname == "certified") {
            searchCriteria.Certified = Operation(searchCriteria.Certified, filterObject, opreration);
        }
        else if (divname == "transmission") {
            searchCriteria.Transmission = Operation(searchCriteria.Transmission, filterObject, opreration);
        }
        else if (divname == "driveTrain") {
            searchCriteria.DriveTrain = Operation(searchCriteria.DriveTrain, filterObject, opreration);

        }
        else if (divname == "engine") {
            searchCriteria.Engine = Operation(searchCriteria.Engine, filterObject, opreration);
        }
        else if (divname == "fuel") {
            searchCriteria.Fuel = Operation(searchCriteria.Fuel, filterObject, opreration);
        }
    } //end of pushToArray
    function Operation(arrayName, filterItem, operation) {
        var found = false;
        for (var i = 0; i < arrayName.length; i++) {
            if (arrayName[i].DisplayName == filterItem.DisplayName) {
                found = true;
            }
        }
        if (operation == 0) {
            if (!found) {
                arrayName.push(filterItem);
            }
        }
        else {
            if (found) {
                arrayName = $.grep(arrayName,
                   function (o, i) { return o.DisplayName === filterItem.DisplayName; },
                   true);
            }
        }
        return arrayName;
    }

}; //end of class

function FilterItem(displayName, count, displayId, isSelected) {
    this.DisplayName = displayName;
    this.Count = count;
    this.DisplayId = displayId;
    this.IsSelected = isSelected;
};
