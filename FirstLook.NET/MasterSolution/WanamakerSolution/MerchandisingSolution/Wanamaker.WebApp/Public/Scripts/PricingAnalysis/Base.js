﻿$.ajaxSetup({ cache: false });
var Max = Max || {};
Max.PricingAnalysis = Max.PricingAnalysis || {};
Max.PricingAnalysis = $.extend({}, Max.PricingAnalysis, {

    IsFunction: function isFunction(functionToCheck) {
        if (typeof functionToCheck === "undefined") return false;
        var getType = {};
        return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
    },
    IsTextSelected: function (input) {
        if (typeof input.selectionStart == "number") {
            return input.selectionStart == 0 && input.selectionEnd == input.value.length;
        } else if (typeof document.selection != "undefined") {
            input.focus();
            return document.selection.createRange().text == input.value;
        }
    },
    RemoveSpecialCharacters: function (string) {
        return string.replace(/[^a-zA-Z0-9]/g, '');
    },

    IsDate: function (obj) {
        return Object.prototype.toString.call(obj) == '[object Date]';
    },
    ToInt: function (value) {
        if (Max.PricingAnalysis.IsDate(value)) {
            value = value.toJSON();
        }
        return parseInt(value.replace(/[^\d]*/g, ""), 10) || 0;
    },
    IsNumber: function (n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    },
    IsUndefined: function (obj) {
        return obj === void 0;
    },
    Comma: function (value) {
        value = Max.PricingAnalysis.IsNumber(value) ? value.toString() : "";
        var regex = /(\d+)(\d{3})/;
        while (regex.test(value)) {
            value = value.replace(regex, '$1,$2');
        }
        return value;
    },

    MoneyOrSymbol: function (value, symbol) {
        if (Max.PricingAnalysis.IsUndefined(value) || value === 0) {
            return symbol;
        }
        return "$" + Max.PricingAnalysis.Comma(Math.round(value));
    },

    IsValidCharacter: function (source, evt, maxLength) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        var numberLength = Max.PricingAnalysis.GetCurrencyTextLength(source.value);
        if (charCode != 44 && charCode != 188 && charCode != 36 && charCode != 8 && charCode != 0 && charCode != 40 && charCode != 41 && charCode != 45 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        if (Max.PricingAnalysis.IsTextSelected(source)) {
            return true;
        }
        if (maxLength != undefined && maxLength != null) {
            if (charCode >= 48 && charCode <= 57) {
                return (numberLength < maxLength);
            }

        }
        return true;
    },

    PostData: function PostData(url, data, success, failure) {
        $.ajax({
            url: url,
            dataType: 'html',
            data: data,
            contentType: "application/json",
            success: success,
            error: function (jqXHR, textStatus, errorThrown) {
                failure(errorThrown);
            }
        });
    },
    GetData: function getData(url, success, failure) {
        $.ajax({
            url: url + '?buster=' + new Date().getTime(),
            dataType: 'html',
            type: 'GET',
            contentType: "application/json",
            success: success,
            error: function (jqXHR, textStatus, errorThrown) {
                failure(errorThrown);
            }
        });
    },
    formatDate: function formatDate(d) {
        function addZero(n) {
            return n < 10 ? '0' + n : '' + n;
        }

        return addZero(d.getMonth() + 1) + "/" + addZero(d.getDate()) + "/" + d.getFullYear();
    },
    ParseMoney: function (moneyStr) {
        if (Max.PricingAnalysis.IsNumber(moneyStr))
            return parseInt(moneyStr);
        if (moneyStr == 'N/A')
            return 0;
        else
            return (
                    (moneyStr.indexOf("-") <= 1 && moneyStr.indexOf("-") >= 0) ||
                        (moneyStr.indexOf("(") == 0 && moneyStr.indexOf(")") == moneyStr.length - 1)) ?
                    -1 * Max.PricingAnalysis.ToInt(moneyStr) :
                    Max.PricingAnalysis.ToInt(moneyStr);
    },
    currencyFormat: function currencyFormat(number) {

        if (number == null || number == undefined) {
            return "$" + 0;
        }
        if (number >= 0)
            return Max.PricingAnalysis.MoneyOrSymbol(number);
        else
            return '(' + Max.PricingAnalysis.MoneyOrSymbol(Math.abs(Max.PricingAnalysis.ParseMoney(number))) + ')';
    },
    FormatNumber: function FormatNumber(number) {

        return number == null || number == undefined ? 0 : (number.toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    },

    ParsePercentNumber: function (number) {
        return number == null || number == undefined ? 0 : (number.toString().replace(/\%/g, '').replace(/,/g, ''));
    },

    GetCurrencyTextLength: function GetCurrencyTextLength(number) {
        var numberLength = Math.abs(Max.PricingAnalysis.ParseMoney(number));
        return numberLength.toString().length;
    },
    ShowPricingHistory: function ShowPricingHistory() {
        var url = $('#lnkpricinghistory').attr('post-url');
        Max.PricingAnalysis.GetData(url,
                        function (result) {
                            $("#divPricingHistory").html("");
                            var priceHistoryHtml = "";
                            $.each(JSON.parse(result), function (i, item) {
                                priceHistoryHtml = priceHistoryHtml + '<span class="price-history-date">' + item.historydate + '</span> <span class="price-history-name">' + item.appraisername + '</span> <span class="price-history-amount">' + Max.PricingAnalysis.currencyFormat(item.money) + '</span>';
                            }
                            );
                            $("#divPricingHistory").html(priceHistoryHtml);
                            $("#divPricingHistory").addClass("show-control");
                        },
                        function (message) { alert(message); });
    },
    BindPricingCalcEvents: function () {
        $('#potentialgrossprofit').keypress(function (e) {
            return Max.PricingAnalysis.IsValidCharacter(this, e, 6);
        });


        $('.pricing-calulator-variables').keypress(function (e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
                e.preventDefault();
                $(this).blur();
            }
        });

        $('#btnApproveAd').off("click").on("click", function () {
            var internetprice = $("#internetPrice").val();
            var pMarketAvg = $("#pMarketAvg").val();
            var data = { "internetprice": Max.PricingAnalysis.ParseMoney(internetprice), "pMarketAvg": pMarketAvg };
            var url = $(this).attr('post-url');
            Max.PricingAnalysis.PostData(url, data, function () {
                if ($('#divPricingHistory').css('display') != 'none') {
                    Max.PricingAnalysis.ShowPricingHistory();
                }
                $('#ListPrice').val(Max.PricingAnalysis.ParseMoney(internetprice));
                $('table#jqGrid.ui-jqgrid-btable tbody tr.ui-widget-content.jqgrow.ui-row-ltr.ui-state-highlight td.internet-price').html(internetprice);
                $('table#jqGrid.ui-jqgrid-btable tbody tr.ui-widget-content.jqgrow.ui-row-ltr.ui-state-highlight td.market-avg').html(Max.PricingAnalysis.ParsePercentNumber(pMarketAvg));
            }, function (message) { });

        });


        $('#lnkSavePriceonly').off("click").on("click", function () {
            var internetprice = $("#internetPrice").val();
            internetprice = { "internetprice": Max.PricingAnalysis.ParseMoney(internetprice) };
            var url = $(this).attr('post-url');
            Max.PricingAnalysis.PostData(url, internetprice, function () {
                $('#showSuccess').css('display', 'inline');
                $('#showSuccess').fadeOut(3000);
                $('#ListPrice').val(Max.PricingAnalysis.ParseMoney(internetprice));
                $('table#jqGrid.ui-jqgrid-btable tbody tr.ui-widget-content.jqgrow.ui-row-ltr.ui-state-highlight td.internet-price').html(internetprice);
                $('table#jqGrid.ui-jqgrid-btable tbody tr.ui-widget-content.jqgrow.ui-row-ltr.ui-state-highlight td.market-avg').html(Max.PricingAnalysis.ParsePercentNumber($("#pMarketAvg").val()));
            }, function (message) { alert(message); });

        });

        $('#lnkpricinghistory').off("click").on("click", function () {
            if ($('#arrow').hasClass("calculator-side-arrow")) { // div is not visible.
                $('#arrow').addClass("calculator-down-arrow");
                $('#arrow').removeClass("calculator-side-arrow");
                Max.PricingAnalysis.ShowPricingHistory();
            }
            else {
                $('#arrow').addClass("calculator-side-arrow");
                $('#arrow').removeClass("calculator-down-arrow");
                $("#divPricingHistory").removeClass("show-control");
            }
        });

    },

    BindEvents: function BindEvents() {
        var self = this;
        self.BindPricingCalcEvents();
        $('.pricing-calulator-variables').keypress(function (e) {

            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
                $(this).blur();
            }
        });


        $('#inventorycolor').mouseover(function () {

            var colorText = $("#inventorycolor").text().trim();
            colorText = colorText.replace(/ /g, '');
            var str = "Color:-";
            if (colorText == str) {

            }
            else {
                var boxHeight = $('.arrow_box').height();
                var tMargin = 60 - parseInt(boxHeight);
                tMargin = tMargin + "px";
                $('.arrow_box').css('margin-top', tMargin);
                $('.arrow_box').show();
            }


        });

        $('#inventorycolor').mouseout(function () {
            $('.arrow_box').hide();
        });

        $(".grid-show").click(function () {
            $(".grid-show").hide();
            $(".content").show();
            $("html, body").animate({ scrollTop: $(document).height() }, 1000);
            //$(".grid-show").hide();
            var marketlistingsinput = new Markelistingsinput($('#Vin').val());
            marketlistingsinput.fillGrid(JSON.parse($('#SearchOptions').val()), false);
        });

        $('#btnApproveAd').unbind("click").bind("click", function () {
            var internetprice = $("#internetPrice").val();
            internetprice = { "internetprice": internetprice };
            var url = $(this).attr('post-url');
            Max.PricingAnalysis.PostData(url, internetprice, function () { }, function (message) { alert(message); });
        });

        $('#showdollar0listings').change(function () {
            var marketlistingsinput = new Markelistingsinput($('#Vin').val());
            marketlistingsinput.fillGrid(JSON.parse($('#SearchOptions').val()), false);
        });

        $('#lnkpricinghistory').click(function () {
            $('.pricing-history').toggle();
        });

        $("#ddlChromeStyle").change(function () {
            var ddlSelected = $('#ddlChromeStyle').val();
            var ddlSelecteddata = { "chromeStyleId": ddlSelected };
            var url = $('#hdSaveTrimUrl').val();
            Max.PricingAnalysis.PostData(url, ddlSelecteddata, function () {
                if (ddlSelected != "") {
                    $(".lightbox").hide();
                    Max.PricingAnalysis.ShowLoader();
                    $("#ddlChromeStyle").removeClass("selectList-color");
                    $("#ddlChromeStyle").addClass("selectlist-normal");
                    $(".trim-alert").hide();
                    var oldSearchOptions = JSON.parse($('#overallCriteria').val());
                    oldSearchOptions.Trim = null;
                    oldSearchOptions.Transmission = null;
                    oldSearchOptions.DriveTrain = null;
                    oldSearchOptions.Engine = null;
                    oldSearchOptions.Fuel = null;
                    $('#overallCriteria').val(JSON.stringify(oldSearchOptions));

                    Max.PricingAnalysis.LoadDefineCompetitivePartial($('#ddlChromeStyle').val(), $('#overallCriteria').val(),true);
                }
                else {
                    $(this).addClass("selectList-color");
                    $(".lightbox").show();
                    $(".trim-alert").show();
                }
                $('#ddlChromeStyle').not(this).children().filter(function () {
                    return this.text === "Select Trim...";
                }).remove();

            }, function (message) { });
        });


        $('#btnAutoCheckReport').bind("click", function () {
            postData = { "pullAutoCheckReport": true };
            var url = $(this).attr('post-url');
            Max.PricingAnalysis.PostData(url, postData, function (data) { $('#veicleHistoryReport').html(data); }, function (message) { alert(message); });

        });

        $('#btnPullCarfaxReport').bind("click", function () {
            postData = { "pullCarfaxReport": true };
            var url = $(this).attr('post-url');
            Max.PricingAnalysis.PostData(url, postData, function (data) { $('#veicleHistoryReport').html(data); }, function (message) { alert(message); });

        });

        $('#exportListing').click(function () {
            $('#ExportExel').submit();
        });

        $('#lnkpricinghistory').unbind('click').bind("click", function () {
            if ($('#arrow').hasClass("calculator-side-arrow")) {
                $('#arrow').addClass("calculator-down-arrow");
                $('#arrow').removeClass("calculator-side-arrow");
            }
            else {
                $('#arrow').addClass("calculator-side-arrow");
                $('#arrow').removeClass("calculator-down-arrow");
            }

            var url = $(this).attr('post-url');
            Max.PricingAnalysis.GetData(url,
            function (result) {
                $("#divPricingHistory").toggle();
                //                $("#divPricingHistory").removeClass("hide-control");
                $("#divPricingHistory").html("");
                var priceHistoryHtml = "";
                $.each(JSON.parse(result), function (i, item) {
                    //                    var date = new Date(Date.parse(item.historydate));
                    //                    var formatted = Max.PricingAnalysis.formatDate(date);
                    priceHistoryHtml = priceHistoryHtml + '<span class="price-history-date">' + item.historydate + '</span> <span class="price-history-name">' + item.appraisername + '</span> <span class="price-history-amount">' + Max.PricingAnalysis.currencyFormat(item.money) + '</span>';
                }
                );
                $("#divPricingHistory").html(priceHistoryHtml);
                $("#divPricingHistory").addClass("show-control");
            },

            function (message) { alert(message); });

        });
    },
    formatDate: function formatDate(d) {
        function addZero(n) {
            return n < 10 ? '0' + n : '' + n;
        }

        return addZero(d.getMonth() + 1) + "/" + addZero(d.getDate()) + "/" + d.getFullYear();
    },
    ParseMoney: function (moneyStr) {
        if (Max.PricingAnalysis.IsNumber(moneyStr))
            return parseInt(moneyStr);
        if (moneyStr == 'N/A')
            return 0;
        else
            return (
                    (moneyStr.indexOf("-") <= 1 && moneyStr.indexOf("-") >= 0) ||
                        (moneyStr.indexOf("(") == 0 && moneyStr.indexOf(")") == moneyStr.length - 1)) ?
                    -1 * Max.PricingAnalysis.ToInt(moneyStr) :
                    Max.PricingAnalysis.ToInt(moneyStr);
    },
    currencyFormat: function currencyFormat(number) {

        if (number == null || number == undefined) {
            return "$" + 0;
        }
        if (number >= 0)
            return Max.PricingAnalysis.MoneyOrSymbol(number);
        else
            return '(' + Max.PricingAnalysis.MoneyOrSymbol(Math.abs(Max.PricingAnalysis.ParseMoney(number))) + ')';
    },
    FormatNumber: function FormatNumber(number) {

        return number == null || number == undefined ? 0 : (number.toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    },

    UpdateMatchingCount: function (count, showAlert) {
        if (showAlert) {
            $('#lblMatchingCount').text('N/A');
        } else {
            $('#lblMatchingCount').text(count);
        }
        $('#lblMatchingCountLabel').text('Matching');
    },

    LoadPartialView: function LoadPartialView(url, container) {
        $(container).addClass("loading");
        $.ajax({
            url: url,
            dataType: 'html',
            cache: false,
            success: function (data) {
                $(container).html('');
                $(container).html(data);
                $(container).removeClass("loading");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(container).html('Error Loading data. Please contact Support.');
                $(container).removeClass("loading");
            }
        });
    },

    LoadPageContent: function LoadData() {
        var self = this;
        $('div[action-url]').each(function () {
            self.LoadPartialView($(this).attr('action-url'), this);
        }
        );
    },

    AlignItem: function AlignItem() {
        var lightBoxHeight = $(".ping-header").height() + $(".format-control").offset().top + 2;
        $('.lightbox').css("top", lightBoxHeight);
    },

    ShowLoader: function () {
        var headerHeight = $(".ping-header").height() + $(".format-control").offset().top + 2;
        $('#divDataLoader').css("top", headerHeight);
        var dataLoaderHeight = ($(document).height() - $(".ping-content").offset().top + 10) <= 1500 ? ($(document).height() - $(".ping-content").offset().top + 10) : 1500;
        $('#divDataLoader').css("height", dataLoaderHeight);
        $("#divDataLoader").show();
        $('.loading-ping').css("position", "fixed");
        $('.loading-ping').css("top", "55%");
        $(".loading-ping").show();
    },

    HideLoader: function () {
        $("#divDataLoader").hide();
        $(".loading-ping").hide();
        $(".market-listing-overlay").hide();
    },
    ShowLoaderForMarketListings: function () {
        var loaderTop = $("#ExportExel").height() / 2 + $(".grid-container").offset().top;
        $('.loading-ping').css("top", loaderTop);
        $('.loading-ping').css("position", "absolute");
        $(".loading-ping").show();
    },
    IsLoaderHidden: function () {
        return $("#divDataLoader").is(":hidden");
    },
    LoadMileagePartial: function (searchOption) {
        $('#divMileageParitial').addClass("loading");
        $.ajax({
            url: $('#MileageBarUrl').val(),
            type: 'POST',
            contentType: 'application/json',
            data: searchOption,
            dataType: 'html',
            cache: false,
            success: function (data) {
                $('#divMileageParitial').html('');
                $('#divMileageParitial').html(data);
                var inputs = new SliderInputs();
                inputs.CreateSlider(); 
                if ($('#SearchOptions').val())
                    $('#lblAvgPrice').text(Max.PricingAnalysis.FormatNumber(JSON.parse($('#SearchOptions').val()).AvgMileageValue));
                var cssClass;
                var AvgMileageVal = JSON.parse($('#SearchOptions').val()).AvgMileageValue;
                var callOutTextVal = Max.PricingAnalysis.RemoveSpecialCharacters($('#calloutText').text());

                cssClass = "callout-style";
                if (callOutTextVal >= parseInt($("#EndValue").val(), 10))
                    cssClass = "callout-style-er";
                if (callOutTextVal == 0 || callOutTextVal == 'NA')
                    cssClass = "callout-style-el";

                $('#callout').removeClass("callout-style-el").removeClass("callout-style-er").addClass(cssClass);

                $('#divMileageParitial').removeClass("loading");

            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#divMileageParitial').removeClass("loading");
            }
        });
    },
    UpdateGridData: function (updateGrid) {
        var obj = new searchOptions(JSON.parse($('#SearchOptions').val()));

        $('.dclick').click(function () {
            obj.Show($(this).parent().attr("id"));
        });

        $('.close-box').click(function () {
            obj.Close($(this).attr("id"));
        });

        $('.update-button').click(function () {
            obj.Update($(this).parent().attr("id"), true, true);
        });

        var divList = ["distance", "trim", "transmission", "driveTrain", "engine", "fuel", "certified", "list"];
        for (var i = 0; i < divList.length; i++) {
            obj.Update(divList[i], false, false);
            $('#' + divList[i]).toggle();
        }
        if (updateGrid) {
            var searchOption = JSON.parse($('#SearchOptions').val());
            var gridObject = new Markelistingsinput(searchOption.Vin);

            searchOption.UpdateFacets = true;
            gridObject.fillGrid(searchOption, false);
        }
    },

    LoadDefineCompetitivePartial: function (chromeStyleId, searchOption, updateGrid) {
        var containerDiv = $('#divDefineCompetitiveSet');

        //containerDiv.addClass("loading");
        $.ajax({
            url: $('#SearchFiltersUrl').val(),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({ chromeStyleId: chromeStyleId, searchOption: JSON.parse(searchOption) }),
            dataType: 'html',
            cache: false,
            success: function (data) {
                containerDiv.html('');
                containerDiv.html(data);
                //containerDiv.removeClass("loading");
                Max.PricingAnalysis.UpdateGridData(updateGrid);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //containerDiv.removeClass("loading");
            }
        });
    },

    LoadFacets: function (SelectedEquipment) {
        $('#filter-div').addClass("loading");
        $.ajax({
            url: $('#FacetsUrl').val(),
            dataType: 'html',
            cache: false,
            success: function (data) {
                $('#filter-div').html(data);
                $('#filter-div').removeClass("loading");
                if (SelectedEquipment != null && SelectedEquipment != undefined) {
                    for (i = 0; i < SelectedEquipment.length; i++) {
                        ChangeColor(SelectedEquipment[i], false);
                    }


                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#filter-div').html('Error Loading data. Please contact Support.');
                $('#filter-div').removeClass("loading");
            }
        });
    },

    BindKnockoutEnterEvent: function () {
        ko.bindingHandlers.returnAction = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
                var value = ko.utils.unwrapObservable(valueAccessor());

                $(element).keydown(function (e) {
                    if (e.which === 13) {
                        value(viewModel);
                    }
                });
            }
        };
    },

    LoadPartialView4PricingProof: function LoadPartialView4PricingProof() {
        var data = $("#PPShowAlert").val();

        $.ajax({
            url: $('#PricingProofUrl').val() + '?showAlert=' + data,
            type: 'POST',
            contentType: 'application/json',
            dataType: 'html',
            cache: false,
            success: function (data) {
                $("#pricingproofDisplay").html('');
                $("#pricingproofDisplay").html(data);

                var internetPrice = $("#PPInternetPrice").val();
                var showAlert = $("#PPShowAlert").val();
                var MarketAvg = $("#PPMarketAvg").val();
                UpdateMarketAvg(internetPrice, showAlert, MarketAvg);
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        })
    },

    LoadPricingBar: function (PricingcalculatorModel, showAlert) {

        if (!PricingcalculatorModel.ShowAlert) {
            $('#lblOverallMarketDaysSupply').text(PricingcalculatorModel.OverallMarketAvgCount);
            $('#lblMatchingMarketDaysSupply').text(PricingcalculatorModel.MatchingMarketAvgCount);

            $('#liLowValue').text(Max.PricingAnalysis.currencyFormat(PricingcalculatorModel.MinListPrice));
            $('#liLowPercantage').text("(" + PricingcalculatorModel.LowPercantage.toString() + "%)");
            $('#liAvgPrice').text(Max.PricingAnalysis.currencyFormat(PricingcalculatorModel.AvgListPrice));
            $('#liHighValue').text(Max.PricingAnalysis.currencyFormat(PricingcalculatorModel.MaxListPrice));
            $('#liHighPercantage').text("(" + PricingcalculatorModel.HighPercentage.toString() + "%)");
            $('#spn_TotalRanks').text("of " + PricingcalculatorModel.Count.toString());
            ko.cleanNode(document.getElementById("PCContainer"));
            Max.PricingAnalysis.BindPricingCalcEvents();
            PricingcalculatorModel.InternetPrice = $('#ListPrice').val();
            ko.applyBindings(new AppViewModel(PricingcalculatorModel), document.getElementById("PCContainer"));
            Max.PricingAnalysis.BindKnockoutEnterEvent();
            $('#sliderLabel').show();
            $('#divGaugeValue').show();
            $('#handle').show();
            $('.low-price-alert-gauge').hide();
        } else {

            PricingcalculatorModel.InternetPrice = $('#ListPrice').val();
            ko.cleanNode(document.getElementById("PCContainer"));
            Max.PricingAnalysis.BindPricingCalcEvents();
            PricingcalculatorModel.InternetPrice = $('#ListPrice').val();
            ko.applyBindings(new AppViewModel(PricingcalculatorModel), document.getElementById("PCContainer"));
            Max.PricingAnalysis.BindKnockoutEnterEvent();
            var data = new MarketPricingDto();
            data.MinValue = 0;
            data.MaxValue = 0;
            data.AvgValue = 0;
            data.LowPercentage = 0;
            data.HighPercentage = 0;
            var gauge = new Gauge();
            gauge.createGauge(data);
            $('#handle').hide();
            $('#spn_TotalRanks').text("of N/A");
            $('#pMarketAvg').val("N/A");
            $('#rank').val("-");

            $('.low-price-alert-gauge').show();

            $('#lblOverallMarketDaysSupply').text('N/A');
            $('#lblMatchingMarketDaysSupply').text('N/A');
            $('#lblAvgMileagePricInGame').text('N/A');
            $('#divGaugeValue').hide();
            $('#sliderLabel').hide();
        }
    },

    Init: function Init() {
        var self = this;
        $(function () {
            if ($('#ddlChromeStyle').val() != 0) {
                self.LoadDefineCompetitivePartial($('#ddlChromeStyle').val(), $('#overallCriteria').val(),true);
            } else {
                Max.PricingAnalysis.HideLoader();
            }
            self.BindEvents();
            self.LoadPageContent();
            self.AlignItem();
            self.LoadPartialView4PricingProof();
        });
    }

});
