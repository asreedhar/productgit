﻿function AppViewModel(model) {
    var self = this;
    // Is a given value a number?
    function isNumber(obj) {
        return Object.prototype.toString.call(obj) == '[object Number]';
    };

    // Is a given variable undefined?
    function isUndefined(obj) {
        return obj === void 0;
    };
    function comma(value) {
        value = isNumber(value) ? value.toString() : "";
        var regex = /(\d+)(\d{3})/;
        while (regex.test(value)) {
            value = value.replace(regex, '$1,$2');
        }
        return value;
    };

    function money(value) {
        return moneyOrSymbol(value, "-0-");
    };
    function moneyOrSymbol(value, symbol) {
        if (isUndefined(value) || value === 0) {
            return symbol;
        }
        return "$" + comma(Math.round(value));
    };
    function isDate(obj) {
        return Object.prototype.toString.call(obj) == '[object Date]';
    };
    function toInt(value) {
        if (isDate(value)) {
            value = value.toJSON();
        }
        return parseInt(value.replace(/[^\d]*/g, ""), 10) || 0;
    };

    function percentOrSymbol(value, symbol) {
        if (isNaN(value) || !isFinite(value) || isUndefined(value) || value === 0) {
            return symbol;
        }
        return comma(Math.round(value)) + "%";
    };

    self.is_number=function(n){
        return !isNaN(parseFloat(n)) && isFinite(n);
    };

    self.RemoveSymbol = function (value) {
        if (self.is_number(value))
            return value;
        return value.replace(/[\s,]+/g, ' ').replace('%', '').trim();
    };

    self.format_money = function (intVal, def) {
        if (typeof def === 'undefined')
            def = '$0';
        if (intVal >= 0)

            return moneyOrSymbol(intVal, def);
        else
            return '(' + moneyOrSymbol(Math.abs(intVal), def) + ')';
    };
    self.parse_money = function (moneyStr) {
        
        if (self.is_number(moneyStr))
            return parseInt(moneyStr);
        if (moneyStr == null || moneyStr==undefined || moneyStr == 'N/A')
            return 0;
        else
            return (
                    (moneyStr.indexOf("-") <= 1 && moneyStr.indexOf("-") >= 0) ||
                        (moneyStr.indexOf("(") == 0 && moneyStr.indexOf(")") == moneyStr.length - 1)) ?
                    -1 * toInt(moneyStr) :
                    toInt(moneyStr);
    };

    function sortA(a, b) {
        return parseInt(a) - parseInt(b);
    };

    self.showAlert = model.ShowAlert;
    
    self.internetprice = ko.observable(self.format_money(model.InternetPrice));
    var previousIP = self.parse_money(self.internetprice());
    self.AvgListprice = ko.observable(model.AvgListPrice);
    self.unitcost = ko.observable(self.format_money(model.UnitCost));
    self.listPrices = model.ListingsInternetPrice;

    self.totalListings = model.Count;

    self.marketavg = ko.observable(percentOrSymbol(model.MarketAvg, "0%"));

    var previousmavg = self.marketavg();
    self.rank = ko.observable(model.Rank);
    var previousRank = self.rank();
    self.potentialgrosprofit = ko.observable(self.format_money(model.PotentialGrossProfit));
    var previousgp = self.parse_money(self.potentialgrosprofit());
    var percent = "";

    var data = new MarketPricingDto();
    data.MinValue = model.MinListPrice;
    data.MaxValue = model.MaxListPrice;
    data.AvgValue = model.AvgListPrice;
    data.LowPercentage=model.LowPercantage;
    data.HighPercentage=model.HighPercentage;
    data.RedRisk = model.RedRisk;
    data.GreenRisk = model.GreenRisk;
    data.YellowRisk = model.YellowRisk;

    var gauge = new Gauge();
    gauge.createGauge(data);
    $('#gauge').unbind('click').bind("click", function () {
        var price = gauge.clicked();
        self.internetprice(self.format_money(price));
    });
    gauge.SliderMoved.push(function (value) {
        self.internetprice (self.format_money(value));
    });
    gauge.update_slider(self.parse_money(self.internetprice()));
    UpdateMarketAvg(self.parse_money(self.internetprice()));

    function setMarketAverage(ip) {
        if (self.showAlert === true) {
            return;
        }

            percent = (self.parse_money(ip) / self.RemoveSymbol(self.AvgListprice())) * 100;
            previousmavg = percentOrSymbol(Math.round(percent), "0%");
            self.marketavg(percentOrSymbol(Math.round(percent), "0%"));
            gauge.update_slider(self.parse_money(self.internetprice()));
            UpdateMarketAvg(self.parse_money(self.internetprice()));
            return percentOrSymbol(percent, "0%");
    };
    function setRank(ip) {
        if (self.showAlert===true) {
            return;
        }
        self.listPrices.sort(sortA);
        prevIndex = self.listPrices.indexOf(previousIP);
        self.listPrices.splice(prevIndex, 1);
        self.listPrices[self.listPrices.length] = self.parse_money(ip);
        self.listPrices.sort(sortA);
        var newRank = self.listPrices.indexOf(self.parse_money(ip));
        if (self.parse_money(ip) <= 0) {
            newRank = 0;
        }
        else {
            newRank = newRank + 1;
            newRank = newRank > self.totalListings ? self.totalListings : newRank; 
        }
        previousRank = newRank;
        self.rank(newRank);
        gauge.update_slider(self.parse_money(self.internetprice().toString()));
        UpdateMarketAvg(self.parse_money(self.internetprice()));
        return newRank;

    };

    function setPotentialGrossProfit(ip) {
            previousgp = self.parse_money(self.parse_money(ip) - self.parse_money(self.unitcost()));
            self.potentialgrosprofit(self.format_money(self.parse_money(ip) - self.parse_money(self.unitcost())));
            return self.format_money((self.parse_money(ip) - self.parse_money(self.unitcost())));
    };

    self.calInternetPriceFromPotentialGrossProfit = ko.computed(function () {
        if (self.parse_money(previousgp) != self.parse_money(self.potentialgrosprofit())) {
            var newPotentialGrossProfit = self.parse_money(self.potentialgrosprofit());
            var newinternetprice = 0;
            if (newPotentialGrossProfit < 0) {
                newinternetprice = (self.parse_money(self.unitcost()) - Math.abs(self.parse_money(self.potentialgrosprofit())));
            } else {
                newinternetprice = (self.parse_money(self.unitcost()) + self.parse_money(self.potentialgrosprofit()));
            }
            
            setMarketAverage(newinternetprice);
            setRank(newinternetprice);
            
            previousgp = self.parse_money(self.potentialgrosprofit());
            self.potentialgrosprofit(self.format_money(newPotentialGrossProfit));
            previousIP = newinternetprice;
            self.internetprice(self.format_money(newinternetprice));
            gauge.update_slider(self.parse_money(self.internetprice().toString()));
            UpdateMarketAvg(self.parse_money(self.internetprice()));
            return self.format_money(self.internetprice());
        }
        return;


    });

    self.calInternetPricefromAvg = ko.computed(
    function () {
        if (!parseFloat(self.marketavg().toString().replace('%', ''))) {
            self.marketavg(previousmavg);
            return;
        }
        if (previousmavg != self.marketavg()) {
            var formattedMarketAvg = self.marketavg();
            var str = formattedMarketAvg.toString();
            if (str.indexOf('%') != -1)
                formattedMarketAvg = parseFloat(formattedMarketAvg.replace('%', '').replace(',', ''));
            if (Math.round(percent) == formattedMarketAvg)
                var temp = percent / 100;
            else
                var temp = formattedMarketAvg / 100;
            temp = parseFloat(temp);
            var temp1 = self.AvgListprice();
            var temp2 = temp * temp1;
            var newinternetprice = Math.round(temp2);
            if (newinternetprice.toString().length > 6) 
            {
                self.marketavg(previousmavg);
                return;
            }
            setRank(newinternetprice);
            setPotentialGrossProfit(newinternetprice);
            previousmavg = percentOrSymbol(self.RemoveSymbol(self.marketavg()));
            self.marketavg(previousmavg);
            previousIP = newinternetprice;
            self.internetprice(self.format_money(newinternetprice));
            gauge.update_slider(self.parse_money(self.internetprice().toString()));
            UpdateMarketAvg(self.parse_money(self.internetprice()));
            return self.internetprice();
        }
        return;


    }
    );
    self.calInternetPriceFromRank = ko.computed(
    function () {
        if (self.rank() < 0 || self.rank() > self.totalListings) {
            self.rank(previousRank);
            return self.internetprice();
        }
        if (previousRank != self.rank()) {
            if (self.rank() != self.totalListings) {
                var curentRank = self.rank();
                if (curentRank > previousRank) {
                    var scoreprice = self.listPrices[self.rank()];
                    var newinternetprice = scoreprice - 1;
                    setRank(newinternetprice);
                    setMarketAverage(newinternetprice);
                    setPotentialGrossProfit(newinternetprice);
                    previousIP = newinternetprice;
                    self.internetprice(self.format_money(newinternetprice));
                    gauge.update_slider(self.parse_money(self.internetprice().toString()));
                    UpdateMarketAvg(self.parse_money(self.internetprice()));
                    return self.internetprice();
                }
                else {
                    if (self.rank() == 0) {
                        var newinternetprice = 0;
                        setRank(newinternetprice);
                        setMarketAverage(newinternetprice);
                        setPotentialGrossProfit(newinternetprice);
                        previousIP = newinternetprice;
                        self.internetprice(self.format_money(newinternetprice));
                        gauge.update_slider(self.parse_money(self.internetprice().toString()));
                        UpdateMarketAvg(self.parse_money(self.internetprice()));
                        return self.internetprice();
                    }
                    else {
                        var scoreprice = self.listPrices[self.rank() - 1];
                        var newinternetprice = scoreprice - 1;
                        setRank(newinternetprice);
                        setMarketAverage(newinternetprice);
                        setPotentialGrossProfit(newinternetprice);
                        previousIP = newinternetprice;
                        self.internetprice(self.format_money(newinternetprice));
                        gauge.update_slider(self.parse_money(self.internetprice().toString()));
                        UpdateMarketAvg(self.parse_money(self.internetprice()));
                        return self.internetprice();
                    }
                }
            }
            else {

                var scoreprice = self.listPrices[self.rank() - 1];
                var newinternetprice = scoreprice + 1;
                setRank(newinternetprice);
                setMarketAverage(newinternetprice);
                setPotentialGrossProfit(newinternetprice);
                previousIP = newinternetprice;
                self.internetprice(self.format_money(newinternetprice));
                gauge.update_slider(self.parse_money(self.internetprice().toString()));
                UpdateMarketAvg(self.parse_money(self.internetprice()));
                return self.internetprice();
            }
        }

        return self.internetprice();
    });

    self.calInternetPriceFromInternetPrice = ko.computed(function () {
        if (previousIP != self.parse_money(self.internetprice())) {
            var newinternetprice = self.parse_money(self.internetprice());
            if (newinternetprice.toString().length > 6) newinternetprice = previousIP;
            setRank(newinternetprice);
            setMarketAverage(newinternetprice);
            setPotentialGrossProfit(newinternetprice);
            previousIP = newinternetprice;
            if (newinternetprice < data.MinValue) {
                setRank(self.format_money(data.MinValue));
                setMarketAverage(self.format_money(data.MinValue));
                setPotentialGrossProfit(self.format_money(data.MinValue));
                self.internetprice(self.format_money(data.MinValue));
                gauge.update_slider(self.parse_money(data.MinValue.toString()));
                UpdateMarketAvg(self.parse_money(data.MinValue));
            }
            else if (newinternetprice > data.MaxValue) {
                setRank(self.format_money(data.MaxValue));
                setMarketAverage(self.format_money(data.MaxValue));
                setPotentialGrossProfit(self.format_money(data.MaxValue));
                self.internetprice(self.format_money(data.MaxValue));
                gauge.update_slider(self.parse_money(data.MaxValue));
                UpdateMarketAvg(self.parse_money(data.MaxValue));
            }
            else {
                self.internetprice(self.format_money(newinternetprice));
                gauge.update_slider(self.parse_money(self.internetprice().toString()));
                UpdateMarketAvg(self.parse_money(self.internetprice()));
            }

            if (newinternetprice < data.MinValue && $('#internetPriceEdit').val() == 'Y') {
                setRank(self.format_money(newinternetprice));
                setMarketAverage(self.format_money(newinternetprice));
                setPotentialGrossProfit(self.format_money(newinternetprice));
                self.internetprice(self.format_money(newinternetprice));
                gauge.update_slider(self.parse_money(self.internetprice().toString()));
                UpdateMarketAvg(self.parse_money(self.internetprice()));
            }

            if (newinternetprice > data.MaxValue && $('#internetPriceEdit').val() == 'Y') {
                setRank(self.format_money(newinternetprice));
                setMarketAverage(self.format_money(newinternetprice));
                setPotentialGrossProfit(self.format_money(newinternetprice));
                self.internetprice(self.format_money(newinternetprice));
                gauge.update_slider(self.parse_money(self.internetprice().toString()));
                UpdateMarketAvg(self.parse_money(self.internetprice()));
            }

            $('#internetPriceEdit').val('N');
        }

    });


    $('#internetPrice').keypress(function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if ($(this).val() != $('#internetPriceEdit').attr('data-internetprice')) {
            $('#internetPriceEdit').val('Y');
        }
    });
    $('#internetPrice').focus(function () {
        $('#internetPriceEdit').attr('data-internetprice', $('#internetPrice').val());
    });
}



