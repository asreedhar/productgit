﻿var ctrChart = new CTRChart();
var isDataLoaded = false;
function LoadCtrData(ctrChart) {
    var loading = $("#ctr-graph .loadingGraph");
    var chartData;

    $('<style type="text/css">.hoverGraphArea:hover div.ui-dialog.ui-widget{ display:block}</style>').appendTo('body');
    var invId = $("#CtrGraphLink").attr("data-inventoryId"),
            ymmt = $("label.make-model-style").html(),
            buid = $("#CtrGraphLink").attr("data-businessunitid"),
            stockNum = $("a.stock-number-link").html();

    $('.ui-dialog-title').text(ymmt + " (" + stockNum + ")");
    if (!$("#dialogHeader").length) {
        $('<style id="dialogHeader" type="text/css">.dialogHeader { padding: 0 0 0 12px !important; height: 37 px !important;}   .crossIcon{ background: transparent url(../Themes/MAX3.0/Images/sprite_jQueryUI.png?v=3.0) no-repeat scroll 0 -4px !important; }</style>').appendTo('body');
    }
    $(".ui-dialog-titlebar").addClass("dialogHeader");
    $("a.ui-dialog-titlebar-close").addClass("crossIcon");
    $('#ctr-graph').css("float", "left");
    $(".crossIcon").show();
    //$(this).attr('eventClicked', 'true');
    if (!isDataLoaded)
    {
        $.ajax({
            url: "/merchandising/InventoryMVCGraph.aspx/CtrPrice/" + buid + "/" + invId,
            dataType: "text",
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                chartData = data;
                loading.html('');
                loading.show();
		isDataLoaded = true;
                if ($.isEmptyObject(data.Sources)) {
                    loading.html("<div class='errorGraph'>We're sorry, but there is not enough data for this vehicle.</div>");
                } else {			
                    loading.hide();
                    $("#ctr-graph .carinfo").html($("#ctrGraphHeader").tmpl(data));
                    ctrChart.VehicleData = data;
                    ctrChart.Init();
                }

                if (!$("#dialogHeader").length) {
                    $('<style id="dialogHeader" type="text/css">.dialogHeader { padding: 0 0 0 12px !important; height: 37 px !important;}   .crossIcon{ background: transparent url(../Themes/MAX3.0/Images/sprite_jQueryUI.png?v=3.0) no-repeat scroll 0 -4px !important; }</style>').appendTo('body');
                }
               
            },
            error: function () {
                $("#ctr-graph").dialog('option', 'title', ymmt + " (" + stockNum + ")");
                //$("#ctr-graph").dialog('open');
                loading.html("<div class='errorGraph'>We're sorry, but we can't get data for that vehicle right now.</div>");
            }

        }
        );}
    

}


$(document).ready(function () {
    
    var isClicked = false;
    isDataLoaded = false;
    
    $("#ctr-graph").dialog({
        autoOpen: false,
        width: 840,
        modal: true,
        close: function () {
            isClicked = false;
            if ($.isEmptyObject(ctrChart.Chart)) {
                //ctrChart.Chart.detach();
            }
        }
    });

    $("#CtrGraphLink").on("click", function (event) {
        isClicked = true;
        LoadCtrData(ctrChart);
        $('.ui-dialog.ui-widget').css('display', 'block');
        $("#ctr-graph").dialog('open');
        //$(".ui-widget-overlay").show();
    });

    $(".windowStickerToggle").on("click", function (e) {
        $("#StickerControls").dialog({ modal: true, title: "Print Window Sticker", width: 600, height: 400 });
    });

//    $('#CtrGraphArea').hover(function (e) {
//        $("#CtrGraphLink").attr('eventClicked', 'false');
//        $('.ui-dialog.ui-widget').css('display', 'block');
//        LoadCtrData(ctrChart);
//        $("#ctr-graph").dialog('open');
//	$(".ui-widget-overlay").hide();
//        $('.crossIcon').hide();
//        //$('.ui-dialog.ui-widget').css('top', $(this).offset().top - 20 ).css('display', 'block').css('left', e.pageX - $('.ui-dialog.ui-widget').width());
//        //$('.ui-dialog.ui-widget').css('top', 143 + $(window).scrollTop()).css('display', 'block').css('left', 284).css('width', 849).css('height', 489).addClass('hoverCTR');
//    },
//    function (e) {

//        if (!isClicked) {
//////if ($('.ui-dialog.ui-widget').offset().left < e.clientX < ($('.ui-dialog.ui-widget').offset().left + $('.ui-dialog.ui-widget').width()))
////            $("#ctr-graph").dialog('close');
////            $('.crossIcon').show();
////            $('.ui-dialog.ui-widget').css('display', 'none');
//        }
//    }
//    );
//    $('.ui-dialog.ui-widget').hover(function (e) {
//        
//    },
//        function (e) {
//            if (!isClicked) {
//                $("#ctr-graph").dialog('close');
//                $('.crossIcon').show();
//                $('.ui-dialog.ui-widget').css('display', 'none');
//            }
//        }
//    );
    var totalwidth = $('div.format-control').width() - ($('span.inventory-name').width() + $('span.display-stock-number').width() + 30);

    $("select#selectbox").css("maxWidth", totalwidth);
    $("#ddlChromeStyle").css("maxWidth", totalwidth);

    //Fix for the Firstlook logo.
    $("#firstlooklogo").html('<img id="imgfirstlooklogo" alt="Firstlook" src="/merchandising/Public/Images/fl-logo-white-86x6.png"/>');

});