
function ShowAJAXProgress() {
    var el = document.getElementById("progress");
    if (el != "undefined" && el != null) {
        el.style.display = "block";
        el.style.top = (document.documentElement.scrollTop + 200) + "px";
    }
}

function HideAJAXProgress() {
    var el = document.getElementById("progress");
    if (el != "undefined" && el != null) {
        el.style.display = "none";
        el.style.top = (document.documentElement.scrollTop + 200) + "px";
    }
}
function toggle(elId) {
    var el = document.getElementById(elId);
    if (el != "undefined" && el != null) {
        if (el.style.display == 'block') {
            el.style.display = 'none';
        }else {
            el.style.display = 'block';
        }
    }
}
function show(elId) {
    var el = document.getElementById(elId);
    if (el != "undefined" && el != null) {
        el.style.display = 'block';
    }
}
function hide(elId) {
    var el = document.getElementById(elId);
    if (el != "undefined" && el != null) {
        el.style.display = "none";    
    }
}
function ShowAlertMessage(alertStr) {
    alert(alertStr);
}

Sys.Net.WebRequestManager.add_invokingRequest(ShowAJAXProgress);
Sys.Net.WebRequestManager.add_completedRequest(HideAJAXProgress);

