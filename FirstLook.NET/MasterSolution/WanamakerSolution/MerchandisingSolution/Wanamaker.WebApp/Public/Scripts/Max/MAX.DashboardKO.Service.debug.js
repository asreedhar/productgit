// assumes:
//   MaxDashboardBase

// http://www.jshint.com/  validated regularly
/* global _, ko */

///////////////////////////////////////////////////////////////////////////////
// HELPER CLASSES

// wrapper that can call an internal service endpoint and store the result
var Service = function( options ) {
    "use strict";
    var self = this;

    // properties
    self.dashboard = options.dashboard || null;
    self.enableDebug = options.enableDebug || null;
    self.id = options.id;
    self.auto_start = _(options.auto_start).isUndefined()? true : options.auto_start;
    
    self.service_url = options.url;
    self.service_args = options.args;
    self.dataArgs = options.dataArgs || {};
    
    // sometimes length property is getting corrupted (IE8 only)
    // filter nulls and undefineds that are passed in directly; those can't be used correctly anyway
    self.service_args = _(self.service_args).filter( function( arg ) {
        return (arg !== null && typeof arg != "undefined");
    });

    self.service_data_args = ko.computed(function () {
        if (!self.dataArgs) return {};
        var opts = self.dataArgs;
        var args = {};
        for (var arg in opts) {
            if (typeof opts[arg] == "function") {
                args[arg] = opts[arg]();
            } else {
                args[arg] = opts[arg];
            }
        }
        return args;
    });
    self.jqXHR_settings = {
        type: options.type || "GET",
        dataType: "json",
        cache: false,
        data: self.service_data_args()
    };
    self.jqXHR = ko.observable( null );
    self.enabled = ko.observable( false );
    self.loading = ko.observable( options.auto_start ); // display thing
    self.response = ko.observable( null );
    self.previous_response = ko.observable( null ); // response is moved here on subsequent fetches

    // computed properties
    self.url = ko.computed(function() {
        // prepare url
        var url = self.service_url;
        if (self.service_args !== undefined) {
            for (var i = 0; i < self.service_args.length; ++i) {
                var arg = self.service_args[i];
                var arg_value = ((typeof arg == "function") ? arg() : arg);
                if (arg_value === null || typeof arg_value == "undefined" || _.isNaN(arg_value))
                    return null; // invalid
                url += "/" + arg_value;
            }
        }
        return url;
    });
    

    // handlers
    self.jqXHR_settings.complete = function() {
        self.loading( false );
        // user-supplied function to call only once, and only for this request
        if( self.single_request_on_complete ) {
            if( typeof self.single_request_on_complete == "function" )
                self.single_request_on_complete();
            self.single_request_on_complete = undefined;
        }
    };
    self.jqXHR_settings.success = function( data ) {
        self.response( data );
    };
    self.jqXHR_settings.error = function( xhr, textStatus, errorThrown ) {
        if( self.enableDebug && self.enableDebug() ) {
            if( window.console )
                window.console.log( this.url+" > "+xhr.status+" "+errorThrown );
        } else if( xhr.status === 500 || (xhr.Status === 0 && xhr.readyState === 4) ) {
            window.location.assign("/merchandising/ErrorPage.aspx?Error="+textStatus);
        }
    };
    // methods
    self.fetch = function( on_complete ) {
        var enabled = self.enabled();
        var url = self.url();
        var dataArgs = self.service_data_args();
        if( !enabled )
            return;
        if( url ) {
            if( self.jqXHR() && self.loading() )
                self.jqXHR().abort();
            self.loading( true );
            self.jqXHR( null );
            self.previous_response( self.response() );
            self.response( null );
            self.jqXHR_settings.url = url;
            self.jqXHR_settings.data = dataArgs;
            if( on_complete )
                self.single_request_on_complete = on_complete;
            self.jqXHR( $.ajax( self.jqXHR_settings ));
        }
    };
    // auto-refetch 
    self.url.subscribe( function() {
        self.fetch();
    });
    self.service_data_args.subscribe(function () {
        self.fetch();
    });
};
