/*global Highcharts, _, $, jQuery, _gaq */

var MaxDashboard = (function () {

    "use strict";

    var config = {

        isDebug: false,
        businessUnitId: null,
        inventoryTypeDDL: null,
        isGroup: false,
        analyticsSuiteEnabled: false,
        userPerms: null,
        perfData: null,
        chartWrapper: null,
        chartContainer: null,
        baseURL: "/merchandising/",
        defaultDate: new Date().addMonths(-1), // previous month stats for charts
        maxVendors: {
            "AutoTrader.com": { DestDDL: 2, colors: {} },
            "Cars.com": { DestDDL: 1, colors: {} },
            "Dealer Website": { colors: {} }
        }
    };

    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];

    config.dateRange = {        
        month: config.defaultDate.getMonth() + 1,
        year: config.defaultDate.getFullYear(),
        endMonth: config.defaultDate.getMonth() + 1,
        endYear: config.defaultDate.getFullYear()
    };

    var linearGradient = { x1: 0, y1: 0, x2: 1, y2: 0 };
    config.maxVendors["AutoTrader.com"].colors.linearGradient = linearGradient;
    config.maxVendors["AutoTrader.com"].colors.stops = [[0, 'rgb(225, 123, 26)'], [0.25, 'rgb(255, 162, 42)'], [0.75, 'rgb(255, 162, 42)'], [1, 'rgb(225, 123, 26)']];
    config.maxVendors["AutoTrader.com"].colors.hex = '#FFA22A';
    config.maxVendors["Cars.com"].colors.linearGradient = linearGradient;
    config.maxVendors["Cars.com"].colors.stops = [[0, 'rgb(65, 20, 140)'], [0.25, 'rgb(115, 64, 199)'], [0.75, 'rgb(115, 64, 199)'], [1, 'rgb(65, 20, 140)']];
    config.maxVendors["Cars.com"].colors.hex = '#7340C7';
    config.maxVendors["Dealer Website"].colors.linearGradient = linearGradient;
    config.maxVendors["Dealer Website"].colors.stops = [[0, 'rgb(0, 168, 238)'], [1, 'rgb(15, 137, 187)']];
    config.maxVendors["Dealer Website"].colors.hex = '#0F89BB';

    Highcharts.setOptions({
        chart: {
            style: {
                fontFamily: "Arial, Helvetica, sans-serif"
            }
        }
    });

    $.tablesorter.addWidget({
        id: 'numbering',
        format: function(table) {
            var c = table.config;
            $('tr', table.tBodies[0]).each( function(i) {
                $(this).find('td').eq(0).text(i + 1);
            });
        }
    });
    $.tablesorter.addParser({
        id: 'vdp_trends_table_sort',
        is: function(s) { return false; /*never autodetect*/ },
        type: 'text',
        format: function(s) {
            var d = Date.parse( s );
            if( !d )
                return null;
            if( d.getTime() != NaN )
                return d.toString("yyyy-MM-dd");
            else
                return "0000-01-01";
        }
    });
    $.tablesorter.addParser({
        id: 'currency_sort',
        is: function(s) { return false; },
        type: 'numeric',
        format: function(s) {
            var n = parseInt(s.replace(/\$|\.|,|\s/g,'')); // compare cents as ints
            if( !_(n).isFinite() )
                return 0;
            return n;
        }
    })

    function parseJSONPDate (datestring) {
        return new Date(parseInt(datestring.replace("/Date(", "").replace(")/", ""), 10));
    }

    return {

        /// Initialize
        OnReady: function (settings) {

            $.extend(config, settings);

            var instance = this,
                dateSelect = instance.SetupDatePicker(),
                dateRange = config.dateRange,
                response = {};

            // create our list of service calls
            response.alerts = instance.GetDataModel(config);
            response.performance = instance.GetSitePerformance(config);
            response.analytics = instance.GetAnalytics(config);
            response.ttm = instance.GetTimeToMarket(config);
            if (config.isGroup) {
                response.permissions = instance.GetUserPermissions(config);
                response.cache = instance.GetCacheStatus(config);
            }

            // do something with the service responses
            instance.SetupAlerts(response, config);
            instance.SetupCharts(response, config);
            if(config.isGroup) {
                instance.SetupCacheStatus(response, config);
                instance.SetupUserPermissions(response, config);
            }
            
            // remaining setup that doesn't depend on services
            instance.AddCustomTableSorters();
            instance.InitMaxSeSearch();
            $("#Page").css("visibility", "visible");
            $("#InventoryType").insertBefore("#HeaderSearch");
            if (jQuery.ui) {
                $(".tabs").tabs({
                    selected: 0,
                    select: function (event, ui) {
                        _gaq.push(['_trackEvent', 'Dashboard', 'Click', 'Tab: ' + ui.tab.innerText]);
                        instance.SetTotalsData(ui);
                    }
                });
            }
            
            // hide analytics based on DOM value for store level or by default for GLD
            config.chartWrapper.find(".chart_toggle").on( "click", { wrapper: config.chartWrapper, instance: instance }, instance.ToggleAnalyticsSuite );
            
            if (config.analyticsSuiteEnabled) {
                instance.EnableAnalyticsSuite();
            } else {
                instance.DisableAnalyticsSuite();
            }

            $(".chart-controls select") // #startDate, #endDate
                .html($("#perfDataDateSelect").tmpl(dateSelect))
                .on("change", function (event) {
                    // setup new response object
                    var responseFromDateChange = {};

                    config.chartContainer.find(".error").hide().end().find(".loading").show();
                    
                    instance.DateChangeCheck(event.target, config);
                    responseFromDateChange.performance = instance.GetSitePerformance(config);
                    responseFromDateChange.analytics = instance.GetAnalytics(config);
                    responseFromDateChange.ttm = instance.GetTimeToMarket(config);
                    if (config.isGroup) {
                        // permissions ajax promise should already be resolved
                        responseFromDateChange.permissions = response.permissions;
                    } 

                    instance.SetupCharts(responseFromDateChange, config);
                });

        },

        SetTotalsData: function()
        {
            var updateIt = function() {
                if(config && config.isGroup && config.userPerms && config.gaData && config.perfData)
                {
                    var hash;
                    if($("#chart_stats").is(":visible"))
                    {
                        hash = '#tab_fake_tab_overview';
                    }
                    else if($("#chart_tabs").is(":visible"))
                    {
                        hash = $('#chart_tabs').children("#chart_select").find("li.ui-tabs-active").children('a').attr('href');
                    }
                    switch (hash) {
                        case '#tab_fake_tab_overview':
                            if(config.userPerms && config.userPerms.AnalyticsSuiteBusinessUnits)
                                config.chartWrapper.find(".data-total").html("Data for " + config.userPerms.AnalyticsSuiteBusinessUnits.length + "/" + config.userPerms.UserBusinessUnits.length + " dealerships");
                            break;
                        case '#tab_ttm':
                            if(config.ttmData && config.ttmData.BusinessUnits && config.ttmData.BusinessUnits.length)
                                config.chartWrapper.find(".data-total").html("Data for " + config.ttmData.BusinessUnits.length + "/" + config.userPerms.UserBusinessUnits.length + " dealerships");
                            else
                                config.chartWrapper.find(".data-total").html("&nbsp;");
                            break;
                        case '#tab_perf':
                            config.chartWrapper.find(".data-total").html("Data for " + config.perfData.length + "/" + config.userPerms.UserBusinessUnits.length + " dealerships");
                            break;
                        case '#tab_ga':
                            // "TOTAL" is not a real location, don't count it.
                            config.chartWrapper.find(".data-total").html("Data for " + (config.gaData.length -1) + "/" + config.userPerms.UserBusinessUnits.length + " dealerships");
                            break;
                    }
                }
            }

            _.delay(updateIt, 100); 

        },

        ShowCharts_IEAntiFlickerThrottling: null,
        ShowCharts: function (charts, data, parents, settings) {
            if (parents && parents[0]) {
                parents.find(".loading, .error").hide();
            }
            charts = this.BuildCharts(charts, data);
            this.RenderCharts(charts);

            // fogbugz case 26574
            if( MAXUI && MAXUI.IsIE && MAXUI.IsIE() ) {
                // throttle the anti-flicker logic if it has already executed for this combination of date-dropdown values
                var timespan_hash = $("#startDate").val()+" - "+$("#endDate").val();
                if( timespan_hash != this.ShowCharts_IEAntiFlickerThrottling ) {
                    this.ShowCharts_IEAntiFlickerThrottling = timespan_hash;
                    // hide all charts to be shown
                    _.each( Highcharts.charts, function (chart) {
                        if( !chart )
                            return;
                        var c = $(chart.container);
                        c.hide();
                    });
                    // show them again after a brief (but non-trivial) delay
                    // forcing a DOM redraw in IE
                    setTimeout( function() {
                        _.each( Highcharts.charts, function (chart) {
                            if( !chart )
                                return;
                            var c = $(chart.container);
                            c.show();
                        });
                    }, 15 );
                }
            }
        },

        ShowChartError: function (elem, msg, isHTML) {
            elem.find(".loading").hide().end()
                .find(".error")[isHTML?'html':'text'](msg).show();
        },

        DateChangeCheck: function (target, settings) {
            settings = $.extend({}, config, settings);
            var instance = this,
                dateRange = settings.dateRange,
                startSelect = $("#startDate"),
                endSelect = $("#endDate"),
                startValue = startSelect.val().split("/"),
                endValue = (endSelect[0]) ? endSelect.val().split("/") : false,
                isStartDateChanged = (target.id === "startDate");

            dateRange.month = startValue[0];
            dateRange.year = startValue[1];
            dateRange.endMonth = endValue[0];
            dateRange.endYear = endValue[1];

            if(!endValue) { return; }

            var startDate = new Date(dateRange.year, dateRange.month, 1).addMonths(-1), //month is zero based so getting previous one
                endDate = new Date(dateRange.endYear, dateRange.endMonth, 1).addMonths(-1); //month is zero based so getting previous one
        
            if (startDate > endDate) {
                if (isStartDateChanged) {
                    dateRange.endMonth = dateRange.month;
                    dateRange.endYear = dateRange.year;
                    endSelect.val(dateRange.month + "/" + dateRange.year);
                }
                else {
                    dateRange.month = dateRange.endMonth;
                    dateRange.year = dateRange.endYear;
                    startSelect.val(dateRange.month + "/" + dateRange.year);
                }
            }
        },

        SetupAlerts: function (promise, settings) {
            var instance = this;

            if (settings.isGroup) {
                $.when(promise.permissions, promise.alerts).done(function (permissions, alerts) {
                    config.userPerms = permissions[0];
                    alerts = instance.FilterDealers(alerts[0], permissions[0]);
                    instance.InitGroupBucketLists(alerts, config);
                });
            } else {
                $.when(promise.alerts).done(function (alerts) {
                    instance.InitBucketLists(alerts);
                    instance.InitGauges(alerts);
                });
            }
        },

        SetupCharts: function (promise, settings) {
            settings = $.extend({}, config, settings);
            var instance = this;
            
            if (promise.performance && promise.permissions) {
                
                $.when(promise.performance, promise.permissions).done(function (perfDataResponse, permissions) {
                    
                    var perfData = {},
                        trends = {};

                    if(config.isGroup)
                    {
                        perfData = $.extend({}, perfDataResponse);
                    }
                    else
                    {
                        perfData = $.extend(true, {}, perfDataResponse.Totals);
                        trends = $.extend(true, {}, perfDataResponse.Trends);
                    }

                    perfData = config.perfData = instance.SetPerformance(instance.FilterDealers(perfData[0], permissions[0]));
                    // FB 26115
                    if (config.isGroup) {
                        // deep clone the perfData.Totals so we can use different data sets for the graph and the full report
                        var groupData = $.extend(true, {}, perfData.Totals);
                        var morphedData = instance.CombineDealerSites(groupData);
                        
                        instance.CalculateAveragesBySite(morphedData, perfData.length);
                        instance.InitPerfGraph(morphedData);
                        instance.SetTotalsData();
                    }
                    else {
                        instance.InitPerfGraph(perfData.Totals);
                    }
                    instance.InitFullReport(perfData);
                    
                });

            } else if (promise.performance) {
                
                $.when(promise.performance).done(function (perfDataResponse) {
                    var perfData = {},
                        trendsData = {};
                    if(config.isGroup)
                    {
                        perfData = perfDataResponse;
                    }
                    else
                    {
                        perfData = $.extend(true, {}, perfDataResponse.Totals);
                        trendsData = $.extend(true, {}, perfDataResponse.Trends);
                    }
                    
                    if(perfData != null && perfData != undefined)
                    {
                        perfData = config.perfData = instance.SetPerformance(perfData);
                        instance.InitPerfGraph(perfData.Totals);
                    }


                    // Trends Data - check for empty
                    var domIDs = ["vdp_trend","costper_vdp_trend","lead_trend","costper_lead_trends"];
                    if (_(trendsData).isEmpty()) {
                        _(domIDs).each(function(domID){
                            instance.ShowChartError($('#tab_'+domID), "No data available.");
                        });
                        return;
                    }
                    // Sum Leads types into Total
                    trendsData = instance.UpdateTrends( trendsData );
                    // VDP Trend
                    var domID = "vdp_trend";
                    var chart_init_id = "trends";
                    var tab = $("#tab_"+domID);
                    instance.VDPTrendsReport(trendsData, domID);
                    instance.ShowCharts(chart_init_id, trendsData, tab);
                    // Cost per VDP trend
                    domID = "costper_vdp_trend";
                    chart_init_id = "costper_vdp_trend";
                    tab = $("#tab_"+domID);
                    instance.CostPerVDPTrendsReport(trendsData, domID);
                    instance.ShowCharts(chart_init_id, trendsData, tab);
                    // Leads trend
                    domID = "lead_trend";
                    chart_init_id = "lead_trends";
                    tab = $("#tab_"+domID);
                    instance.LeadsTrendReport(trendsData, domID);
                    instance.InitTrendGraph(trendsData);
                    instance.ShowCharts(chart_init_id, trendsData, tab);
                    // Cost Per Lead trend
                    domID = "costper_lead_trends";
                    chart_init_id = "costper_lead_trends";
                    tab = $("#tab_"+domID);
                    instance.CostPerLeadsTrendReport(trendsData, domID);
                    instance.InitCostPerTrendGraph(trendsData);
                    instance.ShowCharts(chart_init_id, trendsData, tab);
                });
                
            }
            
            if (promise.ttm) {
                
                $.when(promise.ttm).done(function (ttm) {
                    ttm = instance.SetTimeToMarket(ttm);
                    config.ttmData = ttm;
                    if (!config.isGroup) {
                        if (_.isEmpty(ttm) || _.isEmpty(ttm.Days) || ttm.BusinessUnitId === -1) {
                            instance.ShowChartError($("#tab_ttm"), "No data available.");
                            return;
                        }
                        instance.TimeToMarketReport("ttm", ttm);
                        instance.ShowCharts("ttm", ttm, $("#tab_ttm"));
                    } else { // config.isGroup
                        instance.TimeToMarketReport("ttm", ttm);
                        instance.ShowCharts("ttm_g_owp", ttm, $("#tab_ttm_g_owp"));
                        instance.ShowCharts("ttm_g_cao", ttm, $("#tab_ttm_g_cao"));
                        instance.SetTotalsData();
                    }
                });
                
            }

            if (promise.analytics) {
                
                $.when(promise.analytics).done(function (gdata) {
                    
                    instance.SetAnalytics( gdata );
                    
                    if( !config.isGroup ) {
                        if( _.isEmpty( gdata ) || !gdata.Referrals || _.isEmpty( gdata.Referrals.Mobile.Rows ))
                            instance.ShowChartError($("#tab_mobile"), "No data available.");
                        if( _.isEmpty( gdata ) || !gdata.Referrals || _.isEmpty( gdata.Referrals.Sources.Rows ))
                            instance.ShowChartError($("#tab_sources"), "No data available.");
                    } else { // group
                        var gdata_total = _.find(gdata,function(i){return i.BusinessUnit=='TOTAL'});
                        if( _.isEmpty( gdata ) || !gdata_total.Referrals || _.isEmpty( gdata_total.Referrals.Sources.Rows ))
                            instance.ShowChartError($("#tab_sources"), "No data available.");
                    }
                    
                });

            }

        },

        CreateReport: function (options) {
            
            var dialog = { 
                    enabled: true,
                    config: {
                        width: "800px", 
                        height: 410, 
                        modal: true, 
                        autoOpen: false, 
                        position: { 
                            my: "center top+25%", 
                            at: "center top", 
                            of: window
                        }
                    }
                },
                tableSortConfig = {
                    widthFixed: true,
                    sortList: [[0, 1]],
                    parsers: []
                },
                linkAction;

            $.extend(true, dialog, options.dialog);
            $.extend(true, tableSortConfig, options.tableSortConfig);

            if (dialog.enabled) {
                options.report.dialog(dialog.config);
            }

            // delay sorting for IE8
            var sortList = tableSortConfig.sortList
            tableSortConfig.sortList = undefined;

            options.report.empty()
                .html(options.reportTemplate.tmpl(options.reportData))
                .find("table").tablesorter(tableSortConfig);

            // dynamic sort, after tablesorter creation
            // skip if options object says it has no data
            if( !('data_length' in options) || options.data_length > 0 )
                options.report.find("table").trigger('sorton', [sortList]);

            if (dialog.enabled) {
                linkAction = function (e) {
                    options.report.dialog("open").mCustomScrollbar((options.report.hasClass("mCustomScrollbar")) ? "update" : false);
                }; 
            } else {
                linkAction = function (e) {
                    //show the gld report
                    options.report.show();
                };
            }            

            // setup event listeners on chart / details link
            options.reportLink.on("click", linkAction);
        },

        VDPTrendsReport: function (data, domId) {
            var report = { 
                reportLink: $("#"+domId+"_details"), 
                report: $("#"+domId+"_report"), 
                reportTemplate: $("#"+domId+"_report_tmpl"),
                reportData: {
                    Sites: _.map( data, function( site, site_name, data ) {
                        if( !site.length )
                            return null;
                        return {
                            name: site[0].Site.Name,
                            id: site[0].Site.Id
                        };
                    }),
                    Months: []
                },
                tableSortConfig: {
                    sortList: [[1, 1]], // [[col#,order],...]; order -> 0:asc,1:desc
                    headers: {
                        0: { sorter: false },
                        1: { sorter: 'vdp_trends_table_sort' }
                        // [...] other headers are 1:1 with data contents and are thus dynamic, added below
                    },
                    widgets: ['numbering']
                }
            };
            // add sorters for currency/numeric
            var header_offset = 2;
            var site_count = _(data).keys().length;
            for( var i = 0; i < site_count; ++i )
                report.tableSortConfig.headers[i + header_offset] = { sorter: 'currency_sort' };
            // loop through every site array to get the dates
            report.data_length = 0;
            _.each( data, function (months) {
                _.each( months, function (month) {
                    ++report.data_length;
                    var dt = new Date(parseInt(month.DateRange.StartDate.substr(6,13), 10));
                    dt = monthNames[dt.getMonth()].substr(0,3) + " " + dt.getFullYear();
                    // check if same month already exists in array
                    var m = _.find(report.reportData.Months, function (value) {
                        return value.month == dt;
                    });
                    if (!m) {
                        m = {};
                        report.reportData.Months.push(m);
                        m.month = dt;
                        m.vdp = {};
                    }
                    var s = month.Site.Id;
                    m.vdp[s] = month['DetailPageViewCount'];
                });                            
            });

            this.CreateReport(report);
        },

        CostPerVDPTrendsReport: function (data, domId) {
            var filtered_data = _.pick(data, "AutoTrader.com","Cars.com");
            var report = { 
                reportLink: $("#"+domId+"_details"), 
                report: $("#"+domId+"_report"), 
                reportTemplate: $("#"+domId+"_report_tmpl"),
                reportData: {
                    Sites: _.map( filtered_data, function( site, site_name, data ) {
                        if( !site.length )
                            return null;
                        return {
                            name: site[0].Site.Name,
                            id: site[0].Site.Id
                        };
                    }),
                    Months: []
                },
                tableSortConfig: {
                    sortList: [[1, 1]], // [[col#,order],...]; order -> 0:asc,1:desc
                    headers: {
                        0: { sorter: false },
                        1: { sorter: 'vdp_trends_table_sort' }
                        // [...] other headers are 1:1 with data contents and are thus dynamic, added below
                    },
                    widgets: ['numbering']
                }
            };
            // add sorters for currency/numeric
            var header_offset = 2;
            var site_count = _(filtered_data).keys().length;
            for( var i = 0; i < site_count; ++i )
                report.tableSortConfig.headers[i + header_offset] = { sorter: 'currency_sort' };
            // loop through every site array to get the dates
            report.data_length = 0;
            _.each( filtered_data, function (months) {
                _.each(months, function (month) {
                    ++report.data_length;
                    var dt = new Date(parseInt(month.DateRange.StartDate.substr(6,13), 10));
                    dt = monthNames[dt.getMonth()].substr(0,3) + " " + dt.getFullYear();
                    // check if same month already exists in array
                    var m = _.find(report.reportData.Months, function (value) {
                        return value.month == dt;
                    });
                    if (!m) {
                        m = {};
                        report.reportData.Months.push(m);
                        m.month = dt;
                        m.cost = {};
                    }
                    var s = month.Site.Id;
                    m.cost[s] = (month['DetailPageViewCount'] != 0 && month['Budget'] != 0) ? (parseFloat(month['Budget'] / month['DetailPageViewCount']).toFixed(2)) : 0;
                });                            
            });

            this.CreateReport(report);
        },

        LeadsTrendReport: function (data, domId) {
            var filtered_data = _.pick(data, "AutoTrader.com","Cars.com");
            // var v = { // visibility
            //     phone: $('#leadPhone').prop('checked'),
            //     email: $('#leadEmail').prop('checked'),
            //     chat: $('#leadChat').prop('checked')
            // };
            var report = { 
                reportLink: $("#"+domId+"_details"), 
                report: $("#"+domId+"_report"), 
                reportTemplate: $("#"+domId+"_report_tmpl"),
                reportData: {
                    Sites: _.map( filtered_data, function( site, site_name, data ) {
                        if( !site.length )
                            return null;
                        return {
                            name: site[0].Site.Name,
                            id: site[0].Site.Id
                        };
                    }),
                    Months: []/*,
                    Visible: v*/
                },
                tableSortConfig: {
                    sortList: [[1, 1]], // [[col#,order],...]; order -> 0:asc,1:desc
                    headers: {
                        0: { sorter: false },
                        1: { sorter: 'vdp_trends_table_sort' },
                        2: { sorter: 'currency_sort' }, // phone
                        3: { sorter: 'currency_sort' }, // email
                        4: { sorter: 'currency_sort' }, // chat
                        5: { sorter: 'currency_sort' }  // total
                    },
                    widgets: ['numbering']
                }
            };
            // loop through every site array to get the dates
            report.data_length = 0;
            _.each( filtered_data, function (months) {
                _.each(months, function (month) {
                    ++report.data_length;
                    var dt = new Date(parseInt(month.DateRange.StartDate.substr(6,13), 10));
                    dt = monthNames[dt.getMonth()].substr(0,3) + " " + dt.getFullYear();
                    // check if same month already exists in array
                    var m = _.find(report.reportData.Months, function (value) {
                        return value.month === dt;
                    });
                    if (!m) {
                        m = {};
                        report.reportData.Months.push(m);
                        m.month = dt;
                        m.phone = {};
                        m.email = {};
                        m.chat = {};
                        m.total = {};
                    }
                    var s = month.Site.Id;
                    m.phone[s] = month['PhoneLeadsCount'];
                    m.email[s] = month['EmailLeadsCount'];
                    m.chat[s] = month['ChatRequestsCount'];
                    //m.total[s] = (v['phone']? m.phone[s]:0) + (v['email']? m.email[s]:0) + (v['chat']? m.chat[s]:0);
                    m.total[s] = m.phone[s] + m.email[s] + m.chat[s];
                });                            
            });

            this.CreateReport(report);
            
            if( report.report.hasClass('ui-tabs') )
                report.report.tabs('destroy');
            report.report.tabs();
        },

        CostPerLeadsTrendReport: function (data, domId) {
            var filtered_data = _.pick(data, "AutoTrader.com","Cars.com");
            var report = { 
                reportLink: $("#"+domId+"_details"), 
                report: $("#"+domId+"_report"), 
                reportTemplate: $("#"+domId+"_report_tmpl"),
                reportData: {
                    Sites: _.map( filtered_data, function( site, site_name, data ) {
                        if( !site.length )
                            return null;
                        return {
                            name: site[0].Site.Name,
                            id: site[0].Site.Id
                        };
                    }),
                    Months: []
                },
                tableSortConfig: {
                    sortList: [[1, 1]], // [[col#,order],...]; order -> 0:asc,1:desc
                    headers: {
                        0: { sorter: false },
                        1: { sorter: 'vdp_trends_table_sort' }
                        // [...] other headers are 1:1 with data contents and are thus dynamic, added below
                    },
                    widgets: ['numbering']
                }
            };
            // add sorters for currency/numeric
            var header_offset = 2;
            var site_count = _(filtered_data).keys().length;
            for( var i = 0; i < site_count; ++i )
                report.tableSortConfig.headers[i + header_offset] = { sorter: 'currency_sort' };
            // loop through every site array to get the dates
            report.data_length = 0;
            _.each( filtered_data, function (months) {
                _.each(months, function (month) {
                    ++report.data_length;
                    var dt = new Date(parseInt(month.DateRange.StartDate.substr(6,13), 10));
                    dt = monthNames[dt.getMonth()].substr(0,3) + " " + dt.getFullYear();
                    // check if same month already exists in array
                    var m = _.find(report.reportData.Months, function (value) {
                        return value.month === dt;
                    });
                    if (!m) {
                        m = {};
                        report.reportData.Months.push(m);
                        m.month = dt;
                        m.cost = {};
                    }
                    var s = month.Site.Id;
                    var total = month['PhoneLeadsCount'] + month['EmailLeadsCount'] + month['ChatRequestsCount']; // should agree with TotalLeads
                    m.cost[s] = (total != 0 && month['Budget'] != 0) ? (parseFloat(month['Budget'] / total).toFixed(2)) : 0;
                });                            
            });

            this.CreateReport(report);
        },

        TimeToMarketReport: function (id, data) {
            if (data.Vehicles && !_.isEmpty(data.Vehicles)) { // single store
                var report = {
                    report: $("#"+id+"_report"),
                    reportLink: $("#chart_"+id+", #"+id+"_details"),
                    reportTemplate: $("#"+id+"_report_tmpl"),
                    reportData: data,
                    tableSortConfig: {
                        widthFixed: true,
                        sortList: [[3, 1]],
                        headers: { 0: { sorter: false }},
                        widgets: ['numbering']
                    }
                };
                $("#tab_"+id+" .detaillinks").removeClass("hidden");
                this.CreateReport(report);
                
            } else if (data.BusinessUnits && !_.isEmpty(data.BusinessUnits)) { // group
                var report = {
                    report: $("#"+id+"_report"),
                    reportLink: $("#chart_"+id+", #"+id+"_details"),
                    reportTemplate: $("#"+id+"_report_tmpl"),
                    reportData: data,
                    dialog: {
                        enabled: false
                    },
                    tableSortConfig: {
                        widthFixed: true,
                        sortList: [[3, 1]],
                        headers: { 0: { sorter: false }},
                        widgets: ['numbering']
                    }
                };
                $("#tab_"+id+" .detaillinks").removeClass("hidden");
                this.CreateReport(report);
            }
        },

        SetupCacheStatus: function (promise, settings) {
            settings = $.extend({}, config, settings);
            var instance = this;

            promise.cache.done(function (data) {
                instance.DisplayCacheStatus(data);
                if (data.Status === "Pending" || data.Status === "Processing") {
                    // run refresh cache to keep checking status
                    instance.RefreshCache(true);
                }
            });
        },

        SetupUserPermissions: function (promise, settings) {
            settings = $.extend({}, config, settings);
            var instance = this;
            
            promise.permissions.done(function (data) {
                // store them locally
                config.userPerms = data;
                instance.SetTotalsData();

                // check for BUs with analytics enabled
                var permissibleAnalyticsBUs = _.intersection(config.userPerms.AnalyticsSuiteBusinessUnits, config.userPerms.UserBusinessUnits);
                if( !_.isEmpty( permissibleAnalyticsBUs )) {
                    if( !config.analyticsSuiteEnabled ) {
                        config.analyticsSuiteEnabled = true;
                        instance.EnableAnalyticsSuite();
                    }
                }
            });
        },

        DisableAnalyticsSuite: function() {
            var instance = this;
            instance.ShowStats();
            config.chartWrapper.find(".toggle").hide();
            config.chartWrapper.find(".chart_toggle").css('cursor', 'auto')
        },

        EnableAnalyticsSuite: function() {
            var instance = this;
            instance.ShowTabs();
            config.chartWrapper.find(".toggle").show();
            config.chartWrapper.find(".chart_toggle").css('cursor', 'pointer')
        },

        ShowStats: function() {
            if(!$("#chart_stats").is(':visible'))
                config.chartWrapper.find(".toggle").click();
        },

        ShowTabs: function() {
            if(!$("#chart_tabs").is(':visible'))
                config.chartWrapper.find(".toggle").click();
        },

        ToggleAnalyticsSuite: function (event) {
            var id = event.data.wrapper,
                chart1 = $("#chart_stats"),
                chart2 = $("#chart_tabs"),
                reportlink = id.next(".report-link"),
                instance = event.data.instance;

            if( !config.analyticsSuiteEnabled && chart1.is(':visible'))
                return; // block

            chart1.toggle();
            chart2.toggle();
            if (chart1.is(":visible")/* || !config.analyticsSuiteEnabled*/) {
                id.find("h1").text("Online Classified Overview");
                if ($("#reports").is(":visible")) { $("#reports .close").click(); }

                instance.SetTotalsData();

                reportlink.data('shouldShow', false);
                reportlink.hide();
            } else {
                id.find("h1").text("Digital Performance Analytics");
                instance.SetTotalsData();
                reportlink.data('shouldShow', true);
                reportlink.show();
            }
        },

        DisplayStatusMessage: function (type, message) {

            $("#UserMessaging").remove();

            var messageBox = $("<div />").attr("id", "UserMessaging"),
                messageText = $("<p />").addClass(type.toLowerCase()).text(message);

            $(messageBox).append(messageText);
            $("#Body").prepend(messageBox);

        },

        GetData: function (args) {
            
            var ajaxConfig = {
                    url: args.url,
                    type: (args.requesttype) ? args.requesttype : "GET"
                },
                error;

            // default error handling (redirects on any ajax fail)
            error = function (jqXHR, textStatus, errorThrown) {
                if( jqXHR.status >= 400 ) { // server returned error code, for which we will have server-side stacktrace
                    window.location.assign("/merchandising/ErrorPage.aspx?Error="+textStatus);
                } else { // error occurred after non-error server response, do not redirect
                    //console.log( jqXHR.status+' '+textStatus );
                }
            };

            // for debug, don't redirect the browser
            if (config.isDebug) {
                error = function (jqXHR, textStatus, errorThrown) {
                    //console.log( jqXHR.status+' '+textStatus );
                };
            }
            
            // use the passed in error handling, even if debug
            ajaxConfig.error = args.error || error;

            // use global ajax function if it exists
            if (typeof MAXUI !== "undefined") {
                return MAXUI.maxAjax(ajaxConfig);
            } else {
                return $.ajax({
                    url: ajaxConfig.url,
                    type: ajaxConfig.requesttype,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    error: ajaxConfig.error
                });
            }
        },

        GetDataModel: function (settings) {
            settings = $.extend({}, config, settings);
            var instance = this,
                request = {},
                response;

            request.url = settings.baseURL;
            request.url += (settings.isGroup) ? "GroupDashboardData.aspx/" : "DashboardData.aspx/";
            request.url += settings.businessUnitId + "/" + settings.inventoryTypeDDL;

            response = instance.GetData(request);
            return response;
        },

        GetTimeToMarket: function (settings) {
            settings = $.extend({}, config, settings);
            var instance = this,
                request = {},
                response;

            request.url = settings.baseURL;
            request.url += (settings.isGroup) ? "GroupTimeToMarketReport.aspx/" : "TimeToMarketReport.aspx/";
            request.url += settings.businessUnitId + "/" + config.inventoryTypeDDL + "/" + settings.dateRange.month + "/" + settings.dateRange.year + "/" + settings.dateRange.endMonth + "/" + settings.dateRange.endYear;
            response = instance.GetData(request);
            return response;
        },

        GetSiteTrends: function (settings) {
            settings = $.extend({}, config, settings);
            var instance = this,
                request = {},
                response;

            request.url = settings.baseURL + "SitePerformanceTrend.aspx/";
            request.url += settings.businessUnitId + "/" + config.inventoryTypeDDL + "/" + settings.dateRange.month + "/" + settings.dateRange.year + "/" + settings.dateRange.endMonth + "/" + settings.dateRange.endYear;
            response = instance.GetData(request);
            return response;
        },

        GetAnalytics: function (settings) {
            settings = $.extend({}, config, settings);
            var instance = this,
                request = {},
                response;

            request.url = settings.baseURL;
            request.url += (settings.isGroup) ? "GroupGoogleAnalyticsDataAll.aspx/" : "GoogleAnalyticsDataAll.aspx/";
            request.url += settings.businessUnitId + "/1/" + settings.dateRange.month + "/" + settings.dateRange.year + "/" + settings.dateRange.endMonth + "/" + settings.dateRange.endYear + "/25";
            response = instance.GetData(request);

            return response;
        },

        GetUserPermissions: function (settings) {
            settings = $.extend({}, config, settings);
            var instance = this,
                request = {},
                response;

            request.url = settings.baseURL + "User.aspx/permissions/";
            request.url += settings.businessUnitId;

            response = instance.GetData(request);
            return response;
        },

        GetSitePerformance: function (settings) {
            settings = $.extend({}, config, settings);
            var instance = this,
                request = {},
                response;

            request.url = settings.baseURL;
            request.url += (settings.isGroup) ? "GroupSitePerformanceForMonth.aspx/" : "SitePerformanceTrend.aspx/";
            request.url += settings.businessUnitId + "/" + config.inventoryTypeDDL + "/" + settings.dateRange.month + "/" + settings.dateRange.year;
            request.url += "/" + settings.dateRange.endMonth + "/" + settings.dateRange.endYear;
            

            response = instance.GetData(request);
            return response;
        },

        // returns HH:MM:SS string from GA milliseconds
        FriendlyTimeFormat: function (time) {
            var hours = parseInt(time / 3600, 10) % 24,
                minutes = parseInt(time / 60, 10) % 60,
                seconds = parseInt(time % 60, 10);
            return (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
        },

        SetMobileData: function (data) {
            var mobilePercent = (data.mobile.TotalsForAllResults.Visits/data.sources.TotalsForAllResults.Visits)*100;
            return [["Mobile", mobilePercent],["Desktop/Laptop", 100 - mobilePercent]];
        },

        // special labesl for organic/paid (hide "direct")
        FormatSources: function (source) {
            var separator = source.indexOf("|"),
                medium = source.substr(separator+1);
            
            source = source.substr(0, separator);
            if (medium === "organic") {
                source += " " + medium;
            } else if (_.contains(["cpc","ppc"], medium)) {
                source += " (paid)";
            }
            return source;
        },

        SetAnalytics: function (garesponses) {

            var instance = this,
                parents = $("#tab_ga"),
                complete = garesponses,
                rows = {};

            if (_.isArray(garesponses)) { 
                complete = _.findWhere(garesponses, { BusinessUnitId: config.businessUnitId });
            }

            config.gaData = garesponses;

            if (!complete || typeof(complete.Referrals) == 'undefined') { return false; }

            complete = { 
                sources: complete.Referrals.Sources,
                keywords: complete.Referrals.Keywords,
                mobile: complete.Referrals.Mobile
            };

            // calculate % of total visits, set the key as a property so we can convert to an array, and format timestamps
            _.each(complete, function (set, setkey, original) {
                // count the number of the actual visits accumulated in this metric.
                var totalVisits = _.reduce(set.Rows, function(memo, row){
                    return memo + row.Visits;
                }, 0);

                rows[setkey] = _.map(set.Rows, function (row, key) {
                    row.Percentage = (row.Visits / totalVisits) * 100;
                    row.Key = (setkey==="sources") ? instance.FormatSources(key) : key;
                    row.AverageVisitDuration = instance.FriendlyTimeFormat(row.AverageVisitDuration);
                    return row;
                });
            });


            instance.ShowCharts("mobile", instance.SetMobileData(complete));
            // Sort the sources by Visits, descending, and take the top 10 for the chart.
            instance.ShowCharts("sources", _.first( _.sortBy(rows.sources, function(row) { return row.Visits }).reverse(), 10), parents);
            instance.CreateAnalyticsReports(rows);
            return true; // otherwise it displays no data
        },
                
        CreateAnalyticsReports: function (data) {
            var instance = this,
                reports = {};

            _.each(data, function (type, key) {
                reports[key] = {
                    tableSortConfig: {
                        sortList: [[3, 1]],
                        headers: { 0: { sorter: false }},
                        widgets: ['numbering']
                    }
                };
                if (config.isGroup) {
                    reports[key].dialog = { enabled: false };
                }
            });

            $.extend(reports.sources, {
                report: $("#ga_sources_report"),
                reportLink: $("#ga_sources_details"),
                reportTemplate: $("#ga_sources_tmpl"),
                reportData: { source: data.sources }                
            });

            $.extend(reports.keywords, {
                report: $("#ga_keywords_report"),
                reportLink: $("#chart_ga_sources"),
                reportTemplate: $("#ga_keywords_tmpl"),
                reportData: { keyword: data.keywords }
            });

            $.extend(reports.mobile, {
                report: $("#ga_mobile_report"),
                reportLink: $("#chart_ga_mobile, #ga_mobile_details"),
                reportTemplate: $("#ga_mobile_tmpl"),
                reportData: {source: data.mobile }
            });

            _.each(reports, function (report) {
                try {
                    instance.CreateReport(report);
                    report.report.find('.tablesorter').trigger('sorton',[report.tableSortConfig.sortList]);
                } catch( e ) {
                    ; // hmm
                }
            });
        },

        FilterDealers: function (groupdata, userdata) {

            var instance = this,
                filtered;

            // store level dashboard will not pass an array, so it will skip the filtering and just return that store
            if (!_.isArray(groupdata)) { return groupdata; }

            filtered = _.filter(groupdata, function (dealer) {
                return _.contains(userdata.UserBusinessUnits, dealer.BusinessUnitId);
            });

            return filtered;
        },

        // get the number of months (with decimal for partial current month)
        GetMonthRange: function (start, end) {        
            var today = new Date(),
                difference = (end.getMonth()+1) - start.getMonth() + (12 * (end.getFullYear() - start.getFullYear())),
                includesCurrentMonth = (end.getMonth() === today.getMonth() && end.getFullYear() === today.getFullYear()),
                totalDaysInCurrentMonth = Date.getDaysInMonth(today.getFullYear(), today.getMonth());

            if(includesCurrentMonth) {  difference = (difference - 1) + (today.getDate()/totalDaysInCurrentMonth); }
            return difference;
        },

        SetTimeToMarket: function (data) {

            var instance = this/*,
                parents = $('#tab_ttm')*/;

            if (!config.isGroup)
                data = { "BusinessUnits": [data] };

            _.each( data.BusinessUnits, function (dealer, dealerkey) {

                var totals = {},
                    averages = {};

                // format date and sum averages
                _.each(dealer.Days, function (day) {
                    _.each(day, function (value, dayproperty, original) {
                        if (dayproperty === "Date") {
                            original.DayDate = parseJSONPDate(value).clearTime();
                        } else {
                            totals[dayproperty] = totals[dayproperty] ? totals[dayproperty] += value : value;
                        }
                    });
                });

                _.each(dealer.Vehicles, function (vehicle) {
                    var dates = ["DateAddedToInventory", "DateOfFirstPhoto", "DateOfFirstCompleteAd"];
                    _.each(dates, function (dateproperty) {
                        vehicle[dateproperty] = parseJSONPDate(vehicle[dateproperty]);
                    });
                });

                dealer.Days = _.sortBy(dealer.Days, function (day) {
                    return day.Date;
                });

                dealer.Totals = totals;

            });

            if (!config.isGroup)
                data = data.BusinessUnits[0];

            return data;
        },

        SetPerformance: function (data, settings) {
            // allow config override (useful for testing)
            $.extend(config, settings);
            var instance = this,
                totals = { Sites: {} },
                datatypes = {
                    percent: ["ConversionRate", "ClickThroughRate"],
                    string: ["SiteName"],
                    number: ["Budget", "AdPrintValue", "ActionValue", "ChatValue", "DetailValue", "EmailValue", "MapValue", "PhoneValue", "SearchValue"]
                };

            // if single store (exec. dashboard), create single array just to use same code
            if (!config.isGroup) { data = [data]; }

            _.each(data, function (dealer, dealerkey) {

                var perfdata,
                    start,
                    end,
                    difference,
                    months,
                    today = new Date();

                if (config.isGroup) {
                    perfdata = dealer.Data;
                } else {
                    perfdata = dealer;
                }

                // fix for cached data using incorrectly capitalized key
                if(perfdata.Enddate) { perfdata.EndDate = perfdata.Enddate; }

                //hotfix FB 26194
                var mydate = perfdata.TimePeriod;
                start = (_.isDate(perfdata.StartDate)) ? perfdata.StartDate : new Date(mydate.substr(0, mydate.indexOf(" - ")));//parseJSONPDate(perfdata.StartDate);
                end = (_.isDate(perfdata.EndDate)) ? perfdata.EndDate : new Date(mydate.substr(mydate.indexOf(" - ")+3));//parseJSONPDate(perfdata.EndDate);

                months = instance.GetMonthRange(start, end);
                var monthsRoundedUp = (Math.ceil(months * 1)/1);

                if (config.isGroup) {
                    data[dealerkey].Data.StartDate = start;
                    data[dealerkey].Data.EndDate = end;
                } else {
                    data[dealerkey].StartDate = start;
                    data[dealerkey].EndDate = end;
                }

                // set StartDate/EndDate once (since they should all be the same)
                if (!_.has(totals, "StartDate")) { totals.StartDate = start; }
                if (!_.has(totals, "EndDate")) { totals.EndDate = end; }

                // FB 26118 -- superceded by implementation of 26115
                //if (config.isGroup) { 
                //    perfdata.Sites = _.pick(perfdata.Sites, "AutoTrader.com","Cars.com");
                //}
                // click-thru, conversion fix
                _.each( _.pick(perfdata.Sites, "AutoTrader.com","Cars.com"), function (site) {
                    site.ShowAdvertisingEffectiveness = true;
                });
                
                _.each(perfdata.Sites, function (site, sitekey, original) {
                    
                    // exclude this site object from the totals if it contains "no real data"/all zeroes
                    var count = _.compact( _.map( site, function( val ){ return parseFloat( val ); })).length;
                    if( count == 0 )
                        return;

                    var smonths = site.Months;
                    var smonthsRoundedUp = site.Months;

                    // create site object in totals, if not already set
                    if (!_.has(totals.Sites, sitekey)) { totals.Sites[sitekey] = { Count: 0 }; }
                    if (config.isGroup && !_.has(totals.Sites[sitekey], "WithBudget")) { totals.Sites[sitekey].WithBudget = { Count: 0}; }
                    site.ProratedBudget = site.Budget/site.Months;

                    _.each(site, function (stat, statkey) {

                        var total;
                        // sum everything except sitename
                        if (statkey !== "SiteName") {

                            if (!_.contains(["ClickThroughRate","ConversionRate"], statkey)) {
                                if (statkey==="Budget") {
                                    // budget should not use "weighted" average
                                    stat = Math.abs(stat/smonths);
                                } else if (statkey==="ProratedBudget") {                                    
                                    var average = stat/smonths;
                                    stat = Math.abs((stat - average + (average*(smonths % 1)))/smonths);
                                } else if (statkey==="BenchMarkLeadCost" || statkey==="BenchMarkImpressionCost") {
                                    ; // stat should remain static
                                } else {
                                    // average based on # of months selected
                                    var amount = (smonths<1) ? smonths : smonths;
                                    stat = Math.abs(stat/amount);
                                }
                            }

                            // separate tally for dealers/sites with budgets set (needed for cost per vdp/lead)
                            if (config.isGroup) {

                                if (_.contains(datatypes.number, statkey) && (!_.isNull(site.Budget) && site.Budget > 0)) {
                                    var wvalue = (_.isUndefined(totals.Sites[sitekey].WithBudget[statkey])) ? 0 : totals.Sites[sitekey].WithBudget[statkey];
                                    totals.Sites[sitekey].WithBudget[statkey] = (_.isUndefined(wvalue)) ? stat : (wvalue += stat);
                                }
                                total = (_.isUndefined(totals.Sites[sitekey][statkey])) ? null : totals.Sites[sitekey][statkey];
                                total = (_.isNull(total) && _.isNull(stat)) ? null : total += stat;
                            }

                        }

                        totals.Sites[sitekey][statkey] = (config.isGroup && statkey !== "SiteName") ? total : stat;
                        original[sitekey][statkey] = stat;

                        if(config.maxVendors[sitekey] && config.maxVendors[sitekey].DestDDL) {
                            totals.Sites[sitekey].BudgetLink = config.baseURL+"CustomizeMAX/DealerPreferences.aspx?t=Reports&d=" + (start.getMonth() + 1) + "/" + start.getFullYear() + "&DestDDL=" + config.maxVendors[sitekey].DestDDL;
                        }
                    });

                    // count # dealers w/data for each site to determine average CTR and conversion
                    if(config.isGroup)
                    {
                        if(!_.isNull(site.Budget) && site.Budget > 0)
                        {
                            totals.Sites[sitekey].WithBudget["Count"]++;
                        }
                    }
                    totals.Sites[sitekey].Count++;
                });

                if (config.isGroup) {
                    data[dealerkey].Data = instance.UpdateLeads(perfdata);
                } else {
                    data[dealerkey] = instance.UpdateLeads(perfdata);
                }
                totals = instance.UpdateLeads(totals);

            });

            // divide by count to get average CTR and conversion (CPL, CPV calculated later)
            if (config.isGroup) {
                _.each(totals.Sites, function (site, sitekey) {
                    _.each(datatypes.percent, function (statkey) {
                        totals.Sites[sitekey][statkey] = parseFloat((totals.Sites[sitekey][statkey] / totals.Sites[sitekey].Count).toFixed(1));
                    });
                });
            }

            data.Totals = totals;

            return data;

        },

        CombineDealerSites: function (totals) {
            // We don't want to show all the different dealer sites on the graph, just AutoTrader, Cars and a combined "Dealer Web Sites"
            var sitesData = totals.Sites;
            var sitesToReturn = _.pick(sitesData, "AutoTrader.com", "Cars.com");
            var sitesToCombine = _.omit(sitesData, "AutoTrader.com", "Cars.com");
            var combinedSite = undefined;

            for (var csKey in sitesToCombine) {
                // if we have a combineable site...
                if (csKey != undefined) {
                    var site = sitesToCombine[csKey];

                    // if this is our first combineable site
                    if (combinedSite == undefined) {
                        combinedSite = site;
                    }
                    else {
                        // we already have a combineable site so we want to add to it.
                        for (var prop in site) {
                            // only combine number values
                            if (!isNaN(Number(site[prop]))) {
                                combinedSite[prop] += Number(site[prop]);
                            }
                        }
                    }
                }
            }

            // If we have a combined site, set the name accordingly
            if (combinedSite != undefined) {
                combinedSite.SiteName = "Dealer Web Sites";
                sitesToReturn["Dealer Web Sites"] = combinedSite;
            }
            totals.Sites = sitesToReturn;

            return totals;
        },

        CalculateAveragesBySite: function( sites, dealerCount ) {
            var avg_fields = [
                // Digital Performance Analytics - Online Classified Performance - {common}
                'Budget',
                // Digital Performance Analytics - Online Classified Performance - Cost Per Consumer (VDP) Impression
                'DetailValue','CostPerVDP','DetailPerVehicle',
                // Digital Performance Analytics - Online Classified Performance - Cost Per Direct Lead
                'LeadValue','EmailValue','PhoneValue','ChatValue','CostPerLead',
                // Online Classified Overview - {common}
                'SearchValue',/*'DetailValue',*/'ActionValue'
            ];
            var instance = this;

            _.each( sites.Sites, function( val, key, list ) {
                _.each( avg_fields, function( field ) {
                    var denominator = (field == 'Budget' && config.isGroup) ? val.WithBudget.Count : val.Count;

                    list[key]['Average'+field] = val[field] / denominator;
                });
                // special exceptions due to inconsistencies
                list[key]['CostPerVDP'] = list[key]['Budget']/list[key]['DetailValue'];
            });

            if(config.isGroup && !_.isUndefined(sites.StartDate))
            {
                instance.UpdateLeads(sites);
            }
        },

        InitGroupBucketLists: function (alertData, settings) {
            settings = $.extend({}, config, settings);
            var instance = this,
                invtype = settings.inventoryTypeDDL,
                threshold = 15; // default % of inventory

            function createAlertList(domId, tmplId, stat, limit) {
                var data = _.chain(alertData)
                    .filter(function (dealer) { return ((dealer.Data.BucketsData[stat] / dealer.Data.BucketsData.SelectedFilterCount) * 100) > limit; })
                    .sortBy(function (dealer) { return -dealer.Data.BucketsData[stat]; })
                        .value(),
                    update = ($(domId).hasClass("mCustomScrollbar")) ? "update" : false;

                $("tbody", domId)
                .html($(tmplId).tmpl(data))
                    .end().mCustomScrollbar(update);

                if (data.length === 0) {
                    if ($(".js-no-alerts", domId).length > 0) { return; }
                    $(domId).prepend("<p class='js-no-alerts'>All dealerships are below the alert threshold.</p>");
                } else {
                    $(".js-no-alerts", domId).remove();
                }
            }

            // map the alert objects to a key so they're easier to parse in the templates
            _.each(alertData, function (dealer, key, list) {

                var mapped = {};

                // used/new? same for all dealers, but easier to consume it at this level in the template
                list[key].InventoryType = invtype;

                if (dealer.Data.DealerSiteData) {
                    list[key].Data.DealerSiteData.Invalid = _.some(dealer.Data.DealerSiteData, function (site) {
                        return site.CredentialStatus === -1;
                    });
                }

                _.each(dealer.Data.BucketsData.ActivityBuckets, function (alert) {
                    mapped[alert.Id] = alert;
                });
                list[key].Data.BucketsData.ActivityBuckets = mapped;

                _.each(dealer.Data.BucketsData.ActionBuckets, function (alert) {
                    mapped[alert.Id] = alert;
                });
                list[key].Data.BucketsData.ActionBuckets = mapped;
            });


            // render templates with the alert counts sorted desc.
            createAlertList("#inventory-alerts", "#inventoryAlerts", "ActionBucketsTotal", threshold);
            createAlertList("#quality-alerts", "#qualityAlerts", "ActivityBucketsTotal", threshold);

            $('.report-link').each(function (idx, elem) {
                elem = $(elem);
                if (elem.data('shouldShow') !== false) {
                    elem.show();
                }
                else {
                    elem.hide();
                }
            });

            $(".threshold-edit-link").on("click", function (e) {
                $(this).hide().next(".threshold-setting").show();
                return false;
            });

            $(".threshold-setting").on("click", function (e) { e.stopPropagation(); });

            $(".threshold-setting input").on("keyup", function (event) {
                switch ($(this).attr('id')) {
                    case ('onlineThreshold'):
                        // ONLINE INVENTORY ALERTS
                        createAlertList('#inventory-alerts', '#inventoryAlerts', 'ActionBucketsTotal', $(this).val().replace('%', ''));
                        break;
                    case ('activityThreshold'):
                        // AD QUALITY ALERTS
                        createAlertList('#quality-alerts', '#qualityAlerts', 'ActivityBucketsTotal', $(this).val().replace('%', ''));
                        break;
                }
            });

            // render View Full Report templates with all of the dealer data
            $("#ad_quality_alerts_report_table tbody").html($("#quality_alerts_report").tmpl({ dealer: alertData, reportviewer: settings.userPerms.IsReportViewer }));
            $("#ad_quality_alerts_report_table").tablesorter({
                widthFixed: true,
                sortList: [[2, 1]],
                headers: { 0: { sorter: false }},
                widgets: ['numbering']
            });

            $("#online_inv_alerts_report_table tbody").html($("#inventory_alerts_report").tmpl({ dealer: alertData, reportviewer: settings.userPerms.IsReportViewer }));
            $("#online_inv_alerts_report_table").tablesorter({
                widthFixed: true,
                sortList: [[2, 1]],
                headers: { 0: { sorter: false }},
                widgets: ['numbering']
            });


        },

        InitBucketLists: function (alertsData) {

            var alertsTmpl = $("#alerts_tmpl"),
                offlineLink;

            // Pass data to jQuery templates
            $("#ActionAlerts")
                .removeClass("loading")
                .html($.tmpl(alertsTmpl, { type: "Online", alerts: alertsData.BucketsData.ActionBuckets, total: alertsData.BucketsData.ActionBucketsTotal }));
            $("#ActivityAlerts")
                .removeClass("loading")
                .html($.tmpl(alertsTmpl, { type: "Ad Quality", alerts: alertsData.BucketsData.ActivityBuckets, total: alertsData.BucketsData.ActivityBucketsTotal }));

            $("#MaxAdLink span.maxSectionWorkflowText").html($.tmpl($("#maxSectionWorkflowText_tmpl"), alertsData.BucketsData.MaxBuckets));
            offlineLink = "<li><a href='" + alertsData.BucketsData.OfflineBucket.NavigationUrl + "'>Vehicles Marked Offline (" + alertsData.BucketsData.OfflineBucket.Count + ")</a></li>";
            $("#AdditionalLinks ul:first").append(offlineLink);

        },

        InitGauges: function (alertsData) {

            var gaugeHeight = 121,
                gaugeYDelta = 0,
                notOnlineXPos = 0,
                lowActivityXPos = 0,
                notOnlineCount = alertsData.BucketsData.NotOnlineCount,
                notOnlineGauge = $("#NotOnline"),
                notOnlinePercent = alertsData.BucketsData.NotOnlinePercent,
                lowActivityCount = alertsData.BucketsData.LowActivityCount,
                lowActivityGauge = $("#LowActivity"),
                lowActivityPercent = alertsData.BucketsData.LowActivityPercent;

            /* using the step function to animate background-position across browsers
            read more here: http://stackoverflow.com/a/10449575
            original CSS adjustment: .css( "background-position", "0px " + getNotOnlinePosition().toString() + "px" );
            */
            $(".gauge", notOnlineGauge).animate({ 'border-spacing': getNotOnlinePosition() }, { step: function (now, fx) { fx.start = gaugeHeight; $(fx.elem).css("background-position", "0px " + now + "px"); }, duration: 1000 });
            $(".count", notOnlineGauge).html(notOnlineCount.toString() + " Vehicles");
            $(".percentage", notOnlineGauge).html(notOnlinePercent.toString() + "%");

            $(".gauge", lowActivityGauge).animate({ 'border-spacing': getLowActivityPosition() }, { step: function (now, fx) { fx.start = gaugeHeight; $(fx.elem).css("background-position", "0px " + now + "px"); }, duration: 1000 });
            $(".count", lowActivityGauge).html(lowActivityCount.toString() + " Vehicles");
            $(".percentage", lowActivityGauge).html(lowActivityPercent.toString() + "%");

            notOnlineGauge.removeClass("loading");
            lowActivityGauge.removeClass("loading");

            function getLowActivityPosition() {
                return (gaugeHeight - (lowActivityPercent / 100 * gaugeHeight) + gaugeYDelta);
            }

            function getNotOnlinePosition() {

                var position = gaugeYDelta;

                // TODO: Replace this hack with 'area under a curve' function
                if (notOnlinePercent > 0 && notOnlinePercent < 21) {
                    position = position + ((notOnlinePercent / 100) * 99);
                } else if (notOnlinePercent > 20 && notOnlinePercent < 41) {
                    position = position + ((notOnlinePercent / 100) * 89);
                } else if (notOnlinePercent > 40 && notOnlinePercent < 61) {
                    position = position + ((notOnlinePercent / 100) * 79);
                } else if (notOnlinePercent > 60 && notOnlinePercent < 81) {
                    position = position + ((notOnlinePercent / 100) * 99);
                } else if (notOnlinePercent > 80) {
                    position = position + ((notOnlinePercent / 100) * 121);
                } else {
                    position = 0;
                }

                return position;
            }

        },

        InitMaxSeSearch: function () {

            var parent = this;

            $("#SearchMaxSEForm").unbind("submit").bind("submit", { instance: this }, function (e) {

                var instance = e.data.instance,
                    searchText = $("#MaxSeTextbox").val(),
                    ajaxConfig = {};

                ajaxConfig.url = config.baseURL+"SearchMaxSE.aspx/" + searchText;
                parent.GetData(ajaxConfig).done(function (data) {
                    var messageText = data.StatusMessage.MessageText,
                        messageType = data.StatusMessage.MessageTypeText.toLowerCase();

                    if (messageType === "error") {
                        instance.DisplayStatusMessage(messageType, messageText);
                    } else {
                        window.open(data.MaxSEUrl);
                    }
                });

                e.preventDefault();
                e.stopPropagation();

            });

        },

        // create the date drop down
        SetupDatePicker: function (date) {

            var instance = this,
                dateSelect = [],
                i = 1,
                t = 13;

            function createDateValue (date) {
                return date.getMonth() + 1 + "/" +date.getFullYear();
            }

            if (!date) { date = Date.today(); }

            // on the 5th day, make the current month available
            if ((date - Date.today().moveToFirstDayOfMonth()) / 1000 / 3600 / 24 >= 4) {
                dateSelect.push({
                    dateValue: createDateValue(date),
                    dateName: monthNames[date.getMonth()],
                    dateYear: date.getFullYear(),
                    defaultSelectedDate: createDateValue(Date.today().addMonths(-1))
                });
                t--;
            }

            // current month, 12 past months
            for (i; i <= t; i++) {
                var pastDate = date.clone();
                pastDate.addMonths(-i);
                dateSelect.push({
                    dateValue: createDateValue(pastDate),
                    dateName: monthNames[pastDate.getMonth()],
                    dateYear: pastDate.getFullYear(),
                    defaultSelectedDate: createDateValue(Date.today().addMonths(-1))
                });
            }

            return dateSelect;
        },

        InitPerfGraph: function (perfData) {

            var instance = this,
                charts = ["searches", "vdp", "actions", "pages", "leads", "cpl", "cpv"],
                chartdata,
                leads,
                dealercount,
                datacount,
                parents = $("#tab_perf, #chart_stats, #chart_lead");

            if (_.isUndefined(perfData)) { return; }

            // if no data, hide charts & show error
            if (_.isEmpty(perfData.Sites)) {
                instance.ShowChartError(parents, "No data available.");
                return;
            }

            //render charts
            instance.ShowCharts(charts, perfData, parents);

            $('.report-link').each(function (idx, elem) {
                elem = $(elem);
                if (elem.data('shouldShow') !== false) {
                    elem.show();
                }
                else {
                    elem.hide();
                }
            });

            leads = {
                ChatValue: null,
                EmailValue: null,
                PhoneValue: null,
                AdPrintValue: null,
                MapValue: null
            };

            _.each(leads, function (value, key) {
                var nullObject = {},
                    sitearray;

                nullObject[key] = value;
                sitearray = _.where(perfData.Sites, nullObject);

                // compare number of sites with null value to total sites
                if (sitearray.length === perfData.Sites.length) {
                    var input = $("input[name='" + key + "']");
                    // if any lead value is null for all sites, disable the checkbox
                    input.attr("disabled", "disabled");
                    $("label[for='" + input.attr("id") + "']").addClass("disabled");
                }
            });

            $(".vendor-conversion-stats").html($("#vendorConversionStats").tmpl(perfData));
            $(".vendor-budgets").html($("#vendorBudgets").tmpl({ site: _.reject(perfData.Sites, function (site) { return _.isUndefined(config.maxVendors[site.SiteName]); }) }));
            
            // show message if dealers with perfdata is less than total
            dealercount = config.perfData.length,
            datacount = _.max(perfData.Sites, function (site) {
                return site.Count;
            }).Count;

            
            var lead_choices_locked = false;
            $("#lead_choices input").off("change.perf");
            $("#lead_choices input").on("change.perf", function () {
                // delay the 'Online Classified Performance' so that it doesn't run at the same time as 'Trends'
                // show loader during this time
                $('#tab_perf #tab_lead .loading').show();
                setTimeout( function() {
                    var fields = $("#lead_choices :input").serializeArray();
                    // performance stuff
                    var charts = ["leads", "cpl"];
                    perfData = instance.UpdateLeads(perfData, fields);
                    instance.CalculateAveragesBySite( perfData, dealercount );
                    instance.ShowCharts(charts, perfData, parents);
                    instance.DisplayBenchmark(fields, $("#chart_cpa_chart .highcharts-benchmark"));
                    if (dealercount > 1) {
                        _.each( config.perfData, function( dealer, dkey, original ) {
                            original[dkey].Data = instance.UpdateLeads( dealer.Data, fields );
                        });
                        instance.InitFullReport( config.perfData );
                    }
                    $('#tab_perf #tab_lead .loading').hide()
                }, 2000 );
                // update secondary set of checkboxes
                lead_choices_locked = true;
                $('#trends_lead_choices         input[name='+this.name+']')[0].checked = this.checked;
                $('#costper_lead_trends_choices input[name='+this.name+']')[0].checked = this.checked;
                lead_choices_locked = false;
            });
            // secondary sets are faux wrappers to primary set
            $('#trends_lead_choices input').off('change.faux');
            $('#trends_lead_choices input').on('change.faux', function () {
                if( lead_choices_locked )
                    return;
                $('#lead_choices                input[name='+this.name+']')[0].checked = this.checked; // primary
                $('#costper_lead_trends_choices input[name='+this.name+']')[0].checked = this.checked; // other secondary
                $('#lead_choices input[name='+this.name+']').trigger('change');
            });
            // secondary sets are faux wrappers to primary set
            $('#costper_lead_trends_choices input').off('change.faux');
            $('#costper_lead_trends_choices input').on('change.faux', function () {
                if( lead_choices_locked )
                    return;
                $('#lead_choices        input[name='+this.name+']')[0].checked = this.checked; // primary
                $('#trends_lead_choices input[name='+this.name+']')[0].checked = this.checked; // other secondary
                $('#lead_choices input[name='+this.name+']').trigger('change');
            });
            instance.LeadTypesHandlersAttached = true;
        },

        InitTrendGraph: function( trendsData ) {
            var instance = this;
            $("#lead_choices input").off("change.trends");
            $("#lead_choices input").on("change.trends", function () {
                var fields = $("#lead_choices :input").serializeArray();
                // performance stuff
                var charts = ["lead_trends"];
                var parents = $('#tab_trends');
                trendsData = instance.UpdateTrends( trendsData, fields );
                instance.ShowCharts(charts, trendsData, parents);
                // // delay the report calculations so it does not run at the same time as Trends
                // setTimeout( function() {
                //     instance.LeadsTrendReport( trendsData, "lead_trend" );
                // }, 1000 );
            });
        },

        InitCostPerTrendGraph: function( trendsData ) {
            var instance = this;
            $("#lead_choices input").on("change", function () {
                var fields = $("#lead_choices :input").serializeArray();
                // performance stuff
                var charts = ["costper_lead_trends"];
                var parents = $('#tab_trends');
                trendsData = instance.UpdateTrends( trendsData, fields );
                instance.ShowCharts(charts, trendsData, parents);
            });
        },

        // hide/show benchmark label on chart only when the required fields are checked
        DisplayBenchmark: function (fields, benchmarks) {
            var isBenchmarkLeadsChecked;
            
            fields = _.map(fields, function (field) {
                if (field.value === "on") {
                    return field.name;
                }
            });

            isBenchmarkLeadsChecked = (fields.length === 2 && _.contains(fields, "PhoneValue") && _.contains(fields, "EmailValue")) ? true : false;

            if (isBenchmarkLeadsChecked) {
                benchmarks.show();
            } else {
                benchmarks.hide();
            }
        },

        // calculate dynamic data from events (e.g. selected leads) --called by events
        UpdateLeads: function (perfData, formData) {

            var selectedLeads = [];
            var instance = this;
            var today = Date.today();
            var includesCurrentMonth = (perfData.EndDate.getMonth() === today.getMonth() && perfData.EndDate.getFullYear() === today.getFullYear());

            // sum leads from formData, or find checkboxes in DOM
            formData = (!_.isEmpty(formData)) ? formData : $("#lead_choices :input").serializeArray();

            // update values based on formData
            _.each(formData, function (formObject) {
                selectedLeads.push(formObject.name);
            });

            _.each(perfData.Sites, function (site) {

                var selectedLeadsValues,
                    budget,
                    detail,
                    lead;

                // calculated stats, not present in ajax data
                site.LeadValue = null;
                site.CostPerLead = null;
                site.CostPerVDP = null;

                selectedLeadsValues = _.chain(selectedLeads)
                    .map(function (value) { return (config.isGroup && !_.isEmpty(site.WithBudget)) ? site.WithBudget[value] : site[value]; })
                    .filter(function (num) { return _.isNumber(num); })
                    .value();

                if (!_.isEmpty(selectedLeadsValues)) {
                    site.LeadValue = _.reduce(selectedLeadsValues, function (memo, num) {
                        return memo + num;
                    });
                }

                if (site.Budget > 0) {
                    if(config.isGroup && !_.isUndefined(site.AverageBudget))
                    {
                        budget = (_.isUndefined(site.AverageBudget)) ? site.Budget : site.AverageBudget;
                        detail = (_.isUndefined(site.AverageDetailValue)) ? site.DetailValue : site.AverageDetailValue;
                        lead =  (_.isUndefined(site.AverageLeadValue)) ? site.LeadValue : site.AverageLeadValue;
                    }
                    else
                    {
                        budget = (includesCurrentMonth) ? site.ProratedBudget : site.Budget;
                        detail = site.DetailValue;
                        lead = site.LeadValue; 
                    }
                    
                    site.CostPerVDP = instance.CalculateCostPer( budget, detail );
                    site.CostPerLead = instance.CalculateCostPer( budget, lead );
                }

                site.DetailPerVehicle = (site.InventoryCount > 0) ? site.DetailValue / site.InventoryCount : 0;

                var lpv = (site.InventoryCount > 0) ? site.LeadValue / site.InventoryCount : 0;
                lpv = Math.round(parseFloat(lpv) * Math.pow(10, 2)) / Math.pow(10, 2);

                site.LeadPerVehicle = lpv;

            });

            return perfData;
        },

        UpdateTrends: function( trendsData, fields ) {

            var selectedLeads = [];
            var instance = this;

            // sum leads from fields, or find checkboxes in DOM
            fields = (!_.isEmpty(fields)) ? fields : $("#lead_choices :input").serializeArray();

            // update values based on fields
            _.each(fields, function (formObject) {
                selectedLeads.push(formObject.name);
            });

            // map "performance" query fields to "trends" equivalent fields, and recalculate sum of leads categories as before
            _.each( trendsData, function (site_series) {
                _.each( site_series, function (point) {
                    // EmailValue => EmailLeadsCount
                    // PhoneValue => PhoneLeadsCount
                    // ChatValue  => ChatRequestsCount
                    var mappedLeads = [];
                    if( _(selectedLeads).contains('EmailValue')) mappedLeads.push('EmailLeadsCount');
                    if( _(selectedLeads).contains('PhoneValue')) mappedLeads.push('PhoneLeadsCount');
                    if( _(selectedLeads).contains('ChatValue'))  mappedLeads.push('ChatRequestsCount');

                    var selectedLeadsValues = _.chain(mappedLeads)
                        .map(function(fieldName) { return point[fieldName]; })
                        .filter(function(val) { return _(val).isNumber(); })
                        .value();

                    // LeadValue  => TotalLeads
                    point.TotalLeads = _.reduce(selectedLeadsValues, function (memo, num) {
                        return memo + num;
                    },0);
                })
            });

            return trendsData;
        },

        CalculateCostPer: function (cost, stat) {
            // We only display whole numbers, so make sure the division adds up.
            cost = Number(Highcharts.numberFormat(cost,0,'.',''));
            stat = Number(Highcharts.numberFormat(stat,0,'.',''));
            return cost/stat;
        },

        InitFullReport: function (perfData, sort) {

            var instance = this,
                fullreport = $("#WebsitePerformanceReport-reports"),
                sortList = sort || [[5, 1]];

            if (fullreport.hasClass("ui-tabs")) {
                fullreport.tabs("destroy");
                fullreport.empty();
            }

            if (_.isEmpty(perfData.Totals.Sites)) { return; }

            var sites = _.map(perfData.Totals.Sites, function (site) {
                return {
                    SiteName: site.SiteName,
                    CSSID: site.SiteName.replace(".", ""),
                    Dealers: _.filter(perfData, function (dealer) {
                        return _.has(dealer.Data.Sites, site.SiteName);
                    })
                };
            });
            
            fullreport
                .html($("#website_performance_sites")
                .tmpl({ site: sites }))
                .tabs()
                .find("table")
                .tablesorter({
                    widthFixed: true,
                    sortList: sortList,
                    headers: {
                        0: { sorter: false },
                        3: { sorter: 'moneySorter' },
                        6: { sorter: 'moneySorter' }
                    },
                    widgets: ['numbering']
                });
            $('#WebsitePerformanceReport-reports .tablesorter').each( function(i) {
                $(this).trigger('sorton',[this.config.sortList]);
            });
        },

        UpdateBudget: function (settings) {

            var instance = this,
                perfData = config.perfData,
                request = {},
                response,
                dealer,
                tempdata;

            dealer = _.find(perfData, function (dealer) {
                return dealer.BusinessUnitId == settings.business_unit_id;
            });

            // get current sort order so it can be persisted
            var sortList = $("#website_performance_report_table")[0].config.sortList;

            // update budget/cost per stats for this dealer
            dealer.Data.Sites[settings.sitename].Budget = settings.newval;
            dealer = instance.UpdateLeads(dealer.Data);

            // update the total budget
            perfData.Totals.Sites[settings.sitename].Budget = (perfData.Totals.Sites[settings.sitename].Budget - settings.original_editable_val) + settings.newval;

            // update budget below chart
            $(".vendor-budgets").html($("#vendorBudgets").tmpl({ site: perfData.Totals.Sites }));

            // update charts
            tempdata = instance.UpdateLeads(perfData.Totals);
            instance.ShowCharts(["cpl", "cpv"], tempdata);

            // update row in full report
            instance.InitFullReport(perfData, sortList);

            request.url = config.baseURL+"SaveDealerSiteBudget.aspx/" + settings.business_unit_id + "/" + settings.sitename + "/" + settings.newval + "/" + settings.date.getMonth() + "/" + settings.date.getFullYear();
            response = instance.GetData(request);
            response.done(function (data) {
                // stop loading animation
            });

        },

        DefaultChart: function () {

            this.options = {
                chart: {
                    backgroundColor: null, // transparent
                    type: "column"
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            style: {
                                color: "#646c79",
                                fontFamily: "Arial, sans-serif"
                            }
                        },
                        shadow: true
                    },
                    column: {
                        pointPadding: 0,
                        groupPadding: 0.2,
                        borderWidth: 0
                    },
                    bar: {
                        pointPadding: 0,
                        groupPadding: 0.2,
                        borderWidth: 0
                    }
                },
                xAxis: {
                    labels: {
                        style: {
                            fontSize: '9px',
                            fontFamily: "Arial, sans-serif"
                        }
                    },
                    tickLength: 0
                },
                yAxis: {
                    labels: {
                        enabled: false
                    },
                    stackLabels: {
                        style: {
                            fontFamily: "Arial, sans-serif"
                        },
                        crop: false
                    },
                    gridLineWidth: 0,
                    title: {
                        text: null
                    }
                },
                title: {
                    style: {
                        color: "#333333",
                        fontSize: "12px",
                        fontWeight: "normal",
                        fontFamily: "Arial"
                    },
                    margin: 30
                },
                tooltip: {
                    enabled: false
                },
                subtitle: {
                    style: {
                        color: "#333333",
                        fontSize: "10px",
                        fontWeight: "bold",
                        fontFamily: "Arial"
                    }
                },
                legend: {
                    enabled: false
                },
                credits: {
                    enabled: false
                }
            };
        },

        BuildCharts: function (charts, data, settings) {
            
            settings = $.extend({}, config, settings);

            var instance = this,
                largeDataLabels = { fontSize: 17, fontWeight: "bold" },
                smallDataLabels = { fontSize: 12, fontWeight: "bold" },
                wholeNumber,
                missingBudget,
                costPerBenchmark;

            wholeNumber = function () {
                return Highcharts.numberFormat(this.y, 0);
            };

            missingBudget = function () {
                if (_.isNull(this.y) && config.maxVendors[this.x] && config.maxVendors[this.x].DestDDL) {
                    var message = "Please set your <br />";
                    if (!config.isGroup) {
                        message += "<a href='"+settings.baseURL+"CustomizeMAX/DealerPreferences.aspx?t=Reports&d=" + (data.StartDate.getMonth() + 1) + "/" + data.StartDate.getFullYear() + "&DestDDL=" + config.maxVendors[this.x].DestDDL + "'>monthly cost</a>";
                    } else {
                        message += "monthly cost";
                    }
                    message += "<br />to see this data.";
                    return message;
                }
            };

            costPerBenchmark = function (chart) {
                            
                var yAxis = chart.yAxis[0],
                    barEx = yAxis.getExtremes(),
                    benchMetric = chart.options.benchmark,
                    benchmarkMax = _.max(data.Sites, function (site) {
                        return site[benchMetric];
                    })[benchMetric],
                    xPadding = 10,
                    yPadding = 5,
                    xSplit = xPadding/2,
                    ySplit = yPadding/2,
                    margin = 5;

                if(benchmarkMax<=0) { return; }

                if(benchmarkMax > barEx.max) {
                    chart.yAxis[0].setExtremes(barEx.min, benchmarkMax);
                }

                function createLabel (box, group) {
                    var path;

                    if (box.align === "right") {
                        box.x = box.x - (box.width/2);
                        path = ["M", box.x-xSplit, box.y-ySplit,                    // top left corner
                                "L", box.x+box.width-margin, box.y-ySplit,          // bottom left of triangle
                                box.x+box.width, box.y-ySplit-margin,               // top of triangle
                                box.x+box.width+margin, box.y-ySplit,               // bottom right of triangle
                                box.x+box.width+margin, box.y+box.height+ySplit,    // bottom right corner
                                box.x-xSplit, box.y+box.height+ySplit,              // bottom left corner
                                box.x-xSplit, box.y-ySplit];                        // top left corner
                    } else if (box.align === "left") {
                        box.x = box.x + (box.width/2);
                        path = ["M", box.x-xSplit, box.y-ySplit,                    // top left corner
                                "L", box.x-xSplit+margin, box.y-ySplit-margin,      // top of triangle
                                box.x-xSplit+margin+margin, box.y-ySplit,           // bottom right of triangle
                                box.x+box.width+xSplit, box.y-ySplit,               // top right corner
                                box.x+box.width+xSplit, box.y+box.height+ySplit,    // bottom right corner
                                box.x-xSplit, box.y+box.height+ySplit,              // bottom left corner
                                box.x-xSplit, box.y-ySplit];                        // top left corner
                    } else {
                        path = ["M", box.x-xSplit, box.y-ySplit,                    // top left corner
                                "L", box.x+(box.width/2)-margin, box.y-ySplit,      // bottom left of triangle
                                box.x+(box.width/2), box.y-ySplit-margin,           // top of triangle
                                box.x+(box.width/2)+margin, box.y-ySplit,           // bottom right of triangle
                                box.x+box.width+xSplit, box.y-ySplit,               // top right corner
                                box.x+box.width+xSplit, box.y+box.height+ySplit,    // bottom right corner
                                box.x-xSplit, box.y+box.height+ySplit,              // bottom left corner
                                box.x-xSplit, box.y-ySplit];                        // top left corner
                    }
                    return chart.renderer.path(path)
                        .attr({
                            'stroke-width': 1,
                            stroke: "#cccccc",
                            fill: "#ffffff",
                            zIndex: 7
                        })
                        .add(group);
                }
            
                _.each(chart.series[0].data, function (point, pointIndex) {
                    var plotLine = {},
                        benchmark = data.Sites[point.category][benchMetric],
                        yStart = chart.xAxis[0].toPixels(point.x)-(point.pointWidth/2),
                        xStart = yAxis.toPixels(benchmark),
                        yEnd = yStart+point.pointWidth,
                        label,
                        text,
                        tBox,
                        group;

                    group = chart.renderer.g("benchmark").attr({zIndex: 8}).add();

                    plotLine.path = ["M", xStart, yStart, "L", xStart, yEnd];        
                    plotLine.attr = {
                        'stroke-width': 1,
                        stroke: "#cccccc",
                        zIndex: 6
                    };        
                    chart.renderer.path(plotLine.path).attr(plotLine.attr).add(group);
                    
                    text = chart.renderer.text("TOP PERF AVG $" + Highcharts.numberFormat(benchmark, 2), xStart, yEnd+margin+11).attr({
                        color: "#646c79",
                        align: "center", 
                        zIndex: 8
                    }).css({
                        textTransform: "uppercase",
                        fontFamily: "Arial, sans-serif",
                        fontSize: "9px",
                        fontWeight: "bold"
                    }).add(group);

                    label = createLabel(text.getBBox(), group);
                    tBox = text.getBBox();
                    // label extends past right edge of chart
                    if (label.getBBox().x+label.getBBox().width > chart.plotLeft+chart.plotWidth) {
                        label.destroy();
                        text.attr({align: "right" });            
                        tBox.align = "right";
                        label = createLabel(tBox, group);
                    // label extends past the left edge of chart
                    } else if (label.getBBox().x < chart.plotLeft) {
                        label.destroy();
                        text.attr({align: "left" });
                        tBox.align = "left";
                        label = createLabel(tBox, group);
                    }
                });
            };

            // [pseudocode summary]
            // for every dealer listing site,
            //   for every month with data in the given range,
            //     if there is at least one non-zero data point from the given field list,
            //     and that data point also has a non-zero budget value,
            //     and the following month meets the same criteria,
            //     and the budget values for the two months differs
            //       THEN add a "budget change" object to the result.
            function aggregate_budget_changes( data, field_list ) {
                var result = {};
                var sum_of_fields_in_list = function( d, f ) {
                    return _(d).reduce( function( m, v, k ) {
                        if( _(f).contains( k ))
                            m += v;
                        return m;
                    }, 0 );
                }
                _.each(data, function(site_data, site_name, data) {
                    for( var i=1; i<site_data.length; ++i ) {
                        var f0 = sum_of_fields_in_list( site_data[i-1], field_list );
                        var f1 = sum_of_fields_in_list( site_data[i], field_list );
                        if( f0 == 0 || f1 == 0 )
                            continue; // not data for both months
                        var b0 = site_data[i-1].Budget,
                            b1 = site_data[i].Budget;
                        if( b0 == 0 || b1 == 0 )
                            continue; // budget not set for both months
                        if( b0 != b1 ) {
                            // budget changed and all criteria passed for month-pair being compared
                            var x = parseJSONPDate( site_data[i].DateRange.StartDate ).getTime();
                            var value = {
                                time_value: x,
                                site_name: site_name,
                                prev_value: b0,
                                new_value: b1,
                                delta: b1 - b0
                            };
                            if( !result[x] )
                                result[x] = [];
                            result[x].push( value );
                        }
                    }
                });
                return result;
            };

            // this: chart
            // delayed call
            function budget_change_init() {
                // budget-delta "$" image: url, position and mouse event handlers
                var img_src = 'Themes/MaxMvc/Images/Dashboard/$.png';
                var pseudotip_anchor_container_selector = '#trends_pseudotips'
                var r = this.renderer;
                var p = this.xAxis[0].plotLinesAndBands;
                for( var i=0; i < p.length; ++i ) {
                    var line = p[i];
                    var label = p[i].label;
                    if (!p[i].label)
                        return;
                    var hc_img = r.image( img_src, p[i].label.x-13,p[i].label.y-17, 17,17 ).attr({ zIndex: 10 }).add();
                    $(hc_img.element)
                        .data('positional_reference', $(p[i].label))
                        .data('tooltip_anchor', $(
                            '<div class="hc_pseudotip_anchor" style="display:none;top:'+(p[i].label.y-17-8)+'px;left:'+(p[i].label.x-13+21)+'px">'+
                                '<div class="hc_pseudotip_content">'+p[i].options.label.hoverHTML+'</div>'+
                            '</div>')
                        .appendTo($(pseudotip_anchor_container_selector)) );
                    $(hc_img.element)
                        .on('mouseover', function() {
                            $(this).data('tooltip_anchor').show();
                        })
                        .on('mouseout', function() {
                            $(this).data('tooltip_anchor').fadeOut();
                        });
                    if( !this.budget_change_icons )
                        this.budget_change_icons = [];
                    this.budget_change_icons.push( hc_img );
                }
            }

            // this: chart
            // called when chart data changes after initial load
            function budget_change_realign() {
                if( !this.budget_change_icons )
                    return;
                var chart = this;
                setTimeout( function() {
                    _.each( chart.budget_change_icons, function( hc_img ) {
                        var ref = $(hc_img.element).data('positional_reference');
                        var new_position = {
                            x: ref.attr('x')-13,
                            y: ref.attr('y')-17
                        }
                        $(hc_img.element).attr( new_position );
                        var hover = $(hc_img.element).data('tooltip_anchor');
                        $(hover).css({
                            left: ref.attr('x')-13+21,
                            top:  ref.attr('y')-17-8
                        })
                        // hide/show based on visibility of reference element (vertical dotted line)
                        hc_img.element.style.visibility = ref.attr('visibility');
                    });
                }, 450 );
            }

            function budget_change_plotlines( all_changes_at_time, time_value ) {
                var budget_change_popup_html_func = function( text, change_obj ) {
                    return text
                    + (text == ''? ('<span style="font-size:0.8em">'+(new Date(change_obj.time_value)).toString('MMMM yyyy')+'</span>'):'')
                    + '<br>'
                    + '<span style="color:'+instance.GetColorBySiteName(change_obj.site_name).hex+'">'+change_obj.site_name+':</span>&nbsp;'
                    + '<span class="price_arrow '+(change_obj.delta >= 0? 'positive':'negative')+'"></span>&nbsp;'
                    + '$<strong>'+Math.abs(change_obj.delta)+'</strong>';
                };
                return {
                    value: parseInt(time_value),
                    color: '#BFBFC1',
                    width: 2,
                    dashStyle: 'Dash',
                    label: {
                        verticalAlign: "top",
                        rotation: 0,
                        text: '',
                        hoverHTML: _.reduce( all_changes_at_time, budget_change_popup_html_func, '')
                    }
                };
            }                

            function inherit_trends_chart_options( options ) {
                options.title = null;
                options.chart.type = "line";
                options.chart.spacingBottom = 15;
                options.chart.spacingRight = 20;
                options.plotOptions = {};
                options.xAxis = {
                    type: 'datetime',
                    dateTimeLabelFormats: {
                        day: '%b %Y',
                        week: '%b %Y',
                        month: '%b %Y'
                    },
                    minTickInterval: 27*(24*60*60*1000), // prevents months from showing up multiple times
                    tickLength: 4,
                    tickColor: '#000000',
                    labels: {
                        style: { color: '#000000', fontSize: '0.75em' },
                        y: 15
                    },
                    minorGridLineWidth: 0,
                    minorTickWidth: 1,
                    minorTickInterval: 'auto',
                    minorTickColor: '#A0A0A0',
                    minorTickLength: 2
                };
                options.yAxis = {
                    min: 0,
                    title: { text: null },
                    labels: { enabled: true },
                    minorTickInterval: 'auto',
                    minorTickColor: '#A0A0A0',
                    minorTickPosition: 'inside'
                };
                options.tooltip.enabled = true;
                options.tooltip.formatter = function() {
                    return (new Date(this.x)).toString('MMMM yyyy')+'<br>'+
                        '<span style="color:'+this.series.color+'">'+this.series.name+'</span>: <b>'+this.y.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")+'</b><br/>';
                };
                options.legend = {
                    enabled: true,
                    verticalAlign: "bottom",
                    symbolWidth: 7,
                    borderWidth: 0,
                    y: 5
                };
                options.chart.events = {
                    // re-align custom UI elements after any redraw
                    redraw: budget_change_realign
                };
                options.post_processing = budget_change_init;
            };

            if (!_.isArray(charts)) { charts = [charts]; }

            // custom Highchart options
            charts = _.map(charts, function (chart) {
                
                var options = new instance.DefaultChart().options, // pull in default options
                    metric,
                    direction = "desc"; // default sort

                switch (chart) {
                    case "vdp":
                        metric = config.isGroup?"AverageDetailValue":"DetailValue";
                        options.title.text = "Avg Consumer (VDP) Impressions";
                        options.chart.renderTo = "chart_cv_chart";
                        $.extend(true, options.plotOptions.series.dataLabels, { formatter: wholeNumber, style: largeDataLabels });
                        break;
                    case "leads":
                        metric = config.isGroup?"AverageLeadValue":"LeadValue";
                        options.title.text = "Avg Number of Direct Leads";
                        options.chart.renderTo = "chart_act_chart";
                        $.extend(true, options.plotOptions.series.dataLabels, { formatter: wholeNumber, style: largeDataLabels });
                        break;
                    case "cpl":
                        metric = "CostPerLead";
                        direction = "asc";
                        options.title.text = "Avg Cost Per Direct Lead";
                        $.extend(true, options.plotOptions.series.dataLabels, {
                            useHTML: true,
                            align: "left",
                            formatter: missingBudget,
                            x: -126,
                            crop: false
                        });
                        options.plotOptions.series.stacking = "normal";
                        options.xAxis.labels.y = 16;
                        if (!settings.isGroup) { options.maxCallback = costPerBenchmark; }
                        options.benchmark = "BenchMarkLeadCost";
                        $.extend(true, options.chart, { 
                            renderTo: "chart_cpa_chart", 
                            type: "bar", 
                            spacingRight: 0,
                            marginLeft: 150,
                            marginRight: 10
                        });
                        $.extend(true, options.yAxis, {
                            stackLabels: {
                                enabled: true,
                                formatter: function () {
                                    // show budget if we have it
                                    var budget = this.total || 0;
                                    if (budget > 0) { budget = "$" + Highcharts.numberFormat(budget, 2); }
                                    return budget;
                                },
                                style: largeDataLabels,
                                x: -10,
                                textAlign: "right",
                                align: "left"
                            }
                        });
                        break;
                    case "cpv":
                        metric = "CostPerVDP";
                        direction = "asc";
                        options.title.text = "Avg Cost Per Consumer (VDP) Impression";
                        $.extend(true, options.plotOptions.series.dataLabels, {
                            useHTML: true,
                            align: "left",
                            formatter: missingBudget,
                            x: -126,
                            crop: false
                        });
                        options.plotOptions.series.stacking = "normal";
                        options.xAxis.labels.y = 16;
                        if (!settings.isGroup) { options.maxCallback = costPerBenchmark; }
                        options.benchmark = "BenchMarkImpressionCost";
                        $.extend(true, options.chart, {
                            type: "bar",
                            renderTo: "chart_cpv_chart",
                            spacingRight: 0,
                            marginRight: 10
                        });
                        $.extend(true, options.yAxis, {
                            stackLabels: {
                                enabled: true,
                                formatter: function () {
                                    // show budget if we have it
                                    var budget = this.total || 0;
                                    if (budget > 0) { budget = "$" + Highcharts.numberFormat(budget, 2); }
                                    return budget;
                                },
                                style: largeDataLabels,
                                x: -10,
                                textAlign: "right",
                                align: "left"
                            }
                        });
                        break;
                    case "searches":
                        metric = config.isGroup?"AverageSearchValue":"SearchValue";
                        options.title.text = "AVG SEARCHES";
                        options.title.style.fontWeight = "bold";
                        options.chart.renderTo = "chart_stats_searches";
                        $.extend(true, options.plotOptions.series.dataLabels, {
                            formatter: wholeNumber,
                            y: -6,
                            style: smallDataLabels
                        });
                        break;
                    case "pages":
                        metric = config.isGroup?"AverageDetailValue":"DetailValue";
                        options.title.text = "AVG CONSUMER<br />(VDP) IMPRESSION";
                        options.title.style.fontWeight = "bold";
                        options.chart.renderTo = "chart_stats_pages";
                        $.extend(true, options.plotOptions.series.dataLabels, {
                            formatter: wholeNumber,
                            y: -6,
                            style: smallDataLabels
                        });
                        break;
                    case "actions":
                        metric = config.isGroup?"AverageActionValue":"ActionValue";
                        options.title.text = "AVG TOTAL LEADS";
                        options.title.style.fontWeight = "bold";
                        options.chart.renderTo = "chart_stats_actions";
                        $.extend(true, options.plotOptions.series.dataLabels, {
                            formatter: wholeNumber,
                            y: -6,
                            style: smallDataLabels
                        });
                        break;
                    case "sources":
                        options.series = [{
                            data: _.map(data, function (source) {
                                return source.Percentage;
                            }).sort(function(a,b){return b-a;})
                        }];
                        options.xAxis.categories = _.pluck(data, "Key");
                        options.title.text = null;
                        options.chart.renderTo = "chart_ga_sources";
                        options.chart.spacingTop = 20;
                        options.chart.spacingLeft = 20;
                        $.extend(true, options.plotOptions.series.dataLabels, {
                            formatter: function () {
                                var decimal = (Highcharts.numberFormat(this.y, 1) < 1) ? 1 : 0;
                                return Highcharts.numberFormat(this.y, decimal) + "%";
                            },
                            y: -6,
                            style: largeDataLabels
                        });
                        $.extend(true, options.tooltip, {
                            enabled: true,
                            formatter: function () {
                                var decimal = (Highcharts.numberFormat(this.y, 1) < 1) ? 1 : 0;
                                var msg = Highcharts.numberFormat(this.y, decimal)+"% of traffic to your dealership<br /> website came from "+this.x+".";
                                if (!config.isGroup) { msg += "<br /><a href='#' id='ga_keywords_detail'>Click here</a> to see keywords used."; }
                                return msg;
                            },
                            useHTML: true
                        });
                        options.colors = [{
                            linearGradient: { x1: 0, y1: 0, x2: 1, y2: 0 },
                            stops: [[0, '#017fb4'], [0.25, '#00a8ee'], [0.75, '#00a8ee'], [1, '#017fb4']]
                        }];
                        $.extend(true, options.xAxis.labels, {
                            rotation: -45,
                            align: "right",
                            style: {
                                textTransform: "uppercase"
                            },
                            formatter: function () {
                                return (this.value.length > 15) ? this.value.substring(0, 15) + "..." : this.value;
                            }
                        });
                        $.extend(true, options.yAxis, {
                            labels: {
                                enabled: true,
                                formatter: function () {
                                    return this.value + "%";
                                }
                            },
                            gridLineWidth: 1,
                            gridLineColor: "#cccccc",
                            title: {
                                text: null
                            },
                            allowDecimals: true,
                            tickPixelInterval: 25
                        });
                        break;
                    case "mobile":
                        options.series = [{ data: data }];
                        options.title.text = null;
                        options.chart.renderTo = "chart_ga_mobile";
                        options.chart.type = "pie";
                        options.colors = [
                            {
                                radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
                                stops: [[0, "#00c217"],[1, "#018f11"]]
                            },
                            {
                                radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
                                stops: [[0, '#00a8ee'], [1, '#017fb4']]
                            }
                        ];
                        $.extend(true, options.plotOptions.series.dataLabels, {
                            formatter: function () {
                                var decimal = (this.y < 1) ? 1 : 0;
                                return "<div style='text-align: center;'><span style='text-transform: uppercase;'>" + this.point.name + "</span><br /><span style='font-size: 16px;'>" + Highcharts.numberFormat(this.y, decimal) + "%</span></div>";
                            },
                            useHTML: true,
                            style: {
                                textTransform: "uppercase"
                            }
                        });
                        options.plotOptions.pie = {
                            tooltip: {
                                followPointer: false
                            }
                        };
                        $.extend(true, options.tooltip, {
                            enabled: true,
                            followPointer: false,
                            formatter: function () {
                                var decimal = (this.y<1) ? 1 : 0,
                                    message = Highcharts.numberFormat(this.y, decimal)+"% of traffic to your dealership<br /> website came from a "+this.point.name+" device.";
                                return (this.point.name === "Mobile") ? message += "<br /><a href='#' id='ga_mobile_detail'>Click here</a> to see rankings." : message;
                            },
                            positioner: function (labelWidth, labelHeight, point) {
                                return { x: point.plotX-50, y: point.plotY };
                            },
                            useHTML: true
                        });
                        break;
                    case "ttm":
                        var series = [];
                        series.push({
                            name: "Vehicle Online With Photo",
                            data: _.map(data.Days, function (day) { 
                                return { x: day.DayDate.getTime(), y: day.AvgElapsedDaysPhoto }; 
                            })
                        });
                        series.push({
                            name: "Complete Ad Online",
                            data: _.map(data.Days, function (day) {
                                return { x: day.DayDate.getTime(), y: day.AvgElapsedDaysAdComplete };
                            })
                        });
                        options.series = series;
                        $.extend(true, options.title, {
                            text: "Time to Market",
                            style: {
                                fontWeight: "bold",                                
                                textTransform: "uppercase"
                            },
                            margin: 60
                        });
                        options.chart.renderTo = "chart_ttm";
                        options.chart.type = "line";
                        options.legend = {
                            enabled: true,
                            verticalAlign: "top",
                            y: 30,
                            borderWidth: 0,
                            labelFormatter: function () {
                                var labelString = this.name + ': <span style="color: '+this.color+'">';
                                switch(this.name) {
                                    case 'Vehicle Online With Photo':
                                        labelString += Highcharts.numberFormat(data.AverageDaysToPhotoOnline, 1);
                                        break;
                                    case 'Complete Ad Online':
                                        labelString += Highcharts.numberFormat(data.AverageDaysToCompleteAd, 1);
                                        break;
                                }
                                labelString += '</span> Days Avg';
                                
                                return labelString;
                            }
                        };
                        options.plotOptions.series = {
                            marker: {
                                enabled: false
                            },
                            events: {
                                legendItemClick: function () {
                                    return false;
                                }
                            }
                        };
                        options.xAxis = {
                            type: 'datetime',
                            dateTimeLabelFormats: {
                                day: '%m/%e/%y',
                                week: '%m/%e/%y'
                            },
                            tickPixelInterval: 110,
                            minorTickInterval: 24 * 3600 * 1000,
                            minTickInterval: 24 * 3600 * 1000,
                            minorTickWidth: 1,
                            minorGridLineWidth: 0
                        };
                        options.yAxis = {
                            title: {
                                text: null
                            },
                            labels: {
                                enabled: true
                            }
                        };
                        break;
                    case "ttm_g_owp":
                        var cols = 8; // 10;
                        var data_viewing = _.first(_.sortBy(data.BusinessUnits, function (bu) { return -1 * bu.AverageDaysToPhotoOnline; }), cols);
                        options.chart.renderTo = "chart_ttm_g_owp";
                        options.chart.marginRight = 58;
                        options.title = {
                            text: 'Average Days to Online with Photos: '+
                                '<span class="chart_title_inline_value">'+data.AverageDaysToPhotoOnline.toFixed(1)+'</span>',
                            useHTML: true,
                            align: 'left',
                            style: {
                                fontSize: '11px',
                                color: '#666666'
                            }
                        };
                        options.xAxis = {
                            categories: _.map( data_viewing, function( bu ) { return bu.BusinessUnit; }),
                            labels: {
                                rotation: -50,
                                align: 'right',
                                style: {
                                    fontSize: '9px',
                                    textTransform: 'uppercase',
                                    width: '80px',
                                    lineHeight: '.95em'
                                },
                                x: 16,
                                y: 8
                            }
                        };
                        var group_avg_value = parseFloat(data.AverageDaysToPhotoOnline.toFixed(1));
                        options.yAxis = {
                            labels: {
                                enabled: true,
                                style: {
                                    fontSize: '12px',
                                    fontWeight: 'bold',
                                    letterSpacing: '-1px'
                                }
                            },
                            title: {
                                text: null
                            },
                            lineWidth: 1,
                            tickPixelInterval: 25,
                            min: 0,
                            minRange: group_avg_value,
                            plotLines: [{
                                value: group_avg_value,
                                color: '#BFBFC1',
                                width: 2,
                                label: {
                                    text: ''
                                }
                            }]
                        };
                        options.plotOptions.series.dataLabels.style = {
                            fontSize: '16px',
                            fontWeight: 'bold',
                            letterSpacing: '-1px'
                        };
                        options.plotOptions.series.dataLabels.x = 0;
                        options.plotOptions.series.dataLabels.y = 4;
                        options.colors = [{
                            linearGradient: { x1: 0, y1: 0, x2: 1, y2: 0 },
                            stops: [[0, '#00BAEE'], [1, '#008CBD']]
                        }];
                        options.series = [{
                            type: 'column',
                            data: _.map( data_viewing, function( bu ) { return parseFloat(bu.AverageDaysToPhotoOnline.toFixed(1)); })
                        }];
                        options.post_processing = function() {
                            var label = this.yAxis[0].plotLinesAndBands[0].label;
                            if (!label)
                                return;
                            var img_src = 'Themes/MaxMvc/Images/Dashboard/group_avg.png';
                            this.renderer.image( img_src, 428,label.y-7, 50,22 ).add();
                        }
                        break;
                    case "ttm_g_cao":
                        var cols = 8; // 10;
                        var data_viewing = _.first(_.sortBy(data.BusinessUnits, function (bu) { return -1 * bu.AverageDaysToCompleteAd; }), cols);

                        options.chart.renderTo = "chart_ttm_g_cao";
                        options.chart.marginRight = 58;
                        options.title = {
                            text: 'Average Days to Complete Ad Online: ' +
                                '<span class="chart_title_inline_value_green">' + data.AverageDaysToCompleteAdOnline.toFixed(1) + '</span>',
                            useHTML: true,
                            align: 'left',
                            style: {
                                fontSize: '11px',
                                color: '#666666'
                            }
                        };
                        options.xAxis = {
                            categories: _.map( data_viewing, function( bu ) { return bu.BusinessUnit; }),
                            labels: {
                                rotation: -50,
                                align: 'right',
                                style: {
                                    fontSize: '9px',
                                    textTransform: 'uppercase',
                                    width: '80px',
                                    lineHeight: '.95em'
                                },
                                x: 32,
                                y: 8
                            }
                        };
                        var group_avg_value = parseFloat(data.AverageDaysToCompleteAdOnline.toFixed(1));
                        options.yAxis = {
                            labels: {
                                enabled: true,
                                style: {
                                    fontSize: '12px',
                                    fontWeight: 'bold',
                                    letterSpacing: '-1px'
                                }
                            },
                            title: {
                                text: null
                            },
                            lineWidth: 1,
                            tickPixelInterval: 25,
                            min: 0,
                            minRange: group_avg_value,
                            plotLines: [{
                                value: group_avg_value,
                                color: '#BFBFC1',
                                width: 2,
                                label: {
                                    text: ''
                                }
                            }]
                        };
                        options.plotOptions.series.dataLabels.style = {
                            fontSize: '16px',
                            fontWeight: 'bold',
                            letterSpacing: '-1px'
                        };
                        options.plotOptions.series.dataLabels.x = 0;
                        options.plotOptions.series.dataLabels.y = 4;
                        options.colors = [{
                            linearGradient: { x1: 0, y1: 0, x2: 1, y2: 0 },
                            stops: [[0, '#01bb40'], [1, '#118234']]
                        }];
                        options.series = [{
                            type: 'column',
                            data: _.map(data_viewing, function (bu) { return parseFloat(bu.AverageDaysToCompleteAd.toFixed(1)); })
                        }];
                        options.post_processing = function () {
                            var label = this.yAxis[0].plotLinesAndBands[0].label;
                            if (!label)
                                return;
                            var img_src = 'Themes/MaxMvc/Images/Dashboard/group_avg_green.png';
                            this.renderer.image(img_src, 428, label.y - 7, 50, 22).add();
                        }
                        break;
                    case "trends":
                        var id = "chart_vdpt_chart";
                        var budget_changes = {};
                        var nonzero_data_count = 0;
                        if( data ) {
                            options.series = _.map(_.keys(data), function (site_name) {
                                data[site_name] = _.sortBy( data[site_name], function(i){ return parseJSONPDate(i.DateRange.StartDate).getTime(); });
                                return {
                                    name: site_name,
                                    data: _.chain(data[site_name])
                                        .filter(function (vdp_datapoint) {
                                            // pre-filter tallies
                                            if( vdp_datapoint['DetailPageViewCount'] != 0 )
                                                nonzero_data_count++;
                                            return vdp_datapoint['DetailPageViewCount'] != 0;
                                        })
                                        .map(function (vdp_datapoint) {
                                            return {
                                                x: parseInt(vdp_datapoint.DateRange.StartDate.substr(6,13), 10),
                                                y: vdp_datapoint['DetailPageViewCount']
                                            };
                                        })
                                        .value(),
                                    marker: { symbol: 'circle' }
                                };
                            });
                            options.colors = _.map(options.series, function (series) {
                                return instance.GetColorBySiteName(series.name).hex;
                            });
                            budget_changes = aggregate_budget_changes( data, ['DetailPageViewCount'] );
                        }
                        //// This section commented out because it caused some minor IE8 console errors to appear (that do not affect functionality)
                        //// However I am also leaving it in because it has a lot of value, quite possibly in the near future
                        //// Best optimization seemed to be to split the data processing that occurs for 'Online Classified Performance' and 'Trends'
                        ////   into two chunks (time division) which allowed the UI to become interactive between chunks, and fend off the IE8 "unresponsive" error
                        /*
                        // check if chart already exists
                        if($('#'+id+' .highcharts-container').length != 0) {
                            var chart = _(Highcharts.charts).find( function(c){
                                if (!c) return false;
                                return c.renderTo.id == id;
                            });
                            if( chart ) {
                                // add or replace series with new point data
                                _(options.series).each(function(s) {
                                    var e = _(chart.series).find( function(c) {
                                        return c.name == s.name;
                                    });
                                    if( e ) {
                                        e.setData( s.data, false );
                                    } else {
                                        chart.options.colors = [instance.GetColorBySiteName(s.name).hex];
                                        chart.addSeries( s, false, false );
                                    }
                                });
                                // delete series that are no longer present
                                var to_remove = [];
                                _(chart.series).each(function(c) {
                                    var o = _(options.series).find( function(s) {
                                        return c.name == s.name;
                                    });
                                    if( !o )
                                        to_remove.push( c );
                                });
                                _(to_remove).each(function(c) {
                                    c.remove();
                                });
                                chart.redraw();
                                to_remove = null;
                                // potentially create additional budget-change plot lines and icons
                                var plotLines = _.map( budget_changes, budget_change_plotlines );
                                _(plotLines).each( function( l ) {
                                    var e = _(chart.xAxis[0].plotLinesAndBands).find( function( p ) {
                                        return p.options.value == l.value;
                                    });
                                    if( !e ) // no plot-line exists with this x-value
                                        chart.xAxis[0].addPlotLine( l );
                                });
                                budget_change_init.call( chart );
                                // adjust any existing icons that need to be shifted
                                budget_change_realign.call( chart );
                                // indicate to the Render function that this chart should not be reconstructed as it is already initialized
                                return null; 
                            }
                        }
                        */
                        inherit_trends_chart_options( options );
                        options.chart.renderTo = id;
                        options.chart.width = 484;
                        options.chart.height = 236;
                        options.xAxis.plotLines = _.map( budget_changes, budget_change_plotlines );
                        if( nonzero_data_count == 0 )
                            MaxDashboard.ShowChartError( 
                                $('#'+options.chart.renderTo).closest('.chart-container'),
                                'No data available.' );
                        break;
                    case "costper_vdp_trend":
                        inherit_trends_chart_options( options );
                        options.chart.renderTo = "chart_costper_vdp_trend";
                        options.chart.width = 484;
                        options.chart.height = 236;
                        var budget_changes = {};
                        var nonzero_data_count = 0;
                        var nonzero_budget_count = 0;
                        if( data ) {
                            options.series = _.map(_.keys( _.pick(data, "AutoTrader.com","Cars.com")), function (site_name) {
                                data[site_name] = _.sortBy( data[site_name], function(i){ return parseJSONPDate(i.DateRange.StartDate).getTime(); });
                                return {
                                    name: site_name,
                                    data: _.chain(data[site_name])
                                        .filter(function (vdp_datapoint) {
                                            // pre-filter tallies
                                            if( vdp_datapoint['Budget'] > 0 )
                                                nonzero_budget_count++;
                                            if( vdp_datapoint['DetailPageViewCount'] != 0 )
                                                nonzero_data_count++;
                                            return vdp_datapoint['Budget'] > 0 && vdp_datapoint['DetailPageViewCount'] != 0;
                                        })
                                        .map(function (vdp_datapoint) {
                                            return {
                                                x: parseInt(vdp_datapoint.DateRange.StartDate.substr(6,13), 10),
                                                y: parseFloat((vdp_datapoint['Budget'] / vdp_datapoint['DetailPageViewCount']).toFixed(2))
                                            };
                                        })
                                        .value(),
                                    marker: { symbol: 'circle' }
                                };
                            });
                            options.colors = _.map(options.series, function (series) {
                                return instance.GetColorBySiteName(series.name).hex;
                            });
                            budget_changes = aggregate_budget_changes( data, ['DetailPageViewCount'] );
                        }
                        options.xAxis.plotLines = _.map( budget_changes, budget_change_plotlines );
                        options.yAxis.labels.formatter = function() {
                            return '$'+parseFloat(this.value).toFixed(2);
                        };
                        options.tooltip.formatter = function() {
                            return (new Date(this.x)).toString('MMMM yyyy')+'<br>'+
                                '<span style="color:'+this.series.color+'">'+this.series.name+'</span>: $<b>'+parseFloat(this.y).toFixed(2)+'</b><br/>';
                        };
                        if( nonzero_data_count == 0 )
                            MaxDashboard.ShowChartError( 
                                $('#'+options.chart.renderTo).closest('.chart-container'),
                                'No data available.' );
                        if( nonzero_budget_count == 0 )
                            MaxDashboard.ShowChartError( 
                                $('#'+options.chart.renderTo).closest('.chart-container'),
                                '<span class="chart_action">Please set your <a href="/merchandising/CustomizeMAX/DealerPreferences.aspx?t=Reports&d='+$('#startDate').val()+'">monthly cost</a> to see this data.</span>',
                                true );
                        break;
                    case "lead_trends":
                        inherit_trends_chart_options( options );
                        options.chart.renderTo = "chart_lead_trend";
                        options.chart.width = 484;
                        options.chart.height = 197; // must match css!
                        var budget_changes = {};
                        var nonzero_data_count = 0;
                        if( data ) {
                            options.series = _.map(_.keys( _.pick(data, "AutoTrader.com","Cars.com")), function (site_name) {
                                data[site_name] = _.sortBy( data[site_name], function(i){ return parseJSONPDate(i.DateRange.StartDate).getTime(); });
                                return {
                                    name: site_name,
                                    data: _.chain(data[site_name])
                                        .filter(function (vdp_datapoint) {
                                            // pre-filter tallies
                                            if( vdp_datapoint['TotalLeads'] != 0 )
                                                nonzero_data_count++;
                                            return vdp_datapoint['TotalLeads'] != 0;
                                        })
                                        .map(function (vdp_datapoint) {
                                            return {
                                                x: parseInt(vdp_datapoint.DateRange.StartDate.substr(6,13), 10),
                                                y: vdp_datapoint['TotalLeads']
                                            };
                                        })
                                        .value(),
                                    marker: { symbol: 'circle' }
                                };
                            });
                            options.colors = _.map(options.series, function (series) {
                                return instance.GetColorBySiteName(series.name).hex;
                            });
                            budget_changes = aggregate_budget_changes( data, ['TotalLeads'] );
                        }
                        options.xAxis.plotLines = _.map( budget_changes, budget_change_plotlines );
                        if( nonzero_data_count == 0 )
                            MaxDashboard.ShowChartError( 
                                $('#'+options.chart.renderTo).closest('.chart-container'),
                                'No data available.' );
                        break;
                    case "costper_lead_trends":
                        inherit_trends_chart_options( options );
                        options.chart.renderTo = "chart_costper_lead_trends";
                        options.chart.width = 484;
                        options.chart.height = 197; // must match css!
                        var budget_changes = {};
                        var nonzero_data_count = 0;
                        var nonzero_budget_count = 0;
                        if( data ) {
                            options.series = _.map(_.keys( _.pick(data, "AutoTrader.com","Cars.com")), function (site_name) {
                                data[site_name] = _.sortBy( data[site_name], function(i){ return parseJSONPDate(i.DateRange.StartDate).getTime(); });
                                return {
                                    name: site_name,
                                    data: _.chain(data[site_name])
                                        .filter(function (vdp_datapoint) {
                                            // pre-filter tallies
                                            if( vdp_datapoint['Budget'] > 0 )
                                                nonzero_budget_count++;
                                            if( vdp_datapoint['TotalLeads'] != 0 )
                                                nonzero_data_count++;
                                            return vdp_datapoint['Budget'] > 0 && vdp_datapoint['TotalLeads'] != 0;
                                        })
                                        .map(function (vdp_datapoint) {
                                            return {
                                                x: parseInt(vdp_datapoint.DateRange.StartDate.substr(6,13), 10),
                                                y: parseFloat((vdp_datapoint['Budget'] / vdp_datapoint['TotalLeads']).toFixed(2))
                                            };
                                        })
                                        .value(),
                                    marker: { symbol: 'circle' }
                                };
                            });
                            options.colors = _.map(options.series, function (series) {
                                return instance.GetColorBySiteName(series.name).hex;
                            });
                            budget_changes = aggregate_budget_changes( data, ['TotalLeads'] );
                        }
                        options.xAxis.plotLines = _.map( budget_changes, budget_change_plotlines );
                        options.yAxis.labels.formatter = function() {
                            return '$'+parseFloat(this.value).toFixed(2);
                        }
                        options.tooltip.formatter = function() {
                            return (new Date(this.x)).toString('MMMM yyyy')+'<br>'+
                                '<span style="color:'+this.series.color+'">'+this.series.name+'</span>: $<b>'+parseFloat(this.y).toFixed(2)+'</b><br/>';
                        };
                        // if data empty, some kind of message will need to be shown
                        if( nonzero_data_count == 0 )
                            MaxDashboard.ShowChartError( 
                                $('#'+options.chart.renderTo).closest('.chart-container'),
                                '<span class="chart_action">No data available.</span>',
                                true );
                        if( nonzero_budget_count == 0 )
                            MaxDashboard.ShowChartError( 
                                $('#'+options.chart.renderTo).closest('.chart-container'),
                                '<span class="chart_action">Please set your <a href="/merchandising/CustomizeMAX/DealerPreferences.aspx?t=Reports&d='+$('#startDate').val()+'">monthly cost</a> to see this data.</span>',
                                true );
                        break;
                }
                
                // metric is a key in SitePerformance.Data.Sites object
                if (metric) {
                    // sort array of sites by metric
                    var sortedData = _.sortBy(data.Sites, function (site) { return (direction === "desc") ? -site[metric] : site[metric]; });
                    if (chart!=="vdp") {
                        sortedData = _.reject(sortedData, function (site) { return _.isUndefined(config.maxVendors[site.SiteName]); });
                    }
                    options.series = [{
                        // pull value, other data, from prepared performanceData
                        data: _.map(sortedData, function (site) {
                            var vcolor = instance.GetColorBySiteName(site.SiteName);
                            return {
                                y: site[metric],
                                color: {
                                    // reverse gradient order in IE8, since Highcharts doesn't correctly invert gradients
                                    // https://github.com/highslide-software/highcharts.com/issues/797
                                    linearGradient: (options.chart.type==="bar" && $.browser.msie && parseInt($.browser.version, 10) < 9) ? { x1: 0, y1: 1, x2: 0, y2: 0 } : vcolor.linearGradient,
                                    stops: vcolor.stops
                                }
                            };
                        })
                    }];
                    options.xAxis.categories = _.pluck(sortedData, "SiteName");

                    if ((chart === "cpa" || chart === "cpv") && _.max(data.Sites, function (data) { return data[metric]; })[metric] > 0) {
                        options.plotOptions.series.dataLabels.x = 20;
                    }

                    if (data.length < 2) {
                        options.plotOptions.series.pointWidth = 65;
                    }
                }

                instance.DataReplicationQA( chart, options );

                return options;

            });

            return charts;
        },

        // draw (or re-draw if already exists) each chart in array, including Highcharts callbacks
        RenderCharts: function (chartArray) {

            var renderedCharts = [];
            _.each(chartArray, function (chartObject) {
                if(!chartObject)
                    return; 

                var id = $("#" + chartObject.chart.renderTo);
                if(id[0]) {
                    id.highcharts(chartObject, function (chart) {
                        if (chartObject.maxCallback) { chartObject.maxCallback(chart); }

                        if (typeof chartObject.post_processing == 'function')
                            chartObject.post_processing.call(chart);
                    });
                    if( id.highcharts() ) // gated for IE8's benefit
                        $(id.highcharts().container).on("click", function () {
                            $(this).parents(".chart-data").trigger("click");
                        });
                }
            });
        },

        // string match config colors, but default to Dealer Website for unrecognized sitenames
        GetColorBySiteName: function (sitename) {
            var vendors = config.maxVendors;
            return (vendors[sitename] && vendors[sitename].colors) ? vendors[sitename].colors : vendors["Dealer Website"].colors;
        },

        AddCustomTableSorters: function () {
            $.tablesorter.addParser({
                id: 'moneySorter',
                is: function (s) {
                    return false;
                },
                format: function (s) {
                    return Number(s.replace(/[^0-9\.]+/g, ""));
                },
                type: 'numeric'
            });
        },

        GetCacheStatus: function (args) {

            var instance = this,
                request = {},
                response;

            request.url = config.baseURL+"GroupDashboard.aspx/CacheStatus/";
            request.url += config.businessUnitId;

            response = instance.GetData(request);
            return response;
        },

        DisplayCacheStatus: function (cache, refresh) {

            var instance = this,
                status = _.indexOf(["Empty", "Pending", "Processing", "Failed", "Cached"], cache.Status),
                pending = status === 1,
                processing = status === 2,
                refreshDom = $(".refresh"),
                timestamp,
                timezone;

            // don't update timestamp if this is being manually called by refresh
            if (!refresh) {
                //sample cache: {"Status":"Cached","TimeStamp":"\/Date(1362598730000)\/"}
                //if the timestamp is DateTime.MinValue, that means it could not find a cache and we're using live data        
                timestamp = (cache.TimeStamp === "/Date(-62135575200000)/") ? new Date() : parseJSONPDate(cache.TimeStamp),
                timezone = timestamp.getTimezone();

                refreshDom.find(".time").text(timestamp.toString("h:mm tt ") + timezone)
                    .end().removeClass("hidden");
            }

            //if "Pending/Processing" disable refresh button
            if (pending || processing) {
                refreshDom.addClass("long").find(".icon").addClass("loading");
                if (pending) {
                    refreshDom.find(".message").text("Getting latest data...");
                } else {
                    refreshDom.find(".message").text("Processing data...");
                    // display progress bar
                    refreshDom.find(".progress").animate({ width: (cache.PercentComplete * 100) + "%" }, 1000);
                }
            }

            if (refresh && cache.Status === "Cached") {
                refreshDom.find(".progress").animate({ width: "100%" }, 1000);
                refreshDom.find(".text").html($("<a>").text("Please refresh the page to see the newest data.").on("click", function () {
                    location.reload(true);
                })).end().find(".icon").hide();
            }

            if (refresh && (cache.Status === "Failed" || cache.Status === "Empty")) {
                refreshDom.addClass("long")
                    .find(".icon").removeClass("loading")
                    .siblings(".message").text("Refresh failed, please try again later.");
            }
        },

        RefreshCache: function (inProgress) {

            var instance = this,
                response;

            if (!inProgress) {
                response = instance.GetData({ url: config.baseURL+"GroupDashboard.aspx/StartCacheStatus/" + config.businessUnitId, requesttype: "POST" });
                instance.DisplayCacheStatus({ Status: "Pending" }, true);
                response.done(function () {
                    checkCache();
                }).fail(function () {
                    _.delay(function () { instance.DisplayCacheStatus({ Status: "Failed" }, true); });
                });
            } else {
                checkCache();
            }

            function checkCache() {
                _.delay(function () {
                    var refresh = false;
                    //start polling CacheStatus -- use setTimeout/delay instead of setInterval to make sure the ajax finishes
                    instance.GetCacheStatus({ group: true }).done(function (data) {
                        if (data.Status === "Pending" || data.Status === "Processing") {
                            checkCache();
                        } else {
                            refresh = true;
                        }
                        instance.DisplayCacheStatus(data, refresh);
                    });
                }, 5000);
            }

        },

        // setup config settings without running OnReady (useful for testing)
        Settings: function (settings) {
            $.extend(config, settings);
        },
        
        DebugStatus: function (settings) {
            settings = $.extend({}, config, settings);
            return settings.isDebug;
        },

        DataReplicationQA: function (name, data) {
            // setup
            var container = $('#DataReplicationQA');
            if( container.length == 0 ) {
                $('body').append('<span id="DataReplicationQA" style="display:none">');
                container = $('#DataReplicationQA');
            }
            var id = 'DataReplicationQA_'+name;
            var data_field = container.find('#'+id);
            if( data_field.length == 0 ) {
                container.append('<textarea id='+id+'>');
                data_field = container.find('#'+id);
            }
            // output
            var str_data = JSON.stringify( data );
            data_field.text( str_data );
        }

    };

})();
