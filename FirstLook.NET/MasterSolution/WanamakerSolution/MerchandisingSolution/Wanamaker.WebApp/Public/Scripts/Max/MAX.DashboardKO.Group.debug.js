// assumes:
//   MaxDashboardBase

// http://www.jshint.com/  validated regularly
/* global _, ko */
/* global MaxDashboardBase, number_format */
/* global Chart, SimpleColumnChart, SquatColumnChart, BarChartWithBenchmarks, LineChartWithBudgetChanges, HangingLabelColumnChart, HangingLabelColumnChartWithValueIndicator, PieChart, SimpleLineChart, LineChartDynamicLegend  */
/* global Report, ReportCollection, ReportCell, DynamicallyFilterableReport */

///////////////////////////////////////////////////////////////////////////////
// GROUP-LEVEL DASHBOARD (multi-business-unit)

var MaxDashboardGroup = function() {
    "use strict";
    var self = this;

    // inheritance
    MaxDashboardBase.call( self );

    //// properties
    $.extend( self.controls, {
        showEditActionThreshold: ko.observable( false ),
        showEditActivityThreshold: ko.observable( false ),
        alertActionThresholdInput: ko.observable( "15" ),
        alertActivityThresholdInput: ko.observable( "15" ),
        dealershipCountDisplay: ko.observable( "&nbsp;" ) // updated manually in response to UI events
    });
    $.extend( self.virtualControls, {
        alertActionThreshold: null,
        alertActivityThreshold: null,
        activeReport: ko.observable( null ),
        manualCacheRefreshInProgress: ko.observable( false )
    });

    // init
    self.PreBindingInitialize = _(self.PreBindingInitialize).wrap( function( base_fn ) {
        base_fn();

        // wire up
        self.virtualControls.alertActionThreshold = ko.computed( function() {
            var input = self.controls.alertActionThresholdInput();
            var val = parseFloat( input ) / 100;
            return _(val).isFinite()? val : 0; 
        });
        self.virtualControls.alertActivityThreshold = ko.computed( function() {
            var input = self.controls.alertActivityThresholdInput();
            var val = parseFloat( input ) / 100;
            return _(val).isFinite()? val : 0; 
        });

        //// services
        self.RegisterService({
            id: "perms",
            url: self.static_config.baseURL+"/"+"User.aspx/permissions",
            args: [
                self.dynamic_config.businessUnitId
            ]
        });
        self.RegisterService({
            id: "data",
            url: self.static_config.baseURL+"/"+"GroupDashboardData.aspx",
            args: [
                self.dynamic_config.businessUnitId,
                self.controls.vehicleTypeFilter
            ]
        });
        self.RegisterService({
            id: "ttm",
            url: self.static_config.baseURL+"/"+"GroupTimeToMarketReport.aspx",
            args: [
                self.dynamic_config.businessUnitId,
                self.controls.vehicleTypeFilter,
                self.controls.dateRangeFrom,
                self.controls.dateRangeTo
            ]
        });
        self.RegisterService({
            id: "ga",
            url: self.static_config.baseURL+"/"+"GroupGoogleAnalyticsDataAll.aspx",
            args: [
                self.dynamic_config.businessUnitId,
                1, // "PC" (google analytics profile type)
                self.controls.dateRangeFrom,
                self.controls.dateRangeTo,
                25 // results page size limit
            ]
        });
        self.RegisterService({
            id: "perf",
            url: self.static_config.baseURL+"/"+"GroupSitePerformanceForMonth.aspx",
            args: [
                self.dynamic_config.businessUnitId,
                self.controls.vehicleTypeFilter,
                self.controls.dateRangeFrom,
                self.controls.dateRangeTo
            ]
        });
        self.RegisterService({
            id: "cache",
            url: self.static_config.baseURL+"/"+"GroupDashboard.aspx/CacheStatus",
            args: [
                self.dynamic_config.businessUnitId,
            ]
        });
        self.RegisterService({
            id: "cache_refresh_now",
            url: self.static_config.baseURL + "/" + "GroupDashboard.aspx/StartCacheStatus",
            type: "POST",
            args: [
                self.dynamic_config.businessUnitId,
            ],
            auto_start: false
        });
        self.RegisterService({
            id: "website20showroom",
            url: self.static_config.baseURL + "/" + "Dashboard/Traffic/GroupShowroomViews",
            dataArgs: {
                "businessUnitID": self.dynamic_config.businessUnitId,
                "startDate": self.controls.dateRangeFrom,
                "endDate": self.controls.dateRangeTo
            }
        });
        self.RegisterService({
            id: "website20traffic",
            url: self.static_config.baseURL + "/" + "Dashboard/Traffic/GroupWebViews",
            dataArgs: {
                "businessUnitID": self.dynamic_config.businessUnitId,
                "startDate": self.controls.dateRangeFrom,
                "endDate": self.controls.dateRangeTo
            }
        });

        // service-dependent config updates
        self.services.perms.response.subscribe( function( permissions ) {
            if( !permissions )
                return;
            self.dynamic_config.enableAnalyticsSuite(
                !_.isEmpty( 
                    _.intersection( permissions.AnalyticsSuiteBusinessUnits, permissions.UserBusinessUnits )));
            self.dynamic_config.show_Overview(
                !_.isEmpty( 
                    _.intersection( permissions.OnlineClassifiedOverviewBusinessUnits, permissions.UserBusinessUnits )));
            self.dynamic_config.show_TTM(
                !_.isEmpty( 
                    _.intersection( permissions.TimeToMarketBusinessUnits, permissions.UserBusinessUnits )));

            
            self.dynamic_config.hasShowroomReporting(!_.isEmpty(permissions.ShowroomReportsBusinessUnits));
            self.dynamic_config.hasDigitalShowroom(!_.isEmpty(permissions.ShowroomBusinessUnits));
            self.dynamic_config.hasWebsite20(!_.isEmpty(permissions.Website20BusinessUnits));
            
        });

        // basic processing
        self.data.processing = {};

        // apply user-level permissions to permit only those business units they have access to
        self.data.processing.GroupDashboardData_PermittedBusinessUnits = ko.computed( function() {
            var permissions = self.services.perms.response();
            var data = self.services.data.response();
            if( !permissions || !data )
                return null;
            return _(data).filter( function( dealership ) {
                return _(permissions.UserBusinessUnits).contains( dealership.BusinessUnitId );
            });
        });
        self.data.processing.GroupTimeToMarketReport_PermittedBusinessUnits = ko.computed( function() {
            var permissions = self.services.perms.response();
            var data = self.services.ttm.response();
            if( !permissions || !data )
                return null;
            data = $.extend( {}, data ); // shallow copy
            data.BusinessUnits = _(data.BusinessUnits).filter( function( dealership ) {
                return _(permissions.UserBusinessUnits).contains( dealership.BusinessUnitId );
            });
            return data;
        });
        // self.data.processing.GroupSitePerformanceForMonth_PermittedBusinessUnits = ko.computed( function() {
        //     var permissions = self.services.perms.response();
        //     var data = self.services.perf.response();
        //     if( !permissions || !data )
        //         return null;
        //     return _(data).filter( function( dealership ) {
        //         return _(permissions.UserBusinessUnits).contains( dealership.BusinessUnitId );
        //     });
        // });
        self.data.processing.GroupSitePerformanceForMonth_CategoricalBusinessUnits = ko.computed( function() {
            var permissions = self.services.perms.response();
            var data = self.services.perf.response();
            var result = {
                All:                                   null,
                Permitted:                             null,
                Permitted_OnlineClassifiedOverview:    null,
                Permitted_TimeToMarket:                null,
                Permitted_DigitalPerformanceAnalytics: null
            };
            if( !permissions || !data )
                return result;
            var bu_p =     permissions.UserBusinessUnits;
            var bu_p_oco = _.intersection( permissions.OnlineClassifiedOverviewBusinessUnits, permissions.UserBusinessUnits );
            var bu_p_ttm = _.intersection( permissions.TimeToMarketBusinessUnits, permissions.UserBusinessUnits );
            var bu_p_dpa = _.intersection( permissions.AnalyticsSuiteBusinessUnits, permissions.UserBusinessUnits );
            result.All =                                   data;
            result.Permitted =                             _(data).filter( function( dealership ) { return _(bu_p).contains    ( dealership.BusinessUnitId ); });
            result.Permitted_OnlineClassifiedOverview =    _(data).filter( function( dealership ) { return _(bu_p_oco).contains( dealership.BusinessUnitId ); });
            result.Permitted_TimeToMarket =                _(data).filter( function( dealership ) { return _(bu_p_ttm).contains( dealership.BusinessUnitId ); });
            result.Permitted_DigitalPerformanceAnalytics = _(data).filter( function( dealership ) { return _(bu_p_dpa).contains( dealership.BusinessUnitId ); });
            return result;
        });
        
        // calculate and group metadata on a well-formed dataset
        self.fn.processing_calculate_metadata = function( data ) {
            if( !data )
                return null;
            var return_obj = {
                DATA: {}, // these objects will contain precisely the same keys
                META: {}  // one for each dealer site name
            };
            _(data).each( function( dealership ) {
                _(dealership.Data.Sites).each( function( site_data, site_name ) {
                    // append data
                    if( !(site_name in return_obj.DATA) ) {
                        // each site will have a list of values that contribute to it
                        var list = [];
                        return_obj.DATA[site_name] = list;
                        // and each site will have a "meta" object showing the by-field totals & averages
                        var m = new Metadata();
                        return_obj.META[site_name] = m;
                    }
                    var value_list = return_obj.DATA[site_name];
                    var meta = return_obj.META[site_name];
                    // add data to totals
                    // NOTE: data provided is assumed to be the total for a range of months
                    //   so, the average is taken immediately before it is added to the totals
                    _(site_data).each( function( field_value, field_key ) {
                        if( !_.isNumber( field_value )) // non-numeric
                            return;
                        var monthly_average_value = (site_data.Months != 0) ? (field_value / site_data.Months) : 0;
                        if( field_key in meta.TOTALS ) {
                            meta.TOTALS[field_key] += field_value;
                            meta.MONTHLY_TOTALS[field_key] += monthly_average_value;
                        } else {
                            meta.TOTALS[field_key] = field_value;
                            meta.MONTHLY_TOTALS[field_key] = monthly_average_value;
                        }
                    });
                    // add metadata on source of data
                    site_data.__BusinessUnit = dealership.BusinessUnit
                    site_data.__BusinessUnitHome = dealership.BusinessUnitHome
                    site_data.__BusinessUnitId = dealership.BusinessUnitId
                    value_list.push( site_data );
                });
            });
            self.fn.processing_calculate_averages( return_obj );
            return return_obj;
        };
        // 
        self.fn.filter_by_site_name = function( data ) {
            if( !data )
                return null;
            var filter = self.static_config.listing_site_filter;
            function f( site_data, site_name ) {
                return _(filter).contains( site_name );
            }
            return {
                DATA: _(data.DATA).objFilter( f ),
                META: _(data.META).objFilter( f )
            };
        };
        //
        self.fn.group_by_site_name = function( data ) {
            if( !data )
                return null;
            var return_obj = {
                DATA: $.extend( {}, data.DATA ), // shallow (intentional)
                META: $.extend( {}, data.META ) // shallow (intentional)
            };
            var filter = self.static_config.listing_site_filter;
            var regrouped_name = self.static_config.regrouped_listing_site_name;
            var site_names = _(return_obj.DATA).keys();
            _(site_names).each( function( site_name ) {
                if( !_(filter).contains( site_name )) { // would be filtered out
                    // if nothing has been detected in this way, the regrouped objects need to be created first
                    if( !return_obj.DATA[regrouped_name] ) {
                        return_obj.DATA[regrouped_name] = [];
                        return_obj.META[regrouped_name] = new Metadata();
                    }
                    // update DATA segment by aggregating the data objects under a new site name
                    return_obj.DATA[regrouped_name] = return_obj.DATA[regrouped_name].concat( return_obj.DATA[site_name] );
                    // update META segment (TOTALS/MONTHLY_TOTALS only) by summing at field-level
                    self.fn.processing_add_fields( return_obj.META[regrouped_name].TOTALS, return_obj.META[site_name].TOTALS );
                    self.fn.processing_add_fields( return_obj.META[regrouped_name].MONTHLY_TOTALS, return_obj.META[site_name].MONTHLY_TOTALS );
                    // AVERAGES to be calculated later on (must wait 'til all the data is in)
                    // remove filtered-out objects
                    delete return_obj.DATA[site_name];
                    delete return_obj.META[site_name];
                }
            });
            self.fn.processing_calculate_averages( return_obj );
            return return_obj;
        };
        // calculate averages on a well-formed object structure, assuming that TOTALS have already been calculated
        self.fn.processing_calculate_averages = function( data ) {
            _(data.META).each( function( meta_obj, site_name ) {
                var value_count = data.DATA[site_name].length;
                _(meta_obj.TOTALS).each( function( field_value, field_key ) {
                    meta_obj.AVERAGES[field_key] = ((value_count != 0) ? (field_value / value_count) : 0);
                });
                _(meta_obj.MONTHLY_TOTALS).each( function( field_value, field_key ) {
                    meta_obj.MONTHLY_AVERAGES[field_key] = ((value_count != 0) ? (field_value / value_count) : 0);
                });
            });
        };
        // reduces two objects into the first object, by adding the numeric values at the field level
        self.fn.processing_add_fields = function( augend_sum, addend ) {
            _(addend).each( function( addend_value, addend_key ) {
                if( !_(addend_value).isNumber() )
                    return;
                if( typeof augend_sum[addend_key] === "undefined" )
                    augend_sum[addend_key] = addend_value;
                else if( _(augend_sum[addend_key]).isNumber() )
                    augend_sum[addend_key] += addend_value;
            });
        };
        function Metadata() {
            return {
                TOTALS: {}, // these objects will contain precisely the same keys
                MONTHLY_TOTALS: {}, // one for each site-data field name
                AVERAGES: {}, // the "MONTHLY_" ones are derived by dividing by Months on the data
                MONTHLY_AVERAGES: {} // and the AVERAGES are derived by dividing by number of data points present in the site-data-series
            };
        }
        // serves as a useful intermediate data structure that many of the group-level charts depend on
        //   without having to recalculate too much
        // self.data.processing.GroupSitePerformanceForMonth_PermittedBusinessUnits_GroupBySite = ko.computed( function() {
        //     var data = self.data.processing.GroupSitePerformanceForMonth_PermittedBusinessUnits();
        //     return self.fn.processing_calculate_metadata( data );
        // });
        // // remove the sites together that don't match the static_config.listing_site_filter
        // self.data.processing.GroupSitePerformanceForMonth_PermittedBusinessUnits_GroupBySite_Filtered = ko.computed( function() {
        //     var data = self.data.processing.GroupSitePerformanceForMonth_PermittedBusinessUnits_GroupBySite();
        //     return self.fn.filter_by_site_name( data );
        // });
        // // re-group the sites that match the static_config.listing_site_filter under static_config.regrouped_listing_site_name
        // self.data.processing.GroupSitePerformanceForMonth_PermittedBusinessUnits_GroupBySite_ReGrouped = ko.computed( function() {
        //     var data = self.data.processing.GroupSitePerformanceForMonth_PermittedBusinessUnits_GroupBySite();
        //     return self.fn.group_by_site_name( data );
        // });
        // serves as a useful intermediate data structure that many of the group-level charts depend on
        //   without having to recalculate too much
        self.data.processing.GroupSitePerformanceForMonth_CategoricalBusinessUnits_GroupBySite = ko.computed( function() {
            var data = self.data.processing.GroupSitePerformanceForMonth_CategoricalBusinessUnits();
            return _(data).objMap( self.fn.processing_calculate_metadata ); // each category of (data) has its metadata calculated independently
        });
        // remove the sites together that don't match the static_config.listing_site_filter
        self.data.processing.GroupSitePerformanceForMonth_CategoricalBusinessUnits_GroupBySite_Filtered = ko.computed( function() {
            var data = self.data.processing.GroupSitePerformanceForMonth_CategoricalBusinessUnits_GroupBySite();
            return _(data).objMap( self.fn.filter_by_site_name );
        });
        // re-group the sites that match the static_config.listing_site_filter under static_config.regrouped_listing_site_name
        self.data.processing.GroupSitePerformanceForMonth_CategoricalBusinessUnits_GroupBySite_ReGrouped = ko.computed( function() {
            var data = self.data.processing.GroupSitePerformanceForMonth_CategoricalBusinessUnits_GroupBySite();
            return _(data).objMap( self.fn.group_by_site_name );
        });
        //
        // 'data' parameter expected to have a META field of type Metadata
        self.fn.prepare_chart_data = function( data, category, field, sort ) {
            if( !data )
                return null;
            return {
                series: [{
                    data: _(data.META).map( function( site_meta, site_name ) {
                        return {
                            y: site_meta[category][field],
                            name: site_name,
                            color: self.VendorDefinition( site_name ).colors.blend
                        };
                    }).sort( function( a, b ) {
                        return b.y - a.y; // descending
                    })
                }]
            };
        };
        self.fn.prepare_simple_rates = function( data, category, field, sort ) {
            if( !data )
                return null;
            return _(data.META).map( function( site_meta, site_name ) {
                return {
                    value: site_meta[category][field],
                    name: site_name
                };
            }).sort( function( a, b ) {
                return b.value - a.value; // descending
            });
        };
        self.fn.prepare_simple_budgets = function( data, category, field, sort ) {
            if( !data )
                return null;
            return _(data.META).map( function( site_meta, site_name ) {
                return {
                    budget: site_meta[category][field],
                    site_name: site_name
                };
            }).sort( function( a, b ) {
                return b.budget - a.budget; // descending
            });
        }

        // filter GA items

        // select only the TOTAL item from GA data
        self.data.processing.GroupGoogleAnalyticsDataAll_Totals = ko.computed( function() {
            var data = self.services.ga.response();
            if( !data )
                return null;
            return _(data).find( function( dealership ) {
                return dealership.BusinessUnit == "TOTAL";
            });
        });

        self.data.processing.Website20Data = ko.computed(function() {
            if (!self.dynamic_config.hasShowroomReporting || !self.dynamic_config.hasDigitalShowroom) return {};
            var data = self.services.website20showroom.response();
            if (!data)
                return null;
            return data;
        });
        self.data.processing.Website20Traffic = ko.computed(function() {
            if (!self.dynamic_config.hasShowroomReporting || !self.dynamic_config.hasWebsite20) return {};
            var data = self.services.website20traffic.response();
            if (!data)
                return null;
            return data;
        });
        // dynamic data is computed from service response + UI state
        self.data.dynamic = {};

        self.data.dynamic.GroupSitePerformanceForMonth_PermittedBusinessUnits_GroupBySite_Filtered_TotalDirectLeadsDynamic = ko.computed( function() {
            var data = self.data.processing.GroupSitePerformanceForMonth_CategoricalBusinessUnits_GroupBySite_Filtered().Permitted_DigitalPerformanceAnalytics;
            if( !data )
                return null;
            var ui = {
                email: self.controls.enableLeadsEmail(),
                phone: self.controls.enableLeadsPhone(),
                chat: self.controls.enableLeadsChat()
            };
            _(data.DATA).each( function( site_data_array, site_name ) {
                var meta = data.META[site_name];
                meta.TOTALS.__TotalDirectLeadsDynamic = 0;
                meta.MONTHLY_TOTALS.__TotalDirectLeadsDynamic = 0;
                _(site_data_array).each( function( site_data ) {
                    site_data.__TotalDirectLeadsDynamic =
                        (ui.email? site_data.EmailValue : 0) +
                        (ui.phone? site_data.PhoneValue : 0) +
                        (ui.chat?  site_data.ChatValue : 0);
                    var field_value = site_data.__TotalDirectLeadsDynamic;
                    meta.TOTALS.__TotalDirectLeadsDynamic += field_value;
                    var monthly_average_value = (site_data.Months != 0) ? (field_value / site_data.Months) : 0;
                    meta.MONTHLY_TOTALS.__TotalDirectLeadsDynamic += monthly_average_value;
                });
                // not strictly necessary anymore, but does prevent confusion during debugging
                delete meta.AVERAGES.__TotalDirectLeadsDynamic;
                delete meta.MONTHLY_AVERAGES.__TotalDirectLeadsDynamic;
            });
            self.fn.processing_calculate_averages( data ); // for the sake of the new field
            return data;
        });

        // alert panels, left/right
        self.data.alerts = {};

        function aggregate_alerts_list_function_factory( threshold_accessor, bucket_field ) {
            return function() {
                var data = self.services.data.response();
                if( !data )
                    return null;
                var threshold = threshold_accessor();
                return _.chain(data)
                    .map( function( dealership ) {
                        return {
                            name: dealership.BusinessUnit,
                            count: dealership.Data.BucketsData[bucket_field],
                            total: dealership.Data.BucketsData.SelectedFilterCount
                        };
                    }).filter( function( list_item ) {
                        if( list_item.total === 0 )
                            return false;
                        return (list_item.count / list_item.total) > threshold;
                    }).sortBy( function( list_item ) {
                        return list_item.count * -1;
                    }).value();
            };
        }

        self.data.alerts.action = ko.computed( 
            aggregate_alerts_list_function_factory(
                self.virtualControls.alertActionThreshold, "ActionBucketsTotal" ));

        self.data.alerts.activity = ko.computed( 
            aggregate_alerts_list_function_factory(
                self.virtualControls.alertActivityThreshold, "ActivityBucketsTotal" ));

        // cache indicator/UI
        self.data.cache = {};

        self.data.cache.current = ko.computed( function() {
            return self.services.cache.response() || self.services.cache.previous_response();
        });

        self.data.cache.display = ko.computed( function() {
            // Status: "Empty|Pending|Processing|Failed|Cached"
            var data = self.data.cache.current();
            var result = {
                active: false,
                progress: "0%",
                message: null,
                visible: false,
                css: null
            };
            if( data ) {
                var refresh_in_progress = self.virtualControls.manualCacheRefreshInProgress();
                result.active = (data.Status == "Pending" || data.Status == "Processing" );
                result.progress = result.active? ((data.PercentComplete * 100) + "%") : "0%";
                var d = self.fn.parseJSONPDate( data.TimeStamp );
                var d_str = d.toString("h:mm tt") +" "+ d.getTimezone();
                if( data.Status == "Failed" || data.Status == "Empty" ) {
                    result.message = "Refresh failed, please try again later.";
                    self.virtualControls.manualCacheRefreshInProgress( false );
                } else { // data.Status == Pending|Processing|Cached
                    if( data.Status == "Pending" )
                        result.message = "Data refreshed at "+d_str+". <strong>Getting latest data ...</strong>";
                    else if( data.Status == "Processing" )
                        result.message = "Data refreshed at "+d_str+". <strong>Processing data ...</strong>";
                    else if( data.Status == "Cached" ) {
                        if( !refresh_in_progress ) {
                            result.message = "Data refreshed at "+d_str+".";
                        } else {
                            result.message = "Click here to see the newest data.";
                            result.css = "interactive";
                        }
                    }
                }
                result.visible = true;
            }
            return result;
        });
    
        // does not return any useful value; the purpose of this computed is its side effects
        self.data.cache.auto_poll = ko.computed( function() {
            // establish dependencies
            var loading = self.services.cache.loading();
            if( loading )
                return;
            var display = self.data.cache.display();
            if( !display.active )
                return;
            // not loading, but not finished; re-request
            _.delay( function() {
                // must re-check dependencies unfortunately
                if( self.services.cache.loading() || !(self.data.cache.display().active) )
                    return;
                self.services.cache.fetch();
            }, 5000 );
        });

        // UI method
        self.ForceCacheRefresh = function() {
            // block rapid clicks or circumvention of UI
            if( self.services.cache_refresh_now.loading() || self.data.cache.display().active )
                return;
            self.services.cache_refresh_now.fetch();
            self.virtualControls.manualCacheRefreshInProgress( true );
            self.services.cache.response( $.extend( true, {}, self.services.cache.response(), { Status: "Pending" }));
        };
        self.ForceCachePull = function() {
            var data = self.data.cache.current();
            if( !data || (data.Status != "Cached") || (!(self.virtualControls.manualCacheRefreshInProgress())) )
                return;
            self.virtualControls.manualCacheRefreshInProgress( false );
            self.services.ttm.fetch();
            self.services.ga.fetch();
            self.services.perf.fetch();
        };

        // chart data
        self.data.charts = {};


        self.data.charts.Overview_AvgSearches = ko.computed( function() {
            return self.fn.prepare_chart_data(
                self.data.processing.GroupSitePerformanceForMonth_CategoricalBusinessUnits_GroupBySite_Filtered().Permitted_OnlineClassifiedOverview,
                "MONTHLY_AVERAGES", "SearchValue" );
        });

        self.data.charts.Overview_AvgConsumerVDPImpressions = ko.computed( function() {
            return self.fn.prepare_chart_data(
                self.data.processing.GroupSitePerformanceForMonth_CategoricalBusinessUnits_GroupBySite_Filtered().Permitted_OnlineClassifiedOverview,
                "MONTHLY_AVERAGES", "DetailValue" );
        });

        self.data.charts.Overview_AvgTotalLeads = ko.computed( function() {
            return self.fn.prepare_chart_data(
                self.data.processing.GroupSitePerformanceForMonth_CategoricalBusinessUnits_GroupBySite_Filtered().Permitted_OnlineClassifiedOverview,
                "MONTHLY_AVERAGES", "ActionValue" );
        });

        self.data.charts.Overview_ClickThruRate = ko.computed( function() {
            return self.fn.prepare_simple_rates(
                self.data.processing.GroupSitePerformanceForMonth_CategoricalBusinessUnits_GroupBySite_Filtered().Permitted_OnlineClassifiedOverview,
                "AVERAGES", "ClickThroughRate" );
        });

        self.data.charts.Overview_ConversionRate = ko.computed( function() {
            return self.fn.prepare_simple_rates(
                self.data.processing.GroupSitePerformanceForMonth_CategoricalBusinessUnits_GroupBySite_Filtered().Permitted_OnlineClassifiedOverview,
                "AVERAGES", "ConversionRate" );
        });

        self.data.charts.Performance_AvgConsumerVDPImpressions = ko.computed( function() {
            return self.fn.prepare_chart_data(
                self.data.processing.GroupSitePerformanceForMonth_CategoricalBusinessUnits_GroupBySite_ReGrouped().Permitted_DigitalPerformanceAnalytics,
                "MONTHLY_AVERAGES", "DetailValue" );
        });

        self.data.charts.Performance_AvgMonthlyCost = ko.computed( function() {
            return self.fn.prepare_simple_budgets(
                self.data.processing.GroupSitePerformanceForMonth_CategoricalBusinessUnits_GroupBySite_Filtered().Permitted_DigitalPerformanceAnalytics,
                "MONTHLY_AVERAGES", "Budget" );
        });

        self.data.charts.Performance_AvgCostPerConsumerVDPImpression = ko.computed(function () {
            var cost_data = self.data.charts.Performance_AvgMonthlyCost();
            var impressions_data = self.data.charts.Performance_AvgConsumerVDPImpressions();
            if (!cost_data || !impressions_data)
                return null;
            return {
                series: [{
                    data: _.chain(impressions_data.series[0].data)
                        .map(function (impressions_data_point) {
                            var cost_data_point = _(cost_data).find(function (cost_data_point) {
                                return cost_data_point.site_name == impressions_data_point.name;
                            });
                            if( !cost_data_point )
                                return undefined;
                            var numerator__less_accurate = (cost_data_point && cost_data_point.budget) ? parseFloat( (cost_data_point.budget).toFixxed( 0 )) : 0;
                            var denominator__less_accurate = parseFloat( (impressions_data_point.y).toFixxed( 0 ));
                            return {
                                name: impressions_data_point.name,
                                y: ((denominator__less_accurate > 0) ? (numerator__less_accurate / denominator__less_accurate) : 0),
                                color: impressions_data_point.color
                            };
                        })
                        .compact()
                        .value()
                }]
            };
        });

        self.data.charts.Performance_AvgNumberDirectLeads = ko.computed( function() {
            return self.fn.prepare_chart_data(
                self.data.dynamic.GroupSitePerformanceForMonth_PermittedBusinessUnits_GroupBySite_Filtered_TotalDirectLeadsDynamic(),
                "MONTHLY_AVERAGES", "__TotalDirectLeadsDynamic" );
        });

        self.data.charts.Performance_AvgCostPerDirectLead = ko.computed( function() {
            var cost_data = self.data.charts.Performance_AvgMonthlyCost();
            var leads_data = self.data.charts.Performance_AvgNumberDirectLeads();
            if( !cost_data || !leads_data )
                return null;
            return {
                series: [{
                    data: _(leads_data.series[0].data).map( function( leads_data_point ) {
                        var cost_data_point = _(cost_data).find( function( cost_data_point ) {
                            return cost_data_point.site_name == leads_data_point.name;
                        });
                        var numerator__less_accurate = (cost_data_point && cost_data_point.budget) ? parseFloat( (cost_data_point.budget).toFixxed( 0 )) : 0;
                        var denominator__less_accurate = parseFloat( (leads_data_point.y).toFixxed( 0 ));
                        return {
                            name: leads_data_point.name,
                            y: ((denominator__less_accurate > 0) ? (numerator__less_accurate / denominator__less_accurate) : 0),
                            color: leads_data_point.color
                        };
                    })
                }]
            };
        });

        function format_GA_source_medium( source, maxlen ) {
            var separator = source.indexOf("|"),
                medium = source.substr(separator+1);
            source = source.substr(0, separator);
            if (medium === "organic")
                source += " " + medium;
            else if (_(["cpc","ppc"]).contains(medium))
                source += " (paid)";
            source = trimString(source, maxlen);
            /*if( maxlen && source.length > maxlen )
                source = source.substr( 0, (maxlen-3) ) + "...";*/
            return source;
        }
        function trimString( source, maxlen ) {
            if (maxlen && source.length > maxlen)
                source = source.substr(0, (maxlen - 3)) + "...";
            return source;
        }

        self.data.charts.WebsiteTraffic_TrafficSources = ko.computed( function() {
            var data = self.data.processing.GroupGoogleAnalyticsDataAll_Totals();
            if( !data || !data.Referrals )
                return null;
            var totalVisits = _(data.Referrals.Sources.Rows).reduce( function( tally, data_point ) {
                return tally + data_point.Visits;
            }, 0 );
            if( totalVisits === 0 )
                return null;
            return {
                series: [{
                    data: _.chain(data.Referrals.Sources.Rows)
                        .map( function( data_point, name ) {
                            return {
                                name: format_GA_source_medium(name, 18),
                                __longname: format_GA_source_medium(name),
                                color: self.VendorDefinition("Dealer Website").colors.blend,
                                y: (data_point.Visits / totalVisits) * 100
                            };
                        })
                        .sortBy( function( mapped_data_point ) {
                            return mapped_data_point.y * -1;
                        })
                        .take( 10 )
                        .value()
                }]
            };
        });
        self.data.charts.Website20Showroom = ko.computed(function () {
            if (!self.dynamic_config.hasShowroomReporting || !self.dynamic_config.hasDigitalShowroom) return {};
            var data = self.services.website20showroom.response();
            if (!data)
                return null;
            var scrubbed_data = _.chain(data.DealershipTrafficList).map(function(value) {

                    var datapoint = {
                        name: trimString(value.DealershipName, 18),
                        __longname: value.DealershipName,
                        color: self.VendorDefinition("Dealer Website").colors.blend,
                        y: value.CombinedCount
                    };

                    return datapoint;
                })
                .sortBy(function(datapoint) {
                    return datapoint.y * -1;
                }).take(8).value();
            return {
                series: [{
                    data: scrubbed_data
                }]
            };
        });

        self.data.charts.Website20Engagement = ko.computed(function () {
            if (!self.dynamic_config.hasShowroomReporting || !self.dynamic_config.hasWebsite20) return {};
            var data = self.services.website20traffic.response();
            if (!data) return null;
            var avg = _.reduce(data.DealershipTrafficList, function(memo, traffic) {
                return traffic.AvgTimeOnPageMilliseconds + memo;
            }, 0);
            var dataSets = _.reduce(data.DealershipTrafficList, function(memo, traffic) { // do not average on zero value datapoints
                if (traffic.AvgTimeOnPageMilliseconds > 0) return 1 + memo; // increment
                return memo; // nothing
            }, 0);
            if (dataSets > 0) { // no divide by zero
                avg /= dataSets;
            }
            var columns = [];
            columns.push({
                name: "",
                color: self.static_config.styles.BlueColumnGradient,
                y: avg
            });
            return {
                series: [{
                    data: columns
                }] 
            };
        });
        self.data.charts.Website20Views = ko.computed(function () {
            if (!self.dynamic_config.hasShowroomReporting || !self.dynamic_config.hasWebsite20) return {};
            var data = self.services.website20traffic.response();
            if (!data) return null;
            var totalViews = _.reduce(data.DealershipTrafficList, function(memo, traffic) {
                return traffic.PageViews + memo;
            }, 0);
            
            var columns = [];
            columns.push({
                name: "",
                color: self.static_config.styles.YellowColumnGradient,
                y: totalViews
            });
            return {
                series: [{
                    data: columns
                }] 
            };
        });
        function aggregate_timeToMarket_averages_function_factory( field ) {
            return function() {
                var data = self.services.ttm.response();
                if( !data )
                    return null;
                return {
                    series: [{
                        data: _.chain(data.BusinessUnits)
                            .map( function( data_point ) {
                                return {
                                    name: data_point.BusinessUnit,
                                    y: data_point[field]
                                };
                            }).sortBy("y")
                            .reverse()
                            .take( 8 )
                            .value()
                    }]
                };
            };
        }
        self.data.charts.TimeToMarket_DaysToOnlineWithPhotos = ko.computed( 
            aggregate_timeToMarket_averages_function_factory( "AverageDaysToPhotoOnline" ));

        self.data.charts.TimeToMarket_DaysToCompleteAdOnline = ko.computed(
            aggregate_timeToMarket_averages_function_factory("AverageDaysToCompleteAd"));

        self.data.charts.TimeToMarket_DaysToOnlineWithPhotos_GroupAverage = ko.computed( function() {
            var data = self.services.ttm.response();
            if( !data )
                return null;
            return data.AverageDaysToPhotoOnline;
        });

        self.data.charts.TimeToMarket_DaysToCompleteAdOnline_GroupAverage = ko.computed( function() {
            var data = self.services.ttm.response();
            if( !data )
                return null;
            return data.AverageDaysToCompleteAdOnline;
        });

        


        // chart UI wrappers
        self.CreateChart({
            id: "Overview_AvgSearches",
            type: SimpleColumnChart,
            container_selector: "#chart_stats_searches",
            data_observable: self.data.charts.Overview_AvgSearches,
            highcharts_settings: {
                chart: { width: 190, height: 256 },
                title: { text: "AVG SEARCHES" }
            }
        });
        self.CreateChart({
            id: "Overview_AvgConsumerVDPImpressions",
            type: SimpleColumnChart,
            container_selector: "#chart_stats_pages",
            data_observable: self.data.charts.Overview_AvgConsumerVDPImpressions,
            highcharts_settings: {
                chart: { width: 190, height: 256 },
                title: { text: "AVG CONSUMER (VDP) IMPRESSIONS" }
            }
        });
        self.CreateChart({
            id: "Overview_AvgTotalLeads",
            type: SimpleColumnChart,
            container_selector: "#chart_stats_actions",
            data_observable: self.data.charts.Overview_AvgTotalLeads,
            highcharts_settings: {
                chart: { width: 190, height: 256 },
                title: { text: "AVG TOTAL LEADS" }
            }
        });

        self.CreateChart({
            id: "Performance_AvgConsumerVDPImpressions",
            type: SquatColumnChart,
            container_selector: "#chart_cv_chart",
            data_observable: self.data.charts.Performance_AvgConsumerVDPImpressions,
            highcharts_settings: {
                chart: { width: 262, height: 190, spacingLeft: 5 },
                title: { text: "Avg Consumer (VDP) Impressions" }
            }
        });

        var missing_budget_message_options = {
            series: {
                dataLabels: {
                    formatter: function() {
                        if( !(this.y > 0) )
                            return "Please set your<br /><a class='not-a-link'>monthly cost</a><br />to see this data.";
                    },
                    enabled: true,
                    useHTML: true,
                    align: "left",
                    x: 20,
                    y: 7,
                    crop: false,
                    style: {
                        fontWeight: "normal",
                        fontSize: "10px",
                        lineHeight: "12px"
                    }
                }
            }
        };
        self.CreateChart({
            id: "Performance_AvgCostPerConsumerVDPImpression",
            type: BarChartWithBenchmarks,
            container_selector: "#chart_cpv_chart",
            data_observable: self.data.charts.Performance_AvgCostPerConsumerVDPImpression,
            highcharts_settings: {
                chart: { width: 220, height: 190, spacingRight: 0 },
                title: { text: "Avg Cost Per Consumer (VDP) Impression" },
                plotOptions: missing_budget_message_options
            }
        });
        self.CreateChart({
            id: "Performance_AvgNumberDirectLeads",
            type: SquatColumnChart,
            container_selector: "#chart_act_chart",
            data_observable: self.data.charts.Performance_AvgNumberDirectLeads,
            highcharts_settings: {
                chart: { width: 232, height: 161 },
                title: { text: "Avg Number of Direct Leads" }
            }
        });
        self.CreateChart({
            id: "Performance_AvgCostPerDirectLead",
            type: BarChartWithBenchmarks,
            container_selector: "#chart_cpa_chart",
            data_observable: self.data.charts.Performance_AvgCostPerDirectLead,
            highcharts_settings: {
                chart: { width: 232, height: 161 },
                title: { text: "Avg Cost Per Direct Lead" },
                plotOptions: missing_budget_message_options
            }
        });
        
        self.CreateChart({
            id: "WebsiteTraffic_TrafficSources",
            type: HangingLabelColumnChart,
            container_selector: "#chart_ga_sources",
            data_observable: self.data.charts.WebsiteTraffic_TrafficSources,
            highcharts_settings: {
                chart: { width: 484, height: 210 },
                tooltip: {
                    formatter: function () {
                        var value_str = number_format( this.y, (this.y<1?1:0) );
                        if( value_str == "1.0" ) // special case
                            value_str = "1";
                        return value_str+"% of traffic to your dealership<br /> website came from "+
                            this.point.__longname+".";
                    }
                }
            }
        });

        self.CreateChart({
            ignoreQunit: true,
            id: "Website20Showroom",
            type: HangingLabelColumnChart,
            container_selector: "#chart_showroom",
            data_observable: self.data.charts.Website20Showroom,
            highcharts_settings: {
                plotOptions: {
                    series: {
                        dataLabels: {
                            formatter: function() {
                                return this.y;
                            }
                        }
                    }
                },
                yAxis: {
                    labels: {
                        formatter: function() {
                            return this.value;
                        }
                    },
                },
                chart: { marginLeft: 60, marginRight: 35, width: 484, height: 240 },
                    tooltip: {
                        formatter: function () {
                            var value_str = number_format(this.y, (this.y < 1 ? 1 : 0));
                            if (value_str == "1.0") // special case
                                value_str = "1";
                            return this.point.__longname + ": " + value_str + " vehicles viewed.";
                        }
                    }
            }
        });
        self.CreateChart({
            ignoreQunit: true,
            id: "Website20Engagement",
            type: SquatColumnChart,
            container_selector: "#chart_website20engagement",
            data_observable: self.data.charts.Website20Engagement,
            highcharts_settings: {
                plotOptions: {
                    series: {
                        pointWidth: 60,
                        dataLabels: {
                            enabled: true,
                            formatter: function() {
                                return msToTime(this.y);
                            }
                        }
                    }
                },
                xAxis: { labels: { enabled: false }},
                yAxis: { min: 0, minRange: 5 * 60 * 1000 }, /* 5 minute engagement span minimum */
                chart: { width: 242, height: 236 },
                title: { text: 'Consumer Engagement', margin: 20 },
                subtitle: { text: '(Avg Time on Page)'}
            }
        });
        self.CreateChart({
            ignoreQunit: true,
            id: "Website20Views",
            type: SquatColumnChart,
            container_selector: "#chart_website20views",
            data_observable: self.data.charts.Website20Views,
            highcharts_settings: {
                plotOptions: {
                    series: {
                        pointWidth: 60
                    }
                },
                xAxis: { labels: { enabled: false }},
                yAxis: { min: 0, minRange: 0 },
                chart: { width: 242, height: 236 },
                title: { text: 'Total Vehicles Viewed', margin: 20 }
            }
        });

        self.CreateChart({
            id: "TimeToMarket_DaysToOnlineWithPhotos",
            type: HangingLabelColumnChartWithValueIndicator,
            container_selector: "#chart_ttm_g_owp",
            data_observable: self.data.charts.TimeToMarket_DaysToOnlineWithPhotos,
            highcharts_settings: {
                chart: { width: 484, height: 250 },
                plotOptions: { 
                    series: {
                        color: {
                            linearGradient: { x1:0,y1:0, x2:1,y2:0 },
                            stops: [[0,"#00BAEE"],[1,"#008CBD"]]
                        }
                    }
                },
                xAxis: {
                    labels: {
                        formatter: function() {
                            return self.fn.radical_string_shortening( this.value, 25 );
                        }
                    }
                }
            },
            extensions: {
                value_observable: self.data.charts.TimeToMarket_DaysToOnlineWithPhotos_GroupAverage,
                value_label_img_src: 'Themes/MaxMvc/Images/Dashboard/group_avg.png'
            }
        });
        self.CreateChart({
            id: "TimeToMarket_DaysToCompleteAdOnline",
            type: HangingLabelColumnChartWithValueIndicator,
            container_selector: "#chart_ttm_g_cao",
            data_observable: self.data.charts.TimeToMarket_DaysToCompleteAdOnline,
            highcharts_settings: {
                chart: { width: 484, height: 250 },
                plotOptions: { 
                    series: {
                        color: {
                            linearGradient: { x1:0,y1:0, x2:1,y2:0 },
                            stops: [[0,"#01bb40"],[1,"#118234"]]
                        }
                    }
                },
                xAxis: {
                    labels: {
                        formatter: function() {
                            return self.fn.radical_string_shortening( this.value, 25 );
                        }
                    }
                }
            },
            extensions: {
                value_observable: self.data.charts.TimeToMarket_DaysToCompleteAdOnline_GroupAverage,
                value_label_img_src: 'Themes/MaxMvc/Images/Dashboard/group_avg_green.png'
            }
        });

        // report data, all
        self.data.reports = {};

        function make_link_cell( data, href_field, text_field ) {
            if( !data )
                return new ReportCell( 0, Report.data_format.no_data_html );
            else
                return new ReportCell( data[text_field], '<a href="'+data[href_field]+'">'+data[text_field]+'</a>' );
        }
        function make_dealership_name_cell( dealership ) {
            if( !dealership )
                return make_link_cell();
            else {
                var cell = make_link_cell( dealership, "BusinessUnitHome", "BusinessUnit" );
                if( _(dealership.Data.DealerSiteData).some( function( site ){ return site.CredentialStatus == -1; }))
                    cell.display_str += Report.data_format.credential_warning_html;
                return cell;
            }
        }
        function make_bucket_cell( dealership, bucket_array_field, bucket_display_name ) {
            return make_link_cell(
                _(dealership.Data.BucketsData[bucket_array_field]).find( function(b){ return b.DisplayName==bucket_display_name; }),
                "NavigationUrl", "Count" );
        }

        self.data.reports.OnlineInventoryAlerts = ko.conditionallyComputed( function() {
            var data = self.data.processing.GroupDashboardData_PermittedBusinessUnits();
            if( !data )
                return null;
            return {
                cols: [
                    "dealership",
                    "total",
                    "not_online",
                    "no_price",
                    "no_photos",
                    "no_description"
                ],
                rows: _(data).map( function( dealership ) {
                    return [
                        make_dealership_name_cell( dealership ),
                        dealership.Data.BucketsData.ActionBucketsTotal,
                        make_bucket_cell( dealership, "ActionBuckets", "Not Online" ),
                        make_bucket_cell( dealership, "ActionBuckets", "No Price" ),
                        make_bucket_cell( dealership, "ActionBuckets", "No Photos" ),
                        make_bucket_cell( dealership, "ActionBuckets", "No Description" )
                    ];
                })
            };
        }, self.data.throttle.enable_reports );
        
        self.data.reports.AdQualityAlerts = ko.conditionallyComputed( function() {
            var data = self.data.processing.GroupDashboardData_PermittedBusinessUnits();
            if( !data )
                return null;
            return {
                cols: [
                    "dealership",
                    "total",
                    "low_activity",
                    "low_photos",
                    "no_packages",
                    "no_book_value",
                    "no_carfax",
                    "equipment_review",
                    "needs_repricing",
                    "no_trim"
                ],
                rows: _(data).map( function( dealership ) {
                    return [
                        make_dealership_name_cell( dealership ),
                        dealership.Data.BucketsData.ActivityBucketsTotal,
                        make_bucket_cell( dealership, "ActivityBuckets", "Low Online Activity" ),
                        make_bucket_cell( dealership, "ActivityBuckets", "Low Photos" ),
                        make_bucket_cell( dealership, "ActivityBuckets", "No Packages" ),
                        make_bucket_cell( dealership, "ActivityBuckets", "No Book Value" ),
                        make_bucket_cell( dealership, "ActivityBuckets", "No Carfax" ),
                        make_bucket_cell( dealership, "ActivityBuckets", "Equipment Review" ),
                        make_bucket_cell( dealership, "ActivityBuckets", "Needs Re-pricing" ),
                        make_bucket_cell( dealership, "ActivityBuckets", "No Trim" )
                    ];
                })
            };
        }, self.data.throttle.enable_reports );

        self.data.reports.OnlineClassifiedPerformance = ko.conditionallyComputed( function() {
            var perf_data = self.data.processing.GroupSitePerformanceForMonth_CategoricalBusinessUnits().Permitted_DigitalPerformanceAnalytics;
            var dash_data = self.data.processing.GroupDashboardData_PermittedBusinessUnits();
            var ttm_data = self.data.processing.GroupTimeToMarketReport_PermittedBusinessUnits();
            if( !perf_data || !ttm_data )
                return null;
            var result = {};
            var tab_template = {
                cols: [
                    "dealership",
                    "monthly_cost",
                    "consumer_vdps",
                    "costper_vdp",
                    "consumer_vdps_per_veh",
                    "direct_leads",
                    "costper_lead",
                    "direct_leads_per_veh",
                    "email_leads",
                    "phone_leads",
                    "chat_leads"
                ],
                rows: []
            };
            //var month_count = self.virtualControls.dateRangeSelectedMonthCount();
            _(perf_data).each( function( dealership ) {
                _(dealership.Data.Sites).each( function( site_data, site_name ) {
                    var month_count = site_data.Months; // assumed to be present
                    var tab = result[site_name];
                    if( !tab ) {
                        tab = $.extend( true, {}, tab_template );
                        result[site_name] = tab;
                    }
                    var DirectLeads = (site_data.EmailValue + site_data.PhoneValue + site_data.ChatValue);
                    var ttm_dealership = _(ttm_data.BusinessUnits).find( function( ttm_dealership ) {
                        return ttm_dealership.BusinessUnitId == dealership.BusinessUnitId;
                    });
                    var dash_dealership = _(dash_data).find( function( dash_dealership ) {
                        return dash_dealership.BusinessUnitId == dealership.BusinessUnitId;
                    });
                    var VehicleCount = ttm_dealership ? ttm_dealership.VehicleCount : 0;
                    tab.rows.push([
                        make_dealership_name_cell( dash_dealership ),
                        ((month_count > 0)? (site_data.Budget / month_count) : 0),
                        ((month_count > 0)? (site_data.DetailValue / month_count) : 0),
                        (site_data.Budget / site_data.DetailValue),
                        ((VehicleCount > 0)? ((site_data.DetailValue / VehicleCount)) : 0),
                        ((month_count > 0)? (DirectLeads / month_count) : 0),
                        ((DirectLeads > 0)? (site_data.Budget / DirectLeads) : 0),
                        ((VehicleCount > 0)? ((DirectLeads / VehicleCount)) : 0),
                        ((month_count > 0)? (site_data.EmailValue / month_count) : 0),
                        ((month_count > 0)? (site_data.PhoneValue / month_count) : 0),
                        ((month_count > 0)? (site_data.ChatValue / month_count) : 0)
                    ]);
                });
            });
            return result;
        }, self.data.throttle.enable_reports );

        self.data.reports.WebsiteTraffic = ko.conditionallyComputed( function() {
            var data = self.data.processing.GroupGoogleAnalyticsDataAll_Totals();
            if( !data )
                return null;
            var totalVisits = _(data.Referrals.Sources.Rows).reduce( function( tally, ga_row ) {
                return tally + ga_row.Visits;
            }, 0 );
            return {
                cols: [
                    "source",
                    "pct_visits",
                    "visits",
                    "pages_per",
                    "duration",
                    "pct_new",
                    "bounce_rt"
                ],
                rows: _(data.Referrals.Sources.Rows).map( function( ga_row, key ) {
                    return [
                        self.fn.format_GA_source_medium( key ),
                        (ga_row.Visits / totalVisits) * 100,
                        ga_row.Visits,
                        ga_row.PageviewsPerVisit,
                        ga_row.AverageVisitDuration,
                        ga_row.PercentageNew,
                        ga_row.VisitBounceRate
                    ];
                })
            };
        }, self.data.throttle.enable_reports);

        self.data.reports.Website20Showroom = ko.conditionallyComputed(function () {
            var data = self.data.processing.Website20Data();

            if (!data) return null;

            var report = {
                cols: [
                    "dealership",
                    "overall_viewed",
                    "digital_viewed",
                    "mobile_viewed",
                    "shares",
                    "prints"
                ],
                rows: _.map(data.DealershipTrafficList, function(dealership) {
                    var row = [make_link_cell(dealership, "BusinessUnitHome", "DealershipName"),
                        dealership.CombinedCount,
                        dealership.DigitalShowroomCombinedCount,
                        dealership.MobileShowroomCombinedCount,
                        dealership.Shares,
                        dealership.Prints];
                    return row;
                })
            };
            return report;
        }, self.data.throttle.enable_reports);

        self.data.reports.Website20Traffic = ko.conditionallyComputed(function () {
            var data = self.data.processing.Website20Traffic();

            if (!data) return null;

            var report = {
                cols: [
                    "dealership",
                    "customer_engagement",
                    "total_viewed",
                    "direct_leads",
                    "actions"
                ],
                rows: _.map(data.DealershipTrafficList, function(dealership) {
                    var row = [make_link_cell(dealership, "BusinessUnitHome", "DealershipName"),
                        dealership.AvgTimeOnPageMilliseconds,
                        dealership.PageViews,
                        dealership.TotalDirectLeads,
                        dealership.TotalActions];
                    return row;
                })
            };
            return report;
        }, self.data.throttle.enable_reports);

        self.data.reports.TimeToMarket = ko.conditionallyComputed( function() {
            var data = self.data.processing.GroupTimeToMarketReport_PermittedBusinessUnits();
            if (!data) return null;

            // Clamp values to positive ones only when calculating percents
            var LowestMinPhoto = Math.max(0, data.LowestMinDaysToPhotoOnline);
            var HighestMinPhoto = Math.max(0, data.HighestMinDaysToPhotoOnline);
            var UseMinPhoto = (LowestMinPhoto == HighestMinPhoto || HighestMinPhoto == 0 ? false : true);

            var LowestMaxPhoto = Math.max(0, data.LowestMaxDaysToPhotoOnline);
            var HighestMaxPhoto = Math.max(0, data.HighestMaxDaysToPhotoOnline);
            var UseMaxPhoto = (LowestMaxPhoto == HighestMaxPhoto || HighestMaxPhoto == 0 ? false : true);

            var LowestAvgPhoto = Math.max(0, data.LowestAvgDaysToPhotoOnline);
            var HighestAvgPhoto = Math.max(0, data.HighestAvgDaysToPhotoOnline);
            var UseAvgPhoto = (LowestAvgPhoto == HighestAvgPhoto || HighestAvgPhoto == 0 ? false : true);

            var LowestAdOnline = Math.max(0, data.LowestMinDaysToAdOnline);
            var HighestAdOnline = Math.max(0, data.HighestMinDaysToAdOnline);
            var UseAdOnline = (LowestAdOnline == HighestAdOnline || HighestAdOnline == 0 ? false : true);

            return {
                cols: [
                    "dealership",
                    "days_photos",
                    "days_photos_min",
                    "days_photos_max",
                    "days_complete"
                ],
                rows: _(data.BusinessUnits).map(function(dealership) {
                    return [
                        make_link_cell(dealership, "BusinessUnitHome", "BusinessUnit"),
                        dealership.AverageDaysToPhotoOnline,
                        dealership.MinDaysPhoto,
                        dealership.MaxDaysPhoto,
                        dealership.AverageDaysToCompleteAd
                    ];
                }),
                rowPcts: _(data.BusinessUnits).map(function (dealership) {
                    return [
                        0,
                        (UseAvgPhoto ? Math.round(100 * ((Math.max(0, dealership.AverageDaysToPhotoOnline) - LowestAvgPhoto) / (HighestAvgPhoto - LowestAvgPhoto))) : 0),
                        (UseMinPhoto ? Math.round(100 * ((Math.max(0, dealership.MinDaysPhoto) - LowestMinPhoto) / (HighestMinPhoto - LowestMinPhoto))) : 0),
                        (UseMaxPhoto ? Math.round(100 * ((Math.max(0, dealership.MaxDaysPhoto) - LowestMaxPhoto) / (HighestMaxPhoto - LowestMaxPhoto))) : 0),
                        (UseAdOnline ? Math.round(100 * ((Math.max(0, dealership.AverageDaysToCompleteAd) - LowestAdOnline) / (HighestAdOnline - LowestAdOnline))) : 0)
                    ];
                })

        };
        }, self.data.throttle.enable_reports );

        
        // report UI wrappers
        self.CreateReport({
            id: "OnlineInventoryAlerts",
            container_selector: "#action_alerts_report",
            indicator_selector: "#ActionAlerts .pointer",
            data_observable: self.data.reports.OnlineInventoryAlerts,
            loading_observable: self.services.data.loading,
            column_meta: {
                "dealership":     { display: "Dealership",   css: "text_align_left" },
                "total":          { display: "# Vehicles w/ Alerts" },
                "not_online":     { display: "Not Online" },
                "no_price":       { display: "No Price" },
                "no_photos":      { display: "No Photos" },
                "no_description": { display: "No Description" }
            },
            sort: { idx:1, dir:"desc" }
        });
        
        self.CreateReport({
            id: "AdQualityAlerts",
            container_selector: "#activity_alerts_report",
            indicator_selector: "#ActivityAlerts .pointer",
            data_observable: self.data.reports.AdQualityAlerts,
            loading_observable: self.services.data.loading,
            column_meta: {
                "dealership":       { display: "Dealership",   css: "text_align_left" },
                "total":            { display: "# Vehicles w/ Alerts" },
                "low_activity":     { display: "Low Activity" },
                "low_photos":       { display: "Low Photos" },
                "no_packages":      { display: "No Packages" },
                "no_book_value":    { display: "No Book Value" },
                "no_carfax":        { display: "No Carfax" },
                "equipment_review": { display: "Equipment Review" },
                "needs_repricing":  { display: "Needs Re-pricing" },
                "no_trim":          { display: "No Trim" }
            },
            sort: { idx:1, dir:"desc" }
        });

        self.CreateReport({
            id: "OnlineClassifiedPerformance",
            container_selector: "#perf_report",
            indicator_selector: "#WebsitePerformance .pointer",
            data_observable: self.data.reports.OnlineClassifiedPerformance,
            loading_observable: self.services.perf.loading,
            tabbed: true,
            column_meta: {
                "dealership":            { display: "Dealership",   css: "text_align_left" },  
                "monthly_cost":          { display: "Monthly Cost",                           data_format: "number.dollars" },
                "consumer_vdps":         { display: "Consumer (VDP) Impressions",             data_format: "number.integer" },
                "costper_vdp":           { display: "Cost Per Consumer (VDP) Impression",     data_format: "number.currency" },
                "consumer_vdps_per_veh": { display: "Consumer (VDP) Impressions per Vehicle", data_format: "number.float.2" },
                "direct_leads":          { display: "Direct Leads",                           data_format: "number.integer" },
                "costper_lead":          { display: "Cost Per Direct Lead",                   data_format: "number.currency" },
                "direct_leads_per_veh":  { display: "Direct Leads Per Vehicle",               data_format: "number.float.2" },
                "email_leads":           { display: "Email Leads",                            data_format: "number.integer" },
                "phone_leads":           { display: "Phone Leads",                            data_format: "number.integer" },
                "chat_leads":            { display: "Chat Leads",                             data_format: "number.integer" }
            },
            sort: { idx:4, dir:"desc" }
        });

        self.CreateReport({
            id: "WebsiteTraffic",
            container_selector: "#traffic_report",
            indicator_selector: "#WebsitePerformance .pointer",
            data_observable: self.data.reports.WebsiteTraffic,
            loading_observable: self.services.ga.loading,
            column_meta: {
                "source":     { display: "Source / Medium", css: "text_align_left" },
                "pct_visits": { display: "% Visits",           data_format: "number.percent" },
                "visits":     { display: "Visits",             data_format: "number.integer" },
                "pages_per":  { display: "Pages per Visit",    data_format: "number.float.2" },
                "duration":   { display: "Avg Visit Duration", data_format: "timespan.hours" },
                "pct_new":    { display: "% New Visits",       data_format: "number.percent" },
                "bounce_rt":  { display: "Bounce Rate",        data_format: "number.percent"}
            },
            sort: { idx:2, dir:"desc" }
        });

        self.CreateReport({
            id: "TimeToMarket",
            container_selector: "#timetomarket_report",
            indicator_selector: "#WebsitePerformance .pointer",
            data_observable: self.data.reports.TimeToMarket,
            loading_observable: self.services.ttm.loading,
            column_meta: {
                "dealership":         { display: "Dealership",   css: "text_align_left" },
                "days_photos":        { display: "Avg Days to Online with Photo(s)", data_format: "number.float.1" },
                "days_photos_min":    { display: "Min Days to Online with Photo(s)", data_format: "number.integer" },
                "days_photos_max":    { display: "Max Days to Online with Photo(s)", data_format: "number.integer" },
                "days_complete":      { display: "Avg Days to Complete Ad Online",   data_format: "number.float.1" }
            },
            sort: { idx:2, dir:"desc" }
        });

        self.CreateReport({
            ignoreQunit: true,
            id: "Website20Showroom",
            container_selector: "#website20showroom_report",
            indicator_selector: "#WebsitePerformance .pointer",
            data_observable: self.data.reports.Website20Showroom,
            loading_observable: self.services.website20showroom.loading,
            column_meta: {
                "dealership": { display: "Dealership", css: "text_align_left" },
                "overall_viewed": { display: "Overall Vehicles Viewed", data_format: "number.integer" },
                "digital_viewed": { display: "Digital Showroom Vehicles Viewed", data_format: "number.integer" },
                "mobile_viewed": { display: "Mobile Showroom Vehicles Viewed", data_format: "number.integer" },
                "shares": { display: "Shares", data_format: "number.integer" },
                "prints": { display: "Prints", data_format: "number.integer" }
            },
            sort: { idx:1, dir:"desc" }
        });

        self.CreateReport({
            ignoreQunit: true,
            id: "Website20Traffic",
            container_selector: "#website20traffic_report",
            indicator_selector: "#WebsitePerformance .pointer",
            data_observable: self.data.reports.Website20Traffic,
            loading_observable: self.services.website20showroom.loading,
            column_meta: {
                "dealership": { display: "Dealership", css: "text_align_left" },
                "customer_engagement": { display: "Customer Engagement<br/> (Avg. Time on Page)", data_format: "number.ms_to_time" },
                "total_viewed": { display: "Total Vehicles Viewed", data_format: "number.integer" },
                "direct_leads": { display: "Direct Leads", data_format: "number.integer" },
                "actions": { display: "Actions", data_format: "number.integer" }
            },
            sort: { idx:1, dir:"desc" }
        });
        // errors
        self.errors.Performance = ko.computed( function() {
            if( self.services.perf.loading() )
                return null;
            var data = self.services.perf.response();
            if( data === null || (_(data).isArray() && data.length == 0) )
                return MaxDashboardBase.error_html.no_data;
            return null;
        });
        // these are all tied to performance data as well
        self.errors.Overview = self.errors.Performance; 
        self.errors.Performance_CostPerConsumerVDPImpression = self.errors.Performance;
        self.errors.Performance_CostPerDirectLead = self.errors.Performance;

        self.errors.GoogleAnalytics = ko.computed( function() {
            if( self.services.ga.loading() )
                return null;
            var data = self.services.ga.response();
            if( typeof data === "object" && (data == null || (data.errorMessage && data.errorData )))
                return MaxDashboardBase.error_html.no_data;
            return null;
        });
        self.errors.WebsiteTraffic_TrafficSources = ko.computed( function() {
            if( self.services.ga.loading() )
                return null;
            var base_err = self.errors.GoogleAnalytics();
            if( base_err )
                return base_err;
            var data = self.services.ga.response();
            var totals = self.data.processing.GroupGoogleAnalyticsDataAll_Totals();
            if( _(data).isEmpty() || !totals || !totals.Referrals || _(totals.Referrals.Sources.Rows).isEmpty() )
                return MaxDashboardBase.error_html.no_data;
            return null;
        });
        self.errors.Website20Showroom = ko.computed(function () {
            if (self.services.website20showroom.loading())
                return null;
            if (!self.dynamic_config.hasShowroomReporting || !self.dynamic_config.hasDigitalShowroom) return {};
            var data = self.services.website20showroom.response();
            if (_(data).isEmpty() || !_(data.Errors).isEmpty() || _(data.DealershipTrafficList).isEmpty()) {
                return MaxDashboardBase.error_html.no_data;
            }
            return null;
        });
        self.errors.Website20Traffic = ko.computed(function () {
            if (self.services.website20traffic.loading())
                return null;
            if (!self.dynamic_config.hasShowroomReporting || !self.dynamic_config.hasWebsite20) return {};
            var data = self.services.website20traffic.response();
            if (_(data).isEmpty() || !_(data.Errors).isEmpty() || _(data.DealershipTrafficList).isEmpty()) {
                return MaxDashboardBase.error_html.no_data;
            }
            return null;
        });
        
        function check_error_by_chart_data_sum_function_factory( loading_accessor, data_accessor ) {
            return function() {
                if( loading_accessor() )
                    return null;
                var data = data_accessor();
                if( !data )
                    return MaxDashboardBase.error_html.no_data;
                var sum = _(data.series[0].data).reduce( function( tally, data_point ) {
                    return tally + data_point.y;
                }, 0 );
                if( sum <= 0 )
                    return MaxDashboardBase.error_html.no_data;
                return null;
            };         
        }
        self.errors.TimeToMarket_DaysToOnlineWithPhotos = ko.computed( 
            check_error_by_chart_data_sum_function_factory(
                self.services.ttm.loading, self.data.charts.TimeToMarket_DaysToOnlineWithPhotos ));
        
        self.errors.TimeToMarket_DaysToCompleteAdOnline = ko.computed( 
            check_error_by_chart_data_sum_function_factory(
                self.services.ttm.loading, self.data.charts.TimeToMarket_DaysToCompleteAdOnline ));



    });

    self.PostBindingInitialize = _(self.PostBindingInitialize).wrap( function( base_fn ) {
        base_fn();

        // alert panels scrollbars
        var mCustomScrollbar_params = {
            scrollInertia: 0
        };
        $("#inventory-alerts").mCustomScrollbar( mCustomScrollbar_params );
        self.data.alerts.action.subscribe( function( list ) {
            $("#inventory-alerts").mCustomScrollbar( "update" );
        });
        $("#quality-alerts").mCustomScrollbar( mCustomScrollbar_params );
        self.data.alerts.activity.subscribe( function( list ) {
            $("#quality-alerts").mCustomScrollbar( "update" );
        });

        // dependencies
        self.controls.analyticsSuiteToggle.subscribe( self.UpdateDealershipCount );
        self.services.perms.response.subscribe( self.UpdateDealershipCount );
        self.services.perf.response.subscribe( self.UpdateDealershipCount );
        self.services.ga.response.subscribe( self.UpdateDealershipCount );
        self.services.ttm.response.subscribe( self.UpdateDealershipCount );

        // report visibility
        self.controls.analyticsSuiteToggle.subscribe( function( analyticsSuiteToggle ) {
            if( !analyticsSuiteToggle )
                self.virtualControls.activeReport( null );
            else
                self.ViewReportLink_main_click();
        });

    });
    
    // until the tabs themselves are bound to observables, this is the only way to determine
    // which report to show when the link is clicked, since there's only one link.
    self.ViewReportLink_main_click = function() {
        $('#chart_tabs > #chart_select > .ui-state-active > a').click();
    };

    // misc UI methods
    self.UpdateDealershipCount = function() {
        _.defer( function() {

            var denominator = "?";
            var perms = self.services.perms.response();
            if( perms )
                denominator = perms.UserBusinessUnits.length;

            var numerator = "?";
            var data;
            var err;
            if( !self.controls.analyticsSuiteToggle() ) {
                // Online Classified Overview
                err = self.errors.Overview();
                if( !err && perms )
                    numerator = perms.OnlineClassifiedOverviewBusinessUnits.length;
            } else {
                // Digital Performance Analytics
                if( $('#tab_perf').is(":visible")) {
                    // Online Classified Performance
                    data = self.data.processing.GroupSitePerformanceForMonth_CategoricalBusinessUnits().Permitted_DigitalPerformanceAnalytics;
                    if( data )
                        numerator = data.length;
                }
                else if( $('#tab_ga').is(":visible")) {
                    // Website Traffic
                    err = self.errors.GoogleAnalytics();
                    data = self.services.ga.response();
                    if( !err && data )
                        numerator = Math.max( 0, data.length - 1 ); // factor out TOTALS object
                }
                else if( $('#tab_ttm').is(":visible")) {
                    // Time to Market
                    data = self.services.ttm.response();
                    if( data && data.BusinessUnits )
                        numerator = data.BusinessUnits.length;
                }
            }

            if(( numerator == "?" && denominator == "?" ) || ( numerator === 0 ) || ( typeof numerator == "undefined" ) || ( _(numerator).isNaN() ))
                self.controls.dealershipCountDisplay( "&nbsp;" ); // no useful information
            else
                self.controls.dealershipCountDisplay( "Data for "+numerator+"/"+denominator+" dealerships" );
        });
    };

}; // MaxDashboardGroup
