$(function () {

    MaxDashboard.OnReady({
        businessUnitId: parseInt($("#BusinessUnitIdHidden").val(), 10),
        inventoryTypeDDL: $("#InventoryTypeDDL").val(),
        chartWrapper: $("#WebsitePerformance"),
        chartContainer: $(".chart-container"),
        isDebug: $("#jsIsDebug")[0] && $("#jsIsDebug").val() === "1",
        isGroup: true
    });
    // call GroupDB specific methods

    $(".freshPanel.alerts").on("click", function () {
        $(this).next(".report-link").click();
    });
    $("#chart_tabs").on("click", function (event) {
        // filter for edge case; FB# 24790, comment 3/1/2013 5:00 PM, list item 1: "Selecting checkboxes should not cause the Full Report to expand. - T.Muir"
        if( $(event.target).is('#lead_choices,#lead_choices *'))
            return;

        $(this).parents().next(".report-link").click();
    });

    $(".refresh .icon").on("click", function () {
        if($(this).hasClass("loading")) { return false; }
        MaxDashboard.RefreshCache();
        return false;
    });

    $("a[href='#tab_ga'], a[href='#tab_perf']").on("click", function (event) {
        var href = $(this).attr("href");
        if (href === "#tab_ga") {
            $("#view-WebsitePerformance").attr("data-link", "ga_sources_report");
        } else {
            $("#view-WebsitePerformance").attr("data-link", "WebsitePerformanceReport");
        }
    });

    $(".report-link").on("click", function (event) {

        var box = $(this).prev(".linkListBox"),
            id = $(this).attr("data-link"),
            report = $("#"+id);

        // ttm
        if( report.attr('id') != "ActionAlertsReport"
        &&  report.attr('id') != "ActivityAlertsReport"
        &&  $('#tab_ttm[aria-expanded=true]').length >= 1 )
            report = $('#ttm_report');

        if (report.is(":visible")) { return; }

        // hide
        $(".linkListBox").removeClass("active");
        $("#reports .report").hide();
        $(".report-link").each(function(idx, elem) {
            elem = $(elem);
            if(elem.data('shouldShow') !== false)
                elem.show();
        });
        $(this).hide();

        // show
        box.addClass("active");
        $("#reports").show();
        report.show();

        return false;
    });

    $("#reports .close").on("click", function () {
        $("#reports").hide();
        $(".linkListBox").removeClass("active");
        $(".report-link").each(function(idx, elem) {
            elem = $(elem);
            if(elem.data('shouldShow') !== false)
                elem.show();
        });
    });

    $("#WebsitePerformanceReport-reports").on("click", ".editable", function (event) {
        var edit,
            evt = $(event.target),
            isEdit = !$(this).find('.edit').is(":visible"),
            isSave = evt.hasClass("save"),
            isCancel = evt.hasClass("cancel"),
            original_editable_val = ($(this).find(".original").text()!=="") ? parseInt($(this).text(),10) : 0,
            newval;

        if(isEdit) {
            if(!$(this).find(".edit")[0]) {
                edit = '<span class="edit"><input type="text"><span class="save" title="save changes"></span><span class="cancel" title="cancel changes"></span></span>';
                $(this).append(edit);
            }
            $(this).find(".edit").show().find("input").val(original_editable_val)
                .end().end().find(".original").hide();
        }

        if(isSave) {
            newval = parseInt($(this).find("input").val().replace(",","").replace("$",""),10);
            if(_.isFinite(newval) && newval >= 0) {
                if(newval !== original_editable_val) {
                    // start loading animation / disable editing
                    var args = {};

                    args.business_unit_id = $(this).parents("tr").find(".business_unit_id").text(),
                    args.sitename = $(this).parents(".ui-tabs").find(".ui-tabs-nav .ui-tabs-active > a").text(),
                    args.date = new Date(),
                    args.newval = newval,
                    args.original_editable_val = original_editable_val;

                    MaxDashboard.UpdateBudget(args);

                }
                $(this).find(".edit").hide()
                    .end().find(".original").text(newval).show();
            }

        }

        if(isCancel) {
            $(this).find(".edit").hide()
                .end().find(".original").show();
        }

        return false;

    });

});