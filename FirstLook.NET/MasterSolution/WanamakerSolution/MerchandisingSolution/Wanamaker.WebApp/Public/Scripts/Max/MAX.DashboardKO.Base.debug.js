// assumes:
//   $ (jQuery+jQueryUI)
//   _ (Underscore)
//   Highcharts
//   mCustomScrollbar

// http://www.jshint.com/  validated regularly
/* global escape */ // supported in all major browsers
/* global $, jQuery, _, Highcharts, ko */
/* global Service, Chart, Report, ReportCollection, DynamicallyFilterableReport */
/* global _gaq */

// FB case# 25644
// written in the tumultuous summer of 2013 by Tyler W.R. Cole

///////////////////////////////////////////////////////////////////////////////
// DASHBOARD BASE

var MaxDashboardBase = function() {
    "use strict";
    var self = this;

    //// properties

    self.static_config = {
        verbose_logging: true,
        baseURL: "/merchandising",
        defaultDate: null,
        dateRange: [],
        dateRangeMonthCount: 13,
        vendor_definitions: null, // defined later
        listing_site_filter: [ "AutoTrader.com", "Cars.com" ],
        regrouped_listing_site_name: "Dealer Websites",
        DateTime_MinValue: new Date(-62135575200000),
        is_IE8: false,
        service_data_throttling: 0 // nonzero for IE8
    };
    self.dynamic_config = { // retrieved from page upon load, can be changed
        businessUnitId: ko.observable( null ),
        show_Overview: ko.observable(false), // whether the section toggle is active and allows user to see the Online Classified Overview
        show_TTM: ko.observable(true), // whether to show data for the TTM tab of Digital Performance Analytics
        hasDigitalShowroom: ko.observable( false ), 
        hasWebsite20: ko.observable( false ),
        hasShowroomReporting: ko.observable( false ),
        enableAnalyticsSuite: ko.observable( false ), // whether to show data for most of the charts in Digital Performance Analysis
        enableDebug: ko.observable( false ),
        merchandisingAlertSelectionBucket: ko.observable( null ),
        inventoryBaseUrl: ko.observable( null )  // base url for the inventory page
    }; 
    self.controls = { // dynamic via page controls 
        analyticsSuiteToggle: ko.observable( true ), // UI state of the analytics toggle; true means show Digital Performance Analysis
        vehicleTypeFilter: ko.observable( null ),  // TODO: change to markup+js only, with KO binding
        dateRangeFrom: ko.observable( null ),
        dateRangeTo: ko.observable( null ),
        enableLeadsEmail: ko.observable( true ),
        enableLeadsPhone: ko.observable( true ),
        enableLeadsChat: ko.observable( false )
    };
    self.virtualControls = { // variables that can be used like controls but are indirect or complex; defined later
        dateRangeSelectedMonthCount: null,
        budgetLink: null,
        selectedVehicleTypeFilterValue: null,
        showBeta: null
    };

    //// object trackers described

    // xhr objects and raw response data
    // reference service objects by:  {view_model}.services.{service_id}
    // e.g:  dashboard.services.data
    // instances are added to this tracker through MaxDashboardBase.RegisterService
    self.services = {};

    // Highcharts wrapper objects
    // reference chart objects by:  {view_model}.charts.{chart_id}
    // e.g:  dashboard.charts.Trends_ConsumerVDPImpressions
    // instances are added to this tracker through MaxDashboardBase.CreateChart
    self.charts = {};

    // reports store tabular data and handle sorting
    // reference report objects by:  {view_model}.report.{report_id}
    // e.g:  dashboard.reports.Trends_ConsumerVDPImpressions
    // instances are added to this tracker through MaxDashboardBase.CreateReport
    self.reports = {};

    // complex/computed observables that depend on other data
    // instances are added to this tracker directly.
    self.data = {};

    // computed error messages that depend on page data
    self.errors = {};

    // re-useable utility functions that would not be useful outside of dashboard stuff
    self.fn = {};

    // throttling
    self.data.throttle = {};
    self.data.throttle.enable_reports = ko.observable( false ); // indicates that live-recomputing is disabled by default; setting to true resumes normal behavior

    //// methods

    self.RegisterService = function( options ) {
        options.dashboard = self;
        options.enableDebug = self.dynamic_config.enableDebug;
        var service = new Service( options );
        self.services[options.id] = service;
    };

    self.CreateChart = function( options ) {
        options.dashboard = self;
        var CHART_CLASS = options.type || Chart;
        var chart = new CHART_CLASS( options );
        self.charts[options.id] = chart;
    };

    self.CreateReport = function( options ) {
        options.dashboard = self;
        options.notify_on_show = self.enable_reports;
        options.disable_rendering = true;
        var REPORT_CLASS = (options.tabbed)? ReportCollection : ((options.dynamically_filterable)? DynamicallyFilterableReport : Report);
        var report = new REPORT_CLASS( options );
        self.reports[options.id] = report;
    };

    //// initialization and activation methods

    self.StaticInitialization = function() {
        // basic date stuff
        var today = Date.today();
        self.static_config.defaultDate = (new Date( today.getFullYear(), today.getMonth() - 1 )).toString( "MM/yyyy" );
        self.static_config.styles = {};
        self.static_config.styles.YellowColumnGradient = {
            linearGradient: {
                x1: 0, y1: 0, x2: 1, y2: 0
            },
            stops: [
                [0.00, "rgb(255, 162, 42)"],
                [1.00, "rgb(225, 123, 26)"]
            ]
        };
        self.static_config.styles.BlueColumnGradient = {
            linearGradient: {
                x1: 0, y1: 0, x2: 1, y2: 0
            },
            stops: [
                [0.00, "rgb(0,  168, 238)"],
                [1.00, "rgb(15, 137, 187)"]
            ]
        };
        // vendor-specific static configs
        self.static_config.vendor_definitions = {};

        self.static_config.vendor_definitions["AutoTrader.com"] = {
            colors: {
                blend: { 
                    linearGradient: {
                        x1:0, y1:0,
                        x2:1, y2:0
                    },
                    stops: [
                        [0.00, "rgb(225, 123, 26)"],
                        [0.25, "rgb(255, 162, 42)"],
                        [0.75, "rgb(255, 162, 42)"],
                        [1.00, "rgb(225, 123, 26)"]
                    ]
                },
                hex: "#FFA22A"
            }
        };
        self.static_config.vendor_definitions["Cars.com"] = {
            colors: {
                blend: { 
                    linearGradient: {
                        x1:0, y1:0,
                        x2:1, y2:0
                    },
                    stops: [
                        [0.00, "rgb(65,  20, 140)"],
                        [0.25, "rgb(115, 64, 199)"],
                        [0.75, "rgb(115, 64, 199)"],
                        [1.00, "rgb(65,  20, 140)"]
                    ]
                },
                hex: "#7340C7"
            }
        };
        self.static_config.vendor_definitions["Dealer Website"] = {
            colors: {
                blend: { 
                    linearGradient: {
                        x1:0, y1:0,
                        x2:1, y2:0
                    },
                    stops: [
                        [0.00, "rgb(0,  168, 238)"],
                        [1.00, "rgb(15, 137, 187)"]
                    ]
                },
                hex: "#0F89BB"
            }
        };

        self.VendorDefinition = function( name ) {
            if( self.static_config.vendor_definitions[name] )
                return self.static_config.vendor_definitions[name];
            else
                return self.static_config.vendor_definitions["Dealer Website"];
        };
    
        // date range
        self.static_config.dateRange = function( month_count ) {
            var d = Date.today(),
                m = [],
                t = month_count;
            function fmt( d_obj ) {
                return {
                    val: d_obj.toString("MM/yyyy"),
                    str: d_obj.toString("MMM yyyy")
                };
            }
            // on the 5th of the month, include that month
            if( d.getDate() >= 5 ) {
                m.push( fmt( d ));
                --t; // one fewer month at the end
            }
            // backfill past months, up to month_count
            for( var i = 1; i <= t; i++ ) {
                var p = d.clone();
                p.addMonths( -i );
                m.push( fmt( p ));
            }
            return m;
        }( self.static_config.dateRangeMonthCount );

        var dr = cookie_get( "dashboard.date.from" ) || self.static_config.defaultDate; // persisted previous value
        self.controls.dateRangeFrom( dr );
        dr = cookie_get( "dashboard.date.to" ) || self.static_config.defaultDate;
        self.controls.dateRangeTo( dr );
        
        self.controls.dateRangeFrom.subscribe( function( from ) {
            cookie_set( "dashboard.date.from", from );
        });
        self.controls.dateRangeTo.subscribe( function( to ) {
            cookie_set( "dashboard.date.to", to );
        });

        var checkDateVals = function (newVal) {
            var newDate  = Date.parse(newVal);
            var fromDate = Date.parse(self.controls.dateRangeFrom());
            var toDate   = Date.parse(self.controls.dateRangeTo());
            
            var comparison   = Date.compare(newDate, fromDate);

            if (comparison !== 0) {
                // if the comparisons do not equal, the change must have been triggered from ToDate field
                if (comparison == -1) {
                    // new date is before FromDate
                    self.controls.dateRangeFrom(newVal);
                }
            }

            // compare the toDate
            comparison = Date.compare(newDate, toDate);
            if (comparison !== 0) {
                if (comparison == 1) {
                    // new date is after the ToDate
                    self.controls.dateRangeTo(newVal);
                }
            }
        };

        self.controls.dateRangeFrom.subscribe(function (from) {
            checkDateVals(from);
        });

        self.controls.dateRangeTo.subscribe(function (to) {
            checkDateVals(to);
        });
        
        self.virtualControls.dateRangeSelectedMonthCount = ko.computed( function() {
            var d_from_obj = Date.parse( self.controls.dateRangeFrom() );
            var d_to_obj = Date.parse( self.controls.dateRangeTo() );
            return ((d_to_obj.getFullYear() - d_from_obj.getFullYear())*12) + (d_to_obj.getMonth() - d_from_obj.getMonth()) + 1;
        });

        self.virtualControls.budgetLink = ko.computed( function() {
            var d_from_obj = Date.parse( self.controls.dateRangeFrom() );
            return self.static_config.baseURL + "/" +
                "CustomizeMAX/DealerPreferences.aspx?t=Reports&d=" +
                d_from_obj.toString("M/yyyy");
        });

        self.virtualControls.selectedVehicleTypeFilterValue = ko.computed( function() {
            var val = self.controls.vehicleTypeFilter();
            var option = $("select#InventoryTypeDDL option[value="+val+"]");
            var t = option.text();
            var m = t.match(/\((\d+)\)$/);
            return m ? parseInt(m[1],10) : undefined;
        });

        self.virtualControls.showBeta = ko.computed( function() {
            return self.controls.vehicleTypeFilter() != "Used" // New, Both
        });

        
        var email = cookie_get( "dashboard.leads.email" );
        if( typeof email == "string" )
            self.controls.enableLeadsEmail( email == "true" );
        
        var phone = cookie_get( "dashboard.leads.phone" );
        if( typeof phone == "string" )
            self.controls.enableLeadsPhone( phone == "true" );
        
        var chat = cookie_get( "dashboard.leads.chat" );
        if( typeof chat == "string" )
            self.controls.enableLeadsChat( chat == "true" );

        self.controls.enableLeadsEmail.subscribe( function( val ) {
            cookie_set( "dashboard.leads.email", val );
        });
        self.controls.enableLeadsPhone.subscribe( function( val ) {
            cookie_set( "dashboard.leads.phone", val );
        });
        self.controls.enableLeadsChat.subscribe( function( val ) {
            cookie_set( "dashboard.leads.chat", val );
        });

        self.dynamic_config.show_Overview.subscribe( function( show_Overview ) {
            if( !show_Overview && !self.controls.analyticsSuiteToggle() )
                self.controls.analyticsSuiteToggle( true ); // disabled overview, switch back to DPA view
        });

        self.static_config.is_IE8 = (navigator.appName == "Microsoft Internet Explorer" && navigator.appVersion.indexOf( "MSIE 8." ) != -1);
        if( self.static_config.is_IE8 )
            self.static_config.service_data_throttling = 5000;


        self.RegisterService({
            id: "set_vehicle_type",
            url: self.static_config.baseURL+"/"+"ChangeInventoryTypeFilter.aspx",
            args: [
                self.controls.vehicleTypeFilter
            ],
            auto_start: false
        });
    };

    self.ReadDynamicConfig = function() {
        self.dynamic_config.businessUnitId( parseInt( $("#BusinessUnitIdHidden").val(), 10) );
        self.dynamic_config.enableDebug("1" === ($("#jsIsDebug")[0] && $("#jsIsDebug").val()));
        self.controls.vehicleTypeFilter($("#InventoryTypeDDL").val());
    };
    
    self.LegacyDashboardCompatibility = function() {
        // TODO: remove this
        $("#Page").css("visibility", "visible");
        // tabs
        $("#InventoryType").insertBefore("#HeaderSearch");
        if( jQuery.ui ) {
            $(".tabs").tabs({
                selected: 0,
                select: function (event, ui) {
                    _gaq.push(["_trackEvent", "Dashboard", "Click", "Tab: " + ui.tab.innerText]);
                }
            });
        }
        // max for selling elite search box
        $("#SearchMaxSEForm").unbind("submit").bind("submit", function( e ){
            e.preventDefault();
            e.stopPropagation();
            var ajax_cfg = {};
            ajax_cfg.url = self.static_config.baseURL+"/"+"SearchMaxSE.aspx/"+($("#MaxSeTextbox").val());
            ajax_cfg.success = function( data ) {
                var messageText = data.StatusMessage.MessageText,
                    messageType = data.StatusMessage.MessageTypeText.toLowerCase();
                if( messageType == "error" ) {
                    $("#UserMessaging").remove();
                    $('<div id="UserMessaging"><p class="'+messageType+'">'+messageText+'</p></div>')
                        .appendTo("#SearchMaxSEForm .form");
                    $("#UserMessaging").delay(5000).fadeOut(666);
                } else {
                    window.open( data.MaxSEUrl );
                }
            };
            $("#SearchMaxSEForm .form").addClass("disabled");
            $("#SearchMaxSEForm .form input").attr("disabled", "true");
            ajax_cfg.complete = function() {
                $("#SearchMaxSEForm .form").removeClass("disabled");
                $("#SearchMaxSEForm .form input").attr("disabled", null);
            };
            $.ajax( ajax_cfg );
        });
    };

    self.PreBindingInitialize = function() {
        self.StaticInitialization();
        self.ReadDynamicConfig();
        self.LegacyDashboardCompatibility();
    };

    self.PostBindingInitialize = function() {
    };

    self.AllServicesExecute = function() {
        _.each( self.services, function( service ) {
            service.enabled( true );
            if( service.auto_start )
                service.fetch();
        });
    };

    // note that this is not an observable
    self.CountServicesLoading = function() {
        return _(self.services).reduce( function( tally, service ) {
            return service.loading() ? (tally + 1) : tally;
        }, 0 );
    };

    self.AllChartsInitialize = function() {
        _.each( self.charts, function( chart ) {
            chart.initialize_chart();
        });
    };
    self.RedrawCharts = function( chart_id_list ) {
        // forces an immediate redraw of any registered and valid chart objects given
        _(chart_id_list).each( function( chart_id ) {
            var Ch = self.charts[chart_id];
            if( Ch && Ch.chart )
                Ch.chart.redraw();
        });
    };

    self.Activate = function( root ) {
        self.PreBindingInitialize();
        if( self.dynamic_config.enableDebug() && self.static_config.verbose_logging )
            self.InitializeVerboseKnockoutJSLogging(); // outputs detailed observable evaluation timings to the console
        self.AllServicesExecute();
        self.AllChartsInitialize();
        ////
        if( !root )
            ko.applyBindings( self ); // binds to entire document
        else
            ko.applyBindings( self, root ); // applies only to the specified DOM element and descendants
        ////
        self.PostBindingInitialize();
        return self; // chaining
    };

    self.Destroy = function( root ) {
        if( !root )
            ko.cleanNode( document.body );
        else
            ko.cleanNode( root );
    };

    self.InitializeVerboseKnockoutJSLogging = function() {
        
        var page_t = (new Date()).getTime();
        var messages = [];
        subscribe_recursively( self.data, "dashboard.data" );
        function subscribe_recursively( root_obj, path_str ) {
            for( var key in root_obj ) {
                if( root_obj.hasOwnProperty( key )) {
                    var prop = root_obj[key];
                    var prop_path_str = path_str + "." + key;
                    if( ko.isObservable( prop )) {
                        subscribe( prop, prop_path_str );
                    } else if( typeof prop == "object" ) {
                        subscribe_recursively( prop, prop_path_str );
                    } else {
                        // nothing interesting
                    }
                }
            }
        }
        var currently_processing = {};
        function subscribe( observable, name ) {
            observable.subscribe( function() {
                var t = (new Date()).getTime();
                var page_d = t - page_t;
                currently_processing[name] = t;
                var log_statement = pad_left( ("+" + page_d), 8 ) + "  START              " + name + "    ";
                messages.push( log_statement );
                if( messages.length > 10000 )
                    messages.shift();
                // if( window.console ) 
                //     console.log( log_statement );
            }, null, "beforeChange" );
            observable.subscribe( function() {
                var t = (new Date()).getTime();
                var page_d = t - page_t;
                var ko_d = (t - currently_processing[name]);
                delete currently_processing[name];
                var log_statement = pad_left( ("+" + page_d), 8 ) + "    END  " + pad_left( ko_d, 7 ) + " ms  " + name + "    ";
                messages.push( log_statement );
                if( messages.length > 10000 )
                    messages.shift();
                // if( window.console ) 
                //     console.log( log_statement );
            }, null, "change" );
        }
        function pad_left( val, len )
        {
            var str = String( val );
            if( len > str.length )
                str = new Array( len + 1 - str.length ).join( " " ) + str;
            return str;
        }
        self.DumpVerboseLog = function() {
            var all_messages = messages.join("\n");
            if( window.console )
                console.log( all_messages );
            return all_messages; // even if there is no console, this should allow it to be viewed
        };
    };

    // UI methods

    self.Toggle_AnalyticsSuite = function() {
        if( self.dynamic_config.show_Overview() )
            self.controls.analyticsSuiteToggle( !self.controls.analyticsSuiteToggle() );
    };

    self.enable_reports = function() {
        _.delay( self.data.throttle.enable_reports, 25, true );
    };

    
    // lower-level dashboard static utility functions
    self.fn.format_GA_source_medium = function( source, maxlen ) {
        var separator = source.indexOf("|"),
            medium = source.substr(separator+1);
        source = source.substr(0, separator);
        if (medium === "organic")
            source += " " + medium;
        else if (_(["cpc","ppc"]).contains(medium))
            source += " (paid)";
        if( maxlen && source.length > maxlen )
            source = source.substr( 0, (maxlen-3) ) + "...";
        return source;
    };

    self.fn.parseJSONPDate_millisecs = function( dstr ) {
        return parseInt(dstr.substr( 6, dstr.length-6-2 ), 10);
    };
    self.fn.parseJSONPDate = function( dstr ) {
        return new Date(self.fn.parseJSONPDate_millisecs( dstr ));
    };

    self.fn.radical_string_shortening = function( str, target_len ) {
        if( str === null || typeof str === "undefined" )
            return "";
        else if( typeof str != "string" )
            return (new String( str )); // not sure if removing vowels in this case is wise
        else if( str.length <= target_len )
            return str;
        var str_1 = str.replace(/\B(?:[aouieAOUIE]|y(?![aouieAOUIE]))\B/g,''); // remove vowels
        if( str_1.length <= target_len )
            return str_1;
        var str_2 = str_1.substr( 0, target_len - 2 ) + "..";
        return str_2;
    };

    // replaces certain keywords with current binding values
    self.fn.message_template = function( str ) {
        str = str.replace( "{{BUDGET_LINK}}", self.virtualControls.budgetLink() );
        return str;
    };

    self.fn.empty_chart_data = function( chart_data ) {
        return !chart_data || !chart_data.series || !chart_data.series[0] || !chart_data.series[0].data || !chart_data.series[0].data.length;
    }
    
}; // MaxDashboardBase

// class level statics

//// simple message templates
MaxDashboardBase.error_html = {};
MaxDashboardBase.error_html.no_data = '<span class="no_data_message">No data available</span>';
MaxDashboardBase.error_html.no_budget = '<span class="no_budget_message">Please set your <a href="{{BUDGET_LINK}}">monthly cost</a> to see this data.</span>';


///////////////////////////////////////////////////////////////////////////////

// knockout system settings
ko.setTemplateEngine( new ko.nativeTemplateEngine() ); // http://stackoverflow.com/questions/8294515

// knockout plugin settings
if( "deferUpdates" in ko.computed )
    ko.computed.deferUpdates = true; // https://github.com/mbest/knockout-deferred-updates

// this observable will return the last-computed value if it is disabled, without being dependent on it
ko.conditionallyComputed = function( value_fn, enabled_fn, deferredEvaluation, owner ) {
    if( typeof value_fn == "object" ) { // ko.computed initializer object
        deferredEvaluation = value_fn.deferredEvaluation;
        owner = value_fn.owner;
        // write = value_vn.write;
        value_fn = value_fn.read;
    }
    var cached_value = null;
    return ko.computed({
        read: function() {
            if( enabled_fn() ) // check whether live-computing is enabled
                cached_value = value_fn(); // update cache
            return cached_value; // return value
        },
        deferredEvaluation: (typeof deferredEvaluation == "undefined" ? true : deferredEvaluation),
        owner: (typeof owner == "undefined" ? undefined : owner)
    });
};


// underscore mixins
_.mixin({
    // similar to _.map
    objMap: function( input, mapper, context ) {
        return _.reduce( input, function (obj, v, k) {
            obj[k] = mapper.call( context, v, k, input );
            return obj;
        }, {}, context);
    },
    // similar to _.filter
    objFilter: function( input, test, context ) {
        return _.reduce( input, function (obj, v, k) {
            if( test.call( context, v, k, input )) {
                obj[k] = v;
            }
            return obj;
        }, {}, context);
    },
    // similar to _.reject
    objReject: function( input, test, context ) {
        return _.reduce( input, function (obj, v, k) {
            if( !test.call( context, v, k, input )) {
                obj[k] = v;
            }
            return obj;
        }, {}, context);
    }
});        

// highcharts defaults
Highcharts.setOptions({
    chart: {
        style: {
            fontFamily: "Arial, Helvetica, sans-serif"
        }
    }
});

///////////////////////////////////////////////////////////////////////////////
// low-level functions

// simple cookie API
function cookie_get( name ) {
  if( !document.cookie || document.cookie === "" )
    return null;
  var cookie = {};
  _.each( document.cookie.split("; "), function( item ) {
    var val = item.split( "=" );
    cookie[val[0]] = val[1];
  });
  return cookie[name];
}
function cookie_set( name, value, life_days ) {
  var t = new Date();
  t.setDate( t.getDate() + life_days );
  document.cookie = name + "=" + escape(value) + (life_days ? "; expires="+t.toUTCString() : "" );
}

// via phpJS
Number.prototype.toFixxed = function (prec) {
    var k = Math.pow(10, prec);
    return '' + Math.round(this * k) / k;
};
function number_format (number, decimals, dec_point, thousands_sep) {
    number = (number + '').replace(/[^0-9+-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '';
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? n.toFixxed(prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}
function zeroPadTens(value) {
    if (value < 10) return "0" + value;
    return value + "";
}
function msToTime(s) {
    var ms = s % 1000;
    s = (s - ms) / 1000;
    var secs = s % 60;
    s = (s - secs) / 60;
    var mins = s % 60;
    var hrs = (s - mins) / 60;
    if (ms > 500) secs++;  // round ms up

    var value = ":" + zeroPadTens(secs);

    if (hrs > 0) { // prefix value with hours, hidden otherwise
        value = hrs + ":" + zeroPadTens(mins) + value;
    } else {
        value = mins + value;
    }

    return value;
}
