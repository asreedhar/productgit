// assumes:
//   MaxDashboardBase

// http://www.jshint.com/  validated regularly
/* global ko, _, Highcharts, number_format */

///////////////////////////////////////////////////////////////////////////////
// HELPER CLASSES

// wrapper to store chart data and control Highcharts API
var Chart = function( options ) {
    "use strict";
    var self = this;

    // properties
    self.id = options.id;
    self.dashboard = options.dashboard;
    self.loading = ko.observable( true );
    self.container = $(options.container_selector);
    self.chart = null;
    self.data = options.data_observable;
    if (options.ignoreQunit) self.ignoreQunit = options.ignoreQunit;

    self.settings = $.extend( true, {
        chart: {
            backgroundColor: null, // transparent
            type: "column"
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    style: {
                        color: "#646c79"
                    }
                },
                shadow: true
            },
            column: {
                pointPadding: 0,
                groupPadding: 0.2,
                borderWidth: 0,
                dataLabels: {
                    crop: false,
                    overflow: "none"
                }
            },
            bar: {
                pointPadding: 0,
                groupPadding: 0.2,
                borderWidth: 0,
                dataLabels: {
                    crop: false,
                    overflow: "none"
                }
            }
        },
        xAxis: {
            labels: {
                style: {
                    fontSize: "9px"
                }
            },
            tickLength: 0
        },
        yAxis: {
            labels: {
                enabled: false
            },
            stackLabels: {
                crop: false
            },
            gridLineWidth: 0,
            title: {
                text: null
            }
        },
        title: {
            style: {
                color: "#333333",
                fontSize: "12px",
                fontWeight: "bold"
            },
            margin: 30
        },
        tooltip: {
            enabled: false
        },
        subtitle: {
            style: {
                color: "#333333",
                fontSize: "10px",
                fontWeight: "bold"
            }
        },
        legend: {
            enabled: false
        },
        credits: {
            enabled: false
        }
    }, options.highcharts_settings );

    // infrastructure settings
    self.reuseChartObject = true; // if false, creates an entirely new chart object each time
    self.use_setData = true; // calls either Series.setData() or Series.update();  http://api.highcharts.com/highcharts#Series

    // must be called explicitly before chart will be allowed to display
    self.initialize_chart = function() {
        if( self.container.length === 0 )
            return;
        // it only makes sense to pre-initialize if chart object will be reused
        if( self.reuseChartObject ) {
            // layout - initialize highcharts
            var settings = $.extend( true, {}, self.settings, { chart: { renderTo: self.container[0] } });
            self.chart = new Highcharts.Chart( settings );
        }
        // data - establish data dependency for auto-update
        if( self.data && ('subscribe' in self.data) ) { // valid observable
            self.data.extend({ throttle:25 }).subscribe( function( data ) {
                if( self.reuseChartObject && !self.chart )
                    return; // chart not initialized, abort
                if( !data )
                    self.loading( true );
                else {
                    self.update_chart_data( data );
                    if( self.update_extensions )
                        self.update_extensions();
                    self.loading( false );
                }
            });
        }
    };

    self.update_chart_data = function( data ) {
        if( !self.reuseChartObject ) {
            // if chart is not reused, chart must be initialized with all layout settings & data at the same time
            var settings = $.extend( true, {}, self.settings, data, { chart: { renderTo: self.container[0] } });
            self.chart = new Highcharts.Chart( settings );
        } else {
            // normal methods
            var chart = self.chart;
            if( !chart )
                return;
            // add or replace series with new point data
            _(data.series).each( function( s ) {
                if( !("name" in s) )
                    s.name = "Series 1"; // required
                var e = _(chart.series).find( function(c) {
                    return c.name == s.name;
                });
                if( e ) {
                    if( self.use_setData )
                        e.setData( s.data, false );
                    else // use update
                        e.update( s, false );
                } else {
                    chart.addSeries( s, false, false );
                }
            });
            // delete series that are no longer present
            var to_remove = [];
            _(chart.series).each(function(c) {
                var o = _(data.series).find( function(s) {
                    return c.name == s.name;
                });
                if( !o )
                    to_remove.push( c );
            });
            _(to_remove).each(function(c) {
                c.remove();
            });
            try {
                chart.redraw();
            } catch(ex) {}
        }
    };
    
    self.update_extensions = null;
};

var SimpleColumnChart = function( options ) {
    var self = this;
    
    // inheritance
    Chart.call( self, $.extend( {}, options, { highcharts_settings: null }));

    // apply this derived class' settings defaults, then the passed-in settings last
    self.settings = $.extend( true, self.settings, {
        chart: {
            type: "column",
            marginBottom: null,
            marginLeft: 10,
            marginRight: 24,
            marginTop: 75
        },
        plotOptions: {
            series: {
                dataLabels: {
                    crop: false,
                    enabled: true,
                    y: -6,
                    style: {
                        fontSize: 12,
                        fontWeight: "bold"
                    },
                    formatter: function() {
                        return number_format( this.y, 0 ); // round to nearest whole number
                    }
                }
            }
        },
        xAxis: {
            type: "category"
        }
    }, options.highcharts_settings );
};

var SquatColumnChart = function( options ) {
    var self = this;
    
    // inheritance
    SimpleColumnChart.call( self, $.extend( {}, options, { highcharts_settings: null }));

    // apply this derived class' settings defaults, then the passed-in settings last
    self.settings = $.extend( true, self.settings, {
        chart: {
            marginBottom: null,
            marginLeft: null,
            marginRight:null,
            marginTop: null
        },
        title: {
            style: {
                fontWeight: "normal"
            }
        },
        plotOptions: {
            series: {
                dataLabels: {
                    style: {
                        fontSize: 17
                    }
                }
            }
        }
    }, options.highcharts_settings );
};

var BarChartWithBenchmarks = function( options ) {
    var self = this;
    
    // inheritance
    SquatColumnChart.call( self, $.extend( {}, options, { highcharts_settings: null }));

    // apply this derived class' settings defaults, then the passed-in settings last
    self.settings = $.extend( true, self.settings, {
        chart: {
            type: "bar"
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: false,
                },
                shadow: true,
                stacking: "normal"
            }
        },
        yAxis: {
            stackLabels: {
                align: "left",
                crop: false,
                enabled: true,
                style: {
                    fontFamily: "Arial, sans-serif",
                    fontSize: 17,
                    fontWeight: "bold"
                },
                textAlign: "right",
                y: -2,
                x: -10,
                formatter: function() {
                    return this.total ? ("$" + number_format( this.total, 2 )) : "";
                }
            }
        },
        xAxis: {
            labels: {
                y: 11
            }
        }
    }, options.highcharts_settings );

    self.use_setData = false; // calls either Series.setData() or Series.update();  http://api.highcharts.com/highcharts#Series

    if( options.extensions && ('benchmarks_observable' in options.extensions) )
        self.benchmarks = options.extensions.benchmarks_observable;

    //// private methods

    self.update_extensions = function() {
        if( !self.benchmarks )
            return;
        var data = self.data();
        var benchmarks = self.benchmarks();
        if( !data || !benchmarks )
            return;
        // clear out old benchmark labels
        $('.highcharts-benchmark', self.container).remove();
        var grp = self.chart.renderer.g("benchmark").attr({ zIndex: 8 }).add();
        var p = 2; // label text padding
        var m = 4; // label text upper margin
        var max = 0;
        // ensure chart extremes will include the benchmark data + 10%
        _(data.series[0].data).each( function( data_point, idx ) {
            var b = benchmarks[data_point.name];
            /// visual range
            if( data_point.y > max )
                max = data_point.y;
            if( b > max )
                max = b;
        });
        if( max > 0 )
            self.chart.yAxis[0].setExtremes( null, max );
        _(data.series[0].data).each( function( data_point, idx ) {
            // do not add benchmark visualization if data value is 0 (missing budget message)
            if( data_point.y === 0 )
                return; // skip rest of iteration
            var b = benchmarks[data_point.name];
            var w = self.chart.series[0].data[idx].pointWidth;
            var px = Math.ceil( self.chart.yAxis[0].toPixels(b) ) - 0.5;
            var py0 = Math.ceil( self.chart.xAxis[0].toPixels(idx) - w/2 ) - 2;
            var py1 = Math.ceil( self.chart.xAxis[0].toPixels(idx) + w/2 ) - m - 1;
            var x = self.chart.yAxis[0].toPixels(0)+p;
            var y = self.chart.xAxis[0].toPixels(idx)+w/2+p+m;
            var str = "TOP PERF AVG $" + number_format( b, 2 );
            var ruler = $('<span id="ruler" style="text-transform:uppercase;font-family:Arial,sans-serif;font-size:9px;font-weight:bold;visibility:hidden;">'+str+'</span>').appendTo('body');
            var text_width = ruler.width();
            var text_height = ruler.height();
            ruler.remove();
            var r = { x:x-2*p+0.5, y:Math.floor(y-p-m-(text_height/2))+0.5, w:text_width+4*p, h:text_height+2*p };
            var delta = 0;
            if( r.x+r.w <= px )
                delta = (px - (r.x+r.w)) + 1;
            r.x += delta;
            var text = self.chart.renderer.text( str, x+delta, y ).attr({ align: "left", zIndex: 10 }).css({ textTransform: "uppercase", fontFamily: "Arial,sans-serif", fontSize: "9px", fontWeight: "bold", color: "#666" });
            text.add( grp );
            var rect = self.chart.renderer.path([ "M",r.x,r.y, "L",r.x+r.w,r.y, "L",r.x+r.w,r.y+r.h, "L",r.x,r.y+r.h, "Z" ]).attr({ fill: "#fff", "stroke-width": 1, stroke: "#666", zIndex: 8 });
            rect.add( grp );
            self.chart.renderer.path([ "M",px-1,py0, "L",px-1,py1 ]).attr({ "stroke-width": 1, stroke: "#666", zIndex: 7 }).add( grp );
            self.chart.renderer.path([ "M",px+1,py0, "L",px+1,py1 ]).attr({ "stroke-width": 1, stroke: "#666", zIndex: 7 }).add( grp );
            self.chart.renderer.path([ "M",px,  py0, "L",px,  py1+1.5 ]).attr({ "stroke-width": 1, stroke: "#fff", zIndex: 9 }).add( grp );
            self.chart.renderer.path([ "M",px-1,py0+0.5, "L",px+1,py0+0.5 ]).attr({ "stroke-width": 1, stroke: "#666", zIndex: 9  }).add( grp );
        });
    };
};

var TrendsStyleLineChart = function( options ) {
    var self = this;
    Chart.call( self, $.extend( {}, options, { highcharts_settings: null }));
    self.settings = $.extend( true, self.settings, {
        title: null,
        chart: {
            type: "line",
            spacingBottom: 15,
            spacingRight: 20
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: false,
                }
            },
        },
        xAxis: {
            type: "datetime",
            dateTimeLabelFormats: {
                day: "%b %Y",
                week: "%b %Y",
                month: "%b %Y"
            },
            minTickInterval: 27*(24*60*60*1000), // prevents months from showing up multiple times
            tickLength: 4,
            tickColor: "#000000",
            labels: {
                style: {
                    color: "#000000",
                    fontSize: "0.75em"
                },
                y: 15
            },
            minorGridLineWidth: 0,
            minorTickWidth: 1,
            minorTickInterval: "auto",
            minorTickColor: "#A0A0A0",
            minorTickLength: 2
        },
        yAxis: {
            gridLineWidth: 1,
            min: 0,
            title: {
                text: null
            },
            labels: {
                enabled: true
            },
            minorTickInterval: "auto",
            minorTickColor: "#A0A0A0",
            minorTickPosition: "inside"
        },
        tooltip: {
            enabled: true,
            formatter: function() {
                return '<span style="font-size:0.8em">'+(new Date(this.x)).toString('MMMM yyyy')+'</span><br>'+
                    '<span style="color:'+this.series.color+'">'+this.series.name+'</span>: <b>'+this.y.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")+'</b><br/>';
            }
        },
        legend: {
            enabled: true,
            verticalAlign: "bottom",
            symbolWidth: 7,
            borderWidth: 0,
            y: 5
        }
    }, options.highcharts_settings );
}

var TrendsStyleLineChartDetailed = function( options ) {
    var self = this;
    Chart.call( self, $.extend( {}, options, { highcharts_settings: null }));
    self.settings = $.extend( true, self.settings, {
        title: null,
        chart: {
            type: "line",
            spacingBottom: 15,
            spacingRight: 20
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: false,
                }
            },
        },
        xAxis: {
            type: "datetime",
            dateTimeLabelFormats: {
                day: "%m/%e/%y",
                week: "%m/%e/%y"
            },
            style: {
                fontSize: "11px"
            },
            minorTickInterval: 24 * 3600 * 1000,
            minTickInterval: 24 * 3600 * 1000,
            tickPixelInterval: 110,
            minorTickWidth: 1,
            minorGridLineWidth: 0
        },
        yAxis: {
            gridLineWidth: 1,
            min: 0,
            title: {
                text: null
            },
            labels: {
                enabled: true
            },
            minorTickInterval: "auto",
            minorTickColor: "#A0A0A0",
            minorTickPosition: "inside"
        },
        tooltip: {
            enabled: true,
            formatter: function() {
                return '<span style="font-size:0.8em">'+(new Date(this.x)).toString('MMMM yyyy')+'</span><br>'+
                    '<span style="color:'+this.series.color+'">'+this.series.name+'</span>: <b>'+this.y.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")+'</b><br/>';
            }
        },
        legend: {
            enabled: true,
            verticalAlign: "bottom",
            symbolWidth: 7,
            borderWidth: 0,
            y: 5
        }
    }, options.highcharts_settings );
}

var LineChartWithBudgetChanges = function( options ) {
    var self = this;

    // inheritance
    TrendsStyleLineChart.call( self, $.extend( {}, options, { highcharts_settings: null })); // inherit default settings only at this step

    // clear your mind of questions; you must un-learn what you have learned.
    self.settings = $.extend( true, self.settings, {
        chart: {
            events: {
                redraw: budget_change_realign
            }
        }
    }, options.highcharts_settings );

    // extensions

    if( options.extensions ) {
        self.budget_changes = options.extensions.budget_changes_observable;

        self.update_extensions = function() {
            initialize_budget_change_icons( self.budget_changes() );
        };

        if( options.extensions.currency_mode ) {
            self.settings = $.extend( true, self.settings, {
                yAxis: {
                    labels: {
                        formatter: function() {
                            return "$" + number_format( this.value, 2 );
                        }
                    }
                },
                tooltip: {
                    formatter: function() {
                        return '<span style="font-size:0.8em">'+(new Date(this.x)).toString('MMMM yyyy')+'</span><br>'+
                            '<span style="color:'+this.series.color+'">'+this.series.name+'</span>: $<b>'+parseFloat(this.y).toFixed(2)+'</b><br/>';
                    }
                }
            });
        }
    }

    //// private methods

    self.__plotLine_ids = [];

    function initialize_budget_change_icons( budget_changes ) {
        if( !budget_changes )
            return;
        var chart = self.chart;
        // remove existing plot lines, icons and popups
        _(self.__plotLine_ids).each( function( plotLine_id ) {
            chart.xAxis[0].removePlotLine( plotLine_id );
        });
        self.container.find("image[width=17][height=17]").remove();
        var clazz = "chart_pseudotip__"+self.id;
        $("#trends_pseudotips .hc_pseudotip_anchor."+clazz).remove();
        // create budget-change plot lines and icons
        var plotLines = _.map( budget_changes, budget_change_plotlines );
        _(plotLines).each( function( l ) {
            var e = _(chart.xAxis[0].plotLinesAndBands).find( function( p ) {
                return p.options.value == l.value;
            });
            if( !e ) // no plot-line exists with this x-value
                self.__plotLine_ids.push( chart.xAxis[0].addPlotLine( l ));
        });
        budget_change_icon_init( chart );
        // adjust any existing icons that need to be shifted
        budget_change_realign.call( chart );
    }
    
    function budget_change_popup_html_func( text, change_obj ) {
        return text +
          (text === ''? ('<span style="font-size:0.8em">'+(new Date(change_obj.time_value)).toString('MMMM yyyy')+'</span>'):'') +
          '<br>' +
          '<span style="color:'+self.dashboard.VendorDefinition(change_obj.site_name).colors.hex+'">'+change_obj.site_name+':</span>&nbsp;' +
          '<span class="price_arrow '+(change_obj.delta >= 0? 'positive':'negative')+'"></span>&nbsp;' +
          '$<strong>'+Math.abs(change_obj.delta)+'</strong>';
    }

    function budget_change_plotlines( all_changes_at_time, time_value ) {
        return {
            value: parseInt(time_value, 10),
            color: "#BFBFC1",
            width: 2,
            dashStyle: "Dash",
            label: {
                verticalAlign: "top",
                rotation: 0,
                text: "",
                hoverHTML: _.reduce( all_changes_at_time, budget_change_popup_html_func, "")
            }
        };
    }                

    function budget_change_icon_init( chart ) {
        // budget-delta "$" image: url, position and mouse event handlers
        var img_src = "Themes/MaxMvc/Images/Dashboard/$.png";
        var pseudotip_anchor_container_selector = "#trends_pseudotips";
        var clazz = "chart_pseudotip__"+self.id;
        var r = chart.renderer;
        var p = chart.xAxis[0].plotLinesAndBands;
        var img_mouseover_fn = function() {
          $(this).data("tooltip_anchor").show();
        };
        var img_mouseout_fn = function() {
          $(this).data("tooltip_anchor").fadeOut();
        };
        for( var i=0; i < p.length; ++i ) {
            var line = p[i];
            var label = p[i].label;
            if (!p[i].label)
                return;
            var hc_img = r.image( img_src, p[i].label.x-13,p[i].label.y-17, 17,17 ).attr({ zIndex: 10 }).add();
            $(hc_img.element)
                .data("positional_reference", 
                    $(p[i].label))
                .data("tooltip_anchor", 
                    $('<div class="hc_pseudotip_anchor '+clazz+'" style="display:none;top:'+(p[i].label.y-17-8)+'px;left:'+(p[i].label.x-13+21)+'px">'+
                        '<div class="hc_pseudotip_content">'+p[i].options.label.hoverHTML+'</div>'+
                    '</div>').appendTo($(pseudotip_anchor_container_selector))
                );
            $(hc_img.element)
                .on("mouseover", img_mouseover_fn )
                .on("mouseout", img_mouseout_fn );
            if( !chart.budget_change_icons )
                chart.budget_change_icons = [];
            chart.budget_change_icons.push( hc_img );
        }
    }

    function budget_change_realign() {
        var chart = this;
        if( !chart.budget_change_icons )
            return;
        setTimeout( function() {
            _.each( chart.budget_change_icons, function( hc_img ) {
                var ref = $(hc_img.element).data("positional_reference");
                if( !ref )
                    return; // wtf
                var new_position = {
                    x: ref.attr("x")-13,
                    y: ref.attr("y")-17
                };
                $(hc_img.element).attr( new_position );
                var hover = $(hc_img.element).data("tooltip_anchor");
                $(hover).css({
                    left: ref.attr("x")-13+21,
                    top:  ref.attr("y")-17-8
                });
                // hide/show based on visibility of reference element (vertical dotted line)
                $(hc_img.element).css({ visibility: ref.attr("visibility") });
            });
        }, 450 ); // roughly the amount of time that highcharts uses to animate
    }
};

var LineChartWithInitiallyHiddenSeries = function( options ) {
    var self = this;
    
    // inheritance
    TrendsStyleLineChart.call( self, $.extend( {}, options, { highcharts_settings: null })); // inherit default settings only at this step
    self.settings = $.extend( true, self.settings, {}, options.highcharts_settings );

    // extensions
    if( options.extensions ) {
        var show_only_top = options.extensions.show_only_top;
        self.update_extensions = function() {
            var data = self.data();
            if( !data || typeof show_only_top != "number" )
                return;
            // any time the data changes, check which series should be hidden, and hide them
            var sorted_series = _(self.chart.series).sortBy( function( series ) {
                return -1 * series.yAxis.getExtremes().dataMax;
            });
            for( var i = show_only_top; i < sorted_series.length; ++i ) {
                sorted_series[i].hide();
            }
        };
    }
}

var HangingLabelColumnChart = function( options ) {
    var self = this;
    
    Chart.call( self, $.extend( {}, options, { highcharts_settings: null }));
    
    self.settings = $.extend( true, self.settings, {
        chart: {
            spacingLeft: 20,
            spacingTop: 20
        },
        title: {
            text: null
        },
        plotOptions: {
            series: {
                dataLabels: {
                    crop: false,
                    style: {
                        fontSize: 17,
                        fontWeight: "bold"
                    },
                    x: 0,
                    y: 0,
                    formatter: function() {
                        var value_str = number_format( this.y, (this.y<1?1:0) );
                        if( value_str == "1.0" ) // special case
                            value_str = "1";
                        return value_str+"%";
                    }
                }
            }
        },
        tooltip: {
            enabled: true,
            useHTML: true
        },
        xAxis: {
            type: "category",
            labels: {
                rotation: -45,
                align: "right",
                style: {
                    textTransform: "uppercase"
                }
            },
            tickLength: 0
        },
        yAxis: {
            allowDecimals: true,
            gridLineColor: "#ccc",
            gridLineWidth: 1,
            labels: {
                enabled: true,
                formatter: function() {
                    return number_format( this.value, 0 )+"%";
                }
            },
            tickPixelInterval: 50,
            lineWidth: 1,
            stackLabels: {
                crop: false
            },
            minTickInterval: 1
        }
    }, options.highcharts_settings );
    
    self.update_extensions = function() {
        var yAxis = self.chart.series[0].yAxis;
        // actual space available, in axis-space units
        var availableMargin = yAxis.max - yAxis.dataMax;
        // height of text as reported by browser using current styles, plus maximum observed separation, translated to axis space
        var requiredMargin = ((19 + 1) / yAxis.translationSlope); 
        // option to shrink bars and make room for labels
        if( availableMargin < requiredMargin ) {
            yAxis.setExtremes( yAxis.min, (yAxis.max + (requiredMargin - availableMargin)), true, true );
        }
    }
};

var HangingLabelColumnChartWithValueIndicator = function( options ) {
    var self = this;
    
    HangingLabelColumnChart.call( self, $.extend( {}, options, { highcharts_settings: null }));
    
    self.settings = $.extend( true, self.settings, {
        chart: {
            marginLeft: 83,
            marginRight: 58
        },
        plotOptions: {
            series: {
                dataLabels: {
                    formatter: function() {
                        return number_format( this.y, 1 );
                    }
                }
            }
        },
        tooltip: {
            formatter: function() {
                return '<span style="color:#333">'+this.point.name+'</span><br>'+
                    '<span style="font-size:1.1em;font-weight:bold;color:'+this.series.color+'">'+number_format(this.point.y,1)+'</span><span style="font-size:0.9em;color:#333"> days</span>';
            },
            useHTML: true
        },
        xAxis: {
            labels: {
                useHTML: false
            }
        },
        yAxis: {
            labels: {
                formatter: function() {
                    return number_format( this.value, 0 );
                }
            }
        }
    }, options.highcharts_settings );

    self.indicated_value = options.extensions.value_observable;

    self.plotLines = [];
    self.update_extensions = _(self.update_extensions).wrap( function( base_fn ) {
        // update indicator values & positions
        var chart = self.chart;
        var value = ((self.indicated_value) ? self.indicated_value() : null);
        var img_src = options.extensions.value_label_img_src;
        if( !chart || !img_src )
            return;
        // remove old indicator assets
        while( self.plotLines.length > 0 )
            chart.yAxis[0].removePlotLine( self.plotLines.pop() );
        self.container.find(".highcharts-extensions").remove();
        chart.yAxis[0].setExtremes( null, Math.max( value, chart.yAxis[0].getExtremes().dataMax ));
        if( !value )
            return; // do not show zero on the indicator
        // add indicator assets (deferred)
        setTimeout( function() {
            var g = chart.renderer.g("extensions").add();
            self.plotLines.push( chart.yAxis[0].addPlotLine({
                value: value,
                color: "#BFBFC1",
                width: 2,
                label: { text: "" }
            }));
            var Y = chart.yAxis[0].toPixels( value );
            chart.renderer.image( img_src, 426,Y-11, 50,22 ).add( g );
            //var text = number_format( value, 1 ) + " →";
            var text = number_format( value, 1 ) + " \u2192";
            var color = "#808080";
            if( chart.series[0].color && chart.series[0].color.stops && chart.series[0].color.stops[1] )
                color = chart.series[0].color.stops[1][1];
            chart.renderer.text( text, 13, Y+4 ).css({ fontSize:'14px',color:color,fontWeight:'bold' }).add( g );
            chart.renderer.text( "days", 13, Y+15 ).css({ fontSize:'9px',color:"#808080" }).add( g );
        }, 550 ); // approx potential animation time (from setExtremes and data updates)

        // call base function, which has the option to increase y-axis scale maximum in order to make room for column labels
        base_fn();
    });
};

var PieChart = function( options ) {
    var self = this;
    
    Chart.call( self, $.extend( {}, options, { highcharts_settings: null }));
    
    self.settings = $.extend( true, self.settings, {
        chart: {
            type: "pie"
        },
        plotOptions: {
            pie: {
                tooltip: {
                    followPointer: false
                }
            },
            series: {
                dataLabels: {
                    enabled: true,
                    style: {
                        textTransform: "uppercase"
                    },
                    useHTML: true,
                    formatter: function () {
                        return "<div style='text-align: center;'><span style='text-transform: uppercase;'>"+this.point.name+"</span><br />"+
                            "<span style='font-size: 16px;'>"+number_format( this.y, ((this.y<1)?1:0) )+"%</span></div>";
                    }
                },
                shadow: true
            }
        },
        title: {
            text: null
        },
        tooltip: {
            enabled: true,
            followPointer: false,
            useHTML: false,
            positioner: function (w, h, point) {
                return {
                    x: point.plotX-50,
                    y: point.plotY
                };
            }
        },
        xAxis: {
            type: "category",
            labels: {
                style: {
                    fontSize: "9px"
                }
            },
            tickLength: 0
        },
        yAxis: {
            gridLineWidth: 0,
            labels: {
                enabled: false
            },
            stackLabels: {
                crop: false,
            },
            title: {
                text: null
            }
        }
    }, options.highcharts_settings );

    // infrastructure settings
    self.use_setData = false; // workaround for https://github.com/highslide-software/highcharts.com/issues/2239
};

var SimpleLineChart = function( options ) {
    var self = this;

    Chart.call( self, $.extend( {}, options, { highcharts_settings: null }));

    self.settings = $.extend( true, self.settings, {
        chart: {
            type: "line"
        },
        legend: {
            enabled: true,
            borderWidth: 0,
            verticalAlign: "top",
            y: 30
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: false
                },
                marker: {
                    enabled: false
                }
            }
        },
        title: {
            margin: 60,
            style: {
                textTransform: "uppercase"
            }
        },
        tooltip: {
            enabled: false
        },
        xAxis: {
            type: "datetime",
            dateTimeLabelFormats: {
                day: "%m/%e/%y",
                week: "%m/%e/%y"
            },
            style: {
                fontSize: "11px"
            },
            minorTickInterval: 24 * 3600 * 1000,
            minTickInterval: 24 * 3600 * 1000,
            tickPixelInterval: 110,
            minorTickWidth: 1,
            minorGridLineWidth: 0
        },
        yAxis: {
            gridLineWidth: 1,
            labels: {
                enabled: true
            },
            title: {
                text: null
            }
        }
    }, options.highcharts_settings );
};

var LineChartDynamicLegend = function( options ) {
    var self = this;

    SimpleLineChart.call( self, $.extend( {}, options, { highcharts_settings: null }));

    self.settings = $.extend( true, self.settings, {
        tooltip: {
            enabled: true,
            useHTML: true,
            formatter: function() {
                return '<span style="font-size:0.8em">'+(new Date(this.x)).toString("dddd, MMM d, yyyy")+'</span><br>'+
                    '<span style="color:'+this.series.color+'">'+this.series.name.substr(0,this.series.name.indexOf(":"))+'</span>: <span style="font-weight:bold">'+this.y+'</span><br/>';
            }
        }
    }, options.highcharts_settings );

    self.reuseChartObject = false;
};

var LineChartWithInitiallyHiddenSeriesDetailed = function(options) {
    var self = this;

    // inheritance
    TrendsStyleLineChartDetailed.call(self, $.extend({}, options, { highcharts_settings: null })); // inherit default settings only at this step
    self.settings = $.extend(true, self.settings, {}, options.highcharts_settings);

    // extensions
    if (options.extensions) {
        var show_only_top = options.extensions.show_only_top;
        self.update_extensions = function() {
            var data = self.data();
            if (!data || typeof show_only_top != "number")
                return;
            // any time the data changes, check which series should be hidden, and hide them
            var sorted_series = _(self.chart.series).sortBy(function(series) {
                return -1 * series.yAxis.getExtremes().dataMax;
            });
            for (var i = show_only_top; i < sorted_series.length; ++i) {
                sorted_series[i].hide();
            }
        };
    }
};

