// assumes:
//   MaxDashboardBase

// http://www.jshint.com/  validated regularly
/* global _, ko, number_format */
/* global MaxDashboardBase */

///////////////////////////////////////////////////////////////////////////////
// HELPER CLASSES

// wrapper to control sorted, tabular data
var ReportBase = function( options ) {
    "use strict";
    var self = this;

    // properties
    self.id = options.id;
    self.dashboard = options.dashboard;
    self.container = $(options.container_selector);
    self.indicator = $(options.indicator_selector);
    self.data = options.data_observable;
    self.column_meta = options.column_meta;
    self.sort = ko.observable( options.sort || { idx:0, dir:"desc" }); // dir:"asc|desc"  also: multiple sort columns are not supported
    self.notify_on_show = options.notify_on_show;
    if (options.ignoreQunit) self.ignoreQunit = options.ignoreQunit;

    self.disable_rendering = ko.observable( options.disable_rendering || false );
    self.loading = options.loading_observable || ko.computed( function() {
        return (self.data && self.data()) ? false : true;
    });

    // methods

    self.show_popup = function() {
        if( self.container.length === 0 )
            return; // container not found
        self.container.dialog( Report.defaults.jQueryUIDialog_options );
        self.container.mCustomScrollbar( 
            self.container.hasClass("mCustomScrollbar") ?
            "update" : Report.defaults.mCustomScrollbar_options );
        _.delay( self.disable_rendering, 250, false ); // has no effect if already false
        if( self.notify_on_show && typeof self.notify_on_show == "function" )
            self.notify_on_show();
    };

    self.show_inline = function() {
        if( self.container.length === 0 || self.indicator.length === 0 || self.dashboard === null )
            return; // container/indicator not found, or owner object not set
        self.dashboard.virtualControls.activeReport( self );
        _.delay( self.disable_rendering, 250, false ); // has no effect if already false
        if( self.notify_on_show && typeof self.notify_on_show == "function" )
            self.notify_on_show();
    };
};

// optional class used in place of report values for when the standard data formatting functions are not sufficient
// it is not necessary to use this class; raw values are fine as well, but they will be formatted using a data_formatter provided in column_meta
var ReportCell = function( val, str, str_fn ) {
    this.sort_value = val;
    this.display_str = str;
    
    this.toString = str_fn || function() {
        return this.display_str;
    };
};

// class to wrap an individual dataset, and control sorting and formatting
var Report = function( options ) {
    var self = this;
    ReportBase.call( self, options ); // inheritance

    self.error = ko.computed( function() {
        if( self.loading() || self.disable_rendering() )
            return null;
        var data = self.data ? self.data() : null;
        if( !data || !data.rows || data.rows.length == 0 )
            return MaxDashboardBase.error_html.no_data;
        return null;
    });
    function sortFunc( row_A, row_B ) {
        var sort = self.sort(); // sort row data
            var A = row_A[sort.idx];
            var B = row_B[sort.idx];
            // if using the optional ReportCell class, extract sort data
            if( A instanceof ReportCell )
                A = A.sort_value;
            if( B instanceof ReportCell )
                B = B.sort_value;
            // A, B assumed to be "natively comparable": string, number, Date
            if( A < B )
                return ((sort.dir == "asc")? -1 : 1);
            else if( A > B )
                return ((sort.dir == "asc")? 1 : -1);
            else //( A == B )
                return 0;
        }
    self.sorted_formatted_data = ko.computed( function() {
        if( self.disable_rendering() )
            return { cols:null, rows:null }; // structural preservation
        if( !self.data )
            return { cols:null, rows:null }; // structural preservation
        var data = self.data();
        if( !data )
            return { cols:null, rows:null }; // structural preservation
        data = $.extend( true, {}, data );
        
        data.rows.sort( sortFunc );
        if (data.rowPcts)
            data.rowPcts.sort(sortFunc);
        var meta = self.column_meta; // apply column metadata
        data.rows = _(data.rows).map( function( row ) { // apply data formats
            return _(row).map( function( col_val, col_idx ) {
                var col_key = data.cols[col_idx];
                var col_meta = meta[col_key] || meta.__default || null;
                if( col_meta && typeof Report.data_format[col_meta.data_format] === "function" )
                    return Report.data_format[col_meta.data_format]( col_val ); // formatted value
                else
                    return col_val; // raw value
            });
        });
        data.cols = _(data.cols).map( function( col_key ) { // create column labels
            var col_meta = meta[col_key] || meta.__default || null;
            if( col_meta )
                return col_meta.display.replace("$key",col_key); // custom display name
            else
                return col_key; // raw
        });
        data.cols.unshift( null ); // insert empty column for row index
        _(data.rows).each( function( row, idx ) {
            row.unshift( idx + 1 ); // insert row index value, post-sort
        });
        return data;
    });

    self.sorted_formatted_data.subscribe( function() {
        setTimeout( function() {
            if( self.container.hasClass("mCustomScrollbar") )
                self.container.mCustomScrollbar( "update" ); // update after content change
        }, 550 );
    });

    // methods

    // applies a context-sensitive sort operation depending on current sort and index of column clicked (like tablesorter)
    self.cycle_sort = function( col_idx ) {
        col_idx = ko.unwrap(col_idx) - 1; // ignore first column
        if( col_idx < 0 )
            return; 
        var sort = $.extend( {}, self.sort() );
        if( sort.idx == col_idx )
            sort.dir = (sort.dir == "asc"? "desc":"asc");
        else {
            sort.idx = col_idx;
            sort.dir = "desc";
        }
        self.sort( sort );
    };

    // shortcut method to get the appropriate column css class list, if any is defined
    self.column_css = function( col_idx ) {
        col_idx = ko.unwrap(col_idx) - 1; // ignore first column
        var data = self.data();
        if( !data || !data.cols )
            return null;
        var col_key = data.cols[col_idx];
        if( !col_key )
            return null;
        var meta = self.column_meta[col_key] || self.column_meta.__default;
        if( !meta )
            return null;
        return meta.css;
    };

    // default implementation: "odd" for odd-indexed rows
    self.row_css = function( row_idx ) {
        row_idx = ko.unwrap(row_idx);
        if( row_idx % 2 == 1 )
            return "odd";
        else
            return "";
    };

};

// fb# 27489
//  label is hardcoded to say "Highlight"
//  filterable column assumed to be column index 0 in data_observable (index 1 in sorted_formatted_data)
//  row style for filtered rows is hard-coded
var DynamicallyFilterableReport = function (options) {
    var self = this;
    Report.call(self, options); // inherit

    self.check_column_idx = 0;
    self.filtered_row_class = "emphasized";

    var initial_filter_value = "";
    if (typeof options.initial_filter_value !== "undefined")
        initial_filter_value = options.initial_filter_value;
    self.filter_input = ko.observable(initial_filter_value);
    self.filter_input_throttled = self.filter_input.extend({ throttle: 100 });

    self.filter_input_throttled.subscribe(function (newFilter) {
        cookie_set('lastFilter:' + self.dashboard.dynamic_config.businessUnitId(), newFilter, 7);

    });


    // row data apply to raw data (unsorted, unformatted)
    // used for stats calculations
    self.filtered_row_data = ko.computed(function () {
        var filter = self.filter_input_throttled();
        if (typeof filter == "string")
            filter = filter.toLowerCase();
        else
            return null;
        if (filter === "")
            return null;
        var data = self.data();
        if (data == null)
            return null;
        return _(data.rows).filter(function (row) {
            return row[self.check_column_idx].indexOf(filter) != -1;
        });
    });
    // row indices apply to sorted_formatted_data; not applicable to self.filtered_row_data()
    // used for visual row styling
    self.filtered_sorted_row_indices = ko.computed(function () {
        var filter = self.filter_input_throttled();
        if (typeof filter == "string")
            filter = filter.toLowerCase();
        else
            return null;
        if (filter === "")
            return null;
        var data = self.sorted_formatted_data();
        if (data == null)
            return null;
        var indices = [];
        _(data.rows).each(function (row, row_idx) {
            if (row[1 + self.check_column_idx].indexOf(filter) != -1)
                indices.push(row_idx);
        });
        return indices;
    });

    self.extended_output = options.extended_output_observable;

    self.row_css = _(self.row_css).wrap(function (old_fn, row_idx) {
        var css = old_fn(row_idx);
        row_idx = ko.unwrap(row_idx);
        var filtered_sorted_row_indices = self.filtered_sorted_row_indices();
        if (filtered_sorted_row_indices === null)
            return css;
        ///
        if (_(filtered_sorted_row_indices).contains(row_idx))
            return css + " " + self.filtered_row_class;
        else
            return css;
    });
};

//// Report - static properties and methods

// data formatting
Report.data_format = {};
Report.data_format.no_data_html = '<span class="no-data">no&nbsp;data</span>';
Report.data_format.credential_warning_html = '<span class="notification-warning" title="Invalid credentials for 3rd-party site(s) found"></span>';

Report.data_format["date.month"] = function( _Date ) {
    return _Date.toString( "MMM yyyy" );
};
Report.data_format["date.day"] = function( _Date ) {
    return _Date.toString( "M/d/yyyy" );
};
Report.data_format["timespan.hours"] = function( _Seconds ) {
    var hours = parseInt(_Seconds / 3600, 10) % 24,
        minutes = parseInt(_Seconds / 60, 10) % 60,
        seconds = parseInt(_Seconds % 60, 10);
    return (hours < 10 ? "0" + hours : hours) + ":" +
        (minutes < 10 ? "0" + minutes : minutes) + ":" +
        (seconds < 10 ? "0" + seconds : seconds);
};
Report.data_format["number.integer"] = function( _Number ) {
    if( !_Number )
        return Report.data_format.no_data_html;
    return number_format( _Number, 0 );
};
Report.data_format["number.integer.unchecked"] = function( _Number ) {
    return number_format( _Number, 0 );
};
Report.data_format["number.positive_integer"] = function( _Number ) {
    if( !_Number || _Number <= 0 )
        return Report.data_format.no_data_html;
    return number_format( _Number, 0 );
};
Report.data_format["number.positive_integer.unchecked"] = function( _Number ) {
    // note that negative numbers are still considered "no data"
    if( _Number < 0 )
        return Report.data_format.no_data_html;
    return number_format( _Number, 0 );
};

Report.data_format.number_formatting_function_factory = function( precision ) {
    return function( _Number ) {
        if( !_Number )
            return Report.data_format.no_data_html;
        return number_format( _Number, precision );
    };
};
Report.data_format["number.float.1"] = Report.data_format.number_formatting_function_factory( 1 );
Report.data_format["number.float.2"] = Report.data_format.number_formatting_function_factory( 2 );
Report.data_format["number.float.3"] = Report.data_format.number_formatting_function_factory( 3 );
Report.data_format["number.float.4"] = Report.data_format.number_formatting_function_factory( 4 );
Report.data_format["number.float.5"] = Report.data_format.number_formatting_function_factory( 5 );
Report.data_format["number.float"] = Report.data_format["number.float.2"];

Report.data_format["number.currency"] = function( _Number ) {
    if( !_Number )
        return Report.data_format.no_data_html;
    return "$"+number_format( _Number, 2 );
};
Report.data_format["number.dollars"] = function( _Number ) {
    if( !_Number )
        return Report.data_format.no_data_html;
    return "$"+number_format( _Number, 0 );
};
Report.data_format["number.percent"] = function( _Number ) {
    if( !_Number )
        return Report.data_format.no_data_html;
    return number_format( _Number, 2 )+"%";
};

Report.data_format["number.percent.unchecked"] = function( _Number ) {
    return number_format( _Number, 2 )+"%";
};

Report.data_format["number.ms_to_time"] = function (_Number) {
    if (!_Number || _Number <= 0)
        return Report.data_format.no_data_html;
    return msToTime(_Number);
};

// default options
Report.defaults = {};

Report.defaults.jQueryUIDialog_options = {
    width: "800px",
    height: 480,
    modal: true,
    autoOpen: true,
    position: { 
        my: "center top+25%",
        at: "center top",
        of: window
    }
};
Report.defaults.mCustomScrollbar_options = {
    scrollInertia: 0
};

// a collection of Reports, divides up similarly-structured data into separate tabs, by key
var ReportCollection = function (options) {
    var self = this;
    ReportBase.call( self, options ); // inheritance
    
    self.error = ko.computed( function() {
        if( self.loading() || self.disable_rendering() )
            return null;
        var data = self.data ? self.data() : null;
        if( !data || _.keys(data).length == 0 )
            return MaxDashboardBase.error_html.no_data;
        return null;
    });

    // properties
    self.active_tab = ko.observable( null ); // string name of tab only

    self.loading = ko.computed( function() {
        return (self.data && self.data())? false : true;
    });

    self.child_reports = ko.computed( function() {
        if( !self.data )
            return { }; // structural preservation
        var data = self.data();
        if( !data )
            return { }; // structural preservation
        return _(data).objMap( function( child_data, child_report_key, data_collection ) {
            // create a new Report with a data observable bound to one of the keys' data
            return new Report({
                container: null, // controlled by parent
                data_observable: ko.computed( function() {
                    var data = self.data();
                    if( !data )
                        return null;
                    return data[ child_report_key ];
                }),
                column_meta: options.column_meta, // all children inherit the column options of the collection
                sort: options.sort
            });
        });
    });

    // subscriptions

    // set a default tab the first time any tabs become available
    // also enforce the validity of the selected tab, in case a tab that was previously present is present no longer
    self.child_reports.subscribe( function( child_reports ) {
        var keys = _.keys( child_reports );
        if( self.active_tab() !== null && _(keys).contains( self.active_tab() ))
            return; // there is a currently selected tab, and it references a dataset that currently exists
        if( keys.length > 0 )
            self.active_tab( keys[0] ); // select the first tab
    });

    // notify scrollbar when active tab changes
    self.active_tab.subscribe( function() {
        if( self.container.hasClass("mCustomScrollbar") )
            self.container.mCustomScrollbar( "update" );
    });
};


// TABLE BINDING plugin for Knockout http://knockoutjs.com/
// (c) Michael Best
// License: MIT (http://www.opensource.org/licenses/mit-license.php)
// Version 0.2.2
// Modified by Tyler Cole 11-21-2013 for MAX

(function(ko, undefined) {

var div = document.createElement('div'),
    elemTextProp = 'textContent' in div ? 'textContent' : 'innerText';
div = null;

function makeRangeIfNotArray(primary, secondary) {
    if (primary === undefined && secondary)
        primary = secondary.length;
    return (typeof primary === 'number' && !isNaN(primary)) ? ko.utils.range(0, primary-1) : primary;
}

function isArray(a) {
    return a && typeof a === 'object' && typeof a.length === 'number';
}

// report binding
ko.bindingHandlers.report = {
    // init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    //     // establish subscriptions that will cause this binding to update
    //     var report = ko.utils.unwrapObservable(valueAccessor()),
    //         data = report.sorted_formatted_data().rows,
    //         header = report.sorted_formatted_data().cols,
    //         sort = report.sort();
    // },
    update: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var report = ko.utils.unwrapObservable(valueAccessor()),
            sorted = report.sorted_formatted_data(),
            data = sorted.rows,
            dataPcts = sorted.rowPcts || undefined,
            dataItem = undefined,
            header = sorted.cols,
            sort = report.sort(),
            dataIsArray = isArray(data),
            dataIsObject = typeof data === 'object',
            dataItemIsFunction = typeof dataItem === 'function',
            headerIsArray = isArray(header),
            headerIsFunction = typeof header === 'function',
            cols = makeRangeIfNotArray(ko.utils.unwrapObservable(undefined), headerIsArray && header),
            rows = makeRangeIfNotArray(ko.utils.unwrapObservable(undefined), dataIsArray && data),
            numCols = cols && cols.length,
            numRows = rows && rows.length,
            itemSubs = [], 
            tableBody, 
            rowIndex, 
            colIndex;

        // data must be set and be either a function or an array
        if (!dataIsObject && !dataItemIsFunction)
            throw Error('table binding requires a data array or dataItem function');

        // If not set, read number of columns from data
        if (numCols === undefined && dataIsArray && isArray(data[0])) {
            for (numCols = rowIndex = 0; rowIndex < data.length; rowIndex++) {
                if (data[0].length > numCols)
                    numCols = data[0].length;
            }
            cols = makeRangeIfNotArray(numCols);
        }

        // Remove event handlers
        $("th",element).each( function( idx, th ) {
            $(th).off(".cycle_sort");
        });
        // Remove previous table contents (use removeNode so any subscriptions will be disposed)
        while (element.firstChild)
            ko.removeNode(element.firstChild);

        if (!(numRows >= 0) || !(numCols > 0))
            return; // cannot display anything
        
        var html = '<table>';

        // Generate a header section if a header function is provided
        if (header) {
            html += '<thead><tr>';
            for (colIndex = 0; colIndex < numCols; colIndex++) {
                var headerValue = headerIsArray ? header[colIndex] : (headerIsFunction ? header(cols[colIndex]) : cols[colIndex][header]);
                var th_css = '';
                if (colIndex > 0) {
                    th_css += ' header';
                    if (sort.idx + 1 == colIndex) {
                        if (sort.dir == 'asc')
                            th_css += ' headerSortUp';
                        if (sort.dir == 'desc')
                            th_css += ' headerSortDown';
                    }
                }
                html += '<th class="' + th_css + '">' + (headerValue == null ? '' : headerValue) + '</th>';
            }
            html += '</tr></thead>';
        }

        // Generate the table body section
        html += '<tbody>';

        for (rowIndex = 0; rowIndex < numRows; rowIndex++) {
            var tr_css = report.row_css( rowIndex );
            if (!tr_css)
                html += '<tr>';
            else
                html += '<tr class="' + tr_css + '">';
            

            for (colIndex = 0; colIndex < numCols; colIndex++) {
                var td_class = report.column_css( colIndex );
                if (!td_class)
                    html += '<td class="wrapped">';
                else
                    html += '<td class="wrapped ' + td_class + '">';

                html += '<div class="rowWrapper">'; // wrapper
                
                var value = unwrapItemAndSubscribe(rowIndex, colIndex);
                
                if (colIndex <= 1) {
                    html += value;
                } else {
                    html += '<div class="rowEntry" style="z-index:2">' + value + '</div>';
                    if (dataPcts && sort.idx + 1 == colIndex) { // not the index column or dealership column, is the sorted column
                        var relativeWidth = dataPcts[rowIndex][colIndex - 1] || 0;
                        html += '<div class="rowEntry pctBar" style="width:' + relativeWidth + '%"></div>';
                    }
                }
                html += '</div></td>';
            }
            html += '</tr>';
        }
        html += '</tbody></table>';

        // Insert new table contents
        var tempDiv = document.createElement('div');
        tempDiv.innerHTML = html;
        var tempTable = tempDiv.firstChild;
        element.innerHTML = '<table class="report_table tablesorter" cellpadding="0" cellspacing="0" border="0"></table>';
        while (tempTable.firstChild)
            element.firstChild.appendChild(tempTable.firstChild);
        // Attach event handlers
        $("th",element).each( function( idx, th ) {
            $(th).on("click.cycle_sort", function() {
                report.cycle_sort( idx );
            });
        });

        // Make sure subscriptions are disposed if the table is cleared
        if (itemSubs) {
            tableBody = element.firstChild.tBodies[0];
            ko.utils.domNodeDisposal.addDisposeCallback(tableBody, function() {
                ko.utils.arrayForEach(itemSubs, function(itemSub) {
                    itemSub.dispose();
                });
            });
        }
        
        // Return the item value and update table cell if observable item changes
        function unwrapItemAndSubscribe(rowIndex, colIndex) {
            // Use a data function if provided; otherwise use the column value as a property of the row item
            var rowItem = rows[rowIndex], colItem = cols[colIndex],
                itemValue = dataItem ? (dataItemIsFunction ? dataItem(rowItem, colItem, data) : data[rowItem][colItem[dataItem]]) : data[rowItem][colItem];

            if (ko.isObservable(itemValue)) {
                itemSubs.push(itemValue.subscribe(function(newValue) {
                    if (tableBody)
                        tableBody.rows[rowIndex].cells[colIndex][elemTextProp] = newValue == null ? '' : newValue;
                }));
                itemValue = itemValue.peek ? itemValue.peek() : ko.ignoreDependencies(itemValue);
            }
            return itemValue == null ? '' : itemValue;
        }

    }
};

/*
 * Escape a string for html representation
 */
ko.utils.escape = function(string) {
    return (''+string).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&#x27;').replace(/\//g,'&#x2F;');
};

/*
 * Helper functions for finding minified property names
 */
function findNameMethodSignatureContaining(obj, match) {
    for (var a in obj)
        if (obj.hasOwnProperty(a) && obj[a].toString().indexOf(match) >= 0)
            return a;
}

function findPropertyName(obj, equals) {
    for (var a in obj)
        if (obj.hasOwnProperty(a) && obj[a] === equals)
            return a;
}

function findSubObjectWithProperty(obj, prop) {
    for (var a in obj)
        if (obj.hasOwnProperty(a) && obj[a] && obj[a][prop])
            return obj[a];
}

/*
 * ko.ignoreDependencies is used to access observables without creating a dependency
 */
if (!ko.ignoreDependencies) {
    var depDet = findSubObjectWithProperty(ko, 'end'),
        depDetBeginName = findNameMethodSignatureContaining(depDet, '.push({');
    ko.ignoreDependencies = function(callback, object, args) {
        try {
            depDet[depDetBeginName](function() {});
            return callback.apply(object, args || []);
        } finally {
            depDet.end();
        }
    }
}

})(ko);

