/*global Highcharts, _, $, _gaq */

var MAXUI = (function () {

    "use strict";

    var panelBorderRadius = 16;
    var credentialNagHtml;
    return {

        IsIE: function () {
            return navigator.appName === "Microsoft Internet Explorer";
        },
        IsIE6: function () {
            return navigator.appName === "Microsoft Internet Explorer" && navigator.appVersion.indexOf("MSIE 6.") > -1;
        },
        IsIE7: function () {
            return navigator.appName === "Microsoft Internet Explorer" && navigator.appVersion.indexOf("MSIE 7.") > -1;
        },
        IsIE8: function () {
            return navigator.appName === "Microsoft Internet Explorer" && navigator.appVersion.indexOf("MSIE 8.") > -1;
        },
        IsIE9: function () {
            return navigator.appName === "Microsoft Internet Explorer" && navigator.appVersion.indexOf("MSIE 9.") > -1;
        },

        /// Initialize
        OnReady: function () {
            this.Buid = $("#BusinessUnitIdHidden").val(); // business unit should be output to a hidden form element on every page

            this.InitNavMenu();
            this.InitPanelShadow();
            this.InitNewWindowLinks();
            this.CheckCredentials();
            this.QuickDealerChooser();

            if (this.IsIE()) { this.InitIE(); }
        },

        InitIE: function () {

            $.each($("#MainNavList li.menuHead"), function () {

                var menu = $(this).find("ul:first"),
                    menuWidth = $(menu).width();

                $.each($(menu).find("li a"), function () {
                    $(this).width(menuWidth);
                });

            });

        },

        /// Automatically sets up links with the 'newWindowLink' class to open in a new window
        /// (optionally populates and/or polls the child window)
        InitNewWindowLinks: function () {
            var self = this;
            $(".newWindowLink").off("click").on("click", function (e) {

                var linkId = $(this).attr("id") === undefined ? "NEWWIN" : $(this).attr("id"),
                    linkHref = $(this).attr("href") === undefined || $(this).attr("href") === "" ? window.location : $(this).attr("href"),
                    newWin = window.open(linkHref, linkId.replace("Link", ""), $(this).attr("options")),
                    poller;

                // Handle child window polling (Replace 'if' with 'switch' to scale)
                if (linkId === "priceLnk") {
                    setChildWindowPolling(function () { self.MaskWindow("Loading ..."); window.location.reload(); });
                }

                // Child window polling helper
                function setChildWindowPolling(callback) {
                    poller = window.setInterval(pollChildWindow, 500);
                    function pollChildWindow() {
                        if (newWin.closed) {
                            clearInterval(poller);
                            callback();
                        }
                    }
                }

                e.preventDefault();
                e.stopPropagation();

            });

        },

        /// Setup main navigation drop-down-menus
        InitNavMenu: function () {

            // Setup menu headers to open menus on mouseover
            $("li.menuHead").hover(function () { $(this).find("ul").show(); }, function () { $(this).find("ul").hide(); });
            // Set z-index so drop-downs cover adjacent menu items
            $("li.menuHead").each(function (index) {
                $(this).css("z-index", 9999 - index);
            });

        },

        /// Setup panel drop shadow
        InitPanelShadow: function () {

            $(".panel").each(function () {
                var borderRadius = panelBorderRadius,
                // Store positioning CSS
                positionCss = "margin-top:" + $(this).css("margin-top") + ";" +
                    "margin-right:" + $(this).css("margin-right") + ";" +
                    "margin-bottom:" + $(this).css("margin-bottom") + ";" +
                    "margin-left:" + $(this).css("margin-left") + ";" +
                    "clear:" + $(this).css("clear") + ";" +
                    "float:" + $(this).css("float") + ";" +
                    "position:" + $(this).css("position") + ";" +
                    "top:" + $(this).css("top") + ";" +
                    "right:" + $(this).css("right") + ";" +
                    "bottom:" + $(this).css("bottom") + ";" +
                    "left:" + $(this).css("left") + ";";

                // Reset positioning CSS
                $(this).css("margin", "0").css("position", "static").css("top", "auto").css("right", "auto").css("bottom", "auto").css("left", "auto");
                $(this).wrap(
                            "<div style='border:solid #fafafa 1px;border-radius:" + (borderRadius + 4) + "px;-moz-border-radius:" + (borderRadius + 4) + "px;-webkit-border-radius:" + (borderRadius + 4) + "px;height:" + (parseInt($(this).css("height"), 10) + 8) + "px;width:" + (parseInt($(this).css("width"), 10) + 8) + "px;" + positionCss + "'>")
                                .wrap("<div style='border:solid #f4f4f4 1px;border-radius:" + (borderRadius + 3) + "px;-moz-border-radius:" + (borderRadius + 3) + "px;-webkit-border-radius:" + (borderRadius + 3) + "px;height:" + (parseInt($(this).css("height"), 10) + 6) + "px;width:" + (parseInt($(this).css("width"), 10) + 6) + "px;' />")
                                .wrap("<div style='border:solid #e8e8e8 1px;border-radius:" + (borderRadius + 2) + "px;-moz-border-radius:" + (borderRadius + 2) + "px;-webkit-border-radius:" + (borderRadius + 2) + "px;height:" + (parseInt($(this).css("height"), 10) + 4) + "px;width:" + (parseInt($(this).css("width"), 10) + 4) + "px;'>")
                                .wrap("<div style='border:solid #d6d6d6 1px;border-radius:" + (borderRadius + 1) + "px;-moz-border-radius:" + (borderRadius + 1) + "px;-webkit-border-radius:" + (borderRadius + 1) + "px;height:" + (parseInt($(this).css("height"), 10) + 2) + "px;width:" + (parseInt($(this).css("width"), 10) + 2) + "px;' />"
                            );

                //$( this ).append( "<p style=\"position:absolute;\">" + ( parseInt( $( this ).css( "height" ), 10 ) + 8 ) + ", " + ( parseInt( $( this ).css( "height" ), 10 ) + 6 ) + ", " + ( parseInt( $( this ).css( "height" ), 10 ) + 4 ) + ", " + ( parseInt( $( this ).css( "height" ), 10 ) + 2 ) + ", " + "</p>" );

            });
        },

        MaskWindow: function (message) {

            message = message === undefined ? "" : message;

            $("html").css("overflow", "hidden");
            $("body").append("<div class=\"mask\" style=\"height:100%;width:100%;top:0;left:0;z-index:999999;\">" +
                    "<p style=\"margin:20% auto;\">" + message + "</p></div>");

        },

        Loading: false,
        Buid: null,

        CheckCredentials: function (forceCheck) {

            /*
            requires: underscore, jQuery cookie, jQuery templates, jQuery UI

            cookie needs to track:
            have I checked for valid credentials this session?
            were the credentials invalid?
            have I shown the modal?
            */

            if (!$.cookie) { return; }

            var credentials = {},
                instance = this,
                expires = new Date(),
                external_credentials = $.cookie("external_credentials" + instance.Buid); // null if not set

            expires.setMinutes(expires.getMinutes() + 15);

            // override cookie to force check the DB
            forceCheck = (forceCheck) ? forceCheck : false;
            //forceCheck = true; // testing

            // credentials are invalid, or have not been checked
            if (!_.isUndefined(instance.Buid) && (external_credentials === "invalid" || external_credentials === null || forceCheck === true)) {

                // call ExternalConnections
                $.ajax({
                    url: "/merchandising/ExternalCredentials.aspx/get/" + instance.Buid,
                    cache: false,
                    error: function () {

                    },
                    success: function (data) {
                        credentials = data;
                        //credentials.HasInvalidCredentials = true; // testing

                        if (credentials.HasInvalidCredentials === true) {

                            $.cookie("external_credentials" + instance.Buid, "invalid", { path: '/' });
                            instance.LoadCredentialsNag(credentials);

                        } else {

                            $.cookie("external_credentials" + instance.Buid, "valid", { path: '/', expires: expires });

                        }

                    }
                });

            } // else: credentials cookie must say valid

        },

        CreateNagModal: function (instance, credentials) {
            var expires = new Date();
            var modalExists = $("#external_credentials_modal").length !== 0;

            expires.setMinutes(expires.getMinutes() + 15);

            if (modalExists) {
                $("#external_credentials_modal").dialog('destroy').remove();
                $("#crednagbar").remove();
            }
            $("body").prepend(instance.credentialNagHtml);

            // build form elements with data from credentials
            if (!_.isEmpty(credentials.SiteCredentials)) {
                $("#credSitesField .fields").html($("#credSites").tmpl(credentials.SiteCredentials));
            }
            if (!_.isEmpty(credentials.OEMCredentials)) {
                $("#credOEMField .fields").html($("#credOEM").tmpl(credentials.OEMCredentials));
            }

            $("#external_credentials_modal").dialog({
                autoOpen: false,
                modal: true,
                width: 840,
                title: "Invalid Credentials",
                open: function () {
                    // set cookie to "shown" for this session
                    $.cookie("external_credentials_modal" + instance.Buid, "shown", { path: '/', expires: expires });
                },
                close: function () {
                    //$("#UserMessaging").show();
                    instance.Loading = false;
                    $(".dialog .save").removeClass("loading").removeAttr("disabled");
                }
            });

            $(".dialog .save").on("click", function () {
                $(this).addClass("loading").attr("disabled", "disabled").text("\xA0");
                instance.SaveCredentials($(this).closest("form").serializeArray());
                return false;
            });

            $(".dialog .cancel").on("click", function () {
                if (instance.Loading === false) {
                    $(".dialog").dialog("close");
                }
                return false;
            });

            $("#crednagbar .close").on("click", function () {
                $(this).parents(".globalmessage").hide();
                // hide nag for the session
                $.cookie("external_credentials" + instance.Buid, "valid", { path: '/' });
            });

            $(".dialog .dismiss, #crednagbar .dismiss").on("click", function () {
                // set cooke for one week
                $.cookie("external_credentials" + instance.Buid, "valid", { path: '/', expires: Date.today().add(7).days() });
                $("#crednagbar").hide();
                $("#external_credentials_modal").dialog("close");
                return false;
            });

            $(".dialog-link-credentials").on("click", function () {
                $("#external_credentials_modal").dialog("open");
                return false;
            });

            $("#credVersionRetryButton").on("click", function () {
                $.cookie("external_credentials_modal" + instance.Buid, null, { path: '/', expires: expires });
                instance.CheckCredentials(true);
            });

        },

        LoadCredentialsNag: function (credentials) {

            var instance = this,
                formData,
                external_credentials_modal = $.cookie("external_credentials_modal" + instance.Buid); // null if not set



            // load HTML for nag from remote file
            $.ajax({
                url: "/merchandising/Public/Scripts/Max/CredentialNag.html",
                cache: false,
                dataType: "html",
                success: function (data) {
                    instance.credentialNagHtml = data;
                    instance.CreateNagModal(instance, credentials);
                    // if not shown or not set, then show modal
                    if (external_credentials_modal !== "shown") {
                        $("#external_credentials_modal").dialog("open");
                    }

                }
            });

        },

        SaveCredentials: function (data) {

            var instance = this,
                formData = {},
                expires = new Date();

            expires.setMinutes(expires.getMinutes() + 15);

            instance.Loading = true;

            formData = {
                "BusinessUnitId": parseInt(instance.Buid, 10),
                "Credentials": []
            };

            // need to loop through and combine user/pass into one object
            _.each(data, function (field, key, list) {

                if (field.name.substr(0, 8) === "Username") {

                    var groupId, typeId, username, password, credVersion, lookup;


                    // pull out IDs from the field name (e.g. "UsernameG2T1")
                    //                    groupId = field.name.substr(9, 1);
                    //                    typeId = field.name.substr(11, 1);
                    username = field.value;
                    lookup = field.name.substr(8); // ex: G2T1

                    var groupAndType = lookup.split("T"); // split on arrays based on the letter "T"

                    if (groupAndType.length > 1) {
                        groupId = groupAndType[0].replace("G", "");
                        typeId = groupAndType[1];
                    }

                    password = _.find(list, function (passfield) {
                        //get value for password from object where name = PasswordG2T1
                        return passfield.name === "Password" + lookup;
                    });
                    credVersion = _.find(list, function (versionField) {
                        //get value for credentialVersion from object where name = UIDG2T1
                        return versionField.name === "UID" + lookup;
                    });

                    formData.Credentials.push({
                        "GroupId": parseInt(groupId, 10),
                        "TypeId": parseInt(typeId, 10),
                        "Username": username,
                        "Password": password.value,
                        "CredentialVersion": (credVersion === undefined ? null : credVersion.value)
                    });

                }

            });

            // attempt to save via $.ajax()
            $.ajax({
                type: "POST",
                url: "/merchandising/ExternalCredentials.aspx/set",
                data: JSON.stringify(formData),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                error: function (error) {
                    var showFormError = function () {
                        $("#credFormError").show();
                        $("#credFormThanks").hide();
                        $("#credForm").hide();
                        $("#crednagbar").hide();
                        $("#credFormVersionError").hide();
                    };
                    var showVersionError = function () {
                        $("#credFormVersionError").show();
                        $("#credFormThanks").hide();
                        $("#credForm").hide();
                        $("#crednagbar").hide();
                        $("#credFormError").hide();
                    };

                    var returnData;
                    if (error.responseText !== undefined) {
                        returnData = JSON.parse(error.responseText);
                    }

                    if (returnData && returnData.message == "This credential update failed because it has been updated by another user.") {
                        var obj = $("#credVersionErrorMessage");

                        if (obj !== undefined)
                            obj[0].textContent = returnData.message;

                        showVersionError();
                    } else if (returnData) {
                        var obj = $("#credFormErrorMessage");

                        if (obj !== undefined)
                            obj[0].textContent = returnData.message;

                        showFormError();
                    } else {
                        showFormError();
                    }


                },
                success: function () {
                    $("#credFormThanks").show();
                    $("#credForm").hide();
                    $("#crednagbar").hide();
                    $("#credFormError").hide();
                    $("#credFormVersionError").hide();
                    $.cookie("external_credentials" + instance.Buid, "valid", { path: '/', expires: expires });
                },
                complete: function () {
                    instance.Loading = false;
                }
            });

        },

        QuickDealerChooser: function () {

            var instance = this,
                menuDom = $("#groupMenu"),
                ajaxcall,
                config = {};

            config.url = "/merchandising/ExternalCredentials.aspx/dealers/" + instance.Buid;

            if (!menuDom[0] || !$().mCustomScrollbar) { return false; }
            ajaxcall = instance.maxAjax(config);
            ajaxcall.done(function (userdealers) {

                userdealers = JSON.parse(userdealers);

                var $ul = $("<ul></ul>");
                var generatedHtml = $("#groupMenuLinks").clone().tmpl(userdealers);
                $ul.html(generatedHtml);

                var $groupListContainer = $("#groupListContainer");
                $groupListContainer.append($ul);

                menuDom.appendTo("#Header"); //.find("ul").mCustomScrollbar();

                $("#Header .dealerSelector").on("click", function () {
                    menuDom.toggle(); //.find("ul").mCustomScrollbar("update");
                    $('#groupMenu .textFilter-input').focus();
                    return false;
                });

                /* textFilter - a simple module for a text input filter.
                * Author -- Daniel Bruckner 2011
                *
                * To use:
                * 1. Include this file on your page, probably at the bottom.
                * 2. Add the class "textFilter-input" to any text input tag you would like to
                *    act as a filter.
                * 3. Add the class "textFilter-target" to any DOM element to be hidden if it
                *    does not match the filter text.
                * 4. Add the class "textFilter-match" to the DOM element (contained by a
                *    textFilter-target) that contains the match text for its ancestor.
                *

                /* Initialize filter inputs */
                var defaultText = $('.textFilter-input').val();

                $('.textFilter-input')
                    .focus(function (e) {
                        if ($(this).val() === defaultText)
                            $(this).val('');
                    })
                    .blur(function (e) {
                        if ($(this).val() === '')
                            $(this).val(defaultText);
                    })
                    .keyup(function (e) {
                        var patterns = $(this).val().toLowerCase().split(' ');
                        if (!patterns.length)
                            return;
                        $('.textFilter-target')
                            .hide()
                            .filter(function () {
                                var matchText = $(this)
                                    .find('.textFilter-match')
                                    .text()
                                    .toLowerCase();
                                for (var i = 0; i < patterns.length; i++)
                                    if (matchText.indexOf(patterns[i]) === -1)
                                        return false;
                                return true;
                            })
                            .show();

                    });

            });

        },

        // default $.ajax object (returns a $.deferred)
        maxAjax_config: {
            type: "GET",
            dataType: "json",
            cache: false,
            contentType: "application/json; charset=utf-8", // not sure why this is here, we are using GET
            error: function (xhr, textStatus) {
                // only direct to error page if there's a server error:
                // http://stackoverflow.com/questions/1370322/jquery-ajax-fires-error-callback-on-window-unload
                if (xhr.status === 500 || (xhr.Status === 0 && xhr.readyState === 4)) {
                    window.location.assign("/merchandising/ErrorPage.aspx?Error=" + textStatus);
                }
            }
        },
        maxAjax: function (override) {
            return $.ajax($.extend({}, this.maxAjax_config, override));
        }

    };


})();
var MaxUiPage;
$(function () {
    MaxUiPage = MAXUI;
    MaxUiPage.OnReady();

    if($.cookie) {
        var ie7upgrade = $("#ie7upgrade");
        if(!$.cookie("ie7upgrade") && ie7upgrade[0]) {
            ie7upgrade.show()
            .find(".close").on("click", function() {
                $(this).parents(".globalmessage").hide();
                // hide nag for the session
                $.cookie("ie7upgrade","warned", { path: '/' });
            });
        }
    }

} );