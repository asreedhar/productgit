// assumes:
//   MaxDashboardBase

// http://www.jshint.com/  validated regularly
/* global _, ko, number_format */
/* global MaxDashboardBase */
/* global Chart, SimpleColumnChart, SquatColumnChart, BarChartWithBenchmarks, LineChartWithBudgetChanges, HangingLabelColumnChart, HangingLabelColumnChartWithValueIndicator, PieChart, SimpleLineChart, LineChartDynamicLegend  */
/* global Report, ReportCollection, ReportCell, DynamicallyFilterableReport */

///////////////////////////////////////////////////////////////////////////////
// EXECUTIVE DASHBOARD (single-business-unit)
var MaxDashboardExecutive = function() {
    "use strict";
    var self = this;

    // inheritance
    MaxDashboardBase.call( self );

    self.static_config = $.extend( true, self.static_config, {
        GA_Trends__AcquisitionChannels_NameMapping: {
            "(none)": "Direct",
            "(not set)": "Direct",
            "display": "Display",
            "cpm": "Display",
            "organic": "Organic Search",
            "cpc": "Paid Search",
            "ppc": "Paid Search",
            "email": "Email",
            "referral__non_social": "Referral",
            "referral__social": "Social"
        }
    });


    // init
    self.PreBindingInitialize = _(self.PreBindingInitialize).wrap( function( base_fn ) {
        base_fn();

        // services
        self.RegisterService({
            id: "data",
            url: self.static_config.baseURL+"/"+"DashboardData.aspx",
            args: [
                self.dynamic_config.businessUnitId,
                self.controls.vehicleTypeFilter
            ]
        });
        self.RegisterService({
            id: "trends",
            url: self.static_config.baseURL+"/"+"SitePerformanceTrend.aspx",
            args: [
                self.dynamic_config.businessUnitId,
                self.controls.vehicleTypeFilter,
                self.controls.dateRangeFrom,
                self.controls.dateRangeTo
            ]
        });
        self.RegisterService({
            id: "ga",
            url: self.static_config.baseURL+"/"+"GoogleAnalyticsDataAll.aspx",
            args: [
                self.dynamic_config.businessUnitId,
                1, // "PC" (google analytics profile type)
                self.controls.dateRangeFrom,
                self.controls.dateRangeTo,
                25 // results page size limit
            ]
        });
        self.RegisterService({
            id: "ga_trends",
            url: self.static_config.baseURL+"/"+"GoogleAnalyticsDataAllUngrouped.aspx",
            args: [
                self.dynamic_config.businessUnitId,
                1, // "PC" (google analytics profile type)
                self.controls.dateRangeFrom,
                self.controls.dateRangeTo,
                25 // results page size limit
            ]
        });
        self.RegisterService({
            id: "ttm",
            url: self.static_config.baseURL+"/"+"TimeToMarketReport.aspx",
            args: [
                self.dynamic_config.businessUnitId,
                self.controls.vehicleTypeFilter,
                self.controls.dateRangeFrom,
                self.controls.dateRangeTo
            ]
        });
        self.RegisterService({
            id: "marketing",
            url: self.static_config.baseURL+"/"+"Dashboard/Traffic/ShowroomViews",
            dataArgs: {
                "businessUnitID" : self.dynamic_config.businessUnitId,
                "startDate" : self.controls.dateRangeFrom,
                "endDate" : self.controls.dateRangeTo
            }
        });
        self.RegisterService({
            id: "website20traffic",
            url: self.static_config.baseURL+"/"+"Dashboard/Traffic/WebTraffic",
            dataArgs: {
                "businessUnitID" : self.dynamic_config.businessUnitId,
                "startDate" : self.controls.dateRangeFrom,
                "endDate" : self.controls.dateRangeTo
            }
        });

        // config
        var configOpts = jQuery.parseJSON($('#dashboardSettingsHidden').val());
        if( configOpts ) {
            self.dynamic_config.enableAnalyticsSuite( configOpts.HasAnalyticsSuite );
            self.dynamic_config.show_Overview( configOpts.ShowOnlineClassifiedOverview );
            self.dynamic_config.show_TTM( configOpts.ShowTimeToMarket );
            self.dynamic_config.merchandisingAlertSelectionBucket( configOpts.MerchandisingAlertSelectionBucket );
            self.dynamic_config.inventoryBaseUrl(configOpts.InventoryBaseUrl);
            self.dynamic_config.hasDigitalShowroom(configOpts.HasDigitalShowroom);
            self.dynamic_config.hasWebsite20(configOpts.HasWebsite20);
            self.dynamic_config.hasShowroomReporting(configOpts.HasShowroomReporting);
        }

        // basic processing - computed directly from service responses
        self.data.processing = {};

        self.data.processing.SitePerformanceTrend_Totals = ko.computed( function() {
            var data = self.services.trends.response();
            if( !data || !data.Trends )
                return null;
            return data.Totals.Sites;
        });
        
        self.data.processing.SitePerformanceTrend_Filtered = ko.computed( function() {
            var data = self.services.trends.response();
            if( !data )
                return null;
            var filter_fn = function(val, key) {
                return _(self.static_config.listing_site_filter).contains(key);
            };
            var filtered_data = {
                Totals: $.extend( {}, data.Totals ),
                Trends: $.extend( {}, data.Trends )
            };
            filtered_data.Totals.Sites = _(data.Totals.Sites).objFilter( filter_fn );
            filtered_data.Trends = _(data.Trends).objFilter( filter_fn );
            return filtered_data;
        });

        // gauges
        self.data.gauges = {};

        self.data.gauges.counts = ko.computed( function() {
            var struct = {
                NotOnlineCount: 0,
                NotOnlinePercent: 0,
                MerchandisingAlertCount: 0,
                MerchandisingAlertPercent: 0
            };
            var data = self.services.data.response();
            
            if( data && data.BucketsData )
                $.extend( struct, data.BucketsData );

            var merchandisingBucket = _.find(struct.ActionBuckets, function(bucket) {
                if(self.dynamic_config.merchandisingAlertSelectionBucket())
                    return bucket.Id == self.dynamic_config.merchandisingAlertSelectionBucket();
                return null;
            });

            struct.MerchandisingAlertCount = (merchandisingBucket != null) ? merchandisingBucket.Count : 0;
            struct.MerchandisingAlertPercent = (merchandisingBucket != null) ? self.calculatePercentage(merchandisingBucket, struct) : 0;

            return struct;
        });
        
        self.calculatePercentage = function(merchandisingBucket, struct) {
            var percentage = merchandisingBucket.Count / (struct.SelectedFilterCount);
            return (percentage * 100).toFixxed(0);
        };


        self.data.gauges.merchandisingAlertText = ko.computed(function() {
            var data = self.services.data.response();

            if (data && data.BucketsData && self.dynamic_config.merchandisingAlertSelectionBucket()) {
                var bucket = _.find(data.BucketsData.ActionBuckets, function(bucket) {
                    return bucket.Id == self.dynamic_config.merchandisingAlertSelectionBucket();
                });

                if (typeof (bucket) != "undefined") {
                    return bucket.DisplayName;
                }
            }
            return "N/A";
        });

        self.data.gauges.merchandisingAlertText.subscribe(function(data) {
            if (data == "N/A") {
                $('#MerchandisingAlertGauge').hide();
            } else {
                $('#MerchandisingAlertGauge').show();
            }
        });

        // alerts
        self.data.alerts = {};

        self.data.alerts.action = ko.computed( function() {
            var data = self.services.data.response();
            if( !data || !data.BucketsData )
                return null;
            return {
                type: "Online",
                items: data.BucketsData.ActionBuckets,
                total: data.BucketsData.ActionBucketsTotal
            };
        });
        self.data.alerts.activity = ko.computed( function() {
            var data = self.services.data.response();
            if( !data || !data.BucketsData )
                return null;
            return {
                type: "Merchandising",
                items: data.BucketsData.ActivityBuckets,
                total: data.BucketsData.ActivityBucketsTotal
            };
        });
        self.data.alerts.misc = ko.computed( function() {
            var data = self.services.data.response();
            if( !data || !data.BucketsData )
                return null;
            return {
                OfflineBucket: data.BucketsData.OfflineBucket
            };
        });

        // dynamic data is computed from service response + UI state
        self.data.dynamic = {};

        self.data.dynamic.SitePerformanceTrend_DirectLeadsDynamic = ko.computed( function() {
            // modifies the original data, but it's safe because it only adds new fields
            var data = self.services.trends.response();
            if( !data || !data.Trends )
                return null;
            var ui = {
                email: self.controls.enableLeadsEmail(),
                phone: self.controls.enableLeadsPhone(),
                chat: self.controls.enableLeadsChat()
            };
            _(data.Trends).each( function( site_data ) {
                _(site_data).each( function( raw_data_point ) {
                    raw_data_point.__DirectLeadsDynamic =
                        (ui.email? raw_data_point.EmailLeadsCount : 0) +
                        (ui.phone? raw_data_point.PhoneLeadsCount : 0) +
                        (ui.chat?  raw_data_point.ChatRequestsCount : 0);
                });
            });
            return data;
        });
        self.data.dynamic.SitePerformanceTrend_Filtered_TotalDirectLeadsDynamic = ko.computed( function() {
            // modifies the original data, but it's safe because it only adds new fields
            var data = self.data.processing.SitePerformanceTrend_Filtered();
            if( !data || !data.Totals )
                return null;
            var ui = {
                email: self.controls.enableLeadsEmail(),
                phone: self.controls.enableLeadsPhone(),
                chat: self.controls.enableLeadsChat()
            };
            _(data.Totals.Sites).each( function( site_totals_data ) {
                site_totals_data.__TotalDirectLeadsDynamic =
                    (ui.email? site_totals_data.EmailValue : 0) +
                    (ui.phone? site_totals_data.PhoneValue : 0) +
                    (ui.chat?  site_totals_data.ChatValue : 0);
            });
            return data;
        });

        self.data.dynamic.CurrentMonthProgressFactor = ko.computed( function() {
            return (Date.today().getDate() - 1) / Date.getDaysInMonth( Date.today().getFullYear(), Date.today().getMonth() );
        });
        self.data.dynamic.SitePerformanceTrend_ProRatedBudgetTotals = ko.computed( function() {
            // for each site, returns a number similar to the total budget, but with the following modification:
            //   the current month's budget is multiplied by a factor representing the progress through the current month
            //   so that the VDPs that have not yet been collected for this month do not significantly affect the calculation of an average
            // for this to be valid, one must assume that VDPs are spread evenly throughout any given month.
            var data = self.services.trends.response();
            if( !data || !data.Trends )
                return null;
            var current_month_dt = new Date( Date.today().getFullYear(), Date.today().getMonth() );
            return _(data.Trends).objMap( function( site_data_array ) {
                return _(site_data_array).reduce( function( total_budget, data_point ) {
                    var budget = data_point.Budget;
                    if( current_month_dt.equals( self.fn.parseJSONPDate( data_point.DateRange.StartDate )))
                        budget *= self.data.dynamic.CurrentMonthProgressFactor();
                    return total_budget + budget;
                }, 0 );
            });
        });

        // chart data, all
        self.data.charts = {};

        function aggregate_performance_average_data_function_factory( data_accessor, numerator ) {
            return function() {
                var data = data_accessor();
                if( !data || !data.Totals )
                    return null;
                return {
                    series: [{
                        data: _.chain(data.Totals.Sites)
                            .map( function( site_totals, site_name ) {
                                return {
                                    name: site_name,
                                    color: self.VendorDefinition( site_name ).colors.blend,
                                    y: Math.round( site_totals[numerator] / site_totals.Months )
                                };
                            }).sortBy( function( mapped_data_point ) {
                                return mapped_data_point.y * -1;
                            }).value()
                    }]
                };
            };
        }
        self.data.charts.Overview_AvgSearches = ko.computed( 
            aggregate_performance_average_data_function_factory(
                self.data.processing.SitePerformanceTrend_Filtered, "SearchValue" ));

        self.data.charts.Overview_AvgConsumerVDPImpressions = ko.computed( 
            aggregate_performance_average_data_function_factory(
                self.data.processing.SitePerformanceTrend_Filtered, "DetailValue" ));

        self.data.charts.Overview_AvgTotalLeads = ko.computed( 
            aggregate_performance_average_data_function_factory(
                self.data.processing.SitePerformanceTrend_Filtered, "ActionValue" ));

        function aggregate_rate_data_function_factory( data_accessor, numerator ) {
            return function() {
                var data = data_accessor();
                if( !data || !data.Totals )
                    return null;
                return _(data.Totals.Sites).map( function( site_totals, site_name ) {
                    return {
                        name: site_name,
                        value: site_totals[numerator].toFixxed( 2 )
                    };
                });
            };
        }
        self.data.charts.Overview_ClickThruRate = ko.computed( 
            aggregate_rate_data_function_factory(
                self.data.processing.SitePerformanceTrend_Filtered, "ClickThroughRate" ));

        self.data.charts.Overview_ConversionRate = ko.computed(
            aggregate_rate_data_function_factory(
                self.data.processing.SitePerformanceTrend_Filtered, "ConversionRate" ));

        self.data.charts.Performance_AvgConsumerVDPImpressions = ko.computed(
            aggregate_performance_average_data_function_factory( 
                self.services.trends.response, "DetailValue" ));

        self.data.charts.Performance_AvgMonthlyCost = ko.computed( function() {
            var data = self.data.processing.SitePerformanceTrend_Filtered();
            if( !data || !data.Totals )
                return null;
            return _.chain(data.Totals.Sites)
                .map( function( site_totals, site_name ) {
                    return {
                        site_name: site_name,
                        budget: Math.floor( site_totals.Budget / site_totals.Months )
                    };
                }).sortBy( function( data_point ) {
                    return data_point.budget * -1;
                }).value();
        });

        self.data.charts.Performance_AvgCostPerConsumerVDPImpression = ko.computed( function() {
            /*
            var data = self.data.processing.SitePerformanceTrend_Filtered();
            var pro_rated_budgets = self.data.dynamic.SitePerformanceTrend_ProRatedBudgetTotals();
            if( !data || !data.Totals || !pro_rated_budgets )
                return null;
            return {
                series: [{
                    data: _.chain(data.Totals.Sites)
                        .map( function( site_totals, site_name ) {
                            var numerator__less_accurate = parseFloat(pro_rated_budgets[site_name].toFixxed(0));
                            var denominator__less_accurate = parseFloat(site_totals["DetailValue"].toFixxed(0));
                            return {
                                name: site_name,
                                color: self.VendorDefinition( site_name ).colors.blend,
                                y: (denominator__less_accurate > 0) ? (numerator__less_accurate / denominator__less_accurate) : 0
                            };
                        }).sortBy( function( mapped_data_point ) {
                            return mapped_data_point.y * -1;
                        }).value()
                }]
            }
            */
            var cost_data = self.data.charts.Performance_AvgMonthlyCost();
            var impressions_data = self.data.charts.Performance_AvgConsumerVDPImpressions();
            if (!cost_data || !impressions_data)
                return null;
            var chain = _.chain(impressions_data.series[0].data);
            var map = chain.map(function(impressions_data_point) {
                var cost_data_point = _(cost_data).find(function(cost_data_point) {
                    return cost_data_point.site_name == impressions_data_point.name;
                });
                if (!cost_data_point)
                    return undefined;
                var numerator__less_accurate = parseFloat((cost_data_point.budget).toFixxed(0));
                var denominator__less_accurate = parseFloat((impressions_data_point.y).toFixxed(0));
                return {
                    name: impressions_data_point.name,
                    y: ((denominator__less_accurate > 0) ? (numerator__less_accurate / denominator__less_accurate) : 0),
                    color: impressions_data_point.color
                };
            });
            var compact = map.compact();
            var values = compact.value();
            return {

                series: [{
                    data: values
                }]
            };
        });


        self.data.charts.Performance_ConsumerVDPImpressions_Benchmarks = ko.computed( function() {
            var data = self.data.processing.SitePerformanceTrend_Filtered();
            if( !data || !data.Totals )
                return null;
            return _(data.Totals.Sites).objMap( function( data_point ) {
                return data_point.BenchMarkImpressionCost;
            });
        });

        self.data.charts.Performance_AvgNumberDirectLeads = ko.computed(
            aggregate_performance_average_data_function_factory(
                self.data.dynamic.SitePerformanceTrend_Filtered_TotalDirectLeadsDynamic, "__TotalDirectLeadsDynamic" ));

        self.data.charts.Performance_AvgCostPerDirectLead = ko.computed( function() {
            // FB 27713 - note from Tyler Cole
            //   Displayed values are required be based on the already-presented screen values such that:
            //   [Avg Monthly Cost] / [Avg Number of Direct Leads] = [Avg Cost Per Direct Lead, per data source]
            //   respecting the data loss of rounding BEFORE performing the calculation (Yes! I know! It doesn't make sense to me either).
            //   Old Dashboard worked this way, so New Dashboard does too, but I disagree with it.
            
            /*
            // FB ??? - note from Tyler Cole
            //   Note: this does not make any logical sense (using SitePerformanceTrend_Filtered_TotalDirectLeadsDynamic)
            //   because it is not possible to know how to "split" the numerator (the budget) based on Direct Lead Type
            //   but we're going to do it anyway
            var data = self.data.dynamic.SitePerformanceTrend_Filtered_TotalDirectLeadsDynamic();
            // var data = self.data.processing.SitePerformanceTrend_Filtered();
            var pro_rated_budgets = self.data.dynamic.SitePerformanceTrend_ProRatedBudgetTotals();
            if( !data || !data.Totals || !pro_rated_budgets )
                return null;
            return {
                series: [{
                    data: _.chain(data.Totals.Sites)
                        .map( function( site_totals, site_name ) {
                            var numerator = pro_rated_budgets[site_name];
                            var denominator = site_totals["__TotalDirectLeadsDynamic"];
                            // var denominator = (site_totals["EmailValue"] + site_totals["PhoneValue"] + site_totals["ChatValue"]);
                            return {
                                name: site_name,
                                color: self.VendorDefinition( site_name ).colors.blend,
                                y: (denominator > 0 ? numerator / denominator : 0)
                            };
                        }).sortBy( function( mapped_data_point ) {
                            return mapped_data_point.y * -1;
                        }).value()
                }]
            }
            */

            /*
            var leads_data = self.data.dynamic.SitePerformanceTrend_Filtered_TotalDirectLeadsDynamic();
            var cost_data = self.data.processing.SitePerformanceTrend_Totals();
            var selected_month_count = self.virtualControls.dateRangeSelectedMonthCount();
            if( !leads_data || !cost_data )
                return null;
            return {
                series: [{
                    data: _.chain( leads_data.Totals.Sites )
                        .map( function( site_totals, site_name ) {
                            var numerator = cost_data[site_name].Budget;
                            var denominator = site_totals.__TotalDirectLeadsDynamic / selected_month_count;
                            var numerator__less_accurate = parseFloat(numerator.toFixxed(0));
                            var denominator__less_accurate = parseFloat(denominator.toFixxed(0));
                            return {
                                name: site_name,
                                color: self.VendorDefinition( site_name ).colors.blend,
                                y: (denominator__less_accurate > 0) ? (numerator__less_accurate / denominator__less_accurate) : 0
                            };
                        }).sortBy( function( mapped_data_point ) {
                            return mapped_data_point.y * -1;
                        }).value()
                }]
            }
            */

            var cost_data = self.data.charts.Performance_AvgMonthlyCost();
            var leads_data = self.data.charts.Performance_AvgNumberDirectLeads();
            if( !cost_data || !leads_data )
                return null;
            return {
                series: [{
                    data: _(leads_data.series[0].data).map( function( leads_data_point ) {
                        var cost_data_point = _(cost_data).find( function( cost_data_point ) {
                            return cost_data_point.site_name == leads_data_point.name;
                        });
                        var numerator__less_accurate = parseFloat( (cost_data_point.budget).toFixxed( 0 ));
                        var denominator__less_accurate = parseFloat( (leads_data_point.y).toFixxed( 0 ));
                        return {
                            name: leads_data_point.name,
                            y: ((denominator__less_accurate > 0) ? (numerator__less_accurate / denominator__less_accurate) : 0),
                            color: leads_data_point.color
                        };
                    })
                }]
            };
        });

        self.data.charts.Performance_DirectLeads_Benchmarks = ko.computed( function() {
            var data = self.data.processing.SitePerformanceTrend_Filtered();
            if( !data || !data.Totals )
                return null;
            return _(data.Totals.Sites).objMap( function( data_point ) {
                return data_point.BenchMarkLeadCost;
            });
        });

        function aggregate_trends_data_function_factory( data_accessor, numerator, denominator ) {
            return function() {
                var data = data_accessor();
                if( !data || !data.Trends )
                    return null;
                var marker = { symbol: "circle" };
                return {
                    series: _(data.Trends).map( function( site_data_array, site_name ) {
                        return {
                            name: site_name,
                            color: self.VendorDefinition( site_name ).colors.hex,
                            data: _.chain( site_data_array )
                                .filter( function( raw_data_point ) {
                                    if( raw_data_point[numerator] === 0 )
                                        return false;
                                    if( denominator && raw_data_point[denominator] === 0 )
                                        return false;
                                    return true;
                                }).map( function( raw_data_point ) {
                                    var x = self.fn.parseJSONPDate( raw_data_point.DateRange.StartDate ).getTime();
                                    var y = raw_data_point[numerator];
                                    if( denominator )
                                        y = parseFloat((y / raw_data_point[denominator]).toFixed(2));
                                    return [ x, y ];
                                }).sortBy( function( mapped_data_point ) {
                                    return mapped_data_point[0]; // sort by x
                                }).value(),
                            marker: marker
                        };
                    })
                };
            };
        }
        self.data.charts.Trends_ConsumerVDPImpressions = ko.computed(
            aggregate_trends_data_function_factory(
                self.services.trends.response,
                "DetailPageViewCount", null ));
        
        self.data.charts.Trends_CostPerConsumerVDPImpression = ko.computed(
            aggregate_trends_data_function_factory(
                self.services.trends.response,
                "Budget", "DetailPageViewCount" ));
        
        self.data.charts.Trends_DirectLeads = ko.computed(
            aggregate_trends_data_function_factory(
                self.data.dynamic.SitePerformanceTrend_DirectLeadsDynamic,
                "__DirectLeadsDynamic", null ));
        
        self.data.charts.Trends_CostPerDirectLead = ko.computed(
            aggregate_trends_data_function_factory(
                self.data.dynamic.SitePerformanceTrend_DirectLeadsDynamic,
                "Budget", "__DirectLeadsDynamic" ));


        function sum_of_fields_in_list( d, f ) {
            return _(d).reduce( function( m, v, k ) {
                if( _(f).contains( k ))
                    m += v;
                return m;
            }, 0 );
        }
        
        self.data.charts.Trends_BudgetChanges = ko.computed( function() {
            var data = self.services.trends.response();
            if( !data || !data.Trends )
                return null;
            var field_list = ["DetailPageViewCount","TotalLeads"];
            var result = {};
            // [pseudocode summary]
            // for every dealer listing site,
            //   for every month with data in the given range,
            //     if there is at least one non-zero data point from the given field list,
            //     and that data point also has a non-zero budget value,
            //     and the following month meets the same criteria,
            //     and the budget values for the two months differs
            //       THEN add a "budget change" object to the result.
            _.each( data.Trends, function( site_data, site_name ) {
                for( var i=1; i<site_data.length; ++i ) {
                    var f0 = sum_of_fields_in_list( site_data[i-1], field_list );
                    var f1 = sum_of_fields_in_list( site_data[i], field_list );
                    if( f0 === 0 || f1 === 0 )
                        continue; // not data for both months
                    var b0 = site_data[i-1].Budget,
                        b1 = site_data[i].Budget;
                    if( b0 === 0 || b1 === 0 )
                        continue; // budget not set for both months
                    if( b0 != b1 ) {
                        // budget changed and all criteria passed for month-pair being compared
                        var x = self.fn.parseJSONPDate( site_data[i].DateRange.StartDate ).getTime();
                        var value = {
                            time_value: x,
                            site_name: site_name,
                            prev_value: b0,
                            new_value: b1,
                            delta: b1 - b0
                        };
                        if( !result[x] )
                            result[x] = [];
                        result[x].push( value );
                    }
                }
            });
            return result;
        });
        self.data.charts.styles = {};
        self.data.charts.styles.MarketingTraffic = {
            "Digital Showroom": {
                lineWidth: 1,
                color: "#FEA029",
                visible: false,
                legendIndex: 2
            },
            "Mobile Showroom": {
                lineWidth: 1,
                color: "#37B546",
                visible: false,
                legendIndex: 3
            },
            "Overall": {
                lineWidth: 4,
                color: "#3761B5",
                legendIndex: 1
            }
        };
        self.data.charts.MarketingTrafficLinks = {
            'Digital Showroom': 'Mobile Showroom',
            'Mobile Showroom': 'Digital Showroom'
        };
        self.data.charts.MarketingTraffic = ko.computed(function() {
            if (!self.dynamic_config.hasShowroomReporting || !self.dynamic_config.hasDigitalShowroom) return {};
            var data = self.services.marketing.response();
            if (!data)
                return null;
            var marker = { symbol: "circle" };
            var result = { series: [] };
            var dataSet = {
                id: "Overall",
                name: "Overall",
                marker: marker,
                data: []
            };
            $.extend(true, dataSet, self.data.charts.styles.MarketingTraffic["Overall"]);
            $.each(data.Combined, function(index, dateValue) {
                var x = new Date(dateValue.Time).getTime(); // highcharts doesn't like javascript dates? why.
                var y = dateValue.Count;
                dataSet.data.push([x, y]);
            });
            result.series.push(dataSet);

            dataSet = {
                id: "Digital Showroom",
                name: "Digital Showroom",
                marker: marker,
                data: []
            };
            $.extend(true, dataSet, self.data.charts.styles.MarketingTraffic["Digital Showroom"]);
            $.each(data.DigitalShowroom, function(index, dateValue) {
                var x = new Date(dateValue.Time).getTime(); // highcharts doesn't like javascript dates? why.
                var y = dateValue.Count;
                dataSet.data.push([x, y]);
            });
            result.series.push(dataSet);

            dataSet = {
                id: "Mobile Showroom",
                name: "Mobile Showroom",
                marker: marker,
                data: []
            };
            $.extend(true, dataSet, self.data.charts.styles.MarketingTraffic["Mobile Showroom"]);
            $.each(data.MobileShowroom, function(index, dateValue) {
                var x = new Date(dateValue.Time).getTime(); // highcharts doesn't like javascript dates? why.
                var y = dateValue.Count;
                dataSet.data.push([x, y]);
            });
            result.series.push(dataSet);

            return result;
        });
        self.data.charts.Website20Pageviews = ko.computed(function() {
            if (!self.dynamic_config.hasShowroomReporting || !self.dynamic_config.hasWebsite20) return {};
            var data = self.services.website20traffic.response();
            if (!data)
                return null;
            var columns = [];
            var top = data.PageViews;
            
            var column = {
                name: "",
                color: self.static_config.styles.YellowColumnGradient,
                y: top
            }
            columns.push(column);
            
            return {
                series: [{
                    data: columns
                }] 
            };
        });
        self.data.charts.Website20Engagement = ko.computed(function() {
            if (!self.dynamic_config.hasShowroomReporting || !self.dynamic_config.hasWebsite20) return {};
            var data = self.services.website20traffic.response();
            if (!data)
                return null;
            var columns = [];
            var ms = data.AvgTimeOnPageMilliseconds;
            var column = {
                name: "",
                color: self.static_config.styles.BlueColumnGradient,
                y: ms
            }
            columns.push(column);
            return {
                series: [{
                    data: columns
                }] 
            };
        });
        self.data.charts.Website20Leads = ko.computed(function() {
            if (!self.dynamic_config.hasShowroomReporting || !self.dynamic_config.hasWebsite20) return {};
            var data = self.services.website20traffic.response();
            if (!data)
                return null;
            var columns = [];
            var totalDirectLeads = data.TotalDirectLeads;
            var totalActions = data.TotalActions;
            
            columns.push({
                name: "TOTAL DIRECT LEADS",
                color: self.static_config.styles.BlueColumnGradient,
                y: totalDirectLeads
            });
            columns.push({
                name: "TOTAL ACTIONS",
                color: self.static_config.styles.YellowColumnGradient,
                y: totalActions
            });
            return {
                series: [{
                    data: columns
                }] 
            };
        });
        self.data.charts.WebsiteTraffic_TrafficTrends = ko.computed( function() {
            var data = self.services.ga_trends.response();
            if( !data )
                return null;
            // apply a conditional grouping of raw visits data to emulate the Google Analytics "Acquisition > Channels" report
            var marker = { symbol: "circle" };
            var result = { series: [] };
            var name_map = self.static_config.GA_Trends__AcquisitionChannels_NameMapping;
            _(data).each( function( ga_data ) {
                if( !ga_data || !ga_data.Referrals || !ga_data.Referrals.Trends || !ga_data.Referrals.Trends.Rows )
                    return; // nothing to process
                _(ga_data.Referrals.Trends.Rows).each( function( row, key ) { // null ref exception
                    var medium_socialNetwork = key.split("|");
                    var medium = medium_socialNetwork[0];
                    var socialNetwork = medium_socialNetwork[1];
                    var lookup_key = medium.toLowerCase();
                    if( medium == "referral" ) {
                        if( socialNetwork == "(not set)" )
                            lookup_key = "referral__non_social";
                        else
                            lookup_key = "referral__social";
                    }
                    var series_name = name_map[lookup_key];
                    if( !series_name && window.console ) {
                        console.log("unrecognized:  " + medium_socialNetwork);
                        return; // do not display
                    }
                    var series = _(result.series).find( function( s ) { return s.name == series_name; });
                    if( !series ) { // add it
                        series = {
                            name: series_name,
                            color: undefined,
                            marker: marker,
                            data: []
                        };
                        result.series.push( series );
                    }
                    var x = self.fn.parseJSONPDate( ga_data.StartDate ).getTime();
                    var y = row.Visits;
                    var data_point = _(series.data).find( function( d ) { return d[0] == x; });
                    if( !data_point ) { // medium not enountered yet for this month
                        series.data.push([ x, y ]); 
                    } else { // month has data already for this medium; sum the values (mostly applies to Social Referrals)
                        data_point.y += y;
                    }
                });
            });
            _(result.series).each( function( series ) {
                _(series.data).sortBy( function( item ) {
                    return item[0]; // x
                });
            });
            return result;
        });

        self.data.charts.WebsiteTraffic_TrafficSources = ko.computed( function() {
            var data = self.services.ga.response();
            if( !data || !data.Referrals || !data.Referrals.Sources )
                return null;
            var totalVisits = _(data.Referrals.Sources.Rows).reduce( function( tally, data_point ) {
                return tally + data_point.Visits;
            }, 0 );
            if( totalVisits === 0 )
                return null;
            return {
                series: [{
                    data: _.chain(data.Referrals.Sources.Rows)
                        .map( function( data_point, name ) {
                            return {
                                name: self.fn.format_GA_source_medium(name, 18),
                                __longname: self.fn.format_GA_source_medium(name),
                                color: self.VendorDefinition("Dealer Website").colors.blend,
                                y: (data_point.Visits / totalVisits) * 100
                            };
                        })
                        .sortBy( function( mapped_data_point ) {
                            return mapped_data_point.y * -1;
                        })
                        .take( 10 )
                        .value()
                }]
            };
        });

        self.data.charts.WebsiteTraffic_MobileTraffic = ko.computed( function() {
            var data = self.services.ga.response();
            if( !data || !data.Referrals )
                return null;
            var mobile_pct = (data.Referrals.Mobile.TotalsForAllResults.Visits / data.Referrals.Sources.TotalsForAllResults.Visits) * 100;
            return {
                series: [{
                    data: [{
                        name: "Mobile",
                        y: mobile_pct,
                        color: {
                            radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
                            stops: [[0, "#00c217"], [1, "#018f11"]]
                        },
                    },{
                        name: "Desktop/Laptop",
                        y: (100 - mobile_pct),
                        color: {
                            radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
                            stops: [[0, '#00a8ee'], [1, '#017fb4']]
                        }
                    }]
                }]
            };
        });

        function format_field_average( data, field ) {
            var total = _(data).reduce( function( tally, data_point ) {
                return tally + data_point[field];
            }, 0 );
            var average = total / data.length;
            return number_format( average, 1 );
        }

        self.data.charts.TimeToMarket = ko.computed( function() {
            var data = self.services.ttm.response();
            if( !data )
                return null;
            return {
                series: [{
                    name: "Days to Vehicle Online With Photo: " +
                        "<span style='font-weight:bold;color:#2f7ed8'>"+number_format( data.AverageDaysToPhotoOnline, 1 )+"</span>" +
                        " Days Avg",
                    data: _(data.Days).map( function( data_point ) {
                        return {
                            x: new Date( data_point.Date ).getTime(),
                            y: data_point.AvgElapsedDaysPhoto
                        };
                    })
                },{
                    name: "Days to Complete Ad Online: " +
                        "<span style='font-weight:bold;color:#0d233a'>"+number_format( data.AverageDaysToCompleteAd, 1 )+"</span>" +
                        " Days Avg",
                    data: _(data.Days).map( function( data_point ) {
                        return {
                            x: new Date( data_point.Date ).getTime(),
                            y: data_point.AvgElapsedDaysAdComplete
                        };
                    })
                }]
            };
        });

        // chart UI wrappers
        //   TODO: no data message
        //   TODO: no budget message for "cost per" charts
        self.CreateChart({
            id: "Overview_AvgSearches",
            type: SimpleColumnChart,
            container_selector: "#chart_stats_searches",
            data_observable: self.data.charts.Overview_AvgSearches,
            highcharts_settings: {
                chart: { width: 190, height: 256 },
                title: { text: "AVG SEARCHES" }
            }
        });
        self.CreateChart({
            id: "Overview_AvgConsumerVDPImpressions",
            type: SimpleColumnChart,
            container_selector: "#chart_stats_pages",
            data_observable: self.data.charts.Overview_AvgConsumerVDPImpressions,
            highcharts_settings: {
                chart: { width: 190, height: 256 },
                title: { text: "AVG CONSUMER (VDP) IMPRESSIONS" }
            }
        });
        self.CreateChart({
            id: "Overview_AvgTotalLeads",
            type: SimpleColumnChart,
            container_selector: "#chart_stats_actions",
            data_observable: self.data.charts.Overview_AvgTotalLeads,
            highcharts_settings: {
                chart: { width: 190, height: 256 },
                title: { text: "AVG TOTAL LEADS" }
            }
        });

        self.CreateChart({
            id: "Performance_AvgConsumerVDPImpressions",
            type: SquatColumnChart,
            container_selector: "#chart_cv_chart",
            data_observable: self.data.charts.Performance_AvgConsumerVDPImpressions,
            highcharts_settings: {
                chart: { width: 232, height: 190 },
                title: { text: "Avg Consumer (VDP) Impressions" }
            }
        });
       
        var missing_budget_message_options = {
            series: {
                dataLabels: {
                    formatter: function() {
                        if( !(this.y > 0) )
                            return "Please set your<br /><a href='"+self.virtualControls.budgetLink()+"'>monthly cost</a><br />to see this data.";
                    },
                    enabled: true,
                    useHTML: true,
                    align: "left",
                    x: 5,
                    y: 5,
                    style: {
                        fontWeight: "normal",
                        fontSize: "10px",
                        lineHeight: "12px"
                    }
                }
            }
        };
        self.CreateChart({
            id: "Performance_AvgCostPerConsumerVDPImpression",
            type: BarChartWithBenchmarks,
            container_selector: "#chart_cpv_chart",
            data_observable: self.data.charts.Performance_AvgCostPerConsumerVDPImpression,
            highcharts_settings: {
                chart: { width: 232, height: 190 },
                title: { text: "Avg Cost Per Consumer (VDP) Impression" },
                plotOptions: missing_budget_message_options
            },
            extensions: {
                benchmarks_observable: self.data.charts.Performance_ConsumerVDPImpressions_Benchmarks
            }
        });
        self.CreateChart({
            id: "Performance_AvgNumberDirectLeads",
            type: SquatColumnChart,
            container_selector: "#chart_act_chart",
            data_observable: self.data.charts.Performance_AvgNumberDirectLeads,
            highcharts_settings: {
                chart: { width: 232, height: 161 },
                title: { text: "Avg Number of Direct Leads" }
            }
        });
        self.CreateChart({
            id: "Performance_AvgCostPerDirectLead",
            type: BarChartWithBenchmarks,
            container_selector: "#chart_cpa_chart",
            data_observable: self.data.charts.Performance_AvgCostPerDirectLead,
            highcharts_settings: {
                chart: { width: 232, height: 161 },
                title: { text: "Avg Cost Per Direct Lead" },
                plotOptions: missing_budget_message_options
            },
            extensions: {
                benchmarks_observable: self.data.charts.Performance_DirectLeads_Benchmarks
            }
        });

        self.CreateChart({
            id: "Trends_ConsumerVDPImpressions",
            type: LineChartWithBudgetChanges,
            container_selector: "#chart_vdpt_chart",
            data_observable: self.data.charts.Trends_ConsumerVDPImpressions,
            highcharts_settings: {
                chart: { width: 484, height: 236 }
            },
            extensions: {
                budget_changes_observable: self.data.charts.Trends_BudgetChanges
            }
        });
        self.CreateChart({
            id: "Trends_CostPerConsumerVDPImpression",
            type: LineChartWithBudgetChanges,
            container_selector: "#chart_costper_vdp_trend",
            data_observable: self.data.charts.Trends_CostPerConsumerVDPImpression,
            highcharts_settings: {
                chart: { width: 484, height: 236 }
            },
            extensions: {
                budget_changes_observable: self.data.charts.Trends_BudgetChanges,
                currency_mode: true
            }
        });
        self.CreateChart({
            id: "Trends_DirectLeads",
            type: LineChartWithBudgetChanges,
            container_selector: "#chart_lead_trend",
            data_observable: self.data.charts.Trends_DirectLeads,
            highcharts_settings: {
                chart: { width: 484, height: 197 }
            },
            extensions: {
                budget_changes_observable: self.data.charts.Trends_BudgetChanges
            }
        });
        self.CreateChart({
            id: "Trends_CostPerDirectLead",
            type: LineChartWithBudgetChanges,
            container_selector: "#chart_costper_lead_trends",
            data_observable: self.data.charts.Trends_CostPerDirectLead,
            highcharts_settings: {
                chart: { width: 484, height: 197 }
            },
            extensions: {
                budget_changes_observable: self.data.charts.Trends_BudgetChanges,
                currency_mode: true
            }
        });
        self.CreateChart({
            id: "MarketingTraffic",
            type: LineChartWithInitiallyHiddenSeriesDetailed,
            container_selector: "#chart_dspvt",
            data_observable: self.data.charts.MarketingTraffic,
            highcharts_settings: {
                chart: { width: 484, height: 236 },
                tooltip: {
                    enabled: true,
                    formatter: function() {
                        return '<span style="font-size:0.8em">'+(new Date(this.x)).toString('MMMM yyyy')+'</span><br>'+
                            '<span style="color:'+this.series.color+'">'+this.series.name+'</span>: <b>'+number_format( this.y, 0 )+' Visits</b><br/>';
                    }
                },
                plotOptions: {
                    series: {
                        events: {
                            legendItemClick: function(event) {
                                var link = self.data.charts.MarketingTrafficLinks[this.name];
                                if (link === undefined) return;
                                var linkedSeries = this.chart.get(link);
                                if (linkedSeries !== null) {
                                    if (this.visible) {
                                        linkedSeries.hide();
                                    } else {
                                        linkedSeries.show();
                                    }
                                }
                            }
                        }
                    }
                }
            },
            extensions: {
                show_only_top: 4
            }
        });

        self.CreateChart({
            id: "Website20Engagement",
            type: SquatColumnChart,
            container_selector: "#chart_wsengagement",
            data_observable: self.data.charts.Website20Engagement,
            highcharts_settings: {
                plotOptions: {
                    series: {
                        pointWidth: 60,
                        dataLabels: {
                            enabled: true,
                            formatter: function() {
                                return msToTime(this.y);
                            }
                        }
                    }
                },
                xAxis: { labels: { enabled: false }},
                yAxis: { min: 0, minRange: 5 * 60 * 1000 }, /* 5 minute engagement span minimum */
                chart: { width: 242, height: 236 },
                title: { text: 'CONSUMER ENGAGEMENT', margin: 20 },
                subtitle: { text: '(Avg Time on Page)'}
            }
        });
        
        self.CreateChart({
            id: "Website20Pageviews",
            type: SquatColumnChart,
            container_selector: "#chart_wspageviews",
            data_observable: self.data.charts.Website20Pageviews,
            highcharts_settings: {
                plotOptions: {
                    series: {
                        pointWidth: 60
                    }
                },
                xAxis: { labels: { enabled: false }},
                yAxis: { min: 0, minRange: 50 },
                chart: { width: 242, height: 236 },
                title: { text: 'TOTAL PAGE VIEWS', margin: 20}
            }
        });
        self.CreateChart({
            id: "Website20Leads",
            type: SquatColumnChart,
            container_selector: "#chart_wsleads",
            data_observable: self.data.charts.Website20Leads,
            highcharts_settings: {
                plotOptions: {
                    series: {
                        pointWidth: 68
                    }
                },
                xAxis: { labels: {
                    style: {
                        "fontWeight": "bold",
                        "fontSize": "11px"
                    }
                }},
                yAxis: { min: 0, minRange: 0.1 },
                chart: { width: 484, height: 236, marginLeft: 50, marginRight: 50, marginTop: 25 },
                title: { text: '', margin: 0 }
            }
        });
        self.CreateChart({
            id: "WebsiteTraffic_TrafficTrends",
            type: LineChartWithInitiallyHiddenSeries,
            container_selector: "#chart_ga_trends",
            data_observable: self.data.charts.WebsiteTraffic_TrafficTrends,
            highcharts_settings: {
                chart: { width: 484, height: 197 },
                tooltip: {
                    enabled: true,
                    formatter: function() {
                        return '<span style="font-size:0.8em">'+(new Date(this.x)).toString('MMMM yyyy')+'</span><br>'+
                            '<span style="color:'+this.series.color+'">'+this.series.name+'</span>: <b>'+number_format( this.y, 0 )+' Visits</b><br/>';
                    }
                }
            },
            extensions: {
                show_only_top: 4
            }
        });
        self.CreateChart({
            id: "WebsiteTraffic_TrafficSources",
            type: HangingLabelColumnChart,
            container_selector: "#chart_ga_sources",
            data_observable: self.data.charts.WebsiteTraffic_TrafficSources,
            highcharts_settings: {
                chart: { 
                    width: 484, 
                    height: 210,
                    events: {
                        click: function() {
                            self.reports.WebsiteTraffic_KeywordsOrganic.show_popup();
                        }
                    }
                },
                tooltip: {
                    formatter: function () {
                        return number_format( this.y, (this.y<1?1:0) ) +
                            "% of traffic to your dealership<br /> website came from " +
                            this.point.__longname+"." +
                            '<br /> <a href="#">Click here</a> to see organic keywords.';
                    }
                }
            }
        });
        self.CreateChart({
            id: "WebsiteTraffic_MobileTraffic",
            type: PieChart,
            container_selector: "#chart_ga_mobile",
            data_observable: self.data.charts.WebsiteTraffic_MobileTraffic,
            highcharts_settings: {
                chart: { width: 484, height: 210 },
                tooltip: {
                    formatter: function() {
                        return number_format( this.y, ((this.y<1)?1:0) ) +
                            "% of traffic to your dealership<br /> website came from a " +
                            this.point.name+" device.";
                    }
                }
            }
        });

        self.CreateChart({
            id: "TimeToMarket",
            type: LineChartDynamicLegend,
            container_selector: "#chart_ttm",
            data_observable: self.data.charts.TimeToMarket,
            highcharts_settings: {
                chart: { width: 484, height: 250 },
                title: { text: "Time to Market" },
                legend: {
                    useHTML: true
                },
                plotOptions: {
                    series: {
                        events: {
                            legendItemClick: function () { return false; }
                        }
                    }
                }
            }
        });

        // report data, all
        self.data.reports = {};

        function aggregate_performance_data_pivoted_by_month_function_factory( data_accessor ) {
            return function() {
                var data = data_accessor();
                if( !data || !data.Trends )
                    return null;
                var result = [];
                _(data.Trends).each( function( site, site_name ) {
                    _(site).each( function( raw_data_point ) {
                        var month = self.fn.parseJSONPDate( raw_data_point.DateRange.StartDate );
                        var row = _(result).find( function( row ) {
                            return row.month.getTime() == month.getTime();
                        });
                        if( !row ) {
                            row = {
                                month: month,
                                sites: {}
                            };
                            result.push( row );
                        }
                        row.sites[site_name] = raw_data_point;
                    });
                });
                return result;
            };
        }

        self.data.reports.SitePerformanceTrend_Pivot_byMonth = ko.conditionallyComputed(
            aggregate_performance_data_pivoted_by_month_function_factory(
                self.services.trends.response ), self.data.throttle.enable_reports );

        self.data.reports.SitePerformanceTrend_Filtered_Pivot_byMonth = ko.conditionallyComputed(
            aggregate_performance_data_pivoted_by_month_function_factory(
                self.data.processing.SitePerformanceTrend_Filtered ), self.data.throttle.enable_reports );

        // used for the single-column-per-site reports (3x)
        function aggregate_simple_trends_report_data_function_factory( data_accessor, numerator, denominator ) {
            return function() {
                var data = data_accessor();
                if( !data )
                    return null;
                var result = {
                    cols: ["month"], // +1 column per site
                    rows: []
                };
                // add a dynamic column for each site that appears in the data
                _(data).each( function( data_row ) {
                    _(data_row.sites).each( function( site, site_name ) {
                        if( !_(result.cols).contains( site_name ))
                            result.cols.push( site_name );
                    });
                });
                // compose and push report rows
                result.rows = _(data).map( function( data_row ) {
                    return _(result.cols).map( function( col ) {
                        if( col == "month" ) {
                            return data_row.month;
                        } else { // site name
                            var site_data = data_row.sites[col];
                            if( !site_data )
                                return null;
                            if( !denominator )
                                return site_data[numerator];
                            else
                                return (site_data[denominator] !== 0)? (site_data[numerator] / site_data[denominator]) : 0;
                        }
                    });
                });
                return result;
            };
        }

        self.data.reports.Trends_ConsumerVDPImpressions = ko.conditionallyComputed( 
            aggregate_simple_trends_report_data_function_factory(
                self.data.reports.SitePerformanceTrend_Pivot_byMonth,
                "DetailPageViewCount", null ), self.data.throttle.enable_reports );

        self.data.reports.Trends_CostPerConsumerVDPImpression = ko.conditionallyComputed( 
            aggregate_simple_trends_report_data_function_factory(
                self.data.reports.SitePerformanceTrend_Filtered_Pivot_byMonth,
                "Budget", "DetailPageViewCount" ), self.data.throttle.enable_reports );

        self.data.reports.Trends_DirectLeads = ko.conditionallyComputed( function() {
            var data = self.data.processing.SitePerformanceTrend_Filtered();
            if( !data )
                return null;
            // one set of full table data associated with the string to display as the tab name
            return _(data.Trends).objMap( function( site_data_array ) {
                return {
                    cols: [
                        "month",
                        "phone",
                        "email",
                        "chat",
                        "total"
                    ],
                    rows: _(site_data_array).map( function( raw_data_point ) {
                        return [
                            self.fn.parseJSONPDate( raw_data_point.DateRange.StartDate ),
                            raw_data_point.PhoneLeadsCount,
                            raw_data_point.EmailLeadsCount,
                            raw_data_point.ChatRequestsCount,
                            (raw_data_point.PhoneLeadsCount + raw_data_point.EmailLeadsCount + raw_data_point.ChatRequestsCount)
                        ];
                    })
                };
            });
        }, self.data.throttle.enable_reports );

        self.data.reports.Trends_CostPerDirectLead = ko.conditionallyComputed( 
            aggregate_simple_trends_report_data_function_factory(
                self.data.reports.SitePerformanceTrend_Filtered_Pivot_byMonth,
                "Budget", "TotalLeads" ), self.data.throttle.enable_reports );

        function aggregate_analytics_report_data_function_factory( referral_category, key_fmt_fn ) {
            return function() {
                var data = self.services.ga.response();
                if( !data )
                    return null;
                var totalVisits = _(data.Referrals[referral_category].Rows).reduce( function( tally, data_point ) {
                    return tally + data_point.Visits;
                }, 0 );
                if( totalVisits === 0 )
                    return null;
                return {
                    cols: [
                        "row_key",
                        "pct_visits",
                        "visits",
                        "pages",
                        "duration",
                        "pct_new",
                        "bounce_pct"
                    ],
                    rows: _(data.Referrals[referral_category].Rows).map( function( data_point, key ) {
                        return [
                            (key_fmt_fn? key_fmt_fn(key):key),
                            (data_point.Visits / totalVisits) * 100,
                            data_point.Visits,
                            data_point.PageviewsPerVisit,
                            data_point.AverageVisitDuration,
                            data_point.PercentageNew,
                            data_point.VisitBounceRate
                        ];
                    })
                };
            };
        }

        self.data.reports.WebsiteTraffic_TrafficSources = ko.conditionallyComputed( 
            aggregate_analytics_report_data_function_factory(
                "Sources", self.fn.format_GA_source_medium ), self.data.throttle.enable_reports );

        self.data.reports.WebsiteTraffic_MobileTraffic = ko.conditionallyComputed( 
            aggregate_analytics_report_data_function_factory( 
                "Mobile", null ), self.data.throttle.enable_reports );

        self.data.reports.WebsiteTraffic_KeywordsOrganic = ko.conditionallyComputed(
            aggregate_analytics_report_data_function_factory(
                "Keywords", null ), self.data.throttle.enable_reports );

        self.data.reports.WebsiteTraffic_KeywordsOrganic_DynamicFilterStats = ko.conditionallyComputed({
            read: function() {
                // requires deferred evaluation due to direct dependency on non-observable report wrapper object 
                var ga_data = self.services.ga.response();
                var data = self.reports.WebsiteTraffic_KeywordsOrganic.data ? self.reports.WebsiteTraffic_KeywordsOrganic.data() : null;
                var filtered_rows = self.reports.WebsiteTraffic_KeywordsOrganic.filtered_row_data();
                if( !ga_data || !data || !filtered_rows )
                    return null; // not enough data
                var visits_col_idx = _(data.cols).indexOf( "visits" );
                var filtered_visits = _(filtered_rows).reduce( function( tally, row ) {
                    return tally + row[visits_col_idx];
                }, 0 );
                var total_organic_visits = ga_data.Referrals.Keywords.TotalsForAllResults.Visits;
                var total_visits = ga_data.Referrals.Sources.TotalsForAllResults.Visits;
                return '<span class="filterable_report_output_value">'+number_format( (100 * (filtered_visits/total_organic_visits)), 0 )+'</span> <span class="filterable_report_output_label_minor">% of Organic Visits</span> '+
                    '<span class="filterable_report_output_value">'+number_format( (100 * (filtered_visits/total_visits)), 0 )+'</span> <span  class="filterable_report_output_label_minor">% of Total Visits</span>';
            },
            deferEvaluation: true
        }, self.data.throttle.enable_reports );

        
        function internal_vehicle_link_html( vehicle_data ) {
            // BUGZID: 26724 - no URL for inactive inventory, tureen 10/23/2013
            var sReturnURL ;

            if (vehicle_data.InventoryActive != 0) {
                sReturnURL = '<a href="/merchandising/Workflow/RedirectToInventoryItem.aspx'+
                                '?bu='+vehicle_data.BusinessUnitId+
                                '&inv='+vehicle_data.InventoryId+
                                '">'+vehicle_data.YearMakeModelTrim+'</a>';
                }
            else {
                    sReturnURL = vehicle_data.YearMakeModelTrim;
                }
    

            return sReturnURL;
         
         
            //return '<a href="/merchandising/Workflow/RedirectToInventoryItem.aspx'+
            //'?bu='+vehicle_data.BusinessUnitId+
            //'&inv='+vehicle_data.InventoryId+
            //'">'+vehicle_data.YearMakeModelTrim+'</a>';
        }

        self.data.reports.TimeToMarket = ko.conditionallyComputed( function() {
            var data = self.services.ttm.response();
            if( !data )
                return null;
            return {
                cols: [
                    "vehicle",
                    "date_received",
                    "date_online_photos",
                    "days_photos",
                    "days_complete",
                    "stock_number"
                ],
                rows: _(data.Vehicles).map( function( data_point ) {
                    return [
                        internal_vehicle_link_html( data_point ),
                        new Date( data_point.DateAddedToInventory ),
                        new Date( data_point.DateOfFirstPhoto ),
                        data_point.ElapsedDaysPhoto,
                        data_point.ElapsedDaysCompleteAd,
                        data_point.StockNumber
                    ];
                })
            };
        }, self.data.throttle.enable_reports );


        // report UI wrappers
        self.CreateReport({
            id: "Trends_ConsumerVDPImpressions",
            container_selector: "#vdp_trend_report",
            data_observable: self.data.reports.Trends_ConsumerVDPImpressions,
            column_meta: {
                "__default": { display: "$key VDP", data_format: "number.integer" },
                "month":     { display: "Month",    data_format: "date.month"     }
            }
        });
        self.CreateReport({
            id: "Trends_CostPerConsumerVDPImpression",
            container_selector: "#costper_vdp_trend_report",
            data_observable: self.data.reports.Trends_CostPerConsumerVDPImpression,
            column_meta: {
                "__default": { display: "Cost per $key VDP", data_format: "number.currency" },
                "month":     { display: "Month",             data_format: "date.month"      }
            }
        });
        self.CreateReport({
            id: "Trends_DirectLeads",
            container_selector: "#lead_trend_report",
            data_observable: self.data.reports.Trends_DirectLeads,
            tabbed: true,
            column_meta: {
                "month": { display: "Month", data_format: "date.month" },
                "phone": { display: "Phone", data_format: "number.integer" },
                "email": { display: "Email", data_format: "number.integer" },
                "chat":  { display: "Chat",  data_format: "number.integer" },
                "total": { display: "Total", data_format: "number.integer" }
            }
        });
        self.CreateReport({
            id: "Trends_CostPerDirectLead",
            container_selector: "#costper_lead_trends_report",
            data_observable: self.data.reports.Trends_CostPerDirectLead,
            column_meta: {
                "__default": { display: "Cost per $key Lead", data_format: "number.currency" },
                "month":     { display: "Month",              data_format: "date.month"      }
            }
        });

        // self.CreateReport({
        //     id: "WebsiteTraffic_TrafficTrends",
        //     container_selector: "#ga_trends_report",
        //     data_observable: null,
        //     column_meta: {}
        // });
        self.CreateReport({
            id: "WebsiteTraffic_TrafficSources",
            container_selector: "#ga_sources_report",
            data_observable: self.data.reports.WebsiteTraffic_TrafficSources,
            column_meta: {
                "row_key":    { display: "Source / Medium",  css: "text_align_left" },
                "pct_visits": { display: "% Visits",           data_format: "number.percent" },
                "visits":     { display: "Visits",             data_format: "number.integer" },
                "pages":      { display: "Pages Per Visit",    data_format: "number.float.2" },
                "duration":   { display: "Avg Visit Duration", data_format: "timespan.hours" },
                "pct_new":    { display: "% New Visits",       data_format: "number.percent.unchecked" },
                "bounce_pct": { display: "Bounce Rate",        data_format: "number.percent.unchecked" }
            },
            sort: { idx:1, dir:"desc" }
        });
        self.CreateReport({
            id: "WebsiteTraffic_MobileTraffic",
            container_selector: "#ga_mobile_report",
            data_observable: self.data.reports.WebsiteTraffic_MobileTraffic,
            column_meta: {
                "row_key":    { display: "Device",   css: "text_align_left" },
                "pct_visits": { display: "% Visits",           data_format: "number.percent" },
                "visits":     { display: "Visits",             data_format: "number.integer" },
                "pages":      { display: "Pages Per Visit",    data_format: "number.float.2" },
                "duration":   { display: "Avg Visit Duration", data_format: "timespan.hours" },
                "pct_new":    { display: "% New Visits",       data_format: "number.percent.unchecked" },
                "bounce_pct": { display: "Bounce Rate",        data_format: "number.percent.unchecked" }
            },
            sort: { idx:1, dir:"desc" }
        });
        self.CreateReport({
            id: "WebsiteTraffic_KeywordsOrganic",
            dynamically_filterable: true,
            initial_filter_value: typeof cookie_get('lastFilter:' + self.dynamic_config.businessUnitId()) !== "undefined" ? unescape(cookie_get('lastFilter:' + self.dynamic_config.businessUnitId())) : "",
            container_selector: "#ga_keywords_report",
            data_observable: self.data.reports.WebsiteTraffic_KeywordsOrganic,
            column_meta: {
                "row_key":    { display: "Keyword",   css: "text_align_left" },
                "pct_visits": { display: "% Visits",           data_format: "number.percent" },
                "visits":     { display: "Visits",             data_format: "number.integer" },
                "pages":      { display: "Pages Per Visit",    data_format: "number.float.2" },
                "duration":   { display: "Avg Visit Duration", data_format: "timespan.hours" },
                "pct_new":    { display: "% New Visits",       data_format: "number.percent.unchecked" },
                "bounce_pct": { display: "Bounce Rate",        data_format: "number.percent.unchecked" }
            },
            sort: { idx:1, dir:"desc" },
            extended_output_observable: self.data.reports.WebsiteTraffic_KeywordsOrganic_DynamicFilterStats
        });

        self.CreateReport({
            id: "TimeToMarket",
            container_selector: "#ttm_report",
            data_observable: self.data.reports.TimeToMarket,
            column_meta: {
                "vehicle":            { display: "Vehicle",   css: "text_align_left" },
                "date_received":      { display: "Inventory Received Date",      data_format: "date.day" },
                "date_online_photos": { display: "Online with Photo(s) Date",    data_format: "date.day" },
                "days_photos":        { display: "Days to Online with Photo(s)", data_format: "number.positive_integer.unchecked" },
                "days_complete":      { display: "Days to Complete Ad Online",   data_format: "number.positive_integer.unchecked" },
                "stock_number":       { display: "Stock Number" }
            },
            sort: { idx:2, dir:"desc" }
        });

        self.RedirectClickAlertGauge = function() {
            var baseUrl = self.dynamic_config.inventoryBaseUrl();
            var usedNewType = self.GetIntVehicleType(self.controls.vehicleTypeFilter());
            var filter = self.dynamic_config.merchandisingAlertSelectionBucket();

            window.location = baseUrl + "?usednew=" + usedNewType + "&filter=" + filter;
            
            return false; // because we don't want to follow the original url
        };

        self.GetIntVehicleType = function(stringType) {
            switch (stringType) {
                case "New":
                    return 1;
                case "Used":
                    return 2;
                default:
                    return 0;
            }
        };

        //// vanilla js (or non-knockout) library glue and addons
        // placeholders
        $("#MaxSeTextbox").simplePlaceholder({ placeholderClass: "placeholder_text" });
        $("#MaxAdSearchTextbox").simplePlaceholder({ placeholderClass: "placeholder_text" });

        // errors
        self.errors.Trends = ko.computed( function() {            
            if( self.services.trends.loading() )
                return null;
            var data = self.services.trends.response();
            if( _(data).isEmpty() )
                return MaxDashboardBase.error_html.no_data;

            var data_count = 0;
            _.map(data.Trends, function(values){
                data_count += _.reduce(values, function(sum, obj){
                    return sum + obj.EmailLeadsCount + obj.PhoneLeadsCount + obj.ChatRequestsCount + obj.DetailPageViewCount;
              }, 0);
            });

            if(data_count === 0)
                return MaxDashboardBase.error_html.no_data;

            return null;
        });

        // errors
        self.errors.TrendsOverview = ko.computed( function() {            
            if( self.services.trends.loading() )
                return null;
            var data = self.data.processing.SitePerformanceTrend_Filtered();
            if( _(data).isEmpty() || (typeof data === "undefined"))  
                return MaxDashboardBase.error_html.no_data;

            var data_count = 0;
            _.map(data.Trends, function(values){
                data_count += _.reduce(values, function(sum, obj){
                    return sum + obj.EmailLeadsCount + obj.PhoneLeadsCount + obj.ChatRequestsCount + obj.DetailPageViewCount;
              }, 0);
            });

            if(data_count === 0)
                return MaxDashboardBase.error_html.no_data;

            return null;
        }).extend({ throttle: 50 });
        self.errors.CostPerDirectLead = ko.computed( function() {
            if( self.services.trends.loading() )
                return null;
            var data = self.data.charts.Performance_AvgNumberDirectLeads();
            if( self.fn.empty_chart_data( data ))
                return MaxDashboardBase.error_html.no_data;
            return self.errors.Trends();
        });

        self.errors.TrendsWithBudgets = ko.computed( function() {
            if( self.services.trends.loading() )
                return null;
            
            var budget = self.data.charts.Performance_AvgMonthlyCost();
            if (budget) {
                var sum = _.reduce(budget, function(sum, obj){
                    return sum + obj.budget;
                }, 0);
                if(sum === 0)
                    return self.fn.message_template( MaxDashboardBase.error_html.no_budget );                
            }

            return self.errors.Trends();
        });

        self.errors.GoogleAnalytics = ko.computed( function() {
            if( self.services.ga.loading() )
                return null;
            var data = self.services.ga.response();
            if( _(data).isEmpty() || !data.Referrals ||  _(data.Referrals.Mobile.Rows).isEmpty() ||  _(data.Referrals.Sources.Rows).isEmpty() )
                return MaxDashboardBase.error_html.no_data;
            return null;
        });
        self.errors.GoogleAnalyticsTrends = ko.computed( function() {
            if( self.services.ga_trends.loading() )
                return null;
            var data = self.services.ga_trends.response();
            if( _(data).isEmpty() )
                return MaxDashboardBase.error_html.no_data;
            return null;
        });

        self.errors.TimeToMarket = ko.computed( function() {
            if( self.services.ttm.loading() )
                return null;
            var data = self.services.ttm.response();
            if( _(data).isEmpty() || _(data.Days).isEmpty() ||  data.BusinessUnitId == -1 )
                return MaxDashboardBase.error_html.no_data;
            return null;
        });
        self.errors.MarketingTraffic = ko.computed(function() {
            if (self.services.marketing.loading())
                return null;
            if (!self.dynamic_config.hasShowroomReporting || !self.dynamic_config.hasDigitalShowroom) return {};
            var data = self.services.marketing.response();
            if (_(data).isEmpty() || !_(data.Errors).isEmpty() || _(data.Combined).isEmpty()) {
                return MaxDashboardBase.error_html.no_data;
            }
            return null;
        });
        self.errors.Website20Traffic = ko.computed(function() {
            if (self.services.website20traffic.loading())
                return null;
            if (!self.dynamic_config.hasShowroomReporting || !self.dynamic_config.hasWebsite20) return {};
            var data = self.services.website20traffic.response();
            if (_(data).isEmpty() || !_(data.Errors).isEmpty()) {
                return MaxDashboardBase.error_html.no_data;
            }
            return null;
        });

        // [This might be useful]
        // http://api.highcharts.com/highcharts#Axis.getExtremes()
        // FOR EXAMPLE:
        // if( chart.yAxis[0].getExtremes().dataMax == 0 ) 
        //    it means there is no useful data to show in the chart and should be universally true

        // animations

        // this is still mostly legacy code, but we've no need of a full animation framework yet
        self.data.gauges.counts.subscribe( function( data ) {
            var gaugeHeight = 121,
                gaugeYDelta = 0;
            var anim_spec = {
                step: function (now, fx) {
                    fx.start = gaugeHeight;
                    $(fx.elem).css( "background-position", "0px "+now+"px" );
                },
                duration: 1000
            };
            // gauges - animation
            var gauge_GetMerchandisingAlertPosition = function( x, h, yd ) {
                return h - (x/100*h) + yd;
            };
            $("#MerchandisingAlertGauge .gauge").animate(
                { "border-spacing": gauge_GetMerchandisingAlertPosition( data.MerchandisingAlertPercent, gaugeHeight, gaugeYDelta ) },
                anim_spec
            );
            var gauge_GetNotOnlinePosition = function( x ) {
                // approximated based on legacy function
                // see old function and side-by-side comparison here:   http://jsfiddle.net/Npqn6/
                return 0.305*Math.pow(x,1.3);    
            };
            $("#NotOnline .gauge").animate(
                { "border-spacing": gauge_GetNotOnlinePosition( data.NotOnlinePercent ) },
                anim_spec
            );
        });
    
        ///////////////////////////////////
        // browser-specific considerations (dirty hacks for IE8)
        if( self.static_config.is_IE8 ) {
            // hanging label column chart y-axis label position bug
            self.charts.WebsiteTraffic_TrafficSources.use_setData = false; // falls back to Series.update()
            // pie chart gradient render bug
            self.charts.WebsiteTraffic_MobileTraffic.data = ko.computed( function() {
                var data = self.data.charts.WebsiteTraffic_MobileTraffic();
                if( !data )
                    return null;
                _(data.series[0].data).each( function( data_point ) {
                    data_point.color = data_point.color.stops[0][1];
                });
                return data;
            });
        }

    });

}; // MaxDashboardExecutive

