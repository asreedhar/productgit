$(function () {
    MaxDashboard.OnReady({
		businessUnitId: parseInt($("#BusinessUnitIdHidden").val(), 10),
		inventoryTypeDDL: $("#InventoryTypeDDL").val(),
		analyticsSuiteEnabled: $("#HasAnalyticsSuite").val()==="True",
		chartWrapper: $("#WebsitePerformance"),
        chartContainer: $(".chart-container"),
        isDebug: $("#jsIsDebug")[0] && $("#jsIsDebug").val() === "1"
    });
    $("#chart_stats").on("click", function () {
        _gaq.push(['_trackEvent', 'Dashboard', 'Click', 'Website Stats Graph']);
        window.location.href="/merchandising/Reports/PerformanceSummaryReport.aspx";
    });
});