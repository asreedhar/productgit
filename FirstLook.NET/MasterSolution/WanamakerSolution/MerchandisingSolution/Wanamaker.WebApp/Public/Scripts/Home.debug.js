//  ===========================================================================
//  This script is intended for use with the Inventory.aspx page
//  ===========================================================================
//  Dependencies:
//      - jQuery.Combined.js
//      - MAX.Utils.debug.js
//      - MAX.UI.Global.debug.js
//  ---------------------------------------------------------------------------

var NewCarPricing = {};

(function () {

    var domSelectors = {
        rebatePrice: $('#rebate-price'),
        discountPrice: $('#discount-price'),
        expirationDate: $('#newCar-Expiration'),
        applyPricesButton: $('#apply-prices'),
        errorText: $('#apply-buttons .error'),
        priceBaseLine: $('#priceBaseLine'),
        yearSelectBox: $('#yearSelectBox'),
        makeSelectBox: $('#makeSelectBox'),
        modelsSelectBox: $('#modelSelectBox'),
        trimsSelectBox: $('#trimSelectBox'),
        bodySelectBox: $('#bodyStyleSelectBox'),
        engineSelectBox: $('#engineDescriptionSelectBox'),
        newCarVehicles: $('#new-car-vehicles'),
        priceDelta: $('#price-buttons input[name=priceDelta]')
    };
    
    var businessunitId = $('#BusinessUnitIdHidden').val();

    var campaignFilter = {};

    var ajaxCalls = {};

    function PriceInfo(){
        this.InventoryIds = [];
        this.RebateAmount = 0;
        this.DiscountAmount = 0;
        this.PriceDelta = "subtract";
        this.PriceBaseLine = "Msrp";
        this.ExpirationDate = "";
        this.Make =  [];
        this.Model = [];
        this.Year = [];
        this.Trim = [];
        this.BodyStyle = [];
        this.EngineDescription = [];
    };

    var priceInfo = new PriceInfo();

    function loadCampaignFilter() {
        campaignFilter.Year              = selectors.year.getValues()   || [];
        campaignFilter.Make              = selectors.make.getValues()   || [];
        campaignFilter.Model             = selectors.model.getValues()  || [];
        campaignFilter.Trim              = selectors.trim.getValues()   || [];
        campaignFilter.BodyStyle         = selectors.bodystyle.getValues()   || [];

        campaignFilter.CampaignEngineDescription = [];
        window.selectors = selectors;
        var engineOptions = selectors.enginedescription.dom().children();
        if (engineOptions.length > 0) {
            _.each(engineOptions, function(opt) {
                opt = $(opt);
                var selected = opt.attr('selected') == 'selected';
                var val = opt.attr('value');
                var text = opt.text();
                var cfEngineDescription = {};
                if (selected) {
                    cfEngineDescription.Description = text;
                    cfEngineDescription.IsStandard = (val == "maxchromestandard");
                    cfEngineDescription.OptionCode = (cfEngineDescription.IsStandard) ? null : val;
                    
                    campaignFilter.CampaignEngineDescription.push(cfEngineDescription);
                }
            });
        }

        return $.extend({}, campaignFilter);
    }
    
    function okToPopulateSelector(dom) {
        var ok = true;
        var selectorId = dom.data('selector-order');
        var selectorParentId = dom.data('selector-parent');
        if (selectorId > 0 && selectorParentId > 0) {
            var selectorParent = dom.parents('#new-car-pricing').find('select[data-selector-order=' + selectorParentId + ']');
            if (
                selectorParent.data('allowmultiselect') == false && 
                (selectorParent.val() == null || selectorParent.val().length > 1))
                ok = false;
        }

        if (!ok) {
            var childSelectors = dom.parents('#new-car-pricing').children('select');
            _.each(childSelectors, function(selector) {
                selector = $(selector);
                if (selector.data('selector-order') >= dom.data('selector-order')) {
                    selector.empty();
                }
            });
        }

        return ok;
    }

    function selectBox(dom, url, shouldPost) {
        return {
            load: function(filter, callback) {
                dom.addClass('loading');

                var postData = (shouldPost) ? JSON.stringify({ filter: filter, businessUnitId: businessunitId }) : null;
                if (ajaxCalls[url()]) {
                    ajaxCalls[url()].abort();
                }
                
                ajaxCalls[url] = $.ajax({
                    type: "POST",
                    url: url(),
                    cache: false,
                    contentType: 'application/json',
                    data: postData,
                    success: function(data) {
                        if (data.errorMessage) {
                            OnError(data.errorMessage);
                            return;
                        }

                        var i, options = "", selected = [];

                        // reselect options if they are available.
                        if (dom.selector.match(/enginedescription/gi)) {
                            if (priceInfo.EngineDescription != null) {
                                selected = priceInfo.EngineDescription;
                            }
                        } else if (dom.selector.match(/body/gi)) {
                            if (priceInfo.BodyStyle != null) {
                                selected = priceInfo.BodyStyle;
                            }
                        }

                        // We don't hang on to query handles, so I can't abort a request.
                        // So make sure it's still okay to show the results before doing so.
                        if (okToPopulateSelector(dom)) {
                            for (var optKey in data.Options) {
                                if (data.Options.hasOwnProperty(optKey)) {
                                    var isSelected = false;
                                    if (selected.length > 0) {
                                        isSelected = _.contains(selected, optKey);
                                    }
                                    options += "<option " + ((isSelected) ? "selected=\"selected\"" : "") + " value=\"" + htmlEntities(optKey) + "\" title=\"" + htmlEntities(data.Options[optKey]) + "\">" + htmlEntities(data.Options[optKey]) + "</option>";
                                }
                            }
                            
                            priceInfo.InventoryIds = data.InventoryIds;

                            if (callback) {
                                domSelectors.newCarVehicles.fadeOut(callback());
                            } else
                                domSelectors.newCarVehicles.fadeOut(function() {
                                    $(this).text(priceInfo.InventoryIds.length).fadeIn();
                                });
                        }

                        dom.html(options);
                    },
                    complete: function() {
                        dom.removeClass('loading');
                    }
                });
            },

            loadAll: function() {
                var postData = (shouldPost) ? JSON.stringify({ filter: loadCampaignFilter(), businessUnitId: businessunitId }) : null;
                _.each(ajaxCalls, function(ac) {
                    ac.abort();
                });
                
                $.ajax({
                    type: "POST",
                    url: url(),
                    cache: false,
                    contentType: 'application/json',
                    data: postData,
                    success: function(data) {
                        if (data.errorMessage) {
                            OnError(data.errorMessage);
                            return;
                        }
                        if (data.CampaignFilter)
                            campaignFilter = data.CampaignFilter;

                        priceInfo.InventoryIds = data.InventoryIds;
                        domSelectors.newCarVehicles.fadeOut(function() {
                            $(this).text(priceInfo.InventoryIds.length).fadeIn();
                        });
                    }
                });
            },

            dom: function(){
                return dom;
            },

            empty: function(){
                dom.empty();
            },

            getValues: function(){
                return dom.val();
            },

            getUrlArgs: function(){
                var urlValues = "";
                _.each(this.getValues(), function(val){
                    urlValues += val + ",";
                });

                urlValues = urlValues.slice(0, -1);
                return urlValues;
            }

        };
    };

    var selectors = {
        year: selectBox(
            domSelectors.yearSelectBox,
            function() {
                return "/merchandising/InventorySearch.aspx/Years";
            }, true),
        make: selectBox(
            domSelectors.makeSelectBox,
            function() {
                return "/merchandising/InventorySearch.aspx/Makes";
            }, true),
        model: selectBox(
            domSelectors.modelsSelectBox,
            function() {
                return "/merchandising/InventorySearch.aspx/Models";
            }, true),
        trim: selectBox(
            domSelectors.trimsSelectBox,
            function() {
                return "/merchandising/InventorySearch.aspx/Trims";
            }, true),
        bodystyle: selectBox(
            domSelectors.bodySelectBox,
            function() {
                return "/merchandising/InventorySearch.aspx/Bodys";
            }, true),
        enginedescription: selectBox(
            domSelectors.engineSelectBox,
            function() {
                return "/merchandising/InventorySearch.aspx/Engines";
            }, true),

        all: selectBox(null,
             function(){
                return "/merchandising/InventorySearch.aspx/Filter";
            }, true)
    };

    function OnError(message){
        domSelectors.errorText.show();
        domSelectors.errorText.text(message);
    }

    function clearErrorMessage(){
        domSelectors.errorText.hide();
    }
    
    function htmlEntities(str) {
        return str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g,"&quot;");
    }
    
    function resetDialog(){
        priceInfo = new PriceInfo();
        
        selectors.year.empty();
        selectors.make.empty();
        selectors.model.empty();
        selectors.trim.empty();
        selectors.bodystyle.empty();
        selectors.enginedescription.empty();

        domSelectors.rebatePrice.val(priceInfo.RebateAmount);
        domSelectors.discountPrice.val(priceInfo.DiscountAmount);
        domSelectors.expirationDate.val(priceInfo.ExpirationDate);
        domSelectors.priceBaseLine.val(priceInfo.PriceBaseLine);
        $('#price-buttons input[name=priceDelta]').val([priceInfo.PriceDelta]);

        domSelectors.rebatePrice.removeAttr("disabled"); 
        clearErrorMessage();
        $('#apply-buttons .loading').hide();
        $('#apply-buttons .checkmark').hide();
        domSelectors.applyPricesButton.show();
    };

    function ValidInteger(val) {
        //test for alpha characters.
        if(/[A-Za-z]/.test(val)){
            return false;
        }

        var intVal = parseInt(val, 10);
        var isValidValue = !isNaN(intVal);
        if(isValidValue && intVal < 0){
            return false;
        }

        return (isValidValue && Math.floor(val) == val && $.isNumeric(val));
    };

    function registerChanges() {
        selectors.year.dom().change(function(){
            priceInfo.Year = $(this).val();

            selectors.make.empty();
            selectors.model.empty();
            selectors.trim.empty();
            selectors.bodystyle.empty();
            selectors.enginedescription.empty();
            priceInfo.Make = priceInfo.Model = priceInfo.Trim = priceInfo.BodyStyle = priceInfo.EngineDescription = [];
            
            if(priceInfo.Year == null) {
                selectors.year.load(loadCampaignFilter());
                return;
            }

            if (priceInfo.Year.length == 1) {
                selectors.make.load(loadCampaignFilter());
            }
            else
                selectors.all.loadAll();
        });

        selectors.make.dom().change(function(){
            priceInfo.Make = $(this).val();
            if(priceInfo.Make == null) {
                selectors.year.dom().change();
                return;
            }

            selectors.model.empty();
            selectors.trim.empty();
            selectors.bodystyle.empty();
            selectors.enginedescription.empty();

            priceInfo.Model = priceInfo.Trim = priceInfo.BodyStyle = priceInfo.EngineDescription = [];
            
            if (priceInfo.Make.length == 1) {
                selectors.model.load(loadCampaignFilter());
            }
            else
                selectors.all.loadAll();
        });

        selectors.model.dom().change(function(){
            priceInfo.Model = $(this).val();
            if(priceInfo.Model == null) {
                selectors.make.dom().change();
                return;
            }
            
            selectors.trim.empty();
            selectors.bodystyle.empty();
            selectors.enginedescription.empty();
            
            priceInfo.Trim = priceInfo.BodyStyle = priceInfo.EngineDescription = [];
            
            if(priceInfo.Model.length == 1)
                selectors.trim.load(loadCampaignFilter());
            else
                selectors.all.loadAll();
        });

        selectors.trim.dom().change(function(){
            priceInfo.Trim = $(this).val();
            priceInfo.BodyStyle = priceInfo.EngineDescription = [];
            
            if(priceInfo.Trim == null) {
                loadCampaignFilter();
                selectors.model.dom().change();
                return;
            }

            selectors.bodystyle.empty();
            selectors.enginedescription.empty();
            var cf = loadCampaignFilter();
            
            if (priceInfo.Trim.length == 1) {
                
                selectors.bodystyle.load(cf);
                selectors.enginedescription.load(cf);
            }
            else
                selectors.all.loadAll();

        });
        
        selectors.bodystyle.dom().change(function() {
            priceInfo.BodyStyle = selectors.bodystyle.dom().val();
            priceInfo.EngineDescription = selectors.enginedescription.dom().val();

            if (priceInfo.BodyStyle == null) {
                selectors.trim.dom().change();
                return;
            }

            var cf = loadCampaignFilter();
            selectors.enginedescription.empty();
            selectors.enginedescription.load(cf);
        });
        
        selectors.enginedescription.dom().change(function(){
            priceInfo.BodyStyle = selectors.bodystyle.dom().val();
            priceInfo.EngineDescription = selectors.enginedescription.dom().val();
            
            if (priceInfo.EngineDescription == null) {
                selectors.trim.dom().change();
                return;
            }

            var cf = loadCampaignFilter();
            selectors.bodystyle.empty();
            selectors.bodystyle.load(cf);
        });

        $('#new-car-pricing').click(function(){
            if(ValidInteger( domSelectors.rebatePrice.val() )){
                domSelectors.rebatePrice.removeClass('error');
            }
                
            if(ValidInteger( domSelectors.discountPrice.val() )){
                domSelectors.discountPrice.removeClass('error');
            }

            clearErrorMessage();
        });

        $("input[name='priceDelta']").change(function(){
            priceInfo.PriceDelta = $("input[name='priceDelta']:checked").val();
        });
        
        domSelectors.expirationDate.change(function(){
            domSelectors.expirationDate.removeClass('error');
            priceInfo.ExpirationDate = domSelectors.expirationDate.val(); 
        });

        domSelectors.priceBaseLine.change(function(){
            var val = $('#priceBaseLine :selected').val();
            if(val == 'Invoice'){
                domSelectors.rebatePrice.attr("disabled", "disabled");
                priceInfo.RebateAmount = 0;
                domSelectors.rebatePrice.val(priceInfo.RebateAmount);
                $('#price-buttons input[name=priceDelta]').val(["add"]);     
            }
            else{
                domSelectors.rebatePrice.removeAttr("disabled");
                $('#price-buttons input[name=priceDelta]').val(["subtract"]);     
            }

            priceInfo.PriceDelta = $("input[name='priceDelta']:checked").val();
            priceInfo.PriceBaseLine = val;
        });


        domSelectors.rebatePrice.bind('keyup', function(){
            var value = $(this).val();
            if(! ValidInteger(value) ) {
                 domSelectors.rebatePrice.addClass('error');
            }
            else {
                domSelectors.rebatePrice.removeClass('error');
                priceInfo.RebateAmount = parseInt(value, 10);
            }
        });

        domSelectors.discountPrice.bind('keyup', function(){
            var value = $(this).val();
            if(! ValidInteger(value) ) {
                 domSelectors.discountPrice.addClass('error');
            }
            else {
                domSelectors.discountPrice.removeClass('error');
                priceInfo.DiscountAmount = parseInt(value, 10);
            }
        });

        
        $('#apply-buttons button:first').click(function(){
             $('#new-car-pricing').dialog('close');
             resetDialog();
        });

        $('button#apply-prices').click(function(){
                
                if(!ValidInteger( domSelectors.rebatePrice.val() )){
                    domSelectors.rebatePrice.addClass('error');
                    OnError('Rebate must be valid whole number');
                    return false;
                }
                
                if(!ValidInteger( domSelectors.discountPrice.val() )){
                    domSelectors.discountPrice.addClass('error');
                    OnError('Discount must be valid whole number');
                    return false;
                }
                
                if(priceInfo.ExpirationDate === ""){
                    domSelectors.expirationDate.addClass('error');
                    OnError('Must supply an expiration date');
                    return false;
                }

                var date = Date.parse(priceInfo.ExpirationDate);
                var currentDate = new Date();
                if(date < currentDate){
                    domSelectors.expirationDate.addClass('error');
                    OnError('Expiration date cannot be less than current date');
                    return false;
                }

                if(priceInfo.RebateAmount == 0 && priceInfo.DiscountAmount == 0){
                    domSelectors.rebatePrice.addClass('error');
                    domSelectors.discountPrice.addClass('error');
                    OnError('Must supply at least one discount');
                    return false;
                }

                if(confirm('All the vehicles selected will have this discount applied. Are you sure you want to continue?')){

                $('#apply-buttons .loading').show();
                domSelectors.applyPricesButton.hide();

                if(priceInfo.PriceDelta === "add"){
                    priceInfo.DiscountAmount = Math.abs(priceInfo.DiscountAmount);
                    priceInfo.RebateAmount = Math.abs(priceInfo.RebateAmount);
                }

                if(priceInfo.PriceDelta === "subtract"){
                    priceInfo.DiscountAmount = -Math.abs(priceInfo.DiscountAmount);
                    priceInfo.RebateAmount = -Math.abs(priceInfo.RebateAmount);
                }

                var postUrl = "/merchandising/NewCarPricing.aspx/Save";

                $.ajax({
                    type: "POST",
                    contentType: 'application/json',
                    data: JSON.stringify({ priceInfo: priceInfo, businessUnitId: businessunitId }),
                    url: postUrl,
                    cache: false,
                    success: function(data){
                        if(data.errorMessage) {
                            OnError(data.errorMessage);
                            $('#apply-buttons .loading').hide();
                            domSelectors.applyPricesButton.show();
                            return;
                        }
                        $('#apply-buttons .loading').hide();
                        $('#apply-buttons .checkmark').show();
                    
                        setTimeout(function(){
                            $('#new-car-pricing').dialog('close');
                            resetDialog();
                        }, 2000);
                    }
                });
            }
        });
    };

    this.init = function () {
        $('#new-car-pricing').dialog({
            autoOpen: false,
            width: 700,
            modal: true,
            title: 'New Car Pricing (BETA)',
            dialogClass: 'embiggened_title'
        });

        domSelectors.expirationDate.datepicker({ minDate: 1});
        
        registerChanges();
        resetDialog();

        $('#incentives').click(function(){
            
            $('#new-car-pricing').dialog('open');
            if( selectors.year.dom().is(':empty')){
                selectors.year.load();
            }
            return false;
        });
    };

}).apply(NewCarPricing);



var ActiveCampaigns = {};

(function(){
    var campaigns = null;

    /*
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    function formatMoney(val){
        val = val.replace(/,/g, "");
        var intValue = parseInt(val, 10);
        if(intValue >= 0)
            return '$' + numberWithCommas(intValue);
        else
            return "-" + '$' + numberWithCommas(Math.abs(intValue));
    };*/

    // via phpJS
    Number.prototype.toFixxed = function (prec) {
        var k = Math.pow(10, prec);
        return '' + Math.round(this * k) / k;
    };
    function number_format (number, decimals, dec_point, thousands_sep) {
        number = (number + '').replace(/[^0-9+-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '';
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? n.toFixxed(prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    }
    // designed to handle, at the very least:
    //   "11,985"
    //   17655.72
    function to_number (inp) {
        if( inp === null || typeof inp == "undefined" )
            return null;
        else if( typeof inp == "string" )
            return parseFloat( inp.replace(/,/g, "") );
        else if( typeof inp == "number" )
            return inp;
        else if( typeof inp == "object" )
            return parseFloat( inp.toString() );
        else if( typeof inp == "boolean" )
            return inp? 1 : 0;
        else return 0;
    }

    function formatMoney (source) {
        return "$"+number_format( to_number( source ), 0 ); // round to nearest dollar
    }

    function rowClicked(){
        $('#campaignContent table').dataTable().$('tr.selected').removeClass('selected');
        $(this).addClass('selected');

        var campaignId = $(this).attr("id");
        $('#campaignDetailsContent').empty();
        var campaign = _.find(campaigns, function(obj){return obj.CampaignId == campaignId;});
        var html = $('#campaignDetailsTemplate').tmpl(campaign);
        $('#campaignDetailsContent').html(html);
        $('#campaignDetailsContent table').dataTable({
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "aoColumnDefs": [
                {
                    aTargets: [1,2,3,4],
                    mRender: function ( source, type) {
                        if(type == 'display'){
                            return formatMoney(source);
                        }
                                   
                        return source;
                    }
                }
            ]
        });
    };

    function cancelClicked(event){
        event.stopPropagation();

        var businessunitId = $('#BusinessUnitIdHidden').val();
        var campaignId = $(this).closest('tr').attr("id");

        var postUrl = "/merchandising/NewCarPricing.aspx/Cancel";

        var deleteRow = $(this).closest('tr').get(0);
        $(this).closest('td').addClass('loading');
        $(this).hide();
        
        $.ajax({
            type: "POST",
            contentType: 'application/json',
            data: JSON.stringify({campaignId: campaignId, businessunitId: businessunitId}),
            url: postUrl,
            cache: false,
            success: function(data){
                $('#campaignContent table').dataTable().fnDeleteRow(deleteRow);
                $('#campaignDetailsContent').empty();
            }
        });

    };


    function loadCampaignContent(){    
        var businessunitId = $('#BusinessUnitIdHidden').val();
        $('#campaignContent').addClass('loading');   
        $.ajax({
                type: "GET",
                url: '/merchandising/NewCarPricing.aspx/ListCampaigns/' + businessunitId,
                cache: false,
                success: function(data){
                    campaigns = data;
                    var html = $('#activeCampaignTemplate').tmpl({data: data});
                    $('#campaignContent').html(html);
                    $('#campaignContent tbody tr').click(rowClicked);
                    $('#campaignContent button').click(cancelClicked);
                    $('#campaignContent table').dataTable({
                        "bJQueryUI": true,
                        "sPaginationType": "full_numbers",
                        "aaSorting": [[1,'asc'],[0,'asc']], // bugzID: 27660 - initial sort by start date, tureen 10/28/2013                        
                        "aoColumnDefs": [
                            {
                                aTargets: [0],
                                mRender: function(source, type){
                                    return source.replace(/New-/gi, "");
                                }
                            },
                            {
                                aTargets: [4,5],
                                mRender: function ( source, type) {
                                    if(type == 'display'){
                                        return formatMoney(source);
                                    }
                                   
                                   return source;
                                }
                            },
                            {
                                aTargets: [3],
                                mRender: function ( source, type, data) { 
                                    if(source == 'Msrp'){
                                        return source.toUpperCase();
                                    }
                                    return source;
                                }
                            }
                        ]
                    });
                    //IE 8 hack insert &nbsp; for formatting
                    $('#campaignContent .ui-widget-header').prepend(document.createTextNode('\u00A0'));

                    $('#campaignContent').removeClass('loading');   
                }
        });
    };
    
    
    this.init = function(){
         $('#activeCampaignsDialog').dialog({
            autoOpen: false,
            width: 800,
            modal: true,
            title: 'Active Discounts (BETA)',
            dialogClass: 'embiggened_title'
        });    

        $('#viewCampaigns').click(function(){
            $('#activeCampaignsDialog').dialog('open');
            $('#campaignContent').empty();
            $('#campaignContent').removeClass('loading');   
            $('#campaignDetailsContent').empty();
            loadCampaignContent();

            return false;
        });

    };

}).apply(ActiveCampaigns);



var MaxAdHomePage = function () { };

MaxAdHomePage.pageRequestManager = Sys.WebForms.PageRequestManager.getInstance();

MaxAdHomePage.prototype = {

    /// Initializes client-side page functionality on page load
    OnReady: function () {

        this.OnPageLoaded();

        MaxAdHomePage.pageRequestManager.add_pageLoaded( this.OnPageLoaded );
    },

    /// Initializes UI for Auto Load button postback
    InitAutoLoad: function () {

        var autoLoadDialog,
            autoLoadDialogDiv = $( "#NeededLoginInfo:first" );

        $( "#InventoryTable ul.actions li.autoLoad a" ).die( "click" ).live( "click", function ( e ) {

            $( this ).css( "display", "none" );
            $( this ).siblings( ".autoLoadWorking" ).show();

            $( "#InventoryTable ul.actions li.autoLoad a" ).attr( "disabled", "disabled" );

            // IE7 does not execute the postback
            if ( MAXUI.IsIE7() || MAXUI.IsIE8() ) eval( this.href );

        } );

        // Show 'please wait' panel when user clicks cancel on autoload login screen
        $( "input.cancelBtn, #automator_xBtn", autoLoadDialogDiv ).die( "click" ).live( "click", function ( e ) {

            var modalBody = $( ".modalBod:first", autoLoadDialogDiv );
            $( modalBody ).append( "<div class=\"pleaseWaitPanel\" style=\"height:"
                + ( $( autoLoadDialogDiv ).height() + 24 ) + "px\"><p style=\"margin-top:"
                + parseInt( ( $( autoLoadDialogDiv ).height() * 0.4 ), 10 )
                + "px\">Please wait ...</p></div>" );

        } );

        // Redisplay modal using jQuery
        autoLoadDialog = $( "#NeededLoginInfo" ).dialog( { closeOnEscape: false, dialogClass: "autoLoad hideClose", modal: true, title: "Auto Load", width: 600 } );
        autoLoadDialog.parent().appendTo( "form:first" );

    },

    InitBulkActions: function () {

        $( "#BulkActionsCkb" ).die( "click" ).live( "click", function ( e ) {

            var checkboxes = $( "#InventoryTable tbody input.bulkActions" ),
                allChecked = $( checkboxes ).length === $( "#InventoryTable tbody input.bulkActions:checked " ).length;

            if ( allChecked ) {

                $( checkboxes ).removeAttr( "checked" );
                return;
            }

            $( checkboxes ).attr( "checked", "checked" );
            $( this ).attr( "checked", "checked" );

        } );

        $( "#BulkActionsSelect" ).die( "change" ).live( "change", { instance: this }, function ( e ) {

            var action = $( this ).val(),
                checked = $( "#InventoryTable tbody input.bulkActions:checked" ),
                instance = e.data.instance;

            if ( checked.length === 0 ) {

                switch ( action ) {

                    case "workbook":
                        alert( "Please select the vehicles you would like to add to your workbook." );
                        break;

                    case "offline":
                        alert( "Please select the vehicles you would like to mark as offline." );
                        break;

                    case "online":
                        alert( "Please select the vehicles you would like to mark as online." );
                        break;

                    default:
                        break;

                }

                // Reset UI
                $( "#BulkActionsSelect" ).val( "" );

                e.preventDefault();
                e.stopPropagation();
                return false;
            }

            switch ( action ) {

                case "workbook":
                    instance.ShowWorkBook( checked );
                    e.preventDefault();
                    e.stopPropagation();
                    break;

                case "offline":
                case "online":
                    $( "form:first" ).submit();
                    break;

                default:
                    e.preventDefault();
                    e.stopPropagation();
                    break;

            }

        } );

    },

    InitTrimSelect: function () {

        var trimSelects = $( "#Body #Content #InventoryTable div.customSelect" );

        $( "div.customSelect" ).each( function ( index, element ) {

            $( element ).find( "span.selected" ).text( $( element ).find( "ul li:first" ).text() );
            $( element ).attr( "chromeid", "" );
            $( element ).attr( "style", "z-index:" + ( 500 - index ) + ";" );
            $( element ).find( "ul" ).attr( "style", "z-index:" + ( 1000 - index ) + ";" );

        } );

        $( "div.customSelect ul li a" ).die( "click" ).live( "click", function ( e ) {

            var customSelect = $( this ).parents( "div.customSelect:first" ),
                selectedSpan = $( customSelect ).find( "span.selected" );

            $( selectedSpan ).text( $( this ).text() )
                .attr( "chromeid", $( this ).parent().attr( "chromeid" ) )
                .attr( "trim", $( this ).parent().attr( "trim" ) );

            if ( $( selectedSpan ).attr( "chromeid" ) !== $( customSelect ).attr( "chromeid" ) ) {
                $( customSelect ).attr( "chromeid", $( selectedSpan ).attr( "chromeid" ) );
                $( customSelect ).change();
            } else {
                $( customSelect ).find( "ul" ).hide();
            }

        } );

        $( "div.customSelect span.selected" ).die( "click" ).live( "click", function ( e ) {

            var thisList = $( this ).next( "ul" ),
                thisListVisible = $( thisList ).is( ":visible" );

            if ( !thisListVisible ) {
                $( "div.customSelect ul" ).hide();
                $( thisList ).show();
            } else {
                $( thisList ).hide();
            }


        } );

        $( trimSelects ).die( "click" ).live( "click", function ( e ) {

            e.stopPropagation();

            //If the chrome styles are already populated do not repopulate.
            if ( $( this ).find( "ul" ).children().length > 1 ) return;

            var selectBox = $( this ),
                vin = $( selectBox ).attr( "vin" );

            $.ajax( {
                async: false,
                url: "/merchandising/InventoryMVC.aspx/ChromeStyles/" + vin,
                contentType: "application/json; charset=utf-8",
                //We don't specify the data type so the errors that come back in html are processed properly.
                //Jquery default is Default: Intelligent Guess (xml, json, script, or html). http://api.jquery.com/jQuery.ajax/
                //dataType: "json",
                error: function ( jqXHR, textStatus, errorThrown ) {
                    window.location.assign( "/merchandising/ErrorPage.aspx?Error=From Inventory Page populate trim" );
                },
                success: function ( data ) {

                    var listItems, i;

                    if ( data.errorMessage ) {
                        alert( errorMessage );
                        return;
                    } else {
                        listItems = "<li chromeid=''><a>Select trim &hellip;</a></li>";
                        for ( i = 0; i < data.length; i++ ) {
                            listItems += "<li trim='" + data[i].Trim + "' chromeid='" + data[i].ChromeStyleId + "'><a>" + data[i].ChromeStyleName + "</a></li>";
                        }
                        $( selectBox ).find( "ul" ).html( listItems );

                    }
                },
                complete: function () {

                }
            } );

        } ).die( "change" ).live( "change", function ( e ) {

            var buid = $( this ).attr( "buid" ),
                chromeStyleId = $( this ).find( "span.selected" ).attr( "chromeid" ),
                inventoryId = $( this ).attr( "inventoryId" ),
                loadingDiv = $( "<div />" ).addClass( "loading" ).html( "&nbsp;" ),
                selectedTrim = $( this ).find( "span.selected" ).attr( "trim" ).toUpperCase(),
                usedNewFilter = $( this ).attr( "usedNewFilter" );

            // Show spinner
            $( this ).replaceWith( loadingDiv );

            //@TODO Disable all controls and links except the trim drop downs.
            $.ajax( {
                type: "POST",
                url: "/merchandising/InventoryMVC.aspx/SetChromeStyle",
                contentType: 'application/json; charset=utf-8',
                data: "{ businessUnitId: " + buid + ", inventoryId: " + inventoryId + ", chromeStyleId: " + chromeStyleId + ", usedNewFilter: " + usedNewFilter + " }",
                error: function ( jqXHR, textStatus, errorThrown ) {
                    alert( "Error : " + errorThrown.description );
                    window.location.assign( "/merchandising/ErrorPage.aspx?Error=From Inventory page set trim" );
                },
                success: function ( data ) {

                    if ( data.errorMessage ) {

                        //@TODO handle error here
                        alert( "Error from controller json " + data.errorMessage );
                        return;

                    } else {

                        // Append trim/remove spinner
                        $( loadingDiv ).siblings( "a" ).append( selectedTrim );
                        $( loadingDiv ).hide();

                        // Update No Trim bucket count
                        $( "#MainFilters .filterMenu li:contains(No Trim) span.count" ).text( "(" + data + ")" );

                        // Remove row if in No Trim bucket
                        if ( $( "#MainFilters .filterMenu li.selected" ).text().indexOf( "No Trim" ) > -1 ) $( loadingDiv ).parents( "tr" ).hide( 1000 );

                    }
                },
                complete: function () {

                }
            } );

        } );

        $( "body" ).die( "click" ).live( "click", function ( e ) {
            $( "div.customSelect ul" ).hide();
        } );

    },

    /// Adds overlay to rows in inventory table which have an invalid VIN (Canadian, Mexican, etc.)
    MaskInvalidVins: function () {

        $( "#InventoryTable input.validVIN[value=false]" ).each( function () {
            $( this ).after( "<div class=\"overlay\" style=\"height:" + ( $( this ).parents( "tr:first" ).height() - 4 ) + "px;\"><p>Only vehicles manufactured for sale in the USA are currently supported.</p></div>" );
        } );

    },

    /// Initializes client-side page functionality on page load & async postback
    OnPageLoaded: function ( sender, args ) {

        // If I'm called directly or I have panels to update, PM
        var should_fire = arguments.length === 0 || args.get_panelsUpdated() > 0;
        if ( should_fire ) {

            this.ShowIE6Message();
            this.InitAutoLoad();
            this.InitBulkActions();
            this.InitTrimSelect();
            this.MaskInvalidVins();

        }
    },

    ShowIE6Message: function () {

        var dialog, expires,
            isUpgradeCookie = $.cookie( "HideMaxIE6UpgradeMessage" );

        if ( MAXUI.IsIE6() && !isUpgradeCookie ) {

            dialog = $( "#IE6Support" ).dialog( { dialogClass: "ie6modal", modal: true, width: "600px" } );

            $( "#IE6Support" ).siblings( ".ui-dialog-titlebar" ).remove();

            $( "#CloseIE6DialogLink" ).bind( "click", function ( e ) {

                expires = new Date();
                expires.setDate( expires.getDate() + 30 );

                $.cookie( "HideMaxIE6UpgradeMessage", true, { expires: expires } );
                $( dialog ).dialog( "close" );

                e.preventDefault();
                e.stopPropagation();

            } );
        }

    },

    ShowWorkBook: function ( checked ) {

        var invIds = "", url, win;

        // Build list of ids
        $.each( checked, function () {

            invIds += $( this ).siblings( "input:hidden:first" ).val() + ",";

        } );

        // Reset UI
        $( "#BulkActionsSelect" ).val( "" );
        $( checked ).removeAttr( "checked" );
        $( "#BulkActionsCkb" ).removeAttr( "checked" );

        // Open workbook
        url = "/merchandising/Print/PrintBuildSheetBatch.aspx?ids=" + invIds;
        window.location.assign( url );
    }

};

var Page, ctrChart;
$(document).ready(function () {
    Page = new MaxAdHomePage();
    Page.OnReady();

    NewCarPricing.init();
    ActiveCampaigns.init();

    ctrChart = new CTRChart();

    $("#ctr-graph").dialog({
        autoOpen: false,
        width: 840,
        modal: true
    });

    var loading = $("#ctr-graph .loading");
    $(".ctr-graph-link").on("click", function (event) {
        var invId = $(this).attr("data-inventoryId"),
            ymmt = $(this).attr("data-ymmt"),
            buid = $(this).attr("data-businessunitid"),
            stockNum = $(this).attr("data-stockNum");

        loading.html('');
        loading.show();
        $("#ctr-graph").dialog('option', 'title', ymmt + " (" + stockNum + ")");
        $("#ctr-graph").dialog('open');
        $.ajax({
            url: "/merchandising/InventoryMVC.aspx/CtrPrice/" + buid + "/" + invId,
            dataType: "text",
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (_.isEmpty(data.Sources)) {
                    loading.html("<div class='error'>We're sorry, but there is not enough data for this vehicle.</div>");
                } else {
                    loading.hide();
                    $("#ctr-graph .carinfo").html($("#ctrGraphHeader").tmpl(data));
                    ctrChart.VehicleData = data;
                    ctrChart.Init();
                }
            },
            error: function () {
                loading.html("<div class='error'>We're sorry, but we can't get data for that vehicle right now.</div>");
            }
        });
    });
});