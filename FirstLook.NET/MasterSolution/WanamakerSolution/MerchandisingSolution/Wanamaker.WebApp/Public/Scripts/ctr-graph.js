var CTRChart = function () { "use strict"; };

CTRChart.prototype = {
    
    VehicleData: {},
    
    Init: function () {
        this.PrepareData(this.VehicleData);
    },

    PrepareData: function (vehicleData) {

        var returnData = {},
            seriesData = [],
            plotBandData = [],
            plotBandAverages = [],
            colorScheme = {
                "AutoTrader.com": {
                    color: "#F8981D"
                },
                "Cars.com": {
                    color: "#6352b8"
                },
                "Average": {
                    color: "#5481d6",
                    opacity: 0.5
                },
                titles: {
                    color: "#111111"
                },
                plotBands: {
                    color: ['#F2F2F2', '#E5E5E5', '#D9D9D9']
                },
                gridLines: {
                    color: "#bcbcbc"
                }
            },
            average = [],
            dateRange = [],
            colorCounter = 0,
            priceEvents = _.reject(vehicleData.PriceEvents, function(priceEvent, key, list) {
                return (!_.isUndefined(list[key-1]) && priceEvent.Price === list[key-1].Price) || priceEvent.Price===0;// remove duplicates and $0 prices
            });

        _.each(priceEvents, function(priceEvent, key, list) {
            list[key]["Date"] = Date.parse(priceEvent["Date"]); // convert dates to milliseconds
        });

        _.each(vehicleData.Sources, function(ctrData, source) {
            
            var series = {
                name: source,
                selected: true,
                color: colorScheme[source].color,
                data: []
            };

            _.each(ctrData, function(ctrEvent) {
                
                var jsDate = Date.parse(ctrEvent["Date"]),
                    jsCtr = (!_.isNaN(ctrEvent.Rate)) ? ctrEvent.Rate*100 : 0, // set NaN to zero, but keep the CTR date
                    point = {
                        x: jsDate,
                        y: jsCtr
                    },
                    existingdate;

                series.data.push(point);

                // create a date object: dateObject = { date: jsDate, ctr: [jsCtr] };
                // then try to find if an object with that date is already in dateRange
                // if it is, then push just the CTR, otherwise create a new dateObject and push that to dateRange
                existingdate = _.find(dateRange, function (value) { return _.isEqual(value.date, jsDate); });
                if(_.isUndefined(existingdate)) {
                    dateRange.push({ date: jsDate, ctr: [jsCtr]});
                } else {
                    dateRange[_.indexOf(dateRange, existingdate)].ctr.push(jsCtr);
                }

            });

            seriesData.push(series);

        });

        dateRange = _.sortBy(dateRange, function (dateobject) { return dateobject.date; });
        
        // create average plot points
        average = {
            name: "Average",
            color: colorScheme.Average.color,
            fillOpacity: colorScheme.Average.opacity,
            type: "areaspline",
            lineWidth: 0,
            selected: true,
            // for every possible date (based on CTR data from all sources)
            data: _.map(dateRange, function (date) {
                var point = {};

                point = {
                    x: date.date,
                    // from our array of rates from 1-n sources, reduce them to an average
                    y: _.reduce(date.ctr, function(memo, num) {
                            return memo + num;
                    }, 0)/date.ctr.length
                };
                return point;
            })
        };
        seriesData.push(average);

        _.each(priceEvents, function (value, key, list) {
            if(value["Date"] < _.last(dateRange).date) { // for plotBands
                var start = value["Date"],
                    // problem: last price event is > last ctr event (dateRange)
                    end = (value["Date"]!==_.last(list)["Date"]) ? list[key+1]["Date"] : _.last(dateRange).date, // for the last priceEvent, use the last CTR date, otherwise use the next priceEvent date
                    returnData = {
                        from: start,
                        to: end,
                        label: {
                            text: "$"+Highcharts.numberFormat(value.Price,0)
                        }
                    },
                    averages = {
                        from: start,
                        sites: []
                    };

                // pick colors in order
                colorCounter = (colorScheme.plotBands.color[colorCounter]) ? colorCounter : 0;
                returnData.color = colorScheme.plotBands.color[colorCounter];
                colorCounter += 1;
                plotBandData.push(returnData);

                // calculate averages over date ranges (plotBands) for tooltips
                _.each(seriesData, function (site) {
                    var avg = {},
                        data = _.filter(site.data, function (data) {
                            // only the initial price event and the day before the next price change
                            return data.x >= start && data.x < end;
                        });

                    if(!_.isEmpty(data)) {
                        avg.name = site.name;
                        avg.from = start;
                        avg.ctr = _.pluck(data,'y');
                        avg.ctr = _.reduce(avg.ctr, function (memo, num, key, list) {
                            var sum = memo + num;
                            if(list.length===key+1) {
                                sum = sum / list.length; // at the end of the array, divide by the number of points
                            }
                            return sum;
                        }, 0);
                    
                        averages.sites.push(avg);
                    }
                    
                });
                plotBandAverages.push(averages);
            }
        });

        returnData = {
            seriesData: seriesData,
            plotBandData: plotBandData,
            plotBandAverages: plotBandAverages,
            colorScheme: colorScheme
        };

        this.RenderChart(returnData);
    },

    Chart: {},

    RenderChart: function (data) {

        var instance = this;
        this.Chart = $('#chart-line').highcharts(
        {
            chart: {
                type: "spline",
                ignoreHiddenSeries: true,
                style: {
                    fontFamily: 'Arial, Helvetica, sans-serif',
                    fontSize: '12px'
                },
                height: 400,
                width:800
            },
            legend: {
                labelFormatter: function () {
                    return '<span style="color: ' + this.color + '">' + this.name + '</span>';
                },
                padding: 18,
                align: "center",
                floating: false,
                verticalAlign: 'bottom',
                layout: 'horizontal',
                lineHeight: 16,
                symbolWidth: 0,
                symbolPadding: 10
            },
            plotOptions: {
                series: {
                    stickyTracking: false,
                    showCheckbox: true,
                    events: {
                        checkboxClick: function () {
                            if (this.visible) {
                                this.hide();
                            } else {
                                this.show();
                            }
                        },
                        legendItemClick: function () {
                           this.checkbox.checked = this.selected = !this.visible;
                        }
                    },
                    marker: {
                        enabled: false
                    }
                }
            },
            title: {
                text: "Effect of Price Change on Click Through Rate",
                style: {
                    color: data.colorScheme.titles.color,
                    fontSize: "14px",
                    fontWeight: "bold"
                }
            },
            xAxis: [
                {
                type: "datetime",
                plotBands: data.plotBandData,
                offset: 10,
                // since we can't define minTickInterval, minRange stops it from showing hour ticks for small ranges
                // feature request: http://highcharts.uservoice.com/forums/55896-general/suggestions/1357793-specify-a-minimum-unit-for-datetimelabelformats
                // source code hack: http://highslide.com/forum/viewtopic.php?f=9&p=49447
                minRange: 6 * 24 * 3600 * 1000,
                tickColor: "#6D6D6D"
              }
            ],
            yAxis: [{
                title: {
                    text: "Click Through Rate",
                    style: {
                        color: data.colorScheme.titles.color,
                        fontWeight: "normal"
                    },
                    id: "yAxisTitle"
                },
                gridLineColor: data.colorScheme.gridLines.color,
                labels: {
                    formatter: function() {
                        return this.value+"%";
                    }
                },
                min: 0
            }],
            tooltip: {
                shared: true,
                useHTML: true,
                animation: false,
                snap: 0,
                borderColor: "#9B9B9B",
                formatter: function () {
                    var bands = (instance.Chart.xAxis ? instance.Chart.xAxis[0].plotLinesAndBands : null);
                    var x = this.x;
                    var curBand;
                    var box;
                    var tooltip = {
                            message: false
                        };
                    
                    // get the plotBand data based on currently hovered point
                    curBand = _.find(bands, function (band) {
                        return band.options.from <= x && band.options.to >= x;
                    });

                    if (curBand) {

                        // get the size and x/y of plotBand
                        box = curBand.svgElem.getBBox();

                        // position the tooltip within the center of the plotBand
                        instance.Chart.tooltip.options.positioner = function (labelWidth) {
                            var posX = box.x + (box.width / 2)-(labelWidth/2);
                            if(posX+labelWidth >= instance.Chart.chartWidth) {
                                posX = box.x+box.width - labelWidth - 5; // right side
                            } else if (posX <= 0) {
                                posX =box.x + 5; // left side
                            }
                            return { x: posX, y: 70 };
                        };

                        tooltip.startDate = Highcharts.dateFormat("%m/%d/%Y", curBand.options.from);
                        tooltip.endDate = Highcharts.dateFormat("%m/%d/%Y", curBand.options.to);
                        tooltip.startPrice = curBand.options.label.text;
                        tooltip.ctr = _.find(data.plotBandAverages, function (average) {
                            return average.from === curBand.options.from;
                        });

                        tooltip.message = "<span style='font-weight: bold; text-decoration: underline;'>" + tooltip.startDate + " - " + tooltip.endDate + "</span><br />" +
                            "Price: " + tooltip.startPrice;

                        _.each(tooltip.ctr.sites, function (ctr) {
                            tooltip.message += "<br /><span style='color:" + data.colorScheme[ctr.name].color + "'>" + ctr.name + ":</span> " + Highcharts.numberFormat(ctr.ctr, 2) + "% (avg. CTR)";
                        });

                    }

                    return tooltip.message;
                }
            },
            series: data.seriesData,
            credits: {
                enabled: false
            }
        }, function(chart){
            var series = chart.series,
                container = $(chart.container),
                allBands = chart.xAxis[0].plotLinesAndBands,
                i=0, w, offset = container.offset(),
                x, y, isInside, xPoint, plot;
            
            // move the checkbox to the left, adjust spacing
            for(; i<series.length; i++) {
                w = Math.round(series[i].legendItem.getBBox().width + 30);
                series[i].checkbox.style.marginLeft = -w + 'px';
                series[i].checkbox.style.marginTop = '1px';
                series[i].checkbox.style.verticalAlign = "top";

                series[i].legendItem.attr({
                    translateX: +10
                });
            }

            // hide the plotBand label if it is wider than the plotBand
            _.each(allBands, function (band) {
                if(band.label.element.clientWidth > band.svgElem.getBBox().width) {
                    band.label.hide();
                }
            });

            // change the plotBand color on hover (including hovering overlapping content like series, tooltip, etc.)
            container.mousemove(function (event) {

                var offset = container.offset() || { left: 0, top: 0 };
                x = event.clientX - chart.plotLeft - offset.left;
                y = event.pageY - chart.plotTop - offset.top;
                isInside = chart.isInsidePlot(x, y);
                xPoint = chart.xAxis[0].translate(x, true);

                _.each(allBands, function (band) {
                    // reset bg for all plotbands
                    band.svgElem.attr({fill:band.options.color});
                });

                if(isInside) {
                    // check which plotband I'm on
                    plot = _.find(allBands, function (band) {
                        return band.options.from <= xPoint && band.options.to >= xPoint;
                    });

                    if(plot) {
                        plot.svgElem.attr({fill:"#FCFFC5"});
                    }
                }


            });

            $(chart.container).append("<div class='title-tooltip'>Vehicle Detail Page (VDP) views divided by Search Results Page (SRP) views.</div>");

            $(chart.container).find(".highcharts-axis").hover(function(i) {
                if( i.target.textContent===chart.yAxis[0].axisTitle.text ) {
                    $(".title-tooltip").show();
                }
            }, function(){
                $(".title-tooltip").hide();
            });

        }); // end highchart
    }

};