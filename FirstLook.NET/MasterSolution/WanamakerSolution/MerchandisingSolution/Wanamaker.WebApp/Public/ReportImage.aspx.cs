using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Wanamaker.WebApp.Public
{
    public partial class ReportImage : System.Web.UI.Page
    {
        private static byte[] _imgbytes =
               Convert.FromBase64String("R0lGODlhAQABAIAAANvf7wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==");

        protected void Page_Load(object sender, EventArgs e)
        {

            //Response.ClearHeaders();
            //Response.Headers.Add("Cache-Control", "no-cache, must-revalidate");
            //Response.Headers.Add("Location", "Public/Images/spacer.gif");
            //Response.Flush();
            //Response.End();

            //log request here...
            string host = Request.UserHostAddress;
            string agent = Request.UserAgent;
            string qs = string.Empty;
            string email = Request["email"];
            string buid = Request["dealerId"];
            foreach (string key in Request.QueryString.Keys)
            {
                string val = Request.QueryString[key];
            }

            Response.ContentType = "image/gif";
            Response.AppendHeader("Content-Length", _imgbytes.Length.ToString());
            Response.Cache.SetLastModified(DateTime.Now);
            Response.Cache.SetCacheability(HttpCacheability.Public);
            Response.BinaryWrite(_imgbytes);
        }


    }
}