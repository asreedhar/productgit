using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Profile;

namespace Wanamaker.WebApp
{
    /// <summary>
    /// Wrote out this class instead of having the class dynamically generate to avoid runtime errors.
    /// 
    /// The properties were taken from the <profile><properties /></profile> section of the web.config.
    /// </summary>
    public class ProfileCommon : ProfileBase
    {
        /// <summary>
        /// From MerchandisingWebSite, web.config: <add name="ListMode" type="Int32" defaultValue="1"/>
        /// </summary>
        [SettingsAllowAnonymous(false)] 
        public int ListMode
        {
            get
            {
                return (int)HttpContext.Current.Profile.GetPropertyValue("ListMode");
            }
            set
            {
                HttpContext.Current.Profile.SetPropertyValue("ListMode", value);
            }
        }

        /// <summary>
        /// From MerchandisingWebSite, web.config: <add name="NavMode" type="Boolean" defaultValue="false"/>
        /// </summary>
        [SettingsAllowAnonymous(false)] 
        public bool NavMode
        {
            get
            {
                return (bool)HttpContext.Current.Profile.GetPropertyValue("NavMode");
            }
            set
            {
                HttpContext.Current.Profile.SetPropertyValue("NavMode", value);
            }
        }

        /// <summary>
        /// From MerchandisingWebSite, web.config: <add name="InventoryObjective" type="Int32" defaultValue="0"/>
        /// </summary>
        [SettingsAllowAnonymous(false)] 
        public int InventoryObjective
        {
            get
            {
                return (int)HttpContext.Current.Profile.GetPropertyValue("InventoryObjective");
            }
            set
            {
                HttpContext.Current.Profile.SetPropertyValue("InventoryObjective", value);
            }
        }

        /// <summary>
        /// From MerchandisingWebSite, web.config: <add name="PhotoMode" type="Int32" defaultValue="0"/>
        /// </summary>
        [SettingsAllowAnonymous(false)] 
        public int PhotoMode
        {
            get
            {
                return (int)HttpContext.Current.Profile.GetPropertyValue("PhotoMode");
            }
            set
            {
                HttpContext.Current.Profile.SetPropertyValue("PhotoMode", value);
            }
        }

        /// <summary>
        /// From MerchandisingWebSite, web.config: <add name="ActiveFilter" type="Int32" defaultValue="7"/>
        /// </summary>
        [SettingsAllowAnonymous(false)] 
        public int ActiveFilter
        {
            get
            {
                return (int)HttpContext.Current.Profile.GetPropertyValue("ActiveFilter");
            }
            set
            {
                HttpContext.Current.Profile.SetPropertyValue("ActiveFilter", value);
            }
        }

        /// <summary>
        /// From MerchandisingWebSite, web.config: <add name="Overdue" type="Boolean" defaultValue="false"/>
        /// </summary>
        [SettingsAllowAnonymous(false)] 
        public bool Overdue
        {
            get
            {
                return (bool)HttpContext.Current.Profile.GetPropertyValue("Overdue");
            }
            set
            {
                HttpContext.Current.Profile.SetPropertyValue("Overdue", value);
            }
        }

        /// <summary>
        /// From MerchandisingWebSite, web.config: <add name="Sort" type="String" defaultValue=""/>
        /// </summary>
        [SettingsAllowAnonymous(false)] 
        public string Sort
        {
            get
            {
                return (string)HttpContext.Current.Profile.GetPropertyValue("Sort");
            }
            set
            {
                HttpContext.Current.Profile.SetPropertyValue("Sort", value);
            }
        }

        [SettingsAllowAnonymous(false)]
        public int NewUsed
        {
            get
            {
                return (int)HttpContext.Current.Profile.GetPropertyValue("UsedNewMode");
            }
            set
            {
                HttpContext.Current.Profile.SetPropertyValue("UsedNewMode", value);
            }
        }

        [SettingsAllowAnonymous(false)]
        public int DemoBusinessUnitId
        {
            get
            {
                return (int)HttpContext.Current.Profile.GetPropertyValue("DemoBusinessUnitId");
            }
            set
            {
                HttpContext.Current.Profile.SetPropertyValue("DemoBusinessUnitId", value);
            }
        }
    }
}
