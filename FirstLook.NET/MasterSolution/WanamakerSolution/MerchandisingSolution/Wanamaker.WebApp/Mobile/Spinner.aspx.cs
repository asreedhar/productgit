using System;

namespace Wanamaker.WebApp.Mobile
{
    public partial class Spinner : BasePage
    {
        public string MessageText
        {
            get
            {
                object o = ViewState["msg"];
                return (o == null) ? "" : (string)o;
            }
            set
            {
                ViewState["msg"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Request["msg"]))
            {
                MessageText = Request["msg"];
                this.Page.DataBind();
            }
        }
    }
}