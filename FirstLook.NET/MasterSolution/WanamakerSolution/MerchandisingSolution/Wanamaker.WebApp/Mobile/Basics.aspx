<%@ Page Language="C#" Theme="shredder" EnableTheming="True" AutoEventWireup="true" CodeBehind="Basics.aspx.cs" Inherits="Wanamaker.WebApp.Mobile.Basics" %>
<%@ Register TagPrefix="cwc" TagName="MobileHeader" Src="~/Mobile/Controls/MobileHeader.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>MAX AD - Web Lot Loader</title>
    <meta name = "viewport" content = "initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
</head>
<body class="mobileInventoryBody" runat="Server" onorientationchange="updateOrientation();">    
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            <asp:ScriptReference Path="~/Public/Scripts/global.debug.js" />
            <asp:ScriptReference Path="~/Public/Scripts/Loader.debug.js" />
        </Scripts>
        </asp:ScriptManager>
        
        <asp:ObjectDataSource runat="server" ID="TrimStyleDS" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.ChromeStyle" OnSelecting="StylePieces_Selecting"
            SelectMethod="ChromeTrimStyleNamesSelect">
            <SelectParameters>
                <asp:Parameter Name="vin" Type="String" DefaultValue='1' />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:SqlDataSource ID="ConditionAdjectivesDS" runat="server" SelectCommandType="StoredProcedure" ConnectionString='<%$ ConnectionStrings:Merchandising %>'
            SelectCommand="builder.getConditionAdjectives" 
            OnSelecting="Conditions_Selecting">
            <SelectParameters>
                <asp:Parameter Name="BusinessUnitID" DefaultValue="-1" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        
        <asp:HiddenField ID="dirtyFlag" runat="server" Value="0" />
        <%--<div id="BuilderPanel">--%>
            <cwc:MobileHeader ID="mhHeader" runat="server" SearchVisible="true" ListVisible="false"/>
            <asp:Panel ID="InvDataPanel" runat="server" CssClass="inventoryData" >
                <table>
	                 <tr><td colspan="2">
	                    <asp:Label runat="server" ID="stockNumberDisplay" CssClass="mobileStockDisplay" Text='<%# InventoryInfo.StockNumber %>'></asp:Label>
                        <asp:Label CssClass="statusLabel" ID="YMMStatusLabel" runat="server" Text='<%# InventoryInfo.Year + " " + InventoryInfo.Make + " " + InventoryInfo.Model %>'></asp:Label>
                        <asp:Label CssClass="statusLabel" ID="TrimStatusLabel" runat="server" Text=""></asp:Label>
	                 </td></tr>
	                 <tr>
	                 <td>
	                 <asp:Label CssClass="smallLabel" ID="VinLabel" runat="server" Text='<%# InventoryInfo.VIN %>'></asp:Label>
	                 </td>
	                 <td colspan="2" class="certifyWorkingTD">
                        <%# InventoryInfo.CertificationTitle %>    
                     </td>
                     </tr>
	                 <tr>
	                    <td>Mileage: <asp:Label CssClass="dataValue" runat="server" id="mileage" Text='<%# InventoryInfo.DisplayMileage %>' ></asp:Label>
	                    </td>
	                    <td><asp:Label CssClass="dataValue" runat="server" ID="Label1" Text='<%# String.Format("{0:c0}", InventoryInfo.ListPrice) %>'></asp:Label></td>
	                 </tr>
                </table>
            </asp:Panel>
            <asp:Button ID="btnSaveContinueTop" runat="server" Text="SAVE & NEXT" OnClick="btnSaveContinue_Click"
                CssClass="wideSaveButton" />
            <asp:Panel runat="server" ID="newTrim" CssClass="mobileTrimBits" >
               <div>
                    <h5>Trim</h5>
                    <asp:DropDownList ID="styleDDL" runat="server" DataSourceID="TrimStyleDS" AutoPostBack="true" DataTextField="Value"
                        DataValueField="Key" OnSelectedIndexChanged="StyleComponentDDL_Changed" OnDataBound="StyleDDL_DataBound" />
                    <asp:Label ID="styleLabelWhenOnlyOnePossible" runat="server" Text=""></asp:Label>        
               </div>
               
               <div class="basicSection">
                <h5>Colors</h5>
                    <span>Exterior:</span>
                    <asp:DropDownList ID="colorSelection" runat="server">
                    </asp:DropDownList> 
                    <span>Interior:</span>
                    <asp:DropDownList ID="intColorSelection" runat="server" ></asp:DropDownList>
                </div>
                
                <div id="conditions" class="basicSection">
                    <h5>Condition</h5>
                    <span>Overall:</span>
                    <asp:DropDownList ID="ConditionDDL" runat="server" DataSourceID="ConditionAdjectivesDS"
                        DataTextField="conditionDescription" DataValueField="conditionDescription"
                        OnDataBound="ConditionAdjectives_Bound"/>
                    <br />
                </div>
            </asp:Panel>
            <asp:Button ID="btnSaveContinue" runat="server" Text="SAVE & NEXT" CssClass="wideSaveButton" OnClick="btnSaveContinue_Click" />
        <%--</div>--%>
    </form>
</body>
</html>
