<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="Spinner.aspx.cs" Inherits="Wanamaker.WebApp.Mobile.Spinner" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Spinner</title>
    <meta name = "viewport" content = "initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
</head>
<body>
<div class="spinDiv">
    <asp:Image ID="spinner" runat="server" />
    <asp:Label ID="SpinnerText" ForeColor="LightGray" runat="server" Text='<%# MessageText %>'></asp:Label>
</div>
</body>
</html>
