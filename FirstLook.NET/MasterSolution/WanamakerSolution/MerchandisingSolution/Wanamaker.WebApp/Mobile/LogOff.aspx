<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="LogOff.aspx.cs" Inherits="Wanamaker.WebApp.Mobile.LogOff" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>MAX Internet Merchandising Manager - Log Off</title>
    <meta name = "viewport" content = "initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <p>Redirecting you to CAS ...</p>
    </div>
    </form>
</body>
</html>
