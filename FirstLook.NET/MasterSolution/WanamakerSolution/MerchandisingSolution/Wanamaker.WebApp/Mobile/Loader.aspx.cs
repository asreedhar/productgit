using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using FirstLook.Common.WebControls.UI;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using VehicleDataAccess;
using Wanamaker.WebApp.AppCode.AccessControl;
using Wanamaker.WebApp.Mobile.Controls;

namespace Wanamaker.WebApp.Mobile
{
    public class Loader : BasePage
    {
        #region Controls

        protected ObjectDataSource AdditionalInfoItems;
        protected CheckBoxList AdditionalInfoItemsList;
        protected UpdatePanel BasicPanel;
        protected TabPanel BasicsTab;
        protected SqlDataSource ConditionAdjectivesDS;
        protected DropDownList ConditionDDL;
        protected ObjectDataSource ConditionTypesDS;
        protected TabPanel ExtTabPanel;
        protected UpdatePanel ExteriorUpdatePanel;
        protected ImageButton ImageButton1;
        protected TabPanel IntTabPanel;
        protected ObjectDataSource InteriorFeaturesDS;

        protected Panel InvDataPanel;
        protected ImageButton Inventory;
        protected Label Label1;
        protected Label Label2;
        protected Label Label3;
        protected UpdatePanel LoadUpdatePanel;
        protected TextBox LoaderNotes;

        protected Repeater NoteRepeater;
        protected TabPanel PackagesTabPanel;
        protected ImageButton SaveButton;
        protected Image SaveOKPlaceholder;
        protected ScriptManager ScriptManager2;
        protected ObjectDataSource StdExteriorDS;


        protected ObjectDataSource StdInteriorDS;
        protected TextBox TireTreadTB;

        protected Label TrimStatusLabel;
        protected ObjectDataSource TrimStyleDS;
        protected ImageButton UndoButton;
        protected UpdatePanel UpdatePanel1;

        protected Label VinLabel;
        protected Label YMMStatusLabel;
        protected Repeater cRptr;
        protected DropDownList colorSelection;
        protected HiddenField dirtyFlag;
        protected Repeater exteriorOp;
        protected ObjectDataSource exteriorOptionsDS;
        protected HtmlForm form2;
        protected DropDownList intColorSelection;
        protected Repeater interiorOp;
        protected MobileHeader mhHeader;

        protected Label mileage;

        protected TabContainer mobileTabs;

        protected Panel newTrim;

        protected ObjectDataSource odsOptions;
        protected Repeater rptPackages;
        protected Panel stdEq;


        protected Panel stdEq2;
        protected Panel stdEq2Ex;
        protected Panel stdEqEx;
        protected Repeater stdEqExt;


        protected Repeater stdEqIn;
        protected Label stockNumberDisplay;
        protected DropDownList styleDDL;

        protected Label styleLabelWhenOnlyOnePossible;
        protected UpdatePanel upPackages;

        #endregion

        private DetailedEquipmentCollection _equipment;
        private InventoryData inventoryInfo;
        private VehicleConfiguration vehConfig;

        private string vin
        {
            get
            {
                object o = ViewState["vin"];
                return (o == null) ? "" : (string) o;
            }
            set { ViewState["vin"] = value; }
        }

        public int BusinessUnitID
        {
            get
            {
                object o = ViewState["BusinessUnitID"];
                return (o == null) ? -1 : (int) o;
            }
            set { ViewState["BusinessUnitID"] = value; }
        }

        public int InventoryId
        {
            get
            {
                object o = ViewState["invId"];
                return (o == null) ? -1 : (int) o;
            }
            set { ViewState["invId"] = value; }
        }

        public int ChromeStyleID
        {
            get
            {
                object o = ViewState["styleID"];
                return ((o == null) ? -1 : (int) o);
            }
            set { ViewState["styleID"] = value; }
        }

        public DetailedEquipmentCollection Equipment
        {
            get
            {
                if (_equipment == null)
                {
                    _equipment = DetailedEquipmentCollection.FetchSelections(BusinessUnitID, InventoryId);
                }
                return _equipment;
            }
            set { _equipment = value; }
        }

        public VehicleConfiguration VehConfig
        {
            get
            {
                if (vehConfig == null)
                {
                    vehConfig = VehicleConfiguration.FetchOrCreateDetailed(BusinessUnitID, InventoryId,
                                                                           Context.User.Identity.Name);
                }
                return vehConfig;
            }
        }

        public AvailableChromeColors Colors
        {
            get
            {
                if (ViewState["Colors"] == null)
                {
                    return null;
                }
                return (AvailableChromeColors) ViewState["Colors"];
            }
            set { ViewState["Colors"] = value; }
        }

        public GenericEquipmentCollection genericSelections
        {
            get
            {
                var de = new GenericEquipmentCollection();
                de.LoadViewState(ViewState["gens"]);
                return de;
            }
            set { ViewState["gens"] = value.SaveViewState(); }
        }

        public ArrayList additionalItems
        {
            get
            {
                object o = ViewState["adds"];
                return ((o == null) ? new ArrayList() : (ArrayList) o);
            }
            set { ViewState["adds"] = value; }
        }

        public InventoryData InventoryInfo
        {
            get
            {
                if (inventoryInfo == null)
                {
                    try
                    {
                        inventoryInfo = InventoryData.Fetch(BusinessUnitID, InventoryId);
                    }
                    catch
                    {
                        Response.Redirect("Inventory.aspx");
                    }
                }

                return inventoryInfo;
            }
            set { inventoryInfo = value; }
        }

        /*PAGE LIFE CYCLE FUNCTIONS */

        protected void Page_PreRender(object sender, EventArgs e)
        {
            var user = Context.Items[IdentityHelper.MembershipContextKey] as MembershipUser;
            if (user == null)
            {
                Response.Redirect("~/Default.aspx");
            }

            //PERFORM IN DESCENDING ORDER TO PREVENT CHANGING TAB INDICES

            if (!(Roles.IsUserInRole(user.UserName, "Administrator") || Roles.IsUserInRole(user.UserName, "Builder")))
            {
                Response.Redirect("~/Default.aspx");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var bu = Context.Items["BusinessUnit"] as BusinessUnit;
                if (bu == null)
                {
                    Response.Redirect("~/Default.aspx");
                }

                BusinessUnitID = bu.Id;

                int invId = -1;

                if (!Int32.TryParse(Request["id"], out invId))
                {
                    Response.Redirect("Inventory.aspx");
                }
                InventoryId = invId;

                setNewVehicle(invId);
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
        }

        protected void UndoChanges_Click(object sender, EventArgs e)
        {
            setNewVehicle(InventoryId);
            mobileTabs.ActiveTabIndex = 0;
        }

        private void SavePackages()
        {
            Equipment.Save(BusinessUnitID, InventoryId);
            Equipment = null;
            rptPackages.DataBind();
        }

        private void SaveOptionsTab()
        {
            genericSelections.Save(BusinessUnitID, InventoryId);
        }

        private void SaveVehicle(Tabs tab)
        {
            switch (tab)
            {
                case Tabs.Packages:
                    SavePackages();
                    break;
                case Tabs.Equipment:
                    SaveOptionsTab();
                    break;
            }


            //dirtyFlag.Value = "0";
        }

        public void Options_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["chromeStyleID"] = ChromeStyleID;
        }


        protected void Package_CheckedChanged(object sender, EventArgs e)
        {
            var changedCB = sender as CheckBoxPlus;
            if (changedCB != null)
            {
                //dirtyFlag.Value = "1";
                //get the category ID from its argument
                string optionCode = changedCB.CheckBoxArgument;
                if (changedCB.Checked)
                {
                    //GenericEquipmentCollection tempC = genericSelections;
                    //if (!tempC.Contains(catID))
                    //{
                    //    tempC.Add(new GenericEquipment(catID));
                    //    genericSelections = tempC;
                    //}

                    string titleTxt = String.Empty;
                    string catList = String.Empty;
                    string useText = String.Empty;
                    EquipmentCollection possible = EquipmentCollection.FetchOptionalByOptionCode(ChromeStyleID,
                                                                                                 String.Empty,
                                                                                                 BusinessUnitID);
                    foreach (Equipment item in possible)
                    {
                        if (item.OptionCode == optionCode)
                        {
                            titleTxt = item.NormalizedDescription;
                            catList = item.CategoryList;
                            useText = item.ExtDescription;
                            break;
                        }
                    }

                    var detailedEquipment = new DetailedEquipment(optionCode, titleTxt, catList, useText, false);
                    DetailedEquipmentCollection tmp = Equipment;
                    tmp.Add(detailedEquipment);
                    Equipment = tmp;
                }
                else
                {
                    ////remove it if not checked
                    //GenericEquipmentCollection tempC = genericSelections;
                    //tempC.Remove(catID);
                    //genericSelections = tempC;
                    DetailedEquipmentCollection tmp = Equipment;
                    tmp.Remove(optionCode);
                    Equipment = tmp;
                }
            }
        }

        protected void Feature_CheckChanged(object sender, EventArgs e)
        {
            //get the changed checkbox...custom checkbox has cmd arg
            var changedCB = sender as CheckBoxPlus;
            if (changedCB != null)
            {
                //dirtyFlag.Value = "1";
                //get the category ID from its argument
                int catID = Int32.Parse(changedCB.CheckBoxArgument);
                if (changedCB.Checked)
                {
                    GenericEquipmentCollection tempC = genericSelections;
                    if (!tempC.Contains(catID))
                    {
                        tempC.Add(new GenericEquipment(catID));
                        genericSelections = tempC;
                    }
                }
                else
                {
                    //remove it if not checked
                    GenericEquipmentCollection tempC = genericSelections;
                    tempC.Remove(catID);
                    genericSelections = tempC;
                }
            }
        }

        protected void CheckBox_Bound(object sender, RepeaterItemEventArgs e)
        {
            var cbp = e.Item.FindControl("OptionCB") as CheckBoxPlus;

            var anchor = e.Item.FindControl("checkButton") as HtmlAnchor;
            var lb = e.Item.FindControl("CheckBoxText") as Label;
            if (cbp == null || anchor == null || lb == null) return;

            if (cbp.Checked)
            {
                anchor.Attributes["class"] = "checkedHolder";
            }
            else
            {
                anchor.Attributes["class"] = "checkHolder";
            }
            lb.Attributes.Add("onclick", "javascript:checkItem('" + cbp.ClientID + "','" + anchor.ClientID + "');");
        }


        public void loadStyleInfo(VehicleConfiguration vehicleConfig, int chromeStyleID)
        {
            ChromeStyleID = chromeStyleID;
            vehicleConfig.UpdateStyleId(BusinessUnitID, chromeStyleID);
            genericSelections = vehicleConfig.VehicleOptions.generics;

            string extColors = ColorPair.GetPairCode(vehicleConfig.ExteriorColor1, vehConfig.ExteriorColor2);
            string intColor = vehConfig.InteriorColor; //interior is the third item - should be productionized


            Colors = AvailableChromeColors.Fetch(chromeStyleID);
            intColorSelection.DataSource = Colors.InteriorColors;
            intColorSelection.DataTextField = "Description";
            intColorSelection.DataValueField = "Description";
            intColorSelection.DataBind();

            var colors = new List<string>();
            foreach (ColorPair pair in Colors.ExteriorColors)
            {
                colors.Add(pair.GetPairDescription());
            }
            colorSelection.DataSource = colors;
            colorSelection.DataBind();
            colorSelection.Items.Insert(0, new ListItem("Choose a color...", "Choose a color..."));

            //default, then look for a match
            intColorSelection.Items.Insert(0, new ListItem("Choose a color...", "Choose a color..."));
            if (!String.IsNullOrEmpty(intColor))
            {
                for (int i = 0; i < Colors.InteriorColors.Count; i++)
                {
                    //if (Colors.InteriorColors[i].ColorCode == InteriorColorCode)
                    if (Colors.InteriorColors[i].Description == intColor &&
                        intColorSelection.Items.FindByValue(intColor) != null)
                    {
                        intColorSelection.SelectedValue = intColor;
                        break;
                    }
                }
            }

            //code to check if the loaded exterior code matches anywhere in the list
            colorSelection.Text = "Choose a color...";
            if (!String.IsNullOrEmpty(extColors))
            {
                for (int i = 0; i < Colors.ExteriorColors.Count; i++)
                {
                    //if (AvailableChromeColors.getPairCode(Colors.ExteriorColors[i]) == ExteriorColorCode)
                    if (Colors.ExteriorColors[i].GetPairDescription() == extColors)
                    {
                        colorSelection.Text = Colors.ExteriorColors[i].GetPairDescription();
                        break;
                    }
                }
            }
        }


        //update view state or selected index after bound
        protected void StyleDDL_DataBound(object sender, EventArgs e)
        {
            if (styleDDL.Items.Count == 1)
            {
                //indexchanged event will increment the step so user doesn't have to see the single style option
                styleDDL.SelectedIndex = 0;

                StyleComponentDDL_Changed(styleDDL, e);
                //ChromeStyleID = Int32.Parse(styleDDL.SelectedValue);
                //loadStyleInfo(ChromeStyleID, "", "");
            }
            else
            {
                styleDDL.Items.Insert(0, new ListItem("Select Trim/Style...", ""));
                if (styleDDL.Items.FindByValue(ChromeStyleID.ToString()) != null)
                {
                    styleDDL.SelectedValue = ChromeStyleID.ToString();
                    StyleComponentDDL_Changed(styleDDL, e);
                }
            }
        }

        protected void StyleComponentDDL_Changed(object sender, EventArgs e)
        {
            if (styleDDL.Items.Count > 1 && styleDDL.SelectedIndex <= 0)
            {
                //can't set trim w/o both selected --but if only one item, then we don't have to choose
                return;
            }
            ChromeStyleID = Int32.Parse(styleDDL.SelectedValue);
            //if using drop downs with Trim and StyleNameWOTrim

            vehConfig = VehicleConfiguration.FetchOrCreateDetailed(BusinessUnitID, InventoryId,
                                                                   Context.User.Identity.Name);
            //add color info to the controls
            loadStyleInfo(vehConfig, ChromeStyleID);
        }

        public void setNewVehicle(int inventoryId)
        {
            //dirtyFlag.Value = "0";
            try
            {
                InventoryId = inventoryId;
                if (!String.IsNullOrEmpty(InventoryInfo.VIN))
                {
                    vin = InventoryInfo.VIN;
                }
                vehConfig = VehicleConfiguration.FetchOrCreateDetailed(BusinessUnitID, InventoryId,
                                                                       Context.User.Identity.Name);
                genericSelections = vehConfig.VehicleOptions.generics;
                additionalItems = new ArrayList(vehConfig.AfterMarketEquipment);

                //check the database for the currently saved style for this vehicle
                ChromeStyleID = vehConfig.ChromeStyleID;
                TrimStatusLabel.Text = string.Empty;
                InvDataPanel.DataBind();
            }
            catch
            {
                //show not found alert
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "alertMe", "alert('Vehicle Not Found')",
                                                            true);
                Response.Redirect("Inventory.aspx");
                return;
            }
        }

        protected void WorkingTab_Changing(object sender, EventArgs e)
        {
            SaveVehicle((Tabs) mobileTabs.ActiveTabIndex);
        }

        protected void WorkingTab_Changed(object sender, EventArgs e)
        {
            //blows up if page is requested because it depends on BusinessUnitID
            if (IsPostBack)
            {
                switch (mobileTabs.ActiveTabIndex)
                {
                    case (int) Tabs.Equipment:
                        exteriorOp.DataBind();
                        interiorOp.DataBind();
                        break;
                }
            }
        }

        protected void Styles_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["VIN"] = vin;
        }

        protected void StylePieces_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["vin"] = vin;
        }

        protected void InteriorRepeater_PreRender(object sender, EventArgs e)
        {
            var rpt = sender as Repeater;
            if (rpt == null) return;

            var vsds = new VINStyleDataSource();

            //get standard equipment

            var ec = vsds.StandardCategoriesSelect(ChromeStyleID, 1);
            ec.Sort(new CategoryCollectionListLengthOrderer());

            foreach (RepeaterItem repeaterItem in rpt.Items)
            {
                var lb = repeaterItem.FindControl("HeaderText") as Label;
                var lbDetails = repeaterItem.FindControl("HeaderDetail") as Label;
                if (lb == null || lbDetails == null) continue;

                try
                {
                    CategoryCollection cc = GetCategoryCollectionByHeader(lb.Text, ec);


                    var sb = new StringBuilder();
                    foreach (CategoryLink cl in cc)
                    {
                        if (!genericSelections.Contains(cl.CategoryID))
                        {
                            sb.Append("<span class=\"strike\">");
                            sb.Append(cl.Description);
                            sb.Append("</span>");
                            sb.Append(", ");
                        }
                        else
                        {
                            sb.Append(cl.Description);
                            sb.Append(", ");
                        }
                    }

                    lbDetails.Text = @"STD: " + sb.ToString(0, sb.Length - 2) + "";
                    ec.Remove(cc);
                }
                catch
                {
                    continue;
                }
            }

            stdEqIn.DataSource = ec;
            stdEqIn.DataBind();
        }

        protected void ExteriorRepeater_PreRender(object sender, EventArgs e)
        {
            var rpt = sender as Repeater;
            if (rpt == null) return;

            var vsds = new VINStyleDataSource();

            //get standard equipment

            var ec = vsds.StandardCategoriesSelect(ChromeStyleID, 2);
            ec.Sort(new CategoryCollectionListLengthOrderer());

            foreach (RepeaterItem repeaterItem in rpt.Items)
            {
                var lb = repeaterItem.FindControl("HeaderText") as Label;
                var lbDetails = repeaterItem.FindControl("HeaderDetail") as Label;
                if (lb == null || lbDetails == null) continue;

                try
                {
                    CategoryCollection cc = GetCategoryCollectionByHeader(lb.Text, ec);


                    var sb = new StringBuilder();
                    foreach (CategoryLink cl in cc)
                    {
                        if (!genericSelections.Contains(cl.CategoryID))
                        {
                            sb.Append("<span class=\"strike\">");
                            sb.Append(cl.Description);
                            sb.Append("</span>");
                            sb.Append(", ");
                        }
                        else
                        {
                            sb.Append(cl.Description);
                            sb.Append(", ");
                        }
                    }

                    lbDetails.Text = @"STD: " + sb.ToString(0, sb.Length - 2) + "";
                    ec.Remove(cc);
                }
                catch
                {
                    continue;
                }
            }

            stdEqExt.DataSource = ec;
            stdEqExt.DataBind();
        }

        private CategoryCollection GetCategoryCollectionByHeader(string headerText,
                                                                 List<CategoryCollection> categoryCollections)
        {
            foreach (CategoryCollection cc in categoryCollections)
            {
                if (cc.HeaderText == headerText)
                {
                    return cc;
                }
            }
            throw new ApplicationException("Category collection not found...");
        }

        protected void AdditionalInfo_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["BusinessUnitID"] = BusinessUnitID;
        }

        protected void Conditions_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@BusinessUnitID"].Value = BusinessUnitID;
        }

        protected void ConditionAdjectives_Bound(object sender, EventArgs e)
        {
            ConditionDDL.Items.Insert(0, new ListItem("Select condition..."));
        }

        protected void Inventory_Click(object sender, EventArgs e)
        {
            Response.Redirect("Inventory.aspx");
        }

        protected string GetClass(object evalObj)
        {
            if (evalObj is int)
            {
                if (genericSelections.Contains((int) evalObj))
                {
                    return "checkedHolder";
                }
                return "checkHolder";
            }
            if (evalObj is string)
            {
                if (genericSelections.Contains((string) evalObj))
                {
                    return "checkedPackage";
                }
                return "checkPackage";
            }
            return "checkHolder";
        }

        protected bool IsSelected(object evalObj)
        {
            if (evalObj is int)
                return genericSelections.Contains((int) evalObj);
            if (evalObj is string)
                return Equipment.ContainsOption((string) evalObj);
            return false;
        }

        protected void Condition_DataBound(object sender, RepeaterItemEventArgs e)
        {
            var conditionId = e.Item.FindControl("ConditionTypeId") as HiddenField;
            var conditionLevel = e.Item.FindControl("ConditionLevel") as HiddenField;

            if (conditionId == null || conditionLevel == null) return;

            int lvl = GetConditionLevel(Int32.Parse(conditionId.Value));
            conditionLevel.Value = lvl.ToString();

            //use level value to set images appropriately
        }

        public int GetConditionLevel(int conditionId)
        {
            return VehConfig.Conditions.GetLevel(conditionId);
        }

        public string GetButtonClass(object evalObj, int buttonLevel)
        {
            int lvl = GetConditionLevel((int) evalObj);
            if (lvl == buttonLevel)
            {
                return "Active";
            }
            return string.Empty;
        }

        public string GetConditionLink(int conditionValue, int conditionId)
        {
            //return "alert('" + conditionValue + " " + conditionId + "');";
            return "javascript:return setConditionValue(this, '" + conditionValue + "', '" + conditionId + "');";
            //needs to get all three images, get hidden value item
            //set all images class to blank
            //set this image to Active
            //set hidden value item value to conditionValue
        }

        //
        // Packages
        //
        protected void OptionList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["chromeStyleId"] = ChromeStyleID;
            e.InputParameters["customizedDescriptionDealerId"] = BusinessUnitID;
        }

        protected void rptPackages_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var cbp = e.Item.FindControl("OptionCB") as CheckBoxPlus;

            var anchor = e.Item.FindControl("checkButton") as HtmlAnchor;
            var lb = e.Item.FindControl("CheckBoxText") as Label;
            if (cbp == null || anchor == null || lb == null) return;

            if (cbp.Checked)
            {
                anchor.Attributes["class"] = "checkedHolder";
            }
            else
            {
                anchor.Attributes["class"] = "checkHolder";
            }
            lb.Attributes.Add("onclick", "javascript:checkItem('" + cbp.ClientID + "','" + anchor.ClientID + "');");
        }

        protected void StdInteriorDS_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["chromeStyleId"] = ChromeStyleID;
        }

        protected void StdExteriorDS_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["chromeStyleId"] = ChromeStyleID;
        }

        protected void btnSaveContinue_Click(object sender, EventArgs e)
        {
            SaveVehicle((Tabs) mobileTabs.ActiveTabIndex);
            switch (mobileTabs.ActiveTabIndex)
            {
                case (int) Tabs.Equipment:
                    interiorOp.DataBind();
                    exteriorOp.DataBind();
                    break;
            }
            if (String.IsNullOrEmpty(mhHeader.SearchText))
                Response.Redirect( "~/Mobile/Home.aspx?list=" + mhHeader.ActiveList );
            else
                Response.Redirect("~/Mobile/Home.aspx");
        }

        #region Nested type: Tabs

        private enum Tabs
        {
            Equipment,
            Packages
        }

        #endregion
    }
}