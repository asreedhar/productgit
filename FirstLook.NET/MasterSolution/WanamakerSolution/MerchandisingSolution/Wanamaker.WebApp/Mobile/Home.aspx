<%@ Page Language="C#" Theme="Shredder" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Wanamaker.WebApp.Mobile.Home" %>
<%@ Register TagPrefix="cwc" TagName="MobileHeader" Src="~/Mobile/Controls/MobileHeader.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Home | MAX Web Loader</title>
    <meta name = "viewport" content = "initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
</head>
<body onorientationchange="updateOrientation();">
    <center>
        <form id="form1" runat="server">
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <div>
                <cwc:MobileHeader ID="mhHeader" runat="server" SearchVisible="false" ListVisible="false"/>
                <br />
                <ajax:TextBoxWatermarkExtender ID="txtWatermarkExt" runat="server" TargetControlID="txtSearch" WatermarkText="Stock #" />
                <asp:TextBox ID="txtSearch" runat="server" Width="300"></asp:TextBox>
                <br /><br />
                <asp:Button ID="btnSearch" runat="server" Text="SEARCH" Font-Size="X-Large" Width="200" OnClick="btnSearch_Click"/>
                <br /><br /><br /><br />
                <div class="button">
                    <asp:LinkButton ID="btnAllVehicles" runat="server" Text="Newly Received: ALL Vehicles<br />USED VEHICLES"
                        Width="300" OnClick="btnAllVehicles_Click" Font-Size="Medium"/>
                </div>
                <br />
                <div class="button">
                    <asp:LinkButton ID="btnInitialAdNeeded" runat="server" Text="Newly Received: Initial Ad Needed<br />USED VEHICLES"
                        Width="300" OnClick="btnInitialAdNeeded_Click" Font-Size="Medium"/>
                </div>
                <br />
                <div class="button">
                    <asp:LinkButton ID="btnEquipmentNeeded" runat="server" Text="Newly Received: Equipment Needed<br />USED VEHICLES"
                        Width="300" OnClick="btnEquipmentNeeded_Click" Font-Size="Medium"/>
                </div>
            </div>
        </form>
    </center>
</body>
</html>
