using System;
using System.Collections.Generic;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;

namespace Wanamaker.WebApp.Mobile
{
    public partial class Home : BasePage
    {
        public int BusinessUnitID
        {
            get
            {
                object o = ViewState["BusinessUnitID"];
                return (o == null) ? -1 : (int)o;
            }
            set
            {
                ViewState["BusinessUnitID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BusinessUnit bu = Context.Items["BusinessUnit"] as BusinessUnit;
                if (bu == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                BusinessUnitID = bu.Id;
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            var allInventory = InventoryDataFilter.FetchListFromSession(BusinessUnitID);

            Bin bin = Bin.Create(txtSearch.Text);
            List<InventoryData> searchResults = InventoryDataFilter.WorkflowSearch(BusinessUnitID, allInventory, 0, 9999, String.Empty, bin);

            if (searchResults.Count > 0)
                Response.Redirect("~/Mobile/Basics.aspx?id=" + searchResults[0].InventoryID + "&search=" + txtSearch.Text);
            else
                txtSearch.Text = "";
        }
        protected void btnEquipmentNeeded_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Mobile/Inventory.aspx?list=0");
        }
        protected void btnInitialAdNeeded_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Mobile/Inventory.aspx?list=1");
        }
        protected void btnAllVehicles_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Mobile/Inventory.aspx?list=2");
        }
    }
}
