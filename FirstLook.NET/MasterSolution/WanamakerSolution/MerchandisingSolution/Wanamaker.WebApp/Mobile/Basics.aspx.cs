using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.Security;
using System.Web.UI.WebControls;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using Wanamaker.WebApp.AppCode.AccessControl;

namespace Wanamaker.WebApp.Mobile
{
    public partial class Basics : BasePage
    {
        #region Private Fields
        private VehicleConfiguration vehConfig;
        private string vin
        {
            get
            {
                object o = ViewState["vin"];
                return (o == null) ? "" : (string)o;
            }
            set
            {
                ViewState["vin"] = value;
            }
        }
        private InventoryData inventoryInfo;
        #endregion

        #region Public Properties
        public InventoryData InventoryInfo
        {
            get
            {
                if (inventoryInfo == null)
                {
                    try
                    {
                        inventoryInfo = InventoryData.Fetch(BusinessUnitID, InventoryId);
                    }
                    catch
                    {
                        Response.Redirect("Inventory.aspx");
                    }
                }

                return inventoryInfo;

            }
            set
            {
                inventoryInfo = value;
            }
        }
        public VehicleConfiguration VehConfig
        {
            get
            {
                if (vehConfig == null)
                {
                    vehConfig = VehicleConfiguration.FetchOrCreateDetailed(BusinessUnitID, InventoryId, Context.User.Identity.Name);
                }
                return vehConfig;
            }
        }
        public int ChromeStyleID
        {
            get
            {
                object o = ViewState["styleID"];
                return ((o == null) ? -1 : (int)o);
            }
            set { ViewState["styleID"] = value; }
        }
        public int BusinessUnitID
        {
            get
            {
                object o = ViewState["BusinessUnitID"];
                return (o == null) ? -1 : (int)o;
            }
            set
            {
                ViewState["BusinessUnitID"] = value;
            }
        }
        public int InventoryId
        {
            get
            {
                object o = ViewState["invId"];
                return (o == null) ? -1 : (int)o;

            }
            set { ViewState["invId"] = value; }
        }
        public GenericEquipmentCollection genericSelections
        {
            get
            {
                GenericEquipmentCollection de = new GenericEquipmentCollection();
                de.LoadViewState(ViewState["gens"]);
                return de;
            }
            set
            {
                ViewState["gens"] = value.SaveViewState();
            }
        }
        public AvailableChromeColors Colors
        {
            get
            {
                if (ViewState["Colors"] == null)
                {
                    return null;
                }
                else
                {
                    return (AvailableChromeColors)ViewState["Colors"];
                }
            }
            set
            {
                ViewState["Colors"] = value;
            }
        }
        public ArrayList additionalItems
        {
            get
            {
                object o = ViewState["adds"];
                return ((o == null) ? new ArrayList() : (ArrayList)o);
            }
            set { ViewState["adds"] = value; }
        }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BusinessUnit bu = Context.Items["BusinessUnit"] as BusinessUnit;
                if (bu == null)
                {
                    Response.Redirect("~/Default.aspx");
                }

                BusinessUnitID = bu.Id;

                int invId = -1;

                if (!Int32.TryParse(Request["id"], out invId))
                {
                    Response.Redirect("~/Mobile/Home.aspx");
                }
                InventoryId = invId;

                setNewVehicle(invId);

            }

        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            MembershipUser user = Context.Items[IdentityHelper.MembershipContextKey] as MembershipUser;
            if (user == null)
            {
                Response.Redirect("~/Default.aspx");
            }

            //PERFORM IN DESCENDING ORDER TO PREVENT CHANGING TAB INDICES

            if (!(Roles.IsUserInRole(user.UserName, "Administrator") || Roles.IsUserInRole(user.UserName, "Builder")))
            {
                Response.Redirect("~/Default.aspx");

            }

        }
        protected void StyleDDL_DataBound(object sender, EventArgs e)
        {
            if (styleDDL.Items.Count == 1)
            {
                //indexchanged event will increment the step so user doesn't have to see the single style option
                styleDDL.SelectedIndex = 0;

                StyleComponentDDL_Changed(styleDDL, e);
                //ChromeStyleID = Int32.Parse(styleDDL.SelectedValue);
                //loadStyleInfo(ChromeStyleID, "", "");

            }
            else
            {
                styleDDL.Items.Insert(0, new ListItem("Select Trim/Style...", ""));
                if (styleDDL.Items.FindByValue(ChromeStyleID.ToString()) != null)
                {
                    styleDDL.SelectedValue = ChromeStyleID.ToString();
                    StyleComponentDDL_Changed(styleDDL, e);

                }
            }
        }
        protected void StyleComponentDDL_Changed(object sender, EventArgs e)
        {
            if (styleDDL.Items.Count > 1 && styleDDL.SelectedIndex <= 0)
            {
                //can't set trim w/o both selected --but if only one item, then we don't have to choose
                return;
            }
            ChromeStyleID = Int32.Parse(styleDDL.SelectedValue);
            //if using drop downs with Trim and StyleNameWOTrim

            vehConfig = VehicleConfiguration.FetchOrCreateDetailed(BusinessUnitID, InventoryId, Context.User.Identity.Name);
            //add color info to the controls
            loadStyleInfo(vehConfig, ChromeStyleID);
        }
        protected void StylePieces_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["vin"] = vin;
        }
        protected void Conditions_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@BusinessUnitID"].Value = BusinessUnitID;
        }
        protected void ConditionAdjectives_Bound(object sender, EventArgs e)
        {
            ConditionDDL.Items.Insert(0, new ListItem("Select condition..."));
        }
        protected void btnSaveContinue_Click(object sender, EventArgs e)
        {
            SaveBasics();
            Response.Redirect("~/Mobile/Loader.aspx?id=" + InventoryId + "&search=" + mhHeader.SearchText + "&list=" + mhHeader.ActiveList);
        }
        #endregion

        #region Public Methods
        public void loadStyleInfo(VehicleConfiguration vehicleConfig, int chromeStyleID)
        {
            ChromeStyleID = chromeStyleID;
            vehicleConfig.UpdateStyleId(BusinessUnitID, chromeStyleID);
            genericSelections = vehicleConfig.VehicleOptions.generics;

            string extColors = ColorPair.GetPairCode(vehicleConfig.ExteriorColor1, vehConfig.ExteriorColor2);
            string intColor = vehConfig.InteriorColor;  //interior is the third item - should be productionized


            Colors = AvailableChromeColors.Fetch(chromeStyleID);
            intColorSelection.DataSource = Colors.InteriorColors;
            intColorSelection.DataTextField = "Description";
            intColorSelection.DataValueField = "Description";
            intColorSelection.DataBind();

            List<string> colors = new List<string>();
            foreach (ColorPair pair in Colors.ExteriorColors)
            {

                colors.Add(pair.GetPairDescription());
            }
            colorSelection.DataSource = colors;
            colorSelection.DataBind();
            colorSelection.Items.Insert(0, new ListItem("Choose a color...", "Choose a color..."));

            //default, then look for a match
            intColorSelection.Items.Insert(0, new ListItem("Choose a color...", "Choose a color..."));
            if (!String.IsNullOrEmpty(intColor))
            {
                for (int i = 0; i < Colors.InteriorColors.Count; i++)
                {
                    //if (Colors.InteriorColors[i].ColorCode == InteriorColorCode)
                    if (Colors.InteriorColors[i].Description == intColor && intColorSelection.Items.FindByValue(intColor) != null)
                    {
                        intColorSelection.SelectedValue = intColor;
                        break;
                    }
                }
            }

            //code to check if the loaded exterior code matches anywhere in the list
            colorSelection.Text = "Choose a color...";
            if (!String.IsNullOrEmpty(extColors))
            {
                for (int i = 0; i < Colors.ExteriorColors.Count; i++)
                {
                    //if (AvailableChromeColors.getPairCode(Colors.ExteriorColors[i]) == ExteriorColorCode)
                    if (Colors.ExteriorColors[i].GetPairDescription() == extColors)
                    {
                        colorSelection.Text = Colors.ExteriorColors[i].GetPairDescription();
                        break;
                    }
                }
            }
        }
        public void setNewVehicle(int inventoryId)
        {
            dirtyFlag.Value = "0";

            try
            {
                InventoryId = inventoryId;
                if (!String.IsNullOrEmpty(InventoryInfo.VIN))
                {
                    vin = InventoryInfo.VIN;
                }

                vehConfig = VehicleConfiguration.FetchOrCreateDetailed(BusinessUnitID, InventoryId, Context.User.Identity.Name);
                genericSelections = vehConfig.VehicleOptions.generics;

                additionalItems = new ArrayList(vehConfig.AfterMarketEquipment);

                //check the database for the currently saved style for this vehicle
                ChromeStyleID = vehConfig.ChromeStyleID;

                TrimStatusLabel.Text = string.Empty;

                InvDataPanel.DataBind();

                ConditionDDL.DataBind();
                //set value for interiorType and Condition if they are in the list
                if (!String.IsNullOrEmpty(vehConfig.VehicleCondition) && ConditionDDL.Items.FindByValue(vehConfig.VehicleCondition) != null)
                {
                    ConditionDDL.SelectedValue = vehConfig.VehicleCondition;
                }
                else
                {
                    ConditionDDL.SelectedIndex = 0;
                }
                styleDDL.DataBind();
            }
            catch
            {
                //show not found alert
                this.Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "alertMe", "alert('Vehicle Not Found')", true);
                Response.Redirect("Inventory.aspx");
                return;
            }
        }
        #endregion

        #region Private Methods
        private void SaveBasics()
        {
            vehConfig = VehicleConfiguration.FetchOrCreateDetailed(BusinessUnitID, InventoryId, Context.User.Identity.Name);
            vehConfig.SetChromeStyleID(BusinessUnitID, InventoryId, ChromeStyleID);

            string memberName = Context.User.Identity.Name;

            string selectedIntColor = "";
            string selectedExtColor1 = "";
            string selectedExtColor2 = "";
            if (!intColorSelection.SelectedItem.Text.Contains("Choose"))
            {
                selectedIntColor = intColorSelection.SelectedItem.Text;
            }
            if (!colorSelection.Text.Contains("Choose") && !String.IsNullOrEmpty(colorSelection.Text))
            {
                string[] colors = colorSelection.Text.Split("|".ToCharArray());
                selectedExtColor1 = colors[0];
                if (colors.Length > 1)
                {
                    selectedExtColor2 = colors[1];
                }
            }

            vehConfig.ExteriorColor1 = selectedExtColor1;
            vehConfig.ExteriorColor2 = selectedExtColor2;
            vehConfig.InteriorColor = selectedIntColor;

            if (ConditionDDL.SelectedIndex == 0)
            {
                vehConfig.VehicleCondition = "";
            }
            else
            {
                vehConfig.VehicleCondition = ConditionDDL.SelectedValue;
            }

            vehConfig.Save(BusinessUnitID, memberName);
        }
        #endregion
    }
}
