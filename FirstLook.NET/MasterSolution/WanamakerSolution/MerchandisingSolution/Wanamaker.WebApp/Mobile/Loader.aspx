<%@ Page Language="C#"  AutoEventWireup="True" CodeBehind="Loader.aspx.cs" Inherits="Wanamaker.WebApp.Mobile.Loader" EnableTheming="true" EnableEventValidation="false" Theme="Shredder"%>
<%@ Register TagPrefix="cwc" TagName="MobileHeader" Src="~/Mobile/Controls/MobileHeader.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" id="vehBuilder" >
<head runat="server">
    <title>Equipment &amp; Packages | MAX Web Loader</title>
    <meta name = "viewport" content = "initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />          

</head>
<body class="mobileInventoryBody" onorientationchange="updateOrientation();">
 
   <form id="form2" runat="server">
 <asp:ScriptManager ID="ScriptManager2" runat="server" LoadScriptsBeforeUI="false">
    <Scripts>
        <asp:ScriptReference Path="~/Public/Scripts/global.debug.js" />
        <asp:ScriptReference Path="~/Public/Scripts/Loader.debug.js" />
    </Scripts>
 </asp:ScriptManager>


<asp:UpdatePanel ID="LoadUpdatePanel" RenderMode="Block" runat="server">
   <ContentTemplate>

    
<%--<asp:HiddenField ID="dirtyFlag" runat="server" Value="0" />--%>

<%--<div id="BuilderPanel">--%>
<cwc:MobileHeader ID="mhHeader" runat="server" SearchVisible="true" ListVisible="false"/>

<asp:Panel ID="InvDataPanel" runat="server" CssClass="inventoryData" >
     <table>
	     <tr><td colspan="2">
	        <asp:Label runat="server" ID="stockNumberDisplay" CssClass="mobileStockDisplay" Text='<%# InventoryInfo.StockNumber %>'></asp:Label>
            <asp:Label CssClass="statusLabel" ID="YMMStatusLabel" runat="server" Text='<%# InventoryInfo.Year + " " + InventoryInfo.Make + " " + InventoryInfo.Model %>'></asp:Label>
            <asp:Label CssClass="statusLabel" ID="TrimStatusLabel" runat="server" Text=""></asp:Label>
            <asp:HyperLink ID="lnkEditVehicle" runat="server" Text="edit" ForeColor="blue" Font-Size="Smaller" Font-Underline="True"
                NavigateUrl='<%# "~/Mobile/Basics.aspx?id=" + InventoryInfo.InventoryID %>' />
	     </td></tr>
	     <tr>
	     <td>
	     <asp:Label CssClass="smallLabel" ID="VinLabel" runat="server" Text='<%# InventoryInfo.VIN %>'></asp:Label>
	     </td>
	     <td colspan="2" class="certifyWorkingTD">
            <%# InventoryInfo.CertificationTitle %>    
         </td>
         </tr>
	     <tr>
	        <td>Mileage: <asp:Label CssClass="dataValue" runat="server" id="mileage" Text='<%# InventoryInfo.DisplayMileage %>' ></asp:Label>
	        </td>
	        <td><asp:Label CssClass="dataValue" runat="server" ID="Label1" Text='<%# String.Format("{0:c0}", InventoryInfo.ListPrice) %>'></asp:Label></td>
	     </tr>
    </table>
</asp:Panel>

 

<asp:Button ID="btnSaveContinueTop" runat="server" Text="SAVE & DONE" CssClass="wideSaveButton" OnClick="btnSaveContinue_Click" />
<div class="paddedContainer">
    <cwc:TabContainer runat="server" ID="mobileTabs" CssClass="mobileEquipmentTabs"  HasEmptyState="false"
        ActiveTabIndex="0"  OnActiveTabChanged="WorkingTab_Changed" OnActiveTabChanging="WorkingTab_Changing" >
    <%--------------------------------------------------------------------------------------Equipment Below Here--------------------------------------------------------------------------------%>
    <cwc:TabPanel ID="ExtTabPanel" CssClass="EquipmentTab" runat="server" Text="Equipment" >
        
        <%--<asp:UpdatePanel ID="ExteriorUpdatePanel" RenderMode="Block" runat="server" UpdateMode="Conditional">
        <ContentTemplate>--%>

        <asp:ObjectDataSource ID="exteriorOptionsDS" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.VINStyleDataSource"
             SelectMethod="lengthSortedKeyFeaturesCategoriesSelect" OnSelecting="Options_Selecting">
            <SelectParameters>
                <asp:Parameter Name="chromeStyleID" Type="Int32" />
                <asp:Parameter Name="categoryCategory" Type="Int32" DefaultValue="2" />
            </SelectParameters>     
        </asp:ObjectDataSource>
        <h3>Exterior</h3>
        <asp:Repeater ID="exteriorOp" runat="server" OnPreRender="ExteriorRepeater_PreRender" DataSourceID="exteriorOptionsDS">
            <ItemTemplate>
                <div class="opGroup">
                    <asp:Label ID="HeaderText" runat="server" Text='<%# Eval("headerText") %>' CssClass="grpHead" ></asp:Label>
                    <asp:Repeater runat="server" ID="innerRepeater" DataSource='<%# Container.DataItem %>' OnItemDataBound="CheckBox_Bound">
                    <ItemTemplate>
                        <vda:CheckBoxPlus CheckBoxArgument='<%# Eval("CategoryID") %>' Checked='<%# IsSelected(Eval("CategoryID")) %>' ID="OptionCB" runat="server" OnCheckedChanged="Feature_CheckChanged" ClientIDMode="AutoID"  />
                        <a id="checkButton" class='<%# GetClass(Eval("CategoryID")) %>' runat="server" ClientIDMode="AutoID" >
                            <asp:Label ID="CheckBoxText" runat="server" Text='<%# Eval("Description") %>'  ></asp:Label>
                        </a>
                    </ItemTemplate>
                    </asp:Repeater>
                    <asp:Label ID="HeaderDetail" runat="server" CssClass="stdList" ></asp:Label>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        
        <asp:Panel ID="stdEqEx" CssClass="stdEquipViewer" runat="server">
            <asp:Label ID="Label3" runat="server" CssClass="grpHead">Other Standard Equipment</asp:Label>
            
        </asp:Panel>

        <asp:Panel ID="stdEq" runat="server" CssClass="stdFeatures">
            <asp:Repeater ID="stdEqExt" runat="server" >
            <ItemTemplate>
            <ul class="stdBox">
                <li><%# Eval("headerText") %></li>
                <asp:Repeater runat="server" ID="nr" DataSource='<%# Container.DataItem %>' >
                <ItemTemplate>
                    <li>
                    <vda:CheckBoxPlus 
                        CheckBoxArgument='<%# Eval("CategoryID") %>' 
                        Checked='<%# IsSelected(Eval("CategoryID")) %>' 
                        ID="StdCB" 
                        runat="server" 
                        OnCheckedChanged="Feature_CheckChanged"
                        Text='<%# Eval("Description") %>'
                          />
                    </li>                    
                </ItemTemplate>
                </asp:Repeater>
            </ul>
            </ItemTemplate>
            </asp:Repeater>
    </asp:Panel>

<asp:ObjectDataSource ID="InteriorFeaturesDS" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.VINStyleDataSource"
             SelectMethod="lengthSortedKeyFeaturesCategoriesSelect" OnSelecting="Options_Selecting">
                <SelectParameters>
                    <asp:Parameter Name="chromeStyleID" Type="Int32" />
                    <asp:Parameter Name="categoryCategory" Type="Int32" DefaultValue="1" />
                </SelectParameters>     
            </asp:ObjectDataSource>
            <br />
            <h3>Interior</h3>
            <asp:Repeater ID="interiorOp" runat="server" OnPreRender="InteriorRepeater_PreRender" DataSourceID="InteriorFeaturesDS">
                 <ItemTemplate>
                <div class="opGroup">
                    <asp:Label ID="HeaderText" runat="server" Text='<%# Eval("headerText") %>' CssClass="grpHead" ></asp:Label>
                    
                    <asp:Repeater runat="server" ID="nr" DataSource='<%# Container.DataItem %>' OnItemDataBound="CheckBox_Bound">
                    <ItemTemplate>
                    
                        <vda:CheckBoxPlus CheckBoxArgument='<%# Eval("CategoryID") %>' Checked='<%# IsSelected(Eval("CategoryID")) %>' ID="OptionCB" runat="server" OnCheckedChanged="Feature_CheckChanged" ClientIDMode="AutoID"  />
                        <a id="checkButton" class='<%# GetClass(Eval("CategoryID")) %>' runat="server" ClientIDMode="AutoID">
                        <asp:Label ID="CheckBoxText" runat="server" Text='<%# Eval("Description") %>'  ></asp:Label>
                        </a>
                    </ItemTemplate>
                    
                    </asp:Repeater>
                    <asp:Label ID="HeaderDetail" runat="server" CssClass="stdList" ></asp:Label>
                </div>
            </ItemTemplate>
           </asp:Repeater>
           
        
        <asp:Panel ID="stdEq2Ex" CssClass="stdEquipViewer" runat="server">
            <asp:Label ID="Label2" runat="server" CssClass="grpHead">Other Standard Equipment</asp:Label>
        </asp:Panel>
        
        <asp:Panel ID="stdEq2" runat="server" CssClass="stdFeatures">
    
            <asp:Repeater ID="stdEqIn" runat="server" >
            <ItemTemplate>
            <ul class="stdBox">
                <li><%# Eval("headerText") %></li>
                <asp:Repeater runat="server" ID="innerRepeater" DataSource='<%# Container.DataItem %>' >
                <ItemTemplate>
                    <li><vda:CheckBoxPlus 
                        CheckBoxArgument='<%# Eval("CategoryID") %>' 
                        Checked='<%# IsSelected(Eval("CategoryID")) %>' 
                        ID="StdCB" 
                        runat="server" 
                        OnCheckedChanged="Feature_CheckChanged"
                        Text='<%# Eval("Description") %>'
                          />    </li>
                </ItemTemplate>
                </asp:Repeater>
            </ul>
            </ItemTemplate>
            </asp:Repeater>
            
        </asp:Panel>

    <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
    
    
    </cwc:TabPanel>
    <%--------------------------------------------------------------------------------------Equipment Above Here--------------------------------------------------------------------------------%>
    <%--------------------------------------------------------------------------------------Packages Below Here--------------------------------------------------------------------------------%>
    <cwc:TabPanel ID="PackagesTabPanel" CssClass="EquipmentTab" runat="server" Text="Packages">
        <%--<asp:UpdatePanel ID="upPackages" RenderMode="Block" runat="server" UpdateMode="Conditional">
            <ContentTemplate>--%>
                <asp:ObjectDataSource ID="odsOptions" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.EquipmentCollection" SelectMethod="FetchOptional" OnSelecting="OptionList_Selecting">
                    <SelectParameters>
                        <asp:Parameter Name="searchText" Type="String" DefaultValue="" ConvertEmptyStringToNull="true" />
                        <asp:Parameter Name="customizedDescriptionDealerId" Type="Int32" DefaultValue="" ConvertEmptyStringToNull="true" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                
                <div class="opGroup">
                    <asp:Repeater runat="server" ID="rptPackages" DataSourceID="odsOptions" OnItemDataBound="rptPackages_ItemDataBound">
                        <ItemTemplate>
                            <vda:CheckBoxPlus CheckBoxArgument='<%# Eval("OptionCode") %>' Checked='<%# IsSelected(Eval("OptionCode")) %>' ID="OptionCB" runat="server" OnCheckedChanged="Package_CheckedChanged" ClientIDMode="AutoID" />
                            <a id="checkButton" class='checkedHolder' runat="server" style="width:301px;" ClientIDMode="AutoID">
                                <asp:Label ID="CheckBoxText" runat="server" Text='<%# Eval("NormalizedDescriptionAbbreviated") %>' Width="245px"></asp:Label>
                            </a>
                            <asp:HiddenField ID="hdnTitle" runat="server" Value='<%# Eval("NormalizedDescription") %>' />
                            <asp:HiddenField ID="hdnDescription" runat="server" Value='<%# Eval("ExtDescription") %>' />
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            <%--</ContentTemplate>
        </asp:UpdatePanel>--%>
    </cwc:TabPanel>
    <%--------------------------------------------------------------------------------------Packages Above Here--------------------------------------------------------------------------------%>
    </cwc:TabContainer>
    
    <asp:Button ID="btnSaveContinue" runat="server" Text="SAVE & DONE" CssClass="wideSaveButton" OnClick="btnSaveContinue_Click" />
    
    </div>
 
 <div style="clear:both"></div>
<%-- </div>--%>
 

    <div style="clear:both"></div>


</ContentTemplate>
</asp:UpdatePanel>
<asp:SqlDataSource ID="ConditionAdjectivesDS" runat="server" SelectCommandType="StoredProcedure" ConnectionString='<%$ ConnectionStrings:Merchandising %>'
        SelectCommand="builder.getConditionAdjectives" 
        OnSelecting="Conditions_Selecting">
        
    <SelectParameters>
        <asp:Parameter Name="BusinessUnitID" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>

<asp:ObjectDataSource ID="StdExteriorDS" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.VINStyleDataSource"
    SelectMethod="standardCategoriesSelect" OnSelecting="StdExteriorDS_Selecting">
    <SelectParameters>
        <asp:Parameter Name="categoryCategory" Type="Int32" DefaultValue="2" />
    </SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="StdInteriorDS" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.VINStyleDataSource"
    SelectMethod="standardCategoriesSelect" OnSelecting="StdInteriorDS_Selecting">
    <SelectParameters>
        <asp:Parameter Name="categoryCategory" Type="Int32" DefaultValue="1" />
    </SelectParameters>
</asp:ObjectDataSource>

    
</form>
</body>
</html>