<%@ Import Namespace="System.Drawing"%>
<%@ Import Namespace="VehicleDataAccess"%>
<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="Inventory.aspx.cs" Inherits="Wanamaker.WebApp.Mobile.Inventory" EnableTheming="true" Theme="Shredder" %>
<%@ Register TagPrefix="cwc" TagName="MobileHeader" Src="~/Mobile/Controls/MobileHeader.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" id="vehBuilder">
<head id="Head1" runat="server">
    <title>Inventory | MAX Web Loader</title>
    <meta name = "viewport" content = "initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
    <link rel="shortcut icon" href="../App_Themes/MAX60/images/max.ico" type="image/x-icon" />
</head>
<body class="mobileInventoryBody">
   <form id="form2" runat="server">
    <asp:ObjectDataSource runat="server" ID="InventoryDSList"
        TypeName="FirstLook.Merchandising.DomainModel.Vehicles.InventoryDataSource"  
        OnSelecting="InventoryDSList_Selecting"
        SelectMethod="WorkflowInventorySubsetSelect"
        SelectCountMethod="InventoryCount" 
        EnablePaging="True"
        StartRowIndexParameterName="startIndex"
        MaximumRowsParameterName="pageSize"
        SortParameterName="sortExpression" >
    <SelectParameters>
        <asp:Parameter Name="businessUnitID" Type="Int32" />
        <asp:Parameter Name="usedOrNewFilter" Type="Int32" DefaultValue="2" />
    </SelectParameters>
    </asp:ObjectDataSource>
      
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
<cwc:MobileHeader ID="mhHeader" runat="server" SearchVisible="true" ListVisible="true" OnActiveListChanged="mhHeader_ActiveListChanged"/>
<asp:Panel runat="server" ID="StagingVehicle" Visible="false" >
    <h3>Add new vehicle to inventory?</h3>
    <h4>
    If a vehicle is not yet in the inventory list, you can create one below:
    </h4>
    <ul style="text-align:left;">
    <li>
        <span class="formLabel">VIN:</span><asp:TextBox ID="vinStaging" Text="" runat="server" ></asp:TextBox>
    </li>
    <li>
        <span class="formLabel">Stock #:</span><asp:TextBox ID="stockNumberStaging" Text="" runat="server" ></asp:TextBox>
    </li>
    <li>
        <asp:Button ID="CreateStaging" runat="server" Text="Create New Vehicle" OnClick="CreateStaging_Click" />
    </li>
    </ul>
    
</asp:Panel>

<asp:Panel runat="server" ID="InventoryGridBox" CssClass="InventoryGridBox">
    <asp:GridView ID="InventoryTable" DataSourceID="InventoryDSList" runat="server" 
        AutoGenerateColumns="False" GridLines="Vertical" OnSorting="InventoryTable_Sorting"
        Font-Size="Small" CssClass="InventorySummaryList" AllowPaging="False" PageSize="20" BorderColor="lightgray"
        HeaderStyle-CssClass="header" AllowSorting="true" HeaderStyle-HorizontalAlign="Left"
        OnRowDataBound="InventoryRow_DataBound" DataKeyNames="InventoryId" RowStyle-CssClass="InventoryRow" Width="100%">
    <Columns>
        <asp:BoundField DataField="Age" HeaderText="Age" SortExpression="InventoryReceivedDate" ItemStyle-HorizontalAlign="Center" />
        <asp:BoundField DataField="StockNumber" HeaderText="Stock #" SortExpression="StockNumber" ItemStyle-HorizontalAlign="Center" />
        <asp:TemplateField HeaderText="Year Make Model" SortExpression="vehicleYear">
            <ItemTemplate>
                <asp:LinkButton ID="btnVehicle" runat="server" CommandArgument='<%# Eval("InventoryId") %>' OnClick="btnVehicle_Click"
                    Text='<%# Eval("YearMakeModel") %>' ForeColor="Maroon"/>
                <br />
                <asp:Label ID="lblMileage" runat="server" Text='<%# Eval("DisplayMileage")+"mi"%>'/>
                <asp:Label ID="lblPrice" runat="server" Text='<%# String.Format("{0:C0}",Eval("ListPrice"))%>' style="float:right;"/>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#EFF3FB" />
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle BackColor="#485775" Font-Bold="True" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <asp:Panel ID="CustomPager" runat="server">
    <div class="gridViewRowCount" >        
        <asp:Button ID="ShowAllButton" runat="server" Text="Show All" OnClick="GridShowAll_Click" />
        <asp:Label ID="startVehNdx" runat="server"></asp:Label>
        -
        <asp:Label ID="endVehNdx" runat="server"></asp:Label>
        of
        <asp:Label ID="totalVeh" runat="server"></asp:Label>
        Vehicles
        
    </div>
   <asp:Panel ID="PagesHolder" CssClass="gridPager" runat="server"></asp:Panel>
   </asp:Panel>
   
   <asp:Button CssClass="Logoff" runat="server" Text="Log out" OnClick="LogOff_Click"></asp:Button>
 </asp:Panel>   

</form>
</body>
</html>

