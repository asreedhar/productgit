<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MobileHeader.ascx.cs" Inherits="Wanamaker.WebApp.Mobile.Controls.MobileHeader" %>

<asp:Panel ID="pnlHeader" runat="server" BorderColor="gray" BorderStyle="solid" BorderWidth="1px"
    style="background:url('../App_Themes/Shredder/images/bg_gradient-left.png') repeat-x scroll 0 0 transparent;">
    <div id="divTop" runat="server" style="padding:3px; display:block;">
        <asp:HyperLink ID="lnkMaxAd" runat="server" Text="MAX Web Loader" NavigateUrl="~/Mobile/Home.aspx" style="float:left;" ForeColor="White"
            Font-Underline="True" Font-Bold="True" />
        <asp:HyperLink ID="lnkDealer" runat="server" Text="Current Dealer" style="float:right; text-decoration:none;"
            Font-Bold="True" Font-Size="small" ForeColor="white" NavigateUrl="~/Mobile/DealerSelector.aspx"></asp:HyperLink>
        &nbsp;
    </div>
    <div id="divSearch" runat="server" style="padding:3px; display:block;">
        <ajax:TextBoxWatermarkExtender ID="txtWatermarkExt" runat="server" TargetControlID="txtSearch" WatermarkText="Stock #" />
        <asp:TextBox ID="txtSearch" runat="server" Width="227px"></asp:TextBox>
        <asp:Button ID="btnSearch" runat="server" Text="SEARCH" Width="75" OnClick="btnSearch_Click"/>
    </div>
    <div id="divList" runat="server" style="padding:3px; display:block;">
        <asp:DropDownList ID="ddlList" runat="server" Width="100%" OnSelectedIndexChanged="ddlList_SelectedIndexChanged"
            AutoPostBack="True">
            <asp:ListItem Text="Newly Received: ALL Vehicles" Value="2"></asp:ListItem>
            <asp:ListItem Text="Newly Received: Initial Ad Needed" Value="1"></asp:ListItem>
            <asp:ListItem Text="Newly Received: Equipment Needed" Value="0"></asp:ListItem>
        </asp:DropDownList>
    </div>
</asp:Panel>