using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using VehicleDataAccess;

namespace Wanamaker.WebApp.Mobile.Controls
{
    public partial class MobileHeader : System.Web.UI.UserControl
    {
        private bool searchVisible = true;
        private bool listVisible = true;

        public bool SearchVisible
        {
            get
            {
                return searchVisible;
            }
            set
            {
                searchVisible = value;
            }
        }
        public bool ListVisible
        {
            get
            {
                return listVisible;
            }
            set
            {
                listVisible = value;
            }
        }
        public string SearchText
        {
            get { return txtSearch.Text; }
        }
        public int ActiveList
        {
            get { return Convert.ToInt32(ddlList.SelectedValue); }
        }
        public int BusinessUnitID
        {
            get
            {
                object o = ViewState["BusinessUnitID"];
                return (o == null) ? -1 : (int)o;
            }
            set
            {
                ViewState["BusinessUnitID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            divSearch.Visible = SearchVisible;
            divList.Visible = ListVisible;

            if (!Page.IsPostBack)
            {
                BusinessUnit bu = Context.Items["BusinessUnit"] as BusinessUnit;
                if (bu != null)
                {
                    lnkDealer.Text = bu.Name;
                    BusinessUnitID = bu.Id;
                }

                if (Request.QueryString["list"] != null)
                {
                    ddlList.SelectedValue = Request.QueryString["list"].ToString();
                }
                if (Request.QueryString["search"] != null)
                {
                    txtSearch.Text = Request.QueryString["search"].ToString();
                }
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            var allInventory = InventoryDataFilter.FetchListFromSession(BusinessUnitID);

            Bin bin = Bin.Create(SearchText);
            List<InventoryData> searchResults = InventoryDataFilter.WorkflowSearch(BusinessUnitID, allInventory, 0, 9999, String.Empty, bin);
            
            if (searchResults.Count > 0)
                Response.Redirect("~/Mobile/Basics.aspx?id=" + searchResults[0].InventoryID + "&search=" + SearchText);
            else
                txtSearch.Text = "";
        }
        protected void ddlList_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnActiveListChanged(EventArgs.Empty);
        }

        //Custom Events
        public event EventHandler ActiveListChanged;
        protected virtual void OnActiveListChanged(EventArgs e)
        {
            if (ActiveListChanged != null)
                ActiveListChanged(this, e);
        }
    }
}