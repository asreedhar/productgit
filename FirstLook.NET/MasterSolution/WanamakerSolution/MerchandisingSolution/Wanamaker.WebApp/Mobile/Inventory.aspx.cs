using System;
using System.Collections;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using FirstLook.Merchandising.DomainModel.Workflow;
using MAX.Entities;
using MerchandisingLibrary;
using Image = System.Web.UI.WebControls.Image;
using Label = System.Web.UI.WebControls.Label;

namespace Wanamaker.WebApp.Mobile
{
    public partial class Inventory : BasePage
    {
        ProfileCommon Profile = new ProfileCommon();

        public int usedOrNewFilter = 2;
        private const int pageSize = 20;
        private int currCount
        {
            get
            {
                object o = ViewState["cc"];
                return (o == null) ? 0 : (int)o;
            }
            set { ViewState["cc"] = value; } 
        }
        private Hashtable lowActivityMessages;
        public Hashtable LowActivityMessages
        {
            get
            {
                if (lowActivityMessages == null)
                {
                    lowActivityMessages = new Hashtable();
                    lowActivityMessages.Add(LowActivityAlerts.LowSearchAlert, "- Low Searches -");
                    lowActivityMessages.Add(LowActivityAlerts.LowClickthruAlert, "- Low Clickthru -");
                    lowActivityMessages.Add(LowActivityAlerts.UnderpricingAlert, "- Potentially Underpriced -");
                    lowActivityMessages.Add(LowActivityAlerts.OverpricingAlert, "- Potentially Overpriced -");

                }
                return lowActivityMessages;
            }
        }
        public bool OverdueFlag
        {
            get
            {
                int flagVal;
                if (string.IsNullOrEmpty(Request["overdue"]) || !int.TryParse(Request["overdue"], out flagVal))
                {
                    return Profile.Overdue;
                }
                bool flag = Convert.ToBoolean(flagVal);
                if (Profile.Overdue != flag)
                {
                    Profile.Overdue = flag;
                }
                return flag;
            }
        }

        public InventoryFilterMode filterValue
        {
            get
            {
                object o = ViewState["filter"];
                return (o == null) ? InventoryFilterMode.ShowAllActivityFilters : (InventoryFilterMode)o;
            }
            set { ViewState["filter"] = value; }
        }

        public string SortExpression
        {
            get
            {
                object o = ViewState["sort"];
                return (o == null) ? string.Empty : (string)o;
            }
            set
            {
                ViewState["sort"] = value;
                if (Profile.Sort != value)
                {
                    Profile.Sort = value;
                }
            }

        }

        public int Layout
        {
            get
            {
                object o = ViewState["layout"];
                return (o == null) ? 0 : (int)o;

            }
            set { ViewState["layout"] = value; }
        }

        public int BusinessUnitID
        {
            get
            {
                object o = ViewState["BusinessUnitID"];
                return (o == null) ? -1 : (int)o;
            }
            set
            {
                ViewState["BusinessUnitID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //SoftwareSystemComponentState s = SoftwareSystemComponentStateFacade.FindOrCreate()

            if (!IsPostBack)
            {
                //Expire the dealer's inventory cache so it will be grabbed fresh
                InventoryDataFilter.ClearListFromSession();

                BusinessUnit bu = Context.Items["BusinessUnit"] as BusinessUnit;
                if (bu == null)
                {
                    Response.Redirect("~/Default.aspx");
                }

                BusinessUnitID = bu.Id;
            }

            usedOrNewFilter = 2; //set to used

            if (CustomPager.Visible)
            {
                SetPager(pageSize);
            }

        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            //if (CustomPager.Visible)
            //{
            //    SetPager(pageSize);
            //}
        }
        protected void GridShowAll_Click(object sender, EventArgs e)
        {
            PageNumber = -1;
            SetPager(-1);

            InventoryTable.DataBind();

        }
        private void SetPager(int pageSize)
        {

            CustomPager.Visible = true;

            //int count = InventoryData.InventoryCount(BusinessUnitID, FilterValue, bucket, usedOrNewFilter);
            int count = 0;
            WorkflowType? workflow;
            switch (mhHeader.ActiveList)
            {
                case 0:
                    workflow = WorkflowType.EquipmentNeeded;
                    break;
                case 1:
                    workflow = WorkflowType.CreateInitialAd;
                    break;
                case 2:
                    workflow = WorkflowType.NewInventory;
                    break;
                default:
                    workflow = null;
                    break;
            }
            if (workflow.HasValue)
                count = InventoryDataFilter
                    .FetchListFromSession(BusinessUnitID)
                    .WorkflowFilter(workflow.Value, ConditionFilters.CreateFromInt(usedOrNewFilter),
                                    null, mhHeader.SearchText)
                    .Count();

            PagesHolder.Controls.Clear();
            if (PageNumber >= 0)
            {
                for (int i = 0; i < Math.Ceiling(Convert.ToDouble(count) / pageSize); i++)
                {
                    if (i == PageNumber)
                    {
                        HtmlGenericControl pageLb = new HtmlGenericControl("span");
                        pageLb.InnerHtml = (i + 1).ToString();
                        PagesHolder.Controls.Add(pageLb);
                    }
                    else
                    {
                        LinkButton pageButton = new LinkButton();
                        pageButton.ID = "Page_" + i.ToString();
                        pageButton.Text = (i + 1).ToString();
                        pageButton.CommandArgument = i.ToString();
                        pageButton.Click += Pager_Click;

                        PagesHolder.Controls.Add(pageButton);
                    }
                }
                startVehNdx.Text = (PageNumber * pageSize + 1).ToString();
                endVehNdx.Text = Math.Min((PageNumber * pageSize + pageSize), count).ToString();
                if (endVehNdx.Text == "0")
                    startVehNdx.Text = "0";

            }
            else
            {
                startVehNdx.Text = "1";
                endVehNdx.Text = count.ToString();
                if (endVehNdx.Text == "0")
                    startVehNdx.Text = "0";
            }

            totalVeh.Text = count.ToString();

        }
        //int businessUnitID, int startIndex, int pageSize, string sortExpression, Bin bin

        protected void InventoryDSList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            WorkflowType ActiveListType = WorkflowType.NewInventory;
            switch(mhHeader.ActiveList)
            {
                case 0:
                    ActiveListType = WorkflowType.EquipmentNeeded;
                    break;
                case 1:
                    ActiveListType = WorkflowType.CreateInitialAd;
                    break;
                case 2:
                    ActiveListType = WorkflowType.NewInventory;
                    break;   
            }

            e.InputParameters["businessUnitID"] = BusinessUnitID;
            int newUsedFilter = (int)e.InputParameters["usedOrNewFilter"];
            
            e.InputParameters.Remove("usedOrNewFilter");
            e.InputParameters.Remove("activityStatus");

            e.InputParameters["bin"] = Bin.Create(ActiveListType, newUsedFilter, null, mhHeader.SearchText);

            if (string.IsNullOrEmpty(e.Arguments.SortExpression))
            {
                e.Arguments.SortExpression = SortExpression;
            }
            else
            {
                SortExpression = e.Arguments.SortExpression;
            }

            if (PageNumber >= 0)
            {
                e.Arguments.StartRowIndex = PageNumber * pageSize;
                e.Arguments.MaximumRows = pageSize;

                SetPager(pageSize);
            }
            else  //we are in show all mode
            {
                e.Arguments.StartRowIndex = 0;
                e.Arguments.MaximumRows = 1000;
            }

        }

        public int PageNumber
        {
            get
            {
                object o = ViewState["pageNum"];
                return (o == null) ? 0 : (int)o;
            }
            set { ViewState["pageNum"] = value; }
        }
        protected void Pager_Click(object sender, EventArgs e)
        {

            LinkButton lb = sender as LinkButton;
            if (lb != null)
            {


                PageNumber = Int32.Parse(lb.CommandArgument);
                startVehNdx.Text = (PageNumber * pageSize + 1).ToString();
                endVehNdx.Text = Math.Min(((PageNumber + 1) * pageSize), Int32.Parse(totalVeh.Text)).ToString();


                InventoryTable.DataBind();

            }

        }

        protected void StockItem_Selected(object sender, EventArgs e)
        {
            GridView sendingGV = sender as GridView;
            if (sendingGV != null)
            {
                if (sendingGV.SelectedDataKey != null)
                {
                    openNewVehicle((int)sendingGV.SelectedDataKey.Value);
                }
            }
        }

        protected void StockItem_Click(object sender, EventArgs e)
        {
            LinkButton lb = sender as LinkButton;
            if (lb != null)
            {
                int invId = Int32.Parse(lb.CommandArgument);
                openNewVehicle(invId);
            }
        }

        private void openNewVehicle(int inventoryId)
        {
            try
            {
                //InventoryData.Fetch(BusinessUnitID, stockKey);
                Response.Redirect("Loader.aspx?id=" + inventoryId);
            }
            catch
            {
                this.Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "alertMe", "alert('Vehicle Not Found')", true);
            }
        }


        protected void InventoryRow_DataBound(object sender, GridViewRowEventArgs e)
        {
            InventoryData currItem = e.Row.DataItem as InventoryData;
            if (currItem != null)
            {
                SetIconVisibility(currItem, e.Row);
            }

            Label certLb = e.Row.FindControl("certifiedLbl") as Label;
            if (certLb != null)
            {
                bool cert = ((InventoryData)e.Row.DataItem).Certified;
                if (cert)
                {
                    certLb.Text = "C";
                    certLb.ToolTip = ((InventoryData)e.Row.DataItem).CertificationTitle;
                }
            }
        }

        private void SetIconVisibility(InventoryData currItem, Control iconRowHolder)
        {
            MerchandisingAlert lowActivities = MerchandisingAlert.GetLowActivityAlerts(BusinessUnitID, currItem.InventoryID);

            Image priceImage = iconRowHolder.FindControl("PricingIcon") as Image;
            Image photoImage = iconRowHolder.FindControl("PhotosIcon") as Image;
            Image merchImage = iconRowHolder.FindControl("MerchandisingIcon") as Image;
            Image pendingImage = iconRowHolder.FindControl("PendingApprovalIcon") as Image;

            Image lowActivityFlag = iconRowHolder.FindControl("LowActivityIcon") as Image;


            if (priceImage != null) priceImage.Visible = currItem.NeedsPricing;
            if (photoImage != null) photoImage.Visible = currItem.NeedsPhotos;
            if (merchImage != null) merchImage.Visible = currItem.NeedsExterior;
            if (pendingImage != null) pendingImage.Visible = currItem.PendingApproval;

            if (lowActivityFlag != null)
            {
                if (lowActivities.HasAlerts())
                {
                    lowActivityFlag.ToolTip = lowActivities.GetAlertText();
                    lowActivityFlag.Visible = true;
                }
                else
                {
                    lowActivityFlag.Visible = false;
                }
            }
        }

        protected static string TradeOrPurchaseText(object value)
        {
            if (value == null)
            {
                return "?";
            }
            string text = value as string;
            if (string.IsNullOrEmpty(text))
            {
                return "?";
            }
            else
            {
                return new string(text.ToCharArray(), 0, 1);
            }
        }
        protected void Search(object sender, EventArgs e)
        {
            //RunSearch(SearchParam.Value);
        }



        protected void CreateStaging_Click(object sender, EventArgs e)
        {
            string vinTxt = vinStaging.Text;
            string stockTxt = stockNumberStaging.Text;

            int createdId = InventoryDataSource.CreateStaging(BusinessUnitID, vinTxt, stockTxt);
            openNewVehicle(createdId);
        }
        protected void AddInventory_Click(object sender, EventArgs e)
        {
            StagingVehicle.Visible = true;

            InventoryTable.Visible = false;
            CustomPager.Visible = false;
        }

        protected void LogOff_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Mobile/Logoff.aspx");
        }

        protected void mhHeader_ActiveListChanged(object sender, EventArgs e)
        {
            InventoryTable.DataBind();
        }
        protected void btnVehicle_Click(object sender, EventArgs e)
        {
            LinkButton btnSender = (LinkButton)sender;
            Response.Redirect("~/Mobile/Basics.aspx?id=" + btnSender.CommandArgument + "&list=" + mhHeader.ActiveList);
        }
        protected void InventoryTable_Sorting(object sender, GridViewSortEventArgs e)
        {
        }
    }
}