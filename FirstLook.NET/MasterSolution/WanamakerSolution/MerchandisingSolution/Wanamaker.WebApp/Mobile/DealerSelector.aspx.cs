using System;
using System.Web.Security;
using System.Web.UI.WebControls;

using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Oltp;
using BusinessUnit=FirstLook.DomainModel.Oltp.BusinessUnit;
using BusinessUnitFinder=FirstLook.DomainModel.Oltp.BusinessUnitFinder;
using SoftwareSystemComponentStateFacade=FirstLook.DomainModel.Oltp.SoftwareSystemComponentStateFacade;
using Wanamaker.WebApp.AppCode.AccessControl;

namespace Wanamaker.WebApp.Mobile
{
    public partial class DealerSelector : BasePage
    {
        //
        // Support Methods
        //

        private static readonly string DealerGroupToken = SoftwareSystemComponentStateFacade.DealerGroupComponentToken;

        private static readonly string DealerToken = SoftwareSystemComponentStateFacade.DealerComponentToken;

        protected SoftwareSystemComponentState SoftwareSystemComponentState
        {
            get { return (SoftwareSystemComponentState)Context.Items[SoftwareSystemComponentStateFacade.HttpContextKey]; }
            set { Context.Items[SoftwareSystemComponentStateFacade.HttpContextKey] = value; }
        }

        //
        // Page Initialize
        //

        protected void Page_Init(object sender, EventArgs e)
        {
            if( !User.Identity.IsAuthenticated )
            {
                // User should be authenticated before reaching this screen, if not treat as unknown error.
                Response.Redirect( "~/ErrorPage.aspx?Error=410", true );
            }
            MembershipUser user = IdentityHelper.GetUserByMappingLogin( User.Identity.Name );
            if( user == null || !Roles.IsUserInRole( user.UserName, "Administrator" ) )
            {
                Response.Redirect( "~/ErrorPage.aspx?Error=403", true );
            }

            SoftwareSystemComponentState = LoadState(DealerGroupToken);
        }

        protected void Page_Load(object sender, EventArgs e) { }

        protected void DealerGroupFilter_TextChanged(object sender, EventArgs e)
        {
            DealerGroupGridView.SelectedIndex = -1;
            DealerFilter.Text = "";
            DealerFilter_TextChanged(sender, e);
        }

        protected void DealerGroupGridView_PageIndexChanged(object sender, EventArgs e)
        {
            DealerGroupFilter_TextChanged(sender, e);
        }

        protected void DealerFilter_TextChanged(object sender, EventArgs e)
        {
            DealerGridView.SelectedIndex = -1;
        }

        protected void DealerGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("EnterDealer"))
            {
                int selectedIndex = Int32Helper.ToInt32(e.CommandArgument);

                GridView grid = sender as GridView;

                if (grid != null)
                {
                    int dealerId = Int32Helper.ToInt32(grid.DataKeys[selectedIndex]["Id"]);

                    BusinessUnit dealer = BusinessUnitFinder.Instance().Find(dealerId);

                    if (dealer != null)
                    {
                        SetupSoftwareSystemComponentState(dealer);

                        Response.Redirect( "~/Mobile/home.aspx", true );
                    }
                }
            }
        }

        protected void DealerGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GridView view = sender as GridView;

                if (view != null)
                {
                    BusinessUnit dealer = e.Row.DataItem as BusinessUnit;

                    if (dealer != null)
                    {
                        //bool upgraded = dealer.HasDealerUpgrade(Upgrade.PingTwo);
                        e.Row.Cells[1].Enabled = true;

                    }
                }
            }
        }

        protected void DealerGridView_PageIndexChanged(object sender, EventArgs e)
        {
            DealerFilter_TextChanged(sender, e);
        }



        private void SetupSoftwareSystemComponentState(BusinessUnit dealer)
        {
            SoftwareSystemComponentState state = LoadState(DealerToken);
            state.DealerGroup.SetValue(dealer.DealerGroup());
            state.Dealer.SetValue(dealer);
            state.Member.SetValue(null);
            state.Save();
        }

        private SoftwareSystemComponentState LoadState(string componentToken)
        {
            
            SoftwareSystemComponentState state = SoftwareSystemComponentStateFacade.FindOrCreate(User.Identity.Name, componentToken);

            if (state == null)
            {
                Member member = MemberFinder.Instance().FindByUserName(User.Identity.Name);

                SoftwareSystemComponent component = SoftwareSystemComponentFinder.Instance().FindByToken(componentToken);

                state = new SoftwareSystemComponentState();
                state.AuthorizedMember.SetValue(member);
                state.SoftwareSystemComponent.SetValue(component);
            }

            return state;
        }
    }
}