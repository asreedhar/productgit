using System;
using System.Configuration;
using System.Web.Security;

namespace Wanamaker.WebApp.Mobile
{
    public partial class LogOff : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Delete the ASP.NET Authentication Token

            Response.Cookies[FormsAuthentication.FormsCookieName].Expires = DateTime.Now.AddYears(-30);

            // Redirect to IMT Logout (which will send us onto CAS)

            //Response.Cookies["dealerId"].Expires = DateTime.Now.AddYears(-30);
            //Response.Cookies["fltracker"].Expires = DateTime.Now.AddYears(-30);
            //Response.Cookies["cas_filter_gateway"].Expires = DateTime.Now.AddYears(-30);



            string url = string.Format("{0}/NextGen/CloseSessionAction.go?forwardTo=/LogoutAction.go", ConfigurationManager.AppSettings["edge_host_name"], ConfigurationManager.AppSettings["merchandising_host_path"]);
            //string url = ConfigurationManager.AppSettings["merchandising_host_path"];
            Response.Redirect(url, true);
        }
    }
}