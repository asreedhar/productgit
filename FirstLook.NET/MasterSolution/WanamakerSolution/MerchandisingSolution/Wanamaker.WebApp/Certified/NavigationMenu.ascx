<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="NavigationMenu.ascx.cs" Inherits="Wanamaker.WebApp.Certified.NavigationMenu" %>

    <div class="logoBanner">
        <div class="minorLinks">
            <a class="logoff" href="/LogOff.aspx">Log out</a>
            <asp:Image ID="CpoImage" CssClass="SubLogo" runat="server" />
        </div>
        <asp:Image ID="logo" runat="server" SkinID="MaxCpoLogo" />
        <asp:Label CssClass="CertifiedName" ID="CertName" runat="server" ></asp:Label>        
    </div>
  <div id="nav">
  
    <ul>
        <li id="features" runat="server"><asp:HyperLink CssClass="first" ID="mLink" runat="server" Text="Marketing Features" /></li>
        <li id="specials" runat="server"><asp:HyperLink CssClass="chevron" ID="sLink" runat="server" Text="Specials" /></li>
        <li id="profiles" runat="server">
            <asp:HyperLink CssClass="chevron" ID="pLink" runat="server" Text="Vehicle Profiles" />
        </li>
        
    </ul>
    <!-- TODO: Refactor this -->
    <style type="text/css">
        #nav li { float:left; }
    </style>
    
    </div>   
    
