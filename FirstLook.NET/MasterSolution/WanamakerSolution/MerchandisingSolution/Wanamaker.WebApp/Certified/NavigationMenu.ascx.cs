using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Vehicles;
using VehicleDataAccess;

namespace Wanamaker.WebApp.Certified
{
    public partial class NavigationMenu : System.Web.UI.UserControl
    {
        public int CpoGroupId = 1;
        protected int GetCpoGroupId(string cpoGroup)
        {
            try
            {
                int id = (int)(CpoGroup)Enum.Parse(typeof(CpoGroup), cpoGroup);
                return id;
            }
            catch
            {
                return 1;
            }
        }

        public string GetCpoLogoUrl(string cpoGroup)
        {
            CpoGroup grp = (CpoGroup)Enum.Parse(typeof(CpoGroup), cpoGroup);
            switch (grp)
            {
                case CpoGroup.GM:
                    return "~/App_Themes/LazyBoy/images/logo_GM.jpg";
                case CpoGroup.Toyota:
                    return "~/App_Themes/LazyBoy/images/logo_Toyota.gif";
                case CpoGroup.Honda:
                    return "~/App_Themes/LazyBoy/images/logo_Honda.png";
                case CpoGroup.Chrysler:
                    return "~/App_Themes/LazyBoy/images/logo_chrysler.png";
                default:
                    return "";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BusinessUnit bu = Context.Items["BusinessUnit"] as BusinessUnit;
                if (!String.IsNullOrEmpty(Request["cpoGroup"]))
                {
                    CpoGroupId = GetCpoGroupId(Request["cpoGroup"]);
                }
                if (bu != null)
                {
                    CpoImage.ImageUrl = GetCpoLogoUrl(Request["cpoGroup"]);
                    CertName.Text = Request["cpoGroup"] + " Certified";
                }

                string nameLow = this.Page.GetType().Name.ToLower();
                if (nameLow.Contains("profiles"))
                {
                    profiles.Attributes.Add("class", "current");
                }
                else if (nameLow.Contains("specials"))
                {
                    specials.Attributes.Add("class", "current");
                }
                else if (nameLow.Contains("marketing"))
                {
                    //quickApprove.Visible = true; TURNED OFF FOR DEMO
                    features.Attributes.Add("class", "current");
                }

            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            string qStr = "?cpoGroup=" + Request["cpoGroup"];
            mLink.NavigateUrl = "Marketing.aspx" + qStr;
            sLink.NavigateUrl = "Specials.aspx" + qStr;
            pLink.NavigateUrl = "Profiles.aspx" + qStr;
                    


        }
    }

}