<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/Workflow/Workflow.Master" Theme="None" Title="Choose Dealer | MAX 3.0" CodeBehind="DealerChooser.aspx.cs" Inherits="Wanamaker.WebApp.DealerChooser" %>
<%@ Import Namespace="Wanamaker.WebApp.Helpers" %>

<%@ Register Src="~/Workflow/Controls/WorkflowHeader.ascx" TagName="WorkflowHeader" TagPrefix="wanaworkflow" %>
<%@ Register TagPrefix="workflow" TagName="InventorySearch" Src="~/Workflow/Controls/SearchInventory.ascx"  %>

<asp:Content ID="CssContent" ContentPlaceHolderID="CssPlaceHolder" runat="server">
    <link rel="stylesheet" type="text/css" href="<%= HtmlHelpers.VersionedUrl("~/Themes/MAX3.0/Main.debug.css") %>" />
    <style type="text/css">
        
        #Content { margin-top:10px; }
        
        fieldset { background:#fff; border:solid 1px #ccc; float:left; margin:5px; padding: 1em !important; width:46%; }
        legend { color:#666; padding:2px 6px; }
        
        table { border-collapse:collapse; margin:0.5em auto; width:100% !important; }
        table table { width:auto !important; }
        td, th { padding:0.25em 0.5em; }
        th { text-align:left; }
        table, td { border:solid 1px #999; }
        
        .filterWidget { margin:0 0 0.5em 0.5em; }
        .table-row-pagination { text-align:center; }
    </style>
</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="BodyPlaceHolder" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <div id="Content" class="clearfix">
        <fieldset>
            <legend>Dealer Groups</legend>
            <div id="GroupNameFilter" class="filterWidget">
                <asp:Label ID="DealerGroupFilterLabel" AssociatedControlID="DealerGroupFilter" runat="server">Filter Name:</asp:Label>
                <asp:TextBox ID="DealerGroupFilter" runat="server" AutoPostBack="true" ToolTip="Dealer Group Filter" OnTextChanged="DealerGroupFilter_TextChanged" />
                <asp:Button ID="DealerGroupFilterButton" runat="server" Text="Filter" />
            </div>
            <div>
                <asp:ObjectDataSource ID="DealerGroupDataSource" TypeName="FirstLook.DomainModel.Oltp.BusinessUnitFinder" SelectMethod="FindAllDealerGroupsForMember"
                    runat="server">
                    <SelectParameters>
                        <owc:MemberParameter Name="member" Type="Object" AllowImpersonation="false" />
                        <asp:ControlParameter Name="filter" Type="string" ControlID="DealerGroupFilter" DefaultValue="" ConvertEmptyStringToNull="true" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:GridView ID="DealerGroupGridView" runat="server" DataKeyNames="Id" DataSourceID="DealerGroupDataSource" AutoGenerateColumns="false"
                    AllowPaging="true" PageSize="20" OnPageIndexChanged="DealerGroupGridView_PageIndexChanged">
                    <Columns>
                        <asp:BoundField DataField="Name" HeaderText="Name" />
                        <asp:CommandField ButtonType="Button" SelectText="Show Dealers" ShowSelectButton="true" ItemStyle-HorizontalAlign="Center" />
                    </Columns>
                </asp:GridView>
            </div>
        </fieldset>
        <fieldset>
            <legend>Dealers</legend>
            <div id="DealerNameFilter" class="filterWidget">
                <asp:Label ID="DealerFilterLabel" AssociatedControlID="DealerFilter" runat="server">Filter Name:</asp:Label>
                <asp:TextBox ID="DealerFilter" runat="server" AutoPostBack="true" ToolTip="Dealer Filter" OnTextChanged="DealerFilter_TextChanged"></asp:TextBox>
                <asp:Button ID="DealerFilterButton" runat="server" Text="Filter" />
            </div>
            <div>
                <asp:ObjectDataSource ID="DealerDataSource" TypeName="FirstLook.DomainModel.Oltp.BusinessUnitFinder" SelectMethod="FindAllDealersForMemberAndDealerGroup"
                    runat="server">
                    <SelectParameters>
                        <owc:MemberParameter Name="member" Type="Object" AllowImpersonation="false" />
                        <asp:ControlParameter Name="dealerGroupId" Type="Int32" ControlID="DealerGroupGridView" DefaultValue="" ConvertEmptyStringToNull="true" />
                        <asp:ControlParameter Name="filter" Type="string" ControlID="DealerFilter" DefaultValue="" ConvertEmptyStringToNull="true" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:GridView ID="DealerGridView" runat="server" DataKeyNames="Id" DataSourceID="DealerDataSource" AutoGenerateColumns="false"
                    AllowPaging="true" PageSize="20" UseAccessibleHeader="true" OnRowCommand="DealerGridView_RowCommand" OnRowDataBound="DealerGridView_RowDataBound"
                    OnPageIndexChanged="DealerGridView_PageIndexChanged">
                    <Columns>
                        <asp:BoundField DataField="Name" HeaderText="Name" />
                        <asp:ButtonField ButtonType="Button" Text="Enter" CommandName="EnterDealer" />
                    </Columns>
                </asp:GridView>
            </div>
        </fieldset>
    </div>

</asp:Content>

<asp:Content ID="ScriptContent" ContentPlaceHolderID="ScriptsPlaceHolder" runat="server">
    
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push( ['_setAccount', 'UA-1230037-1'] );
        _gaq.push( ['_trackPageview'] );

        (function () {
            var ga = document.createElement( 'script' ); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ( 'https:' == document.location.protocol ? 'https://ssl' : 'http://www' ) + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName( 'script' )[0]; s.parentNode.insertBefore( ga, s );
        } )();
    </script>

</asp:Content>