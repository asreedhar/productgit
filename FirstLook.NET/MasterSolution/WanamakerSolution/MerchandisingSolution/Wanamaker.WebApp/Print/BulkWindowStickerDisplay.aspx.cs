﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BulkWindowSticker;

namespace Wanamaker.WebApp.Print
{
    public partial class BulkWindowStickerDisplay : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if(!IsPostBack)
            {

                try
                {
                    
                    int printBatchId;
                    var windowStickerRepository = new WindowStickerRepository();

                    if (!Int32.TryParse(Request.QueryString["PrintBatchId"], out printBatchId ))
                        throw new ApplicationException("Invalid Print Batch ID");

                    // mark the print batch as accessed
                    windowStickerRepository.RecordPrintBatchAccess(printBatchId, HttpContext.Current.User.Identity.Name, null);
                    
                    // redirect to the pdf
                    Response.Redirect(windowStickerRepository.GetBulkWindowStickerUrl(printBatchId));


                }
                catch (ApplicationException ex)
                {
                    lblMessage.Text = ex.Message;
                    return;
                }
                catch (Exception ex)
                {
                    lblMessage.Text = ex.ToString();
                    return;
                }
                    

            }

        }
    }
}