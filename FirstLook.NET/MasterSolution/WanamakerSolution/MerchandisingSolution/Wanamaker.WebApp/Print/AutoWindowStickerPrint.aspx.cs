﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BulkWindowSticker;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Commands;

namespace Wanamaker.WebApp.Print
{
    public partial class AutoWindowStickerPrint : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                int inventoryId;
                int businessUnitId;

                try
                {

                    // check the inventory id
                    if (!Int32.TryParse(Request.QueryString["InventoryId"], out inventoryId))
                        throw new ApplicationException("The inventory id is not valid");

                    // check the business unit
                    BusinessUnit bu = Context.Items["BusinessUnit"] as BusinessUnit;
                    if (bu == null)
                        throw new ApplicationException("Invalid business unit");
                    else
                        businessUnitId = bu.Id;

                    // check is autoWindowSticker is active
                    WindowStickerRepository windowStickerRepository = new WindowStickerRepository();
                    if (!windowStickerRepository.HasAutoWindowSticker(businessUnitId))
                        throw new ApplicationException(string.Format("Business unit {0} is not configured for Auto Window Stickers", businessUnitId));
                    
                    // create the generator
                    var optionsCommand = GetUpgradeSettings.GetSettings(businessUnitId);

                    if (optionsCommand.AutoWindowStickerTemplateId == null)
                        throw new ApplicationException(string.Format("Business unit {0} is not configured with an Auto Window Sticker template", businessUnitId));

                    int templateId = optionsCommand.AutoWindowStickerTemplateId.Value;

                    // create the generator
                    WindowStickerGenerator windowStickerGenerator = new WindowStickerGenerator(businessUnitId, templateId);
                    
                    // get pdf
                    List<int> inventoryList = new List<int>();
                    inventoryList.Add(inventoryId);
                    var windowStickerPdf = windowStickerGenerator.GeneratePDF(inventoryList);

                    // display
                    string printScript = "this.print(true);";
                    Collection<byte[]> pdfCollection = new Collection<byte[]>();
                    pdfCollection.Add(windowStickerPdf);


                    var pdf = PdfDocumentWrapper.Merge(pdfCollection , 0, 0, 0, 0, printScript);

                    // write the pdf to the response
                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AppendHeader("Content-Length", pdf.Length.ToString());
                    Response.BinaryWrite(pdf);
                    Response.End();

                    // log in print log
                    windowStickerRepository.SavePrintLog(inventoryId, 1, templateId, true, null);

                    //throw new ApplicationException(string.Format("made it with bu: {0} and inventory {1}", businessUnitId, inventoryId));
                }
                catch (ApplicationException ex)
                {
                    lblMessage.Text = ex.Message;
                    return;
                }
                catch (Exception ex)
                {
                    lblMessage.Text = ex.ToString();
                    return;
                }

            }

        }
    }
}