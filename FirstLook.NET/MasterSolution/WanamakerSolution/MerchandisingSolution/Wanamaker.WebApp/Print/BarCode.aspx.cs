using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FirstLook.Merchandising.DomainModel.Barcodes;

namespace Wanamaker.WebApp.Print
{

    public partial class BarCode : System.Web.UI.Page
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            int size = 40;
            int.TryParse(Request["size"], out size);

            byte[] myBytes2 = BarCodeHelper.CreateCode39(Request["vin"], size, true, "",
                                      Server.MapPath(ConfigurationManager.AppSettings["barcode_3_of_9_font_file"]));

            Response.ContentType = "image/png";
            Response.OutputStream.Write(myBytes2, 0, myBytes2.Length);
        }


        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}