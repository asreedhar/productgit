using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using VehicleDataAccess;
using Wanamaker.WebApp.AppCode;

namespace Wanamaker.WebApp.Print
{

    public partial class PrintBuildSheetBatch : System.Web.UI.Page
    {
        public int BusinessUnitID
        {
            get
            {

                object o = ViewState["BusinessUnitID"];
                return (o == null) ? -1 : (int)o;

            }
            set
            {
                ViewState["BusinessUnitID"] = value;

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            BusinessUnit bu = Context.Items["BusinessUnit"] as BusinessUnit;
            if (bu == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            BusinessUnitID = bu.Id;

            string ids = Request["ids"];
            if (string.IsNullOrEmpty(ids))
            {
                //No need to blow up, just give them a blank workbook if they didn't select any cars (probably a better solution but this was quickest)
                return;
                //throw new ApplicationException("No stock numbers supplied to build sheet batch printer");
            }


            //TODO: photos - Commented out Photo stuff directly below. What does this page do? Maybe delete it altogether
            //PhotoItemCollection pics = new PhotoItemDataSource().StoryBoardSelect(BusinessUnitID, 0);
            //StoryBoard.DataSource = pics;
            //StoryBoard.DataBind();

            List<InventoryData> buildSheetData = new List<InventoryData>();

            string[] inventoryIDs = ids.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            foreach (string invId in inventoryIDs)
            {
                int id = -1;
                if (Int32.TryParse(invId, out id))
                {
                    InventoryData inventory = InventoryData.Fetch(BusinessUnitID, id);
                    buildSheetData.Add(inventory);
                }

            }
            BuildSheetRepeater.DataSource = buildSheetData;
            TableOfContents.DataSource = buildSheetData;
            if (buildSheetData.Count <= 1)
            {
                TableOfContents.Visible = false;
            }
            this.Page.DataBind();

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {

            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter writer = new HtmlTextWriter(sw);

            this.Page.RenderControl(writer);

            string html = sb.ToString();

            PdfService converter = new PdfService();

            byte[] stickerPdfBytes = converter.GeneratePdf(html);

            Response.Clear();
            Response.ClearHeaders();
            Response.AddHeader("Content-Disposition", "attachment; filename=buildWorkbook.pdf");
            Response.AddHeader("Content-Length", stickerPdfBytes.Length.ToString());
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(stickerPdfBytes);
            Response.End();

        }


        protected void AdditionalInfo_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["BusinessUnitID"] = BusinessUnitID;
        }
        protected static List<CategoryCollection> GetEquipment(object evalResp, int interiorOrExterior)
        {

            string vin = evalResp as string;
            if (vin == null || vin.Length != 17) return new List<CategoryCollection>();

            List<CategoryCollection> vals = new VINStyleDataSource().AllFeaturesCategoriesSelect(vin, interiorOrExterior);
            vals.Sort(new CategoryCollectionListLengthOrderer());
            return vals;

        }
        protected static Dictionary<int, string> GetStyles(object evalResp)
        {
            string vin = evalResp as string;
            if (vin == null || vin.Length != 17) return new Dictionary<int, string>();

            Dictionary<int, string> styles = ChromeStyle.ChromeTrimStyleNamesSelect(vin);
            return styles;

        }

        protected static string GetMileageDisplay(object evalObj)
        {
            int miles = (int)evalObj;
            if (miles < 0)
            {
                return "--";
            }
            return String.Format("{0:n0}", miles);
        }
    }
}