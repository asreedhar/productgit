﻿using System;
using System.Collections.ObjectModel;
using FirstLook.Common.Core.Utilities;
using Wanamaker.WebApp.Workflow.Controls;


namespace Wanamaker.WebApp.Print
{
    public partial class PrintPdf : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            // get the byte array
            var pdfs = (Collection<byte[]>) Session[PrintWindowSticker.PrintPdfByteArrayCollection];
            Session.Remove(PrintWindowSticker.PrintPdfByteArrayCollection);
            if (pdfs == null) 
                throw new ApplicationException("No Pdf was found to process.");

            // add the print script to the pdf
            string printScript = "this.print(true);";
            var pdf = PdfDocumentWrapper.Merge(pdfs, 0, 0, 0, 0, printScript);
            
            // write the pdf to the response
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AppendHeader("Content-Length", pdf.Length.ToString());
            Response.BinaryWrite(pdf);
            Response.End();


        }

    }
}
