<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="PrintBuildSheetBatch.aspx.cs" Inherits="Wanamaker.WebApp.Print.PrintBuildSheetBatch" 
    EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Build Sheet Booklet</title>
    <style type="text/css">
    @page {
        margin: 0.5in 0.7in;
    }
    div.BulkBuildSheet {
        page: main-pages;
    }
    div.BuildSheetsSection {
        counter-reset: page 1;
        
    }
    body {
        page: toc-pages;
        counter-reset: page 1;
    }
    @page toc-pages {
        @bottom {
            content: counter(page, lower-roman);
        }
    }
    @page main-pages {
        @bottom {
            content: counter(page)
        }
    }
    ul {
        list-style: none;
        padding: 0px;
        margin: 0px;
    }
    h3 {
        font-size: 12px;
        margin-bottom: 0.05in;
        padding: 0.0;
    }
    h1 {
        font-size: 16px;
        margin-bottom: 0.075in;
        text-transform: uppercase;
        color: #555;
    }
    .toc {
        list-style: none;
        page-break-after: always;
        width: 7.0in;
        display: block;
    }
    .toc li {
        width: 98%;
        clear:left;
        border-bottom:dotted 1px #aaa;
        height:1.05em;
        margin-top:10px;
        position:relative;

    }
    .toc a, .toc span {
        background: #fff;
        padding:0 3px 0 0;
        float:left;
        position:absolute;
        text-decoration:none;
    }
    .toc a {
        padding:0 0 0 3px;
        right: 0;
    }
    .StoryBoardItem {
        width: 24%;
        margin: 1px;
        text-align: center;
        display: inline-block;
    }
    .StoryBoardItem p {
        margin-top: 0px;
        margin-bottom: 20px;
    }
    .additionalInfo li {
        font-size: 11px;
        
    }
    .additionalInfo input {
        margin-right: 5px;
    }
.ConditionLbl    {
    display: inline-block;
    width: 1.3in;
}
.buildSheetDetails 
{
	clear: left;
	float: left;
	width: 2.45in;
	margin: 0.1in 0.0in 0.0in;
	
	text-align: left;
	font-size: 10px;
	border-right: solid thin #888;
	
}
.featureGroupBox 
{
	 float: left;
	 position: relative;
	 width: 48%;
	 min-width: 160px;
	 margin: 3px;
}

.featureGroupBox label 
{
	padding-left: 5px;
}



.featureGroupHeader 
{
	font: 12px/14px Verdana, sans;
	text-decoration: underline;
	margin-bottom: 2px;
	color: #444;
	display: block;
}

.OptionsBox 
{
    width: 4.4in;
    text-align: left;
	float: left;
	margin-left: 0.1in;
}
.OptionsBox h2 {
    display: block;
    clear: left;
    font-size: 14px;
    font-family: Arial, Sans-serif;
    color: #555;
    margin-bottom: 4px;
    padding-bottom: 0px;
}

#notesPanel {
    clear: left;
}

.OptionsBox h1 
{
	text-align: left;
	margin-top: 0.1in;
	margin-bottom: 0.0in;
	border-bottom: solid 1px #888;
	font: bold 16px/18px Arial, Sans-serif;
}

.featureGroupBox 
{
	width: 48%;
	font-size: 11px;
}

.buildSheetHead
{
	text-align: left;
}
.buildSheetHead h1 
{
	font-size: 16px;
	margin-bottom: 5px;
	border-bottom: solid 1px #000;
}
.buildSheetHead span 
{
	font-size: 14px;
	text-transform: uppercase;
}
.buildSheetHead img 
{
	float: right;
	height: 40px;
	width: 53px;
}

.BulkBuildSheet 
{
    clear: both;
	
	margin-top: 20px;
	page-break-after: always;
}    
    </style>
</head>
<body>
    <form id="form1" runat="server">
    
    
        <div id="Div1" class="workingPane" runat="server">

<h1>Vehicle Build Workbook Table of Contents</h1>
<asp:Repeater ID="TableOfContents" runat="server">
<HeaderTemplate>
<ol class="toc">
</HeaderTemplate>
<ItemTemplate>
    <li><span><%# Eval("StockNumber") + ": " + Eval("Year") + " " + Eval("Make") + " " + Eval("Model")  %></span><a href="#"><%# Container.ItemIndex + 1 %></a></li>
</ItemTemplate>
<FooterTemplate>
</ol>
</FooterTemplate>
</asp:Repeater>
<asp:Repeater ID="StoryBoard" runat="server">
    <HeaderTemplate>
    <h1>Photograph Sequence Worksheet</h1>
    </HeaderTemplate>
    <ItemTemplate>
    <div class="StoryBoardItem">
        <asp:Image ID="Img" runat="server" ImageUrl='<%# "http://max.firstlook.biz/merchandising" +  ((string)Eval("ImageUrl")).Substring(1) %>' />
        <p><%# (Container.ItemIndex + 1).ToString() + ". " + Eval("PhotoTitle") %></p>
    </div>    
    </ItemTemplate>
</asp:Repeater>

<div class="BuildSheetsSection">


<asp:Repeater ID="BuildSheetRepeater" runat="server">
<ItemTemplate>
<div class="BulkBuildSheet">
        <h1><%# Eval("Year") + " " + Eval("Make") + " " + Eval("Model") + " "/* + Eval("Drivetrain") */%></h1>
        <div class="buildSheetDetails">
        <table>
        <tr><td>Stock #:</td><td><%# Eval("StockNumber") %></td></tr>
        <tr><td>VIN:</td><td><%# Eval("VIN") %></td></tr>
        <tr><td>Drivetrain:</td><td><%# ""/*Eval("Drivetrain")*/ %></td></tr>
        <tr><td>Engine:</td><td><%# ""/*Eval("Engine")*/ %></td></tr>
        <tr><td>Fuel Type:</td><td><%# ""/*Eval("FuelType")*/ %></td></tr>
        
        <tr><td></td><td></td></tr>
        <tr><td>Mileage:</td><td><%# GetMileageDisplay(Eval("MileageReceived")) %></td></tr>
        <tr><td>Exterior Color:</td><td><%# "" /*Eval("ExteriorColor")*/ %></td></tr>
        <tr><td>Interior Color:</td><td><%# "" /*Eval("InteriorColor")*/ %></td></tr>
        <tr><td>Interior Trim:</td><td>Cloth&nbsp;&nbsp;&nbsp;Leather&nbsp;&nbsp;&nbsp;Vinyl</td></tr>
        <tr><td>Certified?</td><td>Yes&nbsp;&nbsp;&nbsp;&nbsp;No</td></tr>
        <tr><td></td><td></td></tr>
        <asp:Repeater ID="ExtConditionRptr" runat="server" 
            DataSourceID="ConditionTypesDS" >
            
            <ItemTemplate>
            <tr><td colspan="2">
                <asp:Label ID="lbl" CssClass="ConditionLbl" runat="server" Text='<%# Eval("Value") + ":" %>' ></asp:Label>
                Excellent&nbsp;&nbsp;&nbsp;Good&nbsp;&nbsp;&nbsp;Fair
            </td></tr>
                
            </ItemTemplate>
            </asp:Repeater>
            
        <tr><td></td><td></td></tr>
        <tr><td colspan="2">
        <h3>Additional Information<h3>
        <ul class="additionalInfo">
        <asp:Repeater ID="AdditionalInfoItemsList" runat="server" DataSourceID="AdditionalInfoItems">
            <ItemTemplate>
                <li><input type="checkbox" name="additionalInfo" /><%# Eval("QuestionText") %></li>
            </ItemTemplate>
        </asp:Repeater>
        </ul>
    
        </td></tr>
        <tr><td colspan="2">
        
            <h3>Select Trim:</h3>
            <asp:RadioButtonList ID="possibleTrims" runat="server" DataSource='<%# GetStyles(Eval("VIN")) %>' DataTextField="Value" DataValueField="Key"></asp:RadioButtonList>
        
        </td></tr>
        </table>
        </div><!-- end of build details -->
        
        <div class="OptionsBox">
        <h1>Options</h1>
        <%--<asp:CheckBoxList ID="ops"  DataTextField="Description" RepeatColumns="3" RepeatDirection="Vertical" runat="server"></asp:CheckBoxList>--%>
        
        <h2>Exterior:</h2>
        <asp:Repeater ID="questionsGV" runat="server" DataSource='<%# GetEquipment(Eval("VIN"), 2) %>'>
         
        <ItemTemplate>
        <div class="featureGroupBox">
            <asp:Label ID="Label1" runat="server" Text='<%# Eval("headerText") %>' CssClass="featureGroupHeader" ></asp:Label>
            <ul>
            <asp:Repeater runat="server" ID="innerRepeater" DataSource='<%# Container.DataItem %>' >
            <ItemTemplate>
            
                <li><asp:CheckBox ID="itemLb" runat="server" Text='<%# Eval("Description") %>' /></li>
            </ItemTemplate>
            </asp:Repeater>
            </ul>
        </div>
        </ItemTemplate>
        </asp:Repeater>
        
        <h2>Interior:</h2>
        <asp:Repeater ID="Repeater1" runat="server" DataSource='<%# GetEquipment(Eval("VIN"), 1) %>'>
         
        <ItemTemplate>
        <div class="featureGroupBox">
            <asp:Label ID="Label1" runat="server" Text='<%# Eval("headerText") %>' CssClass="featureGroupHeader" ></asp:Label>
            <ul>
            <asp:Repeater runat="server" ID="innerRepeater" DataSource='<%# Container.DataItem %>' >
            <ItemTemplate>
            
                <li><asp:CheckBox ID="itemLb" runat="server" Text='<%# Eval("Description") %>' /></li>
            </ItemTemplate>
            </asp:Repeater>
            </ul>
        </div>
        </ItemTemplate>
        </asp:Repeater>
        
        </div>
        <div id="notesPanel">
            <h3>Notes:</h3> 
            <div class="NotesDiv">
            &nbsp;
            </div>
        </div>
        
  </div> 
</ItemTemplate>
</asp:Repeater>       
        
        </div>
</div>
        <asp:ObjectDataSource ID="AdditionalInfoItems" runat="server" 
            TypeName="FirstLook.Merchandising.DomainModel.Vehicles.VehicleConfigurationDataSource"
            SelectMethod="GetAdditionalInfoItems" OnSelecting="AdditionalInfo_Selecting">
                <SelectParameters>
                    <asp:Parameter Name="BusinessUnitId" Type="Int32" DefaultValue="100150" />
                </SelectParameters>    
            </asp:ObjectDataSource>
            
<asp:ObjectDataSource ID="ConditionTypesDS" runat="server" TypeName="FirstLook.Merchandising.DomainModel.Vehicles.VehicleConditionCollection"
    SelectMethod="FetchConditionTypeNames">
    <SelectParameters>
        <asp:Parameter Name="conditionCategory" Type="Int32" DefaultValue="0" />
    </SelectParameters>
</asp:ObjectDataSource>

    </form>
</body>
</html>
