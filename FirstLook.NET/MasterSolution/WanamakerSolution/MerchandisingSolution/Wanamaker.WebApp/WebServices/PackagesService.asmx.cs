using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using FirstLook.Merchandising.DomainModel.Vehicles;
using VehicleDataAccess;

namespace Wanamaker.WebApp.WebServices
{

    /// <summary>
    /// Summary description for InventoryStockService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService()]
    public class PackagesService : System.Web.Services.WebService
    {
        [WebMethod]
        [System.Web.Script.Services.ScriptMethod]
        public string[] GetPackages(string prefixText, int count, string contextKey)
        {
            //Get parameters
            int chromeStyleId;
            int customizedDescriptionDealerId;
            string[] splits = contextKey.Split(',');
            chromeStyleId = Convert.ToInt32(splits[0]);
            customizedDescriptionDealerId = Convert.ToInt32(splits[1]);
            if( prefixText.Contains( " " ) )
            {
                prefixText = prefixText.Substring( prefixText.LastIndexOf( " " ) ).Replace(" ","");
            }

            //Get list of packages
            EquipmentCollection packages = EquipmentCollection.FetchOptionalByOptionCode(chromeStyleId, String.Empty, customizedDescriptionDealerId);

            //Filter list of packages
            List<string> retList = new List<string>();
            foreach (Equipment item in packages)
            {
                if (item.OptionCode.ToLower().Contains(prefixText.ToLower()) || item.NormalizedDescription.ToLower().Contains(prefixText.ToLower()) || item.ExtDescription.ToLower().Contains(prefixText.ToLower()))
                    retList.Add(item.OptionCodePlusName);
            }

            return retList.ToArray();
        }
    }
}
