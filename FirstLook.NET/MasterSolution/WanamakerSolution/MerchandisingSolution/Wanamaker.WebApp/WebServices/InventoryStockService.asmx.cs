using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using FirstLook.Merchandising.DomainModel.Vehicles;
using VehicleDataAccess;

namespace Wanamaker.WebApp.WebServices
{

    /// <summary>
    /// Summary description for InventoryStockService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService()]
    public class InventoryStockService : System.Web.Services.WebService
    {
        [WebMethod]
        [System.Web.Script.Services.ScriptMethod]
        public string[] GetEquipmentCategories(string prefixText, int count)
        {
            string[] returnAra = new string[count];


            VINStyleDataSource vsds = new VINStyleDataSource();
            Hashtable globalCategories = vsds.GetGlobalCategories();

            int counter = 0;
            foreach (DictionaryEntry de in globalCategories)
            {
                string desc = ((CategoryLink)de.Value).Description;
                if (!desc.ToLower().Contains(prefixText.ToLower()))
                {
                    continue;
                }
                returnAra[counter] = desc.Substring(0, Math.Min(50, desc.Length));

                counter++;
                if (counter >= count)
                {
                    break;
                }

            }
            while (counter < count)
            {
                returnAra[counter] = "";
                counter++;
            }
            return returnAra;

        }

        [WebMethod]
        public EquipmentCollection GetPossibleOptions(int categoryId, int chromeStyleId, string contextKey)
        {

            EquipmentCollection eqc = EquipmentCollection.FetchPossibleOptions(categoryId, chromeStyleId);
            return eqc;

        }
    }
}
