using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.WebControls.UI;
using FirstLook.Merchandising.DomainModel.DataSource;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.WebControls.UI;
using MerchandisingLibrary;

namespace FirstLook.Merchandising.WebControls
{
    public class TemplateVisualizer : DataBoundControl, INamingContainer, IPostBackDataHandler
    {
        private static readonly object EventSampleTextSelected = new object();   
        
        private readonly Button _button = new Button();
        
        public TemplateVisualizer()
        {            
            
        }

        #region properties
        public int CurrentBlurbId
        {
            get
            {
                object o = ViewState["blId"];
                return (o == null) ? 0 : (int) o;
            }
            set
            {
                ViewState["blId"] = value;
            }
        }
        private TemplateItem _currentItem;
        public TemplateItem CurrentItem
        {
            get
            {
                return _currentItem;
            }
            set
            {
                _currentItem = value;                
            }
        }
        #endregion

        [Category("Action"), Description("")]
        public event EventHandler<SampleTextEventArgs> SampleTextSelected
        {
            add
            {
                Events.AddHandler(EventSampleTextSelected, value);
            }
            remove
            {
                Events.RemoveHandler(EventSampleTextSelected, value);
            }
        }

      
        

        protected override void PerformDataBinding(IEnumerable data)
        {
            
            if (data != null)
            {

                IEnumerator en = data.GetEnumerator();

                if (en.MoveNext())
                {
                    CurrentItem = (TemplateItem)en.Current;                    
                }
                
                OnDataBound(EventArgs.Empty);
            }
        }

        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }
        protected override string TagName
        {
            get
            {
                return "div";
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (Page != null)
            {
                
            }

        }
        protected override Control FindControl(string id, int pathOffset)
        {
            return this;
        }
 
        #region IPostBackDataHandler Members

        private bool LoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection)
        {
            string subKey = postDataKey.Substring(UniqueID.Length + 1);
            //int index = int.Parse(subKey, CultureInfo.InvariantCulture);

            EnsureDataBound();

                
            return false;
        }
        bool IPostBackDataHandler.LoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection)
        {
            return LoadPostData(postDataKey, postCollection);
        }

        public void RaisePostDataChangedEvent()
        {
            
        }
        #endregion



        protected override void RenderContents(HtmlTextWriter writer)
        {
            writer.Write(CurrentItem.BlurbName);
            
        }
        
        
        protected override void TrackViewState()
        {
            base.TrackViewState();

        }

        protected override object SaveViewState()
        {

            return new object[] {base.SaveViewState(), CurrentItem.SaveViewState()};

        }

        protected override void LoadViewState(object savedState)
        {
            object baseState = null;
            object controlState1 = null;
            

            object[] stateParts = savedState as object[];
            if (stateParts != null && stateParts.Length > 0)
            {
                baseState = stateParts[0];

                if (stateParts.Length >= 2)
                {
                    controlState1 = stateParts[1];
                }

            }

            CurrentItem.LoadViewState(controlState1);
            base.LoadViewState(baseState);
        }
    
        
    }
}
