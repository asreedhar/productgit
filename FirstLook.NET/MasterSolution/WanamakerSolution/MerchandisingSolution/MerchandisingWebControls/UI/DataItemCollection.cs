using System;
using System.Collections;
using System.ComponentModel;
using System.Web.UI;

namespace FirstLook.Merchandising.WebControls.UI
{
	public class DataItemCollection : StateManagedCollection
	{
		private static readonly Type[] knownTypes = new Type[] { typeof(DataItem) };

		public void Add(DataItem field)
		{
			((IList)this).Add(field);
		}

		public bool Contains(DataItem field)
		{
			return ((IList)this).Contains(field);
		}

		protected override object CreateKnownType(int index)
		{
			switch (index)
			{
				case 0: return new DataItem();
			}
			throw new ArgumentOutOfRangeException("index");
		}

		protected override Type[] GetKnownTypes()
		{
			return knownTypes;
		}

		public int IndexOf(DataItem field)
		{
			return ((IList)this).IndexOf(field);
		}

		public void Insert(int index, DataItem field)
		{
			((IList)this).Insert(index, field);
		}

		protected override void OnValidate(object o)
		{
			base.OnValidate(o);

			if (!(o is DataItem))
			{
				throw new ArgumentException("DataControlFieldCollection_InvalidType");
			}
		}

		public void Remove(DataItem field)
		{
			((IList)this).Remove(field);
		}

		public void RemoveAt(int index)
		{
			((IList)this).RemoveAt(index);
		}

		protected override void SetDirtyObject(object item)
		{
			((DataItem) item).SetDirty();
		}

		[Browsable(false)]
		public DataItem this[int index]
		{
			get
			{
				return (DataItem) ((IList)this)[index];
			}
		}
	}
}
