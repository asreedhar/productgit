using System;

namespace FirstLook.Merchandising.WebControls.UI
{
	public class DataItemEventArgs : EventArgs
	{
		private readonly DataItem _item;
	    private readonly bool _isStandard;

		public DataItemEventArgs(DataItem item, bool isStandard)
		{
			_item = item;
		    _isStandard = isStandard;
		}

		public DataItem Item
		{
			get { return _item; }
		}

	    public bool IsStandard
	    {
	        get { return _isStandard; }
	    }
	}
}
