using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;

namespace FirstLook.Merchandising.WebControls.UI
{
    public class GroupedDataItemCollection : DataItemCollection
    {
        private bool _trackViewState = false;
        private StateBag _viewState;
        protected StateBag ViewState
        {
            get
            {
                if (_viewState == null)
                {
                    _viewState = new StateBag(false);

                    if (_trackViewState)
                    {
                        ((IStateManager)_viewState).TrackViewState();
                    }
                }

                return _viewState;
            }
        }

        protected override void OnClear()
        {
            base.OnClear();
            headerMapping = new Hashtable();
        }

        public List<DataItem> GetByHeader(string header)
        {
            List<DataItem> choiceSet = new List<DataItem>();
            ArrayList indexes = (ArrayList)headerMapping[header];
            foreach (int ndx in indexes)
            {
                choiceSet.Add(base[ndx]);
            }
            return choiceSet;
        }
        public List<string> OrderedHeaders
        {
            get
            {
                List<KeyValuePair<string, int>> cts = HeaderCounts;
                cts.Sort(
                    delegate(KeyValuePair<string, int> first, KeyValuePair<string, int> second)
                    {
                         return second.Value.CompareTo(first.Value);  //longest first...
                     });

                List<string> headerList = new List<string>();
                foreach (KeyValuePair<string, int> kvp in cts)
                {
                    headerList.Add(kvp.Key);
                }
                return headerList;
            }
        }
        public List<KeyValuePair<string, int>> HeaderCounts
        {
            get
            {
                List<KeyValuePair<string, int>> ret = new List<KeyValuePair<string, int>>();
                foreach (string key in headerMapping.Keys)
                {
                    ret.Add(new KeyValuePair<string, int>(key, ((ArrayList)headerMapping[key]).Count));
                }
                return ret;
            }
        }


        //hashtable of string, ArrayList of ints
        private Hashtable headerMapping;
        public Hashtable HeaderMapping
        {
            get
            {
                if (headerMapping == null)
                {
                    headerMapping = new Hashtable();
                }
                return headerMapping;
            }
            set
            {
                headerMapping = value;
            }
        }
        public GroupedDataItemCollection() : base()
        {
            headerMapping = new Hashtable();

        }
        public void Add(DataItem item, string header)
        {
            base.Add(item);
            if (!headerMapping.ContainsKey(header))
            {
                headerMapping.Add(header, new ArrayList());
            }
            ((ArrayList)headerMapping[header]).Add(Count - 1); //add the index of the just inserted item...
        }

        public void LoadViewState(object state)
        {
            object[] stateParts = state as object[];
            object choiceState = null;
            Hashtable headers = null;
            if (stateParts != null && stateParts.Length > 0)
            {
                choiceState = stateParts[0];

                if (stateParts.Length >= 2)
                {
                    headers = stateParts[1] as Hashtable;

                }
            }

            ((IStateManager)this).LoadViewState(choiceState);

            if (headers == null)
            {
                headerMapping = new Hashtable();
            }
            else
            {
                headerMapping = headers;
            }
            
        }

       

        public object SaveViewState()
        {
            object[] state = new object[2];
            state[0] = ((IStateManager) this).SaveViewState();
            state[1] = headerMapping;


            return state;

        }

        public void TrackViewState()
        {
            ((IStateManager)this).TrackViewState();
            _trackViewState = true;
            if (_viewState != null)
            {
                ((IStateManager)_viewState).TrackViewState();
            }
            

        }

        public bool IsTrackingViewState
        {
            get { return _trackViewState; }
        }
    }
}
