using System;
using System.ComponentModel;
using System.Web.UI;

namespace FirstLook.Merchandising.WebControls.UI
{
	public class DataItem : IStateManager
	{
		private StateBag _viewState;

		private bool _trackViewState;

		public DataItem()
		{
		}

		public DataItem(int id, string name)
		{
			Id = id;
			Name = name;
			
		}

		[DefaultValue(true), Localizable(false), Bindable(true), Category("Behavior"), Description("Whether the DataItem is enabled")]
		public bool Enabled
		{
			get
			{
				object value = ViewState["Enabled"];
				if (value == null)
					return true;
				return (bool)value;
			}
			set
			{
				if (Enabled != value)
				{
					if (value)
					{
						ViewState.Remove("Enabled");
					}
					else
					{
						ViewState["Enabled"] = value;
					}
				}
			}
		}

		[DefaultValue(0), Localizable(false), Bindable(true), Category("Data"), Description("The DataItem's Id")]
		public int Id
		{
			get
			{
				object value = ViewState["Id"];
				if (value == null)
					return 0;
				return (int) value;
			}
			set
			{
				if (Id != value)
				{
					if (value == 0)
					{
						ViewState.Remove("Id");
					}
					else
					{
						ViewState["Id"] = value;
					}
				}
			}
		}

		[DefaultValue(""), Localizable(true), Bindable(true), Category("Appearance"), Description("The DataItem's name")]
		public string Name
		{
			get
			{
				string str = (string)ViewState["Name"];
				if (string.IsNullOrEmpty(str))
					return string.Empty;
				return str;
			}
			set
			{
				if (string.Compare(Name, value, StringComparison.InvariantCulture) != 0)
				{
					if (string.IsNullOrEmpty(value))
					{
						ViewState.Remove("Name");
					}
					else
					{
						ViewState["Name"] = value;
					}
				}
			}
		}

		[DefaultValue(false), Localizable(false), Bindable(true), Category("Behavior"), Description("Text")]
		public bool Selected
		{
			get
			{
				object value = ViewState["Selected"];
				if (value == null)
					return false;
				return (bool)value;
			}
			set
			{
				if (Selected != value)
				{
					if (value == false)
					{
						ViewState.Remove("Selected");
					}
					else
					{
						ViewState["Selected"] = value;
					}
				}
			}
		}

		internal void SetDirty()
		{
			ViewState.SetDirty(true);
		}

		protected StateBag ViewState
		{
			get
			{
				if (_viewState == null)
				{
					_viewState = new StateBag(false);

					if (_trackViewState)
					{
						((IStateManager)_viewState).TrackViewState();
					}
				}

				return _viewState;
			}
		}

		#region IStateManager Members

		protected bool IsTrackingViewState
		{
			get
			{
				return _trackViewState;
			}
		}

		bool IStateManager.IsTrackingViewState
		{
			get { return IsTrackingViewState; }
		}

		protected void LoadViewState(object state)
		{
			object[] values = (object[]) state;

			if (values[0] != null)
			{
				((IStateManager)ViewState).LoadViewState(values[0]);
			}
		}

		void IStateManager.LoadViewState(object state)
		{
			LoadViewState(state);
		}

		protected object SaveViewState()
		{
			object[] state = new object[1];

			if (_viewState != null)
			{
				state[0] = ((IStateManager)_viewState).SaveViewState();
			}

			return state;
		}

		object IStateManager.SaveViewState()
		{
			return SaveViewState();
		}

		protected void TrackViewState()
		{
			_trackViewState = true;

			if (_viewState != null)
			{
				((IStateManager)_viewState).TrackViewState();
			}
		}

		void IStateManager.TrackViewState()
		{
			TrackViewState();
		}

		#endregion
	}
}
