using System;
using System.Collections.Generic;
using System.Text;

namespace FirstLook.Merchandising.WebControls.UI
{
    public class SampleTextEventArgs : EventArgs
    {
        private int blurbId;
        private int sampleTextId;

        public SampleTextEventArgs(int blurbId, int sampleTextId)
        {
            this.blurbId = blurbId;
            this.sampleTextId = sampleTextId;
        }

        public int BlurbId
        {
            get { return blurbId; }
            set { blurbId = value; }
        }

        public int SampleTextId
        {
            get { return sampleTextId; }
            set { sampleTextId = value; }
        }
    }
}
