using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: System.Web.UI.WebResource("FirstLook.Merchandising.WebControls.Extenders.DefaultButtonBehavior.js", "text/javascript")]
namespace FirstLook.Merchandising.WebControls.Extenders
{
    [TargetControlType(typeof(Button))]
    [TargetControlType(typeof(ImageButton))]
    public class DefaultButtonExtender : ExtenderControl
    {
        
        public string MonitorElementId
        {
            get
            {
                return (string)ViewState["monitorId"];
            }
            set
            {

                ViewState["monitorId"] = value;
            }
        }

        protected override IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
        {
            ScriptBehaviorDescriptor desc = new ScriptBehaviorDescriptor("FirstLook.Merchandising.WebControls.Extenders.DefaultButton", targetControl.ClientID);
            desc.AddProperty("monitoredElementId", GetControlClientId(MonitorElementId));
            
            
            yield return desc;
        }
        private string GetControlClientId(string controlId)
        {
            Control ctl = this.NamingContainer.FindControl(controlId);
            if (ctl != null)
            {
                return ctl.ClientID;
            }
            return string.Empty;

        }

        protected override IEnumerable<ScriptReference> GetScriptReferences()
        {
            ScriptReference refer =
                new ScriptReference("FirstLook.Merchandising.WebControls.Extenders.DefaultButtonBehavior.js",
                                    "FirstLook.Merchandising.WebControls");
            refer.IgnoreScriptPath = true;
            yield return refer;
        }
    }
}
