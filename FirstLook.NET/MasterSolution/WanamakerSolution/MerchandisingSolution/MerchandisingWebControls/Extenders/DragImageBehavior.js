// README
//
// There are two steps to adding a property:
//
// 1. Create a member variable to store your property
// 2. Add the get_ and set_ accessors for your property
//
// Remember that both are case sensitive!


/// <reference name="MicrosoftAjaxTimer.debug.js" />
/// <reference name="MicrosoftAjaxWebForms.debug.js" />
/// <reference name="AjaxControlToolkit.ExtenderBase.BaseScripts.js" assembly="AjaxControlToolkit" />

Type.registerNamespace('FirstLook.Merchandising.WebControls.Extenders');


FirstLook.Merchandising.WebControls.Extenders.DragImageBehavior = function(element) {
    FirstLook.Merchandising.WebControls.Extenders.DragImageBehavior.initializeBase(this, [element]);

    // TODO : (Step 1) Add your property variables here
    this._DragArguments = null;
    this._mouseDownHandler = Function.createDelegate(this,
        this.mouseDownHandler);
    this._visual = null;
    this._tmpParent = null;
}
FirstLook.Merchandising.WebControls.Extenders.DragImageBehavior.prototype = {
    
    // IDragSource methods
    get_dragDataType: function()
    {
        return 'DragDropPhoto';
    },

    getDragData: function(context)
    {
        var retVal = new Array();
        retVal[0] = this.get_element();
        retVal[1] = this._DragArguments;
        return (retVal);
        
    },

    get_dragMode: function()
    {
        return AjaxControlToolkit.DragMode.Copy;
    },

    onDragStart: function() {},

    onDrag: function() {},

    onDragEnd: function(canceled)
    {
        
        if (this._visual && this._tmpParent)
            this._tmpParent.removeChild(this._visual);
        
    },
    
    
    initialize : function() {
        FirstLook.Merchandising.WebControls.Extenders.DragImageBehavior.callBaseMethod(this, 'initialize');
        
        // TODO: Add your initalization code here
        //this._DragArguments = '1';
        $addHandler(this.get_element(), 'mousedown',
            this._mouseDownHandler)
    },

    mouseDownHandler: function(ev)
    {
        window._event = ev; // Needed internally by _DragDropManager
        this._tmpParent = document.body;
        //this._tmpParent = this.get_element().parentNode;
        this._visual = this.get_element().cloneNode(true);
        this._visual.style.opacity = '0.4';
        this._visual.style.filter =
          'progid:DXImageTransform.Microsoft.BasicImage(opacity=0.4)';
        this._visual.style.zIndex = 99999;
        this._tmpParent.appendChild(this._visual);
        //var xoff = this.get_element().offsetLeft;
        //var yoff = this.get_element().offsetTop;
        //Sys.UI.DomElement.setLocation(this._visual, 0,0);//xoff,yoff);
        
        var location = Sys.UI.DomElement.getLocation(this.get_element());
        Sys.UI.DomElement.setLocation(this._visual, location.x, location.y);

        AjaxControlToolkit.DragDropManager.startDragDrop(this,
            this._visual, null);
    },

    dispose: function()
    {
        if (this._mouseDownHandler)
            $removeHandler(this.get_element(), 'mousedown',
                this._mouseDownHandler);
        this._mouseDownHandler = null;
        FirstLook.Merchandising.WebControls.Extenders.DragImageBehavior.callBaseMethod(this,
            'dispose');
    },

    // TODO: (Step 2) Add your property accessors here
    get_DragArguments : function() {
        return this._DragArguments;
    },

    set_DragArguments : function(value) {
        this._DragArguments = value;
    }
}

FirstLook.Merchandising.WebControls.Extenders.DragImageBehavior.registerClass
    ('FirstLook.Merchandising.WebControls.Extenders.DragImageBehavior', Sys.UI.Behavior,
    AjaxControlToolkit.IDragSource);
//if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();