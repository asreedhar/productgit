/**/

    Type.registerNamespace('FirstLook.Merchandising.WebControls.Extenders');
    
    FirstLook.Merchandising.WebControls.Extenders.DirtyButton = function(element) {
        FirstLook.Merchandising.WebControls.Extenders.DirtyButton.initializeBase(this, [element]);
        this._monitoredElement = null;
        this._monitoredElementId = null;
        this._dirtyClass = null;
    }
    FirstLook.Merchandising.WebControls.Extenders.DirtyButton.prototype = {
        initialize : function() {
            FirstLook.Merchandising.WebControls.Extenders.DirtyButton.callBaseMethod(this, 'initialize');
            var callback = Function.createCallback(this._onClick, this);
            $addHandlers(this.get_element(), {click: callback});
        },
        _onClick : function(evt, context) {
            Sys.UI.DomElement.removeCssClass(context.get_element(),context._dirtyClass);            
        },
        _setDirty : function(evt, context) {
            Sys.UI.DomElement.addCssClass(context.get_element(),context._dirtyClass);
            return true;
        },
        get_dirtyClass : function() {
            return this._dirtyClass;
        },
        set_dirtyClass : function(value) {
            this._dirtyClass = value;
        },
        get_monitoredElementId : function() {
            return this._monitoredElementId;
        },
        set_monitoredElementId : function(value) {
            this._monitoredElementId = value;
            this._monitoredElement = $get(value);
            
            if (this._monitoredElement != null && this.monitoredElement != 'undefined') {
                
                var callback = Function.createCallback(this._setDirty, this);
                
                var sels = this._monitoredElement.getElementsByTagName('select');
                for (var i = 1; i < sels.length; i++) {
                    $addHandler(sels[i], 'change', callback);
                    
                }
                var sels = this._monitoredElement.getElementsByTagName('input');
                for (var i = 0; i < sels.length; i++) {
                    $addHandler(sels[i], 'change', callback);
                }
                var sels = this._monitoredElement.getElementsByTagName('textarea');
                for (var i = 0; i < sels.length; i++) {
                    $addHandler(sels[i], 'change', callback);
                }
            }
        },
        dispose : function() {
            $clearHandlers(this.get_element());
            FirstLook.Merchandising.WebControls.Extenders.DirtyButton.callBaseMethod(this, 'dispose');
            
        }
    }
    FirstLook.Merchandising.WebControls.Extenders.DirtyButton.registerClass(
        'FirstLook.Merchandising.WebControls.Extenders.DirtyButton',
        Sys.UI.Behavior);
    
