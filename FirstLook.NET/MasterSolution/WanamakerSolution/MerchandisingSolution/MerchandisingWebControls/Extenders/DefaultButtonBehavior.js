/**/

    Type.registerNamespace('FirstLook.Merchandising.WebControls.Extenders');
    
    FirstLook.Merchandising.WebControls.Extenders.DefaultButton = function(element) {
        FirstLook.Merchandising.WebControls.Extenders.DefaultButton.initializeBase(this, [element]);
        this._monitoredElement = null;
        this._monitoredElementId = null;
    }
    FirstLook.Merchandising.WebControls.Extenders.DefaultButton.prototype = {
        initialize : function() {
            FirstLook.Merchandising.WebControls.Extenders.DefaultButton.callBaseMethod(this, 'initialize');
        },
        _onKeyPress : function(e, context) {
            //Sys.Debug.traceDump(e);
            //Sys.Debug.traceDump(context);
            //Sys.Debug.traceDump(context.get_element().id);
            
            if (e.charCode == Sys.UI.Key.enter) {
                context.btn.click();                
            }
            e.preventDefault = true;
        },
        get_monitoredElementId : function() {
            return this._monitoredElementId;
        },
        set_monitoredElementId : function(value) {
            this._monitoredElementId = value;
            this._monitoredElement = $get(value);
            
            if (this._monitoredElement != null && this.monitoredElement != 'undefined') {                
               var callback = Function.createCallback(this._onKeyPress, {sender:this, btn:this.get_element()});
               $addHandlers(this._monitoredElement, {keypress: callback});
            }
        },
        dispose : function() {
            $clearHandlers(this.get_element());
            $clearHandlers(this._monitoredElement);
            FirstLook.Merchandising.WebControls.Extenders.DefaultButton.callBaseMethod(this, 'dispose');
            
        }
    }
    FirstLook.Merchandising.WebControls.Extenders.DefaultButton.registerClass(
        'FirstLook.Merchandising.WebControls.Extenders.DefaultButton',
        Sys.UI.Behavior);
    
