
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.ComponentModel;


[assembly: System.Web.UI.WebResource("FirstLook.Merchandising.WebControls.Extenders.DragImageBehavior.js", "text/javascript")]

namespace FirstLook.Merchandising.WebControls.Extenders
{
    [TargetControlType(typeof(Image))]
    public class DragImageExtender : ExtenderControl
    {
        // TODO: Add your property accessors here.
        //
        
        [DefaultValue("")]
        public string DragArguments
        {
            get
            {
                return GetPropertyValue("DragArguments", "");
            }
            set
            {
                SetPropertyValue("DragArguments", value);
            }
        }

        private TValue GetPropertyValue<TValue>(string propertyName, TValue nullValue)
        {
            if (ViewState[propertyName] == null)
            {
                return nullValue;
            }
            return (TValue)ViewState[propertyName];
        }

        protected void SetPropertyValue<TValue>(string propertyName, TValue value)
        {
            ViewState[propertyName] = value;
        }


        protected override IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
        {
            ScriptBehaviorDescriptor descriptor = new ScriptBehaviorDescriptor("FirstLook.Merchandising.WebControls.Extenders.DragImageBehavior", targetControl.ClientID);
            descriptor.AddProperty("DragArguments", DragArguments);
            return new ScriptDescriptor[] { descriptor };
        }

        protected override IEnumerable<ScriptReference> GetScriptReferences()
        {
            
            return new ScriptReference[]
                       {
                           new ScriptReference("AjaxControlToolkit.Common.Common.js", "AjaxControlToolkit"), 
                           new ScriptReference("AjaxControlToolkit.Compat.DragDrop.DragDropScripts.js", "AjaxControlToolkit"), 
                           new ScriptReference("FirstLook.Merchandising.WebControls.Extenders.DragImageBehavior.js", "FirstLook.Merchandising.WebControls")
                       };
        }
    }
}
