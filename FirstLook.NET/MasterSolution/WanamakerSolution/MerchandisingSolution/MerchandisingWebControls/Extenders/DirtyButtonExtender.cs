using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: System.Web.UI.WebResource("FirstLook.Merchandising.WebControls.Extenders.DirtyButtonBehavior.js", "text/javascript")]
namespace FirstLook.Merchandising.WebControls.Extenders
{
    [TargetControlType(typeof(Button))]
    public class DirtyButtonExtender : ExtenderControl
    {

        public string DirtyButtonCssClass
        {
            get
            {
                return (string) ViewState["dirtyButtonCssClass"];
            }
            set
            {
                ViewState["dirtyButtonCssClass"] = value;
            }
        }

        public string MonitorElementClientId
        {
            get
            {
                return (string)ViewState["monitorId"];
            }
            set
            {
                ViewState["monitorId"] = value;
            }
        }

        protected override IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
        {
            ScriptBehaviorDescriptor desc = new ScriptBehaviorDescriptor("FirstLook.Merchandising.WebControls.Extenders.DirtyButton", targetControl.ClientID);
            desc.AddProperty("monitoredElementId", MonitorElementClientId);
            desc.AddProperty("dirtyClass", DirtyButtonCssClass);

            yield return desc;
        }

        protected override IEnumerable<ScriptReference> GetScriptReferences()
        {
            yield return new ScriptReference("FirstLook.Merchandising.WebControls.Extenders.DirtyButtonBehavior.js", "FirstLook.Merchandising.WebControls");
        }
    }
}
