// README
//
// There are two steps to adding a property:
//
// 1. Create a member variable to store your property
// 2. Add the get_ and set_ accessors for your property
//
// Remember that both are case sensitive!


/// <reference name="MicrosoftAjaxTimer.debug.js" />
/// <reference name="MicrosoftAjaxWebForms.debug.js" />
/// <reference name="AjaxControlToolkit.ExtenderBase.BaseScripts.js" assembly="AjaxControlToolkit" />



Type.registerNamespace('FirstLook.Merchandising.WebControls.Extenders');

FirstLook.Merchandising.WebControls.Extenders.ImageDropBehavior = function(element) {
    FirstLook.Merchandising.WebControls.Extenders.ImageDropBehavior.initializeBase(this, [element]);

    // TODO : (Step 1) Add your property variables here
    this._PhotoTypeId = null;
    this._SequenceNumber = null;
    this._CurrentPhotoId = null;
    this._DefaultImageUrl = null;
    this._HiddenFieldId = null;
    this._DumpingPanelId = null;
    this._IsDelete = null;
}
FirstLook.Merchandising.WebControls.Extenders.ImageDropBehavior.prototype = {
    
    // IDropTarget methods
    get_dropTargetElement: function()
    {
        return this.get_element();
    },

    canDrop: function(dragMode, dataType, data)
    {
        return (dataType == 'DragDropPhoto' && data);
    },
    
    
    
    drop: function(dragMode, dataType, data)
    {
        
        if (dataType == 'DragDropPhoto' && data)
        {
            
            var el = document.getElementById(this._HiddenFieldId);
            if (el != 'undefined') {
                
                if (this._IsDelete == "1" || this._IsDelete == "true") {
                    this.setDeleteData(data[1],el);
                }else {
                    this.setPictureData(data[1], el, this._PhotoTypeId);
                    
                    var currImg = this.get_element().getElementsByTagName('img');
                    
                    if (currImg && currImg.length > 0 && this._CurrentPhotoId && this._DumpingPanelId) {
                        
                        var dump = document.getElementById(this._DumpingPanelId);
                        
                        if (dump) {
                            dump.appendChild(currImg[0]);
                            this.setPictureData(this._CurrentPhotoId,el,1); //set to undefined
                        }
                    }
                }
                this.get_element().appendChild(data[0]);
                this._CurrentPhotoId = data[1];
                
                if (this._IsDelete == "1" || this.IsDelete == "true") {
                    this.get_element().removeChild(data[0]);
                }
            }
            this.get_element().style.backgroundColor = '';
        }
    },
    
    setDeleteData : function(picId,hiddenEl) {
        var re = new RegExp("[,][\\d]*[|][\\d]*[|]" + picId);
        hiddenEl.value = hiddenEl.value.replace(re, "");
        hiddenEl.value += ',DELETE|0|' + picId;
    },
    setPictureData : function(picId, hiddenEl, newPhotoTypeId) {
        
        var re = new RegExp("[,][\\d]*[|][\\d]*[|]" + picId);
        hiddenEl.value = hiddenEl.value.replace(re, "");
        hiddenEl.value += ',' + newPhotoTypeId + '|' + this._SequenceNumber + '|' + picId;
    },
    onDragEnterTarget: function(dragMode, dataType, data)
    {
        
        // Highlight the drop zone by changing its background
        // color to light gray
        if (dataType == 'DragDropPhoto' && data)
        {
            this.get_element().style.backgroundColor = '#E0E0E0';
            this.get_element().style.borderColor = '#FFFFAA';
        }
    },
    
    onDragLeaveTarget: function(dragMode, dataType, data)
    {
        
        // Unhighlight the drop zone by restoring its original
        // background color
        if (dataType == 'DragDropPhoto' && data)
        {
            this.get_element().style.backgroundColor = '';
            this.get_element().style.borderColor = '';
        }
    },

    onDragInTarget: function(dragMode, dataType, data) {},
    
    initialize : function() {
        FirstLook.Merchandising.WebControls.Extenders.ImageDropBehavior.callBaseMethod(this, 'initialize');
        // TODO: Add your initalization code here
        AjaxControlToolkit.DragDropManager.registerDropTarget(this);
        
        if (this._DefaultImageUrl) {
            this.get_element().style.backgroundImage = 'url(' + this._DefaultImageUrl + ')';
        }
    },

    dispose : function() {
        // TODO: Add your cleanup code here
        AjaxControlToolkit.DragDropManager.unregisterDropTarget(this);
        
        FirstLook.Merchandising.WebControls.Extenders.ImageDropBehavior.callBaseMethod(this, 'dispose');
    },

    // TODO: (Step 2) Add your property accessors here
    get_CurrentPhotoId : function() {
        return this._CurrentPhotoId;
    },

    set_CurrentPhotoId : function(value) {
        this._CurrentPhotoId = value;
    },
    
    get_SequenceNumber : function() {
        return this._SequenceNumber;
    },

    set_SequenceNumber : function(value) {
        this._SequenceNumber = value;
    },
    
    get_PhotoTypeId : function() {
        return this._PhotoTypeId;
    },

    set_PhotoTypeId : function(value) {
        this._PhotoTypeId = value;
    },
    
    get_HiddenFieldId : function() {
        return this._HiddenFieldId;
    },

    set_HiddenFieldId  : function(value) {
        this._HiddenFieldId = value;
    },
    
    get_DumpingPanelId : function() {
        return this._DumpingPanelId;
    },

    set_DumpingPanelId  : function(value) {
        this._DumpingPanelId = value;
    },
    
    get_DefaultImageUrl : function() {
        return this._DefaultImageUrl;
    },

    set_DefaultImageUrl : function(value) {
        this._DefaultImageUrl = value;
    },
    
    get_IsDelete : function() {
        return this._IsDelete;
    },

    set_IsDelete : function(value) {
        this._IsDelete = value;
    }
    
}

FirstLook.Merchandising.WebControls.Extenders.ImageDropBehavior.registerClass
    ('FirstLook.Merchandising.WebControls.Extenders.ImageDropBehavior', Sys.UI.Behavior,
    AjaxControlToolkit.IDropTarget);

//if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();