using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.ComponentModel;



[assembly: WebResource("FirstLook.Merchandising.WebControls.Extenders.ImageDropBehavior.js", "text/javascript")]

namespace FirstLook.Merchandising.WebControls.Extenders
{
    
    [TargetControlType(typeof(Panel))]
    public class ImageDropExtender : ExtenderControl
    {
        // TODO: Add your property accessors here.
        //
        [IDReferenceProperty(typeof(WebControl))]
        [DefaultValue("")]
        public string CurrentPhotoId
        {
            get
            {
                return GetPropertyValue("CurrentPhotoId", "");
            }
            set
            {
                SetPropertyValue("CurrentPhotoId", value);
            }
        }

        [DefaultValue("")]
        public string DefaultImageUrl
        {
            get
            {
                return GetPropertyValue("DefaultImageUrl", "");
            }
            set
            {
                SetPropertyValue("DefaultImageUrl", value);
            }
        }

        

        [DefaultValue("-1")]
        public string PhotoTypeId
        {
            get
            {
                return GetPropertyValue("PhotoTypeId", "");
            }
            set
            {
                SetPropertyValue("PhotoTypeId", value);
            }
        }

        [IDReferenceProperty(typeof(WebControl))]
        [DefaultValue("")]
        public string HiddenFieldId
        {
            get
            {
                return GetPropertyValue("HiddenFieldId", "");
            }
            set
            {
                SetPropertyValue("HiddenFieldId", value);
            }
        }

        [DefaultValue("")]
        public string SequenceNumber
        {
            get
            {
                return GetPropertyValue("SequenceNumber", "");
            }
            set
            {
                SetPropertyValue("SequenceNumber", value);
            }
        }

        [IDReferenceProperty(typeof(WebControl))]
        [DefaultValue("")]
        public string DumpingPanelId
        {
            get
            {
                return GetPropertyValue("DumpingPanelId", "");
            }
            set
            {
                SetPropertyValue("DumpingPanelId", value);
            }
        }

        [DefaultValue("")]
        public string IsDelete
        {
            get
            {
                return GetPropertyValue("IsDelete", "");
            }
            set
            {
                SetPropertyValue("IsDelete", value);
            }
        }


        private TValue GetPropertyValue<TValue>(string propertyName, TValue nullValue)
        {
            if (ViewState[propertyName] == null)
            {
                return nullValue;
            }
            return (TValue)ViewState[propertyName];
        }

        protected void SetPropertyValue<TValue>(string propertyName, TValue value)
        {
            ViewState[propertyName] = value;
        }


        protected override IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
        {
            ScriptBehaviorDescriptor descriptor = new ScriptBehaviorDescriptor("FirstLook.Merchandising.WebControls.Extenders.ImageDropBehavior", targetControl.ClientID);
            descriptor.AddProperty("IsDelete", IsDelete);
            descriptor.AddProperty("DumpingPanelId", DumpingPanelId);
            descriptor.AddProperty("HiddenFieldId", HiddenFieldId);
            descriptor.AddProperty("PhotoTypeId", PhotoTypeId);
            descriptor.AddProperty("DefaultImageUrl", DefaultImageUrl);
            descriptor.AddProperty("CurrentPhotoId", CurrentPhotoId);
            descriptor.AddProperty("SequenceNumber", SequenceNumber);
            return new ScriptDescriptor[] {descriptor};
        }

        protected override IEnumerable<ScriptReference> GetScriptReferences()
        {
            
            return new ScriptReference[]
                       {
                           new ScriptReference("AjaxControlToolkit.Compat.DragDrop.DragDropScripts.js", "AjaxControlToolkit"), 
                           new ScriptReference("FirstLook.Merchandising.WebControls.Extenders.ImageDropBehavior.js", "FirstLook.Merchandising.WebControls")
                       };
        }
    }
}
