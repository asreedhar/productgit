using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using VehicleDataAccess;

namespace FirstLook.Merchandising.WebControls
{
    public class TrimHelper : WebControl
    {
        protected override string TagName
        {
            get { return "Div"; }
        }

        protected override HtmlTextWriterTag TagKey
        {
            get { return HtmlTextWriterTag.Div; }
        }

        public string EquipmentIncludedImageUrl
        {
            get
            {
                string url = ViewState["url"] as string;
                return string.IsNullOrEmpty(url) ? string.Empty : url;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    ViewState.Remove("url");
                }
                else if (value != EquipmentIncludedImageUrl)
                {
                    ViewState["url"] = value;
                }
            }
        }

        public string EvenRowCssClass
        {
            get
            {
                string str = ViewState["even"] as string;
                return string.IsNullOrEmpty(str) ? string.Empty : str;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    ViewState.Remove("even");
                }
                else if (value != EvenRowCssClass)
                {
                    ViewState["even"] = value;
                }
            }
        }


        public string OddRowCssClass
        {
            get
            {
                string str = ViewState["odd"] as string;
                return string.IsNullOrEmpty(str) ? string.Empty : str;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    ViewState.Remove("odd");
                }
                else if (value != OddRowCssClass)
                {
                    ViewState["odd"] = value;
                }
            }
        }


        public string Vin
        {
            get
            {
                string str = ViewState["vin"] as string;
                return string.IsNullOrEmpty(str) ? string.Empty : str;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    ViewState.Remove("vin");
                }
                else if (value != Vin)
                {
                    ViewState.Remove("tdata");
                    ViewState["vin"] = value;
                }
            }
        }
        private DataTable TrimData
        {
            get
            {
                DataTable dt = ViewState["tdata"] as DataTable;
                if (dt == null)
                {
                    if (!string.IsNullOrEmpty(Vin))
                    {
                        dt = new InventoryDataSource().TrimHelpFetch(Vin);
                    }
                    //if the data is still null, remove that guy...
                    if (dt == null)
                    {
                        ViewState.Remove("tdata");
                    }
                    else
                    {
                        ViewState["tdata"] = dt;
                    }                    
                }
                return dt ?? new DataTable();
            }
            
        }
        protected override void Render(HtmlTextWriter writer)
        {
            base.AddAttributesToRender(writer);
            writer.RenderBeginTag(TagKey);


            //if not two columns, then pointless to display
            if (TrimData.Columns.Count < 2 || TrimData.Rows.Count < 1)
            {
                writer.RenderEndTag();
                return;
            }
            writer.RenderBeginTag(HtmlTextWriterTag.Table);
            //create header row
            writer.RenderBeginTag(HtmlTextWriterTag.Thead);
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            foreach (DataColumn dc in TrimData.Columns)
            {
                if (dc.Ordinal == 0)
                {
                    writer.AddAttribute("class", "CategoryColumn");
                    writer.RenderBeginTag(HtmlTextWriterTag.Th);
                    writer.Write("Equipment");
                    writer.RenderEndTag();
                }
                else
                {
                    writer.AddAttribute("class", "StyleColumn");
                    writer.RenderBeginTag(HtmlTextWriterTag.Th);
                    writer.Write(dc.ColumnName);
                    writer.RenderEndTag();
                }
            }
            writer.RenderEndTag(); //end TR
            writer.RenderEndTag(); //end THEAD

            writer.RenderBeginTag(HtmlTextWriterTag.Tbody);

            bool evenFlag = false;
            foreach (DataRow dr in TrimData.Rows)
            {
                if (evenFlag)
                {
                    if (!string.IsNullOrEmpty(EvenRowCssClass))
                        writer.AddAttribute("class", EvenRowCssClass);
                }
                else
                {
                    if (!string.IsNullOrEmpty(OddRowCssClass))
                        writer.AddAttribute("class", OddRowCssClass);
                }
                evenFlag = !evenFlag;
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.RenderBeginTag(HtmlTextWriterTag.Th);
                writer.Write((string) dr[0]);
                writer.RenderEndTag(); //end TH

                //just covered the description column...so start with 1
                for (int i = 1; i < TrimData.Columns.Count; i++)
                {
                    
                    if ((int) dr[i] > 0) //if we should check it...check it!
                    {
                        writer.AddAttribute("class", "CheckedCell");
                    }
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.RenderEndTag(); //end td
                }

                writer.RenderEndTag(); //end TR
            }
            writer.RenderEndTag(); //end TBODY
            writer.RenderEndTag(); //end TABLE

            writer.RenderEndTag(); //end tagkey
        }
    }
}