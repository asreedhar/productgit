using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.WebControls.UI;
using FirstLook.Merchandising.DomainModel.DataSource;
using FirstLook.Merchandising.WebControls.UI;

namespace FirstLook.Merchandising.WebControls
{
    public class EquipmentGroups : DataBoundControl, INamingContainer, IPostBackDataHandler, IScriptControl
    {
        private ScriptManager manager;

        private static readonly object EventCheckedEquipement = new object();
        private static readonly object EventItemDataBound = new object();

        private readonly static HiddenField _clientField = new HiddenField();
        private static readonly string _collapseControlID = "ui-child-ex";
        //headerCssClass
        //groupFlagPropertyField --IsStandard
        //ItemDescriptionField
        //ItemValueField
        //ItemHeaderField

        private readonly CheckBox _checkBox = new CheckBox();
        private readonly List<Pair> _checkedEquipment = new List<Pair>();

        public EquipmentGroups()
        {
            _checkBox.AutoPostBack = false;
            _checkBox.EnableViewState = false;
            _checkBox.ID = "0";

            _clientField.ID = "ClientState";
        }

        #region properties 
        public int RepeatColumns
        {
            get
            {
                object value = ViewState["col"];
                if (value == null)
                    return 4;
                return (int)value;
            }
            set
            {
                if (RepeatColumns != value)
                {
                    ViewState["col"] = value;
                }
            }
        }
        public string ChildDivCssClass
        {
            get
            {
                object value = ViewState["cdiv"];
                if (value == null)
                    return string.Empty;
                return (string)value;
            }
            set
            {
                if (string.Compare(ChildDivCssClass, value, StringComparison.InvariantCulture) != 0)
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        ViewState.Remove("cdiv");
                    }
                    else
                    {
                        ViewState["cdiv"] = value;
                    }
                }
            }
        }
        public string ChildDivTitle
        {
            get
            {
                object value = ViewState["cdivt"];
                if (value == null)
                    return string.Empty;
                return (string)value;
            }
            set
            {
                if (string.Compare(ChildDivCssClass, value, StringComparison.InvariantCulture) != 0)
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        ViewState.Remove("cdivt");
                    }
                    else
                    {
                        ViewState["cdivt"] = value;
                    }
                }
            }
        }
        public string HeaderCssClass
        {
            get
            {
                object value = ViewState["hcls"];
                if (value == null)
                    return string.Empty;
                return (string)value;
            }
            set
            {
                if (string.Compare(ChildDivCssClass, value, StringComparison.InvariantCulture) != 0)
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        ViewState.Remove("hcls");
                    }
                    else
                    {
                        ViewState["hcls"] = value;
                    }
                }
            }
        }
        
        public string ChildDivID
        {
            get
            {
                object value = ViewState["cdivid"];
                if (value == null)
                    return "equipment-child";
                return (string)value;
            }
            set
            {
                if (string.Compare(ChildDivID, value, StringComparison.InvariantCulture) != 0)
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        ViewState.Remove("cdivid");
                    }
                    else
                    {
                        ViewState["cdivid"] = value;
                    }
                }
            }
        }

        private GroupedDataItemCollection _choices;
        protected GroupedDataItemCollection OptionChoices
        {
            get
            {
                if (_choices == null)
                {
                    _choices = new GroupedDataItemCollection();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager)_choices).TrackViewState();
                    }
                }
                return _choices;
            }
            set
            {
                _choices = value;
            }
        }
        private GroupedDataItemCollection _stdchoices;
        protected GroupedDataItemCollection StandardChoices
        {
            get
            {
                if (_stdchoices == null)
                {
                    _stdchoices = new GroupedDataItemCollection();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager)_stdchoices).TrackViewState();
                    }
                }
                return _stdchoices;
            }
            set
            {
                _stdchoices = value;
            }
        }
        #endregion

        [Category("Action"), Description("")]
        public event EventHandler<DataItemEventArgs> CheckedEquipment
        {
            add
            {
                Events.AddHandler(EventCheckedEquipement, value);
            }
            remove
            {
                Events.RemoveHandler(EventCheckedEquipement, value);
            }
        }

        private void OnCheckedEquipment(DataItem item, bool isStandard)
        {
            EventHandler<DataItemEventArgs> handler = (EventHandler<DataItemEventArgs>)Events[EventCheckedEquipement];

            if (handler != null)
            {
                handler(this, new DataItemEventArgs(item, isStandard));
            }
        }

        [Category("Action"), Description("")]
        public event EventHandler<DataItemEventArgs> ItemDataBound
        {
            add
            {
                Events.AddHandler(EventItemDataBound, value);
            }
            remove
            {
                Events.RemoveHandler(EventItemDataBound, value);
            }
        }

        private void OnItemDataBound(DataItem item, bool isStandard)
        {
            EventHandler<DataItemEventArgs> handler = (EventHandler<DataItemEventArgs>)Events[EventItemDataBound];

            if (handler != null)
            {
                handler(this, new DataItemEventArgs(item, isStandard));
            }
        }
        

        protected override void PerformDataBinding(IEnumerable data)
        {
            OptionChoices.Clear();
            StandardChoices.Clear();

            if (data != null)
            {
                IEnumerator en = data.GetEnumerator();

                while (en.MoveNext())
                {
                    GenericChoice ch = (GenericChoice)en.Current;
                    DataItem di = new DataItem(ch.ID, ch.Description);
                    if (ch.IsStandard)
                    {
                        StandardChoices.Add(di, ch.Header);
                    }else {
                        OptionChoices.Add(di, ch.Header);
                    }

                    OnItemDataBound(di, ch.IsStandard);
                }
                
                OnDataBound(EventArgs.Empty);
            }
        }

        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }
        protected override string TagName
        {
            get
            {
                return "div";
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Controls.Add(_checkBox);
            Controls.Add(_clientField);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (Page != null)
            {
                for (int i = 0, l = OptionChoices.Count; i < l; i++)
                {
                    _checkBox.ID = i.ToString(CultureInfo.InvariantCulture);
                        
                    Page.RegisterRequiresPostBack(_checkBox);
                }
                for (int j=OptionChoices.Count, l=OptionChoices.Count + StandardChoices.Count; j<l; j++)
                {
                    _checkBox.ID = j.ToString(CultureInfo.InvariantCulture);

                    Page.RegisterRequiresPostBack(_checkBox);
                }
            }

            if (!DesignMode)
            {
                manager = ScriptManager.GetCurrent(Page);

                if (manager != null)
                    manager.RegisterScriptControl(this);
                else
                    throw new ApplicationException("You must have a ScriptManager!");
            }
        }
        protected override Control FindControl(string id, int pathOffset)
        {
            return this;
        }
 
        #region IPostBackDataHandler Members

        private bool LoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection)
        {
            string subKey = postDataKey.Substring(UniqueID.Length + 1);

            if (subKey == _clientField.ID)
                return false;

            int index = int.Parse(subKey, CultureInfo.InvariantCulture);
            EnsureDataBound();

            bool returnValue = false;
            if (index >= 0)
            {
                DataItem item;
                bool isStandard = false;
                if (index < OptionChoices.Count)
                {
                    item = OptionChoices[index];
                }
                else if (index < OptionChoices.Count + StandardChoices.Count)
                {
                    item = StandardChoices[index - OptionChoices.Count];
                    isStandard = true;
                }
                else
                {
                    return false;
                }

                bool flag = postCollection[postDataKey] != null;
                item.Selected = flag;
                if (item.Selected)
                {
                    _checkedEquipment.Add(new Pair(item, isStandard));
                    returnValue = _checkedEquipment.Count == 1;
                }
            }

            return returnValue;
        }

        bool IPostBackDataHandler.LoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection)
        {
            return LoadPostData(postDataKey, postCollection);
        }

        public void RaisePostDataChangedEvent()
        {
            foreach (Pair itemPair in _checkedEquipment)
            {
                DataItem item = itemPair.First as DataItem;
                bool isStandard = (bool)itemPair.Second;
                OnCheckedEquipment(item, isStandard);
            }
        }
        #endregion


        protected override void RenderContents(HtmlTextWriter writer)
        {
            int itemCount = OptionChoices.Count;

            //if (itemCount != 0)
            //{

                
                int index = 0;
                

                foreach (string header in OptionChoices.OrderedHeaders)
                {
                    
                    writer.RenderBeginTag(HtmlTextWriterTag.Ul);

                    writer.RenderBeginTag(HtmlTextWriterTag.Li);
                    writer.RenderBeginTag(HtmlTextWriterTag.H5);
                    writer.Write(header);
                    writer.RenderEndTag();
                    writer.RenderEndTag();

                    foreach (DataItem choice in OptionChoices.GetByHeader(header))
                    {

                        RenderItem(choice, OptionChoices.IndexOf(choice), writer);
                        index++;
                    }

                    writer.RenderEndTag();
                }

                //render the client field
                _clientField.RenderControl(writer);

                if (!string.IsNullOrEmpty(ChildDivCssClass))
                {
                    writer.AddAttribute("class", ChildDivCssClass);
                }
                writer.AddAttribute("id", ID + ClientIDSeparator + _collapseControlID);
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.Write(ChildDivTitle);
                writer.RenderEndTag();

                writer.AddAttribute("id", ChildDivClientID);
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                int optionCount = index;    //save total count of options for use in std IDs
                foreach (string header in StandardChoices.OrderedHeaders)
                {

                    writer.RenderBeginTag(HtmlTextWriterTag.Ul);

                    writer.RenderBeginTag(HtmlTextWriterTag.Li);
                    writer.RenderBeginTag(HtmlTextWriterTag.H5);
                    writer.Write(header);
                    writer.RenderEndTag(); //end H5
                    writer.RenderEndTag(); //end LI

                    foreach (DataItem choice in StandardChoices.GetByHeader(header))
                    {

                        RenderItem(choice, optionCount + StandardChoices.IndexOf(choice), writer);
                        index++;
                    }

                    writer.RenderEndTag(); //end UL
                }
                writer.RenderEndTag(); //end DIV
               
            //}
            if (!DesignMode)
            {
                manager.RegisterScriptDescriptors(this);
            }
        }
        
        private void RenderItem(DataItem choice, int index, HtmlTextWriter writer)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Li);
            _checkBox.ID = index.ToString(NumberFormatInfo.InvariantInfo);
            _checkBox.Text = choice.Name;
            _checkBox.Checked = choice.Selected;
            _checkBox.RenderControl(writer);
            writer.RenderEndTag();
        }

        protected override void TrackViewState()
        {
            base.TrackViewState();

            if (_choices != null)
            {
                ((IStateManager)_choices).TrackViewState();
            }
        }

        protected override object SaveViewState()
        {

            return new object[] { base.SaveViewState(), OptionChoices.SaveViewState(), StandardChoices.SaveViewState() };

        }

        protected override void LoadViewState(object savedState)
        {
            object baseState = null;
            object controlState1 = null;
            object controlState2 = null;

            object[] stateParts = savedState as object[];
            if (stateParts != null && stateParts.Length > 0)
            {
                baseState = stateParts[0];

                if (stateParts.Length >= 2)
                {
                    controlState1 = stateParts[1];
                }

                if (stateParts.Length >= 3)
                {
                    controlState2 = stateParts[2];
                }
            }

            StandardChoices.LoadViewState(controlState2);
            OptionChoices.LoadViewState(controlState1);
            base.LoadViewState(baseState);
        }
        public string ChildDivClientID
        {
            get
            {
                return ID + ClientIDSeparator + ChildDivID;   
            }
        }
        public IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            ScriptBehaviorDescriptor descriptor = new ScriptBehaviorDescriptor("AjaxControlToolkit.CollapsiblePanelBehavior", ChildDivClientID);
            descriptor.AddProperty("ClientStateFieldID", ID + ClientIDSeparator + _clientField.ID);
            descriptor.AddProperty("CollapseControlID", ID + ClientIDSeparator + _collapseControlID);
            descriptor.AddProperty("ExpandControlID", ID + ClientIDSeparator + _collapseControlID);
            descriptor.AddProperty("Collapsed", true);
            descriptor.AddProperty("CollapsedSize", 0);
            descriptor.AddProperty("id", ID);

            return new ScriptDescriptor[] { descriptor };
        }

        public IEnumerable<ScriptReference> GetScriptReferences()
        {

            return new ScriptReference[]
                {
                    new ScriptReference("AjaxControlToolkit.Compat.Timer.Timer.js", "AjaxControlToolkit"),
                    new ScriptReference("AjaxControlToolkit.Animation.Animations.js", "AjaxControlToolkit"),
                    new ScriptReference("AjaxControlToolkit.Common.Common.js", "AjaxControlToolkit"),
                    new ScriptReference("AjaxControlToolkit.ExtenderBase.BaseScripts.js", "AjaxControlToolkit"),
                    
                    new ScriptReference("AjaxControlToolkit.CollapsiblePanel.CollapsiblePanelBehavior.js", "AjaxControlToolkit")
                };
        }
    }
}
