using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;

namespace VehicleDataAccess
{
    public class CheckBoxPlus : CheckBox
    {

        public string CheckBoxArgument
        {
            get
            {
                    
                object o = ViewState["cba"];
                return (o == null) ? String.Empty : (string) o;
            }
            set { ViewState["cba"] = value; }
        }

        public string OnClickEvent
        {
            set
            {
                Attributes.Add("onclick",value);
            }
        }

        public string CheckedCssClass
        {
            get
            {

                object o = ViewState["ckCls"];
                return (o == null) ? String.Empty : (string)o;
            }
            set { ViewState["ckCls"] = value; }
        }
        public string UnCheckedCssClass
        {
            get
            {

                object o = ViewState["uCkCls"];
                return (o == null) ? String.Empty : (string)o;
            }
            set { ViewState["uCkCls"] = value; }
        }

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            if (Checked && !string.IsNullOrEmpty(CheckedCssClass))
            {
                CssClass = CheckedCssClass;
            }else if (!Checked && !string.IsNullOrEmpty(UnCheckedCssClass))
            {
                CssClass = UnCheckedCssClass;
            }
            base.Render(writer);
        }
        
    }
}
