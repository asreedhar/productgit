using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VehicleDataAccess
{

    public class MouseOverImage : System.Web.UI.WebControls.ImageButton
    {
        
        public event ImageClickEventHandler PopupClick;

        

        public string DisplayText
        {
            get
            {   object o = ViewState["dispText"];
                return (o == null) ? String.Empty : (string) o;
            }
            set { ViewState["dispText"] = value; }
        }
        
        private string popupHeight = "";

        public string PopupHeight
        {
            get { return popupHeight; }
            set { popupHeight = value; }
        }

        private string popupWidth = "";

        public string PopupWidth
        {
            get { return popupWidth; }
            set { popupWidth = value; }
        }

        private string popupCssClass = "";

        public string PopupCssClass
        {
            get { return popupCssClass; }
            set { popupCssClass = value; }
        }
        private string containerCssClass = "";

        public string ContainerCssClass
        {
            get { return containerCssClass; }
            set { containerCssClass = value; }
        }

        public ImageButton popupImage;

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            popupImage = new ImageButton();
            popupImage.ImageUrl = base.ImageUrl;
            popupImage.CommandArgument = this.CommandArgument;
            popupImage.Click += ClickHandler;
            if (!String.IsNullOrEmpty(this.popupWidth))
            {
                popupImage.Width = new Unit(this.popupWidth);
            }
            if (!String.IsNullOrEmpty(this.popupHeight))
            {
                popupImage.Height = new Unit(this.popupHeight);
            }

            string js_script_block_id = "mouseOverImage_showDivFunctionBlock";

            if (!this.Page.ClientScript.IsClientScriptBlockRegistered(js_script_block_id))
            {
            string js_script_block_code = @"
                    function mouseOverImage_showPopupDiv(showDivName) {
                        var el = document.getElementById(showDivName);
                        // if (typeof el != 'undefined')
                            
                       // for (i=0;i<el.length;i++)
                        //{ 
                          el.style.display = 'block';
                        //}
                    }
                    function mouseOverImage_hidePopupDiv(hideDivName) {
                        var el = document.getElementById(hideDivName);
                        // if (typeof el != 'undefined')
                            
                        //for (i=0;i<el.length;i++)
                        //{ 
                          el.style.display = 'none';
                        //}
                    }
                ";
            this.Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), js_script_block_id, js_script_block_code, true);
            }

        }
        protected void ClickHandler(object sender, ImageClickEventArgs e)
        {
            if (PopupClick != null)
            {
                this.PopupClick(sender, e);
            }
        }
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            
            writer.AddAttribute("onmouseover", "mouseOverImage_showPopupDiv('" + this.ClientID + "zoomDiv')");
            writer.AddAttribute("onmouseout", "mouseOverImage_hidePopupDiv('" + this.ClientID + "zoomDiv')");
            writer.AddAttribute("class", this.ContainerCssClass);
            writer.AddStyleAttribute("position","relative");
            writer.RenderBeginTag("div");

            popupImage.RenderControl(writer);

            writer.AddAttribute("class", this.popupCssClass);
            writer.AddStyleAttribute("display", "none");
            writer.AddAttribute("id", this.ClientID + "zoomDiv" );
            writer.RenderBeginTag("div");

            Label popupLabel = new Label();
            popupLabel.Text = this.DisplayText;
            popupLabel.RenderControl(writer);

            
            base.Render(writer);
            

            writer.WriteEndTag("div");
            writer.WriteEndTag("div");
        }
    
    }
}
