﻿using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.AdStatus;
using MAX.TimeToMarket.Repositories;
using Moq;
using NUnit.Framework;

namespace TimeToMarketTests
{
	[TestFixture]
	public class TimeToMarketRespositoryFactoryTests
	{
		[Test]
		public void Factory_Returns_V1()
		{
			var factory = new TimeToMarketRepositoryFactory(
				new Mock<IAdStatusRepository>().Object, 
				new Mock<IInventoryDataRepository>().Object,
				new Mock<IDashboardBusinessUnitRepository>().Object);

			var repo = factory.GetRepository(TimeToMarketRepositoryVersion.Version1);
			Assert.True(repo is TimeToMarketRepositoryDecorator);
		}

		[Test]
		public void Factory_Returns_V2()
		{
			var factory = new TimeToMarketRepositoryFactory(
				new Mock<IAdStatusRepository>().Object,
				new Mock<IInventoryDataRepository>().Object,
				new Mock<IDashboardBusinessUnitRepository>().Object);

			var repo = factory.GetRepository(TimeToMarketRepositoryVersion.Version2);
			Assert.True(repo is TimeToMarketV2Repository);
		}
	}
}