﻿using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket.Items;
using FirstLook.Merchandising.DomainModel.Templating;
using MAX.Entities;

namespace TimeToMarketTests.TestData
{
	internal class TimeToMarketTestData
	{
		#region TestData

		internal static IEnumerable<RowItem> GetTest_VinsToUpdate(int buid)
		{
			return new List<RowItem>
			{
				new RowItem
				{
					BusinessUnitId = buid,
					StockNumber = "StockNumber-Test",
					VIN = "1N4AL3AP1EN246354",
					InventoryId = 1,
					InventoryReceivedDate = new DateTime(1, 2, 1)
				}
			};
		}

		internal static IEnumerable<RowItem> GetTest_DuplicateVins(int buid)
		{
			return new List<RowItem>
			{
				new RowItem
				{
					BusinessUnitId = buid,
					StockNumber = "StockNumber-Test1",
					VIN = "1N4AL3AP1EN246354",
					InventoryId = 1,
					InventoryType = VehicleType.New,
					InventoryActive = true,
					InventoryReceivedDate = new DateTime(2014,06,18),
					DeleteDate = new DateTime(2014,06,30)
				},
				new RowItem
				{
					BusinessUnitId = buid,
					StockNumber = "StockNumber-Test2",
					VIN = "1N4AL3AP1EN246354",
					InventoryId = 2,
					InventoryActive = true,
					InventoryType = VehicleType.Used,
					InventoryReceivedDate = new DateTime(2014,08,01)
				}
			};
		}

		#endregion


	}
}
