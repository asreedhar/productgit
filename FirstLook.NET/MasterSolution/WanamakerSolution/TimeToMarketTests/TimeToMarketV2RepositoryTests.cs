﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.Enumerations;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.AdStatus;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket.Items;
using MAX.Entities;
using MAX.Entities.Enumerations;
using MAX.TimeToMarket.Repositories;
using Moq;
using NUnit.Framework;

namespace TimeToMarketTests
{
	[TestFixture]
	public class TimeToMarketV2RepositoryTests
	{
		Mock<ITimeToMarketRepository> _mockTimeToMarketRepository;
		Mock<IAdStatusRepository> _mockAdStatus;
		Mock<IDashboardBusinessUnitRepository> _mockDashboardBuRepo;

		[Test]
		public void SameVinTwiceAtOneDealerGetsCorrectGidData()
		{
			// The same vin was recently active at the same dealership twice.
			// make sure we only set the dates for the correct instance of that vin
			/*ITimeToMarketRepository internalTimeToMarketRepository, 
			IAdStatusRepository adStatusRepository, 
			IDashboardBusinessUnitRepository businessUnitRepository */

			var dataLoader = new TestDataLoader();

			var ttmRepo = new TestRepo(_mockTimeToMarketRepository.Object, _mockAdStatus.Object, _mockDashboardBuRepo.Object);

			var vehicles = TestData.TimeToMarketTestData.GetTest_DuplicateVins(1).ToArray();

			_mockAdStatus.Setup(x => x.GetAdStatusResults(It.IsAny<IAdStatusArgs>()))
				.Returns(dataLoader.FakeDuplicateAdStatusResults());

			ttmRepo.GetGidDataForVins(vehicles);

			foreach (var rowItem in vehicles)
			{
				var lastDate = rowItem.DeleteDate.HasValue ? rowItem.DeleteDate.Value : DateTime.UtcNow;
				var okaydates = new DateRange(rowItem.InventoryReceivedDate, lastDate);
				var results = ttmRepo.GetGidDataForVehicle(rowItem.VIN, okaydates, GidProvider.Aultec, ReportDataSource.Aultec);

				var before = results.ToJson();

				var all = results.Cast<MaxAnalyticsDates>().All(adresults => 
					   (adresults.FirstAdDate == null || (adresults.FirstAdDate >= okaydates.StartDate && adresults.FirstAdDate <= okaydates.EndDate)) 
					&& (adresults.FirstNonZeroPriceDate == null || (adresults.FirstNonZeroPriceDate >= okaydates.StartDate && adresults.FirstNonZeroPriceDate <= okaydates.EndDate)) 
					&& (adresults.FirstPhotoDate == null || (adresults.FirstPhotoDate >= okaydates.StartDate && adresults.FirstPhotoDate <= okaydates.EndDate)) 
					&& (adresults.FirstRequiredPhotoCountDate == null || (adresults.FirstRequiredPhotoCountDate >= okaydates.StartDate && adresults.FirstRequiredPhotoCountDate <= okaydates.EndDate)));
				
				Assert.IsTrue(all);
			}
		}

		[SetUp]
		public void SetupMocks()
		{
			_mockTimeToMarketRepository = new Mock<ITimeToMarketRepository>();
			_mockAdStatus = new Mock<IAdStatusRepository>();
			_mockDashboardBuRepo = new Mock<IDashboardBusinessUnitRepository>();
		}

		private class TestRepo : TimeToMarketV2Repository
		{
			private readonly TestDataLoader _testDataLoader;

			public TestRepo(ITimeToMarketRepository internalTimeToMarketRepository, IAdStatusRepository adStatusRepository, IDashboardBusinessUnitRepository businessUnitRepository) : base(internalTimeToMarketRepository, adStatusRepository, businessUnitRepository)
			{
				_testDataLoader = new TestDataLoader();
				GidProviders = new GidProviders {{VehicleType.New, GidProvider.Aultec}, {VehicleType.Used, GidProvider.Aultec}};
			}

			internal override AdStatusV2Args GetAdStatusTuples(IEnumerable<RowItem> @where)
			{
				return _testDataLoader.DuplicateAdStatusTuples();
			}
 		}

		[Test]
		public void TestDoNotChangeMaxDates()
		{
			var dataLoader = new TestDataLoader();

			var ttmRepo = new TestRepo(_mockTimeToMarketRepository.Object, _mockAdStatus.Object, _mockDashboardBuRepo.Object);

			var vehicles = TestData.TimeToMarketTestData.GetTest_DuplicateVins(1).ToArray();

			var originals = new List<RowItem>();
			originals.AddRange(vehicles.Select(x => x.Clone()));

			_mockAdStatus.Setup(x => x.GetAdStatusResults(It.IsAny<IAdStatusArgs>()))
				.Returns(dataLoader.FakeDuplicateAdStatusResults());

			//Sanity Check
			VerifyMatch(vehicles, originals);

			ttmRepo.GetGidDataForVins(vehicles);
			VerifyMatch(vehicles, originals);
			
			ttmRepo.FillDescriptionFirstDate(vehicles);
			VerifyMatch(vehicles, originals);

			ttmRepo.FillPhotosFirstDate(vehicles, vehicles.Min(vs => vs.InventoryReceivedDate));
			VerifyMatch(vehicles, originals);

			ttmRepo.FillMinimumPhotosFirstDate(vehicles, vehicles.Min(vs => vs.InventoryReceivedDate));
			VerifyMatch(vehicles, originals);

			ttmRepo.FillPriceFirstDate(vehicles);
			VerifyMatch(vehicles, originals);

			ttmRepo.FillCompleteAdFirstDate(vehicles);
			VerifyMatch(vehicles, originals);

			ttmRepo.FillGidFirstDate(vehicles);
			VerifyMatch(vehicles, originals);
		}

		private static void VerifyMatch(IEnumerable<RowItem> vehicles, List<RowItem> originals)
		{
			foreach (var rowItem in vehicles)
			{
				var originalCompare = originals.First(or => or.VIN == rowItem.VIN);
				Assert.AreEqual(originalCompare.MAXPhotoFirstDate, rowItem.MAXPhotoFirstDate);
				Assert.AreEqual(originalCompare.MAXMinimumPhotoFirstDate, rowItem.MAXMinimumPhotoFirstDate);
				Assert.AreEqual(originalCompare.MAXDescriptionFirstDate, rowItem.MAXDescriptionFirstDate);
				Assert.AreEqual(originalCompare.MAXPriceFirstDate, rowItem.MAXPriceFirstDate);
			}
		}
	}
}