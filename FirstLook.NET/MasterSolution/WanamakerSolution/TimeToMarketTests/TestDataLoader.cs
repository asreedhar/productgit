using System;
using System.Collections.Generic;
using System.IO;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.AdStatus;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket.Items;
using TimeToMarketTests.TestData;

namespace TimeToMarketTests
{
	public class TestDataLoader
	{
		internal IEnumerable<IAdStatusResult> FakeAdStatusResults()
		{
			const string file = "TimeToMarketTests.TestData.Aultec_AdStatusResults.json";
			string value = null;
			var assembly = typeof (TimeToMarketManagerTests).Assembly;
			using (var stream = assembly.GetManifestResourceStream(file))
				if (stream != null)
				{
					using (TextReader textReader = new StreamReader(stream))
						value = textReader.ReadToEnd();
				}

			List<AdStatusResultV2> res = null;
			if (!string.IsNullOrWhiteSpace(value))
				res = value.FromJson<List<AdStatusResultV2>>();

			return res;
		}

		internal IEnumerable<IAdStatusResult> FakeDuplicateAdStatusResults()
		{
			const string file = "TimeToMarketTests.TestData.Aultec_SameVinDifferentDates.json";
			string value = null;
			var assembly = typeof (TimeToMarketManagerTests).Assembly;
			using (var stream = assembly.GetManifestResourceStream(file))
				if (stream != null)
				{
					using (TextReader textReader = new StreamReader(stream))
						value = textReader.ReadToEnd();
				}

			List<AdStatusResultV2> res = null;
			if (!string.IsNullOrWhiteSpace(value))
				res = value.FromJson<List<AdStatusResultV2>>();

			return res;
		}

		internal IEnumerable<RowItem> FakeInventoryData()
		{
			return TimeToMarketTestData.GetTest_VinsToUpdate(1);
		}

		internal AdStatusV2Args DuplicateAdStatusTuples()
		{
			return new AdStatusV2Args
			{
				InventoryTuples = new List<AdStatusVehicleTuples>
				{
					new AdStatusVehicleTuples
					{
						EndDate = new DateTime(2014, 06, 30),
						StartDate = new DateTime(2014, 06, 18),
						Vin = "1N4AL3AP1EN246354"
					},
					new AdStatusVehicleTuples
					{
						EndDate = DateTime.UtcNow,
						StartDate = new DateTime(2014, 08, 01),
						Vin = "1N4AL3AP1EN246354"
					}
				},
				MinPhotoCount = 1,
				Store = "TestStore"
			};
		}
	}
}