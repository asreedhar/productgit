﻿using MAX.TimeToMarket;
using MAX.TimeToMarket.Queues;
using MAX.TimeToMarket.Tasks;
using Merchandising.Messages.TimeToMarket;
using Moq;
using NUnit.Framework;

namespace TimeToMarketTests
{
	[TestFixture]
	public class TimeToMarketTaskV2Test
	{
		[Test]
		public void TaskRunsBoth()
		{
			var mockQueueFactory = new Mock<ITimeToMarketQueueFactory>();
			var mockTimeToMarketManager = new Mock<ITimeToMarketManager>();
			var task = new TimeToMarketTaskV2(mockQueueFactory.Object, mockTimeToMarketManager.Object);
			var message = new TimeToMarketGenerationMessage(1);

			task.Process(message);

			mockTimeToMarketManager.Verify(x => x.FillAdStatusDates(It.IsAny<int>()), Times.Once);
			mockTimeToMarketManager.Verify(x => x.FillMaxDates(It.IsAny<int>()), Times.Once);
		}
	}
}