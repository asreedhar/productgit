﻿using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Enumerations;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket.Items;
using MAX.TimeToMarket;
using MAX.TimeToMarket.Repositories;
using Moq;
using NUnit.Framework;

namespace TimeToMarketTests
{
	[TestFixture]
	public class TimeToMarketManagerTests
	{
		private readonly TestDataLoader _testDataLoader = new TestDataLoader();
		private Mock<IReportStartDates> _mockReportStartDates;
		private Mock<ITimeToMarketRepository> _mockTimeToMarketRepository;
		private Mock<ITimeToMarketRepositoryFactory> _mockTimeToMarketRepositoryFactory;

		[Test]
		public void TestFillMaxDatesCallsEverythingItShould()
		{
			var manager = new TimeToMarketManager(
				_mockTimeToMarketRepositoryFactory.Object,
				_mockReportStartDates.Object
			);

			manager.FillMaxDates(1);

			_mockTimeToMarketRepository.Verify(repo => repo.Initialize(It.IsAny<int>(), It.IsAny<GidProviders>()), Times.Once);
			_mockTimeToMarketRepository.Verify(repo => repo.GetVinData(It.IsAny<int>(), It.IsAny<DateTime>()), Times.Once);
			
			_mockTimeToMarketRepository.Verify(repo => repo.FillPriceFirstDate(It.IsAny<IEnumerable<RowItem>>()), Times.Never);
			_mockTimeToMarketRepository.Verify(repo => repo.FillDescriptionFirstDate(It.IsAny<IEnumerable<RowItem>>()), Times.Never);
			_mockTimeToMarketRepository.Verify(repo => repo.FillPhotosFirstDate(It.IsAny<IEnumerable<RowItem>>(), It.IsAny<DateTime>()), Times.Never);
			_mockTimeToMarketRepository.Verify(repo => repo.FillMinimumPhotosFirstDate(It.IsAny<IEnumerable<RowItem>>(), It.IsAny<DateTime>()), Times.Never);
			
			_mockTimeToMarketRepository.Verify(repo => repo.FillCompleteAdFirstDate(It.IsAny<IEnumerable<RowItem>>()), Times.Never);
			_mockTimeToMarketRepository.Verify(repo => repo.FillGidFirstDate(It.IsAny<IEnumerable<RowItem>>()), Times.Never);
			
			_mockTimeToMarketRepository.Verify(repo => repo.UpdateVins(It.IsAny<IEnumerable<RowItem>>()), Times.Once);
			_mockTimeToMarketRepository.Verify(repo => repo.FillMAXPhotosFirstDate(It.IsAny<IEnumerable<RowItem>>(), It.IsAny<DateTime>()), Times.Once);
			_mockTimeToMarketRepository.Verify(repo => repo.FillMAXMinimumPhotosFirstDate(It.IsAny<IEnumerable<RowItem>>(), It.IsAny<DateTime>()), Times.Once);
			_mockTimeToMarketRepository.Verify(repo => repo.FillMAXPriceFirstDate(It.IsAny<IEnumerable<RowItem>>()), Times.Once);
			_mockTimeToMarketRepository.Verify(repo => repo.FillMAXDescriptionFirstDate(It.IsAny<IEnumerable<RowItem>>()), Times.Once);
		}

		[Test]
		public void TestFillAdStatusDatesCallsEverythingItShould()
		{
			var manager = new TimeToMarketManager(
				_mockTimeToMarketRepositoryFactory.Object,
				_mockReportStartDates.Object
			);

			manager.FillAdStatusDates(1);

			_mockTimeToMarketRepository.Verify(repo => repo.Initialize(It.IsAny<int>(), It.IsAny<GidProviders>()), Times.Once);
			_mockTimeToMarketRepository.Verify(repo => repo.GetVinData(It.IsAny<int>(), It.IsAny<DateTime>()), Times.Once);

			_mockTimeToMarketRepository.Verify(repo => repo.FillPriceFirstDate(It.IsAny<IEnumerable<RowItem>>()), Times.Once);
			_mockTimeToMarketRepository.Verify(repo => repo.FillDescriptionFirstDate(It.IsAny<IEnumerable<RowItem>>()), Times.Once);
			_mockTimeToMarketRepository.Verify(repo => repo.FillPhotosFirstDate(It.IsAny<IEnumerable<RowItem>>(), It.IsAny<DateTime>()), Times.Once);
			_mockTimeToMarketRepository.Verify(repo => repo.FillMinimumPhotosFirstDate(It.IsAny<IEnumerable<RowItem>>(), It.IsAny<DateTime>()), Times.Once);
			_mockTimeToMarketRepository.Verify(repo => repo.FillCompleteAdFirstDate(It.IsAny<IEnumerable<RowItem>>()), Times.Once);
			_mockTimeToMarketRepository.Verify(repo => repo.FillGidFirstDate(It.IsAny<IEnumerable<RowItem>>()), Times.Once);
			_mockTimeToMarketRepository.Verify(repo => repo.UpdateVins(It.IsAny<IEnumerable<RowItem>>()), Times.Once);

			_mockTimeToMarketRepository.Verify(repo => repo.FillMAXPhotosFirstDate(It.IsAny<IEnumerable<RowItem>>(), It.IsAny<DateTime>()), Times.Never);
			_mockTimeToMarketRepository.Verify(repo => repo.FillMAXMinimumPhotosFirstDate(It.IsAny<IEnumerable<RowItem>>(), It.IsAny<DateTime>()), Times.Never);
			_mockTimeToMarketRepository.Verify(repo => repo.FillMAXPriceFirstDate(It.IsAny<IEnumerable<RowItem>>()), Times.Never);
			_mockTimeToMarketRepository.Verify(repo => repo.FillMAXDescriptionFirstDate(It.IsAny<IEnumerable<RowItem>>()), Times.Never);
		}

		[SetUp]
		public void SetupMocks()
		{
			_mockReportStartDates = new Mock<IReportStartDates>();
			_mockTimeToMarketRepository = new Mock<ITimeToMarketRepository>();
			_mockTimeToMarketRepositoryFactory = new Mock<ITimeToMarketRepositoryFactory>();

			_mockReportStartDates.SetupGet(rsd => rsd.FirstPhotoStartDate).Returns(new DateTime(1, 1, 1));
			_mockReportStartDates.SetupGet(rsd => rsd.CompleteStartDate).Returns(new DateTime(1, 1, 2));
		    _mockReportStartDates.SetupGet(rsd => rsd.CarsApiStartDate).Returns(new DateTime(1, 1, 3));

			_mockTimeToMarketRepositoryFactory
				.Setup(x => x.GetRepository(It.IsAny<TimeToMarketRepositoryVersion>()))
				.Returns(_mockTimeToMarketRepository.Object);

			_mockTimeToMarketRepository
				.Setup(repo => repo.GetVinData(It.IsAny<int>(), It.IsAny<DateTime>()))
				.Returns(_testDataLoader.FakeInventoryData());

			_mockTimeToMarketRepository
				.Setup(repo => repo.GetGidDataForVins(It.IsAny<IEnumerable<RowItem>>()))
				.Returns(_testDataLoader.FakeAdStatusResults());
		}

		[TearDown]
		public void TearDown()
		{
			_mockReportStartDates = null;
			_mockTimeToMarketRepository = null;
			_mockTimeToMarketRepositoryFactory = null;
		}
	}
}