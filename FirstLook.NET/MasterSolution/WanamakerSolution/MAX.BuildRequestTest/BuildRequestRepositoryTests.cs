﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using MAX.BuildRequest;
using MAX.BuildRequest.Adaptors;
using MAX.BuildRequest.DataObjects;
using MAX.BuildRequest.VehicleDataProviders;
using Merchandising.Messages;
using Merchandising.Messages.AutoLoad;
using Merchandising.Messages.Slurpee;
using NUnit.Framework;


namespace MAX.BuildRequestTest
{
    [TestFixture]
    public class BuildRequestRepositoryTests
    {
        private BuildRequestRepository _buildRequestRepository;

        [SetUp]
        public void Setup()
        {
            _buildRequestRepository = new BuildRequestRepository();
        }

        /// <summary>
        /// Test DeterMine Agent to make sure the correct agent is returned by examining the XML
        /// </summary>
        /// <param name="oemSite">String found in the OEM_Site element of the connotate XML</param>
        /// <param name="agent">Agent that is returned</param>
        [TestCase("GM-VIS", Agent.GmGlobalConnect)]  // gm
        [TestCase("dealerspeed", Agent.DealerSpeed)] // bmw
        [TestCase("dealerspeed.net", Agent.DealerSpeed)] //bmw
        [TestCase("autopartners.net", Agent.GmGlobalConnect)] //gm
        [TestCase("dealerConnect", Agent.DealerConnect)] // chrysler
        [TestCase("vwHub.com", Agent.VolkswagenHub)] // volkswagon
        [TestCase("portal.mazdaUSA.com", Agent.MazdaDcs)] // mazda
        [TestCase("accessAudi.com", Agent.AccessAudi)] // audi
        [TestCase("webdcs.hyundaiDealer.com", Agent.HyundaiTechInfo)]  //hyundai
        [TestCase("dealerdaily.Toyota.com", Agent.NatDealerDaily)] // National Toyota
        [TestCase("dealerdaily.Lexus.com", Agent.NatDealerDaily)] // National Toyota
        [TestCase("pilota.setDealerDaily.com", Agent.SetDealerDaily)]  // SouthEast Toyota
        [TestCase("nnanet.com", Agent.NissanNorthAmerica)]  // SouthEast Toyota
        public void DetermineAgent(string oemSite, Agent agent)
        {

            // kinda cheating and using the Slurpee adapter to produce the Connotate format
            var slurpeeResult = new SlurpeeResultMessage
                {
                    Vehicle = new SlurpeeVehicle {site = oemSite}
                };

            var adapter = new BuildResultAdaptor();

            var xmlString = adapter.Translate(slurpeeResult);

            var resultAgent = _buildRequestRepository.DetermineAgent(xmlString);

            Assert.AreEqual(agent, resultAgent, string.Format("Cannot find the correct agent OEM_Site: {0}", oemSite));

        }


        [TestCase("HondaEconfig", Agent.HondaEconfig)]  // Honda Econfig
        [TestCase(null, Agent.Undefined)]  // null
        [TestCase("", Agent.Undefined)]  // null
        public void DetermineAgentJson(string dataSource, Agent agent)
        {

            var hondaEconfig = new HondaEconfig();

            hondaEconfig.Envelope = new BuildRequestEnvelope{DataSource = dataSource};

            var javaScriptSerializer = new JavaScriptSerializer();
            var jsonString = javaScriptSerializer.Serialize(hondaEconfig);
            
            var resultAgent = _buildRequestRepository.DetermineAgent(jsonString);

            Assert.AreEqual(agent, resultAgent, string.Format("Cannot find the correct agent for data source: {0}", dataSource));

        }


        /// <summary>
        /// this test the current vehicleDataProvider validity test which requests basically nothing to validate
        /// </summary>
        /// <param name="modelCode">option code sent</param>
        /// <param name="optionList">List of "|" delimited options</param>
        /// <param name="valid">result of IsVaid</param>
        [TestCase("CK13D2", "P102", true)]
        [TestCase("CK13D2", "", true)]
        [TestCase("", "P102", true)]
        [TestCase("", "", true)]
        public void IsValidBuildRequestTest(string modelCode, string optionList, bool valid)
        {
            // something
            // just easy to create a connotate xml like this
            var slurpeeResult = new SlurpeeResultMessage
            {
                Vehicle = new SlurpeeVehicle { site = "autopartners.net", vin = "test1234" }

            };

            slurpeeResult.Vehicle.options = new List<SlurpeeOption>();



            foreach (var option in optionList.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
            {
                slurpeeResult.Vehicle.options.Add(new SlurpeeOption { code = option });
            }

            slurpeeResult.Vehicle.model = modelCode;

            var adapter = new BuildResultAdaptor();

            var xmlString = adapter.Translate(slurpeeResult);

            var vehicleDataProvider = new SlurpeeXmlVehilceDataProvider
                {
                    ColorOptionCodeList = new List<FirstLook.Common.Core.Tuple<string, int>>(),
                    OptionWhiteList = new List<string>()
                };

            Assert.AreEqual(valid, vehicleDataProvider.IsValid(xmlString));
        }

        /// <summary>
        /// this test the current vehicleDataProvider validity test which requests basically nothing to validate
        /// </summary>
        /// <param name="modelCode">option code sent</param>
        /// <param name="optionList">List of "|" delimited options</param>
        /// <param name="valid">result of IsVaid</param>
        [TestCase("CK13D2", "P102", true)]
        [TestCase("CK13D2", "", false)]
        [TestCase("", "P102", false)]
        [TestCase("ZZUSV", "", true)]
        [TestCase("zzusv", "", true)]
        [TestCase("ZZUSV", "P102|PDI|Z28", true)]
        [TestCase("ZZUSV - 1970", "", true)]
        public void IsValidGmBuildRequestTest(string modelCode, string optionList, bool valid)
        {
            // just easy to create a connotate xml like this
            var slurpeeResult = new SlurpeeResultMessage
            {
                Vehicle = new SlurpeeVehicle { site = "autopartners.net", vin = "test1234" }

            };

            slurpeeResult.Vehicle.options = new List<SlurpeeOption>();

            

            foreach (var option in optionList.Split("|".ToCharArray() , StringSplitOptions.RemoveEmptyEntries))
            {
                slurpeeResult.Vehicle.options.Add(new SlurpeeOption { code = option });
            }

            slurpeeResult.Vehicle.model = modelCode;

            var adapter = new BuildResultAdaptor();

            var xmlString = adapter.Translate(slurpeeResult);

            // don't really need options or colors here
            var gmDataProvider = new GmGlobalConnectVehicleDataProvider
                {
                    ColorOptionCodeList = new List<FirstLook.Common.Core.Tuple<string, int>>(),
                    OptionWhiteList = new List<string>()
                };

            Assert.AreEqual(valid, gmDataProvider.IsValid(xmlString));
        }




    }
}

