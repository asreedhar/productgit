using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using FirstLook.LotIntegration.DomainModel;


namespace LotIntegrationWinForm
{
    public class FakeVehicleDataProvider : IVehicleDataProvider
    {
        public List<OptionPackage> GetPackages(string vin)
        {
            List<OptionPackage> retList = new List<OptionPackage>();
                    
            switch (vin)
            {
                case "WBSWD93519P361311":
                    retList = OptionPackage.CreateList("2MK,2MT,381,494,4MY,655,6FL,NCSW,ZPP", ',');
                    break;

            }
            return retList;
        }

        public int? GetStyleId(string vin)
        {
            switch (vin)
            {
                case "WBSWD93519P361311":
                    return 307910;
            }
            return -1;
        }

        public string GetExteriorColor(string vin)
        {
            switch (vin)
            {
                case "WBSWD93519P361311":
                    return "LE MANS BLUE METALLIC";
            }
            return string.Empty;
        }

        public string GetInteriorColor(string vin)
        {
            return string.Empty;
        }

        public string GetCredentialRequirementDescription()
        {
            throw new System.NotImplementedException();
        }

        public bool SupportsMake(string make)
        {
            return false;
        }

        public bool RequiresLogin()
        {
            return false;
        }

        public void SetCredential(NetworkCredential credential)
        {
            return;
        }

        public bool SupportsDealer(int dealerId)
        {
            return false;
        }
    }
}
