using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net;
using System.Text;
using System.Windows.Forms;
using FirstLook.LotIntegration.DomainModel;
using VehicleDataAccess;

namespace LotIntegrationWinForm
{
    public partial class VehicleBuildDataTest : Form
    {
        private VehicleDataProviderManager providerManager;
        private string winformName = "autoloaderWinform";
        public VehicleBuildDataTest()
        {
            InitializeComponent();
            providerManager = new VehicleDataProviderManager();
        }

        
        private void button1_Click(object sender, EventArgs e)
        {
            /*VehicleAutoLoader loader = new VehicleAutoLoader(new FakeVehicleDataProvider());

            List<int> buids = new List<int>(new int[] { 100148 });
            foreach (int buid in buids)
            {
                List<InventoryData> myInventory = InventoryData.FetchList(buid);
                foreach (InventoryData inv in myInventory)
                {
                    loader.LoadOntoVehicle(buid, inv.InventoryID, inv.VIN, "autoLoadWinForm");
                }
            }*/
            VehicleAutoLoader loader = new VehicleAutoLoader();
            
            int buid = int.Parse(dealerIdTB.Text);
            List<InventoryData> inventoryList = InventoryData.FetchList(buid);
            InventoryData myInventory = inventoryList.Find(delegate(InventoryData item)
                                                                               { return item.Make == makeCombo.Text && item.IsNew(); });
            if (myInventory != null)
            {


                //get the provider for this make
                IVehicleDataProvider dataProvider = providerManager.GetProvider(buid, myInventory.Make, new NetworkCredential(usernameTB.Text, passwordTB.Text));

                summaryTB.Text = CreateSummary(dataProvider, myInventory.VIN);
                /*
                    VehicleConfiguration currentVehicle = VehicleConfiguration.FetchOrCreateDetailed(buid, myInventory.InventoryID, winformName);
                    loader.LoadOntoVehicle(buid, myInventory,
                                       new NetworkCredential(usernameTB.Text, passwordTB.Text), winformName);
                 */
            }

        }
        private string CreateSummary(IVehicleDataProvider provider, string vin)
        {
            StringBuilder sb = new StringBuilder("Returned DataSummary for VIN: ");
            sb.Append(vin);
            sb.AppendLine("\n").Append("EXTERIOR: ").AppendLine(provider.GetExteriorColor(vin));
            sb.Append("INTERIOR: ").AppendLine(provider.GetInteriorColor(vin));
            sb.Append("StyleId: ").AppendLine(provider.GetStyleId(vin).GetValueOrDefault(-1).ToString());

            sb.Append("OptionCodes: ").AppendLine(GetOptionCodes(provider.GetPackages(vin)));
            return sb.ToString();
        }

        private string GetOptionCodes(List<OptionPackage> packages)
        {
            StringBuilder sb = new StringBuilder();
            foreach (OptionPackage pkg in packages)
            {
                sb.Append(pkg.OemCode).Append(",");
            }
            return sb.ToString().TrimEnd(",".ToCharArray());
        }
    }
}