﻿using System.Configuration;
using System.Data.SqlClient;

namespace LotIntegrationDomainModel.IntegrationTests.Core
{
    public class Database
    {
        public static SqlConnection GetOpenConnection(string connectionStringKey)
        {
            var cs = ConfigurationManager.ConnectionStrings[connectionStringKey].ConnectionString;
            var cn = new SqlConnection(cs);
            var success = false;
            try
            {
                cn.Open();
                success = true;
                return cn;
            }
            finally
            {
                if(!success)
                    cn.Close();
            }
        }
    }
}