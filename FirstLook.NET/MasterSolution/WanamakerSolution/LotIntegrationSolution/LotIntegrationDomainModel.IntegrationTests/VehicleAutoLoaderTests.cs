﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoad.FordNew.DataAccess;
using Autofac;
using Core.Messaging;
using FetchDotComClient.VehicleBuild;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.LotIntegration.DomainModel;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Chrome.Mapper;
using FirstLook.Merchandising.DomainModel.DataAccess;
using MAX.BuildRequest;
using Merchandising.Messages;
using Merchandising.Messages.AutoLoad;
using Moq;
using NUnit.Framework;
using ChromeStyle = FirstLook.Merchandising.DomainModel.Vehicles.ChromeStyle;
using OptionPackage = Merchandising.Messages.VehicleBuild.OptionPackage;
using VehicleBuildData = Merchandising.Messages.VehicleBuild.VehicleBuildData;

namespace LotIntegrationDomainModel.IntegrationTests
{
    /// <summary>
    /// These tests are here because the VehiclDataProviders are defined in LotIntegrationDomainModel.
    /// </summary>
    [TestFixture]
    public class VehicleAutoLoaderTests
    {
        private const string VIN1 = "WBAKB8C58BCY65872";
        private const int VINPattern1 = 109300;
        private const string VIN2 = "WBANB33554B088088";
        private const int VINPattern2 = 30669;
        private VehicleDataProviderManager _providerManager;
        
        private Mock<IChromeMapper> ChromeMapperMock;

        private IVehicleReferenceDataRepository vehicleReferenceDataRepository;
        private IAutoloadRepository autoloadRepository;
        private IAdMessageSender adMessageSender;



//        private int? GetTestBusinessUnit(string MAKE)
//        {
//            // I need a business unit that supports this make.
//            // Get the agent and find out how to join correctly to get the businessUnitId
//            Agent agent = VehicleDataProviderFactory.FetchAgent(MAKE);

//            var connection = Database.GetConnection("Merchandising");

//            connection.Open();

//            var cmd = connection.CreateCommand();

//            cmd.CommandType = CommandType.Text;

//            string cmdText = String.Format(@"
//                    select bu.BusinessUnitID, dls.destinationID, dsc.siteId
//                    from imt.dbo.BusinessUnit bu 
//                    join Merchandising.settings.DealerListingSites dls
//                    on bu.BusinessUnitID = dls.businessUnitID
//                    join Merchandising.settings.Dealer_SiteCredentials dsc
//                    on dls.businessUnitID = dsc.businessUnitId
//                    where bu.Active = 1 and dsc.isLockedOut = 0 AND dsc.siteId = {0}
//                ", (int)agent);

//            cmd.CommandText = cmdText;
//            var reader = cmd.ExecuteReader();

//            if (reader.Read())
//            {
//                var buid = reader.GetInt32(reader.GetOrdinal("BusinessUnitId"));
//                return buid;
//            }

//            return null;
//        }

        [TestFixtureSetUp]
        public void Setup()
        {
            // Mocks
            var mockSender = new Mock<IAdMessageSender>();
            var sender = mockSender.Object;
            adMessageSender = mockSender.Object;

            var mockAutoloadRepo = new Mock<IAutoloadRepository>();
            var autoloadrepo = mockAutoloadRepo.Object;
            autoloadRepository = mockAutoloadRepo.Object;

            var queueMock = new Mock<IQueueFactory>();
            var fetchAuditQueue = new Mock<IQueue<FetchAuditMessage>>();
            queueMock.Setup(x => x.CreateAutoLoadAudit()).Returns(fetchAuditQueue.Object);

            ChromeMapperMock = new Mock<IChromeMapper>();
            ChromeMapperMock.Setup(m => m.LookupVinPatternIdByVin(VIN1)).Returns(VINPattern1);
            ChromeMapperMock.Setup(m => m.LookupVinPatternIdByVin(VIN2)).Returns(VINPattern2);

			// Provide a filestore that always returns false for AutoLoadAudit.cs
	        var mockFileStoreFactory = new Mock<IFileStoreFactory>();
	        var mockFileStore = new Mock<IFileStorage>();
	        mockFileStore.Setup(mfs => mfs.ExistsNoStream(It.IsAny<string>())).Returns(false);
	        mockFileStoreFactory.Setup(x => x.CreateAutoLoadAudit()).Returns(mockFileStore.Object);

            var mockVehicleRefDataRepository = new Mock<IVehicleReferenceDataRepository>();
            vehicleReferenceDataRepository = mockVehicleRefDataRepository.Object;

            // registry
            var b = new ContainerBuilder();
            b.RegisterType<NullCache>().As<ICache>();
            b.RegisterInstance(sender).As<IAdMessageSender>();
            b.RegisterInstance(autoloadrepo).As<IAutoloadRepository>();
            b.RegisterInstance(queueMock.Object).As<IQueueFactory>();
            b.RegisterInstance(ChromeMapperMock.Object).As<IChromeMapper>();
	        b.RegisterInstance(mockFileStoreFactory.Object).As<IFileStoreFactory>();
            b.RegisterInstance(vehicleReferenceDataRepository).As<IVehicleReferenceDataRepository>();

            var container = b.Build();
            Registry.RegisterContainer(container);

            _providerManager = new VehicleDataProviderManager();
        }

        //[Ignore] // makes a fetch call, no longer have connectiont to connotate
        //[Test(Description = "Makes a Fetch call. Fails if the credentials are locked/Invalid")]
        //public void GetChromeStyleViaOptionCodeExamination()
        //{
        //    // This vin pattern matches to two style ids, both of which have the same oem style code.
        //    // it can only be matched to a chrome style id via option code examination.
        //    const string VIN = VIN2;
        //    const string MAKE = "BMW";
        //    int? BusinessUnitId = GetTestBusinessUnit(MAKE);
        //    const int InventoryType = 2;

        //    // get the provider for this make
        //    Assert.IsNotNull(BusinessUnitId);
        //    var inferredStyle = GetInferredStyle(_providerManager, BusinessUnitId.Value, MAKE, VIN, InventoryType);
        //    Assert.IsNotNull(inferredStyle);

        //    Console.WriteLine(@"Inferred styleId:" + inferredStyle.ChromeStyleID);
        //}

        //[Ignore] // makes a fetch call, no longer have connectiont to connotate
        //[Test(Description = "Makes a Fetch call. Fails if the credentials are locked/Invalid")] // FB 13591
        //public void GetChromeStyleViaOemStyleCode()
        //{
        //    // This vin pattern matches to two style ids, which have different oem style codes.
        //    // It can be matched to a chrome style id without examining any options.
        //    const string VIN = VIN1;
        //    const string MAKE = "BMW";
        //    const int InventoryType = 2;

        //    int? BusinessUnitId = GetTestBusinessUnit(MAKE);

        //    Assert.IsNotNull(BusinessUnitId);
            
        //    // get the provider for this make
        //    var inferredStyle = GetInferredStyle(_providerManager, BusinessUnitId.Value, MAKE, VIN, InventoryType);
        //    Assert.IsNotNull(inferredStyle);

        //    Console.WriteLine(@"Inferred styleId:" + inferredStyle.ChromeStyleID);
        //}

        //private IChromeStyle GetInferredStyle(VehicleDataProviderManager providerManager, int businessUnit, string make, string vin, int inventoryType)
        //{

        //    IVehicleDataProvider dataProvider = providerManager.GetProvider(businessUnit, make);
        //    var vehicleBuild = dataProvider.GetVehicleBuildData(vin, inventoryType, "test");

        //    Assert.IsNotNull(vehicleBuild);
        //    Console.WriteLine(@"MSRP: " + vehicleBuild.EstimatedMSRP);
        //    Console.WriteLine(@"Exterior Color: " + vehicleBuild.ExteriorColor);
        //    Console.WriteLine(@"Exterior Color Code: " + vehicleBuild.ExteriorColorCode);
        //    Console.WriteLine(@"Interior Color: " + vehicleBuild.InteriorColor);
        //    Console.WriteLine(@"Interior Color Code: " + vehicleBuild.InteriorColorCode);
        //    Console.WriteLine(@"OEM Model Code: " + vehicleBuild.OemModelCode);
        //    Console.WriteLine(@"Option Packages: " + vehicleBuild.OptionPackages.Select(i => i.OemCode.ToString()).ToList().ToDelimitedString(","));
        //    Console.WriteLine(@"Style Id: " + vehicleBuild.StyleId);
        //    Console.WriteLine(@"Trim: " + vehicleBuild.Trim);

        //    var sb = new StringBuilder();
        //    var loader = new VehicleAutoLoader(autoloadRepository, adMessageSender, vehicleReferenceDataRepository);
        //    var style = loader.GetInferredStyle(vin, sb, vehicleBuild);
        //    Console.WriteLine(sb.ToString());

        //    return style;
        //}

        [Test]
        public void GetColorOptionCodeList()
        {
            var list = VehicleAutoLoader.GetColorOptionCodeList(329033);
            Assert.IsNotNull(list);
            Assert.IsTrue(list.Count() > 0 );
        }

        [TestCase(Agent.GmGlobalConnect, "3GCUKREC7FG198115", "CK15543", "1SZ|2LT|RC4")]
        [TestCase(Agent.GmGlobalConnect, "1GB3KYC85FF528053", "CK36003", "1SZ|1WT|RC4|AVF")]
        public void GmDecodingTest(Agent agent, string vin, string oemModelCode, string optionList)
        {

            // mocks
            var autoLoadRepository = new AutoloadRepository();
            
            var mockSender = new Mock<IAdMessageSender>();
            var adSender = mockSender.Object;

            var mockVehicleReferenceDataRepository = new Mock<IVehicleReferenceDataRepository>();
            var vehicleReferenceDataRepo = mockVehicleReferenceDataRepository.Object;

            // create autoload manager
            var autoLoadManager = new AutoloadManager(autoLoadRepository);

            var buildRequestRepository = new BuildRequestRepository();
            var vehicleDataProvider = buildRequestRepository.GetVehicleDataProvider(agent);

            // get the vehicle autoloader for these attributes
            var vehicleAutoLoader = autoLoadManager.CreateAutoLoader(Agent.GmGlobalConnect, 1, adSender, vehicleReferenceDataRepo, "", vehicleDataProvider);

            var vehicleBuildData = new VehicleBuildData();
            vehicleBuildData.OemModelCode = oemModelCode;

            var options = optionList.Split('|');

            foreach (var option in options)
            {
                if(!string.IsNullOrWhiteSpace(option))
                    vehicleBuildData.UnfilteredOptionPackages.Add(new OptionPackage(option.Trim()));
            }



            var style = vehicleAutoLoader.GetInferredStyle(vin, new StringBuilder(), vehicleBuildData);


            Assert.AreNotEqual(style.ChromeStyleID, -1, string.Format("Could not decode style for VIN: {0}", vin));

        }

        [TestCase(Agent.DealerConnect, "1C6RR7KT9ES385977", "DS6L98", "26B|4BY|4KZP|WFPS|5ZLS|2TBA")]
        [TestCase(Agent.DealerConnect, "1C6RR7KT0ES385978", "DS6L98", "26B|DFD|EZH|AJY")]
        [TestCase(Agent.DealerConnect, "3C7WRMFL2EG111082", "DP5L94", "29A|DFD|EZH|AJY")]
        public void ChryslerDecodingTest(Agent agent, string vin, string oemModelCode, string optionList)
        {

            // mocks
            var autoLoadRepository = new AutoloadRepository();

            var mockSender = new Mock<IAdMessageSender>();
            var adSender = mockSender.Object;

            var mockVehicleReferenceDataRepository = new Mock<IVehicleReferenceDataRepository>();
            var vehicleReferenceDataRepo = mockVehicleReferenceDataRepository.Object;

            var vehicleResponse = new VehicleResponse();

            // create autoload manager
            var autoLoadManager = new AutoloadManager(autoLoadRepository);

            // create buildRequestRepo
            var buildRequestRepository = new BuildRequestRepository();
            var vehicleDataProvider = buildRequestRepository.GetVehicleDataProvider(agent);

            // get the vehicle autoloader for these attributes
            var vehicleAutoLoader = autoLoadManager.CreateAutoLoader(agent, 1, adSender, vehicleReferenceDataRepo, "", vehicleDataProvider);

            var vehicleBuildData = new VehicleBuildData();
            vehicleBuildData.OemModelCode = oemModelCode;

            var options = optionList.Split('|');

            foreach (var option in options)
            {
                if (!string.IsNullOrWhiteSpace(option))
                    vehicleBuildData.UnfilteredOptionPackages.Add(new OptionPackage(option.Trim()));
            }



            var style = vehicleAutoLoader.GetInferredStyle(vin, new StringBuilder(), vehicleBuildData);


            Assert.AreNotEqual(style.ChromeStyleID, -1, string.Format("Could not decode style for VIN: {0}", vin));

        }

        [TestCase(Agent.NatDealerDaily, "1NXBU40E89Z071447", "1838A", "CK|LE|CL|EM|FE|KE|CF|DK|-ADMIN")]
        [TestCase(Agent.NatDealerDaily, "1NXBU40E89Z071447", "1838A", "CK|LE|CL|EM|FE|KE|CF|DK")]
        [TestCase(Agent.NatDealerDaily, "JTDKN3DU7F1906075", "1227A", "FE|CF")]
        [TestCase(Agent.NatDealerDaily, "JTDKN3DU4B5315222", "1227K", "SY|FE|CF")]
        [TestCase(Agent.NatDealerDaily, "4T1BD1FK9FU149208", "2559A", "FE|CF")]
        [TestCase(Agent.NatDealerDaily, "JTDKN3DU4B5315222", "1227K", "SY|FE|CF")]
        public void ToyotaDecodingTest(Agent agent, string vin, string oemModelCode, string optionList)
        {

            // mocks
            var autoLoadRepository = new AutoloadRepository();

            var mockSender = new Mock<IAdMessageSender>();
            var adSender = mockSender.Object;

            var mockVehicleReferenceDataRepository = new Mock<IVehicleReferenceDataRepository>();
            var vehicleReferenceDataRepo = mockVehicleReferenceDataRepository.Object;

            var vehicleResponse = new VehicleResponse();

            // create autoload manager
            var autoLoadManager = new AutoloadManager(autoLoadRepository);

            // create buildRequestRepo
            var buildRequestRepository = new BuildRequestRepository();
            var vehicleDataProvider = buildRequestRepository.GetVehicleDataProvider(agent);

            // get the vehicle autoloader for these attributes
            var vehicleAutoLoader = autoLoadManager.CreateAutoLoader(agent, 1, adSender, vehicleReferenceDataRepo, "", vehicleDataProvider);

            var vehicleBuildData = new VehicleBuildData();
            vehicleBuildData.OemModelCode = oemModelCode;

            var options = optionList.Split('|');

            foreach (var option in options)
            {
                if (!string.IsNullOrWhiteSpace(option))
                    vehicleBuildData.UnfilteredOptionPackages.Add(new OptionPackage(option.Trim()));
            }



            var style = vehicleAutoLoader.GetInferredStyle(vin, new StringBuilder(), vehicleBuildData);


            Assert.AreNotEqual(style.ChromeStyleID, -1, string.Format("Could not decode style for VIN: {0}", vin));

        }


        [TestCase(373722, "")] // no options  ... make sure the standard loads (Mini)
        [TestCase(354353, "205|AXA|383")] // trans option in unfiltered and will make it through option filter (Mini)
        [TestCase(373721, "-STDTN|AXA|383")] // trans option in unfiltered and will not make it through option filter (Mini)
        [TestCase(337792, "")] // no options  ... make sure the standard loads (BMW)
        [TestCase(324449, "205")] // trans option in unfiltered and will not make it through option filter (BMW)
        public void BmwDynamicWhiteListTest(int styleId, string unfilteredOptions)
        {

            // create autoload manager
            var autoLoadManager = new AutoloadManager(null);

            var buildRequestRepository = new BuildRequestRepository();
            var vehicleDataProvider = buildRequestRepository.GetVehicleDataProvider(Agent.DealerSpeed);

            // get the vehicle autoloader for these attributes
            var vehicleAutoLoader = autoLoadManager.CreateAutoLoader(Agent.DealerSpeed, 1, null, null, "", vehicleDataProvider);

            var vehicleBuild = new VehicleBuildData();

            foreach (var option in unfilteredOptions.Split('|'))
            {
                if (!string.IsNullOrWhiteSpace(option))
                    vehicleBuild.UnfilteredOptionPackages.Add(new OptionPackage(option.Trim()));
            }



            vehicleAutoLoader.ApplyDynamicWhiteListOptions("", new ChromeStyle { StyleID = styleId }, vehicleBuild);

            // check to make sure there is a transmission in the options to be loaded
            var chromeRepository = new ChromeRepository();


            // Get all transmisstion options for this style
            var transmissionOptions = (from options in chromeRepository.GetOptions(new[] { styleId }).ToList() // just a call to chrome to get all options for this style
                                       where options.OptionKindId == 7 // option kind for transmission
                                       select options).ToList();

            var foundTransOptions = (from optionPackages in vehicleBuild.OptionPackages
                                    join transmissions in transmissionOptions on optionPackages.OemCode equals transmissions.OptionCode
                                    select optionPackages).ToList();


            Assert.GreaterOrEqual(foundTransOptions.Count, 1, "No transmission options will be loaded in BMW/Mini autoload");

            Console.WriteLine(string.Format("StyleID: {0} has the following transmission option(s): {1}", styleId, string.Join(",", foundTransOptions.Select(x => x.OemCode))));

        }

        [TestCase(Agent.NissanNorthAmerica, "C03|L92|N10|N92|G01|P01|B10|U02", 2)]
        [TestCase(Agent.NissanNorthAmerica, "C03|L92|N10|N92|G01|P01|B10|SGD", -1)]
        [TestCase(Agent.NissanNorthAmerica, "C03|L92|N10|N92|G01|P01|B10|U03", 3)]
        public void FindUniqueStyleTest(Agent agent, string packageList, int expectedResult)
        {

            var autoLoadManager = new AutoloadManager(null);

            var vehicleAutoLoader = autoLoadManager.CreateAutoLoader(agent, 1, null, null, "", null);


            var chromeOptions = new List<ChromeOption> 
                        { 
                            new ChromeOption { StyleId = 1, OptionCode = "50S", Pon = "[C03] 50 STATE EMISSIONS"},
                            new ChromeOption { StyleId = 2, OptionCode = "50S", Pon = "[C03] 50 STATE EMISSIONS"},
                            new ChromeOption { StyleId = 3, OptionCode = "50S", Pon = "[C03] 50 STATE EMISSIONS"},

                            new ChromeOption { StyleId = 1, OptionCode = "SGD", Pon = "[B10] SPLASH GUARDS (DISC)"},
                            new ChromeOption { StyleId = 2, OptionCode = "SGD", Pon = "[B10] SPLASH GUARDS (DISC)"},
                            new ChromeOption { StyleId = 3, OptionCode = "SGD", Pon = "[B10] SPLASH GUARDS (DISC)"},
                            
                            new ChromeOption { StyleId = 1, OptionCode = "TE1", Pon = "[U01] TECHNOLOGY PACKAGE"},
                            new ChromeOption { StyleId = 2, OptionCode = "TE2", Pon = "[U02] PREMIUM TECHNOLOGY PACKAGE -inc: Deletes compass in rearview mirror, Bluetooth Streaming Audio, Nissan Hard Drv Navigation Sys w/Voice Recognition, HVAC and DVD playback capacity, "},
                            new ChromeOption { StyleId = 3, OptionCode = "TE3", Pon = "[U03] SPORT TECHNOLOGY PACKAGE -inc: Deletes compass in rearview mirror, Bluetooth Streaming Audio, Nissan Hard Drv Navigation Sys w/Voice Recognition, HVAC and DVD playback capacity"},
                        };


            // load packages from vehicle
            var packageArray = packageList.Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            var packages = packageArray.Select(packageElement => new OptionPackage(packageElement)).ToList();

            var foundStyle = vehicleAutoLoader.FindUniqueStyle(chromeOptions, packages, "");

            Assert.AreEqual(expectedResult, foundStyle == null ? -1 : foundStyle.ChromeStyleID);



        }





    }

     
}
