﻿
using System;
using System.Data;
using System.Diagnostics;
using System.Collections.Generic;
using Autofac;
using Core.Messaging;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.LotIntegration.DomainModel;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Postings;
using MAX.ExternalCredentials;
using Merchandising.Messages;
using Moq;
using NUnit.Framework;

namespace LotIntegrationDomainModel.IntegrationTests
{
    [TestFixture]
    class CredentialTests
    {
        private const string EncryptKey = "test1234";

        private int? _buid;

        private int? BusinessUnitId
        {
            get
            {
                if (! _buid.HasValue) _buid = GetTestBusinessUnit();

                return _buid;
            }
        }

        private int? GetTestBusinessUnit()
        {
            var connection = Database.GetConnection("Merchandising");

            connection.Open();

            var cmd = connection.CreateCommand();

            cmd.CommandType = CommandType.Text;

            cmd.CommandText =
                @"
                    select bu.BusinessUnitID, dls.destinationID, dsc.siteId
                    from imt.dbo.BusinessUnit bu 
                    join Merchandising.settings.DealerListingSites dls
                    on bu.BusinessUnitID = dls.businessUnitID
                    join Merchandising.settings.Dealer_SiteCredentials dsc
                    on dls.businessUnitID = dsc.businessUnitId
                    where bu.Active = 1 and dsc.isLockedOut = 1
                ";

            var reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                var buid = reader.GetInt32(reader.GetOrdinal("BusinessUnitId"));
                return buid;
            }

            return null;
        }

        [TestFixtureSetUp]
        public void Setup()
        {

            var mockFileStoreFactory = new Mock<IFileStoreFactory>();

            var mockFileStorage = new Mock<IFileStorage>();

            var fileStorage = mockFileStorage.Object;

            mockFileStoreFactory.Setup(x => x.CreateFileStore()).Returns(fileStorage);

            var fileStoreFactory = mockFileStoreFactory.Object;

            fileStoreFactory.CreateFileStore();

            var mockIAdMessageSender = new Mock<IAdMessageSender>();
            var adMessageSender = mockIAdMessageSender.Object;

            var builder = new ContainerBuilder();
            builder.RegisterType<NullCache>().As<ICache>();
            builder.RegisterInstance(fileStoreFactory).As<IFileStoreFactory>();
            builder.RegisterInstance(adMessageSender).As<IAdMessageSender>();
            Registry.RegisterContainer(builder.Build());


            Debug.WriteLine("Testing with BusinessUnitId: " + BusinessUnitId);

        }

        [Test]
        public void GetListingSiteCredentials()
        {
            if (!BusinessUnitId.HasValue) return;

            var credentials = CredentialRepository.GetListingSiteCredentials(BusinessUnitId.Value);
            Assert.IsNotNull(credentials);
        }

        [Test]
        public void GetAllCredentials()
        {
            if (!BusinessUnitId.HasValue) return;

            var repository = new CredentialRepository();
            var credentials = repository.GetCredentials(BusinessUnitId.Value);

            Assert.IsNotNull(credentials);
        }


        [Test]
        public void SaveCredentials_Internal()
        {

            /*
             * 
    -- here is a test query you can use to check out what this test does
             * 
    declare @buid int
    set @buid = 100147 -- change this as needed

    select bu.BusinessUnitID, dls.destinationID, edt.description, dls.userName, dls.password
    from imt.dbo.BusinessUnit bu 
    join Merchandising.settings.DealerListingSites dls
    join Merchandising.settings.EdtDestinations edt on dls.destinationID = edt.destinationID
    on bu.BusinessUnitID = dls.businessUnitID
    where bu.Active = 1 and bu.BusinessUnitID = @buid

    select bu.BusinessUnitID, dsc.siteId, cs.siteName, dsc.username, dsc.encryptedPassword
    from imt.dbo.BusinessUnit bu 
    join Merchandising.settings.Dealer_SiteCredentials dsc
    on bu.businessUnitID = dsc.businessUnitId
    join Merchandising.settings.CredentialedSites cs on dsc.siteId = cs.siteId
    where bu.Active = 1 and bu.BusinessUnitID = @buid
             * 
             */
            if (!BusinessUnitId.HasValue) return;

            var repo = new CredentialRepository();

            var credentials = repo.GetCredentials(BusinessUnitId.Value);
                
            ListingSiteCredential lsc = null;
            OEMSiteCredential osc = null;

            foreach (var credential in credentials)
            {
                if (lsc == null && credential is ListingSiteCredential && Enum.IsDefined(typeof(EdtDestinations), credential.TypeId))
                    lsc = (ListingSiteCredential) credential;
                    
                if (osc == null && credential is OEMSiteCredential)
                    osc = (OEMSiteCredential) credential;

                if (osc != null && lsc != null) break;
            }

            if (osc == null || lsc == null) return;

            var newPassword1 = GetRandomString();
            var newPassword2 = GetRandomString();

            Debug.WriteLine(String.Format("Changing ListingSiteCredentail {0} password from {1} to {2}", lsc.Description, lsc.Password, newPassword1 ));



            Debug.WriteLine(String.Format("Changing OEMSiteCredential {0} password from {1} to (encrypted) {2}", osc.Description,
                                          osc.Password, EncryptionHelper.Encrypt(newPassword2, EncryptKey)));

            lsc.Password = newPassword1;
            osc.Password = newPassword2;

            repo.SaveCredential(BusinessUnitId.Value, (ICredential)lsc);
            repo.SaveCredential(BusinessUnitId.Value, (ICredential)osc);

            var updatedCreds = repo.GetCredentials(BusinessUnitId.Value);

            Assert.IsTrue(CredentialListContainsCredential(updatedCreds, lsc));
            Assert.IsTrue(CredentialListContainsCredential(updatedCreds, osc));
        }

        private static string GetRandomString()
        {
            var x = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            x = x.Replace("=", "");
            x = x.Replace("+", "");
            return x;
        }


        private static bool CredentialListContainsCredential(List<ICredential> list, OEMSiteCredential credential)
        {
            
            var x = list.Find(c =>
                              c is OEMSiteCredential
                              && ((OEMSiteCredential) c).TypeId == credential.TypeId
                              && c.UserName == credential.UserName && c.Password == credential.Password);

            return x != null;
        }

        private static bool CredentialListContainsCredential(List<ICredential> list, ListingSiteCredential credential)
        {
            var x = list.Find(c =>
                              c is ListingSiteCredential
                              && ((ListingSiteCredential)c).TypeId == credential.TypeId
                              && c.UserName == credential.UserName && c.Password == credential.Password);

            return x != null;
        }

    }
}
