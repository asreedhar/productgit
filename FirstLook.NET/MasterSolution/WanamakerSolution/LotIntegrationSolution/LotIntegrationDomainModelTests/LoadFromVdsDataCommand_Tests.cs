﻿using System;
using FirstLook.LotIntegration.DomainModel.Tasks;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using MAX.VDS;
using Merchandising.Messages;
using Moq;
using NUnit.Framework;

namespace LotIntegrationDomainModelTests
{
    [TestFixture]
    public class LoadFromVdsDataCommandTests
    {
        private Mock<IQueueFactory> _qFacMock;
        private Mock<InventoryData> _iDataMock;
        private LoadFromVdsDataCommand _command;
        private VdsAutoLoadConfigParser _vdsAutoLoadConfigParser;

        [TestFixtureSetUp]
        public void SetUp()
        {
            _qFacMock = new Mock<IQueueFactory>();
            _iDataMock = new Mock<InventoryData>();

            _vdsAutoLoadConfigParser = new VdsAutoLoadConfigParser();

            // we aren't actually going to run the command.
            _command = new LoadFromVdsDataCommand(_qFacMock.Object, _iDataMock.Object);            
        }

        [Test]
        public void IsEnabled_Scenario_Bmw_SpecificStores()
        {
            // scenario: bmw|123
            var configItems = new[]
            {
                new VdsAutoLoadConfigItem
                {
                    Makes = new[] {"bmw"},
                    Stores = new[] {"1"}
                },
                new VdsAutoLoadConfigItem
                {
                    Makes = new[] {"bmw"},
                    Stores = new[] {"2","3","4"}
                }

            };
            var strConfigItems = _vdsAutoLoadConfigParser.EncodeVdsConfigItems(configItems);
            Assert.IsTrue(_command.IsEnabled("bmw", 1, strConfigItems));
            Assert.IsTrue(_command.IsEnabled("bmw", 2, strConfigItems));
            Assert.IsTrue(_command.IsEnabled("bmw", 3, strConfigItems));
            Assert.IsTrue(_command.IsEnabled("bmw", 4, strConfigItems));
        }

        [Test]
        public void IsEnabled_Scenario_Bmw_AllStores()
        {
            // scenario: bmw|*
            var configItems = new[]
            {
                new VdsAutoLoadConfigItem
                {
                    Makes = new[] {"bmw"},
                    Stores = new[] {"*"}
                }
            };

            var r = new Random((int) DateTime.Now.Ticks);
            var strConfigItems = _vdsAutoLoadConfigParser.EncodeVdsConfigItems(configItems);
            Assert.IsTrue(_command.IsEnabled("bmw", r.Next(10000), strConfigItems));
            Assert.IsTrue(_command.IsEnabled("bmw", r.Next(10000), strConfigItems));
        }

        [Test]
        public void IsEnabled_Scenario_AllMakes_SpecificStores()
        {
            // scenario: *|123
            var configItems = new[]
            {
                new VdsAutoLoadConfigItem
                {
                    Makes = new[] {"*"},
                    Stores = new[] {"1"}
                },
                new VdsAutoLoadConfigItem
                {
                    Makes = new[] {"*"},
                    Stores = new[] {"2","3"}
                }

            };

            var strConfigItems = _vdsAutoLoadConfigParser.EncodeVdsConfigItems(configItems);
            Assert.IsTrue(_command.IsEnabled("foo", 1, strConfigItems));
            Assert.IsTrue(_command.IsEnabled("a", 2, strConfigItems));
            Assert.IsTrue(_command.IsEnabled("b", 3, strConfigItems));
        }

        [Test]
        public void IsEnabled_Sceanrio_AllMakes_AllStores()
        {
            // scenario: *|*
            var configItems = new[]
            {
                new VdsAutoLoadConfigItem
                {
                    Makes = new[] {"*"},
                    Stores = new[] {"*"}
                }
            };

            var r = new Random((int) DateTime.Now.Ticks);
            var strConfigItems = _vdsAutoLoadConfigParser.EncodeVdsConfigItems(configItems);
            Assert.IsTrue(_command.IsEnabled("a", r.Next(0, 10), strConfigItems));
            Assert.IsTrue(_command.IsEnabled("b", r.Next(11,100), strConfigItems));
            Assert.IsTrue(_command.IsEnabled("c", r.Next(101,1000), strConfigItems));   
        }

        [Test]
        public void IsEnabled_ManualConfigString()
        {
            const string STR_CONFIG_ITEMS = "a;1|b;*";
            var r = new Random((int)DateTime.Now.Ticks);
            Assert.IsTrue(_command.IsEnabled("a", 1, STR_CONFIG_ITEMS));
            Assert.IsTrue(_command.IsEnabled("b", r.Next(11, 100), STR_CONFIG_ITEMS));
            Assert.IsTrue(_command.IsEnabled("b", r.Next(101, 1000), STR_CONFIG_ITEMS));
        }
    }
}
