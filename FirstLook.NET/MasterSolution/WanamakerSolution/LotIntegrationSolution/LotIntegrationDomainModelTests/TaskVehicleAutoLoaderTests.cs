﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoLoad.FordNew.DataAccess;
using FetchDotComClient.VehicleBuild;
using FirstLook.LotIntegration.DomainModel;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.DataAccess;
using MAX.BuildRequest;
using Merchandising.Messages;
using Merchandising.Messages.VehicleBuild;
using Moq;
using NUnit.Framework;
using OptionPackage = Merchandising.Messages.VehicleBuild.OptionPackage;

namespace LotIntegrationDomainModelTests
{
    [TestFixture]
    public class TaskVehicleAutoLoaderTests
    {
        /// <summary>
        /// Testing the option matching logic to return correct results
        /// </summary>
        /// <param name="agent"></param>
        /// <param name="package"></param>
        /// <param name="optionList"></param>
        /// <param name="expectedResult"></param>
        [TestCase(Agent.Undefined, "101A", "101A-R|101A-F|644|001|102A|125", "101A-R")]
        [TestCase(Agent.Undefined, "102A", "102A-R|102A-F|644|001|102A|125", "102A")]
        [TestCase(Agent.DealerConnect, "24G", "102A-R|102A-F|644|001|102A|125|24G-B", "24G-B")]
        [TestCase(Agent.DealerConnect, "24G", "102A-R|102A-F|644|001|102A|125|24G-R|24G", "24G")]
        [TestCase(Agent.DealerConnect, "24G", "102A-R|102A-F|644|001|102A|125|24G-B|24G-R", "24G-R")]
        //[TestCase(Agent.Undefined, "24G", "102A-R|102A-F|644|001|102A|125|24G-B", "")]
        public void BaseDecoderOptionMatching(Agent agent, string package, string optionList, string expectedResult)
        {

            //// mocks
            //var autoLoadRepository = new AutoloadRepository();

            //var mockSender = new Mock<IAdMessageSender>();
            //var adSender = mockSender.Object;

            //var mockVehicleReferenceDataRepository = new Mock<IVehicleReferenceDataRepository>();
            //var vehicleReferenceDataRepo = mockVehicleReferenceDataRepository.Object;

            //var vehicleResponse = new VehicleResponse();

            //// create autoload manager
            var autoLoadManager = new AutoloadManager(null);

            //// create buildRequestRepo
            //var buildRequestRepository = new BuildRequestRepository();
            //var vehicleDataProvider = buildRequestRepository.GetVehicleDataProvider(agent);

            //// get the vehicle autoloader for these attributes
            var vehicleAutoLoader = autoLoadManager.CreateAutoLoader(agent, 1, null, null, "", null);

            //var vehicleAutoLoader = new VehicleAutoLoader(null, null, null, null);
            


            //// load options to test against
            var optionArray = optionList.Split(new[] {"|"}, StringSplitOptions.RemoveEmptyEntries);
            var options = optionArray.Select(optionElement => new ChromeOption {OptionCode = optionElement}).ToList();

            var optionResultList = vehicleAutoLoader.MatchOption(options, new OptionPackage(package));

            var count = (from resultList in optionResultList
                        where resultList.OptionCode == expectedResult
                        select resultList).Count();

            Assert.Greater(count, 0, String.Format("Expected option result: {0} was not found in match results", expectedResult));

            count = (from resultList in optionResultList
                         where resultList.OptionCode != expectedResult
                         select resultList).Count();

            Assert.AreEqual(count, 0, String.Format("Results that do not match expected option result: {0} were found in match results", expectedResult));

        }

        /// <summary>
        /// Test to make sure the filtering of option returns the correct results
        /// </summary>
        /// <param name="agent"></param>
        /// <param name="packageList"></param>
        /// <param name="optionList"></param>
        /// <param name="expectedResultList"></param>
        [TestCase(Agent.Undefined, "102A|GRA", "102A-R|102A-F|644|001|102A|125", "102A")]
        [TestCase(Agent.Undefined, "102A|GRA", "102A-R|102A-F|644|001|125", "102A-R")]
        [TestCase(Agent.Undefined, "102A|GRA", "102A-R|102A-F|GRA|001|101A|125", "102A-R|GRA")]
        [TestCase(Agent.Undefined, "102A|GRA|801", "102A-R|102A-F|GRA|001|101A|125", "102A-R|GRA")]
        [TestCase(Agent.Undefined, "102A|GRA|801", "102A-F|102A-R|GRA|GRA|001|101A|125", "102A-R|GRA")]
        //[TestCase(Agent.Undefined, "102A|GRA", "102A-R|102A-F|644|001|102A|125|101", "101")] // bad result for testing
        //[TestCase(Agent.Undefined, "102A|GRA", "102A-R|102A-F|644|001|102A|125|101|GRA", "102A")] // bad result for testing
        public void BaseDecoderOptionFilter(Agent agent, string packageList, string optionList, string expectedResultList)
        {

            // mocks
            //var autoLoadRepository = new AutoloadRepository();

            //var mockSender = new Mock<IAdMessageSender>();
            //var adSender = mockSender.Object;

            //var mockVehicleReferenceDataRepository = new Mock<IVehicleReferenceDataRepository>();
            //var vehicleReferenceDataRepo = mockVehicleReferenceDataRepository.Object;

            //var vehicleResponse = new VehicleResponse();

            //// create autoload manager
            //var autoLoadManager = new AutoloadManager(autoLoadRepository);

            //// create buildRequestRepo
            //var buildRequestRepository = new BuildRequestRepository();
            //var vehicleDataProvider = buildRequestRepository.GetVehicleDataProvider(agent);

            //// get the vehicle autoloader for these attributes
            //var vehicleAutoLoader = autoLoadManager.CreateAutoLoader(agent, 1, adSender, vehicleReferenceDataRepo, "", vehicleDataProvider);

            var vehicleAutoLoader = new VehicleAutoLoader(null, null, null, null);

            // load packages from vehicle
            var packageArray = packageList.Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            var packages = packageArray.Select(packageElement => new OptionPackage(packageElement)).ToList();

            // load options to test against
            var optionArray = optionList.Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            var options = optionArray.Select(optionElement => new ChromeOption { OptionCode = optionElement }).ToList();

            // load expected results
            var expectedOptions = expectedResultList.Split(new[] {"|"}, StringSplitOptions.RemoveEmptyEntries);

            var resultOptions = vehicleAutoLoader.FilterOptions(options, packages);


            // did all of the expected results get returned?
            var notInResult = (from expected in expectedOptions
                               where !resultOptions.Any(a => a.OptionCode == expected)
                               select expected).ToList();

            Assert.AreEqual(0, notInResult.Count, string.Format("Option Filtering did not return the experted option(s): {0}", string.Join(",", notInResult)));

            // anything in the option list that we did not expect ?
            var badResult = (from result in resultOptions
                                where !expectedOptions.Any(a => a == result.OptionCode)
                               select result.OptionCode).ToList();

            Assert.AreEqual(0, badResult.Count, string.Format("Option Filtering returned option(s) we did not expect: {0}", string.Join(",", badResult)));


            var goodResult = (from expected in expectedOptions
                             where resultOptions.Any(a => a.OptionCode == expected)
                             select expected).ToList();

            Assert.AreEqual(goodResult.Count, expectedOptions.Count(), "Option filter did not result the expected number of options");

        }

        
        
        /// <summary>
        /// Special code for matching Nissan options on Alternate key contained in the PON of the chrome option
        /// </summary>
        /// <param name="agent"></param>
        /// <param name="packageCode"></param>
        /// <param name="expectedOptionCodes"></param>
        [TestCase(Agent.NissanNorthAmerica, "C03", "50S")] // find on alternate key
        [TestCase(Agent.NissanNorthAmerica, "B10", "SG1")] // find on alternate key
        [TestCase(Agent.NissanNorthAmerica, "T01", "TOW-A")] // find on alternate key
        [TestCase(Agent.NissanNorthAmerica, "blah", "")] // find nothing
        [TestCase(Agent.NissanNorthAmerica, "DVD", "DVD")] // find on regular option code
        [TestCase(Agent.NissanNorthAmerica, "P02", "PR2")] // find on regular option code
        //[TestCase(Agent.NissanNorthAmerica, "DVD2", "blah")] // make it blow up
        public void NissanDecoderOptionMatching(Agent agent, string packageCode, string expectedOptionCodes)
        {

            var autoLoadManager = new AutoloadManager(null);

            var vehicleAutoLoader = autoLoadManager.CreateAutoLoader(agent, 1, null, null, "", null);


            var chromeOptions = new List<ChromeOption> 
                        { 
                            new ChromeOption { OptionCode = "50S", Pon = "[C03] 50 STATE EMISSIONS" },
                            new ChromeOption { OptionCode = "-Z66", Pon = "[Z66] ACTIVATION DISCLAIMER (PIO) " },
                            new ChromeOption { OptionCode = "BAR", Pon = "[B93] CROSS BARS (PIO) " },
                            new ChromeOption { OptionCode = "BUM", Pon = "[B94] REAR BUMPER PROTECTOR (PIO) " },
                            new ChromeOption { OptionCode = "CAR", Pon = "[M92] CARGO PACKAGE -inc: rear cargo protector and under-floor storage dividers, Tonneau Cover, First Aid Kit (PIO)" },
                            new ChromeOption { OptionCode = "DVD", Pon = "[H10] DUAL DVD ENTERTAINMENT SYSTEM (PIO) " },
                            new ChromeOption { OptionCode = "FLO", Pon = "[L92] CARPETED FLOOR MAT (4-PC SET) -inc: 1st, 2nd, and 3rd rows (PIO) " },
                            new ChromeOption { OptionCode = "IKP", Pon = "[N10] ILLUMINATED KICK PLATES (PIO) " },
                            new ChromeOption { OptionCode = "SG1", Pon = "[B10] BLACK SPLASH GUARDS 4-PIECE N/A dealer order; required on vehicles sold in AK, ID, MT, OR, RI, WA, WI. " },
                            new ChromeOption { OptionCode = "SGD", Pon = "[B92] BLACK SPLASH GUARDS (SET OF 4) (PIO) Automatically added to dealer orders in the following states if not ordered with factory installed splash guards or factory installed splash guards are not offered as an orderable option: Alaska, Idaho, Montana, Oregon, Rhode Island, Washington, and Wisconsin. " },
                            new ChromeOption { OptionCode = "TOW-A", Pon = "[T01] TRAILER TOW PACKAGE -inc: finisher, Tow Hitch Receiver, 7-Pin Trailer Wiring Harness " },
                            new ChromeOption { OptionCode = "-Z98", Pon = "[Z98] NISSAN SHIP THROUGH FEE -inc: RDR Type 2 (PIO) Minimum quantity of 10. " },
                            new ChromeOption { OptionCode = "-PAINT", Pon = "STANDARD PAINT " },
                            new ChromeOption { OptionCode = "G-0", Pon = "CHARCOAL, CLOTH SEAT TRIM " },
                            new ChromeOption { OptionCode = "H-0", Pon = "ALMOND, CLOTH SEAT TRIM " },
                            new ChromeOption { OptionCode = "QAA", Pon = "MOONLIGHT WHITE" },
                            new ChromeOption {OptionCode = "PR2", Pon = "(P02) PREMIUM PKG"},
                            new ChromeOption { OptionCode = "NAH", Pon = "CAYENNE RED" }
                        };


            var package = new OptionPackage(packageCode);

            var foundOption = vehicleAutoLoader.MatchOption(chromeOptions, package);

            var foundCodes =  string.Join(",", foundOption.Select(x => x.OptionCode));

            Assert.AreEqual(expectedOptionCodes, foundCodes, "The Nissan match option method did not match the expected results");


        }

        [TestCase(Agent.NatDealerDaily, "05B2", "5B2")] 
        [TestCase(Agent.SetDealerDaily, "0001G3", "1G3")] 
       // [TestCase(Agent.NatDealerDaily, "08W6", "8W6")] // find on alternate key  -- This test fails until the code is changed so I have commented it out to preserve the build
       // [TestCase(Agent.NatDealerDaily, "08W6", "8W6")] // find on alternate key  -- This test fails until the code is changed so I have commented it out to preserve the build
       //  [TestCase(Agent.NatDealerDaily, "8W6", "8W6")] // test the test...
       // [TestCase(Agent.SetDealerDaily, "8W6", "8W6")] // test the test...
        public void ToyotaDecoderColorOptionMatching(Agent agent, string packageCode, string expectedOptionCodes)
        {
            var autoLoadManager = new AutoloadManager(null);

            var vehicleAutoLoader = autoLoadManager.CreateAutoLoader(agent, 1, null, null, "", null);

            //SELECT OptionCode, PON FROM VehicleCatalog.Chrome.Options WHERE StyleID = 363242
            var chromeOptions = new List<ChromeOption>
            {
                new ChromeOption {OptionCode = "-PAINT", Pon = "STANDARD PAINT"},
                new ChromeOption {OptionCode = "070", Pon = "BLIZZARD PEARL"},
                new ChromeOption {OptionCode = "1F7", Pon = "CLASSIC SILVER METALLIC"},
                new ChromeOption {OptionCode = "1G3", Pon = "MAGNETIC GRAY METALLIC"},
                new ChromeOption {OptionCode = "218", Pon = "ATTITUDE BLACK"},
                new ChromeOption {OptionCode = "3R0", Pon = "SIZZLING CRIMSON MICA"},
                new ChromeOption {OptionCode = "3T0", Pon = "OOH LA LA ROUGE MICA"},
                new ChromeOption {OptionCode = "5B2", Pon = "CREME BRULEE MICA"},
                new ChromeOption {OptionCode = "6T7", Pon = "CYPRESS PEARL"},
                new ChromeOption {OptionCode = "8W6", Pon = "PARISIAN NIGHT PEARL"},
                new ChromeOption {OptionCode = "BD", Pon = "BLIND SPOT MONITOR W/REAR CROSS TRAFFIC ALERT"},
                new ChromeOption {OptionCode = "FE-0", Pon = "50 STATE EMISSIONS"},
                new ChromeOption {OptionCode = "LT03", Pon = "ALMOND, LEATHER SEAT TRIM"},
                new ChromeOption {OptionCode = "LT17", Pon = "LIGHT GRAY, LEATHER SEAT TRIM"},
                new ChromeOption {OptionCode = "LT20", Pon = "BLACK, LEATHER SEAT TRIM"},
                new ChromeOption {OptionCode = "PC-0", Pon = "SPECIAL COLOR - BLIZZARD PEARL"}
            };


            var package = new OptionPackage(packageCode);

            var foundOption = vehicleAutoLoader.MatchOption(chromeOptions, package);

            var foundCodes = string.Join(",", foundOption.Select(x => x.OptionCode));

            Assert.AreEqual(expectedOptionCodes, foundCodes, "The Toyota match option method did not match the expected results");
        }



    }


}
