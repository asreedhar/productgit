using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using FirstLook.LotIntegration.DomainModel;
using FirstLook.Merchandising.DomainModel;
using MAX.ExternalCredentials;
using NUnit.Framework;

namespace LotIntegrationDomainModelTests
{
    [TestFixture()]
    public class EncryptionTests
    {
        [Test]
        public void Can_Encrypt_Data()
        {
            string txtToEncrypt = "mypassword";
            string encrypted = EncryptionHelper.Encrypt(txtToEncrypt, "test1234");
            Assert.AreNotEqual(encrypted, txtToEncrypt);

            string decrypted = EncryptionHelper.Decrypt(encrypted, "test1234");
            Assert.AreEqual(decrypted, txtToEncrypt);
        }

        [TestCase("hambone@1")]
        [TestCase("andy1234")]
        [TestCase("Andy1234")]
        [TestCase("Determined1")]
        public void GetEncryptedPasswordForOEM(string password)
	    {
			// This is just a test I use to quickly get the encrypted value to use to do a mass-update
			Debug.WriteLine(string.Format("Encrypted \"{0}\" as: {1}", password, EncryptionHelper.Encrypt(password, "test1234")));
	    }

		[Test]
		public void GetDecryptedPasswordForOEM()
		{
			// This is just a test I use to easily decrypt value stored
			var password = "jKCTaumBJVPkmeDxiOanYw==";
			Debug.WriteLine(string.Format("Decrypted \"{0}\" as: {1}", password, EncryptionHelper.Decrypt(password, "test1234")));
		}


	    private string username = "testUN";
        private string password = "testPW";
        private int buid = 100150;
       
    }
}
