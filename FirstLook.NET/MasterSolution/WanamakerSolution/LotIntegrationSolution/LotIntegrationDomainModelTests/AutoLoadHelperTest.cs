﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Autofac;
using FirstLook.Common.Core.IOC;
using FirstLook.LotIntegration.DomainModel;
using FirstLook.LotIntegration.DomainModel.Tasks;
using FirstLook.Merchandising.DomainModel;
using Merchandising.Messages;
using Merchandising.Messages.AutoLoad;
using Moq;
using NUnit.Framework;

namespace LotIntegrationDomainModelTests
{
    [TestFixture]
    class AutoLoadHelperTest
    {

        [TestFixtureSetUp]
        public void Setup()
        {
            // Mocks
            var mockSender = new Mock<IAdMessageSender>();
            var sender = mockSender.Object;

            var mockAutoloadRepo = new Mock<IAutoloadRepository>();
            var autoloadrepo = mockAutoloadRepo.Object;

            // registry
            var b = new ContainerBuilder();
            b.RegisterInstance(sender).As<IAdMessageSender>();
            b.RegisterInstance(autoloadrepo).As<IAutoloadRepository>();

            var container = b.Build();
            Registry.RegisterContainer(container);

        }


        /* 
          Very simple test to make sure the correct Vehicle auto loader is created for fetch autoload processing
         */
        //[TestCase(Agent.MazdaDcs, "MazdaVehicleAutoLoader", 1)]
        //[TestCase(Agent.FordConnect, "FordNewVehicleAutoLoader", 1)]
        //[TestCase(Agent.FordConnect, "FordUsedVehicleAutoLoader", 2)]
        //[TestCase(Agent.DealerConnect, "TaskVehicleAutoLoader", 1)]
        //[TestCase(Agent.DealerSpeed, "TaskVehicleAutoLoader", 1)]
        //[TestCase(Agent.GmGlobalConnect, "TaskVehicleAutoLoader", 1)]
        //[TestCase(Agent.ToyotaTechinfo, "TaskVehicleAutoLoader", 1)]
        //[TestCase(Agent.VolkswagenHub, "TaskVehicleAutoLoader", 1)]
        //public void TestCreateAutoLoader(Agent agent, string className, int inventoryType)
        //{
        //    // fake document, is not important at this point
        //    XmlDocument xmlDocument = new XmlDocument();
        //    xmlDocument.AppendChild(xmlDocument.CreateElement("AgentExecution"));
            
        //    TaskVehicleAutoLoader loader = AutoLoadHelper.CreateAutoLoader(agent, inventoryType, xmlDocument);

        //    // test the class that is created
        //    Assert.AreEqual(loader.GetType().Name, className, "Inncorrect Vehicle Autoload class was created!!");

        //}

    }
}
