﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoLoad.FordNew.DataAccess;
using FirstLook.LotIntegration.DomainModel;
using Merchandising.Messages.VehicleBuild;
using NUnit.Framework;

namespace LotIntegrationDomainModelTests
{
    [TestFixture]
    public class SanitizedOptionsTest
    {
        private List<ChromeOption> chromeOptions;

        [TestFixtureSetUp]
        public void Setup()
        {
            chromeOptions = new List<ChromeOption>
                {
                    new ChromeOption {OptionCode = "-PAINT", Pon = "STANDARD PAINT ", Msrp = 0.00m} ,
                    new ChromeOption {OptionCode = "077", Pon = "STARFIRE PEARL ", Msrp = 0.00m} ,
                    new ChromeOption {OptionCode = "1H9", Pon = "NEBULA GRAY PEARL ", Msrp = 0.00m} ,
                    new ChromeOption {OptionCode = "1J4", Pon = "SILVER LINING METALLIC ", Msrp = 0.00m} ,
                    new ChromeOption {OptionCode = "212", Pon = "OBSIDIAN ", Msrp = 0.00m} ,
                    new ChromeOption {OptionCode = "217", Pon = "STARGAZER BLACK ", Msrp = 0.00m} ,
                    new ChromeOption {OptionCode = "3P", Pon = "PAINT PROTECTION FILM ", Msrp = 429.00m} ,
                    new ChromeOption {OptionCode = "3S0", Pon = "CLARET MICA ", Msrp = 0.00m} ,
                    new ChromeOption {OptionCode = "3T", Pon = "CROSS BARS ", Msrp = 259.00m} ,
                    new ChromeOption {OptionCode = "4U7", Pon = "SATIN CASHMERE METALLIC ", Msrp = 0.00m} ,
                    new ChromeOption {OptionCode = "4V3", Pon = "FIRE AGATE ", Msrp = 0.00m} ,
                    new ChromeOption {OptionCode = "8V3", Pon = "DEEP SEA MICA ", Msrp = 0.00m} ,
                    new ChromeOption {OptionCode = "A6", Pon = "ASHTRAY ", Msrp = 26.00m} ,
                    new ChromeOption {OptionCode = "C1", Pon = "CARGO MAT ", Msrp = 105.00m} ,
                    new ChromeOption {OptionCode = "C6", Pon = "CARGO SPIDER NET ", Msrp = 49.00m} ,
                    new ChromeOption {OptionCode = "CP", Pon = "COMFORT PKG ", Msrp = 1340.00m} ,
                    new ChromeOption {OptionCode = "D5", Pon = "DOOR EDGE GUARDS ", Msrp = 115.00m} ,
                    new ChromeOption {OptionCode = "DA", Pon = "LEXUS DISPLAY AUDIO PKG ", Msrp = 860.00m} ,
                    new ChromeOption {OptionCode = "DH", Pon = "TOWING HITCH RECEIVER W/BALL MOUNT ", Msrp = 650.00m} ,
                    new ChromeOption {OptionCode = "EK", Pon = "PREMIUM AUDIO SYSTEM ", Msrp = 0.00m} ,
                    new ChromeOption {OptionCode = "EY", Pon = "DUAL-SCREEN REAR SEAT ENTERTAINMENT SYSTEM W/NAVIGATION ", Msrp = 4920.00m} ,
                    new ChromeOption {OptionCode = "FA10", Pon = "LIGHT GRAY, CLOTH SEAT TRIM ", Msrp = 0.00m} ,
                    new ChromeOption {OptionCode = "FT", Pon = "19' X 7.5' TRIPLE SPLIT 5-SPOKE ALLOY WHEELS ", Msrp = 770.00m} ,
                    new ChromeOption {OptionCode = "GF", Pon = "HEADS UP DISPLAY ", Msrp = 1200.00m} ,
                    new ChromeOption {OptionCode = "GN", Pon = "CARGO NET ", Msrp = 59.00m} ,
                    new ChromeOption {OptionCode = "HL", Pon = "XENON HIGH INTENSITY DISCHARGE (HID) HEADLAMPS ", Msrp = 515.00m} ,
                    new ChromeOption {OptionCode = "HS", Pon = "HEATED & VENTILATED FRONT SEATS ", Msrp = 640.00m} ,
                    new ChromeOption {OptionCode = "JC", Pon = "120V AC PWR OUTLET ", Msrp = 100.00m} ,
                    new ChromeOption {OptionCode = "LA00", Pon = "PARCHMENT, SEMI-ANILINE LEATHER SEAT TRIM ", Msrp = 0.00m} ,
                    new ChromeOption {OptionCode = "LA10", Pon = "LIGHT GRAY, SEMI-ANILINE LEATHER SEAT TRIM ", Msrp = 0.00m} ,
                    new ChromeOption {OptionCode = "LA20", Pon = "BLACK, SEMI-ANILINE LEATHER SEAT TRIM ", Msrp = 0.00m} ,
                    new ChromeOption {OptionCode = "LA40", Pon = "SADDLE TAN, SEMI-ANILINE LEATHER SEAT TRIM ", Msrp = 0.00m} ,
                    new ChromeOption {OptionCode = "LC00", Pon = "PARCHMENT, PERFORATED LEATHER SEAT TRIM ", Msrp = 0.00m} ,
                    new ChromeOption {OptionCode = "LC10", Pon = "LIGHT GRAY, PERFORATED LEATHER SEAT TRIM ", Msrp = 0.00m} ,
                    new ChromeOption {OptionCode = "LC20", Pon = "BLACK, PERFORATED LEATHER SEAT TRIM ", Msrp = 0.00m} ,
                    new ChromeOption {OptionCode = "LC40", Pon = "SADDLE TAN, PERFORATED LEATHER SEAT TRIM ", Msrp = 0.00m} ,
                    new ChromeOption {OptionCode = "LD", Pon = "LUXURY PKG W/BLIND SPOT MONITOR SYSTEM ", Msrp = 5020.00m} ,
                    new ChromeOption {OptionCode = "LL", Pon = "LUXURY PKG ", Msrp = 4520.00m} ,
                    new ChromeOption {OptionCode = "ML", Pon = "MARK LEVINSON AUDIO ", Msrp = 995.00m} ,
                    new ChromeOption {OptionCode = "NV", Pon = "NAVIGATION PKG ", Msrp = 2775.00m} ,
                    new ChromeOption {OptionCode = "PA", Pon = "INTUITIVE PARKING ASSIST ", Msrp = 500.00m} ,
                    new ChromeOption {OptionCode = "PD", Pon = "PREMIUM PKG W/BLIND SPOT MONITOR SYSTEM ", Msrp = 2760.00m} ,
                    new ChromeOption {OptionCode = "PM", Pon = "PREMIUM PKG ", Msrp = 2260.00m} ,
                    new ChromeOption {OptionCode = "PS", Pon = "PRE-COLLISION SYSTEM ", Msrp = 1500.00m} ,
                    new ChromeOption {OptionCode = "TO", Pon = "TOWING PREP PKG ", Msrp = 245.00m} ,
                    new ChromeOption {OptionCode = "V2", Pon = "GLASS BREAKAGE SENSORS ", Msrp = 329.00m} ,
                    new ChromeOption {OptionCode = "V4", Pon = "REMOTE ENGINE START ", Msrp = 499.00m} ,
                    new ChromeOption {OptionCode = "WL", Pon = "WHEEL LOCKS ", Msrp = 81.00m} ,
                    new ChromeOption {OptionCode = "WU", Pon = "WOOD/LEATHER-WRAPPED STEERING WHEEL & SHIFT KNOB ", Msrp = 330.00m} ,
                    new ChromeOption {OptionCode = "Z1", Pon = "PREFERRED ACCESSORY PKG ", Msrp = 255.00m} ,                
                };
        }


        [TestCase("ZL1|NV|PM|CP|HS|WU|EK|C1|Z1", "EK|NV", 199, "package|collection|group|pack|pkg", "wheel lock|cargo|floot mat|PREFERRED ACCESSORY", 6)]
        [TestCase("WL|C6", "EK|NV", 25, "package|collection|group|pack|pkg", "wheel lock|cargo|floot mat|PREFERRED ACCESSORY", 0)]
        [TestCase("WL|C6|DA|V2|ML|4V3", "", -1000000, "", "", 6)]
        [TestCase("WL|C6|DA|V2|ML|4V3", "", -1000000, "", "wheel lock|cargo|floot mat|PREFERRED ACCESSORY", 4)]
        public void SanitizeOptionTestSanitizesCounts(string packages, string whitelList, decimal msrp, string includePhrases, string excludePhrases, int sanitizedOptionCount)
        {
            // just testing sanatize don't hassle the injected properties
            var vehicleAutoLoader = new VehicleAutoLoader(null, null, null, null);

            var optionFilter = new OptionFilter
            {
                MinumumMsrp = msrp,
                IncludePhrases = new List<string>(includePhrases.Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries)),
                ExcludePhrases = new List<string>(excludePhrases.Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries))
            };

            if(msrp == -1000000) // could not get nUnit to accept nullable decimal as a aurguement to a testcase
                optionFilter.MinumumMsrp = null;

            var unfilteredOptions = packages.Split(new[] {"|"}, StringSplitOptions.RemoveEmptyEntries).Select(package => new OptionPackage(package)).ToList();

            var whiteListOptions = new List<string>(whitelList.Split(new[] {"|"}, StringSplitOptions.RemoveEmptyEntries));

            var sanitizedOptions = vehicleAutoLoader.SanitizeOptions(unfilteredOptions, chromeOptions, whiteListOptions, optionFilter);

            Assert.AreEqual(sanitizedOptionCount, sanitizedOptions.Count, "The sanitized option process returned an unexpected number of options");

        }

    }
}
