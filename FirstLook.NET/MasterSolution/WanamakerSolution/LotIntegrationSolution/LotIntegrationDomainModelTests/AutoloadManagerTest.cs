﻿using FetchDotComClient.VehicleBuild;
using FirstLook.LotIntegration.DomainModel;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.DataAccess;
using MAX.BuildRequest;
using Merchandising.Messages;
using Moq;
using NUnit.Framework;

namespace LotIntegrationDomainModelTests
{
    [TestFixture]
    public class AutoloadManagerTest
    {

        /* test to make sure the correct vehicleAutoLoader descendant is instanciated based on agent and vehicle type */
        [TestCase(Agent.GmGlobalConnect, 1, "GeneralMotorsVehicleAutoLoader")]
        [TestCase(Agent.MazdaDcs, 1, "MazdaVehicleAutoLoader")]
        //[TestCase(Agent.FordConnect, 1, "FordNewVehicleAutoLoader")]
        //[TestCase(Agent.FordConnect, 2, "FordUsedVehicleAutoLoader")]
        [TestCase(Agent.DealerConnect, 1, "ChryslerVehicleAutoLoader")]
        [TestCase(Agent.DealerSpeed, 1, "BmwVehicleAutoLoader")]
        //[TestCase(Agent.ToyotaTechinfo, 1, "TaskVehicleAutoLoader")]
        [TestCase(Agent.VolkswagenHub, 1, "TaskVehicleAutoLoader")]
        [TestCase(Agent.AccessAudi, 1, "AudiVehicleAutoLoader")]
        [TestCase(Agent.HyundaiTechInfo, 1, "HyundaiVehicleAutoLoader")]
        [TestCase(Agent.NatDealerDaily, 1, "ToyotaVehicleAutoLoader")]
        [TestCase(Agent.SetDealerDaily, 1, "ToyotaVehicleAutoLoader")]
        [TestCase(Agent.NissanNorthAmerica, 1, "NissanVehicleAutoLoader")]
        public void CreateAutoLoaderTest(Agent agent, int inventoryType, string className)
        {
            // mocks
            var autoLoadRepository = new AutoloadRepository();
            
            var mockSender = new Mock<IAdMessageSender>();
            var adSender = mockSender.Object;

            var mockVehicleReferenceDataRepository = new Mock<IVehicleReferenceDataRepository>();
            var vehicleReferenceDataRepo = mockVehicleReferenceDataRepository.Object;

            var vehicleResponse = new VehicleResponse();

            // create autoload manager
            var autoLoadManager = new AutoloadManager(autoLoadRepository);

            // create buildRequestRepo
            var buildRequestRepository = new BuildRequestRepository();
            var vehicleDataProvider = buildRequestRepository.GetVehicleDataProvider(agent);

            // get the vehicle autoloader for these attributes
            var vehicleAutoLoader = autoLoadManager.CreateAutoLoader(agent, inventoryType, adSender, vehicleReferenceDataRepo, "", vehicleDataProvider);

            // test 'em
            var autoLoaderClassName = vehicleAutoLoader.GetType().Name;
            var agentName = agent.ToString();

            Assert.AreEqual(autoLoaderClassName, className, string.Format("The {0} agent should not use the autoloader class: {1}", agentName, autoLoaderClassName));



        }

    }
}
