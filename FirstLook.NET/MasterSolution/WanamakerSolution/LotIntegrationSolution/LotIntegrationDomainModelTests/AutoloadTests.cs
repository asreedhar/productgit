﻿using System.Linq;
using FetchDotComClient.VehicleBuild;
using FirstLook.LotIntegration.DomainModel;
using Merchandising.Messages;
using NUnit.Framework;
using Moq;
using System;
using System.Collections.Generic;


namespace LotIntegrationDomainModelTests
{
     [TestFixture]
    public class AutoloadTests
    {
        private Mock<IAutoloadRepository> _repoMock;
        private AutoloadManager _manager;
        private IAutoloadRepository _repo;

        [SetUp]
        public void Setup()
        {
            var statuses = new List<AutoloadStatus>
                               {
                                   new AutoloadStatus
                                       {
                                           BusinessUnitId = 1011,
                                           InventoryId = 1234,
                                           CreatedBy = "testuser",
                                           CreatedOn = DateTime.Parse("12/12/11").AddMinutes(30),
                                           StartTime = DateTime.Parse("12/12/11").AddMinutes(30),
                                           LastUpdatedBy = "testuser",
                                           LastUpdatedOn = DateTime.Parse("12/12/11").AddMinutes(33),
                                           EndTime =  null,
                                           StatusTypeId = Status.Pending

                                       },
                                       new AutoloadStatus
                                       {
                                           BusinessUnitId = 1011,
                                           InventoryId = 1235,
                                           CreatedBy = "testuser2",
                                           CreatedOn = DateTime.Parse("12/12/11").AddMinutes(31),
                                           StartTime = DateTime.Parse("12/12/11").AddMinutes(31),
                                           LastUpdatedBy = "testuser2",
                                           LastUpdatedOn = DateTime.Parse("12/12/11").AddMinutes(34),
                                           EndTime =  null,
                                           StatusTypeId = Status.Pending

                                       },
                                       new AutoloadStatus
                                       {
                                           BusinessUnitId = 1012,
                                           InventoryId = 1236,
                                           CreatedBy = "testuser1",
                                           CreatedOn = DateTime.Parse("12/12/11").AddMinutes(33),
                                           StartTime = DateTime.Parse("12/12/11").AddMinutes(33),
                                           LastUpdatedBy = "testuser1",
                                           LastUpdatedOn = DateTime.Parse("12/12/11").AddMinutes(35),
                                           EndTime =  null,
                                           StatusTypeId = Status.Pending

                                       },
                                       new AutoloadStatus
                                       {
                                           BusinessUnitId = 1012,
                                           InventoryId = 1239,
                                           CreatedBy = "testuser3",
                                           CreatedOn = DateTime.Parse("12/12/11").AddMinutes(35),
                                           StartTime = DateTime.Parse("12/12/11").AddMinutes(35),
                                           LastUpdatedBy = "testuser5",
                                           LastUpdatedOn = DateTime.Parse("12/12/11").AddMinutes(37),
                                           EndTime =  DateTime.Parse("12/12/11").AddMinutes(37),
                                           StatusTypeId = Status.Pending

                                       },
                                        new AutoloadStatus
                                       {
                                           BusinessUnitId = 1012,
                                           InventoryId = 1237,
                                           CreatedBy = "testuser3",
                                           CreatedOn = DateTime.Parse("12/12/11").AddMinutes(37),
                                           StartTime = DateTime.Parse("12/12/11").AddMinutes(37),
                                           LastUpdatedBy = "testuser5",
                                           LastUpdatedOn = DateTime.Parse("12/12/11").AddMinutes(39),
                                           EndTime =  DateTime.Parse("12/12/11").AddMinutes(39),
                                           StatusTypeId = Status.Successful

                                       },
                                       new AutoloadStatus
                                       {
                                           BusinessUnitId = 1012,
                                           InventoryId = 1236,
                                           CreatedBy = "testuser3",
                                           CreatedOn = DateTime.Parse("12/12/11").AddMinutes(37),
                                           StartTime = DateTime.Parse("12/12/11").AddMinutes(37),
                                           LastUpdatedBy = "testuser5",
                                           LastUpdatedOn = DateTime.Parse("12/12/11").AddMinutes(39),
                                           EndTime =  DateTime.Parse("12/12/11").AddMinutes(39),
                                           StatusTypeId = Status.InvalidCredentials

                                       },
                                       new AutoloadStatus
                                       {
                                           BusinessUnitId = 1013,
                                           InventoryId = 1236,
                                           CreatedBy = "testuser3",
                                           CreatedOn = DateTime.Parse("12/12/11").AddMinutes(37),
                                           StartTime = DateTime.Parse("12/12/11").AddMinutes(37),
                                           LastUpdatedBy = "testuser5",
                                           LastUpdatedOn = DateTime.Parse("12/12/11").AddMinutes(39),
                                           EndTime =  DateTime.Parse("12/12/11").AddMinutes(39),
                                           StatusTypeId = Status.InvalidCredentials

                                       }
                               };
            _repoMock = new Mock<IAutoloadRepository>();
            
            _repoMock.Setup(mr => mr.Fetch(
                It.IsAny<int>(), It.IsAny<int>())).Returns((int i, int j) => statuses.Where(
                x => x.BusinessUnitId == i && x.InventoryId ==j).Single());

            _repoMock.Setup(mr => mr.GetAllByStatusType(
                It.IsAny<int>(), It.IsAny<Status>())).Returns((int i, Status j) => statuses.Where(
                x => x.BusinessUnitId == i && x.StatusTypeId == j));

            _repoMock.Setup(mr => mr.GetAllWithErrors(
                It.IsAny<int>())).Returns(
                    ((int i) => statuses.Where(
                        x => x.BusinessUnitId == i && x.StatusTypeId < 0).Select(x => x.InventoryId).ToList()));



            _repo = _repoMock.Object;

            _manager = new AutoloadManager(_repo);

        }

         //make sure the code changes do not break the numbers in DB
        [TestCase(Status.InvalidCredentials, -99)]
        [TestCase(Status.UnknownError, -100)]
        [TestCase(Status.NoVehicleFound, -98)]
        [TestCase(Status.NetworkError, -97)]
        [TestCase(Status.Pending, 0)]
        [TestCase(Status.Successful, 100)]
        public void Status_enums_should_map_to_status_numbers_in_DB(
            Status statusTypeIdEnum, int dbStatusTypeId)
        {
            var statusTypeId = (int) statusTypeIdEnum;

            Assert.That(statusTypeId, Is.EqualTo(dbStatusTypeId));
        }

         [Test]
         public void All_Status_Enums_Exist_In_DB()
         {

             var statusTypeIds = new List<int>{-100, -99,-98, -97, -96, -89, -88, -9, -8, 0, 100};
             foreach (var statusEnum in Enum.GetValues(typeof(Status)))
             {
                 Assert.That(statusTypeIds.Contains((int) statusEnum));
             } 

         }
         [TestCase((int)Status.InvalidCredentials, "")]
         [TestCase((int)Status.NetworkError, "")]
         [TestCase((int)Status.NoVehicleFound, "")]
         [TestCase((int)Status.Pending, "")]
         [TestCase((int)Status.Successful, "Autoloaded")]
         [TestCase((int)Status.UnknownError, "")]
         [TestCase(22, "")]
         public void AutoloadStatus_Should_Return_Appropriate_State(int statusTypeId, string expectedState)
         {
             var stateResult = AutoloadManager.GetAutoloadState(statusTypeId);
             Assert.That(stateResult, Is.EqualTo(expectedState));
         }

         [TestCase]
         public void Autoload_status_should_be_invalid_if_endtime_is_less_than_starttime()
         {
             var status = new AutoloadStatus
                              {BusinessUnitId = 12, StartTime = DateTime.Now, EndTime = DateTime.Now.AddDays(-2)};
             List<string> messages;

             var isValid = status.IsValid(status, out messages);
             Assert.IsFalse(isValid);
             
             Assert.That(messages != null && messages.Count > 0);
         }

         [TestCase]
         public void Autoload_should_be_invalid_if_autoload_ends_with_pending_status()
         {
             var status = new AutoloadStatus { BusinessUnitId = 12, StartTime = DateTime.Now, EndTime = DateTime.Now.AddDays(2), StatusTypeId = Status.Pending};
             List<string> messages;

             var isValid = status.IsValid(status, out messages);
             Assert.IsFalse(isValid);
             Assert.That(messages != null && messages.Count > 0);
         }

         [TestCase]
         public void Autoload_status_should_be_valid_for_autoloadstart()
         {
             var status = new AutoloadStatus { BusinessUnitId = 12, StartTime = DateTime.Now, StatusTypeId = Status.Pending };
             List<string> messages;

             var isValid = status.IsValid(status, out messages);
             Assert.IsTrue(isValid);
             Assert.That(messages != null && messages.Count == 0);
         }

         [TestCase]
         public void Autoload_status_should_be_valid_for_autoloadend()
         {
             var status = new AutoloadStatus { BusinessUnitId = 12, StartTime = DateTime.Now, EndTime = DateTime.Now.AddDays(2), StatusTypeId = Status.Successful };
             List<string> messages;

             var isValid = status.IsValid(status, out messages);
             Assert.IsTrue(isValid);
             Assert.That(messages != null && messages.Count == 0);
         }

    }
}
