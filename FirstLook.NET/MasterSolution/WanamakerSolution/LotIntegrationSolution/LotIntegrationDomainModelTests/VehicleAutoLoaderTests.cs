﻿using System.Collections.Generic;
using System.Text;
using Autofac;
using Core.Messaging;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.LotIntegration.DomainModel;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Chrome.Mapper;
using FirstLook.Merchandising.DomainModel.DataAccess;
using FirstLook.Merchandising.DomainModel.Vehicles;
using MAX.BuildRequest;
using Merchandising.Messages;
using Merchandising.Messages.AutoLoad;
using Merchandising.Messages.VehicleBuild;
using Moq;
using NUnit.Framework;

using VehicleDataAccess;




namespace LotIntegrationDomainModelTests
{
    /// <summary>
    /// These tests are here because the VehiclDataProviders are defined in LotIntegrationDomainModel.
    /// </summary>
    [TestFixture]
    public class VehicleAutoLoaderTests
    {
        private const string VIN1 = "WBAKB8C58BCY65872";
        private const int VINPattern1 = 109300;
        private const string VIN2 = "WBANB33554B088088";
        private const int VINPattern2 = 30669;
        private VehicleDataProviderManager _providerManager;
        
        private Mock<IChromeMapper> ChromeMapperMock;

        private IVehicleReferenceDataRepository vehicleReferenceDataRepository;
        private IAutoloadRepository autoloadRepository;
        private IAdMessageSender adMessageSender;


        [TestFixtureSetUp]
        public void Setup()
        {
            // Mocks
            var mockSender = new Mock<IAdMessageSender>();
            var sender = mockSender.Object;
            adMessageSender = mockSender.Object;

            var mockAutoloadRepo = new Mock<IAutoloadRepository>();
            var autoloadrepo = mockAutoloadRepo.Object;
            autoloadRepository = mockAutoloadRepo.Object;

            var queueMock = new Mock<IQueueFactory>();
            var fetchAuditQueue = new Mock<IQueue<FetchAuditMessage>>();
            queueMock.Setup(x => x.CreateAutoLoadAudit()).Returns(fetchAuditQueue.Object);

            ChromeMapperMock = new Mock<IChromeMapper>();
            ChromeMapperMock.Setup(m => m.LookupVinPatternIdByVin(VIN1)).Returns(VINPattern1);
            ChromeMapperMock.Setup(m => m.LookupVinPatternIdByVin(VIN2)).Returns(VINPattern2);

            var mockVehicleRefDataRepository = new Mock<IVehicleReferenceDataRepository>();
            vehicleReferenceDataRepository = mockVehicleRefDataRepository.Object;

            // registry
            var b = new ContainerBuilder();
            b.RegisterType<NullCache>().As<ICache>();
            b.RegisterInstance(sender).As<IAdMessageSender>();
            b.RegisterInstance(autoloadrepo).As<IAutoloadRepository>();
            b.RegisterInstance(queueMock.Object).As<IQueueFactory>();
            b.RegisterInstance(ChromeMapperMock.Object).As<IChromeMapper>();
            b.RegisterInstance(vehicleReferenceDataRepository).As<IVehicleReferenceDataRepository>();

            var container = b.Build();
            Registry.RegisterContainer(container);

            _providerManager = new VehicleDataProviderManager();
        }

        [Test]
        public void TestFordPrefixScrub()
        {

            var optionPackages = new List<OptionPackage>
                {
                    new OptionPackage("F10122W", "test"),
                    new OptionPackage("F1012295", "test"),
                    new OptionPackage("F1012995", "test"),
                    new OptionPackage("F101262S", "test"),
                    new OptionPackage("F101277L", "test"),
                    new OptionPackage("F1012UG", "test"),
                    new OptionPackage("F1012UT", "test")
                };

            var data = new VehicleBuildData {OptionPackages = optionPackages};

            string prefix = FordNewVehicleAutoLoader.GetCommonPrefix(data);

            Assert.IsTrue(prefix == "F1012");
        }

        [Test]
        public void TestFordPrefixScrubNoMatch()
        {
            var optionPackages = new List<OptionPackage>
                {
                    new OptionPackage("345"), 
                    new OptionPackage("123"), 
                    new OptionPackage("34"), 
                    new OptionPackage("23")
                };

            var data = new VehicleBuildData {OptionPackages = optionPackages};

            string prefix = FordNewVehicleAutoLoader.GetCommonPrefix(data);

            Assert.IsTrue(prefix == string.Empty);
        }

        [Test]
        public void TestFordUsedWhiteList()
        {
            string[] tests = new string[]{
                "MOONROOF, POWER",
                "LTHR blah seats",
                "LTHR blah sea",
                "FRONT LICENSE PLATE BRACKET",
                "POLISHED ALUMINUM WHEELS",
                "PL LEATHER CAPTAINS CHAIRS",
                "REVERSE SENSING SYSTEM",
                "REAR VIEW CAMERA",
                "CHROME PWR FLD HTD SIGNAL MIR",
                "POWER DEPLOYABLE RUNNING BDS",
                "PLATINUM PACKAGE",
                "PREFERRED EQUIPMENT PKG.508A",
                "HEAT/COOLED FRONT SEATS",
                "PREFERRED EQUIPMENT PKG.507A",
                "LT CONVENIENCE PACKAGE",
                "BED LINER DROP-IN *ACCY",
                "alum wheels",
                "v6 engine",
                "KING RANCH"};

            Assert.IsTrue(FordUsedVehicleAutoLoader.WhiteListMatch(tests[0]));
            Assert.IsTrue(FordUsedVehicleAutoLoader.WhiteListMatch(tests[1]));
            Assert.IsFalse(FordUsedVehicleAutoLoader.WhiteListMatch(tests[2]));
            Assert.IsFalse(FordUsedVehicleAutoLoader.WhiteListMatch(tests[3]));
            
            Assert.IsTrue((FordUsedVehicleAutoLoader.WhiteListMatch(tests[4])));
            Assert.IsTrue((FordUsedVehicleAutoLoader.WhiteListMatch(tests[5])));
            Assert.IsTrue((FordUsedVehicleAutoLoader.WhiteListMatch(tests[6])));
            Assert.IsTrue((FordUsedVehicleAutoLoader.WhiteListMatch(tests[7])));
            Assert.IsTrue((FordUsedVehicleAutoLoader.WhiteListMatch(tests[8])));

            Assert.IsFalse((FordUsedVehicleAutoLoader.WhiteListMatch(tests[9])));

            Assert.IsTrue((FordUsedVehicleAutoLoader.WhiteListMatch(tests[10])));
            Assert.IsTrue((FordUsedVehicleAutoLoader.WhiteListMatch(tests[11])));
            Assert.IsTrue((FordUsedVehicleAutoLoader.WhiteListMatch(tests[12])));
            Assert.IsTrue((FordUsedVehicleAutoLoader.WhiteListMatch(tests[13])));
            Assert.IsTrue((FordUsedVehicleAutoLoader.WhiteListMatch(tests[14])));

            Assert.IsFalse((FordUsedVehicleAutoLoader.WhiteListMatch(tests[15])));

            Assert.IsTrue((FordUsedVehicleAutoLoader.WhiteListMatch(tests[16])));
            Assert.IsTrue((FordUsedVehicleAutoLoader.WhiteListMatch(tests[17])));
            Assert.IsTrue((FordUsedVehicleAutoLoader.WhiteListMatch(tests[18])));
        }

        [Test, Ignore]
        public void TestFordOptionMatch()
        {
            var buildRequestRepository = new BuildRequestRepository();
            var vehicleDataProvider = buildRequestRepository.GetVehicleDataProvider(Agent.FordConnect);

            var loader = new FordNewVehicleAutoLoader(null, null, null, null, vehicleDataProvider);
            EquipmentCollection equipmentCollection = new EquipmentCollection();
            equipmentCollection.Add(new Equipment() { OptionCode = "446" });
            equipmentCollection.Add(new Equipment() { OptionCode = "2W" });
            equipmentCollection.Add(new Equipment() { OptionCode = "995" });
            equipmentCollection.Add(new Equipment() { OptionCode = "D9E" });
            equipmentCollection.Add(new Equipment() { OptionCode = "GT" });
            equipmentCollection.Add(new Equipment() { OptionCode = "UH" });
            equipmentCollection.Add(new Equipment() { OptionCode = "UG" });

            var optionPackages = new List<OptionPackage>();
            optionPackages.Add(new OptionPackage("F10122W", "CHARCOAL BLACK BUCKET SEATS"));
            optionPackages.Add(new OptionPackage("F1012995", ".5.4L SOHC V8 ENGINE"));
            optionPackages.Add(new OptionPackage("F101262S", "SIRIUS RADIO W/6 MOS SVC"));
            optionPackages.Add(new OptionPackage("F101277L", "LIMITED SERIES"));
            optionPackages.Add(new OptionPackage("F1012UG", "WHITE PLATINUM MET TRI-COAT"));
            optionPackages.Add(new OptionPackage("F101UG2", "Miss"));

            var equipment = loader.MatchOption(equipmentCollection, optionPackages[0]);
            Assert.IsTrue(equipment.OptionCode == "2W");
            equipment = loader.MatchOption(equipmentCollection, optionPackages[1]);
            Assert.IsTrue(equipment.OptionCode == "995");
            equipment = loader.MatchOption(equipmentCollection, optionPackages[2]);
            Assert.IsTrue(equipment == null);
            equipment = loader.MatchOption(equipmentCollection, optionPackages[3]);
            Assert.IsTrue(equipment == null);
            equipment = loader.MatchOption(equipmentCollection, optionPackages[4]);
            Assert.IsTrue(equipment.OptionCode == "UG");
            equipment = loader.MatchOption(equipmentCollection, optionPackages[5]);
            Assert.IsTrue(equipment == null);
        }

       

        [Test]
        public void NextActions()
        {
            // Current Style Id	Inferred Style Id	Action
            // 0	                0	            Don't load options or colors, don't set the style
            // 1	                0	            Load options and colors, don't set the style
            // 0	                1	            Load options and colors, set the style
            // 1	                1	            Load options and colors, don't set the style
            // 1	                2	            Don’t load options or colors, don't set the style, exit


            var buildRequestRepository = new BuildRequestRepository();
            var vehicleDataProvider = buildRequestRepository.GetVehicleDataProvider(Agent.GmGlobalConnect);
            
            var loader = new VehicleAutoLoader(autoloadRepository, adMessageSender, vehicleReferenceDataRepository, vehicleDataProvider);
            var sb = new StringBuilder();

            bool loadOptionsAndColors, setStyle;

            loadOptionsAndColors = false; setStyle = false;
            loader.GetNextActions(0,0,sb,out loadOptionsAndColors, out setStyle);
            Assert.IsFalse(loadOptionsAndColors);
            Assert.IsFalse(setStyle);

            loadOptionsAndColors = false; setStyle = false;
            loader.GetNextActions(1, 0, sb, out loadOptionsAndColors, out setStyle);
            Assert.IsTrue(loadOptionsAndColors);
            Assert.IsFalse(setStyle);

            loadOptionsAndColors = false; setStyle = false;
            loader.GetNextActions(0, 1, sb, out loadOptionsAndColors, out setStyle);
            Assert.IsTrue(loadOptionsAndColors);
            Assert.IsTrue(setStyle);

            loadOptionsAndColors = false; setStyle = false;
            loader.GetNextActions(1, 1, sb, out loadOptionsAndColors, out setStyle);
            Assert.IsTrue(loadOptionsAndColors);
            Assert.IsFalse(setStyle);

            // Behavior changed on 4/10/2014. Autoload will overwrite in the case of a conflict.
            loadOptionsAndColors = false; setStyle = false;
            loader.GetNextActions(1, 2, sb, out loadOptionsAndColors, out setStyle);
            Assert.IsTrue(loadOptionsAndColors);
            Assert.IsTrue(setStyle);

        }

       
    
        [Test]
        public void IsColorSpecified()
        {
            Assert.IsFalse(VehicleAutoLoader.IsColorSpecified(string.Empty));
            Assert.IsFalse(VehicleAutoLoader.IsColorSpecified(" "));
            Assert.IsFalse(VehicleAutoLoader.IsColorSpecified("uNknowN"));
            Assert.IsTrue(VehicleAutoLoader.IsColorSpecified("a"));
        }

    }

     
}
