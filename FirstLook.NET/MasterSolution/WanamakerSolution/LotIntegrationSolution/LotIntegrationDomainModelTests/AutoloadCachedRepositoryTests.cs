﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Common.Core;
using FirstLook.LotIntegration.DomainModel;
using Moq;
using NUnit.Framework;

namespace LotIntegrationDomainModelTests
{
      [TestFixture]
    class AutoloadCachedRepositoryTests
    {
   
        private Mock<ICache> CacheMock;
        private ICache Cache;
        private Mock<IAutoloadRepository> InnerMock;
        private IAutoloadRepository Inner;

        private readonly List<string> _makes = new List<string>() { "BMW", "Mazda", "Chrysler" };

        private readonly List<ManufacturerAutoLoadSettings> _manufacturers = new List<ManufacturerAutoLoadSettings>() { new ManufacturerAutoLoadSettings(1, "BMW", true) };

        [SetUp]
        public void Setup()
        {
            CacheMock = new Mock<ICache>();
            Cache = CacheMock.Object;

            InnerMock = new Mock<IAutoloadRepository>();
            Inner = InnerMock.Object;
        }

        [Test]
        public void GetSupportedMakeList_should_call_inner_GetSupportedMakeList_when_data_not_in_cache()
        {
            var repo = new AutoloadCachedRepository(Cache, Inner);

            repo.GetSupportedMakeList();

            InnerMock.Verify(i => i.GetSupportedMakeList(), Times.Once());
        }

        [Test]
        public void GetSupportedManufacturerList_should_call_inner_GetSupportedManufacturerList_when_data_not_in_cache()
        {
            var repo = new AutoloadCachedRepository(Cache, Inner);

            repo.GetSupportedManufacturerList();

            InnerMock.Verify(i => i.GetSupportedManufacturerList(), Times.Once());
        }

        [Test]
        public void GetSupportedMakeList_should_store_data_in_cache_when_data_not_in_cache()
        {
           

            InnerMock.Setup(i => i.GetSupportedMakeList()).Returns(_makes);

            var repo = new AutoloadCachedRepository(Cache, Inner);

            repo.GetSupportedMakeList();

            CacheMock.Verify(c => c.Set(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<int?>()), Times.Once());
        }
        [Test]
        public void GetSupportedManufacturerList_should_store_data_in_cache_when_data_not_in_cache()
        {
            InnerMock.Setup(i => i.GetSupportedManufacturerList()).Returns(_manufacturers);

            var repo = new AutoloadCachedRepository(Cache, Inner);

            repo.GetSupportedManufacturerList();

            CacheMock.Verify(c => c.Set(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<int?>()), Times.Once());
        }

        [Test]
        public void GetSupportedMakeList_should_utilize_cache_when_called_multiple_times()
        {
            var miniCache = new Dictionary<string, object>();
            CacheMock.Setup(c => c.Set(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<int?>()))
                .Callback<string, object, int?>((k, v, t) => miniCache[k] = v);
            CacheMock.Setup(c => c.Get(It.IsAny<string>()))
                .Returns<string>(k =>
                                     {
                                         object val;
                                         miniCache.TryGetValue(k, out val);
                                         return val;
                                     });
            InnerMock.Setup(i => i.GetSupportedMakeList()).Returns(_makes);

            var repo = new AutoloadCachedRepository(Cache, Inner);

            repo.GetSupportedMakeList();
            repo.GetSupportedMakeList();
            var makes = repo.GetSupportedMakeList();


            InnerMock.Verify(i => i.GetSupportedMakeList(), Times.Once());
            Assert.That(makes, Is.SameAs(_makes));
        }

        [Test]
        public void GetSupportedManufacturerList_should_utilize_cache_when_called_multiple_times()
        {
            var miniCache = new Dictionary<string, object>();
            CacheMock.Setup(c => c.Set(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<int?>()))
                .Callback<string, object, int?>((k, v, t) => miniCache[k] = v);
            CacheMock.Setup(c => c.Get(It.IsAny<string>()))
                .Returns<string>(k =>
                {
                    object val;
                    miniCache.TryGetValue(k, out val);
                    return val;
                });
            InnerMock.Setup(i => i.GetSupportedManufacturerList()).Returns(_manufacturers);

            var repo = new AutoloadCachedRepository(Cache, Inner);

            repo.GetSupportedManufacturerList();
            repo.GetSupportedManufacturerList();
            var manufacturers = repo.GetSupportedManufacturerList();


            InnerMock.Verify(i => i.GetSupportedManufacturerList(), Times.Once());
            Assert.That(manufacturers, Is.SameAs(_manufacturers));
        }

        [Test]
        public void GetMakeList_should_not_call_inner_GetMakeList_when_data_already_in_cache()
        {
            CacheMock.Setup(c => c.Get(It.IsAny<string>())).Returns(_makes);

            var repo = new AutoloadCachedRepository(Cache, Inner);

            repo.GetSupportedMakeList();

            InnerMock.Verify(i => i.GetSupportedMakeList(), Times.Never());
        }

        [Test]
        public void GetManufacturerList_should_not_call_inner_GetManufacturerList_when_data_already_in_cache()
        {
            CacheMock.Setup(c => c.Get(It.IsAny<string>())).Returns(_manufacturers);

            var repo = new AutoloadCachedRepository(Cache, Inner);

            repo.GetSupportedManufacturerList();

            InnerMock.Verify(i => i.GetSupportedManufacturerList(), Times.Never());
        }

        [Test]
        public void UpdateAutoloadSettings_should_clear_the_manufacturer_cache()
        {
            var miniCache = new Dictionary<string, object>();
            CacheMock.Setup(c => c.Set(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<int?>()))
                .Callback<string, object, int?>((k, v, t) => miniCache[k] = v);
            CacheMock.Setup(c => c.Get(It.IsAny<string>()))
                .Returns<string>(k =>
                {
                    object val;
                    miniCache.TryGetValue(k, out val);
                    return val;
                });
            InnerMock.Setup(i => i.GetSupportedManufacturerList()).Returns(_manufacturers); 

            var repo = new AutoloadCachedRepository(Cache, Inner);


            var manufacturersList = new List<int> { 1, 2, 3 };

            repo.UpdateAutoLoadSettings(manufacturersList);

            repo.GetSupportedManufacturerList();

            InnerMock.Verify(i => i.GetSupportedManufacturerList(), Times.Once());
        }

        [Test]
        public void UpdateAutoloadSettings_should_clear_the_makes_cache()
        {

            var miniCache = new Dictionary<string, object>();
            CacheMock.Setup(c => c.Set(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<int?>()))
                .Callback<string, object, int?>((k, v, t) => miniCache[k] = v);
            CacheMock.Setup(c => c.Get(It.IsAny<string>()))
                .Returns<string>(k =>
                {
                    object val;
                    miniCache.TryGetValue(k, out val);
                    return val;
                });
            InnerMock.Setup(i => i.GetSupportedMakeList()).Returns(_makes); 

            var repo = new AutoloadCachedRepository(Cache, Inner);

            var manufacturersList = new List<int> { 1, 2, 3 };

            repo.UpdateAutoLoadSettings(manufacturersList);

            repo.GetSupportedMakeList();

            InnerMock.Verify(i => i.GetSupportedMakeList(), Times.Once());
        }
    }
}