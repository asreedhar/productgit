﻿using System;
using System.Collections.Generic;
using Merchandising.Messages;

namespace FirstLook.LotIntegration.DomainModel
{
    public class AutoloadStatus
    {
        public Status StatusTypeId { get; set; }
        public int BusinessUnitId { get; set; }
        public int InventoryId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public string LastUpdatedBy { get; set; }
        
        public bool IsValid(AutoloadStatus autoloadStatus, out List<string> messages)
        {
            var valid = true;
            messages = new List<string>();
            if (EndTime.HasValue && EndTime < StartTime)
            {
                valid =  false;
                messages.Add("End time cannot be less than start time.");
            }
            if (EndTime.HasValue && StatusTypeId == Status.Pending)
            {
                valid = false;
                messages.Add("The status cannot be pending if the autoload has finished.");
            }
            return valid;
        }
    }
}
