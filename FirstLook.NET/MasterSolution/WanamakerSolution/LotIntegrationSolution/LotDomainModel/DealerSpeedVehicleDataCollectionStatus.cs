using System;
using System.Collections.Generic;
using System.Text;

namespace FirstLook.LotIntegration.DomainModel
{
    public enum DealerSpeedVehicleDataCollectionStatus
    {
        Undefined = 0,
        Success = 1,
        InvalidLogin = 2,
        AccountLockedOut = 3,
        NoLoginSupplied = 999
    }

    public enum GmGlobalConnectVehicleDataCollectionStatus
    {
        Undefined = 0,
        Success = 1,
        InvalidLogin = 2,
        AccountLockedOut = 3,
        NoLoginSupplied = 999
    }
}