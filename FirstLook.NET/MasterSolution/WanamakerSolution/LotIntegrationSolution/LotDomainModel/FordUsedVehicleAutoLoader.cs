﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.DataAccess;
using FirstLook.Merchandising.DomainModel.Vehicles;
using MAX.BuildRequest.VehicleDataProviders;
using Merchandising.Messages.VehicleBuild;
using VehicleDataAccess;

namespace FirstLook.LotIntegration.DomainModel
{
    internal class FordUsedVehicleAutoLoader : TaskVehicleAutoLoader
    {
        private static string[] WhiteListExpressions = new[]
                                           {
                                               "pkg",
                                               "package",
                                               "group",
                                               "kit",
                                               "order code",
                                               "rapid spec",
                                               "leather",
                                               "nav",
                                               "lariat",
                                               "remote start",
                                               "camera",
                                               "reverse sensing",
                                               "chrome",
                                               "LTHR.*seats",
                                               "Heat[ed]?/cooled.*seats", // todo/logic: fix regex semantics, add unit test
                                               "alloy",
                                               "sync",
                                               "premium",
                                               "turbo",
                                               "myford",
                                               "panoramic",
                                               "power liftgate",
                                               "THX",
                                               "blind spot monitoring system",
                                               "park assist",
                                               "air susp",
                                               "moonroof",
                                               "alum[inum]?", // todo/logic: fix regex semantics, add unit test
                                               "engine",
                                               "king ranch"
                                           };

        public FordUsedVehicleAutoLoader(IAutoloadRepository autoloadRepository, IAdMessageSender adMessageSender, IVehicleReferenceDataRepository vehicleReferenceDataRepository, string fileConent, IVehicleDataProvider vehicleDataProvider)
            : base(autoloadRepository, adMessageSender, vehicleReferenceDataRepository, fileConent, vehicleDataProvider)
        {

        }

        public override bool HasLoadedOption(DetailedEquipmentCollection options, OptionPackage package)
        {
            return options.ContainsOptionText(package.ProviderDescription);
        }

        public override void LoadPackage(DisplayEquipmentCollection vehicleOptions, Equipment equip, int businessUnitId, int inventoryId)
        {
            DetailedEquipment detailedEquip = new DetailedEquipment(equip);
            detailedEquip.DetailText = detailedEquip.OptionText;

            vehicleOptions.options.Add(detailedEquip);
        }

        public override Equipment MatchOption(EquipmentCollection equipmentCollection, OptionPackage package)
        {
            return new Equipment(){Description = package.ProviderDescription, OptionCode = String.Empty};
        }

        public static bool WhiteListMatch(string description)
        {
            foreach (string pattern in WhiteListExpressions)
            {
                if (Regex.IsMatch(description, pattern, RegexOptions.IgnoreCase))
                    return true;
            }

            return false;
        }

        internal override VehicleBuildData GetBuildData(int businessUnitId, int inventoryId, string processName, string vin, int inventoryType)
        {
            var vehicleBuild = base.GetBuildData(businessUnitId, inventoryId, processName, vin, inventoryType);

            //clear options and go through unfilteredoptions and apply regex white list
            vehicleBuild.OptionPackages = new List<OptionPackage>();

            //white list regex impl for used cars
            foreach(var option in vehicleBuild.UnfilteredOptionPackages)
            {
                if(WhiteListMatch(option.ProviderDescription))
                    vehicleBuild.OptionPackages.Add(option);
            }

            return vehicleBuild;
        }
    }
}
