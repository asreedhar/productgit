﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using Core.Messaging;
using Dapper;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Vehicles;
using Merchandising.Messages;
using Merchandising.Messages.AutoApprove;
using Merchandising.Messages.AutoLoad;

namespace FirstLook.LotIntegration.DomainModel.Tasks
{
    /// <summary>
    /// Listen for new VinDecodedMessage messages. When a message is received, save the data back to the back-end databases
    /// and send an AutoApprove message (if appropriate).
    /// 
    /// The intent is to decouple _how_ the different autoloads determine styles, options, etc... from all the common stuff that needs to happen
    /// when it's time to apply that data to the system.
    /// </summary>
    internal class VinDecodedMessageProcessor : TaskRunner<VinDecodedMessage>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private AutoloadManager _autoloadManager;

        #region Injected dependencies

        public IAdMessageSender MessageSender { get; set; }
        private readonly IAutoloadRepository _autoloadRepository;

        #endregion

        public VinDecodedMessageProcessor(IQueueFactory factory, IAdMessageSender messageSender, IAutoloadRepository autoloadRepository)
            : base(factory.CreateVinDecodedQueue())
        {
            _autoloadRepository = autoloadRepository;
            _autoloadManager = new AutoloadManager(_autoloadRepository);

            MessageSender = messageSender;
        }

        public override void Process(VinDecodedMessage message)
        {
	        log4net.LogicalThreadContext.Properties["threadId"] = string.Format("VIN: {0}, ChromeStyleId: {1}", message.Vin, message.ChromeStyleId);
            Log.InfoFormat("Processing VinDecodedMessage for VIN: {0}", message.Vin);

            string vin = message.Vin;
            int chromeStyleId = message.ChromeStyleId;
            string[] optionCodes = message.OptionCodes;

            string exteriorColorCode = message.ExteriorManufacturerColorCode;
            string exteriorColor1Desc = message.ExteriorManufacturerColorDesc;

            string interiorColorCode = message.InteriorManufacturerColorCode;
            string interiorColorDesc = message.InteriorManufacturerColorDesc;

            string processName = message.SourceProcessName;

            // Find out where the vin is active.  Need the businessUnitId and inventoryId.
            var repository = new InventoryRepository();
            var inventories = repository.FindByVin(vin).ToList();

            if (!inventories.Any())
            {
                Log.InfoFormat("No active inventory found for VIN: {0}", message.Vin);
            }

            // These are all active inventory units with the specified VIN.  Process all of them.
            foreach (var inventory in inventories)
            {
                ProcessInventory(inventory, vin, processName, chromeStyleId,
                                 exteriorColorCode, interiorColorCode, interiorColorDesc, optionCodes);

            }

            Log.InfoFormat("Finished processing VinDecodedMessage for VIN: {0}", message.Vin);
        }

        private void ProcessInventory(Inventory inventory, string vin, string processName, int chromeStyleId,
                                      string exteriorColorCode, string interiorColorCode, string interiorColorDesc,
                                      string[] optionCodes)
        {
            int businessUnitId = inventory.BusinessUnitId;
            int inventoryId = inventory.InventoryId;

            Log.InfoFormat("Applying changes for vin: {0} at businessUnitId: {1}, inventoryId: {2}", vin, businessUnitId,
                           inventoryId);

            _autoloadManager.SetAutoloadStart(businessUnitId, inventoryId, processName);

            VehicleConfiguration currentVehicle = VehicleConfiguration.FetchOrCreateDetailed(businessUnitId, inventoryId,
                                                                                             processName);

            // If locked, we can't autoload.
            if (currentVehicle.LotDataImportLock)
            {
                Log.WarnFormat(
                    "Vehicle lot data import lock is set to LOCKED preventing AutoLoad data from saving. Details: businessUnitID: {0}, inventoryId: {1}, vin: {2}, processName: {3}",
                    businessUnitId, inventoryId, vin, processName);

                // TODO: we should add LotDataImportLock as a status.
                _autoloadManager.SetAutoloadEnd(businessUnitId, inventoryId, Status.UnknownError, processName);

                // TODO: Additional logging.

                return;
            }

            bool chromeStyleSet = currentVehicle.ChromeStyleID != chromeStyleId;
            currentVehicle.SetChromeStyleID(businessUnitId, inventoryId, chromeStyleId);

            currentVehicle.ExteriorColorCode = exteriorColorCode ?? string.Empty;
            currentVehicle.InteriorColorCode = interiorColorCode ?? string.Empty;
            currentVehicle.InteriorColor = interiorColorDesc ?? string.Empty;

            // currentVehicle.ExteriorColor1 = exteriorColor1Desc;
            // TODO: need to make sure this works with two-color vehicles.

            // get the color descriptions from chrome
            var colors = AvailableChromeColors.Fetch(chromeStyleId);
            var exterior = colors.ExteriorColors.Where(c => c.First.ColorCode == exteriorColorCode).ToList();
            if (exterior.Any())
            {
                var exteriorOne = exterior.First().First;
                currentVehicle.ExteriorColor1 = !string.IsNullOrWhiteSpace(exteriorOne.Description)
                                                    ? exteriorOne.Description
                                                    : exteriorOne.GenericColor;
            }

            var interior = colors.InteriorColors.Where(c => c.ColorCode == interiorColorCode).ToList();
            if (interior.Any())
            {
                currentVehicle.InteriorColor = !string.IsNullOrWhiteSpace(interior.First().Description)
                                                   ? interior.First().Description
                                                   : interior.First().GenericColor;
            }

            // Go get the chrome equipment for this styleId
            EquipmentCollection equipment = EquipmentCollection.FetchUnfilteredOptions(currentVehicle.ChromeStyleID);

            foreach (var optionCode in optionCodes)
            {
                var eq = equipment.GetByCode(optionCode);
                if (eq != null)
                {
                    currentVehicle.VehicleOptions.AddOption(eq, businessUnitId, inventoryId);
                }
                else
                {
                    Log.WarnFormat("The specified option code ({0}) is not available on this style.", optionCode);
                }
            }

            // Save the changes we've made to this vehicle
            currentVehicle.Save(businessUnitId, processName);

            Log.InfoFormat("Saved changes for vin: {0} at businessUnitId: {1}, inventoryId: {2}", vin, businessUnitId,
                           inventoryId);

            // TODO: Refactor this out to a message.
            // Mark the equipment packages complete.
            StepStatusCollection
                .Fetch(businessUnitId, inventoryId)
                .SetStatus(StepStatusTypes.ExteriorEquipment, ActivityStatusCodes.Complete)
                .SetStatus(StepStatusTypes.InteriorEquipment, ActivityStatusCodes.Complete)
                .Save(processName);

            // update the autoload status
            _autoloadManager.SetAutoloadEnd(businessUnitId, inventoryId, Status.Successful, processName);
            
            if(chromeStyleSet)
                MessageSender.SendStyleSet(businessUnitId, inventoryId, processName, GetType().FullName);

            // TODO: Save an audit record.
        }
    }

    internal class Inventory
    {
        public int BusinessUnitId { get; set; }
        public string StockNumber { get; set; }
        public int InventoryId { get; set; }
    }

    internal class InventoryRepository
    {
        public IEnumerable<Inventory> FindByVin(string vin)
        {
            // removed the join; AND V.BusinessUnitID = IA.BusinessUnitID
            // talked to Zac, could be active at one but needs to be updated at the other dealer
            // i had put it in as part of FB: 29451 thinking only active needed updating


             using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["FLDW"].ConnectionString))
            {
                sqlConnection.Open();

                const string query =
                    @"
                        SELECT IA.BusinessUnitID, IA.StockNumber, IA.InventoryID
                        FROM FLDW.dbo.InventoryActive IA
                            JOIN FLDW.dbo.Vehicle V 
                                ON V.VehicleID = IA.VehicleID
                        WHERE V.VIN = @vin
                    ";

                 
                //var inventories = sqlConnection.Query<Inventory>(query, new {vin});
                var inventories = sqlConnection.Query<Inventory>(query, new { vin = new DbString() { Value = vin, IsAnsi = true, Length = 17 } });
                 
                sqlConnection.Close();

                return inventories;
            }
            
        }
    }
}
