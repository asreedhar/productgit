﻿using System.Collections.Generic;
using System.Linq;
using Core.Messages;
using Core.Messaging;
using Merchandising.Messages;
using Merchandising.Messages.AutoApprove;
using Merchandising.Messages.AutoLoad;
using Merchandising.Messages.AutoOffline;
using Merchandising.Messages.AutoWindowSticker;
using Merchandising.Messages.GroupLevel;
using Merchandising.Messages.InventoryDocuments;
using Merchandising.Messages.Slurpee;
using Merchandising.Messages.VehicleTransfer;
using Merchandising.Messages.WebLoader;

namespace FirstLook.LotIntegration.DomainModel.Tasks
{
    //Cached queues so we don't create anew for each inventory in Autoload Tasks
    internal class CachedFetchQueueFactory : IQueueFactory
    {
        private readonly IQueueFactory _innerFactory;

        public IQueue<InventoryMessage> CreateInventoryChanged()
        {
            return _innerFactory.CreateInventoryChanged();
        }

        public IQueue<AutoApproveInventoryMessage> CreateAutoApproveInventory()
        {
            return _innerFactory.CreateAutoApproveInventory();
        }

        public IQueue<AutoApproveMessage> CreateAutoApprove()
        {
            return _innerFactory.CreateAutoApprove();
        }

        public IQueue<FetchAuditMessage> CreateAutoLoadAudit()
        {
            return _innerFactory.CreateAutoLoadAudit();
        }

        public IQueue<AutoLoadResultMessage> CreateAutoLoadResult()
        {
            return _innerFactory.CreateAutoLoadResult();
        }

        public IQueue<AutoLoadInventoryMessage> CreateAutoLoadInventory()
        {
            return _innerFactory.CreateAutoLoadInventory();
        }

        public IQueue<NewInventoryMessage> CreateAutoLoadNewInventory()
        {
            return _innerFactory.CreateAutoLoadNewInventory();
        }

        public IQueue<VinDecodedMessage> CreateVinDecodedQueue()
        {
            return _innerFactory.CreateVinDecodedQueue();
        }

        public IQueue<FordDirectAutoLoadMessage> CreateFordDirectAutoLoadMessageQueue()
        {
            return _innerFactory.CreateFordDirectAutoLoadMessageQueue();
        }

        public IQueue<SlurpeeResultMessage> CreateSlurpeeResultQueue()
        {
            return _innerFactory.CreateSlurpeeResultQueue();
        }

        public IQueue<HondaEconfigBuildRequestMessage> CreateHondaEconfigRequestQueue()
        {
            return _innerFactory.CreateHondaEconfigRequestQueue();
        }

        public IQueue<NewInventoryMessage> CreateMerchandisingNewInventory()
        {
            return _innerFactory.CreateMerchandisingNewInventory();
        }

        public IQueue<ProcessAutoOfflineMessage> CreateProcessAutoOffline()
        {
            return _innerFactory.CreateProcessAutoOffline();
        }

        public IQueue<AutoWindowStickerMessage> CreateAutoWindowSticker()
        {
            return _innerFactory.CreateAutoWindowSticker();
        }

        public IQueue<ClickStatsMessage> CreateWindowStickClickStats()
        {
            return _innerFactory.CreateWindowStickClickStats();
        }

        public IQueue<AutoWindowStickerReport> CreateWindowStickerReportQueue()
        {
            return _innerFactory.CreateWindowStickerReportQueue();
        }

        public IQueue<PhotoUploadMessage> CreateWebLoaderPhotoUpload()
        {
            return _innerFactory.CreateWebLoaderPhotoUpload();
        }

        public IQueue<PhotoBatchUploadMessage> CreateWebLoaderAbsolutePosition()
        {
            return _innerFactory.CreateWebLoaderAbsolutePosition();
        }

        public IQueue<DirectPhotoUploadMessage> CreateWebLoaderDirectPhotoUpload()
        {
            return _innerFactory.CreateWebLoaderDirectPhotoUpload();
        }

        public IQueue<PhotoTransferMessage> CreatePhotoTransferQueue()
        {
            return _innerFactory.CreatePhotoTransferQueue();
        }

        public IQueue<GroupLevelAggregationMessage> CreateGroupLevelAggregation()
        {
            return _innerFactory.CreateGroupLevelAggregation();
        }


        public IQueue<DeadLetterMessage> CreateDeadLetterArchive()
        {
            return _innerFactory.CreateDeadLetterArchive();
        }


		public IQueue<global::Merchandising.Messages.GoogleAnalytics.GoogleAnalyticsGenerateMessage> CreateGoogleAnalyticsQueue()
        {
            return _innerFactory.CreateGoogleAnalyticsQueue();
        }

        #region Document Publishing Queues
        
        public IQueue<InactiveInventoryTaskStartMessage> CreateInactiveInventoryTaskControlQueue()
        {
            return _innerFactory.CreateInactiveInventoryTaskControlQueue();
        }

        #endregion

        public IQueue<ApproveMessage> CreateApprove()
        {
            return _innerFactory.CreateApprove();
        }

        public IQueue<CreatePdfMessage> CreatePdf()
        {
            return _innerFactory.CreatePdf();
        }

    }
}
