﻿using System;
using System.Reflection;
using AmazonServices.CW;
using Core.Messaging;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using MAX.BuildRequest;
using MAX.ExternalCredentials;
using Merchandising.Messages;
using Core.Messages;
using Merchandising.Messages.AutoLoad;


namespace FirstLook.LotIntegration.DomainModel.Tasks
{
    internal class AutoLoadNewInventoryTask : TaskRunner<NewInventoryMessage>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly string DeclaringTypeName = MethodBase.GetCurrentMethod().DeclaringType.Name;


        private readonly IAutoloadRepository _autoloadRepository;
        private readonly IFileStoreFactory _fileStoreFactory;
        private readonly IQueue<AutoLoadResultMessage> _autoloadResultQueue;
        private readonly IFileStorage _autoloadFileStore;
        private readonly IBuildRequestRepository _buildRequestRepository;
        private readonly ICredentialRepository _credentialRepository;
        private readonly IQueueFactory _queueFactory;
        private readonly ICredentialedSiteRepository _credentialedSiteRepository;

        

        public AutoLoadNewInventoryTask(IQueueFactory factory, IAutoloadRepository autoloadRepository, IFileStoreFactory fileStoreFactory, IBuildRequestRepository buildRequestRepository, ICredentialRepository credentialRepository, ICredentialedSiteRepository credentialedSiteRepository)
            :base(factory.CreateAutoLoadNewInventory())
        {
            //subscribe to the buses
            var inventoryBus = BusFactory.CreateNewInventory<NewInventoryMessage, NewInventoryMessage>();
            inventoryBus.Subscribe(ReadQueue);

            _autoloadRepository = autoloadRepository;
            _fileStoreFactory = fileStoreFactory;
            _autoloadFileStore = _fileStoreFactory.CreateAutoLoadAudit();
            _autoloadResultQueue = factory.CreateAutoLoadResult();
            _buildRequestRepository = buildRequestRepository;
            _credentialRepository = credentialRepository;
            _credentialedSiteRepository = credentialedSiteRepository;

            _queueFactory = factory;
        }

        public override void Process(NewInventoryMessage message)
        {
			log4net.LogicalThreadContext.Properties["threadId"] = string.Format("BusinessUnitId: {0}, InventoryId: {1}", message.BusinessUnitId, message.InventoryId);
            var businessUnit = BusinessUnitFinder.Instance().Find(message.BusinessUnitId);
            var settings = GetMiscSettings.GetSettings(message.BusinessUnitId);

            // must have dealer upgrade 
            if(!settings.HasSettings || !businessUnit.HasDealerUpgrade(Upgrade.Merchandising))
                return;

            Log.DebugFormat("AutoLoadNewInventoryTask: InventoryData.Fetch complete : Got InventoryData Auto Load valid vehicle: BU = {0}, InvID = {1}", message.BusinessUnitId, message.InventoryId);
            var inventoryData = InventoryData.Fetch(message.BusinessUnitId, message.InventoryId);

            // check if an audit file exist prior to fetching the same data again
            if (_buildRequestRepository.LoadDataExists(_autoloadFileStore, inventoryData.VIN))
            {
                CloudWatch.Instance.IncrementMetric(1, "AutoLoadNew-Data-Exists");
                Log.DebugFormat("AutoLoadNewInventoryTask: Auto Load audit file found: BU = {0}, InvID = {1}", message.BusinessUnitId, message.InventoryId);
                
                // a good xml exist in the audit, now skip the fetch task and load an autoload result message as if
                // the fetchAgent pulled the file
                
                string xmlString = AutoLoadAudit.Fetch(_autoloadFileStore, inventoryData.VIN);
                var manager = new AutoloadManager(_autoloadRepository);

                var agent = _buildRequestRepository.DetermineAgent(xmlString);
                
                manager.SetAutoloadStart(message.BusinessUnitId, message.InventoryId, DeclaringTypeName);

                _autoloadResultQueue.SendMessage(new AutoLoadResultMessage(message.BusinessUnitId, message.InventoryId, xmlString, Status.Successful, agent, inventoryData.InventoryType), GetType().FullName);

            }
            else
            {
                CloudWatch.Instance.IncrementMetric(1, "AutoLoadNew-Data-None");

                // no data in autoload audit for this VIN, have to request data from somewhere ....
                var foundSite = false;

                var buildRequestSources = _credentialedSiteRepository.FetchBuildRequestSites(inventoryData);
                foreach (var buildRequestSource in buildRequestSources)
                {
                    if (!buildRequestSource.DealerCredentialsRequired)
                    {
                        // send open
                        SendOpen(inventoryData, buildRequestSource);

                        // we are good
                        foundSite = true;

                    }
                    else
                    {
                        // see if creds are available
                        var encryptedSiteCredential = _credentialRepository.Fetch(inventoryData.BusinessUnitID, (Agent) buildRequestSource.SiteId);
                        if (encryptedSiteCredential != null)
                        {
                            // send
                            SendCredentialed(inventoryData, encryptedSiteCredential, buildRequestSource);

                            // we are good
                            foundSite = true;

                        }

                    }

                    if (foundSite)
                        break;
                }

                // couldn't find an open of credentialed site
                if (!foundSite) 
                {
                    // send VDS
                    CloudWatch.Instance.IncrementMetric(1, "AutoLoadNew-Creds-VDS");
                    var loadFromVdsDataCommand = new LoadFromVdsDataCommand(_queueFactory, inventoryData);
                    loadFromVdsDataCommand.Run();

                }
            }
        }

        private void SendOpen(InventoryData inventoryData, CredentialSite credentialSite)
        {
            
            if(credentialSite.BuildRequestTypeId == 3) // honda econfig
            {
                var requestQueue = _queueFactory.CreateHondaEconfigRequestQueue();

                // start autoload
                _buildRequestRepository.SetAutoloadStart(inventoryData.BusinessUnitID, inventoryData.InventoryID, DeclaringTypeName);
                
                requestQueue.SendMessage(
                        new HondaEconfigBuildRequestMessage
                            {BusinessUnitId = inventoryData.BusinessUnitID, 
                            InventoryId =  inventoryData.InventoryID,
                            Vin = inventoryData.VIN,
                            Make = inventoryData.Make}, 
                        DeclaringTypeName);

                Log.DebugFormat("AutoLoadNewInventoryTask: sending HondaEconfig request for:  BU: {0}, InvID: {1}, VIN: {2}", inventoryData.BusinessUnitID, inventoryData.InventoryID, inventoryData.VIN);
            }
            else
            {
                Log.ErrorFormat("AutoLoadNewInventoryTask: did not send request :  BU: {0}, InvID: {1}, VIN: {2} because of invalid build request type {3}", inventoryData.BusinessUnitID, inventoryData.InventoryID, inventoryData.VIN, credentialSite.BuildRequestTypeId);
            }



        }

        private void SendCredentialed(InventoryData inventoryData, EncryptedSiteCredential encryptedSiteCredential, CredentialSite credentialSite)
        {
            if (credentialSite.BuildRequestTypeId == 2) // slurpee!!
            {

                if (!Convert.ToBoolean(credentialSite.BatchProcessOnly))
                {

                    // start autoload
                    _buildRequestRepository.SetAutoloadStart(inventoryData.BusinessUnitID, inventoryData.InventoryID, DeclaringTypeName);

                    // send request message to slurpee topic
                    _buildRequestRepository.SendSlurpeeRequest(inventoryData.BusinessUnitID, inventoryData.VIN, encryptedSiteCredential);

                    // log message
                    Log.DebugFormat("AutoLoadNewInventoryTask: called send slurpee request :  BU: {0}, InvID: {1}, VIN: {2}", inventoryData.BusinessUnitID, inventoryData.InventoryID, inventoryData.VIN);
                    
                }
                else
                {

                    // start autoload
                    _buildRequestRepository.SetAutoloadStart(inventoryData.BusinessUnitID, inventoryData.InventoryID, DeclaringTypeName);

                    // we still want to get this in the queue.  use this code to "batch" the new vehicles
                    // new vehicles need in queue so maxDecoder doesn't to AutoLoadStatus = 100 and no autoload happens
                    _buildRequestRepository.SetAutoloadEnd(inventoryData.BusinessUnitID, inventoryData.InventoryID, Status.Delayed, DeclaringTypeName);

                    Log.DebugFormat("AutoLoadNewInventoryTask: did NOT call send slurpee request :  BU: {0}, InvID: {1}, VIN: {2} because this agent configured for batch processing only", inventoryData.BusinessUnitID, inventoryData.InventoryID, inventoryData.VIN);
                }
            }
            else
            {
                Log.ErrorFormat("AutoLoadNewInventoryTask: did not send request :  BU: {0}, InvID: {1}, VIN: {2} because of invalid build request type {3}", inventoryData.BusinessUnitID, inventoryData.InventoryID, inventoryData.VIN, credentialSite.BuildRequestTypeId);
            }

        }

    }
}
