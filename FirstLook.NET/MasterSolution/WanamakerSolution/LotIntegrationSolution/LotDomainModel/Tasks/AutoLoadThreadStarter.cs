﻿using System;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.DataAccess;
using MAX.BuildRequest;
using MAX.BuildRequest.Tasks;
using MAX.ExternalCredentials;
using MAX.Threading;
using Merchandising.Messages;

namespace FirstLook.LotIntegration.DomainModel.Tasks
{
    internal class AutoLoadThreadStarter : IAutoLoadThreadStarter
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IQueueFactory _queueFactory;
        private readonly IAutoloadRepository _autoloadRepository;
        private readonly IAdMessageSender _adMessageSender;
        private readonly IFileStoreFactory _fileStoreFactory;
        private readonly IVehicleReferenceDataRepository _vehicleReferenceDataRepository;
        private readonly IBuildRequestRepository _buildRequestRepository;
        private readonly ICredentialRepository _credentialRepository;
        private readonly ICredentialedSiteRepository _credentialedSiteRepository;

        public AutoLoadThreadStarter(IQueueFactory queueFactory, IAutoloadRepository autoloadRepository, IAdMessageSender adMessageSender, IFileStoreFactory fileStoreFactory, IVehicleReferenceDataRepository vehicleReferenceDataRepository, IBuildRequestRepository buildRequestRepository, ICredentialRepository credentialRepository, ICredentialedSiteRepository credentialedSiteRepository)
        {
            _queueFactory = queueFactory;
            _autoloadRepository = autoloadRepository;
            _adMessageSender = adMessageSender;
            _fileStoreFactory = fileStoreFactory;
            _vehicleReferenceDataRepository = vehicleReferenceDataRepository;
            _buildRequestRepository = buildRequestRepository;
            _credentialRepository = credentialRepository;
            _credentialedSiteRepository = credentialedSiteRepository;
        }

        public void Run(Func<bool> shouldCancel, Func<bool> blockWhileNoMessages )
        {
            Log.Info("AutoLoad threads starting.");

            NamedBackgroundThread.Start("Worker - AutoLoadInventory",
                () => new AutoLoadInventoryTask(_queueFactory, _autoloadRepository, _fileStoreFactory, _buildRequestRepository, _credentialRepository, _credentialedSiteRepository).Run(shouldCancel, blockWhileNoMessages));

            NamedBackgroundThread.Start("Worker - AutoLoadResult",
                () => new AutoLoadResultTask(_queueFactory, _autoloadRepository, _adMessageSender, _vehicleReferenceDataRepository, _buildRequestRepository).Run(shouldCancel, blockWhileNoMessages));

            NamedBackgroundThread.Start("Worker - AutoLoadNewInventory",
                () => new AutoLoadNewInventoryTask(_queueFactory, _autoloadRepository, _fileStoreFactory, _buildRequestRepository, _credentialRepository, _credentialedSiteRepository).Run(shouldCancel, blockWhileNoMessages));

            NamedBackgroundThread.Start("Worker - Honda Econfig Request Task",
                () => new HondaEconfigRequestTask(_queueFactory, _buildRequestRepository).Run(shouldCancel, blockWhileNoMessages));

            NamedBackgroundThread.Start("Worker - VinDecodedMessageProcessor",
                () => new VinDecodedMessageProcessor(_queueFactory, _adMessageSender, _autoloadRepository).Run(shouldCancel, blockWhileNoMessages));

            NamedBackgroundThread.Start("Worker - SlurpeeResultTask",
                () => new SlurpeeResultTask(_queueFactory, _buildRequestRepository).Run(shouldCancel, blockWhileNoMessages));

            NamedBackgroundThread.Start("Worker - FetchAudit",
                () => new FetchAuditTask(_queueFactory, _fileStoreFactory.CreateAutoLoadAudit(), _buildRequestRepository).Run(shouldCancel, blockWhileNoMessages));
            
        }
    }
}
