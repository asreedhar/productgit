﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using AmazonServices.CW;
using Core.Messaging;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using MAX.BuildRequest;
using MAX.ExternalCredentials;
using Merchandising.Messages;
using Merchandising.Messages.AutoLoad;
using Merchandising.Messages.Slurpee;

namespace FirstLook.LotIntegration.DomainModel.Tasks
{
    internal class AutoLoadInventoryTask : TaskRunner<AutoLoadInventoryMessage>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly string DeclaringTypeName = MethodBase.GetCurrentMethod().DeclaringType.Name;

        private readonly AutoloadManager _autoloadManager;
        private readonly IFileStoreFactory _fileStoreFactory;
        private readonly IFileStorage _autoloadFileStore;
        private readonly IQueue<AutoLoadResultMessage> _autoloadResultQueue;
        private readonly IBuildRequestRepository _buildRequestRepository;
        private readonly ICredentialRepository _credentialRepository;
        private readonly IQueueFactory _queueFactory;
        private readonly ICredentialedSiteRepository _credentialedSiteRepository;


        

        public AutoLoadInventoryTask(IQueueFactory factory, IAutoloadRepository autoloadRepository,
                                     IFileStoreFactory fileStoreFactory, IBuildRequestRepository buildRequestRepository,
                                     ICredentialRepository credentialRepository,
                                     ICredentialedSiteRepository credentialedSiteRepository)
            : base(factory.CreateAutoLoadInventory())
        {
            _autoloadManager = new AutoloadManager(autoloadRepository);
            _fileStoreFactory = fileStoreFactory;
            _autoloadFileStore = _fileStoreFactory.CreateAutoLoadAudit();
            _autoloadResultQueue = factory.CreateAutoLoadResult();
            _buildRequestRepository = buildRequestRepository;
            _credentialRepository = credentialRepository;
            _credentialedSiteRepository = credentialedSiteRepository;

            _queueFactory = factory;
            
        }

        private AutoLoadInventory[] GetInventory(AutoLoadInventoryMessage message)
        {
            var list = new AutoLoadInventory[0];

            if (message.Type == AutoLoadInventoryType.NoStatus)
            {
                list = _autoloadManager.GetInventoryWithNoAutoloadStatus(message.BusinessUnitId)
                    .Select(x => new AutoLoadInventory {BusinessUnitId = message.BusinessUnitId, InventoryId = x})
                    .ToArray();
            }
            else if (message.Type == AutoLoadInventoryType.Errors)
            {
                list = _autoloadManager.GetAllWithErrors(message.BusinessUnitId)
                    .Select(x => new AutoLoadInventory {BusinessUnitId = message.BusinessUnitId, InventoryId = x})
                    .ToArray();
            }
            else if (message.Type == AutoLoadInventoryType.AgentErrors)
            {
                list = _autoloadManager.GetAllWithErrors();
            }
            else if (message.Type == AutoLoadInventoryType.SlurpeeRequeue)
            {
                list = _autoloadManager.GetSlurpeeRequeues(message.BusinessUnitId)
                    .Select(x => new AutoLoadInventory {BusinessUnitId = message.BusinessUnitId, InventoryId = x})
                    .ToArray();
            }else if(message.Type == AutoLoadInventoryType.OpenRequeue)
            {
                list = _autoloadManager.GetOpenRequeue(message.BusinessUnitId);

                Log.DebugFormat("AutoLoadInventoryTask found {0} Open Autoload row(s) to process", list.Count());

                int requestLimit;

                if (!int.TryParse(ConfigurationManager.AppSettings["OpenRequeueLimit"], out requestLimit))
                    requestLimit = -1;

                if (requestLimit >= 0)
                {
                    list = list.Take(requestLimit).ToArray();
                    Log.DebugFormat("AutoLoadInventoryTask has limted Open Autoload vehicle(s) to: {0}", list.Count());
                }
            }

            return list;
        }

        public override void Process(AutoLoadInventoryMessage message)
        {
            var buildRequestBatch = new List<BuildRequestBatch>();

            Log.DebugFormat("AutoLoadInventoryTask called, Get list of inventory to be autoloaded. BusinessUnitID = {0}",message.BusinessUnitId);

            var inventoryList = GetInventory(message);

            Log.DebugFormat("AutoLoadInventoryTask called, Got the list of inventory to be autoloaded . Total count : {1} . BusinessUnitID = {0}", message.BusinessUnitId, inventoryList.Length);

            foreach (var autoLoadInventory in inventoryList)
            {
                log4net.LogicalThreadContext.Properties["threadId"] =
                    string.Format("BusinessUnitId: {0}, Agent: {1}, AutoLoad Type: {2}", autoLoadInventory.BusinessUnitId,
                                  message.Agent, message.Type);

                var inventoryData = InventoryData.Fetch(autoLoadInventory.BusinessUnitId, autoLoadInventory.InventoryId);

                // check if an audit file exist prior to fetching the same data again
                if (_buildRequestRepository.LoadDataExists(_autoloadFileStore, inventoryData.VIN))
                {
                    CloudWatch.Instance.IncrementMetric(1, "AutoLoad-Data-Exists");
                    Log.DebugFormat("AutoLoadInventoryTask: Auto Load audit file found: BU = {0}, InvID = {1}",
                                    inventoryData.BusinessUnitID, inventoryData.InventoryID);

                    // a good xml exist in the audit, now skip the fetch task and load an autoload result message as if the fetchAgent pulled the file
                    string xmlString = AutoLoadAudit.Fetch(_autoloadFileStore, inventoryData.VIN);

                    _autoloadManager.SetAutoloadStart(inventoryData.BusinessUnitID, inventoryData.InventoryID, DeclaringTypeName);

                    var agent = _buildRequestRepository.DetermineAgent(xmlString);
                    
                    _autoloadResultQueue.SendMessage(
                                new AutoLoadResultMessage(
                                                            inventoryData.BusinessUnitID, inventoryData.InventoryID, xmlString,
                                                            Status.Successful, agent, inventoryData.InventoryType
                                                        ),
                                                    GetType().FullName);

                }
                else
                {
                    CloudWatch.Instance.IncrementMetric(1, "AutoLoad-Data-None");

                    // no data in autoload audit for this VIN, have to request data from somewhere ....
                    var foundSite = false;

                    var buildRequestSources = _credentialedSiteRepository.FetchBuildRequestSites(inventoryData);
                    foreach (var buildRequestSource in buildRequestSources)
                    {
                        if (!buildRequestSource.DealerCredentialsRequired)
                        {
                            // send open
                            SendOpen(inventoryData, buildRequestSource);

                            // we are good
                            foundSite = true;

                        }
                        else
                        {
                            // see if creds are available
                            var encryptedSiteCredential = _credentialRepository.Fetch(inventoryData.BusinessUnitID, (Agent) buildRequestSource.SiteId);
                            if (encryptedSiteCredential != null)
                            {
                                // send (or batch)
                                if (encryptedSiteCredential.BuildRequestType == 2) // slurpee!!
                                {
                                    // add to the batch for batch processing
                                    var site = buildRequestBatch.Find(x => x.EncryptedSiteCredential.SiteId == encryptedSiteCredential.SiteId
                                                                        && x.EncryptedSiteCredential.BusinessUnitId == encryptedSiteCredential.BusinessUnitId);

                                    if (site != null)
                                        site.Vehicles.Add(new BuildRequestVehicle { InventoryId = inventoryData.InventoryID, Vin = inventoryData.VIN });
                                    else
                                    {
                                        buildRequestBatch.Add(new BuildRequestBatch
                                        {
                                            EncryptedSiteCredential = encryptedSiteCredential,
                                            Vehicles =
                                                new List<BuildRequestVehicle> { new BuildRequestVehicle { InventoryId = inventoryData.InventoryID, Vin = inventoryData.VIN } }
                                        });
                                    }
                                }

                                // we are good
                                foundSite = true;

                            }

                        }

                        if (foundSite)
                            break;
                    }

                    // couldn't find an open of credentialed site
                    if (!foundSite) 
                    {
                        // send VDS
                        CloudWatch.Instance.IncrementMetric(1, "AutoLoadNew-Creds-VDS");
                        var loadFromVdsDataCommand = new LoadFromVdsDataCommand(_queueFactory, inventoryData);
                        loadFromVdsDataCommand.Run();

                    }
                    
                }
            }

            // batch and send Slurpee request that have accummulated
            SendBuildRequestBatches(buildRequestBatch);

        }

        private void SendOpen(InventoryData inventoryData, CredentialSite credentialSite)
        {

            if (credentialSite.BuildRequestTypeId == 3) // honda econfig
            {
                var requestQueue = _queueFactory.CreateHondaEconfigRequestQueue();

                // start autoload
                _buildRequestRepository.SetAutoloadStart(inventoryData.BusinessUnitID, inventoryData.InventoryID, DeclaringTypeName);

                requestQueue.SendMessage(
                        new HondaEconfigBuildRequestMessage
                        {
                            BusinessUnitId = inventoryData.BusinessUnitID,
                            InventoryId = inventoryData.InventoryID,
                            Vin = inventoryData.VIN,
                            Make = inventoryData.Make
                        },
                        DeclaringTypeName);

                    
                Log.DebugFormat("AutoLoadInventoryTask: sending HondaEconfig request for:  BU: {0}, InvID: {1}, VIN: {2}", inventoryData.BusinessUnitID, inventoryData.InventoryID, inventoryData.VIN);

            }
            else
            {
                Log.ErrorFormat("AutoLoadInventoryTask: did not send request :  BU: {0}, InvID: {1}, VIN: {2} because of invalid build request type {3}", inventoryData.BusinessUnitID, inventoryData.InventoryID, inventoryData.VIN, credentialSite.BuildRequestTypeId);
            }



        }



        /// <summary>
        /// breaks the batches into calls and set limits
        /// </summary>
        private void SendBuildRequestBatches(IEnumerable<BuildRequestBatch> buildRequestBatch)
        {

            // loop through the site and build calls to slurpee
            foreach (var requestBatch in buildRequestBatch)
            {

                // set site based settings
                var credSite = _credentialedSiteRepository.Fetch((int) requestBatch.EncryptedSiteCredential.SiteId);
                int maxVehiclesPerCall = credSite.MaxRequestsPerCall;
                int maxCallsPerBatch = credSite.MaxCallsPerBatch ?? 1000000;
                int numberOfCalls = 0;

                var currentVehicles = new List<BuildRequestVehicle>();
                foreach (var vehicle in requestBatch.Vehicles)
                {

                    // build up batches for this site
                    currentVehicles.Add(vehicle);

                    // check for max vehicles per call ... send and clear
                    if (currentVehicles.Count == maxVehiclesPerCall)
                    {

                        SendBatch(new BuildRequestBatch
                            {
                                EncryptedSiteCredential = requestBatch.EncryptedSiteCredential,
                                Vehicles = currentVehicles
                            }
                            );

                        currentVehicles.Clear();

                        // track and break on max number of calls
                        numberOfCalls++;
                        if (numberOfCalls >= maxCallsPerBatch)
                            break;
                    }

                }

                // if there is anything left in the batch ... send it (basically of maxes were not hit)
                if (currentVehicles.Count > 0)
                {
                    SendBatch(new BuildRequestBatch
                        {
                            EncryptedSiteCredential = requestBatch.EncryptedSiteCredential,
                            Vehicles = currentVehicles
                        });
                }

            }

        }
        
        /// <summary>
        /// just sends a batch that has been "batched"
        /// </summary>
        /// <param name="batch"></param>
        private void SendBatch(BuildRequestBatch batch)
        {

            // slurpee credential call back URL
            var baseUrl = ConfigurationManager.AppSettings["credentialServiceBaseUrl"];

            if (string.IsNullOrWhiteSpace(baseUrl))
            {
                Log.Error(
                    "AutoLoadInventoryTask: cannot call slurpee request because the 'credentialServiceBaseUrl' is not set in the app.config");
                return;
            }


            // just making the slupree call
            var messageId = _buildRequestRepository.SendSlurpeeRequest(
                new SlurpeeRequestMessage
                    {
                        bu_id = batch.EncryptedSiteCredential.BusinessUnitId,
                        site_id = (int)batch.EncryptedSiteCredential.SiteId,
                        vin = string.Join(",", batch.Vehicles.Select(x => x.Vin)),
                        pass_word = batch.EncryptedSiteCredential.Password,
                        user_name = batch.EncryptedSiteCredential.UserName,
                        correlation_id = Guid.NewGuid().ToString(),
                        dealer_code = batch.EncryptedSiteCredential.DealerCode,
                        creds_fail_webhook = _buildRequestRepository.GetSlurpeeCallbackUrl(baseUrl, "fail",
                                                                          batch.EncryptedSiteCredential.CredentialHash,
                                                                          batch.EncryptedSiteCredential.
                                                                              CredentialVersion),
                        creds_pass_webhook = _buildRequestRepository.GetSlurpeeCallbackUrl(baseUrl, "pass",
                                                                          batch.EncryptedSiteCredential.CredentialHash,
                                                                          batch.EncryptedSiteCredential.
                                                                              CredentialVersion),
                        creds_proceed_webhook = _buildRequestRepository.GetSlurpeeCallbackUrl(baseUrl, "status",
                                                                          batch.EncryptedSiteCredential.CredentialHash,
                                                                          batch.EncryptedSiteCredential.
                                                                              CredentialVersion)
                    });

            foreach (var vehicle in batch.Vehicles)
            {
                // start autoload
                _buildRequestRepository.SetAutoloadStart(batch.EncryptedSiteCredential.BusinessUnitId,
                                                         vehicle.InventoryId, DeclaringTypeName);

                //    log message
                Log.DebugFormat("AutoLoadInventoryTask: called send slurpee request :  BU: {0}, InvID: {1}, VIN: {2} Message ID: {3}",
                                batch.EncryptedSiteCredential.BusinessUnitId, vehicle.InventoryId, vehicle.Vin, messageId);

            }

        }
    }

    public class BuildRequestBatch
    {
        public EncryptedSiteCredential EncryptedSiteCredential { get; set; }
        public List<BuildRequestVehicle> Vehicles { get; set; }

    }

    public class BuildRequestVehicle
    {
        public int InventoryId { get; set; }
        public string Vin { get; set; }
    }
}

