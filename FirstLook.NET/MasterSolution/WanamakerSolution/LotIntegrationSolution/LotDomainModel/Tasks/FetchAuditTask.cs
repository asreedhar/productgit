﻿using Core.Messaging;
using MAX.BuildRequest;
using Merchandising.Messages;
using Merchandising.Messages.AutoLoad;

namespace FirstLook.LotIntegration.DomainModel.Tasks
{
    internal class FetchAuditTask : TaskRunner<FetchAuditMessage>
    {
        private readonly IFileStorage _autoloadAuditStore;
        private readonly IBuildRequestRepository _buildRequestRepository;

        public FetchAuditTask(IQueueFactory factory, IFileStorage autoloadAuditStore, IBuildRequestRepository buildRequestRepository)
            : base(factory.CreateAutoLoadAudit())
        {
            _autoloadAuditStore = autoloadAuditStore;
            _buildRequestRepository = buildRequestRepository;
        }

        public override void Process(FetchAuditMessage message)
        {
            if (!string.IsNullOrEmpty(message.Xml))
            {
                // don't overwrite file if the file is not valid (only save sucessful fetch OR nothing exist)
                if (_buildRequestRepository.IsValidBuildRequest(message.Xml) || !AutoLoadAudit.Exists(_autoloadAuditStore, message.Vin))
                    AutoLoadAudit.Audit(_autoloadAuditStore, message.Vin, message.Xml);    

            }
        }
    }
}
