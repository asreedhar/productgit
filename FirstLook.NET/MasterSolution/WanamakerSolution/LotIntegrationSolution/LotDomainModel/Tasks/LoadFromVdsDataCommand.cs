using System;
using System.Configuration;
using System.Globalization;
using System.Linq;
using Core.Messaging;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using MAX.VDS;
using Merchandising.Messages;
using Merchandising.Messages.AutoLoad;

namespace FirstLook.LotIntegration.DomainModel.Tasks
{
    internal class LoadFromVdsDataCommand
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IQueue<VinDecodedMessage> _vinDecodedQueue;
        private readonly InventoryData _inventoryData;
        private readonly VdsAutoLoadConfigParser _vdsAutoLoadConfigParser;

        private const string All = "*";

        internal LoadFromVdsDataCommand(IQueueFactory factory, InventoryData inventoryData)
        {
            _vinDecodedQueue = factory.CreateVinDecodedQueue();
            _inventoryData = inventoryData;
            _vdsAutoLoadConfigParser = new VdsAutoLoadConfigParser();
        }

        internal bool IsEnabled(string make, int businessUnitId, string vdsAutoLoadConfigItems)
        {
            var configItems = _vdsAutoLoadConfigParser.ParseVdsConfigItemsText(vdsAutoLoadConfigItems).ToList();

            // Note: * is taken to mean "all" and can be used for either make, store, or both.

            // *|*
            if (configItems.Any(i => i.Makes.Contains(All) && i.Stores.Contains(All)))
            {
                return true;
            }

            // *|123
            if (configItems.Any(i => i.Makes.Contains(All) && i.Stores.Contains(businessUnitId.ToString(CultureInfo.InvariantCulture), StringComparer.InvariantCultureIgnoreCase)))
            {
                return true;
            }

            // bmw|*
            if (configItems.Any(i => i.Makes.Contains(make, StringComparer.InvariantCultureIgnoreCase) && i.Stores.Contains(All)))
            {
                return true;
            }

            // bmw|123
            if (
                configItems.Any(
                    i =>
                        i.Makes.Contains(make, StringComparer.InvariantCultureIgnoreCase) &&
                        i.Stores.Contains(businessUnitId.ToString(CultureInfo.InvariantCulture), StringComparer.InvariantCultureIgnoreCase)))
            {
                return true;
            }

            return false;
        }

        internal void Run()
        {
            try
            {
                string vdsAutoLoadConfigItems = ConfigurationManager.AppSettings["VdsAutoloadConfigItems"];

                var isEnabled = IsEnabled(_inventoryData.Make, _inventoryData.BusinessUnitID, vdsAutoLoadConfigItems);
                if (!isEnabled)
                {
                    Log.InfoFormat("VDS AutoLoads are not yet enabled for make {0} at store {1}.", _inventoryData.Make, _inventoryData.BusinessUnitID);
                    return;
                }

                // Get VDS data.
                var vds = new VdsDataFacade();
                var vdsData = vds.GetChromeData(_inventoryData.VIN);

                if (!vdsData.ChromeStyleId.HasValue || !vdsData.CountryCode.HasValue)
                {
                    Log.InfoFormat("Chrome style id is '{0}' and country code is '{1}'. Cannot proceed.", vdsData.ChromeStyleId, vdsData.CountryCode);
                    return;                    
                }

                // Send a message to VinDecodedMessageProcessor
                var vinDecodedMessage = new VinDecodedMessage
                {
                    ChromeStyleId = vdsData.ChromeStyleId.Value,
                    CountryCode = vdsData.CountryCode.Value,
                    OptionCodes = vdsData.OptionCodes.ToArray(),
                    InteriorManufacturerColorCode = vdsData.InteriorColorCode,
                    InteriorManufacturerColorDesc = vdsData.InteriorColorDescription,
                    ExteriorManufacturerColorCode = vdsData.ExteriorColorCode,
                    ExteriorManufacturerColorDesc = vdsData.ExteriorColorDescription,
                    SourceDataUrl = "VDS",
                    SourceProcessName = "LoadFromVdsDataCommand",
                    TimeStamp = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture),
                    Vin = _inventoryData.VIN                
                };

                _vinDecodedQueue.SendMessage(vinDecodedMessage, GetType().FullName);

                Log.Info("VDS Autoload performed. VinDecodedMessage issued.");
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

        }
    }
}