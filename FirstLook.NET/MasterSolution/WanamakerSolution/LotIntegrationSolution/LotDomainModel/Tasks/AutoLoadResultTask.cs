﻿using Core.Messaging;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.DataAccess;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using MAX.BuildRequest;
using Merchandising.Messages;
using Merchandising.Messages.AutoLoad;

namespace FirstLook.LotIntegration.DomainModel.Tasks
{
    internal class AutoLoadResultTask : TaskRunner<AutoLoadResultMessage>
    { 
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IAutoloadRepository _autoloadRepository;
        private readonly IAdMessageSender _adMessageSender;
        private readonly IVehicleReferenceDataRepository _vehicleReferenceDataRepository;
        private readonly IBuildRequestRepository _buildRequestRepository;

        public AutoLoadResultTask(IQueueFactory factory, IAutoloadRepository autoloadRepository, IAdMessageSender adMessageSender, IVehicleReferenceDataRepository vehicleReferenceDataRepository, IBuildRequestRepository buildRequestRepository)
            : base(factory.CreateAutoLoadResult())
        {
            _autoloadRepository = autoloadRepository;
            _adMessageSender = adMessageSender;
            _vehicleReferenceDataRepository = vehicleReferenceDataRepository;
            _buildRequestRepository = buildRequestRepository;
        }

        public override void Process(AutoLoadResultMessage message)
        {
			// FB: 30552: changed fetch code to send only sucessfull fetches
            // leaving this code to handle messages in this queue until they are process ... or die
            
            log4net.LogicalThreadContext.Properties["threadId"] = string.Format("BusinessUnitId: {0}, InventoryId: {1}", message.BusinessUnitId, message.InventoryId);

            Log.DebugFormat("Processing Autoload Result: BU = {0}, InvID = {1}", message.BusinessUnitId, message.InventoryId);

            var manager = new AutoloadManager(_autoloadRepository);

            if(message.XmlResult ==  null) //no xml result. Either it is locked or the request errored
            {
                if(message.Status == Status.Pending) //here just for backwards compatability when message type changed. Should not be hit but for 10.1 release night.
                    manager.SetAutoloadEnd(message.BusinessUnitId, message.InventoryId, Status.NetworkError, AutoLoadHelper.User);
                else
                    manager.SetAutoloadEnd(message.BusinessUnitId, message.InventoryId, message.Status, AutoLoadHelper.User);
                
                return;
            }

            var vehicleDataProvider = _buildRequestRepository.GetVehicleDataProvider(message.Agent);

            TaskVehicleAutoLoader loader = manager.CreateAutoLoader(message.Agent, message.InventoryType, _adMessageSender, _vehicleReferenceDataRepository, message.XmlResult, vehicleDataProvider);

            var inventoryData = InventoryData.Fetch(message.BusinessUnitId, message.InventoryId);

            // set VIN in log4net thread
			log4net.LogicalThreadContext.Properties["threadId"] = string.Format("BusinessUnitId: {0}, InventoryId: {1}, VIN: {2}", message.BusinessUnitId, message.InventoryId, inventoryData.VIN);

            loader.LoadOntoVehicle(message.BusinessUnitId, new BuildDataVehicleRequest(inventoryData.InventoryID, inventoryData.Make, inventoryData.VIN, inventoryData.InventoryType, message.Agent));


        }
    }
}
