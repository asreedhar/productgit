﻿using MAX.Threading;

namespace FirstLook.LotIntegration.DomainModel.Tasks
{
    public interface IAutoLoadThreadStarter : IRunWithCancellation 
    {
    }
}
