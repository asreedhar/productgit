﻿using AmazonServices.CW;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using Merchandising.Messages;
using Merchandising.Messages.AutoLoad;

namespace FirstLook.LotIntegration.DomainModel.Tasks
{
    public  class AutoLoadHelper
    {
        public const string User = "max_user";

        //private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //public static bool CanAutoLoadVehicle(int businessUnitId, IInventoryData inventoryData, out FetchMessage fetchMessage, out IVehicleDataProvider dataProvider)
        //{
        //    var manager = new AutoloadManager(Registry.Resolve<IAutoloadRepository>());

        //    Log.DebugFormat("AutoLoadHelper.CanAutoLoadVehicle called for BU : {0}, InvId : {1}", businessUnitId,
        //                        inventoryData.InventoryID);
        //    fetchMessage = null;
        //    dataProvider = null;
            
        //    if (manager.MakeIsSupported(inventoryData.Make))
        //    {
        //        Log.DebugFormat("AutoLoadHelper.CanAutoLoadVehicle: Autoloadmanager.MakeIsSupported returned True for BU : {0}, InvId : {1}, make : {2}", businessUnitId,
        //                            inventoryData.InventoryID, inventoryData.Make);
        //        var providerManager = new VehicleDataProviderManager();


        //        Log.DebugFormat("AutoLoadHelper.CanAutoLoadVehicle: GetProvider  BU : {0}, InvId : {1}, make : {2}", businessUnitId,
        //                        inventoryData.InventoryID, inventoryData.Make);
        //        dataProvider = providerManager.GetProvider(businessUnitId, inventoryData.Make);

        //        if (dataProvider.HasCredentials)
        //        {

        //            Log.DebugFormat("AutoLoadHelper.CanAutoLoadVehicle: dataProvider.HasCredentials returned True  BU : {0}, InvId : {1}, make : {2}", businessUnitId,
        //                            inventoryData.InventoryID, inventoryData.Make);

        //            fetchMessage = dataProvider.CreateFetchMessage(businessUnitId, inventoryData.InventoryID, inventoryData.VIN, inventoryData.InventoryType);

        //            Log.DebugFormat("AutoLoadHelper.CanAutoLoadVehicle: Created new fetch message  BU : {0}, InvId : {1}, make : {2}", businessUnitId,
        //                            inventoryData.InventoryID, inventoryData.Make);
        //        }
        //        else
        //        {
        //            Log.DebugFormat("AutoLoadHelper.CanAutoLoadVehicle: dataProvider.HasUsableCredential returned False  BU : {0}, InvId : {1}, make : {2}", businessUnitId,
        //                                 inventoryData.InventoryID, inventoryData.Make);
        //        }
        //    }
        //    else
        //    {

        //        Log.DebugFormat("AutoLoadHelper.CanAutoLoadVehicle: AutoloadManager.MakeIsSupported returned False for BU return false for CanAutoLoadVehicle: {0}, InvId : {1}, make : {2}", businessUnitId,
        //                          inventoryData.InventoryID, inventoryData.Make);
        //    }

        //    return fetchMessage != null;
        //}

        //internal static TaskVehicleAutoLoader CreateAutoLoader(Agent agent, int inventoryType, XmlDocument xmlDocument)
        //{
        //    VehicleResponse vehicleResponse = FetchDotComAgent.GetVehicleBuildDataFromFetchXml(xmlDocument);

        //    TaskVehicleAutoLoader loader;
        //    if (agent == Agent.FordConnect)
        //    {
        //        if (inventoryType == 1)
        //            loader = new FordNewVehicleAutoLoader(vehicleResponse);
        //        else
        //            loader = new FordUsedVehicleAutoLoader(vehicleResponse);
        //    }
        //    else if (agent == Agent.MazdaDcs) // FB: 27766
        //    {
        //        loader = new MazdaVehicleAutoLoader(vehicleResponse);
        //    }
        //    else
        //    {
        //        loader = new TaskVehicleAutoLoader(vehicleResponse);
        //    }

        //    return loader;
        //}

    }
}
