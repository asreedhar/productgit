﻿using System.Collections.Generic;
using System.Linq;
using AutoLoad.FordNew.DataAccess;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.DataAccess;
using MAX.BuildRequest.VehicleDataProviders;
using Merchandising.Messages.VehicleBuild;

namespace FirstLook.LotIntegration.DomainModel
{
    internal class ChryslerVehicleAutoLoader: TaskVehicleAutoLoader
    {
        public ChryslerVehicleAutoLoader(IAutoloadRepository autoloadRepository, IAdMessageSender adMessageSender, IVehicleReferenceDataRepository vehicleReferenceDataRepository, string fileConent, IVehicleDataProvider vehicleDataProvider)
            : base(autoloadRepository, adMessageSender, vehicleReferenceDataRepository, fileConent, vehicleDataProvider)
        {
        }

        internal override Merchandising.DomainModel.Vehicles.ChromeStyle MatchStyle(VehicleBuildData vehicleBuild, List<ChromeStyle> chromeStyleList, List<ChromeOption> chromeOptionList)
        {
            var foundStyle = base.MatchStyle(vehicleBuild, chromeStyleList, chromeOptionList);

            // try by option kind 19 and model
            if (foundStyle == null)
            {

                // try to filter to by kind option id 19 - 'Quick-Order Package'
                var filteredOptionList = (from options in chromeOptionList
                                          join styles in chromeStyleList on options.StyleId equals styles.StyleId
                                          where
                                              styles.FullStyleCode == vehicleBuild.OemModelCode &&
                                              options.OptionKindId == 19
                                          select options).ToList();


                foundStyle = FindUniqueStyle(filteredOptionList.ToList(), vehicleBuild.UnfilteredOptionPackages, "model and quick-order pkg filtered Chrome options list");
            }

            return foundStyle;

        }

        internal override List<ChromeOption> MatchOption(List<ChromeOption> optionList, OptionPackage package)
        {
            // check the base
            var chromeOption = base.MatchOption(optionList, package);

            // if nothing ....
            if(!chromeOption.Any())
            {
                // try anything with a dash ("-") suffix and something else
                var searchCode = package.OemCode + "-";

                var foundList = (from optList in optionList
                                 where optList.OptionCode.StartsWith(searchCode)
                                 orderby optList.OptionCode
                                 select optList).ToList();
                                 

                if (foundList.Any())
                {
                    // if we find anything ... just load the first
                    chromeOption.Add(foundList.First());
                }

            }

            return chromeOption;
        }


    }
}
