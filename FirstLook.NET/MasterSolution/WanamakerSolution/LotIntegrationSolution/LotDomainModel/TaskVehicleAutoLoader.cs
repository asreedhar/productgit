﻿using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.DataAccess;
using MAX.BuildRequest.VehicleDataProviders;
using Merchandising.Messages;
using Merchandising.Messages.VehicleBuild;

namespace FirstLook.LotIntegration.DomainModel
{
    internal class TaskVehicleAutoLoader : VehicleAutoLoader
    {
        private readonly string _fileContent;

        public TaskVehicleAutoLoader(IAutoloadRepository autoloadRepository, IAdMessageSender adMessageSender, IVehicleReferenceDataRepository vehicleReferenceDataRepository, string fileContent, IVehicleDataProvider vehicleDataProvider)
            : base(autoloadRepository, adMessageSender, vehicleReferenceDataRepository, vehicleDataProvider)
        {
            _fileContent = fileContent;
        }

        protected override void SetEndBatchAutoload(AutoloadManager autoloadManager, int businessUnitId, int inventoryId, string processName)
        {
            autoloadManager.SetAutoloadEnd(businessUnitId, inventoryId, Status.Successful, processName);
        }

        /// <summary>
        /// place for desendants to mess with the vehilce build data before it is loaded
        /// </summary>
        /// <param name="vehicleBuildData"></param>
        /// <returns></returns>
        protected virtual VehicleBuildData PreProcessVehicleResponse(VehicleBuildData vehicleBuildData)
        {
            return vehicleBuildData;
        }

        internal override VehicleBuildData GetBuildData(int businessUnitId, int inventoryId, string processName, string vin, int inventoryType)
        {
            // get the vehicle build data from the file content
            var vehicleBuildData = DataProvider.GetVehicleBuildData(_fileContent);
            

            return PreProcessVehicleResponse(vehicleBuildData);
        }
    }
}
