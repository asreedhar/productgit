﻿using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.DataAccess;
using FirstLook.Merchandising.DomainModel.Vehicles;
using MAX.BuildRequest.VehicleDataProviders;
using Merchandising.Messages.VehicleBuild;

namespace FirstLook.LotIntegration.DomainModel
{
    internal class AudiVehicleAutoLoader : TaskVehicleAutoLoader
    {
        public AudiVehicleAutoLoader(IAutoloadRepository autoloadRepository, IAdMessageSender adMessageSender, IVehicleReferenceDataRepository vehicleReferenceDataRepository, string fileConent, IVehicleDataProvider vehicleDataProvider)
            : base(autoloadRepository, adMessageSender, vehicleReferenceDataRepository, fileConent, vehicleDataProvider)        
        {
        }
        //protected override void ApplyDynamicWhiteListOptions(int inventoryId, IChromeStyle inferredStyle, VehicleBuildData vehicleBuild)
        //{

        //    if (inferredStyle.ChromeStyleID > 0)
        //    {
        //        // create a list of strings that should be included
        //        var includePhraseList = new List<string> { "package", "collection", "group", "pack", "pkg" };

        //        // $199.00 per chad and michaela
        //        vehicleBuild.OptionPackages = CleanseOptions(vehicleBuild.UnfilteredOptionPackages,
        //                                                     inferredStyle.ChromeStyleID, 199.0m, includePhraseList);

        //    }

        //}
    }
}
