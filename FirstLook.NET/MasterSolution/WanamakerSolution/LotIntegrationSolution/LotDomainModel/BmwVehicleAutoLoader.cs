﻿using System;
using System.Linq;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.DataAccess;
using MAX.BuildRequest.VehicleDataProviders;
using Merchandising.Messages.VehicleBuild;

namespace FirstLook.LotIntegration.DomainModel
{
    class BmwVehicleAutoLoader : TaskVehicleAutoLoader
    {
        public BmwVehicleAutoLoader(IAutoloadRepository autoloadRepository, IAdMessageSender adMessageSender, IVehicleReferenceDataRepository vehicleReferenceDataRepository, string fileContent, IVehicleDataProvider vehicleDataProvider)
            : base(autoloadRepository, adMessageSender, vehicleReferenceDataRepository, fileContent, vehicleDataProvider)
        {
        }

        
        /// <summary>
        /// Custom code for adding on option for the transmission during autoload.
        ///     
        /// Once the transmission is loaded, a transmission category type should be set on builder.NewEquipmentMapping
        /// Once set on NewEquipmentMapping, it should be set in the GID file (FB 32306)
        /// 
        /// This process will
        ///     1. check to see if a transmission option has been set and made it throught the option sanitization
        ///     2. check to see if an option got 'filtered' out of unfiltered option
        ///     3  finds the 'standard' option and added into the options to be loaded
        /// 
        /// NOTE: This is very BMW/Mini specific.  There is actually an option that matches
        /// the text in the stardard table with the correct transmittion categories.
        /// </summary>
        /// <param name="vin"></param>
        /// <param name="inferredStyle"></param>
        /// <param name="vehicleBuild"></param>
        internal override void ApplyDynamicWhiteListOptions(string vin, Merchandising.DomainModel.Vehicles.IChromeStyle inferredStyle, VehicleBuildData vehicleBuild)
        {
            const int transmissionOptionKindId = 7; // option kind id from Chrome for transmission options

            // base call to sanitize options
            base.ApplyDynamicWhiteListOptions(vin, inferredStyle, vehicleBuild);

            // check for or try to add a transmission option for this vehicle
            if(inferredStyle.ChromeStyleID > 0)
            {
                bool foundTransmission = false;

                // Get all transmisstion options for this style
                var transmissionOptions = (from options in ChromeRepository.GetOptions(new[] { inferredStyle.ChromeStyleID }).ToList() // just a call to chrome to get all options for this style
                                           where options.OptionKindId == transmissionOptionKindId
                                           select options).ToList();
                

                // is there a transmission in sanitized options?
                var foundPackage = from sanitizedOptions in vehicleBuild.OptionPackages
                                   join trans in transmissionOptions on sanitizedOptions.OemCode equals trans.OptionCode
                                  select sanitizedOptions;


                if (foundPackage.Any())
                {
                    foundTransmission = true;
                    Log.DebugFormat("Autoload: Transmission already exists in sanitized options for VIN: {0}", vin);
                }

                
                if(!foundTransmission)  // no luck above, see if a trans option is in unfiltered options
                {
                    // is there a transmission in the unfilter options (might not have made it through santizing)
                    var matchedOptons = FilterOptions(transmissionOptions, vehicleBuild.UnfilteredOptionPackages); // match to chrome options

                    var foundOption = (from matched in matchedOptons
                                       join trans in transmissionOptions on matched.OptionCode equals trans.OptionCode
                                       select matched).ToList();

                    if(foundOption.Any()) 
                    {
                        // found one that was not santized, have to add it back in to set transCategoryType
                        vehicleBuild.OptionPackages.AddRange(from found in foundOption
                                                        select new OptionPackage(found.OptionCode, found.Pon));

                        var optionList = string.Join(",", foundOption.Select(x => x.OptionCode));

                        foundTransmission = true;
                        Log.DebugFormat("Autoload: Transmission option {0} already exists in unfiltered options for VIN: {1} and was re-added to the sanitized option list", optionList, vin);


                    }

                }

                if (!foundTransmission)// no luck above, see if the 'standard' transmission can be added
                {
                    // is there a transmission in this styles options that matches the standards for this vehicle
                    var standardOptions = ChromeRepository.GetStandards(new[] { inferredStyle.ChromeStyleID });

                    var standardTrans = (from trans in transmissionOptions
                                       where standardOptions.Any(so => so.Standard.ToUpper().Trim().Contains(trans.Pon.ToUpper().Trim())) 
                                       select trans).ToList();

                    if (standardTrans.Any())  
                    {
                        // found option that matched standards, have to add it back into sanitized options
                        vehicleBuild.OptionPackages.AddRange(from found in standardTrans
                                                             select new OptionPackage(found.OptionCode, found.Pon));

                        var optionList = string.Join(",", standardTrans.Select(x => x.OptionCode));

                        
                        foundTransmission = true;
                        Log.DebugFormat("Autoload: Transmission option {0} was pulled from standards for VIN: {1} and was added to the sanitized option list", optionList, vin);

                    }

                }
                
                if(!foundTransmission)
                {
                    Log.WarnFormat("Autoload: Transmission option cold not be found for VIN: {0} ", vin);
                }

            }
        }
    }
}
