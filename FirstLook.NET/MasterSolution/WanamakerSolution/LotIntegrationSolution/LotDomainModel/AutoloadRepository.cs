﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Data;
using FirstLook.Merchandising.DomainModel.Commands;
using Merchandising.Messages;

namespace FirstLook.LotIntegration.DomainModel
{
    internal class AutoloadRepository : IAutoloadRepository
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        internal AutoloadRepository()
        {

        }

        public void Save(AutoloadStatus status, string memberLogin)
        {
            List<string> messages;

            var isValid = status.IsValid(status, out messages);

            if (!isValid)
            {
                var message = messages == null || messages.Count == 0 ? "Invalid status" : string.Join(" ", messages);
                throw new Exception(message);
            }

            using (var con = Database.GetConnection(Database.Merchandising))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.AutoloadStatus#Save";

                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddWithValue(cmd, "BusinessUnitId", status.BusinessUnitId, DbType.Int32);

                    Database.AddWithValue(cmd, "InventoryId", status.InventoryId, DbType.Int32);

                    Database.AddWithValue(cmd, "StatusTypeId", status.StatusTypeId, DbType.Int32);

                    Database.AddWithValue(cmd, "startTime", status.StartTime, DbType.DateTime);

                    Database.AddWithValue(cmd, "endTime", status.EndTime, DbType.DateTime);

                    Database.AddWithValue(cmd, "MemberLogin", memberLogin, DbType.String);

                    cmd.ExecuteNonQuery();
                }
            }
        }



        public AutoloadStatus Fetch(int businessUnitId, int inventoryId)
        {
            using (var con = Database.GetConnection(Database.Merchandising))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.AutoloadStatus#FetchByInventoryId";

                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddWithValue(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);

                    Database.AddWithValue(cmd, "InventoryId", inventoryId, DbType.Int32);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            return PopulateStatusFromReader(reader);
                            
                        }
                    }
                }
            }
            return null;
        }

        public IEnumerable<AutoloadStatus> GetAllByStatusType(int businessUnitId, Status statusType)
        {

            var statusList = new List<AutoloadStatus>();

            using (var con = Database.GetConnection(Database.Merchandising))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.AutoloadStatus#FetchAllWithErrorsForBU";

                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddWithValue(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);

                    Database.AddWithValue(cmd, "StatusTypeId", (int) statusType, DbType.Int32);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var status = PopulateStatusFromReader(reader);

                            if (status != null)
                            {
                                status.BusinessUnitId = businessUnitId;
                                status.StatusTypeId = statusType;
                                statusList.Add(status);
                            }
                        }
                    }
                }
            }
            return statusList;
        }

        private static AutoloadStatus PopulateStatusFromReader(IDataReader rdr)
        {
            var status = new AutoloadStatus();
            if (rdr.HasColumn("businessUnitID")) status.BusinessUnitId = rdr.GetInt32(rdr.GetOrdinal("businessUnitID"));

            if (rdr.HasColumn("inventoryID")) status.InventoryId = rdr.GetInt32(rdr.GetOrdinal("inventoryID"));

            if (rdr.HasColumn("statusTypeID")) status.StatusTypeId = (Status) rdr.GetInt32(rdr.GetOrdinal("statusTypeId"));

            if (rdr.HasColumn("startTime")) status.StartTime = rdr.GetDateTime(rdr.GetOrdinal("startTime"));

            if (rdr.HasColumn("endTime") && rdr["endTime"] != DBNull.Value) status.EndTime = rdr.GetDateTime(rdr.GetOrdinal("endTime"));

            if (rdr.HasColumn("createdOn")) status.CreatedOn = rdr.GetDateTime(rdr.GetOrdinal("createdOn"));

            if (rdr.HasColumn("createdBy")) status.CreatedBy = rdr.GetString(rdr.GetOrdinal("createdBy"));

            if (rdr.HasColumn("lastUpdatedOn")) status.LastUpdatedOn = rdr.GetDateTime(rdr.GetOrdinal("lastUpdatedOn"));

            if (rdr.HasColumn("lastUpdatedBy")) status.LastUpdatedBy = rdr.GetString(rdr.GetOrdinal("lastUpdatedBy"));
            return status;

        }

        public AutoLoadInventory[] GetAllWithErrors()
        {
            var autoLoadInventory = new List<AutoLoadInventory>();

            using (var con = Database.GetConnection(Database.Merchandising))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.AutoloadStatus#FetchAllWithErrors";
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var status = PopulateStatusFromReader(reader);

                            if (status != null)
                                autoLoadInventory.Add(new AutoLoadInventory(){BusinessUnitId = status.BusinessUnitId, InventoryId = status.InventoryId});
                        }
                    }
                }
            }

            return autoLoadInventory.ToArray();
        }


        public IList<int> GetAllWithErrors(int businessUnitId)
        {
            var inventoryIdList = new List<int>();

            using (var con = Database.GetConnection(Database.Merchandising))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.AutoloadStatus#FetchAllWithErrorsForBU";

                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddWithValue(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var status = PopulateStatusFromReader(reader);

                            if (status != null)
                                inventoryIdList.Add(status.InventoryId);
                        }
                    }
                }
            }

            return inventoryIdList;
        }

        public List<int> GetInventoryWithNoAutoloadStatus(int businessUnitId)
        {
            var inventoryIdList = new List<int>();

            using (var con = Database.GetConnection(Database.Merchandising))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.AutoloadStatus#FetchAllWithNoStatus";

                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddWithValue(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var inventoryId = reader.GetInt32(reader.GetOrdinal("InventoryID"));

                            inventoryIdList.Add(inventoryId);
                        }
                    }
                }
            }

            return inventoryIdList;
        }

        public bool IsBatchAutoloadEnabled(int businessUnitId)
        {
            return GetMiscSettings.GetSettings(businessUnitId).BatchAutoload;
        }

        public  List<string> GetSupportedMakeList()
        {
            Log.DebugFormat("AutoloadRepository.GetSupportedMakeList Getting SupportedMakeList from Database. ");

            var supportedMakeList = new List<string>();

            using (IDbConnection con = Common.Core.Data.Database.Connection(Database.Merchandising))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "merchandising.VehicleAutoLoadSettingsByMake#Fetch";

                    cmd.CommandType = CommandType.StoredProcedure;

                    if (con.State != ConnectionState.Open) { con.Open(); }

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var isAutoLoadEnabled = reader.GetBoolean(reader.GetOrdinal("IsAutoLoadEnabled"));

                            if (isAutoLoadEnabled)
                            {
                                var make = reader.GetString(reader.GetOrdinal("Make"));

                                supportedMakeList.Add(make);

                                Log.DebugFormat("AutoloadRepository.GetSupportedMakeList : Adding {0} to supportedMakeList ", make);
                            }
                        }
                    }
                }
            }

            return supportedMakeList;
        }

        public  List<ManufacturerAutoLoadSettings> GetSupportedManufacturerList()
        {
            Log.DebugFormat("AutoloadRepository.GetSupportedManufacturerList Getting SupportedMakeList from Database. ");

            var supportedManufacturerList = new List<ManufacturerAutoLoadSettings>();

            using (IDbConnection con = Common.Core.Data.Database.Connection(Database.Merchandising))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "merchandising.VehicleAutoLoadSettings#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (con.State != ConnectionState.Open) { con.Open(); }
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var manufacturer = new ManufacturerAutoLoadSettings(reader);
                            
                            supportedManufacturerList.Add(manufacturer);

                            Log.DebugFormat("AutoloadRepository.GetSupportedMakeList : Adding name: {0}, IsAutoLoadEnabled : {1} to supportedManufacturerList ", manufacturer.Name, manufacturer.IsAutoLoadEnabled);

                        }
                    }
                }
            }

            return supportedManufacturerList;
        }

        public  void UpdateAutoLoadSettings(IEnumerable<int> manufacturerIdList)
        {
            var list = string.Join(",", manufacturerIdList.ToList().ConvertAll(i => i.ToString()).ToArray());
            using (IDbConnection con = Common.Core.Data.Database.Connection(Database.Merchandising))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "merchandising.VehicleAutoLoadSettings#Update";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.AddParameter("@manufacturerIdList", DbType.String, list);

                    if (con.State != ConnectionState.Open) { con.Open(); }
                    cmd.ExecuteNonQuery();
                }
            }
           
        }

        public List<int> GetSlurpeeRequeues(int businessUnitId)
        {
            var inventoryIdList = new List<int>();

            using (var con = Database.GetConnection(Database.Merchandising))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = @"[builder].[AutoloadStatus#FetchSlurpeeRequeue]";

                    Database.AddWithValue(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var inventoryId = reader.GetInt32(reader.GetOrdinal("InventoryID"));

                            inventoryIdList.Add(inventoryId);
                        }
                    }
                }
            }

            return inventoryIdList;
        }

        public List<AutoLoadInventory>  GetOpenRequeues(int? businessUnitId)
        {

            var vehicleList = new List<AutoLoadInventory>(); 

            using (var con = Database.GetConnection(Database.Merchandising))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = @"[builder].[AutoloadStatus#FetchOpenRequeue]";

                    if(businessUnitId.HasValue)
                        Database.AddWithValue(cmd, "businessUnitId", businessUnitId, DbType.Int32);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            vehicleList.Add(
                                    new AutoLoadInventory{
                                                            BusinessUnitId = Convert.ToInt32(reader["businessUnitId"]),
                                                            InventoryId = Convert.ToInt32(reader["inventoryId"])
                                                        });
                        }
                    }
                }
            }

            return vehicleList;


        }

    }
}
