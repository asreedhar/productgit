﻿
namespace FirstLook.LotIntegration.DomainModel
{
    public class AutoLoadInventory
    {
        public int BusinessUnitId { get; set; }
        public int InventoryId { get; set; }
    }
}
