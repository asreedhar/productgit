using System;
using System.Collections.Generic;
using System.Xml.Linq;
using MAX.BuildRequest.VehicleDataProviders;
using Merchandising.Messages;
using System.Linq;

namespace FirstLook.LotIntegration.DomainModel
{

    public class VehicleDataProviderFactory
    {

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //public static IVehicleDataProvider Fetch(Agent site)
        //{
        //    switch (site)
        //    {
        //        case Agent.DealerSpeed:
        //            return new DealerSpeedVehicleDataProvider {Site = site};
        //        case Agent.GmGlobalConnect:
        //            return new GmGlobalConnectVehicleDataProvider {Site = site };
        //        case Agent.DealerConnect:
        //            return new DealerConnectVehicleDataProvider {Site = site };
        //        case Agent.VolkswagenHub:
        //            return new VolkswagenHubDataProvider {Site = site };
        //        case Agent.MazdaDcs:
        //            return new MazdaDcsVehicleDataProvider {Site = site };
        //        case Agent.AccessAudi:
        //            return new AccessAudiVehicleDataProvider {Site = site };
        //        case Agent.HyundaiTechInfo:
        //            return new HyundaiTechInfoVehicleDataProvider {Site = site };
        //        case Agent.NatDealerDaily:
        //            return new DealerDailyVehicleDataProvider {Site = site };
        //        case Agent.SetDealerDaily:
        //            return new DealerDailyVehicleDataProvider {Site = site };

        //        default:
        //            throw new ApplicationException(string.Format("No provider available for agent:{0}", site));
        //    }

        //}
    }

    public enum Make
    {
        MINI,
        BMW,
        Chevrolet,
        GMC,
        Pontiac,
        Buick,
        Cadillac,
        Hummer,
        Saturn,
        Chrysler,
        Dodge,
        Ram,
        Jeep,
        Mazda,
        Volkswagen,
        Ford, // needed for ford direct autoload
        Oldsmobile,
        Geo,
        Fiat,
        Plymouth,
        Audi,
        Hyundai,
        Lexus,
        Toyota,
        Scion
    }
}
