﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.DataAccess;
using MAX.BuildRequest.VehicleDataProviders;
using Merchandising.Messages.VehicleBuild;

namespace FirstLook.LotIntegration.DomainModel
{
    internal class GeneralMotorsVehicleAutoLoader: TaskVehicleAutoLoader
    {
        public GeneralMotorsVehicleAutoLoader(IAutoloadRepository autoloadRepository, IAdMessageSender adMessageSender, IVehicleReferenceDataRepository vehicleReferenceDataRepository, string fileConent, IVehicleDataProvider vehicleDataProvider)
            : base(autoloadRepository, adMessageSender, vehicleReferenceDataRepository, fileConent, vehicleDataProvider)
        {
        }

        internal override Merchandising.DomainModel.Vehicles.ChromeStyle MatchStyle(VehicleBuildData vehicleBuild, List<AutoLoad.FordNew.DataAccess.ChromeStyle> chromeStyleList, List<AutoLoad.FordNew.DataAccess.ChromeOption> chromeOptionList)
        {
            var foundStyle = base.MatchStyle(vehicleBuild, chromeStyleList, chromeOptionList);


            // try by option kind 16 and model
            if (foundStyle == null)
            {

                // try to filter to by kind option id 16 - Package Equipment Group
                var filteredOptionList = (from options in chromeOptionList
                                          join styles in chromeStyleList on options.StyleId equals styles.StyleId
                                          where
                                              styles.FullStyleCode == vehicleBuild.OemModelCode &&
                                              options.OptionKindId == 16
                                          select options).ToList();


                foundStyle = FindUniqueStyle(filteredOptionList.ToList(), vehicleBuild.UnfilteredOptionPackages, "model and PEG filtered Chrome options list");
            }

            // try by option kind 65 and model (Required option)
            if (foundStyle == null)
            {

                // try to filter to by kind option id 65 - Required Option
                var filteredOptionList = (from options in chromeOptionList
                                          join styles in chromeStyleList on options.StyleId equals styles.StyleId
                                          where
                                              styles.FullStyleCode == vehicleBuild.OemModelCode &&
                                              options.OptionKindId == 65
                                          select options).ToList();


                foundStyle = FindUniqueStyle(filteredOptionList.ToList(), vehicleBuild.UnfilteredOptionPackages, "model and Required option filtered Chrome options list");
            }

            // last shot, remove styles w/ Required options then filter by PEG options and model
            // this is for models WITHOUT the required option i.e. "AVF	- 2015 INTERIM PROCESSING CODE" for new 2015s
            if (foundStyle == null)
            {

                var filteredOptionList = (from options in chromeOptionList
                                          join styles in chromeStyleList on options.StyleId equals styles.StyleId
                                          where
                                              styles.FullStyleCode == vehicleBuild.OemModelCode &&
                                              options.OptionKindId == 16 &&
                                              !(    from options2 in chromeOptionList
                                                    where options2.OptionKindId == 65
                                                    select options2.StyleId
                                                ).Distinct().Contains(options.StyleId)
                                          select options).ToList();


                foundStyle = FindUniqueStyle(filteredOptionList.ToList(), vehicleBuild.UnfilteredOptionPackages, "model and PEG w/o styles w/ required options Chrome options list");
            }


            return foundStyle;

        }

    }
}
