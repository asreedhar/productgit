﻿using System.Collections.Generic;
using System.Linq;
using AutoLoad.FordNew.DataAccess;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.DataAccess;
using MAX.BuildRequest.VehicleDataProviders;
using Merchandising.Messages.VehicleBuild;

namespace FirstLook.LotIntegration.DomainModel
{
    internal class NissanVehicleAutoLoader : TaskVehicleAutoLoader
    {
        public NissanVehicleAutoLoader(IAutoloadRepository autoloadRepository, IAdMessageSender adMessageSender, IVehicleReferenceDataRepository vehicleReferenceDataRepository, string fileConent, IVehicleDataProvider vehicleDataProvider)
            : base(autoloadRepository, adMessageSender, vehicleReferenceDataRepository, fileConent, vehicleDataProvider)
        {
        }

        internal override List<ChromeOption> MatchOption(List<ChromeOption> optionList, OptionPackage package)
        {
            var matchedOptions = base.MatchOption(optionList, package);

            // special code to look for option code in PON
            if(matchedOptions.Count == 0)
            {
                matchedOptions = (from chomeOption in optionList
                                 where chomeOption.Pon.StartsWith("[" + package.OemCode + "]")
                                 select chomeOption).ToList();

            }

            // special code to look for option code in PON, in pre-2007 the were coded with parentheses, ie: "(<optionCode>)":
            if (matchedOptions.Count == 0)
            {
                matchedOptions = (from chomeOption in optionList
                                  where chomeOption.Pon.StartsWith("(" + package.OemCode + ")")
                                  select chomeOption).ToList();

            }

            return matchedOptions;
        }

    }


}
