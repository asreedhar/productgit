﻿
using System.Data;

namespace FirstLook.LotIntegration.DomainModel
{
    public class ManufacturerAutoLoadSettings
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public bool IsAutoLoadEnabled { get; private set; }

        public ManufacturerAutoLoadSettings(int id, string name, bool isAutoLoadEnabled)
        {
            Id = id;
            Name = name;
            IsAutoLoadEnabled = isAutoLoadEnabled;
        }

        internal ManufacturerAutoLoadSettings(IDataRecord reader)
        {
            Id = reader.GetInt32(reader.GetOrdinal("ManufacturerID"));
            Name = reader.GetString(reader.GetOrdinal("ManufacturerName"));
            IsAutoLoadEnabled = reader.GetBoolean(reader.GetOrdinal("IsAutoLoadEnabled"));
        }
    }
}
