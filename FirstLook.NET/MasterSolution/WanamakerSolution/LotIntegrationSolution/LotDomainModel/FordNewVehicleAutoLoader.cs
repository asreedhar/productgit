﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.DataAccess;
using MAX.BuildRequest.VehicleDataProviders;
using Merchandising.Messages.VehicleBuild;


namespace FirstLook.LotIntegration.DomainModel
{
    internal class FordNewVehicleAutoLoader : TaskVehicleAutoLoader
    {
        public FordNewVehicleAutoLoader(IAutoloadRepository autoloadRepository, IAdMessageSender adMessageSender, IVehicleReferenceDataRepository vehicleReferenceDataRepository, string fileConent, IVehicleDataProvider vehicleDataProvider)
            : base(autoloadRepository, adMessageSender, vehicleReferenceDataRepository, fileConent, vehicleDataProvider)
        {

        }

        protected override VehicleBuildData PreProcessVehicleResponse(VehicleBuildData vehicleBuildData)
        {
            vehicleBuildData = base.PreProcessVehicleResponse(vehicleBuildData);

            string commonPrefix = GetCommonPrefix(vehicleBuildData);
            ClearPrefix(vehicleBuildData, commonPrefix);


            return vehicleBuildData;
        }

        private static void ClearPrefix(VehicleBuildData data, string prefix)
        {
            foreach (var t in data.OptionPackages)
                t.OemCode = Regex.Replace(t.OemCode, prefix, String.Empty);

            data.InteriorColorCode = Regex.Replace(data.InteriorColorCode, prefix, String.Empty);
            data.ExteriorColorCode = Regex.Replace(data.InteriorColorCode, prefix, String.Empty);

            if (!String.IsNullOrEmpty(data.OemModelCode))
                data.OemModelCode = Regex.Replace(data.OemModelCode, prefix, String.Empty);
        }

        public static string GetCommonPrefix(VehicleBuildData data)
        {
            string commonPrefix = string.Empty;

            //Determine the common prefix and remove it so it matches up with chrome.
            if (data.OptionPackages.Count > 1) //at least two to compare
            {
                var optionPackages = data.OptionPackages
                    .Where(x => x.OemCode != String.Empty)
                    .OrderBy(x => x.OemCode.Length).ToList();

                commonPrefix = optionPackages[0].OemCode;

                bool partialMatch = true;
                while (commonPrefix.Length > 0 && partialMatch)
                {
                    partialMatch = false;
                    foreach (var package in optionPackages)
                    {
                        string subMatch = package.OemCode.Substring(0, commonPrefix.Length);

                        if (subMatch != commonPrefix)
                        {
                            commonPrefix = commonPrefix.Substring(0, subMatch.Length - 1);
                            partialMatch = true;
                            break;
                        }
                    }
                }
            }

            return commonPrefix;
        }

    }
}
