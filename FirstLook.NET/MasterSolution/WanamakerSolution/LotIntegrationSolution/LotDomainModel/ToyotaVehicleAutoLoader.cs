﻿using System.Collections.Generic;
using System.Linq;
using AutoLoad.FordNew.DataAccess;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.DataAccess;
using FirstLook.Merchandising.DomainModel.Vehicles;
using MAX.BuildRequest.VehicleDataProviders;
using Merchandising.Messages.VehicleBuild;

namespace FirstLook.LotIntegration.DomainModel
{
    internal class ToyotaVehicleAutoLoader : TaskVehicleAutoLoader
    {
        public ToyotaVehicleAutoLoader(IAutoloadRepository autoloadRepository, IAdMessageSender adMessageSender, IVehicleReferenceDataRepository vehicleReferenceDataRepository, string fileConent, IVehicleDataProvider vehicleDataProvider)
            : base(autoloadRepository, adMessageSender, vehicleReferenceDataRepository, fileConent, vehicleDataProvider)
        {
        }

        //protected override void ApplyDynamicWhiteListOptions(int inventoryId, IChromeStyle inferredStyle, VehicleBuildData vehicleBuild)
        //{

        //    if (inferredStyle.ChromeStyleID > 0)
        //    {
        //        // create a list of strings that should be included
        //        var includePhraseList = new List<string> { "package", "collection", "group", "pack", "pkg" };

        //        // $199.00 per chad and michaela
        //        vehicleBuild.OptionPackages = CleanseOptions(vehicleBuild.UnfilteredOptionPackages,
        //                                                     inferredStyle.ChromeStyleID, 199.0m, includePhraseList);

        //    }

        //}

        /// <summary>
        /// for Toyota, check merchandising.chrome.InventoryChromeStyle, then default to model code with national default
        /// </summary>
        /// <param name="vin"></param>
        /// <param name="vehicleBuild"></param>
        /// <returns></returns>
        internal override Merchandising.DomainModel.Vehicles.ChromeStyle DecodeStyle(string vin, VehicleBuildData vehicleBuild)
        {
            var foundStyle = base.DecodeStyle(vin, vehicleBuild);

            if(foundStyle == null) // any luck with default decoding??
            {
                // try inventoryChromeStyle view from merchandising.  whole bunch of stuff already done, including regional
                foundStyle = Merchandising.DomainModel.Vehicles.ChromeStyle.InventoryChromeStyle(vin);
            }

            // last shot, we know it is not SE from above, only two left GS and National ... default to Nation per everyone!!!
            if (foundStyle == null)
            {

                // find all the styles possible for this VIN (for the US)
                var styleList = ChromeRepository.GetStyles(vin).Where(x => x.CountryCode == 1).ToList();

                // default to national if everything else has not worked!!
                var foundStyleList = (from styles in styleList
                                     where styles.StyleCode == GetModelCode(vehicleBuild.OemModelCode) + "-N"
                                     select styles.StyleId).ToList();

                if (foundStyleList.Count == 1)
                {
                    foundStyle = Merchandising.DomainModel.Vehicles.ChromeStyle.ChromeStyleSelect(foundStyleList[0]);

                    Log.InfoFormat("Style decoded using {0}, style: {1} - {2} - {3}", " model plus -N",
                                   foundStyle.ChromeStyleID, foundStyle.ModelCode, foundStyle.ConsumerFriendlyStyleName);
                }

            }




            return foundStyle;
        }


        /// <summary>
        /// for toyota, lexus, scion - if the last charactor is alpha, remove it from the model code to match Chrome's fullstyleCode
        /// ?? wonder if the letter stands for anything special?????
        /// </summary>
        /// <param name="modelCode"></param>
        /// <returns></returns>
        internal override string GetModelCode(string modelCode)
        {
            var returnCode = base.GetModelCode(modelCode);

            if (!string.IsNullOrWhiteSpace(returnCode))
            {
                returnCode = returnCode.Trim();

                // if it is a letter ... removed it
                if (char.IsLetter(returnCode.Last()))
                {
                    returnCode = returnCode.Substring(0, returnCode.Length - 1);
                }
            }

            return returnCode;
        }

        internal override Merchandising.DomainModel.Vehicles.ChromeStyle MatchStyle(VehicleBuildData vehicleBuild, List<AutoLoad.FordNew.DataAccess.ChromeStyle> chromeStyleList, List<ChromeOption> chromeOptionList)
        {
            var foundStyle = base.MatchStyle(vehicleBuild, chromeStyleList, chromeOptionList);


            // try by option kind 2 and model (toyota model option)
            if (foundStyle == null)
            {

                // try to filter to by kind option id 2 - toyota model option (basically a unique fee on (SE - southeast models)
                var filteredOptionList = (from options in chromeOptionList
                                          join styles in chromeStyleList on options.StyleId equals styles.StyleId
                                          where
                                              styles.FullStyleCode == GetModelCode(vehicleBuild.OemModelCode) &&
                                              options.OptionKindId == 2
                                          select options).ToList();


                foundStyle = FindUniqueStyle(filteredOptionList.ToList(), vehicleBuild.UnfilteredOptionPackages, "model and Required option filtered Chrome options list");
            }

            return foundStyle;

        }

        /// <summary>
        /// had to override the match because SET has an "A" at the end of some of the options and 
        /// chrome does not.  
        /// </summary>
        /// <param name="optionList"></param>
        /// <param name="package"></param>
        /// <returns></returns>
        internal override List<ChromeOption> MatchOption(List<ChromeOption> optionList, OptionPackage package)
        {
            var resultList = base.MatchOption(optionList, package);
            if (resultList.Count == 0)
            {
                package.OemCode=package.OemCode.TrimStart('0');
                resultList = base.MatchOption(optionList, package);
            }
            // there is a "A" at the end of some of the packages from SET, try again without the "A"
            if (resultList.Count < 1 && package.OemCode.Last() == 'A' && package.OemCode.Trim().Length > 1)
            {
                package.OemCode = package.OemCode.Substring(0, package.OemCode.Length - 1); // remove the "A"

                resultList = base.MatchOption(optionList, package);
            }

            return resultList;

        }



    }
}