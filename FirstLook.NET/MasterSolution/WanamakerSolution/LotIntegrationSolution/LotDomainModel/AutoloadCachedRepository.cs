﻿
using System.Collections.Generic;
using FirstLook.Common.Core;
using Merchandising.Messages;

namespace FirstLook.LotIntegration.DomainModel
{
    public class AutoloadCachedRepository : IAutoloadRepository
    {
        private static readonly string CacheKey = typeof(AutoloadCachedRepository).FullName;
        private static readonly string CacheKeyManufacturers = CacheKey + "Manufacturers";
        private static readonly string CacheKeyMakes = CacheKey + "Makes";
        private const int TimeoutInSeconds = 5 * 60;

        private ICache Cache { get; set; }
        private IAutoloadRepository Inner { get; set; }

        public AutoloadCachedRepository(ICache cache, IAutoloadRepository inner)
        {
            Cache = cache;
            Inner = inner;
        }


        public void Save(AutoloadStatus status, string memberLogin)
        {
            Inner.Save(status, memberLogin);
        }

        public AutoloadStatus Fetch(int businessUnitId, int inventoryId)
        {
            return Inner.Fetch(businessUnitId, inventoryId);
        }

        public IEnumerable<AutoloadStatus> GetAllByStatusType(int businessUnitId, Status statusType)
        {
            return Inner.GetAllByStatusType(businessUnitId, statusType);
        }

        public AutoLoadInventory[] GetAllWithErrors()
        {
            return Inner.GetAllWithErrors();
        }

        public IList<int> GetAllWithErrors(int businessUnitId)
        {
            return Inner.GetAllWithErrors(businessUnitId);
        }

        public List<int> GetSlurpeeRequeues(int businessUnitId)
        {
            return Inner.GetSlurpeeRequeues(businessUnitId);
        }

        public List<int> GetInventoryWithNoAutoloadStatus(int businessUnitId)
        {
            return Inner.GetInventoryWithNoAutoloadStatus(businessUnitId);
        }

        public List<AutoLoadInventory>  GetOpenRequeues(int? businessUnitId)
        {
            return Inner.GetOpenRequeues(businessUnitId);
        }

        public bool IsBatchAutoloadEnabled(int businessUnitId)
        {
            return Inner.IsBatchAutoloadEnabled(businessUnitId);
        }

        public List<string> GetSupportedMakeList()
        {
            var data = (List<string>)Cache.Get(CacheKeyMakes);

            if (data != null) return data;

            data = Inner.GetSupportedMakeList();

            Cache.Set(CacheKeyMakes, data, TimeoutInSeconds);

            return data;
        }

        public List<ManufacturerAutoLoadSettings> GetSupportedManufacturerList()
        {
            var data = (List<ManufacturerAutoLoadSettings>)Cache.Get(CacheKeyManufacturers);

            if (data != null) return data;

            data = Inner.GetSupportedManufacturerList();

            Cache.Set(CacheKeyManufacturers, data, TimeoutInSeconds);

            return data;
        }

        public void UpdateAutoLoadSettings(IEnumerable<int> manufacturerIdList)
        {
            Inner.UpdateAutoLoadSettings(manufacturerIdList);
            ClearCache();
        }

        private void ClearCache()
        {
            Cache.Delete(CacheKeyManufacturers);
            Cache.Delete(CacheKeyMakes);
        }

       

    }
}
