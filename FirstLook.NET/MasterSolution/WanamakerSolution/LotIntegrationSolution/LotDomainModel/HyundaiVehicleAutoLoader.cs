﻿using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.DataAccess;
using FirstLook.Merchandising.DomainModel.Vehicles;
using MAX.BuildRequest.VehicleDataProviders;
using Merchandising.Messages.VehicleBuild;

namespace FirstLook.LotIntegration.DomainModel
{
    internal class HyundaiVehicleAutoLoader : TaskVehicleAutoLoader
    {
        public HyundaiVehicleAutoLoader(IAutoloadRepository autoloadRepository, IAdMessageSender adMessageSender, IVehicleReferenceDataRepository vehicleReferenceDataRepository, string fileConent, IVehicleDataProvider vehicleDataProvider)
            : base(autoloadRepository, adMessageSender, vehicleReferenceDataRepository, fileConent, vehicleDataProvider)
        {
        }
        //protected override void ApplyDynamicWhiteListOptions(int inventoryId, IChromeStyle inferredStyle, VehicleBuildData vehicleBuild)
        //{

        //    if (inferredStyle.ChromeStyleID > 0)
        //    {
        //        // create a list of strings that should be included
        //        var includePhraseList = new List<string> { "package", "collection", "group", "pack", "pkg" };

        //        // $199.00 per chad and michaela
        //        vehicleBuild.OptionPackages = CleanseOptions(vehicleBuild.UnfilteredOptionPackages,
        //                                                     inferredStyle.ChromeStyleID, 199.0m, includePhraseList);

        //    }
                
        //}
        public override VehicleDataAccess.Equipment MatchOption(EquipmentCollection equipmentCollection, OptionPackage package)
        {
            var equipment = base.MatchOption(equipmentCollection, package);



            // if no match and valid oemcode ... do some further checking
            if(equipment == null && !string.IsNullOrWhiteSpace(package.OemCode))
            {
                foreach (var chromeOption in equipmentCollection.ToList())
                {
                    // check for starts with oemCode + "-"
                    if (chromeOption.OptionCode.StartsWith(package.OemCode.Trim() + "-"))
                    {
                        equipment = chromeOption;
                        break;
                    }
                    
                    // check for starts with oemCode + "/"
                    if (chromeOption.OptionCode.StartsWith(package.OemCode.Trim() + "/")) // check for starts with oemCode + "/"
                    {
                        equipment = chromeOption;
                        break;
                    }
                }
            }


            return equipment;

        }
    }
}
