﻿using System.Text.RegularExpressions;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.DataAccess;
using MAX.BuildRequest.VehicleDataProviders;

namespace FirstLook.LotIntegration.DomainModel
{
    class MazdaVehicleAutoLoader : TaskVehicleAutoLoader
    {
        public MazdaVehicleAutoLoader(IAutoloadRepository autoloadRepository, IAdMessageSender adMessageSender, IVehicleReferenceDataRepository vehicleReferenceDataRepository, string fileContent, IVehicleDataProvider vehicleDataProvider)
            : base(autoloadRepository, adMessageSender, vehicleReferenceDataRepository, fileContent, vehicleDataProvider)
        {

        }

        /// <summary>
        /// correct the oemmodelCode, Mazda has a spaces and a "/ year" as part the model code
        /// moved this from the preProcess during FB: 32073
        /// </summary>
        /// <param name="modelCode"></param>
        /// <returns></returns>
        internal override string GetModelCode(string modelCode)
        {
            // call base
            modelCode = base.GetModelCode(modelCode);
            
            if (!string.IsNullOrWhiteSpace(modelCode))
            {
                modelCode = Regex.Replace(modelCode, @"\n", "");

                modelCode = modelCode.Replace(" ", "");

                // get rid of the year
                int iPos = modelCode.IndexOf("/", System.StringComparison.Ordinal);
                if (iPos > -1)
                    modelCode = modelCode.Substring(0, iPos);
            }


            return modelCode;
        }


        //protected override VehicleResponse PreProcessVehicleResponse(VehicleResponse vehicleResponse)
        //{
        //    // correct the oemmodelCode, Mazda has a spaces and a "/ <year>" as part the model code
        //    string modelCode = vehicleResponse.VehicleBuildData.OemModelCode;

        //    modelCode = Regex.Replace(modelCode, @"\n", "");
            
        //    modelCode = modelCode.Replace(" ", "");

        //    // get rid of the year
        //    int iPos = modelCode.IndexOf("/");
        //    if (iPos > -1)
        //        modelCode = modelCode.Substring(0, iPos);

        //    vehicleResponse.VehicleBuildData.OemModelCode = modelCode;

        //    return base.PreProcessVehicleResponse(vehicleResponse);
        //}
    }

    
}
