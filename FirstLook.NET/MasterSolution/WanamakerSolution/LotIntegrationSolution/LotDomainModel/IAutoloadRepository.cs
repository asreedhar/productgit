﻿using System.Collections.Generic;
using Merchandising.Messages;

namespace FirstLook.LotIntegration.DomainModel
{
    public interface IAutoloadRepository
    {
        void Save(AutoloadStatus status, string memberLogin);

        AutoloadStatus Fetch(int businessUnitId, int inventoryId);

        IEnumerable<AutoloadStatus> GetAllByStatusType(int businessUnitId, Status statusType);

        IList<int> GetAllWithErrors(int businessUnitId);
        List<int> GetInventoryWithNoAutoloadStatus(int businessUnitId);
        List<int> GetSlurpeeRequeues(int businessUnitId);

        AutoLoadInventory[] GetAllWithErrors();

        bool IsBatchAutoloadEnabled(int businessUnitId);

        List<string> GetSupportedMakeList();

        List<ManufacturerAutoLoadSettings> GetSupportedManufacturerList();

        void UpdateAutoLoadSettings(IEnumerable<int> manufacturerIdList);

        List<AutoLoadInventory> GetOpenRequeues(int? businessUnitId);

    }
}
