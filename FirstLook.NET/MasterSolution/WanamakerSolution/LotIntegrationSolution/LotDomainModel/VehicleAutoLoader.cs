using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Net;
using System.Security.Authentication;
using System.Text;
using System.Text.RegularExpressions;
using AutoLoad.FordNew.DataAccess;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Data;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.DataAccess;
using FirstLook.Merchandising.DomainModel.Vehicles;
using MAX.BuildRequest;
using MAX.BuildRequest.VehicleDataProviders;
using MAX.ExternalCredentials;
using Merchandising.Messages;
using Merchandising.Messages.AutoApprove;
using Merchandising.Messages.VehicleBuild;
using VehicleDataAccess;
using System.Linq;
using log4net;
using ChromeStyle = FirstLook.Merchandising.DomainModel.Vehicles.ChromeStyle;


namespace FirstLook.LotIntegration.DomainModel
{

    public class VehicleAutoLoader
    {
        private const int NoChromeStyleId = -1;

        #region Logging

        protected static readonly ILog Log = LogManager.GetLogger(typeof(VehicleAutoLoader).FullName);

        #endregion

        #region Injected dependencies

        public IAdMessageSender MessageSender { get; set; }
        private readonly AutoloadManager _autoloadManager;
        private readonly IVehicleReferenceDataRepository _vehicleReferenceDataRepository;
        private readonly IAutoloadRepository _autoloadRepository;
        protected readonly IVehicleDataProvider DataProvider;
        
        #endregion

        // Messages returned by methods in this class.
        public static class Messages
        {
            public static string GenericError = " is not responding.  Please try again later.";
            public static string InvalidCredentials = ": Invalid username/password";
            public static string NoVehicleDataFound = ": No vehicle data found.";
        }

        //private readonly VehicleDataProviderManager _providerManager;
        protected readonly ChromeRepository ChromeRepository;
        protected readonly BuildRequestRepository BuildRequestRepository;
        
        public VehicleAutoLoader(IAutoloadRepository autoloadRepository, 
                                    IAdMessageSender adMessageSender, 
                                    IVehicleReferenceDataRepository vehicleReferenceDataRepository,
                                    IVehicleDataProvider vehicleDataProvider)
        {

            _autoloadRepository = autoloadRepository;
            _vehicleReferenceDataRepository = vehicleReferenceDataRepository;
            MessageSender = adMessageSender;
            DataProvider = vehicleDataProvider;
            
            _autoloadManager = new AutoloadManager(_autoloadRepository);
            //_providerManager = new VehicleDataProviderManager();
            ChromeRepository = new ChromeRepository(); // ToDo: inject this one day
            BuildRequestRepository = new BuildRequestRepository(); // TODO inject this in


        }

        public delegate EncryptedSiteCredential RequestingLoginEventHandler(int businessUnitId, Agent requestingSite);
        public event RequestingLoginEventHandler RequestingLogin;
        public EncryptedSiteCredential OnRequestingLogin(int businessUnitId, Agent requestingSite)
        {
            return GetCredential(businessUnitId, requestingSite); //bubbles up if attached by using class
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <param name="vehicleRequest"></param>
        /// <param name="processName"></param>
        /// <returns>message reporting results</returns>
        public string LoadOntoVehicle(int businessUnitId, BuildDataVehicleRequest vehicleRequest)
        {

            var processName = GetType().Name;

            Log.DebugFormat("AutoLoad: Loading onto vehicle, inventoryId {3}, vin {0}, make {1}, processName {2}", vehicleRequest.Vin, vehicleRequest.Make, processName, vehicleRequest.InventoryId);

            VehicleConfiguration currentVehicle = VehicleConfiguration.FetchOrCreateDetailed(businessUnitId, vehicleRequest.InventoryId, processName);
            
            //get the provider for this make
            //IVehicleDataProvider dataProvider = BuildRequestRepository.GetVehicleDataProvider(vehicleRequest.Site);

            Log.DebugFormat("AutoLoad: Autoloading using provider type {0}", DataProvider.GetType());

            try
            {
                return LoadOntoVehicle(businessUnitId, vehicleRequest.Vin, vehicleRequest.InventoryType, currentVehicle, processName, vehicleRequest.InventoryId);            
            }
            catch (InvalidCredentialException exception)
            {
                Log.Info("AutoLoad: Invalid Credentials Used", exception);
                _autoloadManager.SetAutoloadEnd(businessUnitId, vehicleRequest.InventoryId, Status.InvalidCredentials, processName);

                return DataProvider.SiteDescription + Messages.InvalidCredentials;
            }
            catch (WebException exception) //http request fail
            {
                Log.Warn("Call to LoadOntoVehicle() failed: Web request timed out.", exception);
                _autoloadManager.SetAutoloadEnd(businessUnitId, vehicleRequest.InventoryId, Status.NetworkError, processName);
                return DataProvider.SiteDescription + Messages.GenericError;
            }
            catch (SqlException exception) //We need to rethrow here so queues can retry.
            {
                Log.Warn("AutoLoad: SQL Exception", exception);
                _autoloadManager.SetAutoloadEnd(businessUnitId, vehicleRequest.InventoryId, Status.UnknownError, processName);
                throw;
            }
            catch (Exception exception)
            {
                Log.Warn("AutoLoad: General Error", exception);
                _autoloadManager.SetAutoloadEnd(businessUnitId, vehicleRequest.InventoryId, Status.UnknownError, processName);
                return DataProvider.SiteDescription + Messages.GenericError;
            }
        }
      
        internal virtual VehicleBuildData GetBuildData(int businessUnitId, int inventoryId,  string processName, string vin, int inventoryType)
        {
            // Record start for maual load.  // remember this is overridden in TaskVehicleAutoLoader
            _autoloadManager.SetAutoloadStart(businessUnitId, inventoryId, processName);
            return DataProvider.GetVehicleBuildData(vin, inventoryType, processName);
        }

        protected virtual void SetEndBatchAutoload(AutoloadManager autoloadManager, int businessUnitId, int inventoryId, string processName)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <param name="vin"></param>
        /// <param name="inventoryType"></param>
        /// <param name="currentVehicle"></param>
        /// <param name="processName"></param>
        /// <param name="inventoryId"></param>
        /// <returns>message reporting results</returns>
        internal string LoadOntoVehicle(int businessUnitId, string vin, int inventoryType, VehicleConfiguration currentVehicle, string processName, int inventoryId)
        {
            var sb = new StringBuilder(string.Format("AutoBuild for VIN {0}\n", vin));

            var methodInvocationInfo = string.Format("businessUnitId {0}, vin {1}, currentVehicle InventoryId {2}, dataProvider type {3}, processName {4}, input inventory id {5}",
                businessUnitId, vin, currentVehicle.InventoryId, DataProvider.GetType(), processName, inventoryId);

            Log.DebugFormat("AutoLoad: LoadOntoVehicle invoked with {0}", methodInvocationInfo);

            // If locked, we can't autoload.
            if (currentVehicle.LotDataImportLock)
            {
                //Need to record the result for BatchAutoload only
                SetEndBatchAutoload(_autoloadManager, businessUnitId, inventoryId, processName);
                sb.AppendLine("Vehicle LOCKED preventing autoloading");
                return sb.ToString();
            }


            Log.Debug("AutoLoad: current vehicle is not locked, proceeding with autoload...");

            if (currentVehicle.ChromeStyleID != NoChromeStyleId)
            {
                Log.InfoFormat("AutoLoad: A chrome style id is already set for this vehicle with a value of {0}", currentVehicle.ChromeStyleID);
            }

            /**** The autoload start record is recorded here for manual auto load. For Batch Autoload the start record was added when the message was queued.***/
            var vehicleBuild = GetBuildData(businessUnitId, inventoryId, processName, vin, inventoryType);

            if (vehicleBuild == null)
            {
                Log.Info(string.Format("AutoLoad: No vehicle build data found.  Method invocation info: {0}",
                                       methodInvocationInfo));
                _autoloadManager.SetAutoloadEnd(businessUnitId, inventoryId, Status.NoVehicleFound, processName);

                return Messages.NoVehicleDataFound;
            }

            Log.Debug("AutoLoad: vehicle data found.");

            // Get the style
            IChromeStyle inferredStyle = GetInferredStyle(vin, sb, vehicleBuild);

            // if we can't infer a style and the current vehicle has a style, assume it is correct
            if (inferredStyle.ChromeStyleID == -1 && currentVehicle.ChromeStyleID != -1)
                inferredStyle = ChromeStyle.ChromeStyleSelect(currentVehicle.ChromeStyleID);

            ApplyDynamicWhiteListOptions(vin, inferredStyle, vehicleBuild);

            // Determine what the next actions should be
            bool loadColorsAndOptions, setStyle;
            GetNextActions(currentVehicle.ChromeStyleID, inferredStyle.ChromeStyleID, sb, out loadColorsAndOptions, out setStyle);

            // If we aren't going to change the vechicle, then we have nothing to do.
            if( !(setStyle || loadColorsAndOptions ))
            {
                SetPackageEquipmentComplete(businessUnitId, inventoryId, processName);
                // set as an "error" so slurpee will re-queue
                _autoloadManager.SetAutoloadEnd(businessUnitId, inventoryId, Status.CannotDecodeStyle, processName);
                return sb.ToString();
            }

            if( setStyle )
            {
                currentVehicle.SetChromeStyleID(businessUnitId, inventoryId, inferredStyle.ChromeStyleID);
                MessageSender.SendStyleSet(businessUnitId, inventoryId, processName, GetType().FullName);
                sb.AppendLine(string.Format("AutoLoad: Found and set chrome style id '{0}'", inferredStyle.ChromeStyleID));
            }

            if (loadColorsAndOptions)
            {
                // Figure out the colors based on the style, now that we know the style.
                DetermineColors(inferredStyle.ChromeStyleID, vehicleBuild);

                // Load the data we got from AutoLoad
                AddAutoLoadColorsToVehicle(vehicleBuild, currentVehicle, sb);
                AddAutoLoadOptionsToVehicle(businessUnitId, currentVehicle, processName, inventoryId, sb, vehicleBuild);
                Log.Debug("AutoLoad: Finished updating vehicle, saving vehicle...");

            }

            // Save the changes we've made to this vehicle
            currentVehicle.Save(businessUnitId, processName);

            // Save the MSRP if available, moved as part of FB: 30440
            SaveMsrp(vin, vehicleBuild);

            // Send an auto-approve message.
            // TODO: Should this be moved into VehicleConfiguration.Save() ?
            try
            {
                MessageSender.SendAutoApproval(new AutoApproveMessage(businessUnitId, inventoryId, processName), GetType().FullName);
            }
            catch (Exception ex)
            {
                Log.Error("Unable to send auto approve message.", ex);
            }

            sb.AppendLine("AutoLoad: Complete");
            SetPackageEquipmentComplete(businessUnitId, inventoryId, processName);
            // extra credit, if exterior color code is not set ... mark autoload status as such
            if(string.IsNullOrEmpty(currentVehicle.ExteriorColorCode))  // just exterior for now
                _autoloadManager.SetAutoloadEnd(businessUnitId, inventoryId, Status.CannotDecodeExtColor, processName);
            else
                _autoloadManager.SetAutoloadEnd(businessUnitId, inventoryId, Status.Successful, processName);
            return sb.ToString();
        }

        /// <summary>
        /// use the autoload settings sanitized the options on the build data to options that will be attached to the vehicle
        /// </summary>
        /// <param name="vin">just used for logging</param>
        /// <param name="inferredStyle"></param>
        /// <param name="vehicleBuild"></param>
        internal virtual void ApplyDynamicWhiteListOptions(string vin, IChromeStyle inferredStyle, VehicleBuildData vehicleBuild)
        {
            // only if a chrome style has been found 
            if(inferredStyle.ChromeStyleID > 0)
            {
                // get the vehicle manufacturer and make
                var manufacturerId = ChromeStyle.GetManufacturerId(inferredStyle.ChromeStyleID);
                var divisionId = ChromeStyle.GetDivisionId(inferredStyle.ChromeStyleID);
                
                // Get all possible options for this style.
                var chromeOptions = ChromeRepository.GetOptions(new[] {inferredStyle.ChromeStyleID}).ToList();

                // get white listed options
                var whiteListOptions = DataProvider.GetOptionsWhiteList(manufacturerId, divisionId);

                // get option filters
                var optionFilters = DataProvider.GetOptionFilters(manufacturerId);

                // replace the options to be load on the vehicle with the santizedOption
                vehicleBuild.OptionPackages = SanitizeOptions(vehicleBuild.UnfilteredOptionPackages, chromeOptions, whiteListOptions, optionFilters);

                // log results
                Log.DebugFormat("AutoLoad options after santinization for VIN: {0} unfiltered: {1} sanitized: {2}", 
                                            vin, 
                                            String.Join(",", vehicleBuild.UnfilteredOptionPackages.Select(x => x.OemCode)),
                                            String.Join(",", vehicleBuild.OptionPackages.Select(x => x.OemCode))
                                            );

            }


            /*  BMW code, will now be data driven, tureen 2/28/2015 fB32397
            
            // We currently only do this for BMW.  We will expand this to include other manufacturers goign forward. ZB
            var makeCommand = new GetInventoryManufacturerMakeInfo(inventoryId);
            AbstractCommand.DoRun( makeCommand );

            if (!makeCommand.Result.ManufacturerName.ToUpper().Contains("BMW"))
            {
                return;
            }

            // Get all possible options for this style.
            var options = ChromeRepository.GetOptions(new[] {inferredStyle.ChromeStyleID});

            // Add any options that have an MSRP > 0 to the VehicleBuild if:
            // the option is not already on the vehicle and
            // the provider reported the option
            foreach (var option in options)
            {
                ChromeOption option1 = option; // avoid access to foreach variable in closure
                if (option1.Msrp > 0 &&
                    vehicleBuild.OptionPackages.All(o => o.OemCode != option1.OptionCode) &&            // not already there
                    vehicleBuild.UnfilteredOptionPackages.Any(o => o.OemCode == option1.OptionCode))    // but found by provider
                {
                    vehicleBuild.OptionPackages.Add(new OptionPackage(option1.OptionCode, option1.OptionDesc));
                }
            }
             */
        }

        private static void SetPackageEquipmentComplete(int businessUnitId, int inventoryId, string processName)
        {
            StepStatusCollection
                .Fetch(businessUnitId, inventoryId)
                .SetStatus(StepStatusTypes.ExteriorEquipment, ActivityStatusCodes.Complete)
                .SetStatus(StepStatusTypes.InteriorEquipment, ActivityStatusCodes.Complete)
                .Save(processName);
        }

        private static void DetermineColors(int styleId, VehicleBuildData vehicleBuild)
        {
            // We can't do this until we know the style.
            var colorOptionCodeKindList = new HashSet<Common.Core.Tuple<string, int>>();
            foreach (var colorOptionCode in GetColorOptionCodeList(styleId))
            {
                colorOptionCodeKindList.Add(new Common.Core.Tuple<string, int>(colorOptionCode.Item1.ToLower(), colorOptionCode.Item2));
            }

            // convert option packages
            foreach (var optionPackage in vehicleBuild.UnfilteredOptionPackages)
            {
                var extColorMatchRegex = new Regex("^[0-9]{2}U$", RegexOptions.IgnoreCase);
                var intColorMatchRegex = new Regex("^[0-9]{2}I$", RegexOptions.IgnoreCase);

                var oemCode = optionPackage.OemCode.ToLower();

                if (
                    // Only set the color for the first oemCode found.
                    string.IsNullOrEmpty(vehicleBuild.ExteriorColorCode) && colorOptionCodeKindList.Count > 0 && 
                    colorOptionCodeKindList.Any(
                        c => (
                            c.First == oemCode
                            || (extColorMatchRegex.IsMatch(oemCode)
                                && colorOptionCodeKindList.Any(c2 => c2.First == oemCode.Substring(0, 2)))
                            )
                            && c.Second == 0
                        )
                    )
                {
                    vehicleBuild.ExteriorColorCode = optionPackage.OemCode;
                    vehicleBuild.ExteriorColor = optionPackage.ProviderDescription;
                }
                else if (
                    // Only set the color for the first oemCode found.
                    string.IsNullOrEmpty(vehicleBuild.InteriorColorCode) && colorOptionCodeKindList.Count > 0 && 
                    colorOptionCodeKindList.Any(
                        c => (
                            c.First == oemCode
                            || (intColorMatchRegex.IsMatch(oemCode)
                                && colorOptionCodeKindList.Any(c2 => c2.First == oemCode.Substring(0, 2)))
                            )
                            && c.Second == 1
                        )
                    )
                {
                    vehicleBuild.InteriorColorCode = optionPackage.OemCode;
                    vehicleBuild.InteriorColor = optionPackage.ProviderDescription;
                }
            }
        }

        internal static IEnumerable<Tuple<string, int>> GetColorOptionCodeList(int styleId)
        {
            var list = new List<Tuple<string, int>>();
            using (var con = Common.Core.Data.Database.Connection(Database.Merchandising))
            {
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }
                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = "SELECT DISTINCT * FROM chrome.ColorOptionCode#LookupByManufacturer WHERE StyleID = @styleID and CountryCode = 1";
                    cmd.CommandType = CommandType.Text;
                    cmd.AddParameter("@StyleId", DbType.Int32, styleId);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var code = ((string)reader["OptionCode"]).ToLower();
                            var kind = ((int)reader["OptionKind"]);
                            list.Add(new Tuple<string, int>(code, kind));
                        }
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// What should the next actions be?
        /// </summary>
        /// <returns></returns>
        internal void GetNextActions(int currentStyleId, int inferredStyleId, StringBuilder sb, 
            out bool loadOptionsAndColors, out bool setStyle)
        {
            // Current Style Id	Inferred Style Id	Action
            // 0	                0	            Don't load options or colors, don't set the style
            // 1	                0	            Load options and colors, don't set the style
            // 0	                1	            Load options and colors, set the style
            // 1	                1	            Load options and colors, don't set the style
            // 1	                2	            //FB: 29554 - now we overwrite selected style and load options/colors

            var match = currentStyleId == inferredStyleId;
            var currentKnown = currentStyleId > 0;
            var inferredKnown = inferredStyleId > 0;

            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (!currentKnown && !inferredKnown)
            {
                sb.AppendLine(
                    "AutoLoad cannot determine this vehicle's trim by looking at the OEM data and the trim has not been specified. Please specify the trim and try again.");
                loadOptionsAndColors = false;
                setStyle = false;
                return;
            }
            if (currentKnown && !inferredKnown)
            {
                loadOptionsAndColors = true;
                setStyle = false;
                return;
            }
            if (!currentKnown && inferredKnown)
            {
                loadOptionsAndColors = true;
                setStyle = true;
                return;
            }
            if (currentKnown && inferredKnown && match)
            {
                loadOptionsAndColors = true;
                setStyle = false;
                return;
            }
            if( currentKnown && inferredKnown && !match)
            {
                // Log inconsistency
                Log.WarnFormat("AutoLoad: The inferred chrome style id '{0}' does not match the existing chrome style id '{1}'. Current style overridden.",
                    inferredStyleId, currentStyleId);
                sb.AppendLine("AutoLoad has detected that a different trim should be used than the one currently set for this car.");

                // changed behavior in FB: 29554
                loadOptionsAndColors = true;
                setStyle = true;
                return;
            }
            // ReSharper restore ConditionIsAlwaysTrueOrFalse

// ReSharper disable HeuristicUnreachableCode
            loadOptionsAndColors = false;
            setStyle = false;
            //return;
// ReSharper restore HeuristicUnreachableCode
        }

        public virtual bool HasLoadedOption(DetailedEquipmentCollection options, OptionPackage package)
        {
            return options.ContainsOption(package.OemCode);
        }

        public virtual Equipment MatchOption(EquipmentCollection equipmentCollection, OptionPackage package)
        {
            var equipment = equipmentCollection.GetByCode(package.OemCode);
            if(equipment == null)
            {
                equipment = equipmentCollection.GetByCode(package.OemCode + "-F");
                if (equipment == null)
                {
                    equipment = equipmentCollection.GetByCode(package.OemCode + "-R");
                }
            }

            return equipment;
        }

        public virtual void LoadPackage(DisplayEquipmentCollection vehicleOptions, Equipment equip, int businessUnitId, int inventoryId)
        {
            vehicleOptions.AddOption(equip, businessUnitId, inventoryId);
        }

        /// <summary>
        /// Add the reported autoload options onto the vehicle if appropriate.
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <param name="currentVehicle"></param>
        /// <param name="processName"></param>
        /// <param name="inventoryId"></param>
        /// <param name="sb"></param>
        /// <param name="vehicleBuild"></param>
        internal virtual void AddAutoLoadOptionsToVehicle(int businessUnitId, VehicleConfiguration currentVehicle, string processName, 
            int inventoryId, StringBuilder sb, VehicleBuildData vehicleBuild)
        {
            // The autoload equipment reported by the manufacturer
            List<OptionPackage> vehiclePackages = vehicleBuild.OptionPackages;

            // the option packages need to be 'filtered' using the new match logic so equipment match works
            // trying to respect the legacy code
            if (currentVehicle.ChromeStyleID != -1)
            {
                // clean up these options first!!!!

                // find all the options for these styles
                var optionList = ChromeRepository.GetOptions(new List<int> {currentVehicle.ChromeStyleID}).ToList();

                var foundOptionList = FilterOptions(optionList, vehicleBuild.OptionPackages);

                // convert the chromeOptions into a optionPackage list
                vehiclePackages = (from chromeOption in foundOptionList
                                  select new OptionPackage(chromeOption.OptionCode)).ToList();

            }


            Log.DebugFormat(
                "AutoLoad: chrome style id allows {0} packages to be loaded onto vehicle, styleid:{1}",
                vehiclePackages.Count, currentVehicle.ChromeStyleID);

            if (vehiclePackages.Count > 0)
            {
                // Go get the chrome equipment for this styleId
                EquipmentCollection equipment = EquipmentCollection.FetchUnfilteredOptions(currentVehicle.ChromeStyleID);

                Log.DebugFormat("AutoLoad: Fetched {0} equipment items for styleId {1}", equipment.Count,
                                currentVehicle.ChromeStyleID);

                foreach (Equipment e in equipment)
                {
                    Log.DebugFormat("AutoLoad: -- Fetched equipment code:  {0}", e.OptionCode ?? "(null)");
                }

                int optionCount = 0;
                foreach (OptionPackage package in vehiclePackages)
                {
                    // Add the option if it isn't already configured...
                    if (!HasLoadedOption(currentVehicle.VehicleOptions.options, package))
                    {
                        Equipment equip = MatchOption(equipment, package);
                        if (equip != null)
                        {
                            Log.DebugFormat(
                                "AutoLoad: Loading option onto inventoryid {0}, option code: {1}, option description: {2} ...",
                                inventoryId, equip.OptionCode, equip.Description);

                            LoadPackage(currentVehicle.VehicleOptions, equip, businessUnitId, inventoryId);
                            optionCount++;
                        }
                        else
                        {
                            Log.InfoFormat("AutoLoad: equipment is null.  package.OemCode: {0}",
                                            package.OemCode ?? "(null reference)");
                        }
                    }
                    else
                    {
                        Log.DebugFormat(
                            "AutoLoad: currentVehicle.VehicleOptions.options contained package code:{0}",
                            package.OemCode ?? "(null reference)");
                    }
                }

                if (optionCount > 0)
                {
                    sb.AppendLine(string.Format("Applying {0} OEM Vehicle packages", optionCount));
                }

                // We also need to set the states for equipment as completed
                Log.DebugFormat(
                    "AutoLoad: Setting equipment state, businessUnitId {0}, currentVehicle.InventoryId {1}, equipTypes {{ ExteriorEquipment, InteriorEquipment }}, processName {2} ...",
                    businessUnitId, currentVehicle.InventoryId, processName);
            }

        }


        /// <summary>
        /// Return a chrome style.  Will not return null.
        /// </summary>
        /// <param name="vin"></param>
        /// <param name="sb"></param>
        /// <param name="vehicleBuild"></param>
        /// <returns></returns>
        internal virtual IChromeStyle GetInferredStyle(string vin, StringBuilder sb, VehicleBuildData vehicleBuild)
        {
            sb.AppendLine("AutoLoad: examining trim information...");

            // We use _all_ the options we got back from the provider when inferring the style.
            // If we use the filtered (whitelisted) options then we ignore a lot of useful data.
            List<string> options = vehicleBuild.UnfilteredOptionPackages.Select(i => i.OemCode).ToList();            
            ChromeStyle inferredStyle = ChromeStyle.InferStyleId(vin, options, GetModelCode(vehicleBuild.OemModelCode),
                                                                 vehicleBuild.Trim, TransmissionType.Unknown,
                                                                 DrivetrainType.Unknown);

            if( inferredStyle == null )
            {
                // try the decoder with access to this object and (its descendants) properties,
                // the standard chromeStyle decoding does not know about manufacture specific data
                inferredStyle = DecodeStyle(vin, vehicleBuild);

                if (inferredStyle == null)
                    inferredStyle = new ChromeStyle(); // the default behavior is to return an unknown style
            }

            Log.DebugFormat("AutoLoad: inferred chrome style: {0}", inferredStyle);
            return inferredStyle;
        }

        /// <summary>
        /// This processes the colors that we receive from the provider (Fetch).  If for some reason we get the codes but not the color descriptions
        /// we try to derieve the descriptions from the chorme database.
        /// </summary>
        /// <param name="vehicleBuild">the data received from autoload source - fetch</param>
        /// <param name="currentVehicle">current values stored in the database for this vehicle.</param>
        /// <param name="report">The autoload report that will be shown to the user</param>
        private void AddAutoLoadColorsToVehicle(VehicleBuildData vehicleBuild, VehicleConfiguration currentVehicle, StringBuilder report)
        {
            AvailableChromeColors chromeColors = null;

            //if color code or color is specified in autoload, we set the color of the vehicle.
            if (IsColorSpecified(vehicleBuild.ExteriorColorCode) || IsColorSpecified(vehicleBuild.ExteriorColor))
            {
                //Set the exterior colors
                string extColor = vehicleBuild.ExteriorColor;

                //set the exterior color code
                currentVehicle.ExteriorColorCode = vehicleBuild.ExteriorColorCode ?? string.Empty;

                //If there is a color code, no description and the vehicle has a trim set then maybe we can get the color description from Chrome
                if (!IsColorSpecified(extColor) &&
                    IsColorSpecified(vehicleBuild.ExteriorColorCode) &&
                    currentVehicle.ChromeStyleID > 0)
                {
                    var colorCodeFromAutload = vehicleBuild.ExteriorColorCode;
                    Log.DebugFormat(
                        "Fetch had exterior color code {0} but no description.  will get the description from chrome",
                        colorCodeFromAutload);

                    // This is the same method used to populate the exterior color drop downs on the approval page.
                    chromeColors = AvailableChromeColors.Fetch(currentVehicle.ChromeStyleID);
                    if (chromeColors != null)
                    {
                        var chromeExteriorColor = chromeColors.ExteriorColors.FirstOrDefault(x => x.GetPairCode().ToLower() == colorCodeFromAutload.ToLower());
                        if (chromeExteriorColor != null)
                        {
                            Log.DebugFormat("Found a match for the exterior color in Chrome.");
                            extColor = chromeExteriorColor.PairDescription;
                        }

                    }
                }

                currentVehicle.ExteriorColor1 = extColor ?? "";
                if (!string.IsNullOrEmpty(extColor))
                {
                    report.AppendLine(string.Format("Setting exterior color to {0}", extColor));
                }
            }


            // if interior color code or interior colored is specified in autoload, we set the color of the vehicle.
            if (IsColorSpecified(vehicleBuild.InteriorColorCode) || IsColorSpecified(vehicleBuild.InteriorColor))
            {
                // Set the Interior colors
                string intColor = vehicleBuild.InteriorColor;

                currentVehicle.InteriorColorCode = vehicleBuild.InteriorColorCode ?? string.Empty;

                // If there is an color code, no description and the vehicle has a trim set then maybe we can get the color description from Chrome
                if ( !IsColorSpecified(intColor) && 
                     IsColorSpecified(vehicleBuild.InteriorColorCode) &&
                     currentVehicle.ChromeStyleID > 0)
                {
                    var colorCodeFromAutload = vehicleBuild.InteriorColorCode;
                    Log.DebugFormat(
                        "Fetch had interior color code {0} but no description.  will get the description from chrome",
                        colorCodeFromAutload);

                    // This is the same method used to populate the interior color drop downs on the approval page.
                    chromeColors = chromeColors ?? AvailableChromeColors.Fetch(currentVehicle.ChromeStyleID);
                    if (chromeColors != null)
                    {
                        var chromeInteriorColor = chromeColors.InteriorColors.FirstOrDefault(x => x.ColorCode.ToLower() == colorCodeFromAutload.ToLower());
                        if (chromeInteriorColor != null)
                        {
                            Log.DebugFormat("Found a match for the interior color in Chrome.");
                            intColor = chromeInteriorColor.Description;
                        }

                    }

                }
                currentVehicle.InteriorColor = intColor ?? "";
                if (!string.IsNullOrEmpty(intColor))
                {
                    report.AppendLine(string.Format("Setting interior color to {0}", intColor));
                }
            }
        }

        /// <summary>
        /// If the color is an empty string or "unknown", then it hasn't been specified.
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        internal static bool IsColorSpecified(string color)
        {
            if (string.IsNullOrWhiteSpace(color)) return false;
            return color.ToLower().Replace(" ", "") != "unknown";
        }

        private readonly Dictionary<int, Dictionary<Agent, EncryptedSiteCredential>> _dealerCredentials = new Dictionary<int, Dictionary<Agent, EncryptedSiteCredential>>();
      
        /// <summary>
        /// Fetches and stores the credentials in a cache to lookup (and reduce number of repeated database requests for credentials)
        /// Can be overriden by using class, short circuiting the caching behavior
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <param name="site"></param>
        /// <returns></returns>
        private EncryptedSiteCredential GetCredential(int businessUnitId, Agent site)
        {


            if (RequestingLogin != null)  //return what the outside world gives us if that's wired up...
            {
                return RequestingLogin(businessUnitId, site);
            }
            //cache lookup
            if (_dealerCredentials.ContainsKey(businessUnitId) && _dealerCredentials[businessUnitId].ContainsKey(site))
            {
                return _dealerCredentials[businessUnitId][site];
            }

            var credential = EncryptedSiteCredential.Fetch(businessUnitId, site, false);

            //add dealer key if not there
            if (!_dealerCredentials.ContainsKey(businessUnitId))
            {
                _dealerCredentials.Add(businessUnitId, new Dictionary<Agent, EncryptedSiteCredential>());
            }

            //add site with creds for dealer -- note, this may be a null credential!
            _dealerCredentials[businessUnitId].Add(site, credential);
            return credential;
        }

        protected void SaveMsrp(string vin, VehicleBuildData vehicleBuild)
        {
            // this was moved from its own task as part of FB:30440 because S3 audit data does not go through fetch

            try
            {
                // put inside of its own try catch to prevent the task from reprocessing the AutoLoadResult is something errors

                // check MSRP value for value
                if (vehicleBuild.EstimatedMSRP > 0m)
                {
                    _vehicleReferenceDataRepository.Save(vin, ReferenceDataType.OriginalMsrp, GetType().FullName,
                                                        vehicleBuild.EstimatedMSRP.ToString("#########0.00", CultureInfo.CurrentCulture));
                }
            }
            catch(Exception ex)
            {
                Log.Error(ex);
            }

        }

        /// <summary>
        /// Overload of Match Option to use AutoLoadNew objects
        /// </summary>
        /// <param name="optionList"></param>
        /// <param name="package"></param>
        /// <returns></returns>
        internal virtual List<ChromeOption> MatchOption(List<ChromeOption> optionList, OptionPackage package)
        {

            var suffixList = new List<string>{"", "-R", "-F", "-0", "-1", "-2"};

            var resultList = new List<ChromeOption>();

            foreach (var suffix in suffixList)
            {
                var searchCode = package.OemCode + suffix;

                var foundList = (from optList in optionList
                                 where optList.OptionCode.Equals(searchCode)
                                 select optList).ToList();


                if (foundList.Any())
                {
                    resultList.AddRange(foundList);
                    break;
                }

            }

            return resultList;

        }

        /// <summary>
        /// Just filtering the option list to the packages passed in
        /// </summary>
        /// <param name="optionList"></param>
        /// <param name="packageList"></param>
        /// <returns></returns>
        internal virtual List<ChromeOption> FilterOptions(List<ChromeOption> optionList, List<OptionPackage> packageList)
        {

            var resultList = new List<ChromeOption>();

            foreach (var optionPackage in packageList)
            {
                var searchResult = MatchOption(optionList, optionPackage);

                if (searchResult.Any())
                    resultList.AddRange(searchResult);

            }

            return resultList;

        }

        /// <summary>
        /// Set up the decoding process for a VIN and its build data
        /// This method and many of its components can be customized per manufacture for more specialized decoding
        /// </summary>
        /// <param name="vin"></param>
        /// <param name="vehicleBuild"></param>
        /// <returns></returns>
        internal virtual ChromeStyle DecodeStyle(string vin, VehicleBuildData vehicleBuild)
        {

            // find all the styles possible for this VIN (for the US)
            var styleList = ChromeRepository.GetStyles(vin).Where(x => x.CountryCode == 1).ToList();

            // find all the options for these styles
            var optionList = ChromeRepository.GetOptions(styleList.Select(x => x.StyleId)).ToList();

            return MatchStyle(vehicleBuild, styleList, optionList);

        }

        /// <summary>
        /// Can a unique style be extracted from the option list and the optionPackage pulled from the build
        /// </summary>
        /// <param name="chromeOptionList"></param>
        /// <param name="optionPackage"></param>
        /// <param name="logMessage"></param>
        /// <returns></returns>
        internal virtual ChromeStyle FindUniqueStyle(List<ChromeOption> chromeOptionList, List<OptionPackage> optionPackage, string logMessage = "unknown")
        {

            // first try with all options from the style
            var foundOptionList = FilterOptions(chromeOptionList, optionPackage);

            var foundStyleList = foundOptionList.Select(x => x.StyleId).Distinct().ToList();


            if (foundStyleList.Count == 1)
            {
                var foundStyle = ChromeStyle.ChromeStyleSelect(foundStyleList[0]);
                Log.InfoFormat("Style decoded using {0}, style: {1} - {2} - {3}", logMessage, foundStyle.ChromeStyleID, foundStyle.ModelCode, foundStyle.ConsumerFriendlyStyleName);
                return foundStyle;
            }
            else
            {
                // give it another try using options unique to a particular style
                var uniqueOptions = (from opt in chromeOptionList
                                     group opt by opt.OptionCode
                                     into grp
                                     where grp.Count() == 1
                                     select grp.First()).ToList();

                foundOptionList = FilterOptions(uniqueOptions, optionPackage);

                foundStyleList = foundOptionList.Select(x => x.StyleId).Distinct().ToList();


                if (foundStyleList.Count == 1)
                {
                    var foundStyle = ChromeStyle.ChromeStyleSelect(foundStyleList[0]);
                    Log.InfoFormat("Style decoded using {0}, style: {1} - {2} - {3}", logMessage, foundStyle.ChromeStyleID, foundStyle.ModelCode, foundStyle.ConsumerFriendlyStyleName);
                    return foundStyle;
                }
            }

            return null;

        }
        
        /// <summary>
        /// This method should be used and customized at descendants to put in find a unique style based on specific 
        /// attributes of the build data
        /// </summary>
        /// <param name="vehicleBuild"></param>
        /// <param name="chromeStyleList"></param>
        /// <param name="chromeOptionList"></param>
        /// <returns></returns>
        internal virtual ChromeStyle MatchStyle(VehicleBuildData vehicleBuild, 
                                                List<AutoLoad.FordNew.DataAccess.ChromeStyle> chromeStyleList, 
                                                List<ChromeOption> chromeOptionList)
        {

            // find unqiue style from the all of options possible for this VIN
            var foundStyle = FindUniqueStyle(chromeOptionList, vehicleBuild.UnfilteredOptionPackages, "unfiltered Chrome options list");

            if (foundStyle == null)
            {
                // filter by model code
                var filteredOptionList = (from options in chromeOptionList
                                      join styles in chromeStyleList on options.StyleId equals styles.StyleId
                                          where styles.FullStyleCode == GetModelCode(vehicleBuild.OemModelCode)
                                      select options
                                     ).ToList();


                foundStyle = FindUniqueStyle(filteredOptionList.ToList(), vehicleBuild.UnfilteredOptionPackages, "model filtered Chrome options list");

            }


            return foundStyle;
        }

        /// <summary>
        /// allows descendants to manipulate the model code from the OEM site to match chrome's fullStyleCode
        /// </summary>
        /// <param name="modelCode"></param>
        /// <returns></returns>
        internal virtual string GetModelCode(string modelCode)
        {
            return modelCode;
        }

        /// <summary>
        /// filter raw option packages from build data against Chrome options and attributes
        /// </summary>
        /// <param name="packages"></param>  // normally unfilter from build data
        /// <param name="chromeStyleId"></param> // chrome style id 
        /// <param name="minimumMsrp"></param> // minumum MSRP value of options included
        /// <param name="includePhrases"></param> // list of phrases if found in option's PON (description) are included reguardless of MSRP
        /// <returns></returns>
        internal virtual List<OptionPackage> CleanseOptions(List<OptionPackage> packages, int chromeStyleId, decimal minimumMsrp, List<string> includePhrases)
        {

            var optionPackages = packages;

            // filter to only options with a positive MSRP
            if (chromeStyleId > 0)
            {
                // Get all possible options for this style.
                var chromeOptions = ChromeRepository.GetOptions(new[] { chromeStyleId }).ToList();

                // match the unfiltered packages to the best option match (if any)
                var matchedOptons = FilterOptions(chromeOptions, packages);

                // create a list of strings
                //var testPhrase = new List<string> {"package", "collection", "group", "pack", "pkg"};

                // find all options with a positive msrp and replace in vehicleBuild.OptionPackages
                // NOTE: this will completely override optionpackages code, including manufacturer white list
                 optionPackages = (
                                    from chrome in matchedOptons
                                    where
                                        (chrome.Msrp >= minimumMsrp || includePhrases.Any(chrome.Pon.ToLower().Contains))
                                    select new OptionPackage(chrome.OptionCode, chrome.OptionDesc)
                                              ).ToList();

            }

            return optionPackages;
        }

        
        /// <summary>
        /// this method will prepare the options extracted from the build data by filtering them with the following 3 processes
        /// 1. filtering the build options against Chrome's options using the option matching logic
        /// 2. extracting matched options against the white listed options
        /// 3. filtering the matched options with the msrp, include and exclude phrases
        /// the matched whitelist options and the matched filtered options are combined to return the options that should be load on the vehicle
        /// </summary>
        /// <param name="packages">the unfiltered packages from the build data</param>
        /// <param name="chromeOptions">the available options from Chrome for this vehicles style</param>
        /// <param name="whiteListOptionCodes">options extracted from the manufacture white list</param>
        /// <param name="optionFilter">options filters autoload setting that include min MSRP, include phrases and exclude phrases</param>
        /// <returns></returns>
        internal virtual List<OptionPackage> SanitizeOptions(List<OptionPackage> packages, List<ChromeOption> chromeOptions, List<string> whiteListOptionCodes, OptionFilter optionFilter)
        {

            // filter the options against chromeOptions using the optionmatch logic
            var matchedOptons = FilterOptions(chromeOptions, packages);

            // special rule to be backwardly compatibility for manufactures w/o a whilte list and don't have inlcude filters
            if (whiteListOptionCodes.Count == 0 && optionFilter.MinumumMsrp == null && optionFilter.IncludePhrases.Count == 0)
                optionFilter.MinumumMsrp = -10000000; // basically all options will be return if not matched with the exclude phrases list
            
            // find options on the manufactures white list
            var whiteListOptions = from matched in matchedOptons
                                   where whiteListOptionCodes.Contains(matched.OptionCode)
                                   select matched;

            // make sure all phrases are lower for case insensitive comparision
            optionFilter.IncludePhrases = optionFilter.IncludePhrases.ConvertAll(x => x.ToLower());
            optionFilter.ExcludePhrases = optionFilter.ExcludePhrases.ConvertAll(x => x.ToLower());
            
            // find option using option filters
            var cleansedOptions = from matched in matchedOptons
                                  where
                                      (matched.Msrp >= optionFilter.MinumumMsrp ||                          // msrp greater than or equal min MSRP or 
                                      optionFilter.IncludePhrases.Any(matched.Pon.ToLower().Contains))      //  phrase is in PON
                                      && !optionFilter.ExcludePhrases.Any(matched.Pon.ToLower().Contains)  // phrase is not in PON
                                  select matched;

            // combine the whiteList and cleanedLists
            var sanitizedOptions = (from sanitized in whiteListOptions.Concat(cleansedOptions).Distinct()
                                    select new OptionPackage(sanitized.OptionCode, sanitized.OptionDesc)).ToList();

            return sanitizedOptions;
        }


    }
}