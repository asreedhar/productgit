using System;
using System.Data;
using System.Xml;
using FirstLook.Common.Data;


namespace FirstLook.LotIntegration.DomainModel
{
    internal static class Database
    {
        public static string Merchandising { get { return "Merchandising"; } }
        public static string IMT { get { return "IMT"; } }


        public static IDataConnection GetConnection(string databaseName)
        {
            return SimpleQuery.ConfigurationManagerConnection(databaseName);
        }

        public static void AddWithValue(IDbCommand command, string parameterName, object parameterValue, DbType dbType)
        {
            SimpleQuery.AddWithValue(command, parameterName, parameterValue, dbType);
        }


        public static void AddNullValue(IDbCommand command, string parameterName, DbType dbType)
        {
            SimpleQuery.AddNullValue(command, parameterName, dbType);
        }

        public static bool getPossibleNull(IDataRecord reader, string column)
        {
            if (reader.IsDBNull(reader.GetOrdinal(column)))
            {
                return false;
            }
            return 1 <= reader.GetByte(reader.GetOrdinal(column));
        }

        public static int getPossibleNullInt(IDataRecord reader, string column)
        {
            if (reader.IsDBNull(reader.GetOrdinal(column)))
            {
                return 0;
            }
            return reader.GetInt32(reader.GetOrdinal(column));
        }

        public static double getPossibleNullDouble(IDataRecord reader, string column)
        {
            if (reader.IsDBNull(reader.GetOrdinal(column)))
            {
                return 0;
            }
            return reader.GetDouble(reader.GetOrdinal(column));
        }

        public static decimal getPossibleNullDecimal(IDataRecord reader, string column)
        {
            if (reader.IsDBNull(reader.GetOrdinal(column)))
            {
                return 0;
            }
            return reader.GetDecimal(reader.GetOrdinal(column));
        }

        public static string getPossibleNullString(IDataRecord reader, string column)
        {
            if (reader.IsDBNull(reader.GetOrdinal(column)))
            {
                return string.Empty;
            }
            return reader.GetString(reader.GetOrdinal(column));
        }

        public static decimal? getNullableDecimal(IDataRecord reader, string column)
        {
            int ord = reader.GetOrdinal(column);
            if (reader.IsDBNull(ord))
            {
                return null;
            }

            return reader.GetDecimal(ord);

        }

        public static DateTime? getNullableDateTime(IDataRecord reader, string column)
        {
            int ord = reader.GetOrdinal(column);
            if (reader.IsDBNull(ord))
            {
                return null;
            }

            return reader.GetDateTime(ord);

        }
        public static float? getNullableFloat(IDataRecord reader, string column)
        {
            int ord = reader.GetOrdinal(column);
            if (reader.IsDBNull(ord))
            {
                return null;
            }

            return reader.GetFloat(ord);

        }
        public static double? getNullableDouble(IDataRecord reader, string column)
        {
            int ord = reader.GetOrdinal(column);
            if (reader.IsDBNull(ord))
            {
                return null;
            }

            return reader.GetDouble(ord);

        }
        public static bool? getNullableBoolean(IDataRecord reader, string column)
        {
            int ord = reader.GetOrdinal(column);
            if (reader.IsDBNull(ord))
            {
                return null;
            }

            return reader.GetBoolean(ord);

        }
        public static decimal? getNullableDecimal(object o)
        {
            if (o == DBNull.Value)
            {
                return null;
            }
            return (decimal)o;
        }
        public static int? getNullableInt(object o)
        {
            if (o == DBNull.Value)
            {
                return null;
            }
            return (int)o;
        }
        public static int? getNullableInt(IDataRecord reader, string column)
        {
            int ord = reader.GetOrdinal(column);
            if (reader.IsDBNull(ord))
            {
                return null;
            }

            return reader.GetInt32(ord);

        }
        public static int? getNullableInt(XmlNode node, string column)
        {
            XmlAttribute xa = node.Attributes[column];
            if (xa == null || string.IsNullOrEmpty(xa.Value))
            {
                return null;
            }

            return Convert.ToInt32(node.Attributes[column].Value);

        }
        public static decimal? getNullableDecimal(XmlNode node, string column)
        {
            XmlAttribute xa = node.Attributes[column];
            if (xa == null || string.IsNullOrEmpty(xa.Value))
            {
                return null;
            }

            return Convert.ToDecimal(node.Attributes[column].Value);

        }
        public static bool? getNullableBoolean(XmlNode node, string column)
        {
            XmlAttribute xa = node.Attributes[column];
            if (xa == null || string.IsNullOrEmpty(xa.Value))
            {
                return null;
            }
            string val = node.Attributes[column].Value;
            if (val.Length == 1)
            {
                return Convert.ToBoolean(Convert.ToInt32(val));
            }
            return Convert.ToBoolean(val);

        }

        public static string getNullableString(IDataRecord reader, string column)
        {
            if (reader.IsDBNull(reader.GetOrdinal(column)))
            {
                return string.Empty;
            }
            return reader.GetString(reader.GetOrdinal(column));
        }

        public static bool HasColumn(this IDataRecord dr, string columnName)
        {
            for (int i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            return false;
        }

    }
}