﻿using Autofac;
using FirstLook.Common.Core;
using FirstLook.LotIntegration.DomainModel.Tasks;
using MAX.ExternalCredentials;

namespace FirstLook.LotIntegration.DomainModel
{
    public class LotIntegrationServiceModule : Module   
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AutoLoadThreadStarter>().As<IAutoLoadThreadStarter>();
            builder.Register(c => new AutoloadRepository()).Named<IAutoloadRepository>("DbAccess");
            builder.Register(c => new AutoloadCachedRepository(
                                      c.Resolve<ICache>(),
                                      c.ResolveNamed<IAutoloadRepository>("DbAccess")
                                      )).SingleInstance().As<IAutoloadRepository>();
            base.Load(builder);
        }
    }
}
