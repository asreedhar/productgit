using System;

namespace FirstLook.LotIntegration.DomainModel
{
    public class VehicleBuildDataCollectionException : Exception
    {
        private string siteName;
        private string vin;
        private string status;

        public override string Message
        {
            get
            {
                return string.Format("VIN {0} failed on site {1} with collection status of {2}", vin, siteName, status);
            }
        }
        public string SiteName
        {
            get { return siteName; }
            set { siteName = value; }
        }

        public string Vin
        {
            get { return vin; }
            set { vin = value; }
        }

        public VehicleBuildDataCollectionException(string siteName, string vin, string status)
        {
            this.siteName = siteName;
            this.vin = vin;
            this.status = status;
        }

        public string Status
        {
            get { return status; }
            set { status = value; }
        }
    }
}
