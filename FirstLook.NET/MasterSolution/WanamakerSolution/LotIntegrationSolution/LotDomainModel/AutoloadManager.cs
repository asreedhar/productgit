﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.DataAccess;
using MAX.BuildRequest.VehicleDataProviders;
using Merchandising.Messages;

namespace FirstLook.LotIntegration.DomainModel
{
    public class AutoloadManager
    {
        private readonly IAutoloadRepository _repository;

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public AutoloadManager(IAutoloadRepository repository)
        {
            _repository = repository;
        }

        public AutoloadStatus SetAutoloadStart(int businessUnitId, int inventoryId,  string memberLogin)
         {

             using ( var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
             {
                 sqlConnection.Open();

                 using (SqlCommand cmd = sqlConnection.CreateCommand())
                 {
                     cmd.CommandText = "[builder].[AutoloadStatus#Save]";

                     cmd.CommandType = CommandType.StoredProcedure;

                     cmd.Parameters.AddWithValue("BusinessUnitId", businessUnitId);
                     cmd.Parameters.AddWithValue("InventoryId", inventoryId);
                     cmd.Parameters.AddWithValue("StatusTypeId", Status.Pending);
                     cmd.Parameters.AddWithValue("MemberLogin", memberLogin);

                     cmd.ExecuteNonQuery();
                 }
             }

             var autoloadStatus = _repository.Fetch(businessUnitId, inventoryId);

            return autoloadStatus;


         }

        public AutoloadStatus SetAutoloadEnd(int businessUnitId, int inventoryId, Status statusType, string memberLogin)
         {

             using ( var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
             {
                 sqlConnection.Open();

                 using (SqlCommand cmd = sqlConnection.CreateCommand())
                 {
                     cmd.CommandText = "builder.AutoloadStatus#SetEnd";

                     cmd.CommandType = CommandType.StoredProcedure;

                     cmd.Parameters.AddWithValue("BusinessUnitId", businessUnitId);
                     cmd.Parameters.AddWithValue("InventoryId", inventoryId);
                     cmd.Parameters.AddWithValue("StatusTypeId", statusType);
                     cmd.Parameters.AddWithValue("MemberLogin", memberLogin);

                     cmd.ExecuteNonQuery();
                 }
             }

             var autoloadStatus = _repository.Fetch(businessUnitId, inventoryId);
             return autoloadStatus;
            

            
         }

        public AutoLoadInventory[] GetAllWithErrors()
        {
            return _repository.GetAllWithErrors();
        }

        public List<int> GetInventoryWithNoAutoloadStatus(int businessUnitId)
        {
            return _repository.GetInventoryWithNoAutoloadStatus(businessUnitId);
        }

        public IList<int> GetAllWithErrors(int businessUnitId)
        {
             return _repository.GetAllWithErrors(businessUnitId);
        }

        public AutoLoadInventory[] GetOpenRequeue(int? businessUnitId)
        {
            if (businessUnitId.HasValue && businessUnitId == 0)
                businessUnitId = null;

            return _repository.GetOpenRequeues(businessUnitId).ToArray();

        }

        public bool IsBatchAutoloadEnabled(int businessUnitId)
        {
            return _repository.IsBatchAutoloadEnabled(businessUnitId);
        }

        //Gets the less granular state the autoloadStatusTypeId reprsents.  Specified here https://incisent.fogbugz.com/default.asp?W2660
        //Changed https://incisent.fogbugz.com/default.asp?19605
        public static string GetAutoloadState(int autoLoadStatusTypeId)
        {
             if (!Enum.IsDefined(typeof(Status), autoLoadStatusTypeId)) return string.Empty;

             var autoloadStatusType = (Status) autoLoadStatusTypeId;

             if (autoloadStatusType == Status.Successful) return "Autoloaded";

            return string.Empty;
        }

        public static bool IsInvalidCredentials(int autoLoadStatusTypeId)
        {
            if (!Enum.IsDefined(typeof(Status), autoLoadStatusTypeId)) return false;
            var autoloadStatusType = (Status) autoLoadStatusTypeId;
            return autoloadStatusType == Status.InvalidCredentials;
        }

        public List<string> GetSupportedMakeList()
        {
            return _repository.GetSupportedMakeList();
        }

        public List<ManufacturerAutoLoadSettings> GetSupportedManufacturerList()
        {
            return _repository.GetSupportedManufacturerList();
        }

        public  bool MakeIsBatchAutoloadOnly(string make)
        {
            //var agent = VehicleDataProviderFactory.FetchAgent(make);
            //return (agent == Agent.DealerConnect || agent == Agent.VolkswagenHub || agent == Agent.MazdaDcs || agent == Agent.FordConnect) || agent == Agent.ToyotaTechinfo;
            return true;
        }

        public  void UpdateAutoLoadSettings(IEnumerable<int> manufacturerIdList)
        {
            _repository.UpdateAutoLoadSettings(manufacturerIdList);
        }

        public IList<int> GetSlurpeeRequeues(int businessUnitId)
        {
            return _repository.GetSlurpeeRequeues(businessUnitId);
        }

        internal TaskVehicleAutoLoader CreateAutoLoader(Agent agent, int inventoryType, IAdMessageSender adMessageSender, IVehicleReferenceDataRepository vehicleReferenceDataRepository, string fileContent, IVehicleDataProvider vehicleDataProvider )
        {

            TaskVehicleAutoLoader loader;

            switch (agent)
            {
                case Agent.DealerSpeed:
                    loader = new BmwVehicleAutoLoader(_repository, adMessageSender, vehicleReferenceDataRepository, fileContent, vehicleDataProvider);
                    break;
                
                case Agent.GmGlobalConnect:
                    loader = new GeneralMotorsVehicleAutoLoader(_repository, adMessageSender, vehicleReferenceDataRepository, fileContent, vehicleDataProvider);
                    break;
                
                case Agent.DealerConnect:
                    loader = new ChryslerVehicleAutoLoader(_repository, adMessageSender, vehicleReferenceDataRepository, fileContent, vehicleDataProvider);
                    break;
                
                case Agent.FordConnect:
                    if (inventoryType == 1)
                        loader = new FordNewVehicleAutoLoader(_repository, adMessageSender, vehicleReferenceDataRepository, fileContent, vehicleDataProvider);
                    else
                        loader = new FordUsedVehicleAutoLoader(_repository, adMessageSender, vehicleReferenceDataRepository, fileContent, vehicleDataProvider);
                    break;
                
                case Agent.MazdaDcs:
                    loader = new MazdaVehicleAutoLoader(_repository, adMessageSender, vehicleReferenceDataRepository, fileContent, vehicleDataProvider);
                    break;
                
                case Agent.AccessAudi:
                    loader = new AudiVehicleAutoLoader(_repository, adMessageSender, vehicleReferenceDataRepository, fileContent, vehicleDataProvider);
                    break;
                
                case Agent.HyundaiTechInfo:
                    loader = new HyundaiVehicleAutoLoader(_repository, adMessageSender, vehicleReferenceDataRepository, fileContent, vehicleDataProvider);
                    break;
                
                case Agent.NatDealerDaily:
                case Agent.SetDealerDaily:
                    loader = new ToyotaVehicleAutoLoader(_repository, adMessageSender, vehicleReferenceDataRepository, fileContent, vehicleDataProvider);
                    break;

                case Agent.NissanNorthAmerica:
                    loader = new NissanVehicleAutoLoader(_repository, adMessageSender, vehicleReferenceDataRepository, fileContent, vehicleDataProvider);
                    break;
                    
                default:
                    loader = new TaskVehicleAutoLoader(_repository, adMessageSender, vehicleReferenceDataRepository, fileContent, vehicleDataProvider);
                    break;
            }

            return loader;
        }



    }
}
