using Merchandising.Messages;

namespace FirstLook.LotIntegration.DomainModel
{
    //[Serializable]
    public class BuildDataVehicleRequest
    {
        public BuildDataVehicleRequest(int inventoryId, string make, string vin, int inventoryType, Agent site)
        {
            InventoryId = inventoryId;
            Make = make;
            Vin = vin;
            InventoryType = inventoryType;
            Site = site;

        }

        public int InventoryType { get; private set; }

        public int InventoryId { get; set; }

        public string Make { get; set; }

        public string Vin { get; set; }

        public Agent Site { get; set; }
    }
}