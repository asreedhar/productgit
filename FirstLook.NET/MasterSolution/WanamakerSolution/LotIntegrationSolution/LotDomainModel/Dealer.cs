using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using FirstLook.Common.Data;

namespace FirstLook.LotIntegration.DomainModel
{
    public class Dealer
    {
        public int DealerId { get; set; }
        public string Name { get; set; }

        public static List<Dealer> FetchAllWithWebLoadingIncomplete(bool requireAutoBuildEnabled)
        {
            
            using (IDataConnection con = Database.GetConnection(Database.Merchandising))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    //for now, use the fact that they have a sitecredential as a proxy for requireAutoBuildEnabled
                    cmd.CommandText = @"
                        SELECT DISTINCT bu.businessUnitId, bu.businessUnit
                          FROM settings.Merchandising sm
                               LEFT JOIN [settings].[Dealer_SiteCredentials] dsc ON dsc.businessUnitId = sm.businessUnitId
                               JOIN Interface.BusinessUnit bu ON bu.businessUnitId = sm.businessUnitId
                         WHERE (@requireAutoBuildEnabled = 0 OR NOT dsc.businessUnitId IS NULL)
                        ";
                    cmd.CommandType = CommandType.Text;
                    Database.AddWithValue(cmd, "requireAutoBuildEnabled", requireAutoBuildEnabled, DbType.Boolean);

                    con.Open();
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        List<Dealer> retList = new List<Dealer>();
                        while (reader.Read())
                        {
                            retList.Add(new Dealer(){DealerId = (int)reader["businessUnitId"], Name = (string)reader["businessUnit"]});

                        }
                        return retList;
                    }
                }

            }
            
        }
    }

}
