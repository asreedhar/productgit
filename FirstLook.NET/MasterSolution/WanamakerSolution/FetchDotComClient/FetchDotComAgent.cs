﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using FetchDotComClient.VehicleBuild;
using log4net;
using Merchandising.Messages;

namespace FetchDotComClient
{
    public static class FetchDotComAgent
    {
        #region Logging

        private static readonly ILog Log = LogManager.GetLogger(typeof(FetchDotComAgent).FullName);

        #endregion


        //public static VehicleResponse GetHttpVehicleResponse(IQueue<FetchAuditMessage> auditQueue, Agent agent, string userName, string password, string vin, string dealerCode, int inventoryType, out string xmlResponse, out string url)
        //{
        //    url = GetFetchUrl(agent, userName, password, vin, dealerCode, inventoryType);

        //    Log.DebugFormat("Calling Fetch URL: {0}", url);

        //    var request = (HttpWebRequest)WebRequest.Create(url);
        //    request.Method = WebRequestMethods.Http.Post;
        //    request.ContentType = "application/x-www-form-urlencoded";

        //    if (agent == Agent.DealerConnect
        //    ||  agent == Agent.ToyotaTechinfo) // Chrysler & Toyota use a slower fetch agent, requiring a higher timeout
        //        request.Timeout = 80000;
        //    else
        //        request.Timeout = 30000;

        //    xmlResponse = String.Empty;
        //    using (WebResponse response = request.GetResponse())
        //    using (Stream responseStream = response.GetResponseStream())
        //    {
        //        TextReader reader = new StreamReader(responseStream);
        //        xmlResponse = reader.ReadToEnd();
        //    }

        //    XmlDocument xmlDocument = new XmlDocument();
        //    xmlDocument.LoadXml(xmlResponse);
        //    VehicleResponse vehicleResponse = GetVehicleBuildDataFromFetchXml(xmlDocument);

        //    auditQueue.SendMessage(new FetchAuditMessage(vin, xmlResponse), MethodBase.GetCurrentMethod().DeclaringType.FullName);
        //    return vehicleResponse;
        //}

        //public static VehicleResponse GetVehicleData(Agent agent, string userName, string password, string vin, string dealerCode, int inventoryType )
        //{
        //    var factory = Registry.Resolve<IQueueFactory>();
        //    var fileStoreFactory = Registry.Resolve<IFileStoreFactory>();
        //    var autoloadFileStore = fileStoreFactory.CreateAutoLoadAudit();
        //    VehicleResponse vehicleResponse;

        //    string xmlResponse;
            
        //    // FB: 30440 check if an audit file exist prior to fetching the same data again
        //    if (LoadDataExists(autoloadFileStore, vin))
        //    {

        //        Log.InfoFormat("Fetch agent: Audit data found for vin:{0}", vin );

        //        xmlResponse = AutoLoadAudit.Fetch(autoloadFileStore, vin);

        //        XmlDocument xmlDocument = new XmlDocument();
        //        xmlDocument.LoadXml(xmlResponse);
        //        vehicleResponse = GetVehicleBuildDataFromFetchXml(xmlDocument);

        //    }
        //    else // a valid audit file does not exists, go ahead and pull the data
        //    {
        //        string url;

        //        vehicleResponse = GetHttpVehicleResponse(factory.CreateAutoLoadAudit(), agent, userName, password, vin, dealerCode, inventoryType, out xmlResponse, out url);

        //        string parameters = url.Substring(url.IndexOf("?") + 1);
        //        LogResponseStatus(vehicleResponse.ResponseStatus, parameters);
        //    }

        //    return vehicleResponse;
        //}

        //private static string GetMainFetchUrl(string agenturl, string userName, string password, string vin)
        //{
        //    return string.Format("https://incisent.fetch.com/agent/execute?_item={0}&inputUsername={1}&inputPassword={2}&inputVin={3}",
        //      agenturl, userName, password, vin);
        //}

        //public static string GetFetchUrl(Agent agent, string userName, string password, string vin, string dealerCode, int inventoryType)
        //{
        //    switch( agent )
        //    {
        //        case Agent.DealerSpeed:
        //            return GetMainFetchUrl("incisent/feedgrouppoc/feeds/dealerspeed/f_dealerspeed", userName, password, vin);
                
        //        case Agent.GmGlobalConnect:
        //            return GetMainFetchUrl("incisent/feedgrouppoc/feeds/gmglobalconnect/f_gmglobalconnect", userName, password, vin);
                
        //        case Agent.DealerConnect: // inputOdometer is a required parameter for the Fetch.com robot, but the value is not important - defaulting to 0 for all requests.
        //            return GetMainFetchUrl("incisent/feedgrouppoc/feeds/chryslerdealerconnect/f_chryslerdealerconnect", userName, password, vin) + "&inputOdometer=0";
                
        //        case Agent.MazdaDcs:
        //            return GetMainFetchUrl("incisent/feedgrouppoc/feeds/mazdadcs/f_mazdadcs", userName, password, vin);
                
        //        case Agent.VolkswagenHub:
        //            return GetMainFetchUrl("incisent/feedgrouppoc/feeds/volkswagenhub/f_volkswagenhub", userName, password, vin);
                
        //        case Agent.FordConnect:
        //            if (inventoryType == 1) // new
        //                return GetMainFetchUrl("incisent/feedgrouppoc/feeds/fordnewcars/f_fordnewcars", userName, password, vin) + "&inputDealerCode="+dealerCode;
        //            if (inventoryType == 2) // used
        //                return GetMainFetchUrl("incisent/feedgrouppoc/feeds/fordusedcars/f_fordusedcars", userName, password, vin);
        //            else
        //                return "";
                
        //        case Agent.ToyotaTechinfo:
        //            return GetMainFetchUrl( "incisent/feedgrouppoc/feeds/toyotatechinfo/f_toyotatechinfo", userName, password, vin );

        //        default:
        //            return "";
        //    }
        //}

        public static VehicleResponse GetVehicleBuildDataFromFetchXml(XmlDocument xmlDocument)
        {
            // instantiate a response to build up...
            var vehicleResponse = new VehicleResponse();
            
            // get an xDocument version of our xml
            XDocument xDocument = XDocument.Parse(xmlDocument.OuterXml);
            
            // check for errors, set response status
            vehicleResponse.ResponseStatus = GetRequestStatus(xDocument);

            // if we had a successful response, get the vehicle data
            if (vehicleResponse.ResponseStatus == Status.Successful)
            {
                vehicleResponse.VehicleBuildData = GetVehicleBuildData(xDocument);
            }

            return vehicleResponse;
        }

        private static VehicleBuildData GetVehicleBuildData(XContainer xDocument)
        {
            if (xDocument.Descendants("Vehicle").First() == null) throw new ApplicationException("Vehicle element not found in fetch xml");

            var xVehicle = xDocument.Descendants("Vehicle").First();

            // We need these nodes, if we don't have any of them, throw an exception
            if (xVehicle.Element("VIN") == null) throw new ApplicationException("VIN not found, but is required.");

            // load the VIN
            string vin = xVehicle.Element("VIN").Value.Trim();

            //------------------------------------------------
            // these are optional:
            string trim = null;
            if ( xVehicle.Element("Trim")!=null)
                trim = xVehicle.Element("Trim").Value.Trim();

            decimal? estimatedMsrp = null;
            if (xVehicle.Element("MSRP") != null)
                estimatedMsrp = decimal.Parse(xVehicle.Element("MSRP").Value);

            string oemModelCode = null;
            if (xVehicle.Element("OEM_Model_Code") != null)
                oemModelCode = xVehicle.Element("OEM_Model_Code").Value;

            // interior color
            List<OptionPackage> optionPackages =
                (from option in xVehicle.Descendants("Option")
                 select new OptionPackage
                            {
                                OemCode = option.Element("Code").Value.Trim(),
                                Description = option.Element("Description") != null ? option.Element("Description").Value.Trim() : string.Empty
                            }).ToList();

            InteriorColor intColor = null;
            var xInteriorColor = xVehicle.Element("InteriorColor");
            if (xInteriorColor != null && xInteriorColor.HasElements)
                intColor = new InteriorColor(xInteriorColor.Element("Code").Value.Trim(),
                                             xInteriorColor.Element("Name") != null ?  xInteriorColor.Element("Name").Value.Trim() : string.Empty,
                                             xInteriorColor.Element("Description") != null ? xInteriorColor.Element("Description").Value.Trim() : string.Empty);

            // exterior color
            ExteriorColor extColor = null;
            var xExteriorColor = xVehicle.Element("ExteriorColor");
            if (xExteriorColor != null && xExteriorColor.HasElements)
                extColor = new ExteriorColor(xExteriorColor.Element("Code").Value.Trim(),
                                             xExteriorColor.Element("Description") != null ? xExteriorColor.Element("Description").Value.Trim() : string.Empty);

            // (end of optional properties)
            //------------------------------------------------

            return new VehicleBuildData(vin, trim, oemModelCode, intColor, extColor, optionPackages, estimatedMsrp);
        }

        public static Status GetRequestStatus(string xmlString)
        {
            XContainer xDocument = XDocument.Parse(xmlString);

            return GetRequestStatus(xDocument);
        }

        public static Status GetRequestStatus(XContainer document)
        {
            List<string> errorCodes = document.Descendants("ErrorCode").Select(error => error.Value).ToList();
            
            if( errorCodes.Contains( "101" ) )
            {
                return Status.InvalidCredentials;
            }

            if ( errorCodes.Contains("102") )
            {
                return Status.InsufficientPermissions;
            }

            IEnumerable<XElement> vehicles = document.Descendants( "Vehicle" ).Select( vehicle => vehicle );
            if( vehicles.Count() == 0 && !errorCodes.Contains( "301" ) )
            {
                return Status.NoVehicleFound;
            }

            if( errorCodes.Count > 0 )
            {
                return Status.UnknownError;
            }

            return Status.Successful;
        }

    }
}
