namespace FetchDotComClient.VehicleBuild
{
    public class ExteriorColor 
    {
        private readonly Color _color;

        public ExteriorColor(string oemCode, string description)
        {
            _color = new Color(oemCode, description);
        }

        public string OemCode
        {
            get { return _color.OemCode; }
            set { _color.OemCode = value; }
        }

        public string Description        
        { 
            get { return _color.Description;}
        }
    }
}