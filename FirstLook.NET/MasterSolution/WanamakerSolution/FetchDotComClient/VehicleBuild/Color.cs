namespace FetchDotComClient.VehicleBuild
{
    internal class Color
    {
        internal Color(string oemCode, string description)
        {
            OemCode = oemCode;
            Description = description;
        }

        internal string OemCode { get; set; }
        internal string Description { get; set; }
    }
}