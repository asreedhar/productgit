using System;

namespace FetchDotComClient.VehicleBuild
{
    public interface IOemOption
    {
        string OemCode { get; set; }
        string Description { get; set; }
    }
}