using Merchandising.Messages;

namespace FetchDotComClient.VehicleBuild
{
    public class VehicleResponse
    {
        public Status ResponseStatus { get; internal set; }
        public VehicleBuildData VehicleBuildData {get; internal set;}
    }

}