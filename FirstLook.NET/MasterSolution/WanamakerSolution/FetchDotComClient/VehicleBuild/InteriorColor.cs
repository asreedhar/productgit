namespace FetchDotComClient.VehicleBuild
{
    public class InteriorColor 
    {
        private readonly Color _color;

        public InteriorColor(string oemCode, string name,  string description)
        {
            _color = new Color(oemCode, description);
            Name = name;
        }

        public string OemCode
        { 
            get { return _color.OemCode;}
            set { _color.OemCode = value;}
        }

        public string Name{ get; set;}

        public string Description        
        { 
            get { return _color.Description;}
            set { _color.OemCode = value; }
        }
    }
}