﻿using System;
using System.Collections.Generic;

namespace FetchDotComClient.VehicleBuild
{
    public interface IVehicleBuildData
    {
        string Vin { get; }
        string Trim { get; }
        string OemModelCode { get; set; }
        InteriorColor InteriorColor { get; }
        ExteriorColor ExteriorColor { get; }
        IList<OptionPackage> OptionPackages { get; }
        decimal? EstimatedMSRP { get; }
    }
}
