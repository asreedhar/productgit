using System;
using System.Collections.Generic;

namespace FetchDotComClient.VehicleBuild
{
    public class VehicleBuildData : IVehicleBuildData
    {
        public VehicleBuildData(string vin, string trim, string oemModelCode, InteriorColor interiorColor, ExteriorColor exteriorColor, IList<OptionPackage> optionPackages, decimal? estimatedMsrp)
        {
            Vin = vin;
            Trim = trim;
            OemModelCode = oemModelCode;
            InteriorColor = interiorColor;
            ExteriorColor = exteriorColor;
            OptionPackages = optionPackages;
            EstimatedMSRP = estimatedMsrp;
        }

        public string Vin { get; private set; }
        public string Trim { get; private set; }
        public string OemModelCode { get; set; }
        public InteriorColor InteriorColor { get; private set; }
        public ExteriorColor ExteriorColor { get; private set; }
        public IList<OptionPackage> OptionPackages { get; private set; }
        public decimal? EstimatedMSRP { get; private set; }
    }
}