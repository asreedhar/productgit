namespace FetchDotComClient.VehicleBuild
{
    public class OptionPackage : IOemOption
    {
        public string OemCode {get; set;}
        public string Description{get; set;}
    }
}