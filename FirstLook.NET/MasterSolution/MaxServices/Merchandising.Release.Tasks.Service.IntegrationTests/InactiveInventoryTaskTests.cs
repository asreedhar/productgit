﻿using System;
using System.Collections.Generic;
using Autofac;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.Tasks;
using Merchandising.Messages;
using Merchandising.Messages.InventoryDocuments;
using NUnit.Framework;

namespace Merchandising.Release.Tasks.Service.IntegrationTests
{
    [TestFixture]
    public class InactiveInventoryTaskTests
    {
        private IContainer _container;

        [TestFixtureSetUp]
        public void SetUp()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new MerchandisingMessagesModule());
            builder.RegisterType<DashboardThreadStarter>().As<IDashboardThreadStarter>();

            _container = builder.Build();
            Registry.RegisterContainer(_container);
        }

        [Test]
        public void NoDealer()
        {
            var task = new InactiveInventoryTask(_container.Resolve<IQueueFactory>(),
                _container.Resolve<IFileStoreFactory>());
            task.Process(new InactiveInventoryTaskStartMessage(new List<int>(), null));            
        }

        [Test]
        public void DefaultStartDate()
        {
            var task = new InactiveInventoryTask(_container.Resolve<IQueueFactory>(),
                _container.Resolve<IFileStoreFactory>());
            task.Process(new InactiveInventoryTaskStartMessage(new List<int> { 101590 }, null));
        }

        [Test]
        public void SpecificStartDate()
        {
            var task = new InactiveInventoryTask(_container.Resolve<IQueueFactory>(), 
                _container.Resolve<IFileStoreFactory>());
            task.Process(new InactiveInventoryTaskStartMessage(new List<int> { 101590 }, DateTime.Now));
        }
    }
}
