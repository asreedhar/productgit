﻿using System;

namespace FirstLook.Valuation.DomainModel.Broker.Commands.TransferObjects
{
    [Serializable]
    public enum ClientTypeDto
    {
        Undefined,
        Dealer,
        Seller
    }
}
