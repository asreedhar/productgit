﻿using System;

namespace FirstLook.Valuation.DomainModel.Broker.Commands.TransferObjects
{
    [Serializable]
    public class BrokerDto
    {
        private string _handle;
        private ClientDto _client;

        public string Handle
        {
            get { return _handle; }
            set { _handle = value; }
        }

        public ClientDto Client
        {
            get { return _client; }
            set { _client = value; }
        }
    }
}
