using System;
using System.Collections.Generic;
using FirstLook.Valuation.DomainModel.Common.Commands.TransferObjects;

namespace FirstLook.Valuation.DomainModel.Broker.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class BrokerQueryResultsDto : PaginatedResultsDto
    {
        private BrokerQueryArgumentsDto _arguments;
        private List<BrokerDto> _results = new List<BrokerDto>();

        public BrokerQueryArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public List<BrokerDto> Results
        {
            get { return _results; }
            set { _results = value; }
        }
    }
}