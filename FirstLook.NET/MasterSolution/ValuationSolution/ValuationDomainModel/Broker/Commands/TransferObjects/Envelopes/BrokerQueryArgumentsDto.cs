using System;
using FirstLook.Valuation.DomainModel.Common.Commands.TransferObjects;

namespace FirstLook.Valuation.DomainModel.Broker.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class BrokerQueryArgumentsDto : PaginatedArgumentsDto
    {
        private ClientTypeDto _clientType;
        private ClientDto _example;

        public ClientTypeDto ClientType
        {
            get { return _clientType; }
            set { _clientType = value; }
        }

        public ClientDto Example
        {
            get { return _example; }
            set { _example = value; }
        }
    }
}