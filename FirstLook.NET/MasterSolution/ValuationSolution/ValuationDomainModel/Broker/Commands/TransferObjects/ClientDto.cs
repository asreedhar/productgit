﻿using System;

namespace FirstLook.Valuation.DomainModel.Broker.Commands.TransferObjects
{
    [Serializable]
    public class ClientDto
    {
        private string _name;
        private AddressDto _address;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public AddressDto Address
        {
            get { return _address; }
            set { _address = value; }
        }
    }
}
