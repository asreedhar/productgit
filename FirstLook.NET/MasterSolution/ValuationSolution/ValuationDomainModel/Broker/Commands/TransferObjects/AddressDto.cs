﻿using System;

namespace FirstLook.Valuation.DomainModel.Broker.Commands.TransferObjects
{
    [Serializable]
    public class AddressDto
    {
        private string _deliveryAddressLine1 = string.Empty;
        private string _deliveryAddressLine2 = string.Empty;
        private string _city = string.Empty;
        private string _state = string.Empty;
        private string _zipCode = string.Empty;

        public string DeliveryAddressLine1
        {
            get { return _deliveryAddressLine1; }
            set { _deliveryAddressLine1 = value; }
        }

        public string DeliveryAddressLine2
        {
            get { return _deliveryAddressLine2; }
            set { _deliveryAddressLine2 = value; }
        }

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }

        public string ZipCode
        {
            get { return _zipCode; }
            set { _zipCode = value; }
        }
    }
}
