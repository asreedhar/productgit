﻿using FirstLook.Common.Core.Command;
using FirstLook.Valuation.DomainModel.Broker.Commands.TransferObjects.Envelopes;
using FirstLook.Valuation.DomainModel.Common.Commands;

namespace FirstLook.Valuation.DomainModel.Broker.Commands
{
    public interface ICommandFactory : IPolicyProvider
    {
        ICommand<BrokerQueryResultsDto, BrokerQueryArgumentsDto>
            CreateBrokerQueryCommand();
    }
}
