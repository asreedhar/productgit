﻿using System;

namespace FirstLook.Valuation.DomainModel.Appraisal.Commands.TransferObjects
{
    [Serializable]
    public enum GenderDto
    {
        Undefined,
        Male,
        Female
    }
}
