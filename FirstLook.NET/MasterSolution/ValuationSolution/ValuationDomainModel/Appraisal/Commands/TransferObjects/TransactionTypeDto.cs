﻿using System;

namespace FirstLook.Valuation.DomainModel.Appraisal.Commands.TransferObjects
{
    [Serializable]
    public enum TransactionTypeDto
    {
        Undefined,
        TradeIn,
        Purchase
    }
}
