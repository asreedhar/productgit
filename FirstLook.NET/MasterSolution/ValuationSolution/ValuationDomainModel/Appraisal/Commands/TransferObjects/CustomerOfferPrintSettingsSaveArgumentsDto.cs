﻿using System;

namespace FirstLook.Valuation.DomainModel.Appraisal.Commands.TransferObjects
{
    [Serializable]
    public class CustomerOfferPrintSettingsSaveArgumentsDto : AppraisalArgumentsDto
    {
        private CustomerOfferPrintSettingsDto _customerOfferPrintSettings;

        public CustomerOfferPrintSettingsDto CustomerOfferPrintSettings
        {
            get { return _customerOfferPrintSettings; }
            set { _customerOfferPrintSettings = value; }
        }
    }
}
