using System;
using FirstLook.Valuation.DomainModel.Personnel.Commands.TransferObjects;

namespace FirstLook.Valuation.DomainModel.Appraisal.Commands.TransferObjects
{
    [Serializable]
    public class CustomerQueryDto
    {
        private StockTypeDto _stockType;
        private string _stockNumber;
        private PersonDto _salesperson;

        public StockTypeDto StockType
        {
            get { return _stockType; }
            set { _stockType = value; }
        }

        public string StockNumber
        {
            get { return _stockNumber; }
            set { _stockNumber = value; }
        }

        public PersonDto Salesperson
        {
            get { return _salesperson; }
            set { _salesperson = value; }
        }
    }
}
