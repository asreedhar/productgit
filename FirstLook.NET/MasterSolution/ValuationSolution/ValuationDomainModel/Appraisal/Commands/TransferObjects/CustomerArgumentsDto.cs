﻿using System;

namespace FirstLook.Valuation.DomainModel.Appraisal.Commands.TransferObjects
{
    [Serializable]
    public class CustomerArgumentsDto : AppraisalArgumentsDto
    {
        private CustomerDto _customer;

        public CustomerDto Customer
        {
            get { return _customer; }
            set { _customer = value; }
        }
    }
}
