﻿using System;

namespace FirstLook.Valuation.DomainModel.Appraisal.Commands.TransferObjects
{
    [Serializable]
    public class TransactionDecisionDto
    {
        private TransactionDecisionTypeDto _transactionDecisionType;
        private ConditionInfoDto _conditionInfo;

        public TransactionDecisionTypeDto TransactionDecisionType
        {
            get { return _transactionDecisionType; }
            set { _transactionDecisionType = value; }
        }

        public ConditionInfoDto ConditionInfo
        {
            get { return _conditionInfo; }
            set { _conditionInfo = value; }
        }

        private RetailInfoDto _retailInfo;
        private WholesaleInfoDto _wholesaleInfo;

        public RetailInfoDto RetailInfo
        {
            get { return _retailInfo; }
            set { _retailInfo = value; }
        }

        public WholesaleInfoDto WholesaleInfo
        {
            get { return _wholesaleInfo; }
            set { _wholesaleInfo = value; }
        }
    }

    [Serializable]
    public class ConditionInfoDto
    {
        private decimal _estimatedReconditioningCost;
        private string _conditionDescription;

        public decimal EstimatedReconditioningCost
        {
            get { return _estimatedReconditioningCost; }
            set { _estimatedReconditioningCost = value; }
        }

        public string ConditionDescription
        {
            get { return _conditionDescription; }
            set { _conditionDescription = value; }
        }
    }

    [Serializable]
    public class WholesaleInfoDto
    {
        private decimal _wholesalePrice;

        public decimal WholesalePrice
        {
            get { return _wholesalePrice; }
            set { _wholesalePrice = value; }
        }
    }

    [Serializable]
    public class RetailInfoDto
    {
        private decimal _transferPrice;
        private bool _transferForRetailOnly;

        public decimal TransferPrice
        {
            get { return _transferPrice; }
            set { _transferPrice = value; }
        }

        public bool TransferForRetailOnly
        {
            get { return _transferForRetailOnly; }
            set { _transferForRetailOnly = value; }
        }
    }
}
