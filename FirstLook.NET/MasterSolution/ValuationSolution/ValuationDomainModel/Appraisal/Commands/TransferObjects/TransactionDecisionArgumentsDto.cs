﻿using System;

namespace FirstLook.Valuation.DomainModel.Appraisal.Commands.TransferObjects
{
    [Serializable]
    public class TransactionDecisionArgumentsDto : AppraisalArgumentsDto
    {
        private TransactionDecisionDto _transactionDecision;

        public TransactionDecisionDto TransactionDecision
        {
            get { return _transactionDecision; }
            set { _transactionDecision = value; }
        }
    }
}
