﻿using System;

namespace FirstLook.Valuation.DomainModel.Appraisal.Commands.TransferObjects
{
    [Serializable]
    public class ValuationDto
    {
        private DateTime _created;
        private int _value;
        private string _appraiserName;

        public DateTime Created
        {
            get { return _created; }
            set { _created = value; }
        }

        public int Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public string AppraiserName
        {
            get { return _appraiserName; }
            set { _appraiserName = value; }
        }
    }
}
