﻿using System;

namespace FirstLook.Valuation.DomainModel.Appraisal.Commands.TransferObjects
{
    [Serializable]
    public class AppraisalCreateResultsDto
    {
        private AppraisalCreateArgumentsDto _arguments;
        private AppraisalDto _appraisal;

        public AppraisalCreateArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public AppraisalDto Appraisal1
        {
            get { return _appraisal; }
            set { _appraisal = value; }
        }
    }
}
