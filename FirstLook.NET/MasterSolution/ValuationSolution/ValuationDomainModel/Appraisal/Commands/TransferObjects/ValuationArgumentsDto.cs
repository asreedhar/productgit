﻿using System;

namespace FirstLook.Valuation.DomainModel.Appraisal.Commands.TransferObjects
{
    [Serializable]
    public class ValuationArgumentsDto : AppraisalArgumentsDto
    {
        private ValuationDto _valuation;

        public ValuationDto Valuation
        {
            get { return _valuation; }
            set { _valuation = value; }
        }
    }
}
