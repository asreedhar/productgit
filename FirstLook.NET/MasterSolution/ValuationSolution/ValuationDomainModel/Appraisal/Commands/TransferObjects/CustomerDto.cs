﻿using System;

namespace FirstLook.Valuation.DomainModel.Appraisal.Commands.TransferObjects
{
    [Serializable]
    public class CustomerDto
    {
        private string _email;
        private string _firstName;
        private string _lastName;
        private string _phoneNumber;
        private GenderDto _gender;

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { _phoneNumber = value; }
        }

        public GenderDto Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }
    }
}
