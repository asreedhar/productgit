﻿using System;

namespace FirstLook.Valuation.DomainModel.Appraisal.Commands.TransferObjects
{
    [Serializable]
    public enum TransactionDecisionTypeDto
    {
        Undefined,
        PurchaseForRetail,
        PurchaseForWholesaleInternally,
        PurchaseForWholesaleExternally,
        Declined,
        Undecided
    }
}
