﻿using System;

namespace FirstLook.Valuation.DomainModel.Appraisal.Commands.TransferObjects
{
    [Serializable]
    public class CustomerOfferPrintSettingsDto
    {
        private bool _showEquipmentOptions;
        private bool _showPhotos;
        private bool _showMileageAdjustment;
        private bool _showKelleyConsumerValue;

        public bool ShowEquipmentOptions
        {
            get { return _showEquipmentOptions; }
            set { _showEquipmentOptions = value; }
        }

        public bool ShowPhotos
        {
            get { return _showPhotos; }
            set { _showPhotos = value; }
        }

        public bool ShowMileageAdjustment
        {
            get { return _showMileageAdjustment; }
            set { _showMileageAdjustment = value; }
        }

        public bool ShowKelleyConsumerValue
        {
            get { return _showKelleyConsumerValue; }
            set { _showKelleyConsumerValue = value; }
        }

        private bool _showNadaRetail;
        private bool _showNadaTradeIn;
        private bool _showNadaLoan;

        public bool ShowNadaRetail
        {
            get { return _showNadaRetail; }
            set { _showNadaRetail = value; }
        }

        public bool ShowNadaTradeIn
        {
            get { return _showNadaTradeIn; }
            set { _showNadaTradeIn = value; }
        }

        public bool ShowNadaLoan
        {
            get { return _showNadaLoan; }
            set { _showNadaLoan = value; }
        }

        private bool _showBlackBookExtraClean;
        private bool _showBlackBookClean;
        private bool _showBlackBookAverage;
        private bool _showBlackBookFair;

        public bool ShowBlackBookExtraClean
        {
            get { return _showBlackBookExtraClean; }
            set { _showBlackBookExtraClean = value; }
        }

        public bool ShowBlackBookClean
        {
            get { return _showBlackBookClean; }
            set { _showBlackBookClean = value; }
        }

        public bool ShowBlackBookAverage
        {
            get { return _showBlackBookAverage; }
            set { _showBlackBookAverage = value; }
        }

        public bool ShowBlackBookFair
        {
            get { return _showBlackBookFair; }
            set { _showBlackBookFair = value; }
        }

        private bool _showKelleyWholesale;
        private bool _showKelleyRetail;
        private bool _showKelleyTradeIn;
        private bool _showKelleyPrivateParty;

        public bool ShowKelleyWholesale
        {
            get { return _showKelleyWholesale; }
            set { _showKelleyWholesale = value; }
        }

        public bool ShowKelleyRetail
        {
            get { return _showKelleyRetail; }
            set { _showKelleyRetail = value; }
        }

        public bool ShowKelleyTradeIn
        {
            get { return _showKelleyTradeIn; }
            set { _showKelleyTradeIn = value; }
        }

        public bool ShowKelleyPrivateParty
        {
            get { return _showKelleyPrivateParty; }
            set { _showKelleyPrivateParty = value; }
        }

        private bool _showGalvesWholesale;
        private bool _showGalvesMarketReady;

        public bool ShowGalvesWholesale
        {
            get { return _showGalvesWholesale; }
            set { _showGalvesWholesale = value; }
        }

        public bool ShowGalvesMarketReady
        {
            get { return _showGalvesMarketReady; }
            set { _showGalvesMarketReady = value; }
        }
    }
}
