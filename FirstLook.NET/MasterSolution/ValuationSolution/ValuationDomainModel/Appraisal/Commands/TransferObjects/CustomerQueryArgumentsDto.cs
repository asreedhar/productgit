﻿using System;

namespace FirstLook.Valuation.DomainModel.Appraisal.Commands.TransferObjects
{
    [Serializable]
    public class CustomerQueryArgumentsDto : AppraisalArgumentsDto
    {
        private CustomerQueryDto _customerQuery;

        public CustomerQueryDto CustomerQuery
        {
            get { return _customerQuery; }
            set { _customerQuery = value; }
        }
    }
}
