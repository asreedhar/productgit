﻿using System;
using System.Collections.Generic;

namespace FirstLook.Valuation.DomainModel.Appraisal.Commands.TransferObjects
{
    [Serializable]
    public class AppraisalDto
    {
        private int _id;
        private DateTime _created;
        private DateTime _modified;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public DateTime Created
        {
            get { return _created; }
            set { _created = value; }
        }

        public DateTime Modified
        {
            get { return _modified; }
            set { _modified = value; }
        }

        private TransactionTypeDto _transactionType;
        private TransactionDecisionDto _transactionDecision;
        private CustomerDto _customer;
        private CustomerQueryDto _customerQuery;
        private List<CustomerOfferDto> _customerOffers = new List<CustomerOfferDto>();
        private List<ValuationDto> _valuations = new List<ValuationDto>();
        
        // buyer
        // deal completed
        // sold

        public TransactionTypeDto TransactionType
        {
            get { return _transactionType; }
            set { _transactionType = value; }
        }

        public CustomerDto Customer
        {
            get { return _customer; }
            set { _customer = value; }
        }

        public CustomerQueryDto CustomerQuery
        {
            get { return _customerQuery; }
            set { _customerQuery = value; }
        }

        public List<CustomerOfferDto> CustomerOffers
        {
            get { return _customerOffers; }
            set { _customerOffers = value; }
        }

        public List<ValuationDto> Valuations
        {
            get { return _valuations; }
            set { _valuations = value; }
        }

        public TransactionDecisionDto TransactionDecision
        {
            get { return _transactionDecision; }
            set { _transactionDecision = value; }
        }
    }
}
