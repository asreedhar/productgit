﻿using System;

namespace FirstLook.Valuation.DomainModel.Appraisal.Commands.TransferObjects
{
    [Serializable]
    public enum StockTypeDto
    {
        Undefined,
        New,
        Used
    }
}
