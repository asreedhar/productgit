﻿using System;

namespace FirstLook.Valuation.DomainModel.Appraisal.Commands.TransferObjects
{
    [Serializable]
    public class CustomerOfferDto
    {
        private int _value;

        public int Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}
