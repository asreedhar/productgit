﻿using System;

namespace FirstLook.Valuation.DomainModel.Appraisal.Commands.TransferObjects
{
    [Serializable]
    public class AppraisalLoadResultsDto
    {
        private AppraisalLoadArgumentsDto _arguments;
        private AppraisalDto _appraisal;

        public AppraisalLoadArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public AppraisalDto Appraisal1
        {
            get { return _appraisal; }
            set { _appraisal = value; }
        }
    }
}
