﻿using System;

namespace FirstLook.Valuation.DomainModel.Appraisal.Commands.TransferObjects
{
    [Serializable]
    public class CustomerOfferArgumentsDto : AppraisalArgumentsDto
    {
        private CustomerOfferDto _customerOffer;

        public CustomerOfferDto CustomerOffer
        {
            get { return _customerOffer; }
            set { _customerOffer = value; }
        }
    }
}
