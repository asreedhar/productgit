﻿using System;

namespace FirstLook.Valuation.DomainModel.Appraisal.Commands.TransferObjects
{
    [Serializable]
    public class AppraisalCreateArgumentsDto
    {
        private string _brokerHandle;
        private string _vin;

        public string BrokerHandle
        {
            get { return _brokerHandle; }
            set { _brokerHandle = value; }
        }

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }

        private ValuationDto _valuation;
        private TransactionTypeDto _transactionType;
        private TransactionDecisionDto _transactionDecision;

        public ValuationDto Valuation
        {
            get { return _valuation; }
            set { _valuation = value; }
        }

        public TransactionTypeDto TransactionType
        {
            get { return _transactionType; }
            set { _transactionType = value; }
        }

        public TransactionDecisionDto TransactionDecision
        {
            get { return _transactionDecision; }
            set { _transactionDecision = value; }
        }
    }
}
