using System;

namespace FirstLook.Valuation.DomainModel.Appraisal.Commands.TransferObjects
{
    [Serializable]
    public abstract class AppraisalArgumentsDto
    {
        private string _brokerHandle;
        private int _id;

        public string BrokerHandle
        {
            get { return _brokerHandle; }
            set { _brokerHandle = value; }
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
    }
}