﻿using FirstLook.Common.Core.Command;
using FirstLook.Valuation.DomainModel.Appraisal.Commands.TransferObjects;
using FirstLook.Valuation.DomainModel.Common.Commands;
using FirstLook.Valuation.DomainModel.Common.Commands.TransferObjects;

namespace FirstLook.Valuation.DomainModel.Appraisal.Commands
{
    public interface ICommandFactory : IPolicyProvider
    {
        ICommand<AppraisalCreateResultsDto, AppraisalCreateArgumentsDto>
            CreateCreateAppraisalCommand();

        ICommand<AppraisalLoadResultsDto, AppraisalLoadArgumentsDto>
            CreateLoadAppraisalCommand();

        ICommand<NullDto, CustomerArgumentsDto>
            CreateSetCustomerCommand();

        ICommand<NullDto, CustomerOfferArgumentsDto>
            CreateSetCustomerOfferCommand();

        ICommand<NullDto, CustomerQueryArgumentsDto>
            CreateSetCustomerQueryCommand();

        ICommand<NullDto, ValuationArgumentsDto>
            CreateSetValuationCommand();

        ICommand<NullDto, TransactionDecisionArgumentsDto>
            CreateSetTransactionDecisionCommand();

        ICommand<NullDto, CustomerOfferPrintSettingsLoadArgumentsDto>
            CreateLoadCustomerOfferPrintSettingsCommand();

        ICommand<NullDto, CustomerOfferPrintSettingsSaveArgumentsDto>
            CreateSaveCustomerOfferPrintSettingsCommand();
    }
}
