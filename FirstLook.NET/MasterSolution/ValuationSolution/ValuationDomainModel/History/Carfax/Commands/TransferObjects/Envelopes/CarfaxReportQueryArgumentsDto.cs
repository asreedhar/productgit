using System;

namespace FirstLook.Valuation.DomainModel.History.Carfax.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class CarfaxReportQueryArgumentsDto
    {
        private string _brokerHandle;
        private string _vin;

        public string BrokerHandle
        {
            get { return _brokerHandle; }
            set { _brokerHandle = value; }
        }

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }
    }
}
