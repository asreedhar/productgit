using System;

namespace FirstLook.Valuation.DomainModel.History.Carfax.Commands.TransferObjects
{
    [Serializable]
    public enum CarfaxReportTypeDto
    {
        Undefined,
        BTC,
        VHR,
        CIP
    }
}
