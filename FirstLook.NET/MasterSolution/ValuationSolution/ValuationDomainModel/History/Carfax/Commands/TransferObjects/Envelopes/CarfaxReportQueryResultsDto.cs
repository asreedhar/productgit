﻿using System;

namespace FirstLook.Valuation.DomainModel.History.Carfax.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class CarfaxReportQueryResultsDto : CarfaxReportResultsDto
    {
        private CarfaxReportQueryArgumentsDto _arguments;

        public CarfaxReportQueryArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }
    }
}
