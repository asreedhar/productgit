﻿using System;

namespace FirstLook.Valuation.DomainModel.History.Carfax.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class CarfaxReportPurchaseResultsDto : CarfaxReportResultsDto
    {
        private CarfaxReportPurchaseArgumentsDto _arguments;

        public CarfaxReportPurchaseArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }
    }
}
