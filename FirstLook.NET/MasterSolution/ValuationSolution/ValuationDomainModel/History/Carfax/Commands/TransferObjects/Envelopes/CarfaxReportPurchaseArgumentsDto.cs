using System;

namespace FirstLook.Valuation.DomainModel.History.Carfax.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class CarfaxReportPurchaseArgumentsDto : CarfaxReportQueryArgumentsDto
    {
        private CarfaxReportTypeDto _reportType;
        private bool _displayInHotList;

        public CarfaxReportTypeDto ReportType
        {
            get { return _reportType; }
            set { _reportType = value; }
        }

        public bool DisplayInHotList
        {
            get { return _displayInHotList; }
            set { _displayInHotList = value; }
        }
    }
}
