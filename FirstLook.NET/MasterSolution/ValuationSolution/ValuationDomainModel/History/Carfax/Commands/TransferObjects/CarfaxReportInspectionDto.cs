using System;

namespace FirstLook.Valuation.DomainModel.History.Carfax.Commands.TransferObjects
{
    [Serializable]
    public class CarfaxReportInspectionDto
    {
        private int _id;
        private string _name;
        private bool _selected;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }
    }
}
