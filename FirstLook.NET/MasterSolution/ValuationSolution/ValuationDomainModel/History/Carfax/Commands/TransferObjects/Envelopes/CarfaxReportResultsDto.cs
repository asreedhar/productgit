﻿using System;

namespace FirstLook.Valuation.DomainModel.History.Carfax.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class CarfaxReportResultsDto
    {
        private CarfaxReportResultsStatusDto _status;
        private CarfaxReportDto _report;

        public CarfaxReportDto Report
        {
            get { return _report; }
            set { _report = value; }
        }

        public CarfaxReportResultsStatusDto Status
        {
            get { return _status; }
            set { _status = value; }
        }
    }
}
