﻿using System;
using System.Collections.Generic;

namespace FirstLook.Valuation.DomainModel.History.Carfax.Commands.TransferObjects
{
    public class CarfaxReportDto
    {
        private DateTime _expirationDate;
        private string _userName;
        private string _vin;
        private CarfaxReportTypeDto _reportType;
        private bool _eligibleForHotList;
        private bool _displayInHotList;
        private bool _hasProblem;
        private int _ownerCount;
        private readonly List<CarfaxReportInspectionDto> _inspections = new List<CarfaxReportInspectionDto>();

        public DateTime ExpirationDate
        {
            get { return _expirationDate; }
            set { _expirationDate = value; }
        }

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }

        public CarfaxReportTypeDto ReportType
        {
            get { return _reportType; }
            set { _reportType = value; }
        }

        public bool EligibleForHotList
        {
            get { return _eligibleForHotList; }
            set { _eligibleForHotList = value; }
        }

        public bool DisplayInHotList
        {
            get { return _displayInHotList; }
            set { _displayInHotList = value; }
        }

        public bool HasProblem
        {
            get { return _hasProblem; }
            set { _hasProblem = value; }
        }

        public int OwnerCount
        {
            get { return _ownerCount; }
            set { _ownerCount = value; }
        }

        public List<CarfaxReportInspectionDto> Inspections
        {
            get { return _inspections; }
        }
    }
}
