﻿using FirstLook.Common.Core.Command;
using FirstLook.Valuation.DomainModel.Common.Commands;
using FirstLook.Valuation.DomainModel.History.Carfax.Commands.TransferObjects.Envelopes;

namespace FirstLook.Valuation.DomainModel.History.Carfax.Commands
{
    public interface ICommandFactory : IPolicyProvider
    {
        ICommand<CarfaxReportQueryResultsDto, CarfaxReportQueryArgumentsDto>
            CreateCarfaxQueryCommand();

        ICommand<CarfaxReportResultsDto, CarfaxReportPurchaseArgumentsDto>
            CreateCarfaxPurchaseCommand();
    }
}
