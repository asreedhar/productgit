﻿using FirstLook.Common.Core.Command;
using FirstLook.Valuation.DomainModel.Common.Commands;
using FirstLook.Valuation.DomainModel.History.AutoCheck.Commands.TransferObjects.Envelopes;

namespace FirstLook.Valuation.DomainModel.History.AutoCheck.Commands
{
    public interface ICommandFactory : IPolicyProvider
    {
        ICommand<AutoCheckReportResultsDto, AutoCheckReportArgumentsDto>
            CreateAutoCheckQueryCommand();

        ICommand<AutoCheckReportResultsDto, AutoCheckReportArgumentsDto>
            CreateAutoCheckPurchaseCommand();
    }
}
