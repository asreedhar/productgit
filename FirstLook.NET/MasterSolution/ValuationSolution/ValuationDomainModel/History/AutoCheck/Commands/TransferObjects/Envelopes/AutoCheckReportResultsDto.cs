﻿using System;

namespace FirstLook.Valuation.DomainModel.History.AutoCheck.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class AutoCheckReportResultsDto
    {
        private AutoCheckReportArgumentsDto _arguments;
        private AutoCheckReportResultsStatusDto _status;
        private AutoCheckReportDto _report;

        public AutoCheckReportArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public AutoCheckReportResultsStatusDto Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public AutoCheckReportDto Report
        {
            get { return _report; }
            set { _report = value; }
        }
    }
}
