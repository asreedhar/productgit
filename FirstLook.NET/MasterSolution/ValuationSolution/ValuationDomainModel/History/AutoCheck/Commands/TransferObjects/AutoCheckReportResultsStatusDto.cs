using System;

namespace FirstLook.Valuation.DomainModel.History.AutoCheck.Commands.TransferObjects
{
    [Serializable]
    public enum AutoCheckReportResultsStatusDto
    {
        Success             = 0,
        ReportNotPurchased  = 1,
        ReportNotAvailable  = 2,
        AccountNotAvailable = 3,
        AccountNotOperable  = 4,
        NoPermission        = 5,
        ServiceUnavailable  = 6
    }
}
