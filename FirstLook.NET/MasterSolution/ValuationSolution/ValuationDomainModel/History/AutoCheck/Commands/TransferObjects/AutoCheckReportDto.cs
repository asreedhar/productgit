using System;
using System.Collections.Generic;

namespace FirstLook.Valuation.DomainModel.History.AutoCheck.Commands.TransferObjects
{
    [Serializable]
    public class AutoCheckReportDto
    {
        private DateTime _expirationDate;
        private string _userName;
        private string _vin;
        private int _score;
        private int _compareScoreRangeLow;
        private int _compareScoreRangeHigh;
        private readonly List<AutoCheckReportInspectionDto> _inspections = new List<AutoCheckReportInspectionDto>();

        public DateTime ExpirationDate
        {
            get { return _expirationDate; }
            set { _expirationDate = value; }
        }

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }

        public int Score
        {
            get { return _score; }
            set { _score = value; }
        }

        public int CompareScoreRangeLow
        {
            get { return _compareScoreRangeLow; }
            set { _compareScoreRangeLow = value; }
        }

        public int CompareScoreRangeHigh
        {
            get { return _compareScoreRangeHigh; }
            set { _compareScoreRangeHigh = value; }
        }

        public List<AutoCheckReportInspectionDto> Inspections
        {
            get { return _inspections; }
        }
    }
}
