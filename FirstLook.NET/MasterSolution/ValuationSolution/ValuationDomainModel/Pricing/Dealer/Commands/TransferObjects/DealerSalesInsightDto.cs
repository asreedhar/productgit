﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Dealer.Commands.TransferObjects
{
    [Serializable]
    public class DealerSalesInsightDto
    {
        private DealerSalesInsightTypeDto _insightType;
        private string _insightText;

        public DealerSalesInsightTypeDto InsightType
        {
            get { return _insightType; }
            set { _insightType = value; }
        }

        public string InsightText
        {
            get { return _insightText; }
            set { _insightText = value; }
        }
    }
}
