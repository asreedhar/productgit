﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Dealer.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class DealerSalesReportResultsDto
    {
        private DealerSalesReportArgumentsDto _arguments;
        private DealerSalesReportTableDto _colorReport;
        private DealerSalesReportTableDto _modelYearReport;
        private DealerSalesReportTableDto _priceReport;
        private DealerSalesReportTableDto _seriesReport;
        private DealerSalesReportTableDto _summaryReport;

        public DealerSalesReportArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public DealerSalesReportTableDto ColorReport
        {
            get { return _colorReport; }
            set { _colorReport = value; }
        }

        public DealerSalesReportTableDto ModelYearReport
        {
            get { return _modelYearReport; }
            set { _modelYearReport = value; }
        }

        public DealerSalesReportTableDto PriceReport
        {
            get { return _priceReport; }
            set { _priceReport = value; }
        }

        public DealerSalesReportTableDto SeriesReport
        {
            get { return _seriesReport; }
            set { _seriesReport = value; }
        }

        public DealerSalesReportTableDto SummaryReport
        {
            get { return _summaryReport; }
            set { _summaryReport = value; }
        }
    }
}
