﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Dealer.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class DealerSalesReportArgumentsDto
    {
        private string _brokerHandle;
        private string _vin;
        private string _series;
        private DimensionStockTypeDto _stockType;
        private DimensionTimeDto _time;
        private DimensionTimeDirectionDto _timeDirection;

        private RestrictionDto _restrictionOnMileage;
        private RestrictionDto _restrictionOnSeries;

        public string BrokerHandle
        {
            get { return _brokerHandle; }
            set { _brokerHandle = value; }
        }

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }

        public string Series
        {
            get { return _series; }
            set { _series = value; }
        }

        public DimensionStockTypeDto StockType
        {
            get { return _stockType; }
            set { _stockType = value; }
        }

        public DimensionTimeDto Time
        {
            get { return _time; }
            set { _time = value; }
        }

        public DimensionTimeDirectionDto TimeDirection
        {
            get { return _timeDirection; }
            set { _timeDirection = value; }
        }

        public RestrictionDto RestrictionOnMileage
        {
            get { return _restrictionOnMileage; }
            set { _restrictionOnMileage = value; }
        }

        public RestrictionDto RestrictionOnSeries
        {
            get { return _restrictionOnSeries; }
            set { _restrictionOnSeries = value; }
        }
    }
}
