using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Dealer.Commands.TransferObjects
{
    [Serializable]
    public enum DimensionTimeDirectionDto
    {
        Undefined,
        Historical,
        Projection
    }
}
