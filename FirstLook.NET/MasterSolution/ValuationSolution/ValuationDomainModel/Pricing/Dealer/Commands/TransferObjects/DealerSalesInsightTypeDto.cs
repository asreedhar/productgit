using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Dealer.Commands.TransferObjects
{
    [Serializable]
    public enum DealerSalesInsightTypeDto
    {
        Undefined,
        InsufficientOrNoSalesHistory,
        AverageGrossProfit,
        FrequentNoSales,
        AverageMargin,
        ContributionRank,
        AverageDaysToSale,
        TrafficLight,
        StockingLevels,
        HighMileage,
        OlderVehicle,
        AnnualReturnOnInvestment,
        MakeModelMarketSharePercentage,
        MakeModelMarketShareRank,
        ModelYearMarketSharePercentage,
        ModelYearMarketShareRank
    }
}
