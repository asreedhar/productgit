﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Dealer.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class DealerSalesInsightArgumentsDto
    {
        private string _brokerHandle;
        private string _vin;
        private string _series;
        private int _mileage;
        private DimensionStockTypeDto _stockType;
        private DimensionTimeDto _time;
        private RestrictionDto _modelYearRestriction;
        private RestrictionDto _seriesRestriction;

        public string BrokerHandle
        {
            get { return _brokerHandle; }
            set { _brokerHandle = value; }
        }

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }

        public string Series
        {
            get { return _series; }
            set { _series = value; }
        }

        public int Mileage
        {
            get { return _mileage; }
            set { _mileage = value; }
        }

        public DimensionStockTypeDto StockType
        {
            get { return _stockType; }
            set { _stockType = value; }
        }

        public DimensionTimeDto Time
        {
            get { return _time; }
            set { _time = value; }
        }

        public RestrictionDto ModelYearRestriction
        {
            get { return _modelYearRestriction; }
            set { _modelYearRestriction = value; }
        }

        public RestrictionDto SeriesRestriction
        {
            get { return _seriesRestriction; }
            set { _seriesRestriction = value; }
        }
    }
}
