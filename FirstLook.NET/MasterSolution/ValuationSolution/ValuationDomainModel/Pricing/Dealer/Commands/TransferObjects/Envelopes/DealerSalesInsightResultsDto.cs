﻿using System;
using System.Collections.Generic;

namespace FirstLook.Valuation.DomainModel.Pricing.Dealer.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class DealerSalesInsightResultsDto
    {
        private DealerSalesInsightArgumentsDto _arguments;
        private List<DealerSalesInsightDto> _insights;
        private FactTrafficLightDto _trafficLight;

        public DealerSalesInsightArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public List<DealerSalesInsightDto> Insights
        {
            get { return _insights; }
            set { _insights = value; }
        }

        public FactTrafficLightDto TrafficLight
        {
            get { return _trafficLight; }
            set { _trafficLight = value; }
        }
    }
}
