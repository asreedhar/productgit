using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Dealer.Commands.TransferObjects
{
    [Serializable]
    public enum FactDealTypeDto
    {
        Undefined,
        Purchase,
        Trade,
        Unknown
    }
}
