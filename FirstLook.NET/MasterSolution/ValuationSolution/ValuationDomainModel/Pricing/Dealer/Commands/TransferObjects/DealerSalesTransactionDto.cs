﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Dealer.Commands.TransferObjects
{
    [Serializable]
    public class DealerSalesTransactionDto
    {
        #region Deal Information

        private DateTime _dealDate;
        private string _dealNumber;
        private bool _dealIsNoSale;
        private int _daysToSale;
        private FactDealTypeDto _dealType;

        public DateTime DealDate
        {
            get { return _dealDate; }
            set { _dealDate = value; }
        }

        public string DealNumber
        {
            get { return _dealNumber; }
            set { _dealNumber = value; }
        }

        public bool DealIsNoSale
        {
            get { return _dealIsNoSale; }
            set { _dealIsNoSale = value; }
        }

        public int DaysToSale
        {
            get { return _daysToSale; }
            set { _daysToSale = value; }
        }

        public FactDealTypeDto DealType
        {
            get { return _dealType; }
            set { _dealType = value; }
        }

        #endregion

        #region Financial Information

        private decimal _unitCost;
        private decimal _salePrice;
        private decimal _retailGrossProfit;
        private decimal _financeInsuranceGrossProfit;

        public decimal UnitCost
        {
            get { return _unitCost; }
            set { _unitCost = value; }
        }

        public decimal SalePrice
        {
            get { return _salePrice; }
            set { _salePrice = value; }
        }

        public decimal RetailGrossProfit
        {
            get { return _retailGrossProfit; }
            set { _retailGrossProfit = value; }
        }

        public decimal FinanceInsuranceGrossProfit
        {
            get { return _financeInsuranceGrossProfit; }
            set { _financeInsuranceGrossProfit = value; }
        }

        #endregion
        
        #region Vehicle Information

        private string _stockNumber;
        private int _modelYear;
        private string _make;
        private string _model;
        private string _series;
        private string _bodyStyle;
        private int _mileage;
        private string _baseColor;

        public string StockNumber
        {
            get { return _stockNumber; }
            set { _stockNumber = value; }
        }

        public int ModelYear
        {
            get { return _modelYear; }
            set { _modelYear = value; }
        }

        public string Make
        {
            get { return _make; }
            set { _make = value; }
        }

        public string Model
        {
            get { return _model; }
            set { _model = value; }
        }

        public string Series
        {
            get { return _series; }
            set { _series = value; }
        }

        public string BodyStyle
        {
            get { return _bodyStyle; }
            set { _bodyStyle = value; }
        }

        public int Mileage
        {
            get { return _mileage; }
            set { _mileage = value; }
        }

        public string BaseColor
        {
            get { return _baseColor; }
            set { _baseColor = value; }
        }

        #endregion

        #region Sales Performance Information
        
        private FactTrafficLightDto _initialTrafficLight;

        public FactTrafficLightDto InitialTrafficLight
        {
            get { return _initialTrafficLight; }
            set { _initialTrafficLight = value; }
        }

        #endregion
    }
}
