﻿using System;
using System.Collections.Generic;

namespace FirstLook.Valuation.DomainModel.Pricing.Dealer.Commands.TransferObjects
{
    [Serializable]
    public class DealerSalesReportTableDto
    {
        private bool _isMarketShareAnalysisPresent;

        private List<DealerSalesReportTableRowDto> _rows;

        public bool IsMarketShareAnalysisPresent
        {
            get { return _isMarketShareAnalysisPresent; }
            set { _isMarketShareAnalysisPresent = value; }
        }

        public List<DealerSalesReportTableRowDto> Rows
        {
            get { return _rows; }
            set { _rows = value; }
        }
    }
}
