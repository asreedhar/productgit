﻿using System;
using System.Collections.Generic;

namespace FirstLook.Valuation.DomainModel.Pricing.Dealer.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class DealerSalesTransactionsResultsDto
    {
        private DealerSalesTransactionsArgumentsDto _arguments;
        private List<DealerSalesTransactionDto> _transactions;

        public DealerSalesTransactionsArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public List<DealerSalesTransactionDto> Transactions
        {
            get { return _transactions; }
            set { _transactions = value; }
        }
    }
}
