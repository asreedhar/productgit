﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Dealer.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class DealerSalesTransactionsArgumentsDto
    {
        private string _brokerHandle;
        private DimensionTimeDto _time;
        private DimensionStockTypeDto _stockType;
        private DimensionSaleTypeDto _saleType;

        private RestrictionDto _restrictionOnEquivalenceClass;
        private RestrictionDto _restrictionOnMileage;
        private RestrictionDto _restrictionOnSeries;

        private string _vin;
        private string _series;

        public string BrokerHandle
        {
            get { return _brokerHandle; }
            set { _brokerHandle = value; }
        }

        public DimensionTimeDto Time
        {
            get { return _time; }
            set { _time = value; }
        }

        public DimensionStockTypeDto StockType
        {
            get { return _stockType; }
            set { _stockType = value; }
        }

        public DimensionSaleTypeDto SaleType
        {
            get { return _saleType; }
            set { _saleType = value; }
        }

        public RestrictionDto RestrictionOnEquivalenceClass
        {
            get { return _restrictionOnEquivalenceClass; }
            set { _restrictionOnEquivalenceClass = value; }
        }

        public RestrictionDto RestrictionOnMileage
        {
            get { return _restrictionOnMileage; }
            set { _restrictionOnMileage = value; }
        }

        public RestrictionDto RestrictionOnSeries
        {
            get { return _restrictionOnSeries; }
            set { _restrictionOnSeries = value; }
        }

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }

        public string Series
        {
            get { return _series; }
            set { _series = value; }
        }
    }
}
