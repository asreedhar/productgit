﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Dealer.Commands.TransferObjects
{
    [Serializable]
    public enum DimensionStockTypeDto
    {
        Undefined,
        New,
        Used
    }
}
