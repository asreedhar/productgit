﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Dealer.Commands.TransferObjects
{
    [Serializable]
    public enum RestrictionDto
    {
        Undefined,
        Disabled,
        Enabled
    }
}
