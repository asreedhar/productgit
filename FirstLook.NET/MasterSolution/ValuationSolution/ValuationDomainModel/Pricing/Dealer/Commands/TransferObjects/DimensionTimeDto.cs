﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Dealer.Commands.TransferObjects
{
    [Serializable]
    public enum DimensionTimeDto
    {
        Undefined       = 0,
        FourWeeks       = 1,
        EightWeeks      = 2,
        ThirteenWeeks   = 3,
        TwentySixWeeks  = 4,
        FiftyTwoWeeks   = 5,
        CurrentMonth    = 6,
        PriorMonth      = 7,
        CurrentWeek     = 8,
        PriorWeek       = 9
    }
}
