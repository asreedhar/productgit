﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Dealer.Commands.TransferObjects
{
    [Serializable]
    public class DealerSalesReportTableRowDto
    {
        private String _groupingColumn;
        private int _lowerBound;
        private int _upperBound;
        private int _unitsSold;
        private int _averageGrossProfit;
        private int _totalGrossMargin;
        private int _averageDays;
        private int _averageMileage;
        private int _totalDays;
        private int _unitsInStock;
        private int _noSales;
        private int _totalInventoryDollars;
        private int _totalRevenue;
        private int _averageBackEnd;
        private int _marketShareDeals;
        private double _annualReturnOnInvestment;
        private double _totalFrontEndGross;
        private double _totalBackEndGross;
        private double _percentageTotalGrossMargin;
        private double _percentageTotalRevenue;
        private double _percentageTotalInventoryDollars;
        private double _percentageMarketShare;

        public string GroupingColumn
        {
            get { return _groupingColumn; }
            set { _groupingColumn = value; }
        }

        public int LowerBound
        {
            get { return _lowerBound; }
            set { _lowerBound = value; }
        }

        public int UpperBound
        {
            get { return _upperBound; }
            set { _upperBound = value; }
        }

        public int UnitsSold
        {
            get { return _unitsSold; }
            set { _unitsSold = value; }
        }

        public int AverageGrossProfit
        {
            get { return _averageGrossProfit; }
            set { _averageGrossProfit = value; }
        }

        public int TotalGrossMargin
        {
            get { return _totalGrossMargin; }
            set { _totalGrossMargin = value; }
        }

        public int AverageDays
        {
            get { return _averageDays; }
            set { _averageDays = value; }
        }

        public int AverageMileage
        {
            get { return _averageMileage; }
            set { _averageMileage = value; }
        }

        public int TotalDays
        {
            get { return _totalDays; }
            set { _totalDays = value; }
        }

        public int UnitsInStock
        {
            get { return _unitsInStock; }
            set { _unitsInStock = value; }
        }

        public int NoSales
        {
            get { return _noSales; }
            set { _noSales = value; }
        }

        public int TotalInventoryDollars
        {
            get { return _totalInventoryDollars; }
            set { _totalInventoryDollars = value; }
        }

        public int TotalRevenue
        {
            get { return _totalRevenue; }
            set { _totalRevenue = value; }
        }

        public int AverageBackEnd
        {
            get { return _averageBackEnd; }
            set { _averageBackEnd = value; }
        }

        public int MarketShareDeals
        {
            get { return _marketShareDeals; }
            set { _marketShareDeals = value; }
        }

        public double AnnualReturnOnInvestment
        {
            get { return _annualReturnOnInvestment; }
            set { _annualReturnOnInvestment = value; }
        }

        public double TotalFrontEndGross
        {
            get { return _totalFrontEndGross; }
            set { _totalFrontEndGross = value; }
        }

        public double TotalBackEndGross
        {
            get { return _totalBackEndGross; }
            set { _totalBackEndGross = value; }
        }

        public double PercentageTotalGrossMargin
        {
            get { return _percentageTotalGrossMargin; }
            set { _percentageTotalGrossMargin = value; }
        }

        public double PercentageTotalRevenue
        {
            get { return _percentageTotalRevenue; }
            set { _percentageTotalRevenue = value; }
        }

        public double PercentageTotalInventoryDollars
        {
            get { return _percentageTotalInventoryDollars; }
            set { _percentageTotalInventoryDollars = value; }
        }

        public double PercentageMarketShare
        {
            get { return _percentageMarketShare; }
            set { _percentageMarketShare = value; }
        }
    }
}
