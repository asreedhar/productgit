using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Dealer.Commands.TransferObjects
{
    [Serializable]
    public enum DimensionSaleTypeDto
    {
        Undefined,
        Retail,
        Wholesale
    }
}
