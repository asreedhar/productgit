﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Dealer.Commands.TransferObjects
{
    [Serializable]
    public enum FactTrafficLightDto
    {
        Undefined,
        Red,
        Yellow,
        Green
    }
}
