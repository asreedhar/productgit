﻿using FirstLook.Common.Core.Command;
using FirstLook.Valuation.DomainModel.Common.Commands;
using FirstLook.Valuation.DomainModel.Pricing.Dealer.Commands.TransferObjects.Envelopes;

namespace FirstLook.Valuation.DomainModel.Pricing.Dealer.Commands
{
    public interface ICommandFactory : IPolicyProvider
    {
        ICommand<DealerSalesInsightResultsDto, DealerSalesInsightArgumentsDto>
            CreateDealerSalesInsightsCommand();

        ICommand<DealerSalesReportResultsDto, DealerSalesReportArgumentsDto>
            CreateDealerSalesReportCommand();

        ICommand<DealerSalesTransactionsResultsDto, DealerSalesTransactionsArgumentsDto>
            CreateDealerSalesTransactionsCommand();
    }
}
