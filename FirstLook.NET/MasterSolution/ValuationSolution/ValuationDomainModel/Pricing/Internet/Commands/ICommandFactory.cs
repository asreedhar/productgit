﻿using FirstLook.Common.Core.Command;
using FirstLook.Valuation.DomainModel.Common.Commands;
using FirstLook.Valuation.DomainModel.Pricing.Internet.Commands.TransferObjects.Envelopes;

namespace FirstLook.Valuation.DomainModel.Pricing.Internet.Commands
{
    public interface ICommandFactory : IPolicyProvider
    {
        ICommand<InternetReportResultsDto, InternetReportArgumentsDto>
            CreateInternetReportCommand();

        ICommand<InternetSearchSpaceResultsDto, InternetSearchSpaceArgumentsDto>
            CreateInternetSearchSpaceCommand();

        ICommand<InternetFilterUpdateResultsDto, InternetFilterUpdateArgumentsDto>
            CreateInternetFilterUpdateCommand();

        ICommand<InternetSearchSaveResultsDto, InternetSearchSaveArgumentsDto>
            CreateInternetSearchSaveCommand();

        ICommand<InternetListingsResultsDto, InternetListingsArgumentsDto>
            CreateInternetListingsCommand();
    }
}
