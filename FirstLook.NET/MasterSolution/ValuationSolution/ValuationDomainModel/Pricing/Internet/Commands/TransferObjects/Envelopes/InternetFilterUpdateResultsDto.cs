﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Internet.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class InternetFilterUpdateResultsDto
    {
        private InternetFilterUpdateArgumentsDto _arguments;
        private InternetFilterDto _filter;

        public InternetFilterUpdateArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public InternetFilterDto Filter
        {
            get { return _filter; }
            set { _filter = value; }
        }
    }
}
