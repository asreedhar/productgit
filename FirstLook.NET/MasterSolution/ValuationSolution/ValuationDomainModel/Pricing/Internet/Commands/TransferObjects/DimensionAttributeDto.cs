using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Internet.Commands.TransferObjects
{
    [Serializable]
    public abstract class DimensionAttributeDto
    {
        private int _id;
        private string _name;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
    }

    [Serializable]
    public class DimensionModelFamilyDto : DimensionAttributeDto
    {

    }

    [Serializable]
    public class DimensionSegmentDto : DimensionAttributeDto
    {

    }

    [Serializable]
    public class DimensionBodyTypeDto : DimensionAttributeDto
    {

    }

    [Serializable]
    public class DimensionPassengerDoorDto : DimensionAttributeDto
    {

    }

    [Serializable]
    public class DimensionFuelTypeDto : DimensionAttributeDto
    {

    }

    [Serializable]
    public class DimensionDriveTrainDto : DimensionAttributeDto
    {

    }

    [Serializable]
    public class DimensionTransmissionDto : DimensionAttributeDto
    {

    }

    [Serializable]
    public class DimensionEngineDto : DimensionAttributeDto
    {

    }

    [Serializable]
    public class DimensionSeriesDto : DimensionAttributeDto
    {

    }
}
