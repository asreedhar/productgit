﻿using System;
using System.Collections.Generic;
using FirstLook.Valuation.DomainModel.Common.Commands.TransferObjects;

namespace FirstLook.Valuation.DomainModel.Pricing.Internet.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class InternetListingsResultsDto : PaginatedResultsDto
    {
        private InternetListingsArgumentsDto _arguments;
        private List<InternetListingDto> _listings;

        public InternetListingsArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public List<InternetListingDto> Listings
        {
            get { return _listings; }
            set { _listings = value; }
        }
    }
}
