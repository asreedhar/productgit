﻿using System;
using System.Collections.Generic;

namespace FirstLook.Valuation.DomainModel.Pricing.Internet.Commands.TransferObjects
{
    [Serializable]
    public class InternetFilterSpaceDto
    {
        private List<DimensionModelFamilyDto> _modelFamilies = new List<DimensionModelFamilyDto>();
        private List<DimensionSegmentDto> _segments = new List<DimensionSegmentDto>();
        private List<DimensionBodyTypeDto> _bodyTypes = new List<DimensionBodyTypeDto>();
        private List<DimensionPassengerDoorDto> _passengerDoors = new List<DimensionPassengerDoorDto>();
        private List<DimensionFuelTypeDto> _fuelTypes = new List<DimensionFuelTypeDto>();
        private List<DimensionDriveTrainDto> _driveTrains = new List<DimensionDriveTrainDto>();
        private List<DimensionTransmissionDto> _transmissions = new List<DimensionTransmissionDto>();
        private List<DimensionEngineDto> _engines = new List<DimensionEngineDto>();
        private List<DimensionSeriesDto> _series = new List<DimensionSeriesDto>();

        public List<DimensionModelFamilyDto> ModelFamilies
        {
            get { return _modelFamilies; }
            set { _modelFamilies = value; }
        }

        public List<DimensionSegmentDto> Segments
        {
            get { return _segments; }
            set { _segments = value; }
        }

        public List<DimensionBodyTypeDto> BodyTypes
        {
            get { return _bodyTypes; }
            set { _bodyTypes = value; }
        }

        public List<DimensionPassengerDoorDto> PassengerDoors
        {
            get { return _passengerDoors; }
            set { _passengerDoors = value; }
        }

        public List<DimensionFuelTypeDto> FuelTypes
        {
            get { return _fuelTypes; }
            set { _fuelTypes = value; }
        }

        public List<DimensionDriveTrainDto> DriveTrains
        {
            get { return _driveTrains; }
            set { _driveTrains = value; }
        }

        public List<DimensionTransmissionDto> Transmissions
        {
            get { return _transmissions; }
            set { _transmissions = value; }
        }

        public List<DimensionEngineDto> Engines
        {
            get { return _engines; }
            set { _engines = value; }
        }

        public List<DimensionSeriesDto> Series
        {
            get { return _series; }
            set { _series = value; }
        }
    }
}
