﻿using System;
using System.Collections.Generic;

namespace FirstLook.Valuation.DomainModel.Pricing.Internet.Commands.TransferObjects
{
    [Serializable]
    public class InternetSearchSpaceDto
    {
        private InternetFilterSpaceDto _filterSpace;
        private List<DimensionRadiusDto> _radiusValues = new List<DimensionRadiusDto>();
        private List<DimensionOdomoterDto> _odometerValues = new List<DimensionOdomoterDto>();

        public InternetFilterSpaceDto FilterSpace
        {
            get { return _filterSpace; }
            set { _filterSpace = value; }
        }

        public List<DimensionRadiusDto> RadiusValues
        {
            get { return _radiusValues; }
            set { _radiusValues = value; }
        }

        public List<DimensionOdomoterDto> OdometerValues
        {
            get { return _odometerValues; }
            set { _odometerValues = value; }
        }
    }
}
