﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Internet.Commands.TransferObjects
{
    [Serializable]
    public class DimensionLocationDto
    {
        private string _zipCode;
        private decimal _latitude;
        private decimal _longitude;

        public string ZipCode
        {
            get { return _zipCode; }
            set { _zipCode = value; }
        }

        public decimal Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }

        public decimal Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }
    }
}
