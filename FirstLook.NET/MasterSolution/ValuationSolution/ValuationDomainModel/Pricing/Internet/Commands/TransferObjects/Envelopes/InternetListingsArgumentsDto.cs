﻿using System;
using FirstLook.Valuation.DomainModel.Common.Commands.TransferObjects;

namespace FirstLook.Valuation.DomainModel.Pricing.Internet.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class InternetListingsArgumentsDto : PaginatedArgumentsDto
    {
        private InternetSearchDto _search;

        public InternetSearchDto Search
        {
            get { return _search; }
            set { _search = value; }
        }
    }
}
