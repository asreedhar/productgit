﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Internet.Commands.TransferObjects
{
    [Serializable]
    public class InternetSearchDto
    {
        private string _handle;
        private DimensionTerritoryDto _territory;
        private RangeOdometerDto _odometer;
        private InternetFilterDto _filter;

        public string Handle
        {
            get { return _handle; }
            set { _handle = value; }
        }

        public DimensionTerritoryDto Territory
        {
            get { return _territory; }
            set { _territory = value; }
        }

        public RangeOdometerDto Odometer
        {
            get { return _odometer; }
            set { _odometer = value; }
        }

        public InternetFilterDto Filter
        {
            get { return _filter; }
            set { _filter = value; }
        }
    }
}
