﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Internet.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class InternetSearchSaveArgumentsDto
    {
        private InternetSearchDto _search;

        public InternetSearchDto Search
        {
            get { return _search; }
            set { _search = value; }
        }
    }
}
