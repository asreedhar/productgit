﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Internet.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class InternetSearchSpaceResultsDto
    {
        private InternetSearchSpaceArgumentsDto _arguments;
        private InternetSearchSpaceDto _searchSpace;

        public InternetSearchSpaceArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public InternetSearchSpaceDto SearchSpace
        {
            get { return _searchSpace; }
            set { _searchSpace = value; }
        }
    }
}
