﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Internet.Commands.TransferObjects
{
    [Serializable]
    public class FactAggregateDto
    {
        private int _minimum;
        private int _average;
        private int _maximum;
        private int _sampleSize;

        public int Minimum
        {
            get { return _minimum; }
            set { _minimum = value; }
        }

        public int Average
        {
            get { return _average; }
            set { _average = value; }
        }

        public int Maximum
        {
            get { return _maximum; }
            set { _maximum = value; }
        }

        public int SampleSize
        {
            get { return _sampleSize; }
            set { _sampleSize = value; }
        }
    }
}
