﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Internet.Commands.TransferObjects
{
    [Serializable]
    public class InternetListingDto
    {
        #region Summary

        private int _id;
        private bool _isAnalyzedVehicle;
        private string _sellerName;
        private int _age;
        private string _vehicleDescription;
        private string _vehicleColor;
        private int _vehicleMileage;
        private int _listPrice;
        private int _percentageOfMarketAveragePrice;
        private int _distanceFromDealer;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public bool IsAnalyzedVehicle
        {
            get { return _isAnalyzedVehicle; }
            set { _isAnalyzedVehicle = value; }
        }

        public string SellerName
        {
            get { return _sellerName; }
            set { _sellerName = value; }
        }

        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }

        public string VehicleDescription
        {
            get { return _vehicleDescription; }
            set { _vehicleDescription = value; }
        }

        public string VehicleColor
        {
            get { return _vehicleColor; }
            set { _vehicleColor = value; }
        }

        public int VehicleMileage
        {
            get { return _vehicleMileage; }
            set { _vehicleMileage = value; }
        }

        public int ListPrice
        {
            get { return _listPrice; }
            set { _listPrice = value; }
        }

        public int PercentageOfMarketAveragePrice
        {
            get { return _percentageOfMarketAveragePrice; }
            set { _percentageOfMarketAveragePrice = value; }
        }

        public int DistanceFromDealer
        {
            get { return _distanceFromDealer; }
            set { _distanceFromDealer = value; }
        }

        #endregion

        #region Detail

        private string _listingVehicleDescription;
        private string _listingVehicleColor;
        private string _listingStockNumber;
        private string _listingVin;
        private string _listingSellerDescription;
        private string _listingOptions;
        private bool _listingCertified;

        public string ListingVehicleDescription
        {
            get { return _listingVehicleDescription; }
            set { _listingVehicleDescription = value; }
        }

        public string ListingVehicleColor
        {
            get { return _listingVehicleColor; }
            set { _listingVehicleColor = value; }
        }

        public string ListingStockNumber
        {
            get { return _listingStockNumber; }
            set { _listingStockNumber = value; }
        }

        public string ListingVin
        {
            get { return _listingVin; }
            set { _listingVin = value; }
        }

        public string ListingSellerDescription
        {
            get { return _listingSellerDescription; }
            set { _listingSellerDescription = value; }
        }

        public string ListingOptions
        {
            get { return _listingOptions; }
            set { _listingOptions = value; }
        }

        public bool ListingCertified
        {
            get { return _listingCertified; }
            set { _listingCertified = value; }
        }

        #endregion
    }
}
