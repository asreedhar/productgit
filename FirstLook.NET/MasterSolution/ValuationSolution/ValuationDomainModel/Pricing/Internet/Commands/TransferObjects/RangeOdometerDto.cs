﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Internet.Commands.TransferObjects
{
    [Serializable]
    public class RangeOdometerDto
    {
        private DimensionOdomoterDto _lower;
        private DimensionOdomoterDto _upper;

        public DimensionOdomoterDto Lower
        {
            get { return _lower; }
            set { _lower = value; }
        }

        public DimensionOdomoterDto Upper
        {
            get { return _upper; }
            set { _upper = value; }
        }
    }
}
