﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Internet.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class InternetReportResultsDto
    {
        private InternetReportArgumentsDto _arguments;
        private FactAggregateDto _mileageSummary;
        private FactAggregateDto _priceSummary;
        private InternetSearchDto _search;

        public InternetReportArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public FactAggregateDto MileageSummary
        {
            get { return _mileageSummary; }
            set { _mileageSummary = value; }
        }

        public FactAggregateDto PriceSummary
        {
            get { return _priceSummary; }
            set { _priceSummary = value; }
        }

        public InternetSearchDto Search
        {
            get { return _search; }
            set { _search = value; }
        }
    }
}
