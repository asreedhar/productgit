﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Internet.Commands.TransferObjects
{
    [Serializable]
    public class DimensionRadiusDto
    {
        private int _miles;

        public int Miles
        {
            get { return _miles; }
            set { _miles = value; }
        }
    }
}
