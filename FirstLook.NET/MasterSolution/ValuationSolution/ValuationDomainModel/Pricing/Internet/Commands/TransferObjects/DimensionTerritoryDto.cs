﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Internet.Commands.TransferObjects
{
    [Serializable]
    public class DimensionTerritoryDto
    {
        private DimensionLocationDto _location;
        private DimensionRadiusDto _radius;

        public DimensionLocationDto Location
        {
            get { return _location; }
            set { _location = value; }
        }

        public DimensionRadiusDto Radius
        {
            get { return _radius; }
            set { _radius = value; }
        }
    }
}
