﻿using System;
using System.Collections.Generic;

namespace FirstLook.Valuation.DomainModel.Pricing.Internet.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class InternetFilterUpdateArgumentsDto
    {
        private InternetFilterDto _filter;
        private List<DimensionAttributeDto> _selections;

        public InternetFilterDto Filter
        {
            get { return _filter; }
            set { _filter = value; }
        }

        public List<DimensionAttributeDto> Selections
        {
            get { return _selections; }
            set { _selections = value; }
        }
    }
}
