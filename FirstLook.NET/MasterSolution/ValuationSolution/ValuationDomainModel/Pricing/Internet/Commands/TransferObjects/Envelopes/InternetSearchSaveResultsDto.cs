﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.Internet.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class InternetSearchSaveResultsDto
    {
        private InternetSearchSaveArgumentsDto _arguments;
        private FactAggregateDto _mileageSummary;
        private FactAggregateDto _priceSummary;

        public InternetSearchSaveArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public FactAggregateDto MileageSummary
        {
            get { return _mileageSummary; }
            set { _mileageSummary = value; }
        }

        public FactAggregateDto PriceSummary
        {
            get { return _priceSummary; }
            set { _priceSummary = value; }
        }
    }
}
