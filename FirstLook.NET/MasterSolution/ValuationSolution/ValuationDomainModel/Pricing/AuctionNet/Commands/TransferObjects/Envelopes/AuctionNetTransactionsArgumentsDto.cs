﻿using System;
using FirstLook.Valuation.DomainModel.Common.Commands.TransferObjects;

namespace FirstLook.Valuation.DomainModel.Pricing.AuctionNet.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class AuctionNetTransactionsArgumentsDto : PaginatedArgumentsDto
    {
        private AuctionNetReportArgumentsDto _reportArguments;

        public AuctionNetReportArgumentsDto ReportArguments
        {
            get { return _reportArguments; }
            set { _reportArguments = value; }
        }
    }
}
