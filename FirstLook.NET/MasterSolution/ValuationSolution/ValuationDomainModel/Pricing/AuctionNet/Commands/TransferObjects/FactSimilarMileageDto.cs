﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.AuctionNet.Commands.TransferObjects
{
    [Serializable]
    public class FactSimilarMileageDto
    {
        private decimal _averageSalePrice;
        private int _vehicleCount;

        public decimal AverageSalePrice
        {
            get { return _averageSalePrice; }
            set { _averageSalePrice = value; }
        }

        public int VehicleCount
        {
            get { return _vehicleCount; }
            set { _vehicleCount = value; }
        }
    }
}
