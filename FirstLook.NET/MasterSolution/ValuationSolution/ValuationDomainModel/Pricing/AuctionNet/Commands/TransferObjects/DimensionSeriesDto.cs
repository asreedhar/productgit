﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.AuctionNet.Commands.TransferObjects
{
    [Serializable]
    public class DimensionSeriesDto
    {
        private string _vic;
        private int _modelYear;
        private string _make;
        private string _series;
        private string _body;

        public string Vic
        {
            get { return _vic; }
            set { _vic = value; }
        }

        public int ModelYear
        {
            get { return _modelYear; }
            set { _modelYear = value; }
        }

        public string Make
        {
            get { return _make; }
            set { _make = value; }
        }

        public string Series
        {
            get { return _series; }
            set { _series = value; }
        }

        public string Body
        {
            get { return _body; }
            set { _body = value; }
        }
    }
}
