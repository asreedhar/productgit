﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.AuctionNet.Commands.TransferObjects
{
    [Serializable]
    public class FactVehicleDescription
    {
        private int _id;
        private string _make;
        private string _model;
        private int _modelYear;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Make
        {
            get { return _make; }
            set { _make = value; }
        }

        public string Model
        {
            get { return _model; }
            set { _model = value; }
        }

        public int ModelYear
        {
            get { return _modelYear; }
            set { _modelYear = value; }
        }
    }
}
