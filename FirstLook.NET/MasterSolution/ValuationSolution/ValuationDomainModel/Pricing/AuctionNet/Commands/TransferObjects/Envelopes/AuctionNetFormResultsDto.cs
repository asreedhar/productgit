﻿using System;
using System.Collections.Generic;

namespace FirstLook.Valuation.DomainModel.Pricing.AuctionNet.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class AuctionNetFormResultsDto
    {
        private AuctionNetFormArgumentsDto _arguments;
        private List<DimensionAreaDto> _areaDimension;
        private List<DimensionAreaDto> _timeDimension;
        private List<DimensionAreaDto> _seriesDimension;

        public AuctionNetFormArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public List<DimensionAreaDto> AreaDimension
        {
            get { return _areaDimension; }
            set { _areaDimension = value; }
        }

        public List<DimensionAreaDto> TimeDimension
        {
            get { return _timeDimension; }
            set { _timeDimension = value; }
        }

        public List<DimensionAreaDto> SeriesDimension
        {
            get { return _seriesDimension; }
            set { _seriesDimension = value; }
        }
    }
}
