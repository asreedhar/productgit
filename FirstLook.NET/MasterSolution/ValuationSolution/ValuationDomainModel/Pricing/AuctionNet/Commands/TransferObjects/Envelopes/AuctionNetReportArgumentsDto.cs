﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.AuctionNet.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class AuctionNetReportArgumentsDto
    {
        private int _periodId;
        private int _areaId;
        private int _seriesId;
        private string _vin;
        private int _mileage;

        public int PeriodId
        {
            get { return _periodId; }
            set { _periodId = value; }
        }

        public int AreaId
        {
            get { return _areaId; }
            set { _areaId = value; }
        }

        public int SeriesId
        {
            get { return _seriesId; }
            set { _seriesId = value; }
        }

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }

        public int Mileage
        {
            get { return _mileage; }
            set { _mileage = value; }
        }
    }
}
