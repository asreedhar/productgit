﻿using System;
using System.Collections.Generic;
using FirstLook.Valuation.DomainModel.Common.Commands.TransferObjects;

namespace FirstLook.Valuation.DomainModel.Pricing.AuctionNet.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class AuctionNetTransactionsResultsDto : PaginatedResultsDto
    {
        private AuctionNetTransactionsArgumentsDto _arguments;
        private List<AuctionNetTransactionDto> _transactions;

        public AuctionNetTransactionsArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public List<AuctionNetTransactionDto> Transactions
        {
            get { return _transactions; }
            set { _transactions = value; }
        }
    }
}
