﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.AuctionNet.Commands.TransferObjects
{
    [Serializable]
    public class AuctionNetTransactionDto
    {
        private int _rowNumber;
        private DateTime _saleDate;
        private string _regionName;
        private string _saleTypeName;
        private string _series;
        private decimal _salePrice;
        private int _mileage;
        private string _engine;
        private string _transmission;

        public int RowNumber
        {
            get { return _rowNumber; }
            set { _rowNumber = value; }
        }

        public DateTime SaleDate
        {
            get { return _saleDate; }
            set { _saleDate = value; }
        }

        public string RegionName
        {
            get { return _regionName; }
            set { _regionName = value; }
        }

        public string SaleTypeName
        {
            get { return _saleTypeName; }
            set { _saleTypeName = value; }
        }

        public string Series
        {
            get { return _series; }
            set { _series = value; }
        }

        public decimal SalePrice
        {
            get { return _salePrice; }
            set { _salePrice = value; }
        }

        public int Mileage
        {
            get { return _mileage; }
            set { _mileage = value; }
        }

        public string Engine
        {
            get { return _engine; }
            set { _engine = value; }
        }

        public string Transmission
        {
            get { return _transmission; }
            set { _transmission = value; }
        }
    }
}
