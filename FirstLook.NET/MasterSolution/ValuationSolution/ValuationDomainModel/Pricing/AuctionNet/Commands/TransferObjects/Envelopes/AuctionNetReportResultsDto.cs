﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.AuctionNet.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class AuctionNetReportResultsDto
    {
        private AuctionNetReportArgumentsDto _arguments;
        private FactVehicleDescription _vehicleDescription;
        private FactMileageDto _mileage;
        private FactSalePriceDto _salePrice;
        private FactSimilarMileageDto _similarMileage;

        public AuctionNetReportArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public FactVehicleDescription VehicleDescription
        {
            get { return _vehicleDescription; }
            set { _vehicleDescription = value; }
        }

        public FactMileageDto Mileage
        {
            get { return _mileage; }
            set { _mileage = value; }
        }

        public FactSalePriceDto SalePrice
        {
            get { return _salePrice; }
            set { _salePrice = value; }
        }

        public FactSimilarMileageDto SimilarMileage
        {
            get { return _similarMileage; }
            set { _similarMileage = value; }
        }
    }
}
