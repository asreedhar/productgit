﻿using System;

namespace FirstLook.Valuation.DomainModel.Pricing.AuctionNet.Commands.TransferObjects
{
    [Serializable]
    public class FactSalePriceDto
    {
        private decimal _minimum;
        private decimal _average;
        private decimal _maximum;
        private int _sampleSize;

        public decimal Minimum
        {
            get { return _minimum; }
            set { _minimum = value; }
        }

        public decimal Average
        {
            get { return _average; }
            set { _average = value; }
        }

        public decimal Maximum
        {
            get { return _maximum; }
            set { _maximum = value; }
        }

        public int SampleSize
        {
            get { return _sampleSize; }
            set { _sampleSize = value; }
        }
    }
}
