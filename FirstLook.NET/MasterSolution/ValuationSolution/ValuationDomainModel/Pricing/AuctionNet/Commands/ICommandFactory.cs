﻿using FirstLook.Common.Core.Command;
using FirstLook.Valuation.DomainModel.Common.Commands;
using FirstLook.Valuation.DomainModel.Pricing.AuctionNet.Commands.TransferObjects.Envelopes;

namespace FirstLook.Valuation.DomainModel.Pricing.AuctionNet.Commands
{
    public interface ICommandFactory : IPolicyProvider
    {
        ICommand<AuctionNetFormResultsDto, AuctionNetFormArgumentsDto>
            CreateFormCommand();

        ICommand<AuctionNetReportResultsDto, AuctionNetReportArgumentsDto>
            CreateReportCommand();

        ICommand<AuctionNetTransactionsResultsDto, AuctionNetTransactionsArgumentsDto>
            CreateTransactionsCommand();
    }
}
