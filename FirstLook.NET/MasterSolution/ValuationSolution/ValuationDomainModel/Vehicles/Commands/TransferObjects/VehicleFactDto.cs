﻿using System;

namespace FirstLook.Valuation.DomainModel.Vehicles.Commands.TransferObjects
{
    [Serializable]
    public class VehicleFactDto
    {
        private VehicleFactTypeDto _factType;
        private string _value;

        public VehicleFactTypeDto FactType
        {
            get { return _factType; }
            set { _factType = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}
