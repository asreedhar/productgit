﻿using System;
using System.Collections.Generic;

namespace FirstLook.Valuation.DomainModel.Vehicles.Commands.TransferObjects
{
    [Serializable]
    public class VehicleDto
    {
        private string _vin = string.Empty;
        private string _odometer;

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }

        public string Odometer
        {
            get { return _odometer; }
            set { _odometer = value; }
        }

        private VehicleTaxonomyDto _taxonomy;

        public VehicleTaxonomyDto Taxonomy
        {
            get { return _taxonomy; }
            set { _taxonomy = value; }
        }

        private List<VehicleFactDto> _facts = new List<VehicleFactDto>();

        public List<VehicleFactDto> Facts
        {
            get { return _facts; }
            set { _facts = value; }
        }

        private string _handle = string.Empty; // for API where VIN is not enough ...

        public string Handle
        {
            get { return _handle; }
            set { _handle = value; }
        }       
    }
}