﻿using System;

namespace FirstLook.Valuation.DomainModel.Vehicles.Commands.TransferObjects
{
    [Serializable]
    public class VehicleTaxonomyDto
    {
        #region Model

        private string _makeName = string.Empty;
        private string _modelFamilyName = string.Empty;
        private string _seriesName = string.Empty;
        private string _segmentName = string.Empty;
        private string _bodyTypeName = string.Empty;

        public string MakeName
        {
            get { return _makeName; }
            set { _makeName = value; }
        }

        public string ModelFamilyName
        {
            get { return _modelFamilyName; }
            set { _modelFamilyName = value; }
        }

        public string SeriesName
        {
            get { return _seriesName; }
            set { _seriesName = value; }
        }

        public string SegmentName
        {
            get { return _segmentName; }
            set { _segmentName = value; }
        }

        public string BodyTypeName
        {
            get { return _bodyTypeName; }
            set { _bodyTypeName = value; }
        }

        #endregion

        #region Configuration

        private int _modelYear;
        private string _passengerDoorName = string.Empty;
        private string _fuelTypeName = string.Empty;
        private string _engineName = string.Empty;
        private string _driveTrainName = string.Empty;
        private string _transmissionName = string.Empty;

        public int ModelYear
        {
            get { return _modelYear; }
            set { _modelYear = value; }
        }

        public string PassengerDoorName
        {
            get { return _passengerDoorName; }
            set { _passengerDoorName = value; }
        }

        public string FuelTypeName
        {
            get { return _fuelTypeName; }
            set { _fuelTypeName = value; }
        }

        public string EngineName
        {
            get { return _engineName; }
            set { _engineName = value; }
        }

        public string DriveTrainName
        {
            get { return _driveTrainName; }
            set { _driveTrainName = value; }
        }

        public string TransmissionName
        {
            get { return _transmissionName; }
            set { _transmissionName = value; }
        }

        #endregion
    }
}
