﻿using System;

namespace FirstLook.Valuation.DomainModel.Vehicles.Commands.TransferObjects
{
    [Serializable]
    public enum VehicleFactTypeDto
    {
        Undefined,
        StockNumber,
        UnitCost,
        ListPrice
    }
}
