using System;

namespace FirstLook.Valuation.DomainModel.Vehicles.Commands.TransferObjects
{
    [Serializable]
    public class ListDto
    {
        private string _handle = string.Empty;
        private string _name = string.Empty;
        private int _count;

        public string Handle
        {
            get { return _handle; }
            set { _handle = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int Count
        {
            get { return _count; }
            set { _count = value; }
        }
    }
}