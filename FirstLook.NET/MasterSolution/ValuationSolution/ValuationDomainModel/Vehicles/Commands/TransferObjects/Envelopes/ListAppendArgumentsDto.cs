﻿using System;

namespace FirstLook.Valuation.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ListAppendArgumentsDto
    {
        private string _brokerHandle;
        private string _vin;
        private int _odomoter;

        public string BrokerHandle
        {
            get { return _brokerHandle; }
            set { _brokerHandle = value; }
        }

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }

        public int Odomoter
        {
            get { return _odomoter; }
            set { _odomoter = value; }
        }
    }
}
