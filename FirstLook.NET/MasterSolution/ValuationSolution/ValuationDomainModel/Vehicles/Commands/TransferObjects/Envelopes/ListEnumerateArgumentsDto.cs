using System;

namespace FirstLook.Valuation.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ListEnumerateArgumentsDto
    {
        private string _brokerHandle;

        public string BrokerHandle
        {
            get { return _brokerHandle; }
            set { _brokerHandle = value; }
        }
    }
}
