using System;
using System.Collections.Generic;

namespace FirstLook.Valuation.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ListEnumerateResultsDto
    {
        private ListEnumerateArgumentsDto _arguments;
        private List<ListDto> _lists;

        public ListEnumerateArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public List<ListDto> Lists
        {
            get { return _lists; }
            set { _lists = value; }
        }
    }
}
