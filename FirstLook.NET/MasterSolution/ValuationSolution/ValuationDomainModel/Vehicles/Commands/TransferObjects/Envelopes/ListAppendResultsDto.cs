using System;

namespace FirstLook.Valuation.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ListAppendResultsDto
    {
        private ListAppendArgumentsDto _arguments;
        private VehicleDto _vehicle;

        public ListAppendArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public VehicleDto Vehicle
        {
            get { return _vehicle; }
            set { _vehicle = value; }
        }
    }
}
