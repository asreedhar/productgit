using System;

namespace FirstLook.Valuation.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class VehicleQueryResultsDto
    {
        private VehicleQueryArgumentsDto _arguments;
        private VehicleQueryResultsStatusDto _status;
        private VehicleDto _vehicle;

        public VehicleQueryArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public VehicleQueryResultsStatusDto Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public VehicleDto Vehicle
        {
            get { return _vehicle; }
            set { _vehicle = value; }
        }
    }
}
