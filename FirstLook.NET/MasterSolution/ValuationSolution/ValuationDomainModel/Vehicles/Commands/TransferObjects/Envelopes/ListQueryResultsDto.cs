using System;
using System.Collections.Generic;
using FirstLook.Valuation.DomainModel.Common.Commands.TransferObjects;

namespace FirstLook.Valuation.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ListQueryResultsDto : PaginatedResultsDto
    {
        private ListQueryArgumentsDto _arguments;
        private List<VehicleDto> _vehicles;

        public ListQueryArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public List<VehicleDto> Vehicles
        {
            get { return _vehicles; }
            set { _vehicles = value; }
        }
    }
}
