using System;
using FirstLook.Valuation.DomainModel.Common.Commands.TransferObjects;

namespace FirstLook.Valuation.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ListQueryArgumentsDto : PaginatedArgumentsDto
    {
        private string _brokerHandle;
        private string _handle = string.Empty;

        public string BrokerHandle
        {
            get { return _brokerHandle; }
            set { _brokerHandle = value; }
        }

        public string Handle
        {
            get { return _handle; }
            set { _handle = value; }
        }
    }
}
