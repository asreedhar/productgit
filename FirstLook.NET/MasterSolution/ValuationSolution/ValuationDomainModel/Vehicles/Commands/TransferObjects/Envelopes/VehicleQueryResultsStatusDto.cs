using System;

namespace FirstLook.Valuation.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public enum VehicleQueryResultsStatusDto
    {
        Undefined,
        Success,
        NotFound,
        VinInvalidLength,
        VinInvalidChecksum
    }
}
