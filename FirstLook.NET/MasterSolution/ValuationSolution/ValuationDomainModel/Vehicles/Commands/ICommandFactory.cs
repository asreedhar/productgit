using FirstLook.Common.Core.Command;
using FirstLook.Valuation.DomainModel.Common.Commands;
using FirstLook.Valuation.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;

namespace FirstLook.Valuation.DomainModel.Vehicles.Commands
{
    public interface ICommandFactory : IPolicyProvider
    {
        ICommand<VehicleQueryResultsDto, VehicleQueryArgumentsDto>
            CreateVehicleQueryCommand();

        ICommand<ListEnumerateResultsDto, ListEnumerateArgumentsDto>
            CreateListEnumerateCommand();

        ICommand<ListAppendResultsDto, ListAppendArgumentsDto>
            CreateListAppendCommand();

        ICommand<ListQueryResultsDto, ListQueryArgumentsDto>
            CreateListQueryCommand();
    }
}
