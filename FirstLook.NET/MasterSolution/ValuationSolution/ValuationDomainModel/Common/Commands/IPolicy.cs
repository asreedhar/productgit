﻿using System.Security.Principal;
using FirstLook.Valuation.DomainModel.Common.Commands.TransferObjects;

namespace FirstLook.Valuation.DomainModel.Common.Commands
{
    public interface IPolicy
    {
        PermissionsDto Permissions(IPrincipal principal);

        PermissionsDto Permissions(IPrincipal principal, string brokerHandle);

        PermissionsDto Permissions(IPrincipal principal, string brokerHandle, string vehicleHandle);
    }
}
