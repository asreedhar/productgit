﻿using System;
using System.Collections.Generic;

namespace FirstLook.Valuation.DomainModel.Common.Commands.TransferObjects
{
    /// <summary>
    /// <para>Abstract class for representing access to a system resource. All permissions
    /// have a name whose interpretation depends on the subclass.</para>
    /// <para>Most Permission objects also include an "actions" list that tells the actions
    /// that are permitted for the object.  The actions list is optional for Permission objects
    /// that don't need such a list; you either have the named permission (such as "system.exit")
    /// or you don't.</para>
    /// </summary>
    [Serializable]
    public abstract class PermissionDto
    {
        private string _name;
        private List<string> _actions = new List<string>();

        /// <summary>
        /// The name of this Permission.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// The list of actions for the permission.
        /// </summary>
        public List<string> Actions
        {
            get { return _actions; }
            set { _actions = value; }
        }
    }

    /// <summary>
    /// This class is used to represent the permission to execute a command.  The name of the
    /// permission is that of the command and the possible actions are:
    /// <list type="bullet">
    /// <item>
    /// <term>execute</term>
    /// <description>Invoke the command</description>
    /// </item>
    /// </list>
    /// </summary>
    [Serializable]
    public class CommandPermissionDto : PermissionDto
    {
        
    }
}
