namespace FirstLook.Valuation.DomainModel.Common.Commands.TransferObjects
{
    public class PaginatedArgumentsDto
    {
        private string _sortColumns;
        private int _maximumRows;
        private int _startRowIndex;

        public string SortColumns
        {
            get { return _sortColumns; }
            set { _sortColumns = value; }
        }

        public int MaximumRows
        {
            get { return _maximumRows; }
            set { _maximumRows = value; }
        }

        public int StartRowIndex
        {
            get { return _startRowIndex; }
            set { _startRowIndex = value; }
        }
    }
}