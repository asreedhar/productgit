﻿using System;
using System.Collections.Generic;

namespace FirstLook.Valuation.DomainModel.Common.Commands.TransferObjects
{
    [Serializable]
    public class PermissionsDto
    {
        private List<PermissionDto> _items = new List<PermissionDto>();

        public List<PermissionDto> Items
        {
            get { return _items; }
            set { _items = value; }
        }
    }
}
