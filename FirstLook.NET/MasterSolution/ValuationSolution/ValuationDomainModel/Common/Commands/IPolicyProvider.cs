﻿namespace FirstLook.Valuation.DomainModel.Common.Commands
{
    public interface IPolicyProvider
    {
        IPolicy Policy { get; }
    }
}
