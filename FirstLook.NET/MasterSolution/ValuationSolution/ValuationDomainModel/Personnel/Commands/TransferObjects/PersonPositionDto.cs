﻿using System;

namespace FirstLook.Valuation.DomainModel.Personnel.Commands.TransferObjects
{
    [Serializable]
    public enum PersonPositionDto
    {
        NotDefined = 0,
        Salesperson = 1,
        Appraiser = 2,
        Buyer = 3,
    }
}
