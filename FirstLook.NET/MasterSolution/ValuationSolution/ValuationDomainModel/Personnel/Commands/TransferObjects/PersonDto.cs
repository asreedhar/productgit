﻿using System;

namespace FirstLook.Valuation.DomainModel.Personnel.Commands.TransferObjects
{
    [Serializable]
    public class PersonDto
    {
        private int _id;
        private string _lastName;
        private string _middleInitial;
        private string _firstName;
        private string _nickName;
        private string _salutation;
        private string _phoneNumber;
        private string _email;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        public string MiddleInitial
        {
            get { return _middleInitial; }
            set { _middleInitial = value; }
        }

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        public string NickName
        {
            get { return _nickName; }
            set { _nickName = value; }
        }

        public string Salutation
        {
            get { return _salutation; }
            set { _salutation = value; }
        }

        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { _phoneNumber = value; }
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
    }
}
