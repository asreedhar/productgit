﻿using System;
using System.Collections.Generic;
using FirstLook.Valuation.DomainModel.Common.Commands.TransferObjects;

namespace FirstLook.Valuation.DomainModel.Personnel.Commands.TransferObjects
{
    [Serializable]
    public class PersonQueryArgumentsDto : PaginatedArgumentsDto
    {
        private string _brokerHandle;
        private PersonQueryArgumentsDto _example;
        private List<PersonPositionDto> _positions = new List<PersonPositionDto>();

        public string BrokerHandle
        {
            get { return _brokerHandle; }
            set { _brokerHandle = value; }
        }

        public PersonQueryArgumentsDto Example
        {
            get { return _example; }
            set { _example = value; }
        }

        public List<PersonPositionDto> Positions
        {
            get { return _positions; }
            set { _positions = value; }
        }
    }
}
