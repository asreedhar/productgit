using System;
using System.Collections.Generic;
using FirstLook.Valuation.DomainModel.Common.Commands.TransferObjects;

namespace FirstLook.Valuation.DomainModel.Personnel.Commands.TransferObjects
{
    [Serializable]
    public class PersonQueryResultsDto : PaginatedResultsDto
    {
        private PersonQueryArgumentsDto _arguments;
        private List<PersonDto> _results = new List<PersonDto>();

        public PersonQueryArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public List<PersonDto> Results
        {
            get { return _results; }
            set { _results = value; }
        }
    }
}
