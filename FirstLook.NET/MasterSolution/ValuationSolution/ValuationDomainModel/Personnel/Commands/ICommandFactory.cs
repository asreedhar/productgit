﻿using FirstLook.Common.Core.Command;
using FirstLook.Valuation.DomainModel.Common.Commands;
using FirstLook.Valuation.DomainModel.Personnel.Commands.TransferObjects;

namespace FirstLook.Valuation.DomainModel.Personnel.Commands
{
    public interface ICommandFactory : IPolicyProvider
    {
        ICommand<PersonQueryResultsDto, PersonQueryArgumentsDto>
            CreatePersonQueryCommand();
    }
}
