﻿using FirstLook.Common.Core.Command;
using FirstLook.Valuation.DomainModel.Common.Commands;
using FirstLook.Valuation.DomainModel.Inventory.Commands.TransferObjects.Envelopes;

namespace FirstLook.Valuation.DomainModel.Inventory.Commands
{
    public interface ICommandFactory : IPolicyProvider
    {
        ICommand<InventoryStockLevelsResultsDto, InventoryStockLevelsArgumentsDto>
            CreateInventoryStockLevelsCommand();
    }
}
