﻿using System;

namespace FirstLook.Valuation.DomainModel.Inventory.Commands.TransferObjects
{
    [Serializable]
    public class InventoryStockLevelsDto
    {
        private string _dealerName;
        private string _dealerTelephone;
        private string _dealerDistanceInMiles;

        public string DealerName
        {
            get { return _dealerName; }
            set { _dealerName = value; }
        }

        public string DealerTelephone
        {
            get { return _dealerTelephone; }
            set { _dealerTelephone = value; }
        }

        public string DealerDistanceInMiles
        {
            get { return _dealerDistanceInMiles; }
            set { _dealerDistanceInMiles = value; }
        }

        private string _modelYear;

        public string ModelYear
        {
            get { return _modelYear; }
            set { _modelYear = value; }
        }

        private int _optimalUnitsModelYear;
        private int _optimalUnitsModel;

        public int OptimalUnitsModelYear
        {
            get { return _optimalUnitsModelYear; }
            set { _optimalUnitsModelYear = value; }
        }

        public int OptimalUnitsModel
        {
            get { return _optimalUnitsModel; }
            set { _optimalUnitsModel = value; }
        }

        private int _stockedUnitsModelYear;
        private int _stockedUnitsModel;

        public int StockedUnitsModelYear
        {
            get { return _stockedUnitsModelYear; }
            set { _stockedUnitsModelYear = value; }
        }

        public int StockedUnitsModel
        {
            get { return _stockedUnitsModel; }
            set { _stockedUnitsModel = value; }
        }

        private bool _hasOptimalUnitsModelYear;
        private bool _hasOptimalUnitsModel;

        public bool HasOptimalUnitsModelYear
        {
            get { return _hasOptimalUnitsModelYear; }
            set { _hasOptimalUnitsModelYear = value; }
        }

        public bool HasOptimalUnitsModel
        {
            get { return _hasOptimalUnitsModel; }
            set { _hasOptimalUnitsModel = value; }
        }
    }
}
