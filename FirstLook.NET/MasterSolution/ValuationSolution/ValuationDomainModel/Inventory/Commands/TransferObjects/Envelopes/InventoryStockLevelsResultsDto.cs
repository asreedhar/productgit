﻿using System;
using System.Collections.Generic;

namespace FirstLook.Valuation.DomainModel.Inventory.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class InventoryStockLevelsResultsDto
    {
        private InventoryStockLevelsArgumentsDto _arguments;
        private List<InventoryStockLevelsDto> _results;

        public InventoryStockLevelsArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public List<InventoryStockLevelsDto> Results
        {
            get { return _results; }
            set { _results = value; }
        }
    }
}
