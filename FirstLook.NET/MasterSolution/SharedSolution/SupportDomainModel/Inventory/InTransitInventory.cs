﻿using System;
using System.ComponentModel;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Support.DomainModel.Inventory
{
    /// <summary>
    /// There are a variety of situations where it is necessary for a dealership to be able to create and manage an
    /// inventory item before it is available for extraction from the DMS. These kinds of inventory may be created from
    /// scratch or pre-populated from an existing appraisal.    
    /// </summary>
    public class InTransitInventory
    {
        #region Properties

        /// <summary>
        /// Dealer identifier.
        /// </summary>
        public int DealerId { get; set; }

        /// <summary>
        /// Identifier of the appraisal this inventory item is created from, if applicable.
        /// </summary>
        public int? AppraisalId { get; set; }

        /// <summary>
        /// Vehicle identification number.
        /// </summary>
        public string Vin { get; set; }

        /// <summary>
        /// Inventory stock number.
        /// </summary>
        public string StockNumber { get; set; }

        /// <summary>
        /// Vehicle mileage.
        /// </summary>
        public int? Mileage { get; set; }

        /// <summary>
        /// Unit cost.
        /// </summary>
        public double? UnitCost { get; set; }

        /// <summary>
        /// Type of deal -- purchase or trade?
        /// </summary>
        public int DealType
        {
            get
            {
                return _dealType;
            }

            set
            {
                if (value != 1 && value != 2)
                {
                    throw new ArgumentException("Deal type must be 'trade' or 'purchase'.");
                }
                _dealType = value;
            }
        }
        private int _dealType;        

        #endregion

        #region Validation

        /// <summary>
        /// Validate the in-transit inventory details.
        /// </summary>
        private static void Validate(InTransitInventory inventory)
        {
            if (string.IsNullOrEmpty(inventory.StockNumber))
            {
                throw new ApplicationException("Stock number cannot be empty.");
            }
            if (!inventory.AppraisalId.HasValue && string.IsNullOrEmpty(inventory.Vin))
            {
                throw new ApplicationException("One of appraisal id or VIN is expected.");
            }
            if (inventory.DealType != 1 && inventory.DealType != 2)
            {
                throw new ApplicationException("Inventory must be either 'Trade' or 'Purchase'.");
            }
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Create an empty in-transit inventory item for the given dealer.
        /// </summary>
        private InTransitInventory(int dealerId)
        {            
            DealerId = dealerId;
        }        

        /// <summary>
        /// Create in-transit inventory from the provided values.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <param name="appraisalId">Appraisal identifier.</param>
        /// <param name="vin">VIN.</param>
        /// <param name="stockNumber">Stock number.</param>
        /// <param name="mileage">Mileage.</param>
        /// <param name="unitCost">Unit cost.</param>
        /// <param name="dealType">Trade / purchase?</param>
        private InTransitInventory(int dealerId, int? appraisalId, string vin, string stockNumber, int? mileage, 
                               double? unitCost, int dealType)
        {
            DealerId = dealerId;
            AppraisalId = appraisalId;
            Vin = vin;
            StockNumber = stockNumber;
            Mileage = mileage;
            UnitCost = unitCost;
            DealType = dealType;
        }

        /// <summary>
        /// Create an in-transit inventory item. If an appraisal identifier is provided, pre-populate the item with the
        /// values of an existing appraisal. Otherwise, create an empty item.        
        /// </summary>
        /// <param name="dealerStr">Dealer identifier.</param>
        /// <param name="appraisalStr">Appraisal identifier.</param>
        /// <returns>A new in-transit inventory item, possibly pre-populated from an appraisal.</returns>
        public static InTransitInventory CreateInTransitInventory(string dealerStr, string appraisalStr)
        {
            int temp;            
            if (string.IsNullOrEmpty(dealerStr) || !int.TryParse(dealerStr, out temp))
            {
                throw new ApplicationException("Invalid dealer identifier.");
            }
            int dealerId = temp;

            int? appraisalId = null;            
            if (!string.IsNullOrEmpty(appraisalStr) && int.TryParse(appraisalStr, out temp))
            {
                appraisalId = temp;
            }

            InTransitInventory inventory;            
            if (!appraisalId.HasValue || appraisalId.Value < 0)
            {
                inventory = new InTransitInventory(dealerId);
            }
            else
            {                
                inventory = Create(dealerId, appraisalId.Value);
            }            
            return inventory;
        }

        #endregion

        #region Data Access

        /// <summary>
        /// Data source for populating and saving in-transit inventory items.
        /// </summary>
        [DataObject]
        public class DataSource
        {            
            /// <summary>
            /// Insert an in-transit inventory item into the database.
            /// </summary>  
            /// <param name="dealerId">Dealer identifier.</param>
            /// <param name="appraisalId">Appraisal identifier.</param>          
            /// <param name="vin">VIN.</param>            
            /// <param name="stockNumber">Stock number.</param>            
            /// <param name="mileage">Vehicle mileage.</param>
            /// <param name="unitCost">Unit cost.</param>
            /// <param name="dealType">Trade or purchase?</param>
            [DataObjectMethod(DataObjectMethodType.Insert)]            
            public void Insert(int dealerId, int? appraisalId, string vin, string stockNumber, int? mileage, 
                               double? unitCost, int dealType)
            {
                // Adjust the deal type.
                if (dealType == 0)
                {
                    dealType = 2;
                }

                try
                {
                    InTransitInventory inventory =
                     new InTransitInventory(dealerId, appraisalId, vin, stockNumber, mileage, unitCost, dealType);
                    InTransitInventory.Insert(inventory);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        /// <summary>
        /// Create an in-transit inventory item by pre-populating with the values of an existing appraisal.        
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <param name="appraisalId">Appraisal identifier.</param>
        protected static InTransitInventory Create(int dealerId, int appraisalId)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "dbo.Appraisals#Fetch";
                    command.CommandType = CommandType.StoredProcedure;

                    command.AddParameterWithValue("AppraisalID", DbType.Int32, false, appraisalId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            throw new DataException("Expected appraisal data but received none.");
                        }

                        string vin = reader.GetString(reader.GetOrdinal("VIN"));
                        int mileage = reader.GetInt32(reader.GetOrdinal("Mileage"));
                        int dealType = reader.GetInt32(reader.GetOrdinal("DealType"));

                        InTransitInventory inventory = 
                            new InTransitInventory(dealerId, appraisalId, vin.ToUpper(), null, mileage, null, dealType);
                        return inventory;                        
                    }
                }
            }
        }
    
        /// <summary>
        /// Insert an in-transit inventory item into the database.
        /// </summary>
        protected static void Insert(InTransitInventory inventory)
        {
            Validate(inventory);

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "dbo.InventoryInTransit#Create";
                    command.CommandType = CommandType.StoredProcedure;

                    command.AddParameterWithValue("BusinessUnitID", DbType.Int32, false, inventory.DealerId);
                    command.AddParameterWithValue("StockNumber", DbType.String, false, inventory.StockNumber);
                    command.AddParameterWithValue("TradeOrPurchase", DbType.Byte, false, (byte)inventory.DealType);

                    if (inventory.AppraisalId.HasValue)
                    {
                        command.AddParameterWithValue("AppraisalId", DbType.Int32, false, inventory.AppraisalId.Value);
                    }

                    /* Here we convert vin to uppercase because client side css doesnt make vin to be 
                    stored in the uppercase in the database*/
                    string VIN = inventory.Vin.ToUpper();
                    command.AddParameterWithValue("Vin", DbType.String, false,VIN);

                    if (inventory.Mileage.HasValue)
                    {
                        command.AddParameterWithValue("Mileage", DbType.Int32, false, inventory.Mileage.Value);
                    }

                    if (inventory.UnitCost.HasValue)
                    {
                        command.AddParameterWithValue("UnitCost", DbType.Decimal, false, (double)inventory.UnitCost.Value);
                    }

                    try
                    {
                        command.ExecuteNonQuery();
                    }   
                    // End-user-ify any db exception messages.
                    catch (Exception exception)
                    {
                        if (exception.Message.Contains("Inventory exists for this VIN"))
                        {
                            throw new DataException("In-transit inventory could not be created because active inventory already exists for this VIN.");
                        }
                        if (exception.Message.Contains("exists for this Stock Number"))
                        {
                            throw new DataException("In-transit inventory could not be created because active inventory already exists for this Stock Number.");
                        }
                        if (exception.Message.Contains("currently exists as inactive inventory"))
                        {
                            throw new DataException("In-transit inventory could not be created because vehicle exist in inactive inventory.");
                        }
                        throw;
                    }
                }
            }     
        }        

        #endregion
    }
}
