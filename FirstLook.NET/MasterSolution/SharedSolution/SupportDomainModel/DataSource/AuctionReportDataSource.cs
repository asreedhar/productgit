using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.Support.DomainModel.DataSource
{
    public class AuctionReportDataSource
    {
        private const string ReportId = "D6328AA1-A6CF-44bd-AF38-EB8909117905";

        private const string ReportDataSet_Summary = "AuctionTransactions_Summary";

        private const string ReportDataSet_Data = "AuctionTransactions_Data";

        private readonly DataTableCallback NullCallback = new DataTableCallback();

        public DataTable FindSummaryByVinOrVic(string vin, string vic, string areaId, string periodId)
        {
            if (string.IsNullOrEmpty(vin))
                throw new ArgumentNullException("vin", "Cannot be null nor empty");

            int value;

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["VIN"] = vin;
            parameterValues["VIC"] = vic;

            if (!string.IsNullOrEmpty(areaId) && Int32.TryParse(areaId, out value))
                parameterValues["AreaID"] = value;
            else
                parameterValues["AreaID"] = null;

            if (!string.IsNullOrEmpty(periodId) && Int32.TryParse(periodId, out value))
                parameterValues["PeriodID"] = value;
            else
                parameterValues["PeriodID"] = null;
            
            return ReportHelper.LoadReportDataTable(ReportId, ReportDataSet_Summary, parameterValues, NullCallback);
        }

        public DataTable FindDataByVinOrVic(string vin, string vic, string areaId, string periodId)
        {
            if (string.IsNullOrEmpty(vin))
                throw new ArgumentNullException("vin", "Cannot be null nor empty");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["VIN"] = vin;
            parameterValues["VIC"] = vic;
            parameterValues["AreaID"] = areaId;
            parameterValues["PeriodID"] = periodId;

            return ReportHelper.LoadReportDataTable(ReportId, ReportDataSet_Data, parameterValues, NullCallback);
        }
    }
}
