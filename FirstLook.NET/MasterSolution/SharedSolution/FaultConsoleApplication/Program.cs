﻿using System;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Configuration;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.Fault.DomainModel.Model;
using FirstLook.Fault.DomainModel.Repositories;

namespace FirstLook.Fault.Console
{
    /// <summary>
    /// The purpose of this application is to read the Rejections database to recover Java exceptions, and then using that information
    /// populate the Fault database so that a single repository will be responsible for housing the exceptions across stacks.
    /// </summary>
    class Fault
    {

        private const string Platform = "Java";

        static void Main()
        {
                      
            IRegistry registry = RegistryFactory.GetRegistry();
            registry.Register<DomainModel.Module>();
            registry.Register<IDataSessionManager, DataSessionManager>(ImplementationScope.Shared);

            IResolver resolver = RegistryFactory.GetResolver();
            var builder = resolver.Resolve<IFaultEventBuilder>();
            var faultRepository = resolver.Resolve<IFaultRepository>();

            //Used to gather some stats about failing exceptions from Rejections that could not be saved entirely (some stack trace elements incomplete).
            int countStackParserExceptions = 0; 
            StreamWriter file = new StreamWriter("FailedParsingIds.log");
            file.WriteLine("Import started at: " + DateTime.Now);
            file.WriteLine("The following StackTrace Ids from the Rejection.StackTrace table have thrown a parsing exception.");
 
            //Establish a connection to the Rejections database
            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings["Rejection"];
            DbProviderFactory factory = DbProviderFactories.GetFactory(settings.ProviderName);
            using (IDbConnection connection = factory.CreateConnection())
            {
                if (connection != null)
                {
                    connection.ConnectionString = settings.ConnectionString;

                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    using (IDbCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                    
                        //Build an SQL command to retrieve the exceptions from the corresponding rejections tables. 
                        command.CommandText =
                            @"
					SELECT Trace=Text, M.Login, E.Product, E.DateCreated, E.Source, E.HTTPSessionID, E.Id
                    FROM Rejection.StackTrace E
                    JOIN Rejection.Member M ON M.Id=E.MemberId 
					JOIN Rejection.Dealer B ON B.Id=E.DealerId
					";

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            //For each entry in the result set create a FaultEvent to be stored in the fault database.
                            while (reader.Read())
                            {
 
                                string traceText = reader.GetString(reader.GetOrdinal("Trace"));

                                string login = DataRecord.GetString(reader, "Login");

                                string product = DataRecord.GetString(reader, "Source");

                                string[] path = product.Split('/');

                                string application = null;

                                if (path.Length >= 2) application = "/" + path[1];

                                DateTime timestamp = reader.GetDateTime(reader.GetOrdinal("DateCreated"));

                                string source = DataRecord.GetString(reader, "Source");

                                string sessionId = DataRecord.GetString(reader, "HTTPSessionID");

                                string machine = sessionId.Substring(sessionId.LastIndexOf("on") + 3,
                                                                     sessionId.LastIndexOf("/") -
                                                                     sessionId.LastIndexOf("on") - 3);

                                int traceId = reader.GetInt32(reader.GetOrdinal("Id"));

                                traceText = traceText.Replace("||", "\n");

                                StackTraceParser parser = new StackTraceParser(new StringReader(traceText));
                                StackTrace trace = null;
                                try
                                {
                                    //Get the stack trace associated with the exception
                                    if (parser.Read())
                                    {
                                        trace = parser.GetStackTrace();
                                    }
                                }

                                catch (StackTraceParseException e)
                                {
                                    //If there wasn an exception thrown while parsing the stack trace then we need to get the set
                                    //of incomplete stack trace elements so we can record as much as possible.
                                    file.WriteLine("Id: " + traceId + "   " + e.Message);
                                    countStackParserExceptions++;
                                    trace = parser.GetIncompleteStackTrace();

                                }
                                finally
                                {
                                    //Using the acquired information build the fault and fault chain to build the FaultEvent object
                                    //to be saved in the Fault database
                                    builder.Application = application;
                                    builder.Machine = machine;
                                    builder.Platform = Platform;

                                    if (trace != null) builder.Message = trace.Description.ExceptionMessage;
                                    builder.Timestamp = timestamp;

                                    builder.BeginRequestorInformation();
                                    builder.User = login;
                                    builder.Path = source;
                                    builder.Url = string.Empty;
                                    builder.EndRequestorInformation();

                                    builder.BeginFault();

                                    //Check to see if there was a causing exception, if so then recursively handle all of the
                                    //causing exceptions.
                                    if (trace != null)
                                    {
                                        if (trace.Cause != null)
                                        {
                                            ProcessCauseFaults(trace.Cause, builder);
                                        }

                                        BuildFault(trace, builder);
                                    }
                                    builder.EndFault();
                                    parser.Dispose();
                                }

                                //Build the fault event.
                                IFaultEvent toSave = builder.ToFaultEvent();

                                //Save the fault event, returning the saved fault event.
                                ((FaultRepository)faultRepository).Save(toSave);
                           
                            }

                        }
                    }
                }
            }

            //Used to gather some stats about exceptions that were partially saved.
            file.WriteLine("Import completed at: " + DateTime.Now);
            file.WriteLine("Total parsing exceptions thrown: " + countStackParserExceptions);
            file.Close();
        }

        /// <summary>
        /// This is responsible for recursively handling the causing(inner) exceptions. If an exception has a causing exception (StackTrace)
        /// then recursively go through the causing StackTraces, building the FaultEvent to save.
        /// </summary>
        /// <param name="causeTrace">The StackTrace that caused the parent StackTrace.</param>
        /// <param name="builder">Used to continue building onto the FaultEvent to save.</param>
        static void ProcessCauseFaults(StackTrace causeTrace, IFaultEventBuilder builder)
        {
            builder.BeginCauseFault();
            if(causeTrace.Cause == null)
            {
                BuildFault(causeTrace, builder);
                builder.EndCauseFault();
                return;
            }
     
            ProcessCauseFaults(causeTrace.Cause, builder);
            BuildFault(causeTrace, builder);
            builder.EndCauseFault();
            return;
        }

        /// <summary>
        /// This will build on to the fault-in-progress, incorporating the stack and the stack trace elements.
        /// </summary>
        /// <param name="trace"> The stack trace to add to the fault being built. </param>
        /// <param name="builder"> The active builder. </param>
        private static void BuildFault(StackTrace trace, IFaultEventBuilder builder)
        {
            builder.ExceptionType = trace.Description.ExceptionType;
            if (trace.Elements != null)
            {
                builder.BeginStack();

                if (trace.Elements.Count > 0)
                {
                    foreach (StackTraceElement frame in trace.Elements)
                    {
                        builder.BeginStackFrame();
                        builder.ClassName = frame.ClassName;
                        builder.FileName = frame.FileName;
                        builder.MethodName = frame.MethodName;
                        builder.IsNativeMethod = frame.IsNativeMethod;
                        builder.IsUnknownSource = frame.IsUnknownSource;
                        builder.LineNumber = frame.LineNumber;
                        builder.EndStackFrame();
                    }

                }
                builder.EndStack();
            }

        }

    }
}
