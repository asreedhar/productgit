﻿using System;

namespace FirstLook.Fault.Console
{
    /// <summary>
    /// One line of a Java stack trace.
    /// </summary>
    /// <remarks>
    /// <para>The information is parsed output from the following Java code.</para>
    /// <code>
    /// return getClassName() + "." + methodName +
    ///		(isNativeMethod()
    ///			? "(Native Method)"
    ///			: (fileName != null && lineNumber >= 0
    ///				? "(" + fileName + ":" + lineNumber + ")"
    ///				: (fileName != null
    ///					? "("+fileName+")"
    ///					: "(Unknown Source)")));
    /// </code>
    /// </remarks>
    [Serializable]
    public class StackTraceElement : IEquatable<StackTraceElement>
    {
        private readonly string _className;
        private readonly string _fileName;
        private readonly string _methodName;
        private readonly int? _lineNumber;
        private readonly bool _isNativeMethod;
        private readonly bool _isUnknownSource;

        public StackTraceElement(string className, string fileName, string methodName, int? lineNumber,
                                 bool isNativeMethod, bool isUnknownSource)
        {
            _className = className;
            _fileName = fileName;
            _methodName = methodName;
            _lineNumber = lineNumber;
            _isNativeMethod = isNativeMethod;
            _isUnknownSource = isUnknownSource;
        }

        public string ClassName
        {
            get { return _className; }
        }

        public string FileName
        {
            get { return _fileName; }
        }

        public string MethodName
        {
            get { return _methodName; }
        }

        public int? LineNumber
        {
            get { return _lineNumber; }
        }

        public bool IsNativeMethod
        {
            get { return _isNativeMethod; }
        }

        public bool IsUnknownSource
        {
            get { return _isUnknownSource; }
        }

        public bool Equals(StackTraceElement stackTraceElement)
        {
            if (stackTraceElement == null) return false;
            if (!Equals(_className, stackTraceElement._className)) return false;
            if (!Equals(_fileName, stackTraceElement._fileName)) return false;
            if (!Equals(_methodName, stackTraceElement._methodName)) return false;
            if (!Equals(_lineNumber, stackTraceElement._lineNumber)) return false;
            if (!Equals(_isNativeMethod, stackTraceElement._isNativeMethod)) return false;
            if (!Equals(_isUnknownSource, stackTraceElement._isUnknownSource)) return false;
            return true;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj as StackTraceElement);
        }

        public override int GetHashCode()
        {
            int result = _className.GetHashCode();
            result = 29 * result + (_fileName != null ? _fileName.GetHashCode() : 0);
            result = 29 * result + _methodName.GetHashCode();
            result = 29 * result + _lineNumber.GetHashCode();
            result = 29 * result + _isNativeMethod.GetHashCode();
            result = 29 * result + _isUnknownSource.GetHashCode();
            return result;
        }
    }
}