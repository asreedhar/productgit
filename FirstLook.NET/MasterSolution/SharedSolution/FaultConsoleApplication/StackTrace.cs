﻿using System;
using System.Collections.Generic;

namespace FirstLook.Fault.Console
{
    [Serializable]
    public class StackTrace : IEquatable<StackTrace>
    {
        private readonly StackTraceDescription _description;
        private readonly List<StackTraceElement> _elements = new List<StackTraceElement>();
        private StackTrace _cause;

        public StackTrace(StackTraceDescription description)
        {
            if (description == null)
                throw new ArgumentNullException("description");
            _description = description;
        }

        public StackTraceDescription Description
        {
            get { return _description; }
        }

        public List<StackTraceElement> Elements
        {
            get { return _elements; }
        }

        public StackTrace Cause
        {
            get { return _cause; }
            set { _cause = value; }
        }

        public void InitCause(StackTrace stackTrace)
        {
            if (_cause != null)
                throw new InvalidOperationException("Cause already initialized");
            _cause = stackTrace;
        }

        public bool Equals(StackTrace stackTrace)
        {
            if (stackTrace == null) return false;
            if (!Equals(_description, stackTrace._description)) return false;
            if (!Equals(_elements, stackTrace._elements)) return false;
            if (!Equals(_cause, stackTrace._cause)) return false;
            return true;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj as StackTrace);
        }

        public override int GetHashCode()
        {
            int result = _description.GetHashCode();
            result = 29 * result + GetHashCode(_elements);
            result = 29 * result + (_cause != null ? _cause.GetHashCode() : 0);
            return result;
        }

        private static bool Equals(List<StackTraceElement> x, List<StackTraceElement> y)
        {
            List<StackTraceElement> xs = x.FindAll(OurCode);

            List<StackTraceElement> ys = y.FindAll(OurCode);

            if (xs.Count == ys.Count)
            {
                for (int i = 0, c = xs.Count; i < c; i++)
                {
                    if (!xs[i].Equals(ys[i]))
                    {
                        return false;
                    }
                }

                return true;
            }

            return false;
        }

        private static int GetHashCode(List<StackTraceElement> x)
        {
            int result = 1;
            List<StackTraceElement> xs = x.FindAll(OurCode);
            foreach (StackTraceElement xi in xs)
            {
                result = 29 * result + xi.GetHashCode();
            }
            return result;
        }


        public static bool OurCode(StackTraceElement element)
        {
            string name = element.ClassName;
            if (string.IsNullOrEmpty(name))
                return false;
            return (name.StartsWith("biz.firstlook.") ||
                    name.StartsWith("com.firstlook."));
        }
    }
}