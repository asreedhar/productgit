﻿using System;
using System.IO;

namespace FirstLook.Fault.Console
{
    public class StackTraceParser : IDisposable
    {
        private readonly StackTraceTokenizer _tokenizer;
        private bool _disposed;
        private bool _initialized;
        private bool _ready;
        private bool _error;
        private StackTraceToken _token;

        private char[] _buffer = new char[8192];
        private int _bufferSize;

        private StackTrace _traceRoot;
        private StackTrace _trace;




        public StackTraceParser(TextReader reader)
        {
            _tokenizer = new StackTraceTokenizer(reader);
        }

        public StackTrace GetStackTrace()
        {
            if (_disposed)
                throw new InvalidOperationException("Parser was disposed");
            if (!_ready)
                throw new InvalidOperationException("Parser result not ready. Call 'Read' next time!");
            return _traceRoot;
        }

        public StackTrace GetIncompleteStackTrace()
        {
            if (_disposed)
                throw new InvalidOperationException("Parser was disposed");
            if (!_error)
                throw new InvalidOperationException("Parser did not die. Call 'Read' next time!");
            return _traceRoot;
        }

        public bool Read()
        {
            if (_disposed)
                throw new InvalidOperationException("Parser was disposed");

            if (_initialized)
                return _ready;

            _ready = false;

            if (!_initialized)
            {
                _initialized = true;

                _token = _tokenizer.NextToken();
            }

            if (_token == StackTraceToken.EndOfFile)
            {
                return _ready;
            }

            try
            {
                StackTrace();
            }
            catch (StackTraceParseException e)
            {
                _error = true;
                throw;
            }

            return (_ready = true);
        }

        private void StackTrace()
        {
            ExceptionDescription();


            while (_token != StackTraceToken.Whitespace)
            {
                while (_token != StackTraceToken.CR && _token != StackTraceToken.LF && _token != StackTraceToken.EndOfFile)
                {
                    _token = _tokenizer.NextToken();
                }
                if (!AcceptNewLine())
                {
                    throw new StackTraceParseException("Expected a 'NewLine;'", "ExceptionDescription", _tokenizer.Line,
                                                    _tokenizer.Column, _token);
                }
            } 

            StackTraceElementList();

            StackTraceAsCause();
        }

        private void ExceptionDescription()
        {
            SkipWhiteSpace();

            ClassName();

            string type = PopText();

            SkipWhiteSpace();

            string message = string.Empty;

            if (Accept(StackTraceToken.Colon))
            {
                SkipWhiteSpace();

                if (CollectToNewLine())
                {
                    message = PopText();
                }
            }

            if (!AcceptNewLine())
            {
                throw new StackTraceParseException("Expected a 'NewLine;'", "ExceptionDescription", _tokenizer.Line,
                                                   _tokenizer.Column, _token);
            }


            _traceRoot = new StackTrace(new StackTraceDescription(type, message));

            _trace = _traceRoot;
            
        }

        private void ClassName()
        {
            if (Collect(StackTraceToken.Text))
            {
                while (Collect(StackTraceToken.Period))
                {
                    if (!Collect(StackTraceToken.Text))
                    {
                        throw new StackTraceParseException("Period without following Text", "ClassName", _tokenizer.Line,
                                                           _tokenizer.Column, _token);
                    }
                }
            }
            else
            {
                throw new StackTraceParseException("Expected Text", "ClassName", _tokenizer.Line, _tokenizer.Column,
                                                   _token);
            }
        }

        private void StackTraceElementList()
        {           
            while (Accept(StackTraceToken.Whitespace))
            {
                if (AcceptText("at"))
                {
                    if (Accept(StackTraceToken.Whitespace))
                    {
                        StackTraceElement();
                    }
                    else
                    {
                        throw new StackTraceParseException("Expected Whitespace", "StackTraceElementList",
                                                           _tokenizer.Line, _tokenizer.Column, _token);
                    }
                }
                else
                {
                    break;
                }
            }
        }

        /// <remarks>
        /// <para>Parses code generated by the following Java code where <code>trace</code>
        /// is a stack trace element.</para>
        /// <example>
        /// <code>"\tat " + trace[i];</code>
        /// </example>
        /// <para>This is confused by the retarded Spring framework whose nested exception
        /// type includes the </para>
        /// </remarks>
        private void StackTraceElement()
        {
            ClassName();

            string classAndMethod = PopText();

            int lastPeriod = classAndMethod.LastIndexOf('.');

//          if (lastPeriod == -1 && _token == StackTraceToken.EndOfFile)  I was running into errors in which lastPeriod was -1 but _token was 
//          whitespace, this also generated an exception so I added that case to the if condition below.
            if (lastPeriod == -1 && (_token == StackTraceToken.EndOfFile || _token == StackTraceToken.Whitespace))
            {
                throw new StackTraceParseException("Truncated StackTrace", "StackTraceElement", _tokenizer.Line,
                                                   _tokenizer.Column, _token);
            }

            string className = classAndMethod.Substring(0, lastPeriod);

            string methodName = classAndMethod.Substring(lastPeriod + 1);

            bool isNativeMethod = false, isUnknownSource = false;

            string fileName = string.Empty;

            int? lineNumber = null;

            if (Accept(StackTraceToken.LParen))
            {
                if (AcceptText("StackTraceElement", "Native", "Method"))
                {
                    isNativeMethod = true;

                    goto CloseParen;
                }
                else if (AcceptText("StackTraceElement", "Unknown", "Source"))
                {
                    isUnknownSource = true;

                    goto CloseParen;
                }
                else if (Collect(StackTraceToken.Text))
                {
                    if (Collect(StackTraceToken.Period))
                    {
                        if (Collect(StackTraceToken.Text))
                        {
                            fileName = PopText();

                            if (Accept(StackTraceToken.Colon))
                            {
                                if (Collect(StackTraceToken.Text))
                                {
                                    string lineNumberText = PopText();
                                    int l;
                                    if (int.TryParse(lineNumberText, out l))
                                    {
                                        lineNumber = l;
                                    }
                                }
                                else
                                {
                                    throw new StackTraceParseException("Missing line number (expected Text)",
                                                                       "StackTraceElement", _tokenizer.Line,
                                                                       _tokenizer.Column, _token);
                                }
                            }
                        }
                    }
                }
                else
                {
                    throw new StackTraceParseException("Expected code location", "StackTraceElement", _tokenizer.Line,
                                                       _tokenizer.Column, _token);
                }

            CloseParen:

                if (Accept(StackTraceToken.RParen))
                {
                    if (AcceptNewLine())
                    {
                        _trace.Elements.Add(new StackTraceElement(
                                                className, fileName, methodName, lineNumber,
                                                isNativeMethod, isUnknownSource));
                    }
                    else
                    {
                        throw new StackTraceParseException("Expected a 'NewLine'", "ExceptionDescription",
                                                           _tokenizer.Line, _tokenizer.Column, _token);
                    }
                }
                else
                {
                    throw new StackTraceParseException("Expected closing parens", "StackTraceElement", _tokenizer.Line,
                                                       _tokenizer.Column, _token);
                }
            }
            else
            {
                throw new StackTraceParseException("Expected opening parens", "StackTraceElement", _tokenizer.Line,
                                                   _tokenizer.Column, _token);
            }
        }

        /// <remarks>
        /// <para>Parses text generated by the following Java code.</para>
        /// <example>
        /// <code>
        /// s.println("Caused by: " + this);
        /// for (int i=0; i <= m; i++)
        ///		s.println("\tat " + trace[i]);
        /// if (framesInCommon != 0)
        ///		s.println("\t... " + framesInCommon + " more");
        /// </code>
        /// </example>
        /// </remarks>
        private void StackTraceAsCause()
        {
            if (CauseDescription())
            {
                StackTraceElementList();

                FramesInCommon();

                StackTraceAsCause();
            }
        }

        private bool CauseDescription()
        {
            if (AcceptText("CauseDescription", "Caused", "By"))
            {
                if (!Accept(StackTraceToken.Colon))
                {
                    throw new StackTraceParseException("Expected colon", "CauseDescription", _tokenizer.Line,
                                                       _tokenizer.Column, _token);
                }

                SkipWhiteSpace();

                ClassName();

                string type = PopText();

                string message = string.Empty;

                if (Accept(StackTraceToken.Colon))
                {
                    SkipWhiteSpace();

                    if (CollectToNewLine())
                    {
                        message = PopText();
                    }
                }

                if (!AcceptNewLine())
                {
                    throw new StackTraceParseException("Expected NewLine", "CauseDescription", _tokenizer.Line,
                                                       _tokenizer.Column, _token);
                }

                StackTrace cause = new StackTrace(new StackTraceDescription(type, message));

                _trace.InitCause(cause);

                _trace = cause;

                return true;
            }

            return false;
        }

        private void FramesInCommon()
        {
            SkipWhiteSpace();

            if (AcceptText("..."))
            {
                if (CollectToNewLine())
                {
                    if (AcceptNewLine())
                    {
                        PopText();
                    }
                    else
                    {
                        throw new StackTraceParseException("Expected NewLine", "FramesInCommon", _tokenizer.Line,
                                                           _tokenizer.Column, _token);
                    }
                }
            }
        }

        private string PopText()
        {
            if (_bufferSize == 0)
            {
                return string.Empty;
            }
            else
            {
                string field = new string(_buffer, 0, _bufferSize);

                _bufferSize = 0;

                return field;
            }
        }

        private void PushText()
        {
            if (_bufferSize + _tokenizer.Length >= _buffer.Length)
            {
                char[] newBuffer = new char[_buffer.Length + 1024];
                Buffer.BlockCopy(_buffer, 0, newBuffer, 0, sizeof(char) * _buffer.Length);
                _buffer = newBuffer;
            }

            _bufferSize += _tokenizer.CopyTo(_buffer, _bufferSize);
        }

        private void SkipWhiteSpace()
        {
            while (_token == StackTraceToken.Whitespace)
            {
                _token = _tokenizer.NextToken();
            }
        }

        private bool AcceptText(string nonTerminal, params string[] text)
        {
            for (int i = 0, l = text.Length; i < l; i++)
            {
                if (AcceptText(text[i]))
                {
                    if (i + 1 < l && !Accept(StackTraceToken.Whitespace))
                    {
                        throw new StackTraceParseException(
                            "Expected whitespace after text '" + text[i] + "'",
                            nonTerminal, _tokenizer.Line, _tokenizer.Column, _token);
                    }
                }
                else
                {
                    if (i == 0)
                    {
                        return false;
                    }
                    else
                    {
                        throw new StackTraceParseException(
                            "Expected text '" + text[i] + "'",
                            nonTerminal, _tokenizer.Line, _tokenizer.Column, _token);
                    }
                }
            }

            return true;
        }

        private bool AcceptText(string text)
        {
            if (_token == StackTraceToken.Text)
            {
                if (string.Equals(_tokenizer.TokenText(), text, StringComparison.OrdinalIgnoreCase))
                {
                    _token = _tokenizer.NextToken();

                    return true;
                }
            }

            return false;
        }

        private bool Accept(StackTraceToken token)
        {
            if (_token == token)
            {
                _token = _tokenizer.NextToken();

                return true;
            }

            return false;
        }

        private bool AcceptNewLine()
        {
            if (_token == StackTraceToken.CR)
            {
                _token = _tokenizer.NextToken();

                if (_token == StackTraceToken.LF)
                {
                    _token = _tokenizer.NextToken();

                    return true;
                }

                return true;
            }
            else if (_token == StackTraceToken.LF)
            {
                _token = _tokenizer.NextToken();

                return true;
            }

            return false;
        }

        private bool Collect(StackTraceToken token)
        {
            if (_token == token)
            {
                PushText();

                _token = _tokenizer.NextToken();

                return true;
            }

            return false;
        }

        private bool CollectToNewLine()
        {
            int c = 0;

            while (_token != StackTraceToken.CR && _token != StackTraceToken.LF)
            {
                PushText();

                c++;

                _token = _tokenizer.NextToken();
            }

            return c > 0;
        }

        #region IDisposable Members

        ~StackTraceParser()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            if (!_disposed)
            {
                try
                {
                    Dispose(true);
                }
                finally
                {
                    GC.SuppressFinalize(this);
                    _disposed = true;
                }
            }
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_tokenizer != null)
                {
                    _tokenizer.Dispose();
                }
            }
            _initialized = false;
            _ready = false;
        }

        #endregion
    }
}
