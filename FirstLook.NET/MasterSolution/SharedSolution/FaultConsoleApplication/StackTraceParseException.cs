﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace FirstLook.Fault.Console
{
    public class StackTraceParseException : Exception
    {
        private readonly string _nonTerminal;
        private readonly int _line;
        private readonly int _column;
        private readonly StackTraceToken _token;

        /// <summary>
        /// The name of the non-terminal being processed when the exception occurred.
        /// </summary>
        public string NonTerminal
        {
            get { return _nonTerminal; }
        }

        /// <summary>
        /// The input line on which the exception occurred.
        /// </summary>
        public int Line
        {
            get { return _line; }
        }

        /// <summary>
        /// The index of the column at which the exception occurred.
        /// </summary>
        public int Column
        {
            get { return _column; }
        }

        /// <summary>
        /// The unexpected token that caused the exception to be raised.
        /// </summary>
        public StackTraceToken Token
        {
            get { return _token; }
        }

        public StackTraceParseException(string nonTerminal, int line, int column, StackTraceToken token)
        {
            _nonTerminal = nonTerminal;
            _line = line;
            _column = column;
            _token = token;
        }

        public StackTraceParseException(string message, string nonTerminal, int line, int column, StackTraceToken token)
            : base(message)
        {
            _nonTerminal = nonTerminal;
            _line = line;
            _column = column;
            _token = token;
        }

        /// <summary>
        /// Initializes a new instance of the <code>StackTraceParseException</code> class.
        /// </summary>
        /// <remarks>
        /// This constructor is not called from the library code as all informational properties
        /// are not specified.
        /// </remarks>
        public StackTraceParseException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <code>StackTraceParseException</code> class with a specified
        /// error message.
        /// </summary>
        /// <remarks>
        /// This constructor is not called from the library code as all informational properties
        /// are not specified.
        /// </remarks>
        public StackTraceParseException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <code>StackTraceParseException</code> class.
        /// </summary>
        /// <remarks>
        /// This constructor is not called from the library code as all informational properties
        /// are not specified.
        /// </remarks>
        public StackTraceParseException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected StackTraceParseException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            _nonTerminal = info.GetString("NonTerminal");
            _line = info.GetInt32("Line");
            _column = info.GetInt32("Column");
            _token = (StackTraceToken)info.GetValue("Token", typeof(StackTraceToken));
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }
            base.GetObjectData(info, context);
            info.AddValue("NonTerminal", _nonTerminal, typeof(string));
            info.AddValue("Line", _line);
            info.AddValue("Column", _column);
            info.AddValue("Token", _token, typeof(StackTraceToken));
        }

        public override string ToString()
        {
            string text = "Parse exception whilst processing non-terminal " + _nonTerminal + " on line " + Line +
                          " column " + Column + ".";
            if (!string.IsNullOrEmpty(Message))
                text += " " + Message;
            return text;
        }

        public override string Message
        {
            get
            {
                string text = "Parse exception whilst processing non-terminal " + _nonTerminal + " on line " + Line +
                              " column " + Column + ".";
                if (!string.IsNullOrEmpty(base.Message))
                    text += " " + base.Message;
                return text;
            }
        }
    }
}