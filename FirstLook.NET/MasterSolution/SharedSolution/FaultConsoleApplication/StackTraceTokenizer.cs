﻿using System;
using System.IO;

namespace FirstLook.Fault.Console
{
    [Serializable]
    public class StackTraceTokenizer : IDisposable
    {
        private const int CR = '\r';
        private const int LF = '\n';
        private const int Period = '.';
        private const int Colon = ':';
        private const int LParen = '(';
        private const int RParen = ')';
        private const int SizeOfChar = sizeof(char);

        private readonly TextReader _reader;
        private char[] _buffer;
        private char[] _chars;
        private int _begin;
        private int _index;
        private int _length;
        private int _line = 1;
        private int _column;

        private bool _isDisposed;

        private StackTraceToken _token = StackTraceToken.Whitespace;

        public StackTraceTokenizer(TextReader reader)
        {
            _reader = reader;

            _buffer = new char[1024 * 4];

            _length = _reader.Read(_buffer, 0, 1024 * 4);
        }

        public StackTraceToken NextToken()
        {
            _chars = null;

            // fill buffer (unless EOF)

            if (_length > 0 && _index >= _length)
            {
                _length = _reader.Read(_buffer, 0, _buffer.Length);

                _index = 0;
            }

            // take the start position

            _begin = _index;

            // read next character

            int c = (_length == 0) ? -1 : _buffer[_index++];

            if (c == -1)
            {
                _token = StackTraceToken.EndOfFile;
            }
            else
            {
                _column++;

                if (c == CR)
                {
                    _token = StackTraceToken.CR;
                }
                else if (c == LF)
                {
                    _token = StackTraceToken.LF;
                }
                else if (c == Period)
                {
                    _token = StackTraceToken.Period;
                }
                else if (c == Colon)
                {
                    _token = StackTraceToken.Colon;
                }
                else if (c == LParen)
                {
                    _token = StackTraceToken.LParen;
                }
                else if (c == RParen)
                {
                    _token = StackTraceToken.RParen;
                }
                else
                {
                    _token = Char.IsWhiteSpace((char)c)
                                 ? StackTraceToken.Whitespace
                                 : StackTraceToken.Text;

                    // fill the buffer (if have just read the last char)

                    if (_index >= _length)
                    {
                        _chars = new char[] { (char)c };

                        _length = _reader.Read(_buffer, 0, _buffer.Length);

                        _begin = _index = 0;
                    }

                    // consume all subsequent (whitespace OR text) data

                    while (_length > 0)
                    {
                        while (_index < _length)
                        {
                            c = _buffer[_index];

                            bool isWs = Char.IsWhiteSpace((char)c);

                            if (c == CR || c == LF || c == Period || c == Colon || c == LParen || c == RParen ||
                                (_token == StackTraceToken.Whitespace && !isWs) ||
                                (_token == StackTraceToken.Text && isWs))
                            {
                                goto MarshallChars; // break from outer loop
                            }

                            _index++;

                            _column++;
                        }

                        // save matched characters and fill buffer

                        AppendCharacters();

                        _length = _reader.Read(_buffer, 0, _buffer.Length);

                        _begin = _index = 0;
                    }

                MarshallChars:

                    if (_chars != null && _begin != _index)
                    {
                        AppendCharacters();
                    }
                }
            }

            if (_token == StackTraceToken.LF)
            {
                _line++;

                _column = 0;
            }

            return _token;
        }

        private void AppendCharacters()
        {
            int numNewChars = (_index - _begin), numOldChars = _chars == null ? 0 : _chars.Length;

            if (numOldChars + numNewChars == 0)
            {
                throw new InvalidOperationException("Must have more than one character to append");
            }

            char[] newChars = new char[numOldChars + numNewChars];

            if (numOldChars > 0)
            {
                Buffer.BlockCopy(_chars, 0, newChars, 0, SizeOfChar * numOldChars);
            }

            if (numNewChars > 0)
            {
                Buffer.BlockCopy(_buffer, SizeOfChar * _begin, newChars, SizeOfChar * numOldChars, SizeOfChar * numNewChars);
            }

            _chars = newChars;

            _begin = _index;
        }

        public StackTraceToken Token
        {
            get { return _token; }
        }

        public int Line
        {
            get { return _line; }
        }

        public int Column
        {
            get { return _column; }
        }

        public int Length
        {
            get { return (_chars == null) ? (_index - _begin) : _chars.Length; }
        }

        public int CopyTo(char[] array, int index)
        {
            int len;

            if (_chars != null)
            {
                len = _chars.Length;

                Buffer.BlockCopy(_chars, 0, array, SizeOfChar * index, SizeOfChar * len);
            }
            else
            {
                len = (_index - _begin);

                Buffer.BlockCopy(_buffer, SizeOfChar * _begin, array, SizeOfChar * index, SizeOfChar * len);
            }

            return len;
        }

        public string TokenText()
        {
            if (_chars != null)
            {
                return new string(_chars);
            }
            else
            {
                return new string(_buffer, _begin, _index - _begin);
            }
        }

        #region IDisposable Members

        ~StackTraceTokenizer()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            if (!_isDisposed)
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
        }

        private void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    try
                    {
                        _buffer = null;
                        _chars = null;

                        if (_reader != null)
                        {
                            _reader.Dispose();
                        }
                    }
                    finally
                    {
                        _isDisposed = true;
                    }
                }
            }
        }

        #endregion
    }
}