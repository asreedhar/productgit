﻿using System;

namespace FirstLook.Fault.Console
{
    [Serializable]
    public class StackTraceSummary
    {
        private readonly Guid _id = Guid.NewGuid();
        private readonly StackTrace _trace;
        private int _count = 1;

        public StackTraceSummary(StackTrace trace, int count)
        {
            _trace = trace;
            _count = count;
        }

        public void IncrementCount(int count)
        {
            _count += count;
        }

        public Guid Id
        {
            get { return _id; }
        }

        public StackTrace Trace
        {
            get { return _trace; }
        }

        public string ExceptionType
        {
            get { return _trace.Description.ExceptionType; }
        }

        public string ExceptionMessage
        {
            get { return _trace.Description.ExceptionMessage; }
        }

        public int Count
        {
            get { return _count; }
        }
    }
}