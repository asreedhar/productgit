﻿using System;

namespace FirstLook.Fault.Console
{
    [Serializable]
    public enum StackTraceToken
    {
        Text,
        Period,
        Colon,
        Whitespace,
        LParen,
        RParen,
        CR,
        LF,
        EndOfFile
    }
}
