﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Demonstration.aspx.cs"
    Inherits="FirstLook.Fault.WebApplication.Pages.Fault.Demonstration"
    Theme="Grey" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Demonstration</title>
</head>
<body>
    <form id="DemonstrationForm" runat="server">
        <div id="container">
            <div id="watermark">
                <p>www.firstlook.biz</p>
            </div>
            <div id="header">
                <h1>Fault</h1>
                <h2>As in it's <em>your</em> fault ...</h2>
                <span style="clear:both;"></span>
            </div>
            <ul id="nav">
                <li><a href="#graphs">Graphs</a></li>
                <li><a href="#tables">Tables</a></li>
            </ul>
            <div id="sidebar">
                <table>
                    <thead>
                        <tr>
                            <th>Application</th>
                            <th>Count</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div id="content">
                <table>
                    <thead>
                        <tr>
                            <th>Application</th>
                            <th>Exception Type</th>
                            <th>First Seen</th>
                            <th>Last Seen</th>
                            <th>Count</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div id="footer">
                <p>&copy; 2011 Simon Is Awesome - <em>Bugs are Not</em> | template design by <a href="http://www.webtom.be">webtom.be</a></p>
            </div>
        </div>
    </form>

    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/Lib/jquery/jquery-ui-1.8.2.custom/js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/Fault/Application.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/Fault/Controls.js"></script>

    <script type="text/javascript" charset="utf-8">
        var _Fault = new Fault();
        _Fault.main();

        if (typeof JSON === "undefined") {
            $LAB.script("../../Public/Scripts/Lib/json/json2.min.js");
        }
    </script>
</body>
</html>
