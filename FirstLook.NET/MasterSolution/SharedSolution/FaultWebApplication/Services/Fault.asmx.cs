﻿using System.Web.Services;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes;
using FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes.Reporting;
using ICommandFactory=FirstLook.Fault.DomainModel.Commands.ICommandFactory;

namespace FirstLook.Fault.WebApplication.Services
{
    /// <summary>
    /// Summary description for Fault
    /// </summary>
    [WebService(Namespace = "http://services.firstlook.biz/Fault/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class Fault : WebService
    {
        /// <summary>
        /// This web service function will save a fault event passed in via the SaveArgumentsDto
        /// </summary>
        /// <param name="arguments">Contains a faultevent object to be saved in the database.</param>
        /// <returns>Returns a SaveResultsDto, which just contains a saved fault event object.</returns>
        [WebMethod]
        public SaveRemoteResultsDto SaveFault(SaveRemoteArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<SaveRemoteResultsDto, SaveRemoteArgumentsDto> command = factory.CreateSaveCommand();

            return command.Execute(arguments);
        }

        [WebMethod]
        public FetchFaultEventResultsDto FetchFaultEvent(FetchFaultEventArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<FetchFaultEventResultsDto, FetchFaultEventArgumentsDto> command = factory.CreateFetchFaultEventCommand();

            return command.Execute(arguments);
        }

        #region Reporting
        
        [WebMethod]
        public ApplicationSummaryResultsDto ApplicationSummary(ApplicationSummaryArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<ApplicationSummaryResultsDto, ApplicationSummaryArgumentsDto> command = factory.CreateApplicationSummaryCommand();

            return command.Execute(arguments);
        }

        [WebMethod]
        public FaultSummaryResultsDto FaultSummary(FaultSummaryArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<FaultSummaryResultsDto, FaultSummaryArgumentsDto> command = factory.CreateFaultSummaryCommand();

            return command.Execute(arguments);
        }

        [WebMethod]
        public FaultEventSummaryResultsDto FaultEventSummary(FaultEventSummaryArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<FaultEventSummaryResultsDto, FaultEventSummaryArgumentsDto> command = factory.CreateFaultEventSummaryCommand();

            return command.Execute(arguments);
        }

        #endregion
    }
}