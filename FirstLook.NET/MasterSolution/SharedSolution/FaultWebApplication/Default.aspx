﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="FirstLook.Fault.WebApplication._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Fault - Default Page</title>
</head>
<body>
    <form id="DefaultForm" runat="server">
    <div>
        <ul>
            <li>Pages
                <ul>
                    <li>
                        <asp:HyperLink runat="server" ID="FaultPage" NavigateUrl="~/Pages/Fault/Demonstration.aspx">Fault</asp:HyperLink></li>
                </ul>
            </li>
            <li>Services
                <ul>
                    <li>
                        <asp:HyperLink runat="server" ID="FaultService" NavigateUrl="~/Services/Fault.asmx">Fault</asp:HyperLink></li>
                </ul>
            </li>
        </ul>
    </div>
    </form>
</body>
</html>
