﻿using System;
using System.Web;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.Fault.DomainModel;

namespace FirstLook.Fault.WebApplication
{
    public class Global : HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {

            IRegistry registry = RegistryFactory.GetRegistry();

            registry.Register<Module>();

            registry.Register<IDataSessionManager, DataSessionManager>(ImplementationScope.Shared);

        }

        protected void Application_End(object sender, EventArgs e)
        {
            RegistryFactory.GetRegistry().Dispose();
        }

    }
}
