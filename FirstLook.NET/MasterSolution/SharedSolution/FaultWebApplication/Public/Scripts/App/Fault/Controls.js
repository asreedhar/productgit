﻿
if (typeof (Fault) !== "function") {
    var Fault = function () { };
}

(function () {

    var ns = this;
    
    // +-+-+-+-+-+-+
    // |e|v|e|n|t|s|
    // +-+-+-+-+-+-+

    var PublicEvents = {
        "Fault.Initialize": Initialize,
        "Fault.FaultEventLoaded": FaultEventLoaded,
        "Fault.ApplicationSummaryLoaded": ApplicationSummaryLoaded,
        "Fault.FaultSummaryLoaded": FaultSummaryLoaded,
        "Fault.FaultEventSummaryLoaded": FaultEventSummaryLoaded
    };

    $(Fault).bind(PublicEvents);
    
    function Initialize (evt, data) {
        // no (currently)
    }

    function FaultEventLoaded (evt, data) {
        var t = new FaultEventTemplate (data.FaultEvent);
        t.clean();
        t.build();
    }

    function ApplicationSummaryLoaded (evt, data) {
        var t = new ApplicationSummaryTemplate (data.Results);
        t.clean();
        t.build();
    }

    function FaultSummaryLoaded (evt, data) {
        var t = new FaultSummaryTemplate(data.Results);
        t.clean();
        t.build();
    }

    function FaultEventSummaryLoaded (evt, data) {
        var t = new FaultEventSummaryTemplate(data.FaultEventSummary);
        t.clean();
        t.build();
    }

    // +-+-+-+-+-+-+-+-+-+
    // |t|e|m|p|l|a|t|e|s|
    // +-+-+-+-+-+-+-+-+-+

    var ApplicationSummaryTemplate = function(data) {
        this.data = data || [];
        this.$dom = {
            Container: $('#sidebar')
        };
    };

    ApplicationSummaryTemplate.prototype = {
        build: function() {
            var t = $('<table />').append(
                $('<thead />').append(
                    $('<tr />').append(
                        $('<th />').text('Application'),
                        $('<th />').text('# Faults'),
                        $('<th />').text('# Events')
                    )
                ),
                $('<tbody/>').append($.proxy(function(index, html) {
                    var x = $('<tbody/>'), i, l, item;
                    for (i = 0, l = this.data.length; i < l; i++) {
                        item = this.data[i];
                        $(x).append(
                            $('<tr/>').append(
                                $('<td/>').append(
                                    $('<a />').attr('href', '#application').text(item.Application.Name)
                                ),
                                $('<td/>').text(item.NumberOfFaults),
                                $('<td/>').text(item.NumberOfFaultEvents)
                            )
                        );
                    }
                    return x.children();
                }, this))
            );
            this.$dom.Container.append(t);
            t.bind('click', $.proxy(this.selectApplication, this));
        },
        clean: function() {
            this.$dom.Container.unbind('click');
            this.$dom.Container.empty();
        },
        selectApplication: function(evt) {
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var i = tgt.href.lastIndexOf('#'),
                    n = tgt.href.substring(i);
                if (n == '#application') {
                    $(Fault).trigger("Fault.ApplicationChange", $(tgt).text());
                }
            }
            // kill the event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

    function asmxDate (date_string) {
        var date = new Date(parseInt(date_string.replace(/\/Date\((\d+)\)\//, "$1"), 10));
        return date.toDateString() + " " + date.toTimeString().substring(0,9);
    }

    var FaultSummaryTemplate = function(data) {
        this.data = data || [];
        this.$dom = {
            Container: $('#content')
        };
    };

    FaultSummaryTemplate.prototype = {
        build: function() {
            var t = $('<table />').append(
                $('<thead />').append(
                    $('<tr />').append(
                        $('<th />').text('Application'),
                        $('<th />').text('Min Fault Time'),
                        $('<th />').text('Max Fault Time'),
                        $('<th />').text('# Events'),
                        $('<th />').text('ExceptionType')
                    )
                ),
                $('<tbody/>').append($.proxy(function(index, html) {
                    var x = $('<tbody/>'), i, l, item;
                    for (i = 0, l = this.data.length; i < l; i++) {
                        item = this.data[i];
                        $(x).append(
                            $('<tr/>').append(
                                $('<td/>').text(item.Application.Name),
                                $('<td/>').text(asmxDate(item.MinFaultTime)),
                                $('<td/>').text(asmxDate(item.MaxFaultTime)),
                                $('<td/>').append(
                                    $('<a />').attr('href', '#' + item.FaultId + ',' + item.MaxFaultEventId).text(item.NumFaultEvent)
                                ),
                                $('<td/>').text(item.ExceptionType)
                            )
                        );
                    }
                    return x.children();
                }, this))
            );
            this.$dom.Container.append(t);
            t.bind('click', $.proxy(this.selectFaultEvent, this));
        },
        clean: function() {
            this.$dom.Container.unbind('click');
            this.$dom.Container.empty();
        },
        selectFaultEvent: function(evt) {
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var i = tgt.href.lastIndexOf('#'),
                    n = tgt.href.substring(i+1),
                    a = n.split(',');
                $(Fault).trigger("Fault.FaultEventChange", a);
            }
            // kill the event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

    var FaultEventSummaryTemplate = function(data) {
        this.data = data || [];
        this.$dom = {
            Container: $('#sidebar')
        };
    };

    FaultEventSummaryTemplate.prototype = {
        build: function() {
            var d = this.data;
            var p = $('<ul>').append(
                $('<li />').append(
                    $('<a />').attr('href', '#' + d.FaultId + ',1').text('First')
                ),
                $('<li />').append(
                    $('<a />').attr('href', '#' + d.FaultId + ',' + (d.Rank-1)).text('Previous')
                ),
                $('<li />').append(
                    $('<a />').attr('href', '#' + d.FaultId + ',' + (d.Rank+1)).text('Next')
                )
            );
            this.$dom.Container.append(p);
            var t = $('<table />').append(
                $('<tbody />').append(
                    $('<tr />').append(
                        $('<th />').text('Application'),
                        $('<th />').text(d.Application.Name)
                    ),
                    $('<tr />').append(
                        $('<th />').text('Machine'),
                        $('<th />').text(d.Machine.Name)
                    ),
                    $('<tr />').append(
                        $('<th />').text('Platform'),
                        $('<th />').text(d.Platform.Name)
                    ),
                    $('<tr />').append(
                        $('<th />').text('Message'),
                        $('<th />').text(d.Message)
                    )
                )
            );
            this.$dom.Container.append(t);
            p.bind('click', $.proxy(this.selectFaultEvent, this));
        },
        clean: function() {
            this.$dom.Container.unbind('click');
            this.$dom.Container.empty();
        },
        selectFaultEvent: function(evt) {
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var i = tgt.href.lastIndexOf('#'),
                    n = tgt.href.substring(i+1),
                    a = n.split(',');
                $(Fault).trigger("Fault.FaultEventChangePage", a);
            }
            // kill the event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

    var FaultEventTemplate = function(data) {
        this._data = data;
        this._stack = 0;
        this._stacks = [];
        this.$dom = {
            Container: $('#content')
        };
    };

    FaultEventTemplate.prototype = {
        build: function() {
            var ul = $('<ul/>').append(
                $('<li/>').append(
                    $('<a/>').attr('href', '#tab_1').text('Event'),
                    $('<a/>').attr('href', '#tab_2').text('Stack'),
                    $('<a/>').attr('href', '#tab_3').text('Data')
                )
            );
            var d1 = $('<div class="tab_1"/>').append(
                $('<fieldset/>').append(
                    $('<legend/>').text('Fault Event'),
                    this.FaultEvent()
                ),
                $('<fieldset/>').append(
                    $('<legend/>').text('Request Information'),
                    this.RequestInformation()
                )
            );
            var d2 = $('<div class="tab_2"/>').append(
                $('<fieldset/>').append(
                    $('<legend/>').text('Stacks'),
                    this.Stacks()
                ),
                $('<fieldset/>').append(
                    $('<legend/>').text('Stack Frames'),
                    this.StackFrames()
                )
            );
            var d3 = $('<div class="tab_3"/>').append(
                $('<fieldset/>').append(
                    $('<legend/>').text('Fault Event Data'),
                    this.FaultEventData()
                )
            );
            this.$dom.Container.append(ul, d1, d2, d3);
            this.$dom.Container.bind('click', $.proxy(this.click, this));
        },
        clean: function() {
            this.$dom.Container.unbind('click');
            this.$dom.Container.empty();
        },
        click: function(evt) {
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var i = tgt.href.lastIndexOf('#'),
                    n = tgt.href.substring(i),
                    a = n.split('_');
                if (n.indexOf('#stack_') == 0) {
                    this._stack = a[1];
                    this.$dom.Container.find('div[class="tab_2"] table').replaceWith(this.StackFrames());
                }
            }
            // kill the event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        },
        FaultEvent: function () {
            var d = this._data;
            return $('<table/>').append(
                $('<tbody />').append(
                    $('<tr/>').append(
                        $('<th/>').text('Id'),
                        $('<td/>').text(d.Id)
                    ),
                    $('<tr/>').append(
                        $('<th/>').text('Message'),
                        $('<td/>').text(d.Message)
                    ),
                    $('<tr/>').append(
                        $('<th/>').text('Fault Time'),
                        $('<td/>').text(asmxDate(d.FaultTime))
                    )
                )
            );
        },
        FaultEventData: function () {
            var d = this._data.FaultData, t, i, j;
            if (d == null || d.length == 0) {
                return $('<p/>').text('No Fault Event Data');
            }
            t = $('<tbody/>');
            for (i = 0; i < d.length; i++) {
                j = d[i];
                t.append(
                    $('<tr/>').append(
                        $('<td/>').text(j.Key),
                        $('<td/>').text(j.Value)
                    )
                );
            }
            return $('<table/>').append(
                $('<thead/>').append(
                    $('<tr/>').append(
                        $('<th/>').text('Key'),
                        $('<th/>').text('Value')
                    )
                ),
                t
            );
        },
        RequestInformation: function () {
            var d = this._data.RequestInformation;
            if (d == null) {
                return $('<p/>').text('No Request Information');
            }
            return $('<table/>').append(
                $('<tbody />').append(
                    $('<tr/>').append(
                        $('<th/>').text('Path'),
                        $('<td/>').text(d.Path)
                    ),
                    $('<tr/>').append(
                        $('<th/>').text('Url'),
                        $('<td/>').text(d.Url)
                    ),
                    $('<tr/>').append(
                        $('<th/>').text('User'),
                        $('<td/>').text(d.User)
                    ),
                    $('<tr/>').append(
                        $('<th/>').text('User Host Address'),
                        $('<td/>').text(d.UserHostAddress)
                    )
                )
            );
        },
        Stacks: function () {
            var s = [],
                f = this._data.Fault,
                d = $('<ul/>');
            // get the stacks
            while (f != null) {
                if (f.Stack != null) {
                    s.push(f.Stack);
                }
                else {
                    console.log('what the hell!');
                }
                f = f.Cause;
            }
            // build the list
            d.append(
                $('<li/>').append($('<em/>').text('-TOP-'))
            );
            for (i = 0; i < s.length; i++) {
                d.append(
                    $('<li/>').append(
                        $('<a/>').attr('href', '#stack_' + i).text(i)
                    )
                );
            }
            d.append(
                $('<li/>').append($('<em/>').text('-BOTTOM-'))
            );
            // stuff the data
            this._stack = 0;
            this._stacks = s;
            // return
            return d;
        },
        StackFrames: function () {
            var l = this._stacks,
                i = this._stack,
                s = l[i],
                f = s.StackFrames,
                j, k,
                tbody = $('<tbody/>');
            for (j = 0; j < f.length; j++) {
                k = f[j];
                tbody.append(
                    $('<tr/>').append(
                        $('<td/>').text(k.ClassName),
                        $('<td/>').text(k.FileName),
                        $('<td/>').text(k.IsNativeMethod),
                        $('<td/>').text(k.IsUnknownSource),
                        $('<td/>').text(k.LineNumber),
                        $('<td/>').text(k.MethodName)
                    )
                );
            }
            return $('<table/>').append(
                $('<thead/>').append(
                    $('<tr />').append(
                        $('<th/>').text('Class Name'),
                        $('<th/>').text('File Name'),
                        $('<th/>').text('Is Native Method'),
                        $('<th/>').text('Is Unknown Source'),
                        $('<th/>').text('Line Number'),
                        $('<th/>').text('Method Name')
                    )
                ),
                tbody
            );
        }
    };

}).apply(Fault);
