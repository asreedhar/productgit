﻿
if (typeof (Fault) !== "function") {
    var Fault = function () { };
}

(function () {

    var ns = this;

    // +-+-+-+-+-+-+-+
    // |c|l|a|s|s|e|s|
    // +-+-+-+-+-+-+-+

    function ApplicationDto() {
        this.Name = '';
    }

    function MachineDto() {
        this.Name = '';
    }

    function PlatformDto() {
        this.Name = '';
    }

    function FaultEventDto() {
        this.Application = null;
        this.Machine = null;
        this.Platform = null;
        this.Id = 0;
        this.FaultTime = null;
        this.Message = '';
        this.Fault = null;
        this.FaultData = [];
        this.RequestInformation = null;
    }

    function RequestInformationDto() {
        this.Path = '';
        this.Url = '';
        this.User = '';
        this.UserHostAddress = '';
    }

    function FaultDataDto() {
        this.Key = '';
        this.Value = '';
    }

    function FaultDto() {
        this.ExceptionType = '';
        this.Cause = null;
        this.Stack = null;
    }

    function StackDto() {
        this.StackFrames = [];
    }

    function StackFrameDto() {
        this.ClassName = '';
        this.FileName = '';
        this.MethodName = '';
        this.IsNativeMethod = '';
        this.IsUnknownMethod = '';
        this.LineNumber = 0;
    }

    // reporting

    function ApplicationSummaryDto()  {
        this.Application = null;
        this.NumberOfFaults = 0;
        this.NumberOfFaultEvents = 0;
    }

    function FaultSummaryDto()  {
        this.Application = null;
        this.ExceptionType = '';
        this.FaultId = 0;
        this.MinFaultTime = null;
        this.MaxFaultTime = null;
        this.MaxFaultEventId = 0;
        this.NumFaultEvent = '';
    }

    function FaultEventSummaryDto()  {
        this.Application = null;
        this.Machine = null;
        this.Platform = null;
        this.Id = 0;
        this.FaultId = 0;
        this.Rank = 0;
        this.Message = '';
    }

    // +-+-+-+-+-+-+-+-+-+
    // |e|n|v|e|l|o|p|e|s|
    // +-+-+-+-+-+-+-+-+-+

    function FetchFaultEventArgumentsDto() {
        this.FaultEventId = 0;
        EventBinder(this);
    }

    function FetchFaultEventResultsDto() {
        this.Arguments = null;
        this.FaultEvent = null;
    }

    // reporting

    function ApplicationSummaryArgumentsDto() {
        EventBinder(this);
    }

    function ApplicationSummaryResultsDto() {
        this.Arguments = null;
        this.Results = [];
    }

    function FaultSummaryArgumentsDto() {
        this.ApplicationName = '';
        this.SortColumns = [];
        this.MaximumRows = 50;
        this.StartRowIndex = 0;
        EventBinder(this);
    }

    function FaultSummaryResultsDto() {
        this.Arguments = null;
        this.Results = [];
    }

    function FaultEventSummaryArgumentsDto() {
        this.FaultId = 0;
        this.HasFaultEventId = false;
        this.FaultEventId = 0;
        this.HasFaultRank = false;
        this.FaultRank = 0;
        EventBinder(this);
    }

    function FaultEventSummaryResultsDto() {
        this.Arguments = null;
        this.FaultEventSummary = null;
    }

    /* == END: Classes == */

    function genericMapper(type, config) {
        if (typeof config != "object") {
            config = {};
        }
        return function (json) {
            var result = new type(),
            prop,
            subJson,
            idx;
            for (prop in json) {
                if (!json.hasOwnProperty(prop)) continue; /// Ensure actual property
                subJson = json[prop];
                if (config.hasOwnProperty(prop)) {
                    if (subJson !== null && typeof subJson.length == "number" && typeof result[prop].push == "function") {
                        for (idx in subJson) {
                            if (!subJson.hasOwnProperty(idx)) continue;
                            result[prop].push(DataMapper[config[prop]](subJson[idx]));
                        }
                    } else {
                        result[prop] = DataMapper[config[prop]](subJson);
                    }
                } else if (typeof result[prop] !== "undefined") { // don't overwrite properties
                    result[prop] = subJson;
                }
            }
            return result;
        };
    }

    var DataMapper = {
        "FetchFaultEventArgumentsDto": genericMapper(FetchFaultEventArgumentsDto),
        "FetchFaultEventResultsDto": genericMapper(FetchFaultEventResultsDto, {
            "Arguments": "FetchFaultEventArgumentsDto",
            "FaultEvent": "FaultEventDto"
        }),
        "ApplicationSummaryArgumentsDto": genericMapper(ApplicationSummaryArgumentsDto),
        "ApplicationSummaryResultsDto": genericMapper(ApplicationSummaryResultsDto, {
            "Arguments": "ApplicationSummaryArgumentsDto",
            "Results": "ApplicationSummaryDto"
        }),
        "FaultSummaryArgumentsDto": genericMapper(FaultSummaryArgumentsDto),
        "FaultSummaryResultsDto": genericMapper(FaultSummaryResultsDto, {
            "Arguments": "FaultSummaryArgumentsDto",
            "Results": "FaultSummaryDto"
        }),
        "FaultEventSummaryArgumentsDto": genericMapper(FaultEventSummaryArgumentsDto),
        "FaultEventSummaryResultsDto": genericMapper(FaultEventSummaryResultsDto, {
            "Arguments": "FaultEventSummaryArgumentsDto",
            "FaultEventSummary": "FaultEventSummaryDto"
        }),
        "ApplicationDto": genericMapper(ApplicationDto),
        "MachineDto": genericMapper(MachineDto),
        "PlatformDto": genericMapper(PlatformDto),
        "FaultEventDto": genericMapper(FaultEventDto, {
            "Application": "ApplicationDto",
            "Machine": "MachineDto",
            "Platform": "PlatformDto",
            "Fault": "FaultDto",
            "FaultData": "FaultDataDto",
            "RequestInformation": "RequestInformationDto"
        }),
        "FaultDataDto": genericMapper(FaultDataDto),
        "RequestInformationDto": genericMapper(RequestInformationDto),
        "FaultDto": genericMapper(FaultDto, {
            "Cause": "FaultDto",
            "Stack": "StackDto"
        }),
        "StackDto": genericMapper(StackDto, {
            "StackFrames": "StackFrameDto"
        }),
        "StackFrameDto": genericMapper(StackFrameDto),
        "ApplicationSummaryDto": genericMapper(ApplicationSummaryDto, {
            "Application": "ApplicationDto"
        }),
        "FaultSummaryDto": genericMapper(FaultSummaryDto, {
            "Application": "ApplicationDto"
        }),
        "FaultEventSummaryDto": genericMapper(FaultEventSummaryDto, {
            "Application": "ApplicationDto",
            "Machine": "MachineDto",
            "Platform": "PlatformDto"
        }),
    };

    function genericService(serviceUrl, resultType, getDataFunc) {
        return {
            AjaxSetup: {
                "url": serviceUrl
            },
            fetch: function () {
                var me = this;
                var ajaxSetup = $.extend(Services.Default.AjaxSetup, this.AjaxSetup);
                ajaxSetup.data = this.getData();
                function onFetchSuccess(json) {
                    var results = resultType.fromJSON(json.d);
                    $(me).trigger("fetchComplete", [results]);
                }
                ajaxSetup.success = onFetchSuccess;
                $.ajax(ajaxSetup);
            },
            getData: getDataFunc
        };
    }

    var Services = {
        "Default": {
            AjaxSetup: {
                type: "POST",
                dataType: "json",
                processData: false,
                contentType: "application/json; charset=utf-8",
                error: function (xhr, status, errorThrown) {
                    if (xhr.status === 500) {
                        $(Fault).trigger("Fault.ErrorState", { errorType: errorThrown, errorResponse: xhr });
                    } else if (xhr.status === 401) {
                        var response = JSON.parse(xhr.responseText);
                        if (!!response['auth-login']) {
                            window.location.assign(response['auth-login']);
                        }
                    }
                }
            }
        },
        "FetchFaultEvent": genericService("/Fault/Services/Fault.asmx/FetchFaultEvent", FetchFaultEventResultsDto, function () {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes.FetchFaultEventArgumentsDto",
                    "FaultEventId": this.FaultEventId
                }
            });
        }),
        "ApplicationSummary": genericService("/Fault/Services/Fault.asmx/ApplicationSummary", ApplicationSummaryResultsDto, function () {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes.Reporting.ApplicationSummaryArgumentsDto"
                }
            });
        }),
        "FaultSummary": genericService("/Fault/Services/Fault.asmx/FaultSummary", FaultSummaryResultsDto, function () {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes.Reporting.FaultSummaryArgumentsDto",
                    "ApplicationName": this.ApplicationName,
                    "SortColumns": this.SortColumns,
                    "StartRowIndex": this.StartRowIndex,
                    "MaximumRows": this.MaximumRows
                }
            });
        }),
        "FaultEventSummary": genericService("/Fault/Services/Fault.asmx/FaultEventSummary", FaultEventSummaryResultsDto, function () {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes.Reporting.FaultEventSummaryArgumentsDto",
                    "FaultId": this.FaultId,
                    "HasFaultEventId": (this.FaultEventId == 0 ? false : true),
                    "FaultEventId": this.FaultEventId,
                    "HasFaultRank": (this.FaultRank == 0 ? false : true),
                    "FaultRank": this.FaultRank
                }
            });
        })
    };

    var Events = {
        "FetchFaultEventArgumentsDto": {
            fetchComplete: function (event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(Fault).trigger("Fault.FaultEventLoaded", data);
            },
            stateChange: function () {
                $(this).trigger('fetch');
            }
        },
        "ApplicationSummaryArgumentsDto": {
            fetchComplete: function (event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(Fault).trigger("Fault.ApplicationSummaryLoaded", data);
            }
        },
        "FaultSummaryArgumentsDto": {
            fetchComplete: function (event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(Fault).trigger("Fault.FaultSummaryLoaded", data);
            },
            stateChange: function () {
                $(this).trigger('fetch');
            }
        },
        "FaultEventSummaryArgumentsDto": {
            fetchComplete: function (event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(Fault).trigger("Fault.FaultEventSummaryLoaded", data);
            },
            stateChange: function () {
                $(this).trigger('fetch');
            }
        }
    };

    var EventBinder = function (obj) {
        var type = EventBinder.getType(obj);
        if (!!Events[type]) {
            var events = Events[type];
            for (var e in events) {
                if (!events.hasOwnProperty(e)) continue;
                $(obj).bind(e, events[e]); // TODO: events need to be framework independent
            }
        }
    };
    EventBinder.map = {
        'FetchFaultEventArgumentsDto': FetchFaultEventArgumentsDto,
        'ApplicationSummaryArgumentsDto': ApplicationSummaryArgumentsDto,
        'FaultSummaryArgumentsDto': FaultSummaryArgumentsDto,
        'FaultEventSummaryArgumentsDto': FaultEventSummaryArgumentsDto
    };
    EventBinder.getType = function (obj) {
        for (var type in EventBinder.map) {
            if (!EventBinder.map.hasOwnProperty(type)) continue;
            if (obj instanceof EventBinder.map[type]) {
                return type;
            }
        }
        return undefined;
    };

    // ====================
    // = Bind DataMappers =
    // ====================
    (function () {
        function BindDataMapper(target, source) {
            target.fromJSON = source;
        }

        BindDataMapper(FetchFaultEventArgumentsDto, DataMapper.FetchFaultEventArgumentsDto);
        BindDataMapper(FetchFaultEventResultsDto, DataMapper.FetchFaultEventResultsDto);

        BindDataMapper(ApplicationSummaryArgumentsDto, DataMapper.ApplicationSummaryArgumentsDto);
        BindDataMapper(ApplicationSummaryResultsDto, DataMapper.ApplicationSummaryResultsDto);

        BindDataMapper(FaultSummaryArgumentsDto, DataMapper.FaultSummaryArgumentsDto);
        BindDataMapper(FaultSummaryResultsDto, DataMapper.FaultSummaryResultsDto);

        BindDataMapper(FaultEventSummaryArgumentsDto, DataMapper.FaultEventSummaryArgumentsDto);
        BindDataMapper(FaultEventSummaryResultsDto, DataMapper.FaultEventSummaryResultsDto);

        BindDataMapper(ApplicationDto, DataMapper.ApplicationDto);
        BindDataMapper(MachineDto, DataMapper.MachineDto);
        BindDataMapper(PlatformDto, DataMapper.PlatformDto);
        BindDataMapper(FaultEventDto, DataMapper.FaultEventDto);
        BindDataMapper(FaultDataDto, DataMapper.FaultDataDto);
        BindDataMapper(RequestInformationDto, DataMapper.RequestInformationDto);
        BindDataMapper(FaultDto, DataMapper.FaultDto);
        BindDataMapper(StackDto, DataMapper.StackDto);
        BindDataMapper(StackFrameDto, DataMapper.StackFrameDto);

        BindDataMapper(ApplicationSummaryDto, DataMapper.ApplicationSummaryDto);
        BindDataMapper(FaultSummaryDto, DataMapper.FaultSummaryDto);
        BindDataMapper(FaultEventSummaryDto, DataMapper.FaultEventSummaryDto);

    })();

    // =======================
    // = Bind Service Fetchs =
    // =======================
    (function () {
        function BindFetch(target, source) {
            for (var m in source) {
                target.prototype[m] = source[m];
            }
            $(target).bind("fetch", target.prototype.fetch); // TODO: events need to be framework independent
        }

        BindFetch(FetchFaultEventArgumentsDto, Services.FetchFaultEvent);
        
        BindFetch(ApplicationSummaryArgumentsDto, Services.ApplicationSummary);
        
        BindFetch(FaultSummaryArgumentsDto, Services.FaultSummary);
        
        BindFetch(FaultEventSummaryArgumentsDto, Services.FaultEventSummary);
        
    })();

    /* ================== */
    /* = PUBLIC METHODS = */
    /* ================== */
    if (typeof this.prototype === "undefined") {
        this.prototype = {};
    }
    this.prototype.main = function (selected) {
        if (typeof selected === "undefined") {
            selected = {};
        }
        // TODO: events need to be framework independent
        $(new ApplicationSummaryArgumentsDto()).trigger("fetch"); 
        $(new FaultSummaryArgumentsDto()).trigger("fetch");
    };

    this.raises = this.raises || [];
    this.raises.push("Fault.FaultEventLoaded");
    this.raises.push("Fault.ApplicationSummaryLoaded");
    this.raises.push("Fault.FaultSummaryLoaded");
    this.raises.push("Fault.FaultEventSummaryLoaded");
    this.raises.push("Fault.ErrorState");

    this.listensTo = this.listensTo || [];
    this.listensTo.push("Fault.FaultEvent");
    this.listensTo.push("Fault.FaultEventChange");
    this.listensTo.push("Fault.FaultEventChangePage");

    var PublicEvents = {
        "Fault.ApplicationChange": function (evt, applicationName) {
            var args = new FaultSummaryArgumentsDto();
            args.ApplicationName = applicationName;
            $(args).trigger("stateChange"); // TODO: events need to be framework independent
        },
        "Fault.FaultEventChange": function (evt, faultId, faultEventId) {
            // event
            var args1 = new FetchFaultEventArgumentsDto();
            args1.FaultEventId = faultEventId;
            $(args1).trigger("stateChange"); // TODO: events need to be framework independent
            // and summary
            var args2 = new FaultEventSummaryArgumentsDto();
            args2.FaultId = faultId;
            args2.HasFaultEventId = true;
            args2.FaultEventId = faultEventId;
            args2.HasFaultRank = false;
            args2.FaultRank = 0;
            $(args2).trigger("stateChange"); // TODO: events need to be framework independent
        },
        "Fault.FaultEventChangePage": function (evt, faultId, faultRank) {
            var args = new FaultEventSummaryArgumentsDto();
            args.FaultId = faultId;
            args.HasFaultEventId = false;
            args.FaultEventId = 0;
            args.HasFaultRank = true;
            args.FaultRank = faultRank;
            $(args).trigger("stateChange"); // TODO: events need to be framework independent
        }
    };

    $(Fault).bind(PublicEvents);

    /* ========================== */
    /* = PUBLIC STATIC METHODS = */
    /* ========================== */
    // none
    
}).apply(Fault);
