﻿<%@ WebHandler Language="C#" Class="BackgroundImageHandler" %>

using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;

public class BackgroundImageHandler : IHttpHandler {
    
    private string _buid;
    
    public void ProcessRequest (HttpContext context)
    {
        try{
            HttpRequest request = context.Request;
            HttpResponse response = context.Response;

            NameValueCollection qs = request.QueryString;

            // get business unit id
            _buid = request["bu"];

            string fileName = qs["img"];
            string filePath = string.Concat(GetDocumentRoot(), _buid, @"\", fileName);

            response.Clear();
            response.ContentType = GetContentType(request.QueryString);
            response.WriteFile(filePath);
            response.End();
        }
        catch(Exception exception)
        {
            throw new Exception(exception.Message);
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    private static string GetContentType(NameValueCollection qs)
    {
        var extension = qs["img"].Substring(qs["img"].LastIndexOf("."), qs["img"].Length - qs["img"].LastIndexOf(".")) ?? string.Empty;

        switch (extension.ToLower())
        {
            case ".pdf":
                return "application/pdf";

            case ".bmp":
                return "image/bmp";

            case ".gif":
                return "image/gif";

            case ".jpg":
                return "image/jpeg";

            case ".png":
                return "image/png";

            case ".tif":
                return "image/tiff";

            default:
                throw new Exception("Unsupported file extension: '" + extension + "'");

        }
    }

    private static string GetDocumentRoot()
    {
        // Get document root from config and verify trailing slash
        string docRoot = ConfigurationManager.AppSettings["MerchandisingDocumentRoot"];
        docRoot = docRoot.EndsWith(@"\") ? docRoot : docRoot + @"\";
        docRoot += @"WindowStickers\";

        return docRoot;
    }

}