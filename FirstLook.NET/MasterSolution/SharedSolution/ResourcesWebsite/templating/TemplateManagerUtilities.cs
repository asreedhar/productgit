﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Web.UI.HtmlControls;


/// <summary>
/// Summary description for TemplateManagerUtilities
/// </summary>
public class TemplateManagerUtilities
{

    public static string GetContentType(NameValueCollection qs)
    {
        var extension = qs["img"].Substring(qs["img"].LastIndexOf("."), qs["img"].Length - qs["img"].LastIndexOf(".")) ?? string.Empty;

        switch (extension.ToLower())
        {
            case ".pdf":
                return "application/pdf";

            case ".bmp":
                return "image/bmp";

            case ".gif":
                return "image/gif";

            case ".jpg":
                return "image/jpeg";

            case ".png":
                return "image/png";

            case ".tif":
                return "image/tiff";

            default:
                throw new Exception("Unsupported file extension: '" + extension + "'");

        }
    }

    public static string GetDocumentRoot()
    {
        // Get document root from config and verify trailing slash
        string docRoot = ConfigurationManager.AppSettings["MerchandisingDocumentRoot"];
        docRoot = docRoot.EndsWith(@"\") ? docRoot : docRoot + @"\";
        docRoot += @"WindowStickers\";

        return docRoot;
    }

    // Returns file extension based on content type
    public static string GetFileExtension(HtmlInputFile file)
    {
        var extension = string.Empty;

        switch (file.PostedFile.ContentType.ToLower())
        {
            case "image/bmp":
                extension = "bmp";
                break;

            case "image/gif":
                extension = "gif";
                break;

            case "image/jpeg":
                extension = "jpg";
                break;

            case "image/pjpeg":
                extension = "jpg";
                break;

            case "image/png":
                extension = "png";
                break;

            case "image/ppng":
                extension = "png";
                break;

            //case "application/pdf":
            //    extension = "pdf";
            //    break;

            //case "image/tiff":
            //    extension = "tif";
            //    break;
            default:
                break;

        }

        return extension;
    }

    public static Dictionary<string, string> UiMessages = new Dictionary<string, string>()
            {
                { "unsupportedImageType", "Unsupported image format. Must be either BMP, GIF, JPG or PNG." }
            };

}
