
(function($){$.extend($.ui.tabs.prototype,{rotation:null,rotate:function(ms){var self=this;function stop(e){if(e.clientX){clearInterval(self.rotation);}}
if(ms){var t=this.options.selected;this.rotation=setInterval(function(){t=++t<self.$tabs.length?t:0;self.click(t);},ms);this.$tabs.bind(this.options.event,stop);}
else{clearInterval(this.rotation);this.$tabs.unbind(this.options.event,stop);}}});})(jQuery);