/* Author: Paul Monson

*/
(function($) {
    function onstart(evt) {
        var target = $(this);

        target.bind("touchmove mousemove", onmove);
        target.data({
            "touchstarted": true, 
            "el": this,
            "touchtimer": setTimeout(function() {
                tryfiretap(target);
            }, 100)
        });
    }
    function onend(evt) {
        var target = $(this);
        if (target.data("touchstarted") === true) {
            evt.preventDefault();
        }
        tryfiretap(target);
    }
    function onmove(evt) {
        var target = $(this);
        removetraces(target);
    }
    function tryfiretap(target) {
        if (target.data("touchstarted") === true && target.data("el") === target.get(0)) {
            target.trigger("tap");
        }
        removetraces(target);
    }
    function removetraces(target) {
        target.unbind("touchmove mousemove", onmove);
        clearTimeout( target.data("touchtimer") );
        target.data({"touchstarted": null, "el": null});
    }
    function clickfallback() {
        $(this).trigger("tap");
    }
    $.event.special.tap = {
        setup: function(eventData, namespace) {
            if (_.isAndroid()) {
                $(this).bind("click", clickfallback)
            } else {
                $(this).bind("touchstart mousedown", onstart).bind("touchend mouseup", onend);
            }
        },
        teardown: function(namespace) {
            var target = $(this);
            if (_.isAndroid()) {
                $(this).unbind("click", clickfallback);
            } else {
                target.unbind("touchstart mousedown", onstart).unbind("touchend mouseup", onend);

                removetraces(target);
            }

            return;
        }
    }
})( jQuery );

(function() {
    /// reduce helpers
    _.mixin({
        group_by: function(collection, prop) {
            return _.reduce(collection, function(group, item) {
                var value = prop.indexOf(".") === - 1 ? item[prop] : _.access(item, prop);
                (group[value] = group[value] || []).push(item);

                return group;
            },
            {});
        },
        join: function(collection, prop, other_collection) {
            var other_map = _.group_by(other_collection, prop);
            return _.reduce(collection, function(results, item) {
                var value = item[prop];
                _.each(other_map[value], function(other_item) {
                    results.push(_.extend({},
                    item, other_item));
                },
                []);
                return results;
            });
        },
        missing: function(array, values) {
            return _.filter(array, function(value) {
                return ! _.include(values, value);
            });
        },
        access: function(item, prop) {
            var path = prop.split(".");
            if (path.length > 1) {
                return _.access(item[_.head(path)], _.tail(path).join("."));
            }
            return item[_.head(path)];
        }
    });

    /// validation helpers
    _.mixin({
        hasTouch: function() {
            return "ontouchstart" in window;
        },
        isVin: function(text) {
            if (text === null || text === undefined) {
                return false;
            }
            if (text.length != 17) {
                return false;
            }
            text = text.toUpperCase();
            if (/^[ABCDEFGHJKLMNPRSTUVWXYZ0123456789]{17}$/.test(text) === false) {
                return false;
            }
            var factors = [8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2];
            var char_map = {
                "A": 1,
                "B": 2,
                "C": 3,
                "D": 4,
                "E": 5,
                "F": 6,
                "G": 7,
                "H": 8,
                "J": 1,
                "K": 2,
                "L": 3,
                "M": 4,
                "N": 5,
                "P": 7,
                "R": 9,
                "S": 2,
                "T": 3,
                "U": 4,
                "V": 5,
                "W": 6,
                "X": 7,
                "Y": 8,
                "Z": 9
            };

            var total = 0;
            for (var i = 0; i < 17; i++) {
                var v = text.charCodeAt(i),
                f = factors[i];
                v = char_map[text[i]] || parseInt(text[i], 10);

                total += (v * f);
            }
            var remainder = total % 11;
            if (remainder == 10) {
                remainder = 'X';
            }
            else {
                remainder = remainder.toString();
            }
            return text[8] == remainder;
        },
        isPlatform: _.memoize(function(pattern) {
            return !! pattern.test(navigator.appVersion);
        }),
        isAndroid: function() {
            return _.isPlatform(/android/gi);
        },
        isIThing: function() {
            return _.isPlatform(/ipad|iphone/gi);
        }
    });

    /// string helpers
    _.mixin({
        concat: function(arr) {
            if (_.isArray(arr)) {
                return arr.join("");
            }
            return arr;
        },
        toPx: function(val) {
            return _.concat(val, "px");
        },
        toPct: function(val) {
            return _.concat(val, "pct");
        },
        comma: function(value) {
            value = value.toString();
            var regex = /(\d+)(\d{3})/;
            while (regex.test(value)) {
                value = value.replace(regex, '$1,$2');
            }
            return value;
        },
        money: function(value) {
            if (value === 0) {
                return "-0-";
            }
            return "$" + _.comma(Math.round(value));
        },
        moneyAdjusted: function(value) {
            var indicator = value === 0 ? "": value > 0 ? "+": "-";
            return indicator + _.money(Math.abs(value));
        },
        moneyToK: function(value) {
            return "$" + _.roundToK(value);
        },
        roundToK: function(value) {
            if (value > 999) {
                return Math.round(value / 1e3) + "k";
            } else {
                return value;
            }
        },
        shortVin: function(value) {
            return value[0] + "_" + value[9] + "_" + value.slice(11, 17);
        },
        asmxDate: function(date_string) {
            var date = new Date(parseInt(date_string.replace(/\/Date\((\d+)\)\//, "$1"), 10));
            return date.toDateString();
        }
    });

    /// Array Helpers
    _.mixin({
        usStates: function() {
            return [{
                name: "Alabama",
                value: "AL"
            },
            {
                name: "Alaska",
                value: "AK"
            },
            {
                name: "Arizona",
                value: "AZ"
            },
            {
                name: "Arkansas",
                value: "AR"
            },
            {
                name: "California",
                value: "CA"
            },
            {
                name: "Colorado",
                value: "CO"
            },
            {
                name: "Connecticut",
                value: "CT"
            },
            {
                name: "Delaware",
                value: "DE"
            },
            {
                name: "Florida",
                value: "FL"
            },
            {
                name: "Georgia",
                value: "GA"
            },
            {
                name: "Hawaii",
                value: "HI"
            },
            {
                name: "Idaho",
                value: "ID"
            },
            {
                name: "Illinois",
                value: "IL"
            },
            {
                name: "Indiana",
                value: "IN"
            },
            {
                name: "Iowa",
                value: "IA"
            },
            {
                name: "Kansas",
                value: "KS"
            },
            {
                name: "Kentucky",
                value: "KY"
            },
            {
                name: "Louisiana",
                value: "LA"
            },
            {
                name: "Maine",
                value: "ME"
            },
            {
                name: "Maryland",
                value: "MD"
            },
            {
                name: "Massachusetts",
                value: "MA"
            },
            {
                name: "Michigan",
                value: "MI"
            },
            {
                name: "Minnesota",
                value: "MN"
            },
            {
                name: "Mississippi",
                value: "MS"
            },
            {
                name: "Missouri",
                value: "MO"
            },
            {
                name: "Montana",
                value: "MT"
            },
            {
                name: "Nebraska",
                value: "NE"
            },
            {
                name: "Nevada",
                value: "NV"
            },
            {
                name: "New Hampshire",
                value: "NH"
            },
            {
                name: "New Jersey",
                value: "NJ"
            },
            {
                name: "New Mexico",
                value: "NM"
            },
            {
                name: "New York",
                value: "NY"
            },
            {
                name: "North Carolina",
                value: "NC"
            },
            {
                name: "North Dakota",
                value: "ND"
            },
            {
                name: "Ohio",
                value: "OH"
            },
            {
                name: "Oklahoma",
                value: "OK"
            },
            {
                name: "Oregon",
                value: "OR"
            },
            {
                name: "Pennsylvania",
                value: "PA"
            },
            {
                name: "Rhode Island",
                value: "RI"
            },
            {
                name: "South Carolina",
                value: "SC"
            },
            {
                name: "South Dakota",
                value: "SD"
            },
            {
                name: "Tennessee",
                value: "TN"
            },
            {
                name: "Texas",
                value: "TX"
            },
            {
                name: "Utah",
                value: "UT"
            },
            {
                name: "Vermont",
                value: "VT"
            },
            {
                name: "Virginia",
                value: "VA"
            },
            {
                name: "Washington",
                value: "WA"
            },
            {
                name: "West Virginia",
                value: "WV"
            },
            {
                name: "Wisconsin",
                value: "WI"
            },
            {
                name: "Wyoming",
                value: "WY"
            }];
        },
        require: function(args, props) {
            /// Should be updated to work with nonaurgument objects, PM
            var test_obj = args;
            if (_.isUndefined(args)) throw "Requres Arugments";
            if (_.isArguments(args)) test_obj = args[0];
            if ( !! props && typeof props.length == "number") {
                _.each(props, function(prop) {
                    if (_.isUndefined(test_obj[prop])) throw _.concat(["Requires: ", prop]);
                });
            }
            return true;
        }
    });

    /// Event Helpers
    _.mixin({
        preventDefault: function(evt) {
            evt.preventDefault();
        }
    });

    /// Pattern Helper... should probably be protected and not in this space, PM
    function where(value, key) {
        var state, return_value = true;
        var isObject = ! (_.isString(value) || _.isNumber(value) || _.isBoolean(value) || _.isDate(value) || _.isNaN(value) || _.isFunction(value) || _.isArray(value));
        if (_.isString(key)) {
            state = this[key];
            if (value === _) {
                return_value = ! _.isUndefined(state);
            } else if (_.isFunction(value)) {
                return_value = value(state);
            } else if (_.isUndefined(state)) {
                return_value = false;
            } else if (_.isArray(value) && value.length > 0) {
                if (state.length > 0) return_value = _.all(value[0], where, state[0]);
            } else {
                return_value = _.isEqual(value, state);
            }
        }
        if (isObject) return_value = _.all(value, where, state);
        return return_value;
    }

    /// Template Code, PM
    var mainScroller = {};
    var DOM = {};
    function getDOM() {
        DOM = {
            header: $("#header"),
            main_top: $("#main_top"),
            footer: $("#footer"),
            main: $("#main"),
            scrollable: $("#scrollable"),
            container: $("#container")
        };
    }
    function resetScroll() {
        window.scrollTo(0, 0);
        if (mainScroller.reset) mainScroller.reset();
    }
    function setHeight() {
        var header_height, footer_height, wrapper_height, window_height, window_width, adj;
        header_height = DOM.header.height() + DOM.main_top.height();
        footer_height = DOM.footer.height();
        window_height = window.innerHeight;
        window_width = window.innerWidth;
        adj = _.isIThing() ? - 20: 0;
        wrapper_height = window_height - header_height - footer_height + adj;

        if (!_.isAndroid()) DOM.main.css("height", _.toPx(wrapper_height));
    }

    function applyJsScroll() {
        setHeight();
        var setScrollAndHeight = _.compose(resetScroll, function() {
            _.defer(setHeight);
        });

        document.body.addEventListener("touchMove", _.preventDefault, true);
        window.onscroll = _.throttle(function() {
            window.scrollTo(0,0);
            return false;
        }, 100);

        setScrollAndHeight();
        DOM.main.addClass("scrolling");
        mainScroller = new Dragdealer('main', {
            horizontal: false,
            vertical: true,
            speed: 50,
            slide: true,
            slideSpeed: 5,
            loose: true,
            tapToLocation: false
        });
    }

    function applySensibleScroller() {
        var basic_fixed = {
            "position": "fixed",
            "left": "0",
            "right": "0",
            "z-index": "100"
        };
        DOM.container.css({
            "padding-top": _.toPx(DOM.header.height())
        });
        DOM.header.css($.extend({
            "top": "0"
        },
        basic_fixed));
        DOM.footer.css($.extend({
            "bottom": "0"
        },
        basic_fixed));
    }

    /// Apply Behavour for Carousel
    function applyCarousel(selector, items_selector) {
        var el = $(selector),
        items = $(items_selector, el),
        s = items.size();

        var scroller = new Dragdealer(el.get(0), {
            steps: s,
            speed: 50,
            slide: true,
            slideSpeed: 4,
            loose: true,
            tapToLocation: false
        });

        scroller.onrebuild = function() {
            var handle = $('.handle', el),
            h = items.first().height(),
            w = items.first().width(),
            hpx = _.toPx(h),
            wpx = _.toPx(w);

            handle.css({
                "width": _.toPx(s * w),
                "height": hpx
            });

            el.css({
                "width": wpx
            });

            items.css({
                "width": wpx,
                "height": hpx
            });

            scroller.refresh();
        }

        _.defer(scroller.onrebuild);

        return scroller;
    }

    //// Component Templates
    var Templates = (function() {
        function func(args, body) {
            return _.concat(["function(", args, "){", body, "}"]);
        }
        function inner(args, body) {
            return func(args, escape_outer(body));
        }
        function escape_outer(body) {
            return _.concat(["%>", body, "<%"]);
        }
        function make_attr(attr_name, value) {
            return value ? _.concat([" ", attr_name, "='", value, "'"]) : "";
        }
        function make_idx_attr(attr_name, value, idxer) {
            return value ? _.concat([" ", attr_name, "='<%=", value, "[", idxer, "]%>'"]) : "";
        }
        function make_each(items, item, each) {
            return _.concat(["<% _.each(", items, ",", inner(item, each), "); %>"]);
        }

        var tmpl = {};

        tmpl.generateGroupedSelectList = function(config, as_string) {
            _.require(arguments, ["data_accessor", "text_accessor", "item_accessor"]);

            var list_class = make_attr("class", config.class_name),
            list_items,
            groups,
            list_tmp,
            text_tmp = _([config.text_accessor]).chain().flatten().reduce(function(memo, acc) {
                return _.concat([memo, "<span class='", acc.replace(".", "-"), "'><%= item.", acc, " %></span> "]);
            },
            "").value(),
            data_tmp = _([config.data_accessor]).chain().flatten().reduce(function(memo, acc) {
                return _.concat([memo, " data-", acc.replace(".", "-"), "='<%= item.", acc, " %>'"]);
            },
            "").value(),

            items = make_each("group", "item", _.concat(["<li", data_tmp, ">", text_tmp, "</li>"]));
            groups = make_each(config.item_accessor, "group,v,k", _.concat(["<h3 class='select_list_group_name'><%= v %></h3><ul", list_class, ">", items, "</ul>"]));

            if (as_string) {
                return list_tmp;
            } else {
                return _.template(groups);
            }
        };
        tmpl.generateSelectList = function(config, as_string) {
            _.require(arguments, ["text_accessor", "item_accessor"]);

            var list_class = make_attr("class", config.class_name),
            list_items,
            list_tmp,
            class_name_tmp = make_idx_attr("class", config.item_class_name_accessor ? "item": "", config.item_class_name_accessor),
            text_tmp = _([config.text_accessor]).chain().flatten().reduce(function(memo, acc) {
                return _.concat([memo, "<span class='", acc, "'><%= item.", acc, " %></span> "]);
            },
            "").value(),
            data_tmp = _([config.data_accessor]).chain().flatten().reduce(function(memo, acc) {
                return _.concat([memo, " data-", acc.replace(".", "-"), "='<%= item.", acc, " %>'"]);
            },
            "").value();

            list_items = make_each(config.item_accessor, "item, idx", _.concat(["<li", data_tmp, class_name_tmp, ">", text_tmp, "</li>"]));
            list_tmp = _.concat(["<ul", list_class, ">", list_items, "</ul>"]);

            if (as_string) {
                return list_tmp;
            } else {
                return _.template(list_tmp);
            }
        };
        tmpl.generateButton = function(config, as_string) {
            var id = make_attr("id", config.id),
            className = make_attr("class", config.className),
            button_tmp = _.concat(["<button", id, className, "><%= text %></button>"]);
            if (as_string) {
                return button_tmp;
            } else {
                return _.template(button_tmp);
            }
        };
        tmpl.generateActionButton = function(config, as_string) {
            return tmpl.generateButton({
                id: "action",
                className: "button"
            },
            as_string);
        };
        tmpl.generateBackButton = function(config, as_string) {
            return tmpl.generateButton({
                id: "back",
                className: "button"
            },
            as_string);
        };
        tmpl.generateInput = function(config, as_string) {
            var input_tmpl = _.concat(["<p class='field'><label><%= input.", //
            config.label_accessor, " %></label>", //
            "<% if (_.isArray(input.options)) { %>", //
            "<select name='<%= input.", // 
            config.name_accessor, //
            " %>'>", //
            make_each("input.options", "option", _.concat([ //
            "<option value='<%= option.value %>'", //
            "<% if (option.value == input.", //
            config.value_accessor, //
            ") { %> selected='true' <% } %>", // 
            "><%= option.name %></option>"])), //
            "</select>", //
            "<% } else { %>", //
            "<input name='<%= input.", //
            config.name_accessor, //
            " %>' type='<%= input.", //
            (config.type_accessor), //
            " || 'text' %>' value='<%= input.", //
            config.value_accessor, //
            " %>' />", //
            "<% } %>", //
            "</p>"]);
            if (as_string) {
                return input_tmpl;
            } else {
                return _.template(input_tmpl);
            }
        };
        tmpl.generateFieldset = function(config, as_string) {
            _.require(arguments, ["legend_accessor", "input_accessor", "label_accessor", "value_accessor", "type_accessor", "button_accessor"]);

            var inputs_tmpl = make_each(config.input_accessor, "input", tmpl.generateInput({
                name_accessor: config.name_accessor,
                value_accessor: config.value_accessor,
                type_accessor: config.type_accessor,
                label_accessor: config.label_accessor
            },
            true)),
            fieldset_id = make_attr('id', config.id),
            fieldset_class = make_attr('class', config.class_name),
            button_tmpl = make_each(config.button_accessor, "button, idx", "<button class='fieldset_button' id='b_" + fieldset_id + "_<%= idx %>'><%= button %></button>"),
            fieldset_tmpl = _.concat(["<fieldset", fieldset_id, fieldset_class, "><legend><%= ", config.legend_accessor, " %></legend>", inputs_tmpl, "</fieldset>", button_tmpl]);

            if (as_string) {
                return fieldset_tmpl;
            } else {
                return _.template(fieldset_tmpl);
            }
        };
        tmpl.generateTableRow = function(accessor, config, as_string, tag) {
            var col_span_attr = make_idx_attr("colspan", config.col_span_accessor, "idx"),
            col_class_name = make_idx_attr("class", config.col_class_name_accessor, "idx"),
            row_class_name = make_idx_attr("class", config.row_class_name_accessor, "r_idx"),
            row_tmpl;

            if (_.isUndefined(tag) && _.isString(config.body_header_col_accessor)) {
                tag = _.concat(["<% if (idx === ", config.body_header_col_accessor, ") {%>th<% } else { %>td<% } %>"]);
            }

            row_tmpl = make_each(accessor, "row, r_idx", _.concat(["<tr", row_class_name, ">", //
            make_each("row", "item, idx", _.concat(["<", tag || "td", col_span_attr, col_class_name, "><%= item %></", tag || "td", ">"])), "</tr>"]));

            if (as_string) {
                return row_tmpl;
            } else {
                return _.template(row_tmpl);
            }
        };
        tmpl.generateTable = function(config, as_string) {
            _.require(arguments, ["head_accessor", "body_accessor", "caption_accessor"]);

            var class_attr = make_attr("class", config.classname_accessor),
            id_attr = make_attr("id", config.id),
            caption_tmpl = _.concat(["<% if (_.isString(", config.caption_accessor, ")) { %><caption><%=", config.caption_accessor, " %></caption> <% } %>"]),
            table_head_tmpl = _.concat(["<thead>", tmpl.generateTableRow(config.head_accessor, config, true, "th"), "</thead>"]),
            table_body_tmpl = _.concat(["<tbody>", tmpl.generateTableRow(config.body_accessor, config, true), "</tbody>"]),
            table_tmpl = _.concat(["<table", id_attr, class_attr, ">", caption_tmpl, table_head_tmpl, table_body_tmpl, "</table>"]);

            if (as_string) {
                return table_tmpl;
            } else {
                return _.template(table_tmpl);
            }
        };
        return {
            clearPage: function() {
                DOM.scrollable.html("");
                if (mainScroller.reset) mainScroller.reset();
            },
            appendToPage: function(html) {
                DOM.scrollable.append(html);
                if (mainScroller.refresh) mainScroller.refresh();
            },
            appendToFooter: function(html) {
                DOM.footer.prepend(html);
                setHeight();
                if (mainScroller.refresh) mainScroller.refresh();
            },
            appendToEl: function(html, el) {
                $(el).append(html);
                if (mainScroller.refresh) mainScroller.refresh();
            },
            addToolbarActionButton: function(text, callback) {
                $("#action").unbind();
                var template = tmpl.generateActionButton();
                DOM.header.append(template({
                    text: text
                }));
                $("#action").bind("tap", callback);
            },
            addToolbarBackButton: function(text, callback) {
                $("#back").unbind();
                var template = tmpl.generateBackButton();
                DOM.header.prepend(template({
                    text: text
                }));
                $("#back").bind("tap", callback);
            },
            hideToolbarActionButton: function() {
                $("#action").fadeOut(125)
            },
            showToolbarActionButton: function() {
                $("#action").fadeIn(125)
            },
            hideToolbarBackButton: function() {
                $("#back").fadeOut(125)
            },
            showToolbarBackButton: function() {
                $("#back").fadeIn(125)
            },
            removeToolbarActionButton: function() {
                $("#action").unbind();
                $("#action").fadeOut(125, function(evt) {
                    $(this).unbind().remove();
                });
            },
            removeToolbarBackButton: function() {
                $("#back").unbind();
                $("#back").fadeOut(125, function(evt) {
                    $(this).remove();
                });
            },
            animateIn: (function() {
                if (_.isAndroid()) {
                    return function() {
                        DOM.main.fadeIn(250);
                        return 250;
                    };
                }
                return function(isRight) {
                    if (isRight) {
                        DOM.main.css({
                            "-webkit-transition": "none",
                            "-webkit-transform": "translate3d(100%,0,0)"
                        });
                    }
                    DOM.main.css({
                        "-webkit-transition": "-webkit-transform 0.25s ease-in",
                        "-webkit-transform": "translate3d(0,0,0)"
                    });
                    return 250; // 0.25s
                };
            })(),
            animateOut: (function() {
                if (_.isAndroid()) {
                    return function() {
                        DOM.main.fadeOut(100);
                        return 250;
                    };
                }
                return function(isRight) {
                    var translate3d = isRight ? "translate3d(100%,0,0)": "translate3d(-100%,0,0)";
                    DOM.main.css({
                        "-webkit-transition": "-webkit-transform 0.25s ease-out",
                        "-webkit-transform": translate3d
                    });
                    return 250; // 0.25s
                };
            })(),
            generateGroupedSelectList: tmpl.generateGroupedSelectList,
            generateSelectList: tmpl.generateSelectList,
            generateFieldset: tmpl.generateFieldset,
            generateTable: tmpl.generateTable,
            addToolbarBackEvent: function(obj) {
                Templates.removeToolbarBackButton();
                if (obj) {
                    Templates.addToolbarBackButton(obj.text, function(evt) {
                        evt.preventDefault();
                        $(obj.scope).trigger(obj.event);
                    });
                }
            },
            addToolbarActionEvent: function(obj) {
                Templates.removeToolbarActionButton();
                if (obj) {
                    Templates.addToolbarActionButton(obj.text, function(evt) {
                        evt.preventDefault();
                        $(obj.scope).trigger(obj.event);
                    })
                }
            }
        };
    })();

    var FirstLook = {};
    FirstLook.templates = Templates;
    if (_.isUndefined(window.FirstLook)) window.FirstLook = FirstLook;

    FirstLook.Session = {};
    FirstLook.Session = {
        lastHash: "",
        matches: "([A-Za-z0-9_\\-%]+)",
        patterns: {},
        inTransaction: false,
        transactionHash: "",
        add_url_pattern: function(pattern, key) {
            var regexPattrn = pattern.replace(":" + key, FirstLook.Session.matches);
            FirstLook.Session.patterns[key] = new RegExp(regexPattrn);
        },
        wire: function(wires) {
            _.each(wires, FirstLook.Session.add_url_pattern);
        },
        get_state: function(key) {
            var matches, hash = window.location.hash;
            if (_.isUndefined(FirstLook.Session.patterns[key])) {
                matches = _.reduce(FirstLook.Session.patterns, function(return_value, value, key) {
                    var inner_matches = [];
                    if (value.test(this)) inner_matches = value.exec(this);
                    return_value[key] = inner_matches[1] ? decodeURIComponent(inner_matches[1]) : null;
                    return return_value;
                },
                {},
                hash);
                return matches;
            }
            if (!_.isUndefined(FirstLook.Session.patterns[key]) && FirstLook.Session.patterns[key].test(hash)) {

                matches = FirstLook.Session.patterns[key].exec(hash);
                return decodeURIComponent(matches[1]) || null;
            }

            return null;
        },
        set_state: function(key, value, passed) {
            var hash = passed || window.location.hash,
            new_hash = hash,
            pattern = FirstLook.Session.patterns[key],
            escaped_value = encodeURIComponent(value);
            if (!_.isRegExp(pattern)) {
                throw "No Wireing Defined";
            } else if (pattern.test(hash)) {
                if (value === null) {
                    sessionStorage.removeItem(key);
                    new_hash = hash.replace(pattern, "");
                } else {
                    sessionStorage.setItem(key, value);
                    new_hash = hash.replace(pattern, pattern.source.replace(FirstLook.Session.matches, escaped_value));
                }
            } else if (value !== null) {
                sessionStorage.setItem(key, value);
                new_hash += pattern.source.replace(FirstLook.Session.matches, escaped_value + "/");
            }

            if (!FirstLook.Session.inTransaction) {
                window.location.hash = new_hash;
                $(window).trigger("hashchange");
            } else {
                FirstLook.Session.transactionHash += new_hash;
            }
            return new_hash;
        },
        set_states: function(obj) {
            FirstLook.Session.inTransaction = true;
            var hash = window.location.hash;
            if (hash[hash.length - 1] !== "/") {
                hash += "/";
            }
            var new_hash = _.reduce(obj, function(memo, val, key) {
                var h = FirstLook.Session.set_state(key, val, memo);
                return h;
            },
            hash);
            window.location.hash = new_hash.replace(/\/\//g, '');
            $(window).trigger("hashchange");
            FirstLook.Session.transactionHash = "";
            FirstLook.Session.inTransaction = false;
        },
        onChange: function(evt) {
            sessionStorage.setItem('hash', window.location.hash);
            FirstLook.Session.lastHash = window.location.hash;
        },
        checkForChange: function() {
            if (FirstLook.Session.lastHash != window.location.hash) {
                $(window).trigger("hashchange");
            }
        },
        pop: function() {
            var a = _.compact(window.location.hash.split("/"));
            if (a.length > 2) {
                FirstLook.Session.set_state(a[a.length - 2], "");
            }
        },
        clear: function() {
            window.location.hash = "";

            $(window).trigger("hashchange");
        }
    };
    $(window).bind("hashchange", FirstLook.Session.onChange);

    function generateTemplate(card) {
        _.require(arguments, ["view", "pattern"]);

        var template = {};

        if (!_.isUndefined(card.default_view)) {
            template.default_view = card.default_view;
        }

        if (!_.isUndefined(card.pattern)) {
            template.can_render = (function() {
                var gate = generate_pattern_gate(card.pattern);
                return function() {
                    return gate(FirstLook.Session.get_state());
                };
            })();
        }

        if (card.view) {
            template.views = {};
            _.reduce(card.view, generateView, template.views);
        }

        template.build = function(data, opts) {
            var views = this.views,
            is_array = _.isArray(views),
            temp_views,
            view_to_render;

            if (is_array) {
                temp_views = views;
            } else {
                temp_views = _.sortBy(views, function(v) {
                    return _.isNumber(v.order) ? v.order: Infinity;
                });
            }

            view_to_render = _.detect(temp_views, function(v) {
                if (_.isUndefined(v.pattern)) return false;
                return generate_pattern_gate(v.pattern)(data);
            });

            if (view_to_render) {
                _.invoke(views, "clean", true);
                view_to_render.build(data, opts);
            }

        };

        return template;
    }

    function generate_pattern_gate(pattern) {
        return function(data) {
            var return_value = _.all(pattern, where, data);
            return return_value;
        };
    }

    function generateView(value, field, key, list) {
        var newView = {};
        var components = [];
        var known_keys = ["pattern", "behaviors", "order"];
        var view_fields = _.reject(field, function(value, key) {
            return _.contains(known_keys, key);
        });

        newView.__type__ = _.concat([key, "_", _.uniqueId()]);

        newView.pattern = field.pattern;
        newView.order = field.order;

        _.each(field, function(inner_value, inner_key, inner_list) {
            if (_.contains(known_keys, inner_key)) return;
            if (_.isUndefined(newView[inner_key])) newView[inner_key] = {};
            newView[inner_key] = generateComponent(inner_value);
            components.push(newView[inner_key]);
        });

        newView.build = function(data, opts) {
            var self = this;
            if (_.isUndefined(opts)) opts = {};

            if (!opts.avoid_render) FirstLook.cards.render(this);
            _.each(components, function(item) {
                _.defer(function() {
                    item.build(data, self.__type__);
                });
            });
        };
        newView.clean = function(args) {
            _.invoke(components, "clean", args);
        };

        value[key] = newView;

        return value;
    }

    function generateBaseComponent(base) {
        return _.extend(base, {
            rendered: false,
            build: function(data, view_type) {
                var pass = this.data_gate(data);
                if (!pass) {
                    console.log(this, data);
                    throw new Error("Incorrect Data For View");
                }
                var merged_data = _.extend(data, this.static_data);
                $(this).trigger("prepare_data", [merged_data]);
                if (_.isFunction(this.filter)) data = this.filter(data);
                this.data = data;
                $(this).trigger("before_clean");
                $(this).trigger("before_dom_render");
                var el = this.get_el(),
                newRender = ! el.size();
                if (newRender) {
                    el = this.$el = $("<div/>").attr("id", this.__type__);
                    if (this.class_name) el.addClass(this.class_name);
                }
                var new_dom = this.template(merged_data);
                el.append(new_dom);

                $("a", el).bind("tap", function(evt) {
                    var clickEvt = document.createEvent("HTMLEvents");
                    clickEvt.initEvent("click", true, true);
                    this.dispatchEvent(clickEvt);
                });

                if (newRender) {
                    var view = $("#" + view_type);
                    if (view.size() > 0) {
                        Templates.appendToEl(el, view);
                    } else {
                        Templates.appendToPage(el);
                    }
                }
                $(this).trigger("dom_rendered");
            },
            clean: function(clean_all) {
                $(this).trigger("before_clean");
                var el = this.get_el();
                $(el).unbind("tap");
                if (clean_all) {
                    el.remove();
                } else {
                    el.children().remove();
                }
                this.$el = null;
            },
            get_el: function() {
                return this.$el = this.$el || $("#" + this.__type__);
            },
            build_complete: function() {
                $(this).trigger("build_complete");
            },
            behaviors: {}
        });
    }

    function bequeath(accessor) {
        return function(memo, value, key) {
            function handle_array_bequeath(_memo, _value, _key) { // Hack to fix extending arrays...PM
                if (_.isArray(_value) && _.isArray(memo[_key])) {
                    _.extend(memo[_key][0], _value[0]);
                    delete _memo[_key];
                }
                return _memo;
            }

            var accessed_value = value[accessor],
            avoid_extend = (_.isEqual(value, _) || _.isUndefined(accessed_value));
            if (!avoid_extend) {
                _.reduce(accessed_value, handle_array_bequeath, accessed_value);
                _.extend(memo, accessed_value);
            }
            return memo;
        };
    }

    function generateComponent(config) {
        var return_value = {},
        type_behaviors = [],
        config_clone = _.clone(config);

        _.require(config_clone, ["type", "pattern"]);
        // TODO: should I test for existance fo behaviors?
        /// Build Full pattern from behaviors and get the gate
        _.reduce(config_clone.behaviors, bequeath("pattern"), config_clone.pattern);
        return_value.data_gate = generate_pattern_gate(config_clone.pattern);

        generateBaseComponent(return_value);

        /// Generate the unique id for the componentType
        return_value.__type__ = _.concat([config_clone.type, "_", _.uniqueId()]);

        if (config_clone.static_data) {
            return_value.static_data = config_clone.static_data;
        }
        if (config_clone.filter) {
            return_value.filter = config_clone.filter;
        }
        if (config_clone.class_name) {
            return_value.class_name = config_clone.class_name;
        }

        switch (config.type) {
        case "List":
            _.require(config_clone, components.List.requires);

            _.extend(return_value, components.List.extend_to);

            apply_behaviors(config_clone, return_value);

            return_value.template = components.List.generateTemplate(config_clone);
            break;
        case "GroupedList":
            _.require(config_clone, components.GroupedList.requires);

            _.extend(return_value, components.GroupedList.extend_to);

            apply_behaviors(config_clone, return_value);

            return_value.template = components.GroupedList.generateTemplate(config_clone);
            break;
        case "Fieldset":
            _.require(config_clone, components.Fieldset.requires);

            _.extend(return_value, components.Fieldset.extend_to);

            apply_behaviors(config_clone, return_value);

            return_value.template = components.Fieldset.generateTemplate(config_clone);
            break;
        case "Table":
            _.require(config_clone, components.Table.requires);
            _.extend(return_value, components.Table.extend_to);

            apply_behaviors(config_clone, return_value);

            return_value.template = components.Table.generateTemplate(config_clone);
            break;
        case "Tabs":
            _.require(config_clone, components.Table.requires);

            return_value.components = config_clone.components;

            apply_behaviors(config_clone, return_value);

            return_value.template = components.Tabs.generateTemplate();
            break;
        case "Carousel":
            _.require(config_clone, components.Carousel.requires);

            return_value.components = config_clone.components;

            apply_behaviors(config_clone, return_value);

            return_value.components = components.Carousel.get_components(config_clone);

            return_value.template = components.Carousel.generateTemplate(config_clone, return_value.components);
            break;
        case "Static":
            _.require(config_clone, ["template"]);

            apply_behaviors(config_clone, return_value);

            return_value.template = config_clone.template;
            break;
        case "Placeholder":
            _.require(config_clone, ["component"]);

            apply_behaviors(config_clone, return_value);

            return_value.template = components.Placeholder.generateTemplate(config_clone);
            break;
        case "Cardholder":
            _.require(config_clone, ["card"]);

            apply_behaviors(config_clone, return_value);

            return_value.template = components.Cardholder.generateTemplate(config_clone);
            break;
        default:
            break;
        }
        return return_value;
    }

    function apply_behaviors(config, obj) {
        /// Ensure required attributes for behaviors
        _.each(config.behaviors, function(value, key) {
            var behave = behaviors[key];
            if (_.isUndefined(behave)) return;
            _.require(value, behave.requires || []);

            obj.behaviors[key] = value;

            if (!_.isUndefined(behave.bequeath)) {
                _.each(behave.bequeath, function(_value, _key) {
                    config[_value] = config.behaviors[key][_value];
                });
            }

            if (_.isArray(behave.listens_to)) apply_behavior_events(behave, obj);

            _.each(value.binds || {},
            function(value, key) {
                /// If the value is a string we call the event with the object as the scope,
                if (_.isString(value)) {
                    $(obj).bind(key, function(evt) {
                        $(obj).trigger(value, _.toArray(arguments).slice(1));
                    });
                } else if (value.scope && value.type) {
                    // otherwise we raise the event with the passed in scope and type attributes, PM
                    $(obj).bind(key, function(evt) {
                        $(value.scope).trigger(value.type, _.toArray(arguments).slice(1));
                    });
                }
            });
        });
    }

    function apply_behavior_events(behavior, obj) {
        _.each(behavior.listens_to, function(value) {
            var handler = behavior["on_" + value];
            if (_.isFunction(handler)) $(obj).bind(value, handler);
        });
    }

    var behaviors = {
        // Behavior Helper Methods
        get_instance_var: function(scope, key) {
            /// TODO Add error handling
            return scope[key];
        },
        get_instance_type_var: function(scope, type, key) {
            /// TODO Add error handling
            return scope.behaviors[type][key];
        },
        get_instance_data: function(scope, type, key) {
            /// TODO Add error handling
            var accessor = behaviors.get_instance_type_var(scope, type, key) || "";
            function get_access(scope, accessor, tail) {
                if (tail && tail.length > 0) {
                    return get_access(scope[accessor] || {},
                    _.first(tail), _.rest(tail));
                }
                return scope[accessor];
            }
            return get_access(scope, "data", accessor.split("."));
        },
        get_type_var: function(type, key) {
            /// TODO Add error handling
            return behaviors[type][key];
        },
        get_type: function(type) {
            /// TODO Add error handling
            return behaviors[type];
        },
        // Behavior Types
        selectable: {
            requires: ["data_accessor"],
            bequeath: ["data_accessor"],
            raises: ["item_selected"],
            listens_to: ["dom_rendered", "before_clean"],
            on_dom_rendered: function() {
                var raising = $.proxy(behaviors.get_type_var("selectable", "on_item_selected"), this),
                el = this.get_el(),
                selector = behaviors.get_instance_var(this, "item_selector"); // refactor so it can also handler clicks with out a item selector (i.e. the whole comonent);
                $(selector, el).bind("tap", raising);
            },
            on_before_clean: function() {
                var selector = behaviors.get_instance_var(this, "item_selector"),
                el = this.get_el();

                $(selector, el).unbind("tap"); // reconfigure to die
            },
            on_item_selected: function(evt) {
                var data_accessor = behaviors.get_instance_type_var(this, "selectable", "data_accessor"),
                data,
                el = this.get_el(),
                selector = behaviors.get_instance_var(this, "item_selector"),
                target = $(evt.target).is(selector) ? evt.target: $(evt.target).parents(selector);

                if (_.isArray(data_accessor)) {
                    data = _([data_accessor]).chain().flatten().reduce(function(memo, acc) {
                        var a = acc.replace(".", "-").toLowerCase();
                        memo[acc] = this.attr('data-' + a);
                        return memo;
                    },
                    {},
                    $(target)).value();
                } else {
                    data = $(target).attr('data-' + data_accessor);
                }
                $(target).addClass("selected");
                el.find(selector + '.selected').not(target).removeClass('selected');
                $(this).trigger("item_selected", [data]);
            }
        },
        multi_selectable: {
            requires: ["multi_data_accessor", "multi_item_accessor"],
            bequeath: ["multi_data_accessor", "multi_item_accessor"],
            raises: ["item_selected"],
            listens_to: ["dom_rendered", "before_clean", "selection_set"],
            on_dom_rendered: function() {
                var raising = $.proxy(behaviors.get_type_var("multi_selectable", "on_item_selected"), this),
                el = this.get_el(),
                selector = behaviors.get_instance_var(this, "item_selector"),
                items = behaviors.get_instance_data(this, "multi_selectable", "multi_item_accessor"); // refactor so it can also handler clicks with out a item selector (i.e. the whole comonent);
                $(selector, el).bind("tap", raising);
                $(this).trigger("selection_set", [items]);
            },
            on_before_clean: function() {
                var selector = behaviors.get_instance_var(this, "item_selector"),
                el = this.get_el();

                $(selector, el).unbind("tap"); // reconfigure to die
            },
            on_item_selected: function(evt) {
                var data_accessor = behaviors.get_instance_type_var(this, "multi_selectable", "multi_data_accessor"),
                map_accessor = behaviors.get_instance_type_var(this, "multi_selectable", "map_data_accessor"),
                data,
                el = this.get_el(),
                selector = behaviors.get_instance_var(this, "item_selector"),
                target = $(evt.target).is(selector) ? evt.target: $(evt.target).parents(selector);

                if (_.isArray(data_accessor)) {
                    data = _.reduce(data_accessor, function(memo, attr) {
                        memo[attr] = this.attr('data-' + attr);
                        return memo;
                    },
                    {},
                    $(target));
                } else {
                    data = $(target).attr('data-' + (map_accessor || data_accessor));
                }
                $(target).toggleClass("selected");

                $(this).trigger("item_selected", [data, !! $(target).hasClass("selected")]);
            },
            on_selection_set: function(evt, data) {
                var data_accessor = behaviors.get_instance_type_var(this, "multi_selectable", "multi_data_accessor"),
                map_accessor = behaviors.get_instance_type_var(this, "multi_selectable", "map_data_accessor"),
                el = this.get_el(),
                selector = behaviors.get_instance_var(this, "item_selector"),
                items = el.find(selector),
                selected = _(data).chain().filter(function(i) {
                    return i.Selected;
                }).pluck(data_accessor).invoke("toString").value();

                _.each(items, function(item) {
                    var item_data = $(item).attr("data-" + (map_accessor || data_accessor)),
                    is_selected = _(selected).contains(item_data);
                    $(item)[is_selected ? "addClass": "removeClass"]("selected");
                });
            }
        },
        pageable: {
            requires: ["page_size_accessor", "total_items_accessor", "start_index_accessor"],
            raises: ["next", "prev", "start_index_change"],
            listens_to: ["dom_rendered", "before_clean", "should_call_next", "should_call_prev"],
            has_next: function() {
                var t = behaviors.get_instance_data(this, "pageable", "total_items_accessor"),
                s = behaviors.get_instance_data(this, "pageable", "start_index_accessor"),
                m = behaviors.get_instance_data(this, "pageable", "page_size_accessor");
                return t > s + m;
            },
            has_prev: function() {
                var s = behaviors.get_instance_data(this, "pageable", "start_index_accessor");
                return s > 0;
            },
            next: function() {
                var s = behaviors.get_instance_data(this, "pageable", "start_index_accessor"),
                m = behaviors.get_instance_data(this, "pageable", "page_size_accessor"),
                new_start_index = s + m;
                $(this).trigger("start_index_change", [new_start_index]);
            },
            prev: function() {
                var s = behaviors.get_instance_data(this, "pageable", "start_index_accessor"),
                m = behaviors.get_instance_data(this, "pageable", "page_size_accessor"),
                new_start_index = s - m;
                $(this).trigger("start_index_change", [new_start_index]);
            },
            on_dom_rendered: function() {
                var selector = "#" + this.__type__,
                has_next = behaviors.get_type_var("pageable", "has_next").apply(this),
                append_more = behaviors.get_type_var("pageable", "append_more_link");

                if (has_next) {
                    append_more.apply(this);
                }
            },
            on_before_clean: function() {
                var el = this.get_el();
                $("li.loading", el).unbind("tap").remove();
            },
            append_more_link: function() {
                var el = this.get_el(),
                next_action = $.proxy(behaviors.get_type_var("pageable", "next"), this);

                $("li:last", el).after("<li class='loading'>More</li>"); // Assumes List Type
                $("li.loading", el).bind("tap", next_action);
            },
            on_should_call_next: function() {
                var has_next = behaviors.get_type_var("pageable", "has_next").apply(this),
                next_action = $.proxy(behaviors.get_type_var("pageable", "next"), this);

                if (has_next) next_action();
            },
            on_should_call_prev: function() {
                var has_prev = behaviors.get_type_var("pageable", "has_prev").apply(this),
                prev_action = $.proxy(behaviors.get_type_var("pageable", "prev"), this);

                if (has_prev) prev_action();
            }
        },
        infinate_scrollable: {
            requires: [],
            raises: ["scroll_at_end"],
            listens_to: ["dom_rendered", "before_clean", "scroll_at_end"],
            on_dom_rendered: function() {
                $(document.body).bind("maxScrollY", $.proxy(function() {
                    $(this).trigger("scroll_at_end");
                },
                this));
            },
            on_before_clean: function() {
                $(document.body).unbind("maxScrollY");
            },
            on_scroll_at_end: function(evt) {
                $(this).trigger("should_call_next");
            }
        },
        groupable: {
            requires: ["group_accessor"],
            raises: ["data_prepared"],
            listens_to: ["prepare_data"],
            bequeath: ["group_accessor"],
            on_prepare_data: function(evt, data) {
                var group = behaviors.get_instance_type_var(this, "groupable", "group_accessor"),
                by = behaviors.get_instance_type_var(this, "groupable", "group_by_accessor");

                data[group] = _.group_by(data[group], by);

                return data;
            }
        },
        data_mergable: {
            requires: ["data_destination_accessor"],
            listens_to: ["prepare_data"],
            on_prepare_data: function(evt, data) {
                var data_destination_accessor = behaviors.get_instance_type_var(this, "data_mergable", "data_destination_accessor");
                if (data[data_destination_accessor]) {
                    _.each(data[data_destination_accessor], function(value, key) {
                        if (value.data && value.data.indexOf(".") !== - 1) {
                            value.data = _.reduce(value.data.split("."), function(memo, acc) {
                                var return_value = memo[acc];
                                return return_value;
                            },
                            data);
                        } else {
                            value.data = data[value.data] || "";
                        }
                        if (_.isString(value.options)) {
                            if (value.options.indexOf(".") !== - 1) {
                                value.options = _.reduce(value.data.split("."), function(memo, acc) {
                                    var return_value = memo[acc];
                                    return return_value;
                                },
                                data);
                            } else {
                                value.options = data[value.options] || [];
                            }
                        }
                    });
                }
            }
        },
        submitable: {
            requires: ["event", "scope"],
            raises: ["submited"],
            listens_to: ["dom_rendered", "clean"],
            on_dom_rendered: function() {
                var event_name = behaviors.get_instance_type_var(this, "submitable", "event");
                var scope = behaviors.get_instance_type_var(this, "submitable", "scope");
                var submit_on_change = behaviors.get_instance_type_var(this, "submitable", "submit_on_change");
                var raise_event = function(evt) {
                    evt.preventDefault();
                    evt.stopPropagation();
                    var params = {};
                    var inputs = el.find("input, select").serializeArray();
                    _.reduce(inputs, function(memo, item) {
                        memo[item.name] = item.value;
                        return memo;
                    },
                    params);
                    $(scope).trigger(event_name, params); // serializeArray with custom info, PM
                };
                var el = this.get_el();
                el.find("button").bind("tap", raise_event);
                el.find("input").bind("keypress", function(evt) {
                    if (evt.charCode === 13) {
                        raise_event(evt);
                    }
                });
                if (submit_on_change) {
                    el.find("input").bind("blur", raise_event);
                    el.find("select").bind("change", raise_event);
                }
            },
            on_clean: function() {
                var el = this.get_el();
                $("button", el).unbind("tap");
            }
        },
        rebuildable: {
            requires: [],
            raises: ["rebuilt"],
            listens_to: ["rebuild"],
            on_rebuild: function(evt, data) {
                this.clean();
                this.build(data);
            }
        },
        tabable: {
            requires: ["application_accessor"],
            bequeath: ["application_accessor"],
            raises: ["tab_change"],
            listens_to: ["dom_rendered", "before_clean"],
            on_dom_rendered: function() {
                var applications = behaviors.get_instance_data(this, "tabable", "application_accessor");
                var sub_tabs = behaviors.get_instance_data(this, "tabable", "sub_tabs_accessor");
                var options = behaviors.get_instance_data(this, "tabable", "application_options_accessor");
                _.each(applications, function(v) {
                    _.delay(v.main, 0, _.isFunction(options) ? options(v) : {});
                });

                var id_suffix = this.get_el().attr("id");
                var tabs = $("#tabs_" + id_suffix);

                $(":first-child", tabs).addClass("active");
                tabs.find("li").bind("tap", function(evt) {
                    var el = $(evt.target);
                    if (!el.is("li")) return;

                    var idx = el.attr("data-idx"),
                    adjusted = idx == "0" ? idx: parseInt(idx, 10) * $(document.body).width(),
                    tab_group = el.parent(),
                    tab_groups;

                    if (sub_tabs) {
                        var classes = _.reduce(sub_tabs, function(m, v) {
                            return m + ".tab_button_" + v + ", ";
                        },
                        "");
                        if (el.is(classes)) {
                            tab_group.addClass("small_tab");
                            tab_groups = tab_group.siblings("ul.tabs");
                            tab_groups.hide();
                            var at_idx = tab_groups.length - idx;
                            tab_groups.slice(at_idx, at_idx + 1).show();
                        } else {
                            tab_group.removeClass("small_tab");
                            tab_group.siblings().hide();
                        }
                    }

                    $("#tab_panel_" + id_suffix).css({
                        "left": "-" + adjusted + "px"
                    });
                    tab_group.children().removeClass("active");
                    el.addClass("active");
                    if (mainScroller.reset) {
                        setHeight();
                        resetScroll();
                    }
                });
            },
            on_before_clean: function() {
                var applications = behaviors.get_instance_data(this, "tabable", "application_accessor"),
                applications_with_clean = _.select(applications, function(i) {
                    return _.isFunction(i.clean);
                });

                _.invoke(applications_with_clean, "clean");

                var id_suffix = this.get_el().attr("id");
                var tabs = $("#tabs_" + id_suffix);
                tabs.unbind().remove();
            }
        },
        carouselable: {
            requires: [],
            listens_to: ["dom_rendered", "before_clean", "rebuild"],
            on_dom_rendered: function(evt) {
                var el = this.get_el();
                el.addClass("carousel");
                this.__scroller__ = applyCarousel("#" + this.__type__, "div.slide");
            },
            on_rebuild: function(evt, data) {
                _.each(this.components, function(comp) {
                    _.defer(function() {
                        comp.clean();
                        comp.build(data);
                    });
                });
                this.__scroller__.onrebuild();
            }
        },
        hideable: {
            requires: ["visible_on_render","animate"],
            listens_to: ["dom_rendered","hide","show"],
            on_dom_rendered: function(evt) {
                var visible_on_render = behaviors.get_instance_type_var(this, "hideable", "visible_on_render");
                if (!visible_on_render) {  
                    this.get_el().hide();
                } else {
                    this.get_el().show();
                }
            },
            on_hide: function(evt) {
                var animate_out = behaviors.get_instance_type_var(this, "hideable", "animate");
                this.get_el()[animate_out ? "fadeOut": "hide"]();
            },
            on_show: function(evt) {
                var animate_in = behaviors.get_instance_type_var(this, "hideable", "animate");
                this.get_el()[animate_in ? "fadeIn": "show"]();
            }
        },
        focusable: {
            requires: [],
            listens_to: ["set_to_focus"],
            on_set_to_focus: function() {
                this.get_el().findw(":input:first").focus();
            }
        }
    };

    var components = {
        List: {
            requires: ["item_accessor", "text_accessor"],
            extend_to: {
                item_selector: "li"
            },
            generateTemplate: function(config) {
                return FirstLook.templates.generateSelectList(config);
            }
        },
        GroupedList: {
            requires: ["item_accessor", "text_accessor"],
            extend_to: {
                item_selector: "li"
            },
            generateTemplate: function(config) {
                return FirstLook.templates.generateGroupedSelectList(config);
            }
        },
        Fieldset: {
            requires: ["input_accessor", "label_accessor", "type_accessor", "button_accessor"],
            generateTemplate: function(config) {
                return FirstLook.templates.generateFieldset(config);
            }
        },
        Table: {
            requires: [],
            generateTemplate: function(config) {
                return FirstLook.templates.generateTable(config);
            }
        },
        Tabs: {
            requires: ["keys"],
            generateTemplate: function() {
                return function(data) {
                    data.labels = data.labels || [];
                    var w = $(document.body).width();
                    var cards = _.reduce(data.keys, function(memo, card) {
                        memo.push(FirstLook.cards.get(card));
                        return memo;
                    },
                    []);

                    var el = this.get_el();

                    var tabs = $("<ul />").attr("id", "tabs_" + this.__type__).addClass("tabs");

                    var els = _.map(data.keys, function(id, idx) {
                        var tab_panel = $("<div/>").addClass("card_" + id).addClass("tab tab_" + idx).width(w).css({
                            "float": "left",
                            "white-space": "normal"
                        });
                        var card = cards[idx];

                        _.each(card.views, function(v, k, l) {
                            var view = $("<div />").attr({
                                "id": v.__type__
                            });
                            tab_panel.append(view);
                        });

                        tabs.append($("<li/>").text(data.labels[idx] || id).attr({
                            "data-idx": idx
                        }).addClass("tab_button tab_button_" + idx + " tab_button_" + id));

                        return tab_panel;
                    });
                    el.width(w).css({
                        "overflow": "hidden"
                    });

                    FirstLook.templates.appendToFooter(tabs);

                    var panels = $("<div />").attr({
                        "id": "tab_panel_" + this.__type__
                    }).css({
                        "white-space": "nowrap",
                        "position": "relative",
                        "-webkit-transition": "left 0.25s ease-in-out"
                    });

                    panels.width(w * (cards.length + 1));

                    _.invoke(els, "appendTo", panels);

                    return panels;
                };
            }
        },
        Carousel: {
            requires: ["components"],
            get_components: function(config) {
                return _.map(config.components, function(comp) {
                    var _comp = generateComponent(FirstLook.components.get(comp));
                    return _comp;
                });

            },
            generateTemplate: function(config, components) {
                return function(data) {
                    var types = _.pluck(components, "__type__");
                    els = _.map(types, function(id) {
                        return "<div id='" + id + "' class='slide'/>";
                    });

                    _.defer((function(comps, data) {
                        return function() {
                            _.invoke(comps, "build", data);
                        };
                    })(components, data));

                    return _.concat(["<div class='handle'>", _.concat(els), "</div>"]);
                };
            }
        },
        Placeholder: {
            requires: ["component"],
            generateTemplate: function(config) {
                return function(data) {
                    if (!config._component) {
                        config._component = generateComponent(FirstLook.components.get(config.component));
                    }
                    if (!config.rendered) {
                        config.rendered = true;
                        return "<div id='" + config._component.__type__ + "'></div>";
                    } else {
                        _.defer(function() {
                            config._component.clean();
                            config._component.build(data);
                        });
                    }
                    return null;
                };
            }
        },
        Cardholder: {
            requires: ["card"],
            generateTemplate: function(config) {
                return function(data) {
                    if (!config._card) {
                        config._card = FirstLook.cards.get(config.card);
                    }
                    if (!config.rendered) {
                        config.rendered = true;
                        var t =_(config._card.views).chain().pluck("__type__").reduce(function(memo, v) {
                            var r = _.concat([memo,"<div id='" , v , "'></div>", ""]);
                            return r;
                        }, "").value();
                        return t;
                    } else {
                        _.defer(function() {
                            config._card.build(data, {
                                avoid_render: true
                            });
                        });
                    }
                };
            }
        }
    };

    var patterns = {};

    var cards = {
        rendered: false,
        lastRenderedString: "",
        lastRendered: undefined,
        lastRenderedCollection: undefined,
        cards: [],
        key_card: [],
        card_data: {},
        render: function(cls) { /// Consider renaming... it's not really rendered here, PM
            var rendering, time;

            rendering = cls.__type__ || JSON.stringify(cls);

            if (!cards.rendered) FirstLook.templates.animateIn();

            if (cards.lastRenderedString !== "" && ! _.isEqual(rendering, cards.lastRenderedString) && $("#" + cls.__type__).size() === 0) {
                time = FirstLook.templates.animateOut();
                _.delay(_.bind(cards.lastRendered.clean, cards.lastRendered, true), time);
                if (cards.lastRenderedCollection) {
                    _.delay(_.bind(cards.lastRenderedCollection.clean, cards.lastRenderedCollection, true), time);
                    resetScroll();
                }
                _.delay(_.bind(FirstLook.templates.animateIn, cards, true), time + 100);
                if (mainScroller.reset) _.defer(function() {
                    mainScroller.reset();
                });
                cards.lastRenderedCollection = cls;
            }

            cards.lastRenderedString = rendering;
            cards.lastRendered = cls;
            cards.rendered = true;
            return time || 0;
        },
        add: function(card, key) {
            this.key_card.push(key);
            this.cards.push(generateTemplate(card));
        },
        get: function(key) {
            var idx = _.indexOf(this.key_card, key);
            if (idx !== - 1) {
                return this.cards[idx];
            }
            return null;
        },
        register_card_data: function(key, data) {
            this.card_data[key] = data;
        },
        get_card_data: function(key) {
            return this.card_data[key] || {};
        }
    };
    FirstLook.cards = cards;

    function create_obj() {
        return function() {
            if (_.isFunction(this.init)) {
                this.init();
            }
        }
    }
    var proto_registry = {
        init: function() {
            this.items = [];
            this.key_card = []
        },
        add: function(comp, key) {
            this.key_card.push(key);
            this.items.push(comp);
        },
        get: function(key) {
            var idx = _.indexOf(this.key_card, key);
            if (idx !== - 1) {
                return this.items[idx];
            }
            return null;
        }
    };

    var registered_components = {
        add: function(comp, key) {
            registered_components.key_card.push(key);
            registered_components.components.push(comp);
        },
        get: function(key) {
            var idx = _.indexOf(registered_components.key_card, key);
            if (idx !== - 1) {
                return registered_components.components[idx];
            }
            return null;
        },
        key_card: [],
        components: []
    };
    var registry = create_obj();
    registry.prototype = proto_registry;

    FirstLook.components = new registry();
    FirstLook.action_button = new registry();
    FirstLook.back_button = new registry();

    function onReady() {
        var isAndroid = _.isAndroid();
        var isIThing = _.isIThing();
        getDOM(); /// Load DOM Template Items
        if (!isAndroid) { // All supported versions of Android have position fixed, Pm
            applyJsScroll(); /// iOs doesn't ... so we fake it, PM
        } else {
            applySensibleScroller(); // Useing a sensable scroller for everything else, Pm
        }
        if (isIThing) {
            document.body.className += " is_ithing";
        }
        if (isAndroid) {
            document.body.className += " is_android";
        }
        else {
            document.body.className += " non_android";
        }
        if (!isAndroid && ! isIThing) {
            document.body.className += " is_desktop";
        }
        $("form").bind("submit", _.preventDefault)
    }
    $(document).ready(onReady);

    FirstLook.components.add({
        type: "Static",
        pattern: {
            errorType: _.isString,
            errorResponse: {
                responseText: _.isString
            }
        },
        template: function(data) {
            return "<p class='error'>" + data.message + "</p>";
        },
        filter: function(data) {
            var message = JSON.parse(data.errorResponse.responseText);

            if (_.isString(message.Message)) {
                message = message.Message;
            } else {
                message = data.errorType;
            }
            data.message = message;
            return data;
        }
    },
    "Application.ServerError");
})();

