var cards = {
	dealers: {
		title: "Dealerships",
		// Title to show in the Top Toolbar
		default_view: "select",
		/// Not needed if there is only a single  view, PM
		pattern: {
			broker_id: _.isEmpty
			// Where is broker_id... smells like State to me, PM
		},
		view: {
			select: {
				back_button: false,
				action_button: {
					text: "Search",
					show: cards.dealers.view.search
				},
				ClientList: {
					type: "List",
					selectable: true,
					infinate_scrollable: true,
					groupable: true,
					group_accessor: "OrganizationName",
					item_accessor: "Clients",
					text_accessor: "Name",
					data_accessor: "Id",
					pattern: {
						Clients: [{
							Id: _.isNumber,
							Name: _.isString,
							OrganizationName: _.isString
						}],
						MaximumRows: _isNumber,
						SortColumns: [{
							Ascending: _.isBoolean,
							ColumnName: _.bind(_.isEqual, "", this.group_accessor)
							// Assuming that "this" is cards.dealers.view.select.List.group_accessor, PM
						},
						{
							Ascending: _.isBoolean,
							ColumnName: _.isString
						}]
					},
					raises: ["item_selected", "next"],
					listens_to: ["scroller.maxScrollY", "ClientSearchForm.submited"],
					binds: {
						"scroller.maxScrollY": "next",
						// This might need configure what gets forward on, PM
						"item_selected": "cards.actions",
						// This seems questionable to me, PM Might need to pass a navigateTo Message, PM
						"ClientSearchForm.submited": "reinit"
					}
				}
			},
			search: {
				backButton: {
					text: "Client List",
					show: cards.dealers.view.select
				},
				actionButton: false,
				ClientSearchForm: {
					type: "Fieldset",
					preventsSubmit: true,
					legend: "Search",
					input_accessor: _,
					// wtf?
					button_accessor: _.values,
					// Matches provided static buttons array _.values(this.buttons)
					buttons: ["Search"],
					label_accessor: _.keys,
					// Matches provided static labels object _.keys(this.labels)
					value_accessor: _.values,
					// Matches provided static buttons array _.values(this.values)
					values: ["AuthorityName", "Client$Name", "Client$OrganizationName", "Address$State", "Address$City", "Address$Zip"],
					labels: {
						"AuthorityName": "Type",
						"Client$Name": "Name",
						"Client$OrganizationName": "Group",
						"Address$State": "State",
						"Address$City": "City",
						"Address$Zip": "Zip"
					},
					pattern: {
						"AuthorityName": _.isString,
						"Client$Name": _.isString,
						"Client$OrganizationName": _.isString,
						"Address$State": _.usStates(),
						"Address$City": _.isString,
						"Address$Zip": _.isNumber
					},
					raises: ["before_submit", "submitted", "client_search"]
				}
			}
		}
	},
	actions: {
		title: "Actions",
		tabs: ["actions", "preferences"],
		pattern: {
			broker_id: _.isString,
			vin: _.isEmpty,
			/// I'm not sure if this is really needed... but it seems logical to enforce empty VINa and Vehicle and list Ids to show the actions list, PM
			fl_vehicle_id: _.isEmpty,
			// FirstLook Vehicle Id
			fl_tag_id: _.isEmpty // FirstLook List Id
		},
		view: {
			actions: {
				action_button: false,
				back_button: false,
				ActionList: {
					type: "List",
					selectable: true,
					item_accessor: _.values,
					items: ["Scan Vin", "Manual Books", "Vehicle List"],
					pattern: _,
					raises: ["item_selected"]
				}
			},
			prefences: {
				action_button: {
					text: "Done",
					raise: "save"
				},
				back_button: false,
				PreferenceForm: {
					type: "Fieldset",
					preventsSubmit: true,
					legend: "Preferences",
					input_accessor: _,
					// wtf?
					button_accessor: _,
					buttons: [],
					value_accessor: _,
					validation_accesssor: _,
					/// wft?
					label_accessor: _.keys,
					labels: {
						/// Needs structure for the Client Prefernces, PM
						"Availble Books": "",
						"Books Order": "",
						"VHR Accounts": "",
						"Overview Page Values": ""
					},
					pattern: _.any,
					raises: ["before_submit", "submitted", "preferences_changed"]
				}
			}
		}
	},
	books: {
		title: "Manual Books",
		default_view: "black_book",
		// Should be a Dealer/User Preference
		tabs: ["black_book", "kelly_blue_book", "galves", "nada"],
		/// Configurable, PM
		pattern: {
			broker_id: _.isString
		},
		view: { /// Visibility/Order Needs to be a preference, PM
			black_book: cards.black_book,
			kelly_blue_book: cards.kelly_blue_book,
			galves: cards.galves,
			nada: cards.nada
		}
	},
	vhr: {
		title: "Vehicle History Report",
		default_view: "carfax",
		tabs: ["carfax", "autocheck"],
		pattern: {
			broker_id: _.isString
		},
		view: {
			carfax_order: {
				pattern: {
					vin: _.isString
				}
			},
			carfax_overview: {
				pattern: {
					vin: _.isString,
					report: {
						status: _.isNumber
					}
				}
			},
			autocheck_order: {
				pattern: {
					vin: _.isString
				}
			},
			autocheck_overview: {
				pattern: {
					vin: _.isString,
					report: {
						status: _.isNumber
					}
				}
			}
		}
	},
	vin_info: {
		title: "Vin Info",
		default_view: "overview",
		// Should be a Dealer/User Preference
		tabs: ["black_book", "kelly_blue_book", "galves", "nada"],
        pattern: {
            broker_id: _.isString,
            vin: _.isString
        },
		/// Configurable, PM
		view: { /// Visibility/Order Needs to be a preference, PM
			overview: {},
			books: {
				views: cards.books
			},
			vhr: {
				views: cards.vhr
			},
			auction: cards.naaa
		}
	},
	black_book: {
		title: "BlackBook",
		pattern: {
			broker_id: _.isString
		},
		view: {
			select_uid: { /// Could be generalized for all the books, PM
				pattern: {
					uid: _.isEmpty
				},
				SelectionList: {
					type: "List",
					selectable: true,
					text_accessor: "Name",
					data_accessor: "Id",
					pattern: {
						Name: _.isString,
						Id: _.Id,
						Type: _
					},
					raises: ["item_selected", "year_selected", "make_selected", "model_selected"],
					listens_to: ["year_selected", "make_selected", "model_changed"],
					binds: {
						"item_selected": {
							"year_selected": {
								"Type": "Year"
							},
							"make_selected": {
								"Type": "Make"
							},
							"model_selected": {
								"Type": "Model"
							}
						}
					}
				}
			},
			values: {
				pattern: {
					uid: _.isNumber
				},
				DetailsForm: {
					type: "Fieldset"
				},
				courosel: ["RetailTable", "WholesaleTable", "Trade-InTable"],
				// Order should be configurable, PM
				RetailTable: {
					type: "Table"
				},
				WholesaleTable: {
					type: "Table"
				},
				TradeInTable: {
					type: "Table"
				},
				Options: {
					type: "List"
				}
			},
			preferences: {
				type: "Fieldset",
				title: "BlackBook Preferences",
				preventsSubmit: true,
				legend: "Black Book Preferences",
				input_accessor: _,
				button_accessor: _,
				label_accessor: _.keys,
				label: {
					MarketOrder: "Market Order"
				},
				pattern: _,
				raises: ["before_submit", "submitted", "blackbook_preferences"]
			}
		}
	},
	kelly_blue_book: {
		title: "Kelly Blue Book",
		pattern: {
			broker_id: _.isString
		},
		view: {
			select_uid: {
				pattern: {
					uid: _.isEmpty
				},
				SelectionList: {
					type: "List",
					selectable: true,
					text_accessor: "Name",
					data_accessor: "Id",
					pattern: {
						"Name": _.isString,
						"Id": _.Id,
						"Type": _
					},
					raises: ["item_selected", "year_selected", "make_selected", "model_selected"],
					listens_to: ["year_selected", "make_selected", "model_selected"],
					binds: {
						"item_selected": {
							"year_selected": {
								"Type": "Year"
							},
							"make_selected": {
								"Type": "Make"
							},
							"model_selected": {
								"Type": "Model"
							}
						}
					}
				}
			},
			values: {
				pattern: {
					uid: _.isNumber
				},
				DetailsForm: {
					type: "Fieldset"
				},
				courosel: ["WholesaleLendingTable", "SuggestedRetailTable", "Trade-InTable", "PrivatePartyTable", "AuctionTable"],
				// Order should be configurable, PM
				WholesaleLendingTable: {
					type: "Table"
				},
				SuggestedRetailTable: {
					type: "Table"
				},
				TradeInTable: {
					type: "Table"
				},
				PrivatePartyTable: {
					type: "Table"
				},
				AuctionTable: {
					type: "Table"
				},
				KnownOptions: {
					type: "fieldset"
				},
				Options: {
					type: "List"
				}
			}
		}
	},
	galves: {
		title: "Galves",
		pattern: {
			broker_id: _.isString
		},
		view: {
			select_uid: {
				pattern: {
					uid: _.isEmpty
				},
				SelectionList: {
					type: "List",
					selectable: true,
					text_accessor: "Name",
					data_accessor: "Id",
					pattern: {
						"Name": _.isString,
						"Id": _.Id,
						"Type": _
					},
					raises: ["item_selected", "year_selected", "make_selected", "model_selected"],
					listens_to: ["year_selected", "make_selected", "model_selected"],
					binds: {
						"item_selected": {
							"year_selected": {
								"Type": "Year"
							},
							"make_selected": {
								"Type": "Make"
							},
							"model_selected": {
								"Type": "Model"
							}
						}
					}
				}
			},
			values: {
				pattern: {
					uid: _.isNumber
				},
				DetailsForm: {
					type: "Fieldset"
				},
				courosel: ["TradeInTable", "RetailTable"],
				// Order should be configurable, PM
				TradeInTable: {
					type: "Table"
				},
				RetailTable: {
					type: "Table"
				},
				Options: {
					type: "List"
				}
			}
		}
	},
	nada: {
		title: "NADA",
		pattern: {
			broker_id: _.isString
		},
		view: {
			select_uid: {
				pattern: {
					uid: _.isEmpty
				},
				SelectionList: {
					type: "List",
					selectable: true,
					text_accessor: "Name",
					data_accessor: "Id",
					pattern: {
						"Name": _.isString,
						"Id": _.Id,
						"Type": _
					},
					raises: ["item_selected", "year_selected", "make_selected", "model_selected"],
					listens_to: ["year_selected", "make_selected", "model_selected"],
					binds: {
						"item_selected": {
							"year_selected": {
								"Type": "Year"
							},
							"make_selected": {
								"Type": "Make"
							},
							"model_selected": {
								"Type": "Model"
							}
						}
					}
				}
			},
			values: {
				pattern: {
					uid: _.isNumber
				},
				DetailsForm: {
					type: "Fieldset"
				},
				courosel: ["RetailTable", "WholesaleTable", "Trade-InTable"],
				// Order should be configurable, PM
				TradeInRoughTable: {
					type: "Table"
				},
				TradeInAverageTable: {
					type: "Table"
				},
				TradeInCleanTable: {
					type: "Table"
				},
				LoanTable: {
					type: "Table"
				},
				RetailTable: {
					type: "Table"
				},
				Options: {
					type: "List"
				}
			}
		}
	},
	scan: {
		title: "Scan a Vin",
		pattern: {
			broker_id: _.isString,
			vin: _.isEmpty
		},
		view: {
			redirect: "firstlook:vin/scan"
		}
	},
	enter_vin: {
		title: "Enter a Vin",
		pattern: {
			broker_id: _.isString,
			vin: _.isEmpty
		},
		view: {
			redirect_to: "firstlook:vin/enter"
		}
	},
	vehicles_list: {
		title: "Entered Vehicles",
		pattern: {
			broker_id: _.isString
		},
		view: {
			view_lists: {
				pattern: {
					fl_tag_id: _.isEmpty
				},
				raises: ["select_list"]
			},
			view_list: {
				pattern: {
					fl_tag_id: _.isString
				},
				raises: ["select_vin"]
			}
		}
	}
};

var wires = {
	priority: {
		dealers: 0,
		actions: 10,
		books: 20,
		scan: 30,
		enter_vin: 40,
		vehicels_list: 50,
		vin_info: 60
	},
    hash_state: {
        split: "/",
        action: '#/:action',
        broker: 'broker/:broker',
        vin: 'vin/:vin',
        fl_tag_id: 'tag/:fl_tag_id',
        fl_vehicle_id: "vid/:fl_vehicle_id",
    },
    dispatch: function(next) {
        var hash = "#/";
        function getAsString(v) {
            if (_.isString(v)) { 
                return v;
            };
            if (_.isFunction(v.toString)) {
                return getAsString(v.toString());
            }
            if (_.isFunction(v)) {
                return getAsString(v())
            }
            if (_.isArray(v)) {
                return v.join("");
            }
            return "";
        }
        if (_.isString(next.action)) {  };
    }
};
