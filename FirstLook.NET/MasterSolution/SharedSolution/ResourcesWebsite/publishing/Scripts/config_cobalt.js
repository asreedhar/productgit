
var MAXSEConfiguration = { 

    CustomCssClass:     "customCssClass",
    ElementImage:       "",
    ElementText:        "",
    OutputType:         "image",
    ShowNoPdfMessage:   false,
    VendorID:           "cobalt",
    VinElementId:       "vehicle:buy:vin"
    
};