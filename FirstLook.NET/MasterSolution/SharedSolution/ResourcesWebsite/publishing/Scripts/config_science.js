var MAXSEConfiguration = { 
  VinElementId:     "vehicle:buy:vin",
  VendorID:         "science",
  CustomCssClass:   "customCssClass",
  ElementImage:     "",
  ElementText:      "",
  OutputType:       "image",
  ShowNoPdfMessage: false
};

var MAXSEDynamic = {
  DynamicImagePath: "http://maxinsights.biz/good-deal-button/",
  cookie_get: null,
  cookie_set: null,
  experiment_group: null
};

// simple cookie API
MAXSEDynamic.cookie_get = function( name ) {
  if( !document.cookie || document.cookie === "" )
    return null;
  var cookie = {};
  var vals = document.cookie.split("; ");
  for( var i = 0; i < vals.length; ++i ) {
    var item = vals[i];
    var val = item.split( "=" );
    cookie[val[0]] = val[1];
  }
  return cookie[name];
}
MAXSEDynamic.cookie_set = function( name, value, life_days ) {
  var t = new Date();
  t.setDate( t.getDate() + life_days );
  document.cookie = name + "=" + escape(value) + (life_days ? "; expires="+t.toUTCString() : "" );
}

MAXSEDynamic.experiment_group = MAXSEDynamic.cookie_get( "A/B?" );
if( !MAXSEDynamic.experiment_group ) {
	var r = (Math.floor(Math.random()*2)==1);
	MAXSEDynamic.experiment_group = r? "A":"B";
	MAXSEDynamic.cookie_set( "A/B?", MAXSEDynamic.experiment_group, 1 );
}
if(        MAXSEDynamic.experiment_group == "A" ) {
	MAXSEConfiguration.ElementImage = MAXSEDynamic.DynamicImagePath + "A.png";
} else if( MAXSEDynamic.experiment_group == "B" ) {
	MAXSEConfiguration.ElementImage = MAXSEDynamic.DynamicImagePath + "B.png";
}


// Standard Google Analytics Tracking Code
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-46320630-1');
ga('send', 'pageview');
ga('send', 'event', MAXSEDynamic.experiment_group, 'loaded');

// Install Click Event after it is found, unless a full 5 minutes passes and no button is found
MAXSEDynamic.InstallationAttemptElapsedTime = 0;
MAXSEDynamic.ClickEventInstallerHandle = setInterval( function() {
  var ph = document.getElementById('MaxSeLinkPlaceholder');
  if( ph ) {
    var img = ph.getElementsByTagName('img');
    if( img.length > 0 ) {
      img = img[0];
      img.onclick = function() {
        ga('send','event',MAXSEDynamic.experiment_group,'clicked'); // found img; done
      }
      clearInterval( MAXSEDynamic.ClickEventInstallerHandle );
    }
  }
  MAXSEDynamic.InstallationAttemptElapsedTime += 1000;
  if( MAXSEDynamic.InstallationAttemptElapsedTime >= (5 * 60 * 1000))
    clearInterval( MAXSEDynamic.ClickEventInstallerHandle ); // timeout (failure); done
}, 1000 );

