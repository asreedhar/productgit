﻿
The MAX SE Publishing API is served by the Default.aspx page in this folder.

This was done in order to leverage the configuration files in order to insert the appropriate (environment specific) domain names in to the script file.

The Scripts folder contains the individual API configuration script files for specific website vendors.

More information can be found in the documentation:
	[REPOSITORY]\FirstLook.NET\Documentation\MAX for Selling and Email\MAX for Selling and Email Third Party Publishing API.docx