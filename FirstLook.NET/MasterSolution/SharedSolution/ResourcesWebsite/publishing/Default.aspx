﻿<%@ Page Language="C#" %>

/*
    --------------------------------------------------------------------------------
    MAXSEPublishing.js - 1/28/2011 - Kevin Boucher
    --------------------------------------------------------------------------------
     - Writes a link to a MAX SE PDF on a dealer's webpage.
     - Dealer's page will need to reference this script and a configuration script.
     - Complete documentation in "MAX For Selling PDF Website Integration.docx".
    --------------------------------------------------------------------------------
    
    MAX Selling & Email PDF Link Configuration
    --------------------------------------------------------------------------------
    VinElementId:       (Required) ID attribute value of input or single node
    
    CustomCssClass:     (Optional) custom css class name to be added to link/button element [default: 'maxSePdfLink']
    ElementImage:       (Optional) custom image path to be used for button [default: [ENVIRONMENT]/resources/publishing/images/btn_GoodDeal.png]
    ElementText:        (Optional) text to be displayed in link or button [default: 'View Selling Sheet']
    OutputType:         (Optional) 'link' or 'button' [default: 'link']
    ShowNoPdfMessage:   (Optional) [default: true]
    VendorID:           (Optional) ID of website vendor for internal tracking (omitted in inline script configuration)
    
    
    EXAMPLE: With minimum required paramaters
    --------------------------------------------------------------------------------
    var MAXSEConfiguration = { VinElementId: "[ID or Name of element containing VIN]" };
    
        
    EXAMPLE: With all paramaters
    --------------------------------------------------------------------------------
    var MAXSEConfiguration = {
    
        VinElementId:       "[ID or Name of element containing VIN]",
        
        CustomCssClass:     "[custom Css class]",   [optional]
        ElementImage:       "[custom image url]",   [optional]
        ElementText:        "[custom CTA text]",    [optional]
        OutputType:         "[image/link/text]"     [optional]
        ShowNoPdfMessage:   [true/false]            [optional]
        VendorID:           "[vendorID]"            [optional and omitted from inline configuration]
        
    };
    
*/

var MAXSEPublishing = function() {
    
    /// Setup action element types
    this.ElementTypes =              ["link", "button", "image"];
    
    /// Setup default values
    this.DefaultCssClass =           "maxSePdfLink";
    this.DefaultElementImage =       "<%= ConfigurationManager.AppSettings["EnvironmentDomain"] %>" + "resources/publishing/images/btn_GoodDeal.png";
    this.DefaultElementText =        "Is this a good deal? CLICK TO FIND OUT";
    this.DefaultElementType =        this.ElementTypes.link;

    /// Setup error strings
    this.ErrorPrefix =               "MAX for Selling & Email error: ";
    this.Errors = { config:          "MAXSEConfiguration object not found.",
                    service:         "We are unable to access the  consumer value analysis report for this vehicle. Please contact the dealer directly for more information.",
                    vinElementId:    "Required property 'VinElementId' not found.",
                    vinElement:      "VIN element not found.",
                    vin:             "VIN not found" };
                    
    this.HasRun = false;
                           
    /// Setup web service references
    this.WebServiceDomain = "<%= ConfigurationManager.AppSettings["WebServiceDomain"]%>";
    this.PdfWebService = "<%= ConfigurationManager.AppSettings["GetPdfWebService"]%>";
    
};

MAXSEPublishing.prototype = {

    /// Adds event handler to window onload event
    AddLoadEvent: function ( func ) {
    
        var oldOnLoad = window.onload;
        
        if (typeof window.onload !== 'function') {
            window.onload = func;
        } else {
            
            window.onload = function() {
                if ( oldOnLoad ) {
                    oldOnLoad();
                }
                func();
            };
            
        }
        
    },
    
    /// Get VIN from DOM via configuration object settings
    GetVin: function( options ) {
    
        var vinElm, vin;
        
        // Validate required properties
        if (options.VinElementId === null || options.VinElementId === "") { 
            throw new Error(this.ErrorPrefix + this.Errors.vinElementId);
        }
        
        // NOTE: If element is identified by 'name' it must be first instance of an element with this name
        vinElm = document.getElementById(options.VinElementId) || document.getElementsByName(options.VinElementId)[0];
        
        // Verify VIN element is present
        if (!vinElm) { throw new Error(this.ErrorPrefix + this.Errors.vinElement); }

        // Get VIN element value
        vin = vinElm.value ? vinElm.value : vinElm.innerHTML;

        // Verify VIN is present
        if (!vin || vin === "") { throw new Error(this.ErrorPrefix + this.Errors.vin); }
        
        return vin;
        
    },
    
    /// Outputs markup for link/button on window load
    OnReady: function(e) {
        
        var options;
        var page = MAXPublishPage;
        
        // Make sure we have configuration object
        try {
            options = MAXSEConfiguration;
        } catch(error) {
            throw new Error(page.ErrorPrefix + page.Errors.config);
        }

        // The web service call
        var request  = page.WebServiceDomain + page.PdfWebService + page.GetVin( options )
                     + (options.VendorID !== null && options.VendorID !== "" ? "&vid=" + options.VendorID : "");
        
        // Create a new request object
        scriptRequest = new JSONScriptRequest(request); 
        
        // Build the dynamic script tag
        scriptRequest.buildScriptTag(); 
        
        // Add the script tag to the page
        scriptRequest.addScriptTag();
        
        // Writes error message if service does not respond
        setTimeout("MAXPublishPage.WriteError()", 1000);
        
    },
    
    WriteError: function() {
        
        if (!this.HasRun && MAXSEConfiguration.ShowNoPdfMessage !== false) { document.getElementById("MaxSeLinkPlaceholder").innerHTML = this.Errors.service; }
        
    },
    
    WritePdfLink: function( url ) {
        
        // This function only called if a response is returned from the web service
        this.HasRun = true;
    
        var cssClass, elementText, imageUrl, linkHtml, options, outputType;
        
        // Make sure we have configuration object
        try {
            options = MAXSEConfiguration;
        } catch(error) {
            throw new Error(this.ErrorPrefix + this.Errors.config);
        }
        
        // Setup local variables
        cssClass = this.DefaultCssClass + ( options.CustomCssClass ? " " + options.CustomCssClass : "" );
        elementText = ( options.ElementText === null || options.ElementText === "" ) ? this.DefaultElementText : options.ElementText;
        imageUrl = ( options.ElementImage === null || options.ElementImage === "" ) ? this.DefaultElementImage : options.ElementImage;
        outputType = ( options.OutputType === null || options.OutputType === "" ) ? this.DefaultElementType : options.OutputType;
        
        // If PDF URL is not found return empty string
        if (!url || url === "") { return ""; }
        
        // Assign relevant markup
        switch (outputType) {
        
            case "button":
                linkHtml = "<button value=\"" + url + "\" onclick=\"window.open(this.value); return false;\" class=\"" + cssClass + "\">" + elementText + "</button>";
                break;
                
            case "image":
                linkHtml = "<a href=\"" + url + "\" class=\"" + cssClass + "\" target=\"_blank\"><img border=\"0\" src=\"" + imageUrl + "\" alt=\"" + elementText + "\" /></a>";
                break;
                
            case "link":
                linkHtml = "<a href=\"" + url + "\" class=\"" + cssClass + "\" target=\"_blank\">" + elementText + "</a>";
                break;
        
        }
        
        document.getElementById("MaxSeLinkPlaceholder").innerHTML = linkHtml;
    
    }
    
};

/// ----------------------------------
/// JSON Script Request implementation
/// ----------------------------------

function JSONScriptRequest(fullUrl) {

    this.fullUrl = fullUrl; 
    this.noCacheIE = '&noCacheIE=' + (new Date()).getTime();
    this.headLoc = document.getElementsByTagName("head").item(0);
    this.scriptId = 'JscriptId' + JSONScriptRequest.scriptCounter++;
    
}

JSONScriptRequest.scriptCounter = 1;

JSONScriptRequest.prototype = {

    buildScriptTag: function () {

        this.scriptObj = document.createElement("script");
        
        this.scriptObj.setAttribute("type", "text/javascript");
        this.scriptObj.setAttribute("charset", "utf-8");
        this.scriptObj.setAttribute("src", this.fullUrl + this.noCacheIE);
        this.scriptObj.setAttribute("id", this.scriptId);
        
    },
    
    removeScriptTag: function () {

        this.headLoc.removeChild(this.scriptObj);  
    
    },
    
    addScriptTag: function () {

        this.headLoc.appendChild(this.scriptObj);
    
    }

};

/// ----------------------------------
/// Setup page load actions
/// ----------------------------------

/// Add placeholder element (PDF link will be written to this element)
document.write("<span id=\"MaxSeLinkPlaceholder\">&nbsp;</span>");

/// Instantiate publishing object and add window.onload event handler
var MAXPublishPage = new MAXSEPublishing();
MAXPublishPage.AddLoadEvent( MAXPublishPage.OnReady );

