﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;

namespace App_Code
{
    /// <summary>
    /// This class is build to avoid DLL hell of requests for static files for MicrsoftAjax library
    /// The issue arises from Ajax.NET being bundled with .NET which means something as simple as a
    /// Windows Update could cause build version changes and force us to update production. This class
    /// allows us to always serve a know version of JS to clients from a central cached resource.
    /// 
    /// -- Paul Monson
    /// </summary>   
    public class MicrosoftAjaxStaticServer : IHttpHandler
    {
        /// <summary>
        /// Handle Each Request for Static Resources and test if it is a Microsoft Javascript Versioned Library
        /// If it is Rewrite the output as the specified version, PM
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            HttpRequest request = context.Request;
            HttpResponse response = context.Response;

            if (IsMicrosoftVersionedResource(request) && !File.Exists(request.PhysicalPath))
            {
                Uri mappedRequest = ReplaceBuildAndVersion(request);
               
                context.RewritePath(mappedRequest.LocalPath);
            }

            if (File.Exists(request.PhysicalPath))
            {
                response.WriteFile(request.PhysicalPath);
                response.Cache.SetCacheability(HttpCacheability.ServerAndPrivate);                
                response.Cache.SetExpires(DateTime.Today.AddDays(5));                
                response.Cache.SetETagFromFileDependencies();
                response.Cache.SetLastModifiedFromFileDependencies();
                response.End();
            }
        }

        public bool IsReusable
        {
            get { return true; }
        }        

        /// <summary>
        /// Checks if the request is in a Microsoft Versioned Library
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private static bool IsMicrosoftVersionedResource(HttpRequest request)
        {
            return MicrosoftVersionedResourceIndex(request) != -1;
        }

        /// <summary>
        /// Return the index of a folder "/" delimited request path of a known Microsoft Versioned Library Folder
        /// if the no know folders are found return -1
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private static int MicrosoftVersionedResourceIndex(HttpRequest request)
        {
            string[] segments = request.Url.Segments;
            IList<string> paths = StaticResource.VersionedResource;

            // Because the two segments after a known folder are requered the last two folders are not viable
            // for a Microsoft Javascript Versioned folder. This also alows safe calling of the 
            // ReplaceBuildAndVersion function with out continously checking the length of the segments.
            for (int i = 0; i < segments.Length - StaticResource.VersionedFolderDepth; i++)
            {
                if (paths.Contains(segments[i]))
                {
                    return i;
                }
            }

            return -1;
        }

        /// <summary>
        /// Replace the two segments after the well known folder with the Microsoft Version Name
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private static Uri ReplaceBuildAndVersion(HttpRequest request)
        {
            int idx = MicrosoftVersionedResourceIndex(request);
            Uri uri = request.Url;
            UriBuilder builder = new UriBuilder(uri);
            IList<string> segments = uri.Segments;

            if (segments.Count > idx + StaticResource.VersionedFolderDepth)
            {
                segments[++idx] = StaticResource.VersionToServe;
                segments[++idx] = StaticResource.BuildToServe; 
            }

            builder.Path = string.Empty;
            for (int i = 1; i < segments.Count; i++)
            {
                builder.Path += segments[i];
            }

            return builder.Uri;
        }

        /// <summary>
        /// Static Values used in this hacky class
        /// </summary>
        private static class StaticResource
        {
            public const int VersionedFolderDepth = 2;
            public const string VersionToServe = "4.0.0.0/";
            public const string BuildToServe = "4.0.30319.206/";
            public static readonly string[] VersionedResource = {
                                                                    "System.Web/",
                                                                    "System.Web.Extensions/"
                                                                };
        }
    }
}