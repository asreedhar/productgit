using System;
using System.IO;
using System.IO.Compression;
using System.Web;

namespace App_Code
{
    public class CompressionModule : IHttpModule
    {
        #region IHttpModule Members

        void IHttpModule.Dispose()
        {
            // nothing to dispose
        }

        void IHttpModule.Init(HttpApplication context)
        {
            context.PreRequestHandlerExecute += PreRequestHandlerExecute;
        }

        private const string GZIP = "gzip";
        private const string DEFLATE = "deflate";

        private static void PreRequestHandlerExecute(object sender, EventArgs e)
        {
            HttpApplication application = (HttpApplication)sender;

            HttpContext context = application.Context;

            if (IsCompressionEnabled(application.Context))
            {
                HttpRequest request = application.Request;

                FileInfo info = new FileInfo(application.Server.MapPath(request.CurrentExecutionFilePath));

                if (info.Exists)
                {
                    foreach (string value in new string[] {".js", ".css"})
                    {
                        if (string.Compare(value, info.Extension, true) == 0)
                        {
                            HttpResponse response = application.Response;

                            Stream filter = response.Filter;

                            if (IsEncodingAccepted(context, GZIP))
                            {
                                response.Filter = new GZipStream(filter, CompressionMode.Compress);
                                SetEncoding(context, GZIP);
                            }
                            else if (IsEncodingAccepted(context, DEFLATE))
                            {
                                response.Filter = new DeflateStream(filter, CompressionMode.Compress);
                                SetEncoding(context, DEFLATE);
                            }
                        }
                    }
                }
            }
        }

        private static bool IsEncodingAccepted(HttpContext context, string encoding)
        {
            string acceptEncoding = context.Request.Headers["Accept-Encoding"];
            if (string.IsNullOrEmpty(acceptEncoding))
                return false;
            return acceptEncoding.Contains(encoding);
        }

        private static void SetEncoding(HttpContext context, string encoding)
        {
            context.Response.AppendHeader("Content-Encoding", encoding);
        }

        private static bool IsCompressionEnabled(HttpContext context)
        {
            if ((context != null) && context.Request.Browser.IsBrowser("IE"))
            {
                return (context.Request.Browser.MajorVersion > 6);
            }
            return true;
        }

        #endregion
    }
}