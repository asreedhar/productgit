using System;
using System.Web;
using FirstLook.DomainModel.Oltp;

public partial class _Default : System.Web.UI.Page 
{
    /// <summary>
    /// Route to the correct destination on page load.
    /// </summary>    
    protected void Page_Load(object sender, EventArgs e)
    {
        string pageName = Request.QueryString["pageName"];

        // In-Transit Inventory.
        if (!string.IsNullOrEmpty(pageName) && pageName.Contains("Pages/Inventory"))
        {
            string url = "~/Pages/Inventory/InTransitInventory.aspx?";

            string identity = HttpContext.Current.User.Identity.Name;
            SoftwareSystemComponentState state = SoftwareSystemComponentStateFacade.
                FindOrCreate(identity, SoftwareSystemComponentStateFacade.DealerComponentToken);

            // Business unit id.
            string dealerId = state.Dealer.GetValue().Id.ToString();
            url += "bu=" + dealerId;

            // Appraisal id.
            string appraisalId = Request.QueryString["appraisalId"];
            if (!string.IsNullOrEmpty(appraisalId))
            {
                url += "&ap=" + appraisalId;
            }

            Response.Redirect(url, true);
        }    
    }
}
