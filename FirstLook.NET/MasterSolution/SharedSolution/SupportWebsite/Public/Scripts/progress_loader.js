﻿function add_loader() {
	function ShowAJAXProgress() {
	    document.getElementById("progress").style.display = 'block';
	}
	
	function HideAJAXProgress() {
	    document.getElementById("progress").style.display = 'none';
	}
	
	Sys.Net.WebRequestManager.add_invokingRequest(ShowAJAXProgress);
    Sys.Net.WebRequestManager.add_completedRequest(HideAJAXProgress);
}

if (window.attachEvent) {
	window.attachEvent('onload', add_loader);
} else if (el.addEventListener) {
    window.addEventListener('onload', add_loader, false);
}