if (typeof Application !== "function") {
    var Application = function() {};
}

(function() {
    var wires = {
        broker_id: "broker/:broker_id",
        state: "state/:state",
        actuality: "act/:actuality",
        action: "action/:action",
        vin: "vin/:vin",
        vuid: "vuid/:vuid"
    };
    var State = {
        kbb: {
            Regions: []
        }
    }

    FirstLook.Session.wire(wires);

    // Client State
    function client_wireup() {
        $(Clients).bind("Clients.BrokerLoaded", function(evt, data) {
            FirstLook.Session.set_states({
                "broker_id": data.Broker.Handle || null,
                "state": data.Broker.Client.Address.State.toUpperCase() || null
            });
        });
    }

    function vehicle_wireup() {
        $(Vehicles).bind("Vehicles.VehicleSelected", function(evt, data) {
            var new_states = {
                "vin": data["Identification.Vin"] || null
            };
            $(Vehicles).trigger("Vehicles.VinChange", new_states.vin || null);
        });
        $(Vehicles).bind("Vehicles.VehicleLoaded", function(evt, data) {
            FirstLook.Session.set_states({
                "vin": data.Arguments.Vin,
                "vuid": data.Vehicle
            });
            var card = FirstLook.cards.get("Vin");
            if (card.can_render()) {
                _.delay(function() {
                    card.views[0].title.build(data);
                    $(Vehicles).trigger("Vehicles.BrokerTags");
                    $(Vehicles).trigger("Vehicles.VehicleTags", [data.Vehicle]);
                },
                700);
            }
        });
        $(Vehicles).bind("Vehicles.TagsLoaded", function(evt, data) {
            var card = FirstLook.cards.get("Overview");
            if (card.can_render()) {
                card.views.overview.Tags.build(data);
            }
        });
        $(Vehicles).bind("Vehicles.Back", function(evt, data) {
            $(Application).trigger("ResetAction");
        })
    }

    function bb_wireup() {
        function load_bb_values(evt, data) {
            var card = FirstLook.cards.get("Overview");
            if (card.can_render()) {
                card.views.overview.BlackBook.build(data);
            }
        }
        function load_bb_message(evt, data) {
            var card = FirstLook.cards.get("Overview");
            if (card.can_render()) {
                card.views.overview.BlackBookMessage.build(data);
            }
        }
        $(BlackBook).bind({
            "BlackBook.InitialValueLoaded": load_bb_values,
            "BlackBook.SavePublication": function(evt, data) {
                load_bb_message.apply(this, [evt, {Message: "Black Book Saving..."}]);
            },
            "BlackBook.PublicationSaved": function(evt, data) {
                load_bb_message.apply(this, [evt, {Message: "Black Book Saved"}]);
            },
            "BlackBook.PublicationLoaded": function(evt, data) {
                if (data.Publication.VehicleConfiguration) load_bb_values.apply(this, [evt, data.Publication]);
            },
            "BlackBook.AdjustedValueLoaded": load_bb_values,
            "BlackBook.TraversalLoaded": load_bb_values,
            "BlackBook.MileageOrStateChange": function(evt, data) {
                $(Application).trigger("Application.ChangeMileageOrState", [data, "BlackBook"]);
            },
            "BlackBook.ErrorState": load_bb_values
        });
    }

    function kbb_wireup() {
        function load_kbb_values(evt, data) {
            var card = FirstLook.cards.get("Overview");
            if (card.can_render()) {
                card.views.overview.KelleyBlueBook.build(data);
            }
        }

        function load_kbb_message(evt, data) {
            var card = FirstLook.cards.get("Overview");
            if (card.can_render()) {
                card.views.overview.KelleyBlueBookMessage.build(data);
            }
        }

        $(KelleyBlueBook).bind({
            "KelleyBlueBook.InitialValueLoaded": load_kbb_values,
            "KelleyBlueBook.PublicationSave": function(evt, data) {
                load_kbb_message.apply(this, [evt, {Message: "Kelley Blue Book Saving..."}]);
            },
            "KelleyBlueBook.PublicationSaved": function(evt, data) {
                load_kbb_message.apply(this, [evt, {Message: "Kelley Blue Book Saved"}]);
            },
            "KelleyBlueBook.PublicationLoaded": function(evt, data) {
                if (data.Publication.VehicleConfiguration) load_kbb_values.apply(this, [evt, data.Publication]);
            },
            "KelleyBlueBook.AdjustedValueLoaded": load_kbb_values,
            "KelleyBlueBook.TraversalLoaded": load_kbb_values,
            "KelleyBlueBook.MileageOrStateChange": function(evt, data) {
                $(Application).trigger("Application.ChangeMileageOrState", [data, "KelleyBlueBook"]);
            },
            "KelleyBlueBook.ErrorState": load_kbb_values
        });
    }

    function edmunds_wireup() {
        function load_edmunds_values(evt, data) {
            var card = FirstLook.cards.get("Overview");
            if (card.can_render()) {
                card.views.overview.Edmunds.build(data);
            }
        }

        function load_edmunds_message(evt, data) {
            var card = FirstLook.cards.get("Overview");
            if (card.can_render()) {
                card.views.overview.EdmundsMessage.build(data);
            }
        }

        $(Edmunds).bind({
            "Edmunds.InitialValuationLoaded": load_edmunds_values,
            "Edmunds.PublicationLoaded": function(evt, data) {
                if (data.Publication.VehicleConfiguration) load_edmunds_values.apply(this, [evt, data.Publication]);
            },
            "Edmunds.PublicationSave": function(evt, data) {
                load_edmunds_message.apply(this, [evt, {Message: "Edmunds Saving..."}]);
            },
            "Edmunds.PublicationSaved": function(evt, data) {
                load_edmunds_message.apply(this, [evt, {Message: "Edmunds Saved"}]);
            },
            "Edmunds.AdjustedValueLoaded": load_edmunds_values,
            "Edmunds.TraversalLoaded": load_edmunds_values,
            "Edmunds.MileageOrStateChange": function(evt, data) {
                $(Application).trigger("Application.ChangeMileageOrState", [data, "Edmunds"]);
            },
            "Edmunds.ErrorState": load_edmunds_values
        });
    }

    function galves_wireup() {
        function load_galves_values(evt, data) {
            var card = FirstLook.cards.get("Overview");
            if (card.can_render()) {
                card.views.overview.Galves.build(data);
            }
        }

        function load_galves_message(evt, data) {
            var card = FirstLook.cards.get("Overview");
            if (card.can_render()) {
                card.views.overview.GalvesMessage.build(data);
            }
        }

        $(Galves).bind({
            "Galves.InitialValueLoaded": load_galves_values,
            "Galves.PublicationSave": function(evt, data) {
                load_galves_message.apply(this, [evt, {Message: "Galves Saving..."}]);
            },
            "Galves.PublicationSaved": function(evt, data) {
                load_galves_message.apply(this, [evt, {Message: "Galves Saved"}]);
            },
            "Galves.PublicationLoaded": function(evt, data) {
                if (data.Publication.VehicleConfiguration) load_galves_values.apply(this, [evt, data.Publication]);
            },
            "Galves.AdjustedValueLoaded": load_galves_values,
            "Galves.TraversalLoaded": load_galves_values,
            "Galves.MileageOrStateChange": function(evt, data) {
                $(Application).trigger("Application.ChangeMileageOrState", [data, "Galves"]);
            },
            "Galves.ErrorState": load_galves_values
        });
    }

    function nada_wireup() {
        function load_nada_values(evt, data) {
            var card = FirstLook.cards.get("Overview");
            if (card.can_render()) {
                card.views.overview.Nada.build(data);
            }
        }
        function load_nada_message(evt, data) {
            var card = FirstLook.cards.get("Overview");
            if (card.can_render()) {
                card.views.overview.NadaMessage.build(data);
            }
        }
        $(Nada).bind({
            "Nada.InitialValueLoaded": load_nada_values,
            "Nada.PublicationSave": function(evt, data) {
                load_nada_message.apply(this, [evt, {Message: "NADA Saving..."}]);
            },
            "Nada.PublicationSaved": function(evt, data) {
                load_nada_message.apply(this, [evt, {Message: "NADA Saved"}]);
            },
            "Nada.PublicationLoaded": function(evt, data) {
                if (data.Publication.VehicleConfiguration) load_nada_values.apply(this, [evt, data.Publication]);
            },
            "Nada.AdjustedValueLoaded": load_nada_values,
            "Nada.TraversalLoaded": load_nada_values,
            "Nada.MileageOrStateChange": function(evt, data) {
                $(Application).trigger("Application.ChangeMileageOrState", [data, "Nada"]);
            },
            "Nada.ErrorState": load_nada_values
        });
    }

    function naaa_wireup() {
        function load_naaa_values(evt, data) {
            var card = FirstLook.cards.get("Overview");
            if (card.can_render()) {
                card.views.overview.Naaa.build(data);
            }
        }

        $(Naaa).bind({
            "Naaa.ReportLoaded": load_naaa_values,
            "Nada.TraversalLoaded": load_naaa_values,
            "Nada.ErrorState": load_naaa_values
        });
    }

    function carfaxOverview_wireup() {
        $(Carfax).bind("Carfax.ReportLoaded", function(evt, data) {
            var card = FirstLook.cards.get("Overview");
                if (card.can_render()) {
                    card.views.overview.CarfaxOverview.build(data);
                }
        });
    }

    function autoCheckOverview_wireup() {
        $(AutoCheck).bind("AutoCheck.ReportLoaded", function(evt, data) {
            var card = FirstLook.cards.get("Overview");
                if (card.can_render()) {
                    card.views.overview.AutoCheckOverview.build(data);
                }
        });
    }

    var State = {};
    function get_state() {
        if (typeof this.instance_state !== "undefined") {
            return this.instance_state;
        } else {
            return State;
        }
    }
    function set_state(new_state) {
        if (typeof this.instance_state !== "undefined") {
            return this.instance_state = new_state;
        } else {
            return State = new_state;
        }
    }
    this.prototype.get_state = function() {
        return get_state.call(this);
    };
    this.prototype.set_state = function(new_state) {
        return set_state.call(this, new_state);
    };
    this.prototype.hash_change = function(evt, first_time) {
        var state = this.get_state(),
        new_state = FirstLook.Session.get_state();
        first_time = first_time || false;
        if (_.isEqual(state, new_state)) return;

        if (new_state.broker_id && state.broker_id !== new_state.broker_id) {
            $(Vehicles).trigger("Vehicles.BrokerChange", new_state.broker_id || null);
        }
        if (first_time && new_state.vin && state.vin !== new_state.vin && !! new_state.broker_id) {
            $(Vehicles).trigger("Vehicles.VinChange", new_state.vin || null);
            return;
        }

        var action_card = FirstLook.cards.get("Actions");
        var card, application, data;

        if (FirstLook.cards.get("Clients").can_render()) {
            var c = new Clients();
            c.main();
            FirstLook.templates.addToolbarBackEvent(FirstLook.back_button.get("Clients"));
            FirstLook.templates.addToolbarActionEvent(FirstLook.action_button.get("Clients"));
        } else if (FirstLook.cards.get("Vin").can_render()) {
            card = FirstLook.cards.get("Vin");
            card.views[0].clean();
            data = FirstLook.cards.get_card_data("Vin");
            card.views[0].build(data);
            FirstLook.templates.addToolbarBackEvent(FirstLook.back_button.get("Vin"));
            FirstLook.templates.addToolbarActionEvent(FirstLook.action_button.get("Vin"));
        } else if (action_card.can_render()) {
            action_card.views[0].clean();
            action_card.views[0].build(FirstLook.cards.get_card_data("Actions"));
            FirstLook.templates.addToolbarBackEvent(FirstLook.back_button.get("Actions"));
            FirstLook.templates.addToolbarActionEvent(FirstLook.action_button.get("Actions"));
        } else if (new_state.action === "s") {
            window.location.href = "firstlook:vin/scan";
        } else if (new_state.action === "e") {
            window.location.href = "firstlook:vin/enter";
        } else if (new_state.action === "r" && FirstLook.cards.get("Vehicles").can_render()) {
            application = new Vehicles();
            application.main({
                broker: new_state.broker_id,
                actuality: new_state.actuality
            });
            FirstLook.templates.addToolbarBackEvent(FirstLook.back_button.get("Vehicles"));
            FirstLook.templates.addToolbarActionEvent(FirstLook.action_button.get("Vehicles"));
        } else if (new_state.action === "m" && FirstLook.cards.get("ManualBooks").can_render()) {
            card = FirstLook.cards.get("ManualBooks");
            card.views[0].build(FirstLook.cards.get_card_data("ManualBooks"));
            FirstLook.templates.addToolbarBackEvent(FirstLook.back_button.get("ManualBooks"));
            FirstLook.templates.addToolbarActionEvent(FirstLook.action_button.get("ManualBooks"));
        } else {
            alert("Application is not in a valid state");
            FirstLook.Session.pop();
        }

        this.set_state(new_state);
    };

    this.prototype.setup_static_data = function() {
        FirstLook.cards.register_card_data("Actions", {
            Actions: [{
                Name: "Scan Vin",
                Id: "s"
            },
            {
                Name: "Enter Vin",
                Id: "e"
            },
            {
                Name: "Review Scanned Vins",
                Id: "r"
            },
            {
                Name: "Manual Books",
                Id: "m"
            }]
        });
        FirstLook.cards.register_card_data("ManualBooks", {
            keys: ["BlackBook", "KelleyBlueBook", "Galves", "Naaa", "Nada"],
            labels: ["BB", "KBB", "Galves", "NAAA", "NADA"],
            applications: [
            new BlackBook(), new KelleyBlueBook(), new Galves(), new Naaa(), new Nada()],
            application_options: function(type) {
                var _state = FirstLook.Session.get_state("state");
                var state;

                if (type instanceof Galves) {
                    state = _.detect(_.usStates(), function(s) { return s.value === _state }).name;
                } else {
                    state = _state;
                }

                return {
                    state: state
                }
            }
        });
        FirstLook.cards.register_card_data("Books", {
            keys: ["BlackBook", "KelleyBlueBook", "Galves", "Nada", "Edmunds"],
            labels: ["BB", "KBB", "Galves", "NADA", "TMV"],
            applications: [
            new BlackBook(), new KelleyBlueBook(), new Galves(), new Nada(), new Edmunds()],
            application_options: function(type) {
                var vin = FirstLook.Session.get_state("vin");
                var vuid = FirstLook.Session.get_state("vuid");
                var buid = FirstLook.Session.get_state("broker_id");
                var _state = FirstLook.Session.get_state("state");
                var state;

                if (type instanceof Galves) {
                    state = _.detect(_.usStates(), function(s) { return s.value === _state }).name;
                } else {
                    state = _state;
                }

                return {
                    vin: vin,
                    vuid: vuid,
                    buid: buid,
                    state: state
                };
            }
        });
        FirstLook.cards.register_card_data("Vhr", {
            keys: ["Carfax", "AutoCheck"],
            applications: [new Carfax(), new AutoCheck()],
            application_options: function(type) {
                var vin = FirstLook.Session.get_state("vin");
                var buid = FirstLook.Session.get_state("broker_id");
                return {
                    vin: vin,
                    broker: buid
                };
            }
        });
        FirstLook.cards.register_card_data("Vin", {
            keys: ["Overview", "Books", "Vhr", "Naaa"],
            labels: ["Overview", "Books", "VHR"],
            applications: [new Overview(), new Books(), new Vhr(), new Naaa()],
            sub_tabs: ["Books", "Vhr"],
            application_options: function(type) {
                return {
                    vin: FirstLook.Session.get_state("vin"),
                    state: FirstLook.Session.get_state("state")
                };
            }
        });
    };
    this.prototype.main = function(instance_state) {
        var state;
        // start_ga_events();
        log_events();

        this.setup_static_data();

        if (instance_state) {
            this.instance_state = instance_state;
        }

        client_wireup();
        vehicle_wireup();
        bb_wireup();
        kbb_wireup();
        edmunds_wireup();
        naaa_wireup();
        nada_wireup();
        galves_wireup();
        carfaxOverview_wireup();
        autoCheckOverview_wireup();

        $(window).bind("hashchange", $.proxy(this.hash_change, this));
        this.hash_change(null, true);
    };

    // Google Analytics Tie in, PM
    function track_event(evt) {
        _.defer(function() {
            _gaq.push(["_trackEvent", evt.type, evt.namespace]);
        });
    }

    function log_event(evt) {
        console.log([evt.type, evt.namespace], evt);
    }

    function map_events(func) {
        _.each([Clients, Vehicles, BlackBook, KelleyBlueBook, Galves, Naaa, Nada, Edmunds, Carfax, AutoCheck], function(app) {
            $(app).bind(app.raises.join(" "), this);
        }, func);
    }

    function log_events() {
        map_events(log_event);
    }

    function start_ga_events() {
        map_events(track_event);
    }

    var PublicEvents = {
        "Actions.Selected": function(evt, action) {
            FirstLook.Session.set_states({
                "action": action,
                "actuality": action === "r" ? "2": "1"
            });
        },
        "ResetClient": function(evt) {
            FirstLook.Session.set_states({
                broker_id: null,
                state: null
            });
        },
        "ResetAction": function(evt) {
            FirstLook.Session.set_states({
                actuality: null,
                action: null,
                vin: null,
                vuid: null
            });
            window.location.reload();
        },
        "ResetVehicle": function(evt) {
            FirstLook.Session.set_states({
                vin: null,
                vuid: null
            });
        },
        "SaveBooks": function() {
            var books = {
                "BlackBook": BlackBook,
                "KelleyBlueBook": KelleyBlueBook,
                "Galves": Galves,
                "Nada": Nada,
                "Edmunds": Edmunds
            };
            _.each(books, function(v, k) {
                $(v).trigger(k + ".SavePublication");
            });
        },
        "Application.ChangeMileageOrState": function(evt, data, exclude) {
            /// revise to only call one
            var mileage_apps = {
                "BlackBook": BlackBook,
                "KelleyBlueBook": KelleyBlueBook,
                "Galves": Galves,
                "Nada": Nada,
                "Naaa": Naaa,
                "Edmunds": Edmunds
            };
            _.each(mileage_apps, function(a, e) {
                if (e === exclude) {
                    return;
                }
                if (e !== "Galves") {
                    $(a).trigger(e + ".StateChange", data.State);
                } else {
                    var state = _.detect(_.usStates(), function(s) {
                        return s.value === data.State;
                    });
                    $(a).trigger(e + ".StateChange", state.name);
                }
                $(a).trigger(e + ".MileageChange", data.Mileage);
            });
            var card = FirstLook.cards.get("Overview");
            card.views.overview.fieldset.$el.find("select[name=State]").val(data.State);
            card.views.overview.fieldset.$el.find("input[name=Mileage]").val(data.Mileage);
        }
    };
    $(Application).bind(PublicEvents);
}).apply(Application);

(function generateApplicationCards() {
    var actions = {
        title: "Actions",
        pattern: {
            broker_id: _.isString,
            action: _.isEmpty
        },
        view: [{
            ActionList: {
                type: "List",
                pattern: {},
                behaviors: {
                    selectable: {
                        pattern: {
                            Actions: [{
                                Id: _
                            }]
                        },
                        data_accessor: "Id",
                        binds: {
                            "item_selected": {
                                type: "Actions.Selected",
                                scope: Application
                            }
                        }
                    }
                },
                class_name: "select_list",
                item_accessor: "Actions",
                text_accessor: "Name",
                data_accessor: "Id"
            }
        }]
    };

    var books = {
        title: "Manual",
        pattern: _,
        view: [{
            Tabs: {
                type: "Tabs",
                pattern: {
                    keys: [_.isString]
                },
                class_name: "book_tabs",
                behaviors: {
                    tabable: {
                        pattern: {
                            applications: [{
                                main: _.isFunction
                            }]
                        },
                        application_accessor: "applications",
                        application_options_accessor: "application_options"
                    }
                }
            }
        }]
    };

    FirstLook.components.add({
        type: "Static",
        pattern: {
            Description: {
                ModelYear: _,
                ModelFamilyName: _
            },
            Arguments: {
                Vin: _.isVin
            }
        },
        template: function(data) {
            var temp = _.concat(["<p class='title'>", "<span class='year'>", data.Description.ModelYear, " </span>", "<span class='fam'>" + data.Description.ModelFamilyName, " </span>", "<span class='vin'>", _.shortVin(data.Arguments.Vin), " </span>", "</p>"]);
            return temp;
        }
    },
    "Vehicle.Identity");

    FirstLook.components.add({
        type: "Static",
        pattern: {
            Message: _.isString
        },
        template: function(data) {
            return _.concat(["<p class='vehicle_message'>",data.Message,"</p>"]);
        }
    }, "Vehicle.Message");

    var vin = {
        title: "Vin",
        pattern: {
            vin: _.isVin,
            vuid: _.isString
        },
        view: [{
            title: {
                type: "Placeholder",
                pattern: _,
                component: "Vehicle.Identity"
            },
            main: {
                type: "Tabs",
                pattern: [_.isString],
                class_name: "vin_overview",
                behaviors: {
                    tabable: {
                        pattern: {
                            applications: [{
                                main: _.isFunction
                            }]
                        },
                        application_accessor: "applications",
                        application_options_accessor: "application_options",
                        sub_tabs_accessor: "sub_tabs"
                    }
                }
            }
        }]
    };

    var overview = {
        title: "",
        pattern: {
            vin: _.isVin
        },
        view: {
            overview: {
                fieldset: {
                    type: "Fieldset",
                    pattern: {
                        region: _.isString,
                        regions: _.isArray
                    },
                    behaviors: {
                        data_mergable: {
                            data_destination_accessor: "inputs"
                        },
                        submitable: {
                            event: "Application.ChangeMileageOrState",
                            scope: Application,
                            submit_on_change: true
                        }
                    },
                    class_name: "book_fieldset",
                    legend_accessor: "legend",
                    input_accessor: "inputs",
                    name_accessor: "name",
                    label_accessor: "label",
                    value_accessor: "data",
                    button_accessor: "button",
                    type_accessor: "type",
                    static_data: {
                        legend: "Overview",
                        inputs: [{
                            name: "State",
                            label: "Region",
                            data: "region",
                            options: "regions"
                        },
                        {
                            name: "Mileage",
                            label: "Mileage",
                            data: "mileage",
                            value: 0,
                            type: "tel"
                        }],
                        button: ["Update"]
                    }
                },
                sep: {
                    type: "Static",
                    pattern: _,
                    template: function() {
                        return "<hr />";
                    }
                },
                BlackBook: {
                    type: "Cardholder",
                    pattern: _,
                    card: "BlackBookSummary"
                },
                BlackBookMessage: {
                    type: "Placeholder",
                    pattern: _,
                    component: "Vehicle.Message"
                },
                KelleyBlueBook: {
                    type: "Cardholder",
                    pattern: _,
                    card: "KelleyBlueBookSummary"
                },
                KelleyBlueBookMessage: {
                    type: "Placeholder",
                    pattern: _,
                    component: "Vehicle.Message"
                },
                Galves: {
                    type: "Cardholder",
                    pattern: _,
                    card: "GalvesSummary"
                },
                GalvesMessage: {
                    type: "Placeholder",
                    pattern: _,
                    component: "Vehicle.Message"
                },
                Edmunds: {
                    type: "Cardholder",
                    pattern: _,
                    card: "EdmundsSummary"
                },
                EdmundsMessage: {
                    type: "Placeholder",
                    pattern: _,
                    component: "Vehicle.Message"
                },
                Nada: {
                    type: "Cardholder",
                    pattern: _,
                    card: "NadaSummary"
                },
                NadaMessage: {
                    type: "Placeholder",
                    pattern: _,
                    component: "Vehicle.Message"
                },
                Naaa: {
                    type: "Cardholder",
                    pattern: _,
                    card: "NaaaSummary"
                },
                TagsSep: {
                    type: "Static",
                    pattern: _,
                    template: function() {
                        return "<hr />";
                    }
                },
                Tags: {
                    type: "Cardholder",
                    pattern: _,
                    card: "Tags"
                },
                VHRSep: {
                    type: "Static",
                    pattern: _,
                    template: function() {
                        return "<hr />";
                    }
                },
                CarfaxOverview: {
                    type: "Cardholder",
                    pattern: _,
                    card: "CarfaxOverview"
                },
                AutoCheckOverview: {
                    type: "Cardholder",
                    pattern: _,
                    card: "AutoCheckOverview"
                }
            }
        }
    };

    var vhr = {
        title: "Vhr",
        pattern: _,
        view: [{
            main: {
                type: "Tabs",
                pattern: {
                    keys: [_.isString]
                },
                class_name: "vhr_tabs",
                behaviors: {
                    tabable: {
                        pattern: {
                            applications: [{
                                main: _.isFunction
                            }]
                        },
                        application_accessor: "applications",
                        application_options_accessor: "application_options"
                    }
                }
            }
        }]
    };

    FirstLook.cards.add(actions, "Actions");
    FirstLook.cards.add(books, "ManualBooks");
    FirstLook.cards.add(books, "Books");
    FirstLook.cards.add(overview, "Overview");
    FirstLook.cards.add(vin, "Vin");
    FirstLook.cards.add(vhr, "Vhr");

    FirstLook.back_button.add({
        text: "&#8678;",
        event: "ResetClient",
        scope: Application
    },
    "Actions")

    FirstLook.back_button.add({
        text: "&#8678;",
        event: "ResetAction",
        scope: Application
    },
    "ManualBooks");

    FirstLook.back_button.add({
        text: "&#8678;",
        event: "ResetAction",
        scope: Application
    },
    "Vehicles");

    FirstLook.back_button.add({
        text: "&#8678;",
        event: "ResetAction",
        scope: Application
    },
    "Vin");

    FirstLook.back_button.add({
        text: "&#8678;",
        event: "Clients.HideQuery",
        scope: Clients
    },
    "Clients");

    FirstLook.action_button.add({
        text: "Search",
        event: "Clients.ShowQuery",
        scope: Clients
    },
    "Clients");

    FirstLook.action_button.add({
        text: "Save",
        event: "SaveBooks",
        scope: Application
    },
    "Vin")
})();

/// Overview Application Code for the Oveview Card
var Overview = function() {};
Overview.prototype.main = function(opts) {
    var card = FirstLook.cards.get("Overview");
    card.views.overview.clean();
    opts.region = opts.state;
    opts.regions = _.usStates();
    card.views.overview.build(opts);
};

/// Books Application Code for the Vin Card
var Books = function() {};
Books.prototype.main = function(opts) {
    var card = FirstLook.cards.get("Books");
    card.views[0].clean();
    card.views[0].build(_.extend(FirstLook.cards.get_card_data("Books"), opts));
};
var Vhr = function() {};
Vhr.prototype.main = function(opts) {
    var card = FirstLook.cards.get("Vhr");
    card.views[0].clean();
    card.views[0].build(_.extend(FirstLook.cards.get_card_data("Vhr"), opts));
};

if (applicationCache) {
    function updateMe(a) {
        clearProgress();
        if (applicationCache.status == applicationCache.UPDATEREADY) {
            applicationCache.swapCache();
            if (window.confirm("This App is ready to update. Would you like to update now?")) {
                window.location.reload();
            }
        }
    }
    function progressReport() {
        var el = document.getElementById("progress"),
        text;
        if (!el) {
            el = document.createElement("span");
            el.setAttribute("id", "progress");
            document.body.appendChild(el);
        }
        text = el.innerText;
        if (!text.length) {
            text = "Loading ";
        }
        el.innerText = text + "|";
    }

    function clearProgress() {
        var el = document.getElementById("progress");
        if (el) el.style.display = "none";
    }
    _.delay(clearProgress, 2000);

    applicationCache.addEventListener('updateready', updateMe, false);
    applicationCache.addEventListener('cached', clearProgress, false);
    applicationCache.addEventListener('noupdate', clearProgress, false);
    applicationCache.addEventListener('obsolete', clearProgress, false);
    // applicationCache.addEventListener('progress', progressReport, false);
    setInterval(function() {
        applicationCache.update();
    },
    3.6e6); /// Check every hour, PM
}

var _app = new Application();
$(document).ready(function() {
    if (sessionStorage && sessionStorage.getItem("hash")) {
        var values = _(sessionStorage).chain().keys().intersect(["broker_id", "state"]).reduce(function(m, v) {
            m[v] = sessionStorage.getItem(v);
            return m;
        },
        {}).value();

        FirstLook.Session.set_states(values);
    }
    _app.main();
});

