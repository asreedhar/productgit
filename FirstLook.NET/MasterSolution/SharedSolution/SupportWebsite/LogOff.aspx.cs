using System;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;

public partial class LogOff : Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		// Delete the ASP.NET Authentication Token

		Response.Cookies[FormsAuthentication.FormsCookieName].Expires = DateTime.Now.AddYears(-30);

		// Redirect to IMT Logout (which will send us onto CAS)

		string url = string.Format("{0}/IMT/LogoutAction.go", ConfigurationManager.AppSettings["edge_host_name"]);

		Response.Redirect(url, true);
	}
}
