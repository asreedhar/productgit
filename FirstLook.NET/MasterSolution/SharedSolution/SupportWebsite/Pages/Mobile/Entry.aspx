﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Entry.aspx.cs" Inherits="Pages_Mobile_Entry" %>

<!DOCTYPE html>

<html manifest="../../Public/Scripts/Pages/Mobile/cache.manifest">
<head runat="server">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta charset="utf-8"/>

    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="HandheldFriendly" content="yes" />

    <title>Mobile FirstLook</title>
    <link rel="stylesheet" href="/resources/Scripts/FirstLook.Mobile/css/style.deploy.css"/>
  <script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-22250230-1']);
    _gaq.push(['_trackPageview']);

    (function() {
         var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
         ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
         var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
     })();
  </script>
</head>
<body>
    <form id="form1" runat="server">
        <header id="header" class="toolbar clearfix">
            <h1 id="page_title">FirstLook Mobile</h1>
        </header>
        <div id="main" class="clearfix">
            <div id="scrollable" class="handle">
            </div>
        </div>
        <footer id="footer" class="clearfix">
        </footer>
    </form>

  <!--
  <script src="/resources/Scripts/jQuery/1.6/jquery.js"></script>
  <script src="/resources/Scripts/underscore/1.1.4/underscore-min.js"></script>
  <script src="/resources/Scripts/DragDealer/0.9.5/dragdealer.js"></script>
  <script src="/resources/Scripts/FirstLook.Mobile/lib.js"></script>
  <script src="/Client/Public/Scripts/App/Clients/Application.js"></script>
  <script src="/Client/Public/Scripts/App/Clients/MobileTemplate.js"></script>
  <script src="/Client/Public/Scripts/App/Vehicles/Application.js"></script>
  <script src="/Client/Public/Scripts/App/Vehicles/MobileTemplate.js"></script>
  <script src="/VehicleValuationGuide/Public/Scripts/App/BlackBook/Application.js"></script>
  <script src="/VehicleValuationGuide/Public/Scripts/App/BlackBook/MobileTemplate.js"></script>
  <script src="/VehicleValuationGuide/Public/Scripts/App/Edmunds/Application.js"></script>
  <script src="/VehicleValuationGuide/Public/Scripts/App/Edmunds/MobileTemplate.js"></script>
  <script src="/VehicleValuationGuide/Public/Scripts/App/Galves/Application.js"></script>
  <script src="/VehicleValuationGuide/Public/Scripts/App/Galves/MobileTemplate.js"></script>
  <script src="/VehicleValuationGuide/Public/Scripts/App/KelleyBlueBook/Application.js"></script>
  <script src="/VehicleValuationGuide/Public/Scripts/App/KelleyBlueBook/MobileTemplate.js"></script>
  <script src="/VehicleValuationGuide/Public/Scripts/App/Naaa/Application.js"></script>
  <script src="/VehicleValuationGuide/Public/Scripts/App/Naaa/MobileTemplate.js"></script>
  <script src="/VehicleValuationGuide/Public/Scripts/App/Nada/Application.js"></script>
  <script src="/VehicleValuationGuide/Public/Scripts/App/Nada/MobileTemplate.js"></script>
  <script src="/VehicleHistoryReport/Public/Scripts/App/Carfax/Application.js"></script>
  <script src="/VehicleHistoryReport/Public/Scripts/App/Carfax/MobileTemplate.js"></script>
  <script src="/VehicleHistoryReport/Public/Scripts/App/AutoCheck/Application.js"></script>
  <script src="/VehicleHistoryReport/Public/Scripts/App/AutoCheck/MobileTemplate.js"></script>
  <script src="../../Public/Scripts/Pages/Mobile/Application.js"></script>
  -->
  <script src="../../Public/Scripts/Pages/Mobile/Combined.js"></script>
</body>
</html>
