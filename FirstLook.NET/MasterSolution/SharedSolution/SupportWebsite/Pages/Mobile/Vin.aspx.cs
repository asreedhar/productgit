﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_Mobile_Vin : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        string vin = Request.QueryString.Get("VIN");
        if (!string.IsNullOrEmpty(vin))
        {
            Response.Redirect(string.Format("Entry.aspx#vin/{0}/", vin));
        }
    }
}
