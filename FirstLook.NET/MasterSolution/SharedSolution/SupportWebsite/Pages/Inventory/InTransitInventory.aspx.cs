﻿using System;
using System.Web.UI.WebControls;
using FirstLook.Common.WebControls.UI;
using FirstLook.Support.DomainModel.Inventory;
using System.Web.UI;
using FirstLook.Reports.GoogleAnalytics;


public partial class Pages_Inventory_InTransitInventory : System.Web.UI.Page, IGAnalytics
{

    #region Page Events
    protected void Page_PreInit(object sender, EventArgs e)
    {
        IsAnalyicsEnabled = true;
        this.PageTitle = "Create New Inventory";
    }


    protected void Page_Init(object sender, EventArgs e)
    {
        if (IsAnalyicsEnabled)
        {
            Page.GetGoogleAnalyticsScript();
        }
     
    }
    #endregion 

    #region Event Handlers

    /// <summary>
    /// Pre-populate the form with values of an existing appraisal, if an appraisal identifier was passed in.
    /// </summary>        
    protected void InTransitInventoryFormView_PreRender(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            // Dealer identifier.
            string dealerId = Request.QueryString["bu"];
            if (string.IsNullOrEmpty(dealerId))
            {
                throw new ArgumentException("Missing dealer identifier.");
            }            
            HiddenField dealerIdField = InTransitInventoryFormView.FindControl("DealerId") as HiddenField;
            if (dealerIdField != null)
            {
                dealerIdField.Value = dealerId;
            }

            // Pre-populate form if appraisal id is passed in.
            string appraisalId = Request.QueryString["ap"];
            if (!string.IsNullOrEmpty(appraisalId))
            {
                InTransitInventory inventory = InTransitInventory.CreateInTransitInventory(dealerId, appraisalId);

                // Appraisal identifier.
                HiddenField appraisalField = InTransitInventoryFormView.FindControl("AppraisalId") as HiddenField;
                if (appraisalField != null)
                {
                    appraisalField.Value = appraisalId;
                }
                
                // Vin.
                TextBox vin = InTransitInventoryFormView.FindControl("VinTextBox") as TextBox;
                if (vin != null)
                {
                    vin.Text = inventory.Vin;                    
                }

                // Stock number.
                TextBox stockNumber = InTransitInventoryFormView.FindControl("StockNumberTextBox") as TextBox;
                if (stockNumber != null)
                {
                    stockNumber.Focus();
                }

                // Mileage.
                TextBox mileage = InTransitInventoryFormView.FindControl("MileageTextBox") as TextBox;
                if (mileage != null)
                {
                    mileage.Text = inventory.Mileage.ToString();
                }

                // Deal type.
                RadioButtonList dealType = InTransitInventoryFormView.FindControl("DealTypeRadioButtons") as RadioButtonList;
                if (dealType != null)
                {
                    if (inventory.DealType == 1)
                    {
                        dealType.SelectedIndex = 1;
                    }                    
                    else if (inventory.DealType == 2)
                    {
                        dealType.SelectedIndex = 0;
                    }
                }
            }
            // If no appraisal id is available, focus on the VIN field.
            else
            {                
                TextBox vin = InTransitInventoryFormView.FindControl("VinTextBox") as TextBox;
                if (vin != null)
                {
                    vin.Focus();
                }
            }
        }
        else
        {
            if (Request.Params["InTransitInventoryFormView$VinTextBox"] != null)
            {
                ((TextBox) InTransitInventoryFormView.FindControl("VinTextBox")).Text =
                    Request.Params["InTransitInventoryFormView$VinTextBox"];
            }
        }


    }

    /// <summary>
    /// Validate values prior to insertion.
    /// </summary>        
    protected void InTransitInventoryFormView_ItemInserting(object sender, FormViewInsertEventArgs e)
    {
        string vin         = e.Values["vin"] as string;
        string stockNumber = e.Values["stockNumber"] as string;
        string mileage     = e.Values["mileage"] as string;
        string unitCost    = e.Values["unitCost"] as string;
        int dealType       = (int)e.Values["dealType"];

        bool inError = ValidateValues(vin, stockNumber, mileage, unitCost, dealType);
        e.Cancel = inError;
    }

    /// <summary>
    /// Redirect after finishing with the form. If we came from the appraisal page, head back there. Otherwise, go
    /// to the dealer's home page.
    /// </summary>        
    protected void InTransitInventoryFormView_ItemInserted(object sender, FormViewInsertedEventArgs e)
    {
       if (e.Exception == null)
            {
                Redirect();
            }
            else
            {
                e.ExceptionHandled = true;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "showalert", "alert('Error while saving data:" + e.Exception.InnerException.Message + "')", true);
                
            }
       
    }

    /// <summary>
    /// Redirect if the user cancels out of the form. If we came from the appraisal page, head back there. Otherwise, 
    /// go to the dealer's home page.
    /// </summary>    
    protected void InsertCancelButton_Click(object sender, EventArgs e)
    {
        Redirect();
    }    

    /// <summary>
    /// Redirect to the correct destination depending on whether or not we've got an appraisal id.
    /// </summary>
    protected void Redirect()
    {
        // If we had an appraisal id, go back to the Trade Manager.
        string appraisalId = Request.QueryString["ap"];
        if (!string.IsNullOrEmpty(appraisalId))
        {
            Response.Redirect("/IMT/TradeManagerEditDisplayAction.go?pageName=bullpen&appraisalId=" + appraisalId, true);
        }
        // No appraisal id? Go to the home page then.
        else
        {
            Response.Redirect("/IMT/DealerHomeDisplayAction.go", true);
        }
    }

    #endregion

    #region Validation

    /// <summary>
    /// Hide the error dialog when the user clicks on it.
    /// </summary>        
    protected void ValidationErrorDialog_ButtonClick(object sender, DialogButtonClickEventArgs e)
    {
        ValidationErrorDialog.Hidden = true;
    }

    /// <summary>
    /// Validate the form values.
    /// </summary>
    /// <param name="vin">VIN.</param>
    /// <param name="stockNumber">Stock number.</param>
    /// <param name="mileage">Mileage.</param>
    /// <param name="unitCost">Unit cost.</param>
    /// <param name="dealType">Deal type.</param>
    /// <returns>True if the values are in error. False if they are valid.</returns>
    private bool ValidateValues(string vin, string stockNumber, string mileage, string unitCost, int dealType)
    {
        BulletedList errorList = ValidationErrorDialog.FindControl("ErrorList") as BulletedList;
        if (errorList == null)
        {
            throw new ApplicationException("Missing expected validation error list.");
        }
        errorList.Items.Clear();

        // Vin.
        if (string.IsNullOrEmpty(vin))
        {
            errorList.Items.Add(new ListItem("VIN must be specified"));
        }

        // Vin length
        if (vin.Trim().Length != 17 || vin.Contains(" "))
        {
            errorList.Items.Add(new ListItem("Please enter 17 characters valid VIN"));
        }

      

        // Stock number.
            if (string.IsNullOrEmpty(stockNumber))
            {
                errorList.Items.Add(new ListItem("Stock number must be specified"));
            }

        // Mileage.
        int mileageInt;
        if (int.TryParse(mileage, out mileageInt))
        {
            if (mileageInt < 0)
            {
                errorList.Items.Add("If specified, mileage must be greater than or equal to 0");
            }
        }

        // Unit cost.
        double unitCostInt;
        if (double.TryParse(unitCost, out unitCostInt))
        {
            if (unitCostInt < 0)
            {
                errorList.Items.Add("If specified, unit cost must be greater than or equal to 0");
            }
        }

        // Deal type.
        if (dealType != 0 && dealType != 1)
        {
            errorList.Items.Add("The new inventory must either be a trade or a purchase.");
        }

        bool inError = errorList.Items.Count > 0;
        ValidationErrorDialog.Hidden = !inError;
        return inError;
    }

    #endregion

    #region IGAnalytics Members

    public string PageTitle
    {
        get;
        set;
    }

    public bool IsAnalyicsEnabled
    {
        get;
        set;
    }

    #endregion
}
