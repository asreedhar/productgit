﻿<%@ Page Language="C#" AutoEventWireup="true" Theme="Max" CodeFile="InTransitInventory.aspx.cs" Inherits="Pages_Inventory_InTransitInventory"%>
<%@ Register Assembly="FirstLook.Common.WebControls" Namespace="FirstLook.Common.WebControls.UI" TagPrefix="cwc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Create New Inventory</title>
    <link href="../../Public/Css/Application.css" rel="stylesheet" />
    <script type="text/javascript" src="/resources/Scripts/jQuery/1.3.2/jquery-1.3.2.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            
            // Adjust menu items
            $(".ui-menu-leaf").each(function() {
            
                // Remove non-linked menu items
                if ($(this).find("a").length < 1) { $(this).remove(); }
                
                // Remove print link
                if ($(this).find("a[target=Print]").length > 0) { $(this).remove(); }
                
            });
            
            // Add back link
            $(".ui-menu").append("<li class=\"ui-menu-leaf last\"><a href=\"javascript:window.history.back();\">&lt; Back</a></li>");
            
            // Prepend dialog mask element & adjust dialog CSS
            var validationDialog = $("#ValidationErrorDialog");
            if($(validationDialog).css("display") === "block") {
                $(validationDialog).before("<div class=\"ui-widget-overlay\">&nbsp;</div>");
                $(validationDialog).css("top", parseInt(($(window).height() - 360)/3, 10));
                $(validationDialog).css("left", parseInt(($(window).width() - 640)/2, 10));
            }
            
        });
    </script>
    <!--[if IE]>
    <style type="text/css">
    
        #InTransitInventoryFormView fieldset.buttons
        {
	        margin-left:64px !important;
        }

    </style
    <![endif]-->
    <!--[if IE 6]>
    <style type="text/css">
    
        #InventoryItemFieldsDiv
        {
	        float:left;
	        width:385px;
        }

    </style
    <![endif]-->

</head>
<body>
<form id="Form1" runat="server">
<div id="Container">
    <asp:ScriptManager ID="ScriptManager" EnablePartialRendering="true" EnablePageMethods="true" runat="server" LoadScriptsBeforeUI="false" ></asp:ScriptManager>
    
    <asp:ObjectDataSource ID="InTransitInventoryDataSource" runat="server"         
        TypeName="FirstLook.Support.DomainModel.Inventory.InTransitInventory+DataSource"                
        InsertMethod="Insert">
        
        <InsertParameters>
            <asp:Parameter Name="dealerId" Type="Int32" />
            <asp:Parameter Name="appraisalId" Type="Int32" ConvertEmptyStringToNull="true"/>
            <asp:Parameter Name="vin" Type="String"/>
            <asp:Parameter Name="stockNumber" Type="String"/>
            <asp:Parameter Name="mileage" Type="Int32" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="unitCost" Type="Double" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="dealType" Type="Int32" />
        </InsertParameters>
    </asp:ObjectDataSource>
    
    <div id="Header">
    
        <asp:SiteMapDataSource ID="ApplicationSiteMapDataSource" runat="server" 
	            ShowStartingNode="false" 
			    SiteMapProvider="ApplicationSiteMap" />
	    <asp:Menu id="ApplicationMenuContainer" runat="server" 
	            CssClass="smallCaps blue" 
	            Orientation="Horizontal" 
	            DataSourceID="ApplicationSiteMapDataSource" />
	            
    </div>
    
    <div id="Content">
        <!-- Insertion Form -->
        <asp:FormView ID="InTransitInventoryFormView" runat="server"
        DataSourceID="InTransitInventoryDataSource" 
        DefaultMode="Insert"
        OnPreRender="InTransitInventoryFormView_PreRender"
        OnItemInserting="InTransitInventoryFormView_ItemInserting"
        OnItemInserted="InTransitInventoryFormView_ItemInserted">
        
        <InsertItemTemplate>
            <h2>Create New Inventory Item</h2>
            <fieldset class="clearfix">
                <!-- Dealer Id -->
                <asp:HiddenField ID="DealerId" Value='<%# Bind("dealerId") %>' runat="server" />
                
                <!-- Appraisal Id -->
                <asp:HiddenField ID="AppraisalId" Value='<%# Bind("appraisalId") %>' runat="server" />
                
                <div id="InventoryItemFieldsDiv" class="clearfix">
                
                    <!-- VIN -->
                    <div class="fieldLabelPair clearfix">
                        <asp:Label ID="VinLabel" runat="server" Text="VIN" EnableViewState="false" AssociatedControlID="VinTextBox" />
                        <asp:TextBox ID="VinTextBox" runat="server" Text='<%# Bind("vin") %>' MaxLength="17" CssClass="upper-case"/>
                    </div>
                    
                    <!-- Stock Number -->
                    <div class="fieldLabelPair clearfix">
                        <asp:Label ID="StockNumberLabel" runat="server" Text="Stock Number" EnableViewState="false" AssociatedControlID="StockNumberTextBox" />
                        <asp:TextBox ID="StockNumberTextBox" runat="server" Text='<%# Bind("stockNumber") %>' MaxLength="15" />
                    </div>
                    
                    <!-- Mileage -->
                    <div class="fieldLabelPair clearfix">
                        <asp:Label ID="MileageLabel" runat="server" Text="Mileage" EnableViewState="false" AssociatedControlID="MileageTextBox" />
                        <asp:TextBox ID="MileageTextBox" runat="server" Text='<%# Bind("mileage") %>' MaxLength="10" />
                    </div>
                    
                    <!-- Unit Cost -->
                    <div class="fieldLabelPair clearfix">
                        <asp:Label ID="UnitCostLabel" runat="server" Text="Unit Cost" EnableViewState="false" AssociatedControlID="UnitCostTextBox" />
                        <asp:TextBox ID="UnitCostTextBox" runat="server" Text='<%# Bind("unitCost") %>' MaxLength="10" />
                    </div>
                
                    <p class="disclaimer">Note: Newly created inventory items may not be reflected in summary statistics or reports for up to 24 hours.</p>
                    
                </div>
                       
                <!-- Deal Type -->
                <div id="DealTypeRadioButtonsDiv" class="clearfix">
                    <asp:Label ID="DealTypeLabel" runat="server" Text="Trade/Purchase" EnableViewState="false" AssociatedControlID="UnitCostTextBox" />
                    <br />
                    <asp:RadioButtonList ID="DealTypeRadioButtons" SelectedIndex='<%# Bind("dealType") %>' runat="server" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Flow" TextAlign="Right" >                    
                        <asp:ListItem  Value="trade">Trade</asp:ListItem>
                        <asp:ListItem Value="purchase">Purchase</asp:ListItem>
                    </asp:RadioButtonList>                    
                </div> 
                      
                 
            </fieldset>         
            <fieldset class="buttons">
                <asp:Button ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Save"/>
                <asp:Button ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" OnClick="InsertCancelButton_Click" />
            </fieldset>
        </InsertItemTemplate>   
                                     
    </asp:FormView>
    
    </div>
    
    <div id="Footer">
    
        <div id="bottomstatus">
            <ul class="navlist">
                <li>USER: <%= Context.User.Identity.Name %></li>
                <li><em>SECURE AREA</em></li>
                <li style="border-right: 0pt none;">Copyright &copy;<%= DateTime.Now.Year.ToString() %> First Look, LLC.  All Rights Reserved.</li>
            </ul>
        </div>

        <div id="logo"><img alt="FirstLook" src="../../App_Themes/MAX/Images/fllogo.gif?1289245008"></div>

    </div>
</div>
        <!-- Validation Message -->
        <cwc:Dialog runat="server" ID="ValidationErrorDialog"
           Visible="true" 
           Hidden="true"
           TitleText="Validation Error"
           Top="0" Left="0" Width="640" Height="360" AllowResize="false" AllowDrag="false"
           OnButtonClick="ValidationErrorDialog_ButtonClick">
        <ItemTemplate>
            <asp:BulletedList ID="ErrorList" runat="server">
            </asp:BulletedList>
        </ItemTemplate>
        
        <Buttons>
            <cwc:DialogButton ButtonType="button" ButtonCommand="Ok" Text="OK" IsDefault="true" />
        </Buttons>
    </cwc:Dialog> 
</form>
</body>
</html>
