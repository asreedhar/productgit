<%@ Page Language="C#" MasterPageFile="~/Master_Pages/Marketing.master" AutoEventWireup="true" CodeFile="AboutUs.aspx.cs" Inherits="Marketing_Pages_AboutUs" Title="About First Look" %>

<asp:Content ID="bd" runat="server" ContentPlaceHolderID="bd">
	<p>Headquartered in Chicago, business intelligence &ndash; performance management software pioneer First Look empowers traditionally non-analytically skilled or inclined management with the benefits of sophisticated analytical insight.</p>

	<h2>Background</h2>
	<p>One of the most powerful drivers of economic growth in the 1990s was the revolution in the precision of business decision making. By introducing increasingly sophisticated analytical tools that could be operated by analytically skilled general management personnel, large corporations were able to catalyze significantly increased efficiency and profitability in their operations. At the same time, these powerful developments failed to take root beyond the ranks of already analytically skilled and inclined management.</p>

	<h2>First Look Solution</h2>
	<p>First Look empowers non-analytically skilled or inclined managers with the high level of analytical sophistication that senior Fortune 500 management harnessed to drive unprecedented efficiency and profitability. By moving beyond existing <em>"do-it-yourself"</em> tools and reports into sophisticated yet easy to use <em>"do-it-for-you"</em> tools and systems, First Look introduces the power of data driven decision making to non-analytically skilled or inclined line management.</p>
	<p>First Look's unique solution combines tools built around industry best practices with a unique and proprietary Artificial Intelligence <em>(Expert System)</em> approach that provides the benefits of complex analytics to managers that either lack the time, inclination or skills to perform such analyses on their own. In place of current data intensive reports, First Look Insight automatically highlights in plain English text on a single page the key findings that an analytically skilled user would obtain through exhaustive analysis of dozens of traditional data reports. The defining characteristic of all First Look products is their unique combination of exceptional quickness and ease of use with maximum analytical insight.</p>
	<p>First Look currently implements this revolutionary analytic software in the $500 billion automotive retail market. First Look's major products for the automotive retail market include:</p>
	<ul>
		<li>
			<strong>Performance Dashboard powered by Insight</strong> &mdash; a revolutionary Performance Management system that enables non-analytical managers to quickly determine key areas for improvement while obtaining the penetrating insight necessary to drive performance improvement. 
		</li>
		<li>
			<strong>Variable Operations Best Practices Tools</strong> &mdash; allows managers to integrate sophisticated analysis into daily activities while making it easy to consistently execute and track compliance with industry best practices.
		</li>
		<li>
			<strong>Dynamic Reporting Software</strong> &mdash; enables managers, even those with limited computer savvy, to quickly and easily make optimal inventory management decisions. 
		</li>	
	</ul>
</asp:Content>