﻿using System;

using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace App_Code
{
    /// <summary>
    /// Summary description for CacheManifestHandler
    /// </summary>
    public class CacheManifestHandler : IHttpHandler
    {
        #region Implementation of IHttpHandler

        public void ProcessRequest(HttpContext context)
        {
            HttpRequest request = context.Request;
            HttpResponse response = context.Response;
            response.ContentType = SR.ContentType;
            response.BufferOutput = false;

            if (request != null && request.PhysicalPath != null)
            {
                if (File.Exists(request.PhysicalPath))
                {
                    response.WriteFile(request.PhysicalPath);
                    response.Cache.SetExpires(DateTime.Today.AddDays(5));
                    response.Cache.SetETagFromFileDependencies();
                    response.Cache.SetLastModifiedFromFileDependencies();
                    response.Cache.SetCacheability(HttpCacheability.ServerAndPrivate);
                    response.End();
                    return;
                }

                List<string> fileList = new List<string>();
                string cachedFilePattern = ConfigurationManager.AppSettings[SR.CachedFilePatternSetting];
                string[] cachedFilePatterns = cachedFilePattern.Split(new [] {";"}, StringSplitOptions.RemoveEmptyEntries);
                foreach (var pattern in cachedFilePatterns)
                {
                    fileList.AddRange(Directory.GetFiles(request.PhysicalApplicationPath, pattern, SearchOption.AllDirectories));
                }

                fileList.Sort();

                response.Write("CACHE MANIFEST\n\n");

                response.Write(string.Format("# Created {0}\n\n", DateTime.Now));

                foreach (var filePath in fileList)
                {
                    response.Write(String.Format("{0}/{1}\n", request.ApplicationPath, filePath.Replace(request.PhysicalApplicationPath, "").Replace(@"\", "/")));
                }

                response.Cache.SetExpires(DateTime.Today.AddDays(5));
                response.Cache.SetETagFromFileDependencies();
                response.Cache.SetLastModifiedFromFileDependencies();
                response.Cache.SetCacheability(HttpCacheability.ServerAndPrivate);

                response.End();
            }
        }

        public bool IsReusable
        {
            get { return true; }
        }

        #endregion

        private static class SR
        {
            public const string ContentType = "text/cache-manifest";
            public const string DefaultFile = "cache.manifest";
            public const string CachedFilePatternSetting = "App_Code.CachedManifestHandler.FilePattern";
        }
    }
}