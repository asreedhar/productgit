using System;
using System.Net;
using System.Threading;
using System.Web.Services.Protocols;
using System.Web.UI;
using FirstLook.Common.Core.Utilities;
using FirstLook.VehicleHistoryReport.WebService.Client.AutoCheck;
using System.Xml;
using HtmlAgilityPack;

public partial class Reports_AutoCheckReport : Page
{
    private string Vin
    {
        get
        {
            return Request.QueryString["Vin"];
        }
    }

    private int DealerId
    {
        get
        {
            return Int32Helper.ToInt32(Request.QueryString["DealerId"]);
        }
    }

    protected override void Render(HtmlTextWriter writer)
    {
        string reportHtml = string.Empty;

        try
        {
            reportHtml = GetReportHtml(DealerId, Vin);
        }
        catch (SoapException exception)
        {
            if (exception.Code != SoapException.ServerFaultCode)
            {
                throw;
            }
        }

        if (string.IsNullOrEmpty(reportHtml))
        {
            base.Render(writer);
        }
        else
        {
            //Fix for Print Issue Case ID 31117
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(reportHtml);
            var doctype = doc.DocumentNode.SelectSingleNode("//head");
            if (doctype == null)
                doctype = doc.DocumentNode.PrependChild(doc.CreateComment());

            HtmlNode link = doc.CreateElement("meta");
            doctype.PrependChild(link);
            link.SetAttributeValue("http-equiv", "X-UA-Compatible");
            link.SetAttributeValue("content", "IE=edge");

            writer.Write(doc.DocumentNode.InnerHtml);
           // writer.Write(reportHtml);
        }
    }   


    private static string GetReportHtml(int dealerId, string vin)
    {
        UserIdentity identity = new UserIdentity();

        identity.UserName = Thread.CurrentPrincipal.Identity.Name;

        AutoCheckWebService service = new AutoCheckWebService();

        service.UserIdentityValue = identity;

        string report;

        try
        {
            report = service.GetReportHtml(dealerId, vin);
        }
        catch (WebException exception)
        {
            if (exception.Status != WebExceptionStatus.ConnectFailure)
            {
                throw;
            }

            report = string.Empty;
        }

        return report;
    }
}
