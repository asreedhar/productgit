<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AuctionTransactions.aspx.cs" Inherits="Reports_AuctionTransactions" %>

<%@ Register Assembly="FirstLook.Common.WebControls" Namespace="FirstLook.Common.WebControls.UI" TagPrefix="cwc" %>

<%@ Register Assembly="FirstLook.DomainModel.Oltp" Namespace="FirstLook.DomainModel.Oltp.WebControls" TagPrefix="owc" %>

<%@ OutputCache Location="None" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Auction Report</title>
</head>
<body class="report-page">
    <form id="form1" runat="server">
        <!-- script manager -->
        <asp:ScriptManager ID="ScriptManager1" runat="server" LoadScriptsBeforeUI="false" >
            <Scripts>
                <asp:ScriptReference Path="../Public/Scripts/progress_loader.js" NotifyScriptLoaded="false" />
            </Scripts>            
        </asp:ScriptManager>
        <!-- hedgehog -->
        <owc:HedgehogTracking ID="hedgehog" runat="server" />
        
        <!-- display content -->
        <div class="container">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                        <!-- page state as hidden fields -->
                        <fieldset style="display: none;">
                                <asp:HiddenField ID="vin" runat="server" />
                                <asp:HiddenField ID="vic" runat="server" />
                                <asp:HiddenField ID="areaId" runat="server" />
                                <asp:HiddenField ID="periodId" runat="server" />
                                <asp:HiddenField ID="mileage" runat="server" />
                        </fieldset>
                        <!-- data sources -->
                        <asp:ObjectDataSource ID="NationalAuctionPanelDataSource" runat="server" TypeName="FirstLook.DomainModel.Olap.WebControls.NationalAuctionPanelDataSource" SelectMethod="SelectByAll">
                            <SelectParameters>
                                <asp:ControlParameter ConvertEmptyStringToNull="true" DefaultValue="" Name="vin" ControlID="vin" />
                                <asp:ControlParameter ConvertEmptyStringToNull="true" DefaultValue="" Name="vic" ControlID="vic" />
                                <asp:ControlParameter ConvertEmptyStringToNull="true" DefaultValue="" Name="areaId" ControlID="areaId" />
                                <asp:ControlParameter ConvertEmptyStringToNull="true" DefaultValue="" Name="periodId" ControlID="periodId" />
                                <asp:ControlParameter ConvertEmptyStringToNull="true" DefaultValue="" Name="mileage" ControlID="mileage" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <csla:CslaDataSource
                            ID="gridViewDataSource"
                            runat="server"
                            TypeName="FirstLook.DomainModel.Olap.NationalAuctionVehicle"
                            TypeSupportsPaging="true"
                            TypeSupportsSorting="true"
                            OnSelectObject="cslaDataSource1_SelectObject" />
                
                    <div class="content">
                        <div class="header">
                            <p class="close"><a href="javascript:window.close()">Close Window</a></p>
                            <br class="clear" />
                            <h1>NAAA Report</h1>
                            <olap:NationalAuctionPanel ID="nationalAuctionPanel" runat="server" DataSourceID="NationalAuctionPanelDataSource" ShowReportLink="false"
                                OnSelectedAreaChanged="nationalAuctionPanel_SelectedAreaChanged"
                                OnSelectedPeriodChanged="nationalAuctionPanel_SelectedPeriodChanged"
                                OnSelectedSeriesChanged="nationalAuctionPanel_SelectedSeriesChanged" />
                        </div>
                        <asp:GridView ID="GridView1" runat="server" AllowPaging="true" AllowSorting="true" RowStyle-CssClass="odd" AutoGenerateColumns="false" DataSourceID="gridViewDataSource" UseAccessibleHeader="true" CssClass="primary-data"  PageSize="100">
                            <Columns>
                                <asp:BoundField AccessibleHeaderText="Deal" ReadOnly="true" DataField="RowNumber" DataFormatString="{0:0'.'}" HeaderText="Deal" HtmlEncode="false" SortExpression="RowNumber" />
                                <asp:BoundField AccessibleHeaderText="Sale Date" DataField="SaleDate" DataFormatString="{0:MM/dd/yyyy}" HeaderText="Sale Date" HtmlEncode="false" ReadOnly="true" SortExpression="SaleDate" />
                                <asp:BoundField AccessibleHeaderText="NAAA Region" DataField="RegionName" HeaderText="NAAA Region" ReadOnly="true" SortExpression="RegionName" />
                                <asp:BoundField AccessibleHeaderText="Sale Type" DataField="SaleTypeName" HeaderText="Sale Type" ReadOnly="true" SortExpression="SaleTypeName" />
                                <asp:BoundField AccessibleHeaderText="NAAA Series" DataField="Series" HeaderText="NAAA Series" ReadOnly="true" SortExpression="Series" />
                                <asp:BoundField AccessibleHeaderText="Price" DataField="SalePrice" DataFormatString="{0:c0}" HeaderText="Price" HtmlEncode="false" ReadOnly="true" SortExpression="SalePrice" />
                                <asp:BoundField AccessibleHeaderText="Mileage" DataField="Mileage" DataFormatString="{0:#,0}" HeaderText="Mileage" HtmlEncode="false" ReadOnly="true" SortExpression="Mileage" />
                                <asp:BoundField AccessibleHeaderText="Engine" DataField="Engine" HeaderText="Engine" ReadOnly="true" SortExpression="Engine" />
                                <asp:BoundField AccessibleHeaderText="Transmission" DataField="Transmission" HeaderText="Transmission" ReadOnly="true" SortExpression="Transmission" />
                            </Columns>
                        </asp:GridView>
                        <asp:Panel ID="NoResultsPanel" runat="server" OnPreRender="NoResultsPanel_PreRender">
                            <p>There have been 0 sales of this vehicle in this time period.</p>
                        </asp:Panel>
                    </div>
                    <cwc:PageFooter ID="PageFooter" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
                
        <div id="progress" style="display: none;">
            <iframe id="Iframe1" runat="server" src="Public/blank.html"></iframe>
            <div>
                <img src="../Public/Images/loading.gif" alt="Loading ... Please Wait" /> Please wait...
            </div>
        </div>
    </form>
</body>
</html>
