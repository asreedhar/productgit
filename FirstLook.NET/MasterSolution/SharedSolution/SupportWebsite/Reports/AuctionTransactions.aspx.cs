using System;
using System.Globalization;
using System.Web.UI;
using Csla.Web;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Olap;
using FirstLook.DomainModel.Olap.WebControls;

public partial class Reports_AuctionTransactions : Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            vin.Value = Request.QueryString["VIN"];
            vic.Value = Request.QueryString["VIC"];
            areaId.Value = Request.QueryString["AreaID"];
            periodId.Value = Request.QueryString["PeriodID"];
            mileage.Value = Request.QueryString["Mileage"];
        }
    }

    protected void cslaDataSource1_SelectObject(object sender, SelectObjectArgs e)
    {
        NationalAuctionSeriesList seriesList = NationalAuctionSeriesList.GetSeriesList(vin.Value);
        NationalAuctionReportingAreaList areaList = NationalAuctionReportingAreaList.GetReportingAreaList();
        NationalAuctionReportingPeriodList periodList = NationalAuctionReportingPeriodList.GetReportingPeriodList();

        NationalAuctionReportCriteria criteria = new NationalAuctionReportCriteria(
            vin.Value,
            seriesList.GetSeries(vic.Value),
            areaList.GetReportingArea(Int32Helper.ToNullableInt32(areaId.Value)),
            periodList.GetReportingPeriod(Int32Helper.ToNullableInt32(periodId.Value)),
            Int32Helper.ToNullableInt32(mileage.Value));

        e.BusinessObject = NationalAuctionVehicleList.GetVehicleList(
            criteria, e.SortExpression, e.MaximumRows, e.StartRowIndex);
    }

    protected void nationalAuctionPanel_SelectedAreaChanged(object sender, NationalAuctionEventArgs<int> e)
    {
        areaId.Value = e.Value.ToString(CultureInfo.InvariantCulture);
        GridView1.PageIndex = 0;
        GridView1.DataBind();
    }

    protected void nationalAuctionPanel_SelectedPeriodChanged(object sender, NationalAuctionEventArgs<int> e)
    {
        periodId.Value = e.Value.ToString(CultureInfo.InvariantCulture);
        GridView1.PageIndex = 0;
        GridView1.DataBind();
    }

    protected void nationalAuctionPanel_SelectedSeriesChanged(object sender, NationalAuctionEventArgs<string> e)
    {
        vic.Value = e.Value;
        GridView1.PageIndex = 0;
        GridView1.DataBind();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        DataBindChildren();
    }

    protected void NoResultsPanel_PreRender(object sender, EventArgs e)
    {
        NoResultsPanel.Visible = GridView1.Rows.Count < 1;
    }
}
