<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AutoCheckReport.aspx.cs" Inherits="Reports_AutoCheckReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AutoCheck Report</title>
</head>
<body>
    <form id="form1" runat="server">
    
    <div id="doc3">
			<div id="hd">			
			<asp:SiteMapDataSource ID="MemberSiteMapDataSource" runat="server" ShowStartingNode="false" SiteMapProvider="MemberSiteMap" />
			<asp:Menu ID="member_menu_container" runat="server" CssClass="ltGrey" Orientation="Horizontal" DataSourceID="MemberSiteMapDataSource" />
			</div>		
            <div id="bd" style="color: #5A5A5A; padding: 2em">
                <h2>Error Retrieving your AutoCheck Report</h2>

                <p>If the problem persists, please contact your account manager or call Customer Service at 1-877-378-5665. Click <a href="javascript:window.close()">here</a> to close the window.</p>
            </div>
            <cwc:PageFooter ID="SiteFooter" runat="server" />
        </div>
    
    </form>
</body>
</html>
