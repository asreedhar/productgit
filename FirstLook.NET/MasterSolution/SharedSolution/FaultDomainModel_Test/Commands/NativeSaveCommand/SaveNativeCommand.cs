﻿using System;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes;
using NUnit.Framework;
using ICommandFactory = FirstLook.Fault.DomainModel.Commands.ICommandFactory;

namespace FirstLook.Fault.DomainModel.Test.Repositories.Commands.NativeSaveCommand
{
    [TestFixture]
    public class SaveNativeCommand
    {
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            IRegistry registry = RegistryFactory.GetRegistry();
            registry.Register<Module>();
            registry.Register<IDataSessionManager, DataSessionManager>(ImplementationScope.Shared);
            registry.Register<ICache, NoCache>(ImplementationScope.Shared);
        }

        [Test]
        public void SaveNativeCommandTest()
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<SaveNativeResultsDto, SaveNativeArgumentsDto> command = factory.CreateNativeSaveCommand();

            try
            {
                throw new ItFailedExceptionaa("blah");
            } catch(Exception e)
            {
                DateTime myTime = new DateTime(2011, 6, 3, 8, 23, 32);
                command.Execute(new SaveNativeArgumentsDto
                                    {
                                        Fault = e,
                                        Application = "myapplication",
                                        Machine = "mymachine",
                                        Path = "mypath",
                                        Platform = "mylpatform",
                                        Timestamp = myTime,
                                        Url = "myurl",
                                        User = "myusr",
                                        UserHostAddress = "myuserhost"
                                    });
            }

        }

        [Test]
        public void SaveNativeCommandInnerExceptionTest()
        {
            try
            {
                throw new Exception("MyApplicationException");
            }
            catch (Exception e)
            {
                MethodCall(e);
            }
        }

        public void MethodCall(Exception e)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<SaveNativeResultsDto, SaveNativeArgumentsDto> command = factory.CreateNativeSaveCommand();

            try
            {
                throw new ItFailedExceptionab("MyInnerException", e);
            }
            catch (ItFailedExceptionaa f)
            {
                DateTime myTime = new DateTime(2011, 6, 3, 8, 23, 32);

                command.Execute(new SaveNativeArgumentsDto
                {
                    Fault = f,
                    Application = "myapplication",
                    Machine = "mymachine",
                    Path = "mypath",
                    Platform = "mylpatform",
                    Timestamp = myTime,
                    Url = "myurl",
                    User = "myusr",
                    UserHostAddress = "myuserhost"
                });

            }

        }

        protected class NoCache : ICache
        {
            public object Add(string key, object value, DateTime absoluteExpiration, TimeSpan slidingExpiration)
            {
                return null;
            }

            public object Get(string key)
            {
                return null;
            }

            public object Remove(string key)
            {
                return null;
            }
        }

        public class ItFailedExceptionaa : Exception
        {
            public ItFailedExceptionaa(string message)
                : base(message)
            {
            }
        }

        public class ItFailedExceptionab : Exception
        {
            public ItFailedExceptionab(string message,
               Exception innerException)
                : base(message, innerException)
            {
            }
        }
    }
}