﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.Fault.DomainModel.Commands.TransferObjects;
using FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes;
using NUnit.Framework;
using ICommandFactory = FirstLook.Fault.DomainModel.Commands.ICommandFactory;

namespace FirstLook.Fault.DomainModel.Test.Repositories.Commands.RemoteSaveCommand
{
    [TestFixture]
    public class SaveRemoteCommand
    {
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            IRegistry registry = RegistryFactory.GetRegistry();
            registry.Register<Module>();
            registry.Register<IDataSessionManager, DataSessionManager>(ImplementationScope.Shared);
            registry.Register<ICache, NoCache>(ImplementationScope.Shared);
        }

        [Test]
        public void SaveRemoteCommandTest()
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<SaveRemoteResultsDto, SaveRemoteArgumentsDto> command = factory.CreateSaveCommand();

            SaveRemoteArgumentsDto argumentsDto = new SaveRemoteArgumentsDto
                                                      {
                                                          FaultEvent = new FaultEventDto
                                                                           {
                                                                               Application = new ApplicationDto
                                                                                                 {
                                                                                                     Name =
                                                                                                         "TestRemoteApp"
                                                                                                 },
                                                                               Fault = new FaultDto
                                                                                           {
                                                                                               Cause = null,
                                                                                               ExceptionType =
                                                                                                   "RemoteExceptionType",
                                                                                               Message = "Remotemsg",
                                                                                               Stack = new StackDto
                                                                                                           {
                                                                                                               StackFrames
                                                                                                                   =
                                                                                                                   new List
                                                                                                                   <
                                                                                                                   StackFrameDto
                                                                                                                   >
                                                                                                                       {
                                                                                                                           new StackFrameDto
                                                                                                                               {
                                                                                                                                   ClassName
                                                                                                                                       =
                                                                                                                                       "aRemClass123",
                                                                                                                                   FileName
                                                                                                                                       =
                                                                                                                                       "aRemFile123",
                                                                                                                                   LineNumber
                                                                                                                                       =
                                                                                                                                       5,
                                                                                                                                   IsNativeMethod
                                                                                                                                       =
                                                                                                                                       false,
                                                                                                                                   IsUnknownSource
                                                                                                                                       =
                                                                                                                                       false,
                                                                                                                                   MethodName
                                                                                                                                       =
                                                                                                                                       "aRemMethod123"
                                                                                                                               }
                                                                                                                       }
                                                                                                           }
                                                                                           },
                                                                               FaultData = new List<FaultDataDto>(),
                                                                               FaultTime = DateTime.Now,
                                                                               Machine = new MachineDto
                                                                                             {
                                                                                                 Name = "aRemMachine"
                                                                                             },
                                                                               Platform = new PlatformDto
                                                                                              {
                                                                                                  Name = "aRemPlatform"
                                                                                              },
                                                                               RequestInformation =
                                                                                   new RequestInformationDto
                                                                                       {
                                                                                           Path = "RemPath",
                                                                                           Url = "RemUrl",
                                                                                           User = "RemUser",
                                                                                           UserHostAddress = "RemUHA"
                                                                                       }
                                                                           }
                                                      };


            SaveRemoteResultsDto res = command.Execute(argumentsDto);
        }


        [Test]
        public void SaveRemoteCommandInnerExceptionTest()
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<SaveRemoteResultsDto, SaveRemoteArgumentsDto> command = factory.CreateSaveCommand();

            SaveRemoteArgumentsDto argumentsDto = new SaveRemoteArgumentsDto
                                                      {
                                                          FaultEvent = new FaultEventDto
                                                                           {
                                                                               Application = new ApplicationDto
                                                                                                 {
                                                                                                     Name =
                                                                                                         "TestRemoteApp"
                                                                                                 },
                                                                               Fault = new FaultDto
                                                                                           {
                                                                                               Cause =
                                                                                                   new FaultDto
                                                                                                       {
                                                                                                           Cause
                                                                                                               =
                                                                                                               null,
                                                                                                           ExceptionType
                                                                                                               =
                                                                                                               "RemoteInnerExceptionType123",
                                                                                                           Message
                                                                                                               =
                                                                                                               "RemoteInnermsg123",
                                                                                                           Stack
                                                                                                               =
                                                                                                               new StackDto
                                                                                                                   {
                                                                                                                       StackFrames
                                                                                                                           =
                                                                                                                           new List
                                                                                                                           <
                                                                                                                           StackFrameDto
                                                                                                                           >
                                                                                                                               {
                                                                                                                                   new StackFrameDto
                                                                                                                                       {
                                                                                                                                           ClassName
                                                                                                                                               =
                                                                                                                                               "aRemInnerClass123a",
                                                                                                                                           FileName
                                                                                                                                               =
                                                                                                                                               "aRemInnerFile123a",
                                                                                                                                           LineNumber
                                                                                                                                               =
                                                                                                                                               10,
                                                                                                                                           IsNativeMethod
                                                                                                                                               =
                                                                                                                                               false,
                                                                                                                                           IsUnknownSource
                                                                                                                                               =
                                                                                                                                               false,
                                                                                                                                           MethodName
                                                                                                                                               =
                                                                                                                                               "aRemInnerMethod123a"
                                                                                                                                       }
                                                                                                                               }
                                                                                                                   }
                                                                                                       },
                                                                                               ExceptionType =
                                                                                                   "RemoteExceptionType",
                                                                                               Message = "Remotemsg",
                                                                                               Stack = new StackDto
                                                                                                           {
                                                                                                               StackFrames
                                                                                                                   =
                                                                                                                   new List
                                                                                                                   <
                                                                                                                   StackFrameDto
                                                                                                                   >
                                                                                                                       {
                                                                                                                           new StackFrameDto
                                                                                                                               {
                                                                                                                                   ClassName
                                                                                                                                       =
                                                                                                                                       "aRemClass321a",
                                                                                                                                   FileName
                                                                                                                                       =
                                                                                                                                       "aRemFile321a",
                                                                                                                                   LineNumber
                                                                                                                                       =
                                                                                                                                       5,
                                                                                                                                   IsNativeMethod
                                                                                                                                       =
                                                                                                                                       false,
                                                                                                                                   IsUnknownSource
                                                                                                                                       =
                                                                                                                                       false,
                                                                                                                                   MethodName
                                                                                                                                       =
                                                                                                                                       "aRemMethod321a"
                                                                                                                               }
                                                                                                                       }
                                                                                                           }
                                                                                           },
                                                                               FaultData = new List<FaultDataDto>(),
                                                                               FaultTime = DateTime.Now,
                                                                               Machine = new MachineDto
                                                                                             {
                                                                                                 Name = "RemMachine"
                                                                                             },
                                                                               Platform = new PlatformDto
                                                                                              {
                                                                                                  Name = "RemPlatform"
                                                                                              },
                                                                               RequestInformation =
                                                                                   new RequestInformationDto
                                                                                       {
                                                                                           Path = "RemPath",
                                                                                           Url = "RemUrl",
                                                                                           User = "RemUser",
                                                                                           UserHostAddress = "RemUHA"
                                                                                       }
                                                                           }
                                                      };


            SaveRemoteResultsDto res = command.Execute(argumentsDto);
        }

        protected class NoCache : ICache
        {
            public object Add(string key, object value, DateTime absoluteExpiration, TimeSpan slidingExpiration)
            {
                return null;
            }

            public object Get(string key)
            {
                return null;
            }

            public object Remove(string key)
            {
                return null;
            }
        }

        public class ItFailedExceptionaa : Exception
        {
            public ItFailedExceptionaa(string message)
                : base(message)
            {
            }
        }

        public class ItFailedExceptionab : Exception
        {
            public ItFailedExceptionab(string message,
               Exception innerException)
                : base(message, innerException)
            {
            }
        }
    }
}