﻿using System;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.Fault.DomainModel.Model;
using NUnit.Framework;

namespace FirstLook.Fault.DomainModel.Test.Repositories.Repository.Fetch
{
    [TestFixture]
    public class RepositoryFetchTest
    {
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            IRegistry registry = RegistryFactory.GetRegistry();
            registry.Register<Module>();
            registry.Register<IDataSessionManager, DataSessionManager>(ImplementationScope.Shared);
            registry.Register<ICache, NoCache>(ImplementationScope.Shared);
        }

        /// <summary>
        /// Test the generation of a fault event in which the following structure is specified:
        /// FaultEvent:
        ///     Application 
        ///     Platform
        ///     Machine
        ///     Message
        ///     Timestamp
        ///     RequestorInformation:
        ///         Path
        ///         Url
        ///         User
        ///         UserHostAddress
        ///     Fault:
        ///         ExceptionType
        ///         Stack:
        ///             StackFrame:
        ///                 ClassName
        ///                 MethodName
        ///                 FileName
        /// 
        /// As of right now the test only ensures a saved fault event was returned.
        /// </summary>
        
        [Test]
        public void FetchTest()
        {
            IResolver resolver = RegistryFactory.GetResolver();
            var faultRepository = resolver.Resolve<IFaultRepository>();
            var builder = resolver.Resolve<IFaultEventBuilder>();

            builder.Application = "MyApp";
            builder.Platform = "MyPlatform";
            builder.Machine = "MyMachine";
            builder.Timestamp = new DateTime(2011,6,2,9,49,32);

            builder.BeginRequestorInformation();
            builder.Path = "MyPath";
            builder.Url = "MyUrl";
            builder.User = "MyUser";
            builder.UserHostAddress = "MyUserHost";
            builder.EndRequestorInformation();

            builder.BeginFault();
            builder.ExceptionType = "MyExceptionType";
            builder.Message = "MyMessage";
            builder.BeginStack();
            builder.BeginStackFrame();
            builder.ClassName = "MyClassaaa";
            builder.MethodName = "MyMethodName";
            builder.FileName = "MyFileName";
            builder.EndStackFrame();
            builder.EndStack();
            builder.EndFault();

            IFaultEvent toSave = builder.ToFaultEvent();

            toSave = faultRepository.Save(toSave);
            Assert.AreNotEqual(toSave.Id, 0);
            Assert.IsFalse(toSave.IsNew);

            IFaultEvent fetched = faultRepository.Fetch(toSave.Id);

        }

        [Test]
        public void FetchTest_OneCauseFault()
        {
            IResolver resolver = RegistryFactory.GetResolver();
            var faultRepository = resolver.Resolve<IFaultRepository>();
            var builder = resolver.Resolve<IFaultEventBuilder>();

            builder.Application = "MyAppXFaults";
            builder.Platform = "MyPlatformXFaults";
            builder.Machine = "MyMachineXFaults";
            builder.Timestamp = new DateTime(2011, 7, 7, 9, 49, 32);

            builder.BeginRequestorInformation();
            builder.Path = "MyXPath";
            builder.Url = "MyXUrl";
            builder.User = "MyXUser";
            builder.UserHostAddress = "MyXUserHost";
            builder.EndRequestorInformation();

            //External Fault
            builder.BeginFault();
            builder.ExceptionType = "MyExceptionType";
            builder.Message = "MyMessageXFaults";

            //Cause Fault
            builder.BeginCauseFault();
            builder.ExceptionType = "MyCauseException";
            builder.Message = "MyCauseMessage";
            builder.BeginStack();
            builder.BeginStackFrame();
            builder.ClassName = "MyCauseClassaaa";
            builder.MethodName = "MyCauseMethodName";
            builder.FileName = "MyCauseFileName";
            builder.IsNativeMethod = true;
            builder.IsUnknownSource = true;
            builder.LineNumber = 23;
            builder.EndStackFrame();
            builder.EndStack();
            builder.EndCauseFault();

            //Finish up External fault
            builder.BeginStack();
            builder.BeginStackFrame();
            builder.ClassName = "MyExtClassa";
            builder.MethodName = "MyExtMethodName";
            builder.FileName = "MyExtFileName";
            builder.IsNativeMethod = true;
            builder.IsUnknownSource = true;
            builder.LineNumber = 33;
            builder.EndStackFrame();
            builder.EndStack();
            builder.EndFault();

            IFaultEvent toSave = builder.ToFaultEvent();

            toSave = faultRepository.Save(toSave);
            Assert.AreNotEqual(toSave.Id, 0);
            Assert.IsFalse(toSave.IsNew);

            IFaultEvent fetched = faultRepository.Fetch(toSave.Id);

        } 

        [Test]
        public void FetchTest_OneFault_MultipleStackFrames()
        {
            IResolver resolver = RegistryFactory.GetResolver();
            var faultRepository = resolver.Resolve<IFaultRepository>();
            var builder = resolver.Resolve<IFaultEventBuilder>();

            builder.Application = "MyAppXFrames";
            builder.Platform = "MyPlatformXFrames";
            builder.Machine = "MyMachineXFrames";
            builder.Timestamp = new DateTime(2011, 7, 8, 9, 49, 32);

            builder.BeginRequestorInformation();
            builder.Path = "MyXFPath";
            builder.Url = "MyXFUrl";
            builder.User = "MyXFUser";
            builder.UserHostAddress = "MyXFUserHost";
            builder.EndRequestorInformation();

            //Define Fault with a stack with multiple stack frames
            builder.BeginFault();
            builder.ExceptionType = "MyXFExceptionType";
            builder.Message = "MyMessageXFFrames";
            builder.BeginStack();
            builder.BeginStackFrame();
            builder.ClassName = "MyClass1aaa";
            builder.MethodName = "MyMethodName1";
            builder.FileName = "MyFileName1";
            builder.IsNativeMethod = true;
            builder.IsUnknownSource = true;
            builder.LineNumber = 24;
            builder.EndStackFrame();
            builder.BeginStackFrame();
            builder.ClassName = "MyClass2";
            builder.MethodName = "MyMethodName2";
            builder.FileName = "MyFileName2";
            builder.IsNativeMethod = true;
            builder.IsUnknownSource = true;
            builder.LineNumber = 34;
            builder.EndStackFrame();
            builder.BeginStackFrame();
            builder.ClassName = "MyClass3";
            builder.MethodName = "MyMethodName3";
            builder.FileName = "MyFileName3";
            builder.IsNativeMethod = true;
            builder.IsUnknownSource = true;
            builder.LineNumber = 44;
            builder.EndStackFrame();
            builder.EndStack();
            builder.EndFault();

            IFaultEvent toSave = builder.ToFaultEvent();

            toSave = faultRepository.Save(toSave);
            Assert.AreNotEqual(toSave.Id, 0);
            Assert.IsFalse(toSave.IsNew);

            IFaultEvent fetched = faultRepository.Fetch(toSave.Id);

        } 

        [Test]
        public void FetchTest_MultipleFaults_MultipleStackFrames()
        {
            IResolver resolver = RegistryFactory.GetResolver();
            var faultRepository = resolver.Resolve<IFaultRepository>();
            var builder = resolver.Resolve<IFaultEventBuilder>();

            builder.Application = "MyAppXFaultXFrames";
            builder.Platform = "MyPlatformXFaultXFrames";
            builder.Machine = "MyMachineXFaultXFrames";
            builder.Timestamp = new DateTime(2011, 7, 8, 9, 49, 32);

            builder.BeginRequestorInformation();
            builder.Path = "MyXFPath";
            builder.Url = "MyXFUrl";
            builder.User = "MyXFUser";
            builder.UserHostAddress = "MyXFUserHost";
            builder.EndRequestorInformation();

            //Define Fault with a stack with multiple stack frames
            builder.BeginFault();
            builder.ExceptionType = "MyInner1ExceptionType";
            builder.Message = "MyInner1Message";
            builder.BeginCauseFault();
            builder.ExceptionType = "MyInner2ExceptionType";
            builder.Message = "MyInner2Message";
            builder.BeginCauseFault();
            builder.ExceptionType = "MyInner3ExceptionType";
            builder.Message = "MyInner3Message";
            builder.BeginCauseFault();
            builder.ExceptionType = "MyInner4ExceptionType";
            builder.Message = "MyInner4Message";
            builder.BeginCauseFault();
            builder.ExceptionType = "MyInner5ExceptionType";
            builder.Message = "MyInner5Message";
            
            //build stack for cause 5
            builder.BeginStack();
            builder.BeginStackFrame();
            builder.ClassName = "MyClass5aaaa";
            builder.MethodName = "MyMethodName5a";
            builder.FileName = "MyFileName5a";
            builder.IsNativeMethod = true;
            builder.IsUnknownSource = true;
            builder.LineNumber = 55;
            builder.EndStackFrame();
            builder.BeginStackFrame();
            builder.ClassName = "MyClass5b";
            builder.MethodName = "MyMethodName5b";
            builder.FileName = "MyFileName5b";
            builder.IsNativeMethod = true;
            builder.IsUnknownSource = true;
            builder.LineNumber = 56;
            builder.EndStackFrame();
            builder.BeginStackFrame();
            builder.ClassName = "MyClass5c";
            builder.MethodName = "MyMethodName5c";
            builder.FileName = "MyFileName5c";
            builder.IsNativeMethod = true;
            builder.IsUnknownSource = true;
            builder.LineNumber = 57;
            builder.EndStackFrame();
            builder.EndStack();
            builder.EndCauseFault();

            //build stack for cause 4
            builder.BeginStack();
            builder.BeginStackFrame();
            builder.ClassName = "MyClass4a";
            builder.MethodName = "MyMethodName4a";
            builder.FileName = "MyFileName4a";
            builder.IsNativeMethod = true;
            builder.IsUnknownSource = true;
            builder.LineNumber = 45;
            builder.EndStackFrame();
            builder.BeginStackFrame();
            builder.ClassName = "MyClass4b";
            builder.MethodName = "MyMethodName4b";
            builder.FileName = "MyFileName4b";
            builder.IsNativeMethod = true;
            builder.IsUnknownSource = true;
            builder.LineNumber = 46;
            builder.EndStackFrame();
            builder.BeginStackFrame();
            builder.ClassName = "MyClass4c";
            builder.MethodName = "MyMethodName4c";
            builder.FileName = "MyFileName4c";
            builder.IsNativeMethod = true;
            builder.IsUnknownSource = true;
            builder.LineNumber = 47;
            builder.EndStackFrame();
            builder.EndStack();
            builder.EndCauseFault();


            //build stack for cause 3
            builder.BeginStack();
            builder.BeginStackFrame();
            builder.ClassName = "MyClass3a";
            builder.MethodName = "MyMethodName3a";
            builder.FileName = "MyFileName3a";
            builder.IsNativeMethod = true;
            builder.IsUnknownSource = true;
            builder.LineNumber = 35;
            builder.EndStackFrame();
            builder.BeginStackFrame();
            builder.ClassName = "MyClass3b";
            builder.MethodName = "MyMethodName3b";
            builder.FileName = "MyFileName3b";
            builder.IsNativeMethod = true;
            builder.IsUnknownSource = true;
            builder.LineNumber = 36;
            builder.EndStackFrame();
            builder.BeginStackFrame();
            builder.ClassName = "MyClass3c";
            builder.MethodName = "MyMethodName3c";
            builder.FileName = "MyFileName3c";
            builder.IsNativeMethod = true;
            builder.IsUnknownSource = true;
            builder.LineNumber = 37;
            builder.EndStackFrame();
            builder.EndStack();
            builder.EndCauseFault();


            //build stack for cause 2
            builder.BeginStack();
            builder.BeginStackFrame();
            builder.ClassName = "MyClass2a";
            builder.MethodName = "MyMethodName2a";
            builder.FileName = "MyFileName2a";
            builder.IsNativeMethod = true;
            builder.IsUnknownSource = true;
            builder.LineNumber = 25;
            builder.EndStackFrame();
            builder.BeginStackFrame();
            builder.ClassName = "MyClass2b";
            builder.MethodName = "MyMethodName2b";
            builder.FileName = "MyFileName2b";
            builder.IsNativeMethod = true;
            builder.IsUnknownSource = true;
            builder.LineNumber = 26;
            builder.EndStackFrame();
            builder.BeginStackFrame();
            builder.ClassName = "MyClass2c";
            builder.MethodName = "MyMethodName2c";
            builder.FileName = "MyFileName2c";
            builder.IsNativeMethod = true;
            builder.IsUnknownSource = true;
            builder.LineNumber = 27;
            builder.EndStackFrame();
            builder.EndStack();
            builder.EndCauseFault();

            //build stack for cause 1
            builder.BeginStack();
            builder.BeginStackFrame();
            builder.ClassName = "MyClass1a";
            builder.MethodName = "MyMethodName1a";
            builder.FileName = "MyFileName1a";
            builder.IsNativeMethod = true;
            builder.IsUnknownSource = true;
            builder.LineNumber = 15;
            builder.EndStackFrame();
            builder.BeginStackFrame();
            builder.ClassName = "MyClass1b";
            builder.MethodName = "MyMethodName1b";
            builder.FileName = "MyFileName1b";
            builder.IsNativeMethod = true;
            builder.IsUnknownSource = true;
            builder.LineNumber = 16;
            builder.EndStackFrame();
            builder.BeginStackFrame();
            builder.ClassName = "MyClass1c";
            builder.MethodName = "MyMethodName1c"; 
            builder.FileName = "MyFileName1c";
            builder.IsNativeMethod = true;
            builder.IsUnknownSource = true;
            builder.LineNumber = 17;
            builder.EndStackFrame();
            builder.EndStack();
            builder.EndFault();

            IFaultEvent toSave = builder.ToFaultEvent();

            toSave = faultRepository.Save(toSave);
            Assert.AreNotEqual(toSave.Id, 0);
            Assert.IsFalse(toSave.IsNew);

            IFaultEvent fetched = faultRepository.Fetch(toSave.Id);

        } 

        [Test]
        public void FetchTest_MultipleFaultDataElements()
        {
            IResolver resolver = RegistryFactory.GetResolver();
            var faultRepository = resolver.Resolve<IFaultRepository>();
            var builder = resolver.Resolve<IFaultEventBuilder>();

            builder.Application = "MyApp";
            builder.Platform = "MyPlatform";
            builder.Machine = "MyMachine";
            builder.Timestamp = new DateTime(2011, 6, 2, 9, 49, 32);

            builder.BeginRequestorInformation();
            builder.Path = "MyPath";
            builder.Url = "MyUrl";
            builder.User = "MyUser";
            builder.UserHostAddress = "MyUserHost";
            builder.EndRequestorInformation();

            builder.BeginFault();
            builder.ExceptionType = "MyExceptionType";
            builder.Message = "MyMessage";
            builder.BeginStack();
            builder.BeginStackFrame();
            builder.ClassName = "MyClassaaa";
            builder.MethodName = "MyMethodName";
            builder.FileName = "MyFileName";
            builder.EndStackFrame();
            builder.EndStack();
            builder.EndFault();

            builder.BeginFaultData();
            builder.BeginFaultDataItem();
            builder.Key = "data1";
            builder.Value = "value1";
            builder.EndFaultDataItem();
            builder.EndFaultData();
            builder.BeginFaultData();
            builder.BeginFaultDataItem();
            builder.Key = "data2";
            builder.Value = "value2";
            builder.EndFaultDataItem();
            builder.EndFaultData();

            IFaultEvent toSave = builder.ToFaultEvent();

            toSave = faultRepository.Save(toSave);
            Assert.AreNotEqual(toSave.Id, 0);
            Assert.IsFalse(toSave.IsNew);

            IFaultEvent fetched = faultRepository.Fetch(toSave.Id);

        }
        
        protected class NoCache : ICache
        {
            public object Add(string key, object value, DateTime absoluteExpiration, TimeSpan slidingExpiration)
            {
                return null;
            }

            public object Get(string key)
            {
                return null;
            }

            public object Remove(string key)
            {
                return null;
            }
        }

    }
}