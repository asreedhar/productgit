﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.Fault.DomainModel.Model;
using FirstLook.Fault.DomainModel.Model.Reporting;
using FirstLook.Fault.DomainModel.Repositories.Entities;
using FirstLook.Fault.DomainModel.Repositories.Gateways;

namespace FirstLook.Fault.DomainModel.Repositories.Mappers
{
    /// <summary>
    /// This mapper class will interface with the gateways to interact with the database. It contains the application logic
    /// to insert a fault into the database.
    /// </summary>
    public class FaultEventMapper
    {
        private readonly ReportingGateway _reportingGateway = new ReportingGateway();

        private readonly ApplicationGateway _applicationGateway = new ApplicationGateway();

        private readonly FaultDataGateway _faultDataGateway = new FaultDataGateway();

        private readonly FaultEventGateway _faultEventGateway = new FaultEventGateway();

        private readonly FaultGateway _faultGateway = new FaultGateway();

        private readonly MachineGateway _machineGateway = new MachineGateway();

        private readonly PlatformGateway _platformGateway = new PlatformGateway();

        private readonly RequestInformationGateway _requestInformationGateway = new RequestInformationGateway();

        private readonly StackFrameGateway _stackFrameGateway = new StackFrameGateway();

        private readonly StackGateway _stackGateway = new StackGateway();

        private readonly StackStackFrameGateway _stackStackFrameGateway = new StackStackFrameGateway();

        private readonly FaultEventFaultMessageGateway _faultEventFaultMessageGateway = new FaultEventFaultMessageGateway();

        private readonly MessageGateway _messageGateway = new MessageGateway();

        /// <summary>
        /// The logic that should be followed to save a FaultEvent
        /// 
        /// 1) Search for application name
        ///       Does it already exist
        ///       - Yes, if so then retrieve ID
        ///       - No, then add it and retrieve ID.
        /// 2) Search for machine name
        ///       Does it already exist
        ///       - Yes, if so then retrieve ID
        ///       - No, then add it and retrieve ID
        /// 3) Search for platform name
        ///       Does it already exist
        ///       - Yes, if so then retrieve ID
        ///       - No, then add it and retrieve ID
        /// 4) Check to see if hash code on Fault associated with FaultEvent is already in Fault table. 
        ///       - Yes, then add the the FaultEvent and tie it to the existing Fault record by using the FaultID
        ///       - No, then there's a bit more work to do
        ///    
        ///            Does the Stack associated with the Fault already exist in the Stack table (by searching for the hash code)
        ///            - Yes, Add an entry to the Fault table, associating it with the Stack entry.
        ///                   Insert a FaultEvent record associating it with the Fault entry just inserted.
        /// 
        ///            - No, insert the Stack record
        ///                  For each StackFrame within the Stack add an entry in the StackFrame table. Using the StackID to
        ///                   associate each StackFrame with the record in the Stack table.
        ///                  Insert a Fault record, associating it with the inserted Stack record
        ///                  Insert a FaultEvent record associating it with the Fault entry just inserted
        /// 5) Cycle through causing fault if one is provided, if a causing fault is provided, repeat step 4. Continue this process 
        ///    until the chain of causing faults ends. 
        /// 6) Insert the required FaultData records associated with the FaultEvent, associating each record to the FaultEvent record
        /// 7) Insert the RequestInformation record associated with the FaultEvent, associating each record to the FaultEvent record
        /// </summary>
        /// <param name="faultEvent">The FaultEvent to save.</param>
        /// <returns>The saved FaultEvent</returns>
        public IFaultEvent Save(IFaultEvent faultEvent)
        {
            int? applicationId = null;
            int? machineId = null;
            int? platformId = null;

            //Check for existing application, machine and platform. If not found then insert a new record into the 
            //corresponding table.
            if (!string.IsNullOrEmpty(faultEvent.Application.Name))
            {

                ApplicationGateway.Row applicationRow = _applicationGateway.Fetch(faultEvent.Application.Name) ??
                                                        _applicationGateway.Insert(faultEvent.Application.Name);
                applicationId = applicationRow.ApplicationId;
            }

            if (!string.IsNullOrEmpty(faultEvent.Machine.Name))
            {
                MachineGateway.Row machineRow = _machineGateway.Fetch(faultEvent.Machine.Name) ??
                                                _machineGateway.Insert(faultEvent.Machine.Name);
                machineId = machineRow.MachineId;
            }

            if (!string.IsNullOrEmpty(faultEvent.Platform.Name))
            {
                PlatformGateway.Row platformRow = _platformGateway.Fetch(faultEvent.Platform.Name) ??
                                                  _platformGateway.Insert(faultEvent.Platform.Name);
                platformId = platformRow.PlatformId;
            }

            //Get count of exceptions (+inner exceptions) ... default at least 1
            int count = 1;

            IFault temp = faultEvent.Fault.Cause;

            while (temp != null)
            {
                count++;
                temp = temp.Cause;
            }

            //Is the fault in question already stored in the database?
            FaultGateway.Row faultRow = _faultGateway.Fetch(faultEvent.Fault.Hash());
          
            //If not, then process the fault chain
            int faultRowId = 0;

            IList<int> chain;

            if (faultRow == null)
            {
                chain = AddFaultChain(faultEvent.Fault, new List<int>());

                if (chain.Count > 0)
                {
                    chain = chain.Reverse().ToList();    

                    faultRowId = chain[0];
                }
            }
            else
            {
                faultRowId = faultRow.FaultId;

                chain = RetrieveFaultChain(faultRow, count);
            }

            //Insert the faultevent into the faultevent database
            FaultEventGateway.Row faultEventRow = _faultEventGateway.Insert(applicationId, machineId,
                                                      platformId, faultRowId, faultEvent.Timestamp);

            //Process messages
            ProcessMessages(faultEventRow.FaultEventId, faultEvent.Fault, chain);
            
            //Check to see if there are key/value pairs associated with the exception thrown
            if (faultEvent.Data != null)
            {
                //If so add them to the FaultData table
                foreach (IFaultData data in faultEvent.Data)
                {
                    _faultDataGateway.Insert(faultEventRow.FaultEventId, data.Key,data.Value);
                }
            }

            //Add the request information if it is provided
            if(faultEvent.RequestInfo != null)
            {
                    _requestInformationGateway.Insert(faultEventRow.FaultEventId, faultEvent.RequestInfo.Path,
                                                      faultEvent.RequestInfo.Url, faultEvent.RequestInfo.User,
                                                      faultEvent.RequestInfo.UserHostAddress);
            }

            //Set the Id, indicating it has been saved
            FaultEvent e = (FaultEvent)faultEvent;
            e.Id = faultEventRow.FaultEventId;

            //Return the saved fault event.
            return e;
        }

        /// <summary>
        /// This method will be recursively called to handle faults and chained faults if any exist.
        /// </summary>
        /// <param name="faultToAdd">The fault to add.</param>
        /// <param name="faultIds"></param>
        /// <returns>After the recursion is complete this will contain a list of fault IDs starting with the inner most exception</returns>
        public IList<int> AddFaultChain(IFault faultToAdd, IList<int> faultIds)
        {
            //If there is no cause fault, then the recursion is over, add this fault and return.
            if(faultToAdd.Cause == null)
            {
                FaultGateway.Row faultRowCheck = AddFault(faultToAdd,null);
               
                faultIds.Add(faultRowCheck.FaultId);

                return faultIds;
            }

            //Process internal faults before this fault
            IList<int> chain = AddFaultChain(faultToAdd.Cause, faultIds);

            if (chain.Count > 0)
            {
                //Insert the fault
                FaultGateway.Row faultRowInserted = AddFault(faultToAdd, chain[chain.Count-1]);

                faultIds.Add(faultRowInserted.FaultId);
            }

            //Return the fault Id
            return faultIds;
        }

        public IList<int> RetrieveFaultChain(FaultGateway.Row row, int count)
        {
            IList<int> chain = new List<int>();

            int? faultId = row.FaultId;

            while (faultId != null && count > 0)
            {
                FaultGateway.Row causeRow = _faultGateway.Fetch((int)faultId);

                if (causeRow != null)
                {
                    chain.Add(causeRow.FaultId);
                }
                else
                {
                    break;
                }

                faultId = causeRow.CauseId;

                count--;
            }

            return chain;
        }

        public void ProcessMessages(int faultEventId, IFault fault, IList<int> chain)
        {
            IFault current = fault;

            int index = 0;

            foreach(int faultId in chain)
            {
                if (current != null)
                {
                    MessageGateway.Row msg = _messageGateway.Fetch(current.Message);

                    if (msg == null)
                    {
                        msg = _messageGateway.Insert(current.Message);
                    }

                    _faultEventFaultMessageGateway.Insert(faultEventId, faultId, msg.MessageId, index);

                    index++;

                    current = current.Cause;
                }
            }
        }

        /// <summary>
        /// Will add a fault to the fault table as well as a stack and stackframes.
        /// </summary>
        /// <param name="faultToAdd">The fault to add</param>
        /// <param name="causeId">A causing fault, if one is present.</param>
        /// <returns></returns>
        public FaultGateway.Row AddFault(IFault faultToAdd, int? causeId)
        {
            //Check to see if the fault is already present in the Fault table
            FaultGateway.Row faultRowCheck = _faultGateway.Fetch(faultToAdd.Hash());

            //If not add it
            if (faultRowCheck == null)
            {

                //Check to see if the stack trace is already in the Stack table
                StackGateway.Row stackRow = _stackGateway.Fetch(faultToAdd.Stack.Hash());

                //If not then add it
                if (stackRow == null)
                {
                    //Add the stack
                    stackRow = _stackGateway.Insert(faultToAdd.Stack.Hash());
                    short sequence = 0;

                    //Add the stack frames associated with the stack
                    foreach (IStackFrame frameToAdd in faultToAdd.Stack.Frames)
                    {
                        //If the stack frame is already in the stackframe table, do not add it, just add the stack/stackframe record in the stack_stackframe join table
                        //If its not in the stackframe table, add it and add the record in the join table.
                        StackFrameGateway.Row stackFrameRow = _stackFrameGateway.Fetch(frameToAdd.Hash()) ??
                                                              _stackFrameGateway.Insert(frameToAdd.ClassName, frameToAdd.FileName,
                                                                                                             frameToAdd.IsNativeMethod,
                                                                                                                                    frameToAdd.IsUnknownSource,
                                                                                                                                       frameToAdd.LineNumber,
                                                                                                                                       frameToAdd.MethodName,
                                                                                                                                       frameToAdd.Hash());


                        _stackStackFrameGateway.Insert(stackRow.StackId, stackFrameRow.StackFrameId, sequence);
                        sequence++;

                    }
                }

                //Insert the new fault, and return the fault row inserted
                faultRowCheck = _faultGateway.Insert(stackRow.StackId, causeId, faultToAdd.ExceptionType, faultToAdd.Hash());
            }

            return faultRowCheck;
        }

        public IFaultEvent Fetch(int id)
        {
            FaultEventBuilder builder = new FaultEventBuilder();

            #region FaultEvent

            FaultEventGateway.Row faultEventRow = _faultEventGateway.Fetch(id);

            if (faultEventRow.ApplicationId.HasValue)
            {
                ApplicationGateway.Row application = _applicationGateway.Fetch(faultEventRow.ApplicationId.Value);

                builder.Application = application.Name;
            }

            if (faultEventRow.MachineId.HasValue)
            {
                MachineGateway.Row machine = _machineGateway.Fetch(faultEventRow.MachineId.Value);

                builder.Machine = machine.Name;
            }

            if (faultEventRow.PlatformId.HasValue)
            {
                PlatformGateway.Row platform = _platformGateway.Fetch(faultEventRow.PlatformId.Value);

                builder.Platform = platform.Name;
            }

            builder.Timestamp = faultEventRow.FaultTime;

            AddFaultData(builder, faultEventRow.FaultEventId);

            AddRequestInformation(builder, faultEventRow.FaultId);

            #endregion

            #region Fault

            FaultGateway.Row faultRow = _faultGateway.Fetch(faultEventRow.FaultId);
         
            IList<FaultEventFaultMessageGateway.Row> faultMsgRows = _faultEventFaultMessageGateway.Fetch(id);
           
            string msg = null;

            if (faultMsgRows.Count() > 0)
            {
                msg = faultMsgRows[0].Message;
            }          

            builder.BeginFault();

            builder.ExceptionType = faultRow.ExceptionType;

            builder.Message = msg;

            AddStack(builder, faultRow.StackId);

            AddCause(builder, faultRow.CauseId, faultMsgRows, 1);
            
            builder.EndFault();

            #endregion

            IFaultEvent faultEvent = builder.ToFaultEvent();

            ((FaultEvent)faultEvent).IsNew = false;

            return faultEvent;
        }

        private void AddFaultData(FaultEventBuilder builder, int faultEventId)
        {
            IList<FaultDataGateway.Row> rows = _faultDataGateway.Fetch(faultEventId);

            builder.BeginFaultData();

            foreach (FaultDataGateway.Row row in rows)
            {
                builder.BeginFaultDataItem();

                builder.Key = row.Key;
                builder.Value = row.Value;

                builder.EndFaultDataItem();
            }

            builder.EndFaultData();
        }

        private void AddRequestInformation(FaultEventBuilder builder, int faultEventId)
        {
            RequestInformationGateway.Row row = _requestInformationGateway.Fetch(faultEventId);

            if (row != null)
            {
                builder.BeginRequestorInformation();

                builder.Path = row.Path;
                builder.Url = row.Url;
                builder.User = row.User;
                builder.UserHostAddress = row.UserHostAddress;

                builder.EndRequestorInformation();
            }
        }

        private void AddCause(FaultEventBuilder builder, int? causeId, IList<FaultEventFaultMessageGateway.Row> msgs, int index)
        {
            if (causeId.HasValue && index < msgs.Count)
            {
                FaultGateway.Row fault = _faultGateway.Fetch(causeId.Value);

                builder.BeginCauseFault();

                builder.ExceptionType = fault.ExceptionType;

                string msg = null;

                if (msgs.Count > index)
                {
                    msg = msgs[index].Message;

                    index++;
                }

                builder.Message = msg;

                AddStack(builder, fault.StackId);

                AddCause(builder, fault.CauseId, msgs, index);

                builder.EndCauseFault();
            }
        }

        private void AddStack(FaultEventBuilder builder, int stackId)
        {
            IList<StackFrameGateway.Row> rows = _stackFrameGateway.Fetch(stackId);

            builder.BeginStack();

            foreach (StackFrameGateway.Row row in rows)
            {
                builder.BeginStackFrame();

                builder.ClassName = row.ClassName;
                builder.FileName = row.FileName;
                builder.IsNativeMethod = row.IsNativeMethod;
                builder.IsUnknownSource = row.IsUnknownSource;
                builder.LineNumber = row.LineNumber;
                builder.MethodName = row.MethodName;

                builder.EndStackFrame();
            }

            builder.EndStack();
        }

        #region Reporting
        
        public IList<IApplicationSummary> ApplicationSummary()
        {
            return _reportingGateway.ApplicationSummary();
        }

        public Page<IFaultSummary> FaultSummary(string application, PageArguments pagination)
        {
            int? applicationId = null;

            if (!string.IsNullOrEmpty(application))
            {
                ApplicationGateway.Row row = _applicationGateway.Fetch(application);

                applicationId = row.ApplicationId;
            }

            return _reportingGateway.FaultSummary(applicationId, pagination);
        }

        public IFaultEventSummary FaultEventSummaryById(int faultId, int faultEventId)
        {
            return _reportingGateway.FaultEventSummaryById(faultId, faultEventId);
        }

        public IFaultEventSummary FaultEventSummaryByRank(int faultId, int rank)
        {
            return _reportingGateway.FaultEventSummaryByRank(faultId, rank);
        }

        #endregion
    }
}
