﻿using FirstLook.Common.Core.Registry;
using FirstLook.Fault.DomainModel.Model;

namespace FirstLook.Fault.DomainModel.Repositories
{
    /// <summary>
    /// The intent of this class is to add pertinent interfaces and implementing classes to the registry via the Configure method.
    /// </summary>
    public class Module : IModule
    {
        #region IModule Members

        /// <summary>
        /// Add pertinent interfaces and impelementing classes to the registry.
        /// </summary>
        public void Configure(IRegistry registry)
        {
            registry.Register<Datastores.Module>();

            registry.Register<IFaultEventBuilder, FaultEventBuilder>(ImplementationScope.Isolated);

            registry.Register<IFaultRepository, FaultRepository>(ImplementationScope.Shared);

        }

        #endregion
    }
}