﻿using System.Collections.Generic;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.Fault.DomainModel.Model;
using FirstLook.Fault.DomainModel.Model.Reporting;
using FirstLook.Fault.DomainModel.Repositories.Entities;
using FirstLook.Fault.DomainModel.Repositories.Mappers;

namespace FirstLook.Fault.DomainModel.Repositories
{
    /// <summary>
    /// This class is responsible for checking to see if we are trying to save a faultevent that has already been saved, 
    /// in which case we just return the already saved fault event.
    /// 
    /// Other wise it invokes the save operation to save the fault event within a single transaction.  That way if there is 
    /// a failure while saving, the entire transaction is rolled back and there is no parial fault event saved in the database.
    /// </summary>
    public class FaultRepository : RepositoryBase, IFaultRepository
    {
        private readonly FaultEventMapper _mapper = new FaultEventMapper();

        protected override string DatabaseName
        {
            get { return "Fault"; }
        }

        /// <summary>
        /// Check to see if we are saving a previously saved fault event.  If not, invoke the save operation to save 
        /// the fault event within a single transaction.
        /// </summary>
        /// <param name="faultEvent"> The fault event passed in to save to the db.</param>
        /// <returns>A saved fault event.</returns>
        public IFaultEvent Save(IFaultEvent faultEvent)
        {
            FaultEvent e = (FaultEvent) faultEvent; 

            //If this event has already been saved then don't save it again. Return the faultEvent to the caller.
            //Also, add a lock in case multiple threads being invoked on the same fault event object come through
            //roughly at the same time.
            lock (e)
            {
                if (e.IsNew)
                {
                    e.IsNew = false;
                }
                else
                {
                    return e;
                }
            }
            //Establishes a DB connection, runs the Save operation within a single transaction so it can be rolled 
            //back if an error is encountered and returns
            return DoInTransaction(() => _mapper.Save(e));
        }

        public IFaultEvent Fetch(int id)
        {
            return DoInSession(() => _mapper.Fetch(id));
        }

        #region Reporting

        public IList<IApplicationSummary> ApplicationSummary()
        {
            return DoInSession(() => _mapper.ApplicationSummary());
        }

        public Page<IFaultSummary> FaultSummary(string application, PageArguments pagination)
        {
            return DoInSession(() => _mapper.FaultSummary(application, pagination));
        }

        public IFaultEventSummary FaultEventSummaryById(int faultId, int faultEventId)
        {
            return DoInSession(() => _mapper.FaultEventSummaryById(faultId, faultEventId));
        }

        public IFaultEventSummary FaultEventSummaryByRank(int faultId, int rank)
        {
            return DoInSession(() => _mapper.FaultEventSummaryByRank(faultId, rank));
        }

        #endregion

        protected override void OnBeginTransaction(IDataSession session)
        {
            //deprecated method
        }
    }
}
