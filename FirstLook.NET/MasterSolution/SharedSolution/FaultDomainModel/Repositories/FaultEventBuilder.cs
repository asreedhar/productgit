﻿using System;
using System.Collections.Generic;
using FirstLook.Fault.DomainModel.Model;
using FirstLook.Fault.DomainModel.Repositories.Entities;
using Stack=FirstLook.Fault.DomainModel.Repositories.Entities.Stack;

namespace FirstLook.Fault.DomainModel.Repositories
{
    /// <summary>
    /// Logically this works as follows:
    /// 
    /// A FaultEventBuilder will get instantiated.
    /// 
    /// At that point a series of requests get made to the builder in order for the builder to construct an appropriate object graph
    /// representing a "FaultEvent".
    /// 
    /// A FaultEvent should have a structure similar to the following:
    /// 
    /// <!-- 
    /// <FaultEvent>
    ///       <Application> Value </Application>
    ///       <Machine> Value </Machine>
    ///       <Platform> Value </Platform>
    ///       <FaultData>
    ///           <Key> Key </Key>
    ///           <value> Value </value>   
    ///       </FaultData>
    ///       <FaultData>
    ///           <Key> Key </Key>
    ///           <value> Value </value>   
    ///       </FaultData>
    ///            .
    ///            .      Possibly multiple FaultData elements       
    ///            .
    ///       <RequestInformation>
    ///             <Path> Value </Path>
    ///             <Url>  Value </Url>
    ///             <User> Value </User>
    ///             <UserHostAddress> Value </UserHostAddress>
    ///       </RequestInformation>
    ///       <Fault> 
    ///           <ExceptionType> Value </ExceptionType>
    ///           <Message> Value </Message>
    ///           <Stack>
    ///               <StackTrace>
    ///                    <ClassName> Value </ClassName>
    ///                    <FileName> Value </FileName>
    ///                    <IsNativeMethod> Value </IsNativeMethod>
    ///                    <IsUnknownSource> Value </IsUnknownSource>
    ///                    <LineNumber> Value </LineNumber>
    ///                    <MethodName> Value </MethodName>
    ///               </StackTrace>
    ///               <StackTrace>
    ///                     ...
    ///               </StackTrace>
    ///                     .
    ///                     . Possibly multiple StackTrace elements
    ///                     .
    ///           <Fault>
    ///                <ExceptionType> Value </ExceptionType>
    ///                <Message> Value </Message>
    ///                <Stack>
    ///                   <StackTrace>
    ///                   ...
    ///                   </StackTrace>
    ///                   .
    ///                   . Possibly multiple StackTrace elements tied to inner faults
    ///                   .
    ///           </Fault>
    ///              . 
    ///              . Possibly multiple InnerFaults
    ///              .
    ///       </Fault>
    ///  </FaultEvent> 
    /// -->
    /// 
    /// In order to build an object graph, a sequence of calls would need to be made similar to the following:
    /// 
    /// builder.application
    /// builder.platform
    /// builder.machine
    /// beginfaultdata
    /// builder.key
    /// builder.value
    /// endfaultdata
    /// beginrequestinformation
    /// builder.path
    /// builder.url
    /// builder.user
    /// builder.userhostaddress
    /// endrequestinformation
    /// beginfault
    /// builder.exceptiontype(value)
    /// builder.message(value)
    /// beginstack
    /// beginstackframe
    /// builder.classname(class1)
    /// builder.filename(file1)
    /// builder.method(method1)
    /// endstackframe
    /// beginstackframe
    /// builder.classname(class2)
    /// builder.filename(file2)
    /// builder.method(method2)
    /// endstackframe
    /// endstack
    /// beginfault
    /// .
    /// .
    /// .
    /// endfault
    /// endfault
    /// 
    /// The structure of the code uses a strategy pattern.  Depending on what gets called a different strategy will get pushed
    /// onto the strategy stack.  It makes it easier to handle the sequence of events required to build a fault event.
    /// 
    /// Initially the faultevent strategy gets pushed onto the stack.  This allows the user to set attributes like the application,
    /// platform, machine etc.  If a buildfaultdata request is made, then a faultdata strategy is pushed on to the stack to handle
    /// a key value pair.  Similarly if a buildfault, buildstack or buildstackframe is called the corresponding strategy is pushed 
    /// on the stack to be handled appropriately. 
    /// </summary>
    public class FaultEventBuilder : IFaultEventBuilder
    {
        #region Implementation

        private readonly Stack<IFaultEventBuilder> _stack = new Stack<IFaultEventBuilder>();

        public FaultEventBuilder()
        {
            _stack.Push(new FaultEventStrategy(_stack));
        }

        private IFaultEventBuilder Current { get { return _stack.Peek(); } }

        public string Application
        {
            get { return Current.Application; }
            set { Current.Application = value; }
        }

        public string Machine
        {
            get { return Current.Machine; }
            set { Current.Machine = value; }
        }

        public string Platform
        {
            get { return Current.Platform; }
            set { Current.Platform = value; }
        }

        public DateTime Timestamp
        {
            get { return Current.Timestamp; }
            set { Current.Timestamp = value; }
        }

        public void BeginRequestorInformation()
        {
            Current.BeginRequestorInformation();
        }

        public string Path
        {
            get { return Current.Path; }
            set { Current.Path = value; }
        }

        public string Url
        {
            get { return Current.Url; }
            set { Current.Url = value; }
        }

        public string User
        {
            get { return Current.User; }
            set { Current.User = value; }
        }

        public string UserHostAddress
        {
            get { return Current.UserHostAddress; }
            set { Current.UserHostAddress = value; }
        }

        public void EndRequestorInformation()
        {
            Current.EndRequestorInformation();
        }

        public void BeginFaultData()
        {
            Current.BeginFaultData();
        }

        public void BeginFaultDataItem()
        {
            Current.BeginFaultDataItem();
        }

        public string Key
        {
            get { return Current.Key; }
            set { Current.Key = value; }
        }

        public string Value
        {
            get { return Current.Value; }
            set { Current.Value = value; }
        }

        public void EndFaultDataItem()
        {
            Current.EndFaultDataItem();
        }

        public void EndFaultData()
        {
            Current.EndFaultData();
        }

        public void BeginFault()
        {
            Current.BeginFault();
        }

        public string ExceptionType
        {
            get { return Current.ExceptionType; }
            set { Current.ExceptionType = value; }
        }

        public string Message
        {
            get { return Current.Message; }
            set { Current.Message = value; }
        }

        public void EndFault()
        {
            Current.EndFault();
        }

        public void BeginCauseFault()
        {
            Current.BeginCauseFault();
        }

        public void EndCauseFault()
        {
            Current.EndCauseFault();
        }

        public void BeginStack()
        {
            Current.BeginStack();
        }

        public void EndStack()
        {
            Current.EndStack();
        }

        public void BeginStackFrame()
        {
            Current.BeginStackFrame();
        }

        public string ClassName
        {
            get { return Current.ClassName; }
            set { Current.ClassName = value; }
        }

        public string FileName
        {
            get { return Current.FileName; }
            set { Current.FileName = value; }
        }

        public bool IsNativeMethod
        {
            get { return Current.IsNativeMethod; }
            set { Current.IsNativeMethod = value; }
        }

        public bool IsUnknownSource
        {
            get { return Current.IsUnknownSource; }
            set { Current.IsUnknownSource = value; }
        }

        public int? LineNumber
        {
            get { return Current.LineNumber; }
            set { Current.LineNumber = value; }
        }

        public string MethodName
        {
            get { return Current.MethodName; }
            set { Current.MethodName = value; }
        }

        public void EndStackFrame()
        {
            Current.EndStackFrame();
        }

        public IFaultEvent ToFaultEvent()
        {
            return Current.ToFaultEvent();
        }

        #endregion

        abstract class Strategy : IFaultEventBuilder
        {
            private readonly Stack<IFaultEventBuilder> _stack;

            protected Strategy(Stack<IFaultEventBuilder> stack)
            {
                _stack = stack;
            }

            protected Stack<IFaultEventBuilder> Stack
            {
                get { return _stack; }
            }

            #region IFaultEventBuilder Members

            public virtual string Application
            {
                get { throw new NotSupportedException(); }
                set { throw new NotSupportedException(); }
            }

            public virtual string Machine
            {
                get { throw new NotSupportedException(); }
                set { throw new NotSupportedException(); }
            }

            public virtual string Platform
            {
                get { throw new NotSupportedException(); }
                set { throw new NotSupportedException(); }
            }

            public virtual DateTime Timestamp
            {
                get { throw new NotSupportedException(); }
                set { throw new NotSupportedException(); }
            }

            public virtual void BeginRequestorInformation()
            {
                throw new NotSupportedException();
            }

            public virtual string Path
            {
                get { throw new NotSupportedException(); }
                set { throw new NotSupportedException(); }
            }

            public virtual string Url
            {
                get { throw new NotSupportedException(); }
                set { throw new NotSupportedException(); }
            }

            public virtual string User
            {
                get { throw new NotSupportedException(); }
                set { throw new NotSupportedException(); }
            }

            public virtual string UserHostAddress
            {
                get { throw new NotSupportedException(); }
                set { throw new NotSupportedException(); }
            }

            public virtual void EndRequestorInformation()
            {
                throw new NotSupportedException();
            }

            public virtual void BeginFaultData()
            {
                throw new NotSupportedException();
            }

            public virtual void BeginFaultDataItem()
            {
                throw new NotSupportedException();
            }

            public virtual string Key
            {
                get { throw new NotSupportedException(); }
                set { throw new NotSupportedException(); }
            }

            public virtual string Value
            {
                get { throw new NotSupportedException(); }
                set { throw new NotSupportedException(); }
            }

            public virtual void EndFaultDataItem()
            {
                throw new NotSupportedException();
            }

            public virtual void EndFaultData()
            {
                throw new NotSupportedException();
            }

            public virtual void BeginFault()
            {
                throw new NotSupportedException();
            }

            public virtual string ExceptionType
            {
                get { throw new NotSupportedException(); }
                set { throw new NotSupportedException(); }
            }

            public virtual string Message
            {
                get { throw new NotSupportedException(); }
                set { throw new NotSupportedException(); }
            }

            public virtual void EndFault()
            {
                throw new NotSupportedException();
            }

            public virtual void BeginCauseFault()
            {
                throw new NotSupportedException();
            }

            public virtual void EndCauseFault()
            {
                throw new NotSupportedException();
            }

            public virtual void BeginStack()
            {
                throw new NotSupportedException();
            }

            public virtual void EndStack()
            {
                throw new NotSupportedException();
            }

            public virtual void BeginStackFrame()
            {
                throw new NotSupportedException();
            }

            public virtual string ClassName
            {
                get { throw new NotSupportedException(); }
                set { throw new NotSupportedException(); }
            }

            public virtual string FileName
            {
                get { throw new NotSupportedException(); }
                set { throw new NotSupportedException(); }
            }

            public virtual bool IsNativeMethod
            {
                get { throw new NotSupportedException(); }
                set { throw new NotSupportedException(); }
            }

            public virtual bool IsUnknownSource
            {
                get { throw new NotSupportedException(); }
                set { throw new NotSupportedException(); }
            }

            public virtual int? LineNumber
            {
                get { throw new NotSupportedException(); }
                set { throw new NotSupportedException(); }
            }

            public virtual string MethodName
            {
                get { throw new NotSupportedException(); }
                set { throw new NotSupportedException(); }
            }

            public virtual void EndStackFrame()
            {
                throw new NotSupportedException();
            }

            public virtual IFaultEvent ToFaultEvent()
            {
                throw new NotSupportedException();
            }

            #endregion
        }

        class FaultEventStrategy : Strategy
        {
            private string _application;
            private string _machine;
            private string _platform;
            private DateTime _timestamp;
            private IFault _fault;
            private readonly IList<IFaultData> _data = new List<IFaultData>();
            private IRequestInformation _requestInformation;

            public IFault Fault
            {
                set { _fault = value; }
            }

            public IRequestInformation RequestInformation
            {
                set { _requestInformation = value; }
            }

            public FaultEventStrategy(Stack<IFaultEventBuilder> stack)
                : base(stack)
            {
            }

            public override string Application
            {
                get { return _application; }
                set { _application = value; }
            }

            public override string Machine
            {
                get { return _machine; }
                set { _machine = value; }
            }

            public override string Platform
            {
                get { return _platform; }
                set { _platform = value; }
            }

            public override DateTime Timestamp
            {
                get { return _timestamp; }
                set { _timestamp = value; }
            }

            public void ReturnDataValue(IEnumerable<IFaultData> list)
            {
                foreach (var item in list)
                {
                    _data.Add(item);
                }
            }

            public override void BeginRequestorInformation()
            {
                Stack.Push(new RequestorInformationStrategy(Stack,this));
            }

            public override void BeginFaultData()
            {
                Stack.Push(new FaultDataStrategy(Stack, this));
            }

            public override void BeginFault()
            {
                Stack.Push(new FaultStrategy(Stack, this));
            }

            public override IFaultEvent ToFaultEvent()
            {
                Application application = new Application {Name = _application};

                Machine machine = new Machine {Name = _machine};

                Platform platform = new Platform {Name = _platform};

                FaultEvent value = new FaultEvent
                                       {
                                           Application = application,
                                           Machine = machine,
                                           Platform = platform,
                                           Timestamp = _timestamp,
                                           Fault = _fault,
                                           Data = _data,
                                           IsNew = true,
                                           RequestInfo = _requestInformation
                                       };

                return value;
            }
        }

        class FaultStrategy : Strategy
        {
            private readonly FaultEventStrategy _parent;
            private string _exceptionType;
            private string _message;
            private IStack _stack;
            private IFault _causeFault;

            public IFault CauseFault
            {
                set { _causeFault = value; }
            }

            public void ReturnValue(IStack stack)
            {
                _stack = stack;
            }

            public FaultStrategy(Stack<IFaultEventBuilder> stack, FaultEventStrategy parent)
                : base(stack)
            {
                _parent = parent;
            }

            public override string ExceptionType
            {
                get { return _exceptionType; }
                set { _exceptionType = value; }
            }

            public override string Message
            {
                get { return _message; }
                set { _message = value; }
            }

            public override void EndFault()
            {
                Entities.Fault fault = new Entities.Fault
                                           {
                                               ExceptionType = _exceptionType,
                                               Message = _message,
                                               Stack = _stack,
                                               Cause = _causeFault
                                           };

                _parent.Fault = fault;
                
                Stack.Pop();
            }

            public override void BeginCauseFault()
            {
                Stack.Push(new CauseFaultStrategy(Stack, this));
            }

            public override void BeginStack()
            {
                Stack.Push(new StackStrategy(Stack, this));
            }
        }

        class CauseFaultStrategy : Strategy
        {
            private readonly FaultStrategy _parent;
            private readonly CauseFaultStrategy _causeFaultParent;
            private string _exceptionType;
            private string _message;
            private IStack _stack;
            private IFault _causeFault;

            public IFault CauseFault
            {
                set { _causeFault = value; }
            }

            public void ReturnValue(IStack stack)
            {
                _stack = stack;
            }

            public CauseFaultStrategy(Stack<IFaultEventBuilder> stack, FaultStrategy parent)
                : base(stack)
            {
                _parent = parent;
            }

            public CauseFaultStrategy(Stack<IFaultEventBuilder> stack, CauseFaultStrategy parent)
                : base(stack)
            {
                _causeFaultParent = parent;
            }

            public override string ExceptionType
            {
                get { return _exceptionType; }
                set { _exceptionType = value; }
            }

            public override string Message
            {
                get { return _message; }
                set { _message = value; }
            }

            public override void EndCauseFault()
            {
                Entities.Fault fault = new Entities.Fault
                                           {
                                               ExceptionType = _exceptionType,
                                               Message = _message,
                                               Stack = _stack,
                                               Cause = _causeFault
                                           };

                if (_parent != null)
                {
                    _parent.CauseFault = fault;
                }
                else
                {
                    _causeFaultParent.CauseFault = fault;
                }
                Stack.Pop();
            }

            public override void BeginCauseFault()
            {
                Stack.Push(new CauseFaultStrategy(Stack, this));
            }

            public override void BeginStack()
            {
                Stack.Push(new StackStrategy(Stack, this));
            }
        }

        class RequestorInformationStrategy : Strategy
        {
            private readonly FaultEventStrategy _parent;
            private string _path;
            private string _url;
            private string _user;
            private string _userHostAddress;
            private RequestInformation _requestInformation;


            public RequestorInformationStrategy(Stack<IFaultEventBuilder> stack, FaultEventStrategy parent)
                : base(stack)
            {
                _parent = parent;
            }

            public override string Path
            {
                get { return _path; }
                set { _path = value; }
            }

            public override string Url
            {
                get { return _url; }
                set { _url = value; }
            }

            public override string User
            {
                get { return _user; }
                set { _user = value; }
            }

            public override string UserHostAddress
            {
                get { return _userHostAddress; }
                set { _userHostAddress = value; }
            }

            public override void EndRequestorInformation()
            {
                _requestInformation = new RequestInformation
                                          {
                                              Path = _path,
                                              Url = _url,
                                              User = _user,
                                              UserHostAddress = _userHostAddress
                                          };

                _parent.RequestInformation = _requestInformation;

                Stack.Pop();
            }

        }

        class FaultDataStrategy : Strategy
        {
            private readonly FaultEventStrategy _parent;
            private readonly List<FaultData> _faultDataItems = new List<FaultData>();

            public FaultDataStrategy(Stack<IFaultEventBuilder> stack, FaultEventStrategy parent)
                : base(stack)
            {
                _parent = parent;
            }

            public override void BeginFaultDataItem()
            {
                Stack.Push(new FaultDataItemStrategy(Stack, this));
            }

            public void ReturnValue(FaultData faultData)
            {
                _faultDataItems.Add(faultData);
            }

            public override void EndFaultData()
            {
                _parent.ReturnDataValue(_faultDataItems);
                
                Stack.Pop();
            }

        }

        class FaultDataItemStrategy : Strategy
        {
            private readonly FaultDataStrategy _parent;
            private string _key;
            private string _value;
            private FaultData _faultData;

            public FaultDataItemStrategy(Stack<IFaultEventBuilder> stack, FaultDataStrategy parent)
                : base(stack)
            {
                _parent = parent;
            }

            public override string Key
            {
                get { return _key; }
                set { _key = value; }
            }

            public override string Value
            {
                get { return _value; }
                set { _value = value; }
            }

            public override void EndFaultDataItem()
            {
                _faultData = new FaultData { Key = _key, Value = _value };

                _parent.ReturnValue(_faultData);

                Stack.Pop();
            }
        }

        class StackStrategy : Strategy
        {
            private readonly FaultStrategy _parent;
            private readonly CauseFaultStrategy _causeFaultParent;
            private readonly Stack _stack = new Stack();

            public void ReturnValue(IStackFrame frame)
            {
                _stack.Frames.Add(frame);
            }

            public StackStrategy(Stack<IFaultEventBuilder> stack, FaultStrategy parent)
                : base(stack)
            {
                _parent = parent;
            }

            public StackStrategy(Stack<IFaultEventBuilder> stack, CauseFaultStrategy parent)
                : base(stack)
            {
                _causeFaultParent = parent;
            }


            public override void EndStack()
            {
                if(_parent != null)
                    _parent.ReturnValue(_stack);
                else
                {
                    _causeFaultParent.ReturnValue(_stack);
                }
                Stack.Pop();
            }

            public override void BeginStackFrame()
            {
                Stack.Push(new StackFrameStrategy(Stack, this));
            }
        }

        class StackFrameStrategy : Strategy
        {
            private readonly StackStrategy _parent;
            private string _className;
            private string _fileName;
            private bool _isNativeMethod;
            private bool _isUnknownSource;
            private int? _lineNumber;
            private string _methodName;


            public StackFrameStrategy(Stack<IFaultEventBuilder> stack, StackStrategy parent)
                : base(stack)
            {
                _parent = parent;
            }

            public override string ClassName
            {
                get { return _className; }
                set { _className = value; }
            }

            public override string FileName
            {
                get { return _fileName; }
                set { _fileName = value; }
            }

            public override bool IsNativeMethod
            {
                get { return _isNativeMethod; }
                set { _isNativeMethod = value; }
            }

            public override bool IsUnknownSource
            {
                get { return _isUnknownSource; }
                set { _isUnknownSource = value; }
            }

            public override int? LineNumber
            {
                get { return _lineNumber; }
                set { _lineNumber = value; }
            }

            public override string MethodName
            {
                get { return _methodName; }
                set { _methodName = value; }
            }

            public override void EndStackFrame()
            {
                StackFrame frame = new StackFrame
                                       {
                                           ClassName = _className,
                                           FileName = _fileName,
                                           IsNativeMethod = _isNativeMethod,
                                           IsUnknownSource = _isUnknownSource,
                                           LineNumber = _lineNumber,
                                           MethodName = _methodName
                                       };

                _parent.ReturnValue(frame);

                Stack.Pop();
            }
        }
    }
}