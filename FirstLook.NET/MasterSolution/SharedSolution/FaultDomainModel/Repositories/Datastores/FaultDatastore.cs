﻿using System;
using System.Data;
using System.Reflection;
using FirstLook.Common.Core.Data;

namespace FirstLook.Fault.DomainModel.Repositories.Datastores
{
    /// <summary>
    /// This class is responsible for the inserting and fetching from the Faults database.
    /// It is invoked via the table gateways.
    /// </summary>
    public class FaultDatastore : SessionDataStore, IFaultDatastore
    {
        internal const string Prefix = "FirstLook.Fault.DomainModel.Repositories.Resources";

        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        #region IFaultDatastore Members

        /// <summary>
        /// Fetch a record from the FaultEvent table where the primary key is faultEventId
        /// </summary>
        /// <param name="faultEventId">Fetch the record from the table where FaultEventID = faultEventId.</param>
        /// <returns>A DataReader containing the returned row(s).</returns>
        public IDataReader FaultEvent_Fetch(int faultEventId)
        {
            const string queryName = Prefix + ".FaultDatastore_FaultEvent_Fetch.txt";

            return Query(
                new[] { "FaultEvent" },
                queryName,
                 new Parameter("FaultEventID", faultEventId, DbType.Int32));
        }

        public IDataReader FaultEvent_Fault_Count(int faultEventId)
        {
            const string queryName = Prefix + ".FaultDatastore_FaultEvent_Fault_Count_Fetch.txt";

            return Query(
                new[] { "FaultEvent_Fault_Message" },
                queryName,
                 new Parameter("FaultEventID", faultEventId, DbType.Int32));
        }

        /// <summary>
        /// Fetch a record from the Application table where the name is equal to the application string.
        /// </summary>
        /// <param name="application">The name of the application to search the Application table for.</param>
        /// <returns>A DataReader containing the returned row(s).</returns>
        public IDataReader Application_Fetch(string application)
        {
            const string queryName = Prefix + ".FaultDatastore_Application_Fetch_ByName.txt";

            return Query(
                new[] {"Application"},
                queryName,
                new Parameter("Name", application, DbType.String));
        }

        public IDataReader Application_Fetch(int id)
        {
            const string queryName = Prefix + ".FaultDatastore_Application_Fetch_ById.txt";

            return Query(
                new[] { "Application" },
                queryName,
                new Parameter("ApplicationID", id, DbType.Int32));
        }

        /// <summary>
        /// Fetch a record from the Machine table where name is equal to the machine string.
        /// </summary>
        /// <param name="machine">The name of the machine to search the Machine table for.</param>
        /// <returns>A DataReader containing the returned row(s).</returns>
        public IDataReader Machine_Fetch(string machine)
        {
            const string queryName = Prefix + ".FaultDatastore_Machine_Fetch_ByName.txt";

            return Query(
                new[] { "Machine" },
                queryName,
                new Parameter("Name", machine, DbType.String));
        }

        public IDataReader Machine_Fetch(int id)
        {
            const string queryName = Prefix + ".FaultDatastore_Machine_Fetch_ById.txt";

            return Query(
                new[] { "Machine" },
                queryName,
                new Parameter("MachineID", id, DbType.Int32));
        }

        /// <summary>
        /// Fetch a record from the Platform table where name is equal to the platform string.
        /// </summary>
        /// <param name="platform">The name of the platform to search the Platform table for.</param>
        /// <returns>A DataReader containing the returned row(s).</returns>
        public IDataReader Platform_Fetch(string platform)
        {
            const string queryName = Prefix + ".FaultDatastore_Platform_Fetch_ByName.txt";

            return Query(
                new[] { "Platform" },
                queryName,
                new Parameter("Name", platform, DbType.String));
        }

        public IDataReader Platform_Fetch(int id)
        {
            const string queryName = Prefix + ".FaultDatastore_Platform_Fetch_ById.txt";

            return Query(
                new[] { "Platform" },
                queryName,
                new Parameter("PlatformID", id, DbType.Int32));
        }

        /// <summary>
        /// Fetch a record from the Fault table where the hashcode is equal to the passed in hash value.
        /// </summary>
        /// <param name="hashCode">A unique value per Fault.</param>
        /// <returns>A DataReader containing the returned row(s).</returns>
        public IDataReader Fault_Fetch(long hashCode)
        {
            const string queryName = Prefix + ".FaultDatastore_Fault_Fetch_ByHashCode.txt";

            return Query(
                new[] { "Fault" },
                queryName,
                new Parameter("HashCode", hashCode, DbType.Int64));
        }

        public IDataReader Fault_Fetch(int id)
        {
            const string queryName = Prefix + ".FaultDatastore_Fault_Fetch_ById.txt";

            return Query(
                new[] { "Fault" },
                queryName,
                new Parameter("FaultID", id, DbType.Int32));
        }

        /// <summary>
        /// Fetch record(s) from the FaultData table in which FaultEventID = faultEventId.
        /// </summary>
        /// <param name="faultEventId">Return all records from the FaultData table where FaultEventID is equal to this value.</param>
        /// <returns>A DataReader containing the returned row(s).</returns>
        public IDataReader FaultData_Fetch(int faultEventId)
        {
            const string queryName = Prefix + ".FaultDatastore_FaultData_Fetch.txt";

            return Query(
                new[] { "FaultData" },
                queryName,
                new Parameter("FaultEventID", faultEventId, DbType.Int32));
        }

        /// <summary>
        /// Fetch a record from the Stack table that matches the unique hash code passed in.
        /// </summary>
        /// <param name="hashCode">Return the record from the stack table that has a matching hashcode value</param>
        /// <returns>A DataReader containing the returned row(s).</returns>
        public IDataReader Stack_Fetch(long hashCode)
        {
            const string queryName = Prefix + ".FaultDatastore_Stack_Fetch_ByHashCode.txt";

            return Query(
                new[] { "Stack" },
                queryName,
                new Parameter("HashCode", hashCode, DbType.Int64));
        }

        /// <summary>
        /// Fetch a record from the Stack_StackFrame table that matches the unique stackid/stackframeid combination
        /// </summary>
        /// <param name="stackId">Id of the stack in the stack/stackframe combination</param>
        /// <param name="stackFrameId">Id of the stack frame in the stack/stackframe combination</param>
        /// <param name="sequenceId">Id of the frame sequence within the stack trace.</param>
        /// <returns>The record containing the stackid/stackframeid combination.</returns>
        public IDataReader Stack_StackFrame_Fetch(int stackId, int stackFrameId, short sequenceId)
        {
            const string queryName = Prefix + ".FaultDatastore_Stack_StackFrame_Fetch.txt";

            return Query(
                new[] {"Stack_StackFrame"},
                queryName,
                new Parameter("StackID", stackId, DbType.Int32),
                new Parameter("StackFrameID", stackFrameId, DbType.Int32),
                new Parameter("SequenceID", sequenceId, DbType.Int16));
        }

        /// <summary>
        /// Fetch a record from the RequestInformation table that has FaultEventId = faultEventId
        /// </summary>
        /// <param name="faultEventId">Return the record from the RequestInformation table that has a matching faultEventId value</param>
        /// <returns>A DataReader containing the returned row(s).</returns>
        public IDataReader RequestInformation_Fetch(int faultEventId)
        {
            const string queryName = Prefix + ".FaultDatastore_RequestInformation_Fetch.txt";

            return Query(
                new[] { "RequestInformation" },
                queryName,
                new Parameter("FaultEventID", faultEventId, DbType.Int32));
        }

        /// <summary>
        /// Fetch the stack frame with the given hashcode.
        /// </summary>
        /// <param name="hashCode">The hashcode of the stack frame to match on.</param>
        /// <returns>A DataReader containing the returned row.</returns>
        public IDataReader StackFrame_Fetch(long hashCode)
        {
            const string queryName = Prefix + ".FaultDatastore_StackFrame_Fetch_ByHashCode.txt";

            return Query(
                new[] { "StackFrame" },
                queryName,
                new Parameter("HashCode", hashCode, DbType.Int64));
        }

        public IDataReader StackFrame_Fetch(int stackId)
        {
            const string queryName = Prefix + ".FaultDatastore_StackFrame_Fetch_ByStackId.txt";

            return Query(
                new[] { "StackFrame" },
                queryName,
                new Parameter("StackId", stackId, DbType.Int32));
        }

        /// <summary>
        /// Insert a new fault event record in the FaultEvent table.
        /// </summary>
        /// <param name="applicationId">Application name.</param>
        /// <param name="machineId">Machine name that generated exception. </param>
        /// <param name="platformId">Platform name.</param>
        /// <param name="faultId">A foreign key to the Fault table, to link the fault to the fault event.</param>
        /// <param name="faultTime">The timestamp of the occuring fault.</param>
        /// <returns>Returns a datareader to retrieve information from inserted row (like IDENTITY values).</returns>
        public IDataReader FaultEvent_Insert(int? applicationId, int? machineId, int? platformId, int faultId,
                                             DateTime faultTime)
        {
            const string queryNameI = Prefix + ".FaultDatastore_FaultEvent_Insert.txt";

            const string queryNameF = Prefix + ".FaultDatastore_FaultEvent_Fetch_Identity.txt";

            NonQuery(
                queryNameI,
                new Parameter("ApplicationID", applicationId, DbType.Int32),
                new Parameter("MachineID", machineId, DbType.Int32),
                new Parameter("PlatformID", platformId, DbType.Int32),
                new Parameter("FaultID", faultId, DbType.Int32),
                new Parameter("FaultTime", faultTime, DbType.DateTime));

            return Query(
                new[] { "FaultEvent" },
                queryNameF); 
        }

        /// <summary>
        /// Insert a record into the Application table.
        /// </summary>
        /// <param name="name">The name of the application to insert.</param>
        /// <returns>Returns a datareader to retrieve information from inserted row (like IDENTITY values).</returns>
        public IDataReader Application_Insert(string name)
        {
            const string queryNameI = Prefix + ".FaultDatastore_Application_Insert.txt";

            const string queryNameF = Prefix + ".FaultDatastore_Application_Fetch_Identity.txt";

            name = CheckString(name, 50);

            NonQuery(
                queryNameI,
                new Parameter("Name", name, DbType.String));

            return Query(
                new[] { "Application" },
                queryNameF); 
        }

        /// <summary>
        /// Insert a record into the Machine table.
        /// </summary>
        /// <param name="name">The name of the machine to insert.</param>
        /// <returns>Returns a datareader to retrieve information from inserted row (like IDENTITY values).</returns>
        public IDataReader Machine_Insert(string name)
        {
            const string queryNameI = Prefix + ".FaultDatastore_Machine_Insert.txt";

            const string queryNameF = Prefix + ".FaultDatastore_Machine_Fetch_Identity.txt";

            name = CheckString(name, 50);

            NonQuery(
                queryNameI,
                new Parameter("Name", name, DbType.String));

            return Query(
                new[] { "Machine" },
                queryNameF); 
        }

        /// <summary>
        /// Insert a record into the Platform table.
        /// </summary>
        /// <param name="name">The name of the platform to insert.</param>
        /// <returns>Returns a datareader to retrieve information from inserted row (like IDENTITY values).</returns>
        public IDataReader Platform_Insert(string name)
        {
            const string queryNameI = Prefix + ".FaultDatastore_Platform_Insert.txt";

            const string queryNameF = Prefix + ".FaultDatastore_Platform_Fetch_Identity.txt";

            name = CheckString(name, 50);

            NonQuery(
                queryNameI,
                new Parameter("Name", name, DbType.String));

            return Query(
                new[] { "Platform" },
                queryNameF); 
        }

        /// <summary>
        /// Insert a fault into the Fault table.
        /// </summary>
        /// <param name="stackId">Foreign key of record from the stack table to tie a stack trace to this fault.</param>
        /// <param name="causeId">A foreign key to the same table to indicate a causing fault.</param>
        /// <param name="exceptionType">The type of exception generated.</param>
        /// <param name="hashCode">A unique value for the fault.</param>
        /// <returns>Returns a datareader to retrieve information from inserted row (like IDENTITY values).</returns>
        public IDataReader Fault_Insert(int stackId, int? causeId, string exceptionType, long hashCode)
        {
            const string queryNameI = Prefix + ".FaultDatastore_Fault_Insert.txt";

            const string queryNameF = Prefix + ".FaultDatastore_Fault_Fetch_Identity.txt";

            exceptionType = exceptionType.Length > 512 ? exceptionType.Substring(0,512) : exceptionType;

            NonQuery(
                queryNameI,
                new Parameter("StackID", stackId, DbType.Int32),
                new Parameter("CauseID", causeId, DbType.Int32),
                new Parameter("ExceptionType", exceptionType, DbType.String),
                new Parameter("HashCode", hashCode, DbType.Int64));

            return Query(
                    new[] { "Fault" },
                            queryNameF); 
        }

        /// <summary>
        /// Insert a key/value pair to represent fault data associated with the exception.
        /// </summary>
        /// <param name="faultEventId">A foreign key to the fault event.</param>
        /// <param name="key">The key used for the key value pair.</param>
        /// <param name="value">The value of the key value pair.</param>
        /// <returns>Returns a datareader to retrieve information from inserted row (like IDENTITY values).</returns>
        public IDataReader FaultData_Insert(int faultEventId, string key, string value)
        {
            const string queryNameI = Prefix + ".FaultDatastore_FaultData_Insert.txt";

            const string queryNameF = Prefix + ".FaultDatastore_FaultData_Fetch_Identity.txt";

            key = key.Length > 256 ? key.Substring(0, 256) : key;
            value = value.Length > 2000 ? value.Substring(0, 2000) : value;

            NonQuery(
                queryNameI,
                new Parameter("FaultEventID", faultEventId, DbType.Int32),
                new Parameter("Key", key, DbType.String),
                new Parameter("Value", value, DbType.String));

           return Query(
                new[] { "FaultData" },
                queryNameF); 
        }

        /// <summary>
        /// Insert a stack record into the table.
        /// </summary>
        /// <param name="hashCode">A unique value per stack record.</param>
        /// <returns>Returns a datareader to retrieve information from inserted row (like IDENTITY values).</returns>
        public IDataReader Stack_Insert(long hashCode)
        {
            const string queryNameI = Prefix + ".FaultDatastore_Stack_Insert.txt";

            const string queryNameF = Prefix + ".FaultDatastore_Stack_Fetch_Identity.txt";

            NonQuery(
                queryNameI,
                new Parameter("HashCode", hashCode, DbType.Int64));

            return Query(
                new[] { "Stack" },
                queryNameF); 
        }



        /// <summary>
        /// Insert a record into the Stack_StackFrame table that matches the unique stackid/stackframeid combination
        /// </summary>
        /// <param name="stackId">Id of the stack in the stack/stackframe combination</param>
        /// <param name="stackFrameId">Id of the stack frame in the stack/stackframe combination</param>
        /// <param name="sequenceId">Id of the frame sequence within the stack trace.</param>
        /// <returns>The record inserted.</returns>
        public IDataReader Stack_StackFrame_Insert(int stackId, int stackFrameId, short sequenceId )
        {
            const string queryNameI = Prefix + ".FaultDatastore_Stack_StackFrame_Insert.txt";

            const string queryNameF = Prefix + ".FaultDatastore_Stack_StackFrame_Fetch.txt";

            NonQuery(
                queryNameI,
                new Parameter("StackID", stackId, DbType.Int32),
                new Parameter("StackFrameID", stackFrameId, DbType.Int32),
                new Parameter("SequenceID", sequenceId, DbType.Int16));
           
            return Query(
                new[] { "Stack_StackFrame" },
                queryNameF,
                new Parameter("StackID", stackId, DbType.Int32),
                new Parameter("StackFrameID", stackFrameId, DbType.Int32),
                new Parameter("SequenceID", sequenceId, DbType.Int16));
        }

        /// <summary>
        /// Insert information specific to the request coming in.
        /// </summary>
        /// <param name="faultEventId">A foreign key to the fault event.</param>
        /// <param name="path">The path associated with the exception request.</param>
        /// <param name="url">The url associated with the exception request.</param>
        /// <param name="user">The user making the request.</param>
        /// <param name="userHostAddress">The user host address of the requestor.</param>
        /// <returns>Returns a datareader to retrieve information from inserted row (like IDENTITY values).</returns>
        public IDataReader RequestInformation_Insert(int faultEventId, string path, string url, string user,
                                                     string userHostAddress)
        {
            const string queryNameI = Prefix + ".FaultDatastore_RequestInformation_Insert.txt";

            const string queryNameF = Prefix + ".FaultDatastore_RequestInformation_Fetch_Identity.txt";

            path = CheckString(path, 2000);
            url = CheckString(url, 2000);
            user = CheckString(user, 200);
            userHostAddress = CheckString(userHostAddress, 15);

            NonQuery(
                queryNameI,
                new Parameter("FaultEventID", faultEventId, DbType.Int32),
                new Parameter("Url", url, DbType.String),
                new Parameter("Path", path, DbType.String),
                new Parameter("User", user, DbType.String),
                new Parameter("UserHostAddress", userHostAddress, DbType.String));

            return Query(
                new[] { "RequestInformation" },
                queryNameF); 
        }

        /// <summary>
        /// Insert a stack frame into the StackFrame table.
        /// </summary>
        /// <param name="className">Class name tied to exception.</param>
        /// <param name="fileName">File name tied to exception.</param>
        /// <param name="isNativeMethod">Boolean value determining if this is a native method.</param>
        /// <param name="isUnknownSource">Boolean value determining if this was derived from unknown source</param>
        /// <param name="lineNumber">The line number of the exception.</param>
        /// <param name="methodName">The method name associated with the exception.</param>
        /// <param name="hashCode">A unique value per stack frame.</param>
        /// <returns>Returns a datareader to retrieve information from inserted row (like IDENTITY values).</returns>
        public IDataReader StackFrame_Insert(string className, string fileName, bool isNativeMethod,
                                             bool isUnknownSource, int? lineNumber, string methodName, long hashCode)
        {
            const string queryNameI = Prefix + ".FaultDatastore_StackFrame_Insert.txt";

            const string queryNameF = Prefix + ".FaultDatastore_StackFrame_Fetch_Identity.txt";


            className = CheckString(className, 2000);
            fileName = CheckString(fileName, 2000);
            methodName = CheckString(methodName, 2000);

            NonQuery(
                queryNameI,
                new Parameter("ClassName", className, DbType.String),
                new Parameter("FileName", fileName, DbType.String),
                new Parameter("IsNativeMethod", isNativeMethod, DbType.Boolean),
                new Parameter("IsUnknownSource", isUnknownSource, DbType.Boolean),
                new Parameter("LineNumber", lineNumber, DbType.Int32),
                new Parameter("MethodName", methodName, DbType.String),
                new Parameter("HashCode", hashCode, DbType.Int64));

            return Query(
                new[] { "StackFrame" },
                queryNameF); 
        }

        public IDataReader Message_Fetch(string message)
        {
            const string queryName = Prefix + ".FaultDatastore_Message_Fetch_ByMessage.txt";

            message = CheckString(message, 1024);

            return Query(
                new[] { "Message" },
                queryName,
                new Parameter("Message", message, DbType.String));
        }

        public IDataReader Message_Fetch(int id)
        {
            const string queryName = Prefix + ".FaultDatastore_Message_Fetch_ById.txt";

            return Query(
                new[] { "Message" },
                queryName,
                new Parameter("MessageID", id, DbType.Int32));
        }

        public IDataReader Message_Insert(string message)
        {
            const string queryNameI = Prefix + ".FaultDatastore_Message_Insert.txt";

            const string queryNameF = Prefix + ".FaultDatastore_Message_Fetch_Identity.txt";

            message = CheckString(message, 1024);

            NonQuery(
                queryNameI,
                new Parameter("Message", message, DbType.String));

            return Query(
                new[] { "Message" },
                queryNameF);
        }

        public IDataReader FaultEvent_Fault_Message_Fetch(int faultEventId)
        {
            const string queryName = Prefix + ".FaultDatastore_FaultEvent_Fault_Message_Fetch.txt";

            return Query(
                new[] { "FaultEvent_Fault_Message" },
                queryName,
                new Parameter("FaultEventID", faultEventId, DbType.Int32));
        }

        public void FaultEvent_Fault_Message_Insert(int faultEventId, int faultId, int messageId, int index)
        {
            const string queryNameI = Prefix + ".FaultDatastore_FaultEvent_Fault_Message_Insert.txt";

            NonQuery(
                queryNameI,
                new Parameter("FaultID", faultId, DbType.Int32),
                new Parameter("FaultEventID", faultEventId, DbType.Int32),
                new Parameter("Index", index, DbType.Int32),
                new Parameter("MessageID", messageId, DbType.Int32));
        }

        #endregion

        #region Dashboard

        internal const string PrefixDashboard = "FirstLook.Fault.DomainModel.Repositories.Resources.Dashboard";

        public IDataReader ReportSummaryApplication()
        {
            const string queryName = PrefixDashboard + ".FaultDatastore_Dashboard_Summary_Application.txt";

            return Query(
                new[] { "Summary_Application" },
                queryName);
        }

        public IDataReader ReportSummaryFault(int? applicationId, string sortExpression, int startRowIndex, int maximumRows)
        {
            const string queryName = PrefixDashboard + ".FaultDatastore_Dashboard_Summary_Fault.txt";

            Parameter p = applicationId.HasValue
                              ? new Parameter("ApplicationID", applicationId.Value, DbType.Byte)
                              : new Parameter("ApplicationID", null, DbType.Byte);

            return Query(
                new[] { "Summary_Fault", "TotalRowCount" },
                queryName,
                p,
                new Parameter("SortExpression", sortExpression, DbType.String),
                new Parameter("StartRowIndex", startRowIndex, DbType.Int32),
                new Parameter("MaximumRows", maximumRows, DbType.Int32));
        }

        public IDataReader ReportSummaryFaultEventById(int faultId, int faultEventId)
        {
            const string queryName = PrefixDashboard + ".FaultDatastore_Dashboard_Summary_FaultEvent_ById.txt";

            return Query(
                new[] { "Details_FaultEvent_ById" },
                queryName,
                new Parameter("FaultEventID", faultEventId, DbType.Int32),
                new Parameter("FaultID", faultId, DbType.Int32));
        }

        public IDataReader ReportSummaryFaultEventByRank(int faultId, int faultRank)
        {
            const string queryName = PrefixDashboard + ".FaultDatastore_Dashboard_Summary_FaultEvent_ByRank.txt";

            return Query(
                new[] { "Details_FaultEvent_ByRank" },
                queryName,
                new Parameter("FaultRank", faultRank, DbType.Int32),
                new Parameter("FaultID", faultId, DbType.Int32));
        }

        public IDataReader ReportDetailsStack(int faultId)
        {
            const string queryName = PrefixDashboard + ".FaultDatastore_Dashboard_Details_Stack.txt";

            return Query(
                new[] { "Details_Stack" },
                queryName,
                new Parameter("FaultID", faultId, DbType.Int32));
        }

        public IDataReader ReportDetailsStackFrame(int stackId)
        {
            const string queryName = PrefixDashboard + ".FaultDatastore_Dashboard_Details_StackFrame.txt";

            return Query(
                new[] { "Details_StackFrame" },
                queryName,
                new Parameter("StackID", stackId, DbType.Int32));
        }

        #endregion

        /// <summary>
        /// This method will truncate the passed in string if necessary so that the length of the string does not exceed the passed in length.
        /// If the string is empty or null, null is returned.  Otherwise the truncated string is returned.
        /// </summary>
        /// <param name="value">The string being checked.</param>
        /// <param name="length">The length being checked against.</param>
        /// <returns>Truncated string or null</returns>
        public string CheckString(string value, int length)
        {
            if(!string.IsNullOrEmpty(value))
            {
                value = value.Length > length ? value.Substring(0, length) : value;
                return value;
            }
            return null;
        }
    }

  
}