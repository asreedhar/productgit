﻿using System;
using System.Data;

namespace FirstLook.Fault.DomainModel.Repositories.Datastores
{
    /// <summary>
    /// The interface implemented by the FaultDatastore to insert/fetch rows from the Fault database.
    /// This interface is invoked from the table gateways.
    /// </summary>
    public interface IFaultDatastore
    {
        /// <summary>
        /// Fetch a record from the FaultEvent table where the primary key is faultEventId
        /// </summary>
        /// <param name="faultEventId">Fetch the record from the table where FaultEventID = faultEventId.</param>
        /// <returns>A DataReader containing the returned row(s).</returns>
        IDataReader FaultEvent_Fetch(int faultEventId);

        IDataReader FaultEvent_Fault_Count(int faultEventId);

        /// <summary>
        /// Fetch a record from the Application table where name is equal to the application string.
        /// </summary>
        /// <param name="application">The name of the application to search the Application table for.</param>
        /// <returns>A DataReader containing the returned row(s).</returns>
        IDataReader Application_Fetch(string application);

        IDataReader Application_Fetch(int id);

        /// <summary>
        /// Fetch a record from the Machine table where name is equal to the machine string.
        /// </summary>
        /// <param name="machine">The name of the machine to search the Machine table for.</param>
        /// <returns>A DataReader containing the returned row(s).</returns>
        IDataReader Machine_Fetch(string machine);

        IDataReader Machine_Fetch(int id);

        /// <summary>
        /// Fetch a record from the Platform table where name is equal to the platform string.
        /// </summary>
        /// <param name="platform">The name of the platform to search the Platform table for.</param>
        /// <returns>A DataReader containing the returned row(s).</returns>
        IDataReader Platform_Fetch(string platform);

        IDataReader Platform_Fetch(int id);

        /// <summary>
        /// Fetch a record from the Fault table where the hashcode is equal to the passed in hash value.
        /// </summary>
        /// <param name="hashCode">A unique value per Fault.</param>
        /// <returns>A DataReader containing the returned row(s).</returns>
        IDataReader Fault_Fetch(long hashCode);

        IDataReader Fault_Fetch(int id);

        /// <summary>
        /// Fetch record(s) from the FaultData table in which FaultEventID = faultEventId.
        /// </summary>
        /// <param name="faultEventId">Return all records from the FaultData table where FaultEventID is equal to this value.</param>
        /// <returns>A DataReader containing the returned row(s).</returns>
        IDataReader FaultData_Fetch(int faultEventId);

        /// <summary>
        /// Fetch a record from the Stack table that matches the unique hash code passed in.
        /// </summary>
        /// <param name="hashCode">Return the record from the stack table that has a matching hashcode value</param>
        /// <returns>A DataReader containing the returned row(s).</returns>
        IDataReader Stack_Fetch(long hashCode);

        /// <summary>
        /// Fetch a record from the Stack_StackFrame table that matches the unique stackid/stackframeid combination
        /// </summary>
        /// <param name="stackId">Id of the stack in the stack/stackframe combination</param>
        /// <param name="stackFrameId">Id of the stack frame in the stack/stackframe combination</param>
        /// <param name="sequenceId">Id of the frame sequence within the stack trace.</param>
        /// <returns>The record containing the stackid/stackframeid combination.</returns>
        IDataReader Stack_StackFrame_Fetch(int stackId, int stackFrameId, short sequenceId);

        /// <summary>
        /// Fetch a record from the RequestInformation table that has FaultEventId = faultEventId
        /// </summary>
        /// <param name="faultEventId">Return the record from the RequestInformation table that has a matching faultEventId value</param>
        /// <returns>A DataReader containing the returned row(s).</returns>
        IDataReader RequestInformation_Fetch(int faultEventId);

        /// <summary>
        /// Fetch the stack frame with the given hashcode.
        /// </summary>
        /// <param name="hashCode">The hashcode of the stack frame to match on.</param>
        /// <returns>A DataReader containing the returned row.</returns>
        IDataReader StackFrame_Fetch(long hashCode);

        IDataReader StackFrame_Fetch(int stackId);

        /// <summary>
        /// Insert a new fault event record in the FaultEvent table.
        /// </summary>
        /// <param name="applicationId">Application name.</param>
        /// <param name="machineId">Machine name that generated exception.</param>
        /// <param name="platformId">Platform name.</param>
        /// <param name="faultId">A foreign key to the Fault table, to link the fault to the fault event.</param>
        /// <param name="faultTime">The timestamp of the occuring fault.</param>
        /// <param name="message">A message associated with the fault.</param>
        /// <returns>Returns a datareader to retrieve information from inserted row (like IDENTITY values).</returns>
        IDataReader FaultEvent_Insert(int? applicationId, int? machineId, int? platformId, int faultId,
                                      DateTime faultTime /*, string message*/);

        /// <summary>
        /// Insert a record into the Application table.
        /// </summary>
        /// <param name="name">The name of the application to insert.</param>
        /// <returns>Returns a datareader to retrieve information from inserted row (like IDENTITY values).</returns>
        IDataReader Application_Insert(string name);

        /// <summary>
        /// Insert a record into the Machine table.
        /// </summary>
        /// <param name="name">The name of the machine to insert.</param>
        /// <returns>Returns a datareader to retrieve information from inserted row (like IDENTITY values).</returns>
        IDataReader Machine_Insert(string name);

        /// <summary>
        /// Insert a record into the Platform table.
        /// </summary>
        /// <param name="name">The name of the platform to insert.</param>
        /// <returns>Returns a datareader to retrieve information from inserted row (like IDENTITY values).</returns>
        IDataReader Platform_Insert(string name);

        /// <summary>
        /// Insert a fault into the Fault table.
        /// </summary>
        /// <param name="stackId">Foreign key of record from the stack table to tie a stack trace to this fault.</param>
        /// <param name="causeId">A foreign key to the same table to indicate a causing fault.</param>
        /// <param name="exceptionType">The type of exception generated.</param>
        /// <param name="hashCode">A unique value for the fault.</param>
        /// <returns>Returns a datareader to retrieve information from inserted row (like IDENTITY values).</returns>
        IDataReader Fault_Insert(int stackId, int? causeId, string exceptionType, long hashCode);

        /// <summary>
        /// Insert a key/value pair to represent fault data associated with the exception.
        /// </summary>
        /// <param name="faultEventId">A foreign key to the fault event.</param>
        /// <param name="key">The key used for the key value pair.</param>
        /// <param name="value">The value of the key value pair.</param>
        /// <returns>Returns a datareader to retrieve information from inserted row (like IDENTITY values).</returns>
        IDataReader FaultData_Insert(int faultEventId, string key, string value);

        /// <summary>
        /// Insert a stack record into the table.
        /// </summary>
        /// <param name="hashCode">A unique value per stack record.</param>
        /// <returns>Returns a datareader to retrieve information from inserted row (like IDENTITY values).</returns>
        IDataReader Stack_Insert(long hashCode);


        /// <summary>
        /// Insert a record into the Stack_StackFrame table that matches the unique stackid/stackframeid combination
        /// </summary>
        /// <param name="stackId">Id of the stack in the stack/stackframe combination</param>
        /// <param name="stackFrameId">Id of the stack frame in the stack/stackframe combination</param>
        /// <param name="sequenceId">Id of the frame sequence within the stack trace.</param>
        /// <returns>The record inserted.</returns>
        IDataReader Stack_StackFrame_Insert(int stackId, int stackFrameId, short sequenceId);


        /// <summary>
        /// Insert information specific to the request coming in.
        /// </summary>
        /// <param name="faultEventId">A foreign key to the fault event.</param>
        /// <param name="path">The path associated with the exception request.</param>
        /// <param name="url">The url associated with the exception request.</param>
        /// <param name="user">The user making the request.</param>
        /// <param name="userHostAddress">The user host address of the requestor.</param>
        /// <returns>Returns a datareader to retrieve information from inserted row (like IDENTITY values).</returns>
        IDataReader RequestInformation_Insert(int faultEventId, string path, string url, string user,
                                              string userHostAddress);

        /// <summary>
        /// Insert a stack frame into the StackFrame table.
        /// </summary>
        /// <param name="className">Class name tied to exception.</param>
        /// <param name="fileName">File name tied to exception.</param>
        /// <param name="isNativeMethod">Boolean value determining if this is a native method.</param>
        /// <param name="isUnknownSource">Boolean value determining if this was derived from unknown source</param>
        /// <param name="lineNumber">The line number of the exception.</param>
        /// <param name="methodName">The method name associated with the exception.</param>
        /// <param name="hashCode">A unique value per stack frame.</param>
        /// <returns>Returns a datareader to retrieve information from inserted row (like IDENTITY values).</returns>
        IDataReader StackFrame_Insert(string className, string fileName, bool isNativeMethod,
                                      bool isUnknownSource, int? lineNumber, string methodName, long hashCode);

        IDataReader Message_Fetch(string message);

        IDataReader Message_Fetch(int id);

        IDataReader Message_Insert(string message);

        IDataReader FaultEvent_Fault_Message_Fetch(int faultEventId);

        void FaultEvent_Fault_Message_Insert(int faultEventId, int faultId, int messageId, int index); 


        #region Dashboard

        IDataReader ReportSummaryApplication();

        IDataReader ReportSummaryFault(int? applicationId, string sortExpression, int startRowIndex, int maximumRows);

        IDataReader ReportSummaryFaultEventById(int faultId, int faultEventId);

        IDataReader ReportSummaryFaultEventByRank(int faultId, int faultRank);

        IDataReader ReportDetailsStack(int faultId);

        IDataReader ReportDetailsStackFrame(int stackId);

        #endregion
    }
}