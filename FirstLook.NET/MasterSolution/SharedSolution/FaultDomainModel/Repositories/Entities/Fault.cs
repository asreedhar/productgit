﻿using System;
using FirstLook.Common.Core.Hashing;
using FirstLook.Fault.DomainModel.Model;

namespace FirstLook.Fault.DomainModel.Repositories.Entities
{

    /// <summary>
    /// This class implements the IFault interface to reflect a fault object.
    /// A fault object is referenced on a FaultEvent Object.
    /// </summary>
    [Serializable]
    public class Fault : IFault
    {
        /// <summary>
        /// A unique ID per fault object.
        /// </summary>
        public int Id { get; internal set; }

        /// <summary>
        /// This hashing algorithm must return a unique value per fault.
        /// </summary>
        /// <returns>A unique Int64 value.</returns>
        public long Hash()
        {
            HashAlgorithm algo = new HashAlgorithm();
            if(ExceptionType!=null) algo.AddString(ExceptionType);
            if(Stack!=null) algo.AddLong(Stack.Hash());
            if(Cause!=null) algo.AddLong(Cause.Hash());

            return algo.Value;
        }
  
        #region IFault Members
        
        /// <summary>
        /// ExceptionType - The type of exception this fault generated.
        /// </summary>
        public string ExceptionType { get; internal set;}

        /// <summary>
        /// Message - Message associated with fault.
        /// </summary>
        public string Message { get; internal set; }

        /// <summary>
        /// Stack - The referenced stack trace for the exception.
        /// </summary>
        public IStack Stack { get; internal set; }

        /// <summary>
        /// Cause - The fault that caused this fault, if one exists.
        /// </summary>
        public IFault Cause { get; internal set; }
      
        #endregion
    }
}