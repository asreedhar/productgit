﻿using System;
using FirstLook.Common.Core.Hashing;
using FirstLook.Fault.DomainModel.Model;

namespace FirstLook.Fault.DomainModel.Repositories.Entities
{
    /// <summary>
    /// This class implements the IStackFrame interface to reflect a StackFrame object.
    /// A StackFrame object is referenced by a Stack object.  A Stack will reference multiple stack frames.
    /// </summary>
    [Serializable]
    public class StackFrame : IStackFrame,IComparable

    {
        /// <summary>
        /// Id - A unique value per StackFrame object.
        /// </summary>
        public int Id { get; internal set; }

        /// <summary>
        /// Hash() - This hashing algorithm must return a unique value per stack frame.
        /// </summary>
        /// <returns>A unique Int64 value.</returns>
        public long Hash()
        {
            HashAlgorithm algo = new HashAlgorithm();

            if(ClassName!=null) algo.AddString(ClassName);
            if(FileName!=null) algo.AddString(FileName);
            if(MethodName!=null) algo.AddString(MethodName);
            
            algo.AddBool(IsNativeMethod);
            algo.AddBool(IsUnknownSource);
            algo.AddInt(LineNumber);

            return algo.Value;
        }

        #region IStackFrame Members

        /// <summary>
        /// ClassName - Class name associated with the generated exception.
        /// </summary>
        public string ClassName { get; internal set; }

        /// <summary>
        /// FileName - File name associated with the generated exception.
        /// </summary>
        public string FileName { get; internal set; }

        /// <summary>
        /// IsNativeMethod - Is this a native method?
        /// </summary>
        public bool IsNativeMethod { get; internal set; }

        /// <summary>
        /// IsUnknownSource - Is the source known?
        /// </summary>
        public bool IsUnknownSource { get; internal set; }

        /// <summary>
        /// LineNumber - Line number of exception.
        /// </summary>
        public int? LineNumber { get; internal set; }

        /// <summary>
        /// MethodName - Method name the exception was thrown from.
        /// </summary>
        public string MethodName { get; internal set; }

        /// <summary>
        /// Implements the CompareTo method, to compare two StackFrames.
        /// </summary>
        /// <param name="obj">obj must be a StackFrame instance.</param>
        /// <returns> -1 for less, 0 for equals, 1 for greater than</returns>
        public int CompareTo(object obj)
        {
            StackFrame frame = obj as StackFrame;

            if(frame == null)
            {
                throw new ArgumentException("Cannot compare to anything other than a StackFrame.");
            }

            if (ClassName.CompareTo(frame.ClassName) < 0) 
            {
                return -1;
            }

            if (ClassName.CompareTo(frame.ClassName) > 0)
            {
                return 1;
            }

            if (FileName.CompareTo(frame.FileName) < 0)
            {
                return -1;
            }

            if (FileName.CompareTo(frame.FileName) > 0)
            {
                return 1;
            }

            if (MethodName.CompareTo(frame.MethodName) < 0)
            {
                return -1;
            }

            if (MethodName.CompareTo(frame.MethodName) > 0)
            {
                return 1;
            }

            if(LineNumber < frame.LineNumber)
            {
                return -1;
            }

            if (LineNumber > frame.LineNumber)
            {
                return 1;
            }

            if (IsUnknownSource)
            {
                if(!frame.IsUnknownSource)
                {
                    return -1;
                }
            } else
            {
                if(frame.IsUnknownSource)
                {
                    return 1;
                }
            }

            if (IsNativeMethod)
            {
                if (!frame.IsNativeMethod)
                {
                    return -1;
                }
            }
            else
            {
                if (frame.IsNativeMethod)
                {
                    return 1;
                }
            }

            return 0;

        }

        #endregion
    }
}