﻿using System;
using System.Collections.Generic;
using FirstLook.Fault.DomainModel.Model;

namespace FirstLook.Fault.DomainModel.Repositories.Entities
{
    /// <summary>
    /// This class implements the IFaultEvent interface to reflect a FaultEvent object.
    /// A FaultEvent object is what will get "SAVED" into the database.
    /// </summary>
    [Serializable]
    public class FaultEvent : IFaultEvent
    {
        /// <summary>
        /// FaultEvent() - When a FaultEvent gets created initially set the IsNew flag to true;
        /// </summary>
        public FaultEvent()
        {
            IsNew = true;
        }

        /// <summary>
        /// Id - A unique value per FaultEvent
        /// </summary>
        public int Id { get; internal set; }

        /// <summary>
        /// IsNew - A boolean value to say whether or not a FaultEvent has already been saved.
        /// </summary>
        public bool IsNew { get; internal set; }

        #region IFaultEvent Members

        /// <summary>
        /// Application - Refers to an Application object tied to the fault event.
        /// </summary>
        public IApplication Application { get; internal set; }

        /// <summary>
        /// Machine - Refers to the Machine on which the fault event was generated.
        /// </summary>
        public IMachine Machine { get; internal set; }

        /// <summary>
        /// Platform - Refers to the Platform on which the fault event was generated.
        /// </summary>
        public IPlatform Platform { get; internal set; }

        /// <summary>
        /// Fault - A reference to the fault object itself.
        /// </summary>
        public IFault Fault { get; internal set; }

        /// <summary>
        /// Timestamp - The timestamp the exception was generated.
        /// </summary>
        public DateTime Timestamp { get; internal set; }

        /// <summary>
        /// Data - Each FaultEvent can be associated with mutiple pieces of data that come in key/value pairs.
        /// </summary>
        public IList<IFaultData> Data { get; internal set; }

        /// <summary>
        ///  RequestInfo - Each generated FaultEvent is associated with a requestor.
        /// </summary>
        public IRequestInformation RequestInfo { get; internal set;}

        #endregion
    }
}