﻿using System;
using FirstLook.Fault.DomainModel.Model;

namespace FirstLook.Fault.DomainModel.Repositories.Entities
{
    /// <summary>
    /// This class implements the IPlatform interface to reflect a Platform object.
    /// A Platform object is referenced on a FaultEvent Object.
    /// </summary>
    [Serializable]
    public class Platform : IPlatform
    {
        /// <summary>
        /// Id - Unique Platform ID as returned from inserting a record into the Platform table.
        /// </summary>
        public int Id { get; internal set; }

        #region IPlatform Members

        /// <summary>
        /// Name - The name of the platform.
        /// </summary>
        public string Name { get; internal set; }

        #endregion
    }
}