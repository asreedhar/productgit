﻿using System;
using FirstLook.Fault.DomainModel.Model;

namespace FirstLook.Fault.DomainModel.Repositories.Entities
{
    /// <summary>
    /// This class implements the IApplication interface to reflect an application object.
    /// An application object is referenced on a FaultEvent Object.
    /// </summary>
    [Serializable]
    public class Application : IApplication
    {
        /// <summary>
        /// Id - Unique Application ID as returned from inserting a record into the Application table.
        /// </summary>
        public int Id { get; internal set; }

        #region IApplication Members

        /// <summary>
        /// Name - The name of the application.
        /// </summary>
        public string Name { get; internal set; }

        #endregion
    }
}