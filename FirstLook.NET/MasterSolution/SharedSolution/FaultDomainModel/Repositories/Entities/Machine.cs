﻿using System;
using FirstLook.Fault.DomainModel.Model;

namespace FirstLook.Fault.DomainModel.Repositories.Entities
{
    /// <summary>
    /// This class implements the IMachine interface to reflect a Machine object.
    /// A Machine object is referenced on a FaultEvent Object.
    /// </summary>
    [Serializable]
    public class Machine : IMachine
    {
        /// <summary>
        /// Id - Unique Machine ID as returned from inserting a record into the Machine table.
        /// </summary>
        public int Id { get; internal set; }

        #region IMachine Members

        /// <summary>
        /// Name - The name of the machine.
        /// </summary>
        public string Name { get; internal set; }

        #endregion
    }
}