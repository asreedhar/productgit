﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Hashing;
using FirstLook.Fault.DomainModel.Model;

namespace FirstLook.Fault.DomainModel.Repositories.Entities
{
    /// <summary>
    /// This class implements the IStack interface to reflect a Stack object.
    /// A Stack object is referenced on a Fault Object.  It is possible that several Fault objects refer to the same stack trace (stack record)
    /// </summary>
    [Serializable]
    public class Stack : IStack
    {
        /// <summary>
        /// Stack() - Whenver a new stack is created initialize the stackframe list. 
        /// </summary>
        public Stack()
        {
            Frames = new List<IStackFrame>();
        }

        /// <summary>
        /// Id - A unique value per Stack object.
        /// </summary>
        public int Id { get; internal set; }
        
        /// <summary>
        /// Hash() - This hashing algorithm must return a unique value per stack.
        /// </summary>
        /// <returns>A unique Int64 value.</returns>
        public long Hash()
        {
            HashAlgorithm algo = new HashAlgorithm();

            List<IStackFrame> frames = Frames as List<IStackFrame>;
            
            if (frames == null)
            {
                throw new ApplicationException();
            }
                   
            //The hash value for a stack is simply the hash of the corresponding frames
            foreach(StackFrame frame in frames)
            {
                algo.AddLong(frame.Hash());
            }

            return algo.Value;
        }

        /// <summary>
        /// Frames - Each Stack can contain multiple stack frames.
        /// </summary>  
        public IList<IStackFrame> Frames { get; internal set; }
    }
}