﻿using System;
using FirstLook.Fault.DomainModel.Model;
using FirstLook.Fault.DomainModel.Model.Reporting;

namespace FirstLook.Fault.DomainModel.Repositories.Entities.Reporting
{
    [Serializable]
    public class FaultSummary : IFaultSummary
    {
        public IApplication Application { get; set; }

        public int FaultId { get; set; }

        public string ExceptionType { get; set; }

        public string Message { get; set; }

        public DateTime MinFaultTime { get; set; }

        public DateTime MaxFaultTime { get; set; }

        public int MaxFaultEventId { get; set; }

        public int NumFaultEvent { get; set; }
    }
}
