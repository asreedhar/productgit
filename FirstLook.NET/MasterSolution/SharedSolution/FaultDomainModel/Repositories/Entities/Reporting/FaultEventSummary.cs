﻿using System;
using FirstLook.Fault.DomainModel.Model;
using FirstLook.Fault.DomainModel.Model.Reporting;

namespace FirstLook.Fault.DomainModel.Repositories.Entities.Reporting
{
    [Serializable]
    public class FaultEventSummary : IFaultEventSummary
    {
        public IApplication Application { get; set; }

        public IMachine Machine { get; set; }

        public IPlatform Platform { get; set; }

        public DateTime FaultTime { get; set; }

        public int FaultId { get; set; }

        public int Id { get; set; }

        public long Rank { get; set; }
    }
}
