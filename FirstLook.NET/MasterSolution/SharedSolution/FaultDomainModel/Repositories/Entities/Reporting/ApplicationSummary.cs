﻿using System;
using FirstLook.Fault.DomainModel.Model;
using FirstLook.Fault.DomainModel.Model.Reporting;

namespace FirstLook.Fault.DomainModel.Repositories.Entities.Reporting
{
    [Serializable]
    public class ApplicationSummary : IApplicationSummary
    {
        public IApplication Application { get; set; }

        public int NumberOfFaults { get; set; }

        public int NumberOfFaultEvents { get; set; }
    }
}
