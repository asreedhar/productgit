﻿using System;
using FirstLook.Fault.DomainModel.Model;

namespace FirstLook.Fault.DomainModel.Repositories.Entities
{
    /// <summary>
    /// This class implements the IFaultData interface to reflect a FaultData object.
    /// A FaultData object is referenced on a FaultEvent Object.
    /// </summary>
    [Serializable]
    public class FaultData : IFaultData
    {
        /// <summary>
        /// Id - A unique ID per FaultData object.
        /// </summary>
        public int Id { get; internal set; }

        #region IFaultData Members

        /// <summary>
        /// Key - Key of key/value pair associated with the fault event. This is part of the Data attribute tied to an Exception.
        /// Value - Value of key/value pair associated with the fault event. This is part of the Data attribute tied to an Exception.
        /// </summary>
        public string Key { get; internal set; }
        public string Value { get; internal set; }
        
        #endregion
    }
}