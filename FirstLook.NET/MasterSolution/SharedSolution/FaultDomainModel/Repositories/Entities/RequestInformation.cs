﻿using System;
using FirstLook.Fault.DomainModel.Model;

namespace FirstLook.Fault.DomainModel.Repositories.Entities
{
    /// <summary>
    /// This class implements the IRequestInformation interface to reflect a RequestInformation object.
    /// A RequestInformation object is referenced on a FaultEvent Object.
    /// </summary>
    [Serializable]
    public class RequestInformation : IRequestInformation
    {
        /// <summary>
        /// Id - A unique value per RequestInformation object.
        /// </summary>
        public int Id { get; internal set; }

        #region IRequestInformation Members

        /// <summary>
        /// Path - The Path associated with the request.
        /// </summary>
        public string Path { get; internal set;}

        /// <summary>
        /// Url - The Url associated with the request.
        /// </summary>
        public string Url { get; internal set; }

        /// <summary>
        /// User - The User associated with the request.
        /// </summary>
        public string User { get; internal set; }

        /// <summary>
        /// UrlHostAddress - The UrlHostAddress associated with the request.
        /// </summary>
        public string UserHostAddress { get; internal set; }

        #endregion
    }
}