﻿using System;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Fault.DomainModel.Repositories.Datastores;

namespace FirstLook.Fault.DomainModel.Repositories.Gateways
{
    /// <summary>
    /// Serves as the gateway to the Application table. Providing Insert and Fetch functionality to the table.
    /// </summary>
    public class ApplicationGateway : GatewayBase
    {
        /// <summary>
        /// Fetch a record from the application table, searching by application name.
        /// </summary>
        /// <param name="application">The application name to search on.</param>
        /// <returns>The record containing that application name (if one exists).</returns>
        public Row Fetch(string application)
        {
            Row row = null;

            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.Application_Fetch(application))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    row = serializer.Deserialize((IDataRecord)reader);
                }
            }

            return row;
        }

        public Row Fetch(int id)
        {
            Row row = null;

            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.Application_Fetch(id))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    row = serializer.Deserialize((IDataRecord)reader);
                }
            }

            return row;
        }

        /// <summary>
        /// Insert a record into the application table using the provided application name.
        /// </summary>
        /// <param name="name">The name of the application to insert.</param>
        /// <returns>Returns the inserted row.</returns>
        public Row Insert(string name)
        {
            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.Application_Insert(name))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    return serializer.Deserialize((IDataRecord)reader);
                }
            }

            return null;
        }

        #region Nested type: Row

        [Serializable]
        public class Row
        {
            public int ApplicationId { get; set; }
            public string Name { get; set; }
        }

        #endregion

        #region Nested type: RowSerializer

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int applicationId = record.GetInt32(record.GetOrdinal("ApplicationID"));
                string name = record.GetString(record.GetOrdinal("Name"));

                return new Row {ApplicationId = applicationId, Name = name};
            }
        }

        #endregion
    }
}