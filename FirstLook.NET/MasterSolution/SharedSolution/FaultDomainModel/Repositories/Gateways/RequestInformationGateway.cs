﻿using System;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.Fault.DomainModel.Repositories.Datastores;

namespace FirstLook.Fault.DomainModel.Repositories.Gateways
{
    /// <summary>
    /// Serves as the gateway to the RequestInformation table. Providing Insert and Fetch functionality to the table.
    /// </summary>
    public class RequestInformationGateway : GatewayBase
    {
        /// <summary>
        /// Fetch a row from the RequestInformation table, searching by faultEventId
        /// </summary>
        /// <param name="faultEventId">Search for record based on faultEventId, one record per faultEventId</param>
        /// <returns>The record containing that requestor information(if one exists).</returns>
        public Row Fetch(int faultEventId)
        {
            Row row = null;

            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.RequestInformation_Fetch(faultEventId))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    row = serializer.Deserialize((IDataRecord)reader);
                }
            }

            return row;
        }

        /// <summary>
        /// Insert a row into the RequestInformation table.
        /// </summary>
        /// <param name="faultEventId">A foreign key to the FaultEvent table</param>
        /// <param name="path">Path associated with request information.</param>
        /// <param name="url">Url associated with request information.</param>
        /// <param name="user">User associated with request.</param>
        /// <param name="userHostAddress">User host address associated with request.</param>
        /// <returns>Returns the inserted row.</returns>
        public Row Insert(int faultEventId, string path, string url, string user, string userHostAddress)
        {
            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.RequestInformation_Insert(faultEventId, path, url, user, userHostAddress))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    return serializer.Deserialize((IDataRecord)reader);
                }
            }

            return null;

        }

        #region Nested type: Row

        [Serializable]
        public class Row
        {
            public int RequestInformationId { get; set; }
            public string Path { get; set; }
            public string Url { get; set; }
            public string User { get; set; }
            public string UserHostAddress { get; set; }
        }

        #endregion

        #region Nested type: RowSerializer

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int requestInformationId = record.GetInt32(record.GetOrdinal("RequestInformationID"));
                string path = DataRecord.GetString(record, "Path");
                string url = DataRecord.GetString(record, "Url");
                string user = DataRecord.GetString(record, "User"); 
                string userHostAddress = DataRecord.GetString(record,"UserHostAddress");


                return new Row
                           {
                               RequestInformationId = requestInformationId,
                               Path = path,
                               Url = url,
                               User = user,
                               UserHostAddress = userHostAddress
                           };
            }
        }

        #endregion
    }
}