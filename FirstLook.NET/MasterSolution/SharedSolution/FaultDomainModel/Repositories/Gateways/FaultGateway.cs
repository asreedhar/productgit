﻿using System;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.Fault.DomainModel.Repositories.Datastores;

namespace FirstLook.Fault.DomainModel.Repositories.Gateways
{
    /// <summary>
    /// Serves as the gateway to the Fault table. Providing Insert and Fetch functionality to the table.
    /// </summary>
    public class FaultGateway : GatewayBase
    {
        /// <summary>
        /// Fetch a record from the Fault table, searching by hashCode.
        /// </summary>
        /// <param name="hashCode">A unique value per Fault.</param>
        /// <returns>The record containing that hashCode (if one exists).</returns>
        public Row Fetch(long hashCode)
        {
            Row row = null;

            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.Fault_Fetch(hashCode))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    row = serializer.Deserialize((IDataRecord)reader);
                }
            }

            return row;
        }

        public Row Fetch(int id)
        {
            Row row = null;

            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.Fault_Fetch(id))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    row = serializer.Deserialize((IDataRecord)reader);
                }
            }

            return row;
        }

        /// <summary>
        /// Insert a record into the Fault table.
        /// </summary>
        /// <param name="stackId">Foreign key to a record in the stack table.</param>
        /// <param name="causeId">Foreign key to a record in the same Fault table. (Causing exception)</param>
        /// <param name="exceptionType">The type of exception.</param>
        /// <param name="hashCode">A unique hashcode per Fault.</param>
        /// <returns>Returns the inserted row.</returns>
        public Row Insert(int stackId, int? causeId, string exceptionType, long hashCode)
        {
            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.Fault_Insert(stackId, causeId, exceptionType, hashCode))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    return serializer.Deserialize((IDataRecord)reader);
                }
            }

            return null;
        }

        #region Nested type: Row

        [Serializable]
        public class Row
        {
            public int FaultId { get; set; }
            public int StackId { get; set; }
            public int? CauseId { get; set; }
            public string ExceptionType { get; set; }
            public string Message { get; set; }
            public long HashCode { get; set; }
        }

        #endregion

        #region Nested type: RowSerializer

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int faultId = record.GetInt32(record.GetOrdinal("FaultID"));
                int stackId = record.GetInt32(record.GetOrdinal("StackID"));
                int? causeId = DataRecord.GetNullableInt32(record, "CauseID"); 
                string exceptionType = record.GetString(record.GetOrdinal("ExceptionType"));
                long hashCode = record.GetInt64(record.GetOrdinal("HashCode"));

                return new Row
                           {
                               FaultId = faultId,
                               StackId = stackId,
                               CauseId = causeId,
                               ExceptionType = exceptionType,
                               HashCode = hashCode
                           };
            }
        }

        #endregion
    }
}