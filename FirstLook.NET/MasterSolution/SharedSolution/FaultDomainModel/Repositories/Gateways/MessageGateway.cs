﻿using System;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Fault.DomainModel.Repositories.Datastores;
using FirstLook.Common.Core.Data;

namespace FirstLook.Fault.DomainModel.Repositories.Gateways
{
    public class MessageGateway : GatewayBase
    {
        public Row Fetch(string message)
        {
            Row row = null;

            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.Message_Fetch(message))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    row = serializer.Deserialize((IDataRecord)reader);
                }
            }

            return row;
        }

        public Row Fetch(int messageId)
        {
            Row row = null;

            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.Message_Fetch(messageId))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    row = serializer.Deserialize((IDataRecord)reader);
                }
            }

            return row;
        }

        public Row Insert(string message)
        {
            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.Message_Insert(message))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    return serializer.Deserialize((IDataRecord)reader);
                }
            }

            return null;
        }

        #region Nested type: Row

        [Serializable]
        public class Row
        {
            public int MessageId { get; set; }
            public string Message { get; set; }
        }

        #endregion

        #region Nested type: RowSerializer

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int messageId = record.GetInt32("MessageID", 0);
                string message = record.GetString("Message");

                return new Row
                           {
                               Message = message,
                               MessageId = messageId
                           };
            }
        }

        #endregion
    }
}