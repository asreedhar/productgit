﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Fault.DomainModel.Repositories.Datastores;

namespace FirstLook.Fault.DomainModel.Repositories.Gateways
{
    public class FaultEventFaultMessageGateway : GatewayBase
    {
        public IList<Row> Fetch(int faultEventId)
        {
            IList<Row> rows = new List<Row>();

            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.FaultEvent_Fault_Message_Fetch(faultEventId))
            {
                ISerializer<Row> serializer = new RowSerializer();

                rows = serializer.Deserialize(reader);
            }

            return rows;
        }

        public void Insert(int faultEventId, int faultId, int messageId, int index)
        {
            var datastore = Resolve<IFaultDatastore>();

            datastore.FaultEvent_Fault_Message_Insert(faultEventId, faultId, messageId, index);
        }

        public int Count(int faultEventId)
        {
            var datastore = Resolve<IFaultDatastore>();

            int cnt = 0;

            using (IDataReader reader = datastore.FaultEvent_Fault_Count(faultEventId))
            {
                if (reader.Read())
                {
                    cnt = reader.GetInt32(reader.GetOrdinal("Count"));
                }
            }

            return cnt;
        }

        #region Nested type: Row

        [Serializable]
        public class Row
        {
            public int MessageId { get; set; }
            public int FaultId { get; set; }
            public int FaultEventId { get; set; }
            public int Index { get; set; }
            public string Message { get; set; }

        }

        #endregion

        #region Nested type: RowSerializer

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int messageId = record.GetInt32(record.GetOrdinal("MessageID"));
                int faultId = record.GetInt32(record.GetOrdinal("FaultID"));
                int faultEventId = record.GetInt32(record.GetOrdinal("FaultEventID"));
                int index = record.GetInt32(record.GetOrdinal("Index"));
                string message = record.GetString(record.GetOrdinal("Message"));               

                return new Row
                           {
                               MessageId = messageId,
                               FaultId = faultId,
                               FaultEventId = faultEventId,
                               Index = index,
                               Message = message
                           };
            }
        }

        #endregion
    }
}