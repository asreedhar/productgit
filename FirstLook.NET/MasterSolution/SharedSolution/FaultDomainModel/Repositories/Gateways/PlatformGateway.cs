﻿using System;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Fault.DomainModel.Repositories.Datastores;

namespace FirstLook.Fault.DomainModel.Repositories.Gateways
{
    /// <summary>
    /// Serves as the gateway to the Platform table. Providing Insert and Fetch functionality to the table.
    /// </summary>
    public class PlatformGateway : GatewayBase
    {
        /// <summary>
        /// Fetch a record from the platform table, searching by platform name.
        /// </summary>
        /// <param name="platform">The platform name to search on.</param>
        /// <returns>The record containing that platform name (if one exists).</returns>
        public Row Fetch(string platform)
        {
            Row row = null;

            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.Platform_Fetch(platform))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    row = serializer.Deserialize((IDataRecord)reader);
                }
            }

            return row;
        }

        public Row Fetch(int id)
        {
            Row row = null;

            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.Platform_Fetch(id))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    row = serializer.Deserialize((IDataRecord)reader);
                }
            }

            return row;
        }

        /// <summary>
        /// Insert a record into the platform table using the provided platform name.
        /// </summary>
        /// <param name="name">The name of the platform to insert.</param>
        /// <returns>Returns the inserted row.</returns>
        public Row Insert(string name)
        {
            var datastore = Resolve<IFaultDatastore>();


            using (IDataReader reader = datastore.Platform_Insert(name))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    return serializer.Deserialize((IDataRecord)reader);
                }
            }

            return null;
        }

        #region Nested type: Row

        [Serializable]
        public class Row
        {
            public int PlatformId { get; set; }
            public string Name { get; set; }
        }

        #endregion

        #region Nested type: RowSerializer

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int platformId = record.GetInt32(record.GetOrdinal("PlatformID"));
                string name = record.GetString(record.GetOrdinal("Name"));
                return new Row {PlatformId = platformId, Name = name};
            }
        }

        #endregion
    }
}