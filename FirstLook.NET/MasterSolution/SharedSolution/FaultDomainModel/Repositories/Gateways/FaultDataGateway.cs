﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Fault.DomainModel.Repositories.Datastores;

namespace FirstLook.Fault.DomainModel.Repositories.Gateways
{
    /// <summary>
    /// Serves as the gateway to the FaultData table. Providing Insert and Fetch functionality to the table.
    /// </summary>
    public class FaultDataGateway : GatewayBase
    {
        /// <summary>
        /// Fetch a record from the FaultData table, searching by faultEventId.
        /// </summary>
        /// <param name="faultEventId">The faultEventId to search on.</param>
        /// <returns>The record(s) containing that faultEventId (if one exists).</returns>
        public IList<Row> Fetch(int faultEventId)
        {
            IList<Row> row;

            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.FaultData_Fetch(faultEventId))
            {
                ISerializer<Row> serializer = new RowSerializer();

                row = serializer.Deserialize(reader);
            }

            return row;
        }

        /// <summary>
        /// Insert a record into the FaultData table using the provided faulteventid and key/value pair.
        /// </summary>
        /// <param name="faultEventId">The name of the application to insert.</param>
        /// <param name="key">The key to insert.</param>
        /// <param name="value">The value to insert.</param>
        /// <returns>Returns the inserted row.</returns>
        public Row Insert(int faultEventId, string key, string value)
        {
            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.FaultData_Insert(faultEventId, key, value))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    return serializer.Deserialize((IDataRecord)reader);
                }
            }

            return null;
        }

        #region Nested type: Row

        [Serializable]
        public class Row
        {
            public int FaultDataId { get; set; }

            public int FaultEventId { get; set; }

            public string Key { get; set; }

            public string Value { get; set; }
        }

        #endregion

        #region Nested type: RowSerializer

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int faultDataId = record.GetInt32(record.GetOrdinal("FaultDataID"));

                int faultEventId = record.GetInt32(record.GetOrdinal("FaultEventID"));

                string key = record.GetString(record.GetOrdinal("Key"));

                string value = record.GetString(record.GetOrdinal("Value"));

                return new Row {FaultDataId = faultDataId, FaultEventId = faultEventId, Key = key, Value = value};
            }
        }

        #endregion
    }
}