﻿using System;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Fault.DomainModel.Repositories.Datastores;

namespace FirstLook.Fault.DomainModel.Repositories.Gateways
{
    /// <summary>
    /// Serves as the gateway to the Stack_StackFrame table. Providing Insert and Fetch functionality to the table.
    /// </summary>
    public class StackStackFrameGateway : GatewayBase
    {
        /// <summary>
        /// Fetch a row from the Stack_StackFrame table, searching by StackId/StackFrameID.
        /// </summary>
        /// <param name="stackId">Id of stack in stack/stackframe combination</param>
        /// <param name="stackFrameId">Id of stackFrame in stack/stackframe combination</param>
        /// <param name="sequenceId">Id of the frame sequence within the stack trace.</param>
        /// <returns>The record containing the Stack/StackFrame record combination.</returns>
        public Row Fetch(int stackId, int stackFrameId, short sequenceId)
        {
            Row row = null;
            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.Stack_StackFrame_Fetch(stackId,stackFrameId,sequenceId))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    row = serializer.Deserialize((IDataRecord)reader);

                }
            }

            return row;
        }

        /// <summary>
        /// Insert a record into the Stack_StackFrame table.
        /// </summary>
        /// <param name="stackId">Id of stack in stack/stackframe combination</param>
        /// <param name="stackFrameId">Id of stackFrame in stack/stackframe combination</param>
        /// <param name="sequenceId">Id of the frame sequence within the stack trace.</param>
        /// <returns>The record containing the Stack/StackFrame record combination.</returns>
        public Row Insert(int stackId, int stackFrameId, short sequenceId)
        {
            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.Stack_StackFrame_Insert(stackId, stackFrameId, sequenceId))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    return serializer.Deserialize((IDataRecord)reader);
                }
            }

            return null;

        }

        #region Nested type: Row

        [Serializable]
        public class Row
        {
            public int StackId { get; set; }

            public int StackFrameId { get; set; }

            public short SequenceId { get; set; }
        }

        #endregion

        #region Nested type: RowSerializer

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int stackId = record.GetInt32(record.GetOrdinal("StackID"));

                int stackFrameId = record.GetInt32(record.GetOrdinal("StackFrameId"));

                short sequenceId = record.GetInt16(record.GetOrdinal("SequenceId"));

                return new Row {StackId = stackId, StackFrameId = stackFrameId, SequenceId = sequenceId};
            }
        }

        #endregion
    }
}