﻿using System;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Fault.DomainModel.Repositories.Datastores;

namespace FirstLook.Fault.DomainModel.Repositories.Gateways
{
    /// <summary>
    /// Serves as the gateway to the Stack table. Providing Insert and Fetch functionality to the table.
    /// </summary>
    public class StackGateway : GatewayBase
    {
        /// <summary>
        /// Fetch a row from the Stack table, searching by unique hash code.
        /// </summary>
        /// <param name="hashCode">Search for record based on hashCode, there will be one record per hashCode</param>
        /// <returns>The record containing the stack associated with the hashCode.</returns>
        public Row Fetch(long hashCode)
        {
            Row row = null;

            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.Stack_Fetch(hashCode))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    row = serializer.Deserialize((IDataRecord)reader);
                }
            }

            return row;
        }

        /// <summary>
        /// Insert a record into the Stack table.
        /// </summary>
        /// <param name="hashCode">A unique hash value per Stack trace.</param>
        /// <returns>Returns the inserted row.</returns>
        public Row Insert(long hashCode)
        {
            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.Stack_Insert(hashCode))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    return serializer.Deserialize((IDataRecord)reader);
                }
            }

            return null;

        }

        #region Nested type: Row

        [Serializable]
        public class Row
        {
            public int StackId { get; set; }

            public long HashCode { get; set; }
        }

        #endregion

        #region Nested type: RowSerializer

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int stackId = record.GetInt32(record.GetOrdinal("StackID"));

                long hashCode = record.GetInt64(record.GetOrdinal("HashCode"));

                return new Row {StackId = stackId, HashCode = hashCode};
            }
        }

        #endregion
    }
}