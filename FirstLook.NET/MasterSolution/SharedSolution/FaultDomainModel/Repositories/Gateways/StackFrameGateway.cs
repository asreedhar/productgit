﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.Fault.DomainModel.Repositories.Datastores;

namespace FirstLook.Fault.DomainModel.Repositories.Gateways
{
    /// <summary>
    /// Serves as the gateway to the StackFrame table. Providing Insert and Fetch functionality to the table.
    /// </summary>
    public class StackFrameGateway : GatewayBase
    {
        /// <summary>
        /// Fetch the stack frame with the given hashcode.
        /// </summary>
        /// <param name="hashCode">The hashcode of the stack frame to match on.</param>
        /// <returns>A Row containing the returned row.</returns>
        public Row Fetch(long hashCode)
        {
            Row row = null;

            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.StackFrame_Fetch(hashCode))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    row = serializer.Deserialize((IDataRecord)reader);
                }
            }

            return row;
        }

        public IList<Row> Fetch(int stackId)
        {
            IList<Row> row;

            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.StackFrame_Fetch(stackId))
            {
                ISerializer<Row> serializer = new RowSerializer();

                row = serializer.Deserialize(reader);
            }

            return row;
        }

        /// <summary>
        /// Insert a stack frame record into the StackFrame table.
        /// </summary>
        /// <param name="className">Class name associated with the generated exception.</param>
        /// <param name="fileName">File name associated with the generated exception.</param>
        /// <param name="isNativeMethod">Is this a native method?</param>
        /// <param name="isUnknownSource">Is the source known?</param>
        /// <param name="lineNumber">Line number of exception.</param>
        /// <param name="methodName">Method name the exception was thrown from.</param>
        /// <param name="hashCode">A unique hash value.</param>
        /// <returns>Returns the inserted row.</returns>
        public Row Insert(string className, string fileName, bool isNativeMethod,
                           bool isUnknownSource, int? lineNumber, string methodName, long hashCode)
        {
            var datastore = Resolve<IFaultDatastore>();


            using (IDataReader reader = datastore.StackFrame_Insert(className, fileName, isNativeMethod, isUnknownSource, lineNumber,
                                        methodName, hashCode))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    return serializer.Deserialize((IDataRecord)reader);
                }
            }

            return null;

        }

        #region Nested type: Row

        [Serializable]
        public class Row
        {
            public int StackFrameId { get; set; }

            public string ClassName { get; set; }

            public string FileName { get; set; }

            public bool IsNativeMethod { get; set; }

            public bool IsUnknownSource { get; set; }

            public int? LineNumber { get; set; }

            public string MethodName { get; set; }

            public long HashCode { get; set; }
        }

        #endregion

        #region Nested type: RowSerializer

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int stackFrameId = record.GetInt32(record.GetOrdinal("StackFrameID"));

                string className = DataRecord.GetString(record, "ClassName");

                string fileName = DataRecord.GetString(record, "FileName");

                bool isNativeMethod = record.GetBoolean(record.GetOrdinal("IsNativeMethod"));

                bool isUnknownSource = record.GetBoolean(record.GetOrdinal("IsUnknownSource"));

                int? lineNumber = DataRecord.GetNullableInt32(record, "LineNumber");

                string methodName = DataRecord.GetString(record, "MethodName");

                long hashCode = record.GetInt64(record.GetOrdinal("HashCode"));

                return new Row
                           {
                               StackFrameId = stackFrameId,
                               ClassName = className,
                               FileName = fileName,
                               IsNativeMethod = isNativeMethod,
                               IsUnknownSource = isUnknownSource,
                               LineNumber = lineNumber,
                               MethodName = methodName,
                               HashCode = hashCode
                           };
            }
        }

        #endregion
    }
}