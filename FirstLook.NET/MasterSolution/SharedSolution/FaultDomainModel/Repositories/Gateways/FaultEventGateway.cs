﻿using System;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.Fault.DomainModel.Repositories.Datastores;

namespace FirstLook.Fault.DomainModel.Repositories.Gateways
{
    /// <summary>
    /// Serves as the gateway to the FaultEvent table. Providing Insert and Fetch functionality to the table.
    /// </summary>
    public class FaultEventGateway : GatewayBase
    {
        /// <summary>
        /// Fetch a record from the FaultEvent table, searching by faultEventId.
        /// </summary>
        /// <param name="faultEventId">The faultEventId to search on.</param>
        /// <returns>The record containing that faultEventId (if one exists).</returns>
        public Row Fetch(int faultEventId)
        {
            Row row = null;
            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.FaultEvent_Fetch(faultEventId))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    row = serializer.Deserialize((IDataRecord)reader);

                }
            }

            return row;
        }

        /// <summary>
        /// Insert a record into the FaultEvent table.
        /// </summary>
        /// <param name="applicationId">Foreign key to application in application table.</param>
        /// <param name="machineId">Foreign key to machine in machine table.</param>
        /// <param name="platformId">Foreign key to platform in platform table.</param>
        /// <param name="faultId">Foreign key to fault in fault table.</param>
        /// <param name="faultTime">Timestamp of the event.</param>
        /// <returns>Returns the inserted row.</returns>
        public Row Insert(int? applicationId, int? machineId, int? platformId, int faultId, DateTime faultTime)
        {
            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.FaultEvent_Insert(applicationId, machineId, platformId, faultId, faultTime))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    return serializer.Deserialize((IDataRecord)reader);
                }
            }

            return null;
        }

        #region Nested type: Row

        [Serializable]
        public class Row
        {
            public int FaultEventId { get; set; }

            public int? ApplicationId { get; set; }  

            public int? MachineId { get; set; }

            public int? PlatformId { get; set; }

            public int FaultId { get; set; }

            public DateTime FaultTime { get; set; }
        }

        #endregion

        #region Nested type: RowSerializer

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int faultEventId = record.GetInt32(record.GetOrdinal("FaultEventID"));

                int? applicationId = DataRecord.GetNullableInt32(record, "ApplicationID");

                int? machineId = DataRecord.GetNullableInt32(record, "MachineID");

                int? platformId = DataRecord.GetNullableInt32(record, "PlatformID");

                int faultId = record.GetInt32(record.GetOrdinal("FaultID"));

                DateTime faultTime = record.GetDateTime(record.GetOrdinal("FaultTime"));

                return new Row
                           {
                               FaultEventId = faultEventId,
                               ApplicationId = applicationId,
                               MachineId = machineId,
                               PlatformId = platformId,
                               FaultId = faultId,
                               FaultTime = faultTime
                           };
            }
        }

        #endregion
    }
}