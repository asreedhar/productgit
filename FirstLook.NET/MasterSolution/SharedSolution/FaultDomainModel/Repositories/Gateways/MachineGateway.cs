﻿using System;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Fault.DomainModel.Repositories.Datastores;

namespace FirstLook.Fault.DomainModel.Repositories.Gateways
{
    /// <summary>
    /// Serves as the gateway to the Machine table. Providing Insert and Fetch functionality to the table.
    /// </summary>
    public class MachineGateway : GatewayBase
    {
        /// <summary>
        /// Fetch a record from the machine table, searching by machine name.
        /// </summary>
        /// <param name="machine">The machine name to search on.</param>
        /// <returns>The record containing that machine name (if one exists).</returns>
        public Row Fetch(string machine)
        {
            Row row = null;

            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.Machine_Fetch(machine))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    row = serializer.Deserialize((IDataRecord)reader);
                }
            }

            return row;
        }

        public Row Fetch(int id)
        {
            Row row = null;

            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.Machine_Fetch(id))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    row = serializer.Deserialize((IDataRecord)reader);
                }
            }

            return row;
        }

        /// <summary>
        /// Insert a record into the machine table using the provided machine name.
        /// </summary>
        /// <param name="name">The name of the machine to insert.</param>
        /// <returns>Returns the inserted row.</returns>
        public Row Insert(string name)
        {
            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.Machine_Insert(name))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    return serializer.Deserialize((IDataRecord)reader);
                }
            }

            return null;
        }

        #region Nested type: Row

        [Serializable]
        public class Row
        {
            public int MachineId { get; set; }
            public string Name { get; set; }
        }

        #endregion

        #region Nested type: RowSerializer

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int machineId = record.GetInt32(record.GetOrdinal("MachineID")); 
                string name = record.GetString(record.GetOrdinal("Name"));
                return new Row {MachineId = machineId, Name = name};
            }
        }

        #endregion
    }
}