﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Fault.DomainModel.Model.Reporting;
using FirstLook.Fault.DomainModel.Repositories.Datastores;
using FirstLook.Fault.DomainModel.Repositories.Entities;
using FirstLook.Fault.DomainModel.Repositories.Entities.Reporting;

namespace FirstLook.Fault.DomainModel.Repositories.Gateways
{
    public class ReportingGateway : GatewayBase
    {
        protected class ApplicationSummarySerializer : Serializer<IApplicationSummary>
        {
            public override IApplicationSummary Deserialize(IDataRecord record)
            {
                int applicationId = record.GetInt32(record.GetOrdinal("ApplicationID"));

                string applicationName = record.GetString(record.GetOrdinal("Name"));

                int numberOfFaultEvents = record.GetInt32(record.GetOrdinal("NumberOfFaultEvents"));

                int numberOfFaults = record.GetInt32(record.GetOrdinal("NumberOfFaults"));

                return new ApplicationSummary
                {
                    Application = new Application
                    {
                        Id = applicationId,
                        Name = applicationName
                    },
                    NumberOfFaultEvents = numberOfFaultEvents,
                    NumberOfFaults = numberOfFaults
                };
            }
        }

        protected class FaultSummarySerializer : Serializer<IFaultSummary>
        {
            public override IFaultSummary Deserialize(IDataRecord record)
            {
                int applicationId = record.GetInt32(record.GetOrdinal("ApplicationID"));

                string applicationName = record.GetString(record.GetOrdinal("Name"));
                
                int faultId = record.GetInt32(record.GetOrdinal("FaultID"));

                string exceptionType = record.GetString(record.GetOrdinal("ExceptionType"));

                string message = record.GetString(record.GetOrdinal("Message"));

                DateTime minFaultTime = record.GetDateTime(record.GetOrdinal("MinFaultTime"));

                DateTime maxFaultTime = record.GetDateTime(record.GetOrdinal("MaxFaultTime"));

                int maxFaultEventId = record.GetInt32(record.GetOrdinal("MaxFaultEventID"));

                int numFaultEvent = record.GetInt32(record.GetOrdinal("NumFaultEvent"));

                return new FaultSummary
                {
                    Application = new Application
                    {
                        Id = applicationId,
                        Name = applicationName
                    },
                    ExceptionType = exceptionType,
                    FaultId = faultId,
                    Message = message,
                    MinFaultTime = minFaultTime,
                    MaxFaultTime = maxFaultTime,
                    MaxFaultEventId = maxFaultEventId,
                    NumFaultEvent = numFaultEvent
                };
            }
        }

        protected class FaultEventSummarySerializer : Serializer<IFaultEventSummary>
        {
            public override IFaultEventSummary Deserialize(IDataRecord record)
            {
                int applicationOrdinal = record.GetOrdinal("ApplicationID");

                int applicationId = 0;

                string applicationName = string.Empty;

                if (!record.IsDBNull(applicationOrdinal))
                {
                    applicationId = record.GetInt32(applicationOrdinal);

                    applicationName = record.GetString(record.GetOrdinal("ApplicationName"));
                }

                int machineOrdinal = record.GetOrdinal("MachineID");
                
                int machineId = 0;

                string machineName = string.Empty;

                if (!record.IsDBNull(machineOrdinal))
                {
                    machineId = record.GetInt32(machineOrdinal);

                    machineName = record.GetString(record.GetOrdinal("MachineName"));
                }

                int platformOrdinal = record.GetOrdinal("PlatformID");

                int platformId = 0;

                string platformName = string.Empty;

                if (!record.IsDBNull(platformOrdinal))
                {
                    platformId = record.GetInt32(platformOrdinal);

                    platformName = record.GetString(record.GetOrdinal("PlatformName"));
                }

                int faultId = record.GetInt32(record.GetOrdinal("FaultID"));

                long faultRank = record.GetInt64(record.GetOrdinal("FaultRank"));

                DateTime faultTime = record.GetDateTime(record.GetOrdinal("FaultTime"));

                int faultEventId = record.GetInt32(record.GetOrdinal("FaultEventID"));

                return new FaultEventSummary
                {
                    Application = new Application{Id = applicationId, Name = applicationName},
                    Machine = new Machine { Id = machineId, Name = machineName },
                    Platform = new Platform { Id = platformId, Name = platformName },
                    FaultTime = faultTime,
                    FaultId = faultId,
                    Id = faultEventId,
                    Rank = faultRank
                };
            }
        }

        public IList<IApplicationSummary> ApplicationSummary()
        {
            IList<IApplicationSummary> rows;

            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.ReportSummaryApplication())
            {
                ISerializer<IApplicationSummary> serializer = new ApplicationSummarySerializer();

                rows = serializer.Deserialize(reader);
            }

            return rows;
        }

        public Page<IFaultSummary> FaultSummary(int? applicationId, PageArguments pagination)
        {
            Page<IFaultSummary> page = new Page<IFaultSummary>();

            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.ReportSummaryFault(applicationId, pagination.SortExpression, pagination.StartRowIndex, pagination.MaximumRows))
            {
                ISerializer<IFaultSummary> serializer = new FaultSummarySerializer();

                page.Items = serializer.Deserialize(reader);

                if (reader.NextResult())
                {
                    if (reader.Read())
                    {
                        page.TotalRowCount = reader.GetInt32(reader.GetOrdinal("TotalRowCount"));
                    }
                }
            }

            return page;
        }

        public IFaultEventSummary FaultEventSummaryById(int faultId, int faultEventId)
        {
            IFaultEventSummary row = null;

            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.ReportSummaryFaultEventById(faultId, faultEventId))
            {
                if (reader.Read())
                {
                    ISerializer<IFaultEventSummary> serializer = new FaultEventSummarySerializer();

                    row = serializer.Deserialize((IDataRecord) reader);
                }
            }

            return row;
        }

        public IFaultEventSummary FaultEventSummaryByRank(int faultId, int rank)
        {
            IFaultEventSummary row = null;

            var datastore = Resolve<IFaultDatastore>();

            using (IDataReader reader = datastore.ReportSummaryFaultEventByRank(faultId, rank))
            {
                if (reader.Read())
                {
                    ISerializer<IFaultEventSummary> serializer = new FaultEventSummarySerializer();

                    row = serializer.Deserialize((IDataRecord)reader);
                }
            }

            return row;
        }
    }
}
