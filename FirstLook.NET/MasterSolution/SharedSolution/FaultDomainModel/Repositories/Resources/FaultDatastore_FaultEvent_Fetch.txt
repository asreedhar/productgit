﻿SELECT  F.FaultEventID, F.FaultTime, F.ApplicationID, F.MachineID, F.PlatformID, F.FaultID
FROM    Fault.FaultEvent F
WHERE   F.FaultEventID = @FaultEventID
