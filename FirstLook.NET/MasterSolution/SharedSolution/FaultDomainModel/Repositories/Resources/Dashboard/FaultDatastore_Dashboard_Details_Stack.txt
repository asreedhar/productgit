﻿
; WITH Seed (StackID, CauseID, Ctr) AS (
	SELECT	F.StackID, F.CauseID, 1
	FROM	Fault.Fault F
	WHERE	F.FaultID = @FaultID
UNION ALL
	SELECT	F.StackID, F.CauseID, S.Ctr+1
	FROM	Seed S
	JOIN	Fault.Fault F ON F.StackID = S.CauseID
)
SELECT	S.StackID ,
        S.CauseID ,
        S.Ctr
FROM	Fault.Fault F
JOIN	Seed S ON S.StackID = F.FaultID
