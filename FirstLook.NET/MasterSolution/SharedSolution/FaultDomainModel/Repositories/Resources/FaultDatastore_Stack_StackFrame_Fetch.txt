﻿SELECT  S.StackID, S.StackFrameID, S.SequenceID
FROM    Fault.Stack_StackFrame S
WHERE   S.StackID = @StackID
AND		S.StackFrameID = @StackFrameID
AND		S.SequenceID = @SequenceID
