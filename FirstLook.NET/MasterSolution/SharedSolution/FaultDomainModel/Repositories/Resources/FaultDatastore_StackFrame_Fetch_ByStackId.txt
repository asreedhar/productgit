﻿SELECT	F.StackFrameID ,
        F.HashCode ,
        F.ClassName ,
        F.FileName ,
        F.IsNativeMethod ,
        F.IsUnknownSource ,
        F.LineNumber ,
        F.MethodName
FROM	Fault.Stack_StackFrame S
JOIN	Fault.StackFrame F ON S.StackFrameID = F.StackFrameID
WHERE	S.StackID = @StackID
ORDER
BY		S.SequenceID
