﻿SELECT  R.RequestInformationID, R.[Path], R.Url, R.[User], R.UserHostAddress, R.FaultEventID
FROM    Fault.RequestInformation R
WHERE   R.FaultEventID = @FaultEventID