﻿SELECT  S.StackID, S.HashCode
FROM    Fault.Stack S
WHERE   S.HashCode = @HashCode
