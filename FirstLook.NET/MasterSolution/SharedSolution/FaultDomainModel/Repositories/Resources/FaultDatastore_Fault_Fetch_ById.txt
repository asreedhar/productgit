﻿SELECT  F.FaultID, F.StackID, F.CauseID, F.ExceptionType, F.HashCode
FROM    Fault.Fault F
WHERE   F.FaultID = @FaultID