﻿using System;

namespace FirstLook.Fault.DomainModel.Commands.TransferObjects
{
    /// <summary>
    /// A data transfer object respresenting a faultdata entry.
    /// </summary>
    [Serializable]
    public class FaultDataDto
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }
}
