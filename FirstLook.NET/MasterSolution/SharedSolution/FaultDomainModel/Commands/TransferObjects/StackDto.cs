﻿using System;
using System.Collections.Generic;

namespace FirstLook.Fault.DomainModel.Commands.TransferObjects
{
    /// <summary>
    /// A data transfer object representing a stack.
    /// </summary>
    [Serializable]
    public class StackDto
    {
        public List<StackFrameDto> StackFrames { get; set; }
    }
}
