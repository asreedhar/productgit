﻿using System;
using System.Collections.Generic;

namespace FirstLook.Fault.DomainModel.Commands.TransferObjects
{
    /// <summary>
    /// A data transfer object representing a fault event.
    /// </summary>
    [Serializable]
    public class FaultEventDto
    {
        public DateTime FaultTime { get; set; }

        public FaultDto Fault { get; set; }

        public List<FaultDataDto> FaultData { get; set; }

        public RequestInformationDto RequestInformation { get; set; }

        public ApplicationDto Application { get; set; }

        public MachineDto Machine { get; set; }

        public PlatformDto Platform { get; set; }

        public int Id { get; set; }
    }
}
