﻿using System;

namespace FirstLook.Fault.DomainModel.Commands.TransferObjects
{
    /// <summary>
    /// A data transfer object representing requestor information.
    /// </summary>
    [Serializable]
    public class RequestInformationDto
    {
        public string Path { get; set; }

        public string Url { get; set; }

        public string User { get; set; }

        public string UserHostAddress { get; set; }
    }
}
