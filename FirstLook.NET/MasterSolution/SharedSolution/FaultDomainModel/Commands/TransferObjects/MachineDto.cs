﻿using System;

namespace FirstLook.Fault.DomainModel.Commands.TransferObjects
{
    /// <summary>
    /// A data transfer object representing a machine.
    /// </summary>
    [Serializable]
    public class MachineDto
    {
        public string Name { get; set; }
    }
}
