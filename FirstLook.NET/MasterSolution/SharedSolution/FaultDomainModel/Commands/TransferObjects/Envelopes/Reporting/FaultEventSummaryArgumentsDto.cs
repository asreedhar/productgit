﻿using System;

namespace FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes.Reporting
{
    [Serializable]
    public class FaultEventSummaryArgumentsDto
    {
        public int FaultId { get; set; }

        public bool HasFaultEventId { get; set; }

        public int FaultEventId { get; set; }

        public bool HasFaultRank { get; set; }

        public int FaultRank { get; set; }
    }
}
