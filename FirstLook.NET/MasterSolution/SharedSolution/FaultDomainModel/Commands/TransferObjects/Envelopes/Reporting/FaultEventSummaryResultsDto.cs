﻿using System;
using FirstLook.Fault.DomainModel.Commands.TransferObjects.Reporting;

namespace FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes.Reporting
{
    [Serializable]
    public class FaultEventSummaryResultsDto
    {
        public FaultEventSummaryArgumentsDto Arguments { get; set; }

        public FaultEventSummaryDto FaultEventSummary { get; set; }
    }
}
