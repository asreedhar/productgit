﻿using System;
using System.Collections.Generic;
using FirstLook.Fault.DomainModel.Commands.TransferObjects.Reporting;

namespace FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes.Reporting
{
    [Serializable]
    public class ApplicationSummaryResultsDto
    {
        public ApplicationSummaryArgumentsDto Arguments { get; set; }

        public List<ApplicationSummaryDto> Results { get; set; }
    }
}
