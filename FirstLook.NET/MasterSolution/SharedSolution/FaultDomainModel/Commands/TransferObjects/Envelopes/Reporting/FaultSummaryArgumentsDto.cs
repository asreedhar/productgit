﻿using System;
using System.Collections.Generic;
using FirstLook.Fault.DomainModel.Commands.TransferObjects.Reporting;

namespace FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes.Reporting
{
    [Serializable]
    public class FaultSummaryArgumentsDto
    {
        public string ApplicationName { get; set; }

        #region Pagination

        public List<SortColumnDto> SortColumns { get; set; }

        public int MaximumRows { get; set; }

        public int StartRowIndex { get; set; }

        #endregion
    }
}
