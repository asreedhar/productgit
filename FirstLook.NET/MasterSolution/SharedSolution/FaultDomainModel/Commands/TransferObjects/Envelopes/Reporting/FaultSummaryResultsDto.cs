﻿using System;
using System.Collections.Generic;
using FirstLook.Fault.DomainModel.Commands.TransferObjects.Reporting;

namespace FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes.Reporting
{
    [Serializable]
    public class FaultSummaryResultsDto
    {
        public FaultSummaryArgumentsDto Arguments { get; set; }

        public List<FaultSummaryDto> Results { get; set; }

        public int TotalRowCount { get; set; }
    }
}
