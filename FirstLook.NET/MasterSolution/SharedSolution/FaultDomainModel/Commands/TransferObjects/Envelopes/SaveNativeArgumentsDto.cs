﻿using System;

namespace FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// An object that contains information required to save a fault event. (Used for native exception handling)
    /// </summary>
    [Serializable]
    public class SaveNativeArgumentsDto
    {
        public string Application { get; set; }

        public string Machine { get; set; }

        public string Platform { get; set; }

        public DateTime Timestamp { get; set; }

        public string User { get; set; }

        public string Path { get; set; }

        public string Url { get; set; }

        public string UserHostAddress { get; set; }

        public Exception Fault { get; set; }
    }
}
