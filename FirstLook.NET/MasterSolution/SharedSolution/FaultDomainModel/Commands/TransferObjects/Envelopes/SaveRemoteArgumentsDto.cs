﻿using System;

namespace FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// An object containing a FaultEventDto, the information coming in on a web service request.
    /// </summary>
    [Serializable]
    public class SaveRemoteArgumentsDto
    {
        public FaultEventDto FaultEvent { get; set; }
    }
}
