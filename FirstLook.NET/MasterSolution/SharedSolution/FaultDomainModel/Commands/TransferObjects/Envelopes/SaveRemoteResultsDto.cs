﻿using System;

namespace FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// An object containing a FaultEventDto returned to the application that invoked the web service.
    /// </summary>
    [Serializable]
    public class SaveRemoteResultsDto
    {
        public FaultEventDto FaultEvent { get; set; }
    }
}