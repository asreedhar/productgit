﻿using System;

namespace FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class FetchFaultEventResultsDto
    {
        public FetchFaultEventArgumentsDto Arguments { get; set; }

        public FaultEventDto FaultEvent { get; set; }
    }
}
