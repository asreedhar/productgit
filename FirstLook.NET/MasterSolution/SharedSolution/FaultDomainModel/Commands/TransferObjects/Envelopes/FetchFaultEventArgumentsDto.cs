﻿using System;

namespace FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class FetchFaultEventArgumentsDto
    {
        public int FaultEventId { get; set; }
    }
}
