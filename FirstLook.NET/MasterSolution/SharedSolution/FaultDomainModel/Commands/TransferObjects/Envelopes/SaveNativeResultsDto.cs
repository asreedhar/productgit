﻿using System;
using FirstLook.Fault.DomainModel.Repositories.Entities;

namespace FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// An object to return to the caller indicating the faultevent has been saved.
    /// </summary>
    [Serializable]
    public class SaveNativeResultsDto
    {
        public FaultEvent FaultEvent { get; set; }
    }
}
