﻿using System;

namespace FirstLook.Fault.DomainModel.Commands.TransferObjects.Reporting
{
    [Serializable]
    public class FaultSummaryDto
    {
        public ApplicationDto Application { get; set; }

        public string ExceptionType { get; set; }

        public string Message { get; set; }

        public int FaultId { get; set; }

        public DateTime MinFaultTime { get; set; }

        public DateTime MaxFaultTime { get; set; }

        public int MaxFaultEventId { get; set; }

        public int NumFaultEvent { get; set; }
    }
}
