﻿using System;

namespace FirstLook.Fault.DomainModel.Commands.TransferObjects.Reporting
{
    [Serializable]
    public class ApplicationSummaryDto
    {
        public ApplicationDto Application { get; set; }

        public int NumberOfFaults { get; set; }

        public int NumberOfFaultEvents { get; set; }
    }
}
