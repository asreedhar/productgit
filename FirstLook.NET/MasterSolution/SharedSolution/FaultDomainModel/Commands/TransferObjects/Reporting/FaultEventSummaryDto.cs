﻿using System;

namespace FirstLook.Fault.DomainModel.Commands.TransferObjects.Reporting
{
    [Serializable]
    public class FaultEventSummaryDto
    {
        public ApplicationDto Application { get; set; }

        public MachineDto Machine { get; set; }

        public PlatformDto Platform { get; set; }

        public DateTime FaultTime { get; set; }

        public int FaultId { get; set; }

        public int Id { get; set; }

        public long Rank { get; set; }

        public string Message { get; set; }
    }
}
