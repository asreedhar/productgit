﻿using System;

namespace FirstLook.Fault.DomainModel.Commands.TransferObjects.Reporting
{
    [Serializable]
    public class SortColumnDto
    {
        private string _columnName;
        private bool _ascending;

        public string ColumnName
        {
            get { return _columnName; }
            set { _columnName = value; }
        }

        public bool Ascending
        {
            get { return _ascending; }
            set { _ascending = value; }
        }
    }
}