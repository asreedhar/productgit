﻿using System;

namespace FirstLook.Fault.DomainModel.Commands.TransferObjects
{
    /// <summary>
    /// A data transfer object representing a Fault.
    /// </summary>
    [Serializable]
    public class FaultDto
    {
        public FaultDto Cause { get; set; }

        public string ExceptionType { get; set; }

        public string Message { get; set; }

        public StackDto Stack { get; set; }
    }
}
