﻿using System;

namespace FirstLook.Fault.DomainModel.Commands.TransferObjects
{
    /// <summary>
    /// A data transfer object representing a stack frame.
    /// </summary>
    [Serializable]
    public class StackFrameDto
    {
        public string ClassName { get; set; }

        public string FileName { get; set; }

        public string MethodName { get; set; }

        public bool IsNativeMethod { get; set; }

        public bool IsUnknownSource { get; set; }

        public int? LineNumber { get; set; }
    }
}
