﻿using System;

namespace FirstLook.Fault.DomainModel.Commands.TransferObjects
{
    /// <summary>
    /// A data transfer object representing a platform.
    /// </summary>
    [Serializable]
    public class PlatformDto
    {
        public string Name { get; set; }
    }
}
