﻿using System;

namespace FirstLook.Fault.DomainModel.Commands.TransferObjects
{
    /// <summary>
    /// A data transfer object representing an application.
    /// </summary>
    [Serializable]
    public class ApplicationDto
    {
        public string Name { get; set; }
    }
}
