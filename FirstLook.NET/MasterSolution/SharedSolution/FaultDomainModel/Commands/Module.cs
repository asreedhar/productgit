﻿using FirstLook.Common.Core.Registry;
using FirstLook.Fault.DomainModel.Commands.Impl;

namespace FirstLook.Fault.DomainModel.Commands
{
    /// <summary>
    /// The intent of this class is to add pertinent interfaces and implementing classes to the registry via the Configure method.
    /// </summary>
    public class Module : IModule
    {
        #region IModule Members

        /// <summary>
        /// Add pertinent interfaces and impelementing classes to the registry.
        /// </summary>
        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory, CommandFactory>(ImplementationScope.Shared);
        }

        #endregion
    }
}
