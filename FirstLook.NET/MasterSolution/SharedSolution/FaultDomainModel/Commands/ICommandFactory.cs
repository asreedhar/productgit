﻿using FirstLook.Common.Core.Command;
using FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes;
using FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes.Reporting;

namespace FirstLook.Fault.DomainModel.Commands
{
    /// <summary>
    /// Return the appropriate instance of a save command, whether that be one being invoked via a web service (ie called from a Java 
    /// based application), or natively to handle a C# excpeption.
    /// </summary>
    public interface ICommandFactory
    {
        /// <summary>
        /// Return a command that has been decorated with the fault handling code. That way if an exception is generated
        /// when executing the command it is saved to the Fault database.
        /// </summary>
        /// <typeparam name="TResults">A generic representing the results returned to the user.</typeparam>
        /// <typeparam name="TParameters">A generic representing the parameters passed in on the call to be passed into the callers command.</typeparam>
        /// <param name="cmd">The command to wrap in a try catch block to catch any exceptions.</param>
        /// <param name="args">Information required for saving the exception (user, url, path... etc)</param>
        /// <returns></returns>
        ICommand<TResults, TParameters> DecorateCommand<TResults, TParameters>(ICommand<TResults,TParameters> cmd, SaveNativeArgumentsDto args);

        /// <summary>
        /// Return a new save command supporting web service invocation.
        /// </summary>
        /// <returns>A new save command.</returns>
        ICommand<SaveRemoteResultsDto, SaveRemoteArgumentsDto> CreateSaveCommand();

        /// <summary>
        /// Returns a native save command to handle C# exceptions.
        /// </summary>
        /// <returns>Returns a native save command to handle C# exceptions.</returns>
        ICommand<SaveNativeResultsDto,SaveNativeArgumentsDto> CreateNativeSaveCommand();

        /// <summary>
        /// Return a new fetch command supporting web service invocation.
        /// </summary>
        /// <returns>A new save command.</returns>
        ICommand<FetchFaultEventResultsDto, FetchFaultEventArgumentsDto> CreateFetchFaultEventCommand();

        #region Reporting

        ICommand<ApplicationSummaryResultsDto, ApplicationSummaryArgumentsDto> CreateApplicationSummaryCommand();

        ICommand<FaultSummaryResultsDto, FaultSummaryArgumentsDto> CreateFaultSummaryCommand();

        ICommand<FaultEventSummaryResultsDto, FaultEventSummaryArgumentsDto> CreateFaultEventSummaryCommand();

        #endregion
    }
}
