﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.Fault.DomainModel.Commands.TransferObjects;
using FirstLook.Fault.DomainModel.Commands.TransferObjects.Reporting;
using FirstLook.Fault.DomainModel.Model;
using FirstLook.Fault.DomainModel.Model.Reporting;
using FirstLook.Fault.DomainModel.Repositories.Entities;

namespace FirstLook.Fault.DomainModel.Commands.Impl
{
    /// <summary>
    /// This is responsible for taking in a fault event, and converting it to a serializable
    /// FaultEventDto to be shipped back to the application that invoked this webservice.
    /// </summary>
    public static class Mapper
    {
        /// <summary>
        /// Map a faultEvent to a FaultEventDto.
        /// </summary>
        /// <param name="faultEvent">The fault event to be mapped.</param>
        /// <returns>A FaultEventDto is returned back to the application that invoked the web service.</returns>
        public static FaultEventDto Map(IFaultEvent faultEvent)
        {
            //Begin creating a FaultEventDto from the fault event information
            FaultEventDto returnFaultEvent = new FaultEventDto();
            ApplicationDto application = new ApplicationDto {Name = faultEvent.Application.Name};
            MachineDto machine = new MachineDto{Name = faultEvent.Machine.Name};
            PlatformDto platform = new PlatformDto {Name = faultEvent.Platform.Name};


            IRequestInformation information = faultEvent.RequestInfo;
            RequestInformationDto requestInformation = null;

            //Process the request information
            if (information != null)
            {
                requestInformation = new RequestInformationDto
                {
                    Path = information.Path,
                    Url = information.Url,
                    User = information.User,
                    UserHostAddress = information.UserHostAddress
                };
            }

            List<FaultDataDto> faultDataDtos = new List<FaultDataDto>();

            //Add the key/value pairs associated with the Data attribute of an exception.
            foreach(FaultData faultData in faultEvent.Data)
            {
                faultDataDtos.Add(new FaultDataDto{Key = faultData.Key,Value = faultData.Value});
            }

            FaultDto fault = new FaultDto();
            if(faultEvent.Fault.Cause != null)
            {
                //Recursively go through all Cause Faults constructing an appropriate FaultDto and linking it to it's parent.
                fault.Cause = ProcessCauseFault(faultEvent.Fault.Cause);
            }

            BuildFault(fault, faultEvent.Fault);

            returnFaultEvent.Application = application;
            returnFaultEvent.Machine = machine;
            returnFaultEvent.Platform = platform;
            returnFaultEvent.FaultData = faultDataDtos;
            returnFaultEvent.RequestInformation = requestInformation;
            returnFaultEvent.FaultTime = faultEvent.Timestamp;
            returnFaultEvent.Id = faultEvent.Id;
            returnFaultEvent.Fault = fault;

            return returnFaultEvent;
        }


        /// <summary>
        /// This will recursivley go through all of the cause faults and build the respective FaultEventDto chain.
        /// </summary>
        /// <param name="causeFault">The cause fault to be built and linked to the parent node.</param>
        /// <returns>Returns the created cause fault in the form of a FaultDto.</returns>
        private static FaultDto ProcessCauseFault(IFault causeFault)
        {
            //Initialize a new FaultDto object and call build fault to populate the object with the appropriate attributes
            FaultDto nextFault = new FaultDto();
            BuildFault(nextFault,causeFault);

            //Process the cause/inner fault if one exists
            if(causeFault.Cause != null)
            {
                nextFault.Cause = ProcessCauseFault(causeFault.Cause);
            }

            return nextFault;

        }


        /// <summary>
        /// Populates a FaultDto object from a native IFault object.
        /// </summary>
        /// <param name="fault">The FaultDto to build.</param>
        /// <param name="nativeFault">The native fault command to build the FaultDto from.</param>
        private static void BuildFault(FaultDto fault, IFault nativeFault)
        {
            fault.ExceptionType = nativeFault.ExceptionType;
            fault.Message = nativeFault.Message;
            fault.Stack = new StackDto { StackFrames = new List<StackFrameDto>() };

            foreach (StackFrame frame in nativeFault.Stack.Frames)
            {
                fault.Stack.StackFrames.Add(new StackFrameDto
                {
                    ClassName = frame.ClassName,
                    FileName = frame.FileName,
                    IsNativeMethod = frame.IsNativeMethod,
                    IsUnknownSource = frame.IsUnknownSource,
                    LineNumber = frame.LineNumber,
                    MethodName = frame.MethodName
                });
            }

        }

        public static List<ApplicationSummaryDto> Map(IList<IApplicationSummary> list)
        {
            return list.Select(Map).ToList();
        }

        public static ApplicationSummaryDto Map(IApplicationSummary item)
        {
            if (item == null) return null;

            return new ApplicationSummaryDto
            {
                Application = new ApplicationDto { Name = item.Application.Name },
                NumberOfFaultEvents = item.NumberOfFaultEvents,
                NumberOfFaults = item.NumberOfFaults
            };
        }

        public static List<FaultSummaryDto> Map(IList<IFaultSummary> list)
        {
            return list.Select(Map).ToList();
        }

        public static FaultSummaryDto Map(IFaultSummary item)
        {
            if (item == null) return null;

            return new FaultSummaryDto
            {
                Application = new ApplicationDto { Name = item.Application.Name },
                ExceptionType = item.ExceptionType,
                Message = item.Message,
                FaultId = item.FaultId,
                MinFaultTime = item.MinFaultTime,
                MaxFaultEventId = item.MaxFaultEventId,
                MaxFaultTime = item.MaxFaultTime,
                NumFaultEvent = item.NumFaultEvent
            };
        }

        public static FaultEventSummaryDto Map(IFaultEventSummary item)
        {
            if (item == null) return null;

            return new FaultEventSummaryDto
            {
                Application = new ApplicationDto { Name = item.Application.Name },
                Machine = new MachineDto { Name = item.Machine.Name },
                Platform = new PlatformDto { Name = item.Platform.Name },
                FaultId = item.FaultId,
                FaultTime = item.FaultTime,
                Id = item.Id,
                Rank = item.Rank
            };
        }
    }
}
