﻿using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes.Reporting;
using FirstLook.Fault.DomainModel.Model;
using FirstLook.Fault.DomainModel.Model.Reporting;

namespace FirstLook.Fault.DomainModel.Commands.Impl.Reporting
{
    public class ApplicationSummaryCommand : ICommand<ApplicationSummaryResultsDto, ApplicationSummaryArgumentsDto>
    {
        public ApplicationSummaryResultsDto Execute(ApplicationSummaryArgumentsDto parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            var repository = resolver.Resolve<IFaultRepository>();

            IList<IApplicationSummary> list = repository.ApplicationSummary();

            return new ApplicationSummaryResultsDto
            {
                Arguments = parameters,
                Results = Mapper.Map(list)
            };
        }
    }
}
