﻿using System.Linq;
using System.Collections.Generic;
using System.Text;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes.Reporting;
using FirstLook.Fault.DomainModel.Commands.TransferObjects.Reporting;
using FirstLook.Fault.DomainModel.Model;
using FirstLook.Fault.DomainModel.Model.Reporting;

namespace FirstLook.Fault.DomainModel.Commands.Impl.Reporting
{
    public class FaultSummaryCommand : ICommand<FaultSummaryResultsDto, FaultSummaryArgumentsDto>
    {
        internal static readonly IList<string> ColumnNames;

        static FaultSummaryCommand()
        {
            ColumnNames = new List<string>
            {
                "ApplicationName",
                "ExceptionType",
                "Message",
                "FaultId",
                "MinFaultTime",
                "MaxFaultTime",
                "MaxFaultEventId",
                "NumFaultEvent"
            }.AsReadOnly();
        }

        public FaultSummaryResultsDto Execute(FaultSummaryArgumentsDto parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            var repository = resolver.Resolve<IFaultRepository>();

            Page<IFaultSummary> page = repository.FaultSummary(parameters.ApplicationName, Map(ColumnNames, parameters));

            return new FaultSummaryResultsDto
            {
                Arguments = parameters,
                Results = Mapper.Map(page.Items),
                TotalRowCount = page.TotalRowCount
            };
        }

        public static PageArguments Map(IEnumerable<string> columnNames, FaultSummaryArgumentsDto arguments)
        {
            // force sensible default sort order

            List<SortColumnDto> sortColumnDtos = arguments.SortColumns ?? new List<SortColumnDto>();

            if (sortColumnDtos.Count == 0)
            {
                sortColumnDtos.Add(new SortColumnDto { Ascending = false, ColumnName = "MaxFaultTime" });
            }

            // build sort expression

            StringBuilder sortExpression = new StringBuilder();

            foreach (SortColumnDto sortColumn in sortColumnDtos)
            {
                string name = sortColumn.ColumnName;

                if (columnNames.Contains(name))
                {
                    if (sortExpression.Length > 0)
                    {
                        sortExpression.Append(", ");
                    }

                    sortExpression.Append(name);

                    if (sortColumn.Ascending)
                    {
                        sortExpression.Append(" ASC");
                    }
                    else
                    {
                        sortExpression.Append(" DESC");
                    }
                }
            }

            int maximumRows = arguments.MaximumRows;

            if (maximumRows < 1)
            {
                maximumRows = 1;
            }

            int startRowIndex = arguments.StartRowIndex;

            if (startRowIndex < 0)
            {
                startRowIndex = 0;
            }

            return new PageArguments
            {
                MaximumRows = maximumRows,
                SortExpression = sortExpression.ToString(),
                StartRowIndex = startRowIndex
            };
        }
    }
}
