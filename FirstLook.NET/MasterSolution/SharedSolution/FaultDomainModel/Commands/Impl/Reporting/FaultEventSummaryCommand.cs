﻿using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes.Reporting;
using FirstLook.Fault.DomainModel.Model;
using FirstLook.Fault.DomainModel.Model.Reporting;

namespace FirstLook.Fault.DomainModel.Commands.Impl.Reporting
{
    public class FaultEventSummaryCommand : ICommand<FaultEventSummaryResultsDto, FaultEventSummaryArgumentsDto>
    {
        public FaultEventSummaryResultsDto Execute(FaultEventSummaryArgumentsDto parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            var repository = resolver.Resolve<IFaultRepository>();

            IFaultEventSummary item = null;
            
            if (parameters.HasFaultEventId)
            {
                item = repository.FaultEventSummaryById(parameters.FaultId, parameters.FaultEventId);
            }
            else if (parameters.HasFaultRank)
            {
                item = repository.FaultEventSummaryByRank(parameters.FaultId, parameters.FaultRank);
            }
            
            return new FaultEventSummaryResultsDto
            {
                Arguments = parameters,
                FaultEventSummary = Mapper.Map(item)
            };
        }
    }
}
