﻿using System;
using System.Diagnostics;
using System.Text;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes;
using FirstLook.Fault.DomainModel.Commands.TransferObjects;
using FirstLook.Fault.DomainModel.Model;
using FirstLook.Fault.DomainModel.Repositories;

namespace FirstLook.Fault.DomainModel.Commands.Impl
{
    /// <summary>
    /// Using the passed in information to construct a FaultEvent by using the FaultEventBuilder, then save the FaultEvent to the
    /// database. This implementation is for a web service invocation of the fault event handler.
    /// </summary>
    [Serializable]
    public class SaveRemoteCommand : ICommand<SaveRemoteResultsDto, SaveRemoteArgumentsDto>
    {
        #region ICommand<SaveResultsDto,SaveArgumentsDto> Members
        private const string SLog = "FaultHandler";
        private const string SEvent = "FaultLoggingException";

        /// <summary>
        /// Using the information provided in the SaveArgumentsDto, construct a fault event and save it to the db.
        /// </summary>
        /// <param name="parameters">Contains the necessary information to create a faultEvent to be saved.</param>
        /// <returns>Returns a saved fault event in the form of a SaveResultsDto to the caller.</returns>
        public SaveRemoteResultsDto Execute(SaveRemoteArgumentsDto parameters)
        {
            try
            {
                if (parameters == null)
                {
                    throw new ArgumentNullException("parameters");
                }

                IResolver resolver = RegistryFactory.GetResolver();

                //Initialize instances of the faulteventbuilder and faultrepository to construct and save a fault event.
                var builder = resolver.Resolve<IFaultEventBuilder>();
                var faultRepository = resolver.Resolve<IFaultRepository>();

                //Call the builder to set the appropriate attributes of the fault event being built.
                builder.Application = parameters.FaultEvent.Application.Name;
                builder.Machine = parameters.FaultEvent.Machine.Name;
                builder.Platform = parameters.FaultEvent.Platform.Name;
                builder.Timestamp = parameters.FaultEvent.FaultTime;

                //Process the request specific information if available.
                if (parameters.FaultEvent.RequestInformation != null)
                {
                    builder.BeginRequestorInformation();
                    builder.User = parameters.FaultEvent.RequestInformation.User;
                    builder.Path = parameters.FaultEvent.RequestInformation.Path;
                    builder.Url = parameters.FaultEvent.RequestInformation.Url;
                    builder.UserHostAddress = parameters.FaultEvent.RequestInformation.UserHostAddress;
                    builder.EndRequestorInformation();
                }

                //Construct a faultdata entry for each key/value data pair associated with the exception thrown.
                if (parameters.FaultEvent.FaultData != null)
                {
                    builder.BeginFaultData();

                    foreach (FaultDataDto fault in parameters.FaultEvent.FaultData)
                    {
                        builder.BeginFaultDataItem();
                        builder.Key = fault.Key;
                        builder.Value = fault.Value;
                        builder.EndFaultDataItem();
                    }

                    builder.EndFaultData();
                }

                if (parameters.FaultEvent.Fault != null)
                {
                    builder.BeginFault();

                    if (parameters.FaultEvent.Fault.Cause != null)
                    {
                        //If an inner exception (causing fault) was specified, recursively go through them all
                        ProcessCauseFaults(parameters.FaultEvent.Fault.Cause, builder);
                    }

                    //Call BuildFault to process the stacktrace and stack trace elements associated with this exception.
                    BuildFault(parameters.FaultEvent.Fault, builder);

                    builder.EndFault();
                }

                //Build the fault event.
                IFaultEvent toSave = builder.ToFaultEvent();

                //Save the fault event, returning the saved fault event.
                return new SaveRemoteResultsDto
                           {FaultEvent = Mapper.Map(((FaultRepository) faultRepository).Save(toSave))};
            }
            catch (Exception e)
            {
                //If there was an exception thrown while saving an exception, write it to the windows event log.
                if (parameters != null)
                {
                    string application = parameters.FaultEvent.Application.Name ?? "RemoteApplication";

                    if (!EventLog.SourceExists(application))
                        EventLog.CreateEventSource(application, SLog);

                    EventLog log = new EventLog { Source = application };
                    log.WriteEntry(SEvent);

                    if (parameters.FaultEvent != null)
                    {
                        StringBuilder builder = new StringBuilder();

                        FaultDto faultDto = parameters.FaultEvent.Fault;

                        while (faultDto != null)
                        {
                            if (faultDto.ExceptionType != null)
                            {
                                builder.Append("ExceptionType: " + faultDto.ExceptionType ?? string.Empty + "\r");
                            }

                            if (faultDto.Message != null)
                            {
                                builder.Append("Message: " + faultDto.Message ?? string.Empty + "\r");
                            }

                            if (faultDto.Stack != null &&
                                faultDto.Stack.StackFrames != null &&
                                faultDto.Stack.StackFrames.Count > 0)
                            {
                                foreach (StackFrameDto frame in faultDto.Stack.StackFrames)
                                {
                                    builder.Append(frame.ClassName ?? string.Empty);
                                    builder.Append("\t" + frame.FileName ?? string.Empty);
                                    builder.Append("\t" + frame.MethodName ?? string.Empty);
                                    builder.Append("\t" + frame.LineNumber ?? string.Empty);
                                    builder.Append("\r");
                                }
                            }

                            faultDto = faultDto.Cause;
                        }

                        builder.Append("\r\r\r" + "Fault Exception:" + e.StackTrace);

                        log.WriteEntry(builder.ToString(), EventLogEntryType.Error);
                    }
                }

                return new SaveRemoteResultsDto();
            }
        }

        #endregion

        /// <summary>
        ///  Recursivley go through all of the InnerExceptions (causing faults) building the appropriate chain
        ///  of fault events.
        /// </summary>
        /// <param name="faultChain">The exception we are currently dealing with.</param>
        /// <param name="builder">The builder used so we can continue building onto our fault event.</param>
        public void ProcessCauseFaults(FaultDto faultChain,IFaultEventBuilder builder)
        {
            builder.BeginCauseFault();
            //If there is no more inner exception that stop the recursion and return
            if (faultChain.Cause == null)
            {                
                BuildFault(faultChain, builder);
                builder.EndCauseFault();
                return;
            }

            //Process the inner fault before the current fault
            ProcessCauseFaults(faultChain.Cause,builder);
            BuildFault(faultChain, builder);
            builder.EndCauseFault();
            return;
    
        }

        /// <summary>
        /// This will build on to the builder fault, incorporating the stack and the stack trace elements.
        /// </summary>
        /// <param name="fault">The fault being built from.</param>
        /// <param name="builder">The active builder.</param>
        private static void BuildFault(FaultDto fault, IFaultEventBuilder builder)
        {
            builder.ExceptionType = fault.ExceptionType;
            builder.Message = fault.Message;

            if (fault.Stack != null)
            {
                builder.BeginStack();
                if (fault.Stack.StackFrames != null)
                {
                    foreach (StackFrameDto frame in fault.Stack.StackFrames)
                    {
                        builder.BeginStackFrame();
                        builder.ClassName = frame.ClassName;
                        builder.FileName = frame.FileName;
                        builder.MethodName = frame.MethodName;
                        builder.IsNativeMethod = frame.IsNativeMethod;
                        builder.IsUnknownSource = frame.IsUnknownSource;
                        builder.LineNumber = frame.LineNumber;
                        builder.EndStackFrame();
                    }

                }
                builder.EndStack();
            }
        }


    }

   
}