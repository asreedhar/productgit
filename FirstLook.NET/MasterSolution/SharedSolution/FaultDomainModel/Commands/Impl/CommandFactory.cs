﻿using FirstLook.Common.Core.Command;
using FirstLook.Fault.DomainModel.Commands.Impl.Reporting;
using FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes;
using FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes.Reporting;

namespace FirstLook.Fault.DomainModel.Commands.Impl
{
    /// <summary>
    /// Return the appropriate instance of a save command, whether that be one being invoked via a web service (ie called from a Java 
    /// based application), or natively to handle a C# exception.
    /// </summary>
    public class CommandFactory : ICommandFactory 
    {
        /// <summary>
        /// Return a new save command supporting web service invocation.
        /// </summary>
        /// <returns>A new save command.</returns>
        public ICommand<SaveRemoteResultsDto, SaveRemoteArgumentsDto> CreateSaveCommand()
        {
            return new SaveRemoteCommand();
        }

        /// <summary>
        /// Returns a native save command to handle C# exceptions.
        /// </summary>
        /// <returns>Returns a native save command to handle C# exceptions.</returns>
        public ICommand<SaveNativeResultsDto, SaveNativeArgumentsDto> CreateNativeSaveCommand()
        {
            return new NativeSaveCommand();
        }

        public ICommand<FetchFaultEventResultsDto, FetchFaultEventArgumentsDto> CreateFetchFaultEventCommand()
        {
            return new FetchFaultEventCommand();
        }

        /// <summary>
        /// Returns a decorated command that supports native C# exception logging.
        /// </summary>
        /// <typeparam name="TResults">The type of object the sent command returns</typeparam>
        /// <typeparam name="TParameters">The type of object the sent command takes as a parameter</typeparam>
        /// <param name="cmd">The command to decorate with the exception handling code.</param>
        /// <param name="args">Parameters required to save an exception. (ie application name, machine name, platform name...)</param>
        /// <returns></returns>
        public ICommand<TResults, TParameters> DecorateCommand<TResults, TParameters>(ICommand<TResults, TParameters> cmd, SaveNativeArgumentsDto args)
        {
            return new FaultLoggingDecoratorCommand<TResults, TParameters>(cmd, args);
        }

        #region Reporting
        
        public ICommand<ApplicationSummaryResultsDto, ApplicationSummaryArgumentsDto> CreateApplicationSummaryCommand()
        {
            return new ApplicationSummaryCommand();
        }

        public ICommand<FaultSummaryResultsDto, FaultSummaryArgumentsDto> CreateFaultSummaryCommand()
        {
            return new FaultSummaryCommand();
        }

        public ICommand<FaultEventSummaryResultsDto, FaultEventSummaryArgumentsDto> CreateFaultEventSummaryCommand()
        {
            return new FaultEventSummaryCommand();
        }

        #endregion
    }
}
