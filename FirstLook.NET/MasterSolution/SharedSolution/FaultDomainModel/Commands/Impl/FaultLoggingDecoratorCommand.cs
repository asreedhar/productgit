﻿using System;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes;

namespace FirstLook.Fault.DomainModel.Commands.Impl
{
    /// <summary>
    /// This class will wrap the incoming command in a try catch so that exception logging can be performed on unhandled C# exceptions.
    /// </summary>
    /// <typeparam name="TResult">The result to return when executing the passed in command.</typeparam>
    /// <typeparam name="TArguments">The arguments to pass in when executing the command.</typeparam>
    class FaultLoggingDecoratorCommand<TResult,TArguments> : ICommand<TResult,TArguments>
     {
        private readonly ICommand<TResult, TArguments> _command;
        private readonly SaveNativeArgumentsDto _args;
        
        /// <summary>
        /// Constructor to save off the command and the args required to save an exception.
        /// </summary>
        /// <param name="command">Command to wrap in the try catch block.</param>
        /// <param name="args">Args to pass when creating the exception to save.</param>
        public FaultLoggingDecoratorCommand(ICommand<TResult, TArguments> command,SaveNativeArgumentsDto args)
        {
            _command = command;
            _args = args;
        }

        /// <summary>
        /// Execute the command that was initially passed in within a try catch block. If an unhandled
        /// exception is thrown, save it in the fault database.
        /// </summary>
        /// <param name="parameters">The parms passed in on the call, that need to be passed to the command.</param>
        /// <returns>The resulting object to return (based on the command being wrapped).</returns>
        public TResult Execute(TArguments parameters)
        {
            try
            {
                return _command.Execute(parameters);
            }
            catch(Exception e)
            {
                //Log using save native command and rethrow e
                ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
                ICommand<SaveNativeResultsDto, SaveNativeArgumentsDto> command = factory.CreateNativeSaveCommand();
                _args.Fault = e;
                command.Execute(_args);
                throw;
            }
        }
     }
}
