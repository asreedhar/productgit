﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Text;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes;
using FirstLook.Fault.DomainModel.Model;
using FirstLook.Fault.DomainModel.Repositories;
using FirstLook.Fault.DomainModel.Repositories.Entities;
using StackFrame=System.Diagnostics.StackFrame;

namespace FirstLook.Fault.DomainModel.Commands.Impl
{
    /// <summary>
    /// Using the passed in information construct a FaultEvent by using the FaultEventBuilder, then save the FaultEvent to the
    /// database.  This implementation is for native C# exception handling calls.
    /// </summary>
    public class NativeSaveCommand : ICommand<SaveNativeResultsDto, SaveNativeArgumentsDto>
    {
        private const string Platform = ".NET";
        private const string SLog = "FaultHandler";
        private const string SEvent = "FaultLoggingException";

        /// <summary>
        /// The Execute method will take in the required values to construct a fault event. It will then return the 
        /// saved fault event to the caller.
        /// </summary>
        /// <param name="args">The information required to save an exception</param>
        
        public SaveNativeResultsDto Execute(SaveNativeArgumentsDto args)
        {
            try
            {
                IResolver resolver = RegistryFactory.GetResolver();

                //Initialize instances of the faulteventbuilder and faultrepository to construct and save a fault event.
                var builder = resolver.Resolve<IFaultEventBuilder>();

                var faultRepository = resolver.Resolve<IFaultRepository>();

                //Call the builder to set the appropriate attributes of the fault event being built.
                builder.Application = args.Application;
                builder.Machine = args.Machine ?? Environment.MachineName;
                builder.Platform = args.Platform ?? Platform;
                builder.Timestamp = args.Timestamp;

                builder.BeginRequestorInformation();
                builder.User = args.User;
                builder.Path = args.Path;
                builder.Url = args.Url;
                builder.UserHostAddress = args.UserHostAddress;
                builder.EndRequestorInformation();

                //Construct a faultdata entry for each key/value data pair associated with the exception thrown.
                builder.BeginFaultData();

                foreach (DictionaryEntry entry in args.Fault.Data)
                {
                    builder.BeginFaultDataItem();
                    builder.Key = entry.Key.ToString();
                    builder.Value = entry.Value.ToString();
                    builder.EndFaultDataItem();
                }

                builder.EndFaultData();

                builder.BeginFault();

                //If an inner exception (causing fault) was specified, recursively go through them all
                if (args.Fault.InnerException != null)
                {
                    ProcessCauseFaults(args.Fault.InnerException, builder);
                }

                //Call BuildFault to process the stacktrace and stack trace elements associated with this exception.
                BuildFault(args.Fault, builder);

                builder.EndFault();

                //Build the fault event to be saved.
                IFaultEvent toSave = builder.ToFaultEvent();

                //Save the fault event, returning the saved fault event.
                return new SaveNativeResultsDto
                           {FaultEvent = (FaultEvent) ((FaultRepository) faultRepository).Save(toSave)};
            }
            catch(Exception e)
            {
                if (args != null)
                {
                    //If there was an exception thrown while saving an exception, write it to the windows event log.
                    string application = args.Application ?? "NativeApplication";

                    if (!EventLog.SourceExists(application))
                        EventLog.CreateEventSource(application, SLog);

                    EventLog log = new EventLog {Source = application};

                    log.WriteEntry(SEvent);

                    StringBuilder builder = new StringBuilder();

                    Exception fault = args.Fault;

                    while (fault != null)
                    {
                        builder.Append("ExceptionType: " + fault.GetType() ?? string.Empty + "\r");

                        builder.Append("Message: " + fault.Message ?? string.Empty + "\r");

                        builder.Append(fault.StackTrace);

                        fault = fault.InnerException;
                    }

                    builder.Append("\r\r\r" + "Fault Exception:" + e.StackTrace);

                    log.WriteEntry(builder.ToString(), EventLogEntryType.Error);
                }

                return new SaveNativeResultsDto();                
            }
        }

        /// <summary>
        /// Recursivley go through all of the InnerExceptions (causing faults) building the appropriate chain
        /// of fault events.
        /// </summary>
        /// <param name="innerException">The exception we are currently dealing with.</param>
        /// <param name="builder">The builder used so we can continue building onto our fault event.</param>
        public void ProcessCauseFaults(Exception innerException, IFaultEventBuilder builder)
        {
            builder.BeginCauseFault();

            //If there is no more inner exception that stop the recursion and return
            if (innerException.InnerException == null)
            {
                BuildFault(innerException, builder);
                builder.EndCauseFault();
                return;
            }

            //Process the inner fault before the current fault
            ProcessCauseFaults(innerException.InnerException, builder);
            BuildFault(innerException, builder);
            builder.EndCauseFault();
            return;

        }



        /// <summary>
        /// This will build on to the fault being constructed by the builder, incorporating the stack and the stack trace elements.
        /// </summary>
        /// <param name="fault">The fault being built from.</param>
        /// <param name="builder">The active builder.</param>
        private static void BuildFault(Exception fault, IFaultEventBuilder builder)
        {
            builder.ExceptionType = fault.GetType().ToString();
            builder.Message = fault.Message;

            if (fault.StackTrace != null)
            {
                builder.BeginStack();
                StackTrace stackTrace = new StackTrace(fault);
                if (stackTrace.FrameCount > 0)
                {
                    StackFrame[] frames = stackTrace.GetFrames();
                    if (frames != null)
                        foreach (StackFrame frame in frames)
                        {
                            builder.BeginStackFrame();
                            builder.ClassName = frame.GetMethod().DeclaringType.AssemblyQualifiedName;
                            builder.FileName = frame.GetFileName();
                            builder.MethodName = frame.GetMethod().ToString();
                            builder.LineNumber = frame.GetFileLineNumber();
                            builder.EndStackFrame();
                        }
                }
                builder.EndStack();
            }
        }


    
    }
}
