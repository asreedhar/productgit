﻿using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes;
using FirstLook.Fault.DomainModel.Model;

namespace FirstLook.Fault.DomainModel.Commands.Impl
{
    public class FetchFaultEventCommand : ICommand<FetchFaultEventResultsDto, FetchFaultEventArgumentsDto>
    {
        public FetchFaultEventResultsDto Execute(FetchFaultEventArgumentsDto parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            var repository = resolver.Resolve<IFaultRepository>();

            IFaultEvent obj = repository.Fetch(parameters.FaultEventId);

            return new FetchFaultEventResultsDto
            {
                Arguments = parameters,
                FaultEvent = Mapper.Map(obj)
            };
        }
    }
}
