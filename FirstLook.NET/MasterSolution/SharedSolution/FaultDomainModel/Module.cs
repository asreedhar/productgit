﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.Fault.DomainModel
{
    /// <summary>
    /// The intent of this class is to add pertinent interfaces and impelementing classes to the registry via the Configure method.
    /// </summary>
    public class Module : IModule
    {
        #region IModule Members

        /// <summary>
        /// Add pertinent interfaces and impelementing classes to the registry.
        /// </summary>
        public void Configure(IRegistry registry)
        {
            registry.Register<Commands.Module>();

            registry.Register<Repositories.Module>();
        }

        #endregion
    }
}