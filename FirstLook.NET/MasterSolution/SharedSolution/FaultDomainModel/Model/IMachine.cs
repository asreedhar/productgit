﻿namespace FirstLook.Fault.DomainModel.Model
{
    /// <summary>
    /// Define a Machine interface.
    /// </summary>  
    public interface IMachine
    {
        /// <summary>
        /// Name - The name of the machine.
        /// </summary>
        string Name { get; }
    }
}
