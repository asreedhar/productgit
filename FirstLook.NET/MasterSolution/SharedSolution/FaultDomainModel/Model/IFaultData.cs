﻿namespace FirstLook.Fault.DomainModel.Model
{
    /// <summary>
    /// Define a FaultData interface.  This interface defines a set of key/value pairs.  These pairs are derived from the Data attribute
    /// associated with a C# Exception.
    /// </summary>
    public interface IFaultData
    {
        /// <summary>
        /// Key - Key of key/value pair associated with the fault event. This is part of the Data attribute tied to an Exception.
        /// Value - Value of key/value pair associated with the fault event. This is part of the Data attribute tied to an Exception.
        /// </summary>
        string Key { get; }

        string Value { get; }
    }
}