﻿namespace FirstLook.Fault.DomainModel.Model
{
    /// <summary>
    /// Define a RequestInformation interface.
    /// </summary>
    public interface IRequestInformation
    {
        /// <summary>
        /// Path - The Path associated with the request.
        /// </summary>
        string Path { get; }

        /// <summary>
        /// Url - The Url associated with the request.
        /// </summary>
        string Url { get; }

        /// <summary>
        /// User - The User associated with the request.
        /// </summary>
        string User { get; }

        /// <summary>
        /// UrlHostAddress - The UrlHostAddress associated with the request.
        /// </summary>
        string UserHostAddress { get; }

    }
}