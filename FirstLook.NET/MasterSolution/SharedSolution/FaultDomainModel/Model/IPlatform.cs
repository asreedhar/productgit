﻿namespace FirstLook.Fault.DomainModel.Model
{
    /// <summary>
    /// Define a Platform interface.
    /// </summary>  
    public interface IPlatform
    {
        /// <summary>
        /// Name - The of the platform.
        /// </summary>
        string Name { get; }

    }
}