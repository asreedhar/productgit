﻿using System.Collections.Generic;
using FirstLook.Common.Core.Hashing;

namespace FirstLook.Fault.DomainModel.Model
{
    /// <summary>
    /// Define a Stack interface.
    /// </summary>
    public interface IStack : IHashable
    {
        /// <summary>
        /// Frames - Each Stack can contain multiple stack frames.
        /// </summary>       
        IList<IStackFrame> Frames { get; }
    }
}