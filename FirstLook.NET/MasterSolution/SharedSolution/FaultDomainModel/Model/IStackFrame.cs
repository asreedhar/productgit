﻿using FirstLook.Common.Core.Hashing;

namespace FirstLook.Fault.DomainModel.Model
{
    /// <summary>
    /// Define a StackFrame interface.
    /// </summary>
    public interface IStackFrame : IHashable
    {
        /// <summary>
        /// ClassName - Class name associated with the generated exception.
        /// </summary>
        string ClassName { get; }

        /// <summary>
        /// FileName - File name associated with the generated exception.
        /// </summary>
        string FileName { get; }

        /// <summary>
        /// IsNativeMethod - Is this a native method?
        /// </summary>
        bool IsNativeMethod { get; }

        /// <summary>
        /// IsUnknownSource - Is the source known?
        /// </summary>
        bool IsUnknownSource { get; }

        /// <summary>
        /// LineNumber - Line number of exception.
        /// </summary>
        int? LineNumber { get; }

        /// <summary>
        /// MethodName - Method name the exception was thrown from.
        /// </summary>
        string MethodName { get; }

    }
}