﻿using FirstLook.Common.Core.Hashing;

namespace FirstLook.Fault.DomainModel.Model
{
    /// <summary>
    /// Define a Fault interface.
    /// </summary>
    public interface IFault : IHashable
    {
        /// <summary>
        /// ExceptionType - The type of exception this fault generated.
        /// </summary>
        string ExceptionType { get; }

        /// <summary>
        /// Message - Message associated with fault.
        /// </summary>
        string Message { get; }

        /// <summary>
        /// Stack - The referenced stack trace for the exception.
        /// </summary>
        IStack Stack { get; }

        /// <summary>
        /// Cause - The fault that caused this fault (Inner exception), if one exists.
        /// </summary>
        IFault Cause { get; }
    }
}