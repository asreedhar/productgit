﻿namespace FirstLook.Fault.DomainModel.Model.Reporting
{
    public interface IApplicationSummary
    {
        IApplication Application { get; }

        int NumberOfFaults { get; }

        int NumberOfFaultEvents { get; }
    }
}
