﻿using System;

namespace FirstLook.Fault.DomainModel.Model.Reporting
{
    public interface IFaultSummary
    {
        IApplication Application { get; }

        string ExceptionType { get; }

        string Message { get; }

        int FaultId { get; }

        DateTime MinFaultTime { get; }

        DateTime MaxFaultTime { get; }

        int MaxFaultEventId { get; }

        int NumFaultEvent { get; }
    }
}
