﻿using System;

namespace FirstLook.Fault.DomainModel.Model.Reporting
{
    public interface IFaultEventSummary
    {
        IApplication Application { get; }

        IMachine Machine { get; }

        IPlatform Platform { get; }

        DateTime FaultTime { get; }

        int FaultId { get; } // i hate exposing this property (simon)

        int Id { get; }

        long Rank { get; }
    }
}
