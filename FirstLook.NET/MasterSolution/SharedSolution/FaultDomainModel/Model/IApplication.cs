﻿namespace FirstLook.Fault.DomainModel.Model
{
    /// <summary>
    /// Define an Application interface.
    /// </summary>  
    public interface IApplication
    {
        /// <summary>
        /// Name - The name of the application.
        /// </summary>
        string Name { get; }     
    }
}