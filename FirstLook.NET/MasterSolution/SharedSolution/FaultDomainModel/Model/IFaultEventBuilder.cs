﻿using System;

namespace FirstLook.Fault.DomainModel.Model
{
    /// <summary>
    /// Defines the FaultEventBuilder interface. 
    /// </summary>
    public interface IFaultEventBuilder
    {

        #region Fault Event

        /// <summary>
        /// Application - The application generating this fault event.
        /// </summary>
        string Application { get; set; }

        /// <summary>
        /// Machine - The machine on which the fault was generated.
        /// </summary>
        string Machine { get; set; }

        /// <summary>
        /// Platform - The platform on which the fault was generated.
        /// </summary>
        string Platform { get; set; }

        /// <summary>
        /// Timestamp - The timestamp when the fault was generated.
        /// </summary>
        DateTime Timestamp { get; set; }

        #endregion

        #region Requestor Information

        /// <summary>
        /// BeginRequestorInformation() - A method signifying the start of a request to supply requestor related information.
        ///                             - One per request.
        /// </summary>
        void BeginRequestorInformation();

        /// <summary>
        /// Path - The Path associated with the request.
        /// </summary>
        string Path { get; set; }

        /// <summary>
        /// Url - The Url associated with the request.
        /// </summary>
        string Url { get; set;}

        /// <summary>
        /// User - The User associated with the request.
        /// </summary>
        string User { get; set; }

        /// <summary>
        /// UrlHostAddress - The UrlHostAddress associated with the request.
        /// </summary>
        string UserHostAddress { get; set; }

        /// <summary>
        /// EndRequestorInformation() - A method signifying the end of a request to supply requestor related information.
        /// </summary>
        void EndRequestorInformation();

        #endregion

        #region Fault Data

        /// <summary>
        /// BeginFaultData() - A method signifying the start of a request to supply information specific to fault data (key/value pairs). 
        ///                  - Can be several per request (ie multiple key value pairs providing information about the fault).
        /// </summary>
        void BeginFaultData();

        void BeginFaultDataItem();

        /// <summary>
        /// Key,Value - Key/value pairs of data associated with the fault event.
        /// </summary>
        string Key { get; set; }
        string Value { get; set; }

        void EndFaultDataItem();

        /// <summary>
        /// EndFaultData() - A method signifying the end of a request to supply information specific to fault data (key/value pairs).
        /// </summary>
        void EndFaultData();

        #endregion

        #region Fault

        /// <summary>
        /// BeginFault() - A method signifying the start of a request to supply information specific to the fault. 
        ///              - Can be several per request (ie specifying the fault that caused this fault (this can be chained)).
        /// </summary>
        void BeginFault();

        /// <summary>
        /// ExceptionType - The type of exception this fault generated.
        /// </summary>
        string ExceptionType { get; set; }

        /// <summary>
        /// Message - A message describing the fault.
        /// </summary>
        string Message { get; set; }

        /// <summary>
        /// EndFault() - A method signifying the end of a request to supply information specific to the fault.
        /// </summary>
        void EndFault();

        #endregion

        #region Cause Fault
        
        /// <summary>
        /// BeginCauseFault() - Indicate the start of a causing fault. Any fault can be the result of a causing fault.
        /// </summary>
        void BeginCauseFault();

        /// <summary>
        /// EndCauseFault() - Indicate the end of a causing fault. Any fault can be the result of a causing fault.
        /// </summary>
        void EndCauseFault();

        #endregion

        #region Stack

        /// <summary>
        /// BeginStack() - A method signifying the start of a request to supply information specific to the stack trace associated with the fault. 
        ///              - Can be several per request, as each causing fault can have it's own stack trace.
        /// </summary>
        void BeginStack();

        /// <summary>
        /// EndStack() - A method signifying the end of a request to supply information specific to the stack.
        /// </summary>
        void EndStack();

        #endregion

        #region Stack Frame

        /// <summary>
        /// BeginStackFrame() - A method signifying the start of a request to supply information specific to the stack frames associated with a stack. 
        ///                   - Can be several per request, as there are usually multiple frames per stack, and possibly multiple stacks per
        ///                     request if each causing request supplies a stack trace.
        /// </summary>
        void BeginStackFrame();

        /// <summary>
        /// ClassName - Class name associated with the generated exception.
        /// </summary>
        string ClassName { get; set; }

        /// <summary>
        /// FileName - File name associated with the generated exception.
        /// </summary>
        string FileName { get; set; }

        /// <summary>
        /// IsNativeMethod - Is this a native method?
        /// </summary>
        bool IsNativeMethod { get; set; }

        /// <summary>
        /// IsUnknownSource - Is the source known?
        /// </summary>
        bool IsUnknownSource { get; set; }

        /// <summary>
        /// LineNumber - Line number of exception.
        /// </summary>
        int? LineNumber { get; set; }

        /// <summary>
        /// MethodName - Method name the exception was thrown from.
        /// </summary>
        string MethodName { get; set; }

        /// <summary>
        /// EndStackFrame() - A method signifying the end of a request to supply information specific to the stack frames associated with a stack trace.
        /// </summary>
        void EndStackFrame();

        #endregion

        /// <summary>
        /// ToFaultEvent() - Will convert the information provided via the current request to a FaultEvent object.
        /// </summary>
        /// <returns>A FaultEvent object, to be saved in the database.</returns>
        IFaultEvent ToFaultEvent();
    }
}