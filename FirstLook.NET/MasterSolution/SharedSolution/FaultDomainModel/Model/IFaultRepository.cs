﻿using System.Collections.Generic;
using FirstLook.Fault.DomainModel.Model.Reporting;

namespace FirstLook.Fault.DomainModel.Model
{
    /// <summary>
    /// Define a fault repository interface.  Allows you to save a fault event to the configured database.
    /// </summary>
    public interface IFaultRepository
    {
        IFaultEvent Fetch(int id);

        IFaultEvent Save(IFaultEvent faultEvent);

        #region Reporting

        IList<IApplicationSummary> ApplicationSummary();

        Page<IFaultSummary> FaultSummary(string application, PageArguments pagination);

        IFaultEventSummary FaultEventSummaryById(int faultId, int faultEventId);

        IFaultEventSummary FaultEventSummaryByRank(int faultId, int rank);

        #endregion
    }
}
