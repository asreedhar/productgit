﻿using System;
using System.Collections.Generic;

namespace FirstLook.Fault.DomainModel.Model
{
    /// <summary>
    /// An interface describing the data specified in a fault event.
    /// </summary>
    public interface IFaultEvent
    {
        /// <summary>
        /// Id - A unique value per FaultEvent
        /// </summary>
        int Id { get; }

        /// <summary>
        /// IsNew - A boolean value to say whether or not a FaultEvent has already been saved.
        /// </summary>
        bool IsNew { get; }

        /// <summary>
        /// Application - Refers to an Application object tied to the fault event.
        /// </summary>
        IApplication Application { get; }

        /// <summary>
        /// Machine - Refers to the Machine on which the fault event was generated.
        /// </summary>
        IMachine Machine { get; }

        /// <summary>
        /// Platform - Refers to the Platform on which the fault event was generated.
        /// </summary>
        IPlatform Platform { get; }

        /// <summary>
        /// Fault - A reference to the fault object itself.
        /// </summary>
        IFault Fault { get; }

        /// <summary>
        /// Timestamp - The timestamp the exception was generated.
        /// </summary>
        DateTime Timestamp { get; }

        /// <summary>
        /// Data - Each FaultEvent can be associated with mutiple pieces of data that come in key/value pairs.
        /// </summary>
        IList<IFaultData> Data { get; }

        /// <summary>
        ///  RequestInfo - Each generated FaultEvent is associated with a requestor.
        /// </summary>
        IRequestInformation RequestInfo { get; }
    }
}