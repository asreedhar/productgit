﻿using System;
using System.Web.Management;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes;
using System.Diagnostics;
using ICommandFactory=FirstLook.Fault.DomainModel.Commands.ICommandFactory;


namespace FirstLook.Fault.DomainModel.Utilities
{
    /// <summary>
    /// Impelement a Health Monitor Provider.  Whenever a WebBaseErrorEvent is generated with an exception this provider
    /// will capture the exception and some other relevant data for the event and save it in the fault database.
    /// </summary>
    public class FaultDatabaseWebEventProvider : BufferedWebEventProvider
    {

        private const string Platform = ".NET";
        private const string SLog = "FaultHandler";
        private const string SEvent = "FaultLoggingException";
        
        /// <summary>
        /// Process the incoming event if it is a WebBaseErrorEvent, in which case there should be a valid exception
        /// to store. If this is the case grab relevant information required to store the exception.
        /// </summary>
        /// <param name="eventRaised">The raised event.</param>
        public override void ProcessEvent(WebBaseEvent eventRaised)
        {

            string path = null;
            string url = null; 
            string user = null; 
            string userHostAddress = null;
 

            try
            {
                //Check to see if this is an instance of a WebBaseErrorEvent
                if (eventRaised is WebBaseErrorEvent)
                {
                    
                    //If so there should be a valid exception to store in the fault database.
                    if ((eventRaised as WebBaseErrorEvent).ErrorException != null)
                    {
                        //Get the required elements to save a fault.
                        Exception e = (eventRaised as WebBaseErrorEvent).ErrorException;
                        string application = WebBaseEvent.ApplicationInformation.ApplicationVirtualPath;
                        string machine = WebBaseEvent.ApplicationInformation.MachineName;

                        if ((eventRaised as WebRequestErrorEvent) != null)
                        {
                            WebRequestErrorEvent raisedEvent = eventRaised as WebRequestErrorEvent;


                            if (raisedEvent.RequestInformation != null)
                            {
                                path = raisedEvent.RequestInformation.RequestPath;
                                url = raisedEvent.RequestInformation.RequestUrl;
                                userHostAddress = raisedEvent.RequestInformation.UserHostAddress;
                                user = raisedEvent.RequestInformation.ThreadAccountName;
                            }
                        }

                        //Create a native command to be invoked to save the exception, providing the obtained
                        //values.
                        ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
                        ICommand<SaveNativeResultsDto, SaveNativeArgumentsDto> command = factory.CreateNativeSaveCommand();
                        command.Execute(new SaveNativeArgumentsDto
                        {
                            Fault = e,
                            Application = application,
                            Machine = machine,
                            Path = path,
                            Platform = Platform,
                            Timestamp = eventRaised.EventTime,
                            Url = url,
                            User = user,
                            UserHostAddress = userHostAddress
                        });

                    }
                }
            }
            catch
            {
                //If there was an exception thrown while saving an exception, write it to the windows event log.
                string application = WebBaseEvent.ApplicationInformation.ApplicationVirtualPath;
                if (!EventLog.SourceExists(application))
                    EventLog.CreateEventSource(application, SLog);

                EventLog log = new EventLog { Source = application };
                log.WriteEntry(SEvent);                
                if (eventRaised is WebBaseErrorEvent)
                {
                    Exception e = (eventRaised as WebBaseErrorEvent).ErrorException;
                    if (e != null)
                        log.WriteEntry(e.StackTrace, EventLogEntryType.Warning);
                }else
                {
                    log.WriteEntry(eventRaised.Message, EventLogEntryType.Warning);
                }

            }
            
        }

        /// <summary>
        /// Processes the messages that have been buffered.  It is called by ASP.NET when the flushing of 
        /// the buffer is required according to the parameters defined in the bufferModes element of the 
        /// healthMonitoring configuration section.
        /// </summary>
        /// <param name="flushInfo">An object containing information such as a list of WebBaseErrorEvents</param>
        public override void ProcessEventFlush(WebEventBufferFlushInfo flushInfo)
        {
            string path = string.Empty;
            string url = string.Empty;
            string user = string.Empty;
            string userHostAddress = string.Empty;


            //Cycle through each error event, saving it where applicable.
            foreach (WebBaseEvent eventRaised in flushInfo.Events)
            {
                try
                {
                    //Check to see if this is an instance of a WebBaseErrorEvent
                    if (eventRaised is WebBaseErrorEvent)
                    {
                        //If so there should be a valid exception to store in the fault database.
                        if ((eventRaised as WebBaseErrorEvent).ErrorException != null)
                        {
                            //Get the required elements to save a fault.
                            Exception e = (eventRaised as WebBaseErrorEvent).ErrorException;
                            string application = WebBaseEvent.ApplicationInformation.ApplicationVirtualPath;
                            string machine = WebBaseEvent.ApplicationInformation.MachineName;

                            if ((eventRaised as WebRequestErrorEvent) != null)
                            {
                                WebRequestErrorEvent raisedEvent = eventRaised as WebRequestErrorEvent;

                                if (raisedEvent.RequestInformation != null)
                                {
                                    path = raisedEvent.RequestInformation.RequestPath;
                                    url = raisedEvent.RequestInformation.RequestUrl;
                                    userHostAddress = raisedEvent.RequestInformation.UserHostAddress;
                                    user = raisedEvent.RequestInformation.ThreadAccountName;
                                }
                            }

                            //Create a native command to be invoked to save the exception, providing the obtained
                            //values.
                            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
                            ICommand<SaveNativeResultsDto, SaveNativeArgumentsDto> command =
                                factory.CreateNativeSaveCommand();

                            command.Execute(new SaveNativeArgumentsDto
                                                {
                                                    Fault = e,
                                                    Application = application,
                                                    Machine = machine,
                                                    Path = path,
                                                    Platform = Platform,
                                                    Timestamp = eventRaised.EventTime,
                                                    Url = url,
                                                    User = user,
                                                    UserHostAddress = userHostAddress
                                                });

                        }
                    }
                }
                catch
                {
                    //If there was an exception thrown while saving an exception, write it to the windows event log.
                    string application = WebBaseEvent.ApplicationInformation.ApplicationVirtualPath;
                    if (!EventLog.SourceExists(application))
                        EventLog.CreateEventSource(application, SLog);

                    EventLog log = new EventLog { Source = application };
                    log.WriteEntry(SEvent);
                    if (eventRaised is WebBaseErrorEvent)
                    {
                        Exception e = (eventRaised as WebBaseErrorEvent).ErrorException;
                        if (e != null)
                            log.WriteEntry(e.StackTrace, EventLogEntryType.Warning);
                    }
                    else
                    {
                        log.WriteEntry(eventRaised.Message, EventLogEntryType.Warning);
                    }
                }
            }
        }

        /// <summary>
        /// Provides standard shutdown.
        /// </summary>
        public override void Shutdown()
        {
            // Flush the buffer, if needed.
            Flush();    

        }

    }

}
