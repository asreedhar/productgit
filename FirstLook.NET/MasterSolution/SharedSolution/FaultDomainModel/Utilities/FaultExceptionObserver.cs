﻿using System;
using FirstLook.Common.Core.Command;
using FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Fault.DomainModel.Utilities
{
    public class FaultExceptionObserver : ICommandObserver
    {

        private readonly string _application;

        public FaultExceptionObserver(string application)
        {
            _application = application;
        }

        protected FaultExceptionObserver()
        {
        }

        #region ICommandListener Members

        public virtual void OnInvoke(CommandEventArgs e)
        {
            throw new NotImplementedException();
        }

        public virtual void OnInvokeComplete(CommandEventArgs e)
        {
            throw new NotImplementedException();
        }

        public virtual void OnInvokeException(CommandExceptionEventArgs e)
        {

             SaveNativeArgumentsDto saveNativeArgs = new SaveNativeArgumentsDto
            {
                Application = _application,
                Fault = e.Exception,                    
                Machine = Environment.MachineName,
                Path = string.Empty,
                Platform = ".NET",
                Timestamp = DateTime.Now,
                Url = string.Empty,
                User = string.Empty,
                UserHostAddress = string.Empty
            };

            Commands.ICommandFactory factory = RegistryFactory.GetResolver().Resolve<Commands.ICommandFactory>();
            ICommand<SaveNativeResultsDto, SaveNativeArgumentsDto> command = factory.CreateNativeSaveCommand();
            command.Execute(saveNativeArgs);
            
        }

        #endregion
    }
}
