﻿using System;
using FirstLook.Common.Core.Command;
using FirstLook.Fault.DomainModel.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Registry;
using System.Web;

namespace FirstLook.Fault.DomainModel.Utilities
{
    public class FaultWebExceptionObserver : FaultExceptionObserver
    {
        #region ICommandListener Members

        public override void OnInvokeException(CommandExceptionEventArgs e)
        {
            SaveException(e.Exception);
        }

        public static void SaveException(Exception e)
        {
            HttpContext httpContext = HttpContext.Current;
            HttpRequest httpRequest = httpContext.Request;

            var saveNativeArgs = new SaveNativeArgumentsDto
            {
                Application = httpRequest.ApplicationPath,
                Fault = e,
                Machine = Environment.MachineName,
                Path = httpRequest.Path,
                Platform = ".NET",
                Timestamp = DateTime.Now,
                Url = httpRequest.Url.AbsolutePath,
                User = httpContext.User.Identity.Name,
                UserHostAddress = httpRequest.UserHostAddress
            };

            var factory = RegistryFactory.GetResolver().Resolve<Commands.ICommandFactory>();

            var command = factory.CreateNativeSaveCommand();
            
            var results = command.Execute(saveNativeArgs);
            
            if (results.FaultEvent != null)
            {
                e.Data.Add("FaultEventId", results.FaultEvent.Id);
            }
            
        }

        #endregion
    }
}
