namespace FirstLook.Common.Core.Xml
{
    public interface IRenderXml
    {
        string AsXml();
    }
}