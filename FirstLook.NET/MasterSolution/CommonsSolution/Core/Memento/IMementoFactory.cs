namespace FirstLook.Common.Core.Memento
{
    public interface IMementoFactory<T>
    {
        IMemento<T> For(T item);
    }
}