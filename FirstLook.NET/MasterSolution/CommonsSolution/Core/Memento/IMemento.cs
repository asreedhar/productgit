using System;

namespace FirstLook.Common.Core.Memento
{
    public interface IMemento<TEntity> : IEquatable<TEntity>
    {
    }
}