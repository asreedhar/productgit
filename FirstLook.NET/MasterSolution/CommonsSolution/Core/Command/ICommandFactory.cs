namespace FirstLook.Common.Core.Command
{
    public interface ICommandFactory
    {
        ICommandTransaction CreateTransactionCommand();
    }
}