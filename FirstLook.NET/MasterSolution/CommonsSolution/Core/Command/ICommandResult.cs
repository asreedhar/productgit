namespace FirstLook.Common.Core.Command
{
    public interface ICommandResult<TResult>
    {
        TResult Result { get; }
    }
}