﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using FirstLook.Common.Core.Diagnostics.Counters;

namespace FirstLook.Common.Core.Diagnostics
{
    class PerformanceCounterFactory
    {
        public static IPerformanceCounter Create(Configuration.PerformanceMonitoringMappingElement mapping)
        {
            IPerformanceCounter counter = null;

            string name = string.Format("{0}{1}", mapping.Process.Split('.').Last(), mapping.Type);

            switch (mapping.Type) {

                case PerformanceCounterType.AverageOperationTime:
                    counter = new AverageOperationTimePerformanceCounter(name, mapping.Type, mapping.Category);
                    break;
                case PerformanceCounterType.OperationsPerSecond:
                    counter = new OperationsPerSecondPerformanceCounter(name, mapping.Type, mapping.Category);
                    break;
                case PerformanceCounterType.OperationSuccessRatio:
                    counter = new OperationSuccessRatioPerformanceCounter(name, mapping.Type, mapping.Category);
                    break;
            }

            return counter;
        }

    }
    
    public enum PerformanceCounterCategory
    {
        FirstLookCommon = 0     //,
        //FirstLookOther = 1
    }

    public enum PerformanceCounterType
    {
        OperationsPerSecond = 0,
        AverageOperationTime = 1,
        OperationSuccessRatio = 2  

    }
}
