﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Diagnostics.Counters;


namespace FirstLook.Common.Core.Diagnostics
{
    public class PerformanceMonitoringObserver : ICommandObserver
    {
        
        public PerformanceMonitoringObserver()
        {
            // empty constructor
        }

        public void OnInvoke(CommandEventArgs e)
        {
            foreach (IPerformanceCounter c in PerformanceCounterRegistry.Fetch(e.CommandType))
            {
                var counter = (IPerformanceCounter)c;
                switch (counter.CounterType)
                {
                    case PerformanceCounterType.AverageOperationTime:
                        ((AverageOperationTimePerformanceCounter)counter).Stopwatch.Start();
                        break;
                }
            }
        }

        public void OnInvokeComplete(CommandEventArgs e)
        {
            foreach (IPerformanceCounter c in PerformanceCounterRegistry.Fetch(e.CommandType))
            {
                var counter = (IPerformanceCounter)c;
                switch (counter.CounterType)
                {
                    case PerformanceCounterType.AverageOperationTime:
                    case PerformanceCounterType.OperationsPerSecond:
                        counter.Increment();
                        break;
                    case PerformanceCounterType.OperationSuccessRatio:
                        ((OperationSuccessRatioPerformanceCounter)counter).OperationSuccessful = true;
                        counter.Increment();
                        break;
                }
            }

            
        }

        public void OnInvokeException(CommandExceptionEventArgs e)
        {
            foreach (IPerformanceCounter c in PerformanceCounterRegistry.Fetch(e.CommandType))
            {
                var counter = (IPerformanceCounter)c;
                switch (counter.CounterType)
                {
                    case PerformanceCounterType.AverageOperationTime: 
                    case PerformanceCounterType.OperationSuccessRatio:
                    case PerformanceCounterType.OperationsPerSecond:
                        counter.Increment();
                        break;
                }
            }
        }

    }
}
