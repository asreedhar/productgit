﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;


namespace FirstLook.Common.Core.Diagnostics.Configuration
{
    public class PerformanceMonitoringMappingElement : ConfigurationElement
    {
        private const string ProcessType = "processType";
        private const string CounterCategory = "counterCategory";
        private const string CounterType = "counterType";

        [ConfigurationProperty(ProcessType, IsRequired = true)]
        public string Process
        {
            get
            {
                return (string)this[ProcessType];
            }
            set
            {
                this[ProcessType] = value;
            }
        }

        [ConfigurationProperty(CounterCategory, IsRequired = true)]
        public FirstLook.Common.Core.Diagnostics.PerformanceCounterCategory Category
        {
            get
            {
                return (FirstLook.Common.Core.Diagnostics.PerformanceCounterCategory)this[CounterCategory];
            }
            set
            {
                this[CounterCategory] = value;
            }
        }

        [ConfigurationProperty(CounterType, IsRequired = true)]
        public FirstLook.Common.Core.Diagnostics.PerformanceCounterType Type
        {
            get
            {
                return (FirstLook.Common.Core.Diagnostics.PerformanceCounterType)this[CounterType];
            }
            set
            {
                this[CounterType] = value;
            }
        }
    }
}
