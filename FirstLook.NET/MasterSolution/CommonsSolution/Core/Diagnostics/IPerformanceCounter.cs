﻿using System;
using System.Collections.Generic;
using System.ComponentModel;


namespace FirstLook.Common.Core.Diagnostics
{
    public interface IPerformanceCounter : IDisposable 
    {
        long Increment();

        long IncrementBy(long value);

        PerformanceCounterType CounterType { get; }
    }
}


