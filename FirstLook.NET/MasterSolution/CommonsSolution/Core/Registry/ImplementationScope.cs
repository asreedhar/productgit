namespace FirstLook.Common.Core.Registry
{
    public enum ImplementationScope
    {
        Isolated,
        Shared
    }
}