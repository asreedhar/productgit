namespace FirstLook.Common.Core.Registry
{
    public interface IModule
    {
        void Configure(IRegistry registry);
    }
}
