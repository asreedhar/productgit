using System;

namespace FirstLook.Common.Core.Registry
{
    public interface IResolver : IDisposable
    {
        TContract Resolve<TContract>();
    }
}