using Autofac;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Logging;

namespace FirstLook.Common.Core
{
    public class CoreModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => new DotnetMemoryCacheWrapper()).As(typeof(ICache), typeof(IMemoryCache)).SingleInstance();
            builder.Register(c => new LoggerFactory()).As<ILoggerFactory>().SingleInstance();
            builder.RegisterType<DbConnectionFactory>().As<IDbConnectionFactory>().SingleInstance();
        }
    }
}