namespace FirstLook.Common.Core.Membership
{
    public interface IMemberContext
    {
        IMember Current { get; }
    }
}