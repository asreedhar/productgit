﻿using System;

namespace FirstLook.Common.Core.Validation.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class ConstrainedAttribute : Attribute
    {
    }
}