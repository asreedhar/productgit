﻿namespace FirstLook.Common.Core.Validation
{
    public interface IConstrained
    {
        bool IsValid { get; }

        IConstraintViolations ConstraintViolations { get; }
    }
}