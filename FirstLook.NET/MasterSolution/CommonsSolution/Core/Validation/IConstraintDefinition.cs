﻿using System.Collections.Generic;

namespace FirstLook.Common.Core.Validation
{
    public interface IConstraintDefinition<T>
    {
        IList<IConstraint<T>> ObjectConstraints { get; }

        IDictionary<string, IList<IConstraint<T>>> PropertyConstraints { get; }
    }
}