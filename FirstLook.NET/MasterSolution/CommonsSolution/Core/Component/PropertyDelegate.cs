﻿namespace FirstLook.Common.Core.Component
{
    public delegate TProperty PropertyDelegate<TClass,TProperty>(TClass value);
}