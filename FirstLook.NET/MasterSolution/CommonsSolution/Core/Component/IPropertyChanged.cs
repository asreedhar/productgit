﻿namespace FirstLook.Common.Core.Component
{
    public interface IPropertyChanged
    {
        void OnUnknownPropertyChanged();
    }
}