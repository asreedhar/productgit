﻿using System;

namespace FirstLook.Common.Core.Component
{
    public class ConcurrentModificationException : Exception
    {
        public ConcurrentModificationException(string message) : base(message) { }
    }
}