﻿using System;

namespace FirstLook.Common.Core.Component
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class BoundAttribute : Attribute
    {
    }
}