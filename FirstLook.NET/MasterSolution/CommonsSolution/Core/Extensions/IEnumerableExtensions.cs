﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Common.Core.Extensions
{
    public static class EnumerableExtensions
    {
        public static string ToDelimitedString<T>(this IEnumerable<T> enumerable, string delimiter)
        {
            var items = enumerable as T[] ?? enumerable.ToArray();
            if (items.Count() == 0)
                return String.Empty;

            var sb = new StringBuilder();

            foreach (var item in items)
            {
                sb.Append(item);
                sb.Append(delimiter);
            }

            // Remove the trailing delimiter.
            if( sb.Length > 0 )
            {
                sb.Length = sb.Length - delimiter.Length;
            }

            return sb.ToString();
        }
    }
}
