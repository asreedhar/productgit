﻿using System;
using FirstLook.Common.Core.Extensions;

namespace FirstLook.Common.Core
{
    public static class TypeUtility
    {
        public static string Name(Type t, string postfix)
        {
            // alphanumeric, underscore, hyphens, length 1-80
            var fullName = t.FullName.Replace(".", "_");

            if (!string.IsNullOrEmpty(postfix))
                fullName += "_" + postfix;

            return fullName.Right(80);
        }

        public static string Name(string name, string postfix, char postfixSeperator)
        {
            if (!string.IsNullOrEmpty(postfix))
                name += postfixSeperator + postfix;

            return name.Right(80);
        }

        public static string Name(string name, string postfix)
        {
            return Name(name, postfix, '_');
        }

    }
}
