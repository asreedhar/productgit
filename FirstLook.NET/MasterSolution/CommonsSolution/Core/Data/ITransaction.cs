using System;

namespace FirstLook.Common.Core.Data
{
    public interface ITransaction : IDisposable
    {
        void Commit();

        void Rollback();
    }
}