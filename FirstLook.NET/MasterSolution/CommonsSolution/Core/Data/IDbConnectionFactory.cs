﻿using System.Data;

namespace FirstLook.Common.Core.Data
{
    public interface IDbConnectionFactory
    {
        IDbConnection GetConnection(string connectionName);
    }
}