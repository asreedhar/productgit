using System;
using System.Collections;

namespace FirstLook.Common.Core.Data
{
    public interface IDataSession : IDisposable
    {
        IDataConnection OpenConnection();

        IDataConnection Connection { get; }

        void Close();

        IDictionary Items { get; }
    }
}