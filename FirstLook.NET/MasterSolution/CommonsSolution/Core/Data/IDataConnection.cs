﻿using System.Data;

namespace FirstLook.Common.Core.Data
{
    public interface IDataConnection : ITransactionFactory
    {
        IDbCommand CreateCommand();

        void Close();
    }
}
