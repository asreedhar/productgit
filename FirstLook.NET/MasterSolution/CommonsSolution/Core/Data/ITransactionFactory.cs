using System;

namespace FirstLook.Common.Core.Data
{
    public interface ITransactionFactory : IDisposable
    {
        ITransaction BeginTransaction();
    }
}