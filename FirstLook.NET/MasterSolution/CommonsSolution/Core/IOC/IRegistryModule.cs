using Autofac;
namespace FirstLook.Common.Core.IOC
{
    public interface IRegistryModule
    {
        void Register(ContainerBuilder builder);
    }
}