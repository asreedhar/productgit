namespace FirstLook.Common.Core.PhotoServices
{
    public interface IPhotoServicesError
    {
        string Code { get; }
        string Message { get; }
    }
}