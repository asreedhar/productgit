﻿namespace FirstLook.Common.Core.PhotoServices
{
    public interface IVehiclePhotos
    {
        string Vin { get; }
        string[] PhotoUrls { get; }
    }
}