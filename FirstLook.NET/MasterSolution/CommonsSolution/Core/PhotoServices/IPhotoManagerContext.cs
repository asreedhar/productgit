namespace FirstLook.Common.Core.PhotoServices
{
    public interface IPhotoManagerContext
    {
        string Code { get; }
        string Name { get; }
    }
}