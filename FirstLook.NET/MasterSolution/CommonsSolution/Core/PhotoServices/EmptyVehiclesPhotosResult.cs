﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.PhotoServices.WebService.V1.Model;

namespace FirstLook.Common.Core.PhotoServices
{
    public class EmptyVehiclesPhotosResult : IVehiclesPhotosResult
    {
        private EmptyVehiclesPhotosResult()
        {
            PhotosByVin = new Dictionary<string, IVehiclePhotos>();
        }

        public EmptyVehiclesPhotosResult(IEnumerable<Exception> capturedExceptions)
            : this()
        {
            Errors =
                capturedExceptions.Select(
                    e => (IPhotoServicesError) new Error
                                                   {
                                                       Code = e.GetType().ToString(),
                                                       Message = e.Message
                                                   }).
                    ToArray();
        }

        public EmptyVehiclesPhotosResult(IEnumerable<IPhotoServicesError> errors)
            : this()
        {
            Errors = errors.ToArray();
        }

        public IDictionary<string, IVehiclePhotos> PhotosByVin { get; private set; }

        public ICollection<IPhotoServicesError> Errors { get; private set; }
    }
}