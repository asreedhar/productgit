﻿using System;

namespace FirstLook.Common.Core.PhotoServices
{
    public class ThumbnailResult : IPhotoThumbnailResult
    {
        public string Url { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }

        public ThumbnailResult(string url, int width, int height)
        {
            ValidateUrl(url);
            ValidateDimension(width);
            ValidateDimension(height);

            Url = url;
            Width = width;
            Height = height;
        }

        private static void ValidateDimension(int x)
        {
            if(x < 0)
                throw new ArgumentException("Dimension should be a positive number.");
        }

        private static void ValidateUrl(string url)
        {
            if(string.IsNullOrWhiteSpace(url))
                throw new ArgumentNullException("url");
        }
    }
}