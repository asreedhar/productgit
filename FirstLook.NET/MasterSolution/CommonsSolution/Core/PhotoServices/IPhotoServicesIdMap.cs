namespace FirstLook.Common.Core.PhotoServices
{
    public interface IPhotoServicesIdMap
    {
        string MapBusinessUnitIdToBusinessUnitCode(int businessUnitId);

        BusinessUnitAndVIN MapOwnerAndVehicleHandleToBusinessUnitAndVIN(string ownerHandle, string vehicleHandle);
    }
}