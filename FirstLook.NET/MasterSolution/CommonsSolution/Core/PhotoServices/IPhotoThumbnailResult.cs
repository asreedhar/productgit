﻿namespace FirstLook.Common.Core.PhotoServices
{
    public interface IPhotoThumbnailResult
    {
        string Url { get; }
        int Width { get; }
        int Height { get; }
    }
}