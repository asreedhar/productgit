namespace FirstLook.Common.Core.PhotoServices
{
    public interface IPhotoServicesSettings
    {
        string MainThumbnailPlaceholderUrl { get; }

        int MainThumbnailWidth { get; }

        int MainThumbnailHeight { get; }

        string MainPhotoPlaceholderUrl { get; }

        string PhotoManagerNotAvailableUrl { get; }

        string BulkUploadManagerNotAvailableUrl { get; }

        string PhotoWebServicesUrl { get; }

        string PhotoWebServicesV2Url { get; }

        string PhotoWebServiceV2_BusinessUnit_Whitelist { get; }

        int PhotoWebServiceV2Timeout { get; }
    }
}