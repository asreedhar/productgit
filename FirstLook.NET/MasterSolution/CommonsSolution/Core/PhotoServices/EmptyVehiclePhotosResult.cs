﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.PhotoServices.WebService.V1.Model;

namespace FirstLook.Common.Core.PhotoServices
{
    public class EmptyVehiclePhotosResult : IVehiclePhotosResult
    {
        public EmptyVehiclePhotosResult(IEnumerable<Exception> capturedException)
        {
            Errors = capturedException
                .Select(e => new Error(e.GetType().ToString(), e.Message))
                .ToArray();
        }

        public EmptyVehiclePhotosResult(IEnumerable<IPhotoServicesError> errors)
        {
            Errors = errors.ToArray();
        }

        public string Vin
        {
            get { return ""; }
        }

        public string[] PhotoUrls
        {
            get { return new string[0]; }
        }

        public ICollection<IPhotoServicesError> Errors { get; private set; }
    }
}