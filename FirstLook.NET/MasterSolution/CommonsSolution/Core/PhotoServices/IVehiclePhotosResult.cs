﻿using System;
using System.Collections.Generic;

namespace FirstLook.Common.Core.PhotoServices
{
    public interface IVehiclePhotosResult : IVehiclePhotos
    {
        ICollection<IPhotoServicesError> Errors { get; }
    }
}