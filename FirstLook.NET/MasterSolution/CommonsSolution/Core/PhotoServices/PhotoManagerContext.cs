﻿using FirstLook.Common.Core.PhotoServices.WebService.V1.Model;

namespace FirstLook.Common.Core.PhotoServices
{
    public abstract class PhotoManagerContext
    {
        public static PhotoManagerContext IMS { get; private set; }
        public static PhotoManagerContext MAX { get; private set; }

        static PhotoManagerContext()
        {
            IMS = new Impl("IMS", PhotoManagerWebContext.IMS);
            MAX = new Impl("MAX", PhotoManagerWebContext.MAX);
        }

        public abstract string Name { get; protected set; }
        public abstract PhotoManagerWebContext Code { get; protected set; }

        private class Impl : PhotoManagerContext
        {
            public Impl(string name, PhotoManagerWebContext code)
            {
                Name = name;
                Code = code;
            }

            public override sealed string Name { get; protected set; }
            public override sealed PhotoManagerWebContext Code { get; protected set; }
        }
    }
}