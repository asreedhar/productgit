﻿using System.Collections.Generic;

namespace FirstLook.Common.Core.PhotoServices
{
    public interface IVehiclesPhotosResult
    {
        IDictionary<string, IVehiclePhotos> PhotosByVin { get; }
        ICollection<IPhotoServicesError> Errors { get; }
    }
}