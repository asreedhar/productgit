﻿namespace FirstLook.Common.Core.PhotoServices
{
    public class BusinessUnitAndVIN
    {
        public string BusinessUnitCode { get; private set; }
        public string VIN { get; private set; }

        public BusinessUnitAndVIN(string businessUnitCode, string vin)
        {
            BusinessUnitCode = businessUnitCode;
            VIN = vin;
        }
    }
}