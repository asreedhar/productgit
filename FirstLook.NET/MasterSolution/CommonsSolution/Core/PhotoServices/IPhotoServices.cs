using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Membership;

namespace FirstLook.Common.Core.PhotoServices
{
    public interface IPhotoServices
    {
        string GetPhotoManagerUrl(IMember auditUser, string businessUnitCode, string vin, string stockNumber, PhotoManagerContext context);
        string GetBulkUploadManagerUrl(IMember auditUser, string businessUnitCode, PhotoManagerContext context);

        IVehiclePhotosResult GetPhotoUrlsByVin(string businessUnitCode, string vin, TimeSpan timeout);
        IVehiclesPhotosResult GetPhotoUrlsByBusinessUnit(string businessUnitCode, TimeSpan timeout);
        IDictionary<String, int> GetVinPhotoCountsForBusinessUnit(String businessUnitCode, TimeSpan timeout);

        string GetMainPhotoUrl(string businessUnitCode, string vin);
        IPhotoThumbnailResult GetMainThumbnailUrl(string businessUnitCode, string vin);
    }
}