using System.Runtime.Serialization;

namespace FirstLook.Common.Core.PhotoServices.WebService.V1.Model
{
    [DataContract(Namespace = Constants.PhotoWebServices_V1_Namespace)]
    public class Error : IPhotoServicesError
    {
        public Error()
        {
        }

        public Error(string code, string message)
        {
            Code = code;
            Message = message;
        }

        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}