using System.Runtime.Serialization;

namespace FirstLook.Common.Core.PhotoServices.WebService.V1.Model
{
    [DataContract(Namespace = Constants.PhotoWebServices_V1_Namespace)]
    public class ThumbnailResult
    {
        [DataMember]
        public int Width { get; set; }

        [DataMember]
        public int Height { get; set; }

        [DataMember]
        public string Url { get; set; }
    }
}