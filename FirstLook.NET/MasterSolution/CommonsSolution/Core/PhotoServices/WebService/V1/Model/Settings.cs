﻿using System.Runtime.Serialization;

namespace FirstLook.Common.Core.PhotoServices.WebService.V1.Model
{
    [DataContract(Namespace = Constants.PhotoWebServices_V1_Namespace)]
    public class Settings
    {
        [DataMember]
        public string ImageHostingBaseUrl { get; set; }

        [DataMember]
        public int ThumbnailWidth { get; set; }

        [DataMember]
        public int ThumbnailHeight { get; set; }
    }
}