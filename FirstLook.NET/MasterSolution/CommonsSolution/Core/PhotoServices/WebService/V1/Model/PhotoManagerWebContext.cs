using System.Runtime.Serialization;

namespace FirstLook.Common.Core.PhotoServices.WebService.V1.Model
{
    [DataContract(Namespace = Constants.PhotoWebServices_V1_Namespace)]
    public enum PhotoManagerWebContext
    {
        [EnumMember]
        IMS = 1,

        [EnumMember]
        MAX = 2
    }
}