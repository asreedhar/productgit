using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.Common.Core.PhotoServices.WebService.V1.Model
{
    [DataContract(Namespace = Constants.PhotoWebServices_V1_Namespace)]
    public class VehiclesPhotosResult
    {
        private static readonly Error[] EmptyErrorArray = new Error[0];
        private static readonly VehiclePhotos[] EmptyVehiclePhotosArray = new VehiclePhotos[0];

        public VehiclesPhotosResult()
        {
        }

        public VehiclesPhotosResult(params object[] objects)
        {
            var vehicles = new List<VehiclePhotos>();
            var errors = new List<Error>();
            foreach (var obj in objects)
            {
                if (obj is VehiclePhotos)
                    vehicles.Add((VehiclePhotos) obj);
                else
                    errors.Add((Error) obj);
            }
            Vehicles = vehicles.ToArray();
            Errors = errors.ToArray();
        }

        public VehiclesPhotosResult(params VehiclePhotos[] vehicles)
        {
            Vehicles = vehicles ?? EmptyVehiclePhotosArray;
            Errors = EmptyErrorArray;
        }

        public VehiclesPhotosResult(params Error[] errors)
        {
            Vehicles = EmptyVehiclePhotosArray;
            Errors = errors ?? EmptyErrorArray;
        }

        [DataMember]
        public VehiclePhotos[] Vehicles { get; set; }

        [DataMember]
        public Error[] Errors { get; set; }
    }
}