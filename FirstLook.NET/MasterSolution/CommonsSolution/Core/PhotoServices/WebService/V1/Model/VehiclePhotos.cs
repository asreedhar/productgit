﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace FirstLook.Common.Core.PhotoServices.WebService.V1.Model
{
    [Serializable]
    [DataContract(Namespace = Constants.PhotoWebServices_V1_Namespace)]
    public class VehiclePhotos : IVehiclePhotos
    {
        private static readonly string[] EmptyStringArray = new string[0];

        public VehiclePhotos()
        {
            PhotoUrls = EmptyStringArray;
        }

        public VehiclePhotos(string vin, params string[] urls)
        {
            Vin = vin;
            PhotoUrls = urls ?? EmptyStringArray;
        }

        [XmlIgnore]
        [DataMember(IsRequired = true)]
        public string Vin { get; set; }

        [XmlArray("Photos")]
        [XmlArrayItem("URL")]
        [DataMember]
        public string[] PhotoUrls { get; set; }
    }
}