using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using FirstLook.Common.Core.PhotoServices.WebService.V1.Model;

namespace FirstLook.Common.Core.PhotoServices.WebService.V1
{
    [ServiceContract(Name = "PhotoWebServices", Namespace = Constants.PhotoWebServices_V1_Namespace)]
    public interface IPhotoWebServices
    {
        [WebGet(BodyStyle = WebMessageBodyStyle.Bare)]
        [OperationContract]
        string GetPhotoManagerUrl(string username, string firstname, string lastname,
                                  string businessUnitCode, string vin, string stockNumber, PhotoManagerWebContext context);

        [WebGet]
        [OperationContract]
        string GetBulkUploadManagerUrl(string username, string firstname, string lastname,
                                       string businessUnitCode, PhotoManagerWebContext context);

        [WebGet]
        [OperationContract]
        VehiclesPhotosResult GetPhotoUrlsByVin(string businessUnitCode, string vin, int timeoutMilliseconds);

        [WebGet]
        [OperationContract]
        VehiclesPhotosResult GetPhotoUrlsByBusinessUnit(string businessUnitCode, int timeoutMilliseconds);

        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        Dictionary<String, int> GetVinPhotoCountsForBusinessUnit(string businessUnitCode, int timeoutMilliseconds);

        [WebGet]
        [OperationContract]
        string GetMainPhotoUrl(string businessUnitCode, string vin);

        [WebGet]
        [OperationContract]
        Model.ThumbnailResult GetMainThumbnailUrl(string businessUnitCode, string vin);

        [WebGet]
        [OperationContract]
        string Echo(string input);

        [WebGet]
        [OperationContract]
        Settings GetSettings();
    }
}