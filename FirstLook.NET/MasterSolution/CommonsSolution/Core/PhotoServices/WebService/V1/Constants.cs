namespace FirstLook.Common.Core.PhotoServices.WebService.V1
{
    public static class Constants
    {
        public const string PhotoWebServices_V1_Namespace = "http://schemas.incisent.com/internal/photoWebServices/2011/01";
    }
}