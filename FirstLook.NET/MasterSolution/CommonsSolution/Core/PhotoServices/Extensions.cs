using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using FirstLook.Common.Core.Logging;
using FirstLook.Common.Core.Membership;

namespace FirstLook.Common.Core.PhotoServices
{
    public static class Extensions
    {
        public static string GetPhotoManagerUrl(this IPhotoServices services, int businessUnitId, string vin,
                                                string stockNumber, PhotoManagerContext context)
        {
            string businessUnitCode;
            IMember currentMember;
            if(!LogWhenExceptionOccurs(
                    () => MapBusinessUnitIdToBusinessUnitCode(businessUnitId),
                    () => string.Format("Failed to map businessUnitId ('{0}') to businessUnitCode.", businessUnitId),
                    out businessUnitCode) 
            || !LogWhenExceptionOccurs(
                    GetCurrentMember,
                    () => "Failed to retrieve current member.",
                    out currentMember))
            {
                return PhotoServicesSettings().PhotoManagerNotAvailableUrl;
            }
            
            return services.GetPhotoManagerUrl(currentMember, businessUnitCode, vin, stockNumber, context);
        }

        public static string GetBulkUploadManagerUrl(this IPhotoServices services, int businessUnitId, PhotoManagerContext context)
        {
            string businessUnitCode;
            IMember currentMember;
            if (!LogWhenExceptionOccurs(
                    () => MapBusinessUnitIdToBusinessUnitCode(businessUnitId),
                    () => string.Format("Failed to map businessUnitId ('{0}') to businessUnitCode.", businessUnitId),
                    out businessUnitCode)
            || !LogWhenExceptionOccurs(
                    GetCurrentMember,
                    () => "Failed to retrieve current member.",
                    out currentMember))
            {
                return PhotoServicesSettings().BulkUploadManagerNotAvailableUrl;
            }

            return services.GetBulkUploadManagerUrl(currentMember, businessUnitCode, context);
        }

        public static IVehiclePhotosResult GetPhotoUrlsByVin(this IPhotoServices services, int businessUnitId, string vin, TimeSpan timeout)
        {
            string businessUnitCode;
            var capturedExceptions = new List<Exception>();
            return !LogWhenExceptionOccurs(() => MapBusinessUnitIdToBusinessUnitCode(businessUnitId),
                                           () =>
                                           string.Format("Failed to map businessUnitId ('{0}') to businessUnitCode.",
                                                         businessUnitId),
                                           out businessUnitCode, capturedExceptions)
                       ? new EmptyVehiclePhotosResult(capturedExceptions)
                       : services.GetPhotoUrlsByVin(businessUnitCode, vin, timeout);
        }

        public static IVehiclesPhotosResult GetPhotoUrlsByBusinessUnit(this IPhotoServices services, int businessUnitId, TimeSpan timeout)
        {
            string businessUnitCode;
            var capturedExceptions = new List<Exception>();
            var fakeSetting = ConfigurationManager.AppSettings["FakePhotoCount"] ?? ""; // for testing invalid photo counts FB: 31178

            return !LogWhenExceptionOccurs(() => MapBusinessUnitIdToBusinessUnitCode(businessUnitId),
                () => string.Format("Failed to map businessUnitId ('{0}') to businessUnitCode.",
                    businessUnitId), out businessUnitCode, capturedExceptions)
                    ? new EmptyVehiclesPhotosResult(capturedExceptions)
                    : fakeSetting.Contains("True") ? new EmptyVehiclesPhotosResult(capturedExceptions) // for testing invalid photo counts FB: 31178
                    : services.GetPhotoUrlsByBusinessUnit(businessUnitCode, timeout);
        }

        public static IDictionary<String, int> GetVinPhotoCountsForBusinessUnit(this IPhotoServices services, int businessUnitId, TimeSpan timeout)
        {
            string businessUnitCode;
            var capturedExceptions = new List<Exception>();

            return !LogWhenExceptionOccurs(
                () => MapBusinessUnitIdToBusinessUnitCode(businessUnitId),
                () => String.Format("Failed to map businessUnitId {0} to businessUnitCode", businessUnitId),
                out businessUnitCode,
                capturedExceptions) ? new Dictionary<String, int>() : services.GetVinPhotoCountsForBusinessUnit(businessUnitCode, timeout);
        }

        public static string GetMainPhotoUrlByOwnerAndVehicleHandle(this IPhotoServices services, string ownerHandle, string vehicleHandle)
        {
            BusinessUnitAndVIN businessUnitAndVIN;
            return !LogWhenExceptionOccurs(() => MapOwnerAndVehicleHandleToBusinessUnitAndVIN(ownerHandle, vehicleHandle),
                                       () => string.Format(
                                           "Failed to map ownerHandle ('{0}') to businessUnitCode or vehicleHandle ('{1}') to VIN.",
                                           ownerHandle, vehicleHandle), out businessUnitAndVIN)
                       ? PhotoServicesSettings().MainPhotoPlaceholderUrl
                       : services.GetMainPhotoUrl(businessUnitAndVIN.BusinessUnitCode, businessUnitAndVIN.VIN);
        }

        public static string GetMainPhotoUrl(this IPhotoServices services, int businessUnitId, string vin)
        {
            string businessUnitCode;
            return !LogWhenExceptionOccurs(() => MapBusinessUnitIdToBusinessUnitCode(businessUnitId),
                                           () =>
                                           string.Format("Failed to map businessUnitId ('{0}') to businessUnitCode",
                                                         businessUnitId),
                                           out businessUnitCode)
                       ? PhotoServicesSettings().MainPhotoPlaceholderUrl
                       : services.GetMainPhotoUrl(businessUnitCode, vin);
        }

        public static IPhotoThumbnailResult GetMainThumbnailUrl(this IPhotoServices services, int businessUnitId, string vin)
        {
            var photoServicesSettings = PhotoServicesSettings();
            string businessUnitCode;
            return !LogWhenExceptionOccurs(() => MapBusinessUnitIdToBusinessUnitCode(businessUnitId),
                                           () =>
                                           string.Format("Failed to map businessUnitId ('{0}') to businessUnitCode",
                                                         businessUnitId), out businessUnitCode)
                       ? new ThumbnailResult(photoServicesSettings.MainThumbnailPlaceholderUrl,
                                             photoServicesSettings.MainThumbnailWidth,
                                             photoServicesSettings.MainThumbnailHeight)
                       : services.GetMainThumbnailUrl(businessUnitCode, vin);
        }

        private static string MapBusinessUnitIdToBusinessUnitCode(int businessUnitId)
        {
            var mappedBusinessUnitCode = IdMapper.MapBusinessUnitIdToBusinessUnitCode(businessUnitId);
            if(string.IsNullOrEmpty(mappedBusinessUnitCode))
                throw new DataException("Business unit id could not be mapped to BusinessUnitCode.");
            return mappedBusinessUnitCode;
        }

        private static BusinessUnitAndVIN MapOwnerAndVehicleHandleToBusinessUnitAndVIN(string ownerHandle, string vehicleHandle)
        {
            var mappedValues = IdMapper.MapOwnerAndVehicleHandleToBusinessUnitAndVIN(ownerHandle, vehicleHandle);
            if(mappedValues == null)
                throw new DataException("Owner handle could not be mapped to BusinessUnitCode or vehicle handle could not be mapped to VIN.");
            return mappedValues;
        }

        private static IPhotoServicesIdMap IdMapper
        {
            get { return IOC.Registry.Resolve<IPhotoServicesIdMap>(); }
        }

        private static IMember GetCurrentMember()
        {
            var memberContext = IOC.Registry.Resolve<IMemberContext>();
            var currentMember = memberContext.Current;
            if(currentMember == null)
                throw new InvalidOperationException("Current user not set.");
            return currentMember;
        }

        private static ILog GetLog()
        {
            return LoggerFactory.GetLogger(typeof (Extensions));
        }

        private static IPhotoServicesSettings PhotoServicesSettings()
        {
            return IOC.Registry.Resolve<IPhotoServicesSettings>();
        }

        private static bool LogWhenExceptionOccurs<T>(Func<T> mapper, Func<string> formatErrorMsg, out T mappedValue, 
            ICollection<Exception> captureExceptions = null)
        {
            mappedValue = default(T);
            try
            {
                mappedValue = mapper();
            }
            catch (Exception ex)
            {
                var log = GetLog();
                if (log.IsErrorEnabled)
                {
                    log.Error(formatErrorMsg(), ex);
                }
                if(captureExceptions != null)
                {
                    captureExceptions.Add(ex);
                }
                return false;
            }

            return true;
        }
    }
}