﻿namespace Core.Messages
{
    /// <summary>
    /// A marker interface used to identify messages that apply to active inventory.
    /// </summary>
    public interface IActiveInventoryMessage : IInventoryMessage
    {
    }
}
