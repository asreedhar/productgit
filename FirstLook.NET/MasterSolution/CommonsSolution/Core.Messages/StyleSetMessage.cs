﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Messages
{
    public class StyleSetMessage : InventoryMessage
    {
        public StyleSetMessage(int businessUnitId, int inventoryId, string user)
            : base(businessUnitId, inventoryId)
        {
            User = user;
        }

        public string User { get; set; }
    }
}
