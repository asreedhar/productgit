﻿namespace Core.Messages
{
    public interface IInventoryMessage : IMessageBase
    {
        int BusinessUnitId { get; }
        int InventoryId { get; }
    }
}
