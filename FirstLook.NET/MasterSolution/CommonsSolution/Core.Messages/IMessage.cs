﻿namespace Core.Messages
{
    public interface IMessage<TValue>
    {
        TValue Object { get; }

        string ClaimCheck { get; }
        string ReceiptHandle { get; }
    }
}
