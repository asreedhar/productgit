﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Messages
{
    public class NewBookoutMessage : InventoryMessage
    {
        public NewBookoutMessage(int bookoutId, int businessUnitId, int inventoryId)
            :base(businessUnitId, inventoryId)
        {
            BookoutId = bookoutId;
        }

        public int BookoutId { get; set; }
    }
}
