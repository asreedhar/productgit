﻿using System;

namespace Core.Messages
{
    public class InventoryMessage : MessageBase, IInventoryMessage, ICloneable
    {
        public InventoryMessage(int businessUnitId, int inventoryId)
        {
            BusinessUnitId = businessUnitId;
            InventoryId = inventoryId;
        }

        //Leave setters public. There is weird behavior with Json.NET serializer if left private in that
        //certain properties fail to serialize. Don't want to use Json.NET JsonProperty attribute
        public int BusinessUnitId { get; set; }
        public int InventoryId { get; set; }
        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
