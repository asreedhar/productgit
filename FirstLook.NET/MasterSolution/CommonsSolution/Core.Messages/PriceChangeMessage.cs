﻿namespace Core.Messages
{
    public class PriceChangeMessage : InventoryMessage
    {
        public PriceChangeMessage(int eventId, int businessUnitId, int inventoryId)
            : base(businessUnitId, inventoryId)
        {
            EventId = eventId;
        }

        //Leave setters public. There is weird behavior with Json.NET serializer if left private in that
        //certain properties fail to serialize. Don't want to use Json.NET JsonProperty attribute
        public int EventId { get; set; }
    }
}
