﻿namespace Core.Messages
{
    public class NewInventoryMessage : InventoryMessage
    {
        public NewInventoryMessage(int businessUnitId, int inventoryId)
            :base(businessUnitId, inventoryId)
        {
        }
    }
}


