﻿using System;

namespace Core.Messages
{
    public interface IMessageBase
    {
        void SerializeMessageBaseProperties(bool value);

        string[] Errors { get; }
        bool ShouldSerializeErrors();

        DateTime Sent { get; set; }
        bool ShouldSerializeSent();

        TimeSpan Delay { get; }
        bool ShouldSerializeDelay();

        string SentFrom { get; set; }
        bool ShouldSerializeSentFrom();

        void AddError(string error);
        void SetDelay(TimeSpan delay);
    }
}
