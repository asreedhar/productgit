﻿using FirstLook.Common.Core.Extensions;

namespace Core.Messages
{
    public class DeadLetterMessage : MessageBase
    {
        public DeadLetterMessage(IMessageBase message)
        {
            if (message != null)
            {
                MessageType = message.GetType().FullName;
                ErrorMessage = message.ToJson();
            }
        }

        public DeadLetterMessage() { }

        public string MessageType { get; set; }
        public string ErrorMessage { get; set; }
    }
}
