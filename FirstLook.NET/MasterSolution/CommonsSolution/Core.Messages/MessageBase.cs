﻿using System;
using System.Linq;

namespace Core.Messages
{
    public class MessageBase : IMessageBase
    {
        private bool _serializeMessageBaseProperties;

        protected MessageBase()
        {
            Errors = new string[0];
            SentFrom = String.Empty;
            _serializeMessageBaseProperties = true;
        }

        #region Control JSON serialization of base message properties 
        public void SerializeMessageBaseProperties(bool value)
        {
            _serializeMessageBaseProperties = value;
        }
        public bool ShouldSerializeErrors()
        {
            return _serializeMessageBaseProperties;
        }
        public bool ShouldSerializeDelay()
        {
            return _serializeMessageBaseProperties;
        }
        public bool ShouldSerializeSent()
        {
            return _serializeMessageBaseProperties;
        }
        public bool ShouldSerializeSentFrom()
        {
            return _serializeMessageBaseProperties;
        }
        #endregion

        public string[] Errors { get; set; }

        public DateTime Sent { get; set; }

        public string SentFrom { get; set; }

        public TimeSpan Delay { get; set; }



        public void AddError(string error)
        {
            var list = Errors.ToList();
            list.Add(error);
            Errors = list.ToArray();
        }

        public void SetDelay(TimeSpan delay)
        {
            Delay = delay;
        }
    }
}
