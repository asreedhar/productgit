﻿namespace Core.Messages
{
    /// <summary>
    /// A marker interface used to identify messages that apply to active inventory where doNotPostFlag != 1.
    /// </summary>
    public interface IActiveAndNotOfflineInventoryMessage : IActiveInventoryMessage
    {
    }
}
