using System.ComponentModel;
using System.Web.UI;

namespace FirstLook.Common.WebControls
{
    /// <summary>
    /// Abstract base class for an asynchronous <code>DataSourceControl</code> by Nikhil Kothari.
    /// </summary>
    /// <see cref="http://msdn2.microsoft.com/en-us/library/ms364049(VS.80).aspx" />
    /// <see cref="http://msdn2.microsoft.com/en-us/library/ms364050(VS.80).aspx" />
    /// <see cref="http://msdn2.microsoft.com/en-us/library/ms364051(VS.80).aspx" />
    /// <see cref="http://msdn2.microsoft.com/en-us/library/ms364052(VS.80).aspx" />
    /// <see cref="http://msdn2.microsoft.com/en-us/library/ms364053(VS.80).aspx" />
    public abstract class AsyncDataSourceControl : DataSourceControl, IAsyncDataSource
    {
        private bool async;

        protected AsyncDataSourceControl()
        {
            async = true;
        }

        [Category("Behavior"), DefaultValue(true)]
        public virtual bool Async
        {
            get { return async; }
            set { async = value; }
        }

        bool IAsyncDataSource.IsAsync
        {
            get { return async; }
        }
    }
}