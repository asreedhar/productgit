﻿using System;
using System.Web;
using System.Configuration;

namespace FirstLook.Common.WebControls
{
    public class PromotionFilter : HttpApplication, IHttpModule
    {
        void IHttpModule.Init(HttpApplication context)
        {
            context.PostAuthorizeRequest += OnPostAuthorizeRequest;
        }

        public static void OnPostAuthorizeRequest(object sender, EventArgs args)
        {
            HttpRequest request = HttpContext.Current.Request;

            HttpCookie cookie = request.Cookies["promotion"];

            if (null == cookie)
            {
                HttpContext.Current.Response.Cookies.Add(new HttpCookie("promotion"));
                //HttpContext.Current.Response.Cookies["promotion"].Expires = DateTime.Now.AddYears(-30);
                String promotionRedirectUri = ConfigurationManager.AppSettings["promotion_url"];
                String servicePath = request.Path + "?" + request.QueryString;
                HttpContext.Current.Response.Redirect(promotionRedirectUri + "&service=" + HttpUtility.UrlEncode(servicePath), true);
            }
        }

        #region IHttpModule Members

        void IHttpModule.Dispose()
        {
        }

        #endregion
    }
}
