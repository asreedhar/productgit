using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;

namespace FirstLook.Common.WebControls.UI
{
	[ParseChildren(true),
	 PersistChildren(false)]
	public class UpdateHtmlTag : UpdatePanel
	{
		private string cssClass;
		private HtmlTextWriterTag tag;

		[DefaultValue(""),
		 Description("Css Attribute to Html Element")]
		public string CssClass
		{
			get
			{
				if (cssClass == null)
				{
					return string.Empty;
				}
				return cssClass;
			}
			set
			{
				if (value != cssClass)
				{
					cssClass = value;
				}
			}
		}

		[DefaultValue(HtmlTextWriterTag.Div)]
		[Description("The tag to render for the panel.")]
		public HtmlTextWriterTag Tag
		{
			get
			{
				if (tag == HtmlTextWriterTag.Unknown)
				{
					return HtmlTextWriterTag.Div;
				}
				return tag;
			}
			set
			{
				if (value != tag)
				{
					tag = value; 
				}
			}
		}

		protected override void RenderChildren(HtmlTextWriter writer)
		{
			if (IsInPartialRendering)
			{
				// Only Render Children
				base.RenderChildren(writer);
			}
			else
			{			
				writer.AddAttribute(HtmlTextWriterAttribute.Id, ClientID);
				if (CssClass.Length > 0)
				{
					writer.AddAttribute(HtmlTextWriterAttribute.Class, CssClass);
				}
				writer.RenderBeginTag(Tag);
				foreach (Control child in Controls)
				{
					child.RenderControl(writer);
				}
				writer.RenderEndTag();
			}
		}

	}
}
