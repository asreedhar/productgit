using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using FirstLook.Common.WebControls.Extenders;
using FirstLook.Common.WebControls.Helpers;

[assembly: WebResource("FirstLook.Common.WebControls.Extenders.ClickableControlBehavior.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Extenders.ClickableControlBehavior.debug.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Extenders.HasJavascriptControlBehavior.js", "text/javascript")]

namespace FirstLook.Common.WebControls.UI
{
    [ParseChildren(true),
     PersistChildren(false)]
    public class CollapsiblePanel : CompositeControl, IScriptControl
    {
        private ScriptManager manager;

        private CollapsiblePanelHeaderStyle expandedHeaderStyle;
        private CollapsiblePanelHeaderStyle collapsedHeaderStyle;

        private ITemplate headerTemplate;

        private CollapsiblePanelHeader head;
        private Panel body;

        private HtmlGenericControl fieldset;
        private HiddenField clientState;
        private Panel wrapper = new Panel();

        [DefaultValue(""),
         Description("The panel title text")]
        public string Title
        {
            get
            {
                string str = (string)ViewState["Title"];
                if (string.IsNullOrEmpty(str))
                    return string.Empty;
                return str;
            }
            set
            {
                if (string.Compare(Title, value) != 0)
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        ViewState.Remove("Title");
                    }
                    else
                    {
                        ViewState["Title"] = value;
                    }
                }
            }
        }

        [DefaultValue(""),
         Description("The toggle button"),
         IDReferencePropertyAttribute]
        public string ButtonID
        {
            get
            {
                string str = (string)ViewState["ButtonID"];
                if (string.IsNullOrEmpty(str))
                    return string.Empty;
                return str;
            }
            set
            {
                if (string.Compare(ButtonID, value) != 0)
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        ViewState.Remove("ButtonID");
                    }
                    else
                    {
                        ViewState["ButtonID"] = value;
                    }
                }
            }
        }

        [DefaultValue(""),
         Description("The toggle button"),
         IDReferencePropertyAttribute]
        public string LabelID
        {
            get
            {
                string str = (string)ViewState["LabelID"];
                if (string.IsNullOrEmpty(str))
                    return string.Empty;
                return str;
            }
            set
            {
                if (string.Compare(LabelID, value) != 0)
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        ViewState.Remove("LabelID");
                    }
                    else
                    {
                        ViewState["LabelID"] = value;
                    }
                }
            }
        }
		
		private bool HasJavascript
		{
			get
			{
				object value = ViewState["HasJavascript"];
				if (value == null)
					return false;
				return (bool)value;
			}
			set
			{
				if (HasJavascript != value)
				{
					ViewState["HasJavascript"] = value;
				}
			}
		}

		[DefaultValue(false),
		 Category("Behavior"),
		 Description("Set by the clients javascript, ensure js support")]
		public bool EnableScriptBehavior
		{
			get
			{
				object value = ViewState["EnableScriptBehavior"];
				if (value == null)
					return true;
				return (bool)value;
			}
			set
			{
				if (HasJavascript != value)
				{
					ViewState["EnableScriptBehavior"] = value;
				}
			}
		}

        [DefaultValue(false),
         Category("Styles"),
         Description("Allow the dialog to be dragged")]
        public bool Collapsed
        {
            get
            {
                object value = ViewState["Collapsed"];
                if (value == null)
                    return false;
                return (bool)value;
            }
            set
            {
                if (Collapsed != value)
                {
                    ViewState["Collapsed"] = value;
                }
            }
        }

        [DefaultValue(CollapsiblePanelExpandDirection.Vertical)]
        public CollapsiblePanelExpandDirection ExpandDirection
        {
            get
            {
                object value = ViewState["ExpandDirection"];
                if (value == null)
                    return CollapsiblePanelExpandDirection.Vertical;
                return (CollapsiblePanelExpandDirection) value;
            }
            set
            {
                if (ExpandDirection != value)
                {
                    ViewState["ExpandDirection"] = value;
                }
            }
        }

        [DefaultValue(true),
         Category("Behavior"),
         Description("Require a postback for initially collapsed panels")]
        public bool LazyItemTemplate
        {
            get
            {
                object value = ViewState["LazyItemTemplate"];
                if (value == null)
                    return true;
                return (bool)value;
            }
            set
            {
                if (LazyItemTemplate != value)
                {
                    ViewState["LazyItemTemplate"] = value;
                }
            }
        }

        protected bool ItemTemplateCreated
        {
            get
            {
                object value = ViewState["ItemTemplateCreated"];
                if (value == null)
                    return false;
                return (bool)value;
            }
            set
            {
                if (ItemTemplateCreated != value)
                {
                    ViewState["ItemTemplateCreated"] = value;
                }
            }
        }
        
        [PersistenceMode(PersistenceMode.InnerProperty),
         NotifyParentProperty(true),
         Description("Collapsed header style"),
         Category("Styles"),
         DefaultValue(null),
         DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public CollapsiblePanelHeaderStyle CollapsedHeaderStyle
        {
            get
            {
                if (collapsedHeaderStyle == null)
                {
                    collapsedHeaderStyle = new CollapsiblePanelHeaderStyle();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager)collapsedHeaderStyle).TrackViewState();
                    }
                }
                return collapsedHeaderStyle;
            }
        }

        [PersistenceMode(PersistenceMode.InnerProperty),
         NotifyParentProperty(true),
         Description("Collapsed header style"),
         Category("Styles"),
         DefaultValue(null),
         DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public CollapsiblePanelHeaderStyle ExpandedHeaderStyle
        {
            get
            {
                if (expandedHeaderStyle == null)
                {
                    expandedHeaderStyle = new CollapsiblePanelHeaderStyle();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager)expandedHeaderStyle).TrackViewState();
                    }
                }
                return expandedHeaderStyle;
            }
        }
        
        [Browsable(false),
         DefaultValue(null),
         Description("The collapse header template"),
         PersistenceMode(PersistenceMode.InnerProperty),
         TemplateContainer(typeof(CollapsiblePanelHeader))]
        public virtual ITemplate HeaderTemplate
        {
            get
            {
                return headerTemplate;
            }
            set
            {
                headerTemplate = value;
            }
        }

        [Browsable(false),
		Description("The collapible panel"),
         PersistenceMode(PersistenceMode.InnerProperty)]
        public virtual Panel ItemTemplate
        {
            get
            {
                return wrapper;
            }
        }

        private void Head_DataBinding(object sender, EventArgs e)
        {
            CollapsiblePanelHeaderStyle style = Collapsed ? collapsedHeaderStyle : expandedHeaderStyle;

			head.Title = Title;
			head.Collapsed = Collapsed;

            if (style != null)
            {
                head.ImageUrl = style.ImageUrl;
                head.ToolTip = style.ToolTip;
            	
				if (!string.IsNullOrEmpty(style.CssClass))
				{
					CssClassHelper.AddClass(head, style.CssClass);
				}            	                
            }
			else
            {
            	head.ToolTip = ToolTip;
            }
        }

        private void CollapsiblePanel_StateChanged(object sender, EventArgs e)
        {
			Hashtable table = new JavaScriptSerializer().Deserialize<Hashtable>(clientState.Value);

			if (table.ContainsKey("collapsed"))
				Collapsed = bool.Parse(table["collapsed"].ToString());
			if (table.ContainsKey("hasJavascript"))
				HasJavascript = bool.Parse(table["hasJavascript"].ToString());

            UpdateBodyControls();
        }

        private void Button_Click(object sender, EventArgs e)
        {
            if (Collapsed)
            {
                Collapsed = false;
            }
            else
            {
                Collapsed = true;
            }

            UpdateBodyControls();
        }

        protected override void TrackViewState()
        {
            base.TrackViewState();

            if (collapsedHeaderStyle != null)
            {
                ((IStateManager)collapsedHeaderStyle).TrackViewState();
            }

            if (expandedHeaderStyle != null)
            {
                ((IStateManager)expandedHeaderStyle).TrackViewState();
            }
        }

        protected override void LoadViewState(object savedState)
        {
            if (savedState != null)
            {
                object[] objArray = (object[])savedState;
                if (objArray[0] != null)
                {
                    base.LoadViewState(objArray[0]);
                }
                if (objArray[1] != null)
                {
                    ((IStateManager)CollapsedHeaderStyle).LoadViewState(objArray[1]);
                }
                if (objArray[2] != null)
                {
                    ((IStateManager)ExpandedHeaderStyle).LoadViewState(objArray[2]);
                }
            }
        }

        protected override object SaveViewState()
        {
            object obj0 = base.SaveViewState();
            object obj1 = (collapsedHeaderStyle != null) ? ((IStateManager)collapsedHeaderStyle).SaveViewState() : null;
            object obj2 = (expandedHeaderStyle != null) ? ((IStateManager)expandedHeaderStyle).SaveViewState() : null;
            return new object[] { obj0, obj1, obj2 };
        }

        protected override void CreateChildControls()
        {
            Controls.Clear();

            head = new CollapsiblePanelHeader();
            head.ID = "Head";
            head.CssClass = "ui-collapse-panel-head"; 
            head.DataBinding += Head_DataBinding;

            Controls.Add(head);

            fieldset = new HtmlGenericControl("fieldset");
            fieldset.Style["display"] = "none";

            Controls.Add(fieldset);

            clientState = new HiddenField();
            clientState.ID = "ClientState";

			clientState.Value = "{ hasJavascript: " + HasJavascript.ToString().ToLower() + "}";
            clientState.ValueChanged += CollapsiblePanel_StateChanged;

            fieldset.Controls.Add(clientState);

            if (HeaderTemplate == null)
            {
                throw new InvalidOperationException("Missing HeaderTemplate!");
            }

			HeaderTemplate.InstantiateIn(head);
			
            IButtonControl button = head.FindControl(ButtonID) as IButtonControl;

            if (button != null)
            {
                button.Click += Button_Click;
            }

            CreateBodyControls();
        }

        private void SetBodyVisibility(bool visible)
        {
			if (body != null)
			{
				if (visible && !HasJavascript)
				{
					body.Style.Remove("display");
				}
				else
				{
					body.Style["display"] = "none";
				}
			}            
        }

        private void UpdateBodyControls()
        {
            CreateBodyControls();

            SetBodyVisibility(!Collapsed);
        }

        private void CreateBodyControls()
        {
            // create panel body if
            // (a) was created on previous postback
            // (b) is not collapsed
            // (c) lazy creation is off

            if (body == null && (ItemTemplateCreated || !Collapsed || !LazyItemTemplate))
            {
                body = new Panel();
                body.ID = "Body";
				body.CssClass = "ui-collapse-panel-body " + CssClass;
				switch(ExpandDirection)
				{
					case CollapsiblePanelExpandDirection.Horizontal:
						body.CssClass += " ui-collapse-panel-horizontal-expanding";
						break;
					case CollapsiblePanelExpandDirection.Vertical:
						body.CssClass += " ui-collapse-panel-vertical-expanding";
						break;
				}

            	SetBodyVisibility(!Collapsed);

				Controls.Add(body);								

                wrapper.ID = "Wrapper";
                wrapper.CssClass = "ui-collapse-panel-body-wrapper";
				
                body.Controls.Add(wrapper);

                ItemTemplateCreated = true;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {			
            DataBind();

			if (Collapsed)
			{
				CssClassHelper.AddClass(head, "ui-collapsed");
				CssClassHelper.RemoveClass(head, "ui-open");
			}
			else
			{
				CssClassHelper.RemoveClass(head, "ui-collapsed");
				CssClassHelper.AddClass(head, "ui-open");
			}

            base.OnPreRender(e);

            if (!DesignMode)
            {
                manager = ScriptManager.GetCurrent(Page);

                if (manager != null)
                    manager.RegisterScriptControl(this);
                else
                    throw new ApplicationException("You must have a ScriptManager!");
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);

            if (!DesignMode)
            {
                manager.RegisterScriptDescriptors(this);
            }
        }

        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            // no begin tag please
        }

        public override void RenderEndTag(HtmlTextWriter writer)
        {
            // no end tag either thanks
        }

        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }

        protected override string TagName
        {
            get
            {
                return "div";
            }
        }

        #region IScriptControl Members

        public IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
			if (EnableScriptBehavior == false)
			{
				return new ScriptDescriptor[0];
			}
            if (body != null)
            {
                ScriptBehaviorDescriptor descriptor = new ScriptBehaviorDescriptor("AjaxControlToolkit.CollapsiblePanelBehavior", body.ClientID);
                descriptor.AddProperty("ClientStateFieldID", clientState.ClientID);
                descriptor.AddProperty("CollapseControlID", head.ClientID);
                descriptor.AddProperty("ExpandControlID", head.ClientID);
                descriptor.AddProperty("Collapsed", Collapsed);
                descriptor.AddProperty("SuppressPostBack", true);
                if (collapsedHeaderStyle != null)
                {
                    descriptor.AddProperty("CollapsedText", collapsedHeaderStyle.ToolTip);
                    descriptor.AddProperty("CollapsedImage", ResolveClientUrl(collapsedHeaderStyle.ImageUrl));
                }
                if (expandedHeaderStyle != null)
                {
                    descriptor.AddProperty("ExpandedText", expandedHeaderStyle.ToolTip);
                    descriptor.AddProperty("ExpandedImage", ResolveClientUrl(expandedHeaderStyle.ImageUrl));
                }
                Label label = head.FindControl(LabelID) as Label;
                if (label != null)
                    descriptor.AddProperty("TextLabelID", label.ClientID);
                Image image = head.FindControl(ButtonID) as Image;
                if (image != null)
                    descriptor.AddProperty("ImageControlID", image.ClientID);
                descriptor.AddProperty("ExpandDirection", ExpandDirection);
				descriptor.AddElementProperty("ChildDiv",wrapper.ClientID);
                return new ScriptDescriptor[] { descriptor };
            }
            else
            {
                ScriptBehaviorDescriptor clickable = new ScriptBehaviorDescriptor("FirstLook.Common.WebControls.Extenders.ClickableControlBehavior", head.ClientID);
				clickable.AddProperty("buttonID", head.FindControl(ButtonID).ClientID);				

				ScriptBehaviorDescriptor hasJavascript = new ScriptBehaviorDescriptor("FirstLook.Common.WebControls.Extenders.HasJavascriptControlBehavior", head.ClientID);
				hasJavascript.AddProperty("hasJavascript", HasJavascript.ToString());
				hasJavascript.AddElementProperty("clientStateField", clientState.ClientID);

				return new ScriptDescriptor[] { clickable, hasJavascript };
            }
        }

        public IEnumerable<ScriptReference> GetScriptReferences()
        {
			if (EnableScriptBehavior == false)
			{
				return new ScriptReference[0];
			}
            if (body != null)
            {
                return new ScriptReference[]
                {
					new ScriptReference("FirstLook.Common.WebControls.Timer.js", "FirstLook.Common.WebControls"),
                    new ScriptReference("FirstLook.Common.WebControls.Animations.Animation.js", "FirstLook.Common.WebControls"),
                    new ScriptReference("FirstLook.Common.WebControls.Animations.PropertyAnimation.js", "FirstLook.Common.WebControls"),
                    new ScriptReference("FirstLook.Common.WebControls.Animations.InterpolatedAnimation.js", "FirstLook.Common.WebControls"),
                    new ScriptReference("FirstLook.Common.WebControls.Animations.LengthAnimation.js", "FirstLook.Common.WebControls"),
                    new ScriptReference("FirstLook.Common.WebControls.Extenders.CollapsiblePanelBehavior.js", "FirstLook.Common.WebControls"),
					new ScriptReference("FirstLook.Common.WebControls.Extenders.HasJavascriptControlBehavior.js", "FirstLook.Common.WebControls")
                };
            }
            else
            {
            	return new ScriptReference[]
            		{
            			new ScriptReference("FirstLook.Common.WebControls.Extenders.ClickableControlBehavior.js",
            			                    "FirstLook.Common.WebControls"),
            			new ScriptReference("FirstLook.Common.WebControls.Extenders.HasJavascriptControlBehavior.js",
            			                    "FirstLook.Common.WebControls")
            		};
            }
            
        }

        #endregion
    }
}
