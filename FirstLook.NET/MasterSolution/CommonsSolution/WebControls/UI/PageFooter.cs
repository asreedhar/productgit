using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstLook.Common.WebControls.UI
{
    public class PageFooter : WebControl
    {
		[DefaultValue("False"),
		 Description("Show Chrome Data Copywrite")]
		public bool ShowChromeCopyright
		{
			get { return showChromeCopyright; }
			set { showChromeCopyright = value; }
		}
		
		private bool showChromeCopyright = false;

        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "page-footer");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.WriteLine();
            writer.Indent++;
        }

        public override void RenderEndTag(HtmlTextWriter writer)
        {
            writer.WriteLine();
            writer.Indent--;
            writer.RenderEndTag();
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            // <div class="status">
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "status");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.WriteLine();
            writer.Indent++;

            // <ul>
            writer.RenderBeginTag(HtmlTextWriterTag.Ul);
            writer.WriteLine();
            writer.Indent++;

            // <li>
            writer.RenderBeginTag(HtmlTextWriterTag.Li);
            writer.Write("USER: ");
            writer.Write(Context.User.Identity.Name);
            writer.RenderEndTag();
            writer.WriteLine();

            // <li>
            writer.RenderBeginTag(HtmlTextWriterTag.Li);
            writer.RenderBeginTag(HtmlTextWriterTag.Em);
            writer.Write("SECURE AREA");
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.WriteLine();

            // <li>
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "last");
            writer.RenderBeginTag(HtmlTextWriterTag.Li);
            writer.Write("Copyright &copy; ");
            writer.Write(DateTime.Now.Year);
            writer.Write(" First Look, LLC. All Rights Reserved.");
			if (ShowChromeCopyright)
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Class, "chrome-copy");
				writer.RenderBeginTag(HtmlTextWriterTag.Span);				
				writer.Write("Vehicle Data &copy;1986-");
				writer.Write(DateTime.Now.Year);
				writer.Write(" Chrome Systems, Inc.");
				writer.RenderEndTag();
			}
            writer.RenderEndTag();
            writer.WriteLine();			

        	// </ul>
            writer.WriteLine();
            writer.Indent--;
            writer.RenderEndTag();

            // </div>
            writer.WriteLine();
            writer.Indent--;
            writer.RenderEndTag();
            writer.WriteLine();

            // <div class="logo">&nbsp;</div>
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "logo");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.Write("&nbsp;");
            writer.RenderEndTag();
        }
    }
}
