using System.Web.UI.WebControls;

namespace FirstLook.Common.WebControls.UI
{
    public sealed class NamingContainerPanel : Panel, IIndexedPropertyNamingContainer
    {
        public object this[string index]
        {
            get
            {
                return ViewState[index];
            }
            set
            {
                if (!Equals(this[index], value))
                {
                    ViewState[index] = value;
                }
            }
        }
    }
}
