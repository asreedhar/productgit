using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using FirstLook.Common.WebControls.Helpers;

[assembly: WebResource("FirstLook.Common.WebControls.UI.Dialog.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.UI.Dialog.debug.js", "text/javascript")]

namespace FirstLook.Common.WebControls.UI
{
    /// <summary>
    /// Dialog control.
    /// </summary>
    /// <remarks>
    /// The reason the protected fields use the rather ugly hungarian notation is that to be CLS compliant
    /// (this making the DLL available to all .NET languages) the member name cannot differ soley in case
    /// (i.e. a public property of <code>Foo</code> backed by a protected field <code>foo</code>), nor can
    /// externally accessible members start with an underscore (therefore no protected field <code>_foo</code>).
    /// The available alternative is to use hungarian notation.
    /// </remarks>
    [ParseChildren(true),
     PersistChildren(false)]
    public class Dialog : CompositeControl, IScriptControl
    {
        private static readonly object EventButtonClick = new object();

        protected Dimension m_maximumDimension;
		protected Dimension m_minimumDimension;

		protected ITemplate m_headerTemplate;
		protected ITemplate m_itemTemplate;

		protected DialogButtonCollection m_buttonCollection;

		protected NamingContainerPanel m_headerPanel;
		protected NamingContainerPanel m_itemPanel;

		protected HtmlGenericControl m_fieldSet;
        protected HiddenField m_clientStateField;
		protected Panel m_containerPanel;
		protected Panel m_contentPanel;
		protected Panel m_footerPanel;

        public event EventHandler<DialogButtonClickEventArgs> ButtonClick
        {
            add
            {
                Events.AddHandler(EventButtonClick, value);
            }
            remove
            {
                Events.RemoveHandler(EventButtonClick, value);
            }
        }

        protected void OnDialogButtonClick(DialogButtonClickEventArgs e)
        {
            EventHandler<DialogButtonClickEventArgs> handler = (EventHandler<DialogButtonClickEventArgs>)Events[EventButtonClick];
            if (handler != null)
                handler(this, e);
        }

        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            CommandEventArgs commandArgs = args as CommandEventArgs;

            if (commandArgs != null)
            {
                if (string.Compare(commandArgs.CommandName, DialogButton.CommandName, StringComparison.Ordinal) == 0)
                {
                    int index;

                    if (int.TryParse((string)commandArgs.CommandArgument, out index))
                    {
                        DialogButtonCommand command = (DialogButtonCommand) Enum.ToObject(typeof (DialogButtonCommand), index);

                        OnDialogButtonClick(new DialogButtonClickEventArgs(command));
                    }

                    return true;
                }
            }

            return base.OnBubbleEvent(source, args);
        }

        [PersistenceMode(PersistenceMode.InnerProperty),
         NotifyParentProperty(true),
         Description("Maximum Control Size"),
         Category("Styles"),
         DefaultValue(null),
         DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public Dimension MaximumDimension
        {
            get
            {
                if (m_maximumDimension == null)
                {
                    m_maximumDimension = new Dimension();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager)m_maximumDimension).TrackViewState();
                    }
                }
                return m_maximumDimension;
            }
        }

        [PersistenceMode(PersistenceMode.InnerProperty),
         NotifyParentProperty(true),
         Description("Minimum Control Size"),
         Category("Styles"),
         DefaultValue(null),
         DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public Dimension MinimumDimension
        {
            get
            {
                if (m_minimumDimension == null)
                {
                    m_minimumDimension = new Dimension();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager)m_minimumDimension).TrackViewState();
                    }
                }
                return m_minimumDimension;
            }
        }

        [Browsable(false),
         DefaultValue(null),
         Description("The dialog header template"),
         PersistenceMode(PersistenceMode.InnerProperty),
         TemplateContainer(typeof(IIndexedPropertyNamingContainer))]
        public virtual ITemplate HeaderTemplate
        {
            get
            {
                return m_headerTemplate;
            }
            set
            {
                m_headerTemplate = value;
            }
        }

        [Browsable(false),
         DefaultValue(null),
         Description("The dialog item template"),
         PersistenceMode(PersistenceMode.InnerProperty),
         TemplateContainer(typeof(IIndexedPropertyNamingContainer))]
        public virtual ITemplate ItemTemplate
        {
            get
            {
                return m_itemTemplate;
            }
            set
            {
                m_itemTemplate = value;
            }
        }

        [DefaultValue((string)null),
         Description("Dialog Buttons"),
         MergableProperty(false),
         PersistenceMode(PersistenceMode.InnerProperty),
         Category("Default")]
        public virtual DialogButtonCollection Buttons
        {
            get
            {
                if (m_buttonCollection == null)
                {
                    m_buttonCollection = new DialogButtonCollection();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager)m_buttonCollection).TrackViewState();
                    }
                }
                return m_buttonCollection;
            }
        }

        protected virtual DialogButtonCollection ButtonsInternal
        {
            get
            {
                return m_buttonCollection;
            }
        }

		[DefaultValue(false),
		 Category("Behavior"),
		 Description("Hide Dialog while still rendering the html")]
		public bool Hidden
		{
			get
			{
				object value = ViewState["Hidden"];
				if (value == null)
					return false;
				return (bool)value;
			}
			set
			{
				if (Hidden != value)
				{
					ViewState["Hidden"] = value;
				}
			}
		}

        [DefaultValue(true),
         Category("Behavior"),
         Description("Allow the dialog to be dragged")]
        public bool AllowDrag
        {
            get
            {
                object value = ViewState["AllowDrag"];
                if (value == null)
                    return true;
                return (bool) value;
            }
            set
            {
                if (AllowDrag != value)
                {
                    ViewState["AllowDrag"] = value;
                }
            }
        }

        [DefaultValue(true),
         Category("Behavior"),
         Description("Allow the dialog to be dragged")]
        public bool AllowResize
        {
            get
            {
                object value = ViewState["AllowResize"];
                if (value == null)
                    return true;
                return (bool)value;
            }
            set
            {
                if (AllowResize != value)
                {
                    ViewState["AllowResize"] = value;
                }
            }
        }

		[DefaultValue(false),
		 Category("Behavior"),
		 Description("Allow the dialog to be dragged")]
		public bool AllowCancelPostBack
		{
			get
			{
				object value = ViewState["AllowCancelPostBack"];
				if (value == null)
					return false;
				return (bool)value;
			}
			set
			{
				if (AllowCancelPostBack != value)
				{
					ViewState["AllowCancelPostBack"] = value;
				}
			}
		}

        [DefaultValue(""),
         Description("The title of the dialog")]
        public string TitleText
        {
            get
            {
                string text = (string) ViewState["TitleText"];
                if (string.IsNullOrEmpty(text))
                    return string.Empty;
                return text;
            }
            set
            {
                if (string.Compare(TitleText, value, StringComparison.Ordinal) != 0)
                {
                    ViewState["TitleText"] = value;
                }
            }
        }

        [Category("Layout"), DefaultValue(typeof(Unit), ""), Description("Position:Top")]
        public Unit Top
        {
            get
            {
                if (ControlStyleCreated)
                {
                    return ((DialogStyle)ControlStyle).Top;
                }
                return Unit.Empty;
            }
            set
            {
                ((DialogStyle) ControlStyle).Top = value;
            }
        }

        [Category("Layout"), DefaultValue(typeof(Unit), ""), Description("Position:Left")]
        public Unit Left
        {
            get
            {
                if (ControlStyleCreated)
                {
                    return ((DialogStyle)ControlStyle).Left;
                }
                return Unit.Empty;
            }
            set
            {
                ((DialogStyle)ControlStyle).Left = value;
            }
        }

		public void Show()
		{
			Hidden = false;
		}

		public void Hide()
		{
			Hidden = true;
		}

		public void Toggle()
		{
			Hidden = !Hidden;
		}

        protected override void TrackViewState()
        {
            base.TrackViewState();

            if (m_minimumDimension != null)
            {
                ((IStateManager)m_minimumDimension).TrackViewState();
            }
            if (m_maximumDimension != null)
            {
                ((IStateManager)m_maximumDimension).TrackViewState();
            }
            if (m_buttonCollection != null)
            {
                ((IStateManager)m_buttonCollection).TrackViewState();
            }
        }

		protected IList<object[]> actualButtons = new List<object[]>();

        protected override void CreateChildControls()
        {
            Controls.Clear();

            m_fieldSet = new HtmlGenericControl("fieldset");
            m_fieldSet.Style["display"] = "none";

            Controls.Add(m_fieldSet);

            m_clientStateField = new HiddenField();
            m_clientStateField.ID = "ClientState";
        	m_clientStateField.Value = "{ hidden: " + Hidden.ToString(CultureInfo.InvariantCulture).ToLower() + " }";
            m_clientStateField.ValueChanged += Dialog_StateChanged;
			

            m_fieldSet.Controls.Add(m_clientStateField);

            m_containerPanel = new Panel();
            m_containerPanel.CssClass = "ui-dialog-container";

            Controls.Add(m_containerPanel);

            m_headerPanel = new NamingContainerPanel();
            m_headerPanel.ID = "TitleBar";
            m_headerPanel.CssClass = "ui-dialog-titlebar";
        	m_headerPanel.DataBinding += SetHeaderControlValues;

            m_containerPanel.Controls.Add(m_headerPanel);

            m_contentPanel = new Panel();
            m_contentPanel.CssClass = "ui-dialog-content";

			m_containerPanel.Controls.Add(m_contentPanel);

            m_itemPanel = new NamingContainerPanel();
            ItemPanel.ID = "ItemPanel";
            ItemPanel.CssClass = "ui-dialog-content-wrapper";
        	m_itemPanel.DataBinding += SetItemControlValues;

            m_contentPanel.Controls.Add(ItemPanel);

            HtmlGenericControl ul = new HtmlGenericControl("ul");

            if (ButtonsInternal != null && ButtonsInternal.Count > 0)
            {
				int i = 0;
                foreach (DialogButton button in ButtonsInternal)
                {                	
                    if (button.ButtonCommand != DialogButtonCommand.NoButton)
                    {
                        HtmlGenericControl li = new HtmlGenericControl("li");
                    	WebControl webButton = button.CreateControl();
                    	webButton.ID = "button_" + i;

						object[] buttons = new object[2];
                    	buttons[0] = webButton;
                    	buttons[1] = button.ButtonCommand;
						actualButtons.Add(buttons);
                    	
						li.Controls.Add(webButton);
                        ul.Controls.Add(li);
                    	i++;
                    }

                }
            }

            if (ul.Controls.Count > 0)
            {
                m_footerPanel = new NamingContainerPanel();
                m_footerPanel.ID = "FooterPanel";
                m_footerPanel.CssClass = "ui-dialog-buttonpane";
                m_footerPanel.Controls.Add(ul);

                Controls.Add(m_footerPanel);

            	foreach (object[] button in actualButtons)
            	{
            		button[0] = ((WebControl) button[0]).ClientID;
            	}
            }

            foreach (string value in new string[] {"n", "s", "e", "w", "ne", "se", "sw", "nw"})
            {
                Panel handle = new Panel();
                handle.CssClass = string.Format(CultureInfo.InvariantCulture, "ui-resizable-{0} ui-resizable-handle", value);
                Controls.Add(handle);
            }

            if (HeaderTemplate == null)
            {
                HeaderTemplate = CreateDefaultHeaderTemplate();
            }

            if (HeaderTemplate == null)
            {
                if (AllowDrag)
                {
                    throw new InvalidOperationException("AllowDrag needs a non-null HeaderTemplate");
                }
                else
                {
                    m_headerPanel.Visible = false;
                }
            }
            else
            {
                HeaderTemplate.InstantiateIn(m_headerPanel);
				DialogButton button = CancelButton();
				if (button != null)
                {
                    WebControl control = button.CreateControl();
                    control.CssClass = "ui-dialog-titlebar-close";
                    ((IButtonControl)control).Text = "X";
                    m_headerPanel.Controls.Add(control);

                	object[] closeButton = new object[2];
                	closeButton[0] = control.ClientID;
                	closeButton[1] = button.ButtonCommand;
					actualButtons.Add(closeButton);
                }
            }
            
            if (ItemTemplate == null)
            {
                ItemTemplate = CreateDefaultItemTemplate();
            }

            if (ItemTemplate != null)
            {
                ItemTemplate.InstantiateIn(ItemPanel);
            }
        }

        private DialogButton CancelButton()
        {
            if (ButtonsInternal != null)
                foreach (DialogButton button in ButtonsInternal)
                    if (button.IsCancel)
                        return button;
            return null;
        }

        protected virtual void SetHeaderControlValues(object sender, EventArgs e)
        {
        	NamingContainerPanel control = (NamingContainerPanel) sender;
            control["TitleText"] = TitleText;
        }

		protected virtual void SetItemControlValues(object sender, EventArgs e)
		{
			// do nothing
		}

        protected virtual ITemplate CreateDefaultHeaderTemplate()
        {
            return new DefaultDialogHeaderTemplate();
        }

        protected virtual ITemplate CreateDefaultItemTemplate()
        {
            return null;
        }

        private void Dialog_StateChanged(object sender, EventArgs e)
        {
            Hashtable table = new JavaScriptSerializer().Deserialize<Hashtable>(m_clientStateField.Value);
            if (table.ContainsKey("width"))
                Width = new Unit(table["width"].ToString(), CultureInfo.InvariantCulture);
            if (table.ContainsKey("height"))
                Height = new Unit(table["height"].ToString(), CultureInfo.InvariantCulture);
            if (table.ContainsKey("top"))
                Top = new Unit(table["top"].ToString(), CultureInfo.InvariantCulture);
            if (table.ContainsKey("left"))
                Left = new Unit(table["left"].ToString(), CultureInfo.InvariantCulture);
			if (table.ContainsKey("hidden"))
				Hidden = bool.Parse(table["hidden"].ToString());
			if (table.ContainsKey("allowCancelPostBack"))
				AllowCancelPostBack = bool.Parse(table["AllowCancelPostBack"].ToString());
        }

		protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }

        protected override string TagName
        {
            get
            {
                return "div";
            }
        }

    	protected NamingContainerPanel ItemPanel
    	{
    		get { return m_itemPanel; }
    	}

		protected NamingContainerPanel HeaderPanel
		{
			get { return m_headerPanel; }
		}

		protected Panel FooterPanel
		{
			get { return m_footerPanel; }
		}

		public override Control FindControl(string id)
		{
			if (ItemPanel.FindControl(id) != null)
			{
				return ItemPanel.FindControl(id);
			}
			if (HeaderPanel.FindControl(id) != null)
			{
				return HeaderPanel.FindControl(id);
			}
			if (FooterPanel.FindControl(id) != null)
			{
				return FooterPanel.FindControl(id);
			}
			if (base.FindControl(id) != null)
			{
				return base.FindControl(id);
			}
			return null;
		}

    	protected override void LoadViewState(object savedState)
        {
            if (savedState != null)
            {
                object[] objArray = (object[])savedState;
                if (objArray[0] != null)
                {
                    base.LoadViewState(objArray[0]);
                }
                if (objArray[1] != null)
                {
                    ((IStateManager)MinimumDimension).LoadViewState(objArray[1]);
                }
                if (objArray[2] != null)
                {
                    ((IStateManager)MaximumDimension).LoadViewState(objArray[2]);
                }
                if (objArray[3] != null)
                {
                    ((IStateManager)Buttons).LoadViewState(objArray[3]);
                }
            }
        }

        protected override object SaveViewState()
        {
            object obj0 = base.SaveViewState();
            object obj1 = (m_minimumDimension != null) ? ((IStateManager)m_minimumDimension).SaveViewState() : null;
            object obj2 = (m_maximumDimension != null) ? ((IStateManager)m_maximumDimension).SaveViewState() : null;
            object obj3 = (m_buttonCollection != null) ? ((IStateManager)m_buttonCollection).SaveViewState() : null;
            return new object[] { obj0, obj1, obj2, obj3 };
        }

        protected override Style CreateControlStyle()
        {
            return new DialogStyle(ViewState);
        }

        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            if (string.IsNullOrEmpty(CssClass))
            {
                CssClass = "ui-dialog";
            }
            else
            {
                string[] values = new Regex("\\s+").Split(CssClass);

                bool found = false;

                for (int i = 0, l = values.Length; i < l && !found; i++)
                {
                    found |= values[i].Equals("ui-dialog");
                }

                if (!found) CssClass += " ui-dialog";
            }

            if (Top != Unit.Empty)
                writer.AddStyleAttribute(HtmlTextWriterStyle.Top, Top.ToString());

            if (Left != Unit.Empty)
                writer.AddStyleAttribute(HtmlTextWriterStyle.Left, Left.ToString());			

            base.AddAttributesToRender(writer);
        }

        private ScriptManager manager;

        protected override void OnPreRender(EventArgs e)
        {
            DataBind();

			if (Hidden)
			{
				CssClassHelper.Hide(this);
			}
			else
			{
				CssClassHelper.Show(this);
			}
				

            base.OnPreRender(e);

            if (!DesignMode)
            {
                manager = ScriptManager.GetCurrent(Page);

                if (manager != null)
                    manager.RegisterScriptControl(this);
                else
                    throw new InvalidOperationException("You must have a ScriptManager!");
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);

            if (!DesignMode)
            {
                manager.RegisterScriptDescriptors(this);
            }
        }

        #region IScriptControl Members

        public virtual IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            List<ScriptDescriptor> descriptors = new List<ScriptDescriptor>();

            if (AllowResize)
            {
                ScriptBehaviorDescriptor resizable = new ScriptBehaviorDescriptor("FirstLook.Common.WebControls.Extenders.ResizableControlBehavior", ClientID);
                resizable.AddProperty("clientStateFieldID", m_clientStateField.ClientID);
                if (m_minimumDimension != null)
                {
                    resizable.AddProperty("minimumWidth", m_minimumDimension.Width.Value);
                    resizable.AddProperty("minimumHeight", m_minimumDimension.Height.Value);
                }
                if (m_maximumDimension != null)
                {
                    resizable.AddProperty("maximumWidth", m_maximumDimension.Width.Value);
                    resizable.AddProperty("maximumHeight", m_maximumDimension.Height.Value);
                }
                descriptors.Add(resizable);
            }

            if (AllowDrag)
            {
                ScriptBehaviorDescriptor draggable = new ScriptBehaviorDescriptor("FirstLook.Common.WebControls.Extenders.DraggableControlBehavior", ClientID);
                draggable.AddProperty("clientStateFieldID", m_clientStateField.ClientID);
                draggable.AddElementProperty("handle", m_headerPanel.ClientID);
                descriptors.Add(draggable);
            }

			ScriptBehaviorDescriptor dialog = new ScriptBehaviorDescriptor("FirstLook.Common.WebControls.UI.Dialog",ClientID);
			dialog.AddElementProperty("ClientStateField", m_clientStateField.ClientID);			
			dialog.AddProperty("ButtonCollection", actualButtons);
			dialog.AddProperty("AllowCancelPostBack", AllowCancelPostBack);
			dialog.AddProperty("Hidden", Hidden);
        	dialog.ID = ClientID;
			descriptors.Add(dialog);

            return descriptors;
        }

        public virtual IEnumerable<ScriptReference> GetScriptReferences()
        {
			return new ScriptReference[] {
                new ScriptReference("FirstLook.Common.WebControls.Extenders.DraggableControlBehavior.js", "FirstLook.Common.WebControls"),
                new ScriptReference("FirstLook.Common.WebControls.Extenders.ResizableControlBehavior.js", "FirstLook.Common.WebControls"),
				new ScriptReference("FirstLook.Common.WebControls.UI.Dialog.js", "FirstLook.Common.WebControls")
           };
        }

        #endregion

        class DefaultDialogHeaderTemplate : ITemplate
        {
            public void InstantiateIn(Control container)
            {
                Label label = new Label();
                label.CssClass = "ui-dialog-title";
            	label.DataBinding += Label_DataBinding;
                container.Controls.Add(label);
            }

			private static void Label_DataBinding(object sender, EventArgs e)
			{
				Label label = sender as Label;
				if (label != null)
				{
					IIndexedPropertyNamingContainer container = (IIndexedPropertyNamingContainer)label.NamingContainer;
	                label.Text = (string) container["TitleText"];
				}
			}
        }
    }
}
