using System;
using System.ComponentModel;
using System.Globalization;
using System.Web;
using System.Web.UI;
using FirstLook.Common.WebControls.Helpers;

namespace FirstLook.Common.WebControls.UI
{
    [ToolboxData( "<{0}:TabContainer runat=\"server\"></{0}:TabContainer>" ),
     DefaultEvent( "ActiveTabChanged" ),
     ParseChildren( typeof( TabPanel ) )]
    public class TabContainer : Control, IPostBackEventHandler
    {
        private int _activeTabIndex = -1;
        private int _cachedActiveTabIndex = -1;
        private bool _controlStateApplied;
        private bool _initApplied;

        #region Events

        private static readonly object EventActiveTabChanging = new object();
        private static readonly object EventActiveTabChanged = new object();

        [Category( "Behavior" )]
        public event EventHandler ActiveTabChanging
        {
            add { Events.AddHandler( EventActiveTabChanging, value ); }
            remove { Events.RemoveHandler( EventActiveTabChanging, value ); }
        }

        protected void OnActiveTabChanging ( EventArgs e )
        {
            EventHandler handler = Events[ EventActiveTabChanging ] as EventHandler;
            if ( handler != null )
            {
                handler( this, e );
            }
        }

        [Category( "Behavior" )]
        public event EventHandler ActiveTabChanged
        {
            add { Events.AddHandler( EventActiveTabChanged, value ); }
            remove { Events.RemoveHandler( EventActiveTabChanged, value ); }
        }

        protected void OnActiveTabChanged ( EventArgs e )
        {
            EventHandler handler = Events[ EventActiveTabChanged ] as EventHandler;
            if ( handler != null )
            {
                handler( this, e );
            }
        }

        #endregion

        #region Properties

        [PersistenceMode( PersistenceMode.InnerDefaultProperty ),
         Browsable( false ),
         Description( "Collection of tab panels" )]
        public TabPanelCollection Tabs
        {
            get { return ( TabPanelCollection )Controls; }
        }

        public string CssClass
        {
            get
            {
                string text = ViewState[ "CssClass" ] as string;
                if ( string.IsNullOrEmpty( text ) )
                    return string.Empty;
                return text;
            }
            set
            {
                if ( !string.Equals( CssClass, value ) )
                {
                    if ( string.IsNullOrEmpty( value ) )
                    {
                        ViewState.Remove( "CssClass" );
                    }
                    else
                    {
                        ViewState[ "CssClass" ] = value;
                    }
                }
            }
        }
        [DefaultValue( -1 ),
         Category( "Behavior" ),
         Description( "The zero-based index of the active tab" )]
        public int ActiveTabIndex
        {
            get
            {
                if ( _cachedActiveTabIndex > -1 )
                {
                    return _cachedActiveTabIndex;
                }
                return _activeTabIndex;
            }
            set
            {
                if ( value < -1 )
                {
                    throw new ArgumentOutOfRangeException( "value",
                                                          string.Format( CultureInfo.InvariantCulture,
                                                              "MultiView_ActiveViewIndex_less_than_minus_one {0}", value ) );
                }
                if ( ( Tabs.Count == 0 ) && !_initApplied )
                {
                    _cachedActiveTabIndex = value;
                }
                else
                {
                    if ( value >= Tabs.Count )
                    {
                        throw new ArgumentOutOfRangeException( "value",
                                                              string.Format( CultureInfo.InvariantCulture,
                                                                  "MultiView_ActiveViewIndex_equal_or_greater_than_count {0} {1}",
                                                                  value, Tabs.Count ) );
                    }
                    int oldTabIndex = ( _cachedActiveTabIndex != -1 ) ? -1 : _activeTabIndex;
                    // turn off old tab
                    if ( oldTabIndex != value )
                    {
                        if ( ( ( oldTabIndex != -1 ) ) && ( oldTabIndex < Tabs.Count ) )
                        {
                            if ( ShouldTriggerTabEvent )
                            {
                                OnActiveTabChanging( EventArgs.Empty );
                            }
                            Tabs[ oldTabIndex ].Active = false;
                        }
                    }
                    // change tabs
                    _activeTabIndex = value;
                    _cachedActiveTabIndex = -1;
                    // turn on new tab
                    if ( oldTabIndex != value )
                    {
                        if ( ( ( Tabs.Count != 0 ) ) && ( value != -1 ) )
                        {
                            Tabs[ value ].Active = true;
                            if ( ShouldTriggerTabEvent )
                            {
                                OnActiveTabChanged( EventArgs.Empty );
                            }
                        }
                    }
                }
            }
        }

        private bool ShouldTriggerTabEvent
        {
            get { return ( _controlStateApplied || ( ( Page != null ) && !Page.IsPostBack ) ); }
        }

        [DefaultValue( false ),
         Category( "Behavior" ),
         Description( "TabContainer Set Contains Empty State" )]
        public bool HasEmptyState
        {
            get
            {
                object value = ViewState[ "HasEmptyState" ];
                if ( value == null )
                    return false;
                return ( bool )value;
            }
            set
            {
                if ( value != HasEmptyState )
                {
                    ViewState[ "HasEmptyState" ] = value;
                }
            }
        }

        #endregion

        #region Methods

        public TabPanel GetActiveTabPanel ()
        {
            int activeViewIndex = ActiveTabIndex;
            if ( activeViewIndex >= Tabs.Count )
            {
                throw new Exception( "MultiView_ActiveViewIndex_out_of_range" );
            }
            if ( activeViewIndex < 0 )
            {
                return null;
            }
            TabPanel tabPanel = Tabs[ activeViewIndex ];
            if ( !tabPanel.Active )
            {
                UpdateActiveTab( activeViewIndex );
            }
            return tabPanel;
        }

        private void UpdateActiveTab ( int activeViewIndex )
        {
            for ( int i = 0; i < Tabs.Count; i++ )
            {
                TabPanel view = Tabs[ i ];
                if ( i == activeViewIndex )
                {
                    view.Active = true;
                }
                else if ( view.Active )
                {
                    view.Active = false;
                }
            }
        }

        #endregion

        #region Parent Overrides

        protected override void AddParsedSubObject ( object obj )
        {
            if ( obj is TabPanel )
            {
                Controls.Add( ( Control )obj );
            }
            else if ( !( obj is LiteralControl ) )
            {
                throw new HttpException(
                    string.Format( CultureInfo.InvariantCulture, "TabContainer cannot have children of type {0}", new object[] { obj.GetType().Name } ) );
            }
        }

        protected override void RemovedControl ( Control ctl )
        {
            if ( ( ( TabPanel )ctl ).Active && ( ActiveTabIndex < Tabs.Count ) )
            {
                GetActiveTabPanel();
            }
            base.RemovedControl( ctl );
        }

        protected override ControlCollection CreateControlCollection ()
        {
            return new TabPanelCollection( this );
        }

        protected override void LoadControlState ( object state )
        {
            Pair pair = state as Pair;
            if ( pair != null )
            {
                base.LoadControlState( pair.First );
                ActiveTabIndex = ( int )pair.Second;
            }
            else
            {
                base.LoadControlState( state );
            }
            _controlStateApplied = true;
        }

        protected override object SaveControlState ()
        {
            object state = base.SaveControlState();
            int tabIndex = ActiveTabIndex;
            if ( tabIndex != -1 )
            {
                return new Pair( state, tabIndex );
            }
            return state;
        }

        protected override void OnInit ( EventArgs e )
        {
            base.OnInit( e );
            Page.RegisterRequiresControlState( this );
            _initApplied = true;
            if ( _cachedActiveTabIndex > -1 )
            {
                ActiveTabIndex = _cachedActiveTabIndex;
                _cachedActiveTabIndex = -1;
                GetActiveTabPanel();
            }
        }

        protected override void OnPreRender ( EventArgs e )
        {
            base.OnPreRender( e );

            foreach ( TabPanel tab in Tabs )
            {
                tab.RaiseDataBinding();
            }
        }

        #endregion

        #region Render

        private const string WrapperCssClass = "ui-tab-container";
        private const string TabsCssClass = "ui-tabs";
        private const string TabCssClass = "ui-tab";
        private const string LiActiveCssClass = "ui-tab-active";
        private const string UlActiveCssClass = "ui-active-tab-"; // + ActiveTab base 1
        private const string ButtonActiveCssClass = "ui-tab-active-input";
        private const string TabContentCssClass = "ui-tab-content";
        private const string EmptyTabContentCssClass = "ui-empty-tab-content";
        private const string TabButtonNameFormat = "Tab_Button_{0}";

        protected void AddAttributesToRender ( HtmlTextWriter writer )
        {
            if ( ID != null )
            {
                writer.AddAttribute( HtmlTextWriterAttribute.Id, ClientID );
            }
            writer.AddAttribute( HtmlTextWriterAttribute.Class, CssClassHelper.AddClass( CssClass, WrapperCssClass ) );
        }

        protected override void Render ( HtmlTextWriter writer )
        {
            RenderBeginTag( writer );
            RenderContents( writer );
            RenderEndTag( writer );
        }

        public void RenderBeginTag ( HtmlTextWriter writer )
        {
            AddAttributesToRender( writer );

            writer.RenderBeginTag( HtmlTextWriterTag.Div );

            int activeTabIndex = ActiveTabIndex;
            string ulCssClass = TabsCssClass;
            if ( activeTabIndex == -1 )
                ulCssClass = CssClassHelper.AddClass( ulCssClass, UlActiveCssClass + "none" );
            else
                ulCssClass = CssClassHelper.AddClass( ulCssClass, UlActiveCssClass + activeTabIndex );
            writer.AddAttribute( HtmlTextWriterAttribute.Class, ulCssClass );
            writer.RenderBeginTag( HtmlTextWriterTag.Ul );

            for ( int i = 0, l = Tabs.Count; i < l; i++ )
            {
                TabPanel tab = Tabs[ i ];

                #region List Item

                string liCssClass = CssClassHelper.AddClass( TabCssClass, tab.CssClass );
                if ( tab.Active )
                {
                    liCssClass = CssClassHelper.AddClass( liCssClass, LiActiveCssClass );
                }

                writer.AddAttribute( HtmlTextWriterAttribute.Class, liCssClass );
                writer.RenderBeginTag( HtmlTextWriterTag.Li );

                #region Button

                string buttonCssClass = string.Format( CultureInfo.InvariantCulture, TabButtonNameFormat, i );
                if ( tab.Active )
                {
                    buttonCssClass = CssClassHelper.AddClass( buttonCssClass, ButtonActiveCssClass );
                }

                writer.AddAttribute( HtmlTextWriterAttribute.Type, "submit" );
                writer.AddAttribute( HtmlTextWriterAttribute.Name, UniqueID );
                writer.AddAttribute( HtmlTextWriterAttribute.Id, UniqueID + "$" + CssClassHelper.RemoveClass( buttonCssClass, ButtonActiveCssClass ) );
                writer.AddAttribute( HtmlTextWriterAttribute.Value, tab.Text );
                writer.AddAttribute( HtmlTextWriterAttribute.Onclick, ( !string.IsNullOrEmpty( tab.OnClientClick ) ? tab.OnClientClick + ";" : string.Empty ) + Page.ClientScript.GetPostBackEventReference( this, i.ToString() ) + ";return false;" );
                writer.AddAttribute( HtmlTextWriterAttribute.Class, buttonCssClass );
                if ( !string.IsNullOrEmpty( tab.ToolTip ) )
                    writer.AddAttribute( HtmlTextWriterAttribute.Title, tab.ToolTip );
                writer.RenderBeginTag( HtmlTextWriterTag.Input );
                writer.RenderEndTag();

                #endregion

                writer.RenderEndTag();

                #endregion

                writer.WriteLine();
            }

            writer.RenderEndTag(); // en ul
        }

        protected void RenderContents ( HtmlTextWriter writer )
        {
            string cssClass = TabContentCssClass;
            if ( ActiveTabIndex == -1 )
                cssClass = CssClassHelper.AddClass( cssClass, EmptyTabContentCssClass );
            writer.AddAttribute( HtmlTextWriterAttribute.Class, cssClass );
            writer.RenderBeginTag( HtmlTextWriterTag.Div );
            RenderChildren( writer );
            writer.RenderEndTag();
        }


        private void RenderEndTag ( HtmlTextWriter writer )
        {
            writer.RenderEndTag();
        }

        #endregion

        #region IPostBackEventHandler Members

        void IPostBackEventHandler.RaisePostBackEvent ( string eventArgument )
        {
            if ( !string.IsNullOrEmpty( eventArgument ) )
            {
                int index;
                if ( int.TryParse( eventArgument, out index ) )
                {
                    if ( ActiveTabIndex == index && HasEmptyState )
                    {
                        ActiveTabIndex = -1;
                    }
                    else
                    {
                        ActiveTabIndex = index;
                    }
                }
            }
        }

        #endregion
    }
}