using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstLook.Common.WebControls.UI
{
    public class CollapsiblePanelHeader : Panel, INamingContainer
    {
        private bool _collapsed;
        private string _title;
        private string _imageUrl;

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public string ImageUrl
        {
            get { return _imageUrl; }
            set { _imageUrl = value; }
        }

        public bool Collapsed
        {
            get { return _collapsed; }
            set { _collapsed = value; }
        }
    }
}
