using System;
using System.ComponentModel;
using System.Web.UI;

namespace FirstLook.Common.WebControls.UI
{
    [ToolboxData("<{0}:TabPanel runat=\"server\"></{0}:View>"), ParseChildren(false)]
    public class TabPanel : Control
    {
        private bool _active;

        internal bool Active
        {
            get { return _active; }
            set
            {
                _active = value;

                base.Visible = true;
            }
        }

        public override bool Visible
        {
            get
            {
                if (Parent == null)
                {
                    return Active;
                }
                return (Active && Parent.Visible);
            }
            set
            {
                if (!DesignMode)
                {
                    throw new InvalidOperationException("Tab Cannot Set Visible");
                }
            }
        }

        [Category(""), Description("")]
        public string Text
        {
            get
            {
                string text = ViewState["Text"] as string;
                if (string.IsNullOrEmpty(text))
                    return string.Empty;
                return text;
            }
            set
            {
                if (!string.Equals(Text, value))
                {
                    ViewState["Text"] = value;
                }
            }
        }

        public string ToolTip
        {
            get
            {
                string text = ViewState["ToolTip"] as string;
                if (string.IsNullOrEmpty(text))
                    return string.Empty;
                return text;
            }
            set
            {
                if (!string.Equals(ToolTip, value))
                {
                    ViewState["ToolTip"] = value;
                }
            }
        }

        public string CssClass
        {
            get
            {
                string text = ViewState["CssClass"] as string;
                if (string.IsNullOrEmpty(text))
                    return string.Empty;
                return text;
            }
            set
            {
                if (!string.Equals(CssClass, value))
                {
                    ViewState["CssClass"] = value;
                }
            }
        }

        public string OnClientClick
        {
            get
            {
                string text = ViewState[ "OnClientClick" ] as string;
                if ( string.IsNullOrEmpty( text ) )
                    return string.Empty;
                return text;
            }
            set
            {
                if ( !string.Equals( OnClientClick, value ) )
                {
                    ViewState[ "OnClientClick" ] = value;
                }
            }
        }

        protected override void DataBindChildren()
        {
            if (Visible)
            {
                base.DataBindChildren(); 
            }
        }

        internal void RaiseDataBinding()
        {
            OnDataBinding(EventArgs.Empty);
        }
    }
}