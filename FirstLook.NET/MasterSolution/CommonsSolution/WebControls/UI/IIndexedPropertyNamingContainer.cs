using System.Web.UI;

namespace FirstLook.Common.WebControls.UI
{
    public interface IIndexedPropertyNamingContainer : INamingContainer
    {
        object this[string index]
        {
            get; set;
        }
    }
}
