﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstLook.Common.WebControls.UI.Chart
{
    [ParseChildren(true), PersistChildren(false)]
    public class BarChart : CompositeDataBoundControl
    {
        #region Properties

        private BarChartDirection _direction = BarChartDirection.TopToBottom;
        private Axis _yAxis;
        private BarCollection _bars;

        public BarChartDirection Direction
        {
            get
            {
                if (BarChartDirection.Unkown.Equals(_direction))
                {
                    return BarChartDirection.TopToBottom;
                }
                return _direction;
            }
            set { _direction = value; }
        }

        [PersistenceMode(PersistenceMode.InnerProperty),
         NotifyParentProperty(true),
         Description("Definition of the yAxis"),
         Category("Axis"),
         DefaultValue(null),
         DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public Axis YAxis
        {
            get
            {
                if (IsTrackingViewState)
                {
                    ((IStateManager)_yAxis).TrackViewState();
                }
                return _yAxis;
            }
            set { _yAxis = value; }
        }       

        public BarCollection Bars
        {
            get
            {
                if (_bars == null)
                {
                    _bars = new BarCollection();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager)_bars).TrackViewState();
                    }
                }

                return _bars;
            }
        }

        public string ColumnNameField { get; set; }
        public string ColumnValueField { get; set; }
        public string ColumnOnClickField { get; set; }
        public string ColumnOnClickFormat { get; set; }
        public string ColumnValueFormat { get; set; }
        public string ColumnValueTextField { get; set; }
        public bool ShowPercentage { get; set; }

        #endregion       

        #region Methods

        private double GetBarPercent(int value)
        {
            if (value < _yAxis.Min || value > _yAxis.Max)
            {
                throw new InvalidDataException("Bar.value must be between Min and Max");
            }
            return (value - _yAxis.Min) / (double)(_yAxis.Max - _yAxis.Min) * 100;
        }

        private static double CssPctRange(double pct)
        {
            if (pct == 0)
            {
                pct = 0.0;
            }
            if (pct == 100)
            {
                pct = 99.99;
            }
            return pct;
        }

        private static string StringToCssClass(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            
            int i;
            if (int.TryParse(s.Substring(0,1), out i))
            {
                s = "num_" + s;
            }
            
            return s.Replace(" ", "_").ToLower();
        }

        #endregion

        #region Rendering

        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }

        protected override string TagName
        {
            get
            {
                return "div";
            }
        }

        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            if (!string.IsNullOrEmpty(CssClass))
            {
                CssClass += " ";
            }

            CssClass += SR.CssClass;

            base.AddAttributesToRender(writer);
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            RenderTable(writer);
        }

        private void RenderTable(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.TableCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Table);
            RenderTableFoot(writer);
            RenderTableBody(writer);
            writer.RenderEndTag(); // Table
        }

        private void RenderTableBody(HtmlTextWriter writer)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Tbody);
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            if (Direction.Equals(BarChartDirection.TopToBottom))
            {
                foreach (Bar bar in Bars)
                {
                    string cssClass = StringToCssClass(bar.Label.Text);
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    RenderBar(writer, bar);
                    writer.RenderEndTag(); // Td
                }
                writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.AxisCssClass);
                writer.RenderBeginTag(HtmlTextWriterTag.Th);
                RenderAxis(writer);
                writer.RenderEndTag(); // Th
            }            

            writer.RenderEndTag(); // Tr
            writer.RenderEndTag(); // Tbody 
        }

        private void RenderTableFoot(HtmlTextWriter writer)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Tfoot);
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            if (Direction.Equals(BarChartDirection.TopToBottom))
            {
                foreach (Bar bar in Bars)
                {
                    string cssClass = bar.CssClass.Equals("bar") ? string.Empty : bar.CssClass.Replace(" bar", string.Empty);
                    cssClass += " " + StringToCssClass(bar.Label.Text);
                    if (!string.IsNullOrEmpty(cssClass))
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass.Trim());
                    }
                    writer.RenderBeginTag(HtmlTextWriterTag.Th);
                    writer.Write(bar.Label.Text);
                    writer.RenderEndTag(); // Th   
                }
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(_yAxis.Label);
                writer.RenderEndTag(); // Th
            }            

            writer.RenderEndTag(); // Tr
            writer.RenderEndTag(); // TFoot
        }        

        private void RenderBar(HtmlTextWriter writer, Bar bar)
        {
            double pct = CssPctRange(GetBarPercent(bar.Value));

            bar.Percentage = pct;
           
            bar.RenderControl(writer);
        }

        private void RenderAxis(HtmlTextWriter writer)
        {
            if (_yAxis.GridLines.Count > 0)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Ul);
                double step = CssPctRange((1/(double)_yAxis.GridLines.Count)*100);
                double pct = 0.0;
                foreach (GridLine gridLine in _yAxis.GridLines)
                {                    
                    if (Direction.Equals(BarChartDirection.TopToBottom))
                    {
                        writer.AddStyleAttribute("top", Unit.Percentage(pct).ToString());
                        gridLine.AddAttributesToRender(writer);
                    }
                    writer.RenderBeginTag(HtmlTextWriterTag.Li);
                    writer.RenderBeginTag(HtmlTextWriterTag.Span);
                    writer.Write(gridLine.Value);
                    writer.RenderEndTag(); // Span
                    writer.RenderEndTag(); // Li
                    pct += step;
                }
                writer.RenderEndTag(); // Ul
            }
        }

        #endregion

        #region DataBinding

        protected override int CreateChildControls(IEnumerable dataSource, bool dataBinding)
        {
            int count = 0;

            if (dataBinding && dataSource != null)
            {
                IEnumerator en = dataSource.GetEnumerator();

                Bars.Clear();
                Controls.Clear();

                while (en.MoveNext())
                {
                    DataRowView row = en.Current as DataRowView;
                    if (row != null)
                    {
                        Bar bar = new Bar();
                        int value;
                        bar.Value = int.TryParse(DataBinder.Eval(row, ColumnValueField, "{0}"), out value) ? value : 0;
                        bar.ValueLable.Format = (string.IsNullOrEmpty(ColumnValueFormat)) ? "{0}" : ColumnValueFormat;
                        if (!string.IsNullOrEmpty(ColumnValueTextField))
                        {
                            bar.ValueLable.Text = DataBinder.Eval(row, ColumnValueTextField, "{0}");
                        }
                        bar.Label.Text = DataBinder.Eval(row, ColumnNameField, "{0}");
                        bar.ShowPercentage = ShowPercentage;

                        if (!string.IsNullOrEmpty(ColumnOnClickField))
                        {
                            string format = string.IsNullOrEmpty(ColumnOnClickFormat)
                                                ? "{0}"
                                                : ColumnOnClickFormat;
                            bar.OnClientClick = string.Format(format, row[ColumnOnClickField]);
                        }

                        if (bar.Value > _yAxis.Max)
                        {
                            _yAxis.Max = bar.Value;
                        }
                        if (bar.Value < _yAxis.Min)
                        {
                            _yAxis.Min = bar.Value;
                        }

                        ((ICollection<Bar>) Bars).Add(bar);
                        Controls.Add(bar);
                        count++;
                    }
                }

                _yAxis.Initialize();
            }

            return count;
        }

        #endregion

        #region StateManagement

        protected override object SaveViewState()
        {
            object[] state = new object[2];
            state[0] = ((IStateManager) Bars).SaveViewState();
            state[1] = (_yAxis != null) ? ((IStateManager) _yAxis).SaveViewState() : null;

            return new Triplet(base.SaveViewState(), state[0], state[1]);
        }

        protected override void LoadViewState(object savedState)
        {
            Triplet stateTriplet = savedState as Triplet;
            if (stateTriplet != null)
            {
                base.LoadViewState(stateTriplet.First);
                if (stateTriplet.Second != null)
                {
                    ((IStateManager)Bars).LoadViewState(stateTriplet.Second); 
                }
                if (stateTriplet.Third != null)
                {
                    ((IStateManager)_yAxis).LoadViewState(stateTriplet.Third); 
                }
            }
            else
            {
                base.LoadViewState(savedState);
            }
        }

        #endregion

        #region InnerClasses

        private static class SR
        {
            public const string CssClass = "bar_chart";
            public const string TableCssClass = "chart_table";
            public const string AxisCssClass = "axis";
        }

        #endregion
    }
}