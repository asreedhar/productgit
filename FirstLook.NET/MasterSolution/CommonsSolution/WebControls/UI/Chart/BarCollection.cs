﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;

namespace FirstLook.Common.WebControls.UI.Chart
{
    public class BarCollection : StateManagedCollection, ICollection<Bar>
    {
        private static readonly Type[] knownTypes = new Type[]
			{
				typeof (Bar)
			};

        protected override object CreateKnownType(int index)
        {
            switch (index)
            {
                case 0:
                    return new Bar();
            }
            throw new ArgumentOutOfRangeException("index");
        }

        protected override Type[] GetKnownTypes()
        {
            return knownTypes;
        }

        protected override void SetDirtyObject(object o)
        {
            
        }

        IEnumerator<Bar> IEnumerable<Bar>.GetEnumerator()
        {
            List<Bar> bars = new List<Bar>();
            foreach (object item in (IList) this)
            {
                bars.Add((Bar)item);
            }
            return bars.GetEnumerator();
        }

        void ICollection<Bar>.Add(Bar item)
        {
            ((IList) this).Add(item);
        }

        bool ICollection<Bar>.Contains(Bar item)
        {
            return ((IList)this).Contains(item);
        }

        void ICollection<Bar>.CopyTo(Bar[] array, int arrayIndex)
        {
            ((IList)this).CopyTo(array, arrayIndex);
        }

        bool ICollection<Bar>.Remove(Bar item)
        {
            if (((IList)this).Contains(item))
            {
                ((IList)this).Remove(item);
                return true;
            }
            return false;
        }

        public bool IsReadOnly
        {
            get { return true; }
        }
    }
}
