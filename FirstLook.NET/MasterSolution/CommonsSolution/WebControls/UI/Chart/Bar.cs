using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstLook.Common.WebControls.UI.Chart
{
    public class Bar : WebControl, IStateManager
    {
        #region Properties

        private Label _label;
        private Label _valueLabel;

        public string OnClientClick { get; set; }

        public string Format { get; set; }

        public int Value { get; internal set; }

        public Label Label
        {
            get
            {
                if (_label == null)
                {
                    _label = new Label();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager)_label).TrackViewState();
                    }
                }
                return _label;
            }
        }

        public Label ValueLable
        {
            get
            {
                if (_valueLabel == null)
                {
                    _valueLabel = new Label(Value.ToString(), Format);
                    if (IsTrackingViewState)
                    {
                        ((IStateManager)_label).TrackViewState();
                    }
                }
                return _valueLabel;
            }
        }

        public bool ShowPercentage { get; set; }

        internal double Percentage { get; set; }

        #endregion        

        #region Render

        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }

        protected override string TagName
        {
            get
            {
                return "div";
            }
        }

        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            if (!CssClass.Equals("bar"))
            {
                CssClass += " bar";
            }

            writer.AddStyleAttribute(HtmlTextWriterStyle.Height, Unit.Percentage(Percentage).ToString());

            AddClientClickToRender(writer);

            base.AddAttributesToRender(writer);
        }

        private void AddClientClickToRender(HtmlTextWriter writer)
        {
            if (!string.IsNullOrEmpty(OnClientClick))
            {
                writer.AddStyleAttribute(HtmlTextWriterStyle.Cursor, "pointer");
                writer.AddAttribute(HtmlTextWriterAttribute.Onclick, OnClientClick);
            }
        }

        protected override void RenderChildren(HtmlTextWriter writer)
        {
            AddClientClickToRender(writer);
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "label");
            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            writer.Write(ValueLable.Text);
            if (ShowPercentage && Percentage > 1)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "percentage");
                writer.RenderBeginTag(HtmlTextWriterTag.Span);
                writer.Write(string.Format("{0}%", (int)Percentage));
                writer.RenderEndTag();
            }
            writer.RenderEndTag(); // Span            
        }

        protected override void CreateChildControls()
        {
            base.Controls.Clear();

            Label.ID = ID + IdSeparator + "label";
            ValueLable.ID = ID + IdSeparator + "valueLabel";

            Controls.Add(Label);
            Controls.Add(ValueLable);

            base.CreateChildControls();
        }

        #endregion        

        #region Implementation of IStateManager

        protected override void LoadViewState(object state)
        {

            Pair savedState = state as Pair;
            if (savedState != null)
            {
                base.LoadViewState(savedState.First);

                object[] customState = savedState.Second as object[];
                int idx = 0;

                if (customState != null)
                {
                    Value = (int)customState[idx++];
                    Format = (string)customState[idx++]; 
                    OnClientClick = (string)customState[idx++];
                    Percentage = (double)customState[idx++]; 
                    ShowPercentage = (bool) customState[idx++];
                    ((IStateManager)Label).LoadViewState(customState[idx++]); 
                    ((IStateManager)ValueLable).LoadViewState(customState[idx]); 
                }
            }
        }

        protected override object SaveViewState()
        {
            object[] state = new object[7];
            int idx = 0;

            state[idx++] = Value;
            state[idx++] = Format;
            state[idx++] = OnClientClick;
            state[idx++] = Percentage;
            state[idx++] = ShowPercentage;
            state[idx++] = ((IStateManager)Label).SaveViewState();
            state[idx] = ((IStateManager)ValueLable).SaveViewState();

            return new Pair(base.SaveViewState(), state);
        }

        #endregion

        #region IStateManager Members

        bool IStateManager.IsTrackingViewState
        {
            get { return IsTrackingViewState; }
        }

        void IStateManager.LoadViewState(object state)
        {
            LoadViewState(state);
        }

        object IStateManager.SaveViewState()
        {
            return SaveViewState();
        }

        void IStateManager.TrackViewState()
        {
            TrackViewState();
        }

        #endregion
    }
}
