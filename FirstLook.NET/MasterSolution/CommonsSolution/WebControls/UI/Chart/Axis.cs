﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstLook.Common.WebControls.UI.Chart
{
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class Axis : IStateManager
    {
        #region Properties

        private int _min;
        private int _max;
        private int _steps;
        private Unit _gridLineWidth = Unit.Pixel(1);
        private BorderStyle _gridLineStyle = BorderStyle.Solid;
        private Color _gridLineColor = Color.Black;
        private Collection<GridLine> _gridLines;

        [NotifyParentProperty(true)]
        public int Min
        {
            get { return _min; }
            set
            {
                if (_min > _max)
                {
                    throw new NotSupportedException("Must be smaller than max");
                }
                if (_min % Modulo != 0)
                {
                    _min = ((_min / Modulo)) * Modulo;
                }
                if (_min !=- value)
                {
                    _min = value; 
                }
            }
        }

        [NotifyParentProperty(true)]
        public int Max
        {
            get { return _max; }
            set
            {
                if (value < _min)
                {
                    throw new NotSupportedException("Must be greater than min");
                }
                if (value % Modulo != 0)
                {
                    value = ((value/Modulo) + 1)*Modulo;
                }
                if (_max != value)
                {
                    _max = value; 
                }
            }
        }

        [NotifyParentProperty(true)]
        public int Steps
        {
            get { return _steps; }
            set
            {
                if (_steps < 0)
                {
                    throw new NotSupportedException("Must be non-negative");
                }
                if (_steps != value)
                {
                    _steps = value; 
                }
            }
        }

        [NotifyParentProperty(true)]
        public string LabelFormat { get; set; }

        [NotifyParentProperty(true)]
        public string Label { get; set; }

        private int _modulo = 1;

        [NotifyParentProperty(true)]
        public int Modulo { get { return _modulo; } set { _modulo = value; } }

        internal Collection<GridLine> GridLines
        {
            get
            {
                if (_gridLines == null)
                {
                    _gridLines = new Collection<GridLine>();
                }
                return _gridLines;
            }
        }

        public Unit GridLineWidth
        {
            get { return _gridLineWidth; }
            set { _gridLineWidth = value; }
        }

        public BorderStyle GridLineStyle
        {
            get { return _gridLineStyle; }
            set { _gridLineStyle = value; }
        }

        public Color GridLineColor
        {
            get { return _gridLineColor; }
            set { _gridLineColor = value; }
        }

        #endregion

        internal void Initialize()
        {
            GridLines.Clear();
            int prevValue = 0;
            for (int i = _steps; i >= 1; i--)
            {
                int value = (int) (((_max - _min)*(i/(double) _steps)) + _min);
                if (value % Modulo != 0)
                {
                    value = ((value/Modulo) + 1)*Modulo;
                }
                if (prevValue == value)
                {
                    continue;
                }
                prevValue = value;
                GridLines.Add(new GridLine(value, _gridLineWidth, _gridLineStyle, _gridLineColor));
            }
        }

        public void LoadViewState(object state)
        {
            object[] savedState = state as object[];
            if (savedState != null)
            {
                int idx = 0;
                _min = (int)savedState[idx++];
                _max = (int)savedState[idx++];
                _steps = (int)savedState[idx++];
                Label = (string)savedState[idx++];
                LabelFormat = (string)savedState[idx++];
                Modulo = (int)savedState[idx];
            }
            Initialize();
        }

        public object SaveViewState()
        {
            object[] state = new object[8];
            int idx = 0;

            state[idx++] = _min;
            state[idx++] = _max;
            state[idx++] = _steps;
            state[idx++] = Label;
            state[idx++] = LabelFormat;
            state[idx++] = Modulo;

            return state;
        }

        public void TrackViewState()
        {
            _isTrackingViewState = true;
        }

        private bool _isTrackingViewState; 
        public bool IsTrackingViewState
        {
            get { return _isTrackingViewState; }
        }
    }
}
