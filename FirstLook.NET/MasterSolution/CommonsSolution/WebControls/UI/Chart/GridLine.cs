﻿using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstLook.Common.WebControls.UI.Chart
{
    internal class GridLine
    {
        public GridLine(int value, Unit width, BorderStyle borderStyle, Color color)
        {
            _style = new Style();
            _style.BorderWidth = width;
            _style.BorderStyle = borderStyle;
            _style.BorderColor = color;
            Value = value;
        }

        #region Properties

        private Style _style;

        private Style Style
        {
            get
            {
                if (_style == null)
                {
                    _style = new Style();
                }

                return _style;
            }
        }

        public Unit Width
        {
            get
            {
                return Style.BorderWidth;
            }
            set
            {
                if (Style.BorderWidth != value)
                {
                    Style.BorderWidth = value; 
                }
            }
        }

        public Color Color
        {
            get
            {
                return Style.BorderColor;
            }
            set
            {
                if (Style.BorderColor != value)
                {
                    Style.BorderColor = value;
                }
            }
        }

        public BorderStyle LineStyle
        {
            get
            {
                return Style.BorderStyle;
            }
            set
            {
                if (Style.BorderStyle != value)
                {
                    Style.BorderStyle = value;
                }
            }
        }

        public int Value { get; set; }

        #endregion

        public void AddAttributesToRender(HtmlTextWriter writer)
        {
            string value = Width + " " + LineStyle + " #" + Color.R.ToString("X") + Color.G.ToString("X") + Color.B.ToString("X"); 
            writer.AddStyleAttribute("border-top", value);
        }
    }
}
