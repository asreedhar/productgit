﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Common.WebControls.UI.Chart
{
    public class ChartBarDataBoundEventArgs : EventArgs
    {
        public Bar Bar { get; set; }
    }
}
