﻿using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstLook.Common.WebControls.UI.Chart
{
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class Label : WebControl, IStateManager
    {
        public Label(string text, string format)
        {
            _text = text;
            Format = format;
        }

        public Label()
        {
        }

        #region Properites

        private string _text;

        public string Text
        {
            get
            {
                if (!string.IsNullOrEmpty(Format))
                {
                    return string.Format(Format, _text);
                }
                return _text;
            }
            set { _text = value; }
        }

        public string Format { get; internal set; }

        #endregion

        #region Render

        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Span;
            }
        }

        protected override string TagName
        {
            get
            {
                return"span";
            }
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            writer.Write(Text);
        }

        #endregion

        #region Implementation of IStateManager

        protected override void LoadViewState(object state)
        {
            Pair statePair = state as Pair;
            if (statePair != null)
            {
                _text = (string) statePair.First;
                Format = (string) statePair.Second;
            }
        }

        protected override object SaveViewState()
        {
            return new Pair(_text, Format);
        }        

        #endregion

        #region IStateManager Members

        bool IStateManager.IsTrackingViewState
        {
            get { return IsTrackingViewState; }
        }

        void IStateManager.LoadViewState(object state)
        {
            LoadViewState(state);
        }

        object IStateManager.SaveViewState()
        {
            return SaveViewState();
        }

        void IStateManager.TrackViewState()
        {
            TrackViewState();
        }

        #endregion
    }
}
