namespace FirstLook.Common.WebControls.UI
{
    public enum DialogButtonType
    {
        Button,
        Image,
        Input,
        Anchor
    }
}
