using System;
using System.Web.UI;

namespace FirstLook.Common.WebControls.UI
{
    public class TabPanelCollection : ControlCollection
    {
        public TabPanelCollection(Control owner) : base(owner)
        {
        }

        public override void Add(Control v)
        {
            if (!(v is TabPanel))
            {
                throw new ArgumentException("TabPaneCollection must contain TabPane");
            }
            base.Add(v);
        }

        public override void AddAt(int index, Control v)
        {
            if (!(v is TabPanel))
            {
                throw new ArgumentException("TabPaneCollection must contain TabPane");
            }
            base.AddAt(index, v);
        }

        // Properties
        public new TabPanel this[int i]
        {
            get { return (TabPanel) base[i]; }
        }
    }
}