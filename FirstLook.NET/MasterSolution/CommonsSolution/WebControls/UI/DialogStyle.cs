using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstLook.Common.WebControls.UI
{
    internal class DialogStyle : Style
    {
        public DialogStyle(StateBag bag)
            : base(bag)
        {
        }

        [NotifyParentProperty(true), DefaultValue(typeof(Unit), ""), Description("Style:Position:Top"), Category("Layout")]
        public Unit Top
        {
            get
            {
                object value = ViewState["Top"];
                if (value == null)
                    return Unit.Empty;
                return (Unit)value;
            }
            set
            {
                if (value.Value < 0.0)
                {
                    throw new ArgumentOutOfRangeException("value", "Style_InvalidTopPosition");
                }
                if (!Top.Equals(value))
                {
                    ViewState["Top"] = value;
                }
            }
        }

        [NotifyParentProperty(true), DefaultValue(typeof(Unit), ""), Description("Style:Position:Top"), Category("Layout")]
        public Unit Left
        {
            get
            {
                object value = ViewState["Left"];
                if (value == null)
                    return Unit.Empty;
                return (Unit)value;
            }
            set
            {
                if (value.Value < 0.0)
                {
                    throw new ArgumentOutOfRangeException("value", "Style_InvalidLeftPosition");
                }
                if (!Left.Equals(value))
                {
                    ViewState["Left"] = value;
                }
            }
        }
    }
}
