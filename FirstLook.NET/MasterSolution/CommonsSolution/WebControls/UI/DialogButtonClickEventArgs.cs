using System;

namespace FirstLook.Common.WebControls.UI
{
    public class DialogButtonClickEventArgs : EventArgs
    {
        private readonly DialogButtonCommand button;

        public DialogButtonClickEventArgs(DialogButtonCommand button)
        {
            this.button = button;
        }

        public DialogButtonCommand Button
        {
            get { return button; }
        }
    }
}