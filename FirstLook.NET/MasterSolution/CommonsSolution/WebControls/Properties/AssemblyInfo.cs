﻿using System.Reflection;
using System.Web.UI;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("WebControls")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyProduct("WebControls")]

// Assign a tag prefix so people 'choose' the same one each time.
[assembly: TagPrefix("FirstLook.Common.WebControls", "cwc")]

// Common JS Files

[assembly: WebResource("FirstLook.Common.WebControls.Timer.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Timer.debug.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.DragDropScripts.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.DragDropScripts.debug.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Animations.Animation.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Animations.PropertyAnimation.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Animations.InterpolatedAnimation.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Animations.LengthAnimation.js", "text/javascript")]