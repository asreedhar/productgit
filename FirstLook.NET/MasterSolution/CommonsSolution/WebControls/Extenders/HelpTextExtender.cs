using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

[assembly: WebResource("FirstLook.Common.WebControls.Extenders.HelpTextExtender.debug.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Extenders.HelpTextExtender.js", "text/javascript")]

namespace FirstLook.Common.WebControls.Extenders
{
    [TargetControlType(typeof(Control))]
    public class HelpTextExtender : ExtenderControl
    {
        #region Properties

        private bool helperControlHidden = true;
        private bool showHelpAtMouseY = false;
        private bool showHelpAtMouseX = false;

        private string helperControlID;

        private HelpTextEventTrigger showHelperTextEventTrigger = HelpTextEventTrigger.None;
        private HelpTextEventTrigger hideHelperTextEventTrigger = HelpTextEventTrigger.None;

        public bool HelperControlHidden
        {
            get { return helperControlHidden; }
            set { helperControlHidden = value; }
        }

        public bool ShowHelpAtMouseY
        {
            get { return showHelpAtMouseY; }
            set { showHelpAtMouseY = value; }
        }

        public bool ShowHelpAtMouseX
        {
            get { return showHelpAtMouseX; }
            set { showHelpAtMouseX = value; }
        }

        public string HelperControlID
        {
            get { return helperControlID; }
            set { helperControlID = value; }
        }

        public HelpTextEventTrigger ShowHelperTextEventTrigger
        {
            get { return showHelperTextEventTrigger; }
            set { showHelperTextEventTrigger = value; }
        }

        public HelpTextEventTrigger HideHelperTextEventTrigger
        {
            get { return hideHelperTextEventTrigger; }
            set { hideHelperTextEventTrigger = value; }
        }

        protected Control HelperControl
        {
            get
            {
                return FindControl(HelperControlID);
            }
        }

        #endregion

        protected override void Render(HtmlTextWriter writer)
        {
            WebControl webControl = HelperControl as WebControl;
            HtmlGenericControl htmlGenericControl = HelperControl as HtmlGenericControl;

            if (webControl  != null && HelperControlHidden)
            {
                webControl.Style.Add(HtmlTextWriterStyle.Display, "none");
            }
            else if (htmlGenericControl != null && helperControlHidden)
            {
                htmlGenericControl.Style.Add(HtmlTextWriterStyle.Display, "none");
            }

            base.Render(writer);
        }

        #region Extender Control Methods

        protected override IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
        {
            IList<ScriptDescriptor> scriptDescriptors = new List<ScriptDescriptor>();

            if (HelperControl.Visible)
            {
                ScriptBehaviorDescriptor helpTextExtender = new ScriptBehaviorDescriptor("FirstLook.Common.WebControls.Extenders.HelpTextExtender", targetControl.ClientID);

                helpTextExtender.AddElementProperty("helper_element", HelperControl.ClientID);
                helpTextExtender.AddProperty("show_help_at_mouse_x", ShowHelpAtMouseX);
                helpTextExtender.AddProperty("show_help_at_mouse_y", ShowHelpAtMouseY);
                helpTextExtender.AddProperty("helper_element_hidden", HelperControlHidden);

                helpTextExtender.AddProperty("show_helper_text_event", showHelperTextEventTrigger);
                helpTextExtender.AddProperty("hide_helper_text_event", hideHelperTextEventTrigger);

                scriptDescriptors.Add(helpTextExtender); 
            }

            return scriptDescriptors;
        }

        protected override IEnumerable<ScriptReference> GetScriptReferences()
        {
            IList<ScriptReference> scriptReferences = new List<ScriptReference>();

            if (HelperControl.Visible)
            {
                ScriptReference helpText =
                           new ScriptReference("FirstLook.Common.WebControls.Extenders.HelpTextExtender.debug.js",
                                               "FirstLook.Common.WebControls");

                scriptReferences.Add(helpText);

            }
            return scriptReferences;
        } 

        #endregion

        #region IStateManager Methods

        protected static HelpTextEventTrigger IntToHelpTextEventTrigger(int idx)
        {
            return (HelpTextEventTrigger)Enum.ToObject(typeof(HelpTextEventTrigger), idx);
        }

        protected override void LoadViewState(object savedState)
        {
            Pair pair = (Pair)savedState;

            base.LoadViewState(pair.First);

            if (pair.Second != null)
            {
                object[] values = (object[])pair.Second;

                /* ============================= */
                /* = Control ViewState Variables */
                /* ============================= */
                int idx = 0;
                helperControlHidden = (bool) values[idx++];
                showHelpAtMouseX = (bool) values[idx++];
                showHelpAtMouseY = (bool)values[idx++];
                helperControlID = (string) values[idx++];
                TargetControlID = (string) values[idx++];

                showHelperTextEventTrigger = IntToHelpTextEventTrigger((int) values[idx++]);
                hideHelperTextEventTrigger = IntToHelpTextEventTrigger((int) values[idx]);
            }
        }

        protected override object SaveViewState()
        {
            object[] values = new object[7];
            
            /* ============================= */
            /* = Control ViewState Variables */
            /* ============================= */
            int idx = 0;
            values[idx++] = helperControlHidden;
            values[idx++] = showHelpAtMouseX;
            values[idx++] = showHelpAtMouseY;
            values[idx++] = helperControlID;
            values[idx++] = TargetControlID;

            values[idx++] = (int)showHelperTextEventTrigger;
            values[idx]   = (int)hideHelperTextEventTrigger;

            return new Pair(base.SaveViewState(), values);
        } 

        #endregion
    }
}
