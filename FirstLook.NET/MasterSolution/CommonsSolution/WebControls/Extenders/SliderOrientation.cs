namespace FirstLook.Common.WebControls.Extenders
{
    public enum SliderOrientation
    {
        Horizontal,
        Vertical
    }
}