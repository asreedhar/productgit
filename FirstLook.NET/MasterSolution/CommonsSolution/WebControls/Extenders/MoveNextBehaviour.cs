using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("FirstLook.Common.WebControls.Extenders.MoveNextBehaviour.js", "text/javascript")]

namespace FirstLook.Common.WebControls.Extenders
{
    [TargetControlType(typeof(TextBox))]
    public class MoveNextBehaviour : ExtenderControl
    {
        protected override IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
        {
            return new ScriptDescriptor[] { new ScriptBehaviorDescriptor("FirstLook.Common.WebControls.Extenders.MoveNextBehaviour", targetControl.ClientID) };
        }

        protected override IEnumerable<ScriptReference> GetScriptReferences()
        {
            return new[] { new ScriptReference("FirstLook.Common.WebControls.Extenders.MoveNextBehaviour.js", "FirstLook.Common.WebControls") };
        }
    }
}
