
Type.registerNamespace('FirstLook.Common.WebControls.Extenders');

FirstLook.Common.WebControls.Extenders.LinkButtonExtender = function FirstLook$Common$WebControls$Extenders$LinkButtonExtender (element)
{
    FirstLook.Common.WebControls.Extenders.LinkButtonExtender.initializeBase(this,[element]);
    this._removeButtonText = false;
}

function FirstLook$Common$WebControls$Extenders$LinkButtonExtender$initialize()
{
    FirstLook.Common.WebControls.Extenders.LinkButtonExtender.callBaseMethod(this, 'initialize');
    
    // get source and null its id
    var from = this.get_element();
    var id = from.id;
    from.id = null;
    
    // create new element
    var into = document.createElement('a');
    into.className = from.className;
    into.id = id;
    into.href = "javascript:__doPostBack('" + id + "','')";
    into.title = from.title;
    if (!this._removeButtonText) {
        into.appendChild(document.createTextNode(from.value));
    }
    
    // replace the button with the link
    from.parentNode.replaceChild(into, from);
}

function FirstLook$Common$WebControls$Extenders$LinkButtonExtender$get_removeButtonText()
{
    return this._removeButtonText;
}

function FirstLook$Common$WebControls$Extenders$LinkButtonExtender$set_removeButtonText(value)
{
    this._removeButtonText = value;
}

FirstLook.Common.WebControls.Extenders.LinkButtonExtender.prototype =
{
    initialize: FirstLook$Common$WebControls$Extenders$LinkButtonExtender$initialize,
    get_removeButtonText: FirstLook$Common$WebControls$Extenders$LinkButtonExtender$get_removeButtonText,
    set_removeButtonText: FirstLook$Common$WebControls$Extenders$LinkButtonExtender$set_removeButtonText
}

FirstLook.Common.WebControls.Extenders.LinkButtonExtender.registerClass('FirstLook.Common.WebControls.Extenders.LinkButtonExtender', Sys.UI.Behavior);
