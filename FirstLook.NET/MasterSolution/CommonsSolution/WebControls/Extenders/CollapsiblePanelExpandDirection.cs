namespace FirstLook.Common.WebControls.Extenders
{
    public enum CollapsiblePanelExpandDirection
    {
        Horizontal = 0,
        Vertical = 1,
    }
}
