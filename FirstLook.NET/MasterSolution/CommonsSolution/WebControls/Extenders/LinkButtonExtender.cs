using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("FirstLook.Common.WebControls.Extenders.LinkButtonExtender.js", "text/javascript")]

namespace FirstLook.Common.WebControls.Extenders
{
    [TargetControlType(typeof(Button))]
    public class LinkButtonExtender : ExtenderControl
    {
        private bool removeButtonText;

        public bool RemoveButtonText
        {
            get { return removeButtonText; }
            set { removeButtonText = value; }
        }

        protected override IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
        {
            ScriptBehaviorDescriptor descriptor = new ScriptBehaviorDescriptor("FirstLook.Common.WebControls.Extenders.LinkButtonExtender", targetControl.ClientID);
            descriptor.AddProperty("removeButtonText", RemoveButtonText);
            return new ScriptDescriptor[] { descriptor };
        }

        protected override IEnumerable<ScriptReference> GetScriptReferences()
        {
            return new ScriptReference[] { new ScriptReference("FirstLook.Common.WebControls.Extenders.LinkButtonExtender.js", "FirstLook.Common.WebControls") };
        }
    }
}
