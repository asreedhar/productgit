if (!!Sys) {
	Type.registerNamespace('FirstLook.Common.WebControls.Extenders');
	
	FirstLook.Common.WebControls.Extenders.TextBoxOverLabelExtender = function(element) {
		FirstLook.Common.WebControls.Extenders.TextBoxOverLabelExtender.initializeBase(this,[element]);
	};
	FirstLook.Common.WebControls.Extenders.TextBoxOverLabelExtender.prototype = {
		initialize: function() {
			if (!!!this.get_applyClass()) {
				this.set_applyClass("overlabel-apply");
			};
			FirstLook.Common.WebControls.Extenders.TextBoxOverLabelExtender.callBaseMethod(this, 'initialize');
		},
		updated: function() {
			var el = this.get_element(),
			label = this.get_label();
			this.addApplyClassName();
			if (!this.isEmpty()) {
				this.hideLabel();
			};
			this.addHandler(el, "focus", this.getDelegate("onFocus"));
			this.addHandler(el, "blur", this.getDelegate("onBlur"));
			this.addHandler(label, "click", this.getDelegate("onLabelClick"));
			FirstLook.Common.WebControls.Extenders.TextBoxOverLabelExtender.callBaseMethod(this, 'updated');
		},
		displose: function() {
			this.removeApplyClassName();
			this.removeHandlers();
			this.clearDelegates();
			FirstLook.Common.WebControls.Extenders.TextBoxOverLabelExtender.callBaseMethod(this, 'displose');
		},
		addApplyClassName: function() {
			jQuery(this.get_label()).addClass(this.get_applyClass());
		},
		removeApplyClassName: function() {
			jQuery(this.get_label()).removeClass(this.get_applyClass());
		},
		hideLabel: function() {
			jQuery(this.get_label()).hide();
		},
		showLabel: function() {
			jQuery(this.get_label()).show();
		},
		isEmpty: function() {
			return this.get_element().value === '';
		},
		// ==========
		// = Events =
		// ==========
		onFocus: function(evt) {
			this.hideLabel();
		},
		onBlur: function(evt) {
			if (this.isEmpty()) {
				this.showLabel();
			};
		},
		onLabelClick: function(evt) {
			this.hideLabel();
			this.get_element().focus();
		}
	}
	FirstLook.Utility.createGetterSetters(FirstLook.Common.WebControls.Extenders.TextBoxOverLabelExtender, ["label","applyClass"]);
	FirstLook.Common.WebControls.Extenders.TextBoxOverLabelExtender.registerClass('FirstLook.Common.WebControls.Extenders.TextBoxOverLabelExtender', Sys.UI.Behavior);
	
	Sys.Application.notifyScriptLoaded();
};