Type.registerNamespace('FirstLook.Common.WebControls.Extenders');

FirstLook.Common.WebControls.Extenders.HasJavascriptControlBehavior = function(element) {
    FirstLook.Common.WebControls.Extenders.HasJavascriptControlBehavior.initializeBase(this, [element]);
    this._clientStateField;
};
FirstLook.Common.WebControls.Extenders.HasJavascriptControlBehavior.prototype = {
    initialize: function() {
        FirstLook.Common.WebControls.Extenders.HasJavascriptControlBehavior.callBaseMethod(this, "initialize");
    },
    get_hasJavascript: function() {
        return true;
    },
    set_hasJavascript: function() {
    	this.set_clientState();
    },
    get_clientStateField: function() {
        return this._clientStateField;
    },
    set_clientStateField: function(value) {
        this._clientStateField = value;
    },
    get_clientState: function() {
        if (this.get_clientStateField()) {
            var input = this.get_clientStateField();
            if (input) {
                if (input.value.length > 4 && typeof(input.value) == 'string') {
                    return eval('(' + input.value + ')');
                }
                return {};
            }
        }
        return null;
    },
    set_clientState: function() {
    	var input = this.get_clientStateField();
    	if (input) {
    		var value = this.get_clientState();
    		value.hasJavascript = true;
    		
    		var results = [];
    		for (var prop in value) {
    			if (prop != undefined) {
    				results.push(prop + ": "+ value[prop]);
    			};    			
    		}
    		input.value = "{"+results.join(", ")+"}";
    	};
    }
};

FirstLook.Common.WebControls.Extenders.HasJavascriptControlBehavior.registerClass('FirstLook.Common.WebControls.Extenders.HasJavascriptControlBehavior', Sys.UI.Behavior);

Sys.Application.notifyScriptLoaded();