using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("FirstLook.Common.WebControls.Extenders.ExpandingTextarea.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Extenders.ExpandingTextarea.debug.js", "text/javascript")]


namespace FirstLook.Common.WebControls.Extenders
{
    [TargetControlType(typeof(TextBox))]
    public class ExpandingTextarea: ExtenderControl
    {
        #region ExtederControls
        protected override IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
        {
            throw new NotImplementedException();
        }

        protected override IEnumerable<ScriptReference> GetScriptReferences()
        {
            throw new NotImplementedException();
        } 
        #endregion
    }
}
