using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("FirstLook.Common.WebControls.Extenders.ClickableControlBehavior.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Extenders.ClickableControlBehavior.debug.js", "text/javascript")]

namespace FirstLook.Common.WebControls.Extenders
{
    public class ClickableControlExtender : ExtenderControl
    {
        [DefaultValue(""),
         Description("Button to which clicks are delegated"),
         Category("Behavior"),
         IDReferenceProperty(typeof(IButtonControl))]
        public string ButtonID
        {
            get
            {
                string value = (string)ViewState["ButtonID"];
                if (string.IsNullOrEmpty(value))
                    return string.Empty;
                return value;
            }
            set
            {
                if (string.Equals(ButtonID, value, StringComparison.Ordinal))
                {
                    ViewState["ButtonID"] = value;
                }
            }
        }

        protected override IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
        {
            ScriptBehaviorDescriptor descriptor = new ScriptBehaviorDescriptor("FirstLook.Common.WebControls.Extenders.ClickableControlBehavior", targetControl.ClientID);
            descriptor.AddElementProperty("buttonID", FindControl(ButtonID).ClientID);
            return new ScriptDescriptor[] { descriptor };
        }

        protected override IEnumerable<ScriptReference> GetScriptReferences()
        {
            return new ScriptReference[] { new ScriptReference("FirstLook.Common.WebControls.Extenders.ClickableControlBehavior.js", "FirstLook.Common.WebControls") };
        }
    }
}
