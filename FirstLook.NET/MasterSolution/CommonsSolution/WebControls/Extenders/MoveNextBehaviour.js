
Type.registerNamespace('FirstLook.Common.WebControls.Extenders');

FirstLook.Common.WebControls.Extenders.MoveNextBehaviour = function FirstLook$Common$WebControls$Extenders$MoveNextBehaviour (element)
{
    FirstLook.Common.WebControls.Extenders.MoveNextBehaviour.initializeBase(this,[element]);
}

function FirstLook$Common$WebControls$Extenders$MoveNextBehaviour$initialize()
{
    FirstLook.Common.WebControls.Extenders.MoveNextBehaviour.callBaseMethod(this, 'initialize');
    this._keyDownDelegate = Function.createDelegate(this, this._keyDown);
    $addHandler(this.get_element(), 'keydown', this._keyDownDelegate);
}

function FirstLook$Common$WebControls$Extenders$MoveNextBehaviour$dispose()
{
    $removeHandler(this.get_element(), 'keydown', this._keyDownDelegate);
    delete this._keyDownDelegate;
    FirstLook.Common.WebControls.Extenders.MoveNextBehaviour.callBaseMethod(this, 'dispose');
}

function FirstLook$Common$WebControls$Extenders$MoveNextBehaviour$keyDown(e) {
    if (e.keyCode == Sys.UI.Key.enter) {
        try {
            var el = this.get_element();
            var els = el.form.elements;
            var bl = true;
            for (var i = 0, l = els.length; i < l; i++) {
                var eli = els[i].tabIndex;
                if (eli.tabIndex == el.tabIndex+1) {
                    eli.focus();
                    if (eli.type == "text") {
                        eli.select();
                    }
                    bl = false;
                    break;
                }
            }
            if (bl) {
                el.blur();
            }
            e.preventDefault();
        }
        catch (e) {
            // ignore stupid errors
        }
    }
}

FirstLook.Common.WebControls.Extenders.MoveNextBehaviour.prototype =
{
    initialize: FirstLook$Common$WebControls$Extenders$MoveNextBehaviour$initialize,
    dispose: FirstLook$Common$WebControls$Extenders$MoveNextBehaviour$dispose,
    _keyDown: FirstLook$Common$WebControls$Extenders$MoveNextBehaviour$keyDown
}

FirstLook.Common.WebControls.Extenders.MoveNextBehaviour.registerClass('FirstLook.Common.WebControls.Extenders.MoveNextBehaviour', Sys.UI.Behavior);
