if (!!Sys) {
	Type.registerNamespace("FirstLook.Common.WebControls.Extenders");
	
	FirstLook.Common.WebControls.Extenders.FadingUserMessageLabel = function(element) {
		FirstLook.Common.WebControls.Extenders.FadingUserMessageLabel.initializeBase(this, [element]);	
	};
	FirstLook.Common.WebControls.Extenders.FadingUserMessageLabel.prototype = {
		initialize: function () {
			FirstLook.Common.WebControls.Extenders.FadingUserMessageLabel.callBaseMethod(this, "initialize");
		},
		updated: function() {		
			this.show();
			FirstLook.Common.WebControls.Extenders.FadingUserMessageLabel.callBaseMethod(this, "updated");
		},
		dispose: function() {
            this.removeHandlers();
            this.clearDelegates();
            
			FirstLook.Common.WebControls.Extenders.FadingUserMessageLabel.callBaseMethod(this, "dispose");
		},
		bindEvents: function() {
			var binder = (function(context) {
				return function() {
					context.addHandler(document.body, "mousemove", context.getDelegate("onMouseMove"));
				};
			})(this);
			setTimeout(binder, 1000);
		},
		onMouseMove: function() {
			this.hide()
		},
		onKeyDown: function() {
			this.hide();
		},
		hide: function() {
			this.removeHandlers();
			var element = this.get_element();
			var hide_target = function () {
				if (jQuery) {
					jQuery(element).fadeOut(1000);
				} else {
					element.style.display = "none";
				}
			};
			setTimeout(hide_target, 750);
		},
		show: function() {
			this.bindEvents();
			if (jQuery) {
				jQuery(this.get_element()).fadeIn()
			} else {
				this.get_element().style.display = "";
			}
		}
	};
	
	FirstLook.Common.WebControls.Extenders.FadingUserMessageLabel.registerClass("FirstLook.Common.WebControls.Extenders.FadingUserMessageLabel", Sys.UI.Behavior);
	
	Sys.Application.notifyScriptLoaded();
};