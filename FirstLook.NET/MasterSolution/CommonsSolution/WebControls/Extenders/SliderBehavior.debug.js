// (c) Copyright Microsoft Corporation.
// This source is subject to the Microsoft Permissive License.
// See http://www.microsoft.com/resources/sharedsource/licensingbasics/sharedsourcelicenses.mspx.
// All other rights reserved.
Type.registerNamespace('AjaxControlToolkit');

//  _SliderDragDropManagerInternal
//  This is a DragDropManager that returns always a GenericDragDropManager instance.
//  It is used in the Slider control to prevent IE from displaying the drag/drop icons
//  when dragging the slider's handle.
//
AjaxControlToolkit._SliderDragDropManagerInternal = function() {
    AjaxControlToolkit._SliderDragDropManagerInternal.initializeBase(this);

    this._instance = null;
};
AjaxControlToolkit._SliderDragDropManagerInternal.prototype = {
    _getInstance: function() {
        this._instance = new AjaxControlToolkit.GenericDragDropManager();

        this._instance.initialize();
        this._instance.add_dragStart(Function.createDelegate(this, this._raiseDragStart));
        this._instance.add_dragStop(Function.createDelegate(this, this._raiseDragStop));

        return this._instance;
    }
};
AjaxControlToolkit._SliderDragDropManagerInternal.registerClass('AjaxControlToolkit._SliderDragDropManagerInternal', AjaxControlToolkit._DragDropManager);
AjaxControlToolkit.SliderDragDropManagerInternal = new AjaxControlToolkit._SliderDragDropManagerInternal();

// SliderOrientation
// Specifies the orientation of the slider.
//
AjaxControlToolkit.SliderOrientation = function() {

};
AjaxControlToolkit.SliderOrientation.prototype = {
    Horizontal: 0,
    Vertical: 1
};
AjaxControlToolkit.SliderOrientation.registerEnum('AjaxControlToolkit.SliderOrientation', false);

// The SliderBehavior upgrades a textbox to a graphical slider.
//
AjaxControlToolkit.SliderBehavior = function(element) {
    AjaxControlToolkit.SliderBehavior.initializeBase(this, [element]);

    // Members
    //
    this._minimum = 0;
    this._maximum = 100;
    this._value = null;
    this._steps = 0;
    this._decimals = 0;
    this._orientation = AjaxControlToolkit.SliderOrientation.Horizontal;
    this._railElement = null;
    this._railCssClass = null;
    this._isHorizontal = true;
    this._isUpdatingInternal = false;
    this._isInitializedInternal = false;
    this._enableHandleAnimation = false;
    this._handle = null;
    this._handleImage = null;
    this._handleAnimation = null;
    this._handleAnimationDuration = 0.1;
    this._handleImageUrl = null;
    this._handleCssClass = null;
    this._dragHandle = null;
    this._mouseupHandler = null;
    this._selectstartHandler = null;
    this._boundControlChangeHandler = null;
    this._boundControlKeyPressHandler = null;
    this._boundControlID = null;
    this._boundControl = null;
    this._boundControlFormat = null;
    this._justBoundControl = null;
    this._length = null;
    this._raiseChangeOnlyOnMouseUp = true;
    this._animationPending = false;
    this._selectstartPending = false;
    this._tooltipText = '';
    this._enabled = true;

    // Events
    //
    // sliderInitialized
    // valueChanged
    // slideStart
    // slideEnd
};
AjaxControlToolkit.SliderBehavior.prototype = {

    // initialize / dispose
    //
    initialize: function() {
        AjaxControlToolkit.SliderBehavior.callBaseMethod(this, 'initialize');
        
    	// ===============================================
    	// = These calls are need to set a default value =
    	// ===============================================
    	this.set_Steps(this.get_Steps());
    	this.set_Minimum(this.get_Minimum());
    	this.set_Maximum(this.get_Maximum());
    	
        this._initializeLayout();
    },

    dispose: function() {
        this._disposeHandlers();

        this._disposeBoundControl();

        if (this._enableHandleAnimation && this._handleAnimation) {
            this._handleAnimation.dispose();
        }

        AjaxControlToolkit.SliderBehavior.callBaseMethod(this, 'dispose');
    },
    
    update: function () {
    	this._disposeHandlers();        
        this._initializeHandlers();        
    },
        
    // _initializeLayout
    _initializeLayout: function() {
        // Element for the slider's rail.
        this._railElement = document.createElement('DIV');
        this._railElement.id = this.get_id() + '_railElement';

        // Key events in Firefox are raised only if a DIV has
        // tabIndex set to -1.
        this._railElement.tabIndex = -1;

        // Layout for the slider's handle.
        this._railElement.innerHTML = '<div></div>';
        this._handle = this._railElement.childNodes[0];
        this._handle.style.overflow = 'hidden';
        this._handle.style.position = 'absolute';

        // Initialize left and top to 0px for Opera
        if (Sys.Browser.agent == Sys.Browser.Opera) {
            this._handle.style.left = '0px';
            this._handle.style.top = '0px';
        }

        // Replace the textbox with the graphical slider.
        var el = this.get_element();
        var textBoxOffset = this._getLocation(el);
        var textBoxElementBounds = new Sys.UI.Bounds(textBoxOffset.x, textBoxOffset.y, el.offsetWidth || 0, el.offsetHeight || 0);
        el.parentNode.insertBefore(this._railElement, el);
        el.style.display = 'none';

        // This is a flag for saving keystrokes.
        this._isHorizontal = (this._orientation == AjaxControlToolkit.SliderOrientation.Horizontal);

        var defaultRailCssClass = (this._isHorizontal) ? 'ajax_slider_h_rail': 'ajax_slider_v_rail';
        var defaultHandleCssClass = (this._isHorizontal) ? 'ajax_slider_h_handle': 'ajax_slider_v_handle';

        this._railElement.className = (this._railCssClass) ? this._railCssClass: defaultRailCssClass;
        this._handle.className = (this._handleCssClass) ? this._handleCssClass: defaultHandleCssClass;

        // If the length property is set, override rail's CSS.
        if (this._isHorizontal) {
            if (this._length) {
                this._railElement.style.width = this._length;
            }
        }
        else {
            if (this._length) {
                this._railElement.style.height = this._length;
            }
        }

        this._loadHandleImage();

        // Enforce positioning set via the associated textbox's style.
        this._enforceTextBoxElementPositioning();

        // Layout is done.
        this._initializeSlider();
    },

    // _enforceTextBoxElementPositioning
    // Enforce positioning set via the associated textbox's style.
    //
    _enforceTextBoxElementPositioning: function() {
        var tbPosition =
        {
            position: this.get_element().style.position,
            top: this.get_element().style.top,
            right: this.get_element().style.right,
            bottom: this.get_element().style.bottom,
            left: this.get_element().style.left
        };

        if (tbPosition.position != '') {
            this._railElement.style.position = tbPosition.position;
        }
        if (tbPosition.top != '') {
            this._railElement.style.top = tbPosition.top;
        }
        if (tbPosition.right != '') {
            this._railElement.style.right = tbPosition.right;
        }
        if (tbPosition.bottom != '') {
            this._railElement.style.bottom = tbPosition.bottom;
        }
        if (tbPosition.left != '') {
            this._railElement.style.left = tbPosition.left;
        }
    },

    // _loadHandleImage
    // This method loads the image used for the handle.
    //
    _loadHandleImage: function() {
        this._handleImage = document.createElement('IMG');
        this._handleImage.id = this.get_id() + '_handleImage';
        this._handle.appendChild(this._handleImage);
        this._handleImage.src = this._handleImageUrl;
    },

    // _initializeSlider
    // This method initializes the slider control and is called after the
    // layout has been setup.
    //
    _initializeSlider: function() {
    	this._initializeBoundControl();
    	
        // Check if a value is already set in the textbox.
        var _elementValue;
        try {
            _elementValue = parseFloat(this._getElementValue());
        } catch(ex) {
            _elementValue = Number.NaN;
        }

        this.set_Value(_elementValue);

        // Position the slider's handle to the current value.
        this._setHandleOffset(this._value);

        // Setup the invisible drag handle.
        this._initializeDragHandle();

        // Register ourselves as a drop target.
        AjaxControlToolkit.SliderDragDropManagerInternal.registerDropTarget(this);

        this._initializeHandlers();

        this._initializeHandleAnimation();

        this._isInitializedInternal = true;

        this._raiseEvent('sliderInitialized');
        this._initialized = true;
    },

    // _initializeBoundControl
    // Creates and initializes the control that is bound to the slider.
    //
    _initializeBoundControl: function() {

        if (this._boundControl) {
            var isInputElement = this._boundControl.nodeName == 'INPUT';

            if (isInputElement) {
                this._boundControlChangeHandler = Function.createDelegate(this, this._onBoundControlChange);
                this._boundControlKeyPressHandler = Function.createDelegate(this, this._onBoundControlKeyPress);

                $addHandler(this._boundControl, 'change', this._boundControlChangeHandler);
                $addHandler(this._boundControl, 'keypress', this._boundControlKeyPressHandler);
            }
        }
    },

    _disposeBoundControl: function() {

        if (this._boundControl) {
            var isInputElement = this._boundControl.nodeName == 'INPUT';

            if (isInputElement) {
                $removeHandler(this._boundControl, 'change', this._boundControlChangeHandler);
                $removeHandler(this._boundControl, 'keypress', this._boundControlKeyPressHandler);
            }
        }
    },

    _onBoundControlChange: function(evt) {
        this._animationPending = true;
        this._setValueFromBoundControl();
    },

    _onBoundControlKeyPress: function(evt) {
        if (evt.charCode == 13) {
            this._animationPending = true;
            this._setValueFromBoundControl();
            evt.preventDefault();
        }
    },

    _setValueFromBoundControl: function() {
    	if (this._isUpdatingInternal) return;
        this._isUpdatingInternal = true;

        if (this._boundControlID) {
        	var origValue = this._boundControl.value,
        		value = null;

        	if (origValue != null) {
        		value = parseFloat(origValue.replace(/[^0-9]/g,""));
        		if (value >= this._minimum && value <= this._maximum) {

        			this._calcValue(value);	
        		} else if (value < this._minimum) {
    				this._setHandleOffset(this._minimum, this._enableHandleAnimation);
    			} else if ( value > this._maximum) {
    				this._setHandleOffset(this._maximum, this._enableHandleAnimation);
    			}
        	}      
        }

        this._isUpdatingInternal = false;
    },

    // _initializeHandleAnimation
    // Initializes the animation for the handle element.
    //
    _initializeHandleAnimation: function() {
        if (this._steps > 0) {
            this._enableHandleAnimation = false;
            return;
        }

        if (this._enableHandleAnimation) {
            this._handleAnimation = new AjaxControlToolkit.Animation.LengthAnimation(
            this._handle, this._handleAnimationDuration, 100, 'style');
        }
    },

    // _ensureBinding
    //
    _ensureBinding: function() {
        if (this._boundControl && this.get_Enabled()) {
            var value = this._value;

            if( (value >= this._minimum && value <= this._maximum)) {
                var isInputElement = this._boundControl.nodeName == 'INPUT';

                if (isInputElement) {
                	var boundValue = this._boundControl.value.replace(/[^0-9]/g,'');
                	
                	if (!window.event && (boundValue >= this._minimum || boundValue <= this._maximum)) {
                		return;
                	}                
                	if (Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack()) {
                		return;
                	}

                    if (this.get_BoundControlFormat()) {
                        var newValue = String.localeFormat(this.get_BoundControlFormat(), parseFloat(value));
                        if (this._boundControl.value != newValue) {
                			this._isUpdatingInternal = true;
                            this._boundControl.value = newValue;
                			this._justBoundControl = false;
                            this._fireChangeEvent(this._boundControl);
                			this._isUpdatingInternal = false;
                		}
                    } else {
                        if (this._boundControl.value != value) {
                			this._isUpdatingInternal = true;
                            this._boundControl.value = value;
                			this._justBoundControl = false;
                            this._fireChangeEvent(this._boundControl);
                			this._isUpdatingInternal = value;
                		}
                    }
                }
                else if (this._boundControl) {
                    this._boundControl.innerHTML = value;
                }
            }
        }
    },


	_getElementValue: function() {
		var element = this.get_element();
		if (element.nodeName == "INPUT") {
			return element.value;
		} else if (element.nodeName == "SELECT") {
			return element.options[element.selectedIndex].value;
		} else {
			return null;
		}
	},
	_setElementValue: function(value) {
		var element = this.get_element();
		if (element.nodeName == "INPUT") {
			element.value = value;
		} else if (element.nodeName == "SELECT") {
			for (var i=0, l = element.options.length; i < l; i++) {
				if (element.options[i].value == value) {
					element.selectedIndex = i;
				};
			};	
		}	
	},
    // _getBoundsInternal
    // This function swaps the x and y coordinates to use the same logic
    // for both the horizontal and vertical slider.
    //
    _getBoundsInternal: function(element, throwError) {

        var offset = this._getLocation(element);
        var bounds = new Sys.UI.Bounds(offset.x, offset.y, element.offsetWidth || 0, element.offsetHeight || 0);

        // This function checks whether width and height are defined
        // for the DOM element passed as an argument.
        function hasSize() {
            return bounds.width > 0 && bounds.height > 0;
        }

        // If we weren't able to compute the size, let's try getting it from CSS.
        // For example, if the slider has a containing element with display:none, then
        // the size computed will always be zero. Note: in Opera, the width parsed from CSS is always 0.
        if (!hasSize()) {
            bounds.width = parseInt(this._getCurrentStyle(element, 'width'), 10);
            bounds.height = parseInt(this._getCurrentStyle(element, 'height'), 10);

            if (!hasSize()) {
                // If size is still invalid, let's try temporary adding the element
                // as a child of the BODY element.
                var tempNode = element.cloneNode(true);
                tempNode.visibility = 'hidden';
                document.body.appendChild(tempNode);

                bounds.width = parseInt(this._getCurrentStyle(tempNode, 'width'), 10);
                bounds.height = parseInt(this._getCurrentStyle(tempNode, 'height'), 10);
                document.body.removeChild(tempNode);

                // If size is still invalid, then give up.
                if (!hasSize() && !!throwError) {
                    throw Error.argument('element size', 'Please set valid values for the height and width attributes in the sliders CSS classes');
                }
            }
        }

        if (this._orientation == AjaxControlToolkit.SliderOrientation.Vertical) {
             bounds = { x : bounds.y, 
                y: bounds.x,
                height: bounds.width,
                width: bounds.height,
                right: bounds.right,
                bottom: bounds.bottom,
                        location : {x:bounds.y, y:bounds.x},
                        size : {width:bounds.height, height:bounds.width}
            };
        }

        return bounds;
    },

    // _getRailBounds
    // This method returns the slider's rail bounds.
    //
    _getRailBounds: function() {
        var bounds = this._getBoundsInternal(this._railElement, true);

        return bounds;
    },

    // _getHandleBounds
    // This method returns the slider's handle bounds.
    //
    _getHandleBounds: function() {
        var bounds = this._getBoundsInternal(this._handle, false);
        if (bounds.width != 0 && this._getStaticHandleWidth() != 0) {
        	this._setStaticHandleWidth(bounds.width);
        };
        return bounds;
    },
    
    _getStaticHandleWidth: function() {
    	return this._staticHandleWidth || 0;
    },
    
    _setStaticHandleWidth: function(value) {
    	this._staticHandleWidth = value;
    },

    // _initializeDragHandle
    // Initializes the invisible drag handle used to obtain
    // an horizontal or vertical drag effect with the ASP.NET AJAX framework.
    //
    _initializeDragHandle: function() {
        var dh = this._dragHandle = document.createElement('DIV');

        dh.style.position = 'absolute';
        dh.style.width = '1px';
        dh.style.height = '1px';
        dh.style.overflow = 'hidden';
        dh.style.zIndex = '999';
        dh.style.background = 'none';

        document.body.appendChild(this._dragHandle);
    },

    // _resetDragHandle
    // This method is called to reset the invisible drag handle to its
    // default position.
    //
    _resetDragHandle: function() {
        var handleOffset = this._getLocation(this._handle);
        var handleBounds = new Sys.UI.Bounds(handleOffset.x, handleOffset.y, this._handle.offsetWidth || 0, this._handle.offsetHeight || 0);
        Sys.UI.DomElement.setLocation(this._dragHandle, handleBounds.x, handleBounds.y);
    },

    // _initializeHandlers
    // This method creates the event handlers and attach them to the
    // corresponding events.
    //
    _initializeHandlers: function() {
        if (!this.get_Enabled()) {
            return;
    	}
        this._selectstartHandler = Function.createDelegate(this, this._onSelectStart);
        this._mouseupHandler = Function.createDelegate(this, this._onMouseUp);

        $addHandler(document, 'mouseup', this._mouseupHandler);

        $addHandlers(this._handle,
        {
            'mousedown': this._onMouseDown,
            'dragstart': this._IEDragDropHandler,
            'drag': this._IEDragDropHandler,
            'dragend': this._IEDragDropHandler
        },
        this);

        $addHandlers(this._railElement,
        {
            'click': this._onRailClick
        },
        this);
    },

    _disposeHandlers: function() {
        $clearHandlers(this._handle);
        $clearHandlers(this._railElement);

        if (this._mouseupHandler) {
            $removeHandler(document, 'mouseup', this._mouseupHandler);
        }
        this._mouseupHandler = null;
        this._selectstartHandler = null;
    },

    // startDragDrop
    // Tells the DragDropManager to start a drag and drop operation.
    //
    startDragDrop: function(dragVisual) {
        this._resetDragHandle();

        AjaxControlToolkit.SliderDragDropManagerInternal.startDragDrop(this, dragVisual, null);
    },

    // _onMouseDown
    // Handles the slider's handle mousedown event.
    //
    _onMouseDown: function(evt) {
        window._event = evt;
        evt.preventDefault();

        if (!AjaxControlToolkit.SliderBehavior.DropPending) {
            AjaxControlToolkit.SliderBehavior.DropPending = this;

            $addHandler(document, 'selectstart', this._selectstartHandler);
            this._selectstartPending = true;

            this.startDragDrop(this._dragHandle);
        }
    },

    // _onMouseUp
    // Handles the document's mouseup event.
    //
    _onMouseUp: function(evt) {
        var srcElement = evt.target;

        if (AjaxControlToolkit.SliderBehavior.DropPending == this) {
            AjaxControlToolkit.SliderBehavior.DropPending = null;

            if (this._selectstartPending) {
                $removeHandler(document, 'selectstart', this._selectstartHandler);
            }
        }
    },

    // _onRailClick
    // Handles the rail element's click event.
    _onRailClick: function(evt) {
        if (evt.target == this._railElement) {
            this._animationPending = true;
            this._onRailClicked(evt);
        }
    },

    // _IEDragDropHandler
    // This method handles the drag events raised by Internet Explorer,
    // preventing the default behaviors since we are using the
    // GenericDragDropManager
    //
    _IEDragDropHandler: function(evt) {
        evt.preventDefault();
    },

    // _onSelectStart
    // Handles the document's selectstart event.
    //
    _onSelectStart: function(evt) {
        evt.preventDefault();
    },

    // _calcValue
    // This function computes the slider's value.
    //
    _calcValue: function(value, mouseOffset) {

        var val;

        if (value != null) {
            if (!Number.isInstanceOfType(value)) {
                try {
                    value = parseFloat(value.replace(/[^0-9]/g, ""));
                } catch(ex) {
                    value = Number.NaN;
                }
            }

            if (isNaN(value)) {
                value = this._minimum;
            }

            val = (value < this._minimum) ? this._minimum: (value > this._maximum) ? this._maximum: value;
        }
        else {
            var _minimum = this._minimum;
            var _maximum = this._maximum;
            var handleBounds = this._getHandleBounds();
            var sliderBounds = this._getRailBounds();
            var handleX = (mouseOffset) ? mouseOffset - handleBounds.width / 2: handleBounds.x - sliderBounds.x;
            var extent = sliderBounds.width - handleBounds.width;
            var percent = handleX / extent;

            val = (handleX == 0) ? _minimum: (handleX == (sliderBounds.width - handleBounds.width)) ? _maximum: _minimum + percent * (_maximum - _minimum);
        }

        if (this._steps > 0) {
            val = this._getNearestStepValue(val);
        }

        val = (val < this._minimum) ? this._minimum: (val > this._maximum) ? this._maximum: val;

        this._isUpdatingInternal = true;
        this.set_Value(val);
        this._isUpdatingInternal = false;

        return val;
    },

    // _setHandleOffset
    // This function computes the handle's offset corresponding
    // to a given value.
    //
    _setHandleOffset: function(value, playHandleAnimation) {
        var _minimum = this._minimum;
        var _maximum = this._maximum;
        var handleBounds = this._getHandleBounds();
        var handleWidth = this._getStaticHandleWidth();
        var sliderBounds = this._getRailBounds();
        
        var extent = _maximum - _minimum;
        var fraction = (value - _minimum) / extent;
        var hypOffset = Math.round(fraction * (sliderBounds.width - handleWidth));

        var offset = (value == _minimum) ? 0: (value == _maximum) ? (sliderBounds.width - handleWidth) : hypOffset;
        
        if (playHandleAnimation) {
            this._handleAnimation.set_startValue(handleBounds.x - sliderBounds.x);
            this._handleAnimation.set_endValue(offset);
            this._handleAnimation.set_propertyKey((this._isHorizontal) ? 'left': 'top');
            this._handleAnimation.play();

            this._animationPending = false;
        }
        else {
            if (this._isHorizontal) {
                this._handle.style.left = offset + 'px';
            }
            else {
                this._handle.style.top = offset + 'px';
            }
        }
    },

    // _getNearestStepValue
    // This function computes the current value for a "discrete" slider.
    //
    _getNearestStepValue: function(value) {
        if (this._steps == 0) return value;

        var extent = this._maximum - this._minimum;
        if (extent == 0) return value;

        var delta = extent / (this._steps - 1);

        return Math.round(value / delta) * delta;
    },

    // _onHandleReleased
    // This function is invoked when the slider's handle is released.
    //
    _onHandleReleased: function() {
        if (this._raiseChangeOnlyOnMouseUp) {
            this._fireElementChangeEvent();
        }

        this._raiseEvent('slideEnd');
    },

    // _onRailClicked
    // This function is invoked when the slider's rail is clicked.
    //
    _onRailClicked: function(evt) {
        // Compute the pointer's offset.
        var handleBounds = this._getHandleBounds();
        var sliderBounds = this._getRailBounds();
        var offset = (this._isHorizontal) ? evt.offsetX: evt.offsetY;
        var minOffset = handleBounds.width / 2;
        var maxOffset = sliderBounds.width - minOffset;

        offset = (offset < minOffset) ? minOffset: (offset > maxOffset) ? maxOffset: offset;

        this._calcValue(null, offset, true);

        this._fireElementChangeEvent();
    },

    // _fireElementChangeEvent
    // Raise the change event on the underlying textbox when
    // its value is updated programmatically.
    //
    _fireElementChangeEvent: function() {
        this._fireChangeEvent(this.get_element());
    },
    _fireChangeEvent: function(element) {
        if (document.createEvent) {
            var onchangeEvent = document.createEvent('HTMLEvents');
            onchangeEvent.initEvent('change', true, false);

            element.dispatchEvent(onchangeEvent);
        }
        else if (document.createEventObject) {
            element.fireEvent('onchange');
        }
    },
    _getCurrentStyle: function(element, attribute, defaultValue) {
        /// <summary>
        /// CommonToolkitScripts.getCurrentStyle is used to compute the value of a style attribute on an
        /// element that is currently being displayed.  This is especially useful for scenarios where
        /// several CSS classes and style attributes are merged, or when you need information about the
        /// size of an element (such as its padding or margins) that is not exposed in any other fashion.
        /// </summary>
        /// <param name="element" type="Sys.UI.DomElement" domElement="true">
        /// Live DOM element to check style of
        /// </param>
        /// <param name="attribute" type="String">
        /// The style attribute's name is expected to be in a camel-cased form that you would use when
        /// accessing a JavaScript property instead of the hyphenated form you would use in a CSS
        /// stylesheet (i.e. it should be "backgroundColor" and not "background-color").
        /// </param>
        /// <param name="defaultValue" type="Object" mayBeNull="true" optional="true">
        /// In the event of a problem (i.e. a null element or an attribute that cannot be found) we
        /// return this object (or null if none if not specified).
        /// </param>
        /// <returns type="Object">
        /// Current style of the element's attribute
        /// </returns>
        var currentValue = null;
        if (element) {
            if (element.currentStyle) {
                currentValue = element.currentStyle[attribute];
            } else if (document.defaultView && document.defaultView.getComputedStyle) {
                var style = document.defaultView.getComputedStyle(element, null);
                if (style) {
                    currentValue = style[attribute];
                }
            }

            if (!currentValue && element.style.getPropertyValue) {
                currentValue = element.style.getPropertyValue(attribute);
            }
            else if (!currentValue && element.style.getAttribute) {
                currentValue = element.style.getAttribute(attribute);
            }
        }

        if ((!currentValue || currentValue == "" || typeof(currentValue) === 'undefined')) {
            if (typeof(defaultValue) != 'undefined') {
                currentValue = defaultValue;
            }
            else {
                currentValue = null;
            }
        }
        return currentValue;
    },

    _getLocation: function(element) {
        /// <summary>Gets the coordinates of a DOM element.</summary>
        /// <param name="element" domElement="true"/>
        /// <returns type="Sys.UI.Point">
        ///   A Point object with two fields, x and y, which contain the pixel coordinates of the element.
        /// </returns>
        // workaround for an issue in getLocation where it will compute the location of the document element.
        // this will return an offset if scrolled.
        //
        if (element === document.documentElement) {
            return new Sys.UI.Point(0, 0);
        }

        // Workaround for IE6 bug in getLocation (also required patching getBounds - remove that fix when this is removed)
        if (Sys.Browser.agent == Sys.Browser.InternetExplorer && Sys.Browser.version < 7) {
            if (element.window === element || element.nodeType === 9 || !element.getClientRects || !element.getBoundingClientRect) return new Sys.UI.Point(0, 0);

            // Get the first bounding rectangle in screen coordinates
            var screenRects = element.getClientRects();
            if (!screenRects || !screenRects.length) {
                return new Sys.UI.Point(0, 0);
            }
            var first = screenRects[0];

            // Delta between client coords and screen coords
            var dLeft = 0;
            var dTop = 0;

            var inFrame = false;
            try {
                inFrame = element.ownerDocument.parentWindow.frameElement;
            } catch(ex) {
                // If accessing the frameElement fails, a frame is probably in a different
                // domain than its parent - and we still want to do the calculation below
                inFrame = true;
            }

            // If we're in a frame, get client coordinates too so we can compute the delta
            if (inFrame) {
                // Get the bounding rectangle in client coords
                var clientRect = element.getBoundingClientRect();
                if (!clientRect) {
                    return new Sys.UI.Point(0, 0);
                }

                // Find the minima in screen coords
                var minLeft = first.left;
                var minTop = first.top;
                for (var i = 1; i < screenRects.length; i++) {
                    var r = screenRects[i];
                    if (r.left < minLeft) {
                        minLeft = r.left;
                    }
                    if (r.top < minTop) {
                        minTop = r.top;
                    }
                }

                // Compute the delta between screen and client coords
                dLeft = minLeft - clientRect.left;
                dTop = minTop - clientRect.top;
            }

            // Subtract 2px, the border of the viewport (It can be changed in IE6 by applying a border style to the HTML element,
            // but this is not supported by ASP.NET AJAX, and it cannot be changed in IE7.), and also subtract the delta between
            // screen coords and client coords
            var ownerDocument = element.document.documentElement;
            return new Sys.UI.Point(first.left - 2 - dLeft + ownerDocument.scrollLeft, first.top - 2 - dTop + ownerDocument.scrollTop);
        }

        return Sys.UI.DomElement.getLocation(element);
    },

    // IDragSource Members.
    //
    get_dragDataType: function() {
        return 'HTML';
    },

    getDragData: function() {
        return this._handle;
    },

    get_dragMode: function() {
        return AjaxControlToolkit.DragMode.Move;
    },

    onDragStart: function() {
        this._resetDragHandle();
        this._raiseEvent('slideStart');
    },

    onDrag: function() {
        var dragHandleBounds = this._getBoundsInternal(this._dragHandle, true);
        var handleBounds = this._getHandleBounds();
        var sliderBounds = this._getRailBounds();

        var handlePosition;
        if (this._isHorizontal) {
            handlePosition = { x:dragHandleBounds.x - sliderBounds.x, y:0 };
        }
        else {
            handlePosition = { y:dragHandleBounds.x - sliderBounds.x, x:0 };
        }

        Sys.UI.DomElement.setLocation(this._handle, handlePosition.x, handlePosition.y);

        this._calcValue(null, null);

        // If we have a discrete slider, correct the handle's position
        // based on the computed value.
        if (this._steps > 1) {
            this._setHandleOffset(this.get_Value(), false);
        }
    },

    onDragEnd: function() {
        this._onHandleReleased();
    },

    // IDropTarget members.
    //
    get_dropTargetElement: function() {
        return document.body;
    },

    canDrop: function(dragMode, dataType) {
        return dataType == 'HTML';
    },

    drop: Function.emptyMethod,

    onDragEnterTarget: Function.emptyMethod,

    onDragLeaveTarget: Function.emptyMethod,

    onDragInTarget: Function.emptyMethod,

    // Events
    //
    add_sliderInitialized: function(handler) {
        this.get_events().addHandler('sliderInitialized', handler);
    },

    remove_sliderInitialized: function(handler) {
        this.get_events().removeHandler('sliderInitialized', handler);
    },

    add_valueChanged: function(handler) {
        this.get_events().addHandler('valueChanged', handler);
    },

    remove_valueChanged: function(handler) {
        this.get_events().removeHandler('valueChanged', handler);
    },

    add_slideStart: function(handler) {
        this.get_events().addHandler('slideStart', handler);
    },

    remove_slideStart: function(handler) {
        this.get_events().removeHandler('slideStart', handler);
    },

    add_slideEnd: function(handler) {
        this.get_events().addHandler('slideEnd', handler);
    },

    remove_slideEnd: function(handler) {
        this.get_events().removeHandler('slideEnd', handler);
    },

    _raiseEvent: function(eventName, eventArgs) {
        var handler = this.get_events().getHandler(eventName);
        if (handler) {
            if (!eventArgs) {
                eventArgs = Sys.EventArgs.Empty;
            }
            handler(this, eventArgs);
        }
    },

	get_isSelect: function() {
		if (this._isSelect === undefined) {
			this._isSelect = this.get_element().nodeName == "SELECT";
		};
		return !!this._isSelect;
	},
	
    // Properties.
    //
    get_Value: function() {
        return this._value;
    },

    set_Value: function(value) {
        var oldValue = this._value;
        var newValue = value;

        if (!this._isUpdatingInternal&&!this._isInitializedInternal) {
            newValue = this._calcValue(value);
        }
        
        this._value = newValue.toFixed(this._decimals);        
        this._setElementValue(this._value);

        this._ensureBinding();

        if (!Number.isInstanceOfType(this._value)) {
            try {
                this._value = parseFloat(this._value);
            } catch(ex) {
                this._value = Number.NaN;
            }
        }

        if (this._tooltipText) {
            this._handle.alt = this._handle.title =
            String.format(this._tooltipText, this._value);
        }

        if (this._isInitializedInternal) {
            this._setHandleOffset(newValue, this._enableHandleAnimation && this._animationPending);

            if (this._isUpdatingInternal && this._value != oldValue) {
                if (!this._raiseChangeOnlyOnMouseUp) {
                    this._fireElementChangeEvent();
                }
            }

            if (this._value != oldValue) {
                this._raiseEvent('valueChanged');
            }
        }
    },

    get_RailCssClass: function() {
        return this._railCssClass;
    },

    set_RailCssClass: function(value) {
        this._railCssClass = value;
        if (this._railElement) {
        	this._railElement.className = value;
        };
    },

    get_HandleImageUrl: function() {
        return this._handleImageUrl;
    },

    set_HandleImageUrl: function(value) {
        this._handleImageUrl = value;
    },

    get_HandleCssClass: function() {
        return this._handleCssClass;
    },

    set_HandleCssClass: function(value) {
        this._handleCssClass = value;
        if (this._handle) {
        	this._handle.className = value;
        };
    },

    get_Minimum: function() {
    	if (this.get_isSelect()) {
    		return parseFloat(this.get_element().options[0].value);
    	} else {
    		return this._minimum;	
    	}
    },

    set_Minimum: function(value) {
        this._minimum = value;
    },

    get_Maximum: function() {
    	if (this.get_isSelect()) {
    		var el = this.get_element();
    		return parseFloat(el.options[el.options.length - 1].value);
    	} else {
    		return this._maximum;	
    	}        
    },

    set_Maximum: function(value) {
        this._maximum = value;
    },

    get_Orientation: function() {
        return this._orientation;
    },

    set_Orientation: function(value) {
        this._orientation = value;
    },

    get_Steps: function() {
    	if (this.get_isSelect()) {
    		return this.get_element().options.length;
    	} else {
    		return this._steps;
    	}        
    },

    set_Steps: function(value) {
        this._steps = Math.abs(value);
        this._steps = (this._steps == 1) ? 2: this._steps;
    },

    get_Decimals: function() {
        return this._decimals;
    },

    set_Decimals: function(value) {
        this._decimals = Math.abs(value);
    },

    get_EnableHandleAnimation: function() {
        return this._enableHandleAnimation;
    },

    set_EnableHandleAnimation: function(value) {
        this._enableHandleAnimation = value;
    },

    get_HandleAnimationDuration: function() {
        return this._handleAnimationDuration;
    },

    set_HandleAnimationDuration: function(value) {
        this._handleAnimationDuration = value;
    },

    get_BoundControlID: function() {
        return this._boundControlID;
    },

    set_BoundControlID: function(value) {
        this._boundControlID = value;
        if (this._boundControlID) {
            this._boundControl = $get(this._boundControlID);
            this._justBoundControl = true;
        } else {
            this._boundControl = null;
            this._justBoundControl = null;
        }
    },
    get_BoundControlFormat: function() {
        return this._boundControlFormat;
    },
    set_BoundControlFormat: function(value) {
        if (this._boundControlFormat != value) {
            this._boundControlFormat = value;
        }
    },
    get_Length: function() {
        return this._length;
    },

    set_Length: function(value) {
        this._length = value + 'px';
    },

    get_SliderInitialized: function() {
        return this._isInitializedInternal;
    },

    get_RaiseChangeOnlyOnMouseUp: function() {
        return this._raiseChangeOnlyOnMouseUp;
    },

    set_RaiseChangeOnlyOnMouseUp: function(value) {
        this._raiseChangeOnlyOnMouseUp = value;
    },

    get_TooltipText: function() {
        return this._tooltipText;
    },

    set_TooltipText: function(value) {
        this._tooltipText = value;
    },
    get_Enabled: function() {
        return this._enabled;
    },
    set_Enabled: function(value) {
        if (this._enabled != value) {
            this._enabled = value;
        }
    }
};

AjaxControlToolkit.SliderBehavior.DropPending = null;
// Global, used to work around an issue when using the GenericDragDropManager in IE.
if (AjaxControlToolkit.SliderBehavior.registerClass != undefined)
 AjaxControlToolkit.SliderBehavior.registerClass('AjaxControlToolkit.SliderBehavior', Sys.UI.Behavior, AjaxControlToolkit.IDragSource, AjaxControlToolkit.IDropTarget);
