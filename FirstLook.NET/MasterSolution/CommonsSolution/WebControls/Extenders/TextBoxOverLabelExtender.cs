﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("FirstLook.Common.WebControls.Extenders.TextBoxOverLabelExtender.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Extenders.TextBoxOverLabelExtender.debug.js", "text/javascript")]

namespace FirstLook.Common.WebControls.Extenders
{
    [TargetControlType(typeof(TextBox))]
    public class TextBoxOverLabelExtender : ExtenderControl
    {
        private const string JavaScriptType = "FirstLook.Common.WebControls.Extenders.TextBoxOverLabelExtender";

        [DefaultValue(""),
         Description("Label to be placed in the TextBox"),
         Category("Behavior"),
         IDReferenceProperty(typeof(Label))]
        public string LabelID { get; set; }

        #region Overrides of ExtenderControl

        protected override IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
        {
            ScriptBehaviorDescriptor scriptBehaviorDescriptor = new ScriptBehaviorDescriptor(JavaScriptType, targetControl.ClientID);

            scriptBehaviorDescriptor.AddElementProperty("label", FindControl(LabelID).ClientID);

            return new[] {scriptBehaviorDescriptor};
        }

        protected override IEnumerable<ScriptReference> GetScriptReferences()
        {
            ScriptManager scriptManager = ScriptManager.GetCurrent(Page);
            string suffix = scriptManager == null || scriptManager.ScriptMode == ScriptMode.Debug ? ".debug.js" : ".js";
            return new[]
                       {
                           new ScriptReference(JavaScriptType + suffix,
                                               "FirstLook.Common.WebControls")
                       };
        }

        #endregion

        #region StateManagement
        protected override object SaveViewState()
        {
            return new Pair(base.SaveViewState(), LabelID);
        }

        protected override void LoadViewState(object savedState)
        {
            Pair pair = savedState as Pair;
            if (pair != null)
            {
                base.LoadViewState(pair.First);
                LabelID = (string) pair.Second;
            } 
            else
            {
                base.LoadViewState(savedState);    
            }            
        } 
        #endregion

    }
}
