using System;

namespace FirstLook.Common.WebControls.Extenders
{
    public enum HelpTextEventTrigger
    {
        None = 1,
        OnFocus = 2,
        OnBlur = 4,
        OnMouseOver = 8,
        OnMouseOut = 16,
        OnSelect = 32,        
        OnChange = 64,
        OnClick = 128,
        OnDblClick = 256,
        Checked = 512,
        Visible = 1024
    }
}
