﻿using System.Collections.Generic;
using System.Web.UI;

[assembly: WebResource("FirstLook.Common.WebControls.Extenders.FadingUserMessageLabel.debug.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Extenders.FadingUserMessageLabel.js", "text/javascript")]

namespace FirstLook.Common.WebControls.Extenders
{
    [TargetControlType(typeof(Control))]
    public class FadingUserMessageLabel : ExtenderControl
    {
        private const string JavaScriptNamespace = "FirstLook.Common.WebControls.Extenders.FadingUserMessageLabel";

        #region Overrides of ExtenderControl

        protected override IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
        {
            ScriptBehaviorDescriptor descriptor = new ScriptBehaviorDescriptor(JavaScriptNamespace,
                                                                               targetControl.ClientID);

            return new []{descriptor};
        }

        protected override IEnumerable<ScriptReference> GetScriptReferences()
        {
            ScriptReference scriptReference = new ScriptReference(JavaScriptNamespace + ".js",
                                                                  "FirstLook.Common.WebControls");

            return new[] {scriptReference};
        }

        #endregion
    }
}
