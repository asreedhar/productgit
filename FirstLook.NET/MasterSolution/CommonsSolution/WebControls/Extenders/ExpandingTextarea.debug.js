Type.registerNamespace('FirstLook.Common.WebControls.Extenders');

FirstLook.Common.WebControls.Extenders.ExpandingTextarea = function(element) {
    FirstLook.Common.WebControls.Extenders.ExpandingTextarea.initializeBase(this, [element]);

    this._animation = null;
    this._windowPadding = 50;
    this.delegates = [];
};

FirstLook.Common.WebControls.Extenders.ExpandingTextarea.prototype = {
    initialize: function() {
        FirstLook.Common.WebControls.Extenders.ExpandingTextarea.callBaseMethod(this, "initialize");
    },
    updated: function() {
        var el = this.get_element();

        this.setup_currentBounds();
        this.setup_rootBounds();

        this.set_minHeight(this.get_currentHeight());

		this.delegates["element_on_propertyChange"] = Function.createDelegate(this, this._onPropertyChange);
        this.delegates["keydown"] = Function.createDelegate(this, this._onKeyDown);
        this.delegates["blur"] = Function.createDelegate(this, this._onBlur);
        this.delegates["focus"] = Function.createDelegate(this, this._onFocus);
        
        this._animation = new AjaxControlToolkit.Animation.LengthAnimation(el, .25, 10, 'style', null, 0, 0, 'px');
        this._animation.set_propertyKey('height');

        this.__addHandler(el, 'focus', this.delegates["focus"]);
        this.__addHandler(el, 'blur', this.delegates["blur"]);
        this.__addHandler(el, 'keydown', this.delegates["keydown"]);
        this.__addHandler(el, 'keyup', this.delegates["keydown"]);

        FirstLook.Common.WebControls.Extenders.ExpandingTextarea.callBaseMethod(this, "updated");
    },
    dispose: function() {
        if (this._animation) {
            this._animation.dispose();
            this._animation = undefined;
        }

        for (var i = 0, l = this.delegates.length; i < l; i++) {
            delete this.delegates[i];
        };

        this.__removeHandlers();

        FirstLook.Common.WebControls.Extenders.ExpandingTextarea.callBaseMethod(this, "dispose");
    },
    get_currentHeight: function() {
        var textarea = this.get_element(),
        bounds = Sys.UI.DomElement.getBounds(textarea);

        return bounds.height;
    },
    get_targetHeight: function() {
        if (! !!this._targetHeight)
        this._targetHeight = this.get_currentHeight();
        return this._targetHeight;
    },
    set_targetHeight: function(value) {    	
        var min = this.get_minHeight(),
        max = this.get_maxHeight();

        if (value < min)
        value = min;

        if (value > max)
        value = max;

        if (this._targetHeight != value) {
            this._targetHeight = value;
            this._animate();
        };
    },
    get_minHeight: function() {
        return this._minHeight;
    },
    set_minHeight: function(value) {
        this._minHeight = value;
    },
    get_maxHeight: function() {
        if (! !!this._rootBounds)
        this.setup_rootBounds();
        return this._rootBounds.height;
    },
    get_origOverflow: function() {
        return this._origOverflow;
    },
    set_origOverflow: function(value) {
        this._origOverflow = value;
    },
    setup_rootBounds: function() {
        var doc = document.getElementsByTagName("body");

        this._rootBounds = Sys.UI.DomElement.getBounds(doc[0]);

        doc = undefined;
    },
    setup_currentBounds: function() {
        var textarea = this.get_element();

        this._currentBounds = Sys.UI.DomElement.getBounds(textarea);
    },
    setup_css: function() {
        var el = this.get_element();
        this.set_origOverflow(el.style.overflow);
        el.style.overflow = "hidden";
    },
    reset_css: function() {
        var el = this.get_element();
        el.style.overflow = this.get_origOverflow();
    },
    _animate: function() {
        if (this._animation) {
            var current = this.get_currentHeight(),
            target = this.get_targetHeight(),
            diff = Math.abs(current - target);

            if (diff > 7) {
            	this._animation.stop();
	
	            this._animation.set_startValue(current);
	            this._animation.set_endValue(target);
	            this._animation.play();
            }
        }
    },
    _onKeyDown: function(e, args) {
        if (this._timer)
        clearTimeout(this._timer);

        this.set_targetHeight(e.target.scrollHeight + 16);
        this.raisePropertyChanged("targetHeight");
    },
    _onFocus: function(e, args) {
        if (this._timer)
        clearTimeout(this._timer);

        this.set_targetHeight(e.target.scrollHeight + 16);
        this.raisePropertyChanged("targetHeight");
    },
    _onBlur: function(e, args) {
        var timer = (function(scope) {
            return function() {
                // ===================================================================================
                // = If we don't have an element the DOM has been changed before the timer has fired =
                // ===================================================================================
                if (scope.get_element() !== undefined) {
                    scope.set_targetHeight(scope.get_minHeight());
                    scope.raisePropertyChanged("targetHeight");
                }
            };
        })(this);
        this._timer = setTimeout(timer, 500);
    },
    _onAnimationStart: function() {
    	this.raisePropertyChanged('targetHeight');
    },
	_onAnimationEnd: function() {
    	this.raisePropertyChanged('currentHeight');
    },
    _onPropertyChange: function(sender, args) {
    	var property = args.get_propertyName();
    	
    	Sys.Debug.assert(! !!FirstLook.Common.WebControls.Extenders.ExpandingTextarea["debug_" + property], property);
    	
    	if ( !! FirstLook.Common.WebControls.Extenders.ExpandingTextarea || !! FirstLook.Common.WebControls.Extenders.ExpandingTextarea["trace_"+property]) {
            Sys.Debug.traceDump(sender["get_" + property](), sender.get_name() + ":" + property);
        };
    },
    __addHandler: function(el, type, fn) {
        if (! !!this.__eventCache) {
            this.__eventCache = [];
        }
        this.__eventCache.push([el, type, fn]);
        $addHandler(el, type, fn);
    },
    __removeHandlers: function() {
        if (!!this.__eventCache) {
            for (var i = 0, l = this.__eventCache.length; i < l; i++) {
                $removeHandler(this.__eventCache[i][0], this.__eventCache[i][1], this.__eventCache[i][2]);
                this.__eventCache[i][0] = undefined;
                this.__eventCache[i][1] = undefined;
                this.__eventCache[i][2] = undefined;
            };
            delete this.__eventCache;
        };
    },
    _getScrollPosition: function(percentage) {
        return this.get_startValue() + (this.get_endValue() - this.get_startValue()) * (percentage / 100);
    },
    __getComputedStyle: function(el, style) {
        if (!!el.currentStyle) {
            return el.currentStyle;
        } else {
            return document.defaultView.getComputedStyle(el);
        }
    }
};

if (FirstLook.Common.WebControls.Extenders.ExpandingTextarea.registerClass != undefined)
 FirstLook.Common.WebControls.Extenders.ExpandingTextarea.registerClass('FirstLook.Common.WebControls.Extenders.ExpandingTextarea', Sys.UI.Behavior);

Sys.Application.notifyScriptLoaded();