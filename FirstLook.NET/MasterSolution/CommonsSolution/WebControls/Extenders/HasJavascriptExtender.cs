using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("FirstLook.Common.WebControls.Extenders.HasJavascriptControlBehavior.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Extenders.HasJavascriptControlBehavior.debug.js", "text/javascript")]

namespace FirstLook.Common.WebControls.Extenders
{
	public class HasJavascriptControlExtender : ExtenderControl
	{
		[DefaultValue(""),
		 Description("Button to which clicks are delegated"),
		 Category("Behavior"),
		IDReferenceProperty(typeof(bool))]
		public bool HasJavascript
		{
			get
			{
				object value = ViewState["HasJavascript"];
				if (value == null)
					return false;
				return (bool)value;
			}
			set
			{
				if (HasJavascript != value)
				{
					ViewState["HasJavascript"] = value;
				}
			}
		}

		[DefaultValue(""),
		 Description("Button to which clicks are delegated"),
		 Category("Behavior"),
		IDReferenceProperty(typeof(Control))]
		public Control ClientStateField
		{
			get
			{
				Control value = ViewState["ClientStateField"] as Control;
				if (value == null)
					return null;
				return value;
			}
			set
			{
				if (ClientStateField != value)
				{
					ViewState["ClientStateField"] = value;
				}
			}
		}

		protected override IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
		{
			ScriptBehaviorDescriptor descriptor = new ScriptBehaviorDescriptor("FirstLook.Common.WebControls.Extenders.HasJavascript.js", targetControl.ClientID);
			descriptor.AddProperty("hasJavascript", HasJavascript.ToString());
			descriptor.AddElementProperty("clientStateField", ClientStateField.ClientID);
			return new ScriptDescriptor[] { descriptor };
		}

		protected override IEnumerable<ScriptReference> GetScriptReferences()
		{
			return new ScriptReference[] { new ScriptReference("FirstLook.Common.WebControls.Extenders.HasJavascript.js", "FirstLook.Common.HasJavascript.js") };
		}
	}
}
