Type.registerNamespace('FirstLook.Common.WebControls.Extenders');

FirstLook.Common.WebControls.Extenders.HelpTextExtender = function(element) {
    var props = [
    "helper_element",
    "helper_element_hidden",
    "show_helper_text_event",
    "hide_helper_text_event",
    "show_help_at_mouse_y",
    "show_help_at_mouse_x",
    "mouse_y",
    "mouse_x"
    ];

    for (var i = 0, l = props.length; i < l; i++) {
        this.create_getter_setter(props[i]);
    };

    FirstLook.Common.WebControls.Extenders.HelpTextExtender.initializeBase(this, [element]);
};

FirstLook.Common.WebControls.Extenders.HelpTextExtender.prototype = {
    initialize: function() {
        this.delegates = {};
        this.delegates['property_changed'] = Function.createDelegate(this, this.onPropertyChanged);
        this.add_propertyChanged(this.delegates['property_changed']);

        FirstLook.Common.WebControls.Extenders.HelpTextExtender.callBaseMethod(this, "initialize");
    },
    updated: function() {
        var trigger_element = this.get_element(),
        show_event_type = this.get_show_helper_text_event(),
        hide_event_type = this.get_hide_helper_text_event();

        this.bgiframe(this.get_helper_element());

        if (show_event_type != hide_event_type) {
            this.delegates["show_event"] = Function.createDelegate(this, this.onShowEvent);
            this.delegates["hide_event"] = Function.createDelegate(this, this.onHideEvent);
            this.delegates["checked_event"] = Function.createDelegate(this, this.onChecked);

            if (show_event_type == FirstLook.Common.WebControls.Extenders.HelpTextEventTrigger.None ||
            show_event_type == FirstLook.Common.WebControls.Extenders.HelpTextEventTrigger.Visible
            ) {
                this.set_helper_element_hidden(false);
            } else
            if (show_event_type == FirstLook.Common.WebControls.Extenders.HelpTextEventTrigger.Checked) {
                this.add_handler(trigger_element, "click", this.delegates["checked_event"]);
                this.set_helper_element_hidden( !! !this.get_element().checked);
            } else {
                this.add_handler(trigger_element, this.help_text_event_trigger_type(show_event_type), this.delegates["show_event"]);
            }

            if (hide_event_type != FirstLook.Common.WebControls.Extenders.HelpTextEventTrigger.None &&
            hide_event_type != FirstLook.Common.WebControls.Extenders.HelpTextEventTrigger.Visible &&
            hide_event_type != FirstLook.Common.WebControls.Extenders.HelpTextEventTrigger.Checked
            ) {
                this.add_handler(trigger_element, this.help_text_event_trigger_type(hide_event_type), this.delegates["hide_event"]);
            }
        } else {
            this.delegates["toggle_event"] = Function.createDelegate(this, this.onToggleEvent);

            if (show_event_type == FirstLook.Common.WebControls.Extenders.HelpTextEventTrigger.None) {
                this.set_helper_element_hidden(false);
            } else {
                this.add_handler(trigger_element, this.help_text_event_trigger_type(show_event_type), this.delegates["toggle_event"]);
            }
        }

        if (this.get_helper_element_hidden()) {
            this.hide_helper_element();
        } else {
            this.show_helper_element();
        }

        FirstLook.Common.WebControls.Extenders.HelpTextExtender.callBaseMethod(this, "updated");
    },
    dispose: function() {
        this.remove_propertyChanged(this.delegates['property_changed']);

        this.remove_handlers();

        this.delegates = undefined;

        FirstLook.Common.WebControls.Extenders.HelpTextExtender.callBaseMethod(this, "dispose");
    },
    help_text_event_trigger_type: function(type) {
        var event_type;

        if (type == FirstLook.Common.WebControls.Extenders.HelpTextEventTrigger.OnFocus) {
            event_type = "focus";
        } else
        if (type == FirstLook.Common.WebControls.Extenders.HelpTextEventTrigger.OnBlur) {
            event_type = "blur";
        } else
        if (type == FirstLook.Common.WebControls.Extenders.HelpTextEventTrigger.OnMouseOver) {
            event_type = "mouseover";
        } else
        if (type == FirstLook.Common.WebControls.Extenders.HelpTextEventTrigger.OnMouseOut) {
            event_type = "mouseout";
        } else
        if (type == FirstLook.Common.WebControls.Extenders.HelpTextEventTrigger.OnSelect) {
            event_type = "select";
        } else
        if (type == FirstLook.Common.WebControls.Extenders.HelpTextEventTrigger.OnChange) {
            event_type = "change";
        } else
        if (type == FirstLook.Common.WebControls.Extenders.HelpTextEventTrigger.OnClick) {
            event_type = "click";
        } else
        if (type == FirstLook.Common.WebControls.Extenders.HelpTextEventTrigger.OnDblClick) {
            event_type = "dbl_click";
        } else
        if (type == FirstLook.Common.WebControls.Extenders.HelpTextEventTrigger.Checked) {
            event_type = "click";
        }

        return event_type;
    },
    // =================
    // = Events: start =
    // =================
    onShowEvent: function(e) {
        this.set_xy_for_event(e);
        this.set_helper_element_hidden(false);
    },
    onHideEvent: function(e) {
        this.set_xy_for_event(e);
        this.set_helper_element_hidden(true);
    },
    onToggleEvent: function(e) {
        this.set_xy_for_event(e);
        this.set_helper_element_hidden(!this.get_helper_element_hidden());
    },
    onChecked: function(e) {
        this.set_xy_for_event(e);
        this.set_helper_element_hidden( !! !this.get_element().checked);
    },
    onPropertyChanged: function(sender, args) {
        var property = args.get_propertyName();

        Sys.Debug.assert(!!!FirstLook.Common.WebControls.Extenders.HelpTextExtender["debug_" + property], property);

        if (property == "helper_element_hidden") {
            if (sender.get_helper_element_hidden()) {
                this.hide_helper_element();
            } else {
                this.show_helper_element();
            }
        } else
        if (property == "mouse_y" && !!this.get_show_help_at_mouse_y()) {
            this.update_helper_position_y(sender.get_mouse_y());
        } else
        if (property == "mouse_x" && !!this.get_show_help_at_mouse_x()) {
            this.update_helper_position_x(sender.get_mouse_x());
        }

        if ( !! FirstLook.Common.WebControls.Extenders.HelpTextExtender.Trace || !!FirstLook.Common.WebControls.Extenders.HelpTextExtender["trace_" + property]) {
            Sys.Debug.traceDump(sender["get_" + property](), sender.get_id() + ":" + property);
        };
    },
    // ==================
    // = Methods: start =
    // ==================
    show_helper_element: function() {
        this.get_helper_element().style.display = "block";
        this.get_helper_element().style.visibility = "visible";
    },
    hide_helper_element: function() {
        this.get_helper_element().style.display = "none";
        this.get_helper_element().style.visibility = "hidden";
    },
    update_helper_position_y: function(y) {
        this.get_helper_element().style.top = y;
    },
    update_helper_position_x: function(x) {
        this.get_helper_element().style.left = x;
    },
    // =========================
    // = Helper Methods: start =
    // =========================
    create_getter_setter: function(property) {
        // =================================================================
        // = Generic Function to create AJAX.net style getters and setters =
        // =================================================================
        if (this["get_" + property] === undefined) {
            this["get_" + property] = (function(thisProp) {
                return function get_property() {
                    return this[thisProp];
                };
            })("_" + property);
        };
        if (this["set_" + property] === undefined) {
            this["set_" + property] = (function(thisProp, raiseType) {
                return function set_property(value) {
                    if (value !== this[thisProp]) {
                        this[thisProp] = value;
                        this.raisePropertyChanged(raiseType);
                    };
                };
            })("_" + property, property);
        };
    },
    add_handler: function(el, type, fn) {
        if (this._eventCache === undefined) {
            this._eventCache = [];
        }
        this._eventCache.push([el, type, fn]);
        $addHandler(el, type, fn);
    },
    remove_handlers: function() {
        if (this._eventCache !== undefined) {
            for (var i = 0, l = this._eventCache.length; i < l; i++) {
                $removeHandler(this._eventCache[i][0], this._eventCache[i][1], this._eventCache[i][2]);
                this._eventCache[i][0] = undefined;
                this._eventCache[i][1] = undefined;
                this._eventCache[i][2] = undefined;
            };
            delete this._eventCache;
        };
    },
    set_xy_for_event: function(e) {
        this.set_mouse_x(e.clientX + this.get_scroll_x() - 20);
        this.set_mouse_y(e.clientY + this.get_scroll_y() - 20);
    },
    get_scroll_y: function() {
        var y = 0;
        if (document.body && document.body.scrollTop) {
            y = document.body.scrollTop;
        } else if (document.documentElement && document.documentElement.scrollTop) {
            y = document.documentElement.scrollTop;
        }

        return y;
    },
    get_scroll_x: function() {
        var x = 0;
        if (document.body && document.body.scrollLeft) {
            x = document.body.scrollLeft;
        } else if (document.documentElement && document.documentElement.scrollLeft) {
            x = document.documentElement.scrollLeft;
        }

        return x;
    },
    bgiframe: function(el) {
        // ========================================================================
        // = This form of insert is neccissary to avoid 404's and unsecure errors =
        // ========================================================================
        var html = '<iframe class="bgiframe"frameborder="0"tabindex="-1"src="javascript:false;"style="display:block;position:absolute;z-index:-1;filter:Alpha(Opacity=\'0\');top:0;left:0;bottom:0;right:0;"/>';
        var bgiframe = document.createElement(html);
        el.insertBefore(bgiframe, el.firstChild);
    }
};

FirstLook.Common.WebControls.Extenders.HelpTextEventTrigger = function() {
    throw Error.invalidOperation();
};
FirstLook.Common.WebControls.Extenders.HelpTextEventTrigger.prototype = {
    None: 1,
    OnFocus: 2,
    OnBlur: 4,
    OnMouseOver: 8,
    OnMouseOut: 16,
    OnSelect: 32,
    OnChange: 64,
    OnClick: 128,
    OnDblClick: 256,
    Checked: 512,
    Visible: 1024
};
if (FirstLook.Common.WebControls.Extenders.HelpTextEventTrigger.registerEnum != undefined)
 FirstLook.Common.WebControls.Extenders.HelpTextEventTrigger.registerEnum("FirstLook.Common.WebControls.Extenders.HelpTextEventTrigger", false);

if (FirstLook.Common.WebControls.Extenders.HelpTextExtender != undefined)
 FirstLook.Common.WebControls.Extenders.HelpTextExtender.registerClass('FirstLook.Common.WebControls.Extenders.HelpTextExtender', Sys.UI.Behavior);