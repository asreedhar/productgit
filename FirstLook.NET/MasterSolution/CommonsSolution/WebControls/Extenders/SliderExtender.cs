using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Drawing.Design;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("FirstLook.Common.WebControls.Extenders.SliderBehavior.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Extenders.SliderBehavior.debug.js", "text/javascript")]

namespace FirstLook.Common.WebControls.Extenders
{
    [TargetControlType(typeof (TextBox)),
     TargetControlType(typeof (DropDownList))]
    public sealed class SliderExtender : ExtenderControl
    {
        /// <summary>
        ///  Get/Set the minimum value available for input.
        /// </summary>
        [DefaultValue(0)]
        public double Minimum
        {
            get { return GetPropertyValue<double>("Minimum", 0); }
            set { SetPropertyValue("Minimum", value); }
        }

        /// <summary>
        /// Get/Set the maximum value available for input.
        /// </summary>
        [DefaultValue(100)]
        public double Maximum
        {
            get { return GetPropertyValue<double>("Maximum", 100); }
            set { SetPropertyValue("Maximum", value); }
        }

        /// <summary>
        /// Get/Set the CSS class for the slider's rail element.
        /// </summary>
        [DefaultValue("")]
        public string RailCssClass
        {
            get { return GetPropertyValue("RailCssClass", ""); }
            set { SetPropertyValue("RailCssClass", value); }
        }

        /// <summary>
        /// Get/Set the URL for the image to display in the slider's handle.
        /// </summary>
        [DefaultValue(""),
         Editor(
             "System.Web.UI.Design.ImageUrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
             , typeof (UITypeEditor)),
         UrlProperty,
         SuppressMessage("Microsoft.Design", "CA1056:UriPropertiesShouldNotBeStrings",
             Justification = "Following ASP.NET pattern")]
        public string HandleImageUrl
        {
            get { return GetPropertyValue("HandleImageUrl", ""); }
            set { SetPropertyValue("HandleImageUrl", value); }
        }

        /// <summary>
        /// Get/Set the CSS class for the handle element.
        /// </summary>
        [DefaultValue("")]
        public string HandleCssClass
        {
            get { return GetPropertyValue("HandleCssClass", ""); }
            set { SetPropertyValue("HandleCssClass", value); }
        }

        /// <summary>
        /// Enable/disable the handle animation played when the user clicks
        /// on the slider's rail.
        /// </summary>
        [DefaultValue(false)]
        public bool EnableHandleAnimation
        {
            get { return GetPropertyValue("EnableHandleAnimation", false); }
            set { SetPropertyValue("EnableHandleAnimation", value); }
        }

        /// <summary>
        /// Get/Set the number of discrete values available for input. The steps are equi-spaced
        /// within the slider's range of values.
        /// </summary>
        [DefaultValue(0)]
        public int Steps
        {
            get { return GetPropertyValue("Steps", 0); }
            set { SetPropertyValue("Steps", value); }
        }

        /// <summary>
        /// The orientation of the slider. A slider can be rendered horizontally or vertically.
        /// </summary>
        [DefaultValue(SliderOrientation.Horizontal)]
        public SliderOrientation Orientation
        {
            get { return GetPropertyValue("Orientation", SliderOrientation.Horizontal); }
            set { SetPropertyValue("Orientation", value); }
        }

        /// <summary>
        /// Get/Set the number of decimal digits in the slider's value. A value 
        /// of 0 means an integer value.
        /// </summary>
        [DefaultValue(0)]
        public int Decimals
        {
            get { return GetPropertyValue("Decimals", 0); }
            set { SetPropertyValue("Decimals", value); }
        }

        /// <summary>
        /// Get/Set the ID of the server control to which the slider's value is bound.
        /// </summary>
        /// <remarks>
        /// The server control should be a TextBox or Label control.
        /// </remarks>
        [IDReferenceProperty(typeof (WebControl))]
        [DefaultValue("")]
        [SuppressMessage("Microsoft.Naming", "CA1706:ShortAcronymsShouldBeUppercase",
            Justification = "Following ASP.NET AJAX pattern")]
        public string BoundControlID
        {
            get { return GetPropertyValue("BoundControlID", ""); }
            set { SetPropertyValue("BoundControlID", value); }
        }

        // TODO: Give Me some reasonable meta data
        public string BoundControlFormat
        {
            get { return GetPropertyValue("BoundControlFormat", "undefined"); }
            set { SetPropertyValue("BoundControlFormat", value); }
        }

        // TODO: Give Me some reasonable meta data
        public bool Enabled
        {
            get { return GetPropertyValue("Enabled", true); }
            set { SetPropertyValue("Enabled", value); }
        }

        /// <summary>
        /// This property allows to specify a width (or an height, in case of a vertical slider)
        /// when using the slider's default layout.
        /// </summary>
        [DefaultValue(150)]
        public int Length
        {
            get { return GetPropertyValue("Length", 150); }
            set { SetPropertyValue("Length", value); }
        }

        /// <summary>
        /// This property allows to raise a client-side change event on the
        /// extended TextBox. This is useful for example to trigger an UpdatePanel's update
        /// when the slider's value changes.
        /// If set to true, the change event will be raised only when the user releases the 
        /// left mouse button on the slider. 
        /// If set to false, the change event will be raised whenever the slider's value
        /// changes.
        /// This property is set to true by default to prevent flooding of requests to 
        /// the web server.
        /// </summary>
        [DefaultValue(true)]
        public bool RaiseChangeOnlyOnMouseUp
        {
            get { return GetPropertyValue("RaiseChangeOnlyOnMouseUp", true); }
            set { SetPropertyValue("RaiseChangeOnlyOnMouseUp", value); }
        }

        /// <summary>
        /// This property allows to specify some text to display in the tooltip of 
        /// the slider's handle. If the text contains the {0} placeholder, it will 
        /// be replaced by the current value of the slider.
        /// </summary>
        [DefaultValue("")]
        public string TooltipText
        {
            get { return GetPropertyValue("TooltipText", String.Empty); }
            set { SetPropertyValue("TooltipText", value); }
        }        

        private T GetPropertyValue<T>(string propertyName, T nullValue)
        {
            if (ViewState[propertyName] == null)
            {
                return nullValue;
            }
            return (T) ViewState[propertyName];
        }

        private void SetPropertyValue<T>(string propertyName, T value)
        {
            ViewState[propertyName] = value;
        }

        protected override IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
        {
            if (targetControl.Visible)
            {
                ScriptBehaviorDescriptor descriptor =
                    new ScriptBehaviorDescriptor("AjaxControlToolkit.SliderBehavior", targetControl.ClientID);
                descriptor.ID = ID;
                descriptor.AddProperty("Minimum", Minimum);
                descriptor.AddProperty("Maximum", Maximum);
                descriptor.AddProperty("RailCssClass", RailCssClass);
                descriptor.AddProperty("HandleImageUrl", HandleImageUrl);
                descriptor.AddProperty("HandleCssClass", HandleCssClass);
                descriptor.AddProperty("EnableHandleAnimation", EnableHandleAnimation);
                descriptor.AddProperty("Steps", Steps);
                descriptor.AddProperty("Orientation", Orientation);
                descriptor.AddProperty("BoundControlID", BoundControlID);
                descriptor.AddProperty("BoundControlFormat", BoundControlFormat);
                descriptor.AddProperty("Length", Length);
                descriptor.AddProperty("RaiseChangeOnlyOnMouseUp", RaiseChangeOnlyOnMouseUp);
                descriptor.AddProperty("TooltipText", TooltipText);
                descriptor.AddProperty("Enabled", Enabled);

                return new ScriptDescriptor[] {descriptor};
            }
            else
            {
                return new ScriptDescriptor[] {};
            }
        }

        protected override IEnumerable<ScriptReference> GetScriptReferences()
        {
            return new ScriptReference[]
                {
                    new ScriptReference("FirstLook.Common.WebControls.Timer.js",
                                        "FirstLook.Common.WebControls"),
                    new ScriptReference("FirstLook.Common.WebControls.DragDropScripts.js",
                                        "FirstLook.Common.WebControls"),
                    new ScriptReference("FirstLook.Common.WebControls.Animations.Animation.js",
                                        "FirstLook.Common.WebControls"),
                    new ScriptReference("FirstLook.Common.WebControls.Animations.PropertyAnimation.js",
                                        "FirstLook.Common.WebControls"),
                    new ScriptReference("FirstLook.Common.WebControls.Animations.InterpolatedAnimation.js",
                                        "FirstLook.Common.WebControls"),
                    new ScriptReference("FirstLook.Common.WebControls.Animations.LengthAnimation.js",
                                        "FirstLook.Common.WebControls"),
                    new ScriptReference("FirstLook.Common.WebControls.Extenders.SliderBehavior.js",
                                        "FirstLook.Common.WebControls")
                };
        }
    }
}