using System;
using System.Collections;
using System.Web.UI;

namespace FirstLook.Common.WebControls
{
    internal sealed class ReadOnlyDataSource : IDataSource
    {
        // Fields
        private readonly string _dataMember;
        private readonly object _dataSource;
        private static readonly string[] ViewNames = new string[0];

        // Events
        event EventHandler IDataSource.DataSourceChanged
        {
            add { }
            remove { }
        }

        // Methods
        public ReadOnlyDataSource(object dataSource, string dataMember)
        {
            _dataSource = dataSource;
            _dataMember = dataMember;
        }

        DataSourceView IDataSource.GetView(string viewName)
        {
            IDataSource source = _dataSource as IDataSource;
            if (source != null)
            {
                return source.GetView(viewName);
            }
            return
                new ReadOnlyDataSourceView(this, _dataMember,
                                           DataSourceHelper.GetResolvedDataSource(_dataSource, _dataMember));
        }

        ICollection IDataSource.GetViewNames()
        {
            return ViewNames;
        }
    }
}