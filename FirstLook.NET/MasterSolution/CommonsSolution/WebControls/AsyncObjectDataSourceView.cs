using System;
using System.Collections;
using System.Web.Compilation;
using System.Web.UI;
using FirstLook.Common.Core;

namespace FirstLook.Common.WebControls
{
    public class AsyncObjectDataSourceView : AsyncDataSourceView
    {
        private readonly AsyncObjectDataSourceControl owner;
        private string typeName;
        private IDataService service;

        public AsyncObjectDataSourceView(AsyncObjectDataSourceControl owner, string viewName)
            : base(owner, viewName)
        {
            this.owner = owner;
        }

        public string TypeName
        {
            get
            {
                if (typeName == null)
                {
                    return string.Empty;
                }
                return typeName;
            }
            set
            {
                if (TypeName != value)
                {
                    typeName = value;
                    OnDataSourceViewChanged(EventArgs.Empty);
                }
            }
        }

        private IDataService GetDataService()
        {
            if (!string.IsNullOrEmpty(typeName))
            {
                Type type = BuildManager.GetType(typeName, true, true);

                if (type.GetInterface(typeof(IDataService).FullName) != null)
                {
                    if (service == null || !service.GetType().Equals(type))
                    {
                        service = (IDataService)type.GetConstructor(Type.EmptyTypes).Invoke(new object[0]);
                        service.Initialize(owner.GetParameterValue);
                    }
                }
            }

            if (service != null)
                return service;

            throw new ArgumentException("DataSource_InvalidTypeName");
        }

        #region AsyncDataSourceView

        /*
         * Delegates are simpler but much slower than a simple call to ThreadPool.QueueUserWorkItem
         * as it is the basis for a general purpose messaging/remoting infrastructure.  As such we
         * leave it to the discretion of the service to provide its own async operations. It seems
         * the same applies to AsyncOperationManager. Also, to make the approach taken work you need
         * to edit the value the processModel element in the machine.config to read (something similar to)
         * <processModel autoConfig="true" minWorkerThreads="10" maxWorkerThreads="20" />
         * where the default value for minWorkerThreads is 1 so it takes a really long time to build
         * a proper pool of threads. Yes, setting the min value artifically high has performance
         * implications but it performs like shit when you start up otherwise. See
         * http://support.microsoft.com/?id=821268
         */

        /*
         * Repeater directly calls ExecuteSelect. Wow. That sucks big time. As if it were hard to
         * call the Select method. You suck Microsoft.
         */

        protected override IEnumerable ExecuteSelect(DataSourceSelectArguments arguments)
        {
            arguments.RaiseUnsupportedCapabilitiesError(this);

            return GetDataService().GetData(Convert(arguments));
        }

        protected override IAsyncResult BeginExecuteSelect(DataSourceSelectArguments arguments, AsyncCallback asyncCallback, object asyncState)
        {
            arguments.RaiseUnsupportedCapabilitiesError(this);

            return GetDataService().BeginGetData(Convert(arguments), asyncCallback, asyncState);
        }

        protected override IEnumerable EndExecuteSelect(IAsyncResult asyncResult)
        {
            return GetDataService().EndGetData(asyncResult);
        }

        public override bool CanPage
        {
            get { return GetDataService().CanPage; }
        }

        public override bool CanRetrieveTotalRowCount
        {
            get { return GetDataService().CanRetrieveTotalRowCount; }
        }

        public override bool CanSort
        {
            get { return GetDataService().CanSort; }
        }
        
        #endregion

        internal void RaiseChangedEvent()
        {
            OnDataSourceViewChanged(EventArgs.Empty);
        }

        static internal DataServiceArguments Convert(DataSourceSelectArguments arguments)
        {
            return new DataServiceArguments(
                arguments.MaximumRows,
                arguments.RetrieveTotalRowCount,
                arguments.SortExpression,
                arguments.StartRowIndex,
                arguments.TotalRowCount);
        }
    }
}