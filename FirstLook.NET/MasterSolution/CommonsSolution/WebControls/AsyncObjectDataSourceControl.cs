using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing.Design;
using System.Web.UI;
using System.Web.UI.Design.WebControls;
using System.Web.UI.WebControls;

namespace FirstLook.Common.WebControls
{
    [DefaultProperty("TypeName"), ParseChildren(true), PersistChildren(false)]
    public class AsyncObjectDataSourceControl : AsyncDataSourceControl
    {
        private ParameterCollection parameters;
        private AsyncObjectDataSourceView view;
        private string[] viewNames;

        [DefaultValue(null),
         Editor(typeof(ParameterCollectionEditor), typeof(UITypeEditor)),
         MergableProperty(false),
         PersistenceMode(PersistenceMode.InnerProperty),
         Category("Data")]
        public ParameterCollection Parameters
        {
            get
            {
                if (parameters == null)
                {
                    parameters = new ParameterCollection();
                    parameters.ParametersChanged += OnParametersChanged;

                    if (IsTrackingViewState)
                    {
                        ((IStateManager)parameters).TrackViewState();
                    }
                }
                return parameters;
            }
        }

        [DefaultValue(""),
         MergableProperty(false),
         Category("Data")]
        public string TypeName
        {
            get
            {
                return GetView().TypeName;
            }
            set
            {
                GetView().TypeName = value;
            }
        }

        private AsyncObjectDataSourceView GetView()
        {
            if (view == null)
            {
                view = new AsyncObjectDataSourceView(this, "DefaultView");
            }
            return view;
        }

        protected override DataSourceView GetView(string viewName)
        {
            if ((viewName == null) || ((viewName.Length != 0) && !string.Equals(viewName, "DefaultView", StringComparison.OrdinalIgnoreCase)))
            {
                throw new ArgumentException("DataSource_InvalidViewName", "viewName");
            }
            return GetView();
        }

        protected override ICollection GetViewNames()
        {
            if (viewNames == null)
            {
                viewNames = new string[] { "DefaultView" };
            }
            return viewNames;
        }

        internal object GetParameterValue(string parameterName)
        {
            if (parameters != null)
            {
                Parameter parameter = parameters[parameterName];
                if (parameter != null)
                {
                    IOrderedDictionary parameterValues = parameters.GetValues(Context, this);
                    return parameterValues[parameter.Name];
                }
            }

            return null;
        }

        protected override void LoadViewState(object savedState)
        {
            object baseState = null;

            if (savedState != null)
            {
                Pair p = (Pair)savedState;
                baseState = p.First;

                if (p.Second != null)
                {
                    ((IStateManager)Parameters).LoadViewState(p.Second);
                }
            }

            base.LoadViewState(baseState);
        }

        protected override void OnInit(EventArgs e)
        {
            Page.LoadComplete += OnPageLoadComplete;
        }

        private void OnPageLoadComplete(object sender, EventArgs e)
        {
            Parameters.UpdateValues(Context, this);
        }

        private void OnParametersChanged(object sender, EventArgs e)
        {
            GetView().RaiseChangedEvent();
        }

        protected override object SaveViewState()
        {
            object baseState = base.SaveViewState();
            object parameterState = null;

            if (parameters != null)
            {
                parameterState = ((IStateManager)parameters).SaveViewState();
            }

            if ((baseState != null) || (parameterState != null))
            {
                return new Pair(baseState, parameterState);
            }

            return null;
        }

        protected override void TrackViewState()
        {
            base.TrackViewState();
            if (view != null)
                if (parameters != null)
                    ((IStateManager)parameters).TrackViewState();
        }
    }
}