using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstLook.Common.WebControls
{
    public class Dimension : Component, IStateManager
    {
        internal const string SetBitsKey = "_!SB";

        internal const int PROP_WIDTH  = 1 << 1;
        internal const int PROP_HEIGHT = 1 << 2;

        private StateBag statebag;
        private bool marked;
        private int markedBits;
        private int setBits;

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        protected internal StateBag ViewState
        {
            get
            {
                if (statebag == null)
                {
                    statebag = new StateBag(false);

                    if (IsTrackingViewState)
                    {
                        ((IStateManager)statebag).TrackViewState();
                    }
                }

                return statebag;
            }
        }

        [NotifyParentProperty(true), DefaultValue(typeof(Unit), ""), Description("Style_Height"), Category("Layout")]
        public Unit Height
        {
            get
            {
                if (IsSet(PROP_HEIGHT))
                {
                    return (Unit)ViewState["Height"];
                }
                return Unit.Empty;
            }
            set
            {
                if (value.Value < 0.0)
                {
                    throw new ArgumentOutOfRangeException("value", "Style_InvalidHeight");
                }
                ViewState["Height"] = value;
                SetBit(PROP_HEIGHT);
            }
        }

        [NotifyParentProperty(true), Category("Layout"), DefaultValue(typeof(Unit), ""), Description("Style_Width")]
        public Unit Width
        {
            get
            {
                if (IsSet(PROP_WIDTH))
                {
                    return (Unit)ViewState["Width"];
                }
                return Unit.Empty;
            }
            set
            {
                if (value.Value < 0.0)
                {
                    throw new ArgumentOutOfRangeException("value", "Style_InvalidWidth");
                }
                ViewState["Width"] = value;
                SetBit(PROP_WIDTH);
            }
        }

        protected bool IsSet(int bit)
        {
            return ((setBits & bit) != 0);
        }

        protected virtual void SetBit(int bit)
        {
            setBits |= bit;

            if (IsTrackingViewState)
            {
                markedBits |= bit;
            }
        }

        #region IStateManager Members

        protected bool IsTrackingViewState
        {
            get { return marked; }
        }

        protected void LoadViewState(object state)
        {
            if (state != null)
            {
                ((IStateManager)ViewState).LoadViewState(state);
            }
            if (statebag != null)
            {
                object obj2 = ViewState[SetBitsKey];
                if (obj2 != null)
                {
                    markedBits = (int)obj2;
                    setBits |= markedBits;
                }
            }
        }

        protected object SaveViewState()
        {
            if (statebag != null)
            {
                if (markedBits != 0)
                {
                    ViewState[SetBitsKey] = markedBits;
                }

                return ((IStateManager)ViewState).SaveViewState();
            }

            return null;
        }

        protected void TrackViewState()
        {
            ((IStateManager)ViewState).TrackViewState();
            marked = true;
        }

        bool IStateManager.IsTrackingViewState
        {
            get { return IsTrackingViewState; }
        }

        void IStateManager.LoadViewState(object state)
        {
            LoadViewState(state);
        }

        object IStateManager.SaveViewState()
        {
            return SaveViewState();
        }

        void IStateManager.TrackViewState()
        {
            TrackViewState();
        }

        #endregion
    }
}