using System.Web.UI;

namespace FirstLook.Common.WebControls
{
    public interface IAsyncDataSource : IDataSource
    {
        bool IsAsync { get; }
    }
}