using System.Globalization;
using System.Web;
using System.Web.UI;

namespace FirstLook.Common.WebControls
{
    public static class DataBoundControlHelper
    {
        public static Control FindControl(Control control, string controlID)
        {
            Control namingContainer = control;
            Control control3 = null;
            if (control != control.Page)
            {
                while ((control3 == null) && (namingContainer != control.Page))
                {
                    namingContainer = namingContainer.NamingContainer;
                    if (namingContainer == null)
                    {
						throw new HttpException(string.Format(CultureInfo.InvariantCulture, "DataBoundControlHelper_NoNamingContainer: {0} / {1}", new object[] { control.GetType().Name, control.ID }));
                    }
                    control3 = namingContainer.FindControl(controlID);
                }
                return control3;
            }
            return control.FindControl(controlID);
        }

        public static IDataSource FindDataSourceControl(Control dataBoundControl, string dataSourceID)
        {
            IDataSource source = null;
            if (dataSourceID.Length != 0)
            {
                Control control = FindControl(dataBoundControl, dataSourceID);
                if (control == null)
                {
					throw new HttpException(string.Format(CultureInfo.InvariantCulture, "DataControl_DataSourceDoesntExist: {0}", new object[] { dataSourceID }));
                }
                source = control as IDataSource;
                if (source == null)
                {
					throw new HttpException(string.Format(CultureInfo.InvariantCulture, "DataControl_DataSourceIDMustBeDataControl: {0}", new object[] { dataSourceID }));
                }
            }
            return source;
        }
    }
}
