using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Timers;
using System.Web.UI;
using System.Web.UI.Adapters;
using System.Web.UI.HtmlControls;
using System.Xml;
using Timer=System.Timers.Timer;

namespace FirstLook.Common.WebControls.Adapters
{
    public class HtmlHeadAdapter : ControlAdapter
    {
        private delegate string ResolveUrlFunction(string path);

        protected override void Render(HtmlTextWriter writer)
        {
            HtmlHead head = (HtmlHead) Control;

            writer.RenderBeginTag(HtmlTextWriterTag.Head);

            #region Non Theme CSS Controls

            for (int i = 0, l = head.Controls.Count; i < l; i++)
            {
                Control control = head.Controls[i];

                HtmlLink link = control as HtmlLink;

                if (link != null)
                {
                    if (!link.Href.StartsWith("~/App_Themes/"))
                    {
                        control.RenderControl(writer);
                    }
                }
                else
                {
                    control.RenderControl(writer);
                }

                writer.WriteLine();
            }

            #endregion

            #region Theme CSS

            string themeName = Page.Theme;

            if (string.IsNullOrEmpty(themeName))
            {
                themeName = Page.StyleSheetTheme;
            }

            if (!string.IsNullOrEmpty(themeName))
            {
                CssControlCollection collection = GetThemeLinks(Page.Request.PhysicalApplicationPath, themeName, Page.ResolveUrl);
                collection.LockObject.AcquireReaderLock(Timeout.Infinite);
                try
                {
                    for (int i = 0, l = collection.Count; i < l; i++)
                    {
                        collection[i].RenderControl(writer);

                        writer.WriteLine();
                    }
                }
                finally
                {
                    collection.LockObject.ReleaseReaderLock();
                }
            }

            #endregion

            writer.RenderEndTag();
        }

        private static readonly Dictionary<string, CssControlCollection> themes = new Dictionary<string, CssControlCollection>();

        private static CssControlCollection GetThemeLinks(string path, string themeName, ResolveUrlFunction resolveUrl)
        {
            CssControlCollection result;

            lock (themes)
            {
                if (themes.ContainsKey(themeName))
                {
                    result = themes[themeName];
                }
                else
                {
					string filename = string.Format(CultureInfo.InvariantCulture, "{0}App_Themes{1}{2}{1}css.xml", path, Path.DirectorySeparatorChar, themeName);

                    result = new CssControlCollection(new FileInfo(filename), resolveUrl);

                    themes.Add(themeName, result);
                }
            }

            return result;
        }

        sealed class CssControlCollection : CollectionBase
        {
            ResolveUrlFunction resolveUrl;
            private readonly FileInfo fileInfo;
            private DateTime lastWriteTime;
            private readonly Timer timer;
            private readonly ReaderWriterLock mutex = new ReaderWriterLock();

            public CssControlCollection(FileInfo fileInfo, ResolveUrlFunction resolveUrl)
            {
                this.fileInfo = fileInfo;
                this.resolveUrl = resolveUrl;

                timer = new Timer();
                timer.Interval = new TimeSpan(0, 1, 0).TotalMilliseconds;
                timer.Elapsed += OnElapsed;

                Load();
            }

            public Control this[int index]
            {
                get
                {
                    return (Control) List[index];
                }
            }

            public ReaderWriterLock LockObject
            {
                get { return mutex; }
            }

            protected override void OnValidate(Object value)
            {
                bool isCorrectType = (value.GetType() == typeof (HtmlLink));
                isCorrectType |= (value.GetType() == typeof(ConditionalComment));
                if (!isCorrectType)
                    throw new ArgumentException("value must be of type HtmlLink or ConditionalComment.", "value");
            }

            private void OnElapsed(object source, ElapsedEventArgs e)
            {
                timer.Stop();

                mutex.AcquireReaderLock(Timeout.Infinite);
                try
                {
                    if (File.GetLastWriteTime(fileInfo.FullName).CompareTo(lastWriteTime) > 0)
                    {
                        LockCookie cookie = mutex.UpgradeToWriterLock(Timeout.Infinite);
                        try
                        {
                            Load();
                        }
                        finally
                        {
                            mutex.DowngradeFromWriterLock(ref cookie);
                        }
                    }
                }
                finally
                {
                    mutex.ReleaseReaderLock();
                }
            }

            private void Load()
            {
                List.Clear();

                XmlDocument document = new XmlDocument();

                document.Load(fileInfo.FullName);

                lastWriteTime = File.GetLastWriteTime(fileInfo.FullName);

                XmlNodeList linksNodes = document.GetElementsByTagName("links")[0].ChildNodes;

                for (int i = 0, l = linksNodes.Count; i < l; i++)
                {
                    HtmlLink link = new HtmlLink();

                    XmlNode linkElement = linksNodes[i];

                    link.Href = resolveUrl("~/App_Themes/"
                                + fileInfo.Directory.Name
                                + "/"
                                + linkElement.Attributes["href"].Value);

                    XmlAttribute nameAtt = linkElement.Attributes["title"];
                    if (nameAtt != null)
                    {
                        link.Attributes.Add("title", nameAtt.Value);
                    }

                    link.Attributes.Add("type", linkElement.Attributes["type"].Value);
                    link.Attributes.Add("rel", linkElement.Attributes["rel"].Value);

                    XmlAttribute mediaAtt = linkElement.Attributes["media"];
                    if (mediaAtt != null)
                    {
                        link.Attributes.Add("media", mediaAtt.Value);    
                    }
                    
                    XmlAttribute conditionAttr = linkElement.Attributes["condition"];

                    if (conditionAttr != null && !string.IsNullOrEmpty(conditionAttr.Value))
                    {
                        List.Add(new ConditionalComment(link, conditionAttr.Value));
                    }
                    else
                    {
                        List.Add(link);
                    }
                }

                timer.Start();
            }
        }

        sealed class ConditionalComment : Control
        {
            private readonly HtmlLink link;
            private readonly string condition;

            public ConditionalComment(HtmlLink link, string condition)
            {
                if (string.IsNullOrEmpty(condition))
                    throw new ArgumentNullException("condition");

                this.link = link;
                this.condition = condition;
            }

            public override bool HasControls()
            {
                return true;
            }

            protected override void CreateChildControls()
            {
                Controls.Add(link);
            }

            protected override void Render(HtmlTextWriter writer)
            {
                writer.Write("<!--[");
                writer.Write(condition);
                writer.Write("]>");
                writer.WriteLine();

                link.RenderControl(writer);

                // base.Render(writer);

                writer.WriteLine();
                writer.Write("<![endif]-->");
            }
        }
    }
}