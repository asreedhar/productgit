using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.Adapters;

namespace FirstLook.Common.WebControls.Adapters
{
    public class CheckBoxListAdapter : WebControlAdapter
    {
        protected override void RenderBeginTag(HtmlTextWriter writer)
        {
            if (!string.IsNullOrEmpty(Control.CssClass))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, Control.CssClass);
            }
            writer.RenderBeginTag(HtmlTextWriterTag.Ul);
        }

        protected override void RenderEndTag(HtmlTextWriter writer)
        {
            writer.RenderEndTag();
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            CheckBoxList ButtonList = Control as CheckBoxList;
            if (null != ButtonList)
            {
                writer.Indent++;

                int i = 0;
                foreach (ListItem li in ButtonList.Items)
                {
					string itemID = string.Format(CultureInfo.InvariantCulture, "{0}_{1}", ButtonList.ClientID, i++);

                    #region Li
                    writer.WriteLine();
                    writer.RenderBeginTag(HtmlTextWriterTag.Li);

                    #region Input
                    writer.AddAttribute(HtmlTextWriterAttribute.Id, itemID);
                    writer.AddAttribute(HtmlTextWriterAttribute.Type, "checkbox");
                    writer.AddAttribute(HtmlTextWriterAttribute.Name, string.Format("{0}${1}", ButtonList.UniqueID, i - 1));
                    writer.AddAttribute(HtmlTextWriterAttribute.Value, li.Value);
                    if (li.Selected)
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Checked, "checked");
                    }
                    if (!ButtonList.Enabled || !li.Enabled)
                    {
                        writer.AddAttribute("disabled", "disabled");
                    }
                    writer.RenderBeginTag(HtmlTextWriterTag.Input);
                    writer.RenderEndTag(); // </input>
                    #endregion

                    #region Label
                    writer.AddAttribute("for", itemID);
                    writer.RenderBeginTag(HtmlTextWriterTag.Label);
                    writer.Write(HttpUtility.HtmlEncode(li.Text));
                    writer.RenderEndTag(); // </label>
                    #endregion

                    writer.RenderEndTag();  // </li>
                    #endregion

                    Page.ClientScript.RegisterForEventValidation(ButtonList.UniqueID, li.Value);
                }
                writer.Indent--;
                Page.ClientScript.RegisterForEventValidation(ButtonList.UniqueID);
            }
        }
    }
}