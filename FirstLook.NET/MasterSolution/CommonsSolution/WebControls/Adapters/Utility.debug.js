﻿//  Utility.debug.js
//  Firstlook.Common.WebControls
//  
//  Created by pmonson on 2010-02-12.
//  Copyright 2010 Firstlook Solutions. All rights reserved.
// 
/*
	TODO Add Documentation for methods See: JS Doc
*/
if ( !! Sys) {
    Type.registerNamespace('FirstLook');
    FirstLook.Utility = {    	
        augment: function(receivingClass, givingClass) {
            FirstLook.Utility.addMethods(receivingClass.prototype, givingClass.prototype);
        },
        addModule: function(receivingClass, module) {
            var givingClass = {};
            givingClass.prototype = module;
            FirstLook.Utility.augment(receivingClass, givingClass);
        },
        addMethods: function(receivingClass, givingClass) {
                for (methodName in givingClass) {
                    if (!receivingClass[methodName]) {
                        receivingClass[methodName] = givingClass[methodName];
                    }
                }
        },
        createGetter: function(object, property) {
            // ====================================================
            // = Generic Function to create AJAX.net style getter =
            // ====================================================
            if (object.prototype["get_" + property] === undefined) {
                object.prototype["get_" + property] = (function(thisProp) {
                    return function() {
                        return this[thisProp];
                    };
                })("_" + property);
            };
        },
        createGetters: function(object, properties) {
            for (var i = properties.length - 1; i >= 0; i--) {
                FirstLook.Utility.createGetter(object, properties[i]);
            };
        },
        createSetter: function(object, property) {
            // ====================================================
            // = Generic Function to create AJAX.net style getter =
            // ====================================================
            if (object.prototype["set_" + property] === undefined) {
                object.prototype["set_" + property] = (function(thisProp, raiseType) {
                    return function(value) {
                        if (value !== this[thisProp]) {
                            this[thisProp] = value;
                            this.raisePropertyChanged(raiseType);
                        };
                    };
                })("_" + property, property);
            };
        },
        createSetters: function(object, properties) {
            for (var i = properties.length - 1; i >= 0; i--) {
                FirstLook.Utility.createSetter(object, properties[i]);
            };
        },
        createGetterSetters: function(object, properties) {
        	if (properties instanceof Array) {
        		for (var i = properties.length - 1; i >= 0; i--) {
	                FirstLook.Utility.createGetter(object, properties[i]);
	                FirstLook.Utility.createSetter(object, properties[i]);
            	};
        	};            
        }
    };

    // ===============
    // = Augment Sys =
    // ===============
    (function() {
        var Behaviors = {
            Properties: {
                createGetter: function(property) {
                    FirstLook.Utility.createGetter(this, property);
                },
                createSetter: function(property) {
                    FirstLook.Utility.createSetter(this, property);
                },
                createGetterSetter: function(property) {
                    this.createGetter(property);
                    this.createSetter(property);
                },
                createGetterSetters: function(props) {
                    FirstLook.Utility.createGetterSetters(this, properties);
                }
            },
            EventHandlers: {
                addHandler: function(el, type, fn) {
                    if (this._eventCache === undefined) {
                        this._eventCache = [];
                    }
                    this._eventCache.push([el, type, fn]);
                    $addHandler(el, type, fn);
                },
                removeHandler: function(el, type, fn) {
                    if (this._eventCache !== undefined) {
                        for (var i = 0, l = this._eventCache.length; i < l; i++) {
                        	if (el == this._eventCache[i][0] && type == this._eventCache[i][1] && fn == this._eventCache[i][2]) {
                        		$removeHandler(this._eventCache[i][0], this._eventCache[i][1], this._eventCache[i][2]);
                        		break;
                        	};
                        };
                    }
                },
                removeHandlers: function() {
                    if (this._eventCache !== undefined) {
                        for (var i = 0, l = this._eventCache.length; i < l; i++) {
                            $removeHandler(this._eventCache[i][0], this._eventCache[i][1], this._eventCache[i][2]);
                            this._eventCache[i][0] = undefined;
                            this._eventCache[i][1] = undefined;
                            this._eventCache[i][2] = undefined;
                        };
                        delete this._eventCache;
                    };
                },
                fireEvent: function(el, ev) {
                    if (document.createEvent) {
                        var new_event = document.createEvent('HTMLEvents');
                        new_event.initEvent(ev, true, false);

                        el.dispatchEvent(new_event);
                    }
                    else if (document.createEventObject) {
                        el.fireEvent('on' + ev);
                    }
                },
                createDelegate: function(method_name) {
                    if ( !! !this._delegates) {
                        this.delegates = {};
                    };
                    this.delegates[method_name] = Function.createDelegate(this, this[method_name]);

                    return this.delegates[method_name];
                },
                getDelegate: function(method_name) {
                    if ( !! !this.delegates) {
                        this.delegates = {};
                    }
                    return this.delegates[method_name] || this.createDelegate(method_name);
                },
                clearDelegates: function() {
                    if ( !! this.delegates) {
                        for (delegate in this.delegates) {
                            this.delegates[delegate] = null;
                        }
                    };
                    this.delegates = null;
                },
                onTrace: function(sender, args) {
                    var property = args.get_propertyName();
                    type = (Object.getType instanceof Function) ? Object.getType(sender) : {};

                    Sys.Debug.assert( !! !type["debug_" + property], property);

                    if ( !! type.Trace || !!type["trace_" + property]) {
                        Sys.Debug.traceDump(sender["get_" + property](), (sender.get_id() || type.getName()) + ":" + property);
                    };
                }
            },
            CSS: {
                getComputedStyle: function(el, style) {
                    if ( !! el.currentStyle) {
                        return el.currentStyle[style];
                    } else if ( !! document.defaultView && !!document.defaultView.getComputedStyle) {
                        return document.defaultView.getComputedStyle(el, "")[style];
                    } else {
                        return el.style[style];
                    }
                },
                setStyle: function(el, styles) {
                    if ( !! el) {
                        var key;
                        for (key in styles) {
                            el.style[key] = styles[key];
                        }
                    };
                }
            },
            DOM: {
                findParentNode: function(el, nodeName) {
                    if ( !! !el.parentElement) {
                        return null;
                    } else if (el.parentElement.nodeName === nodeName.toUpperCase()) {
                        return el.parentElement;
                    } else {
                        return this.findParentNode(el.parentElement, nodeName);
                    }
                },
                getTextValue: function(el) {
                    return el.value || el.textContent || el.innerText;
                },
                setTextValue: function(el, text) {
                    if (el.value) {
                        el.value = text;
                    } else if (el.textContent) {
                        el.textContent = text;
                    } else if (el.innerText) {
                        el.innerText = text;
                    } else {
                    	el.appendChild(document.createTextNode(text));
                    }
                }
            }
        };

        FirstLook.Utility.addModule(Sys.Component, Behaviors.Properties);
        FirstLook.Utility.addModule(Sys.Component, Behaviors.EventHandlers);
        FirstLook.Utility.addMethods(Sys.UI.DomElement, Behaviors.CSS);
        FirstLook.Utility.addMethods(Sys.UI.DomElement, Behaviors.DOM);
    })();
    
    Sys.Application.notifyScriptLoaded();
}