﻿using System.Web.UI;

[assembly: WebResource("FirstLook.Common.WebControls.Adapters.Utility.debug.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Adapters.Utility.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Adapters.Page.debug.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Adapters.Page.js", "text/javascript")]
namespace FirstLook.Common.WebControls.Adapters
{
    public class UtilityPageAdapter : VersionAdapter
    {
        #region Properties
        private string JavaScriptSuffix
        {
            get
            {
                return ScriptManager.ScriptMode == ScriptMode.Debug ? ".debug.js" : ".js";
            }
        }

        private ScriptManager _scritpManager;
        private ScriptManager ScriptManager
        {
            get
            {
                if (_scritpManager == null)
                {
                    _scritpManager = ScriptManager.GetCurrent(Page);
                }

                return _scritpManager;
            }
        }

        private ScriptReference[] _scriptReferences;
        private ScriptReference[] ScriptReferences
        {
            get
            {
                if (_scriptReferences == null)
                {
                    const string assembly = "FirstLook.Common.WebControls";
                    string javaScriptSuffix = JavaScriptSuffix;
                    _scriptReferences = new ScriptReference[]
                                            {
                                                new ScriptReference(
                                                    "FirstLook.Common.WebControls.Adapters.Utility" + javaScriptSuffix,
                                                    assembly),
                                                new ScriptReference(
                                                    "FirstLook.Common.WebControls.Adapters.Page" + javaScriptSuffix,
                                                    assembly)
                                            };
                }

                return _scriptReferences;
            }
        } 
        #endregion

        #region Methods
        protected override void OnInit(System.EventArgs e)
        {
            base.OnInit(e);

            AddScriptsToScriptManager();
        }

        private void AddScriptsToScriptManager()
        {
            if (ScriptManager != null)
            {
                int idx = 0;
                foreach (ScriptReference scriptReference in ScriptReferences)
                {
                    ScriptManager.Scripts.Insert(idx++, scriptReference);
                }
            }
        }
        #endregion
    }
}