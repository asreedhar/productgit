﻿using System;
using System.Configuration;
using System.Linq;
using System.Web.UI.Adapters;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using FirstLook.Common.WebControls.UI;

namespace FirstLook.Common.WebControls.Adapters
{
    public class VersionAdapter : PageAdapter
    {
        private const string VersionScript =
            @" window.onload = function(){{ var child = document.createElement('div');child.className = 'version';child.style.position = 'fixed';child.style.right='2px';child.style.bottom='1px';{0}child.innerHTML = 'Build Number: <strong>{1}</strong>';if(document.body != null){{document.body.appendChild(child);}} }};";

        private string CssDisplay
        {
            get
            {
                #if !RELEASE
                return string.Empty;
                #else
                return "child.style.display = 'none';";
                #endif
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            try
            {
                //Get version from assembly
                string version = Page.GetType().BaseType.Assembly.GetName().Version.ToString();

                if (version != null)
                {
                    string script = string.Format(VersionScript, CssDisplay, version);
                    if (!Page.ClientScript.IsStartupScriptRegistered("VersionScript"))
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "VersionScript", script, true);
                }
            }
            catch (Exception)
            {
            }
        }

    }
}
