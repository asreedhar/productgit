//	Page.debug.js
//	FirstLook.Common.Page
//	
//	Created by pmonson on 2010-02-12.
//	Copyright 2010 Firstlook Solutions. All rights reserved.
//
/*
	TODO Document Methods and Classes
*/
if ( !! Sys) {
    Type.registerNamespace('FirstLook.Page');
    // ========
    // = Page =
    // ========
    FirstLook.Page.Base = function() {
        if (this.load) {
            Sys.Application.add_load(this.getDelegate("load"));
        };
        if (this.unload) {
            Sys.Application.add_unload(this.getDelegate("unload"));
        };
        FirstLook.Page.Base.initializeBase(this);
    };
    FirstLook.Page.Base.prototype = {
        initialize: function() {
            FirstLook.Page.Base.callBaseMethod(this, 'initialize');
            this.addHasJavascriptClass();
            this.setBackgroundCache();
            this.createJQueryPanelBridge();
        },
        unload: function() {
            if (this.load) {
                Sys.Application.remove_load(this.getDelegate("load"));
            };
            Sys.Application.remove_unload(this.getDelegate("unload"));
            this.clearDelegates();
        },
        setBackgroundCache: function() {
            try {
                document.execCommand("BackgroundImageCache", false, true);
            } catch(e) {}
        },
        addHasJavascriptClass: function() {
            document.body.className += " has_javascript";
        },
        createJQueryPanelBridge: function() {
            $create(FirstLook.Page.JQueryUpdatePanelBridge, null, null, null, null);
        }
    };
    FirstLook.Page.Base.registerClass("FirstLook.Page.Base", Sys.Component);

    // ==============================
    // = JQuery UpdatePanel Cleaner =
    // ==============================
    FirstLook.Page.JQueryUpdatePanelBridge = function() {
        if ( !! FirstLook.Page.JQueryUpdatePanelBridge.__instance) {
            FirstLook.Page.JQueryUpdatePanelBridge.__instance = this;
            FirstLook.Page.JQueryUpdatePanelBridge.initializeBase(this);
        };
        return FirstLook.Page.JQueryUpdatePanelBridge.__instance;
    };
    FirstLook.Page.JQueryUpdatePanelBridge.get_instance = function() {
        return FirstLook.Page.JQueryUpdatePanelBridge.__instance || $create(FirstLook.Page.JQueryUpdatePanelBridge, null, null, null, null);
    };
    FirstLook.Page.JQueryUpdatePanelBridge.prototype = {
        initialize: function() {
            if (this.hasJQuery()) {
                this.cleanJQueryBindingsOnRequest();
            }
            FirstLook.Page.JQueryUpdatePanelBridge.callBaseMethod(this, 'initialize');
        },
        dispose: function() {
            if (this.hasJQuery()) {
                Sys.WebForms.PageRequestManager.getInstance().remove_beginRequest(this.getDelegate("cleanJQueryBindings"));
            };
            FirstLook.Page.JQueryUpdatePanelBridge.callBaseMethod(this, 'initialize');
        },
        cleanJQueryBindings: function(sender, args) {
            if (this.hasJQuery()) {
                var updatePanels = args.get_panelsUpdating(),
                removedPanels = args.get_panelsDeleting();
                for (var i = updatePanels.length - 1; i >= 0; i--) {
                    jQuery('*', updatePanels[i]);
                };
                for (var i = removedPanels.length - 1; i >= 0; i--) {
                    jQuery('*', removedPanels[i]);
                };
            };
        },
        hasJQuery: function() {
            return !! jQuery;
        },
        hasPartialPageRendering: function() {
            return !! Sys && !!Sys.WebForms;
        },
        cleanJQueryBindingsOnRequest: function() {
            if (this.hasPartialPageRendering()) {
                Sys.WebForms.PageRequestManager.getInstance().add_pageLoading(this.getDelegate("cleanJQueryBindings"));
            }
        }
    };
    FirstLook.Page.JQueryUpdatePanelBridge.registerClass("FirstLook.Page.JQueryUpdatePanelBridge", Sys.Component);


    // ===================
    // = ProgressManager =
    // ===================
    FirstLook.Page.ProgressManager = function() {
        if ( !! FirstLook.Page.ProgressManager.__instance) {
            FirstLook.Page.ProgressManager.__instance = this;
            FirstLook.Page.ProgressManager.initializeBase(this);
        };
        return FirstLook.Page.ProgressManager.__instance;
    };
    FirstLook.Page.ProgressManager.get_instance = function() {
        return FirstLook.Page.ProgressManager.__instance || $create(FirstLook.Page.ProgressManager, null, null, null, null);
    };
    FirstLook.Page.ProgressManager.prototype = {
        initialize: function() {
            this.hideAjaxProgress();
            this.showProgressOnRequest();
            this.hideProgressOnRequest();
            FirstLook.Page.ProgressManager.callBaseMethod(this, 'initialize');
        },
        updated: function() {
            this.hideAjaxProgress();
            FirstLook.Page.ProgressManager.callBaseMethod(this, 'updated');
        },
        dispose: function() {
            this.hideProgressOnRequest();
            this.clearDelegates();
            FirstLook.Page.ProgressManager.callBaseMethod(this, 'dispose');
        },
        has_progressEleemnt: function() {
            return !! this.get_progressElement();
        },
        get_progressElement: function() {
            if ( !! !this._progressElement) {
                this._progressElement = document.getElementById('progress');
            };
            return this._progressElement;
        },
        showAjaxProgress: function() {
            if (this.has_progressEleemnt()) {
                Sys.UI.DomElement.setStyle(this.get_progressElement(), {
                    display: "block"
                });
            }
            Sys.UI.DomElement.addCssClass(document.body, "loading");
        },
        showUpdatePanels: function(sender, args) {
            var updatePanels = args.get_panelsUpdating();
            for (var i = updatePanels.length - 1; i >= 0; i--) {
                Sys.UI.DomElement.addCssClass(updatePanels[i], "panel_loading");
            };
        },
        hideUpdatePanels: function(sender, args) {
            var updatePanels = args.get_panelsUpdated();
            for (var i = updatePanels.length - 1; i >= 0; i--) {
                Sys.UI.DomElement.removeCssClass(updatePanels[i], "panel_loading");
            };
        },
        hideAjaxProgress: function() {
            if (this.has_progressEleemnt()) {
                Sys.UI.DomElement.setStyle(this.get_progressElement(), {
                    display: "none"
                });
            }
            Sys.UI.DomElement.removeCssClass(document.body, "loading");
        },
        hasPartialPageRendering: function() {
            return !!Sys.WebForms;
        },
        showProgressOnRequest: function() {
            Sys.Net.WebRequestManager.add_invokingRequest(this.getDelegate("showAjaxProgress"));
            Sys.Net.WebRequestManager.add_completedRequest(this.getDelegate("hideAjaxProgress"));
            if (this.hasPartialPageRendering()) {
                Sys.WebForms.PageRequestManager.getInstance().add_pageLoading(this.getDelegate("showUpdatePanels"));
                Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(this.getDelegate("hideUpdatePanels"));
            };
        },
        hideProgressOnRequest: function() {
            Sys.Net.WebRequestManager.remove_invokingRequest(this.getDelegate("showAjaxProgress"));
            Sys.Net.WebRequestManager.remove_completedRequest(this.getDelegate("hideAjaxProgress"));
            if (this.hasPartialPageRendering()) {
            	Sys.WebForms.PageRequestManager.getInstance().remove_pageLoading(this.getDelegate("showUpdatePanels"));
            	Sys.WebForms.PageRequestManager.getInstance().remove_pageLoaded(this.getDelegate("hideUpdatePanels"));
            };
        }
    };
    FirstLook.Utility.createSetter(FirstLook.Page.ProgressManager, "progressElement");
    FirstLook.Page.ProgressManager.registerClass("FirstLook.Page.ProgressManager", Sys.Component);

    // =================
    // = PopUp Manager =
    // =================
    FirstLook.Page.PopupManager = function() {
        if ( !! FirstLook.Page.PopupManager.__instance) {
            FirstLook.Page.PopupManager.initializeBase(this);
            FirstLook.Page.PopupManager.__instance = this;
        };

        return FirstLook.Page.PopupManager.__instance;
    };
    FirstLook.Page.PopupManager.get_instance = function() {
        return FirstLook.Page.PopupManager.__instance || $create(FirstLook.Page.PopupManager, null, null, null, null);
    };
    FirstLook.Page.PopupManager.prototype = {
        initialize: function() {
            FirstLook.Page.PopupManager.callBaseMethod(this, 'initialize');
            this._windowSize = {};
            this._options = {};
            this.registerWindowSize("default", 800, 600);
            this.registerWindowOptions("default", {
                status: 1,
                scrollbars: 1,
                resizable: 1
            });
            FirstLook.Page.PopupManager.open = this.getDelegate("instanceOpen");
        },
        dispose: function() {
            FirstLook.Page.PopupManager.callBaseMethod(this, 'dispose');
        },
        updated: function() {
            FirstLook.Page.PopupManager.callBaseMethod(this, 'updated');
        },
        instanceOpen: function(url, target, optionString, addToHistory, windowCloseCallBack) {
            var openedWindow,
            size = this.get_windowSizeFor(target),
            options = this.get_windowOptionsFor(target),
            optionString = "";

            for (option in options) {
                optionString += option + "=" + options[option] + ",";
            }
            optionString += "width=" + size[0];
            optionString += ",height=" + size[1];

            openedWindow = window.open(url, target, optionString, !!addToHistory);
            if ( !! windowCloseCallBack) {
                this.bindWindowCloseCallBack(w, windowCloseCallBack);
            };
        },
        bindWindowCloseCallBack: function(w, windowCloseCallBack) {
            var parentWindow = window,
            addEvent,
            removeEvent,
            onChildWindowload;
            // =========================================================================
            // = These (Non-Library) Event Handlers are needed for cross window events =
            // =========================================================================
            addEvent = (function() {
                if (window.attachEvent) {
                    return function(el, type, action) {
                        return el.attachEvent("on" + type, action);
                    };
                }
                else {
                    return function(el, type, action) {
                        return el.addEventListener(type, action, false);
                    };
                }
            })();
            removeEvent = (function() {
                if (window.detachEvent) {
                    return function(el, type, action) {
                        return el.detachEvent("on" + type, action);
                    };
                }
                else {
                    return function(el, type, action) {
                        return el.removeEventListener(type, action, false);
                    };
                }
            })();
            onChildWindowLoad = function() {
                var childForms,
                onChildWindowUnloadCalback,
                onChildWindowUnload,
                onChildFormSubmit;

                childForms = w.document.getElementsByTagName("form");

                onChildWindowUnloadCalback = function() {
                    removeEvent(w, "unload", onChildWindowUnloadCalback);
                    parentWindow.setTimeout(windowCloseCallBack, 0);
                };
                onChildWindowUnload = function() {
                    removeEvent(w, "unload", onChildWindowUnload);
                    removeEvent(w, "load", onChildWindowLoad);
                    for (var i = 0, l = childForms.length; i < l; i++) {
                        removeEvent(childForms[i], "submit", onChildFormSubmit);
                    };
                    parentWindow.setTimeout(function() {
                        try {
                            if ( !! w)
                            addEvent(w, "load", onChildWindowLoad);
                        } catch(err) {
                            // ====================================================================
                            // = Do nothing... with the timeout you can get permission errors
                            // = as the window might be in a closing state, PM
                            // ====================================================================
                            }
                    },
                    0);
                };
                onChildFormSubmit = function() {
                    addEvent(w, "unload", onChildWindowUnloadCalback);
                };

                for (var i = 0, l = childForms.length; i < l; i++) {
                    addEvent(childForms[i], "submit", onChildFormSubmit);
                };

                addEvent(w, "unload", onChildWindowUnload);
            };

            addEvent(w, "load", onChildWindowLoad);
        },
        get_windowSizeFor: function(target) {
            var windowSize = this.get_windowSize();
            return windowSize[target] || windowSize["default"];
        },
        get_windowOptionsFor: function(target) {
            var options = this.get_options();
            return options[target] || options["default"];
        },
        registerWindowSize: function(target, width, height) {
            var windowSize = this.get_windowSize();
            windowSize[target] = [parseInt(width, 10), parseInt(height, 10)];
        },
        registerWindowOptions: function(target, options) {
            var existing_options = this.get_options();
            existing_options[target] = options;
        }
    };
    FirstLook.Utility.createGetters(FirstLook.Page.PopupManager, ["instance", "options", "windowSize", "createGlobal"]);
    FirstLook.Page.PopupManager.registerClass("FirstLook.Page.PopupManager", Sys.Component);

    Sys.Application.notifyScriptLoaded();
}
