using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("FirstLook.Common.WebControls.Adapters.MenuAdapter.debug.js", "text/javascript")]
[assembly: WebResource("FirstLook.Common.WebControls.Adapters.MenuAdapter.js", "text/javascript")]

namespace FirstLook.Common.WebControls.Adapters
{
	public class MenuAdapter : System.Web.UI.WebControls.Adapters.MenuAdapter
	{
		private const string MenuRootCssClass = "ui-menu";
		private const string MenuItemCssClass = "ui-menu-parent";
		private const string MenuItemLeafCssClass = "ui-menu-leaf";
		private const string MenuItemTemplateCssClass = "ui-menu-template";
		private const string MenuItemLinkCssClass = "ui-menu-link";
		private const string MenuItemTextCssClass = "ui-menu-text";
		private const string MenuItemSelectedCssClass = "ui-menu-selected";
		private const string MenuItemChildSelectedCssClass = "ui-menu-child-selected";
		private const string MenuItemParentSelectedCssClass = "ui-menu-parent-selected";

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			Control.MenuItemDataBound += MenuItemDataBound;

			HttpBrowserCapabilities browser = Page.Request.Browser;

			if (string.Compare(browser.Browser, "IE") == 0)
			{
				ScriptManager scriptManager = ScriptManager.GetCurrent(Page);
				if (scriptManager != null)
				{
					scriptManager.Scripts.Add(new ScriptReference("FirstLook.Common.WebControls.Adapters.MenuAdapter.js", "FirstLook.Common.WebControls"));
					ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), GetType() + "_" + Control.ClientID,
															"window.attachEvent('onload',function() { MenuManager.addMenu('" + Control.ClientID + "'); });\n", true);
				}
				else
				{
					Page.ClientScript.RegisterClientScriptResource(typeof(MenuAdapter),
																   "FirstLook.Common.WebControls.Adapters.MenuAdapter.js");
					Page.ClientScript.RegisterClientScriptBlock(GetType(), GetType() + "_" + Control.ClientID,
																"MenuManager.addMenu('" + Control.ClientID + "');\n", true);
				}
			}
		}

		private static void MenuItemDataBound(object sender, MenuEventArgs e)
		{
			SiteMapNode o = e.Item.DataItem as SiteMapNode;

			if (o != null)
			{
				string target = o["target"];

				if (!string.IsNullOrEmpty(target))
				{
					e.Item.Target = target;
				}
			}
		}

		protected override void RenderBeginTag(HtmlTextWriter writer)
		{
			string cssClass = MenuRootCssClass + "-" + Control.Orientation.ToString().ToLower();
			string id = Control.ClientID;

			if (!string.IsNullOrEmpty(Control.CssClass))
			{
				WriteBeginDiv(writer, Control.CssClass, id);
				WriteBeginDiv(writer, cssClass, string.Empty);
			}
			else
			{
				WriteBeginDiv(writer, cssClass, id);
			}
		}

		static public void WriteBeginDiv(HtmlTextWriter writer, string className, string id)
		{
			writer.WriteLine();
			if (!String.IsNullOrEmpty(className))
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Class, className);
			}
			if (!String.IsNullOrEmpty(id))
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Id, id);
			}
			writer.RenderBeginTag(HtmlTextWriterTag.Div);
			writer.Indent++;
		}

		protected override void RenderEndTag(HtmlTextWriter writer)
		{
			WriteEndDiv(writer);

			if (!string.IsNullOrEmpty(Control.CssClass))
			{
				WriteEndDiv(writer);
			}
		}

		static public void WriteEndDiv(HtmlTextWriter writer)
		{
			writer.Indent--;
			writer.WriteLine();
			writer.RenderEndTag(); // div
		}

		protected override void RenderContents(HtmlTextWriter writer)
		{
			writer.Indent++;
			BuildItems(Control.Items, true, writer);
			writer.Indent--;
			writer.WriteLine();
		}

		private void BuildItems(MenuItemCollection items, bool isRoot, HtmlTextWriter writer)
		{
			if (items.Count > 0)
			{
				writer.WriteLine();
				if (isRoot)
				{
					writer.AddAttribute(HtmlTextWriterAttribute.Class, MenuRootCssClass);
				}
				writer.RenderBeginTag(HtmlTextWriterTag.Ul);
				writer.Indent++;

				foreach (MenuItem item in items)
				{
					BuildItem(item, writer, items[items.Count - 1].Equals(item));
				}

				writer.Indent--;
				writer.WriteLine();
				writer.RenderEndTag(); // ul
			}
		}

		private void BuildItem(MenuItem item, HtmlTextWriter writer, bool isLast)
		{
			Menu menu = Control;

			if ((menu != null) && (item != null) && (writer != null))
			{
				writer.WriteLine();

				string theClass = HasChildItems(item) ? MenuItemCssClass : MenuItemLeafCssClass;
				string selectedStatusClass = GetSelectStatusClass(item);

				if (!string.IsNullOrEmpty(selectedStatusClass))
				{
					theClass += " " + selectedStatusClass;
				}

				if (isLast)
				{
					theClass += " last";
				}
				writer.AddAttribute(HtmlTextWriterAttribute.Class, theClass);
				writer.RenderBeginTag(HtmlTextWriterTag.Li);
				writer.Indent++;
				writer.WriteLine();

				if (((item.Depth < menu.StaticDisplayLevels) && (menu.StaticItemTemplate != null)) ||
					((item.Depth >= menu.StaticDisplayLevels) && (menu.DynamicItemTemplate != null)))
				{
					writer.AddAttribute(HtmlTextWriterAttribute.Class, GetItemClass(menu, item));
					writer.RenderBeginTag(HtmlTextWriterTag.Div);
					writer.Indent++;
					writer.WriteLine();

					MenuItemTemplateContainer container = new MenuItemTemplateContainer(menu.Items.IndexOf(item), item);
					if ((item.Depth < menu.StaticDisplayLevels) && (menu.StaticItemTemplate != null))
					{
						menu.StaticItemTemplate.InstantiateIn(container);
					}
					else
					{
						menu.DynamicItemTemplate.InstantiateIn(container);
					}
					container.DataBind();
					container.RenderControl(writer);

					writer.Indent--;
					writer.WriteLine();
					writer.RenderEndTag(); // div
				}
				else
				{
					if (IsLink(item))
					{
						if (!string.IsNullOrEmpty(item.NavigateUrl))
						{
						    string NavigationUrl = Page.Server.HtmlEncode(menu.ResolveClientUrl(item.NavigateUrl));

                            NavigationUrl = NavigationUrl.Replace("&amp;", "&");

							writer.AddAttribute(HtmlTextWriterAttribute.Href, NavigationUrl);
						}
						else
						{
							writer.AddAttribute(HtmlTextWriterAttribute.Href, Page.ClientScript.GetPostBackClientHyperlink(menu, "b" + item.ValuePath.Replace(menu.PathSeparator.ToString(), "\\"), true));
						}

						writer.AddAttribute(HtmlTextWriterAttribute.Class, GetItemClass(menu, item));


						if (!string.IsNullOrEmpty(item.Target))
						{
							writer.AddAttribute(HtmlTextWriterAttribute.Target, item.Target);
						}

						if (!string.IsNullOrEmpty(item.ToolTip))
						{
							writer.AddAttribute(HtmlTextWriterAttribute.Title, item.ToolTip);
						}
						else if (!string.IsNullOrEmpty(menu.ToolTip))
						{
							writer.AddAttribute(HtmlTextWriterAttribute.Title, menu.ToolTip);
						}
						writer.RenderBeginTag(HtmlTextWriterTag.A);
						writer.Indent++;
						writer.WriteLine();
					}
					else
					{
						writer.AddAttribute(HtmlTextWriterAttribute.Class, GetItemClass(menu, item));
						writer.RenderBeginTag(HtmlTextWriterTag.Span);
						writer.Indent++;
						writer.WriteLine();
					}

					if (!string.IsNullOrEmpty(item.ImageUrl))
					{
						writer.AddAttribute(HtmlTextWriterAttribute.Src, menu.ResolveClientUrl(item.ImageUrl));
						writer.AddAttribute(HtmlTextWriterAttribute.Alt, !string.IsNullOrEmpty(item.ToolTip) ? item.ToolTip : (!string.IsNullOrEmpty(menu.ToolTip) ? menu.ToolTip : item.Text));
						writer.Write(HtmlTextWriter.SelfClosingTagEnd);
						writer.RenderBeginTag(HtmlTextWriterTag.Img);
						writer.RenderEndTag();
					}

					writer.Write(item.Text);

					writer.Indent--;
					writer.RenderEndTag(); // either a or span
				}

				if (HasChildItems(item))
				{
					BuildItems(item.ChildItems, false, writer);
				}

				writer.Indent--;
				writer.WriteLine();
				writer.RenderEndTag(); // li
			}
		}

        private bool HasChildItems(MenuItem item)
        {
            Menu menu = Control;

            return
                (item.ChildItems != null) && (item.ChildItems.Count > 0) && (item.Depth + 1 < menu.MaximumDynamicDisplayLevels);
        }

		private static bool IsLink(MenuItem item)
		{
			return (item != null) && item.Enabled && ((!string.IsNullOrEmpty(item.NavigateUrl)) || item.Selectable);
		}

		private static string GetItemClass(Menu menu, MenuItem item)
		{
			string value = MenuItemTextCssClass;
			if (item != null)
			{
				if (((item.Depth < menu.StaticDisplayLevels) && (menu.StaticItemTemplate != null)) ||
					((item.Depth >= menu.StaticDisplayLevels) && (menu.DynamicItemTemplate != null)))
				{
					value = MenuItemTemplateCssClass;
				}
				else if (IsLink(item))
				{
					value = MenuItemLinkCssClass;
				}
				string selectedStatusClass = GetSelectStatusClass(item);
				if (!String.IsNullOrEmpty(selectedStatusClass))
				{
					value += " " + selectedStatusClass;
				}
			}

			return value;
		}

		private static string GetSelectStatusClass(MenuItem item)
		{
			StringBuilder sb = new StringBuilder();
			if (item.Selected)
			{
				sb.Append(" ").Append(MenuItemSelectedCssClass);
			}
			else if (IsChildItemSelected(item))
			{
				sb.Append(" ").Append(MenuItemChildSelectedCssClass);
			}
			else if (IsParentItemSelected(item))
			{
				sb.Append(" ").Append(MenuItemParentSelectedCssClass);
			}
			return sb.ToString();
		}

		private static bool IsChildItemSelected(MenuItem item)
		{
			bool bRet = false;

			if ((item != null) && (item.ChildItems != null))
			{
				bRet = IsChildItemSelected(item.ChildItems);
			}

			return bRet;
		}

		private static bool IsChildItemSelected(MenuItemCollection items)
		{
			bool bRet = false;

			if (items != null)
			{
				foreach (MenuItem item in items)
				{
					if (item.Selected || IsChildItemSelected(item.ChildItems))
					{
						bRet = true;
						break;
					}
				}
			}

			return bRet;
		}

		private static bool IsParentItemSelected(MenuItem item)
		{
			bool bRet = false;

			if ((item != null) && (item.Parent != null))
			{
				if (item.Parent.Selected)
				{
					bRet = true;
				}
				else
				{
					bRet = IsParentItemSelected(item.Parent);
				}
			}

			return bRet;
		}
	}
}