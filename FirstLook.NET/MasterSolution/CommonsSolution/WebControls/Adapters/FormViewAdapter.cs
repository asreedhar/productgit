using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.Adapters;

namespace FirstLook.Common.WebControls.Adapters
{
    public class FormViewAdapter : WebControlAdapter
    {

        protected override void RenderBeginTag(HtmlTextWriter writer)
        {
            if (!string.IsNullOrEmpty(this.Control.ID))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Id, this.Control.ClientID);
            }

            if (!string.IsNullOrEmpty(this.Control.CssClass))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, this.Control.CssClass);
            }

            writer.RenderBeginTag(HtmlTextWriterTag.Div);
        }

        protected override void RenderEndTag(HtmlTextWriter writer)
        {
            writer.RenderEndTag();
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            FormView formView = this.Control as FormView;
            if (formView != null)
            {
                writer.Indent++;

                WritePagerSection(writer, PagerPosition.Top);

                if (formView.HeaderRow.Cells.Count > 0)
                {
                    foreach (TableCell cell in formView.HeaderRow.Cells)
                    {
                        foreach (Control control in cell.Controls)
                        {
                            control.RenderControl(writer);
                        }
                    }
                }

                if (formView.Row.Cells.Count > 0)
                {
                    foreach (TableCell cell in formView.Row.Cells)
                    {
                        foreach (Control control in cell.Controls)
                        {
                            control.RenderControl(writer);
                        }
                    }
                }

                if (formView.FooterRow.Cells.Count > 0)
                {
                    foreach (TableCell cell in formView.FooterRow.Cells)
                    {
                        foreach (Control control in cell.Controls)
                        {
                            control.RenderControl(writer);
                        }
                    }
                }

                WritePagerSection(writer, PagerPosition.Bottom);

                writer.Indent--;
            } 
            else
            {
                throw new InvalidCastException("FormViewAdapter must adapt a Form View!");
            }
        }

        private void WritePagerSection(HtmlTextWriter writer, PagerPosition pos)
        {
            FormView formView = Control as FormView;
            if ((formView != null) &&
                formView.AllowPaging &&
                ((formView.PagerSettings.Position == pos) || (formView.PagerSettings.Position == PagerPosition.TopAndBottom)))
            {
                Table innerTable = null;
                if ((pos == PagerPosition.Top) &&
                    (formView.TopPagerRow != null) &&
                    (formView.TopPagerRow.Cells.Count == 1) &&
                    (formView.TopPagerRow.Cells[0].Controls.Count == 1) &&
                    typeof(Table).IsAssignableFrom(formView.TopPagerRow.Cells[0].Controls[0].GetType()))
                {
                    innerTable = formView.TopPagerRow.Cells[0].Controls[0] as Table;
                }
                else if ((pos == PagerPosition.Bottom) &&
                         (formView.BottomPagerRow != null) &&
                         (formView.BottomPagerRow.Cells.Count == 1) &&
                         (formView.BottomPagerRow.Cells[0].Controls.Count == 1) &&
                         typeof(Table).IsAssignableFrom(formView.BottomPagerRow.Cells[0].Controls[0].GetType()))
                {
                    innerTable = formView.BottomPagerRow.Cells[0].Controls[0] as Table;
                }

                if ((innerTable != null) && (innerTable.Rows.Count == 1) && (innerTable.Rows[0].Cells.Count > 1))
                {
                    string className = "table-row-pagination table-row-";
                    className += (pos == PagerPosition.Top) ? "top " : "bottom ";
                    if (formView.PagerStyle != null)
                    {
                        className += formView.PagerStyle.CssClass;
                    }
                    className = className.Trim();

                    writer.WriteLine();
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, className);
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    writer.Indent++;

                    TableRow row = innerTable.Rows[0];
                    foreach (TableCell cell in row.Cells)
                    {
                        foreach (Control ctrl in cell.Controls)
                        {
                            writer.WriteLine();
                            ctrl.RenderControl(writer);
                        }
                    }

                    writer.Indent--;
                    writer.WriteLine();
                    writer.RenderEndTag();
                }
            }
        }
    }
}
