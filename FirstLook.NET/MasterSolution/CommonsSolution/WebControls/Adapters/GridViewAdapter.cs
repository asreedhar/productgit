using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.Adapters;

namespace FirstLook.Common.WebControls.Adapters
{
    public class GridViewAdapter : WebControlAdapter
    {
        protected override void RenderBeginTag(HtmlTextWriter writer)
        {
            // ignore: RenderContents performs this task
        }

        protected override void RenderEndTag(HtmlTextWriter writer)
        {
            // ignore: RenderContents performs this task
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            GridView gridView = Control as GridView;
            if (gridView != null)
            {
                writer.Indent++;
                WritePagerSection(writer, PagerPosition.Top);

                writer.WriteLine();

                writer.WriteBeginTag("table");
                writer.WriteAttribute("id", gridView.ClientID);

                if (!string.IsNullOrEmpty(Control.ToolTip))
                {
                    writer.WriteAttribute("summary", Control.ToolTip);
                }

                if (!String.IsNullOrEmpty(gridView.CssClass))
                {
                    writer.WriteAttribute("class", gridView.CssClass);
                }

                writer.Write(HtmlTextWriter.TagRightChar);
                writer.Indent++;

                if (!string.IsNullOrEmpty(gridView.Caption))
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Caption);
                    writer.Write(gridView.Caption);
                    writer.RenderEndTag();
                }

                ArrayList rows = new ArrayList();
                GridViewRowCollection gvrc = null;

                ///////////////////// HEAD /////////////////////////////

                rows.Clear();
                if (gridView.ShowHeader && (gridView.HeaderRow != null))
                {
                    foreach (Control c in gridView.HeaderRow.Parent.Controls)
                    {
                        GridViewRow r = c as GridViewRow;
                        if (r != null && r.Visible && r.RowType.Equals(DataControlRowType.Header))
                        {
                            rows.Add(r);
                        }
                    }
                }
                gvrc = new GridViewRowCollection(rows);
                WriteRows(writer, gridView, gvrc, "thead");

                ///////////////////// FOOT /////////////////////////////

                rows.Clear();
                if (gridView.ShowFooter && (gridView.FooterRow != null))
                {
                    foreach (Control c in gridView.FooterRow.Parent.Controls)
                    {
                        GridViewRow r = c as GridViewRow;
                        if (r != null && r.Visible && r.RowType.Equals(DataControlRowType.Footer))
                            rows.Add(r);
                    }
                }
                gvrc = new GridViewRowCollection(rows);
                WriteRows(writer, gridView, gvrc, "tfoot");

                ///////////////////// BODY /////////////////////////////

                WriteRows(writer, gridView, gridView.Rows, "tbody");

                ////////////////////////////////////////////////////////

                writer.Indent--;
                writer.WriteLine();
                writer.WriteEndTag("table");

                WritePagerSection(writer, PagerPosition.Bottom);

                writer.Indent--;
                writer.WriteLine();
            }
        }

        private void WriteRows(HtmlTextWriter writer, GridView gridView, GridViewRowCollection rows, string tableSection)
        {
            if (rows.Count > 0)
            {
                writer.WriteLine();
                writer.WriteBeginTag(tableSection);
                writer.Write(HtmlTextWriter.TagRightChar);
                writer.Indent++;

                foreach (GridViewRow row in rows)
                {
                    if (!row.Visible)
                    {
                        continue;
                    }

                    writer.WriteLine();
                    writer.WriteBeginTag("tr");

                    string className = GetRowClass(gridView, row);
                    if (!String.IsNullOrEmpty(className))
                    {
                        writer.WriteAttribute("class", className);
                    }

                    writer.Write(HtmlTextWriter.TagRightChar);
                    writer.Indent++;

                    foreach (TableCell cell in row.Cells)
                    {
                        DataControlFieldCell fieldCell = cell as DataControlFieldCell;
                        if ((fieldCell != null) && (fieldCell.ContainingField != null))
                        {
                            // this logic is only for header rows
                            DataControlField field = fieldCell.ContainingField;
                            if (!field.Visible)
                            {
                                cell.Visible = false;
                            }

                            if ((field.ItemStyle != null) && (!String.IsNullOrEmpty(field.ItemStyle.CssClass)))
                            {
                                if (!String.IsNullOrEmpty(cell.CssClass))
                                {
                                    cell.CssClass += " ";
                                }
                                cell.CssClass += field.ItemStyle.CssClass;
                            }
                        }

                        writer.WriteLine();
                        cell.RenderControl(writer);
                    }

                    writer.Indent--;
                    writer.WriteLine();
                    writer.WriteEndTag("tr");
                }

                writer.Indent--;
                writer.WriteLine();
                writer.WriteEndTag(tableSection);
            }
        }

        private string GetRowClass(GridView gridView, GridViewRow row)
        {
            string className = "";

            if (row.RowType.Equals(DataControlRowType.Header))
            {
                if ((gridView.HeaderStyle != null) && (!String.IsNullOrEmpty(gridView.HeaderStyle.CssClass)))
                    className += " " + gridView.HeaderStyle.CssClass;
            }
            else if (row.RowType.Equals(DataControlRowType.Footer))
            {
                if ((gridView.FooterStyle != null) && (!String.IsNullOrEmpty(gridView.FooterStyle.CssClass)))
                    className += " " + gridView.FooterStyle.CssClass;
            }
            else if ((row.RowState & DataControlRowState.Alternate) == DataControlRowState.Alternate)
            {
                if (gridView.AlternatingRowStyle != null)
                    if (!string.IsNullOrEmpty(gridView.AlternatingRowStyle.CssClass))
                        className += " " + gridView.AlternatingRowStyle.CssClass;
            }
            else // normal row
            {
                if (gridView.RowStyle != null)
                    if (!string.IsNullOrEmpty(gridView.RowStyle.CssClass))
                        className += " " + gridView.RowStyle.CssClass;
            }

            if (!string.IsNullOrEmpty(row.CssClass))
            {
                className += " " + row.CssClass;
            }

            if ((row.RowState & DataControlRowState.Edit) == DataControlRowState.Edit)
            {
                className += " table-row-edit ";
                if (gridView.EditRowStyle != null)
                {
                    className += gridView.EditRowStyle.CssClass;
                }
            }

            if ((row.RowState & DataControlRowState.Insert) == DataControlRowState.Insert)
            {
                className += " table-row-insert ";
            }

            if ((row.RowState & DataControlRowState.Selected) == DataControlRowState.Selected)
            {
                className += " table-row-selected ";
                if (gridView.SelectedRowStyle != null)
                {
                    className += gridView.SelectedRowStyle.CssClass;
                }
            }

            return className.Trim();
        }

        private void WritePagerSection(HtmlTextWriter writer, PagerPosition pos)
        {
            GridView gridView = Control as GridView;
            if ((gridView != null) &&
                gridView.AllowPaging &&
                gridView.Rows.Count > 0 &&
                ((gridView.PagerSettings.Position == pos) || (gridView.PagerSettings.Position == PagerPosition.TopAndBottom)))
            {
                Table innerTable = null;
                if ((pos == PagerPosition.Top) &&
                    (gridView.TopPagerRow != null) &&
                    (gridView.TopPagerRow.Cells.Count == 1) &&
                    (gridView.TopPagerRow.Cells[0].Controls.Count == 1) &&
                    typeof(Table).IsAssignableFrom(gridView.TopPagerRow.Cells[0].Controls[0].GetType()))
                {
                    innerTable = gridView.TopPagerRow.Cells[0].Controls[0] as Table;
                }
                else if ((pos == PagerPosition.Bottom) &&
                         (gridView.BottomPagerRow != null) &&
                         (gridView.BottomPagerRow.Cells.Count == 1) &&
                         (gridView.BottomPagerRow.Cells[0].Controls.Count == 1) &&
                         typeof(Table).IsAssignableFrom(gridView.BottomPagerRow.Cells[0].Controls[0].GetType()))
                {
                    innerTable = gridView.BottomPagerRow.Cells[0].Controls[0] as Table;
                }

                if ((innerTable != null) && (innerTable.Rows.Count == 1) && (innerTable.Rows[0].Cells.Count > 1))
                {
                    string className = "table-row-pagination table-row-";
                    className += (pos == PagerPosition.Top) ? "top " : "bottom ";
                    if (gridView.PagerStyle != null)
                    {
                        className += gridView.PagerStyle.CssClass;
                    }
                    className = className.Trim();

                    writer.WriteLine();
                    writer.WriteBeginTag("div");
                    writer.WriteAttribute("class", className);
                    writer.Write(HtmlTextWriter.TagRightChar);
                    writer.Indent++;

                    TableRow row = innerTable.Rows[0];
                    foreach (TableCell cell in row.Cells)
                    {
                        foreach (Control ctrl in cell.Controls)
                        {
                            writer.WriteLine();
                            ctrl.RenderControl(writer);
                        }
                    }

                    writer.Indent--;
                    writer.WriteLine();
                    writer.WriteEndTag("div");
                }
            }
        }
    }
}