using System;
using System.Collections;
using System.Web.UI;

namespace FirstLook.Common.WebControls
{
    /// <summary>
    /// Abstract base class for an asynchronous <code>DataSourceView</code> by Nikhil Kothari.
    /// </summary>
    /// <see cref="http://msdn2.microsoft.com/en-us/library/ms364049(VS.80).aspx" />
    /// <see cref="http://msdn2.microsoft.com/en-us/library/ms364050(VS.80).aspx" />
    /// <see cref="http://msdn2.microsoft.com/en-us/library/ms364051(VS.80).aspx" />
    /// <see cref="http://msdn2.microsoft.com/en-us/library/ms364052(VS.80).aspx" />
    /// <see cref="http://msdn2.microsoft.com/en-us/library/ms364053(VS.80).aspx" />
    public abstract class AsyncDataSourceView : DataSourceView
    {
        private readonly IAsyncDataSource owner;

        protected AsyncDataSourceView(IAsyncDataSource owner, string viewName)
            : base(owner, viewName)
        {
            this.owner = owner;
        }

        protected abstract IAsyncResult BeginExecuteSelect(DataSourceSelectArguments arguments, AsyncCallback asyncCallback, object asyncState);

        protected abstract IEnumerable EndExecuteSelect(IAsyncResult asyncResult);

        protected override IEnumerable ExecuteSelect(DataSourceSelectArguments arguments)
        {
            return EndExecuteSelect(BeginExecuteSelect(arguments, null, null));
        }

        private IAsyncResult OnBeginSelect(object sender, EventArgs e, AsyncCallback asyncCallback, object extraData)
        {
            object[] data = (object[]) extraData;
            DataSourceSelectArguments arguments = (DataSourceSelectArguments) data[0];
            DataSourceViewSelectCallback callback = (DataSourceViewSelectCallback) data[1];

            return BeginExecuteSelect(arguments, asyncCallback, callback);
        }

        private void OnEndSelect(IAsyncResult asyncResult)
        {
            IEnumerable data = EndExecuteSelect(asyncResult);
            DataSourceViewSelectCallback callback = (DataSourceViewSelectCallback) asyncResult.AsyncState;

            callback(data);
        }

        public override void Select(DataSourceSelectArguments arguments, DataSourceViewSelectCallback callback)
        {
            if (owner.IsAsync)
            {
                Page page = ((Control)owner).Page;
                PageAsyncTask task = new PageAsyncTask(
                    OnBeginSelect,
                    OnEndSelect,
                    null,
                    new object[] {arguments, callback},
                    true);
                page.RegisterAsyncTask(task);
            }
            else
            {
                base.Select(arguments, callback);
            }
        }
    }
}