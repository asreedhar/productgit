using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;

namespace FirstLook.Common.WebControls.Helpers
{
    public static class CssClassHelper
    {
        static public void Show(WebControl control)
        {
            control.Style["display"] = "block";
        }

        static public void Hide(WebControl control)
        {
            control.Style["display"] = "none";
        }

        static public void AddClass(WebControl control, string name)
        {
            if (control == null) {
                throw new ArgumentNullException("control");
            }
            control.CssClass = AddClass(control.CssClass, name);
        }

        static public void RemoveClass(WebControl control, string name)
        {
            if (control == null)
            {
                throw new ArgumentNullException("control");
            }
            control.CssClass = RemoveClass(control.CssClass, name);
        }

        static public bool HasClass(WebControl control, string name)
        {
            if (control == null)
            {
                throw new ArgumentNullException("control");
            }
            return HasClass(control.CssClass, name);
        }

        static public string AddClass(string text, string name)
        {
            if (string.IsNullOrEmpty(name))
                return text ?? string.Empty;

            if (string.IsNullOrEmpty(text))
                return name;

            if (HasClass(text, name))
                return text;

            return text + " " + name;
        }

        static public string RemoveClass(string text, string name)
        {
            if (string.IsNullOrEmpty(name))
                return text ?? string.Empty;

            if (string.IsNullOrEmpty(text))
                return string.Empty;

            StringBuilder sb = new StringBuilder();

            string[] values = new Regex("\\s+").Split(text);

            foreach (string value in values)
            {
                if (!name.Equals(value))
                {
                    if (sb.Length > 0)
                        sb.Append(' ');
                    sb.Append(value);
                }
            }

            return sb.ToString();
        }

        static public bool HasClass(string text, string name)
        {
            if (string.IsNullOrEmpty(text))
                throw new ArgumentNullException("text");

            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException("name");

            string[] values = new Regex("\\s+").Split(text);

            foreach (string value in values)
            {
                if (name.Equals(value))
                {
                    return true;
                }
            }

            return false;
        }
    }
}