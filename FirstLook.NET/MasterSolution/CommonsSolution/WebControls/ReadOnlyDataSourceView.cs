using System.Collections;
using System.Web.UI;

namespace FirstLook.Common.WebControls
{
    internal sealed class ReadOnlyDataSourceView : DataSourceView
    {
        private readonly IEnumerable _dataSource;

        public ReadOnlyDataSourceView(ReadOnlyDataSource owner, string name, IEnumerable dataSource)
            : base(owner, name)
        {
            _dataSource = dataSource;
        }

        protected override IEnumerable ExecuteSelect(DataSourceSelectArguments arguments)
        {
            arguments.RaiseUnsupportedCapabilitiesError(this);
            return _dataSource;
        }
    }
}
