﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace PowershellUsingDotnet4
{
    class Program
    {
        static  int Main()
        {
            SetupDotnetRuntimeConfig();

            var startInfo = new ProcessStartInfo
                                {
                                    UseShellExecute = false,
                                    FileName = "Powershell.exe",
                                    Arguments = GetPassthroughArguments()
                                };

            var proc = Process.Start(startInfo);

            proc.WaitForExit();
            return proc.ExitCode;
        }

        private static void SetupDotnetRuntimeConfig()
        {
            var path = Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName;
            var dir = Path.GetDirectoryName(path);

            Environment.SetEnvironmentVariable("COMPLUS_ApplicationMigrationRuntimeActivationConfigPath", dir);
        }

        private static string GetPassthroughArguments()
        {
            var cmdline = Environment.CommandLine;
            var argStart = GetArgumentStart(cmdline);
            return cmdline.Substring(argStart);
        }

        private static int GetArgumentStart(string cmdline)
        {
            var state = 0;
            int pos;
            for (pos = 0; pos < cmdline.Length; ++pos)
            {
                var ch = cmdline[pos];
                switch (state)
                {
                    case 0:
                        if (Char.IsWhiteSpace(ch))
                            state = 1;
                        else if (ch == '\'')
                            state = 2;
                        else if (ch == '"')
                            state = 3;
                        else
                            state = 4;
                        break;

                    case 1:
                        if (ch == '\'')
                            state = 2;
                        else if (ch == '"')
                            state = 3;
                        else
                            state = 4;
                        break;

                    case 2:
                        if (ch == '\'')
                            return ++pos;
                        break;

                    case 3:
                        if (ch == '"')
                            return ++pos;
                        break;

                    case 4:
                        if (Char.IsWhiteSpace(ch))
                            return pos;
                        break;
                }
            }
            return pos;
        }
    }
}
