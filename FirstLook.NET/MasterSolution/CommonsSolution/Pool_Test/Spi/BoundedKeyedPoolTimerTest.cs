using System;
using NUnit.Framework;

namespace FirstLook.Common.Pool.Spi
{
    [TestFixture]
    public class BoundedKeyedPoolTimerTest : KeyedTimerTest
    {
        public override void SetUp(int min, int max, TimeSpan idlePoll, TimeSpan idleTime, int idleCount, TimeSpan idleCountTime)
        {
            IKeyedPooledItemFactory<TestItem,string> factory = new TestItemFactory(Clean_Counter);

            Bounds bounds = new Bounds(min, max);

            EvictionCriteria criteria = new EvictionCriteria(idlePoll, idleTime, idleCount, idleCountTime);

            pool = new CustomBoundedKeyedPool("CustomBoundedKeyedPool<TestItem,string>", factory, bounds, criteria);
        }

        public override int NumberIdleItems()
        {
            return ((CustomBoundedKeyedPool)pool).IdleCounter;
        }

        public override int NumberLeakItems()
        {
            return ((CustomBoundedKeyedPool)pool).LeakCounter;
        }

        class CustomBoundedKeyedPool : BoundedKeyedPool<TestItem,string>
        {
            private int leakCounter = 0;
            private int idleCounter = 0;

            public CustomBoundedKeyedPool(string instanceName, IKeyedPooledItemFactory<TestItem,string> factory, Bounds bounds,
                                     EvictionCriteria criteria)
                : base(instanceName, factory, bounds, criteria)
            {

            }

            public int LeakCounter
            {
                get { return leakCounter; }
            }

            public int IdleCounter
            {
                get { return idleCounter; }
            }

            protected override void HandleItemLeak(string key)
            {
                lock (this) { leakCounter += 1; }

                base.HandleItemLeak(key);
            }

            protected override void ShrinkPool(string key, TestItem item)
            {
                lock (this) { idleCounter += 1; }

                base.ShrinkPool(key, item);
            }
        }
    }
}
