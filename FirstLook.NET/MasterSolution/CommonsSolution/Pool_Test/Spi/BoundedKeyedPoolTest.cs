using System;
using NUnit.Framework;

namespace FirstLook.Common.Pool.Spi
{
    [TestFixture]
    public class BoundedKeyedPoolTest : KeyedPoolTest
    {
        private readonly TimeSpan oneMinute = new TimeSpan(TimeSpan.TicksPerMinute);

        private readonly TimeSpan never = new TimeSpan(0);

        public override void SetUp(int minCount, int maxCount)
        {
            IKeyedPooledItemFactory<TestItem,string> factory = new TestItemFactory();

            Bounds bounds = new Bounds(minCount, maxCount);

            EvictionCriteria criteria = new EvictionCriteria(oneMinute, oneMinute, 0, never);

            pool = new BoundedKeyedPool<TestItem,string>("BoundedKeyedPool<TestItem,string>", factory, bounds, criteria);
        }
    }
}
