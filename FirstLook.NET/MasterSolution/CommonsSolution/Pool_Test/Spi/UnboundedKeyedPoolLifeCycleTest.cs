using System;
using NUnit.Framework;

namespace FirstLook.Common.Pool.Spi
{
    [TestFixture]
    public class UnboundedKeyedPoolLifeCycleTest : KeyedPoolLifeCycleTest
    {
        private readonly TimeSpan oneMinute = new TimeSpan(TimeSpan.TicksPerMinute);

        private readonly TimeSpan never = new TimeSpan(0);

        [SetUp]
        public override void SetUp()
        {
            IKeyedPooledItemFactory<LifeCycleItem,string> factory = new LifeCyclePoolItemFactory();

            EvictionCriteria criteria = new EvictionCriteria(oneMinute, oneMinute, 0, never);

            pool = new UnboundedKeyedPool<LifeCycleItem, string>("TestUnboundedKeyedPool<LifeCycleItem, string>", factory, criteria);
        }
    }
}
