using System;
using NUnit.Framework;

namespace FirstLook.Common.Pool.Spi
{
    [TestFixture]
    public class UnboundedKeyedPoolTest : KeyedPoolTest
    {
        private readonly TimeSpan oneMinute = new TimeSpan(TimeSpan.TicksPerMinute);

        private readonly TimeSpan never = new TimeSpan(0);

        public override void SetUp(int minCount, int maxCount)
        {
            IKeyedPooledItemFactory<TestItem, string> factory = new TestItemFactory();

            EvictionCriteria criteria = new EvictionCriteria(oneMinute, oneMinute, 0, never);

            pool = new UnboundedKeyedPool<TestItem, string>("UnboundedKeyedPool<TestItem,string>", factory, criteria);
        }
    }
}
