using System;
using NUnit.Framework;

namespace FirstLook.Common.Pool.Spi
{
    [TestFixture]
    public class BoundedKeyedPoolLifeCycleTest : KeyedPoolLifeCycleTest
    {
        private readonly TimeSpan oneMinute = new TimeSpan(TimeSpan.TicksPerMinute);

        private readonly TimeSpan never = new TimeSpan(0);

        [SetUp]
        public override void SetUp()
        {
            IKeyedPooledItemFactory<LifeCycleItem,string> factory = new LifeCyclePoolItemFactory();

            Bounds bounds = new Bounds(0, 1);

            EvictionCriteria criteria = new EvictionCriteria(oneMinute, oneMinute, 0, never);

            pool = new BoundedKeyedPool<LifeCycleItem, string>("BoundedKeyedPool<LifeCycleItem,string>", factory, bounds, criteria);
        }
    }
}
