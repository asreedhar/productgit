using NUnit.Framework;

namespace FirstLook.Common.Pool.Spi
{
    public abstract class PoolLifeCycleTest
    {
        protected IPool<LifeCycleItem> pool;

        public abstract void SetUp();

        [TearDown]
        public void TearDown()
        {
            pool.Close();

            pool = null;
        }

        [Test]
        public void Pool_Borrow_Activated()
        {
            LifeCycleItem item = pool.BorrowItem();

            Assert.IsTrue(item.Activated);
        }

        [Test]
        public void Pool_Borrow_Validated()
        {
            LifeCycleItem item = pool.BorrowItem();

            Assert.IsTrue(item.Validated);
        }

        [Test]
        public void Pool_Return_Passivated()
        {
            LifeCycleItem item = pool.BorrowItem();

            pool.ReturnItem(item);

            Assert.IsTrue(item.Passivated);
        }

        [Test]
        public void Pool_Close_Destroyed()
        {
            LifeCycleItem item = pool.BorrowItem();

            pool.ReturnItem(item);

            pool.Close();

            Assert.IsTrue(item.Destroyed);
        }

        protected class LifeCycleItem
        {
            private readonly string name;
            private bool activated;
            private bool validated;
            private bool passivated;
            private bool destroyed;

            public LifeCycleItem(string name)
            {
                this.name = name;
            }

            public string Name
            {
                get { return name; }
            }

            public bool Activated
            {
                get { return activated; }
                set { activated = value; }
            }

            public bool Validated
            {
                get { return validated; }
                set { validated = value; }
            }

            public bool Passivated
            {
                get { return passivated; }
                set { passivated = value; }
            }

            public bool Destroyed
            {
                get { return destroyed; }
                set { destroyed = value; }
            }
        }

        protected class LifeCyclePoolItemFactory : IPooledItemFactory<LifeCycleItem>
        {
            private int counter = 0;

            #region PooledItemFactory<TestItem> Members

            public LifeCycleItem MakeItem()
            {
                lock (this)
                {
                    try
                    {
                        return new LifeCycleItem(counter.ToString());
                    }
                    finally
                    {
                        counter++;
                    }
                }
            }

            public void DestroyItem(LifeCycleItem item)
            {
                item.Destroyed = true;
            }

            public bool ValidateItem(LifeCycleItem item)
            {
                item.Validated = true;

                return true;
            }

            public void ActivateItem(LifeCycleItem item)
            {
                item.Activated = true;
            }

            public void PassivateItem(LifeCycleItem item)
            {
                item.Passivated = true;
            }

            #endregion
        }
    }
}