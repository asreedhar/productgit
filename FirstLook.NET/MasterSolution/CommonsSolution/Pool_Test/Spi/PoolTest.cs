using System;
using NUnit.Framework;

namespace FirstLook.Common.Pool.Spi
{
    public abstract class PoolTest
    {
        protected IPool<TestItem> pool;

        public abstract void SetUp(int minCount, int maxCount);

        [TearDown]
        public void TearDown()
        {
            pool.Close();

            pool = null;
        }

        [Test]
        public void Borrow_Success_Block()
        {
            SetUp(0, 1);

            Assert.IsNotNull(pool.BorrowItem(), "Borrow Item");
        }

        [Test]
        public void Borrow_Success_NonBlock()
        {
            SetUp(0, 1);

            Assert.IsNotNull(pool.BorrowItem(new TimeSpan(0)), "Borrow Item");
        }

        [Test]
        public void Borrow_Success_Timeout()
        {
            SetUp(0, 1);

            Assert.IsNotNull(pool.BorrowItem(new TimeSpan(TimeSpan.TicksPerSecond)), "Borrow Item");
        }

        [Test, ExpectedException(typeof(ArgumentException))]
        public void Borrow_Timeout_ArgumentError()
        {
            SetUp(0, 1);

            pool.BorrowItem(new TimeSpan(-2));
        }

        [Test, ExpectedException(typeof(PoolClosedException))]
        public void Borrow_Closed()
        {
            SetUp(0, 1);

            pool.Close();

            pool.BorrowItem();
        }

        [Test, ExpectedException(typeof(ArgumentNullException))]
        public void Return_Null()
        {
            SetUp(0, 1);

            pool.ReturnItem(null);
        }

        [Test, ExpectedException(typeof(ArgumentException))]
        public void Return_Unmanaged()
        {
            SetUp(0, 1);

            pool.ReturnItem(new TestItem("Unmanaged"));
        }

        [Test, ExpectedException(typeof(PoolClosedException))]
        public void Return_Closed()
        {
            SetUp(0, 1);

            TestItem item = pool.BorrowItem();

            pool.Close();

            pool.ReturnItem(item);
        }

        [Test]
        public void Close()
        {
            SetUp(0, 1);

            pool.Close();

            Assert.IsTrue(true, "Close");
        }

        [Test]
        public void Close_Close()
        {
            SetUp(0, 1);

            pool.Close();

            pool.Close();

            Assert.IsTrue(true, "Close");
        }

        protected class TestItem
        {
            private readonly string name;
            private int counter;

            public TestItem(string name)
            {
                this.name = name;
            }

            public string Name
            {
                get { return name; }
            }

            public int Counter
            {
                get { return counter; }
                set { counter = value; }
            }
        }

        protected class TestItemFactory : IPooledItemFactory<TestItem>
        {
            private int counter = 0;

            #region PooledItemFactory<TestItem> Members

            public TestItem MakeItem()
            {
                lock (this)
                {
                    return new TestItem(string.Format("TestItem{0}", ++counter));
                }
            }

            public void DestroyItem(TestItem item)
            {
                
            }

            public bool ValidateItem(TestItem item)
            {
                return true;
            }

            public void ActivateItem(TestItem item)
            {
                
            }

            public void PassivateItem(TestItem item)
            {
                
            }

            #endregion
        }
    }
}
