using System;
using NUnit.Framework;

namespace FirstLook.Common.Pool.Spi
{
    [TestFixture]
    public class BoundedPoolTimerTest : TimerTest
    {
        public override void SetUp(int min, int max, TimeSpan idlePoll, TimeSpan idleTime, int idleCount, TimeSpan idleCountTime)
        {
            IPooledItemFactory<TestItem> factory = new TestItemFactory(Clean_Counter);

            Bounds bounds = new Bounds(min, max);

            EvictionCriteria criteria = new EvictionCriteria(idlePoll, idleTime, idleCount, idleCountTime);

            pool = new CustomBoundedPool("CustomBoundedPool<TestItem>", factory, bounds, criteria);
        }

        public override int NumberIdleItems()
        {
            return ((CustomBoundedPool)pool).IdleCounter;
        }

        public override int NumberLeakItems()
        {
            return ((CustomBoundedPool)pool).LeakCounter;
        }

        class CustomBoundedPool : BoundedPool<TestItem>
        {
            private int leakCounter = 0;
            private int idleCounter = 0;

            public CustomBoundedPool(string instanceName, IPooledItemFactory<TestItem> factory, Bounds bounds,
                                     EvictionCriteria criteria)
                : base(instanceName, factory, bounds, criteria)
            {
                
            }

            public int LeakCounter
            {
                get { return leakCounter; }
            }

            public int IdleCounter
            {
                get { return idleCounter; }
            }

            protected override void HandleItemLeak(int count)
            {
                lock (this) { leakCounter += count; }

                base.HandleItemLeak(count);
            }

            protected override void ShrinkPool(TestItem item)
            {
                lock (this) { idleCounter+=1; }

                base.ShrinkPool(item);
            }
        }
    }
}
