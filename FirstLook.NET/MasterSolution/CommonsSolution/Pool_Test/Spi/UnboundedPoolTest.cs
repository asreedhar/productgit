using System;
using NUnit.Framework;

namespace FirstLook.Common.Pool.Spi
{
    [TestFixture]
    public class UnboundedPoolTest : PoolTest
    {
        private readonly TimeSpan oneMinute = new TimeSpan(TimeSpan.TicksPerMinute);

        private readonly TimeSpan never = new TimeSpan(0);

        public override void SetUp(int minCount, int maxCount)
        {
            IPooledItemFactory<TestItem> factory = new TestItemFactory();

            Bounds bounds = new Bounds(minCount, maxCount);

            EvictionCriteria criteria = new EvictionCriteria(oneMinute, oneMinute, 0, never);

            pool = new UnboundedPool<TestItem>("UnboundedPool<TestItem>", factory, bounds, criteria);
        }
    }
}
