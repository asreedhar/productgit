using System;
using NUnit.Framework;

namespace FirstLook.Common.Pool.Spi
{
    [TestFixture]
    public class UnboundedKeyedPoolTimerTest : KeyedTimerTest
    {
        public override void SetUp(int min, int max, TimeSpan idlePoll, TimeSpan idleTime, int idleCount, TimeSpan idleCountTime)
        {
            IKeyedPooledItemFactory<TestItem,string> factory = new TestItemFactory(Clean_Counter);

            EvictionCriteria criteria = new EvictionCriteria(idlePoll, idleTime, idleCount, idleCountTime);

            pool = new CustomUnboundedKeyedPool("CustomUnboundedKeyedPool<TestItem>", factory, criteria);
        }

        public override int NumberIdleItems()
        {
            return ((CustomUnboundedKeyedPool)pool).IdleCounter;
        }

        public override int NumberLeakItems()
        {
            return ((CustomUnboundedKeyedPool)pool).LeakCounter;
        }

        class CustomUnboundedKeyedPool : UnboundedKeyedPool<TestItem,string>
        {
            private int leakCounter = 0;
            private int idleCounter = 0;

            public CustomUnboundedKeyedPool(string instanceName, IKeyedPooledItemFactory<TestItem,string> factory,
                                     EvictionCriteria criteria)
                : base(instanceName, factory, criteria)
            {
            }

            public int LeakCounter
            {
                get { return leakCounter; }
            }

            public int IdleCounter
            {
                get { return idleCounter; }
            }

            protected override void HandleItemLeak(string key)
            {
                lock (this) { ++leakCounter; }

                base.HandleItemLeak(key);
            }

            protected override void ShrinkPool(string key, TestItem item)
            {
                lock (this) { idleCounter += 1; }

                base.ShrinkPool(key, item);
            }
        }
    }
}
