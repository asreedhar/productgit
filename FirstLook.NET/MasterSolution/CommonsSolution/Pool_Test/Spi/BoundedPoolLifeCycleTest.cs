using System;
using NUnit.Framework;

namespace FirstLook.Common.Pool.Spi
{
    [TestFixture]
    public class BoundedPoolLifeCycleTest : PoolLifeCycleTest
    {
        private readonly TimeSpan oneMinute = new TimeSpan(TimeSpan.TicksPerMinute);

        private readonly TimeSpan never = new TimeSpan(0);

        [SetUp]
        public override void SetUp()
        {
            IPooledItemFactory<LifeCycleItem> factory = new LifeCyclePoolItemFactory();

            Bounds bounds = new Bounds(0,1);

            EvictionCriteria criteria = new EvictionCriteria(oneMinute, oneMinute, 0, never);

            pool = new BoundedPool<LifeCycleItem>("BoundedPool<LifeCycleItem>", factory, bounds, criteria);
        }
    }
}