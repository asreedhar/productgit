using System;
using System.Threading;
using NUnit.Framework;

namespace FirstLook.Common.Pool.Spi
{
    public abstract class TimerTest
    {
        private readonly TimeSpan poll = new TimeSpan(0, 0, 0, 0, 100);

        private readonly TimeSpan time = new TimeSpan(0, 0, 0, 0, 100);

        private readonly TimeSpan zero = new TimeSpan(0);

        protected IPool<TestItem> pool;

        public abstract void SetUp(int min, int max, TimeSpan idlePoll, TimeSpan idleTime, int idleCount,
                                   TimeSpan idleCountTime);

        public abstract int NumberIdleItems();

        public abstract int NumberLeakItems();

        protected int numberOfDisposedItems = 0;

        [TearDown]
        public virtual void TearDown()
        {
            pool.Close();

            pool = null;

            numberOfDisposedItems = 0;
        }

        [Test]
        public void Pool_Idle()
        {
            SetUp(0, 3, poll, time, 0, time);

            TestItem[] items = new TestItem[3];

            for (int i = 0; i < 3; i++)
            {
                items[i] = pool.BorrowItem();
            }

            for (int i = 0; i < 3; i++)
            {
                pool.ReturnItem(items[i]);
            }

            Thread.Sleep(new TimeSpan(TimeSpan.TicksPerSecond*5));

            Assert.AreEqual(3, NumberIdleItems(), "Idle Items");
        }

        [Test]
        public void Pool_Lost_Count()
        {
            SetUp(0, 3, poll, zero, 0, zero);

            Lose_Items();

            GC.Collect();

            GC.WaitForPendingFinalizers();

            Thread.Sleep(new TimeSpan(TimeSpan.TicksPerSecond*5));

            Assert.AreEqual(3, NumberLeakItems(), "Items Leaked");
        }

        [Test]
        public void Pool_Lost_Disposed_Count()
        {
            SetUp(0, 3, poll, zero, 0, zero);

            Lose_Items();

            GC.Collect();

            GC.WaitForPendingFinalizers();

            Thread.Sleep(new TimeSpan(TimeSpan.TicksPerSecond * 5));

            Assert.AreEqual(3, numberOfDisposedItems, "Items Disposed");
        }

        void Lose_Items()
        {
            for (int i = 0; i < 3; i++)
            {
                pool.BorrowItem();
            }
        }

        protected void Clean_Counter()
        {
            lock (this) { ++numberOfDisposedItems; }
        }

        protected delegate void Clean();

        protected class TestItem : IDisposableHandle
        {
            private readonly string name;

            private readonly Cleaner cleaner;

            public TestItem(string name, Clean clean)
            {
                this.name = name;

                cleaner = new Cleaner(clean);
            }

            public string Name
            {
                get { return name; }
            }

            public IDisposable Disposable
            {
                get { return cleaner; }
            }

            class Cleaner : IDisposable
            {
                private readonly Clean clean;

                public Cleaner(Clean clean)
                {
                    this.clean = clean;
                }

                public void Dispose()
                {
                    clean();
                }
            }
        }

        protected class TestItemFactory : IPooledItemFactory<TestItem>
        {
            private readonly Clean clean;

            public TestItemFactory(Clean clean)
            {
                this.clean = clean;
            }

            private int counter = 0;

            #region PooledItemFactory<TestItem> Members

            public TestItem MakeItem()
            {
                lock (this)
                {
                    return new TestItem(string.Format("TestItem{0}", ++counter), clean);
                }
            }

            public void DestroyItem(TestItem item)
            {

            }

            public bool ValidateItem(TestItem item)
            {
                return true;
            }

            public void ActivateItem(TestItem item)
            {

            }

            public void PassivateItem(TestItem item)
            {

            }

            #endregion
        }
    }
}
