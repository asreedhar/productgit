using System;
using System.Threading;
using NUnit.Framework;

namespace FirstLook.Common.Pool.Spi
{
    [TestFixture]
    public class BoundedPoolTest : PoolTest
    {
        private readonly TimeSpan oneMinute = new TimeSpan(TimeSpan.TicksPerMinute);

        private readonly TimeSpan never = new TimeSpan(0);

        public override void SetUp(int minCount, int maxCount)
        {
            IPooledItemFactory<TestItem> factory = new TestItemFactory();

            Bounds bounds = new Bounds(minCount, maxCount);

            EvictionCriteria criteria = new EvictionCriteria(oneMinute, oneMinute, 0, never);

            pool = new BoundedPool<TestItem>("BoundedPool<TestItem>", factory, bounds, criteria);
        }

        [Test]
        public void Borrow_Empty_Timeout()
        {
            SetUp(0, 1);

            pool.BorrowItem(); // empty the pool

            Assert.IsNull(pool.BorrowItem(new TimeSpan(TimeSpan.TicksPerSecond)), "Borrow Empty Timeout");
        }

        [Test]
        public void Borrow_Loop()
        {
            SetUp(0, 1);

            const int numWorkers = 2;

            Thread[] workers = new Thread[numWorkers];

            for (int i = 0; i < numWorkers; i++)
            {
                workers[i] = new Thread(Borrow_Item);
                workers[i].Start();
            }

            for (int i = 0; i < numWorkers; i++)
            {
                workers[i].Join();
            }

            TestItem item = pool.BorrowItem();

            Assert.AreEqual(item.Counter, 20, "Borrow Empty Timeout");

            pool.ReturnItem(item);
        }

        void Borrow_Item()
        {
            for (int i = 0; i < 10; i++)
            {
                TestItem item = pool.BorrowItem();

                item.Counter++;

                Thread.Sleep(100);

                pool.ReturnItem(item);
            }
        }
    }
}
