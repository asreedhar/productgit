﻿using System.Net;
using System.Web.Services.Protocols;


namespace CASWrapper
{
    public class CasServiceWrapper
    {
        private string _url;

        public CasServiceWrapper(string url)
        {
            _url = url;
        }

        public string CreateTicket(string user, string password, out bool authenticated)
        {
            authenticated = false;

            try
            {
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                CentralAuthenticationServiceWrapper wrapper = new CentralAuthenticationServiceWrapper(_url);
                UsernamePasswordCredentials credentials = new UsernamePasswordCredentials();
                credentials.password = password;
                credentials.username = user;

                string ticket = wrapper.createTicketGrantingTicket(credentials);
                authenticated = true;
                return ticket;
            }
            catch (SoapHeaderException ex)
            {
                if (ex.Message == "error.authentication.credentials.bad")
                {
                    return string.Empty;
                }
                throw;
            }
        }

    }
}
