using System;
using System.Runtime.Serialization;

namespace FirstLook.Common.Security.CentralAuthenticationService
{
    [Serializable]
    public class CasAuthenticationException : CasException
    {
        public CasAuthenticationException() { }

        public CasAuthenticationException(string message) : base(message) { }

        public CasAuthenticationException(string message, Exception cause) : base(message, cause) { }

        protected CasAuthenticationException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
