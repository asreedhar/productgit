using System.Configuration;
using System.Diagnostics.CodeAnalysis;

namespace FirstLook.Common.Security.CentralAuthenticationService.Client.Configuration
{
    [SuppressMessage("Microsoft.Design", "CA1010:CollectionsShouldImplementGenericInterface", Justification = "ASP.NET Pattern")]
    public class FileConfigurationElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new FileConfigurationElement();
        }

        protected override ConfigurationElement CreateNewElement(string elementName)
        {
            return new FileConfigurationElement(elementName);
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((FileConfigurationElement)element).Name;
        }

        protected override string ElementName
        {
            get { return "files"; }
        }
    }
}
