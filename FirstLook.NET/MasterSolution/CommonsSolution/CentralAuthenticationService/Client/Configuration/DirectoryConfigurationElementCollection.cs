using System.Configuration;
using System.Diagnostics.CodeAnalysis;

namespace FirstLook.Common.Security.CentralAuthenticationService.Client.Configuration
{
    [SuppressMessage("Microsoft.Design", "CA1010:CollectionsShouldImplementGenericInterface", Justification = "ASP.NET Pattern")]
    public class DirectoryConfigurationElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new DirectoryConfigurationElement();
        }

        protected override ConfigurationElement CreateNewElement(string elementName)
        {
            return new DirectoryConfigurationElement(elementName);
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((DirectoryConfigurationElement)element).Name;
        }

        protected override string ElementName
        {
            get { return "directories"; }
        }
    }
}
