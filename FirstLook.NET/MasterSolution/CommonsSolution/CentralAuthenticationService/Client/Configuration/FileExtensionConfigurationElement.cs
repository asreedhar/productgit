using System.Configuration;

namespace FirstLook.Common.Security.CentralAuthenticationService.Client.Configuration
{
    public class FileExtensionConfigurationElement : ConfigurationElement
    {
        public FileExtensionConfigurationElement()
        {
            Name = string.Empty;
        }

        public FileExtensionConfigurationElement(string name)
        {
            Name = name;
        }

        [ConfigurationProperty("name", DefaultValue = "", IsRequired = true)]
        public string Name
        {
            get { return (string)this["name"]; }
            set { this["name"] = value; }
        }
    }
}
