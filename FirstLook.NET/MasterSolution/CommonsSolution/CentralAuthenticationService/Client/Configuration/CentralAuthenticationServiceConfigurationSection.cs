using System;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;

namespace FirstLook.Common.Security.CentralAuthenticationService.Client.Configuration
{
    public class CentralAuthenticationServiceConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("applicationServerName", IsRequired = true)]
        public string ApplicationServerName
        {
            get { return (string)this["applicationServerName"]; }
            set { this["applicationServerName"] = value; }
        }

        [SuppressMessage("Microsoft.Design", "CA1056:UriPropertiesShouldNotBeStrings", Justification = "ASP.NET ConfigurationSection Pattern")]
        [ConfigurationProperty("validateUrl", IsRequired = true)]
        public string ValidateUrl
        {
            get { return (string) this["validateUrl"]; }
            set { this["validateUrl"] = value; }
        }

        [SuppressMessage("Microsoft.Design", "CA1056:UriPropertiesShouldNotBeStrings", Justification = "ASP.NET ConfigurationSection Pattern")]
        [SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "Login", Justification = "CAS Specification Uses Login")]
        [ConfigurationProperty("loginUrl", IsRequired = true)]
        public string LoginUrl
        {
            get { return (string) this["loginUrl"]; }
            set { this["loginUrl"] = value; }
        }

        [SuppressMessage("Microsoft.Design", "CA1056:UriPropertiesShouldNotBeStrings", Justification = "ASP.NET ConfigurationSection Pattern")]
        [SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "Logout", Justification = "CAS Specification Uses Logout")]
        [ConfigurationProperty("logoutUrl", IsRequired = true)]
        public string LogoutUrl
        {
            get { return (string) this["logoutUrl"]; }
            set { this["logoutUrl"] = value; }
        }

        [ConfigurationProperty("gateway", DefaultValue = false, IsRequired = true)]
        public bool Gateway
        {
            get { return (bool) this["gateway"]; }
            set { this["gateway"] = value; }
        }

        [ConfigurationProperty("renew", DefaultValue = false, IsRequired = true)]
        public bool Renew
        {
            get { return (bool) this["renew"]; }
            set { this["renew"] = value; }
        }
		
		[ConfigurationProperty("ignoreSslErrors", DefaultValue = false, IsRequired = false)]
		public bool IgnoreSslErrors
		{
			get { return (bool)this["ignoreSslErrors"]; }
			set { this["ignoreSslErrors"] = value; }
		}

        [ConfigurationProperty("exceptions", IsDefaultCollection = true)]
        public ExceptionConfigurationElement Exceptions
        {
            get { return (ExceptionConfigurationElement) this["exceptions"]; }
        }

        public bool IgnoreAppRelativeFilePath(string path)
        {
            string extension = (path.LastIndexOf('.') == -1)
                ? string.Empty
                : path.Substring(path.LastIndexOf('.'));

            string directory = path.Substring(0, path.LastIndexOf('/') + 1);

            bool ignore = false;

            foreach (DirectoryConfigurationElement element in Exceptions.Directories)
            {
                if (ElementNameIsValid(element.Name))
                    continue;

                if (string.Compare(element.Name, directory, StringComparison.OrdinalIgnoreCase) == 0 || directory.StartsWith(element.Name, StringComparison.OrdinalIgnoreCase))
                {
                    ignore = true;
                    break;
                }
            }

            if (!ignore)
            {
                foreach (FileExtensionConfigurationElement element in Exceptions.FileExtensions)
                {
                    if (ElementNameIsValid(element.Name))
                        continue;

                    if (string.Compare(element.Name, extension, StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        ignore = true;
                        break;
                    }
                }
            }

            if (!ignore)
            {
                foreach (FileConfigurationElement element in Exceptions.Files)
                {
                    if (ElementNameIsValid(element.Name))
                        continue;

                    if (string.Compare(element.Name, path, StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        ignore = true;
                        break;
                    }
                }
            }

            return ignore;
        }

        private static bool ElementNameIsValid(string elementName)
        {
            return string.IsNullOrEmpty(elementName) || !elementName.StartsWith("~/", StringComparison.Ordinal);
        }
    }
}