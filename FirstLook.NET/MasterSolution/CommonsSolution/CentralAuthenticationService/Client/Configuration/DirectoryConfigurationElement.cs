using System.Configuration;

namespace FirstLook.Common.Security.CentralAuthenticationService.Client.Configuration
{
    public class DirectoryConfigurationElement : ConfigurationElement
    {
        public DirectoryConfigurationElement()
        {
            Name = string.Empty;
        }

        public DirectoryConfigurationElement(string name)
        {
            Name = name;
        }

        [ConfigurationProperty("name", DefaultValue = "", IsRequired = true)]
        public string Name
        {
            get { return (string) this["name"]; }
            set { this["name"] = value; }
        }
    }
}
