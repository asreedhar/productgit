using System.Configuration;
using System.Diagnostics.CodeAnalysis;

namespace FirstLook.Common.Security.CentralAuthenticationService.Client.Configuration
{
    [SuppressMessage("Microsoft.Design", "CA1010:CollectionsShouldImplementGenericInterface", Justification = "ASP.NET Pattern")]
    public class FileExtensionConfigurationElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new FileExtensionConfigurationElement();
        }

        protected override ConfigurationElement CreateNewElement(string elementName)
        {
            return new FileExtensionConfigurationElement(elementName);
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((FileExtensionConfigurationElement)element).Name;
        }

        protected override string ElementName
        {
            get { return "extensions"; }
        }
    }
}
