using System;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml.Serialization;
using FirstLook.Common.Security.CentralAuthenticationService.Xml;

namespace FirstLook.Common.Security.CentralAuthenticationService.Client
{
    internal class ServiceTicketValidator
    {
        #region ConstructorProperties

        private string validateUrl;
        private string serviceTicket;
        private string service;
        private bool renew;
		private bool ignoreSslErrors;
        
		#endregion

        #region State

        private string user;
        private string errorMessage;

        #endregion

        #region ConstructorPropertyMethods

        public string ValidateUrl
        {
            get { return validateUrl; }
            set
            {
                Uri uri;
                if (Uri.TryCreate(value, UriKind.RelativeOrAbsolute, out uri))
                {
                    validateUrl = value;
                }
                else
                {
                    CasValidationException exception = new CasValidationException("Validation URL is bad");
                    exception.Data["URL"] = value;
                    throw exception;
                }
            }
        }

        public string ServiceTicket
        {
            get { return serviceTicket; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new CasValidationException("ServiceTicket is null or blank");
                }
                serviceTicket = value;
            }
        }

        public string Service
        {
            get { return service; }
            set { service = value; }
        }

        public bool Renew
        {
            get { return renew; }
            set { renew = value; }
        }
		
		public bool IgnoreSslErrors
		{
			get { return ignoreSslErrors; }
			set { ignoreSslErrors = value; }
		}

        #endregion

        #region StateAccessors
        
		public string User
        {
            get { return user; }
            protected set
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentException("User is null or blank");
                user = value;
            }
        }

        #endregion

        public void Validate()
        {
            StringBuilder url = new StringBuilder();
            url.Append(ValidateUrl);
            url.Append("?");
            url.Append("service=").Append(Uri.EscapeDataString(Service));
            url.Append("&");
            url.Append("ticket=").Append(ServiceTicket);

            if (Renew)
            {
                url.Append("&renew=true");
            }

			ServicePointManager.ServerCertificateValidationCallback += ValidateRemoteCertificate;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url.ToString());

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            XmlSerializer serializer = new XmlSerializer(typeof(ServiceResponseType));

            ServiceResponseType serviceResponse = (ServiceResponseType)serializer.Deserialize(response.GetResponseStream());

            if (serviceResponse.Item is AuthenticationFailureType)
            {
                AuthenticationFailureType failure = (AuthenticationFailureType)serviceResponse.Item;
                errorMessage = failure.Value;
                throw new CasValidationException(errorMessage);
            }
            else if (serviceResponse.Item is AuthenticationSuccessType)
            {
                AuthenticationSuccessType success = (AuthenticationSuccessType)serviceResponse.Item;
                User = success.User;
            }
        }

		private bool ValidateRemoteCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors policyErrors)
		{
			if (IgnoreSslErrors)
			{
				return true;
			}
			else
			{
				return policyErrors == SslPolicyErrors.None;
			}
		}		
    }
}
