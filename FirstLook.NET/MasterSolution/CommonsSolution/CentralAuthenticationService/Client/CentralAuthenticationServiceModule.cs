using System;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Security;
using FirstLook.Common.Security.CentralAuthenticationService.Client.Configuration;

namespace FirstLook.Common.Security.CentralAuthenticationService.Client
{
	/// <summary>
	/// Custom IHttpModule to allow authentication through a CAS server.
	/// </summary>
	public sealed class CentralAuthenticationServiceModule : IHttpModule
	{
		public static readonly string CasFilterGateway = "cas_filter_gateway";
		public static readonly string CasFilterUser = "cas_filter_user";
		public static readonly string CasFilterReceipt = "cas_filter_receipt";

		public void Dispose()
		{
			// no state to destroy
		}

		public void Init(HttpApplication context)
		{
			context.AuthenticateRequest += OnAuthenticateRequest;
		}

		[SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "Authentication check should consume all errors and redirect back to authentication provider")]
		private static void OnAuthenticateRequest(object sender, EventArgs args)
		{
			HttpRequest request = HttpContext.Current.Request;

			CentralAuthenticationServiceConfigurationSection configuration = (CentralAuthenticationServiceConfigurationSection)
				ConfigurationManager.GetSection("centralAuthenticationService");

			if (configuration == null)
			{
				configuration = new CentralAuthenticationServiceConfigurationSection();
			}

			if (configuration.IgnoreAppRelativeFilePath(request.AppRelativeCurrentExecutionFilePath))
			{
				return; // this path is not protected by an authorization requirement so stay anonymous
			}

			// extract the user from the cookie which contains the encrypted forms-authentication ticket

			Receipt receipt = null;

			HttpCookie cookie = request.Cookies[FormsAuthentication.FormsCookieName];

			int version = 0;

		    HttpResponse response = HttpContext.Current.Response;

		    if (cookie != null)
			{
				try
				{
					FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);

					if (ticket != null && !ticket.Expired)
					{
						version = ticket.Version;

						bool ticketIsAuthenticated = false;

						bool ticketRenew = false;

						foreach (string pair in ticket.UserData.Split(new char[] { ';' }))
						{
							string[] kv = pair.Split(new char[] { '=' });

							if (kv[0] == "IsAuthenticated")
								ticketIsAuthenticated = Boolean.Parse(kv[1]);

							else if (kv[0] == "Renew")
								ticketRenew = Boolean.Parse(kv[1]);
						}
						receipt = new Receipt(new GenericPrincipal(new GenericIdentity(ticket.Name, "CAS"), new string[] { }),
										ticketIsAuthenticated, ticketRenew);
					}
				}
				catch
				{
					// this handles the case when you incorrectly jump boxes (blame the F5) - in which case you'll have a cookie that
					// you can't decrypt as the machine key has changed.  When this happens blow away the cookie - DW/SW 10/26/07
					response.Cookies[FormsAuthentication.FormsCookieName].Expires = DateTime.Now.AddYears(-30);
				}
			}

			// load the cas configurationu

			if (configuration.Gateway && configuration.Renew)
			{
				throw new CasValidationException("Cannot set both renew and gateway flags");
			}

			// test the authentication

			bool valid = false;

			if (receipt != null)
			{
				valid = Authenticate(receipt, configuration.Renew, version);
			}
			else
			{
				string ticket = request["ticket"];

				if (!string.IsNullOrEmpty(ticket))
				{
					ServiceTicketValidator svt = new ServiceTicketValidator();
					svt.Service = ServiceUrl(request, configuration.ApplicationServerName);
					svt.ServiceTicket = ticket;
					svt.ValidateUrl = ValidateUrl(request, configuration.ValidateUrl);
					svt.Renew = configuration.Renew;
					svt.IgnoreSslErrors = configuration.IgnoreSslErrors;

					try
					{
						try
						{
							svt.Validate();
						}
						catch (CasValidationException ve)
						{
							throw new CasAuthenticationException("Unable to validate on ServiceTicketValidator", ve);
						}

						try
						{
							receipt = new Receipt(new GenericPrincipal(new GenericIdentity(svt.User, "CAS"), new string[] { }), true, svt.Renew);
						}
						catch (CasValidationException ve)
						{
							throw new CasAuthenticationException("Validation of ServiceTicketValidator did not result in an internally consistent Receipt", ve);
						}

						valid = Authenticate(receipt, configuration.Renew, version);
					}
					catch (CasAuthenticationException)
					{
						// log error here
					}
				}
				else
				{
					HttpCookie gatewayCookie = request.Cookies[CasFilterGateway];

					if (configuration.Gateway && gatewayCookie != null)
					{
						receipt = new Receipt(new GenericPrincipal(new GenericIdentity("anonymous", "CAS"), new string[] { }), false, false);

						valid = Authenticate(receipt, configuration.Renew, version);
					}
					else
					{
						response.Cookies.Add(new HttpCookie(CasFilterGateway));
					}
				}
			}

			if (!valid)
			{
				string loginUrl = LoginUrl(request, configuration.LoginUrl);

				if (string.IsNullOrEmpty(loginUrl))
				{
					throw new CasException("Can't redirect without login url");
				}

				string serviceUrl = ServiceUrl(request, configuration.ApplicationServerName);

				string callbackUrl = CallbackUrl(loginUrl, serviceUrl, configuration.Renew);

                if (IsJsonWebServiceCall(request))
                {
                    callbackUrl = CallbackUrl(loginUrl, request.Headers["Referer"], configuration.Renew);

                    JsonWebServiceCallAuthenticationFailure(response, callbackUrl);
                }
                else
                {
                    response.Redirect(callbackUrl, true);
                }
			}
		}

	    private static bool IsJsonWebServiceCall(HttpRequest request)
        {
            string path = request.AppRelativeCurrentExecutionFilePath,
			           extension = (path.LastIndexOf('.') == -1)
			                           ? string.Empty
			                           : path.Substring(path.LastIndexOf('.'));

            if (!".asmx".Equals(extension))
            {
                return false;
            }

            return (request.Headers["Content-Type"] ?? "").Contains("application/json");
        }

        private static void JsonWebServiceCallAuthenticationFailure(HttpResponse response, string callbackUrl)
        {
            response.ClearHeaders();
            response.ClearContent();
            response.Clear();
            response.StatusCode = 401;
            response.StatusDescription = HttpWorkerRequest.GetStatusDescription(401);
            response.ContentType = "application/json";
            response.AddHeader("jsonerror", "true");
            response.AddHeader("WWW-Authenticate", string.Format("CAS login=\"{0}\"", callbackUrl));

            using (StreamWriter writer = new StreamWriter(response.OutputStream))
            {
                writer.Write("{{\"auth-scheme\":\"CAS\",\"auth-login\":\"{0}\"}}", callbackUrl);
                writer.Flush();
            }

            response.Flush();
            response.Close();
        }

		private static bool Authenticate(Receipt receipt, bool renew, int version)
		{
			bool valid = false;

			if (IsReceiptValid(receipt, renew))
			{
				HttpContext.Current.User = receipt.Principal;
				AddFormsAuthenticationTicket(receipt, version + 1);
				valid = true;
			}

			return valid;
		}

		private static void AddFormsAuthenticationTicket(Receipt receipt, int version)
		{
			FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
				version,
				receipt.Principal.Identity.Name,
				DateTime.Now,
				DateTime.Now.AddMinutes(720),
				false,
				"IsAuthenticated=" + receipt.IsAuthenticated + ";Renew=" + receipt.Renew,
				FormsAuthentication.FormsCookiePath);

			HttpContext.Current.Response.Cookies.Add(
				new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket)));
		}

		private static String ServiceUrl(HttpRequest request, string serverName)
		{
			if (!string.IsNullOrEmpty(request["service"]))
			{
				return request["service"];
			}

			StringBuilder queryParameters = new StringBuilder();

			foreach (string key in request.QueryString.AllKeys)
			{
				if ("ticket".Equals(key))
					continue;
				if (queryParameters.Length > 0)
					queryParameters.Append("&");
				queryParameters.Append(key).Append("=").Append(request.QueryString[key]);
			}

			StringBuilder url = new StringBuilder();

			if (request.IsSecureConnection
				|| "https".Equals(request.Headers["X-Forwarded-Proto"])  // standard spelling
				|| "https".Equals(request.Headers["X_FORWARDED_PROTO"])) // alt-spelling
			{
				url.Append("https://");
			}
			else
			{
				url.Append("http://");
			}

			if (!string.IsNullOrEmpty(request.Headers["X-Forwarded-Host"]))
			{
				url.Append(request.Headers["X-Forwarded-Host"]);
			}
			else if (!string.IsNullOrEmpty(request.Headers["X_FORWARDED_HOST"]))
			{
				url.Append(request.Headers["X_FORWARDED_HOST"]);
			}
			else if (!string.IsNullOrEmpty(request.Headers["Host"]))
			{
				url.Append(request.Headers["Host"]);
			}
			else
			{
				url.Append(serverName); // HTTP/1.0 Fallback Host
			}

			url.Append(request.Path);

			if (queryParameters.Length > 0)
			{
				url.Append("?").Append(queryParameters);
			}

			return url.ToString();
		}

		private static string CallbackUrl(string url, string serviceUrl, bool renew)
		{
			StringBuilder callback = new StringBuilder();
			callback.Append(url).Append("?service=").Append(Uri.EscapeDataString(serviceUrl));
			if (renew)
			{
				callback.Append("&renew=").Append(renew);
			}
			return callback.ToString();
		}

		private static bool IsReceiptValid(Receipt receipt, bool renew)
		{
			return !(renew && !receipt.Renew);
		}

		private static string AuthHostUrl(HttpRequest request, string defaultUrl, string finalPath)
		{
			string authUrlbase = request.Headers["X_AUTH_HOST"];
			if (!string.IsNullOrEmpty(authUrlbase))
			{
				StringBuilder url = new StringBuilder(authUrlbase);
				if (authUrlbase.EndsWith("/", StringComparison.Ordinal))
				{
					url.Append(finalPath);
				}
				else
				{
					url.Append("/").Append(finalPath);
				}
				return url.ToString();
			}
			else
			{
				return defaultUrl;
			}
		}

		private static string LoginUrl(HttpRequest request, string defaultLoginUrl)
		{
			//corresponds to /cas/login, where login is the last part of the url path
			return AuthHostUrl(request, defaultLoginUrl, "login");
		}

		private static string ValidateUrl(HttpRequest request, string defaultValidateUrl)
		{
			//corresponds to /cas/login, where login is the last part of the url path
			return AuthHostUrl(request, defaultValidateUrl, "serviceValidate");
		}

		private static string LogoutUrl(HttpRequest request, string defaultLogoutUrl)
		{
			//corresponds to /cas/login, where login is the last part of the url path
			return AuthHostUrl(request, defaultLogoutUrl, "logout");
		}
	}
}