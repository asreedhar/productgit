using System.Security.Principal;

namespace FirstLook.Common.Security.CentralAuthenticationService.Client
{
    internal class Receipt
    {
        private readonly IPrincipal principal;
        private readonly bool authenticated;
        private readonly bool renew;

        public Receipt(IPrincipal principal, bool authenticated, bool renew)
        {
            this.principal = principal;
            this.authenticated = authenticated;
            this.renew = renew;
        }

        public IPrincipal Principal
        {
            get { return principal; }
        }

        public bool IsAuthenticated
        {
            get { return authenticated; }
        }

        public bool Renew
        {
            get { return renew; }
        }
    }
}
