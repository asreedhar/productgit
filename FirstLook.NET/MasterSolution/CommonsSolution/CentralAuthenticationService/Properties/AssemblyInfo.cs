using System.Reflection;
using System;
using System.Resources;

/* This is not using FirstLookAssemblyInfo because if the version changes on this assembly
 * the Web deployment projects do not build because the web.configs have a hard coded version for this assembly
 * <section name="centralAuthenticationService" type="FirstLook.Common.Security.CentralAuthenticationService.Client.Configuration.CentralAuthenticationServiceConfigurationSection, FirstLook.Common.Security.CentralAuthenticationService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" allowLocation="true" allowDefinition="Everywhere" restartOnExternalChanges="false" requirePermission="false" />
 * and the version must be specified.
 */

[assembly: AssemblyTitle("CentralAuthenticationService")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyProduct("CentralAuthenticationService")]

[assembly: AssemblyCompany("Firstlook, LLC")]
[assembly: AssemblyCopyright("Copyright � Firstlook, LLC 2007")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyTrademark("")]

[assembly: CLSCompliant(true)]
[assembly: AssemblyCulture("")]
[assembly: NeutralResourcesLanguage("en-US")]
[assembly: AssemblyConfiguration("")]
