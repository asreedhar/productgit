using System;
using System.Runtime.Serialization;

namespace FirstLook.Common.Security.CentralAuthenticationService
{
    [Serializable]
    public class CasValidationException : CasException
    {
        public CasValidationException() { }

        public CasValidationException(string message) : base(message) { }

        public CasValidationException(string message, Exception cause) : base(message, cause) { }

        protected CasValidationException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
