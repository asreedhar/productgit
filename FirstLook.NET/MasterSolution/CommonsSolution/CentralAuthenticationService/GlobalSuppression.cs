
using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "FirstLook.Common.Security.CentralAuthenticationService", Justification = "Acts as parent to client/server/xml child namespaces")]
[assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "FirstLook.Common.Security.CentralAuthenticationService.Client", Justification = "Client namespace will be small until we implement proxy ticket behaviour")]