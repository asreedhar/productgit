using System;
using System.Runtime.Serialization;

namespace FirstLook.Common.Security.CentralAuthenticationService
{
    [Serializable]
    public class CasException : Exception
    {
        public CasException() { }

        public CasException(string message) : base(message) { }

        public CasException(string message, Exception cause) : base(message, cause) { }

        protected CasException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
