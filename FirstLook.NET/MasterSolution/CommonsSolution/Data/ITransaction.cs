using System;

namespace FirstLook.Common.Data
{
    public interface ITransaction : IDisposable
    {
        void Commit();

        void Rollback();

    }
}