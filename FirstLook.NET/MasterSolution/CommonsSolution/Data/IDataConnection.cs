using System.Data;

namespace FirstLook.Common.Data
{
    public interface IDataConnection : IDbConnection
    {
        IDataCommand CreateCommand(IDataCommandTemplate commandTemplate, DataParameterValue parameterValue);
    }
}
