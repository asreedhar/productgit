using System.Collections.Generic;
using System.Data;

namespace FirstLook.Common.Data
{
    public interface IDataCommandTemplate
    {
        string CommandText
        {
            get;
            set;
        }

        CommandType CommandType
        {
            get;
            set;
        }

        IList<IDataParameterTemplate> Parameters
        {
            get;
        }

        IDataMap DataMap
        {
            get;
            set;
        }
    }
}
