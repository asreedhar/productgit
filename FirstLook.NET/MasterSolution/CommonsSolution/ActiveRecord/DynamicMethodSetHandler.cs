namespace FirstLook.Common.ActiveRecord
{
    public delegate void DynamicMethodSetHandler(object source, object value);
}