using System;
using System.Diagnostics;

namespace dundasimagehandler
{
    internal class Counter : IDisposable
    {
        private readonly PerformanceCounter rate, avgDuration, avgDurationBase;
        private readonly long startTime;
        private long endTime;

        public Counter(PerformanceCounter rate, PerformanceCounter avgDuration, PerformanceCounter avgDurationBase)
        {
            this.rate = rate;
            this.avgDuration = avgDuration;
            this.avgDurationBase = avgDurationBase;
            NativeMethods.QueryPerformanceCounter(ref startTime);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                NativeMethods.QueryPerformanceCounter(ref endTime);
                rate.Increment();
                avgDuration.IncrementBy(endTime - startTime);
                avgDurationBase.Increment();
            }
        }
    }
}
