using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;

namespace dundasimagehandler
{
    /// <summary>
    /// Class to install the performance counters for the custom Dundas IImageHandler
    /// implementations.  To install the counters, run the following command:
    /// <code>installutil "C:\full\path\to\dundasimagehandler.dll"</code>.
    /// </summary>
    /// <see cref="http://msdn2.microsoft.com/en-us/library/50614e95(VS.80).aspx"/>
    [RunInstaller(true)]
    public class ImageHanderPerformanceCounterInstaller : PerformanceCounterInstaller
    {
        internal const string DatabaseCategoryName = "Dundas:ChartHttpHandler:Database";

        internal const string FileCategoryName = "Dundas:ChartHttpHandler:File";

        internal static readonly string[] OperationNames = new string[] { "Exists", "Remove", "Retrieve", "Store" };

        internal const string CounterNameFormat_Rate = "{0} Operations/Sec";

        internal const string CounterNameFormat_Time = "{0} Avg Operation Time";

        internal const string CounterNameFormat_Base = "{0} Avg Operation Time Base";

        internal const int CounterIndex_Exists = 0;

        internal const int CounterIndex_Remove = 1;

        internal const int CounterIndex_Retrieve = 2;

        internal const int CounterIndex_Store = 3;

        public ImageHanderPerformanceCounterInstaller()
        {
            InstallPerformanceCounterCategory(DatabaseCategoryName);
            InstallPerformanceCounterCategory(FileCategoryName);
        }

        private static void InstallPerformanceCounterCategory(string categoryName)
        {
            if (!PerformanceCounterCategory.Exists(categoryName))
            {
                CounterCreationDataCollection counters = new CounterCreationDataCollection();

                foreach (string operationName in OperationNames)
                {
                    // Rate

                    CounterCreationData ratePerSecond = new CounterCreationData();
                    ratePerSecond.CounterName = string.Format(CultureInfo.InstalledUICulture, CounterNameFormat_Rate, operationName);
                    ratePerSecond.CounterHelp = string.Format(CultureInfo.InstalledUICulture, "Number of ChartHttpHandler.IImageHandler.{0} operations executed per second", operationName);
                    ratePerSecond.CounterType = PerformanceCounterType.RateOfCountsPerSecond32;
                    counters.Add(ratePerSecond);

                    // Average Time per Operation

                    CounterCreationData avgDuration = new CounterCreationData();
                    avgDuration.CounterName = string.Format(CultureInfo.InstalledUICulture, CounterNameFormat_Time, operationName);
                    avgDuration.CounterHelp = string.Format(CultureInfo.InstalledUICulture, "Average duration of a ChartHttpHandler.IImageHandler.{0} operations", operationName);
                    avgDuration.CounterType = PerformanceCounterType.AverageTimer32;
                    counters.Add(avgDuration);

                    // Base Counter for Counting Average Time

                    CounterCreationData avgDurationBase = new CounterCreationData();
                    avgDurationBase.CounterName = string.Format(CultureInfo.InstalledUICulture, CounterNameFormat_Base, operationName);
                    avgDurationBase.CounterHelp = string.Format(CultureInfo.InstalledUICulture, "Average duration of a ChartHttpHandler.IImageHandler.{0} operations base", operationName);
                    avgDurationBase.CounterType = PerformanceCounterType.AverageBase;
                    counters.Add(avgDurationBase);
                }

                PerformanceCounterCategory.Create(categoryName, "Help text", PerformanceCounterCategoryType.SingleInstance, counters);
            }
        }
    }
}
