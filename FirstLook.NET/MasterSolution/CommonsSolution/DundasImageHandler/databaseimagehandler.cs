using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Diagnostics.CodeAnalysis;
using Dundas.Charting.WebControl;
using MyInstaller=dundasimagehandler.ImageHanderPerformanceCounterInstaller;

namespace dundasimagehandler
{
    /// <summary>
    /// The Dundas chaps have exceeded themselves and require a class with an all lower case name
    /// assembly and class name. Genius. Pure and simple.
    /// </summary>
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "databaseimagehandler"),
     SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "databaseimagehandler")]
    public sealed class databaseimagehandler : baseimagehandler, ChartHttpHandler.IImageHandler
    {
        public databaseimagehandler() : base(MyInstaller.DatabaseCategoryName)
        {
        }

        #region Data Access
        
        private static IDbConnection CreateConnection(string database)
        {
            if (string.IsNullOrEmpty(database))
            {
                throw new ArgumentNullException("database", "You must specify a database to store your images.");
            }

            IDbConnection connection = null;

            try
            {
                string providerName = ConfigurationManager.ConnectionStrings[database].ProviderName;
                connection = DbProviderFactories.GetFactory(providerName).CreateConnection();
                connection.ConnectionString = ConfigurationManager.ConnectionStrings[database].ConnectionString;
                connection.Open();
                return connection;
            }
            catch
            {
                if (connection != null)
                {
                    connection.Close();
                }

                throw;
            }
        }

        private static string DatabaseName(ChartHttpHandler.StorageSettings settings)
        {
            return settings.Params["database"];
        }

        private static IDbDataParameter CreateStringParameter(IDbCommand command, string parameterName, string parameterValue)
        {
            IDbDataParameter parameter = command.CreateParameter();
            parameter.ParameterName = parameterName;
            parameter.DbType = DbType.String;
            parameter.Value = parameterValue;
            return parameter;
        }

        private static IDbDataParameter CreateImageKeyParameter(IDbCommand command, string key)
        {
            return CreateStringParameter(command, "ImageKey", key);
        }

        private static IDbDataParameter CreateImageExpiresParameter(IDbCommand command, TimeSpan timeout)
        {
            IDbDataParameter parameter = command.CreateParameter();
            parameter.ParameterName = "ImageExpires";
            parameter.DbType = DbType.DateTime;
            parameter.Value = DateTime.Now.Add(timeout);
            return parameter;
        }

        private static IDbDataParameter CreateImageDataParameter(IDbCommand command, byte[] data)
        {
            IDbDataParameter parameter = command.CreateParameter();
            parameter.ParameterName = "ImageData";

            SqlParameter sql = parameter as SqlParameter;

            if (sql != null)
            {
                sql.SqlDbType = SqlDbType.VarBinary;
                sql.SqlValue = new SqlBytes(data);
            }
            else
            {
                parameter.DbType = DbType.Object;
                parameter.Value = data;
            }

            return parameter;
        }

        #endregion

        #region IImageHandler Members

        bool ChartHttpHandler.IImageHandler.Exists(ChartHttpHandler.StorageSettings settings, string key)
        {
            using (Counter c = CreateMeasurement(MyInstaller.CounterIndex_Exists))
            {
                using (IDbConnection connection = CreateConnection(DatabaseName(settings)))
                {
                    using (IDbCommand command = connection.CreateCommand())
                    {
                        command.CommandText = @"SELECT CASE WHEN COUNT(*) = 0 THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END FROM Images WHERE ImageKey = @ImageKey";
                        command.CommandType = CommandType.Text;
                        command.Parameters.Add(CreateImageKeyParameter(command, key));

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                                return reader.GetBoolean(0);
                            else
                                return false;
                        }
                    }
                }
            }
        }

        void ChartHttpHandler.IImageHandler.Remove(ChartHttpHandler.StorageSettings settings, string key)
        {
            using (Counter c = CreateMeasurement(MyInstaller.CounterIndex_Remove))
            {
                using (IDbConnection connection = CreateConnection(DatabaseName(settings)))
                {
                    using (IDbCommand command = connection.CreateCommand())
                    {
                        command.CommandText = @"DELETE FROM Images WHERE ImageKey = @ImageKey";
                        command.CommandType = CommandType.Text;
                        command.Parameters.Add(CreateImageKeyParameter(command, key));
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        void ChartHttpHandler.IImageHandler.Retrieve(ChartHttpHandler.StorageSettings settings, string key, out byte[] data)
        {
            using (Counter c = CreateMeasurement(MyInstaller.CounterIndex_Retrieve))
            {
                using (IDbConnection connection = CreateConnection(DatabaseName(settings)))
                {
                    using (IDbCommand command = connection.CreateCommand())
                    {
                        command.CommandText = @"SELECT ImageData FROM Images WHERE ImageKey = @ImageKey";
                        command.CommandType = CommandType.Text;
                        command.Parameters.Add(CreateImageKeyParameter(command, key));

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                SqlDataReader special = reader as SqlDataReader;

                                if (special == null)
                                {
                                    data = new byte[0];
                                }
                                else
                                {
                                    SqlBytes bytes = special.GetSqlBytes(0);
                                    data = new byte[bytes.Length];
                                    bytes.Read(0, data, 0, data.Length);
                                }
                            }
                            else
                            {
                                data = new byte[0];
                            }
                        }
                    }
                }
            }
        }

        void ChartHttpHandler.IImageHandler.Store(ChartHttpHandler.StorageSettings settings, string key, byte[] data)
        {
            ChartHttpHandler.IImageHandler handler = this;
            if (handler.Exists(settings, key))
                handler.Remove(settings, key);

            using (Counter c = CreateMeasurement(MyInstaller.CounterIndex_Store))
            {
                using (IDbConnection connection = CreateConnection(DatabaseName(settings)))
                {
                    using (IDbCommand command = connection.CreateCommand())
                    {
                        command.CommandText = @"INSERT INTO Images (ImageKey, ImageData, ImageExpires) VALUES (@ImageKey, @ImageData, @ImageExpires)";
                        command.CommandType = CommandType.Text;
                        command.Parameters.Add(CreateImageKeyParameter(command, key));
                        command.Parameters.Add(CreateImageDataParameter(command, data));
                        command.Parameters.Add(CreateImageExpiresParameter(command, settings.Timeout));
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        #endregion
    }
}