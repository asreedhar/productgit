using System.Diagnostics;
using System.Globalization;
using MyInstaller=dundasimagehandler.ImageHanderPerformanceCounterInstaller;

namespace dundasimagehandler
{
    public abstract class baseimagehandler
    {
        #region Performance Counters

        private readonly PerformanceCounter[] ratePerSecond;
        private readonly PerformanceCounter[] avgDuration;
        private readonly PerformanceCounter[] avgDurationBase;

        protected baseimagehandler(string categoryName)
        {
            ratePerSecond = new PerformanceCounter[MyInstaller.OperationNames.Length];
            avgDuration = new PerformanceCounter[MyInstaller.OperationNames.Length];
            avgDurationBase = new PerformanceCounter[MyInstaller.OperationNames.Length];

            for (int i = 0; i < MyInstaller.OperationNames.Length; i++)
            {
                ratePerSecond[i] = new PerformanceCounter();
                ratePerSecond[i].CategoryName = categoryName;
                ratePerSecond[i].CounterName = string.Format(CultureInfo.InstalledUICulture, MyInstaller.CounterNameFormat_Rate, MyInstaller.OperationNames[i]);
                ratePerSecond[i].MachineName = ".";
                ratePerSecond[i].ReadOnly = false;

                avgDuration[i] = new PerformanceCounter();
                avgDuration[i].CategoryName = categoryName;
                avgDuration[i].CounterName = string.Format(CultureInfo.InstalledUICulture, MyInstaller.CounterNameFormat_Time, MyInstaller.OperationNames[i]);
                avgDuration[i].MachineName = ".";
                avgDuration[i].ReadOnly = false;

                avgDurationBase[i] = new PerformanceCounter();
                avgDurationBase[i].CategoryName = categoryName;
                avgDurationBase[i].CounterName = string.Format(CultureInfo.InstalledUICulture, MyInstaller.CounterNameFormat_Base, MyInstaller.OperationNames[i]);
                avgDurationBase[i].MachineName = ".";
                avgDurationBase[i].ReadOnly = false;
            }
        }

        internal Counter CreateMeasurement(int i)
        {
            return new Counter(ratePerSecond[i], avgDuration[i], avgDurationBase[i]);
        }

        #endregion
    }
}
