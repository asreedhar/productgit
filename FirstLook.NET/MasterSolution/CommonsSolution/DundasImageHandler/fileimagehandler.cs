using System;
using System.IO;
using Dundas.Charting.WebControl;
using FirstLook.Common.Impersonation;
using MyInstaller = dundasimagehandler.ImageHanderPerformanceCounterInstaller;

namespace dundasimagehandler
{
    public sealed class fileimagehandler : baseimagehandler, ChartHttpHandler.IImageHandler
    {
        public fileimagehandler() : base(MyInstaller.FileCategoryName)
        {
        }

        #region Impersonation

        private class DoNotImpersonate : IDisposable
        {
            public void Dispose()
            {
                GC.SuppressFinalize(this);
            }
        }

        private static readonly IDisposable skipImpersonation = new DoNotImpersonate();

        private static IDisposable CreateImpersonation(string active, string identifier, string password)
        {
            if (PerformImpersonation(active))
            {
                if (string.IsNullOrEmpty(identifier))
                {
                    throw new ArgumentNullException("identifier", "You must specify a user to impersonate.");
                }

                if (string.IsNullOrEmpty(password))
                {
                    throw new ArgumentNullException("password", "You must specify the users password to impersonate.");
                }

                int domainSeperatorIndex = identifier.IndexOf('\\');

                string domain, name;

                if (domainSeperatorIndex == -1)
                {
                    domain = Environment.MachineName;
                    name = identifier;
                }
                else
                {
                    domain = identifier.Substring(0, domainSeperatorIndex);
                    name = identifier.Substring(domainSeperatorIndex + 1);
                }

                return new ImpersonationCodeSection(name, domain, password);
            }
            else
            {
                return skipImpersonation;
            }
        }

        private static bool PerformImpersonation(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }
            else
            {
                return (string.Compare(value, Boolean.TrueString, StringComparison.OrdinalIgnoreCase) == 0);
            }
        }

        #endregion

        #region IImageHandler Members

        bool ChartHttpHandler.IImageHandler.Exists(ChartHttpHandler.StorageSettings settings, string key)
        {
            using (IDisposable i = CreateMeasurement(MyInstaller.CounterIndex_Exists))
            {
                using (IDisposable j = CreateImpersonation(settings.Params["Impersonate"], settings.Params["User ID"], settings.Params["Password"]))
                {
                    return File.Exists(settings.Directory + key);
                }
            }
        }

        void ChartHttpHandler.IImageHandler.Remove(ChartHttpHandler.StorageSettings settings, string key)
        {
            using (IDisposable i = CreateMeasurement(MyInstaller.CounterIndex_Exists))
            {
                using (IDisposable j = CreateImpersonation(settings.Params["Impersonate"], settings.Params["User ID"], settings.Params["Password"]))
                {
                    if (File.Exists(settings.Directory + key))
                        File.Delete(settings.Directory + key);
                }
            }
        }

        void ChartHttpHandler.IImageHandler.Retrieve(ChartHttpHandler.StorageSettings settings, string key, out byte[] data)
        {
            using (IDisposable i = CreateMeasurement(MyInstaller.CounterIndex_Exists))
            {
                using (IDisposable j = CreateImpersonation(settings.Params["Impersonate"], settings.Params["User ID"], settings.Params["Password"]))
                {
                    using (FileStream s = File.OpenRead(settings.Directory + key))
                    {
                        data = new byte[(int) s.Length];
                        s.Read(data, 0, data.Length);
                    }
                }
            }
        }

        void ChartHttpHandler.IImageHandler.Store(ChartHttpHandler.StorageSettings settings, string key, byte[] data)
        {
            using (IDisposable i = CreateMeasurement(MyInstaller.CounterIndex_Exists))
            {
                using (IDisposable j = CreateImpersonation(settings.Params["Impersonate"], settings.Params["User ID"], settings.Params["Password"]))
                {
                    string path = settings.Directory + key;

                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }   

                    using (FileStream stream = File.Create(path))
                    {
                        stream.Write(data, 0, data.Length);
                    }
                }
            }
        }

        #endregion
    }
}
