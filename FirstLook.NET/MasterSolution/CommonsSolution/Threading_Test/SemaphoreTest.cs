using System;
using System.Threading;
using NUnit.Framework;

namespace FirstLook.Common.Threading
{
    [TestFixture]
    public class SemaphoreTest
    {
        private Semaphore semaphore;

        private Latch latch;

        private int counter;

        private int result;

        [TearDown]
        private void TearDown()
        {
            semaphore = null;
            latch = null;
        }

        [Test]
        public void Semaphore_Take_NonBlock()
        {
            semaphore = new Semaphore(0);

            DateTime start = DateTime.Now;

            bool b = semaphore.Take(new TimeSpan(0,0,0,0,100));

            TimeSpan elapsed = DateTime.Now.Subtract(start);

            Assert.IsFalse(b, "Attempt");

            Assert.LessOrEqual(elapsed, new TimeSpan(TimeSpan.TicksPerSecond), "Elapsed");
        }

        [Test]
        public void Semaphore_Take_Block()
        {
            semaphore = new Semaphore(0);

            Thread t = new Thread(Release);

            t.Start(); // t waits until this thread is pre-empted or yields

            DateTime begin = DateTime.Now;

            semaphore.Take();

            DateTime wait = DateTime.Now;

            t.Join();

            DateTime end = DateTime.Now;

            Assert.LessOrEqual(begin.Subtract(wait), new TimeSpan(TimeSpan.TicksPerSecond), "Elapsed");

            Assert.LessOrEqual(wait.Subtract(end), new TimeSpan(TimeSpan.TicksPerSecond), "Join");
        }

        void Release()
        {
            Thread.Sleep(500);

            semaphore.Release();

            Thread.Sleep(500);
        }

        [Test]
        public void Semaphore_Counting()
        {
            counter = 25;

            result = counter;

            semaphore = new Semaphore(0);

            Thread[] waiting = new Thread[counter];

            for (int i = 0; i < counter; i++)
            {
                waiting[i] = new Thread(Decrement);
                waiting[i].Start();
            }

            Thread.Sleep(0); // suspend allowing other threads to work

            semaphore.Release(); // create the 1st permit

            for (int i = 0; i < counter; i++)
            {
                waiting[i].Join();
            }

            Assert.AreEqual(0, result, "Counter");
        }

        void Decrement()
        {
            semaphore.Take();

            result = result - 1; // if there is no locking this is not guaranteed

            semaphore.Release();
        }

        [Test]
        public void Semaphore_Counting_Race()
        {
            latch = new Latch();

            counter = 25;

            semaphore = new Semaphore(0);

            Thread[] waiting = new Thread[counter];

            for (int i = 0; i < counter; i++)
            {
                waiting[i] = new Thread(QuickRelease);
                waiting[i].Start();
            }

            Thread.Sleep(0); // suspend allowing other threads to work

            latch.Release();

            for (int i = 0; i < counter; i++)
            {
                waiting[i].Join();
            }

            Assert.AreEqual(counter, semaphore.AvailablePermits(), "Counter");
        }

        void QuickRelease()
        {
            latch.Acquire();

            semaphore.Release();
        }
    }
}
