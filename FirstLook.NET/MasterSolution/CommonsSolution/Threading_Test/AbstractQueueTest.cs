using System;
using System.Threading;
using NUnit.Framework;

namespace FirstLook.Common.Threading
{
    public abstract class AbstractQueueTest
    {
        protected IQueue<string> queue;

        protected const string welcome = "hello world";

        [TearDown]
        public void TearDown()
        {
            queue = null;
        }

        [Test]
        public void EmptyQueue_Size()
        {
            Assert.AreEqual(0, queue.Size(), "Size");
        }

        [Test]
        public void EmptyQueue_IsEmpty()
        {
            Assert.IsTrue(queue.IsEmpty(), "IsEmpty");
        }

        [Test]
        public void EmptyQueue_Poll_Null()
        {
            Assert.IsNull(queue.Poll(new TimeSpan(TimeSpan.TicksPerSecond)), "Poll");
        }

        [Test]
        public void EmptyQueue_Peek_Null()
        {
            Assert.IsNull(queue.Peek(), "Peek");
        }

        [Test]
        public void Offer_Size()
        {
            queue.Offer(welcome);

            Assert.AreEqual(1, queue.Size(), "Size");
        }

        [Test]
        public void Offer_IsEmpty()
        {
            queue.Offer(welcome);

            Assert.IsFalse(queue.IsEmpty(), "IsEmpty");
        }

        [Test]
        public void Offer_ReturnValue()
        {
            bool b = queue.Offer(welcome);

            Assert.IsTrue(b, "Offer ReturnValue");
        }

        [Test]
        public void Offer_Poll_Value()
        {
            queue.Offer(welcome);

            string s = queue.Poll();

            Assert.AreEqual(welcome, s, "Poll Value");
        }

        [Test]
        public void Offer_Poll_Poll_Value()
        {
            queue.Offer(welcome);

            string s1 = queue.Poll();

            string s2 = queue.Poll(new TimeSpan(TimeSpan.TicksPerSecond));

            Assert.AreEqual(welcome, s1, "Poll Value");

            Assert.IsNull(s2, "Poll Value");
        }

        [Test]
        public void Offer_Peek_Value()
        {
            queue.Offer(welcome);

            string s = queue.Peek();

            Assert.AreEqual(welcome, s, "Peek Value");
        }

        [Test]
        public void Poll_Offer_Value_Timeout()
        {
            Thread t = new Thread(delegate() { Thread.Sleep(1000); queue.Offer(welcome); });

            t.Start();

            string s = queue.Poll(new TimeSpan(0,0,0,0,500));

            t.Join();

            Assert.IsNull(s, "Poll Value");

            Assert.AreEqual(welcome, queue.Peek(), "Peek Value");
        }

        [Test]
        public void Poll_Offer_Value()
        {
            Thread t = new Thread(delegate() { Thread.Sleep(500); queue.Offer(welcome); });

            t.Start();

            string s = queue.Poll();

            t.Join();

            Assert.AreEqual(welcome, s, "Poll Offer Value");
        }
    }
}
