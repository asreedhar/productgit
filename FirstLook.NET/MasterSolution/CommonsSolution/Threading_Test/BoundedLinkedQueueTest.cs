using System;
using NUnit.Framework;

namespace FirstLook.Common.Threading
{
    [TestFixture]
    public class BoundedLinkedQueueTest : AbstractQueueTest
    {
        private const string overflow = "foobar";

        [SetUp]
        public void SetUp()
        {
            queue = new BoundedLinkedQueue<string>(1);
        }

        [Test]
        public void Offer_Offer_Timeout()
        {
            queue.Offer(welcome);

            bool b2 = queue.Offer(overflow, new TimeSpan(TimeSpan.TicksPerSecond));

            Assert.IsFalse(b2, "Offer Timeout");
        }
    }
}
