﻿using System;

namespace Core.Messaging
{
    public abstract class AbstractDelayCalculator
    {
        protected TimeSpan MinBackoff { get; private set; }
        protected TimeSpan MaxBackoff { get; private set; }

        internal AbstractDelayCalculator(TimeSpan minMessageDelay, TimeSpan maxMessageDelay)
        {
            MinBackoff = minMessageDelay;
            MaxBackoff = maxMessageDelay;
        }

        public abstract TimeSpan CalculateDelay(int errorCount);

    }
}
