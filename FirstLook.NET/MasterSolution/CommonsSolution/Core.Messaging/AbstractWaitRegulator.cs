﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Messaging
{
    public abstract class AbstractWaitRegulator
    {
        public TimeSpan Wait { get; protected set; }

        public virtual void Record(bool hit) { }
    }
}
