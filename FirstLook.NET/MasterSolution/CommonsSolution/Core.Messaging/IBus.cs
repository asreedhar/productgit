﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Messaging
{
    /// <summary>
    /// The IBus interface exposes pub-sub semantics on messages of type T.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IBus<T,Q>
    {
        string Id { get; }

        bool IsSubscribed(IQueue<Q> queue);
        void Subscribe(IQueue<Q> subscriber);
        void Unsubscribe(IQueue<Q> queue);
        string Publish(T message, string sentFrom);
        void PublishAync(T message, string sentFrom);
    }
}
