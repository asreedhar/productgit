﻿using System;
using System.Net.Mail;

namespace Core.Messaging
{
    public interface IEmail
    {
        string SendEmail(MailMessage message);
        string SendRawEmail(MailMessage message);
        bool IsHardBounce(Exception e);
    }
}
