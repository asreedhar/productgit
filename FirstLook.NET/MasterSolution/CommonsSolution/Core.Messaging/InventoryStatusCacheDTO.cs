﻿namespace Core.Messaging
{
	public class InventoryStatusCacheEntity
	{
		public bool IsActive { get; set; }
		public bool IsOffline { get; set; }
	}

	public class EmptyInventoryStatusCacheEntity : InventoryStatusCacheEntity { }

	internal static class InventoryStatusCacheExtensions
	{
		internal static bool CheckResults(this InventoryStatusCacheEntity inventoryStatusCacheEntity, bool activeOnly = true, bool offlineIsOkay = true)
		{
			if (inventoryStatusCacheEntity == null)
				return false;

			if (inventoryStatusCacheEntity is EmptyInventoryStatusCacheEntity)
				return false;

			var result = (activeOnly == inventoryStatusCacheEntity.IsActive);

			if (offlineIsOkay == false)
				result = result && inventoryStatusCacheEntity.IsOffline == false;

			return result;
		}
	} 
}