﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using Core.Messages;
using Newtonsoft.Json;

namespace Core.Messaging
{
    public abstract class TaskRunner<TValue> where TValue : IMessageBase
    {
        // Note that because we send a "new" message (so we can append errors to it) and delete
        // the existing one, the delay is precisely how long between message processing attempts.
        // Visibility has nothing to do with it, since we don't normally retry a message (we send a new one).
        // The exception to this is for "transient" sql errors, which we don't log to the message as errors.
        // We really need to store errors in an external data store and not in the message itself.
        
        // ReSharper disable once StaticFieldInGenericType
        private static readonly TimeSpan DefaultRequeueDelay = TimeSpan.FromMinutes(10);

        // ReSharper disable once StaticFieldInGenericType
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private readonly AbstractDelayCalculator _delayCalculator;

        // Specific sql exceptions to treat specially.
        private readonly List<int> _transientCustomErrors = new List<int> { 50300, 50302, 50303, 50304, 50310, 50311, 50312 };
        private readonly List<int> _connectionErrors = new List<int> { -1, 2, 53, 233, 4060, 6005, 18401, 18451, 18456, 18458, 18470, 18489 };
        private readonly List<int> _transientLockErrors = new List<int> { 1203, 1204, 1205, 1222 };
        private readonly List<int> _timeoutErrors = new List<int> { -2 };
	    // private readonly InventoryService _inventoryService;
	    //private readonly List<int> _constrantViolationErrors = new List<int> { 2601, 2627 };
        
        /// <summary>
        /// Amount of messages to request from Amazon at a time. Clamped to 1-10.
        /// </summary>
        private int _getMessageCount = 1;

        protected TaskRunner(IQueue<TValue> readQueue)
        {
            ReadQueue = readQueue;
            _delayCalculator = CreateDelayCalculator();
			// TODO: Inject this. It will require changes to all derived types so I am not doing it this late in the 20.2 release cycle.
	        // _inventoryService = new InventoryService(new CacheKeyBuilder(), new MemoryCacheWrapper(), new InventoryServiceDao());
            var getMessageCountSetting = ConfigurationManager.AppSettings["TaskRunnerGetMessageCount"];
            if (!string.IsNullOrWhiteSpace(getMessageCountSetting))
            {
                var parsed = Int32.TryParse(getMessageCountSetting, out _getMessageCount);
                if (parsed)
                {
                    _getMessageCount = Math.Min(10, _getMessageCount);
                    _getMessageCount = Math.Max(1, _getMessageCount);
                }
                else
                {
                    // TryParse sets the int to the default value ( 0 ):
                    _getMessageCount = 1;
                }
                Log.InfoFormat("TaskRunner SQS message read count set to {0}.", _getMessageCount);
            }
        }
        
        protected IQueue<TValue> ReadQueue { get; private set; }
        public abstract void Process(TValue message);

        /// <summary>
        /// Override this to use your own custom delay calculator implementing AbstractDelayCalculator
        /// </summary>
        /// <returns></returns>
        protected virtual AbstractDelayCalculator CreateDelayCalculator()
        {
            return new ConstantDelayCalculator(DefaultRequeueDelay);
        }

        protected void RequeueNoError(TValue message)
        {
            // This is the case where a message is not in error, just needs to be requeued to wait again.
            ReadQueue.SendMessage(message, DefaultRequeueDelay, GetType().FullName);
        }

        protected void RequeueWithError(TValue message, string error)
        {
            RequeueWithErrorAndDelay(message, error, DefaultRequeueDelay);
        }

        private void RequeueWithErrorAndDelay(TValue message, string error, TimeSpan requeueDelay)
        {
            message.AddError(error);

            // Set the delay according to the calculator's algorithm
            message.SetDelay(_delayCalculator.CalculateDelay(message.Errors.Length));

            ReadQueue.SendMessage(message, requeueDelay, GetType().FullName);
        }
        
        private void LogErrorAndRequeue(Exception e, IMessage<TValue> message)
        {
            Log.Error(e);
            if (message != null)
            {
                RequeueWithError(message.Object, e.Message);
                // Delete message because exception was thrown before it was deleted after the process call above
                // Call delete after requeue so if process is stopped between these calls we only delete after requeueing the message
                ReadQueue.DeleteMessage(message);
            }
        }

        /// <summary>
        /// Run against the the queue.
        /// </summary>
        /// <param name="shouldCancel">Should we break out of our run loop?</param>
        /// <param name="blockWhenNoMessages">Evaluated when no messages are availabe. 
        /// Call will block, so no need for wasteful Thread.Sleep()
        /// </param>
        public void Run(Func<bool> shouldCancel, Func<bool> blockWhenNoMessages )
        {
            string typeString = GetType().Name;

            while (!shouldCancel()) 
            {
                using (log4net.ThreadContext.Stacks["callStackId"].Push(Guid.NewGuid().ToString()))
                {

                    Log.DebugFormat("{0}.Run checking for more messages.", typeString);

                    IEnumerable<IMessage<TValue>> messages;
                    try
                    {
                        // 10 is the maximum according to amazon documentation
                        messages = ReadQueue.GetMessages(_getMessageCount);
                    }
                    catch (Exception e)
                    {
                        Log.ErrorFormat("Error raised reading message(s) from queue: {0}", e);
                        continue;
                    }

                    // messages should never be null
                    // messages will at least be an empty list
                    //
                    if (messages != null && messages.Any())
                    {
                        var start = DateTime.UtcNow;
                        
                        foreach (var message in messages)
                        {
                            try
                            {
                                ProcessMessageWithLog4NetContextWrapper(message, typeString);
                            }
                            catch (Exception e)
                            {
                                Log.Error(e);
                            }
                        }
                        var executionTime = DateTime.UtcNow - start;
                        // our default visibility timeout is 30, this isn't the most intelligent logging code.
                        // it would be better if we could know what the visibility timeout was for the queue
                        // we are processing messages on. At that point, we could tell if the execution time
                        // was too slow and messages became visible in SQS again.
                        if (executionTime.TotalMinutes > 30)
                        {
                            Log.WarnFormat("Processing tasks took more than 30 minutes to complete {0} messages. (Total time: {1} minutes)", messages.Count(), executionTime.TotalMinutes);
                        }
                    }
                    else
                    {
                        Log.DebugFormat("{0}.Run found no messages to process. Blocking.", typeString);
                        blockWhenNoMessages();
                    }
                }
            }
        }

        private void ProcessMessageWithLog4NetContextWrapper(IMessage<TValue> message, string typeString)
        {
            // Capture message properties in the context - but ignore the base properties of IMessage. 
            message.Object.SerializeMessageBaseProperties(false);
            string childMessageJson = JsonConvert.SerializeObject(message.Object);

            // All of the message's properties will need to be serialized while processing the message though.
            message.Object.SerializeMessageBaseProperties(true);

            // set this in the log4net threadcontext stack for logging
            using (log4net.ThreadContext.Stacks["context"].Push(childMessageJson))
            {
                ProcessMessage(message, typeString);
            }
        }

        private void ProcessMessage(IMessage<TValue> message, string typeString)
        {
            if (message == null)
            {
                Log.Warn("Null message detected");
                return;
            }

            try
            {
                // Pulling this from 20.2.  Needs more testing.  Need to deal with messages that have state, e.g. PriceChangeMessage
                // Either make messages stateless or guarantee that we process them in the right order, even when some may be delayed.
                // Think about embedding a sequence #, only running message if we havne't arleady run a larger sequence # of that same type.

/*              
                if (!ShouldProceed(message))
                {
                    // reschedule the message for 8 hours from now.
                    ReadQueue.DelayMessage(message.ReceiptHandle, (int) TimeSpan.FromHours(8).TotalSeconds);
                    Log.InfoFormat("Message delayed for 8 hours.");
                    return;
                }
 */
                // If this message derives from MessageBase, let's log some info about it.
                try
                {
                    var msgBase = message.Object as MessageBase;
                    if (msgBase != null)
                    {
                        var logObject = new
                        {
                            sent_utc = msgBase.Sent.ToUniversalTime().ToLongTimeString(),
                            sent_from = msgBase.SentFrom,
                            delay_total_seconds = msgBase.Delay.TotalSeconds,
                            error_count = msgBase.Errors.Count()
                        };

                        var logObjectJson = JsonConvert.SerializeObject(logObject);

                        Log.Info(logObjectJson);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }
                

                // TODO: move the code that sends it to deadletter here.
                Log.DebugFormat("{0}.Run processing message.", typeString);
                Process(message.Object);

                Log.DebugFormat("{0}.Run processed message. Deleting message.", typeString);
                ReadQueue.DeleteMessage(message);

                Log.DebugFormat("Message deleted.");
            }
            catch (SqlException e)
            {
                //if the message is connection related we don't delete the message. The databases are down.
                //The visibility timeout will requeue it.
                Log.ErrorFormat("SQL Exception, Number = {0}", e.Number);

                // Connection Errors
                if (_connectionErrors.Contains(e.Number) || _transientCustomErrors.Contains(e.Number) ||
                    _transientLockErrors.Contains(e.Number) ||
                    _timeoutErrors.Contains(e.Number))
                {
                    // Don't log these as erors - just keep retrying.
                    Log.ErrorFormat(
                        "Transient SQL exception '{0}' encountered. Message will be reprocessed when it again becomes visible.",
                        e.Number);
                }
                else
                {
                    Log.ErrorFormat("SQL exception '{0}' encountered. Logging error to message and requeueing.",
                        e.Number);
                    LogErrorAndRequeue(e, message);
                }
            }
            catch (Exception e) //catch exception caused by Processing the message and requeue them
            {
                LogErrorAndRequeue(e, message);
            }
        }

/*
        /// <summary>
        /// Check for various conditions which might cause us to not process a message right now.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
*/
/*
        private bool ShouldProceed(IMessage<TValue> message)
        {
            // Is this message only for activeOnly inventory?
	        if (message.Object is IActiveAndNotOfflineInventoryMessage)
	        {
		        var invMessage = message.Object as IActiveAndNotOfflineInventoryMessage;

		        return _inventoryService.IsActiveAndNotOffline(invMessage.InventoryId);
	        }
			if (message.Object is IActiveInventoryMessage)
            {
                var invMessage = message.Object as IActiveInventoryMessage;

                // If the inventory is active, proceed. Otherwise, don't.
                return _inventoryService.IsActive(invMessage.InventoryId);
            }

            // If it's not an IActiveInventoryMessage, proceed.
            return true;
        }
*/
	}
}
