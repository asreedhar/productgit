using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using log4net;

namespace Core.Messaging
{
	public interface IInventoryServiceDao
	{
		InventoryStatusCacheEntity InventoryStatusItemFromDb(int inventoryId, string cacheKey);
	}

	public class InventoryServiceDao : IInventoryServiceDao
	{
		#region Logging

		protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		#endregion Logging

		/// <summary></summary>
		/// <param name="inventoryId"></param>
		/// <param name="cacheKey"></param>
		/// <returns></returns>
		public InventoryStatusCacheEntity InventoryStatusItemFromDb(int inventoryId, string cacheKey)
		{
			var resultObject = new InventoryStatusCacheEntity();
			// DB Access
			Log.DebugFormat("Reading from DB for {0}, {1}", cacheKey, inventoryId);
			using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
			{
				if (connection.State == ConnectionState.Closed)
				{
					connection.Open();
				}

				using (IDbCommand command = connection.CreateCommand())
				{
					command.CommandType = CommandType.StoredProcedure;
					command.CommandText = @"dbo.[VehicleIsActiveAndNotOffline]";

					var parameter = command.CreateParameter();
					parameter.ParameterName = "InventoryId";
					parameter.DbType = DbType.Int32;
					parameter.Value = inventoryId;
					command.Parameters.Add(parameter);

					using (var reader = command.ExecuteReader())
					{
						if (!reader.Read())
						{
							resultObject.IsActive = false; // actually couldn't find it, but it's still not mustBeActive inventory right?
							resultObject.IsOffline = true;
						}
						else
						{
							resultObject.IsActive = reader.GetByte(reader.GetOrdinal("InventoryActive")) == 1;
							resultObject.IsOffline = reader.GetBoolean(reader.GetOrdinal("IsOffline"));
						}
					}
				}
			}
			Log.DebugFormat("DB Results: IsActive:{0}, IsOffline:{1}", resultObject.IsActive, resultObject.IsOffline);
			return resultObject;
		}
	}
}