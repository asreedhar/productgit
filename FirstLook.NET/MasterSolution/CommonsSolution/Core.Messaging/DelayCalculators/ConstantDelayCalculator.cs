﻿using System;

namespace Core.Messaging
{
    /// <summary>
    /// Constructor takes the constant timespan to be used for the delay.
    /// </summary>
    public class ConstantDelayCalculator : AbstractDelayCalculator
    {
        public ConstantDelayCalculator(TimeSpan defaultMessageDelay)
            : base(defaultMessageDelay, defaultMessageDelay)
        {
        }

        public override TimeSpan CalculateDelay(int errorCount)
        {
            return MinBackoff;
        }
    }
}
