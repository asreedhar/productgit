﻿using System;

namespace Core.Messaging
{
    public class ExponentialDelayCalculator : AbstractDelayCalculator
    {
        public ExponentialDelayCalculator(TimeSpan defaultMessageDelay, TimeSpan maximumDelay)
            : base(defaultMessageDelay, maximumDelay)
        {
        }

        public override TimeSpan CalculateDelay(int errorCount)
        {
            Random rand = new Random(1618033988);
            ulong increment = (ulong)((Math.Pow(2, errorCount) - 1) * rand.Next((int)(MinBackoff.TotalMilliseconds * 0.8), (int)(MinBackoff.TotalMilliseconds * 1.2)));
            ulong timeToSleepMsec = (ulong)Math.Min(MinBackoff.TotalMilliseconds + increment, MaxBackoff.TotalMilliseconds);

            return TimeSpan.FromMilliseconds(timeToSleepMsec);
        }
    }
}
