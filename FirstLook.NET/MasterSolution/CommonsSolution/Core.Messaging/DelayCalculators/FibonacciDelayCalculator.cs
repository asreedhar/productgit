﻿using System;

namespace Core.Messaging
{
    public class FibonacciDelayCalculator : AbstractDelayCalculator
    {
        public FibonacciDelayCalculator(TimeSpan defaultMessageDelay, TimeSpan maxMessageDelay) 
            : base(defaultMessageDelay, maxMessageDelay) { }

        public override TimeSpan CalculateDelay(int errorCount)
        {
            int increment = 0;
            if (errorCount > 0)
            {
                increment =(int)((FibonacciSequence((int)(MinBackoff.TotalMinutes + errorCount))) * iSqrt5);
                
            }
            int timeToSleepMsec = (int)Math.Min(MinBackoff.TotalMinutes + increment, MaxBackoff.TotalMinutes);
            return TimeSpan.FromMinutes(timeToSleepMsec);
        }

        /**
         * calculated with: 
         * static double iSqrt5 = 1 / Math.Sqrt(5);
         * static double phi = (1 + Math.Sqrt(5)) / 2;
         **/
        const double phi =    1.618033988749895;
        const double iSqrt5 = 0.447213595499958;

        static int FibonacciSequence(int n)
        {
            return (int)Math.Floor(Math.Pow(phi, n) * iSqrt5 + 0.5);
        }
    }
}
