﻿using System;

namespace Core.Messaging
{
    public class PowerOfPiDelayCalculator : AbstractDelayCalculator
    {
        public PowerOfPiDelayCalculator(TimeSpan defaultMessageDelay, TimeSpan maxDelay)
            : base(defaultMessageDelay, maxDelay) { }

        public override TimeSpan CalculateDelay(int errorCount)
        {
            Random rand = new Random(1618033988);
            ulong increment = (ulong)((Math.Pow(Math.PI, errorCount) - 1) * rand.Next((int)(MinBackoff.TotalMilliseconds * 0.8), (int)(MinBackoff.TotalMilliseconds * 1.2)));
            ulong timeToSleepMin = (ulong)Math.Min(MinBackoff.TotalMilliseconds + increment, MaxBackoff.TotalMilliseconds);

            return TimeSpan.FromMilliseconds(timeToSleepMin);
        }
    }
}
