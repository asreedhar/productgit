﻿using System;
using System.Collections.Generic;
using System.Net;

namespace Core.Messaging
{
    public interface ISimpleDb
    {
        String ContainerName();

        // Synchronous Methods
        List<Dictionary<string, string>> Select(Dictionary<String, String> whereClause);
        List<Dictionary<string, string>> Select(List<String> selectList, Dictionary<String, String> whereClause);
        List<Dictionary<string, string>> Select(String query);

        void Write(String primaryKeyValue, Dictionary<String, String> updateList);

        int BatchDelete(Dictionary<String, String> whereClause);
        HttpStatusCode Delete(String itemName);

        // Asynchronous Methods
        IAsyncResult BeginSelect(Dictionary<String, String> whereClause, AsyncCallback callback, Object asyncState);
        IAsyncResult BeginSelect(List<String> selectList, Dictionary<String, String> whereClause, AsyncCallback callback, Object asyncState);
        IAsyncResult BeginSelect(String query, AsyncCallback callback, Object asyncState);
        List<Dictionary<string, string>> EndSelect(IAsyncResult asyncResult);

        IAsyncResult BeginBatchDelete(Dictionary<String, String> whereClause, AsyncCallback callback, Object asyncState);
        HttpStatusCode EndBatchDelete(IAsyncResult asyncResult);

        IAsyncResult BeginDelete(String itemName, AsyncCallback callback, Object asyncState);
        HttpStatusCode EndDelete(IAsyncResult asyncResult);
    }
}
