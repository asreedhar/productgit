using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Reflection;
using log4net;
using MAX.Caching;

namespace Core.Messaging
{
	/// <summary>
	/// TODO: Pull this and related classes out to their own project.
	/// This class provides information about inventory items. That information is used by the messaging base class,
	/// but inventory information is not Messaging specific. When this pulled out to it's own project it can be
	/// injected into TaskRunner. That change will require updates to all decendent classes and may well require
	/// changes to all solutions that use Core.Messaging as we will be adding a dependency upon that new project.
	/// </summary>
	public class InventoryService : IInventoryService
	{
		#region Logging

		protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		#endregion Logging

		private readonly ICacheKeyBuilder _cacheKeyBuilder;
		private readonly ICacheWrapper _cacheWrapper;

		private const int InactiveInventoryCacheMinutes = 30;
		private static readonly object CacheLock = new object();
		private readonly IInventoryServiceDao _inventoryServiceDao;

		/// <summary>
		/// This implementation requires cache access implementations
		/// </summary>
		/// <param name="cacheKeyBuilder"></param>
		/// <param name="cacheWrapper"></param>
		public InventoryService(ICacheKeyBuilder cacheKeyBuilder, ICacheWrapper cacheWrapper, IInventoryServiceDao inventoryServiceDao)
		{
			_cacheKeyBuilder = cacheKeyBuilder;
			_cacheWrapper = cacheWrapper;
			_inventoryServiceDao = inventoryServiceDao;
		}

		/// <summary></summary>
		/// <param name="inventoryId"></param>
		/// <returns>bool</returns>
		public bool IsActiveAndNotOffline(int inventoryId)
		{
			return GetInventoryStatus(inventoryId).CheckResults(offlineIsOkay: false);
		}

		/// <summary></summary>
		/// <param name="inventoryId"></param>
		/// <returns>bool</returns>
		public bool IsActive(int inventoryId)
		{
			return GetInventoryStatus(inventoryId).CheckResults();
		}


		/// <summary>
		/// Gets inventory status results for this inventory id.
		/// </summary>
		/// <remarks>
		/// First we check the cache, then we query the DB. If we have to query the DB we set the results in the cache at that time.
		/// </remarks>
		/// <param name="inventoryId"></param>
		/// <returns></returns>
		private InventoryStatusCacheEntity GetInventoryStatus(int inventoryId)
		{
			var methodName = MethodBase.GetCurrentMethod().Name;

			var cacheKey = _cacheKeyBuilder.CacheKey(new[] { methodName, inventoryId.ToString(CultureInfo.InvariantCulture) });
			Log.DebugFormat("cacheKey for InventoryId {0} is {1}", inventoryId, cacheKey);

			// Check the cache first.
			var resultObject = ReadFromCache(cacheKey);

			if (!(resultObject is EmptyInventoryStatusCacheEntity))
			{
				return resultObject;
			}

			// only one cache write at a time
			// only lock when necessary
			lock (CacheLock)
			{
				try
				{
					// Check the cache again, you may have been blocked.
					resultObject = ReadFromCache(cacheKey);
					// Still nothing in the cache... carry on
					if (resultObject is EmptyInventoryStatusCacheEntity)
					{
						resultObject = _inventoryServiceDao.InventoryStatusItemFromDb(inventoryId, cacheKey);
						// Put it in the cache.
						Log.DebugFormat("Caching InventoryItem {0}, {1}", inventoryId, cacheKey);
						_cacheWrapper.Set(cacheKey, resultObject, DateTime.Now.AddMinutes(InactiveInventoryCacheMinutes));
					}
				}
				catch (Exception ex)
				{
					Log.Error(ex);
					return new EmptyInventoryStatusCacheEntity();
				}
			}

			return resultObject;
		}

		/// <summary></summary>
		/// <param name="cacheKey"></param>
		/// <returns>InventoryStatusCacheEntity</returns>
		private InventoryStatusCacheEntity ReadFromCache(string cacheKey)
		{
			try
			{
				// Check cache.
				var resultObject = _cacheWrapper.Get<InventoryStatusCacheEntity>(cacheKey);
				if (resultObject != null)
				{
					Log.Debug("cache.hit for "+ cacheKey);

					return resultObject;
				}
				Log.Debug("cache.miss for " + cacheKey);
			}
			catch (Exception ex)
			{
				Log.Error(ex);
			}
			return new EmptyInventoryStatusCacheEntity();
		}
	}
}