﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Core.Messaging
{
    public interface INoSqlDb
    {
        String ContainerName();

        // Synchronous Methods
        void Write(Dictionary<String, Object> item);
        void Write(Object item);

        DataTable Query(Dictionary<String, Object> whereClause);
        IList<T> Query<T>(Dictionary<String, Object> whereClause) where T : class, new();
        
        // Asynchronous Methods
        IAsyncResult BeginWrite(Dictionary<String, Object> item, AsyncCallback callback, Object asyncState);
        IAsyncResult BeginWrite(Object item, AsyncCallback callback, Object asyncState);
        void EndWrite(IAsyncResult asyncResult);
    }
}
