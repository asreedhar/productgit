﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Messaging
{
    internal class WaitRegulator : AbstractWaitRegulator
    {
        public static readonly TimeSpan MaxWait = TimeSpan.FromSeconds(65); //make greater than 60 seconds for amazon profiling
        public static readonly TimeSpan MinWait = TimeSpan.FromMilliseconds(100);
        public static readonly TimeSpan StepUp = TimeSpan.FromMilliseconds(500);
        public static readonly TimeSpan StepDown = TimeSpan.FromSeconds(10); //want to step down faster. If there is work there is probably more work

        public WaitRegulator()
        {
            Wait = MinWait;
        }

        private void Miss()
        {
            if (Wait + StepUp <= MaxWait)
                Wait = Wait + StepUp;
            else
                Wait = MaxWait;
        }

        private void Hit()
        {
            if (Wait - StepDown >= MinWait)
                Wait = Wait - StepDown;
            else
                Wait = MinWait;
        }

        public override void Record(bool hit)
        {
            if (hit)
                Hit();
            else
                Miss();
        }
    }
}
