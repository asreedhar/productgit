﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace Core.Messaging
{
    public interface IFileStorage
    {
        void Write(string fileName, Stream stream);
        void Write(string fileName, Stream stream, string contentType);
        Stream Read(string fileName);
        Stream Read(string fileName, DateTime? modifiedSince);
        Stream Exists(string fileName, out bool exists);
        Stream Exists(string fileName, out bool exists, DateTime? modifiedSince);
        Stream ReadIfModified(string fileName, DateTime modifiedSince, out bool exists);
        bool ExistsNoStream(string fileName);
        void Delete(string fileName);
        string ContainerName();

        #region Asynchronous methods
        IAsyncResult BeginWrite(string fileName, Stream stream, AsyncCallback callback);
        void LogEndWrite(IAsyncResult asyncResult);
        HttpStatusCode EndWrite(IAsyncResult asyncResult);
        #endregion
    }
}
