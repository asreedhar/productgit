﻿using System;
using System.Collections.Generic;
using Core.Messages;

namespace Core.Messaging
{
    public interface IQueue<TValue>
    {
        void SendMessage(TValue message, string sentFrom);
        void SendMessageAsync(TValue message, string sentFrom);
        void SendMessage(TValue message, TimeSpan deplay, string sentFrom);
        bool SendToDeadLetter(TValue message, string comments); 
        
        IMessage<TValue> GetMessage();
        IList<IMessage<TValue>> GetMessages(int count);
        void DeleteMessage(IMessage<TValue> message);

        int DeadLetterErrorLimit { get; set; }
        string QueueArn { get; }
        string Url { get; }

        void DelayMessage(string receiptHandle, int seconds);
    }
}