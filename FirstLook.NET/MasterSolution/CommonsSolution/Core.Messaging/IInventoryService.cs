namespace Core.Messaging
{
	public interface IInventoryService
	{
		/// <summary></summary>
		/// <param name="inventoryId"></param>
		/// <returns>bool</returns>
		bool IsActiveAndNotOffline(int inventoryId);

		/// <summary></summary>
		/// <param name="inventoryId"></param>
		/// <returns>bool</returns>
		bool IsActive(int inventoryId);
	}
}