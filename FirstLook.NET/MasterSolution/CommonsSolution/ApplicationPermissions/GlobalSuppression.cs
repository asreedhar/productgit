
using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "FirstLook.Common.ApplicationPermissions", Justification = "Fix: Please move into Core project")]
