using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;

namespace FirstLook.Common.ApplicationPermissions
{
    [RunInstaller(true)]
    public partial class ApplicationPermissionInstaller : Installer
    {
        public ApplicationPermissionInstaller()
        {
            InitializeComponent();
        }

        // CustomActionData of /targetDirectory="[TARGETVDIR]\"
        public override void Install(IDictionary stateSaver)
        {
            base.Install(stateSaver);

            if (Context.Parameters.Count > 0)
            {
                Program.ApplyPermissions(Context.Parameters["targetDirectory"]);
            }
        }
    }
}