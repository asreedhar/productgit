using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Security.AccessControl;
using System.Xml.Serialization;
using FirstLook.Common.ApplicationPermissions.Xml;
using Microsoft.Win32;
using File=FirstLook.Common.ApplicationPermissions.Xml.File;

namespace FirstLook.Common.ApplicationPermissions
{
    internal static class Program
    {
        public delegate void AccessRuleDelegate(string fileName, string account, List<FileSystemRights> rights, AccessControlType controlType);

        public static void ApplyPermissions(string str)
        {
            string configuration = null;
            string directory = null;

            if (System.IO.File.Exists(str))
            {
                configuration = str;
                directory = new FileInfo(configuration).DirectoryName;
            }
            else if (Directory.Exists(str))
            {
                string f = str + Path.DirectorySeparatorChar + "applicationPermissions.config";

                if (System.IO.File.Exists(f))
                {
                    configuration = f;
                    directory = str;
                }
            }

            if (string.IsNullOrEmpty(directory) || string.IsNullOrEmpty(configuration))
                throw new ArgumentException("Please specify the permissions file or a directory that contains a file 'applicationPermissions.config'. {str=" + str + ",directory=" + directory + ",configuration=" + configuration + "}");

            ApplyPermissions(directory, configuration);
        }

        public static void ApplyPermissions(string directory, string configuration)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Xml.ApplicationPermissions));

            Stream stream = System.IO.File.OpenRead(configuration);

            Xml.ApplicationPermissions permissions = (Xml.ApplicationPermissions)serializer.Deserialize(stream);

            stream.Close();

            foreach (User user in permissions.User)
            {
                if (!string.IsNullOrEmpty(user.If))
                {
                    if (user.If.Equals("IIS6") && GetDword(@"SOFTWARE\Microsoft\InetStp", "MajorVersion") != 6)
                    {
                        continue;
                    }
                }

                string account;

                if (user.Location.Equals("Machine"))
                {
                    account = Environment.MachineName + "\\" + user.Name;
                }
                else
                {
                    account = user.Location + "\\" + user.Name;
                }

                if (user.Add != null)
                {
                    foreach (Permission add in user.Add)
                    {
                        if (add.DirList != null && add.DirList.Length > 0)
                        {
                            InvokeDirSecurity(account, add.Permissions, directory, add.DirList, AddDirectorySecurity);
                        }

                        if (add.FileList != null && add.FileList.Length > 0)
                        {
                            InvokeFileSecurity(account, add.Permissions, directory, add.FileList,
                                               AddDirectorySecurity,
                                               AddFileSecurity);
                        }
                    }
                }

                if (user.Remove != null)
                {
                    foreach (Permission remove in user.Remove)
                    {
                        if (remove.DirList != null && remove.DirList.Length > 0)
                        {
                            InvokeDirSecurity(account, remove.Permissions, directory, remove.DirList, RemoveDirectorySecurity);
                        }

                        if (remove.FileList != null && remove.FileList.Length > 0)
                        {
                            InvokeFileSecurity(account, remove.Permissions, directory, remove.FileList,
                                               RemoveDirectorySecurity,
                                               RemoveFileSecurity);
                        }
                    }
                }
            }
        }

        public static List<FileSystemRights> ParseFileSystemRights(string str)
        {
            List<FileSystemRights> rights = new List<FileSystemRights>();

            foreach (string right in str.Split(new char[] { ',' }))
            {
                rights.Add((FileSystemRights)Enum.Parse(typeof(FileSystemRights), right, true));
            }

            return rights;
        }

        public static void InvokeDirSecurity(string account, string fileSystemRightsText, string directory, File[] subdirectories, AccessRuleDelegate changeDirRights)
        {
            List<FileSystemRights> fileSystemRights = ParseFileSystemRights(fileSystemRightsText);

            foreach (File subdirectory in subdirectories)
            {
                string name = directory + subdirectory.Name;

                DirectoryInfo info = new DirectoryInfo(name);

                if (!info.Exists)
                {
                    info.Create();

                    Debug(string.Format(CultureInfo.CurrentCulture, "Created Directory {0}", name));
                }

                changeDirRights(name, account, fileSystemRights, AccessControlType.Allow);
            }
        }

        public static void InvokeFileSecurity(string account, string fileSystemRightsText, string directory, FileList[] fileLists, AccessRuleDelegate changeDirRights, AccessRuleDelegate changeFileRights)
        {
            List<FileSystemRights> fileSystemRights = ParseFileSystemRights(fileSystemRightsText);

            changeDirRights(directory, account, fileSystemRights, AccessControlType.Allow);

            foreach (FileList fileList in fileLists)
            {
                if (fileList.Dir.IndexOf(Path.DirectorySeparatorChar) > 0)
                {
                    string dirName = directory;

                    foreach (string subdirectory in fileList.Dir.Split(new char[] { Path.DirectorySeparatorChar }))
                    {
                        dirName += Path.DirectorySeparatorChar + subdirectory;

                        Debug(string.Format(CultureInfo.CurrentCulture, "Apply Rights To {0}", dirName));

                        changeDirRights(dirName, account, fileSystemRights, AccessControlType.Allow);
                    }
                }

                string dir = directory + Path.DirectorySeparatorChar + fileList.Dir + Path.DirectorySeparatorChar;

                Debug(string.Format(CultureInfo.CurrentCulture, "Apply Rights To {0}", dir));

                changeDirRights(directory, account, fileSystemRights, AccessControlType.Allow);

                foreach (File file in fileList.File)
                {
                    string name = dir + file.Name;

                    Debug(string.Format(CultureInfo.CurrentCulture, "Apply Rights To {0}", name));

                    changeFileRights(name, account, fileSystemRights, AccessControlType.Allow);
                }
            }
        }

        public static void AddFileSecurity(string fileName, string account, List<FileSystemRights> rights, AccessControlType controlType)
        {
            FileSecurity fSecurity = System.IO.File.GetAccessControl(fileName);

            foreach (FileSystemRights right in rights)
            {
                fSecurity.AddAccessRule(new FileSystemAccessRule(account, right, controlType));
            }

            System.IO.File.SetAccessControl(fileName, fSecurity);
        }

        public static void RemoveFileSecurity(string fileName, string account, List<FileSystemRights> rights, AccessControlType controlType)
        {
            FileSecurity fSecurity = System.IO.File.GetAccessControl(fileName);

            foreach (FileSystemRights right in rights)
            {
                fSecurity.RemoveAccessRule(new FileSystemAccessRule(account, right, controlType));
            }

            System.IO.File.SetAccessControl(fileName, fSecurity);
        }

        public static void AddDirectorySecurity(string dirName, string account, List<FileSystemRights> rights, AccessControlType controlType)
        {
            DirectorySecurity dSecurity = Directory.GetAccessControl(dirName);

            foreach (FileSystemRights right in rights)
            {
                dSecurity.AddAccessRule(new FileSystemAccessRule(account, 
                    right, 
                    InheritanceFlags.ObjectInherit, 
                    PropagationFlags.InheritOnly, 
                    controlType));

                Debug(string.Format(CultureInfo.CurrentCulture, "Added Permission {0} to {1} for {2}", right, dirName, account));
            }

            Directory.SetAccessControl(dirName, dSecurity);
        }

        public static void RemoveDirectorySecurity(string dirName, string account, List<FileSystemRights> rights, AccessControlType controlType)
        {
            DirectorySecurity dSecurity = Directory.GetAccessControl(dirName);

            foreach (FileSystemRights right in rights)
            {
                dSecurity.RemoveAccessRule(new FileSystemAccessRule(account, right, controlType));
            }

            Directory.SetAccessControl(dirName, dSecurity);
        }

        public static void Debug(string message)
        {
            string sourceName = "CommandCenterInstaller";
            string logName = "Application";

            if (!EventLog.SourceExists(sourceName))
            {
                EventLog.CreateEventSource(sourceName, logName);
            }

            EventLog log = new EventLog();
            log.Source = sourceName;
            log.WriteEntry(message, EventLogEntryType.Information);
        }

        public static Nullable<uint> GetDword(string subKeyName, string valueName)
        {
            Nullable<UInt32> dword = null;

            using (RegistryKey key = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, Environment.MachineName))
            {
                if (key != null)
                {
                    using (RegistryKey subKey = key.OpenSubKey(subKeyName))
                    {
                        if (subKey != null)
                        {
                            object value = subKey.GetValue(valueName);

                            if (value != null)
                            {
                                dword = UInt32.Parse(value.ToString(), CultureInfo.InvariantCulture);
                            }
                        }
                    }
                }
            }

            return dword;
        }
    }
}

