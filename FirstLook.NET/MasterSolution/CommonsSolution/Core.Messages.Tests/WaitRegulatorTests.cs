﻿using Core.Messaging;
using NUnit.Framework;

namespace Core.Messages.Tests
{
    [TestFixture]
    public class WaitRegulatorTests
    {
        [Test]
        public void TestIncrement()
        {
            var regulator = new WaitRegulator();
            regulator.Record(false);

            Assert.IsTrue(regulator.Wait == WaitRegulator.MinWait + WaitRegulator.StepUp);
        }

        [Test]
        public void TestRunOverIncrement()
        {
            var regulator = new WaitRegulator();
            int successCount = 1000;
            for (int x = 0; x < successCount; x++)
                regulator.Record(false);

            Assert.IsTrue(regulator.Wait == WaitRegulator.MaxWait);
        }

        [Test]
        public void TestDecrement()
        {
            var regulator = new WaitRegulator();
            int successCount = 1000;
            for (int x = 0; x < successCount; x++)
                regulator.Record(false);

            Assert.IsTrue(regulator.Wait == WaitRegulator.MaxWait);

            regulator.Record(true);
            Assert.IsTrue(regulator.Wait == WaitRegulator.MaxWait - WaitRegulator.StepDown);

        }

        [Test]
        public void TestRunUnderDecrement()
        {
            var regulator = new WaitRegulator();
            regulator.Record(false);

            Assert.IsTrue(regulator.Wait == WaitRegulator.MinWait + WaitRegulator.StepUp);

            int successCount = 1000;
            for (int x = 0; x < successCount; x++)
                regulator.Record(true);

            Assert.IsTrue(regulator.Wait == WaitRegulator.MinWait);
        }


    }
}
