﻿using FirstLook.Common.Core.Extensions;
using NUnit.Framework;

namespace Core.Messages.Tests
{
    [TestFixture]
    public class MessageSerializationTests
    {
        [Test]
        public void RoundTripSubsetDifferentTypes()
        {
            var superSet = new SuperSet(1, 2, 100, 101);
            string json = superSet.ToJson();

            var subset = json.FromJson<SubSet>();

            Assert.IsTrue(subset.GetType() == typeof(SubSet));
            Assert.IsTrue(superSet.BusinessUnitId == subset.BusinessUnitId);
            Assert.IsTrue(superSet.InventoryId == subset.InventoryId);
        }

       

        private class SuperSet
        {
            public SuperSet(int oldPrice, int price, int businessUnitId, int inventoryId)
            {
                OldPrice = oldPrice;
                Price = price;
                BusinessUnitId = businessUnitId;
                InventoryId = inventoryId;
            }

            public int BusinessUnitId { get; set; }
            public int InventoryId { get; set; }
            public int OldPrice { get; set; }
            public int Price { get; set; }
        }

        private class SubSet
        {
            public SubSet(int businessUnitId, int inventoryId, string userName)
            {
                BusinessUnitId = businessUnitId;
                InventoryId = inventoryId;
            }

            public string UserName { get; set; }
            public int BusinessUnitId { get; set; }
            public int InventoryId { get; set; }
        }


    }
}
