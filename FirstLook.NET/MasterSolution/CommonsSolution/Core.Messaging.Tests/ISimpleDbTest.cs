﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Merchandising.Messages;
using NUnit.Framework;

namespace Core.Messaging.Tests
{
    /// <summary>
    ///This is a test class for ISimpleDb and is intended
    ///to contain all ISimpleDb Unit Tests
    ///</summary>
    [TestFixture]
    public class ISimpleDbTest
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        internal TestContext TestContext
        {
            get
            {
                return _testContextInstance;
            }
            set
            {
                _testContextInstance = value;
            }
        }
        private TestContext _testContextInstance;

        internal ISimpleDbFactory SimpleDbFactory
        {
            get { return _simpleDbFactory ?? (_simpleDbFactory = new SimpleDbFactory()); }
        }
        private ISimpleDbFactory _simpleDbFactory;

        /// <summary>
        /// ItemName is the "primary key" of any AWS SimpleDB table
        /// </summary>
        internal String ItemName
        {
            get { return "DBDBD9F4-49B7-4164-A77F-6DCCC93BF0FF"; }
        }

        #region Setup/Teardown
        //Use TestFixtureSetUp to run code before running the first test in the class
        //[TestFixtureSetUp]
        //public void MyTestFixtureSetUp()
        //{
        //}

        ////Use TestFixtureTearDown to run code after all tests in a class have run
        //[TestFixtureTearDown]
        //public void MyTestFixtureTearDown()
        //{
        //}

        ////Use SetUp to run code before running each test
        //[SetUp]
        //public void MySetUp()
        //{
        //}

        ////Use TearDown to run code after each test has run
        //[TearDown]
        //public void MyTearDown()
        //{
        //}

        internal Dictionary<String, String> DealerRaterRecord()
        {
            var record = new Dictionary<String, String>();

            record["dealer_rater_id"] = "Test12345";

            return record;
        }
        #endregion

        #region Dealer Rater DB tests
        [Test]
        public void DealerRater_ContainerNameTest()
        {
            const string expected = "dealer_rater_id_map";

            var target = SimpleDbFactory.GetDealerRaterDb();
            var actual = target.ContainerName();

            Assert.IsTrue(actual.StartsWith(expected)); // ignore the suffix
        }

        [Test]
        public void DealerRater_SelectAllByWhereClauseTest()
        {
            var target = SimpleDbFactory.GetDealerRaterDb();
            var whereClause = new Dictionary<String, String>();

            whereClause["itemName()"] = ItemName;

            var results = target.Select(whereClause);
        }

        [Test]
        public void DealerRater_SelectFieldByWhereClauseTest()
        {
            var target = SimpleDbFactory.GetDealerRaterDb();
            var selectList = new List<String>();
            var whereClause = new Dictionary<String, String>();

            selectList.Add("dealer_rater_id");
            whereClause["itemName()"] = ItemName;

            var results = target.Select(selectList, whereClause);
        }

        [Test]
        public void DealerRater_WriteTest()
        {
            var target = SimpleDbFactory.GetDealerRaterDb();
            var record = DealerRaterRecord();

            target.Write(ItemName, record);
        }

        [Test]
        [Ignore] // The AWS AccessKey/SecretAccessKey does not have rights to delete records in the dealer_rater_id_map table
        public void DealerRater_DeleteTest()
        {
            var target = SimpleDbFactory.GetDealerRaterDb();

            target.Delete(ItemName);
        }

        [Test]
        public void DealerRater_WriteReadTest()
        {
            const String dealerRaterId = "123456";
            var target = SimpleDbFactory.GetDealerRaterDb();
            var record = DealerRaterRecord();
            record["dealer_rater_id"] = dealerRaterId;

            target.Write(ItemName, record);
            Thread.Sleep(3000); // wait a 3 seconds for it to propogate in AWS

            var whereClause = new Dictionary<String, String>();
            whereClause["itemName()"] = ItemName;
            var results = target.Select(whereClause);

            Assert.AreEqual(1, results.Count);
            Assert.AreEqual(results[0]["itemName"], ItemName);
            Assert.AreEqual(results[0]["dealer_rater_id"], dealerRaterId);
        }
        #endregion

        #region Google Analytics Report DB tests
        [Test]
        public void GoogleAnalytics_ContainerNameTest()
        {
            const string expected = "ga_reports_ds_report_data";

            var target = SimpleDbFactory.GetGoogleAnalyticsReportDb();
            var actual = target.ContainerName();

            Assert.IsTrue(actual.StartsWith(expected)); // ignore the suffix
        }

        [Test]
        public void GoogleAnalytics_ReadTest()
        {
            var target = SimpleDbFactory.GetGoogleAnalyticsReportDb();
            var results = target.Select(new Dictionary<string, string> {{"config_business_unit_id", "105239"}});

            Assert.Greater(results.Count, 0);
        }
        #endregion
    }
}
