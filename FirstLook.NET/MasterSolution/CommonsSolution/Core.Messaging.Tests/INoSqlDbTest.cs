﻿using System;
using System.Collections.Generic;
using Merchandising.Messages;
using NUnit.Framework;

namespace Core.Messaging.Tests
{
    /// <summary>
    ///This is a test class for INoSqlDbTest and is intended
    ///to contain all INoSqlDbTest Unit Tests
    ///</summary>
    [TestFixture]
    public class INoSqlDbTest
    {
        private TestContext _testContextInstance;
        private INoSqlDbFactory _noSqlDbFactory;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return _testContextInstance;
            }
            set
            {
                _testContextInstance = value;
            }
        }

        #region Setup/Teardown
        //Use TestFixtureSetUp to run code before running the first test in the class
        [TestFixtureSetUp]
        public void MyTestFixtureSetUp()
        {
            _noSqlDbFactory = new NoSqlDbFactory();
        }

        ////Use TestFixtureTearDown to run code after all tests in a class have run
        //[TestFixtureTearDown]
        //public void MyTestFixtureTearDown()
        //{
        //}

        ////Use SetUp to run code before running each test
        //[SetUp]
        //public void MySetUp()
        //{
        //}

        ////Use TearDown to run code after each test has run
        //[TearDown]
        //public void MyTearDown()
        //{
        //}
        #endregion


        internal virtual INoSqlDb CreateINoSqlDb()
        {
            return _noSqlDbFactory.GetBucketValueDb();
        }

        ///// <summary>
        /////A test for BeginWrite
        /////</summary>
        //[Test]
        //public void BeginWriteTest()
        //{
        //    var target = CreateINoSqlDb();
        //    Dictionary<string, object> item = null; // TODO: Initialize to an appropriate value
        //    AsyncCallback callback = null; // TODO: Initialize to an appropriate value
        //    object asyncState = null; // TODO: Initialize to an appropriate value
        //    IAsyncResult expected = null; // TODO: Initialize to an appropriate value
        //    IAsyncResult actual;
        //    actual = target.BeginWrite(item, callback, asyncState);
        //    Assert.AreEqual(expected, actual);
        //    Assert.Inconclusive("Verify the correctness of this test method.");
        //}

        ///// <summary>
        /////A test for BeginWrite
        /////</summary>
        //[Test]
        //public void BeginWriteTest1()
        //{
        //    INoSqlDb target = CreateINoSqlDb(); // TODO: Initialize to an appropriate value
        //    object item = null; // TODO: Initialize to an appropriate value
        //    AsyncCallback callback = null; // TODO: Initialize to an appropriate value
        //    object asyncState = null; // TODO: Initialize to an appropriate value
        //    IAsyncResult expected = null; // TODO: Initialize to an appropriate value
        //    IAsyncResult actual;
        //    actual = target.BeginWrite(item, callback, asyncState);
        //    Assert.AreEqual(expected, actual);
        //    Assert.Inconclusive("Verify the correctness of this test method.");
        //}

        /// <summary>
        ///A test for ContainerName
        ///</summary>
        [Test]
        public void ContainerNameTest()
        {
            const string expected = "max-merchandising-daily-bucket-values";

            var target = CreateINoSqlDb();
            var actual = target.ContainerName();

            Assert.IsTrue(actual.StartsWith(expected));
        }

        ///// <summary>
        /////A test for EndWrite
        /////</summary>
        //[Test]
        //public void EndWriteTest()
        //{
        //    INoSqlDb target = CreateINoSqlDb(); // TODO: Initialize to an appropriate value
        //    IAsyncResult asyncResult = null; // TODO: Initialize to an appropriate value
        //    target.EndWrite(asyncResult);
        //    Assert.Inconclusive("A method that does not return a value cannot be verified.");
        //}

        ///// <summary>
        /////A test for Write
        /////</summary>
        //[Test]
        //public void WriteTest()
        //{
        //    INoSqlDb target = CreateINoSqlDb(); // TODO: Initialize to an appropriate value
        //    object item = null; // TODO: Initialize to an appropriate value
        //    target.Write(item);
        //    Assert.Inconclusive("A method that does not return a value cannot be verified.");
        //}

        ///// <summary>
        /////A test for Write
        /////</summary>
        //[Test]
        //public void WriteTest1()
        //{
        //    INoSqlDb target = CreateINoSqlDb(); // TODO: Initialize to an appropriate value
        //    Dictionary<string, object> item = null; // TODO: Initialize to an appropriate value
        //    target.Write(item);
        //    Assert.Inconclusive("A method that does not return a value cannot be verified.");
        //}
    }
}
