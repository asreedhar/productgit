﻿using System.Collections.Generic;
using MAX.Caching;
using Moq;
using NUnit.Framework;


namespace Core.Messaging.Tests
{
	[TestFixture]
	public class InventoryServiceTests
	{
		private Mock<ICacheKeyBuilder> _cacheKeyBuilder;
		private Mock<ICacheWrapper> _cacheWrapper;
		private Mock<IInventoryServiceDao> _inventoryServiceDao;

		[SetUp]
		public void SetUpTests()
		{
			_cacheWrapper = new Mock<ICacheWrapper>();
			_cacheKeyBuilder = new Mock<ICacheKeyBuilder>();

			_cacheKeyBuilder.Setup(ckb => ckb.CacheKey(It.IsAny<IEnumerable<string>>())).Returns(() => "testcachekey");
			_cacheWrapper.Setup(cw => cw.Get(It.IsAny<string>())).Returns(() => new EmptyInventoryStatusCacheEntity());
			_cacheWrapper.Setup(cw => cw.Set(It.IsAny<string>(), It.IsAny<object>())).Returns(true);

			_inventoryServiceDao = new Mock<IInventoryServiceDao>();
		}

		[Test]
		public void IsActive_Returns_True_For_ActiveInventory()
		{
			_inventoryServiceDao.Setup(isd => isd.InventoryStatusItemFromDb(It.IsAny<int>(), It.IsAny<string>()))
				.Returns(new InventoryStatusCacheEntity { IsActive = true });

			var inventoryService = new InventoryService(_cacheKeyBuilder.Object, _cacheWrapper.Object, _inventoryServiceDao.Object);
			Assert.True(inventoryService.IsActive(1), "Is Active worked");
		}

		[Test]
		public void IsActive_Returns_False_For_InactiveInventory()
		{
			_inventoryServiceDao.Setup(isd => isd.InventoryStatusItemFromDb(It.IsAny<int>(), It.IsAny<string>()))
				.Returns(new InventoryStatusCacheEntity { IsActive = false });

			var inventoryService = new InventoryService(_cacheKeyBuilder.Object, _cacheWrapper.Object, _inventoryServiceDao.Object);
			Assert.False(inventoryService.IsActive(1), "Is Inactive worked");
		}

		[Test]
		public void IsActiveAndNotOffline_Returns_True_For_ActiveOnlineInventory()
		{
			_inventoryServiceDao.Setup(isd => isd.InventoryStatusItemFromDb(It.IsAny<int>(), It.IsAny<string>()))
				.Returns(new InventoryStatusCacheEntity { IsActive = true, IsOffline = false });

			var inventoryService = new InventoryService(_cacheKeyBuilder.Object, _cacheWrapper.Object, _inventoryServiceDao.Object);
			Assert.True(inventoryService.IsActiveAndNotOffline(1), "Is Active And Not Offline worked");
		}

		[Test]
		public void IsActiveAndNotOffline_Returns_False_For_ActiveOfflineInventory()
		{
			_inventoryServiceDao.Setup(isd => isd.InventoryStatusItemFromDb(It.IsAny<int>(), It.IsAny<string>()))
				.Returns(new InventoryStatusCacheEntity { IsActive = true, IsOffline = true });

			var inventoryService = new InventoryService(_cacheKeyBuilder.Object, _cacheWrapper.Object, _inventoryServiceDao.Object);
			Assert.False(inventoryService.IsActiveAndNotOffline(1), "Is Active And Not Offline worked");
		}

		[Test]
		public void IsActiveAndNotOffline_Returns_False_For_InactiveOfflineInventory()
		{
			_inventoryServiceDao.Setup(isd => isd.InventoryStatusItemFromDb(It.IsAny<int>(), It.IsAny<string>()))
				.Returns(new InventoryStatusCacheEntity { IsActive = false, IsOffline = true });

			var inventoryService = new InventoryService(_cacheKeyBuilder.Object, _cacheWrapper.Object, _inventoryServiceDao.Object);
			Assert.False(inventoryService.IsActiveAndNotOffline(1), "Is Active And Not Offline worked");
		}

		[Test]
		public void IsActiveAndNotOffline_Returns_False_For_InactiveNotOfflineInventory()
		{
			_inventoryServiceDao.Setup(isd => isd.InventoryStatusItemFromDb(It.IsAny<int>(), It.IsAny<string>()))
				.Returns(new InventoryStatusCacheEntity { IsActive = false, IsOffline = false });

			var inventoryService = new InventoryService(_cacheKeyBuilder.Object, _cacheWrapper.Object, _inventoryServiceDao.Object);
			Assert.False(inventoryService.IsActiveAndNotOffline(1), "Is Active And Not Offline worked");
		}
	}
}