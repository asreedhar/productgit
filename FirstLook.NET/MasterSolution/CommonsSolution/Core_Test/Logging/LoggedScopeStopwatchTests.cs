﻿using Moq;
using NUnit.Framework;

namespace FirstLook.Common.Core.Logging
{
    // ReSharper disable ObjectCreationAsStatement

    [TestFixture]
    public class LoggedScopeStopwatchTests
    {
        private Mock<ILog> _logMock;
        private ILog _log;

        [SetUp]
        public void Setup()
        {
            _logMock = new Mock<ILog>();
            _log = _logMock.Object;
        }

        [Test]
        public void Constructor_should_log_scope_start()
        {
            _logMock.Setup(l => l.IsInfoEnabled).Returns(true);


            new LoggedScopeStopwatch(_log, LogLevel.Info, log => log.Info("Foo"),
                                                 (log, t) => log.Info("Bar"));

            _logMock.Verify(l => l.IsInfoEnabled, Times.Once());
            _logMock.Verify(l => l.Info("Foo"), Times.Once());
            _logMock.Verify(l => l.Info("Bar"), Times.Never());
        }

        [Test]
        public void Dispose_should_log_scope_stop()
        {
            _logMock.Setup(l => l.IsInfoEnabled).Returns(true);

            using(new LoggedScopeStopwatch(_log, LogLevel.Info, log => log.Info("Foo"),
                                                 (log, t) => log.Info("Bar")))
            {
            }

            _logMock.Verify(l => l.IsInfoEnabled, Times.Exactly(2));
            _logMock.Verify(l => l.Info("Foo"), Times.Once());
            _logMock.Verify(l => l.Info("Bar"), Times.Once());
        }

        [Test]
        public void Dispose_twice_should_only_log_scope_stop_once()
        {
            _logMock.Setup(l => l.IsInfoEnabled).Returns(true);

            var timer = new LoggedScopeStopwatch(_log, LogLevel.Info, log => log.Info("Foo"),
                                                 (log, t) => log.Info("Bar"));

            timer.Dispose();
            timer.Dispose();

            _logMock.Verify(l => l.IsInfoEnabled, Times.Exactly(2));
            _logMock.Verify(l => l.Info("Foo"), Times.Once());
            _logMock.Verify(l => l.Info("Bar"), Times.Once());
        }

        [Test]
        public void Nothing_should_be_logged_if_log_leve_not_enabled()
        {
            _logMock.Setup(l => l.IsInfoEnabled).Returns(false);

            using(new LoggedScopeStopwatch(_log, LogLevel.Info, log => log.Info("Foo"),
                                                 (log, t) => log.Info("Bar")))
            {
            }

            _logMock.Verify(l => l.IsInfoEnabled, Times.Exactly(2));
            _logMock.Verify(l => l.Info("Foo"), Times.Never());
            _logMock.Verify(l => l.Info("Bar"), Times.Never());
        }
    }
}