using Autofac;
using Moq;
using NUnit.Framework;

namespace FirstLook.Common.Core.Logging
{
    [TestFixture]
    public class LoggerFactory_Tests
    {
        private Mock<ILoggerFactory> factory;
        private Mock<ILog> logA;

        [SetUp]
        public void Setup()
        {
            IOC.Registry.Reset();

            factory = new Mock<ILoggerFactory>();
            logA = new Mock<ILog>();

            var builder = new ContainerBuilder();
            builder.RegisterInstance(factory.Object).As<ILoggerFactory>();

            IOC.Registry.RegisterContainer(builder.Build());
        }

        [TearDown]
        public void TearDown()
        {
            IOC.Registry.Reset();
        }

        [Test]
        public void GetLogger_T_should_call_ILoggerFactory_GetLogger_T()
        {
            LoggerFactory.GetLogger<string>();

            factory.Verify(f => f.GetLogger<string>(), Times.Once());
        }

        [Test]
        public void GetLogger_T_should_return_inners_return_value()
        {
            factory.Setup(f => f.GetLogger<int>()).Returns(logA.Object);

            var log = LoggerFactory.GetLogger<int>();

            Assert.That(log, Is.SameAs(logA.Object));
        }

        [Test]
        public void GetLogger_with_type_should_call_ILoggerFactory_GetLogger_with_type()
        {
            var type = typeof (LoggerFactory_Tests);
            
            LoggerFactory.GetLogger(type);

            factory.Verify(f => f.GetLogger(type), Times.Once());
        }

        [Test]
        public void GetLogger_with_type_should_return_inners_return_value()
        {
            var type = typeof (double);
            factory.Setup(f => f.GetLogger(type)).Returns(logA.Object);

            var log = LoggerFactory.GetLogger(type);

            Assert.That(log, Is.SameAs(logA.Object));
        }

        [Test]
        public void GetLogger_with_string_should_call_ILoggerFactory_GetLogger_with_same_string()
        {
            const string someString = "SomeValue";
            LoggerFactory.GetLogger(someString);

            factory.Verify(f => f.GetLogger(someString), Times.Once());
        }

        [Test]
        public void GetLogger_with_string_should_return_inners_return_value()
        {
            const string anotherString = "AnotherValue";
            factory.Setup(f => f.GetLogger(anotherString)).Returns(logA.Object);
            
            var log = LoggerFactory.GetLogger(anotherString);

            Assert.That(log, Is.SameAs(logA.Object));
        }

        [Test]
        public void GetLogger_with_null_container_and_T_should_return_ILog()
        {
            IOC.Registry.Reset();

            var log = LoggerFactory.GetLogger<double>();

            Assert.That(log, Is.Not.Null);
        }

        [Test]
        public void GetLogger_with_null_container_and_T_should_return_valid_ILog()
        {
            IOC.Registry.Reset();

            var log = LoggerFactory.GetLogger<double>();

            log.Error("Something");
        }

        [Test]
        public void GetLogger_with_null_container_and_type_should_return_ILog()
        {
            var type = typeof (double);

            IOC.Registry.Reset();

            var log = LoggerFactory.GetLogger(type);

            Assert.That(log, Is.Not.Null);
        }

        [Test]
        public void GetLogger_with_null_container_and_type_should_return_valid_ILog()
        {
            var type = typeof(double);

            IOC.Registry.Reset();

            var log = LoggerFactory.GetLogger(type);

            log.Error("Something");
        }

        [Test]
        public void GetLogger_with_null_container_and_string_should_return_ILog()
        {
            const string str = "Logger";

            IOC.Registry.Reset();

            var log = LoggerFactory.GetLogger(str);

            Assert.That(log, Is.Not.Null);
        }

        [Test]
        public void GetLogger_with_null_container_and_string_should_return_valid_ILog()
        {
            const string str = "Logger";

            IOC.Registry.Reset();

            var log = LoggerFactory.GetLogger(str);

            log.Error("Something");
        }
    }
}