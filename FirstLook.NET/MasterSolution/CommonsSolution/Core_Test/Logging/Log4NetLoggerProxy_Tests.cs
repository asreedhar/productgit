using System;
using System.Globalization;
using Moq;
using NUnit.Framework;

namespace FirstLook.Common.Core.Logging
{
    [TestFixture]
    public class Log4NetLoggerProxy_Tests
    {
        private Mock<log4net.ILog> inner;

        [SetUp]
        public void Setup()
        {
            inner = new Mock<log4net.ILog>();
        }

        [Test]
        public void Constructor_with_null_inner_should_throw()
        {
            Assert.Throws<ArgumentNullException>(
                () => new Log4NetLoggerProxy(null));
        }

        [TestCase(true)]
        [TestCase(false)]
        public void IsDebugEnabled_passes_through_inner_values(bool val)
        {
            inner.Setup(i => i.IsDebugEnabled).Returns(val);

            var proxy = new Log4NetLoggerProxy(inner.Object);

            Assert.That(proxy.IsDebugEnabled, Is.EqualTo(val));
        }

        [TestCase(true)]
        [TestCase(false)]
        public void IsInfoEnabled_passes_through_inner_values(bool val)
        {
            inner.Setup(i => i.IsInfoEnabled).Returns(val);

            var proxy = new Log4NetLoggerProxy(inner.Object);

            Assert.That(proxy.IsInfoEnabled, Is.EqualTo(val));
        }

        [TestCase(true)]
        [TestCase(false)]
        public void IsWarnEnabled_passes_through_inner_values(bool val)
        {
            inner.Setup(i => i.IsWarnEnabled).Returns(val);

            var proxy = new Log4NetLoggerProxy(inner.Object);

            Assert.That(proxy.IsWarnEnabled, Is.EqualTo(val));
        }

        [TestCase(true)]
        [TestCase(false)]
        public void IsErrorEnabled_passes_through_inner_values(bool val)
        {
            inner.Setup(i => i.IsErrorEnabled).Returns(val);

            var proxy = new Log4NetLoggerProxy(inner.Object);

            Assert.That(proxy.IsErrorEnabled, Is.EqualTo(val));
        }

        [TestCase(true)]
        [TestCase(false)]
        public void IsFatalEnabled_passes_through_inner_values(bool val)
        {
            inner.Setup(i => i.IsFatalEnabled).Returns(val);

            var proxy = new Log4NetLoggerProxy(inner.Object);

            Assert.That(proxy.IsFatalEnabled, Is.EqualTo(val));
        }

        [Test]
        public void Debug_should_call_inner()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);

            proxy.Debug("Foo");

            inner.Verify(i => i.Debug("Foo"), Times.Once());
        }

        
        [Test]
        public void Debug_should_call_inner_2()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);
            var exception = new Exception();
            
            proxy.Debug("Foo", exception);

            inner.Verify(i => i.Debug("Foo", exception), Times.Once());
        }

        [Test]
        public void DebugFormat_should_call_inner()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);
            var array = new[] {1, 2, 3};

            proxy.DebugFormat("{0}", array);

            inner.Verify(i => i.DebugFormat("{0}", array), Times.Once());
        }

        [Test]
        public void DebugFormat_should_call_inner_1()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);

            proxy.DebugFormat("{0}", 1);

            inner.Verify(i => i.DebugFormat("{0}", 1), Times.Once());
        }

        [Test]
        public void DebugFormat_should_call_inner_2()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);

            proxy.DebugFormat("{0} {1}", 1, 2);

            inner.Verify(i => i.DebugFormat("{0} {1}", 1, 2), Times.Once());
        }

        [Test]
        public void DebugFormat_should_call_inner_3()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);

            proxy.DebugFormat("{0} {1} {2}", 1, 2, 3);

            inner.Verify(i => i.DebugFormat("{0} {1} {2}", 1, 2, 3), Times.Once());
        }

        [Test]
        public void DebugFormat_should_call_inner_4()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);
            var formatInfo = NumberFormatInfo.CurrentInfo;
            var array = new[] {1, 2, 3};

            proxy.DebugFormat(formatInfo, "{0} {1} {2}", array);

            inner.Verify(i => i.DebugFormat(formatInfo, "{0} {1} {2}", array), Times.Once());
        }

        [Test]
        public void Info_should_call_inner()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);

            proxy.Info("Foo");

            inner.Verify(i => i.Info("Foo"), Times.Once());
        }


        [Test]
        public void Info_should_call_inner_2()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);
            var exception = new Exception();

            proxy.Info("Foo", exception);

            inner.Verify(i => i.Info("Foo", exception), Times.Once());
        }

        [Test]
        public void InfoFormat_should_call_inner()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);
            var array = new[] { 1, 2, 3 };

            proxy.InfoFormat("{0}", array);

            inner.Verify(i => i.InfoFormat("{0}", array), Times.Once());
        }

        [Test]
        public void InfoFormat_should_call_inner_1()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);

            proxy.InfoFormat("{0}", 1);

            inner.Verify(i => i.InfoFormat("{0}", 1), Times.Once());
        }

        [Test]
        public void InfoFormat_should_call_inner_2()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);

            proxy.InfoFormat("{0} {1}", 1, 2);

            inner.Verify(i => i.InfoFormat("{0} {1}", 1, 2), Times.Once());
        }

        [Test]
        public void InfoFormat_should_call_inner_3()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);

            proxy.InfoFormat("{0} {1} {2}", 1, 2, 3);

            inner.Verify(i => i.InfoFormat("{0} {1} {2}", 1, 2, 3), Times.Once());
        }

        [Test]
        public void InfoFormat_should_call_inner_4()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);
            var formatInfo = NumberFormatInfo.CurrentInfo;
            var array = new[] { 1, 2, 3 };

            proxy.InfoFormat(formatInfo, "{0} {1} {2}", array);

            inner.Verify(i => i.InfoFormat(formatInfo, "{0} {1} {2}", array), Times.Once());
        }

        [Test]
        public void Warn_should_call_inner()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);

            proxy.Warn("Foo");

            inner.Verify(i => i.Warn("Foo"), Times.Once());
        }


        [Test]
        public void Warn_should_call_inner_2()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);
            var exception = new Exception();

            proxy.Warn("Foo", exception);

            inner.Verify(i => i.Warn("Foo", exception), Times.Once());
        }

        [Test]
        public void WarnFormat_should_call_inner()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);
            var array = new[] {1, 2, 3};

            proxy.WarnFormat("{0}", array);

            inner.Verify(i => i.WarnFormat("{0}", array), Times.Once());
        }

        [Test]
        public void WarnFormat_should_call_inner_1()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);

            proxy.WarnFormat("{0}", 1);

            inner.Verify(i => i.WarnFormat("{0}", 1), Times.Once());
        }

        [Test]
        public void WarnFormat_should_call_inner_2()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);

            proxy.WarnFormat("{0} {1}", 1, 2);

            inner.Verify(i => i.WarnFormat("{0} {1}", 1, 2), Times.Once());
        }

        [Test]
        public void WarnFormat_should_call_inner_3()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);

            proxy.WarnFormat("{0} {1} {2}", 1, 2, 3);

            inner.Verify(i => i.WarnFormat("{0} {1} {2}", 1, 2, 3), Times.Once());
        }

        [Test]
        public void WarnFormat_should_call_inner_4()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);
            var formatInfo = NumberFormatInfo.CurrentInfo;
            var array = new[] {1, 2, 3};

            proxy.WarnFormat(formatInfo, "{0} {1} {2}", array);

            inner.Verify(i => i.WarnFormat(formatInfo, "{0} {1} {2}", array), Times.Once());
        }

        [Test]
        public void Error_should_call_inner()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);

            proxy.Error("Foo");

            inner.Verify(i => i.Error("Foo"), Times.Once());
        }


        [Test]
        public void Error_should_call_inner_2()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);
            var exception = new Exception();

            proxy.Error("Foo", exception);

            inner.Verify(i => i.Error("Foo", exception), Times.Once());
        }

        [Test]
        public void ErrorFormat_should_call_inner()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);
            var array = new[] {1, 2, 3};

            proxy.ErrorFormat("{0}", array);

            inner.Verify(i => i.ErrorFormat("{0}", array), Times.Once());
        }

        [Test]
        public void ErrorFormat_should_call_inner_1()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);

            proxy.ErrorFormat("{0}", 1);

            inner.Verify(i => i.ErrorFormat("{0}", 1), Times.Once());
        }

        [Test]
        public void ErrorFormat_should_call_inner_2()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);

            proxy.ErrorFormat("{0} {1}", 1, 2);

            inner.Verify(i => i.ErrorFormat("{0} {1}", 1, 2), Times.Once());
        }

        [Test]
        public void ErrorFormat_should_call_inner_3()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);

            proxy.ErrorFormat("{0} {1} {2}", 1, 2, 3);

            inner.Verify(i => i.ErrorFormat("{0} {1} {2}", 1, 2, 3), Times.Once());
        }

        [Test]
        public void ErrorFormat_should_call_inner_4()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);
            var formatInfo = NumberFormatInfo.CurrentInfo;
            var array = new[] {1, 2, 3};

            proxy.ErrorFormat(formatInfo, "{0} {1} {2}", array);

            inner.Verify(i => i.ErrorFormat(formatInfo, "{0} {1} {2}", array), Times.Once());
        }

        [Test]
        public void Fatal_should_call_inner()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);

            proxy.Fatal("Foo");

            inner.Verify(i => i.Fatal("Foo"), Times.Once());
        }


        [Test]
        public void Fatal_should_call_inner_2()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);
            var exception = new Exception();

            proxy.Fatal("Foo", exception);

            inner.Verify(i => i.Fatal("Foo", exception), Times.Once());
        }

        [Test]
        public void FatalFormat_should_call_inner()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);
            var array = new[] {1, 2, 3};

            proxy.FatalFormat("{0}", array);

            inner.Verify(i => i.FatalFormat("{0}", array), Times.Once());
        }

        [Test]
        public void FatalFormat_should_call_inner_1()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);

            proxy.FatalFormat("{0}", 1);

            inner.Verify(i => i.FatalFormat("{0}", 1), Times.Once());
        }

        [Test]
        public void FatalFormat_should_call_inner_2()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);

            proxy.FatalFormat("{0} {1}", 1, 2);

            inner.Verify(i => i.FatalFormat("{0} {1}", 1, 2), Times.Once());
        }

        [Test]
        public void FatalFormat_should_call_inner_3()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);

            proxy.FatalFormat("{0} {1} {2}", 1, 2, 3);

            inner.Verify(i => i.FatalFormat("{0} {1} {2}", 1, 2, 3), Times.Once());
        }

        [Test]
        public void FatalFormat_should_call_inner_4()
        {
            var proxy = new Log4NetLoggerProxy(inner.Object);
            var formatInfo = NumberFormatInfo.CurrentInfo;
            var array = new[] {1, 2, 3};

            proxy.FatalFormat(formatInfo, "{0} {1} {2}", array);

            inner.Verify(i => i.FatalFormat(formatInfo, "{0} {1} {2}", array), Times.Once());
        }
    }
}