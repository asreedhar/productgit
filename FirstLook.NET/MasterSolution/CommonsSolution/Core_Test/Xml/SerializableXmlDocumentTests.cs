﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using NUnit.Framework;

namespace FirstLook.Common.Core.Xml
{
    [TestFixture]
    public class SerializableXmlDocumentTests
    {

        [Test]
        public void CanConstruct()
        {
            SerializableXmlDocument doc = new SerializableXmlDocument();
            Assert.IsNotNull( doc );
        }

        [Test]
        public void DeserializeCorrectly()
        {
            SerializableXmlDocument doc = new SerializableXmlDocument();
            string xml = "<hello world=\"earth\"></hello>";
            doc.LoadXml( xml );

            var deserializedDoc = SerializationTester.RoundTrip(doc);

            Assert.AreEqual( doc.InnerXml, deserializedDoc.InnerXml);
            Assert.AreEqual( doc.Attributes, deserializedDoc.Attributes );
            Assert.AreEqual( doc.BaseURI, deserializedDoc.BaseURI );
            Assert.AreEqual( doc.ChildNodes.Count, deserializedDoc.ChildNodes.Count );
            Assert.AreEqual( doc.DocumentType, deserializedDoc.DocumentType );
        }
    }

    public static class SerializationTester
    {
        public static T RoundTrip<T>(T value)
        {
            if (value == null)
                throw new ArgumentNullException("value");

            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, value);

                stream.Seek(0, SeekOrigin.Begin);

                return (T)formatter.Deserialize(stream);
            }
        }
    }
}
