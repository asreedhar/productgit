﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace FirstLook.Common.Core
{
    [TestFixture]
    public class DateRangeTests
    {
        [Test]
        public void DateRangeFromDates()
        {
            DateTime start = new DateTime(2013, 01, 01);
            DateTime end = new DateTime(2013, 02, 01, 23, 59, 59);

            DateRange range = new DateRange(start, end);

            Assert.AreEqual(range.StartDate, start);
            Assert.AreEqual(range.EndDate, end);
        }

        [Test]
        public void MonthFromDate()
        {
            DateTime testDate = DateTime.Now;
            DateRange range = DateRange.MonthOfDate(testDate);

            DateTime firstOfMonth = new DateTime(testDate.Year, testDate.Month, 1);
            DateTime lastOfMonth = new DateTime(testDate.Year, testDate.Month, DateTime.DaysInMonth(testDate.Year, testDate.Month)).AddDays(1).AddSeconds(-1);
            
            Assert.AreEqual(range.StartDate, firstOfMonth);
            Assert.AreEqual(range.EndDate, lastOfMonth);
        }

        [Test]
        public void RangeContainsThisMonth()
        {
            DateTime testStartDate = DateTime.Now.AddYears(-1);
            DateTime testEndDate = DateTime.Now.AddYears(1);

            DateRange range = new DateRange(testStartDate, testEndDate);

            Assert.IsTrue(range.AsMonths().Contains(DateRange.MonthOfDate(DateTime.Now)));
        }

        [Test]
        public void StartDateAfterEndDateThrowsException()
        {
            DateTime testStartDate = DateTime.Now.AddDays(1);
            DateTime testEndDate = DateTime.Now;

            Assert.Throws<ArgumentException>(() => { new DateRange(testStartDate, testEndDate); });
        }
    }
}
