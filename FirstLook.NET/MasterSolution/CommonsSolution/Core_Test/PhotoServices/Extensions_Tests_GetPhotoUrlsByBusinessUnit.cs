using System;
using Autofac;
using FirstLook.Common.Core.Logging;
using Moq;
using NUnit.Framework;

namespace FirstLook.Common.Core.PhotoServices
{
    [TestFixture]
    public class Extensions_Tests_GetPhotoUrlsByBusinessUnit
    {
        private Mock<IPhotoServicesIdMap> mockPhotoServicesIdMap;
        private Mock<IPhotoServices> mockPhotoServices;
        private Mock<IVehiclesPhotosResult> mockVehiclesPhotosResult;
        private Mock<ILoggerFactory> mockLoggerFactory;
        private Mock<ILog> mockLog;

        #region Setup / Teardown
        [SetUp]
        public void Setup()
        {
            IOC.Registry.Reset();
            mockPhotoServicesIdMap = new Mock<IPhotoServicesIdMap>();
            mockPhotoServices = new Mock<IPhotoServices>();
            mockVehiclesPhotosResult = new Mock<IVehiclesPhotosResult>();
            mockLoggerFactory = new Mock<ILoggerFactory>();
            mockLog = new Mock<ILog>();
            SetupIocContainer();
        }

        private void SetupIocContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterInstance(mockPhotoServicesIdMap.Object).As<IPhotoServicesIdMap>();
            builder.RegisterInstance(mockLoggerFactory.Object).As<ILoggerFactory>();

            IOC.Registry.RegisterContainer(builder.Build());
        }

        [TearDown]
        public void Teardown()
        {
            IOC.Registry.Reset();
        }
        #endregion

        [Test]
        public void Should_call_photoServices_GetPhotoUrlsByBusinessUnit_with_mapped_businessUnitId()
        {
            mockPhotoServicesIdMap.Setup(svc => svc.MapBusinessUnitIdToBusinessUnitCode(78)).Returns("WINDY01");
            mockPhotoServices.Setup(svc => svc.GetPhotoUrlsByBusinessUnit("WINDY01", TimeSpan.FromHours(1)))
                .Returns(mockVehiclesPhotosResult.Object);

            var results = mockPhotoServices.Object.GetPhotoUrlsByBusinessUnit(78, TimeSpan.FromHours(1));

            Assert.That(results, Is.SameAs(mockVehiclesPhotosResult.Object));
        }

        [Test]
        public void When_mapping_businessUnitId_throws_then_should_return_empty_results_with_errors()
        {
            mockPhotoServicesIdMap.Setup(svc => svc.MapBusinessUnitIdToBusinessUnitCode(-9))
                .Throws<ApplicationException>();
            mockLoggerFactory.Setup(f => f.GetLogger(typeof(Extensions))).Returns(mockLog.Object);

            var results = mockPhotoServices.Object.GetPhotoUrlsByBusinessUnit(-9, TimeSpan.FromHours(1));

            Assert.That(results.Errors.Count, Is.EqualTo(1));
            Assert.That(results.PhotosByVin.Count, Is.EqualTo(0));
        }

        [Test]
        public void When_mapping_businessUnitId_throws_then_should_log_error()
        {
            mockPhotoServicesIdMap.Setup(svc => svc.MapBusinessUnitIdToBusinessUnitCode(-9))
                .Throws<ApplicationException>();
            mockLoggerFactory.Setup(f => f.GetLogger(typeof(Extensions))).Returns(mockLog.Object);
            mockLog.Setup(l => l.IsErrorEnabled).Returns(true);

            mockPhotoServices.Object.GetPhotoUrlsByBusinessUnit(-9, TimeSpan.FromHours(1));

            mockLog.Verify(l => l.Error(It.IsAny<string>(), It.IsAny<Exception>()));
        }

        [TestCase(null)]
        [TestCase("")]
        public void When_mapping_businessUnitId_returns_null_or_empty_then_should_return_empty_results_with_errors(string businessUnitCodeToReturn)
        {
            mockPhotoServicesIdMap.Setup(svc => svc.MapBusinessUnitIdToBusinessUnitCode(-9)).Returns(businessUnitCodeToReturn);
            mockLoggerFactory.Setup(f => f.GetLogger(typeof(Extensions))).Returns(mockLog.Object);

            var results = mockPhotoServices.Object.GetPhotoUrlsByBusinessUnit(-9, TimeSpan.FromHours(1));

            Assert.That(results.Errors.Count, Is.EqualTo(1));
            Assert.That(results.PhotosByVin.Count, Is.EqualTo(0));
        }

        [TestCase(null)]
        [TestCase("")]
        public void When_mapping_businessUnitId_returns_null_or_empty_then_should_log_error(string businessUnitCodeToReturn)
        {
            mockPhotoServicesIdMap.Setup(svc => svc.MapBusinessUnitIdToBusinessUnitCode(-9)).Returns(
                businessUnitCodeToReturn);
            mockLoggerFactory.Setup(f => f.GetLogger(typeof(Extensions))).Returns(mockLog.Object);
            mockLog.Setup(l => l.IsErrorEnabled).Returns(true);

            mockPhotoServices.Object.GetPhotoUrlsByBusinessUnit(-9, TimeSpan.FromHours(1));

            mockLog.Verify(l => l.Error(It.IsAny<string>(), It.IsAny<Exception>()));
        }
    }
}