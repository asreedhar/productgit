using System;
using Autofac;
using FirstLook.Common.Core.Logging;
using Moq;
using NUnit.Framework;

namespace FirstLook.Common.Core.PhotoServices
{
    [TestFixture]
    public class Extensions_Tests_GetMainPhotoUrl
    {
        private Mock<IPhotoServicesIdMap> mockPhotoServicesIdMap;
        private Mock<IPhotoServices> mockPhotoServices;
        private Mock<ILoggerFactory> mockLoggerFactory;
        private Mock<IPhotoServicesSettings> mockPhotoServicesSettings;
        private Mock<ILog> mockLog;

        #region Setup / Teardown
        [SetUp]
        public void Setup()
        {
            IOC.Registry.Reset();
            mockPhotoServicesIdMap = new Mock<IPhotoServicesIdMap>();
            mockPhotoServices = new Mock<IPhotoServices>();
            mockLoggerFactory = new Mock<ILoggerFactory>();
            mockLog = new Mock<ILog>();
            mockPhotoServicesSettings = new Mock<IPhotoServicesSettings>();
            SetupIocContainer();
        }

        private void SetupIocContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterInstance(mockPhotoServicesIdMap.Object).As<IPhotoServicesIdMap>();
            builder.RegisterInstance(mockLoggerFactory.Object).As<ILoggerFactory>();
            builder.RegisterInstance(mockPhotoServicesSettings.Object).As<IPhotoServicesSettings>();

            IOC.Registry.RegisterContainer(builder.Build());
        }

        [TearDown]
        public void Teardown()
        {
            IOC.Registry.Reset();
        }
        #endregion

        [Test]
        public void Should_call_photoServices_GetMainPhotoUrl_with_mapped_businessUnitId()
        {
            mockPhotoServicesIdMap.Setup(svc => svc.MapBusinessUnitIdToBusinessUnitCode(78)).Returns("WINDY01");
            mockPhotoServices.Setup(svc => svc.GetMainPhotoUrl("WINDY01", "V1234"))
                .Returns("http://photos/WINDY01/V1234/main.jpg");

            var url = mockPhotoServices.Object.GetMainPhotoUrl(78, "V1234");

            Assert.That(url, Is.EqualTo("http://photos/WINDY01/V1234/main.jpg"));
        }

        [Test]
        public void When_mapping_businessUnitId_throws_then_should_return_place_holder_url()
        {
            mockPhotoServicesIdMap.Setup(svc => svc.MapBusinessUnitIdToBusinessUnitCode(-9))
                .Throws<ApplicationException>();
            mockLoggerFactory.Setup(f => f.GetLogger(typeof(Extensions))).Returns(mockLog.Object);
            mockPhotoServicesSettings.Setup(svc => svc.MainPhotoPlaceholderUrl).Returns("http://photos/placeholder.jpg");

            var url = mockPhotoServices.Object.GetMainPhotoUrl(-9, "V1234A");

            Assert.That(url, Is.EqualTo("http://photos/placeholder.jpg"));
        }

        [Test]
        public void When_mapping_businessUnitId_throws_then_should_log_error()
        {
            mockPhotoServicesIdMap.Setup(svc => svc.MapBusinessUnitIdToBusinessUnitCode(-9))
                .Throws<ApplicationException>();
            mockLoggerFactory.Setup(f => f.GetLogger(typeof(Extensions))).Returns(mockLog.Object);
            mockLog.Setup(l => l.IsErrorEnabled).Returns(true);

            mockPhotoServices.Object.GetMainPhotoUrl(-9, "V1234");

            mockLog.Verify(l => l.Error(It.IsAny<string>(), It.IsAny<Exception>()));
        }

        [TestCase(null)]
        [TestCase("")]
        public void When_mapping_businessUnitId_returns_null_or_empty_then_should_return_place_holder_url(string businessUnitCodeToReturn)
        {
            mockPhotoServicesIdMap.Setup(svc => svc.MapBusinessUnitIdToBusinessUnitCode(-9)).Returns(businessUnitCodeToReturn);
            mockLoggerFactory.Setup(f => f.GetLogger(typeof(Extensions))).Returns(mockLog.Object);
            mockPhotoServicesSettings.Setup(svc => svc.MainPhotoPlaceholderUrl).Returns("http://photos/placeholder.jpg");

            var url = mockPhotoServices.Object.GetMainPhotoUrl(-9, "V1234C");

            Assert.That(url, Is.EqualTo("http://photos/placeholder.jpg"));
        }

        [TestCase(null)]
        [TestCase("")]
        public void When_mapping_businessUnitId_returns_null_or_empty_then_should_log_error(string businessUnitCodeToReturn)
        {
            mockPhotoServicesIdMap.Setup(svc => svc.MapBusinessUnitIdToBusinessUnitCode(-9)).Returns(
                businessUnitCodeToReturn);
            mockLoggerFactory.Setup(f => f.GetLogger(typeof(Extensions))).Returns(mockLog.Object);
            mockLog.Setup(l => l.IsErrorEnabled).Returns(true);

            mockPhotoServices.Object.GetMainPhotoUrl(-9, "V1234D");

            mockLog.Verify(l => l.Error(It.IsAny<string>(), It.IsAny<Exception>()));
        }
    }
}