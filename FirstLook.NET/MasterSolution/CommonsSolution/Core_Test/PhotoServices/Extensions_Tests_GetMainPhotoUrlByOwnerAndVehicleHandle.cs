using System;
using Autofac;
using FirstLook.Common.Core.Logging;
using Moq;
using NUnit.Framework;

namespace FirstLook.Common.Core.PhotoServices
{
    [TestFixture]
    public class Extensions_Tests_GetMainPhotoUrlByOwnerAndVehicleHandle
    {
        private Mock<IPhotoServicesIdMap> mockPhotoServicesIdMap;
        private Mock<IPhotoServices> mockPhotoServices;
        private Mock<ILoggerFactory> mockLoggerFactory;
        private Mock<ILog> mockLog;
        private Mock<IPhotoServicesSettings> mockPhotoServicesSettings;

        #region Setup / Teardown
        [SetUp]
        public void Setup()
        {
            IOC.Registry.Reset();
            mockPhotoServicesIdMap = new Mock<IPhotoServicesIdMap>();
            mockPhotoServices = new Mock<IPhotoServices>();
            mockLoggerFactory = new Mock<ILoggerFactory>();
            mockLog = new Mock<ILog>();
            mockPhotoServicesSettings = new Mock<IPhotoServicesSettings>();
            SetupIocContainer();
        }

        private void SetupIocContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterInstance(mockPhotoServicesIdMap.Object).As<IPhotoServicesIdMap>();
            builder.RegisterInstance(mockPhotoServicesSettings.Object).As<IPhotoServicesSettings>();
            builder.RegisterInstance(mockLoggerFactory.Object).As<ILoggerFactory>();

            IOC.Registry.RegisterContainer(builder.Build());
        }

        [TearDown]
        public void Teardown()
        {
            IOC.Registry.Reset();
        }
        #endregion

        [Test]
        public void Should_call_photoServices_GetMainPhotoUrl_with_mapped_values()
        {
            mockPhotoServicesIdMap.Setup(svc => svc.MapOwnerAndVehicleHandleToBusinessUnitAndVIN("Owner-Handle", "Veh-Handle"))
                .Returns(new BusinessUnitAndVIN("BUCODE", "SOMEVIN123"));
            mockPhotoServices.Setup(svc => svc.GetMainPhotoUrl("BUCODE", "SOMEVIN123"))
                .Returns("http://photos/WINDYCITY/SOMEVIN123/main.jpg");

            var returnedUrl = mockPhotoServices.Object.GetMainPhotoUrlByOwnerAndVehicleHandle("Owner-Handle", "Veh-Handle");

            Assert.That(returnedUrl, Is.EqualTo("http://photos/WINDYCITY/SOMEVIN123/main.jpg"));
        }

        [Test]
        public void When_mapper_throws_should_return_placeholder_main_image_url()
        {
            mockPhotoServicesIdMap.Setup(
                svc => svc.MapOwnerAndVehicleHandleToBusinessUnitAndVIN("Invalid_Owner_Handle", It.IsAny<string>()))
                .Throws(new ApplicationException("Owner not found"));
            mockPhotoServicesSettings.Setup(svc => svc.MainPhotoPlaceholderUrl).Returns("http://place-holder/main.jpg");
            mockLoggerFactory.Setup(f => f.GetLogger(typeof(Extensions))).Returns(mockLog.Object);

            var url = mockPhotoServices.Object.GetMainPhotoUrlByOwnerAndVehicleHandle("Invalid_Owner_Handle", "AnyVIN");

            Assert.AreEqual("http://place-holder/main.jpg", url);
        }

        [Test]
        public void When_mapper_returns_null_should_return_placeholder_main_image_url()
        {
            mockPhotoServicesIdMap.Setup(
                svc => svc.MapOwnerAndVehicleHandleToBusinessUnitAndVIN("Invalid_Owner_Handle", It.IsAny<string>()))
                .Returns((BusinessUnitAndVIN)null);
            mockPhotoServicesSettings.Setup(svc => svc.MainPhotoPlaceholderUrl).Returns("http://place-holder/main.jpg");
            mockLoggerFactory.Setup(f => f.GetLogger(typeof(Extensions))).Returns(mockLog.Object);

            var url = mockPhotoServices.Object.GetMainPhotoUrlByOwnerAndVehicleHandle("Invalid_Owner_Handle", "AnyVIN");

            Assert.AreEqual("http://place-holder/main.jpg", url);
        }

        [Test]
        public void When_mapper_throws_should_log_error()
        {
            mockPhotoServicesIdMap.Setup(
                svc => svc.MapOwnerAndVehicleHandleToBusinessUnitAndVIN("Invalid_Owner_Handle", It.IsAny<string>()))
                .Throws(new ApplicationException("Owner not found"));
            mockLoggerFactory.Setup(f => f.GetLogger(typeof(Extensions))).Returns(mockLog.Object);
            mockLog.Setup(l => l.IsErrorEnabled).Returns(true);

            mockPhotoServices.Object.GetMainPhotoUrlByOwnerAndVehicleHandle("Invalid_Owner_Handle", "AnyVIN");

            mockLog.Verify(l => l.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce());
        }

        [Test]
        public void When_mapper_returns_null_should_log_error()
        {
            mockPhotoServicesIdMap.Setup(
                svc => svc.MapOwnerAndVehicleHandleToBusinessUnitAndVIN("Invalid_Owner_Handle", It.IsAny<string>()))
                .Returns((BusinessUnitAndVIN)null);
            mockLoggerFactory.Setup(f => f.GetLogger(typeof(Extensions))).Returns(mockLog.Object);
            mockLog.Setup(l => l.IsErrorEnabled).Returns(true);

            mockPhotoServices.Object.GetMainPhotoUrlByOwnerAndVehicleHandle("Invalid_Owner_Handle", "AnyVIN");

            mockLog.Verify(l => l.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce());
        }
    }
}