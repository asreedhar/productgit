using System;
using Autofac;
using FirstLook.Common.Core.Logging;
using FirstLook.Common.Core.Membership;
using Moq;
using NUnit.Framework;

namespace FirstLook.Common.Core.PhotoServices
{
    [TestFixture]
    public class Extensions_Test_GetPhotoManagerUrl
    {
        private Mock<IPhotoServicesIdMap> mockPhotoServicesIdMap;
        private Mock<IPhotoServices> mockPhotoServices;
        private Mock<IMemberContext> mockMemberContext;
        private Mock<IMember> mockMember;
        private Mock<ILoggerFactory> mockLoggerFactory;
        private Mock<ILog> mockLog;
        private Mock<IPhotoServicesSettings> mockPhotoServicesSettings;

        #region Setup / Teardown
        [SetUp]
        public void Setup()
        {
            IOC.Registry.Reset();
            mockPhotoServicesIdMap = new Mock<IPhotoServicesIdMap>();
            mockPhotoServices = new Mock<IPhotoServices>();
            mockMemberContext = new Mock<IMemberContext>();
            mockMember = new Mock<IMember>();
            mockLoggerFactory = new Mock<ILoggerFactory>();
            mockLog = new Mock<ILog>();
            mockPhotoServicesSettings = new Mock<IPhotoServicesSettings>();
            SetupIocContainer();
        }

        private void SetupIocContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterInstance(mockPhotoServicesIdMap.Object).As<IPhotoServicesIdMap>();
            builder.RegisterInstance(mockMemberContext.Object).As<IMemberContext>();
            builder.RegisterInstance(mockPhotoServicesSettings.Object).As<IPhotoServicesSettings>();
            builder.RegisterInstance(mockLoggerFactory.Object).As<ILoggerFactory>();

            IOC.Registry.RegisterContainer(builder.Build());
        }

        [TearDown]
        public void Teardown()
        {
            IOC.Registry.Reset();
        }
        #endregion

        [Test]
        public void Should_call_photoServices_GetMainPhotoUrl_with_mapped_businessUnitId_and_currentUser()
        {
            mockPhotoServicesIdMap.Setup(
                svc => svc.MapBusinessUnitIdToBusinessUnitCode(219)).Returns("WINDYCITY");
            mockMemberContext.Setup(svc => svc.Current).Returns(mockMember.Object);

            mockPhotoServices.Object.GetPhotoManagerUrl(219, "F1234", "STK1234", PhotoManagerContext.MAX);

            mockPhotoServices.Verify(
                svc =>
                svc.GetPhotoManagerUrl(mockMember.Object, "WINDYCITY", "F1234", "STK1234", PhotoManagerContext.MAX));
        }

        [Test]
        public void When_mapping_businessUnitId_throws_then_should_return_place_holder_url()
        {
            mockPhotoServicesIdMap.Setup(
                svc => svc.MapBusinessUnitIdToBusinessUnitCode(-1000)).Throws<ApplicationException>();
            mockMemberContext.Setup(svc => svc.Current).Returns(mockMember.Object);
            mockPhotoServicesSettings.Setup(svc => svc.PhotoManagerNotAvailableUrl).Returns("http://photo-manager/placeholder");
            mockLoggerFactory.Setup(f => f.GetLogger(typeof (Extensions))).Returns(mockLog.Object);

            var url = mockPhotoServices.Object.GetPhotoManagerUrl(-1000, "F1234", "STK1234", PhotoManagerContext.IMS);

            Assert.That(url, Is.EqualTo("http://photo-manager/placeholder"));
        }

        [Test]
        public void When_mapping_businessUnitId_throws_then_should_log_error()
        {
            mockPhotoServicesIdMap.Setup(
                svc => svc.MapBusinessUnitIdToBusinessUnitCode(-1000)).Throws<ApplicationException>();
            mockMemberContext.Setup(svc => svc.Current).Returns(mockMember.Object);
            mockLoggerFactory.Setup(f => f.GetLogger(typeof (Extensions))).Returns(mockLog.Object);
            mockLog.Setup(l => l.IsErrorEnabled).Returns(true);

            mockPhotoServices.Object.GetPhotoManagerUrl(-1000, "F1234", "STK1234", PhotoManagerContext.IMS);

            mockLog.Verify(l => l.Error(It.IsAny<string>(), It.IsAny<Exception>()));
        }

        [TestCase("")]
        [TestCase(null)]
        public void When_mapping_businessUnitId_returns_null_or_empty_then_should_return_place_holder_url(string returnedBusinessUnitCode)
        {
            mockPhotoServicesIdMap.Setup(
                svc => svc.MapBusinessUnitIdToBusinessUnitCode(-1000)).Returns(returnedBusinessUnitCode);
            mockMemberContext.Setup(svc => svc.Current).Returns(mockMember.Object);
            mockPhotoServicesSettings.Setup(svc => svc.PhotoManagerNotAvailableUrl).Returns("http://photo-manager/placeholder");
            mockLoggerFactory.Setup(f => f.GetLogger(typeof (Extensions))).Returns(mockLog.Object);

            var url = mockPhotoServices.Object.GetPhotoManagerUrl(-1000, "F1234", "STK1234", PhotoManagerContext.IMS);

            Assert.That(url, Is.EqualTo("http://photo-manager/placeholder"));
        }

        [TestCase("")]
        [TestCase(null)]
        public void When_mapping_businessUnitId_returns_null_or_empty_then_should_log_error(string returnedBusinessUnitCode)
        {
            mockPhotoServicesIdMap.Setup(
                svc => svc.MapBusinessUnitIdToBusinessUnitCode(-1000)).Returns(returnedBusinessUnitCode);
            mockMemberContext.Setup(svc => svc.Current).Returns(mockMember.Object);
            mockLoggerFactory.Setup(f => f.GetLogger(typeof(Extensions))).Returns(mockLog.Object);
            mockLog.Setup(l => l.IsErrorEnabled).Returns(true);

            mockPhotoServices.Object.GetPhotoManagerUrl(-1000, "F1234", "STK1234", PhotoManagerContext.IMS);

            mockLog.Verify(l => l.Error(It.IsAny<string>(), It.IsAny<Exception>()));
        }

        [Test]
        public void When_MemberContext_Current_throws_then_should_return_place_holder_url()
        {
            mockMemberContext.Setup(svc => svc.Current).Throws<ApplicationException>();
            mockPhotoServicesIdMap.Setup(svc => svc.MapBusinessUnitIdToBusinessUnitCode(1)).Returns("WINDY");
            mockPhotoServicesSettings.Setup(svc => svc.PhotoManagerNotAvailableUrl).Returns("http://photo-manager/placeholder");
            mockLoggerFactory.Setup(f => f.GetLogger(typeof (Extensions))).Returns(mockLog.Object);

            var url = mockPhotoServices.Object.GetPhotoManagerUrl(1, "F1234", "STK1234", PhotoManagerContext.MAX);

            Assert.That(url, Is.EqualTo("http://photo-manager/placeholder"));
        }

        [Test]
        public void When_MemberContext_Current_throws_then_should_log_error()
        {
            mockMemberContext.Setup(svc => svc.Current).Throws<ApplicationException>();
            mockPhotoServicesIdMap.Setup(svc => svc.MapBusinessUnitIdToBusinessUnitCode(1)).Returns("WINDY");
            mockLoggerFactory.Setup(f => f.GetLogger(typeof(Extensions))).Returns(mockLog.Object);
            mockLog.Setup(l => l.IsErrorEnabled).Returns(true);

            mockPhotoServices.Object.GetPhotoManagerUrl(1, "F1234", "STK1234", PhotoManagerContext.MAX);

            mockLog.Verify(l => l.Error(It.IsAny<string>(), It.IsAny<Exception>()));
        }

        [Test]
        public void When_MemberContext_Current_returns_null_then_should_return_place_holder_url()
        {
            mockMemberContext.Setup(svc => svc.Current).Returns((IMember)null);
            mockPhotoServicesIdMap.Setup(svc => svc.MapBusinessUnitIdToBusinessUnitCode(1)).Returns("WINDY");
            mockPhotoServicesSettings.Setup(svc => svc.PhotoManagerNotAvailableUrl).Returns("http://photo-manager/placeholder");
            mockLoggerFactory.Setup(f => f.GetLogger(typeof(Extensions))).Returns(mockLog.Object);

            var url = mockPhotoServices.Object.GetPhotoManagerUrl(1, "F1234", "STK1234", PhotoManagerContext.MAX);

            Assert.That(url, Is.EqualTo("http://photo-manager/placeholder"));
        }

        [Test]
        public void When_MemberContext_Current_returns_null_then_should_log_error()
        {
            mockMemberContext.Setup(svc => svc.Current).Returns((IMember) null);
            mockPhotoServicesIdMap.Setup(svc => svc.MapBusinessUnitIdToBusinessUnitCode(1)).Returns("WINDY");
            mockLoggerFactory.Setup(f => f.GetLogger(typeof(Extensions))).Returns(mockLog.Object);
            mockLog.Setup(l => l.IsErrorEnabled).Returns(true);

            mockPhotoServices.Object.GetPhotoManagerUrl(1, "F1234", "STK1234", PhotoManagerContext.MAX);

            mockLog.Verify(l => l.Error(It.IsAny<string>(), It.IsAny<Exception>()));
        }
    }
}