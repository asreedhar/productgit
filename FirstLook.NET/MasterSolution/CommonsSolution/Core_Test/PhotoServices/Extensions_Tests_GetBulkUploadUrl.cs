using System;
using Autofac;
using FirstLook.Common.Core.Logging;
using FirstLook.Common.Core.Membership;
using Moq;
using NUnit.Framework;

namespace FirstLook.Common.Core.PhotoServices
{
    [TestFixture]
    public class Extensions_Tests_GetBulkUploadUrl
    {
        private Mock<IPhotoServicesIdMap> mockPhotoServicesIdMap;
        private Mock<IPhotoServices> mockPhotoServices;
        private Mock<IMemberContext> mockMemberContext;
        private Mock<IMember> mockMember;
        private Mock<ILoggerFactory> mockLoggerFactory;
        private Mock<ILog> mockLog;
        private Mock<IPhotoServicesSettings> mockPhotoServicesSettings;

        #region Setup / Teardown
        [SetUp]
        public void Setup()
        {
            IOC.Registry.Reset();
            mockPhotoServicesIdMap = new Mock<IPhotoServicesIdMap>();
            mockPhotoServices = new Mock<IPhotoServices>();
            mockMemberContext = new Mock<IMemberContext>();
            mockMember = new Mock<IMember>();
            mockLoggerFactory = new Mock<ILoggerFactory>();
            mockLog = new Mock<ILog>();
            mockPhotoServicesSettings = new Mock<IPhotoServicesSettings>();
            SetupIocContainer();
        }

        private void SetupIocContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterInstance(mockPhotoServicesIdMap.Object).As<IPhotoServicesIdMap>();
            builder.RegisterInstance(mockMemberContext.Object).As<IMemberContext>();
            builder.RegisterInstance(mockPhotoServicesSettings.Object).As<IPhotoServicesSettings>();
            builder.RegisterInstance(mockLoggerFactory.Object).As<ILoggerFactory>();

            IOC.Registry.RegisterContainer(builder.Build());
        }

        [TearDown]
        public void Teardown()
        {
            IOC.Registry.Reset();
        }
        #endregion

        [Test]
        public void Should_call_photoServices_GetBulkUploadManagerUrl_with_mapped_businessUnitId_and_currentUser()
        {
            mockPhotoServicesIdMap.Setup(
                svc => svc.MapBusinessUnitIdToBusinessUnitCode(219)).Returns("WINDYCITY");
            mockMemberContext.Setup(svc => svc.Current).Returns(mockMember.Object);

            mockPhotoServices.Object.GetBulkUploadManagerUrl(219, PhotoManagerContext.MAX);

            mockPhotoServices.Verify(
                svc =>
                svc.GetBulkUploadManagerUrl(mockMember.Object, "WINDYCITY", PhotoManagerContext.MAX));
        }

        [Test]
        public void When_mapping_businessUnitId_throws_then_should_return_place_holder_url()
        {
            mockPhotoServicesIdMap.Setup(
                svc => svc.MapBusinessUnitIdToBusinessUnitCode(-1000)).Throws<ApplicationException>();
            mockMemberContext.Setup(svc => svc.Current).Returns(mockMember.Object);
            mockPhotoServicesSettings.Setup(svc => svc.BulkUploadManagerNotAvailableUrl).Returns("http://bulk-upload/placeholder");
            mockLoggerFactory.Setup(f => f.GetLogger(typeof (Extensions))).Returns(mockLog.Object);

            var url = mockPhotoServices.Object.GetBulkUploadManagerUrl(-1000, PhotoManagerContext.IMS);

            Assert.That(url, Is.EqualTo("http://bulk-upload/placeholder"));
        }

        [Test]
        public void When_mapping_businessUnitId_throws_then_should_log_error()
        {
            mockPhotoServicesIdMap.Setup(
                svc => svc.MapBusinessUnitIdToBusinessUnitCode(-1000)).Throws<ApplicationException>();
            mockMemberContext.Setup(svc => svc.Current).Returns(mockMember.Object);
            mockLoggerFactory.Setup(f => f.GetLogger(typeof(Extensions))).Returns(mockLog.Object);
            mockLog.Setup(l => l.IsErrorEnabled).Returns(true);

            mockPhotoServices.Object.GetBulkUploadManagerUrl(-1000, PhotoManagerContext.IMS);

            mockLog.Verify(l => l.Error(It.IsAny<string>(), It.IsAny<Exception>()));
        }

        [TestCase("")]
        [TestCase(null)]
        public void When_mapping_businessUnitId_returns_null_or_empty_then_should_return_place_holder_url(string returnedBusinessCodeMapping)
        {
            mockPhotoServicesIdMap.Setup(
                svc => svc.MapBusinessUnitIdToBusinessUnitCode(-10)).Returns(returnedBusinessCodeMapping);
            mockMemberContext.Setup(svc => svc.Current).Returns(mockMember.Object);
            mockPhotoServicesSettings.Setup(svc => svc.BulkUploadManagerNotAvailableUrl).Returns("http://bulk-upload/placeholder");
            mockLoggerFactory.Setup(f => f.GetLogger(typeof (Extensions))).Returns(mockLog.Object);

            var url = mockPhotoServices.Object.GetBulkUploadManagerUrl(-10, PhotoManagerContext.MAX);

            Assert.That(url, Is.EqualTo("http://bulk-upload/placeholder"));
        }

        [TestCase("")]
        [TestCase(null)]
        public void When_mapping_businessUnitId_returns_null_or_empty_then_should_log_error(string returnedBusinessCodeMapping)
        {
            mockPhotoServicesIdMap.Setup(
                svc => svc.MapBusinessUnitIdToBusinessUnitCode(-10)).Returns(returnedBusinessCodeMapping);
            mockMemberContext.Setup(svc => svc.Current).Returns(mockMember.Object);
            mockLoggerFactory.Setup(f => f.GetLogger(typeof(Extensions))).Returns(mockLog.Object);
            mockLog.Setup(l => l.IsErrorEnabled).Returns(true);

            mockPhotoServices.Object.GetBulkUploadManagerUrl(-10, PhotoManagerContext.MAX);

            mockLog.Verify(l => l.Error(It.IsAny<string>(), It.IsAny<Exception>()));
        }

        [Test]
        public void When_MemberContext_Current_throws_then_should_return_place_holder_url()
        {
            mockPhotoServicesIdMap.Setup(svc => svc.MapBusinessUnitIdToBusinessUnitCode(25)).Returns("WINDY");
            mockMemberContext.Setup(svc => svc.Current).Throws<ApplicationException>();
            mockPhotoServicesSettings.Setup(svc => svc.BulkUploadManagerNotAvailableUrl).Returns("http://bulk-upload/placeholder");
            mockLoggerFactory.Setup(f => f.GetLogger(typeof (Extensions))).Returns(mockLog.Object);

            var url = mockPhotoServices.Object.GetBulkUploadManagerUrl(25, PhotoManagerContext.MAX);

            Assert.That(url, Is.EqualTo("http://bulk-upload/placeholder"));
        }

        [Test]
        public void When_MemberContext_Current_throws_then_should_log_error()
        {
            mockPhotoServicesIdMap.Setup(svc => svc.MapBusinessUnitIdToBusinessUnitCode(25)).Returns("WINDY");
            mockMemberContext.Setup(svc => svc.Current).Throws<ApplicationException>();
            mockLoggerFactory.Setup(f => f.GetLogger(typeof(Extensions))).Returns(mockLog.Object);
            mockLog.Setup(l => l.IsErrorEnabled).Returns(true);

            mockPhotoServices.Object.GetBulkUploadManagerUrl(25, PhotoManagerContext.MAX);

            mockLog.Verify(l => l.Error(It.IsAny<string>(), It.IsAny<Exception>()));
        }

        [Test]
        public void When_MemberContext_Current_is_null_should_return_place_holder_url()
        {
            mockPhotoServicesIdMap.Setup(svc => svc.MapBusinessUnitIdToBusinessUnitCode(25)).Returns("WINDY");
            mockMemberContext.Setup(svc => svc.Current).Returns((IMember) null);
            mockPhotoServicesSettings.Setup(svc => svc.BulkUploadManagerNotAvailableUrl).Returns("http://bulk-upload/placeholder");
            mockLoggerFactory.Setup(f => f.GetLogger(typeof(Extensions))).Returns(mockLog.Object);

            var url = mockPhotoServices.Object.GetBulkUploadManagerUrl(25, PhotoManagerContext.MAX);

            Assert.That(url, Is.EqualTo("http://bulk-upload/placeholder"));
        }

        [Test]
        public void When_MemberContext_Current_is_null_should_log_error()
        {
            mockPhotoServicesIdMap.Setup(svc => svc.MapBusinessUnitIdToBusinessUnitCode(25)).Returns("WINDY");
            mockMemberContext.Setup(svc => svc.Current).Returns((IMember)null);
            mockLoggerFactory.Setup(f => f.GetLogger(typeof(Extensions))).Returns(mockLog.Object);
            mockLog.Setup(l => l.IsErrorEnabled).Returns(true);

            mockPhotoServices.Object.GetBulkUploadManagerUrl(25, PhotoManagerContext.MAX);

            mockLog.Verify(l => l.Error(It.IsAny<string>(), It.IsAny<Exception>()));
        }
    }
}