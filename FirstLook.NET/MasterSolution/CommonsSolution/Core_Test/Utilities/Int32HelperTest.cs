using System;
using NUnit.Framework;

namespace FirstLook.Common.Core.Utilities
{
    [TestFixture]
    public class Int32HelperTest
    {
        #region ToInt32 Tests

        [Test]
        public void ToInt32_Null()
        {
            Assert.AreEqual(0, Int32Helper.ToInt32(null), "Null is not converted to zero");
        }

        [Test]
        public void ToInt32_DBNull()
        {
            Assert.AreEqual(0, Int32Helper.ToInt32(DBNull.Value), "DBNull.Value is not converted to 0");
        }

        [Test]
        public void ToInt32_EmptyString()
        {
            Assert.AreEqual(0, Int32Helper.ToInt32(string.Empty), "Empty string is not converted to zero");
        }

        [Test]
        public void ToInt32_NaN()
        {
            Assert.AreEqual(0, Int32Helper.ToInt32("NaN"), "'NaN' string is not converted to zero");
        }

        [Test]
        public void ToInt32_Infinity()
        {
            Assert.AreEqual(0, Int32Helper.ToInt32("Infinity"), "'Infinity' string is not converted to zero");
        }

        [Test]
        public void ToInt32_NonNumber()
        {
            Assert.AreEqual(0, Int32Helper.ToInt32("Hello"), "'Hello' string is not converted to zero");
        }

        [Test]
        public void ToInt32_MinValue()
        {
            Assert.AreEqual(Int32.MinValue, Int32Helper.ToInt32(Int32.MinValue),
                            "Int32.MinValue is not converted to Int32.MinValue");
        }

        [Test]
        public void ToInt32_MaxValue()
        {
            Assert.AreEqual(Int32.MaxValue, Int32Helper.ToInt32(Int32.MaxValue),
                            "Int32.MaxValue is not converted to Int32.MaxValue");
        }

        [Test]
        public void ToInt32_Double()
        {
            Assert.AreEqual(3, Int32Helper.ToInt32(Math.PI), "Math.PI is not converted to 3");
        }

        #endregion

        #region ToNullableInt32 Tests

        [Test]
        public void ToNullableInt32_Null()
        {
            Assert.IsNull(Int32Helper.ToNullableInt32(null), "Null is not converted to null");
        }

        [Test]
        public void ToNullableInt32_DBNull()
        {
            Assert.IsNull(Int32Helper.ToNullableInt32(DBNull.Value), "DBNull.Value is not converted to null");
        }

        [Test]
        public void ToNullableInt32_EmptyString()
        {
            Assert.IsNull(Int32Helper.ToNullableInt32(string.Empty), "Empty string is not converted to null");
        }

        [Test]
        public void ToNullableInt32_NaN()
        {
            Assert.IsNull(Int32Helper.ToNullableInt32("NaN"), "'NaN' string is not converted to null");
        }

        [Test]
        public void ToNullableInt32_Infinity()
        {
            Assert.IsNull(Int32Helper.ToNullableInt32("Infinity"), "'Infinity' string is not converted to null");
        }

        [Test]
        public void ToNullableInt32_NonNumber()
        {
            Assert.IsNull(Int32Helper.ToNullableInt32("Hello"), "'Hello' string is not converted to null");
        }

        [Test]
        public void ToNullableInt32_MinValue()
        {
            Assert.AreEqual(Int32.MinValue, Int32Helper.ToNullableInt32(Int32.MinValue),
                            "Int32.MinValue is not converted to Int32.MinValue");
        }

        [Test]
        public void ToNullableInt32_MaxValue()
        {
            Assert.AreEqual(Int32.MaxValue, Int32Helper.ToNullableInt32(Int32.MaxValue),
                            "Int32.MaxValue is not converted to Int32.MaxValue");
        }

        [Test]
        public void ToNullableInt32_Double()
        {
            Assert.AreEqual(3, Int32Helper.ToNullableInt32(Math.PI), "Math.PI is not converted to 3");
        }

        #endregion
    }
}