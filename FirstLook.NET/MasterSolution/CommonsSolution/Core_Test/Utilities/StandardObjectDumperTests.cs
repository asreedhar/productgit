﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

namespace FirstLook.Common.Core.Utilities
{
    [TestFixture]
    public class StandardObjectDumperTests
    {
        [Test]
        public void Test()
        {
            Foo foo = new Foo();
            foo.Bar = "bar";
            foo.Baz = new List<string>{"a", "b"};

            string fooString = StandardObjectDumper.Write(foo, 1);
            Console.WriteLine(fooString);

            Assert.IsNotEmpty(fooString);
        }

    }

    public class Foo
    {
        public string Bar { get; set; }
        public List<string> Baz { get; set; }
    }
}
