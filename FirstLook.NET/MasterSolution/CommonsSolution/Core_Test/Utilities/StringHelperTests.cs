﻿using System;
using System.Linq;
using FirstLook.Common.Core.Extensions;
using NUnit.Framework;

namespace FirstLook.Common.Core.Utilities
{
    [TestFixture]
    public class StringHelperTests
    {
        [Test]
        public void LevenshteinDistance()
        {
            const string S = "zac";
            const string T = "man";

            Assert.AreEqual(2, StringHelper.LevenshteinDistance(S, T));
        }

        [Test]
        public void LevenshteinDistanceConsidersWhiteSpace()
        {
            const string S = "";
            const string T = " ";

            Assert.AreEqual(1, StringHelper.LevenshteinDistance(S, T));
        }

        [Test]
        public void LevenshteinDistanceEmptyStrings()
        {
            const string S = "";
            const string T = "";

            Assert.AreEqual(0, StringHelper.LevenshteinDistance(S, T));
        }

        [Test]
        public void LevenshteinDistanceOneEmptyString()
        {
            const string S = "";
            const string T = "a";

            Assert.AreEqual(1, StringHelper.LevenshteinDistance(S, T));
        }

        [Test]
        public void LevenshteinDistanceOneNull()
        {
            const string A = null;
            const string B = "";

            Assert.AreNotEqual( 0, StringHelper.LevenshteinDistance(A, B));
        }

        [Test]
        public void LevenshteinDistanceBothNull()
        {
            const string A = null;
            const string B = null;

            Assert.AreEqual(0, StringHelper.LevenshteinDistance(A, B));
        }


        [Test]
        public void EmptyStringsAreSimilar()
        {
            const string A = "";
            const string B = "";

            Assert.IsTrue(StringHelper.AreSimilar(A, B));
        }

        [Test]
        public void NullStringsAreSimilar()
        {
            const string A = null;
            const string B = null;

            Assert.IsTrue(StringHelper.AreSimilar(A, B));            
        }

        [Test]
        public void NullNotSimilarToNonNull()
        {
            const string A = null;
            const string B = "";

            Assert.IsFalse(StringHelper.AreSimilar(A, B));                        
        }


        [Test]
        public void LessThanTwentyFivePercentAreSimilar()
        {
            // 25 a followed by 75 space
            var a = "a".PadRight(25, 'a') + " ".PadRight(75, ' ');

            // 25 b followed by 75 spaces
            var b = "b".PadRight(25, 'b') + " ".PadRight(75, ' ');

            Assert.IsTrue(StringHelper.AreSimilar(a, b));
        }

        [Test]
        public void MoreThanTwentyFivePercentAreNotSimilar()
        {
            // 26 a followed by 74 space
            var a = "a".PadRight(26, 'a') + " ".PadRight(74, ' ');

            // 25 b followed by 75 spaces
            var b = "b".PadRight(25, 'b') + " ".PadRight(75, ' ');

            Assert.IsFalse(StringHelper.AreSimilar(a, b));
        }

         



    }
}