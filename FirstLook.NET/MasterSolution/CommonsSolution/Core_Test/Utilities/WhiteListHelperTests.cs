﻿using System;
using NUnit.Framework;

namespace FirstLook.Common.Core.Utilities
{
    [TestFixture]
    public class WhiteListHelperTests
    {
        [TestCase("WINDYCIT05")]
        [TestCase("WINDYCIT01,WINDYCIT05")]
        [TestCase("ALL")]
        public void IsInWhiteList_T_should_return_true_when_value_is_in_white_list(string list)
        {
            var whiteList = list.Split(',');
            var comparer = StringComparer.InvariantCultureIgnoreCase;

            Assert.That(WhiteListHelper.IsInWhiteList("WINDYCIT05", whiteList, comparer.Compare, "all"), Is.True);
        }

        [TestCase("WINDYCIT05")]
        [TestCase("WINDYCIT01,WINDYCIT05")]
        public void IsInWhiteList_T_should_return_false_when_value_is_not_in_white_list(string list)
        {
            var whiteList = list.Split(',');
            var comparer = StringComparer.InvariantCultureIgnoreCase;

            Assert.That(WhiteListHelper.IsInWhiteList("ANOTHER_BUSINESS_UNIT", whiteList, comparer.Compare, "all"),
                        Is.False);
        }

        [Test]
        public void IsInWhiteList_T_should_throw_if_whiteList_is_null()
        {
            Assert.Throws<ArgumentNullException>(
                () =>
                WhiteListHelper.IsInWhiteList("Any", null, StringComparer.InvariantCultureIgnoreCase.Compare, "All"));
        }

        [TestCase("WINDYCIT05")]
        [TestCase("  WINDYCIT05  ")]
        [TestCase(",WINDYCIT05")]
        [TestCase("WINDYCIT01,WINDYCIT05")]
        public void IsInWhiteList_should_return_false_when_value_is_not_in_white_list(string list)
        {
            Assert.That(WhiteListHelper.IsInWhiteList("OTHER_BUSINESS_UNIT", list), Is.False);
        }

        [TestCase("WINDYCIT05")]
        [TestCase("  WINDYCIT05  ")]
        [TestCase(",WINDYCIT05")]
        [TestCase("WINDYCIT01,WINDYCIT05")]
        [TestCase("ALL")]
        public void IsInWhiteList_should_return_true_when_value_is_in_white_list(string list)
        {
            Assert.That(WhiteListHelper.IsInWhiteList("WINDYCIT05", list), Is.True);
        }

        [Test]
        public void IsInWhiteList_should_throw_if_whiteList_is_null()
        {
            Assert.Throws<ArgumentNullException>(
                () => WhiteListHelper.IsInWhiteList("Any", null));
        }

        [TestCase("", "")]
        [TestCase(null, "")]
        [TestCase("", null)]
        [TestCase(null, null)]
        [TestCase("test", "")]
        [TestCase("test", null)]
        public void IsInWhiteList_should_all_empty_whitelist(string value, string whitelist)
        {
            Assert.IsTrue(WhiteListHelper.IsInWhiteList(value, whitelist, emptyWhitelistAllowsAll: true));
        }

        [TestCase("string1,string2",',', true)]
        [TestCase("string1;string2", ',', false)]
        [TestCase("string1;string2", ';', true)]
        [TestCase("string1,string2", ';', false)]
        [TestCase("string1", ',', true)]
        [TestCase("string1", ';', true)]
        public void IsInWhiteList_should_use_separator_char(string testWhiteList, char separator, bool expectedResult)
        {
            string testString = "string1";
            Assert.AreEqual(WhiteListHelper.IsInWhiteList(testString, testWhiteList,whiteListSeparator: separator), expectedResult);
        }
    }
}