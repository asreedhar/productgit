﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Extensions;
using NUnit.Framework;

namespace FirstLook.Common.Core.Web
{
    [TestFixture]
    public class JsonExtensionsTests
    {
        [Test]
        public void SerializeFoo()
        {
            Foo f = new Foo {Bar = 0, Baz = ""};
            string json = f.ToJson();

            Console.WriteLine(json);
            Assert.IsNotEmpty(json);
        }

        [Test]
        public void DeserializeFoo()
        {
            string json = "{\"Bar\":0,\"Baz\":\"\"}";
            Foo f = json.FromJson<Foo>();

            Assert.IsNotNull(f);

            Assert.AreEqual(0, f.Bar);
            Assert.AreEqual("", f.Baz);            
        }

        [Test]
        public void SerializeIFoo()
        {
            IFoo f = new Foo { Bar = 0, Baz = "" };
            string json = f.ToJson();

            Console.WriteLine(json);
            Assert.IsNotEmpty(json);
        }

        [Test]
        public void DeserializeIFoo()
        {
            string json = "{\"Bar\":0,\"Baz\":\"\"}";
            IFoo f = json.FromJson<Foo>();

            Assert.IsNotNull(f);

            Assert.AreEqual(0, f.Bar);
            Assert.AreEqual("", f.Baz);
        }

        [Test]
        public void SerializeEnumeration()
        {
            var foos = new List<Foo> {new Foo {Bar = 1, Baz = "a"}, new Foo {Bar = 2, Baz = "b"}};
            string json = foos.ToJson();

            Console.WriteLine(json);
            Assert.IsNotEmpty(json);
        }

        [Test]
        public void SerializeInterfaceEnumeration()
        {
            var foos = new List<IFoo> { new Foo { Bar = 1, Baz = "a" }, new Foo { Bar = 2, Baz = "b" } };
            string json = foos.ToJson();

            Console.WriteLine(json);
            Assert.IsNotEmpty(json);
        }

        [Test]
        public void DeserializeEnumeration()
        {
            var foos = new List<Foo> { new Foo { Bar = 1, Baz = "a" }, new Foo { Bar = 2, Baz = "b" } };
            string json = foos.ToJson();

            var list = json.FromJson<List<Foo>>();
            Assert.IsTrue(list.Count == 2);

            var foo1 = list[0];
            var foo2 = list[1];

            Assert.AreEqual(1, foo1.Bar);
            Assert.AreEqual("a", foo1.Baz);

            Assert.AreEqual(2, foo2.Bar);
            Assert.AreEqual("b", foo2.Baz);
        }

        [Test]
        public void DeserializeInterfaceEnumeration()
        {
            var foos = new List<IFoo> { new Foo { Bar = 1, Baz = "a" }, new Foo { Bar = 2, Baz = "b" } };
            string json = foos.ToJson();

            var list = json.FromJson<List<Foo>>();
            Assert.IsTrue(list.Count == 2);

            var foo1 = list[0];
            var foo2 = list[1];

            Assert.AreEqual(1, foo1.Bar);
            Assert.AreEqual("a", foo1.Baz);

            Assert.AreEqual(2, foo2.Bar);
            Assert.AreEqual("b", foo2.Baz);
        }

        [Test]
        public void ConversionError()
        {
            string json = "{\"Bar\":0,\"Baz\":\"\"}";
            Biz b = json.FromJson<Biz>();

            Console.WriteLine(b.Hello);
            Console.WriteLine(b.Num);
        }

    }

    public class Foo : IFoo 
    {
        public int Bar { get; set; }
        public string Baz { get; set; }
    }

    public interface IFoo
    {
        int Bar { get; set; }
        string Baz { get; set; }
    }

    public class Biz
    {
        public DateTime Hello { get; set; }
        public float Num { get; set; }
    }
}
