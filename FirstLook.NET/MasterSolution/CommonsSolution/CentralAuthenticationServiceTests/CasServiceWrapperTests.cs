﻿using System;
using CASWrapper;
using NUnit.Framework;

namespace CentralAuthenticationServiceTests
{
    [TestFixture]
    public class CasServiceWrapperTests
    {
        private static string Url = @"https://auth-b.int.firstlook.biz/cas/CentralAuthenticationService";

        [Test]
        public void CanConstruct()
        {
            CasServiceWrapper wrapper = new CasServiceWrapper(Url);
            Assert.IsNotNull(wrapper);
        }

        [Test]
        public void Authenticate()
        {
            CasServiceWrapper wrapper = new CasServiceWrapper(Url);

            // Test with an invalid password.
            bool valid;
            string ticket = wrapper.CreateTicket("breeve", "asdf", out valid);

            Console.WriteLine(valid);
            Console.WriteLine(ticket);

            Assert.IsFalse(valid);
            Assert.IsEmpty(ticket);

            // Now test with a valid password
            ticket = wrapper.CreateTicket("breeve", "N@d@123", out valid);

            Console.WriteLine(valid);
            Console.WriteLine(ticket);

            Assert.IsTrue(valid);
            Assert.IsNotEmpty(ticket);
        }

    }
}
