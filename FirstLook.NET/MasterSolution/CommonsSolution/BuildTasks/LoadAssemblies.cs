using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reflection;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace FirstLook.Common.BuildTasks
{
    public class LoadAssemblies : Task
    {
        private ITaskItem[] assemblyFiles;

        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "Exception is consumed by a logger.")]
        public override bool Execute()
        {
            try
            {
                AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
            }
            catch (Exception e)
            {
                Log.LogErrorFromException(e, true);
            }

            string assemblyName = string.Empty;
            try
            {
                LogAppDomainInfo();

                foreach (ITaskItem item in AssemblyFiles)
                {
                    assemblyName = item.GetMetadata("Identity");

                    FileInfo asm = new FileInfo(assemblyName);

                    FileInfo pdb = new FileInfo(asm.Name.Replace(asm.Extension, ".pdb"));

                    Log.LogMessage(
                        MessageImportance.High, "Try and load assembly {0}.",
                        new object[] {assemblyName});
                    
                    if (asm.Exists)
                    {
                        if (pdb.Exists)
                            LogAssemblyLoad(Assembly.Load(LoadFile(asm), LoadFile(pdb)));
                        else
                            LogAssemblyLoad(Assembly.Load(LoadFile(asm)));
                    }
                    else
                    {
                        Log.LogError("No such assembly {0} {1}", new object[] { assemblyName, asm.FullName });
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                Log.LogError("Failed to load assembly {0}", new object[] {assemblyName});
                Log.LogErrorFromException(e, true);

                return false;
            }
        }

        private void LogAssemblyLoad(Assembly value)
        {
            Log.LogMessage(MessageImportance.High, "Assembly {0} loaded ({1}).", new object[] { value.FullName, value.Location });
        }

        static private byte[] LoadFile(FileInfo file)
        {
            using (FileStream fs = file.OpenRead())
            {
                byte[] buffer = new byte[(int)fs.Length];
                fs.Read(buffer, 0, buffer.Length);
                return buffer;
            }
        }

        private void LogAppDomainInfo()
        {
            Log.LogMessage(
                MessageImportance.High, "AppDomain.CurrentDomain.BaseDirectory: {0}",
                new object[] {AppDomain.CurrentDomain.BaseDirectory});

            Log.LogMessage(
                MessageImportance.High, "AppDomain.CurrentDomain.RelativeSearchPath: {0}",
                new object[] {AppDomain.CurrentDomain.RelativeSearchPath});

            Log.LogMessage(
                MessageImportance.High, "AppDomain.CurrentDomain.SetupInformation.PrivateBinPath: {0}",
                new object[] {AppDomain.CurrentDomain.SetupInformation.PrivateBinPath});

            Log.LogMessage(
                MessageImportance.High,
                "AppDomain.CurrentDomain.(Id, FriendlyName, HashCode) = ('{0}', '{1}', '{2}')",
                new object[]
                    {
                        AppDomain.CurrentDomain.Id, AppDomain.CurrentDomain.FriendlyName,
                        AppDomain.CurrentDomain.GetHashCode()
                    });

            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                Log.LogMessage(MessageImportance.High, "[IsLoaded] Assembly: {0} ({1})",
                               new object[] {assembly.FullName, assembly.Location});
            }
        }

        private Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            //
            // NOTE: In MsBuild 2008, uncommenting these LogMessage calls results in the error -
            // "A call is made on an inactive IBuildEngine interface corresponding to a task that already finished execution"
            // ZB 2-22-2010
            //

//            Log.LogMessage(MessageImportance.High, "AssemblyResolve '{0}'.", new object[] { args.Name });
//            LogAppDomainInfo();

            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (string.Compare(assembly.FullName, args.Name, StringComparison.OrdinalIgnoreCase) == 0)
                {
//                    Log.LogMessage(MessageImportance.High, "AssemblyResolved '{0}' ({1})!", new object[] { assembly.FullName, assembly.Location });

                    return assembly;
                }
            }

            throw new ArgumentException("Could not find the assembly!");
        }

        [Required]
        public ITaskItem[] AssemblyFiles
        {
            get { return assemblyFiles; }
            set { assemblyFiles = value; }
        }
    }
}