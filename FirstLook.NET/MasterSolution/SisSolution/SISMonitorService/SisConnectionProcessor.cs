using System;
using System.Configuration;
using System.Threading;

namespace Firstlook.Sis.SisMonitorService
{
    /// <summary>
    /// SisConnectionProcessor class calls an SisFacade, executes a query that aligns to a SisConnectionProcessorState, and if applicable writes the result using an IPsvWriter.
    /// </summary>
    public class SisConnectionProcessor
    {
        /// <summary>
        /// Tracking variable used to stop the SisConnectionProcessor.
        /// </summary>
        private int stopCount = 0;


        /// <summary>
        /// Adjusts the tracking variable used to stop the SisConnectionProcessor.
        /// </summary>
        public void Stop()
        {
            Interlocked.Increment(ref stopCount);
        }

        /// <summary>
        /// Restarts the tracking variable used to stop the SisConnectionProcessor.
        /// </summary>
        public void Restart()
        {
            if (stopCount == 1)
            {
                Interlocked.Decrement(ref stopCount);
            }
        }

        /// <summary>
        /// Continually processes SisConnectionProcessorCommands until the SisConnectionProcessor's Stop method is invoked.
        /// </summary>
        public void Process()
        {
            //loop always runs until the Stop method is invoked and the stopCount variable in incremented.
            while (stopCount == 0)
            {
                //try catch the whole thing, no error should ever get out of here uncaught
                try
                {
                    //get a new SisConnectionProcessorCommand from the database
                    SisConnectionProcessorCommand command = SisConnectionProcessorCommand.GetSisConnectionProcessorCommand();

                    //catch errors after a query has been obtained so the
                    //query may be able to be retried
                    try
                    {
                        //if the database doesn't return an actual query, don't do anything.
                        if (command.HasSisConnection)
                        {
                            command.Process();
                        }
                    }
                    catch (Exception e)
                    {
                        //have the database handle any remaining uncaught exception
                        SisConnectionProcessorCommand.ProcessException(e, command.SisConnection);
                    }
                    
                    //sleep time between processing SisConnectionProcessorCommands
                    Thread.Sleep(command.Delay * 1000);
                }
                catch(Exception e)
                {
                    //have the database handle any remaining uncaught exception
                    SisConnectionProcessorCommand.ProcessException(e);

                    int delay = int.TryParse(ConfigurationManager.AppSettings["NoQueryDelay"], out delay) ? delay : 60;
                    Thread.Sleep(delay * 1000);
                }
            }
        }
    }
}