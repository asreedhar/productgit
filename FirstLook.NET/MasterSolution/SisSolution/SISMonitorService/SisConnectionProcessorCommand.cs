using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Firstlook.Sis.DomainModel;

namespace Firstlook.Sis.SisMonitorService
{
    /// <summary>
    /// A query processor command representing delay between query attempts, the Query object, and login credentials to pass to the SisFacade.
    /// </summary>
    public class SisConnectionProcessorCommand
    {
        /// <summary>
        /// The delay between query attempts.
        /// </summary>
        private readonly int delay;

        /// <summary>
        /// The sis connection to pass to the SisFacade.
        /// </summary>
        private readonly SisConnection sisConnection;





        /// <summary>
        /// Gets the delay between query attempts.
        /// </summary>
        public int Delay
        {
            get { return delay; }
        }

        /// <summary>
        /// Gets the sis connection to pass to the SisFacade.
        /// </summary>
        public SisConnection SisConnection
        {
            get { return sisConnection; }
        }


        /// <summary>
        /// If the Processor has an sis connection object.
        /// </summary>
        public bool HasSisConnection
        {
            get { return sisConnection != null; }
        }




        /// <summary>
        /// Initializes a new instance of the SisConnectionProcessorCommand class.
        /// </summary>
        /// <param name="delay">The delay between query attempts.</param>
        /// <param name="sisConnection"></param>
        internal SisConnectionProcessorCommand(int delay, SisConnection sisConnection)
        {
            this.delay = delay;
            this.sisConnection = sisConnection;
        }




        /// <summary>
        /// Obtains a new Query whose QueryStatus aligns with the SisConnectionProcessorState passed in.
        /// </summary>
        public static SisConnectionProcessorCommand GetSisConnectionProcessorCommand()
        {
            //result placeholder
            SisConnectionProcessorCommand scpc = null;

            //check database for work items based on SisConnectionProcessorState passed in
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                //Open database connection
                conn.Open();

                //execute the GetQuery stored procedure
                using (SqlCommand comm = new SqlCommand("Runtime.SisConnection#Get", conn))
                {
                    comm.CommandType = CommandType.StoredProcedure;

                    //run stored procedure and get data reader
                    using (SqlDataReader rdr = comm.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        if(rdr.Read())
                        {
                            //set sis connection from reader
                            SisConnection sisconn = new SisConnection(rdr);

                            //set delay between queries, since a query exist the delay is 0
                            int querydelay = int.TryParse(ConfigurationManager.AppSettings["QueryDelay"], out querydelay) ? querydelay : 0;

                            //setup a new SisConnectionProcessorCommand to return
                            scpc = new SisConnectionProcessorCommand(querydelay, sisconn);
                        }
                    }
                }
            }

            //if the SisConnectionProcessorCommand didnt make it out error-free, setup a new SisConnectionProcessorCommand that is empty
            if (scpc == null)
            {
                //set delay between queries, since no query exists the delay is 60
                int noquerydelay = int.TryParse(ConfigurationManager.AppSettings["NoQueryDelay"], out noquerydelay) ? noquerydelay : 60;

                scpc = new SisConnectionProcessorCommand(noquerydelay, null);
            }

            return scpc;
        }

        /// <summary>
        /// Uses a QueryStatusInfo to update the queue with the SisConnectionProcessorCommand's updated QueryStatusInfo.
        /// </summary>
        public void Process()
        {
            int i = 1;
            foreach (ConnectionStatus status in ConnectionStatus.GetConnectionStatus(new Uri(ConfigurationManager.AppSettings["SisUrl"]), sisConnection.Credential))
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
                {
                    //Open database connection
                    conn.Open();

                    //execute the QueryStatusChecked stored procedure
                    using (SqlCommand comm = new SqlCommand("Runtime.SisConnection#SetStatus", conn))
                    {
                        comm.CommandType = CommandType.StoredProcedure;

                        //Add parameter for Transaction ID
                        comm.Parameters.Add(new SqlParameter("@ConnectionId", sisConnection.Id));

                        //Add parameter for Transaction ID
                        comm.Parameters.Add(new SqlParameter("@Number", i));

                        //Add parameter for Transaction ID
                        comm.Parameters.Add(new SqlParameter("@CurrentTime", status.CurrentTime));

                        //Add parameter for Transaction ID
                        comm.Parameters.Add(new SqlParameter("@LastResponse", status.LastResponse));

                        //Add parameter for if the request has been read by Sis
                        comm.Parameters.Add(new SqlParameter("@PollingInterval", status.PollingInterval));

                        //Execute the call
                        comm.ExecuteNonQuery();
                    }
                }
                i++;
            }
        }

        /// <summary>
        /// Sends the details of an SisException related to a Query to the database for storing and processing.
        /// </summary>
        /// <param name="e">The SisException to process.</param>
        /// <param name="connection"></param>
        public static void ProcessException(Exception e, SisConnection connection)
        {
            try
            {
                StringBuilder sb = new StringBuilder();

                sb.AppendLine(e.StackTrace);

                if (e.InnerException != null)
                {
                    int innerReferences = 0;
                    Exception innerException = e.InnerException;

                    while (innerException != null && innerReferences < 50)
                    {
                        sb.AppendLine(innerException.StackTrace);
                        innerException = innerException.InnerException;
                        innerReferences++;
                    }
                }


                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
                {
                    //Open database connection
                    conn.Open();

                    using (SqlCommand comm = new SqlCommand("Runtime.ConnectionException#Insert", conn))
                    {
                        comm.CommandType = CommandType.StoredProcedure;

                        //Add parameter for Query ID, if there is one
                        if (connection == null)
                            comm.Parameters.Add(new SqlParameter("@ConnectionId", DBNull.Value));
                        else
                            comm.Parameters.Add(new SqlParameter("@ConnectionId", connection.Id));

                        //Add parameter for Exception Type
                        comm.Parameters.Add(new SqlParameter("@ErrorType", e.GetType().FullName));

                        //Add parameter for Exception Message
                        comm.Parameters.Add(new SqlParameter("@ErrorMessage", e.Message));

                        //Add parameter for Exception StackTrace
                        comm.Parameters.Add(new SqlParameter("@ErrorDetail", sb.ToString()));

                        comm.ExecuteNonQuery();
                    }
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// Sends the details of an SisException related to a Query to the database for storing and processing.
        /// </summary>
        /// <param name="e">The SisException to process.</param>
        public static void ProcessException(Exception e)
        {
            ProcessException(e, null);
        }
    }
}