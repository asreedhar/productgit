using System.ServiceProcess;
using System.Threading;

namespace Firstlook.Sis.SisMonitorService
{
    /// <summary>
    /// Windows Service which runs 3 parallel threads each with its own QueryProcessor.
    /// Each QueryProcessor aligns to a specific QueryProcessorState, so each QueryProcessor effectively handles only a certain kind of work.
    /// </summary>
    internal partial class SisMonitorService : ServiceBase
    {
        /// <summary>
        /// An SisConnectionProcessor to submit requests to the SisFacade.
        /// </summary>
        private readonly SisConnectionProcessor sisConnectionProcessor;

        /// <summary>
        /// A thread to run the QueryProcessor to submit requests to the SisFacade.
        /// </summary>
        private Thread submitThread;


        /// <summary>
        /// Constructor for the Service.
        /// </summary>
        public SisMonitorService()
        {
            InitializeComponent();
            sisConnectionProcessor = new SisConnectionProcessor();
        }


        /// <summary>
        /// Starts each thread.
        /// </summary>
        protected override void OnStart(string[] args)
        {
            submitThread = new Thread(sisConnectionProcessor.Process);
            sisConnectionProcessor.Restart();
            submitThread.Start();
        }

        /// <summary>
        /// Invokes the Stop method on each QueryProcessor, calling a Join on each thread to ensure all threads terminate before the calling thread.
        /// </summary>
        protected override void OnStop()
        {
            sisConnectionProcessor.Stop();
            submitThread.Join();
        }
    }
}