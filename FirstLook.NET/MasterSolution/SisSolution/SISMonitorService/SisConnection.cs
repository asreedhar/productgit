using System;
using System.Data;
using Firstlook.Sis.DomainModel;

namespace Firstlook.Sis.SisMonitorService
{
    public class SisConnection
    {
        private readonly Credential credential;
        private readonly Guid id;

        public Credential Credential
        {
            get
            {
                return credential;
            }
        }

        public Guid Id
        {
            get
            {
                return id;
            }
        }

        public SisConnection(IDataRecord record)
        {
            credential = new Credential(record["Username"].ToString(), record["Password"].ToString());
            id = new Guid(record["Id"].ToString());
        }
    }
}