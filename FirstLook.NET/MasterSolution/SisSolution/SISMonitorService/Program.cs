using System.ServiceProcess;

namespace Firstlook.Sis.SisMonitorService
{
    /// <summary>
    /// Executable which starts the SisWindowsService.
    /// </summary>
    internal static class Program
    {
        /// <summary>
        /// Static void Main method which starts the SisWindowsService.
        /// </summary>
        private static void Main()
        {
            ServiceBase[] ServicesToRun = new ServiceBase[] { new SisMonitorService() };
            ServiceBase.Run(ServicesToRun);
        }
    }
}