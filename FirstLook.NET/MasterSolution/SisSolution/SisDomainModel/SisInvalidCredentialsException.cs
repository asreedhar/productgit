using System;

namespace Firstlook.Sis.DomainModel
{
    /// <summary>
    /// An exception related to the passing the Sis service invalid login credentials.
    /// </summary>
    public class SisInvalidCredentialsException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the SisInvalidCredentialsException class with a message.
        /// </summary>
        /// <param name="message">The error data related to the exception.</param>
        public SisInvalidCredentialsException(string message) : base(message)
        { }
    }
}