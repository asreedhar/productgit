using Firstlook.Sis.DomainModel.Facade;
using Firstlook.Sis.DomainModel.Serialization;

namespace Firstlook.Sis.DomainModel
{
    public interface IResultProcessor
    {
        void ProcessAccounts(AccountCollection ac, Query Query);
        void ProcessJournals(JournalCollection jc, Query Query);
        void ProcessSchedules(ScheduleCollection sc, Query Query);

        void ProcessResult(IPsvWriter wrtr, Query Query, IPsvSerializable item);

    }
}