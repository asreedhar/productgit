using System;

namespace Firstlook.Sis.DomainModel
{
    /// <summary>
    /// An exception related to an error in the response of an Sis web service call.
    /// </summary>
    public class SisInvalidResponseException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the SisInvalidResponseException class with a message.
        /// </summary>
        /// <param name="message">The error data related to the exception.</param>
        public SisInvalidResponseException(string message) : base(message)
        { }
    }
}