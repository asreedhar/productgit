using System.IO;

namespace Firstlook.Sis.DomainModel.Serialization
{
    /// <summary>
    /// An interface to provide implementation details for the serializing of objects to a Pipe "|" separated value (PSV) format.
    /// </summary>
    public interface IPsvSerializable
    {
        /// <summary>
        /// An interface to provide implementation details for the PSV serializing of the object.
        /// </summary>
        void WritePsv(StreamWriter outputStream);
    }
}