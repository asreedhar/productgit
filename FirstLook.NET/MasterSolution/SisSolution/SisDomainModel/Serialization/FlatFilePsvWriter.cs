using System.IO;

namespace Firstlook.Sis.DomainModel.Serialization
{
    /// <summary>
    /// An implementation of IPsvWriter to write IPsvSerializable items to a file on disk.
    /// </summary>
    public class FlatFilePsvWriter : IPsvWriter
    {
        /// <summary>
        /// Writes the IPsvSerializable item to the path specified in the constructor.
        /// </summary>
        public void Write(string path, IPsvSerializable item)
        {
            StreamWriter sw = null;
            try
            {
                sw = File.CreateText(path);
                item.WritePsv(sw);
                sw.Flush();
                sw.Close();
            }
            catch
            {
                throw new SisPsvWriteException("Error writing to path " + path);
            }
            finally
            {
                if(sw != null)
                {
                    sw.Close();
                }
            }
        }
    }
}