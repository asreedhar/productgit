namespace Firstlook.Sis.DomainModel.Serialization
{
    /// <summary>
    /// An interface to provide implementation details for the writing of IPsvSerializable items.
    /// </summary>
    public interface IPsvWriter
    {
        /// <summary>
        /// A method to provide the acutal write implementation.
        /// </summary>
        void Write(string path, IPsvSerializable item);
    }
}