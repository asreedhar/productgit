using System;
using System.Collections;
using System.Data;
using System.IO;

namespace Firstlook.Sis.DomainModel.Serialization
{
    public class CsvReader : IDataReader
    {
        #region Instance Variables

        //reader properties
        private readonly TextReader _reader = null;
        private bool _readerClosed = false;
        private int _currentRecord = 0;

        //current record properties
        private ArrayList _fields = new ArrayList();
        private int _fieldCount = 0;

        #endregion Instance Variables

        #region Constructors

        public CsvReader(TextReader reader)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader", "can not be null");
            }

            _reader = reader;
        }

        #endregion Constructors

        #region IDataReader Members

        public bool Read()
        {
            string line = _reader.ReadLine();

            if (line != null)
            {
                _currentRecord++;
                _fieldCount = 0;

                int pos = -1;
                _fields = new ArrayList();

                while (pos < line.Length)
                {
                    _fieldCount++;

                    if (pos == line.Length - 1)
                    {
                        pos++;
                        // The last field is empty
                        _fields.Add("");
                        continue;
                    }

                    int fromPos = pos + 1;

                    // Determine if this is a quoted field
                    if (line[fromPos] == '"')
                    {
                        // If we're at the end of the string, let's consider this a field that
                        // only contains the quote
                        if (fromPos == line.Length - 1)
                        {
                            _fields.Add("\"");
                            continue;
                        }

                        // Otherwise, return a string of appropriate length with double quotes collapsed
                        // Note that FSQ returns line.Length if no single quote was found
                        int nextSingleQuote = fromPos;
                        while (++nextSingleQuote < line.Length)
                        {
                            if (line[nextSingleQuote] == '"')
                            {
                                // If this is a double quote, bypass the chars
                                if (nextSingleQuote < line.Length - 1 && line[nextSingleQuote + 1] == '"')
                                {
                                    nextSingleQuote++;
                                    continue;
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }

                        pos = nextSingleQuote + 1;
                        _fields.Add(line.Substring(fromPos + 1, nextSingleQuote - fromPos - 1).Replace("\"\"", "\""));
                        continue;
                    }

                    // The field ends in the next comma or EOL
                    int nextComma = line.IndexOf(',', fromPos);
                    if (nextComma == -1)
                    {
                        pos = line.Length;
                        _fields.Add(line.Substring(fromPos));

                        continue;
                    }
                    else
                    {
                        pos = nextComma;
                        _fields.Add(line.Substring(fromPos, nextComma - fromPos));

                        continue;
                    }
                }
                _currentRecord++;
                return true;
            }
            return false;
        }

        public void Close()
        {
            _reader.Close();
            _readerClosed = true;
        }

        public DataTable GetSchemaTable()
        {
            throw new NotImplementedException();
        }

        public bool NextResult()
        {
            throw new NotImplementedException();
        }

        public int Depth
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsClosed
        {
            get { return _readerClosed; }
        }

        public int RecordsAffected
        {
            get { throw new NotImplementedException(); }
        }

        #endregion

        #region IDataRecord Members

        public string GetName(int i)
        {
            throw new NotImplementedException();
        }

        public string GetDataTypeName(int i)
        {
            throw new NotImplementedException();
        }

        public Type GetFieldType(int i)
        {
            throw new NotImplementedException();
        }

        public object GetValue(int i)
        {
            return _fields[i];
        }

        public int GetValues(object[] values)
        {
            throw new NotImplementedException();
        }

        public int GetOrdinal(string name)
        {
            throw new NotImplementedException();
        }

        public bool GetBoolean(int i)
        {
            return Convert.ToBoolean(_fields[i]);
        }

        public byte GetByte(int i)
        {
            return Convert.ToByte(_fields[i]);
        }

        public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        public char GetChar(int i)
        {
            return Convert.ToChar(_fields[i]);
        }

        public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        public Guid GetGuid(int i)
        {
            throw new NotImplementedException();
        }

        public short GetInt16(int i)
        {
            return Convert.ToInt16(_fields[i]);
        }

        public int GetInt32(int i)
        {
            return Convert.ToInt32(_fields[i]);
        }

        public long GetInt64(int i)
        {
            return Convert.ToInt64(_fields[i]);
        }

        public float GetFloat(int i)
        {
            return Convert.ToSingle(_fields[i]);
        }

        public double GetDouble(int i)
        {
            return Convert.ToDouble(_fields[i]);
        }

        public string GetString(int i)
        {
            return _fields[i].ToString();
        }

        public decimal GetDecimal(int i)
        {
            return Convert.ToDecimal(_fields[i]);
        }

        public DateTime GetDateTime(int i)
        {
            return Convert.ToDateTime(_fields[i]);
        }

        public IDataReader GetData(int i)
        {
            throw new NotImplementedException();
        }

        public bool IsDBNull(int i)
        {
            return _fields[i] == null;
        }

        public int FieldCount
        {
            get { return _fieldCount; }
        }

        public object this[int i]
        {
            get { return _fields[i]; }
        }

        public object this[string name]
        {
            get { return _fields[GetOrdinal(name)]; }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Close();
        }

        #endregion
    }
}