using System.IO;
using System.Xml;
using Firstlook.Sis.DomainModel.Facade;
using NUnit.Framework;

namespace Firstlook.Sis.DomainModel.Serialization.Psv
{
    [TestFixture]
    public class PsvWriterTest
    {
        [Test]
        public void WritePsv()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(new StreamReader("../../TestData/VehicleInfo.xml").ReadToEnd());

            VehicleInfoCollection vi = new VehicleInfoCollection(doc);

            FlatFilePsvWriter ffw = new FlatFilePsvWriter();
            ffw.Write("c:\\example.txt", vi);

            Assert.IsTrue(File.Exists("c:\\example.txt"));

            StreamReader sr = File.OpenText("C:\\example.txt");
            string r = sr.ReadLine();
            sr.Close();

            Assert.AreEqual("N|60801|3VWCT71K06M849156", r);
            File.Delete("c:\\example.txt");

        }
    }
}