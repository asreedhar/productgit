using System;

namespace Firstlook.Sis.DomainModel
{
    /// <summary>
    /// Contains all information related to the status of a request at any given time.
    /// </summary>
    public sealed class QueryStatusInfo
    {
        /// <summary>
        /// QueryStatus enumeration representing current status of request.
        /// </summary>
        private readonly QueryStatus queryStatus;

        /// <summary>
        /// Timestamp of when request was accepted by Sis, default value is DateTime.Min.
        /// </summary>
        private readonly DateTime dateAccepted = DateTime.MinValue;

        /// <summary>
        /// Timestamp of when request was completed by Sis, default value is DateTime.Min.
        /// </summary>
        private readonly DateTime dateCompleted = DateTime.MinValue;




        /// <summary>
        /// Gets the queryStatus enumeration representing current status of request.
        /// </summary>
        public QueryStatus QueryStatus
        {
            get { return queryStatus; }
        }

        /// <summary>
        /// Gets the Timestamp of when request was accepted by Sis, default value is DateTime.Min.
        /// </summary>
        public DateTime DateAccepted
        {
            get { return dateAccepted; }
        }

        /// <summary>
        /// Gets the Timestamp of when request was completed by Sis, default value is DateTime.Min.
        /// </summary>
        public DateTime DateCompleted
        {
            get { return dateCompleted; }
        }





        /// <summary>
        /// Initializes a new instance of the QueryStatusInfo class.
        /// This constructor accepts a QueryStatus representing the current status of the request and a timestamp for when the request was accepted by Sis.
        /// </summary>
        /// <param name="queryStatus">An IDataRecord representing a line item of a csv reader used to populate the object.</param>
        internal QueryStatusInfo(QueryStatus queryStatus)
        {
            this.queryStatus = queryStatus;
        }

        /// <summary>
        /// Initializes a new instance of the Account class.
        /// This constructor accepts a QueryStatus representing the current status of the request, a timestamp for when the request was accepted by Sis, and  a timestamp for when the request was completed by Sis.
        /// </summary>
        /// <param name="queryStatus">An IDataRecord representing a line item of a csv reader used to populate the object.</param>
        /// <param name="accepted">Timestamp of when request was accepted by Sis, default value is DateTime.Min.</param>
        internal QueryStatusInfo(QueryStatus queryStatus, DateTime accepted)
        {
            this.queryStatus = queryStatus;
            dateAccepted = accepted;
        }

        /// <summary>
        /// Initializes a new instance of the Account class.
        /// This constructor accepts a QueryStatus representing the current status of the request.
        /// </summary>
        /// <param name="queryStatus">An IDataRecord representing a line item of a csv reader used to populate the object.</param>
        /// <param name="accepted">Timestamp of when request was accepted by Sis, default value is DateTime.Min.</param>
        /// <param name="completed">Timestamp of when request was completed by Sis, default value is DateTime.Min.</param>
        internal QueryStatusInfo(QueryStatus queryStatus, DateTime accepted, DateTime completed)
        {
            this.queryStatus = queryStatus;
            dateAccepted = accepted;
            dateCompleted = completed;
        }
    }
}