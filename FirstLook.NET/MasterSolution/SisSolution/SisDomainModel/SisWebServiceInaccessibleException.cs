using System;

namespace Firstlook.Sis.DomainModel
{
    /// <summary>
    /// An exception related to the Sis web servicebeing inaccessible.
    /// </summary>
    public class SisWebServiceInaccessibleException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the SisWebServiceInaccessibleException class with a message.
        /// </summary>
        /// <param name="message">The error data related to the exception.</param>
        public SisWebServiceInaccessibleException(string message) : base(message)
        { }
    }
}