using System;
using System.Collections.Generic;
using Firstlook.Sis.DomainModel.Facade;

namespace Firstlook.Sis.DomainModel
{
    /// <summary>
    /// Represents the status of a connection to Sis.
    /// </summary>
    public sealed class ConnectionStatus
    {
        /// <summary>
        /// The polling interval, in seconds.
        /// </summary>
        private readonly int pollingInterval;

        /// <summary>
        /// The current time of the DMS.
        /// </summary>
        private readonly DateTime currentTime;

        /// <summary>
        /// The timestamp of last response from Sis.
        /// </summary>
        private readonly DateTime lastResponse;




        /// <summary>
        /// Gets the polling interval, in seconds.
        /// </summary>
        public int PollingInterval
        {
            get { return pollingInterval; }
        }

        /// <summary>
        /// The current time of the DMS.
        /// </summary>
        public DateTime CurrentTime
        {
            get { return currentTime; }
        }

        /// <summary>
        /// Gets the timestamp of last response from Sis.
        /// </summary>
        public DateTime LastResponse
        {
            get { return lastResponse; }
        }



        /// <summary>
        /// Gets whether the connection with Sis is alive.
        /// </summary>
        public static bool IsAlive(List<ConnectionStatus> statuses)
        {
            foreach (ConnectionStatus status in statuses)
            {
                if ((status.CurrentTime.Subtract(status.LastResponse).TotalSeconds / status.PollingInterval) < 3)
                {
                    return true;
                }
            }
            return false;
        }




        /// <summary>
        /// Initializes a new instance of the Account class.
        /// This constructor accepts an IDataRecord representing a line item of a csv reader used to populate the object
        /// </summary>
        /// <param name="pollingInterval">The polling interval, in seconds, for Sis.
        /// <param name="lastResponse">Sis's last response timestamp.
        internal ConnectionStatus(int pollingInterval, DateTime currentTime, DateTime lastResponse)
        {
            this.pollingInterval = pollingInterval;
            this.currentTime = currentTime;
            this.lastResponse = lastResponse;
        }


        /// <summary>
        /// Checks connectivity to Sis and returns a new instance of the ConnectionStatus class.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="credential">The login credentials to send Sis.</param>
        public static List<ConnectionStatus> GetConnectionStatus(Uri url, Credential credential)
        {
            try
            {
                SisFacade facade = new SisFacade(url);
                List<ConnectionStatus> status = facade.GetConnectionStatus(credential);
                return status;
            }
            catch(Exception e)
            {
                //Sis web service is down
                if (e.GetType().Equals(typeof(System.Net.WebException)))
                {
                    throw new SisWebServiceInaccessibleException("The Sis web service is inaccessible.");
                }
                //invalid credential passed
                else if (e.GetType().Equals(typeof(System.Web.Services.Protocols.SoapException)) && e.Message.Contains("CompanyNotFoundException"))
                {
                    throw new SisInvalidCredentialsException("The credentials you are using are invalid.");
                }
                //unknown error
                else
                {
                    throw;
                }
            }
        }
    }
}