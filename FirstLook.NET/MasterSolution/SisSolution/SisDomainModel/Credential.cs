namespace Firstlook.Sis.DomainModel
{
    /// <summary>
    /// Represents a login credential for Sis [username and password].
    /// </summary>
    public sealed class Credential
    {
        /// <summary>
        /// The username.
        /// </summary>
        private readonly string username;

        /// <summary>
        /// The password.
        /// </summary>
        private readonly string password;




        /// <summary>
        /// Gets the username.
        /// </summary>
        public string Username
        {
            get { return username; }
        }

        /// <summary>
        /// Gets the password.
        /// </summary>
        public string Password
        {
            get { return password; }
        }




        /// <summary>
        /// Initializes a new instance of the Credential class to use to send login information to Sis.
        /// This constructor accepts a username and password.
        /// </summary>
        /// <param name="username">The username.
        /// <param name="password">Sis's password.
        public Credential(string username, string password)
        {
            this.username = username;
            this.password = password;
        }
    }
}