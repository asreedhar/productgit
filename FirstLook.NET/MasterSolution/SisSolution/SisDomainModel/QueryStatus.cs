namespace Firstlook.Sis.DomainModel
{
    /// <summary>
    /// Represents all possible request statuses by the SisFacade.
    /// </summary>
    public enum QueryStatus
    {
        /// <summary>
        /// The Request has not been submitted.
        /// </summary>
        NotSubmitted,

        /// <summary>
        /// The Request has been submitted.
        /// </summary>
        Submitted,

        /// <summary>
        /// The Request has been accepted by Sis.
        /// </summary>
        Accepted,

        /// <summary>
        /// The Request has been processed and completed by Sis.
        /// </summary>
        Completed,

        /// <summary>
        /// The Request has errored.
        /// </summary>
        Error

    }
}