using System;

namespace Firstlook.Sis.DomainModel
{
    /// <summary>
    /// An exception related to passing the Sis service a parameter of the wrong type, or otherwise invalid.
    /// </summary>
    public class SisQueryParameterInvalidException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the SisQueryParameterInvalidException class with a message.
        /// </summary>
        /// <param name="message">The error data related to the exception.</param>
        public SisQueryParameterInvalidException(string message) : base(message)
        { }
    }
}