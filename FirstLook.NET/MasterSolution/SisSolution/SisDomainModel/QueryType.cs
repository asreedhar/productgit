namespace Firstlook.Sis.DomainModel
{
    /// <summary>
    /// Represents all possible request types accepted by the SisFacade.
    /// </summary>
    public enum QueryType
    {
        /// <summary>
        /// Get all Accounting Accounts.
        /// </summary>
        GetAccounts,

        /// <summary>
        /// Get all Accounting Journals.
        /// </summary>
        GetJournals,

        /// <summary>
        /// Get all Accounting Schedules.
        /// </summary>
        GetSchedules,

        /// <summary>
        /// Get all Accounting Journal Entries.
        /// </summary>
        GetJournalDetail,

        /// <summary>
        /// Get a specific Vehicle.
        /// </summary>
        GetVehicle,

        /// <summary>
        /// Get a VehicleList, a lightweight list of vehicles.
        /// </summary>
        GetVehicleList,

        /// <summary>
        /// Get A VehicleList, a full list of vehicles.
        /// </summary>
        GetVehicles,

        /// <summary>
        /// Get a specific Deal.
        /// </summary>
        GetDeal,

        /// <summary>
        /// Get all Deals.
        /// </summary>
        GetDeals,

        /// <summary>
        /// Get the Used Car Schedule.
        /// </summary>
        GetSchedule_Used,

        /// <summary>
        /// Get sold vehicles.
        /// </summary>
        GetSoldVehicles

    }
}