using System;
using System.Collections.Generic;
using System.Globalization;
using Firstlook.Sis.DomainModel.Facade;
using Firstlook.Sis.DomainModel.Serialization;

namespace Firstlook.Sis.DomainModel
{
    /// <summary>
    /// Container for a query to send to Sis along with parameters for the query and the receipt and status of the query.
    /// </summary>
    public class Query
    {
        /// <summary>
        /// The SisFacade to use for this query.
        /// </summary>
        private readonly SisFacade facade;

        /// <summary>
        /// The Guid of this query.
        /// </summary>
        private readonly Guid id;

        /// <summary>
        /// The current QueryType object associated with this query.  This determines which Sis call will be made.
        /// </summary>
        private readonly QueryType queryType;

        /// <summary>
        /// The current Receipt object associated with this query, which holds a unique identifier that can later be used to check on this query with Sis.
        /// </summary>
        private readonly Receipt receipt;

        /// <summary>
        /// The parameters associated with this query.
        /// </summary>
        private readonly QueryParameterCollection parameters;




        /// <summary>
        /// Gets the Guid of this query.
        /// </summary>
        public Guid ID
        {
            get { return id; }
        }

        /// <summary>
        /// Gets the current QueryType object associated with this query.  This determines which Sis call will be made.
        /// </summary>
        public QueryType QueryType
        {
            get { return queryType; }
        }

        /// <summary>
        /// Gets the current Receipt object associated with this query, which holds a unique identifier that can later be used to check on this query with Sis.
        /// </summary>
        public QueryParameterCollection Parameters
        {
            get { return parameters; }
        }

        /// <summary>
        /// Gets the parameters associated with this query.
        /// </summary>
        public Receipt Receipt
        {
            get { return receipt; }
        }




        /// <summary>
        /// Initializes a new instance of the Query class.
        /// </summary>
        /// <param name="url">the Url to use to connect to Sis.</param>
        /// <param name="id">The Guid of this query.</param>
        /// <param name="queryType">The type of request.</param>
        /// <param name="receipt">The request's current Receipt.</param>
        /// <param name="parameters">The parameters associated with the query.</param>
        public Query(Uri url, Guid id, QueryType queryType, Receipt receipt, QueryParameterCollection parameters)
        {
            facade = new SisFacade(url);

            this.id = id;
            this.queryType = queryType;
            this.receipt = receipt;
            this.parameters = parameters;
        }





        /// <summary>
        /// Submits a query to Sis and returns an instance of the Receipt class.
        /// </summary>
        /// <param name="credential">The login credentials to send Sis.</param>
        public Receipt Submit(Credential credential)
        {
            Receipt rec = null;
            try
            {
                switch (queryType)
                {
                    case QueryType.GetAccounts:
                        rec = facade.GetAccounts(credential);
                        break;
                    case QueryType.GetDeal:
                        if (Parameters["DealNumber"] == null)
                        {
                            throw new SisQueryParameterMissingException("DealNumber.");
                        }
                        rec = facade.GetDeal(credential, Parameters["DealNumber"].ToInt32(CultureInfo.InvariantCulture));
                        break;
                    case QueryType.GetJournalDetail:
                        List<string> journals = new List<string>();
                        foreach (QueryParameter p in Parameters)
                        {
                            if (p.Name.Equals("journal", StringComparison.InvariantCultureIgnoreCase))
                            {
                                try
                                {
                                    journals.Add(p.ToString(CultureInfo.InvariantCulture));
                                }
                                catch
                                {
                                    throw new SisQueryParameterInvalidException("Invalid parameter type, must be integer - Journal");
                                }
                            }
                        }
                        List<string> accounts = new List<string>();
                        foreach (QueryParameter p in Parameters)
                        {
                            if (p.Name.Equals("AccountId", StringComparison.InvariantCultureIgnoreCase))
                            {
                                try
                                {
                                    accounts.Add(p.ToString(CultureInfo.InvariantCulture));
                                }
                                catch
                                {
                                    throw new SisQueryParameterInvalidException("Invalid parameter type, must be integer. - AccountId");
                                }
                            }
                        }
                        if (Parameters["StartDate"] == null)
                        {
                            throw new SisQueryParameterMissingException("StartDate");
                        }
                        if (Parameters["EndDate"] == null)
                        {
                            throw new SisQueryParameterMissingException("EndDate");
                        }
                        if (Parameters["StartDate"].ToDateTime(CultureInfo.InvariantCulture) > Parameters["EndDate"].ToDateTime(CultureInfo.InvariantCulture))
                        {
                            throw new SisQueryParameterOutOfRangeException("Start date cannot be greater than end date.");
                        }
                        rec = facade.GetJournalEntries(credential, Parameters["StartDate"].ToDateTime(CultureInfo.InvariantCulture), Parameters["EndDate"].ToDateTime(CultureInfo.InvariantCulture), journals, accounts);
                        break;
                    case QueryType.GetSchedule_Used:
                        rec = facade.GetUsedCarSchedule(credential);
                        break;
                    case QueryType.GetJournals:
                        rec = facade.GetJournals(credential);
                        break;
                    case QueryType.GetSchedules:
                        rec = facade.GetSchedules(credential);
                        break;
                    case QueryType.GetDeals:
                        if (Parameters["StartDate"] == null)
                        {
                            throw new SisQueryParameterMissingException("StartDate.");
                        }
                        if (Parameters["EndDate"] == null)
                        {
                            throw new SisQueryParameterMissingException("EndDate.");
                        }
                        if (Parameters["StartDate"].ToDateTime(CultureInfo.InvariantCulture) > Parameters["EndDate"].ToDateTime(CultureInfo.InvariantCulture))
                        {
                            throw new SisQueryParameterOutOfRangeException("Start date cannot be greater than end date.");
                        }
                        rec = facade.GetDeals(credential, Parameters["StartDate"].ToDateTime(CultureInfo.InvariantCulture), Parameters["EndDate"].ToDateTime(CultureInfo.InvariantCulture));
                        break;
                    case QueryType.GetSoldVehicles:
                        if (Parameters["StartDate"] == null)
                        {
                            throw new SisQueryParameterMissingException("StartDate.");
                        }
                        if (Parameters["EndDate"] == null)
                        {
                            throw new SisQueryParameterMissingException("EndDate.");
                        }
                        if (Parameters["StartDate"].ToDateTime(CultureInfo.InvariantCulture) > Parameters["EndDate"].ToDateTime(CultureInfo.InvariantCulture))
                        {
                            throw new SisQueryParameterOutOfRangeException("Start date cannot be greater than end date.");
                        }
                        rec = facade.GetSoldVehicles(credential, Parameters["StartDate"].ToDateTime(CultureInfo.InvariantCulture), Parameters["EndDate"].ToDateTime(CultureInfo.InvariantCulture));
                        break;
                    case QueryType.GetVehicles:
                        string VehiclesNewUsed = Parameters["New_Used"] == null ? "" : Parameters["New_Used"].ToString(CultureInfo.InvariantCulture);
                        rec = facade.GetVehicles(credential, VehiclesNewUsed);
                        break;
                    case QueryType.GetVehicle:
                        if (Parameters["StockNumber"] == null)
                        {
                            throw new SisQueryParameterMissingException("StockNumber");
                        }
                        rec = facade.GetVehicle(credential, Parameters["StockNumber"].ToString(CultureInfo.InvariantCulture));
                        break;
                    case QueryType.GetVehicleList:
                        string VehicleListNewUsed = Parameters["New_Used"] == null ? "" : Parameters["New_Used"].ToString(CultureInfo.InvariantCulture);
                        rec = facade.GetVehicleList(credential, VehicleListNewUsed);
                        break;
                }
            }
            catch(Exception e)
            {
                HandleException(e);
            }
            return rec;
        }

        /// <summary>
        /// Calls Sis to check the status of a receipt and returns an instance of the QueryStatusInfo class.
        /// </summary>
        /// <param name="credential">The login credentials to send Sis.</param>
        /// <param name="rec">The receipt to check.</param>
        public QueryStatusInfo CheckStatus(Credential credential, Receipt rec)
        {
            QueryStatusInfo info = null;
            try
            {
                info = facade.CheckStatus(credential, rec);
            }
            catch (Exception e)
            {
                HandleException(e);
            }
            return info;
        }

        /// <summary>
        /// Calls Sis for a response to a receipt and returns an instance of the IPsvSerializable interface.
        /// </summary>
        /// <param name="credential">The login credentials to send Sis.</param>
        /// <param name="rec">The receipt to check.</param>
        public IPsvSerializable GetResponse(Credential credential, Receipt rec)
        {
            IPsvSerializable resp = null;
            try
            {
                resp = facade.GetResponse(credential, rec, queryType);
            }
            catch (Exception e)
            {
                HandleException(e);
            }
            return resp;
        }




        /// <summary>
        /// Handles exceptions for each call to the Sis Facade to rethrow Sis application specific exceptions if
        /// one exists or rethrow the original exception.
        /// </summary>
        /// <param name="e">The exception to handle.</param>
        private static void HandleException(Exception e)
        {
            //Sis web service is down
            if(e.GetType().Equals(typeof(System.Net.WebException)))
            {
                throw new SisWebServiceInaccessibleException("The Sis web service is inaccessible.");
            }
                //invalid credential passed
            else if (e.GetType().Equals(typeof(System.Web.Services.Protocols.SoapException)) && e.Message.Contains("CompanyNotFoundException"))
            {
                throw new SisInvalidCredentialsException("The credentials you are using are invalid.");
            }
                //unknown error
            else
            {
                throw e;
            }
        }
    }
}