using System;

namespace Firstlook.Sis.DomainModel
{
    /// <summary>
    /// An exception related to the failure to pass the Sis service an expected parameter.
    /// </summary>
    public class SisQueryParameterMissingException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the SisQueryParameterMissingException class with a message.
        /// </summary>
        /// <param name="parameter">The required parameter missing.</param>
        public SisQueryParameterMissingException(string parameter) : base("Missing Required Parameter - " + parameter)
        { }
    }
}