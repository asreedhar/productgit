using System;

namespace Firstlook.Sis.DomainModel
{
    /// <summary>
    /// Represents a single Parameter for use in a QueryParameterCollection to use in conjunction with a query.
    /// </summary>
    public class QueryParameter : IConvertible
    {
        /// <summary>
        /// The name of the query parameter.
        /// </summary>
        private readonly string name;

        /// <summary>
        /// The value of the query parameter.
        /// </summary>
        private readonly object value;


        /// <summary>
        /// Gets the name of the query parameter.
        /// </summary>
        public string Name
        {
            get { return name; }
        }

        /// <summary>
        /// Gets the value of the query parameter.
        /// </summary>
        public object Value
        {
            get { return value; }
        }


        /// <summary>
        /// Initializes a new instance of the QueryParameter class.
        /// </summary>
        /// <param name="name">The name of the query parameter.</param>
        /// <param name="value">The value of the query parameter.</param>
        public QueryParameter(string name, object value)
        {
            this.name = name;
            this.value = value;
        }

        #region IConvertible Members

        /// <summary>
        /// Returns the value's TypeCode.
        /// </summary>
        public TypeCode GetTypeCode()
        {
            return Type.GetTypeCode(value.GetType());
        }

        /// <summary>
        /// Returns the value as a boolean.
        /// </summary>
        /// <param name="provider">The format to use.</param>
        public bool ToBoolean(IFormatProvider provider)
        {
            return Convert.ToBoolean(value);
        }

        /// <summary>
        /// Returns the value as a byte.
        /// </summary>
        /// <param name="provider">The format to use.</param>
        public byte ToByte(IFormatProvider provider)
        {
            return Convert.ToByte(value);
        }

        /// <summary>
        /// Returns the value as a char.
        /// </summary>
        /// <param name="provider">The format to use.</param>
        public char ToChar(IFormatProvider provider)
        {
            return Convert.ToChar(value);
        }

        /// <summary>
        /// Returns the value as a DateTime.
        /// </summary>
        /// <param name="provider">The format to use.</param>
        public DateTime ToDateTime(IFormatProvider provider)
        {
            return Convert.ToDateTime(value);
        }

        /// <summary>
        /// Returns the value as a decimal.
        /// </summary>
        /// <param name="provider">The format to use.</param>
        public decimal ToDecimal(IFormatProvider provider)
        {
            return Convert.ToDecimal(value);
        }

        /// <summary>
        /// Returns the value as a double.
        /// </summary>
        /// <param name="provider">The format to use.</param>
        public double ToDouble(IFormatProvider provider)
        {
            return Convert.ToDouble(value);
        }

        /// <summary>
        /// Returns the value as a 16 bit integer.
        /// </summary>
        /// <param name="provider">The format to use.</param>
        public short ToInt16(IFormatProvider provider)
        {
            return Convert.ToInt16(value);
        }

        /// <summary>
        /// Returns the value as a 32 bit integer.
        /// </summary>
        /// <param name="provider">The format to use.</param>
        public int ToInt32(IFormatProvider provider)
        {
            return Convert.ToInt32(value);
        }

        /// <summary>
        /// Returns the value as a 64 bit integer.
        /// </summary>
        /// <param name="provider">The format to use.</param>
        public long ToInt64(IFormatProvider provider)
        {
            return Convert.ToInt64(value);
        }

        /// <summary>
        /// Returns the value as a small byte.
        /// </summary>
        /// <param name="provider">The format to use.</param>
        public sbyte ToSByte(IFormatProvider provider)
        {
            return Convert.ToSByte(value);
        }

        /// <summary>
        /// Returns the value as a single.
        /// </summary>
        /// <param name="provider">The format to use.</param>
        public float ToSingle(IFormatProvider provider)
        {
            return Convert.ToSingle(value);
        }

        /// <summary>
        /// Returns the value as a string.
        /// </summary>
        /// <param name="provider">The format to use.</param>
        public string ToString(IFormatProvider provider)
        {
            return Convert.ToString(value);
        }

        /// <summary>
        /// Returns the value as the Type specified.
        /// </summary>
        /// <param name="conversionType">The Type to which to convert the value.</param>
        /// <param name="provider">The format to use.</param>
        public object ToType(Type conversionType, IFormatProvider provider)
        {
            return Convert.ChangeType(value, conversionType, provider);
        }

        /// <summary>
        /// Returns the value as an unsigned 16 but integer.
        /// </summary>
        /// <param name="provider">The format to use.</param>
        public ushort ToUInt16(IFormatProvider provider)
        {
            return Convert.ToUInt16(value);
        }

        /// <summary>
        /// Returns the value as an unsigned 32 but integer.
        /// </summary>
        /// <param name="provider">The format to use.</param>
        public uint ToUInt32(IFormatProvider provider)
        {
            return Convert.ToUInt32(value);
        }

        /// <summary>
        /// Returns the value as an unsigned 64 but integer.
        /// </summary>
        /// <param name="provider">The format to use.</param>
        public ulong ToUInt64(IFormatProvider provider)
        {
            return Convert.ToUInt64(value);
        }

        #endregion
    }
}