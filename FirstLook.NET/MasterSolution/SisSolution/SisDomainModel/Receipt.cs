using System.Xml;

namespace Firstlook.Sis.DomainModel
{
    /// <summary>
    /// A receipt from an Sis call that wraps the identifier used by Sis to check the status and obtain the response for the reuqest that was submitted.
    /// </summary>
    public sealed class Receipt
    {
        /// <summary>
        /// The identifier used by Sis to check the status and obtain the response for the reuqest that was submitted.
        /// </summary>
        private readonly int dmsId;

        /// <summary>
        /// The xml request sent to Sis.
        /// </summary>
        private readonly XmlDocument requestXml;




        /// <summary>
        /// Gets the identifier used by Sis to check the status and obtain the response for the reuqest that was submitted.
        /// </summary>
        public int DmsId
        {
            get { return dmsId; }
        }

        /// <summary>
        /// The xml request sent to Sis.
        /// </summary>
        public XmlDocument RequestXml
        {
            get { return requestXml; }
        }


        /// <summary>
        /// Constructs a receipt using the identifier used by Sis to check the status and obtain the response for the reuqest that was submitted.
        /// </summary>
        /// <param name="dmsId">The identifier used by Sis to check the status and obtain the response for the reuqest that was submitted.</param>
        /// <param name="requestXml">The xml request sent to Sis.</param>
        public Receipt(int dmsId, XmlDocument requestXml)
        {
            this.dmsId = dmsId;
            this.requestXml = requestXml;
        }
    }
}