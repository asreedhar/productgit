using System;

namespace Firstlook.Sis.DomainModel
{
    /// <summary>
    /// An exception related to an error writing a Psv file.
    /// </summary>
    public class SisPsvWriteException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the SisPsvWriteException class with a message.
        /// </summary>
        /// <param name="message">The error data related to the exception.</param>
        public SisPsvWriteException(string message) : base(message)
        { }
    }
}