using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.CompilerServices;
using Firstlook.Sis.DomainModel.Facade;
using Firstlook.Sis.DomainModel.Serialization;

[assembly: InternalsVisibleTo("Firstlook.Sis.DomainModelTest")]

namespace Firstlook.Sis.DomainModel.Facade
{
    /// <summary>
    /// Represents a collection of Schedules.
    /// </summary>
    public sealed class ScheduleCollection : Collection<Schedule>, IPsvSerializable
    {
        private readonly Dictionary<int, int> VersionPropertyCounts;

        /// <summary>
        /// Initializes a new instance of the SheduleEntryCollection class.
        /// This constructor Accepts an IDataReader to initialize a Schedule object for each IDataRecord of the reader.
        /// </summary>
        /// <param name="result">A CsvResult object with an IDataReader representing a csv reader with a record for each Schedule object to initialize.</param>
        internal ScheduleCollection(SisCsvResult result)
        {
            VersionPropertyCounts = new Dictionary<int, int>();
            VersionPropertyCounts.Add(3, 6);
            VersionPropertyCounts.Add(4, 6);
            VersionPropertyCounts.Add(5, 6);
            VersionPropertyCounts.Add(6, 6);
            VersionPropertyCounts.Add(7, 6);


            while (result.Data.Read())
            {
                //the reader must have at least as many fields as the Schedule object has properties
                if (result.Data.FieldCount >= VersionPropertyCounts[result.FirstlookVersion])
                {
                    Add(new Schedule(result.Data, VersionPropertyCounts[result.FirstlookVersion] - 1));
                }
            }
        }




        /// <summary>
        /// Writes the ScheduleCollection's Schedule items in a Pipe "|" separated value format, following the IPsvSerializable interface.
        /// </summary>
        /// <param name="outputStream">The output stream to which to write.</param>
        public void WritePsv(StreamWriter outputStream)
        {
            foreach (Schedule s in Items)
            {
                s.WritePsv(outputStream);
            }
        }
    }
}