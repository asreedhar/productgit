using System;
using System.Data;
using System.IO;
using System.Runtime.CompilerServices;
using System.Xml;
using Firstlook.Sis.DomainModel.Serialization;

//Used to allow this class to be used in the Sis.DomainModel_Test project
[assembly: InternalsVisibleTo("Firstlook.Sis.DomainModelTest")]
namespace Firstlook.Sis.DomainModel.Facade
{
    /// <summary>
    /// Wrapper class over Sis result set.
    /// </summary>
    internal sealed class SisCsvResult : SisResult
    {
        /// <summary>
        /// Data from response.
        /// </summary>
        private readonly IDataReader data;

        /// <summary>
        /// Gets the Data from response.
        /// </summary>
        public IDataReader Data
        {
            get
            {
                return data;
            }
        }


        
        /// <summary>
        /// Initializes a new instance of the SisResult class.
        /// This constructor accepts an XmlDocument representing the response of the Sis web service call.
        /// </summary>
        public SisCsvResult(XmlNode response) : base(response)
        {
            data = new CsvReader(GetCDataReader(response));
        }


        /// <summary>
        /// Private method to get the CData section of an Xml response.
        /// </summary>
        /// <param name="doc"></param>
        private static TextReader GetCDataReader(XmlNode doc)
        {
            using (XmlTextReader reader = new XmlTextReader(new StringReader(doc.OuterXml)))
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.CDATA)
                    {
                        return new StringReader(reader.Value.TrimEnd(' '));
                    }
                }
            }
            return new StringReader(String.Empty);
        }
    }
}
