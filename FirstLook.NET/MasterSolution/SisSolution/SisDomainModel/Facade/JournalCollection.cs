using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.CompilerServices;
using Firstlook.Sis.DomainModel.Facade;
using Firstlook.Sis.DomainModel.Serialization;

//Used to allow this class to be used in the Sis.DomainModel_Test project
[assembly: InternalsVisibleTo("Firstlook.Sis.DomainModelTest")]
namespace Firstlook.Sis.DomainModel.Facade
{
    /// <summary>
    /// Represents a collection of Journals.
    /// </summary>
    public sealed class JournalCollection : Collection<Journal>, IPsvSerializable
    {
        private readonly Dictionary<int, int> VersionPropertyCounts;

        /// <summary>
        /// Initializes a new instance of the JournalCollection class.
        /// This constructor Accepts an IDataReader to initialize a Journal object for each IDataRecord of the reader.
        /// </summary>
        /// <param name="result">A CsvResult object with an IDataReader representing a csv reader with a record for each Journal object to initialize.</param>
        internal JournalCollection(SisCsvResult result)
        {
            VersionPropertyCounts = new Dictionary<int, int>();
            VersionPropertyCounts.Add(3, 4);
            VersionPropertyCounts.Add(4, 4);
            VersionPropertyCounts.Add(5, 4);
            VersionPropertyCounts.Add(6, 4);
            VersionPropertyCounts.Add(7, 4);

            while (result.Data.Read())
            {
                //the reader must have at least as many fields as the Journal object has properties
                if (result.Data.FieldCount >= VersionPropertyCounts[result.FirstlookVersion])
                {
                    Add(new Journal(result.Data, VersionPropertyCounts[result.FirstlookVersion] - 1));
                }
            }
        }




        /// <summary>
        /// Writes the JournalCollection's Journal items in a Pipe "|" separated value format, following the IPsvSerializable interface.
        /// </summary>
        /// <param name="outputStream">The output stream to which to write.</param>
        public void WritePsv(StreamWriter outputStream)
        {
            foreach (Journal j in Items)
            {
                j.WritePsv(outputStream);
            }
        }
    }
}