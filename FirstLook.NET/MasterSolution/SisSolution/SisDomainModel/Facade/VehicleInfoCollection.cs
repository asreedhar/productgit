using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Xml;
using Firstlook.Sis.DomainModel.Facade;
using Firstlook.Sis.DomainModel.Serialization;

[assembly: InternalsVisibleTo("Firstlook.Sis.DomainModelTest")]

namespace Firstlook.Sis.DomainModel.Facade
{
    /// <summary>
    /// Represents a collection of VehicleInfos.
    /// </summary>
    internal sealed class VehicleInfoCollection : Collection<VehicleInfo>, IPsvSerializable
    {
        private readonly Dictionary<int, int> VersionPropertyCounts;

        /// <summary>
        /// Initializes a new instance of the VehicleInfoCollection class.
        /// This constructor Accepts an XmlDocument to initialize a VehicleInfo object for each node of the document representing a VehicleInfo.
        /// </summary>
        /// <param name="result">A CsvResult object with an XmlDocument with one or more nodes representing a VehicleInfo object to initialize using XmlDeserialization.</param>
        internal VehicleInfoCollection(SisXmlResult result)
        {
            VersionPropertyCounts = new Dictionary<int, int>();
            VersionPropertyCounts.Add(3, 3);
            VersionPropertyCounts.Add(4, 3);
            VersionPropertyCounts.Add(5, 3);
            VersionPropertyCounts.Add(6, 3);
            VersionPropertyCounts.Add(7, 3);

            XmlNodeList nl = result.Data.SelectNodes("/DMS_TRANSACTION/DMS_VEH");
            if (nl != null)
            {
                foreach (XmlNode node in nl)
                {
                    if (!(node is XmlElement))
                    {
                        continue;
                    }
                    if (node.Attributes != null && node.Attributes.Count >= VersionPropertyCounts[result.FirstlookVersion])
                    {
                        Add(new VehicleInfo(node, VersionPropertyCounts[result.FirstlookVersion] - 1));
                    }
                }
            }
        }




        /// <summary>
        /// Writes the VehicleInfoCollection's VehicleInfo items in a Pipe "|" separated value format, following the IPsvSerializable interface.
        /// </summary>
        /// <param name="outputStream">The output stream to which to write.</param>
        public void WritePsv(StreamWriter outputStream)
        {
            foreach (VehicleInfo v in Items)
            {
                v.WritePsv(outputStream);
            }
        }
    }
}