using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Xml;
using Firstlook.Sis.DomainModel.Serialization;

//Used to allow this class to be used in the Sis.DomainModel_Test project
[assembly: InternalsVisibleTo("Firstlook.Sis.DomainModelTest")]
namespace Firstlook.Sis.DomainModel.Facade
{
    /// <summary>
    /// Represents a VehicleInfo as returned from Sis, a lightweight info object containing basic vehicle information.
    /// XmlDeserialization is used to contruct a VehicleInfo from an XmlDocument.
    /// </summary>
    internal sealed class VehicleInfo : IPsvSerializable
    {
        /// <summary>
        /// If the Vehicle is new.
        /// </summary>
        private string isNew;

        /// <summary>
        /// The Vehicle's stock number.
        /// </summary>
        private string stockNumber;

        /// <summary>
        /// The Vehicle's vin.
        /// </summary>
        private string vin;




        /// <summary>
        /// Gets or sets if the Vehicle is new.
        /// </summary>
        public string IsNew
        {
            get { return isNew; }
            set { isNew = value; }
        }

        /// <summary>
        /// Gets or sets the Vehicle's stock number.
        /// </summary>
        public string StockNumber
        {
            get { return stockNumber; }
            set { stockNumber = value; }
        }

        /// <summary>
        /// Gets or sets the Vehicle's vin.
        /// </summary>
        public string VIN
        {
            get { return vin; }
            set { vin = value; }
        }




        /// <summary>
        /// Initializes a new instance of the VehicleInfo class.
        /// This constructor accepts an XmlNode representing a VehicleInfo object to initialize.
        /// </summary>
        /// <param name="node">An XmlNode representing a VehicleInfo object to initialize.</param>
        /// <param name="maxIndex"></param>
        internal VehicleInfo(XmlNode node, int maxIndex)
        {
            isNew = node.Attributes["NEW"] != null ? node.Attributes["NEW"].InnerText : "";
            stockNumber = node.Attributes["STOCK_NUMBER"] != null ? node.Attributes["STOCK_NUMBER"].InnerText : "";
            vin = node.Attributes["VIN"] != null ? node.Attributes["VIN"].InnerText : "";
        }




        /// <summary>
        /// Writes the VehicleInfo in a Pipe "|" separated value format, following the IPsvSerializable interface.
        /// Writes all values in alphabetical order by property name.
        /// </summary>
        /// <param name="outputStream">The output stream to which to write.</param>
        public void WritePsv(StreamWriter outputStream)
        {
            StringBuilder b = new StringBuilder();

            b.Append(isNew);
            b.Append("|");

            b.Append(stockNumber);
            b.Append("|");

            b.Append(vin);

            outputStream.WriteLine(b.ToString());
        }
    }
}