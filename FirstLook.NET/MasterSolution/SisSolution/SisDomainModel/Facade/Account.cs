using System.Data;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using Firstlook.Sis.DomainModel.Serialization;

//Used to allow this class to be used in the Sis.DomainModel_Test project
[assembly: InternalsVisibleTo("Firstlook.Sis.DomainModelTest")]
namespace Firstlook.Sis.DomainModel.Facade
{
    /// <summary>
    /// An accounting account as returned from Sis, contained in the dealer's DMS.
    /// </summary>
    public sealed class Account : IPsvSerializable
    {
        /// <summary>
        /// The account number.
        /// </summary>
        private string accountNumber;

        /// <summary>
        /// The account type.
        /// </summary>
        private string accountType;

        /// <summary>
        /// The company ID.
        /// </summary>
        private string companyId;

        /// <summary>
        /// The account control type.
        /// </summary>
        private string controlType;

        /// <summary>
        /// The account department.
        /// </summary>
        private string department;

        /// <summary>
        /// The account description.
        /// </summary>
        private string description;

        /// <summary>
        /// The account subtype.
        /// </summary>
        private string subType;




        /// <summary>
        /// Gets or sets the account number.
        /// </summary>
        public string AccountNumber
        {
            get { return accountNumber; }
            set { accountNumber = value; }
        }

        /// <summary>
        /// Gets or sets the account type.
        /// </summary>
        public string AccountType
        {
            get { return accountType; }
            set { accountType = value; }
        }

        /// <summary>
        /// Gets or sets the company ID.
        /// </summary>
        public string CompanyId
        {
            get { return companyId; }
            set { companyId = value; }
        }

        /// <summary>
        /// Gets or sets the account control type.
        /// </summary>
        public string ControlType
        {
            get { return controlType; }
            set { controlType = value; }
        }

        /// <summary>
        /// Gets or sets the account department.
        /// </summary>
        public string Department
        {
            get { return department; }
            set { department = value; }
        }

        /// <summary>
        /// Gets or sets the account description.
        /// </summary>
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        /// <summary>
        /// Gets or sets the account subtype.
        /// </summary>
        public string SubType
        {
            get { return subType; }
            set { subType = value; }
        }




        /// <summary>
        /// Initializes a new instance of the Account class.
        /// This constructor accepts an IDataRecord representing a line item of a csv reader used to populate the object
        /// </summary>
        /// <param name="record">An IDataRecord representing a line item of a csv reader used to populate the object.</param>
        /// <param name="maxIndex">max index to examine in record</param>
        internal Account(IDataRecord record, int maxIndex)
        {
            accountNumber = DataHelper.GetData(record, 0, maxIndex);
            accountType = DataHelper.GetData(record, 2, maxIndex);
            companyId = DataHelper.GetData(record, 6, maxIndex);
            controlType = DataHelper.GetData(record, 4, maxIndex);
            department = DataHelper.GetData(record, 5, maxIndex);
            description = DataHelper.GetData(record, 1, maxIndex);
            subType = DataHelper.GetData(record, 3, maxIndex);
        }




        /// <summary>
        /// Writes the Account in a Pipe "|" separated value format, following the IPsvSerializable interface.
        /// Writes all values in alphabetical order by property name.
        /// </summary>
        /// <param name="outputStream">The output stream to which to write.</param>
        public void WritePsv(StreamWriter outputStream)
        {
            StringBuilder b = new StringBuilder();

            b.Append(accountNumber);
            b.Append("|");

            b.Append(accountType);
            b.Append("|");

            b.Append(companyId);
            b.Append("|");

            b.Append(controlType);
            b.Append("|");

            b.Append(department);
            b.Append("|");

            b.Append(description);
            b.Append("|");

            b.Append(subType);

            outputStream.WriteLine(b.ToString());
        }
    }
}