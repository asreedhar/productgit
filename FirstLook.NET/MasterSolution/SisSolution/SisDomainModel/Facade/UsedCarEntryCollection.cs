using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.CompilerServices;
using Firstlook.Sis.DomainModel.Facade;
using Firstlook.Sis.DomainModel.Serialization;

[assembly: InternalsVisibleTo("Firstlook.Sis.DomainModelTest")]

namespace Firstlook.Sis.DomainModel.Facade
{
    /// <summary>
    /// Represents a collection of UsedCarEntries.
    /// </summary>
    internal sealed class UsedCarEntryCollection : Collection<UsedCarEntry>, IPsvSerializable
    {
        private readonly Dictionary<int, int> VersionPropertyCounts;

        /// <summary>
        /// Initializes a new instance of the UsedCarEntryCollection class.
        /// This constructor Accepts an IDataReader to initialize a UsedCarEntry object for each IDataRecord of the reader.
        /// </summary>
        /// <param name="result">A CsvResult object with an IDataReader representing a csv reader with a record for each UsedCarEntry object to initialize.</param>
        internal UsedCarEntryCollection(SisCsvResult result)
        {
            VersionPropertyCounts = new Dictionary<int, int>();
            VersionPropertyCounts.Add(3, 16);
            VersionPropertyCounts.Add(4, 16);
            VersionPropertyCounts.Add(5, 16);
            VersionPropertyCounts.Add(6, 16);
            VersionPropertyCounts.Add(7, 16);

            while (result.Data.Read())
            {
                //the reader must have at least as many fields as the UsedCarEntry object has properties
                if (result.Data.FieldCount >= VersionPropertyCounts[result.FirstlookVersion])
                {
                    Add(new UsedCarEntry(result.Data, VersionPropertyCounts[result.FirstlookVersion] - 1));
                }
            }
        }




        /// <summary>
        /// Writes the UsedCarEntryCollection's UsedCarEntry items in a Pipe "|" separated value format, following the IPsvSerializable interface.
        /// </summary>
        /// <param name="outputStream">The output stream to which to write.</param>
        public void WritePsv(StreamWriter outputStream)
        {
            foreach (UsedCarEntry j in Items)
            {
                j.WritePsv(outputStream);
            }
        }
    }
}