using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.CompilerServices;

using Firstlook.Sis.DomainModel.Facade;
using Firstlook.Sis.DomainModel.Serialization;

[assembly: InternalsVisibleTo("Firstlook.Sis.DomainModelTest")]
namespace Firstlook.Sis.DomainModel.Facade
{
    /// <summary>
    /// Represents a collection of Vehicles.
    /// </summary>
    internal sealed class SoldVehicleCollection : Collection<SoldVehicle>, IPsvSerializable
    {
        private readonly Dictionary<int, int> VersionPropertyCounts;

        /// <summary>
        /// Initializes a new instance of the SoldVehicleCollection class.
        /// This constructor Accepts an IDataReader to initialize a SoldVehicle object for each IDataRecord of the reader.
        /// </summary>
        /// <param name="result">A CsvResult object with an IDataReader representing a csv reader with a record for each SoldVehicle object to initialize.</param>
        internal SoldVehicleCollection(SisCsvResult result)
        {
            VersionPropertyCounts = new Dictionary<int, int>();
            VersionPropertyCounts.Add(3, 134);
            VersionPropertyCounts.Add(4, 134);
            VersionPropertyCounts.Add(5, 135);
            VersionPropertyCounts.Add(6, 135);
            VersionPropertyCounts.Add(7, 135);


            while (result.Data.Read())
            {
                //the reader must have at least as many fields as the Vehicle object has properties
                if (result.Data.FieldCount >= VersionPropertyCounts[result.FirstlookVersion])
                {
                    Add(new SoldVehicle(result.Data, VersionPropertyCounts[result.FirstlookVersion] - 1));
                }
            }
        }






        /// <summary>
        /// Writes the VehicleCollection's Vehicle items in a Pipe "|" separated value format, following the IPsvSerializable interface.
        /// </summary>
        /// <param name="outputStream">The output stream to which to write.</param>
        public void WritePsv(StreamWriter outputStream)
        {
            foreach (SoldVehicle v in Items)
            {
                v.WritePsv(outputStream);
            }
        }
    }
}