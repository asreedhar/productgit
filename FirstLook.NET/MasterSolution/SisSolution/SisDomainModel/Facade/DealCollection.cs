using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Xml;
using Firstlook.Sis.DomainModel.Facade;
using Firstlook.Sis.DomainModel.Serialization;

//Used to allow this class to be used in the Sis.DomainModel_Test project
[assembly: InternalsVisibleTo("Firstlook.Sis.DomainModelTest")]
namespace Firstlook.Sis.DomainModel.Facade
{
    /// <summary>
    /// Represents a collection of Deals.
    /// </summary>
    internal sealed class DealCollection : Collection<Deal>, IPsvSerializable
    {
        private readonly Dictionary<int, int> VersionPropertyCounts;

        /// <summary>
        /// Initializes a new instance of the DealCollection class.
        /// This constructor Accepts an IDataReader to initialize a Deal object for each IDataRecord of the reader.
        /// </summary>
        /// <param name="result">A CsvResult object with an IDataReader representing a csv reader with a record for each Deal object to initialize.</param>
        internal DealCollection(SisCsvResult result)
        {
            VersionPropertyCounts = new Dictionary<int, int>();
            VersionPropertyCounts.Add(3, 190);
            VersionPropertyCounts.Add(4, 198);
            VersionPropertyCounts.Add(5, 199);
            VersionPropertyCounts.Add(6, 199);
            VersionPropertyCounts.Add(7, 201);

            while (result.Data.Read())
            {
                //the reader must have at least as many fields as the Deal object has properties
                if (result.Data.FieldCount >= VersionPropertyCounts[result.FirstlookVersion])
                {
                    Add(new Deal(result.Data, VersionPropertyCounts[result.FirstlookVersion] - 1));
                }
            }
        }



        /// <summary>
        /// Initializes a new instance of the DealCollection class.
        /// This constructor Accepts an XmlDocument to initialize a Deal object for each node of the document representing a Deal.
        /// </summary>
        /// <param name="result">A CsvResult object with an XmlDocument with one or more nodes representing a Deal object to initialize using XmlDeserialization.</param>
        internal DealCollection(SisXmlResult result)
        {
            VersionPropertyCounts = new Dictionary<int, int>();
            VersionPropertyCounts.Add(3, 190);
            VersionPropertyCounts.Add(4, 198);
            VersionPropertyCounts.Add(5, 199);
            VersionPropertyCounts.Add(6, 199);
            VersionPropertyCounts.Add(7, 201);

            XmlNodeList nl = result.Data.SelectNodes("/DMS_TRANSACTION/DMS_DEAL");
            if (nl != null)
            {
                foreach (XmlNode node in nl)
                {
                    if (!(node is XmlElement))
                    {
                        continue;
                    }
                    if (node.ChildNodes != null && node.ChildNodes.Count >= VersionPropertyCounts[result.FirstlookVersion])
                    {
                        Add(new Deal(node, VersionPropertyCounts[result.FirstlookVersion] - 1));
                    }
                }
            }
        }




        /// <summary>
        /// Writes the DealCollection's Deal items in a Pipe "|" separated value format, following the IPsvSerializable interface.
        /// </summary>
        /// <param name="outputStream">The output stream to which to write.</param>
        public void WritePsv(StreamWriter outputStream)
        {
            foreach (Deal d in Items)
            {
                d.WritePsv(outputStream);
            }
        }
    }
}