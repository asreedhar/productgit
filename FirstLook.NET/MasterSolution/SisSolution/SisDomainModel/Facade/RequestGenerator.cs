using System.Runtime.CompilerServices;
using System.Xml;

//Used to allow this class to be used in the Sis.DomainModel_Test project
[assembly: InternalsVisibleTo("Firstlook.Sis.DomainModelTest")]
namespace Firstlook.Sis.DomainModel.Facade
{
    /// <summary>
    /// Internal class used to ease the generation of xml requests to be sent to Sis.
    /// </summary>
    internal sealed class RequestGenerator
    {
        /// <summary>
        /// XmlDocument object used to contruct and hold the required nodes for an Sis request.
        /// </summary>
        private readonly XmlDocument doc;



        /// <summary>
        /// Initializes a new instance of the RequestGenerator class.
        /// This constructor accepts a string representing the transaction type to send to Sis.
        /// </summary>
        /// <param name="transactionType">The type of transaction to send to Sis.  Acceptable values can be found in the Sis documentation.</param>
        public RequestGenerator(string transactionType)
        {
            doc = new XmlDocument();
            doc.LoadXml("<DMS_TRANSACTION/>");
            doc.DocumentElement.AppendChild(doc.CreateElement("TRANSACTION_ID"));

            XmlElement transactionTypeElement = doc.CreateElement("TRANSACTION_TYPE");
            transactionTypeElement.InnerText = transactionType;
            doc.DocumentElement.AppendChild(transactionTypeElement);

            doc.DocumentElement.AppendChild(doc.CreateElement("COMPANY_ID"));

            XmlElement vendorTypeElement = doc.CreateElement("VENDOR_TYPE");
            vendorTypeElement.InnerText = "FLO";
            doc.DocumentElement.AppendChild(vendorTypeElement);


            doc.DocumentElement.AppendChild(doc.CreateElement("DMS_TYPE"));
            doc.DocumentElement.AppendChild(doc.CreateElement("DMS_ACCT"));
            doc.DocumentElement.AppendChild(doc.CreateElement("DMS_BRANCH"));
            doc.DocumentElement.AppendChild(doc.CreateElement("SIS_VERSION"));
            doc.DocumentElement.AppendChild(doc.CreateElement("FLO_VERSION"));
            doc.DocumentElement.AppendChild(doc.CreateElement("POLL_DATE"));
            doc.DocumentElement.AppendChild(doc.CreateElement("POLL_TIME"));
        }



        /// <summary>
        /// Method to accept a key/value and add it as a parameter and value to the Request Xml.
        /// </summary>
        /// <param name="parameter">The parameter name to set.</param>
        /// <param name="value">The value of the parameter to set.</param>
        public void SetParameter(string parameter, string value)
        {
            if (doc.SelectSingleNode("/DMS_TRANSACTION/" + parameter.ToUpper()) == null)
            {
                XmlElement el = doc.CreateElement(parameter);
                el.InnerText = value;
                doc.DocumentElement.AppendChild(el);
            }
            else
            {
                doc.SelectSingleNode("/DMS_TRANSACTION/" + parameter.ToUpper()).InnerText = value;
            }
        }

        /// <summary>
        /// Override of ToString method, returns a string of the internal XmlDocument's OuterXml.
        /// </summary>
        public XmlDocument RequestXml
        {
            get
            {
                return doc;
            }
        }
    }
}