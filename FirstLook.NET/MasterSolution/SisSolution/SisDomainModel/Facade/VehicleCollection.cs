using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Xml;
using Firstlook.Sis.DomainModel.Facade;
using Firstlook.Sis.DomainModel.Serialization;

[assembly: InternalsVisibleTo("Firstlook.Sis.DomainModelTest")]
namespace Firstlook.Sis.DomainModel.Facade
{
    /// <summary>
    /// Represents a collection of Vehicles.
    /// </summary>
    internal sealed class VehicleCollection : Collection<Vehicle>, IPsvSerializable
    {
        private readonly Dictionary<int, int> VersionPropertyCounts;

        /// <summary>
        /// Initializes a new instance of the VehicleCollection class.
        /// This constructor Accepts an IDataReader to initialize a Vehicle object for each IDataRecord of the reader.
        /// </summary>
        /// <param name="result">A CsvResult object with an IDataReader representing a csv reader with a record for each Vehicle object to initialize.</param>
        internal VehicleCollection(SisCsvResult result)
        {
            VersionPropertyCounts = new Dictionary<int, int>();
            VersionPropertyCounts.Add(3, 123);
            VersionPropertyCounts.Add(4, 131);
            VersionPropertyCounts.Add(5, 131);
            VersionPropertyCounts.Add(6, 133);
            VersionPropertyCounts.Add(7, 133);

            while (result.Data.Read())
            {
                //the reader must have at least as many fields as the Vehicle object has properties
                if (result.Data.FieldCount >= VersionPropertyCounts[result.FirstlookVersion])
                {
                    Add(new Vehicle(result.Data, VersionPropertyCounts[result.FirstlookVersion] - 1));
                }
            }
        }




        /// <summary>
        /// Initializes a new instance of the VehicleCollection class.
        /// This constructor Accepts an XmlDocument to initialize a Vehicle object for each node of the document representing a Vehicle.
        /// </summary>
        /// <param name="result">A CsvResult object with an XmlDocument with one or more nodes representing a Vehicle object to initialize using XmlDeserialization.</param>
        internal VehicleCollection(SisXmlResult result)
        {
            VersionPropertyCounts = new Dictionary<int, int>();
            VersionPropertyCounts.Add(3, 123);
            VersionPropertyCounts.Add(4, 131);
            VersionPropertyCounts.Add(5, 131);
            VersionPropertyCounts.Add(6, 133);
            VersionPropertyCounts.Add(7, 133);

            XmlNodeList nl = result.Data.SelectNodes("/DMS_TRANSACTION/DMS_VEH");
            if (nl != null)
            {
                foreach (XmlNode node in nl)
                {
                    if (!(node is XmlElement))
                    {
                        continue;
                    }
                    if (node.ChildNodes != null && node.ChildNodes.Count >= VersionPropertyCounts[result.FirstlookVersion])
                    {
                        Add(new Vehicle(node, VersionPropertyCounts[result.FirstlookVersion] - 1));
                    }
                }
            }
        }




        /// <summary>
        /// Writes the VehicleCollection's Vehicle items in a Pipe "|" separated value format, following the IPsvSerializable interface.
        /// </summary>
        /// <param name="outputStream">The output stream to which to write.</param>
        public void WritePsv(StreamWriter outputStream)
        {
            foreach (Vehicle v in Items)
            {
                v.WritePsv(outputStream);
            }
        }
    }
}