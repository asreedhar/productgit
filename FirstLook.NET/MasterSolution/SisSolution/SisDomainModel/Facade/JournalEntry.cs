using System.Data;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using Firstlook.Sis.DomainModel.Serialization;

//Used to allow this class to be used in the Sis.DomainModel_Test project
[assembly: InternalsVisibleTo("Firstlook.Sis.DomainModelTest")]
namespace Firstlook.Sis.DomainModel.Facade
{
    /// <summary>
    /// An accounting journal entry as returned from Sis, contained in the dealer's DMS.
    /// </summary>
    internal sealed class JournalEntry : IPsvSerializable
    {
        private readonly string accountDescription;
        private readonly string accountNumber;
        private readonly string accountType;
        private readonly string companyId;
        private readonly string controlNumber;
        private readonly string controlNumber2;
        private readonly string controlType;
        private readonly string controlType2;
        private readonly string journalDate;
        private readonly string journalDescription;
        private readonly string journalFullId;
        private readonly string journalId;
        private readonly string postAmount;
        private readonly string postDescription;
        private readonly string postSequence;
        private readonly string postTime;
        private readonly string productNumber;
        private readonly string productTypeCode;
        private readonly string referenceNumber;
        private readonly string statCount;
        private readonly string _VIN;
        private readonly string _DEAL_NO;
        private readonly string _CNTL_NAME;





        /// <summary>
        /// Initializes a new instance of the JournalEntry class.
        /// This constructor accepts an IDataRecord representing a line item of a csv reader used to populate the object
        /// </summary>
        /// <param name="record">An IDataRecord representing a line item of a csv reader used to populate the object.</param>
        internal JournalEntry(IDataRecord record, int maxIndex)
        {
            accountDescription = DataHelper.GetData(record, 8, maxIndex);
            accountNumber = DataHelper.GetData(record, 7, maxIndex);
            accountType = DataHelper.GetData(record, 9, maxIndex);
            companyId = DataHelper.GetData(record, 0, maxIndex);
            controlNumber = DataHelper.GetData(record, 12, maxIndex);
            controlNumber2 = DataHelper.GetData(record, 16, maxIndex);
            controlType = DataHelper.GetData(record, 11, maxIndex);
            controlType2 = DataHelper.GetData(record, 15, maxIndex);
            journalDate = DataHelper.GetData(record, 4, maxIndex);
            journalDescription = DataHelper.GetData(record, 2, maxIndex);
            journalFullId = DataHelper.GetData(record, 19, maxIndex);
            journalId = DataHelper.GetData(record, 1, maxIndex);
            postAmount = DataHelper.GetData(record, 10, maxIndex);
            postDescription = DataHelper.GetData(record, 18, maxIndex);
            postSequence = DataHelper.GetData(record, 6, maxIndex);
            postTime = DataHelper.GetData(record, 5, maxIndex);
            productNumber = DataHelper.GetData(record, 14, maxIndex);
            productTypeCode = DataHelper.GetData(record, 13, maxIndex);
            referenceNumber = DataHelper.GetData(record, 3, maxIndex);
            statCount = DataHelper.GetData(record, 17, maxIndex);

            //Version 4 attributes
            _VIN =  DataHelper.GetData(record, 20, maxIndex);
            _DEAL_NO  = DataHelper.GetData(record, 21, maxIndex);
            _CNTL_NAME = DataHelper.GetData(record, 22, maxIndex);
        }




        /// <summary>
        /// Writes the JournalEntry in a Pipe "|" separated value format, following the IPsvSerializable interface.
        /// Writes all values in alphabetical order by property name.
        /// </summary>
        /// <param name="outputStream">The output stream to which to write.</param>
        public void WritePsv(StreamWriter outputStream)
        {
            StringBuilder b = new StringBuilder();

            b.Append(accountDescription + "|");
            b.Append(accountNumber + "|");
            b.Append(accountType + "|");
            b.Append(companyId + "|");
            b.Append(controlNumber + "|");
            b.Append(controlNumber2 + "|");
            b.Append(controlType + "|");
            b.Append(controlType2 + "|");
            b.Append(journalDate + "|");
            b.Append(journalDescription + "|");
            b.Append(journalFullId + "|");
            b.Append(journalId + "|");
            b.Append(postAmount + "|");
            b.Append(postDescription + "|");
            b.Append(postSequence + "|");
            b.Append(postTime + "|");
            b.Append(productNumber + "|");
            b.Append(productTypeCode + "|");
            b.Append(referenceNumber + "|");
            b.Append(statCount + "|");
            b.Append(_VIN + "|");
            b.Append(_DEAL_NO + "|");
            b.Append(_CNTL_NAME);

            outputStream.WriteLine(b.ToString());
        }
    }
}