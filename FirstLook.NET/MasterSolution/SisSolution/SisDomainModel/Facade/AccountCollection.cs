using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.CompilerServices;
using Firstlook.Sis.DomainModel.Serialization;

//Used to allow this class to be used in the Sis.DomainModel_Test project
[assembly: InternalsVisibleTo("Firstlook.Sis.DomainModelTest")]
namespace Firstlook.Sis.DomainModel.Facade
{
    /// <summary>
    /// Represents a collection of Accounts.
    /// </summary>
    public sealed class AccountCollection : Collection<Account>, IPsvSerializable
    {
        private readonly Dictionary<int, int> VersionPropertyCounts;

        /// <summary>
        /// Initializes a new instance of the AccountCollection class.
        /// This constructor Accepts an IDataReader to initialize a Account object for each IDataRecord of the reader.
        /// </summary>
        /// <param name="result">A CsvResult object with an IDataReader representing a csv reader with a record for each Account object to initialize.</param>
        internal AccountCollection(SisCsvResult result)
        {
            VersionPropertyCounts = new Dictionary<int, int>();
            VersionPropertyCounts.Add(3, 7);
            VersionPropertyCounts.Add(4, 7);
            VersionPropertyCounts.Add(5, 7);
            VersionPropertyCounts.Add(6, 7);
            VersionPropertyCounts.Add(7, 7);


            while (result.Data.Read())
            {
                //the reader must have at least as many fields as the Account object has properties
                if (result.Data.FieldCount >= VersionPropertyCounts[result.FirstlookVersion])
                {
                    Add(new Account(result.Data, VersionPropertyCounts[result.FirstlookVersion] - 1));
                }
            }
        }




        /// <summary>
        /// Writes the AccountCollection's Account items in a Pipe "|" separated value format, following the IPsvSerializable interface.
        /// </summary>
        /// <param name="outputStream">The output stream to which to write.</param>
        public void WritePsv(StreamWriter outputStream)
        {
            foreach(Account acc in Items)
            {
                acc.WritePsv(outputStream);
            }
        }
    }
}