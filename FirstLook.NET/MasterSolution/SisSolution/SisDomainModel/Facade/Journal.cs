using System.Data;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using Firstlook.Sis.DomainModel.Serialization;

//Used to allow this class to be used in the Sis.DomainModel_Test project
[assembly: InternalsVisibleTo("Firstlook.Sis.DomainModelTest")]
namespace Firstlook.Sis.DomainModel.Facade
{
    /// <summary>
    /// An accounting journal as returned from Sis, contained in the dealer's DMS.
    /// XmlDeserialization is used to contruct a Journal from an XmlDocument.
    /// </summary>
    public sealed class Journal : IPsvSerializable
    {
        /// <summary>
        /// The Company ID
        /// </summary>
        private string companyId;
        
        /// <summary>
        /// The Journal's description.
        /// </summary>
        private string description;
        
        /// <summary>
        /// The Journal's number.
        /// </summary>
        private string journalNumber;
        
        /// <summary>
        /// The Journal's type.
        /// </summary>
        private string journalType;




        /// <summary>
        /// Gets or sets the Company ID
        /// </summary>
        public string CompanyId
        {
            get { return companyId; }
            set { companyId = value; }
        }

        /// <summary>
        /// Gets or sets the Journal's description.
        /// </summary>
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        /// <summary>
        /// Gets or sets the Journal's number.
        /// </summary>
        public string JournalNumber
        {
            get { return journalNumber; }
            set { journalNumber = value; }
        }

        /// <summary>
        /// Gets or sets the Journal's type.
        /// </summary>
        public string JournalType
        {
            get { return journalType; }
            set { journalType = value; }
        }




        /// <summary>
        /// Initializes a new instance of the Journal class.
        /// This constructor accepts an IDataRecord representing a line item of a csv reader used to populate the object
        /// </summary>
        /// <param name="record">An IDataRecord representing a line item of a csv reader used to populate the object.</param>
        internal Journal(IDataRecord record, int maxIndex)
        {
            companyId = DataHelper.GetData(record, 3, maxIndex);
            description = DataHelper.GetData(record, 1, maxIndex);
            journalNumber = DataHelper.GetData(record, 0, maxIndex);
            journalType = DataHelper.GetData(record, 2, maxIndex);
        }




        /// <summary>
        /// Writes the Journal in a Pipe "|" separated value format, following the IPsvSerializable interface.
        /// Writes all values in alphabetical order by property name.
        /// </summary>
        /// <param name="outputStream">The output stream to which to write.</param>
        public void WritePsv(StreamWriter outputStream)
        {
            StringBuilder b = new StringBuilder();

            b.Append(companyId);
            b.Append("|");

            b.Append(description);
            b.Append("|");

            b.Append(journalNumber);
            b.Append("|");

            b.Append(journalType);

            outputStream.WriteLine(b.ToString());
        }
    }
}