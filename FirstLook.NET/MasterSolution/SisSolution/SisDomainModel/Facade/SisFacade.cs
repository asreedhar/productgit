using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Firstlook.Sis.DomainModel.com.datadms.mv;
using Firstlook.Sis.DomainModel.Serialization;

namespace Firstlook.Sis.DomainModel.Facade
{
    /// <summary>
    /// Public entry point into Sis web service calls, containing methods to facilitate all Sis operations.
    /// </summary>
    internal sealed class SisFacade
    {
        /// <summary>
        /// Reusable DealController, as required for Sis web service.
        /// </summary>
        private readonly DealController dc;

        /// <summary>
        /// Reusable UserInfoHeader, as required for Sis web service.
        /// </summary>
        private UserInfoHeader ui;




        /// <summary>
        /// Initializes a new instance of the SisFacade class.
        /// This constructor accepts a Uri representing the Uri for the Sis web service.
        /// </summary>
        public SisFacade(Uri url)
        {
            dc = new DealController();
            dc.Url = url.ToString();
        }





        /// <summary>
        /// Method to get all Accounts.  Makes the Sis web service call using the CData option.
        /// </summary>
        /// <param name="credential">Login credential to pass to Sis.</param>
        public Receipt GetAccounts(Credential credential)
        {
            RequestGenerator rg = new RequestGenerator("ACCTG_ACCTS_CDATA");
            return SubmitRequest(credential, rg.RequestXml);
        }

        /// <summary>
        /// Method to get all Journals.  Makes the Sis web service call using the CData option.
        /// </summary>
        /// <param name="credential">Login credential to pass to Sis.</param>
        public Receipt GetJournals(Credential credential)
        {
            RequestGenerator rg = new RequestGenerator("ACCTG_JRNLS_CDATA");
            return SubmitRequest(credential, rg.RequestXml);
        }

        /// <summary>
        /// Method to get all Schedules.  Makes the Sis web service call using the CData option.
        /// </summary>
        /// <param name="credential">Login credential to pass to Sis.</param>
        public Receipt GetSchedules(Credential credential)
        {
            RequestGenerator rg = new RequestGenerator("ACCTG_SCHEDS_CDATA");
            return SubmitRequest(credential, rg.RequestXml);
        }

        /// <summary>
        /// Method to get all Journal Entries.  Makes the Sis web service call using the CData option.
        /// </summary>
        /// <param name="credential">Login credential to pass to Sis.</param>
        /// <param name="startDate">Start Date</param>
        /// <param name="endDate">End Date</param>
        /// <param name="journalIds">Journals</param>
        /// <param name="accountIds">Journals</param>
        public Receipt GetJournalEntries(Credential credential, DateTime startDate, DateTime endDate, ICollection<string> journalIds, ICollection<string> accountIds)
        {
            RequestGenerator rg = new RequestGenerator("BATCH_SALES_CDATA");
            rg.SetParameter("START_DATE", startDate.ToShortDateString());
            rg.SetParameter("END_DATE", endDate.ToShortDateString());

            if (journalIds.Count > 0)
            {
                string conn = "";
                StringBuilder j = new StringBuilder();

                foreach (string i in journalIds)
                {
                    j.Append(conn + i);
                    conn = ",";
                }
                rg.SetParameter("JRNLS", j.ToString());
            }

            if (accountIds.Count > 0)
            {
                string conn = "";
                StringBuilder a = new StringBuilder();

                foreach (string i in accountIds)
                {
                    a.Append(conn + i);
                    conn = ",";
                }
                rg.SetParameter("ACCTS", a.ToString());
            }

            return SubmitRequest(credential, rg.RequestXml);
        }

        /// <summary>
        /// Method to get used car schedule.  Makes the Sis web service call using the CData option.
        /// </summary>
        /// <param name="credential">Login credential to pass to Sis.</param>
        public Receipt GetUsedCarSchedule(Credential credential)
        {
            RequestGenerator rg = new RequestGenerator("ACCTG_USEDSCH_CDATA");
            return SubmitRequest(credential, rg.RequestXml);
        }

        /// <summary>
        /// Method to get all Vehicles.  Makes the Sis web service call using the CData option.
        /// </summary>
        /// <param name="credential">Login credential to pass to Sis.</param>
        /// <param name="newUsed"></param>
        public Receipt GetVehicles(Credential credential, string newUsed)
        {
            RequestGenerator rg = new RequestGenerator("BATCH_VEH_CDATA");
            if (!string.IsNullOrEmpty(newUsed))
            {
                if (newUsed.Equals("new", StringComparison.InvariantCultureIgnoreCase) || newUsed.Equals("used", StringComparison.InvariantCultureIgnoreCase))
                {
                    rg.SetParameter("NEW_USED", newUsed);
                }
            }
            return SubmitRequest(credential, rg.RequestXml);
        }

        /// <summary>
        /// Method to get all Deals.  Makes the Sis web service call using the CData option.
        /// </summary>
        /// <param name="credential">Login credential to pass to Sis.</param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        public Receipt GetDeals(Credential credential, DateTime startDate, DateTime endDate)
        {
            RequestGenerator rg = new RequestGenerator("BATCH_CDATA_UPDATE");
            rg.SetParameter("START_DATE", startDate.ToShortDateString());
            rg.SetParameter("END_DATE", endDate.ToShortDateString());
            return SubmitRequest(credential, rg.RequestXml);
        }



        /// <summary>
        /// Method to get all sold vehicles.  Makes the Sis web service call using the CData option.
        /// </summary>
        /// <param name="credential">Login credential to pass to Sis.</param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        public Receipt GetSoldVehicles(Credential credential, DateTime startDate, DateTime endDate)
        {
            RequestGenerator rg = new RequestGenerator("BATCH_VEHSOLD_CDATA");
            rg.SetParameter("START_DATE", startDate.ToShortDateString());
            rg.SetParameter("END_DATE", endDate.ToShortDateString());
            return SubmitRequest(credential, rg.RequestXml);
        }



        /// <summary>
        /// Method to get a specific Vehicle.  Makes the Sis web service call using the Xml option.
        /// </summary>
        /// <param name="credential">Login credential to pass to Sis.</param>
        /// <param name="StockNumber"></param>
        public Receipt GetVehicle(Credential credential, string StockNumber)
        {
            RequestGenerator rg = new RequestGenerator("VEH_INFO");
            rg.SetParameter("STOCK_NUMBER", StockNumber);
            return SubmitRequest(credential, rg.RequestXml);
        }

        /// <summary>
        /// Method to get a VehicleList.  Makes the Sis web service call using the Xml option.
        /// </summary>
        /// <param name="credential">Login credential to pass to Sis.</param>
        /// <param name="newUsed"></param>
        public Receipt GetVehicleList(Credential credential, string newUsed)
        {
            RequestGenerator rg = new RequestGenerator("VEH_LIST");
            if(!string.IsNullOrEmpty(newUsed))
            {
                if (newUsed.Equals("new", StringComparison.InvariantCultureIgnoreCase) || newUsed.Equals("used", StringComparison.InvariantCultureIgnoreCase))
                {
                    rg.SetParameter("NEW_USED", newUsed);
                }
            }
            return SubmitRequest(credential, rg.RequestXml);
        }

        /// <summary>
        /// Method to get a specific Deal.  Makes the Sis web service call using the Xml option.
        /// </summary>
        /// <param name="credential">Login credential to pass to Sis.</param>
        /// <param name="DealNumber"></param>
        public Receipt GetDeal(Credential credential, int DealNumber)
        {
            RequestGenerator rg = new RequestGenerator("DEAL_INFO");
            rg.SetParameter("DEAL_NUMBER", DealNumber.ToString());
            return SubmitRequest(credential, rg.RequestXml);
        }








        /// <summary>
        /// Private method to get the DmsID [unique identifier for request] back from Sis after submitting a request in order to contruct a receipt.
        /// </summary>
        /// <param name="credential"></param>
        /// <param name="requestXml"></param>
        private Receipt SubmitRequest(Credential credential, XmlDocument requestXml)
        {
            SetCredentials(credential);
            EnsureCommManagerAccessibility(credential);

            Receipt r = new Receipt(dc.SubmitRequest(requestXml.OuterXml), requestXml);
            return r;
        }

        /// <summary>
        /// Method to check status of an Sis request via a Receipt, as provided by the SisFacade.
        /// </summary>
        /// <param name="credential">Login credential to pass to Sis.</param>
        /// <param name="receipt"></param>
        public QueryStatusInfo CheckStatus(Credential credential, Receipt receipt)
        {
            SetCredentials(credential);
            EnsureCommManagerAccessibility(credential);

            RequestStatus status = dc.GetRequestStatus(receipt.DmsId, false);

            if (status.Updated)
                return new QueryStatusInfo(QueryStatus.Completed, status.ReadDate, status.UpdatedDate);
            else if (status.Read)
                return new QueryStatusInfo(QueryStatus.Accepted, status.ReadDate);
            else
                return new QueryStatusInfo(QueryStatus.Submitted);
        }

        /// <summary>
        /// Method to get the response of an Sis request via a Receipt, as provided by the SisFacade.
        /// </summary>
        /// <param name="credential">Login credential to pass to Sis.</param>
        /// <param name="receipt"></param>
        /// <param name="type"></param>
        public IPsvSerializable GetResponse(Credential credential, Receipt receipt, QueryType type)
        {
            SetCredentials(credential);
            EnsureCommManagerAccessibility(credential);

            XmlDocument response = new XmlDocument();
            response.LoadXml(dc.GetDealResponse(receipt.DmsId));

            IPsvSerializable psvResult = null;

            switch (type)
            {
                //CData calls can get a CsvReader to assemble a collection
                case QueryType.GetAccounts:
                    psvResult = new AccountCollection(new SisCsvResult(response));
                    break;
                case QueryType.GetJournals:
                    psvResult = new JournalCollection(new SisCsvResult(response));
                    break;
                case QueryType.GetSchedules:
                    psvResult = new ScheduleCollection(new SisCsvResult(response));
                    break;
                case QueryType.GetJournalDetail:
                    psvResult = new JournalEntryCollection(new SisCsvResult(response));
                    break;
                case QueryType.GetSchedule_Used:
                    psvResult = new UsedCarEntryCollection(new SisCsvResult(response));
                    break;
                case QueryType.GetDeals:
                    psvResult = new DealCollection(new SisCsvResult(response));
                    break;
                case QueryType.GetSoldVehicles:
                    psvResult = new SoldVehicleCollection(new SisCsvResult(response));
                    break;
                case QueryType.GetVehicles:
                    psvResult = new VehicleCollection(new SisCsvResult(response));
                    break;

                //Non CData calls pass the Xml Response
                case QueryType.GetDeal:
                    psvResult = new DealCollection(new SisXmlResult(response));
                    break;
                case QueryType.GetVehicle:
                    psvResult = new VehicleCollection(new SisXmlResult(response));
                    break;
                case QueryType.GetVehicleList:
                    psvResult = new VehicleInfoCollection(new SisXmlResult(response));
                    break;
            }
            return psvResult;
        }

        /// <summary>
        /// Method to get the connection status of the comm manager(s) at a DMS.
        /// </summary>
        /// <param name="credential">Login credential to pass to Sis.</param>
        public List<ConnectionStatus> GetConnectionStatus(Credential credential)
        {
            SetCredentials(credential);
            List<ConnectionStatus> result = new List<ConnectionStatus>();
            CommMonitorInformationContainer container = dc.GetCommManagerMonitorInformationContainer();

            if (container.CommMonitorInformationArray != null && container.CommMonitorInformationArray.Length > 0)
            {
                foreach (CommMonitorInformation cmi in container.CommMonitorInformationArray)
                {
                    result.Add(new ConnectionStatus(cmi.PollingIntervalSeconds, cmi.CurrentTime, cmi.LastResponse));
                }
            }
            else
            {
                throw new SisCommManagersInaccessibleException("Comm Manager(s) are inaccessible.");
            }
            return result;
        }







        /// <summary>
        /// Checks connectivity status for comm mgr(s), throwing exception if none are accessible.
        /// </summary>
        /// <param name="credential">The login credentials to send Sis.</param>
        private void EnsureCommManagerAccessibility(Credential credential)
        {
            if (!ConnectionStatus.IsAlive(GetConnectionStatus(credential)))
            {
                throw new SisCommManagersInaccessibleException("Comm Manager(s) are inaccessible.");
            }
        }

        /// <summary>
        /// Private method to set credentials on the DealController required by Sis for web service calls.
        /// </summary>
        /// <param name="credential">Login credential to pass to Sis.</param>
        private void SetCredentials(Credential credential)
        {
            ui = new UserInfoHeader();
            ui.userid = credential.Username;
            ui.password = credential.Password;
            dc.UserInfoHeaderValue = ui;
        }
    }
}