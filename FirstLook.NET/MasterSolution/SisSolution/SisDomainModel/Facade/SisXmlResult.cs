using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Xml;

//Used to allow this class to be used in the Sis.DomainModel_Test project
[assembly: InternalsVisibleTo("Firstlook.Sis.DomainModelTest")]
namespace Firstlook.Sis.DomainModel.Facade
{
    /// <summary>
    /// Wrapper class over Sis result set.
    /// </summary>
    internal sealed class SisXmlResult : SisResult
    {
        /// <summary>
        /// Data from response.
        /// </summary>
        private readonly XmlNode data;


        /// <summary>
        /// Gets the Data from response.
        /// </summary>
        public XmlNode Data
        {
            get
            {
                return data;
            }
        }


        
        /// <summary>
        /// Initializes a new instance of the SisResult class.
        /// This constructor accepts an XmlDocument representing the response of the Sis web service call.
        /// </summary>
        public SisXmlResult(XmlNode response) : base(response)
        {
            data = response;
        }
    }
}
