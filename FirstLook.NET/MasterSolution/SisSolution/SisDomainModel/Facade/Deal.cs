using System.Data;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Xml;
using Firstlook.Sis.DomainModel.Serialization;

//Used to allow this class to be used in the Sis.DomainModel_Test project
[assembly: InternalsVisibleTo("Firstlook.Sis.DomainModelTest")]
namespace Firstlook.Sis.DomainModel.Facade
{
    /// <summary>
    /// A completed sale as returned from Sis, contained in the dealer's DMS.
    /// XmlDeserialization is used to contruct a Vehicle from an XmlDocument.
    /// </summary>
    internal sealed class Deal : IPsvSerializable
    {
        #pragma warning disable 0649

        private readonly string _DEAL_NUMBER;
        private readonly string _DEAL_TYPE;
        private readonly string _STATUS;
        private readonly string _TRANSACTION_TYPE;
        private readonly string _LAST_UPDATED_DATE;
        private readonly string _EFFECTIVE_DATE;
        private readonly string _TRANSACTION_DATE;
        private readonly string _FUNDED_DATE;
        private readonly string _TURNED_DATE;
        private readonly string _CUSTOMER_NUMBER;
        private readonly string _BUSINESS_NAME; 
        private readonly string _FIRST_NAME;
        private readonly string _MIDDLE_INITIAL;
        private readonly string _LAST_NAME;
        private readonly string _ADDRESS1;
        private readonly string _ADDRESS2; 
        private readonly string _CITY;
        private readonly string _STATE;
        private readonly string _ZIPCODE;
        private readonly string _CUST_PHONE_NUMBER;
        private readonly string _CUST_MOBILE_NUMBER; 
        private readonly string _CUST_BUS_PHONE_NUMBER;
        private readonly string _CUST_WORK_PHONE_EXT; 
        private readonly string _CUST_FAX_PHONE; 
        private readonly string _CUST_LIC_NO; 
        private readonly string _EMAIL; 
        private readonly string _CO_CUSTOMER_NUMBER; 
        private readonly string _CO_FIRST_NAME; 
        private readonly string _CO_MIDDLE_INITIAL; 
        private readonly string _CO_LAST_NAME; 
        private readonly string _CO_SUFFIX; 
        private readonly string _CO_SALUTATION; 
        private readonly string _CO_ADDRESS1; 
        private readonly string _CO_CITY; 
        private readonly string _CO_STATE; 
        private readonly string _CO_ZIPCODE; 
        private readonly string _CO_PHONE_NUMBER; 
        private readonly string _CO_EMAIL; 
        private readonly string _CO_BUSINESS_NAME; 
        private readonly string _CO_BIRTH_DATE; 
        private readonly string _CO_LIC_NO; 
        private readonly string _REG_STATE;
        private readonly string _CUST_BIRTH_DATE; 
        private readonly string _BANK_CODE;
        private readonly string _BANK_NAME; 
        private readonly string _BANK_ADDRESS; 
        private readonly string _BANK_CITY; 
        private readonly string _BANK_STATE; 
        private readonly string _BANK_ZIP; 
        private readonly string _BANK_PHONE_NUMBER; 
        private readonly string _PURCHASE_PRICE;
        private readonly string _DAYS_TO_FIRST_PAYMENT; 
        private readonly string _DEPOSIT;
        private readonly string _TOTAL_SELL_PRICE;
        private readonly string _TOTAL_SRP; 
        private readonly string _CASH_DOWN;
        private readonly string _APR;
        private readonly string _BUY_RATE;
        private readonly string _MONTHLY_PAYMENT;
        private readonly string _MSRP;
        private readonly string _REBATE;
        private readonly string _REBATE2;
        private readonly string _REBATE3;
        private readonly string _TERMS; 
        private readonly string _HOLDBACK;
        private readonly string _VEH_COST;
        private readonly string _PACK;
        private readonly string _AMOUNT;
        private readonly string _PRE_PAID_INTRST;
        private readonly string _FIN_CHARGE;
        private readonly string _TOT_DLR_PROFIT;
        private readonly string _TAX_RATE;
        private readonly string _CITY_TAX_RATE; 
        private readonly string _COUNTY_TAX_RATE; 
        private readonly string _WARR_CNTY_RATE;
        private readonly string _WARR_STATE; 
        private readonly string _WARR_STATE_RATE; 
        private readonly string _WARR_VENDOR;
        private readonly string _LIEN_PAYOFF; 
        private readonly string _LIEN_PAYOFF2; 
        private readonly string _TRADE_MAKE; 
        private readonly string _TRADE_MODEL; 
        private readonly string _TRADE_YEAR; 
        private readonly string _TRADE_VIN; 
        private readonly string _TRADE_MILEAGE; 
        private readonly string _TRADE_VALUE; 
        private readonly string _TRADE_LIEN; 
        private readonly string _TRADE2_MAKE; 
        private readonly string _TRADE2_MODEL; 
        private readonly string _TRADE2_YEAR; 
        private readonly string _TRADE2_VIN; 
        private readonly string _TRADE2_MILEAGE; 
        private readonly string _TRADE2_VALUE; 
        private readonly string _TRADE2_LIEN; 
        private readonly string _TRADE2_ACV; 
        private readonly string _TRADE_ACV; 
        private readonly string _FIN_RES;
        private readonly string _FE_GROSS;
        private readonly string _WARR_RES;
        private readonly string _AH_RES;
        private readonly string _LIFE_RES;
        private readonly string _SLSPERSON1_ID;
        private readonly string _SALESPERSON1;
        private readonly string _SLSPERSON2_ID; 
        private readonly string _SALESPERSON2; 
        private readonly string _SLSMANAGER_ID;
        private readonly string _SALESMANAGER;
        private readonly string _FI_MGR_ID;
        private readonly string _FI_MANAGER;
        private readonly string _LEASE_MILEAGE_PENALTY;
        private readonly string _LEASE_MILEAGE_PENALTY_VALUE;
        private readonly string _STANDARD_LEASE_MILEAGE; 
        private readonly string _EXPECTED_LEASE_MILEAGE; 
        private readonly string _MONEY_FACTOR; 
        private readonly string _NET_RESIDUAL_VALUE;
        private readonly string _RESIDUAL;
        private readonly string _APR_LEASE_FLAG;
        private readonly string _BASE_PAYMENT;
        private readonly string _BASE_RESIDUAL_VALUE;
        private readonly string _CAPITALIZE_LEASE_TAX; 
        private readonly string _CAP_REDUCTION;
        private readonly string _STOCK_NUMBER;
        private readonly string _NEW;
        private readonly string _YEAR;
        private readonly string _VIN;
        private readonly string _MAKE;
        private readonly string _MAKE_CODE; 
        private readonly string _MODEL;
        private readonly string _MODEL_NO;
        private readonly string _CARLINE; 
        private readonly string _MILEAGE;
        private readonly string _VEH_TYPE; 
        private readonly string _DOOR_CNT; 
        private readonly string _CYL_CNT; 
        private readonly string _INV_EXT_CLR_DESC;
        private readonly string _INV_INT_CLR_DESC; 
        private readonly string _INV_EXT_CLR_CODE; 
        private readonly string _INV_INT_CLR_CODE; 
        private readonly string _INV_TRIM; 
        private readonly string _LIST_PRICE;
        private readonly string _BASE_BALLOON_PAYMENT;
        private readonly string _BALLOON_MILEAGE_PENALTY; 
        private readonly string _BALLOON_MILEAGE_PENALTY_VALUE;
        private readonly string _BALLOON_PAYMENT;
        private readonly string _BALLOON_PCT;
        private readonly string _BALLOON_TYPE;
        private readonly string _STANDARD_BALLOON_MILEAGE; 
        private readonly string _EXPECTED_BALLOON_MILEAGE; 
        private readonly string _LIFE_LEVEL_PREM; 
        private readonly string _STATUS_CODE;
        private readonly string _CASH_DOWN_AMT;
        private readonly string _CASH_INCEPT_AMT;
        private readonly string _MSRP_HARD_ADDS;
        private readonly string _TRADE_INCEPT_AMT;
        private readonly string _SEC_DEPOSIT;
        private readonly string _REBATE_INCEPT_AMT;
        private readonly string _INIT_MP; 
        private readonly string _REBATE_DOWN_AMT;
        private readonly string _EXT_MNTH_INT_ON_LEV; 
        private readonly string _TRADE_DOWN_AMT;
        private readonly string _MONTHLY_PAYMENT_ADD_ON;
        private readonly string _EQUITY_DEFICIT;
        private readonly string _SLSPERSON1_LAST_NAME;
        private readonly string _SLSPERSON1_FIRST_NAME;
        private readonly string _REBATE4;
        private readonly string _REBATE5;
        private readonly string _SUFFIX; 
        private readonly string _TRADE1_STOCK_NUMBER; 
        private readonly string _TRADE2_STOCK_NUMBER; 
        private readonly string _TRADE3_STOCK_NUMBER; 
        private readonly string _ACQUIRED_DATE;
        private readonly string _RCPT_DATE;
        private readonly string _INV_AMT;
        private readonly string _RECON_AMT;
        private readonly string _RECON_COST;
        private readonly string _I_COMPANY;
        private readonly string _INV_ACCT;
        private readonly string _SM_DEAL_TYPE;
        private readonly string _STOCK_TYPE;
        private readonly string _DLR_SELECT; 
        private readonly string _USE_TAX_RATE; 
        private readonly string _ADD_EQUITY_DEFICIT_TO_CAP; 
        private readonly string _GENERAL_TAX_RATE; 
        private readonly string _PUP1_AMT;
        private readonly string _PUP2_AMT;
        private readonly string _PUP3_AMT;
        private readonly string _IN_SERVICE_DATE;
        private readonly string _POLL_DATE;
        private readonly string _POLL_TIME;
        private readonly string _VEH_LOC;

        private readonly string _VEH_UDF1;
        private readonly string _VEH_UDF2;
        private readonly string _VEH_UDF3;
        private readonly string _VEH_UDF4;
        private readonly string _VEH_UDF5;
        private readonly string _VEH_UDF6;
        private readonly string _VEH_UDF7;
        private readonly string _VEH_UDF8;

        private readonly string _SALE_ACCOUNT;

        private readonly string _INVEN_SRC;
        private readonly string _INVEN_SRC_NAME;

        #pragma warning restore 0649



        /// <summary>
        /// Initializes a new instance of the Deal class.
        /// This constructor accepts an XmlNode representing a Deal object to initialize.
        /// </summary>
        /// <param name="node">An XmlNode representing a Deal object to initialize.</param>
        internal Deal(XmlNode node, int maxIndex)
        {
            _DEAL_NUMBER = DataHelper.GetData(node, 0, maxIndex);
            _DEAL_TYPE = DataHelper.GetData(node, 1, maxIndex);
            _STATUS = DataHelper.GetData(node, 2, maxIndex);
            _TRANSACTION_TYPE = DataHelper.GetData(node, 3, maxIndex);
            _LAST_UPDATED_DATE = DataHelper.GetData(node, 4, maxIndex);
            _EFFECTIVE_DATE = DataHelper.GetData(node, 5, maxIndex);
            _TRANSACTION_DATE = DataHelper.GetData(node, 6, maxIndex);
            _FUNDED_DATE = DataHelper.GetData(node, 7, maxIndex);
            _TURNED_DATE = DataHelper.GetData(node, 8, maxIndex);
            _CUSTOMER_NUMBER = DataHelper.GetData(node, 9, maxIndex);
            _BUSINESS_NAME = DataHelper.GetData(node, 10, maxIndex);

            //NOT POPULATED FOR PRIVACY/LEGAL REASONS

            //_FIRST_NAME = DataHelper.GetData(node, 11, maxIndex);
            //_MIDDLE_INITIAL = DataHelper.GetData(node, 12, maxIndex);
            //_LAST_NAME = DataHelper.GetData(node, 13, maxIndex);
            //_ADDRESS1 = DataHelper.GetData(node, 14, maxIndex);
            //_ADDRESS2 = DataHelper.GetData(node, 15, maxIndex);
            //_CITY = DataHelper.GetData(node, 16, maxIndex);
            //_STATE = DataHelper.GetData(node, 17, maxIndex);
            _ZIPCODE = DataHelper.GetData(node, 18, maxIndex);
            //_CUST_PHONE_NUMBER = DataHelper.GetData(node, 19, maxIndex);
            //_CUST_MOBILE_NUMBER = DataHelper.GetData(node, 20, maxIndex);
            //_CUST_BUS_PHONE_NUMBER = DataHelper.GetData(node, 21, maxIndex);
            //_CUST_WORK_PHONE_EXT = DataHelper.GetData(node, 22, maxIndex);
            //_CUST_FAX_PHONE = DataHelper.GetData(node, 23, maxIndex);
            //_CUST_LIC_NO = DataHelper.GetData(node, 24, maxIndex);
            //_EMAIL = DataHelper.GetData(node, 25, maxIndex);
            //_CO_CUSTOMER_NUMBER = DataHelper.GetData(node, 26, maxIndex);
            //_CO_FIRST_NAME = DataHelper.GetData(node, 27, maxIndex);
            //_CO_MIDDLE_INITIAL = DataHelper.GetData(node, 28, maxIndex);
            //_CO_LAST_NAME = DataHelper.GetData(node, 29, maxIndex);
            //_CO_SUFFIX = DataHelper.GetData(node, 30, maxIndex);
            //_CO_SALUTATION = DataHelper.GetData(node, 31, maxIndex);
            //_CO_ADDRESS1 = DataHelper.GetData(node, 32, maxIndex);
            //_CO_CITY = DataHelper.GetData(node, 33, maxIndex);
            //_CO_STATE = DataHelper.GetData(node, 34, maxIndex);
            //_CO_ZIPCODE = DataHelper.GetData(node, 35, maxIndex);
            //_CO_PHONE_NUMBER = DataHelper.GetData(node, 36, maxIndex);
            //_CO_EMAIL = DataHelper.GetData(node, 37, maxIndex);
            //_CO_BUSINESS_NAME = DataHelper.GetData(node, 38, maxIndex);
            //_CO_BIRTH_DATE = DataHelper.GetData(node, 39, maxIndex);
            //_CO_LIC_NO = DataHelper.GetData(node, 40, maxIndex);


            _REG_STATE = DataHelper.GetData(node, 41, maxIndex);

            //_CUST_BIRTH_DATE = DataHelper.GetData(node, 42, maxIndex);

            _BANK_CODE = DataHelper.GetData(node, 43, maxIndex);
            _BANK_NAME = DataHelper.GetData(node, 44, maxIndex);
            _BANK_ADDRESS = DataHelper.GetData(node, 45, maxIndex);
            _BANK_CITY = DataHelper.GetData(node, 46, maxIndex);
            _BANK_STATE = DataHelper.GetData(node, 47, maxIndex);
            _BANK_ZIP = DataHelper.GetData(node, 48, maxIndex);
            _BANK_PHONE_NUMBER = DataHelper.GetData(node, 49, maxIndex);
            _PURCHASE_PRICE = DataHelper.GetData(node, 50, maxIndex);
            _DAYS_TO_FIRST_PAYMENT = DataHelper.GetData(node, 51, maxIndex);
            _DEPOSIT = DataHelper.GetData(node, 52, maxIndex);
            _TOTAL_SELL_PRICE = DataHelper.GetData(node, 53, maxIndex);
            _TOTAL_SRP = DataHelper.GetData(node, 54, maxIndex);
            _CASH_DOWN = DataHelper.GetData(node, 55, maxIndex);
            _APR = DataHelper.GetData(node, 56, maxIndex);
            _BUY_RATE = DataHelper.GetData(node, 57, maxIndex);
            _MONTHLY_PAYMENT = DataHelper.GetData(node, 58, maxIndex);
            _MSRP = DataHelper.GetData(node, 59, maxIndex);
            _REBATE = DataHelper.GetData(node, 60, maxIndex);
            _REBATE2 = DataHelper.GetData(node, 61, maxIndex);
            _REBATE3 = DataHelper.GetData(node, 62, maxIndex);
            _TERMS = DataHelper.GetData(node, 63, maxIndex);
            _HOLDBACK = DataHelper.GetData(node, 64, maxIndex);
            _VEH_COST = DataHelper.GetData(node, 65, maxIndex);
            _PACK = DataHelper.GetData(node, 66, maxIndex);
            _AMOUNT = DataHelper.GetData(node, 67, maxIndex);
            _PRE_PAID_INTRST = DataHelper.GetData(node, 68, maxIndex);
            _FIN_CHARGE = DataHelper.GetData(node, 69, maxIndex);
            _TOT_DLR_PROFIT = DataHelper.GetData(node, 70, maxIndex);
            _TAX_RATE = DataHelper.GetData(node, 71, maxIndex);
            _CITY_TAX_RATE = DataHelper.GetData(node, 72, maxIndex);
            _COUNTY_TAX_RATE = DataHelper.GetData(node, 73, maxIndex);
            _WARR_CNTY_RATE = DataHelper.GetData(node, 74, maxIndex);
            _WARR_STATE = DataHelper.GetData(node, 75, maxIndex);
            _WARR_STATE_RATE = DataHelper.GetData(node, 76, maxIndex);
            _WARR_VENDOR = DataHelper.GetData(node, 77, maxIndex);
            _LIEN_PAYOFF = DataHelper.GetData(node, 78, maxIndex);
            _LIEN_PAYOFF2 = DataHelper.GetData(node, 79, maxIndex);
            _TRADE_MAKE = DataHelper.GetData(node, 80, maxIndex);
            _TRADE_MODEL = DataHelper.GetData(node, 81, maxIndex);
            _TRADE_YEAR = DataHelper.GetData(node, 82, maxIndex);
            _TRADE_VIN = DataHelper.GetData(node, 83, maxIndex);
            _TRADE_MILEAGE = DataHelper.GetData(node, 84, maxIndex);
            _TRADE_VALUE = DataHelper.GetData(node, 85, maxIndex);
            _TRADE_LIEN = DataHelper.GetData(node, 86, maxIndex);
            _TRADE2_MAKE = DataHelper.GetData(node, 87, maxIndex);
            _TRADE2_MODEL = DataHelper.GetData(node, 88, maxIndex);
            _TRADE2_YEAR = DataHelper.GetData(node, 89, maxIndex);
            _TRADE2_VIN = DataHelper.GetData(node, 90, maxIndex);
            _TRADE2_MILEAGE = DataHelper.GetData(node, 91, maxIndex);
            _TRADE2_VALUE = DataHelper.GetData(node, 92, maxIndex);
            _TRADE2_LIEN = DataHelper.GetData(node, 93, maxIndex);
            _TRADE2_ACV = DataHelper.GetData(node, 94, maxIndex);
            _TRADE_ACV = DataHelper.GetData(node, 95, maxIndex);
            _FIN_RES = DataHelper.GetData(node, 96, maxIndex);
            _FE_GROSS = DataHelper.GetData(node, 97, maxIndex);
            _WARR_RES = DataHelper.GetData(node, 98, maxIndex);
            _AH_RES = DataHelper.GetData(node, 99, maxIndex);
            _LIFE_RES = DataHelper.GetData(node, 100, maxIndex);
            _SLSPERSON1_ID = DataHelper.GetData(node, 101, maxIndex);
            _SALESPERSON1 = DataHelper.GetData(node, 102, maxIndex);
            _SLSPERSON2_ID = DataHelper.GetData(node, 103, maxIndex);
            _SALESPERSON2 = DataHelper.GetData(node, 104, maxIndex);
            _SLSMANAGER_ID = DataHelper.GetData(node, 105, maxIndex);
            _SALESMANAGER = DataHelper.GetData(node, 106, maxIndex);
            _FI_MGR_ID = DataHelper.GetData(node, 107, maxIndex);
            _FI_MANAGER = DataHelper.GetData(node, 108, maxIndex);
            _LEASE_MILEAGE_PENALTY = DataHelper.GetData(node, 109, maxIndex);
            _LEASE_MILEAGE_PENALTY_VALUE = DataHelper.GetData(node, 110, maxIndex);
            _STANDARD_LEASE_MILEAGE = DataHelper.GetData(node, 111, maxIndex);
            _EXPECTED_LEASE_MILEAGE = DataHelper.GetData(node, 112, maxIndex);
            _MONEY_FACTOR = DataHelper.GetData(node, 113, maxIndex);
            _NET_RESIDUAL_VALUE = DataHelper.GetData(node, 114, maxIndex);
            _RESIDUAL = DataHelper.GetData(node, 115, maxIndex);
            _APR_LEASE_FLAG = DataHelper.GetData(node, 116, maxIndex);
            _BASE_PAYMENT = DataHelper.GetData(node, 117, maxIndex);
            _BASE_RESIDUAL_VALUE = DataHelper.GetData(node, 118, maxIndex);
            _CAPITALIZE_LEASE_TAX = DataHelper.GetData(node, 119, maxIndex);
            _CAP_REDUCTION = DataHelper.GetData(node, 120, maxIndex);
            _STOCK_NUMBER = DataHelper.GetData(node, 121, maxIndex);
            _NEW = DataHelper.GetData(node, 122, maxIndex);
            _YEAR = DataHelper.GetData(node, 123, maxIndex);
            _VIN = DataHelper.GetData(node, 124, maxIndex);
            _MAKE = DataHelper.GetData(node, 125, maxIndex);
            _MAKE_CODE = DataHelper.GetData(node, 126, maxIndex);
            _MODEL = DataHelper.GetData(node, 127, maxIndex);
            _MODEL_NO = DataHelper.GetData(node, 128, maxIndex);
            _CARLINE = DataHelper.GetData(node, 129, maxIndex);
            _MILEAGE = DataHelper.GetData(node, 130, maxIndex);
            _VEH_TYPE = DataHelper.GetData(node, 131, maxIndex);
            _DOOR_CNT = DataHelper.GetData(node, 132, maxIndex);
            _CYL_CNT = DataHelper.GetData(node, 133, maxIndex);
            _INV_EXT_CLR_DESC = DataHelper.GetData(node, 134, maxIndex);
            _INV_INT_CLR_DESC = DataHelper.GetData(node, 135, maxIndex);
            _INV_EXT_CLR_CODE = DataHelper.GetData(node, 136, maxIndex);
            _INV_INT_CLR_CODE = DataHelper.GetData(node, 137, maxIndex);
            _INV_TRIM = DataHelper.GetData(node, 138, maxIndex);
            _LIST_PRICE = DataHelper.GetData(node, 139, maxIndex);
            _BASE_BALLOON_PAYMENT = DataHelper.GetData(node, 140, maxIndex);
            _BALLOON_MILEAGE_PENALTY = DataHelper.GetData(node, 141, maxIndex);
            _BALLOON_MILEAGE_PENALTY_VALUE = DataHelper.GetData(node, 142, maxIndex);
            _BALLOON_PAYMENT = DataHelper.GetData(node, 143, maxIndex);
            _BALLOON_PCT = DataHelper.GetData(node, 144, maxIndex);
            _BALLOON_TYPE = DataHelper.GetData(node, 145, maxIndex);
            _STANDARD_BALLOON_MILEAGE = DataHelper.GetData(node, 146, maxIndex);
            _EXPECTED_BALLOON_MILEAGE = DataHelper.GetData(node, 147, maxIndex);
            _LIFE_LEVEL_PREM = DataHelper.GetData(node, 148, maxIndex);
            _STATUS_CODE = DataHelper.GetData(node, 149, maxIndex);
            _CASH_DOWN_AMT = DataHelper.GetData(node, 150, maxIndex);
            _CASH_INCEPT_AMT = DataHelper.GetData(node, 151, maxIndex);
            _MSRP_HARD_ADDS = DataHelper.GetData(node, 152, maxIndex);
            _TRADE_INCEPT_AMT = DataHelper.GetData(node, 153, maxIndex);
            _SEC_DEPOSIT = DataHelper.GetData(node, 154, maxIndex);
            _REBATE_INCEPT_AMT = DataHelper.GetData(node, 155, maxIndex);
            _INIT_MP = DataHelper.GetData(node, 156, maxIndex);
            _REBATE_DOWN_AMT = DataHelper.GetData(node, 157, maxIndex);
            _EXT_MNTH_INT_ON_LEV = DataHelper.GetData(node, 158, maxIndex);
            _TRADE_DOWN_AMT = DataHelper.GetData(node, 159, maxIndex);
            _MONTHLY_PAYMENT_ADD_ON = DataHelper.GetData(node, 160, maxIndex);
            _EQUITY_DEFICIT = DataHelper.GetData(node, 161, maxIndex);
            _SLSPERSON1_LAST_NAME = DataHelper.GetData(node, 162, maxIndex);
            _SLSPERSON1_FIRST_NAME = DataHelper.GetData(node, 163, maxIndex);
            _REBATE4 = DataHelper.GetData(node, 164, maxIndex);
            _REBATE5 = DataHelper.GetData(node, 165, maxIndex);
            _SUFFIX = DataHelper.GetData(node, 166, maxIndex);
            _TRADE1_STOCK_NUMBER = DataHelper.GetData(node, 167, maxIndex);
            _TRADE2_STOCK_NUMBER = DataHelper.GetData(node, 168, maxIndex);
            _TRADE3_STOCK_NUMBER = DataHelper.GetData(node, 169, maxIndex);
            _ACQUIRED_DATE = DataHelper.GetData(node, 170, maxIndex);
            _RCPT_DATE = DataHelper.GetData(node, 171, maxIndex);
            _INV_AMT = DataHelper.GetData(node, 172, maxIndex);
            _RECON_AMT = DataHelper.GetData(node, 173, maxIndex);
            _RECON_COST = DataHelper.GetData(node, 174, maxIndex);
            _I_COMPANY = DataHelper.GetData(node, 175, maxIndex);
            _INV_ACCT = DataHelper.GetData(node, 176, maxIndex);
            _SM_DEAL_TYPE = DataHelper.GetData(node, 177, maxIndex);
            _STOCK_TYPE = DataHelper.GetData(node, 178, maxIndex);
            _DLR_SELECT = DataHelper.GetData(node, 179, maxIndex);
            _USE_TAX_RATE = DataHelper.GetData(node, 180, maxIndex);
            _ADD_EQUITY_DEFICIT_TO_CAP = DataHelper.GetData(node, 181, maxIndex);
            _GENERAL_TAX_RATE = DataHelper.GetData(node, 182, maxIndex);
            _PUP1_AMT = DataHelper.GetData(node, 183, maxIndex);
            _PUP2_AMT = DataHelper.GetData(node, 184, maxIndex);
            _PUP3_AMT = DataHelper.GetData(node, 185, maxIndex);
            _IN_SERVICE_DATE = DataHelper.GetData(node, 186, maxIndex);
            _POLL_DATE = DataHelper.GetData(node, 187, maxIndex);
            _POLL_TIME = DataHelper.GetData(node, 188, maxIndex);
            _VEH_LOC = DataHelper.GetData(node, 189, maxIndex);



            _VEH_UDF1 = DataHelper.GetData(node, 190, maxIndex);
            _VEH_UDF2 = DataHelper.GetData(node, 191, maxIndex);
            _VEH_UDF3 = DataHelper.GetData(node, 192, maxIndex);
            _VEH_UDF4 = DataHelper.GetData(node, 193, maxIndex);
            _VEH_UDF5 = DataHelper.GetData(node, 194, maxIndex);
            _VEH_UDF6 = DataHelper.GetData(node, 195, maxIndex);
            _VEH_UDF7 = DataHelper.GetData(node, 196, maxIndex);
            _VEH_UDF8 = DataHelper.GetData(node, 197, maxIndex);

            _SALE_ACCOUNT = DataHelper.GetData(node, 198, maxIndex);

            _INVEN_SRC = DataHelper.GetData(node, 199, maxIndex);
            _INVEN_SRC_NAME = DataHelper.GetData(node, 200, maxIndex);
        }


        /// <summary>
        /// Initializes a new instance of the Deal class.
        /// This constructor accepts an IDataRecord representing a line item of a csv reader used to populate the object
        /// </summary>
        /// <param name="record">An IDataRecord representing a line item of a csv reader used to populate the object.</param>
        internal Deal(IDataRecord record, int maxIndex)
        {
            _DEAL_NUMBER = DataHelper.GetData(record, 0, maxIndex);
            _DEAL_TYPE = DataHelper.GetData(record, 1, maxIndex);
            _STATUS = DataHelper.GetData(record, 2, maxIndex);
            _TRANSACTION_TYPE = DataHelper.GetData(record, 3, maxIndex);
            _LAST_UPDATED_DATE = DataHelper.GetData(record, 4, maxIndex);
            _EFFECTIVE_DATE = DataHelper.GetData(record, 5, maxIndex);
            _TRANSACTION_DATE = DataHelper.GetData(record, 6, maxIndex);
            _FUNDED_DATE = DataHelper.GetData(record, 7, maxIndex);
            _TURNED_DATE = DataHelper.GetData(record, 8, maxIndex);
            _CUSTOMER_NUMBER = DataHelper.GetData(record, 9, maxIndex);
            _BUSINESS_NAME = DataHelper.GetData(record, 10, maxIndex);

            //NOT POPULATED FOR PRIVACY/LEGAL REASONS

            //_FIRST_NAME = DataHelper.GetData(record, 11, maxIndex);
            //_MIDDLE_INITIAL = DataHelper.GetData(record, 12, maxIndex);
            //_LAST_NAME = DataHelper.GetData(record, 13, maxIndex);
            //_ADDRESS1 = DataHelper.GetData(record, 14, maxIndex);
            //_ADDRESS2 = DataHelper.GetData(record, 15, maxIndex);
            //_CITY = DataHelper.GetData(record, 16, maxIndex);
            //_STATE = DataHelper.GetData(record, 17, maxIndex);
            _ZIPCODE = DataHelper.GetData(record, 18, maxIndex);
            //_CUST_PHONE_NUMBER = DataHelper.GetData(record, 19, maxIndex);
            //_CUST_MOBILE_NUMBER = DataHelper.GetData(record, 20, maxIndex);
            //_CUST_BUS_PHONE_NUMBER = DataHelper.GetData(record, 21, maxIndex);
            //_CUST_WORK_PHONE_EXT = DataHelper.GetData(record, 22, maxIndex);
            //_CUST_FAX_PHONE = DataHelper.GetData(record, 23, maxIndex);
            //_CUST_LIC_NO = DataHelper.GetData(record, 24, maxIndex);
            //_EMAIL = DataHelper.GetData(record, 25, maxIndex);
            //_CO_CUSTOMER_NUMBER = DataHelper.GetData(record, 26, maxIndex);
            //_CO_FIRST_NAME = DataHelper.GetData(record, 27, maxIndex);
            //_CO_MIDDLE_INITIAL = DataHelper.GetData(record, 28, maxIndex);
            //_CO_LAST_NAME = DataHelper.GetData(record, 29, maxIndex);
            //_CO_SUFFIX = DataHelper.GetData(record, 30, maxIndex);
            //_CO_SALUTATION = DataHelper.GetData(record, 31, maxIndex);
            //_CO_ADDRESS1 = DataHelper.GetData(record, 32, maxIndex);
            //_CO_CITY = DataHelper.GetData(record, 33, maxIndex);
            //_CO_STATE = DataHelper.GetData(record, 34, maxIndex);
            //_CO_ZIPCODE = DataHelper.GetData(record, 35, maxIndex);
            //_CO_PHONE_NUMBER = DataHelper.GetData(record, 36, maxIndex);
            //_CO_EMAIL = DataHelper.GetData(record, 37, maxIndex);
            //_CO_BUSINESS_NAME = DataHelper.GetData(record, 38, maxIndex);
            //_CO_BIRTH_DATE = DataHelper.GetData(record, 39, maxIndex);
            //_CO_LIC_NO = DataHelper.GetData(record, 40, maxIndex);


            _REG_STATE = DataHelper.GetData(record, 41, maxIndex);
            
            //_CUST_BIRTH_DATE = DataHelper.GetData(record, 42, maxIndex);
            
            _BANK_CODE = DataHelper.GetData(record, 43, maxIndex);
            _BANK_NAME = DataHelper.GetData(record, 44, maxIndex);
            _BANK_ADDRESS = DataHelper.GetData(record, 45, maxIndex);
            _BANK_CITY = DataHelper.GetData(record, 46, maxIndex);
            _BANK_STATE = DataHelper.GetData(record, 47, maxIndex);
            _BANK_ZIP = DataHelper.GetData(record, 48, maxIndex);
            _BANK_PHONE_NUMBER = DataHelper.GetData(record, 49, maxIndex);
            _PURCHASE_PRICE = DataHelper.GetData(record, 50, maxIndex);
            _DAYS_TO_FIRST_PAYMENT = DataHelper.GetData(record, 51, maxIndex);
            _DEPOSIT = DataHelper.GetData(record, 52, maxIndex);
            _TOTAL_SELL_PRICE = DataHelper.GetData(record, 53, maxIndex);
            _TOTAL_SRP = DataHelper.GetData(record, 54, maxIndex);
            _CASH_DOWN = DataHelper.GetData(record, 55, maxIndex);
            _APR = DataHelper.GetData(record, 56, maxIndex);
            _BUY_RATE = DataHelper.GetData(record, 57, maxIndex);
            _MONTHLY_PAYMENT = DataHelper.GetData(record, 58, maxIndex);
            _MSRP = DataHelper.GetData(record, 59, maxIndex);
            _REBATE = DataHelper.GetData(record, 60, maxIndex);
            _REBATE2 = DataHelper.GetData(record, 61, maxIndex);
            _REBATE3 = DataHelper.GetData(record, 62, maxIndex);
            _TERMS = DataHelper.GetData(record, 63, maxIndex);
            _HOLDBACK = DataHelper.GetData(record, 64, maxIndex);
            _VEH_COST = DataHelper.GetData(record, 65, maxIndex);
            _PACK = DataHelper.GetData(record, 66, maxIndex);
            _AMOUNT = DataHelper.GetData(record, 67, maxIndex);
            _PRE_PAID_INTRST = DataHelper.GetData(record, 68, maxIndex);
            _FIN_CHARGE = DataHelper.GetData(record, 69, maxIndex);
            _TOT_DLR_PROFIT = DataHelper.GetData(record, 70, maxIndex);
            _TAX_RATE = DataHelper.GetData(record, 71, maxIndex);
            _CITY_TAX_RATE = DataHelper.GetData(record, 72, maxIndex);
            _COUNTY_TAX_RATE = DataHelper.GetData(record, 73, maxIndex);
            _WARR_CNTY_RATE = DataHelper.GetData(record, 74, maxIndex);
            _WARR_STATE = DataHelper.GetData(record, 75, maxIndex);
            _WARR_STATE_RATE = DataHelper.GetData(record, 76, maxIndex);
            _WARR_VENDOR = DataHelper.GetData(record, 77, maxIndex);
            _LIEN_PAYOFF = DataHelper.GetData(record, 78, maxIndex);
            _LIEN_PAYOFF2 = DataHelper.GetData(record, 79, maxIndex);
            _TRADE_MAKE = DataHelper.GetData(record, 80, maxIndex);
            _TRADE_MODEL = DataHelper.GetData(record, 81, maxIndex);
            _TRADE_YEAR = DataHelper.GetData(record, 82, maxIndex);
            _TRADE_VIN = DataHelper.GetData(record, 83, maxIndex);
            _TRADE_MILEAGE = DataHelper.GetData(record, 84, maxIndex);
            _TRADE_VALUE = DataHelper.GetData(record, 85, maxIndex);
            _TRADE_LIEN = DataHelper.GetData(record, 86, maxIndex);
            _TRADE2_MAKE = DataHelper.GetData(record, 87, maxIndex);
            _TRADE2_MODEL = DataHelper.GetData(record, 88, maxIndex);
            _TRADE2_YEAR = DataHelper.GetData(record, 89, maxIndex);
            _TRADE2_VIN = DataHelper.GetData(record, 90, maxIndex);
            _TRADE2_MILEAGE = DataHelper.GetData(record, 91, maxIndex);
            _TRADE2_VALUE = DataHelper.GetData(record, 92, maxIndex);
            _TRADE2_LIEN = DataHelper.GetData(record, 93, maxIndex);
            _TRADE2_ACV = DataHelper.GetData(record, 94, maxIndex);
            _TRADE_ACV = DataHelper.GetData(record, 95, maxIndex);
            _FIN_RES = DataHelper.GetData(record, 96, maxIndex);
            _FE_GROSS = DataHelper.GetData(record, 97, maxIndex);
            _WARR_RES = DataHelper.GetData(record, 98, maxIndex);
            _AH_RES = DataHelper.GetData(record, 99, maxIndex);
            _LIFE_RES = DataHelper.GetData(record, 100, maxIndex);
            _SLSPERSON1_ID = DataHelper.GetData(record, 101, maxIndex);
            _SALESPERSON1 = DataHelper.GetData(record, 102, maxIndex);
            _SLSPERSON2_ID = DataHelper.GetData(record, 103, maxIndex);
            _SALESPERSON2 = DataHelper.GetData(record, 104, maxIndex);
            _SLSMANAGER_ID = DataHelper.GetData(record, 105, maxIndex);
            _SALESMANAGER = DataHelper.GetData(record, 106, maxIndex);
            _FI_MGR_ID = DataHelper.GetData(record, 107, maxIndex);
            _FI_MANAGER = DataHelper.GetData(record, 108, maxIndex);
            _LEASE_MILEAGE_PENALTY = DataHelper.GetData(record, 109, maxIndex);
            _LEASE_MILEAGE_PENALTY_VALUE = DataHelper.GetData(record, 110, maxIndex);
            _STANDARD_LEASE_MILEAGE = DataHelper.GetData(record, 111, maxIndex);
            _EXPECTED_LEASE_MILEAGE = DataHelper.GetData(record, 112, maxIndex);
            _MONEY_FACTOR = DataHelper.GetData(record, 113, maxIndex);
            _NET_RESIDUAL_VALUE = DataHelper.GetData(record, 114, maxIndex);
            _RESIDUAL = DataHelper.GetData(record, 115, maxIndex);
            _APR_LEASE_FLAG = DataHelper.GetData(record, 116, maxIndex);
            _BASE_PAYMENT = DataHelper.GetData(record, 117, maxIndex);
            _BASE_RESIDUAL_VALUE = DataHelper.GetData(record, 118, maxIndex);
            _CAPITALIZE_LEASE_TAX = DataHelper.GetData(record, 119, maxIndex);
            _CAP_REDUCTION = DataHelper.GetData(record, 120, maxIndex);
            _STOCK_NUMBER = DataHelper.GetData(record, 121, maxIndex);
            _NEW = DataHelper.GetData(record, 122, maxIndex);
            _YEAR = DataHelper.GetData(record, 123, maxIndex);
            _VIN = DataHelper.GetData(record, 124, maxIndex);
            _MAKE = DataHelper.GetData(record, 125, maxIndex);
            _MAKE_CODE = DataHelper.GetData(record, 126, maxIndex);
            _MODEL = DataHelper.GetData(record, 127, maxIndex);
            _MODEL_NO = DataHelper.GetData(record, 128, maxIndex);
            _CARLINE = DataHelper.GetData(record, 129, maxIndex);
            _MILEAGE = DataHelper.GetData(record, 130, maxIndex);
            _VEH_TYPE = DataHelper.GetData(record, 131, maxIndex);
            _DOOR_CNT = DataHelper.GetData(record, 132, maxIndex);
            _CYL_CNT = DataHelper.GetData(record, 133, maxIndex);
            _INV_EXT_CLR_DESC = DataHelper.GetData(record, 134, maxIndex);
            _INV_INT_CLR_DESC = DataHelper.GetData(record, 135, maxIndex);
            _INV_EXT_CLR_CODE = DataHelper.GetData(record, 136, maxIndex);
            _INV_INT_CLR_CODE = DataHelper.GetData(record, 137, maxIndex);
            _INV_TRIM = DataHelper.GetData(record, 138, maxIndex);
            _LIST_PRICE = DataHelper.GetData(record, 139, maxIndex);
            _BASE_BALLOON_PAYMENT = DataHelper.GetData(record, 140, maxIndex);
            _BALLOON_MILEAGE_PENALTY = DataHelper.GetData(record, 141, maxIndex);
            _BALLOON_MILEAGE_PENALTY_VALUE = DataHelper.GetData(record, 142, maxIndex);
            _BALLOON_PAYMENT = DataHelper.GetData(record, 143, maxIndex);
            _BALLOON_PCT = DataHelper.GetData(record, 144, maxIndex);
            _BALLOON_TYPE = DataHelper.GetData(record, 145, maxIndex);
            _STANDARD_BALLOON_MILEAGE = DataHelper.GetData(record, 146, maxIndex);
            _EXPECTED_BALLOON_MILEAGE = DataHelper.GetData(record, 147, maxIndex);
            _LIFE_LEVEL_PREM = DataHelper.GetData(record, 148, maxIndex);
            _STATUS_CODE = DataHelper.GetData(record, 149, maxIndex);
            _CASH_DOWN_AMT = DataHelper.GetData(record, 150, maxIndex);
            _CASH_INCEPT_AMT = DataHelper.GetData(record, 151, maxIndex);
            _MSRP_HARD_ADDS = DataHelper.GetData(record, 152, maxIndex);
            _TRADE_INCEPT_AMT = DataHelper.GetData(record, 153, maxIndex);
            _SEC_DEPOSIT = DataHelper.GetData(record, 154, maxIndex);
            _REBATE_INCEPT_AMT = DataHelper.GetData(record, 155, maxIndex);
            _INIT_MP = DataHelper.GetData(record, 156, maxIndex);
            _REBATE_DOWN_AMT = DataHelper.GetData(record, 157, maxIndex);
            _EXT_MNTH_INT_ON_LEV = DataHelper.GetData(record, 158, maxIndex);
            _TRADE_DOWN_AMT = DataHelper.GetData(record, 159, maxIndex);
            _MONTHLY_PAYMENT_ADD_ON = DataHelper.GetData(record, 160, maxIndex);
            _EQUITY_DEFICIT = DataHelper.GetData(record, 161, maxIndex);
            _SLSPERSON1_LAST_NAME = DataHelper.GetData(record, 162, maxIndex);
            _SLSPERSON1_FIRST_NAME = DataHelper.GetData(record, 163, maxIndex);
            _REBATE4 = DataHelper.GetData(record, 164, maxIndex);
            _REBATE5 = DataHelper.GetData(record, 165, maxIndex);
            _SUFFIX = DataHelper.GetData(record, 166, maxIndex);
            _TRADE1_STOCK_NUMBER = DataHelper.GetData(record, 167, maxIndex);
            _TRADE2_STOCK_NUMBER = DataHelper.GetData(record, 168, maxIndex);
            _TRADE3_STOCK_NUMBER = DataHelper.GetData(record, 169, maxIndex);
            _ACQUIRED_DATE = DataHelper.GetData(record, 170, maxIndex);
            _RCPT_DATE = DataHelper.GetData(record, 171, maxIndex);
            _INV_AMT = DataHelper.GetData(record, 172, maxIndex);
            _RECON_AMT = DataHelper.GetData(record, 173, maxIndex);
            _RECON_COST = DataHelper.GetData(record, 174, maxIndex);
            _I_COMPANY = DataHelper.GetData(record, 175, maxIndex);
            _INV_ACCT = DataHelper.GetData(record, 176, maxIndex);
            _SM_DEAL_TYPE = DataHelper.GetData(record, 177, maxIndex);
            _STOCK_TYPE = DataHelper.GetData(record, 178, maxIndex);
            _DLR_SELECT = DataHelper.GetData(record, 179, maxIndex);
            _USE_TAX_RATE = DataHelper.GetData(record, 180, maxIndex);
            _ADD_EQUITY_DEFICIT_TO_CAP = DataHelper.GetData(record, 181, maxIndex);
            _GENERAL_TAX_RATE = DataHelper.GetData(record, 182, maxIndex);
            _PUP1_AMT = DataHelper.GetData(record, 183, maxIndex);
            _PUP2_AMT = DataHelper.GetData(record, 184, maxIndex);
            _PUP3_AMT = DataHelper.GetData(record, 185, maxIndex);
            _IN_SERVICE_DATE = DataHelper.GetData(record, 186, maxIndex);
            _POLL_DATE = DataHelper.GetData(record, 187, maxIndex);
            _POLL_TIME = DataHelper.GetData(record, 188, maxIndex);
            _VEH_LOC = DataHelper.GetData(record, 189, maxIndex);

            _VEH_UDF1 = DataHelper.GetData(record, 190, maxIndex);
            _VEH_UDF2 = DataHelper.GetData(record, 191, maxIndex);
            _VEH_UDF3 = DataHelper.GetData(record, 192, maxIndex);
            _VEH_UDF4 = DataHelper.GetData(record, 193, maxIndex);
            _VEH_UDF5 = DataHelper.GetData(record, 194, maxIndex);
            _VEH_UDF6 = DataHelper.GetData(record, 195, maxIndex);
            _VEH_UDF7 = DataHelper.GetData(record, 196, maxIndex);
            _VEH_UDF8 = DataHelper.GetData(record, 197, maxIndex);
            _SALE_ACCOUNT = DataHelper.GetData(record, 198, maxIndex);

            _INVEN_SRC = DataHelper.GetData(record, 199, maxIndex);
            _INVEN_SRC_NAME = DataHelper.GetData(record, 200, maxIndex);
        }




        /// <summary>
        /// Writes the Deal in a Pipe "|" separated value format, following the IPsvSerializable interface.
        /// Writes all values in alphabetical order by property name.
        /// </summary>
        /// <param name="outputStream">The output stream to which to write.</param>
        public void WritePsv(StreamWriter outputStream)
        {
            StringBuilder b = new StringBuilder();

            b.Append(_ACQUIRED_DATE);
            b.Append("|");

            b.Append(_BUSINESS_NAME);
            b.Append("|");

            b.Append(_I_COMPANY);
            b.Append("|");

            b.Append(_ADDRESS1);
            b.Append("|");

            b.Append(_ADDRESS2);
            b.Append("|");

            b.Append(_CITY);
            b.Append("|");

            b.Append(_STATE);
            b.Append("|");

            b.Append(_ZIPCODE);
            b.Append("|");

            b.Append(_EFFECTIVE_DATE);
            b.Append("|");

            b.Append(_INV_EXT_CLR_DESC);
            b.Append("|");

            b.Append(_NEW);
            b.Append("|");

            b.Append(_TRADE_VIN);
            b.Append("|");

            b.Append(_TRADE2_VIN);
            b.Append("|");

            b.Append(_FE_GROSS);
            b.Append("|");

            b.Append(_INV_INT_CLR_DESC);
            b.Append("|");

            b.Append(_INV_ACCT);
            b.Append("|");

            b.Append(_MAKE);
            b.Append("|");

            b.Append(_MILEAGE);
            b.Append("|");

            b.Append(_MODEL);
            b.Append("|");

            b.Append(_LAST_UPDATED_DATE);
            b.Append("|");

            b.Append(_PACK);
            b.Append("|");

            b.Append(_PURCHASE_PRICE);
            b.Append("|");

            b.Append(_RCPT_DATE);
            b.Append("|");

            b.Append(_RECON_COST);
            b.Append("|");

            b.Append(_TRANSACTION_TYPE);
            b.Append("|");

            b.Append(_SLSPERSON1_ID);
            b.Append("|");

            b.Append(_SALESPERSON1);
            b.Append("|");

            b.Append(_DEAL_NUMBER);
            b.Append("|");

            b.Append(_STATUS);
            b.Append("|");

            b.Append(_STOCK_NUMBER);
            b.Append("|");

            b.Append(_TOTAL_SELL_PRICE);
            b.Append("|");

            b.Append(_TRANSACTION_DATE);
            b.Append("|");

            b.Append(_VEH_COST);
            b.Append("|");

            b.Append(_STATUS_CODE);
            b.Append("|");

            b.Append(_VEH_TYPE);
            b.Append("|");

            b.Append(_YEAR);
            b.Append("|");

            b.Append(_VIN);
            b.Append("|");

            b.Append(_VEH_LOC);
            b.Append("|");

            b.Append(_TOT_DLR_PROFIT);
            b.Append("|");

            b.Append(_FIN_RES);
            b.Append("|");

            b.Append(_WARR_RES);
            b.Append("|");

            b.Append(_AH_RES);
            b.Append("|");

            b.Append(_LIFE_RES);
            b.Append("|");

            b.Append(_DEAL_TYPE);
            b.Append("|");

            b.Append(_FUNDED_DATE);
            b.Append("|");
            
            b.Append(_TURNED_DATE);
            b.Append("|");
            
            b.Append(_CUSTOMER_NUMBER);
            b.Append("|");
            
            b.Append(_FIRST_NAME);
            b.Append("|");
            
            b.Append(_MIDDLE_INITIAL);
            b.Append("|");
            
            b.Append(_LAST_NAME);
            b.Append("|");
                                    
            b.Append(_CUST_PHONE_NUMBER);
            b.Append("|");
            
            b.Append(_CUST_MOBILE_NUMBER);
            b.Append("|");
            
            b.Append(_CUST_BUS_PHONE_NUMBER);
            b.Append("|");
            
            b.Append(_CUST_WORK_PHONE_EXT);
            b.Append("|");
            
            b.Append(_CUST_FAX_PHONE);
            b.Append("|");
            
            b.Append(_CUST_LIC_NO);
            b.Append("|");
            
            b.Append(_EMAIL);
            b.Append("|");
            
            b.Append(_CO_CUSTOMER_NUMBER);
            b.Append("|");
            
            b.Append(_CO_FIRST_NAME);
            b.Append("|");
            
            b.Append(_CO_MIDDLE_INITIAL);
            b.Append("|");
            
            b.Append(_CO_LAST_NAME);
            b.Append("|");
            
            b.Append(_CO_SUFFIX);
            b.Append("|");
            
            b.Append(_CO_SALUTATION);
            b.Append("|");
            
            b.Append(_CO_ADDRESS1);
            b.Append("|");
            
            b.Append(_CO_CITY);
            b.Append("|");
            
            b.Append(_CO_STATE);
            b.Append("|");
            
            b.Append(_CO_ZIPCODE);
            b.Append("|");
            
            b.Append(_CO_PHONE_NUMBER);
            b.Append("|");
            
            b.Append(_CO_EMAIL);
            b.Append("|");
            
            b.Append(_CO_BUSINESS_NAME);
            b.Append("|");
            
            b.Append(_CO_BIRTH_DATE);
            b.Append("|");
            
            b.Append(_CO_LIC_NO);
            b.Append("|");
            
            b.Append(_REG_STATE);
            b.Append("|");
            
            b.Append(_CUST_BIRTH_DATE);
            b.Append("|");
            
            b.Append(_BANK_CODE);
            b.Append("|");
            
            b.Append(_BANK_NAME);
            b.Append("|");
            
            b.Append(_BANK_ADDRESS);
            b.Append("|");
            
            b.Append(_BANK_CITY);
            b.Append("|");
            
            b.Append(_BANK_STATE);
            b.Append("|");
            
            b.Append(_BANK_ZIP);
            b.Append("|");
            
            b.Append(_BANK_PHONE_NUMBER);
            b.Append("|");
            
            b.Append(_DAYS_TO_FIRST_PAYMENT);
            b.Append("|");
            
            b.Append(_DEPOSIT);
            b.Append("|");
            
            b.Append(_TOTAL_SRP);
            b.Append("|");
            
            b.Append(_CASH_DOWN);
            b.Append("|");
            
            b.Append(_APR);
            b.Append("|");
            
            b.Append(_BUY_RATE);
            b.Append("|");
            
            b.Append(_MONTHLY_PAYMENT);
            b.Append("|");
            
            b.Append(_MSRP);
            b.Append("|");
            
            b.Append(_REBATE);
            b.Append("|");
            
            b.Append(_REBATE2);
            b.Append("|");
            
            b.Append(_REBATE3);
            b.Append("|");
            
            b.Append(_TERMS);
            b.Append("|");
            
            b.Append(_HOLDBACK);
            b.Append("|");
            
            b.Append(_AMOUNT);
            b.Append("|");
            
            b.Append(_PRE_PAID_INTRST);
            b.Append("|");
            
            b.Append(_FIN_CHARGE);
            b.Append("|");
            
            b.Append(_TAX_RATE);
            b.Append("|");
            
            b.Append(_CITY_TAX_RATE);
            b.Append("|");
            
            b.Append(_COUNTY_TAX_RATE);
            b.Append("|");
            
            b.Append(_WARR_CNTY_RATE);
            b.Append("|");
            
            b.Append(_WARR_STATE);
            b.Append("|");
            
            b.Append(_WARR_STATE_RATE);
            b.Append("|");
            
            b.Append(_WARR_VENDOR);
            b.Append("|");
            
            b.Append(_LIEN_PAYOFF);
            b.Append("|");
            
            b.Append(_LIEN_PAYOFF2);
            b.Append("|");
            
            b.Append(_TRADE_MAKE);
            b.Append("|");
            
            b.Append(_TRADE_MODEL);
            b.Append("|");
            
            b.Append(_TRADE_YEAR);
            b.Append("|");
                        
            b.Append(_TRADE_MILEAGE);
            b.Append("|");
            
            b.Append(_TRADE_VALUE);
            b.Append("|");
            
            b.Append(_TRADE_LIEN);
            b.Append("|");
            
            b.Append(_TRADE2_MAKE);
            b.Append("|");
            
            b.Append(_TRADE2_MODEL);
            b.Append("|");
            
            b.Append(_TRADE2_YEAR);
            b.Append("|");
                        
            b.Append(_TRADE2_MILEAGE);
            b.Append("|");
            
            b.Append(_TRADE2_VALUE);
            b.Append("|");
            
            b.Append(_TRADE2_LIEN);
            b.Append("|");
            
            b.Append(_TRADE2_ACV);
            b.Append("|");
            
            b.Append(_TRADE_ACV);
            b.Append("|");
            
            b.Append(_SLSPERSON2_ID);
            b.Append("|");
            
            b.Append(_SALESPERSON2);
            b.Append("|");
            
            b.Append(_SLSMANAGER_ID);
            b.Append("|");
            
            b.Append(_SALESMANAGER);
            b.Append("|");
            
            b.Append(_FI_MGR_ID);
            b.Append("|");
            
            b.Append(_FI_MANAGER);
            b.Append("|");
            
            b.Append(_LEASE_MILEAGE_PENALTY);
            b.Append("|");
            
            b.Append(_LEASE_MILEAGE_PENALTY_VALUE);
            b.Append("|");
            
            b.Append(_STANDARD_LEASE_MILEAGE);
            b.Append("|");
            
            b.Append(_EXPECTED_LEASE_MILEAGE);
            b.Append("|");
            
            b.Append(_MONEY_FACTOR);
            b.Append("|");
            
            b.Append(_NET_RESIDUAL_VALUE);
            b.Append("|");
            
            b.Append(_RESIDUAL);
            b.Append("|");
            
            b.Append(_APR_LEASE_FLAG);
            b.Append("|");
            
            b.Append(_BASE_PAYMENT);
            b.Append("|");
            
            b.Append(_BASE_RESIDUAL_VALUE);
            b.Append("|");
            
            b.Append(_CAPITALIZE_LEASE_TAX);
            b.Append("|");
            
            b.Append(_CAP_REDUCTION);
            b.Append("|");
                        
            b.Append(_MAKE_CODE);
            b.Append("|");

            b.Append(_MODEL_NO);
            b.Append("|");

            b.Append(_CARLINE);
            b.Append("|");

            b.Append(_DOOR_CNT);
            b.Append("|");

            b.Append(_CYL_CNT);
            b.Append("|");

            b.Append(_INV_EXT_CLR_CODE);
            b.Append("|");

            b.Append(_INV_INT_CLR_CODE);
            b.Append("|");

            b.Append(_INV_TRIM);
            b.Append("|");

            b.Append(_LIST_PRICE);
            b.Append("|");

            b.Append(_BASE_BALLOON_PAYMENT);
            b.Append("|");

            b.Append(_BALLOON_MILEAGE_PENALTY);
            b.Append("|");

            b.Append(_BALLOON_MILEAGE_PENALTY_VALUE);
            b.Append("|");

            b.Append(_BALLOON_PAYMENT);
            b.Append("|");

            b.Append(_BALLOON_PCT);
            b.Append("|");

            b.Append(_BALLOON_TYPE);
            b.Append("|");

            b.Append(_STANDARD_BALLOON_MILEAGE);
            b.Append("|");

            b.Append(_EXPECTED_BALLOON_MILEAGE);
            b.Append("|");

            b.Append(_LIFE_LEVEL_PREM);
            b.Append("|");

            b.Append(_CASH_DOWN_AMT);
            b.Append("|");

            b.Append(_CASH_INCEPT_AMT);
            b.Append("|");

            b.Append(_MSRP_HARD_ADDS);
            b.Append("|");

            b.Append(_TRADE_INCEPT_AMT);
            b.Append("|");

            b.Append(_SEC_DEPOSIT);
            b.Append("|");

            b.Append(_REBATE_INCEPT_AMT);
            b.Append("|");

            b.Append(_INIT_MP);
            b.Append("|");

            b.Append(_REBATE_DOWN_AMT);
            b.Append("|");

            b.Append(_EXT_MNTH_INT_ON_LEV);
            b.Append("|");

            b.Append(_TRADE_DOWN_AMT);
            b.Append("|");

            b.Append(_MONTHLY_PAYMENT_ADD_ON);
            b.Append("|");

            b.Append(_EQUITY_DEFICIT);
            b.Append("|");

            b.Append(_SLSPERSON1_LAST_NAME);
            b.Append("|");

            b.Append(_SLSPERSON1_FIRST_NAME);
            b.Append("|");

            b.Append(_REBATE4);
            b.Append("|");

            b.Append(_REBATE5);
            b.Append("|");

            b.Append(_SUFFIX);
            b.Append("|");

            b.Append(_TRADE1_STOCK_NUMBER);
            b.Append("|");

            b.Append(_TRADE2_STOCK_NUMBER);
            b.Append("|");

            b.Append(_TRADE3_STOCK_NUMBER);
            b.Append("|");

            b.Append(_INV_AMT);
            b.Append("|");

            b.Append(_RECON_AMT);
            b.Append("|");

            b.Append(_SM_DEAL_TYPE);
            b.Append("|");

            b.Append(_STOCK_TYPE);
            b.Append("|");

            b.Append(_DLR_SELECT);
            b.Append("|");

            b.Append(_USE_TAX_RATE);
            b.Append("|");

            b.Append(_ADD_EQUITY_DEFICIT_TO_CAP);
            b.Append("|");

            b.Append(_GENERAL_TAX_RATE);
            b.Append("|");

            b.Append(_PUP1_AMT);
            b.Append("|");

            b.Append(_PUP2_AMT);
            b.Append("|");

            b.Append(_PUP3_AMT);
            b.Append("|");

            b.Append(_IN_SERVICE_DATE);
            b.Append("|");

            b.Append(_POLL_DATE);
            b.Append("|");

            b.Append(_POLL_TIME);
            b.Append("|");

            b.Append(_VEH_UDF1);
            b.Append("|");

            b.Append(_VEH_UDF2);
            b.Append("|");

            b.Append(_VEH_UDF3);
            b.Append("|");

            b.Append(_VEH_UDF4);
            b.Append("|");

            b.Append(_VEH_UDF5);
            b.Append("|");

            b.Append(_VEH_UDF6);
            b.Append("|");

            b.Append(_VEH_UDF7);
            b.Append("|");

            b.Append(_VEH_UDF8);
            b.Append("|");

            b.Append(_SALE_ACCOUNT);
            b.Append("|");

            b.Append(_INVEN_SRC);
            b.Append("|");

            b.Append(_INVEN_SRC_NAME);

            outputStream.WriteLine(b.ToString());
        }
    }
}