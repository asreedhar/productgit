using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Xml;

//Used to allow this class to be used in the Sis.DomainModel_Test project
[assembly: InternalsVisibleTo("Firstlook.Sis.DomainModelTest")]
namespace Firstlook.Sis.DomainModel.Facade
{
    /// <summary>
    /// Wrapper class over Sis result set.
    /// </summary>
    internal abstract class SisResult
    {
        /// <summary>
        /// Firstlook resultset version number.
        /// </summary>
        private readonly int firstlookVersion;

        /// <summary>
        /// Sis resultset version number.
        /// </summary>
        private readonly double sisVersion;


        /// <summary>
        /// Gets the Firstlook resultset version number.
        /// </summary>
        public int FirstlookVersion
        {
            get
            {
                return firstlookVersion;
            }
        }

        /// <summary>
        /// Gets the Sis resultset version number.
        /// </summary>
        public double SisVersion
        {
            get
            {
                return sisVersion;
            }
        }


        
        /// <summary>
        /// Initializes a new instance of the SisResult class.
        /// This constructor accepts an XmlDocument representing the response of the Sis web service call.
        /// </summary>
        public SisResult(XmlNode response)
        {
            CheckResponseForErrors(response);

            if (response.SelectSingleNode("/DMS_TRANSACTION/FLO_VERSION") != null
                && !String.IsNullOrEmpty(response.SelectSingleNode("/DMS_TRANSACTION/FLO_VERSION").InnerText))
            {
                firstlookVersion = int.Parse(response.SelectSingleNode("/DMS_TRANSACTION/FLO_VERSION").InnerText);
            }

            if (response.SelectSingleNode("/DMS_TRANSACTION/SIS_VERSION") != null 
                && !String.IsNullOrEmpty(response.SelectSingleNode("/DMS_TRANSACTION/SIS_VERSION").InnerText))
            {
                sisVersion = double.Parse(response.SelectSingleNode("/DMS_TRANSACTION/SIS_VERSION").InnerText);
            }
        }



        /// <summary>
        /// Private method to check the Xml response for errors.
        ///</summary>
        /// <param name="doc"></param>
        private static void CheckResponseForErrors(XmlNode doc)
        {
            //all non web service errors reside in an "ERROR" attribute of the DMS_TRANSACTION node
            if (doc.SelectSingleNode("/DMS_TRANSACTION") != null && doc.SelectSingleNode("/DMS_TRANSACTION").Attributes["ERROR"] != null)
            {
                throw new SisInvalidResponseException(doc.OuterXml);
            }

            //Web Service errors reside in the DMS_ERROR node
            //comm manager down, no dms sessions, etc
            if (doc.SelectSingleNode("//DMS_ERROR") != null && doc.SelectSingleNode("//DMS_ERROR").ChildNodes.Count > 0)
            {
                //throw new SisServiceException("Web service based error in Sis response." + doc.OuterXml);
                throw new SisInvalidResponseException(doc.OuterXml);
            }
        }
    }
}
