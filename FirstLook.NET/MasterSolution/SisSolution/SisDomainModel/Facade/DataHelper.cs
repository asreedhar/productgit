using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Xml;

namespace Firstlook.Sis.DomainModel.Facade
{
    internal static class DataHelper
    {
        public static string GetData(XmlNode node, int index, int maxIndex)
        {
            if (node == null ||
                node.ChildNodes == null ||
                node.ChildNodes.Count < index ||
                node.ChildNodes[index] == null ||
                index > maxIndex)
            {
                return "";
            }

            return node.ChildNodes[index].InnerText;
        }

        public static string GetData(IDataRecord record, int index, int maxIndex)
        {
            if (record == null ||
                record.FieldCount < index ||
                index > maxIndex)
            {
                return "";
            }

            return record.GetString(index);
        }

    }
}
