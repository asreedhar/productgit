using System.Data;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using Firstlook.Sis.DomainModel.Serialization;

//Used to allow this class to be used in the Sis.DomainModel_Test project
[assembly: InternalsVisibleTo("Firstlook.Sis.DomainModelTest")]
namespace Firstlook.Sis.DomainModel.Facade
{
    /// <summary>
    /// An accounting Used Car Entry as returned from Sis, contained in the dealer's DMS.
    /// </summary>
    internal sealed class UsedCarEntry : IPsvSerializable
    {
        /// <summary>
        /// The UsedCarEntry's account number.
        /// </summary>
        private string accountNumber;

        /// <summary>
        /// The UsedCarEntry's company ID.
        /// </summary>
        private string companyId;

        /// <summary>
        /// The UsedCarEntry's control number.
        /// </summary>
        private string controlNumber;

        /// <summary>
        /// The UsedCarEntry's second control number.
        /// </summary>
        private string controlNumber2;

        /// <summary>
        /// The UsedCarEntry's control type.
        /// </summary>
        private string controlType;

        /// <summary>
        /// The UsedCarEntry's second control type.
        /// </summary>
        private string controlType2;

        /// <summary>
        /// The UsedCarEntry's Journal's date.
        /// </summary>
        private string journalDate;

        /// <summary>
        /// The UsedCarEntry's Journal's ID.
        /// </summary>
        private string journalId;

        /// <summary>
        /// The UsedCarEntry's post amount.
        /// </summary>
        private string postAmount;

        /// <summary>
        /// The UsedCarEntry's post description.
        /// </summary>
        private string postDescription;

        /// <summary>
        /// The UsedCarEntry's post sequence.
        /// </summary>
        private string postSequence;

        /// <summary>
        /// The UsedCarEntry's post time.
        /// </summary>
        private string postTime;

        /// <summary>
        /// The UsedCarEntry's product number.
        /// </summary>
        private string productNumber;

        /// <summary>
        /// The UsedCarEntry's product type code.
        /// </summary>
        private string productTypeCode;
        
        /// <summary>
        /// The UsedCarEntry's reference number.
        /// </summary>
        private string referenceNumber;
        
        /// <summary>
        /// The UsedCarEntry's stat count.
        /// </summary>
        private string statCount;














        /// <summary>
        /// Initializes a new instance of the UsedCarEntry class.
        /// This constructor accepts an IDataRecord representing a line item of a csv reader used to populate the object
        /// </summary>
        /// <param name="record">An IDataRecord representing a line item of a csv reader used to populate the object.</param>
        /// <param name="maxIndex"></param>
        internal UsedCarEntry(IDataRecord record, int maxIndex)
        {
            accountNumber = DataHelper.GetData(record, 6, maxIndex);
            companyId = DataHelper.GetData(record, 0, maxIndex);
            controlNumber = DataHelper.GetData(record, 9, maxIndex);
            controlNumber2 = DataHelper.GetData(record, 13, maxIndex);
            controlType = DataHelper.GetData(record, 8, maxIndex);
            controlType2 = DataHelper.GetData(record, 12, maxIndex);
            journalDate = DataHelper.GetData(record, 3, maxIndex);
            journalId = DataHelper.GetData(record, 1, maxIndex);
            postAmount = DataHelper.GetData(record, 7, maxIndex);
            postDescription = DataHelper.GetData(record, 15, maxIndex);
            postSequence = DataHelper.GetData(record, 5, maxIndex);
            postTime = DataHelper.GetData(record, 4, maxIndex);
            productNumber = DataHelper.GetData(record, 11, maxIndex);
            productTypeCode = DataHelper.GetData(record, 10, maxIndex);
            referenceNumber = DataHelper.GetData(record, 2, maxIndex);
            statCount = DataHelper.GetData(record, 14, maxIndex);

        }




        /// <summary>
        /// Writes the UsedCarEntry in a Pipe "|" separated value format, following the IPsvSerializable interface.
        /// Writes all values in alphabetical order by property name.
        /// </summary>
        /// <param name="outputStream">The output stream to which to write.</param>
        public void WritePsv(StreamWriter outputStream)
        {
            StringBuilder b = new StringBuilder();

            b.Append(accountNumber);
            b.Append("|");

            b.Append(companyId);
            b.Append("|");

            b.Append(controlNumber);
            b.Append("|");

            b.Append(controlNumber2);
            b.Append("|");

            b.Append(controlType);
            b.Append("|");

            b.Append(controlType2);
            b.Append("|");

            b.Append(journalDate);
            b.Append("|");

            b.Append(journalId);
            b.Append("|");

            b.Append(postAmount);
            b.Append("|");

            b.Append(postDescription);
            b.Append("|");

            b.Append(postSequence);
            b.Append("|");

            b.Append(postTime);
            b.Append("|");

            b.Append(productNumber);
            b.Append("|");

            b.Append(productTypeCode);
            b.Append("|");

            b.Append(referenceNumber);
            b.Append("|");

            b.Append(statCount);


            outputStream.WriteLine(b.ToString());
        }
    }
}