using System.Data;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Xml;
using Firstlook.Sis.DomainModel.Serialization;

//Used to allow this class to be used in the Sis.DomainModel_Test project
[assembly: InternalsVisibleTo("Firstlook.Sis.DomainModelTest")]
namespace Firstlook.Sis.DomainModel.Facade
{
    /// <summary>
    /// A vehicle as returned from Sis, contained in the dealer's DMS.
    /// XmlDeserialization is used to contruct a Vehicle from an XmlDocument.
    /// </summary>
    internal sealed class Vehicle : IPsvSerializable
    {
        private string _TRANSM_TYPE;
        private string _VIN;
        private string _MAKE;
        private string _MAKE_DESC;
        private string _ACCTG_MAKE;
        private string _VEH_YR;
        private string _MDL_NO;
        private string _MDL_DESC;
        private string _CARLINE;
        private string _BDYSZE;
        private string _EXT_CLR_CODE;
        private string _EXT_CLR_DESC;
        private string _INT_CLR_CODE;
        private string _INT_CLR_DESC;
        private string _ACC_CLR;
        private string _ROOF_CLR;
        private string _AIR_COND;
        private string _NO_AXLES;
        private string _CHASSIS_NO;
        private string _ENG_NO;
        private string _CLASS;
        private string _FUEL_CODE;
        private string _MPG;
        private string _INTR_START_DATE;
        private string _KEY1_NO;
        private string _KEY2_NO;
        private string _VEND_NO;
        private string _FUEL_ECON;
        private string _CYL_CNT;
        private string _MILEAGE;
        private string _POWER_STEER;
        private string _TRANS_NO;
        private string _TRIM;
        private string _TURBO;
        private string _WEIGHT;
        private string _AXLE;
        private string _WHEEL_BASE;
        private string _DRIVE_TYPE;
        private string _NUO;
        private string _UNITS_MEASURE;
        private string _STOCK_NUMBER;
        private string _VEH_LOC;
        private string _RCPT_DATE;
        private string _VEH_STAT_CODE;
        private string _VEH_STAT;
        private string _SLM_NO;
        private string _TI_PAYOFF_AMT;
        private string _TI_STK_NO;
        private string _MEMO1;
        private string _MEMO2;
        private string _PURCH_DATE;
        private string _FP_VEND;
        private string _FP_NAME;
        private string _FP_AMT;
        private string _FP_RATE;
        private string _FP_INTR_START_DATE;
        private string _FP_PAYOFF_DATE;
        private string _ASSEMBLY_PLANT;
        private string _EST_ARRIVAL_DATE;
        private string _FACT_ORD_NO;
        private string _MANUF_SCH_DATE;
        private string _MANUF_STAT;
        private string _MANUF_STAT_DATE;
        private string _OMAL;
        private string _ORD_DATE;
        private string _ORD_TYPE;
        private string _ORD_PROCESS_DATE;
        private string _ORD_PRIORITY;
        private string _PRICE_LVL;
        private string _SPECIAL_ORD_NO;
        private string _BEST_PRICE;
        private string _CODED_COST;
        private string _HOLDBACK;
        private string _INVOICE_AMT;
        private string _INV_AMT;
        private string _INV_AMT2;
        private string _INV_AMT3;
        private string _INV_GL_AMT;
        private string _LIC_FEE;
        private string _LIST_PRICE;
        private string _PREDEL_INSP_AMT;
        private string _SLS_PRICE;
        private string _SLS_TYPE;
        private string _SLS_COST;
        private string _SLS_CODE;
        private string _OPEN_RO_PO;
        private string _RO_PO_TYPE;
        private string _DOOR_CNT;
        private string _INT_TYPE;
        private string _RECON_AMT;
        private string _PACK_AMT;
        private string _DEL_DATE;
        private string _DAYS_IN_STOCK;
        private string _CERTIFIED;
        private string _STICKER_PRICE;
        private string _INV_PRICE1;
        private string _INV_PRICE2;
        private string _SLS_PRICE1;
        private string _SLS_PRICE2;
        private string _BASE_MSRP;
        private string _BASE_SLS_PRICE;
        private string _BASE_SLS_COST;
        private string _COMM_PRICE;
        private string _DRAFT_AMOUNT;
        private string _DLR_RCPT_DATE;
        private string _INV_SEG_CODE;
        private string _VEH_SRC;
        private string _VEH_SRC_NAME;
        private string _INV_ACCT;
        private string _I_COMPANY;
        private string _SOLD_DATE;
        private string _PREV_OWNER;
        private string _ACTVY_DATE;
        private string _CUST_CREATE_DATE;
        private string _CUST_CATEG;
        private string _MDL_TYPE;
        private string _VEH_TYPE;
        private string _POLL_DATE;
        private string _POLL_TIME;
        private string _CATEG_CODE;
        private string _DLR_ACC_COST;
        private string _ACV;
        private string _WHSL_FLAG;

        private string _VEH_UDF1;
        private string _VEH_UDF2;
        private string _VEH_UDF3;
        private string _VEH_UDF4;
        private string _VEH_UDF5;
        private string _VEH_UDF6;
        private string _VEH_UDF7;
        private string _VEH_UDF8;
        private string _INVEN_SRC;
        private string _INVEN_SRC_NAME;

        private string _OPT_CODE;
        private string _OPT_DESC;
        private string _OPT_PRICE;
        private string _OPT_COST;

        private const int _ACC_INDEX = 133;

        /// <summary>
        /// Initializes a new instance of the Vehicle class.
        /// This constructor accepts an XmlNode representing a Vehicle object to initialize.
        /// </summary>
        /// <param name="node">An XmlNode representing a Vehicle object to initialize.</param>
        /// <param name="maxIndex"></param>
        internal Vehicle(XmlNode node, int maxIndex)
        {
            _TRANSM_TYPE = DataHelper.GetData(node, 0, maxIndex);
            _VIN = DataHelper.GetData(node, 1, maxIndex);
            _MAKE = DataHelper.GetData(node, 2, maxIndex);
            _MAKE_DESC = DataHelper.GetData(node, 3, maxIndex);
            _ACCTG_MAKE = DataHelper.GetData(node, 4, maxIndex);
            _VEH_YR = DataHelper.GetData(node, 5, maxIndex);
            _MDL_NO = DataHelper.GetData(node, 6, maxIndex);
            _MDL_DESC = DataHelper.GetData(node, 7, maxIndex);
            _CARLINE = DataHelper.GetData(node, 8, maxIndex);
            _BDYSZE = DataHelper.GetData(node, 9, maxIndex);
            _EXT_CLR_CODE = DataHelper.GetData(node, 10, maxIndex);
            _EXT_CLR_DESC = DataHelper.GetData(node, 11, maxIndex);
            _INT_CLR_CODE = DataHelper.GetData(node, 12, maxIndex);
            _INT_CLR_DESC = DataHelper.GetData(node, 13, maxIndex);
            _ACC_CLR = DataHelper.GetData(node, 14, maxIndex);
            _ROOF_CLR = DataHelper.GetData(node, 15, maxIndex);
            _AIR_COND = DataHelper.GetData(node, 16, maxIndex);
            _NO_AXLES = DataHelper.GetData(node, 17, maxIndex);
            _CHASSIS_NO = DataHelper.GetData(node, 18, maxIndex);
            _ENG_NO = DataHelper.GetData(node, 19, maxIndex);
            _CLASS = DataHelper.GetData(node, 20, maxIndex);
            _FUEL_CODE = DataHelper.GetData(node, 21, maxIndex);
            _MPG = DataHelper.GetData(node, 22, maxIndex);
            _INTR_START_DATE = DataHelper.GetData(node, 23, maxIndex);
            _KEY1_NO = DataHelper.GetData(node, 24, maxIndex);
            _KEY2_NO = DataHelper.GetData(node, 25, maxIndex);
            _VEND_NO = DataHelper.GetData(node, 26, maxIndex);
            _FUEL_ECON = DataHelper.GetData(node, 27, maxIndex);
            _CYL_CNT = DataHelper.GetData(node, 28, maxIndex);
            _MILEAGE = DataHelper.GetData(node, 29, maxIndex);
            _POWER_STEER = DataHelper.GetData(node, 30, maxIndex);
            _TRANS_NO = DataHelper.GetData(node, 31, maxIndex);
            _TRIM = DataHelper.GetData(node, 32, maxIndex);
            _TURBO = DataHelper.GetData(node, 33, maxIndex);
            _WEIGHT = DataHelper.GetData(node, 34, maxIndex);
            _AXLE = DataHelper.GetData(node, 35, maxIndex);
            _WHEEL_BASE = DataHelper.GetData(node, 36, maxIndex);
            _DRIVE_TYPE = DataHelper.GetData(node, 37, maxIndex);
            _NUO = DataHelper.GetData(node, 38, maxIndex);
            _UNITS_MEASURE = DataHelper.GetData(node, 39, maxIndex);
            _STOCK_NUMBER = DataHelper.GetData(node, 40, maxIndex);
            _VEH_LOC = DataHelper.GetData(node, 41, maxIndex);
            _RCPT_DATE = DataHelper.GetData(node, 42, maxIndex);
            _VEH_STAT_CODE = DataHelper.GetData(node, 43, maxIndex);
            _VEH_STAT = DataHelper.GetData(node, 44, maxIndex);
            _SLM_NO = DataHelper.GetData(node, 45, maxIndex);
            _TI_PAYOFF_AMT = DataHelper.GetData(node, 46, maxIndex);
            _TI_STK_NO = DataHelper.GetData(node, 47, maxIndex);
            _MEMO1 = DataHelper.GetData(node, 48, maxIndex);
            _MEMO2 = DataHelper.GetData(node, 49, maxIndex);
            _PURCH_DATE = DataHelper.GetData(node, 50, maxIndex);
            _FP_VEND = DataHelper.GetData(node, 51, maxIndex);
            _FP_NAME = DataHelper.GetData(node, 52, maxIndex);
            _FP_AMT = DataHelper.GetData(node, 53, maxIndex);
            _FP_RATE = DataHelper.GetData(node, 54, maxIndex);
            _FP_INTR_START_DATE = DataHelper.GetData(node, 55, maxIndex);
            _FP_PAYOFF_DATE = DataHelper.GetData(node, 56, maxIndex);
            _ASSEMBLY_PLANT = DataHelper.GetData(node, 57, maxIndex);
            _EST_ARRIVAL_DATE = DataHelper.GetData(node, 58, maxIndex);
            _FACT_ORD_NO = DataHelper.GetData(node, 59, maxIndex);
            _MANUF_SCH_DATE = DataHelper.GetData(node, 60, maxIndex);
            _MANUF_STAT = DataHelper.GetData(node, 61, maxIndex);
            _MANUF_STAT_DATE = DataHelper.GetData(node, 62, maxIndex);
            _OMAL = DataHelper.GetData(node, 63, maxIndex);
            _ORD_DATE = DataHelper.GetData(node, 64, maxIndex);
            _ORD_TYPE = DataHelper.GetData(node, 65, maxIndex);
            _ORD_PROCESS_DATE = DataHelper.GetData(node, 66, maxIndex);
            _ORD_PRIORITY = DataHelper.GetData(node, 67, maxIndex);
            _PRICE_LVL = DataHelper.GetData(node, 68, maxIndex);
            _SPECIAL_ORD_NO = DataHelper.GetData(node, 69, maxIndex);
            _BEST_PRICE = DataHelper.GetData(node, 70, maxIndex);
            _CODED_COST = DataHelper.GetData(node, 71, maxIndex);
            _HOLDBACK = DataHelper.GetData(node, 72, maxIndex);
            _INVOICE_AMT = DataHelper.GetData(node, 73, maxIndex);
            _INV_AMT = DataHelper.GetData(node, 74, maxIndex);
            _INV_AMT2 = DataHelper.GetData(node, 75, maxIndex);
            _INV_AMT3 = DataHelper.GetData(node, 76, maxIndex);
            _INV_GL_AMT = DataHelper.GetData(node, 77, maxIndex);
            _LIC_FEE = DataHelper.GetData(node, 78, maxIndex);
            _LIST_PRICE = DataHelper.GetData(node, 79, maxIndex);
            _PREDEL_INSP_AMT = DataHelper.GetData(node, 80, maxIndex);
            _SLS_PRICE = DataHelper.GetData(node, 81, maxIndex);
            _SLS_TYPE = DataHelper.GetData(node, 82, maxIndex);
            _SLS_COST = DataHelper.GetData(node, 83, maxIndex);
            _SLS_CODE = DataHelper.GetData(node, 84, maxIndex);
            _OPEN_RO_PO = DataHelper.GetData(node, 85, maxIndex);
            _RO_PO_TYPE = DataHelper.GetData(node, 86, maxIndex);
            _DOOR_CNT = DataHelper.GetData(node, 87, maxIndex);
            _INT_TYPE = DataHelper.GetData(node, 88, maxIndex);
            _RECON_AMT = DataHelper.GetData(node, 89, maxIndex);
            _PACK_AMT = DataHelper.GetData(node, 90, maxIndex);
            _DEL_DATE = DataHelper.GetData(node, 91, maxIndex);
            _DAYS_IN_STOCK = DataHelper.GetData(node, 92, maxIndex);
            _CERTIFIED = DataHelper.GetData(node, 93, maxIndex);
            _STICKER_PRICE = DataHelper.GetData(node, 94, maxIndex);
            _INV_PRICE1 = DataHelper.GetData(node, 95, maxIndex);
            _INV_PRICE2 = DataHelper.GetData(node, 96, maxIndex);
            _SLS_PRICE1 = DataHelper.GetData(node, 97, maxIndex);
            _SLS_PRICE2 = DataHelper.GetData(node, 98, maxIndex);
            _BASE_MSRP = DataHelper.GetData(node, 99, maxIndex);
            _BASE_SLS_PRICE = DataHelper.GetData(node, 100, maxIndex);
            _BASE_SLS_COST = DataHelper.GetData(node, 101, maxIndex);
            _COMM_PRICE = DataHelper.GetData(node, 102, maxIndex);
            _DRAFT_AMOUNT = DataHelper.GetData(node, 103, maxIndex);
            _DLR_RCPT_DATE = DataHelper.GetData(node, 104, maxIndex);
            _INV_SEG_CODE = DataHelper.GetData(node, 105, maxIndex);
            _VEH_SRC = DataHelper.GetData(node, 106, maxIndex);
            _VEH_SRC_NAME = DataHelper.GetData(node, 107, maxIndex);
            _INV_ACCT = DataHelper.GetData(node, 108, maxIndex);
            _I_COMPANY = DataHelper.GetData(node, 109, maxIndex);
            _SOLD_DATE = DataHelper.GetData(node, 110, maxIndex);
            _PREV_OWNER = DataHelper.GetData(node, 111, maxIndex);
            _ACTVY_DATE = DataHelper.GetData(node, 112, maxIndex);
            _CUST_CREATE_DATE = DataHelper.GetData(node, 113, maxIndex);
            _CUST_CATEG = DataHelper.GetData(node, 114, maxIndex);
            _MDL_TYPE = DataHelper.GetData(node, 115, maxIndex);
            _VEH_TYPE = DataHelper.GetData(node, 116, maxIndex);
            _POLL_DATE = DataHelper.GetData(node, 117, maxIndex);
            _POLL_TIME = DataHelper.GetData(node, 118, maxIndex);
            _CATEG_CODE = DataHelper.GetData(node, 119, maxIndex);
            _DLR_ACC_COST = DataHelper.GetData(node, 120, maxIndex);
            _ACV = DataHelper.GetData(node, 121, maxIndex);
            _WHSL_FLAG = DataHelper.GetData(node, 122, maxIndex);


            _VEH_UDF1 = DataHelper.GetData(node, 123, maxIndex);
            _VEH_UDF2 = DataHelper.GetData(node, 124, maxIndex);
            _VEH_UDF3 = DataHelper.GetData(node, 125, maxIndex);
            _VEH_UDF4 = DataHelper.GetData(node, 126, maxIndex);
            _VEH_UDF5 = DataHelper.GetData(node, 127, maxIndex);
            _VEH_UDF6 = DataHelper.GetData(node, 128, maxIndex);
            _VEH_UDF7 = DataHelper.GetData(node, 129, maxIndex);
            _VEH_UDF8 = DataHelper.GetData(node, 130, maxIndex);
            _INVEN_SRC = DataHelper.GetData(node, 131, maxIndex);
            _INVEN_SRC_NAME = DataHelper.GetData(node, 132, maxIndex);

            // not worrying about the options for XML
        }

        /// <summary>
        /// Initializes a new instance of the Vehicle class.
        /// This constructor accepts an IDataRecord representing a line item of a csv reader used to populate the object
        /// </summary>
        /// <param name="record">An IDataRecord representing a line item of a csv reader used to populate the object.</param>
        internal Vehicle(IDataRecord record, int maxIndex)
        {
            _TRANSM_TYPE = DataHelper.GetData(record, 0, maxIndex);
            _VIN = DataHelper.GetData(record, 1, maxIndex);
            _MAKE = DataHelper.GetData(record, 2, maxIndex);
            _MAKE_DESC = DataHelper.GetData(record, 3, maxIndex);
            _ACCTG_MAKE = DataHelper.GetData(record, 4, maxIndex);
            _VEH_YR = DataHelper.GetData(record, 5, maxIndex);
            _MDL_NO = DataHelper.GetData(record, 6, maxIndex);
            _MDL_DESC = DataHelper.GetData(record, 7, maxIndex);
            _CARLINE = DataHelper.GetData(record, 8, maxIndex);
            _BDYSZE = DataHelper.GetData(record, 9, maxIndex);
            _EXT_CLR_CODE = DataHelper.GetData(record, 10, maxIndex);
            _EXT_CLR_DESC = DataHelper.GetData(record, 11, maxIndex);
            _INT_CLR_CODE = DataHelper.GetData(record, 12, maxIndex);
            _INT_CLR_DESC = DataHelper.GetData(record, 13, maxIndex);
            _ACC_CLR = DataHelper.GetData(record, 14, maxIndex);
            _ROOF_CLR = DataHelper.GetData(record, 15, maxIndex);
            _AIR_COND = DataHelper.GetData(record, 16, maxIndex);
            _NO_AXLES = DataHelper.GetData(record, 17, maxIndex);
            _CHASSIS_NO = DataHelper.GetData(record, 18, maxIndex);
            _ENG_NO = DataHelper.GetData(record, 19, maxIndex);
            _CLASS = DataHelper.GetData(record, 20, maxIndex);
            _FUEL_CODE = DataHelper.GetData(record, 21, maxIndex);
            _MPG = DataHelper.GetData(record, 22, maxIndex);
            _INTR_START_DATE = DataHelper.GetData(record, 23, maxIndex);
            _KEY1_NO = DataHelper.GetData(record, 24, maxIndex);
            _KEY2_NO = DataHelper.GetData(record, 25, maxIndex);
            _VEND_NO = DataHelper.GetData(record, 26, maxIndex);
            _FUEL_ECON = DataHelper.GetData(record, 27, maxIndex);
            _CYL_CNT = DataHelper.GetData(record, 28, maxIndex);
            _MILEAGE = DataHelper.GetData(record, 29, maxIndex);
            _POWER_STEER = DataHelper.GetData(record, 30, maxIndex);
            _TRANS_NO = DataHelper.GetData(record, 31, maxIndex);
            _TRIM = DataHelper.GetData(record, 32, maxIndex);
            _TURBO = DataHelper.GetData(record, 33, maxIndex);
            _WEIGHT = DataHelper.GetData(record, 34, maxIndex);
            _AXLE = DataHelper.GetData(record, 35, maxIndex);
            _WHEEL_BASE = DataHelper.GetData(record, 36, maxIndex);
            _DRIVE_TYPE = DataHelper.GetData(record, 37, maxIndex);
            _NUO = DataHelper.GetData(record, 38, maxIndex);
            _UNITS_MEASURE = DataHelper.GetData(record, 39, maxIndex);
            _STOCK_NUMBER = DataHelper.GetData(record, 40, maxIndex);
            _VEH_LOC = DataHelper.GetData(record, 41, maxIndex);
            _RCPT_DATE = DataHelper.GetData(record, 42, maxIndex);
            _VEH_STAT_CODE = DataHelper.GetData(record, 43, maxIndex);
            _VEH_STAT = DataHelper.GetData(record, 44, maxIndex);
            _SLM_NO = DataHelper.GetData(record, 45, maxIndex);
            _TI_PAYOFF_AMT = DataHelper.GetData(record, 46, maxIndex);
            _TI_STK_NO = DataHelper.GetData(record, 47, maxIndex);
            _MEMO1 = DataHelper.GetData(record, 48, maxIndex);
            _MEMO2 = DataHelper.GetData(record, 49, maxIndex);
            _PURCH_DATE = DataHelper.GetData(record, 50, maxIndex);
            _FP_VEND = DataHelper.GetData(record, 51, maxIndex);
            _FP_NAME = DataHelper.GetData(record, 52, maxIndex);
            _FP_AMT = DataHelper.GetData(record, 53, maxIndex);
            _FP_RATE = DataHelper.GetData(record, 54, maxIndex);
            _FP_INTR_START_DATE = DataHelper.GetData(record, 55, maxIndex);
            _FP_PAYOFF_DATE = DataHelper.GetData(record, 56, maxIndex);
            _ASSEMBLY_PLANT = DataHelper.GetData(record, 57, maxIndex);
            _EST_ARRIVAL_DATE = DataHelper.GetData(record, 58, maxIndex);
            _FACT_ORD_NO = DataHelper.GetData(record, 59, maxIndex);
            _MANUF_SCH_DATE = DataHelper.GetData(record, 60, maxIndex);
            _MANUF_STAT = DataHelper.GetData(record, 61, maxIndex);
            _MANUF_STAT_DATE = DataHelper.GetData(record, 62, maxIndex);
            _OMAL = DataHelper.GetData(record, 63, maxIndex);
            _ORD_DATE = DataHelper.GetData(record, 64, maxIndex);
            _ORD_TYPE = DataHelper.GetData(record, 65, maxIndex);
            _ORD_PROCESS_DATE = DataHelper.GetData(record, 66, maxIndex);
            _ORD_PRIORITY = DataHelper.GetData(record, 67, maxIndex);
            _PRICE_LVL = DataHelper.GetData(record, 68, maxIndex);
            _SPECIAL_ORD_NO = DataHelper.GetData(record, 69, maxIndex);
            _BEST_PRICE = DataHelper.GetData(record, 70, maxIndex);
            _CODED_COST = DataHelper.GetData(record, 71, maxIndex);
            _HOLDBACK = DataHelper.GetData(record, 72, maxIndex);
            _INVOICE_AMT = DataHelper.GetData(record, 73, maxIndex);
            _INV_AMT = DataHelper.GetData(record, 74, maxIndex);
            _INV_AMT2 = DataHelper.GetData(record, 75, maxIndex);
            _INV_AMT3 = DataHelper.GetData(record, 76, maxIndex);
            _INV_GL_AMT = DataHelper.GetData(record, 77, maxIndex);
            _LIC_FEE = DataHelper.GetData(record, 78, maxIndex);
            _LIST_PRICE = DataHelper.GetData(record, 79, maxIndex);
            _PREDEL_INSP_AMT = DataHelper.GetData(record, 80, maxIndex);
            _SLS_PRICE = DataHelper.GetData(record, 81, maxIndex);
            _SLS_TYPE = DataHelper.GetData(record, 82, maxIndex);
            _SLS_COST = DataHelper.GetData(record, 83, maxIndex);
            _SLS_CODE = DataHelper.GetData(record, 84, maxIndex);
            _OPEN_RO_PO = DataHelper.GetData(record, 85, maxIndex);
            _RO_PO_TYPE = DataHelper.GetData(record, 86, maxIndex);
            _DOOR_CNT = DataHelper.GetData(record, 87, maxIndex);
            _INT_TYPE = DataHelper.GetData(record, 88, maxIndex);
            _RECON_AMT = DataHelper.GetData(record, 89, maxIndex);
            _PACK_AMT = DataHelper.GetData(record, 90, maxIndex);
            _DEL_DATE = DataHelper.GetData(record, 91, maxIndex);
            _DAYS_IN_STOCK = DataHelper.GetData(record, 92, maxIndex);
            _CERTIFIED = DataHelper.GetData(record, 93, maxIndex);
            _STICKER_PRICE = DataHelper.GetData(record, 94, maxIndex);
            _INV_PRICE1 = DataHelper.GetData(record, 95, maxIndex);
            _INV_PRICE2 = DataHelper.GetData(record, 96, maxIndex);
            _SLS_PRICE1 = DataHelper.GetData(record, 97, maxIndex);
            _SLS_PRICE2 = DataHelper.GetData(record, 98, maxIndex);
            _BASE_MSRP = DataHelper.GetData(record, 99, maxIndex);
            _BASE_SLS_PRICE = DataHelper.GetData(record, 100, maxIndex);
            _BASE_SLS_COST = DataHelper.GetData(record, 101, maxIndex);
            _COMM_PRICE = DataHelper.GetData(record, 102, maxIndex);
            _DRAFT_AMOUNT = DataHelper.GetData(record, 103, maxIndex);
            _DLR_RCPT_DATE = DataHelper.GetData(record, 104, maxIndex);
            _INV_SEG_CODE = DataHelper.GetData(record, 105, maxIndex);
            _VEH_SRC = DataHelper.GetData(record, 106, maxIndex);
            _VEH_SRC_NAME = DataHelper.GetData(record, 107, maxIndex);
            _INV_ACCT = DataHelper.GetData(record, 108, maxIndex);
            _I_COMPANY = DataHelper.GetData(record, 109, maxIndex);
            _SOLD_DATE = DataHelper.GetData(record, 110, maxIndex);
            _PREV_OWNER = DataHelper.GetData(record, 111, maxIndex);
            _ACTVY_DATE = DataHelper.GetData(record, 112, maxIndex);
            _CUST_CREATE_DATE = DataHelper.GetData(record, 113, maxIndex);
            _CUST_CATEG = DataHelper.GetData(record, 114, maxIndex);
            _MDL_TYPE = DataHelper.GetData(record, 115, maxIndex);
            _VEH_TYPE = DataHelper.GetData(record, 116, maxIndex);
            _POLL_DATE = DataHelper.GetData(record, 117, maxIndex);
            _POLL_TIME = DataHelper.GetData(record, 118, maxIndex);
            _CATEG_CODE = DataHelper.GetData(record, 119, maxIndex);
            _DLR_ACC_COST = DataHelper.GetData(record, 120, maxIndex);
            _ACV = DataHelper.GetData(record, 121, maxIndex);
            _WHSL_FLAG = DataHelper.GetData(record, 122, maxIndex);

            _VEH_UDF1 = DataHelper.GetData(record, 123, maxIndex);
            _VEH_UDF2 = DataHelper.GetData(record, 124, maxIndex);
            _VEH_UDF3 = DataHelper.GetData(record, 125, maxIndex);
            _VEH_UDF4 = DataHelper.GetData(record, 126, maxIndex);
            _VEH_UDF5 = DataHelper.GetData(record, 127, maxIndex);
            _VEH_UDF6 = DataHelper.GetData(record, 128, maxIndex);
            _VEH_UDF7 = DataHelper.GetData(record, 129, maxIndex);
            _VEH_UDF8 = DataHelper.GetData(record, 130, maxIndex);
            _INVEN_SRC = DataHelper.GetData(record, 131, maxIndex);
            _INVEN_SRC_NAME = DataHelper.GetData(record, 132, maxIndex);

            // CHECK FOR THE "ACC" VALUE IN 133, SIGNIFYING OPTIONS. IF WE HAVE MORE THAN _ACC_INDEX+1 FIELDS, PROCESS THE OPTIONS
            
            _OPT_CODE = "";
            _OPT_DESC = "";
            _OPT_PRICE = "";
            _OPT_COST = "";

            if (record.FieldCount > _ACC_INDEX+1 && record.GetString(_ACC_INDEX) == "ACC")
            {
                for(int i = _ACC_INDEX+1; i < record.FieldCount; i++)
                    switch ((i - (_ACC_INDEX + 1)) % 4)
                    {
                        case 0:
                            _OPT_CODE += ((_OPT_CODE.Length == 0) ? "" : "~") + record.GetString(i);
                            break;
                        case 1:
                            _OPT_DESC += ((_OPT_DESC.Length == 0) ? "" : "~") + record.GetString(i);
                            break;
                        case 2:
                            _OPT_PRICE += ((_OPT_PRICE.Length == 0) ? "" : "~") + record.GetString(i);
                            break;
                        case 3:
                            _OPT_COST += ((_OPT_COST.Length == 0) ? "" : "~") + record.GetString(i);
                            break;
                    }
                }



        }


        /// <summary>
        /// Writes the Vehicle in a Pipe "|" separated value format, following the IPsvSerializable interface.
        /// Writes all values in alphabetical order by property name.
        /// </summary>
        /// <param name="outputStream">The output stream to which to write.</param>
        public void WritePsv(StreamWriter outputStream)
        {
            StringBuilder b = new StringBuilder();

            b.Append(_DEL_DATE);
            b.Append("|");

            b.Append(_BASE_SLS_COST);
            b.Append("|");

            b.Append(_BASE_SLS_PRICE);
            b.Append("|");

            b.Append(_BDYSZE);
            b.Append("|");

            b.Append(_CERTIFIED);
            b.Append("|");

            b.Append(_CODED_COST);
            b.Append("|");

            b.Append(_COMM_PRICE);
            b.Append("|");

            b.Append(_I_COMPANY);
            b.Append("|");

            b.Append(_CYL_CNT);
            b.Append("|");

            b.Append(_SOLD_DATE);
            b.Append("|");

            b.Append(_DLR_RCPT_DATE);
            b.Append("|");

            b.Append(_DOOR_CNT);
            b.Append("|");

            b.Append(_DRAFT_AMOUNT);
            b.Append("|");

            b.Append(_CHASSIS_NO);
            b.Append("|");

            b.Append(_ENG_NO);
            b.Append("|");

            b.Append(_EXT_CLR_DESC);
            b.Append("|");

            b.Append(_NUO);
            b.Append("|");

            b.Append(_FUEL_CODE);
            b.Append("|");

            b.Append(_INT_CLR_DESC);
            b.Append("|");

            b.Append(_INV_ACCT);
            b.Append("|");

            b.Append(_MAKE_DESC);
            b.Append("|");

            b.Append(_MEMO1);
            b.Append("|");

            b.Append(_MEMO2);
            b.Append("|");

            b.Append(_MILEAGE);
            b.Append("|");

            b.Append(_MDL_DESC);
            b.Append("|");

            b.Append(_TRIM);
            b.Append("|");

            b.Append(_PACK_AMT);
            b.Append("|");

            b.Append(_PURCH_DATE);
            b.Append("|");

            b.Append(_RCPT_DATE);
            b.Append("|");

            b.Append(_RECON_AMT);
            b.Append("|");

            b.Append(_POLL_DATE);
            b.Append("|");

            b.Append(_SLS_COST);
            b.Append("|");

            b.Append(_SLS_TYPE);
            b.Append("|");

            b.Append(_SLS_PRICE);
            b.Append("|");

            b.Append(_SLS_PRICE1);
            b.Append("|");

            b.Append(_SLS_PRICE2);
            b.Append("|");

            b.Append(_STICKER_PRICE);
            b.Append("|");

            b.Append(_STOCK_NUMBER);
            b.Append("|");

            b.Append(_TRANS_NO);
            b.Append("|");

            b.Append(_INV_GL_AMT);
            b.Append("|");

            b.Append(_LIST_PRICE);
            b.Append("|");

            b.Append(_VEH_LOC);
            b.Append("|");

            b.Append(_VEH_SRC);
            b.Append("|");

            b.Append(_VEH_SRC_NAME);
            b.Append("|");

            b.Append(_VEH_STAT);
            b.Append("|");

            b.Append(_VEH_STAT_CODE);
            b.Append("|");

            b.Append(_VEH_TYPE);
            b.Append("|");

            b.Append(_VEH_YR);
            b.Append("|");

            b.Append(_VIN);
            b.Append("|");

            b.Append(_TRANSM_TYPE);
            b.Append("|");
            
            b.Append(_MAKE);
            b.Append("|");

            b.Append(_ACCTG_MAKE);
            b.Append("|");
            
            b.Append(_MDL_NO);
            b.Append("|");

            b.Append(_CARLINE);
            b.Append("|");

            b.Append(_EXT_CLR_CODE);
            b.Append("|");

            b.Append(_INT_CLR_CODE);
            b.Append("|");

            b.Append(_ACC_CLR);
            b.Append("|");

            b.Append(_ROOF_CLR);
            b.Append("|");

            b.Append(_AIR_COND);
            b.Append("|");

            b.Append(_NO_AXLES);
            b.Append("|");

            b.Append(_CLASS);
            b.Append("|");

            b.Append(_MPG);
            b.Append("|");

            b.Append(_INTR_START_DATE);
            b.Append("|");

            b.Append(_KEY1_NO);
            b.Append("|");

            b.Append(_KEY2_NO);
            b.Append("|");

            b.Append(_VEND_NO);
            b.Append("|");

            b.Append(_FUEL_ECON);
            b.Append("|");

            b.Append(_POWER_STEER);
            b.Append("|");

            b.Append(_TURBO);
            b.Append("|");

            b.Append(_WEIGHT);
            b.Append("|");

            b.Append(_AXLE);
            b.Append("|");

            b.Append(_WHEEL_BASE);
            b.Append("|");

            b.Append(_DRIVE_TYPE);
            b.Append("|");

            b.Append(_UNITS_MEASURE);
            b.Append("|");

            b.Append(_SLM_NO);
            b.Append("|");

            b.Append(_TI_PAYOFF_AMT);
            b.Append("|");

            b.Append(_TI_STK_NO);
            b.Append("|");

            b.Append(_FP_VEND);
            b.Append("|");

            b.Append(_FP_NAME);
            b.Append("|");

            b.Append(_FP_AMT);
            b.Append("|");

            b.Append(_FP_RATE);
            b.Append("|");

            b.Append(_FP_INTR_START_DATE);
            b.Append("|");

            b.Append(_FP_PAYOFF_DATE);
            b.Append("|");

            b.Append(_ASSEMBLY_PLANT);
            b.Append("|");

            b.Append(_EST_ARRIVAL_DATE);
            b.Append("|");

            b.Append(_FACT_ORD_NO);
            b.Append("|");

            b.Append(_MANUF_SCH_DATE);
            b.Append("|");

            b.Append(_MANUF_STAT);
            b.Append("|");

            b.Append(_MANUF_STAT_DATE);
            b.Append("|");

            b.Append(_OMAL);
            b.Append("|");

            b.Append(_ORD_DATE);
            b.Append("|");

            b.Append(_ORD_TYPE);
            b.Append("|");

            b.Append(_ORD_PROCESS_DATE);
            b.Append("|");

            b.Append(_ORD_PRIORITY);
            b.Append("|");

            b.Append(_PRICE_LVL);
            b.Append("|");

            b.Append(_SPECIAL_ORD_NO);
            b.Append("|");

            b.Append(_BEST_PRICE);
            b.Append("|");

            b.Append(_HOLDBACK);
            b.Append("|");

            b.Append(_INVOICE_AMT);
            b.Append("|");

            b.Append(_INV_AMT);
            b.Append("|");

            b.Append(_INV_AMT2);
            b.Append("|");

            b.Append(_INV_AMT3);
            b.Append("|");

            b.Append(_LIC_FEE);
            b.Append("|");

            b.Append(_PREDEL_INSP_AMT);
            b.Append("|");

            b.Append(_SLS_CODE);
            b.Append("|");

            b.Append(_OPEN_RO_PO);
            b.Append("|");

            b.Append(_RO_PO_TYPE);
            b.Append("|");

            b.Append(_INT_TYPE);
            b.Append("|");

            b.Append(_DAYS_IN_STOCK);
            b.Append("|");

            b.Append(_INV_PRICE1);
            b.Append("|");

            b.Append(_INV_PRICE2);
            b.Append("|");

            b.Append(_BASE_MSRP);
            b.Append("|");

            b.Append(_INV_SEG_CODE);
            b.Append("|");

            b.Append(_PREV_OWNER);
            b.Append("|");

            b.Append(_ACTVY_DATE);
            b.Append("|");

            b.Append(_CUST_CREATE_DATE);
            b.Append("|");

            b.Append(_CUST_CATEG);
            b.Append("|");

            b.Append(_MDL_TYPE);
            b.Append("|");

            b.Append(_POLL_TIME);
            b.Append("|");

            b.Append(_CATEG_CODE);
            b.Append("|");

            b.Append(_DLR_ACC_COST);
            b.Append("|");

            b.Append(_ACV);
            b.Append("|");

            b.Append(_WHSL_FLAG);
            b.Append("|");

            b.Append(_VEH_UDF1);
            b.Append("|");

            b.Append(_VEH_UDF2);
            b.Append("|");

            b.Append(_VEH_UDF3);
            b.Append("|");

            b.Append(_VEH_UDF4);
            b.Append("|");

            b.Append(_VEH_UDF5);
            b.Append("|");

            b.Append(_VEH_UDF6);
            b.Append("|");

            b.Append(_VEH_UDF7);
            b.Append("|");

            b.Append(_VEH_UDF8);
            b.Append("|");

            b.Append(_INVEN_SRC);
            b.Append("|");

            b.Append(_INVEN_SRC_NAME);
            b.Append("|");

            b.Append(_OPT_CODE);
            b.Append("|");

            b.Append(_OPT_DESC);
            b.Append("|");

            b.Append(_OPT_PRICE);
            b.Append("|");

            b.Append(_OPT_COST);

            outputStream.WriteLine(b.ToString());
        }
    }
}