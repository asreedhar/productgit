using System.Data;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using Firstlook.Sis.DomainModel.Serialization;

//Used to allow this class to be used in the Sis.DomainModel_Test project
[assembly: InternalsVisibleTo("Firstlook.Sis.DomainModelTest")]
namespace Firstlook.Sis.DomainModel.Facade
{
    /// <summary>
    /// An accounting schedule as returned from Sis, contained in the dealer's DMS.
    /// XmlDeserialization is used to contruct a Schedule from an XmlDocument.
    /// </summary>
    public sealed class Schedule : IPsvSerializable
    {
        /// <summary>
        /// The Company ID.
        /// </summary>
        private string companyId;

        /// <summary>
        /// The Schedule's control type.
        /// </summary>
        private string controlType;

        /// <summary>
        /// The Schedule's second control type.
        /// </summary>
        private string controlType2;

        /// <summary>
        /// The Schedule's description.
        /// </summary>
        private string description;

        /// <summary>
        /// The Schedule's number.
        /// </summary>
        private string scheduleNumber;

        /// <summary>
        /// The Schedule's type.
        /// </summary>
        private string scheduleType;




        /// <summary>
        /// Gets or sets the Company ID.
        /// </summary>
        public string CompanyId
        {
            get { return companyId; }
            set { companyId = value; }
        }

        /// <summary>
        /// Gets or sets the Schedule's control type.
        /// </summary>
        public string ControlType
        {
            get { return controlType; }
            set { controlType = value; }
        }

        /// <summary>
        /// Gets or sets the Schedule's second control type.
        /// </summary>
        public string ControlType2
        {
            get { return controlType2; }
            set { controlType2 = value; }
        }

        /// <summary>
        /// Gets or sets the Schedule's description.
        /// </summary>
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        /// <summary>
        /// Gets or sets the Schedule's number.
        /// </summary>
        public string ScheduleNumber
        {
            get { return scheduleNumber; }
            set { scheduleNumber = value; }
        }

        /// <summary>
        /// Gets or sets the Schedule's type.
        /// </summary>
        public string ScheduleType
        {
            get { return scheduleType; }
            set { scheduleType = value; }
        }




        /// <summary>
        /// Initializes a new instance of the Schedule class.
        /// This constructor accepts an IDataRecord representing a line item of a csv reader used to populate the object
        /// </summary>
        /// <param name="record">An IDataRecord representing a line item of a csv reader used to populate the object.</param>
        /// <param name="maxIndex"></param>
        internal Schedule(IDataRecord record, int maxIndex)
        {
            companyId = DataHelper.GetData(record, 5, maxIndex);
            controlType = DataHelper.GetData(record, 3, maxIndex);
            controlType2 = DataHelper.GetData(record, 4, maxIndex);
            description = DataHelper.GetData(record, 1, maxIndex);
            scheduleNumber = DataHelper.GetData(record, 0, maxIndex);
            scheduleType = DataHelper.GetData(record, 2, maxIndex);
        }




        /// <summary>
        /// Writes the Schedule in a Pipe "|" separated value format, following the IPsvSerializable interface.
        /// Writes all values in alphabetical order by property name.
        /// </summary>
        /// <param name="outputStream">The output stream to which to write.</param>
        public void WritePsv(StreamWriter outputStream)
        {
            StringBuilder b = new StringBuilder();

            b.Append(companyId);
            b.Append("|");

            b.Append(controlType);
            b.Append("|");

            b.Append(controlType2);
            b.Append("|");

            b.Append(description);
            b.Append("|");

            b.Append(scheduleNumber);
            b.Append("|");

            b.Append(scheduleType);

            outputStream.WriteLine(b.ToString());
        }
    }
}