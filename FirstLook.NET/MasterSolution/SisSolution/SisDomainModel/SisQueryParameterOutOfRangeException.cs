using System;

namespace Firstlook.Sis.DomainModel
{
    /// <summary>
    /// An exception related to passing the Sis service a parameter out of the allowable or expected range.
    /// </summary>
    public class SisQueryParameterOutOfRangeException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the SisQueryParameterOutOfRangeException class with a message.
        /// </summary>
        /// <param name="message">The error data related to the exception.</param>
        public SisQueryParameterOutOfRangeException(string message) : base(message)
        { }
    }
}