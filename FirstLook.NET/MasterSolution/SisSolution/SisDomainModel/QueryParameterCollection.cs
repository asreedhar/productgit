using System;
using System.Collections.ObjectModel;

namespace Firstlook.Sis.DomainModel
{
    /// <summary>
    /// Represents a collection of QueryParameters to use in conjunction with a query.
    /// </summary>
    public class QueryParameterCollection : Collection<QueryParameter>
    {
        /// <summary>
        /// Returns a single QueryParameter by parameter name.
        /// </summary>
        public QueryParameter this[string parameterName]
        {
            get
            {
                foreach (QueryParameter p in Items)
                {
                    if (p.Name.Equals(parameterName, StringComparison.InvariantCultureIgnoreCase))
                    {
                        return p;
                    }
                }
                return null;
            }
        }
    }
}