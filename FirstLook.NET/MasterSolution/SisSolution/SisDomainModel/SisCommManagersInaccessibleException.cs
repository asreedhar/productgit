using System;

namespace Firstlook.Sis.DomainModel
{
    /// <summary>
    /// An exception related to the comm manager(s) being down or otherwise inaccessible.
    /// </summary>
    public class SisCommManagersInaccessibleException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the SisCommManagersInaccessibleException class with a message.
        /// </summary>
        /// <param name="message">The error data related to the exception.</param>
        public SisCommManagersInaccessibleException(string message) : base(message)
        { }
    }
}