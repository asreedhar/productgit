//using System.IO;
//using Firstlook.Sis.DomainModel.Serialization;
//using NUnit.Framework;

//namespace Firstlook.Sis.DomainModel.Facade
//{
//    [TestFixture]
//    public class DealCollectionTest : BaseCollectionTest
//    {
//        [Test]
//        public void MakeDealCollectionFromCData()
//        {
//            DealCollection dc = new DealCollection(new CsvReader(GetCDataReader(new StreamReader("../../TestData/DealCsv.xml").ReadToEnd())));

//            Assert.IsNotNull(dc);
//            Assert.AreEqual(14, dc.Count);



//            Assert.AreEqual("81 RT 46 W", dc[0].CustomerAddress1);
//            Assert.AreEqual("", dc[0].CustomerAddress2);
//            Assert.AreEqual("LODI", dc[0].CustomerCity);
//            Assert.AreEqual("NJ", dc[0].CustomerState);
//            Assert.AreEqual("07644", dc[0].CustomerZip);
//            Assert.AreEqual("07-31-2007", dc[0].DealDate);
//            Assert.AreEqual("GREEN", dc[0].ExteriorColor);
//            Assert.AreEqual("", dc[0].Filler12);
//            Assert.AreEqual("", dc[0].Filler13);
//            Assert.AreEqual("", dc[0].InteriorColor);
//            Assert.AreEqual("DODGE", dc[0].Make);
//            Assert.AreEqual("181881", dc[0].Mileage);
//            Assert.AreEqual("STRATUS", dc[0].Model);
//            Assert.AreEqual("07-31-2007", dc[0].ModifiedDate);
//            Assert.AreEqual("500.00", dc[0].PackAmount);
//            Assert.AreEqual("0.00", dc[0].ReconditionCost);
//            Assert.AreEqual("WHOLESALE", dc[0].SaleDescription);
//            Assert.AreEqual("333", dc[0].SalespersonId);
//            Assert.AreEqual(" HOUSE", dc[0].SalespersonName);
//            Assert.AreEqual("45976", dc[0].SalesReferenceNumber);
//            Assert.AreEqual("POSTED", dc[0].SaleStatus);
//            Assert.AreEqual("70258A", dc[0].StockNumber);
//            Assert.AreEqual("F", dc[0].VehicleStatusCode);
//            Assert.AreEqual("99", dc[0].VehicleYear);
//            Assert.AreEqual("1B3EJ46X7XN591313", dc[0].Vin);

//            Assert.AreEqual("120 NANCY LANE", dc[1].CustomerAddress1);
//            Assert.AreEqual("", dc[1].CustomerAddress2);
//            Assert.AreEqual("STATEN ISLAND", dc[1].CustomerCity);
//            Assert.AreEqual("NY", dc[1].CustomerState);
//            Assert.AreEqual("10307", dc[1].CustomerZip);
//            Assert.AreEqual("07-31-2007", dc[1].DealDate);
//            Assert.AreEqual("RELFEX_SILVER", dc[1].ExteriorColor);
//            Assert.AreEqual("", dc[1].Filler12);
//            Assert.AreEqual("", dc[1].Filler13);
//            Assert.AreEqual("", dc[1].InteriorColor);
//            Assert.AreEqual("VOLKSWAGEN", dc[1].Make);
//            Assert.AreEqual("16", dc[1].Mileage);
//            Assert.AreEqual("JETTA", dc[1].Model);
//            Assert.AreEqual("07-31-2007", dc[1].ModifiedDate);
//            Assert.AreEqual("500.00", dc[1].PackAmount);
//            Assert.AreEqual("0.00", dc[1].ReconditionCost);
//            Assert.AreEqual("LEASE", dc[1].SaleDescription);
//            Assert.AreEqual("384", dc[1].SalespersonId);
//            Assert.AreEqual("BOB GALE", dc[1].SalespersonName);
//            Assert.AreEqual("45891", dc[1].SalesReferenceNumber);
//            Assert.AreEqual("POSTED", dc[1].SaleStatus);
//            Assert.AreEqual("70511", dc[1].StockNumber);
//            Assert.AreEqual("F", dc[1].VehicleStatusCode);
//            Assert.AreEqual("07", dc[1].VehicleYear);
//            Assert.AreEqual("3VWEF71KX7M151655", dc[1].Vin);

//        }

//        [Test]
//        public void MakeDealCollectionFromXml()
//        {
//            DealCollection dc = new DealCollection(GetXmlResponse("../../TestData/Deal.xml"));

//            Assert.IsNotNull(dc);
//            Assert.AreEqual(1, dc.Count);

//            Assert.AreEqual("81 RT 46 W", dc[0].CustomerAddress1);
//            Assert.AreEqual("", dc[0].CustomerAddress2);
//            Assert.AreEqual("LODI", dc[0].CustomerCity);
//            Assert.AreEqual("NJ", dc[0].CustomerState);
//            Assert.AreEqual("07644", dc[0].CustomerZip);
//            Assert.AreEqual("07-31-2007", dc[0].DealDate);
//            Assert.AreEqual("GREEN", dc[0].ExteriorColor);
//            Assert.AreEqual("", dc[0].Filler12);
//            Assert.AreEqual("", dc[0].Filler13);
//            Assert.AreEqual("", dc[0].InteriorColor);
//            Assert.AreEqual("DODGE", dc[0].Make);
//            Assert.AreEqual("181881", dc[0].Mileage);
//            Assert.AreEqual("STRATUS", dc[0].Model);
//            Assert.AreEqual("07-31-2007", dc[0].ModifiedDate);
//            Assert.AreEqual("500.00", dc[0].PackAmount);
//            Assert.AreEqual("0.00", dc[0].ReconditionCost);
//            Assert.AreEqual("WHOLESALE", dc[0].SaleDescription);
//            Assert.AreEqual("333", dc[0].SalespersonId);
//            Assert.AreEqual(" HOUSE", dc[0].SalespersonName);
//            Assert.AreEqual("45976", dc[0].SalesReferenceNumber);
//            Assert.AreEqual("POSTED", dc[0].SaleStatus);
//            Assert.AreEqual("70258A", dc[0].StockNumber);
//            Assert.AreEqual("F", dc[0].VehicleStatusCode);
//            Assert.AreEqual("99", dc[0].VehicleYear);
//            Assert.AreEqual("1B3EJ46X7XN591313", dc[0].Vin);
//        }

//        [Test]
//        public void WriteDealCollection()
//        {
//            DealCollection dc = new DealCollection(new CsvReader(GetCDataReader(new StreamReader("../../TestData/DealCsv.xml").ReadToEnd())));
//            StreamReader re = WriteAndGetReader("C:\\exampleDealCollection.txt", dc);

//            Assert.AreEqual("|GARY MOTORS INC|1|81 RT 46 W||LODI|NJ|07644|07-31-2007|GREEN|FALSE|||200.00||1420|DODGE|181881|STRATUS|07-31-2007|500.00|200.00|07-30-2007|0.00|WHOLESALE|333| HOUSE|45976|POSTED|70258A|0.00|07-31-2007|0.00|F||99|1B3EJ46X7XN591313", re.ReadLine());
//            Assert.AreEqual("||1|120 NANCY LANE||STATEN ISLAND|NY|10307|07-31-2007|RELFEX_SILVER|TRUE|||990.79||1402|VOLKSWAGEN|16|JETTA|07-31-2007|500.00|21149.79|04-30-2007|0.00|LEASE|384|BOB GALE|45891|POSTED|70511|2197694|07-31-2007|20159.00|F||07|3VWEF71KX7M151655", re.ReadLine());

//            re.Close();
//            File.Delete("C:\\exampleDealCollection.txt");
//        }
//    }
//}