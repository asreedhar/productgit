//using System.IO;
//using Firstlook.Sis.DomainModel.Serialization;
//using NUnit.Framework;

//namespace Firstlook.Sis.DomainModel.Facade
//{
//    [TestFixture]
//    public class VehicleCollectionTest : BaseCollectionTest
//    {
//        [Test]
//        public void MakeVehicleCollectionFromCData()
//        {
//            VehicleCollection vc = new VehicleCollection(new CsvReader(GetCDataReader(new StreamReader("../../TestData/VehicleCsv.xml").ReadToEnd())));

//            Assert.IsNotNull(vc);
//            Assert.AreEqual(155, vc.Count);

//            Assert.AreEqual("", vc[0].AgeInDays);
//            Assert.AreEqual("4 DOOR", vc[0].BodyType);
//            Assert.AreEqual("", vc[0].Certified);
//            Assert.AreEqual("6", vc[0].CylinderCount);
//            Assert.AreEqual("", vc[0].DealDate);
//            Assert.AreEqual("", vc[0].DoorCount);
//            Assert.AreEqual("", vc[0].DriveTrainDescription);
//            Assert.AreEqual("3", vc[0].EngineDescription);
//            Assert.AreEqual("BLACK", vc[0].ExteriorColor);
//            Assert.AreEqual("NEW", vc[0].Filler11);
//            Assert.AreEqual("", vc[0].FuelCode);
//            Assert.AreEqual("1", vc[0].InteriorColor);
//            Assert.AreEqual("VOLKSWAGEN", vc[0].Make);
//            Assert.AreEqual("41", vc[0].Mileage);
//            Assert.AreEqual("JETTA", vc[0].Model);
//            Assert.AreEqual("", vc[0].ModelPackage);
//            Assert.AreEqual("", vc[0].PackAmount);
//            Assert.AreEqual("12-12-2006", vc[0].ReceivedDate);
//            Assert.AreEqual("400.23", vc[0].ReconditionCost);
//            Assert.AreEqual("08-14-2008", vc[0].ReferenceDate);
//            Assert.AreEqual("", vc[0].SaleDescription);
//            Assert.AreEqual("", vc[0].SalePrice);
//            Assert.AreEqual("60801", vc[0].StockNumber);
//            Assert.AreEqual("2", vc[0].TRANS_NO);
//            Assert.AreEqual("24522.00", vc[0].UnitCost);
//            Assert.AreEqual("13000.00", vc[0].UnitSellingPrice);
//            Assert.AreEqual("IN STOCK", vc[0].VehicleStatus);
//            Assert.AreEqual("S", vc[0].VehicleStatusCode);
//            Assert.AreEqual("5", vc[0].VehicleType);
//            Assert.AreEqual("06", vc[0].VehicleYear);
//            Assert.AreEqual("3VWCT71K06M849156", vc[0].Vin);

//            Assert.AreEqual("", vc[1].AgeInDays);
//            Assert.AreEqual("4 DOOR", vc[1].BodyType);
//            Assert.AreEqual("", vc[1].Certified);
//            Assert.AreEqual("", vc[1].CylinderCount);
//            Assert.AreEqual("", vc[1].DealDate);
//            Assert.AreEqual("", vc[1].DoorCount);
//            Assert.AreEqual("", vc[1].DriveTrainDescription);
//            Assert.AreEqual("", vc[1].EngineDescription);
//            Assert.AreEqual("FAHRENHEIT_YELLOW", vc[1].ExteriorColor);
//            Assert.AreEqual("NEW", vc[1].Filler11);
//            Assert.AreEqual("", vc[1].FuelCode);
//            Assert.AreEqual("", vc[1].InteriorColor);
//            Assert.AreEqual("VOLKSWAGEN", vc[1].Make);
//            Assert.AreEqual("22", vc[1].Mileage);
//            Assert.AreEqual("JETTA", vc[1].Model);
//            Assert.AreEqual("", vc[1].ModelPackage);
//            Assert.AreEqual("", vc[1].PackAmount);
//            Assert.AreEqual("05-16-2007", vc[1].ReceivedDate);
//            Assert.AreEqual("166.25", vc[1].ReconditionCost);
//            Assert.AreEqual("08-14-2008", vc[1].ReferenceDate);
//            Assert.AreEqual("", vc[1].SaleDescription);
//            Assert.AreEqual("", vc[1].SalePrice);
//            Assert.AreEqual("70541", vc[1].StockNumber);
//            Assert.AreEqual("", vc[1].TransmissionDescription);
//            Assert.AreEqual("26825.00", vc[1].UnitCost);
//            Assert.AreEqual("18000.00", vc[1].UnitSellingPrice);
//            Assert.AreEqual("IN STOCK", vc[1].VehicleStatus);
//            Assert.AreEqual("S", vc[1].VehicleStatusCode);
//            Assert.AreEqual("", vc[1].VehicleType);
//            Assert.AreEqual("07", vc[1].VehicleYear);
//            Assert.AreEqual("3VWWJ71K67M159173", vc[1].Vin);
//        }

//        [Test]
//        public void MakeVehicleCollectionFromXml()
//        {
//            VehicleCollection vc = new VehicleCollection(GetXmlResponse("../../TestData/Vehicle.xml"));

//            Assert.IsNotNull(vc);
//            Assert.AreEqual(1, vc.Count);

//            Assert.AreEqual("", vc[0].AgeInDays);
//            Assert.AreEqual("4 DOOR", vc[0].BodyType);
//            Assert.AreEqual("", vc[0].Certified);
//            Assert.AreEqual("", vc[0].CylinderCount);
//            Assert.AreEqual("", vc[0].DealDate);
//            Assert.AreEqual("", vc[0].DoorCount);
//            Assert.AreEqual("", vc[0].DriveTrainDescription);
//            Assert.AreEqual("", vc[0].EngineDescription);
//            Assert.AreEqual("FAHRENHEIT_YELLOW", vc[0].ExteriorColor);
//            Assert.AreEqual("NEW", vc[0].Filler11);
//            Assert.AreEqual("", vc[0].FuelCode);
//            Assert.AreEqual("", vc[0].InteriorColor);
//            Assert.AreEqual("VOLKSWAGEN", vc[0].Make);
//            Assert.AreEqual("22", vc[0].Mileage);
//            Assert.AreEqual("JETTA", vc[0].Model);
//            Assert.AreEqual("", vc[0].ModelPackage);
//            Assert.AreEqual("", vc[0].PackAmount);
//            Assert.AreEqual("05-16-2007", vc[0].ReceivedDate);
//            Assert.AreEqual("166.25", vc[0].ReconditionCost);
//            Assert.AreEqual("08-14-2008", vc[0].ReferenceDate);
//            Assert.AreEqual("", vc[0].SaleDescription);
//            Assert.AreEqual("", vc[0].SalePrice);
//            Assert.AreEqual("70541", vc[0].StockNumber);
//            Assert.AreEqual("", vc[0].TransmissionDescription);
//            Assert.AreEqual("26825.00", vc[0].UnitCost);
//            Assert.AreEqual("18000.00", vc[0].UnitSellingPrice);
//            Assert.AreEqual("IN STOCK", vc[0].VehicleStatus);
//            Assert.AreEqual("S", vc[0].VehicleStatusCode);
//            Assert.AreEqual("", vc[0].VehicleType);
//            Assert.AreEqual("07", vc[0].VehicleYear);
//            Assert.AreEqual("3VWWJ71K67M159173", vc[0].Vin);
//        }

//        [Test]
//        public void WriteVehicleCollection()
//        {
//            VehicleCollection vc = new VehicleCollection(new CsvReader(GetCDataReader(new StreamReader("../../TestData/VehicleCsv.xml").ReadToEnd())));
//            StreamReader re = WriteAndGetReader("C:\\exampleVehicleCollection.txt", vc);

//            Assert.AreEqual("|4 DOOR||6||||3|BLACK|NEW||1|1402|VOLKSWAGEN|41|JETTA|||12-12-2006|400.23|08-14-2008|||60801|2|24522.00|13000.00||||IN STOCK|S|5|06|3VWCT71K06M849156", re.ReadLine());
//            Assert.AreEqual("|4 DOOR|||||||FAHRENHEIT_YELLOW|NEW|||1402|VOLKSWAGEN|22|JETTA|||05-16-2007|166.25|08-14-2008|||70541||26825.00|18000.00||||IN STOCK|S||07|3VWWJ71K67M159173", re.ReadLine());

//            re.Close();
//            File.Delete("C:\\exampleVehicleCollection.txt");
//        }

//    }
//}