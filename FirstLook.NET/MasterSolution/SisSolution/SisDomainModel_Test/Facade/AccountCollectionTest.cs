//using System.IO;
//using Firstlook.Sis.DomainModel.Serialization;
//using NUnit.Framework;

//namespace Firstlook.Sis.DomainModel.Facade
//{
//    [TestFixture]
//    public class AccountCollectionTest : BaseCollectionTest
//    {
//        [Test]
//        public void MakeAccountCollectionFromCData()
//        {
//            AccountCollection ac = new AccountCollection(new CsvReader(GetCDataReader(new StreamReader("../../TestData/AccountCsv.xml").ReadToEnd())));

//            Assert.IsNotNull(ac);
//            Assert.AreEqual(802, ac.Count);

//            Assert.AreEqual("4116", ac[0].AccountNumber);
//            Assert.AreEqual("SLS PASSAT WAG OVR/DIS", ac[0].Description);
//            Assert.AreEqual("S", ac[0].AccountType);
//            Assert.AreEqual("", ac[0].SubType);
//            Assert.AreEqual("0", ac[0].ControlType);
//            Assert.AreEqual("01", ac[0].Department);
//            Assert.AreEqual("1", ac[0].CompanyId);

//            Assert.AreEqual("6150", ac[1].AccountNumber);
//            Assert.AreEqual("NEW OTH DEPR ASSETS EXP", ac[1].Description);
//            Assert.AreEqual("E", ac[1].AccountType);
//            Assert.AreEqual("", ac[1].SubType);
//            Assert.AreEqual("0", ac[1].ControlType);
//            Assert.AreEqual("01", ac[1].Department);
//            Assert.AreEqual("1", ac[1].CompanyId);
//        }

//        [Test]
//        public void WriteAccountCollection()
//        {
//            AccountCollection ac = new AccountCollection(new CsvReader(GetCDataReader(new StreamReader("../../TestData/AccountCsv.xml").ReadToEnd())));
//            StreamReader re = this.WriteAndGetReader("C:\\exampleAccountCollection.txt", ac);

//            Assert.AreEqual("4116|S|1|0|01|SLS PASSAT WAG OVR/DIS|", re.ReadLine());
//            Assert.AreEqual("6150|E|1|0|01|NEW OTH DEPR ASSETS EXP|", re.ReadLine());

//            re.Close();
//            File.Delete("C:\\exampleAccountCollection.txt");
//        }

//    }
//}