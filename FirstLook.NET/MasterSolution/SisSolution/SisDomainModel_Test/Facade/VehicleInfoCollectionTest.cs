//using System.IO;
//using NUnit.Framework;

//namespace Firstlook.Sis.DomainModel.Facade
//{
//    [TestFixture]
//    public class VehicleInfoCollectionTest : BaseCollectionTest
//    {
//        [Test]
//        public void MakeVehicleInfoCollectionFromXml()
//        {
//            VehicleInfoCollection vic = new VehicleInfoCollection(GetXmlResponse("../../TestData/VehicleInfo.xml"));

//            Assert.IsNotNull(vic);
//            Assert.AreEqual(155, vic.Count);

//            Assert.AreEqual("N", vic[0].IsNew);
//            Assert.AreEqual("60801", vic[0].StockNumber);
//            Assert.AreEqual("3VWCT71K06M849156", vic[0].VIN);

//            Assert.AreEqual("N", vic[5].IsNew);
//            Assert.AreEqual("70558", vic[5].StockNumber);
//            Assert.AreEqual("WVWBR71K47W249576", vic[5].VIN);

//            Assert.AreEqual("U", vic[36].IsNew);
//            Assert.AreEqual("P1730A", vic[36].StockNumber);
//            Assert.AreEqual("WVWPD63B44P203912", vic[36].VIN);

//            Assert.AreEqual("U", vic[65].IsNew);
//            Assert.AreEqual("P1719", vic[65].StockNumber);
//            Assert.AreEqual("WVWVD63B53E464158", vic[65].VIN);

//            Assert.AreEqual("N", vic[88].IsNew);
//            Assert.AreEqual("70171", vic[88].StockNumber);
//            Assert.AreEqual("WVWEV71KX7W090183", vic[88].VIN);
//        }

//        [Test]
//        public void WriteVehicleInfoCollection()
//        {
//            VehicleInfoCollection vc = new VehicleInfoCollection(GetXmlResponse("../../TestData/VehicleInfo.xml"));
//            StreamReader re = WriteAndGetReader("C:\\exampleVehicleInfoCollection.txt", vc);

//            Assert.AreEqual("N|60801|3VWCT71K06M849156", re.ReadLine());
//            Assert.AreEqual("N|70541|3VWWJ71K67M159173", re.ReadLine());
//            Assert.AreEqual("N|70546|3VWRW31C97M517771", re.ReadLine());
//            Assert.AreEqual("N|70548|3VWPF71K27M170984", re.ReadLine());
//            Assert.AreEqual("N|70554|WVWUK73C07E219788", re.ReadLine());
//            Assert.AreEqual("N|70558|WVWBR71K47W249576", re.ReadLine());

//            re.Close();
//            File.Delete("C:\\exampleVehicleInfoCollection.txt");
//        }
//    }
//}