//using System.IO;
//using Firstlook.Sis.DomainModel.Serialization;
//using NUnit.Framework;

//namespace Firstlook.Sis.DomainModel.Facade
//{
//    [TestFixture]
//    public class JournalCollectionTest : BaseCollectionTest
//    {
//        [Test]
//        public void MakeJournalCollectionFromCData()
//        {
//            JournalCollection jc = new JournalCollection(new CsvReader(GetCDataReader(new StreamReader("../../TestData/JournalCsv.xml").ReadToEnd())));

//            Assert.IsNotNull(jc);
//            Assert.AreEqual(28, jc.Count);

//            Assert.AreEqual("1", jc[0].CompanyId);
//            Assert.AreEqual("SET UP GENERAL LEDGER", jc[0].Description);
//            Assert.AreEqual("95", jc[0].JournalNumber);
//            Assert.AreEqual("B", jc[0].JournalType);

//            Assert.AreEqual("1", jc[1].CompanyId);
//            Assert.AreEqual("PARTS SALES CASH", jc[1].Description);
//            Assert.AreEqual("32", jc[1].JournalNumber);
//            Assert.AreEqual("S", jc[1].JournalType);
//        }

//        [Test]
//        public void WriteJournalCollection()
//        {
//            JournalCollection jc = new JournalCollection(new CsvReader(GetCDataReader(new StreamReader("../../TestData/JournalCsv.xml").ReadToEnd())));
//            StreamReader re = this.WriteAndGetReader("C:\\exampleJournalCollection.txt", jc);

//            Assert.AreEqual("1|SET UP GENERAL LEDGER|95|B", re.ReadLine());
//            Assert.AreEqual("1|PARTS SALES CASH|32|S", re.ReadLine());

//            re.Close();
//            File.Delete("C:\\exampleJournalCollection.txt");
//        }
//    }
//}