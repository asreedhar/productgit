//using System.Xml;
//using NUnit.Framework;

//namespace Firstlook.Sis.DomainModel.Facade
//{
//    [TestFixture]
//    public class RequestGeneratorTest
//    {
//        [Test]
//        public void GenerateRequest()
//        {
//            RequestGenerator rg = new RequestGenerator("BATCH_ACCTG_SALES");
//            rg.SetParameter("START_DATE", "08-01-07");
//            rg.SetParameter("END_DATE", "08-01-07");
//            rg.SetParameter("JRNLS", "40,60");

//            XmlDocument doc = new XmlDocument();
//            doc.LoadXml(rg.ToString());

//            Assert.AreEqual(doc.DocumentElement.Name, "DMS_TRANSACTION");

//            Assert.IsNotNull(doc.SelectSingleNode("/DMS_TRANSACTION/TRANSACTION_ID"));
//            Assert.IsNotNull(doc.SelectSingleNode("/DMS_TRANSACTION/COMPANY_ID"));
//            Assert.IsNotNull(doc.SelectSingleNode("/DMS_TRANSACTION/VENDOR_TYPE"));
//            Assert.IsNotNull(doc.SelectSingleNode("/DMS_TRANSACTION/DMS_TYPE"));
//            Assert.IsNotNull(doc.SelectSingleNode("/DMS_TRANSACTION/DMS_ACCT"));
//            Assert.IsNotNull(doc.SelectSingleNode("/DMS_TRANSACTION/DMS_BRANCH"));
//            Assert.IsNotNull(doc.SelectSingleNode("/DMS_TRANSACTION/START_DATE"));
//            Assert.IsNotNull(doc.SelectSingleNode("/DMS_TRANSACTION/END_DATE"));
//            Assert.IsNotNull(doc.SelectSingleNode("/DMS_TRANSACTION/JRNLS"));
//            Assert.IsNotNull(doc.SelectSingleNode("/DMS_TRANSACTION"));

//            Assert.AreEqual(doc.SelectSingleNode("/DMS_TRANSACTION/TRANSACTION_TYPE").InnerText, "BATCH_ACCTG_SALES");
//            Assert.AreEqual(doc.SelectSingleNode("/DMS_TRANSACTION/VENDOR_TYPE").InnerText, "FLO");
//            Assert.AreEqual(doc.SelectSingleNode("/DMS_TRANSACTION/START_DATE").InnerText, "08-01-07");
//            Assert.AreEqual(doc.SelectSingleNode("/DMS_TRANSACTION/END_DATE").InnerText, "08-01-07");
//            Assert.AreEqual(doc.SelectSingleNode("/DMS_TRANSACTION/JRNLS").InnerText, "40,60");

//            Assert.AreEqual(doc.DocumentElement.ChildNodes.Count, 10);
//        }
//    }
//}