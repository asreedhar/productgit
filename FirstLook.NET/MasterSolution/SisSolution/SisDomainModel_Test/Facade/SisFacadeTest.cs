//using System;
//using System.Collections.Generic;
//using System.Threading;
//using System.Web.Services.Protocols;
//using Firstlook.Sis.DomainModel.Facade;
//using Firstlook.Sis.DomainModel.Serialization;
//using NUnit.Framework;

//namespace Firstlook.Sis.DomainModel
//{
//    [TestFixture]
//    public class SisFacadeTest
//    {
//        private static SisFacade GetFacade()
//        {
//            return new SisFacade(new Uri("https://uset.datadms.com/DmsDealsV4/dealController.asmx "));
//        }

//        private static Credential GetCredential()
//        {
//            return new Credential("12604FLO", "P@SSW0RD");
//        }

//        private static Receipt GetReceipt()
//        {
//            return GetFacade().GetVehicleList(GetCredential(), "");
//        }




//        [Test]
//        public void SubmitRequest()
//        {
//            Receipt r = GetReceipt();
//            Assert.IsNotNull(r);
//            Assert.IsNotNull(r.DmsId);
//            Assert.Greater(r.DmsId, 0);
//        }

//        [Test]
//        public void GetConnectionStatus()
//        {
//            List<ConnectionStatus> status = GetFacade().GetConnectionStatus(GetCredential());
//            Assert.IsNotNull(status);
//        }

//        [Test, ExpectedException(ExceptionType = typeof(SisInvalidResponseException))]
//        public void CheckResponseForErrors()
//        {
//            Receipt testReceipt = GetFacade().GetDeals(GetCredential(), DateTime.MinValue, DateTime.MaxValue);
//            while (GetFacade().CheckStatus(GetCredential(), testReceipt).QueryStatus != QueryStatus.Completed)
//            {
//                Thread.Sleep(4000);
//            }
//            IPsvSerializable item = GetFacade().GetResponse(GetCredential(), testReceipt, QueryType.GetDeals);
//        }

//        [Test]
//        public void GetAccounts()
//        {
//            Receipt r = GetFacade().GetAccounts(GetCredential());
//            QueryStatusInfo inf = GetFacade().CheckStatus(GetCredential(), r);

//            ValidateQueryStatusInfo(inf);

//            while (GetFacade().CheckStatus(GetCredential(), r).QueryStatus != QueryStatus.Completed)
//            {
//                Thread.Sleep(4000);
//            }

//            ValidateResult(GetFacade().GetResponse(GetCredential(), r, QueryType.GetAccounts));
//        }

//        [Test]
//        public void GetDeal()
//        {
//            Receipt r = GetFacade().GetDeal(GetCredential(), 45976);
//            QueryStatusInfo inf = GetFacade().CheckStatus(GetCredential(), r);

//            ValidateQueryStatusInfo(inf);

//            while (GetFacade().CheckStatus(GetCredential(), r).QueryStatus != QueryStatus.Completed)
//            {
//                Thread.Sleep(4000);
//            }

//            ValidateResult(GetFacade().GetResponse(GetCredential(), r, QueryType.GetDeal));
//        }

//        [Test]
//        public void GetDeals()
//        {
//            Receipt r = GetFacade().GetDeals(GetCredential(), DateTime.Parse("08-01-07"), DateTime.Parse("08-01-07"));
//            QueryStatusInfo inf = GetFacade().CheckStatus(GetCredential(), r);

//            ValidateQueryStatusInfo(inf);

//            while (GetFacade().CheckStatus(GetCredential(), r).QueryStatus != QueryStatus.Completed)
//            {
//                Thread.Sleep(4000);
//            }

//            ValidateResult(GetFacade().GetResponse(GetCredential(), r, QueryType.GetDeals));
//        }

//        [Test]
//        public void GetJournalEntries()
//        {
//            Receipt r = GetFacade().GetJournalEntries(GetCredential(), DateTime.Parse("07-01-07"), DateTime.Parse("07-31-07"), new string[] { "70" }, new string[] { "70" });
//            QueryStatusInfo inf = GetFacade().CheckStatus(GetCredential(), r);

//            ValidateQueryStatusInfo(inf);

//            while (GetFacade().CheckStatus(GetCredential(), r).QueryStatus != QueryStatus.Completed)
//            {
//                Thread.Sleep(4000);
//            }

//            ValidateResult(GetFacade().GetResponse(GetCredential(), r, QueryType.GetJournalDetail));
//        }

//        [Test]
//        public void GetJournals()
//        {
//            Receipt r = GetFacade().GetJournals(GetCredential());
//            QueryStatusInfo inf = GetFacade().CheckStatus(GetCredential(), r);

//            ValidateQueryStatusInfo(inf);

//            while (GetFacade().CheckStatus(GetCredential(), r).QueryStatus != QueryStatus.Completed)
//            {
//                Thread.Sleep(4000);
//            }

//            ValidateResult(GetFacade().GetResponse(GetCredential(), r, QueryType.GetJournals));
//        }

//        [Test]
//        public void GetSchedules()
//        {
//            Receipt r = GetFacade().GetSchedules(GetCredential());
//            QueryStatusInfo inf = GetFacade().CheckStatus(GetCredential(), r);

//            ValidateQueryStatusInfo(inf);

//            while (GetFacade().CheckStatus(GetCredential(), r).QueryStatus != QueryStatus.Completed)
//            {
//                Thread.Sleep(4000);
//            }

//            ValidateResult(GetFacade().GetResponse(GetCredential(), r, QueryType.GetSchedules));
//        }

//        [Test]
//        public void GetVehicle()
//        {
//            Receipt r = GetFacade().GetVehicle(GetCredential(), "70541");
//            QueryStatusInfo inf = GetFacade().CheckStatus(GetCredential(), r);

//            ValidateQueryStatusInfo(inf);

//            while (GetFacade().CheckStatus(GetCredential(), r).QueryStatus != QueryStatus.Completed)
//            {
//                Thread.Sleep(4000);
//            }

//            ValidateResult(GetFacade().GetResponse(GetCredential(), r, QueryType.GetVehicle));
//        }

//        [Test]
//        public void GetVehicleList()
//        {
//            Receipt r = GetFacade().GetVehicleList(GetCredential(), "");
//            QueryStatusInfo inf = GetFacade().CheckStatus(GetCredential(), r);

//            ValidateQueryStatusInfo(inf);

//            while (GetFacade().CheckStatus(GetCredential(), r).QueryStatus != QueryStatus.Completed)
//            {
//                Thread.Sleep(4000);
//            }

//            ValidateResult(GetFacade().GetResponse(GetCredential(), r, QueryType.GetVehicleList));
//        }

//        [Test]
//        public void GetVehicles()
//        {
//            Receipt r = GetFacade().GetVehicles(GetCredential(), "");
//            QueryStatusInfo inf = GetFacade().CheckStatus(GetCredential(), r);

//            ValidateQueryStatusInfo(inf);

//            while (GetFacade().CheckStatus(GetCredential(), r).QueryStatus != QueryStatus.Completed)
//            {
//                Thread.Sleep(4000);
//            }

//            ValidateResult(GetFacade().GetResponse(GetCredential(), r, QueryType.GetVehicles));
//        }

//        [Test]
//        public void ValidateQueryStatusInfo(QueryStatusInfo info)
//        {
//            Assert.IsNotNull(info);
//            Assert.AreNotEqual(info.QueryStatus, QueryStatus.NotSubmitted);
//        }

//        [Test]
//        public void ValidateResult(IPsvSerializable item)
//        {
//            Assert.IsNotNull(item);
//        }
//    }
//}