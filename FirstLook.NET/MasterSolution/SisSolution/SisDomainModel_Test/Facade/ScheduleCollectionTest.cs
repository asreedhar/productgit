//using System.IO;
//using Firstlook.Sis.DomainModel.Serialization;
//using NUnit.Framework;

//namespace Firstlook.Sis.DomainModel.Facade
//{
//    [TestFixture]
//    public class ScheduleCollectionTest : BaseCollectionTest
//    {
//        [Test]
//        public void MakeScheduleCollectionFromCData()
//        {
//            ScheduleCollection sc = new ScheduleCollection(new CsvReader(GetCDataReader(new StreamReader("../../TestData/ScheduleCsv.xml").ReadToEnd())));

//            Assert.IsNotNull(sc);
//            Assert.AreEqual(22, sc.Count);

//            Assert.AreEqual("1", sc[0].CompanyId);
//            Assert.AreEqual("12", sc[0].ControlType);
//            Assert.AreEqual("", sc[0].ControlType2);
//            Assert.AreEqual("HOLDBACK (1308)", sc[0].Description);
//            Assert.AreEqual("8", sc[0].ScheduleNumber);
//            Assert.AreEqual("2", sc[0].ScheduleType);

//            Assert.AreEqual("1", sc[1].CompanyId);
//            Assert.AreEqual("7", sc[1].ControlType);
//            Assert.AreEqual("", sc[1].ControlType2);
//            Assert.AreEqual("ACCURED SALES SALARIES", sc[1].Description);
//            Assert.AreEqual("2", sc[1].ScheduleNumber);
//            Assert.AreEqual("1", sc[1].ScheduleType);
//        }

//        [Test]
//        public void WriteScheduleCollection()
//        {
//            ScheduleCollection sc = new ScheduleCollection(new CsvReader(GetCDataReader(new StreamReader("../../TestData/ScheduleCsv.xml").ReadToEnd())));
//            StreamReader re = WriteAndGetReader("C:\\exampleScheduleCollection.txt", sc);

//            Assert.AreEqual("1|12||HOLDBACK (1308)|8|2", re.ReadLine());
//            Assert.AreEqual("1|7||ACCURED SALES SALARIES|2|1", re.ReadLine());

//            re.Close();
//            File.Delete("C:\\exampleScheduleCollection.txt");
//        }
//    }
//}