//using System.IO;
//using Firstlook.Sis.DomainModel.Serialization;
//using NUnit.Framework;

//namespace Firstlook.Sis.DomainModel.Facade
//{
//    [TestFixture]
//    public class JournalEntryCollectionTest : BaseCollectionTest
//    {
//        [Test]
//        public void MakeJournalEntryCollectionFromCData()
//        {
//            JournalEntryCollection jec = new JournalEntryCollection(new CsvReader(GetCDataReader(new StreamReader("../../TestData/JournalEntryCsv.xml").ReadToEnd())));

//            Assert.IsNotNull(jec);
//            Assert.AreEqual(209, jec.Count);

//            Assert.AreEqual("AR HOLDBACK", jec[0].AccountDescription);
//            Assert.AreEqual("1308", jec[0].AccountNumber);
//            Assert.AreEqual("A", jec[0].AccountType);
//            Assert.AreEqual("1", jec[0].CompanyId);
//            Assert.AreEqual("70624", jec[0].ControlNumber);
//            Assert.AreEqual("", jec[0].ControlNumber2);
//            Assert.AreEqual("12", jec[0].ControlType);
//            Assert.AreEqual("", jec[0].ControlType2);
//            Assert.AreEqual("07-06-2007", jec[0].JournalDate);
//            Assert.AreEqual("VEHICLE PURCHASES", jec[0].JournalDescription);
//            Assert.AreEqual("1*70*70642*14432*28855042*1", jec[0].JournalFullId);
//            Assert.AreEqual("70", jec[0].JournalId);
//            Assert.AreEqual("-633.00", jec[0].PostAmount);
//            Assert.AreEqual("", jec[0].PostDescription);
//            Assert.AreEqual("1", jec[0].PostSequence);
//            Assert.AreEqual("08:00", jec[0].PostTime);
//            Assert.AreEqual("", jec[0].ProductNumber);
//            Assert.AreEqual("0", jec[0].ProductTypeCode);
//            Assert.AreEqual("70642", jec[0].ReferenceNumber);
//            Assert.AreEqual("", jec[0].StatCount);

//            Assert.AreEqual("INV - NEW CAR", jec[1].AccountDescription);
//            Assert.AreEqual("1402", jec[1].AccountNumber);
//            Assert.AreEqual("A", jec[1].AccountType);
//            Assert.AreEqual("1", jec[1].CompanyId);
//            Assert.AreEqual("70642", jec[1].ControlNumber);
//            Assert.AreEqual("", jec[1].ControlNumber2);
//            Assert.AreEqual("5", jec[1].ControlType);
//            Assert.AreEqual("", jec[1].ControlType2);
//            Assert.AreEqual("07-06-2007", jec[1].JournalDate);
//            Assert.AreEqual("VEHICLE PURCHASES", jec[1].JournalDescription);
//            Assert.AreEqual("1*70*70642*14432*35067487*1", jec[1].JournalFullId);
//            Assert.AreEqual("70", jec[1].JournalId);
//            Assert.AreEqual("20249.00", jec[1].PostAmount);
//            Assert.AreEqual("", jec[1].PostDescription);
//            Assert.AreEqual("1", jec[1].PostSequence);
//            Assert.AreEqual("09:44", jec[1].PostTime);
//            Assert.AreEqual("", jec[1].ProductNumber);
//            Assert.AreEqual("0", jec[1].ProductTypeCode);
//            Assert.AreEqual("70642", jec[1].ReferenceNumber);
//            Assert.AreEqual("0", jec[1].StatCount);
//        }

//        [Test]
//        public void WriteJournalEntryCollection()
//        {
//            JournalEntryCollection jec = new JournalEntryCollection(new CsvReader(GetCDataReader(new StreamReader("../../TestData/JournalEntryCsv.xml").ReadToEnd())));
//            StreamReader re = WriteAndGetReader("C:\\exampleJournalEntryCollection.txt", jec);

//            Assert.AreEqual("AR HOLDBACK|1308|A|1|70624||12||07-06-2007|VEHICLE PURCHASES|1*70*70642*14432*28855042*1|70|-633.00||1|08:00||0|70642|", re.ReadLine());
//            Assert.AreEqual("INV - NEW CAR|1402|A|1|70642||5||07-06-2007|VEHICLE PURCHASES|1*70*70642*14432*35067487*1|70|20249.00||1|09:44||0|70642|0", re.ReadLine());

//            re.Close();
//            File.Delete("C:\\exampleJournalEntryCollection.txt");
//        }
//    }
//}