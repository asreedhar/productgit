using System.ServiceProcess;
using System.Threading;
using Firstlook.Sis.DomainModel.Serialization;
using Firstlook.Sis.SisProcessorService;

namespace Firstlook.Sis.SisProcessorService
{
    /// <summary>
    /// Windows Service which runs 3 parallel threads each with its own QueryProcessor.
    /// Each QueryProcessor aligns to a specific QueryProcessorState, so each QueryProcessor effectively handles only a certain kind of work.
    /// </summary>
    internal partial class SisProcessorService : ServiceBase
    {
        /// <summary>
        /// A QueryProcessor to submit requests to the SisFacade.
        /// </summary>
        private readonly QueryProcessor queryProcessorSubmit;

        /// <summary>
        /// A QueryProcessor to check the status of requests submitted to the SisFacade.
        /// </summary>
        private readonly QueryProcessor queryProcessorStatus;

        /// <summary>
        /// A QueryProcessor to get the response from requests submitted to the SisFacade.
        /// </summary>
        private readonly QueryProcessor queryProcessorResponse;

        /// <summary>
        /// A thread to run the QueryProcessor to submit requests to the SisFacade.
        /// </summary>
        private Thread submitThread;

        /// <summary>
        /// A thread to run the QueryProcessor to check the status of requests submitted to the SisFacade.
        /// </summary>
        private Thread statusThread;

        /// <summary>
        /// A thread to run the QueryProcessor to get the response from requests submitted to the SisFacade.
        /// </summary>
        private Thread responseThread;

        /// <summary>
        /// A writer used to output the results of the SisFacade calls.
        /// </summary>
        private readonly IPsvWriter writer;


        /// <summary>
        /// Constructor for the SisWindows Service.  Establishes a unique thread for a QueryProcessor for each QueryProcessorState, a reusable SisFacade, and an IPsvWriter.
        /// </summary>
        public SisProcessorService()
        {
            InitializeComponent();

            writer = new FlatFilePsvWriter();
            queryProcessorSubmit = new QueryProcessor(QueryProcessorState.Submit, writer);
            queryProcessorStatus = new QueryProcessor(QueryProcessorState.Status, writer);
            queryProcessorResponse = new QueryProcessor(QueryProcessorState.Response, writer);
        }


        /// <summary>
        /// Starts each thread (one unique thread for each QueryProcessorState).
        /// </summary>
        protected override void OnStart(string[] args)
        {
            submitThread = new Thread(queryProcessorSubmit.Process);
            statusThread = new Thread(queryProcessorStatus.Process);
            responseThread = new Thread(queryProcessorResponse.Process);

            queryProcessorSubmit.Restart();
            queryProcessorStatus.Restart();
            queryProcessorResponse.Restart();

            submitThread.Start();
            statusThread.Start();
            responseThread.Start();
        }

        /// <summary>
        /// Invokes the Stop method on each QueryProcessor, calling a Join on each thread to ensure all threads terminate before the calling thread.
        /// </summary>
        protected override void OnStop()
        {
            queryProcessorSubmit.Stop();
            queryProcessorStatus.Stop();
            queryProcessorResponse.Stop();

            submitThread.Join();
            statusThread.Join();
            responseThread.Join();
        }
    }
}