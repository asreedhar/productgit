using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Xml;
using Firstlook.Sis.DomainModel;

namespace Firstlook.Sis.SisProcessorService
{
    /// <summary>
    /// A query processor command representing delay between query attempts, the Query object, and login credentials to pass to the SisFacade.
    /// </summary>
    public class QueryProcessorCommand
    {
        /// <summary>
        /// The delay between query attempts.
        /// </summary>
        private readonly int delay;

        /// <summary>
        /// The login credentials to pass to the SisFacade.
        /// </summary>
        private readonly Credential credential;

        /// <summary>
        /// The Query object containing Receipt, QueryStatusInfo, etc to pass to the SisFacade.
        /// </summary>
        private readonly Query query;




        /// <summary>
        /// Gets the delay between query attempts.
        /// </summary>
        public int Delay
        {
            get { return delay; }
        }

        /// <summary>
        /// Gets the login credentials to pass to the SisFacade.
        /// </summary>
        public Credential Credential
        {
            get { return credential; }
        }

        /// <summary>
        /// Get the Query object containing Receipt, QueryStatusInfo, etc to pass to the SisFacade.
        /// </summary>
        public Query Query
        {
            get { return query; }
        }

        /// <summary>
        /// If the QueryProcessorCommand has a Query object.
        /// </summary>
        public bool HasQuery
        {
            get { return query != null; }
        }




        /// <summary>
        /// Initializes a new instance of the QueryProcessorCommand class.
        /// </summary>
        /// <param name="delay">The delay between query attempts.</param>
        /// <param name="credential">The login credentials to pass to the SisFacade.</param>
        /// <param name="query">The Query object containing Receipt, QueryStatusInfo, etc to pass to the SisFacade.</param>
        internal QueryProcessorCommand(int delay, Credential credential, Query query)
        {
            this.delay = delay;
            this.credential = credential;
            this.query = query;
        }




        /// <summary>
        /// Obtains a new Query whose QueryStatus aligns with the QueryProcessorState passed in.
        /// </summary>
        /// <param name="state">The QueryProcessorState used to obtain queries.</param>
        public static QueryProcessorCommand GetQueryProcessorCommand(QueryProcessorState state)
        {
            //result placeholder
            QueryProcessorCommand qpc = null;

            //check database for work items based on QueryProcessorState passed in
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                //Open database connection
                conn.Open();

                //execute the GetQuery stored procedure
                using (SqlCommand comm = new SqlCommand("Runtime.Query#Get", conn))
                {
                    comm.CommandType = CommandType.StoredProcedure;

                    string buffer = "";

                    //matches the QueryProcessorState to the SisFacade QueryStatus
                    switch (state)
                    {
                        case QueryProcessorState.Submit:
                            buffer = QueryStatus.NotSubmitted.ToString();
                            break;
                        case QueryProcessorState.Status:
                            buffer = QueryStatus.Submitted.ToString();
                            break;
                        case QueryProcessorState.Response:
                            buffer = QueryStatus.Accepted.ToString();
                            break;
                    }


                    //Add parameter for QueryProcessorState to grab appropriate query from queue
                    comm.Parameters.Add(new SqlParameter("@ProcessBuffer", buffer));

                    //run stored procedure and get data reader
                    using (SqlDataReader rdr = comm.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (rdr.Read())
                        {
                            //set credential from reader
                            Credential credential = new Credential(rdr["LogonName"].ToString(), rdr["Password"].ToString());

                            //check request type from reader
                            QueryType type = (QueryType)Enum.Parse(typeof(QueryType), rdr["TransactionType"].ToString());

                            //placeholder for receipt, may not have one if a query has not been submitted yet
                            Receipt receipt = null;

                            //If the query already has a dms id, set dms id from reader
                            if (!String.IsNullOrEmpty(rdr["TransactionId"].ToString()))
                            {
                                receipt = new Receipt(int.Parse(rdr["TransactionId"].ToString()), new XmlDocument());
                            }

                            //sets the Query's Guid
                            Guid id = new Guid(rdr["QueryId"].ToString());

                            //initialize the QueryParameterCollection
                            QueryParameterCollection queryparams = new QueryParameterCollection();

                            //if a second result set exists, it contains parameters for the query
                            if (rdr.NextResult())
                            {
                                while (rdr.Read())
                                {
                                    //add as new query parameter
                                    queryparams.Add(new QueryParameter(rdr["ParameterName"].ToString(), rdr["ParameterValue"].ToString()));
                                }
                            }

                            //setup a new Query based on all the above info
                            Query q = new Query(new Uri(ConfigurationManager.AppSettings["SisUrl"]), id, type, receipt, queryparams);

                            //set delay between queries, since a query exist the delay is 0
                            int querydelay = int.TryParse(ConfigurationManager.AppSettings["QueryDelay"], out querydelay) ? querydelay : 0;

                            //setup a new QueryProcessorCommand to return
                            qpc = new QueryProcessorCommand(querydelay, credential, q);
                        }
                    }
                }
            }

            //if the QueryProcessorCommand didnt make it out error-free, setup a new QueryProcessorCommand that is empty
            if (qpc == null)
            {
                //set delay between queries, since no query exists the delay is 60
                int noquerydelay = int.TryParse(ConfigurationManager.AppSettings["NoQueryDelay"], out noquerydelay) ? noquerydelay : 60;

                qpc = new QueryProcessorCommand(noquerydelay, null, null);
            }

            return qpc;
        }

        /// <summary>
        /// Uses a Receipt to update the queue with the QueryProcessorCommand's updated status.
        /// </summary>
        /// <param name="receipt">Receipt to identify the query to the queue.</param>
        public void Submitted(Receipt receipt)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                //Open database connection
                conn.Open();

                using (SqlCommand comm = new SqlCommand("Runtime.Query#SetTransaction", conn))
                {
                    comm.CommandType = CommandType.StoredProcedure;

                    //Add parameter for Transaction ID
                    comm.Parameters.Add(new SqlParameter("@TransactionId", receipt.DmsId));

                    //Add parameter for Request Xml
                    comm.Parameters.Add(new SqlParameter("@RequestXML", receipt.RequestXml.OuterXml));

                    //Add parameter for QueryID
                    comm.Parameters.Add(new SqlParameter("@QueryId ", query.ID));

                    comm.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Uses a QueryStatusInfo to update the queue with the QueryProcessorCommand's updated QueryStatusInfo.
        /// </summary>
        /// <param name="info">QueryStatusInfo to identify the query to the queue.</param>
        public void StatusChecked(QueryStatusInfo info)
        {
            using (SqlConnection conn =new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                //Open database connection
                conn.Open();

                //execute the QueryStatusChecked stored procedure
                using (SqlCommand comm = new SqlCommand("Runtime.QueryTransaction#SetStatus", conn))
                {
                    comm.CommandType = CommandType.StoredProcedure;

                    //Add parameter for Transaction ID
                    comm.Parameters.Add(new SqlParameter("@TransactionId", query.Receipt.DmsId));

                    //Add parameter for if the request has been read by Sis
                    comm.Parameters.Add(new SqlParameter("@IsRead", info.DateAccepted != DateTime.MinValue));

                    //Add parameter for if the request has been processed by Sis
                    comm.Parameters.Add(new SqlParameter("@IsUpdated", info.DateCompleted != DateTime.MinValue));

                    //Add parameter for when the request was read by Sis
                    if (info.DateAccepted == DateTime.MinValue)
                        comm.Parameters.Add(new SqlParameter("@ReadDate", DBNull.Value));
                    else
                        comm.Parameters.Add(new SqlParameter("@ReadDate", info.DateAccepted));

                    //Add parameter for when the request was processed by Sis
                    if (info.DateCompleted == DateTime.MinValue)
                        comm.Parameters.Add(new SqlParameter("@UpdatedDate", DBNull.Value));
                    else
                        comm.Parameters.Add(new SqlParameter("@UpdatedDate", info.DateCompleted));

                    //Add null parameter for when the request was completed, since it hasn't been completed yet
                    comm.Parameters.Add(new SqlParameter("@CompletedDate", DBNull.Value));

                    //Execute the call
                    comm.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Update the queue with a completed query.
        /// </summary>
        public void Completed()
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                //Open database connection
                conn.Open();

                using (SqlCommand comm = new SqlCommand("Runtime.QueryTransaction#SetStatusComplete", conn))
                {
                    comm.CommandType = CommandType.StoredProcedure;

                    //Add parameter for Dms ID
                    comm.Parameters.Add(new SqlParameter("@TransactionId", query.Receipt.DmsId));

                    //Add parameter for when the request was completed
                    comm.Parameters.Add(new SqlParameter("@CompletedDate", DateTime.Now));

                    comm.ExecuteNonQuery();
                }
            }
        }


        /// <summary>
        /// Sends the details of an SisException related to a Query to the database for storing and processing.
        /// </summary>
        /// <param name="e">The SisException to process.</param>
        /// <param name="query">The SisException to process.</param>
        public static void ProcessException(Exception e, Query query)
        {
            try
            {
                StringBuilder sb = new StringBuilder();

                sb.AppendLine(e.StackTrace);

                if (e.InnerException != null)
                {
                    int innerReferences = 0;
                    Exception innerException = e.InnerException;

                    while (innerException != null && innerReferences < 50)
                    {
                        sb.AppendLine(innerException.StackTrace);
                        innerException = innerException.InnerException;
                        innerReferences++;
                    }
                }

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
                {
                    //Open database connection
                    conn.Open();

                    using (SqlCommand comm = new SqlCommand("Runtime.Exception#Insert", conn))
                    {
                        comm.CommandType = CommandType.StoredProcedure;

                        //Add parameter for Query ID, if there is one
                        if(query == null)
                            comm.Parameters.Add(new SqlParameter("@QueryId", DBNull.Value));
                        else
                            comm.Parameters.Add(new SqlParameter("@QueryId", query.ID));

                        //Add parameter for Exception Type
                        comm.Parameters.Add(new SqlParameter("@ErrorType", e.GetType().FullName));

                        //Add parameter for Exception Message
                        comm.Parameters.Add(new SqlParameter("@ErrorMessage", e.Message));

                        //Add parameter for Exception StackTrace
                        comm.Parameters.Add(new SqlParameter("@ErrorDetail", sb.ToString()));

                        comm.ExecuteNonQuery();
                    }
                }
            }
            catch
            {

            }
        }


        /// <summary>
        /// Sends the details of an SisException to the database for storing and processing.
        /// </summary>
        /// <param name="e">The SisException to process.</param>
        public static void ProcessException(Exception e)
        {
            ProcessException(e, null);
        }
    }
}