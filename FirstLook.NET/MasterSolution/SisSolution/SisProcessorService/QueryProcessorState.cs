namespace Firstlook.Sis.SisProcessorService
{
    /// <summary>
    /// Represents all possible states of a Query Processor.
    /// </summary>
    public enum QueryProcessorState
    {
        /// <summary>
        /// Submit requests to Sis.
        /// </summary>
        Submit,

        /// <summary>
        /// Check status of requests submitted to Sis.
        /// </summary>
        Status,

        /// <summary>
        /// Get the response of requests submitted to Sis.
        /// </summary>
        Response
    }
}