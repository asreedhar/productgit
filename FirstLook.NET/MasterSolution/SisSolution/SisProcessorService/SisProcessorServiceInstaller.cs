using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;

namespace Firstlook.Sis.SisProcessorService
{
    [RunInstaller(true)]
    public partial class SisProcessorServiceInstaller : Installer
    {
        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller serviceInstaller;

        public SisProcessorServiceInstaller()
        {
            InitializeComponent();
        }

        public override void Install(IDictionary stateSaver)
        {
            this.serviceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.serviceInstaller = new System.ServiceProcess.ServiceInstaller();

            // Key values for username and password.
            const string paramUsername = "USERNAME";
            const string paramPassword = "PASSWORD";

            // Make sure username and password parameters have been specified.
            if ((this.Context.Parameters.ContainsKey(paramUsername) == true) && (this.Context.Parameters.ContainsKey(paramPassword) == true))
            {
                string username = this.Context.Parameters[paramUsername];
                string password = this.Context.Parameters[paramPassword];

                // Make sure username is not null or empty - password will be empty if not specified as an argument.
                // Domain accounts cannot have empty passwords.
                if ((string.IsNullOrEmpty(username)) || (string.IsNullOrEmpty(password)))
                {
                    // Remove username and password parameters - this will enable the dialog box that prompts for service credentials.
                    this.Context.Parameters.Remove(paramUsername);
                    this.Context.Parameters.Remove(paramPassword);
                }
                else
                {
                    // Specify username and password for service credentials.
                    this.serviceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.User;
                    this.serviceProcessInstaller.Username = username;
                    this.serviceProcessInstaller.Password = password;
                }
            }

            base.Install(stateSaver);
        }
    }
}