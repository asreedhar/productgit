using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading;
using Firstlook.Sis.DomainModel;
using Firstlook.Sis.DomainModel.Facade;
using Firstlook.Sis.DomainModel.Serialization;
using Firstlook.Sis.SisProcessorService;

namespace Firstlook.Sis.SisProcessorService
{
    /// <summary>
    /// QueryProcessor class calls an SisFacade, executes a query that aligns to a QueryProcessorState, and if applicable writes the result using an IPsvWriter.
    /// </summary>
    public class QueryProcessor : IResultProcessor
    {
        /// <summary>
        /// QueryProcessorState to use to obtain queries.
        /// </summary>
        private readonly QueryProcessorState queryProcessorState;

        /// <summary>
        /// IPsvWriter used to write results.
        /// </summary>
        private readonly IPsvWriter writer;

        /// <summary>
        /// Tracking variable used to stop the QueryProcessor.
        /// </summary>
        private int stopCount = 0;




        /// <summary>
        /// Initializes a new instance of a QueryProcessor.
        /// </summary>
        /// <param name="queryProcessorState">The QueryProcessorState to use to obtain queries.</param>
        /// <param name="writer">IPsvWriter used to write results.</param>
        public QueryProcessor(QueryProcessorState queryProcessorState, IPsvWriter writer)
        {
            this.queryProcessorState = queryProcessorState;
            this.writer = writer;
        }




        /// <summary>
        /// Adjusts the tracking variable used to stop the QueryProcessor.
        /// </summary>
        public void Stop()
        {
            Interlocked.Increment(ref stopCount);
        }

        /// <summary>
        /// Restarts the tracking variable used to stop the QueryProcessor.
        /// </summary>
        public void Restart()
        {
            if (stopCount == 1)
            {
                Interlocked.Decrement(ref stopCount);
            }
        }

        /// <summary>
        /// Continually processes QueryProcessorCommands until the QueryProcessor's Stop method is invoked.
        /// </summary>
        public void Process()
        {
            //loop always runs until the Stop method is invoked and the stopCount variable in incremented.
            while (stopCount == 0)
            {
                //try catch the whole thing, no error should ever get out of here uncaught
                try
                {
                    //get a new QueryProcessorCommand from the database
                    QueryProcessorCommand command = QueryProcessorCommand.GetQueryProcessorCommand(queryProcessorState);

                    //catch errors after a query has been obtained so the
                    //query may be able to be retried
                    try
                    {
                        //if the database doesn't return an actual query, don't do anything.
                        if (command.HasQuery)
                        {
                            switch (queryProcessorState)
                            {
                                case QueryProcessorState.Submit:
                                    Submit(command);
                                    break;
                                case QueryProcessorState.Status:
                                    CheckStatus(command);
                                    break;
                                case QueryProcessorState.Response:
                                    GetResponse(command);
                                    break;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        //have the database handle any remaining uncaught exception
                        QueryProcessorCommand.ProcessException(e, command.Query);
                    }
                    
                    //sleep time between processing QueryProcessorCommands
                    Thread.Sleep(command.Delay * 1000);
                }
                catch(Exception e)
                {
                    //have the database handle any remaining uncaught exception
                    QueryProcessorCommand.ProcessException(e);

                    int delay = int.TryParse(ConfigurationManager.AppSettings["NoQueryDelay"], out delay) ? delay : 60;
                    Thread.Sleep(delay * 1000);
                }
            }
        }

        /// <summary>
        /// Submits the QueryProcessorCommand to the SisFacade.
        /// </summary>
        /// <param name="command">The QueryProcessorCommand to submit to the SisFacade.</param>
        private static void Submit(QueryProcessorCommand command)
        {
            command.Submitted(command.Query.Submit(command.Credential));
        }

        /// <summary>
        /// Checks the status of the QueryProcessorCommand with the SisFacade.
        /// </summary>
        /// <param name="command">The QueryProcessorCommand for which to check the status with the SisFacade.</param>
        private static void CheckStatus(QueryProcessorCommand command)
        {
            command.StatusChecked(command.Query.CheckStatus(command.Credential, command.Query.Receipt));
        }

        /// <summary>
        /// Gets the response of the QueryProcessorCommand with the SisFacade.
        /// </summary>
        /// <param name="command">The QueryProcessorCommand for which to get the response from the SisFacade.</param>
        private void GetResponse(QueryProcessorCommand command)
        {
            IPsvSerializable item = command.Query.GetResponse(command.Credential, command.Query.Receipt);
            Complete(writer, item, command);
            command.Completed();
        }

        /// <summary>
        /// Completes a query request by determining the appropriate action to take on the result.
        /// Accounts, Journals, and Schedules go straight to the database since it is an initial load for a newly setup dealer.
        /// All other results are written to PSV files.
        /// </summary>
        /// <param name="wrtr">The IPsvWriter to use to write the IPsvSerializable item.</param>
        /// <param name="item">The IPsvSerializable item to write.</param>
        /// <param name="command">The QueryProcessorCommand.</param>
        private void Complete(IPsvWriter wrtr, IPsvSerializable item, QueryProcessorCommand command)
        {
            //GetAccounts, GetJournals, and GetSchedules go straight to the database
            //since they are initial loads for newly setup dealers
            switch(command.Query.QueryType)
            {
                case QueryType.GetAccounts:
                    ProcessAccounts((AccountCollection)item, command.Query);
                    break;
                case QueryType.GetJournals:
                    ProcessJournals((JournalCollection)item, command.Query);
                    break;
                case QueryType.GetSchedules:
                    ProcessSchedules((ScheduleCollection)item, command.Query);
                    break;
                default:
                    ProcessResult(wrtr, command.Query, item);                    
                    break;
            }
        }



        #region IResultProcessor Members

        /// <summary>
        /// Writes the AccountCollection straight to the database since it is an initial load for a newly setup dealer.
        /// </summary>
        /// <param name="ac">The AccountCollection to save.</param>
        /// <param name="Query">The Query used to obtain the result.</param>
        public void ProcessAccounts(AccountCollection ac, Query Query)
        {
            //loop AccountCollection for all Accounts
            foreach (Account a in ac)
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
                {
                    //Open database connection
                    conn.Open();

                    using (SqlCommand comm = new SqlCommand("Runtime.Account#Load", conn))
                    {
                        comm.CommandType = CommandType.StoredProcedure;

                        comm.Parameters.Add(new SqlParameter("@QueryId", Query.ID));
                        comm.Parameters.Add(new SqlParameter("@AcctNumber", a.AccountNumber));
                        comm.Parameters.Add(new SqlParameter("@AcctDescription", a.Description));
                        comm.Parameters.Add(new SqlParameter("@AcctType", a.AccountType));
                        comm.Parameters.Add(new SqlParameter("@AcctSubType", a.SubType));
                        comm.Parameters.Add(new SqlParameter("@AcctControlType", a.ControlType));
                        comm.Parameters.Add(new SqlParameter("@AcctDepartment", a.Department));

                        comm.ExecuteNonQuery();
                    }
                }
            }
        }

        /// <summary>
        /// Writes the JournalCollection straight to the database since it is an initial load for a newly setup dealer.
        /// </summary>
        /// <param name="jc">The AccountCollection to save.</param>
        /// <param name="Query">The Query used to obtain the result.</param>
        public void ProcessJournals(JournalCollection jc, Query Query)
        {
            //loop JournalCollection for all Journals
            foreach (Journal j in jc)
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
                {
                    //Open database connection
                    conn.Open();

                    using (SqlCommand comm = new SqlCommand("Runtime.Journal#Load", conn))
                    {
                        comm.CommandType = CommandType.StoredProcedure;

                        comm.Parameters.Add(new SqlParameter("@QueryId", Query.ID));
                        comm.Parameters.Add(new SqlParameter("@JrnlNumber", j.JournalNumber));
                        comm.Parameters.Add(new SqlParameter("@JrnlDescription", j.Description));
                        comm.Parameters.Add(new SqlParameter("@JrnlType", j.JournalType));

                        comm.ExecuteNonQuery();
                    }
                }
            }
        }

        /// <summary>
        /// Writes the ScheduleCollection straight to the database since it is an initial load for a newly setup dealer.
        /// </summary>
        /// <param name="sc">The AccountCollection to save.</param>
        /// <param name="Query">The Query used to obtain the result.</param>
        public void ProcessSchedules(ScheduleCollection sc, Query Query)
        {
            //loop ScheduleCollection for all Schedules
            foreach (Schedule s in sc)
            {
                //loop (ScheduleCollection)item for all Schedules
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
                {
                    //Open database connection
                    conn.Open();

                    using (SqlCommand comm = new SqlCommand("Runtime.Schedule#Load", conn))
                    {
                        comm.CommandType = CommandType.StoredProcedure;

                        comm.Parameters.Add(new SqlParameter("@QueryId", Query.ID));
                        comm.Parameters.Add(new SqlParameter("@SchdNumber", s.ScheduleNumber));
                        comm.Parameters.Add(new SqlParameter("@SchdDescription", s.Description));
                        comm.Parameters.Add(new SqlParameter("@SchdType", s.ScheduleType));
                        comm.Parameters.Add(new SqlParameter("@SchdControlType", s.ControlType));
                        comm.Parameters.Add(new SqlParameter("@SchdControlType2", s.ControlType2));

                        comm.ExecuteNonQuery();
                    }
                }
            }
        }


        /// <summary>
        /// Writes the result of the Query with the IPsvWriter.
        /// </summary>
        /// <param name="wrtr">The IPsvWriter to use to write the IPsvSerializable item.</param>
        /// <param name="Query">The Query used to obtain the result.</param>
        /// <param name="item">The IPsvSerializable item to write.</param>
        public void ProcessResult(IPsvWriter wrtr, Query Query, IPsvSerializable item)
        {
            wrtr.Write(ConfigurationManager.AppSettings["SisResultDrivePath"] + "\\" + Query.QueryType.ToString() + "\\" + Query.ID + ".txt", item);
        }

        #endregion
    }
}