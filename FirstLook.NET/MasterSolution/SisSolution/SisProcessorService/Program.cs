using System.IO;
using System.ServiceProcess;

namespace Firstlook.Sis.SisProcessorService
{
    /// <summary>
    /// Executable which starts the SisProcessorService.
    /// </summary>
    internal static class Program
    {
        /// <summary>
        /// Static void Main method which starts the SisProcessorService.
        /// </summary>
        private static void Main()
        {
            ServiceBase[] ServicesToRun = new ServiceBase[] { new SisProcessorService() };
            ServiceBase.Run(ServicesToRun);
        }
    }
}