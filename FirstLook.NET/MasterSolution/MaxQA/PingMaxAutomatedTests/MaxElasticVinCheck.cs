﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Nest;
using PingMaxAutomatedTests.DAO;
using PingMaxAutomatedTests.Models;
using PingMaxAutomatedTests.Models.ES;

namespace PingMaxAutomatedTests
{
    public class MaxElasticVinCheck
    {
        private static string Result = "vin_results.txt";
        private Dictionary<int, ElasticSearchDataSet> _storeInformation;
        private string dbConnectionString;
        private ElasticClient esClient;

        public MaxElasticVinCheck(string dbConnectionString, string esUsername, string esPassword, Uri esClusterNode)
        {
            this.dbConnectionString = dbConnectionString;
            
            var settings = new ConnectionSettings(esClusterNode, defaultIndex: "current");
            var basicAuth = new BasicAuthHttpConnection(settings, esUsername, esPassword);
            esClient = new ElasticClient(settings, basicAuth);
        }
        /// <summary>
        /// Invoke all required SQL and ES connections to calculate distances, test results and write to result.txt
        /// </summary>
        public void CheckVins()
        {
            var maxVins = InventoryAccess.GetActiveVins(dbConnectionString);
            Console.WriteLine(@"Asking ES for our data. There are {0} VINs in this Search.", maxVins.Count);
            var result = esClient.Search<MarketData>(s => s
                .Index("current") // most current index always
                .Size(maxVins.Count) // get ALL documents for ALL vins
                .Fields(f => f.vin) // limit return value to what we care about, saving bandwidth
                .Filter(f => f
                    .Terms(t => t.vin, maxVins)) // filter SHOULD be faster than query
            );
            Console.WriteLine(@"ES responded.");
            var foundVins =
                result.Hits.Select(h => h.Fields.FieldValues<MarketData, string>(f => f.vin).First()).ToList();
            var missingVins = maxVins.Except(foundVins).ToList();
            Console.WriteLine(@"{0} vins were missing from ES out of the {1} found in MAX.", missingVins.Count(), maxVins.Count);
            Console.WriteLine(@"Writing results to disk.");
            using (var fstream = File.CreateText(Result))
            {
                foreach (var vin in missingVins)
                {
                    fstream.WriteLine(vin);
                }
            }
            Console.WriteLine(@"Press any key to continue...");
            Console.ReadKey(true);
        }
    }
}
