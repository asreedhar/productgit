﻿using System;
using System.Globalization;
using System.Text;
using Elasticsearch.Net.Connection;
using Elasticsearch.Net.Connection.Configuration;


namespace PingMaxAutomatedTests
{
    public sealed class BasicAuthHttpConnection : HttpConnection
    {
        private readonly string password;
        private readonly string userName;

        /// <summary>
        /// constructor with username + password for the basic auth
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        public BasicAuthHttpConnection(IConnectionConfigurationValues settings, string username, string password)
            : base(settings)
        {
            this.userName = username;
            this.password = password;
        }

        protected override System.Net.HttpWebRequest CreateWebRequest(Uri uri, string method, byte[] data,
            IRequestConfiguration requestSpecificConfig)
        {
            var request = base.CreateWebRequest(uri, method, data, requestSpecificConfig);
            request.Headers["Authorization"] = "Basic " +
                                               Convert.ToBase64String(
                                                   Encoding.UTF8.GetBytes(string.Format(CultureInfo.InvariantCulture,
                                                       "{0}:{1}", userName, password)));
            return request;
        }

        protected override System.Net.HttpWebRequest CreateHttpWebRequest(Uri uri, string method, byte[] data,
            IRequestConfiguration requestSpecificConfig)
        {
            var request = base.CreateHttpWebRequest(uri, method, data, requestSpecificConfig);
            request.Headers["Authorization"] = "Basic " +
                                               Convert.ToBase64String(
                                                   Encoding.UTF8.GetBytes(string.Format(CultureInfo.InvariantCulture,
                                                       "{0}:{1}", userName, password)));
            return request;
        }
    }
}
