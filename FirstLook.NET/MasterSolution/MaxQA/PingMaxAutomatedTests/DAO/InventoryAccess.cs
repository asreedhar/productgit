﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using PingMaxAutomatedTests.Models.DB;

namespace PingMaxAutomatedTests.DAO
{
    public class InventoryAccess
    {
        /// <summary>
        /// Get geo locations for provided BUIDs
        /// If no BUID collection is provided, this will return all known BUIDs with locations
        /// </summary>
        /// <param name="buidCollection">Defaults to empty</param>
        /// <returns></returns>
        public static ICollection<string> GetActiveVins(string dbConnectionString, ICollection<int> buidCollection = null)
        {
            var activeVins = new List<string>();
            var buids = new List<BusinessUnit>();
            string query;
            using (var dbConnection = new SqlConnection(dbConnectionString))
            {

                
                if (buidCollection == null)
                {
                    query = string.Format(@"SELECT Latitude, Longitude, BusinessUnitID, BusinessUnitCode FROM IMT.dbo.BusinessUnit WHERE Active = 1");
                }
                else
                {
                    query = string.Format(@"SELECT Latitude, Longitude, BusinessUnitID, BusinessUnitCode FROM IMT.dbo.BusinessUnit WHERE Active = 1 AND BusinessUnitID IN ({0})",
                        string.Join(",", buidCollection.ToArray()));
                }
                Console.WriteLine(@"Getting BUIDs from SQL.");
                buids = dbConnection.Query<BusinessUnit>(query).ToList();

                if (buids.Any())
                {
                    Console.WriteLine(@"Found {0} BUIDs.", buids.Count());
                }
                else
                {
                    Console.WriteLine(@"Nothing found for BUIDs.");
                    return null;
                }

                query = string.Format(@"SELECT InventoryID, VIN, BusinessUnitID FROM FLDW.dbo.InventoryActive WHERE BusinessUnitID IN ({0})",
                    string.Join(",", buids.Select(b => b.BusinessUnitID.ToString()).ToArray()));
                Console.WriteLine(@"Getting active VINs from SQL.");
                var inventory = dbConnection.Query<ActiveInventory>(query).ToList();
                if (inventory.Any())
                {
                    activeVins = inventory.Select(i => i.VIN).ToList();
                }
            }
            return activeVins;
        }
    }
}
