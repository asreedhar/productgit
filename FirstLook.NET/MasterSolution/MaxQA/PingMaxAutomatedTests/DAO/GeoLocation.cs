﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Device.Location;
using System.Linq;
using Dapper;
using PingMaxAutomatedTests.Models;
using PingMaxAutomatedTests.Models.DB;

namespace PingMaxAutomatedTests.DAO
{
    public class GeoLocation
    {
        /// <summary>
        /// Get geo locations for provided BUIDs
        /// If no BUID collection is provided, this will return all known BUIDs with locations
        /// </summary>
        /// <param name="buidCollection">Defaults to empty</param>
        /// <returns></returns>
        public static ICollection<BusinessUnit> GetBusinessLocations(string dbConnectionString, ICollection<int> buidCollection = null)
        {
            var buids = new List<BusinessUnit>();
            using (var dbConnection = new SqlConnection(dbConnectionString))
            {

                string query;
                if (buidCollection == null)
                {
                    query = string.Format(@"SELECT Latitude, Longitude, BusinessUnitID, BusinessUnitCode FROM IMT.dbo.BusinessUnit WHERE Active = 1 AND Latitude != 0 AND Longitude != 0");
                }
                else
                {
                    query = string.Format(@"SELECT Latitude, Longitude, BusinessUnitID, BusinessUnitCode FROM IMT.dbo.BusinessUnit WHERE Active = 1 AND Latitude != 0 AND Longitude != 0 AND BusinessUnitID IN ({0})",
                        string.Join(",", buidCollection.ToArray()));
                }
                Console.WriteLine(@"Getting BUIDs from SQL.");
                buids = dbConnection.Query<BusinessUnit>(query).ToList();

                if (buids.Any())
                {
                    Console.WriteLine(@"Found {0} BUIDs.", buids.Count());
                }
                else
                {
                    Console.WriteLine(@"Nothing found for BUIDs.");
                    return null;
                }
            }
            return buids;
        }

        /// <summary>
        /// Get lat,lon for BUIDs and active inventory for those BUIDs
        /// </summary>
        /// <param name="businessList"></param>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        public static Dictionary<int, ElasticSearchDataSet> GetMaxDataSets(string dbConnectionString)
        {
            var buidToVinDict = new Dictionary<int, ElasticSearchDataSet>();

            List<BusinessUnit> buids;
            using (var dbConnection = new SqlConnection(dbConnectionString))
            {

                var query = string.Format(@"SELECT Latitude, Longitude, BusinessUnitID, BusinessUnitCode FROM IMT.dbo.BusinessUnit WHERE Active = 1 AND Latitude != 0 AND Longitude != 0");
                buids = dbConnection.Query<BusinessUnit>(query).ToList();

                if (buids != null && buids.Any())
                {
                    Console.WriteLine(@"Found {0} BUIDs.", buids.Count());
                }
                else
                {
                    Console.WriteLine(@"Nothing found for BUIDs.");
                    return null;
                }
            }

            using (var dbConnection = new SqlConnection(dbConnectionString)) // TODO: use only one dbConnectionString once Lat/Lon are populated in proper databases
            {
                var query = string.Format(@"SELECT InventoryID, VIN, BusinessUnitID FROM FLDW.dbo.InventoryActive WHERE BusinessUnitID IN ({0})",
                    string.Join(",", buids.Select(b => b.BusinessUnitID.ToString()).ToArray()));
                Console.WriteLine(@"Getting active VINs from SQL.");
                var inventory = dbConnection.Query<ActiveInventory>(query).ToList();

                if (inventory.Any())
                {
                    Console.WriteLine(@"Found {0} inventory.", inventory.Count());
                    // warm the keys
                    //
                    for (var i = 0; i < buids.Count; i++)
                    {
                        buidToVinDict[buids[i].BusinessUnitID] = new ElasticSearchDataSet
                        {
                            BusinessUnitCode = buids[i].BusinessUnitCode,
                            MaxGeoCoordinate = new GeoCoordinate(buids[i].Latitude, buids[i].Longitude)
                        };
                    }
                    for (var i = 0; i < inventory.Count; i++)
                    {
                        var inv = inventory[i];
                        var key = inv.BusinessUnitID;
                        if (buidToVinDict[key].VinGeoCoordinates.ContainsKey(inv.VIN))
                        {
                            Console.WriteLine(@"VIN {0} already exists in BUID {1}'s inventory.", inv.VIN,
                                inv.BusinessUnitID);
                        }
                        else
                        {
                            buidToVinDict[key].VinGeoCoordinates.Add(inv.VIN, null);
                        }

                    }
                }
                else
                {
                    throw new Exception("No inventory found.");
                }
            }
            return buidToVinDict;
        }
    }
}
