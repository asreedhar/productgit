﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.IO;
using System.Linq;
using Nest;
using Newtonsoft.Json;
using PingMaxAutomatedTests.DAO;
using PingMaxAutomatedTests.Models;
using PingMaxAutomatedTests.Models.ES;

namespace PingMaxAutomatedTests
{
    /// <summary>
    /// Gets Lat/Lon for all active businesses,
    /// Gets all active inventory for said businesses
    /// Queries ElasticSearch for known active VINs
    /// Compares ElasticSearch found VINs with Lat/Lon to SQL's Lat/Lon
    /// </summary>
    public class MaxElasticDistanceCheck
    {
        private static string Result = "location_results.txt";
        private Dictionary<int, ElasticSearchDataSet> _storeInformation;
        private string dbConnectionString;
        private ElasticClient esClient;

        /// <summary>
        /// Only populated after CheckDistances is invoked
        /// </summary>
        public Dictionary<int, ElasticSearchDataSet> BuidDistanceInformation { get { return _storeInformation; } }
        
        public MaxElasticDistanceCheck(string dbConnectionString, string esUsername, string esPassword, Uri esClusterNode)
        {
            this.dbConnectionString = dbConnectionString;
            
            var settings = new ConnectionSettings(esClusterNode, defaultIndex: "current");
            var basicAuth = new BasicAuthHttpConnection(settings, esUsername, esPassword);
            esClient = new ElasticClient(settings, basicAuth);
        }

        /// <summary>
        /// Invoke all required SQL and ES connections to calculate distances, test results and write to result.txt
        /// </summary>
        public void CheckDistances()
        {
            _storeInformation = GeoLocation.GetMaxDataSets(dbConnectionString);
            var vinSet = _storeInformation.SelectMany(e=>e.Value.VinGeoCoordinates.Keys).Distinct().ToList();
            Console.WriteLine(@"Asking ES for our data. There are {0} VINs in this Search.", vinSet.Count());
            var result = esClient.Search<MarketData>(s => s
                .Index("current") // most current index always
                .Size(vinSet.Count()) // get ALL documents for ALL vins
                .Fields(f=>f.vin, f=>f.advertisement.advertiser.address.location) // limit return value to what we care about, saving bandwidth
                .Filter(f => f
                    .Terms(t => t.vin, vinSet)) // filter SHOULD be faster than query
            );
            Console.WriteLine(@"ES responded.");
            Console.WriteLine(@"Calculating distance differences.");
            CalculateDistanceDifferences(result);

            double locationProblemCount = 0;
            double locationCount = 0;

            Console.WriteLine(@"Counting problems.");
            foreach (var distanceInfo in BuidDistanceInformation.Values)
            {
                foreach (var inventory in distanceInfo.VinGeoCoordinates)
                {
                    if (inventory.Value == null) continue; // no location found

                    if (distanceInfo.DistanceTo(inventory.Value) > 1.0)
                    {
                        locationProblemCount++;
                    }
                }
                locationCount += distanceInfo.VinsWithCoordinates.Count;

            }

            var orderedDistances = BuidDistanceInformation.OrderByDescending(o => o.Value.AvgDistance);
            Console.WriteLine(@"Writing results to disk.");
            using (var fstream = File.CreateText(Result))
            {

                string output = JsonConvert.SerializeObject(orderedDistances, Formatting.Indented);

                fstream.Write(output);
                fstream.Flush();
            }
            Console.WriteLine("\rDone.");
            Console.WriteLine(@"Found {0} problem VINs out of {1} VINs with coordinates.", locationProblemCount, locationCount);
            Console.WriteLine(@"Press any key to continue...");
            Console.ReadKey(true);
        }

        private void CalculateDistanceDifferences(ISearchResponse<MarketData> result)
        {
            // We are using result.Hits since result.Documents is not populated due to the Field filter provided
            // and a change in ES 1.0 which returns partial fields as arrays
            
            foreach (var hit in result.Hits)
            {
                // get original dealership geo information from ES vin
                //
                var vin = hit.Fields.FieldValues<MarketData, string>(f => f.vin).First();

                var location = hit.Fields.FieldValues<MarketData, string>(f => f.advertisement.advertiser.address.location).First();

                // VIN to Business
                var storeKvp = _storeInformation.First(i => i.Value.VinGeoCoordinates.Keys.Contains(vin));

                // string to geocoordinate
                //
                var latLonString = location.Split(',');
                var lat = Convert.ToDouble(latLonString[0]);
                var lon = Convert.ToDouble(latLonString[1]);
                var esGeoCoord = new GeoCoordinate(lat, lon);

                _storeInformation[storeKvp.Key].VinGeoCoordinates[vin] = esGeoCoord;
            }
        }

        
    }
}
