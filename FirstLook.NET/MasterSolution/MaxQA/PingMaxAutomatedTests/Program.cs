﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Device.Location;
using System.IO;
using System.Linq;
using Nest;
using Dapper;
using Newtonsoft.Json;
using PingMaxAutomatedTests.Models;
using PingMaxAutomatedTests.Models.DB;

namespace PingMaxAutomatedTests
{

    /// <summary>
    /// C:\Users\gsmith>curl -XGET https://searchonly:pass@e32a6e80154e2d489555db0ca3cb2987-us-east-1.foundcluster.com:9243/2015-04-24/_mapping?pretty=1
    /// str ad_text
    /// obj advertisement
    ///     obj advertiser
    ///         obj address
    ///             string location
    /// str equipment
    /// obj product_details ** has YMM
    ///     obj properties
    ///         str year
    ///         str make
    ///         str model
    ///         str style
    ///         str transmission
    /// obj time_line
    /// str vin
    /// </summary>
    
    class Program
    {
        static string betaConnection =
                "Data Source=betadb01sql;Initial Catalog=VehicleCatalog;Persist Security Info=True;User ID=firstlook;Password=look@tm31st";
        static string intConnection =
            "Data Source=intdb01;Initial Catalog=VehicleCatalog;Persist Security Info=True;Type System Version=SQL Server 2005;Application Name=PingMaxAutomation;User ID=firstlook;Password=7ruHuquh";

        private static string dbConnection = betaConnection;

        private static Uri esNode = new Uri("https://e32a6e80154e2d489555db0ca3cb2987-us-east-1.foundcluster.com:9243/");
        const string esUsername = "searchonly";
        const string esPassword = "75rm77k41f1jq36if7";

        private static List<string> quitTerms = new List<string> {"no", "q", "quit", "exit"};

        private class ExecSet
        {
            public string name;
            public BasicExecution execution;
        }
        private delegate void BasicExecution();
        private static List<ExecSet> executions = new List<ExecSet>();

        static void Main(string[] args)
        {
            executions.Add(new ExecSet { name = "Location check", execution = LatLonExecute });
            executions.Add(new ExecSet { name = "Vin check", execution = VinCheckExecute });
            string line;
            int which = -1;
            do
            {
                Console.WriteLine("Pick an action to perform. (Q to quit)");
                for (var i = 0; i < executions.Count; i++)
                {
                    Console.WriteLine("#{0}: {1}", i, executions[i].name);
                }

                line = Console.ReadLine().Trim().ToLower();

                if (quitTerms.Any(q => q.Equals(line)))
                {
                    Console.WriteLine("User is afraid. Quitting.");
                    Console.ReadKey(true);
                    return;
                }

                try
                {
                    which = Convert.ToInt32(line);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("That isn't a number.");
                }
            } while (which < 0);
            var pick = executions[which];
            Console.WriteLine(@"Executing: {0}.", pick.name);
            pick.execution();
        }

        private static void LatLonExecute()
        {
            string line;
            do
            {
                Console.WriteLine(@"This queries all MAX active inventory against Elasticsearch. Are you SURE you want to run this? [yes/no]:");
                line = Console.ReadLine().Trim().ToLower();

                if (quitTerms.Any(q => q.Equals(line)))
                {
                    Console.WriteLine("User is afraid. Quitting.");
                    Console.ReadKey(true);
                    return;
                }
            } while (!"yes".Equals(line));

            var distanceTester = new MaxElasticDistanceCheck(dbConnection, esUsername, esPassword, esNode);

            distanceTester.CheckDistances(); // does all SQL and ES connections
        }

        private static void VinCheckExecute()
        {
            var vinTester = new MaxElasticVinCheck(dbConnection, esUsername, esPassword, esNode);
            vinTester.CheckVins();
        }
    }
}
