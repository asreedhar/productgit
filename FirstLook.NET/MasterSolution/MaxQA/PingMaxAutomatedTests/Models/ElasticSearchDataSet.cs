﻿
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using Newtonsoft.Json;

namespace PingMaxAutomatedTests.Models
{
    /// <summary>
    /// Distances in miles
    /// </summary>
    public class ElasticSearchDataSet
    {
        public string BusinessUnitCode;

        public int SameCoordinateVinCount
        {
            get { return VinsWithCoordinates.Values.Count(c => c.Equals(MaxGeoCoordinate)); }
        }
        public int NoCoordinateVinCount
        {
            get { return VinGeoCoordinates.Count - VinsWithCoordinates.Count; }
        }

        public int TotalVinCount
        {
            get { return VinGeoCoordinates.Count; }
        }

        private double? _minDistance;
        public double? MinDistance
        {
            get
            {
                if (_minDistance != null) return _minDistance;
                foreach (var inv in VinsWithCoordinates.Values)
                {
                    var dist = DistanceTo(inv);
                    if (dist < _minDistance || _minDistance == null)
                    {
                        _minDistance = dist;
                    }
                }
                return _minDistance;
            }
        }
        private double? _maxDistance;
        public double? MaxDistance
        {
            get
            {
                if (_maxDistance != null) return _maxDistance;
                foreach (var inv in VinsWithCoordinates.Values)
                {
                    var dist = DistanceTo(inv);
                    if (dist > _maxDistance || _maxDistance == null)
                    {
                        _maxDistance = dist;
                    }
                }
                return _maxDistance;
            }
        }

        private double? _avgDistance;
        public double? AvgDistance
        {
            get
            {
                if (_avgDistance != null) return _avgDistance;
                _avgDistance = 0;
                foreach (var inv in VinsWithCoordinates.Values)
                {
                    var dist = DistanceTo(inv);
                    _avgDistance += dist;
                }
                _avgDistance /= VinsWithCoordinates.Count;
                return _avgDistance;
            }
        }

        [JsonIgnore]
        public GeoCoordinate MaxGeoCoordinate { get; set; }

        public double Latitude { get { return MaxGeoCoordinate.Latitude; } }
        public double Longitude { get { return MaxGeoCoordinate.Longitude; } }

        [JsonIgnore]
        public Dictionary<string,GeoCoordinate> VinGeoCoordinates = new Dictionary<string, GeoCoordinate>();

        [JsonIgnore]
        public Dictionary<string,GeoCoordinate> VinsWithCoordinates
        {
            get { return VinGeoCoordinates.Where(i=>i.Value != null).ToDictionary(kv=>kv.Key, kv=>kv.Value); }
        }

        private List<VinDistance> _vinDistanceFromDealership;

        public List<VinDistance> VinDistanceFromDealership
        {
            get
            {
                if (_vinDistanceFromDealership != null) return _vinDistanceFromDealership;
                _vinDistanceFromDealership = new List<VinDistance>();
                foreach (var kvp in VinsWithCoordinates)
                {
                    var dist = Math.Round(DistanceTo(kvp.Value), 2);
                    if (dist > 1)
                        _vinDistanceFromDealership.Add(new VinDistance{ VIN = kvp.Key, Distance = dist});
                }
                _vinDistanceFromDealership = _vinDistanceFromDealership.OrderByDescending(t => t.Distance).ToList();
                return _vinDistanceFromDealership;
            }
        }
        public double DistanceTo(GeoCoordinate coord)
        {
            return MaxGeoCoordinate.GetDistanceTo(coord) * 0.000621371; // all in miles
        }
    }
}
