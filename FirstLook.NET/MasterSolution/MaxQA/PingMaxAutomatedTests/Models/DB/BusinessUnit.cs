﻿namespace PingMaxAutomatedTests.Models.DB
{
    public class BusinessUnit
    {
        public int BusinessUnitID { get; set; }
        public string BusinessUnitCode { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
