﻿using Nest;

namespace PingMaxAutomatedTests.Models.ES
{
    [ElasticType(Name = "market_data")]
    public class MarketData
    {
        public Advertisement advertisement;
        public ProductDetails product_details;
        public string vin;
    }
}
