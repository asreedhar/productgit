﻿namespace PingMaxAutomatedTests.Models.ES
{
    public class ProductDetails
    {
        public int year;
        public string make;
        public string model;
        public string style;
        public string transmission;
    }
}
