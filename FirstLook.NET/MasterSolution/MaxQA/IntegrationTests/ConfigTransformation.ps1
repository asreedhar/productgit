
param(
    [string] $artifactsDir,
    [string] $configuration
)

$env = @{ 
    alpha= @{
        ConnectionStrings= @{
            chrome="Data Source=intdb01;Initial Catalog=VehicleCatalog;Persist Security Info=True;Application Name=Merchandising;User ID=merchandisingWebsite;Password=cA4HeKe9RE";
            IMT="Data Source=intdb01;Initial Catalog=IMT;Persist Security Info=True;Application Name=Merchandising;User ID=merchandisingWebsite;Password=cA4HeKe9RE";
            merchandising="Data Source=intdb01;Initial Catalog=Merchandising;Persist Security Info=True;Type System Version=SQL Server 2005;Application Name=Merchandising;User ID=merchandisingWebsite;Password=cA4HeKe9RE";
        };
        AppSettings = @{
            MaxWebsiteBaseUrl = "https://max-b.int.firstlook.biz";
            MaxServicesBaseUrl = "http://2K3BAPP-SVCS01X.int.firstlook.biz";
			MaxStandAloneBaseUrl = "http://google.com/fix-me";
			Environment = "INT-B";
        };
    };
    
    staging= @{
         ConnectionStrings= @{
            chrome="Data Source=BETADB01SQL;Initial Catalog=VehicleCatalog;Persist Security Info=True;User ID=merchandisingWebsite;Password=JBd!95Zk(8x+";
            IMT="Data Source=BETADB01SQL;Initial Catalog=IMT;Persist Security Info=True;Application Name=Merchandising;User ID=firstlook;Password=look@tm31st";
            merchandising="Data Source=BETADB01SQL;Initial Catalog=Merchandising;Persist Security Info=True;Type System Version=SQL Server 2005;Application Name=Merchandising;User ID=merchandisingWebsite;Password=JBd!95Zk(8x+";
        };
        AppSettings = @{
            MaxWebsiteBaseUrl = "https://betamax.firstlook.biz";
            MaxServicesBaseUrl = "http://betaservices.firstlook.biz";
			MaxStandAloneBaseUrl = "https://betamaxad.firstlook.biz";
			Environment = "Beta";
        };
    };
		
}

function ReplaceConfigurationSettingsInFile($configFileName)
{
    Write-Host (split-path -Leaf $configFileName) + ' replace start..'
    $doc = New-Object Xml
    $doc.Load($configFileName)
    $config = $doc.configuration
    
    $config.connectionStrings.add | where { $_.name -eq "chrome" } | foreach { $_.SetAttribute("connectionString", $env.$configuration.ConnectionStrings.chrome) }
    $config.connectionStrings.add | where { $_.name -eq "IMT" } | foreach { $_.SetAttribute("connectionString", $env.$configuration.ConnectionStrings.IMT) }
    $config.connectionStrings.add | where { $_.name -eq "merchandising" } | foreach { $_.SetAttribute("connectionString", $env.$configuration.ConnectionStrings.merchandising) }
    
    $config.appSettings.add | where { $_.key -eq "MaxWebsiteBaseUrl" } | foreach { $_.SetAttribute("value", $env.$configuration.AppSettings.MaxWebsiteBaseUrl) }
    $config.appSettings.add | where { $_.key -eq "MaxServicesBaseUrl" } | foreach { $_.SetAttribute("value", $env.$configuration.AppSettings.MaxServicesBaseUrl) }
    $config.appSettings.add | where { $_.key -eq "MaxStandAloneBaseUrl" } | foreach { $_.SetAttribute("value", $env.$configuration.AppSettings.MaxStandAloneBaseUrl) }
	$config.appSettings.add | where { $_.key -eq "Environment" } | foreach { $_.SetAttribute("value", $env.$configuration.AppSettings.Environment) }
    
    $doc.Save($configFileName)
    
    Write-Host (split-path -Leaf $configFileName) + ' replace complete..'
}


try
{
    $configuration = $configuration.ToLower()
    Write-Host 'ArtifactsDir: ' $artifactsDir
    Write-Host 'Configuration: ' $configuration 
    if($configuration -eq "alpha" -or $configuration -eq "staging" -or $configuration -eq "prod2")
    {
        # Process files
        Write-Host "Processing files ..."
    
        foreach($file in (dir (Join-Path $artifactsDir "*.dll.config")))
        {
            Write-Host ''
            ReplaceConfigurationSettingsInFile  $file 
        }
    }
    else
    {
        throw 'Unsupported configuration. Supported configurations are `Alpha` and `Staging` and `Prod2`'  
    }
}
catch
{
    $_ | Out-Host
    exit 1
}
