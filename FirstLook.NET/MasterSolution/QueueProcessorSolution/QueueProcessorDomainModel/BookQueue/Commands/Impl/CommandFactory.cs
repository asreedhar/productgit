﻿using FirstLook.Common.Core.Command;

namespace FirstLook.QueueProcessor.DomainModel.BookQueue.Commands.Impl
{
    public class CommandFactory:ICommandFactory
    {
        #region ICommandFactory Members

        public ICommand<Model.BookRequest, int> CreateQueueCommand()
        {
           return new QueueCommand();
        }

        #endregion
    }
}
