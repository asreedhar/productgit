﻿using FirstLook.Common.Core.Registry;
using FirstLook.QueueProcessor.DomainModel.BookQueue.Commands.Impl;

namespace FirstLook.QueueProcessor.DomainModel.BookQueue.Commands
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory, CommandFactory>(ImplementationScope.Shared);
        }
    }
}
