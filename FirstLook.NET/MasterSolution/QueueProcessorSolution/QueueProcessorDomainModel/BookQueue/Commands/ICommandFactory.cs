﻿using FirstLook.Common.Core.Command;
using FirstLook.QueueProcessor.DomainModel.BookQueue.Model;

namespace FirstLook.QueueProcessor.DomainModel.BookQueue.Commands
{
    public interface ICommandFactory
    {
        ICommand<BookRequest, int> CreateQueueCommand();
    }
}
