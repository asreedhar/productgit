using FirstLook.QueueProcessor.DomainModel.BookQueue.Model;

namespace FirstLook.QueueProcessor.DomainModel.BookQueue
{
    /// <summary>
    /// Interface that defines a BookRequest queue.
    /// </summary>
    public interface IQueue
    {
        /// <summary>
        /// Push a BookRequest onto the queue. Must be thread safe.
        /// </summary>
        /// <param name="BookRequest">BookRequest to push onto the queue.</param>
        void Push(BookRequest BookRequest);

        /// <summary>
        /// Pop a BookRequest off the queue. Must be thread safe.
        /// </summary>
        /// <returns>BookRequest popped off the queue.</returns>
        BookRequest Pop();

        /// <summary>
        /// Get the number of entries on the queue. Must be thread safe.
        /// </summary>
        /// <returns>Queue size.</returns>
        int Count();
    }
}
