﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.QueueProcessor.DomainModel.BookQueue
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IQueue,MemoryQueue>(ImplementationScope.Shared);

            registry.Register<Commands.Module>();
        }
    }
}
