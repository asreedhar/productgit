using System.Collections.Generic;
using FirstLook.QueueProcessor.DomainModel.BookQueue.Model;


namespace FirstLook.QueueProcessor.DomainModel.BookQueue
{
    /// <summary>
    /// In-memory BookRequest queue.
    /// </summary>
    public class MemoryQueue : IQueue
    {
        /// <summary>
        /// BookRequest queue.
        /// </summary>
        static private readonly Queue<BookRequest> _queue = new Queue<BookRequest>();

        /// <summary>
        /// Push a BookRequest onto the queue. Thread safe.
        /// </summary>
        /// <param name="BookRequest">BookRequest to push onto the queue.</param>
        public void Push(BookRequest request)
        {
            lock (_queue)
            {
                _queue.Enqueue(request);
            }
        }

        /// <summary>
        /// Pop a BookRequest off the queue. Thread safe.
        /// </summary>
        /// <returns>BookRequest popped off the queue. Null if the queue is empty.</returns>
        public BookRequest Pop()
        {
            BookRequest request = null;
            lock (_queue)
            {
                if (_queue.Count > 0)
                {
                    request = _queue.Dequeue();
                }
            }
            return request;
        }

        /// <summary>
        /// Get the number of entries on the queue. Thread safe.
        /// </summary>
        /// <returns>Queue size.</returns>
        public int Count()
        {
            int count;

            lock (_queue)
            {
                count = _queue.Count;
            }
            return count;
        }
    }
}
