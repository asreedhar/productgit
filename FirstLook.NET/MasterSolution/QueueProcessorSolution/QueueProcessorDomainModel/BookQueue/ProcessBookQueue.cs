﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using FirstLook.Common.Core.Registry;
using FirstLook.QueueProcessor.DomainModel.BookQueue.Model;

namespace FirstLook.QueueProcessor.DomainModel.BookQueue
{
    public abstract class ProcessBookQueue
    {
        public IQueue RequestQueue { get; internal set; }
        public List<QueueResult> ServiceResult { get; internal set; }
        private ManualResetEvent Event;
        public int RecordCount { get;  internal set; }

        /// <summary>
        /// Push Request data in Queue
        /// </summary>
        /// <param name="requests">List of BookRequest</param>
        public void LoadTableQueueInMemoryQueue(IList<BookRequest> requests)
        {
            RecordCount = requests.Count;
            IResolver resolver = RegistryFactory.GetResolver();
            RequestQueue = resolver.Resolve<IQueue>();

            foreach (BookRequest bookRequest in requests)
            {
                RequestQueue.Push(bookRequest);
            }
        }

        /// <summary>
        /// Get Web service result for all Queue Items. It will call the web service from Thread. 
        /// This method will process queue records one by one
        /// </summary>
        public void GetServiceResult()
        {
            ServiceResult = new List<QueueResult>();
            Event = new ManualResetEvent(false);
           
            while (RequestQueue.Count() > 0)
            {
                BookRequest request = RequestQueue.Pop();
                ThreadPool.QueueUserWorkItem(new WaitCallback(UpdateResultList), request);
            }

            Event.WaitOne();
        }

        /// <summary>
        /// This method will send Multiple records from queue to web service.
        /// </summary>
        /// <param name="multiPriceSupportedRecords">Number(Count) of records to submit to Web service</param>
        public void GetBulkServiceResult(int multiPriceSupportedRecords)
        {
            ServiceResult = new List<QueueResult>();
            Event = new ManualResetEvent(false);
            bool isRecordExits = RequestQueue.Count() > 0;

            while (RequestQueue.Count() > 0)
            {
                List<BookRequest> requests = new List<BookRequest>();

                while (RequestQueue.Count() > 0 && requests.Count() < multiPriceSupportedRecords)
                {
                    requests.Add(RequestQueue.Pop());
                }

                ThreadPool.QueueUserWorkItem(new WaitCallback(UpdateBulkResultList), requests);    
            }
            if (isRecordExits)
                Event.WaitOne();
        }

        /// <summary>
        /// Abstract method to be implemented by Derived class.
        /// This method should implement call to web service and return result.
        /// </summary>
        /// <param name="request">Request object to submit to web service</param>
        /// <returns>Result from web service for given request</returns>
        public abstract QueueResult GetWebServiceResult(BookRequest request);

        /// <summary>
        /// Abstract method to be implemented by Derived class.
        /// This method should implement call to web service bulk method and return list of result.
        /// </summary>
        /// <param name="request">List of Request to submit to Bulk web service</param>
        /// <returns>List of Result</returns>
        public abstract List<QueueResult> GetWebServiceResult(List<BookRequest> request);

        /// <summary>
        /// Method to update result list. This method is called from Thread.
        /// Once all results are fetched it will indicate caller.
        /// </summary>
        /// <param name="request">Request object to submit to web service</param>
        private void UpdateResultList(object request)
        {
            try
            {
                QueueResult result = GetWebServiceResult((BookRequest) request);
                ServiceResult.Add(result);
                if (ServiceResult.Count == RecordCount)
                {
                    Event.Set();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Method to update result list with bulk data. This method is called from Thread.
        /// Once all results are fetched it will indicate caller.
        /// </summary>
        /// <param name="request"></param>
        private void UpdateBulkResultList(object request)
        {
            try
            {
                List<BookRequest> requests = (List<BookRequest>) request;
                List<QueueResult> results = GetWebServiceResult(requests);
                ServiceResult.AddRange(results);
                if (ServiceResult.Count == RecordCount)
                {
                    Event.Set();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
       
    }
}
