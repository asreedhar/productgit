﻿using System;

namespace FirstLook.QueueProcessor.DomainModel.BookQueue.Model
{
    [Serializable]
    public class BookRequest
    {
        public int QueueId { get; set; }
        public string VIN { get; set; }
        public string Trim { get; set; }
        public string Color { get; set; }
        public int Mileage { get; set; }
    }
}
