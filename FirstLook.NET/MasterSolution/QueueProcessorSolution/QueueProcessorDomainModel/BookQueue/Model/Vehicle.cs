﻿
namespace FirstLook.QueueProcessor.DomainModel.BookQueue.Model
{
    public class Vehicle
    {
        public string VIN { get; set; }
        public int Mileage { get; set; }
        public int OriginalYear { get; set; }
    }
}
