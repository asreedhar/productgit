﻿
namespace FirstLook.QueueProcessor.DomainModel.BookQueue.Model
{
    public enum QueueStatus
    {
        //Queue is waiting to be called from application
        Awaiting=1,

        //Record is under processing
        Processing=2,

        //Records processed successfully
        Success=3,

        //Error while updating Record
        Error=4,

        MMRError=5
    }
}
