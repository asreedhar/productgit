﻿using System;

namespace FirstLook.QueueProcessor.DomainModel.BookQueue.Model
{
    [Serializable]
    public class QueueResult
    {
        public int QueueId { get; set; }
        public QueueStatus Status { get; set; }
        public string ErrorMessage { get; set; }
    }
}
