﻿
namespace FirstLook.QueueProcessor.DomainModel.BookQueue.Model
{
    public class InventoryQueue:QueueData
    {
        public int InventoryId { get; set; }
        public Vehicle Vehicle { get; set;  }
    }
}
