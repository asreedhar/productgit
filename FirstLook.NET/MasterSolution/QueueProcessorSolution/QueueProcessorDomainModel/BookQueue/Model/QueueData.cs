﻿using System;

namespace FirstLook.QueueProcessor.DomainModel.BookQueue.Model
{
    public class QueueData
    {
        public int RowId { get; set; }
        public int QueueID { get; set; }
        public int StatusID { get; set; }
        public int Attempts { get; set; }
        public string ErrorMessage { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }

    }
}
