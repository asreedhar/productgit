﻿using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.QueueProcessor.DomainModel.BookQueue.Model;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Commands.TransferObjects;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Commands
{
    public interface ICommandFactory
    {
        ICommand<IList<MMRInventoryRequest>, MMRLoadQueueRequest> CreateMenheimLoadQueueCommand();

        ICommand<IList<QueueResult>> CreateMenheimUpdateQueueCommand();

        ICommand<PurgeQueueDto> CreateMenheimPurgeQueueCommand();

        ICommand<LoadQueueDto> CreateMenheimPushInventoryQueueCommand();

        ICommand<IList<MMRResult>> CreateMenheimUpdateInventoryQueueCommand();

        ICommand<MMRInventoryRequest, int> CreateMenheimInventoryDetailsFetchCommand();
    }
}
