﻿using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Repository;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Commands.Impl
{
    public class MenheimUpdateInventoryQueueCommand : ICommand<IList<MMRResult>>
    {
        #region ICommand<List<MMRResult>> Members

        public void Execute(IList<MMRResult> parameter)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            resolver.Resolve<IManheimInventoryRepository>().UpdateMMRAveragePrice(parameter);
        }

        #endregion
    }
}
