﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Repository;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Commands.Impl
{
    public class ManheimLoadQueueCommand : ICommand<IList<MMRInventoryRequest>, MMRLoadQueueRequest>
    {

        #region ICommand<IList<MMRInventoryRequest>,int> Members

        public IList<MMRInventoryRequest> Execute(MMRLoadQueueRequest request)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            return resolver.Resolve<IManheimQueueRepository>().LoadBookRequestQueueList(request.RecordCount, request.MaxAttempts);
        }

        public IList<string> GetRegions()
        {
            IResolver resolver = RegistryFactory.GetResolver();

            return resolver.Resolve<IManheimQueueRepository>().FetchQueueRegions();
        }

        public List<MMRErrorTypes> FetchMMRErrorTypes()
        {
            IResolver resolver = RegistryFactory.GetResolver();

            return resolver.Resolve<IManheimQueueRepository>().FetchMMRErrorTypes().ToList();
        }
        #endregion
    }
}
