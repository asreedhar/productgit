﻿using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Commands.Impl
{
    /// <summary>
    /// Class to Initilise Command objetcs
    /// </summary>
    public class CommandFactory : ICommandFactory
    {
        #region ICommandFactory Members

        public ICommand<IList<MMRInventoryRequest>, MMRLoadQueueRequest> CreateMenheimLoadQueueCommand()
        {
            return new ManheimLoadQueueCommand();
        }

        public ICommand<IList<BookQueue.Model.QueueResult>> CreateMenheimUpdateQueueCommand()
        {
            return new MenheimUpdateQueueCommand();
        }

        public ICommand<TransferObjects.PurgeQueueDto> CreateMenheimPurgeQueueCommand()
        {
            return new MenheimPurgeQueueCommand();
        }

        public ICommand<TransferObjects.LoadQueueDto> CreateMenheimPushInventoryQueueCommand()
        {
            return new ManheimPushInventoryQueueCommand();
        }

        public ICommand<IList<MMRResult>> CreateMenheimUpdateInventoryQueueCommand()
        {
            return new MenheimUpdateInventoryQueueCommand();
        }

        public ICommand<MMRInventoryRequest, int> CreateMenheimInventoryDetailsFetchCommand()
        {
            return new ManheimInventoryDetailsFetchCommand();
        }
        
        #endregion
    }
}
