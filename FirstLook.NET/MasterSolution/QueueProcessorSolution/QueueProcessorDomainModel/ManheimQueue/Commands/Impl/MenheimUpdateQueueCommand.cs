﻿using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.QueueProcessor.DomainModel.BookQueue.Model;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Repository;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Commands.Impl
{
    public class MenheimUpdateQueueCommand:ICommand<IList<QueueResult>>
    {
        #region ICommand<IList<QueueResult>> Members

        public void Execute(IList<QueueResult> parameter)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            resolver.Resolve<IManheimQueueRepository>().Update(parameter);
        }

        #endregion
    }
}
