﻿using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Repository;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Commands.Impl
{
    public class ManheimInventoryDetailsFetchCommand : ICommand<MMRInventoryRequest, int>
    {

        #region ICommand<int,MMRInventoryRequest> Members

        public MMRInventoryRequest Execute(int parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            return resolver.Resolve<IManheimInventoryRepository>().GetMmrInventoryRequest(parameters);
        }

        #endregion
    }
}
