﻿using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Commands.TransferObjects;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Repository;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Commands.Impl
{
    public class MenheimPurgeQueueCommand : ICommand<PurgeQueueDto>
    {
        #region ICommand<PurgeQueueDto> Members

        public void Execute(PurgeQueueDto parameter)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            resolver.Resolve<IManheimQueueRepository>().PurgeQueue(parameter.Days,parameter.Status);
        }

        #endregion
    }
}
