﻿using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Commands.TransferObjects;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Repository;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Commands.Impl
{
    public class ManheimPushInventoryQueueCommand : ICommand<LoadQueueDto>
    {
        #region ICommand<LoadQueueDto> Members

        public void Execute(LoadQueueDto parameter)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            resolver.Resolve<IManheimInventoryRepository>().PushPeriodicInventoryDataToQueue(parameter.Days);
        }

        #endregion
    }
}
