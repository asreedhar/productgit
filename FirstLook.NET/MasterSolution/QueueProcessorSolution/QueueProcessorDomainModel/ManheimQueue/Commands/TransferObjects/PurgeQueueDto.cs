﻿using System;
using FirstLook.QueueProcessor.DomainModel.BookQueue.Model;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Commands.TransferObjects
{
    [Serializable]
    public class PurgeQueueDto
    {
        public int Days { get; set; }
        public QueueStatus Status { get; set; }
    }
}
