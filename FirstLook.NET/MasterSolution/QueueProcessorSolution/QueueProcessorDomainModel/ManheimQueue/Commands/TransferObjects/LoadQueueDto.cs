﻿using System;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Commands.TransferObjects
{
    [Serializable]
    public class LoadQueueDto
    {
        public int Days { get; set; }
    }
}
