﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Commands
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory,Impl.CommandFactory>(ImplementationScope.Shared);
        }
    }
}
