﻿using System.Collections.Generic;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue
{
    public interface IManheimQueue
    {
        void PushPeriodicDataToQueue(int days);
        IList<VehicleType> DecodeVIN(string vin);
        void UpdateMMRAveragePriceFromQueue(int recordCount);
        MMRResult UpdateAveragePrice(MMRInventoryRequest request);
        void PurgeFailureQueue(int days);
        void PurgeSuccessQueue(int days);
        IList<MMRInventoryRequest> LoadMMRInventoryQueue(int recordCount,int maxAttempts);
    }
}
