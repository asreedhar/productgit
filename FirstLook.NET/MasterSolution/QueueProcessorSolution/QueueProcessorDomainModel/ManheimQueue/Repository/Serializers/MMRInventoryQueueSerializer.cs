﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Repository.Serializers
{
    public class MMRInventoryQueueSerializer : Serializer<MMRInventoryRequest>
    {
        public override MMRInventoryRequest Deserialize(IDataRecord record)
        {
            int inventoryId = record.GetInt32(record.GetOrdinal("InventoryID"));
            string vin =   record.GetString("VIN");
            string mid = record.GetString("MID");
            string regionCode = record.GetString("RegionCode");
            int? mileage = record.GetNullableInt32("Mileage");
            int queueId = record.GetInt32(record.GetOrdinal("QueueID"));
            string year =  record.GetString("Year");
            DateTime dateCreated = record.GetDateTime(record.GetOrdinal("DateCreated"));

            mileage = mileage == null ? 0 : mileage;
            int _year = Convert.ToInt32(year == null || year.Trim() == "" ? "1900" : year);

            return new MMRInventoryRequest()
                   {
                       QueueId = queueId,
                       InventoryId = inventoryId,
                       MID = mid,
                       Mileage = (int)mileage,
                       VehicleYear = _year,
                       RegionCode = regionCode,
                       VIN = vin

                   };
        }
    }
}
