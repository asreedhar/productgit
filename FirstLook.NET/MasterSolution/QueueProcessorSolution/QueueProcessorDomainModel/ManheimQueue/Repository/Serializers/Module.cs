﻿using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Repository.Serializers
{
    public class Module : IModule
    {
        /// <summary>
        /// Register the MMR Inventory serializers.
        /// </summary>
        /// <param name="registry">Registry.</param>
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<MMRInventoryRequest>, MMRInventoryQueueSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<MMRErrorTypes>, MMRErrorTypesSerializer>(ImplementationScope.Shared);
        }
    }
}
