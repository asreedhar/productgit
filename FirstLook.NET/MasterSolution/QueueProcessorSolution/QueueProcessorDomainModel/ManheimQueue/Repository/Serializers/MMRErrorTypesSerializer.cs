﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model;


namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Repository.Serializers
{
    public class MMRErrorTypesSerializer : Serializer<MMRErrorTypes>
    {
        public override MMRErrorTypes  Deserialize(IDataRecord record)
        {
            int errorTypeId = record.GetInt32(record.GetOrdinal("ErrorTypeId"));
            string errorDescription = record.GetString("Description");

            return new MMRErrorTypes()
            {
                MMRErrorTypeId = errorTypeId,
                ErrorDescription = errorDescription
            };
        }
    }
}
