﻿using System.Collections.Generic;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Repository
{
    public interface IManheimInventoryRepository
    {
        void UpdateMMRAveragePrice(IList<MMRResult> results);
        void PushPeriodicInventoryDataToQueue(int days);
        MMRInventoryRequest GetMmrInventoryRequest(int inventoryId);
    }
}
