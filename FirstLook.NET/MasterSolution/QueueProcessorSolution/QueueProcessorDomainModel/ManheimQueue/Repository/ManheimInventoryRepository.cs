﻿using System.Collections.Generic;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Repository.Mapper;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Repository
{
    public class ManheimInventoryRepository : RepositoryBase, IManheimInventoryRepository
    {
        #region Properties

        ManheimInventoryMapper _mapper = new ManheimInventoryMapper();

        #endregion

        #region IManheimInventoryRepository Members

        public void UpdateMMRAveragePrice(IList<Model.MMRResult> results)
        {
            _mapper.UpdateMMRAveragePrice(results);
        }

        public void PushPeriodicInventoryDataToQueue(int days)
        {
            _mapper.PushPeriodicInventoryDataToQueue(days);
        }

        public Model.MMRInventoryRequest GetMmrInventoryRequest(int inventoryId)
        {
            return _mapper.GetMmrInventoryRequest(inventoryId);
        }

        #endregion

        #region RepositoryBase Methods
        /// <summary>
        /// The Kelley Blue Book database is Vehicle.
        /// </summary>
        protected override string DatabaseName
        {
            get { return DomainModel.DatabaseName.InventoryDatabase; }
        }

        /// <summary>
        /// Do nothing on the start of a transaction.
        /// </summary>                
        protected override void OnBeginTransaction(IDataSession session)
        {
            // Do nothing on the start of a transaction.
        }
        #endregion
        
    }
}
