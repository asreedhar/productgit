﻿using System.Collections.Generic;
using FirstLook.QueueProcessor.DomainModel.BookQueue.Model;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model.Soap;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Repository.Datastores
{
    public interface IManheimQueueDatastore
    {
        IList<MMRInventoryRequest> FetchQueueData(int recordCount, int maxAttempts);

        IList<string> FetchRegions();

        IList<MMRErrorTypes> FetchMMRErrorTypes();

        void UpdateQueue(IList<QueueResult> results);

        void PurgeQueue(int days, QueueStatus queueStatus);
    }
}
