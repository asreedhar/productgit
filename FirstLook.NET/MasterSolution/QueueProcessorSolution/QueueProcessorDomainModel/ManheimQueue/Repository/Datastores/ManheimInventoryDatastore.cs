﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Repository.Datastores
{
    public class ManheimInventoryDatastore:IManheimInventoryDatastore
    {
        #region IManheimInventoryDatastore Members

        public void UpdateMMRAveragePrice(IList<Model.MMRResult> results)
        {
            int sqlTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["MMRBulkUpdateCommandTimeOut"]);

            DataTable mmrResultDataTable = new DataTable();
            mmrResultDataTable.Columns.Add(new DataColumn("InventoryID"));
            mmrResultDataTable.Columns.Add(new DataColumn("AveragePrice"));
            mmrResultDataTable.Columns.Add(new DataColumn("MID"));
            mmrResultDataTable.Columns.Add(new DataColumn("Region"));

            foreach (MMRResult result in results)
            {
                DataRow dataRow = mmrResultDataTable.NewRow();
                dataRow["InventoryID"] = result.InventoryID;
                dataRow["AveragePrice"] = result.AveragePrice;
                dataRow["MID"] = result.MID;
                dataRow["Region"] = result.Region;
                mmrResultDataTable.Rows.Add(dataRow);
            }

            using (IDbConnection connection = Database.Connection(DatabaseName.InventoryDatabase))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "MMR.MMRAveragePrice#Update";
                    if (sqlTimeout != 0)
                    {
                        command.CommandTimeout = 120;
                    }

                    SqlParameter param = new SqlParameter("UpdateDataTable", mmrResultDataTable);
                    param.SqlDbType = SqlDbType.Structured;
                    command.Parameters.Add(param);

                    command.ExecuteNonQuery();
                }
            }
        }

        public void PushPeriodicInventoryDataToQueue(int days)
        {
            int sqlTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["MMRBulkUpdateCommandTimeOut"]);

            using (IDbConnection connection = Database.Connection(DatabaseName.InventoryDatabase))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "MMR.PushPeriodicDataToQueue";
                    if (sqlTimeout != 0)
                    {
                        command.CommandTimeout = 120;
                    }

                    Database.AddWithValue(command, "days", days, DbType.Int32);

                    command.ExecuteNonQuery();
                }
            }
        }
        
        public MMRInventoryRequest GetMmrInventoryRequest(int inventoryId)
        {
            int sqlTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["MMRBulkUpdateCommandTimeOut"]);

            using (IDbConnection connection = Database.Connection(DatabaseName.InventoryDatabase))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "MMR.InventoryDetails#Fetch";
                    
                    if (sqlTimeout != 0)
                    {
                        command.CommandTimeout = 120;
                    }

                    Database.AddWithValue(command, "InventoryID", inventoryId, DbType.Int32);

                    IDataReader reader = command.ExecuteReader();

                    if (reader.Read())
                    {
                        return Deserialize((IDataRecord)reader);    
                    }
                    
                    return null;
                }
            }
        }
        #endregion

        #region Utitlity Methods
        protected T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }

        private MMRInventoryRequest Deserialize(IDataRecord record)
        {
            string mid = record.GetString("MID");
            string regionCode = record.GetString("RegionCode");
            int? mileage = record.GetNullableInt32("MileageReceived");
            int? year = Convert.ToInt32(record.GetValue(record.GetOrdinal("VehicleYear")));

            mileage = mileage == null ? 0 : mileage;
            int _year = Convert.ToInt32(year == null ? 1900 : year);

            return new MMRInventoryRequest()
            {
                MID = mid,
                RegionCode = regionCode,
                VehicleYear = Convert.ToInt32(year),
                Mileage = (int)mileage
            };
        }
        #endregion
    }
}
