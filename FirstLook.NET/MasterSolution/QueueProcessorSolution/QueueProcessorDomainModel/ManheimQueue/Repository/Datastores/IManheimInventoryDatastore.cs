﻿using System.Collections.Generic;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Repository.Datastores
{
    public interface IManheimInventoryDatastore
    {
        void UpdateMMRAveragePrice(IList<MMRResult> results);
        void PushPeriodicInventoryDataToQueue(int Days);
        MMRInventoryRequest GetMmrInventoryRequest(int inventoryId);
    }
}
