﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.QueueProcessor.DomainModel.BookQueue.Model;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model;


namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Repository.Datastores
{
    public class ManheimQueueDatastore : IManheimQueueDatastore
    {
        #region IManheimQueueDatastore Members

        public IList<MMRInventoryRequest> FetchQueueData(int recordCount, int maxAttempts)
        {
            int sqlTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["MMRBulkUpdateCommandTimeOut"]);

            IList<MMRInventoryRequest> list = new List<MMRInventoryRequest>();
            ISerializer<MMRInventoryRequest> serializer = Resolve<ISerializer<MMRInventoryRequest>>();

            using (IDbConnection connection = Database.Connection(DatabaseName.InventoryDatabase))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "MMR.QueueTable#Fetch";
                    if (sqlTimeout != 0)
                    {
                        command.CommandTimeout = 120;
                    }

                    Database.AddWithValue(command, "RecordCount", recordCount, DbType.Int32);
                    Database.AddWithValue(command, "MaxAttempts", maxAttempts, DbType.Int32);
                    
                    IDataReader reader = command.ExecuteReader();
                    
                    while (reader.Read())
                    {
                        list.Add(serializer.Deserialize((IDataRecord)reader));
                    }
                    
                    return list;
                }
            }
           
        }

        public IList<string> FetchRegions()
        {
            IList<string> list = new List<string>();
            
            int sqlTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["MMRBulkUpdateCommandTimeOut"]);

            using (IDbConnection connection = Database.Connection(DatabaseName.InventoryDatabase))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "select distinct ISNULL(RegionCode,'') RegionCode from IMT.MMR.QueueTable Where StatusID<>"+((int)QueueStatus.Success).ToString();
                    if (sqlTimeout != 0)
                    {
                        command.CommandTimeout = sqlTimeout;
                    }


                    IDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        list.Add(reader.GetString("RegionCode"));
                    }

                    return list;
                }
            }
        }
        
        public void UpdateQueue(IList<QueueResult> results)
        {
            DataTable queueResultDataTable = new DataTable();
            queueResultDataTable.Columns.Add(new DataColumn("QueueID"));
            queueResultDataTable.Columns.Add(new DataColumn("StatusID"));
            queueResultDataTable.Columns.Add(new DataColumn("ErrorMessage"));

            int sqlTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["MMRBulkUpdateCommandTimeOut"]);

            foreach (QueueResult result in results)
            {
                DataRow dataRow = queueResultDataTable.NewRow();
                dataRow["QueueID"] = result.QueueId;
                dataRow["StatusID"] = (int)result.Status;
                dataRow["ErrorMessage"] = result.ErrorMessage;
                queueResultDataTable.Rows.Add(dataRow);
            }

            using (IDbConnection connection = Database.Connection(DatabaseName.InventoryDatabase))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "MMR.QueueTable#Update";
                    if (sqlTimeout != 0)
                    {
                        command.CommandTimeout = 120;
                    }


                    SqlParameter param = new SqlParameter("QueueResult", queueResultDataTable);
                    param.SqlDbType = SqlDbType.Structured;
                    command.Parameters.Add(param);

                    command.ExecuteNonQuery();
                }
            }
        }

        public void PurgeQueue(int days, QueueStatus queueStatus)
        {
            int sqlTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["MMRBulkUpdateCommandTimeOut"]);

            using (IDbConnection connection = Database.Connection(DatabaseName.InventoryDatabase))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "MMR.PurgeQueue";
                    if (sqlTimeout != 0)
                    {
                        command.CommandTimeout = 120;
                    }


                    Database.AddWithValue(command, "Days", days, DbType.Int32);
                    Database.AddWithValue(command, "StatusID", queueStatus, DbType.Int32);

                    command.ExecuteNonQuery();
                }
            }
        }

        public IList<MMRErrorTypes> FetchMMRErrorTypes()
        {
            int sqlTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["MMRBulkUpdateCommandTimeOut"]);

            IList<MMRErrorTypes> list = new List<MMRErrorTypes>();
            ISerializer<MMRErrorTypes> serializer = Resolve<ISerializer<MMRErrorTypes>>();

            using (IDbConnection connection = Database.Connection(DatabaseName.InventoryDatabase))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "select ErrorTypeId,[Description] from IMT.MMR.MMR_ErrorType";
                 
                    IDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        list.Add(serializer.Deserialize((IDataRecord)reader));
                    }

                    return list;
                }
            }
        }
        #endregion

        #region Utitlity Methods
        protected T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }
        #endregion

    }
}
