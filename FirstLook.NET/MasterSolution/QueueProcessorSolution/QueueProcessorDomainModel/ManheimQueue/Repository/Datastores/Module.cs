﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Repository.Datastores
{
    public class Module : IModule
    {
        /// <summary>
        /// Register the MMR Inventory serializers.
        /// </summary>
        /// <param name="registry">Registry.</param>
        public void Configure(IRegistry registry)
        {
            registry.Register<IManheimInventoryDatastore, ManheimInventoryDatastore>(ImplementationScope.Shared);
            registry.Register<IManheimQueueDatastore, ManheimQueueDatastore>(ImplementationScope.Shared);
        }
    }
}
