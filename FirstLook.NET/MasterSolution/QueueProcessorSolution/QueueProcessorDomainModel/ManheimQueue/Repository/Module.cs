﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Repository
{
    public class Module : IModule
    {
        /// <summary>
        /// Register the MMR Inventory serializers.
        /// </summary>
        /// <param name="registry">Registry.</param>
        public void Configure(IRegistry registry)
        {
            registry.Register<IManheimInventoryRepository, ManheimInventoryRepository>(ImplementationScope.Shared);
            registry.Register<IManheimQueueRepository, ManheimQueueRepository>(ImplementationScope.Shared);
        }
    }
}
