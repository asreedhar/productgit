﻿using System.Collections.Generic;
using FirstLook.Common.Core.Registry;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Repository.Datastores;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Repository.Mapper
{
    public class ManheimInventoryMapper
    {
        #region Properties


        #endregion

        #region Methods

        public void UpdateMMRAveragePrice(IList<MMRResult> results)
        {
            Resolve<IManheimInventoryDatastore>().UpdateMMRAveragePrice(results);
        }

        public void PushPeriodicInventoryDataToQueue(int days)
        {
            Resolve<IManheimInventoryDatastore>().PushPeriodicInventoryDataToQueue(days);
        }

        public MMRInventoryRequest GetMmrInventoryRequest(int inventoryId)
        {
           return Resolve<IManheimInventoryDatastore>().GetMmrInventoryRequest(inventoryId);
        }

        #endregion

        #region Utitlity Methods
        protected T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }

        #endregion
    }
}
