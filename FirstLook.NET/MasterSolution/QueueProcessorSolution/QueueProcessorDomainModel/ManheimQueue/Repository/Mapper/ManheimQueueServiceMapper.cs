﻿using System.Collections.Generic;
using FirstLook.Common.Core.Registry;
using FirstLook.QueueProcessor.DomainModel.BookQueue.Model;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Repository.Datastores;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Repository.Mapper
{
    public class ManheimQueueServiceMapper
    {
        #region Properties


        #endregion

        #region Methods

        public IList<MMRInventoryRequest> FetchMMRInventoryQueue(int recordCount, int maxAttempts)
        {
            return Resolve<IManheimQueueDatastore>().FetchQueueData(recordCount,maxAttempts);
        }

        public IList<string> FetchQueueRegions()
        {
            return Resolve<IManheimQueueDatastore>().FetchRegions();
        }
        
        public IList<MMRErrorTypes> FetchMMRErrorTypes()
        {
            return Resolve<IManheimQueueDatastore>().FetchMMRErrorTypes();
        } 

        public void UpdateInventoryQueue(IList<QueueResult> results)
        {
            Resolve<IManheimQueueDatastore>().UpdateQueue(results);
        }

        public void PurgeInventoryQueue(int days, QueueStatus status)
        {
            Resolve<IManheimQueueDatastore>().PurgeQueue(days, status);
        }
        
        #endregion

        #region Utitlity Methods
        protected T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }

        #endregion
    }
}
