﻿using System.Collections.Generic;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.QueueProcessor.DomainModel.BookQueue.Model;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Repository.Mapper;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Repository
{
    public class ManheimQueueRepository:RepositoryBase,IManheimQueueRepository
    {
        #region Properties

         ManheimQueueServiceMapper _mapper= new ManheimQueueServiceMapper();

        #endregion

         #region IManheimQueueRepository Members

         public IList<MMRInventoryRequest> LoadBookRequestQueueList(int recordCount, int maxAttempts)
        {
            return DoInSession(() => _mapper.FetchMMRInventoryQueue(recordCount,maxAttempts));
        }

         public IList<string> FetchQueueRegions()
        {
            return DoInSession(() => _mapper.FetchQueueRegions());
        }

         public IList<MMRErrorTypes> FetchMMRErrorTypes()
        {
            return DoInSession(() => _mapper.FetchMMRErrorTypes());
        }
        

        public void Update(IList<QueueResult> results)
        {
            _mapper.UpdateInventoryQueue(results);
        }

        public void PurgeQueue(int days, QueueStatus queueStatus)
        {
             _mapper.PurgeInventoryQueue(days,queueStatus);
        }
        
        #endregion

        #region RepositoryBase Methods
        /// <summary>
        /// The Kelley Blue Book database is Vehicle.
        /// </summary>
        protected override string DatabaseName
        {
            get { return DomainModel.DatabaseName.InventoryDatabase; }
        }

        /// <summary>
        /// Do nothing on the start of a transaction.
        /// </summary>                
        protected override void OnBeginTransaction(IDataSession session)
        {
            // Do nothing on the start of a transaction.
        }
        #endregion

    }
}
