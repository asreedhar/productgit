﻿using System.Collections.Generic;
using FirstLook.QueueProcessor.DomainModel.BookQueue.Model;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Repository
{
    public interface IManheimQueueRepository
    {
        IList<MMRInventoryRequest> LoadBookRequestQueueList(int recordCount, int maxAttempts);

        IList<string> FetchQueueRegions();

        IList<MMRErrorTypes> FetchMMRErrorTypes();

        void Update(IList<QueueResult> results);

        void PurgeQueue(int days,QueueStatus queueStatus);
    }
}
