﻿using System;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model
{
    [Serializable]
    public class MMRErrorTypes
    {
        public int MMRErrorTypeId { get; set; }

        public string ErrorDescription { get; set; }
    }
}
