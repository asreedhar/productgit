﻿using System;
using FirstLook.QueueProcessor.DomainModel.BookQueue.Model;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model
{
    [Serializable]
    public class MMRResult:QueueResult
    {
        public int InventoryID { get; set; }
        public string MID { get; set; }
        public int AveragePrice { get; set; }
        public string Region { get; set; }
    }
}
