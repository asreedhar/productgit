﻿
namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model
{
    public class MMRLoadQueueRequest
    {
        public int RecordCount { get; set; }
        public int MaxAttempts { get; set; }
    }
}
