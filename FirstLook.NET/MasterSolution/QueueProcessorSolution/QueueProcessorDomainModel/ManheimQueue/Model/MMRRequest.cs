﻿using System;
using FirstLook.QueueProcessor.DomainModel.BookQueue.Model;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model
{
    [Serializable]
    public class MMRRequest:BookRequest
    {
        public string RegionCode { get; set; }
        public string MID { get; set; }
        public bool UseSeasonalAdjustment { get; set; }
    }
}
