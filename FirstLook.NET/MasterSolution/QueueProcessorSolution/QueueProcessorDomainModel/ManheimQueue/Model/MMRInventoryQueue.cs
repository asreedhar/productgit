﻿using FirstLook.QueueProcessor.DomainModel.BookQueue.Model;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model
{
    public class MMRInventoryQueue : InventoryQueue
    {
        public string MID { get; set; }
    }
}
