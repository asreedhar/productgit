﻿
namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model
{
    public class MMRInventoryRequest:MMRRequest
    {
        public int InventoryId { get; set; }
        public int VehicleYear { get; set; }
        public bool IsMultipleVin { get; set; }
        public bool IsDecodingError { get; set; }
        public string DecodeVinError { get; set; }
        public bool IsDbUpdateRequired { get; set; }
    }
}
