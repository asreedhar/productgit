using System.Collections.Generic;
using System.Net;
using FirstLook.QueueProcessor.DomainModel.com.manheim.developer.webservices;
using FirstLook.QueueProcessor.DomainModel.MMRMultiMidPricing;


namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model.Soap
{
    public interface ISoapService
    {
        IList<BulkPricing> Query(ICredentials credentials, int year, string mid, int miles);
        IList<BulkPricing> Query(ICredentials credentials, int year, string mid, int miles, string regionCode);
        MultiMidOutput Query(ICredentials credentials, IList<MMRInventoryRequest> mulitMidRequests, string region,
            bool useSeasonalAdjustment);
    }
}
