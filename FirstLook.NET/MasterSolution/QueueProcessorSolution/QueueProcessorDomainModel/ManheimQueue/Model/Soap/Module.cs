﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model.Soap
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISoapService, SoapService>(ImplementationScope.Shared);
        }
    }
}
