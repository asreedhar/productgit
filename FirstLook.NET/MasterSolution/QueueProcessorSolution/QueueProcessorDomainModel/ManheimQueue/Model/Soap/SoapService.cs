using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using FirstLook.QueueProcessor.DomainModel.com.manheim.developer.webservices;
using FirstLook.QueueProcessor.DomainModel.MMRMultiMidPricing;
using System.Configuration;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model.Soap
{
    public class SoapService : ISoapService
    {
        protected const string DecodeType = "MMR";

        protected const string MinYear = "1980";

        private BulkBookService service;
        private MultiMidPricingService multiMidPricingService;

        /// <summary>
        /// Query MMR web service for Price
        /// </summary>
        /// <param name="credentials">MMR Credentials</param>
        /// <param name="year">Vehicle Year</param>
        /// <param name="mid">Mid</param>
        /// <param name="miles">Mileage</param>
        /// <returns>List of Pricing</returns>
        public IList<BulkPricing> Query(ICredentials credentials, int year, string mid, int miles)
        {
            service = new BulkBookService()
            {
                Credentials = credentials
            };
           
            int timeOut = Convert.ToInt32(ConfigurationManager.AppSettings["MMRBulkServiceTimeout"]);

            if (timeOut != 0)
            {
                service.Timeout = timeOut;
            }

            VehicleType type=new VehicleType(){mid = mid,year = year};
            
            BulkPricing[] pricing=service.getBulkPricing(type, false, miles);

            return pricing.ToList();
        }

        /// <summary>
        /// Query MMR web service for Price
        /// </summary>
        /// <param name="credentials">MMR Credentials</param>
        /// <param name="year">Vehicle Year</param>
        /// <param name="mid">Mid</param>
        /// <param name="miles">Mileage</param>
        /// <param name="regionCode">MMR Region Code</param>
        /// <returns>List of Pricing</returns>
        public IList<BulkPricing> Query(ICredentials credentials, int year, string mid, int miles, string regionCode)
        {
            service = new BulkBookService()
            {
                Credentials = credentials
            };

            int timeOut = Convert.ToInt32(ConfigurationManager.AppSettings["MMRBulkServiceTimeout"]);

            if (timeOut != 0)
            {
                service.Timeout = timeOut;
            }
           
            VehicleType type = new VehicleType() { mid = mid, year = year };

            BulkPricing[] pricing = service.getBulkPricing(type, false, miles);

            return pricing.Where(p=>p.regionId==regionCode).ToList();
        }

        /// <summary>
        /// Query MMR web service for Bulk Records for Price
        /// </summary>
        /// <param name="credentials">MMR Credentials</param>
        /// <param name="mulitMidRequests"></param>
        /// <param name="region">Region Code</param>
        /// <param name="useSeasonalAdjustment"></param>
        /// <returns>List of Average price for List of mid and given region</returns>
        public MultiMidOutput Query(ICredentials credentials, IList<MMRInventoryRequest> mulitMidRequests, string region, bool useSeasonalAdjustment)
        {
            multiMidPricingService = new MultiMidPricingService(){Credentials = credentials};
            int timeOut = Convert.ToInt32(ConfigurationManager.AppSettings["MMRBulkServiceTimeout"]);
            
            if (timeOut != 0)
            {
                multiMidPricingService.Timeout = timeOut;
            }

            MultiMidInput input=new MultiMidInput();
            
            List<MultiMids> mulitMids=new List<MultiMids>();

            mulitMidRequests.ToList()
                .ForEach(mmr => mulitMids.Add(new MultiMids() {mid = mmr.MID, miles = mmr.Mileage}));

            input.multiMids = mulitMids.ToArray();
            input.region = region;
            input.useSeasonalAdjustment = useSeasonalAdjustment;

            return multiMidPricingService.getMultiPricing(input);
        }
    }
}
