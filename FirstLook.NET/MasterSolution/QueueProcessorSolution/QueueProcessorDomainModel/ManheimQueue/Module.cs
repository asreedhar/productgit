﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<Model.Soap.Module>();

            registry.Register<Commands.Module>();

            registry.Register<Repository.Serializers.Module>();

            registry.Register<Repository.Datastores.Module>();

            registry.Register<Repository.Module>();

            registry.Register<IManheimQueue,ProcessManheimQueue>(ImplementationScope.Shared);

        }
    }
}
