﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Threading;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.QueueProcessor.DomainModel.BookQueue;
using FirstLook.QueueProcessor.DomainModel.BookQueue.Model;
using FirstLook.QueueProcessor.DomainModel.com.manheim.developer.webservices;
using FirstLook.QueueProcessor.DomainModel.Exceptions;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Commands.Impl;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Commands.TransferObjects;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model;
using FirstLook.QueueProcessor.DomainModel.MMRMultiMidPricing;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Soap;
using ICommandFactory = FirstLook.QueueProcessor.DomainModel.ManheimQueue.Commands.ICommandFactory;
using VehicleType = FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.VehicleType;


namespace FirstLook.QueueProcessor.DomainModel.ManheimQueue
{
    public class ProcessManheimQueue : ProcessBookQueue, IManheimQueue
    {

        #region Properties

        private const string NaionalRegion = "NA";
        private ManualResetEvent Event;

        public string ManheimUserId { get; set; }

        public string ManheimPassword { get; set; }

        public IList<MMRInventoryRequest> NonMidMMRRequest { get; set; }
        
        #endregion

        #region Constructor

        public ProcessManheimQueue()
        {
            ManheimUserId = ConfigurationManager.AppSettings["ManheimUser"];
            ManheimPassword = ConfigurationManager.AppSettings["ManheimPsw"];
        }

        #endregion

        #region IManheimQueue Members
        /// <summary>
        /// Push Invenory data to MMR queue.
        /// </summary>
        /// <param name="days">Number of days</param>
        public void PushPeriodicDataToQueue(int days)
        {
            ExceptionLogger exceptionLogger = new ExceptionLogger();

            try
            {
                ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

                ICommand<LoadQueueDto> command =
                    factory.CreateMenheimPushInventoryQueueCommand();

                command.Execute(new LoadQueueDto() { Days = days });
            }
            catch (Exception ex)
            {
                exceptionLogger.Record(ex);
            }
        }

        /// <summary>
        /// Get MID from VIN
        /// </summary>
        /// <param name="vin">VIN</param>
        /// <returns>MMR Vehicle Types</returns>
        public IList<VehicleType> DecodeVIN(string vin)
        {
            ExceptionLogger exceptionLogger = new ExceptionLogger();
            ISoapService service = null;
            NetworkCredential credentials = new NetworkCredential(ManheimUserId, ManheimPassword);
            try
            {
                IResolver registry = RegistryFactory.GetResolver();

                service = registry.Resolve<ISoapService>();
            }
            catch (Exception ex)
            {
                exceptionLogger.Record(ex, vin);
            }
            return service.Vin(credentials, vin);

        }

        /// <summary>
        /// Update MMR Price from Queue table.
        /// </summary>
        /// <param name="recordCount">Number of Queue records to Process</param>
        public void UpdateMMRAveragePriceFromQueue(int recordCount)
        {
            ExceptionLogger exceptionLogger = new ExceptionLogger();
            int maxAttempts = Convert.ToInt32(ConfigurationManager.AppSettings["MaxFailureRetryAttempts"]);
            try
            {
                ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

                ICommand<IList<MMRResult>> command =
                    factory.CreateMenheimUpdateInventoryQueueCommand();
                IList<string> mmrRegions = GetRegions();
                
                if (mmrRegions == null || mmrRegions.Count==0)
                {
                    return;
                }

                IList<MMRInventoryRequest> requestAllRegions = LoadMMRInventoryQueue(recordCount, maxAttempts);

                if (requestAllRegions == null || requestAllRegions.Count==0)
                    return;

                foreach (string mmrRegion in mmrRegions)
                {
                    IList<MMRInventoryRequest> requests =
                        requestAllRegions.Where(r => r.RegionCode == mmrRegion).ToList();

                    if (requests.Count > 0)
                    {
                        IList<BookRequest> bookRequests = new List<BookRequest>();

                        NonMidMMRRequest = requests.Where(r => r.MID == null || r.MID.Trim() == "").ToList();
                        if (NonMidMMRRequest != null && NonMidMMRRequest.Count > 0)
                        {
                            DecodeVins(NonMidMMRRequest); //Decode Vins
                            foreach (MMRInventoryRequest nonMidRequest in NonMidMMRRequest)
                            {
                                requests.FirstOrDefault(req => req.QueueId == nonMidRequest.QueueId).MID = nonMidRequest.MID;
                                requests.FirstOrDefault(req => req.QueueId == nonMidRequest.QueueId).IsMultipleVin = nonMidRequest.IsMultipleVin;
                                requests.FirstOrDefault(req => req.QueueId == nonMidRequest.QueueId).IsDecodingError = nonMidRequest.IsDecodingError;
                                requests.FirstOrDefault(req => req.QueueId == nonMidRequest.QueueId).DecodeVinError = nonMidRequest.DecodeVinError;
                            }
                        }

                        requests.Where(r => r.IsMultipleVin == false && r.MID != null && r.MID.Trim() != "" && r.IsDecodingError == false)
                            .ToList()
                            .ForEach(bookRequests.Add);

                        if (bookRequests.Count > 0)
                        {
                            LoadTableQueueInMemoryQueue(bookRequests);

                            GetBulkServiceResult(
                                Convert.ToInt32(ConfigurationManager.AppSettings["MMRMultiPriceSupportedRecords"]));
                        }
                        
                        IList<MMRResult> mmrResults = new List<MMRResult>();
                        if (ServiceResult == null)
                        {
                            ServiceResult = new List<QueueResult>();
                        }

                        
                        this.ServiceResult.ToList().ForEach(sr => mmrResults.Add((MMRResult)sr));

                        if (mmrResults != null && mmrResults.Count > 0)
                        {
                            mmrResults.Where(mr => mr.Region == null).ToList().ForEach(result => result.Region = mmrRegion == "" ? NaionalRegion : mmrRegion);
                            //Update Inventory Price
                            command.Execute(mmrResults.Where(mr => mr.Status == QueueStatus.Success).ToList());
                        }

                        //Multiple MID Results
                        requests.Where(r => r.IsMultipleVin == true)
                            .ToList()
                            .ForEach(r => ServiceResult.Add(new MMRResult()
                                                            {
                                                                Status = QueueStatus.Success,
                                                                QueueId = r.QueueId,
                                                                InventoryID = r.InventoryId
                                                            }));
                        UpdateMMRErrorId(ServiceResult);
                        
                        //Add Failed Vin decode error messages
                        requests.Where(r => r.IsDecodingError == true)
                            .ToList()
                            .ForEach(r => ServiceResult.Add(new MMRResult()
                            {
                                Status = QueueStatus.Error,
                                ErrorMessage = r.DecodeVinError,
                                QueueId = r.QueueId,
                                InventoryID = r.InventoryId
                            }));

                        ServiceResult.Where(sr => sr.Status == QueueStatus.Awaiting && sr.ErrorMessage.Trim() != "").ToList().ForEach(errors => errors.Status = QueueStatus.Error);

                        if (ServiceResult.Count > 0)
                        {
                            ICommand<IList<QueueResult>> queueCommand =
                                factory.CreateMenheimUpdateQueueCommand();

                            //Update Queue table
                            queueCommand.Execute(this.ServiceResult);
                        }
                        
                    }
                    ServiceResult = null;
                }
            }
            catch (Exception ex)
            {
                exceptionLogger.Record(ex);
            }

        }

        /// <summary>
        /// Update MMR average price. This method updates MMR price in database and returns the same
        /// </summary>
        /// <param name="request">Request data with Inventory Id</param>
        /// <returns>MMR Average Price</returns>
        public MMRResult UpdateAveragePrice(MMRInventoryRequest request)
        {
            MMRResult result = new MMRResult();
            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

            ExceptionLogger exceptionLogger = new ExceptionLogger();

            ICommand<MMRInventoryRequest, int> manheimInventoyFetchCommand =
                factory.CreateMenheimInventoryDetailsFetchCommand();

            MMRInventoryRequest inventoryRequest = manheimInventoyFetchCommand.Execute(request.InventoryId);

            if (inventoryRequest == null)
            {
                throw new Exception("Inventory not found");
            }

            if (request.MID != null && Convert.ToString(request.MID).Trim() != "")
            {
                inventoryRequest.MID = request.MID;
            }
            if (request.RegionCode != null && Convert.ToString(request.RegionCode).Trim() != "")
            {
                inventoryRequest.RegionCode = request.RegionCode;
            }

            ICommand<IList<MMRResult>> command =
                factory.CreateMenheimUpdateInventoryQueueCommand();

            MultiMidOutput multiMidOutput;
            Model.Soap.ISoapService service = GetResolver().Resolve<Model.Soap.ISoapService>();
            NetworkCredential credentials = new NetworkCredential(ManheimUserId, ManheimPassword);
            try
            {
                IList<MMRInventoryRequest> mmrInventoryRequests=new List<MMRInventoryRequest>();
                mmrInventoryRequests.Add(inventoryRequest);
                
                if (inventoryRequest.RegionCode == null || inventoryRequest.RegionCode.Trim() == "")
                {
                    multiMidOutput = service.Query(credentials, mmrInventoryRequests, NaionalRegion, false);
                }
                else
                {
                    multiMidOutput = service.Query(credentials, mmrInventoryRequests, request.RegionCode, false);
                }

                if (multiMidOutput != null && multiMidOutput.multiMidPriceInfo != null && multiMidOutput.multiMidPriceInfo.Any())
                {
                    
                    MultiMidPriceInfo priceInfo=multiMidOutput.multiMidPriceInfo.Where(mmp => mmp.mid == request.MID).FirstOrDefault();
                    if (priceInfo.errormid.Trim() == "")
                    {
                        result.AveragePrice =priceInfo.wholesaleGoodPrice;
                        result.InventoryID = request.InventoryId;
                        result.MID = inventoryRequest.MID;
                        result.Status = QueueStatus.Success;
                    }
                    else
                    {
                        result.InventoryID = request.InventoryId;
                        result.MID = inventoryRequest.MID;
                        result.ErrorMessage = priceInfo.errormid;
                        result.Status = QueueStatus.MMRError;
                    }
                }
            }
            catch (Exception ex)
            {
                result.QueueId = request.QueueId;
                result.AveragePrice = 0;
                result.ErrorMessage = ex.Message;
                result.Status = QueueStatus.Error;
                exceptionLogger.Record(ex, request.QueueId.ToString());
            }
            
            IList<MMRResult> results = new List<MMRResult>();
            results.Add(result);
            if (request.IsDbUpdateRequired)
            {
                command.Execute(results);
            }
            return result;

        }

        /// <summary>
        /// Purge Queue data for Failed Queue records
        /// </summary>
        /// <param name="days">Number of days to Purge data</param>
        public void PurgeFailureQueue(int days)
        {
            ExceptionLogger exceptionLogger = new ExceptionLogger();
            try
            {
                ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

                ICommand<PurgeQueueDto> command =
                    factory.CreateMenheimPurgeQueueCommand();

                command.Execute(new PurgeQueueDto() { Days = days, Status = QueueStatus.Error });
            }
            catch (Exception ex)
            {
                exceptionLogger.Record(ex);
            }
        }

        /// <summary>
        /// Purge Queue data with status as Success
        /// </summary>
        /// <param name="days">Number of days to Purge data</param>
        public void PurgeSuccessQueue(int days)
        {
            ExceptionLogger exceptionLogger = new ExceptionLogger();
            try
            {
                ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

                ICommand<PurgeQueueDto> command =
                    factory.CreateMenheimPurgeQueueCommand();

                command.Execute(new PurgeQueueDto() { Days = days, Status = QueueStatus.Success });
            }
            catch (Exception ex)
            {
                exceptionLogger.Record(ex);
            }
        }

        /// <summary>
        /// Load MMR inventory in queue table
        /// </summary>
        /// <param name="recordCount">Number of Records to Load in Queue</param>
        /// <returns>MMRInventoryRequest objects</returns>
        public IList<MMRInventoryRequest> LoadMMRInventoryQueue(int recordCount, int maxAttempts)
        {
            ExceptionLogger exceptionLogger = new ExceptionLogger();
            try
            {
                ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

                ICommand<IList<MMRInventoryRequest>, MMRLoadQueueRequest> command =
                    factory.CreateMenheimLoadQueueCommand();

                return command.Execute(new MMRLoadQueueRequest()
                                       {
                                           RecordCount = recordCount,
                                           MaxAttempts = maxAttempts
                                       });
            }
            catch (Exception ex)
            {
                exceptionLogger.Record(ex, ex.Message);
            }
            return null;
        }

        public IList<string> GetRegions()
        {
            ExceptionLogger exceptionLogger = new ExceptionLogger();
            try
            {
                return new ManheimLoadQueueCommand().GetRegions();
            }
            catch (Exception ex)
            {
                exceptionLogger.Record(ex, ex.Message);
            }
            return null;
        }

        public void UpdateMMRErrorId(List<QueueResult> results)
        {
            ExceptionLogger exceptionLogger = new ExceptionLogger();
            try
            {
                IList<MMRErrorTypes> mmrErrorTypes = new ManheimLoadQueueCommand().FetchMMRErrorTypes();

                foreach (MMRErrorTypes mmrErrorType in mmrErrorTypes)
                {
                    results.Where(r => r.Status == QueueStatus.Error && r.ErrorMessage.Contains(mmrErrorType.ErrorDescription)).ToList().ForEach(mmrError => mmrError.Status = QueueStatus.MMRError);
                }
            }
            catch (Exception ex)
            {
                exceptionLogger.Record(ex, ex.Message);
            }
        }


        #endregion

        #region Utility Methods
        protected static IResolver GetResolver()
        {
            IResolver resolver = RegistryFactory.GetResolver();

            return resolver;
        }

        private void DecodeVins(IList<MMRInventoryRequest> requests)
        {
            if (requests==null || requests.Count==0){return;}

            foreach (MMRInventoryRequest request in requests)
            {
                Event = new ManualResetEvent(false);
                ThreadPool.QueueUserWorkItem(new WaitCallback(DecodeVinInThread), request);
            }
            if (requests.Count > 0)
                Event.WaitOne();

        }

        private void DecodeVinInThread(object mmrRequest)
        {
            MMRInventoryRequest mmrInventoryRequest= (MMRInventoryRequest)mmrRequest;
            
            try
            {
                if (mmrInventoryRequest==null){throw new Exception("No VIN to decode.");}

                NonMidMMRRequest.Where(nmr => nmr.QueueId == mmrInventoryRequest.QueueId).FirstOrDefault();
                IList<VehicleType> vehicleTypes =
                    DecodeVIN(
                        NonMidMMRRequest.Where(nmr => nmr.QueueId == mmrInventoryRequest.QueueId).FirstOrDefault().VIN);

                if (vehicleTypes.Count > 1)
                {
                    //More than 1 MID Found
                    NonMidMMRRequest.Where(nmr => nmr.QueueId == mmrInventoryRequest.QueueId)
                        .FirstOrDefault()
                        .IsMultipleVin = true;

                }
                else
                {
                    //single MID found
                    NonMidMMRRequest.Where(nmr => nmr.QueueId == mmrInventoryRequest.QueueId).FirstOrDefault().MID =
                        vehicleTypes.FirstOrDefault().Mid;
                }
            }
            catch (Exception ex)
            {
                NonMidMMRRequest.Where(nmr => nmr.QueueId == mmrInventoryRequest.QueueId)
                    .FirstOrDefault()
                    .IsDecodingError = true;
                NonMidMMRRequest.Where(nmr => nmr.QueueId == mmrInventoryRequest.QueueId)
                    .FirstOrDefault()
                    .DecodeVinError = ex.Message;
            }
           
            if (NonMidMMRRequest.Count(req => (req.MID == null || req.MID.Trim() == "") && req.IsMultipleVin == false && req.IsDecodingError == false) == 0)
            {
                Event.Set();
            }
           
        }
        
        #endregion

        #region ProcessBookQueue Class Methods
        /// <summary>
        /// Call MMR web service and Get result
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public override QueueResult GetWebServiceResult(BookRequest request)
        {
            ExceptionLogger exceptionLogger = new ExceptionLogger();
            MMRInventoryRequest mmrRequest = (MMRInventoryRequest)request;

            Model.Soap.ISoapService service = GetResolver().Resolve<Model.Soap.ISoapService>();

            NetworkCredential credentials = new NetworkCredential(ManheimUserId, ManheimPassword);
            IList<BulkPricing> pricingList = null;
            MMRResult result = new MMRResult();

            try
            {
                //If MID is not found then Call VINdecoder service to retrive VIN
                if (mmrRequest.MID == null || mmrRequest.MID.Trim() == "")
                {
                    IList<VehicleType> vehicleTypes = DecodeVIN(request.VIN);

                    if (vehicleTypes == null)
                    {
                        throw new Exception("MID not found.");
                    }
                    // If more than one MID is found mark Queue row as success.
                    if (vehicleTypes.Count > 1)
                    {
                        result.InventoryID = mmrRequest.InventoryId;
                        result.QueueId = mmrRequest.QueueId;
                        result.Status = QueueStatus.Success;
                    }
                    else
                    {
                        mmrRequest.MID = vehicleTypes.FirstOrDefault().Mid;
                    }
                }
                if (result.Status != QueueStatus.Success)
                {
                    if (mmrRequest.RegionCode == null || mmrRequest.RegionCode.Trim() == "")
                    {
                        pricingList = service.Query(credentials, mmrRequest.VehicleYear, mmrRequest.MID,
                            mmrRequest.Mileage);
                        pricingList = pricingList.Where(pl => pl.regionId == NaionalRegion).ToList();
                    }
                    else
                    {
                        pricingList = service.Query(credentials, mmrRequest.VehicleYear, mmrRequest.MID,
                            mmrRequest.Mileage,
                            mmrRequest.RegionCode);
                    }
                    if (pricingList != null && pricingList.Count > 0)
                    {
                        result.AveragePrice = pricingList.FirstOrDefault().averagePriceNow;
                        result.InventoryID = mmrRequest.InventoryId;
                        result.QueueId = mmrRequest.QueueId;
                        result.Status = QueueStatus.Success;
                        result.MID = mmrRequest.MID;
                    }
                    else
                    {
                        throw new Exception("Price Not found for MID - " + ((MMRInventoryRequest) request).MID +
                                            ", for region " + ((MMRInventoryRequest) request).RegionCode);
                    }
                }

            }
            catch (Exception ex)
            {
                exceptionLogger.Record(ex, request.QueueId.ToString());
                result.QueueId = request.QueueId;
                result.ErrorMessage = ex.Message;
                result.Status = QueueStatus.Error;
            }
           
            return result;
        }

        /// <summary>
        /// Call MMR web service for Bulk data
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>

        public override List<QueueResult> GetWebServiceResult(List<BookRequest> request)
        {
            ExceptionLogger exceptionLogger = new ExceptionLogger();
            List<MMRInventoryRequest> mmrRequests = new List<MMRInventoryRequest>();
            request.ForEach(r => mmrRequests.Add((MMRInventoryRequest)r));

            Model.Soap.ISoapService service = GetResolver().Resolve<Model.Soap.ISoapService>();

            NetworkCredential credentials = new NetworkCredential(ManheimUserId, ManheimPassword);
            MultiMidOutput pricingList = null;
            List<MMRResult> results = new List<MMRResult>();

            try
            {
                pricingList = service.Query(credentials, mmrRequests.Where(mmr => mmr.IsMultipleVin == false).ToList(), mmrRequests.FirstOrDefault().RegionCode, mmrRequests.FirstOrDefault().UseSeasonalAdjustment);

                if (pricingList.multiMidPriceInfo != null && pricingList.multiMidPriceInfo.Length > 0)
                {
                    foreach (MMRInventoryRequest mmrInventoryRequest in mmrRequests)
                    {
                        results.Add(new MMRResult()
                                                         {
                                                             AveragePrice = pricingList.multiMidPriceInfo.Where(mmp => mmp.mid == mmrInventoryRequest.MID && mmp.miles == mmrInventoryRequest.Mileage).FirstOrDefault().wholesaleGoodPrice,
                                                             InventoryID = mmrInventoryRequest.InventoryId,
                                                             QueueId = mmrInventoryRequest.QueueId,
                                                             MID = mmrInventoryRequest.MID,
                                                             ErrorMessage = pricingList.multiMidPriceInfo.Where(mmp => mmp.mid == mmrInventoryRequest.MID && mmp.miles == mmrInventoryRequest.Mileage).FirstOrDefault().errormid,
                                                             Region = mmrInventoryRequest.RegionCode == null || mmrInventoryRequest.RegionCode.Trim() == "" ? NaionalRegion : mmrInventoryRequest.RegionCode,
                                                             Status = pricingList.multiMidPriceInfo.Where(mmp => mmp.mid == mmrInventoryRequest.MID && mmp.miles == mmrInventoryRequest.Mileage).FirstOrDefault().errormid == "" ? QueueStatus.Success : QueueStatus.Error
                                                         });

                    }

                }
                else
                {
                    throw new Exception("Error Connecting to web service");
                }
            }
            catch (Exception ex)
            {
                mmrRequests.ForEach(mmr => results.Add(new MMRResult()
                                                {
                                                    InventoryID = mmr.InventoryId,
                                                    QueueId = mmr.QueueId,
                                                    MID = mmr.MID,
                                                    ErrorMessage = ex.Message,
                                                    Region = mmr.RegionCode == null || mmr.RegionCode.Trim() == "" ? NaionalRegion : mmr.RegionCode,
                                                    Status = QueueStatus.Error
                                                }));
                exceptionLogger.Record(ex);
            }
           
            List<QueueResult> returnResult = new List<QueueResult>();
            results.ForEach(returnResult.Add);

            return returnResult;
        }
        #endregion
    }
}
