﻿using System;

namespace FirstLook.QueueProcessor.DomainModel.Exceptions
{
    /// <summary>
    /// Abstract base class for exceptions caused by bad user input.
    /// </summary>
    [Serializable]
    public abstract class UserInputException : Exception
    {
        /// <summary>
        /// Create an exception with the given message.
        /// </summary>
        /// <param name="message">Exception message.</param>
        protected UserInputException(string message)
            : base(message)
        {
        }
    }

    #region Identity Exceptions

    /// <summary>
    /// Exception for when the passed-in user identity is not valid.
    /// </summary>
    [Serializable]
    public class UserIdentityException : UserInputException
    {
        public UserIdentityException()
            : base("User identity is not valid.")
        {
        }
    }

    #endregion
}
