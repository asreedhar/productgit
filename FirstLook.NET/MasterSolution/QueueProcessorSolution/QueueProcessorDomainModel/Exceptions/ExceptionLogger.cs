﻿using System;
using System.Data;
using System.Diagnostics;
using System.Threading;
using FirstLook.Common.Core.Data;

namespace FirstLook.QueueProcessor.DomainModel.Exceptions
{
    public class ExceptionLogger : IExceptionLogger
    {
        #region IExceptionLogger Members

        public void Record(Exception exception)
        {
            Insert(exception, null);
        }

        public void Record(Exception exception, string message)
        {
            Insert(exception, message);
        }

        #endregion

        /// <summary>
        /// Insert Exception in Database
        /// </summary>
        /// <param name="exception"> Exception details</param>
        /// <param name="request">Request Details</param>
        private static void Insert(Exception exception, string request)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection(DatabaseName.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "MMR.DatafeedException#Insert";

                    string exceptionUser = Thread.CurrentPrincipal.Identity.Name;

                    command.AddParameter("MachineName", DbType.String, GetMachineNameWithAssert());
                    command.AddParameter("Message", DbType.String, exception.Message);
                    command.AddParameter("ExceptionType", DbType.String, exception.GetType().ToString());
                    command.AddParameter("Details", DbType.String, new StackTrace(exception).ToString());

                    if (!string.IsNullOrEmpty(request))
                    {
                        command.AddParameter("RequestMessage", DbType.String, request);
                    }
                    else
                    {
                        command.AddParameter("RequestMessage", DbType.String, DBNull.Value);
                    }
                    
                    command.AddParameter("ExceptionUser", DbType.String, exceptionUser);

                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Get the name of the machine on which this exception occurred.
        /// </summary>
        /// <returns>Name of the machine this code is running on.</returns>        
        private static string GetMachineNameWithAssert()
        {
            return Environment.MachineName;
        }     
    }
}
