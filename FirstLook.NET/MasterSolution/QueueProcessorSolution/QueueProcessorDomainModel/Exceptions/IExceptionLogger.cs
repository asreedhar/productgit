﻿using System;

namespace FirstLook.QueueProcessor.DomainModel.Exceptions
{
    public interface IExceptionLogger
    {
        /// <summary>
        /// Record an exception.
        /// </summary>
        /// <param name="exception">Exception to record.</param>
        void Record(Exception exception);

        /// <summary>
        /// Record an exception with the incoming request message that likely caused it.
        /// </summary>
        /// <param name="exception">Exception to record.</param>
        /// <param name="message">Incoming request message.</param>
        void Record(Exception exception, string message);

    }
}
