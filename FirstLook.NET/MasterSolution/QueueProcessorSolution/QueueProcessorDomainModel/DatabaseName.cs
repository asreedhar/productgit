﻿
namespace FirstLook.QueueProcessor.DomainModel
{
        public static class DatabaseName
        {
            public const string PricingDatabase = "Market";
            public const string ReportDatabase = "Market";
            public const string InventoryDatabase = "IMT";
            public const string ListDatabase = "IMT";
            public const string MarketingDatabase = "IMT";
            public const string JDPowerDatabase = "AnalysisServices.JDPower";
            public const string VehicleCatalogDatabase = "VehicleCatalog";
            public const string LotDatabase = "Lot";
            public const string Merchandising = "Merchandising";
        }
    
}
