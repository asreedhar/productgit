﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1008
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.1008.
// 
#pragma warning disable 1591

namespace FirstLook.QueueProcessor.DomainModel.MMRMultiMidPricing {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.ComponentModel;
    using System.Xml.Serialization;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="MultiMidPricingSoapBinding", Namespace="http://webservices.manheim.com")]
    public partial class MultiMidPricingService : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback getEditionInfoOperationCompleted;
        
        private System.Threading.SendOrPostCallback getRegionListOperationCompleted;
        
        private System.Threading.SendOrPostCallback getRegionByAuctionInitialsOperationCompleted;
        
        private System.Threading.SendOrPostCallback getMultiPricingOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public MultiMidPricingService() {
            this.Url = global::FirstLook.QueueProcessor.DomainModel.Properties.Settings.Default.QueueProcessorDomainModel_MMRMultiMidPricing_MultiMidPricingService;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event getEditionInfoCompletedEventHandler getEditionInfoCompleted;
        
        /// <remarks/>
        public event getRegionListCompletedEventHandler getRegionListCompleted;
        
        /// <remarks/>
        public event getRegionByAuctionInitialsCompletedEventHandler getRegionByAuctionInitialsCompleted;
        
        /// <remarks/>
        public event getMultiPricingCompletedEventHandler getMultiPricingCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://webservices.manheim.com", ResponseNamespace="http://webservices.manheim.com", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("getEditionInfoReturn")]
        public Version getEditionInfo() {
            object[] results = this.Invoke("getEditionInfo", new object[0]);
            return ((Version)(results[0]));
        }
        
        /// <remarks/>
        public void getEditionInfoAsync() {
            this.getEditionInfoAsync(null);
        }
        
        /// <remarks/>
        public void getEditionInfoAsync(object userState) {
            if ((this.getEditionInfoOperationCompleted == null)) {
                this.getEditionInfoOperationCompleted = new System.Threading.SendOrPostCallback(this.OngetEditionInfoOperationCompleted);
            }
            this.InvokeAsync("getEditionInfo", new object[0], this.getEditionInfoOperationCompleted, userState);
        }
        
        private void OngetEditionInfoOperationCompleted(object arg) {
            if ((this.getEditionInfoCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.getEditionInfoCompleted(this, new getEditionInfoCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://webservices.manheim.com", ResponseNamespace="http://webservices.manheim.com", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("getRegionListReturn")]
        public Region[] getRegionList() {
            object[] results = this.Invoke("getRegionList", new object[0]);
            return ((Region[])(results[0]));
        }
        
        /// <remarks/>
        public void getRegionListAsync() {
            this.getRegionListAsync(null);
        }
        
        /// <remarks/>
        public void getRegionListAsync(object userState) {
            if ((this.getRegionListOperationCompleted == null)) {
                this.getRegionListOperationCompleted = new System.Threading.SendOrPostCallback(this.OngetRegionListOperationCompleted);
            }
            this.InvokeAsync("getRegionList", new object[0], this.getRegionListOperationCompleted, userState);
        }
        
        private void OngetRegionListOperationCompleted(object arg) {
            if ((this.getRegionListCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.getRegionListCompleted(this, new getRegionListCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://webservices.manheim.com", ResponseNamespace="http://webservices.manheim.com", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("getRegionByAuctionInitialsReturn")]
        public Region getRegionByAuctionInitials(string auctionInit) {
            object[] results = this.Invoke("getRegionByAuctionInitials", new object[] {
                        auctionInit});
            return ((Region)(results[0]));
        }
        
        /// <remarks/>
        public void getRegionByAuctionInitialsAsync(string auctionInit) {
            this.getRegionByAuctionInitialsAsync(auctionInit, null);
        }
        
        /// <remarks/>
        public void getRegionByAuctionInitialsAsync(string auctionInit, object userState) {
            if ((this.getRegionByAuctionInitialsOperationCompleted == null)) {
                this.getRegionByAuctionInitialsOperationCompleted = new System.Threading.SendOrPostCallback(this.OngetRegionByAuctionInitialsOperationCompleted);
            }
            this.InvokeAsync("getRegionByAuctionInitials", new object[] {
                        auctionInit}, this.getRegionByAuctionInitialsOperationCompleted, userState);
        }
        
        private void OngetRegionByAuctionInitialsOperationCompleted(object arg) {
            if ((this.getRegionByAuctionInitialsCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.getRegionByAuctionInitialsCompleted(this, new getRegionByAuctionInitialsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://webservices.manheim.com", ResponseNamespace="http://webservices.manheim.com", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("getMultiPricingReturn")]
        public MultiMidOutput getMultiPricing(MultiMidInput multiMidInput) {
            object[] results = this.Invoke("getMultiPricing", new object[] {
                        multiMidInput});
            return ((MultiMidOutput)(results[0]));
        }
        
        /// <remarks/>
        public void getMultiPricingAsync(MultiMidInput multiMidInput) {
            this.getMultiPricingAsync(multiMidInput, null);
        }
        
        /// <remarks/>
        public void getMultiPricingAsync(MultiMidInput multiMidInput, object userState) {
            if ((this.getMultiPricingOperationCompleted == null)) {
                this.getMultiPricingOperationCompleted = new System.Threading.SendOrPostCallback(this.OngetMultiPricingOperationCompleted);
            }
            this.InvokeAsync("getMultiPricing", new object[] {
                        multiMidInput}, this.getMultiPricingOperationCompleted, userState);
        }
        
        private void OngetMultiPricingOperationCompleted(object arg) {
            if ((this.getMultiPricingCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.getMultiPricingCompleted(this, new getMultiPricingCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://webservices.manheim.com")]
    public partial class Version {
        
        private long bytesField;
        
        private System.Nullable<System.DateTime> dateField;
        
        private string displayMonthAndDayField;
        
        private string displayNextMonthField;
        
        private string displayNextYearField;
        
        private string displaySixMonthsAgoField;
        
        private string displayThisMonthField;
        
        private string displayTwelveMonthsAgoField;
        
        private string displayTwoMonthsAgoField;
        
        private string displayWeekOfField;
        
        private string editionField;
        
        private int numberField;
        
        private int schemaField;
        
        /// <remarks/>
        public long bytes {
            get {
                return this.bytesField;
            }
            set {
                this.bytesField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<System.DateTime> date {
            get {
                return this.dateField;
            }
            set {
                this.dateField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string displayMonthAndDay {
            get {
                return this.displayMonthAndDayField;
            }
            set {
                this.displayMonthAndDayField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string displayNextMonth {
            get {
                return this.displayNextMonthField;
            }
            set {
                this.displayNextMonthField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string displayNextYear {
            get {
                return this.displayNextYearField;
            }
            set {
                this.displayNextYearField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string displaySixMonthsAgo {
            get {
                return this.displaySixMonthsAgoField;
            }
            set {
                this.displaySixMonthsAgoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string displayThisMonth {
            get {
                return this.displayThisMonthField;
            }
            set {
                this.displayThisMonthField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string displayTwelveMonthsAgo {
            get {
                return this.displayTwelveMonthsAgoField;
            }
            set {
                this.displayTwelveMonthsAgoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string displayTwoMonthsAgo {
            get {
                return this.displayTwoMonthsAgoField;
            }
            set {
                this.displayTwoMonthsAgoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string displayWeekOf {
            get {
                return this.displayWeekOfField;
            }
            set {
                this.displayWeekOfField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string edition {
            get {
                return this.editionField;
            }
            set {
                this.editionField = value;
            }
        }
        
        /// <remarks/>
        public int number {
            get {
                return this.numberField;
            }
            set {
                this.numberField = value;
            }
        }
        
        /// <remarks/>
        public int schema {
            get {
                return this.schemaField;
            }
            set {
                this.schemaField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://webservices.manheim.com")]
    public partial class MultiMidPriceInfo {
        
        private int auctionAverageMilesField;
        
        private string errormidField;
        
        private string midField;
        
        private int milesField;
        
        private int wholesaleExcellentPriceField;
        
        private int wholesaleFairPriceField;
        
        private int wholesaleGoodPriceField;
        
        /// <remarks/>
        public int auctionAverageMiles {
            get {
                return this.auctionAverageMilesField;
            }
            set {
                this.auctionAverageMilesField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string errormid {
            get {
                return this.errormidField;
            }
            set {
                this.errormidField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string mid {
            get {
                return this.midField;
            }
            set {
                this.midField = value;
            }
        }
        
        /// <remarks/>
        public int miles {
            get {
                return this.milesField;
            }
            set {
                this.milesField = value;
            }
        }
        
        /// <remarks/>
        public int wholesaleExcellentPrice {
            get {
                return this.wholesaleExcellentPriceField;
            }
            set {
                this.wholesaleExcellentPriceField = value;
            }
        }
        
        /// <remarks/>
        public int wholesaleFairPrice {
            get {
                return this.wholesaleFairPriceField;
            }
            set {
                this.wholesaleFairPriceField = value;
            }
        }
        
        /// <remarks/>
        public int wholesaleGoodPrice {
            get {
                return this.wholesaleGoodPriceField;
            }
            set {
                this.wholesaleGoodPriceField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://webservices.manheim.com")]
    public partial class MultiMidOutput {
        
        private MultiMidPriceInfo[] multiMidPriceInfoField;
        
        private string regionField;
        
        private bool useSeasonalAdjustmentField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(IsNullable=true)]
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable=false)]
        public MultiMidPriceInfo[] multiMidPriceInfo {
            get {
                return this.multiMidPriceInfoField;
            }
            set {
                this.multiMidPriceInfoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string region {
            get {
                return this.regionField;
            }
            set {
                this.regionField = value;
            }
        }
        
        /// <remarks/>
        public bool useSeasonalAdjustment {
            get {
                return this.useSeasonalAdjustmentField;
            }
            set {
                this.useSeasonalAdjustmentField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://webservices.manheim.com")]
    public partial class MultiMids {
        
        private string midField;
        
        private int milesField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string mid {
            get {
                return this.midField;
            }
            set {
                this.midField = value;
            }
        }
        
        /// <remarks/>
        public int miles {
            get {
                return this.milesField;
            }
            set {
                this.milesField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://webservices.manheim.com")]
    public partial class MultiMidInput {
        
        private MultiMids[] multiMidsField;
        
        private string regionField;
        
        private bool useSeasonalAdjustmentField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(IsNullable=true)]
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable=false)]
        public MultiMids[] multiMids {
            get {
                return this.multiMidsField;
            }
            set {
                this.multiMidsField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string region {
            get {
                return this.regionField;
            }
            set {
                this.regionField = value;
            }
        }
        
        /// <remarks/>
        public bool useSeasonalAdjustment {
            get {
                return this.useSeasonalAdjustmentField;
            }
            set {
                this.useSeasonalAdjustmentField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://webservices.manheim.com")]
    public partial class Region {
        
        private string regionCodeField;
        
        private string regionDescriptionField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string regionCode {
            get {
                return this.regionCodeField;
            }
            set {
                this.regionCodeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string regionDescription {
            get {
                return this.regionDescriptionField;
            }
            set {
                this.regionDescriptionField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    public delegate void getEditionInfoCompletedEventHandler(object sender, getEditionInfoCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class getEditionInfoCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal getEditionInfoCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public Version Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((Version)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    public delegate void getRegionListCompletedEventHandler(object sender, getRegionListCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class getRegionListCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal getRegionListCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public Region[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((Region[])(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    public delegate void getRegionByAuctionInitialsCompletedEventHandler(object sender, getRegionByAuctionInitialsCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class getRegionByAuctionInitialsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal getRegionByAuctionInitialsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public Region Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((Region)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    public delegate void getMultiPricingCompletedEventHandler(object sender, getMultiPricingCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class getMultiPricingCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal getMultiPricingCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public MultiMidOutput Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((MultiMidOutput)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591