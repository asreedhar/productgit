﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.QueueProcessor.DomainModel
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ManheimQueue.Module>();
            registry.Register<BookQueue.Module>();
            registry.Register<VehicleValuationGuide.DomainModel.Manheim.Model.Soap.Module>();
        }
    }

}
