﻿using System;
using System.Configuration;
using System.Net;
using System.Threading;
using FirstLook.Queue.ManheimQueueWebService.Client.ManheimQueue;

namespace QueueProcessorSolution
{
    public class Program
    {
        static ManualResetEvent m = new ManualResetEvent(false);

        static void Main(string[] args)
        {
            int recordCount = Convert.ToInt32(ConfigurationManager.AppSettings["FetchRecordCount"]);
            string userName = ConfigurationManager.AppSettings["UserId"];
            int serviceTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["QueueServiceTimeout"]);
            
            UpdateMMREventargs updateMmrEventargs=new UpdateMMREventargs(){RecordCount = recordCount,UserName = userName};
            ThreadPool.QueueUserWorkItem(new WaitCallback(UpdateMMRPrice), updateMmrEventargs);
            m.WaitOne();
        }

        private static void UpdateMMRPrice(object updateMmrEventargs)
        {
            try
            {
                UpdateMMREventargs updateMmr = (UpdateMMREventargs)updateMmrEventargs;
                ManheimQueue manheimQueue = new ManheimQueue();
                if (updateMmr.ServiceTimeOut != 0)
                {
                    manheimQueue.Timeout = updateMmr.ServiceTimeOut;
                }

                manheimQueue.Credentials = new NetworkCredential(updateMmr.UserName, "N@d@123");
                manheimQueue.UpdateMMRAveragePriceFromQueue(updateMmr.RecordCount);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                m.Set();
            }
        }

        private class UpdateMMREventargs
        {
            public string UserName { get; set; }
            public int RecordCount { get; set; }
            public int  ServiceTimeOut { get; set; }
        }
        
    }
}
