﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web;
using System.Web.Caching;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model.Soap;
using NUnit.Framework;


namespace FirstLook.QueueProcessor.DomainModel.Tests.Unit
{
     [TestFixture]
    public class ManheimQueueTest
    {
        private IRegistry _registry;
        //private IResolver _resolver;

         [SetUp]
         public void SetUp()
         {
             _registry = RegistryFactory.GetRegistry();
             _registry.Register<Module>();
             _registry.Register<BookQueue.Module>();
             _registry.Register<ManheimQueue.Module>();
             _registry.Register<Client.DomainModel.Module>();

             _registry.Register<IDataSessionManager, DataSessionManager>(ImplementationScope.Shared);
             _registry.Register<ICache, WebCache>(ImplementationScope.Shared);
             _registry.Register<VehicleValuationGuide.DomainModel.Manheim.Model.Soap.Module>();
         }


         [Test]
         public void UpdateMMRAveragePriceFromQueueTest()
         {
             ProcessManheimQueue _queue = new ProcessManheimQueue();
             _queue.UpdateMMRAveragePriceFromQueue(10);
         }

         [Test]
         public void PushPeriodicDataToQueueTest()
         {
             ProcessManheimQueue _queue = new ProcessManheimQueue();
             _queue.PushPeriodicDataToQueue(1);
         }

         [Test]
         public void UpdateAveragePriceTest()
         {
             ProcessManheimQueue _queue = new ProcessManheimQueue();
             MMRInventoryRequest request = new MMRInventoryRequest() { InventoryId = 34390923, IsDbUpdateRequired = false, MID = "201106957601144", RegionCode = "NA" };
             MMRResult result = _queue.UpdateAveragePrice(request);
         }

         [Test]
         public void PurgeSuccessQueueTest()
         {
             ProcessManheimQueue _queue = new ProcessManheimQueue();
             _queue.PurgeSuccessQueue(20);
         }

        [Test]
         public void GetPriceWithVin()
         {
             SoapService service=new SoapService();
             ICredentials cred = new NetworkCredential("1stlkwebservice", "1stlkwebservice");
             IList<MMRInventoryRequest> mulitMidRequests=new List<MMRInventoryRequest>();
             mulitMidRequests.Add(new MMRInventoryRequest() { MID = "201206902809948", Mileage = 23757, VehicleYear = 2012 });
             mulitMidRequests.Add(new MMRInventoryRequest() { MID = "200101462781106", Mileage = 191517, VehicleYear = 2001 });
             object priceData = service.Query(cred, 2012, "201206902809948", 23757);

             object multipriceData = service.Query(cred, mulitMidRequests,"NA",false);
         }

         private class WebCache : ICache
         {
             private readonly Cache _cache;

             public WebCache()
             {
                 _cache = HttpContext.Current.Cache;
             }

             public object Add(string key, object value, DateTime absoluteExpiration, TimeSpan slidingExpiration)
             {
                 return _cache.Add(
                     key,
                     value,
                     null,
                     absoluteExpiration,
                     slidingExpiration,
                     CacheItemPriority.Normal,
                     null);
             }

             public object Get(string key)
             {
                 return _cache.Get(key);
             }

             public object Remove(string key)
             {
                 return _cache.Remove(key);
             }
         }
    }

     
}
