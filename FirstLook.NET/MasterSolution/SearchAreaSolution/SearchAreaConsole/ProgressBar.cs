using System.Globalization;

namespace FirstLook.SearchArea.Console
{
    /// <summary>
    /// A simple class for displaying progress through a loop to the console. When predefined thresholds are hit
    /// (i.e. we have progressed enough to warrant a display update), an updated message is displayed to console.
    /// </summary>
    public class ProgressBar
    {        
        private long            _counter;           // Counts from 0 to the total number of elements.
        private long            _incrementCounter;  // Counts from 0 to the increment total.
        private int             _percentDone;       // Counts from 0 to 100.
        private readonly string _message;           // Message to be displayed.
        private readonly long   _numItems;          // Total number of elements.
        private readonly long   _increment;         // Number of elements per increment.
        private readonly int    _percentIncrement;  // How many percent to let by between display updates.                

        #region Initialization

        /// <summary>
        /// Constructs a progress bar with the default increment of 1%.
        /// </summary>
        /// <param name="message">Message to be displayed</param>
        /// <param name="numberItems">Total number of items we are progressing through.</param>
        public ProgressBar(string message, long numberItems)
        {            
            _message = message;
            _numItems = numberItems;
            _percentIncrement = 1;
            _increment = (long)(_numItems * 0.01f);
            Print();
        }

        /// <summary>
        /// Constructs a progress bar with a custom percent increment.
        /// </summary>
        /// <param name="message">Message to be displayed</param>
        /// <param name="numberItems">Total number of items we are progressing through.</param>
        /// <param name="percentIncrement">The percent increment at which we will update the console.</param>
        public ProgressBar(string message, long numberItems, int percentIncrement)
        {            
            _message = message;
            _numItems = numberItems;
            _percentIncrement = percentIncrement;
            _increment = (long)(_numItems * (percentIncrement / 100f));
            Print();
        }

        #endregion

        #region Update

        /// <summary>
        /// Updates all counters. Call this on every iteration of the loop.
        /// If we hit an increment, or if we have progressed through all elements, print to console.
        /// </summary>
        /// <remarks>
        /// Only actually display if we've explicitly marked this as a console application.
        /// </remarks>
        public void Update()
        {
            _counter++;
            _incrementCounter++;

            if (_incrementCounter == _increment)
            {
                _percentDone += _percentIncrement;
                Print();                
                _incrementCounter = 0;    
            }
            else if (_counter == _numItems)
            {
                Print();                
            }
        }        

        #endregion        

        #region Display

        /// <summary>
        /// Display the progress message to the console.
        /// </summary>
        /// <remarks>
        /// Only actually display if we've explicitly marked this as a console application.
        /// </remarks>
        private void Print()
        {                        
            System.Console.Write("\r{0}: {1}% [{2} / {3}]", _message, _percentDone, 
                                                            _counter.ToString("#,0", CultureInfo.InvariantCulture), 
                                                            _numItems.ToString("#,0", CultureInfo.InvariantCulture));
        }

        /// <summary>
        /// Display a new line to end the progress.
        /// </summary>
        /// <remarks>
        /// Only actually display if we've explicitly marked this as a console application.
        /// </remarks>
        public static void Finish()
        {
            System.Console.WriteLine("");
        }

        #endregion
    }
}
