using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace FirstLook.SearchArea.Console
{
    /// <summary>
    /// Generates and maintains performance characteristics of this application.
    /// </summary>
    public class Statistics
    {
        private readonly DateTime _startTime;            // Time when execution started.
        private DateTime          _endTime;              // Time when execution stopped.
        private TimeSpan          _duration;             // Duration of execution.
        private bool              _isGenerated;          // Generated statistics yet? Set before calling Print().
        private int               _zipCountInput;        // Number of zip codes in the output file.
        private int               _zipCountOutput;       // Number of zip codes in the output file.        
        private int               _rectangleCount;       // Number of rectangles in output file.
        private int               _maxRectsPerBucket;    // Max. # of rectangles for a zip code and distance bucket.
        private int               _minRectsPerBucket;    // Min. # of rectangles for a zip code and distance bucket.
        private double            _avgRectsPerBucket;    // Avg. # of rectangles for a zip code and distance bucket.
        private readonly string   _inputFilePath;        // Filepath to input file for counting zip codes.
        private readonly string   _outputFilePath;       // Filepath to output file for generating statistics.
        private readonly string   _statisticsFilePath;   // Filepath to our statistics file.        
        private readonly int      _subdivisionLimit;     // How many times to subdivide rectangles.
        private readonly Dictionary<int, byte> _buckets; // Number of different distance buckets.

        #region Properties

        public int LocationsInInput
        {
            get
            {
                return _zipCountInput;
            }
        }

        public int LocationsInOutput
        {
            get
            {
                return _zipCountOutput;
            }
        }

        public int MaximumRectanglesPerBucket
        {
            get
            {
                return _maxRectsPerBucket;
            }
        }

        public int MinimumRectanglesPerBucket
        {
            get
            {
                return _minRectsPerBucket;
            }
        }

        public double AverageRectanglesPerBucket
        {
            get
            {
                return _avgRectsPerBucket;
            }
        }

        public int TotalRectangles
        {
            get
            {
                return _rectangleCount;
            }
        }

        public TimeSpan Duration
        {
            get
            {
                return _duration;
            }
        }

        public DateTime StartOfExecution
        {
            get
            {
                return _startTime;
            }
        }

        public DateTime EndOfExecution
        {
            get
            {
                return _endTime;
            }
        }

        #endregion

        #region Initialization

        /// <summary>
        /// Initialize this statistics object. Start time for execution starts now.
        /// </summary>
        public Statistics(string statisticsFilePath, string inputFilePath, string outputFilePath, int subdivisionLimit)
        {
            _startTime          = DateTime.Now;                                    
            _maxRectsPerBucket  = int.MinValue;
            _minRectsPerBucket  = int.MaxValue;                           
            _inputFilePath      = inputFilePath;
            _outputFilePath     = outputFilePath;
            _subdivisionLimit   = subdivisionLimit;
            _buckets            = new Dictionary<int, byte>();
            _statisticsFilePath = statisticsFilePath;
        }

        #endregion

        #region Generation

        /// <summary>
        /// Generate statistics based on an output.csv. Ends execution time now. Automatically writes results to file.
        /// </summary>        
        public void Generate()
        {   
            Generate(true);
        }

        /// <summary>
        /// Generate statistics based on an output.csv. Ends execution time now. Only writes to file if explicitly
        /// told to.
        /// </summary>
        /// <param name="writeToFile">Should results be written to file?</param>
        public void Generate(bool writeToFile)
        {
            _endTime = DateTime.Now;
            _duration = _endTime - _startTime;

            TextReader reader;

            // Determine the number of zip codes in input file. Skips header info on first line.
            System.Console.Write("Preprocessing input file for statistics...");
            using (reader = new StreamReader(File.OpenRead(_inputFilePath)))
            {
                reader.ReadLine();
                while (reader.ReadLine() != null)
                {
                    _zipCountInput++;
                }
            }
            System.Console.Write("\rPreprocessing input file for statistics...done\n");

            // Determine how many entries this file has. Skips header info on first line.
            System.Console.Write("Preprocessing output file for statistics...");
            using (reader = new StreamReader(File.OpenRead(_outputFilePath)))
            {
                reader.ReadLine();
                while (reader.ReadLine() != null)
                {
                    _rectangleCount++;
                }
            }
            System.Console.Write("\rPreprocessing output file for statistics...done\n");

            // Now that we now how many entries there are in the output file, we can display a progress bar.
            ProgressBar progressBar = new ProgressBar("Generating statistics", _rectangleCount);

            string previousZip = "";
            int previousBucket = 0;
            int currentCount = 0;

            // Read in the output file and compile statistics.
            using (reader = new StreamReader(File.OpenRead(_outputFilePath)))
            {
                reader.ReadLine(); // Skip header info.

                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    string[] data = line.Split(',');

                    int currentBucket = int.Parse(data[1], CultureInfo.InvariantCulture);
                    string currentZip = data[0];

                    // We're looking at a new location or a new distance bucket.
                    if (currentZip != previousZip || currentBucket != previousBucket)
                    {
                        // Compile data on number of rectangles and counts and such.
                        if (currentZip != previousZip)
                        {
                            _zipCountOutput++;
                        }
                        if (currentCount > _maxRectsPerBucket)
                        {
                            _maxRectsPerBucket = currentCount;
                        }
                        else if (currentCount < _minRectsPerBucket)
                        {
                            _minRectsPerBucket = currentCount;
                        }

                        // Add to list of buckets we've seen.
                        if (!_buckets.ContainsKey(currentBucket))
                        {
                            _buckets.Add(currentBucket, 0);
                        }

                        currentCount = 0;
                        previousBucket = currentBucket;
                        previousZip = currentZip;
                    }

                    currentCount++;
                    progressBar.Update();
                }

                ProgressBar.Finish();

                _avgRectsPerBucket = (double)_rectangleCount / (_zipCountOutput * _buckets.Keys.Count);
                _isGenerated = true;
                Print();

                if (writeToFile)
                {
                    Write();                    
                }                
            }
        }

        #endregion

        #region Outputting

        /// <summary>
        /// Print previously generated statistics.
        /// </summary>
        public void Print()
        {
            if (!_isGenerated)
            {
                throw new InvalidOperationException("Cannot print statistics before they've been generated.");
            }
            
            System.Console.WriteLine("\n[Statistics]");
            System.Console.WriteLine("  total rectangles:     {0}", _rectangleCount.ToString("#,0", CultureInfo.InvariantCulture));
            System.Console.WriteLine("  avg rects per bucket: {0:#.##}", _avgRectsPerBucket);
            System.Console.WriteLine("  max rects per bucket: {0}", _maxRectsPerBucket);
            System.Console.WriteLine("  min rects per bucket: {0}", _minRectsPerBucket);
            System.Console.WriteLine("  execution time:       {0} hours {1} minutes {2} seconds", _duration.Hours, _duration.Minutes, _duration.Seconds);            
        }
        
        /// <summary>
        /// Write our statistics out to file.
        /// </summary>
        public void Write()
        {            
            using (TextWriter writer = new StreamWriter(_statisticsFilePath, true))
            {
                writer.WriteLine("STATISTICS");
                writer.WriteLine("start time,{0}", _startTime);
                writer.WriteLine("end time,{0}", _endTime);
                writer.WriteLine("subdivision limit,{0}", _subdivisionLimit);                
                writer.WriteLine("duration,{0}h{1}m{2}s", _duration.Hours, _duration.Minutes, _duration.Seconds);
                writer.WriteLine("total rectangles,{0}", _rectangleCount);
                writer.WriteLine("avg rects per bucket,{0:#.##}", _avgRectsPerBucket);
                writer.WriteLine("max rects per bucket,{0}", _maxRectsPerBucket);
                writer.WriteLine("min rects per bucket,{0}", _minRectsPerBucket);                                
                writer.WriteLine();
            }
        }

        #endregion
    }
}
