using System;
using System.Collections.Generic;
using System.Globalization;
using FirstLook.SearchArea.DomainModel;

namespace FirstLook.SearchArea.Console
{
    public static class Program
    {
        #region Default Values

        public const string      InputFile        = @"..\..\..\SearchAreaDomainModel_Data\Locations.csv";
        public const string      OutputFile       = @"..\..\..\SearchAreaDomainModel_Data\Output.csv";
        public const string      StatisticsFile   = @"..\..\..\SearchAreaDomainModel_Data\Statistics.csv";
        public const int         SubdivisionLimit = 4;
        static readonly string[] BucketDistances  = new[] {"10","25","50","75","100","150","250","500","750","1000"};

        #endregion        

        /// <summary>
        /// Parse command-line arguments.
        /// </summary>
        /// <exception cref="ArgumentException">It is an exception to receive unexpected arguments.</exception>
        /// <param name="args">Command-line arguments.</param>        
        /// <returns>Dictionary of parameter name to parameter values.</returns>
        static Dictionary<string, object> ParseArgs(IEnumerable<string> args)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();

            foreach (string argument in args)
            {
                string[] data = argument.Split('=');
                switch (data[0])
                {
                    // Values for the distance of each bucket.
                    case "--bucket-distances":
                        parameters["bucketDistances"] = data[1].Split(',');
                        break;

                    // How many times will we subdivide rectangles?
                    case "--subdivision-limit":
                        parameters["subdivisionLimit"] = int.Parse(data[1], CultureInfo.InvariantCulture);
                        break;

                    // Where is the input file?
                    case "--input-file":
                        parameters["inputFile"] = data[1];
                        break;

                    // Where is the output file?
                    case "--output-file":
                        parameters["outputFile"] = data[1];
                        break;

                    // Where is the statistics file?
                    case "--statistics-file":
                        parameters["statisticsFile"] = data[1];
                        break;

                    // Unknown arguments are fatal errors.
                    default:
                        throw new ArgumentException("Unrecognized command-line argument.");
                }
            }
            return parameters;
        }                                   

        /// <summary>
        /// <para>Load the file <value>Location.csv</value> and generate bounding rectangles for each zip code at the
        /// following radii (in miles)</para>
        /// <list type="bullet">
        /// <item>10</item>
        /// <item>25</item>
        /// <item>50</item>
        /// <item>75</item>
        /// <item>100</item>
        /// <item>150</item>
        /// <item>250</item>
        /// <item>500</item>
        /// <item>750</item>
        /// <item>1000</item>
        /// </list>
        /// <para>The result of this computation is a comma delimited flat file whose columns are as follows.</para>
        /// <list type="table">
        /// <term>ZIP</term><description>The ZIP at the center of the circle</description>
        /// <term>Distance</term><description>The radius as an integer</description>
        /// <term>Lat1</term><description>The latitude of the upper left corner of the bounding rectangle.</description>
        /// <term>Long1</term><description>The longitude of the upper left corner of the bounding rectangle.</description>
        /// <term>Lat2</term><description>The latitude of the lower right corner of the bounding rectangle.</description>
        /// <term>Long2</term><description>The longitude of the lower right corner of the bounding rectangle.</description>
        /// </list>
        /// </summary>
        /// <param name="args">Command-line parameters.</param>
        /// <returns>0 on success, -1 on failure.</returns>
        static int Main(string[] args)
        {            
            try
            {
                Dictionary<string, object> parameters = ParseArgs(args);                               

                // Read input file, initialize all locations.
                LocationCollection locations = 
                    new LocationCollection(parameters.ContainsKey("inputFile") ? (string)parameters["inputFile"] : InputFile);

                // Output file.
                OutputCollection outputEntries = 
                    new OutputCollection(parameters.ContainsKey("outputFile") ? (string)parameters["outputFile"] : OutputFile);

                // Distance buckets.
                DistanceBucketCollection distanceBuckets = 
                    new DistanceBucketCollection(parameters.ContainsKey("bucketDistances") ? (IEnumerable<string>)parameters["bucketDistances"] : BucketDistances);

                // Subdivision limit for our rectangles.
                Rectangle.SubdivisionLimit = parameters.ContainsKey("subdivisionLimit") ? (int)parameters["subdivisionLimit"] : SubdivisionLimit;

                // Stat collection.
                Statistics stats =
                    new Statistics(parameters.ContainsKey("statisticsFile") ? (string)parameters["statisticsFile"] : StatisticsFile, 
                                   locations.FilePath, outputEntries.FilePath, Rectangle.SubdivisionLimit);

                // Print some welcome / parameter info.
                System.Console.WriteLine("[Zip Code Prototype]");
                System.Console.WriteLine("  start time:\t\t{0}", stats.StartOfExecution);
                System.Console.WriteLine("  distance buckets:\t{0}", distanceBuckets);
                System.Console.WriteLine("  subdivision limit:\t{0}\n", Rectangle.SubdivisionLimit);

                // Set up a progress bar for display.
                ProgressBar progressBar = new ProgressBar("Processing zip codes", locations.Count);                

                // Execute!
                foreach (Location currentLocation in locations)
                {
                    // Recenter all distance buckets around the current location.
                    distanceBuckets.Initialize(currentLocation.Coordinate);

                    // Examine each location, and drop it in a distance bucket if appropriate.
                    foreach (Location location in locations)
                    {
                        if (currentLocation == location)
                        {
                            continue;
                        }
                        distanceBuckets.DropInBucket(location.Coordinate);
                    }

                    // All locations have now been categorized by distance bucket. Now do the hard part: determine 
                    // the bounding rectangles for each bucket. 
                    foreach (DistanceBucket bucket in distanceBuckets)
                    {
                        IList<Rectangle> boundingRectangles = bucket.CalculateBoundingRectangles();
                        outputEntries.AddRectangles(currentLocation.ZipCode, bucket.Distance, boundingRectangles);
                    }

                    // Write output to file, and clear out that memory.
                    outputEntries.WriteToFile();
                    outputEntries.Clear();

                    progressBar.Update();
                }
                ProgressBar.Finish();
                outputEntries.Close();

                // Display statistics and execution timing info.                
                stats.Generate();  
            }
            catch (Exception exception)
            {
                System.Console.WriteLine(exception.Message);                
                return -1;
            }
            return 0;
        }     
    }
}