using System;
using System.Collections.Generic;
using System.IO;

namespace FirstLook.SearchArea.DomainModel
{
    /// <summary>
    /// The collection of all output entries to be written to the output csv.
    /// </summary>
    public class OutputCollection : List<OutputEntry>, IDisposable
    {        
        private readonly string     _filePath;    // Path to the output file.             
        private readonly TextWriter _writer;      // Ouput writer objects.
        private int                 _outputCount; // Number of items written to the file.        
        #if BACKUP
        private readonly string     _backupPath;  // To prevent output files from being overwritten.
        #endif

        #region Properties

        public string FilePath
        {
            get
            {
                return _filePath;
            }
        }

        public int OutputCount
        {
            get
            {
                return _outputCount;
            }
        }

        #endregion

        #region Initialization

        /// <summary>
        /// Will check to see if a file with the given path and name exists and, if so, will try to delete it.
        /// Inability to delete this file will be a fatal exception. Will also create a filepath to a backup file
        /// if we've turned the backup ability on.
        /// </summary>        
        /// <param name="filePath">Path to and name for the output file.</param>
        public OutputCollection(string filePath)
        {                        
#if BACKUP
            DateTime now = DateTime.Now;
            _backupPath = filePath.Substring(0, filePath.LastIndexOf('.')) + " " +
                          now.Year + "." + now.Month  + "." + now.Day      + " " + 
                          now.Hour + "h" + now.Minute + "m" + now.Second   + "s.csv";
#endif
            _filePath = filePath;

            // If the output file already exists, off with its head.
            if (File.Exists(_filePath))
            {                
                File.Delete(_filePath);
            }            

            // Open the output file and write the header info.
            _writer = new StreamWriter(File.OpenWrite(_filePath));
            _writer.WriteLine("ZIP,Distance,Top Left - Lat,Top Left - Lon,Bottom Right - Lat,Bottom Right - Lon");
        }        

        /// <summary>
        /// Make sure we close our stream writer object.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// FxCop made me write this. I do what FxCop says, or fear its wrath.
        /// </summary>
        /// <param name="cleanEverything"></param>
        protected virtual void Dispose(bool cleanEverything)
        {
            Close();            
        }

        /// <summary>
        /// Close the file we've got open and make a backup of it so that it's not overwritten on the next run if 
        /// we've turned backups on.
        /// </summary>
        public void Close()
        {
            _writer.Close();
        #if BACKUP
            File.Copy(_filePath, _backupPath, true);
        #endif
        }

        #endregion

        /// <summary>
        /// Add a collection of bounding rectangles for a given zip code and distance bucket.
        /// </summary>
        /// <param name="zipCode">Zip code for the current location.</param>
        /// <param name="bucketDistance">Distance bucket radius we are currently looking at.</param>
        /// <param name="rectangles">Bounding rectangles for this location + distance bucket.</param>
        public void AddRectangles(string zipCode, int bucketDistance, IList<Rectangle> rectangles)
        {
            foreach (Rectangle rectangle in rectangles)
            {
                Add(new OutputEntry(zipCode, bucketDistance, rectangle));
            }
        }

        /// <summary>
        /// Will write the contents of this list to file. All exceptions are treated as fatal.
        /// </summary>
        /// <exception cref="Exception">A new fatal exception is thrown for any caught exception.</exception>
        public void WriteToFile()
        {
            foreach (OutputEntry outputEntry in this)
            {
                _outputCount++;
                _writer.WriteLine(outputEntry);                
            }
            _writer.Flush();            
        }                
    }
}
