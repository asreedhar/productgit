using System;

namespace FirstLook.SearchArea.DomainModel
{
    /// <summary>
    /// The location of a US zip code, defined by its zip code value and its latitude / longitude coordinate.
    /// </summary>    
	public class Location : IComparable
    {
        #region Properties

        public string ZipCode { get; set; }

        public Coordinate Coordinate { get; set; }

        #endregion

        #region Initialization

        /// <summary>
        /// Creates a location object with the given zip code and coordinates.
        /// </summary>
        /// <param name="zipCode">The zip code that defines this location.</param>
        /// <param name="coordinate">The coordinate value that represents this location.</param>
        public Location(string zipCode, Coordinate coordinate)
		{
			ZipCode = zipCode;
            Coordinate = coordinate;            
        }

        #endregion
       
        #region Misc. Overrides

        /// <summary>
        /// Compare this location to another. Comparison only cares about coordinate values.
        /// </summary>
        /// <param name="obj">Other location object to compare agaisnt.</param>
        /// <returns>Return value of the coordinate version of <code>CompareTo()</code>.</returns>
        public int CompareTo(Object obj)
        {
            Location otherLocation = obj as Location;
            if (otherLocation == null)
            {
                throw new ArgumentException("Object is not a Location");
            }
            return Coordinate.CompareTo(otherLocation.Coordinate);
        }

        /// <summary>
        /// Are two locations equal?
        /// </summary>
        /// <param name="obj">Other location to compare.</param>
        /// <returns>True if the two locations have the same zipcodes and coordinates.</returns>
        public override bool Equals(object obj)
        {
            Location otherLocation = obj as Location;
            if (otherLocation == null)
            {
                return false;
            }
            return this == otherLocation;
        }

        /// <summary>
        /// Get the hashcode for this object.
        /// </summary>
        /// <returns>Integer hashcode for this object.</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #endregion

        #region Overloaded Operators

        /// <summary>
        /// Are two locations equal? Compares zipcodes and coordinates.
        /// </summary>
        /// <param name="firstLocation">Location to compare.</param>
        /// <param name="secondLocation">Location to compare.</param>
        /// <returns>True if the zipcodes and coordinates are equal; false otherwise. If either location is null,
        /// returns false.</returns>
        public static bool operator == (Location firstLocation, Location secondLocation)
        {
            if ((object)firstLocation == null || (object)secondLocation == null)
            {
                return false;
            }
            return firstLocation.ZipCode == secondLocation.ZipCode &&
                   firstLocation.Coordinate == secondLocation.Coordinate;
        }

        /// <summary>
        /// Are two locations not equal? Compares zipcodes and coordinates.
        /// </summary>
        /// <param name="firstLocation">Location to compare.</param>
        /// <param name="secondLocation">Location to compare.</param>
        /// <returns>True if the zipcodes and coordinates are inequal; false otherwise. If either location is null,
        /// returns false.</returns>
        public static bool operator != (Location firstLocation, Location secondLocation)
        {
            if ((object)firstLocation == null || (object)secondLocation == null)
            {
                return false;
            }
            return firstLocation.ZipCode != secondLocation.ZipCode ||
                   firstLocation.Coordinate != secondLocation.Coordinate;
        }

        /// <summary>
        /// Is one location "less than" another? This is entirely meaningless, but FxCop wants me to implement it
        /// because FxCop is the digitized reincarnation of Hitler.        
        /// </summary>
        /// <param name="firstLocation">Location to compare.</param>
        /// <param name="secondLocation">Location to compare.</param>
        /// <returns>Who cares, really? This is meaningless.</returns>
        public static bool operator < (Location firstLocation, Location secondLocation)
        {
            if ((object)firstLocation == null || (object)secondLocation == null)
            {
                return false;
            }
            return firstLocation.Coordinate < secondLocation.Coordinate;
        }

        /// <summary>
        /// Is one location "greater than" another? This is entirely meaningless, but FxCop wants me to implement it
        /// because FxCop is the digitized reincarnation of Hitler.        
        /// </summary>
        /// <param name="firstLocation">Location to compare.</param>
        /// <param name="secondLocation">Location to compare.</param>
        /// <returns>Who cares, really? This is meaningless.</returns>
        public static bool operator > (Location firstLocation, Location secondLocation)
        {
            if ((object)firstLocation == null || (object)secondLocation == null)
            {
                return false;
            }
            return firstLocation.Coordinate > secondLocation.Coordinate;
        }

        #endregion
    }
}
