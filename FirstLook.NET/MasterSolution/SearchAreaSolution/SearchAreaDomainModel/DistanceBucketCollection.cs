using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace FirstLook.SearchArea.DomainModel
{
    /// <summary>
    /// A collection of distance buckets, and the functionality for filling them and determining the bounding
    /// rectangles that approximate them.
    /// </summary>
    public class DistanceBucketCollection : List<DistanceBucket>
    {                  
        #region Initialization

        /// <summary>
        /// Create the distance buckets that we will use in processing zip codes. The number of distance buckets and
        /// their distance values are read from the command-line arguments. Buckets are sorted by distance.
        /// </summary>           
        /// <exception cref="ArgumentException">The distance bucket values passed in cannot be null.</exception>
        /// <param name="bucketDistances">The distances of each bucket to create. Will be sorted.</param>
        public DistanceBucketCollection(IEnumerable<string> bucketDistances)
        {      
            if (bucketDistances == null)
            {
                throw new ArgumentException("Cannot initialize the distance buckets with null distanes.");
            }
        
            // Sort the distance bucket values.
            SortedList<int, int> distanceValues = new SortedList<int, int>();
            foreach (string distance in bucketDistances)
            {
                distanceValues.Add(int.Parse(distance, CultureInfo.InvariantCulture), 0);
            }            

            // Done in a separate loop b/c above loop will sort its entries on each Add.
            foreach (int distanceVal in distanceValues.Keys)
            {
                Add(new DistanceBucket(distanceVal));
            }
        }

        /// <summary>
        /// (Re-)Initializes all distance buckets around the new center coordinate. Will empty all buckets,
        /// provide them the new center coordinate, and then have them establish their preliminary bounding
        /// rectangles.        
        /// </summary>        
        /// <param name="newCenter">Coordinate around which buckets are to be set.</param>
        public void Initialize(Coordinate newCenter)
        {
            foreach (DistanceBucket bucket in this)
            {                
                bucket.Initialize(newCenter);                
            }            
        }

        #endregion
              
        /// <summary>
        /// Compares the given location against the current one and at this location to the applicable distance
        /// bucket (if there is one).        
        /// </summary>
        /// <remarks>
        /// <para>This code is called n^2 times where n is the number of locations in the input file. Given that it
        /// itself loops m times where m is the number of buckets, we're looking at m*n^2. Since we return in the 
        /// inner loop if a coordinate was added, on average we're going to m/2.</para>
        /// <para>So, for 10 distance buckets and 43,000 locations, we're looking at 10 * 43000 * 43000 / 2, which is
        /// approximately 9 BILLION times we call bucket.Coordinates.Add(coordinate).</para>
        /// </remarks>
        /// <param name="coordinate">Coordinate to be put into a bucket.</param>        
        public void DropInBucket(Coordinate coordinate)
        {                  
            foreach (DistanceBucket bucket in this)
            {
                bucket.DropIn(coordinate);                
            }            
        }

        /// <summary>
        /// Display all the distance values of the buckets held by this list.
        /// </summary>
        /// <returns>Bucket distance values, separated by commas.</returns>
        public override string ToString()
        {
            bool moreThanOne = false;
            StringBuilder builder = new StringBuilder();

            foreach (DistanceBucket bucket in this)
            {
                if (moreThanOne)
                {
                    builder.Append(",");
                }
                else
                {
                    moreThanOne = true;
                }
                builder.Append(bucket);
            }
            return builder.ToString();
        }
    }    
}