using System;
using System.Collections.Generic;
using System.Globalization;

namespace FirstLook.SearchArea.DomainModel
{
    /// <summary>
    /// A point on the globe, represented by degree values for latitude and longitude.
    /// </summary>
    /// <remarks>
    /// A coordinate may be marked as bogus as a temporary measure to assist in the creation of bounding
    /// rectangles. "Bogus" means that a coordinate is held by a distance bucket for a given location, but is not
    /// actually within a distance equal to the radius that defines that distance bucket.
    /// </remarks>
    public class Coordinate : IComparable, ICloneable
    {        
        private double      _latitude;          // Latitudinal degrees. Positive = N, negative = S.
        private double      _longitude;         // Longitudinal degrees. Positive = E, negative = W.
        private bool        _bogus;             // Lays outside the bounds of a distance bucket? Temp status.
        public const double EarthRadius = 3959; // Average radius of the earth, in miles.               

        #region Properties

        public double Latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                _latitude = value;
            }
        }

        public double Longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                _longitude = value;
            }
        }

        public bool Bogus
        {
            get
            {
                return _bogus;
            }
            set
            {
                _bogus = value;
            }            
        }

        #endregion        

        #region Initialization

        /// <summary>
        /// Creates a coordinate object with the provided latitude and longitude.
        /// </summary>
        /// <remarks>
        /// A coordinate cannot be created as bogus, as it's determined during execution, and specific to a location.
        /// </remarks>
        /// <param name="latitude">Latitude position of this coordinate.</param>
        /// <param name="longitude">Longitude position of this coordinate.</param>
        public Coordinate(double latitude, double longitude)
        {
            _latitude = latitude;
            _longitude = longitude;            
        }

        #endregion        

        #region Distance Calculations

        /// <summary>
        /// Calculates a latitude and longitude for a given distance (in miles) and a bearing in degrees.
        /// </summary>
        /// <param name="distance">Distance in miles from the current coordinate.</param>
        /// <param name="bearing">Degree bearing from the current coordinate.</param>
        /// <returns>New location at the given distance and bearing from the current coordinate.</returns>
        public Coordinate PointForDistance(double distance, double bearing)
        {            
            double lat1 = DegreesToRadians(_latitude);
            double lon1 = DegreesToRadians(_longitude);
            double brng = DegreesToRadians(bearing);

            double lat2 = Math.Asin(Math.Sin(lat1) * Math.Cos(distance / EarthRadius) +
                                    Math.Cos(lat1) * Math.Sin(distance / EarthRadius) * Math.Cos(brng));

            double lon2 = lon1 + Math.Atan2(Math.Sin(brng) * Math.Sin(distance / EarthRadius) * Math.Cos(lat1),
                                            Math.Cos(distance / EarthRadius) - Math.Sin(lat1) * Math.Sin(lat2));
            lon2 = (lon2 + Math.PI) % (2 * Math.PI) - Math.PI;

            return new Coordinate(RadiansToDegrees(lat2), RadiansToDegrees(lon2));
        }        

        /// <summary>
        /// Find the distance from the current location to the given coordinate, using the Haversine formula.
        /// </summary>
        /// <param name="coordinate">Coordinate to which we calculate the distance.</param>
        /// <returns>Distance in miles.</returns>
        public double DistanceToPoint(Coordinate coordinate)
        {
            double dLat = DegreesToRadians(coordinate.Latitude - _latitude);
            double dLon = DegreesToRadians(coordinate.Longitude - _longitude);

            double a = Math.Pow(Math.Sin(dLat / 2), 2) +
                       Math.Cos(DegreesToRadians(_latitude)) * Math.Cos(DegreesToRadians(coordinate.Latitude)) *
                       Math.Pow(Math.Sin(dLon / 2), 2);

            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

            return EarthRadius * c;
        }

        #endregion

        #region Degree / Radian Conversion

        /// <summary>
        /// Convert a degree value into radians.
        /// </summary>
        /// <param name="degrees">Degree value to convert.</param>
        /// <returns>The radian value for the given parameter.</returns>
        private static double DegreesToRadians(double degrees)
        {
            return degrees * Math.PI / 180.0;
        }

        /// <summary>
        /// Convert a radian value into degrees. Will be a signed value.
        /// </summary>
        /// <param name="radians">Radian value to convert.</param>
        /// <returns>The degree value for the given parameter.</returns>
        private static double RadiansToDegrees(double radians)
        {
            return radians / Math.PI * 180.0;
        }

        #endregion

        #region Comparators

        /// <summary>
        /// Helper for returning a comparer class that will sort coordinates by latitude.
        /// </summary>
        /// <returns>An IComparer class object for sorting by latitude.</returns>
        public static IComparer<Coordinate> SortByLatitude()
        {
            return new SortByLatitudeHelper();
        }

        /// <summary>
        /// Helper for returning a comparer class that will sort coordinates by longitude.
        /// </summary>
        /// <returns>An IComparer class object for sorting by longitude.</returns>
        public static IComparer<Coordinate> SortByLongitude()
        {
            return new SortByLongitudeHelper();
        }

        /// <summary>
        /// Helper for returning a comparer class that will sort coordinates by bogus status.
        /// </summary>
        /// <returns></returns>
        public static IComparer<Coordinate> SortByBogus()
        {
            return new SortByBogusHelper();
        }

        /// <summary>
        /// Helper class for providing to a sorting data structure a method for sorting coordinate objects
        /// by latitude first, longitude second.        
        /// </summary>
        private class SortByLatitudeHelper : IComparer<Coordinate>
        {
            int IComparer<Coordinate>.Compare(Coordinate a, Coordinate b)
            {
                return a.CompareByLatitude(b);
            }
        }

        /// <summary>
        /// Helper class for providing to a sorting data structure a method for sorting coordinate objects
        /// by longitude first, latitude second.        
        /// </summary>
        private class SortByLongitudeHelper : IComparer<Coordinate>
        {
            int IComparer<Coordinate>.Compare(Coordinate a, Coordinate b)
            {
                return a.CompareByLongitude(b);
            }
        }

        /// <summary>
        /// Helper class for providing to a sorting data structure a method for sorting coordinate objects
        /// by bogus status.
        /// </summary>
        private class SortByBogusHelper : IComparer<Coordinate>
        {
            int IComparer<Coordinate>.Compare(Coordinate a, Coordinate b)
            {
                return a.CompareByBogus(b);
            }
            
        }

        /// <summary>
        /// Compare a coordinate object to this one. Will compare by bogus status.
        /// </summary>
        /// <param name="obj">Coordinate object to compare against.</param>
        /// <returns>See CompareByBogus().</returns>
        public int CompareTo(Object obj)
        {
            return CompareByBogus(obj);
        }

        /// <summary>
        /// Compare a coordinate object to this one. Will compare by latitude (from high values to low values)
        /// first, then by longitude (from low value to high values). This comparison is done to help in sorting
        /// from North to South, West to East.
        /// </summary>
        /// <exception cref="ArgumentException">Exception is thrown when invoked with a parameter that is not a 
        /// coordinate object.</exception>
        /// <param name="value">Other coordinate object to compare agaisnt.</param>
        /// <returns>-1 if lat is higher, 1 if lower; if the same, -1 if lon is lower, 1 if lon is higher.</returns>      
        public int CompareByLatitude(Object value)
        {            
            Coordinate otherCoordinate = value as Coordinate;

            if (otherCoordinate == null)
            {
                throw new ArgumentException("Object is not a Coordinate");
            }                        

            if (_latitude != otherCoordinate.Latitude)
            {
                return otherCoordinate.Latitude.CompareTo(_latitude);
            }
            return _longitude.CompareTo(otherCoordinate.Longitude);                                                             
        }

        /// <summary>
        /// Compare a coordinate object to this one. Will compare by longitude (from low values to high values)
        /// first, then by latitude (from high values to low values). This comparison is done to help in sorting
        /// from West to East, North to South.
        /// </summary>
        /// <exception cref="ArgumentException">Exception is thrown when invoked with a parameter that is not a 
        /// coordinate object.</exception>
        /// <param name="value">Other coordinate object to compare agaisnt.</param>
        /// <returns>-1 if lon is lower, 1 if higher; if the same, -1 if lat is higher, 1 if lat is lower.</returns>      
        public int CompareByLongitude(Object value)
        {
            Coordinate otherCoordinate = value as Coordinate;

            if (otherCoordinate == null)
            {
                throw new ArgumentException("Object is not a Coordinate");
            }                       

            if (_longitude != otherCoordinate.Longitude)
            {
                return _longitude.CompareTo(otherCoordinate.Longitude);

            }            
            return otherCoordinate.Latitude.CompareTo(_latitude);                            
        }

        /// <summary>
        /// Compare a coordinate object to this one. Will compare by bogus status, meaning coordinates that are
        /// labeled as 'bogus' will by considered to be first before coordinates that are not 'bogus'. Reason for
        /// this is it will then be quick in a sorted structure to check for the presence of any bogus points (just
        /// look at first item).
        /// </summary>
        /// <remarks>
        /// Never returns '0' to mark that two coordinates are equal with regard to sort order. This should
        /// make it faster to insert a new item, as if an object is bogus, it will be placed first regardless
        /// of the bogus status of the existing first item.
        /// </remarks>       
        /// <exception cref="ArgumentException">Exception is thrown when invoked with a parameter that is not a
        /// coordinate object.</exception>
        /// <param name="value">Other coordinate object to compare against.</param>
        /// <returns>1 if this coordinate is not bogus and the other is, -1 otherwise.</returns>
        public int CompareByBogus(Object value)
        {
            Coordinate otherCoordinate = value as Coordinate;

            if (otherCoordinate == null)
            {
                throw new ArgumentException("Object is not a Coordinate");
            }            

            if (!_bogus && otherCoordinate.Bogus)
            {
                return 1;
            }
            return -1;
        }

        #endregion

        #region Misc. Overrides

        /// <summary>
        /// Gives a csv-formatted string, ready for output to file.
        /// </summary>
        /// <returns>String with coordinate data.</returns>
        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "{0:###.0000},{1:###.0000}", _latitude, _longitude);
        }

        /// <summary>
        /// Create a new Coordinate object with the same latitude and longitude values.
        /// </summary>
        /// <returns>New Coordinate object.</returns>
        public object Clone()
        {
            return new Coordinate(_latitude, _longitude);            
        }

        /// <summary>
        /// Compare two coordinate objects for equality, which is defined as having the same latitude and longitude
        /// values without regard for bogus status.
        /// </summary>
        /// <param name="obj">Other coordinate to compare.</param>
        /// <exception cref="ArgumentException">Cannot compare a coordinate to a non-coordinate object.</exception>
        /// <returns>True if obj is a coordinate that has the same lat and long values; false otherwise.</returns>
        public override bool Equals(object obj)
        {
            Coordinate otherCoordinate = obj as Coordinate;            
            if (otherCoordinate == null)
            {
                return false;
            }
            return this == otherCoordinate;
        }

        /// <summary>
        /// Just returns the base implementation of getting a hash code for this object.
        /// </summary>
        /// <returns>An integer hash code.</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #endregion

        #region Overloaded Operators

        /// <summary>
        /// Are two coordinate objects equal? Compares latitude and longitude values without regard to bogus status.        
        /// </summary>
        /// <param name="firstCoordinate">Coordinate to compare.</param>
        /// <param name="secondCoordinate">Coordinate to compare.</param>
        /// <returns>True if the two coordinates have the same lat and long values; false otherwise. If either
        /// coordinate is null, returns false.</returns>
        public static bool operator == (Coordinate firstCoordinate, Coordinate secondCoordinate)
        {
            if ((object)firstCoordinate == null || (object)secondCoordinate == null)
            {
                return false;
            }
            return firstCoordinate.Latitude == secondCoordinate.Latitude && 
                   firstCoordinate.Longitude == secondCoordinate.Longitude;
        }

        /// <summary>
        /// Are two coordinate objects inequal? Compares latitude and longitude values without regard to bogus status.        
        /// </summary>
        /// <param name="firstCoordinate">Coordinate to compare.</param>
        /// <param name="secondCoordinate">Coordinate to compare.</param>
        /// <returns>True if the two coordinates have different lat or long values; false otherwise. If either
        /// coordinate is null, returns false.</returns>
        public static bool operator != (Coordinate firstCoordinate, Coordinate secondCoordinate)
        {
            if ((object)firstCoordinate == null || (object)secondCoordinate == null)
            {
                return false;
            }
            return firstCoordinate.Latitude != secondCoordinate.Latitude || 
                   firstCoordinate.Longitude != secondCoordinate.Longitude;
        }

        /// <summary>
        /// Is one coordinate greater than another? Doesn't really make sense, and is only implemented because FxCop 
        /// wants me to have it to round out the IComparable interface.        
        /// </summary>
        /// <param name="firstCoordinate">Coordinate to compare.</param>
        /// <param name="secondCoordinate">Coordinate to compare.</param>
        /// <returns>True if the first coordinate is "greater than" the second.  If either coordinate is null, returns 
        /// false.</returns>
        public static bool operator < (Coordinate firstCoordinate, Coordinate secondCoordinate)
        {
            if ((object)firstCoordinate == null || (object)secondCoordinate == null)
            {
                return false;
            }
            return firstCoordinate.Latitude < secondCoordinate.Latitude && 
                   firstCoordinate.Longitude < secondCoordinate.Longitude;
        }

        /// <summary>
        /// Is one coordinate less than another? Doesn't really make sense, and is only implemented because FxCop wants
        /// me to have it to round out the IComparable interface.        
        /// </summary>
        /// <param name="firstCoordinate">Coordinate to compare.</param>
        /// <param name="secondCoordinate">Coordinate to compare.</param>
        /// <returns>True if the first coordinate is "less than" the second.  If either coordinate is null, returns 
        /// false.</returns>
        public static bool operator > (Coordinate firstCoordinate, Coordinate secondCoordinate)
        {
            if ((object)firstCoordinate == null || (object)secondCoordinate == null)
            {
                return false;
            }
            return firstCoordinate.Latitude > secondCoordinate.Latitude &&
                   firstCoordinate.Longitude > secondCoordinate.Longitude;
        }

        #endregion
    }
}
