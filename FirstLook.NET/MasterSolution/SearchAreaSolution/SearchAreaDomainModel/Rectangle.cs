using System;
using System.Collections.Generic;
using System.Globalization;

namespace FirstLook.SearchArea.DomainModel
{
    /// <summary>
    /// A rectangle defined by its top left and bottom right coordinates. Will hold all coordinate objects contained
    /// by this rectangle. May also have children spawned during the subdivision process. How many times we will 
    /// subdivide is provided via the command-line.
    /// </summary>
    /// <remarks>
    /// The <code>_topLeft</code> and <code>_bottoRight</code> coordinates are not held within 
    /// <code>_coordinates</code>; they are descriptors of the shape of the rectangle, not objects held by it.    
    /// </remarks>
    public class Rectangle
    {        
        private readonly Coordinate                    _topLeft;     // Top left of the rectangle.
        private readonly Coordinate                    _bottomRight; // Bottom right of the rectangle.
        private readonly SortedList<Coordinate, byte>  _coordinates; // All coordinates contained in this rectangle. 
                                                                     // The 'byte' is just a placeholder so that I can 
                                                                     // use a sorted list structure.
        private readonly Dictionary<string, Rectangle> _children;    // Subdivided rectangles that belong to this one.
                                                                     // The keys will be the relative position of the 
                                                                     // child (e.g. "bottomLeft").
        private static int _subdivisionLimit;                        // How many times we'll subdivide.       

        #region Properties

        public Coordinate TopLeft
        {
            get
            {
                return _topLeft;
            }
        }

        public Coordinate BottomRight
        {
            get
            {
                return _bottomRight;
            }
        }

        public SortedList<Coordinate, byte> Coordinates
        {
            get
            {
                return _coordinates;
            }
        }

        public static int SubdivisionLimit
        {
            get
            {
                return _subdivisionLimit;
            }
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentException("Cannot set subdivision level to be less than or equal to 0.");
                }
                _subdivisionLimit = value;
            }
        }

        #endregion     
           
        #region Initialization

        /// <summary>
        /// Creates a rectangle object that will span from <code>topLeft</code> to <code>bottomRight</code>.
        /// </summary>        
        /// <exception cref="ArgumentException">Coordinate objects cannot be null.</exception>
        /// <param name="topLeft">Latitude and longitude at the top left of the new rectangle.</param>
        /// <param name="bottomRight">Latitude and longitude at the bottom right of the new rectangle.</param>
        public Rectangle(ICloneable topLeft, ICloneable bottomRight)
        {        
            if (topLeft == null || bottomRight == null)
            {
                throw new ArgumentException("Coordinate parameters cannot be null.");
            }
                         
            _coordinates = new SortedList<Coordinate, byte>(Coordinate.SortByBogus());
            _children    = new Dictionary<string, Rectangle>();

            // Clone the parameters so that we don't affect the caller, who is most likely the parent of this rectangle.
            _topLeft     = (Coordinate)topLeft.Clone();
            _bottomRight = (Coordinate)bottomRight.Clone();

            // Ensure the relative monikers are correct (e.g. that "top left" actually has the highest latitude / 
            // lowest longitude.
            Refit();                                   
        }

        /// <summary>
        /// Creates a rectangle object that will span from <code>topLeft</code> to <code>bottomRight</code>, and
        /// will add any coordinates objects that are bound by this new rectangle.
        /// </summary>        
        /// <param name="topLeft">Latitude and longitude at the top left of the new rectangle.</param>
        /// <param name="bottomRight">Latitude and longitude at the bottom right of the new rectangle.</param>
        /// <param name="coordinates">Coordinate objects to potentially add if bound by this rectangle.</param>
        public Rectangle(ICloneable topLeft, ICloneable bottomRight, SortedList<Coordinate, byte> coordinates) :
            this (topLeft, bottomRight)
        {            
            // Add coordinates if they are contained by this new rectangle.
            Add(coordinates);                        
        }

        /// <summary>
        /// Creates a new rectangle -- technically, a square -- that is centered on the given location. There is no 
        /// need to call <code>Refit()</code> as is done in the other constructors, as we are in control of where the
        /// top left and bottom right coordinates are spatially, and thus don't need to double check it.
        /// </summary>        
        /// <remarks>
        /// Will automatically expand the rectangle until the distance from the center to its sides are at least equal
        /// to the given distance. This is done because the <code>PointForDistance</code> function seems wonky and prone
        /// to cutting one short.
        /// </remarks>
        /// <exception cref="ArgumentException">If the center object is null, or if the distance from center is less
        /// than or equal to 0.</exception>
        /// <param name="center">Coordinate the rectange should be centered on.</param>
        /// <param name="distance">Distance from center to the corners of this square.</param>
        public Rectangle(Coordinate center, double distance)
        {
            if (center == null)
            {
                throw new ArgumentException("Cannot center a rectangle on a null coordinate.");
            }
            if (distance <= 0)
            {
                throw new ArgumentException("Distance from center cannot be less than or equal to 0.");
            }

            _coordinates = new SortedList<Coordinate, byte>(Coordinate.SortByBogus());
            _children = new Dictionary<string, Rectangle>();

            double originalDistance = distance;
            bool sentinel = true;                       

            while (sentinel)
            {
                // Instead of directly calculating the top left and bottom right coordinates, we go about it indirectly
                // by calculating the top and bottom latitudes and the left and right longitudes. Done because other-
                // wise results are curiously off.
                double top    = center.PointForDistance(distance, 0).Latitude;
                double bottom = center.PointForDistance(distance, 180).Latitude;
                double left   = center.PointForDistance(distance, 270).Longitude;
                double right  = center.PointForDistance(distance, 90).Longitude;

                _topLeft     = new Coordinate(top, left);
                _bottomRight = new Coordinate(bottom, right);

                // If, because of the inaccuracies of the distance calculations, our rectangle is not as big as we 
                // want, make it bigger 1% at a time.                
                if (center.DistanceToPoint(new Coordinate(center.Latitude, left)) < originalDistance ||
                    center.DistanceToPoint(new Coordinate(top, center.Longitude)) < originalDistance)
                {
                    distance *= 1.01;                    
                }
                else
                {
                    sentinel = false;
                }                                
            }
        }      

        /// <summary>
        /// When we set our rectangle, there's a chance that the values given for 'top left' and 'bottom right' are
        /// not correct relative to each other, which would screw up our containment tests. So we make sure here that
        /// the top left coordinate has the highest latitude and lowest longitude, and bottom right has the lowest
        /// latitude and highest longitude.        
        /// </summary>        
        private void Refit()
        {
            if (_topLeft.Latitude < _bottomRight.Latitude)
            {
                double temp           = _topLeft.Latitude;
                _topLeft.Latitude     = _bottomRight.Latitude;
                _bottomRight.Latitude = temp;
            }
            if (_bottomRight.Longitude < _topLeft.Longitude)
            {
                double temp            = _bottomRight.Longitude;
                _bottomRight.Longitude = _topLeft.Longitude;
                _topLeft.Longitude     = temp;
            }
        }

        #endregion

        #region Containment Tests

        /// <summary>
        /// Determine whether a given coordinate is contained within this rectangle. Containment defined as "being
        /// within the coordinate boundaries of", and not literally that the coordinate object is held by this 
        /// rectangle's data structures. Use <code>Has()</code> for that.
        /// </summary>
        /// <param name="coordinate">Coordinate to test against this rectangle for containment.</param>
        /// <returns>True if the coordinate is within this rectangle, false otherwise.</returns>
        public bool Contains(Coordinate coordinate)
        {
            return coordinate.Latitude  <= _topLeft.Latitude  && coordinate.Latitude  >= _bottomRight.Latitude &&
                   coordinate.Longitude >= _topLeft.Longitude && coordinate.Longitude <= _bottomRight.Longitude;
        }

        /// <summary>
        /// Does this rectangle contain any of the given coordinates? Containment defined as "being within the 
        /// coordinate boundaries of", and not literally that the coordinate objects are held by this rectangle's 
        /// data structures. Use <code>Has()</code> for that.
        /// </summary>
        /// <param name="coordinates">List of coordinates to test for containment.</param>
        /// <returns>True if any coordinate is contained by this rectangle, false if none are.</returns>
        public bool ContainsAny(IList<Coordinate> coordinates)
        {            
            foreach (Coordinate coordinate in coordinates)
            {
                if (Contains(coordinate))
                {
                    return true;
                }                
            }
            return false;            
        }

        /// <summary>
        /// Does this rectangle contain all of the given coordinates? Containment defined as "being within the 
        /// coordinate boundaries of", and not literally that the coordinate objects are held by this rectangle's 
        /// data structures. Use <code>Has()</code> for that.
        /// </summary>
        /// <param name="coordinates">List of coordinates to test for containment.</param>
        /// <returns>True if all coordinates are contained by this rectangle, false if any aren't.</returns>
        public bool ContainsAll(IList<Coordinate> coordinates)
        {
            foreach (Coordinate coordinate in coordinates)
            {
                if (!Contains(coordinate))
                {
                    return false;
                }
            }
            return true;
        }

        #endregion        

        #region Presence Tests

        /// <summary>
        /// Does this rectangle have a given object? Given that objects are only added to this rectangle if 
        /// contained (bound) by it, this should also mean this coordinate would pass <code>Contains()</code>,
        /// but there's nothing the explicitly tests for this.
        /// </summary>
        /// <param name="coordinate">Object to check if is in <code>_coordinates</code>.</param>
        /// <returns></returns>
        public bool Has(Coordinate coordinate)
        {
            return _coordinates.Keys.Contains(coordinate);
        }

        /// <summary>
        /// Are there any bogus points in this rectangle? Since our list of coordinates is sorted to put bogus points
        /// first, we only have to check if the first coordinate is bogus.
        /// </summary>
        /// <returns>False if there are no bogus (or no points at all), or true if there are.</returns>
        public bool HasBogus()
        {
            if (_coordinates.Count == 0)
            {
                return false;
            }
            return _coordinates.Keys[0].Bogus;
        }

        /// <summary>
        /// Are there any valid (i.e. non-bogus) points in this rectangle? Since our list of coordinates is sorted to
        /// put all bogus points before valid points, we only have to check if the last coordinate is valid.
        /// </summary>
        /// <returns>True if there are any valid points, false otherwise.</returns>
        public bool HasValid()
        {
            if (_coordinates.Count == 0)
            {
                return false;
            }
            return !_coordinates.Keys[_coordinates.Count - 1].Bogus;            
        }

        /// <summary>
        /// Does this rectangle hold any coordinate objects?
        /// </summary>
        /// <returns>True if no objects are held, false otherwise.</returns>
        public bool Empty()
        {
            return _coordinates.Count == 0;
        }

        #endregion   
        
        #region Modifiers

        /// <summary>
        /// Adds a coordinate object if it is contained (bound) by this rectangle.
        /// </summary>
        /// <param name="coordinate">Coordinate object to add.</param>
        /// <returns>True if the coordinate was added, false otherwise.</returns>
        public bool Add(Coordinate coordinate)
        {
            if (Contains(coordinate))
            {
                _coordinates.Add(coordinate, 0);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Adds coordinate objects if they are contained (bound) by this rectangle.        
        /// </summary>
        /// <param name="coordinates">Sorted list of coordinates to add if contained.</param>
        /// <returns>True if any of the coordinates were added, false otherwise.</returns>
        public bool Add(SortedList<Coordinate, byte> coordinates)
        {
            bool returnVal = true;
            foreach (Coordinate coordinate in coordinates.Keys)
            {
                returnVal &= Add(coordinate);
            }
            return returnVal;
        }

        /// <summary>
        /// Adds coordinate objects if they are contained (bound) by this rectangle.
        /// </summary>
        /// <param name="coordinates">List of coordinates to add if contained.</param>
        /// <returns>True if any of the coordinates were added, false otherwise.</returns>
        public bool Add(IList<Coordinate> coordinates)
        {
            bool returnVal = true;
            foreach (Coordinate coordinate in coordinates)
            {
                returnVal &= Add(coordinate);
            }
            return returnVal;
        }

        /// <summary>
        /// Combines the given rectangle to this one, which only means it rearranges the top left and bottom
        /// right markers because that's the only thing that's needed for our end goals. If we were really 
        /// comining these things for realz then we'd combine coordinates and children.
        /// </summary>
        /// <param name="otherRectangle">Rectangle to combine into this one.</param>
        private void Combine(Rectangle otherRectangle)
        {            
            if (_topLeft.Latitude < otherRectangle.TopLeft.Latitude)
            {                
                _topLeft.Latitude = otherRectangle.TopLeft.Latitude;
            }
            if (_topLeft.Longitude > otherRectangle.TopLeft.Longitude)
            {                
                _topLeft.Longitude = otherRectangle.TopLeft.Longitude;
            }
            if (_bottomRight.Latitude > otherRectangle.BottomRight.Latitude)
            {
                _bottomRight.Latitude = otherRectangle.BottomRight.Latitude;
            }
            if (_bottomRight.Longitude < otherRectangle.BottomRight.Longitude)
            {                
                _bottomRight.Longitude = otherRectangle.BottomRight.Longitude;
            }
        }

        #endregion

        #region Subdividers

        /// <summary>
        /// Recursively break this rectangle down into children that include only non-bogus points, or are at the
        /// subdivision limit in which case we don't care whether they have bogus points or not.
        /// </summary>
        /// <param name="subdivisionLevel">How deep into the recursion are we?</param>
        /// <returns>A list of bounding rectangles which approximate a given distance bucket.</returns>
        public IList<Rectangle> Subdivide(int subdivisionLevel)
        {
            List<Rectangle> boundingRectangles = new List<Rectangle>();

            // If somehow this rectangle holds no coordinates (what the? how did that happen?) or just no valid
            // coordinates, then don't return any bounding rectangles.
            if (Empty() || !HasValid())
            {
                return boundingRectangles;
            }            
            
            // If there's no bogus points to this rectangle, or if we've reached our subdivision limit,
            // then don't divide any further and just return this rectangle.
            if (subdivisionLevel == _subdivisionLimit || !HasBogus())
            {
                boundingRectangles.Add(this);
                return boundingRectangles;
            }

            // Otherwise - we subdivide further.                       
            // Break this rectangle into its four children.
            SpawnChildren();

            // Recursively subdivide!
            IList<Rectangle> topLeftChildSubdivisions     = _children["topLeft"    ].Subdivide(subdivisionLevel + 1);
            IList<Rectangle> topRightChildSubdivisions    = _children["topRight"   ].Subdivide(subdivisionLevel + 1);
            IList<Rectangle> bottomLeftChildSubdivisions  = _children["bottomLeft" ].Subdivide(subdivisionLevel + 1);
            IList<Rectangle> bottomRightChildSubdivisions = _children["bottomRight"].Subdivide(subdivisionLevel + 1);

            // Combine! 
            // Attempt to simplify by combining bogus-less rectangles. If all four children brought back 1 child 
            // (meaning they all hit the subdivision limit), just return one rectangle for the lot. Otherwise, try 
            // to combine any two rectangles that only have 1 child and share an edge.            
            if (topLeftChildSubdivisions.Count == 1 && topRightChildSubdivisions.Count == 1 && 
                bottomLeftChildSubdivisions.Count == 1 && bottomRightChildSubdivisions.Count == 1)
            {
                boundingRectangles.Add(this);
            }
            // Top left quad + top right quad?
            else if (topLeftChildSubdivisions.Count == 1 && topRightChildSubdivisions.Count == 1)
            {
                topLeftChildSubdivisions[0].Combine(topRightChildSubdivisions[0]);
                boundingRectangles.Add(topLeftChildSubdivisions[0]);
                boundingRectangles.AddRange(bottomLeftChildSubdivisions);
                boundingRectangles.AddRange(bottomRightChildSubdivisions);
            }
            // Top left quad + bottom left quad?
            else if (topLeftChildSubdivisions.Count == 1 && bottomLeftChildSubdivisions.Count == 1)
            {
                topLeftChildSubdivisions[0].Combine(bottomLeftChildSubdivisions[0]);
                boundingRectangles.Add(topLeftChildSubdivisions[0]);
                boundingRectangles.AddRange(topRightChildSubdivisions);
                boundingRectangles.AddRange(bottomRightChildSubdivisions);
            }
            // Bottom left quad + bottom right quad?
            else if (bottomLeftChildSubdivisions.Count == 1 && bottomRightChildSubdivisions.Count == 1)
            {
                bottomLeftChildSubdivisions[0].Combine(bottomRightChildSubdivisions[0]);
                boundingRectangles.Add(bottomLeftChildSubdivisions[0]);
                boundingRectangles.AddRange(topLeftChildSubdivisions);
                boundingRectangles.AddRange(topRightChildSubdivisions);                
            }
            // Top right quad + bottom right quad?
            else if (topRightChildSubdivisions.Count == 1 && bottomRightChildSubdivisions.Count == 1)
            {
                topRightChildSubdivisions[0].Combine(bottomRightChildSubdivisions[0]);       
                boundingRectangles.Add(topRightChildSubdivisions[0]);
                boundingRectangles.AddRange(topLeftChildSubdivisions);
                boundingRectangles.AddRange(bottomLeftChildSubdivisions);
            }
            // No combinations were possible. Just add what we've got and move on.
            else
            {
                boundingRectangles.AddRange(topLeftChildSubdivisions);
                boundingRectangles.AddRange(topRightChildSubdivisions);
                boundingRectangles.AddRange(bottomLeftChildSubdivisions);
                boundingRectangles.AddRange(bottomRightChildSubdivisions);
            }

            // Our children are no longer necessary, so we may as well clear out that memory.
            _children.Clear();

            return boundingRectangles;
        }

        /// <summary>
        /// Create four equal-sized children from this rectangle, breaking on the center of this rectangle.
        /// </summary>
        public void SpawnChildren()
        {
            Coordinate center = new Coordinate(((_topLeft.Latitude - _bottomRight.Latitude) / 2) + _bottomRight.Latitude,
                                               ((_bottomRight.Longitude - _topLeft.Longitude) / 2) + _topLeft.Longitude);

            _children.Add("topLeft",     new Rectangle(_topLeft, center, _coordinates));
            _children.Add("bottomRight", new Rectangle(center, _bottomRight, _coordinates));
            _children.Add("topRight",    new Rectangle(new Coordinate(_topLeft.Latitude, center.Longitude),
                                                       new Coordinate(center.Latitude, _bottomRight.Longitude),
                                                       _coordinates));
            _children.Add("bottomLeft",  new Rectangle(new Coordinate(center.Latitude, _topLeft.Longitude),
                                                       new Coordinate(_bottomRight.Latitude, center.Longitude),
                                                       _coordinates));
        }        

        #endregion        

        /// <summary>
        /// Gives a csv-formatted string, ready for output to file.
        /// </summary>
        /// <returns>String with coordinate data.</returns>
        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "{0},{1}", _topLeft, _bottomRight);
        }       
    }
}