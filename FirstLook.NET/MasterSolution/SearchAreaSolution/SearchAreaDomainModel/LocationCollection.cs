using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace FirstLook.SearchArea.DomainModel
{
    /// <summary>
    /// A collection of location objects, and the functionality required to read their values from an input csv.
    /// </summary>
	public class LocationCollection : List<Location>
	{
        private readonly string _filePath;

        #region Properties

        public string FilePath
        {
            get
            {
                return _filePath;
            }
        }

        #endregion

        /// <summary>
        /// Parses a location list csv, creating new <code>Location</code> objects for each entry.
        /// </summary>        
        /// <param name="filePath">File system path to the location list csv.</param>
        public LocationCollection(string filePath)
        {                           
            _filePath = filePath;            

            using (TextReader reader = new StreamReader(File.OpenRead(_filePath)))
            {
                // Skip header info.
                reader.ReadLine();

                // Parse all location entries, which have the form <zip, lat, long>.
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    string[] data = line.Split(',');
                    Add(new Location(data[0], new Coordinate(double.Parse(data[1], CultureInfo.InvariantCulture), 
                                                             double.Parse(data[2], CultureInfo.InvariantCulture))));
                }                
            }
        }
	}
}
