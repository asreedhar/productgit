using System;
using System.Collections.Generic;
using System.Globalization;

namespace FirstLook.SearchArea.DomainModel
{
    /// <summary>
    /// Encapsulates a circle with a given radius, around a given coordinate. Has a crude approximation of this circle
    /// in rectangle form (<code>preliminaryRectangle</code> - the square that holds this circle completely).
    /// Additionally holds the coordinates of all locations within <code>_distance</code> from 
    /// <code>_center</code> (and even some that aren't).
    /// </summary>
    /// <remarks>
    /// <para>An important assumption is made regarding the relation of this distance bucket to any others: we assume 
    /// that the next larger bucket is at least sqrt(2) bigger. This is because we assume that all points - bogus or 
    /// not - held by a given distance bucket will automatically be valid points in the next larger bucket.</para>
    /// <para>We know, though, that this assumption is violated twice with the default group of distance buckets.
    /// The 75 mile bucket times sqrt(2) is larger than the 100 mile bucket (likewise with 750mi and 1000mi).</para>
    /// </remarks>
    public class DistanceBucket
    {        
        private readonly int              _distance;             // Radial distance that defines this circle.                
        private Coordinate                _center;               // Center point of this circle.
        private readonly List<Coordinate> _coordinates;          // All coords in this circle (and some that aren't).
        private Rectangle                 _preliminaryRectangle; // Rough approximation of this circle in rect. form.

        #region Properties

        public int Distance
        {
            get
            {
                return _distance;
            }
        }

        public Coordinate Center
        {
            get
            {
                return _center;
            }        
        }

        public Rectangle PreliminaryRectangle
        {
            get
            {
                return _preliminaryRectangle;
            }            
        }

        #endregion                        

        #region Initialization

        /// <summary>
        /// Creates a new bucket object with a given distance threshold, which must be positive. No center coordinate
        /// is given as we will be re-centering this distance bucket for each location in the input file.
        /// </summary>
        /// <exception cref="ArgumentException">Distance must be greater than 0.</exception>
        /// <param name="distance">Radius of the distance this bucket covers.</param>
        public DistanceBucket(int distance)
        {
            if (distance <= 0)
            {
                throw new ArgumentException("Bucket distance must be a positive integer.");
            }

            _distance = distance;                        
            _coordinates = new List<Coordinate>();            
        }

        /// <summary>
        /// <para>(Re-)Initializes this distance bucket around a new center. Will clear out any coordinates previously 
        /// held by this bucket, set the new center, and set the preliminary rectangle around this center.</para>        
        /// </summary>
        /// <exception cref="ArgumentException">Center coordinate cannot be null.</exception>
        /// <param name="center">New center point of this distance bucket.</param>
        public void Initialize(Coordinate center)
        {
            if (center == null)
            {
                throw new ArgumentException("Cannot initialize a distance bucket around a null coordinate.");
            }

            _coordinates.Clear();
            _center = (Coordinate)center.Clone();            
            _preliminaryRectangle = new Rectangle(_center, _distance);
        }        

        #endregion

        /// <summary>
        /// Drop a coordinate into this distance bucket if contained by the preliminary rectangle.
        /// </summary>
        /// <exception cref="ArgumentException">Cannot drop in a null coordinate.</exception>
        /// <param name="coordinate">Coordinate to consider for adding.</param>
        /// <returns>True if the coordinate was added, false otherwise.</returns>
        public bool DropIn(Coordinate coordinate)
        {
            if (coordinate == null)
            {
                throw new ArgumentException("Cannot drop a null coordinate into this distance bucket.");
            }
            if (_preliminaryRectangle.Contains(coordinate))
            {
                _coordinates.Add(coordinate);
                return true;
            }            
            return false;
        }

        /// <summary>
        /// Determines the bounding rectangles that more accurately approximate this distance bucket. A small 
        /// percentage of the time, this is a simple task because there are no bogus coordinates in this bucket.
        /// The majority of the time, however, we do have bogus points and so we'll subdivide our preliminary
        /// rectangle until, as best we can, we've excluded them all.
        /// </summary>
        /// <remarks>
        /// For info on what it means for a coordinate to be bogus, refer to <code>Coordinate</code>.
        /// </remarks>
        /// <returns>A list of new bounding rectangles.</returns>    
        public IList<Rectangle> CalculateBoundingRectangles()
        {            
            bool hasBogusLocations = false;
            bool hasValidLocations = false;

            IList<Rectangle> output = new List<Rectangle>();

            // If the distance from the current location to this one is larger than the allowable distance for this
            // bucket, then mark it as "bogus". Otherwise (re)set this coordinate as valid.
            foreach (Coordinate coordinate in _coordinates)
            {
                if (_center.DistanceToPoint(coordinate) > _distance)
                {
                    hasBogusLocations = true;
                    coordinate.Bogus  = true;
                }
                else
                {
                    hasValidLocations = true;
                    coordinate.Bogus  = false;
                }
            }            
            // If this distance bucket has both valid and bogus locations, then we need to subdivide to get a 
            // collection of smaller rectangles that ideally hold only valid points. This accounts for probably 98%
            // of all situations.
            if (hasValidLocations && hasBogusLocations)
            {
                _preliminaryRectangle.Add(_coordinates);
                output = _preliminaryRectangle.Subdivide(0);                            
            }            
            // Occasionally, we'll have a distance bucket that only has bogus points (i.e. no valid points). In this
            // case we'll return a single square that fits entirely within the distance bucket circle, meaning it will
            // be a square with no valid or bogus points.
            else if (hasBogusLocations)
            {                
                output.Add(new Rectangle(_center, _distance));                
            }
            // Otherwise, we have one of two scenarios: this bucket holds no points; or this bucket holds only valid
            // points. In either case, adding the preliminary bounding rectangle is good enough.            
            else
            {
                output.Add(_preliminaryRectangle);                
            }            
            
            return output;            
        }

        /// <summary>
        /// Return the distance value of this bucket for display.
        /// </summary>
        /// <returns>Distance value as a string.</returns>
        public override string ToString()
        {
            return _distance.ToString(CultureInfo.InvariantCulture);
        }
    }
}
