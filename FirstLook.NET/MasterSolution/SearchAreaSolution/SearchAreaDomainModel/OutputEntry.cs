using System.Globalization;

namespace FirstLook.SearchArea.DomainModel
{
    /// <summary>
    /// A single entry to be written to the output csv. Entries include the zip code, the radius of the distance bucket
    /// and a single bounding rectangle. In the file as a hole, there may be multiple rectangles for a particular
    /// zip code and distance.
    /// </summary>
    public class OutputEntry
    {      
        private readonly string    _zipCode;           // The zip code the following bounding rectangle belongs to.
        private readonly int       _radius;            // The radius the following bounding rectangle accounts for.
        private readonly Rectangle _boundingRectangle; // One (of potentially many) bounding rectangles that approximate
                                                       // the circle defined by _radius around _zipCode.        
        #region Properties

        public string ZipCode
        {
            get
            {
                return _zipCode;
            }
        }

        public int Radius
        {
            get
            {
                return _radius;
            }
        }

        public Rectangle BoundingRectangle
        {
            get
            {
                return _boundingRectangle;
            }
        }

        #endregion

        /// <summary>
        /// Creates a default output object that represents a single entry into a result csv.
        /// </summary>
        /// <remarks>
        /// For a given zip code and radius, there may be multiple bounding rectangles. These will each
        /// have their own output entry object.
        /// </remarks>
        /// <param name="zipCode">Zip code that defines a location.</param>
        /// <param name="radius">The distance bucket for which the bounding rectangle applies.</param>
        /// <param name="boundingRectangle">A rectangle that accounts for at least a portion of this bucket.</param>
        public OutputEntry(string zipCode, int radius, Rectangle boundingRectangle)
        {
            _zipCode = zipCode;
            _radius = radius;
            _boundingRectangle = boundingRectangle;
        }        

        /// <summary>
        /// Convert this object into a string representation, which will be put directly into the output csv.
        /// </summary>
        /// <returns>A comma-separated string, ready to be written to file.</returns>
        public override string ToString()
        {            
            return string.Format(CultureInfo.InvariantCulture, "{0},{1},{2}", _zipCode,_radius, _boundingRectangle);
        }
    }
}
