using System.Collections.Generic;
using System.Globalization;
using System.IO;
using FirstLook.SearchArea.Console;
using NUnit.Framework;

namespace FirstLook.SearchArea.DomainModel
{
    [TestFixture]
    public class SearchTests
    {
        private Statistics _stats;           // Uses a stats object since it maintains details of the execution.                        
        private int[]      _distanceBuckets; // Values for all the distance buckets.        
        LocationCollection       _locations;       // Location objects.
        
        [TestFixtureSetUp]
        public virtual void FixtureSetup()
        {
            System.Console.WriteLine("Initializing test fixture.");                
            _locations       = new LocationCollection(Program.InputFile);                        
            _distanceBuckets = new[] {10, 25, 50, 75, 100, 150, 250, 500, 750, 1000};            
            _stats           = new Statistics(Program.StatisticsFile, Program.InputFile, 
                                              Program.OutputFile,     Program.SubdivisionLimit);            
            _stats.Generate();
        }

        [TestFixtureTearDown]
        public virtual void FixtureTeardown()
        {            
        }

        /// <summary>
        /// Does every location from the input file have at least one resultangle in the output file?
        /// </summary>
        [Test]
        public virtual void AllLocationsAccountedFor()
        {
            System.Console.WriteLine("All locations account for: {0}", 
                _stats.LocationsInInput == _stats.LocationsInOutput);            
            Assert.AreEqual(_stats.LocationsInInput, _stats.LocationsInOutput);            
        }

        /// <summary>
        /// Does every location have at least one rectangle for every distance bucket? Assumes all rectangles are 
        /// written to the output file in sorted order by distance bucket size, ascending. So, 10, 25, 50, etc.
        /// </summary>
        [Test]
        public virtual void AllBucketsForAllLocations()
        {            
            int counter = 0;
            int previousDistance = _distanceBuckets[0];            
            bool allBucketsAccountedFor = true;

            using (TextReader reader = new StreamReader(File.OpenRead(Program.OutputFile)))
            {                
                reader.ReadLine();                
                string line;
                while ((line = reader.ReadLine()) != null)
                {                    
                    int distance = int.Parse(line.Split(',')[1], CultureInfo.InvariantCulture);

                    if (distance == previousDistance)
                    {
                        continue;
                    }
                    
                    if (++counter == 10)
                    {
                        counter = 0;
                    }

                    if (distance != _distanceBuckets[counter])
                    {
                        allBucketsAccountedFor = false;
                        break;
                    }
                    previousDistance = _distanceBuckets[counter];                    
                }
            }

            System.Console.WriteLine("Every location has every bucket: {0}", allBucketsAccountedFor);                
            Assert.AreEqual(allBucketsAccountedFor, true);
        }		

        /// <summary>
        /// Will check every location against every other location and see if ones that are within a certain distance
        /// bucket actually are contained by any of the bounding rectangles. This will memory on the scale of 1GB
        /// for every 1.5 million entries in the output file.
        /// </summary>      
        [Test]
        public virtual void AllValidPointsAccountedFor()
        {            
            int[] distanceBuckets = new[] { 10, 25, 50, 75, 100, 150, 250, 500, 750, 1000 };            
            int   totalCount      = 0;
            int   badCount        = 0;

            // Massive structure that maps distance buckets to every location, and for every distance bucket a group
            // of bounding rectangles. 
            Dictionary<string, Dictionary<string, List<Rectangle>>> boundingRectangles =
                new Dictionary<string, Dictionary<string, List<Rectangle>>>();            
            
            // Populate the massive dictionary structure from the output file.
            using (TextReader reader = new StreamReader(File.OpenRead(Program.OutputFile)))
            {
                // Skip header info.
                reader.ReadLine();

                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    string[] data = line.Split(',');

                    string zip = data[0];
                    string bucket = data[1];

                    // Have we encountered this zipcode before? If not, create it's objects.
                    if (!boundingRectangles.ContainsKey(zip))
                    {
                        boundingRectangles[zip] = new Dictionary<string, List<Rectangle>>();
                    }
                    // Have we encountered this distance bucket for this zipcode before? If not, create objects.
                    if (!boundingRectangles[zip].ContainsKey(bucket))
                    {
                        boundingRectangles[zip][bucket] = new List<Rectangle>();
                    }
                    // Add a new bounding rectangle to the list of all of them.
                    Rectangle boundingRectangle = 
                        new Rectangle(new Coordinate(double.Parse(data[2], CultureInfo.InvariantCulture), 
                                                     double.Parse(data[3], CultureInfo.InvariantCulture)),
                                      new Coordinate(double.Parse(data[4], CultureInfo.InvariantCulture), 
                                                     double.Parse(data[5], CultureInfo.InvariantCulture)));
                    boundingRectangles[zip][bucket].Add(boundingRectangle);
                }
            }                                

            // Analyze every location for every other location for every distance bucket. Will determine the distance
            // between the two locations. If it is within the given distance bucket, then bounding rectangles should
            // contain those locations. If the bounding rectangles do not, this is a problem.
            foreach (Location location in _locations)
            {
                foreach (Location otherLocation in _locations)
                {
                    if (location == otherLocation)
                    {
                        continue;
                    }

                    totalCount++;

                    foreach (int bucket in distanceBuckets)
                    {
                        double distance = location.Coordinate.DistanceToPoint(otherLocation.Coordinate);

                        if (distance < bucket)
                        {
                            bool contained = false;
                            List<Rectangle> rects = 
                                boundingRectangles[location.ZipCode][bucket.ToString(CultureInfo.InvariantCulture)];

                            foreach (Rectangle rectangle in rects)
                            {
                                if (rectangle.Contains(otherLocation.Coordinate))
                                {
                                    contained = true;
                                    break;
                                }
                            }
                            if (!contained)
                            {
                                // We only increment our bad counter if the location should have been contained and was
                                // not contained. We allow a 5% tolerance.
                                if ((bucket - distance) > (bucket * 0.05))
                                {                      
                                    System.Console.WriteLine("{0} {1} {2}", 
                                        location.ZipCode, otherLocation.ZipCode, distance);
                                    badCount++;
                                }
                            }
                        }
                    } // For every distance bucket
                } // For every other location
            } // For every location

            System.Console.WriteLine("[All Valid Points Accounted For?]");
            System.Console.WriteLine("  total points processed: {0}", totalCount);
            System.Console.WriteLine("  total valid points missed: {0}", badCount);
            System.Console.WriteLine("  as percentage of total: {0}", badCount * 100.0 / totalCount);
            
            return;           
        }
	}
}