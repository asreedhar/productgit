﻿using System;

namespace MaxInterfaces
{
    public interface IRunWithCancellation
    {
        void Run(Func<bool> shouldCancel, Func<bool> blockWhileNoMessages);
    }
}
