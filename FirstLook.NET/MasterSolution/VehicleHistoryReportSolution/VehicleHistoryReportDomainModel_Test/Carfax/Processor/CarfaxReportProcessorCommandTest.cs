using NUnit.Framework;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Processor
{
	[TestFixture]
	public class CarfaxReportProcessorCommandTest
	{
		[Test]
		public void TestReportProcessorCommand()
		{
			CarfaxReportProcessorCommand command = CarfaxReportProcessorCommand.NextReportProcessorCommand();

			if (command.HasWork)
			{
				command.Execute();
			}
		}
	}
}
