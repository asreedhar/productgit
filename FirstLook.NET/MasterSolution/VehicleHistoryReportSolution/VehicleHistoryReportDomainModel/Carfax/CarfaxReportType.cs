using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax
{
	[Serializable]
	public enum CarfaxReportType
	{
		Undefined,
		BTC,
		VHR,
		CIP
	}
}
