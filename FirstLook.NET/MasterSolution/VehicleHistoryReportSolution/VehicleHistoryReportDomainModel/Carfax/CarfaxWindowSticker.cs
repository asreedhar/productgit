using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax
{
	[Serializable]
	public enum CarfaxWindowSticker
	{
		Undefined,
		WS1
	}
}
