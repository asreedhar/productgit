using System;
using System.Data;
using System.Threading;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax
{
	[Serializable]
	public class DealerAccount : BusinessBase<DealerAccount>
	{
		#region Business Methods

		private int dealerId;
		private CarfaxAccount account;
		
		public int DealerId
		{
			get { return dealerId; }
		}

		public CarfaxAccount Account
		{
			get { return account; }
		}

		protected override object GetIdValue()
		{
			return DealerId;
		}

		#endregion

		#region Validation

		public override bool IsDirty
		{
			get
			{
				return base.IsDirty || Account.IsDirty;
			}
		}

		public override bool IsValid
		{
			get
			{
				return base.IsValid && Account.IsValid;
			}
		}

		#endregion

		#region Factory Methods

		public static bool Exists(int dealerId)
		{
			ExistsCommand command = new ExistsCommand(dealerId);
			DataPortal.Execute(command);
			return command.Exists;
		}

		public static DealerAccount GetDealerAccount(int dealerId)
		{
			return DataPortal.Fetch<DealerAccount>(new Criteria(dealerId));
		}

		public static DealerAccount NewDealerAccount(int dealerId)
		{
			return new DealerAccount(dealerId);
		}

		private DealerAccount()
		{
			/* force use of factory methods */
		}

		private DealerAccount(int dealerId)
		{
			this.dealerId = dealerId;
			
			account = CarfaxAccount.NewAccount(CarfaxAccountType.Dealer);
		}

		#endregion

		#region Data Access

		public class DataSource
		{
			public CarfaxAccount Select(int dealerId)
			{
				if (Exists(dealerId))
				{
					return GetDealerAccount(dealerId).Account;
				}

				return null;
			}

			public void Insert(int dealerId, string userName, string password, bool active)
			{
				DealerAccount dealerAccount = NewDealerAccount(dealerId);
				dealerAccount.Account.UserName = userName;
				dealerAccount.Account.Password = password;
				dealerAccount.Account.Active = active;
				dealerAccount.Save();
			}

			public void Update(int dealerId, int id, string userName, string password, bool active)
			{
				DealerAccount dealerAccount = GetDealerAccount(dealerId);
				dealerAccount.Account.UserName = userName;
				dealerAccount.Account.Password = password;
				dealerAccount.Account.Active = active;
				dealerAccount.Save();
			}

			public void Delete(int dealerId)
			{
			}
		}

		[Serializable]
		protected class Criteria
		{
			private readonly int dealerId;

			public Criteria(int dealerId)
			{
				this.dealerId = dealerId;
			}

			public int DealerId
			{
				get { return dealerId; }
			}
		}

		[Serializable]
		private class ExistsCommand : CommandBase
		{
			private readonly int dealerId;
			private bool exists;

			public ExistsCommand(int dealerId)
			{
				this.dealerId = dealerId;
			}

			public bool Exists
			{
				get { return exists; }
			}

			[Transactional(TransactionalTypes.Manual)]
			protected override void DataPortal_Execute()
			{
				using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
				{
					connection.Open();

					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = "Carfax.DealerAccount#Exists";
						command.AddParameterWithValue("DealerId", DbType.Int32, true, dealerId);

						using (IDataReader reader = command.ExecuteReader())
						{
							if (reader.Read())
							{
								exists = reader.GetBoolean(reader.GetOrdinal("Exists"));
							}
							else
							{
								exists = false;
							}
						}
					}
				}
			}
		}

		[Transactional(TransactionalTypes.Manual)]
		protected void DataPortal_Fetch(Criteria criteria)
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
			{
				connection.Open();

				using (IDataCommand command = new DataCommand(connection.CreateCommand()))
				{
					command.CommandType = CommandType.StoredProcedure;
					command.CommandText = "Carfax.DealerAccount#Fetch";
					command.AddParameterWithValue("DealerId", DbType.Int32, true, criteria.DealerId);

					using (IDataReader reader = command.ExecuteReader())
					{
						if (reader.Read())
						{
							dealerId = criteria.DealerId;

							account = CarfaxAccount.GetAccount(reader);
						}
						else
						{
							throw new DataException("Missing Dealer information");
						}
					}
				}
			}

			MarkOld();
		}

		[Transactional(TransactionalTypes.Manual)]
		protected override void DataPortal_Insert()
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
			{
				connection.Open();

				using (IDbTransaction transaction = connection.BeginTransaction())
				{
					account.DoInsert(connection, transaction);

					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = "Carfax.DealerAccount#Insert";
						command.Transaction = transaction;

						command.AddParameterWithValue("DealerId", DbType.Int32, false, DealerId);
						command.AddParameterWithValue("AccountId", DbType.Int32, false, account.Id);
						command.AddParameterWithValue("InsertUser", DbType.String, false, Thread.CurrentPrincipal.Identity.Name);

						command.ExecuteNonQuery();
					}

					transaction.Commit();
				}
			}

			MarkOld();
		}

		[Transactional(TransactionalTypes.Manual)]
		protected override void DataPortal_Update()
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
			{
				connection.Open();

				using (IDbTransaction transaction = connection.BeginTransaction())
				{
					account.DoUpdate(connection, transaction);

					transaction.Commit();
				}
			}

			MarkOld();
		}

		#endregion
	}
}