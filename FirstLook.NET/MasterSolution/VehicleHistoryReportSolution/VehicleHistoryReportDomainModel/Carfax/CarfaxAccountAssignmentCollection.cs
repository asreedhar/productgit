using System;
using System.Data;
using Csla;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax
{
	[Serializable]
	public class CarfaxAccountAssignmentCollection : BusinessListBase<CarfaxAccountAssignmentCollection, CarfaxAccountAssignment>
	{
		#region Business Methods

		public CarfaxAccountAssignment Find(int memberId)
		{
			foreach (CarfaxAccountAssignment res in this)
				if (res.MemberId.Equals(memberId))
					return res;
			return null;
		}

		public void AssignTo(int memberId)
		{
			if (!Contains(memberId))
			{
				Add(CarfaxAccountAssignment.NewCarfaxAccountAssignment(memberId));
			}
			else
			{
				throw new InvalidOperationException("Resource already assigned to project");
			}
		}

		public void Remove(int memberId)
		{
			foreach (CarfaxAccountAssignment res in this)
			{
				if (res.MemberId.Equals(memberId))
				{
					Remove(res);
					break;
				}
			}
		}

		public bool Contains(int memberId)
		{
			foreach (CarfaxAccountAssignment project in this)
				if (project.MemberId == memberId)
					return true;
			return false;
		}

		public bool ContainsDeleted(int memberId)
		{
			foreach (CarfaxAccountAssignment project in DeletedList)
				if (project.MemberId == memberId)
					return true;
			return false;
		}

		#endregion

		private CarfaxAccountAssignmentCollection()
		{
			/* force use of factory methods */
		}

		private CarfaxAccountAssignmentCollection(IDataReader reader)
		{
			RaiseListChangedEvents = false;

			while (reader.Read())
			{
				Add(CarfaxAccountAssignment.GetCarfaxAccountAssignment(reader));
			}

			RaiseListChangedEvents = true;
		}

		public static CarfaxAccountAssignmentCollection NewCarfaxAccountAssignments()
		{
			return new CarfaxAccountAssignmentCollection();
		}

		internal static CarfaxAccountAssignmentCollection GetCarfaxAccountAssignments(IDataReader reader)
		{
			return new CarfaxAccountAssignmentCollection(reader);
		}

		internal void DoUpdate(IDbConnection connection, IDbTransaction transaction, CarfaxAccount account)
		{
			RaiseListChangedEvents = false;

			foreach (CarfaxAccountAssignment assignment in DeletedList)
			{
				assignment.DeleteSelf(connection, transaction, account);
			}

			DeletedList.Clear();

			foreach (CarfaxAccountAssignment assignment in this)
			{
				if (assignment.IsNew)
				{
					assignment.Insert(connection, transaction, account);
				}
			}

			RaiseListChangedEvents = true;
		}
	}
}
