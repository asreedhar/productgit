using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax
{
	[Serializable]
	public class SystemAccount : BusinessBase<SystemAccount>
	{
		#region Business Methods
		
		private CarfaxAccount account;

		public CarfaxAccount Account
		{
			get { return account; }
		}

		protected override object GetIdValue()
		{
			return account.Id;
		}

		#endregion

		#region Validation

		public override bool IsDirty
		{
			get
			{
				return base.IsDirty || Account.IsDirty;
			}
		}

		public override bool IsValid
		{
			get
			{
				return base.IsValid && Account.IsValid;
			}
		}

		#endregion

		#region Factory Methods

		public static bool Exists()
		{
			ExistsCommand command = new ExistsCommand();
			DataPortal.Execute(command);
			return command.Exists;
		}

		public static SystemAccount GetSystemAccount()
		{
			return DataPortal.Fetch<SystemAccount>(new Criteria());
		}

		public static SystemAccount NewSystemAccount()
		{
			return new SystemAccount();
		}

		private SystemAccount()
		{
			account = CarfaxAccount.NewAccount(CarfaxAccountType.System);
		}

		#endregion

		#region Data Access

		public class DataSource
		{
			public CarfaxAccount Select()
			{
				if (Exists())
				{
					return GetSystemAccount().Account;
				}

				return null;
			}

			public void Insert(string userName, string password)
			{
				if (Exists())
				{
					throw new InvalidOperationException("There can be only one System account!");
				}

				SystemAccount systemAccount = NewSystemAccount();
				systemAccount.Account.UserName = userName;
				systemAccount.Account.Password = password;
				systemAccount.Account.Active = true;
				systemAccount.Save();
			}

			public void Update(int id, string userName, string password)
			{
				if (Exists())
				{
					SystemAccount systemAccount = GetSystemAccount();
					systemAccount.Account.UserName = userName;
					systemAccount.Account.Password = password;
					systemAccount.Account.Active = true;
					systemAccount.Save();
				}
				else
				{
					throw new InvalidOperationException("There is no System account to update!");
				}
			}

			public void Delete()
			{
				
			}
		}

		[Serializable]
		protected class Criteria
		{
			
		}

		[Serializable]
		private class ExistsCommand : CommandBase
		{
			private bool exists;

			public bool Exists
			{
				get { return exists; }
			}

			[Transactional(TransactionalTypes.Manual)]
			protected override void DataPortal_Execute()
			{
				using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
				{
					connection.Open();

					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = "Carfax.SystemAccount#Exists";

						using (IDataReader reader = command.ExecuteReader())
						{
							if (reader.Read())
							{
								exists = reader.GetBoolean(reader.GetOrdinal("Exists"));
							}
							else
							{
								exists = false;
							}
						}
					}
				}
			}
		}

		[Transactional(TransactionalTypes.Manual)]
		protected void DataPortal_Fetch(Criteria criteria)
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
			{
				connection.Open();

				using (IDataCommand command = new DataCommand(connection.CreateCommand()))
				{
					command.CommandType = CommandType.StoredProcedure;
					command.CommandText = "Carfax.SystemAccount#Fetch";

					using (IDataReader reader = command.ExecuteReader())
					{
						if (reader.Read())
						{
							account = CarfaxAccount.GetAccount(reader);
						}
						else
						{
							throw new DataException("Missing System information");
						}
					}
				}
			}

			MarkOld();
		}

		[Transactional(TransactionalTypes.Manual)]
		protected override void DataPortal_Insert()
		{
			DoInsertUpdate();
		}

		[Transactional(TransactionalTypes.Manual)]
		protected override void DataPortal_Update()
		{
			DoInsertUpdate();
		}

		private void DoInsertUpdate()
		{
			if (!account.IsDirty) return;

			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
			{
				connection.Open();

				using (IDbTransaction transaction = connection.BeginTransaction())
				{
					if (account.IsNew)
					{
						account.DoInsert(connection, transaction);
					}
					else
					{
						account.DoUpdate(connection, transaction);
					}
					
					transaction.Commit();
				}
			}

			MarkOld();
		}

		#endregion
	}
}
