using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax
{
	[Serializable]
	public class CarfaxException : Exception
	{
		private readonly int reportId;
		private readonly CarfaxResponseCode responseCode;

		public CarfaxException(int reportId, CarfaxResponseCode responseCode)
		{
			this.reportId = reportId;
			this.responseCode = responseCode;
		}

		public CarfaxException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			reportId = info.GetInt32("ReportId");
			responseCode = (CarfaxResponseCode)info.GetValue("ResponseCode", typeof(CarfaxResponseCode));
		}

		public CarfaxException(string message, Exception innerException) : base(message, innerException)
		{
		}

		public CarfaxException(string message) : base(message)
		{
		}

		public CarfaxException()
		{
		}

		public int ReportId
		{
			get { return reportId; }
		}

		public CarfaxResponseCode ResponseCode
		{
			get { return responseCode; }
		}

		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null) throw new ArgumentNullException("info");
			base.GetObjectData(info, context);
			info.AddValue("ReportId", ReportId);
			info.AddValue("ResponseCode", responseCode, typeof(CarfaxResponseCode));
		}
	}
}
