using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax
{
	[Serializable]
	public enum CarfaxResponseCode
	{
		Undefined                  = 0,
		Success_Eligable           = 500,
		Success_NotEligable        = 501,
		Failure_NoData             = 600,
		Failure_InvalidVin         = 800,
		Failure_ServiceUnavailable = 900,
		Failure_TransactionError   = 901,
		Failure_AccountStatus      = 903,
		Failure_BadUserName        = 904,
		Failure_BadPassword        = 905,
		Failure_NoCodeSupplied     = 906
	}
}
