using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax
{
	[Serializable]
	public class CarfaxReportInspectionResultCollection : BusinessListBase<CarfaxReportInspectionResultCollection, CarfaxReportInspectionResult>
	{
		private CarfaxReportInspectionResultCollection()
		{
			/* force use of factory methods */
		}

		private CarfaxReportInspectionResultCollection(IDataReader reader)
		{
			RaiseListChangedEvents = false;

			while (reader.Read())
			{
				Add(CarfaxReportInspectionResult.GetReportInspectionResult(reader));
			}

			RaiseListChangedEvents = true;
		}

		public static CarfaxReportInspectionResultCollection NewReportInspectionResults()
		{
			return new CarfaxReportInspectionResultCollection();
		}

		internal void DoUpdate(IDataConnection connection, IDbTransaction transaction, CarfaxReport report)
		{
			RaiseListChangedEvents = false;

			//foreach (CarfaxReportInspectionResult assignment in DeletedList)
			//{
			//    assignment.DeleteSelf(connection, transaction, account);
			//}

			//DeletedList.Clear();

			foreach (CarfaxReportInspectionResult inspectionResult in this)
			{
				if (inspectionResult.IsNew)
				{
					inspectionResult.Insert(connection, transaction, report);
				}
			}

			RaiseListChangedEvents = true;
		}

		internal static CarfaxReportInspectionResultCollection GetReportInspectionResults(IDataReader reader)
		{
			return new CarfaxReportInspectionResultCollection(reader);
		}
	}
}
