using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax
{
	[Serializable]
	public class CarfaxReportPreference : BusinessBase<CarfaxReportPreference>
	{
		#region Business Methods

		private int dealerId;
		private VehicleEntityType vehicleEntityType;
		private bool displayInHotList;
		private bool purchaseReport;
		private CarfaxReportType reportType;
		private byte[] version = new byte[8];

		public int DealerId
		{
			get { return dealerId; }
		}

		public VehicleEntityType VehicleEntityType
		{
			get { return vehicleEntityType; }
		}

		public bool DisplayInHotList
		{
			get
			{
				CanReadProperty("DisplayInHotList", true);

				return displayInHotList;
			}
			set
			{
				CanWriteProperty("DisplayInHotList", true);

				if (DisplayInHotList != value)
				{
					displayInHotList = value;

					PropertyHasChanged("DisplayInHotList");
				}
			}
		}

		public bool PurchaseReport
		{
			get
			{
				CanReadProperty("PurchaseReport", true);

				return purchaseReport;
			}
			set
			{
				CanWriteProperty("PurchaseReport", true);

				if (PurchaseReport != value)
				{
					purchaseReport = value;

					PropertyHasChanged("PurchaseReport");
				}
			}
		}

		public CarfaxReportType ReportType
		{
			get
			{
				CanReadProperty("ReportType", true);

				return reportType;
			}
			set
			{
				CanWriteProperty("ReportType", true);

				if (ReportType != value)
				{
					reportType = value;

					PropertyHasChanged("ReportType");
				}
			}
		}

		protected override object GetIdValue()
		{
			return new Criteria(DealerId, VehicleEntityType);
		}

		#endregion

		#region Factory Methods
		
		private CarfaxReportPreference()
		{
			/* force use of factory methods */
		}

		private CarfaxReportPreference(int dealerId, VehicleEntityType vehicleEntityType)
		{
			this.dealerId = dealerId;
			this.vehicleEntityType = vehicleEntityType;
		}

		public static CarfaxReportPreference NewReportPreference(int dealerId, VehicleEntityType vehicleEntityType)
		{
			return new CarfaxReportPreference(dealerId, vehicleEntityType);
		}

		public static CarfaxReportPreference GetReportPreference(int dealerId, VehicleEntityType vehicleEntityType)
		{
			return DataPortal.Fetch<CarfaxReportPreference>(new Criteria(dealerId, vehicleEntityType));
		}

		public static void DeleteReportPreference(int dealerId, VehicleEntityType vehicleEntityType)
		{
			DataPortal.Delete(new Criteria(dealerId, vehicleEntityType));
		}

		public static bool Exists(int dealerId, VehicleEntityType vehicleEntityType)
		{
			ExistsCommand command = new ExistsCommand(dealerId, vehicleEntityType);
			DataPortal.Execute(command);
			return command.Exists;
		}

		public class DataSource
		{
			public CarfaxReportPreference Select(int dealerId, int vehicleEntityType)
			{
				if (Exists(dealerId, (VehicleEntityType)vehicleEntityType))
				{
					return GetReportPreference(dealerId, (VehicleEntityType)vehicleEntityType);
				}
				else
				{
					return null;
				}
			}

			public void Insert(int dealerId, int vehicleEntityType, bool displayInHotList, bool purchaseReport, string reportType)
			{
				CarfaxReportPreference preference = NewReportPreference(dealerId, (VehicleEntityType) vehicleEntityType);
				preference.DisplayInHotList = displayInHotList;
				preference.PurchaseReport = purchaseReport;
				preference.ReportType = (CarfaxReportType) Enum.Parse(typeof (CarfaxReportType), reportType);
				preference.Save();
			}

			public void Update(int dealerId, int vehicleEntityType, bool displayInHotList, bool purchaseReport, string reportType)
			{
				CarfaxReportPreference preference = GetReportPreference(dealerId, (VehicleEntityType)vehicleEntityType);
				preference.DisplayInHotList = displayInHotList;
				preference.PurchaseReport = purchaseReport;
				preference.ReportType = (CarfaxReportType)Enum.Parse(typeof(CarfaxReportType), reportType);
				preference.Save();
			}

			public void Delete(int dealerId, int vehicleEntityType)
			{
				DeleteReportPreference(dealerId, (VehicleEntityType)vehicleEntityType);
			}
		}

		#endregion

		#region Data Access

		[Serializable]
		protected class Criteria
		{
			private readonly int dealerId;
			private readonly VehicleEntityType vehicleEntityType;

			public Criteria(int dealerId, VehicleEntityType vehicleEntityType)
			{
				this.dealerId = dealerId;
				this.vehicleEntityType = vehicleEntityType;
			}

			public int DealerId
			{
				get { return dealerId; }
			}

			public VehicleEntityType VehicleEntityType
			{
				get { return vehicleEntityType; }
			}
		}

		[Serializable]
		private class ExistsCommand : CommandBase
		{
			private readonly int dealerId;
			private readonly VehicleEntityType vehicleEntityType;
			private bool exists;

			public ExistsCommand(int dealerId, VehicleEntityType vehicleEntityType)
			{
				this.dealerId = dealerId;
				this.vehicleEntityType = vehicleEntityType;
			}

			public bool Exists
			{
				get { return exists; }
			}

			protected override void DataPortal_Execute()
			{
				using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
				{
					connection.Open();

					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = "Carfax.ReportPreference#Exists";
						command.AddParameterWithValue("DealerId", DbType.Int32, true, dealerId);
						command.AddParameterWithValue("VehicleEntityTypeId", DbType.Int32, true, (int)vehicleEntityType);

						using (IDataReader reader = command.ExecuteReader())
						{
							if (reader.Read())
							{
								exists = reader.GetBoolean(reader.GetOrdinal("Exists"));
							}
							else
							{
								exists = false;
							}
						}
					}
				}
			}
		}

		protected void DataPortal_Fetch(Criteria criteria)
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
			{
				connection.Open();

				using (IDataCommand command = new DataCommand(connection.CreateCommand()))
				{
					command.CommandType = CommandType.StoredProcedure;
					command.CommandText = "Carfax.ReportPreference#Fetch";
					command.AddParameterWithValue("DealerId", DbType.Int32, false, criteria.DealerId);
					command.AddParameterWithValue("VehicleEntityTypeId", DbType.Int32, false, (int)criteria.VehicleEntityType);

					using (IDataReader reader = command.ExecuteReader())
					{
						if (reader.Read())
						{
							dealerId = reader.GetInt32(reader.GetOrdinal("DealerId"));

							vehicleEntityType = (VehicleEntityType)Enum.ToObject(typeof(VehicleEntityType), reader.GetInt32(reader.GetOrdinal("VehicleEntityTypeId")));

							displayInHotList = reader.GetBoolean(reader.GetOrdinal("DisplayInHotList"));

							purchaseReport = reader.GetBoolean(reader.GetOrdinal("PurchaseReport"));

                            reportType = (CarfaxReportType)Enum.Parse(typeof(CarfaxReportType), reader.GetString(reader.GetOrdinal("ReportType")));
                            
							reader.GetBytes(reader.GetOrdinal("Version"), 0, version, 0, 8);
						}
						else
						{
							throw new DataException("Missing ReportPreference information");
						}
					}
				}
			}

			MarkOld();
		}

		[Transactional(TransactionalTypes.Manual)]
		protected override void DataPortal_Insert()
		{
			DoInsertUpdate("Carfax.ReportPreference#Insert", true);
		}

		[Transactional(TransactionalTypes.Manual)]
		protected override void DataPortal_Update()
		{
			DoInsertUpdate("Carfax.ReportPreference#Update", false);
		}

		private void DoInsertUpdate(string procedureName, bool isInsert)
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
			{
				connection.Open();

				using (IDbTransaction transaction = connection.BeginTransaction())
				{
					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = procedureName;
						command.Transaction = transaction;

						command.AddParameterWithValue("DealerId", DbType.Int32, false, DealerId);
						command.AddParameterWithValue("VehicleEntityTypeId", DbType.Int32, false, (int)VehicleEntityType);
						command.AddParameterWithValue("DisplayInHotList", DbType.Boolean, false, DisplayInHotList);
						command.AddParameterWithValue("PurchaseReport", DbType.Boolean, false, PurchaseReport);
						command.AddParameterWithValue("ReportType", DbType.String, false, ReportType.ToString());

						if (isInsert)
						{
							command.AddParameterWithValue("InsertUser", DbType.String, false, System.Threading.Thread.CurrentPrincipal.Identity.Name);
						}
						else
						{
							Database.AddArrayParameter(command, "Version", DbType.Binary, false, version);

							command.AddParameterWithValue("UpdateUser", DbType.String, false, System.Threading.Thread.CurrentPrincipal.Identity.Name);
						}

						IDataParameter newVersion = Database.AddArrayOutParameter(command, "NewVersion", DbType.Binary, false, 8);

						command.ExecuteNonQuery();

						version = (byte[])newVersion.Value;
					}

					transaction.Commit();
				}
			}

			MarkOld();
		}

		[Transactional(TransactionalTypes.Manual)]
		protected void DataPortal_Delete(Criteria criteria)
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
			{
				connection.Open();

				using (IDbTransaction transaction = connection.BeginTransaction())
				{
					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = "Carfax.ReportPreference#Delete";
						command.Transaction = transaction;

						command.AddParameterWithValue("DealerId", DbType.Int32, false, criteria.DealerId);
						command.AddParameterWithValue("VehicleEntityTypeId", DbType.Int32, false, (int)criteria.VehicleEntityType);

						command.ExecuteNonQuery();
					}

					transaction.Commit();
				}
			}

			MarkNew();
		}

		#endregion
	}
}
