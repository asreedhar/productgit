using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax
{
	[Serializable]
	public class CarfaxReport : BusinessBase<CarfaxReport>
	{
		private int id;
		private int accountId;
		private string vin;
		private CarfaxReportType reportType;
		private bool displayInHotList;
		private XPathDocument document;
		private DateTime expirationDate = DateTime.MinValue;
		private int ownerCount;
		private bool hasProblem;
		private CarfaxReportInspectionResultCollection inspections;

		public int Id
		{
			get { return id; }
		}

		public int AccountId
		{
			get { return accountId; }
		}

		public CarfaxAccount Account
		{
			get { return CarfaxAccount.GetAccount(accountId); }
		}

		public string Vin
		{
			get { return vin; }
		}

		public CarfaxReportType ReportType
		{
			get { return reportType; }
		}

		public bool DisplayInHotList
		{
			get { return displayInHotList; }
		}

		public bool HasProblem
		{
			get { return hasProblem; }
		}

		public XPathDocument Document
		{
			get { return document; }
		}

		public DateTime ExpirationDate
		{
			get { return expirationDate; }
		}

		public int OwnerCount
		{
			get { return ownerCount; }
		}

		public CarfaxReportInspectionResultCollection Inspections
		{
			get { return inspections; }
		}

		public CarfaxReport Renew(CarfaxTrackingCode trackingCode)
		{
			return NewReport(
				accountId,
				vin,
				trackingCode,
				CarfaxRequestType.IM2,
				ReportType,
				CarfaxWindowSticker.WS1,
				DisplayInHotList);
		}

		public CarfaxReport Upgrade(CarfaxTrackingCode trackingCode, CarfaxReportType newReportType)
		{
			if (reportType == CarfaxReportType.BTC && newReportType == CarfaxReportType.CIP)
				Upgrade(trackingCode, CarfaxReportType.VHR);

			return NewReport(
					accountId,
					vin,
					trackingCode,
					CarfaxRequestType.IM2,
					newReportType,
					CarfaxWindowSticker.WS1,
					DisplayInHotList);
		}

		public CarfaxReport ChangeHotListStatus(CarfaxTrackingCode trackingCode, bool newDisplayInHotList)
		{
			if (displayInHotList != newDisplayInHotList)
			{
				return NewReport(
					accountId,
					vin,
					trackingCode,
					CarfaxRequestType.IM2,
					reportType,
					CarfaxWindowSticker.WS1,
					newDisplayInHotList);
			}

			return this;
		}

		protected void Purchase(CarfaxTrackingCode trackingCode, CarfaxRequestType requestType, CarfaxWindowSticker windowSticker)
		{
			CarfaxAccount account = Account;

			CarfaxReportClient client = new CarfaxReportClient();
			client.UserName = account.UserName;
			client.Password = account.Password;
			client.Vin = Vin;
			client.ReportType = ReportType;
			client.RequestType = requestType;
			client.TrackingCode = trackingCode;
			client.WindowSticker = windowSticker;
			client.DisplayInHotList = DisplayInHotList;
			client.AutomaticallyPurchaseReport = true;
			client.RequestOwnerCount = true;
			client.DoRequest();

			DataPortal.Execute(new ResponseCommand(Id, (int) client.ResponseCode));

			if (client.IsSuccess())
			{
				document = client.Document;

				XPathNavigator navigator = document.CreateNavigator();

				XPathNavigator node = navigator.SelectSingleNode("/carfaxInventoryManager/Problem");

				if (node != null)
				{
					hasProblem = string.Equals("Y", node.Value);
				}

				node = navigator.SelectSingleNode("/carfaxInventoryManager/Expiration");

				if (node != null)
				{
                    DateTime.TryParse(node.Value, out expirationDate);
				}

				node = navigator.SelectSingleNode("/carfaxInventoryManager/Ownership");

				if (node != null)
				{
					int.TryParse(node.Value, out ownerCount);
				}

				CarfaxReportInspectionCollection referenceInspections = CarfaxReportInspectionCollection.GetReportInspections();

				foreach (CarfaxReportInspection inspection in referenceInspections)
				{
					node = navigator.SelectSingleNode(inspection.XPath);

					bool selected = (node != null);

					inspections.Add(CarfaxReportInspectionResult.NewReportInspectionResult(inspection.Id, selected));
				}
			}
			else
			{
				throw new CarfaxException(Id, client.ResponseCode);
			}
		}

		protected override object GetIdValue()
		{
			return Id;
		}

		private CarfaxReport()
		{
			/* force use of factory methods */
		}

		internal static CarfaxReport NewReport(
			int accountId,
			string vin,
			CarfaxTrackingCode trackingCode,
			CarfaxRequestType requestType,
			CarfaxReportType reportType,
			CarfaxWindowSticker windowSticker,
			bool displayInHotList)
		{
			CarfaxReport report = DataPortal.Create<CarfaxReport>(
				new CreateCriteria(
					accountId,
					vin,
					trackingCode,
					requestType,
					reportType,
					windowSticker,
					displayInHotList));
			report.Purchase(trackingCode, requestType, windowSticker);
			report.Save();
			return report;
		}

		internal static CarfaxReport GetReport(int accountId, string vin)
		{
			return DataPortal.Fetch<CarfaxReport>(new FetchCriteria(accountId, vin));
		}

		[Serializable]
		protected class ResponseCommand : CommandBase
		{
			private readonly int reportId;
			private readonly int responseCode;

			public ResponseCommand(int reportId, int responseCode)
			{
				this.reportId = reportId;
				this.responseCode = responseCode;
			}

			[Transactional(TransactionalTypes.Manual)]
			protected override void DataPortal_Execute()
			{
				using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
				{
					connection.Open();

					using (IDbTransaction transaction = connection.BeginTransaction())
					{
						using (IDataCommand command = new DataCommand(connection.CreateCommand()))
						{
							command.CommandType = CommandType.StoredProcedure;
							command.CommandText = "Carfax.Report#ResponseCode";
							command.Transaction = transaction;

							command.AddParameterWithValue("ReportId", DbType.Int32, false, reportId);
							command.AddParameterWithValue("ResponseCode", DbType.Int32, false, responseCode);

							command.ExecuteNonQuery();
						}

						transaction.Commit();
					}
				}
			}
		}

		[Serializable]
		protected class CreateCriteria
		{
			private readonly int accountId;
			private readonly string vin;
			private readonly CarfaxTrackingCode trackingCode;
			private readonly CarfaxRequestType requestType;
			private readonly CarfaxReportType reportType;
			private readonly CarfaxWindowSticker windowSticker;
			private readonly bool displayInHotList;

			public CreateCriteria(int accountId, string vin, CarfaxTrackingCode trackingCode, CarfaxRequestType requestType, CarfaxReportType reportType, CarfaxWindowSticker windowSticker, bool displayInHotList)
			{
				this.accountId = accountId;
				this.vin = vin;
				this.trackingCode = trackingCode;
				this.requestType = requestType;
				this.reportType = reportType;
				this.windowSticker = windowSticker;
				this.displayInHotList = displayInHotList;
			}

			public int AccountId
			{
				get { return accountId; }
			}

			public string Vin
			{
				get { return vin; }
			}

			public CarfaxTrackingCode TrackingCode
			{
				get { return trackingCode; }
			}

			public CarfaxRequestType RequestType
			{
				get { return requestType; }
			}

			public CarfaxReportType ReportType
			{
				get { return reportType; }
			}

			public CarfaxWindowSticker WindowSticker
			{
				get { return windowSticker; }
			}

			public bool DisplayInHotList
			{
				get { return displayInHotList; }
			}
		}

		[Serializable]
		protected class FetchCriteria
		{
			private readonly int accountId;
			private readonly string vin;

			public FetchCriteria(int accountId, string vin)
			{
				this.accountId = accountId;
				this.vin = vin;
			}

			public int AccountId
			{
				get { return accountId; }
			}

			public string Vin
			{
				get { return vin; }
			}
		}

		[Transactional(TransactionalTypes.Manual)]
		protected void DataPortal_Create(CreateCriteria criteria)
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
			{
				connection.Open();

				using (IDbTransaction transaction = connection.BeginTransaction())
				{
					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = "Carfax.Report#Create";
						command.Transaction = transaction;

						command.AddParameterWithValue("AccountId", DbType.Int32, false, criteria.AccountId);
						command.AddParameterWithValue("Vin", DbType.String, false, criteria.Vin);
						command.AddParameterWithValue("TrackingCode", DbType.String, false, criteria.TrackingCode);
						command.AddParameterWithValue("RequestType", DbType.String, false, criteria.RequestType);
						command.AddParameterWithValue("ReportType", DbType.String, false, criteria.ReportType);
						command.AddParameterWithValue("WindowSticker", DbType.String, false, criteria.WindowSticker);
						command.AddParameterWithValue("DisplayInHotList", DbType.Boolean, false, criteria.DisplayInHotList);
						command.AddParameterWithValue("InsertUser", DbType.String, false, Thread.CurrentPrincipal.Identity.Name);

						IDataParameter newId = command.AddOutParameter("Id", DbType.Int32);

						command.ExecuteNonQuery();

						id = (int) newId.Value;

						accountId = criteria.AccountId;

						vin = criteria.Vin;

						reportType = criteria.ReportType;

						displayInHotList = criteria.DisplayInHotList;

						inspections = CarfaxReportInspectionResultCollection.NewReportInspectionResults();
					}

					transaction.Commit();
				}
			}
		}

		[Transactional(TransactionalTypes.Manual)]
		protected void DataPortal_Fetch(FetchCriteria criteria)
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
			{
				connection.Open();

				using (IDataCommand command = new DataCommand(connection.CreateCommand()))
				{
					command.CommandType = CommandType.StoredProcedure;
					command.CommandText = "Carfax.Report#Fetch";

					command.AddParameterWithValue("AccountId", DbType.Int32, false, criteria.AccountId);
					command.AddParameterWithValue("Vin", DbType.String, false, criteria.Vin);

					using (IDataReader reader = command.ExecuteReader())
					{
						if (reader.Read())
						{
							Fetch(reader);
						}
						else
						{
							throw new DataException("Expected Carfax Report data!");
						}
					}
				}
			}
		}

		private const string IdentityTransform = "<xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" version=\"1.0\"><xsl:template match=\"node()|@*\"><xsl:copy><xsl:apply-templates select=\"@*\"/><xsl:apply-templates/></xsl:copy></xsl:template></xsl:stylesheet>";

		[Transactional(TransactionalTypes.Manual)]
		protected override void DataPortal_Insert()
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
			{
				connection.Open();

				using (IDbTransaction transaction = connection.BeginTransaction())
				{
					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = "Carfax.Report#Insert";
						command.Transaction = transaction;

						StringBuilder sb = new StringBuilder();

						using (StringReader sr = new StringReader(IdentityTransform))
						{
							using (XmlReader xr = XmlReader.Create(sr))
							{
								using (StringWriter sw = new StringWriter(sb))
								{
									using (XmlWriter xw = XmlWriter.Create(sw))
									{
										XslCompiledTransform xt = new XslCompiledTransform();
										xt.Load(xr);
										xt.Transform(Document, xw);
									}
								}
							}
						}

						command.AddParameterWithValue("Id", DbType.Int32, false, Id);
						command.AddParameterWithValue("Document", DbType.Xml, false, sb.ToString());
						command.AddParameterWithValue("ExpirationDate", DbType.DateTime, false, ExpirationDate);
						command.AddParameterWithValue("OwnerCount", DbType.Int32, false, OwnerCount);
						command.AddParameterWithValue("HasProblem", DbType.Boolean, false, HasProblem);

						command.ExecuteNonQuery();
					}

					inspections.DoUpdate(connection, transaction, this);

					transaction.Commit();
				}
			}

			MarkOld();
		}

		private void Fetch(IDataReader reader)
		{
			id = reader.GetInt32(reader.GetOrdinal("Id"));
			accountId = reader.GetInt32(reader.GetOrdinal("AccountId"));
			vin = reader.GetString(reader.GetOrdinal("Vin"));
			reportType = (CarfaxReportType) Enum.Parse(typeof (CarfaxReportType), reader.GetString(reader.GetOrdinal("ReportType")));
			displayInHotList = reader.GetBoolean(reader.GetOrdinal("DisplayInHotList"));

			SqlDataReader sqlReader = reader as SqlDataReader;

			if (sqlReader != null)
			{
				SqlXml xml = sqlReader.GetSqlXml(reader.GetOrdinal("Document"));

				using (XmlReader xmlReader = xml.CreateReader())
				{
					document = new XPathDocument(xmlReader);
				}
			}

			expirationDate = reader.GetDateTime(reader.GetOrdinal("ExpirationDate"));
			ownerCount = reader.GetInt32(reader.GetOrdinal("OwnerCount"));
			hasProblem = reader.GetBoolean(reader.GetOrdinal("HasProblem"));

			if (reader.NextResult())
			{
				inspections = CarfaxReportInspectionResultCollection.GetReportInspectionResults(reader);
			}
			else
			{
				inspections = CarfaxReportInspectionResultCollection.NewReportInspectionResults();
			}

			MarkOld();
		}

        public static  bool CheckAvailability(string vin)
        {
           bool isAvail = false;
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
            {
                connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Carfax#ReportAvailability";
                        command.AddParameterWithValue("Vin", DbType.String, false, vin);
                        command.Transaction = transaction;
                        using (IDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                isAvail = reader.GetBoolean(reader.GetOrdinal("Avail"));
                            }
                            else
                            {
                                throw new DataException("Expected Carfax Report data!");
                            }
                        }

                    }
                    transaction.Commit();
                }
                return isAvail;
            }

        }
	}
}
