using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.XPath;
using FirstLook.VehicleHistoryReport.DomainModel.Diagnostics;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax
{
	public sealed class CarfaxReportClient
	{
		private static readonly string CarfaxServiceUriText;

		static CarfaxReportClient()
		{
			string text = "http://socket.carfax.com:8080/";

			try
			{
				string value = ConfigurationManager.AppSettings["CarfaxServiceUri"];
				if (!string.IsNullOrEmpty(value))
					text = value;
			}
			catch
			{
				// ignore error!
			}

			CarfaxServiceUriText = text;
		}

		private string userName;
		private string password;
		private string vin;
		private CarfaxReportType reportType;
		private CarfaxRequestType requestType;
		private CarfaxTrackingCode trackingCode;
		private CarfaxWindowSticker windowSticker;
		private bool displayInHotList = false;
		private bool automaticallyPurchaseReport = false;
		private bool requestOwnerCount = false;

		public string UserName
		{
			get { return userName; }
			set { userName = value; }
		}

		public string Password
		{
			get { return password; }
			set { password = value; }
		}

		public string Vin
		{
			get { return vin; }
			set { vin = value; }
		}

		public CarfaxReportType ReportType
		{
			get { return reportType; }
			set { reportType = value; }
		}

		public CarfaxRequestType RequestType
		{
			get { return requestType; }
			set { requestType = value; }
		}

		public CarfaxTrackingCode TrackingCode
		{
			get { return trackingCode; }
			set { trackingCode = value; }
		}

		public CarfaxWindowSticker WindowSticker
		{
			get { return windowSticker; }
			set { windowSticker = value; }
		}

		public bool DisplayInHotList
		{
			get { return displayInHotList; }
			set { displayInHotList = value; }
		}

		public bool RequestOwnerCount
		{
			get { return requestOwnerCount; }
			set { requestOwnerCount = value; }
		}

		public bool AutomaticallyPurchaseReport
		{
			get { return automaticallyPurchaseReport; }
			set { automaticallyPurchaseReport = value; }
		}

		private CarfaxResponseCode responseCode;
		private XPathDocument document;

		public CarfaxResponseCode ResponseCode
		{
			get { return responseCode; }
		}

		public XPathDocument Document
		{
			get { return document; }
		}

		public void DoRequest()
		{
			ValidateParameters();

            if (Configuration.IsCarfaxUnavailable())
            {
                responseCode = CarfaxResponseCode.Failure_ServiceUnavailable;

                return;
            }

			using (ICounter counter = CounterRegistry.GetCounter(InstrumentedResponsibility.External, InstrumentedSystem.WebService, InstrumentedProvider.Carfax, InstrumentedOperation.ExternalXml))
			{
				try
				{
                    HttpWebRequest request = ConstructWebRequest();

					using (WebResponse response = request.GetResponse())
					{
						using (StreamReader stream = new StreamReader(response.GetResponseStream()))
						{
							string line = stream.ReadLine();

							Match match = new Regex("^(\\d+)").Match(line);

							if (match.Success)
							{
								string text = match.Groups[1].Captures[0].Value;

								int code;

								if (int.TryParse(text, out code))
								{
									if (Enum.IsDefined(typeof (CarfaxResponseCode), code))
									{
										responseCode = (CarfaxResponseCode) Enum.ToObject(typeof (CarfaxResponseCode), code);

										if (IsSuccess())
										{
											using (XmlTextReader reader = new XmlTextReader(stream))
											{
												using (MemoryStream ms = new MemoryStream())
												{
													using (XmlNoNamespaceWriter writer = new XmlNoNamespaceWriter(ms, reader.Encoding))
													{
														writer.WriteNode(reader, true);

														writer.Flush();

														ms.Seek(0, SeekOrigin.Begin);

														document = new XPathDocument(ms);
													}
												}
											}
										}

										counter.Success();
									}
									else
									{
										responseCode = CarfaxResponseCode.Failure_ServiceUnavailable;
									}
								}
							}
							else
							{
								responseCode = CarfaxResponseCode.Failure_ServiceUnavailable;
							}
						}
					}
				}
				catch
				{
					responseCode = CarfaxResponseCode.Failure_ServiceUnavailable;
				}
			}
		}

		private void ValidateParameters()
		{
			if (string.IsNullOrEmpty(UserName))
			{
				throw new InvalidOperationException("UserName is a required property");
			}

			if (string.IsNullOrEmpty(Password) && (AutomaticallyPurchaseReport || WindowSticker == CarfaxWindowSticker.WS1))
			{
				throw new InvalidOperationException("Password is a required property when automatically purchasing reports or requesting a window-sticker");
			}

			if (RequestType == CarfaxRequestType.Undefined)
			{
				throw new InvalidOperationException("RequestType is a required property");
			}

			if (TrackingCode == CarfaxTrackingCode.Undefined)
			{
				throw new InvalidOperationException("TrackingCode is a required property");
			}

			if (string.IsNullOrEmpty(Vin))
			{
				throw new InvalidOperationException("VIN is a required property");
			}

			if (Vin.Length != 17)
			{
				throw new InvalidOperationException("VIN has invalid length");
			}
		}

		public bool IsSuccess()
		{
			return responseCode == CarfaxResponseCode.Success_Eligable ||
			       responseCode == CarfaxResponseCode.Success_NotEligable;
		}

		private HttpWebRequest ConstructWebRequest()
		{
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConstructRequestString());
			request.Method = "GET";
			request.ContentType = "text/xml";
			request.AllowAutoRedirect = true;
		    request.Timeout = Configuration.WebRequestTimeout();
			return request;
		}

		private String ConstructRequestString()
		{
			StringBuilder sb = new StringBuilder(CarfaxServiceUriText);
			sb.Append("?UID=").Append(UserName);
			sb.Append("&PW=").Append(Password);
			sb.Append("&VIN=").Append(Vin);
			sb.Append("&CODE=").Append(TrackingCode);
			sb.Append("&REQUEST=").Append(RequestType);
			sb.Append("&OWNERSHIP=Y");

			if (AutomaticallyPurchaseReport)
			{
				sb.Append("&AUTOPURCHASE=").Append("Y");
			}
			
			if (ReportType != CarfaxReportType.Undefined)
			{
				sb.Append("&REPORT=").Append(ReportType.ToString());
			}
			
			sb.Append("&CCC=").Append(DisplayInHotList ? "Y" : "N");
			
			if (WindowSticker != CarfaxWindowSticker.Undefined)
			{
				sb.Append("&WS=").Append(WindowSticker.ToString());
			}
			
			return sb.ToString();
		}

		/// <summary>
		/// For some odd reason Carfax has a namespace of HTML 4 transitional.
		/// I use this class to remove that madness.
		/// </summary>
		public class XmlNoNamespaceWriter : XmlTextWriter
		{
			bool skipAttribute = false;

			public XmlNoNamespaceWriter(Stream w, Encoding encoding) : base(w, encoding)
			{
			}

			public override void WriteStartElement(string prefix, string localName, string ns)
			{
				base.WriteStartElement(null, localName, null);
			}

			public override void WriteStartAttribute(string prefix, string localName, string ns)
			{
				//If the prefix or localname are "xmlns", don't write it.
				if (prefix.CompareTo("xmlns") == 0 || localName.CompareTo("xmlns") == 0)
				{
					skipAttribute = true;
				}
				else
				{
					base.WriteStartAttribute(null, localName, null);
				}
			}

			public override void WriteString(string text)
			{
				//If we are writing an attribute, the text for the xmlns
				//or xmlns:prefix declaration would occur here.  Skip
				//it if this is the case.
				if (!skipAttribute)
				{
					base.WriteString(text);
				}
			}

			public override void WriteEndAttribute()
			{
				//If we skipped the WriteStartAttribute call, we have to
				//skip the WriteEndAttribute call as well or else the XmlWriter
				//will have an invalid state.
				if (!skipAttribute)
				{
					base.WriteEndAttribute();
				}
				//reset the boolean for the next attribute.
				skipAttribute = false;
			}


			public override void WriteQualifiedName(string localName, string ns)
			{
				//Always write the qualified name using only the
				//localname.
				base.WriteQualifiedName(localName, null);
			}
		}
	}
}
