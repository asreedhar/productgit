using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax
{
	[Serializable]
	public class CarfaxReportInspectionResult : BusinessBase<CarfaxReportInspectionResult>
	{
		private int reportInspectionId;
		private bool selected;

		public int ReportInspectionId
		{
			get { return reportInspectionId; }
		}

		public bool Selected
		{
			get
			{
				CanReadProperty("Selected", true);

				return selected;
			}
		}

		protected override object GetIdValue()
		{
			return reportInspectionId;
		}

		private CarfaxReportInspectionResult()
		{
			MarkAsChild();
		}

		private CarfaxReportInspectionResult(int reportInspectionId, bool selected) : this()
		{
			this.reportInspectionId = reportInspectionId;
			this.selected = selected;
		}

		private CarfaxReportInspectionResult(IDataRecord record) : this()
		{
			Fetch(record);
		}

		internal static CarfaxReportInspectionResult NewReportInspectionResult(int reportInspectionId, bool selected)
		{
			return new CarfaxReportInspectionResult(reportInspectionId, selected);
		}

		internal static CarfaxReportInspectionResult GetReportInspectionResult(IDataRecord record)
		{
			return new CarfaxReportInspectionResult(record);
		}

		private void Fetch(IDataRecord record)
		{
			reportInspectionId = record.GetInt32(record.GetOrdinal("ReportInspectionId"));

			selected = record.GetBoolean(record.GetOrdinal("Selected"));

			MarkOld();
		}

		internal void Insert(IDataConnection connection, IDbTransaction transaction, CarfaxReport report)
		{
			using (IDataCommand command = new DataCommand(connection.CreateCommand()))
			{
				command.CommandType = CommandType.StoredProcedure;
				command.CommandText = "Carfax.ReportInspectionResult#Insert";
				command.Transaction = transaction;

				command.AddParameterWithValue("ReportId", DbType.Int32, false, report.Id);
				command.AddParameterWithValue("ReportInspectionId", DbType.Int32, false, ReportInspectionId);
				command.AddParameterWithValue("Selected", DbType.Boolean, false, Selected);

				command.ExecuteNonQuery();
			}

			MarkOld();
		}
	}
}