using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax
{
	[Serializable]
	public class CarfaxReportInspectionCollection : ReadOnlyListBase<CarfaxReportInspectionCollection, CarfaxReportInspection>
	{
		public CarfaxReportInspection Find(int reportInspectionId)
		{
			foreach (CarfaxReportInspection res in this)
				if (res.Id == reportInspectionId)
					return res;
			return null;
		}

		private CarfaxReportInspectionCollection()
		{
			/* force use of factory methods */
		}

		public static CarfaxReportInspectionCollection GetReportInspections()
		{
			return DataPortal.Fetch<CarfaxReportInspectionCollection>(new EmptyCriteria());
		}

		[Serializable]
		protected class EmptyCriteria
		{
			
		}

		protected void DataPortal_Fetch(EmptyCriteria criteria)
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
			{
				connection.Open();

				using (IDataCommand command = new DataCommand(connection.CreateCommand()))
				{
					command.CommandType = CommandType.StoredProcedure;
					command.CommandText = "Carfax.ReportInspectionCollection#Fetch";

					using (IDataReader reader = command.ExecuteReader())
					{
						IsReadOnly = false;
						while (reader.Read())
						{
							Add(CarfaxReportInspection.GetReportInspection(reader));
						}
						IsReadOnly = true;
					}
				}
			}
		}
	}
}
