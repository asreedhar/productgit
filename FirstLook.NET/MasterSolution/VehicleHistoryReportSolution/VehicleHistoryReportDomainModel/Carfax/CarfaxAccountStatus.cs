using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax
{
	[Serializable]
	public enum CarfaxAccountStatus
	{
		New,
		Okay,
		BadUserName,
		BadPassword,
		BadAccountStatus
	}
}
