using System;
using System.Data;
using Csla;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax
{
	[Serializable]
	public class CarfaxReportInspection : ReadOnlyBase<CarfaxReportInspection>
	{
		private int id;
		private string name;
		private string xpath;

		public int Id
		{
			get { return id; }
		}

		public string Name
		{
			get { return name; }
		}

		public string XPath
		{
			get { return xpath; }
		}

		protected override object GetIdValue()
		{
			return Id;
		}

		private CarfaxReportInspection(IDataRecord record)
		{
			Fetch(record);
		}

		public static CarfaxReportInspection GetReportInspection(IDataRecord record)
		{
			return new CarfaxReportInspection(record);
		}

		private void Fetch(IDataRecord record)
		{
			id = record.GetInt32(record.GetOrdinal("Id"));
			name = record.GetString(record.GetOrdinal("Name"));
			xpath = record.GetString(record.GetOrdinal("XPath"));
		}
	}
}
