using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax
{
	/// <summary>
	/// The type of information desired from a soecket request.
	/// </summary>
	[Serializable]
	public enum CarfaxRequestType
	{
		/// <summary>
		/// Uninitialized state.
		/// </summary>
		Undefined,

		/// <summary>
		/// CARFAX Inventory Manager
		/// </summary>
		IM2
	}
}
