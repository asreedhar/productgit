using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax
{
	/// <summary>
	/// The tracking-codes gievn to partners as a means of tracking activity
	/// and monitoring each programs success.
	/// </summary>
	[Serializable]
	public enum CarfaxTrackingCode
	{
		Undefined,

		/// <summary>
		/// FirstLook auto-purchase tracking-code.
		/// </summary>
		FLA,

		/// <summary>
		/// FirstLook normal tracking-code.
		/// </summary>
		FLN
	}
}
