using System;
using System.Data;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax
{
	[Serializable]
	public class CarfaxAccount : BusinessBase<CarfaxAccount>
	{
		#region Business Methods

		private int id;
		private bool active;
		private CarfaxAccountType accountType;
		private CarfaxAccountStatus accountStatus;
		private CarfaxAccountAssignmentCollection assignments = CarfaxAccountAssignmentCollection.NewCarfaxAccountAssignments();
		private string userName;
		private string password;
		private byte[] version = new byte[8];

		public bool Active
		{
			get
			{
				CanReadProperty("Active", true);

				return active;
			}
			set
			{
				CanWriteProperty("Active", true);

				if (Active != value)
				{
					active = value;

					PropertyHasChanged("Active");
				}
			}
		}

		public int Id
		{
			get { return id; }
		}

		public CarfaxAccountType AccountType
		{
			get { return accountType; }
		}

		public CarfaxAccountStatus AccountStatus
		{
			get { return accountStatus; }
		}

		public CarfaxAccountAssignmentCollection Assignments
		{
			get { return assignments; }
		}

		public string UserName
		{
			get
			{
				CanReadProperty("UserName", true);

				return userName;
			}
			set
			{
				CanWriteProperty("UserName", true);

				if (!Equals(UserName, value))
				{
					userName = value;

					PropertyHasChanged("UserName");
				}
			}
		}

		public string Password
		{
			get
			{
				CanReadProperty("Password", true);

				return password;
			}
			set
			{
				CanWriteProperty("Password", true);

				if (!Equals(Password, value))
				{
					password = value;

					PropertyHasChanged("Password");
				}
			}
		}

		public void AccountStatusResolved()
		{
			throw new NotImplementedException();
		}

		public bool HasPurchasedReport(string vin)
		{
			HasPurchasedReportCommand command = new HasPurchasedReportCommand(Id, vin);
			DataPortal.Execute(command);
			return command.Exists;
		}


        public bool HasExpiredPurchasedReport(string vin)
        {
            HasExpiredPurchasedReportCommand command = new HasExpiredPurchasedReportCommand(Id, vin);
            DataPortal.Execute(command);
            return command.Exists;
        }



		public CarfaxReport GetReport(string vin)
		{
			return CarfaxReport.GetReport(Id, vin);
		}

		public CarfaxReport PurchaseReport(string vin, CarfaxReportType reportType, bool displayInHotList, CarfaxTrackingCode trackingCode)
		{
			return CarfaxReport.NewReport(
				Id,
				vin,
				trackingCode,
				CarfaxRequestType.IM2,
				reportType,
				CarfaxWindowSticker.WS1,
				displayInHotList);
		}

		protected override object GetIdValue()
		{
			return Id;
		}

		#endregion

		#region Validation

		public override bool IsDirty
		{
			get
			{
				return base.IsDirty || assignments.IsDirty;
			}
		}

		public override bool IsValid
		{
			get
			{
				return base.IsValid && assignments.IsValid;
			}
		}

		protected override void AddBusinessRules()
		{
			ValidationRules.AddRule(CommonRules.StringRequired, "UserName");

			ValidationRules.AddRule(CommonRules.StringMaxLength,
				new CommonRules.MaxLengthRuleArgs("UserName", 20));

			ValidationRules.AddRule(CommonRules.StringRequired, "Password");

			ValidationRules.AddRule(CommonRules.StringMaxLength,
				new CommonRules.MaxLengthRuleArgs("Password", 5));
		}

		#endregion

		#region Factory Methods

		internal static CarfaxAccount GetAccount(IDataReader reader)
		{
			return new CarfaxAccount(reader);
		}

		internal static CarfaxAccount GetAccount(int id)
		{
			return DataPortal.Fetch<CarfaxAccount>(new Criteria(id));
		}

		internal static CarfaxAccount NewAccount(CarfaxAccountType accountType)
		{
			return new CarfaxAccount(accountType);
		}

		private CarfaxAccount()
		{
			MarkAsChild();
		}

		private CarfaxAccount(IDataReader reader)
			: this()
		{
			Fetch(reader);
		}

		private CarfaxAccount(CarfaxAccountType accountType)
			: this()
		{
			accountStatus = CarfaxAccountStatus.New;

			active = true;

			this.accountType = accountType;
		}

		#endregion

		#region Data Access

		[Serializable]
		protected class HasPurchasedReportCommand : CommandBase
		{
			private readonly int id;
			private readonly string vin;
			private bool exists;

			public HasPurchasedReportCommand(int id, string vin)
			{
				this.id = id;
				this.vin = vin;
			}

			public bool Exists
			{
				get { return exists; }
			}

			protected override void DataPortal_Execute()
			{
				using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
				{
					connection.Open();

					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = "Carfax.Account#HasPurchasedReport";
						command.AddParameterWithValue("Id", DbType.Int32, true, id);
						command.AddParameterWithValue("Vin", DbType.String, true, vin);

						using (IDataReader reader = command.ExecuteReader())
						{
							if (reader.Read())
							{
								exists = reader.GetBoolean(reader.GetOrdinal("Exists"));
							}
							else
							{
								exists = false;
							}
						}
					}
				}
			}
		}


        [Serializable]
        protected class HasExpiredPurchasedReportCommand : CommandBase
        {
            private readonly int _accountId;
            private readonly string vin;
            private bool _exists;

            public HasExpiredPurchasedReportCommand(int accountId, string _vin)
            {
                this._accountId = accountId;
                this.vin = _vin;
            }

            public bool Exists
            {
                get { return _exists; }
            }

            protected override void DataPortal_Execute()
            {
                using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
                {
                    connection.Open();

                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Carfax.Account#HasExpiredPurchasedReport";
                        command.AddParameterWithValue("Id", DbType.Int32, true, _accountId);
                        command.AddParameterWithValue("Vin", DbType.String, true, vin);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                _exists = reader.GetBoolean(reader.GetOrdinal("Exists"));
                            }
                            else
                            {
                                _exists = false;
                            }
                        }
                    }
                }
            }
        }




		[Serializable]
		protected class Criteria
		{
			private readonly int id;

			public Criteria(int id)
			{
				this.id = id;
			}

			public int Id
			{
				get { return id; }
			}
		}

		protected void DataPortal_Fetch(Criteria criteria)
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
			{
				connection.Open();

				using (IDataCommand command = new DataCommand(connection.CreateCommand()))
				{
					command.CommandType = CommandType.StoredProcedure;
					command.CommandText = "Carfax.Account#Fetch";
					command.AddParameterWithValue("Id", DbType.Int32, true, criteria.Id);

					using (IDataReader reader = command.ExecuteReader())
					{
						if (reader.Read())
						{
							Fetch(reader);
						}
						else
						{
							throw new DataException("Missing Account data!");
						}
					}
				}
			}
		}

		private void Fetch(IDataReader reader)
		{
			id = reader.GetInt32(reader.GetOrdinal("Id"));

			active = reader.GetBoolean(reader.GetOrdinal("Active"));

			accountType = (CarfaxAccountType)Enum.ToObject(typeof(CarfaxAccountType), reader.GetInt32(reader.GetOrdinal("AccountType")));

			accountStatus = (CarfaxAccountStatus)Enum.ToObject(typeof(CarfaxAccountStatus), reader.GetInt32(reader.GetOrdinal("AccountStatus")));

			userName = reader.GetString(reader.GetOrdinal("UserName"));

			password = reader.GetString(reader.GetOrdinal("Password"));

			reader.GetBytes(reader.GetOrdinal("Version"), 0, version, 0, 8);

			if (reader.NextResult())
			{
				assignments = CarfaxAccountAssignmentCollection.GetCarfaxAccountAssignments(reader);
			}

			MarkOld();
		}

		public void DoInsert(IDataConnection connection, IDbTransaction transaction)
		{
			using (IDataCommand command = new DataCommand(connection.CreateCommand()))
			{
				command.CommandType = CommandType.StoredProcedure;
				command.CommandText = "Carfax.Account#Insert";
				command.Transaction = transaction;

				command.AddParameterWithValue("AccountTypeId", DbType.Int32, false, (int)AccountType);
				command.AddParameterWithValue("Active", DbType.Boolean, false, Active);
				command.AddParameterWithValue("UserName", DbType.String, false, UserName);
				command.AddParameterWithValue("Password", DbType.String, false, Password);
				command.AddParameterWithValue("InsertUser", DbType.String, false, System.Threading.Thread.CurrentPrincipal.Identity.Name);

				IDataParameter newId = command.AddOutParameter("Id", DbType.Int32);

				IDataParameter newVersion = Database.AddArrayOutParameter(command, "NewVersion", DbType.Binary, false, 8);

				command.ExecuteNonQuery();

				id = (int)newId.Value;

				version = (byte[])newVersion.Value;
			}

			assignments.DoUpdate(connection, transaction, this);

			MarkOld();
		}

		internal void DoUpdate(IDbConnection connection, IDbTransaction transaction)
		{
			using (IDataCommand command = new DataCommand(connection.CreateCommand()))
			{
				command.CommandType = CommandType.StoredProcedure;
				command.CommandText = "Carfax.Account#Update";
				command.Transaction = transaction;

				command.AddParameterWithValue("Id", DbType.Int32, false, Id);
				command.AddParameterWithValue("Active", DbType.Boolean, false, Active);
				command.AddParameterWithValue("UserName", DbType.String, false, UserName);
				command.AddParameterWithValue("Password", DbType.String, false, Password);
				command.AddParameterWithValue("UpdateUser", DbType.String, false, System.Threading.Thread.CurrentPrincipal.Identity.Name);

				Database.AddArrayParameter(command, "Version", DbType.Binary, false, version);

				IDataParameter newVersion = Database.AddArrayOutParameter(command, "NewVersion", DbType.Binary, false, 8);

				command.ExecuteNonQuery();

				version = (byte[])newVersion.Value;
			}

			assignments.DoUpdate(connection, transaction, this);

			MarkOld();
		}

		#endregion
	}
}
