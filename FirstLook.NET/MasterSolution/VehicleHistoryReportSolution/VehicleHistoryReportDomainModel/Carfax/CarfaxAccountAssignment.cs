using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax
{
	[Serializable]
	public class CarfaxAccountAssignment : BusinessBase<CarfaxAccountAssignment>
	{
		private int memberId;
		private DateTime assigned = DateTime.MinValue;

		public int MemberId
		{
			get { return memberId; }
		}

		public DateTime Assigned
		{
			get { return assigned; }
		}

		protected override object GetIdValue()
		{
			return MemberId;
		}

		private CarfaxAccountAssignment()
		{
			MarkAsChild();
		}

		private CarfaxAccountAssignment(int memberId) : this()
		{
			this.memberId = memberId;
			
			assigned = DateTime.Now;
		}

		private CarfaxAccountAssignment(IDataRecord record) : this()
		{
			Fetch(record);
		}

		internal static CarfaxAccountAssignment GetCarfaxAccountAssignment(IDataRecord record)
		{
			return new CarfaxAccountAssignment(record);
		}

		public static CarfaxAccountAssignment NewCarfaxAccountAssignment(int memberId)
		{
			return new CarfaxAccountAssignment(memberId);
		}

		private void Fetch(IDataRecord record)
		{
			memberId = record.GetInt32(record.GetOrdinal("MemberId"));

			assigned = record.GetDateTime(record.GetOrdinal("Assigned"));

			MarkOld();
		}

		public void DeleteSelf(IDbConnection connection, IDbTransaction transaction, CarfaxAccount account)
		{
			using (IDataCommand command = new DataCommand(connection.CreateCommand()))
			{
				command.CommandType = CommandType.StoredProcedure;
				command.CommandText = "Carfax.Account#DeleteAssignment";
				command.Transaction = transaction;

				command.AddParameterWithValue("AccountId", DbType.Int32, false, account.Id);
				command.AddParameterWithValue("MemberId", DbType.Int32, false, MemberId);
				command.AddParameterWithValue("DeleteUser", DbType.String, false, System.Threading.Thread.CurrentPrincipal.Identity.Name);

				command.ExecuteNonQuery();
			}

			MarkNew();
		}

		public void Insert(IDbConnection connection, IDbTransaction transaction, CarfaxAccount account)
		{
			using (IDataCommand command = new DataCommand(connection.CreateCommand()))
			{
				command.CommandType = CommandType.StoredProcedure;
				command.CommandText = "Carfax.Account#AddAssignment";
				command.Transaction = transaction;

				command.AddParameterWithValue("AccountId", DbType.Int32, false, account.Id);
				command.AddParameterWithValue("MemberId", DbType.Int32, false, MemberId);
				command.AddParameterWithValue("InsertUser", DbType.String, false, System.Threading.Thread.CurrentPrincipal.Identity.Name);

				command.ExecuteNonQuery();
			}

			MarkOld();
		}
	}
}
