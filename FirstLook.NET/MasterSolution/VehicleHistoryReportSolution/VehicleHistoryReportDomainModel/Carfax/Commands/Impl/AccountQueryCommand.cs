﻿using System;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.Impl
{
    [Serializable]
    public class AccountQueryCommand : ICommand<AccountQueryResultsDto, AccountQueryArgumentsDto>
    {
        public AccountQueryResultsDto Execute(AccountQueryArgumentsDto parameters)
        {
            if (parameters.Client.ClientType != ClientTypeDto.Dealer)
            {
                throw new NotSupportedException(string.Format("ClientType.{0}", parameters.Client.ClientType));
            }

            int dealerId = parameters.Client.Id;

            bool hasAccount = false;

            Member member = Mapper.GetMember(parameters.UserName);

            if (member != null)
            {
                CarfaxAccount account = Mapper.GetAccount(dealerId, member);

                if (account != null)
                {
                    hasAccount = account.Active;
                }
            }

            return new AccountQueryResultsDto
            {
                Arguments = parameters,
                HasAccount = hasAccount
            };
        }
    }
}
