﻿using System;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.Impl
{
    public class PurchaseReportsCommand : ICommand<PurchaseReportsResultsDto, PurchaseReportsArgumentsDto>
    {
        public PurchaseReportsResultsDto Execute(PurchaseReportsArgumentsDto parameters)
        {
            if (parameters.Client.ClientType != ClientTypeDto.Dealer)
            {
                throw new NotSupportedException(string.Format("ClientType.{0}", parameters.Client.ClientType));
            }

            int dealerId = parameters.Client.Id;

            Member member = Mapper.GetMember(parameters.UserName);

            if (member != null)
            {
                CarfaxAccount account = Mapper.GetAccount(dealerId, member);

                if (account != null)
                {
                    if (!account.Active)
                    {
                        goto EndOfCommand;
                    }

                    if (account.AccountType == CarfaxAccountType.Dealer)
                    {
                        if (!account.Assignments.Contains(member.Id))
                        {
                            goto EndOfCommand;
                        }
                    }

                    VehicleCollection vehicles = VehicleCollection.GetVehicles(dealerId, parameters.VehicleEntityType);

                    foreach (Vehicle vehicle in vehicles)
                    {
                        try
                        {
                            account.PurchaseReport(vehicle.Vin, parameters.ReportType, parameters.DisplayInHotList, parameters.TrackingCode);
                        }
                        catch (CarfaxException ce)
                        {
                            CarfaxExceptionLogger.Record(ce, ce.ReportId);
                        }
                        catch (Exception ex)
                        {
                            CarfaxExceptionLogger.Record(ex);
                        }
                    }
                }
            }

            EndOfCommand:

            return new PurchaseReportsResultsDto
            {
                Arguments = parameters
            };
        }
    }
}
