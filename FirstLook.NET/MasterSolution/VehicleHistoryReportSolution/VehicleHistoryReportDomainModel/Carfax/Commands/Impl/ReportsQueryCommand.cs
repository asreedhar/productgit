﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.Impl
{
    public class ReportsQueryCommand : ICommand<ReportsQueryResultsDto, ReportsQueryArgumentsDto>
    {
        public ReportsQueryResultsDto Execute(ReportsQueryArgumentsDto parameters)
        {
            if (parameters.Client.ClientType != ClientTypeDto.Dealer)
            {
                throw new NotSupportedException(string.Format("ClientType.{0}", parameters.Client.ClientType));
            }

            List<CarfaxReportTO> reports = new List<CarfaxReportTO>();

            int dealerId = parameters.Client.Id;

            Member member = Mapper.GetMember(parameters.UserName);

            if (member != null)
            {
                List<CarfaxAccount> accounts = new List<CarfaxAccount>();
 
                if (DealerAccount.Exists(dealerId))
                {
                    DealerAccount dealerAccount = DealerAccount.GetDealerAccount(dealerId);

                    if (dealerAccount.Account.Active)
                    {
                        accounts.Add(dealerAccount.Account);
                    }
                }
                
                if (member.MemberType == MemberType.Administrator)
                {
                    SystemAccount systemAccount = SystemAccount.GetSystemAccount();

                    if (systemAccount.Account.Active)
                    {
                        accounts.Add(systemAccount.Account);
                    }
                }

                if (accounts.Count > 0)
                {
                    VehicleCollection vehicles = VehicleCollection.GetVehicles(dealerId, parameters.VehicleEntityType);

                    foreach (Vehicle vehicle in vehicles)
                    {
                        try
                        {
                            foreach(CarfaxAccount cfAccount in accounts)
                            {
                                if (cfAccount.HasPurchasedReport(vehicle.Vin))
                                {
                                    reports.Add(Mapper.GetTransferObject(cfAccount.GetReport(vehicle.Vin)));
                                    break;
                                }
                            }
                        }
                        catch (CarfaxException ce)
                        {
                            CarfaxExceptionLogger.Record(ce, ce.ReportId);
                        }
                        catch (Exception ex)
                        {
                            CarfaxExceptionLogger.Record(ex);
                        }
                    }
                }
            }

            return new ReportsQueryResultsDto
            {
                Arguments = parameters,
                Reports = reports
            };
        }
    }
}
