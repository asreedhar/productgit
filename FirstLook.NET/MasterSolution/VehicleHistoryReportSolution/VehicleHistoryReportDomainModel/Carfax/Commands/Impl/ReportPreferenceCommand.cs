using System;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.Impl
{
    public class ReportPreferenceCommand : ICommand<ReportPreferenceResultsDto, ReportPreferenceArgumentsDto>
    {
        public ReportPreferenceResultsDto Execute(ReportPreferenceArgumentsDto parameters)
        {
            if (parameters.Client.ClientType != ClientTypeDto.Dealer)
            {
                throw new NotSupportedException(string.Format("ClientType.{0}", parameters.Client.ClientType));
            }

            int dealerId = parameters.Client.Id;

            VehicleEntityType vehicleEntityType = parameters.VehicleEntityType;

            CarfaxReportPreferenceTO dto = new CarfaxReportPreferenceTO();

            if (CarfaxReportPreference.Exists(dealerId, vehicleEntityType))
            {
                CarfaxReportPreference preference = CarfaxReportPreference.GetReportPreference(dealerId, vehicleEntityType);

                dto.DisplayInHotListings = preference.DisplayInHotList;
                dto.PurchaseReport = preference.PurchaseReport;
                dto.ReportType = preference.ReportType;
            }
            else
            {
                dto.DisplayInHotListings = false;
                dto.PurchaseReport = false;
                dto.ReportType = CarfaxReportType.BTC;
            }

            return new ReportPreferenceResultsDto
            {
                Arguments = parameters,
                Preference = dto
            };
        }
    }
}
