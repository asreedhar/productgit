﻿namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.Impl.V_2_0
{
    public class CarFaxUrlBuilder : ICarFaxUrlBuilder
    {
        internal const string CarfaxReportUrl = "http://www.carfaxonline.com/cfm/Display_Dealer_Report.cfm?partner=FLK_0&UID={0}&vin={1}";

        public string BuildUrl(string carFaxUserId, string vin)
        {
            var url = string.Format(
                CarfaxReportUrl,
                carFaxUserId,
                vin);

            return url;
        }
    }

    public interface ICarFaxUrlBuilder
    {
        string BuildUrl(string carFaxUserId, string vin);
    }
}
