using System;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes.V_2_0;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.Impl.V_2_0
{
    [Serializable]
    [CLSCompliant(false)]
    public class CarfaxPurchaseCommand : ICommand<CarfaxReportPurchaseResultsDto, IdentityContextDto<CarfaxReportPurchaseArgumentsDto>>
    {
        public CarfaxReportPurchaseResultsDto Execute(IdentityContextDto<CarfaxReportPurchaseArgumentsDto> parameters)
        {
            CarfaxReportPurchaseArgumentsDto arguments = parameters.Arguments;

            IdentityDto identity = parameters.Identity;

            IBroker broker = Broker.Get(
                identity.Name,
                identity.AuthorityName,
                arguments.Broker);

            CarfaxReportResultsStatusDto status = CarfaxReportResultsStatusDto.Success;

            CarfaxReportTO report = null;

            ICarFaxUrlBuilder urlBuilder = new CarFaxUrlBuilder();
            string url = string.Empty;

            try
            {
                Member member = Mapper.GetMember(identity.Name);

                if (member != null)
                {
                    CarfaxAccount account = Mapper.GetAccount(broker.Client.Id, member);

                    if (account != null)
                    {
                        url = urlBuilder.BuildUrl(account.UserName, arguments.Vin);

                        if (account.Active)
                        {
                            if (account.AccountType == CarfaxAccountType.Dealer)
                            {
                                if (account.Assignments.Contains(member.Id))
                                {
                                    report = Mapper.GetTransferObject(
                                        account.PurchaseReport(
                                        arguments.Vin,
                                        arguments.ReportType,
                                        arguments.DisplayInHotList,
                                        CarfaxTrackingCode.FLN));
                                }
                                else
                                {
                                    status = CarfaxReportResultsStatusDto.NoPermission;
                                }
                            }
                            else
                            {
                                report = Mapper.GetTransferObject(
                                    account.PurchaseReport(
                                    arguments.Vin,
                                    arguments.ReportType,
                                    arguments.DisplayInHotList,
                                    CarfaxTrackingCode.FLN));
                            }
                        }
                        else
                        {
                            status = CarfaxReportResultsStatusDto.AccountNotOperable;
                        }
                    }
                    else
                    {
                        status = CarfaxReportResultsStatusDto.AccountNotAvailable;
                    }
                }
                else
                {
                    status = CarfaxReportResultsStatusDto.NoPermission;
                }
            }
            catch (Exception)
            {
                status = CarfaxReportResultsStatusDto.ServiceUnavailable;
            }

            return new CarfaxReportPurchaseResultsDto
            {
                Arguments = arguments,
                Report = report,
                Status = status,
                Url = url
            };
        }
    }
}
