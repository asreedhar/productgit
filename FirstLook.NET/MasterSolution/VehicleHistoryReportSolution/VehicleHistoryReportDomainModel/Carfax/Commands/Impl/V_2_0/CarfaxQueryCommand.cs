﻿using System;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes.V_2_0;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.Impl.V_2_0
{
    [CLSCompliant(false)]
    public class CarfaxQueryCommand : ICommand<CarfaxReportQueryResultsDto, IdentityContextDto<CarfaxReportQueryArgumentsDto>>
    {
        public CarfaxReportQueryResultsDto Execute(IdentityContextDto<CarfaxReportQueryArgumentsDto> parameters)
        {
            IdentityDto identity = parameters.Identity;

            CarfaxReportQueryArgumentsDto arguments = parameters.Arguments;

            IBroker broker = Broker.Get(
                identity.Name,
                identity.AuthorityName,
                arguments.Broker);

            string vin = arguments.Vin;

            CarfaxReportResultsStatusDto status = CarfaxReportResultsStatusDto.Success;

            CarfaxReportTO report = null;

            ICarFaxUrlBuilder urlBuilder = new CarFaxUrlBuilder();
            string url = string.Empty;

            try
            {
                Member member = Mapper.GetMember(identity.Name);

                if (member != null)
                {
                    if (broker.Integration.IsDealer)
                    {
                        int dealerId = broker.Client.Id;

                        if (DealerAccount.Exists(dealerId))
                        {
                            DealerAccount dealerAccount = DealerAccount.GetDealerAccount(dealerId);

                            CarfaxAccount account = dealerAccount.Account;

                            url = urlBuilder.BuildUrl(account.UserName, arguments.Vin);

                            if (account.HasPurchasedReport(vin))
                            {
                                report = Mapper.GetTransferObject(account.GetReport(vin));
                            }
                            else
                            {
                                bool knowVehicleType = arguments.VehicleEntityType != VehicleEntityType.Undefined;

                                if (knowVehicleType)
                                {
                                    bool haveReportPreference = CarfaxReportPreference.Exists(dealerId, arguments.VehicleEntityType);

                                    if (haveReportPreference)
                                    {
                                        CarfaxReportPreference preference = CarfaxReportPreference.GetReportPreference(dealerId, arguments.VehicleEntityType);

                                        if (preference.PurchaseReport)
                                        {
                                            report = Mapper.GetTransferObject(
                                                account.PurchaseReport(
                                                    arguments.Vin,
                                                    preference.ReportType,
                                                    preference.DisplayInHotList,
                                                    CarfaxTrackingCode.FLA));
                                        }
                                        else
                                        {
                                            status = CarfaxReportResultsStatusDto.ReportNotPurchased;
                                        }
                                    }
                                }
                                else
                                {
                                    status = CarfaxReportResultsStatusDto.ReportNotPurchased;
                                }
                            }
                        }
                        else
                        {
                            status = CarfaxReportResultsStatusDto.AccountNotAvailable;
                        }
                    }

                    if (report == null)
                    {
                        if (member.MemberType == MemberType.Administrator)
                        {
                            if (SystemAccount.Exists())
                            {
                                SystemAccount systemAccount = SystemAccount.GetSystemAccount();

                                CarfaxAccount account = systemAccount.Account;
                                url = urlBuilder.BuildUrl(account.UserName, arguments.Vin);

                                if (account.HasPurchasedReport(vin))
                                {
                                    report = Mapper.GetTransferObject(account.GetReport(vin));
                                    status = CarfaxReportResultsStatusDto.Success;
                                }
                                else
                                {
                                    report = Mapper.GetTransferObject(
                                        account.PurchaseReport(
                                            arguments.Vin,
                                            CarfaxReportType.VHR,
                                            false,
                                            CarfaxTrackingCode.FLA));

                                    status = CarfaxReportResultsStatusDto.Success;
                                }
                            }
                        }
                        else // do not clobber status for dealer flow ...
                        {
                            if (broker.Integration.IsSeller)
                            {
                                status = CarfaxReportResultsStatusDto.AccountNotAvailable;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                status = CarfaxReportResultsStatusDto.ServiceUnavailable;
            }

            return new CarfaxReportQueryResultsDto
            {
                Arguments = arguments,
                Report = report,
                Status = status,
                Url = url
            };
        }
    }
}
