using System;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.Impl
{
    public class PurchaseReportCommand : ICommand<PurchaseReportResultsDto, PurchaseReportArgumentsDto>
    {
        public PurchaseReportResultsDto Execute(PurchaseReportArgumentsDto parameters)
        {
            if (parameters.Client.ClientType != ClientTypeDto.Dealer)
            {
                throw new NotSupportedException(string.Format("ClientType.{0}", parameters.Client.ClientType));
            }

            int dealerId = parameters.Client.Id;

            CarfaxReportTO report = null;

            Member member = Mapper.GetMember(parameters.UserName);

            if (member != null)
            {
                CarfaxAccount account = Mapper.GetAccount(dealerId, member);

                if (account != null)
                {
                    if (account.Active)
                    {
                        if (account.AccountType == CarfaxAccountType.Dealer)
                        {
                            if (account.Assignments.Contains(member.Id))
                            {
                                report = Mapper.GetTransferObject(account.PurchaseReport(parameters.Vin, parameters.ReportType, parameters.DisplayInHotList, parameters.TrackingCode));
                            }
                        }
                        else
                        {
                            report = Mapper.GetTransferObject(account.PurchaseReport(parameters.Vin, parameters.ReportType, parameters.DisplayInHotList, parameters.TrackingCode));
                        }
                    }
                }
            }

            return new PurchaseReportResultsDto
            {
                Arguments = parameters,
                Report = report
            };
        }
    }
}
