using System.Security.Principal;
using System.Threading;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.Impl
{
    public static class Mapper
    {
        public static Member GetMember(string userName)
        {
            if (Member.Exists(userName))
            {
                Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(userName, "SOAP"), new string[0]);

                return Member.GetMember(userName);
            }

            return null;
        }

        public static CarfaxAccount GetAccount(int dealerId, Member member)
        {
            if (member.MemberType == MemberType.User)
            {
                if (DealerAccount.Exists(dealerId))
                {
                    DealerAccount dealerAccount = DealerAccount.GetDealerAccount(dealerId);

                    return dealerAccount.Account;
                }
            }
            else if (member.MemberType == MemberType.Administrator)
            {
                if (SystemAccount.Exists())
                {
                    SystemAccount systemAccount = SystemAccount.GetSystemAccount();

                    return systemAccount.Account;
                }
            }

            return null;
        }

        public static CarfaxReportTO GetTransferObject(CarfaxReport report)
        {
            CarfaxReportTO info = new CarfaxReportTO
            {
                DisplayInHotList = report.DisplayInHotList,
                ExpirationDate = report.ExpirationDate,
                ReportType = report.ReportType,
                UserName = report.Account.UserName,
                Vin = report.Vin,
                HasProblem = report.HasProblem,
                OwnerCount = report.OwnerCount
            };

            CarfaxReportInspectionCollection inspections = CarfaxReportInspectionCollection.GetReportInspections();

            foreach (CarfaxReportInspectionResult result in report.Inspections)
            {
                CarfaxReportInspection inspection = inspections.Find(result.ReportInspectionId);

                CarfaxReportInspectionTO item = new CarfaxReportInspectionTO
                {
                    Id = result.ReportInspectionId,
                    Name = inspection == null ? "(Unknown)" : inspection.Name,
                    Selected = result.Selected
                };

                info.Inspections.Add(item);
            }

            return info;
        }

        public static bool IsAvailable(string vin)
        {
            return CarfaxReport.CheckAvailability(vin);
        }

    }
}
