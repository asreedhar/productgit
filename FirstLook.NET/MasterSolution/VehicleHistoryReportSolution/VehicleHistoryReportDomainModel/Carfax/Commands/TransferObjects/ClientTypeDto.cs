﻿using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects
{
    [Serializable]
    public enum ClientTypeDto
    {
        Undefined,
        Dealer
    }
}
