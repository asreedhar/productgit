using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects
{
	[Serializable]
	public class CarfaxReportPreferenceTO
	{
		private bool _displayInHotListings;
		private bool _purchaseReport;
		private CarfaxReportType _reportType;

		public bool DisplayInHotListings
		{
			get { return _displayInHotListings; }
			set { _displayInHotListings = value; }
		}

		public bool PurchaseReport
		{
			get { return _purchaseReport; }
			set { _purchaseReport = value; }
		}

		public CarfaxReportType ReportType
		{
			get { return _reportType; }
			set { _reportType = value; }
		}
	}
}
