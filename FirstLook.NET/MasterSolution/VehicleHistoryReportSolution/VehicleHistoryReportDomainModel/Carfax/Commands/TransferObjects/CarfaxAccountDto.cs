using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects
{
    [Serializable]
    public class CarfaxAccountTO
    {
        private string _userName;
        private string _password;

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }
    }
}