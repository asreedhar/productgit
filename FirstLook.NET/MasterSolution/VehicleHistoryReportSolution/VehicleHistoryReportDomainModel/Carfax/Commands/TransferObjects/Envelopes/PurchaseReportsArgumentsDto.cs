﻿using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class PurchaseReportsArgumentsDto : ArgumentsDto
    {
        private ClientDto _client;
        private VehicleEntityType _vehicleEntityType;
        private CarfaxReportType _reportType;
        private bool _displayInHotList;
        private CarfaxTrackingCode _trackingCode;

        public ClientDto Client
        {
            get { return _client; }
            set { _client = value; }
        }

        public VehicleEntityType VehicleEntityType
        {
            get { return _vehicleEntityType; }
            set { _vehicleEntityType = value; }
        }

        public CarfaxReportType ReportType
        {
            get { return _reportType; }
            set { _reportType = value; }
        }

        public bool DisplayInHotList
        {
            get { return _displayInHotList; }
            set { _displayInHotList = value; }
        }

        public CarfaxTrackingCode TrackingCode
        {
            get { return _trackingCode; }
            set { _trackingCode = value; }
        }
    }
}
