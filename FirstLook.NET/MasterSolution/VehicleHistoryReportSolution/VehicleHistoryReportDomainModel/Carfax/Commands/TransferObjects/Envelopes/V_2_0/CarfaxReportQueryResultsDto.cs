﻿using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes.V_2_0
{
    [Serializable]
    public class CarfaxReportQueryResultsDto : CarfaxReportResultsDto
    {
        private CarfaxReportQueryArgumentsDto _arguments;

        public CarfaxReportQueryArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }
    }
}
