using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class PurchaseReportResultsDto
    {
        private PurchaseReportArgumentsDto _arguments;
        private CarfaxReportTO _report;

        public PurchaseReportArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public CarfaxReportTO Report
        {
            get { return _report; }
            set { _report = value; }
        }
    }
}
