﻿using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class PurchaseAuthorityArgumentsDto : ArgumentsDto
    {
        private ClientDto _client;

        public ClientDto Client
        {
            get { return _client; }
            set { _client = value; }
        }
    }
}
