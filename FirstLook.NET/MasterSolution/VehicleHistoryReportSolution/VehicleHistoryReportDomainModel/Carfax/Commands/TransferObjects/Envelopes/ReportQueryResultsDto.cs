using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ReportQueryResultsDto
    {
        private ReportQueryArgumentsDto _arguments;
        private CarfaxReportTO _report;

        public ReportQueryArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public CarfaxReportTO Report
        {
            get { return _report; }
            set { _report = value; }
        }

        public bool ReportAvail { get; set; }
    }
}
