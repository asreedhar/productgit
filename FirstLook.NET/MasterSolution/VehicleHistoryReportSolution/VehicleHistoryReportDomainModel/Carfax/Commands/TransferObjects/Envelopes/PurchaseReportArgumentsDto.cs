﻿using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class PurchaseReportArgumentsDto : ArgumentsDto
    {
        private ClientDto _client;
        private string _vin;
        private CarfaxReportType _reportType;
        private bool _displayInHotList;
        private CarfaxTrackingCode _trackingCode;

        public ClientDto Client
        {
            get { return _client; }
            set { _client = value; }
        }

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }

        public CarfaxReportType ReportType
        {
            get { return _reportType; }
            set { _reportType = value; }
        }

        public bool DisplayInHotList
        {
            get { return _displayInHotList; }
            set { _displayInHotList = value; }
        }

        public CarfaxTrackingCode TrackingCode
        {
            get { return _trackingCode; }
            set { _trackingCode = value; }
        }
    }
}
