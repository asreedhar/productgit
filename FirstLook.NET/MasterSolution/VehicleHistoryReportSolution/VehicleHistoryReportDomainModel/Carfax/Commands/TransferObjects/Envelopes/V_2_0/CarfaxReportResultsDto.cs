﻿using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes.V_2_0
{
    [Serializable]
    public class CarfaxReportResultsDto
    {
        private CarfaxReportResultsStatusDto _status;
        private CarfaxReportTO _report;
        private string _url;

        public CarfaxReportTO Report
        {
            get { return _report; }
            set { _report = value; }
        }

        public CarfaxReportResultsStatusDto Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public string Url
        {
            get { return _url; }
            set { _url = value; }
        }
    }
}
