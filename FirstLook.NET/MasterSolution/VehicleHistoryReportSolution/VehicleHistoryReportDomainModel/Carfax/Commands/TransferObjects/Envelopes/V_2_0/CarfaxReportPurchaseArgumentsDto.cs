using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes.V_2_0
{
    [Serializable]
    public class CarfaxReportPurchaseArgumentsDto : CarfaxReportQueryArgumentsDto
    {
        private CarfaxReportType _reportType;
        private bool _displayInHotList;

        public CarfaxReportType ReportType
        {
            get { return _reportType; }
            set { _reportType = value; }
        }

        public bool DisplayInHotList
        {
            get { return _displayInHotList; }
            set { _displayInHotList = value; }
        }
    }
}
