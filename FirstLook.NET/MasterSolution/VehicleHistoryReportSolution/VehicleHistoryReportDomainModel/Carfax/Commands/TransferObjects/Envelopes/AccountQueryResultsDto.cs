﻿using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class AccountQueryResultsDto
    {
        private AccountQueryArgumentsDto _arguments;
        private bool _hasAccount;

        public AccountQueryArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public bool HasAccount
        {
            get { return _hasAccount; }
            set { _hasAccount = value; }
        }
    }
}
