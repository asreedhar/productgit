﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ReportsQueryResultsDto
    {
        private ReportsQueryArgumentsDto _arguments;
        private List<CarfaxReportTO> _reports;

        public ReportsQueryArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public List<CarfaxReportTO> Reports
        {
            get { return _reports; }
            set { _reports = value; }
        }
    }
}
