﻿using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes.V_2_0
{
    [Serializable]
    public class CarfaxReportPurchaseResultsDto : CarfaxReportResultsDto
    {
        private CarfaxReportPurchaseArgumentsDto _arguments;

        public CarfaxReportPurchaseArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }
    }
}
