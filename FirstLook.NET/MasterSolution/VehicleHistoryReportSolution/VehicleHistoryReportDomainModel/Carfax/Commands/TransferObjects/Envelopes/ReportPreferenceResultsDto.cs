using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ReportPreferenceResultsDto
    {
        private ReportPreferenceArgumentsDto _arguments;
        private CarfaxReportPreferenceTO _preference;

        public ReportPreferenceArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public CarfaxReportPreferenceTO Preference
        {
            get { return _preference; }
            set { _preference = value; }
        }
    }
}
