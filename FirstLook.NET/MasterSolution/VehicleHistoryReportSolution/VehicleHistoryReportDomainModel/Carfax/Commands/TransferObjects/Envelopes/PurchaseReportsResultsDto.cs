﻿using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class PurchaseReportsResultsDto
    {
        private PurchaseReportsArgumentsDto _arguments;

        public PurchaseReportsArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        // TODO: Summary of purchase activity ...
    }
}
