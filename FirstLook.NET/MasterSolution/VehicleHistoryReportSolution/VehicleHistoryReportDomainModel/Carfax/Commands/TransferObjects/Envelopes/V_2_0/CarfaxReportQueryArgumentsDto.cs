using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes.V_2_0
{
    [Serializable]
    public class CarfaxReportQueryArgumentsDto
    {
        private Guid _broker;
        private string _vin;
        private VehicleEntityType _vehicleEntityType;

        public Guid Broker
        {
            get { return _broker; }
            set { _broker = value; }
        }

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }

        public VehicleEntityType VehicleEntityType
        {
            get { return _vehicleEntityType; }
            set { _vehicleEntityType = value; }
        }
    }
}
