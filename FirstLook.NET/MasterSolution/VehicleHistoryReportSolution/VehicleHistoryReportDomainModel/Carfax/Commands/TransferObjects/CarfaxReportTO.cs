using System;
using System.Collections.Generic;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects
{
	[Serializable]
	public class CarfaxReportTO
	{
		private DateTime _expirationDate;
		private string _userName;
		private string _vin;
		private CarfaxReportType _reportType;
		private bool _displayInHotList;
	    private bool _hasProblem;
		private int _ownerCount;
		private readonly List<CarfaxReportInspectionTO> _inspections = new List<CarfaxReportInspectionTO>();

		public DateTime ExpirationDate
		{
			get { return _expirationDate; }
			set { _expirationDate = value; }
		}

		public string UserName
		{
			get { return _userName; }
			set { _userName = value; }
		}

		public string Vin
		{
			get { return _vin; }
			set { _vin = value; }
		}

		public CarfaxReportType ReportType
		{
			get { return _reportType; }
			set { _reportType = value; }
		}

		public bool DisplayInHotList
		{
			get { return _displayInHotList; }
			set { _displayInHotList = value; }
		}

        public bool HasProblem
        {
            get { return _hasProblem; }
            set { _hasProblem = value; }
        }

		public int OwnerCount
		{
			get { return _ownerCount; }
			set { _ownerCount = value; }
		}

		public List<CarfaxReportInspectionTO> Inspections
		{
			get { return _inspections; }
		}
	}
}
