﻿using System;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes.V_2_0;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands
{
    [CLSCompliant(false)]
    public interface ICommandFactory
    {
        #region V_1_0
        
        ICommand<AccountQueryResultsDto, AccountQueryArgumentsDto> CreateAccountQueryCommand();

        ICommand<ReportQueryResultsDto, ReportQueryArgumentsDto> CreateReportQueryCommand();

        ICommand<ReportsQueryResultsDto, ReportsQueryArgumentsDto> CreateReportsQueryCommand();

        ICommand<ReportPreferenceResultsDto, ReportPreferenceArgumentsDto> CreateReportPreferenceCommand();

        ICommand<PurchaseAuthorityResultsDto, PurchaseAuthorityArgumentsDto> CreatePurchaseAuthorityCommand();

        ICommand<PurchaseReportResultsDto, PurchaseReportArgumentsDto> CreatePurchaseReportCommand();

        ICommand<PurchaseReportsResultsDto, PurchaseReportsArgumentsDto> CreatePurchaseReportsCommand();

        #endregion

        #region V_2_0

        ICommand<CarfaxReportQueryResultsDto, IdentityContextDto<CarfaxReportQueryArgumentsDto>> CreateQueryCommand();

        ICommand<CarfaxReportPurchaseResultsDto, IdentityContextDto<CarfaxReportPurchaseArgumentsDto>> CreatePurchaseCommand();

        #endregion
    }
}
