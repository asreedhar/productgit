﻿using FirstLook.Common.Core.Registry;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.Impl;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory, CommandFactory>(ImplementationScope.Shared);
        }
    }
}
