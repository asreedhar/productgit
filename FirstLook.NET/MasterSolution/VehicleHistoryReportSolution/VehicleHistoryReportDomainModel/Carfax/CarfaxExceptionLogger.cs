using System;
using System.Data;
using System.Diagnostics;
using System.Security.Permissions;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax
{
	[Serializable]
	public class CarfaxExceptionLogger : CommandBase
	{
		public static void Record(Exception exception)
		{
			Record(exception, null);
		}

		public static void Record(Exception exception, int? reportId)
		{
			CarfaxExceptionLogger logger = new CarfaxExceptionLogger(exception, reportId);

			DataPortal.Execute(logger);
		}

		private readonly Exception exception;
		private int? reportId;

		private CarfaxExceptionLogger(Exception exception, int? reportId)
		{
			this.exception = exception;
			this.reportId = reportId;
		}

		[Transactional(TransactionalTypes.Manual)]
		protected override void DataPortal_Execute()
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
			{
				connection.Open();

				using (IDataCommand command = new DataCommand(connection.CreateCommand()))
				{
					command.CommandType = CommandType.StoredProcedure;
					command.CommandText = "Carfax.Exception#Insert";

					command.AddParameterWithValue("MachineName", DbType.String, false, GetMachineNameWithAssert());
					command.AddParameterWithValue("Time", DbType.DateTime, false, DateTime.Now);
					command.AddParameterWithValue("Type", DbType.String, false, exception.GetType().ToString());
					command.AddParameterWithValue("Message", DbType.String, false, exception.Message);
					command.AddParameterWithValue("Detail", DbType.String, false, new StackTrace(exception).ToString());
                    
					if (reportId.HasValue)
					{
						command.AddParameterWithValue("ReportId", DbType.Int32, true, reportId.Value);
					}
					else
					{
						command.AddParameterWithValue("ReportId", DbType.String, true, DBNull.Value);
					}

					command.ExecuteNonQuery();
				}
			}
		}

		[PermissionSet(SecurityAction.Assert, Unrestricted = true)]
		private string GetMachineNameWithAssert()
		{
			return Environment.MachineName;
		}
	}
}
