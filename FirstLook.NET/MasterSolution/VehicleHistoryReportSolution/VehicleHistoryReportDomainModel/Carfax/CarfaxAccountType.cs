using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax
{
	[Serializable]
	public enum CarfaxAccountType
	{
		Undefined = 0,
		Dealer    = 1,
		System    = 2
	}
}
