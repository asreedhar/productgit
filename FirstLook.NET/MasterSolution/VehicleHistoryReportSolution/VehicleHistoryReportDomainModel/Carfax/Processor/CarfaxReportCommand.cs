using System;
using System.Data;
using Csla;
using FirstLook.VehicleHistoryReport.DomainModel.Diagnostics;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Processor
{
	[Serializable]
	public abstract class CarfaxReportCommand : CommandBase
	{
		internal static CarfaxReportCommand GetCarfaxReportCommand(CarfaxReportCommandType type, IDataRecord record)
		{
			switch (type)
			{
				case CarfaxReportCommandType.PurchaseReport:
					return new PurchaseCommand(record);
				case CarfaxReportCommandType.RenewReport:
					return new RenewCommand(record);
				case CarfaxReportCommandType.UpgradeReport:
					return new UpgradeCommand(record);
				case CarfaxReportCommandType.ChangeHotListStatus:
					return new ChangeHotListStatus(record);
				default:
					throw new ArgumentException("Not Recognised", "type");
			}
		}

		private int dealerId;
		private string vin;
		private CarfaxTrackingCode trackingCode;

		private int? reportId;

		protected CarfaxReportCommand(IDataRecord record)
		{
			Fetch(record);
		}

		public int DealerId
		{
			get { return dealerId; }
		}

		public string Vin
		{
			get { return vin; }
		}

		public CarfaxTrackingCode TrackingCode
		{
			get { return trackingCode; }
		}

		public int? ReportId
		{
			get { return reportId; }
		}

		protected void SetReportId(int newReportId)
		{
			reportId = newReportId;
		}

		private void Fetch(IDataRecord record)
		{
			dealerId = record.GetInt32(record.GetOrdinal("DealerId"));
			vin = record.GetString(record.GetOrdinal("Vin"));
			trackingCode = (CarfaxTrackingCode) Enum.Parse(typeof (CarfaxTrackingCode), record.GetString(record.GetOrdinal("TrackingCode")));
		}

		protected override void DataPortal_OnDataPortalException(DataPortalEventArgs e, Exception ex)
		{
			CarfaxException exception = ex as CarfaxException;

			if (exception != null)
			{
				SetReportId(exception.ReportId);
			}
		}

        [Serializable]
        public class PurchaseCommand : CarfaxReportCommand
		{
			public PurchaseCommand(IDataRecord record) : base(record)
			{
				Fetch(record);
			}

			private CarfaxReportType reportType;
			private bool displayInHotList;

			public CarfaxReportType ReportType
			{
				get { return reportType; }
			}

			public bool DisplayInHotList
			{
				get { return displayInHotList; }
			}

			private new void Fetch(IDataRecord record)
			{
				reportType = (CarfaxReportType)Enum.Parse(typeof(CarfaxReportType), record.GetString(record.GetOrdinal("ReportType")));
				displayInHotList = record.GetBoolean(record.GetOrdinal("DisplayInHotList"));
			}

			protected override void DataPortal_Execute()
			{
				using (ICounter counter = CounterRegistry.GetCounter(InstrumentedResponsibility.Internal, InstrumentedSystem.Processor, InstrumentedProvider.Carfax, InstrumentedOperation.ProcessorReportPurchase))
				{
					if (DealerAccount.Exists(dealerId))
					{
						DealerAccount dealerAccount = DealerAccount.GetDealerAccount(dealerId);

						CarfaxAccount account = dealerAccount.Account;

						CarfaxReport report = account.PurchaseReport(vin, reportType, displayInHotList, trackingCode);

						SetReportId(report.Id);

						counter.Success();
					}
				}
			}
		}

        [Serializable]
        public class RenewCommand : CarfaxReportCommand
		{
			public RenewCommand(IDataRecord record)
				: base(record)
			{
			}

			protected override void DataPortal_Execute()
			{
				using (ICounter counter = CounterRegistry.GetCounter(InstrumentedResponsibility.Internal, InstrumentedSystem.Processor, InstrumentedProvider.Carfax, InstrumentedOperation.ProcessorReportRenew))
				{
					if (DealerAccount.Exists(dealerId))
					{
						DealerAccount dealerAccount = DealerAccount.GetDealerAccount(dealerId);

						CarfaxAccount account = dealerAccount.Account;

						if (account.HasExpiredPurchasedReport(vin))
						{
							CarfaxReport report = account.GetReport(vin);

							report = report.Renew(trackingCode);

							SetReportId(report.Id);

							counter.Success();
						}
                        else
						{
						    String errMsg =
						        String.Format(
						            "RenewCommand called for a vin with no ExpiredPurchasedReport with the parameters: account id: {0} , vin: {1}",
						            account.Id, vin);
                            
                            throw new InvalidOperationException(errMsg);
						}


					}
				}
			}
		}

        [Serializable]
        public class UpgradeCommand : CarfaxReportCommand
		{
			public UpgradeCommand(IDataRecord record)
				: base(record)
			{
				Fetch(record);
			}

			private CarfaxReportType reportType;

			private new void Fetch(IDataRecord record)
			{
				reportType = (CarfaxReportType)Enum.Parse(typeof(CarfaxReportType), record.GetString(record.GetOrdinal("ReportType")));
			}

			public CarfaxReportType ReportType
			{
				get { return reportType; }
			}

			protected override void DataPortal_Execute()
			{
				using (ICounter counter = CounterRegistry.GetCounter(InstrumentedResponsibility.Internal, InstrumentedSystem.Processor, InstrumentedProvider.Carfax, InstrumentedOperation.ProcessorReportUpgrade))
				{
					if (DealerAccount.Exists(dealerId))
					{
						DealerAccount dealerAccount = DealerAccount.GetDealerAccount(dealerId);

						CarfaxAccount account = dealerAccount.Account;

						if (account.HasPurchasedReport(vin))
						{
							CarfaxReport report = account.GetReport(vin);

							report = report.Upgrade(trackingCode, ReportType);

							SetReportId(report.Id);

							counter.Success();
						}
					}
				}
			}
		}

        [Serializable]
        public class ChangeHotListStatus : CarfaxReportCommand
		{
			public ChangeHotListStatus(IDataRecord record)
				: base(record)
			{
				Fetch(record);
			}

			private bool displayInHotList;

			public bool DisplayInHotList
			{
				get { return displayInHotList; }
			}

			private new void Fetch(IDataRecord record)
			{
				displayInHotList = record.GetBoolean(record.GetOrdinal("DisplayInHotList"));
			}

			protected override void DataPortal_Execute()
			{
				using (ICounter counter = CounterRegistry.GetCounter(InstrumentedResponsibility.Internal, InstrumentedSystem.Processor, InstrumentedProvider.Carfax, InstrumentedOperation.ProcessorReportStatusChange))
				{
					if (DealerAccount.Exists(dealerId))
					{
						DealerAccount dealerAccount = DealerAccount.GetDealerAccount(dealerId);

						CarfaxAccount account = dealerAccount.Account;

						if (account.HasPurchasedReport(vin))
						{
							CarfaxReport report = account.GetReport(vin);

							report = report.ChangeHotListStatus(trackingCode, displayInHotList);

							SetReportId(report.Id);

							counter.Success();
						}
					}
				}
			}
		}
	}
}
