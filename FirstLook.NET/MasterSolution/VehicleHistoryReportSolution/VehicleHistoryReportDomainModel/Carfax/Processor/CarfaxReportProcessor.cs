using System;
using System.Diagnostics;
using System.Threading;
using FirstLook.VehicleHistoryReport.DomainModel.Diagnostics;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Processor
{
	public class CarfaxReportProcessor
	{
		/// <summary>
		/// Tracking variable used to stop the QueryProcessor.
		/// </summary>
		private int stopCount = 0;

		/// <summary>
		/// Adjusts the tracking variable used to stop the QueryProcessor.
		/// </summary>
		public void Stop()
		{
			if (stopCount == 0)
			{
				Interlocked.Increment(ref stopCount);
			}
		}

		/// <summary>
		/// Restarts the tracking variable used to stop the QueryProcessor.
		/// </summary>
		public void Start()
		{
			if (stopCount == 1)
			{
				Interlocked.Decrement(ref stopCount);
			}
		}

		public void Process()
		{
			while (stopCount == 0)
			{
				int delay = 60;

				using (ICounter counter = CounterRegistry.GetCounter(InstrumentedResponsibility.Internal, InstrumentedSystem.Processor, InstrumentedProvider.Carfax, InstrumentedOperation.ProcessorCommand))
				{
					try
					{
						CarfaxReportProcessorCommand command = CarfaxReportProcessorCommand.NextReportProcessorCommand();

						delay = command.Delay;

						if (command.HasWork)
						{
							command.Execute();
						}

						counter.Success();
					}
					catch (Exception e)
					{
						try
						{
							CarfaxExceptionLogger.Record(e);
						}
						catch (Exception f)
						{
							const string sourceName = "Vehicle History Report Processor";
							const string logName = "Application";

							if (!EventLog.SourceExists(sourceName))
							{
								EventLog.CreateEventSource(sourceName, logName);
							}

							EventLog log = new EventLog();
							log.Source = sourceName;
							log.WriteEntry(e.ToString(), EventLogEntryType.Error);
							log.WriteEntry(f.ToString(), EventLogEntryType.Error);
						}
					}
				}

				Thread.Sleep(delay * 1000);
			}
		}
	}
}
