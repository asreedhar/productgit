using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Processor
{
	[Serializable]
	public enum CarfaxReportCommandType
	{
		Undefined,
		PurchaseReport,
		RenewReport,
		UpgradeReport,
		ChangeHotListStatus
	}
}
