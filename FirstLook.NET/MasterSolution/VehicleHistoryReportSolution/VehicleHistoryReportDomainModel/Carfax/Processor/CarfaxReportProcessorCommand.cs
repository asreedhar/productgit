using System;
using System.Data;
using System.Diagnostics;
using System.Security.Principal;
using System.Threading;
using Csla;
using FirstLook.Common.Data;
using FirstLook.VehicleHistoryReport.DomainModel.Diagnostics;

namespace FirstLook.VehicleHistoryReport.DomainModel.Carfax.Processor
{
	[Serializable]
	public class CarfaxReportProcessorCommand
	{
		private int delay = 60;

		private CarfaxReportCommandType reportCommandType = CarfaxReportCommandType.Undefined;

		private int id;

		private string userName;

		private CarfaxReportCommand reportCommand;

		/// <summary>
		/// The delay desired before the next item of work is requested.
		/// </summary>
		public int Delay
		{
			get { return delay; }
		}

		/// <summary>
		/// If the command has a task to perform.
		/// </summary>
		public bool HasWork
		{
			get { return reportCommandType != CarfaxReportCommandType.Undefined; }
		}

		/// <summary>
		/// Identifies the the type of work embodied by this command.
		/// </summary>
		public CarfaxReportCommandType ReportCommandType
		{
			get { return reportCommandType; }
		}

		public void Execute()
		{
			try
			{
				Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(userName, "Processor"), new string[0]);

				DataPortal.Execute(reportCommand);

				HandleSuccess();
			}
			catch (Exception exception)
			{
                exception.Data.Add("CarfaxReportCommand", reportCommand); // add this for logging purposes.
                HandleException(exception);
			}
		}

		private void HandleSuccess()
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
			{
				connection.Open();

				using (IDbTransaction transaction = connection.BeginTransaction())
				{
					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = "Carfax.ReportProcessorCommand#Success";
						command.Transaction = transaction;

						command.AddParameterWithValue("Id", DbType.Int32, false, id);
						command.AddParameterWithValue("ReportId", DbType.Int32, false, reportCommand.ReportId.Value);

						command.ExecuteNonQuery();
					}

					transaction.Commit();
				}
			}
		}

		private void HandleException(Exception exception)
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
			{
				connection.Open();

				using (IDbTransaction transaction = connection.BeginTransaction())
				{
					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = "Carfax.ReportProcessorCommand#Exception";
						command.Transaction = transaction;

						command.AddParameterWithValue("Id", DbType.Int32, false, id);

						if (reportCommand.ReportId.HasValue)
						{
							command.AddParameterWithValue("ReportId", DbType.Int32, false, reportCommand.ReportId.Value);
						}
						else
						{
							command.AddParameterWithValue("ReportId", DbType.Int32, false, DBNull.Value);
						}

						command.AddParameterWithValue("MachineName", DbType.String, false, Environment.MachineName);
						command.AddParameterWithValue("Time", DbType.DateTime, false, DateTime.Now);
						command.AddParameterWithValue("Type", DbType.String, false, exception.GetType().ToString());
						command.AddParameterWithValue("Message", DbType.String, false, exception.Message);

                        CarfaxReportCommand carfaxReportCommand = exception.Data["CarfaxReportCommand"] as CarfaxReportCommand;

					    String dealerId = String.Empty;
					    String vin = String.Empty;
					    String trackingCode = String.Empty;
					    String reportId = String.Empty;
                        
                        if (carfaxReportCommand != null)
                        {
                            dealerId = carfaxReportCommand.DealerId.ToString();

                            vin = String.IsNullOrEmpty(carfaxReportCommand.Vin)
                                                  ? String.Empty
                                                  : carfaxReportCommand.Vin;

                            trackingCode = Enum.GetName(typeof(CarfaxTrackingCode), carfaxReportCommand.TrackingCode);

                            reportId = (carfaxReportCommand.ReportId != null)
                                                  ? carfaxReportCommand.ReportId.ToString()
                                                  : "Missing!";
                        }


					    String detailMessage = String.Format("StackTrace: {0}\n Message: {1}\n CarfaxReportCommand:\n" +
					                                         "DealerId: {2}, Vin: {3}, tracking code: {4} Report ID: {5}",
					                                         new StackTrace(exception).ToString(), exception.Message, dealerId, vin,
					                                         trackingCode, reportId);

                        command.AddParameterWithValue("Detail", DbType.String, false, detailMessage);

						command.ExecuteNonQuery();
					}

					transaction.Commit();
				}
			}
		}

		private CarfaxReportProcessorCommand()
		{
			/* force use of factory methods */
		}

		private CarfaxReportProcessorCommand(IDataReader reader)
		{
			Fetch(reader);
		}

		public static CarfaxReportProcessorCommand NextReportProcessorCommand()
		{
			CarfaxReportProcessorCommand processorCommand;

			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
			{
				connection.Open();

				using (IDbTransaction transaction = connection.BeginTransaction())
				{
					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = "Carfax.ReportProcessorCommand#Fetch";
						command.Transaction = transaction;

						using (IDataReader reader = command.ExecuteReader())
						{
							if (reader.Read())
							{
								processorCommand = new CarfaxReportProcessorCommand(reader);
							}
							else
							{
								processorCommand = new CarfaxReportProcessorCommand();
							}
						}
					}

					transaction.Commit();
				}
			}

			return processorCommand;
		}

		private void Fetch(IDataReader reader)
		{
			reportCommandType = (CarfaxReportCommandType) Enum.ToObject(typeof (CarfaxReportCommandType), reader.GetInt32(reader.GetOrdinal("CommandType")));

			delay = reader.GetInt32(reader.GetOrdinal("Delay"));

			id = reader.GetInt32(reader.GetOrdinal("Id"));

			userName = reader.GetString(reader.GetOrdinal("UserName"));

			if (HasWork)
			{
				if (reader.NextResult() && reader.Read())
				{
					reportCommand = CarfaxReportCommand.GetCarfaxReportCommand(reportCommandType, reader);
				}
				else
				{
					// this exception rolls-back the transaction in NextReportProcessorCommand method (as designed)

					throw new DataException("Missing Work result-set");
				}
			}
		}
	}
}
