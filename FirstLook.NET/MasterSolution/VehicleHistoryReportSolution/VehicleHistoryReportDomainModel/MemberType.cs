namespace FirstLook.VehicleHistoryReport.DomainModel
{
	public enum MemberType
	{
		Dummy = 0,
		Administrator = 1,
		User = 2,
		AccountRepresentative = 3
	}
}
