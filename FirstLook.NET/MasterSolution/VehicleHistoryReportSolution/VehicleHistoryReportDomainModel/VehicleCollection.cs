using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.VehicleHistoryReport.DomainModel
{
	[Serializable]
	public class VehicleCollection : ReadOnlyListBase<VehicleCollection, Vehicle>
	{
		#region Factory Methods

		private VehicleCollection()
		{
			/* force use of factory methods */
		}

		public static VehicleCollection GetVehicles(int dealerId, VehicleEntityType vehicleEntityType)
		{
			return DataPortal.Fetch<VehicleCollection>(new Criteria(dealerId, vehicleEntityType));
		}

		#endregion

		#region Data Access

		protected class Criteria
		{
			private readonly int _dealerId;
			private readonly VehicleEntityType _vehicleEntityType;

			public Criteria(int dealerId, VehicleEntityType vehicleEntityType)
			{
				_dealerId = dealerId;
				_vehicleEntityType = vehicleEntityType;
			}

			public int DealerId
			{
				get { return _dealerId; }
			}

			public VehicleEntityType VehicleEntityType
			{
				get { return _vehicleEntityType; }
			}
		}

		protected void DataPortal_Fetch(Criteria criteria)
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
			{
				connection.Open();

				using (IDataCommand command = new DataCommand(connection.CreateCommand()))
				{
					command.CommandType = CommandType.StoredProcedure;
					command.CommandText = "dbo.VehicleCollection#Fetch";

					command.AddParameterWithValue("DealerId", DbType.Int32, true, criteria.DealerId);
					command.AddParameterWithValue("VehicleEntityTypeId", DbType.Int32, true, (int) criteria.VehicleEntityType);

					using (IDataReader reader = command.ExecuteReader())
					{
						IsReadOnly = false;

						while (reader.Read())
						{
							Add(Vehicle.GetVehicle(reader));
						}

						IsReadOnly = true;
					}
				}
			}
		}

		#endregion
	}
}
