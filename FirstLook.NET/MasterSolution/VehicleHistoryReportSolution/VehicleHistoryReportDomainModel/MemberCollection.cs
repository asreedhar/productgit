using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.VehicleHistoryReport.DomainModel
{
	[Serializable]
	public class MemberCollection : ReadOnlyListBase<MemberCollection, Member>
	{
		public static MemberCollection GetMembers(int dealerId)
		{
			return DataPortal.Fetch<MemberCollection>(new Criteria(dealerId));
		}

		protected class Criteria
		{
			private readonly int dealerId;

			public Criteria(int dealerId)
			{
				this.dealerId = dealerId;
			}

			public int DealerId
			{
				get { return dealerId; }
			}
		}

		protected void DataPortal_Fetch(Criteria criteria)
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
			{
				connection.Open();

				using (IDataCommand command = new DataCommand(connection.CreateCommand()))
				{
					command.CommandType = CommandType.StoredProcedure;
					command.CommandText = "dbo.MemberCollection#Fetch";

					command.AddParameterWithValue("DealerId", DbType.Int32, true, criteria.DealerId);

					using (IDataReader reader = command.ExecuteReader())
					{
						IsReadOnly = false;

						while (reader.Read())
						{
							Add(Member.GetMember(reader));
						}

						IsReadOnly = true;
					}
				}
			}
		}
	}
}
