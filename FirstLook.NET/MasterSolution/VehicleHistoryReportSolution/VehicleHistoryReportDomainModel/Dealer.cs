using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.VehicleHistoryReport.DomainModel
{
	[Serializable]
	public class Dealer : ReadOnlyBase<Dealer>
	{
		private int id;
		private string name;

		public int Id
		{
			get { return id; }
		}

		public string Name
		{
			get { return name; }
		}

		protected override object GetIdValue()
		{
			return Id;
		}

		public static Dealer GetDealer(int id)
		{
			return DataPortal.Fetch<Dealer>(new Criteria(id));
		}

		public static bool Exists(int id)
		{
			ExistsCommand command = new ExistsCommand(id);
			DataPortal.Execute(command);
			return command.Exists;
		}

		[Serializable]
		protected class Criteria
		{
			private readonly int id;

			public Criteria(int id)
			{
				this.id = id;
			}

			public int Id
			{
				get { return id; }
			}
		}

		[Serializable]
		private class ExistsCommand : CommandBase
		{
			private readonly int id;
			private bool exists;

			public ExistsCommand(int id)
			{
				this.id = id;
			}

			public bool Exists
			{
				get { return exists; }
			}

			protected override void DataPortal_Execute()
			{
				using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
				{
					connection.Open();

					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = "dbo.Dealer#Exists";
						command.AddParameterWithValue("Id", DbType.Int32, true, id);

						using (IDataReader reader = command.ExecuteReader())
						{
							if (reader.Read())
							{
								exists = reader.GetBoolean(reader.GetOrdinal("Exists"));
							}
							else
							{
								exists = false;
							}
						}
					}
				}
			}
		}

		protected void DataPortal_Fetch(Criteria criteria)
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
			{
				connection.Open();

				using (IDataCommand command = new DataCommand(connection.CreateCommand()))
				{
					command.CommandType = CommandType.StoredProcedure;
					command.CommandText = "dbo.Dealer#Fetch";
					command.AddParameterWithValue("Id", DbType.Int32, true, criteria.Id);

					using (IDataReader reader = command.ExecuteReader())
					{
						if (reader.Read())
						{
							id = reader.GetInt32(reader.GetOrdinal("Id"));
							name = reader.GetString(reader.GetOrdinal("Name"));
						}
						else
						{
							throw new DataException("Missing Dealer information");
						}
					}
				}
			}
		}
	}
}