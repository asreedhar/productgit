﻿using System;
using System.Configuration;

namespace FirstLook.VehicleHistoryReport.DomainModel
{
    public static class Configuration
    {
        public static int WebRequestTimeout()
        {
            const int defaultTimeout = 10000; // 10000 milliseconds = 10 seconds

            string customTimeout = ConfigurationManager.AppSettings["WebRequest.Timeout"];

            if (!String.IsNullOrEmpty(customTimeout))
            {
                int timeout;

                if (Int32.TryParse(customTimeout, out timeout))
                {
                    return timeout;
                }
            }

            return defaultTimeout;
        }

        public static bool IsCarfaxUnavailable()
        {
            string unavailableText = ConfigurationManager.AppSettings["Carfax.Unavailable"];

            if (!string.IsNullOrEmpty(unavailableText))
            {
                bool unavailable;

                if (bool.TryParse(unavailableText, out unavailable))
                {
                    return unavailable;
                }
            }

            return false;
        }

        public static bool IsAutoCheckUnavailable()
        {
            string unavailableText = ConfigurationManager.AppSettings["AutoCheck.Unavailable"];

            if (!string.IsNullOrEmpty(unavailableText))
            {
                bool unavailable;

                if (bool.TryParse(unavailableText, out unavailable))
                {
                    return unavailable;
                }
            }

            return false;
        }
    }
}
