using System;

namespace FirstLook.VehicleHistoryReport.DomainModel
{
	[Serializable]
	public enum VehicleEntityType
	{
		Undefined = 0,
		Inventory = 1,
		Appraisal = 2
	}
}