using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Xml.XPath;
using FirstLook.VehicleHistoryReport.DomainModel.Diagnostics;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck
{
	public sealed class AutoCheckReportXmlClient : AutoCheckReportClient
	{
		private static readonly string autoCheckServiceUriText;

		static AutoCheckReportXmlClient()
		{
            string text = "http://www.experian.com/ais/servlets/VHRXML/";

			try
			{
				string value = ConfigurationManager.AppSettings["AutoCheckServiceUri"];
				if (!string.IsNullOrEmpty(value))
					text = value;
			}
			catch
			{
				// ignore error!
			}

			autoCheckServiceUriText = text;
		}
		
		private AutoCheckReportType reportType;
		
		private XPathDocument document;

		public AutoCheckReportType ReportType
		{
			get { return reportType; }
			set { reportType = value; }
		}

		public XPathDocument Document
		{
			get { return document; }
		}

		public void DoRequest()
		{
			ValidateParameters();

            if (Configuration.IsAutoCheckUnavailable())
            {
                ResponseCode = AutoCheckResponseCode.Error;

                return;
            }

			using (ICounter counter = CounterRegistry.GetCounter(InstrumentedResponsibility.External, InstrumentedSystem.WebService, InstrumentedProvider.AutoCheck, InstrumentedOperation.ExternalXml))
			{
				try
				{
					HttpWebRequest request = ConstructWebRequest();

					using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
					{
                        switch (response.StatusCode)
						{
							case HttpStatusCode.OK:
								ResponseCode = AutoCheckResponseCode.Success;
								using (StreamReader stream = new StreamReader(response.GetResponseStream()))
								{
									document = new XPathDocument(stream);
								}
								counter.Success();
								break;
							case HttpStatusCode.Forbidden:
								ResponseCode = AutoCheckResponseCode.Forbidden;
								break;
							default:
								ResponseCode = AutoCheckResponseCode.Error;
								break;
						}
					}
				}
				catch(Exception ex)
				{
                    ResponseAutoCheckReportException = ex;
                    ResponseCode = AutoCheckResponseCode.Error;
				}
			}
		}

		protected override string ConstructRequestString()
		{
			StringBuilder sb = new StringBuilder(autoCheckServiceUriText);

			sb.Append("?id=").Append(Id);
			sb.Append("&password=").Append(Password);
			sb.Append("&vinlist=").Append(Vin);

			if (!string.IsNullOrEmpty(SubId))
			{
				sb.Append("&subid=").Append(SubId);
			}

			if (ReportType != AutoCheckReportType.Undefined)
			{
				sb.Append("&level=").Append(ReportType.ToString());
			}

			return sb.ToString();
		}

		protected override string AutoCheckServiceUriText
		{
			get { return autoCheckServiceUriText; }
		}
	}
}
