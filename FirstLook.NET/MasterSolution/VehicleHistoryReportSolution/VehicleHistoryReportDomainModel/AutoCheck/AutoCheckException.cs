using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck
{
	[Serializable]
    public class AutoCheckException : Exception
    {
        private readonly int reportId;
        private readonly AutoCheckResponseCode responseCode;


        public int ReportId
        {
            get { return reportId; }
        }

        public AutoCheckResponseCode ResponseCode
        {
            get { return responseCode; }
        }



        public AutoCheckException(int reportId, AutoCheckResponseCode responseCode, String message, Exception innerException)
            : this(message, innerException)
        {
            this.reportId = reportId;
            this.responseCode = responseCode;
        }

        public AutoCheckException(AutoCheckResponseCode responseCode)
        {
            this.reportId = -1;
            this.responseCode = responseCode;
        }

        public AutoCheckException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            reportId = info.GetInt32("ReportId");
            responseCode = (AutoCheckResponseCode)info.GetValue("ResponseCode", typeof(AutoCheckResponseCode));
        }

        public AutoCheckException(string message, Exception innerException)
            : base(message, innerException)
        {

        }

        public AutoCheckException(string message)
            : base(message)
        {

        }

        public AutoCheckException()
        {

        }



        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null) throw new ArgumentNullException("info");
            base.GetObjectData(info, context);
            info.AddValue("ReportId", ReportId);
            info.AddValue("ResponseCode", responseCode, typeof(AutoCheckResponseCode));
        }
    }
}
