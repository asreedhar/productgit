using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck
{
	[Serializable]
	public enum AutoCheckAccountType
	{
		Undefined = 0,
		Dealer    = 1,
		System    = 2,
        Service   = 3
	}
}
