using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck
{
	[Serializable]
	public class AutoCheckReportInspectionResultCollection : BusinessListBase<AutoCheckReportInspectionResultCollection, AutoCheckReportInspectionResult>
	{
		private AutoCheckReportInspectionResultCollection()
		{
			/* force use of factory methods */
		}

		private AutoCheckReportInspectionResultCollection(IDataReader reader)
		{
			RaiseListChangedEvents = false;

			while (reader.Read())
			{
				Add(AutoCheckReportInspectionResult.GetReportInspectionResult(reader));
			}

			RaiseListChangedEvents = true;
		}

		public static AutoCheckReportInspectionResultCollection NewReportInspectionResults()
		{
			return new AutoCheckReportInspectionResultCollection();
		}

		internal void DoUpdate(IDataConnection connection, IDbTransaction transaction, AutoCheckReport report)
		{
			RaiseListChangedEvents = false;

			//foreach (AutoCheckReportInspectionResult assignment in DeletedList)
			//{
			//    assignment.DeleteSelf(connection, transaction, account);
			//}

			//DeletedList.Clear();

			foreach (AutoCheckReportInspectionResult inspectionResult in this)
			{
				if (inspectionResult.IsNew)
				{
					inspectionResult.Insert(connection, transaction, report);
				}
			}

			RaiseListChangedEvents = true;
		}

		internal static AutoCheckReportInspectionResultCollection GetReportInspectionResults(IDataReader reader)
		{
			return new AutoCheckReportInspectionResultCollection(reader);
		}
	}
}
