using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.Security.Authentication;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck
{
	[Serializable]
	public class AutoCheckReport : BusinessBase<AutoCheckReport>
    {
        #region Business Methods

        private int id;
		private int accountId;
		private string vin;
		private AutoCheckReportType reportType;
		private XPathDocument document;
        private int score;
        private int compareScoreRangeLow;
        private int compareScoreRangeHigh;
        private bool assured;
        private int ownerCount;
        private AutoCheckReportInspectionResultCollection inspections;

		public int Id
		{
			get { return id; }
		}

		public int AccountId
		{
			get { return accountId; }
		}

		public AutoCheckAccount Account
		{
			get { return AutoCheckAccount.GetAccount(accountId); }
		}

		public string Vin
		{
			get { return vin; }
		}

		public AutoCheckReportType ReportType
		{
			get { return reportType; }
		}

		public XPathDocument Document
		{
			get { return document; }
		}

        public int Score
        {
            get { return score; }
        }

        public int CompareScoreRangeLow
        {
            get { return compareScoreRangeLow; }
        }

        public int CompareScoreRangeHigh
        {
            get { return compareScoreRangeHigh; }
        }

        public bool Assured
        {
            get { return assured; }
        }

        public int OwnerCount
        {
            get { return ownerCount; }
        }

		public AutoCheckReportInspectionResultCollection Inspections
		{
			get { return inspections; }
		}

		protected override object GetIdValue()
		{
			return Id;
        }

		public AutoCheckReport Renew()
		{
			return NewReport(accountId, vin, ReportType);
		}

		public string GetHtml()
		{
			AutoCheckAccount serviceAccount = ServiceAccount.GetServiceAccount(AutoCheckServiceType.Html).Account;

			AutoCheckAccount account = Account;

			AutoCheckReportHtmlClient client = new AutoCheckReportHtmlClient();
			client.Id = serviceAccount.UserName;
			client.Password = serviceAccount.Password;
			client.SubId = account.UserName;
			client.Vin = Vin;
			client.DoRequest();

			if (client.IsSuccess())
			{
				return client.Document;
			}
			else
			{
                throw new AutoCheckException(id, client.ResponseCode, "Fail to get html for Autocheck Report", client.ResponseAutoCheckReportException);
			}
		}

		protected void Purchase()
		{
			AutoCheckAccount serviceAccount = ServiceAccount.GetServiceAccount(AutoCheckServiceType.Xml).Account;

			AutoCheckAccount account = Account;

			AutoCheckReportXmlClient client = new AutoCheckReportXmlClient();
			client.Id = serviceAccount.UserName;
			client.Password = serviceAccount.Password;
			client.SubId = account.UserName;
			client.Vin = Vin;
			client.ReportType = ReportType;
			client.DoRequest();

			DataPortal.Execute(new ResponseCommand(Id, (int)client.ResponseCode));

			if (client.IsSuccess())
			{
				document = client.Document;

				XPathNavigator navigator = document.CreateNavigator();

				// <SCORE OWNER_COUNT="4" AGE="15" SCORE="77" COMPARE_SCORE_RANGE_HIGH="71" COMPARE_SCORE_RANGE_HIGH="79" >

				XPathNavigator node = navigator.SelectSingleNode("/VHR/VEHICLE/SCORE");

				if (node != null)
				{
					int.TryParse(node.GetAttribute("OWNER_COUNT", string.Empty), out ownerCount);
					int.TryParse(node.GetAttribute("SCORE", string.Empty), out score);
					int.TryParse(node.GetAttribute("COMPARE_SCORE_RANGE_LOW", ""), out compareScoreRangeLow);
					int.TryParse(node.GetAttribute("COMPARE_SCORE_RANGE_HIGH", ""), out compareScoreRangeHigh);
				}

				node = navigator.SelectSingleNode("/VHR/VEHICLE/HISTORY");

				if (node != null)
				{
					assured = "Y".Equals(node.GetAttribute("ASSURED", string.Empty));
				}

				AutoCheckReportInspectionCollection referenceInspections = AutoCheckReportInspectionCollection.GetReportInspections();

				foreach (AutoCheckReportInspection inspection in referenceInspections)
				{
					node = navigator.SelectSingleNode(inspection.XPath);

					bool selected = (node == null);

					inspections.Add(AutoCheckReportInspectionResult.NewReportInspectionResult(inspection.Id, selected));
				}
			}
			else
			{
                throw new AutoCheckException(id, client.ResponseCode, "Fail to purchase Autocheck Report",client.ResponseAutoCheckReportException);
			}
		}

        #endregion Business Methods

        #region Factory Methods

        private AutoCheckReport()
		{
			/* force use of factory methods */
		}

		internal static AutoCheckReport NewReport(int accountId, string vin, AutoCheckReportType reportType)
		{
			AutoCheckReport report = DataPortal.Create<AutoCheckReport>(new CreateCriteria(accountId, vin, reportType));
			report.Purchase();
			report.Save();
			return report;
		}

		internal static AutoCheckReport GetReport(int accountId, string vin)
		{
			return DataPortal.Fetch<AutoCheckReport>(new FetchCriteria(accountId, vin));
        }

        #endregion Factory Methods

        #region Data Access

        [Serializable]
		protected class ResponseCommand : CommandBase
		{
			private readonly int reportId;
			private readonly int responseCode;

			public ResponseCommand(int reportId, int responseCode)
			{
				this.reportId = reportId;
				this.responseCode = responseCode;
			}

			[Transactional(TransactionalTypes.Manual)]
			protected override void DataPortal_Execute()
			{
				using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.AutoCheckDatabase))
				{
					connection.Open();

					using (IDbTransaction transaction = connection.BeginTransaction())
					{
						using (IDataCommand command = new DataCommand(connection.CreateCommand()))
						{
							command.CommandType = CommandType.StoredProcedure;
							command.CommandText = "AutoCheck.Report#ResponseCode";
							command.Transaction = transaction;

							command.AddParameterWithValue("ReportId", DbType.Int32, false, reportId);
							command.AddParameterWithValue("ResponseCode", DbType.Int32, false, responseCode);

							command.ExecuteNonQuery();
						}

						transaction.Commit();
					}
				}
			}
		}

		[Serializable]
		protected class CreateCriteria
		{
			private readonly int accountId;
			private readonly string vin;
			private readonly AutoCheckReportType reportType;

			public CreateCriteria(int accountId, string vin, AutoCheckReportType reportType)
			{
				this.accountId = accountId;
				this.vin = vin;
				this.reportType = reportType;
			}

			public int AccountId
			{
				get { return accountId; }
			}

			public string Vin
			{
				get { return vin; }
			}

			public AutoCheckReportType ReportType
			{
				get { return reportType; }
			}
		}

		[Serializable]
		protected class FetchCriteria
		{
			private readonly int accountId;
			private readonly string vin;

			public FetchCriteria(int accountId, string vin)
			{
				this.accountId = accountId;
				this.vin = vin;
			}

			public int AccountId
			{
				get { return accountId; }
			}

			public string Vin
			{
				get { return vin; }
			}
		}

		[Transactional(TransactionalTypes.Manual)]
		protected void DataPortal_Create(CreateCriteria criteria)
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.AutoCheckDatabase))
			{
				connection.Open();

				using (IDbTransaction transaction = connection.BeginTransaction())
				{
					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = "AutoCheck.Report#Create";
						command.Transaction = transaction;

						command.AddParameterWithValue("AccountId", DbType.Int32, false, criteria.AccountId);
						command.AddParameterWithValue("Vin", DbType.String, false, criteria.Vin);
						command.AddParameterWithValue("ReportTypeID", DbType.Byte, false, (byte) criteria.ReportType);
						command.AddParameterWithValue("InsertUser", DbType.String, false, Thread.CurrentPrincipal.Identity.Name);

						IDataParameter newId = command.AddOutParameter("Id", DbType.Int32);

						command.ExecuteNonQuery();

						id = (int) newId.Value;

						accountId = criteria.AccountId;

						vin = criteria.Vin;

						reportType = criteria.ReportType;

						inspections = AutoCheckReportInspectionResultCollection.NewReportInspectionResults();
					}

					transaction.Commit();
				}
			}
		}

		[Transactional(TransactionalTypes.Manual)]
		protected void DataPortal_Fetch(FetchCriteria criteria)
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.AutoCheckDatabase))
			{
				connection.Open();

				using (IDataCommand command = new DataCommand(connection.CreateCommand()))
				{
					command.CommandType = CommandType.StoredProcedure;
					command.CommandText = "AutoCheck.Report#Fetch";

					command.AddParameterWithValue("AccountId", DbType.Int32, false, criteria.AccountId);
					command.AddParameterWithValue("Vin", DbType.String, false, criteria.Vin);

					using (IDataReader reader = command.ExecuteReader())
					{
						if (reader.Read())
						{
							Fetch(reader);
						}
						else
						{
							throw new DataException("Expected AutoCheck Report data!");
						}
					}
				}
			}
		}

		private const string IdentityTransform = "<xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" version=\"1.0\"><xsl:template match=\"node()|@*\"><xsl:copy><xsl:apply-templates select=\"@*\"/><xsl:apply-templates/></xsl:copy></xsl:template></xsl:stylesheet>";

		[Transactional(TransactionalTypes.Manual)]
		protected override void DataPortal_Insert()
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.AutoCheckDatabase))
			{
				connection.Open();

				using (IDbTransaction transaction = connection.BeginTransaction())
				{
					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = "AutoCheck.Report#Insert";
						command.Transaction = transaction;

						StringBuilder sb = new StringBuilder();

						using (StringReader sr = new StringReader(IdentityTransform))
						{
							using (XmlReader xr = XmlReader.Create(sr))
							{
								using (StringWriter sw = new StringWriter(sb))
								{
									using (XmlWriter xw = XmlWriter.Create(sw))
									{
										XslCompiledTransform xt = new XslCompiledTransform();
										xt.Load(xr);
										xt.Transform(Document, xw);
									}
								}
							}
						}

						command.AddParameterWithValue("Id", DbType.Int32, false, Id);
						command.AddParameterWithValue("Document", DbType.Xml, false, sb.ToString());

                        command.AddParameterWithValue("OwnerCount", DbType.Int32, false, OwnerCount);
                        command.AddParameterWithValue("Score", DbType.Int32, false, Score);
                        command.AddParameterWithValue("CompareScoreRangeLow", DbType.Int32, false, CompareScoreRangeLow);
                        command.AddParameterWithValue("CompareScoreRangeHigh", DbType.Int32, false, CompareScoreRangeHigh);
                        command.AddParameterWithValue("Assured", DbType.Boolean, false, Assured);

						command.ExecuteNonQuery();
					}

					inspections.DoUpdate(connection, transaction, this);

					transaction.Commit();
				}
			}
		}

		private void Fetch(IDataReader reader)
		{
			id = reader.GetInt32(reader.GetOrdinal("Id"));
			accountId = reader.GetInt32(reader.GetOrdinal("AccountId"));
			vin = reader.GetString(reader.GetOrdinal("Vin"));
			reportType = (AutoCheckReportType) Enum.ToObject(typeof (AutoCheckReportType), reader.GetInt32(reader.GetOrdinal("ReportTypeID")));

			SqlDataReader sqlReader = reader as SqlDataReader;

			if (sqlReader != null)
			{
				SqlXml xml = sqlReader.GetSqlXml(reader.GetOrdinal("Document"));

				using (XmlReader xmlReader = xml.CreateReader())
				{
					document = new XPathDocument(xmlReader);
				}
			}


            ownerCount = reader.GetInt32(reader.GetOrdinal("OwnerCount"));
            score = reader.GetInt32(reader.GetOrdinal("Score"));
            compareScoreRangeLow = reader.GetInt32(reader.GetOrdinal("CompareScoreRangeLow"));
            compareScoreRangeHigh = reader.GetInt32(reader.GetOrdinal("CompareScoreRangeHigh"));
            assured = reader.GetBoolean(reader.GetOrdinal("Assured"));


			if (reader.NextResult())
			{
				inspections = AutoCheckReportInspectionResultCollection.GetReportInspectionResults(reader);
			}
			else
			{
				inspections = AutoCheckReportInspectionResultCollection.NewReportInspectionResults();
			}
        }

        #endregion Data Access
    }
}
