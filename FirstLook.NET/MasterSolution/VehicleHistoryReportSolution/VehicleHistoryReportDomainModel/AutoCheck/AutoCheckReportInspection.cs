using System;
using System.Data;
using Csla;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck
{
	[Serializable]
	public class AutoCheckReportInspection : ReadOnlyBase<AutoCheckReportInspection>
	{
		private int id;
		private string name;
		private string xpath;

		public int Id
		{
			get { return id; }
		}

		public string Name
		{
			get { return name; }
		}

		public string XPath
		{
			get { return xpath; }
		}

		protected override object GetIdValue()
		{
			return Id;
		}

		private AutoCheckReportInspection(IDataRecord record)
		{
			Fetch(record);
		}

		public static AutoCheckReportInspection GetReportInspection(IDataRecord record)
		{
			return new AutoCheckReportInspection(record);
		}

		private void Fetch(IDataRecord record)
		{
			id = record.GetInt32(record.GetOrdinal("Id"));
			name = record.GetString(record.GetOrdinal("Name"));
			xpath = record.GetString(record.GetOrdinal("XPath"));
		}
	}
}
