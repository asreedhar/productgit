using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck
{
	[Serializable]
	public enum AutoCheckResponseCode
	{
		Undefined = 0,
		Success   = 200,
		Forbidden = 403,
		Error     = 500
	}
}
