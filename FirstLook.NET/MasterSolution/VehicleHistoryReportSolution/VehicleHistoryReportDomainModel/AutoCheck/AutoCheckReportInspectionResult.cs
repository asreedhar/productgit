using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck
{
	[Serializable]
	public class AutoCheckReportInspectionResult : BusinessBase<AutoCheckReportInspectionResult>
	{
		private int reportInspectionId;
		private bool selected;

		public int ReportInspectionId
		{
			get { return reportInspectionId; }
		}

		public bool Selected
		{
			get
			{
				CanReadProperty("Selected", true);

				return selected;
			}
		}

		protected override object GetIdValue()
		{
			return reportInspectionId;
		}

		private AutoCheckReportInspectionResult()
		{
			MarkAsChild();
		}

		private AutoCheckReportInspectionResult(int reportInspectionId, bool selected) : this()
		{
			this.reportInspectionId = reportInspectionId;
			this.selected = selected;
		}

		private AutoCheckReportInspectionResult(IDataRecord record) : this()
		{
			Fetch(record);
		}

		internal static AutoCheckReportInspectionResult NewReportInspectionResult(int reportInspectionId, bool selected)
		{
			return new AutoCheckReportInspectionResult(reportInspectionId, selected);
		}

		internal static AutoCheckReportInspectionResult GetReportInspectionResult(IDataRecord record)
		{
			return new AutoCheckReportInspectionResult(record);
		}

		private void Fetch(IDataRecord record)
		{
			reportInspectionId = record.GetInt32(record.GetOrdinal("ReportInspectionId"));

			selected = record.GetBoolean(record.GetOrdinal("Selected"));

			MarkOld();
		}

		internal void Insert(IDataConnection connection, IDbTransaction transaction, AutoCheckReport report)
		{
			using (IDataCommand command = new DataCommand(connection.CreateCommand()))
			{
				command.CommandType = CommandType.StoredProcedure;
				command.CommandText = "AutoCheck.ReportInspectionResult#Insert";
				command.Transaction = transaction;

				command.AddParameterWithValue("ReportId", DbType.Int32, false, report.Id);
				command.AddParameterWithValue("ReportInspectionId", DbType.Int32, false, ReportInspectionId);
				command.AddParameterWithValue("Selected", DbType.Boolean, false, Selected);

				command.ExecuteNonQuery();
			}

			MarkOld();
		}
	}
}