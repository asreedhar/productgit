using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck
{
	[Serializable]
	public enum AutoCheckReportType
	{
		Undefined  = 0,
		Check      = 1,
		Lemon      = 2,
		Summary    = 3,
		Indicators = 4,
		Full       = 5
	}
}
