using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck
{
	[Serializable]
	public class SystemAccount : BusinessBase<SystemAccount>
	{
		#region Business Methods
		
		private AutoCheckAccount account;

		public AutoCheckAccount Account
		{
			get { return account; }
		}

		protected override object GetIdValue()
		{
			return account.Id;
		}

		#endregion

		#region Validation

		public override bool IsDirty
		{
			get
			{
				return base.IsDirty || Account.IsDirty;
			}
		}

		public override bool IsValid
		{
			get
			{
				return base.IsValid && Account.IsValid;
			}
		}

		#endregion

		#region Factory Methods

		public static bool Exists()
		{
			ExistsCommand command = new ExistsCommand();
			DataPortal.Execute(command);
			return command.Exists;
		}

		public static SystemAccount GetSystemAccount()
		{
			return DataPortal.Fetch<SystemAccount>(new Criteria());
		}

		public static SystemAccount NewSystemAccount()
		{
			return new SystemAccount();
		}

		private SystemAccount()
		{
			account = AutoCheckAccount.NewAccount(AutoCheckAccountType.System);
		}

		#endregion

		#region Data Access

		public class DataSource
		{
			public AutoCheckAccount Select()
			{
				if (Exists())
				{
					return GetSystemAccount().Account;
				}

				return null;
			}

			public void Insert(string userName)
			{
				SystemAccount systemAccount = NewSystemAccount();
				systemAccount.Account.Active = true;
				systemAccount.Account.UserName = userName;
				systemAccount.Save();
			}

			public void Update(int id, string userName)
			{
				SystemAccount systemAccount = GetSystemAccount();
				systemAccount.Account.Active = true;
				systemAccount.Account.UserName = userName;
				systemAccount.Save();
			}

			public void Delete()
			{
				
			}
		}

		[Serializable]
		protected class Criteria
		{
			
		}

		[Serializable]
		private class ExistsCommand : CommandBase
		{
			private bool exists;

			public bool Exists
			{
				get { return exists; }
			}

			[Transactional(TransactionalTypes.Manual)]
			protected override void DataPortal_Execute()
			{
				using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.AutoCheckDatabase))
				{
					connection.Open();

					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = "AutoCheck.SystemAccount#Exists";

						using (IDataReader reader = command.ExecuteReader())
						{
							if (reader.Read())
							{
								exists = reader.GetBoolean(reader.GetOrdinal("Exists"));
							}
							else
							{
								exists = false;
							}
						}
					}
				}
			}
		}

		[Transactional(TransactionalTypes.Manual)]
		protected void DataPortal_Fetch(Criteria criteria)
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.AutoCheckDatabase))
			{
				connection.Open();

				using (IDataCommand command = new DataCommand(connection.CreateCommand()))
				{
					command.CommandType = CommandType.StoredProcedure;
					command.CommandText = "AutoCheck.SystemAccount#Fetch";

					using (IDataReader reader = command.ExecuteReader())
					{
						if (reader.Read())
						{
							account = AutoCheckAccount.GetAccount(reader);
						}
						else
						{
							throw new DataException("Missing System information");
						}
					}
				}
			}
		}

		[Transactional(TransactionalTypes.Manual)]
		protected override void DataPortal_Insert()
		{
			DoInsertUpdate();
		}

		[Transactional(TransactionalTypes.Manual)]
		protected override void DataPortal_Update()
		{
			DoInsertUpdate();
		}

		private void DoInsertUpdate()
		{
			if (!account.IsDirty) return;

			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.AutoCheckDatabase))
			{
				connection.Open();

				using (IDbTransaction transaction = connection.BeginTransaction())
				{
					if (account.IsNew)
					{
						account.DoInsert(connection, transaction);
					}
					else
					{
						account.DoUpdate(connection, transaction);
					}
					
					transaction.Commit();
				}
			}

			MarkOld();
		}

		#endregion
	}
}
