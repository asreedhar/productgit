using System;
using System.Net;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck
{
	public abstract class AutoCheckReportClient
	{
		private string id;
		private string subId;
		private string password;
		private string vin;

		private AutoCheckResponseCode responseCode;
	    private Exception responseAutoCheckReportException = null;

		public string Id
		{
			get { return id; }
			set { id = value; }
		}

		public string Password
		{
			get { return password; }
			set { password = value; }
		}

		public string SubId
		{
			get { return subId; }
			set { subId = value; }
		}

		public string Vin
		{
			get { return vin; }
			set { vin = value; }
		}

		public AutoCheckResponseCode ResponseCode
		{
			get { return responseCode; }
			protected set { responseCode = value; }
		}

        public Exception ResponseAutoCheckReportException
        {
            get { return responseAutoCheckReportException; }
            set { responseAutoCheckReportException = value; }
        }

		protected virtual void ValidateParameters()
		{
			if (string.IsNullOrEmpty(Id))
			{
				throw new InvalidOperationException("UserName is a required property");
			}

			if (string.IsNullOrEmpty(Password))
			{
				throw new InvalidOperationException("Password is a required property");
			}

			if (string.IsNullOrEmpty(Vin))
			{
				throw new InvalidOperationException("VIN is a required property");
			}

			if (Vin.Length != 17)
			{
				throw new InvalidOperationException("VIN has invalid length");
			}
		}

		public bool IsSuccess()
		{
			return ResponseCode == AutoCheckResponseCode.Success;
		}

		protected HttpWebRequest ConstructWebRequest()
		{
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConstructRequestString());
			request.Method = "POST";
			request.ContentType = "text/xml";
			request.AllowAutoRedirect = true;
		    request.Timeout = Configuration.WebRequestTimeout();
			return request;
		}

		protected abstract string ConstructRequestString();

		protected abstract string AutoCheckServiceUriText { get; }

	}
}
