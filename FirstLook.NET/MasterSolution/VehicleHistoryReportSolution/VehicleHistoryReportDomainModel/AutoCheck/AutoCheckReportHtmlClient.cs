using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using FirstLook.VehicleHistoryReport.DomainModel.Diagnostics;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck
{
	public sealed class AutoCheckReportHtmlClient : AutoCheckReportClient
	{
		private static readonly string autoCheckServiceUriText;

		static AutoCheckReportHtmlClient()
		{
            string text = "http://www.autocheck.com/DealerWebLink.jsp";

			try
			{
				string value = ConfigurationManager.AppSettings["AutoCheckServiceUri"];
				if (!string.IsNullOrEmpty(value))
					text = value;
			}
			catch
			{
				// ignore error!
			}

			autoCheckServiceUriText = text;
		}
		
		private string document;

		public string Document
		{
			get { return document; }
		}

		public void DoRequest()
		{
            ValidateParameters();

            if (Configuration.IsAutoCheckUnavailable())
            {
                ResponseCode = AutoCheckResponseCode.Error;

                return;
            }

			using (ICounter counter = CounterRegistry.GetCounter(InstrumentedResponsibility.External, InstrumentedSystem.WebService, InstrumentedProvider.AutoCheck, InstrumentedOperation.ExternalHtml))
			{
				try
				{
					HttpWebRequest request = ConstructWebRequest();

					using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
					{   
                        switch (response.StatusCode)
						{
							case HttpStatusCode.OK:
								ResponseCode = AutoCheckResponseCode.Success;
								using (StreamReader stream = new StreamReader(response.GetResponseStream()))
								{
									document = stream.ReadToEnd();
								}
								counter.Success();
								break;
							case HttpStatusCode.Forbidden:
								ResponseCode = AutoCheckResponseCode.Forbidden;
								break;
							default:
								ResponseCode = AutoCheckResponseCode.Error;
								break;
						}
					}
				}
				catch(Exception ex)
				{
				    ResponseAutoCheckReportException = ex;
                    ResponseCode = AutoCheckResponseCode.Error;
				}
			}
		}

		protected override void ValidateParameters()
		{
			base.ValidateParameters();

			if (string.IsNullOrEmpty(SubId))
			{
				throw new InvalidOperationException("SubId is a required property");
			}
		}

		protected override string ConstructRequestString()
		{
			StringBuilder sb = new StringBuilder(autoCheckServiceUriText);

			sb.Append("?CID=").Append(Id);
			sb.Append("&PWD=").Append(Password);
			sb.Append("&SID=").Append(SubId);
			sb.Append("&VIN=").Append(Vin);
			sb.Append("&REPORT="); // THE JAVA CODE HAS NO VALUE AND I HAVE NO DOCUMENTATION
			
			return sb.ToString();
		}

		protected override string AutoCheckServiceUriText
		{
			get { return autoCheckServiceUriText; }
		}
	}
}
