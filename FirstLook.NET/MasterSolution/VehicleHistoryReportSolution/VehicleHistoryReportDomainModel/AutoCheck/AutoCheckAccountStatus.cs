using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck
{
	[Serializable]
	public enum AutoCheckAccountStatus
	{
		New,
		Okay,
		BadUserNamePassword
	}
}
