using System;
using System.Data;
using Csla;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck
{
	[Serializable]
	public class AutoCheckAccountAssignmentCollection : BusinessListBase<AutoCheckAccountAssignmentCollection, AutoCheckAccountAssignment>
	{
		#region Business Methods

		public AutoCheckAccountAssignment Find(int memberId)
		{
			foreach (AutoCheckAccountAssignment res in this)
				if (res.MemberId.Equals(memberId))
					return res;
			return null;
		}

		public void AssignTo(int memberId)
		{
			if (!Contains(memberId))
			{
				Add(AutoCheckAccountAssignment.NewAutoCheckAccountAssignment(memberId));
			}
			else
			{
				throw new InvalidOperationException("Resource already assigned to project");
			}
		}

		public void Remove(int memberId)
		{
			foreach (AutoCheckAccountAssignment res in this)
			{
				if (res.MemberId.Equals(memberId))
				{
					Remove(res);
					break;
				}
			}
		}

		public bool Contains(int memberId)
		{
			foreach (AutoCheckAccountAssignment project in this)
				if (project.MemberId == memberId)
					return true;
			return false;
		}

		public bool ContainsDeleted(int memberId)
		{
			foreach (AutoCheckAccountAssignment project in DeletedList)
				if (project.MemberId == memberId)
					return true;
			return false;
		}

		#endregion

		private AutoCheckAccountAssignmentCollection()
		{
			/* force use of factory methods */
		}

		private AutoCheckAccountAssignmentCollection(IDataReader reader)
		{
			RaiseListChangedEvents = false;

			while (reader.Read())
			{
				Add(AutoCheckAccountAssignment.GetAutoCheckAccountAssignment(reader));
			}

			RaiseListChangedEvents = true;
		}

		public static AutoCheckAccountAssignmentCollection NewAutoCheckAccountAssignments()
		{
			return new AutoCheckAccountAssignmentCollection();
		}

		internal static AutoCheckAccountAssignmentCollection GetAutoCheckAccountAssignments(IDataReader reader)
		{
			return new AutoCheckAccountAssignmentCollection(reader);
		}

		internal void DoUpdate(IDbConnection connection, IDbTransaction transaction, AutoCheckAccount account)
		{
			RaiseListChangedEvents = false;

			foreach (AutoCheckAccountAssignment assignment in DeletedList)
			{
				assignment.DeleteSelf(connection, transaction, account);
			}

			DeletedList.Clear();

			foreach (AutoCheckAccountAssignment assignment in this)
			{
				if (assignment.IsNew)
				{
					assignment.Insert(connection, transaction, account);
				}
			}

			RaiseListChangedEvents = true;
		}
	}
}
