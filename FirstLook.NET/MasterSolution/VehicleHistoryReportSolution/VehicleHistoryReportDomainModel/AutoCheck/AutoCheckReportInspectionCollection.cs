using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck
{
	[Serializable]
	public class AutoCheckReportInspectionCollection : ReadOnlyListBase<AutoCheckReportInspectionCollection, AutoCheckReportInspection>
	{
		public AutoCheckReportInspection Find(int reportInspectionId)
		{
			foreach (AutoCheckReportInspection res in this)
				if (res.Id == reportInspectionId)
					return res;
			return null;
		}

		private AutoCheckReportInspectionCollection()
		{
			/* force use of factory methods */
		}

		public static AutoCheckReportInspectionCollection GetReportInspections()
		{
			return DataPortal.Fetch<AutoCheckReportInspectionCollection>(new EmptyCriteria());
		}

		[Serializable]
		protected class EmptyCriteria
		{
			
		}

		protected void DataPortal_Fetch(EmptyCriteria criteria)
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.AutoCheckDatabase))
			{
				connection.Open();

				using (IDataCommand command = new DataCommand(connection.CreateCommand()))
				{
					command.CommandType = CommandType.StoredProcedure;
					command.CommandText = "AutoCheck.ReportInspectionCollection#Fetch";

					using (IDataReader reader = command.ExecuteReader())
					{
						IsReadOnly = false;
						while (reader.Read())
						{
							Add(AutoCheckReportInspection.GetReportInspection(reader));
						}
						IsReadOnly = true;
					}
				}
			}
		}
	}
}
