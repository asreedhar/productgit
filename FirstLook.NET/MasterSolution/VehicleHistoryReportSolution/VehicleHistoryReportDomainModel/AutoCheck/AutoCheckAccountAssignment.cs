using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck
{
	[Serializable]
	public class AutoCheckAccountAssignment : BusinessBase<AutoCheckAccountAssignment>
	{
		private int memberId;
		private DateTime assigned = DateTime.MinValue;

		public int MemberId
		{
			get { return memberId; }
		}

		public DateTime Assigned
		{
			get { return assigned; }
		}

		protected override object GetIdValue()
		{
			return MemberId;
		}

		private AutoCheckAccountAssignment()
		{
			MarkAsChild();
		}

		private AutoCheckAccountAssignment(int memberId) : this()
		{
			this.memberId = memberId;
			
			assigned = DateTime.Now;
		}

		private AutoCheckAccountAssignment(IDataRecord record) : this()
		{
			Fetch(record);
		}

		internal static AutoCheckAccountAssignment GetAutoCheckAccountAssignment(IDataRecord record)
		{
			return new AutoCheckAccountAssignment(record);
		}

		public static AutoCheckAccountAssignment NewAutoCheckAccountAssignment(int memberId)
		{
			return new AutoCheckAccountAssignment(memberId);
		}

		private void Fetch(IDataRecord record)
		{
			memberId = record.GetInt32(record.GetOrdinal("MemberId"));

			assigned = record.GetDateTime(record.GetOrdinal("Assigned"));

			MarkOld();
		}

		public void DeleteSelf(IDbConnection connection, IDbTransaction transaction, AutoCheckAccount account)
		{
			using (IDataCommand command = new DataCommand(connection.CreateCommand()))
			{
				command.CommandType = CommandType.StoredProcedure;
				command.CommandText = "AutoCheck.Account#DeleteAssignment";
				command.Transaction = transaction;

				command.AddParameterWithValue("AccountId", DbType.Int32, false, account.Id);
				command.AddParameterWithValue("MemberId", DbType.Int32, false, MemberId);
				command.AddParameterWithValue("DeleteUser", DbType.String, false, System.Threading.Thread.CurrentPrincipal.Identity.Name);

				command.ExecuteNonQuery();
			}

			MarkNew();
		}

		public void Insert(IDbConnection connection, IDbTransaction transaction, AutoCheckAccount account)
		{
			using (IDataCommand command = new DataCommand(connection.CreateCommand()))
			{
				command.CommandType = CommandType.StoredProcedure;
				command.CommandText = "AutoCheck.Account#AddAssignment";
				command.Transaction = transaction;

				command.AddParameterWithValue("AccountId", DbType.Int32, false, account.Id);
				command.AddParameterWithValue("MemberId", DbType.Int32, false, MemberId);
				command.AddParameterWithValue("InsertUser", DbType.String, false, System.Threading.Thread.CurrentPrincipal.Identity.Name);

				command.ExecuteNonQuery();
			}

			MarkOld();
		}
	}
}
