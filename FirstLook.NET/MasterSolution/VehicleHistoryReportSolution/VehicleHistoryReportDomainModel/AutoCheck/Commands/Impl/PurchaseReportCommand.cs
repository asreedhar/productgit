using System;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects.Envelopes;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.Impl
{
    public class PurchaseReportCommand : ICommand<PurchaseReportResultsDto, PurchaseReportArgumentsDto>
    {
        public PurchaseReportResultsDto Execute(PurchaseReportArgumentsDto parameters)
        {
            if (parameters.Client.ClientType != ClientTypeDto.Dealer)
            {
                throw new NotSupportedException(string.Format("ClientType.{0}", parameters.Client.ClientType));
            }

            int dealerId = parameters.Client.Id;

            AutoCheckReportTO report = null;

            Member member = Mapper.GetMember(parameters.UserName);

            if (member != null)
            {
                AutoCheckAccount account = Mapper.GetAccount(dealerId, member);

                if (account != null)
                {
                    if (account.Active)
                    {
                        if (account.AccountType == AutoCheckAccountType.Dealer)
                        {
                            if (account.Assignments.Contains(member.Id))
                            {
                                report = Mapper.GetTransferObject(account.PurchaseReport(parameters.Vin));
                            }
                        }
                        else
                        {
                            report = Mapper.GetTransferObject(account.PurchaseReport(parameters.Vin));
                        }
                    }
                }
            }

            return new PurchaseReportResultsDto
            {
                Arguments = parameters,
                Report = report
            };
        }
    }
}
