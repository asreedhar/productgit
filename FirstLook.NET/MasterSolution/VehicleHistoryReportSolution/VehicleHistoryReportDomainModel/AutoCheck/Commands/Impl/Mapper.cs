using System;
using System.Security.Principal;
using System.Threading;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.Impl
{
    public static class Mapper
    {
        public static Member GetMember(string userName)
        {
            if (Member.Exists(userName))
            {
                Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(userName, "SOAP"), new string[0]);

                return Member.GetMember(userName);
            }

            return null;
        }

        public static AutoCheckAccount GetAccount(int dealerId, Member member)
        {
            if (member.MemberType == MemberType.User)
            {
                if (DealerAccount.Exists(dealerId))
                {
                    DealerAccount dealerAccount = DealerAccount.GetDealerAccount(dealerId);

                    return dealerAccount.Account;
                }
            }
            else if (member.MemberType == MemberType.Administrator)
            {
                if (SystemAccount.Exists())
                {
                    SystemAccount systemAccount = SystemAccount.GetSystemAccount();

                    return systemAccount.Account;
                }
            }

            return null;
        }

        public static AutoCheckReportTO GetTransferObject(AutoCheckReport report)
        {
            AutoCheckReportTO info = new AutoCheckReportTO
            {
                UserName = report.Account.UserName,
                Vin = report.Vin,
                ExpirationDate = DateTime.MaxValue,
                OwnerCount = report.OwnerCount,
                Score = report.Score,
                CompareScoreRangeLow = report.CompareScoreRangeLow,
                CompareScoreRangeHigh = report.CompareScoreRangeHigh
            };

            AutoCheckReportInspectionCollection inspections = AutoCheckReportInspectionCollection.GetReportInspections();

            foreach (AutoCheckReportInspectionResult result in report.Inspections)
            {
                AutoCheckReportInspection inspection = inspections.Find(result.ReportInspectionId);

                AutoCheckReportInspectionTO item = new AutoCheckReportInspectionTO
                {
                    Id = result.ReportInspectionId,
                    Name = inspection == null ? "(Unknown)" : inspection.Name,
                    Selected = result.Selected
                };

                info.Inspections.Add(item);
            }

            return info;
        }
    }
}
