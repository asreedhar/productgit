﻿using System;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects.Envelopes;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.Impl
{
    [Serializable]
    public class AccountQueryCommand : ICommand<AccountQueryResultsDto, AccountQueryArgumentsDto>
    {
        public AccountQueryResultsDto Execute(AccountQueryArgumentsDto parameters)
        {
            if (parameters.Client.ClientType != ClientTypeDto.Dealer)
            {
                throw new NotSupportedException(string.Format("ClientType.{0}", parameters.Client.ClientType));
            }

            int dealerId = parameters.Client.Id;

            bool hasAccount = false;

            Member member = Mapper.GetMember(parameters.UserName);

            if (member != null)
            {
                AutoCheckAccount account = Mapper.GetAccount(dealerId, member);

                if (account != null)
                {
                    hasAccount = account.Active;
                }
            }

            return new AccountQueryResultsDto
            {
                Arguments = parameters,
                HasAccount = hasAccount
            };
        }
    }
}
