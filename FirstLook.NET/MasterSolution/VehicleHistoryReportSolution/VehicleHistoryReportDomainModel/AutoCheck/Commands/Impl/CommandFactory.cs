using System;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.Impl.V_2_0;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects.Envelopes.V_2_0;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.Impl
{
    [CLSCompliant(false)]
    public class CommandFactory : ICommandFactory
    {
        #region V_1_0
        
        public ICommand<AccountQueryResultsDto, AccountQueryArgumentsDto> CreateAccountQueryCommand()
        {
            return new AccountQueryCommand();
        }

        public ICommand<ReportQueryResultsDto, ReportQueryArgumentsDto> CreateReportQueryCommand()
        {
            return new ReportQueryCommand();
        }

        public ICommand<ReportHtmlResultsDto, ReportHtmlArgumentsDto> CreateReportHtmlCommand()
        {
            return new ReportHtmlCommand();
        }

        public ICommand<PurchaseAuthorityResultsDto, PurchaseAuthorityArgumentsDto> CreatePurchaseAuthorityCommand()
        {
            return new PurchaseAuthorityCommand();
        }

        public ICommand<PurchaseReportResultsDto, PurchaseReportArgumentsDto> CreatePurchaseReportCommand()
        {
            return new PurchaseReportCommand();
        }

        public ICommand<PurchaseReportsResultsDto, PurchaseReportsArgumentsDto> CreatePurchaseReportsCommand()
        {
            return new PurchaseReportsCommand();
        }

        #endregion

        #region V_2_0
        
        public ICommand<AutoCheckReportResultsDto, IdentityContextDto<AutoCheckReportArgumentsDto>> CreateQueryCommand()
        {
            return new AutoCheckQueryCommand();
        }

        public ICommand<AutoCheckReportResultsDto, IdentityContextDto<AutoCheckReportArgumentsDto>> CreatePurchaseCommand()
        {
            return new AutoCheckPurchaseCommand();
        }

        #endregion
    }
}
