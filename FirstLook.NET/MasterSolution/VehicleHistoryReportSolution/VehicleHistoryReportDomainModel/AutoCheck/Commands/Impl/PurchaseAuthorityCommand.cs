﻿using System;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects.Envelopes;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.Impl
{
    public class PurchaseAuthorityCommand : ICommand<PurchaseAuthorityResultsDto, PurchaseAuthorityArgumentsDto>
    {
        public PurchaseAuthorityResultsDto Execute(PurchaseAuthorityArgumentsDto parameters)
        {
            if (parameters.Client.ClientType != ClientTypeDto.Dealer)
            {
                throw new NotSupportedException(string.Format("ClientType.{0}", parameters.Client.ClientType));
            }

            int dealerId = parameters.Client.Id;

            bool canPurchaseReport = false;

            Member member = Mapper.GetMember(parameters.UserName);

            if (member != null)
            {
                AutoCheckAccount account = Mapper.GetAccount(dealerId, member);

                if (account != null)
                {
                    if (account.Active)
                    {
                        if (account.AccountType == AutoCheckAccountType.Dealer)
                        {
                            canPurchaseReport = account.Assignments.Contains(member.Id);
                        }
                        else
                        {
                            canPurchaseReport = (account.AccountType == AutoCheckAccountType.System);
                        }
                    }
                }
            }

            return new PurchaseAuthorityResultsDto
            {
                Arguments = parameters,
                Authority = canPurchaseReport
            };
        }
    }
}
