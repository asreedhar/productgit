using System;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects.Envelopes;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.Impl
{
    public class ReportQueryCommand : ICommand<ReportQueryResultsDto, ReportQueryArgumentsDto>
    {
        public ReportQueryResultsDto Execute(ReportQueryArgumentsDto parameters)
        {
            if (parameters.Client.ClientType != ClientTypeDto.Dealer)
            {
                throw new NotSupportedException(string.Format("ClientType.{0}", parameters.Client.ClientType));
            }

            int dealerId = parameters.Client.Id;

            string vin = parameters.Vin;

            AutoCheckReportTO report = null;

            Member member = Mapper.GetMember(parameters.UserName);

            if (member != null)
            {
                /* this will take a dealers report over one ordered by an administrator */

                bool isSalesTool = member.MemberType == MemberType.Administrator;

                if (isSalesTool)
                {
                    isSalesTool = !Dealer.Exists(dealerId);
                }

                if (!isSalesTool && DealerAccount.Exists(dealerId))
                {
                    DealerAccount dealerAccount = DealerAccount.GetDealerAccount(dealerId);

                    if (dealerAccount.Account.HasPurchasedReport(vin))
                    {
                        report = Mapper.GetTransferObject(dealerAccount.Account.GetReport(vin));
                    }
                }

                if (report == null)
                {
                    if (member.MemberType == MemberType.Administrator)
                    {
                        if (SystemAccount.Exists())
                        {
                            SystemAccount systemAccount = SystemAccount.GetSystemAccount();

                            if (systemAccount.Account.HasPurchasedReport(vin))
                            {
                                report = Mapper.GetTransferObject(systemAccount.Account.GetReport(vin));
                            }
                        }
                    }
                }
            }

            return new ReportQueryResultsDto
            {
                Arguments = parameters,
                Report = report
            };
        }
    }
}
