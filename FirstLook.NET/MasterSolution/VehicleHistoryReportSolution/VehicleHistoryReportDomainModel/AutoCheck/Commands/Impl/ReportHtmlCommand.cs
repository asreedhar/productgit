﻿using System;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects.Envelopes;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.Impl
{
    public class ReportHtmlCommand : ICommand<ReportHtmlResultsDto, ReportHtmlArgumentsDto>
    {
        public ReportHtmlResultsDto Execute(ReportHtmlArgumentsDto parameters)
        {
            if (parameters.Client.ClientType != ClientTypeDto.Dealer)
            {
                throw new NotSupportedException(string.Format("ClientType.{0}", parameters.Client.ClientType));
            }

            int dealerId = parameters.Client.Id;

            string vin = parameters.Vin;

            string html = string.Empty;

            Member member = Mapper.GetMember(parameters.UserName);

            if (member != null)
            {
                if (DealerAccount.Exists(dealerId))
                {
                    DealerAccount dealerAccount = DealerAccount.GetDealerAccount(dealerId);

                    AutoCheckAccount account = dealerAccount.Account;

                    if (account.HasPurchasedReport(vin))
                    {
                        AutoCheckReport report = account.GetReport(vin);

                        html = report.GetHtml();
                    }
                    else
                    {
                        if (member.MemberType == MemberType.User)
                        {
                            if (account.Active)
                            {
                                if (account.Assignments.Contains(member.Id))
                                {
                                    AutoCheckReport report = account.PurchaseReport(vin);

                                    html = report.GetHtml();
                                }
                            }
                        }
                    }
                }

                if (string.IsNullOrEmpty(html))
                {
                    if (member.MemberType == MemberType.Administrator)
                    {
                        if (SystemAccount.Exists())
                        {
                            SystemAccount systemAccount = SystemAccount.GetSystemAccount();

                            if (systemAccount.Account.HasPurchasedReport(vin))
                            {
                                AutoCheckReport report = systemAccount.Account.GetReport(vin);

                                html = report.GetHtml();
                            }
                            else
                            {
                                AutoCheckReport report = systemAccount.Account.PurchaseReport(vin);

                                html = report.GetHtml();
                            }
                        }
                    }
                }
            }

            return new ReportHtmlResultsDto
            {
                Arguments = parameters,
                Report = html
            };
        }
    }
}
