﻿using System;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects.Envelopes;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.Impl
{
    public class PurchaseReportsCommand : ICommand<PurchaseReportsResultsDto, PurchaseReportsArgumentsDto>
    {
        public PurchaseReportsResultsDto Execute(PurchaseReportsArgumentsDto parameters)
        {
            if (parameters.Client.ClientType != ClientTypeDto.Dealer)
            {
                throw new NotSupportedException(string.Format("ClientType.{0}", parameters.Client.ClientType));
            }

            int dealerId = parameters.Client.Id;

            Member member = Mapper.GetMember(parameters.UserName);

            if (member != null)
            {
                AutoCheckAccount account = Mapper.GetAccount(dealerId, member);

                if (account != null)
                {
                    if (!account.Active)
                    {
                        goto EndOfCommand;
                    }

                    if (account.AccountType == AutoCheckAccountType.Dealer)
                    {
                        if (!account.Assignments.Contains(member.Id))
                        {
                            goto EndOfCommand;
                        }
                    }

                    VehicleCollection vehicles = VehicleCollection.GetVehicles(dealerId, parameters.VehicleEntityType);

                    foreach (Vehicle vehicle in vehicles)
                    {
                        try
                        {
                            account.PurchaseReport(vehicle.Vin);
                        }
                        catch (AutoCheckException ce)
                        {
                            AutoCheckExceptionLogger.Record(ce, ce.ReportId);
                        }
                        catch (Exception ex)
                        {
                            AutoCheckExceptionLogger.Record(ex);
                        }
                    }
                }
            }

            EndOfCommand:

            return new PurchaseReportsResultsDto
            {
                Arguments = parameters
            };
        }
    }
}
