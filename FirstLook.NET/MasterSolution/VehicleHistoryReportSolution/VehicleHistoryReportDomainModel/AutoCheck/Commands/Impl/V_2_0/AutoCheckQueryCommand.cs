﻿using System;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects.Envelopes.V_2_0;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.Impl.V_2_0
{
    [CLSCompliant(false)]
    public class AutoCheckQueryCommand : ICommand<AutoCheckReportResultsDto, IdentityContextDto<AutoCheckReportArgumentsDto>>
    {
        internal const string AutoCheckUrl = "/support/Reports/AutoCheckReport.aspx?DealerId={0}&Vin={1}";

        public AutoCheckReportResultsDto Execute(IdentityContextDto<AutoCheckReportArgumentsDto> parameters)
        {
            IdentityDto identity = parameters.Identity;

            AutoCheckReportArgumentsDto arguments = parameters.Arguments;

            IBroker broker = Broker.Get(
                identity.Name,
                identity.AuthorityName,
                arguments.Broker);

            string vin = arguments.Vin;

            AutoCheckReportResultsStatusDto status = AutoCheckReportResultsStatusDto.Success;

            AutoCheckReportTO report = null;

            string url = string.Empty;

            try
            {
                Member member = Mapper.GetMember(identity.Name);

                if (member != null)
                {
                    if (broker.Integration.IsDealer)
                    {
                        int dealerId = broker.Client.Id;

                        if (DealerAccount.Exists(dealerId))
                        {
                            DealerAccount dealerAccount = DealerAccount.GetDealerAccount(dealerId);

                            if (dealerAccount.Account.HasPurchasedReport(vin))
                            {
                                report = Mapper.GetTransferObject(dealerAccount.Account.GetReport(vin));

                                url = string.Format(AutoCheckUrl, dealerId, arguments.Vin);
                            }
                            else
                            {
                                status = AutoCheckReportResultsStatusDto.ReportNotPurchased;
                            }
                        }
                        else
                        {
                            status = AutoCheckReportResultsStatusDto.AccountNotAvailable;
                        }
                    }

                    if (report == null)
                    {
                        if (member.MemberType == MemberType.Administrator)
                        {
                            if (SystemAccount.Exists())
                            {
                                SystemAccount systemAccount = SystemAccount.GetSystemAccount();

                                if (systemAccount.Account.HasPurchasedReport(vin))
                                {
                                    report = Mapper.GetTransferObject(systemAccount.Account.GetReport(vin));

                                    url = string.Format(AutoCheckUrl, broker.Client.Id, arguments.Vin);

                                    status = AutoCheckReportResultsStatusDto.Success;
                                }
                                else
                                {
                                    report = Mapper.GetTransferObject(
                                        systemAccount.Account.PurchaseReport(
                                            arguments.Vin));

                                    url = string.Format(AutoCheckUrl, broker.Client.Id, arguments.Vin);

                                    status = AutoCheckReportResultsStatusDto.Success;
                                }
                            }
                        }
                        else
                        {
                            if (broker.Integration.IsSeller)
                            {
                                status = AutoCheckReportResultsStatusDto.AccountNotAvailable;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                status = AutoCheckReportResultsStatusDto.ServiceUnavailable;
            }

            return new AutoCheckReportResultsDto
            {
                Arguments = arguments,
                Report = report,
                Status = status,
                Url = url
            };
        }
    }
}
