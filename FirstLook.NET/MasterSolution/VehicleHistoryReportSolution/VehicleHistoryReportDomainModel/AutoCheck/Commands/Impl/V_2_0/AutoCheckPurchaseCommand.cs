﻿using System;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects.Envelopes.V_2_0;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.Impl.V_2_0
{
    [Serializable]
    [CLSCompliant(false)]
    public class AutoCheckPurchaseCommand : ICommand<AutoCheckReportResultsDto, IdentityContextDto<AutoCheckReportArgumentsDto>>
    {
        public AutoCheckReportResultsDto Execute(IdentityContextDto<AutoCheckReportArgumentsDto> parameters)
        {
            AutoCheckReportArgumentsDto arguments = parameters.Arguments;

            IdentityDto identity = parameters.Identity;

            IBroker broker = Broker.Get(
                identity.Name,
                identity.AuthorityName,
                arguments.Broker);

            AutoCheckReportResultsStatusDto status = AutoCheckReportResultsStatusDto.Success;

            AutoCheckReportTO report = null;

            string url = string.Empty;

            try
            {
                Member member = Mapper.GetMember(identity.Name);

                if (member != null)
                {
                    AutoCheckAccount account = Mapper.GetAccount(broker.Client.Id, member);

                    if (account != null)
                    {
                        if (account.Active)
                        {
                            if (account.AccountType == AutoCheckAccountType.Dealer)
                            {
                                if (account.Assignments.Contains(member.Id))
                                {
                                    report = Mapper.GetTransferObject(
                                        account.PurchaseReport(
                                        arguments.Vin));

                                    url = string.Format(
                                        AutoCheckQueryCommand.AutoCheckUrl,
                                        broker.Client.Id,
                                        arguments.Vin);
                                }
                                else
                                {
                                    status = AutoCheckReportResultsStatusDto.NoPermission;
                                }
                            }
                            else
                            {
                                report = Mapper.GetTransferObject(
                                    account.PurchaseReport(
                                    arguments.Vin));

                                url = string.Format(
                                    AutoCheckQueryCommand.AutoCheckUrl,
                                    broker.Client.Id,
                                    arguments.Vin);
                            }
                        }
                        else
                        {
                            status = AutoCheckReportResultsStatusDto.AccountNotOperable;
                        }
                    }
                    else
                    {
                        status = AutoCheckReportResultsStatusDto.AccountNotAvailable;
                    }
                }
                else
                {
                    status = AutoCheckReportResultsStatusDto.NoPermission;
                }
            }
            catch (Exception)
            {
                status = AutoCheckReportResultsStatusDto.ServiceUnavailable;
            }

            return new AutoCheckReportResultsDto
            {
                Arguments = arguments,
                Report = report,
                Status = status,
                Url = url
            };
        }
    }
}
