﻿using FirstLook.Common.Core.Registry;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.Impl;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory, CommandFactory>(ImplementationScope.Shared);
        }
    }
}
