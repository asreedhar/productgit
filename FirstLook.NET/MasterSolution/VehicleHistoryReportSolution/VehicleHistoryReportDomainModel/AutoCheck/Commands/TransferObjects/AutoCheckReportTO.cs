using System;
using System.Collections.Generic;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects
{
	[Serializable]
	public class AutoCheckReportTO
	{
		private DateTime _expirationDate;
		private string _userName;
		private string _vin;
        private int _ownerCount;
		private int _score;
		private int _compareScoreRangeLow;
		private int _compareScoreRangeHigh;
		private readonly List<AutoCheckReportInspectionTO> _inspections = new List<AutoCheckReportInspectionTO>();

		public DateTime ExpirationDate
		{
			get { return _expirationDate; }
			set { _expirationDate = value; }
		}

		public string UserName
		{
			get { return _userName; }
			set { _userName = value; }
		}

		public string Vin
		{
			get { return _vin; }
			set { _vin = value; }
		}

	    public int OwnerCount
	    {
	        get { return _ownerCount; }
	        set { _ownerCount = value; }
	    }

	    public int Score
		{
			get { return _score; }
			set { _score = value; }
		}

		public int CompareScoreRangeLow
		{
			get { return _compareScoreRangeLow; }
			set { _compareScoreRangeLow = value; }
		}

		public int CompareScoreRangeHigh
		{
			get { return _compareScoreRangeHigh; }
			set { _compareScoreRangeHigh = value; }
		}

		public List<AutoCheckReportInspectionTO> Inspections
		{
			get { return _inspections; }
		}
	}
}
