﻿using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class PurchaseReportsArgumentsDto : ArgumentsDto
    {
        private ClientDto _client;
        private VehicleEntityType _vehicleEntityType;

        public ClientDto Client
        {
            get { return _client; }
            set { _client = value; }
        }

        public VehicleEntityType VehicleEntityType
        {
            get { return _vehicleEntityType; }
            set { _vehicleEntityType = value; }
        }
    }
}
