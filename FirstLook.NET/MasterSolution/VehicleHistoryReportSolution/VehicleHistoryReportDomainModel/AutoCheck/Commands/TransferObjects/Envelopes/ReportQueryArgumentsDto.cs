﻿using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ReportQueryArgumentsDto : ArgumentsDto
    {
        private ClientDto _client;
        private string _vin;

        public ClientDto Client
        {
            get { return _client; }
            set { _client = value; }
        }

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }
    }
}
