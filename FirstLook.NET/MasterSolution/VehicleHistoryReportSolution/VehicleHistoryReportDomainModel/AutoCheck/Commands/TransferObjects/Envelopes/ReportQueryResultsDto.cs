using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ReportQueryResultsDto
    {
        private ReportQueryArgumentsDto _arguments;
        private AutoCheckReportTO _report;

        public ReportQueryArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public AutoCheckReportTO Report
        {
            get { return _report; }
            set { _report = value; }
        }
    }
}
