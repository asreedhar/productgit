﻿using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ReportHtmlResultsDto
    {
        private ReportHtmlArgumentsDto _arguments;
        private string _report;

        public ReportHtmlArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public string Report
        {
            get { return _report; }
            set { _report = value; }
        }
    }
}
