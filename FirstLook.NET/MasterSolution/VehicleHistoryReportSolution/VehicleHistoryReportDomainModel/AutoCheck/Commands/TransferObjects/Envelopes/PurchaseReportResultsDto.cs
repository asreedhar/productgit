using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class PurchaseReportResultsDto
    {
        private PurchaseReportArgumentsDto _arguments;
        private AutoCheckReportTO _report;

        public PurchaseReportArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public AutoCheckReportTO Report
        {
            get { return _report; }
            set { _report = value; }
        }
    }
}
