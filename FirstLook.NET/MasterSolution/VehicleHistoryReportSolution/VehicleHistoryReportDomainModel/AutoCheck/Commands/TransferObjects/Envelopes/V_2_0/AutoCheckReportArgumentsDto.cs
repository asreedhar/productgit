﻿using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects.Envelopes.V_2_0
{
    [Serializable]
    public class AutoCheckReportArgumentsDto
    {
        private Guid _broker;
        private string _vin;

        public Guid Broker
        {
            get { return _broker; }
            set { _broker = value; }
        }

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }
    }
}
