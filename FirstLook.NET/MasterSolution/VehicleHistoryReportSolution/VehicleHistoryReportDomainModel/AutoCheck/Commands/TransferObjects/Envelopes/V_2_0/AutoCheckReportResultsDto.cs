using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects.Envelopes.V_2_0
{
    [Serializable]
    public class AutoCheckReportResultsDto
    {
        private AutoCheckReportArgumentsDto _arguments;
        private AutoCheckReportResultsStatusDto _status;
        private AutoCheckReportTO _report;
        private string _url;

        public AutoCheckReportArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public AutoCheckReportResultsStatusDto Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public AutoCheckReportTO Report
        {
            get { return _report; }
            set { _report = value; }
        }

        public string Url
        {
            get { return _url; }
            set { _url = value; }
        }
    }
}
