﻿using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class PurchaseAuthorityResultsDto
    {
        private PurchaseAuthorityArgumentsDto _arguments;
        private bool _authority;

        public PurchaseAuthorityArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public bool Authority
        {
            get { return _authority; }
            set { _authority = value; }
        }
    }
}
