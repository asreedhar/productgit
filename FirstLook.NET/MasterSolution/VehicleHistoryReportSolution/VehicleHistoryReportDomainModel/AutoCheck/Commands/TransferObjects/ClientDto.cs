﻿using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects
{
    [Serializable]
    public class ClientDto
    {
        private ClientTypeDto _clientType;
        private int _id;

        public ClientTypeDto ClientType
        {
            get { return _clientType; }
            set { _clientType = value; }
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
    }
}
