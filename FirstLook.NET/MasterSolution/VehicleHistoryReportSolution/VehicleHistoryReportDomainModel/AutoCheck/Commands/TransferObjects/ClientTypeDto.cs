﻿using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects
{
    [Serializable]
    public enum ClientTypeDto
    {
        Undefined,
        Dealer
    }
}
