using System;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects.Envelopes.V_2_0;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands
{
    [CLSCompliant(false)]
    public interface ICommandFactory
    {
        #region V_1_0
        
        ICommand<AccountQueryResultsDto, AccountQueryArgumentsDto> CreateAccountQueryCommand();

        ICommand<ReportQueryResultsDto, ReportQueryArgumentsDto> CreateReportQueryCommand();

        ICommand<ReportHtmlResultsDto, ReportHtmlArgumentsDto> CreateReportHtmlCommand();

        ICommand<PurchaseAuthorityResultsDto, PurchaseAuthorityArgumentsDto> CreatePurchaseAuthorityCommand();

        ICommand<PurchaseReportResultsDto, PurchaseReportArgumentsDto> CreatePurchaseReportCommand();

        ICommand<PurchaseReportsResultsDto, PurchaseReportsArgumentsDto> CreatePurchaseReportsCommand();

        #endregion

        #region V_2_0

        ICommand<AutoCheckReportResultsDto, IdentityContextDto<AutoCheckReportArgumentsDto>>
            CreateQueryCommand();

        ICommand<AutoCheckReportResultsDto, IdentityContextDto<AutoCheckReportArgumentsDto>>
            CreatePurchaseCommand();

        #endregion
    }
}
