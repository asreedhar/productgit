using System;
using System.Data;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck
{
	[Serializable]
	public class AutoCheckAccount : BusinessBase<AutoCheckAccount>
	{
		private int id;
		private bool active;
		private AutoCheckAccountType accountType;
		private AutoCheckAccountStatus accountStatus;
		private AutoCheckAccountAssignmentCollection assignments = AutoCheckAccountAssignmentCollection.NewAutoCheckAccountAssignments();
		private string userName;
		private string password;
		private byte[] version = new byte[8];

		public bool Active
		{
			get
			{
				CanReadProperty("Active", true);

				return active;
			}
			set
			{
				CanWriteProperty("Active", true);

				if (Active != value)
				{
					active = value;

					PropertyHasChanged("Active");
				}
			}
		}
		public int Id
		{
			get { return id; }
		}

		public AutoCheckAccountType AccountType
		{
			get { return accountType; }
		}

		public AutoCheckAccountStatus AccountStatus
		{
			get { return accountStatus; }
		}

		public AutoCheckAccountAssignmentCollection Assignments
		{
			get { return assignments; }
		}

		public string UserName
		{
			get
			{
				CanReadProperty("UserName", true);

				return userName;
			}
			set
			{
				CanWriteProperty("UserName", true);

				if (!Equals(UserName, value))
				{
					userName = value;

					PropertyHasChanged("UserName");
				}
			}
		}

		public string Password
		{
			get
			{
				CanReadProperty("Password", true);

				return password;
			}
			set
			{
				CanWriteProperty("Password", true);

				if (!Equals(Password, value))
				{
					password = value;

					PropertyHasChanged("Password");
				}
			}
		}

		protected override object GetIdValue()
		{
			return Id;
		}

		public void AccountStatusResolved()
		{
			throw new NotImplementedException();
		}

		public bool HasPurchasedReport(string vin)
		{
			HasPurchasedReportCommand command = new HasPurchasedReportCommand(Id, vin);
			DataPortal.Execute(command);
			return command.Exists;
		}

		public AutoCheckReport GetReport(string vin)
		{
			return AutoCheckReport.GetReport(Id, vin);
		}

		public AutoCheckReport PurchaseReport(string vin)
		{
			return AutoCheckReport.NewReport(
				Id,
				vin,
				AutoCheckReportType.Full);
		}

		#region Validation

		public override bool IsDirty
		{
			get
			{
				return base.IsDirty || assignments.IsDirty;
			}
		}

		public override bool IsValid
		{
			get
			{
				return base.IsValid && assignments.IsValid;
			}
		}

		protected override void AddBusinessRules()
		{
			ValidationRules.AddRule(CommonRules.StringRequired, "UserName");

			ValidationRules.AddRule(CommonRules.StringMaxLength,
				new CommonRules.MaxLengthRuleArgs("UserName", 20));

			ValidationRules.AddRule(CommonRules.StringMaxLength,
				new CommonRules.MaxLengthRuleArgs("Password", 20));
		}

		protected override void AddInstanceBusinessRules()
		{
			ValidationRules.AddInstanceRule(ValidatePassword, "Password");
		}

		private bool ValidatePassword(object target, RuleArgs e)
		{
			if (AccountType == AutoCheckAccountType.Service)
			{
				if (string.IsNullOrEmpty(Password))
				{
					e.Description = "Password required";

					return false;
				}
			}
			else
			{
				if (!string.IsNullOrEmpty(Password))
				{
					e.Description = "Password must be null";

					return false;
				}
			}

			return true;
		}

		#endregion

		internal static AutoCheckAccount GetAccount(IDataReader reader)
		{
			return new AutoCheckAccount(reader);
		}

		internal static AutoCheckAccount GetAccount(int id)
		{
			return DataPortal.Fetch<AutoCheckAccount>(new Criteria(id));
		}

		internal static AutoCheckAccount NewAccount(AutoCheckAccountType accountType)
		{
			return new AutoCheckAccount(accountType);
		}

		private AutoCheckAccount()
		{
			MarkAsChild();
		}

		private AutoCheckAccount(IDataReader reader) : this()
		{
			Fetch(reader);
		}

		private AutoCheckAccount(AutoCheckAccountType accountType) : this()
		{
			active = true;

			accountStatus = AutoCheckAccountStatus.New;

			this.accountType = accountType;
		}



		[Serializable]
		protected class HasPurchasedReportCommand : CommandBase
		{
			private readonly int id;
			private readonly string vin;
			private bool exists;

			public HasPurchasedReportCommand(int id, string vin)
			{
				this.id = id;
				this.vin = vin;
			}

			public bool Exists
			{
				get { return exists; }
			}

			protected override void DataPortal_Execute()
			{
				using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.AutoCheckDatabase))
				{
					connection.Open();

					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = "AutoCheck.Account#HasPurchasedReport";
						command.AddParameterWithValue("Id", DbType.Int32, true, id);
						command.AddParameterWithValue("Vin", DbType.String, true, vin);

						using (IDataReader reader = command.ExecuteReader())
						{
							if (reader.Read())
							{
								exists = reader.GetBoolean(reader.GetOrdinal("Exists"));
							}
							else
							{
								exists = false;
							}
						}
					}
				}
			}
		}

		[Serializable]
		protected class Criteria
		{
			private readonly int id;

			public Criteria(int id)
			{
				this.id = id;
			}

			public int Id
			{
				get { return id; }
			}
		}

		protected void DataPortal_Fetch(Criteria criteria)
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.AutoCheckDatabase))
			{
				connection.Open();

				using (IDataCommand command = new DataCommand(connection.CreateCommand()))
				{
					command.CommandType = CommandType.StoredProcedure;
					command.CommandText = "AutoCheck.Account#Fetch";
					command.AddParameterWithValue("Id", DbType.Int32, true, criteria.Id);

					using (IDataReader reader = command.ExecuteReader())
					{
						if (reader.Read())
						{
							Fetch(reader);
						}
						else
						{
							throw new DataException("Missing Account data!");
						}
					}
				}
			}
		}


		private void Fetch(IDataReader reader)
		{
			id = reader.GetInt32(reader.GetOrdinal("Id"));

			active = reader.GetBoolean(reader.GetOrdinal("Active"));

			accountType = (AutoCheckAccountType)Enum.ToObject(typeof(AutoCheckAccountType), reader.GetInt32(reader.GetOrdinal("AccountType")));

			accountStatus = (AutoCheckAccountStatus) Enum.ToObject(typeof(AutoCheckAccountStatus), reader.GetInt32(reader.GetOrdinal("AccountStatus")));

			userName = reader.GetString(reader.GetOrdinal("UserName"));

            if (!reader.IsDBNull(reader.GetOrdinal("Password")))
            {
                password = reader.GetString(reader.GetOrdinal("Password"));
            }

			reader.GetBytes(reader.GetOrdinal("Version"), 0, version, 0, 8);

			if (reader.NextResult())
			{
				assignments = AutoCheckAccountAssignmentCollection.GetAutoCheckAccountAssignments(reader);
			}

			MarkOld();
		}

		public void DoInsert(IDataConnection connection, IDbTransaction transaction)
		{
			using (IDataCommand command = new DataCommand(connection.CreateCommand()))
			{
				command.CommandType = CommandType.StoredProcedure;
				command.CommandText = "AutoCheck.Account#Insert";
				command.Transaction = transaction;

				command.AddParameterWithValue("AccountTypeId", DbType.Int32, false, (int) AccountType);
				command.AddParameterWithValue("Active", DbType.Boolean, false, Active);
				command.AddParameterWithValue("UserName", DbType.String, false, UserName);
                command.AddParameterWithValue("Password", DbType.String, true, String.IsNullOrEmpty(Password) ? null : Password);
				command.AddParameterWithValue("InsertUser", DbType.String, false, System.Threading.Thread.CurrentPrincipal.Identity.Name);

				IDataParameter newId = command.AddOutParameter("Id", DbType.Int32);

				IDataParameter newVersion = Database.AddArrayOutParameter(command, "NewVersion", DbType.Binary, false, 8);

				command.ExecuteNonQuery();

				id = (int) newId.Value;

				version = (byte[]) newVersion.Value;
			}

			assignments.DoUpdate(connection, transaction, this);

			MarkNew();
		}

		internal void DoUpdate(IDbConnection connection, IDbTransaction transaction)
		{
			using (IDataCommand command = new DataCommand(connection.CreateCommand()))
			{
				command.CommandType = CommandType.StoredProcedure;
				command.CommandText = "AutoCheck.Account#Update";
				command.Transaction = transaction;

				command.AddParameterWithValue("Id", DbType.Int32, true, Id);
				command.AddParameterWithValue("Active", DbType.Boolean, false, Active);
				command.AddParameterWithValue("UserName", DbType.String, false, UserName);
                command.AddParameterWithValue("Password", DbType.String, true, String.IsNullOrEmpty(Password) ? null : Password);
				command.AddParameterWithValue("UpdateUser", DbType.String, false, System.Threading.Thread.CurrentPrincipal.Identity.Name);

				Database.AddArrayParameter(command, "Version", DbType.Binary, false, version);

				IDataParameter newVersion = Database.AddArrayOutParameter(command, "NewVersion", DbType.Binary, false, 8);

				command.ExecuteNonQuery();

				version = (byte[]) newVersion.Value;
			}

			assignments.DoUpdate(connection, transaction, this);

			MarkOld();
		}
	}
}
