namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck
{
	public enum AutoCheckServiceType
	{
		Unknown = 0,
		Xml     = 1,
		Html    = 2
	}
}
