using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck
{
	[Serializable]
	public class ServiceAccount : BusinessBase<ServiceAccount>
	{
		#region Business Methods
		
		private AutoCheckAccount account;

		private AutoCheckServiceType serviceType;

		public AutoCheckAccount Account
		{
			get { return account; }
		}

		public AutoCheckServiceType ServiceType
		{
			get { return serviceType; }
		}

		protected override object GetIdValue()
		{
			return serviceType;
		}

		#endregion

		#region Validation

		public override bool IsDirty
		{
			get
			{
				return base.IsDirty || Account.IsDirty;
			}
		}

		public override bool IsValid
		{
			get
			{
				return base.IsValid && Account.IsValid;
			}
		}

		#endregion

		#region Factory Methods

		public static bool Exists(AutoCheckServiceType serviceType)
		{
			ExistsCommand command = new ExistsCommand(serviceType);
			DataPortal.Execute(command);
			return command.Exists;
		}

		public static ServiceAccount GetServiceAccount(AutoCheckServiceType serviceType)
		{
			return DataPortal.Fetch<ServiceAccount>(new Criteria(serviceType));
		}

		public static ServiceAccount NewServiceAccount(AutoCheckServiceType serviceType)
		{
			return new ServiceAccount(serviceType);
		}

		private ServiceAccount()
		{
			
		}

		private ServiceAccount(AutoCheckServiceType serviceType)
		{
			this.serviceType = serviceType;

			account = AutoCheckAccount.NewAccount(AutoCheckAccountType.Service);
		}

		#endregion

		#region Data Access

		public class DataSource
		{
			public AutoCheckAccount Select(int serviceTypeId)
			{
				AutoCheckServiceType serviceType = (AutoCheckServiceType) Enum.ToObject(typeof (AutoCheckServiceType), serviceTypeId);

				if (Exists(serviceType))
				{
					return GetServiceAccount(serviceType).Account;
				}

				return null;
			}

			public void Insert(int serviceTypeId, string userName, string password)
			{
				AutoCheckServiceType serviceType = (AutoCheckServiceType)Enum.ToObject(typeof(AutoCheckServiceType), serviceTypeId);

				if (Exists(serviceType))
				{
					throw new InvalidOperationException("There can be only one Service account per type!");
				}

				ServiceAccount serviceAccount = NewServiceAccount(serviceType);
				serviceAccount.Account.Active = true;
                serviceAccount.Account.UserName = userName;
                serviceAccount.Account.Password = password;
                serviceAccount.Save();
			}

			public void Update(int serviceTypeId, int id, string userName, string password)
			{
				AutoCheckServiceType serviceType = (AutoCheckServiceType)Enum.ToObject(typeof(AutoCheckServiceType), serviceTypeId);

				if (Exists(serviceType))
				{
					ServiceAccount serviceAccount = GetServiceAccount(serviceType);
					serviceAccount.Account.Active = true;
					serviceAccount.Account.UserName = userName;
					serviceAccount.Account.Password = password;
					serviceAccount.Save();
				}
				else
				{
					throw new InvalidOperationException("There is no Service account to update!");
				}
			}

			public void Delete(int serviceTypeId)
			{
				
			}
		}

		[Serializable]
		protected class Criteria
		{
			private readonly AutoCheckServiceType serviceType;

			public Criteria(AutoCheckServiceType serviceType)
			{
				this.serviceType = serviceType;
			}

			public AutoCheckServiceType ServiceType
			{
				get { return serviceType; }
			}
		}

		[Serializable]
		private class ExistsCommand : CommandBase
		{
			private readonly AutoCheckServiceType serviceType;

			private bool exists;

			public ExistsCommand(AutoCheckServiceType serviceType)
			{
				this.serviceType = serviceType;
			}

			public bool Exists
			{
				get { return exists; }
			}

			[Transactional(TransactionalTypes.Manual)]
			protected override void DataPortal_Execute()
			{
				using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.AutoCheckDatabase))
				{
					connection.Open();

					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = "AutoCheck.ServiceAccount#Exists";

						command.AddParameterWithValue("ServiceTypeId", DbType.Int32, false, (int) serviceType);

						using (IDataReader reader = command.ExecuteReader())
						{
							if (reader.Read())
							{
								exists = reader.GetBoolean(reader.GetOrdinal("Exists"));
							}
							else
							{
								exists = false;
							}
						}
					}
				}
			}
		}

		[Transactional(TransactionalTypes.Manual)]
		protected void DataPortal_Fetch(Criteria criteria)
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.AutoCheckDatabase))
			{
				connection.Open();

				using (IDataCommand command = new DataCommand(connection.CreateCommand()))
				{
					command.CommandType = CommandType.StoredProcedure;
					command.CommandText = "AutoCheck.ServiceAccount#Fetch";

					command.AddParameterWithValue("ServiceTypeId", DbType.Int32, false, (int) criteria.ServiceType);

					using (IDataReader reader = command.ExecuteReader())
					{
						if (reader.Read())
						{
							serviceType = (AutoCheckServiceType) Enum.ToObject(typeof(AutoCheckServiceType), reader.GetInt32(reader.GetOrdinal("ServiceTypeId")));

							account = AutoCheckAccount.GetAccount(reader);
						}
						else
						{
							throw new DataException("Missing System information");
						}
					}
				}
			}

			MarkOld();
		}

		[Transactional(TransactionalTypes.Manual)]
		protected override void DataPortal_Insert()
		{
			if (!account.IsNew) return;

			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.AutoCheckDatabase))
			{
				connection.Open();

				using (IDbTransaction transaction = connection.BeginTransaction())
				{
					account.DoInsert(connection, transaction);

					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = "AutoCheck.ServiceAccount#Insert";
						command.Transaction = transaction;

						command.AddParameterWithValue("ServiceTypeId", DbType.Int32, true, (int) ServiceType);
						command.AddParameterWithValue("AccountId", DbType.Int32, true, account.Id);
						command.AddParameterWithValue("InsertUser", DbType.String, true, System.Threading.Thread.CurrentPrincipal.Identity.Name);

						command.ExecuteNonQuery();
					}

					transaction.Commit();
				}
			}

			MarkOld();
		}

		[Transactional(TransactionalTypes.Manual)]
		protected override void DataPortal_Update()
		{
			if (!account.IsDirty) return;

			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.AutoCheckDatabase))
			{
				connection.Open();

				using (IDbTransaction transaction = connection.BeginTransaction())
				{
					account.DoUpdate(connection, transaction);

					transaction.Commit();
				}
			}

			MarkOld();
		}

		#endregion
	}
}
