using System;
using System.ComponentModel;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.VehicleHistoryReport.DomainModel.AutoCheck
{
	[Serializable]
	public class DealerAccount : BusinessBase<DealerAccount>
	{
		#region Business Methods

		private int dealerId;
		private AutoCheckAccount account;

		public int DealerId
		{
			get { return dealerId; }
		}

		public AutoCheckAccount Account
		{
			get { return account; }
		}

		protected override object GetIdValue()
		{
			return DealerId;
		}

		#endregion

		#region Validation

		public override bool IsDirty
		{
			get
			{
				return base.IsDirty || Account.IsDirty;
			}
		}

		public override bool IsValid
		{
			get
			{
				return base.IsValid && Account.IsValid;
			}
		}

		#endregion

		#region Factory Methods

		public static bool Exists(int dealerId)
		{
			ExistsCommand command = new ExistsCommand(dealerId);
			DataPortal.Execute(command);
			return command.Exists;
		}

		public static DealerAccount GetDealerAccount(int dealerId)
		{
			return DataPortal.Fetch<DealerAccount>(new Criteria(dealerId));
		}

		public static DealerAccount NewDealerAccount(int dealerId)
		{
			return new DealerAccount(dealerId);
		}

		private DealerAccount()
		{
			/* force use of factory methods */
		}

		private DealerAccount(int dealerId)
		{
			this.dealerId = dealerId;

			account = AutoCheckAccount.NewAccount(AutoCheckAccountType.Dealer);
		}

		#endregion

		#region Data Access

		[DataObject]
		public class DataSource
		{
			[DataObjectMethod(DataObjectMethodType.Select)]
			public AutoCheckAccount Select(int dealerId)
			{
				if (Exists(dealerId))
				{
					return GetDealerAccount(dealerId).Account;
				}

				return null;
			}

			[DataObjectMethod(DataObjectMethodType.Insert)]
			public void Insert(int dealerId, string userName, bool active)
			{
				DealerAccount dealerAccount = NewDealerAccount(dealerId);
				dealerAccount.Account.Active = active;
				dealerAccount.Account.UserName = userName;
				dealerAccount.Save();
			}

			[DataObjectMethod(DataObjectMethodType.Update)]
			public void Update(int dealerId, int id, string userName, bool active)
			{
				DealerAccount dealerAccount = GetDealerAccount(dealerId);
				dealerAccount.Account.Active = active;
				dealerAccount.Account.UserName = userName;
				dealerAccount.Save();
			}

			[DataObjectMethod(DataObjectMethodType.Delete)]
			public void Delete(int dealerId)
			{
				
			}
		}

		[Serializable]
		protected class Criteria
		{
			private readonly int dealerId;

			public Criteria(int dealerId)
			{
				this.dealerId = dealerId;
			}

			public int DealerId
			{
				get { return dealerId; }
			}
		}

		[Serializable]
		private class ExistsCommand : CommandBase
		{
			private readonly int dealerId;
			private bool exists;

			public ExistsCommand(int dealerId)
			{
				this.dealerId = dealerId;
			}

			public bool Exists
			{
				get { return exists; }
			}

			protected override void DataPortal_Execute()
			{
				using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.AutoCheckDatabase))
				{
					connection.Open();

					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = "AutoCheck.DealerAccount#Exists";
						command.AddParameterWithValue("DealerId", DbType.Int32, true, dealerId);

						using (IDataReader reader = command.ExecuteReader())
						{
							if (reader.Read())
							{
								exists = reader.GetBoolean(reader.GetOrdinal("Exists"));
							}
							else
							{
								exists = false;
							}
						}
					}
				}
			}
		}

		protected void DataPortal_Fetch(Criteria criteria)
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.AutoCheckDatabase))
			{
				connection.Open();

				using (IDataCommand command = new DataCommand(connection.CreateCommand()))
				{
					command.CommandType = CommandType.StoredProcedure;
					command.CommandText = "AutoCheck.DealerAccount#Fetch";
					command.AddParameterWithValue("DealerId", DbType.Int32, true, criteria.DealerId);

					using (IDataReader reader = command.ExecuteReader())
					{
						if (reader.Read())
						{
							dealerId = criteria.DealerId;

							account = AutoCheckAccount.GetAccount(reader);
						}
						else
						{
							throw new DataException("Missing Dealer information");
						}
					}
				}
			}

			MarkOld();
		}

		protected override void DataPortal_Insert()
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.AutoCheckDatabase))
			{
				connection.Open();

				using (IDbTransaction transaction = connection.BeginTransaction())
				{
					account.DoInsert(connection, transaction);

					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = "AutoCheck.DealerAccount#Insert";
						command.Transaction = transaction;

						command.AddParameterWithValue("DealerId", DbType.Int32, true, DealerId);
						command.AddParameterWithValue("AccountId", DbType.Int32, true, account.Id);
						command.AddParameterWithValue("InsertUser", DbType.String, true, System.Threading.Thread.CurrentPrincipal.Identity.Name);

						command.ExecuteNonQuery();
					}

					transaction.Commit();
				}
			}

			MarkOld();
		}

		protected override void DataPortal_Update()
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.AutoCheckDatabase))
			{
				connection.Open();

				using (IDbTransaction transaction = connection.BeginTransaction())
				{
					account.DoUpdate(connection, transaction);

					transaction.Commit();
				}
			}

			MarkOld();
		}

		#endregion
	}
}