using System;
using System.ComponentModel;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.VehicleHistoryReport.DomainModel
{
	[Serializable]
	public class Member : ReadOnlyBase<Member>
	{
		private int id;
		private string firstName;
		private string lastName;
		private string userName;
		private MemberType memberType;

		public int Id
		{
			get { return id; }
		}

		public string FirstName
		{
			get { return firstName; }
		}

		public string LastName
		{
			get { return lastName; }
		}

		public string UserName
		{
			get { return userName; }
		}

		public MemberType MemberType
		{
			get { return memberType; }
		}

		public string DisplayName
		{
			get { return LastName + ", " + FirstName; }
		}

		protected override object GetIdValue()
		{
			return Id;
		}

		internal static Member GetMember(IDataRecord record)
		{
			return new Member(record);
		}

		private Member()
		{
			/* force use of factory methods */
		}

		private Member(IDataRecord record)
		{
			Fetch(record);
		}

		public static bool Exists(string userName)
		{
			ExistsCommand command = new ExistsCommand(userName);
			DataPortal.Execute(command);
			return command.Exists;
		}

		public static Member GetMember(string userName)
		{
			return DataPortal.Fetch<Member>(new Criteria(userName));
		}
		
		[DataObject]
		public class DataSource
		{
			[DataObjectMethod(DataObjectMethodType.Select)]
			public MemberCollection Select(int dealerId)
			{
				return MemberCollection.GetMembers(dealerId);
			}
		}

		[Serializable]
		protected class Criteria
		{
			private readonly string userName;

			public Criteria(string userName)
			{
				this.userName = userName;
			}

			public string UserName
			{
				get { return userName; }
			}
		}
		
		[Serializable]
		private class ExistsCommand : CommandBase
		{
			private readonly string userName;
			private bool exists;

			public ExistsCommand(string userName)
			{
				this.userName = userName;
			}

			public bool Exists
			{
				get { return exists; }
			}

			protected override void DataPortal_Execute()
			{
				using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
				{
					connection.Open();

					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = "dbo.Member#Exists";
						command.AddParameterWithValue("UserName", DbType.String, true, userName);

						using (IDataReader reader = command.ExecuteReader())
						{
							if (reader.Read())
							{
								exists = reader.GetBoolean(reader.GetOrdinal("Exists"));
							}
							else
							{
								exists = false;
							}
						}
					}
				}
			}
		}

		protected void DataPortal_Fetch(Criteria criteria)
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CarfaxDatabase))
			{
				connection.Open();

				using (IDataCommand command = new DataCommand(connection.CreateCommand()))
				{
					command.CommandType = CommandType.StoredProcedure;
					command.CommandText = "dbo.Member#Fetch";
					command.AddParameterWithValue("UserName", DbType.String, true, criteria.UserName);

					using (IDataReader reader = command.ExecuteReader())
					{
						if (reader.Read())
						{
							Fetch(reader);
						}
						else
						{
							throw new DataException("Missing Dealer information");
						}
					}
				}
			}
		}

		private void Fetch(IDataRecord record)
		{
			id = record.GetInt32(record.GetOrdinal("Id"));
			firstName = record.GetString(record.GetOrdinal("FirstName"));
			lastName = record.GetString(record.GetOrdinal("LastName"));
			userName = record.GetString(record.GetOrdinal("UserName"));
			memberType = (MemberType) Enum.ToObject(typeof (MemberType), record.GetInt32(record.GetOrdinal("MemberType")));
		}
	}
}