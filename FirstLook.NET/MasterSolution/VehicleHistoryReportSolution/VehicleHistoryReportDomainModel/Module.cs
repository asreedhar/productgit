﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleHistoryReport.DomainModel
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<AutoCheck.Commands.Module>();

            registry.Register<Carfax.Commands.Module>();
            registry.Register<AutoCheck.Commands.Module>();
        }
    }
}
