using System;
using System.Data;
using Csla;

namespace FirstLook.VehicleHistoryReport.DomainModel
{
	[Serializable]
	public class Vehicle : ReadOnlyBase<Vehicle>
	{
		#region Business Method
		
		private string vin;

		public string Vin
		{
			get { return vin; }
		}

		protected override object GetIdValue()
		{
			return Vin;
		}

		#endregion

		#region Factory Methods

		private Vehicle()
		{
			/* force use of factory methods */
		}

		private Vehicle(IDataRecord record)
		{
			Fetch(record);
		}

		internal static Vehicle GetVehicle(IDataRecord record)
		{
			return new Vehicle(record);
		}

		#endregion

		#region Data Access
		
		private void Fetch(IDataRecord record)
		{
			vin = record.GetString(record.GetOrdinal("Vin"));
		}

		#endregion
	}
}
