using System;
using System.Data;
using System.Data.SqlClient;
using FirstLook.Common.Data;

namespace FirstLook.VehicleHistoryReport.DomainModel
{
	static class Database
	{
		private const string carfaxDatabase = "IMT";
		private const string autoCheckDatabase = "IMT";

		public static string CarfaxDatabase
		{
			get { return carfaxDatabase; }
		}

		public static string AutoCheckDatabase
		{
			get { return autoCheckDatabase; }
		}

		public static void AddArrayParameter(IDataCommand command, string name, DbType type, bool nullable, Array value)
		{
			IDbDataParameter paramVersion = command.CreateParameter();
			paramVersion.DbType = type;
			paramVersion.ParameterName = name;
			paramVersion.Size = value.Length;
			paramVersion.Value = value;

			SqlParameter paramVersionSql = paramVersion as SqlParameter;

			if (paramVersionSql != null)
			{
				paramVersionSql.IsNullable = nullable;
			}

			command.Parameters.Add(paramVersion);
		}

		public static IDataParameter AddArrayOutParameter(IDataCommand command, string name, DbType type, bool nullable, int size)
		{
			IDbDataParameter parameter = command.CreateParameter();
			parameter.DbType = type;
			parameter.Direction = ParameterDirection.Output;
			parameter.ParameterName = name;
			parameter.Size = size;
            
			SqlParameter sqlParameter = parameter as SqlParameter;

			if (sqlParameter != null)
			{
				sqlParameter.IsNullable = nullable;
			}

			command.Parameters.Add(parameter);

			return parameter;
		}
	}
}