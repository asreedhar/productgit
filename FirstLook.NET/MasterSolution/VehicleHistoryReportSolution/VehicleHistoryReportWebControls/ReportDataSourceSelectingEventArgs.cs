using System.Collections.Specialized;
using System.Web.UI;

namespace FirstLook.VehicleHistoryReport.WebControls
{
	public class ReportDataSourceSelectingEventArgs : ReportDataSourceMethodEventArgs
	{
		private readonly DataSourceSelectArguments _arguments;
		private readonly bool _executingSelectCount;

		public ReportDataSourceSelectingEventArgs(IOrderedDictionary inputParameters, DataSourceSelectArguments arguments, bool executingSelectCount)
			: base(inputParameters)
		{
			_arguments = arguments;
			_executingSelectCount = executingSelectCount;
		}

		public DataSourceSelectArguments Arguments
		{
			get
			{
				return _arguments;
			}
		}

		public bool ExecutingSelectCount
		{
			get
			{
				return _executingSelectCount;
			}
		}
	}
}