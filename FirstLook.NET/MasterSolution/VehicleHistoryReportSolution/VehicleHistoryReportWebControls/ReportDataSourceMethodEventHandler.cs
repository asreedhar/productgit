namespace FirstLook.VehicleHistoryReport.WebControls
{
	public delegate void ReportDataSourceMethodEventHandler(
		object sender,
		ReportDataSourceMethodEventArgs e);
}
