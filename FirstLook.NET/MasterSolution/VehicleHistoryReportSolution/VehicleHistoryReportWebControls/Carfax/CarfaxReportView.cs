using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Utilities;

namespace FirstLook.VehicleHistoryReport.WebControls.Carfax
{
	[ParseChildren(true), PersistChildren(false)]
	public class CarfaxReportView : DataBoundControl, INamingContainer, IPostBackEventHandler
	{
		#region Events

		private static readonly object EventClick = new object();

		public event EventHandler<EventArgs> Click
		{
			add
			{
				Events.AddHandler(EventClick, value);
			}
			remove
			{
				Events.RemoveHandler(EventClick, value);
			}
		}

		protected virtual void OnClick(EventArgs e)
		{
			EventHandler<EventArgs> handler = (EventHandler<EventArgs>)Events[EventClick];

			if (handler != null)
			{
				handler(this, e);
			}
		}

		#endregion

		#region Properties

		private string dataExpirationDateField = "ExpirationDate";
		private string dataItemsField = "Inspections";
		private string dataItemTextField = "Name";
		private string dataItemValueField = "Id";
		private string dataItemSelectedField = "Selected";
		private string dataUserNameField = "UserName";
		private string dataVinField = "Vin";
        private string dataHasProblemField = "HasProblem";

	    private bool hasProblem = true;
        private DateTime expirationDate;
		private ListItemCollection items;
		private string userName;
		private string vin;

		[Themeable(false), Description("Expiration Date Property Name"), DefaultValue(""), Category("Data")]
		public string DataExpirationDateField
		{
			get { return dataExpirationDateField; }
			set { dataExpirationDateField = value; }
		}

		[Themeable(false), Description("Report Inspection Items Property Name"), DefaultValue(""), Category("Data")]
		public string DataItemsField
		{
			get { return dataItemsField; }
			set { dataItemsField = value; }
		}

		[Themeable(false), Description("Inspection Text Property Name"), DefaultValue(""), Category("Data")]
		public string DataItemTextField
		{
			get { return dataItemTextField; }
			set { dataItemTextField = value; }
		}

		[Themeable(false), Description("Inspection Value Property Name"), DefaultValue(""), Category("Data")]
		public string DataItemValueField
		{
			get { return dataItemValueField; }
			set { dataItemValueField = value; }
		}

		[Themeable(false), Description("Inspection Selected Property Name"), DefaultValue(""), Category("Data")]
		public string DataItemSelectedField
		{
			get { return dataItemSelectedField; }
			set { dataItemSelectedField = value; }
		}

		[Themeable(false), Description("Report UserName Property Name"), DefaultValue(""), Category("Data")]
		public string DataUserNameField
		{
			get { return dataUserNameField; }
			set { dataUserNameField = value; }
		}

		[Themeable(false), Description("Report VIN Property Name"), DefaultValue(""), Category("Data")]
		public string DataVinField
		{
			get { return dataVinField; }
			set { dataVinField = value; }
		}

		[Themeable(false), Description("Report Expiration Date"), DefaultValue(""), Category("Data")]
		public DateTime ExpirationDate
		{
			get { return expirationDate; }
		}

        public string DataHasProblemField
        {
            get { return dataHasProblemField; }
            set { dataHasProblemField = value; }
        }

		[Themeable(false), Description("Report Inspection Items"), DefaultValue(""), Category("Data")]
		[MergableProperty(false), PersistenceMode(PersistenceMode.InnerProperty)]
		public ListItemCollection Items
		{
			get
			{
				if (items == null)
				{
					items = new ListItemCollection();

					if (IsTrackingViewState)
					{
						((IStateManager)items).TrackViewState();
					}
				}

				return items;
			}
		}

		[Themeable(false), Description("Account UserName"), DefaultValue(""), Category("Data")]
		public string UserName
		{
			get { return userName; }
		}

		[Themeable(false), Description("Report VIN"), DefaultValue(""), Category("Data")]
		public string Vin
		{
			get { return vin; }
		}

        [Themeable(false), Description("Autogenerate Inspection Item Badges"), DefaultValue(false), Category("Data")]
        public bool AutoGenerateInspectionItems
        {
            get
            {
                object value = ViewState["AutoGenerateInspectionItems"];
                if (value == null)
                    return false;
                return (bool)value;
            }
            set
            {
                if (AutoGenerateInspectionItems != value)
                {
                    ViewState["AutoGenerateInspectionItems"] = value;
                }
            }
        }

        [Themeable(false), Description("Display One Owner Icon"), DefaultValue(true), Category("Data")]
        public bool ShowOneOwner
        {
            get
            {
                object value = ViewState["ShowOneOwner"];
                if (value == null)
                    return true;
                return (bool)value;
            }
            set
            {
                if (ShowOneOwner != value)
                {
                    ViewState["ShowOneOwner"] = value; 
                }
            }
        }

        [Themeable(false), Description("Inspection Has One Owner"), DefaultValue(false), Category("Data")]
	    public bool HasOneOwner
	    {
	        get
	        {
                bool returnValue = false;

                foreach (ListItem item in Items)
                {
                    int value = Int32Helper.ToInt32(item.Value);
                    if (value == 1 && item.Enabled)
                    {
                        returnValue = item.Enabled;
                        break;
                    }
                }

                return returnValue;
	        }
	    }

        [Themeable(false), Description("Display One Owner Icons"), DefaultValue(true), Category("Data")]
        public bool ShowBuyBackGuarantee
        {
            get
            {
                object value = ViewState["ShowBuyBackGuarantee"];
                if (value == null)
                    return true;
                return (bool)value;
            }
            set
            {
                if (ShowBuyBackGuarantee != value)
                {
                    ViewState["ShowBuyBackGuarantee"] = value;
                }
            }
        }

        [Themeable(false), Description("Inspection Shows Buy Back Guarantee"), DefaultValue(true), Category("Data")]
        public bool HasBuyBackGuarantee
        {
            get
            {
                bool returnValue = false;


                foreach (ListItem item in Items)
                {
                    int value = Int32Helper.ToInt32(item.Value);
                    if (value == 2 && item.Enabled)
                    {
                        returnValue = item.Enabled;
                        break;
                    }

                }

                return returnValue;
            }
        }

        [Themeable(false), Description("Display One Owner Icons"), DefaultValue(false), Category("Data")]
        public bool CombineUnbrandedInspectionItems
        {
            get
            {
                object value = ViewState["CombineUnbrandedInspectionItems"];
                if (value == null)
                    return false;
                return (bool)value;
            }
            set
            {
                if (CombineUnbrandedInspectionItems != value)
                {
                    ViewState["CombineUnbrandedInspectionItems"] = value;
                }
            }
        }

        [Themeable(false), Description("Display One Owner Icons"), DefaultValue(true), Category("Data")]
        public bool ShowNoAccident
        {
            get
            {
                object value = ViewState["ShowNoAccident"];
                if (value == null)
                    return true;
                return (bool)value;
            }
            set
            {
                if (ShowNoAccident != value)
                {
                    ViewState["ShowNoAccident"] = value;
                }
            }
        }

        [Themeable(false), Description("Inspection Shows No Accident"), DefaultValue(true), Category("Data")]
        public bool HasNoAccident
        {
            get
            {
                bool returnValue = false;


                foreach (ListItem item in Items)
                {
                    int value = Int32Helper.ToInt32(item.Value);
                    if (value == 7 && item.Enabled)
                    {
                        returnValue = item.Enabled;
                        break;
                    }

                }

                return returnValue;
            }
        }

        [Themeable(false), Description("Display One Owner Icons"), DefaultValue(true), Category("Data")]
        public bool ShowCleanTitle
        {
            get
            {
                object value = ViewState["ShowCleanTitle"];
                if (value == null)
                    return true;
                return (bool)value;
            }
            set
            {
                if (ShowCleanTitle != value)
                {
                    ViewState["ShowCleanTitle"] = value;
                }
            }
        }

        [Themeable(false), Description("Inspection Shows Clean Title"), DefaultValue(true), Category("Data")]
        public bool HasCleanTitle
        {
            get
            {
                return !hasProblem;
            }
        }

        private CarfaxReportStatus ReportStatus
	    {
	        get
	        {
                CarfaxReportStatus status;

                if (expirationDate == DateTime.MinValue)
                {
                    status = CarfaxReportStatus.Unordered;
                }
                else if (expirationDate > DateTime.Now)
                {
                    status = CarfaxReportStatus.Valid;
                }
                else
                {
                    status = CarfaxReportStatus.Expired;
                }

	            return status;
	        }
	    }

	    private string SubmitButtonText
	    {
	        get
	        {
                string text;

                switch (ReportStatus)
                {
                    case CarfaxReportStatus.Unknown:
                    case CarfaxReportStatus.Unordered:
                        text = "Purchase Report";
                        break;
                    case CarfaxReportStatus.Expired:
                        text = "Renew Report";
                        break;
                    case CarfaxReportStatus.Valid:
                    default:
                        text = "View Report";
                        break;
                }

	            return text;
	        }
	    }

	    private string ReportStatusIndicatorCssClass
	    {
	        get
	        {
	            string cssClass;

                switch (ReportStatus)
                {
                    case CarfaxReportStatus.Unknown:
                        cssClass = "report-status-unknow";
                        break;
                    case CarfaxReportStatus.Unordered:
                        cssClass = "report-status-unordered";
                        break;
                    case CarfaxReportStatus.Valid:
                        cssClass = "report-status-valid";
                        break;
                    case CarfaxReportStatus.Expired:
                        cssClass = "report-status-expired";
                        break;
                    default:
                        cssClass = "report-status";
                        break;
                }

	            return cssClass;
	        }
	    }
        
        protected override string TagName
		{
			get
			{
				return "div";
			}
		}

		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}

        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            if (string.IsNullOrEmpty(CssClass))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "carfax_report_view");
            }
            else
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, CssClass + " carfax_report_view");
            }
            base.AddAttributesToRender(writer);
        }

		#endregion

		#region Data Binding

		protected override void PerformDataBinding(IEnumerable data)
		{
			// clear the values

			Items.Clear();

			expirationDate = DateTime.MinValue;

			userName = string.Empty;

			vin = string.Empty;

			// extract information from data

			if (data != null)
			{
				IEnumerator en = data.GetEnumerator();

				if (en.MoveNext())
				{
					object report = en.Current;

					expirationDate = (DateTime)DataBinder.GetPropertyValue(report, DataExpirationDateField);

					userName = DataBinder.GetPropertyValue(report, DataUserNameField, null);

					vin = DataBinder.GetPropertyValue(report, DataVinField, null);

                    hasProblem = bool.Parse(DataBinder.GetPropertyValue(report, DataHasProblemField, null));

					IEnumerable source = (IEnumerable)DataBinder.GetPropertyValue(report, DataItemsField);

					foreach (object value in source)
					{
						ListItem item = new ListItem();

						item.Text = DataBinder.GetPropertyValue(value, DataItemTextField, null);

						item.Value = DataBinder.GetPropertyValue(value, DataItemValueField, null);

						item.Enabled = bool.Parse(DataBinder.GetPropertyValue(value, DataItemSelectedField, null));

						Items.Add(item);
					}
				}                
			}
		} 

		#endregion

		#region Render

		protected override void RenderContents(HtmlTextWriter writer)
		{
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "report_status " + ReportStatusIndicatorCssClass);
            writer.AddAttribute(HtmlTextWriterAttribute.For, UniqueID + IdSeparator + "Button");
            writer.RenderBeginTag(HtmlTextWriterTag.Label);
            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "carfax_button");
			writer.AddAttribute(HtmlTextWriterAttribute.Type, "submit");
			writer.AddAttribute(HtmlTextWriterAttribute.Name, UniqueID);
            writer.AddAttribute(HtmlTextWriterAttribute.Id, UniqueID + IdSeparator + "Button");
            writer.AddAttribute(HtmlTextWriterAttribute.Value, SubmitButtonText);
			writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();

            if (AutoGenerateInspectionItems)
            {
                RenderAutoGeneratedInspectionItems(writer);
            }
            else
            {
                RenderSpecifiedInspectionItems(writer);
            }
		}

        protected void RenderAutoGeneratedInspectionItems(HtmlTextWriter writer)
        {
            if (Items.Count > 0)
            {
                IList<ListItem> enabledItems = new List<ListItem>();

                foreach (ListItem item in Items)
                {
                    if (item.Enabled)
                    {
                        enabledItems.Add(item);
                    }
                }

                if (enabledItems.Count > 0)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Ul);

                    foreach (ListItem item in enabledItems)
                    {
                        RenderInspectionItemBadge(writer, item.Text);
                    }

                    writer.RenderEndTag(); 
                }
            }
        }

        protected void RenderSpecifiedInspectionItems(HtmlTextWriter writer)
        {
            bool hasInspectionItems = ((ShowOneOwner && HasOneOwner) ||
                                (ShowBuyBackGuarantee && HasBuyBackGuarantee) ||
                                (ShowCleanTitle && HasCleanTitle) ||
                                (ShowNoAccident && HasNoAccident));

            if (hasInspectionItems)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Ul);
            }

            if (ShowOneOwner && HasOneOwner)
            {
                RenderOneOwner(writer);
            }
            
            if (ShowBuyBackGuarantee && HasBuyBackGuarantee) 
            {
                RenderBuyBackGuarentee(writer);
            }

            if (CombineUnbrandedInspectionItems)
            {
                RenderCombineUnbrandedInspectionItems(writer);
            }
            else
            {
                if (ShowCleanTitle && HasCleanTitle)
                {
                    RenderCleanTitle(writer);
                }

                if (ShowNoAccident && HasNoAccident)
                {
                    RenderNoAccident(writer);
                }
            }

            if (hasInspectionItems)
            {
                writer.RenderEndTag(); // UL
            }
        }

        protected static void RenderOneOwner(HtmlTextWriter writer)
        {
            RenderInspectionItemBadge(writer, "One Owner", "carfax-oneowner-badge");
        }

        protected static void RenderBuyBackGuarentee(HtmlTextWriter writer)
        {
            RenderInspectionItemBadge(writer, "Buyback Guarantee", "carfax-buyback-guarentee-badge");
        }

        protected static void RenderCleanTitle(HtmlTextWriter writer)
        {
            RenderInspectionItemBadge(writer, "Clean Title", "carfax-cleantitle");
        }

        protected static void RenderNoAccident(HtmlTextWriter writer)
        {
            RenderInspectionItemBadge(writer, "No Accident", "carfax-noaccident");
        }

        protected void RenderCombineUnbrandedInspectionItems(HtmlTextWriter writer)
        {
            if (HasCleanTitle && HasNoAccident)
            {
                RenderInspectionItemBadge(writer, "Clean Title, No Accident", "carfax-cleantitle-noaccident");
            }
            else if (!HasCleanTitle && HasNoAccident)
            {
                RenderNoAccident(writer);
            } 
            else if (HasCleanTitle && !HasNoAccident)
            {
                RenderCleanTitle(writer);
            }
        }

        protected static void RenderInspectionItemBadge(HtmlTextWriter writer, string innerText)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "carfax-inspection-badge");
            writer.RenderBeginTag(HtmlTextWriterTag.Li);
            writer.Write(innerText);
            writer.RenderEndTag();
        }

        protected static void RenderInspectionItemBadge(HtmlTextWriter writer, string innerText, string cssClass)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "carfax-inspection-badge " + cssClass);
            writer.AddAttribute(HtmlTextWriterAttribute.Title, innerText);
            writer.RenderBeginTag(HtmlTextWriterTag.Li);
            writer.Write(innerText);
            writer.RenderEndTag();
        }

		#endregion

		#region IStateManager

		protected override void LoadViewState(object savedState)
		{
			Pair pair = (Pair) savedState;

			base.LoadViewState(pair.First);

			if (pair.Second != null)
			{
				object[] values = (object[]) pair.Second;

				dataExpirationDateField = values[0] as string;
				dataItemsField = values[1] as string;
				dataItemTextField = values[2] as string;
				dataItemValueField = values[3] as string;
				dataItemSelectedField = values[4] as string;
				dataUserNameField = values[5] as string;
				dataVinField = values[6] as string;

				expirationDate = (DateTime) values[7];
				userName = values[8] as string;
				vin = values[9] as string;
			    hasProblem = (bool)values[10];

                ((IStateManager)Items).LoadViewState(values[11]);
			}
		}

		protected override object SaveViewState()
		{
			object[] values = new object[12];

			values[0] = dataExpirationDateField;
			values[1] = dataItemsField;
			values[2] = dataItemTextField;
			values[3] = dataItemValueField;
			values[4] = dataItemSelectedField;
			values[5] = dataUserNameField;
			values[6] = dataVinField;

			values[7] = expirationDate;
			values[8] = userName;
			values[9] = vin;
		    values[10] = hasProblem;

			values[11] = ((IStateManager)Items).SaveViewState();

			return new Pair(base.SaveViewState(), values);
		}

		#endregion

		#region IPostBackEventHandler Members

		protected void RaisePostBackEvent(string eventArgument)
		{
			OnClick(EventArgs.Empty);
		}

		void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
		{
			RaisePostBackEvent(eventArgument);
		}

		#endregion
	}
}
