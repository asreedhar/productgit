using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Web.Services.Protocols;
using System.Web.UI;

namespace FirstLook.VehicleHistoryReport.WebControls.Carfax
{
	[ParseChildren(true), PersistChildren(false)]
	public class CarfaxReportForm : ReportForm, IPostBackDataHandler
	{
		#region Properties

		public ReportFormMode CurrentMode
		{
			get
			{
                ReportFormMode currentMode;
                switch (ReportStatus)
                {                    
                    case CarfaxReportStatus.Valid:
                        currentMode = (ReportType == CarfaxReportType.CIP) ? ReportFormMode.ReadOnly : ReportFormMode.Edit;
                        break;
                    case CarfaxReportStatus.Expired:
                    case CarfaxReportStatus.Unknown:
                    case CarfaxReportStatus.Unordered:
                    default:
                        currentMode = ReportFormMode.Insert;
                        break;
                }

			    return currentMode;
			}
		}

        private CarfaxReportStatus ReportStatus
        {
            get
            {
                CarfaxReportStatus status;

                if (ExpirationDate == DateTime.MinValue)
                {
                    status = CarfaxReportStatus.Unordered;
                }
                else if (ExpirationDate > DateTime.Now)
                {
                    status = CarfaxReportStatus.Valid;
                }
                else
                {
                    status = CarfaxReportStatus.Expired;
                }

                return status;
            }
        }

		private string dataDisplayInHotListField = "DisplayInHotList";
		private string dataExpirationDateField = "ExpirationDate";
		private string dataReportTypeField = "ReportType";
		private string dataUserNameField = "UserName";
		private string dataVinField = "Vin";

		[Themeable(false), Description("Display in Hot List Property Name"), DefaultValue(""), Category("Data")]
		public string DataDisplayInHotListField
		{
			get { return dataDisplayInHotListField; }
			set { dataDisplayInHotListField = value; }
		}

		[Themeable(false), Description("Expiration Date Property Name"), DefaultValue(""), Category("Data")]
		public string DataExpirationDateField
		{
			get { return dataExpirationDateField; }
			set { dataExpirationDateField = value; }
		}

		[Themeable(false), Description("Report Type Property Name"), DefaultValue(""), Category("Data")]
		public string DataReportTypeField
		{
			get { return dataReportTypeField; }
			set { dataReportTypeField = value; }
		}

		[Themeable(false), Description("UserName Property Name"), DefaultValue(""), Category("Data")]
		public string DataUserNameField
		{
			get { return dataUserNameField; }
			set { dataUserNameField = value; }
		}

		[Themeable(false), Description("VIN Property Name"), DefaultValue(""), Category("Data")]
		public string DataVinField
		{
			get { return dataVinField; }
			set { dataVinField = value; }
		}

        private bool oldDisplayInHotList;
        private DateTime oldExpirationDate;
        private CarfaxReportType oldReportType;
        private string oldUserName;
        private string oldVin;
        
        private bool displayInHotList;
		private DateTime expirationDate;
		private CarfaxReportType reportType;
		private string userName;
		private string vin;

		[Themeable(false), Description("Report VIN in Hot List"), DefaultValue(""), Category("Data")]
		public bool DisplayInHotList
		{
			get { return displayInHotList; }
			protected set { displayInHotList = value; }
		}

		[Themeable(false), Description("Report Expiration Date"), DefaultValue(""), Category("Data")]
		public DateTime ExpirationDate
		{
			get { return expirationDate; }
            protected set { expirationDate = value; }
		}

		[Themeable(false), Description("Report Type"), DefaultValue(""), Category("Data")]
		public CarfaxReportType ReportType
		{
			get { return reportType; }
			protected set { reportType = value; }
		}

	    private string ReportTypeName
	    {
	        get
	        {
	            return GetNameForReportType(ReportType);
	        }
	    }
        
        [Themeable(false), Description("Account UserName"), DefaultValue(""), Category("Data")]
		public string UserName
		{
			get { return userName; }
            protected set { userName = value; }
		}

		[Themeable(false), Description("Report VIN"), DefaultValue(""), Category("Data")]
		public string Vin
		{
			get { return vin; }
            protected set { vin = value; }
		}

		private bool defaultDisplayInHotList;
		private CarfaxReportType defaultReportType;
		private bool hasPurchaseAuthority;

		[Themeable(false), Description("Default Display In Hot List"), DefaultValue(""), Category("Behavior")]
		public bool DefaultDisplayInHotList
		{
			get { return defaultDisplayInHotList; }
			set { defaultDisplayInHotList = value; }
		}

		[Themeable(false), Description("Default Report Type"), DefaultValue(""), Category("Behavior")]
		public CarfaxReportType DefaultReportType
		{
			get { return defaultReportType; }
			set { defaultReportType = value; }
		}

		[Themeable(false), Description("If user has purchase-authority"), DefaultValue(""), Category("Behavior")]
		public bool HasPurchaseAuthority
		{
			get { return hasPurchaseAuthority; }
			set { hasPurchaseAuthority = value; }
		}

	    [Themeable(false), Description("Report Url"), DefaultValue(""), Category("Behavior")]
	    public string ReportUrl
	    {
	        get
	        {
                return string.Format(
                "http://www.carfaxonline.com/cfm/Display_Dealer_Report.cfm?partner=FLK_0&UID={0}&vin={1}",
                UserName,
                Vin);
	        }
	    }
        
        private string errorMessage = string.Empty;

	    private bool hasError
	    {
	        get
	        {
	            return !string.IsNullOrEmpty(errorMessage);
	        }
	    }

		public string ErrorMessage
		{
			get { return errorMessage; }
		}      

		protected override string TagName
		{
			get
			{
				return "div";
			}
		}

		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}

	    protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            if (string.IsNullOrEmpty(CssClass))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, " vhr_report_form" + " vhr_report_form_" + CurrentMode);
            }
            else
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, CssClass + " vhr_report_form" + " vhr_report_form_" + CurrentMode);
            }

            base.AddAttributesToRender(writer);
        }

        #endregion

		#region Methods

		protected override Control FindControl(string id, int pathOffset)
		{
			return this;
		}

        protected static string GetNameForReportType(CarfaxReportType reportType)
        {
            string returnValue;
            switch (reportType)
            {
                case CarfaxReportType.BTC:
                    returnValue = "Branded Title Check";
                    break;
                case CarfaxReportType.VHR:
                    returnValue = "Vehicle History Report";
                    break;
                case CarfaxReportType.CIP:
                    returnValue = "Consumer Information Pack";
                    break;
                default:
                    returnValue = string.Empty;
                    break;
            }

            return returnValue;
        }

		#endregion

		#region Data Binding
		
		protected override void PerformDataBinding(IEnumerable data)
		{
			// clear the values

            DisplayInHotList = oldDisplayInHotList = DefaultDisplayInHotList;

            ExpirationDate = oldExpirationDate = DateTime.MinValue;

            ReportType = oldReportType = DefaultReportType;

            UserName = oldUserName = string.Empty;

            Vin = oldVin = string.Empty;

			// extract information from data

			if (data != null)
			{
				IEnumerator en = data.GetEnumerator();

				if (en.MoveNext())
				{
					object report = en.Current;

                    DisplayInHotList = oldDisplayInHotList = Convert.ToBoolean(DataBinder.GetPropertyValue(report, DataDisplayInHotListField));

                    ExpirationDate = oldExpirationDate = Convert.ToDateTime(DataBinder.GetPropertyValue(report, DataExpirationDateField));

                    ReportType = oldReportType = (CarfaxReportType)Enum.Parse(typeof(CarfaxReportType), DataBinder.GetPropertyValue(report, DataReportTypeField, null));

                    UserName = oldUserName = DataBinder.GetPropertyValue(report, DataUserNameField, null);

                    Vin = oldVin = DataBinder.GetPropertyValue(report, DataVinField, null);                    
				}
			}
		}
		#endregion

		#region Event Handling

		protected void HandleEvent(string commandName)
		{           
			if (string.Equals(commandName, "Insert"))
			{
			    CancelEventArgs e = new CancelEventArgs();

                OnInserting(e);

                if (!e.Cancel)
                {
                    IDictionary values = new Hashtable();

                    values["ReportType"] = (int)ReportType;
                    values["DisplayInHotList"] = DisplayInHotList;

                    GetData().Insert(values, HandleInsertCallback); 
                }
			} 
            else if (string.Equals(commandName, "Update"))
			{

                CancelEventArgs e = new CancelEventArgs();

                OnUpdating(e);

                if (!e.Cancel)
                {
                    IDictionary keys = new Hashtable();
                    IDictionary newValues = new Hashtable();
                    IDictionary oldValues = new Hashtable();

                    ReportType = _reportType;

                    newValues["ReportType"] = (int)ReportType;
                    newValues["DisplayInHotList"] = DisplayInHotList;

                    oldValues["ReportType"] = (int)oldReportType;
                    oldValues["DisplayInHotList"] = oldDisplayInHotList;

                    GetData().Update(keys, newValues, oldValues, HandleUpdateCallback); 
                }
			}
		}

		protected bool HandleInsertCallback(int affectedRecords, Exception ex)
		{
			if (ex != null)
			{
				HandleCallbackException(ex);
			}
			else
			{
                OnInserted(new ReportFormInsertedEventArgs(affectedRecords, ex));
				errorMessage = string.Empty;

			    DataBind();
			}

            return true;
		}

		protected bool HandleUpdateCallback(int affectedRecords, Exception ex)
		{
			if (ex != null)
			{
				HandleCallbackException(ex);
			}
			else
			{
                OnUpdated(new ReportFormUpdatedEventArgs(affectedRecords, ex));
				errorMessage = string.Empty;

                DataBind();
			}

			return true;
		}

		private void HandleCallbackException(Exception ex)
		{
			SoapException ce = ex as SoapException;

			if (ce != null)
			{
				if (ce.Code == SoapException.ServerFaultCode)
				{
                    string message = ce.Detail.InnerText;

                    int code;

                    if (int.TryParse(message, out code))
                    {
                        HandleCallbackException(code);

                        return;
                    }
                    else
                    {
                        HandleCallbackException(906);
                    }
				}
			}
			else if (!string.IsNullOrEmpty(ex.Message))
			{
				string message = ex.Message;

				int code;

				if (int.TryParse(message, out code))
				{
					HandleCallbackException(code);

					return;
				}
			}
			else
			{
				HandleCallbackException(906);
			}
		}

		private void HandleCallbackException(int code)
		{
			switch (code)
			{
				case 600:
                    errorMessage = "VIN does not exist in Inventory Manager OR no data available.";
					break;
				case 800:
					errorMessage = "Invalid VIN";
					break;
				case 900:
					errorMessage = "Service Unavailable";
					break;
				case 901:
					errorMessage = "Transaction Error";
					break;
				case 903:
					errorMessage = "Account Status. Contact CARFAX to verify account status";
					break;
				case 904:
					errorMessage = "Bad User Name";
					break;
				case 905:
					errorMessage = "Bad Password";
					break;
				case 906:
                    errorMessage = "No Code Supplied. Contact First Look Help Desk if problem persists";
					break;
				default:
					errorMessage = "Unexpected Error. Please contact FirstLook support.";
					break;
			}
		}

		#endregion

		#region Life Cycle

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			Page.RegisterRequiresPostBack(this); // because of the damn checkbox :P
		}

		#endregion

		#region Render

		protected override void RenderContents(HtmlTextWriter writer)
		{
			if (CurrentMode == ReportFormMode.ReadOnly)
			{
				RenderReadOnlyMode(writer);
			}
			else if (HasPurchaseAuthority)
			{
				if (CurrentMode == ReportFormMode.Edit)
				{
					RenderEditMode(writer);
				}
				else if (CurrentMode == ReportFormMode.Insert)
				{
					RenderInsertMode(writer);
				}
			}
			else
			{
				if (CurrentMode == ReportFormMode.Edit)
				{
					RenderReadOnlyMode(writer);
				}
				else
				{
					RenderCannotPurchaseMode(writer);
				}
			}
		}

		private static void RenderCannotPurchaseMode(HtmlTextWriter writer)
		{
			writer.RenderBeginTag(HtmlTextWriterTag.P);
			writer.Write("You have no purchase rights! Sorry!");
			writer.RenderEndTag();
		}

		private void RenderReadOnlyMode(HtmlTextWriter writer)
		{
			RenderCarfaxLink(writer);
		}

        private void RenderEditMode(HtmlTextWriter writer)
        {
            RenderCarfaxLink(writer);

            if (hasError)
            {
                RenderErrorMessage(writer);
            }

            if (ReportType == CarfaxReportType.BTC)
            {
                RenderHiddenReportTypeInput(writer, CarfaxReportType.VHR);
            }
            else if (ReportType == CarfaxReportType.VHR)
            {
                RenderHiddenReportTypeInput(writer, CarfaxReportType.CIP);
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "controls");
            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

            RenderUpgradeButton(writer);


            writer.RenderEndTag();
        }

	    private void RenderInsertMode(HtmlTextWriter writer)
		{
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "order_form");
            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);
			
            RenderRadioGroup(writer);

			RenderHotListCheckBox(writer);

            writer.RenderEndTag();

            if (hasError)
            {
                RenderErrorMessage(writer);
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "controls");
            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

			RenderPurchaseButton(writer);

            writer.RenderEndTag();
		}

	    private void RenderErrorMessage(HtmlTextWriter writer)
	    {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "error");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

	        writer.AddAttribute(HtmlTextWriterAttribute.Class, "error_msg");
            writer.RenderBeginTag(HtmlTextWriterTag.P);
            writer.Write(errorMessage);
            writer.RenderEndTag();

            writer.RenderEndTag();
	    }

	    private void RenderHotListCheckBox(HtmlTextWriter writer)
		{
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "carfax_hot_list");
            writer.RenderBeginTag(HtmlTextWriterTag.P);

			string Id = UniqueID + IdSeparator + "HotList";

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "checkbox");
			writer.AddAttribute(HtmlTextWriterAttribute.Type, "checkbox");
			writer.AddAttribute(HtmlTextWriterAttribute.Name, Id);

            if (DisplayInHotList)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Checked, "checked");
            }

			writer.RenderBeginTag(HtmlTextWriterTag.Input);
			writer.RenderEndTag();

			writer.AddAttribute(HtmlTextWriterAttribute.For, Id);
			writer.RenderBeginTag(HtmlTextWriterTag.Label);
			writer.Write("Display CARFAX Consumer Free Report in my online listings and add to CARFAX Hot Listings");
			writer.RenderEndTag();

            writer.RenderEndTag();
		}

		private void RenderRadioGroup(HtmlTextWriter writer)
		{
			string group = UniqueID + IdSeparator + "ReportType";

            RenderRadioButton(writer, group, CarfaxReportType.BTC);
            RenderRadioButton(writer, group, CarfaxReportType.VHR);
            RenderRadioButton(writer, group, CarfaxReportType.CIP);
		}		

        private void RenderRadioButton(HtmlTextWriter writer, string group, CarfaxReportType radioReportType)
        {

            string id = group + IdSeparator;

            writer.RenderBeginTag(HtmlTextWriterTag.P);
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "radio");
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "radio");
            writer.AddAttribute(HtmlTextWriterAttribute.Id, id + radioReportType);
            writer.AddAttribute(HtmlTextWriterAttribute.Name, group);
            writer.AddAttribute(HtmlTextWriterAttribute.Value, radioReportType.ToString());

            if (radioReportType == ReportType)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Checked, "checked");
            }

            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.For, id);
            writer.RenderBeginTag(HtmlTextWriterTag.Label);
            writer.Write( GetNameForReportType(radioReportType) );
            writer.RenderEndTag();
            writer.RenderEndTag();
        }

        private void RenderPurchaseButton(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "button");
            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "submit");
            writer.AddAttribute(HtmlTextWriterAttribute.Name, UniqueID + IdSeparator + "Purchase");

            string value = hasError
                               ? "Try Again"
                               : (ReportStatus == CarfaxReportStatus.Expired) ? "Reorder Report" : "Order Report";

            writer.AddAttribute(HtmlTextWriterAttribute.Value, value);
            
            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();

            writer.RenderEndTag();
        }

        private void RenderHiddenReportTypeInput(HtmlTextWriter writer, CarfaxReportType inputReportType)
		{
            string name = UniqueID + IdSeparator + "ReportType";
            string id = name + IdSeparator;

            writer.AddAttribute(HtmlTextWriterAttribute.Type, "hidden");
            writer.AddAttribute(HtmlTextWriterAttribute.Id, id + inputReportType);
            writer.AddAttribute(HtmlTextWriterAttribute.Name, name);
            writer.AddAttribute(HtmlTextWriterAttribute.Value, inputReportType.ToString());
            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();
		}
        
        private void RenderUpgradeButton(HtmlTextWriter writer)
		{
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "button");
            writer.RenderBeginTag(HtmlTextWriterTag.Span);

			string text;

			if (ReportType == CarfaxReportType.BTC)
			{
				text = "Upgrade to Vehicle History Report";
			}
			else if (ReportType == CarfaxReportType.VHR)
			{
				text = "Upgrade to Consumer Information Pack";
			}
			else
			{
				text = "Renew Consumer Information Pack";
			}

			writer.AddAttribute(HtmlTextWriterAttribute.Type, "submit");
			writer.AddAttribute(HtmlTextWriterAttribute.Name, UniqueID + IdSeparator + "Upgrade");
			writer.AddAttribute(HtmlTextWriterAttribute.Value, text);
			writer.RenderBeginTag(HtmlTextWriterTag.Input);
			writer.RenderEndTag();

            writer.RenderEndTag();
		}

		private void RenderCarfaxLink(HtmlTextWriter writer)
		{
			string text = string.Format("View {0}", ReportTypeName);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "vhr_report_link");
            writer.RenderBeginTag(HtmlTextWriterTag.P);

            writer.AddAttribute(HtmlTextWriterAttribute.Target, "CarfaxReport");
			writer.AddAttribute(HtmlTextWriterAttribute.Href, ReportUrl);
			writer.RenderBeginTag(HtmlTextWriterTag.A);
			writer.Write(text);
			writer.RenderEndTag();

            writer.RenderEndTag();
		}

		#endregion

		#region IPostBackDataHandler Members

		private string _commandName;
	    private CarfaxReportType _reportType;

	    protected bool LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
			if (IsEnabled)
			{
				int controlNameOffset = UniqueID.Length + 1;

				if (postDataKey.Length > controlNameOffset)
				{
					string controlName = postDataKey.Substring(controlNameOffset);

					if (string.Equals(controlName, "HotList"))
					{
						bool flag = postCollection[postDataKey] != null;

						if (flag != DisplayInHotList)
						{
							DisplayInHotList = flag;

							return true;
						}
					}
					else if (string.Equals(controlName, "ReportType"))
					{
						string value = postCollection[postDataKey];

						if (Enum.IsDefined(typeof(CarfaxReportType), value))
						{
							CarfaxReportType type = (CarfaxReportType) Enum.Parse(typeof (CarfaxReportType), value);

						    _reportType = ReportType;

                            if (type != _reportType)
							{
								_reportType = type;

								return true;
							} 
						}
					}
					else if (string.Equals(controlName, "Purchase"))
					{
						_commandName = "Insert";

						return true;
					}
					else if (string.Equals(controlName, "Upgrade"))
					{
						_commandName = "Update";

						return true;
					}
				}
			}

			return false;
		}

		protected void RaisePostDataChangedEvent()
		{
			HandleEvent(_commandName);

		    _commandName = string.Empty;
		}

		bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
			return LoadPostData(postDataKey, postCollection);
		}

		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{
			RaisePostDataChangedEvent();
		}

		#endregion

        #region IStateManager Members

        protected override void LoadViewState(object state)
        {
            Pair pair = (Pair)state;

            base.LoadViewState(pair.First);

            if (pair.Second != null)
            {
                object[] values = (object[])pair.Second;

                displayInHotList = (bool)values[0];
                expirationDate = (DateTime)values[1];
                reportType = (CarfaxReportType) Enum.ToObject( typeof (CarfaxReportType), (int)values[2]);
                userName = values[3] as string;
                vin = values[4] as string;

                oldDisplayInHotList = (bool)values[5];
                oldExpirationDate = (DateTime)values[6];
                oldReportType = (CarfaxReportType)Enum.ToObject(typeof(CarfaxReportType), (int)values[7]);
                oldUserName = values[8] as string;
                oldVin = values[9] as string;

            	defaultDisplayInHotList = (bool) values[10];
				defaultReportType = (CarfaxReportType)values[11];
				hasPurchaseAuthority = (bool)values[12];
			}
        }

        protected override object SaveViewState()
        {
            object[] values = new object[13];

            values[0] = DisplayInHotList;
            values[1] = ExpirationDate;
            values[2] = (int)ReportType;
            values[3] = UserName;
            values[4] = Vin;

            values[5] = oldDisplayInHotList;
            values[6] = oldExpirationDate;
            values[7] = (int)oldReportType;
            values[8] = oldUserName;
            values[9] = oldVin;

			values[10] = defaultDisplayInHotList;
			values[11] = defaultReportType;
			values[12] = hasPurchaseAuthority;

            return new Pair(base.SaveViewState(), values);
        }

        #endregion
    }
}
