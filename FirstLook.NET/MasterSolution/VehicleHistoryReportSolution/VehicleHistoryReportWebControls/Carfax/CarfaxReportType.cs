namespace FirstLook.VehicleHistoryReport.WebControls.Carfax
{
	/// <remarks>
	/// YES, these enum names should be Btc, Vhr and Cip but
	/// I deliberately named them contrary to the .NET best
	/// practises to maintain consistency with the CARFAX
	/// specification. DO NOT CHANGE THEM BACK AGAIN!!!
	/// </remarks>
	public enum CarfaxReportType
	{
		Undefined,
		BTC,
		VHR,
		CIP
	}
}
