namespace FirstLook.VehicleHistoryReport.WebControls.Carfax
{
    public enum CarfaxReportStatus
    {
        Unknown,
        Unordered,
        Valid,
        Expired
    }
}
