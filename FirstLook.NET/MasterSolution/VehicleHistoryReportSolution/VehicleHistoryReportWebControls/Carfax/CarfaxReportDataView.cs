using System;
using System.Collections;
using System.Collections.Specialized;
using System.Threading;
using System.Web.UI;
using FirstLook.Common.Core.Utilities;
using FirstLook.VehicleHistoryReport.WebService.Client.Carfax;

namespace FirstLook.VehicleHistoryReport.WebControls.Carfax
{
	internal sealed class CarfaxReportDataView : ReportDataView
	{
		private readonly CarfaxReportDataSource owner;

		public CarfaxReportDataView(CarfaxReportDataSource owner, string viewName) : base(owner, viewName)
		{
			this.owner = owner;
		}

		protected override int ExecuteInsert(IDictionary values)
		{
			// inserting event args

			int dealerId = owner.GetDealerId();

            string vin = owner.GetVin();

			ReportDataSourceMethodEventArgs inserting = GetMethodEventArgs(dealerId, vin);

			OnInserting(inserting);

            if (inserting.Cancel)
            {
                return 0;
            }

			// get updated values from event args

            GetInputParameters(ref dealerId, ref vin, inserting);

			// validate values from data-source

			if (!IsValid(dealerId, vin)) return 0;

			// validate values from dictionary

			WebService.Client.Carfax.CarfaxReportType reportType = GetReportType(values["ReportType"]);

			if (reportType == WebService.Client.Carfax.CarfaxReportType.Undefined)
			{
				return 0;
			}

			bool displayInHotList = GetDisplayInHotList(values["DisplayInHotList"]);

			// make request for report

			CarfaxReportTO report = PurchaseReport(dealerId, vin, reportType, displayInHotList, true);

			return report == null ? 0 : 1;
		}

		protected override IEnumerable ExecuteSelect(DataSourceSelectArguments arguments)
		{
			arguments.RaiseUnsupportedCapabilitiesError(this);

			int dealerId = owner.GetDealerId();

			string vin = owner.GetVin();

			// raise selecting args

			OrderedDictionary parameters = GetInputParameters(dealerId, vin);

			ReportDataSourceSelectingEventArgs selecting = new ReportDataSourceSelectingEventArgs(
				parameters,
				arguments,
				false);

			OnSelecting(selecting);

			if (selecting.Cancel)
			{
				return null;
			}

			// get updated values from event args

			GetInputParameters(ref dealerId, ref vin, selecting);

			// validate values from data-source

			if (!IsValid(dealerId, vin)) return new CarfaxReportTO[0];

			// make request for report

			CarfaxReportTO report = GetReport(dealerId, vin);

			// return

			if (report == null)
			{
				return new CarfaxReportTO[0];
			}
			else
			{
				return new CarfaxReportTO[] {report};
			}
		}

		protected override int ExecuteUpdate(IDictionary keys, IDictionary values, IDictionary oldValues)
		{
			// validate values from data-source

			int dealerId = owner.GetDealerId();

            string vin = owner.GetVin();

			OrderedDictionary parameters = GetInputParameters(dealerId, vin);

            ReportDataSourceMethodEventArgs updating = new ReportDataSourceMethodEventArgs(parameters);

            OnUpdating(updating);

            if (updating.Cancel)
            {
                return 0;
            }

			GetInputParameters(ref dealerId, ref vin, updating);

			if (!IsValid(dealerId, vin)) return 0;

			// validate values from dictionary

			WebService.Client.Carfax.CarfaxReportType reportType = GetReportType(values["ReportType"]);

			if (reportType == WebService.Client.Carfax.CarfaxReportType.Undefined)
			{
				return 0;
			}

			bool displayInHotList = GetDisplayInHotList(values["DisplayInHotList"]);

			// make request for report

			CarfaxReportTO report = PurchaseReport(dealerId, vin, reportType, displayInHotList, false);

			return report == null ? 0 : 1;
		}

		private static ReportDataSourceMethodEventArgs GetMethodEventArgs(int dealerId, string vin)
		{
			return new ReportDataSourceMethodEventArgs(GetInputParameters(dealerId, vin));
		}

		private static OrderedDictionary GetInputParameters(int dealerId, string vin)
		{
			OrderedDictionary parameters = new OrderedDictionary(StringComparer.OrdinalIgnoreCase);
			parameters.Add(CarfaxReportDataSource.DealerIdParameterName, dealerId);
			parameters.Add(CarfaxReportDataSource.VinParameterName, vin);
			return parameters;
		}

		private static void GetInputParameters(ref int dealerId, ref string vin, ReportDataSourceMethodEventArgs args)
		{
			foreach (DictionaryEntry entry in args.InputParameters)
			{
				if (string.Equals(entry.Key.ToString(), CarfaxReportDataSource.DealerIdParameterName, StringComparison.OrdinalIgnoreCase))
				{
					if (entry.Value != null)
					{
						dealerId = Int32Helper.ToInt32(entry.Value);
					}
				}
				else if (string.Equals(entry.Key.ToString(), CarfaxReportDataSource.VinParameterName, StringComparison.OrdinalIgnoreCase))
				{
					if (entry.Value != null)
					{
						vin = entry.Value.ToString();
					}
				}
			}
		}

		private static bool IsValid(int dealerId, string vin)
		{
			if (dealerId == 0)
			{
				return false;
			}

			if (string.IsNullOrEmpty(vin) || vin.Length != 17)
			{
				return false;
			}

			return true;
		}

		private static bool GetDisplayInHotList(object displayInHotListValue)
		{
			bool displayInHotList = false;

			if (displayInHotListValue is bool)
			{
				displayInHotList = (bool)displayInHotListValue;
			}

			return displayInHotList;
		}

		private static WebService.Client.Carfax.CarfaxReportType GetReportType(object reportTypeValue)
		{
			WebService.Client.Carfax.CarfaxReportType reportType = WebService.Client.Carfax.CarfaxReportType.Undefined;

			if (reportTypeValue is int)
			{
				if (Enum.IsDefined(typeof(WebService.Client.Carfax.CarfaxReportType), reportTypeValue))
				{
					reportType =
						(WebService.Client.Carfax.CarfaxReportType)
						Enum.ToObject(typeof(WebService.Client.Carfax.CarfaxReportType), reportTypeValue);
				}
			}

			return reportType;
		}

		private static CarfaxWebService GetService()
		{
			UserIdentity identity = new UserIdentity();

			identity.UserName = Thread.CurrentPrincipal.Identity.Name;

			CarfaxWebService service = new CarfaxWebService();

			service.UserIdentityValue = identity;

			return service;
		}

		private CarfaxReportTO GetReport(int dealerId, string vin)
		{
			CarfaxWebService service = GetService();

			bool raisedStatusEvent = false;

			CarfaxReportTO report = null;

			try
			{
				report = service.GetReport(dealerId, vin);
			}
			catch (Exception exception)
			{
				raisedStatusEvent = true;

				ReportDataSourceStatusEventArgs e = new ReportDataSourceStatusEventArgs(
					null, new OrderedDictionary(), exception);

				OnSelected(e);

				if (!e.ExceptionHandled)
				{
					throw;
				}
			}
			finally
			{
				if (!raisedStatusEvent)
				{
					ReportDataSourceStatusEventArgs e = new ReportDataSourceStatusEventArgs(
						null, new OrderedDictionary(), null);

					OnSelected(e);
				}
			}

			return report;
		}

		private CarfaxReportTO PurchaseReport(int dealerId, string vin, WebService.Client.Carfax.CarfaxReportType reportType, bool displayInHotList, bool isInserting)
		{
			CarfaxWebService service = GetService();

			bool raisedStatusEvent = false;

			CarfaxReportTO report;

			try
			{
				report = service.PurchaseReport(dealerId, vin, reportType, displayInHotList, CarfaxTrackingCode.FLN);
			}
			catch (Exception exception)
			{
				raisedStatusEvent = true;

				ReportDataSourceStatusEventArgs e = new ReportDataSourceStatusEventArgs(
					null, new OrderedDictionary(), exception);

				if (isInserting) OnInserted(e); else OnUpdated(e);

				if (!e.ExceptionHandled)
				{
					throw;
				}

				throw;
			}
			finally
			{
				if (!raisedStatusEvent)
				{
					ReportDataSourceStatusEventArgs e = new ReportDataSourceStatusEventArgs(
						null, new OrderedDictionary(), null);

					if (isInserting) OnInserted(e); else OnUpdated(e);
				}
			}

			return report;
		}
	}
}