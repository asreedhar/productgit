using System;

namespace FirstLook.VehicleHistoryReport.WebControls
{
	public class ReportFormInsertedEventArgs : EventArgs
	{
		private readonly int _affectedRows;
		private readonly Exception _exception;
		private bool _exceptionHandled = false;
		private bool _keepInEditMode = false;

		public ReportFormInsertedEventArgs(int _affectedRows, Exception _exception)
		{
			this._affectedRows = _affectedRows;
			this._exception = _exception;
		}

		public int AffectedRows
		{
			get { return _affectedRows; }
		}

		public bool KeepInEditMode
		{
			get { return _keepInEditMode; }
			set { _keepInEditMode = value; }
		}

		public Exception Exception
		{
			get { return _exception; }
		}

		public bool ExceptionHandled
		{
			get { return _exceptionHandled; }
			set { _exceptionHandled = value; }
		}
	}
}