using System;
using System.Web.UI;

namespace FirstLook.VehicleHistoryReport.WebControls
{
	public abstract class ReportDataView : DataSourceView
	{
		public ReportDataView(IDataSource owner, string viewName) : base(owner, viewName)
		{
		}

		#region Events

		private static readonly object EventSelected = new object();

		private static readonly object EventSelecting = new object();

		private static readonly object EventInserting = new object();

		private static readonly object EventInserted = new object();

		private static readonly object EventUpdating = new object();

		private static readonly object EventUpdated = new object();

		public event ReportDataSourceStatusEventHandler Selected
		{
			add { Events.AddHandler(EventSelected, value); }
			remove { Events.RemoveHandler(EventSelected, value); }
		}

		protected virtual void OnSelected(ReportDataSourceStatusEventArgs e)
		{
			ReportDataSourceStatusEventHandler handler = Events[EventSelected] as ReportDataSourceStatusEventHandler;
			if (handler != null)
			{
				handler(this, e);
			}
		}

		public event ReportDataSourceSelectingEventHandler Selecting
		{
			add { Events.AddHandler(EventSelecting, value); }
			remove { Events.RemoveHandler(EventSelecting, value); }
		}

		protected virtual void OnSelecting(ReportDataSourceSelectingEventArgs e)
		{
			ReportDataSourceSelectingEventHandler handler = Events[EventSelecting] as ReportDataSourceSelectingEventHandler;
			if (handler != null)
			{
				handler(this, e);
			}
		}

		public event ReportDataSourceMethodEventHandler Inserting
		{
			add { Events.AddHandler(EventInserting, value); }
			remove { Events.RemoveHandler(EventInserting, value); }
		}

		protected virtual void OnInserting(ReportDataSourceMethodEventArgs e)
		{
			ReportDataSourceMethodEventHandler handler = Events[EventInserting] as ReportDataSourceMethodEventHandler;
			if (handler != null)
			{
				handler(this, e);
			}
		}

		public event ReportDataSourceStatusEventHandler Inserted
		{
			add { Events.AddHandler(EventInserted, value); }
			remove { Events.RemoveHandler(EventInserted, value); }
		}

		protected virtual void OnInserted(ReportDataSourceStatusEventArgs e)
		{
			ReportDataSourceStatusEventHandler handler = Events[EventSelected] as ReportDataSourceStatusEventHandler;
			if (handler != null)
			{
				handler(this, e);
			}
		}

		public event ReportDataSourceMethodEventHandler Updating
		{
			add { Events.AddHandler(EventUpdating, value); }
			remove { Events.RemoveHandler(EventUpdating, value); }
		}

		protected virtual void OnUpdating(ReportDataSourceMethodEventArgs e)
		{
			ReportDataSourceMethodEventHandler handler = Events[EventUpdating] as ReportDataSourceMethodEventHandler;
			if (handler != null)
			{
				handler(this, e);
			}
		}

		public event ReportDataSourceStatusEventHandler Updated
		{
			add { Events.AddHandler(EventUpdated, value); }
			remove { Events.RemoveHandler(EventUpdated, value); }
		}

		protected virtual void OnUpdated(ReportDataSourceStatusEventArgs e)
		{
			ReportDataSourceStatusEventHandler handler = Events[EventUpdated] as ReportDataSourceStatusEventHandler;
			if (handler != null)
			{
				handler(this, e);
			}
		}

		#endregion

		internal void RaiseChangedEvent()
		{
			OnDataSourceViewChanged(EventArgs.Empty);
		}
	}
}
