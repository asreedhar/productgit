using System;
using System.ComponentModel;
using System.Web.UI.WebControls;

namespace FirstLook.VehicleHistoryReport.WebControls
{
	public abstract class ReportForm : DataBoundControl
	{
		#region Events

		private static readonly object EventModeChanging = new object();
		private static readonly object EventInserted = new object();
		private static readonly object EventInserting = new object();
		private static readonly object EventUpdated = new object();
		private static readonly object EventUpdating = new object();

		public event EventHandler<ReportFormModeEventArgs> ModeChanging
		{
			add
			{
				Events.AddHandler(EventModeChanging, value);
			}
			remove
			{
				Events.RemoveHandler(EventModeChanging, value);
			}
		}

		protected virtual void OnModeChanging(ReportFormModeEventArgs e)
		{
			EventHandler<ReportFormModeEventArgs> handler = (EventHandler<ReportFormModeEventArgs>)Events[EventModeChanging];

			if (handler != null)
			{
				handler(this, e);
			}
		}

		public event EventHandler<ReportFormUpdatedEventArgs> Updated
		{
			add
			{
				Events.AddHandler(EventUpdated, value);
			}
			remove
			{
				Events.RemoveHandler(EventUpdated, value);
			}
		}

		protected virtual void OnUpdated(ReportFormUpdatedEventArgs e)
		{
			EventHandler<ReportFormUpdatedEventArgs> handler = (EventHandler<ReportFormUpdatedEventArgs>)Events[EventUpdated];

			if (handler != null)
			{
				handler(this, e);
			}
		}

		public event EventHandler<CancelEventArgs> Updating
		{
			add
			{
				Events.AddHandler(EventUpdating, value);
			}
			remove
			{
				Events.RemoveHandler(EventUpdating, value);
			}
		}

		protected virtual void OnUpdating(CancelEventArgs e)
		{
			EventHandler<CancelEventArgs> handler = (EventHandler<CancelEventArgs>)Events[EventUpdating];

			if (handler != null)
			{
				handler(this, e);
			}
		}

		public event EventHandler<CancelEventArgs> Inserting
		{
			add
			{
				Events.AddHandler(EventInserting, value);
			}
			remove
			{
				Events.RemoveHandler(EventInserting, value);
			}
		}

		protected virtual void OnInserting(CancelEventArgs e)
		{
			EventHandler<CancelEventArgs> handler = (EventHandler<CancelEventArgs>)Events[EventInserting];

			if (handler != null)
			{
				handler(this, e);
			}
		}

		public event EventHandler<ReportFormInsertedEventArgs> Inserted
		{
			add
			{
				Events.AddHandler(EventInserted, value);
			}
			remove
			{
				Events.RemoveHandler(EventInserted, value);
			}
		}

		protected virtual void OnInserted(ReportFormInsertedEventArgs e)
		{
			EventHandler<ReportFormInsertedEventArgs> handler = (EventHandler<ReportFormInsertedEventArgs>)Events[EventInserted];

			if (handler != null)
			{
				handler(this, e);
			}
		}

		#endregion
	}
}
