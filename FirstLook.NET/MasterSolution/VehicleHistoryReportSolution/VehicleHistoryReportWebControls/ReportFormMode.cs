namespace FirstLook.VehicleHistoryReport.WebControls
{
	public enum ReportFormMode
	{
		Edit,
		Insert,
		ReadOnly
	}
}