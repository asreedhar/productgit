using System;
using System.Collections;

namespace FirstLook.VehicleHistoryReport.WebControls
{
	public class ReportDataSourceStatusEventArgs : EventArgs
	{
		private int _affectedRows;
		private readonly Exception _exception;
		private bool _exceptionHandled;
		private readonly IDictionary _outputParameters;
		private readonly object _returnValue;

		public ReportDataSourceStatusEventArgs(object returnValue, IDictionary outputParameters)
			: this(returnValue, outputParameters, null)
		{
		}

		public ReportDataSourceStatusEventArgs(object returnValue, IDictionary outputParameters, Exception exception)
		{
			_affectedRows = -1;
			_returnValue = returnValue;
			_outputParameters = outputParameters;
			_exception = exception;
		}

		public int AffectedRows
		{
			get { return _affectedRows; }
			set { _affectedRows = value; }
		}

		public Exception Exception
		{
			get { return _exception; }
		}

		public bool ExceptionHandled
		{
			get { return _exceptionHandled; }
			set { _exceptionHandled = value; }
		}

		public IDictionary OutputParameters
		{
			get { return _outputParameters; }
		}

		public object ReturnValue
		{
			get { return _returnValue; }
		}
	}
}