using System.ComponentModel;

namespace FirstLook.VehicleHistoryReport.WebControls
{
	public class ReportFormModeEventArgs : CancelEventArgs
	{
		private readonly ReportFormMode newMode;

		public ReportFormModeEventArgs(ReportFormMode newMode)
		{
			this.newMode = newMode;
		}

		public ReportFormMode NewMode
		{
			get { return newMode; }
		}
	}
}