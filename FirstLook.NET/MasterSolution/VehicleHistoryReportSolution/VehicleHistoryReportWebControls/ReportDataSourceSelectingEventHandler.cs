namespace FirstLook.VehicleHistoryReport.WebControls
{
	public delegate void ReportDataSourceSelectingEventHandler(
		object sender, ReportDataSourceSelectingEventArgs e);
}