using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstLook.VehicleHistoryReport.WebControls.AutoCheck
{
	public class AutoCheckReportDataSource : ReportDataSource
	{
		private static ReadOnlyCollection<string> ViewNames;

		internal static readonly string DealerIdParameterName = "DealerId";

		internal static readonly string VinParameterName = "Vin";

		private static readonly string ViewName = "Report";

		private AutoCheckReportDataView dataView;

		protected override ReportDataView DataView
		{
			get
			{
				if (dataView == null)
				{
					dataView = new AutoCheckReportDataView(this, ViewName);
				}
				return dataView;
			}
		}

		public int GetDealerId()
		{
			if (HasParameters)
			{
				Parameter dealerParameter = Parameters[DealerIdParameterName];

				if (dealerParameter != null)
				{
					IOrderedDictionary parameterValues = Parameters.GetValues(Context, this);

					object value = parameterValues[dealerParameter.Name];

					if (value != null)
					{
						return (int)value;
					}
				}
			}

			return 0;
		}

		public string GetVin()
		{
			if (HasParameters)
			{
				Parameter vinParameter = Parameters[VinParameterName];

				if (vinParameter != null)
				{
					IOrderedDictionary parameterValues = Parameters.GetValues(Context, this);

					object value = parameterValues[vinParameter.Name];

					if (value != null)
					{
						return (string)value;
					}
				}
			}

			return string.Empty;
		}

		protected override ICollection GetViewNames()
		{
			if (ViewNames == null)
			{
				ViewNames = new ReadOnlyCollection<string>(
					new string[]
						{
							ViewName
						});
			}

			return ViewNames;
		}

		protected override DataSourceView GetView(string viewName)
		{
			if (String.IsNullOrEmpty(viewName) || (String.Compare(viewName, ViewName, StringComparison.OrdinalIgnoreCase) == 0))
			{
				return DataView;
			}
			throw new ArgumentOutOfRangeException("viewName");
		}
	}
}
