using System;
using System.Collections;
using System.Collections.Specialized;
using System.Threading;
using System.Web.UI;
using FirstLook.Common.Core.Utilities;
using FirstLook.VehicleHistoryReport.WebService.Client.AutoCheck;

namespace FirstLook.VehicleHistoryReport.WebControls.AutoCheck
{
	internal sealed class AutoCheckReportDataView : ReportDataView
	{
		private readonly AutoCheckReportDataSource owner;

        public AutoCheckReportDataView(AutoCheckReportDataSource owner, string viewName) : base(owner, viewName)
		{
			this.owner = owner;
		}

		protected override int ExecuteInsert(IDictionary values)
		{
			// inserting event args

			int dealerId = owner.GetDealerId();

			string vin = owner.GetVin();

			ReportDataSourceMethodEventArgs inserting = GetMethodEventArgs(dealerId, vin);

			OnInserting(inserting);

			if (inserting.Cancel)
			{
				return 0;
			}

			// get updated values from event args

			GetInputParameters(ref dealerId, ref vin, inserting);

			// validate values from data-source

			if (!IsValid(dealerId, vin)) return 0;

			AutoCheckReportTO report = PurchaseReport(dealerId, vin, true);

			return report == null ? 0 : 1;
		}

		protected override IEnumerable ExecuteSelect(DataSourceSelectArguments arguments)
		{
			arguments.RaiseUnsupportedCapabilitiesError(this);

			int dealerId = owner.GetDealerId();

			string vin = owner.GetVin();

			// raise selecting args

			OrderedDictionary parameters = GetInputParameters(dealerId, vin);

			ReportDataSourceSelectingEventArgs selecting = new ReportDataSourceSelectingEventArgs(
				parameters,
				arguments,
				false);

			OnSelecting(selecting);

			if (selecting.Cancel)
			{
				return null;
			}

			// get updated values from event args

			GetInputParameters(ref dealerId, ref vin, selecting);

			// validate values from data-source

			if (!IsValid(dealerId, vin)) return new AutoCheckReportTO[0];

			// make request for report

			AutoCheckReportTO report = GetReport(dealerId, vin);

			// return

			if (report == null)
			{
				return new AutoCheckReportTO[0];
			}
			else
			{
				return new AutoCheckReportTO[] { report };
			}
		}

		protected override int ExecuteUpdate(IDictionary keys, IDictionary values, IDictionary oldValues)
		{
            throw new NotImplementedException("AutoCheck should never be updated, if this is an expired report, please check your code.");
		}

		private static ReportDataSourceMethodEventArgs GetMethodEventArgs(int dealerId, string vin)
		{
			return new ReportDataSourceMethodEventArgs(GetInputParameters(dealerId, vin));
		}

		private static OrderedDictionary GetInputParameters(int dealerId, string vin)
		{
			OrderedDictionary parameters = new OrderedDictionary(StringComparer.OrdinalIgnoreCase);
			parameters.Add(AutoCheckReportDataSource.DealerIdParameterName, dealerId);
			parameters.Add(AutoCheckReportDataSource.VinParameterName, vin);
			return parameters;
		}

		private static void GetInputParameters(ref int dealerId, ref string vin, ReportDataSourceMethodEventArgs args)
		{
			foreach (DictionaryEntry entry in args.InputParameters)
			{
				if (string.Equals(entry.Key.ToString(), AutoCheckReportDataSource.DealerIdParameterName, StringComparison.OrdinalIgnoreCase))
				{
					if (entry.Value != null)
					{
						dealerId = Int32Helper.ToInt32(entry.Value);
					}
				}
				else if (string.Equals(entry.Key.ToString(), AutoCheckReportDataSource.VinParameterName, StringComparison.OrdinalIgnoreCase))
				{
					if (entry.Value != null)
					{
						vin = entry.Value.ToString();
					}
				}
			}
		}

		private static bool IsValid(int dealerId, string vin)
		{
			if (dealerId == 0)
			{
				return false;
			}

			if (string.IsNullOrEmpty(vin) || vin.Length != 17)
			{
				return false;
			}

			return true;
		}		

		private static AutoCheckWebService GetService()
		{
			UserIdentity identity = new UserIdentity();

			identity.UserName = Thread.CurrentPrincipal.Identity.Name;

			AutoCheckWebService service = new AutoCheckWebService();

			service.UserIdentityValue = identity;

			return service;
		}

		private AutoCheckReportTO GetReport(int dealerId, string vin)
		{
			AutoCheckWebService service = GetService();

			bool raisedStatusEvent = false;

			AutoCheckReportTO report = null;

			try
			{
				report = service.GetReport(dealerId, vin);
			}
			catch (Exception exception)
			{
				raisedStatusEvent = true;

				ReportDataSourceStatusEventArgs e = new ReportDataSourceStatusEventArgs(
					null, new OrderedDictionary(), exception);

				OnSelected(e);

				if (!e.ExceptionHandled)
				{
					throw;
				}
			}
			finally
			{
				if (!raisedStatusEvent)
				{
					ReportDataSourceStatusEventArgs e = new ReportDataSourceStatusEventArgs(
						null, new OrderedDictionary(), null);

					OnSelected(e);
				}
			}

			return report;
		}

		private AutoCheckReportTO PurchaseReport(int dealerId, string vin, bool isInserting)
		{
			AutoCheckWebService service = GetService();

			bool raisedStatusEvent = false;

			AutoCheckReportTO report;

			try
			{
				report = service.PurchaseReport(dealerId, vin);
			}
			catch (Exception exception)
			{
				raisedStatusEvent = true;

				ReportDataSourceStatusEventArgs e = new ReportDataSourceStatusEventArgs(
					null, new OrderedDictionary(), exception);

				if (isInserting) OnInserted(e); else OnUpdated(e);

				if (!e.ExceptionHandled)
				{
					throw;
				}

				throw;
			}
			finally
			{
				if (!raisedStatusEvent)
				{
					ReportDataSourceStatusEventArgs e = new ReportDataSourceStatusEventArgs(
						null, new OrderedDictionary(), null);

					if (isInserting) OnInserted(e); else OnUpdated(e);
				}
			}

			return report;
		}
	}
}
