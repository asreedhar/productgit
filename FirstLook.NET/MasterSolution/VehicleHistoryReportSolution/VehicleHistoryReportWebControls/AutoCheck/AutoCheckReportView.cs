using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Utilities;

namespace FirstLook.VehicleHistoryReport.WebControls.AutoCheck
{
	[ParseChildren(true), PersistChildren(false)]
	public class AutoCheckReportView : DataBoundControl, INamingContainer, IPostBackEventHandler
	{
		#region Events

		private static readonly object EventClick = new object();

		public event EventHandler<EventArgs> Click
		{
			add
			{
				Events.AddHandler(EventClick, value);
			}
			remove
			{
				Events.RemoveHandler(EventClick, value);
			}
		}

		protected virtual void OnClick(EventArgs e)
		{
			EventHandler<EventArgs> handler = (EventHandler<EventArgs>)Events[EventClick];

			if (handler != null)
			{
				handler(this, e);
			}
		}

		#endregion

		#region Properties

		private string dataItemsField = "Inspections";
		private string dataItemTextField = "Name";
		private string dataItemValueField = "Id";
		private string dataItemSelectedField = "Selected";
		private string dataUserNameField = "UserName";
		private string dataVinField = "Vin";
        private string dataExpirationDateField = "ExpirationDate";
	    private string dataCompareScoreRangeHighField = "CompareScoreRangeHigh";
	    private string dataCompareScoreRangeLowField = "CompareScoreRangeLow";
	    private string dataScoreField = "Score";

		private ListItemCollection items;
	    private int score;	   
	    private int highRangeScore;
	    private int lowRangeScore;
		private string userName;
		private string vin;
        private DateTime expirationDate;

		[Themeable(false), Description("Report Inspection Items Property Name"), DefaultValue(""), Category("Data")]
		public string DataItemsField
		{
			get { return dataItemsField; }
			set { dataItemsField = value; }
		}

		[Themeable(false), Description("Inspection Text Property Name"), DefaultValue(""), Category("Data")]
		public string DataItemTextField
		{
			get { return dataItemTextField; }
			set { dataItemTextField = value; }
		}

		[Themeable(false), Description("Inspection Value Property Name"), DefaultValue(""), Category("Data")]
		public string DataItemValueField
		{
			get { return dataItemValueField; }
			set { dataItemValueField = value; }
		}

		[Themeable(false), Description("Inspection Selected Property Name"), DefaultValue(""), Category("Data")]
		public string DataItemSelectedField
		{
			get { return dataItemSelectedField; }
			set { dataItemSelectedField = value; }
		}

		[Themeable(false), Description("Report UserName Property Name"), DefaultValue(""), Category("Data")]
		public string DataUserNameField
		{
			get { return dataUserNameField; }
			set { dataUserNameField = value; }
		}

        [Themeable(false), Description("Expiration Date Property Name"), DefaultValue(""), Category("Data")]
        public string DataExpirationDateField
        {
            get { return dataExpirationDateField; }
            set { dataExpirationDateField = value; }
        }

		[Themeable(false), Description("Report VIN Property Name"), DefaultValue(""), Category("Data")]
		public string DataVinField
		{
			get { return dataVinField; }
			set { dataVinField = value; }
		}

        [Themeable(false), Description("Report High Range Score"), DefaultValue(""), Category("Data")]
        public string DataCompareScoreRangeHighField
        {
            get { return dataCompareScoreRangeHighField; }
            set { dataCompareScoreRangeHighField = value; }
        }

        [Themeable(false), Description("Report Low Range Score"), DefaultValue(""), Category("Data")]
        public string DataCompareScoreRangeLowField
        {
            get { return dataCompareScoreRangeLowField; }
            set { dataCompareScoreRangeLowField = value; }
        }

        [Themeable(false), Description("Report Score"), DefaultValue(""), Category("Data")]
        public string DataScoreField
        {
            get { return dataScoreField; }
            set { dataScoreField = value; }
        }

		[Themeable(false), Description("Report Inspection Items"), DefaultValue(""), Category("Data")]
		[MergableProperty(false), PersistenceMode(PersistenceMode.InnerProperty)]
		public ListItemCollection Items
		{
			get
			{
				if (items == null)
				{
					items = new ListItemCollection();

					if (IsTrackingViewState)
					{
						((IStateManager)items).TrackViewState();
					}
				}

				return items;
			}
		}

		[Themeable(false), Description("Account UserName"), DefaultValue(""), Category("Data")]
		public string UserName
		{
			get { return userName; }
		}

		[Themeable(false), Description("Report VIN"), DefaultValue(""), Category("Data")]
		public string Vin
		{
			get { return vin; }
		}

        [Themeable(false), Description("Autogenerate Inspection Item Badges"), DefaultValue(false), Category("Data")]
        public bool AutoGenerateInspectionItems
        {
            get
            {
                object value = ViewState["AutoGenerateInspectionItems"];
                if (value == null)
                    return false;
                return (bool)value;
            }
            set
            {
                if (AutoGenerateInspectionItems != value)
                {
                    ViewState["AutoGenerateInspectionItems"] = value;
                }
            }
        }

        [Themeable(false), Description("Display One Owner Icon"), DefaultValue(true), Category("Data")]
        public bool ShowOneOwner
        {
            get
            {
                object value = ViewState["ShowOneOwner"];
                if (value == null)
                    return true;
                return (bool)value;
            }
            set
            {
                if (ShowOneOwner != value)
                {
                    ViewState["ShowOneOwner"] = value; 
                }
            }
        }

        [Themeable(false), Description("Inspection Has One Owner"), DefaultValue(false), Category("Data")]
	    public bool HasOneOwner
	    {
	        get
	        {
                bool returnValue = false;


                foreach (ListItem item in Items)
                {
                    int value = Int32Helper.ToInt32(item.Value);
                    if (value == 1)
                    {
                        returnValue = true;
                        break;
                    }

                }

                return returnValue;
	        }
	    }

        [Themeable(false), Description("Display One Owner Icons"), DefaultValue(true), Category("Data")]
        public bool ShowAutoCheckAssured
        {
            get
            {
                object value = ViewState["ShowAutoCheckAssured"];
                if (value == null)
                    return true;
                return (bool)value;
            }
            set
            {
                if (ShowAutoCheckAssured != value)
                {
                    ViewState["ShowAutoCheckAssured"] = value;
                }
            }
        }

        [Themeable(false), Description("Inspection Shows Buy Back Guarantee"), DefaultValue(true), Category("Data")]
        public bool HasAutoCheckAssured
        {
            get
            {
                bool returnValue = false;


                foreach (ListItem item in Items)
                {
                    int value = Int32Helper.ToInt32(item.Value);
                    if (value == 2)
                    {
                        returnValue = true;
                        break;
                    }

                }

                return returnValue;
            }
        }

        [Themeable(false), Description("Display One Owner Icons"), DefaultValue(false), Category("Data")]
        public bool CombineUnbrandedInspectionItems
        {
            get
            {
                object value = ViewState["CombineUnbrandedInspectionItems"];
                if (value == null)
                    return false;
                return (bool)value;
            }
            set
            {
                if (CombineUnbrandedInspectionItems != value)
                {
                    ViewState["CombineUnbrandedInspectionItems"] = value;
                }
            }
        }

        [Themeable(false), Description("Display One Owner Icons"), DefaultValue(true), Category("Data")]
        public bool ShowAutoCheckScore
        {
            get
            {
                object value = ViewState["ShowAutoCheckScore"];
                if (value == null)
                    return true;
                return (bool)value;
            }
            set
            {
                if (ShowAutoCheckScore != value)
                {
                    ViewState["ShowAutoCheckScore"] = value;
                }
            }
        }

        [Themeable(false), Description("Display One Owner Icons"), DefaultValue(true), Category("Data")]
        public bool ShowCleanTitle
        {
            get
            {
                object value = ViewState["ShowCleanTitle"];
                if (value == null)
                    return true;
                return (bool)value;
            }
            set
            {
                if (ShowCleanTitle != value)
                {
                    ViewState["ShowCleanTitle"] = value;
                }
            }
        }

        [Themeable(false), Description("Inspection Shows Clean Title"), DefaultValue(true), Category("Data")]
        public bool HasCleanTitle
        {
            get
            {
                bool returnValue = false;


                foreach (ListItem item in Items)
                {
                    int value = Int32Helper.ToInt32(item.Value);
                    if (value >= 3 && value <= 8)
                    {
                        returnValue |= true;
                        break;
                    }

                }

                return returnValue;
            }
        }

        public int Score
        {
            get { return score; }
            set { score = value; }
        }

        public int HighRangeScore
        {
            get { return highRangeScore; }
            set { highRangeScore = value; }
        }

        public int LowRangeScore
        {
            get { return lowRangeScore; }
            set { lowRangeScore = value; }
        }

        private AutoCheckReportStatus ReportStatus
        {
            get
            {
                AutoCheckReportStatus status;

                if (expirationDate == DateTime.MinValue)
                {
                    status = AutoCheckReportStatus.Unordered;
                }
                else if (expirationDate > DateTime.Now)
                {
                    status = AutoCheckReportStatus.Valid;
                }
                else
                {
                    status = AutoCheckReportStatus.Expired;
                }

                return status;
            }
        }

	    private string SubmitButtonText
	    {
	        get
	        {
                string text;

                switch (ReportStatus)
                {
                    case AutoCheckReportStatus.Valid:
                    default:
                        text = "View Report";
                        break;
                }

	            return text;
	        }
	    }

        private string ReportStatusIndicatorCssClass
        {
            get
            {
                string cssClass;

                switch (ReportStatus)
                {
                    case AutoCheckReportStatus.Unknown:
                        cssClass = "report-status-unknow";
                        break;
                    case AutoCheckReportStatus.Unordered:
                        cssClass = "report-status-unordered";
                        break;
                    case AutoCheckReportStatus.Valid:
                        cssClass = "report-status-valid";
                        break;
                    case AutoCheckReportStatus.Expired:
                        cssClass = "report-status-expired";
                        break;
                    default:
                        cssClass = "report-status";
                        break;
                }

                return cssClass;
            }
        }
        
        protected override string TagName
		{
			get
			{
				return "div";
			}
		}

		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}

        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            if (string.IsNullOrEmpty(CssClass))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "autocheck_report_view");
            }
            else
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, CssClass + " autocheck_report_view");
            }
            base.AddAttributesToRender(writer);
        }

		#endregion

		#region Data Binding

		protected override void PerformDataBinding(IEnumerable data)
		{
			// clear the values

			Items.Clear();

			userName = string.Empty;

			vin = string.Empty;

            expirationDate = DateTime.MinValue;

			// extract information from data

			if (data != null)
			{
				IEnumerator en = data.GetEnumerator();

				if (en.MoveNext())
				{
					object report = en.Current;

					userName = DataBinder.GetPropertyValue(report, DataUserNameField, null);

					vin = DataBinder.GetPropertyValue(report, DataVinField, null);

                    expirationDate = (DateTime)DataBinder.GetPropertyValue(report, DataExpirationDateField);

				    score = Int32Helper.ToInt32(DataBinder.GetPropertyValue(report, DataScoreField));

                    highRangeScore = Int32Helper.ToInt32(DataBinder.GetPropertyValue(report, DataCompareScoreRangeHighField));

                    lowRangeScore = Int32Helper.ToInt32(DataBinder.GetPropertyValue(report, DataCompareScoreRangeLowField));

					IEnumerable source = (IEnumerable)DataBinder.GetPropertyValue(report, DataItemsField);

					foreach (object value in source)
					{
						ListItem item = new ListItem();

						item.Text = DataBinder.GetPropertyValue(value, DataItemTextField, null);

						item.Value = DataBinder.GetPropertyValue(value, DataItemValueField, null);

						item.Enabled = bool.Parse(DataBinder.GetPropertyValue(value, DataItemSelectedField, null));

						Items.Add(item);
					}
				}
			}
		} 

		#endregion

		#region Render

		protected override void RenderContents(HtmlTextWriter writer)
		{
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "report_status " + ReportStatusIndicatorCssClass);
            writer.AddAttribute(HtmlTextWriterAttribute.For, UniqueID + IdSeparator + "Button");
            writer.RenderBeginTag(HtmlTextWriterTag.Label);
            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "autocheck_button");
			writer.AddAttribute(HtmlTextWriterAttribute.Type, "submit");
			writer.AddAttribute(HtmlTextWriterAttribute.Name, UniqueID);
            writer.AddAttribute(HtmlTextWriterAttribute.Id, UniqueID + IdSeparator + "Button");
            writer.AddAttribute(HtmlTextWriterAttribute.Value, SubmitButtonText);
			writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();

            if (AutoGenerateInspectionItems)
            {
                RenderAutoGeneratedInspectionItems(writer);
            }
            else
            {
                RenderSpecifiedInspectionItems(writer);
            }
		}

        protected void RenderAutoGeneratedInspectionItems(HtmlTextWriter writer)
        {
            if (Items.Count > 0)
            {
                IList<ListItem> enabledItems = new List<ListItem>();

                foreach (ListItem item in Items)
                {
                    if (item.Enabled)
                    {
                        enabledItems.Add(item);
                    }
                }

                if (enabledItems.Count > 0)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Ul);

                    foreach (ListItem item in enabledItems)
                    {
                        RenderInspectionItemBadge(writer, item.Text + ": " + item.Value);
                    }

                    writer.RenderEndTag(); 
                }
            }
        }

        protected void RenderSpecifiedInspectionItems(HtmlTextWriter writer)
        {
            bool hasInspectionItems = ((ShowOneOwner && HasOneOwner) ||
                                (ShowAutoCheckAssured && HasAutoCheckAssured) ||
                                (ShowCleanTitle && HasCleanTitle) ||
                                (ShowAutoCheckScore));

            if (hasInspectionItems)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Ul);
            }
            
            if (ShowAutoCheckAssured && HasAutoCheckAssured) 
            {
                RenderAutoCheckAssured(writer);
            }

            if (ShowAutoCheckScore && ReportStatus != AutoCheckReportStatus.Unordered)
            {
                RenderScore(writer);
            }

            if (CombineUnbrandedInspectionItems)
            {
                RenderCombineUnbrandedInspectionItems(writer);
            }
            else
            {
                if (ShowCleanTitle && HasCleanTitle)
                {
                    RenderCleanTitle(writer);
                }

                if (ShowOneOwner && HasOneOwner)
                {
                    RenderOneOwner(writer);
                }

            }

            if (hasInspectionItems)
            {
                writer.RenderEndTag(); // UL
            }
        }

        protected static void RenderOneOwner(HtmlTextWriter writer)
        {
            RenderInspectionItemBadge(writer, "One Owner", "autocheck-oneowner");
        }

        protected static void RenderAutoCheckAssured(HtmlTextWriter writer)
        {
            RenderInspectionItemBadge(writer, "AutoCheck Assured", "autocheck-assured");
        }

        protected static void RenderCleanTitle(HtmlTextWriter writer)
        {
            RenderInspectionItemBadge(writer, "Clean Title", "autocheck-cleantitle");
        }

        protected void RenderScore(HtmlTextWriter writer)
        {
            RenderInspectionItemBadge(writer, Score.ToString(), "autocheck-score");
        }

        protected void RenderCombineUnbrandedInspectionItems(HtmlTextWriter writer)
        {
            if (HasCleanTitle && HasOneOwner)
            {
                RenderInspectionItemBadge(writer, "Clean Title, One Owner", "autocheck-cleantitle-oneowner");
            }
            else if (!HasCleanTitle && HasOneOwner)
            {
                RenderCleanTitle(writer);
            } 
            else if (HasCleanTitle && !HasOneOwner)
            {
                RenderCleanTitle(writer);
            }
        }

        protected static void RenderInspectionItemBadge(HtmlTextWriter writer, string innerText)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "autocheck-inspection-badge");
            writer.RenderBeginTag(HtmlTextWriterTag.Li);
            writer.Write(innerText);
            writer.RenderEndTag();
        }

        protected static void RenderInspectionItemBadge(HtmlTextWriter writer, string innerText, string cssClass)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "autocheck-inspection-badge " + cssClass);
            writer.AddAttribute(HtmlTextWriterAttribute.Title, innerText);
            writer.RenderBeginTag(HtmlTextWriterTag.Li);
            writer.Write(innerText);
            writer.RenderEndTag();
        }

		#endregion

		#region IStateManager

		protected override void LoadViewState(object savedState)
		{
			Pair pair = (Pair) savedState;

			base.LoadViewState(pair.First);

			if (pair.Second != null)
			{
				object[] values = (object[]) pair.Second;

			    int idx = 0;
                dataItemsField = values[idx++] as string;
                dataItemTextField = values[idx++] as string;
                dataItemValueField = values[idx++] as string;
                dataItemSelectedField = values[idx++] as string;
                dataUserNameField = values[idx++] as string;
                dataVinField = values[idx++] as string;

                userName = values[idx++] as string;
                vin = values[idx++] as string;
                expirationDate = (DateTime)values[idx++];

			    score = Int32Helper.ToInt32(values[idx++]);
			    highRangeScore = Int32Helper.ToInt32(values[idx++]);
                lowRangeScore = Int32Helper.ToInt32(values[idx++]);

                ((IStateManager)Items).LoadViewState(values[idx]);
			}
		}

		protected override object SaveViewState()
		{
			object[] values = new object[13];

		    int idx = 0;
            values[idx++] = dataItemsField;
            values[idx++] = dataItemTextField;
            values[idx++] = dataItemValueField;
            values[idx++] = dataItemSelectedField;
            values[idx++] = dataUserNameField;
            values[idx++] = dataVinField;

            values[idx++] = userName;
            values[idx++] = vin;
		    values[idx++] = expirationDate;

		    values[idx++] = score;
            values[idx++] = highRangeScore;
            values[idx++] = lowRangeScore;

            values[idx] = ((IStateManager)Items).SaveViewState();

			return new Pair(base.SaveViewState(), values);
		}

		#endregion

		#region IPostBackEventHandler Members

		protected void RaisePostBackEvent(string eventArgument)
		{
			OnClick(EventArgs.Empty);
		}

		void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
		{
			RaisePostBackEvent(eventArgument);
		}

		#endregion
	}
}
