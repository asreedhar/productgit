namespace FirstLook.VehicleHistoryReport.WebControls.AutoCheck
{
    public enum AutoCheckReportStatus
    {
        Unknown,
        Unordered,
        Valid,
        Expired
    }
}
