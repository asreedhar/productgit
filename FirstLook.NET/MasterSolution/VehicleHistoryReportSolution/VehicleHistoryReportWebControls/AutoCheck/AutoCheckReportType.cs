using System;
using System.Collections.Generic;
using System.Text;

namespace FirstLook.VehicleHistoryReport.WebControls.AutoCheck
{
	public enum AutoCheckReportType
	{
        Undefined,
        Full,
        Summary,
        Lemon,
        Check,
        Indicators
    }
}
