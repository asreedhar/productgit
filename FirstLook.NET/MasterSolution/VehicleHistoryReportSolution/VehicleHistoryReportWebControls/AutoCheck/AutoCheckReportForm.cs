using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Web.Services.Protocols;
using System.Web.UI;

namespace FirstLook.VehicleHistoryReport.WebControls.AutoCheck
{
	[ParseChildren(true), PersistChildren(false)]
	public class AutoCheckReportForm : ReportForm, IPostBackDataHandler
	{
		#region Properties

		public ReportFormMode CurrentMode
		{
			get
			{
				ReportFormMode currentMode;

                if (ReportStatus == AutoCheckReportStatus.Valid)
                {
                    currentMode = ReportFormMode.ReadOnly;
                }
                else
                {
                    currentMode = ReportFormMode.Insert;
                }

			    return currentMode;
			}
		}

        private AutoCheckReportStatus ReportStatus
        {
            get
            {
                AutoCheckReportStatus status;

                if (expirationDate == DateTime.MinValue)
                {
                    status = AutoCheckReportStatus.Unordered;
                }
                else if (expirationDate > DateTime.Now)
                {
                    status = AutoCheckReportStatus.Valid;
                }
                else
                {
                    status = AutoCheckReportStatus.Expired;
                }

                return status;
            }
        }


		private string dataReportTypeField = "ReportType";
		private string dataUserNameField = "UserName";
		private string dataVinField = "Vin";
        private string dataExpirationDateField = "ExpirationDate";

		[Themeable(false), Description("Report Type Property Name"), DefaultValue(""), Category("Data")]
		public string DataReportTypeField
		{
			get { return dataReportTypeField; }
			set { dataReportTypeField = value; }
		}

		[Themeable(false), Description("UserName Property Name"), DefaultValue(""), Category("Data")]
		public string DataUserNameField
		{
			get { return dataUserNameField; }
			set { dataUserNameField = value; }
		}

		[Themeable(false), Description("VIN Property Name"), DefaultValue(""), Category("Data")]
		public string DataVinField
		{
			get { return dataVinField; }
			set { dataVinField = value; }
		}

        [Themeable(false), Description("Expiration Date Property Name"), DefaultValue(""), Category("Data")]
        public string DataExpirationDateField
        {
            get { return dataExpirationDateField; }
            set { dataExpirationDateField = value; }
        }

        private string oldUserName;
        private string oldVin;
        private DateTime oldExpirationDate;
        
		private string userName;
		private string vin;
        private DateTime expirationDate;
        
        [Themeable(false), Description("Account UserName"), DefaultValue(""), Category("Data")]
		public string UserName
		{
			get { return userName; }
            protected set { userName = value; }
		}

		[Themeable(false), Description("Report VIN"), DefaultValue(""), Category("Data")]
		public string Vin
		{
			get { return vin; }
            protected set { vin = value; }
		}

		private bool hasPurchaseAuthority;

		[Themeable(false), Description("If user has purchase-authority"), DefaultValue(""), Category("Behavior")]
		public bool HasPurchaseAuthority
		{
			get { return hasPurchaseAuthority; }
			set { hasPurchaseAuthority = value; }
		}
	    
	    private string reportUrl = string.Empty;

        [Themeable(true), Description("Report Url"), DefaultValue(""), Category("Behavior")]
        public string ReportUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(reportUrl))
                {
                    return reportUrl; 
                }
                else
                {
                    throw new NullReferenceException("ReportUrl Must be configured for the AutoCheckReportForm");
                }
            }
            set
            {
                reportUrl = value;
            }
        }
        
        private string errorMessage = string.Empty;

	    private bool hasError
	    {
	        get
	        {
	            return !string.IsNullOrEmpty(errorMessage);
	        }
	    }

		public string ErrorMessage
		{
			get { return errorMessage; }
		}      

		protected override string TagName
		{
			get
			{
				return "div";
			}
		}

		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}

	    protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            if (string.IsNullOrEmpty(CssClass))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, " vhr_report_form vhr_report_form_" + CurrentMode);
            }
            else
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, CssClass + " vhr_report_form vhr_report_form_" + CurrentMode);
            }

            base.AddAttributesToRender(writer);
        }

        #endregion

		#region Methods

		protected override Control FindControl(string id, int pathOffset)
		{
			return this;
		}

		#endregion

		#region Data Binding
		
		protected override void PerformDataBinding(IEnumerable data)
		{
			// clear the values

            UserName = oldUserName = string.Empty;

            Vin = oldVin = string.Empty;

		    expirationDate = oldExpirationDate = DateTime.MinValue;

			// extract information from data

			if (data != null)
			{
				IEnumerator en = data.GetEnumerator();

				if (en.MoveNext())
				{
					object report = en.Current;

                    UserName = oldUserName = DataBinder.GetPropertyValue(report, DataUserNameField, null);

                    Vin = oldVin = DataBinder.GetPropertyValue(report, DataVinField, null);

                    expirationDate = oldExpirationDate = Convert.ToDateTime(DataBinder.GetPropertyValue(report, DataExpirationDateField));
				}
			}
		}
		#endregion

		#region Event Handling

		protected void HandleEvent(string commandName)
		{           
			if (string.Equals(commandName, "Insert"))
			{
			    CancelEventArgs e = new CancelEventArgs();

                OnInserting(e);

                if (!e.Cancel)
                {
                    IDictionary values = new Hashtable();

                    GetData().Insert(values, HandleInsertCallback); 
                }
			} 
            else if (string.Equals(commandName, "Update"))
			{

                CancelEventArgs e = new CancelEventArgs();

                OnUpdating(e);

                if (!e.Cancel)
                {
                    IDictionary keys = new Hashtable();
                    IDictionary newValues = new Hashtable();
                    IDictionary oldValues = new Hashtable();

                    GetData().Update(keys, newValues, oldValues, HandleUpdateCallback); 
                }
			}
		}

		protected bool HandleInsertCallback(int affectedRecords, Exception ex)
		{
			if (ex != null)
			{
				HandleCallbackException(ex);
			}
			else
			{
                OnInserted(new ReportFormInsertedEventArgs(affectedRecords, ex));
				errorMessage = string.Empty;

                DataBind();
			}

            return true;
		}

		protected bool HandleUpdateCallback(int affectedRecords, Exception ex)
		{
			if (ex != null)
			{
				HandleCallbackException(ex);
			}
			else
			{
                OnUpdated(new ReportFormUpdatedEventArgs(affectedRecords, ex));
				errorMessage = string.Empty;

                DataBind();
			}

			return true;
		}

		private void HandleCallbackException(Exception ex)
		{
			SoapException ce = ex as SoapException;

			if (ce != null)
			{
				if (ce.Code == SoapException.ServerFaultCode)
				{
                    string message = ce.Detail.InnerText;

                    int code;

                    if (int.TryParse(message, out code))
                    {
                        HandleCallbackException(code);

                        return;
                    }
                    else
                    {
                        HandleCallbackException(906);
                    }
				}
			}
			else if (!string.IsNullOrEmpty(ex.Message))
			{
				string message = ex.Message;

				int code;

				if (int.TryParse(message, out code))
				{
					HandleCallbackException(code);

					return;
				}
			}
			else
			{
				HandleCallbackException(906);
			}
		}

		private void HandleCallbackException(int code)
		{
			switch (code)
			{			
				case 403:
					errorMessage = "Account Status. Contact AutoCheck to verify account status";
					break;
                case 500:
                    errorMessage = "Transaction Error";
                    break;
				default:
					errorMessage = "Unknown Error";
					break;
			}
		}

		#endregion

		#region Life Cycle

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			Page.RegisterRequiresPostBack(this); // because of the damn checkbox :P
		}

		#endregion

		#region Render

		protected override void RenderContents(HtmlTextWriter writer)
		{
			if (CurrentMode == ReportFormMode.ReadOnly)
			{
				RenderReadOnlyMode(writer);
			}
			else if (HasPurchaseAuthority)
			{
				if (CurrentMode == ReportFormMode.Edit)
				{
					RenderEditMode(writer);
				}
				else if (CurrentMode == ReportFormMode.Insert)
				{
					RenderInsertMode(writer);
				}
			}
			else
			{
				if (CurrentMode == ReportFormMode.Edit)
				{
					RenderReadOnlyMode(writer);
				}
				else
				{
					RenderCannotPurchaseMode(writer);
				}
			}
		}

		private static void RenderCannotPurchaseMode(HtmlTextWriter writer)
		{
			writer.RenderBeginTag(HtmlTextWriterTag.P);
			writer.Write("You have no purchase rights! Sorry!");
			writer.RenderEndTag();
		}

		private void RenderReadOnlyMode(HtmlTextWriter writer)
		{
			RenderAutoCheckLink(writer);
		}

        private void RenderEditMode(HtmlTextWriter writer)
        {
            RenderAutoCheckLink(writer);

            if (hasError)
            {
                RenderErrorMessage(writer);
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "controls");
            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);


            writer.RenderEndTag();
        }

	    private void RenderInsertMode(HtmlTextWriter writer)
		{
            if (hasError)
            {
                RenderErrorMessage(writer);
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "controls");
            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

			RenderPurchaseButton(writer);

            writer.RenderEndTag();
		}

	    private void RenderErrorMessage(HtmlTextWriter writer)
	    {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "error");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

	        writer.AddAttribute(HtmlTextWriterAttribute.Class, "error_msg");
            writer.RenderBeginTag(HtmlTextWriterTag.P);
            writer.Write(errorMessage);
            writer.RenderEndTag();

            writer.RenderEndTag();
	    }

        private void RenderPurchaseButton(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "button");
            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "submit");
            writer.AddAttribute(HtmlTextWriterAttribute.Name, UniqueID + IdSeparator + "Purchase");

            string value = hasError
                               ? "Try Again"
                               : (ReportStatus == AutoCheckReportStatus.Unknown) ? "Reorder AutoCheck Report" : "Order AutoCheck Report";

            writer.AddAttribute(HtmlTextWriterAttribute.Value, value);
            
            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();

            writer.RenderEndTag();
        }
        
		private void RenderAutoCheckLink(HtmlTextWriter writer)
		{		
			string text = string.Format("View AutoCheck Report");

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "vhr_report_link");
            writer.RenderBeginTag(HtmlTextWriterTag.P);

            writer.AddAttribute(HtmlTextWriterAttribute.Target, "AutoCheckReport");
			writer.AddAttribute(HtmlTextWriterAttribute.Href, ReportUrl);
			writer.RenderBeginTag(HtmlTextWriterTag.A);
			writer.Write(text);
			writer.RenderEndTag();

            writer.RenderEndTag();
		}

		#endregion

		#region IPostBackDataHandler Members

		private string _commandName;

	    protected bool LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
			if (IsEnabled)
			{
				int controlNameOffset = UniqueID.Length + 1;

				if (postDataKey.Length > controlNameOffset)
				{
					string controlName = postDataKey.Substring(controlNameOffset);

                    if (string.Equals(controlName, "Purchase"))
					{
						_commandName = "Insert";

						return true;
					}
				}
			}

			return false;
		}

		protected void RaisePostDataChangedEvent()
		{
			HandleEvent(_commandName);

		    _commandName = string.Empty;
		}

		bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
			return LoadPostData(postDataKey, postCollection);
		}

		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{
			RaisePostDataChangedEvent();
		}

		#endregion

        #region IStateManager Members

        protected override void LoadViewState(object state)
        {
            Pair pair = (Pair)state;

            base.LoadViewState(pair.First);

            if (pair.Second != null)
            {
                object[] values = (object[])pair.Second;

                int idx = 0;

                reportUrl = values[idx++] as string;
                userName = values[idx++] as string;
                vin = values[idx++] as string;
                expirationDate = (DateTime) values[idx++];

                oldUserName = values[idx++] as string;
                oldVin = values[idx++] as string;
                oldExpirationDate = (DateTime) values[idx++];

            	hasPurchaseAuthority = (bool) values[idx];
            }
        }

        protected override object SaveViewState()
        {
            object[] values = new object[8];

            int idx = 0;

            values[idx++] = reportUrl;
            values[idx++] = userName;
            values[idx++] = vin;
            values[idx++] = expirationDate;

            values[idx++] = oldUserName;
            values[idx++] = oldVin;
            values[idx++] = oldExpirationDate;

			values[idx] = hasPurchaseAuthority;

            return new Pair(base.SaveViewState(), values);
        }

        #endregion
    }
}
