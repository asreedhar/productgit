using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Web.UI;
using System.Web.UI.Design.WebControls;
using System.Web.UI.WebControls;

namespace FirstLook.VehicleHistoryReport.WebControls
{
	[ParseChildren(true), PersistChildren(false)]
	public abstract class ReportDataSource : DataSourceControl
	{
		#region Events

		[Category("Data"), Description("ObjectDataSource_Selected")]
		public event ReportDataSourceStatusEventHandler Selected
		{
			add
			{
				DataView.Selected += value;
			}
			remove
			{
				DataView.Selected -= value;
			}
		}

		[Category("Data"), Description("ObjectDataSource_Selecting")]
		public event ReportDataSourceSelectingEventHandler Selecting
		{
			add
			{
				DataView.Selecting += value;
			}
			remove
			{
				DataView.Selecting -= value;
			}
		}

		[Category("Data"), Description("ObjectDataSource_Updating")]
		public event ReportDataSourceMethodEventHandler Updating
		{
			add
			{
				DataView.Updating += value;
			}
			remove
			{
				DataView.Updating -= value;
			}
		}

		[Category("Data"), Description("ObjectDataSource_Updated")]
		public event ReportDataSourceStatusEventHandler Updated
		{
			add
			{
				DataView.Updated += value;
			}
			remove
			{
				DataView.Updated -= value;
			}
		}

		[Category("Data"), Description("ObjectDataSource_Inserting")]
		public event ReportDataSourceMethodEventHandler Inserting
		{
			add
			{
				DataView.Inserting += value;
			}
			remove
			{
				DataView.Inserting -= value;
			}
		}

		[Category("Data"), Description("ObjectDataSource_Inserted")]
		public event ReportDataSourceStatusEventHandler Inserted
		{
			add
			{
				DataView.Inserted += value;
			}
			remove
			{
				DataView.Inserted -= value;
			}
		}

		#endregion

		protected abstract ReportDataView DataView { get; }

		private ParameterCollection parameters;

		protected bool HasParameters
		{
			get { return parameters != null; }
		}

		[
		DefaultValue(null),
		Editor(typeof(ParameterCollectionEditor), typeof(UITypeEditor)),
		MergableProperty(false),
		PersistenceMode(PersistenceMode.InnerProperty),
		Category("Data")
		]
		public ParameterCollection Parameters
		{
			get
			{
				if (parameters == null)
				{
					parameters = new ParameterCollection();

					parameters.ParametersChanged += this.OnParametersChanged;

					if (IsTrackingViewState)
					{
						((IStateManager)parameters).TrackViewState();
					}
				}
				return parameters;
			}
		}


		protected override void LoadViewState(object savedState)
		{
			object baseState = null;

			if (savedState != null)
			{
				Pair p = (Pair)savedState;

				baseState = p.First;

				if (p.Second != null)
				{
					((IStateManager)Parameters).LoadViewState(p.Second);
				}
			}

			base.LoadViewState(baseState);
		}

		protected override void OnInit(EventArgs e)
		{
			Page.LoadComplete += this.OnPageLoadComplete;
		}

		private void OnPageLoadComplete(object sender, EventArgs e)
		{
			Parameters.UpdateValues(Context, this);
		}

		private void OnParametersChanged(object sender, EventArgs e)
		{
			DataView.RaiseChangedEvent();
		}

		protected override object SaveViewState()
		{
			object baseState = base.SaveViewState();

			object parameterState = null;

			if (parameters != null)
			{
				parameterState = ((IStateManager)parameters).SaveViewState();
			}

			if ((baseState != null) || (parameterState != null))
			{
				return new Pair(baseState, parameterState);
			}

			return null;
		}

		protected override void TrackViewState()
		{
			base.TrackViewState();

			if (parameters != null)
			{
				((IStateManager)parameters).TrackViewState();
			}
		}
	}
}
