using System.Collections.Specialized;
using System.ComponentModel;

namespace FirstLook.VehicleHistoryReport.WebControls
{
	public class ReportDataSourceMethodEventArgs : CancelEventArgs
	{
		private readonly IOrderedDictionary _inputParameters;

		public ReportDataSourceMethodEventArgs(IOrderedDictionary inputParameters)
		{
			_inputParameters = inputParameters;
		}

		public IOrderedDictionary InputParameters
		{
			get
			{
				return _inputParameters;
			}
		}

	}
}
