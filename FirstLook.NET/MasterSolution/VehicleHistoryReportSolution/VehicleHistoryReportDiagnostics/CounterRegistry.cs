using System.Collections.Generic;
using System.Diagnostics;

namespace FirstLook.VehicleHistoryReport.DomainModel.Diagnostics
{
	public class CounterRegistry
	{
		private const string ExternalWebServiceAutoCheckXml = "VHR$External:WebService:AutoCheck:Xml";
		private const string ExternalWebServiceAutoCheckHtml = "VHR$External:WebService:AutoCheck:Html";
		private const string ExternalWebServiceCarfaxXml = "VHR$External:WebService:Carfax:Xml";
		private const string InternalWebServiceAutoCheckCanPurchaseReport = "VHR$Internal:WebService:AutoCheck:CanPurchaseReport";
		private const string InternalWebServiceCarfaxCanPurchaseReport = "VHR$Internal:WebService:Carfax:CanPurchaseReport";
		private const string InternalWebServiceAutoCheckGetReport = "VHR$Internal:WebService:AutoCheck:GetReport";
		private const string InternalWebServiceCarfaxGetReport = "VHR$Internal:WebService:Carfax:GetReport";
		private const string InternalWebServiceCarfaxGetReportPreference = "VHR$Internal:WebService:Carfax:GetReportPreference";
		private const string InternalWebServiceAutoCheckHasAccount = "VHR$Internal:WebService:AutoCheck:HasAccount";
		private const string InternalWebServiceCarfaxHasAccount = "VHR$Internal:WebService:Carfax:HasAccount";
		private const string InternalWebServiceAutoCheckPurchaseReport = "VHR$Internal:WebService:AutoCheck:PurchaseReport";
		private const string InternalWebServiceCarfaxPurchaseReport = "VHR$Internal:WebService:Carfax:PurchaseReport";
		private const string InternalProcessorCarfax = "VHR$Internal:Processor:Carfax";
		private const string InternalProcessorCarfaxPurchase = "VHR$Internal:Processor:Carfax:Purchase";
		private const string InternalProcessorCarfaxRenew = "VHR$Internal:Processor:Carfax:Renew";
		private const string InternalProcessorCarfaxUpgrade = "VHR$Internal:Processor:Carfax:Upgrade";
		private const string InternalProcessorCarfaxStatusChange = "VHR$Internal:Processor:Carfax:StatusChange";

		internal static readonly string[] PerformanceObjectNames = new string[]
			{
				ExternalWebServiceAutoCheckXml,
				ExternalWebServiceAutoCheckHtml,
				ExternalWebServiceCarfaxXml,
				InternalWebServiceAutoCheckCanPurchaseReport,
				InternalWebServiceCarfaxCanPurchaseReport,
				InternalWebServiceAutoCheckGetReport,
				InternalWebServiceCarfaxGetReport,
				InternalWebServiceCarfaxGetReportPreference,
				InternalWebServiceAutoCheckHasAccount,
				InternalWebServiceCarfaxHasAccount,
				InternalWebServiceAutoCheckPurchaseReport,
				InternalWebServiceCarfaxPurchaseReport,
				InternalProcessorCarfax,
				InternalProcessorCarfaxPurchase,
				InternalProcessorCarfaxRenew,
				InternalProcessorCarfaxUpgrade,
				InternalProcessorCarfaxStatusChange
			};

		internal const string OperationPerSecondCounterName = "Operations/Sec";

		internal const string AverageOperationTimeCounterName = "Avg Operation Time";
		internal const string AverageOperationBaseCounterName = "Avg Operation Time Base";

		internal const string OperationSuccessFractionNumerCounterName = "Operation Success Ratio (%)";
		internal const string OperationSuccessFractionDenomCounterName = "Operation Success Ratio Base";

		private static readonly CounterRegistry instance = new CounterRegistry();

		public static ICounter GetCounter(
			InstrumentedResponsibility responsibility,
			InstrumentedSystem system,
			InstrumentedProvider provider,
			InstrumentedOperation operation)
		{
			string counterName = string.Empty;

			if (responsibility == InstrumentedResponsibility.External)
			{
				if (system == InstrumentedSystem.WebService)
				{
					if (provider == InstrumentedProvider.AutoCheck)
					{
						if (operation == InstrumentedOperation.ExternalXml)
						{
							counterName = ExternalWebServiceAutoCheckXml;
						}
						else if (operation == InstrumentedOperation.ExternalHtml)
						{
							counterName = ExternalWebServiceAutoCheckHtml;
						}
					}
					else if (provider == InstrumentedProvider.Carfax)
					{
						if (operation == InstrumentedOperation.ExternalXml)
						{
							counterName = ExternalWebServiceCarfaxXml;
						}
					}
				}
			}
			else if (responsibility == InstrumentedResponsibility.Internal)
			{
				if (system == InstrumentedSystem.Processor)
				{
					if (provider == InstrumentedProvider.Carfax)
					{
						if (operation == InstrumentedOperation.ProcessorCommand)
						{
							counterName = InternalProcessorCarfax;
						}
						else if (operation == InstrumentedOperation.ProcessorReportPurchase)
						{
							counterName = InternalProcessorCarfaxPurchase;
						}
						else if (operation == InstrumentedOperation.ProcessorReportRenew)
						{
							counterName = InternalProcessorCarfaxRenew;
						}
						else if (operation == InstrumentedOperation.ProcessorReportUpgrade)
						{
							counterName = InternalProcessorCarfaxUpgrade;
						}
						else if (operation == InstrumentedOperation.ProcessorReportStatusChange)
						{
							counterName = InternalProcessorCarfaxStatusChange;
						}
					}
				}
				else if (system == InstrumentedSystem.WebService)
				{
					if (operation == InstrumentedOperation.WebServiceCanPurchaseReport)
					{
						if (provider == InstrumentedProvider.AutoCheck)
						{
							counterName = InternalWebServiceAutoCheckCanPurchaseReport;
						}
						else
						{
							counterName = InternalWebServiceCarfaxCanPurchaseReport;
						}
					}
					else if (operation == InstrumentedOperation.WebServiceGetReport)
					{
						if (provider == InstrumentedProvider.AutoCheck)
						{
							counterName = InternalWebServiceAutoCheckGetReport;
						}
						else
						{
							counterName = InternalWebServiceCarfaxGetReport;
						}
					}
					else if (operation == InstrumentedOperation.WebServiceGetReportPreference)
					{
						if (provider == InstrumentedProvider.Carfax)
						{
							counterName = InternalWebServiceCarfaxGetReportPreference;
						}
					}
					else if (operation == InstrumentedOperation.WebServiceHasAccount)
					{
						if (provider == InstrumentedProvider.AutoCheck)
						{
							counterName = InternalWebServiceAutoCheckHasAccount;
						}
						else
						{
							counterName = InternalWebServiceCarfaxHasAccount;
						}
					}
					else if (operation == InstrumentedOperation.WebServicePurchaseReport)
					{
						if (provider == InstrumentedProvider.AutoCheck)
						{
							counterName = InternalWebServiceAutoCheckPurchaseReport;
						}
						else
						{
							counterName = InternalWebServiceCarfaxPurchaseReport;
						}
					}
				}
			}
			
			return instance.GetCounter(counterName);
		}

		private readonly IDictionary<string, PerformanceCounter[]> counters;

		private CounterRegistry()
		{
			counters = new Dictionary<string, PerformanceCounter[]>();

			foreach (string performanceObjectName in PerformanceObjectNames)
			{
				if (PerformanceCounterCategory.Exists(performanceObjectName))
				{
					PerformanceCounter ratePerSecond = new PerformanceCounter();
					ratePerSecond.CategoryName = performanceObjectName;
					ratePerSecond.CounterName = OperationPerSecondCounterName;
					ratePerSecond.MachineName = ".";
					ratePerSecond.ReadOnly = false;

					PerformanceCounter avgDurationNumer = new PerformanceCounter();
					avgDurationNumer.CategoryName = performanceObjectName;
					avgDurationNumer.CounterName = AverageOperationTimeCounterName;
					avgDurationNumer.MachineName = ".";
					avgDurationNumer.ReadOnly = false;

					PerformanceCounter avgDurationDenom = new PerformanceCounter();
					avgDurationDenom.CategoryName = performanceObjectName;
					avgDurationDenom.CounterName = AverageOperationBaseCounterName;
					avgDurationDenom.MachineName = ".";
					avgDurationDenom.ReadOnly = false;

					PerformanceCounter operationSuccessNumer = new PerformanceCounter();
					operationSuccessNumer.CategoryName = performanceObjectName;
					operationSuccessNumer.CounterName = OperationSuccessFractionNumerCounterName;
					operationSuccessNumer.MachineName = ".";
					operationSuccessNumer.ReadOnly = false;

					PerformanceCounter operationSuccessDenom = new PerformanceCounter();
					operationSuccessDenom.CategoryName = performanceObjectName;
					operationSuccessDenom.CounterName = OperationSuccessFractionDenomCounterName;
					operationSuccessDenom.MachineName = ".";
					operationSuccessDenom.ReadOnly = false;

					counters.Add(
						performanceObjectName,
						new PerformanceCounter[]
							{
								ratePerSecond,
								avgDurationNumer, avgDurationDenom,
								operationSuccessNumer, operationSuccessDenom
							});
				}
			}
		}

		public ICounter GetCounter(string counterName)
		{
			if (counters.Keys.Contains(counterName))
			{
				PerformanceCounter[] values = counters[counterName];

				return new Counter(values[0], values[1], values[2], values[3], values[4]);
			}

			return new NullCounter();
		}
	}
}
