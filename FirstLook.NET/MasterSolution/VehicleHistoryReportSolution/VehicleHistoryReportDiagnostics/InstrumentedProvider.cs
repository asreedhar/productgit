namespace FirstLook.VehicleHistoryReport.DomainModel.Diagnostics
{
	public enum InstrumentedProvider
	{
		Undefined,
		AutoCheck,
		Carfax
	}
}
