namespace FirstLook.VehicleHistoryReport.DomainModel.Diagnostics
{
	public enum InstrumentedSystem
	{
		Undefined,
		Processor,
		WebService
	}
}
