namespace FirstLook.VehicleHistoryReport.DomainModel.Diagnostics
{
	public enum InstrumentedOperation
	{
		Undefined,
		ExternalXml,
		ExternalHtml,
		ProcessorCommand,
		ProcessorReportPurchase,
		ProcessorReportRenew,
		ProcessorReportUpgrade,
		ProcessorReportStatusChange,
		WebServiceCanPurchaseReport,
		WebServiceHasAccount,
		WebServiceGetReport,
		WebServiceGetReportPreference,
		WebServicePurchaseReport
	}
}
