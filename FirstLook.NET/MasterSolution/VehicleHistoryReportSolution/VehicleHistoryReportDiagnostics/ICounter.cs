using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Diagnostics
{
	public interface ICounter : IDisposable
	{
		void Success();
	}
}
