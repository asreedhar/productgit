using System;
using System.Diagnostics;

namespace FirstLook.VehicleHistoryReport.DomainModel.Diagnostics
{
	public class Counter : ICounter
	{
		private readonly PerformanceCounter rate, avgDurationNumer, avgDurationDenom, operationSuccessNumer, operationSuccessDenom;
		private readonly long startTime;
		private long endTime;
		private bool success;

		public Counter(PerformanceCounter rate, PerformanceCounter avgDurationNumer, PerformanceCounter avgDurationDenom, PerformanceCounter operationSuccessNumer, PerformanceCounter operationSuccessDenom)
		{
			this.rate = rate;
			this.avgDurationNumer = avgDurationNumer;
			this.avgDurationDenom = avgDurationDenom;
			this.operationSuccessNumer = operationSuccessNumer;
			this.operationSuccessDenom = operationSuccessDenom;
			NativeMethods.QueryPerformanceCounter(ref startTime);
		}

        ~Counter()
        {
            Dispose(false);
        }

		private void Dispose()
		{
			try
			{
				Dispose(true);
			}
			finally
			{
				GC.SuppressFinalize(this);
			}
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				NativeMethods.QueryPerformanceCounter(ref endTime);
				rate.Increment();
				avgDurationNumer.IncrementBy(endTime - startTime);
				avgDurationDenom.Increment();
				if (success) operationSuccessNumer.Increment();
				operationSuccessDenom.Increment();
			}
		}

		#region ICounter Members

		void ICounter.Success()
		{
			success = true;
		}

		#endregion

		#region IDisposable Members

		void IDisposable.Dispose()
		{
			Dispose();
		}

		#endregion
	}
}
