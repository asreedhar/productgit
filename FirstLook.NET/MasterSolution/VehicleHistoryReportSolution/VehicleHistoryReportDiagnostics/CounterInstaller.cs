using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;

namespace FirstLook.VehicleHistoryReport.DomainModel.Diagnostics
{
	/// <code>installutil "C:\full\path\to\FirstLook.VehicleHistoryReport.DomainModel.dll"</code>.
    /// <see cref="http://msdn2.microsoft.com/en-us/library/50614e95(VS.80).aspx"/>
	[RunInstaller(true)]
	public class CounterInstaller : PerformanceCounterInstaller
	{
		public CounterInstaller()
		{
			foreach (string counterName in CounterRegistry.PerformanceObjectNames)
			{
				InstallPerformanceCounterCategory(counterName);
			}
		}

		private static void InstallPerformanceCounterCategory(string categoryName)
		{
			if (!PerformanceCounterCategory.Exists(categoryName))
			{
				CounterCreationDataCollection counters = new CounterCreationDataCollection();

				// Rate

				CounterCreationData ratePerSecond = new CounterCreationData();
				ratePerSecond.CounterName = CounterRegistry.OperationPerSecondCounterName;
				ratePerSecond.CounterType = PerformanceCounterType.RateOfCountsPerSecond32;
				counters.Add(ratePerSecond);

				// Average Time per Operation

				CounterCreationData avgDurationNumer = new CounterCreationData();
				avgDurationNumer.CounterName = CounterRegistry.AverageOperationTimeCounterName;
				avgDurationNumer.CounterType = PerformanceCounterType.AverageTimer32;
				counters.Add(avgDurationNumer);

				// Base Counter for Counting Average Time

				CounterCreationData avgDurationDenom = new CounterCreationData();
				avgDurationDenom.CounterName = CounterRegistry.AverageOperationBaseCounterName;
				avgDurationDenom.CounterType = PerformanceCounterType.AverageBase;
				counters.Add(avgDurationDenom);

				// Operation "Success" Fraction

				CounterCreationData successNumer = new CounterCreationData();
				successNumer.CounterName = CounterRegistry.OperationSuccessFractionNumerCounterName;
				successNumer.CounterType = PerformanceCounterType.SampleFraction;
				counters.Add(successNumer);

				// Operation "Success" Fraction Base

				CounterCreationData successDenom = new CounterCreationData();
				successDenom.CounterName = CounterRegistry.OperationSuccessFractionDenomCounterName;
				successDenom.CounterType = PerformanceCounterType.SampleBase;
				counters.Add(successDenom);

				// Create Category

				PerformanceCounterCategory.Create(categoryName, string.Empty, PerformanceCounterCategoryType.SingleInstance, counters);
			}
		}
	}
}
