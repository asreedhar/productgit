namespace FirstLook.VehicleHistoryReport.DomainModel.Diagnostics
{
	public enum InstrumentedResponsibility
	{
		Undefined,
		Internal,
		External
	}
}
