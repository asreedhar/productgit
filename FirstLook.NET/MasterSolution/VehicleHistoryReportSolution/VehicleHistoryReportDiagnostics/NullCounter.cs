using System;

namespace FirstLook.VehicleHistoryReport.DomainModel.Diagnostics
{
	internal class NullCounter : ICounter
	{
		#region ICounter Members

		void ICounter.Success()
		{
			// no-operation
		}

		#endregion

		#region IDisposable Members

		void IDisposable.Dispose()
		{
			// no-operation
		}

		#endregion
	}
}
