﻿using System;
using System.Web;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Fault.DomainModel.Utilities;
using FirstLook.VehicleHistoryReport.DomainModel;

namespace FirstLook.VehicleHistoryReport.WebService
{
    public class Global : HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            IRegistry registry = RegistryFactory.GetRegistry();

            registry.Register<Module>();

            registry.Register<ICommandExecutor, ListeningCommandExecutor>(ImplementationScope.Isolated);
        }

        protected void Application_End(object sender, EventArgs e)
        {
            RegistryFactory.GetRegistry().Dispose();
        }

        private class ListeningCommandExecutor : SimpleCommandExecutor
        {

            public ListeningCommandExecutor()
            {
                Add(new FaultWebExceptionObserver());
            }

        }
    }
}