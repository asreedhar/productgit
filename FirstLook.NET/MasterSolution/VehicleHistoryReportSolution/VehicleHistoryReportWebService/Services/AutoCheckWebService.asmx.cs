using System;
using System.ComponentModel;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleHistoryReport.DomainModel;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleHistoryReport.DomainModel.Diagnostics;
using ICommandFactory=FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.ICommandFactory;

namespace FirstLook.VehicleHistoryReport.WebService.Services
{
    /// <summary>
    /// Summary description for AutoCheckWebService
    /// </summary>
    [WebService(Namespace = "http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class AutoCheckWebService : System.Web.Services.WebService
    {
        private UserIdentity _userIdentity;

        public UserIdentity UserIdentity
        {
            get { return _userIdentity; }
            set { _userIdentity = value; }
        }

        private string UserName
        {
            get { return UserIdentity == null ? string.Empty : UserIdentity.UserName; }
        }

        #region Report Preference

		[WebMethod(Description = "Whether the dealer has a AutoCheck account")]
		[SoapHeader("UserIdentity", Direction = SoapHeaderDirection.In)]
		public bool HasAccount(int dealerId)
		{
//			using (ICounter counter = CounterRegistry.GetCounter(InstrumentedResponsibility.Internal, InstrumentedSystem.WebService, InstrumentedProvider.AutoCheck, InstrumentedOperation.WebServiceHasAccount))
//			{
				bool hasAccount = false;

				try
				{
                    ICommand<AccountQueryResultsDto, AccountQueryArgumentsDto> command = Resolve<ICommandFactory>().CreateAccountQueryCommand();

                    AccountQueryArgumentsDto accountQueryArgumentsDto = new AccountQueryArgumentsDto
                    {
                        Client = new ClientDto
                        {
                            ClientType = ClientTypeDto.Dealer,
                            Id = dealerId
                        },
                        UserName = UserName
                    };

                    AccountQueryResultsDto results = ExecuteCommand(command, accountQueryArgumentsDto);

                    hasAccount = results.HasAccount;

//					counter.Success();
				}
				catch (Exception ex)
				{
					HandleException(ex, "Failed to determine if user can purchase report");
				}

				return hasAccount;
//			}
		}

        [WebMethod(Description = "Whether the member can purchase a report at the dealer")]
        [SoapHeader("UserIdentity", Direction = SoapHeaderDirection.In)]
        public bool CanPurchaseReport(int dealerId)
        {
//			using (ICounter counter = CounterRegistry.GetCounter(InstrumentedResponsibility.Internal, InstrumentedSystem.WebService, InstrumentedProvider.AutoCheck, InstrumentedOperation.WebServiceCanPurchaseReport))
//			{
				bool canPurchaseReport = false;

				try
				{
                    ICommand<PurchaseAuthorityResultsDto, PurchaseAuthorityArgumentsDto> command = Resolve<ICommandFactory>().CreatePurchaseAuthorityCommand();

                    PurchaseAuthorityArgumentsDto purchaseAuthorityArgumentsDto = new PurchaseAuthorityArgumentsDto
                    {
                        Client = new ClientDto
                        {
                            ClientType =
                                ClientTypeDto.Dealer,
                            Id = dealerId
                        },
                        UserName = UserName
                    };

                    PurchaseAuthorityResultsDto results = ExecuteCommand(command, purchaseAuthorityArgumentsDto);

                    canPurchaseReport = results.Authority;

//					counter.Success();
				}
				catch (Exception ex)
				{
					HandleException(ex, "Failed to determine if user can purchase report");
				}

				return canPurchaseReport;
//			}
        }

        #endregion

        #region Report

        [WebMethod(Description = "Return a existing report")]
        [SoapHeader("UserIdentity", Direction = SoapHeaderDirection.In)]
        public AutoCheckReportTO GetReport(int dealerId, string vin)
        {
//			using (ICounter counter = CounterRegistry.GetCounter(InstrumentedResponsibility.Internal, InstrumentedSystem.WebService, InstrumentedProvider.AutoCheck, InstrumentedOperation.WebServiceGetReport))
//			{
				AutoCheckReportTO report = null;

				try
				{
                    ICommand<ReportQueryResultsDto, ReportQueryArgumentsDto> command = Resolve<ICommandFactory>().CreateReportQueryCommand();

                    ReportQueryArgumentsDto reportQueryArgumentsDto = new ReportQueryArgumentsDto
                    {
                        Client = new ClientDto
                        {
                            ClientType = ClientTypeDto.Dealer,
                            Id = dealerId
                        },
                        UserName = UserName,
                        Vin = vin
                    };

                    ReportQueryResultsDto results = ExecuteCommand(command, reportQueryArgumentsDto);

                    report = results.Report;

//					counter.Success();
				}
				catch (AutoCheckException ce)
				{
					HandleAutoCheckException(ce, "Failed to get report");
				}
				catch (Exception ex)
				{
					HandleException(ex, "Failed to get report");
				}

				return report;
//			}
        }

		/// <remarks>
		/// The Java code treats this code as a "purchase and show me the report" which
		/// is different to how the C# code will make you purchase the report (where
		/// we download the XML) before it will show the dealer the full report (make
		/// the HTML request).
		/// </remarks>
		[WebMethod(Description = "Return the html for a report; purchase the report if necessary.")]
		[SoapHeader("UserIdentity", Direction = SoapHeaderDirection.In)]
		public string GetReportHtml(int dealerId, string vin)
		{
			string html = string.Empty;

			try
			{
                ICommand<ReportHtmlResultsDto, ReportHtmlArgumentsDto> command = Resolve<ICommandFactory>().CreateReportHtmlCommand();

                ReportHtmlArgumentsDto reportHtmlArgumentsDto = new ReportHtmlArgumentsDto
                {
                    Client = new ClientDto
                    {
                        ClientType = ClientTypeDto.Dealer,
                        Id = dealerId
                    },
                    UserName = UserName,
                    Vin = vin
                };

                ReportHtmlResultsDto results = ExecuteCommand(command, reportHtmlArgumentsDto);

                html = results.Report;
			}
			catch (AutoCheckException ce)
			{
				HandleAutoCheckException(ce, "Failed to get report");
			}
			catch (Exception ex)
			{
				HandleException(ex, "Failed to get report");
			}

			return html;
		}

        [WebMethod(Description = "Purchase a report")]
        [SoapHeader("UserIdentity", Direction = SoapHeaderDirection.In)]
        public AutoCheckReportTO PurchaseReport(int dealerId, string vin)
        {
//			using (ICounter counter = CounterRegistry.GetCounter(InstrumentedResponsibility.Internal, InstrumentedSystem.WebService, InstrumentedProvider.AutoCheck, InstrumentedOperation.WebServicePurchaseReport))
//			{
				AutoCheckReportTO report = null;

				try
				{
                    ICommand<PurchaseReportResultsDto, PurchaseReportArgumentsDto> command = Resolve<ICommandFactory>().CreatePurchaseReportCommand();

                    PurchaseReportArgumentsDto purchaseReportArgumentsDto = new PurchaseReportArgumentsDto
                    {
                        Client = new ClientDto
                        {
                            ClientType =
                                ClientTypeDto.Dealer,
                            Id = dealerId
                        },
                        UserName = UserName,
                        Vin = vin
                    };

                    PurchaseReportResultsDto results = ExecuteCommand(command, purchaseReportArgumentsDto);

                    report = results.Report;

//					counter.Success();
				}
				catch (AutoCheckException ce)
				{
					HandleAutoCheckException(ce, "Failed to purchase report");
				}
				catch (Exception ex)
				{
					HandleException(ex, "Failed to purchase report");
				}

				return report;
//			}
        }

		[WebMethod(Description = "Purchase reports for a dealers vehicles of the given vehicle entity type")]
		[SoapHeader("UserIdentity", Direction = SoapHeaderDirection.In)]
		public void PurchaseReports(int dealerId, VehicleEntityType vehicleEntityType)
		{
			try
			{
                ICommand<PurchaseReportsResultsDto, PurchaseReportsArgumentsDto> command = Resolve<ICommandFactory>().CreatePurchaseReportsCommand();


                PurchaseReportsArgumentsDto purchaseReportsArgumentsDto = new PurchaseReportsArgumentsDto
                {
                    Client = new ClientDto
                    {
                        ClientType =
                            ClientTypeDto.Dealer,
                        Id = dealerId
                    },
                    UserName = UserName,
                    VehicleEntityType = vehicleEntityType
                };

                ExecuteCommand(command, purchaseReportsArgumentsDto);
			}
			catch (Exception ex)
			{
				HandleException(ex, "Failed to purchase report");
			}
		}

    	#region Helper Methods

        static T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }

		#endregion

        #endregion

		#region Exception Handling

		private void HandleException(Exception ex, string message)
		{
			AutoCheckExceptionLogger.Record(ex);

			XmlDocument document = new XmlDocument();

			XmlNode detail = document.CreateNode(
				XmlNodeType.Element,
				SoapException.DetailElementName.Name,
				SoapException.DetailElementName.Namespace);

			XmlNode serviceException = document.CreateNode(
				XmlNodeType.Element,
				"ServiceException",
				"http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/");

			serviceException.AppendChild(document.CreateTextNode(
				"Unexpected Error. Please contact FirstLook support."));

			detail.AppendChild(serviceException);

			throw new SoapException(
				message,
				SoapException.ServerFaultCode,
				Context.Request.Url.AbsoluteUri,
				detail);
		}

		private void HandleAutoCheckException(AutoCheckException ex, string message)
		{
			AutoCheckExceptionLogger.Record(ex, ex.ReportId);

			XmlDocument document = new XmlDocument();

			XmlNode detail = document.CreateNode(
				XmlNodeType.Element,
				SoapException.DetailElementName.Name,
				SoapException.DetailElementName.Namespace);

			XmlNode responseCode = document.CreateNode(
				XmlNodeType.Element,
				"ResponseCode",
				"http://services.firstlook.biz/VehicleHistoryReport/AutoCheck/");

			responseCode.AppendChild(
				document.CreateTextNode(
					((int)ex.ResponseCode).ToString()));

			detail.AppendChild(responseCode);

			throw new SoapException(
				message,
				SoapException.ServerFaultCode,
				Context.Request.Url.AbsoluteUri,
				detail);
		}

		#endregion

        /// <summary>
        /// Retrieve the registered CommandExecutor as defined in Global.asax.cs.  This command executor will wrap the command in a 
        /// try catch block.  If an exception is thrown the listener(s) associated with the executor is invoked.  In this instance
        /// the listener is FaultWebExceptionListener, it will save the exception information in the fault database.
        /// </summary>
        /// <typeparam name="TResult">The type associated with the result of executing the passed in command.</typeparam>
        /// <typeparam name="TParameter">The type associated with the parameters to pass in on the command.</typeparam>
        /// <param name="command">The command to execute.</param>
        /// <param name="args">The parameters to pass in on the command.</param>
        /// <returns>The respective results dto depending on the command invoked.</returns>
        private static TResult ExecuteCommand<TResult, TParameter>(ICommand<TResult, TParameter> command, TParameter args)
        {
            ICommandExecutor executor = RegistryFactory.GetResolver().Resolve<ICommandExecutor>();
            return executor.Execute(command, args).Result;
    	}
    }
}
