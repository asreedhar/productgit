using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleHistoryReport.DomainModel;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleHistoryReport.DomainModel.Diagnostics;
using ICommandFactory=FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.ICommandFactory;

namespace FirstLook.VehicleHistoryReport.WebService.Services
{
	/// <summary>
	/// Summary description for CarfaxWebService
	/// </summary>
	[WebService(Namespace = "http://services.firstlook.biz/VehicleHistoryReport/Carfax/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[ToolboxItem(false)]
	public class CarfaxWebService : System.Web.Services.WebService
	{
		private UserIdentity _userIdentity;
        
		public UserIdentity UserIdentity
		{
			get { return _userIdentity; }
			set { _userIdentity = value; }
		}

		private string UserName
		{
			get { return UserIdentity == null ? string.Empty : UserIdentity.UserName; }
		}

		#region Report Preference

		[WebMethod(Description = "Whether the dealer has a Carfax account")]
		[SoapHeader("UserIdentity", Direction = SoapHeaderDirection.In)]
		public bool HasAccount(int dealerId)
		{
//			using (ICounter counter = CounterRegistry.GetCounter(InstrumentedResponsibility.Internal, InstrumentedSystem.WebService, InstrumentedProvider.Carfax, InstrumentedOperation.WebServiceHasAccount))
//			{
				bool hasAccount = false;

				try
				{
                    ICommand<AccountQueryResultsDto, AccountQueryArgumentsDto> command = Resolve<ICommandFactory>().CreateAccountQueryCommand();

                    AccountQueryArgumentsDto accountQueryArgumentsDto = new AccountQueryArgumentsDto
                    {
                        Client = new ClientDto
                        {
                            ClientType = ClientTypeDto.Dealer,
                            Id = dealerId
                        },
                        UserName = UserName
                    };

                    AccountQueryResultsDto results = ExecuteCommand(command, accountQueryArgumentsDto);

				    hasAccount = results.HasAccount;

//					counter.Success();
				}
				catch (Exception ex)
				{
					HandleException(ex, "Failed to determine if user can purchase report");
				}

				return hasAccount;
//			}
		}

		[WebMethod(Description = "Return a dealers CARFAX report-preference")]
		[SoapHeader("UserIdentity", Direction = SoapHeaderDirection.In)]
		public CarfaxReportPreferenceTO GetReportPreference(int dealerId, VehicleEntityType vehicleEntityType)
		{
//			using (ICounter counter = CounterRegistry.GetCounter(InstrumentedResponsibility.Internal, InstrumentedSystem.WebService, InstrumentedProvider.Carfax, InstrumentedOperation.WebServiceGetReportPreference))
//			{
				CarfaxReportPreferenceTO info = new CarfaxReportPreferenceTO();

				try
				{
                    ICommand<ReportPreferenceResultsDto, ReportPreferenceArgumentsDto> command = Resolve<ICommandFactory>().CreateReportPreferenceCommand();


                    ReportPreferenceArgumentsDto reportPreferenceArgumentsDto = new ReportPreferenceArgumentsDto
                    {
                        Client = new ClientDto
                        {
                            ClientType =
                                ClientTypeDto.Dealer,
                            Id = dealerId
                        },
                        UserName = UserName,
                        VehicleEntityType = vehicleEntityType
                    };

                   

                    ReportPreferenceResultsDto results = ExecuteCommand(command, reportPreferenceArgumentsDto);

				    info = results.Preference;

//					counter.Success();
				}
				catch (Exception ex)
				{
					HandleException(ex, "Failed to get report preferences");
				}

				return info;
//			}
		}

		[WebMethod(Description = "Whether the member can purchase a report at the dealer")]
		[SoapHeader("UserIdentity", Direction = SoapHeaderDirection.In)]
		public bool CanPurchaseReport(int dealerId)
		{
//			using (ICounter counter = CounterRegistry.GetCounter(InstrumentedResponsibility.Internal, InstrumentedSystem.WebService, InstrumentedProvider.Carfax, InstrumentedOperation.WebServiceCanPurchaseReport))
//			{
				bool canPurchaseReport = false;

				try
				{
                    ICommand<PurchaseAuthorityResultsDto, PurchaseAuthorityArgumentsDto> command = Resolve<ICommandFactory>().CreatePurchaseAuthorityCommand();

                    PurchaseAuthorityArgumentsDto purchaseAuthorityArgumentsDto = new PurchaseAuthorityArgumentsDto
                    {
                        Client = new ClientDto
                        {
                            ClientType =
                                ClientTypeDto.Dealer,
                            Id = dealerId
                        },
                        UserName = UserName
                    };


                  
                    PurchaseAuthorityResultsDto results = ExecuteCommand(command, purchaseAuthorityArgumentsDto);

                    canPurchaseReport = results.Authority;

//					counter.Success();
				}
				catch (Exception ex)
				{
					HandleException(ex, "Failed to determine if user can purchase report");
				}

				return canPurchaseReport;
//			}
		}

		#endregion

		#region Report

		[WebMethod(Description = "Return a existing report")]
		[SoapHeader("UserIdentity", Direction = SoapHeaderDirection.In)]
		public CarfaxReportTO GetReport(int dealerId, string vin)
		{
//			using (ICounter counter = CounterRegistry.GetCounter(InstrumentedResponsibility.Internal, InstrumentedSystem.WebService, InstrumentedProvider.Carfax, InstrumentedOperation.WebServiceGetReport))
//			{
				CarfaxReportTO report = null;

				try
				{
				    ICommand<ReportQueryResultsDto, ReportQueryArgumentsDto> command = Resolve<ICommandFactory>().CreateReportQueryCommand();


                    ReportQueryArgumentsDto reportQueryArgumentsDto = new ReportQueryArgumentsDto
                    {
                        Client = new ClientDto
                        {
                            ClientType = ClientTypeDto.Dealer,
                            Id = dealerId
                        },
                        UserName = UserName,
                        Vin = vin
                    };

                 

                    ReportQueryResultsDto results = ExecuteCommand(command, reportQueryArgumentsDto);

				    report = results.Report;

//					counter.Success();
				}
				catch (CarfaxException ce)
				{
					HandleCarfaxException(ce, "Failed to get report");
				}
				catch (Exception ex)
				{
					HandleException(ex, "Failed to get report");
				}

				return report;
//			}
		}

        [WebMethod(Description = "Return all existing reports")]
        [SoapHeader("UserIdentity", Direction = SoapHeaderDirection.In)]
        public List<CarfaxReportTO> GetReports(int dealerId, VehicleEntityType vehicleEntityType)
        {
            List<CarfaxReportTO> reports = new List<CarfaxReportTO>();

            try
            {
                ICommand<ReportsQueryResultsDto, ReportsQueryArgumentsDto> command = Resolve<ICommandFactory>().CreateReportsQueryCommand();

                ReportsQueryArgumentsDto reportsQueryArgumentsDto = new ReportsQueryArgumentsDto
                {
                    Client = new ClientDto
                    {
                        ClientType =
                            ClientTypeDto.Dealer,
                        Id = dealerId
                    },
                    UserName = UserName,
                    VehicleEntityType = vehicleEntityType
                };

             
                ReportsQueryResultsDto results = ExecuteCommand(command, reportsQueryArgumentsDto);

                reports = results.Reports;
            }
            catch (Exception ex)
            {
                HandleException(ex, "Failed to get reports");
            }

            return reports;
        }

		[WebMethod(Description = "Purchase a report")]
		[SoapHeader("UserIdentity", Direction = SoapHeaderDirection.In)]
		public CarfaxReportTO PurchaseReport(int dealerId, string vin, CarfaxReportType reportType, bool displayInHotList, CarfaxTrackingCode trackingCode)
		{
//			using (ICounter counter = CounterRegistry.GetCounter(InstrumentedResponsibility.Internal, InstrumentedSystem.WebService, InstrumentedProvider.Carfax, InstrumentedOperation.WebServicePurchaseReport))
//			{
				CarfaxReportTO report = null;

				try
				{
                    ICommand<PurchaseReportResultsDto, PurchaseReportArgumentsDto> command = Resolve<ICommandFactory>().CreatePurchaseReportCommand();

                    PurchaseReportArgumentsDto purchaseReportArgumentsDto = new PurchaseReportArgumentsDto
                    {
                        Client = new ClientDto
                        {
                            ClientType =
                                ClientTypeDto.Dealer,
                            Id = dealerId
                        },
                        UserName = UserName,
                        Vin = vin,
                        ReportType = reportType,
                        DisplayInHotList = displayInHotList,
                        TrackingCode = trackingCode
                    };

                   

                    PurchaseReportResultsDto results = ExecuteCommand(command, purchaseReportArgumentsDto);

                    report = results.Report;

//					counter.Success();
				}
				catch (CarfaxException ce)
				{
					HandleCarfaxException(ce, "Failed to purchase report");
				}
				catch (Exception ex)
				{
					HandleException(ex, "Failed to purchase report");
				}

				return report;
//			}
		}

		[WebMethod(Description = "Purchase reports for a dealers vehicles of the given vehicle entity type")]
		[SoapHeader("UserIdentity", Direction = SoapHeaderDirection.In)]
		public void PurchaseReports(int dealerId, VehicleEntityType vehicleEntityType, CarfaxReportType reportType, bool displayInHotList, CarfaxTrackingCode trackingCode)
		{
			try
			{
                ICommand<PurchaseReportsResultsDto, PurchaseReportsArgumentsDto> command = Resolve<ICommandFactory>().CreatePurchaseReportsCommand();

                PurchaseReportsArgumentsDto purchaseReportsArgumentsDto = new PurchaseReportsArgumentsDto
                {
                    Client = new ClientDto
                    {
                        ClientType =
                            ClientTypeDto.Dealer,
                        Id = dealerId
                    },
                    UserName = UserName,
                    VehicleEntityType = vehicleEntityType,
                    ReportType = reportType,
                    DisplayInHotList = displayInHotList,
                    TrackingCode = trackingCode
                };

			 

                ExecuteCommand(command, purchaseReportsArgumentsDto);

			}
			catch (Exception ex)
			{
				HandleException(ex, "Failed to purchase report");
			}
		}

		#region Helper Methods
		
		static T Resolve<T>()
		{
		    return RegistryFactory.GetResolver().Resolve<T>();
		}

		#endregion

		#endregion

		#region Exception Handling
		
		private void HandleException(Exception ex, string message)
		{
			CarfaxExceptionLogger.Record(ex);

			XmlDocument document = new XmlDocument();

			XmlNode detail = document.CreateNode(
				XmlNodeType.Element,
				SoapException.DetailElementName.Name,
				SoapException.DetailElementName.Namespace);

			XmlNode serviceException = document.CreateNode(
				XmlNodeType.Element,
				"ServiceException",
				"http://services.firstlook.biz/VehicleHistoryReport/Carfax/");

			serviceException.AppendChild(document.CreateTextNode(
				"Unexpected Error. Please contact FirstLook support."));

			detail.AppendChild(serviceException);

			throw new SoapException(
				message,
				SoapException.ServerFaultCode,
				Context.Request.Url.AbsoluteUri,
				detail);
		}

		private void HandleCarfaxException(CarfaxException ex, string message)
		{
			CarfaxExceptionLogger.Record(ex, ex.ReportId);

			XmlDocument document = new XmlDocument();

			XmlNode detail = document.CreateNode(
				XmlNodeType.Element,
				SoapException.DetailElementName.Name,
				SoapException.DetailElementName.Namespace);

			XmlNode responseCode = document.CreateNode(
				XmlNodeType.Element,
				"ResponseCode",
				"http://services.firstlook.biz/VehicleHistoryReport/Carfax/");

			responseCode.AppendChild(
				document.CreateTextNode(
					((int)ex.ResponseCode).ToString()));

			detail.AppendChild(responseCode);

			throw new SoapException(
				message,
				SoapException.ServerFaultCode,
				Context.Request.Url.AbsoluteUri,
				detail);
		}

		#endregion

        /// <summary>
        /// Retrieve the registered CommandExecutor as defined in Global.asax.cs.  This command executor will wrap the command in a 
        /// try catch block.  If an exception is thrown the listener(s) associated with the executor is invoked.  In this instance
        /// the listener is FaultWebExceptionListener, it will save the exception information in the fault database.
        /// </summary>
        /// <typeparam name="TResult">The type associated with the result of executing the passed in command.</typeparam>
        /// <typeparam name="TParameter">The type associated with the parameters to pass in on the command.</typeparam>
        /// <param name="command">The command to execute.</param>
        /// <param name="args">The parameters to pass in on the command.</param>
        /// <returns>The respective results dto depending on the command invoked.</returns>
        private static TResult ExecuteCommand<TResult, TParameter>(ICommand<TResult, TParameter> command, TParameter args)
        {
            ICommandExecutor executor = RegistryFactory.GetResolver().Resolve<ICommandExecutor>();
            return executor.Execute(command, args).Result;
    	}
    }
}
