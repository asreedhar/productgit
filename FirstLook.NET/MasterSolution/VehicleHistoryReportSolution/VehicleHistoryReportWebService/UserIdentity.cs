using System;
using System.Web.Services.Protocols;

namespace FirstLook.VehicleHistoryReport.WebService
{
	[Serializable]
	public class UserIdentity : SoapHeader
	{
		private string userName;

		public string UserName
		{
			get { return userName; }
			set { userName = value; }
		}
	}
}
