﻿using System;
using System.Web;
using System.Web.Caching;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Fault.DomainModel.Utilities;

namespace FirstLook.VehicleHistoryReport.WebApplication
{
    public class Global : HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            IRegistry registry = RegistryFactory.GetRegistry();

            registry.Register<Common.Core.Data.IDataSessionManager, Common.Core.Data.DataSessionManager>(ImplementationScope.Shared);

            registry.Register<Client.DomainModel.Module>();

            registry.Register<DomainModel.Module>();

            registry.Register<ICache, WebCache>(ImplementationScope.Shared);

            registry.Register<ICommandExecutor, ListeningCommandExecutor>(ImplementationScope.Isolated);

            //Added for fault reporting (health montioring) integration

            registry.Register<Fault.DomainModel.Module>();
        }

        protected void Application_End(object sender, EventArgs e)
        {
            RegistryFactory.GetRegistry().Dispose();
        }

        private class WebCache : ICache
        {
            private readonly Cache _cache;

            public WebCache()
            {
                _cache = HttpContext.Current.Cache;
            }

            public object Add(string key, object value, DateTime absoluteExpiration, TimeSpan slidingExpiration)
            {
                return _cache.Add(
                    key,
                    value,
                    null,
                    absoluteExpiration,
                    slidingExpiration,
                    CacheItemPriority.Normal,
                    null);
            }

            public object Get(string key)
            {
                return _cache.Get(key);
            }

            public object Remove(string key)
            {
                return _cache.Remove(key);
            }
        }

        private class ListeningCommandExecutor : SimpleCommandExecutor
        {

            public ListeningCommandExecutor()
            {
                Add(new FaultWebExceptionObserver());
            }

        }
    }
}