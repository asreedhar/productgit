using System;
using System.Web.UI;
using FirstLook.VehicleHistoryReport.DomainModel;

namespace FirstLook.VehicleHistoryReport.WebApplication.Pages
{
	public partial class Page : MasterPage
	{
		protected void Page_Init(object sender, EventArgs e)
		{
			string pageName = Request.AppRelativeCurrentExecutionFilePath;

			if (pageName.Contains("Carfax/SystemAccount.aspx") ||
				pageName.Contains("AutoCheck/ServiceAccount.aspx") ||
				pageName.Contains("AutoCheck/SystemAccount.aspx") ||
                pageName.Contains("VehicleHistoryReportAdminHelp.aspx"))
			{
				if (pageName.Contains("SystemAccount.aspx"))
				{
					DealerNameLabel.Text = "System Account";
				}
				else
				{
					DealerNameLabel.Text = "Service Account";
				}
				
				CarfaxAccountLink.Enabled = false;
				CarfaxPermissionsLink.Enabled = false;
				CarfaxReportPreferenceLink_I.Enabled = false;
				CarfaxReportPreferenceLink_A.Enabled = false;

				AutoCheckAccountLink.Enabled = false;
				AutoCheckPermissionsLink.Enabled = false;
			}
			else
			{
				int id;

				if (int.TryParse(Request.QueryString["DealerId"], out id))
				{
					if (Dealer.Exists(id))
					{
						DealerNameLabel.Text = Dealer.GetDealer(id).Name;

						string context = "DealerId=" + Request.QueryString["DealerId"];

						CarfaxAccountLink.NavigateUrl += "?" + context;
						CarfaxPermissionsLink.NavigateUrl += "?" + context;
						CarfaxReportPreferenceLink_I.NavigateUrl += "&" + context;
						CarfaxReportPreferenceLink_A.NavigateUrl += "&" + context;

					    AutoCheckAccountLink.NavigateUrl += "?" + context;
                        AutoCheckPermissionsLink.NavigateUrl += "?" + context;
					    AutoCheckServiceAccountLink_X.NavigateUrl += "&" + context;
					    AutoCheckServiceAccountLink_H.NavigateUrl += "&" + context;
					}
					else
					{
						Response.Redirect("~/Pages/ErrorPage.aspx?ErrorCode=410");
					}
				}
				else
				{
					Response.Redirect("~/Pages/ErrorPage.aspx?ErrorCode=410");
				}
			}
		}
	}
}
