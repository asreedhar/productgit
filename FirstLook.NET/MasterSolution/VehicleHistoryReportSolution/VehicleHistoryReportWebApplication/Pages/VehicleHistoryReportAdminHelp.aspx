<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VehicleHistoryReportAdminHelp.aspx.cs" Inherits="FirstLook.VehicleHistoryReport.WebApplication.Pages.VehicleHistoryReportAdminHelp" MasterPageFile="~/Pages/Page.Master" %>

<%@ OutputCache Location="None" VaryByParam="None" %>

<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" ID="BodyContent" runat="server">

<h2>Vehicle History Report Help</h2>

<div>
    <h3>Troubleshooting</h3>

    <p>How to Troubleshoot VHR issues:</p>

    <ol>
        <li>Create a non-admin test member.</li>
        <li>Add the test user to the dealer.</li>
        <li>Assign permissions to the test user.</li>
        <li>Log out of FL Admin.</li>
        <li>Log in as test user to troubleshoot. </li>
    </ol>
</div>

<div>
    <h3>System Account</h3>
    
    <p>All Admin members will use the System Account settings.  No configuration is required.</p>
</div>

<div>
    <h3>Training/Demoing</h3>
    
    <p>Admin users may purchase reports at any store for training and demoing.  Reports purchased by Admin members will be visible by the logged in Admin and other Admins only. </p>
</div>

</asp:Content>