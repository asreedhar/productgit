using System;
using System.Web.UI.WebControls;
using FirstLook.VehicleHistoryReport.DomainModel;

namespace FirstLook.VehicleHistoryReport.WebApplication.Pages.AutoCheck
{
    public partial class Permissions : System.Web.UI.Page
    {
        private DomainModel.AutoCheck.DealerAccount _dealerAccount;

        public DomainModel.AutoCheck.DealerAccount DealerAccount
        {
            get
            {
                if (_dealerAccount == null)
                {
                    int id;

                    if (int.TryParse(Request.QueryString["DealerId"], out id))
                    {
                        if (DomainModel.AutoCheck.DealerAccount.Exists(id))
                        {
                            _dealerAccount = DomainModel.AutoCheck.DealerAccount.GetDealerAccount(id);    
                        }
                        else
                        {
                            _dealerAccount = DomainModel.AutoCheck.DealerAccount.NewDealerAccount(id);
                        }
                    }
                }

                return _dealerAccount;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            int id;

            if (int.TryParse(Request.QueryString["DealerId"], out id) && Dealer.Exists(id))
            {
                DealerNameLiteral.Text = Dealer.GetDealer(id).Name;
            }
        }

        protected void MemberGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Member member = e.Row.DataItem as Member;

            if (member != null)
            {
                IButtonControl button = (IButtonControl)e.Row.Cells[2].Controls[0];

                if (DealerAccount.Account.Assignments.Contains(member.Id))
                {
                    button.Text = "Remove";
                }
                else
                {
                    button.Text = "Assign";
                }
            }           
        }

        protected void MemberGridView_DataBound(object sender, EventArgs e)
        {
            NoMembersText.Visible = MemberGridView.Rows.Count == 0;
        }

        protected void MemberGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (string.Equals(e.CommandName, "TogglePermissions"))
            {
                GridView view = sender as GridView;

                if (view != null)
                {
                    int idx = Convert.ToInt32(e.CommandArgument);

                    if (idx >= 0 && idx < view.Rows.Count)
                    {
                        int id = (int)view.DataKeys[idx]["Id"];

                        GridViewRow row = view.Rows[idx];

                        IButtonControl button = (IButtonControl)row.Cells[2].Controls[0];

                        if (DealerAccount.Account.Assignments.Contains(id))
                        {
                            DealerAccount.Account.Assignments.Remove(id);
                            button.Text = "Assign";
                        }
                        else
                        {
                            DealerAccount.Account.Assignments.AssignTo(id);
                            button.Text = "Remove";
                        }

                        DealerAccount.Save();

                        Response.Redirect(Request.Url.ToString(), true);
                    }
                }
            }
        }

        protected void MemberGridView_Load(object sender, EventArgs e)
        {
            if (DealerAccount.IsNew)
            {
                MemberGridView.Visible = false;

                NoAccountText.Visible = true;
            }
        }
    }
}
