﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Test.aspx.cs" Inherits="FirstLook.VehicleHistoryReport.WebApplication.Pages.AutoCheck.Test" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>AutoCheck - Test Page</title>
    <script src="../../Public/Scripts/Lib/LABjs-1.0.2rc1/LAB.js" type="text/javascript" charset="utf-8"></script>
</head>
<body>

    <div id="white"><div id="white_r"></div></div>

    <form id="AutoCheckForm" runat="server">
    
    <div id="nav_header">
    </div>
    
    <div id="main_content" class="Wrapper">
        <div id="header" class="Navigation"><div id="header_r"><div id="header_l">
            <h1 class="title">Vehicle History Report</h1>
        </div></div></div>
        <div id="body" class="Content">
            <h2>AutoCheck Test</h2>
            <fieldset id="example">
                <legend>Vehicle</legend>
                <table border="1">
                    <tbody>
                        <tr>
                            <th><label for="broker">Broker</label></th>
                            <td><input type="text" id="broker" name="broker" maxlength="36" /></td>
                        </tr>
                        <tr>
                            <th><label for="vin">VIN</label></th>
                            <td><input type="text" id="vin" name="vin" maxlength="17" /></td>
                        </tr>
                        <tr>
                            <td><input type="submit" name="See" id="See" value="See" /></td>
                            <td><input type="submit" name="Buy" id="Buy" value="Buy" /></td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>
            <fieldset id="report">
                <legend>Report</legend>
                <div style="float: left; width: 45%;">
                    <h3>Summary</h3>
                    <dl>
                        <dt>Expiration</dt>
                        <dd id="x_expiration">?</dd>
                        <dt>Owner Count</dt>
                        <dd id="x_owner_count">?</dd>
                        <dt>Score</dt>
                        <dd id="x_score">?</dd>
                        <dt>Comparable Score - Low</dt>
                        <dd id="x_score_comparable_low">?</dd>
                        <dt>Comparable Score - High</dt>
                        <dd id="x_score_comparable_high">?</dd>
                    </dl>
                </div>
                <div style="float: left; width: 45%;">
                    <h3>Inspections</h3>
                    <dl>
                        <dt>1-Owner</dt>
                        <dd id="i_ownership">?</dd>
                        <dt>Assured</dt>
                        <dd id="i_assured">?</dd>
                        <dt>No Total Loss Reported</dt>
                        <dd id="i_total_loss">?</dd>
                        <dt>No Frame Damage Reported</dt>
                        <dd id="i_frame_damage">?</dd>
                        <dt>No Airbag Deployment Reported</dt>
                        <dd id="i_air_bag_deployment">?</dd>
                        <dt>No Odometer Rollback Reported</dt>
                        <dd id="i_rollback_odometer">?</dd>
                        <dt>No Accidents / Damage Reported</dt>
                        <dd id="i_accident_indicators">?</dd>
                        <dt>No Manufacturer Recalls Reported</dt>
                        <dd id="i_manufacturer_recall">?</dd>
                    </dl>
                </div>
            </fieldset>
        </div>
    </div>

    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/Lib/jquery/jquery-ui-1.8.2.custom/js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/AutoCheck/Application.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/AutoCheck/Templates.js"></script>

    <script type="text/javascript" charset="utf-8">
        var _AutoCheck = new AutoCheck();
        _AutoCheck.main();

        if (typeof JSON === "undefined") {
            $LAB.script("../../Public/Scripts/Lib/json/json2.min.js");
        }
    </script>
    
    </form>
</body>
</html>
