using System;
using System.Collections;
using System.Web.UI.WebControls;

namespace FirstLook.VehicleHistoryReport.WebApplication.Pages.Carfax
{
	public partial class ReportPreference : System.Web.UI.Page
	{
        protected void Page_Load (Object sender, EventArgs e)
        {
            if (Request.Params["VehicleEntityTypeId"] == "1")
            {
                VehicleEntityTypeLiteral.Text =
                    VehicleEntityTypeLiteral2.Text = VehicleEntityTypeLiteral3.Text = "Inventory";
                InventoryHelpText.Visible = true;
                AppraisalHelpText.Visible = false;
            }
            else if (Request.Params["VehicleEntityTypeId"] == "2")
            {
                VehicleEntityTypeLiteral.Text =
                    VehicleEntityTypeLiteral2.Text = VehicleEntityTypeLiteral3.Text = "Appraisal";
                InventoryHelpText.Visible = false;
                AppraisalHelpText.Visible = true;
            }
            
        }

		protected void ReportPreferenceDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
		{
			if (e.ReturnValue == null)
			{
				ReportPreferenceFormView.ChangeMode(DetailsViewMode.Insert);

				ReportPreferenceFormView.AutoGenerateInsertButton = true;
			}
			else
			{
				ReportPreferenceFormView.AutoGenerateInsertButton = false;
			}
		}

		protected void ReportPreferenceFormView_ItemInserting(object sender, DetailsViewInsertEventArgs e)
		{
			bool hasErrors = ValidateInput(e.Values);

			ErrorList.Visible = hasErrors;

			e.Cancel = hasErrors;
		}

		protected void ReportPreferenceFormView_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
		{
			if (e.Exception != null)
			{
				HandleException(e.Exception);
				e.KeepInInsertMode = true;
				e.ExceptionHandled = true;
			}
			else
			{
				e.KeepInInsertMode = false;
			}
		}

		protected void ReportPreferenceFormView_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
		{
			bool hasErrors = ValidateInput(e.NewValues);

			ErrorList.Visible = hasErrors;

			e.Cancel = hasErrors;
		}

		protected void ReportPreferenceFormView_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
		{
			if (e.Exception != null)
			{
				HandleException(e.Exception);
				e.KeepInEditMode = true;
				e.ExceptionHandled = true;
			}
			else
			{
				e.KeepInEditMode = false;
			}
		}

		private bool ValidateInput(IDictionary newValues)
		{
			ErrorList.Items.Clear();

			object reportTypeValue = newValues["ReportType"];

			if (reportTypeValue == null)
			{
				ErrorList.Items.Add(new ListItem("Default report-type is not specified"));
			}

			return ErrorList.Items.Count > 0;
		}

		private void HandleException(Exception exception)
		{
			ErrorList.Items.Clear();

			if (!string.IsNullOrEmpty(exception.Message))
				ErrorList.Items.Add(new ListItem(exception.Message));

			if (exception.InnerException != null &&
				!string.IsNullOrEmpty(exception.InnerException.Message))
				ErrorList.Items.Add(new ListItem(exception.InnerException.Message));

			ErrorList.Visible = (ErrorList.Items.Count > 0);
		}

	    protected void ReportPreferenceFormView_ModeChanged(object sender, EventArgs e)
	    {
	        ReportPreferenceFormViewEditModeHelp.Visible = (ReportPreferenceFormView.CurrentMode == DetailsViewMode.Edit ||
	                                                        ReportPreferenceFormView.CurrentMode == DetailsViewMode.Insert);
	    }
	}
}
