using System;
using System.Web.UI.WebControls;

namespace FirstLook.VehicleHistoryReport.WebApplication.Pages.Carfax
{
	public partial class SystemAccount : System.Web.UI.Page
	{
		protected void AccountDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
		{
			if (e.ReturnValue == null)
			{
				AccountDetailsView.ChangeMode(DetailsViewMode.Insert);

				AccountDetailsView.AutoGenerateInsertButton = true;
			}
			else
			{
				AccountDetailsView.AutoGenerateInsertButton = false;
			}
		}

		protected void AccountDetailsView_Inserting(object sender, DetailsViewInsertEventArgs e)
		{
			string userName = ToString(e.Values["UserName"]);
			string password = ToString(e.Values["Password"]);

			if (!ValidateInput(userName, password))
			{
				e.Cancel = true;
			}
		}

		protected void AccountDetailsView_Inserted(object sender, DetailsViewInsertedEventArgs e)
		{
			if (e.Exception != null)
			{
				HandleException(e.Exception);
				e.KeepInInsertMode = true;
				e.ExceptionHandled = true;
			}
			else
			{
				e.KeepInInsertMode = false;
			}
		}

		protected void AccountDetailsView_Updating(object sender, DetailsViewUpdateEventArgs e)
		{
			string userName = ToString(e.NewValues["UserName"]);
			string password = ToString(e.NewValues["Password"]);

			if (!ValidateInput(userName, password))
			{
				e.Cancel = true;
			}
		}

		protected void AccountDetailsView_Updated(object sender, DetailsViewUpdatedEventArgs e)
		{
			if (e.Exception != null)
			{
				HandleException(e.Exception);
				e.KeepInEditMode = true;
				e.ExceptionHandled = true;
			}
			else
			{
				e.KeepInEditMode = false;
			}
		}

		protected void AccountDetailsView_Deleted(object sender, DetailsViewDeletedEventArgs e)
		{
			if (e.Exception != null)
			{
				HandleException(e.Exception);

				e.ExceptionHandled = true;
			}
		}

		private static string ToString(object value)
		{
			if (value == null)
				return string.Empty;
			return value.ToString();
		}

		private bool ValidateInput(string userName, string password)
		{
			ErrorList.Items.Clear();

			if (string.IsNullOrEmpty(userName))
			{
				ErrorList.Items.Add(new ListItem("User Name cannot be empty"));
			}
			else
			{
				if (userName.Length > 20)
				{
					ErrorList.Items.Add(new ListItem("User Name cannot exceed 20 characters"));
				}
			}

			if (string.IsNullOrEmpty(password))
			{
				ErrorList.Items.Add(new ListItem("Password cannot be empty"));
			}
			else
			{
				if (password.Length > 20)
				{
					ErrorList.Items.Add(new ListItem("Password cannot exceed 20 characters"));
				}
			}

			ErrorList.Visible = (ErrorList.Items.Count > 0);

			return (ErrorList.Items.Count == 0);
		}

		private void HandleException(Exception exception)
		{
			ErrorList.Items.Clear();

			if (!string.IsNullOrEmpty(exception.Message))
				ErrorList.Items.Add(new ListItem(exception.Message));

			if (exception.InnerException != null &&
				!string.IsNullOrEmpty(exception.InnerException.Message))
				ErrorList.Items.Add(new ListItem(exception.InnerException.Message));

			ErrorList.Visible = (ErrorList.Items.Count > 0);
		}

	    protected void AccountDetailsView_ModeChanged(object sender, EventArgs e)
	    {
	        AccountDetailsViewEditModeHelp.Visible = (AccountDetailsView.CurrentMode == DetailsViewMode.Edit);
	    }
	}
}
