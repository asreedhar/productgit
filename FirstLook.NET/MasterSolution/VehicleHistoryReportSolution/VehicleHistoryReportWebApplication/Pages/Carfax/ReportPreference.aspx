<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportPreference.aspx.cs" Inherits="FirstLook.VehicleHistoryReport.WebApplication.Pages.Carfax.ReportPreference" MasterPageFile="~/Pages/Page.Master" %>

<%@ OutputCache Location="None" VaryByParam="None" %>

<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" ID="BodyContent" runat="server">
    <asp:ObjectDataSource ID="ReportPreferenceDataSource" runat="server"
            TypeName="FirstLook.VehicleHistoryReport.DomainModel.Carfax.CarfaxReportPreference+DataSource"
            SelectMethod="Select"
            InsertMethod="Insert"
            UpdateMethod="Update"
            DeleteMethod="Delete"
            OnSelected="ReportPreferenceDataSource_Selected">
        <SelectParameters>
            <asp:QueryStringParameter Name="dealerId" QueryStringField="DealerId" Type="Int32" />
            <asp:QueryStringParameter Name="vehicleEntityType" QueryStringField="VehicleEntityTypeId" Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:QueryStringParameter Name="dealerId" QueryStringField="DealerId" Type="Int32" />
            <asp:QueryStringParameter Name="vehicleEntityType" QueryStringField="VehicleEntityTypeId" Type="Int32" />
        </InsertParameters>
        <UpdateParameters>
            <asp:QueryStringParameter Name="dealerId" QueryStringField="DealerId" Type="Int32" />
            <asp:QueryStringParameter Name="vehicleEntityType" QueryStringField="VehicleEntityTypeId" Type="Int32" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    <h2>Carfax Report <asp:Literal runat="server" ID="VehicleEntityTypeLiteral" /> Preference</h2>
    <div class="help_text" runat="server" id="InventoryHelpText">
        <p>Preferences selected in this section will be applied to all vehicles with CARFAX reports in Inventory.</p>
    </div>
    <div class="help_text" runat="server" id="AppraisalHelpText">
        <p>Preferences selected in this section will be applied to all vehicles with CARFAX reports in Appraisal.</p>
    </div>
    
    <asp:BulletedList ID="ErrorList" runat="server" Visible="false" CssClass="ErrorList">
    </asp:BulletedList>
    <asp:DetailsView ID="ReportPreferenceFormView" runat="server"
            AllowPaging="false"
            AutoGenerateDeleteButton="false"
            AutoGenerateEditButton="true"
            AutoGenerateInsertButton="false"
            AutoGenerateRows="false"
            DataKeyNames="DealerId,VehicleEntityType"
            DataSourceID="ReportPreferenceDataSource"
            DefaultMode="ReadOnly"
            OnModeChanged="ReportPreferenceFormView_ModeChanged"
            OnItemInserting="ReportPreferenceFormView_ItemInserting"
            OnItemInserted="ReportPreferenceFormView_ItemInserted"
            OnItemUpdating="ReportPreferenceFormView_ItemUpdating"
            OnItemUpdated="ReportPreferenceFormView_ItemUpdated">
        <Fields>            
            <asp:CheckBoxField AccessibleHeaderText="Automatically Purchase Report" HeaderText="Automatically Purchase Report" DataField="PurchaseReport" />
            <asp:CheckBoxField AccessibleHeaderText="Display In Hot List" HeaderText="Display In Hot List" DataField="DisplayInHotList" />
            <asp:TemplateField AccessibleHeaderText="Default Report Type" HeaderText="Default Report Type">
                <ItemTemplate>
                    <asp:RadioButtonList Enabled="false" ID="ReportType" runat="server" SelectedValue='<%# Bind("ReportType") %>'>
                        <asp:ListItem Text="Branded Title Check" Value="BTC" />
                        <asp:ListItem Text="Vehicle History Report" Value="VHR" />
                        <asp:ListItem Text="Consumer Information Pack" Value="CIP" />
                    </asp:RadioButtonList>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:RadioButtonList ID="ReportType" runat="server" SelectedValue='<%# Bind("ReportType") %>'>
                        <asp:ListItem Text="Branded Title Check" Value="BTC" />
                        <asp:ListItem Text="Vehicle History Report" Value="VHR" />
                        <asp:ListItem Text="Consumer Information Pack" Value="CIP" />
                    </asp:RadioButtonList>
                </EditItemTemplate>
            </asp:TemplateField>
        </Fields>
    </asp:DetailsView>
    <div runat="server" id="ReportPreferenceFormViewEditModeHelp" visible="false" class="help_text">
        <dl>                        
            <dt>Automatically Purchase Report:</dt>
            <dd>
                <ol>
                    <li>If <em>Automatically Purchase Report</em> is checked all vehicles in <asp:Literal runat="server" ID="VehicleEntityTypeLiteral3" /> will have CARFAX reports of the <em>Default Report Type</em> be ordered.</li>
                    <li>if <em>Automatically Purchase Reports</em> is <strong>NOT</strong> checked the <em>Default Report Type</em> will be reselected when ordering a report.</li>
                </ol>
            </dd>
            <dt>Display In Hot List:</dt>
            <dd>
                <ol>
                    <li>If <em>Display in Hot List</em> is checked all vehicles in <asp:Literal runat="server" ID="VehicleEntityTypeLiteral2" />
                    will display <em>CARFAX Consurmer Free Report</em> in your online listings</li>
                </ol>
            </dd>
        </dl>        
    </div>
</asp:Content>
