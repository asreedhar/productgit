<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Permissions.aspx.cs" Inherits="FirstLook.VehicleHistoryReport.WebApplication.Pages.Carfax.Permissions" MasterPageFile="~/Pages/Page.Master" %>

<%@ OutputCache Location="None" VaryByParam="None" %>

<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" ID="BodyContent" runat="server">
    <h2>Carfax Permissions</h2>
    <div class="help_text">
        <p>All users of <asp:Literal runat="server" ID="DealerNameLiteral" /> will be able to view available CARFAX reports. Users assigned below have permission to <strong>purchase</strong> CARFAX reports.</p>
    </div>
    <asp:ObjectDataSource ID="MemberDataSource" runat="server"
            TypeName="FirstLook.VehicleHistoryReport.DomainModel.Member+DataSource"
            SelectMethod="Select">
        <SelectParameters>
            <asp:QueryStringParameter Name="dealerId" QueryStringField="DealerId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <p id="NoAccountText" runat="server" visible="false">This dealer has no Account!</p>
    <p id="NoMembersText" runat="server" visible="false">This dealer has no members!</p>
    <asp:GridView ID="MemberGridView" runat="server"
            AutoGenerateColumns="false"
            AllowPaging="false"
            AllowSorting="false"
            DataKeyNames="Id"
            DataSourceID="MemberDataSource"
            OnLoad="MemberGridView_Load"
            OnDataBound="MemberGridView_DataBound"
            OnRowCommand="MemberGridView_RowCommand"
            OnRowDataBound="MemberGridView_RowDataBound">
        <Columns>
            <asp:BoundField ReadOnly="true" AccessibleHeaderText="First Name" HeaderText="First Name" DataField="FirstName" />
            <asp:BoundField ReadOnly="true" AccessibleHeaderText="Last Name" HeaderText="Last Name" DataField="LastName" />
            <asp:ButtonField AccessibleHeaderText="Permission" HeaderText="Permission" ButtonType="Button" CommandName="TogglePermissions" Text="--" />
        </Columns>
    </asp:GridView>
</asp:Content>
