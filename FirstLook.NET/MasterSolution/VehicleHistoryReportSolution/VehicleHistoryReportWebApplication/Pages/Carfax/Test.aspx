﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Test.aspx.cs" Inherits="FirstLook.VehicleHistoryReport.WebApplication.Pages.Carfax.Test" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Carfax - Test Page</title>
    <script src="../../Public/Scripts/Lib/LABjs-1.0.2rc1/LAB.js" type="text/javascript" charset="utf-8"></script>
</head>
<body>

    <div id="white"><div id="white_r"></div></div>

    <form id="CarfaxForm" runat="server">
    
    <div id="nav_header">
    </div>
    
    <div id="main_content" class="Wrapper">
        <div id="header" class="Navigation"><div id="header_r"><div id="header_l">
            <h1 class="title">Vehicle History Report</h1>
        </div></div></div>
        <div id="body" class="Content">
            <h2>CARFA<sub>X</sub> Test</h2>
            <fieldset id="example">
                <legend>Vehicle</legend>
                <table border="1">
                    <tbody>
                        <tr>
                            <th><label for="broker">Broker</label></th>
                            <td colspan="2"><input type="text" id="broker" name="broker" maxlength="36" /></td>
                            <td rowspan="2" style="vertical-align:middle"><input type="submit" id="See" name="See" value="See" /></td>
                        </tr>
                        <tr>
                            <th><label for="vin">VIN</label></th>
                            <td colspan="2"><input type="text" id="vin" name="vin" maxlength="17" /></td>
                        </tr>
                        <tr>
                            <th rowspan="3">Report Type</th>
                            <td><label for="report_type_1">BTC</label></td>
                            <td><input type="radio" id="report_type_1" name="report_type" value="1" checked="checked" /></td>
                            <td rowspan="7" style="vertical-align:middle"><input type="submit" id="Buy" name="Buy" value="Buy" /></td>
                        </tr>
                        <tr>
                            <td><label for="report_type_2">VHR</label></td>
                            <td><input type="radio" id="report_type_2" name="report_type" value="2" /></td>
                        </tr>
                        <tr>
                            <td><label for="report_type_3">CIP</label></td>
                            <td><input type="radio" id="report_type_3" name="report_type" value="3" /></td>
                        </tr>
                        <tr>
                            <th rowspan="2">Display In Hot List?</th>
                            <td><label for="display_in_hotlist_y">Yes</label></td>
                            <td><input type="radio" id="display_in_hotlist_y" name="display_in_hotlist" value="y" /></td>
                        </tr>
                        <tr>
                            <td><label for="display_in_hotlist_n">No</label></td>
                            <td><input type="radio" id="display_in_hotlist_n" name="display_in_hotlist" value="n" checked="checked" /></td>
                        </tr>
                        <tr>
                            <th rowspan="2">Vehicle Entity Type</th>
                            <td><label for="vehicle_entity_type_1">Inventory</label></td>
                            <td><input type="radio" id="vehicle_entity_type_1" name="vehicle_entity_type" value="1" checked="checked" /></td>
                        </tr>
                        <tr>
                            <td><label for="vehicle_entity_type_2">Appraisal</label></td>
                            <td><input type="radio" id="vehicle_entity_type_2" name="vehicle_entity_type" value="2" /></td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>
            <fieldset id="report">
                <legend>Report</legend>
                <div style="float: left; width: 45%;">
                    <h3>Summary</h3>
                    <dl>
                        <dt>Inventory</dt>
                        <dd id="x_inventory">?</dd>
                        <dt>Report</dt>
                        <dd id="x_report_type">?</dd>
                        <dt>Expiration</dt>
                        <dd id="x_expiration">?</dd>
                        <dt>Has Problem</dt>
                        <dd id="x_problem">?</dd>
                        <dt>Owner Count</dt>
                        <dd id="x_ownership">?</dd>
                    </dl>
                </div>
                <div style="float: left; width: 45%;">
                    <h3>Inspections</h3>
                    <dl>
                        <dt>1-Owner</dt>
                        <dd id="i_ownership">?</dd>
                        <dt>Buy Back Guarantee</dt>
                        <dd id="i_buy_back_guarantee">?</dd>
                        <dt>No Total Loss Reported</dt>
                        <dd id="i_total_loss">?</dd>
                        <dt>No Frame Damage Reported</dt>
                        <dd id="i_frame_damage">?</dd>
                        <dt>No Airbag Deployment Reported</dt>
                        <dd id="i_air_bag_deployment">?</dd>
                        <dt>No Odometer Rollback Reported</dt>
                        <dd id="i_rollback_odometer">?</dd>
                        <dt>No Accidents / Damage Reported</dt>
                        <dd id="i_accident_indicators">?</dd>
                        <dt>No Manufacturer Recalls Reported</dt>
                        <dd id="i_manufacturer_recall">?</dd>
                    </dl>
                </div>
            </fieldset>
        </div>
    </div>

    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/Lib/jquery/jquery-ui-1.8.2.custom/js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/Carfax/Application.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/Carfax/TestTemplates.js"></script>

    <script type="text/javascript" charset="utf-8">
        var _Carfax = new Carfax();
        _Carfax.main();

        if (typeof JSON === "undefined") {
            $LAB.script("../../Public/Scripts/Lib/json/json2.min.js");
        }
    </script>

    </form>
</body>
</html>
