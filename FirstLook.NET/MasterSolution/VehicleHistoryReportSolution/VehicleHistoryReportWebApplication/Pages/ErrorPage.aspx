<%@ Page Language="C#" AutoEventWireup="true" Codebehind="ErrorPage.aspx.cs" Inherits="FirstLook.VehicleHistoryReport.WebApplication.Pages.ErrorPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Vehicle History Report</title>
</head>
<body>
    <div id="white"><div id="white_r"></div></div>
    <form id="VehicleHistoryReportForm" runat="server">
        <div class="Wrapper">
        
            <div class="Navigation"><div id="header_r"><div id="header_l">
            
                <h1 class="title">Vehicle History Report</h1>
                
                <p class="DealerName"><asp:Label ID="DealerNameLabel" runat="server" Text="Dealer Not Specified" /></p>
                
                <p class="Shortcut"><asp:HyperLink ID="HomeLink" runat="server" NavigateUrl="/" Text="Home" /></p>
            
            </div></div></div>                       
                        
            <div class="Content">
            
                 <div id="menu">
                    <ul>
                        <li>Carfax
                            <ul>
                                <li><asp:HyperLink ID="CarfaxAccountLink" runat="server" NavigateUrl="~/Pages/Carfax/Account.aspx" Enabled="false">Account</asp:HyperLink></li>
                                <li><asp:HyperLink ID="CarfaxPermissionsLink" runat="server" NavigateUrl="~/Pages/Carfax/Permissions.aspx" Enabled="false">Permissions</asp:HyperLink></li>
                                <li>Report Preference
                                    <ul>
                                        <li><asp:HyperLink ID="CarfaxReportPreferenceLink_I" runat="server" NavigateUrl="~/Pages/Carfax/ReportPreference.aspx?VehicleEntityTypeId=1" Enabled="false">Inventory</asp:HyperLink></li>
                                        <li><asp:HyperLink ID="CarfaxReportPreferenceLink_A" runat="server" NavigateUrl="~/Pages/Carfax/ReportPreference.aspx?VehicleEntityTypeId=2" Enabled="false">Appraisal</asp:HyperLink></li>
                                    </ul>
                                </li>
                                <li><asp:HyperLink ID="CarfaxSystemAccountLink" runat="server" NavigateUrl="~/Pages/Carfax/SystemAccount.aspx">System Account</asp:HyperLink></li>
                            </ul>
                        </li>
                        <li>AutoCheck
                            <ul>
                                <li><asp:HyperLink ID="AutoCheckAccountLink" runat="server" NavigateUrl="~/Pages/AutoCheck/DealerAccount.aspx" Enabled="false">Account</asp:HyperLink></li>
                                <li><asp:HyperLink ID="AutoCheckPermissionsLink" runat="server" NavigateUrl="~/Pages/AutoCheck/Permissions.aspx" Enabled="false">Permissions</asp:HyperLink></li>
                                <li><asp:HyperLink ID="AutoCheckSystemAccountLink" runat="server" NavigateUrl="~/Pages/AutoCheck/SystemAccount.aspx">System Account</asp:HyperLink></li>
                                <li>Service Account
                                    <ul>
                                        <li><asp:HyperLink ID="AutoCheckServiceAccountLink_X" runat="server" NavigateUrl="~/Pages/AutoCheck/ServiceAccount.aspx?ServiceTypeId=1">XML Service Account</asp:HyperLink></li>
                                        <li><asp:HyperLink ID="AutoCheckServiceAccountLink_H" runat="server" NavigateUrl="~/Pages/AutoCheck/ServiceAccount.aspx?ServiceTypeId=2">HTML Service Account</asp:HyperLink></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="help_link">
                            <asp:HyperLink ID="VehicleHistoryReportHelp" runat="server" NavigateUrl="~/Pages/VehicleHistoryReportAdminHelp.aspx">Vehicle History Report Help</asp:HyperLink>
                        </li>
                    </ul>
                </div>
                
                <p>There has been an error in this application. Please contact the Firstlook Engineering team.</p>
                
            </div>
                                    
        </div>
    </form>
</body>
</html>
