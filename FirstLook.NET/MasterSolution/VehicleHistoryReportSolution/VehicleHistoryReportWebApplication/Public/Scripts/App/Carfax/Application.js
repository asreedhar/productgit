if (typeof(Carfax) !== "function") {
    var Carfax = function() {};
}

(function() {
    var ns = this;
    var State = {
        // subjects
        Broker: "",
        Vin: "",
        // report parameters
        DisplayInHotList: false,
        ReportType: 1,
        VehicleEntityType: 1,
        // results
        Report: null
    };

    /* dto */
    var CarfaxReportTypeDto = {
        "Undefined": 0,
        "BTC": 1,
        "VHR": 2,
        "CIP": 3
    };
    CarfaxReportTypeDto.toString = function(value) {
        for (var type in CarfaxReportTypeDto) {
            if (value === CarfaxReportTypeDto[type]) {
                return type;
            };
        }
        throw "Invalid Carfax Type";
    }

    var VehicleEntityTypeDto = {
        "Undefined": 0,
        "Inventory": 1,
        "Appraisal": 2
    };

    var CarfaxReportResultsStatusDto = {
        "Success": 0,
        "ReportNotPurchased": 1,
        "ReportNotAvailable": 2,
        "AccountNotAvailable": 3,
        "AccountNotOperable": 4,
        "NoPermission": 5,
        "ServiceUnavailable": 6
    };
    CarfaxReportResultsStatusDto.toString = function(value) {
        for (var type in CarfaxReportResultsStatusDto) {
            if (value === CarfaxReportResultsStatusDto[type]) {
                return type;
            };
        }
        throw "Invalid Carfax Status";
    }

    function CarfaxReportDto() {
        this.ExpirationDate = "";
        this.UserName = "";
        this.Vin = "";
        this.ReportType = 0;
        this.DisplayInHotList = false;
        this.HasProblem = false;
        this.OwnerCount = 0;
        this.Inspections = [];
    }

    function CarfaxReportInspectionDto() {
        this.Id = 0;
        this.Name = "";
        this.Selected = false;
    }

    /* envelopes dto */

    function CarfaxReportQueryArgumentsDto() {
        this.Broker = "";
        this.Vin = "";
        this.VehicleEntityType = 0;
        EventBinder(this);
    }
    CarfaxReportQueryArgumentsDto.prototype = {
        fromState: function() {
            this.Broker = State.Broker;
            this.Vin = State.Vin;
            this.VehicleEntityType = State.VehicleEntityType;
        }
    };

    function CarfaxReportQueryResultsDto() {
        this.Arguments = null;
        this.Status = 0;
        this.Report = null;
        this.Url = "";
        EventBinder(this);
    }

    function CarfaxReportPurchaseArgumentsDto() {
        this.Broker = "";
        this.Vin = "";
        this.VehicleEntityType = 0;
        this.ReportType = 1;
        this.DisplayInHotList = false;
        EventBinder(this);
    }
    CarfaxReportPurchaseArgumentsDto.prototype = {
        fromState: function() {
            this.Broker = State.Broker;
            this.Vin = State.Vin;
            this.VehicleEntityType = State.VehicleEntityType;
            this.ReportType = State.ReportType;
            this.DisplayInHotList = State.DisplayInHotList;
        }
    };

    function CarfaxReportPurchaseResultsDto() {
        this.Arguments = null;
        this.Status = 0;
        this.Report = null;
        this.Url = "";
        EventBinder(this);
    }

    /* event args */



    /* mapper */
    function genericMapper(type, config) {
        if (typeof config != "object") {
            config = {};
        }
        return function(json) {
            var result = new type(),
            prop,
            subJson,
            idx;
            for (prop in json) {
                if (!json.hasOwnProperty(prop)) continue; /// Ensure actual property
                subJson = json[prop];
                if (config.hasOwnProperty(prop)) {
                    if (subJson !== null && typeof subJson.length == "number" && typeof result[prop].push == "function") {
                        for (idx in subJson) {
                            if (!subJson.hasOwnProperty(idx)) continue;
                            result[prop].push(DataMapper[config[prop]](subJson[idx]));
                        }
                    } else {
                        result[prop] = DataMapper[config[prop]](subJson);
                    }
                } else if (typeof result[prop] !== "undefined") { // don't overwrite properties
                    result[prop] = subJson;
                }
            }
            return result;
        };
    }
    var DataMapper = {
        "CarfaxReportQueryArgumentsDto": genericMapper(CarfaxReportQueryArgumentsDto),
        "CarfaxReportQueryResultsDto": genericMapper(CarfaxReportQueryResultsDto, {
            "Arguments": "CarfaxReportQueryArgumentsDto",
            "Report": "CarfaxReportDto"
        }),
        "CarfaxReportPurchaseArgumentsDto": genericMapper(CarfaxReportPurchaseArgumentsDto),
        "CarfaxReportPurchaseResultsDto": genericMapper(CarfaxReportPurchaseResultsDto, {
            "Arguments": "CarfaxReportPurchaseArgumentsDto",
            "Report": "CarfaxReportDto"
        }),
        "CarfaxReportDto": genericMapper(CarfaxReportDto, {
            "Inspections": "CarfaxReportInspectionDto"
        }),
        "CarfaxReportInspectionDto": genericMapper(CarfaxReportInspectionDto)
    };

    function genericService(serviceUrl, resultType, getDataFunc) {
        return {
            AjaxSetup: {
                "url": serviceUrl
            },
            fetch: function() {
                var me = this;
                var ajaxSetup = $.extend(Services.Default.AjaxSetup, this.AjaxSetup);
                ajaxSetup.data = this.getData();
                function onFetchSuccess(json) {
                    var results = resultType.fromJSON(json.d);
                    $(me).trigger("fetchComplete", [results]);
                }
                ajaxSetup.success = onFetchSuccess;
                $.ajax(ajaxSetup);
            },
            getData: getDataFunc
        };
    }

    var Services = {
        "Default": {
            AjaxSetup: {
                type: "POST",
                dataType: "json",
                processData: false,
                contentType: "application/json; charset=utf-8",
                error: function(xhr, status, errorThrown) {
                    if (console) console.log(xhr, status, errorThrown);
                    if (xhr.status === 401) {
                        var response = JSON.parse(xhr.responseText);
                        if ( !! ! response['auth-login']) {
                            window.location.assign(response['auth-login']);
                        }
                    }
                }
            }
        },
        "Query": genericService("/VehicleHistoryReport/Services/Carfax.asmx/Query", CarfaxReportQueryResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes.V_2_0.CarfaxReportQueryArgumentsDto",
                    "Broker": this.Broker,
                    "Vin": this.Vin,
                    "VehicleEntityType": this.VehicleEntityType
                }
            });
        }),
        "Purchase": genericService("/VehicleHistoryReport/Services/Carfax.asmx/Purchase", CarfaxReportPurchaseResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes.V_2_0.CarfaxReportPurchaseArgumentsDto",
                    "Broker": this.Broker,
                    "Vin": this.Vin,
                    "VehicleEntityType": this.VehicleEntityType,
                    "ReportType": this.ReportType,
                    "DisplayInHotList": !! this.DisplayInHotList
                }
            });
        })
    };

    var Events = {
        "CarfaxReportQueryArgumentsDto": {
            fetchComplete: function(event, data) {
                if ( !! ! data) {
                    throw new Error("Missing Data");
                }
                // save report
                State.Report = data.Report;
                // update template
                $(Carfax).trigger("Carfax.ReportLoaded", data);
            },
            stateChange: function() {
                var args = new CarfaxReportQueryArgumentsDto();
                args.fromState();
                $(args).trigger("fetch"); // TODO: events need to be framework independent
            }
        },
        "CarfaxReportPurchaseArgumentsDto": {
            fetchComplete: function(event, data) {
                if ( !! ! data) {
                    throw new Error("Missing Data");
                }
                // save report
                State.Report = data.Report;
                // update template
                $(Carfax).trigger("Carfax.ReportLoaded", data);
                // var template = new ns.ReportTemplateBuilder(data);
                // template.clean();
                // template.init();
            },
            stateChange: function() {
                var purchaseArgs = new CarfaxReportPurchaseArgumentsDto();
                purchaseArgs.fromState();
                $(purchaseArgs).trigger("fetch"); // TODO: events need to be framework independent
            }
        },
        "CarfaxReportPurchaseResultsDto": {
            fetchComplete: function(event, data) {
                if ( !! ! data) {
                    throw new Error("Missing Data");
                }
                // save report
                State.Report = data.Report;
                // update template
                $(Carfax).trigger("Carfax.ReportLoaded", data);
            }
        }
    };

    var EventBinder = function(obj) {
        var type = EventBinder.getType(obj);
        if ( !! Events[type]) {
            var events = Events[type];
            for (var e in events) {
                $(obj).bind(e, events[e]);
            }
        }
    };
    EventBinder.map = {
        'CarfaxReportQueryArgumentsDto': CarfaxReportQueryArgumentsDto,
        'CarfaxReportPurchaseArgumentsDto': CarfaxReportPurchaseArgumentsDto,
        'CarfaxReportPurchaseResultsDto': CarfaxReportPurchaseResultsDto
    };
    EventBinder.getType = function(obj) {
        for (var type in EventBinder.map) {
            if (obj instanceof EventBinder.map[type]) {
                return type;
            }
        }
        return undefined;
    };

    // ====================
    // = Bind DataMappers =
    // ====================
    (function() {
        function BindDataMapper(target, source) {
            target.fromJSON = source;
        }

        BindDataMapper(CarfaxReportQueryArgumentsDto, DataMapper.CarfaxReportQueryArgumentsDto);
        BindDataMapper(CarfaxReportQueryResultsDto, DataMapper.CarfaxReportQueryResultsDto);

        BindDataMapper(CarfaxReportPurchaseArgumentsDto, DataMapper.CarfaxReportPurchaseArgumentsDto);
        BindDataMapper(CarfaxReportPurchaseResultsDto, DataMapper.CarfaxReportPurchaseResultsDto);

        BindDataMapper(CarfaxReportDto, DataMapper.CarfaxReportDto);
        BindDataMapper(CarfaxReportInspectionDto, DataMapper.CarfaxReportInspectionDto);

    })();

    // =======================
    // = Bind Service Fetchs =
    // =======================
    (function() {
        function BindFetch(target, source) {
            for (var m in source) {
                target.prototype[m] = source[m];
            }
            $(target).bind("fetch", target.prototype.fetch); // TODO: events need to be framework independent
        }

        BindFetch(CarfaxReportQueryArgumentsDto, Services.Query);
        BindFetch(CarfaxReportPurchaseArgumentsDto, Services.Purchase);

    })();

    /* ================== */
    /* = PUBLIC METHODS = */
    /* ================== */
    if (typeof this.prototype === "undefined") {
        this.prototype = {};
    }
    this.prototype.main = function(config) {
        if (typeof config == "undefined") {
            config = {};
        }
        if (config.broker) {
            $(Carfax).trigger("Carfax.BrokerChange", config.broker);
        }
        if (config.vin) {
            $(Carfax).trigger("Carfax.VinChange", config.vin);
        } else {
            $(Carfax).trigger("Carfax.Main");
        }
    };
    
    Carfax.raises = [] || Carfax.raises;
    Carfax.listensTo = [] || Carfax.listensTo;

    Carfax.raises.push("Carfax.BrokerChange");
    Carfax.raises.push("Carfax.VinChange");
    Carfax.raises.push("Carfax.Main");
    Carfax.raises.push("Carfax.ReportLoaded");

    Carfax.listensTo.push("Carfax.BrokerChange");
    Carfax.listensTo.push("Carfax.VinChange");
    Carfax.listensTo.push("Carfax.PurchaseAction");
    Carfax.listensTo.push("Carfax.QueryAction");
    Carfax.listensTo.push("Carfax.VehicleEntityType");
    Carfax.listensTo.push("Carfax.DisplayInHotList");
    Carfax.listensTo.push("Carfax.ReportTypeChange");

    var PublicEvents = {
        "Carfax.BrokerChange": function(evt, newBroker) {
            State.Broker = newBroker;
        },
        "Carfax.VinChange": function(evt, newVin) {
            State.Vin = newVin;
            if (State.Broker) {
                $(new CarfaxReportQueryArgumentsDto()).trigger("stateChange");
            }
        },
        "Carfax.ReportTypeChange": function(evt, newType) {
            State.ReportType = newType;
        },
        "Carfax.DisplayInHotListChange": function(evt, inHotList) {
            State.DisplayInHotList = !! inHotList;
        },
        "Carfax.VehicleEntityTypeChange": function(evt, newVehicleEntityType) {
            State.VehicleEntityType = newVehicleEntityType;
        },
        "Carfax.PurchaseAction": function(evt) {
            $(new CarfaxReportPurchaseArgumentsDto()).trigger("stateChange");
        },
        "Carfax.QueryAction": function(evt) {
            $(new CarfaxReportQueryArgumentsDto()).trigger("stateChange");
        }
    }
    $(Carfax).bind(PublicEvents);

    /* ================== */
    /* = PUBLIC METHODS = */
    /* ================== */
    this.FormatTypeAsString = CarfaxReportTypeDto.toString;
    this.FormatStatusAsString = CarfaxReportResultsStatusDto.toString;
}).apply(Carfax);

