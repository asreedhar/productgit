if (typeof(Carfax) !== "function") {
	var Carfax = function() {};
}
(function() {

    var VehicleTemplateBuilder = function() {
        this.$dom = {
            Vehicle$Broker: $('#broker'),
            Vehicle$Vin: $('#vin'),
            Vehicle$ReportType: $('input[type="radio"][name="report_type"]'),
            Vehicle$DisplayInHotList: $('input[type="radio"][name="display_in_hotlist"]'),
            Vehicle$VehicleEntityType: $('input[type="radio"][name="vehicle_entity_type"]'),
            Action$Purchase: $('#Buy'),
            Action$Query: $('#See'),
            Action$Form: $('#CarfaxForm')
        };
    };

    VehicleTemplateBuilder.prototype = {
        init: function() {
            this.build();
        },
        build: function() {
            this.$dom.Vehicle$Broker.bind('change', this.changeBroker);
            this.$dom.Vehicle$Vin.bind('change', this.changeVin);
            this.$dom.Vehicle$ReportType.bind('change', this.changeReportType);
            this.$dom.Vehicle$DisplayInHotList.bind('change', this.changeDisplayInHotList);
            this.$dom.Vehicle$VehicleEntityType.bind('change', this.changeVehicleEntityType);
            this.$dom.Action$Purchase.bind('click', this.actionPurchase);
            this.$dom.Action$Query.bind('click', this.actionQuery);
            this.$dom.Action$Form.bind('submit', this.actionCancel);
        },
        clean: function() {
            this.$dom.Vehicle$Broker.unbind('change').html('');
            this.$dom.Vehicle$Vin.unbind('change').html('');
            this.$dom.Vehicle$ReportType.unbind('change');
            this.$dom.Vehicle$DisplayInHotList.unbind('change');
            this.$dom.Vehicle$VehicleEntityType.unbind('change');
            this.$dom.Action$Purchase.unbind('click');
            this.$dom.Action$Query.unbind('click');
            this.$dom.Action$Form.unbind('submit');
        },
        changeBroker: function(evt) {
            var newState = new Carfax.BrokerChange(evt.target.value);
            $(newState).trigger("set");
        },
        changeVin: function(evt) {
            var newState = new Carfax.VinChange(evt.target.value);
            $(newState).trigger("set");
        },
        changeReportType: function(evt) {
            var newState = new Carfax.ReportTypeChange(evt.target.value);
            $(newState).trigger("set");
        },
        changeDisplayInHotList: function(evt) {
            var newState = new Carfax.DisplayInHotListChange(evt.target.value);
            $(newState).trigger("set");
        },
        changeVehicleEntityType: function(evt) {
            var newState = new Carfax.VehicleEntityTypeChange(evt.target.value);
            $(newState).trigger("set");
        },
        actionPurchase: function(evt) {
            // purchase
            var newState = new Carfax.PurchaseAction();
            $(newState).trigger("set");
            // cancel submit
            evt.preventDefault();
            return false;
        },
        actionQuery: function(evt) {
            // query
            var newState = new Carfax.QueryAction();
            $(newState).trigger("set");
            // cancel submit
            evt.preventDefault();
            return false;
        },
        actionCancel: function(evt) {
            // cancel submit
            evt.preventDefault();
            return false;
        }
    };

    var ReportTemplateBuilder = function(data) {
        this.data = data;
        this.dom$s = {
            'inventory': $('#x_inventory'),
            'report_type': $('#x_report_type'),
            'expiration': $('#x_expiration'),
            'problem': $('#x_problem'),
            'ownership': $('#x_ownership')
        };
        this.dom$i = {
            'ownership': $('#i_ownership'),
            'buy_back_guarantee': $('#i_buy_back_guarantee'),
            'total_loss': $('#total_loss'),
            'frame_damage': $('#i_frame_damage'),
            'air_bag_deployment': $('#i_air_bag_deployment'),
            'rollback_odometer': $('#i_rollback_odometer'),
            'accident_indicators': $('#i_accident_indicators'),
            'manufacturer_recall': $('#i_manufacturer_recall')
        };
    };

    ReportTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            var report = this.data.Report;
            if (report !== null) {
                // summary
                // this.dom$s.inventory.html('?');
                this.dom$s.report_type.html(report.ReportType);
                this.dom$s.expiration.html(report.ExpirationDate);
                this.dom$s.problem.html(report.HasProblem);
                this.dom$s.ownership.html(report.OwnerCount);
                // inspections
                var inspections = report.Inspections;
                for (var i = 0; i < inspections.length; i++) {
                    var inspection = inspections[i];
                    var selected = inspection.Selected ? "y" : "no";
                    if (report.ReportType == 1) {
                        selected = "N/A (BTC)";
                    }
                    switch (inspection.Id) {
                        case 1:
                            this.dom$i.ownership.html(selected);
                            break;
                        case 2:
                            this.dom$i.buy_back_guarantee.html(selected);
                            break;
                        case 3:
                            this.dom$i.total_loss.html(selected);
                            break;
                        case 4:
                            this.dom$i.frame_damage.html(selected);
                            break;
                        case 5:
                            this.dom$i.air_bag_deployment.html(selected);
                            break;
                        case 6:
                            this.dom$i.rollback_odometer.html(selected);
                            break;
                        case 7:
                            this.dom$i.accident_indicators.html(selected);
                            break;
                        case 8:
                            this.dom$i.manufacturer_recall.html(selected);
                            break;
                    }
                }
            }
        },
        clean: function() {
            // summary
            this.dom$s.inventory.html('?');
            this.dom$s.report_type.html('?');
            this.dom$s.expiration.html('?');
            this.dom$s.problem.html('?');
            this.dom$s.ownership.html('?');
            // inspections
            this.dom$i.ownership.html('?');
            this.dom$i.buy_back_guarantee.html('?');
            this.dom$i.total_loss.html('?');
            this.dom$i.frame_damage.html('?');
            this.dom$i.air_bag_deployment.html('?');
            this.dom$i.rollback_odometer.html('?');
            this.dom$i.accident_indicators.html('?');
            this.dom$i.manufacturer_recall.html('?');
        }
    };

    this.VehicleTemplateBuilder = VehicleTemplateBuilder;
    this.ReportTemplateBuilder = ReportTemplateBuilder;

}).apply(Carfax);
