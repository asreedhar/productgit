if (typeof Carfax !== "function") {
    var Carfax = function() {};
}

(function() {
    var ns = this;
    var ns_string = "Carfax";

    Ext.regModel(ns_string + "Report", {
        fields: ["DisplayInHotList", "ExpirationDate", "HasProblem", "Inspections", "OwnerCount", "ReportType", "Vin"]
    });
    Ext.regModel(ns_string + "Inspections", {
        fields: ["Id", "Name", "Selected"]
    });

    // ************************************
    // VehicleTemplateBuilder
    // ************************************
    function VehicleTemplateBuilder(data) {
        this.data = data;
    }
    VehicleTemplateBuilder.prototype = {
        init: function() {},
        build: function() {},
        clean: function() {}
    };

    // ************************************
    // ReportTemplateBuilder
    // ************************************
    var reportStore = {}; // Single object the closure to help hold state
    var inspectionStore = {}; // Single object the closure to help hold state
    var inspectionList = {}; // Single object the closure to help hold state
    var reportList = {}; // Single object the closure to help hold state
    var reportStatusDisplay = {};
    var reportOrder = {};
    function ReportTemplateBuilder(data) {
        this.data = data;
    }
    ReportTemplateBuilder.prototype = {
        init: function() {
            $(ns).trigger("Carfax.ReportTemplateBuilder.init", [this.data]);
            if (this.data) {
                inspectionStore = new Ext.data.JsonStore({
                    model: ns_string + "Inspections",
                    data: this.data.Report.Inspections
                });

                this.build();
            }
        },
        build: function() {
            if (this.data.Status === 0) {
                reportTypeDisplay = new Ext.Container({
                    html: "<p class='carfax-report-type'>" + ns.FormatTypeAsString(this.data.Report.ReportType) + " Report</p>"
                });

                reportList = new Ext.form.FormPanel({
                    items: [{
                        xtype: "fieldset",
                        defaults: {
                            xtype: "textfield",
                            labelWidth: "35%",
                            listeners: {
                                focus: function(comp) {
                                    comp.blur();
                                }
                            }
                        },
                        items: [{
                            name: "ExpirationDate",
                            label: "Expires",
                            value: Ext.util.Format.asmxDate(this.data.Report.ExpirationDate)
                        },
                        // {
                        //     name: "DisplayInHotList",
                        //     label: "In Hot List",
                        //     value: this.data.Report.DisplayInHotList ? "Displayed" : "Not Displayed"
                        // },
                        {
                            name: "OwnerCount",
                            label: "Owners",
                            value: this.data.Report.OwnerCount
                        }
                        //, {
                        //    html: '<a href="'+this.data.Url+'">Report Link</a>'
                        //}
                        ]
                    }]
                });

                inspectionList = new Ext.List({
                    scroll: false,
                    disableSelection: true,
                    id: "fl-carfax-inspections",
                    cls: 'carfax_inspection_items',
                    grouped: false,
                    itemTpl: "<span class='carfax-report-item selected-{Selected}'>{Name}</span>",
                    emptyText: "No Inspections for Report",
                    store: inspectionStore
                });

                ns.render({
                    xtype: "panel",
                    scroll: true,
                    dockedItems: [reportTypeDisplay],
                    items: [reportList, inspectionList]
                });
            } else if (this.data.Status === 1) {
                reportOrder = new Ext.form.FormPanel({
                    items: [{
                        xtype: "fieldset",
                        defaults: {
                            xtype: "textfield",
                            labelwidth: "35%"
                        },
                        items: [{
                            name: "ReportType",
                            xtype: "selectfield",
                            label: "Type",
                            options: [{
                                text:"Branded Title Check", value: 1
                            },{
                                text:"Vehicle History Report", value: 2
                            },{
                                text:"Consumer Information Pack", value: 3
                            }]
                        }
                        // , {
                        //     name: "DisplayInHotList",
                        //     label: "Hotlist?",
                        //     xtype: "togglefield"
                        // }
                        ]
                    }, {
                        xtype: "button",
                        text: "Order Report",
                        listeners: {
                            tap: function() {
                                this.up('panel').submit()
                            }
                        }
                    }],
                    listeners: {
                        scope: this,
                        beforesubmit: function(comp, values, options) {
                            $(new ns.ReportTypeChange(values.ReportType)).trigger("set");
                            $(new ns.DisplayInHotListChange(values.DisplayInHotListChange)).trigger("set");

                            $(new ns.PurchaseAction()).trigger("set");

                            return false; 
                        }
                    }
                });

                ns.render(reportOrder);
            } else {
                reportStatusDisplay = new Ext.Container({
                    html: "<p class='report-status'>" + ns.FormatStatusAsString(this.data.Status) + "</p>"
                });
                ns.render(reportStatusDisplay);
            }

        },
        clean: function() {}
    };

    this.VehicleTemplateBuilder = VehicleTemplateBuilder;
    this.ReportTemplateBuilder = ReportTemplateBuilder;
}).apply(Carfax);

