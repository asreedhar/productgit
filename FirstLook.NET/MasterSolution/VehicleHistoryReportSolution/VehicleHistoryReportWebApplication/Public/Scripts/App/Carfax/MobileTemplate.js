if (typeof(Carfax) !== "function") {
    var Carfax = function() {};
}
(function() {
    Carfax.raises = [] || Carfax.raises;
    Carfax.listensTo = [] || Carfax.listensTo;

    Carfax.raises.push("Carfax.OrderingReport");
    Carfax.raises.push("Carfax.ReportTypeChange");
    Carfax.raises.push("Carfax.PurchaseAction");

    Carfax.listensTo.push("Carfax.ReportLoaded");
    Carfax.listensTo.push("Carfax.OrderingReport");
    
    var PublicEvents = {
        "Carfax.ReportLoaded": function(evt, data) {
            var card = FirstLook.cards.get("Carfax");
            card.build(data);
        },
        "Carfax.OrderingReport": function(evt, data) {
            $(Carfax).trigger("Carfax.ReportTypeChange", parseInt(data.report_type, 10));
            $(Carfax).trigger("Carfax.PurchaseAction");
        }
    };
    $(Carfax).bind(PublicEvents);

    var card = {
        title: "Carfax",
        pattern: {
            vin: _.isString,
            buid: _.isString
        },
        view: {
            report: {
                order: 0,
                pattern: {
                    Report: _,
                    Status: function(v) { return v === 0; }
                },
                title: {
                    type: "Static",
                    pattern: _,
                    template: function(data) {
                        return "<h2>Carfax</h2>";
                    }
                },
                summary: {
                    type: "Static",
                    pattern: {
                        Report: {
                            ExpirationDate: _,
                            OwnerCount: _.isNumber
                        }
                    },
                    template: function(data) {
                        var temp = _.template([
                        "<fieldset><p class='field'>",
                        "<label>Expiration</label>",
                        "<input type='text' readonly='true' value='<%= _.asmxDate(Report.ExpirationDate) %>' />",
                        "</p><p class='field'>",
                        "<label>Owner Count</label>",
                        "<input type='text' readonly='true' value='<%= Report.OwnerCount %>' />",
                        "</p></fieldset>"
                        ].join(" "));
                        return temp(data);
                    }
                },
                link: {
                    type: "Static",
                    pattern: {
                        Url: _
                    },
                    template: function(data) {
                        var d = {
                            Url: data.Url.replace("http","browse")
                        }
                        return _.template("<p class='report_link'> <a href='<%= Url %>'>View Full Report</a> </p>", d);
                    }
                },
                items: {
                    type: "List",
                    pattern: {
                        Report: {
                            Inspections: [{
                                Id: _,
                                Name: _.isString,
                                Selected: _.isBool
                            }]
                        }
                    },
                    class_name: "inspection_list",
                    item_accessor: "Report.Inspections",
                    text_accessor: "Name",
                    data_accessor: "Id",
                    item_class_name_accessor: "'Selected'"
                }
            },
            order_report: {
                order: 1,
                pattern: {
                    Status: function(v) { return v === 1; }
                },
                order_form: {
                    type: "Fieldset",
                    pattern: _,
                    behaviors: {
                        submitable: {
                            event: "Carfax.OrderingReport",
                            scope: Carfax,
                            submit_on_change: true
                        }
                    },
                    class_name: "vhr_fieldset",
                    legend_accessor: "legend",
                    input_accessor: "inputs",
                    name_accessor: "name",
                    button_accessor: "buttons",
                    label_accessor: "label",
                    value_accessor: "data",
                    type_accessor: "type",
                    static_data: {
                        legend: "Order Carfax",
                        inputs: [{
                            name: "report_type",
                            label: "Type",
                            options: [{
                                name: "Select ...",
                                value: ""
                            }, {
                                name: "Branded Title Check",
                                value: "1"
                            }, {
                                name: "Vehicle History Report",
                                value: "2"
                            }, {
                                name: "Consumer Info Pack",
                                value: "3"
                            }]
                        }],
                        buttons: ["Order"]
                    }
                }
            },
            report_issues:{
                order: 2,
                pattern: {
                    Status: function(v) { return v === 2 || v === 6; }
                },
                title: {
                    type: "Static",
                    pattern: _,
                    template: function(data) {
                        var t = "<h2>Carfax Report Problems</h2>";
                        t += "<p class='error'>";
                        if (data.Status === 2) {
                            t += "Report Not Available";
                        } else if (data.Status === 6) {
                            t += "Carfax Service Unavailable";
                        }
                        t += "</p>";
                        return t;
                    }
                }
            },
            account_issues:{
                order: 3,
                pattern: {
                    Status: function(v) {  return _.include([3,4,5], v); }
                },
                title: {
                    type: "Static",
                    pattern: _,
                    template: function(data) {
                        var t = "<h2>Carfax Account Issues</h2>";
                        t += "<p class='error'>";
                        if (data.Status === 3) {
                            t += "Account Not Available";
                        } else if (data.Status === 4) {
                            t += "Account Not Operable";
                        } else if (data.Status === 5) {
                            t += "No Permission";
                        }
                        t += "</p>";
                        return t;
                    }
                }
            },
            error: {
                pattern: {
                    errorType: _,
                    errorResponse: _
                },
                ErrorMessage: {
                    type: "Static",
                    pattern: {
                        errorType: _.isString,
                        errorResponse: {
                            responseText: _.isString
                        }
                    },
                    template: function(data) {
                        return "<p class='error'>"+data.message+"</p>";
                    },
                    filter: function(data) {
                        var message = JSON.parse(data.errorResponse.responseText);
                        
                        if (_.isString(message.Message)) {
                            message = message.Message;
                        } else {
                            message = data.errorType;
                        }
                        data.message = message;
                        return data;
                    }
                }
            }
        }
    };
    FirstLook.cards.add(card, "Carfax");

    var carfaxOverview = {
        title: "CarfaxOverview",
        pattern: {
            vin: _.isString,
            buid: _.isString
        },
        view: {
            report: {
                pattern: {
                    Report: _,
                    Status: function(v) { return v === 0; }
                },
                title: {
                    type: "Static",
                    pattern: _,
                    template: function(data) {
                          return "<h2 class='carfax-overview-type'>";
                    }
                },
                items: {
                    type: "List",
                    pattern: {
                        Report: {
                            Inspections: [{
                                Id: _,
                                Name: _.isString,
                                Selected: _.isBool
                            }]
                        }
                    },
                    class_name: "inspection_list",
                    item_accessor: "Report.Inspections",
                    text_accessor: "Name",
                    data_accessor: "Id",
                    item_class_name_accessor: "'Selected'"
                }
            }
        }
    };
    FirstLook.cards.add(carfaxOverview, "CarfaxOverview");

}).apply(Carfax);

