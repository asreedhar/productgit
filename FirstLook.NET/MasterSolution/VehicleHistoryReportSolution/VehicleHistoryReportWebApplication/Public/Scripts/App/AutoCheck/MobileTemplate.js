if (typeof(AutoCheck) !== "function") {
	var AutoCheck = function() {};
}
(function(){
    AutoCheck.raises = [] || AutoCheck.raises;
    AutoCheck.listensTo = [] || AutoCheck.listensTo;

    AutoCheck.raises.push("AutoCheck.Purchase");

    AutoCheck.listensTo.push("AutoCheck.ReportLoaded");

    var PublicEvents = {
        "AutoCheck.ReportLoaded": function(evt, data) {
            var card = FirstLook.cards.get("AutoCheck");
            card.build(data);
        }
    };
    $(AutoCheck).bind(PublicEvents);

    var card = {
        title: "AutoCheck",
        pattern: {
            vin: _.isString,
            buid: _.isString
        },
        view: {
            report: {
                order: 0,
                pattern: {
                    Status: function(v) { return v === 0; }
                },
                title: {
                    type: "Static",
                    pattern: _,
                    template: function(data) {
                        return "<h2>AutoCheck</h2>";
                    }
                },
                summary: {
                    type: "Static",
                    pattern: {
                        Report: {
                            ExpirationDate: _,
                            OwnerCount: _.isNumber
                        }
                    },
                    template: function(data) {
                        var temp = _.template([
                        "<fieldset><p class='field'>",
                        "<label>Score</label>",
                        "<input type='text' readonly='true' value='<%= Report.Score %>' />",
                        "</p><p class='field'>",
                        "<label>Similar Score</label>",
                        "<input type='text' readonly='true' value='<%= Report.CompareScoreRangeHigh %> - <%= Report.CompareScoreRangeLow %>' />",
                        "</p><p class='field'>",
                        "<label>Owner Count</label>",
                        "<input type='text' readonly='true' value='<%= Report.OwnerCount %>' />",
                        "</p><p class='field'>",
                        "</p></fieldset>"
                        ].join(" "));
                        return temp(data);
                    }
                },
                items: {
                    type: "List",
                    pattern: {
                        Report: {
                            Inspections: [{
                                Id: _,
                                Name: _.isString,
                                Selected: _.isBool
                            }]
                        }
                    },
                    class_name: "inspection_list",
                    item_accessor: "Report.Inspections",
                    text_accessor: "Name",
                    data_accessor: "Id",
                    item_class_name_accessor: "'Selected'"
                }
            },
            order_report: {
                order: 1,
                pattern: {
                    Status: function(v) { return v === 1; }
                },
                order_form: {
                    type: "Fieldset",
                    pattern: _,
                    behaviors: {
                        submitable: {
                            event: "AutoCheck.Purchase",
                            scope: AutoCheck
                        }
                    },
                    class_name: "vhr_fieldset",
                    legend_accessor: "legend",
                    input_accessor: "inputs",
                    name_accessor: "name",
                    button_accessor: "buttons",
                    label_accessor: "label",
                    value_accessor: "data",
                    type_accessor: "type",
                    static_data: {
                        legend: "Order AutoCheck",
                        inputs: [],
                        buttons: ["Order Report"]
                    }
                }
            },
            report_issues:{
                order: 2,
                pattern: {
                    Status: function(v) { return v === 2 || v === 6; }
                },
                title: {
                    type: "Static",
                    pattern: _,
                    template: function(data) {
                        var t = "<h2>AutoCheck Report Problems</h2>";
                        t += "<p class='error'>";
                        if (data.Status === 2) {
                            t += "Report Not Available";
                        } else if (data.Status === 6) {
                            t += "AutoCheck Service Unavailable";
                        }
                        t += "</p>";
                        return t;
                    }
                }
            },
            account_issues:{
                order: 3,
                pattern: {
                    Status: function(v) {  return _.include([3,4,5], v); }
                },
                title: {
                    type: "Static",
                    pattern: _,
                    template: function(data) {
                        var t = "<h2>AutoCheck Account Issues</h2>";
                        t += "<p class='error'>";
                        if (data.Status === 3) {
                            t += "Account Not Available";
                        } else if (data.Status === 4) {
                            t += "Account Not Operable";
                        } else if (data.Status === 5) {
                            t += "No Permission";
                        }
                        t += "</p>";
                        return t;
                    }
                }
            },
            error: {
                pattern: {
                    errorType: _,
                    errorResponse: _
                },
                ErrorMessage: {
                    type: "Static",
                    pattern: {
                        errorType: _.isString,
                        errorResponse: {
                            responseText: _.isString
                        }
                    },
                    template: function(data) {
                        return "<p class='error'>"+data.message+"</p>";
                    },
                    filter: function(data) {
                        var message = JSON.parse(data.errorResponse.responseText);
                        
                        if (_.isString(message.Message)) {
                            message = message.Message;
                        } else {
                            message = data.errorType;
                        }
                        data.message = message;
                        return data;
                    }
                }
            }
        }
    };
    FirstLook.cards.add(card, "AutoCheck");

    var autoCheckOverview = {
        title: "AutoCheckOverview",
        pattern: {
            vin: _.isString,
            buid: _.isString
        },
        view: {
            report: {
                pattern: {
                    Status: function(v) { return v === 0; }
                },
                title: {
                    type: "Static",
                    pattern: _,
                    template: function(data) {
                          return "<h2 class='autocheck-overview-type'>";
                    }
                },
                items: {
                    type: "List",
                    pattern: {
                        Report: {
                            Inspections: [{
                                Id: _,
                                Name: _.isString,
                                Selected: _.isBool
                            }]
                        }
                    },
                    class_name: "inspection_list",
                    item_accessor: "Report.Inspections",
                    text_accessor: "Name",
                    data_accessor: "Id",
                    item_class_name_accessor: "'Selected'"
                }
            }
        }
    };
    FirstLook.cards.add(autoCheckOverview, "AutoCheckOverview");

}).apply(AutoCheck);
