if (typeof(AutoCheck) !== "function") {
	var AutoCheck = function() {};
}

(function() {
	var ns = this;
	var State = {
		// subjects
		Broker: "",
		Vin: "",
		// results
		Report: null,
		// helper methods
		AsSee: function() {
			var args = new AutoCheckReportArgumentsDtoQ();
			args.Broker = State.Broker;
			args.Vin = State.Vin;
			return args;
		},
		AsBuy: function() {
			var args = new AutoCheckReportArgumentsDtoP();
			args.Broker = State.Broker;
			args.Vin = State.Vin;
			return args;
		}
	};

	/* dto */

	var AutoCheckReportResultsStatusDto = {
		"Success": 0,
		"ReportNotPurchased": 1,
		"ReportNotAvailable": 2,
		"AccountNotAvailable": 3,
		"AccountNotOperable": 4,
		"NoPermission": 5,
		"ServiceUnavailable": 6
	};
	AutoCheckReportResultsStatusDto.toString = function(value) {
		for (var type in AutoCheckReportResultsStatusDto) {
			if (value === AutoCheckReportResultsStatusDto[type]) {
				return type;
			};
		}
		throw "Invalid AutoCheck Type";
	}

	function AutoCheckReportDto() {
		this.ExpirationDate = "";
		this.UserName = "";
		this.Vin = "";
		this.OwnerCount = 0;
		this.Score = 0;
		this.CompareScoreRangeLow = 0;
		this.CompareScoreRangeHigh = 0;
		this.Inspections = [];
	}

	function AutoCheckReportInspectionDto() {
		this.Id = 0;
		this.Name = "";
		this.Selected = false;
	}

	/* envelopes dto */

	function AutoCheckReportArgumentsDtoP() {
		this.Broker = "";
		this.Vin = "";
		EventBinder(this);
	}

	function AutoCheckReportArgumentsDtoQ() {
		this.Broker = "";
		this.Vin = "";
		EventBinder(this);
	}

	function AutoCheckReportResultsDto() {
		this.Arguments = null;
		this.Status = 0;
		this.Report = null;
		EventBinder(this);
	}

	/* mapper */
	function genericMapper(type, config) {
		if (typeof config != "object") {
			config = {};
		}
		return function(json) {
			var result = new type(),
			prop,
			subJson,
			idx;
			for (prop in json) {
				if (!json.hasOwnProperty(prop)) continue; /// Ensure actual property
				subJson = json[prop];
				if (config.hasOwnProperty(prop)) {
					if (subJson !== null && typeof subJson.length == "number" && typeof result[prop].push == "function") {
						for (idx in subJson) {
							if (!subJson.hasOwnProperty(idx)) continue;
							result[prop].push(DataMapper[config[prop]](subJson[idx]));
						}
					} else {
						result[prop] = DataMapper[config[prop]](subJson);
					}
				} else if (typeof result[prop] !== "undefined") { // don't overwrite properties
					result[prop] = subJson;
				}
			}
			return result;
		};
	}
	var DataMapper = {
		"AutoCheckReportArgumentsDto": genericMapper(AutoCheckReportArgumentsDtoQ),
		"AutoCheckReportResultsDto": genericMapper(AutoCheckReportResultsDto, {
			"Arguments": "AutoCheckReportArgumentsDto",
			"Report": "AutoCheckReportDto"
		}),
		"AutoCheckReportDto": genericMapper(AutoCheckReportDto, {
			"Inspections": "AutoCheckReportInspectionDto"
		}),
		"AutoCheckReportInspectionDto": genericMapper(AutoCheckReportInspectionDto)
	};

	function genericService(serviceUrl, resultType, getDataFunc) {
		return {
			AjaxSetup: {
				"url": serviceUrl
			},
			fetch: function() {
				var me = this;
				var ajaxSetup = $.extend(Services.Default.AjaxSetup, this.AjaxSetup);
				ajaxSetup.data = this.getData();
				function onFetchSuccess(json) {
					var results = resultType.fromJSON(json.d);
					$(me).trigger("fetchComplete", [results]);
				}
				ajaxSetup.success = onFetchSuccess;
				$.ajax(ajaxSetup);
			},
			getData: getDataFunc
		};
	}
	var Services = {
		"Default": {
			AjaxSetup: {
				type: "POST",
				dataType: "json",
				processData: false,
				contentType: "application/json; charset=utf-8",
				error: function(xhr, status, errorThrown) {
					if (console) console.log(xhr, status, errorThrown);
					if (xhr.status === 401) {
						var response = JSON.parse(xhr.responseText);
						if ( !! ! response['auth-login']) {
							window.location.assign(response['auth-login']);
						}
					}
				}
			}
		},
		"Query": genericService("/VehicleHistoryReport/Services/AutoCheck.asmx/Query", AutoCheckReportResultsDto, function() {
			return JSON.stringify({
				"arguments": {
					"__type": "FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects.Envelopes.V_2_0.AutoCheckReportArgumentsDto",
					"Broker": this.Broker,
					"Vin": this.Vin
				}
			});
		}),
		"Purchase": genericService("/VehicleHistoryReport/Services/AutoCheck.asmx/Purchase", AutoCheckReportResultsDto, function() {
			return JSON.stringify({
				"arguments": {
					"__type": "FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects.Envelopes.V_2_0.AutoCheckReportArgumentsDto",
					"Broker": this.Broker,
					"Vin": this.Vin
				}
			});
		})
	};

	var Events = {
		"AutoCheckReportArgumentsDtoQ": {
			fetchComplete: function(event, data) {
				if ( !! ! data) {
					throw new Error("Missing Data");
				}
				// save report
				State.Report = data.Report;
				// update template
				$(AutoCheck).trigger("AutoCheck.ReportLoaded", [data]);
			},
			stateChange: function() {
				$(State.AsSee()).trigger("fetch"); // TODO: events need to be framework independent
			}
		},
		"AutoCheckReportArgumentsDtoP": {
			fetchComplete: function(event, data) {
				if ( !! ! data) {
					throw new Error("Missing Data");
				}
				// save report
				State.Report = data.Report;
				// update template
				$(AutoCheck).trigger("AutoCheck.ReportLoaded", [data]);
			},
			stateChange: function() {
				$(State.AsBuy()).trigger("fetch"); // TODO: events need to be framework independent
			}
		}
	};

	var EventBinder = function(obj) {
		var type = EventBinder.getType(obj);
		if ( !! Events[type]) {
			var events = Events[type];
			for (var e in events) {
				$(obj).bind(e, events[e]); // TODO: events need to be framework independent
			}
		}
	};
	EventBinder.map = {
		'AutoCheckReportArgumentsDtoP': AutoCheckReportArgumentsDtoP,
		'AutoCheckReportArgumentsDtoQ': AutoCheckReportArgumentsDtoQ
	};
	EventBinder.getType = function(obj) {
		for (var type in EventBinder.map) {
			if (obj instanceof EventBinder.map[type]) {
				return type;
			}
		}
		return undefined;
	};

	// ====================
	// = Bind DataMappers =
	// ====================
	(function() {
		function BindDataMapper(target, source) {
			target.fromJSON = source;
		}

		BindDataMapper(AutoCheckReportArgumentsDtoP, DataMapper.AutoCheckReportArgumentsDto);
		BindDataMapper(AutoCheckReportArgumentsDtoQ, DataMapper.AutoCheckReportArgumentsDto);
		BindDataMapper(AutoCheckReportResultsDto, DataMapper.AutoCheckReportResultsDto);

		BindDataMapper(AutoCheckReportDto, DataMapper.AutoCheckReportDto);
		BindDataMapper(AutoCheckReportInspectionDto, DataMapper.AutoCheckReportInspectionDto);

	})();

	// =======================
	// = Bind Service Fetchs =
	// =======================
	(function() {
		function BindFetch(target, source) {
			for (var m in source) {
				target.prototype[m] = source[m];
			}
			$(target).bind("fetch", target.prototype.fetch); // TODO: events need to be framework independent
		}

		BindFetch(AutoCheckReportArgumentsDtoQ, Services.Query);

		BindFetch(AutoCheckReportArgumentsDtoP, Services.Purchase);

	})();

	/* ================== */
	/* = PUBLIC METHODS = */
	/* ================== */
	if (typeof this.prototype === "undefined") {
		this.prototype = {};
	}
	this.prototype.main = function(config) {
		if (typeof config == "undefined") {
			config = {};
		}
		if (config.broker) {
			$(AutoCheck).trigger("AutoCheck.BrokerChange", config.broker);
		}
		if (config.vin) {
			$(AutoCheck).trigger("AutoCheck.VinChange", config.vin);
		} else {
			$(AutoCheck).trigger("AutoCheck.LoadVehicleTemplate");
		}
	};

	AutoCheck.raises = [] || AutoCheck.raises;
	AutoCheck.listensTo = [] || AutoCheck.listensTo;

	AutoCheck.raises.push("AutoCheck.BrokerChange");
	AutoCheck.raises.push("AutoCheck.VinChange");
	AutoCheck.raises.push("AutoCheck.LoadVehicleTemplate");
	AutoCheck.raises.push("AutoCheck.ReportLoaded");

	AutoCheck.listensTo.push("AutoCheck.BrokerChange");
	AutoCheck.listensTo.push("AutoCheck.VinChange");
	AutoCheck.listensTo.push("AutoCheck.Purchase");
	AutoCheck.listensTo.push("AutoCheck.Query");

	var PublicEvents = {
		"AutoCheck.BrokerChange": function(evt, newBrokerId) {
			State.Broker = newBrokerId;
		},
		"AutoCheck.VinChange": function(evt, newVin) {
			State.Vin = newVin;
			if (State.Broker) {
				$(new AutoCheckReportArgumentsDtoQ()).trigger("stateChange");
			}
		},
		"AutoCheck.Purchase": function(evt) {
			$(new AutoCheckReportArgumentsDtoP()).trigger("stateChange");
		},
		"AutoCheck.Query": function(evt) {
			$(new AutoCheckReportArgumentsDtoQ()).trigger("stateChange");
		}
	}
	$(AutoCheck).bind(PublicEvents);

	/* ================== */
	/* = PUBLIC METHODS = */
	/* ================== */
	this.FormatStatusAsString = AutoCheckReportResultsStatusDto.toString;

}).apply(AutoCheck);

