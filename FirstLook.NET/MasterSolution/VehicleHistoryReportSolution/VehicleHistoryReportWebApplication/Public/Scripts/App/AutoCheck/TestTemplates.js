if (typeof(AutoCheck) !== "function") {
	var AutoCheck = function() {};
}
(function() {

    var PublicEvents = {
        "AutoCheck.LoadVehicleTemplate": function(evt) {
            var template = new VehicleTemplateBuilder();
            template.clean();
            template.init();
        },
        "AutoCheck.ReportLoaded": function(evt, data) {
            var template = new ReportTemplateBuilder(data);
            template.clean();
            template.init();
        }
    }
    $(AutoCheck).bind(PublicEvents);

    var VehicleTemplateBuilder = function() {
        this.$dom = {
            Vehicle$Broker: $('#broker'),
            Vehicle$Vin: $('#vin'),
            Action$Purchase: $('#Buy'),
            Action$Query: $('#See'),
            Action$Form: $('#AutoCheckForm')
        };
    };

    VehicleTemplateBuilder.prototype = {
        init: function() {
            this.build();
        },
        build: function() {
            this.$dom.Vehicle$Broker.bind('change', this.changeBroker);
            this.$dom.Vehicle$Vin.bind('change', this.changeVin);
            this.$dom.Action$Purchase.bind('click', this.actionPurchase);
            this.$dom.Action$Query.bind('click', this.actionQuery);
            this.$dom.Action$Form.bind('submit', this.actionCancel);
        },
        clean: function() {
            this.$dom.Vehicle$Broker.unbind('change').html('');
            this.$dom.Vehicle$Vin.unbind('change').html('');
            this.$dom.Action$Purchase.unbind('click');
            this.$dom.Action$Query.unbind('click');
            this.$dom.Action$Form.unbind('submit');
        },
        changeBroker: function(evt) {
            var newState = new AutoCheck.BrokerChange(evt.target.value);
            $(newState).trigger("set");
        },
        changeVin: function(evt) {
            var newState = new AutoCheck.VinChange(evt.target.value);
            $(newState).trigger("set");
        },
        actionPurchase: function(evt) {
            // purchase
            var newState = new AutoCheck.PurchaseAction();
            $(newState).trigger("set");
            // cancel submit
            evt.preventDefault();
            return false;
        },
        actionQuery: function(evt) {
            // query
            var newState = new AutoCheck.QueryAction();
            $(newState).trigger("set");
            // cancel submit
            evt.preventDefault();
            return false;
        },
        actionCancel: function(evt) {
            // cancel submit
            evt.preventDefault();
            return false;
        }
    };

    var ReportTemplateBuilder = function(data) {
        this.data = data;
        this.dom$s = {
            'expiration': $('#x_expiration'),
            'owner_count': $('#x_owner_count'),
            'score': $('#x_score'),
            'score_low': $('#x_score_comparable_low'),
            'score_high': $('#x_score_comparable_high')
        };
        this.dom$i = {
            'one_owner': $('#i_one_owner'),
            'assured': $('#i_assured'),
            'total_loss': $('#total_loss'),
            'frame_damage': $('#i_frame_damage'),
            'air_bag_deployment': $('#i_air_bag_deployment'),
            'rollback_odometer': $('#i_rollback_odometer'),
            'accident_indicators': $('#i_accident_indicators'),
            'manufacturer_recall': $('#i_manufacturer_recall')
        };
    };

    ReportTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            var report = this.data.Report;
            if (report !== null) {
                // summary
                this.dom$s.expiration.html(report.ExpirationDate);
                this.dom$s.owner_count.html(report.OwnerCount);
                this.dom$s.score.html(report.Score);
                this.dom$s.score_low.html(report.CompareScoreRangeLow);
                this.dom$s.score_high.html(report.CompareScoreRangeHigh);
                // inspections
                var inspections = report.Inspections;
                for (var i = 0; i < inspections.length; i++) {
                    var inspection = inspections[i];
                    var selected = inspection.Selected ? "y" : "n";
                    switch (inspection.Id) {
                        case 1:
                            this.dom$i.one_owner.html(selected);
                            break;
                        case 2:
                            this.dom$i.assured.html(selected);
                            break;
                        case 3:
                            this.dom$i.total_loss.html(selected);
                            break;
                        case 4:
                            this.dom$i.frame_damage.html(selected);
                            break;
                        case 5:
                            this.dom$i.air_bag_deployment.html(selected);
                            break;
                        case 6:
                            this.dom$i.rollback_odometer.html(selected);
                            break;
                        case 7:
                            this.dom$i.accident_indicators.html(selected);
                            break;
                        case 8:
                            this.dom$i.manufacturer_recall.html(selected);
                            break;
                    }
                }
            }
        },
        clean: function() {
            // summary
            this.dom$s.expiration.html('?');
            this.dom$s.owner_count.html('?');
            this.dom$s.score.html('?');
            this.dom$s.score_low.html('?');
            this.dom$s.score_high.html('?');
            // inspections
            this.dom$i.one_owner.html('?');
            this.dom$i.assured.html('?');
            this.dom$i.total_loss.html('?');
            this.dom$i.frame_damage.html('?');
            this.dom$i.air_bag_deployment.html('?');
            this.dom$i.rollback_odometer.html('?');
            this.dom$i.accident_indicators.html('?');
            this.dom$i.manufacturer_recall.html('?');
        }
    };


}).apply(AutoCheck);
