if (typeof(AutoCheck) !== "function") {
    var AutoCheck = function() {};
}

(function() {
    var ns = this;
    var ns_string = "AutoCheck";

    Ext.regModel(ns_string + "Inspections", {
        fields: ["Id", "Name", "Selected"]
    });

    // ************************************
    // VehicleTemplateBuilder
    // ************************************
    function VehicleTemplateBuilder(data) {
        this.data = data;
    }
    VehicleTemplateBuilder.prototype = {
        init: function() {},
        build: function() {},
        clean: function() {}
    };

    // ************************************
    // ReportTemplateBuilder
    // ************************************
    var reportStore = {}; // Single object the closure to help hold state
    var inspectionStore = {}; // Single object the closure to help hold state
    var inspectionList = {}; // Single object the closure to help hold state
    var reportList = {}; // Single object the closure to help hold state
    var reportStatusDisplay = {};
    function ReportTemplateBuilder(data) {
        this.data = data;
    }
    ReportTemplateBuilder.prototype = {
        init: function() {
            $(ns).trigger("AutoCheck.ReportTemplateBuilder.init", [this.data]);
            if (this.data) {
                inspectionStore = new Ext.data.JsonStore({
                    model: ns_string + "Inspections",
                    data: this.data.Report.Inspections
                });

                this.build();
            }
        },
        build: function() {
            if (this.data.Status === 0) {
                reportList = new Ext.form.FormPanel({
                    items: [{
                        xtype: "fieldset",
                        defaults: {
                            xtype: "textfield",
                            labelWidth: "35%",
                            listeners: {
                                focus: function(comp) {
                                    comp.blur();
                                }
                            }
                        },
                        items: [{
                            name: "Score",
                            label: "Score",
                            value: this.data.Report.Score
                        },
                        {
                            name: "SimilarScore",
                            label: "Similar Score",
                            value: this.data.Report.CompareScoreRangeLow + " - " + this.data.Report.CompareScoreRangeHigh
                        },
                        {
                            name: "ExpirationDate",
                            label: "Expires",
                            value: Ext.util.Format.asmxDate(this.data.Report.ExpirationDate)
                        },
                        {
                            name: "OwnerCount",
                            label: "Owners",
                            value: this.data.Report.OwnerCount.toString()
                        }]
                    }]
                });

                inspectionList = new Ext.List({
                    scroll: false,
                    disableSelection: true,
                    id: "fl-autocheck-inspections",
                    cls: 'autocheck_inspection_items',
                    grouped: false,
                    itemTpl: "<span class='autocheck-report-item selected-{Selected}'>{Name}</span>",
                    emptyText: "No Inspections for Report",
                    store: inspectionStore
                });

                ns.render({
                    xtype: "panel",
                    scroll: true,
                    items: [reportList, inspectionList]
                });
            } else if (this.data.Status === 1) { // UnOrdered
                reportOrder = new Ext.form.FormPanel({
                    items: [{
                        xtype: "button",
                        text: "Order Report",
                        listeners: {
                            tap: function() {
                                this.up('panel').submit()
                            }
                        }
                    }],
                    listeners: {
                        scope: this,
                        beforesubmit: function(comp, values, options) {
                            $(new ns.PurchaseAction()).trigger("set");

                            return false;
                        }
                    }
                });

                ns.render(reportOrder);
                reportStatusDisplay = new Ext.Container({
                    html: "<p class='report-status'>No Report Ordered</p>"
                });
                ns.render(reportStatusDisplay);
            } else {
                reportStatusDisplay = new Ext.Container({
                    html: "<p class='report-status'>" + ns.FormatStatusAsString(this.data.Status) + "</p>"
                });
                ns.render(reportStatusDisplay);
            }

        },
        clean: function() {}
    };

    this.VehicleTemplateBuilder = VehicleTemplateBuilder;
    this.ReportTemplateBuilder = ReportTemplateBuilder;
}).apply(AutoCheck);

