
using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "FirstLook.VehicleHistoryReport.WebApplication.Pages", Justification = "ASP.NET Namespace Reflects Folder Structure")]
[assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "FirstLook.VehicleHistoryReport.WebApplication.Pages.Carfax", Justification = "ASP.NET Namespace Reflects Folder Structure")]
[assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "VehicleHistoryReportWebApplication", Justification = "ASP.NET Namespace Reflects Folder Structure")]
[assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "FirstLook.VehicleHistoryReport.WebApplication.Pages.AutoCheck", Justification = "ASP.NET Namespace Reflects Folder Structure")]

