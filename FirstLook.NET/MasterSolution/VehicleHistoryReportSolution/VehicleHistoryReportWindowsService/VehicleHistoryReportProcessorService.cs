using System.ServiceProcess;
using System.Threading;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Processor;

namespace FirstLook.VehicleHistoryReport.WindowsService
{
	public partial class VehicleHistoryReportProcessorService : ServiceBase
	{
        private readonly CarfaxReportProcessor processor;

        private Thread processorThread;

		public VehicleHistoryReportProcessorService()
		{
			InitializeComponent();

			processor = new CarfaxReportProcessor();
		}

        protected override void OnStart(string[] args)
        {
            processorThread = new Thread(processor.Process);

            processor.Start();

            processorThread.Start();
        }

        protected override void OnStop()
        {
            processor.Stop();

            processorThread.Join();
        }
	}
}