namespace FirstLook.VehicleHistoryReport.WindowsService
{
	partial class VehicleHistoryReportProcessorServiceInstaller
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.vhrServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
			this.vhrServiceInstaller = new System.ServiceProcess.ServiceInstaller();
			// 
			// vhrServiceProcessInstaller
			// 
			this.vhrServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.NetworkService;
			this.vhrServiceProcessInstaller.Password = null;
			this.vhrServiceProcessInstaller.Username = null;
			// 
			// vhrServiceInstaller
			// 
			this.vhrServiceInstaller.Description = "Executes vehicle history report commands polled from a database";
			this.vhrServiceInstaller.DisplayName = "FirstLook - Vehicle History Report Processor";
			this.vhrServiceInstaller.ServiceName = "Vehicle History Report Processor";
			// 
			// VehicleHistoryReportProcessorServiceInstaller
			// 
			this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.vhrServiceProcessInstaller,
            this.vhrServiceInstaller});

		}

		#endregion

		private System.ServiceProcess.ServiceProcessInstaller vhrServiceProcessInstaller;
		private System.ServiceProcess.ServiceInstaller vhrServiceInstaller;
	}
}