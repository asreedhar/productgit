using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;

namespace FirstLook.VehicleHistoryReport.WindowsService
{
	[RunInstaller(true)]
	public partial class VehicleHistoryReportProcessorServiceInstaller : Installer
	{
		public VehicleHistoryReportProcessorServiceInstaller()
		{
			InitializeComponent();
		}
	}
}