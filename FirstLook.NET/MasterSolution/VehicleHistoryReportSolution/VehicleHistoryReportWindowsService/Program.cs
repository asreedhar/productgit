using System;
using System.Diagnostics;
using System.ServiceProcess;

namespace FirstLook.VehicleHistoryReport.WindowsService
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		static void Main()
		{
			AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

			ServiceBase[] ServicesToRun;

			// More than one user Service may run within the same process. To add
			// another service to this process, change the following line to
			// create a second service object. For example,
			//
			//   ServicesToRun = new ServiceBase[] {new Service1(), new MySecondUserService()};
			//
			ServicesToRun = new ServiceBase[] { new VehicleHistoryReportProcessorService() };

			ServiceBase.Run(ServicesToRun);
		}

		static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			const string sourceName = "Vehicle History Report Processor";
			const string logName = "Application";

			if (!EventLog.SourceExists(sourceName))
			{
				EventLog.CreateEventSource(sourceName, logName);
			}

			Exception ex = e.ExceptionObject as Exception;

			if (ex != null)
			{
				EventLog log = new EventLog();
				log.Source = sourceName;
				log.WriteEntry(ex.ToString(), EventLogEntryType.Error);
			}
		}
	}
}