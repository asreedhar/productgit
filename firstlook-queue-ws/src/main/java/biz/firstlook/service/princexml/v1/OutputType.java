
package biz.firstlook.service.princexml.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OutputType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OutputType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OutputTypeCode" type="{http://www.firstlook.biz/service/PrinceXml/v1}OutputTypeCode"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element ref="{http://www.firstlook.biz/service/PrinceXml/v1}SharedFileSystem"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutputType", propOrder = {
    "outputTypeCode",
    "sharedFileSystem"
})
public class OutputType {

    @XmlElement(name = "OutputTypeCode", required = true)
    protected OutputTypeCode outputTypeCode;
    @XmlElement(name = "SharedFileSystem")
    protected SharedFileSystem sharedFileSystem;

    /**
     * Gets the value of the outputTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link OutputTypeCode }
     *     
     */
    public OutputTypeCode getOutputTypeCode() {
        return outputTypeCode;
    }

    /**
     * Sets the value of the outputTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link OutputTypeCode }
     *     
     */
    public void setOutputTypeCode(OutputTypeCode value) {
        this.outputTypeCode = value;
    }

    /**
     * Gets the value of the sharedFileSystem property.
     * 
     * @return
     *     possible object is
     *     {@link SharedFileSystem }
     *     
     */
    public SharedFileSystem getSharedFileSystem() {
        return sharedFileSystem;
    }

    /**
     * Sets the value of the sharedFileSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link SharedFileSystem }
     *     
     */
    public void setSharedFileSystem(SharedFileSystem value) {
        this.sharedFileSystem = value;
    }

}
