package biz.firstlook.service.princexml.pool;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import biz.firstlook.service.princexml.prince.Prince;

public class PrincePool {
	
	private final ExecutorService pool;
	private String pathToPrinceXml;
	
	public PrincePool(int numberOfThreads, String pathToPrinceXml) {
		this.pool = Executors.newFixedThreadPool(numberOfThreads);
		this.pathToPrinceXml = pathToPrinceXml;
	}

	public byte[] generatePDF(String content, List<String> pathsToCSS, boolean isHtml) throws InterruptedException, ExecutionException {
		byte[] result = null;
		Future<ByteArrayOutputStream> future = pool.submit(new CallablePrince(new ByteArrayInputStream(content.getBytes()), pathsToCSS, isHtml));
		result = future.get().toByteArray();
		return result;
	}
	
	private class CallablePrince implements Callable<ByteArrayOutputStream> {

		private InputStream inputStream;
		private Prince prince;
		private boolean isHtml;
		
		public CallablePrince(InputStream inputStream, List<String> pathToCss, boolean isHtml) {
			this.inputStream = inputStream;
			this.prince = createPrince(pathToCss);
			this.isHtml = isHtml;
		}
		
		public ByteArrayOutputStream call() throws Exception {
			ByteArrayOutputStream pdfOutput = new ByteArrayOutputStream();
			prince.convert(inputStream, pdfOutput);
			return pdfOutput;
		}
		
		private Prince createPrince(List<String> pathsToCSS) {
			Prince prince = new Prince(pathToPrinceXml);
			prince.setHTML(isHtml);
			prince.setEmbedFonts(false);
			for (String cssPath : pathsToCSS) {
				prince.addStyleSheet(cssPath);
			}
			return prince;
		}
	}
}
