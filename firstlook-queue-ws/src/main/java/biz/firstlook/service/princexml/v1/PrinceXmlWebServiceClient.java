
package biz.firstlook.service.princexml.v1;

import java.net.MalformedURLException;
import java.util.Collection;
import java.util.HashMap;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.namespace.QName;

import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.annotations.AnnotationServiceFactory;
import org.codehaus.xfire.annotations.jsr181.Jsr181WebAnnotations;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.jaxb2.JaxbTypeRegistry;
import org.codehaus.xfire.service.Endpoint;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.transport.TransportManager;

public class PrinceXmlWebServiceClient {

    private static XFireProxyFactory proxyFactory = new XFireProxyFactory();
    private HashMap<QName,Endpoint> endpoints = new HashMap<QName,Endpoint>();
    private Service service0;

    public PrinceXmlWebServiceClient() {
        create0();
        Endpoint PrinceXmlWebServiceSOAPEP = service0 .addEndpoint(new QName("http://www.firstlook.biz/service/PrinceXml/v1", "PrinceXmlWebServiceSOAP"), new QName("http://www.firstlook.biz/service/PrinceXml/v1", "PrinceXmlWebServiceSOAP"), "https://www.firstlook.biz/firstlook-queue-ws/services/PrinceXmlWebService");
        endpoints.put(new QName("http://www.firstlook.biz/service/PrinceXml/v1", "PrinceXmlWebServiceSOAP"), PrinceXmlWebServiceSOAPEP);
        Endpoint PrinceXmlWebServiceLocalEndpointEP = service0 .addEndpoint(new QName("http://www.firstlook.biz/service/PrinceXml/v1", "PrinceXmlWebServiceLocalEndpoint"), new QName("http://www.firstlook.biz/service/PrinceXml/v1", "PrinceXmlWebServiceLocalBinding"), "xfire.local://PrinceXmlWebService");
        endpoints.put(new QName("http://www.firstlook.biz/service/PrinceXml/v1", "PrinceXmlWebServiceLocalEndpoint"), PrinceXmlWebServiceLocalEndpointEP);
    }

    public Object getEndpoint(Endpoint endpoint) {
        try {
            return proxyFactory.create((endpoint).getBinding(), (endpoint).getUrl());
        } catch (MalformedURLException e) {
            throw new XFireRuntimeException("Invalid URL", e);
        }
    }

    public Object getEndpoint(QName name) {
        Endpoint endpoint = ((Endpoint) endpoints.get((name)));
        if ((endpoint) == null) {
            throw new IllegalStateException("No such endpoint!");
        }
        return getEndpoint((endpoint));
    }

    public Collection getEndpoints() {
        return endpoints.values();
    }

    private void create0() {
        TransportManager tm = (org.codehaus.xfire.XFireFactory.newInstance().getXFire().getTransportManager());
        HashMap<String,Object> props = new HashMap<String,Object>();
        props.put("annotations.allow.interface", Boolean.TRUE);
        AnnotationServiceFactory asf = new AnnotationServiceFactory(new Jsr181WebAnnotations(), tm, new AegisBindingProvider(new JaxbTypeRegistry()));
        asf.setBindingCreationEnabled(false);
        service0 = asf.create((biz.firstlook.service.princexml.v1.PrinceXmlWebService.class), props);
        {
            asf.createSoap11Binding(service0, new QName("http://www.firstlook.biz/service/PrinceXml/v1", "PrinceXmlWebServiceLocalBinding"), "urn:xfire:transport:local");
        }
        {
            asf.createSoap11Binding(service0, new QName("http://www.firstlook.biz/service/PrinceXml/v1", "PrinceXmlWebServiceSOAP"), "http://schemas.xmlsoap.org/soap/http");
        }
    }

    public PrinceXmlWebService getPrinceXmlWebServiceSOAP() {
        return ((PrinceXmlWebService)(this).getEndpoint(new QName("http://www.firstlook.biz/service/PrinceXml/v1", "PrinceXmlWebServiceSOAP")));
    }

    public PrinceXmlWebService getPrinceXmlWebServiceSOAP(String url) {
        PrinceXmlWebService var = getPrinceXmlWebServiceSOAP();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

    public PrinceXmlWebService getPrinceXmlWebServiceLocalEndpoint() {
        return ((PrinceXmlWebService)(this).getEndpoint(new QName("http://www.firstlook.biz/service/PrinceXml/v1", "PrinceXmlWebServiceLocalEndpoint")));
    }

    public PrinceXmlWebService getPrinceXmlWebServiceLocalEndpoint(String url) {
        PrinceXmlWebService var = getPrinceXmlWebServiceLocalEndpoint();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

    public static void main(String[] args) throws DatatypeConfigurationException {

		PrinceXmlWebServiceClient client = new PrinceXmlWebServiceClient();
		PrinceXmlWebService service = client.getPrinceXmlWebServiceSOAP("http://swenmouth01.firstlook.biz/firstlook-queue-ws/services/PrinceXmlWebService");

		PrinceXmlRequest request = new PrinceXmlRequest();
		
		Input input = new Input();
		input.setContent("<html><body>Incisent Rules</body></html>");
		input.setContentType(ContentType.HTML);
		
		request.setInput(input);

		SharedFileSystem sharedFileSystem = new SharedFileSystem();
		sharedFileSystem.setFilePrefix("test-");
		sharedFileSystem.setFileSuffix(".pdf");
		sharedFileSystem.setDirectoryPathRelativeToSharedFileSystemRoot("incisent");
		
		OutputType outputType = new OutputType();
		outputType.setOutputTypeCode(OutputTypeCode.SHARED_FILE_SYSTEM);
		outputType.setSharedFileSystem(sharedFileSystem);
		
		request.setOutputType(outputType);
		
		try {
			PrinceXmlResponse response = service.generate(request);
			System.out.println(String.format("Status Code: %s", response.getStatus().getCode()));
			System.out.println(String.format("Status Message: %s", response.getStatus().getMessage()));
			System.out.println("Path is :  " + response.getOutput().getFilePathRelativeToSharedFileSystemRoot());			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
