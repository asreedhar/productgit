
package biz.firstlook.service.princexml.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for Output complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Output">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OutputTypeCode" type="{http://www.firstlook.biz/service/PrinceXml/v1}OutputTypeCode"/>
 *         &lt;choice>
 *           &lt;element name="ByteStream" type="{http://www.w3.org/2001/XMLSchema}hexBinary"/>
 *           &lt;element name="FilePathRelativeToSharedFileSystemRoot" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Output", propOrder = {
    "outputTypeCode",
    "byteStream",
    "filePathRelativeToSharedFileSystemRoot"
})
public class Output {

    @XmlElement(name = "OutputTypeCode", required = true)
    protected OutputTypeCode outputTypeCode;
    @XmlElement(name = "ByteStream", type = String.class)
    @XmlJavaTypeAdapter(HexBinaryAdapter.class)
    protected byte[] byteStream;
    @XmlElement(name = "FilePathRelativeToSharedFileSystemRoot")
    protected String filePathRelativeToSharedFileSystemRoot;

    /**
     * Gets the value of the outputTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link OutputTypeCode }
     *     
     */
    public OutputTypeCode getOutputTypeCode() {
        return outputTypeCode;
    }

    /**
     * Sets the value of the outputTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link OutputTypeCode }
     *     
     */
    public void setOutputTypeCode(OutputTypeCode value) {
        this.outputTypeCode = value;
    }

    /**
     * Gets the value of the byteStream property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public byte[] getByteStream() {
        return byteStream;
    }

    /**
     * Sets the value of the byteStream property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setByteStream(byte[] value) {
        this.byteStream = ((byte[]) value);
    }

    /**
     * Gets the value of the filePathRelativeToSharedFileSystemRoot property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilePathRelativeToSharedFileSystemRoot() {
        return filePathRelativeToSharedFileSystemRoot;
    }

    /**
     * Sets the value of the filePathRelativeToSharedFileSystemRoot property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilePathRelativeToSharedFileSystemRoot(String value) {
        this.filePathRelativeToSharedFileSystemRoot = value;
    }

}
