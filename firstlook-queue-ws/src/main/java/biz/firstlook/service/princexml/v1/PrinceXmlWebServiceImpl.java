
package biz.firstlook.service.princexml.v1;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

import javax.jws.WebService;

import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.service.princexml.pool.PrincePool;

@WebService(serviceName = "PrinceXmlWebService", targetNamespace = "http://www.firstlook.biz/service/PrinceXml/v1", endpointInterface = "biz.firstlook.service.princexml.v1.PrinceXmlWebService")
public class PrinceXmlWebServiceImpl
    implements PrinceXmlWebService
{
	private static final String UNIX_EXECUTABLE_PATH_KEY = "biz.firstlook.service.princexml.v1.executable";
	
	private static final String UNIX_EXECUTABLE_PATH = "/usr/bin/prince";

	private static final String POOL_SIZE_KEY = "biz.firstlook.service.princexml.v1.pool.size";
	
	private static final int POOL_SIZE = 5;

	private static final String SHARED_FILE_SYSTEM_PATH_KEY = "biz.firstlook.service.princexml.v1.san";
	
	private static final String SHARED_FILE_SYSTEM_PATH = "/var/www/html";
	
	private PrincePool pool;
	
	private String sanPath;
	
	public PrinceXmlWebServiceImpl()
	{
		super();
		this.pool = new PrincePool(
				PropertyLoader.getIntProperty(POOL_SIZE_KEY, POOL_SIZE),
				PropertyLoader.getProperty(UNIX_EXECUTABLE_PATH_KEY, UNIX_EXECUTABLE_PATH));
		this.sanPath = PropertyLoader.getProperty(SHARED_FILE_SYSTEM_PATH_KEY, SHARED_FILE_SYSTEM_PATH);
	}
	
    public PrinceXmlResponse generate(final biz.firstlook.service.princexml.v1.PrinceXmlRequest PrinceXmlRequest) {
        // error by default
        Status status = new Status();
        status.setCode(StatusCode.SERVER_ERROR);
        // bare response
        PrinceXmlResponse response = new PrinceXmlResponse();
        response.setStatus(status);
        // try and get pdf
        try {
			byte[] byteStream = pool.generatePDF(
					PrinceXmlRequest.getInput().getContent(),
					PrinceXmlRequest.getInput().getStyleSheetPaths(),
					ContentType.HTML.equals(PrinceXmlRequest.getInput().getContentType()));
			Output output = new Output();
			output.setOutputTypeCode(PrinceXmlRequest.getOutputType().getOutputTypeCode());
			response.setOutput(output);
			switch (PrinceXmlRequest.getOutputType().getOutputTypeCode()) {
			case BYTE_STREAM:
				status.setCode(StatusCode.SUCCESS);
				output.setByteStream(byteStream);
				break;
			case SHARED_FILE_SYSTEM:
				SharedFileSystem sharedFileSystem = PrinceXmlRequest.getOutputType().getSharedFileSystem();
				if (sharedFileSystem != null) {
					try {
						output.setFilePathRelativeToSharedFileSystemRoot(writePdfToFileSystem(byteStream, sharedFileSystem));
						status.setCode(StatusCode.SUCCESS);
					}
					catch (IOException e) {
						status.setCode(StatusCode.SERVER_ERROR);
						status.setMessage(e.getMessage());
					}
				}
				else {
					status.setCode(StatusCode.CLIENT_ERROR);
					status.setMessage("Missing SharedFileSystem");
				}
				break;
			default:
				status.setCode(StatusCode.CLIENT_ERROR);
				status.setMessage("Unknown OutputType");
				break;
			}
		}
        catch (InterruptedException e) {
			status.setMessage(e.getMessage());
		}
        catch (ExecutionException e) {
			status.setMessage(e.getMessage());
		}
        // return
        return response;
    }

    private String writePdfToFileSystem(byte[] byteStream, SharedFileSystem sharedFileSystem) throws IOException {
		// output directory
		File directory = new File(sanPath, sharedFileSystem.getDirectoryPathRelativeToSharedFileSystemRoot());
		// output file
		File file = File.createTempFile(
				sharedFileSystem.getFilePrefix(),
				sharedFileSystem.getFileSuffix(),
				directory);
		// write to file
		FileOutputStream fileOutputStream = new FileOutputStream(file);
		fileOutputStream.write(byteStream);
		fileOutputStream.flush();
		fileOutputStream.close();
		// return relative path
		return String.format("%s%s%s",
				sharedFileSystem.getDirectoryPathRelativeToSharedFileSystemRoot(),
				File.separatorChar,
				file.getName());
	}
    
}
