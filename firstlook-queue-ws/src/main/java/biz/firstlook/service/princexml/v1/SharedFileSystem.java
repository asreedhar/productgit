
package biz.firstlook.service.princexml.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SharedFileSystem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SharedFileSystem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FilePrefix" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FileSuffix" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DirectoryPathRelativeToSharedFileSystemRoot" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SharedFileSystem", propOrder = {
    "filePrefix",
    "fileSuffix",
    "directoryPathRelativeToSharedFileSystemRoot"
})
public class SharedFileSystem {

    @XmlElement(name = "FilePrefix", required = true)
    protected String filePrefix;
    @XmlElement(name = "FileSuffix", required = true)
    protected String fileSuffix;
    @XmlElement(name = "DirectoryPathRelativeToSharedFileSystemRoot", required = true)
    protected String directoryPathRelativeToSharedFileSystemRoot;

    /**
     * Gets the value of the filePrefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilePrefix() {
        return filePrefix;
    }

    /**
     * Sets the value of the filePrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilePrefix(String value) {
        this.filePrefix = value;
    }

    /**
     * Gets the value of the fileSuffix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileSuffix() {
        return fileSuffix;
    }

    /**
     * Sets the value of the fileSuffix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileSuffix(String value) {
        this.fileSuffix = value;
    }

    /**
     * Gets the value of the directoryPathRelativeToSharedFileSystemRoot property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDirectoryPathRelativeToSharedFileSystemRoot() {
        return directoryPathRelativeToSharedFileSystemRoot;
    }

    /**
     * Sets the value of the directoryPathRelativeToSharedFileSystemRoot property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDirectoryPathRelativeToSharedFileSystemRoot(String value) {
        this.directoryPathRelativeToSharedFileSystemRoot = value;
    }

}
