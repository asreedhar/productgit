
package biz.firstlook.service.reliablemessagequeue.v1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import javax.jws.WebService;
import javax.naming.NamingException;

import biz.firstlook.commons.sql.DataSourceUtils;
import biz.firstlook.commons.sql.PreparedStatementCreator;
import biz.firstlook.commons.sql.RowMapper;

@WebService(serviceName = "ReliableMessageQueueService", targetNamespace = "http://www.firstlook.biz/service/ReliableMessageQueue/v1", endpointInterface = "biz.firstlook.service.reliablemessagequeue.v1.ReliableMessageQueueServiceSoap")
public class ReliableMessageQueueServiceImpl
    implements ReliableMessageQueueServiceSoap
{
	
    public QueueResponse poll(final biz.firstlook.service.reliablemessagequeue.v1.QueueRequest QueueRequest) {
    	// fail by default
    	QueueResponse response = newQueueResponse();
    	// try and read a row
    	try {
			List<QueueItem> queueItems = DataSourceUtils.executeQueryForUpdate(
					DataSourceUtils.getDataSource("jdbc/IMT"),
					new PreparedStatementCreator() {
						public PreparedStatement createPrepareStatement(Connection conn) throws SQLException {
							final String commandText = "SELECT TOP 1 Id, QueueName, Message, Ready FROM ReliableMessageQueue WITH (SERIALIZABLE,TABLOCK,XLOCK) WHERE Ready = 1 AND QueueName = ?";
							PreparedStatement stmt = conn.prepareStatement(commandText, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
							final String queueName = QueueRequest.getQueueName();
							if (queueName == null) {
								stmt.setNull(1, Types.VARCHAR);
							}
							else {
								stmt.setString(1, queueName);
							}
							return stmt;
						}
					},
					new RowMapper<QueueItem>() {
						public QueueItem mapRow(ResultSet rs, int rowNum) throws SQLException {
							QueueItem item = new QueueItem();
							item.setQueueName(rs.getString("QueueName"));
							if (rs.wasNull()) {
								item.setQueueName(null);
							}
							item.setQueueReceipt(Integer.toString(rs.getInt("Id")));
							if (rs.wasNull()) {
								item.setQueueReceipt(null);
							}
							item.setMessage(rs.getBytes("Message"));
							if (rs.wasNull()) {
								item.setMessage(null);
							}
							rs.updateBoolean("Ready", false);
							rs.updateRow();
							return item;
						}
					});
			// add a status message
			response.getStatus().setMessage(String.format("%d Rows Selected", queueItems.size()));
			// update status code and queue item
			if (queueItems.size() == 0) {
				response.getStatus().setCode(StatusCode.SUCCESS);
			}
			else if (queueItems.size() == 1) {
				response.setQueueItem(queueItems.get(0));
				response.getStatus().setCode(StatusCode.SUCCESS);
			}
		}
		catch (NamingException ne) {
			response.getStatus().setMessage(ne.getMessage());
		}
		catch (SQLException se) {
			response.getStatus().setMessage(se.getMessage());
		}
		// return
		return response;
    }

    public QueueResponse commit(final biz.firstlook.service.reliablemessagequeue.v1.QueueRequest QueueRequest) {
    	// fail by default
    	QueueResponse response = newQueueResponse();
    	// delete the row
    	try {
			int rowsDeleted = DataSourceUtils.execute(
					DataSourceUtils.getDataSource("jdbc/IMT"),
					new PreparedStatementCreator() {
						public PreparedStatement createPrepareStatement(Connection conn) throws SQLException {
							PreparedStatement stmt = conn.prepareStatement("DELETE FROM ReliableMessageQueue WHERE Id = ? and QueueName = ? AND Ready = 0");
							final String queueReceipt = QueueRequest.getQueueReceipt();
							if (queueReceipt == null) {
								stmt.setNull(1, Types.INTEGER);
							}
							else {
								stmt.setInt(1, Integer.parseInt(queueReceipt));
							}
							final String queueName = QueueRequest.getQueueName();
							if (queueName == null) {
								stmt.setNull(2, Types.VARCHAR);
							}
							else {
								stmt.setString(2, queueName);
							}
							return stmt;
						}
					});
			// add a status message
			response.getStatus().setMessage(String.format("%d Rows Deleted", rowsDeleted));
			// update status code and queue item
			if (rowsDeleted == 1) {
				response.getStatus().setCode(StatusCode.SUCCESS);
			}
		}
		catch (NamingException ne) {
			response.getStatus().setMessage(ne.getMessage());
		}
		catch (SQLException se) {
			response.getStatus().setMessage(se.getMessage());
		}
    	// return
    	return response;
    }

    public QueueResponse rollback(final biz.firstlook.service.reliablemessagequeue.v1.QueueRequest QueueRequest) {
    	// fail by default
    	QueueResponse response = newQueueResponse();
    	// update the row
    	try {
			int rowsUpdated = DataSourceUtils.execute(
					DataSourceUtils.getDataSource("jdbc/IMT"),
					new PreparedStatementCreator() {
						public PreparedStatement createPrepareStatement(Connection conn) throws SQLException {
							PreparedStatement stmt = conn.prepareStatement("UPDATE ReliableMessageQueue SET Ready = 1 WHERE Id = ? and QueueName = ? AND Ready = 0");
							final String queueReceipt = QueueRequest.getQueueReceipt();
							if (queueReceipt == null) {
								stmt.setNull(1, Types.VARCHAR);
							}
							else {
								stmt.setString(1, queueReceipt);
							}
							final String queueName = QueueRequest.getQueueName();
							if (queueName == null) {
								stmt.setNull(2, Types.VARCHAR);
							}
							else {
								stmt.setString(2, queueName);
							}
							return stmt;
						}
					});
			// add a status message
			response.getStatus().setMessage(String.format("%d Rows Updated", rowsUpdated));
			// update status code and queue item
			if (rowsUpdated == 1) {
				response.getStatus().setCode(StatusCode.SUCCESS);
			}
		}
		catch (NamingException ne) {
			response.getStatus().setMessage(ne.getMessage());
		}
		catch (SQLException se) {
			response.getStatus().setMessage(se.getMessage());
		}
    	// return
    	return response;
    }

    public QueueResponse add(final biz.firstlook.service.reliablemessagequeue.v1.QueueRequest QueueRequest) {
    	// fail by default
    	QueueResponse response = newQueueResponse();
    	// insert the row
    	try {
			int identity = DataSourceUtils.executeInsert(
					DataSourceUtils.getDataSource("jdbc/IMT"),
					new PreparedStatementCreator() {
						public PreparedStatement createPrepareStatement(Connection conn) throws SQLException {
							PreparedStatement stmt = conn.prepareStatement("INSERT INTO ReliableMessageQueue (QueueName,Message,Ready) VALUES (?,?,1)");
							final String queueName = QueueRequest.getQueueItem().getQueueName();
							if (queueName == null) {
								stmt.setNull(1, Types.VARCHAR);
							}
							else {
								stmt.setString(1, queueName);
							}
							final byte[] message = QueueRequest.getQueueItem().getMessage();
							if (message == null) {
								stmt.setNull(2, Types.VARCHAR);
							}
							else {
								stmt.setBytes(2, message);
							}
							return stmt;
						}
					});
			// add a status message
			response.getStatus().setMessage(String.format("Identity: %d", identity));
			// update status code and queue item
			if (identity != -1) {
				response.getStatus().setCode(StatusCode.SUCCESS);
				response.setQueueReceipt(Integer.toString(identity));
			}
		}
		catch (NamingException ne) {
			response.getStatus().setMessage(ne.getMessage());
		}
		catch (SQLException se) {
			response.getStatus().setMessage(se.getMessage());
		}
    	// return
    	return response;
    }

    private static final QueueResponse newQueueResponse() {
    	QueueResponse queueResponse = new QueueResponse();
    	Status status = new Status();
    	status.setCode(StatusCode.FAILURE);
    	queueResponse.setStatus(status);
    	return queueResponse;
    }
}
