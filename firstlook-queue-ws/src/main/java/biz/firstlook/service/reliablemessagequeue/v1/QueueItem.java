
package biz.firstlook.service.reliablemessagequeue.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QueueItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueueItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.firstlook.biz/service/ReliableMessageQueue/v1}QueueName"/>
 *         &lt;element ref="{http://www.firstlook.biz/service/ReliableMessageQueue/v1}QueueReceipt" minOccurs="0"/>
 *         &lt;element name="Message" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueueItem", propOrder = {
    "queueName",
    "queueReceipt",
    "message"
})
public class QueueItem {

    @XmlElement(name = "QueueName", required = true)
    protected String queueName;
    @XmlElement(name = "QueueReceipt")
    protected String queueReceipt;
    @XmlElement(name = "Message", required = true)
    protected byte[] message;

    /**
     * Gets the value of the queueName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueueName() {
        return queueName;
    }

    /**
     * Sets the value of the queueName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueueName(String value) {
        this.queueName = value;
    }

    /**
     * Gets the value of the queueReceipt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueueReceipt() {
        return queueReceipt;
    }

    /**
     * Sets the value of the queueReceipt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueueReceipt(String value) {
        this.queueReceipt = value;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setMessage(byte[] value) {
        this.message = ((byte[]) value);
    }

}
