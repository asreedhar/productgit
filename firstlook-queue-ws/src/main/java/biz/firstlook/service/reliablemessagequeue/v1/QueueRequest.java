
package biz.firstlook.service.reliablemessagequeue.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QueueRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueueRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.firstlook.biz/service/ReliableMessageQueue/v1}QueueName"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element ref="{http://www.firstlook.biz/service/ReliableMessageQueue/v1}QueueItem"/>
 *           &lt;element ref="{http://www.firstlook.biz/service/ReliableMessageQueue/v1}QueueReceipt"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueueRequest", propOrder = {
    "queueName",
    "queueItem",
    "queueReceipt"
})
public class QueueRequest {

    @XmlElement(name = "QueueName", required = true)
    protected String queueName;
    @XmlElement(name = "QueueItem")
    protected QueueItem queueItem;
    @XmlElement(name = "QueueReceipt")
    protected String queueReceipt;

    /**
     * Gets the value of the queueName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueueName() {
        return queueName;
    }

    /**
     * Sets the value of the queueName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueueName(String value) {
        this.queueName = value;
    }

    /**
     * Gets the value of the queueItem property.
     * 
     * @return
     *     possible object is
     *     {@link QueueItem }
     *     
     */
    public QueueItem getQueueItem() {
        return queueItem;
    }

    /**
     * Sets the value of the queueItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueueItem }
     *     
     */
    public void setQueueItem(QueueItem value) {
        this.queueItem = value;
    }

    /**
     * Gets the value of the queueReceipt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueueReceipt() {
        return queueReceipt;
    }

    /**
     * Sets the value of the queueReceipt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueueReceipt(String value) {
        this.queueReceipt = value;
    }

}
