
package biz.firstlook.service.reliablemessagequeue.v1;

import java.net.MalformedURLException;
import java.util.Collection;
import java.util.HashMap;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.namespace.QName;

import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.annotations.AnnotationServiceFactory;
import org.codehaus.xfire.annotations.jsr181.Jsr181WebAnnotations;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.jaxb2.JaxbTypeRegistry;
import org.codehaus.xfire.service.Endpoint;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.transport.TransportManager;

public class ReliableMessageQueueServiceClient {

    private static XFireProxyFactory proxyFactory = new XFireProxyFactory();
    private HashMap<QName,Endpoint> endpoints = new HashMap<QName,Endpoint>();
    private Service service0;

    public ReliableMessageQueueServiceClient() {
        create0();
        Endpoint ReliableMessageQueueServiceSoapEP = service0 .addEndpoint(new QName("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "ReliableMessageQueueServiceSoap"), new QName("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "ReliableMessageQueueServiceSoap"), "https://www.firstlook.biz/firstlook-queue-ws/services/ReliableMessageQueue");
        endpoints.put(new QName("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "ReliableMessageQueueServiceSoap"), ReliableMessageQueueServiceSoapEP);
        Endpoint ReliableMessageQueueServiceSoapLocalEndpointEP = service0 .addEndpoint(new QName("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "ReliableMessageQueueServiceSoapLocalEndpoint"), new QName("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "ReliableMessageQueueServiceSoapLocalBinding"), "xfire.local://ReliableMessageQueueService");
        endpoints.put(new QName("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "ReliableMessageQueueServiceSoapLocalEndpoint"), ReliableMessageQueueServiceSoapLocalEndpointEP);
    }

    public Object getEndpoint(Endpoint endpoint) {
        try {
            return proxyFactory.create((endpoint).getBinding(), (endpoint).getUrl());
        } catch (MalformedURLException e) {
            throw new XFireRuntimeException("Invalid URL", e);
        }
    }

    public Object getEndpoint(QName name) {
        Endpoint endpoint = ((Endpoint) endpoints.get((name)));
        if ((endpoint) == null) {
            throw new IllegalStateException("No such endpoint!");
        }
        return getEndpoint((endpoint));
    }

    public Collection getEndpoints() {
        return endpoints.values();
    }

    private void create0() {
        TransportManager tm = (org.codehaus.xfire.XFireFactory.newInstance().getXFire().getTransportManager());
        HashMap<String,Object> props = new HashMap<String,Object>();
        props.put("annotations.allow.interface", Boolean.TRUE);
        AnnotationServiceFactory asf = new AnnotationServiceFactory(new Jsr181WebAnnotations(), tm, new AegisBindingProvider(new JaxbTypeRegistry()));
        asf.setBindingCreationEnabled(false);
        service0 = asf.create((biz.firstlook.service.reliablemessagequeue.v1.ReliableMessageQueueServiceSoap.class), props);
        {
            asf.createSoap11Binding(service0, new QName("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "ReliableMessageQueueServiceSoap"), "http://schemas.xmlsoap.org/soap/http");
        }
        {
            asf.createSoap11Binding(service0, new QName("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "ReliableMessageQueueServiceSoapLocalBinding"), "urn:xfire:transport:local");
        }
    }

    public ReliableMessageQueueServiceSoap getReliableMessageQueueServiceSoap() {
        return ((ReliableMessageQueueServiceSoap)(this).getEndpoint(new QName("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "ReliableMessageQueueServiceSoap")));
    }

    public ReliableMessageQueueServiceSoap getReliableMessageQueueServiceSoap(String url) {
        ReliableMessageQueueServiceSoap var = getReliableMessageQueueServiceSoap();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

    public ReliableMessageQueueServiceSoap getReliableMessageQueueServiceSoapLocalEndpoint() {
        return ((ReliableMessageQueueServiceSoap)(this).getEndpoint(new QName("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "ReliableMessageQueueServiceSoapLocalEndpoint")));
    }

    public ReliableMessageQueueServiceSoap getReliableMessageQueueServiceSoapLocalEndpoint(String url) {
        ReliableMessageQueueServiceSoap var = getReliableMessageQueueServiceSoapLocalEndpoint();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

    public static void main(String[] args) throws DatatypeConfigurationException {
    	
    	final String queueName = "TestClient";
    	
    	ReliableMessageQueueServiceClient client = new ReliableMessageQueueServiceClient();
    	
    	ReliableMessageQueueServiceSoap service = client.getReliableMessageQueueServiceSoap("http://swenmouth01.firstlook.biz/firstlook-queue-ws/services/ReliableMessageQueueWebService");
    	
    	add(queueName, service);
    	poll(queueName, service);
    }

	private static void add(final String queueName, ReliableMessageQueueServiceSoap service) {
		
		QueueRequest request = new QueueRequest();
		request.setQueueName(queueName);
    	
    	QueueItem item = new QueueItem();
    	item.setQueueName(queueName);
    	item.setMessage("Hello World".getBytes());
    	
    	request.setQueueItem(item);
    	
		try {
			QueueResponse response = service.add(request);
			System.out.println(String.format("Status Code: %s", response.getStatus().getCode()));
			System.out.println(String.format("Status Message: %s", response.getStatus().getMessage()));
			System.out.println(String.format("QueueReceipt: %s", response.getQueueReceipt()));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void poll(final String queueName, ReliableMessageQueueServiceSoap service) {
		
		QueueRequest request = new QueueRequest();
		request.setQueueName(queueName);
    	
		try {
			QueueResponse response = service.poll(request);
			System.out.println(String.format("Status Code: %s", response.getStatus().getCode()));
			System.out.println(String.format("Status Message: %s", response.getStatus().getMessage()));
			final QueueItem queueItem = response.getQueueItem();
			System.out.println(String.format("QueueItem: %s", queueItem));
			if (queueItem != null) {
				// debug
				System.out.println(String.format("Queue Name: %s", queueItem.getQueueName()));
				System.out.println(String.format("Queue Receipt: %s", queueItem.getQueueReceipt()));
				System.out.println(String.format("Message: %s", new String(queueItem.getMessage())));
				// commit
				request.setQueueReceipt(queueItem.getQueueReceipt());
				response = service.commit(request);
				// debug
				System.out.println(String.format("Status Code: %s", response.getStatus().getCode()));
				System.out.println(String.format("Status Message: %s", response.getStatus().getMessage()));
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
