
package biz.firstlook.service.reliablemessagequeue.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QueueResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueueResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.firstlook.biz/service/ReliableMessageQueue/v1}Status"/>
 *         &lt;choice>
 *           &lt;element ref="{http://www.firstlook.biz/service/ReliableMessageQueue/v1}QueueReceipt"/>
 *           &lt;element ref="{http://www.firstlook.biz/service/ReliableMessageQueue/v1}QueueItem"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueueResponse", propOrder = {
    "status",
    "queueReceipt",
    "queueItem"
})
public class QueueResponse {

    @XmlElement(name = "Status", required = true)
    protected Status status;
    @XmlElement(name = "QueueReceipt")
    protected String queueReceipt;
    @XmlElement(name = "QueueItem")
    protected QueueItem queueItem;

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link Status }
     *     
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link Status }
     *     
     */
    public void setStatus(Status value) {
        this.status = value;
    }

    /**
     * Gets the value of the queueReceipt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueueReceipt() {
        return queueReceipt;
    }

    /**
     * Sets the value of the queueReceipt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueueReceipt(String value) {
        this.queueReceipt = value;
    }

    /**
     * Gets the value of the queueItem property.
     * 
     * @return
     *     possible object is
     *     {@link QueueItem }
     *     
     */
    public QueueItem getQueueItem() {
        return queueItem;
    }

    /**
     * Sets the value of the queueItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueueItem }
     *     
     */
    public void setQueueItem(QueueItem value) {
        this.queueItem = value;
    }

}
