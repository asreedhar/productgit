
package biz.firstlook.service.reliablemessagequeue.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the biz.firstlook.service.reliablemessagequeue.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _QueueResponse_QNAME = new QName("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueResponse");
    private final static QName _Status_QNAME = new QName("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "Status");
    private final static QName _QueueRequest_QNAME = new QName("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueRequest");
    private final static QName _QueueItem_QNAME = new QName("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueItem");
    private final static QName _QueueReceipt_QNAME = new QName("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueReceipt");
    private final static QName _QueueName_QNAME = new QName("http://www.firstlook.biz/service/ReliableMessageQueue/v1", "QueueName");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: biz.firstlook.service.reliablemessagequeue.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Status }
     * 
     */
    public Status createStatus() {
        return new Status();
    }

    /**
     * Create an instance of {@link QueueItem }
     * 
     */
    public QueueItem createQueueItem() {
        return new QueueItem();
    }

    /**
     * Create an instance of {@link QueueRequest }
     * 
     */
    public QueueRequest createQueueRequest() {
        return new QueueRequest();
    }

    /**
     * Create an instance of {@link QueueResponse }
     * 
     */
    public QueueResponse createQueueResponse() {
        return new QueueResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueueResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.firstlook.biz/service/ReliableMessageQueue/v1", name = "QueueResponse")
    public JAXBElement<QueueResponse> createQueueResponse(QueueResponse value) {
        return new JAXBElement<QueueResponse>(_QueueResponse_QNAME, QueueResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Status }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.firstlook.biz/service/ReliableMessageQueue/v1", name = "Status")
    public JAXBElement<Status> createStatus(Status value) {
        return new JAXBElement<Status>(_Status_QNAME, Status.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueueRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.firstlook.biz/service/ReliableMessageQueue/v1", name = "QueueRequest")
    public JAXBElement<QueueRequest> createQueueRequest(QueueRequest value) {
        return new JAXBElement<QueueRequest>(_QueueRequest_QNAME, QueueRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueueItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.firstlook.biz/service/ReliableMessageQueue/v1", name = "QueueItem")
    public JAXBElement<QueueItem> createQueueItem(QueueItem value) {
        return new JAXBElement<QueueItem>(_QueueItem_QNAME, QueueItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.firstlook.biz/service/ReliableMessageQueue/v1", name = "QueueReceipt")
    public JAXBElement<String> createQueueReceipt(String value) {
        return new JAXBElement<String>(_QueueReceipt_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.firstlook.biz/service/ReliableMessageQueue/v1", name = "QueueName")
    public JAXBElement<String> createQueueName(String value) {
        return new JAXBElement<String>(_QueueName_QNAME, String.class, null, value);
    }

}
