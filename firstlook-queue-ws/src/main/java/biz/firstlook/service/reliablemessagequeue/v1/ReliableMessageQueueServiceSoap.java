
package biz.firstlook.service.reliablemessagequeue.v1;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(name = "ReliableMessageQueueServiceSoap", targetNamespace = "http://www.firstlook.biz/service/ReliableMessageQueue/v1")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface ReliableMessageQueueServiceSoap {


    @WebMethod(operationName = "poll", action = "https://www.firstlook.biz/firstlook-queue-ws/services/ReliableMessageQueue/poll")
    @WebResult(name = "QueueResponse", targetNamespace = "http://www.firstlook.biz/service/ReliableMessageQueue/v1")
    public QueueResponse poll(
        @WebParam(name = "QueueRequest", targetNamespace = "http://www.firstlook.biz/service/ReliableMessageQueue/v1")
        biz.firstlook.service.reliablemessagequeue.v1.QueueRequest QueueRequest);

    @WebMethod(operationName = "commit", action = "https://www.firstlook.biz/firstlook-queue-ws/services/ReliableMessageQueue/commit")
    @WebResult(name = "QueueResponse", targetNamespace = "http://www.firstlook.biz/service/ReliableMessageQueue/v1")
    public QueueResponse commit(
        @WebParam(name = "QueueRequest", targetNamespace = "http://www.firstlook.biz/service/ReliableMessageQueue/v1")
        biz.firstlook.service.reliablemessagequeue.v1.QueueRequest QueueRequest);

    @WebMethod(operationName = "rollback", action = "https://www.firstlook.biz/firstlook-queue-ws/services/ReliableMessageQueue/rollback")
    @WebResult(name = "QueueResponse", targetNamespace = "http://www.firstlook.biz/service/ReliableMessageQueue/v1")
    public QueueResponse rollback(
        @WebParam(name = "QueueRequest", targetNamespace = "http://www.firstlook.biz/service/ReliableMessageQueue/v1")
        biz.firstlook.service.reliablemessagequeue.v1.QueueRequest QueueRequest);

    @WebMethod(operationName = "add", action = "https://www.firstlook.biz/firstlook-queue-ws/services/ReliableMessageQueue/add")
    @WebResult(name = "QueueResponse", targetNamespace = "http://www.firstlook.biz/service/ReliableMessageQueue/v1")
    public QueueResponse add(
        @WebParam(name = "QueueRequest", targetNamespace = "http://www.firstlook.biz/service/ReliableMessageQueue/v1")
        biz.firstlook.service.reliablemessagequeue.v1.QueueRequest QueueRequest);

}
