
USE IMT
GO

CREATE TABLE dbo.ReliableMessageQueue (
	Id INT IDENTITY(1,1) NOT NULL,
	QueueName VARCHAR(255),
	Message VARBINARY(MAX),
	Ready BIT,
	CONSTRAINT PK_ReliableMessageQueue PRIMARY KEY CLUSTERED (
		Id
	)
)
GO
