/* token delimiters for cut & paste:
{|{}|}
*/

/*

Notes

You need to install node.js if you haven't already. As of 2013, they were making this REAL easy at http://nodejs.org/

You won't even need to set a path.

So assuming version 50.0 and our convention of putting things in a projects directory:
(Node doesn't care how you name them but it will assume the individual project dir is also the version number)

You should see:

<root>/projects/50.0/flConf.js
<root>/projects/50.0/universal_config //with a bunch of tokenized config files inside
<root>/projects/50.0/node_modules //one node dependency for flConf.js

//the above should all be pulled from the remote repo

and in projects or whatever folder you put your dev builds in

<root>/projects/flConf.json

//get a json file from another dev or copy the one in universal config and remove comments

Process is real simple:

1. Put the values you want in the json file.
2. And from <root>/projects/50.0 run the following command:

node flConf.js

What happens:

The tokenized files have values inserted from your JSON file and the files replace the ones they're mapped to
in defaultConfigFileMap below.

flConf also quietly writes a buildNumber.txt file at <root>/projects/50.0/IMT/build for you. This is not connected to fileMap or tokenization process
but it does get the version # from the root dir for the project.

flConf.json tips

You can add an object replacing null in configFileMap in the json file to override defaultConfigFileMap settings. Your file map properties are merged
over the default ones so you need only include the ones to override. Set values in  the json configFileMap to null to ignore listed target config files in default.

*/

var fs = require('fs'),
args = process.argv.slice(2),
interpolate = require('mutil').interpolate,
prjRoot = process.cwd().replace(/\\/g,'/'),
prjRoot = prjRoot.replace(/\:\//,':\\'),
srcRoot = prjRoot + '/universal_config/',
verNum = prjRoot.match(/\/([^\/\\]+)$/)[0].replace(/[\/\\]/g,''),
prjRoot = prjRoot.replace('/'+verNum,''),

issues = [],
successCnt = 0,
errorCnt = 0,

defaultConfigFileMap = { //configFileMap settings in your own flConf.json will override settings in here

	".NET/FirstLookServicesSolution/FirstLookServicesWebServiceStack/Web.config": "FirstLook.NET/MasterSolution/FirstLookServicesSolution/FirstLookServicesWebServiceStack/Web.config",
	
	".NET/ClientSolution/ClientDomainModel_app.config": "FirstLook.Net/MasterSolution/ClientSolution/ClientDomainModel/app.config",
	".NET/ClientSolution/ClientDomainModelTest_app.config": "FirstLook.Net/MasterSolution/ClientSolution/ClientDomainModel_Test/App.config",
	".NET/ClientSolution/ClientWebApplication_cas-debug.config": "FirstLook.Net/MasterSolution/ClientSolution/ClientWebApplication/Configuration/cas-debug.config",
	".NET/ClientSolution/ClientWebApplication_connectionStrings-debug.config": "FirstLook.Net/MasterSolution/ClientSolution/ClientWebApplication/Configuration/connectionStrings-debug.config",
	".NET/ClientSolution/ClientWebApplication_Web.config": "FirstLook.Net/MasterSolution/ClientSolution/ClientWebApplication/Web.config",
	
	".NET/PricingSolution/PricingWebApplication_appSetttings-debug.config": "FirstLook.Net/MasterSolution/PricingSolution/PricingWebApplication/Configuration/appSettings-debug.config",
	".NET/PricingSolution/PricingWebApplication_cas-debug.config": "FirstLook.Net/MasterSolution/PricingSolution/PricingWebApplication/Configuration/cas-debug.config",
	".NET/PricingSolution/PricingWebApplication_connectionStrings-debug.config": "FirstLook.Net/MasterSolution/PricingSolution/PricingWebApplication/Configuration/connectionStrings-debug.config",
	
	".NET/SharedSolution/SupportWebsite_appSettings-debug.config": "FirstLook.Net/MasterSolution/SharedSolution/SupportWebsite/Configuration/appSettings-debug.config",
	".NET/SharedSolution/SupportWebsite_cas-debug.config": "FirstLook.Net/MasterSolution/SharedSolution/SupportWebsite/Configuration/cas-debug.config",
	".NET/SharedSolution/SupportWebsite_connectionStrings-debug.config": "FirstLook.Net/MasterSolution/SharedSolution/SupportWebsite/Configuration/connectionStrings-debug.config",
	
	".NET/SisManagementSolution/SisManagementWebsite_appSettings-debug.config": "FirstLook.Net/MasterSolution/SisManagementSolution/SisManagementWebsite/Configuration/appSettings-debug.config",
	".NET/SisManagementSolution/SisManagementWebsite_connectionStrings-debug.config": "FirstLook.Net/MasterSolution/SisManagementSolution/SisManagementWebsite/Configuration/connectionStrings-debug.config",
	
	".NET/VehicleHistoryReportSolution/VehicleHistoryReportWebApplication_cas-debug.config": "FirstLook.Net/MasterSolution/VehicleHistoryReportSolution/VehicleHistoryReportWebApplication/Configuration/cas-debug.config",
	".NET/VehicleHistoryReportSolution/VehicleHistoryReportWebApplication_connectionStrings-debug.config": "FirstLook.Net/MasterSolution/VehicleHistoryReportSolution/VehicleHistoryReportWebApplication/Configuration/connectionStrings-debug.config",
	
	".NET/VehicleHistoryReportSolution/VehicleHistoryReportWebService_connectionStrings-debug.config": "FirstLook.NET/MasterSolution/VehicleHistoryReportSolution/VehicleHistoryReportWebService/Configuration/connectionStrings-debug.config",
	
	".NET/VehicleInformationSolution/VehicleInformationWebApplication_cas-debug.config": "FirstLook.Net/MasterSolution/VehicleInformationSolution/VehicleInformationWebApplication/Configuration/cas-debug.config",
	
	".NET/VehicleValuationGuideSolution/VehicleValuationGuideWebApplication_cas-debug.config": "FirstLook.Net/MasterSolution/VehicleValuationGuideSolution/VehicleValuationGuideWebApplication/Configuration/cas-debug.config",
	
	".NET/ReportCenterSolution/CommandCenterWebsite/appSettings-debug.config":"FirstLook.NET/MasterSolution/ReportCenterSolution/CommandCenterWebsite/Configuration/appSettings-debug.config",
	".NET/ReportCenterSolution/CommandCenterWebsite/cas-debug.config":"FirstLook.NET/MasterSolution/ReportCenterSolution/CommandCenterWebsite/Configuration/cas-debug.config",
	".NET/ReportCenterSolution/CommandCenterWebsite/connectionStrings-debug.config":"FirstLook.NET/MasterSolution/ReportCenterSolution/CommandCenterWebsite/Configuration/connectionStrings-debug.config",
	
	"Rails/database.yml": "hal/config/database.yml",
	"Rails/environment.rb": "hal/config/environment.rb",
	"Rails/environments_development.rb": "hal/config/environments/development.rb",

	"Java/Tomcat/server.xml": "C:\\Program Files/Apache Software Foundation/apache-tomcat-5.5/conf/server.xml",

	"Java/buildConfigs/requestQProcessor/build.xml":"fl-appraisal-request-q-processor/build.xml"

},

//assumes json is located in your projects folder as in C:\projects if C:\projects\50.0
configJson = JSON.parse(fs.readFileSync(prjRoot + '/flConf.json').toString()),

configSettings = configJson.configSettings,
totalFiles = 0,
//this where any json overrides would merge over
configFileMap = mergeObjects( defaultConfigFileMap, configJson.configFileMap || {} ),

fileDiffs = [];

configSettings.projectsRoot = prjRoot; //plugging in automagically now
configSettings.versionNumber = verNum; //plugging in automagically now
configSettings.dbHalConnection = configSettings.dbHalConnection || (configSettings.dbConnection + '_HAL');

function statusCheck(){
	if( (successCnt + errorCnt) === totalFiles){
		if(errorCnt){
			console.log('\n' + errorCnt + ' ERRORS:\n' + issues.join(''));
		}
		console.log('\n' + successCnt+'/'+totalFiles + ' configurations set.');
	}
}

function setConfig(srcConfigPath,targetPath){
	if(!setConfig.cnt){ setConfig.cnt = 1; }
	console.log(srcConfigPath);
	fs.readFile(srcConfigPath, function(err, buffer){
		
		var srcFileTxt = buffer.toString();
		
		srcFileTxt = interpolate(srcFileTxt, configSettings,{delimiter: '{|{}|}'});
		
		fs.writeFile( targetPath, srcFileTxt, function(err){
			if(err){
				errorCnt++;
				issues.push('\nNode Error:\n    ' + err);
			}
			else {
				successCnt++;
			}
			statusCheck();
		} );
	} );

}

function mergeObjects(){
	var i = arguments.length,
	merger = {};
	
	for (var i = 0, len = arguments.length; i < len; i++){
		var thisObj = arguments[i];
		
		if(thisObj !== null){
			for(var x in thisObj){
				
				if( !/^COMMENT/.test(x) ){
					
					if( thisObj[x] === null && merger[x] ){
						console.log('IGNORED: ' + merger[x]);
						delete merger[x];
					}
					else {
						merger[x] = thisObj[x];
						totalFiles++;
					}
					
				}
				
			}
		}
	}
	
	return merger;
}

//run
if(args[1] !== 'update'){
	console.log('\nREPLACED:\n');
	
	for(var x in configFileMap){
		var src = srcRoot + x,
		target = configFileMap[x].match(/^C:\\/i) ? configFileMap[x] : prjRoot + '/' + verNum +'/' + configFileMap[x]; //sets target from windows OS-root when explicit
		
		if(fs.existsSync(target)){
			setConfig(src, target);
		}
		else {
			errorCnt++;
			issues.push('\nNo file found at: \n    '+target);
		}
	}
	
	//adding a buildNumber.txt
	var buildNumberPath = prjRoot + '/'+verNum+'/'+'IMT/build/';
	process.chdir(buildNumberPath);
	fs.openSync('buildNumber.txt', 'w');
	fs.writeFileSync('buildNumber.txt', 'buildNumber=' + verNum + '.x.y');
	process.chdir(srcRoot);
	console.log('\nbuildNumber.txt file written to: ' + buildNumberPath + '\n(not counted as one of the ' + totalFiles + ' config files)');
	
}
//when uncommented, using the update arg, e.g. (node firstLookConfig.js 14.2 update) pulls from app config files and puts them in your universal_config folder nuking all your tokenized versions.
//so commented out for safety.
/*
else {
	for(var x in defaultConfigFileMap){
		var srcPath = srcRoot+x,
		targetBuffer = fs.readFileSync(prjRoot + configFileMap[x]);
		fs.openSync( srcPath,'w+');
		console.log(srcPath);
		fs.writeFileSync(srcPath, targetBuffer);
	}
}
*/