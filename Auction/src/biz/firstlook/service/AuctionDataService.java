package biz.firstlook.service;

import java.util.ArrayList;
import java.util.List;

import biz.firstlook.DAO.IAreaDAO;
import biz.firstlook.DAO.IAuctionDataDAO;
import biz.firstlook.DAO.IAuctionReportDAO;
import biz.firstlook.DAO.IPeriodDAO;
import biz.firstlook.DAO.IUIDDAO;
import biz.firstlook.DAO.IVICBodyStyleDAO;
import biz.firstlook.entity.Area;
import biz.firstlook.entity.AuctionData;
import biz.firstlook.entity.AuctionReport;
import biz.firstlook.entity.Period;
import biz.firstlook.entity.VICBodyStyle;

public class AuctionDataService implements IAuctionDataService
{

private IAreaDAO areaDAO;
private IPeriodDAO periodDAO;
private IVICBodyStyleDAO vicBodyStyleDAO;
private IAuctionReportDAO auctionReportDAO;
private IAuctionDataDAO auctionDataDAO;
private IUIDDAO uidDAO;

public IUIDDAO getUidDAO() {
	return uidDAO;
}

public void setUidDAO(IUIDDAO uidDAO) {
	this.uidDAO = uidDAO;
}

public List< Period > retrieveAllTimePeriods()
{
    return periodDAO.retrieveAll();
}

public Period retrieveByPeriodId( Integer periodId )
{
	return periodDAO.retrieveById( periodId);
}

public List< Area > retrieveAllAreas()
{
    return areaDAO.retrieveAll();
}

public Area retrieveByAreaId( Integer areaId )
{
    return areaDAO.retrieveById(areaId);
}

public List< VICBodyStyle > retrieveVICBodyStyles( String vin )
{
	return vicBodyStyleDAO.retrieve( vin );
}

public List< AuctionReport > retrieveAuctionReport( Integer periodId, String vic, Integer areaId, Boolean usingVIC, Integer modelYear )
{
	List< AuctionReport > auctionReport = null;
	if ( usingVIC )
	{
		auctionReport = auctionReportDAO.retrieveByVIC(periodId, vic, areaId );
	}
	else
	{
		auctionReport = auctionReportDAO.retrieveByMMG( periodId, vic, areaId, modelYear );
	}
	return auctionReport;
}

public AuctionData retrieveAuctionData( String vic, Integer areaId, Integer periodId, Integer mileage, Boolean usingVIC, Integer modelYear )
{
    Integer lowMileage = computeLowMileage( mileage );
	Integer highMileage = computeHighMileage( lowMileage );
	AuctionData auctionData = null;
	if ( usingVIC )
	{
		auctionData = auctionDataDAO.retrieveByVIC(periodId, vic, areaId, lowMileage, highMileage  );
	}
	else
	{
		auctionData = auctionDataDAO.retrieveByMMG( periodId, Integer.parseInt( vic ), areaId, highMileage, lowMileage, modelYear );
	}
	return auctionData;
}


private Integer computeLowMileage( Integer mileage )
{
	return new Integer( ( mileage / 10000 ) * 10000 );
}

private Integer computeHighMileage( Integer lowMileage )
{
	return new Integer( ( lowMileage.intValue() + 9999 ) );
}

public void setAreaDAO( IAreaDAO areaDAO )
{
	this.areaDAO = areaDAO;
}

public void setAuctionDataDAO( IAuctionDataDAO auctionTransactionDAO )
{
	this.auctionDataDAO = auctionTransactionDAO;
}

public void setAuctionReportDAO( IAuctionReportDAO auctionTransactionReportDAO )
{
	this.auctionReportDAO = auctionTransactionReportDAO;
}

public void setVicBodyStyleDAO( IVICBodyStyleDAO seriesBodyStyleDAO )
{
	this.vicBodyStyleDAO = seriesBodyStyleDAO;
}

public void setPeriodDAO( IPeriodDAO periodDAO )
{
	this.periodDAO = periodDAO;
}

public List<VICBodyStyle> retrieveUIDs(String vin, Integer year) throws Exception {
	if (vin == null || vin.length() < 10) {
		throw new Exception( "Invalid VIN");
	}
	return uidDAO.retrieveUID( vin, year );
}

public List< VICBodyStyle > retrieveVICs(String uids) {
	return vicBodyStyleDAO.retrieveVICs(uids);
}
}
