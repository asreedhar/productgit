package biz.firstlook.service;

import java.util.ArrayList;
import java.util.List;

import biz.firstlook.entity.Area;
import biz.firstlook.entity.AuctionData;
import biz.firstlook.entity.AuctionReport;
import biz.firstlook.entity.Period;
import biz.firstlook.entity.VICBodyStyle;

public interface IAuctionDataService
{

public List<Period> retrieveAllTimePeriods();
public List<Area> retrieveAllAreas();
public Period retrieveByPeriodId( Integer periodId );
public Area retrieveByAreaId( Integer areaId );
public List< VICBodyStyle > retrieveVICBodyStyles( String vin );
public List< AuctionReport > retrieveAuctionReport( Integer period, String vic, Integer areaId, Boolean usingVIC, Integer year );
public AuctionData retrieveAuctionData( String vic, Integer areaId, Integer timePeriodId, Integer mileage, Boolean usingVIC, Integer year );
List<VICBodyStyle> retrieveUIDs(String vin, Integer year) throws Exception;
public List< VICBodyStyle > retrieveVICs(String uids);
}
