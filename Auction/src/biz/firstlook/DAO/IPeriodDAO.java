package biz.firstlook.DAO;

import java.util.List;

import biz.firstlook.entity.Period;

public interface IPeriodDAO
{
public List< Period > retrieveAll();
public Period retrieveById( Integer periodId );
}
