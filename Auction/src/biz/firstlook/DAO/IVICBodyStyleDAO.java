package biz.firstlook.DAO;

import java.util.ArrayList;
import java.util.List;

import biz.firstlook.entity.VICBodyStyle;

public interface IVICBodyStyleDAO
{
public List< VICBodyStyle > retrieve( String vin );
public List< VICBodyStyle > retrieveVICs( String uids);

}