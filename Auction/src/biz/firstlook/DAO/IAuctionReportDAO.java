package biz.firstlook.DAO;

import java.util.List;

import biz.firstlook.entity.AuctionReport;

public interface IAuctionReportDAO
{
public List< AuctionReport > retrieveByVIC( Integer periodId, String vic, Integer areaId );
public List< AuctionReport > retrieveByMMG( Integer periodId, String vic, Integer areaId, Integer modelYear );
}
