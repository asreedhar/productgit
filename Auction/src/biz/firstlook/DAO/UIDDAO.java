package biz.firstlook.DAO;

import java.sql.Types;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.commons.sql.SqlParameterWithValue;
import biz.firstlook.commons.sql.StoredProcedureTemplate;
import biz.firstlook.commons.sql.StoredProcedureTemplateParameters;
import biz.firstlook.entity.VICBodyStyle;

public class UIDDAO extends StoredProcedureTemplate implements IUIDDAO {

	public List<VICBodyStyle> retrieveUID(String vin, Integer year) {

	   String trimmedVIN = vin.substring( 0, 8 )  + vin.substring( 9, 10 );
	   
		List<SqlParameter> parameters = new LinkedList<SqlParameter>(); 
		parameters.add( new SqlParameterWithValue( "@vinPattern", Types.VARCHAR, trimmedVIN ) );
		parameters.add( new SqlParameterWithValue( "@Year", Types.INTEGER, year) );
		
		StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();

		sprocRetrieverParams.setStoredProcedureCallString( "{? = call VehicleCatalog.Firstlook.GetNadaDataFromVinPatternAndYear(?,?)}" );
		sprocRetrieverParams.setSqlParametersWithValues( parameters );
		
		Map results = this.call( sprocRetrieverParams, new UIDMapper());
		
		return (List<VICBodyStyle>)results.get( StoredProcedureTemplate.RESULT_SET );
		
	}

}
