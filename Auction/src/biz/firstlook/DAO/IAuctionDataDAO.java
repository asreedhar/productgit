package biz.firstlook.DAO;

import biz.firstlook.entity.AuctionData;

public interface IAuctionDataDAO
{
public AuctionData retrieveByMMG( Integer periodId, Integer mmg, Integer regionId, Integer highMileage, Integer lowMileage, Integer year );
public AuctionData retrieveByVIC( Integer periodId, String vic, Integer regionId, Integer highMileage, Integer lowMileage );
}