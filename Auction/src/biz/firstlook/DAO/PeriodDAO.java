package biz.firstlook.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.entity.Period;

public class PeriodDAO extends JdbcDaoSupport implements IPeriodDAO
{

public List< Period > retrieveAll()
{
	String sql = "SELECT * FROM PeriodDates order by rank";
	return (List< Period >)getJdbcTemplate().query( sql, new TimePeriodAuctionRowMapper() );
}

public Period retrieveById( Integer id )
{
	String sql = "SELECT * FROM PeriodDates WHERE PeriodID = ? order by rank";
	Object[] parameters = { id };
	List< Period > results = getJdbcTemplate().query( sql, parameters, new TimePeriodAuctionRowMapper() );
	if ( results == null || results.size() == 0 )
	{
		return null;
	}
	return results.get( 0 );
}

class TimePeriodAuctionRowMapper implements RowMapper
{
	public Object mapRow( ResultSet row, int arg1 ) throws SQLException
	{
		Period timePeriod = new Period();
		timePeriod.setPeriodId( row.getInt( "PeriodID" ) );
		timePeriod.setRank( row.getInt( "Rank" )  );
		timePeriod.setDescription( row.getString( "Description" ) );
		return timePeriod;
	}
}


}
