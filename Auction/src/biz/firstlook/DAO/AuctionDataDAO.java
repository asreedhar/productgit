package biz.firstlook.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.entity.AuctionData;

public class AuctionDataDAO extends JdbcDaoSupport implements
        IAuctionDataDAO
{

public AuctionDataDAO()
{
    super();
}

public AuctionData retrieveByMMG( Integer periodId, Integer mmg, Integer areaId, Integer highMileage, Integer lowMileage, Integer year )
{
	StringBuilder sql = new StringBuilder( "SELECT	*, A4.AveSalePrice AS SimilarVehicleAveSalePrice, A4.TransactionCount AS SimilarVehicleTransactionCount FROM	AuctionTransaction_A3 A3 LEFT JOIN AuctionTransaction_A4 A4 ON A3.MakeModelGroupingID = A4.MakeModelGroupingID "); 
	sql.append( " AND A3.PeriodId = A4.PeriodId AND A3.AreaId = A4.AreaId AND A3.ModelYear = A4.ModelYear  AND A4.LowMileageRange = ? AND A4.HighMileageRange = ? ");
	sql.append( " WHERE A3.AreaId = ? AND A3.PeriodID = ? AND A3.MakeModelGroupingID = ? AND A3.ModelYear = ? ");
	Object[] parameters = { lowMileage, highMileage, areaId, periodId, mmg, year };
	
	List< AuctionData > results = getJdbcTemplate().query( sql.toString(), parameters, new AuctionTransactionRowMapper() );
	if ( results == null || results.size() == 0 )
	{
		return null;
	}
	AuctionData auctionData = results.get( 0 );
	auctionData.setSimilarVehicleHighMileage( highMileage );
	auctionData.setSimilarVehicleLowMileage( lowMileage );
	if ( auctionData.getSimilarVehicleAverageSalePrice().equals( 0d ) )
	{
		auctionData.setSimilarVehicleAverageSalePrice( null );
	}
	return auctionData;
}


public AuctionData retrieveByVIC( Integer periodId, String vic, Integer areaId, Integer lowMileage, Integer highMileage )
{
	StringBuilder sql = new StringBuilder( "SELECT	*, A2.TransactionCount AS SimilarVehicleTransactionCount FROM	AuctionTransaction_AV1 A1 LEFT JOIN AuctionTransaction_AV2 A2 ON A1.VIC = A2.VIC "); 
	sql.append( " AND A1.PeriodId = A2.PeriodId AND A1.AreaId = A2.AreaId AND A2.LowMileageRange = ? AND A2.HighMileageRange = ? ");
	sql.append( "  WHERE A1.AreaId = ? AND A1.PeriodID = ? AND A1.VIC = ? ");
	Object[] parameters = { lowMileage, highMileage, areaId, periodId, vic };
	
	List< AuctionData > results = getJdbcTemplate().query( sql.toString(), parameters, new AuctionTransactionRowMapper() );
	if ( results == null || results.size() == 0 )
	{
		return null;
	}
	AuctionData auctionData = results.get( 0 );
	auctionData.setSimilarVehicleHighMileage( highMileage );
	auctionData.setSimilarVehicleLowMileage( lowMileage );
	if ( auctionData.getSimilarVehicleAverageSalePrice().equals( 0d ) )
	{
		auctionData.setSimilarVehicleAverageSalePrice( null );
	}
	return auctionData;
}

class AuctionTransactionRowMapper implements RowMapper
{
	public Object mapRow( ResultSet row, int arg1 ) throws SQLException
	{
		AuctionData auctionData = new AuctionData();
		
		auctionData.setAverageSalePrice( row.getDouble( "AveSalePrice" ) );
		auctionData.setAverageSalePriceMileage( row.getDouble( "AveMileage" ) );
		auctionData.setHighSalePriceMileage( row.getDouble( "HighMileage" ) );
		auctionData.setHighSalePrice( row.getDouble( "HighSalePrice" ) );
		auctionData.setLowSalePriceMileage( row.getDouble( "LowMileage" ) );
		auctionData.setLowSalePrice( row.getDouble( "LowSalePrice" ) );
		auctionData.setTransactionCount( row.getInt( "TransactionCount" ) );
	    auctionData.setSimilarVehicleAverageSalePrice(row.getDouble( "SimilarVehicleAveSalePrice" ) );
	    auctionData.setSimilarVehicleTransactionCount(row.getInt("SimilarVehicleTransactionCount"));
		return auctionData;
	}
}

}
