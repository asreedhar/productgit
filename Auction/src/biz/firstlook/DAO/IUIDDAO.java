package biz.firstlook.DAO;

import java.util.List;

import biz.firstlook.entity.VICBodyStyle;

public interface IUIDDAO {
	public List<VICBodyStyle> retrieveUID( String vin, Integer year ) ;

}
