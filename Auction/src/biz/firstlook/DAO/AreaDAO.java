package biz.firstlook.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.entity.Area;

public class AreaDAO extends JdbcDaoSupport implements IAreaDAO
{

@SuppressWarnings("unchecked")
public List< Area > retrieveAll()
{
	String sql = "SELECT * FROM Area_D";
	return (List< Area >)getJdbcTemplate().query( sql, new AreaAuctionRowMapper() );
}

public Area retrieveById( Integer id )
{
	String sql = "SELECT * FROM Area_D WHERE AreaID = ?";
	Object[] parameters = { id };
	List< Area > results = getJdbcTemplate().query( sql, parameters, new AreaAuctionRowMapper() );
	if ( results == null || results.size() == 0 )
	{
		return null;
	}
	return results.get( 0 );
}

class AreaAuctionRowMapper implements RowMapper
{
	public Object mapRow( ResultSet row, int arg1 ) throws SQLException
	{
		Area area = new Area();
		area.setAreaId( row.getInt( "AreaID" ) );
		area.setAreaName( row.getString( "AreaName" ) );
		return area;
	}
}

}
