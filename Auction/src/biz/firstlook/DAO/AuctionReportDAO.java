package biz.firstlook.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.entity.AuctionReport;

public class AuctionReportDAO extends JdbcDaoSupport implements IAuctionReportDAO
{

public AuctionReportDAO()
{
    super();
}

public List< AuctionReport > retrieveByMMG( Integer periodId, String vic, Integer areaId, Integer modelYear )
{
	StringBuilder sql = new StringBuilder( "SELECT * FROM 	AuctionDetailReport adr JOIN Region_D r ON r.regionId = adr.regionId " );
	sql.append( "WHERE ADR.MakeModelGroupingId = ? AND adr.AreaId = ? AND adr.PeriodID = ? AND adr.modelYear = ? order by adr.SaleDate desc" );
	Object[] parameters = { vic, areaId, periodId, modelYear };
	
	return getJdbcTemplate().query( sql.toString(), parameters, new AuctionTransactionRowMapper() );
}

public List< AuctionReport > retrieveByVIC( Integer periodId, String vic, Integer areaId )
{
	StringBuilder sql = new StringBuilder( "SELECT * FROM 	AuctionDetailReport adr JOIN Region_D r ON r.regionId = adr.regionId " );
	sql.append( "WHERE ADR.VIC = ? AND adr.AreaId = ? AND adr.PeriodID = ? order by adr.SaleDate desc" );
	Object[] parameters = { vic, areaId, periodId };
	
	return getJdbcTemplate().query( sql.toString(), parameters, new AuctionTransactionRowMapper() );
}

class AuctionTransactionRowMapper implements RowMapper
{
	public Object mapRow( ResultSet row, int arg1 ) throws SQLException
	{
		AuctionReport auctionTransactionReport = new AuctionReport();
		auctionTransactionReport.setAreaId( row.getInt( "AreaID" ) );
		auctionTransactionReport.setAuctionTransactionId( row.getInt( "AuctionTransactionID" ) );
		auctionTransactionReport.setEngine( row.getString( "Engine" ) );
		auctionTransactionReport.setMileage( row.getInt( "Mileage" ) );
		auctionTransactionReport.setModelYear( row.getInt( "ModelYear" ) );
		auctionTransactionReport.setPeriodId( row.getInt( "PeriodID" ) );
		auctionTransactionReport.setRegionId( row.getInt( "RegionID" ) );
		auctionTransactionReport.setRegionName( row.getString( "RegionName" ) );
		auctionTransactionReport.setSaleDate( row.getDate( "SaleDate" ) );
		auctionTransactionReport.setSalePrice( row.getDouble( "SalePrice" ) );
		auctionTransactionReport.setSaleTypeName( row.getString( "SaleTypeName" ) );
		auctionTransactionReport.setVicBodyStyleDescription( row.getString( "Series") );
		auctionTransactionReport.setTransmission( row.getString( "Transmission" ) );
		return auctionTransactionReport;
	}
}

}
