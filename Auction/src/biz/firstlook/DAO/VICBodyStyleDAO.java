package biz.firstlook.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.entity.VICBodyStyle;

public class VICBodyStyleDAO extends JdbcDaoSupport  implements IVICBodyStyleDAO
{

public VICBodyStyleDAO()
{
    super();
}

public List< VICBodyStyle > retrieve( String vin )
{
	if (vin == null || vin.length() < 10) {
		List<VICBodyStyle> empty = Collections.emptyList();
		return empty;
	}
    StringBuffer query = new StringBuffer();
    String trimmedVIN = vin.substring( 0, 8 ) + "0" + vin.substring( 9, 10 );
    query.append( "SELECT vinP.VIC, vinP.VIC, series + ' ' + body AS BodyStyleDescription FROM VehicleUC..VINPrefix_VIC vinP JOIN VehicleUC..VIC_Catalog vinC ON vinC.VIC = vinP.VIC WHERE ? = VINPrefix " );
    Object[] parameters = { trimmedVIN };
    return getJdbcTemplate().query( query.toString(), parameters, new SeriesBodyStyleAuctionRowMapper() );
}

class SeriesBodyStyleAuctionRowMapper implements RowMapper
{
	public Object mapRow( ResultSet row, int arg1 ) throws SQLException
	{
		VICBodyStyle vicBodyStyle = new VICBodyStyle();
		vicBodyStyle.setVic( row.getString( "VIC" ) );
		vicBodyStyle.setBodyStyleDescription( row.getString( "BodyStyleDescription" ) );
		return vicBodyStyle;
	}
}

@SuppressWarnings("unchecked")
public List< VICBodyStyle > retrieveVICs( String uids) {
	
	 StringBuffer query = new StringBuffer();
	    query.append( "SELECT Uid, Vic FROM VehicleUC_New.dbo.Vehicles WHERE Uid in("+uids+")");
	    Object[] parameters = {};
	
	    return (List<VICBodyStyle>)getJdbcTemplate().query( query.toString(), parameters, new VICAndUIDRowMapper() );

}


class VICAndUIDRowMapper implements RowMapper
{
	public Object mapRow( ResultSet row, int arg1 ) throws SQLException
	{
		VICBodyStyle vicAndUid = new VICBodyStyle();
		vicAndUid.setUid(row.getInt("UID"));
		vicAndUid.setVic( row.getString( "VIC" ) );
		return vicAndUid;
	}
}

}
