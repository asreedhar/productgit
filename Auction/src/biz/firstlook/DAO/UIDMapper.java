package biz.firstlook.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import biz.firstlook.entity.VICBodyStyle;

public class UIDMapper implements RowMapper{

	public Object mapRow(ResultSet rs, int arg1) throws SQLException {
		
		VICBodyStyle uidBodyStyle= new VICBodyStyle();
		uidBodyStyle.setUid( rs.getInt( "Uid" ) );
		uidBodyStyle.setBodyStyleDescription( rs.getString( "SeriesName" )  + " " + rs.getString( "TRIM" ) );
		return uidBodyStyle;
	}

}
