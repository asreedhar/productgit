package biz.firstlook.DAO;

import java.util.List;

import biz.firstlook.entity.Area;

public interface IAreaDAO
{
public List< Area > retrieveAll();
public Area retrieveById( Integer areaId );
}
