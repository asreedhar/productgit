package biz.firstlook.entity;

/**
 * Area represents a geographical section of the country over which auction data can be searched.
 * 
 * @author kkelly
 */
public class Area
{
private Integer areaId;
private String areaName;

public Area()
{
	super();
}

public Integer getAreaId()
{
	return areaId;
}

public String getAreaName()
{
	return areaName;
}

public void setAreaId( Integer id )
{
	this.areaId = id;
}

public void setAreaName( String name )
{
	this.areaName = name;
}

}