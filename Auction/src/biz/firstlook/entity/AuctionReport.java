package biz.firstlook.entity;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class AuctionReport implements Serializable
{
private static final long serialVersionUID = -9033005687835163599L;
private Integer auctionTransactionId;
private Integer periodId;
private Integer vic;
private Integer regionId;
private Integer areaId;
private Integer modelYear;
private Date saleDate;
private String saleTypeName;
private Double salePrice;
private Integer mileage;
private String engine;
private String transmission;
private String vicBodyStyle;

private transient String regionName;

public AuctionReport()
{
    super();
}

public boolean equals( Object obj )
{
    return EqualsBuilder.reflectionEquals(this, obj);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(this);
}

public String getEngine()
{
    return engine;
}

public Integer getMileage()
{
    return mileage;
}

public Integer getPeriodId()
{
    return periodId;
}

public Integer getRegionId()
{
    return regionId;
}

public Date getSaleDate()
{
    return saleDate;
}

public Double getSalePrice()
{
    return salePrice;
}

public String getSaleTypeName()
{
    return saleTypeName;
}

public Integer getVic()
{
    return vic;
}

public String getTransmission()
{
    return transmission;
}

public void setEngine( String string )
{
    engine = string;
}

public void setMileage( Integer integer )
{
    mileage = integer;
}

public void setPeriodId( Integer integer )
{
    periodId = integer;
}

public void setRegionId( Integer integer )
{
    regionId = integer;
}

public void setSaleDate( Date date )
{
    saleDate = date;
}

public void setSalePrice( Double double1 )
{
    salePrice = double1;
}

public void setSaleTypeName( String string )
{
    saleTypeName = string;
}

public void setVic( Integer integer )
{
    vic = integer;
}

public void setTransmission( String string )
{
    transmission = string;
}

public String getVicBodyStyle()
{
    return vicBodyStyle;
}

public void setVicBodyStyleDescription( String string )
{
    vicBodyStyle = string;
}

public Integer getAuctionTransactionId()
{
    return auctionTransactionId;
}

public void setAuctionTransactionId( Integer integer )
{
    auctionTransactionId = integer;
}

public Integer getModelYear()
{
    return modelYear;
}

public void setModelYear( Integer integer )
{
    modelYear = integer;
}

public Integer getAreaId()
{
    return areaId;
}

public void setAreaId( Integer areaId )
{
    this.areaId = areaId;
}

public String getRegionName()
{
    return regionName;
}

public void setRegionName( String regionName )
{
    this.regionName = regionName;
}

}
