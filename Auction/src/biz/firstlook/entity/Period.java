package biz.firstlook.entity;

public class Period
{
private Integer periodId;
private String description;
private Integer rank;

public Period()
{
	super();
}

public Integer getPeriodId()
{
	return periodId;
}

public String getDescription()
{
	return description;
}

public void setPeriodId( Integer id )
{
	this.periodId = id;
}

public void setDescription( String description )
{
	this.description = description;
}

public Integer getRank()
{
	return rank;
}

public void setRank( Integer rank )
{
	this.rank = rank;
}

}
