package biz.firstlook.entity;


public class AuctionData
{

private Double highSalePrice;
private Double averageSalePrice;
private Double lowSalePrice;
private Double highSalePriceMileage;
private Double averageSalePriceMileage;
private Double lowSalePriceMileage;
private Double similarVehicleAverageSalePrice;
private Integer similarVehicleLowMileage;
private Integer similarVehicleHighMileage;
private Integer transactionCount;
private Integer similarVehicleTransactionCount;

public AuctionData()
{
}

public Double getAverageSalePrice()
{
    return averageSalePrice;
}

public Double getAverageSalePriceMileage()
{
    return averageSalePriceMileage;
}

public Double getHighSalePrice()
{
    return highSalePrice;
}

public Double getHighSalePriceMileage()
{
    return highSalePriceMileage;
}

public Double getLowSalePrice()
{
    return lowSalePrice;
}

public Double getLowSalePriceMileage()
{
    return lowSalePriceMileage;
}

public Double getSimilarVehicleAverageSalePrice()
{
    return similarVehicleAverageSalePrice;
}

public Integer getSimilarVehicleHighMileage()
{
    return similarVehicleHighMileage;
}

public Integer getSimilarVehicleLowMileage()
{
    return similarVehicleLowMileage;
}

public void setAverageSalePrice( Double double1 )
{
    averageSalePrice = double1;
}

public void setAverageSalePriceMileage( Double double1 )
{
    averageSalePriceMileage = double1;
}

public void setHighSalePrice( Double double1 )
{
    highSalePrice = double1;
}

public void setHighSalePriceMileage( Double double1 )
{
    highSalePriceMileage = double1;
}

public void setLowSalePrice( Double double1 )
{
    lowSalePrice = double1;
}

public void setLowSalePriceMileage( Double double1 )
{
    lowSalePriceMileage = double1;
}

public void setSimilarVehicleAverageSalePrice( Double double1 )
{
    similarVehicleAverageSalePrice = double1;
}

public void setSimilarVehicleHighMileage( Integer integer )
{
    similarVehicleHighMileage = integer;
}

public void setSimilarVehicleLowMileage( Integer integer )
{
    similarVehicleLowMileage = integer;
}

public Integer getTransactionCount()
{
    return transactionCount;
}

public void setTransactionCount( Integer integer )
{
    transactionCount = integer;
}

public Integer getSimilarVehicleTransactionCount() {
	return similarVehicleTransactionCount;
}

public void setSimilarVehicleTransactionCount(
		Integer similarVehicleTransactionCount) {
	this.similarVehicleTransactionCount = similarVehicleTransactionCount;
}

}
