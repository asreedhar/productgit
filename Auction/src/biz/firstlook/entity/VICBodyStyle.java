package biz.firstlook.entity;

public class VICBodyStyle
{
private String vic;
private String bodyStyleDescription;
private int uid;


public int getUid() {
	return uid;
}

public void setUid(int uid) {
	this.uid = uid;
}

public VICBodyStyle()
{
	super();
}

public VICBodyStyle( Integer makeModelGroupingId, String groupingDescription )
{
	this.vic = makeModelGroupingId.toString();
	this.bodyStyleDescription = groupingDescription;
}

public String getBodyStyleDescription()
{
	return bodyStyleDescription;
}

public void setBodyStyleDescription( String seriesBodyStyle )
{
	this.bodyStyleDescription = seriesBodyStyle;
}

public String getVic()
{
	return vic;
}

public void setVic( String vic )
{
	this.vic = vic;
}


}
