package biz.firstlook.export.reprice.impl;

/**
 * Value object
 * 
 * @author bfung
 * 
 */
public class EdtContact
{

private Integer businessUnitId;
private String businessUnit;
private String contactEmail;
private Integer failureCount;

public EdtContact( Integer businessUnitId, String businessUnit, String contactEmail, Integer failureCount )
{
	super();
	this.businessUnit = businessUnit;
	this.businessUnitId = businessUnitId;
	this.contactEmail = contactEmail;
	this.failureCount = failureCount;
}

public Integer getBusinessUnitId()
{
	return businessUnitId;
}

public String getContactEmail()
{
	return contactEmail;
}

public Integer getFailureCount()
{
	return failureCount;
}

public String getBusinessUnit()
{
	return businessUnit;
}

}
