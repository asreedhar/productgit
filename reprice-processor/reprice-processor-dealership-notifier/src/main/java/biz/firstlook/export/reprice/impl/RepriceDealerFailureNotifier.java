package biz.firstlook.export.reprice.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import biz.firstlook.commons.email.EmailService;

public class RepriceDealerFailureNotifier implements Runnable, InitializingBean
{

private static final Log log = LogFactory.getLog( RepriceDealerFailureNotifier.class );

private Integer failureRetryAttempts;

private EmailService emailService;
private RepriceDealerFailureDao repriceDealerFailureDao;

public static void main( String[] args )
{
	AbstractApplicationContext applicationContext = new ClassPathXmlApplicationContext( new String[] { "applicationContext.xml",
			"applicationContext-dataSource.xml" } );
	RepriceDealerFailureNotifier notifier = (RepriceDealerFailureNotifier)applicationContext.getBean( "repriceFailureNotifier" );
	notifier.run();
	applicationContext.destroy();
}

public RepriceDealerFailureNotifier()
{
}

public void afterPropertiesSet() throws Exception
{
	if ( failureRetryAttempts == null )
	{
		log.info( "No failure retry attempts set, defaulting to 5" );
		failureRetryAttempts = 5;
	}

	if ( emailService == null )
		throw new IllegalArgumentException( "EmailService was not set." );

	if ( repriceDealerFailureDao == null )
		throw new IllegalArgumentException( "RepriceAdvertiserFailureDao was not set." );
}

public void run()
{
	Date endDate = new Date();
	Date startDate = DateUtils.truncate( endDate, Calendar.DATE );

	List< EdtContact > items = repriceDealerFailureDao.getEdtContacts( startDate, endDate, failureRetryAttempts );

	Map< String, Map<String, Object> > addressAndEmailContext = new HashMap< String, Map<String, Object> >();
	for ( EdtContact contact : items )
	{
		if ( StringUtils.isNotBlank( contact.getContactEmail() ) )
		{
			Map< String, Object > emailContext = new HashMap< String, Object >();
			emailContext.put( "businessUnitId", contact.getBusinessUnitId() );
			addressAndEmailContext.put( contact.getContactEmail(), emailContext );
		}
		else
		{
			log.warn( "BusinessUnit: "
					+ contact.getBusinessUnit() + " has " + contact.getFailureCount() + " Reprice Failures but no ContactEmail setup for EDT!" );
		}
	}
	RepriceDealerFailureEmailBuilder report = new RepriceDealerFailureEmailBuilder( addressAndEmailContext );

	emailService.sendEmail( report );
}

public void setRepriceDealerFailureDao( RepriceDealerFailureDao dao )
{
	this.repriceDealerFailureDao = dao;
}

public void setEmailService( EmailService emailService )
{
	this.emailService = emailService;
}

public void setFailureRetryAttempts( Integer failureRetryAttempts )
{
	this.failureRetryAttempts = failureRetryAttempts;
}

}
