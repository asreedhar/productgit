package biz.firstlook.export.reprice.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.export.reprice.RepriceExportStatus;

public class RepriceDealerFailureDao extends JdbcDaoSupport
{

private static final String query;
private EdtContactRowMapper edtContactRowMapper = new EdtContactRowMapper();

static
{
	StringBuilder queryBuilder = new StringBuilder();
	queryBuilder.append( " SELECT i.BusinessUnitId, bu.BusinessUnit, iad.ContactEmail, count(*) as FailureCount" );
	queryBuilder.append( " FROM RepriceExport e " );
	queryBuilder.append( " JOIN RepriceEvent re ON e.repriceEventId = re.repriceEventId " );
	queryBuilder.append( " JOIN Inventory i ON re.inventoryId = i.inventoryId " );
	queryBuilder.append( " JOIN BusinessUnit bu ON i.businessUnitId = bu.businessUnitId " );
	queryBuilder.append( " JOIN InternetAdvertiserDealership iad ON i.businessUnitId = iad.businessUnitId " );
	queryBuilder.append( " AND e.InternetAdvertiserId = iad.InternetAdvertiserId" );
	queryBuilder.append( " WHERE e.RepriceExportStatusID = " ).append( RepriceExportStatus.Failure.getId() );
	queryBuilder.append( " AND e.FailureCount > ? " );
	queryBuilder.append( " AND e.DateSent between ? and ? " );
	queryBuilder.append( " GROUP BY i.BusinessUnitId, bu.BusinessUnit, iad.ContactEmail " );
	query = queryBuilder.toString();
}

@SuppressWarnings( "unchecked" )
public List< EdtContact > getEdtContacts( Date startDate, Date endDate, Integer failureRetryAttempts )
{
	List< EdtContact > items = getJdbcTemplate().query( query, new Object[] { failureRetryAttempts, startDate, endDate }, edtContactRowMapper );
	return items;
}

private class EdtContactRowMapper implements RowMapper
{
public Object mapRow( ResultSet rs, int index ) throws SQLException
{
	EdtContact contact = new EdtContact(	rs.getInt( "BusinessUnitId" ),
											rs.getString( "BusinessUnit" ),
											rs.getString( "ContactEmail" ),
											rs.getInt( "FailureCount" ) );
	return contact;
}
}

}
