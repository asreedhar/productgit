package biz.firstlook.export.reprice.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.io.Resource;

import biz.firstlook.commons.email.EmailContextBuilder;
import biz.firstlook.commons.email.EmailService.EmailFormat;

public class RepriceDealerFailureEmailBuilder implements EmailContextBuilder
{

private static final String TEMPLATE_NAME = "reprice-dealer-failure";
private static final String EMAIL_SUBJECT = "Reprice Failure Notification";

private Map<String, Map<String, Object>> emailAndContext;

public RepriceDealerFailureEmailBuilder( Map<String, Map<String, Object>> emailAndContext )
{
	this.emailAndContext = emailAndContext;
}

public Map< String, EmailFormat > getEmailFormats()
{
	Map<String, EmailFormat > formatMap = new HashMap< String, EmailFormat >();
	for( String emailAddress : emailAndContext.keySet() )
		formatMap.put( emailAddress, EmailFormat.HTML_MULTIPART );
	
	return formatMap;
}

public Map< String, Resource > getEmbeddedResources()
{
	return null;
}

public Map< String, String > getEmbeddedResourcesContentTypes()
{
	return null;
}

public Map< String, Map<String, Object> > getRecipientsAndContext()
{
	return emailAndContext;
}

public String getReplyTo()
{
	return null;
}

public String getSubject()
{
	return EMAIL_SUBJECT;
}

public String getTemplateName()
{
	return TEMPLATE_NAME;
}

public List<String> getCcEmailAddresses() {
	return Collections.emptyList();
}

}
