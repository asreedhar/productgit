<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<header>
	<title>Price Change Failure Notification</title>
</header>
<body>
<p>There was a problem sending a reprice update.  Please view the Price Change Failure Report in the Edge for more details.</p>
<p>Click <a href="https://www.firstlook.biz/IMT/LoginAction.go?productMode=edge&dealerId=${businessUnitId}&directTo=/PriceChangeFailureReport.go">here</a> to go to the report.</p>
</body>
</html>