<!--
	Parameters: deploy.environment (default: dev), deploymentDir
-->
<project name="reprice-processor-dealership-notifier" default="deploy" basedir=".">
	
	<path id="compile.lib">
		<fileset dir="lib">
			<include name="**/*.jar" />
			<exclude name="test/**" />
		</fileset>
		<fileset id="reprice.processor.common.libs" dir="../reprice-processor-common-libs">
			<include name="**/*.jar" />
			<exclude name="test/**"/>
		</fileset>
	</path>

	<path id="test.lib">
		<fileset dir="lib">
			<include name="**/*.jar" />
		</fileset>
		<fileset dir="../reprice-processor-common-libs">
			<include name="**/*.jar" />
		</fileset>
	</path>
	
	<!-- the fileset definition is used below in the "package" target -->
	<path id="module.dependencies">
		<fileset dir="../../dependencyJars">
			<include name="commons-1.0.jar" />
		</fileset>
		<fileset id="module.reprice.processor" dir="../reprice-processor/target/reprice-processor/lib">
			<include name="reprice-processor.jar" />
		</fileset>
	</path>

	<property name="src.dir" location="src/main/java" />
	<property name="resources.dir" location="src/main/resources" />
	<property name="freemarker.resources.dir" location="src/main/freemarker-templates" />

	<property name="build.dir" location="target" />
	<property name="classes.dir" location="${build.dir}/classes" />

	<property name="assembly.src.dir" location="src/main/assembly" />
	<property name="assembly.build.dir" location="${build.dir}/${ant.project.name}" />
	<property name="assembly.lib.dir" location="${build.dir}/${ant.project.name}/lib" />
	<property name="assembly.conf.dir" location="${build.dir}/${ant.project.name}/conf" />
	<property name="assembly.final.name" value="reprice-processor-dealership-notifier.zip" />
	
	<property name="test.src.dir" location="src/test/java" />
	<property name="test.resources.dir" location="src/test/resources" />
	<property name="test.classes.dir" location="${build.dir}/test-classes" />
	<property name="test.reports.dir" location="${build.dir}/junit-reports" />
	
	<!--
	<property name="fl-assembly-templates" value="src/main/fl-assembly-templates" />
	<property name="fl-resource-templates" value="src/main/fl-resource-templates" />
	-->

	<!-- ================================= 
          These properties are set to true if the file is available.
          This is done so skip targets as nescessary.
         ================================= -->
	<available file="${src.dir}" property="src.dir.available" />
	<available file="${resources.dir}" property="resources.dir.available" />
	<available file="${test.resources.dir}" property="test.resources.dir.available" />

	<!-- check for dependencies -->
	<available file="../../dependencyJars/commons-1.0.jar" property="commons.built" />
	<target name="dependency--commons" unless="commons.built">
		<ant dir="../../Commons" />
	</target>
	
	<available file="../reprice-processor/target/reprice-processor/lib/reprice-processor.jar" property="rp.built" />
	<target name="dependency--reprice-processor" unless="rp.built">
		<ant dir="../reprice-processor" target="deploy" inheritall="false"/>
	</target>
	
	<!-- manual jar/project management, assumes the commons project is checkout under the same parent directory as this project -->
	<target name="build-dependencies">
		<antcall target="dependency--commons" />
		<antcall target="dependency--reprice-processor">
			<param name="deploy.environment" value="${deploy.environment}" />
			<param name="deploymentDir" value="${deploymentDir}" />
		</antcall>
	</target>
	
	<target name="eclipse-build">
		<!-- need to put a database.properties here -->
		<copy tofile="${classes.dir}/database.properties" file="../configuration/dictionaries/${deploy.environment}.dictionary">
		</copy>
	</target>
	
	<!-- ================================= 
          target: process-resources
         ================================= -->
	<target name="process-resources" if="resources.dir.available" description="copy and process the resources into the destination directory, ready for packaging.">
		<echo message="Copying resources" />
		<copy todir="${classes.dir}">
			<fileset dir="${resources.dir}" />
		</copy>
		
		<!-- need to put a database.properties here -->
		<!-- default to dev.dictionary if non specified by -D on this buildfile invocation -->
		<property name="deploy.environment" value="dev" />
		<copy tofile="${classes.dir}/database.properties" file="${basedir}/../../Commons/configuration/dictionaries/${deploy.environment}.dictionary">
		</copy>
	</target>

	<!-- ================================= 
          target: compile
         ================================= -->
	<target name="compile" if="src.dir.available" depends="process-resources, build-dependencies" description="compile the source code of the project.">
		<echo message="[compiler:compile]" />
		
		<javac srcdir="${src.dir}" destdir="${classes.dir}" source="1.7" target="1.7">
			<classpath refid="compile.lib" />
			<classpath refid="module.dependencies" />
			<!-- just incase it matches stuff like "fastest" -->
			<exclude name="**/*Test*" />
			<exclude name="**/Mock*" />
		</javac>
	</target>
	
	<!-- ================================= 
          target: process-test-resources
         ================================= -->
	<target name="process-test-resources" if="test.resources.dir.available" description="copy and process the resources into the destination directory, ready for packaging.">
		<echo message="[resources:testResources]" />
		<mkdir dir="${test.classes.dir}" />
		<copy todir="${test.classes.dir}">
			<fileset dir="${test.resources.dir}" />
		</copy>
	</target>

	<!-- ================================= 
          target: test-compile
         ================================= -->
	<target name="test-compile" if="test.unitTests.run" depends="compile, process-test-resources, test.unitTests.check" description="compiles teste files">
		<echo message="[compiler:testCompile]" />
		<mkdir dir="${test.classes.dir}" />
		<javac srcdir="${test.src.dir}" destdir="${test.classes.dir}" source="1.7" target="1.7">
			<classpath refid="test.lib" />
			<classpath location="${classes.dir}" />
			<classpath refid="module.dependencies" />
			<include name="**/*Test*" />
			<include name="**/Mock*" />
		</javac>
	</target>


	<target name="test.unitTests.check">
		<condition property="test.unitTests.run">
			<and>
				<available file="${test.src.dir}"/>
				<isset property="runUnitTests"/>
			</and>
		</condition>
	</target>


  <!-- ================================= 
          target: test
         ================================= -->
	<target name="test" if="test.unitTests.run" depends="test-compile, test.unitTests.check" description="run tests using junit. These tests should not require the code be packaged or deployed.">
		<echo message="[junit:test]" />
		<mkdir dir="${test.reports.dir}" />
		<junit printsummary="withOutAndErr" haltonfailure="no" showoutput="yes" haltonerror="no">
			<classpath>
				<pathelement path="${classes.dir}" />
				<pathelement path="${test.classes.dir}" />
				<path refid="test.lib" />
			</classpath>

			<formatter type="xml" />

			<batchtest fork="yes" todir="${test.reports.dir}">
				<fileset dir="${test.src.dir}">
					<include name="**/*Test*.java" />
					<exclude name="**/Mock*.java" />
					<exclude name="**/AllTests.java" />
					<exclude name="**/Abstract*.java" />
				</fileset>
			</batchtest>
		</junit>

		<junitreport todir="${test.reports.dir}">
			<fileset dir="${test.reports.dir}">
				<include name="TEST-*.xml" />
			</fileset>
			<report format="frames" todir="${test.reports.dir}/html" />
		</junitreport>
	</target>

	<!-- ================================= 
          target: package
         ================================= -->
	<target name="package" depends="test" description="take the compiled code and package it in its distributable format, such as a JAR.">
		<echo message="[assembly]" />
		
		<echo message="Assembling into zip deployable" />
		<mkdir dir="${assembly.build.dir}" />
		<mkdir dir="${assembly.lib.dir}" />
		
		<!-- copy the libs over -->
		<copy todir="${assembly.lib.dir}" flatten="true">
			<fileset dir="../../dependencyJars">
				<include name="**/*.jar" />
				<exclude name="test/**" />
				<exclude name="**/commons-fileupload.jar" />
				<exclude name="**/spring-mock.jar" />
			</fileset>
		</copy>
		
		<!-- copying libs (commons.jar and repriceprocessor) -->
		<copy todir="${assembly.lib.dir}" flatten="true">
			<fileset refid="module.reprice.processor" />
		</copy>

		<!-- copying libs (from ../reprice-processor-common-libs) -->
		<copy todir="${assembly.lib.dir}" flatten="true">
			<fileset refid="reprice.processor.common.libs" />
		</copy>
		
		<!-- copying libs -->
		<copy todir="${assembly.lib.dir}" flatten="true">
			<fileset dir="lib">
				<include name="**/*.jar" />
				<exclude name="test/**" />
			</fileset>
		</copy>
		
		<!-- copying conf files -->
		<copy todir="${assembly.conf.dir}" flatten="true">
			<fileset dir="${classes.dir}">
				<include name="*.properties" />
				<include name="*.xml" />
			</fileset>
		</copy>
		
		<!-- move *.ftl files to root, they will be in classpath in eclipse -->
		<copy todir="${assembly.conf.dir}">
			<fileset dir="${freemarker.resources.dir}" />
		</copy>
		
		<!-- copy batch file that runs this puppy -->
		<copy todir="${assembly.build.dir}" flatten="true">
			<fileset dir="${assembly.src.dir}" />
		</copy>
		
		<echo message="Creating ${ant.project.name}.jar" />
		<jar jarfile="${assembly.lib.dir}/${ant.project.name}.jar">
			<!-- ignore conf files -->
			<fileset dir="${classes.dir}">
				<exclude name="*.properties"/>
				<exclude name="*.xml"/>
			</fileset>
		</jar>
				
		<!-- zip the whole thing up -->
		<zip destfile="${build.dir}/${assembly.final.name}">
    		<zipfileset dir="${assembly.build.dir}"/>    
        </zip>
	</target>

	<!-- ================================= 
          target: install
         ================================= -->
	<target name="install" depends="package" description="install the package into the local repository, for use as a dependency in other projects locally.">
		<!-- not made to install jar into a jar repository -->
	</target>

	<!-- ================================= 
          target: deploy
         ================================= -->
	<target name="deploy" depends="package" description="done in an integration or release environment, copies the final package to the remote repository for sharing with other developers and projects.">

		<move file="${build.dir}/${assembly.final.name}" todir="${deploymentDir}" overwrite="true" />
	</target>

	<!-- ================================= 
          target: clean
         ================================= -->
	<target name="clean" description="emulates the maven2 clean plugin with the clean goal (clean:clean)">
		<delete dir="target" />
	</target>
	
</project>