package biz.firstlook.syndication.feed.module.ebizautos.types;

import com.sun.syndication.feed.module.Module;

public interface EbizModule extends Module {
	  public static final String URI = "http://XML.eBizAutos.com";

	    public String getRequest_type();
	    
	    public void setRequest_type(String requestType);

	    public Integer getClient_id();

		public void setClient_id(Integer client_id);

		public String getCpo();

		public void setCpo(String cpo);

		public String getCylinders();

		public void setCylinders(String cylinders);

		public String getDay_in_stock();

		public void setDay_in_stock(String day_in_stock);

		public String getDoors();

		public void setDoors(String doors);

		public String getDrivetrain();

		public void setDrivetrain(String drivetrain);

		public String getEngine_desc();

		public void setEngine_desc(String engine_desc);

		public String getGears();

		public void setGears(String gears);

		public String getInt_color();

		public void setInt_color(String int_color);

		public String getInt_surface();

		public void setInt_surface(String int_surface);

		public String getOther_price();

		public void setOther_price(String other_price);

		public String getStandard_features();

		public void setStandard_features(String standard_features);

		public String getStock_no();

		public void setStock_no(String stock_no);

		public String getTrans();

		public void setTrans(String trans);

		public String getTrim();

		public void setTrim(String trim);

		public String getVehicle_cost();

		public void setVehicle_cost(String vehicle_cost);
	    
}
