package biz.firstlook.syndication.feed.synd;

public enum FeedType
{

RSS_0_90( "rss_0.90" ),
RSS_0_91( "rss_0.91" ),
RSS_0_92( "rss_0.92" ),
RSS_0_93( "rss_0.93" ),
RSS_0_94( "rss_0.94" ),
RSS_1_0( "rss_1.0" ),
RSS_2_0( "rss_2.0" ),
ATOM_0_3( "atom_0.3" );

private String feedType;

private FeedType( String romeFeedType )
{
	this.feedType = romeFeedType;
}

public String getFeedType()
{
	return feedType;
}

}
