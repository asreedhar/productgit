package biz.firstlook.syndication.feed.module.ebizautos.io;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.jdom.Element;
import org.jdom.Namespace;

import biz.firstlook.syndication.feed.module.ebizautos.types.EbizModule;

import com.sun.syndication.feed.module.Module;
import com.sun.syndication.io.ModuleGenerator;

public class EBizAutosGenerator implements ModuleGenerator
{
	 private static final Namespace SAMPLE_NS  = Namespace.getNamespace("e", EbizModule.URI);

	 
	 private static final Set<Namespace> NAMESPACES;

	    static {
	        Set<Namespace> nss = new HashSet<Namespace>();
	        nss.add(SAMPLE_NS);
	        NAMESPACES = Collections.unmodifiableSet(nss);
	    }
	    
	public void generate( Module module, Element element )
	{
		// TODO Auto-generated method stub
	    // this is not necessary, it is done to avoid the namespace definition in every item.
        Element root = element;
        while (root.getParent()!=null && root.getParent() instanceof Element) {
            root = (Element) element.getParent();
        }
        root.addNamespaceDeclaration(SAMPLE_NS);

        EbizModule fm = (EbizModule)module;
        if (fm.getRequest_type() != null) {
            element.addContent(generateSimpleElement("request_type", fm.getRequest_type()));
        }
        if (fm.getClient_id() != null) {
            element.addContent(generateSimpleElement("client_id", fm.getClient_id().toString()));
        }
        if (fm.getCpo() != null) {
            element.addContent(generateSimpleElement("cpo", fm.getCpo()));
        }
        if (fm.getCylinders() != null) {
            element.addContent(generateSimpleElement("cylinders", fm.getCylinders()));
        }
        if (fm.getDay_in_stock() != null) {
            element.addContent(generateSimpleElement("day_in_stock", fm.getDay_in_stock()));
        }
        if (fm.getDoors() != null) {
            element.addContent(generateSimpleElement("doors", fm.getDoors()));
        }
        if (fm.getDrivetrain() != null) {
            element.addContent(generateSimpleElement("drivetrain", fm.getDrivetrain()));
        }
        if (fm.getEngine_desc() != null) {
            element.addContent(generateSimpleElement("engine_desc", fm.getEngine_desc()));
        }
        if (fm.getGears() != null) {
            element.addContent(generateSimpleElement("gears", fm.getGears()));
        }
        if (fm.getInt_color() != null) {
            element.addContent(generateSimpleElement("int_color", fm.getInt_color()));
        }
        if (fm.getInt_surface() != null) {
            element.addContent(generateSimpleElement("int_surface", fm.getInt_surface()));
        }
        if (fm.getOther_price() != null) {
            element.addContent(generateSimpleElement("other_price", fm.getOther_price()));
        }
        if (fm.getStandard_features() != null) {
            element.addContent(generateSimpleElement("standard_features", fm.getStandard_features()));
        }
        if (fm.getStock_no() != null) {
            element.addContent(generateSimpleElement("stock_no", fm.getStock_no()));
        }
        if (fm.getTrans() != null) {
            element.addContent(generateSimpleElement("trans", fm.getTrans()));
        }
        if (fm.getTrim() != null) {
            element.addContent(generateSimpleElement("trim", fm.getTrim()));
        }
        if (fm.getVehicle_cost() != null) {
            element.addContent(generateSimpleElement("vehicle_cost", fm.getVehicle_cost()));
        }
	}

	public String getNamespaceUri()
	{
		return EbizModule.URI;
	}

	public Set<Namespace> getNamespaces()
	{
		return NAMESPACES;
	}
	
	 protected Element generateSimpleElement(String name, String value)  {
	        Element element = new Element(name, SAMPLE_NS);
	        element.addContent(value);
	        return element;
	    }

}
