package biz.firstlook.syndication.feed.module.ebizautos.types;

import com.sun.syndication.feed.module.ModuleImpl;

public class EbizModuleImpl extends ModuleImpl implements EbizModule {

	private static final long serialVersionUID = -197137993145400790L;
	
	private String request_type;
	private Integer client_id;
	private String stock_no;
	private String trim;
	private String other_price;
	private String vehicle_cost;
	private String cpo;
	private String engine_desc;
	private String cylinders;
	private String trans;
	private String gears;
	private String drivetrain;
	private String int_color;
	private String int_surface;
	private String doors;
	private String day_in_stock;
	private String standard_features;
	
	
	 public EbizModuleImpl() {
	        super(EbizModule.class,EbizModule.URI);
	    }
	
	public void copyFrom(Object arg0) {
		EbizModule sm = (EbizModule) arg0;
        
        setClient_id(sm.getClient_id());
        setCpo(sm.getCpo());
        setCylinders(sm.getCylinders());
        setDay_in_stock(sm.getDay_in_stock());
        setDoors(sm.getDoors());
        setDrivetrain(sm.getDrivetrain());
        setEngine_desc(sm.getEngine_desc());
        setGears(sm.getGears());
        setInt_color(sm.getInt_color());
        setInt_surface(sm.getInt_surface());
        setOther_price(sm.getOther_price());
        setRequest_type(sm.getRequest_type());
        setStandard_features(sm.getStandard_features());
        setStock_no(sm.getStock_no());
        setTrans(sm.getTrans());
        setTrim(sm.getTrim());
        setVehicle_cost(sm.getVehicle_cost());
        
	}

	public Class<EbizModule> getInterface() {
		return EbizModule.class;
	}

	public String getRequest_type() {
		return request_type;
	}

	public void setRequest_type(String requestType) {
		this.request_type = requestType;
		
	}
	public Integer getClient_id() {
		return client_id;
	}

	public void setClient_id(Integer client_id) {
		this.client_id = client_id;
	}

	public String getCpo() {
		return cpo;
	}

	public void setCpo(String cpo) {
		this.cpo = cpo;
	}

	public String getCylinders() {
		return cylinders;
	}

	public void setCylinders(String cylinders) {
		this.cylinders = cylinders;
	}

	public String getDay_in_stock() {
		return day_in_stock;
	}

	public void setDay_in_stock(String day_in_stock) {
		this.day_in_stock = day_in_stock;
	}

	public String getDoors() {
		return doors;
	}

	public void setDoors(String doors) {
		this.doors = doors;
	}

	public String getDrivetrain() {
		return drivetrain;
	}

	public void setDrivetrain(String drivetrain) {
		this.drivetrain = drivetrain;
	}

	public String getEngine_desc() {
		return engine_desc;
	}

	public void setEngine_desc(String engine_desc) {
		this.engine_desc = engine_desc;
	}

	public String getGears() {
		return gears;
	}

	public void setGears(String gears) {
		this.gears = gears;
	}

	public String getInt_color() {
		return int_color;
	}

	public void setInt_color(String int_color) {
		this.int_color = int_color;
	}

	public String getInt_surface() {
		return int_surface;
	}

	public void setInt_surface(String int_surface) {
		this.int_surface = int_surface;
	}

	public String getOther_price() {
		return other_price;
	}

	public void setOther_price(String other_price) {
		this.other_price = other_price;
	}

	public String getStandard_features() {
		return standard_features;
	}

	public void setStandard_features(String standard_features) {
		this.standard_features = standard_features;
	}

	public String getStock_no() {
		return stock_no;
	}

	public void setStock_no(String stock_no) {
		this.stock_no = stock_no;
	}

	public String getTrans() {
		return trans;
	}

	public void setTrans(String trans) {
		this.trans = trans;
	}

	public String getTrim() {
		return trim;
	}

	public void setTrim(String trim) {
		this.trim = trim;
	}

	public String getVehicle_cost() {
		return vehicle_cost;
	}

	public void setVehicle_cost(String vehicle_cost) {
		this.vehicle_cost = vehicle_cost;
	}

}
