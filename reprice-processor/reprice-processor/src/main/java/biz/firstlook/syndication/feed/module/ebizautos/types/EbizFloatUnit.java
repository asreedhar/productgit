package biz.firstlook.syndication.feed.module.ebizautos.types;

import com.sun.syndication.feed.module.base.types.FloatUnit;

/**
 * Overrides com.sun.syndication.feed.module.base.types.FloatUnit output for eBizAutos since they do not follow goolge base spec exactly.
 * @author bfung
 *
 */
public class EbizFloatUnit extends FloatUnit
{

public EbizFloatUnit( float arg0, String arg1 )
{
	super( arg0, arg1 );
}

public String toString()
{
	return Integer.valueOf((int)getValue()).toString();
}

}
