
package biz.firstlook.export.reprice.autouplink.Stubbed.https.services_autouplinkusa_com.vehicleinventory;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import coldfusion.xml.rpc.CFCInvocationException;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the https.services_autouplinkusa_com.vehicleinventory package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Fault_QNAME = new QName("https://services.autouplinkusa.com/VehicleInventory", "fault");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: https.services_autouplinkusa_com.vehicleinventory
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link StickerPriceUpdateResponse }
     * 
     */
    public StickerPriceUpdateResponse createStickerPriceUpdateResponse() {
        return new StickerPriceUpdateResponse();
    }

    /**
     * Create an instance of {@link CommentUpdateResponse }
     * 
     */
    public CommentUpdateResponse createCommentUpdateResponse() {
        return new CommentUpdateResponse();
    }

    /**
     * Create an instance of {@link PricesUpdate }
     * 
     */
    public PricesUpdate createPricesUpdate() {
        return new PricesUpdate();
    }

    /**
     * Create an instance of {@link StickerPriceUpdate }
     * 
     */
    public StickerPriceUpdate createStickerPriceUpdate() {
        return new StickerPriceUpdate();
    }

    /**
     * Create an instance of {@link InternetPriceUpdateResponse }
     * 
     */
    public InternetPriceUpdateResponse createInternetPriceUpdateResponse() {
        return new InternetPriceUpdateResponse();
    }

    /**
     * Create an instance of {@link InternetPriceUpdate }
     * 
     */
    public InternetPriceUpdate createInternetPriceUpdate() {
        return new InternetPriceUpdate();
    }

    /**
     * Create an instance of {@link PriceUpdateResponse }
     * 
     */
    public PriceUpdateResponse createPriceUpdateResponse() {
        return new PriceUpdateResponse();
    }

    /**
     * Create an instance of {@link PriceUpdate }
     * 
     */
    public PriceUpdate createPriceUpdate() {
        return new PriceUpdate();
    }

    /**
     * Create an instance of {@link PricesUpdateResponse }
     * 
     */
    public PricesUpdateResponse createPricesUpdateResponse() {
        return new PricesUpdateResponse();
    }

    /**
     * Create an instance of {@link CommentUpdate }
     * 
     */
    public CommentUpdate createCommentUpdate() {
        return new CommentUpdate();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CFCInvocationException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://services.autouplinkusa.com/VehicleInventory", name = "fault")
    public JAXBElement<CFCInvocationException> createFault(CFCInvocationException value) {
        return new JAXBElement<CFCInvocationException>(_Fault_QNAME, CFCInvocationException.class, null, value);
    }

}
