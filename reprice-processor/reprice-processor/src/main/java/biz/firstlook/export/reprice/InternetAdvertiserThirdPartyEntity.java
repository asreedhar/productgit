package biz.firstlook.export.reprice;


public class InternetAdvertiserThirdPartyEntity extends ThirdPartyEntity {
	
	private static final long serialVersionUID = -5063266519782653177L;

	public InternetAdvertiserThirdPartyEntity(Integer thirdPartyEntityID, Integer businessUnitID, String name) {
		super(thirdPartyEntityID, businessUnitID, name);
	}
}
