
package biz.firstlook.export.reprice.autouplink.Stubbed.https.services_autouplinkusa_com.vehicleinventory;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import biz.firstlook.export.reprice.autouplink.Stubbed.webservices.vehicleinventory.WSAuth;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VehicleInventoryAuth" type="{http://VehicleInventory.webservices}WSAuth"/>
 *         &lt;element name="VIN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Comment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vehicleInventoryAuth",
    "vin",
    "comment"
})
@XmlRootElement(name = "CommentUpdate")
public class CommentUpdate {

    @XmlElement(name = "VehicleInventoryAuth", required = true)
    protected WSAuth vehicleInventoryAuth;
    @XmlElement(name = "VIN", required = true)
    protected String vin;
    @XmlElement(name = "Comment", required = true)
    protected String comment;

    /**
     * Gets the value of the vehicleInventoryAuth property.
     * 
     * @return
     *     possible object is
     *     {@link WSAuth }
     *     
     */
    public WSAuth getVehicleInventoryAuth() {
        return vehicleInventoryAuth;
    }

    /**
     * Sets the value of the vehicleInventoryAuth property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSAuth }
     *     
     */
    public void setVehicleInventoryAuth(WSAuth value) {
        this.vehicleInventoryAuth = value;
    }

    /**
     * Gets the value of the vin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVIN() {
        return vin;
    }

    /**
     * Sets the value of the vin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVIN(String value) {
        this.vin = value;
    }

    /**
     * Gets the value of the comment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the value of the comment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComment(String value) {
        this.comment = value;
    }

}
