
package biz.firstlook.export.reprice.autouplink.Stubbed;

import java.net.MalformedURLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;

import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.annotations.AnnotationServiceFactory;
import org.codehaus.xfire.annotations.jsr181.Jsr181WebAnnotations;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.jaxb2.JaxbTypeRegistry;
import org.codehaus.xfire.service.Endpoint;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.transport.TransportManager;

public class vehicleinventory1ServiceClient {

    private static XFireProxyFactory proxyFactory = new XFireProxyFactory();
    private Map<QName, Endpoint> endpoints = new HashMap<QName, Endpoint>();
    private Service service0;
    private String siteUrl;

    
    //This constructor has been changed from the original created by the client generator    
    //to enable the configuration to pass the site as a parameter
    //this assumes the name of the service itself stays the same.
    public vehicleinventory1ServiceClient(String siteUrl) 
    {
        this.siteUrl = siteUrl;
    	create0();
        Endpoint vehicleinventory1_cfcEP = service0 .addEndpoint(new QName(this.siteUrl+"/VehicleInventory", "vehicleinventory1.cfc"), new QName(this.siteUrl+"/VehicleInventory", "vehicleinventory1.cfcSoapBinding"), this.siteUrl+"/webservices/VehicleInventory/vehicleinventory1.cfc");
        endpoints.put(new QName(this.siteUrl+"/VehicleInventory", "vehicleinventory1.cfc"), vehicleinventory1_cfcEP);
        Endpoint vehicleinventory1LocalEndpointEP = service0 .addEndpoint(new QName(this.siteUrl+"/VehicleInventory", "vehicleinventory1LocalEndpoint"), new QName(this.siteUrl+"/VehicleInventory", "vehicleinventory1LocalBinding"), "xfire.local://vehicleinventory1Service");
        endpoints.put(new QName(this.siteUrl+"/VehicleInventory", "vehicleinventory1LocalEndpoint"), vehicleinventory1LocalEndpointEP);
    }

    public Object getEndpoint(Endpoint endpoint) {
        try {
            return proxyFactory.create((endpoint).getBinding(), (endpoint).getUrl());
        } catch (MalformedURLException e) {
            throw new XFireRuntimeException("Invalid URL", e);
        }
    }

    public Object getEndpoint(QName name) {
        Endpoint endpoint = ((Endpoint) endpoints.get((name)));
        if ((endpoint) == null) {
            throw new IllegalStateException("No such endpoint!");
        }
        return getEndpoint((endpoint));
    }

    public Collection<Endpoint> getEndpoints() {
        return endpoints.values();
    }

    @SuppressWarnings("unchecked")
	private void create0() {
        TransportManager tm = (org.codehaus.xfire.XFireFactory.newInstance().getXFire().getTransportManager());
        HashMap props = new HashMap();
        props.put("annotations.allow.interface", true);
        AnnotationServiceFactory asf = new AnnotationServiceFactory(new Jsr181WebAnnotations(), tm, new AegisBindingProvider(new JaxbTypeRegistry()));
        asf.setBindingCreationEnabled(false);
        service0 = asf.create((vehicleinventory1.class), props);
        asf.createSoap11Binding(service0, new QName(this.siteUrl+"/VehicleInventory", "vehicleinventory1.cfcSoapBinding"), "http://schemas.xmlsoap.org/soap/http");
        asf.createSoap11Binding(service0, new QName(this.siteUrl+"/VehicleInventory", "vehicleinventory1LocalBinding"), "urn:xfire:transport:local");
    }

    public vehicleinventory1 getvehicleinventory1_cfc() {
        return ((vehicleinventory1)(this).getEndpoint(new QName(this.siteUrl+"/VehicleInventory", "vehicleinventory1.cfc")));
    }

    public vehicleinventory1 getvehicleinventory1_cfc(String url) {
        vehicleinventory1 var = getvehicleinventory1_cfc();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

    public vehicleinventory1 getvehicleinventory1LocalEndpoint() {
        return ((vehicleinventory1)(this).getEndpoint(new QName(this.siteUrl+"/VehicleInventory", "vehicleinventory1LocalEndpoint")));
    }

    public vehicleinventory1 getvehicleinventory1LocalEndpoint(String url) {
        vehicleinventory1 var = getvehicleinventory1LocalEndpoint();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

}
