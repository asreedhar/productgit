 package biz.firstlook.export.reprice.eBizAutos;

import biz.firstlook.export.reprice.Credentials;
import biz.firstlook.export.reprice.ThirdPartyEntity;

interface EBizAutosCredentialsDao
{

public final static String GLOBAL_PARAM_EBIZ_ID = "eBizID";
public final static String GLOBAL_PARAM_EBIZ_AUTH = "eBizAuth";

Credentials getGlobalCredentials();

Credentials getCredentials( Integer businessUnitId, ThirdPartyEntity thirdPartyEntity);

}
