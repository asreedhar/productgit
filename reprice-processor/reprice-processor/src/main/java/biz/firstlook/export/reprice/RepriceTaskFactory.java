package biz.firstlook.export.reprice;

/**
 * Configuration and factory
 * @author bfung
 *
 */
public interface RepriceTaskFactory
{

public RepriceTask createTask( RepriceExport event );

/**
 * This property is used as the key in RepriceTaskFactoryLocator to locate the appropriate factory.
 * This factory will process all Reprice Events that have a ThirdPartyID equal to this value
 * @return the ID of the ThirdPartyEntity in the DB.
 */
public Integer getThirdPartyId();

/**
 * The number of tasks allowed to run concurrently.
 * @return an int between 1 and Integer.MAX_VALUE inclusive.
 */
public int allowNumConcurrentTasks();
}
