package biz.firstlook.export.reprice.eBizAutos;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.export.reprice.Credentials;
import biz.firstlook.export.reprice.ThirdPartyEntity;

class EBizAutosCredentialsJdbcDao extends JdbcDaoSupport implements EBizAutosCredentialsDao
{

private static final String GLOBAL_PARAMS_QUERY;
private static final String CREDENTIAL_QUERY;

static
{
	StringBuilder gpQuery = new StringBuilder();
	gpQuery.append( " SELECT gp1.value as ID, gp2.value as Auth" );
	gpQuery.append( " FROM GlobalParam gp1, GlobalParam gp2" );
	gpQuery.append( " WHERE gp1.name = '" ).append( EBizAutosCredentialsDao.GLOBAL_PARAM_EBIZ_ID ).append( "'" );
	gpQuery.append( " AND gp2.name = '" ).append( EBizAutosCredentialsDao.GLOBAL_PARAM_EBIZ_AUTH ).append( "'" );
	GLOBAL_PARAMS_QUERY = gpQuery.toString();
	gpQuery = null;

	gpQuery = new StringBuilder();
	gpQuery.append( " SELECT ExportClientID, ExportGuid" );
	gpQuery.append( " FROM InternetAdvertiserDealership" );
	gpQuery.append( " WHERE InternetAdvertiserID = ?" );
	gpQuery.append( " AND BusinessUnitID = ?" );
	CREDENTIAL_QUERY = gpQuery.toString();
	gpQuery = null;
}

public Credentials getCredentials( Integer businessUnitId, ThirdPartyEntity thirdPartyEntity )
{
	return (Credentials)getJdbcTemplate().queryForObject( 
			CREDENTIAL_QUERY, 
			new Object[] { thirdPartyEntity.getThirdPartyEntityID(), businessUnitId },
			new RowMapper() {
				public Object mapRow( ResultSet rs, int index ) throws SQLException {
					Credentials creds = new Credentials(
							Integer.valueOf( rs.getInt( "ExportClientID" ) ).toString(),
							rs.getString( "ExportGuid" ));
					return creds;
				}
			});
}

public Credentials getGlobalCredentials()
{
	return (Credentials)getJdbcTemplate().queryForObject( 
			GLOBAL_PARAMS_QUERY, 
			new RowMapper()	{
				public Object mapRow( ResultSet rs, int index ) throws SQLException {
					Credentials creds = new Credentials( 
							rs.getString( "ID" ), 
							rs.getString( "Auth" ));
					return creds;
				}
			});
}

}
