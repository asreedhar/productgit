package biz.firstlook.export.reprice;

enum ThirdPartyEntityTypeEnum {
	
	// THESE VALUES MATCH THE INDEXES IN the ThirdPartyEntitytType table
	INTERNET_ADVERTISER(4, "Internet Advertiser"),
	DMS(7, "DMS Export Target");
	
	private Integer id;
	private String name;
	
	private ThirdPartyEntityTypeEnum(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
}
