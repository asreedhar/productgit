package biz.firstlook.export.reprice;


class DMSExportThirdPartyEntity extends ThirdPartyEntity {

	private static final long serialVersionUID = 218729766386724562L;

	public DMSExportThirdPartyEntity(Integer thirdPartyEntityID, Integer businessUnitID, String name) {
		super(thirdPartyEntityID, businessUnitID, name);
	}	
}
