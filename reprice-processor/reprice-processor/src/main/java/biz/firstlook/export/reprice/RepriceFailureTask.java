package biz.firstlook.export.reprice;

import java.util.Date;


class RepriceFailureTask extends RepriceTask
{

public static final String FAIL_REASON_PREFIX = "No InternetAdvertiser Adapter Implemented for ";

RepriceFailureTask( RepriceExport request )
{
	super( request );
}

public RepriceExportResult call() throws Exception
{
	RepriceExportResult result = new RepriceExportResult( getRepriceEvent(), new Date() );
	result.fail( FAIL_REASON_PREFIX + getRepriceEvent().getThirdPartyEntity().getName());
	return result;
}

}
