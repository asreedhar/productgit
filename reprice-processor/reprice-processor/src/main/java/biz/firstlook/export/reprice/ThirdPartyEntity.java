package biz.firstlook.export.reprice;

import java.io.Serializable;


public abstract class ThirdPartyEntity implements Serializable {

	private static final long serialVersionUID = 301736159782954556L;
	
	private final Integer thirdPartyEntityID;
	private final Integer businessUnitID;
	private final String  name;

	public ThirdPartyEntity(Integer thirdPartyEntityID, Integer businessUnitID, String name) {
		this.thirdPartyEntityID = thirdPartyEntityID;
		this.businessUnitID = businessUnitID;
		this.name = name;
	}
	
	public Integer getBusinessUnitID() {
		return businessUnitID;
	}

	public String getName() {
		return name;
	}

	public Integer getThirdPartyEntityID() {
		return thirdPartyEntityID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((businessUnitID == null) ? 0 : businessUnitID.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime
				* result
				+ ((thirdPartyEntityID == null) ? 0 : thirdPartyEntityID
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ThirdPartyEntity other = (ThirdPartyEntity) obj;
		if (businessUnitID == null) {
			if (other.businessUnitID != null)
				return false;
		} else if (!businessUnitID.equals(other.businessUnitID))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (thirdPartyEntityID == null) {
			if (other.thirdPartyEntityID != null)
				return false;
		} else if (!thirdPartyEntityID.equals(other.thirdPartyEntityID))
			return false;
		return true;
	}
}
