package biz.firstlook.export.reprice;

interface RepriceExportLockRetriever {
	
	/**
	 * 
	 * @param repriceExportId
	 * @return true if lock was obtained, otherwise false.
	 */
	boolean obtainLock(final RepriceExport repriceExport);
}
