package biz.firstlook.export.reprice.eBizAutos;

import biz.firstlook.export.reprice.AbstractRepriceTaskFactory;
import biz.firstlook.export.reprice.Credentials;
import biz.firstlook.export.reprice.RepriceExport;
import biz.firstlook.export.reprice.RepriceTask;
import biz.firstlook.export.reprice.RepriceTaskFactory;

class EBizRepriceTaskFactory extends AbstractRepriceTaskFactory implements RepriceTaskFactory
{

/**
 * This value is the ThirdPartyEntityID in the ThirdPartyEntity table
 * This factory will process all Reprice Events that have a ThirdPartyID equal to this value
 */
private Integer thirdPartyId;
private EBizAutosCredentialsDao ebizAutosCredentialsDao;
private String endpointUrl;

public RepriceTask createTask( RepriceExport event )
{
	Credentials firstlookCreds = ebizAutosCredentialsDao.getGlobalCredentials();
	Credentials dealerCreds = ebizAutosCredentialsDao.getCredentials( event.getBusinessUnitId(), event.getThirdPartyEntity() );
	EBizRepriceTask task = new EBizRepriceTask( event, firstlookCreds, dealerCreds, endpointUrl); 
	return task;
}

public void setEbizAutosCredentialsDao( EBizAutosCredentialsDao eBizAutosCredentialsDao )
{
	this.ebizAutosCredentialsDao = eBizAutosCredentialsDao;
}

public Integer getThirdPartyId() {
	return thirdPartyId;
}

public void setThirdPartyId(Integer thirdPartyId) {
	this.thirdPartyId = thirdPartyId;
}

/**
 * Configuration set by spring config file (applicationContext-plugins)
 * @param endpointUrl
 */
public void setEndpointUrl(String endpointUrl) {
	this.endpointUrl = endpointUrl;
}

}
