package biz.firstlook.export.reprice;

import java.util.concurrent.Callable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class RepriceTask implements Callable< RepriceExportResult >
{

protected static final Log log = LogFactory.getLog( RepriceTask.class );

private final RepriceExport repriceEvent;

public RepriceTask( RepriceExport request ) {
	this.repriceEvent = request;
}

public RepriceExport getRepriceEvent() {
    return repriceEvent;
}

}
