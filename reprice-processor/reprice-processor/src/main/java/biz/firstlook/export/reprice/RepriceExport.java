package biz.firstlook.export.reprice;

import java.io.Serializable;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;


/**
 * The message and data sent to indicate a reprice should happen.  Immutable Object.
 * @author bfung
 *
 */
public class RepriceExport implements Serializable {

private static final long serialVersionUID = -1855850227015696115L;

private final Integer id;
private final ThirdPartyEntity thirdPartyEntity;
private final Integer busniessUnitId;
private final InventoryItem inventoryItem;
private final Integer listPrice;
private final RepriceExportStatus status;
private final Integer failureCount;


public RepriceExport( Integer id, ThirdPartyEntity thirdPartyEntity, Integer businessUnitId, InventoryItem inventoryItem, Integer listPrice, RepriceExportStatus status, Integer failureCount ) {
	this.id = id;
	this.thirdPartyEntity = thirdPartyEntity;
	this.busniessUnitId = businessUnitId;
	this.inventoryItem = inventoryItem;
	this.listPrice = listPrice;
	this.status = status;
	this.failureCount = failureCount;
}

public Integer getListPrice()
{
	return listPrice;
}

public InventoryItem getInventoryItem()
{
	return inventoryItem;
}

public Integer getId()
{
	return id;
}

public Integer getBusinessUnitId()
{
	return busniessUnitId;
}

public RepriceExportStatus getRepriceEventStatus()
{
	return status;
}

public Integer getFailureCount()
{
	return failureCount;
}

public ThirdPartyEntity getThirdPartyEntity() {
	return thirdPartyEntity;
}

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result
			+ ((busniessUnitId == null) ? 0 : busniessUnitId.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result
			+ ((inventoryItem == null) ? 0 : inventoryItem.hashCode());
	result = prime * result + ((listPrice == null) ? 0 : listPrice.hashCode());
	result = prime * result
			+ ((thirdPartyEntity == null) ? 0 : thirdPartyEntity.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	RepriceExport other = (RepriceExport) obj;
	if (busniessUnitId == null) {
		if (other.busniessUnitId != null)
			return false;
	} else if (!busniessUnitId.equals(other.busniessUnitId))
		return false;
	if (id == null) {
		if (other.id != null)
			return false;
	} else if (!id.equals(other.id))
		return false;
	if (inventoryItem == null) {
		if (other.inventoryItem != null)
			return false;
	} else if (!inventoryItem.equals(other.inventoryItem))
		return false;
	if (listPrice == null) {
		if (other.listPrice != null)
			return false;
	} else if (!listPrice.equals(other.listPrice))
		return false;
	if (thirdPartyEntity == null) {
		if (other.thirdPartyEntity != null)
			return false;
	} else if (!thirdPartyEntity.equals(other.thirdPartyEntity))
		return false;
	return true;
}

public String toString()
{
	return ReflectionToStringBuilder.reflectionToString( this, ToStringStyle.SIMPLE_STYLE );
}



}
