
package biz.firstlook.export.reprice.autouplink.Stubbed.https.services_autouplinkusa_com.vehicleinventory;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PriceUpdateReturn" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "priceUpdateReturn"
})
@XmlRootElement(name = "PriceUpdateResponse")
public class PriceUpdateResponse {

    @XmlElement(name = "PriceUpdateReturn", required = true)
    protected String priceUpdateReturn;

    /**
     * Gets the value of the priceUpdateReturn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriceUpdateReturn() {
        return priceUpdateReturn;
    }

    /**
     * Sets the value of the priceUpdateReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriceUpdateReturn(String value) {
        this.priceUpdateReturn = value;
    }

}
