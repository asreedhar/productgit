package biz.firstlook.export.reprice.sis;

import biz.firstlook.client.dms.sis.SISInventoryUpdateClient;
import biz.firstlook.export.reprice.AbstractRepriceTaskFactory;
import biz.firstlook.export.reprice.RepriceExport;
import biz.firstlook.export.reprice.RepriceTask;
import biz.firstlook.export.reprice.RepriceTaskFactory;

public class SISRepriceTaskFactory extends 
		AbstractRepriceTaskFactory implements RepriceTaskFactory {

	/**
	 * This value is the ThirdPartyEntityID in the ThirdPartyEntity table
	 * This factory will process all Reprice Events that have a ThirdPartyID equal to this value
	 */
	private Integer thirdPartyId = 6095; // default
	private SISInventoryUpdateClient sisInventoryUpdateClient;
	private SISVinAndStockNumberMapper sisVinAndStockNumberMapper;
	
	public RepriceTask createTask(RepriceExport event) {
		return new SISRepriceTask(event, sisInventoryUpdateClient, sisVinAndStockNumberMapper);
	}

	public Integer getThirdPartyId() {
		return thirdPartyId;
	}

	public void setThirdPartyId(Integer thirdPartyId) {
		this.thirdPartyId = thirdPartyId;
	}

	public void setSisInventoryUpdateClient(
			SISInventoryUpdateClient sisInventoryUpdateClient) {
		this.sisInventoryUpdateClient = sisInventoryUpdateClient;
	}

	public void setSisVinAndStockNumberMapper(
			SISVinAndStockNumberMapper sisVinAndStockNumberMapper) {
		this.sisVinAndStockNumberMapper = sisVinAndStockNumberMapper;
	}
	
}
