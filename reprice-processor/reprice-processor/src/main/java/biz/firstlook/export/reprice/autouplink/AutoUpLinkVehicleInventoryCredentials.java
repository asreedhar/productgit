/**
 * 
 */
package biz.firstlook.export.reprice.autouplink;

import java.sql.ResultSet;
import java.sql.SQLException;

import biz.firstlook.export.reprice.Credentials;
import biz.firstlook.export.reprice.RepriceExport;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.core.RowMapper;

/**
 * @author nsnow
 *
 */
public class AutoUpLinkVehicleInventoryCredentials extends JdbcDaoSupport
{
	public final static String GLOBAL_PARAM_AutoUpLink_ID = "AutoUpLinkID";
	public final static String GLOBAL_PARAM_AutoUpLink_AUTH = "AutoUpLinkAuth";
	private static final String AUTOUPLINK_CREDENTIAL_PARAMS_QUERY;
	private static final String DEALID_QUERY;
	private static final String INTERNET_PRICE_UPDATE_REQUIRED_QUERY;
	
	static
	{
		//get the user name and password for AutoUpLinkWebservice
		StringBuilder gpQuery = new StringBuilder();
		gpQuery.append( " SELECT gp1.value as ID, gp2.value as Auth" );
		gpQuery.append( " FROM GlobalParam gp1, GlobalParam gp2" );
		gpQuery.append( " WHERE gp1.name = '" ).append( GLOBAL_PARAM_AutoUpLink_ID ).append( "'" );
		gpQuery.append( " AND gp2.name = '" ).append( GLOBAL_PARAM_AutoUpLink_AUTH ).append( "'" );
		AUTOUPLINK_CREDENTIAL_PARAMS_QUERY = gpQuery.toString();
		gpQuery = null;
				
		//get the dealer id
		gpQuery = new StringBuilder();
		gpQuery.append( " SELECT InternetAdvertisersDealershipCode" );
		gpQuery.append( " FROM InternetAdvertiserDealership" );
		gpQuery.append( " WHERE InternetAdvertiserID = ?" );
		gpQuery.append( " AND BusinessUnitID = ?" );
		DEALID_QUERY = gpQuery.toString();
		gpQuery = null;		
		
		// determine if the dealer needs to update InternetPrice instead of Price.
		gpQuery = new StringBuilder();									
		gpQuery.append("SELECT CASE WHEN EXISTS ( ");
		gpQuery.append("SELECT 1 ");
		gpQuery.append("FROM Extract.DealerConfiguration dc ");
		gpQuery.append("JOIN dbo.ThirdPartyEntity tpe on tpe.Name = dc.DestinationName ");
		gpQuery.append("WHERE dc.Active = 1 ");
		gpQuery.append("and tpe.ThirdPartyEntityID = ? ");
		gpQuery.append("and dc.BusinessUnitID = ? ");
		gpQuery.append(") THEN 1 ELSE 0 END AS InternetPriceUpdateRequired");			
		INTERNET_PRICE_UPDATE_REQUIRED_QUERY = gpQuery.toString();
		gpQuery = null;
	}	
	

	public Credentials getCredentials()
	{
		try
		{
			return (Credentials)getJdbcTemplate().queryForObject
			( 
					AUTOUPLINK_CREDENTIAL_PARAMS_QUERY, 
					new RowMapper()	
					{
						public Object mapRow( ResultSet rs, int index ) throws SQLException 
						{
							Credentials creds = new Credentials( 
									rs.getString( "ID" ), 
									rs.getString( "Auth" ));
							return creds;
						}
					}
			);		
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
			return null;
		}		
	}
	
	public String getDealerID(RepriceExport event)
	{
		Integer InternetAdvertiserID = event.getThirdPartyEntity().getThirdPartyEntityID();
		Integer businessUnitId = event.getThirdPartyEntity().getBusinessUnitID();		 
		return (String)getJdbcTemplate().queryForObject
		( 
				DEALID_QUERY, 
				new Object[] { InternetAdvertiserID, businessUnitId },
				new RowMapper() 
					{
						public Object mapRow( ResultSet rs, int index ) throws SQLException 
						{
							String dealerID = rs.getString( "InternetAdvertisersDealershipCode");							
							return dealerID;
						}
					}
		);
	}
	
	public boolean isInternetPriceUpdateRequired(RepriceExport event)
	{
		Integer InternetAdvertiserID = event.getThirdPartyEntity().getThirdPartyEntityID();
		Integer businessUnitId = event.getThirdPartyEntity().getBusinessUnitID();		 
		String result = (String)getJdbcTemplate().queryForObject
		( 
				INTERNET_PRICE_UPDATE_REQUIRED_QUERY, 
				new Object[] { InternetAdvertiserID, businessUnitId },
				new RowMapper() 
					{
						public Object mapRow( ResultSet rs, int index ) throws SQLException 
						{
							String internetPriceRequired = rs.getString( "InternetPriceUpdateRequired");							
							return internetPriceRequired;
						}
					}
		);
		return result.equals("1");
	}

}
