package biz.firstlook.export.reprice.rd;

import biz.firstlook.client.dms.rd.ReynoldsDirectInventoryUpdateClient;
import biz.firstlook.export.reprice.AbstractRepriceTaskFactory;
import biz.firstlook.export.reprice.RepriceExport;
import biz.firstlook.export.reprice.RepriceTask;
import biz.firstlook.export.reprice.RepriceTaskFactory;
import biz.firstlook.export.reprice.sis.SISVinAndStockNumberMapper;

public class ReynoldsDirectRepriceTaskFactory extends
		AbstractRepriceTaskFactory implements RepriceTaskFactory {
	private Integer thirdPartyId = 6232; // default
	private ReynoldsDirectInventoryUpdateClient reynoldsDirectInventoryUpdateClient;
	private SISVinAndStockNumberMapper sisVinAndStockNumberMapper;

	public RepriceTask createTask(RepriceExport event) {
		return new ReynoldsDirectRepriceTask(event,
				reynoldsDirectInventoryUpdateClient, sisVinAndStockNumberMapper);
	}

	public Integer getThirdPartyId() {
		return thirdPartyId;
	}

	public void setReynoldsDirectInventoryUpdateClient(
			ReynoldsDirectInventoryUpdateClient reynoldsDirectInventoryUpdateClient) {
		this.reynoldsDirectInventoryUpdateClient = reynoldsDirectInventoryUpdateClient;
	}

	public void setThirdPartyId(Integer thirdPartyId) {
		this.thirdPartyId = thirdPartyId;
	}

	public void setSisVinAndStockNumberMapper(
			SISVinAndStockNumberMapper sisVinAndStockNumberMapper) {
		this.sisVinAndStockNumberMapper = sisVinAndStockNumberMapper;
	}
}
