
package biz.firstlook.export.reprice.autouplink.Stubbed.https.services_autouplinkusa_com.vehicleinventory;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import biz.firstlook.export.reprice.autouplink.Stubbed.webservices.vehicleinventory.PricesUpdateResponses;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PricesUpdateReturn" type="{http://VehicleInventory.webservices}PricesUpdateResponses"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pricesUpdateReturn"
})
@XmlRootElement(name = "PricesUpdateResponse")
public class PricesUpdateResponse {

    @XmlElement(name = "PricesUpdateReturn", required = true)
    protected PricesUpdateResponses pricesUpdateReturn;

    /**
     * Gets the value of the pricesUpdateReturn property.
     * 
     * @return
     *     possible object is
     *     {@link PricesUpdateResponses }
     *     
     */
    public PricesUpdateResponses getPricesUpdateReturn() {
        return pricesUpdateReturn;
    }

    /**
     * Sets the value of the pricesUpdateReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link PricesUpdateResponses }
     *     
     */
    public void setPricesUpdateReturn(PricesUpdateResponses value) {
        this.pricesUpdateReturn = value;
    }

}
