package biz.firstlook.export.reprice;

import java.io.Serializable;

public class Credentials implements Serializable
{

private static final long serialVersionUID = 1453024491368972932L;

private String username;
private String password;

public Credentials( String username, String password )
{
	super();
	this.username = username;
	this.password = password;
}

public String getPassword()
{
	return password;
}

public String getUsername()
{
	return username;
}

public String toString()
{
	return new StringBuilder("username: ").append( username ).append( ", password: ").append( password ).toString();
}

}
