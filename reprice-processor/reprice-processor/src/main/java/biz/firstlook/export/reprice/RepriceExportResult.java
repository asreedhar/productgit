package biz.firstlook.export.reprice;

import java.util.Date;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class RepriceExportResult
{

private final RepriceExport theExport;
	
private Integer id;
private RepriceExportStatus status;
private Date date;
private String failureReason;
private Integer failureCount;

private static String DEFAULT_FAIL = "Calling code did not set success or failure, defaulting to fail.";
private static String NO_REASON_FAIL = "A failure reason was not provided, or there was a null pointer.";

/**
 * After constructing the object, one of fail(String) or success() should be called.
 * The default behavior is to fail.
 * @param event
 */
public RepriceExportResult( RepriceExport event, Date dateSent ) {
	this.id = event.getId();
	this.date = dateSent == null ? new Date() : dateSent;
	this.failureCount = event.getFailureCount();
	this.status = RepriceExportStatus.Failure;
	this.failureReason = DEFAULT_FAIL;
	this.theExport = event;
}

/**
 * Sets the state in the object for a failed run.
 * @param reason the failure reason
 */
public synchronized void fail( String reason )
{
	this.status = RepriceExportStatus.Failure;
	if( reason == null )
		reason = NO_REASON_FAIL;
	
	this.failureReason = reason;
	failureCount = Integer.valueOf( failureCount.intValue() + 1 );
}

/**
 * Sets the correct fields for a success
 */
public synchronized void success()
{
	this.status = RepriceExportStatus.Successful;
	this.failureReason = null;
}

public synchronized void hardFailure(String reason) {
	fail(reason);
	this.status = RepriceExportStatus.ConfirmedFailure;
}

public Integer getId()
{
	return id;
}

public Date getSentDate()
{
	return date;
}

public String getFailureReason()
{
	return failureReason;
}

public Integer getFailureCount()
{
	if( failureCount == null )
		failureCount = Integer.valueOf( 0 );
	
	return failureCount;
}

public RepriceExportStatus getRepriceEventStatus()
{
	return status;
}

public RepriceExport getRepriceExport() {
	return theExport;
}

public String toString()
{
	return ReflectionToStringBuilder.reflectionToString( this, ToStringStyle.SIMPLE_STYLE );
}

}
