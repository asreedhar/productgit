
package biz.firstlook.export.reprice.autouplink.Stubbed;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import biz.firstlook.export.reprice.autouplink.Stubbed.webservices.vehicleinventory.PricesUpdateResponses;
import biz.firstlook.export.reprice.autouplink.Stubbed.webservices.vehicleinventory.WSAuth;

@WebService(name = "vehicleinventory1", targetNamespace = "https://services.autouplinkusa.com/VehicleInventory")
@SOAPBinding(use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public interface vehicleinventory1 {


    @WebMethod(operationName = "InternetPriceUpdate", action = "")
    @WebResult(name = "InternetPriceUpdateReturn", targetNamespace = "https://services.autouplinkusa.com/VehicleInventory")
    public String internetPriceUpdate(
        @WebParam(name = "VehicleInventoryAuth", targetNamespace = "https://services.autouplinkusa.com/VehicleInventory")
        WSAuth VehicleInventoryAuth,
        @WebParam(name = "VIN", targetNamespace = "https://services.autouplinkusa.com/VehicleInventory")
        String VIN,
        @WebParam(name = "InternetPrice", targetNamespace = "https://services.autouplinkusa.com/VehicleInventory")
        double InternetPrice)
        throws CFCInvocationException
    ;

    @WebMethod(operationName = "PricesUpdate", action = "")
    @WebResult(name = "PricesUpdateReturn", targetNamespace = "https://services.autouplinkusa.com/VehicleInventory")
    public PricesUpdateResponses pricesUpdate(
        @WebParam(name = "VehicleInventoryAuth", targetNamespace = "https://services.autouplinkusa.com/VehicleInventory")
        WSAuth VehicleInventoryAuth,
        @WebParam(name = "VIN", targetNamespace = "https://services.autouplinkusa.com/VehicleInventory")
        String VIN,
        @WebParam(name = "Price", targetNamespace = "https://services.autouplinkusa.com/VehicleInventory")
        String Price,
        @WebParam(name = "InternetPrice", targetNamespace = "https://services.autouplinkusa.com/VehicleInventory")
        String InternetPrice,
        @WebParam(name = "StickerPrice", targetNamespace = "https://services.autouplinkusa.com/VehicleInventory")
        String StickerPrice)
        throws CFCInvocationException
    ;

    @WebMethod(operationName = "PriceUpdate", action = "")
    @WebResult(name = "PriceUpdateReturn", targetNamespace = "https://services.autouplinkusa.com/VehicleInventory")
    public String priceUpdate(
        @WebParam(name = "VehicleInventoryAuth", targetNamespace = "https://services.autouplinkusa.com/VehicleInventory")
        WSAuth VehicleInventoryAuth,
        @WebParam(name = "VIN", targetNamespace = "https://services.autouplinkusa.com/VehicleInventory")
        String VIN,
        @WebParam(name = "Price", targetNamespace = "https://services.autouplinkusa.com/VehicleInventory")
        double Price)
        throws CFCInvocationException
    ;

    @WebMethod(operationName = "StickerPriceUpdate", action = "")
    @WebResult(name = "StickerPriceUpdateReturn", targetNamespace = "https://services.autouplinkusa.com/VehicleInventory")
    public String stickerPriceUpdate(
        @WebParam(name = "VehicleInventoryAuth", targetNamespace = "https://services.autouplinkusa.com/VehicleInventory")
        WSAuth VehicleInventoryAuth,
        @WebParam(name = "VIN", targetNamespace = "https://services.autouplinkusa.com/VehicleInventory")
        String VIN,
        @WebParam(name = "StickerPrice", targetNamespace = "https://services.autouplinkusa.com/VehicleInventory")
        double StickerPrice)
        throws CFCInvocationException
    ;

    @WebMethod(operationName = "CommentUpdate", action = "")
    @WebResult(name = "CommentUpdateReturn", targetNamespace = "https://services.autouplinkusa.com/VehicleInventory")
    public String commentUpdate(
        @WebParam(name = "VehicleInventoryAuth", targetNamespace = "https://services.autouplinkusa.com/VehicleInventory")
        WSAuth VehicleInventoryAuth,
        @WebParam(name = "VIN", targetNamespace = "https://services.autouplinkusa.com/VehicleInventory")
        String VIN,
        @WebParam(name = "Comment", targetNamespace = "https://services.autouplinkusa.com/VehicleInventory")
        String Comment)
        throws CFCInvocationException
    ;

}
