package biz.firstlook.export.reprice.autouplink;

import biz.firstlook.export.reprice.RepriceExportResult;
import biz.firstlook.export.reprice.RepriceTask;
import java.util.Date;


public enum PricesUpdateReturn 
{	 	
	FieldUpdateSuccessful("101"),	
	VehicleNotFound("301"),
	VehicleDeleted("302"),
	FieldHasSameValue("303"),
	FieldLocked("304"),
	AuthenticationError400("400"),
	AuthenticationError401("401"),
	SystemError500("500"),
	SystemError501("501"),
	SystemError510("510");
	
	private Integer _val;	
	
	
	private PricesUpdateReturn(String name) throws NumberFormatException
	{	
		_val = Integer.decode(name);
	}
	
	private Integer getReturn()
	{
		return _val;
	}	
	
	public static PricesUpdateReturn valueOf( final Integer number)
	{
		for( PricesUpdateReturn ret : PricesUpdateReturn.values())
		{
			if( ret.getReturn().equals( number ))
			{				
				return ret;
			}			
		}		
		return null;
	}	
	
	public RepriceExportResult getRepriceExportResult(RepriceTask repriceTask)
	{
		Date dateSent = new Date();
		RepriceExportResult repriceExportResult = 
			new RepriceExportResult(repriceTask.getRepriceEvent(), dateSent);
		
		switch(this)
		{
			case FieldHasSameValue:
			case FieldUpdateSuccessful:
				repriceExportResult.success();
				break;
			case AuthenticationError400:
			case AuthenticationError401:
			case FieldLocked:
			case SystemError500:
			case SystemError501:
			case SystemError510:
			case VehicleDeleted:
			case VehicleNotFound:
				repriceExportResult.fail("Fail in AutoUplinkReprice: " + this.toString() );
				break;
			default:
				repriceExportResult.fail("Fail in AutoUplinkReprice: " + "state unknown" );	
		}		
		return repriceExportResult;
	}	
}
