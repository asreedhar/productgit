package biz.firstlook.export.reprice.eBizAutos;

import java.util.Date;

import biz.firstlook.export.reprice.Credentials;
import biz.firstlook.export.reprice.RepriceExport;
import biz.firstlook.export.reprice.RepriceExportResult;
import biz.firstlook.export.reprice.RepriceTask;

import com.ebizautos.api.VehicleResponseChannel;
import com.ebizautos.api.VehicleResponseItem;
import com.ebizautos.api.VehicleResponseRss;
import com.ebizautos.api.XMLListener;
import com.ebizautos.api2.XMLListenerClient;
import com.ebizautos.api2.XMLListenerSoap;

class EBizRepriceTask extends RepriceTask
{


private final Credentials firstlookCredentials;
private final Credentials dealerCredentials;
private final String endpointUrl;
private final EBizResponseParser parser = new EBizResponseParser();

public EBizRepriceTask( RepriceExport event, Credentials firstlookCreds, Credentials dealerCreds, String endpointUrl )
{
	super( event );
	this.firstlookCredentials = firstlookCreds;
	this.dealerCredentials = dealerCreds;
	this.endpointUrl = endpointUrl;
}

public RepriceExportResult call() throws Exception
{
	Date dateSent = new Date();
	try
	{
		String rssPayload = transform( getRepriceEvent() );

		XMLListenerClient xmlClient = new XMLListenerClient(endpointUrl);
		XMLListenerSoap service = xmlClient.getXMLListenerSoap();

		log.info( "Sending to eBizAutos: " + getRepriceEvent() );
		dateSent = new Date();
		Object responseRss = service.sendVehicle( firstlookCredentials.getUsername(), firstlookCredentials.getPassword(),
														rssPayload );
		VehicleResponseRss vehicleResponseRss = parser.parse( responseRss.toString() );
		VehicleResponseChannel channel = vehicleResponseRss.getChannel();
		VehicleResponseItem item = (channel == null) ? null : channel.getItem();
		if(item == null) {
			channel = new VehicleResponseChannel();
			channel.setItem(null);
			channel.setMsg(responseRss.toString());
			vehicleResponseRss = new VehicleResponseRss();
			vehicleResponseRss.setChannel(channel);
		}

		RepriceExportResult result = transform( vehicleResponseRss, dateSent );
		return result;
	}
	catch ( Exception e )
	{
		RepriceExportResult result = new RepriceExportResult( getRepriceEvent(), dateSent );
		result.fail( e.getMessage() );
		return result;
	}
}

private String transform( RepriceExport repriceEvent )
{
	EBizRequestGenerator generator = new EBizRequestGenerator();
	String payload = generator.generate( Integer.valueOf( dealerCredentials.getUsername() ), dealerCredentials.getPassword(),
											repriceEvent.getInventoryItem(), Float.valueOf( repriceEvent.getListPrice() ) );
	return payload;
}

private RepriceExportResult transform( final VehicleResponseRss vehicleResponseRss, Date dateSent )
{
	VehicleResponseChannel channel = vehicleResponseRss.getChannel();
	VehicleResponseItem item = ( channel == null ? null : channel.getItem() );

	EBizResponseStatus status = EBizResponseStatus.fail;
	if ( item != null )
		status = EBizResponseStatus.valueOf( item.getStatus().toLowerCase() );

	RepriceExportResult result = null;
	switch ( status )
	{
		case success:
			result = new RepriceExportResult( getRepriceEvent(), dateSent );
			result.success();
			break;
		default:
			result = new RepriceExportResult( getRepriceEvent(), dateSent );
			StringBuilder errorMsg = new StringBuilder();
			if(item == null) {
				if(channel.getMsg() != null) {
					errorMsg.append(channel.getMsg());
				} else {
					errorMsg.append(getRepriceEvent().getInventoryItem().getVin()).append(", Unknown error.");
				}
			} else {
				errorMsg.append(getRepriceEvent().getInventoryItem().getVin()).append(", eBizAutos response index: ").append(item.getIndex()).toString();
			}
			result.fail(errorMsg.toString());
	}
	return result;
}

}
