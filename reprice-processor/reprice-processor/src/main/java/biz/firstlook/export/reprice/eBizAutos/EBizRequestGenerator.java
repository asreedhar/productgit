package biz.firstlook.export.reprice.eBizAutos;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import biz.firstlook.export.reprice.InventoryItem;
import biz.firstlook.syndication.feed.module.ebizautos.types.EbizModule;
import biz.firstlook.syndication.feed.module.ebizautos.types.EbizModuleImpl;
import biz.firstlook.syndication.feed.module.ebizautos.types.EbizFloatUnit;
import biz.firstlook.syndication.feed.module.ebizautos.types.RequestType;
import biz.firstlook.syndication.feed.synd.FeedType;

import com.sun.syndication.feed.module.Module;
import com.sun.syndication.feed.module.base.GoogleBaseImpl;
import com.sun.syndication.feed.module.base.Vehicle;
import com.sun.syndication.feed.module.base.types.YearType;
import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.synd.SyndContentImpl;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.synd.SyndFeedImpl;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedOutput;

public class EBizRequestGenerator
{

public String generate( Integer eClientId, String guid, InventoryItem inventoryItem, Float price )
{
	SyndFeed feed = new SyndFeedImpl();
	feed.setFeedType( FeedType.RSS_2_0.getFeedType() );
	feed.setTitle( "" );
	feed.setDescription( "" );
	feed.setLink( "" );
	
	EbizModule requestTypeMod = new EbizModuleImpl();
	requestTypeMod.setRequest_type(RequestType.updateprice.toString());
	
	List<Module> feedMods = new ArrayList<Module>();
	feedMods.add(requestTypeMod);
	
	feed.setModules(feedMods);
	
	SyndEntry entry = new SyndEntryImpl();
	entry.setTitle( "" );
	entry.setPublishedDate( new Date() );
	
	SyndContent content = new SyndContentImpl();
	content.setType( "text/plain" );
	content.setValue( "" );
	
	Vehicle vehicle = new GoogleBaseImpl();
	vehicle.setMake( inventoryItem.getMake() );
	vehicle.setModel( inventoryItem.getModel() );
	vehicle.setYear( new YearType( inventoryItem.getYear().toString() ) );
	vehicle.setVin( inventoryItem.getVin() );
	vehicle.setMileage( inventoryItem.getMileage() );
	vehicle.setCondition( inventoryItem.getInventoryType() );
	
//	 eBizWebService rejects float Unit as of now
	vehicle.setPrice( new EbizFloatUnit( price, "USD" ) );
	
	EbizModule ebizInfoMod = new EbizModuleImpl();
	ebizInfoMod.setClient_id(eClientId);
	ebizInfoMod.setStock_no( inventoryItem.getStockNumber() );
	
	List<Object> entryMods = new ArrayList<Object>();
	entryMods.add( vehicle );
	entryMods.add(ebizInfoMod);
	
	entry.setModules( entryMods );
	entry.setDescription( content );
	entry.setUri(guid);
	
	List< SyndEntry > entries = new ArrayList<SyndEntry>();
	entries.add( entry );
	
	feed.setEntries( entries );
	
	StringWriter writer = new StringWriter();
	SyndFeedOutput output = new SyndFeedOutput();
	try
	{
		output.output( feed, writer );
		writer.close();
	}
	catch ( IOException e )
	{
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	catch ( FeedException e )
	{
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	Pattern rssPattern = Pattern.compile("<rss.*>");
	Matcher rssMatcher = rssPattern.matcher(writer.toString());
	String withNewRssHeader = rssMatcher
				.replaceAll("<rss version=\"2.0\" xmlns:e=\"http://XML.eBizAutos.com\" xmlns:g=\"http://base.google.com/ns/1.0\">");

	Pattern nameSpacePattern = Pattern.compile("g-core");
	Matcher nameSpaceMatcher = nameSpacePattern.matcher(withNewRssHeader);
	String gNameSpace = nameSpaceMatcher.replaceAll("g");
		
        
	return gNameSpace;
}

}
