package biz.firstlook.export.reprice.eBizAutos;

public class EBizResponseParseException extends Exception
{

private static final long serialVersionUID = -1835389174709700479L;

public EBizResponseParseException()
{
}

public EBizResponseParseException( String s )
{
	super(s);
}

public EBizResponseParseException( Throwable t )
{
	super(t);
}

}
