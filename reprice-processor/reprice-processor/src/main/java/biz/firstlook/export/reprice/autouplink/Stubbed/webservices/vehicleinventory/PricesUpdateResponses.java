
package biz.firstlook.export.reprice.autouplink.Stubbed.webservices.vehicleinventory;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PricesUpdateResponses complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PricesUpdateResponses">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InternetPriceUpdateReturn" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PriceUpdateReturn" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StickerPriceUpdateReturn" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PricesUpdateResponses", propOrder = {
    "internetPriceUpdateReturn",
    "priceUpdateReturn",
    "stickerPriceUpdateReturn"
})
public class PricesUpdateResponses {

    @XmlElement(name = "InternetPriceUpdateReturn", required = true, nillable = true)
    protected String internetPriceUpdateReturn;
    @XmlElement(name = "PriceUpdateReturn", required = true, nillable = true)
    protected String priceUpdateReturn;
    @XmlElement(name = "StickerPriceUpdateReturn", required = true, nillable = true)
    protected String stickerPriceUpdateReturn;

    /**
     * Gets the value of the internetPriceUpdateReturn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInternetPriceUpdateReturn() {
        return internetPriceUpdateReturn;
    }

    /**
     * Sets the value of the internetPriceUpdateReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInternetPriceUpdateReturn(String value) {
        this.internetPriceUpdateReturn = value;
    }

    /**
     * Gets the value of the priceUpdateReturn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriceUpdateReturn() {
        return priceUpdateReturn;
    }

    /**
     * Sets the value of the priceUpdateReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriceUpdateReturn(String value) {
        this.priceUpdateReturn = value;
    }

    /**
     * Gets the value of the stickerPriceUpdateReturn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStickerPriceUpdateReturn() {
        return stickerPriceUpdateReturn;
    }

    /**
     * Sets the value of the stickerPriceUpdateReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStickerPriceUpdateReturn(String value) {
        this.stickerPriceUpdateReturn = value;
    }

}
