package biz.firstlook.export.reprice;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.object.StoredProcedure;

class RepriceExportResultPersistorImpl extends StoredProcedure implements
		RepriceExportResultPersistor {
	
	private static final String STORED_PROC_NAME = "dbo.RepriceExport#Update";
	private static final String PARAM_RESULT_SET = "ResultSet";
	
	private static final String PARAM_REPRICE_EXPORT_ID = "RepriceExportID";
	private static final String PARAM_REPRICE_EXPORT_STATUS_ID = "RepriceExportStatusID";
	private static final String PARAM_FAILURE_REASON = "FailureReason";
	private static final String PARAM_FAILURE_COUNT = "FailureCount";
	private static final String PARAM_DATESENT = "DateSent";

	public RepriceExportResultPersistorImpl(DataSource datasource) {
		super(datasource, STORED_PROC_NAME);
		setFunction(false);
		declareParameter(new SqlReturnResultSet(PARAM_RESULT_SET,
				new RepriceExportResultRowMapper()));
		
		declareParameter(new SqlParameter(PARAM_REPRICE_EXPORT_ID, Types.INTEGER));
		declareParameter(new SqlParameter(PARAM_REPRICE_EXPORT_STATUS_ID, Types.INTEGER));
		declareParameter(new SqlParameter(PARAM_FAILURE_REASON, Types.VARCHAR));
		declareParameter(new SqlParameter(PARAM_FAILURE_COUNT, Types.INTEGER));
		declareParameter(new SqlParameter(PARAM_DATESENT, Types.TIMESTAMP));
		compile();
	}
	
	public void save(RepriceExportResult result) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(PARAM_REPRICE_EXPORT_ID, result.getId());
		parameters.put(PARAM_REPRICE_EXPORT_STATUS_ID, result.getRepriceEventStatus().getId());
		
		String failureReason = result.getFailureReason();
		if(failureReason != null && failureReason.length() > 2000) {
			failureReason = failureReason.substring(0, 1999);
		}
		parameters.put(PARAM_FAILURE_REASON, failureReason);
		parameters.put(PARAM_FAILURE_COUNT, result.getFailureCount());
		parameters.put(PARAM_DATESENT, new java.sql.Timestamp(result.getSentDate().getTime()));
		
		execute(parameters);
	}
	
	private static class RepriceExportResultRowMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int index) throws SQLException {
			return null;
		}
	}
}
