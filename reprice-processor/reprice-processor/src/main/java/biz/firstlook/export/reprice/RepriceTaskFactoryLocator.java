package biz.firstlook.export.reprice;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;


class RepriceTaskFactoryLocator implements ApplicationContextAware, InitializingBean
{

private ApplicationContext ac;
private Set<RepriceTaskFactory> factories = new HashSet<RepriceTaskFactory>();

public Collection<RepriceTaskFactory> getFactories() {
	return factories;
}

public void setApplicationContext( ApplicationContext ac ) throws BeansException {
	this.ac = ac;
}

public void afterPropertiesSet() throws Exception {
	String[] beanNames = ac.getBeanNamesForType( RepriceTaskFactory.class );
	for( String beanName : beanNames ) {
		RepriceTaskFactory factory = (RepriceTaskFactory)ac.getBean( beanName );
		factories.add(factory);
	}
}

}
