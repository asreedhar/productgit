package biz.firstlook.export.reprice.rd;

import java.util.Date;

import biz.firstlook.client.dms.rd.ReynoldsDirectInventoryUpdateClient;
import biz.firstlook.export.reprice.RepriceExport;
import biz.firstlook.export.reprice.RepriceExportResult;
import biz.firstlook.export.reprice.RepriceTask;
import biz.firstlook.export.reprice.sis.SISVinAndStockNumberMapper;

public class ReynoldsDirectRepriceTask extends RepriceTask {

	private final ReynoldsDirectInventoryUpdateClient reynoldsDirectInventoryUpdateClient;
	private final SISVinAndStockNumberMapper sisVinAndStockNumberMapper;
	
	public ReynoldsDirectRepriceTask(RepriceExport request, ReynoldsDirectInventoryUpdateClient reynoldsDirectInventoryUpdateClient, SISVinAndStockNumberMapper sisVinAndStockNumberMapper) {
		super(request);
		this.reynoldsDirectInventoryUpdateClient = reynoldsDirectInventoryUpdateClient;
		this.sisVinAndStockNumberMapper = sisVinAndStockNumberMapper;
	}

	public RepriceExportResult call() throws Exception {
		Integer businessUnitId = getRepriceEvent().getBusinessUnitId();
		String vin = getRepriceEvent().getInventoryItem().getVin();
		String stockNumber = getRepriceEvent().getInventoryItem().getStockNumber();
		Integer thirdPartyEntityId = getRepriceEvent().getThirdPartyEntity().getThirdPartyEntityID();
		String storeNumber = getRepriceEvent().getInventoryItem().getStoreNumber();
		String branchNumber = getRepriceEvent().getInventoryItem().getBranchNumber();
		String usedGroup = getRepriceEvent().getInventoryItem().getUsedGroup();
		
		
		// see if we have these mapped 
		// change to reynolds mapping
		String[] mappedVinAndStock = sisVinAndStockNumberMapper.getMappedVinAndStock(businessUnitId, vin, stockNumber);
		if (mappedVinAndStock != null) {
			if (mappedVinAndStock[0] != null && mappedVinAndStock[0].trim().length() > 0) {
				vin = mappedVinAndStock[0];
			}
			if (mappedVinAndStock[1] != null && mappedVinAndStock[1].trim().length() > 0) {
				stockNumber = mappedVinAndStock[1];
			}
		}
		
		Date dateSent = new Date();
		String xmlResponse = reynoldsDirectInventoryUpdateClient.sendRepriceEvent(businessUnitId, thirdPartyEntityId, stockNumber, vin, getRepriceEvent().getListPrice().doubleValue(), storeNumber, branchNumber, usedGroup);
		
		RepriceExportResult result = new RepriceExportResult( getRepriceEvent(), dateSent);
		if (xmlResponse != null && xmlResponse.indexOf("ERROR") == -1) {
			result.success();
		} else {
			log.error(xmlResponse);
			result.fail(xmlResponse);
		}
		return result;
	}

}
