package biz.firstlook.export.reprice;

import java.util.ArrayList;
import java.util.List;

class ShutdownEventDispatcher implements Runnable {

	private final List<LifeCycleEventListener> listeners = new ArrayList<LifeCycleEventListener>();
	
	public void run() {
		synchronized (listeners) {
			for(LifeCycleEventListener l : listeners) {
				l.shutdown();
			}
		}
	}

	public synchronized boolean addListener(LifeCycleEventListener listener) {
		return listeners.add(listener);
	}
}
