package biz.firstlook.export.reprice;

public abstract class AbstractRepriceTaskFactory implements RepriceTaskFactory {

	private int numConcurrentTasks = 1;
	
	/**
	 * Configuration set by spring config.
	 * @param numConcurrentTasks
	 */
	public void setNumConcurrentTasks(int numConcurrentTasks) {
		if(numConcurrentTasks < 1) {
			throw new IllegalArgumentException("Number of concurrent tasks must be between 1 and Integer.MAX_VALUE inclusive.");
		}
		this.numConcurrentTasks = numConcurrentTasks;
	}

	public int allowNumConcurrentTasks() {
		if(numConcurrentTasks < 1)
			return 1;
		return numConcurrentTasks;
	}
}
