package biz.firstlook.export.reprice;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.object.StoredProcedure;

class RepriceExportLockRetrieverImpl extends StoredProcedure implements RepriceExportLockRetriever {

	private static final String STORED_PROC_NAME = "dbo.RepriceExport#SetStatusInProgress";
	private static final String PARAM_RESULT_SET = "ResultSet";
	private static final String PARAM_REPRICE_EXPORT_ID = "RepriceExportID";

	public RepriceExportLockRetrieverImpl(DataSource datasource) {
		super(datasource, STORED_PROC_NAME);
		setFunction(false);
		declareParameter(new SqlReturnResultSet(PARAM_RESULT_SET,
				new RepriceExportLockRetrieverRowMapper()));
		
		declareParameter(new SqlParameter(PARAM_REPRICE_EXPORT_ID, Types.INTEGER));
		compile();
	}
	
	@SuppressWarnings("unchecked")
	public boolean obtainLock(RepriceExport repriceExport) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(PARAM_REPRICE_EXPORT_ID, repriceExport.getId());

		Map results = execute(parameters);
		
		Boolean result = Boolean.FALSE;
//		Integer returnCode = (Integer)results.get(PARAM_RC);
//		if(returnCode != null && returnCode.equals(Integer.valueOf(0))) {

		List<Boolean> rows = (List<Boolean>)results.get(PARAM_RESULT_SET);
		Boolean rs = (rows == null || rows.isEmpty()) ? Boolean.FALSE : rows.get(0);
		if(rs != null) {
			result = rs;
		}
		return result;
	}

	private static class RepriceExportLockRetrieverRowMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int index) throws SQLException {
			Boolean result = rs.getBoolean("IsLockObtained");
			return result;
		}
	}
}
