package biz.firstlook.export.reprice;

import java.io.Serializable;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class InventoryItem implements Serializable {

private static final long serialVersionUID = -2316200864765506609L;

public static final Integer INVENTORY_TYPE_NEW = Integer.valueOf(1);
public static final Integer INVENTORY_TYPE_USED = Integer.valueOf(2);
	
private final String vin;
private final Integer year;
private final String make;
private final String model;
private final String stockNumber;
private final String inventoryType;
private final Integer mileage;
private String storeNumber;
private String branchNumber;
private String usedGroup;

/**
 * 
 * @param vin
 * @param year
 * @param make
 * @param model
 * @param stockNumber
 * @param inventoryType
 * @param mileage
 */
public InventoryItem( String vin, Integer year, String make, String model, String stockNumber, Integer inventoryType, Integer mileage )
{
	super();
	this.vin = vin;
	this.year = year;
	this.make = make;
	this.model = model;
	this.stockNumber = stockNumber;
	this.storeNumber = "";
	this.branchNumber = "";
	this.usedGroup = "";
	
	if( inventoryType != null ) {
		this.inventoryType = (INVENTORY_TYPE_NEW.equals(inventoryType)) ? "New" : "Used";
	} else {
		this.inventoryType = "";
	}
	this.mileage = mileage;
}

public InventoryItem( String vin, Integer year, String make, String model, String stockNumber, Integer inventoryType, Integer mileage, String storeNumber, String branchNumber, String usedGroup )
{
	this( vin, year, make, model, stockNumber, inventoryType, mileage );
	this.storeNumber = storeNumber;
	this.branchNumber = branchNumber;
	this.usedGroup = usedGroup;
}

public String getInventoryType()
{
	return inventoryType;
}

public String getMake()
{
	return make;
}

public String getModel()
{
	return model;
}

public String getStockNumber()
{
	return stockNumber;
}

public String getVin()
{
	return vin;
}

public Integer getYear()
{
	return year;
}

public Integer getMileage()
{
	return mileage;
}

public String getStoreNumber()
{
	return storeNumber;
}

public String getBranchNumber()
{
	return branchNumber;
}

public String getUsedGroup()
{
	return usedGroup;
}

public String toString()
{
	return ReflectionToStringBuilder.reflectionToString( this, ToStringStyle.SIMPLE_STYLE );
}

@Override
public int hashCode()
{
	final int PRIME = 31;
	int result = 1;
	result = PRIME * result + ( ( inventoryType == null ) ? 0 : inventoryType.hashCode() );
	result = PRIME * result + ( ( make == null ) ? 0 : make.hashCode() );
	result = PRIME * result + ( ( mileage == null ) ? 0 : mileage.hashCode() );
	result = PRIME * result + ( ( model == null ) ? 0 : model.hashCode() );
	result = PRIME * result + ( ( stockNumber == null ) ? 0 : stockNumber.hashCode() );
	result = PRIME * result + ( ( vin == null ) ? 0 : vin.hashCode() );
	result = PRIME * result + ( ( year == null ) ? 0 : year.hashCode() );
	return result;
}

@Override
public boolean equals( Object obj )
{
	if ( this == obj )
		return true;
	if ( obj == null )
		return false;
	if ( getClass() != obj.getClass() )
		return false;
	final InventoryItem other = (InventoryItem)obj;
	if ( inventoryType == null )
	{
		if ( other.inventoryType != null )
			return false;
	}
	else if ( !inventoryType.equals( other.inventoryType ) )
		return false;
	if ( make == null )
	{
		if ( other.make != null )
			return false;
	}
	else if ( !make.equals( other.make ) )
		return false;
	if ( mileage == null )
	{
		if ( other.mileage != null )
			return false;
	}
	else if ( !mileage.equals( other.mileage ) )
		return false;
	if ( model == null )
	{
		if ( other.model != null )
			return false;
	}
	else if ( !model.equals( other.model ) )
		return false;
	if ( stockNumber == null )
	{
		if ( other.stockNumber != null )
			return false;
	}
	else if ( !stockNumber.equals( other.stockNumber ) )
		return false;
	if ( vin == null )
	{
		if ( other.vin != null )
			return false;
	}
	else if ( !vin.equals( other.vin ) )
		return false;
	if ( year == null )
	{
		if ( other.year != null )
			return false;
	}
	else if ( !year.equals( other.year ) )
		return false;
	return true;
}

}
