package biz.firstlook.export.reprice.sis;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

/**
 * THIS IS A TEMPORARY CLASS. ONLY INTENEDED TO BE USED DURING TESTING
 * UNTIL WE AND SIS ARE WORKING FROM THE SAME DEALER WITH MATCHING 
 * STOCK AND VIN NUMBERS.
 * 
 * @author nkeen
 */
public class SISVinAndStockNumberMapper extends JdbcDaoSupport {

	public String[] getMappedVinAndStock(int businessUnit, String vin, String stock) {
		try {
			String query = 	"select SISVin, SISStockNumber " +
							"from dbo.SISVinAndStockMapping " +
							"where BusinessUnitId=?" +
							" and Vin=?" +
							" and StockNumber=?";
			Object[] parameters = { businessUnit, vin, stock };
			return (String[])getJdbcTemplate().query(query, parameters, new SISVinAndStockNumberRowMapper()).get(0);
		} catch (Exception e) {
			// any errors - return null
			return null;
		}
	}
	
	private static class SISVinAndStockNumberRowMapper implements RowMapper {
		public String[] mapRow(ResultSet rs, int arg1) throws SQLException {
			String vin = rs.getString("SISVin");
			String stock = rs.getString("SISStockNumber");
			return new String[] {vin, stock};
		}
	}
	
}
