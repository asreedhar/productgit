package biz.firstlook.export.reprice.eBizAutos;

import java.io.StringReader;

import org.apache.commons.digester.Digester;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ebizautos.api.VehicleResponseChannel;
import com.ebizautos.api.VehicleResponseItem;
import com.ebizautos.api.VehicleResponseRss;

public class EBizResponseParser
{

private static final Log log = LogFactory.getLog( EBizResponseParser.class );

private Digester getDigester() {
	//move configuration into xml file at somepoint.
	//also use RSSDigester at somepoint.
	Digester digester = new Digester();
	digester.setValidating( false );
	digester.addObjectCreate( "rss", VehicleResponseRss.class );
	digester.addObjectCreate( "rss/channel", VehicleResponseChannel.class );
	digester.addSetNext( "rss/channel", "setChannel" );
	digester.addObjectCreate( "rss/channel/item", VehicleResponseItem.class );
	digester.addSetNext( "rss/channel/item", "setItem" );
	digester.addBeanPropertySetter( "rss/channel/item/g:vin", "vin" );
	digester.addBeanPropertySetter( "rss/channel/item/status", "status" );
	digester.addBeanPropertySetter( "rss/channel/item/timestamp", "timestamp" );
	digester.addBeanPropertySetter( "rss/channel/item/index", "index" );
	return digester;
}

public VehicleResponseRss parse( String responseRss ) throws EBizResponseParseException
{
	VehicleResponseRss response = null;
	StringReader reader = null;
	try {
		log.debug( "Attempting to parse response..." + responseRss );
		reader = new StringReader( responseRss );		
		response = (VehicleResponseRss)getDigester().parse( reader );
	}
	catch (Exception e) {
		log.debug(e);
		throw new EBizResponseParseException( e );
	}
	finally
	{
		if( reader != null )
			reader.close();
	}
	return response;
}

}
