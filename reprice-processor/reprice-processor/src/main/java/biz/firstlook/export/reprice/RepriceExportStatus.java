package biz.firstlook.export.reprice;

public enum RepriceExportStatus
{

InQueue(1), InProgress(2), Successful(3), Failure(4), ConfirmedFailure(5);

private Integer id;

private RepriceExportStatus( Integer id )
{
	this.id = id;
}

public Integer getId()
{
	return id;
}

public static RepriceExportStatus valueOf( final Integer id )
{
	for( RepriceExportStatus status : RepriceExportStatus.values() )
	{
		if( status.getId().equals( id ) )
			return status;
	}
	return null;
}

}
