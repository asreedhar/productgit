package biz.firstlook.export.reprice.autouplink;


import biz.firstlook.export.reprice.Credentials;
import biz.firstlook.export.reprice.RepriceExport;
import biz.firstlook.export.reprice.RepriceExportResult;
import biz.firstlook.export.reprice.RepriceTask;
import biz.firstlook.export.reprice.autouplink.Stubbed.vehicleinventory1;
import biz.firstlook.export.reprice.autouplink.Stubbed.vehicleinventory1ServiceClient;
import biz.firstlook.export.reprice.autouplink.Stubbed.webservices.vehicleinventory.WSAuth;

import java.util.Date;


class AutoUplinkRepriceTask extends RepriceTask
{
	private final Credentials credentials;
	private final String dealerID;
	private final String endpointUrl;	
	private final boolean isInternetPriceUpdateRequired;

	public AutoUplinkRepriceTask(RepriceExport event, Credentials credentials ,String dealerID,String endpointUrl, boolean isInternetPriceUpdateRequired)
	{
		super( event );
		this.credentials = credentials;
		this.dealerID = dealerID;
		this.endpointUrl = endpointUrl;
		this.isInternetPriceUpdateRequired = isInternetPriceUpdateRequired;
	}

	public RepriceExportResult call() throws Exception 
	{
		vehicleinventory1ServiceClient client = 
			new vehicleinventory1ServiceClient(this.endpointUrl);
		
		vehicleinventory1 inventory = client.getvehicleinventory1_cfc();
		WSAuth wsAuth = new WSAuth();		
		
		wsAuth.setDealerID(dealerID);
		wsAuth.setUserName(credentials.getUsername());
		wsAuth.setPassword(credentials.getPassword());
		
		String vin = this.getRepriceEvent().getInventoryItem().getVin();
		Double newPrice = this.getRepriceEvent().getListPrice().doubleValue();
		
		//Make the actual call
		RepriceExportResult repriceExportResult = null;		
		try 
		{
			log.info( "Sending to AutoUplink with dealer code " + dealerID + ": " + this.getRepriceEvent() );						
			
			// When the provider is Aultec, and the dealer has Aultec enabled in fl-admin's Extract Config tab, update 'InternetPrice'.
			if (isInternetPriceUpdateRequired)
			{
				String internetPriceUpdateReturn = inventory.internetPriceUpdate(wsAuth, vin, newPrice);				
				PricesUpdateReturn pricesUpdateReturn = PricesUpdateReturn.valueOf(Integer.decode(internetPriceUpdateReturn));				
				repriceExportResult = pricesUpdateReturn.getRepriceExportResult(this);
			}
			// In all other situations, update 'Price'.
			else
			{
				String internetPriceUpdateReturn = inventory.priceUpdate(wsAuth, vin, newPrice);				
				PricesUpdateReturn pricesUpdateReturn = PricesUpdateReturn.valueOf(Integer.decode(internetPriceUpdateReturn));				
				repriceExportResult = pricesUpdateReturn.getRepriceExportResult(this);				
			}	
		}
		catch (Exception e) 
		{			
			//just create one from what we do have. new Date() will return the current time. 
			repriceExportResult = new RepriceExportResult(this.getRepriceEvent(), new Date());
			repriceExportResult.fail(e.getMessage());
			
		}
		return repriceExportResult;
	}
}

