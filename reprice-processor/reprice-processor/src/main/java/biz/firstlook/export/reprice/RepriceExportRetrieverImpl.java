package biz.firstlook.export.reprice;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.object.StoredProcedure;


final class RepriceExportRetrieverImpl extends StoredProcedure implements RepriceExportRetriever {

	private static final String STORED_PROC_NAME = "dbo.RepriceExportCollection#Fetch";
	private static final String PARAM_RESULT_SET = "ResultSet";
	private static final String PARAM_DEALER_ID = "DealerID";
	private static final String PARAM_THIRDPARTYENTITYID = "ThirdPartyEntityID";
	private static final String PARAM_MAXFAILUREATTEMPTS = "MaxFailureAttempts";
	private static final String PARAM_MINREPRICEEXPORTID = "MinRepriceExportID";

	public RepriceExportRetrieverImpl(DataSource datasource) {
		super(datasource, STORED_PROC_NAME);
		setFunction(false);
		declareParameter(new SqlReturnResultSet(PARAM_RESULT_SET,
				new RepriceExportResultSetExtractor()));
		
		declareParameter(new SqlParameter(PARAM_DEALER_ID, Types.INTEGER));
		declareParameter(new SqlParameter(PARAM_THIRDPARTYENTITYID, Types.INTEGER));
		declareParameter(new SqlParameter(PARAM_MAXFAILUREATTEMPTS, Types.INTEGER));
		declareParameter(new SqlParameter(PARAM_MINREPRICEEXPORTID, Types.INTEGER));
		compile();
	}

	/* (non-Javadoc)
	 * @see biz.firstlook.export.reprice.impl.RepriceExportRetriever#getRepriceEvents(java.lang.Integer, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	public List<RepriceExport> getRepriceExports(Integer dealerId, final Integer thirdPartyEntityId, final Integer failureAttempts, Integer minRepriceExportId) {

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(PARAM_DEALER_ID, dealerId);
		parameters.put(PARAM_THIRDPARTYENTITYID, thirdPartyEntityId);
		parameters.put(PARAM_MAXFAILUREATTEMPTS, failureAttempts);
		parameters.put(PARAM_MINREPRICEEXPORTID, minRepriceExportId);

		Map results = execute(parameters);
		
//		Integer returnCode = (Integer)results.get(PARAM_RC);
//		if(returnCode != null && returnCode.equals(Integer.valueOf(0))) {
		List<RepriceExport> result = new ArrayList<RepriceExport>();
		result.addAll((List<RepriceExport>)results.get(PARAM_RESULT_SET));
		
		return result;
	}
	
	private static class RepriceExportResultSetExtractor implements
			ResultSetExtractor {
		public Object extractData(ResultSet rs) throws SQLException,
				DataAccessException {
			List<RepriceExport> repriceEvents = new ArrayList<RepriceExport>();
			while(rs.next()) {
				Integer thirdPartyId = rs.getInt( "ThirdPartyEntityID" );
				String thirdPartyName = rs.getString( "Name" );
				Integer businessUnitId = rs.getInt( "BusinessUnitID" );
				Integer thirdPartyEntityTypeId = rs.getInt( "thirdPartyEntityTypeId" );
				ThirdPartyEntity thirdPartyEntity = null; 
				if (ThirdPartyEntityTypeEnum.DMS.getId().intValue() == thirdPartyEntityTypeId.intValue()) {
					thirdPartyEntity = new DMSExportThirdPartyEntity( thirdPartyId, businessUnitId, thirdPartyName );
				} else {
					thirdPartyEntity = new InternetAdvertiserThirdPartyEntity( thirdPartyId, businessUnitId, thirdPartyName );
				}

				String vin = rs.getString( "Vin" );
				Integer vehicleYear = rs.getInt( "VehicleYear" );
				String make = rs.getString( "Make" );
				String model = rs.getString( "Model" );
				String stockNumber = rs.getString( "StockNumber" );
				Integer inventoryType = rs.getInt( "InventoryType" );
				Integer mileage = rs.getInt( "MileageReceived" );
				String storeNumber = rs.getString( "StoreNumber" );
				String branchNumber = rs.getString( "BranchNumber" );
				String usedGroup = rs.getString( "UsedGroup" );
				
				InventoryItem item = new InventoryItem( vin, vehicleYear, make, model, stockNumber, inventoryType, mileage, storeNumber, branchNumber, usedGroup );

				Integer id = rs.getInt( "RepriceExportID" );
				Integer listPrice = rs.getInt( "NewPrice" );
				RepriceExportStatus status = RepriceExportStatus.valueOf( rs.getInt( "RepriceExportStatusID" ) );
				Integer failureCount = rs.getInt( "failureCount" );
				RepriceExport event = new RepriceExport( id, thirdPartyEntity, businessUnitId, item, listPrice, status, failureCount );
				repriceEvents.add(event);
			}
			return repriceEvents;
		}

	}

}
