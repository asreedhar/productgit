package biz.firstlook.export.reprice;

import java.util.List;


interface RepriceExportRetriever {

	/**
	 * Returns the RepriceExport objects for processing
	 * @param dealerId TODO
	 * @param thirdPartyEntityId
	 * @param failureAttempts
	 * @param minRepriceExportId TODO
	 * @return a list containing RepriceExport objects.  An empty list if there are non.  Never null.
	 */
	public abstract List<RepriceExport> getRepriceExports(
			Integer dealerId, final Integer thirdPartyEntityId, final Integer failureAttempts, Integer minRepriceExportId);

}
