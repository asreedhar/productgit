package biz.firstlook.export.reprice.sis;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import biz.firstlook.client.dms.sis.SISInventoryUpdateClient;
import biz.firstlook.export.reprice.RepriceExport;
import biz.firstlook.export.reprice.RepriceExportResult;
import biz.firstlook.export.reprice.RepriceTask;

public class SISRepriceTask extends RepriceTask {

	// these get passed by the client
	private final SISInventoryUpdateClient sisInventoryUpdateClient;
	private final SISVinAndStockNumberMapper sisVinAndStockNumberMapper;
	
	public SISRepriceTask(RepriceExport event, SISInventoryUpdateClient client, SISVinAndStockNumberMapper sisVinAndStockNumberMapper) {
		super(event);
		this.sisInventoryUpdateClient = client;
		this.sisVinAndStockNumberMapper = sisVinAndStockNumberMapper;
	}

	public RepriceExportResult call() throws Exception {
		Integer businessUnitId = getRepriceEvent().getBusinessUnitId();
		String vin = getRepriceEvent().getInventoryItem().getVin();
		String stockNumber = getRepriceEvent().getInventoryItem().getStockNumber();
		Integer thirdPartyEntityId =getRepriceEvent().getThirdPartyEntity().getThirdPartyEntityID();
		
		// see if we have these mapped 
		String[] mappedVinAndStock = sisVinAndStockNumberMapper.getMappedVinAndStock(businessUnitId, vin, stockNumber);
		if (mappedVinAndStock != null) {
			if (mappedVinAndStock[0] != null && mappedVinAndStock[0].trim().length() > 0) {
				vin = mappedVinAndStock[0];
			}
			if (mappedVinAndStock[1] != null && mappedVinAndStock[1].trim().length() > 0) {
				stockNumber = mappedVinAndStock[1];
			}
		}
		Date dateSent = new Date();
		String xmlResponse = sisInventoryUpdateClient.sendRepriceEvent(businessUnitId,thirdPartyEntityId, stockNumber, vin, getRepriceEvent().getListPrice().doubleValue());

		// we could get more fancy by dealing with different error codes. But for now, it either works or it doesn't. 
		// And there is no point parsing the XML response with an actual XML parser.
		RepriceExportResult result = new RepriceExportResult( getRepriceEvent(), dateSent);
		if (StringUtils.isNotBlank(xmlResponse) && xmlResponse.indexOf("ERROR") == -1) {
			result.success();
		} else {
			if(StringUtils.isBlank(xmlResponse)) {
				log.error("No response from SIS.");
				result.fail("No response from SIS.");
			} else if (xmlResponse.indexOf("UPDATE NOT PERMITTED") >= -1) {
				log.warn("UPDATE NOT PERMITTED for Stock Number " + stockNumber);
				result.hardFailure(xmlResponse);
			} else if( xmlResponse.indexOf("COMPANY") >= -1 
					&& xmlResponse.indexOf("NOT") >= -1
					&& xmlResponse.indexOf("FOUND") >= -1
					&& xmlResponse.indexOf("EXCEPTION") >= -1) {
				log.warn("COMPANY NOT FOUND EXCEPTION: something happened to our credentials!");
				result.hardFailure(xmlResponse);
			} else {
				log.error(xmlResponse);
				result.fail(xmlResponse);
			}
		}
		return result;
	}
}
