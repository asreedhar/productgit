package biz.firstlook.export.reprice;

interface RepriceExportResultPersistor {
	
	void save(RepriceExportResult result);
	
}
