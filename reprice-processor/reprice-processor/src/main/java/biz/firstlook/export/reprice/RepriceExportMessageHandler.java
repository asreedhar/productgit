package biz.firstlook.export.reprice;

import java.text.MessageFormat;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

/**
 * The framework class that contains the logic to execute the remote call to the 3rd party listing provider.
 * @author bfung
 *
 */
class RepriceExportMessageHandler implements Runnable, LifeCycleEventListener {

	private final Logger log = Logger.getLogger(RepriceExportMessageHandler.class);
	
	private final Integer failureCount;
	
	private final RepriceExportRetriever repriceExportRetriever;
	private final RepriceExportLockRetriever repriceExportLockRetriever;
	
	private final BlockingQueue<RepriceExport> inQueue = new LinkedBlockingQueue<RepriceExport>(500);
	
	private final RepriceTaskFactory factory;
	private final Integer thirdPartyId;
	
	private final BlockingQueue<RepriceExportResult> outQueue;
	private final ExecutorService pool;
	
	private final DatabaseQueueReader producer = new DatabaseQueueReader(); 
	
	private volatile boolean shuttingDown = false;
	
	public RepriceExportMessageHandler(
			final Integer failureCount,
			final RepriceExportRetriever repriceExportRetriever,
			final RepriceExportLockRetriever repriceExportLockRetriever,
			final RepriceTaskFactory factory,
			final BlockingQueue<RepriceExportResult> outQueue) {
		
		this.failureCount = failureCount;
		this.repriceExportRetriever = repriceExportRetriever;
		this.repriceExportLockRetriever = repriceExportLockRetriever;
		this.factory = factory;
		this.thirdPartyId = factory.getThirdPartyId();
		this.outQueue = outQueue;
		
		this.pool = new ThreadPoolExecutor(
				factory.allowNumConcurrentTasks(), 
				factory.allowNumConcurrentTasks(),
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>(500));  //limit memory growth
	}
	
	public void run() {
		Thread queueReader = new Thread(producer, "QueueReader-ThirdPartyId:" + thirdPartyId);
		queueReader.start();
		
		while(!shuttingDown) {
			RepriceExport export = null;
			try {
				export = inQueue.poll(2L, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				//nothing in queue, check if we need to shutdown or not
				if(shuttingDown) {
					break;
				} else {
					continue;
				}
			}
			
			if(export != null) {
				try {
					pool.execute(new TaskRunner(export));
				} catch (RejectedExecutionException e) {
					if(log.isInfoEnabled()) {
						log.info(MessageFormat.format("[MessageHandler-ThirdPartyId:{0}] Rejected the task for execution; shutdown={0}", shuttingDown));
					}
					break;
				}
			}
		}
	}
	
	public synchronized void shutdown() {
		shuttingDown = true;
		pool.shutdown();
	}
	
	public Integer getThirdPartyId() {
		return thirdPartyId;
	}
	
	private class TaskRunner implements Runnable {
		
		private RepriceExport export;
		private String id_msg;
		
		TaskRunner(RepriceExport export) {
			this.export = export;
			this.id_msg = export.getId().toString(); 
		}
		
		public void run() {
			if(log.isInfoEnabled()) {
				log.info(MessageFormat.format("[MessageHandler-ThirdPartyId:{0}] Attempting to obtain lock for RepriceExport with id: {1}", thirdPartyId.toString(), id_msg));
			}
			
			boolean lockObtained = false;
			try {
				lockObtained = repriceExportLockRetriever.obtainLock(export);
			} catch (Exception e) {
				if(log.isInfoEnabled()) {
					log.info(MessageFormat.format("[MessageHandler-ThirdPartyId:{0}] Could not obtain lock for RepriceExport with id: {1}, {2}", thirdPartyId.toString(), id_msg, e.getMessage()));
				}
				return;
			}
			
			if(lockObtained) {
				if(log.isInfoEnabled()) {
					log.info(MessageFormat.format("[MessageHandler-ThirdPartyId:{0}] Lock obtained for RepriceExport with id: {1}, sending to {2}", thirdPartyId.toString(), id_msg, export.getThirdPartyEntity().getName()));
				}
			
				try {
					final RepriceTask task = factory.createTask(export);
					if(task != null) {
						outQueue.put(task.call());
					}
				} catch (Exception e) {
					log.error("Exception caught trying to create the RepriceTask: " + e.getMessage());
				}
			} else {
				if(log.isInfoEnabled()) {
					log.info(MessageFormat.format("[MessageHandler-ThirdPartyId:{0}] Could not obtain lock for RepriceExport with id: {1}, moving on.", thirdPartyId.toString(), id_msg));
				}
			}
		}
	}

	/**
	 * Reads RepriceExport rows from the database.  A classic Producer in Producer-Consumer problem.
	 * @author bfung
	 *
	 */
	private class DatabaseQueueReader implements Runnable {
		
		//null is the initial value, not zero!
		private Integer minRepriceExportId = null;
		
		public void run() {
			while(!shuttingDown) {
				try {
					produce();
				} catch (Exception e) {
					log.warn(MessageFormat.format("[QueueReader-ThirdPartyId:{0}] Caught Exception: {1}", thirdPartyId.toString(), e.getMessage()));
				}
				
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e) {
					if(shuttingDown) {
						break;
					} else {
						continue;
					}
				}
			}
		}
		
		private void produce() {
			List<RepriceExport> exports = repriceExportRetriever.getRepriceExports(null, thirdPartyId, failureCount, minRepriceExportId);
			if(log.isInfoEnabled()) {
				log.info(MessageFormat.format("[QueueReader-ThirdPartyId:{0}] Found {1} events for processing.", thirdPartyId.toString(), exports.size()));
			}

			Integer maxId = minRepriceExportId == null ? Integer.MIN_VALUE : minRepriceExportId;
			for(RepriceExport export : exports) {				
				if(maxId.compareTo(export.getId()) <= 0) {
					minRepriceExportId = export.getId();
					maxId = minRepriceExportId;
				}
				
				try {
					inQueue.put(export);
				} catch (InterruptedException e) {
					if(shuttingDown) {
						break;
					} else {
						continue;
					}
				}
			}
		}
	}
}
