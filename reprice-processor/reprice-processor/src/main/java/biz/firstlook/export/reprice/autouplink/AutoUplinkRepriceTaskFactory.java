package biz.firstlook.export.reprice.autouplink;

import biz.firstlook.export.reprice.AbstractRepriceTaskFactory;
import biz.firstlook.export.reprice.RepriceExport;
import biz.firstlook.export.reprice.RepriceTask;
import biz.firstlook.export.reprice.RepriceTaskFactory;
import biz.firstlook.export.reprice.Credentials;


class AutoUplinkRepriceTaskFactory extends 
		AbstractRepriceTaskFactory implements RepriceTaskFactory
{	
	private Credentials credentials = null;
	private String endpointUrl = null;
	private Integer thirdPartyId;	
	private AutoUpLinkVehicleInventoryCredentials autoUpLinkVehicleInventoryCredentials = null;	
	
	public synchronized RepriceTask createTask(RepriceExport event) 
	{
		if (credentials == null)
		{			
			credentials = autoUpLinkVehicleInventoryCredentials.getCredentials();
		}		
		
		String dealerID = autoUpLinkVehicleInventoryCredentials.getDealerID(event);
		boolean isInternetPriceUpdateRequired = autoUpLinkVehicleInventoryCredentials.isInternetPriceUpdateRequired(event);
		
		//instantiate the task		
		AutoUplinkRepriceTask autoUplinkRepriceTask = 
			new AutoUplinkRepriceTask(event,credentials,dealerID,endpointUrl, isInternetPriceUpdateRequired);		

		return autoUplinkRepriceTask;
	}
	
	/**
	 * Configuration set by spring config file (applicationContext-plugins)
	 * @param endpointUrl
	 */
	
	public void setAutoUpLinkVehicleInventoryCredentials( AutoUpLinkVehicleInventoryCredentials autoUpLinkVehicleInventoryCredentials )
	{
		this.autoUpLinkVehicleInventoryCredentials = autoUpLinkVehicleInventoryCredentials;
	}
	
	public Integer getThirdPartyId() 
	{
		return this.thirdPartyId;
	}

	public void setThirdPartyId(Integer thirdPartyId) 
	{
		this.thirdPartyId = thirdPartyId;
	}
	
	public void setEndpointUrl(String endpointUrl) 
	{
		this.endpointUrl = endpointUrl;
	}
	
	public String getEndpointUrl() 
	{
		return this.endpointUrl;
	}



}