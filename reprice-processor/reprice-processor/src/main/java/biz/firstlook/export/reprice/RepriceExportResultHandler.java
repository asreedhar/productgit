package biz.firstlook.export.reprice;

import java.text.MessageFormat;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

class RepriceExportResultHandler implements Runnable, LifeCycleEventListener {
	
	private final Logger log = Logger.getLogger(RepriceExportMessageHandler.class);
	
	private final RepriceExportResultPersistor persistor;
	private final BlockingQueue<RepriceExportResult> queue;
	
	private boolean shuttingDown = false;
	
	public RepriceExportResultHandler(
			final RepriceExportResultPersistor persistor,
			final BlockingQueue<RepriceExportResult> queue) {
		this.persistor = persistor;
		this.queue = queue;
	}
	
	public void run() {
		while(true) {
			RepriceExportResult result = null;
			try {
				result = queue.poll(2L, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				//nothing in queue, check if we need to shutdown or not
				synchronized (this) {
					if(shuttingDown) {
						break;
					} else {
						continue;
					}
				}
			}
			
			if(result != null) {
				try {
					if(log.isInfoEnabled()) {
						log.info(MessageFormat.format("[ResultWriter] Saving results for RepriceExport with id: {0}", result.getId()));
					}
					persistor.save(result);
				} catch (Exception e) {
					//error saving...
					e.printStackTrace();
				}
			}
		}
	}

	public synchronized void shutdown() {
		shuttingDown = true;
	}
}
