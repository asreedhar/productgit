package biz.firstlook.export.reprice;

interface LifeCycleEventListener {
	
	/**
	 * This method should be synchronized by implementing classes for thread safety.
	 */
	void shutdown();
}
