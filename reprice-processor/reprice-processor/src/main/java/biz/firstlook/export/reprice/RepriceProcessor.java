
package biz.firstlook.export.reprice;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * Handles Reprices to EBizAutos, AULTec and DMS Writeback to SIS-ADP, SIS-Reynolds, and Reynold direct.
 * <a href="https://incisent.fogbugz.com/default.asp?W891">Fogbuz Wiki Documentation</a>
 * @author bfung
 */
public class RepriceProcessor implements Runnable, LifeCycleEventListener, ApplicationContextAware 
{
	private static final String USAGE = "Usage: RepriceProcessor [test]";
	
	private final BlockingQueue<RepriceExportResult> resultQueue = new LinkedBlockingQueue<RepriceExportResult>();
	
	private final List<RepriceExportMessageHandler> messageHandlers = new ArrayList<RepriceExportMessageHandler>();
	
	private RepriceExportResultHandler resultHandler;
	
	private final ShutdownEventDispatcher shutdownEventDispatcher = new ShutdownEventDispatcher();
	private ApplicationContext appContext;
		
	private void init()
	{
		RepriceTaskFactoryLocator locator = (RepriceTaskFactoryLocator)appContext.getBean("repriceTaskFactoryLocator");
		Collection<RepriceTaskFactory> factories = locator.getFactories();
		
		for(RepriceTaskFactory factory : factories) {
			messageHandlers.add(new RepriceExportMessageHandler(
					Integer.valueOf(5), //failureCount
					(RepriceExportRetriever)appContext.getBean("repriceExportRetriever"), 
					(RepriceExportLockRetriever)appContext.getBean("repriceExportLockRetriever"),
					factory,
					resultQueue));
		}
				
		resultHandler = new RepriceExportResultHandler(
				(RepriceExportResultPersistor)appContext.getBean("repriceExportResultPersistor"),
				resultQueue);
		
		shutdownEventDispatcher.addListener(this);
		for(RepriceExportMessageHandler c : messageHandlers) {
			shutdownEventDispatcher.addListener(c);
		}
		shutdownEventDispatcher.addListener(resultHandler);
		
		Runtime.getRuntime().addShutdownHook(new Thread(shutdownEventDispatcher, "ShutdownEventDispatcher-Thread"));	
	}
	
	public void run() 
	{
		Thread resultHandlerThread = new Thread(resultHandler, "ResultHandler");
		resultHandlerThread.start();
		
		for(RepriceExportMessageHandler messageHandler : messageHandlers) {
			Thread messageHandlerThread = new Thread(messageHandler, "MessageHandler-ThirdPartyId:" + messageHandler.getThirdPartyId());
			messageHandlerThread.start();
		}
		
		try {
			resultHandlerThread.join();
		} catch (InterruptedException e) {
			//do nothing and exit.
		}
		System.out.println("Main RepriceProcessor thread Exit.");
	}
	
	public synchronized void shutdown() 
	{
		System.out.println("Attempting to shutdown cleanly...");
		for(RepriceExportMessageHandler messageHandler : messageHandlers) {
			messageHandler.shutdown();
		}
		resultHandler.shutdown();
	}
	
	public static void main( String[] args ) 
	{
		boolean testMode = false;

		if(args.length > 1) {
			System.out.println(USAGE);
			System.exit(10);
		} else if ( args.length == 1  ) {
			if( args[0].equalsIgnoreCase( "test" ) ) {
				testMode = true;			
			} else {
				System.out.println(USAGE);
				System.exit(10);
			}
		}		
		ClassPathXmlApplicationContext applicationContext = null;
		if ( testMode )	{
			applicationContext = new ClassPathXmlApplicationContext( new String[] { 
					"applicationContext.xml",
//					"applicationContext-dao.xml",
//					"applicationContext-dataSource.xml",
					"test-applicationContext-dao.xml",
					"test-applicationContext-dataSource.xml", 
					"test-applicationContext-plugins.xml" } );
		} else {
			//load the defaults!
			applicationContext = new ClassPathXmlApplicationContext( new String[] { 
					"applicationContext.xml", 
					"applicationContext-dao.xml",
					"applicationContext-dataSource.xml", 
					"applicationContext-plugins.xml" } );
		}
		
		final RepriceProcessor processor =(RepriceProcessor)applicationContext.getBean("RepriceProcessor");
		processor.init();
		processor.run();
	}

	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException 
	{		
		appContext = applicationContext;
	}	
	public void setTrustStoreLocation(String storeLocation) 
	{		
		System.setProperty("javax.net.ssl.trustStore",storeLocation);		
	}
	public void setTrustStorePassword(String trustPassword) 
	{		
		System.setProperty("javax.net.ssl.trustStorePassword",trustPassword);
	}
}
