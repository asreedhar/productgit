
package com.ebizautos.api2;

import javax.jws.WebService;

@WebService(serviceName = "XMLListener", targetNamespace = "http://www.ebizautos.com/", endpointInterface = "com.ebizautos.api2.XMLListenerSoap")
public class XMLListenerImpl
    implements XMLListenerSoap
{


    public String sendVehicle(String id, String auth, String xmlRequest) {
        throw new UnsupportedOperationException();
    }

}
