
package com.ebizautos.api2;

import java.net.MalformedURLException;
import java.util.Collection;
import java.util.HashMap;
import javax.xml.namespace.QName;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.annotations.AnnotationServiceFactory;
import org.codehaus.xfire.annotations.jsr181.Jsr181WebAnnotations;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.jaxb2.JaxbTypeRegistry;
import org.codehaus.xfire.service.Endpoint;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.soap.AbstractSoapBinding;
import org.codehaus.xfire.transport.TransportManager;

public class XMLListenerClient {

	private static Logger log = Logger.getLogger(com.ebizautos.api2.XMLListenerClient.class);
	private static final String DEFAULT_ENDPOINT_URL = "http://api.ebizautos.com/xmllistener.asmx";
	
    private static XFireProxyFactory proxyFactory = new XFireProxyFactory();
    private HashMap endpoints = new HashMap();
    private Service service0;

    public XMLListenerClient(final String endpointUrl) {
        create0();
        Endpoint XMLListenerSoapLocalEndpointEP = service0 .addEndpoint(new QName("http://www.ebizautos.com/", "XMLListenerSoapLocalEndpoint"), new QName("http://www.ebizautos.com/", "XMLListenerSoapLocalBinding"), "xfire.local://XMLListener");
        endpoints.put(new QName("http://www.ebizautos.com/", "XMLListenerSoapLocalEndpoint"), XMLListenerSoapLocalEndpointEP);

        String url = endpointUrl;
        if(StringUtils.isBlank(endpointUrl)) {
        	log.warn("Invalid enpointUrl to ebiz autos.  Using default url:" + DEFAULT_ENDPOINT_URL);
        	url = DEFAULT_ENDPOINT_URL;
        }
        
        Endpoint XMLListenerSoapEP = service0 .addEndpoint(new QName("http://www.ebizautos.com/", "XMLListenerSoap"), new QName("http://www.ebizautos.com/", "XMLListenerSoap"), url);
        endpoints.put(new QName("http://www.ebizautos.com/", "XMLListenerSoap"), XMLListenerSoapEP);
    }

    public Object getEndpoint(Endpoint endpoint) {
        try {
            return proxyFactory.create((endpoint).getBinding(), (endpoint).getUrl());
        } catch (MalformedURLException e) {
            throw new XFireRuntimeException("Invalid URL", e);
        }
    }

    public Object getEndpoint(QName name) {
        Endpoint endpoint = ((Endpoint) endpoints.get((name)));
        if ((endpoint) == null) {
            throw new IllegalStateException("No such endpoint!");
        }
        return getEndpoint((endpoint));
    }

    public Collection getEndpoints() {
        return endpoints.values();
    }

    private void create0() {
        TransportManager tm = (org.codehaus.xfire.XFireFactory.newInstance().getXFire().getTransportManager());
        HashMap props = new HashMap();
        props.put("annotations.allow.interface", true);
        AnnotationServiceFactory asf = new AnnotationServiceFactory(new Jsr181WebAnnotations(), tm, new AegisBindingProvider(new JaxbTypeRegistry()));
        asf.setBindingCreationEnabled(false);
        service0 = asf.create((com.ebizautos.api2.XMLListenerSoap.class), props);
        {
            AbstractSoapBinding soapBinding = asf.createSoap11Binding(service0, new QName("http://www.ebizautos.com/", "XMLListenerSoapLocalBinding"), "urn:xfire:transport:local");
        }
        {
            AbstractSoapBinding soapBinding = asf.createSoap11Binding(service0, new QName("http://www.ebizautos.com/", "XMLListenerSoap"), "http://schemas.xmlsoap.org/soap/http");
        }
    }

    public XMLListenerSoap getXMLListenerSoapLocalEndpoint() {
        return ((XMLListenerSoap)(this).getEndpoint(new QName("http://www.ebizautos.com/", "XMLListenerSoapLocalEndpoint")));
    }

    public XMLListenerSoap getXMLListenerSoapLocalEndpoint(String url) {
        XMLListenerSoap var = getXMLListenerSoapLocalEndpoint();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

    public XMLListenerSoap getXMLListenerSoap() {
        return ((XMLListenerSoap)(this).getEndpoint(new QName("http://www.ebizautos.com/", "XMLListenerSoap")));
    }

    public XMLListenerSoap getXMLListenerSoap(String url) {
        XMLListenerSoap var = getXMLListenerSoap();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

}
