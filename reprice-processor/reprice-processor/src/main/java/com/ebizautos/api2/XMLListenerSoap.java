
package com.ebizautos.api2;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(name = "XMLListenerSoap", targetNamespace = "http://www.ebizautos.com/")
@SOAPBinding(use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public interface XMLListenerSoap {


    @WebMethod(operationName = "SendVehicle", action = "http://www.ebizautos.com/SendVehicle")
    @WebResult(name = "SendVehicleResult", targetNamespace = "http://www.ebizautos.com/")
    public String sendVehicle(
        @WebParam(name = "id", targetNamespace = "http://www.ebizautos.com/")
        String id,
        @WebParam(name = "auth", targetNamespace = "http://www.ebizautos.com/")
        String auth,
        @WebParam(name = "xmlRequest", targetNamespace = "http://www.ebizautos.com/")
        String xmlRequest);

}
