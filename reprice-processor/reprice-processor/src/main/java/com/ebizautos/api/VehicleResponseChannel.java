package com.ebizautos.api;

public class VehicleResponseChannel {
	
	private VehicleResponseItem item;
	private String msg;

	public VehicleResponseItem getItem() {
		return item;
	}

	public void setItem(VehicleResponseItem item) {
		this.item = item;
	}
	
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String toString(){
		return " channel: " + item.toString();
	}
}
