package com.ebizautos.api;


public class VehicleResponseItem {

	private String vin;
	private String status;
	private String timestamp;
	private int index;

	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getVin() {
		return vin;
	}
	public void setVin(String vin) {
		this.vin = vin;
	}
	
	public String toString(){
		return " vin: " + vin + " status: " + status + " timestamp: " + timestamp + " index: " + index;
	}
	
}
