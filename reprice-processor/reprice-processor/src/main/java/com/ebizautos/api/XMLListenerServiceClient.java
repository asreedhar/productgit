
package com.ebizautos.api;

import java.net.MalformedURLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.annotations.AnnotationServiceFactory;
import org.codehaus.xfire.annotations.jsr181.Jsr181WebAnnotations;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.jaxb2.JaxbTypeRegistry;
import org.codehaus.xfire.service.Endpoint;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.transport.TransportManager;

public class XMLListenerServiceClient {

	private static Logger log = Logger.getLogger(com.ebizautos.api.XMLListenerServiceClient.class);
	private static final String DEFAULT_ENDPOINT_URL = "http://api2.ebizautos.com/xml/xmllistener.cfc";
	
	private static XFireProxyFactory proxyFactory = new XFireProxyFactory();
    private Map<QName, Endpoint> endpoints = new HashMap<QName, Endpoint>();
    private Service service0;
    
    public XMLListenerServiceClient() {
    	this(DEFAULT_ENDPOINT_URL);
    }
    
    public XMLListenerServiceClient(final String endpointUrl) {
        create();
        String url = endpointUrl;
        if(StringUtils.isBlank(endpointUrl)) {
        	log.warn("Invalid enpointUrl to ebiz autos.  Using default url:" + DEFAULT_ENDPOINT_URL);
        	url = DEFAULT_ENDPOINT_URL;
        }
        Endpoint XmlListenerApi2EndPoint = service0 .addEndpoint(new QName("http://api.eBizAutos.com", "XMLListener"), new QName("http://api.eBizAutos.com", "XMLListener.cfcSoapBinding"), url);
        endpoints.put(new QName("http://api.eBizAutos.com", "XMLListener.cfc"), XmlListenerApi2EndPoint);
    }

    public Object getEndpoint(Endpoint endpoint) {
        try {
            return proxyFactory.create((endpoint).getBinding(), (endpoint).getUrl());
        } catch (MalformedURLException e) {
            throw new XFireRuntimeException("Invalid URL", e);
        }
    }

    public Object getEndpoint(QName name) {
        Endpoint endpoint = ((Endpoint) endpoints.get((name)));
        if ((endpoint) == null) {
            throw new IllegalStateException("No such endpoint!");
        }
        return getEndpoint((endpoint));
    }

    public Collection<Endpoint> getEndpoints() {
        return endpoints.values();
    }

    private void create() {
        TransportManager tm = (org.codehaus.xfire.XFireFactory.newInstance().getXFire().getTransportManager());
        Map<String, Object> props = new HashMap<String, Object>();
        props.put("annotations.allow.interface", true);
        AnnotationServiceFactory asf = new AnnotationServiceFactory(new Jsr181WebAnnotations(), tm, new AegisBindingProvider(new JaxbTypeRegistry()));
        asf.setBindingCreationEnabled(false);
        service0 = asf.create((com.ebizautos.api.XMLListener.class), props);
        asf.createSoap11Binding(service0, new QName("http://api.eBizAutos.com", "XMLListener.cfcSoapBinding"), "http://schemas.xmlsoap.org/soap/http");
    }

    public XMLListener getXMLListener() {
        return ((XMLListener)(this).getEndpoint(new QName("http://api.eBizAutos.com", "XMLListener.cfc")));
    }
}
