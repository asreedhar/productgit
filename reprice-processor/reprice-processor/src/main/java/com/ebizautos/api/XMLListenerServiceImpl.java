
package com.ebizautos.api;

import javax.jws.WebService;

/**
 * The XMLListenerServiceClient class depends on the annotations in this class, therefore this class cannot be deleted.
 */
@WebService(serviceName = "XMLListenerService", targetNamespace = "http://api.eBizAutos.com", endpointInterface = "com.ebizautos.api.XMLListener")
public class XMLListenerServiceImpl
    implements XMLListener
{


    public Object sendVehicle(double ID, String Auth, String xmlRequest)
        throws CFCInvocationException
    {
        throw new UnsupportedOperationException();
    }

}
