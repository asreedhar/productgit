package com.ebizautos.api;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
	"rss",
    "channel",
    "item",
    "vin",
    "status",
    "timestamp",
    "index"
})
@XmlRootElement(name = "sendVehicleReturn")
public class SendVehicleReturn {
	
	@XmlElement(required = true)
	protected String rss;
	@XmlElement(required = true)
    protected String channel;
	@XmlElement(required = true)
    protected String item;
	@XmlElement(required = true, name = "g:vin")
    protected String vin;
	@XmlElement(required = true)
    protected String status;
	@XmlElement(required = true)
    protected String timestamp;
	@XmlElement(required = true)
    protected String index;
    
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getIndex() {
		return index;
	}
	public void setIndex(String index) {
		this.index = index;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getVin() {
		return vin;
	}
	public void setVin(String vin) {
		this.vin = vin;
	}
	public String getRss() {
		return rss;
	}
	public void setRss(String rss) {
		this.rss = rss;
	}
	
	
}



