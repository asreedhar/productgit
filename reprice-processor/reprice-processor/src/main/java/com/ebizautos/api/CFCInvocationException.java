package com.ebizautos.api;

import javax.xml.namespace.QName;

import org.codehaus.xfire.fault.FaultInfoException;

public class CFCInvocationException extends FaultInfoException
{

private static final long serialVersionUID = 5502269899881103771L;

private coldfusion.xml.rpc.CFCInvocationException faultInfo;

public CFCInvocationException( String message, coldfusion.xml.rpc.CFCInvocationException faultInfo )
{
	super( message );
	this.faultInfo = faultInfo;
}

public CFCInvocationException( String message, Throwable t, coldfusion.xml.rpc.CFCInvocationException faultInfo )
{
	super( message, t );
	this.faultInfo = faultInfo;
}

public coldfusion.xml.rpc.CFCInvocationException getFaultInfo()
{
	return faultInfo;
}

public static QName getFaultName()
{
	return new QName( "http://api.eBizAutos.com", "fault" );
}

}
