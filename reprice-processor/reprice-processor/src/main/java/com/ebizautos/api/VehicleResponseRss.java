package com.ebizautos.api;

public class VehicleResponseRss {

	VehicleResponseChannel channel;

	public VehicleResponseChannel getChannel() {
		return channel;
	}

	public void setChannel(VehicleResponseChannel channel) {
		this.channel = channel;
	}
	
	public String toString(){
		return " rss: " + channel.toString();
	}
	
}
