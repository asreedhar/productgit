
package com.ebizautos.api;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(name = "XMLListener", targetNamespace = "http://api.eBizAutos.com")
@SOAPBinding(style = SOAPBinding.Style.RPC, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface XMLListener {


    @WebMethod(operationName = "SendVehicle", action = "")
    @WebResult(name = "SendVehicleReturn", targetNamespace = "http://api.eBizAutos.com")
    public Object sendVehicle(
        @WebParam(name = "ID", targetNamespace = "http://api.eBizAutos.com")
        double ID,
        @WebParam(name = "Auth", targetNamespace = "http://api.eBizAutos.com")
        String Auth,
        @WebParam(name = "xmlRequest", targetNamespace = "http://api.eBizAutos.com")
        String xmlRequest)
        throws CFCInvocationException
    ;

}
