#!/bin/sh

PIDFILE=reprice.pid

if [ -e $PIDFILE ]; then
	if [ kill -0 `cat ${PIDFILE}` ]; then
		echo "Process is currently running, kill it first!"
		exit
	else
		rm -f ${PIDFILE}	
	fi
fi

##
# build classpath
##
set classpath=
for i in `ls ./lib/*.jar`
do
  classpath=${classpath}:${i}
done

##
# run program, try to write out the pid to pidfile.
##
java -cp "conf/:${classpath}" biz.firstlook.export.reprice.RepriceProcessor & echo $! > ${PIDFILE} 
