package biz.firstlook.syndication.feed.module.ebizautos;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import biz.firstlook.syndication.feed.module.ebizautos.types.EbizFloatUnit;
import biz.firstlook.syndication.feed.module.ebizautos.types.EbizModule;
import biz.firstlook.syndication.feed.module.ebizautos.types.EbizModuleImpl;
import biz.firstlook.syndication.feed.module.ebizautos.types.RequestType;
import biz.firstlook.syndication.feed.synd.FeedType;

import com.sun.syndication.feed.module.Module;
import com.sun.syndication.feed.module.base.GoogleBaseImpl;
import com.sun.syndication.feed.module.base.Vehicle;
import com.sun.syndication.feed.module.base.types.YearType;
import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.synd.SyndContentImpl;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.synd.SyndFeedImpl;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedOutput;

public class AppTest
{

/**
 * @param args
 */
public static void main( String[] args )
{

	SyndFeed feed = new SyndFeedImpl();
	feed.setFeedType( FeedType.RSS_2_0.getFeedType() );

	feed.setTitle( "" );
	feed.setDescription( "" );
	feed.setLink( "" );

	EbizModule requestTypeMod = new EbizModuleImpl();
	requestTypeMod.setRequest_type( RequestType.update.toString() );

	List< Module > feedMods = new ArrayList< Module >();
	feedMods.add( requestTypeMod );

	feed.setModules( feedMods );

	SyndEntry entry = new SyndEntryImpl();
	entry.setTitle( "" );
	entry.setPublishedDate( new Date() );

	SyndContent content = new SyndContentImpl();
	content.setType( "text/plain" );
	content.setValue( "" );

	Vehicle vehicle = new GoogleBaseImpl();

	vehicle.setMake( "honda" );
	vehicle.setModel( "accord" );
	vehicle.setVin( "1G8ZH5497NZ197624" );
	vehicle.setMileage( 762 );
	vehicle.setYear( new YearType( "1998" ) );
    vehicle.setCondition("Used");
    
	
	vehicle.setPrice( new EbizFloatUnit( 1500.32f, "" ) );

	// Float price = new Float(1500);
	// System.out.println(price.toString());

	EbizModule ebizInfoMod = new EbizModuleImpl();
	ebizInfoMod.setClient_id( 6114 );
	ebizInfoMod.setStock_no("xxyz");
	// ebizInfoMod.setOther_price(price.toString());

	List<Object> entryMods = new ArrayList<Object>();
	entryMods.add( vehicle );
	entryMods.add( ebizInfoMod );

	entry.setModules( entryMods );
	entry.setDescription( content );
	entry.setUri( "21DEF2FF-76F9-4395-A4A2-B7AB184422DF" );

	List< SyndEntry > entries = new ArrayList< SyndEntry >();
	entries.add( entry );
	feed.setEntries( entries );

	StringWriter writer = new StringWriter();
	SyndFeedOutput output = new SyndFeedOutput();
	try
	{
		output.output( feed, writer );
		writer.close();
	}
	catch ( IOException e )
	{
		e.printStackTrace();
	}
	catch ( FeedException e )
	{
		e.printStackTrace();
	}

	Pattern rssPattern = Pattern.compile( "<rss.*>" );
	Matcher rssMatcher = rssPattern.matcher( writer.toString() );
	String withNewRssHeader = rssMatcher.replaceAll( "<rss version=\"2.0\" xmlns:g=\"http://base.google.com/ns/1.0\" xmlns:e=\"http://XML.eBizAutos.com\">" );

	Pattern nameSpacePattern = Pattern.compile( "g-core" );

	Matcher nameSpaceMatcher = nameSpacePattern.matcher( withNewRssHeader );
	String gNameSpace = nameSpaceMatcher.replaceAll( "g" );

	System.out.println( gNameSpace );
}

}
