package biz.firstlook.export.reprice.eBizAutos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.apache.commons.lang.StringUtils;

import biz.firstlook.export.reprice.InventoryItem;

public class TestEbizWebServicePayload extends TestCase
{

EBizRequestGenerator eBizRG;
List< String > payloadAsString = new ArrayList< String >();
InventoryItem vehicleInfo;

public TestEbizWebServicePayload( String arg0 )
{
	super( arg0 );
	eBizRG = new EBizRequestGenerator();

}

protected void setUp() throws Exception
{
	super.setUp();
	payloadAsString.add( "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" );
	payloadAsString.add( "<rss version=\"2.0\" xmlns:e=\"http://XML.eBizAutos.com\" xmlns:g=\"http://base.google.com/ns/1.0\">" );
	payloadAsString.add( "<channel>" );
	payloadAsString.add( "<title />" );
	payloadAsString.add( "<link />" );
	payloadAsString.add( "<description />" );
	payloadAsString.add( "<e:request_type>updateprice</e:request_type>" );
	payloadAsString.add( "<item>" );
	payloadAsString.add( "<title />" );
	payloadAsString.add( "<description />" );
	payloadAsString.add( "<guid isPermaLink=\"false\">21DEF2FF-76F9-4395-A4A2-B7AB184422DF</guid>" );
	payloadAsString.add( "<g:condition>Used</g:condition>" );
	payloadAsString.add( "<g:make>Saturn</g:make>" );
	payloadAsString.add( "<g:mileage>76200</g:mileage>" );
	payloadAsString.add( "<g:model>SL</g:model>" );
	payloadAsString.add( "<g:price>1500</g:price>" );
	payloadAsString.add( "<g:vin>1G8ZH5497NZ197624</g:vin>" );
	payloadAsString.add( "<g:year>1992</g:year>" );
	payloadAsString.add( "<e:client_id>6114</e:client_id>" );
	payloadAsString.add( "<e:stock_no>xxyz</e:stock_no>" );
	payloadAsString.add( "</item>" );
	payloadAsString.add( "</channel>" );
	payloadAsString.add( "</rss>" );
	payloadAsString.add( "" );

	vehicleInfo = new InventoryItem( "1G8ZH5497NZ197624", 1992, "Saturn", "SL", "xxyz", 2 , 76200);
}

/**
 * Tests rss generation line by line to avoid whitespace comparison errors.
 */
public void testGeneratedPayload()
{
	String resultingPayload = eBizRG.generate( 6114, "21DEF2FF-76F9-4395-A4A2-B7AB184422DF", vehicleInfo, new Float( 1500 ) );

	BufferedReader reader = new BufferedReader( new StringReader( resultingPayload ) );

	try
	{
		int i = 0;
		String line = null;
		while ( ( line = reader.readLine() ) != null )
		{
			line = StringUtils.strip( line );
			String expected = payloadAsString.get( i );
			assertEquals( expected, line );
			i++;
		}
	}
	catch ( IOException e )
	{
		fail( e.getMessage() );
	}
	finally
	{
		try
		{
			reader.close();
		}
		catch ( IOException e )
		{
			e.printStackTrace();
		}
	}
}

protected void tearDown() throws Exception
{
	super.tearDown();
}

}
