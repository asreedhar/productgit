package biz.firstlook.export.reprice;

import biz.firstlook.export.reprice.AbstractRepriceTaskFactory;
import biz.firstlook.export.reprice.RepriceExport;
import biz.firstlook.export.reprice.RepriceTask;
import biz.firstlook.export.reprice.RepriceTaskFactory;

public class MockRepriceTaskFactory extends AbstractRepriceTaskFactory implements RepriceTaskFactory
{

	private Integer thirdPartyId = Integer.valueOf(-1);
	
	public RepriceTask createTask( RepriceExport event ) {
		return new MockRepriceTask( event );
	}

	public Integer getThirdPartyId() {
		return thirdPartyId;
	}
	
	public void setThirdPartyId(Integer thirdPartyId) {
		this.thirdPartyId = thirdPartyId;
	}
}
