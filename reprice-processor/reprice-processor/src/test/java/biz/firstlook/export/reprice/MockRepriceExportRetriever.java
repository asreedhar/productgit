package biz.firstlook.export.reprice;

import java.util.ArrayList;
import java.util.List;

import biz.firstlook.export.reprice.RepriceExport;
import biz.firstlook.export.reprice.RepriceExportRetriever;


public class MockRepriceExportRetriever implements RepriceExportRetriever {

	private List<RepriceExport> repriceExports = new ArrayList<RepriceExport>();
	
	public List<RepriceExport> getRepriceExports(Integer dealerId,
			Integer thirdPartyEntityId, Integer failureAttempts,
			Integer minRepriceExportId) {
		return repriceExports;
	}

	public boolean insert(RepriceExport export) {
		return repriceExports.add(export);
	}
	
	public void deleteAll() {
		repriceExports.clear();
	}
}
