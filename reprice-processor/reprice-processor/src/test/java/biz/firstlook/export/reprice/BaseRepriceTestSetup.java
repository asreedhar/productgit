package biz.firstlook.export.reprice;

import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

/**
 * This class setups the in memory database for all the reprice tests.  This class is NOT thread safe!.
 * @author bfung
 *
 */
public abstract class BaseRepriceTestSetup extends AbstractTransactionalDataSourceSpringContextTests
{

public static boolean databaseSetup = false;

/**
 * This sets up our base data. The tests method all run in transaction, so we do not need to reset the data each time.
 */
@Override
protected void onSetUpBeforeTransaction() throws Exception
{
	super.onSetUpBeforeTransaction();
	if ( !databaseSetup )
	{
		jdbcTemplate.execute( "CREATE TABLE Vehicle ( VehicleId int PRIMARY KEY, Vin varchar(17), VehicleYear int,Make varchar(20), Model varchar(101) );" );
		jdbcTemplate.execute( "INSERT INTO Vehicle ( VehicleId, Vin, VehicleYear, Make, Model ) VALUES (0 ,'1HGFA16816L014847', 2006, 'HONDA', 'CIVIC');" );
		jdbcTemplate.execute( "INSERT INTO Vehicle ( VehicleId, Vin, VehicleYear, Make, Model ) VALUES (1, '4T1BE32K74U320954', 2004, 'TOYOTA', 'CAMARY');" );
		jdbcTemplate.execute( "INSERT INTO Vehicle ( VehicleId, Vin, VehicleYear, Make, Model ) VALUES (2, '1FTPW14585KE37877', 2005, 'FORD', 'F150');" );

		jdbcTemplate.execute( "CREATE TABLE InternetAdvertiser_ThirdPartyEntity ( InternetAdvertiserID int PRIMARY KEY, DatafeedCode varchar(20) );" );
		jdbcTemplate.execute( "INSERT INTO InternetAdvertiser_ThirdPartyEntity ( InternetAdvertiserID, DatafeedCode ) VALUES(1, 'MockAdvertiser1' );" );
		jdbcTemplate.execute( "INSERT INTO InternetAdvertiser_ThirdPartyEntity ( InternetAdvertiserID, DatafeedCode ) VALUES(2, 'MockAdvertiser2' );" );
		jdbcTemplate.execute( "INSERT INTO InternetAdvertiser_ThirdPartyEntity ( InternetAdvertiserID, DatafeedCode ) VALUES(3, 'MockAdvertiser3' );" );

		StringBuffer createInventory = new StringBuffer();
		createInventory.append( "CREATE TABLE Inventory ( InventoryId int PRIMARY KEY, VehicleId int, BusinessUnitId int, StockNumber varchar(15), InventoryType int, MileageReceived int, " );
		createInventory.append( "CONSTRAINT vehicleId_ref FOREIGN KEY ( VehicleId ) REFERENCES Vehicle (VehicleId) );" );
		jdbcTemplate.execute( createInventory.toString() );
		jdbcTemplate.execute( "INSERT INTO Inventory ( InventoryId, VehicleId, BusinessUnitId, StockNumber, InventoryType, MileageReceived) VALUES (10, 2, 300, 'A123', 2, 20000);" );
		jdbcTemplate.execute( "INSERT INTO Inventory ( InventoryId, VehicleId, BusinessUnitId, StockNumber, InventoryType, MileageReceived) VALUES (20, 0, 300, 'B456', 2, 30000);" );
		jdbcTemplate.execute( "INSERT INTO Inventory ( InventoryId, VehicleId, BusinessUnitId, StockNumber, InventoryType, MileageReceived) VALUES (30, 1, 200, '39G2d3s', 2, 45000);" );

		StringBuffer createRepriceEvent = new StringBuffer();
		createRepriceEvent.append( "CREATE TABLE RepriceEvent ( RepriceEventId int PRIMARY KEY, InventoryId int, NewPrice decimal(8,2), Date date, " );
		createRepriceEvent.append( "CONSTRAINT inventoryId_ref FOREIGN KEY ( InventoryId ) REFERENCES Inventory (InventoryId) );" );
		jdbcTemplate.execute( createRepriceEvent.toString() );
		jdbcTemplate.execute( "INSERT INTO RepriceEvent ( RepriceEventId, InventoryId, NewPrice, Date) VALUES (1, 10, 300, '2007-02-28 17:43:00');" );
		jdbcTemplate.execute( "INSERT INTO RepriceEvent ( RepriceEventId, InventoryId, NewPrice, Date) VALUES (2, 10, 301, '2007-02-28 18:43:00');" );
		jdbcTemplate.execute( "INSERT INTO RepriceEvent ( RepriceEventId, InventoryId, NewPrice, Date) VALUES (3, 10, 302, '2007-02-28 19:43:00');" );
		jdbcTemplate.execute( "INSERT INTO RepriceEvent ( RepriceEventId, InventoryId, NewPrice, Date) VALUES (4, 10, 303, '2007-02-28 20:43:00');" );
		jdbcTemplate.execute( "INSERT INTO RepriceEvent ( RepriceEventId, InventoryId, NewPrice, Date) VALUES (5, 20, 450, '2007-02-28 17:43:00');" );
		jdbcTemplate.execute( "INSERT INTO RepriceEvent ( RepriceEventId, InventoryId, NewPrice, Date) VALUES (6, 20, 400, '2007-02-28 17:44:00');" );
		jdbcTemplate.execute( "INSERT INTO RepriceEvent ( RepriceEventId, InventoryId, NewPrice, Date) VALUES (7, 30, 500, '2007-02-28 17:45:00');" );
		jdbcTemplate.execute( "INSERT INTO RepriceEvent ( RepriceEventId, InventoryId, NewPrice, Date) VALUES (8, 20, 451, '2007-02-28 17:43:00');" );

		StringBuffer createRepriceExport = new StringBuffer();
		createRepriceExport.append( "CREATE TABLE RepriceExport ( RepriceExportId int PRIMARY KEY, RepriceEventId int, RepriceExportStatusId int" );
		createRepriceExport.append( ", InternetAdvertiserId int, FailureReason varchar(50), FailureCount int, DateSent date, " );
		createRepriceExport.append( "CONSTRAINT repriceEventId_ref FOREIGN KEY (RepriceEventId) REFERENCES RepriceEvent(RepriceEventId), " );
		createRepriceExport.append( "CONSTRAINT internetAdvertiserId_ref FOREIGN KEY (InternetAdvertiserId) REFERENCES InternetAdvertiser_ThirdPartyEntity(InternetAdvertiserId) );" );
		jdbcTemplate.execute( createRepriceExport.toString() );
		jdbcTemplate.execute( "INSERT INTO RepriceExport ( RepriceExportId, RepriceEventId, RepriceExportStatusId, InternetAdvertiserId, FailureCount) VALUES (1, 1, 4, 1, 3);" );
		jdbcTemplate.execute( "INSERT INTO RepriceExport ( RepriceExportId, RepriceEventId, RepriceExportStatusId, InternetAdvertiserId, FailureCount) VALUES (2, 3, 4, 1, 6);" );
		jdbcTemplate.execute( "INSERT INTO RepriceExport ( RepriceExportId, RepriceEventId, RepriceExportStatusId, InternetAdvertiserId) VALUES (3, 4, 1, 1);" );
		jdbcTemplate.execute( "INSERT INTO RepriceExport ( RepriceExportId, RepriceEventId, RepriceExportStatusId, InternetAdvertiserId) VALUES (4, 5, 1, 2);" );
		jdbcTemplate.execute( "INSERT INTO RepriceExport ( RepriceExportId, RepriceEventId, RepriceExportStatusId, InternetAdvertiserId, FailureCount) VALUES (5, 7, 4, 3, 3);" );
		databaseSetup = true;
		
	}
}

@Override
protected String[] getConfigLocations()
{
	// note this grabs the real repriceExportDao to test the query.
	return new String[] { "classpath:applicationContext.xml", "classpath:applicationContext-dao.xml",
			"classpath:test-applicationContext-dataSource.xml", "classpath:test-applicationContext-plugins.xml" };
}

}
