package biz.firstlook.export.reprice;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import biz.firstlook.export.reprice.RepriceExport;
import biz.firstlook.export.reprice.RepriceExportResult;
import biz.firstlook.export.reprice.RepriceTask;

public class MockRepriceTask extends RepriceTask
{

private static final Log log = LogFactory.getLog( MockRepriceTask.class );

public MockRepriceTask( RepriceExport event )
{
	super( event );
}

public RepriceExportResult call() throws Exception
{
	Integer id = getRepriceEvent().getId();
	
	
	//random delay, 15 sec max.
	long wait = (long)(Math.random() * 15);
	log.info( "Processing, will finish in " + wait + "s, RepriceEvent " + getRepriceEvent() );
	try {
		Thread.sleep(wait * 1000);
	} catch (Exception e) {
		//don't care
	}

	//do stuff here!
	RepriceExportResult result = new RepriceExportResult( getRepriceEvent(), new Date() );
	result.success();
	
	log.info( "Done processing RepriceEvent " + id );
	
	return result;
}

}
