package biz.firstlook.export.reprice;

public class MockThirdPartyEntity extends ThirdPartyEntity {

	private static final long serialVersionUID = 1045002694229179152L;

	public MockThirdPartyEntity(Integer thirdPartyEntityID,
			Integer businessUnitID, String name) {
		super(thirdPartyEntityID, businessUnitID, name);
	}

}
