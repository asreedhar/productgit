package com.ebizautos.api;


public class MockXMLListener implements XMLListener
{

public Object sendVehicle( double ID, String Auth, String xmlRequest ) throws CFCInvocationException
{
	String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
				"<rss version=\"2.0\" xmlns:g=\"http://base.google.com/ns/1.0\" xmlns:e=\"http://XML.eBizAutos.com\">" +
				"<channel>" +
				"<item>" +
				"<g:vin>SCFAC23323B500572</g:vin>" +
				"<status>fail</status>" +
				"<timestamp>2007-02-27 16:21:51.345</timestamp>" + 
				"<index>123</index>" + 
				"</item></channel></rss>";
	return xml;
}

}
