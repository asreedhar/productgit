package com.ebizautos.api;

import biz.firstlook.export.reprice.InventoryItem;
import biz.firstlook.export.reprice.eBizAutos.EBizRequestGenerator;
import biz.firstlook.export.reprice.eBizAutos.EBizResponseParseException;
import biz.firstlook.export.reprice.eBizAutos.EBizResponseParser;

import com.ebizautos.api2.XMLListenerClient;
import com.ebizautos.api2.XMLListenerSoap;

public class XmlListenerServiceClientTest2 {

	private static final int FIRSTLOOK_EBIZ_AUTH_ID = 170;
	private static final String FIRSTLOOK_EBIZ_AUTH_GUID = "4C4E5C0E-DB43-4BF3-94DE-6148A655ACBE";
	
	private static final int FIRSTLOOK_EBIZ_FEED_E_CLIENT_ID = 6114;
	private static final String FIRSTLOOK_EBIZ_FEED_GUID = "21DEF2FF-76F9-4395-A4A2-B7AB184422DF";

	/**
	 * Change the mileage and price or ebiz might complain that we are sending them multiple simultaneous identical requests.
	 * @param args
	 */
	public static void main(String[] args) {
		InventoryItem item = new InventoryItem(
				"WBAVA33588KX84632", 
				Integer.valueOf(2008), 
				"BMW", "3 Series", "8474", InventoryItem.INVENTORY_TYPE_USED, 
				Integer.valueOf(3210));
		
		EBizRequestGenerator generator = new EBizRequestGenerator();
		String request = generator.generate(FIRSTLOOK_EBIZ_FEED_E_CLIENT_ID, FIRSTLOOK_EBIZ_FEED_GUID, item, Float.valueOf(27206.00f));
		
		System.out.println(request);
		
		XMLListenerClient client = new XMLListenerClient("http://api.ebizautos.com/xmllistener.asmx");
		XMLListenerSoap service = client.getXMLListenerSoap();
		
		VehicleResponseRss rss = null;
		//When ebiz throws Internal Errors, we get a StackOverflowError with STaX.  This is something that we can't catch and is a problem with the xml parsing library.
		try {
			Object response = service.sendVehicle(""+FIRSTLOOK_EBIZ_AUTH_ID, FIRSTLOOK_EBIZ_AUTH_GUID, request);
			EBizResponseParser parser = new EBizResponseParser();
			rss = parser.parse(response.toString());
		} catch (EBizResponseParseException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(rss != null) {
			System.out.println(rss);
		}
		
		System.out.println("Finished.");
	}

}
