This project depends on 

../reprice-processor-common-libs.

Operations:

-Seperation of Business Domain

For each repricing destination (ebiz and aultec), the applicationContext-plugins.xml is generated from 
applicationContext-plugins.vvvv.xml where vvvv is the vendor.  We keep them separate to avoid
the entire processor crashing taking out other vital functions of our infrastructure.

The applicationContext-plugins.xml contains the connections to SIS and Reynolds&Reynolds to provide
DMS writeback functionality.  It would be trivial to break those out onto their own as well.

As of Jan. 26, 2010, we are running once process for each repricing destination, and one process 
for all of the DMS writeback destinations.

-Configuration

In order to connect to SSL enpoints, the certificates from the sites referenced by this processor 
should be imported into the RepriceProcessor.keystore file.  The keystore file should reside in 
the directory where the command to run the processor is invoked.