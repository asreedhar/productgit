Summary for week: ${startDate?date} - ${endDate?date}
-------------------------------------------

Dealership${"\t\t"}Total Updates Attempted${"\t\t"}Total Failed${"\t\t"}% of Failure
<#list summaryItems as summary>
${summary.dealership}${"\t\t"}${summary.attempted}${"\t\t"}${summary.failed}${"\t\t"}${summary.percentFailed}%
</#list>

-------------------------------------------

