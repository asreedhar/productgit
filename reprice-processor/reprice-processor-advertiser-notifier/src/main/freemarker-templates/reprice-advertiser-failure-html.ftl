<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<header>
	<title>Weekly (Pricing)Update Failure Report</title>
</header>
<body>
<table border="1">
<caption>Summary for week: ${startDate?date} - ${endDate?date}</caption>
<thead>
<tr>
	<td>Dealership</td>
	<td>Updates attempted</td>
	<td>Updates failed</td>
	<td>&#37; of failure</td>
</tr>
</thead>
<tfoot></tfoot>
<tbody>
<#list summaryItems as summary>
<tr>
	<td>${summary.dealership}</td>
	<td>${summary.attempted}</td>
	<td>${summary.failed}</td>
	<td>${summary.percentFailed}&#37;</td>
</tr>
</#list>
</tbody>
</table>
<br />
<br />
<table border="1">
<caption>Failures</caption>
<thead>
<tr>
	<td>Dealership</td>
	<td>Vin</td>
	<td>Year</td>
	<td>Make</td>
	<td>Model</td>
	<td>Stock&#35;</td>
	<td>New/Used</td>
	<td>Mileage</td>
	<td>Old price</td>
	<td>New price</td>
	<td>Failures&#35;</td>
</tr>
</thead>
<tfoot></tfoot>
<tbody>
<#list detailItems as detail>
<tr>
	<td>${detail.businessUnit}</td>
	<td>${detail.inventoryItem.vin}</td>
	<td>${detail.inventoryItem.year?string.number}</td>
	<td>${detail.inventoryItem.make}</td>
	<td>${detail.inventoryItem.model}</td>
	<td>${detail.inventoryItem.stockNumber}</td>
	<td>${detail.inventoryItem.inventoryType}</td>
	<td>${detail.inventoryItem.mileage}</td>
	<td>${detail.oldListPrice?string.currency}</td>
	<td>${detail.newListPrice?string.currency}</td>
	<td>${detail.failureCount}</td>
</tr>
</#list>
</tbody>
</table>
</body>
</html>