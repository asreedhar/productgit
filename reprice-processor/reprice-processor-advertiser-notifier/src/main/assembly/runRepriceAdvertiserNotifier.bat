@echo off
echo.
echo Running reprice-processor
echo.

SETLOCAL

SET CLASSPATH=

for %%f in (.\lib\*.jar) do call :cp %%f

SET CLASSPATH=%CLASSPATH%;.\conf;

echo Using CLASSPATH: %CLASSPATH%

java -cp %classpath% biz.firstlook.export.reprice.impl.RepriceAdvertiserFailureNotifier

ENDLOCAL

echo.

:cp
SET CLASSPATH=%CLASSPATH%;%1