#!/bin/sh

##
# build classpath
##
set classpath=
for i in `ls ./lib/*.jar`
do
  classpath=${classpath}:${i}
done

##
# run program
##
java -cp "conf/:${classpath}" biz.firstlook.export.reprice.impl.RepriceAdvertiserFailureNotifier
