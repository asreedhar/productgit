package biz.firstlook.export.reprice.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.export.reprice.InternetAdvertiserThirdPartyEntity;

class InternetAdvertiserJdbcDao extends JdbcDaoSupport implements
		InternetAdvertiserDao {

	private final static String QUERY_BY_ID = "SELECT * FROM dbo.InternetAdvertiser_ThirdPartyEntity ia WHERE ia.InternetAdvertiserID = ?";

	private InternetAdvertiserJdbcRowMapper rowMapper = new InternetAdvertiserJdbcRowMapper();

	public InternetAdvertiserThirdPartyEntity getInternetAdvertiser(
			Integer internetAdvertiserId) {
		return (InternetAdvertiserThirdPartyEntity) getJdbcTemplate()
				.queryForObject(QUERY_BY_ID,
						new Object[] { internetAdvertiserId }, rowMapper);
	}

	private class InternetAdvertiserJdbcRowMapper implements RowMapper {

		public Object mapRow(ResultSet rs, int arg1) throws SQLException {
			String name = rs.getString("DatafeedCode");
			Integer id = rs.getInt("InternetAdvertiserID");
			return new InternetAdvertiserThirdPartyEntity(id, 100150, name);
		}

	}
}
