package biz.firstlook.export.reprice.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.io.Resource;

import biz.firstlook.commons.email.EmailContextBuilder;
import biz.firstlook.commons.email.EmailService.EmailFormat;

public class RepriceAdvertiserFailureEmailBuilder implements EmailContextBuilder
{

private static final String TEMPLATE_NAME = "reprice-advertiser-failure";
private static final String EMAIL_SUBJECT = "Weekly Reprice Failure Report";

private List<String> emailAddresses;
private Map<String, Object> freemarkerContext;

public RepriceAdvertiserFailureEmailBuilder( List<String> emailAddress, Map<String, Object> freemarkerContext )
{
	this.emailAddresses = emailAddress;
	this.freemarkerContext = freemarkerContext;
}

public Map< String, EmailFormat > getEmailFormats()
{
	Map<String, EmailFormat > formatMap = new HashMap< String, EmailFormat >();
	if( emailAddresses != null )
		for( String emailAddress : emailAddresses )
			formatMap.put( emailAddress, EmailFormat.HTML_MULTIPART );
	return formatMap;
}

public Map< String, Resource > getEmbeddedResources()
{
	return null;
}

public Map< String, String > getEmbeddedResourcesContentTypes()
{
	return null;
}

public Map< String, Map<String, Object> > getRecipientsAndContext()
{
	Map<String, Map<String, Object> > contextMap = new HashMap< String, Map<String, Object> >();
	if( emailAddresses != null )
		for( String emailAddress : emailAddresses )
			contextMap.put( emailAddress, freemarkerContext );
	return contextMap;
}

public String getReplyTo()
{
	return null;
}

public String getSubject()
{
	return EMAIL_SUBJECT;
}

public String getTemplateName()
{
	return TEMPLATE_NAME;
}

public List<String> getCcEmailAddresses() {
	return Collections.emptyList();
}

}
