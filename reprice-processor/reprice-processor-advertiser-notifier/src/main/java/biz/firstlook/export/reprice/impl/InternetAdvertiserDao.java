package biz.firstlook.export.reprice.impl;

import biz.firstlook.export.reprice.InternetAdvertiserThirdPartyEntity;




interface InternetAdvertiserDao
{

InternetAdvertiserThirdPartyEntity getInternetAdvertiser( Integer internetAdvertiserId);

}
