package biz.firstlook.export.reprice.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.export.reprice.InternetAdvertiserThirdPartyEntity;
import biz.firstlook.export.reprice.InventoryItem;
import biz.firstlook.export.reprice.RepriceExportStatus;

public class RepriceAdvertiserFailureDao extends JdbcDaoSupport
{

private static final String summaryQuery;
private SummaryItemRowMapper summaryItemRowMapper = new SummaryItemRowMapper();

private static final String detailQuery;
private DetailItemRowMapper detailItemRowMapper = new DetailItemRowMapper();

static
{
	// to count the number attempted, count the successes and failureCounts.
	// Successes have a failure count of 0, so the attempt is 1. failure count stays as is if not 0.
	StringBuilder summaryQueryBuilder = new StringBuilder();
	summaryQueryBuilder.append( " SELECT bu.BusinessUnit, sum( CASE e.FailureCount WHEN 0 THEN 1 ELSE e.FailureCount END ) 'Attempted', sum(e.FailureCount) 'Failed'" );
	summaryQueryBuilder.append( " , cast(cast(sum(e.FailureCount) as decimal(4,2)) / cast(sum( CASE e.FailureCount WHEN 0 THEN 1 ELSE e.FailureCount END ) as decimal(4,2)) * 100 as int) as 'pFailed'" );
	summaryQueryBuilder.append( " FROM RepriceExport e " );
	summaryQueryBuilder.append( " LEFT JOIN ( " );
	summaryQueryBuilder.append( " SELECT RepriceExportId FROM RepriceExport e " );
	summaryQueryBuilder.append( " WHERE RepriceExportStatusId = " ).append( RepriceExportStatus.Failure.getId() );
	summaryQueryBuilder.append( " ) e2 on e.RepriceExportId = e2.RepriceExportId " );
	summaryQueryBuilder.append( " JOIN RepriceEvent re on e.repriceEventId = re.repriceEventId " );
	summaryQueryBuilder.append( " JOIN Inventory i on re.inventoryId = i.inventoryId " );
	summaryQueryBuilder.append( " JOIN BusinessUnit bu on i.businessUnitId = bu.businessUnitId " );
	summaryQueryBuilder.append( " JOIN InternetAdvertiserDealership iad " );
	summaryQueryBuilder.append( " ON i.businessUnitId = iad.businessUnitId AND iad.InternetAdvertiserID = ? " );
	summaryQueryBuilder.append( " WHERE e.RepriceExportStatusID = " ).append( RepriceExportStatus.Successful.getId() );
	summaryQueryBuilder.append( " OR (e.RepriceExportStatusID = " ).append( RepriceExportStatus.Failure.getId() );
	summaryQueryBuilder.append( " AND e.FailureCount > ?) " );
	summaryQueryBuilder.append( " AND e.DateSent between ? and ? " );
	summaryQueryBuilder.append( " GROUP BY bu.BusinessUnit " );
	summaryQueryBuilder.append( " ORDER BY bu.BusinessUnit " );
	summaryQuery = summaryQueryBuilder.toString();

	StringBuilder detailQueryBuilder = new StringBuilder();
	detailQueryBuilder.append( " SELECT bu.BusinessUnit, v.Vin, v.VehicleYear, v.Make, v.Model, i.StockNumber " );
	detailQueryBuilder.append( " , i.InventoryType, i.MileageReceived, re.originalPrice, re.NewPrice, e.FailureCount " );
	detailQueryBuilder.append( " FROM RepriceExport e " );
	detailQueryBuilder.append( " JOIN RepriceEvent re on e.repriceEventId = re.repriceEventId " );
	detailQueryBuilder.append( " JOIN Inventory i on h.inventoryId = i.inventoryId " );
	detailQueryBuilder.append( " JOIN Vehicle v on i.vehicleID = v.vehicleID " );
	detailQueryBuilder.append( " JOIN BusinessUnit bu on i.businessUnitId = bu.businessUnitId " );
	detailQueryBuilder.append( " JOIN InternetAdvertiserDealership iad " );
	detailQueryBuilder.append( "   ON i.businessUnitId = iad.businessUnitId AND iad.InternetAdvertiserID = ?" );
	detailQueryBuilder.append( " WHERE e.RepriceExportStatusID = " ).append( RepriceExportStatus.Failure.getId() );
	detailQueryBuilder.append( " AND e.FailureCount > ? " );
	detailQueryBuilder.append( " AND e.DateSent between ? and ? " );
	detailQueryBuilder.append( " ORDER BY bu.BusinessUnit " );
	detailQuery = detailQueryBuilder.toString();
}

@SuppressWarnings( "unchecked" )
public List< SummaryItem > getSummaryItems( InternetAdvertiserThirdPartyEntity internetAdvertiser, Date startDate, Date endDate, Integer failureRetryAttempts )
{
	List< SummaryItem > items = getJdbcTemplate().query(
															summaryQuery,
															new Object[] { internetAdvertiser.getThirdPartyEntityID(), failureRetryAttempts,
																	startDate, endDate }, summaryItemRowMapper );
	return items;
}

@SuppressWarnings("unchecked")
public List< DetailItem > getDetailItems( InternetAdvertiserThirdPartyEntity internetAdvertiser, Date startDate, Date endDate, Integer failureRetryAttempts )
{
	List< DetailItem > items = getJdbcTemplate().query(
															detailQuery,
															new Object[] { internetAdvertiser.getThirdPartyEntityID(), failureRetryAttempts,
																	startDate, endDate }, detailItemRowMapper );
	return items;
}

private class SummaryItemRowMapper implements RowMapper
{
public Object mapRow( ResultSet rs, int index ) throws SQLException
{
	SummaryItem item = new SummaryItem();
	item.setDealership( rs.getString( "BusinessUnit" ) );
	item.setAttempted( rs.getInt( "Attempted" ) );
	item.setFailed( rs.getInt( "Failed" ) );
	item.setPercentFailed( rs.getInt( "pFailed" ) );
	return item;
}
}

private class DetailItemRowMapper implements RowMapper
{
public Object mapRow( ResultSet rs, int index ) throws SQLException
{
	DetailItem item = new DetailItem();
	item.setBusinessUnit( rs.getString( "BusinessUnit" ) );
	item.setFailureCount( rs.getInt( "FailureCount" ) );
	item.setOldListPrice( rs.getInt( "OldListPrice" ) );
	item.setNewListPrice( rs.getInt( "NewListPrice" ) );
	
	String vin = rs.getString( "Vin" );
	Integer vehicleYear = rs.getInt( "VehicleYear" );
	String make = rs.getString( "Make" );
	String model = rs.getString( "Model" );
	String stockNumber = rs.getString( "StockNumber" );
	Integer inventoryType = rs.getInt( "InventoryType" );
	Integer mileage = rs.getInt( "MileageReceived" );
	InventoryItem inventory = new InventoryItem( vin, vehicleYear, make, model, stockNumber, inventoryType, mileage );

	item.setInventoryItem( inventory );
	
	return item;
}
}

}
