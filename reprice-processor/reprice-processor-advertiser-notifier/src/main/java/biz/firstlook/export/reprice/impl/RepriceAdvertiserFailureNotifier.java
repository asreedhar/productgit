package biz.firstlook.export.reprice.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import biz.firstlook.commons.email.EmailService;
import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.export.reprice.InternetAdvertiserThirdPartyEntity;

public class RepriceAdvertiserFailureNotifier implements Runnable, InitializingBean
{

private static final Log log = LogFactory.getLog( RepriceAdvertiserFailureNotifier.class );

private List<String> emailAddresses = new ArrayList< String >();
private Integer internetAdvertiserThirdPartyEntityId;
private Integer failureRetryAttempts;

private EmailService emailService;
private RepriceAdvertiserFailureDao repriceAdvertiserFailureDao;
private InternetAdvertiserDao internetAdvertiserDao;

public static void main( String[] args )
{
	AbstractApplicationContext applicationContext = new ClassPathXmlApplicationContext( new String[] {"applicationContext.xml", "applicationContext-dataSource.xml"} );
	RepriceAdvertiserFailureNotifier notifier = (RepriceAdvertiserFailureNotifier)applicationContext.getBean( "repriceFailureNotifier" );
	notifier.run();	
	applicationContext.destroy();
}

public RepriceAdvertiserFailureNotifier()
{
}

public RepriceAdvertiserFailureNotifier( List<String> emailAddresses, Integer internetAdvertiserThirdPartyEntityId)
{
	this.emailAddresses = emailAddresses;
	this.internetAdvertiserThirdPartyEntityId = internetAdvertiserThirdPartyEntityId;
}

public void afterPropertiesSet() throws Exception
{
	if( emailAddresses == null )
		throw new IllegalArgumentException( "No Email Address provided!" );
	
	if( internetAdvertiserThirdPartyEntityId == null )
		throw new IllegalArgumentException( "No Internet Advertiser specified!" );
	
	if( failureRetryAttempts == null )
	{
		log.info( "No failure retry attempts set, defaulting to 5" );
		failureRetryAttempts = 5;
	}
	
	if( emailService == null )
		throw new IllegalArgumentException( "EmailService was not set." );
	
	if( repriceAdvertiserFailureDao == null )
		throw new IllegalArgumentException( "RepriceAdvertiserFailureDao was not set." );
	
	if( internetAdvertiserDao == null )
		throw new IllegalArgumentException( "InternetAdvertiserDao was not set." );

}

public void run()
{
	Map<String, Object> emailContext = new HashMap< String, Object >();
	
	Date today = new Date();
	
	Date startDate = DateUtilFL.getWeekStartDate( today );
	Date endDate = DateUtilFL.getWeekEndDate( today );
	
	emailContext.put( "startDate", startDate );
	emailContext.put( "endDate", endDate );
	
	InternetAdvertiserThirdPartyEntity internetAdvertiser = internetAdvertiserDao.getInternetAdvertiser( internetAdvertiserThirdPartyEntityId );
	List<SummaryItem> items = repriceAdvertiserFailureDao.getSummaryItems( internetAdvertiser, startDate, endDate, failureRetryAttempts );
	List<DetailItem> details = repriceAdvertiserFailureDao.getDetailItems( internetAdvertiser, startDate, endDate, failureRetryAttempts );
	
	emailContext.put( "summaryItems", items );
	emailContext.put( "detailItems", details );
	
	RepriceAdvertiserFailureEmailBuilder report = new RepriceAdvertiserFailureEmailBuilder( emailAddresses, emailContext );
	
	emailService.sendEmail( report );
}

public void setRepriceAdvertiserFailureDao( RepriceAdvertiserFailureDao dao )
{
	this.repriceAdvertiserFailureDao = dao;
}

public void setInternetAdvertiserThirdPartyEntityId(
		Integer internetAdvertiserThirdPartyEntityId) {
	this.internetAdvertiserThirdPartyEntityId = internetAdvertiserThirdPartyEntityId;
}

public void setEmailAddresses( List<String> emailAddress )
{
	this.emailAddresses = emailAddress;
}

public void setEmailService( EmailService emailService )
{
	this.emailService = emailService;
}

public void setInternetAdvertiserDao( InternetAdvertiserDao internetAdvertiserDao )
{
	this.internetAdvertiserDao = internetAdvertiserDao;
}

public void setFailureRetryAttempts( Integer failureRetryAttempts )
{
	this.failureRetryAttempts = failureRetryAttempts;
}



}
