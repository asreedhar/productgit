package biz.firstlook.export.reprice.impl;

import biz.firstlook.export.reprice.InventoryItem;

public class DetailItem
{

private String businessUnit;
private InventoryItem inventoryItem;
private Integer oldListPrice;
private Integer newListPrice;
private Integer failureCount;

public DetailItem()
{
	
}

public DetailItem( String businessUnitId, InventoryItem inventoryItem, Integer oldListPrice, Integer newListPrice, Integer failureCount )
{
	super();
	this.businessUnit = businessUnitId;
	this.inventoryItem = inventoryItem;
	this.oldListPrice = oldListPrice;
	this.newListPrice = newListPrice;
	this.failureCount = failureCount;
}

public String getBusinessUnit()
{
	return businessUnit;
}

public void setBusinessUnit( String businessUnit )
{
	this.businessUnit = businessUnit;
}

public Integer getFailureCount()
{
	return failureCount;
}

public void setFailureCount( Integer failureCount )
{
	this.failureCount = failureCount;
}

public InventoryItem getInventoryItem()
{
	return inventoryItem;
}

public void setInventoryItem( InventoryItem inventoryItem )
{
	this.inventoryItem = inventoryItem;
}

public Integer getNewListPrice()
{
	return newListPrice;
}

public void setNewListPrice( Integer newListPrice )
{
	this.newListPrice = newListPrice;
}

public Integer getOldListPrice()
{
	return oldListPrice;
}

public void setOldListPrice( Integer oldListPrice )
{
	this.oldListPrice = oldListPrice;
}
}
