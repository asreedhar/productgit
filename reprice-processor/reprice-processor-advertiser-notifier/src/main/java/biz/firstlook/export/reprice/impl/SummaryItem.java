package biz.firstlook.export.reprice.impl;

public class SummaryItem
{

private String dealership;
private Integer attempted;
private Integer failed;
private Integer percentFailed;

public Integer getAttempted()
{
	return attempted;
}

public void setAttempted( Integer attempted )
{
	this.attempted = attempted;
}

public String getDealership()
{
	return dealership;
}

public void setDealership( String dealership )
{
	this.dealership = dealership;
}

public Integer getFailed()
{
	return failed;
}

public void setFailed( Integer failed )
{
	this.failed = failed;
}

public Integer getPercentFailed()
{
	return percentFailed;
}

public void setPercentFailed( Integer percentFailed )
{
	this.percentFailed = percentFailed;
}

}
