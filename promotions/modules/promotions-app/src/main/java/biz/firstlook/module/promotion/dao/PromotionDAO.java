package biz.firstlook.module.promotion.dao;

import java.util.Date;
import java.util.List;

import biz.firstlook.module.promotion.model.Promotion;

/**
 * @author Jason Koziol
 * @version 1.0
 * @created 25-Aug-2010 3:44:25 PM
 */
public interface PromotionDAO {

	public List<Promotion> list();

	public List<Promotion> listByEnabledStartName();
	
	public List<Promotion> listEnabledByPriorityStartCreated();
	
	public List<Promotion> listValid(Date date);
	
	public Promotion load(Integer id);

	public Promotion save(Promotion promotion, Integer memberId);
}