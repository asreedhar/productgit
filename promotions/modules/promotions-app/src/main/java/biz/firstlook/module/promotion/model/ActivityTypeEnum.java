package biz.firstlook.module.promotion.model;

/**
 * @author Jason Koziol
 * @version 1.0
 * @created 25-Aug-2010 3:44:24 PM
 */
public enum ActivityTypeEnum
{
	VIEW (1),
	SKIP (2),
	NEVER (3);
	
	private Integer activityType;

	ActivityTypeEnum(Integer activityType)
	{
		this.activityType = activityType;
	}
	
	public Integer getActivityType() {
		return activityType;
	}

	public void setActivityType(Integer activityType) {
		this.activityType = activityType;
	}
}
