package biz.firstlook.module.promotion.dao.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.module.promotion.dao.PromotionDAO;
import biz.firstlook.module.promotion.model.Promotion;

/**
 * @author Jason Koziol
 * @version 1.0
 * @created 25-Aug-2010 3:44:25 PM
 */
@SuppressWarnings("unchecked")
public class PromotionDAOImpl extends HibernateDaoSupport implements PromotionDAO 
{
	public List<Promotion> list()
	{
		return (List<Promotion>) getHibernateTemplate().find(" from biz.firstlook.module.promotion.model.Promotion");
	}

	public List<Promotion> listByEnabledStartName()
	{
		return (List<Promotion>) getHibernateTemplate().find(" from biz.firstlook.module.promotion.model.Promotion p " +
				"order by p.enabled desc, p.startDate, p.name");
	}

	public List<Promotion> listEnabledByPriorityStartCreated()
	{
		return (List<Promotion>) getHibernateTemplate().find(" from biz.firstlook.module.promotion.model.Promotion p " +
				"where p.enabled = true " +
				"order by p.priority, p.startDate, p.insertDate");
	}

	public List<Promotion> listValid(Date date)
	{
		return (List<Promotion>) getHibernateTemplate().find(" from biz.firstlook.module.promotion.model.Promotion p " +
			"where p.enabled = true");
	}

	/**
	 * 
	 * @param id    id
	 */
	public Promotion load(Integer id)
	{
		return (Promotion)getHibernateTemplate().get(Promotion.class, id);
	}

	/**
	 * 
	 * @param promotion    promotion
	 */
	public Promotion save(Promotion promotion, Integer memberId)
	{
		Integer promotionId = promotion.getPromotionId(); 
		
		if( promotionId == null )
		{
			promotion.setInsertUser(memberId);
			promotion.setInsertDate(Calendar.getInstance().getTime());
			promotionId = (Integer)getHibernateTemplate().save(promotion);
		}
		else
		{
			promotion.setUpdateUser(memberId);
			promotion.setUpdateDate(Calendar.getInstance().getTime());
			getHibernateTemplate().saveOrUpdate( promotion );
		}
		
		return (Promotion)getHibernateTemplate().get(Promotion.class, promotionId);
	}
}