package biz.firstlook.module.promotion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Jason Koziol
 * @version 1.0
 * @created 25-Aug-2010 3:44:24 PM
 */
@Entity
@Table(name = "Member", schema="dbo")
public class Member implements Serializable 
{
	private static final long serialVersionUID = 4354313556745639892L;
	
	public final static int MEMBER_TYPE_ADMIN = 1;
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "MemberID")
	private Integer id;
	
    @Column(name = "Login")
	private String userName;
    private Integer memberType;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getMemberType() {
		return memberType;
	}

	public void setMemberType(Integer memberType) {
		this.memberType = memberType;
	}
	
	public boolean isAdmin()
	{
		return this.memberType.intValue() == MEMBER_TYPE_ADMIN; 
	}
}