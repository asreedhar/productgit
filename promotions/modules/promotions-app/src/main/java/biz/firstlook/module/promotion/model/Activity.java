package biz.firstlook.module.promotion.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Jason Koziol
 * @version 1.0
 * @created 25-Aug-2010 3:44:24 PM
 */
@Entity
@Table(name = "Activity", schema="Promotion")
public class Activity {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ActivityID")
	private Integer id;
    @Column(name = "ActivityTypeID")
	private Integer typeId;
    @Column(name = "PromotionID")
	private Integer promotionId;
	private Integer insertUser = 121431;
	private Date insertDate = new Date();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public Integer getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(Integer promotionId) {
		this.promotionId = promotionId;
	}

	public Integer getInsertUser() {
		return insertUser;
	}

	public void setInsertUser(Integer insertUser) {
		this.insertUser = insertUser;
	}

	public Date getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}

	@Override
	public String toString() 
	{
		return "activity (" + typeId + "): promo - " + promotionId + ", member - " + insertUser;
	}
}