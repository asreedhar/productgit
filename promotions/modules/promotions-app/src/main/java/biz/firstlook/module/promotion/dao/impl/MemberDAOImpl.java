package biz.firstlook.module.promotion.dao.impl;
import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.module.promotion.dao.MemberDAO;
import biz.firstlook.module.promotion.model.Member;

/**
 * @author Jason Koziol
 * @version 1.0
 * @created 25-Aug-2010 3:44:24 PM
 */
public class MemberDAOImpl extends HibernateDaoSupport implements MemberDAO {

	@SuppressWarnings("unchecked")
	public Member find(String userName)
	{
		Object[] parameterValues = { (Object)userName };
		
		String sql = " from biz.firstlook.module.promotion.model.Member m " +
		"where m.userName = ?";
		
		List<Member> members = (List<Member>)getHibernateTemplate().find( sql, parameterValues);
		
		return members.get(0);
	}

	public Member load(Integer id)
	{
		return (Member)getHibernateTemplate().get(Member.class, id);
	}
}