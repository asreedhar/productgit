package biz.firstlook.module.promotion.service;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import biz.firstlook.module.promotion.dao.ActivityDAO;
import biz.firstlook.module.promotion.dao.MemberDAO;
import biz.firstlook.module.promotion.dao.PromotionDAO;
import biz.firstlook.module.promotion.model.Activity;
import biz.firstlook.module.promotion.model.ActivityTypeEnum;
import biz.firstlook.module.promotion.model.Promotion;

/**
 * @author Jason Koziol
 * @version 1.0
 * @created 25-Aug-2010 3:44:25 PM
 */
public class PromotionServiceImpl implements PromotionService 
{
	private PromotionDAO promotionDAO;
	private ActivityDAO activityDAO;
	private MemberDAO memberDAO;
	
	public void setPromotionDAO( PromotionDAO promotionDAO )
	{
		this.promotionDAO = promotionDAO;
	}
	
	public void setActivityDAO( ActivityDAO activityDAO )
	{
		this.activityDAO = activityDAO;
	}
	
	public void setMemberDAO( MemberDAO memberDAO )
	{
		this.memberDAO = memberDAO;
	}
	
	public Promotion loadPromotion( Integer id )
	{
		Promotion promotion = promotionDAO.load(id);
		
		return promotion;
	}

	public List<Promotion> listPromotions()
	{
		return promotionDAO.listByEnabledStartName();
	}

	public PagedPromotionList listPromotions(int page, int pageSize)
	{
		PagedPromotionList promotions = new PagedPromotionList(pageSize);
		
		promotions.addAll(promotionDAO.listByEnabledStartName());
		
		promotions.setPage(page);
		
		return promotions;
	}

	public void savePromotion(Promotion promotion, String userName)
	{
		Integer memberId = memberDAO.find(userName).getId();
		
		promotionDAO.save(promotion, memberId);
	}

	public Promotion nextPromotionForMember(String userName)
	{
		Promotion promotionResult = null;
		
		Integer memberId = memberDAO.find(userName).getId();
		
		Calendar today = Calendar.getInstance();
		Calendar startDate = Calendar.getInstance();
		Calendar endDate = Calendar.getInstance();
		
		List<Promotion> promotions = promotionDAO.listEnabledByPriorityStartCreated();
		Iterator<Promotion> it = promotions.iterator();
		while( it.hasNext() )
		{
			Promotion promotion = it.next();
			startDate.setTime(promotion.getStartDate());
			endDate.setTime(promotion.getEndDate());
			if( startDate.before(today) && endDate.after(today))
			{
				Integer views = activityDAO.getViews(promotion.getPromotionId(),memberId);
				Integer nevers = activityDAO.getNevers(promotion.getPromotionId(),memberId);
				if( promotion.getMaxViews() > views && 0 == nevers )
				{
					promotionResult = promotion;
					break;
				}
			}
		}
		
		return promotionResult;
	}
	
	private void storePromotionActivity(String userName, ActivityTypeEnum activityType)
	{
		Activity activity = new Activity();
		
		Integer memberId = memberDAO.find(userName).getId();
		activity.setInsertUser(memberId);
		activity.setInsertDate(Calendar.getInstance().getTime());
		activity.setTypeId(activityType.getActivityType());
		activity.setPromotionId(this.nextPromotionForMember(userName).getPromotionId());
		
		activityDAO.save(activity);
		
	}

	public void viewPromotion(String userName, Boolean neverShowAgain)
	{
		storePromotionActivity(userName, ActivityTypeEnum.VIEW);

		if( neverShowAgain )
		{
			storePromotionActivity(userName, ActivityTypeEnum.NEVER);
		}
	}
	
	public void skipPromotion(String userName, Boolean neverShowAgain)
	{
		storePromotionActivity(userName, ActivityTypeEnum.SKIP);

		if( neverShowAgain )
		{
			storePromotionActivity(userName, ActivityTypeEnum.NEVER);
		}
	}
	
	public boolean isMemberAdmin(String userName)
	{
		return memberDAO.find(userName).isAdmin();
	}
}