package biz.firstlook.module.promotion.dao;
import java.util.List;

import biz.firstlook.module.promotion.model.Activity;

/**
 * @author Jason Koziol
 * @version 1.0
 * @created 25-Aug-2010 3:44:24 PM
 */
public interface ActivityDAO {

	public Activity save(Activity activity);
	public List<Activity> getActivities(Integer promotionId, Integer memberId);
	public Integer getViews(Integer promotionId, Integer memberId);
	public Integer getNevers(Integer promotionId, Integer memberId);
}