package biz.firstlook.module.promotion.dao.impl;
import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.module.promotion.dao.ActivityDAO;
import biz.firstlook.module.promotion.model.Activity;
import biz.firstlook.module.promotion.model.ActivityTypeEnum;

/**
 * @author Jason Koziol
 * @version 1.0
 * @created 25-Aug-2010 3:44:24 PM
 */
public class ActivityDAOImpl extends HibernateDaoSupport implements ActivityDAO 
{
	public Activity save(Activity activity){
		Integer activityId = activity.getId(); 
		
		if( activityId == null )
		{
			activityId =  (Integer)getHibernateTemplate().save(activity);
		}
		else
		{
			getHibernateTemplate().saveOrUpdate( activity );
		}
		
		return (Activity)getHibernateTemplate().get(Activity.class, activityId);
	}
	
	@SuppressWarnings("unchecked")
	public List<Activity> getActivities(Integer promotionId, Integer memberId)
	{
		Object[] parameterValues = { (Object)promotionId, (Object)memberId };
		
		String sql = " from biz.firstlook.module.promotion.model.Activity a " +
		"where a.promotionId = ? and a.insertUser = ?";
		
		return (List<Activity>) getHibernateTemplate().find( sql, parameterValues );
	}
	

	@SuppressWarnings("unchecked")
	public List<Activity> getActivities(Integer promotionId, Integer memberId, ActivityTypeEnum activityType)
	{
		Object[] parameterValues = { (Object)promotionId, (Object)memberId, (Object)activityType.getActivityType() };
		
		String sql = " from biz.firstlook.module.promotion.model.Activity a " +
			"where a.promotionId = ? and a.insertUser = ? and a.typeId = ?";
		
		return (List<Activity>) getHibernateTemplate().find( sql, parameterValues );
	}
	
	public Integer getViews(Integer promotionId, Integer memberId)
	{
		List<Activity> activities = getActivities(promotionId, memberId, ActivityTypeEnum.VIEW); 
		return activities.size();
	}
	
	public Integer getNevers(Integer promotionId, Integer memberId)
	{
		List<Activity> activities = getActivities(promotionId, memberId, ActivityTypeEnum.NEVER); 
		return activities.size();
	}
}