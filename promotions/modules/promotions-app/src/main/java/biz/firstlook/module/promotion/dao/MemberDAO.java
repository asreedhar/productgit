package biz.firstlook.module.promotion.dao;
import biz.firstlook.module.promotion.model.Member;

/**
 * @author Jason Koziol
 * @version 1.0
 * @created 25-Aug-2010 3:44:24 PM
 */
public interface MemberDAO
{
	/**
	 * 
	 * @param userName    userName
	 */
	public Member find(String userName);
	public Member load(Integer id);
}