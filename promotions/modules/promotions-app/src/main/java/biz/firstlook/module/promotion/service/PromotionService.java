package biz.firstlook.module.promotion.service;

import java.util.List;

import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import biz.firstlook.module.promotion.model.Promotion;

/**
 * @author Jason Koziol
 * @version 1.0
 * @created 25-Aug-2010 3:44:25 PM
 */
public interface PromotionService {

	public Promotion loadPromotion(Integer id);

	public List<Promotion> listPromotions();
	
	public PagedPromotionList listPromotions(int page, int pageSize);

	@Transactional(isolation=Isolation.READ_COMMITTED,propagation=Propagation.REQUIRES_NEW,rollbackFor={RuntimeException.class,Exception.class})
	public void savePromotion(Promotion promotion, String userName);

	public Promotion nextPromotionForMember(String userName);

	@Transactional(isolation=Isolation.READ_COMMITTED,propagation=Propagation.REQUIRES_NEW,rollbackFor={RuntimeException.class,Exception.class})
	public void viewPromotion(String userName, Boolean neverShowAgain);
	
	@Transactional(isolation=Isolation.READ_COMMITTED,propagation=Propagation.REQUIRES_NEW,rollbackFor={RuntimeException.class,Exception.class})
	public void skipPromotion(String userName, Boolean neverShowAgain);
	
	public boolean isMemberAdmin(String userName);
}