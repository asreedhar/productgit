package biz.firstlook.module.promotion.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Jason Koziol
 * @version 1.0
 * @created 25-Aug-2010 3:44:24 PM
 */
@Entity
@Table(name = "Promotion", schema="Promotion")
public class Promotion implements Serializable{

	private static final long serialVersionUID = 4354313556745639891L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "PromotionID")
	private Integer id;
	private String name;
	private String description;
	private Date startDate;
	private Date endDate;
	private Boolean enabled;
	private Integer maxViews;
	private Integer priority;
	private String promotionUrl;
	private String promotionSummaryUrl;
	private Integer insertUser;
	private Date insertDate;
	private Integer updateUser;
	private Date updateDate;

	public Integer getPromotionId()
	{
	    return id;
	}
	
	public String getName()
	{
	    return name;
	}
	
	public int getMaxViews()
	{
	    return maxViews;
	}
	
	public void setPromotionId( Integer id )
	{
	    this.id = id;
	}
	
	public void setName( String name )
	{
	    this.name = name;
	}
	
	public void setMaxViews( Integer maxViews )
	{
	    this.maxViews = maxViews;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public String getPromotionUrl() {
		return promotionUrl;
	}

	public void setPromotionUrl(String promotionUrl) {
		this.promotionUrl = promotionUrl;
	}

	public String getPromotionSummaryUrl() {
		return promotionSummaryUrl;
	}

	public void setPromotionSummaryUrl(String promotionSummaryUrl) {
		this.promotionSummaryUrl = promotionSummaryUrl;
	}

	public Integer getInsertUser() {
		return insertUser;
	}

	public void setInsertUser(Integer insertUser) {
		this.insertUser = insertUser;
	}

	public Date getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Date startDate) {
		this.insertDate = startDate;
	}

	public Integer getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(Integer updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	public boolean hasStarted()
	{
		Calendar beginningOfToday = Calendar.getInstance();
		beginningOfToday.set(Calendar.HOUR, 0);
		beginningOfToday.set(Calendar.MINUTE, 0);
		beginningOfToday.set(Calendar.SECOND, 0);
		beginningOfToday.set(Calendar.MILLISECOND, 0);
		
		
		return (startDate == null? false : startDate.before(beginningOfToday.getTime()) );
	}

	@Override
	public String toString() 
	{
		return "promotion (" + id + "): " + name;
	}
}