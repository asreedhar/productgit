package biz.firstlook.module.promotion.service;

import java.util.ArrayList;
import java.util.List;

import biz.firstlook.module.promotion.model.Promotion;

public class PagedPromotionList extends ArrayList<Promotion> implements List<Promotion> 
{
    private static final long serialVersionUID = 8683452581122892190L;
    
	int DEFAULT_PAGE_SIZE = 25;
	
	private int page = 1;
	private int pageSize = DEFAULT_PAGE_SIZE;
		
	public PagedPromotionList(int pageSize)
	{
		super();
		this.pageSize = pageSize;
	}
	
	public Integer getPage()
	{
		return page;
	}
	
	public void setPage(int page)
	{
		this.page = Math.max(1, page);
		this.page = Math.min(this.page, (this.size()/pageSize)+1);
		
		int first = (this.page - 1) * pageSize;
		int last = Math.min(this.size(), this.page * pageSize);
		
		ArrayList<Promotion> buffer = new ArrayList<Promotion>();
		buffer.addAll(this);
		this.clear();
		
		this.addAll(buffer.subList(first,last));
	}
	
	public Integer getTotalPages()
	{
		return new Integer((this.size()/pageSize)+1);
	}
}
