package biz.firstlook.main;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AllTests extends TestCase
{

public AllTests( String arg0 )
{
    super(arg0);
}

public static Test suite()
{
    TestSuite suite = new TestSuite();

    suite.addTest(biz.firstlook.module.promotion.service.AllTests.suite());
    return suite;
}

}
