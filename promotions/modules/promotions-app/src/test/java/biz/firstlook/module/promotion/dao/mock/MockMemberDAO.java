package biz.firstlook.module.promotion.dao.mock;

import java.util.HashMap;

import biz.firstlook.module.promotion.dao.MemberDAO;
import biz.firstlook.module.promotion.model.Member;

public class MockMemberDAO implements MemberDAO {

	private HashMap<String,Member> membersByUserName = new HashMap<String,Member>();
	private HashMap<Integer,Member> membersById = new HashMap<Integer,Member>();
	
	public MockMemberDAO()
	{
		Member member = new Member();
		member.setId(1);
		member.setUserName("test");
		
		membersByUserName.put(member.getUserName(), member);
		membersById.put(member.getId(), member);
	}
	
	public Member find(String userName) {
		
		return membersByUserName.get(userName);
	}

	public Member load(Integer id) {
		return membersById.get(id);
	}

}
