package biz.firstlook.module.promotion.service;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

public class AllTests {

	public static void main( String[] args )
	{
		TestRunner.run( suite() );
	}

	public static Test suite()
	{
		TestSuite suite = new TestSuite( "Promotion Service Tests" );
		suite.addTestSuite(PromotionServiceTest.class);
		return suite;
	}
}
