package biz.firstlook.module.promotion.dao.mock;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import biz.firstlook.module.promotion.dao.PromotionDAO;
import biz.firstlook.module.promotion.model.Promotion;

public class MockPromotionDAO implements PromotionDAO {
	
	private HashMap<Integer,Promotion> promotionsById = new HashMap<Integer,Promotion>();
	
	public MockPromotionDAO()
	{
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -1);
		Date startDate = calendar.getTime();
		calendar.add(Calendar.DATE, 2);
		Date endDate = calendar.getTime();
		
		Promotion promotion1 = new Promotion();
		
		promotion1.setPromotionId(1);
		promotion1.setName("testpromo1");
		promotion1.setEnabled(true);
		promotion1.setStartDate(startDate);
		promotion1.setEndDate(endDate);
		promotion1.setMaxViews(2);
		promotion1.setPriority(1);
		
		Promotion promotion2 = new Promotion();
		promotion2.setPromotionId(2);
		promotion2.setName("testpromo2");
		promotion2.setEnabled(true);
		promotion2.setStartDate(startDate);
		promotion2.setEndDate(endDate);
		promotion2.setMaxViews(2);
		promotion2.setPriority(2);
		
		promotionsById.put(promotion2.getPromotionId(), promotion2);
		promotionsById.put(promotion1.getPromotionId(), promotion1);
	}

	public List<Promotion> list() {
		return (List<Promotion>)promotionsById.values();
	}

	public List<Promotion> listValid(Date date) {
		return new ArrayList<Promotion>( promotionsById.values() );
	}

	public List<Promotion> listByEnabledStartName(){
		return new ArrayList<Promotion>( promotionsById.values() );
	}
	
	public List<Promotion> listEnabledByPriorityStartCreated(){
		return new ArrayList<Promotion>( promotionsById.values() );
	}
	
	public Promotion load(Integer id) {
		return promotionsById.get(id);
	}

	public Promotion save(Promotion promotion, Integer memberId) {
		return null;
	}
}
