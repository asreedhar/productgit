package biz.firstlook.module.promotion.service;

import biz.firstlook.module.promotion.dao.mock.MockActivityDAO;
import biz.firstlook.module.promotion.dao.mock.MockMemberDAO;
import biz.firstlook.module.promotion.dao.mock.MockPromotionDAO;
import biz.firstlook.module.promotion.model.Promotion;
import junit.framework.TestCase;


public class PromotionServiceTest extends TestCase {
	
	PromotionService promotionService;

	public PromotionServiceTest( String name )
	{
		super( name );
	}

	protected void setUp() throws Exception
	{
		PromotionServiceImpl promotionServiceImpl = new PromotionServiceImpl();
		
		promotionServiceImpl.setPromotionDAO(new MockPromotionDAO());
		promotionServiceImpl.setActivityDAO(new MockActivityDAO());
		promotionServiceImpl.setMemberDAO(new MockMemberDAO());
		
		promotionService = promotionServiceImpl;
	}

	protected void tearDown() throws Exception
	{
		super.tearDown();
	}
	
	public void testNextPromotion()
	{
		Promotion promotion = promotionService.nextPromotionForMember("test");
		
		assertNotNull(promotion);
		
		promotionService.viewPromotion("test", false);
		
		Promotion promotion2 = promotionService.nextPromotionForMember("test");
		
		promotionService.viewPromotion("test", false);
		
		assertEquals(promotion, promotion2);
		
		Promotion promotion3 = promotionService.nextPromotionForMember("test");
		
		promotionService.viewPromotion("test", false);
		
		assertNotSame(promotion2, promotion3);
	}
}
