package biz.firstlook.module.promotion.dao.mock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import biz.firstlook.module.promotion.dao.ActivityDAO;
import biz.firstlook.module.promotion.model.Activity;
import biz.firstlook.module.promotion.model.ActivityTypeEnum;


public class MockActivityDAO implements ActivityDAO {
	
	private HashMap<Integer,List<Activity>> activityListsByMemberId = new HashMap<Integer,List<Activity>>(); 

	public List<Activity> getActivities(Integer promotionId, Integer memberId) {
		List<Activity> activitiesForMember = activityListsByMemberId.get(memberId);
		List<Activity> result = new ArrayList<Activity>();
		
		if( activitiesForMember != null )
		{
			Iterator<Activity> itActivity = activitiesForMember.iterator();
			while( itActivity.hasNext() )
			{
				Activity activity = itActivity.next();
				if( activity.getPromotionId() == promotionId )
					result.add(activity);
			}
		}
		
		return result;
	}

	public Integer getViews(Integer promotionId, Integer memberId) {
		int views = 0;
		
		List<Activity> activitiesForMember = activityListsByMemberId.get(memberId);
		
		if( activitiesForMember != null )
		{
			Iterator<Activity> itActivity = activitiesForMember.iterator();
			while( itActivity.hasNext() )
			{
				Activity activity = itActivity.next();
				if( activity.getPromotionId() == promotionId && activity.getTypeId() == ActivityTypeEnum.VIEW.getActivityType() )
					views++;
			}
		}
		
		return views;
	}
	
	private int lastIndex = 0;

	public Activity save(Activity activity) {
		activity.setId(lastIndex++);
		
		List<Activity> activitiesForMember = activityListsByMemberId.get(activity.getInsertUser());
		if( null == activitiesForMember )
		{
			activitiesForMember = new ArrayList<Activity>();
		}
		
		activitiesForMember.add(activity);
		activityListsByMemberId.put(activity.getInsertUser(), activitiesForMember);
		
		return activity;
	}

	public Integer getNevers(Integer promotionId, Integer memberId) 
	{
		int nevers = 0;
		
		List<Activity> activitiesForMember = activityListsByMemberId.get(memberId);
		
		if( activitiesForMember != null )
		{
			Iterator<Activity> itActivity = activitiesForMember.iterator();
			while( itActivity.hasNext() )
			{
				Activity activity = itActivity.next();
				if( activity.getPromotionId() == promotionId && activity.getTypeId() == ActivityTypeEnum.NEVER.getActivityType() )
					nevers++;
			}
		}
		
		return nevers;
	}

}
