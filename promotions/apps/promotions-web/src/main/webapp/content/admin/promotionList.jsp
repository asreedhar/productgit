<%@ include file="/include/tag-import.jsp" %>
<%@ taglib uri="/WEB-INF/taglib/struts-html.tld" prefix="html" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html:html xhtml="true">
<head>
	<title>Promotion List</title>
	<link rel="stylesheet" href="../template/Austere/css/austere.css" type="text/css" media="screen" title="no title" charset="utf-8" />
</head>
<body>
<div id="white"><div id="white_r"></div></div>    
<div id="page">
<div class="Wrapper">
	
	<div class="Navigation">
		<div id="header_r">
			<div id="header_l">
				<h1 class="title">Promotion Admin</h1>
				<p class="Shortcut">
					<c:url var="homeUrl" value="/action/list">
						<c:param name="method" value="index" />
					</c:url>
					<a href="${homeUrl}">Home</a>
				</p>
			</div>
		</div>
	</div>

	<div class="Content">
		
		<h2>Promotions</h2>
		
		<html:link action="/info?method=create">Create New Promotion</html:link>
		
		<table id="promotions_table" class="grid_view">
			<thead>
				<tr>
					<th>Name</th>
					<th>Active</th>
					<th>Priority</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th/>
				</tr>
			</thead>
			<tbody id="promotions_table_body">
				<c:forEach items="${listForm.promotions}" var="promotion">
					<tr>
						<td>${promotion.name}</td>
						<td>${promotion.enabled}</td>
						<td>${promotion.priority}</td>
						<td>${promotion.startDate}</td>
						<td>${promotion.endDate}</td>
						<c:url var="editPromo" value="/action/info">
						  <c:param name="method" value="load"/>
						  <c:param name="id" value="${promotion.promotionId}"/>
						</c:url>
						<td><a href="${editPromo}">Edit</a></td>
					</tr>
				</c:forEach>
				<c:if test="${empty listForm.promotions}">
					<tr>
						<td colspan="4">No promotions found</td>
					</tr>
				</c:if>
			</tbody>
		</table>

		<c:if test="${listForm.totalPages gt 1}">
		<!-- first, prev -->
		<c:if test="${listForm.page gt 1}">
			<c:url var="firstPage" value="/action/list">
			  <c:param name="method" value="index"/>
			  <c:param name="page" value="1"/>
			</c:url>
			<a href="${firstPage}">|&lt;</a>&nbsp;
			<c:url var="prevPage" value="/action/list">
			  <c:param name="method" value="index"/>
			  <c:param name="page" value="${listForm.page - 1}"/>
			</c:url>
			<a href="${prevPage}">&lt;</a>&nbsp;
		</c:if>
		<c:if test="${listForm.page le 1}">
			|&lt;&nbsp;
			&lt;&nbsp;
		</c:if>
		
		<!-- page -->
		Page ${listForm.page} of ${listForm.totalPages}
		&nbsp;
		
		<!-- next, last -->
		<c:if test="${listForm.page lt listForm.totalPages}">
			<c:url var="nextPage" value="/action/list">
			  <c:param name="method" value="index"/>
			  <c:param name="page" value="${listForm.page + 1}"/>
			</c:url>
			<a href="${nextPage}">&gt;</a>&nbsp;
			<c:url var="lastPage" value="/action/list">
			  <c:param name="method" value="index"/>
			  <c:param name="page" value="${listForm.totalPages}"/>
			</c:url>
			<a href="${lastPage}">&gt;|</a>&nbsp;
		</c:if>
		<c:if test="${listForm.page ge listForm.totalPages}">
			&gt;&nbsp;
			&gt;|&nbsp;
		</c:if>
		</c:if>
	</div>
</div>
</div>
</body>
</html:html>