<%@ taglib uri="/WEB-INF/taglib/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglib/struts-bean.tld" prefix="bean" %>

<html:html>
<body>
	<bean:size id="promotionCount" name="listForm" property="promotions"/>
	<span id="count">${promotionCount}</span>
</body>
</html:html>