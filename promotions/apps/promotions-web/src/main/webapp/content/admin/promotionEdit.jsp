<%@ page language="java" %>
<%@ include file="/include/tag-import.jsp" %>
<%@ taglib uri="/WEB-INF/taglib/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglib/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglib/struts-logic.tld" prefix="logic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html:html xhtml="true">
<head>
	<title>Edit Promotion</title>
	<link rel="stylesheet" href="../template/Austere/css/austere.css" type="text/css" media="screen" title="no title" charset="utf-8" />
	<link rel="stylesheet" href="../public/css/jquery-ui-1.8.5.custom.css" type="text/css" media="screen" title="no title" charset="utf-8" />
	
	<script src="../public/javascript/jquery-1.4.2.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="../public/javascript/jquery-ui-1.8.5.custom.min.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>
<div id="white"><div id="white_r"></div></div>    
<div id="page">
<div class="Wrapper">

	<div class="Navigation"><div id="header_r"><div id="header_l">
		<h1 class="title">Promotion Admin</h1>
		<p class="Shortcut">
			<c:url var="homeUrl" value="/action/list">
				<c:param name="method" value="index"/>
			</c:url>
			<a href="${homeUrl}">Home</a>
		</p>
	</div></div></div> 

	<div class="Content">

		<logic:present name="org.apache.struts.action.ERROR">
		<div class="ErrorPanel">
			<html:errors />
		</div>
		</logic:present>
		
		<html:form action="info">
		<fieldset id="edit_promotion" class="hform">
			<legend>Edit Promotion</legend>
			
			<input type="hidden" name="method" value="save"/><br/>
			<html:hidden name="infoForm" property="id" />
			<p>
				<label for="infoForm">Name: </label>
				<html:text name="infoForm" property="name" styleId="name" maxlength="40"/>
			</p>
			<p>
				<label for="description">Description:</label>
				<html:text name="infoForm" property="description" styleId="description"/>
			</p>
			<p>
			<logic:equal name="infoForm" property="startDateEnabled" value="true">
				<label for="infoForm">Start Date:</label>
				<html:text name="infoForm" property="startDate" styleId="startDate" maxlength="40"/>
			</logic:equal>
			<logic:equal name="infoForm" property="startDateEnabled" value="false">
				<html:hidden name="infoForm" property="startDate" />
				<label for="startDate">Start Date:</label>
				<bean:write name="infoForm" property="startDate"/>
			</logic:equal>
			</p>
			<p>
				<label for="endDate">End Date:</label>
				<html:text name="infoForm" property="endDate" styleId="endDate"/>
			</p>
			<p class="checkbox">
				<label for="enabled">Enabled: </label>
				<html:checkbox name="infoForm" property="enabled" styleId="enabled"/>
			</p>
			<p>
				<label for="maxViews">Max Views: </label>
				<html:text name="infoForm" property="maxViews" styleId="maxViews"/>
			</p>
			<p>
				<label for="priority">Priority: </label>
				<html:text name="infoForm" property="priority" styleId="priority"/>
			</p>
			<p>
				<label for="promotionUrl">Promotion Url: </label>
				<html:text name="infoForm" property="promotionUrl" styleId="promotionUrl"/>
			</p>
			<p>
				<label for="promotionSummaryUrl">Promotion Summary Url: </label>
				<html:text name="infoForm" property="promotionSummaryUrl" styleId="promotionSummaryUrl"/>
			</p>
			<html:hidden name="infoForm" property="insertUser" />
			<html:hidden name="infoForm" property="insertDate" />
			<p class="controls">
				<html:submit value="Save" styleClass="button"/>
				<html:cancel styleClass="button"/>
			</p>
		</fieldset>
			
		</html:form>
		<script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$("#endDate, #startDate").datepicker();
			});
			
		</script>
	</div>
</div>
</div>
</body>

</html:html>