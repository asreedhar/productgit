<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/taglib/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/taglib/struts-bean.tld" prefix="bean" %>
<%-- VARIABLE DEFINITIONS --%>
<bean:define id="summaryUrl" name="viewForm" property="promotionSummaryUrl"/>
<%-- VARIABLE DEFINITIONS --%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html:html xhtml="true">
<head>
	<title>Promotion Summary</title>
	<link rel="stylesheet" href="../public/css/base.css" type="text/css" media="screen" title="no title" charset="utf-8" />
	<!--[if lte IE 6]>
	<link rel="stylesheet" href="../public/css/ie.css" type="text/css" media="screen" title="no title" charset="utf-8" />	
	<![endif]-->
</head>
<body>
<div id="page" class="vform">
	
	<bean:define id="service" name="viewForm" property="service"/>
	
	<img src="${summaryUrl}" alt="" class="promotion_summary_image" />
	
	<div class="buttons">
	
	<html:form action="view" styleClass="skip">
		<input type="hidden" name="method" value="skip"/>
		<input type="hidden" name="service" value="${service}"/>
		<input type="hidden" name="never" value="false"/>
		<input class="button" type="image" src="../public/images/no_thanks.gif" />
	</html:form>
	
	<html:form action="view" styleClass="view">
		<input type="hidden" name="method" value="view"/>
		<input type="hidden" name="service" value="${service}"/>
		<input type="hidden" name="never" value="false"/>
		<input class="button" type="image" src="../public/images/yes_i_want_to_learn_more.png" />
	</html:form>
	
	
	</div>
	
	<p class="checkbox">
		<input type="checkbox" id="neverCheckbox" name="neverCheckbox" onclick="toggleShowAgain()"/> 
		<label for="neverCheckbox">Don't show again</label>
	</p>
	
	<script type="text/javascript">
	function toggleShowAgain()
	{
		var neverCheckbox = document.getElementById("neverCheckbox");
		var neverInputs = document.getElementsByName("never");
		
		for(var i = 0; i < neverInputs.length; i++) {
			neverInputs[i].value =  neverCheckbox.checked;
		}
	}
	</script>
	
	<a id="contact_us" href="/support/Marketing_Pages/ContactUs.aspx" target="planning">Contact FirstLook</a>

</div>
</body>
</html:html>
