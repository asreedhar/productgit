package biz.firstlook.webapp.promotion.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class ServiceUrlDecoder 
{
	public static String decode(String encodedPath)
	{
		String decodedPath = "/";
		
		if( encodedPath != null && encodedPath != "" )
		{
			try
			{
				decodedPath = URLDecoder.decode(encodedPath, "UTF-8");
			}
			catch(UnsupportedEncodingException e)
			{
			}
		}
		
		return decodedPath;
	}
}
