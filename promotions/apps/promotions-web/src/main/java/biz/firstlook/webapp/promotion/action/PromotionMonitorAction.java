package biz.firstlook.webapp.promotion.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.promotion.service.PromotionService;
import biz.firstlook.webapp.promotion.form.PromotionListForm;

public class PromotionMonitorAction extends BaseAction {

	private PromotionService promotionService;

	public PromotionService getPromotionService() {
		return promotionService;
	}

	public void setPromotionService(PromotionService promotionService) {
		this.promotionService = promotionService;
	}
	
	public ActionForward count(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) 
	{
		if( !promotionService.isMemberAdmin(getUserName(request)) )
			throw new RuntimeException("No access");
		
		PromotionListForm listForm = (PromotionListForm) form;
		
		listForm.setPromotions(getPromotionService().listPromotions(0,10));
		
		return mapping.findForward("success");
	}
}
