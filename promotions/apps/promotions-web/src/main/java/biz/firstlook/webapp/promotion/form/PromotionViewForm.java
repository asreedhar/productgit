package biz.firstlook.webapp.promotion.form;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.struts.action.*;

import biz.firstlook.webapp.promotion.util.ServiceUrlDecoder;

/**
 * @author Jason Koziol
 * @version 1.0
 * @created 25-Aug-2010 12:04:14 PM
 */
public class PromotionViewForm extends ActionForm 
{
	private static final long serialVersionUID = 8877701610427613139L;

	private String promotionUrl;
	private String promotionSummaryUrl;
	private String service;
	private boolean never;
	
	public boolean isNever() {
		return never;
	}

	public void setNever(boolean never) {
		this.never = never;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) 
	{
		if( service.contains(":") || service.contains("/") || service.contains("?") || service.contains("=") || service.contains("&") )
		{
			try 
			{
				this.service = URLEncoder.encode(service, "UTF-8");
			} catch (UnsupportedEncodingException e) 
			{
				this.service = service;
			}
		}
		else
			this.service = service;
		System.out.println("PromotionViewForm.setService: " + this.service);
	}

	public String getServiceDecoded() {
		return ServiceUrlDecoder.decode(service);
	}

	public String getPromotionUrl() {
		return promotionUrl;
	}

	public void setPromotionUrl(String promotionUrl) {
		this.promotionUrl = promotionUrl;
	}

	public String getPromotionSummaryUrl() {
		return promotionSummaryUrl;
	}

	public void setPromotionSummaryUrl(String promotionSummaryUrl) {
		this.promotionSummaryUrl = promotionSummaryUrl;
	}
}