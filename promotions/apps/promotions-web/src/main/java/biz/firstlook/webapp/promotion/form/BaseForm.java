package biz.firstlook.webapp.promotion.form;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

public class BaseForm extends ValidatorForm 
{
	private static final long serialVersionUID = 5662301123170871007L;

	@Override
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		String parameterName = mapping.getParameter();
		boolean validate = false;
		if (parameterName != null) {
			String parameter = request.getParameter(parameterName);
			if (parameter != null) {
				for (String method : validateMethods()) {
					if (parameter.equals(method)) {
						validate = true;
						break;
					}
				}
			}
		}
		if (validate) {
			return super.validate(mapping, request);
		}
		else {
			return null;
		}
	}
	
	protected String[] validateMethods() {
		return new String[0];
	}
	
}
