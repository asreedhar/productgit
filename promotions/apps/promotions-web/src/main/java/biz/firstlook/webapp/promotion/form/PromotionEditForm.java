package biz.firstlook.webapp.promotion.form;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class PromotionEditForm extends BaseForm {
	private static final long serialVersionUID = 8110577538429637284L;
	private String id;
	private String name;
	private String description;
	private String startDate;
	private String endDate;
	private boolean enabled;
	private String maxViews;
	private String priority;
	private String promotionUrl;
	private String promotionSummaryUrl;
	private String insertUser;
	private String insertDate;
	private boolean startDateEnabled;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getMaxViews() {
		return maxViews;
	}

	public void setMaxViews(String maxViews) {
		this.maxViews = maxViews;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getPromotionUrl() {
		return promotionUrl;
	}

	public void setPromotionUrl(String promotionUrl) {
		this.promotionUrl = promotionUrl;
	}

	public String getPromotionSummaryUrl() {
		return promotionSummaryUrl;
	}

	public void setPromotionSummaryUrl(String promotionSummaryUrl) {
		this.promotionSummaryUrl = promotionSummaryUrl;
	}

	public String getInsertUser() {
		return insertUser;
	}

	public void setInsertUser(String insertUser) {
		this.insertUser = insertUser;
	}

	public String getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(String insertDate) {
		this.insertDate = insertDate;
	}

	public boolean isStartDateEnabled() {
		return startDateEnabled;
	}

	public void setStartDateEnabled(boolean startDateEnabled) {
		this.startDateEnabled = startDateEnabled;
	}

	@Override
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) 
	{
		ActionErrors errors = super.validate(mapping, request);
		
		if( this.startDate != null && !this.startDate.equals("") && !errors.get("startDate").hasNext() )
		{
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			
			Date startDate = formatter.parse(this.startDate,new ParsePosition(0));
			
			Calendar startCal = Calendar.getInstance();
			startCal.setTime(startDate);
			
			Calendar yesterday = Calendar.getInstance();
			yesterday.add(Calendar.DATE, -1);
			yesterday.set(Calendar.HOUR_OF_DAY, 23);
			yesterday.set(Calendar.MINUTE, 59);
			yesterday.set(Calendar.SECOND, 59);
			
			this.startDateEnabled = yesterday.before(startCal); //+++ not good to have business logic here .. should probably abstract into static method of promotion object
			
			if( this.endDate != null && !this.endDate.equals("") )
			{
				Date endDate = formatter.parse(this.endDate,new ParsePosition(0));
				
				Calendar endCal = Calendar.getInstance();
				endCal.setTime(endDate);
				
				if( startCal.after(endCal) )
				{
					errors.add("endDate", new ActionMessage("errors.daterange"));
				}
				
				Calendar maxCal = Calendar.getInstance();
				maxCal.set(Calendar.YEAR, 2038);
				maxCal.set(Calendar.MONTH, 0);
				maxCal.set(Calendar.DAY_OF_MONTH, 1);
				
				if( endCal.after(maxCal) )
				{
					errors.add("endDate", new ActionMessage("errors.maxend"));
				}
			}
		}
		else
			this.setStartDateEnabled(true);
		
		return errors;
	}

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		setEnabled(false); 
		super.reset(mapping, request);
	}

	@Override
	protected String[] validateMethods() {
		return new String[] {"save"};
	}

}
