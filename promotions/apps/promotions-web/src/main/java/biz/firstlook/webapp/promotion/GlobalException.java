package biz.firstlook.webapp.promotion;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ExceptionHandler;
import org.apache.struts.config.ExceptionConfig;

import biz.firstlook.webapp.promotion.form.PromotionViewForm;
import biz.firstlook.webapp.promotion.util.ServiceUrlDecoder;

public class GlobalException extends ExceptionHandler {

	@Override
	public ActionForward execute(Exception ex, ExceptionConfig ae,
			ActionMapping mapping, ActionForm formInstance,
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException 
	{
		ActionForward forward = null;
		
		PromotionViewForm promotionForm = null;
		
		try
		{
			promotionForm = (PromotionViewForm) formInstance;
		}
		catch (ClassCastException e)
		{
			throw new ServletException(ex.getMessage());
		}
		
		String serviceString = ServiceUrlDecoder.decode(promotionForm.getService());
		
		if( serviceString != null && serviceString != "" )
		{
			try
			{
				response.sendRedirect(serviceString);
			}
			catch(IOException e)
			{
				throw new ServletException(e.getMessage());
			}
		}
		else
		{
			forward = new ActionForward("noservice", "/content/error.jsp", true, "");
			forward.setRedirect(true);
		}
				
		return forward;
	}
}
