package biz.firstlook.webapp.promotion.action;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class BaseAction extends DispatchAction {
	
	protected String getUserName(HttpServletRequest request)
	{
		String userName = "";
		userName = request.getSession().getAttribute("com.discursive.cas.extend.client.filter.user").toString();
		try
		{
			userName = URLDecoder.decode(userName, "UTF-8");
		}
		catch(UnsupportedEncodingException e)
		{
		}
		
		return userName;
	}

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		response.addHeader( "Cache-Control", "no-cache" );
		response.addHeader( "Pragma", "no-cache" );
		return super.execute(mapping, form, request, response);
	}
}
