package biz.firstlook.webapp.promotion.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.promotion.model.Promotion;
import biz.firstlook.module.promotion.service.PromotionService;
import biz.firstlook.webapp.promotion.form.PromotionViewForm;

/**
 * @author Jason Koziol
 * @version 1.0
 * @created 25-Aug-2010 12:04:14 PM
 */
public class PromotionViewAction extends BaseAction {

	private PromotionService promotionService;

	public PromotionService getPromotionService() {
		return promotionService;
	}

	public void setPromotionService(PromotionService promotionService)
	{
		this.promotionService = promotionService;
	}

	/**
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 */
	public ActionForward next(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
	{
		ActionForward forward = mapping.findForward("promotion");
		
		PromotionViewForm promotionForm = (PromotionViewForm) form;
		
		promotionForm.setPromotionUrl( "" );
		
		Promotion promotion = promotionService.nextPromotionForMember(getUserName(request));
		
		if( promotion != null )
		{
			promotionForm.setPromotionSummaryUrl( promotion.getPromotionSummaryUrl() ); 
			promotionForm.setPromotionUrl( promotion.getPromotionUrl() );
			
			forward = mapping.findForward("summary");
		}
		
		return forward;
	}

	/**
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 */
	public ActionForward skip(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
	{
		PromotionViewForm promotionForm = (PromotionViewForm) form;
		
		promotionForm.setPromotionUrl( "" );
		
		//String serviceString = ServiceUrlDecoder.decode(promotionForm.getService());
		
		//promotionForm.setService(serviceString);
		
		promotionService.skipPromotion(getUserName(request), promotionForm.isNever());
		
		return mapping.findForward("promotion");
	}

	/**
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 */
	public ActionForward view(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
	{
		Promotion promotion = promotionService.nextPromotionForMember(getUserName(request));
		
		PromotionViewForm promotionForm = (PromotionViewForm) form;
		
		promotionForm.setPromotionUrl( promotion.getPromotionUrl() );
		
		//String serviceString = ServiceUrlDecoder.decode(promotionForm.getService());
		
		//promotionForm.setService(serviceString);
		
		promotionService.viewPromotion(getUserName(request), promotionForm.isNever());
		
		return mapping.findForward("promotion");
	}
}