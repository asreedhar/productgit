package biz.firstlook.webapp.promotion.action;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.promotion.model.Promotion;
import biz.firstlook.module.promotion.service.PromotionService;
import biz.firstlook.webapp.promotion.form.PromotionEditForm;

/**
 * @author Jason Koziol
 * @version 1.0
 * @created 25-Aug-2010 12:04:14 PM
 */
public class PromotionEditAction extends BaseAction {
	private PromotionService promotionService;

	public PromotionService getPromotionService() {
		return promotionService;
	}

	public void setPromotionService(PromotionService promotionService) {
		this.promotionService = promotionService;
	}

	/**
	 * 
	 * @param response
	 * @param request
	 * @param form
	 * @param mapping
	 */
	public ActionForward create(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		
		if( !promotionService.isMemberAdmin(getUserName(request)) )
			throw new RuntimeException("No access");
		
		PromotionEditForm promotionForm = (PromotionEditForm) form;

		Calendar calendar = Calendar.getInstance();
		Date startDate = calendar.getTime();

		calendar.set(Calendar.YEAR, 2038);
		calendar.set(Calendar.MONTH,0);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		Date endDate = calendar.getTime();

		Promotion promotion = new Promotion();

		promotion.setName("");
		promotion.setDescription(startDate.toString());
		promotion.setStartDate(startDate);
		promotion.setEndDate(endDate);
		promotion.setEnabled(false);
		promotion.setMaxViews(3);
		promotion.setPriority(1);
		promotion.setPromotionUrl("http://www.firstlook.biz");
		promotion.setPromotionSummaryUrl("http://www.firstlook.biz");

		populateForm(promotionForm, promotion);

		return mapping.findForward("loaded");
	}

	/**
	 * 
	 * @param response
	 * @param request
	 * @param form
	 * @param mapping
	 */
	public ActionForward load(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		if( !promotionService.isMemberAdmin(getUserName(request)) )
			throw new RuntimeException("No access");
		
		String forwardName = "failure";

		PromotionEditForm promotionForm = (PromotionEditForm) form;

		String promotionIdStr = promotionForm.getId();

		if (null != promotionIdStr && "" != promotionIdStr) {
			try {
				int promotionId = 0;

				promotionId = Integer.parseInt(promotionIdStr);

				Promotion promotion = promotionService.loadPromotion(promotionId);

				populateForm(promotionForm, promotion);

				forwardName = "loaded";
			}
			catch (NumberFormatException e) {
				forwardName = "failure";
			}
		}

		return mapping.findForward(forwardName);
	}

	/**
	 * 
	 * @param response
	 * @param request
	 * @param form
	 * @param mapping
	 */
	public ActionForward save(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		
		if( !promotionService.isMemberAdmin(getUserName(request)) )
			throw new RuntimeException("No access");
		
		String forwardName = "failure";

		PromotionEditForm promotionForm = (PromotionEditForm) form;

		if (isCancelled(request)) {
			forwardName = "cancel";
		}
		else {
			
			ActionErrors errors = promotionForm.validate(mapping, request);
			if (null == errors || errors.isEmpty()) {
				Promotion promotion = new Promotion();
				populateObject(promotionForm, promotion);
				getPromotionService().savePromotion(promotion, getUserName(request));
				forwardName = "saved";
			}
		}

		return mapping.findForward(forwardName);
	}

	private void populateForm(PromotionEditForm form, Promotion promotion) {
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		Integer promotionId = promotion.getPromotionId();
		if( null != promotionId )
			form.setId(Integer.toString(promotionId));
		form.setName(promotion.getName());
		form.setDescription(promotion.getDescription());
		form.setStartDate(formatter.format(promotion.getStartDate()));
		form.setEndDate(formatter.format(promotion.getEndDate()));
		form.setEnabled(promotion.getEnabled());
		form.setMaxViews(Integer.toString(promotion.getMaxViews()));
		form.setPriority(Integer.toString(promotion.getPriority()));
		form.setPromotionUrl(promotion.getPromotionUrl());
		form.setPromotionSummaryUrl(promotion.getPromotionSummaryUrl());
		form.setStartDateEnabled(!promotion.hasStarted());
		if( promotion.getInsertUser() != null )
		{
			form.setInsertUser(Integer.toString(promotion.getInsertUser()));
			formatter = new SimpleDateFormat("yyyyMMddhhmmss");
			form.setInsertDate(formatter.format(promotion.getInsertDate()));
		}
	}

	private void populateObject(PromotionEditForm form, Promotion promotion) {
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		formatter.setLenient(true);
		try {
			String promotionId = form.getId();
			if( null != promotionId && promotionId.length() != 0 )
			{
				promotion.setPromotionId(Integer.parseInt(promotionId));
			}
			
			promotion.setName(form.getName());
			promotion.setDescription(form.getDescription());
			if( !promotion.hasStarted() )
			{
				promotion.setStartDate(formatter.parse(form.getStartDate(),new ParsePosition(0)));
			}
			
			Date endDate = formatter.parse(form.getEndDate(),new ParsePosition(0));
			
			Calendar endCal = Calendar.getInstance();
			endCal.setTime(endDate);
			endCal.set(Calendar.HOUR_OF_DAY, 23);
			endCal.set(Calendar.MINUTE, 59);
			endCal.set(Calendar.SECOND, 59);
			promotion.setEndDate(endCal.getTime());
			
			promotion.setEnabled(Boolean.valueOf(form.isEnabled()));
			promotion.setMaxViews(Integer.parseInt(form.getMaxViews()));
			promotion.setPriority(Integer.parseInt(form.getPriority()));
			promotion.setPromotionUrl(form.getPromotionUrl());
			promotion.setPromotionSummaryUrl(form.getPromotionSummaryUrl());
			if( form.getInsertUser() != null && form.getInsertUser() != "" )
			{
				promotion.setInsertUser(Integer.parseInt(form.getInsertUser()));
				formatter = new SimpleDateFormat("yyyyMMddhhmmss");
				promotion.setInsertDate(formatter.parse(form.getInsertDate(),new ParsePosition(0)));
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}