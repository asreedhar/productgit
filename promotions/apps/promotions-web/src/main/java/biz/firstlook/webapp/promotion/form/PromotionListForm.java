package biz.firstlook.webapp.promotion.form;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.promotion.model.Promotion;

public class PromotionListForm extends ActionForm 
{
	private static final long serialVersionUID = -2255454556449118407L;
	
	private List<Promotion> promotions;
	private String page;
	private String totalPages;

	public List<Promotion> getPromotions() {
		return promotions;
	}

	public void setPromotions(List<Promotion> promotions) {
		this.promotions = promotions;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(String totalPages) {
		this.totalPages = totalPages;
	}

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		promotions = new ArrayList<Promotion>();
		super.reset(mapping, request);
	}
}
