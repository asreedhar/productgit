package biz.firstlook.webapp.promotion.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.promotion.service.PagedPromotionList;
import biz.firstlook.module.promotion.service.PromotionService;
import biz.firstlook.webapp.promotion.form.PromotionListForm;

/**
 * @author Jason Koziol
 * @version 1.0
 * @created 25-Aug-2010 12:04:14 PM
 */
public class PromotionListAction extends BaseAction {
	
	static final int NUM_PER_PAGE = 25;
	
	private PromotionService promotionService;

	public PromotionService getPromotionService() {
		return promotionService;
	}

	public void setPromotionService(PromotionService promotionService) {
		this.promotionService = promotionService;
	}
	
	public ActionForward index(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) 
	{
		String userName = getUserName(request);
		promotionService.isMemberAdmin(userName);
		if( !promotionService.isMemberAdmin(getUserName(request)) )
			throw new RuntimeException("No access");
		
		PromotionListForm listForm = (PromotionListForm) form;
		
		String pageStr = listForm.getPage();
		if( pageStr == null || "" == pageStr )
			pageStr = "1";
		
		int page = 1;
		
		try
		{
			page = Integer.parseInt(pageStr);
		}
		catch(NumberFormatException e)
		{
			page = 1;
		}
		
		PagedPromotionList promotions = getPromotionService().listPromotions(page, NUM_PER_PAGE); 
		
		listForm.setPromotions(promotions);
		listForm.setPage(promotions.getPage().toString());
		listForm.setTotalPages(promotions.getTotalPages().toString());

		return mapping.findForward("success");
	}
}