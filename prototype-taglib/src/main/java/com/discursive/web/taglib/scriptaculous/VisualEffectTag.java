package com.discursive.web.taglib.scriptaculous;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class VisualEffectTag extends TagSupport {

	private static final long serialVersionUID = 610553431071868914L;

	private String elementId;
	private String revert;
	private String handleId;

	@Override
	public int doStartTag() throws JspException {
		throw new RuntimeException( "Not Implemented" );
	}

	public String getElementId() {
		return elementId;
	}

	public void setElementId(String elementId) {
		this.elementId = elementId;
	}

	public String getHandleId() {
		return handleId;
	}

	public void setHandleId(String handleId) {
		this.handleId = handleId;
	}

	public String getRevert() {
		return revert;
	}

	public void setRevert(String revert) {
		this.revert = revert;
	}


}
