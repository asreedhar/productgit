package com.discursive.web.taglib.prototype;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

/**
 * A java tag wrapping prototype.js <code>Form.Element.Observer</code> class,
 * but modelled after RoR's PrototypeHelper.
 * 
 * @author tobrien
 * 
 */
public class ObserveFieldTag extends TagSupport {

    private static final long serialVersionUID = -5610355136502589770L;

    /**
     * DOM element object or id
     */
    private String field;

    /**
     * The frequency (in seconds) at which changes to this field will be
     * detected. Not setting this option at all or to a value equal to or less
     * than zero will use event based observation instead of time based
     * observation.
     */
    private String frequency;

    /**
     * This can be the id of an element, the element object itself, or an object
     * with two properties - object.success element (or id) that will be used
     * when the AJAX call succeeds, and object.failure element (or id) that will
     * be used otherwise.
     */
    private String update;
    
    /**
     * The url to be fetched
     */
    private String href;

    @Override
    public int doStartTag() throws JspException {

	VelocityContext context = new VelocityContext();
	context.put("field", field);
	context.put("frequency", frequency);
	context.put("update", update);
	context.put("href", href);

	InputStream template = getClass().getResourceAsStream(
		"observeFieldStart.template");
	InputStreamReader templateReader = new InputStreamReader(template);

	StringWriter writer = new StringWriter();

	try {
	    Velocity.init();
	    Velocity.evaluate(context, writer, "Observe Field Start",
		    templateReader);
	    pageContext.getOut().append(writer.toString());
	} catch (Exception e) {
	    throw new JspException(e);
	}

	return TagSupport.EVAL_BODY_INCLUDE;
    }

    public String getField() {
	return field;
    }

    public void setField(String field) {
	this.field = field;
    }

    public String getFrequency() {
	return frequency;
    }

    public void setFrequency(String frequency) {
	this.frequency = frequency;
    }

    public String getHref() {
	return href;
    }

    public void setHref(String href) {
	this.href = href;
    }

    public String getUpdate() {
	return update;
    }

    public void setUpdate(String update) {
	this.update = update;
    }

}
