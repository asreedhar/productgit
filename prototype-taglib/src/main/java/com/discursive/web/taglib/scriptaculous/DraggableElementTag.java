package com.discursive.web.taglib.scriptaculous;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

public class DraggableElementTag extends TagSupport {

	private static final long serialVersionUID = 610553431071868914L;

	private String elementId;
	private String revert;
	private String handleId;

	@Override
	public int doStartTag() throws JspException {

		VelocityContext context = new VelocityContext();
		context.put( "elementId", elementId );
		context.put( "revert", revert );
		context.put( "handleId", handleId );
		
		InputStream template = getClass().getResourceAsStream("draggableElementStart.template");
		InputStreamReader templateReader = new InputStreamReader( template );
		
		StringWriter writer = new StringWriter();
		
		try {
			Velocity.init();
			Velocity.evaluate( context, writer, "Draggable Element Start", templateReader );

			pageContext.getOut().append( writer.toString() );
		} catch( Exception e ) {
			throw new JspException( e );
		}

		return TagSupport.EVAL_BODY_INCLUDE;
	}

	public String getElementId() {
		return elementId;
	}

	public void setElementId(String elementId) {
		this.elementId = elementId;
	}

	public String getHandleId() {
		return handleId;
	}

	public void setHandleId(String handleId) {
		this.handleId = handleId;
	}

	public String getRevert() {
		return revert;
	}

	public void setRevert(String revert) {
		this.revert = revert;
	}


}
