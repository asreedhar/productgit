package com.discursive.web.taglib.prototype;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

public class FormRemoteTag extends TagSupport {

	private static final long serialVersionUID = -8962095528918324109L;

	private String href;
	private String update;
	private String id;
	private String name;
	private String cssClass;
	
	@Override
	public int doEndTag() throws JspException {
		VelocityContext context = new VelocityContext();
		context.put( "href", href );
		context.put( "update", update );
		context.put( "id", id );
		context.put( "name", name );
		context.put( "cssClass", cssClass );
		
		InputStream template = getClass().getResourceAsStream("formRemoteEnd.template");
		Reader templateReader = new InputStreamReader( template );
		
		StringWriter writer = new StringWriter();
		
		try {
			Velocity.init();
			Velocity.evaluate( context, writer, "Form Remote End", templateReader );

			pageContext.getOut().append( writer.toString() );
		} catch( Exception e ) {
			throw new JspException( e );
		}

		return super.doEndTag();
	}

	@Override
	public int doStartTag() throws JspException {

		VelocityContext context = new VelocityContext();
		context.put( "href", href );
		context.put( "update", update );
		context.put( "id", id );
		context.put( "name", name );
		context.put( "cssClass", cssClass );
		
		InputStream template = getClass().getResourceAsStream("formRemoteStart.template");
		Reader templateReader = new InputStreamReader( template );
		
		StringWriter writer = new StringWriter();
		
		try {
			Velocity.init();
			Velocity.evaluate( context, writer, "Form Remote Start", templateReader );
			pageContext.getOut().append( writer.toString() );
		} catch( Exception e ) {
			throw new JspException( e );
		}

		return TagSupport.EVAL_BODY_INCLUDE;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getUpdate() {
		return update;
	}

	public void setUpdate(String update) {
		this.update = update;
	}

	public String getCssClass() {
		return cssClass;
	}

	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}
