package com.discursive.web.taglib.prototype;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class PeriodicallyCallRemoteTag extends TagSupport {

	private static final long serialVersionUID = 3155732971108517343L;

	private String frequency;
	private String update;
	private String href;
	
	@Override
	public int doStartTag() throws JspException {
		throw new RuntimeException( "Not implemented" );
	}

	
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public String getUpdate() {
		return update;
	}
	public void setUpdate(String update) {
		this.update = update;
	}
}
