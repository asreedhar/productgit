package com.discursive.web.taglib.prototype;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class UpdatePageTag extends TagSupport {

	private static final long serialVersionUID = -7740249023031002182L;

	private String href;
	private String update;

	@Override
	public int doEndTag() throws JspException {
		throw new RuntimeException( "Not Implemented" );
	}

	@Override
	public int doStartTag() throws JspException {
		throw new RuntimeException( "Not Implemented" );
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getUpdate() {
		return update;
	}

	public void setUpdate(String update) {
		this.update = update;
	}

}
