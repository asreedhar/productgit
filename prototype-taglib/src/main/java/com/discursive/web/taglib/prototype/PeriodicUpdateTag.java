package com.discursive.web.taglib.prototype;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

public class PeriodicUpdateTag extends TagSupport {

	private static final long serialVersionUID = 3155732971108517343L;

	private String frequency;
	private String update;
	private String href;
	
	@Override
	public int doStartTag() throws JspException {

		VelocityContext context = new VelocityContext();
		context.put( "frequency", frequency );
		context.put( "update", update );
		context.put( "href", href );
		
		InputStream template = getClass().getResourceAsStream("periodicUpdateStart.template");
		InputStreamReader templateReader = new InputStreamReader( template );
		
		StringWriter writer = new StringWriter();
		
		try {
			Velocity.init();
			Velocity.evaluate( context, writer, "Periodic Update Start", templateReader );
			pageContext.getOut().append( writer.toString() );
		} catch( Exception e ) {
			throw new JspException( e );
		}

		return TagSupport.EVAL_BODY_INCLUDE;
	}

	
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public String getUpdate() {
		return update;
	}
	public void setUpdate(String update) {
		this.update = update;
	}
}
