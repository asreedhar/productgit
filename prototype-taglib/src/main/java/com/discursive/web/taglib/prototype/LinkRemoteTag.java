package com.discursive.web.taglib.prototype;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

public class LinkRemoteTag extends TagSupport {

	private static final long serialVersionUID = -7740249023031002182L;

	private String href;
	private String update;

	private String loading;
//	private String loaded;
//	private String interactive;
//	private String success;
//	private String failure;
	private String complete;
	private String confirm;
	private String cssClass;
	
	@Override
	public int doEndTag() throws JspException {
		VelocityContext context = new VelocityContext();
		context.put( "href", href );
		context.put( "update", update );
		context.put( "complete", complete );
		context.put( "loading", loading );
		context.put( "confirm", confirm );
		context.put( "cssClass", cssClass );
		
		InputStream template = getClass().getResourceAsStream("linkRemoteEnd.template");
		Reader templateReader = new InputStreamReader( template );
		
		StringWriter writer = new StringWriter();
		
		try {
			Velocity.init();
			Velocity.evaluate( context, writer, "Link Remote End", templateReader );

			pageContext.getOut().append( writer.toString() );
		} catch( Exception e ) {
			throw new JspException( e );
		}

		return super.doEndTag();
	}

	@Override
	public int doStartTag() throws JspException {

		VelocityContext context = new VelocityContext();
		context.put( "href", href );
		context.put( "update", update );
		context.put( "complete", complete );
		context.put( "loading", loading );
		
		InputStream template = getClass().getResourceAsStream("linkRemoteStart.template");
		Reader templateReader = new InputStreamReader( template );
		
		StringWriter writer = new StringWriter();
		
		try {
			Velocity.init();
			Velocity.evaluate( context, writer, "Link Remote Start", templateReader );
			pageContext.getOut().append( writer.toString() );
		} catch( Exception e ) {
			throw new JspException( e );
		}

		return TagSupport.EVAL_BODY_INCLUDE;
	}

	public String getComplete() {
		return complete;
	}

	public void setComplete(String complete) {
		this.complete = complete;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getLoading() {
		return loading;
	}

	public void setLoading(String loading) {
		this.loading = loading;
	}

	public String getUpdate() {
		return update;
	}

	public void setUpdate(String update) {
		this.update = update;
	}

	public String getConfirm() {
		return confirm;
	}

	public void setConfirm(String confirm) {
		this.confirm = confirm;
	}

	public String getCssClass() {
		return cssClass;
	}

	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}
	
	

}
