"""Unit test for configureRepositoryxml.py"""

import unittest
from fileConfigurator import FileConfigurator

class CreateDictionary(unittest.TestCase):
	
	def setUp(self):
		self.file = "database=IMT3\nuser=FirstLook\npassword=passw0rd\n"
		self.conf = FileConfigurator()
		self.conf.clearDictionary()
		
	def testAddToDictionary(self):
		expected = {"database":"IMT3", "user":"FirstLook", "password":"passw0rd"}
		self.conf.addToDictionary( "database=IMT3" )
		self.conf.addToDictionary( "user=FirstLook" )
		self.conf.addToDictionary( "password=passw0rd" )
		self.assertEqual( expected, self.conf.getDictionary() )
	
	def testAddLinesToDictionary(self):	
		expected = {"database":"IMT3", "user":"FirstLook", "password":"passw0rd"}
		self.conf.addLinesToDictionary( self.file )
		self.assertEqual( expected, self.conf.getDictionary() )		
	
	def testAddLinesToDictionaryWithSpaces(self):	
		expected = {"database":"IMT3", "user":"FirstLook", "password":"passw0rd"}
		file = "database = IMT3\n user=FirstLook \npassword= passw0rd        \n"
		self.conf.addLinesToDictionary( self.file )	
		self.assertEqual( expected, self.conf.getDictionary() )		
		
	def testLookupSimpleValue(self):
		self.conf.addLinesToDictionary( self.file )
		expected = "IMT3"
		self.assertEqual( expected, self.conf.lookupValue( "database", "prefix" ) )
		
	def testSubstitute(self):
		text="the database is [database] and is running on [dbServer]"
		expectedMetrics="the database is METRICS3 and is running on PLUTODEV"
		file = "database.imt=IMT3\ndatabase.analytics=ANALYTICS3\ndatabase.metrics=METRICS3\ndbServer=PLUTODEV"
		self.conf.addLinesToDictionary( file )
		self.assertEqual( expectedMetrics, self.conf.substitute( text, "metrics" ) )
	
	def testSubstituteNoSuffix(self):
		text="the database is [database] and is running on [dbServer]"
		expected="the database is test and is running on PLUTODEV"
		file = "dbServer=PLUTODEV\ndatabase=test"
		self.conf.addLinesToDictionary( file )
		self.assertEqual( expected, self.conf.substitute( text ) )
		
	def testSubstituteDotInKey(self):
		text = "the database is [database.metrics] and is running on [dbServer]"
		expected = "the database is METRICS and is running on PLUTODEV"
		self.conf.dictionary["dbServer"] = "PLUTODEV"
		self.conf.dictionary["database.metrics"] = "METRICS"
		self.assertEqual( expected, self.conf.substitute( text ) )
		
	def testRemoveBrackets(self):
		list = ["[test1]", "[test2]", "test3"]
		expected = ["test1", "test2", "test3"]
		self.assertEqual( expected, self.conf.removeBrackets(list) )
		
				
def suite():
	return unittest.makeSuite(CreateDictionary, "test")
		
if __name__ == "__main__":
	unittest.main()