import sys
from os import path
from fileConfigurator import FileConfigurator
from glob import glob

suffixes = {"":""}

def readFile( fileName ):
	file = open( fileName, 'r' )
	text = file.read()
	file.close()
	return text
	
def outputFile( fileName, text):
	outputFile = open( fileName, "w" )
	outputFile.write( text )
	outputFile.flush()
	outputFile.close()
	
def createOutputFileName( dir, templateName, suffix ):
	file = path.basename( templateName )
	file = file.split( ".template" )[0]
	if len(suffix) > 0:
		fileParts = file.split( "." )
		file = fileParts[0] + "_" + suffix + "." + fileParts[1]
	dir = dir + path.sep
	return dir + file			
	
def substitute( fileName, text ):
	index = fileName.rindex( path.sep )
	baseName = fileName[index + 1:]
	if( baseName.startswith( "repository" ) ):
		for suffix in suffixes:
			substituteWithSuffix( file, text, suffix )
	else:
		substituteWithSuffix( file, text, "" )
		
def substituteWithSuffix( file, text, suffix ):
	text = readFile( file )
	substitutedText = conf.substitute( text, suffix )
	outputFile( createOutputFileName( outputDir, file, suffix ), substitutedText )	
	
if __name__ == '__main__':
	conf = FileConfigurator()
	
	if len( sys.argv ) < 4:
		print 	"usage: python buildConfigFiles.py dictionaryFile templateDir outputDir dbPassword dbNumber"
	else:
		dictionaryString = readFile(sys.argv[1])
		conf.addLinesToDictionary( dictionaryString )
		
		templateFiles = glob( sys.argv[2] + "/*.template" )
		outputDir=sys.argv[3]
		
		for i in range( 4, len( sys.argv ) ):
			conf.addToDictionary( sys.argv[i] )
				
		for file in templateFiles:
			text = readFile( file )
			substitute( file, text )
