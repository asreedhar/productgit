function stripCents(price)
{
	if (undefined == price) return;
	
	var indexOfPeriod = price.value.indexOf(".");
	if (indexOfPeriod >= 0)
	{
		price.value = price.value.substring(0, indexOfPeriod);
	}
}
function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}
function formatPriceVal(price){
	
	var retStr = '';
	price = price.toString();
	price = stripCommas(price);
	retStr = addCommas(price);

	retStr = '$'+retStr;
	return retStr;

}
function stripCommas(price)
{
	if(price.indexOf('$') != -1){
	
		price = price.replace('$','');
	}
	if(price.indexOf(',') != -1){
	
		price = price.replace(',','');
	}
	if(price.indexOf('.') != -1){
	
		price = price.replace('.','');
	}
	return price;
}
