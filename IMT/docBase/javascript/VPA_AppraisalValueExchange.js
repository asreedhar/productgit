var pollingId;

function updateChildAppraisalValue(child) {	
	if(child != null && child.pageLoadStatus ==true && child.VPA) {
		// stop polling this function
		clearInterval(pollingId);

		var appVal = 0;
		if (document.getElementById('newBumpValue') != null && document.getElementById('newBumpValue').value.length > 0) {
			appVal = document.getElementById('newBumpValue').value;
		}else if (document.getElementById('initialAppraisalValue') != null && document.getElementById('initialAppraisalValue').value != null && document.getElementById('initialAppraisalValue').value.length > 0) { // for ta
			appVal = document.getElementById('initialAppraisalValue').value.replace(/[^0-9]/g,"");
		}else if (document.getElementById('appraisalValue_1') != null) { // for tm
			appVal = document.getElementById('appraisalValue_1').innerHTML.replace(/[^0-9]/g,"");
		} 
		
		if (appVal != null) {
			child.VPA.UpdateAppraisalValue(this, appVal);
		}		
	}
}

function updateAppraisalValue(aV) {
	var updated = false;

	//	don't update if its the same or useless value
	var curAppVal;
	if (aV == null || aV <= 0) {
		return;
	}
	if (document.getElementById('appraisalValue_1') != null && document.getElementById('appraisalValue_1').innerHTML != null) {
	 	curAppVal = document.getElementById('appraisalValue_1').innerHTML.replace(/[^0-9]/g,"");
	} else if (document.getElementById('initialAppraisalValue') != null && document.getElementById('initialAppraisalValue').value != null) {
		curAppVal = document.getElementById('initialAppraisalValue').value.replace(/[^0-9]/g,"");
	}
	if (aV == curAppVal) {
		return; 
	}
	
	// update value
	if (document.getElementById('newBumpValue') != null) {
		document.getElementById('newBumpValue').value = aV;
		updated = true;
	} else if (document.getElementById('initialAppraisalValue') != null) {
		document.getElementById('initialAppraisalValue').value = aV;
		updated = true;
	}
	
	// alert user
	if (updated) {
		alert('An appraisal value has been populated from another page. This value is not yet saved. Please save by clicking \'Update Appraisal\'');
	}
}

function manageVPAChild(child) {
	// wait for the child window to open. save interval id to gloabl so delgate function can clear	
	pollingId = setInterval('updateChildAppraisalValue(child)', 1000);
}
