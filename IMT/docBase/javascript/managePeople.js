var PersonManager = (function() {

	function PersonManager() {}
	PersonManager.pos;
	PersonManager.show = function(pos) {
    	PersonManager.pos = pos || PersonManager.pos;
        showPersonManager(pos);
	};
	PersonManager.close = closePersonManager;
	PersonManager.validate = validatePerson;
	PersonManager.addPerson = showAdd;
	PersonManager.save = function(url) {
		var isValid=validatePerson(url)
		if (isValid) {
			managePerson(url,true,PersonManager.close);
		};
		return isValid;
	};
	PersonManager.saveAndAddAnother = function(url) {
		if (validatePerson(url)) {
			managePerson(url,false,PersonManager.addPerson);
		};
	};
	PersonManager.edit = function(url) {
		/*if (validatePerson(url)) {
			managePerson(url,true);
		};*/
		managePerson(url,true);
	};
	PersonManager.cancel = function(url) {
		managePerson(url,'false');
	
	};
	PersonManager.remove = function(url) {
		confirmRemove(url);
		updateDropDown('','',PersonManager.pos);
	};
	PersonManager.sort = function(col,dir) {
		sortPeople(col,PersonManager.pos,dir);
	};
	PersonManager.select = function(personId) {
		selectPerson(personId,PersonManager.pos);
		showAddLink();
	    
	};
	PersonManager.update = updateTradeAnalyzerForm;
	
	var lastElements = null;
	var sortedElement = null;
	var managePers = new Popup();
		
	// Opens the person manager for the given position (appraiser or salesperson).
	function showPersonManager(pos, bOnTa, callback) {
		if($('groupAppraisals')) {
			$('groupAppraisals').style.display = 'none';
		}
		
	 	var pars = 'pos=' + pos;
		new Ajax.Request(
				'/IMT/ManagePeopleDisplayAction.go',
				 { method: 'get', parameters: pars, 
					onComplete: function(xhr){ 	
					$('personManager').innerHTML = xhr.responseText;
					PersonManagerPopup(true);
					var dropDown = getDropDown(pos);
					if (dropDown) {
						var selected = dropDown.options[dropDown.selectedIndex].value || '';
						if (dropDown.options.length == 1) {
							showAdd();
						};
						if (selected != '') {
							selectPerson(selected,pos);
						}
						Event.observe(dropDown, 'change', updatePersonManager);
					};								
					if (callback != undefined) callback();
					
				},
					onFailure:function(xhr){
						alert('The system has experienced a problem processing your last request.\nIf the problem persists, please contact your account manager or call Customer Service at 1-877-378-5665.');
					}
			});
	}
	
	function closePersonManager() {
		managePers.popup_exit();
	}
	
	// Shows the add person section of the person manager.
	function isCurrentlyEditing() {
		var retVal = false;
		var people = document.getElementById('people');
		var rows = people.getElementsByTagName('tr');
		for (var i = rows.length - 1; i >= 0; i--){		
			if (rows[i].className.indexOf('editing') !== -1 && rows[i].id != 'addPersonRow') {			
				retVal = true;
			}
		};
		return retVal;
	}
	
	function showAdd() {
		if (isCurrentlyEditing()) {
			var cancleUrl = "/IMT/ManagePeopleDisplayAction.go?editMode=false";
			managePerson(cancleUrl,false,showAdd);
			return;
		};
		var sortLinks = document.getElementById('people').getElementsByTagName('thead')[0].getElementsByTagName('a');
		for(i=0;i<sortLinks.length;i++){
			sortLinks[i].onclick = retFalse;
		}
		
		var inputs = Form.getInputs($('peopleForm'), 'text');
		for(i=0;i<inputs.length;i++) {
			inputs[i].disabled = false;
			inputs[i].onkeydown = onInputKeyDown;
		}
		
		clearPersonManager();
		
		var addPersonRow = document.getElementById('addPersonRow');
		if (addPersonRow.style && addPersonRow.style.display && addPersonRow.style.display == 'none') {
			addPersonRow.style.display = '';
			inputs[0].focus();
			inputs[0].select();
		} else {
			addPersonRow.style.display = 'none';
		}
		
		showSaveAndAddLink();
	}
	
	function showSaveAndAddLink() {
		var addButton = document.getElementById('addButton'),
			addButtonParent,
			saveAndAddAnotherButton = document.getElementById('SaveAndAddAnotherButton'),
			saveAndAddAnotherButtonParent;
		if (addButton && saveAndAddAnotherButton) {
			addButtonParent = addButton.parent || addButton.parentElement;
			saveAndAddAnotherButtonParent = saveAndAddAnotherButton.parent || saveAndAddAnotherButton.parentElement;
			if (addButtonParent && saveAndAddAnotherButtonParent) {
				addButtonParent.style.display = "none";
				saveAndAddAnotherButtonParent.style.display = "block";
			};
		};
	};
		function showAddLink() {
		var addButton = document.getElementById('addButton'),
			addButtonParent,
			saveAndAddAnotherButton = document.getElementById('SaveAndAddAnotherButton'),
			saveAndAddAnotherButtonParent;
		if (addButton && saveAndAddAnotherButton) {
			addButtonParent = addButton.parent || addButton.parentElement;
			saveAndAddAnotherButtonParent = saveAndAddAnotherButton.parent || saveAndAddAnotherButton.parentElement;
			if (addButtonParent && saveAndAddAnotherButtonParent) {
				addButtonParent.style.display = "block";
				saveAndAddAnotherButtonParent.style.display = "none";
			};
		};
	};
	
	function onInputKeyDown(event) {
		var event = event || window.event;
		var code = event.charCode || event.keyCode;
		switch(code) {
			case 13: // Enter
				var target = event.target || event.srcElement;
				if (target.parentElement && target.parentElement.parentElement) {
					var buttons = target.parentElement.parentElement.getElementsByTagName('button');
					for (var i = buttons.length - 1; i >= 0; i--){
						if (buttons[i].name == 'save') {
							buttons[i].click();
							closePersonManager();
						}
					};
				}			
			break;
			case 27: // Esc
				closePersonManager();		
			break;
		}
	}
		
	function retFalse() {
		return false;
	}
	
	function confirmRemove(url) {		
		var remove = confirm('Are you sure you wish to delete this user\n');
		if (remove) {
			managePerson(url,'true');
		} 
	}
	
	function validatePerson(url) {
		var inputs = Form.getInputs($('peopleForm'), 'text');
		
		var firstNameFilter = /^.*firstName$/;
		var phoneFilter = /^.*phoneNumber$/;
		var emailFilter = /^.*email$/;
		var lastNameFilter = /^.*lastName$/;
		var nameValid = true;
		var emailValid = true;
		var phoneValid = true;
		var lastnameValid = true;
		
		for(i=0;i<inputs.length;i++) {
			if (!inputs[i].disabled && inputs[i].type == 'text' ) {
			
			/*inputs[i].value = inputs[i].value.trim(inputs[i].value);*/
				
				if (firstNameFilter.test(inputs[i].name)) {
					inputs[i].value = inputs[i].value.replace(/^\s+|\s+$/g,'');
					nameValid = validateRequired(inputs[i], 'First name') && validateLength(inputs[i], 0, 45) &&  validateforDoublequotes(inputs[i] , 'First name');	
					
				}
				else if (lastNameFilter.test(inputs[i].name)){
					inputs[i].value = inputs[i].value.replace(/^\s+|\s+$/g,'');
					lastnameValid = validateforDoublequotes(inputs[i], 'Last name');
				}  
				else if (emailFilter.test(inputs[i].name)) {
					emailValid = validateEmail(inputs[i]);
				}
				else if (phoneFilter.test(inputs[i].name)) {
					phoneValid = validatePhone(inputs[i]);
				}
			}
		}
		return (nameValid && emailValid && phoneValid && lastnameValid);
	}
	
	function validationResponse(msg,el) {
		alert(msg);
	}
	
	function validateforDoublequotes(input,property)
	{
		var n = input.value.search('"'); 
		if (n>-1)  
		{
			validationResponse('Please enter valid ' + property + '. No double quotes allowed');
			return false;
		}
		return true;
		}	
	

    

	
	
	function validateRequired(input, property) {
		if (input.value == null || input.value == '') {
			validationResponse(property + ' is a required field.',input);
			return false;
		}
		return true;
	}
	
	function validateLength(input, min, max) {
		if (input.value == null || input.value == '') {
			return true;
		};
		if (input.value.length < min || input.value.length > max) {
			validationResponse('Must be between '+ min +' and '+ max+' characters long.');
			return false;
		};
		return true;
	}
	
	function validatePhone(phone) {
		var phoneFilter = /^(\()?([0-9]{3})(\)|-)?([0-9]{3})(-)?([0-9]{4}|[0-9]{4})$/;
		if (phone.value == null || phone.value == '') {
			return true; //Phone is not required.
		}
	 	var valid = phoneFilter.test(phone.value);
		if (valid) {
			return true;
		}
		validationResponse('Please enter a valid phone number.\n(111)222-3333 or \n111-222-3333 or \n1112223333',phone);
		return false;
	}
	
	function validateEmail(email) {
		var emailFilter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	 	if (email.value == null || email.value == '') {
			return true; //Email is not required.
		}
	 	var valid = emailFilter.test(email.value);
		if (valid) {
			return true;
		}
		validationResponse('Please enter a valid email. \nname@domain.com',email);
		return false;
	}
	
	//Leave the http method as post! This widget implementation encapsulates all the people fields in 1 form.
	//During Form.serialize, the information of every person is serialized.
	//If the list of people is really big, the 'get' method's parameter limit is reached quite easily.
	//also, doing updates invoke a change on the server -- that is what post is for.
	function managePerson(url, update, callback) {
		var pars = Form.serialize($('peopleForm'));
		//alert(pars); //this could be optimized.
		new Ajax.Updater(
			'personManager',
			url,
			 { method: 'post', parameters: pars,
				onComplete: function(xhr){
					PersonManagerPopup(false);
					ajaxUpdateDropDown(xhr.getResponseHeader('posDesc'), xhr.getResponseHeader('selectedRow'), update);
					var inputs = document.getElementById('people').getElementsByTagName('input');
					var foundFirst = false;
					for (var i=0; i < inputs.length; i++) {
						var	thisInput = inputs[i];						
						if (!thisInput.disabled && thisInput.type == 'text') {
				
							thisInput.onkeydown = onInputKeyDown;
							if (!foundFirst) {
								thisInput.focus();
								thisInput.select();
								foundFirst = true;
							};
						};		
					};
					if (callback !== undefined) callback();
				},
				onFailure:function(xhr){
					alert('The system has experienced a problem processing your last request.\nIf the problem persists, please contact your account manager or call Customer Service at 1-877-378-5665.');
				}
			});
	}
	
	function ajaxUpdateDropDown(pos, selectedRow, selectNewRow) {
	    var updateUrl = 'ManagePeopleDisplayAction.go';
	    var updatePars = 'updateDropDown=true';
	    var el = pos + 'Drop';
	    new Ajax.Updater(
	    el,
	    updateUrl,
	    {
	        method: 'post',
	        parameters: updatePars,
	        onComplete: function() {
	            if (selectNewRow && selectedRow != '') {
	                selectPerson(selectedRow, pos);
	            }
	        }
	    }
	    );
	}
	
	function PersonManagerPopup(firstLoad) {
		if (firstLoad) {
			var x = 0;
			var y = 0;
			var position = 'screen-center';
		} else {
			var personManager = document.getElementById('personManager');
			var x = personManager.offsetLeft;
			var y = personManager.offsetTop;
			var position = 'screen-corner';
		}
		managePers.popup_show('personManager','dragPeople','closePersonManager',position,200,false,x,y);
	}
	
	function removeStyle() {
		if (lastElements != null) {
			for(i=0;i<lastElements.length;i++) {
				var style = 'selectedBgCenter';
				if (i == 0) {
					style = 'selectedBgLeft';
				} else if (i == lastElements.length-1) {
					style = 'selectedBgRight';
				}
				Element.removeClassName(lastElements[i], style);
			}
		}
	}
	
	//Highlight the selected/modified row of the person manager and populate drop down.
	function selectPerson(targetRow, pos) {
		var personId = $('personId'+targetRow).value;
		var personName = $('personName'+targetRow).value;
		var personRow = $('personRow'+targetRow);
	
		updateDropDown(personId, personName, pos);
		
		var rows = document.getElementById('people').getElementsByTagName('tr');
		for (var i = rows.length - 1; i >= 0; i--){
			rows[i].className = rows[i].className.replace(/\shighlighted/,'');
		};
		document.getElementById('addPersonRow').style.display = 'none';
		Element.addClassName(personRow , 'highlighted');
	}
	
	function clearPersonManager() {
		var rows = document.getElementById('people').getElementsByTagName('tr');
		for (var i = rows.length - 1; i >= 0; i--){
			rows[i].className = rows[i].className.replace(/\shighlighted/,'');
		};
	}
	
	//On change function called by appraiser and salesperson drop down. Puts the person name into
	//the hidden field on the tradeAnalyzerForm.
	function updateTradeAnalyzerForm(element, pos) {
		if (element.selectedIndex >= 0) {
			var option = element.options[element.selectedIndex];
			updateDropDown(option.value, option.text, pos);
		} else {
			updateDropDown('', '', pos);
		}
	}
	
	//Takes given values and sets it on the tradeAnalyzerForm.
	//
	function updateDropDown(id, name, pos) {
		var dropDown = getDropDown(pos);
		if (!dropDown) return;
		dropDown.value = id;
		if (dropDown.selectedIndex != -1) {
			dropDown.options[dropDown.selectedIndex].text = name;
		} else {
			if (id != null && name != null) {
				var newOpt = document.createElement('option');
				newOpt.appendChild(document.createTextNode(name));
				newOpt.value = id;
				dropDown.appendChild(newOpt);
				dropDown.value = id;	
			} else {
				dropDown.value = undefined;
				dropDown.options[0].selected = true;
			}
		}	
		switch(pos) {
			case 'Salesperson':
				$('dealTrackSalespersonID').value = id;
				$('dealTrackSalesperson').value = name;
			break;
			case 'Appraiser':
				$('appraiserName').value = name;
			break;
			case 'Buyer':
				$('buyerId').value = id;
			break;
		}
	}
	
	function getDropDown(pos) {
		switch(pos) {
			case "Salesperson":
				return $('SalespersonList') || $('Salesperson');	
			case "Appraiser":
				return $('appraisers') || $('Appraiser');
			case 'Buyer':
				return $('BuyerList');
		}
	}
	
	function getPos(dropDown) {
		switch ( dropDown.id ) {
			case 'SalespersonList': 
				return 'Salesperson';
			case 'appraisers' || 'Appraiser':
				return 'Appraiser';
			case 'BuyerList':
				return 'Buyer';
		}
	}
	
	function updatePersonManager (event) {
		var dropDown = event.target || event.srcElement;
		var selected = dropDown.options[dropDown.selectedIndex].value || '';
		if (selected != '') {
			selectPerson(selected, getPos(dropDown));
		} else {
			clearPersonManager();
		}
	}
	
	
	//Sorts the given column in the given direction (e.g. lastName, desc)
	function sortPeople(sortCol, pos, sortDir) {
		pars = 'pos=' + pos + '&sortColumn=' + sortCol + '&sortDir=' + sortDir;
		new Ajax.Updater(
				'personManager',
				'/IMT/ManagePeopleDisplayAction.go',
				 { method: 'get', parameters: pars,
				 	onComplete:function() {
				 		PersonManagerPopup(false);
				 	},
					onFailure:function(xhr){
						alert('The system has experienced a problem processing your last request.\nIf the problem persists, please contact your account manager or call Customer Service at 1-877-378-5665.');
					}
			});
	}
	
	function sortPeopleDesc(sortCol, pos) {
		sortPeople(sortCol, pos, 'desc');
	}
	
	function sortPeopleAsc(sortCol, pos) {
		sortPeople(sortCol, pos, 'asc');
	}
	
	return PersonManager;
})();
