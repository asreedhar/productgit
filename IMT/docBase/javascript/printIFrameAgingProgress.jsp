<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>

<script language="javascript" type="text/javascript">
var IFrameObj; // our IFrame object
function loadPrintIframe() {
	if (!document.createElement) {return true};
	var IFrameDoc;
	var URL = "<logic:present name="printRef"><bean:write name="printRef"/></logic:present>";
	URL = (URL == "") ? "printHolder.html" : URL;
	if (!IFrameObj && document.createElement) {
		// create the IFrame and assign a reference to the
		// object to our global variable IFrameObj.
		// this will only happen the first time
		// callToServer() is called
		var tempIFrame=document.createElement('iframe');
		tempIFrame.setAttribute('id','printFrame');
		tempIFrame.setAttribute('src','./javascript/printHolder.html');
		tempIFrame.style.border='0px';
		tempIFrame.style.width='0px';
		tempIFrame.style.height='0px';
		IFrameObj = document.body.appendChild(tempIFrame);
	}
	IFrameDoc = IFrameObj.contentWindow.document;
	IFrameDoc.location.replace(URL);
	return false;
}
function loadTop() {
	progress_stop();
	var topDoc = document.getElementById("topDiv");
	var frameDoc = IFrameObj.contentWindow.document.getElementById("frameDiv");
	var oNewNode = document.createElement("div");
	topDoc.innerHTML = frameDoc.innerHTML;
	init();
}
</script>
