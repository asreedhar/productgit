var pageDirty = false;

function makeDirty () {pageDirty = true;}

function isDirty () {
	if (pageDirty) {
		var userSelection = window.confirm ("* You have made changes to the Aging Plan. *\n                       *************\nPress OK to save those changes and continue.\nPress Cancel to discard those changes and continue.");
		return userSelection;
	} else {
		return false;
	}
}
function doMakeDirty() {
	if (typeof(makeDirty) != "undefined") {makeDirty();}
}

