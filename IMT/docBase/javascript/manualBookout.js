var dirtyYear = false;
var dirtyMake = false;
var dirtyModel = false;

function loadMakes(year, guideBook)
{	
	if(year != 0){
		var path = guideBook + 'ManualBookoutAction.go?selected=make&';
		var pars = Form.serialize($('Form') );
		new Ajax.Updater(
			'makeDropDown',
			path + pars,
			{
				method:'post',
				onComplete: function(r,j) {}
			}
		);
	}else{
		resetMake();
		$('make').disabled='true';
	}
	$('model').disabled='true';
	
		
	if(dirtyYear){
			$('mileage').value="0";
	}
	dirtyMake = false;
	dirtyYear = true;
	resetModel();
	resetSeries()
	resetTrim();
	resetStyle()
	resetOptions();
}	

function loadModels(makeCode, guideBook)
{	
	if(makeCode != 0){
		var path = guideBook + 'ManualBookoutAction.go?selected=model&';
		var pars = Form.serialize($('Form') );
		new Ajax.Updater(
			'modelDropDown',
			path + pars,
			{
				method:'post',
				onComplete: function(r,j) {}
			}
		);
	}else{
		resetModel();
		$('model').disabled='true';
	}
	
	resetSeries();
	resetStyle();
	
	if(dirtyMake){
		$('mileage').value="0";
	}
	dirtyModel = false;
	dirtyMake = true;
	resetTrim();
	resetOptions();
}

function loadTrims(modelCode, guideBook)
{
	if(modelCode != 0){
		var path = guideBook + 'ManualBookoutAction.go?selected=trim&';
		var pars = Form.serialize($('Form') );	
		new Ajax.Updater(
			'catalogKeyDropDown',
			path + pars,
			{
				method:'post',
				onComplete: function(r,j) {}
			}
		);
	}else{
		resetTrim();
		resetSeries()
	}
	if(dirtyModel){
		$('mileage').value="0";
	}
	dirtyModel = true;
	resetOptions();
}	


function loadSeries(modelCode, guideBook)
{
	if(modelCode != 0){
		var path = guideBook + 'ManualBookoutAction.go?selected=trim&';
		var pars = Form.serialize($('Form') );	
		new Ajax.Updater(
			'seriesDropDown',
			path + pars,
			{
				method:'post',
				onComplete: function(r,j) {
					if ($('series').options.length <= 1) {
						loadStyles('-1', 'BlackBook'); // only have series for blackbook
					}
				}
			}
		);
	}else{
		resetTrim();
		resetSeries();
	}
	if(dirtyModel){
		$('mileage').value="0";
	}
	dirtyModel = true;
	resetStyle();
	resetOptions();
}	


function loadStyles(trimCode, guideBook)
{

	if(trimCode != 0){
		var path = guideBook + 'ManualBookoutAction.go?selected=style&';
		var pars = Form.serialize($('Form') );	
		new Ajax.Updater(
			'styleDropDown',
			path + pars,
			{
				method:'post',
				onComplete: function(r,j) {}
			}
		);
	}else{
		resetStyle();
		resetSeries();
	}
	if(dirtyModel){
		$('mileage').value="0";
	}
	dirtyModel = true;
	resetOptions();
}	

function resetOptions()
{	
	$("options").innerHTML = "";
}

function loadOptions( guideBook ) {

	var pars = Form.serialize($('Form') );
	var path = guideBook + 'ManualBookoutAction.go?selected=getOptions&';
	new Ajax.Updater(
		'options',
		path + pars,
		{
			method:'post',
			onComplete: function(r,j) {}
		}
	);
}

function loadValues( guideBook ) {
	var pars = Form.serialize($('Form') );
	var path = guideBook + 'ManualBookoutAction.go?selected=getValues&';
	new Ajax.Updater(
		'options',
		path + pars,
		{
			method:'post',
			onComplete: function(r,j) {}
		}
	);
}

function printGuideBook( guideBook ) {
	var pars = Form.serialize($('Form') );
	var path = guideBook + 'ManualBookoutAction.go?selected=print&';
	var params = [
		['make_s','make'],
		['model_s','model'],
		['trim_s','catalogKeyId']
	];
	var replaceInUrl = function(key, value) {
		var pattern = new RegExp("(\&\?)("+key+"=)[^\&]*(\&?)");
		if (pars.match(pattern)) {
			pars = pars.replace(pattern,"$1$2"+encodeURIComponent(value)+"$3");
		} else {
			pars += "&"+encodeURIComponent(key)+"="+encodeURIComponent(value);
		}
	};
	for (var i=0,l=params.length; i < l; i++) {
		var key = params[i][0];
		var el = document.getElementById(params[i][1]);
		if(el) {
			if (el.options) {
				replaceInUrl(key,el.options[el.selectedIndex].text);
			} else if (el.value) {
				replaceInUrl(key,el.value);		
			} else if (el.text) {
				replaceInUrl(key,el.text);
			} else {
				replaceInUrl(key,el.innerHTML);
			}
		}
	}
	if (printURL) {
		printURL(path + pars);
	}
}

function validate(sFrom, guideBook) {
	var year = $('year').value;
	var make = $('make').value;
	var model = $('model').value;
	var trim = '';
	var style = '';
	if (guideBook == 'BlackBook') {
		trim = $('series').value;
		style = $('style').value;
	} else {
		if($('catalogKeyId')) {
			trim = $('catalogKeyId').value;
		}
	}
	
	var mileage = $('mileage').value;
	
	var isValid = true;
	var sMessage = "";
		
	if ( isNaN(year) || year == 0 ) {
		isValid = false;
		sMessage += "Please select a year\n";
	} 
	else if ( !make || make == 0 ) {
		isValid = false;
		sMessage += "Please select a make\n";
	} 
	else if ( !model || model == 0 ) {
		isValid = false;
		sMessage += "Please select a model\n";
	} 
	else if ( !trim || trim == 0 ) {
		if (guideBook != 'BlackBook') {
			isValid = false;
			sMessage += "Please select a trim\n";
		} else {
			// on some black book 'All or trim==0 is valid
			if ($('series').options.length > 1) {
				isValid = false;
				sMessage += "Please select a series\n";			
			}
		}
	} else if ( (guideBook == 'Blackbook') &&  (!series || series == -1) ) {
			isValid = false;
			sMessage += "Please select a series\n";	
	}
	else if ( sFrom == 'values') {
		if ( mileage == "" || isNaN(mileage) || mileage < 0 ) {
			isValid = false;
			sMessage += "Please enter a valid mileage without any punctuation marks";
		} 
		
	}
	if(!isValid) {
		alert(sMessage);
	} 
	else {
	
		if (guideBook == "BlackBook" || guideBook == "NADA" || guideBook =="KBB")
			updatePrintMenu(guideBook);
		document.getElementById('notes').style.display = "block";
		if (sFrom == 'options') {
			loadOptions( guideBook ); 
		} 
		else if (sFrom == 'print') {
			printGuideBook( guideBook ); 
		} 
		else {
			loadValues( guideBook ); 
		}
	}
}

function updatePrintMenu(guideBook) {
	var printMenu = document.getElementById('printCell');	
	printMenu.innerHTML = '';
	var printLink = document.createElement('a');
	var textNode = document.createTextNode('PRINT');
	printLink.onclick = function() { printGuideBook( guideBook ); };
	printLink.style.cursor = "pointer";
	printLink.style.color = "#000";
	printLink.appendChild(textNode);
	printMenu.appendChild(printLink);
}

function resetModel() {
	if ($("model")) {
		modelSelectionOptions = $("model").options;
		if (modelSelectionOptions) {
			numOfOptions = modelSelectionOptions.length;
			for (i = numOfOptions; i > 0; i--) {
				modelSelectionOptions[i] = null;
			}
			modelSelectionOptions[0] = new Option();
			modelSelectionOptions[0].innerHTML = "Please Select Model";
			modelSelectionOptions[0].value = "0";
		}
	}
}

function resetMake() {
	if ($("make")) {
		makeSelectionOptions = $("make").options;
		if (makeSelectionOptions) {
			numOfOptions = makeSelectionOptions.length;
			for (i = numOfOptions; i > 0; i--) {
				makeSelectionOptions[i] = null;
			}
			makeSelectionOptions[0] = new Option();
			makeSelectionOptions[0].innerHTML = "Please Select Make";
			makeSelectionOptions[0].value = "0";
		}
	}
}

function resetTrim() {
	if ($("catalogKeyId")) {
		trimSelectionOptions = $("catalogKeyId").options;
		if (trimSelectionOptions) {
			numOfOptions = trimSelectionOptions.length;
			for (i = numOfOptions; i > 0; i--) {
				trimSelectionOptions[i] = null;
			}
			trimSelectionOptions[0] = new Option();
			trimSelectionOptions[0].innerHTML = "Please Select Trim";
			trimSelectionOptions[0].value = "0";
		
		}
	}
}

function resetSeries() 
{
	if ($("series")) {
		seriesSelectionOptions = $("series").options;
		if (seriesSelectionOptions) {
			numOfOptions = seriesSelectionOptions.length;
			for (i = numOfOptions; i > 0; i--) {
				seriesSelectionOptions[i] = null;
			}
			seriesSelectionOptions[0] = new Option();
			seriesSelectionOptions[0].innerHTML = "Please Select Series";
			seriesSelectionOptions[0].value = "0";
		}
		$('series').disabled='true';
	} else {
		if($('catalogKeyId')) {
			$('catalogKeyId').disabled='true';
		}
	}
}	
function resetStyle() 
{
	if($('style')) {
		$('style').disabled='true';
	} 
}	

function resetResults() {
	
}

