<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<script language="JavaScript1.2" type="text/javascript">
<!--
/*
HM_Array1 = [
    [170,
    178,28,
    "","",
    "","",
    "","",
    0,
    0,0,
    true,
    true,1,
    "null","null",
    "",""],
      ["View/Edit Market Supply","javascript:top.location='SelectMarketDisplayAction.go?destination=supply'",1,0,0],
      ["View/Edit Marketing Book","javascript:top.location='SelectMarketDisplayAction.go?destination=book'",1,0,0]
]
*/

/*
HM_Array3 = [
    [170,
    297,28,
    "","",
    "","",
    "","",
    0,
    0,0,
    true,
    true,1,
    "null","null",
    "",""],
    ["View/Edit Auction","javascript:top.location='AuctionListDisplayAction.go'",1,0,0],
    ["Add New Auction", "javascript:top.location='AllMarketsForAuctionDisplayAction.go'",1,0,0]
]
*/
HM_Array4 = [
    [170,
    180,28,
    "","",
    "","",
    "","",
    0,
    0,0,
    true,
    true,1,
    "null","null",
    "",""],
    ["View/Edit Representative","javascript:top.location='MemberSearchDisplayAction.go'",1,0,0],
    ["Add New Representative","javascript:top.location='DealerGroupSearchSubmitAction.go?submit=Add%20Member&dealerGroupName='",1,0,0],
    ["Add New First Look Admin","javascript:top.location='AddMemberDisplayAction.go?memberType=1'",1,0,0],
    ["Add New Account Rep","javascript:top.location='AccountRepSelectDealerGroupAction.go'",1,0,0]
]

HM_Array5 = [
    [170,
    290,28,
    "","",
    "","",
    "","",
    0,
    0,0,
    true,
    true,1,
    "null","null",
    "",""],
    ["View/Edit Corporation","javascript:top.location='CorporationSearchDisplayAction.go'",1,0,0],
    ["Add New Corporation","javascript:top.location='AddCorporationDisplayAction.go'",1,0,0],
    ["View/Edit Dealer Group","javascript:top.location='DealerGroupSearchDisplayAction.go'",1,0,0],
    ["Add New Dealer Group","javascript:top.location='AddDealerGroupDisplayAction.go'",1,0,0],
    ["View/Edit Dealership","javascript:top.location='DealerSearchDisplayAction.go'",1,0,0],
    ["Add New Dealership","javascript:top.location='DealerGroupSearchSubmitAction.go?submit=Add%20New%20Dealership&dealerGroupName='",1,0,0],
    ["View/Edit Region","javascript:top.location='RegionSearchDisplayAction.go'",1,0,0],
    ["Add New Region","javascript:top.location='AddRegionDisplayAction.go'",1,0,0]
]

HM_Array6 = [
    [170,
    544,28,
    "","",
    "","",
    "","",
    0,
    0,0,
    true,
    true,1,
    "null","null",
    "",""],
    ["View/Edit Dealer Group","javascript:top.location='DealerGroupSearchDisplayAction.go'",1,0,0],
    ["Add New Dealer Group","javascript:top.location='AddDealerGroupDisplayAction.go'",1,0,0]
]

HM_Array7 = [
    [162,
    350,28,
    "","",
    "","",
    "","",
    0,
    0,0,
    true,
    true,1,
    "null","null",
    "",""],
    ["<i>Region Setup</i> <img src='images/common/down_arrow.gif' border='0' width='15' height='11' align='absmiddle'>",,0,0,0],
    ["&nbsp;&nbsp;&nbsp;Add New Region","javascript:top.location='AddFirstLookRegionDisplayAction.go'",1,0,0],
    ["&nbsp;&nbsp;&nbsp;View/Edit Regions","javascript:top.location='FirstLookRegionDisplayAction.go'",1,0,0],
    ["Flush Cache","javascript:top.location='AdminFlushCacheAction.go'",1,0,0],
    ["Market Suppression","javascript:top.location='MarketSuppressionDisplayAction.go'",1,0,0],
    ["Access Group Cost Factors","javascript:top.location='AccessGroupMappingDisplayAction.go'",1,0,0]
]



/* tcb - function to display the pdf's in a new window.  path is required, windowName may be specified as null*/
function openWindow(path,windowName) {
  window.open(path,windowName,"status=yes,toolbar=yes,menubar=no,location=no,scrollbars=yes,resizable=yes");
}

function verifyLogout(thisForm)
{
    var confirmLogout;
    confirmLogout = confirm("Are you sure that you want to log out of the Admin Interface?" );
    
    if(confirmLogout)
    {
        document.location.href = "LogoutAction.go";
    }
}
//-->
</script>
