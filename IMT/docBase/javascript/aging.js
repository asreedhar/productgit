var dirtyInventory = new Array();
var sentInventory = new Array();
var autoSaveRequestId;
var autoSaveInterval = 60000;
var autoSaveIntervalId;

var aip;
var agingPlan;

function initAutoSave() {
	xmlhttp = XMLHTTPObject();
	autoSaveIntervalId = window.setInterval("doAutoSave()", autoSaveInterval);
	autoSaveRequestId = -1;
}

function doAutoSave() {
	sendPlanToServer('', false);
}

function saveAll( strQuery ) {

	if( strQuery.length < 1 )
	{
		strQuery = '?fromTab=true';
	}
	
	// stop the autosave timer during manual save
	window.clearInterval(autoSaveIntervalId);
	// manual save
	sendPlanToServer( strQuery, true );
	// restart the autosave timer
	autoSaveIntervalId = window.setInterval("doAutoSave()", autoSaveInterval);
}

function saveAndGoToNext(rawWeekId,rangeId,fromRangeId,fromTab,rangeSubmit)
{
    var weekId = parseInt(rawWeekId),strQuery = "";
    strQuery = getStrQuery(weekId,rangeId,fromRangeId,fromTab,rangeSubmit);

    if(rangeId == 7)
    {
        submitForm( "AgingReportSelectDisplayAction.go" + strQuery );
    }
    else
    {
        var strQuery;
    	strQuery = getStrQuery(weekId,rangeId,fromRangeId,fromTab,rangeSubmit);
    	saveAll( strQuery );
    }
}

var xmlhttp;

function XMLHTTPObject() {


      if (window.ActiveXObject) {

      // Instantiate the latest Microsoft ActiveX Objects

      if (_XML_ActiveX) {

                  xmlhttp = new ActiveXObject(_XML_ActiveX);

     } else {

            // loops through the various versions of XMLHTTP to ensure we're using the latest

            var versions = ["MSXML2.XMLHTTP", "Microsoft.XMLHTTP","Msxml2.XMLHTTP.7.0", 

                                    "Msxml2.XMLHTTP.6.0", "Msxml2.XMLHTTP.5.0","Msxml2.XMLHTTP.4.0", "MSXML2.XMLHTTP.3.0"];

            for (var i = 0; i < versions.length ; i++) {

                        try {

                        // Try and create the ActiveXObject for Internet Explorer, if it doesn't work, try again.

                        xmlhttp = new ActiveXObject(versions[i]);

                        if (xmlhttp) {

                              var _XML_ActiveX = versions[i];

                              break;

                        }

                        }

                        catch (e) {

                        // TRAP
                        
                        } ;

            };

            }

   } // No ActiveXObject available it must be firefox, opera, or something else

   if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {

      try {
                  xmlhttp = new XMLHttpRequest();

      } catch (e) {

                  xmlhttp = false;

     }

   }

   return xmlhttp;

}

function sendPlanToServer( strQuery, isExplicitSave ) {

	// to minimize the number of possible lost changes we always append unacknowledged
	// items to the dirty list as the auto-save interval is reasonably large enough to
	// imply the previous saves failed
	appendAllUnacknowledgedChangesToDirtyList();
	
     // convert dirtyInventory
	agingPlan.inventory = convertDirtyQueueToInventory();

	// serialize to an XML string
	var s = Sarissa.xmlize(agingPlan, "agingPlan");
	
	if( isExplicitSave )
	{
		//var trackingForm = document.agingInvSelectionForm;
		document.agingInvSelectionForm.action = 'AgingInventoryPlanningSheetTrackingSubmitAction.go' + strQuery;
		document.agingInvSelectionForm.aipXml.value = s;
		document.agingInvSelectionForm.submit();
	}
	else
	{
		if( agingPlan.inventory.length > 0 )
		{
			xmlhttp.open('POST', 'AgingInventoryPlanningSheetTrackingSubmitAction.go' + strQuery, true);

			xmlhttp.onreadystatechange = function() {
			  var theSaveRequestId = autoSaveRequestId;
			  if (xmlhttp.readyState == 4) {
			      if (xmlhttp.status == 200)
				  autoSaveSuccessful( theSaveRequestId );
			  }
			}

			/* Send the POST request */
			xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			s = "aipXml=" + escape(s);
			xmlhttp.send( s);
		}
	}
}


function appendAllUnacknowledgedChangesToDirtyList() {
	for (var i = 0; i < sentInventory.length; i++) {
		for (var j = 0; j < sentInventory[i].length; j++) {
			addToDirtyQueue(sentInventory[i][j]);
		}
	}
}

function autoSaveSuccessful( requestId ) {

  if (xmlhttp.readyState == 4) {

	/*
	 * The return value from the servlet is the HTML of the page, i.e. it is not useful
	 * and has no data on the success of the request. This is not ideal as we have no
	 * error checking if all the saves are done automatically as we can't ask the user
	 * to manually save or take other action.
	 *
	 * Additionally, the use of a single XMLHttpRequest for potentially multiple requests
	 * means the callback for the last request is the only one which will get serviced.
	 */
    
	// send is successful so clear the sent queue for this request
	var acknowledgedItems = sentInventory[requestId];
	sentInventory[requestId] = new Array();
	// every acknowledged item can be removed from prior sentInventory arrays (your changed overwrited theirs)
	for (var i = 0; i < acknowledgedItems.length; i++) {
		for (var j = requestId-1; j >= 0; j--) {
			for (var k = 0; k < sentInventory[j].length; k++) {
				if (sentInventory[j][k] == acknowledgedItems[i]) {
					sentInventory[j].splice(k,1);
				}
			}
		}
	}
  }

}

/**
 *	Determines if we have local changes that have not been acknowledged by the server.
 *	An unacknowledged local change is defined as a dirty item (i.e. not posted to the
 *	server) or a sentInventory item which has not yet been acknowledged.
 *
 *	The method is used to control the display of a modal dialog promtping the user to
 *	save any unacknowledged changes.
 *	
 *	On first look this code appears very susceptible to threading issues as we can have
 *	many <code>autoSaveSuccessful()</code> callbacks running at any instant. However,
 *	since the callback decreases the number of items in the sentInventory the worst-case
 *	result of these concurrent modifications the over-estimation of unacknowledged changes
 *  and therefore a final duplicated save invokation. 
 */
function haveUnacknowledgedChanges() {
	if (dirtyInventory.length == 0) {
		for (var i = 0; i < sentInventory.length; i++) {
			if (sentInventory[i].length > 0) {
				return true;
			}
		}
		return false;
	}
	else {
		return true;
	}
}

function limitText(limitField, limitNum) {
 if(limitField.value.length > limitNum)
 {
  limitField.value = limitField.value.substring(0, limitNum);
  alert("Please limit your Notes to " + limitNum + " character.");
  return false;
 }
}

var rendered = false;
var activeHeader = null, activeMenu = null, loaded = false;
function init() {loaded = true;}

function setMenu(menuHeaderID,menuID) {
    if (!loaded) return;
    var top = 0, left = 0, currentEle;
    if(document.all) {
        if(activeHeader != null && activeMenu != null) {
            if(activeMenu.style.visibility != 'hidden') {
                menuHide();
        }   }
        activeHeader = eval("document.all('" + menuHeaderID + "');");
        activeMenu = eval("document.all('" + menuID + "');");
        currentEle = activeHeader;
        while(currentEle.tagName.toLowerCase() != 'body') {
            top += currentEle.offsetTop;
            left += currentEle.offsetLeft;
            currentEle = currentEle.offsetParent;
        }
        top += (activeHeader.offsetHeight);
        activeMenu.style.left = left - 2;
        activeMenu.style.top = top;
        menuShow();
        event.cancelBubble = true;
}   }

function menuShow() {if(document.all) {activeMenu.style.visibility = 'visible';}}

function menuHide(){if(document.all) {activeMenu.style.visibility = 'hidden';}}

function hideMenu() {
    if(document.all) {
        if(activeHeader != null && activeMenu != null) {
            if( !activeMenu.contains(event.toElement) && !activeHeader.contains(event.toElement) ) {
                activeMenu.style.visibility = 'hidden';
                activeHeader = null; activeMenu = null;
}   }   }   }

function popCopyright(rights) {window.open(rights,"copy","width=780,height=450,location=no,menubar=no,resizeable=no,scrollbars=no,status=no,toolbar=no");}
var helpUp = false,helpDiv = null, helpDivTop, helpDivBottom;

function openHelp(objClickedHelpCell, whichDiv) {
    if (helpUp) return false;
    helpDiv = (whichDiv) ? document.getElementById(whichDiv) : document.getElementById("helpDiv");
    var topPosition = 0, leftPosition = 0, elementToOffset;
    elementToOffset = objClickedHelpCell;
    while(elementToOffset.tagName.toLowerCase() != 'body') {
        topPosition += elementToOffset.offsetTop;
        leftPosition += elementToOffset.offsetLeft;
        elementToOffset = elementToOffset.offsetParent;
    }
    topPosition += objClickedHelpCell.offsetHeight;
    helpDivTop = topPosition;
    leftPosition += objClickedHelpCell.offsetWidth;
    var bodyWidth = document.getElementById("dealerBody").clientWidth;
    var bodyHeight = document.getElementById("dealerBody").clientHeight;
    var helpImageLeft = objClickedHelpCell.offsetLeft;
    helpDiv.style.top = topPosition;
    if ((helpImageLeft + 229) > bodyWidth) {
        helpDiv.style.left = bodyWidth - 232;
    } else {
        helpDiv.style.left = leftPosition;
    }
    helpDiv.style.display = "block";
    if ((topPosition + helpDiv.offsetHeight) > bodyHeight) {
        helpDiv.style.top = topPosition - (helpDiv.offsetHeight - (bodyHeight - topPosition)) - 10;
        helpDivTop = topPosition - (helpDiv.offsetHeight - (bodyHeight - topPosition)) - 10;
    } else {
        helpDiv.style.top = topPosition;
    }
    helpDivBottom = helpDivTop + helpDiv.offsetHeight;
    hideHelpSelect();helpUp = true;
}

function closeHelp (divToClose) {
    var closer = (divToClose) ? document.getElementById(divToClose) : document.getElementById('helpDiv');
    closer.style.display = "none";showHelpSelect();helpUp = false;helpDiv = null;
}

function killIt() {event.cancelbubble="true";return false;}

function showHelpSelect() {
    var obj;
    for(var i = 0; i < document.all.tags("select").length; i++) {
        obj = document.all.tags("select")[i];
        if(!obj || !obj.offsetParent) continue;
        obj.style.visibility = 'visible';
}   }
function hideHelpSelect() {
    var obj, currentEle, top = 0, left = 0, helpHeight, timeout;
    for(var i = 0; i < document.all.tags("select").length; i++) {
        obj = document.all.tags("select")[i];
        currentEle = obj;
        while(currentEle.tagName.toLowerCase() != 'body') {
            top += currentEle.offsetTop;
            left += currentEle.offsetLeft;
            currentEle = currentEle.offsetParent;
        }
        selBottom = top + obj.offsetHeight;
        if(helpDiv != null) {
            /*helpHeight = (helpDiv.offsetTop + helpDiv.offsetHeight);*/
            if(selBottom > helpDivTop && top < helpDivBottom) {
                if((left < (helpDiv.offsetLeft + helpDiv.offsetWidth)) && (left + obj.offsetWidth > helpDiv.offsetLeft))
                    obj.style.visibility = 'hidden';
        }   }
        top = 0;left = 0;
}
}

function isDirtyM () {
	if (haveUnacknowledgedChanges()) {
		var userSelection = window.showModalDialog ("AgingModalDisplayAction.go","","dialogHeight:150px;dialogWidth:400px;center:yes;edge:raised;help:no;resizable:no;scroll:no;status:no;unadorned:yes");
		return userSelection;
	} else {
		return false;
	}
}

function clickNavM(rawWeekId,rangeId,fromRangeId,fromTab,rangeSubmit)
{
	var weekId = parseInt(rawWeekId);

	if(rangeId == 7)
	{
		if ( isDirtyM() )
		{
			saveAll('');
		}
		var trackingForm = document.getElementById("agingInvSelectionForm");
		trackingForm.action = "AgingReportSelectDisplayAction.go" + getStrQuery(weekId,rangeId,fromRangeId,fromTab,rangeSubmit);
		trackingForm.submit();
		
	}
	else
	{
		if (typeof(isDirtyM) != "undefined")
		{
			if ( isDirtyM() )
			{
				//submitForm( getSubmitActionWithParams(weekId,rangeId,fromRangeId,fromTab,rangeSubmit) );
				saveAndGoToNext(weekId,rangeId,fromRangeId,fromTab,rangeSubmit);
				
			}
			else
			{
				document.location.href = getDisplayAction( weekId,rangeId,fromRangeId,fromTab,rangeSubmit );
			}
		}
		else
		{
			submitForm( getSubmitActionWithParams(weekId,rangeId,fromRangeId,fromTab,rangeSubmit) );
		}
	}
}

function getDisplayAction( weekId,rangeId,fromRangeId,fromTab,rangeSubmit )
{
    var displayAction = new Array();
    var arrayWeekId = getArrayWeekId( weekId,fromTab,rangeSubmit );
    displayAction[0] = "AgingInventoryPlanningSheetTrackingDisplayAction.go";
    displayAction[1] = "AgingInventoryPlanningSheetPriorDisplayAction.go";
    var strQuery = getStrQuery(weekId,rangeId,fromRangeId,fromTab,rangeSubmit);
    return displayAction[arrayWeekId] + strQuery;
}

function getArrayWeekId ( weekId,fromTab,rangeSubmit )
{
	/* in a nutshell, current week = 0 and prior week = 1 */
	var weekId = (fromTab == "true" || rangeSubmit == "true") ? weekId - 1:  (weekId == "1") ? weekId: weekId - 2;
	return weekId;

}

function getSubmitAction( arrayWeekId )
{
    var submitAction = new Array();
    submitAction[0] = "AgingInventoryPlanningSheetTrackingSubmitAction.go";
    submitAction[1] = "AgingInventoryPlanningSheetPriorSubmitAction.go";
    return submitAction[arrayWeekId]
}

function getSubmitActionWithParams( weekId,rangeId,fromRangeId,fromTab,rangeSubmit )
{
    var arrayWeekId,strQuery;
    strQuery = getStrQuery(weekId,rangeId,fromRangeId,fromTab,rangeSubmit);
    arrayWeekId = getArrayWeekId( weekId,fromTab,rangeSubmit );

    return getSubmitAction( arrayWeekId ) + strQuery;
}

function getStrQuery( weekId,rangeId,fromRangeId,fromTab,rangeSubmit )
{
    var toWeekId;
    toWeekId = (fromTab == "true" || rangeSubmit == "true") ? weekId: (weekId == "1") ? weekId + 1: weekId - 1;
    strQuery = (rangeSubmit == "false") ? "?weekId=" + toWeekId + "&rangeId=" + rangeId + "&fromRangeId=" + fromRangeId + "&fromTab=" + fromTab : "?weekId=" + toWeekId + "&rangeId=" + rangeId + "&fromRangeId=" + fromRangeId + "&rangeSubmit=true&mode=UCBP";
    return strQuery;
}

function submitForm( action )
{
   if (validate())
    {
	    var trackingForm = document.getElementById("agingInvSelectionForm");
	    trackingForm.action = action;
	    trackingForm.submit();
	}
}

function saveNext(rawWeekId,rangeId,fromRangeId,fromTab,rangeSubmit)
{
    var weekId = parseInt(rawWeekId),strQuery = "";
    strQuery = getStrQuery(weekId,rangeId,fromRangeId,fromTab,rangeSubmit);

    if(rangeId == 7)
    {
        submitForm( "AgingReportSelectDisplayAction.go" + strQuery );
    }
    else
    {
        submitForm( getSubmitActionWithParams( weekId,rangeId,fromRangeId,fromTab,rangeSubmit ) );
    }
}

var vehicleArray = new Array();
function vehicle(vid,approach,wholesaler,auction,nameText,date,notes,spiffs,promos,advertise,other,salePending,soldRetail,soldWholesale,reprice,specialFinancing,spiffNotes) {
    this.vid = vid;
    this.approach = approach;
    this.wholesaler = (wholesaler == "true") ? true : false;
    this.auction = (auction == "true") ? true : false;
    this.nameText = nameText;
    this.date = date;
    this.notes = notes;
    this.spiffs = (spiffs == "true") ? true : false;
    this.promos = (promos == "true") ? true : false;
    this.advertise = (advertise == "true") ? true : false;
    this.saother = (other == "true") ? true : false;
    this.salePending = (salePending == "true") ? true : false;
    this.retail = (soldRetail == "true") ? true : false;
    this.wholesale = (soldWholesale == "true") ? true : false;
    //alert("vid:" + vid + " | " + document.getElementById("planContainer" + vid));
    this.container = document.getElementById("planContainer" + vid);
    this.reprice = reprice;
    this.specialFinancing = specialFinancing;
    this.spiffNotes = spiffNotes;
}

function getRadioValue (radioButtonOrGroup) {
  var buttonsLength = radioButtonOrGroup.length;
  if (buttonsLength) { // group
    for (var b = 0; b < buttonsLength; b++)
      if (radioButtonOrGroup[b].checked)
        return radioButtonOrGroup[b].value;
  }
  else if (radioButtonOrGroup.checked)
    return radioButtonOrGroup.value;
  return null;
}


function loadVehicles( appliedLastWeeksNotes ) {
    var spans = document.getElementsByTagName("SPAN");
    for (var d=0;d<spans.length;d++) {
        if(spans[d].id == "planSpan") {
            var span = spans[d].innerText.split("~~~");
            doSpan(span,d, appliedLastWeeksNotes );
}   }   }

function loadThisVehicle(radio) {
    var radioSet = document.getElementsByName(radio.name);
    for (o=0;o<radioSet.length;o++) {
        if(radioSet[o] == window.event.srcElement) {
            radioSet[o].parentElement.children[1].style.fontWeight = "bold";
        } else {
            radioSet[o].parentElement.children[1].style.fontWeight = "normal";
    }   }
    var vehicleToGet;
    var radioId = radio.name.substring(8);
    addToDirtyQueue( radioId );
    for (var s=0;s<vehicleArray.length;s++) {
        if( (typeof(vehicleArray[s]) == "object") && (vehicleArray[s].vid == radioId) ) {
            vehicleToGet = s;
            break;
    }   }
    if(radio.value == vehicleArray[vehicleToGet].approach) {return};
    var containerToGet = document.getElementById("planContainer" + radioId);
    if(containerToGet.childNodes.length > 2) {
        for(var n=2;n<containerToGet.childNodes.length;n++) {
            containerToGet.childNodes[n].removeNode(true);
    }   }
    var span = containerToGet.childNodes[0];
    var spanList = span.innerText.split("~~~");
    spanList[1] = radio.value;
    if(span.id == "planSpan") {
        doSpan(spanList,vehicleToGet);
}   }

function addToDirtyQueue(inventoryId) {

      var exists = false;

      for (var i = 0; i < dirtyInventory.length; i++) {

            if (dirtyInventory[i] == inventoryId) {

                  exists = true;

                  break;                  

            }

      }

      if (!exists) {

            dirtyInventory[dirtyInventory.length] = inventoryId;

      }
}

function convertDirtyQueueToInventory( isExplicitSave ) {

	  /*
	   * NOTE: Keep solitary reference to dirtyInventory for caller thread
	   * to avoid threading issues.
	   */
	  var dirtyInventoryRef = dirtyInventory;
	  dirtyInventory = new Array();
	  
      var inventory = new Array();
      var vehicleToGet;

      for (var i = 0; i < dirtyInventoryRef.length; i++) {

	  var inventoryToAdd = dirtyInventoryRef[i];
	 
	  inventoryItem = new Object(); 
	  
	  inventoryItem.vid = inventoryToAdd;

	  inventoryItem.approach = getRadioValue( document.getElementsByName("approach" + inventoryToAdd ) );
	  
	  if( inventoryItem.approach == "W" )
	  {
	  	inventoryItem.saDate = document.getElementById("saDate" + inventoryToAdd).value;
	  	inventoryItem.saName = document.getElementById("saName" + inventoryToAdd).value;
	  	inventoryItem.saWholesaler = ((document.getElementById("saWholesaler" + inventoryToAdd)).checked) ? "true" : "false";
	  	inventoryItem.saAuction = ((document.getElementById("saAuction" + inventoryToAdd)).checked) ? "true" : "false";
	  	inventoryItem.wholesaleNotes = document.getElementById("wholesaleNotes" + inventoryToAdd).value;
	  }
	  else if( inventoryItem.approach == "R" )
	  {	
		inventoryItem.Advertise = ((document.getElementById("Advertise" + inventoryToAdd)).checked) ? "true" : "false";
		inventoryItem.Promos = ((document.getElementById("Promos" + inventoryToAdd)).checked) ? "true" : "false";
		inventoryItem.Spiffs = ((document.getElementById("Spiffs" + inventoryToAdd)).checked) ? "true" : "false";
		inventoryItem.SpiffNotes = document.getElementById("spiffNotes" + inventoryToAdd).value;
	  	inventoryItem.saOther = ((document.getElementById("saOther" + inventoryToAdd)).checked) ? "true" : "false";
	  	inventoryItem.retailNotes = document.getElementById("retailNotes" + inventoryToAdd).value;
	  	inventoryItem.specialFinancing = ((document.getElementById("specialFinancing" + inventoryToAdd)).checked) ? "true" : "false";
	  	if( document.getElementById("retailReprice" + inventoryToAdd) != null )
	  	{
	  		inventoryItem.retailPrice = document.getElementById("retailReprice" + inventoryToAdd).value;
	  	}
	  }
	  else if( inventoryItem.approach == "O" )
	  {
	  	inventoryItem.otherNotes = document.getElementById("otherNotes" + inventoryToAdd).value;
	  }
	  else if( inventoryItem.approach == "S" )
	  {
		inventoryItem.soldNotes = document.getElementById("soldNotes" + inventoryToAdd).value;
		inventoryItem.saDate = document.getElementById("saDate" + inventoryToAdd).value;
	  	inventoryItem.saName = document.getElementById("saName" + inventoryToAdd).value;
	  	inventoryItem.retail = getRadioValue( document.getElementsByName("saRetail" + inventoryToAdd ) );
	  }

	  inventory[inventory.length] = inventoryItem;

    }
    
    if( !isExplicitSave && dirtyInventoryRef.length > 0)
    {
    	sentInventory[++autoSaveRequestId] = new Array();
    	for (var i = 0; i < dirtyInventoryRef.length; i++) {
    		sentInventory[autoSaveRequestId].push(dirtyInventoryRef[i]);
    	}
    }

	return inventory;
}


function createCheckbox(strName,boolChecked,strTitle) {
    // title is an inert attribute that is used only for automating regression testing
    var stateOfInput,wInputString,wInput;
    stateOfInput = (boolChecked == true) ? " checked" : "";
    wInputString = "<input name='" + strName + this.vid + "' " + "id='"
        + strName + this.vid + "'" + " "+ stateOfInput + " onclick='addToDirtyQueue(" + this.vid + ")'>";
    wInput = document.createElement(wInputString);
    wInput.type = "checkbox";
    wInput.style.width = "12px";
    //wInput.onclick = addToDirtyQueue( this.vid );
    return wInput;
}

function createRadio(strName,boolChecked) {
    var stateOfInput,wInputString,wInput;
    stateOfInput = (boolChecked == true) ? " checked" : "";
    wInputString = "<input name='sold" + this.vid + "' " + "value='" + strName + "' id='" + strName + this.vid + "'" + stateOfInput + " onclick='addToDirtyQueue(" + this.vid + ")'>";
    wInput = document.createElement(wInputString);
    wInput.type = "radio";
    wInput.style.width = "12px";
    //wInput.onclick = addToDirtyQueue( this.vid );
    return wInput;
}

function createRetailCheckbox(strName,bool2Checked,stuff) {
    var stateOfRetailInput,wInputString,wInput;
    var stuff = " " + stuff;
    stateOfRetailInput = (bool2Checked == true) ? " checked" : "";
    wInputString = "<input name='" + strName + this.vid + "' " + "id='" + strName + this.vid + "'" + stateOfRetailInput + stuff + " onclick='addToDirtyQueue(" + this.vid + ")'>";
    wInput = document.createElement(wInputString);
    wInput.type = "checkbox";
    return wInput;
}
function createLabel (strLabel, strId) {
    var objLabel = document.createElement("label");
    objLabel.htmlFor = strId + this.vid;
    objLabel.innerText = strLabel;
    return objLabel;
}
function createTextBox(strName, strTitle) {
// title is an inert attribute that is used only for automating regression testing
    var wInputMaxlength = 50;
    if(strName == "retailReprice") {this.wInputMaxlength = 6;}
    else {this.wInputMaxlength = 50;}
    var wInputString = "<input name='" + strName + this.vid + "' " + " maxlength='" + this.wInputMaxlength
    	+ "' id='" + strName + this.vid + "' onchange='addToDirtyQueue(" + this.vid + ")'>";
    wInput = document.createElement(wInputString);
    wInput.type = "text";
    if(strName == "saName") {wInput.value = this.nameText;}
    if(strName == "saDate") {wInput.value = this.date;}
    if(strName == "retailReprice") {wInput.value = this.reprice;}
    if(strName == "spiffNotes") {wInput.value = this.spiffNotes;}
    //addToDirtyQueue( this.vid );
    return wInput;
}
function createTable(tid) {
    var objTable = document.createElement("table");
    objTable.cellPadding = 0;
    objTable.cellsSpacing = 0;
    objTable.border = 0;
    objTable.id = tid + this.vid;
    return objTable;
}
function createTextarea(textAreaName, strTitle) {
// title is an inert attribute that is used only for automating regression testing
    var wInputString = "<textarea class='trackingNotesTextArea' name='" + textAreaName + this.vid + "' onKeyUp='limitText(" + textAreaName +  this.vid +",255);' " + "id='" + textAreaName + this.vid + "' onclick='addToDirtyQueue(" + this.vid + ")' cols='35' rows='2'></textarea>";
    var objTA = document.createElement(wInputString);
    objTA.innerText = this.notes;
    //objTA.onchange = addToDirtyQueue( this.vid );
    return objTA;
}
var r1WLabels = new Array();
r1WLabels[0] = "Wholesaler";
r1WLabels[1] = "Auction";
r1WLabels[2] = "Name";
r1WLabels[3] = "Date";
var r1RLabels = new Array();
r1RLabels[0] = "Reprice,reprice";
r1RLabels[1] = "SPIFF,Spiffs";
r1RLabels[2] = "Lot Promote,Promos";
r1RLabels[3] = "Advertise,Advertise";
r1RLabels[4] = "Other,saOther";
var r1SLabels = new Array();
r1SLabels[0] = "Wholesale";
r1SLabels[1] = "Retail";
r1SLabels[2] = "Name";
r1SLabels[3] = "Date";
function writeVehicle( appliedLastWeeksNotes ) {
    var objRow, strLabel, objCell, wInput;
    if( this.approach != "" && appliedLastWeeksNotes )
    {
    	addToDirtyQueue(this.vid);
    }
    if (this.approach == "S") {
        var elementTitleRoot = "approachSold";
        var vTable = this.createTable("Wholesale");
        vTable.width = "100%";
        var vTBody = document.createElement("tbody");
        vTable.appendChild(vTBody);
        var objRowMain = document.createElement("tr");
        objRowMain.id = "mainRow";
        objRowMain.vAlign = "top";
        vTBody.appendChild(objRowMain);
        var objCellWLeft = document.createElement("td");
        objCellWLeft.style.paddingTop = "1px";
        var objNewTableLeft = this.createTable("wLeft");
        var vTBodyLeft = document.createElement("tbody");
        objNewTableLeft.appendChild(vTBodyLeft);
        objCellWLeft.appendChild(objNewTableLeft);
            var objRowWL1 = document.createElement("tr"); vTBodyLeft.appendChild(objRowWL1);
            objCell = document.createElement("td");
                objCell.className = "dataLeft";
                objCell.height = "23";
                objCell.style.paddingBottom = "4px";
                objCell.style.verticalAlign = "bottom";
                objCell.noWrap = "true";
                strLabel = this.createLabel(r1SLabels[0] + ": ", "sa" + r1SLabels[0]);
                objCell.appendChild(strLabel);
            objRowWL1.appendChild(objCell);
            objCell = document.createElement("td");
                objCell.style.verticalAlign = "bottom";
                wInput = this.createRadio("sa" + r1SLabels[0] ,eval("this." + r1SLabels[0].toLowerCase()));
                    wInput.align = "middle";
                objCell.appendChild(wInput);
            objRowWL1.appendChild(objCell);
            var objRowWL2 = document.createElement("tr"); vTBodyLeft.appendChild(objRowWL2);
            objCell = document.createElement("td");
                objCell.className = "dataLeft";
                objCell.style.height = "23px";
                objCell.style.paddingBottom = "4px";
                objCell.style.verticalAlign = "bottom";
                objCell.noWrap = "true";
                strLabel = this.createLabel(r1SLabels[1] + ": ", "sa" + r1SLabels[1]);
                objCell.appendChild(strLabel);
            objRowWL2.appendChild(objCell);
            objCell = document.createElement("td");
                objCell.style.verticalAlign = "bottom";
                wInput = this.createRadio("sa" + r1SLabels[1] ,eval("this." + r1SLabels[1].toLowerCase()));
                    wInput.align = "bottom";
                objCell.appendChild(wInput);
            objRowWL2.appendChild(objCell);
        objRowMain.appendChild(objCellWLeft);
        var objCellWRight = document.createElement("td");
            objCellWRight.align = "right";
        var objNewTableR = this.createTable("wRight");
        var vTBodyR = document.createElement("tbody");
        objNewTableR.appendChild(vTBodyR);
objCellWRight.appendChild(objNewTableR);
var objRowWx = document.createElement("tr"); vTBodyR.appendChild(objRowWx);
        for(var j=2;j<4;j++) {
            objCell = document.createElement("td");
                objCell.className = "dataLeft";
                objCell.style.verticalAlign = "bottom";
                objCell.style.paddingTop = "0px";
                objCell.style.paddingBottom = "3px";
                strLabel = this.createLabel(r1SLabels[j] + ": ", "sa" + r1SLabels[j]);
                objCell.appendChild(strLabel);
            objRowWx.appendChild(objCell);

            objCell = document.createElement("td");
                objCell.className = "dataLeft";
                objCell.style.verticalAlign = "bottom";
                objCell.style.paddingTop = "1px";
                objCell.style.paddingLeft = "0px";
                wInput = this.createTextBox("sa" + r1SLabels[j], elementTitleRoot + r1SLabels[j]);
                    wInput.align = "bottom";
                    wInput.size = "6";
                    wInput.className = "trackingTBox";
                objCell.appendChild(wInput);
            objRowWx.appendChild(objCell);
        }
        objRow = document.createElement("tr"); vTBodyR.appendChild(objRow);
        objCell = document.createElement("td");
            objCell.colSpan = "4";
        objRow.appendChild(objCell);
        var notesSoldTable = this.createTable("wholeSoldNotesTable");
            notesSoldTable.width = "100%";
        objCell.appendChild(notesSoldTable);
        var notesSoldTBody = document.createElement("tbody");
        notesSoldTable.appendChild(notesSoldTBody);
            var objRowTop = document.createElement("tr");
                objRowTop.vAlign = "top";
            notesSoldTBody.appendChild(objRowTop);
                var objCellNotesContainer = document.createElement("td");
                objRowTop.appendChild(objCellNotesContainer);
                    var notesTable = this.createTable("wholeNotesTable");
                    objCellNotesContainer.appendChild(notesTable);
                    var vTBody3 = document.createElement("tbody");
                    notesTable.appendChild(vTBody3);
                        objRow = document.createElement("tr");
                        vTBody3.appendChild(objRow);
                            objCell = document.createElement("td");
                                objCell.className = "dataLeft";
                                objCell.style.verticalAlign = "top";
                                objCell.padding = "0px";
                                    strLabel = this.createLabel("Notes: ", "soldNotes");
                                objCell.appendChild(strLabel);
                            objRow.appendChild(objCell);
                            objCell = document.createElement("td");
                                    wInput = this.createTextarea("soldNotes", elementTitleRoot + "Notes");
                                objCell.appendChild(wInput);
                            objRow.appendChild(objCell);
        objRowMain.appendChild(objCellWRight);
        this.container.appendChild(vTable);
    }
    if (this.approach == "W") {
        var elementTitleRoot = "approachWholesale";
        var vTable = this.createTable("Wholesale");
        vTable.width = "100%";
        var vTBody = document.createElement("tbody");
        vTable.appendChild(vTBody);
        var objRowMain = document.createElement("tr");
        objRowMain.id = "mainRow";
        objRowMain.vAlign = "top";
        vTBody.appendChild(objRowMain);
        var objCellWLeft = document.createElement("td");
        objCellWLeft.style.paddingTop = "1px";
        var objNewTableLeft = this.createTable("wLeft");
        var vTBodyLeft = document.createElement("tbody");
        objNewTableLeft.appendChild(vTBodyLeft);
        objCellWLeft.appendChild(objNewTableLeft);
            var objRowWL1 = document.createElement("tr"); vTBodyLeft.appendChild(objRowWL1);
            objCell = document.createElement("td");
                objCell.className = "dataLeft";
                objCell.height = "23";
                objCell.style.paddingBottom = "4px";
                objCell.style.verticalAlign = "bottom";
                objCell.noWrap = "true";
                strLabel = this.createLabel(r1WLabels[0] + ": ", "sa" + r1WLabels[0]);
                objCell.appendChild(strLabel);
            objRowWL1.appendChild(objCell);
            objCell = document.createElement("td");
                objCell.style.verticalAlign = "bottom";
                wInput = this.createCheckbox("sa" + r1WLabels[0] ,eval("this." + r1WLabels[0].toLowerCase()), elementTitleRoot + "Wholesaler");
                    wInput.align = "middle";
                objCell.appendChild(wInput);
            objRowWL1.appendChild(objCell);
            var objRowWL2 = document.createElement("tr"); vTBodyLeft.appendChild(objRowWL2);
            objCell = document.createElement("td");
                objCell.className = "dataLeft";
                objCell.style.height = "23px";
                objCell.style.paddingBottom = "4px";
                objCell.style.verticalAlign = "bottom";
                objCell.noWrap = "true";
                strLabel = this.createLabel(r1WLabels[1] + ": ", "sa" + r1WLabels[1]);
                objCell.appendChild(strLabel);
            objRowWL2.appendChild(objCell);
            objCell = document.createElement("td");
                objCell.style.verticalAlign = "bottom";
                wInput = this.createCheckbox("sa" + r1WLabels[1] ,eval("this." + r1WLabels[1].toLowerCase()), elementTitleRoot + "Auction");
                    wInput.align = "bottom";
                objCell.appendChild(wInput);
            objRowWL2.appendChild(objCell);
        objRowMain.appendChild(objCellWLeft);
        var objCellWRight = document.createElement("td");
            objCellWRight.align = "right";
        var objNewTableR = this.createTable("wRight");
        var vTBodyR = document.createElement("tbody");
        objNewTableR.appendChild(vTBodyR);
objCellWRight.appendChild(objNewTableR);
var objRowWx = document.createElement("tr"); vTBodyR.appendChild(objRowWx);
        for(var j=2;j<4;j++) {
            objCell = document.createElement("td");
                objCell.className = "dataLeft";
                objCell.style.verticalAlign = "bottom";
                objCell.style.paddingTop = "0px";
                objCell.style.paddingBottom = "3px";
                strLabel = this.createLabel(r1WLabels[j] + ": ", "sa" + r1WLabels[j]);
                objCell.appendChild(strLabel);
            objRowWx.appendChild(objCell);

            objCell = document.createElement("td");
                objCell.className = "dataLeft";
                objCell.style.verticalAlign = "bottom";
                objCell.style.paddingTop = "1px";
                objCell.style.paddingLeft = "0px";
                wInput = this.createTextBox("sa" + r1WLabels[j], elementTitleRoot + r1WLabels[j]);
                    wInput.align = "bottom";
                    wInput.size = "6";
                    wInput.className = "trackingTBox";
                objCell.appendChild(wInput);
            objRowWx.appendChild(objCell);
        }
        objRow = document.createElement("tr"); vTBodyR.appendChild(objRow);
        objCell = document.createElement("td");
            objCell.colSpan = "4";
        objRow.appendChild(objCell);
        var notesSoldTable = this.createTable("wholeSoldNotesTable");
            notesSoldTable.width = "100%";
        objCell.appendChild(notesSoldTable);
        var notesSoldTBody = document.createElement("tbody");
        notesSoldTable.appendChild(notesSoldTBody);
            var objRowTop = document.createElement("tr");
                objRowTop.vAlign = "top";
            notesSoldTBody.appendChild(objRowTop);
                var objCellNotesContainer = document.createElement("td");
                objRowTop.appendChild(objCellNotesContainer);
                    var notesTable = this.createTable("wholeNotesTable");
                    objCellNotesContainer.appendChild(notesTable);
                    var vTBody3 = document.createElement("tbody");
                    notesTable.appendChild(vTBody3);
                        objRow = document.createElement("tr");
                        vTBody3.appendChild(objRow);
                            objCell = document.createElement("td");
                                objCell.className = "dataLeft";
                                objCell.style.verticalAlign = "top";
                                objCell.padding = "0px";
                                    strLabel = this.createLabel("Notes: ", "wholesaleNotes");
                                objCell.appendChild(strLabel);
                            objRow.appendChild(objCell);
                            objCell = document.createElement("td");
                                    wInput = this.createTextarea("wholesaleNotes", elementTitleRoot + "Notes");
                                objCell.appendChild(wInput);
                            objRow.appendChild(objCell);
        objRowMain.appendChild(objCellWRight);
        this.container.appendChild(vTable);
    }
    if (this.approach == "R") {
        var elementTitleRoot = "approachRetail";
        var labelArray;
        var vTable = this.createTable("Wholesale");
        vTable.width = "100%";
        var vTBody = document.createElement("tbody");
        vTable.appendChild(vTBody);
        objRow = document.createElement("tr"); vTBody.appendChild(objRow);
        for(var m=0;m<r1RLabels.length;m++) {
            labelArray = r1RLabels[m].split(",");
            objCell = document.createElement("td");
                objCell.className = "dataLeft";
                objCell.style.verticalAlign = "bottom";
                objCell.noWrap = "true";
                strLabel = this.createLabel(labelArray[0] + ": ", labelArray[1]);
                objCell.appendChild(strLabel);
                wInput = this.createCheckbox(labelArray[1] ,eval("this." + labelArray[1].toLowerCase()), elementTitleRoot + labelArray[0]);
                    wInput.align = "bottom";
                objCell.appendChild(wInput);
            objRow.appendChild(objCell);
        }
        objRow = document.createElement("tr");
        vTBody.appendChild(objRow);
            objCell = document.createElement("td");
            objCell.colSpan="5";
            objRow.appendChild(objCell);
                var table2 = this.createTable("rSubContainer");
                    table2.width = "100%";
                objCell.appendChild(table2);
                var tbody2 = document.createElement("tbody");
                    table2.appendChild(tbody2);
                    objRowTop = document.createElement("tr");
                    objRowTop.vAlign = "top";
                    tbody2.appendChild(objRowTop);
                        objCell = document.createElement("td");
                        objCell.vAlign = "middle";
                    objRowTop.appendChild(objCell);

                var objCellNotesContainer = document.createElement("td");
                    objCellNotesContainer.align = "right";
                objRowTop.appendChild(objCellNotesContainer);
                    var notesTable = this.createTable("retailNotesTable");
                    notesTable.border="0";
                    objCellNotesContainer.appendChild(notesTable);
                    var vTBody3 = document.createElement("tbody");
                    notesTable.appendChild(vTBody3);
                        objRow = document.createElement("tr");
                        vTBody3.appendChild(objRow);

                            objCell = document.createElement("td");
                                objCell.className = "dataRight";
                                objCell.style.verticalAlign = "top";
                                objCell.padding = "0px";
                                strLabel = this.createLabel("Reprice:", "retailReprice");
                                objCell.appendChild(strLabel);
                            objRow.appendChild(objCell);
                            objCell = document.createElement("td");
                                objCell.style.paddingRight = "10";
                                    wInput = this.createTextBox("retailReprice", elementTitleRoot + "Reprice");
                                    wInput.size = "6";
                    wInput.disabled = wInput.value.length == 0;
                                    wInput.onClick="onClickReprice(this);";
                                objCell.appendChild(wInput);
                            objRow.appendChild(objCell);

                            objCell = document.createElement("td");
                                objCell.className = "dataRight";
                                objCell.style.verticalAlign = "top";
                                objCell.padding = "0px";
                                    strLabel = this.createLabel("SPIFF: ", "retailSpliff");
                                objCell.appendChild(strLabel);
                            objRow.appendChild(objCell);
                            objCell = document.createElement("td");
                                    wInput = this.createTextBox("spiffNotes", elementTitleRoot + "Spiff");
                                    wInput.size = "6";
                                objCell.appendChild(wInput);
                                objCell.style.paddingRight = "10";
                            objRow.appendChild(objCell);


                            objCell = document.createElement("td");
                                objCell.className = "dataRight";
                                objCell.style.verticalAlign = "top";
                                objCell.padding = "0px";
                                    strLabel = this.createLabel("Notes: ", "retailNotes");
                                objCell.appendChild(strLabel);
                            objRow.appendChild(objCell);
                            objCell = document.createElement("td");
                            objCell.rowSpan = "2";
                                    wInput = this.createTextarea("retailNotes", elementTitleRoot + "Notes");
                                    wInput.rows = "3";
                                    wInput.cols = "30";
                                objCell.appendChild(wInput);
                            objRow.appendChild(objCell);

                        objRow = document.createElement("tr");
                        vTBody3.appendChild(objRow);

                            objCell = document.createElement("td");
                            objRow.appendChild(objCell);
                            objCell = document.createElement("td");
                                objCell.colSpan = "2";
                                objCell.className = "dataRight";
                                objCell.style.verticalAlign = "top";
                                objCell.padding = "0px";
                                    strLabel = this.createLabel("Special Finance: ", "specialFinancing");
                                objCell.appendChild(strLabel);
                            objRow.appendChild(objCell);
                            objCell = document.createElement("td");
                                objCell.style.paddingRight = "6px";
                                    wInput = this.createCheckbox("specialFinancing" ,eval(this.specialFinancing), elementTitleRoot + labelArray[0]);
                                    objCell.style.verticalAlign = "top";
                                 objCell.appendChild(wInput);
                            objRow.appendChild(objCell);


        this.container.appendChild(vTable);

    var repriceCheckbox = document.getElementById("Reprice" + this.vid);
    repriceCheckbox.inputTextField=document.getElementById("retailReprice" + this.vid );
    repriceCheckbox.checked = repriceCheckbox.inputTextField.value.length > 0;
    repriceCheckbox.isARepriceCheckbox="true";
    repriceCheckbox.onclick=onClickReprice;

//  var x=window.open("_blank")
//  x.document.write(vTable.outerHTML)
    }
    if (this.approach == "O") {
        var labelArray;
        var vTable = this.createTable("Other");
        vTable.width = "100%";
        var vTBody = document.createElement("tbody");
        vTable.appendChild(vTBody);
        objRow = document.createElement("tr"); vTBody.appendChild(objRow);
            objCell = document.createElement("td");
            objCell.align = "right";
                            objCell.style.paddingRight = "4px";
            objRow.appendChild(objCell);
                var table2 = this.createTable("OtherSetupTable");
                objCell.appendChild(table2);
                    var vTBody2 = document.createElement("tbody");
                    table2.appendChild(vTBody2);
                        objRow = document.createElement("tr"); vTBody2.appendChild(objRow);
                            objCell = document.createElement("td");
                            objCell.className = "dataLeft";
                            objCell.vAlign = "top";
                            objCell.style.verticalAlign = "top";
                                strLabel = this.createLabel("Notes: ","otherNotes");
                            objCell.appendChild(strLabel);
                        objRow.appendChild(objCell);
                            objCell = document.createElement("td");
                                wInput = this.createTextarea ("otherNotes", elementTitleRoot + "Notes");
                            objCell.appendChild(wInput);
                        objRow.appendChild(objCell);
        this.container.appendChild(vTable);
}   }
vehicle.prototype.writeVehicle = writeVehicle;
vehicle.prototype.createCheckbox = createCheckbox;
vehicle.prototype.createRadio = createRadio;
vehicle.prototype.createLabel = createLabel;
vehicle.prototype.createTextBox = createTextBox;
vehicle.prototype.createTable = createTable;
vehicle.prototype.createTextarea = createTextarea;
vehicle.prototype.createRetailCheckbox = createRetailCheckbox;
function doSpan(obj,ind, appliedLastWeeksNotes) {
    vehicleArray[ind] = new vehicle(obj[0],obj[1],obj[2],obj[3],obj[4],obj[5],obj[6],obj[7],obj[8],obj[9],obj[10],obj[11],obj[12],obj[13],obj[14],obj[15],obj[16]);
    vehicleArray[ind].writeVehicle( appliedLastWeeksNotes );
}
/**
 *	Callback for when the user has clicked the "reprice" checkbox (under the retail radio option).
 */
function onClickReprice()
{
    this.inputTextField.disabled = !this.checked;
    if (this.inputTextField.disabled)
        this.inputTextField.value = '';
    addToDirtyQueue(this.inputTextField.name.substring(13));
}

function applyLastWeeksNotes()
{
    var agree = true;

    if (haveUnacknowledgedChanges())
        agree=confirm("Applying last week's notes \n will cause changes to be lost. \n Are you sure you wish to continue?");

    if ( agree )
    {
        document.agingInvSelectionForm.action="AgingInventoryPlanningSheetTrackingDisplayAction.go?applyPriorAgingNotes=true\&fromTab=true";
        document.agingInvSelectionForm.submit();
    }
}

function checkHeight() {
    var inv = document.getElementById("inventoryReportHeader").clientHeight;
    var high = document.getElementById("inventoryThemeHeader").clientHeight;
    var invSpacer = document.getElementById("invSpacer");
    var highSpacer = document.getElementById("highlightSpacer");
    if (inv < high) {invSpacer.height = high - inv + 1;}
    if (high < inv) {highSpacer.height = inv - high + 1;}
    rendered = true;
}

function checkRendered(){
	return rendered;}

function saleSold(inputBox) {
    var boxId = inputBox.id;
    var saleOrSold = (boxId.indexOf("Sold") != -1) ? "Sold" : "SalePending";
    var vehicleId =  (saleOrSold == "Sold") ? boxId.substr(4) : boxId.substr(11);
    var otherId = (saleOrSold == "Sold") ? "SalePending" + vehicleId : "Sold" + vehicleId;
    var toggleBox = document.getElementById(otherId);
    if (toggleBox.checked) {
        toggleBox.click();
}   }
var plusHeight = window.screen.availHeight;
var plusWidth =  window.screen.availWidth;
if (plusWidth < 1024) {
    plusWidth = 800;
    plusHeight = 550;
} else {
    plusWidth= 800;
    plusHeight -= 30;
}
function toggleDisabled(formField)
{
    (formField.disabled == true) ? formField.disabled = false:formField.disabled = true;
}
function oPW(gid,mil,weeks) {
    var path = "PopUpPlusDisplayAction.go?groupingDescriptionId=" + gid + "&mileage=" + mil + "&weeks=" + weeks +"&forecast=0&mode=UCBP&mileageFilter=1";
    window.open(path, 'plusAge','width='+ plusWidth + ',height=' + plusHeight + ',location=no,status=no,menubar=no,titlebar=no,toolbar=no,resizable=yes,scrollbars=yes');
}
function saveSubmit() {
    if (validate())
    {
        document.agingInvSelectionForm.action="AgingInventoryPlanningSheetTrackingSubmitAction.go?fromTab=true";
        document.agingInvSelectionForm.submit();
    }
}

function validate() {
    if(!document.forms[0]) return;
    var foo = document.forms[0].elements;

    for (var i=0;i<foo.length;i++)
        if (foo[i].type.toLowerCase() == "checkbox" && foo[i].checked && foo[i].isARepriceCheckbox)
        {
            if (!isANumber(foo[i].inputTextField.value))
            {
                alert(foo[i].inputTextField.value + ' is not a valid number');
                foo[i].focus();
                return false;
        }
    }
    return true;
}

function isANumber(sText)
{
    var ValidChars = "0123456789";
    var IsNumber=true;
    var Char;

    for (i = 0; i < sText.length && IsNumber == true; i++)
    {
        Char = sText.charAt(i);
        if (ValidChars.indexOf(Char) == -1)
        {
            IsNumber = false;
        }
    }
    return IsNumber;
}

function checkMainRadios() {
    if(!document.forms(0)) return;
    var foo = document.forms(0).elements;
    for (var i=0;i<foo.length;i++) {
        if (foo[i].type.toLowerCase() == "radio" && foo[i].checked) {
            if(foo[i].parentElement.children[1])
            {
                foo[i].parentElement.children[1].style.fontWeight = "bold";
}   }   }  }
