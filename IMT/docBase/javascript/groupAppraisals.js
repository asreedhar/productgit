var groupApps = new Popup();	

function showInGroupAppraisals(vin, bOnTa) {
	if($('personManager')) {
		PersonManager.close();
	}
	
	var xPos = 15;
	var yPos = 260;
	if (bOnTa == 'true') {
		xPos = 535;
		yPos = 1300;
	}
	pars = 'vin='+vin;
	new Ajax.Request(
			'/IMT/InGroupAppraisalsDisplayAction.go',
			 { method: 'get', parameters: pars, 
				onComplete: function(xhr){ 
			 	$('groupAppraisals').innerHTML = xhr.responseText;				 	
				groupApps.popup_show('groupAppraisals','dragApp','','screen-corner',0,false,xPos,yPos);
			},
				onFailure:function(xhr){
					alert('The system has experienced a problem processing your last request.\nIf the problem persists, please contact your account manager or call Customer Service at 1-877-378-5665.');
				}
		});
}

function closeInGroupAppraisals() {
	if($('groupAppraisals')) {
		$('groupAppraisals').style.display = 'none';
	}
	groupApps.refresh();
}

function getPreviousResult(activeNum) { pageResults(activeNum,'previous'); }
function getNextResult(activeNum) { pageResults(activeNum,'next'); }
function pageResults(activeNum,whichWay) {
	var currentEl = $('result'+activeNum);
	var nextEl = whichWay=='previous' ? $('result'+(activeNum-1)) : $('result'+(activeNum+1));
	Element.toggle(currentEl,nextEl);
}
