

var Util = {

	initialize:function(){	},
	/**/
	/*Method closes the browser window
	/*
	/**/
	closeWindow:function(){
		window.close();
	},
	
	/**/
	/*Method turns price with non numeric vals into a number
	/*
	/**/
	 formatPrice:function(price)
	{
		var indexOfPeriod = price.indexOf(".");
		if (indexOfPeriod >= 0)
		{
			price = price.substring(0, indexOfPeriod);
		}
		var indexOfComma = price.indexOf(",");
		if (indexOfComma >= 0)
		{
			price = price.substring(0, indexOfComma) + price.substring( (indexOfComma + 1), price.length );
		}
		var indexOfDollar = price.indexOf("$");
		if (indexOfDollar >= 0)
		{
			price = price.substring( (indexOfDollar + 1), price.length );
		}
		return price;
	},	
	/**/
	/*Method to scroll and center an element on screen
	/*
	/**/
	scrollWindowTo:function(id){

		var coords = this.findPosition($(id));  //find coordinates of the element
		var winHeight =  this.getViewportDims();  //get height of browser window
		var eleHeight = $(id).offsetHeight;
		
		if(!eleHeight){
		
			eleHeight = 0;
		}
		
		var yLoc = coords[1] - ((winHeight[1]/2) -  (eleHeight/2));  //y location of element =element  position - .5 window height - .5 element height
		
		window.scrollTo(0,yLoc);   //scroll window to this location

	},
	
	/**/
	/*Method accepts an element as a parameter and returns an array representing
	/*the x and y coordinates of the element
	/**/
	findPosition:function(oElem) {
		if ($(oElem).offsetParent) {	
			for (var posX = 0, posY = 0; $(oElem).offsetParent; oElem = $(oElem).offsetParent) {
				posX += $(oElem).offsetLeft;
				posY += $(oElem).offsetTop;
	    	}
			return [posX, posY];
		} 
		else {
			return [$(oElem).x, $(oElem).y];
	  	}
	},
	/**/
	/*Method a returns an array representing
	/*the x and y coordinates of the screen
	/**/
	findScreenSize:function() {
		var x; var y;
		var x = window.screen.width;
		var y = window.screen.height;
		return [x, y];	
	},
	/**/
	/*Method a returns an array representing
	/*the x and y coordinates of the viewport - chrome
	/**/
	 findViewportDims:function() {
	var x; var y;
		// non-explorer
	if (self.innerHeight) {
		x = self.innerWidth;
		y = self.innerHeight;
	}
		// explorer 6 strict
	else if (document.documentElement && document.documentElement.clientHeight)	{
		x = document.documentElement.clientWidth;
		y = document.documentElement.clientHeight;
	}
		// other explorers
	else if (document.body) {
		x = document.body.clientWidth;
		y = document.body.clientHeight;
	}
	//viewX = x;
	//viewY = y;
	return [x, y];
	},
	
	printPage:function(){
	
		window.print();
	
	},
	/**/
	/*Method returns boolean true if obj is an array
	/**/
	isArray:function(obj){
 
 		 return this.isObject(obj) && obj.constructor == Array;

	},
	 isFunction:function(a) {
  		  return typeof a == 'function';
	},
	isObject:function(a) {
   		 return (a && typeof a == 'object') || this.isFunction(a);
	},
	stripTags:function(str) {
	var s = 0;
	var e = -1;
	var r = "";

	s = str.indexOf("<",e);	

	do {
		r += str.substring(e + 1,s);
		e = str.indexOf(">",s);
		s = str.indexOf("<",e);
	}
	while ((s != -1) && (e != -1))

	r += str.substring(e + 1,str.length);

	return r;
},
	debug:function(msg){
	
		alert("Error: "+msg);
		
	}
}


//AjaxLoader class
function AjaxLoader(msg, pth, params, showLoading){

	var p;
	var div;
	var path = pth;
	var parameters = params;
	var element;
	var message = msg;
	var success;
	var failure;
	
	
	this.showWindow = showWindow;
	this.hideWindow = hideWindow;
	this.setParameters = setParameters;
	this.setElement = setElement;
	this.performAction = performAction;
	this.setPath = setPath;
	this.setOnSuccess =  setOnSuccess;
	this.setOnFailure =  setOnFailure;
	this.setMessage =  setMessage;
	
	function showWindow(){  //create loading window if does not exist, fill it with message, then show it.
	
	        div = '<div id="loadingDiv"  style="z-index:1050; border:1px solid black;background-color:white; padding:10px; width:50px; height:20px; position:absolute; display:none;text-align:center; font-weight:bold;"><img src="common/_images/icons/loading.gif"/><br/>'+message+'</div>';
			if(!$('loadingDiv ')){
						new Insertion.After($('header'),div);
			}else{
				$('loadingDiv').innerHTML='<img src="common/_images/icons/loading.gif"/><br/>'+message;
			}
		 p = new Popup();
		 p.popup_show('loadingDiv', '', '', 'screen-center',100,false,0,0);
	}
	function hideWindow(){
		p.popup_exit();
	}
	function setParameters(params){
		parameters = params;
	}
	function setPath(pth){
	
		path = pth;
	}
	function setElement(ele){
	
		element = ele;
	}

	function setMessage(msg){
	
		message = msg;
	}
	
	//set function to perform on success
	function setOnSuccess(fun){
	
	 	success = fun;

	}
	//set function to perform on failure
	function setOnFailure(fun){
	
	 	failure = fun;

	}
	
	function performAction(){
	
		if(msg != null && msg != ''){
			this.showWindow();  //display "loading window"
		}
		
		if(path){
			new Ajax.Request(
				path,
				{
					method: 'post',
					parameters: stampURL(parameters),
					onSuccess: function(r,j) {
				
					
						if($(element)){
							$(element).innerHTML = r.responseText;
						}
						if(success){
							success(r,j);
						}
						
					},
					onComplete: function(){
						if(msg != null && msg != ''){
							hideWindow();
						}
					},
					onFailure: function(){
						if(failure){
							failure(r,j);
						}
					}
				}
			);
		}else{
		
			alert('Error:Ajax path has not been set.');
			hideWindow();
		}
	}
} //end ajax loader class

