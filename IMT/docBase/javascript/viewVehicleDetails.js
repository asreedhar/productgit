function getPreviousResult(activeNum) { pageResults(activeNum,'previous'); }
function getNextResult(activeNum) { pageResults(activeNum,'next'); }
function pageResults(activeNum,whichWay) {
	var currentEl = $('result'+activeNum);
	var nextEl = whichWay=='previous' ? $('result'+(activeNum-1)) : $('result'+(activeNum+1));
	Element.toggle(currentEl);
	Element.toggle(nextEl);
}
