function setVAItems(N) {
	clr=false;
	if(N<depth-1) {//unless this is the trim select...
		mmm = X; //grab the main array
		for(i=0;i<=N;i++) { //Determine whether we have selected a real item in the option list
			sel = eval("document.vehicleAnalyzerForm."+selects[i]);
			selinx = sel.selectedIndex-1;
			if(selinx<0) break; //if we have not then move on using the base array
			mmm=mmm.s[selinx]; //if we have then set the array to use to the subarray chosen
	  	}
		sel = eval("document.vehicleAnalyzerForm."+selects[i]);
		setmenu(sel,mmm.s); // use it to set the menu
		if(document.vehicleAnalyzerForm.model.selectedIndex != 0) {
		 	document.vehicleAnalyzerForm.trim.options[0].text = "ALL (Click for Trim)";
		} else {
			document.vehicleAnalyzerForm.trim.options[0].text = "ALL";
		}
		i++;
		while(i<depth) {
			sel = eval("document.vehicleAnalyzerForm."+selects[i]);
			clearmenu(sel);
			i++;
		}
	}
}
function validateVehicleAnalyzerForm(thisForm)
{
	var Vmessage = "Vehicle Analyzer:\n\n";
	var isValid = true;
	if(0 == thisForm.make.selectedIndex) {
		Vmessage += "Select Make\n";
		isValid = false;
	}
	if(0 == thisForm.model.selectedIndex) {
		Vmessage += "Select Model\n";
		isValid = false;
	}
	if(0 == thisForm.trim.selectedIndex) {
		thisForm.trim.value = null;
	}
	if(!isValid) {
		alert(Vmessage);

	} else {
		thisForm.submit();
	}

	return isValid;
}

	for (i=0;i<depth;i++) {
		d.writeln("<select name='"+selects[i]+"' onchange='setVAItems("+i+")' tabindex=" + (i+6) + ">");
		if( i == (depth -1) ) {
			d.writeln("<option>ALL</option>");
		} 
		else {
			d.writeln("<option> Please Select "+labels[i]+ "</option>");
		}
		d.writeln("</select>");		
	}
	setVAItems(0,0);
