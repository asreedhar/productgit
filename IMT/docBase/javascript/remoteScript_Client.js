var jsrsContextPoolSize = 0;
var jsrsContextMaxPool = 10;
var jsrsContextPool = new Array();
var jsrsPOST = true;

function jsrsContextObj( contextID ){
  this.id = contextID;
  this.busy = true;
  this.callback = null;
  this.container = contextCreateContainer( contextID );
  
  this.POST = contextPOST;
  this.getPayload = contextGetPayload;
  this.setVisibility = contextSetVisibility;
}

function contextCreateContainer( containerName ){
  var container;

  document.body.insertAdjacentHTML( "afterBegin", '<span id="SPAN' + containerName + '"></span>' );
  var span = document.all( "SPAN" + containerName );
  var html = '<iframe name="' + containerName + '" src=""></iframe>';
  span.innerHTML = html;
  span.style.display = 'none';
  container = window.frames[ containerName ];

  return container;
}

function contextPOST( rsPage, func, parms ) {
  var d = new Date();
  var unique = d.getTime() + '' + Math.floor(1000 * Math.random());
  var doc = this.container.document;
  doc.open();
  doc.write('<html><body>');
  doc.write('<form name="jsrsForm" method="post" target="" ');
  doc.write(' action="' + rsPage + '?U=' + unique + '">');
  doc.write('							<input type="hidden" name="C" value="' + this.id + '">');

  if (func != null){
  doc.write('							<input type="hidden" name="F" value="' + func + '">');

    if (parms != null){
      if (typeof(parms) == "string"){
        doc.write( '					<input type="hidden" name="P0" ' + 'value="[' + jsrsEscapeQQ(parms) + ']">');
      } else {
        for( var i=0; i < parms.length; i++ ){
          doc.write( '					<input type="hidden" name="P' + i + '" ' + 'value="[' + jsrsEscapeQQ(parms[i]) + ']">');
        }
      }
    }
  }

  doc.write('</form></body></html>');
  doc.close();
  doc.forms['jsrsForm'].submit();
}

function contextGetPayload(){
      return this.container.document.forms['jsrs_Form']['jsrs_Payload'].value;
}

function contextSetVisibility( vis ){
      document.all("SPAN" + this.id ).style.display = (vis)? '' : 'none';
}

function jsrsGetContextID(){
  var contextObj;
  
  for (var i = 1; i <= jsrsContextPoolSize; i++){
    contextObj = jsrsContextPool[ 'jsrs' + i ];
    if ( !contextObj.busy ){
      contextObj.busy = true;      
      return contextObj.id;
    }
  }

  if ( jsrsContextPoolSize <= jsrsContextMaxPool ){
    var contextID = "jsrs" + (jsrsContextPoolSize + 1);
    jsrsContextPool[ contextID ] = new jsrsContextObj( contextID );
    jsrsContextPoolSize++;
    return contextID;
  } else {
    alert( "jsrs Error:  context pool full" );
    return null;
  }
}

function jsrsExecute( rspage, callback, func, parms, visibility ){
  var contextObj = jsrsContextPool[ jsrsGetContextID() ];

  contextObj.callback = callback;
  var vis = (visibility == null)? false : visibility;
  contextObj.setVisibility( vis );
  contextObj.POST( rspage, func, parms );

  return contextObj.id;
}

function jsrsLoaded( contextID ){
  var contextObj = jsrsContextPool[ contextID ];
  if( contextObj.callback != null){
    contextObj.callback( jsrsUnescape( contextObj.getPayload() ), contextID );
  }
  contextObj.callback = null;
  contextObj.busy = false;
}

function jsrsError( contextID, str ){
  alert( unescape(str) );
  jsrsContextPool[ contextID ].busy = false
}

function jsrsEscapeQQ( thing ){
  return thing.replace(/'"'/g, '\\"');
}

function jsrsUnescape( str ){
  return str.replace( /\\\//g, "/" );
}

function jsrsArrayFromString( s, delim ){
  var d = (delim == null)? '~' : delim;
  return s.split(d);
}

function jsrsDebugInfo(){
  var doc = window.open().document;
  doc.open;
  doc.write( 'Pool Size: ' + jsrsContextPoolSize + '<br><font face="arial" size="2"><b>' );
  for( var i in jsrsContextPool ){
    var contextObj = jsrsContextPool[i];
    doc.write( '<hr>' + contextObj.id + ' : ' + (contextObj.busy ? 'busy' : 'available') + '<br>');
    doc.write( contextObj.container.document.location.pathname + '<br>');
    doc.write( contextObj.container.document.location.search + '<br>');
    doc.write( '<table border="1"><tr><td>' + contextObj.container.document.body.innerHTML + '</td></tr></table>' );
  }
  doc.write('</table>');
  doc.close();
  return false;
}
