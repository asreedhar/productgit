function reloadPageWithParams(pAddParams) {
	if(loaded == true) {
		if(typeof(pAddParams) == "object") {
			if(pAddParams.length) {
				if(pAddParams.length > 0) {
					var arrQS = window.document.location.search.split('&');
					if(arrQS.length > 0) {
						arrQS[0] = arrQS[0].replace("?", "");
						if(arrQS.length == 1 && arrQS[0] == "") {
							arrQS = new Array();
						}
					}
					for(var intCtr=0; intCtr<arrQS.length; intCtr++) {
						arrQS[intCtr] = arrQS[intCtr].split("=");
					}
					for(var ctrAddParam=0; ctrAddParam<pAddParams.length; ctrAddParam+=2) {
						var isFound = false;
						if(arrQS.length > 0) {
							for(var ctrQS=0; ctrQS<arrQS.length; ctrQS++) {
								if(arrQS[ctrQS][0] == pAddParams[ctrAddParam]) {
									if(pAddParams.length > (ctrAddParam+1)) {
										arrQS[ctrQS][1] = pAddParams[ctrAddParam+1];
									}
									else {
										arrQS[ctrQS][1] = "";
									}
									isFound = true;
								}
							}
						}
						if(!isFound) {
								if(pAddParams.length > (ctrAddParam+1)) {
									arrQS.push(new Array(pAddParams[ctrAddParam], pAddParams[ctrAddParam+1]));
								}
								else {
									arrQS.push(new Array(pAddParams[ctrAddParam], ""));
								}
						}
					}
					var strNewQS = "";
					for(var ctrQS=0; ctrQS<arrQS.length; ctrQS++) {
						if(ctrQS>0) {
							strNewQS += "&";
						}
						else {
							strNewQS += "?";
						}
						strNewQS += arrQS[ctrQS][0] + "=" + arrQS[ctrQS][1];
					}
					window.document.location.search = strNewQS;
				}
			}
		}
	}
}
