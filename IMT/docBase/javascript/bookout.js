
//used for TA options selection
function submitFormToRetrieveNewOptions(gbid, selectedKey) {
	if(selectedKey == "selectAModel") {
        alert("Please select a model.");
        return false;
    }
    else{
  document.getElementById("fldn_temp_tradeAnalyzerForm").action="GuideBookStepTwoSubmitAction.go?" + "gbid=" + gbid + "&selectedKey=" + selectedKey;
  document.getElementById("fldn_temp_tradeAnalyzerForm").submit();
  }
}

//used for other bookout pages options
function changeSelectedVehicleKey(gbid, isActive, identifier, selectedKey, bookOutSourceId, editableMileage, displayValues, displayOptions, allowUpdate)
{
	if (selectedKey != '0') {
		// update the NAAA iframe if NADA dropdown was changed
		if (gbid == 2 && parent.document.getElementById('vic')) // NADA
		{	
		// NK - THIS NEEDS TO BE INVESTIGATED STILL !!!!!
//			parent.document.getElementById('vic').value = selectedKey;
//			parent.document.getElementById('vic').onchange();
		}
	
	    document.getElementById("fldn_temp_bookoutDetailForm").action="BookoutDetailSubmitAction.go?whatToDo=changeDropDowns" + "&guideBookId=" + gbid + "&isActive=" + isActive + "&identifier=" + identifier + "&selectedKey=" + selectedKey + "&bookOutSourceId=" + bookOutSourceId + "&editableMileage=" + editableMileage + "&displayValues=" + displayValues + "&displayOptions=" + displayOptions + "&allowUpdate=" + allowUpdate;
	    document.getElementById("fldn_temp_bookoutDetailForm").submit();
	}
}

function getDocHeight(doc) {
	  var docHt = 0, sh, oh;
	  if (doc.height) docHt = doc.height;
	  else if (doc.body) {
	    if (doc.body.scrollHeight) docHt = sh = doc.body.scrollHeight;
	    if (doc.body.offsetHeight) docHt = oh = doc.body.offsetHeight;
	    if (sh && oh) docHt = Math.max(sh, oh);
	  }
	  return docHt;
	}
function setIframeHeight(iframeName) {

	  var iframeWin = parent.frames[iframeName];
	  var iframeEl = parent.document.getElementById(iframeName);
	  if ( iframeEl && iframeWin ) {
	    iframeEl.style.height = "auto"; // helps resize (for some) if new doc shorter than previous
	    var docHt = getDocHeight(iframeWin.document);
	    // need to add to height to be sure it will all show
	    if (docHt) iframeEl.style.height = docHt + "px";
	  }
	}

function refreshBookValues() {
	    document.getElementById("fldn_temp_bookoutDetailForm").submit();
	}
	
var bookoutsInaccurate = false;// set in iterate loop

function promptUserIfBookoutInaccurate() {
//	setIframeHeight('bookOutIFrame');
	//commented out inaccurate popup - DW 11/7/05
	if ( isActive )	{
/*	
		//if ( bookoutsInaccurateBecauseNotReviewed )	{
			//alert("One or more of the book values for this vehicle may be inaccurate,\nbecause the body style, mileage and options have not been reviewed.");
		//}
		//else if ( bookoutsInaccurateNoBookValues ) {
		//	alert("One or more of the book values have not yet been determined for this vehicle");
		//}
*/		
		if ( bookoutsInaccurate ) {
			alert( "One or more of the book values for this vehicle may be\ninaccurate, because the mileage has been changed." );
			//if (refresh) {
				//refreshBookValues();
		//	}
		}
	}
}


function submitFormToRetrieveTrims(gbid, selectedKey) {
	if(selectedKey == "selectAModel") {
         document.getElementById("guideBooktrim["+gbid+"]").value="selectAModel";
         document.getElementById("guideBooktrim["+gbid+"]").disabled=true;
        return false;
    }
    else{
  document.getElementById("fldn_temp_tradeAnalyzerForm").action="GuideBookDetailSubmitAction.go?" + "gbid=" + gbid + "&selectedKey=" + selectedKey;
  document.getElementById("fldn_temp_tradeAnalyzerForm").submit();
  }
}