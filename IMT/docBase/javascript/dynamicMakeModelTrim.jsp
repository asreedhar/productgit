<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>

<html>
<head>
	<title></title>
<script language="javascript">

var d=document;
var X  = new m(null);
var Y  = new m(null);
function m(txt) {
	this.txt=txt;
	this.s=new Array();
}
function clearmenu(m) {
	options  = m.options;
	for (var i=options.length; i>=1; i--) options[i] = null;
	options[0].selected = true;
}
function setmenu(m,optArray) {
	options  = m.options;
	clearmenu(m);
	if(optArray!=null) {
	 	var optLength = optArray.length;
	 	for (var i = 0; i < optLength; i++) {
		 	options[i+1]=new Option(optArray[i].txt.substring(0,17), optArray[i].txt);
		}
	}
	options[0].selected = true;
}
//---==+0+==--- USER DEFINABLE SECTION ---==+0+==---
<bean:write name="performanceSearchForm" property="javaScript" filter="false"/>

<bean:write name="performanceSearchForm" property="allBodyStyles" filter="false"/>
<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
	<logic:equal name="performanceSearchForm" property="multipleMakes" value="true">
	var depth=4;
	var selects = new Array("make", "model","trim","body");
	var labels = new Array("Make", "Model", "Trim", "Body Style");
	</logic:equal>
	<logic:equal name="performanceSearchForm" property="multipleMakes" value="false">
	var depth=3;
	//var make = "<bean:write name="performanceSearchForm" property="make"/>";
	var selects = new Array("model","trim", "body");
	var labels = new Array("Model", "Trim", "Body Style");
	</logic:equal>
</logic:equal>
<logic:equal name="firstlookSession" property="member.inventoryType.name" value="USED">
	var depth=3;
	//var make = "";
	var selects = new Array("make", "model","trim");
	var labels = new Array("Make", "Model", "Trim");
</logic:equal>
//---==+0+==---END OF USER DEFINABLE SECTION ---==+0+==---

</script>
<script language="javascript">
function main()
{
	if(top.placeholderLoaded)
	{
		top.placeholderLoaded();
	}
<logic:equal name="firstlookSession" property="member.inventoryType.name" value="NEW">
	<logic:equal name="performanceSearchForm" property="multipleMakes" value="false">
	if(top.vehicleAnalyzerForm)
	{
		if(top.vehicleAnalyzerForm.make)
		{
			top.vehicleAnalyzerForm.make.value = "<bean:write name="performanceSearchForm" property="make"/>"
		}
	}
	</logic:equal>
</logic:equal>
}
</script>
</head>
<body onload="main();">
</body>
</html>









