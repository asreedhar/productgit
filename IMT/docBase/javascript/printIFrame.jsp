<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>

<script language="javascript" type="text/javascript">
var IFrameObj;
var isViewDeals = false;

function loadPrintIFrameOnDemand()
{
	if (!document.createElement) {return true};
	var IFrameDoc;
	var URL = "${printRef}";
	URL = (URL == "") ? "printHolder.html" : URL;
	if (!IFrameObj && document.createElement) {
		/* create the IFrame and assign a reference to the
		object to our global variable IFrameObj.
		this will only happen the first time
		callToServer() is called */
		var tempIFrame=document.createElement('iframe');
		tempIFrame.setAttribute('id','printFrame');
		tempIFrame.setAttribute('src','<c:url value="/javascript/printHolder.html"/>');
		tempIFrame.style.border='0px';
		tempIFrame.style.width='0px';
		tempIFrame.style.height='0px';
		IFrameObj = document.body.appendChild(tempIFrame);
	}
	IFrameDoc = IFrameObj.contentWindow.document;
	if(arguments.length > 0 && arguments.length % 2 == 0) {
		if(URL.indexOf("?") < 0) {
			URL += "?";
		}
		for(var i = 0; i < arguments.length; i += 2) {
			URL += "&" + arguments[i] + "=" + arguments[i+1];
		}
	}
	IFrameDoc.location.replace(URL);
	return false;
}

function global$printFunctions() {
	loadPrintIFrameOnDemand();
	progressBarInit();
}

function loadPrintIframe()
{
	var printFunctions = "global$printFunctions()";
	printFrameIsLoaded = "true";
	var printCell = document.getElementById("printCell");

	if(isViewDeals)
	{
			printCell.innerHTML = '<a href="#" onClick="' + printFunctions + '" id="printHref"><img src="images/common/print_small_white.gif" width="25" height="11" border="0" id="printImage"></a><br>'
	}
	else
	{
			printCell.innerHTML = '<a href="#" onClick="' + printFunctions + '" id="printHref">PRINT</a>';
			printCell.className = "navTextoff";
	}
}

function printTheIframe()
{
	self.frames["printFrame"].focus();
	self.frames["printFrame"].print();
}

function lightPrint()
{
	progressBarDestroy();
	printTheIframe();
}
</script>
<jsp:include page="/dealer/tools/includes/printingProgress.jsp" />