

Event.observe(window, 'load', function(event) {
	if($("areaId")) {
		$("areaId").focus();
	}	
});

function popPing(year, mileage, appraisalId) {
    var performanceAnalysisIFrameDoc = document.frames ? document.frames['performanceAnalysisIFrame'].document : document.getElementById('performanceAnalysisIFrame').contentDocument;
	if( !performanceAnalysisIFrameDoc.getElementById('groupingId')){
		alert('Please wait while the page loads...');
		return;
	}
	var groupingDescId = performanceAnalysisIFrameDoc.getElementById('groupingId').value;
	var mmgForPing = document.getElementById("fldn_temp_tradeAnalyzerForm").pingMMG.value;
	
	pop('/NextGen/PricingAnalyzer.go?groupingDescriptionId='+groupingDescId+'&year='+year+'&mileage='+mileage+'&appraisalId='+appraisalId+'&makeModelGroupingId='+mmgForPing,'price');
}



var savedInitialAppraisal;
var savedInitialAppraiser;
function loadTimer() {
    setTimeout("window.navigate('DealerHomeDisplayAction.go')",1200000);
}

function submitForm() {
    document.getElementById("fldn_temp_tradeAnalyzerForm").submit();
}
function submitBackPage(vin) {
        document.getElementById("fldn_temp_tradeAnalyzerForm").action="GuideBookDetailSubmitAction.go?vin=" + vin;
    document.getElementById("fldn_temp_tradeAnalyzerForm").submit();
}

function manageFieldSwitching() {
	if(document.getElementById("transferPriceLabel") != null) {
	    if ( (document.getElementById("fldn_temp_tradeAnalyzerForm").actionId[1].checked) ) {
			document.getElementById("transferPriceLabel").style.display = "";
			document.getElementById("transferPriceValue").style.display = "";
			document.getElementById("transferPriceForRetailOnly").style.display = "";
			if(document.getElementById("wholesalePriceLabel") != null) {
				document.getElementById("wholesalePriceLabel").style.display = "none";
				document.getElementById("wholesalePriceValue").style.display = "none";
			}
	    } else {
			document.getElementById("transferPriceLabel").style.display = "none";
			document.getElementById("transferPriceValue").style.display = "none";
			document.getElementById("transferPriceForRetailOnly").style.display = "none";
			if(document.getElementById("wholesalePriceLabel") != null) {
				document.getElementById("wholesalePriceLabel").style.display = "";
				document.getElementById("wholesalePriceValue").style.display = "";
			}
		}
    }
}

function submitAndNextTradeAnalyzerPlus() {
	submitTradeAnalyzerPlus("next");
}

function submitTradeAnalyzerPlus(mode) {
    var msgText = "";
	var formAction;
	if(mode !== undefined && mode == "next") {
		formAction = "TradeAnalyzerPlusSubmitAction.go?goToNext=waitingAppraisal";
	} else if(mode !== undefined && mode == "quick") {
		formAction = "TradeAnalyzerPlusSubmitAction.go?goToNext=quick";
	} else {
		formAction = "TradeAnalyzerPlusSubmitAction.go?";
	}
	
    var theForm = document.fldn_temp_tradeAnalyzerForm;
	if (theForm.price !== undefined) {
        if (isNaN(theForm.price.value)) {           
            if((theForm.price.value.indexOf(",") != -1) || (theForm.price.value.indexOf("$") != -1)) {
                msgText += "Please enter the Price without any punctuation marks such as dollar symbols ($) or commas (,).\n";
            } else {
                msgText += "Price must be a numeric value.\n";
            }
        }
        else {
        	stripCents(theForm.price);
		}
	}
	var mileageValue = "";
	if(theForm.mileage != undefined) {
		if (theForm.mileage.length == undefined) {
			mileageValue = theForm.mileage.value;
		} else {
			mileageValue = theForm.mileage[0].value; // Becuase in quirks mode, even inputs in iframes can be accessed by form.name -- damn, PM
		}
	} else if(theForm.mileageFormatted != undefined) {
		if (thieForm.mileageFormatted.length == undefined) {
			mileageValue = theForm.mileageFormatted.value;
		} else {
			mileageValue = theForm.mileageFormatted[0].value;
		}			
	}
    if (isNaN(mileageValue) || mileageValue == "") {
        if(mileageValue.indexOf(",") != -1) {
            msgText += "Please enter the Mileage without any punctuation marks such as commas ( , ).\n";
        } else {
            msgText += "Mileage must be a numeric value.\n";
        }
    }
    if (isNaN(theForm.reconditioningCost.value)) {
        if((theForm.reconditioningCost.value.indexOf(",") != -1) || (theForm.reconditioningCost.value.indexOf("$") != -1)) {
            msgText += "Please enter the Estimated Reconditioning Cost without any punctuation marks such as dollar symbols ($) or commas (,).\n";
        } else {
            msgText += "Estimated Reconditioning Cost must be a numeric value.\n";
        }
    }
    else {
    	stripCents(theForm.reconditioningCost);
	 }
    var nAppValue = theForm.appraisalValue.value;
    if (isNaN(nAppValue)) {
        if( (theForm.appraisalValue.value.indexOf(",") != -1) || ( theForm.appraisalValue.value.indexOf("$") != -1 ) ) {
            msgText += "Please enter a whole dollar appraisal value without any punctuation marks such as dollar symbols ($) or commas (,).\n";
        } else {
            msgText += "Appraisal value must be a numeric value.\n";
        }
    } else {
    	stripCents(theForm.appraisalValue);
	 }
    if (nAppValue < 1) {
    	if ( theForm.appraisalRequirement.value == 1 ) {
    		var bContinue = confirm( "No appraisal value entered, do you wish to continue?\n" );
    		if ( bContinue == false ) {
    			return;
    		}
    	} 
    	
    	if ( theForm.appraisalRequirement.value == 2 ) {
    		msgText += "Initial appraisal amount is a required field.\n";
    	}
    }  
    
    var primaryBookValue = null;
		try{
            var bookOutIFrameDoc = document.frames ? document.frames['bookOutIFrame'].document : document.getElementById('bookOutIFrame').contentDocument;
			primaryBookValue =  parseInt( stripCommas( bookOutIFrameDoc.getElementById('bookoutValue').innerHTML ) );
		}catch(exe){
			primaryBookValue = null;
		}
		
    if( primaryBookValue != null && Math.abs( nAppValue - primaryBookValue) > 5000 && primaryBookValue > 0 ){
				if( nAppValue != '' && nAppValue != null ){ //only check this if a value has been entered
					var b = (nAppValue - primaryBookValue > 0);
					var moreLess = b ?'more':'less';
					if( !confirm('The appraisal value of '+formatPriceVal(nAppValue)+' is over $5,000 '+moreLess+' than the primary book value of '+formatPriceVal(primaryBookValue)+'\n Continue?') ){
						theForm.appraisalValue.select();
						return;
					}
				}
	}
    
         
    if (theForm.conditionDisclosure.value.length > 2000) {
        var numCharOver = theForm.conditionDisclosure.innerText.length - 2000;
        numCharOver = numCharOver.toString();
        msgText += "\nThe number of characters in the Condition Disclosure field has exceeded the maximum allowed of 2000 characters by " + numCharOver + " characters.\nPlease shorten the Condition Disclosure by that many characters.\n";
    }

    var isChecked = false;
    var ourRadios = theForm.actionId;
    var curActionId = 0;
    for( i = 0; i < ourRadios.length; i++) {
            if( ourRadios[i].checked == true ) {
                isChecked = true;
                curActionId=ourRadios[i].value;                
                break;
            }
    }
    if(!isChecked ){
        msgText += "Please select an action with the radio buttons.\n";
    }
    if ( msgText == "" ) {  	   
        var appraisalValueEl = document.getElementById("appraisalValue") || document.getElementById("initialAppraisalValue");
        var appraisalValue = appraisalValueEl.value || appraisalValueEl.innerText;
  
    	//if(savedInitialAppraisal == $F('appraisalValue') && savedInitialAppraiser == $F('appraisers')){ //see if the values have already been saved via printing; if they are the same than remove the flag so they do not get saved again.
    	if(savedInitialAppraisal == appraisalValue && savedInitialAppraiser == $F('appraisers')){ //see if the values have already been saved via printing; if they are the same than remove the flag so they do not get saved again.
    		if($('saveBumpFlag')){
    			Element.remove('saveBumpFlag');
    			
    		}
    	}
    	
    	// explictly set form variables in case of loss of session
        //
        var performanceAnalysisIFrameDoc = document.frames ? document.frames['performanceAnalysisIFrame'].document : document.getElementById('performanceAnalysisIFrame').contentDocument;
    	var groupingDescId = performanceAnalysisIFrameDoc.getElementById('groupingId').value;
    	var priceToSend = 0;
    	if (theForm.price !== undefined) {
	    	priceToSend = theForm.price.value;
    	}
	    var allFormParams = "&appraisalId="
			+ theForm.appraisalId.value
			+ "&mileage="
			+ mileageValue
			+ "&reconditioningCost="
			+ theForm.reconditioningCost.value
			+ "&color="
			+ theForm.color.value
			+ "&actionId="
			+ curActionId
			+ "&conditionDisclosure="
			+ theForm.conditionDisclosure.value
			+ "&price="
			+ priceToSend
			+ "&year="
			+ theForm.year.value
			+ "&groupingDescriptionId="
			+ groupingDescId
			+ "&vin="
			+ theForm.vin.value;
			
		if(theForm.buyerId !== undefined) {
			allFormParams += "&buyerId="
			+ theForm.buyerId.value
			+ "&BuyerList="
			+ theForm.buyerId.value;
		} else {
		
			allFormParams += "&appraisalValue="
			+ theForm.appraisalValue.value
			+ "&appraiserName="
			+ theForm.appraiserName.value;
		}
		var gender = 'M';
		if (theForm.femaleGender != null && theForm.femaleGender.checked)
			gender = 'F'; 
		
		if(theForm.customerEmail !== undefined) {
			allFormParams += "&customerEmail="
				+ theForm.customerEmail.value
				+ "&customerFirstName="
				+ theForm.customerFirstName.value
				+ "&customerLastName="
				+ theForm.customerLastName.value
				+ "&customerGender="
				+ gender
				+ "&customerPhoneNumber="
				+ theForm.customerPhoneNumber.value;
		}
		if(theForm.dealTrackStockNumber !== undefined) {
				allFormParams += "&dealTrackStockNumber="
				+ theForm.dealTrackStockNumber.value
				+ "&dealTrackSalespersonID="
				+ theForm.dealTrackSalespersonID.value
				+ "&dealTrackNewOrUsed="
				+ theForm.dealTrackNewOrUsed.value;
		}
    	if (theForm.offerEditInputName != null) {
    		allFormParams += "&offerEditInputName="+ theForm.offerEditInputName.value;
    	}
		if(true){
    		theForm.action = formAction + allFormParams;
		}
        theForm.submit();
    } else {
        alert(msgText);
    }
}

function popKelley(url) {
    window.open(url,"kelley","width=820,height=550,location=yes,menubar=yes,resizeable=yes,scrollbars=yes,status=no,toolbar=yes")
}



