


/* *** New Window Opener Scripts *** */

function openWindow(pPath, pWindowName, pType)
{
	var winPassword = 'width=400,height=400'
	var winMemberProfile = 'width=772,height=575,scrollbars=yes,resizable=yes'
	var winPrint = 'directories=no,fullscreen=no,location=no,menubar=yes,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=yes,left=0,top=0'
	var winDefault = 'status=yes,toolbar=yes,menubar=no,location=no,scrollbars=yes,resizable=yes'

	var windowName = (pWindowName != null) ? pWindowName : "default"

	var params

	if(pType!=null)
	{
		switch(pType.toLowerCase())
		{
			case "member profile":
				params = winMemberProfile;
				break;
			case "password":
				params = winPassword;
				break;
			case "print report":
				params = winPrint;
				break;
			default:
				params = winDefault;
		}
	}
	else
	{
		params = winDefault;
	}
	return window.open(pPath, windowName, params);
}

function openPasswordWindow(pPath, pWindowName)
{	var x = openWindow(pPath, pWindowName, 'password')	}

function openMemberProfileWindow(pPath, pWindowName)
{
	var x = openWindow('EditMemberProfileAction.go', 'aChewMaMooAChewMaMooAChewMaMooMaMoo', 'member profile')
}

function printThisReport(pPath, pWindowName)
{	var x = openWindow(pPath, pWindowName, 'print report')	}

function printAgingReport(path,windowName)
{
	window.open(path, windowName, 'directories=no,fullscreen=no,location=no,menubar=yes,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=yes,left=0,top=0');
}


function changeClass(obj,toggle) {
    if (toggle) {
        obj.className = "flmiOver"
    } else {
        obj.className = "flmi"
    }
}

function navGoTo( action )
{
	if ( window.trapNavEvent )
	{
		trapNavEvent( action );
	}
	else
	{
		document.location.href = action;
	}
}
