
	/*var width = 100; //width of loading element
	var height = 65; //height of loading element
    var left = (document.documentElement.scrollLeft+(document.body.clientWidth -width )/2);
		   
    var top  = (document.documentElement.scrollTop +(document.body.clientHeight-height)/2);
	var div = '<div id="loading" style="border:1px solid black; background-color:#fc3; color:#000000;padding:10px;font-family:Arial,Verdana, sans-serif; font-size:.7em;text-align:center;font-weight:bold; position:absolute;top:'+top+'px; left:'+left+'px; width:'+width+'px; height:'+height+'px;">Loading<br/>Please Wait<br/><img style="margin-top:5px;" src="common/_images/icons/loading_icon_onf3c.gif"/></div>'; 
	new Insertion.After($('menu'),div);*/

var bHasUnsavedChanges = false;
var bCustomerOfferChanges = false;
var theForm = document.fldn_temp_tradeAnalyzerForm;


function submitTradeAnalyzerForm() {
    var theForm = document.fldn_temp_tradeAnalyzerForm;
    var proceed = true;
	var maxConditionLength = 2000;
    if(bCustomerOfferChanges){
    
   		 var prompt = confirm('Click OK to save your open customer offer, click Cancel to discard the changes.');
	    if(prompt){
	    	saveOfferEdit('offerEditInput','offer','offerEdit');
	    }
    }
    // update the mileage from the bookout iframe if it loaded without error
    if (document.bookOutIFrame.fldn_temp_bookoutDetailForm)
    {
    	if (document.bookOutIFrame.fldn_temp_bookoutDetailForm.mileage) {
	   		 theForm.mileage.value = document.bookOutIFrame.fldn_temp_bookoutDetailForm.mileage.value;
	   	}
    }

    theForm.target = "_top";
    var msgText = "";
    var isChecked = false;
    var ourRadios = theForm.actionId;
    for(i=0;i<ourRadios.length;i++) {
            if(ourRadios[i].checked == true) {
                isChecked = true;
                break;
            }
    }

	if (undefined != theForm.price  && isNaN(theForm.price.value)) {
			if((theForm.price.value.indexOf(",") != -1) || (theForm.price.value.indexOf("$") != -1)) {
					msgText += "Please enter the Price without any punctuation marks such as dollar symbols ($) or commas (,).\n";
			} else {
					msgText += "Price must be a numeric value.\n";
			}
	}
	else {
	        	stripCents(theForm.price);
	  }

	var dealTrackStockNumber = document.getElementById("dealTrackStockNumber");
	if(dealTrackStockNumber && dealTrackStockNumber.value.indexOf(" ") > -1) {
		msgText += "Please do not enter any spaces in the stock #.\n"
	}
	if (isNaN(theForm.reconditioningCost.value)) {
			if((theForm.reconditioningCost.value.indexOf(",") != -1) || (theForm.reconditioningCost.value.indexOf("$") != -1)) {
					msgText += "Please enter the Estimated Reconditioning Cost without any punctuation marks such as dollar symbols ($) or commas (,).\n";
			} else {
					msgText += "Estimated Reconditioning Cost must be a numeric value.\n";
			}
	}
	        else {
	        	stripCents(theForm.reconditioningCost);
	}

    if(!isChecked ) {msgText += "Please select an action with the radio buttons.\n";}
	
   	if ($F('newBumpValue') != 0)
	{
		proceed = confirm("Appraisal adjustment has not been applied. \n \nClick OK to proceed without saving appraisal adjustment. \n \nClick Cancel and 'Update Appraisal' button to adjust appraisal.");
	}
	try {
		if($("condition_description").value.length > maxConditionLength) {
				msgText += "Please limit the condition description to 2000 characters or less.\n\n"; 
		}
	} catch(e) {}
    if (msgText == "") 
    { 
	   if (proceed) {
	   	  // getDealTrackingValues();
	      theForm.action="TradeManagerEditSubmitAction.go";
       	  theForm.submit();
       }
    } 
    else 
    {
      alert(msgText);
    }
}
    
function markAsSaved() {
	window.bHasUnsavedChanges = false;
}

function switchUnchangedFlag() {
	window.bHasUnsavedChanges = true;

}

function manageFieldSwitching() {
	if(document.getElementById("transferPriceLabel") != null) {
	    if ( (document.getElementById("fldn_temp_tradeAnalyzerForm").actionId[1].checked) ) {
			document.getElementById("transferPriceLabel").style.display = "";
			document.getElementById("transferPriceValue").style.display = "";
			document.getElementById("transferPriceForRetailOnly").style.display = "";
			if(document.getElementById("wholesalePriceLabel") != null) {
				document.getElementById("wholesalePriceLabel").style.display = "none";
				document.getElementById("wholesalePriceValue").style.display = "none";
			}
	    } else {
			document.getElementById("transferPriceLabel").style.display = "none";
			document.getElementById("transferPriceValue").style.display = "none";
			document.getElementById("transferPriceForRetailOnly").style.display = "none";
			if(document.getElementById("wholesalePriceLabel") != null) {
				document.getElementById("wholesalePriceLabel").style.display = "";
				document.getElementById("wholesalePriceValue").style.display = "";
			}
		}
    }
}
  
function submitDisplayAction() {	
	var proceed = true;
   	if (($('newBumpValue') != null && $F('newBumpValue') != 0) || bHasUnsavedChanges || bCustomerOfferChanges )
	{
	var msg = "Are you sure you want to navigate away from this page? \n \n";
		if(bCustomerOfferChanges){
			msg+="You have an unsaved customer offer";
		}
		if(bHasUnsavedChanges){
			msg+="\nAll unsaved changes will be lost.";
		}
			msg+=" \n \nPress OK to continue, or Cancel to stay on the current page.";
		proceed = confirm(msg);
	}
	return proceed?true:false;
}

function adjustSize(clWidth,clHeight) {
    document.all.bumpFrame.width = clWidth;
    document.all.bumpFrame.height = clHeight;
}

function resetParentForm() {
  document.getElementById("fldn_temp_tradeAnalyzerForm").target = "_top";
  document.getElementById("fldn_temp_tradeAnalyzerForm").update.value = "false";
  document.getElementById("fldn_temp_tradeAnalyzerForm").action="TradeManagerEditDisplayAction.go#perfAnalysis";
}
function storeAppraisal() {
    var msgText = "";
    var savingMessage = 'Saving...';
    
    var nAppValue = document.getElementById("fldn_temp_tradeAnalyzerForm").newBumpValue.value;
	var BuyerId  = null;
    if (undefined != document.getElementById("fldn_temp_tradeAnalyzerForm").BuyerList) {
    	BuyerId = document.getElementById("fldn_temp_tradeAnalyzerForm").BuyerList.value;    
    }
    
	var primaryBookValue = null;
	
	//if edmunds form is on page, cache the value, needed to repopulate the field after the ajax update
	if(document.getElementById("fldn_temp_tradeAnalyzerForm").edmundsTMV != null)
		var edmunds = document.getElementById("fldn_temp_tradeAnalyzerForm").edmundsTMV.value;
		
		
	//this attempts to retrieve the primary book value from the bookoutValue element
	//Saves the necessity to rebook the vehicle outside of the context of the bookout frame
	//if there is a bookout load failure, the missing element throws an error which is caught
	//silently
	try{
        var bookOutIFrameDoc = document.frames ? document.frames['bookOutIFrame'].document : document.getElementById('bookOutIFrame').contentDocument;
		primaryBookValue =  parseInt( stripCommas( bookOutIFrameDoc.getElementById('bookoutValue').innerHTML ) );
	}catch(exe){
		primaryBookValue = null;
	}
	
	if((nAppValue.indexOf(",") != -1) || (nAppValue.indexOf("$") != -1)) {
			msgText += "Please enter a value without any punctuation marks such as dollar symbols ($) or commas (,).\n";
	}else if(parseInt(nAppValue) <= 0){
			msgText += "value must be larger than 0.\n";
	}
	else if(isNaN(nAppValue)){
			msgText += "Value must be a numeric value.\n";
	} else if(nAppValue.length < 1) {
			msgText += "Please enter a value.\n";
	}
	
	if (BuyerId != null && BuyerId == "") {
		msgText += "Please select a Buyer.\n";
	};

	if( primaryBookValue != null && Math.abs( nAppValue - primaryBookValue) > 100000 && primaryBookValue > 0 ){
		var b = (nAppValue - primaryBookValue > 0);
		
		var moreLess = b ?'more':'less';
		if( !confirm('The appraisal value of '+formatPriceVal(nAppValue)+' is over $100,000 '+moreLess+' than the primary book value of '+formatPriceVal(primaryBookValue)+'\n Continue?') ){
			document.getElementById("fldn_temp_tradeAnalyzerForm").newBumpValue.select();
			return;
		}
	}			  
    if (msgText == "") {
 			 document.getElementById('savingMessage').innerHTML = savingMessage;
  			 stripCents(document.getElementById("fldn_temp_tradeAnalyzerForm").appraisalValue);
    		var appValue = $('newBumpValue').value;
			var form = document.getElementById("fldn_temp_tradeAnalyzerForm");
			var serializedForm = Form.serialize(form);
			var date = new Date();
			serializedForm+='&date='+new Date().getTime();
			
			var myAjax = new Ajax.Updater( 
			'appraisalBump', 
			'TradeManagerEditBumpSubmitAction.go', 
			{ 
			method: 'post', 
			parameters: serializedForm,
			onComplete: function(xhr) {
				
				var useAppraisalValue = document.getElementById('useAppraisalValue');
				if ( useAppraisalValue && useAppraisalValue.value  != 0  && appValue > 0) {
					var newOffer = "$" + addCommas(appValue);
					if($('offer')){
						$('offer').innerHTML =  newOffer;
					}
					if ($('offerEditInput')) {
						$('offerEditInput').value = appValue;
					};
				}
				
				var savingsMessage = document.getElementById('savingMessage');
				if (savingsMessage) 
					savingsMessage.innerHTML = '';
				if(edmunds != null)
					document.getElementById("fldn_temp_tradeAnalyzerForm").edmundsTMV.value = edmunds;
				
			}
	 });
    } else {
        alert(msgText);
        document.getElementById('savingMessage').innerHTML= '';
    }
}

function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}
function stripCommas(price)
{
	if(price.indexOf('$') != -1){
	
		price = price.replace('$','');
	}
	if(price.indexOf(',') != -1){
	
		price = price.replace(',','');
	}
	if(price.indexOf('.') != -1){
	
		price = price.replace('.','');
	}
	return price;
}

function popKelley(url) {
	window.open(url,"kelley","width=820,height=550,location=yes,menubar=yes,resizeable=yes,scrollbars=yes,status=no,toolbar=yes")
}
function saveOfferEdit(valEl,showEl,hideEl,appId){
	var val = formatPrice($F(valEl));
	var path = 'AppraisalFormSaveAction.go';
	if(validateOfferTM(val)){
		new Ajax.Request(
					path,
					{
						method: 'post',
						parameters:"offer=" + val + "&appraisalId="+ appId +"&inline=true" ,
						onSuccess: function(r,j) {					
						
						},
						onComplete: function(r,j){						
							$(showEl).innerHTML = formatPriceVal(val);
							toggleOfferEdit(showEl, hideEl, false);
							document.getElementById('useAppraisalValue').value = 0;
						},
						onFailure: function(r,j){						
							
						}
					}
				);
	
	}else{
		$(valEl).value = '';
		return false;
	}
}

function toggleOfferEdit(show, hide, b){

	var shw = $(show);
	var hde = $(hide);

	hde.style.display = 'none';
	shw.style.display = '';

	
	bCustomerOfferChanges = b;
}

function validateOfferTM(val){
	var value = formatPrice(val);
	var check;	 
	if( value=='' || isNaN(value) || value < 1) check=true;
	if(check) {
		var msg = '\"'+value+'\" is not a valid customer offer.';
		msg += '\nPlease try again.'
		alert(msg);
		return false;
	}
	return true;

}

function formatPrice(price)
{
	var indexOfPeriod = price.indexOf(".");
	if (indexOfPeriod >= 0)
	{
		price = price.substring(0, indexOfPeriod);
	}
	var indexOfComma = price.indexOf(",");
	if (indexOfComma >= 0)
	{
		price = price.substring(0, indexOfComma) + price.substring( (indexOfComma + 1), price.length );
	}
	var indexOfDollar = price.indexOf("$");
	if (indexOfDollar >= 0)
	{
		price = price.substring( (indexOfDollar + 1), price.length );
	}
	return price;
}

function formatPriceVal(price){
	
	var retStr = '';
	price = price.toString();
	price = stripCommas(price);
	retStr = addCommas(price);

	retStr = '$'+retStr;
	return retStr;

}


function popPing(year, appraisalId) {

    var performanceAnalysisIFrameDoc = document.frames ? document.frames['performanceAnalysisIFrame'] : document.getElementById('performanceAnalysisIFrame').contentDocument;
    var bookOutIFrameDoc = document.frames ? document.frames['bookOutIFrame'] : document.getElementById('bookOutIFrame').contentDocument;
	if( performanceAnalysisIFrameDoc.getElementById('groupingId') && bookOutIFrameDoc.getElementById('bookMileage')){
		var groupingDescId = performanceAnalysisIFrameDoc.getElementById('groupingId').value;
		var mileage = bookOutIFrameDoc.getElementById('bookMileage').value; 
		var mmgForPing = document.getElementById("fldn_temp_tradeAnalyzerForm").pingMMG.value;
		pop('/NextGen/PricingAnalyzer.go?groupingDescriptionId='+groupingDescId+'&year='+year+'&mileage='+mileage+'&appraisalId='+appraisalId+'&makeModelGroupingId='+mmgForPing,'price');
	}else{
		alert('Please wait while the page loads');
	}
}
