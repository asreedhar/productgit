var pageDirty = false;
function makeDirty () {pageDirty = true;}
function isDirty () {
	if (pageDirty) {
		var userSelection = window.showModalDialog ("CIAModalDisplayAction.go","","dialogHeight:150px;dialogWidth:400px;center:yes;edge:raised;help:no;resizable:no;scroll:no;status:no;unadorned:yes");
		return userSelection;
	} else {
		return false;
	}	
}
function doMakeDirty() {
	if (typeof(makeDirty) != "undefined") {makeDirty();}
}
function clickNav(url, info0, info1, info2, info3) {
	var displayAction,submitAction,strQuery = "?";
	displayAction = url;
	var goCIA = (url.indexOf("BuyingGuide") == -1) ? true:false;
	var goBG = (url.indexOf("BuyingGuide") != -1) ? true:false;
	var coreOrNonCore = (info3 == "nonCoreClassTypeId") ? "nonCoreClassTypeId" : "classTypeId";
	if(pageName == "cia") {
		var ciaForm = document.getElementById("megaForm");
		submitAction = ciaForm.action;
	}
	/*
		Build the query string from the link so we know where we are headed
		attach it to the form so upon processing it knows where to go
		or attach it back to the originally contemplated link 
	*/
	if(goCIA) {
		if(info0 != '') {
			strQuery += coreOrNonCore + "=" + info0;
		}
		if(info1 != '') {
			strQuery += "&sectionId=" + info1;
		}
		if(info2 != '') {
			strQuery += "&mileageFilter=" + info2;
		}

	} else if(goBG) {
		if(info0 != '') {
			strQuery += "ciaSummaryId=" + info0;
		}
		if(info1 != '') {
			strQuery += "&priorWeek=" + info1;
		}
	}

	/*
		determine if a form field has been changed in order to throw the modal and go to correct destination
	*/
	if (typeof(isDirty) != "undefined") {
		if ( isDirty() ) {
			//ciaForm.action = submitAction + strQuery + "&dirtySave=true";
			ciaForm.action = submitAction + "?dirtySave=true";
			if(info1 != null && ciaForm.sectionId) {ciaForm.sectionId.value=info1;}
			if(info0 != null && ciaForm.ciaSummaryId) {ciaForm.ciaSummaryId.value=info0;}
			//alert(ciaForm.action);
			
			ciaForm.submit();
		} else {
			document.location.href = displayAction + strQuery;
		}
	} else {
			ciaForm.action = submitAction + strQuery + "&dirtySave=true";
			ciaForm.submit();
	}	
}
