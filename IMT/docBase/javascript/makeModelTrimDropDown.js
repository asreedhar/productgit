function validateForm(thisForm)
{	
	var Vmessage = "";
	var isValid = true;
	if(0 == thisForm.make.selectedIndex) {
		Vmessage += "Select Make\n";
		isValid = false;
	}
	if(0 == thisForm.model.selectedIndex) {
		Vmessage += "Select Model\n";
		isValid = false;
	}
	if(0 == thisForm.catalogKeyId.selectedIndex) {
		Vmessage += "Select Trim\n";
		isValid = false;
	}
	if(!isValid) {
		alert(Vmessage);
	} 
	return isValid;
}


function validateVehicleAnalyzerForm(thisForm)
{
	var Vmessage = "Vehicle Analyzer:\n\n";
	var isValid = true;
	if(0 == thisForm.make.selectedIndex) {
		Vmessage += "Select Make\n";
		isValid = false;
	}
	if(0 == thisForm.model.selectedIndex) {
		Vmessage += "Select Model\n";
		isValid = false;
	}
	if(0 == document.getElementById('catalogKeyId').value) {
		thisForm.trim.value = null;
	} else {
		thisForm.trim.value = document.getElementById('catalogKeyId').value;
	}
	if(!isValid) {
		alert(Vmessage);

	} else {
		thisForm.submit();
	}

	return isValid;
}

function switchResults(bShowGroup) {
	var clickedButton;
	if ( bShowGroup == 0 ) {
		clickedButton = 'showDealer.x';
	} else {
		clickedButton = 'showDealerGroup.x';
	}
	var pars = '&groupMake=' + $('groupMake').value + '&groupModel=' + $('groupModel').value + '&groupTrim=' + $('groupTrim').value + '&' + clickedButton + '=' + clickedButton;
	var form = document.getElementById('analyzerForm');
	var formAction = form.action;
	var formName = form.name;
	retrieveURL( formAction+pars, formName);
	setTimeout('loadMakes(false)', 3750);
}

function loadMakes(forAppraisal)
{	
	if (forAppraisal) {
		retrieveURL('MakeModelTrimDropDownAction.go?dropDown=make&forAppraisal=true', 'noForm', null, null);
	} else {
		retrieveURL('MakeModelTrimDropDownAction.go?dropDown=make&forAppraisal=false', 'noForm', null, null);
	}
	resetModel();
}	


function resetMake() {
	alert('resetMake');
	if (document.getElementById("make")) {
		makeSelectionOptions = document.getElementById("make").options;
		if (makeSelectionOptions) {
				alert('makeSelection length: ' + makeSelectionOptions.length);
			numOfOptions = makeSelectionOptions.length;
			for (i = numOfOptions; i > 0; i--) {
				makeSelectionOptions[i] = null;
			}
		}
	}
	resetModel();
}

function resetModel() {
	if (document.getElementById("model")) {
		modelSelectionOptions = document.getElementById("model").options;
		if (modelSelectionOptions) {
			numOfOptions = modelSelectionOptions.length;
			for (i = numOfOptions; i > 0; i--) {
				modelSelectionOptions[i] = null;
			}
		}
	}
	resetStyleKey();
}


function resetOptions()
{	
	if(document.getElementById("options")) {
		document.getElementById("options").innerHTML = "";
	}
}

function resetStyleKey() {
	if (document.getElementById("catalogKey")) {
		catalogKeySelectionOptions = document.getElementById("catalogKey").options;
		if (catalogKeySelectionOptions) {
			numOfOptions = catalogKeySelectionOptions.length;
			for (i = numOfOptions; i > 0; i--) {
				catalogKeySelectionOptions[i] = null;
			}
		}
	}
} 

// If forAppraisal= true, the trims being retrived are from the Catalog(e.g. Edmunds) and are used to create a Vehicle row.
// Otherwise the values are from FirstLook tables(MMG) and used to reteive performance info.
function loadModels(make, ignoreMe, forAppraisal)
{	
	if (forAppraisal == "true") {
		year = document.getElementById("year").value;
		retrieveURL('MakeModelTrimDropDownAction.go?dropDown=model&forAppraisal=true&make='+ make + '&year=' + year, 'noForm', null, null);
	} else {
		retrieveURL('MakeModelTrimDropDownAction.go?dropDown=model&forAppraisal=false&make='+ make + '&year=' + null, 'noForm', null, null);
	}
	resetStyleKey();
}	

// If forAppraisal= true, the trims being retrived are from the Catalog(e.g. Edmunds) and are used to create a Vehicle row.
// Otherwise the values are from FirstLook tables(MMG) and used to reteive performance info.
function loadTrims(model, ignoreMe, forAppraisal)
{	
	make = document.getElementById("make").value;
	if (forAppraisal == "true") {
		year = document.getElementById("year").value;
		retrieveURL('MakeModelTrimDropDownAction.go?dropDown=trim&forAppraisal=true&make='+ make + '&model=' + model + '&year=' + year, 'noForm', null, null);
	} else {
		retrieveURL('MakeModelTrimDropDownAction.go?dropDown=trim&forAppraisal=false&make='+ make + '&model=' + model + '&year=' + null, 'noForm', null, null);
	}
}	
