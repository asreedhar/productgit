var isMMRloaded = false;

function submitFormForCalculation(selectedKey) {
  if (formSubmitted == true) { 
    return;
  }
  if(!isMMRloaded)
	{
	 alert("Please wait for MMR to load");
	 return;
	}
	var maxConditionLength = 2000;
	var strMessage = "";
	var mileageCheck = document.getElementById("mileage").value;
	var smsNotify = document.getElementById("smsNotify").checked;
	var selectedManager = document.getElementById("selectedManager").value;
	var mmrTrimSelected = "NA";
	var isAppaiserNameRequired= document.getElementById('isAppraiserNameRequired').value;
	var appraiserName= document.getElementById('Appraiser').value;
	var buyerId=document.getElementById('Buyer').value;
	var appraisaltype=document.getElementById('TypeofAppraisal').value;
	var custPhone = document.getElementById('customerPhoneNumber');
	
	
	var phoneFilter = /^(\()?([0-9]{3})(\)|-)?([0-9]{3})(-)?([0-9]{4}|[0-9]{4})$/;
	/*if (custPhone.value == null || custPhone.value == '') {
		return true; //Phone is not required.
	}
	*/
 	var valid =custPhone.value == null || custPhone.value == ''||phoneFilter.test(custPhone.value);
	if (!valid) {
		
		alert('Please enter a valid phone number.\n(111)222-3333 or \n111-222-3333 or \n1112223333');
		return ;
	}
		
	
	if(appraisaltype=="Trade"){
		if ((isAppaiserNameRequired =="true") && (appraiserName .length <=0))
		{
			alert("Appraiser Name is a Required Field");
			return;
		}
		
	}
	else{
		if ((isAppaiserNameRequired =="true") && (buyerId.length<=0))
		{
			alert("Buyer Name is a Required Field");
			return;
		}
		
	}
	document.getElementById('appraiserName').value=document.getElementById('Appraiser').options[document.getElementById('Appraiser').selectedIndex].text;
	document.getElementById('buyerId').value=document.getElementById('Buyer').value;
	document.getElementById('dealTrackSalespersonID').value = document.getElementById('Salesperson').value;
	
	var dropdownSales=document.getElementById('Salesperson');
	
	document.getElementById('dealTrackSalesperson').value = dropdownSales.options[dropdownSales.selectedIndex].text;
	
	// If the mmrTrimSelected is undefined, it doesn't need to be checked.
	if ((document.fldn_temp_tradeAnalyzerForm != undefined) && 
		(document.fldn_temp_tradeAnalyzerForm.selectedMmrTrim != undefined))
	{
		mmrTrimSelected = document.fldn_temp_tradeAnalyzerForm.selectedMmrTrim.value;
	}
	
  	var catalogKey = document.getElementById("selectCatalogKey") || document.getElementById("catalogKeyId") || document.getElementById("fldn_temp_tradeAnalyzerForm").catalogKey; 
    if(catalogKey && catalogKey.selectedIndex !== undefined && catalogKey.selectedIndex == 0) {
        strMessage += "Please select a Trim from the Trim List.\n\n";
    }
    
    var blackBookSelectModelIsEmpty = false;
    var nadaSelectModelIsEmpty = false;
    var kbbSelectModelIsEmpty = false;
    var galvesSelectModelIsEmpty = false;
    var edmundsSelectModelIsEmpty = false;

    for (i = 0; i < document.fldn_temp_tradeAnalyzerForm.elements.length; i++)
	{
		if (document.fldn_temp_tradeAnalyzerForm.elements[i].value=='selectAModel') 
			 switch (document.fldn_temp_tradeAnalyzerForm.elements[i].className) {
			 	case "1":
			 		blackBookSelectModelIsEmpty = true;
			 		break;
			 	case "2":
			 		nadaSelectModelIsEmpty = true;
			 		break;
			 	case "3":
			 		kbbSelectModelIsEmpty = true;
			 		break;
			 	case "4":
			 		galvesSelectModelIsEmpty = true;
			 		break;
			 	case "5":
			 		edmundsSelectModelIsEmpty = true;
			 		break;
			 }
	}
	try {
		if(document.getElementById("condition").value.length > maxConditionLength) {
			strMessage += "Please limit the condition description to 2000 characters or less.\n\n";
		}
	} catch(e) {}
    
    if(blackBookSelectModelIsEmpty) {
		    strMessage += "Please select a model from the Black Book drop-down list.\n\n";
    }
    
    if(nadaSelectModelIsEmpty) {
		    strMessage += "Please select a model from the NADA drop-down list.\n\n";
    }
    
    if(kbbSelectModelIsEmpty) {
		    strMessage += "Please select a model from the Kelley Blue Book drop-down list.\n\n";
    }
    
    if(galvesSelectModelIsEmpty) {
		    strMessage += "Please select a model from the Galves drop-down list.\n\n";
    }
    
    if(edmundsSelectModelIsEmpty) {
		    strMessage += "Please select a model from the Edmunds drop-down list.\n\n";
    }
    
    if(mileageCheck < 0 || isNaN(mileageCheck))
    {
  	    strMessage += "Mileage amount must be a positive number.";
    }

    if(smsNotify && selectedManager == 0)
    {
  	    strMessage += "Please select a manager to notify";
    }

    if (mmrTrimSelected == "")
    {
  	    strMessage += "Please select a Manheim Trim";
    }

    if(strMessage == "") {
        document.getElementById("fldn_temp_tradeAnalyzerForm").action="GuideBookStepThreeSubmitAction.go"; 
        document.getElementById("fldn_temp_tradeAnalyzerForm").submit();
        formSubmitted = true;
    } else {
        alert(strMessage);
    }
}
disableSetFocus();
Event.observe(window, 'load', function(event) {
	Event.observe($('tradeTypeRadioButton'), 'click', function(event) {
		AppraisalMode.switchToTradeMode();
		if (PersonManager.close) { 
			PersonManager.close(); 			
		};
	});
	Event.observe($('purchaseTypeRadioButton'), 'click', function(event) {
		AppraisalMode.switchToPurchaseMode();
		if (PersonManager.close) { 
			PersonManager.close(); 
		};
	});
	var mileage = $('mileage');
	var selectCatalogKey = $('selectCatalogKey');
	if(mileage) {
		mileage.focus();
	}
	if (mileage && selectCatalogKey) {
		Event.observe(selectCatalogKey, 'change', function(event) {
			mileage.focus();
		});
	};	
	if(document.getElementById('purchaseTypeRadioButton').checked) {
		AppraisalMode.switchToPurchaseMode();
	} else {
		AppraisalMode.switchToTradeMode();
	}
});

var AppraisalMode = {
	
	_show: function(arr) {
		for (var i = 0; i < arr.length; i++) {
			arr[i].style.display = 'block';
		}
	},
	
	_hide: function(arr) {
		for (var i = 0; i < arr.length; i++) {
			arr[i].style.display = 'none';
		}
	},
	
	switchToTradeMode: function() {
		AppraisalMode._show(document.getElementsByClassName('appraisal_type_text_1'));
		AppraisalMode._hide(document.getElementsByClassName('appraisal_type_text_2'));
		document.getElementById('pageTitle').innerHTML = document.title;
		document.getElementById('appraisal_ready_wrap').style.display = 'inline';
		document.getElementById('TypeofAppraisal').value="Trade";
		
	},
	switchToPurchaseMode: function() {
		AppraisalMode._hide(document.getElementsByClassName('appraisal_type_text_1'));
		AppraisalMode._show(document.getElementsByClassName('appraisal_type_text_2'));
		document.getElementById('pageTitle').innerHTML = document.title;
		document.getElementById('appraisal_ready_wrap').style.display = 'none';
		document.getElementById('TypeofAppraisal').value="Purchase";
		updateBuyerSelect();
	}
};

function updateBuyerSelect () {
	var params = "pos=Buyer&updateDropDown=true";
	document.getElementById('buyerId').value = document.getElementById('Buyer').value;

		var buyerIdInput = document.getElementById('buyerId');
		if (buyerIdInput.value != '') {
			params += '&selectedPersonId='+buyerIdInput.value;
		};
		new Ajax.Request(
			'/IMT/ManagePeopleDisplayAction.go',
			{
				method: 'get',
				parameters: params,
				onComplete: function(xhr) {
					document.getElementById('buyerId').value="";
					document.getElementById('BuyerDrop').innerHTML = xhr.responseText;
				}
			}
		);
}
jQuery(document).ready(function(){
	
	if(!(jQuery.browser.msie && parseFloat(jQuery.browser.version)<=9)){
		
		jQuery.ajax({
		url:"/IMT/MMRTrimOnStep2Action.go",
			method:"GET",
			success:function(data){
				//alert(data)
				jQuery("#mmrTrimDropDownContainer").html(data)
				isMMRloaded = true;
			},
			error:function(data){
					alert("Unable to get MMR data");
					isMMRloaded = true;
				}
			
			});
	
	
	}
	else
		{
		
		new Ajax.Request(
				'/IMT/MMRTrimOnStep2Action.go',
				 { method: 'get',
					onComplete: function(xhr){ 	
					$('mmrTrimDropDownContainer').innerHTML = xhr.responseText;
					isMMRloaded = true;
					
				},
					onFailure:function(xhr){
						isMMRloaded = true;
						alert('Unable to get MMR data');
					}
			});
	
	
		}
	
	})
