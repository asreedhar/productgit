<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script src="/hedgehog/hedgehog.js?buildNumber=${applicationScope.buildNumber}" type="text/javascript"></script>

<!-- ************* HEDGEHOG ANALYSIS ************* -->

<script language="javascript" type="text/javascript">

Hedgehog$Bootstrap = function()
{
	if(typeof(Hedgehog) !== "undefined") {
		Hedgehog.SystemIdentifier = '${sessionScope["firstlookSession"].member.memberId}';
	}
}

if (window.addEventListener)
{
	if(typeof(Hedgehog) !== "undefined") {
    	window.addEventListener('load', function () { Hedgehog.Track(); }, false);
	}
}
else if (window.attachEvent)
{
	if(typeof(Hedgehog) !== "undefined") {
    	window.attachEvent('onload', function () { Hedgehog.Track(); });
	}
}

</script>


<!-- ************* HEDGEHOG ANALYSIS ************* -->
