<%@ taglib uri="/WEB-INF/taglibs/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix='logic' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- ************* PRINT MENU ************* -->
		<table cellpadding="0" cellspacing="0" border='0' class='menu' id='printMenu' width='190' onmouseout='hideMenu()' summary='Table for the print menu'>
	<logic:equal name="isKelleyGuideBook" value="false">
			<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="printMenu.PrimaryNonKelleyCell" onclick="printURL('PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=true&includeDealerGroup=${includeDealerGroup}')">${title} Print Page</td></tr> <!-- Primary guide book non-Kelley -->
	</logic:equal>

	<logic:equal name="isKelleyGuideBook" value="true">
			<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="printMenu.PrimaryKelleyRetailCell" onclick="printURL('PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=true&KBBreportType=retail&includeDealerGroup=${includeDealerGroup}')">KBB Suggested Retail</td></tr> <!-- Primary guide book Kelley retail value -->
			<c:if test="${firstlookSession.includeKBBTradeInValues}">
				<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="printMenu.PrimaryKelleyTradeInCell" onclick="printURL('PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=true&KBBreportType=tradeIn&includeDealerGroup=${includeDealerGroup}')">KBB Trade-In Report</td></tr> <!-- Primary guide book Kelley trade in value -->
			</c:if>
			<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="printMenu.PrimaryKelleyWholesaleCell" onclick="javascript:pop('PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=true&includeDealerGroup=${includeDealerGroup}')">KBB Wholesale/Retail Breakdown</td></tr> <!-- Primary guide book Kelley Wholesale value -->
	</logic:equal>

	<logic:equal name="hasSecondaryGuideBook" value="true">
		<logic:equal name="isKelleySecondaryGuideBook" value="false">
			<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="printMenu.SecondaryNonKelleyCell" onclick="printURL('PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&includeDealerGroup=${includeDealerGroup}')">${titleSecondary} Print Page</td></tr> <!-- Secondary guide book non-Kelley -->
		</logic:equal>

		<logic:equal name="isKelleySecondaryGuideBook" value="true">
			<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="printMenu.SecondaryKelleyRetailCell" onclick="printURL('PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&KBBreportType=retail&includeDealerGroup=${includeDealerGroup}')">KBB Suggested Retail</td></tr> <!-- Secondary guide book Kelley retail value -->
			<c:if test="${firstlookSession.includeKBBTradeInValues}">
				<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="printMenu.SecondaryKelleyTradeInCell" onclick="printURL('PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&KBBreportType=tradeIn&includeDealerGroup=${includeDealerGroup}')">KBB Trade-In Report</td></tr> <!-- Secondary guide book Kelley tradein value -->
			</c:if>
			<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="printMenu.SecondaryKelleyWholesaleCell" onclick="printURL('PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&includeDealerGroup=${includeDealerGroup}')">KBB Wholesale/Retail Breakdown</td></tr> <!-- Secondary guide book Kelley Wholesale value -->
		</logic:equal>
	</logic:equal>
			<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="printMenu.AllValuesCell" onclick="printURL('PrintableTAFirstlookDisplayAction.go?appraisalId=${appraisalId}&includeDealerGroup=${includeDealerGroup}&weeks=${weeks}')">First Look Appraisal Report</td></tr> <!-- All Guide Book Values, All The Time (SM) -->
		<c:if test="${isTradeManager && showAppraisalForm}">
			<tr><td nowrap class='flmi' onmouseover="changeClass(this,true)" onmouseout="changeClass(this,false)" id="printMenu.AppraisalFormCell" onclick="javascript:if(validateCustomerOffer(1))pop('<c:url value="/AppraisalFormDisplayAction.go"><c:param name="appraisalId" value="${tradeAnalyzerForm.appraisalId}"/><c:param name="vin" value="${tradeAnalyzerForm.vin}"/><c:param name="mileage" value="${tradeAnalyzerForm.mileage}"/></c:url>&offer='+custOffer,'appraisalForm');" >Appraisal Form</td></tr> <!-- Appraisal Form -->
		</c:if>
		</table>