<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix='logic' %>
<!-- ************* MARKETING HEADER ********************************************************* -->
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td rowspan="100" width="1" height="37">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr><td bgcolor="#000000"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="35" border="0"><br></td></tr>
				<tr><td bgcolor="#000000"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
			</table>
		</td>
		<td height="1" bgcolor="#000000"><img src="images/common/shim.gif" width="758" height="1" border="0"><br></td>
		<td rowspan="100" width="1" height="37">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr><td bgcolor="#000000"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
				<tr><td><img src="images/common/shim.gif" width="1" height="35" border="0"><br></td></tr>
				<tr><td bgcolor="#000000"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="background-image:url(images/common/table_background.gif)">
			<table cellspacing="0" cellpadding="0" width="100%" border="0">
				<tr>
					<td width="172">
						<table border="0" cellpadding="0" cellspacing="0" width="172">
							<tr>
								<td width="122"><img src="images/common/fldn_logo.gif" border="0" hspace="5" vspace="5" id="logoImage"><br></td>
								<td width="1"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
							</tr>
						</table>
					</td>
<logic:present name="firstlookSession">
					<td width="374"><img src="images/common/shim.gif" width="331" height="1" border="0"><br></td><!-- *** was 408 *** -->
					<td align="right">
						<table border="0" cellpadding="0" cellspacing="0" width="212">
							<tr>
								<td width="79"><a href="/support/Marketing_Pages/AboutUs.aspx" id="contactHref"><img src="images/common/aboutFL_75x12_wht.gif" width="75" height="12" border="0" hspace="2" id="contactImage"></a><br></td>
								<td width="1"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
								<td width="1" bgcolor="#ffffff"><img src="images/common/white_bar.gif" width="1" height="11" border="0"><br></td>
								<td width="1"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
								<td width="85"><a href="/support/Marketing_Pages/ContactUs.aspx" id="contactHref"><img src="images/common/contactFL_81x12_wht.gif" width="81" height="12" border="0" hspace="2" id="contactImage"></a><br></td>
                <td width="1"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
                <td width="1" bgcolor="#ffffff"><img src="images/common/white_bar.gif" width="1" height="11" border="0"><br></td>
                <td width="1"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
               <td width="69"><a href="javascript:window.close();"><img src="images/common/closeWindow_white.gif" width="69" height="11" border="0" id="closeWindow"></a><br></td>
								<td width="5"><img src="images/common/shim.gif" width="5" height="1"><br></td>
							</tr>
						</table>
					</td>
</logic:present>
<logic:notPresent name="firstlookSession">
					<td width="414"><img src="images/common/shim.gif" width="414" height="1" border="0"><br></td><!-- *** was 408 *** -->
					<td align="right">
						<table border="0" cellpadding="0" cellspacing="0" width="172">
							<tr>
								<td width="79"><a href="AboutUs.go" id="contactHref"><img src="images/common/aboutFL_75x12_wht.gif" width="75" height="12" border="0" hspace="2" id="contactImage"></a><br></td>
								<td width="1"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
								<td width="1" bgcolor="#ffffff"><img src="images/common/white_bar.gif" width="1" height="11" border="0"><br></td>
								<td width="1"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
								<td width="85"><a href="ContactUs.go" id="contactHref"><img src="images/common/contactFL_81x12_wht.gif" width="81" height="12" border="0" hspace="2" id="contactImage"></a><br></td>
								<td width="5"><img src="images/common/shim.gif" width="5" height="1"><br></td>
							</tr>
						</table>
					</td>
</logic:notPresent>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td width="100%" height="1" bgcolor="#000000"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td></tr>
</table>
<!-- ************* END Marketing HEADER ********************************************************* -->
