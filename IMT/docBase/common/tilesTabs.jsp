<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %> 
<tiles:importAttribute/>

<%-- Globals --%>
<c:set var="tabSelected"><tiles:get name="tabSelected"/></c:set>

<tiles:insert page="/common/genericTabs.jsp" flush="true">
	<tiles:put name="config" direct="true">
		<tabs deadLinkOnSelected="false" selected="one" style="background-image:url(images/common/dashboard_bg.gif)">
			<item id="zero" text="Google" url="http://www.google.com"/>
			<item id="one" text="Gizmodo" url="http://www.gizmodo.com"/>
			<item id="two" text="Toms Hardware" url="http://www.tomshardware.com"/>
			<item id="three" text="Slashdot" url="http://www.slashdot.org"/>
			<item id="four" text="CNN" url="http://www.cnn.com"/>
		</tabs>
	</tiles:put>
</tiles:insert>