<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html"%>
<!-- menu -->
<c:set var="sOn" value="${activeNavItem}"/>
<c:set var="sPrintPage" value="${pageId}"/>
<c:set var="reducedToolsMenu" value="${firstlookSession.reducedToolsMenu}"/>
<c:set var="includeAgingPlan" value="${firstlookSession.includeAgingPlan}"/>
<c:set var="includeCIA" value="${firstlookSession.includeCIA}"/>
<c:set var="includePerformanceDashboard" value="${firstlookSession.includePerformanceDashboard}"/>
<c:set var="includeRedistribution" value="${firstlookSession.includeRedistribution}"/>
<c:set var="includeAppraisal" value="${firstlookSession.includeAppraisal}"/>
<c:set var="includeEquity" value="${firstlookSession.includeEquityAnalyzer}"/>
<c:set var="includeMarketStockingGuide" value="${firstlookSession.includeMarketStockingGuide}"/>
<c:set var="includeInTransitInventory" value="${firstlookSession.showInTransitInventoryForm}"/>

<c:set var="aboutLink" value="/support/Marketing_Pages/AboutUs.aspx"/>
<c:set var="contactLink" value="/support/Marketing_Pages/ContactUs.aspx"/>
<c:set var="bulletImg">&nbsp;<img src="<c:url value="/view/max/navarrow.gif" />" width="6" height="7" /></c:set>
<c:set var="profileLink"><a href="javascript:pop('/IMT/EditMemberProfileAction.go','profile');">Member Profile</a></c:set>
<c:set var="aboutLink" value="/static_page/about"/>
<c:set var="contactLink" value="/support/Marketing_Pages/ContactUs.aspx"/>
<c:if test="${!hideMenu}">
<script type="text/javascript">
var pathname = window.location.pathname;
window.onload = function(){
 var edit_save = document.getElementById('editsave');
 if(document.getElementById('hideThis')!=null)
  {
  edit_save.src = "<c:url value='/images/common/global-header-estock.png'/>";
  document.getElementById("dropnav").style["margin-top"] = "20px"
  document.getElementById("header").style["height"] = "45px"
  }
}

</script>
<div id="header" style="height:40px">

		<c:if test="${isPrintable}">
	<div class="print">
		<img src="<c:url value="/common/_images/logos/FL-print.gif"/>" class="logoFL"/>
	</div>	
		</c:if>
	<div style="float:left;dispay:inline"><img src="<c:url value='/view/max/firstlook_logo_tagline.gif'/>" id="editsave" style="height:30px;width:200px;display:inline" class="logoFLogo"/></div>
	
	<ul id="dropnav" style="margin-left:30px;margin-top:10px;display:inline-block">

		<li class="home"><a href="<c:url value="/DealerHomeDisplayAction.go"/>">HOME</a></li><%--DEALER HOME LINK --%>
		<li><ins>|</ins></li>
		<li${sOn=='tools'?' class="on"':''}><a href="#" onclick="return false;">TOOLS<c:out value="${bulletImg}" escapeXml="false"><img src="<c:url value="/common/_images/bullets/arrowDown-menu${sOn=='tools'?'On':'Off'}.gif"/>" width="8" height="8"/></c:out></a>
			<ul class="tools">
		<c:if test="${!reducedToolsMenu}">
			<c:if test="${includeCIA}">
				<li><a href="<c:url value="/CIASummaryDisplayAction.go"/>">Custom Inventory Analysis</a></li>
				<li><a href="<c:url value="/SearchHomePage.go" context="/NextGen" />">First Look Search Engine</a></li>
			</c:if>
			<c:if test="${includeCIA}">
				<li><a href="<c:url value="/FlashLocateSummary.go" context="/NextGen"/>">Flash Locator</a></li><%--DEALER HOME LINK--%>
			</c:if>
			<c:if test="${includeCIA}">
				<li><a href="<c:url value="/DealerOldHomeDisplayAction.go?forecast=1"/>">Forecaster</a></li>
			</c:if>
			<c:if test="${includeAgingPlan}">
				<li><a href="<c:url value="/InventoryPlan.go" context="/NextGen" />">Inventory Management Plan</a></li>
			</c:if>
			<c:if test="${includeEquity}">
				<li><a href="<c:url value="/EquityAnalyzer.go" context="/NextGen" />">Loan Value - Book Value Calculator</a></li>
			</c:if>	
			<li><a href="/appraisal_review">Make A Deal</a></li>	
			<c:if test="${includeRedistribution}">
				<li><a href="javascript:pop('<c:url value="/ucbp/TileManagementCenter.go"/>','mgmtCenter')">Management Summary</a></li>
			</c:if>
			<c:if test="${includeMarketStockingGuide}">
				<li><a href="<c:url value="/MarketStockingGuideRedirectionAction.go"/>">Market Velocity Stocking Guide</a></li>
			</c:if>
			<c:if test="${includeCIA}">
				<li><a href="<c:url value="/PerformanceAnalyzerDisplayAction.go"/>">Performance Analyzer</a></li>
			</c:if>	
			<c:if test="${includePerformanceDashboard}">
				<li><a href="<c:url value="/EdgeScoreCardDisplayAction.go"/>">Performance Mgmt Dashboard</a></li>
			</c:if>
		</c:if>
			<c:if test="${includeAppraisal}">
				<li><a href="<c:url value="QuickAppraise.go"/>">Trade Analyzer</a></li>
				<li><a href="<c:url value="/AppraisalManagerAction.go?"/>">Appraisal Manager</a></li>
			</c:if>
			<c:if test="${includeCIA}">	
				<li><a href="<c:url value="VehicleAnalyzerDisplayAction.go"/>">Vehicle Analyzer</a></li>
			</c:if>
			<c:if test="${includeInTransitInventory}">	
				<li><a href="<c:url value="/InTransitInventoryRedirectionAction.go"/>">Create New Inventory</a></li>
			</c:if>
			</ul>
		</li>
<c:if test="${!reducedToolsMenu}">
		<li><ins>|</ins></li>
		<li${sOn=='reports'?' class="on"':''}><a href="#" onclick="return false;">REPORTS<c:out value="${bulletImg}" escapeXml="false"><img src="<c:url value="/common/_images/bullets/arrowDown-menu${sOn=='reports'?'On':'Off'}.gif"/>" width="8" height="8" /></c:out></a>
			<ul class="reports">
				<li><a href="<c:url value="/ActionPlansReports.go" context="/NextGen" />">Action Plans</a></li>		
				<c:if test="${includeAgingPlan}">
					<li><a href="<c:url value="/DealLogDisplayAction.go"/>">Deal Log</a></li>			
				</c:if>
				<c:if test="${includeCIA}">
					<li><a href="<c:url value="/FullReportDisplayAction.go?ReportType=FASTESTSELLER&amp;weeks=26"/>">Fastest Sellers</a></li>
					<li><a href="<c:url value="/DashboardDisplayAction.go?module=U"/>">Inventory Manager</a></li>			
				</c:if>
				<c:if test="${includeAgingPlan}">
					<li><a href="<c:url value="/InventoryOverview.go" context="/NextGen" />">Inventory Overview</a></li>
				</c:if>
				<c:if test="${includeCIA}">
					<li><a href="<c:url value="/FullReportDisplayAction.go?ReportType=MOSTPROFITABLE&amp;weeks=26"/>">Most Profitable Vehicles</a></li>	
				</c:if>
				<c:if test="${includeRedistribution}">
					<%-- <li><a href="<c:url value="/ReportCenterRedirectionAction.go"/>">Performance Management Reports</a></li> --%>
					<li><a href="javascript:pop('<c:url value="/ReportCenterRedirectionAction.go"/>','reportCenter')">Performance Management Reports</a></li>
				</c:if>
				<c:if test="${sessionScope.firstlookSession.internetAdvertisersEnabled}"> 
					<li><a href="<c:url value="/PriceChangeFailureReportAction.go" context="/NextGen" />" />Price Change Failure Report</a></li>
			    </c:if>
				<c:if test="${includeAgingPlan}">
					<li><a href="<c:url value="/PricingListDisplayAction.go"/>">Pricing List</a></li>				
				</c:if>
				<c:if test="${includeCIA}">
					<li><a href="<c:url value="/StockingReports.go" context="/NextGen" />">Stocking Reports</a></li>
					<li><a href="<c:url value="/FullReportDisplayAction.go?ReportType=TOPSELLER&amp;weeks=26"/>">Top Sellers</a></li>					
				</c:if>
				<c:if test="${includeAgingPlan}">
					<li><a href="<c:url value="/TotalInventoryReportDisplayAction.go"/>">Total Inventory Report</a></li>
					<li><a href="<c:url value="/DealerTradesDisplayAction.go"/>">Trades &amp; Purchases Report</a></li>		
				</c:if>
			</ul>
		</li>
</c:if>
		<li><ins>|</ins></li>


		<li${isPrintable ? '':' class="off"'}>

<%-- legacy bits --%>		
<c:if var="hasPrintDropdown" test="${not empty enablePrintMenuForTradeAnalyzerOrTradeManager}" />
<c:set var="printJS" value="javascript:printURL('${printUrl}')"/>
<c:if test="${empty printUrl}">
	<c:set var="printJS" value="javascript:printPage()"/>
</c:if>

<c:choose>
	<c:when test="${true}">
		<a ${isPrintable ? '':'no'}href="${printJS}">PRINT<c:if test="${hasPrintDropdown}"><c:out value="${bulletImg}" escapeXml="false"><img src="<c:url value="/common/_images/bullets/arrowDown-menu${sOn=='print'?'On':'Off'}.gif"/>" width="8" height="8"/></c:out></c:if></a>
<c:if test="${hasPrintDropdown}">
	<ul class="printmenu">
		<c:choose>
			<c:when test="${title == 'KBB'}">
				<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=true&KBBreportType=equity&includeDealerGroup=${includeDealerGroup}"/>');">KBB Equity Breakdown</a></li>
				<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=true&KBBreportType=retail&includeDealerGroup=${includeDealerGroup}"/>');">KBB Retail Breakdown</a></li>
				<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=true&KBBreportType=wholesale&includeDealerGroup=${includeDealerGroup}"/>');">KBB Wholesale Breakdown</a></li>
				<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=true&includeDealerGroup=${includeDealerGroup}"/>');">KBB Wholesale/Retail Breakdown</a></li>	
				<c:if test="${firstlookSession.includeKBBTradeInValues}">
					<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=true&KBBreportType=tradeIn&includeDealerGroup=${includeDealerGroup}"/>');">KBB Trade-In Report</a></li>
				</c:if>
				<c:if test="${not empty titleSecondary}">
				<c:choose>
				<c:when test="${titleSecondary == 'NADA'}">
					<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&NADAReportType=nadaValues&includeDealerGroup=${includeDealerGroup}"/>');">NADA Values</a></li>
					<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&NADAReportType=nadaTradeValues&includeDealerGroup=${includeDealerGroup}"/>');">NADA Trade Values</a></li>
					<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&NADAReportType=nadaAppraisal&includeDealerGroup=${includeDealerGroup}"/>');">NADA Retail</a></li>
					<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&NADAReportType=nadaVehicleDescription&includeDealerGroup=${includeDealerGroup}"/>');">NADA Vehicle Description</a></li>
					<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&NADAReportType=nadaLoanSummary&includeDealerGroup=${includeDealerGroup}"/>');">NADA Vehicle Loan Summary</a></li>
					<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&NADAReportType=nadaWholesale&includeDealerGroup=${includeDealerGroup}"/>');">NADA Wholesale</a></li>
				</c:when>
				<c:otherwise>
					<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&includeDealerGroup=${includeDealerGroup}"/>');">${titleSecondary} Print Page</a></li>
				</c:otherwise>
				</c:choose>		
				</c:if>
			</c:when>
			
			<c:otherwise>
				<c:choose>
				<c:when test="${title == 'NADA'}">
					<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=true&NADAReportType=nadaValues&includeDealerGroup=${includeDealerGroup}"/>');">NADA Values</a></li>
					<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=true&NADAReportType=nadaTradeValues&includeDealerGroup=${includeDealerGroup}"/>');">NADA Trade Values</a></li>
					<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=true&NADAReportType=nadaAppraisal&includeDealerGroup=${includeDealerGroup}"/>');">NADA Retail</a></li>
					<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=true&NADAReportType=nadaVehicleDescription&includeDealerGroup=${includeDealerGroup}"/>');">NADA Vehicle Description</a></li>
					<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=true&NADAReportType=nadaLoanSummary&includeDealerGroup=${includeDealerGroup}"/>');">NADA Vehicle Loan Summary</a></li>
					<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=true&NADAReportType=nadaWholesale&includeDealerGroup=${includeDealerGroup}"/>');">NADA Wholesale</a></li>
				</c:when>
				<c:otherwise>
					<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=true&includeDealerGroup=${includeDealerGroup}"/>');">${title} Print Page</a></li>		
				</c:otherwise>
				</c:choose>
				<c:if test="${not empty titleSecondary || isKelleySecondaryGuideBook}">
					<c:choose>
						<c:when test="${isKelleySecondaryGuideBook}">
							<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&KBBreportType=equity&includeDealerGroup=${includeDealerGroup}"/>');">KBB Equity Breakdown</a></li>
							<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&KBBreportType=retail&includeDealerGroup=${includeDealerGroup}"/>');">KBB Retail Breakdown</a></li>
							<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&KBBreportType=wholesale&includeDealerGroup=${includeDealerGroup}"/>');">KBB Wholesale Breakdown</a></li>
							<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&includeDealerGroup=${includeDealerGroup}"/>');">KBB Wholesale/Retail Breakdown</a></li>	
							<c:if test="${firstlookSession.includeKBBTradeInValues}">
								<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&KBBreportType=tradeIn&includeDealerGroup=${includeDealerGroup}"/>');">KBB Trade-In Report</a></li>
							</c:if>
						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="${titleSecondary == 'NADA'}">
									<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&NADAReportType=nadaValues&includeDealerGroup=${includeDealerGroup}"/>');">NADA Values</a></li>
									<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&NADAReportType=nadaTradeValues&includeDealerGroup=${includeDealerGroup}"/>');">NADA Trade Values</a></li>
									<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&NADAReportType=nadaAppraisal&includeDealerGroup=${includeDealerGroup}"/>');">NADA Retail</a></li>
									<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&NADAReportType=nadaVehicleDescription&includeDealerGroup=${includeDealerGroup}"/>');">NADA Vehicle Description</a></li>
									<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&NADAReportType=nadaLoanSummary&includeDealerGroup=${includeDealerGroup}"/>');">NADA Vehicle Loan Summary</a></li>
									<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&NADAReportType=nadaWholesale&includeDealerGroup=${includeDealerGroup}"/>');">NADA Wholesale</a></li>
								</c:when>
								<c:otherwise>
									<li><a href="javascript:printURL('<c:url value="/PrintableTradeAnalyzerDisplayAction.go?appraisalId=${appraisalId}&guideBookPrimary=false&includeDealerGroup=${includeDealerGroup}"/>');">${titleSecondary} Print Page</a></li>
								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:otherwise>
		</c:choose>

			<c:if test="${firstlookSession.includeKBBTradeInValues}">
				<jsp:useBean id="_pars" class="java.util.HashMap"/><c:set property="vin" value="${tradeAnalyzerForm.vin}" target="${_pars}" /><c:set property="mileage" value="${not empty tradeAnalyzerForm.mileage ? tradeAnalyzerForm.mileage : 0}" target="${_pars}" />
				<li><html:link forward="kbbStandalone" name="_pars" styleClass="pop" onclick="window.open(this.href, 'kbb', 'width=950,height=600,location=no,status=yes,menubar=yes,toolbar=no,resizable=yes,scrollbars=yes');return false;" target="_blank">
					Consumer Trade-In Values
				</html:link></li>
			</c:if>
			<li><a id="printPageLink" href="javascript:printURL('<c:url value="/PrintableTAFirstlookDisplayAction.go?appraisalId=${appraisalId}&includeDealerGroup=${includeDealerGroup}&weeks=${weeks}"/>');">Firstlook Print Page</a></li>		
				<c:if test="${isTradeManager && showAppraisalForm && appraisalType == 1}">
					<li><a href="javascript:pop('<c:url value="/AppraisalFormDisplayAction.go"><c:param name="appraisalId" value="${tradeAnalyzerForm.appraisalId}"/><c:param name="vin" value="${tradeAnalyzerForm.vin}"/><c:param name="mileage" value="${tradeAnalyzerForm.mileage}"/></c:url>');">Appraisal Form</a></li>		
				</c:if>
	</ul>
	<%--<c:import url="/dealer/tools/includes/printingProgress.jsp" />--%>		
	</c:if>
	</c:when>
	<c:otherwise>
		<a ${isPrintable ? '':'no'}href="javascript:printPage('${sPrintPage}')">PRINT</a>
	</c:otherwise>
</c:choose>


		</li>
		<li><ins>|</ins></li>
		<li><a href="<c:url value="ExitStoreAction.go"/>">EXIT STORE</a></li>
	</ul>

	<div class="textnav" style = "float:right;margin-top:10px;display:inline-block">
		<c:out value="${profileLink}" escapeXml="false"><a href="javascript:pop('<c:url value="/EditMemberProfileAction.go"/>','profile');">Member Profile</a></c:out><ins>|</ins><a href="javascript:pop('${aboutLink }','');">About First Look</a><ins>|</ins><a href="javascript:pop('${contactLink }','');">Contact First Look</a><ins>|</ins><a href="<c:url value="LogoutAction.go"/>">Log Out</a>
	</div>

</div>
<br>
</c:if>
<!-- /menu -->
<%-- required to prevent select boxes showing through in IE --%>
<!--[if lte IE 6.5]><iframe src="javascript:false;" id="selectFix" class="selectFix" style="display: none;"></iframe><![endif]-->
