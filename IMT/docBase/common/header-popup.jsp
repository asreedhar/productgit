<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div id="header">

	<img src="<c:url value="/view/max/firstlook_logo_tagline.gif" />" class="logo"/>
	<div class="textnav">
<c:if test="${isPrintable}"><a href="javascript:printPage();"><img src="<c:url value="/common/_images/text/printOn333.gif"/>" alt="Print" width="24" height="11"/></a>
<ins>|</ins>
</c:if>
<a href="#" onclick="window.close();" id="closeWindowButton"><!-- 
 <img src="<c:url value="/common/_images/text/closeWindowOn333.gif"/>" alt="Close Window" width="69" height="11"/>
 -->
 Close Window
 </a>
	</div>
</div>