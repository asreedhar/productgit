var aBooks = new Array('BB','KBB','NADA', 'GALVES');

function parentReload() {
	if (!!window.bMileChanged) {
		window.bMileChanged = undefined;
		for (var i=0; i < aBooks.length; i++) {
			toggleWarning('rebook'+aBooks[i],'on');
		};
	};
	
	var reloadWindow = function(w) {
		if (w.opener && !w.opener.closed) {
			if (w.opener.bPageReload) {
					if(w.opener.location.pathname=='/NextGen/InventoryPlan.go') {
						var id = document.vehicleInfoForm.inventoryId.value;
						w.opener.page.handleDirty(id);
						return;
					} else if (w.opener.location.href=='/NextGen/InventoryOverview.go'+w.opener.location.hash) {
						// removes cas ticket that causes trouble
						w.opener.location.href='/NextGen/InventoryOverview.go';
					} else {
						w.opener.location.reload();
					}				
			}
		}
	};
	
	if (window.opener)
		reloadWindow(window.opener)
	
	reloadWindow(window);
}

window.onload = setCatalogKeyDefault;
window.onunload = parentReload;

function flagPageReload() {
	if (window.opener && !window.opener.closed) {
    	window.opener.bPageReload = true;
    }
}

// third party functions
//
function viewCarfaxReport(sVIN,reportTypeVal) {
	if (reportTypeVal == "NONE") {
		return false;
	}
	
	var x = document.getElementsByName("reportType");
	for(var k = 0; k < x.length && reportTypeVal == null; k++){
  		if(x[k].checked){
    		reportTypeVal = x[k].value;
  		}
  	}
	if(reportTypeVal == null) {
		return false;
	}
	
	var bIncludeInHotList = document.getElementById("cccFlagFromTile").checked;
	new Ajax.Updater('carfaxReport', 'CarfaxGetReportAction.go', {method:'get', parameters: {vin: sVIN, reportType: reportTypeVal, cccFlagFromTile:  bIncludeInHotList } } );

}
function viewAuctionReport(sURL) {
	var sURL = sURL;
	
	if (sURL.search('vicBodyStyleName') == -1) {
		var nBodySel = document.getElementById("vic").selectedIndex;
		if (nBodySel == 0)
		{
			alert('Please Select a Series');
			return;
		}
		
		var nBody = document.getElementById("vic").options[nBodySel].value;
        var sBodyName = document.getElementById("vic").options[nBodySel].text;
		sURL += '&vic=' + nBody + '&vicBodyStyleName=' + sBodyName;
	}
	
	var nArea = document.getElementById("areaId").options[document.getElementById("areaId").selectedIndex].value;
	var nTime = document.getElementById("timePeriodId").options[document.getElementById("timePeriodId").selectedIndex].value;	
	sURL += '&areaId=' + nArea + '&timePeriodId=' + nTime;
 	pop(sURL,'thirdparty');
 	return;
}

function submitGMAC(sURL) {
	retrieveURL(sURL, 'noForm', 0, 'false');
	var gmacErr;
	var gmacErr = document.getElementById('gmacErr');
	if (!gmacErr) {
		var gmacURL = document.getElementById('gmacURL').href;
		pop(gmacURL,'thirdparty');
	}
	else {
		alert('Error: ${GMACError}\n If problem continues, please contact your account manager');
	}
}

//no changes from before
function changeDropDown(isAppraisal, appraisalId, vin, mileage)
{
    if(document.getElementById("vic").options)
    {
        var vic = document.getElementById("vic").options[document.getElementById("vic").selectedIndex].value;
    }
    else
    {
        var vic = document.getElementById("vic").value
    }
    var area = document.getElementById("areaId").options[document.getElementById("areaId").selectedIndex].value;
    var timePeriod = document.getElementById("timePeriodId").options[document.getElementById("timePeriodId").selectedIndex].value;
	var usingVIC = $("usingVIC").value;

    if(vic != "" && vic && area != "" && area && timePeriod != "" && timePeriod)
    {
// mileage needs to be on this URL to update the NAAA auction tile - see saveAndRebook in bookout.js
        var url = "ucbp/AuctionDataResults.go?vin=" + vin + "&mileage=" + mileage;
        url += "&isAppraisal=" + isAppraisal + "&appraisalId=" + appraisalId;
        url += "&vic=" + vic;
        url += "&areaId=" + area;
        url += "&timePeriodId=" + timePeriod ;
		url += "&usingVIC=" + usingVIC;

        document.getElementById("auctionIFrame").src = url
    }
}

function submitSearch(obj) {
	formName = obj.name;
	url = obj.action;
	retrieveURL(url,formName);
}


////// update the bookVsCost field when rebooking a guide book
function updateBookVsCost()
{
	var nUnitCost = stripNumberInput(document.getElementById("unitCost").innerHTML);
	var nPrimaryGuideBook = stripNumberInput(document.getElementById("newPrimaryGuideBookValue").innerHTML);
	var nBookVsCost = nPrimaryGuideBook - nUnitCost;
	var sBookVsCost;

	if ( isNaN(nBookVsCost)) {
		sBookVsCost = "N/A";
	}
	else {
		var sign = (nBookVsCost == (nBookVsCost = Math.abs(nBookVsCost)));
		if( nBookVsCost > 999)
		{
			nBookVsCost = (Math.floor( nBookVsCost / 1000 )) + ',' + leadingZero( (nBookVsCost % 1000));
		}
				
		sBookVsCost = ((sign)?'':'(') + '$' + nBookVsCost + ((sign)?'':')');
	}

	document.getElementById("primaryGuideBookValue").innerHTML = document.getElementById("newPrimaryGuideBookValue").innerHTML;
	document.getElementById("bookVscostValue").innerHTML = sBookVsCost;
}

function leadingZero(x){
   y=(x>9)?x:'00'+x;
   y=(x>99)?x:'0'+x;
   return y;
}



////// helper functions
function toggleClassNames(obj,class1,class2) {
	var has1 = Element.hasClassName(obj,class1);
	var has2 = Element.hasClassName(obj,class2);
	if(has1) {
		Element.removeClassName(obj,class1);
		Element.addClassName(obj,class2);
	}
	if(has2) {
		Element.removeClassName(obj,class2);
		Element.addClassName(obj,class1);
	}
}
function toggleHideAndShow(obj) {
	toggleClassNames(obj,'show','hide');
}

//-->
function toggleDisplay(sClassName) {
var sClassName;
	sClassName == 'show' ? sClassName = 'hide' : sClassName = 'show';
	return sClassName;
}
//<--


function switchMode(whichDiv) {
	//parent div
	var parentObj = $(whichDiv);
	document.getElementById('success').innerText = '';
	toggleClassNames(parentObj,'edit','display');

}
function changedMileageValues(inputValue) {
	var newMileage = stripNumberInput(inputValue);
	var oldMileage = stripNumberInput(document.getElementById('mileage').innerText);
	if (newMileage != oldMileage ) return true;
	return false;
}

function changedListPrice(inputValue) {
	var price = validatePrice(inputValue);
	if(price !== false){
		var oldListPrice = stripNumberInput(document.getElementById('list').innerText);
		if (parseInt(price)  != parseInt(oldListPrice) ) {
			document.getElementById('listL').value = price;
			document.getElementById('listPriceChanged').value = true;
		}
	}
}

function changedLotPrice(inputValue) {
	var price = validatePrice(inputValue);
	if(price !== false){
		var oldListPrice = stripNumberInput(document.getElementById('lotPrice').innerText);
		if (parseInt(price)  != parseInt(oldListPrice) ) {
			document.getElementById('lotPriceL').value = price;
		}
	}
}


function changedEdmundsTMV(inputValue) {
	var price = validatePrice(inputValue);
	if(price !== false){
		var oldListPrice = stripNumberInput(document.getElementById('edmundsTMV').innerText);
		if (parseInt(price)  != parseInt(oldListPrice) ) {
			document.getElementById('edmundsTMVL').value = price;
		}
	}
}

function changedTransferPrice(inputValue) {
	var price = validatePrice(inputValue);
	if(price !== false){
		var oldListPrice = stripNumberInput(document.getElementById('transferPrice').innerText);
		if (parseInt(price) != parseInt(oldListPrice) ) {
			document.getElementById('transferPriceL').value = price;
			document.getElementById('transferPriceChanged').value = true;  
		}
	}
}

function cancelAndSwitch(formName,switchId) {
	//reset form
	if (document.forms[formName]) document.forms[formName].reset();
	switchMode(switchId);
	
	toggleWarning('inaccurateEdit','off');
	// makes sure the 'changes have been saved' text doesn't appear
	document.getElementById('success').innerText = '';
	//remove the reprice confirmation popup if the edit window is closed.
	var confirmPopup = document.getElementById('confirmBox');
	if(confirmPopup){confirmPopup.parentNode.removeChild(confirmPopup);}
	var confirmTransferPopup = document.getElementById('confirmTransferBox');
	if(confirmTransferPopup){confirmTransferPopup.parentNode.removeChild(confirmTransferPopup);}
	
	document.vehicleInfoForm.mileageL.value = stripNumberInput(document.getElementById('mileage').innerText);
	document.vehicleInfoForm.listL.value = stripNumberInput(document.getElementById('list').innerText);
	if(document.vehicleInfoForm.transferPriceL != null) {
		document.vehicleInfoForm.transferPriceL.value = stripNumberInput(document.getElementById('transferPrice').innerText);
	}
	
	document.vehicleInfoForm.colorsInput.value = document.getElementById('BaseColor').innerText;
}

function saveVehicleChanges(url,formName,switchId){
	//see if mileage has changed
	var bMileChanged = changedMileageValues(document.forms[formName].mileage.value);
	document.forms[formName].mileageChanged.value=bMileChanged;
	
 	// activate blue i's
 	if (bMileChanged) {
		//check each
		for (i=0;i<aBooks.length;i++) {
			if (document.getElementById('rebook'+aBooks[i])) {
				toggleWarning('rebook'+aBooks[i],'off'); /// don't show books before update, PM
				if (document.getElementById('inaccurate'+aBooks[i])) {
					if (document.getElementById('inaccurate'+aBooks[i]).style) {
						document.getElementById('inaccurate'+aBooks[i]).style.display = 'inline';
					}
				}
			}
		}
		toggleWarning('inaccurateEdit','off');
		toggleWarning('inaccurateDisplay','on');
		
		toggleInaccurate(1, true);
		toggleInaccurate(2, true);
		toggleInaccurate(3, true);
		toggleInaccurate(4, true);
		
 		//need to change light color based on reply
	 	//var oLight = document.getElementById('estocklight');	
	 	window.bMileChanged = true;
 	}
 	switchMode(switchId);
 	document.getElementById('success').innerText = 'Attempting to save changes...';
	retrieveURL(url,formName,'vehicleInformation',false,parentReload);
}
function saveAndSwitch(url,formName,switchId ) {
	var listPriceOk = true;
	var transferPriceOk = true;
	// this updates the trade in link to make sure it has the most up to date mileage
	if ( $('kbbStandAloneLink') ) {
		oldString = $('kbbStandAloneLink').href.substring( $('kbbStandAloneLink').href.indexOf('mileage='), $('kbbStandAloneLink').href.indexOf('&vin') );
		newString = 'mileage=' + $F('mileageL');
		$('kbbStandAloneLink').href = $('kbbStandAloneLink').href.replace(oldString, newString)
	}
	
	if ( $('nadaStandAloneLink') ) {
		oldString = $('nadaStandAloneLink').href.substring( $('nadaStandAloneLink').href.indexOf('mileage='), $('nadaStandAloneLink').href.indexOf('&vin') );
		newString = 'mileage=' + $F('mileageL');
		$('nadaStandAloneLink').href = $('nadaStandAloneLink').href.replace(oldString, newString)
	}
	
	var listPriceChanged = document.getElementById('listPriceChanged').value == 'true'; 
	if(listPriceChanged) {
		listPriceOk = false;
	 	var invId = document.vehicleInfoForm.inventoryId.value;	
		var price = document.getElementById('listL');

		//see global.js
		confirmReprice(price.value, invId, $('listL'),
			function() {
				updatePlanningTile(); // update planning tile to reflect Reprice
				listPriceOk = true;
				saveTransferPrice();
			}); 
	} else {
		saveTransferPrice();
	}
	
	function saveTransferPrice() {
		var transferNode = document.getElementById('transferPriceChanged');
		if(transferNode != null && transferNode.value == 'true') {
			transferPriceOk = false;
		 	var invId = document.vehicleInfoForm.inventoryId.value;	
			var price = document.getElementById('transferPriceL');
	
			confirmTransferReprice(price.value, invId, $('transferPriceL'),
				function() {
					transferPriceOk = true;
					handleReprices(listPriceOk, transferPriceOk);
				}); 
		} else {
			handleReprices(listPriceOk, transferPriceOk);
		}
	}

	function handleReprices(l, t) {
		if(l && t) {
			saveVehicleChanges(url,formName,switchId);
		}
	}
}

function saveRebookAndSwitch(url,formName,switchId) {
	//see if mileage has changed
	var bMileChanged = changedMileageValues(document.forms[formName].mileage.value);
	document.forms[formName].mileageChanged.value=bMileChanged;
	retrieveURL(url,formName);
	switchMode(switchId);
	document.getElementById('success').innerText = 'Attempting to save changes...';
	toggleWarning('inaccurateEdit','off');
	toggleWarning('inaccurateDisplay','off');
	var aActiveForms = findBookForms();
	for (i=0; i<=aActiveForms.length; i++) {
		if(aActiveForms[i]) 
		{
			rebookWithNewMileage('Temp'+ (i+1), null, document.vehicleInfoForm.mileageL.value);
		}
	}
}

function setCatalogKeyDefault() {

	var tempSelect = new Array();
	if ($("incomingCatalogKeyId").value == -1) {
		tempSelect[0] = new Option('Please select a trim...','0');
		for (i = 1; i<=document.vehicleInfoForm.catalogKeySelect.options.length; i++) {
			tempSelect[i] = document.vehicleInfoForm.catalogKeySelect.options[i-1];
		}
		for (i = 0; i<tempSelect.length; i++) {
			document.vehicleInfoForm.catalogKeySelect.options[i] = tempSelect[i];
		}
		document.vehicleInfoForm.catalogKeySelect.selectedIndex = 0; 
	}  else {
		document.vehicleInfoForm.catalogKeySelect.selectedIndex =$("incomingCatalogKeyId").value; 
	}
	updateUnderTheHoodInformation(document.vehicleInfoForm.catalogKeySelect.selectedIndex);
}

function updateUnderTheHoodInformation(selectedId) {	
	//swap out the divs so correct value displays
	//update the forms so right value is returned
	if (document.vehicleInfoForm.catalogKeySelect.options[selectedId].value != 0) { //not the please select trim option
		document.vehicleInfoForm.catalogKeyId.value = document.vehicleInfoForm.catalogKeySelect.options[selectedId].value;
		document.vehicleInfoForm.updatedCatalogKey.value = "true";
	}	
}

// this is used to include the inventory planning tile on the estock card
function updatePlanningTile() {
	new Ajax.Updater('plan', '/NextGen/EstockCardInventoryPlan.go', {method:'get', parameters: 'inventoryId=' + $F('inventoryId') } );
}

function togSummary() {
	Element.toggle('detail');
}

function confirmTransferReprice(transferPrice, invId, ele, ok){
	if(validatePrice(transferPrice) !== false) {
		var ajax = new AjaxLoader();
		
		function handleTransferRepriceConfirmationResponse(resp){
			var confirm = eval('('+resp.getResponseHeader('confirm')+')');  //confirm is true then show confirm box
			if(confirm){
				ele.onfocus =function(){ele.blur();}  //disables element but allows styles
				Element.addClassName(ele,'red');
				var  div = '<div id="confirmTransferBox"  style="z-index:1050;left:-100px; top:-100px; padding:10px;  height:20px; position:absolute; display:none;"></div>';
				if($('confirmTransferBox')){ Element.remove($('confirmTransferBox'));}
				new Insertion.After($('footer'),div);		

				$('confirmTransferBox').innerHTML = resp.responseText;
				$('transferPriceConfirmOk').onclick=function(){ Element.removeClassName(ele,'red');ele.onfocus='';return false;}
				
				 var  p = new Popup();			
				 p.popup_show('confirmTransferBox', '', 'transferPriceConfirmOk', 'element-bottom',100,false,-30,-7, ele.id);
			}else{
				if($('confirmTransferBox')){ Element.remove($('confirmTransferBox'));}
				ok(false);
			}
		}
		
		function handleTransferRepriceConfirmationException(resp,e){
			alert('A server error has occurred while checking the Transfer Price.\n\r' + e.message);
		}
		
		ajax.setPath('/IMT/TransferPriceConfirm.go');
		ajax.setParameters('&transferPrice='+transferPrice+'&inventoryId='+invId);
		ajax.setOnSuccess(handleTransferRepriceConfirmationResponse);
		ajax.setOnException(handleTransferRepriceConfirmationException);
		ajax.performAction();
	}
	
		
}
	
function updateMMRAveragePrice(){
	
	if(jQuery("#mmrTrimDropDown").val()=='selectatrim'){
		
		alert("Please Select an MMR Trim");
		jQuery(document.MMRAvgForm).focus();
		return false;
				
	}
	
	var oldHtml=jQuery("#MMRAverageDetailsDiv").html();
	var params = jQuery(document.MMRAvgForm).serializeArray();
	var html="<div style='align:center;v-align:middle'>";
	html+="<img src='goldfish/public/images/loading.gif'  ><br>";
	html+="Getting Values ...";
	html+="</div>";
	jQuery("#mmrdetailsdiv").html(html);
	
	

		jQuery.ajax({
			url : "MMRSaveDataAction.go",
			request : "POST",
			data : params,
			success : function(xyz) {
				//alert('success');
				jQuery("#MMRAverageDetailsDiv").html(xyz);
				
				
				
			},
			failure : function() {
				alert("Could Not Save Data");
				jQuery("#MMRAverageDetailsDiv").html(oldHtml);
	
			}

		});
	
	
	
}
function updateMMRAveragePriceandSave(){
	if(jQuery("#mmrTrimDropDown").val()=='selectatrim'){
		
		alert("Please Select an MMR Trim");
		jQuery(document.MMRAvgForm).focus();
		return false;
				
	}
	
	jQuery("#isDBUpdate").val("true")
	var oldHtml=jQuery("#MMRAverageDetailsDiv").html();
	var params = jQuery(document.MMRAvgForm).serializeArray();
	var html="<div style='align:center;v-align:middle'>";
	html+="<img src='goldfish/public/images/loading.gif'  ><br>";
	html+="Saving ...";
	html+="</div>";
	jQuery("#mmrdetailsdiv").html(html);
	
	

		jQuery.ajax({
			url : "MMRSaveDataAction.go",
			request : "POST",
			data : params,
			success : function(xyz) {
				//alert('success');
				jQuery("#MMRAverageDetailsDiv").html(xyz);
				//jQuery("#MMRAverageDetailsDiv").focus();
				
				
				
			},
			failure : function() {
				alert("Could Not Save Data");
				jQuery("#MMRAverageDetailsDiv").html(oldHtml);
	
			}

		});
	
	
	
}


