
var appraisalsAndActions = '';
	// collects selection values
function storeDecisionValues(appraisalId,actionId) {

	var positionFromTop = window.scrollY;
	if(window.scrollY == undefined)
	{
		positionFromTop = document.documentElement.scrollTop;
	}
	
	$("#focusedPosition").val(positionFromTop);

	appraisalsAndActions += appraisalId + '&' + actionId + '|';
	submitDecisionValues();
}

function submitDecisionValues() {
	document.getElementById('storedActionPairs').value = appraisalsAndActions;
	appraisalsAndActions=''
	var params = jQuery("#TradeManagerRedistributionForm").serializeArray();
	//var params={};
	//params[0]=params1[0];
	//params[1]= params1[1];
		jQuery.ajax({
			url : "/IMT/TradeManagerSubmitAction.go",
			request : "POST",
			data : params,
			success : function(xyz) {
				//alert('success');
				
				pageLoaded="Not";
				jQuery("#"+jQuery("#currentTab").val().toLowerCase()+"Link").click();
				pageLoaded="Loaded";				

				
				
			},
			failure : function() {
				alert("Failure");
				//functionshow("tabs");
	
			}

		});
	
}

function confirmSold(tapid) {
    var url= "TradeManagerSoldAction.go?pageName=bullpen&appraisalId=" + tapid;
    var result = window.confirm("Press OK to confirm this vehicle is Sold.\nPress Cancel to cancel this action.");
    if(result) {
        document.location.href = url;
    } else {
        return;
    }
}
try { if(Prototype) {
    Event.observe(window, 'load', function() {
        var f = $('TradeManagerRedistributionForm');
        if( f ) // f will be null if there are no vehicles to be displayed
        {
            var vins = f.getInputs('hidden', 'vin');
            for(var i=0; i<vins.length; i++) {
                var _el = vins[i];
                var _row = _el.up(1);
                if(_row.tagName == 'TR') {
                    _row.title = 'VIN: ' + _el.value;
                    Event.observe(_row, 'mouseover', addHilite);
                    Event.observe(_row, 'mouseout', removeHilite);
                }
            }
        }
    });
} } catch(e) {}

// 
// TRADE MANAGER scripts 

var addHilite = function(e) {
    var el = Event.element(e);
	if(el.tagName != 'TR') {
        el = Event.findElement(e, 'TR');
	} 
	Element.addClassName(el, 'hilite');
}
var removeHilite = function(e) {
    var el = Event.element(e);
	if(el.tagName != 'TR') {
        el = Event.findElement(e, 'TR');
	} 
	Element.removeClassName(el, 'hilite');
}



// 
// WAITING APPRAISALS scripts

function deleteWaitingAppraisal(id, group) {


	var obj = document.getElementById("delete_"+id);
	
	
	
	var msg = 'Click OK to permanently DELETE appraisal?';
	var ok = confirm(msg);
	
	if(ok) {
		var xhr = new Ajax.Request(
			'/IMT/DeleteAppraisalAction.go', 
			{
				method: 'post', 
				parameters: 'appraisalId='+id, 
				onComplete: function(xhr) {
					// updateCounts
					var el = document.getElementById('waitingAppraisalsCount');
									
					el.innerHTML = parseInt(el.innerHTML) - 1;	
					
					loadURL("awaiting");
				}
			});		
	} else {
	
	}
}