try {
	document.execCommand("BackgroundImageCache", false, true);
} catch(err) {}
// 
// POPUP scripts 

	//all purpose popup
function pop(sPath,sWin,param,ele) {
	var sAttr = 'location=no,status=yes,menubar=yes,toolbar=no,resizable=yes,scrollbars=yes,';
	var sWinName = sWin;
		if (!sWin) {
			sWinName = 'popup';
		} 
	
	if(param && param!=''){
      if(document.getElementById(ele)){
    		sPath += sPath.indexOf('?') != -1 ? '&':'?' +param+"="+document.getElementById(ele).value;
      }
	}
	//modify popup size based on target
	switch (sWinName) {	
		case 'photos':
			sAttr += 'width=720,height=500';
			break;	
		case 'profile':
		case 'plus':
		case 'deals':
		case 'thirdparty':		
			sAttr += 'width=805,height=575';
			break;
		case 'mgmtCenter':
			sAttr += 'width=470,height=615';
			break;
		case 'estock':
			var aScreen = findScreenSize();
			sAttr += aScreen[1] >= 1024 ? 'width=900,height=870':'width=900,height=700';
			sPath += '&isPopup=true';
			break;
		case 'promo':
		case 'dialog':		
			sAttr += 'location=no,status=yes,menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=350,height=250';
			break;
		case 'legal':
			sAttr += 'width=780,height=450';
			break;
		case 'sticker':
		case 'buyersGuide':
			sAttr += 'location=no,status=yes,menubar=yes,toolbar=no,resizable=yes,scrollbars=yes,width=800,height=830';
			break;
		case 'price':
			sAttr += 'width=950,height=600';
			break;
		case 'appraisalForm':
			sAttr += 'width=950,height=650';
			break;
		case 'lithia':
			sAttr += 'width=575,height=650';
			break;
		case 'bookout':
			sAttr+= 'width=820,height=600,scrollbars=yes,resizable=yes';
			break;
		case 'jdpower':
			sAttr+= 'width=968,height=700,scrollbars=yes,resizeable=yes';
			break;
		case 'carfaxTIR':
			sAttr += 'width=575,height=230,scrollbars=yes,resizeable=yes';
			break;
		case 'pop':
		default:
			sAttr += 'width=800,height=600';
	}
	window.open(sPath, sWinName, sAttr);
}

function closeWindow() {
	window.close();
}

var Util = {

	initialize:function(){	},
	/* Method closes the browser window */
	closeWindow:function(){
		window.close();
	},
	isIE:function(){
	
		return  navigator.appName.indexOf('Microsoft Internet Explorer') != -1;
	},


toggleClassNames:function(obj,class1,class2) {
	var has1 = Element.hasClassName(obj,class1);
	var has2 = Element.hasClassName(obj,class2);
	if(has1) {
		Element.removeClassName(obj,class1);
		Element.addClassName(obj,class2);
	}
	if(has2) {
		Element.removeClassName(obj,class2);
		Element.addClassName(obj,class1);
	}
},

	/**
	*Method turns price with non numeric vals into a number
	*
	**/
	 formatPrice:function(price)
	{
		var indexOfPeriod = price.indexOf(".");
		if (indexOfPeriod >= 0)
		{
			price = price.substring(0, indexOfPeriod);
		}
		var indexOfComma = price.indexOf(",");
		if (indexOfComma >= 0)
		{
			price = price.substring(0, indexOfComma) + price.substring( (indexOfComma + 1), price.length );
		}
		var indexOfDollar = price.indexOf("$");
		if (indexOfDollar >= 0)
		{
			price = price.substring( (indexOfDollar + 1), price.length );
		}
		return price;
	},	
	/**
	*Method to scroll and center an element on screen
	*
	**/
	scrollWindowTo:function(id){

		var coords = this.findPosition($(id));  //find coordinates of the element
		var winHeight =  this.getViewportDims();  //get height of browser window
		var eleHeight = $(id).offsetHeight;
		
		if(!eleHeight){
		
			eleHeight = 0;
		}
		
		var yLoc = coords[1] - ((winHeight[1]/2) -  (eleHeight/2));  //y location of element =element  position - .5 window height - .5 element height
		
		window.scrollTo(0,yLoc);   //scroll window to this location

	},
	
	/**
	*Method accepts an element as a parameter and returns an array representing
	*the x and y coordinates of the element
	**/
	findPosition:function(oElem) {
		if ($(oElem).offsetParent) {	
			for (var posX = 0, posY = 0; $(oElem).offsetParent; oElem = $(oElem).offsetParent) {
				posX += $(oElem).offsetLeft;
				posY += $(oElem).offsetTop;
	    	}
			return [posX, posY];
		} 
		else {
			return [$(oElem).x, $(oElem).y];
	  	}
	},
	/**
	*Method a returns an array representing
	*the x and y coordinates of the screen
	**/
	findScreenSize:function() {
		var x; var y;
		var x = window.screen.width;
		var y = window.screen.height;
		return [x, y];	
	},
	/**
	*Method a returns an array representing
	*the x and y coordinates of the viewport - chrome
	**/
	 findViewportDims:function() {
	var x; var y;
		// non-explorer
	if (self.innerHeight) {
		x = self.innerWidth;
		y = self.innerHeight;
	}
		// explorer 6 strict
	else if (document.documentElement && document.documentElement.clientHeight)	{
		x = document.documentElement.clientWidth;
		y = document.documentElement.clientHeight;
	}
		// other explorers
	else if (document.body) {
		x = document.body.clientWidth;
		y = document.body.clientHeight;
	}
	//viewX = x;
	//viewY = y;
	return [x, y];
	},
	
	printPage:function(){

			window.print();
	
	},
	/**
	*Method returns boolean true if obj is an array
	**/
	isArray:function(obj){
 
 		 return this.isObject(obj) && obj.constructor == Array;

	},
	 isFunction:function(a) {
  		  return typeof a == 'function';
	},
	isObject:function(a) {
   		 return (a && typeof a == 'object') || this.isFunction(a);
	},
	stripTags:function(str) {
	var s = 0;
	var e = -1;
	var r = "";

	s = str.indexOf("<",e);	

	do {
		r += str.substring(e + 1,s);
		e = str.indexOf(">",s);
		s = str.indexOf("<",e);
	}
	while ((s != -1) && (e != -1))

	r += str.substring(e + 1,str.length);

	return r;
},
/* Accepts a date format and date string as parameters, defaults to today
*Format should be a string consisting of the following characters
*Other characters will be displayed in place.
* d: 2 digit day of month
* j: 1 digit day of month
*M: abbreviated month (Oct, Nov)
*F: full month
*m: 2 digit month
*n: 1 digit month
*D: abbreviated day (Sun, Mon)
*l (lower case L): full day name
*S: day suffix (st,th,rd,nd)
*Y: 4 digit year
*y: 2 digit year
*A: uppercase am/pm
*a: lower case am/pm
*g: 12 hour hours without leading 0
*G: 24 hour hours without leading 0
*h: 12 hour hours with leading 0 (2 digit)
*H: 24 hour hours with leading 0 (2 digit)
*i: minutes 2 digit
*s: seconds 2 digit
*/
getFormattedDate:function(fmt, dte){

	if(!fmt){
		alert('A date format is required');
		return;
	}
	var d = new Date();
	
	if(dte){
	
		d.setTime(Date.parse(dte));
	}
	var month = d.getMonth() +1;
	var date = d.getDate();
	var year = d.getFullYear();
	var shortYear = d.getYear();
	var hour = d.getHours();
	var minutes = d.getMinutes();
	var day = d.getDay();
	var seconds = d.getSeconds();
	var retStr = '';
	
	for(var index = 0; index < fmt.length; index++){
	
		switch( fmt.charAt(index)){
			case  'd': //2 digit day of month
				var dateStr = date < 10?'0'+date:date;
				retStr += dateStr;
			break;
			case 'j': //1 digit day of month
				var dateStr = date;
				retStr += dateStr;	
			break;
			case 'M': //abbreviated month
				var monthStr = '';
					switch(month){	
						case 1:
							monthStr = 'Jan';
						break;
						case 2:
							monthStr = 'Feb';
						break;
						case 3:
							monthStr = 'Mar';
						break;
						case 4:
							monthStr = 'Apr';
						break;
						case 5:
							monthStr = 'May';
						break;
						case 6:
							monthStr = 'Jun';
						break;
						case 7:
							monthStr = 'Jul';
						break;
						case 8:
							monthStr = 'Aug';
						break;
						case 9:
							monthStr = 'Sep';
						break;
						case 10:
							monthStr = 'Oct';
						break;
						case 11:
							monthStr = 'Nov';
						break;
						default:
							monthStr = 'Dec';
						break;	
					}
					retStr += monthStr;	
			break;
			case 'F': //full month
				var monthStr = '';
				
				switch(month){	
						case 1:
							monthStr = 'January';
						break;
						case 2:
							monthStr = 'February';
						break;
						case 3:
							monthStr = 'March';
						break;
						case 4:
							monthStr = 'April';
						break;
						case 5:
							monthStr = 'May';
						break;
						case 6:
							monthStr = 'June';
						break;
						case 7:
							monthStr = 'July';
						break;
						case 8:
							monthStr = 'August';
						break;
						case 9:
							monthStr = 'September';
						break;
						case 10:
							monthStr = 'October';
						break;
						case 11:
							monthStr = 'November';
						break;
						default:
							monthStr = 'December';
						break;	
				
				}
			retStr += monthStr;	
			break;
			case 'm': //2 digit  month
				var monthStr = month < 10?'0'+month:month;
				retStr += monthStr;
			break;
			case 'n': //1 digit month
				var monthStr = month;
				retStr +=monthStr;
			break;
			case 'D': //3 letter day abbreviation
				var dayStr = '';
				
				switch(day){
				
					case 0:
						dayStr = 'Sun';
					break;
					case 1:
						dayStr = 'Mon';
					break;
					case 2:
						dayStr = 'Tue';
					break;
					case 3:
						dayStr = 'Wed';
					break;
					case 4:
						dayStr = 'Thu';
					break;
					case 5:
						dayStr = 'Fri';
					break;
					default:
						dayStr = 'Sat';
					break;
				
				}
				retStr+= dayStr;
			break;
			case 'l'://full day 'L' lowercase
				var dayStr = '';
				
				switch(day){
				
					case 0:
						dayStr = 'Sunday';
					break;
					case 1:
						dayStr = 'Monday';
					break;
					case 2:
						dayStr = 'Tuesday';
					break;
					case 3:
						dayStr = 'Wednesday';
					break;
					case 4:
						dayStr = 'Thursday';
					break;
					case 5:
						dayStr = 'Friday';
					break;
					default:
						dayStr = 'Saturday';
					break;
				
				}
				retStr += dayStr;
			break;
			
			case 'S': //day suffix
				var daySuffix = '';
				
				switch(date){
				
					case 1 :			
					case 21:			
					case 31:
							daySuffix = 'st';
					break;
					case 2 :			
					case 22:			
							daySuffix = 'nd';
					break;
					case 3 :			
					case 23:			
							daySuffix = 'rd';
					break;
					default:
						daySuffix = 'th';
					break;
				
				}
				retStr += daySuffix;
			break;
			case 'Y': //4 digit year
			
					retStr += year;
			
			break;
			case 'y'://2 digit year
			
					retStr += shortYear;
			
			break;
			case 'a'://lowercase ampm
				var ampm = 'am';
				if(hour >= 12){
					ampm = 'pm';
				}
			retStr += ampm;
			break;
			case 'A'://capital ampm
				var ampm = 'AM';
				if(hour >= 12){
					ampm = 'PM';
				}
				retStr += ampm;
			break;
			case 'g': //12 hour hours without leading 0
				var hourStr = '';
				if( hour > 12){
					hourStr = hour - 12;
				}else{
					hourStr = hour;
				}
				retStr += hourStr;
			break;
			case 'G'://24 hour hours without leading 0
				var hourStr = '';
				
				hourStr = hour;		
				
				retStr += hourStr;
			break;
			case 'h': //12 hour hours with leading 0
				var hourStr = '';
				if( hour > 12){
					hourStr = hour - 12;
				}else{
					hourStr = hour;
				}
				if(hour < 10){
					hourStr = '0'+hourStr;
				}
				retStr += hourStr;
			break;
			case 'H'://24 hour hours with leading 0
				var hourStr = '';
				
				hourStr = hour;
				
				if(hour < 10){
					hourStr = '0'+hourStr;
				}
			retStr += hourStr;
			break;
			case 'i': //minutes with leading 0
				var minutesStr = minutes;
				if(minutes < 10){
					minutesStr = '0'+minutesStr;
				}
				retStr += minutesStr;
			break;
			
			case 's':	//seconds with leading 0
				var secondStr = seconds;
				
				if(seconds < 10){
					secondStr = '0'+secondStr;
				}
			retStr += secondStr;
			break;
			default:
				retStr += fmt.charAt(index);
			break;
		} //end switch
	}
	
	return retStr;
	

},
	debug:function(msg){
	
		alert("Error: "+msg);
		
	},
	preventInput:function(){
	
			var dims = this.findViewportDims();
		
			 var  div = '<div id="preventInputScreen" onclick="return false;"  style="z-index:5001; position:absolute; top:0; left:0; width:'+dims[0]+';height:'+dims[1]+'"><img src="common/_images/d.gif" width="100%" height="100%"/></div>';
			if(!$('preventInputScreen')){
				new Insertion.After($('wrap'),div);
			}
	
	},
	enableInput:function(){
		Element.remove($('preventInputScreen'));
	}  
}
// lockout
function handleLock(id,tog,comingFrom) {
	var setToState = tog;
	var bIsLockedNow = tog=='lock'?false:true;
	pop('Lockout.go?id=' + id + '&locked=' + bIsLockedNow + '&comingFrom=' + comingFrom,'dialog');
}




/* 
this is clunky - there are more efficent ways to check for forms (zF. 2006-04-14)
*/
function redirect(sURL,oClicked,bSkipConfirm) {
						// 2nd arg is required for forms 
	var sWarning = 'Are you sure you want to navigate away from this page?\n\Unsaved changes will be lost.\n\n\+ Click \'OK\' to continue and close this window\n\n\+ Click \'CANCEL\' to stay on the current page.'
	var sActionURL = sURL;
	var sFormName = oClicked ? oClicked.form.name:'';
	
	if (window.opener) {
		bWinDecision = bSkipConfirm ? true:confirm(sWarning);

		if (bWinDecision) {
			if (sFormName != '') {
				//this needs to load results in a parent window
				oClicked.form.action = sActionURL;
				oClicked.form.submit();
			} else {
				window.opener.focus();
				window.opener.location = sActionURL;
				closeWindow();
			}
		} else {
			return;
		}
	}
	else {
		if (sFormName != '') {
			return true;
		} else {
			window.location = sActionURL;
		}
	}
}


//
//show & hide element (for element w/ different open and close links)
//
function show(sElemId) {
	// placement of onclicked link
    var oTriggerLink = document.getElementById(sElemId + 'Link');
	var aTriggerPos = findPosition(oTriggerLink); //returns x=[0] and y=[1]
	var nTriggerX = aTriggerPos[0]; var nTriggerY = aTriggerPos[1];
	
	// placement of element to show
	var oShowElem = document.getElementById(sElemId);
	var aShowElemPos = findPosition(oShowElem);
	var nShowElemX = aShowElemPos[0]; var nShowElemY = aShowElemPos[1];

	// modify properties
	oShowElem.style.left = (nTriggerX - nShowElemX) + 20 + 'px';
	oShowElem.style.top = (nTriggerY - nShowElemY) + 20 + 'px';
    oShowElem.style.visibility = 'visible';
}

function hide(sElemId) {
	var oHideElem = document.getElementById(sElemId);

	oHideElem.style.visibility = 'hidden';
	oHideElem.style.left = 0;
	oHideElem.style.top = 0;	
}

//
//toggle element (for elements w/ same open and close link)
//
function toggle(sElemId) {
	var oToggleElem = document.getElementById(sElemId);
	var oToggleElemVis = oToggleElem.style.visibility;

	oToggleElem.style.visibility = oToggleElemVis != 'visible' ? 'visible':'hidden';
}

//
// opens system print dialog
// removes selectFix iframe during printing so as to not trip up postscript printing.
function printPage() {
			
	var savedHeight = "350px";
	var selectFix = document.getElementById('selectFix');
	if(selectFix) {
		savedHeight = selectFix.style.height;
		selectFix.style.height = "0";
	}
	window.print();
	if(selectFix) {
		selectFix.style.height = savedHeight;
	}	
}


//
// ***** computational functions *************************
//


function stripNumberInput(string) {
    for (var i=0, output='', valid="1234567890"; i<string.length; i++)
       if (valid.indexOf(string.charAt(i)) != -1)
          output += string.charAt(i)
    return output;
}

//find absolute position of an element
function findPosition(oElem) {
	if (oElem.offsetParent) {	
		for (var posX = 0, posY = 0; oElem.offsetParent; oElem = oElem.offsetParent) {
			posX += oElem.offsetLeft;
			posY += oElem.offsetTop;
    	}
		return [posX, posY];
	} 
	else {
		return [oElem.x, oElem.y];
  	}
}

function findScreenSize() {
	var x; var y;
	var x = window.screen.width;
	var y = window.screen.height;
	return [x, y];	
}



//find dimensions of the viewable browser area minus chrome
function findViewportDims() {
	var x; var y;
		// non-explorer
	if (self.innerHeight) {
		x = self.innerWidth;
		y = self.innerHeight;
	}
		// explorer 6 strict
	else if (document.documentElement && document.documentElement.clientHeight)	{
		x = document.documentElement.clientWidth;
		y = document.documentElement.clientHeight;
	}
		// other explorers
	else if (document.body) {
		x = document.body.clientWidth;
		y = document.body.clientHeight;
	}
	//viewX = x;
	//viewY = y;
	return [x, y];
}


//prevent typing of non numeric chars
//does not prevent paste though,
//use in conjunction with clean onblur to make
//sure only numbers entered
function checkChars(e){
		var ie  = navigator.appName.indexOf('Microsoft Internet Explorer') != -1;
		 var key = ie?event.keyCode:e?e.keyCode:event.keyCode;
	
		 if((key < 48 || key > 57) &&  key!=8){ //only nueric values allowed
		 	return false;
		 }
		return true;

}
//make sure only numeric values have been entered into the field
function clean(el){

	el.value = onlyNumbers(el.value);
}
//strips non numeric chars from the string
function onlyNumbers(val){
	var dirty = false;
	for( var i = 0; i < val.length; i++){
		dirty = false;
		if( isNaN(val.charAt(i)) ){
		
			val = val.substring(0,i) + val.substring(i + 1,val.length) ;
			dirty = true;
		}
	}
	//as the string shortens there is a potential that
	//some invalid chars can sneak through. This makes sure we got them all
	if( dirty ){ 
		val = onlyNumbers(val);
	}
	return val;
}
function formatPrice(price)
{
	var indexOfPeriod = price.indexOf(".");
	if (indexOfPeriod >= 0)
	{
		price = price.substring(0, indexOfPeriod);
	}
	var indexOfComma = price.indexOf(",");
	if (indexOfComma >= 0)
	{
		price = price.substring(0, indexOfComma) + price.substring( (indexOfComma + 1), price.length );
	}
	var indexOfDollar = price.indexOf("$");
	if (indexOfDollar >= 0)
	{
		price = price.substring( (indexOfDollar + 1), price.length );
	}
	return price;
}
function stampURL(srchStr) {
	return (srchStr += '&now='+(new Date()).getTime());
}

function validatePrice(val) {
	var value = formatPrice(val);
	var check;	 
	
	if( value != ''){
		if( value=='$' || isNaN(value) || value < 0 || value > 999999) check=true;
		if(check) {
			var msg = '\"'+value+'\" is not a valid price value.';
			msg += '\nPlease try again.'			
			alert(msg);
			return false;
		}
		return parseInt(value);	
	}
	return false;
}

// Copyright (C) 2005 Ilya S. Lyubinskiy. All rights reserved.
// Technical support: http://www.php-development.ru/


// USAGE
//
// function popup_show(id, drag_id, exit_id, position, x, y, position_id)
//
// id          - id of a popup window;
// drag_id     - id of an element within popup window intended for dragging it
// exit_id     - id of an element within popup window intended for hiding it
// position    - positioning type:
//               "screen-corner", "screen-center"
//               "mouse-corner" , "mouse-center"
//               "element-right", "element-bottom"
// x, y        - offset
// position_id - for the last two types of positioning popup window will be
//               positioned relative to this element
//Modified by John Caron
//drag_id is optional, enter '' or null to eliminate window dragging.

// ----- Variables -------------------------------------------------------------


function Popup(){

	var popup_dragging = false;
	var popup_target;
	var popup_mouseX;
    var popup_mouseY;
	var popup_mouseposX;
	var popup_mouseposY;
	var popup_oldfunction;
	var popup_height;
	var animate = false;
	var element;
	var position_element;
	var selects = null;
	
	var ie = navigator.appName == "Microsoft Internet Explorer";
	this.popup_display = popup_display;
	this.popup_mousedown = popup_mousedown;
	this.popup_mousemove = popup_mousemove;
	this.popup_mouseup =popup_mouseup;
	this.popup_exit = popup_exit;
	this.popup_show = popup_show;
	this.popup_mousepos = popup_mousepos;
	this.refresh = refresh;
	
	function popup_display(x){
	
		 var win = window.open();
		  for (var i in x) win.document.write(i+' = '+x[i]+'<br>');
	
	}
	
	function popup_mousedown(e){
	 	var ie = navigator.appName == "Microsoft Internet Explorer";

		  if ( ie && window.event.button != 1) return;
		  if (!ie && e.button            != 0) return;
		
		 popup_dragging = true;
		 popup_target   = this['target'];
		 popup_mouseX   = ie ? window.event.clientX : e.clientX;
		 popup_mouseY   = ie ? window.event.clientY : e.clientY;
	
		  if (ie)
		       popup_oldfunction      = document.onselectstart;
		  else popup_oldfunction      = document.onmousedown;
		
		  if (ie)
		       document.onselectstart = new Function("return false;");
		  else document.onmousedown   = new Function("return false;");
	
	}
	
	
	function popup_mousemove(e){
	
			 //	alert(this.popup_dragging);
			 if (!popup_dragging) return;
			
			  var ie      = navigator.appName == "Microsoft Internet Explorer";
			//  var element = document.getElementById(this.popup_target);
			
			  var mouseX = ie ? window.event.clientX : e.clientX;
			  var mouseY = ie ? window.event.clientY : e.clientY;
			
			var maxHeight = document.documentElement.clientHeight + document.documentElement.scrollTop;
			var maxWidth = document.documentElement.clientWidth;
			var newX =  (element.offsetLeft+mouseX-popup_mouseX);
			var newY = (element.offsetTop +mouseY-popup_mouseY);
			
			element.style.left = newX+'px';
			element.style.top  = newY+'px';
			
			element.left = newX+'px';
			element.top = newY+'px';
			
		    popup_mouseX = ie ? window.event.clientX : e.clientX;
			popup_mouseY = ie ? window.event.clientY : e.clientY;			
	}
	
	function popup_mouseup(e){
			 if (!popup_dragging) return;
		 		popup_dragging = false;
		
		  var ie      = navigator.appName == "Microsoft Internet Explorer";
		  var element = document.getElementById(popup_target);
		
		  if (ie)
		       document.onselectstart = popup_oldfunction;
		  else document.onmousedown   = popup_oldfunction;			
	}
	
	function popup_exit(e){
			if (!element) { return; }
			 var ie      =Util.isIE();
		  //	 var element = document.getElementById(this.popup_target);
		
			if(exit_element){
		  		popup_mouseup(e);
		  	}
		
		  if(animate){
			  	var myEffect = new fx.Height(element , {duration: 500, onComplete: function()
				  {
				    element.style.visibility = 'hidden';
			 		 element.style.display    = 'none';
				  }
				});
				myEffect.custom(popup_height,0);
				myEffect.toggle();
			}else{
					 element.style.visibility = 'hidden';
			 		 element.style.display    = 'none';
			}
			if(drag_element){
				var selects = document.getElementsByTagName('SELECT');
					for(var i=0; i< selects.length; i++){
					selects[i].style.visibility='visible';
					}
			}
		
			if(position_element){
				if(Util.isIE()){
					 position_element.detachEvent('onclick',preventNewWindow);
				}else{
					 position_element.removeEventListener('click',preventNewWindow, false);
				}
			}
	}
	
	function preventNewWindow(){
		return false;
	}
	
	function popup_show(id, drag_id, exit_id, position, height, anim, x, y, position_id) {
	
	    element = document.getElementById(id);
	    drag_element = document.getElementById(drag_id);
	    if (Util.isArray(exit_id)) {
	        exit_element = new Array();
	        for (var i = 0; i < exit_id.length; i++) {
	
	            exit_element[i] = document.getElementById(exit_id[i]);
	        }
	
	    } else {
	        exit_element = document.getElementById(exit_id);
	    }
	    animate = anim;
	
	    if (element.style.visibility == "visible" && element.style.display == "block") {
	
	        popup_exit();
	
	    }
	
	
	    element.style.position = "absolute";
	    element.style.visibility = "visible";
	    element.style.display = "block";
	    popup_height = height;
	
	    if (animate) {
	
	        var myEffect = new fx.Opacity(element, {
	            duration: 500,
	            onComplete: function() {}
	        });
	
	        myEffect.toggle();
	    }
	
	    if (position == "screen-corner")
	    {
	        element.style.left = (document.documentElement.scrollLeft + x) + 'px';
	        element.style.top = (document.documentElement.scrollTop + y) + 'px';
	    }
	
	    if (position == "screen-center")
	    {
	
	        element.style.left = (document.body.scrollLeft + (document.body.clientWidth - element.clientWidth) / 2 + x) + 'px';
	        element.style.top = (document.body.scrollTop + (document.body.clientHeight - element.clientHeight) / 2 + y) + 'px';
	
	
	    }
	    
	    if (position == "top-center")
	    {
	
	        element.style.left = (document.body.scrollLeft + (document.body.clientWidth - element.clientWidth) / 2 + x) + 'px';
	        element.style.top = (document.body.scrollTop + (document.body.clientHeight - element.clientHeight) / 2) + 'px';
	
	
	    }
	
	    if (position == "mouse-corner")
	    {
	        element.left = (document.documentElement.scrollLeft + popup_mouseposX + x) + 'px';
	        element.top = (document.documentElement.scrollTop + popup_mouseposY + y) + 'px';
	    }
	
	    if (position == "mouse-center")
	    {
	        element.left = (document.documentElement.scrollLeft + popup_mouseposX - element.clientWidth / 2 + x) + 'px';
	        element.top = (document.documentElement.scrollTop + popup_mouseposY - element.clientHeight / 2 + y) + 'px';
	    }
	
	    if (position == "element-right" || position == "element-bottom")
	    {
	        position_element = document.getElementById(position_id);
	
	        if (Util.isIE()) {
	            //prevent duplicate windows
	            position_element.attachEvent('onclick', preventNewWindow);
	        } else {
	            position_element.addEventListener('click', preventNewWindow, false);
	        }
	
	        for (var p = position_element; p; p = p.offsetParent)
	        if (p.style.position != 'absolute')
	        {
	            x += p.offsetLeft;
	            y += p.offsetTop;
	        }
	
	        if (position == "element-right") x += position_element.clientWidth;
	        if (position == "element-bottom") y += position_element.clientHeight;
	
	        if (x + element.clientWidth > document.body.clientWidth) {
	
	            x = x - ((x + element.clientWidth) - document.body.clientWidth) - 15;
	
	        }
	
	
	        if (element.style.left) {
	
	            element.style.left = x + 'px';
	            element.style.top = (y - 7) + 'px';
	
	        }
	
	    }
	
	    if (drag_element) {
	        drag_element['target'] = id;
	        popup_target = id;
	        drag_element.onmousedown = this.popup_mousedown;
	    } else {
	
	        popup_target = id;
	    }
	
	    if (exit_element) {
	
	        if (Util.isArray(exit_element)) {
	
	            for (var i = 0; i < exit_element.length; i++) {
	
	                if (ie) {
	                    exit_element[i].attachEvent('onclick', popup_exit);
	                } else {
	                    exit_element[i].addEventListener('click', popup_exit, false);
	                }
	            }
	        } else {
	
	            if (ie) {
	                exit_element.attachEvent('onclick', popup_exit);
	            } else {
	                exit_element.addEventListener('click', popup_exit, false);
	            }
	        }
	    }
	    if (element && ie){
	    	clip(element);	    
    	}
	}
	
	function popup_mousepos(e){
	
		var ie = navigator.appName == "Microsoft Internet Explorer";

 		popup_mouseposX = ie ? window.event.clientX : e.clientX;
  		popup_mouseposY = ie ? window.event.clientY : e.clientY;
  		
	}
	function refresh(){}
	function clip(el){
		var bgiframe = document.createElement("iframe");
		
		bgiframe.src = "javascript:false";		
		bgiframe.style.display = "block";
		bgiframe.style.position = "absolute";
		bgiframe.style.zIndex = "-1";
		bgiframe.style.filter = "filter:Alpha(Opacity=\'0\')";
		bgiframe.style.top = "0"; 
		bgiframe.style.left = "0";
		bgiframe.style.bottom = "0";
		bgiframe.style.right = "0";
		bgiframe.style.width = "100%";
		bgiframe.style.height = "100%";
		bgiframe.className = "bgiframe";
		bgiframe.frameBorder = "0";
		bgiframe.tabIndex = "-1";
		
		el.insertBefore(bgiframe, el.firstChild);
	}
	
	if (navigator.appName == "Microsoft Internet Explorer")
					    		 document.attachEvent('onmousedown', popup_mousepos);
	else document.addEventListener('mousedown', popup_mousepos, false);
					
	if (navigator.appName == "Microsoft Internet Explorer")
					    		 document.attachEvent('onmousemove',popup_mousemove);
	else document.addEventListener('mousemove',popup_mousemove, false);
					
	if (navigator.appName == "Microsoft Internet Explorer")
					   		  document.attachEvent('onmouseup', popup_mouseup);
	else document.addEventListener('mouseup', popup_mouseup, false);
}


//AjaxLoader class
function AjaxLoader(msg, pth, params){

	var p;
	var div;
	var path = pth;
	var parameters = params;
	var element;
	var message = msg;
	var success;
	var failure;
	var insertion;
	var complete;
	var showAtElement;
	var working = false;
	var exception;
	
	this.showWindow = showWindow;
	this.hideWindow = hideWindow;
	this.setParameters = setParameters;
	this.setElement = setElement;
	this.performAction = performAction;
	this.setPath = setPath;
	this.setOnSuccess =  setOnSuccess;
	this.setOnFailure =  setOnFailure;
	this.setMessage =  setMessage;
	this.setOnComplete = setOnComplete;
	this.setInsertion = setInsertion;
	this.setShowAt = setShowAt;
	this.getWorking = getWorking;
	this.reset = reset;
	this.setOnException = setOnException;
	
	function showWindow(){  //create loading window if does not exist, fill it with message, then show it.
	
	        div = '<div id="loadingDiv"  style="z-index:1050; border:1px solid black;background-color:white; padding:10px; width:50px; height:20px; position:absolute; display:none;text-align:center; font-weight:bold;"><img src="common/_images/icons/loading.gif"/><br/>'+message+'</div>';
			if(!$('loadingDiv')){
						new Insertion.After($('header'),div);
			}else{
				$('loadingDiv').innerHTML='<img src="common/_images/icons/loading.gif"/><br/>'+message;
			}
		 p = new Popup();
		 if(!showAtElement){
			  p.popup_show('loadingDiv', '', '', 'screen-center',100,false,0,0);
		 }else{
		 
			 p.popup_show('loadingDiv', '', '', 'element-bottom',100,false,0,10,showAtElement.id);
		}
	}
	function hideWindow(){
		p.popup_exit();
	}
	function getWorking(){
		return working;
	}
	function setParameters(params){
		parameters = params;
	}
	function reset(){
		p = null;
		div =null;
		path = null;
		parameters = '';
		element = '';
		message = null;
		success = null;
		failure = null;
		insertion = null;
		complete = null;
		showAtElement = null;
		working = false;
		exception = null;
	}
	function setShowAt(ele){

		showAtElement = $(ele);
	}
	function setPath(pth){
	
		path = pth;
	}
	function setElement(ele){
	
		element = ele;
	}

	function setMessage(msg){
	
		message = msg;
	}
	function setOnComplete(fun){
	
		complete = fun;
	}
	
	//set function to perform on success
	function setOnSuccess(fun){
	
	 	success = fun;

	}
	//set function to perform on failure
	function setOnFailure(fun){
	
	 	failure = fun;

	}
	function setOnException(fun){
		exception  = fun;
	}
	function setInsertion(ins){
	
		insertion = ins;
	}
	function performAction(){

		if(message != null && message != ''){
			this.showWindow();  //display "loading window"
		}
		
		
		if(path && !working){
			working = true; //request in progress
			
			if(insertion){
				if(!element){
					alert('Element must be set for ajax updater call.');
					if(message != null && message != '')
						hideWindow();
					return;
				}
			
				new Ajax.Updater(
					element,
					path,
					{
						method:'post',
						parameters: stampURL(parameters),
						insertion: insertion,
						onSuccess: function(r,j) {
												
							if(success){
								success(r,j);
							}
							
						},
						onComplete: function(r,j){
						
							if(message != null && message != ''){
								hideWindow();
							}
							
							if(complete){
								complete(r,j);
							}
							reset(); //reset the state of the object
							
						},
						onFailure: function(){
				
							if(failure){
								failure(r,j);
							}
							
						}
					}
				);
			
			}else{
		
				new Ajax.Request(
					path,
					{
						method: 'post',
						parameters: stampURL(parameters),
						onSuccess: function(r,j) {						
					
							if($(element)){
												
									$(element).innerHTML =r.responseText;
							}
							if(success){
								success(r,j);
							
							}
						
						},
						onComplete: function(r,j){
						
							if(message != null && message != ''){
								hideWindow();
							}
							if(complete){
								complete(r,j);
							}
							reset();
						
						},
						onFailure: function(r,j){
					
							if(failure){
								failure(r,j);
							}
						},
						onException:function(r,e){
							if(exception){
							
								exception(r,e);
							}
						}
					}
				);
			}
		}else{
		
			if(!path){
				alert('Error:Ajax path has not been set.');
				hideWindow();
			}
		}
	}
} //end ajax loader class



function confirmReprice(price, invId, ele, ok){
	if(validatePrice(price) !== false) {
		document.getElementById('listPriceChanged').value = true;
		var ajax = new AjaxLoader();
		
		function handleRepriceConfirmationResponse(resp){
			var confirm = eval('('+resp.getResponseHeader('confirm')+')');  //confirm is true then show confirm box
			if(confirm){
				ele.onfocus =function(){ele.blur();}  //disables element but allows styles
				Element.addClassName(ele,'red');
				var  div = '<div id="confirmBox"  style="z-index:1050;left:-100px; top:-100px; padding:10px;  height:20px; position:absolute; display:none;"></div>';
				if($('confirmBox')){ Element.remove($('confirmBox'));}
				new Insertion.After($('footer'),div);		
				
				$('confirmBox').innerHTML = resp.responseText;
			
				$('repriceConfirmCancel').onclick=function(){ Element.removeClassName(ele,'red');ele.onfocus='';return false;}
		
				if(Util.isIE()){
					$('repriceConfirmOk').attachEvent('onclick', ok);	
				  	$('repriceConfirmOk').attachEvent('onclick', function(){Element.removeClassName(ele,'red');ele.onfocus='';});				
				 }else{
					  $('repriceConfirmOk').addEventListener('click', ok, false);			
				 	  $('repriceConfirmOk').addEventListener('click', function(){Element.removeClassName(ele,'red');ele.onfocus='';}, false);			
				 }		
				
				 var  p = new Popup();			
				 p.popup_show('confirmBox', '', ['repriceConfirmCancel','repriceConfirmOk'], 'element-bottom',100,false,-30,-7, ele.id);
			}else{
				if($('confirmBox')){ Element.remove($('confirmBox'));}
				ok(false);
			}
		}
		
		function handleRepriceConfirmationException(resp,e){
			alert('A server error has occured.');
		}
		
		ajax.setPath('/IMT/RepriceConfirm.go');
		ajax.setParameters('&price='+price+'&inventoryId='+invId);
		ajax.setOnSuccess(handleRepriceConfirmationResponse);
		ajax.setOnException(handleRepriceConfirmationException);
		ajax.performAction();
	}
}

 function fixTags(val){
 
 	var retVal = val.replace(/&amp;/gi,'&');
 	retVal = retVal.replace(/&lt;/gi,'<');
 	retVal = retVal.replace(/&gt;/gi,'>');
 	return retVal;
 }
function unescapeHTML(val) {
    var div = document.createElement('div');
    div.innerHTML = val;
     return fixTags(div.innerHTML);
  }
  
var iframeAutoHeightCommand = function() {};
iframeAutoHeightCommand.prototype = {
    init: function() {
        this.iframes = [];
        if (window.attachEvent) {
            window.attachEvent('onload', this.onload);
        } else {
            window.addEventListener('load', this.onload, false);
        }
    },
    tearDown: function() {
        if (window.detachEvent) {
            window.detachEvent('onload', this.onload);
        } else {
            window.removeEventListener('load', this.onload, false);
        }
        this.iframes = undefined;
        this.instance = undefined;
    },
    get_instance: function() {
        if ( !! !this.instance) {
            this.instance = this;
            this.init();
        }

        return this.instance;
    },
    push_iframe: function(iframe, options) {
    	if (! !!iframeAutoHeightCommand.prototype.loaded) {
    		return this.iframes.push([iframe, (iframe.name) ? iframe.name: iframe, options]);
    	} else {
    		this.autoSetHeight(iframe, (iframe.name) ? iframe.name: iframe, options);
    	}
    },
    onload: function() {
        var instance = iframeAutoHeightCommand.prototype.get_instance();
        for (var i = 0, l = instance.iframes.length; i < l; i++) {
            instance.autoSetHeight(instance.iframes[i][0], instance.iframes[i][1], instance.iframes[i][2]);
            instance.iframes[i] = undefined;
        };
        instance.tearDown();
        iframeAutoHeightCommand.prototype.loaded = true;
    },
    autoSetHeight: function(el, name, options) {
        var iframe, iframeEl, iframeBody, height;
        if (parent.document.frames && parent.document.frames[name]) {
            iframe = parent.document.frames[name];
            iframeEl = (el.style) ? el: document.getElementById(name);
            var iframeBody = iframe.document.body;
            height = iframe.document.body.scrollHeight;
        } else {
            iframeEl = el;
            height = el.contentDocument.body.scrollHeight;
        }
        if (options) {
            if (options.min && options.min > height) {
                iframeEl.style.height = options.min + 'px';
                return;
            }
        }
        iframeEl.style.height = height + 'px';
        return;
    }
};

function iframeAutoHeight(el, options) {
    iframeAutoHeightCommand.prototype.get_instance().push_iframe(el, options);
}

