var hasLimitedPercentage = false;
var hasLimitedWarrantySystemsCovered = false;
var hasLimitedWarrantyDuration = false;

window.onload = function() {
	hasLimitedPercentage = document.getElementById("limitedPercentage") && document.getElementById("limitedLaborPercentage") && document.getElementById("limitedPartsPercentage");
	hasLimitedWarrantySystemsCovered = document.getElementById("systemsCovered") && document.getElementById("servicecontractText");
	hasLimitedWarrantyDuration = document.getElementById("duration") && document.getElementById("vehicleExchangeDuration") && document.getElementById("vehiclePowerTrainDuration");
	
	document.getElementById("warrantyCheckbox").onclick = clickWarrantyCheckbox;
	document.getElementById("nowarrantyCheckbox").onclick = clickNowarrantyCheckbox;
	document.getElementById("fullCheckbox").onclick = clickFullCheckbox;
	document.getElementById("limitedCheckbox").onclick = clickLimitedCheckbox;
	document.getElementById("servicecontractCheckbox").onclick = clickServicecontractCheckbox;
	if (hasLimitedPercentage) {
		document.getElementById("percentlaborCheckbox").onclick = clickPercentlaborCheckbox;
		document.getElementById("percentpartsCheckbox").onclick = clickPercentpartsCheckbox;
	}
	if (hasLimitedWarrantySystemsCovered) {
		document.getElementById("systemscoveredCheckbox").onclick = clickSystemscoveredCheckbox;
	}
	if (hasLimitedWarrantyDuration) {
		document.getElementById("durationCheckbox").onclick = clickDurationCheckbox;
	}
};
function clickWarrantyCheckbox() {
	document.getElementById("warrantyTypes").style.display = "block";
	document.getElementById("nowarrantyCheckmark").style.display = "none";
	document.getElementById("warrantyCheckmark").style.display = "block";
}
function clickNowarrantyCheckbox() {
	document.getElementById("warrantyTypes").style.display = "none";
	document.getElementById("warrantyCheckmark").style.display = "none";
	document.getElementById("limitedCheckmark").style.display = "none";
	document.getElementById("fullCheckmark").style.display = "none";
	document.getElementById("nowarrantyCheckmark").style.display = "block";	
	if (hasLimitedPercentage) {
		document.getElementById("limitedPercentage").style.display = "none";
		document.getElementById("percentlaborCheckbox").checked = false;
		document.getElementById("percentpartsCheckbox").checked = false;
	}
	if (hasLimitedWarrantySystemsCovered) {
		document.getElementById("systemsCovered").style.display = "none";
		document.getElementById("systemscoveredCheckbox").checked = false;
	}
	if (hasLimitedWarrantyDuration) {
		document.getElementById("duration").style.display = "none";
		document.getElementById("durationCheckbox").checked = false;
	}
}
function clickFullCheckbox() {
	document.getElementById("limitedCheckmark").style.display = "none";
	document.getElementById("fullCheckmark").style.display = "block";
	if (hasLimitedPercentage) {
		document.getElementById("limitedPercentage").style.display = "none";
		document.getElementById('limitedLaborPercentage').style.display = "none";
		document.getElementById('limitedPartsPercentage').style.display = "none";
		document.getElementById("percentlaborCheckbox").checked = false;
		document.getElementById("percentpartsCheckbox").checked = false;
	}
	if (hasLimitedWarrantySystemsCovered) {
		document.getElementById("systemsCovered").style.display = "none";
		document.getElementById('servicecontractText').style.display = "none";
		document.getElementById("systemscoveredCheckbox").checked = false;
	}
	if (hasLimitedWarrantyDuration) {
		document.getElementById("duration").style.display = "none";
		document.getElementById('vehicleExchangeDuration').style.display = "none";
		document.getElementById('vehiclePowerTrainDuration').style.display = "none";
		document.getElementById("durationCheckbox").checked = false;
	}
}
function clickLimitedCheckbox() {
	document.getElementById("fullCheckmark").style.display = "none";
	document.getElementById("limitedCheckmark").style.display = "block";
	if (hasLimitedPercentage) {
		document.getElementById("limitedPercentage").style.display = "block";
	}
	if (hasLimitedWarrantySystemsCovered) {
		document.getElementById("systemsCovered").style.display = "block";
	}
	if (hasLimitedWarrantyDuration) {
		document.getElementById("duration").style.display = "block";
	}
	
}
function clickServicecontractCheckbox() {
	if(this.checked) {
		document.getElementById("servicecontractCheckmark").style.display = "block";
	} else { 
		document.getElementById("servicecontractCheckmark").style.display = "none";
	}
}
function hideCheckmarks() {
	document.getElementById("warrantyCheckmark").style.display = "none";
	document.getElementById("nowarrantyCheckmark").style.display = "none";
	document.getElementById("fullCheckmark").style.display = "none";
	document.getElementById("limitedCheckmark").style.display = "none";
	document.getElementById("servicecontractCheckmark").style.display = "none";
}
function clickPercentlaborCheckbox() {
	document.getElementById('limitedLaborPercentage').style.display = this.checked ? "block" : "none";
}
function clickPercentpartsCheckbox() {
	document.getElementById('limitedPartsPercentage').style.display = this.checked ? "block" : "none";
}
function clickSystemscoveredCheckbox() {
	document.getElementById('servicecontractText').style.display = this.checked ? "block" : "none";
}
function clickDurationCheckbox() {
	document.getElementById('vehicleExchangeDuration').style.display = this.checked ? "block" : "none";
	document.getElementById('vehiclePowerTrainDuration').style.display = this.checked ? "block" : "none";
}