
var appraisalsAndActions = '';
	// collects selection values
function storeDecisionValues(appraisalId,actionId) {
	appraisalsAndActions += appraisalId + '&' + actionId + '|';
}

function submitDecisionValues() {
	document.getElementById('storedActionPairs').value = appraisalsAndActions;
	return true;
}

function confirmSold(tapid) {
    var url= "TradeManagerSoldAction.go?pageName=bullpen&appraisalId=" + tapid;
    var result = window.confirm("Press OK to confirm this vehicle is Sold.\nPress Cancel to cancel this action.");
    if(result) {
        document.location.href = url;
    } else {
        return;
    }
}
try { if(Prototype) {
    Event.observe(window, 'load', function() {
        var f = $('TradeManagerRedistributionForm');
        if( f ) // f will be null if there are no vehicles to be displayed
        {
            var vins = f.getInputs('hidden', 'vin');
            for(var i=0; i<vins.length; i++) {
                var _el = vins[i];
                var _row = _el.up(1);
                if(_row.tagName == 'TR') {
                    _row.title = 'VIN: ' + _el.value;
                    Event.observe(_row, 'mouseover', addHilite);
                    Event.observe(_row, 'mouseout', removeHilite);
                }
            }
        }
    });
} } catch(e) {}

// 
// TRADE MANAGER scripts 

var addHilite = function(e) {
    var el = Event.element(e);
	if(el.tagName != 'TR') {
        el = Event.findElement(e, 'TR');
	} 
	Element.addClassName(el, 'hilite');
}
var removeHilite = function(e) {
    var el = Event.element(e);
	if(el.tagName != 'TR') {
        el = Event.findElement(e, 'TR');
	} 
	Element.removeClassName(el, 'hilite');
}



// 
// WAITING APPRAISALS scripts

function deleteWaitingAppraisal(id, group) {
	var obj = document.getElementById('row_'+id);
	obj.style.backgroundColor = '#EEE';
	var msg = 'Click OK to permanently DELETE this appraisal?';
	var ok = confirm(msg);
	if(ok) {
		var xhr = new Ajax.Request(
			'/IMT/DeleteAppraisalAction.go', 
			{
				method: 'post', 
				parameters: 'appraisalId='+id, 
				onComplete: function(xhr) {
					Element.remove(obj);
					// updateCounts
					var el = document.getElementById('waitingAppraisalsCount');
					var e2 = document.getElementById(group + 'Count');					
					el.innerHTML = parseInt(el.innerHTML) - 1;	
					e2.innerHTML = parseInt(e2.innerHTML) - 1;	
					// grouping count is 0
					if (e2.innerHTML == 0) {
						var e3 = document.getElementById(group + 'Grouping');
						if (el.innerHTML == 0) {
							// tab count is also 0
							e3.innerHTML = "<p>There are no matching vehicles.</p>";
						} else {
							e3.innerHTML = "";
						}
					}
				}
			});		
	} else {
		obj.style.backgroundColor = '';
	}
}