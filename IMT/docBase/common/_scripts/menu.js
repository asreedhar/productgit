activateMenu = function() {
	var showIfNavLength = 5;
    var navRoot;
    navRoot = document.getElementById("dropnav");
    if (!navRoot) return;
    if (document.all && document.getElementById) {
        for (i = 0; i < navRoot.childNodes.length; i++) {
            node = navRoot.childNodes[i];
            if (node.nodeName == "LI") {
                node.onmouseover = function() {
                    this.className += " over";
                    /* fix for IE - prevent dropdowns showing through */
                    var list = this.getElementsByTagName('ul');
                    var selectFix = document.getElementById('selectFix');
                    if (list && list[0] && list[0].childNodes && list[0].childNodes.length > showIfNavLength && selectFix) {
                        selectFix.style.display = '';
                        selectFix.className += " "+list[0].className;
                    }
                };
                node.onmouseout = function() {
                    this.className = this.className.replace(" over", "");
                    var list = this.getElementsByTagName('ul');
                    var selectFix = document.getElementById('selectFix');
                    if (list && list[0] && list[0].childNodes && list[0].childNodes.length > showIfNavLength && selectFix) {
                        selectFix.style.display = 'none';
                    	selectFix.className = selectFix.className.replace(" "+list[0].className,"");
                    }
                };
            }
        }
    };
};
if (window.attachEvent) {
    window.attachEvent("onload", activateMenu);
} else if (window.addEventListener) {
    window.addEventListener("load", activateMenu, false);
} else {
    window.onload = activateMenu;
}
