
function saveOffer(showMessage, onTM) {
	var offer = formatOffer($('offer').value);
	if ( !validateOffer( offer ) ){ return false; }
	var pars = Form.serialize($('appraisalForm'));
	var myAjax = new Ajax.Updater( 
		'', 'AppraisalFormSaveAction.go', 
		{ method: 'post', 
		parameters: pars,
		onComplete: function() {
			$('offer').value = '$' + addCommas(offer);
			if ( onTM ) {
				//when coming from MAX, onTM == 1 (set by MAX), but opener.document.getElementById is null.
				if( opener.document.getElementById('offer') != null ) {
					opener.document.getElementById('offer').innerHTML = '$' + addCommas(offer);
				}
				if( opener.document.getElementById('offerEdit') != null ) {
					opener.document.getElementById('offerEdit').value = offer;
				}
				if( opener.document.getElementById('offerEditInput') != null ) {
					opener.document.getElementById('offerEditInput').value = offer;				
				}
				if( opener.document.getElementById('useAppraisalValue') != null ) {
					opener.document.getElementById('useAppraisalValue').value = 0;
				}
			} else {
				if (opener.document.getElementById('offerEditInput') != null)
				{
					opener.document.getElementById('offerEditInput').value = '$' + addCommas(offer);
				}
			}
			
			if (opener.document.getElementById('custofferpricetf') != null)
			{
				opener.document.getElementById('custofferpricetf').value = '$' + addCommas(offer);
				opener.coPrice=addCommas(offer);
				opener.saveAppraisalAndForward();
			}
			
			if ( showMessage == 0) {
				$('offer').value = formatOffer($('offer').value);
				var form = document.getElementById('appraisalForm');
				form.submit();
			} else {
				window.close();
			}
		}
	});
}

function validateOffer( value ) {
	var check;	 
	if( value=='$' || isNaN(value) || value == '' || value < 0) check=true;
	if(check) {
		var msg = '\"'+value+'\" is not a valid offer.';
		msg += '\nPlease try again.'
		alert(msg);
		return false;
	}
	return true;
}

function formatOffer(offer)
{
	var indexOfPeriod = offer.indexOf(".");
	if (indexOfPeriod >= 0)
	{
		offer = offer.substring(0, indexOfPeriod);
	}
	var indexOfComma = offer.indexOf(",");
	if (indexOfComma >= 0)
	{
		offer = offer.substring(0, indexOfComma) + offer.substring( (indexOfComma + 1), offer.length );
	}
	var indexOfComma = offer.indexOf(",");
	if (indexOfComma >= 0)
	{
		offer = offer.substring(0, indexOfComma) + offer.substring( (indexOfComma + 1), offer.length );
	}
	var indexOfComma = offer.indexOf(",");
	if (indexOfComma >= 0)
	{
		offer = offer.substring(0, indexOfComma) + offer.substring( (indexOfComma + 1), offer.length );
	}
	var indexOfDollar = offer.indexOf("$");
	if (indexOfDollar >= 0)
	{
		offer = offer.substring( (indexOfDollar + 1), offer.length );
	}
	return offer;
}


function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

function saveDefaultSettings() {
	var pars = Form.serialize($('appraisalForm'));
	var myAjax = new Ajax.Request( 
		'AppraisalFormSaveDefaultSettingsAction.go', 
		{ method: 'post', 
		parameters: pars,
		onSuccess: function() {
			$('defaultsSetMessage').update('Defaults have been set');
			},
		onFailure: function() {
			$('defaultsSetMessage').update('Error Occured! Defaults have not been set');
		}});
}

function emailPopUp()
{
	jQuery("#emailPopup").show();
  
      
 
}
 
function closePopup()
{        
	jQuery("#emailPopup").hide();
}

function submitEmailandClose()
{
	if(!isEmailComma(jQuery("#emailTo").val())){
		alert("Invalid To Email Address");
		return false;
	}
	if(!(jQuery("#emailReplyTo").val()=="")){
		
		if(!is_email(jQuery("#emailReplyTo").val())){
			alert("Invalid Reply To Email Address");
			return false;
		}
	}
	$('offer').value = formatOffer($('offer').value);

	var params=jQuery("#appraisalForm").serializeArray();
	
	var url="/IMT/PDFEmailAction.go";
	
	jQuery.ajax({
		url:url,
		data:params,
		type:"POST",
		success: function(data , status , jqXhr) {
			
			if(data=="Success"){
				//alert("Email Is Sent");
				window.close();
				
			}
			if(data=="Error"){
				alert("Could Not Send Email");
			}
		},
		error: function( jqXHR, textStatus, errorThrown ){
			alert(textStatus);
		}
		
		
	})
	
		
}


function saveOfferAndDoNothing() {
	var offer = formatOffer($('offer').value);
	if ( !validateOffer( offer ) ){ return false; }
	$('offer').value = formatOffer($('offer').value);
	var pars = Form.serialize($('appraisalForm'));
	var myAjax = new Ajax.Updater( 
		'', 'AppraisalFormSaveAction.go', 
		{ method: 'post', 
		parameters: pars,
		onComplete: function() {
			$('offer').value = '$' + addCommas(offer);
			
			
			if (opener.document.getElementById('custofferpricetf') != null)
			{
				opener.document.getElementById('custofferpricetf').value = '$' + addCommas(offer);
				opener.coPrice=addCommas(offer);
				opener.saveAppraisalAndForward();
			}
			emailPopUp();
			
			
		}
	});
}

function isEmailComma(emails){
	
	var emailArr= new Array();
	if(emails!=null)
	emailArr= emails.split(",");
	
	var i=0;
	var bool=false;
	for(i=0;i<emailArr.length;i++){
		
		if(!is_email(emailArr[i])){
			bool=true;
		}
		
	}
	
	return !bool;
	
	
}


function is_email(email){      
	var emailReg = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	return emailReg.test(email); } 