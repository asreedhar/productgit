//
// BOOKOUT scripts ONLY


////// helper functions
function toggleWarning(which,state) {
	var oWarningBlock = document.getElementById(which);
	if (oWarningBlock) oWarningBlock.style.display = state == 'on' ? 'block':'none';
}

//encodeURIComponent comes from prototype.js
function grabBookFormInfo(sFormId) {

	var oForm = document.getElementById(sFormId);
	var sFormName = oForm.name;
	var sFormURL = oForm.action;

	if (oForm.selectedVehicleKey != null) {
		if (oForm.selectedVehicleKey.value == 0) {
			alert('Please Select A Model');
			return;
		}
	}
	sFormURL += '&fromEstock=true';
	//kelley only
	if (sFormId == ('Form3'||'Temp3')) {
		sFormURL += '&driveTrain='+ encodeURIComponent(oForm.selectedDrivetrainKey.options[oForm.selectedDrivetrainKey.options.selectedIndex].value);
	 	sFormURL += '&engine='+ encodeURIComponent(oForm.selectedEngineKey.options[oForm.selectedEngineKey.options.selectedIndex].value);
	 	sFormURL += '&transmission='+ encodeURIComponent(oForm.selectedTransmissionKey.options[oForm.selectedTransmissionKey.options.selectedIndex].value);
		sFormURL += '&condition='+ encodeURIComponent(oForm.conditionId.options[oForm.conditionId.options.selectedIndex].value);
	}
    sFormURL += '&selectedEquipmentOption=';
	if (oForm.selectedEquipmentOptionKeys != null) {
		if (oForm.selectedEquipmentOptionKeys.type=='hidden') {
		 	sFormURL += encodeURIComponent(oForm.selectedEquipmentOptionKeys.value);
		}
		else {
			//this checks for condition when there is only 1 option
			if (!oForm.selectedEquipmentOptionKeys.length) {
				if (oForm.selectedEquipmentOptionKeys.checked) {
					sFormURL += encodeURIComponent(oForm.selectedEquipmentOptionKeys.value);
				}
			}
			else
			{
				for (i = 0; i < oForm.selectedEquipmentOptionKeys.length; i++) {
					if (oForm.selectedEquipmentOptionKeys[i].checked) {
						sFormURL += encodeURIComponent(oForm.selectedEquipmentOptionKeys[i].value) + '|';
					}
				}
			}
		}
	}
	return sFormURL;
}
function findBookForms() {
	var aBooks = new Array('Temp1','Temp2','Temp3');
	var aActiveBooks = new Array();
	for (i=0; i<aBooks.length; i++) {
		var formId;
		formId = document.getElementById(aBooks[i]);
		if (formId) {
			aActiveBooks[i] = true;
		}
		else {
			aActiveBooks[i] = false;
		}
	}
	return aActiveBooks;
}
function rebook(sBookURL) {
	retrieveURL(sBookURL);
}

//working functions

var optionsOpen = false;

function closeBookOutOptions(formId) {
	togglePrevBookButton('on');
	Element.remove(formId);
	if(document.getElementById('float')){
		Element.remove('float')
	}
	optionsOpen = false;
}
function closeBookOptions() {
	togglePrevBookButton('on');
	Element.remove('float');
	optionsOpen = false;
}
function getBookOptions(sURL,contNum,primaryBookFlag) {
	if (optionsOpen) return;
	rebookButtonStatus();
	toggleAllBookButton('off')
	var nMileage = stripNumberInput($('mileage').innerText);
	var pars = '&inventoryMileage='+nMileage;
	var request = new Ajax.Request(
		sURL, { method: 'post', parameters: pars,
		onComplete: function(xhr){
			optionsOpen = true;
			new Insertion.Bottom('Options'+contNum,xhr.responseText);
		}
	});
}

var inactive = false;
function toggleInactive(value) {
	inactive = value;
}

var bbInaccurate = false;
var kbbInaccurate = false;
var nadaInaccurate = false;
var galvesInaccurate = false;

function toggleInaccurate(bookId, value) {
	if (bookId == '1') {
		bbInaccurate = value;
	} else if (bookId == '2') {
		nadaInaccurate = value;
	} else if (bookId == '3') {
		kbbInaccurate = value;
	} else if (bookId == '4') {
		galvesInaccurate = value;
	}
}

var printBB = false;
var printKBB = false;
var printKBBJSON={};
var printNADA = false;
var printNADAJSON={};
var printGALVES = false;
function toggleToPrint(bookId, isSelected) {



	if (/blackbook/i.test(bookId)) {
		printBB = isSelected;
	} else if (/nada/i.test(bookId)) {

		printNADAJSON[bookId]= isSelected;
		printNADA= false;
		for (var key in printNADAJSON) {
 		 	if (printNADAJSON.hasOwnProperty(key)) {
    			var value=printNADAJSON[key];
    			if(value === true){
					printNADA = true;
				}
  			}
		}


	} else if (/kelley/i.test(bookId)) {
		printKBBJSON[bookId]= isSelected;
		printKBB= false;
		for (var key in printKBBJSON) {
 		 	if (printKBBJSON.hasOwnProperty(key)) {
    			var value=printKBBJSON[key];
    			if(value === true){
					printKBB = true;
				}
  			}
		}

	} else if (/galves/i.test(bookId)) {
		printGALVES = isSelected;
	}
}


function alertInaccurate() {
	if (!inactive) {
		if ((printBB && bbInaccurate) || (printNADA && nadaInaccurate) || (printKBB && kbbInaccurate) || (printGALVES && galvesInaccurate)) {
			alert('This vehicle\'s Book Values are inaccurate. \nPlease re-book this vehicle. ');
			return false;
		}
	} else {
		if ((printBB && bbInaccurate) || (printNADA && nadaInaccurate) || (printKBB && kbbInaccurate) || (printGALVES && galvesInaccurate)) {
			alert('This vehicle\'s Book Values are inaccurate. \n We\'re sorry, but we are unable to print the requested book sheet(s).\n If you have any comments or concerns please call the FirstLook help desk at : 1-877-378-5665 ');
			return false;
		}
	}
	
	var inventoryId = document.getElementById("inventoryId").value;
	var oldMileage = stripNumberInput($('mileage').innerText);

	var params = 'oldMileage='+oldMileage+""+'&inventoryId='+inventoryId ;
	var url="/IMT/BookoutStatusAction.go";
	var flag= false;
	jQuery.ajax({
		url:url,
		async: false,
		data:params,
		type:"POST",
		success: function(data, status , jqXhr) {
			
			if(data=="INACCURATE"){
				flag= true;
				alert("Books values invalid. Please refresh eStock card for updated values.");
			}
			
			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			flag= true;
			alert("Books values invalid. Please refresh eStock card for updated values.");
		}
		
	})
	if(flag == true){
		return false;
		}
		
}

function updateBookOptions(sURL,primaryBookFlag) {
	rebookButtonStatus();
	toggleAllBookButton('off');
	var nMileage = stripNumberInput(document.getElementById('mileage').innerText);
	sURL += '&inventoryMileage=';
	sURL += nMileage;
	rebook(sURL);
}
function checkForInaccurateBookValues(mileValue) {
	var bFlagBookWarning = changedMileageValues(mileValue);
	if (bFlagBookWarning)
	{
		toggleWarning('inaccurateEdit','on');
	}
	else
	{
		toggleWarning('inaccurateEdit','off');
	}
}
function reloadBookOptions(sURL,sSelValue) {
	if(sSelValue == "selectAModel") {
        return false;
    }
    else{
	sURL += '&selectedKey=' + sSelValue;
	sSelValue == 0 ? '':retrieveURL(sURL);
	}
}
function saveAndRebook(formId,iter,priBook) {
	rebookButtonStatus();
	var guideBooktrimElement = document.getElementById("guideBooktrim");
	if(guideBooktrimElement != null)	{
		var selecteVehicleId= guideBooktrimElement.value;
	}

	if(selecteVehicleId != null && selecteVehicleId == 'selectAModel'){
		alert('Please select a trim');
		return false;
	}
	var sURL = grabBookFormInfo(formId);
	// have to wait for the response before you can finish the java script so the bookVsCost value is correct
	var request = new Ajax.Request(
		sURL,
		{
			method: 'post',
			parameters: '',
			onSuccess: function() { optionsOpen = false;},
			onComplete:
				function (xhr) {
				//Originally the error options selection page was being placed into the book values display
				//element.  Now, if there is an error present, the values are reloaded into the options selection element
				if( xhr.responseText.indexOf('<div class="error">') != -1 ) {
					$('Options'+iter).innerHTML = xhr.responseText;
				} else {
					$('bookout'+iter).innerHTML = xhr.responseText;
				}
				if (priBook) {
				 	toggleWarning('inaccurateDisplay','off');
				 	toggleWarning('inaccurateEdit','off');
				}
				if (formId=='Form4') {
					toggleBooKButton(formId,'off');
					togglePrevBookButton('on');
				}
				if (formId=='Form3') {
					toggleBooKButton(formId,'off');
					togglePrevBookButton('on');
				}
				if (formId=='Form2') {
					toggleBooKButton(formId,'off');
					togglePrevBookButton('on');

					var auctionURL = $('auctionReportLink') + "";
					var auctionIFrameURL = $('auctionIFrame').src + "";

					var make = auctionURL.substring( auctionURL.indexOf('make=') + 5, auctionURL.indexOf('&model='));
					var model = auctionURL.substring( auctionURL.indexOf('&model=') + 7, auctionURL.indexOf('&modelYear='));
					var year = auctionURL.substring( auctionURL.indexOf('&modelYear=') + 11, auctionURL.indexOf('&isAppraisal='));
					var vin = auctionIFrameURL.substring( auctionIFrameURL.indexOf('vin=') + 4, auctionIFrameURL.indexOf('&mileage=') );
					var mileage = auctionIFrameURL.substring( auctionIFrameURL.indexOf('&mileage=') + 9, auctionIFrameURL.indexOf('&isAppraisal') );
					var params = 'make='+make+'&model='+model+'&modelYear='+year+'&vin='+vin+'&mileage='+mileage;

					new Ajax.Updater( 'auction', 'ucbp/AuctionData.go?', {parameters: params});

				}
				if (formId=='Form1') {
					toggleBooKButton(formId,'off');
					togglePrevBookButton('on');
				}

				if (priBook) {
					updateBookVsCost();
				}
			}
		}
	);
}

function rebookWithNewMileage(tempFormId,primaryBookFlag, nMileage) {
	rebookButtonStatus();
	if (nMileage == null)
	{
		nMileage = stripNumberInput(document.getElementById('mileage').innerText);
	}
	var sURL = grabBookFormInfo(tempFormId) + '&inventoryMileage=' + nMileage;

	retrieveURL(sURL, 'noForm', tempFormId, 'false');

	if (primaryBookFlag)
	{
		updateBookVsCost();
	}

	var priBook;
	priBook = primaryBookFlag;
	if (priBook)
	{
		toggleWarning('inaccurateDisplay','off');
	 	toggleWarning('inaccurateEdit','off');
	}
	toggleBooKButton(tempFormId,'off');
	togglePrevBookButton('on');
}

function reloadBookOutTile(sURL,sSelValue) {
	if(sSelValue == "selectAModel") {
         document.getElementById("guideBooktrim").value="selectAModel";
         document.getElementById("guideBooktrim").disabled=true;
        return false;
    }
	else{
		sURL += '&selectedModelKey=' + sSelValue;
		sSelValue == 0 ? '':retrieveURL(sURL);
	}
}
var prevNADAFlag = false;
var prevKBBFlag = false;
var prevGalvesFlag = false;
var prevBBFlag = false;
function rebookButtonStatus(){
	var tempNADAValue="none";
	var tempKBBValue="none";
	var tempGalvesValue = "none";
	var tempBBValue ="none";
	if(document.getElementById("rebookNADA")){
		tempNADAValue = document.getElementById("rebookNADA").style.display;
	}
	if(document.getElementById("rebookKBB")){
		tempKBBValue = document.getElementById("rebookKBB").style.display;
	}
	if(document.getElementById("rebookGalves")){
		tempGalvesValue = document.getElementById("rebookGalves").style.display;
	}
	if(document.getElementById("rebookBB")){
		tempBBValue = document.getElementById("rebookBB").style.display;
	}
	if(tempNADAValue == "block"){
		prevNADAFlag = true;
	}
	if(tempKBBValue == "block"){
		prevKBBFlag = true;
	}
	if(tempGalvesValue == "block"){
		prevGalvesFlag = true;
	}
	if(tempBBValue == "block"){
		prevBBFlag = true;
	}
}
function toggleBooKButton(formId,offFlag){
	if(formId == 'Form4' || formId == "Temp4"){
		prevGalvesFlag=false;
		toggleWarning('inaccurateGalves',offFlag);
		toggleWarning('rebookGalves',offFlag);
		toggleInaccurate(4, false);
	}
	if(formId == 'Form3' || formId=="Temp3"){
		prevKBBFlag=false;
		toggleWarning('inaccurateKBB',offFlag);
		toggleWarning('rebookKBB',offFlag);
		toggleInaccurate(3, false);
	}
	if(formId == 'Form2' || formId=="Temp2"){
		prevNADAFlag=false;
		toggleWarning('inaccurateNADA',offFlag);
		toggleWarning('rebookNADA',offFlag);
		toggleInaccurate(2, false);
	}
	if(formId == 'Form1' || formId=="Temp1"){
		prevBBFlag=false;
		toggleWarning('inaccurateBB',offFlag);
		toggleWarning('rebookBB',offFlag);
		toggleInaccurate(1, false);
	}
	
}
function togglePrevBookButton(flag){
	if(prevNADAFlag){
		toggleWarning('inaccurateNADA',flag);
		toggleWarning('rebookNADA',flag);
	}
	if(prevKBBFlag){
		toggleWarning('inaccurateKBB',flag);
		toggleWarning('rebookKBB',flag);
	}
	if(prevGalvesFlag){
		toggleWarning('inaccurateGalves',flag);
		toggleWarning('rebookGalves',flag);
	}
	if(prevBBFlag){
		toggleWarning('inaccurateBB',flag);
		toggleWarning('rebookBB',flag);
	}
}
function toggleAllBookButton(flag){
	toggleWarning('inaccurateGalves',flag);
	toggleWarning('rebookGalves',flag);
	toggleWarning('inaccurateKBB',flag);
	toggleWarning('rebookKBB',flag);
	toggleWarning('inaccurateNADA',flag);
	toggleWarning('rebookNADA',flag);
	toggleWarning('inaccurateBB',flag);
	toggleWarning('rebookBB',flag);
}