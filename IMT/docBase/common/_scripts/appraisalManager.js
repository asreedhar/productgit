var pageLoaded=""
function loadAppraisals(page){
	
clearDivs();
loadURL(page);


};
function clearDivs(){
	jQuery("#tabs div").html("");
}

function showImageLoading(elem){
	var html="<div style='align:center;v-align:middle'>";
	html+="<img src='goldfish/public/images/loading.gif'  ><br>";
	html+="Loading...";
	html+="</div>";
	jQuery(elem).html(html);

}

function submitSearch(){


var html="<div style='align:center;v-align:middle'>";
	html+="<img src='goldfish/public/images/loading.gif'  ><br>";
	html+="Searching...";
	html+="</div>";
	jQuery("#searchContent").html(html);
	functionshow("SearchDiv");
	
	var params = jQuery("#searchVinForm").serializeArray();

		jQuery.ajax({
			url : "AppraisalManagerSearchAppraisalsSubmitAction.go",
			request : "POST",
			data : params,
			success : function(xyz) {
				//alert('success');
				jQuery("#searchContent").html(xyz);
				functionshow("SearchDiv");
				
				
			},
			failure : function() {
				alert("Could Not Search the VIN");
				functionshow("tabs");
	
			}

		});


}

function functionshow(divname){

	jQuery("#SearchDiv").hide();
	jQuery("#tabs").hide();
	jQuery("#"+divname).show();

}
function loadURL(page){
	showImageLoading("#tabs-"+page);
	var URL="";
	
	var params= jQuery("#dataForm").serializeArray();
	page=page.toLowerCase();
	if(page=='purchase'){
		URL="AppraisalManagerPurchaseAppraisalsTabAction.go";;
	}
	else if(page=='trade'){
		URL="AppraisalManagerTradeAppraisalsTabAction.go";;
	}
	 else if(page=='all'){
		URL="AppraisalManagerAllAppraisalsTabAction.go";;
	}
	 else if(page=='awaiting'){
		URL="AppraisalManagerAwaitingAppraisalsTabAction.go";;
	}
	else{
		URL="AppraisalManagerAllAppraisalsTabAction.go";
	}
	if(pageLoaded =="Loaded")
		params="";
	jQuery.ajax({
			url : URL,
			request : "GET",
			data:params,
			success : function(xyz) {
				//alert('success');
				clearDivs();
				jQuery("#tabs-"+page).html(xyz);
				
				
			},
			failure : function() {
				//alert("Could Not Load Appraisals");
				clearDivs();
				jQuery("#tabs-"+page).html("Could Not Load Appraisals");
				
			}

		});
	
	//jQuery.get(URL,{},function(xhrResponseText){ 
	
	//	jQuery("#tabs-"+page).html(xhrResponseText);
	//},"");
			
		


	


}

jQuery(document).ready(function(){

	jQuery('#tabs').tabs();
	var page="all";
	if(jQuery("#pageNamehidden").val()!=null){
		page=jQuery("#pageNamehidden").val();
	}
	
	jQuery("#"+page.toLowerCase()+"Link").click();
	pageLoaded="Loaded";
	jQuery(document).keypress(function(e) { if(e.keyCode === 13) { e.preventDefault(); return false; } });
	jQuery('#vinForSearch').keyup(function(e){
	    if(e.keyCode == 13)
	    {
	        $("#searchButton").click();
	    }
	});
	
});

