<%@ taglib uri="/WEB-INF/taglibs/struts-html.tld" prefix="html" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>

<script language="javascript">
function setReportType( faxEmailForm, isFax )
{
	faxEmailForm.faxRequest.value = isFax
}

</script>

<html:hidden property="faxRequest" value=""/>

<table id="printFaxTable" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="25"><br></td>
		<td><img src="images/common/shim.gif" width="50" height="25"><br></td>
		<td><img src="images/common/shim.gif" width="190" height="25"><br></td>
		<td><img src="images/common/shim.gif" width="50" height="25"><br></td>
	</tr>
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td class="mainContent">Email to: </td>
		<td><html:text name="faxEmailForm" property="emailAddress" size="25" value=""/></td>
		<td align="left">
			<logic:present name="isAdminSpace">
				<html:submit  property="emailme" onclick="setReportType( this.form, false )" value="Email Report"/>
			</logic:present>
			<logic:notPresent name="isAdminSpace">
				<html:image property="emailme" src="images/dashboard/buttonEmail.gif" onclick="setReportType( this.form, false )"/><br>
			</logic:notPresent>
		</td>
	</tr>
	<tr><td colspan="3"><img src="images/common/shim.gif" height="15"><br></td></tr><!-- SPACER -->
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td class="mainContent">Fax to:</td>
		<td><html:text property="faxToName" size="15" value=""/></td>
	</tr>
	<tr><td colspan="3"><img src="images/common/shim.gif" height="5"><br></td></tr><!-- SPACER -->
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="1"><br></td>
		<td class="mainContent">Fax #:</td>
		<td class="mainContent"><html:text property="faxNumber.areaCode" size="3" maxlength="3" value=""/> - <html:text property="faxNumber.prefix" size="3" maxlength="3" value=""/> - <html:text property="faxNumber.suffix" size="4" maxlength="4" value=""/></td>
		<td>
			<logic:present name="isAdminSpace">
				<html:submit  property="faxme" onclick="setReportType( this.form, true )" value="Fax Report"/>
			</logic:present>
			<logic:notPresent name="isAdminSpace">
				<html:image property="faxme" src="images/dashboard/buttonFax.gif" onclick="setReportType( this.form, true )" /><br>
			</logic:notPresent>
		</td>
	</tr>
</table>
