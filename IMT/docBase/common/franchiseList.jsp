<%@ taglib uri='/WEB-INF/taglibs/struts-html.tld' prefix='html' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>

<td>
	<html:select name="dealerForm" multiple="true" property="franchisesById" tabindex="21" size="7">
    <firstlook:getAllFranchises/>
    <html:options collection="franchises" property="franchiseId" labelProperty="franchiseDescription"/>
 	</html:select>
</td>
