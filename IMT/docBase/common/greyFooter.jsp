<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<div id='pageFooter'>
<strong>SECURE AREA</strong> | <span>Copyright &copy;<firstlook:currentDate format="yyyy"/> <em>INCISENT</em> Technologies, Inc.</span>
</div>