<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<tiles:importAttribute/>

<c:if test="${not(showLogo == false)}"><%-- showLogo defaults to true  --%>
	<c:set var="showLogo" value="true"/>
</c:if>
<c:if test="${not(showClose == false)}"><%-- showClose defaults to true  --%>
	<c:set var="showClose" value="true"/>
</c:if>
<c:if test="${not(showPrint == true)}"><%-- showPrint defaults to false  --%>
	<c:set var="showPrint" value="false"/>
</c:if>


<!-- ******************************* FIRST LOOK NAV *************************************** -->
<table border="0" cellspacing="0" cellpadding="0" width="100%" id="headerSetupTable">
	<tr><td height="1" bgcolor="#000000"><img src="images/common/shim.gif" width="772" height="1" border="0"><br></td></tr>
	<tr>
		<td>
			<table cellspacing="0" cellpadding="0" border="0" width="100%" id="headerMainTable">
				<tr>
					<td width="644">
						<table border="0" cellpadding="0" cellspacing="0" width="644" id="headerTable">
							<tr>
						<c:choose>
							<c:when test="${showLogo}">
								<td width="122"><img src="images/common/fldn_logo.gif" border="0" hspace="5" vspace="5"><br></td>
							</c:when>
							<c:otherwise>
								<td width="122"><img src="images/common/shim.gif" width="112" height="25" border="0" hspace="5" vspace="5"><br></td>
							</c:otherwise>
						</c:choose>
								<td width="522"><img src="images/common/shim.gif" width="1" height="1" border="0"><br></td>
							</tr>
						</table>
					</td>
					<td width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
					<td align="right">
						<table border="0" cellpadding="0" cellspacing="0" id="globalNavTable">
							<tr>
							<c:if test="${showPrint}">
								<td width="25" id="printCell"><img src="images/common/newPrint_25x11_gray.gif" width="25" height="11" border="0" id="print"><br></td>
							</c:if>
							<c:if test="${showPrint && showClose}">
								<td width="3"><img src="images/common/shim.gif" width="3" height="1" border="0"><br></td>							
								<td width="1" bgcolor="#ffffff"><img src="images/common/white_bar.gif" width="1" height="11" border="0"><br></td>
								<td width="3"><img src="images/common/shim.gif" width="3" height="1" border="0"><br></td>
							</c:if>
							<c:if test="${showClose}">
								<td width="69"><a href="RefreshParentAndCloseChildAction.go"><img src="images/common/closeWindow_white.gif" width="69" height="11" border="0" id="closeWindow"></a><br></td>
							</c:if>								
								<td width="5"><img src="images/common/shim.gif" width="5" height="1"><br></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td height="1" bgcolor="#000000"><img src="images/common/shim.gif" width="772" height="1" border="0"><br></td></tr>
</table>
<!-- ******************************** END FIRST LOOK NAV ************************************** -->