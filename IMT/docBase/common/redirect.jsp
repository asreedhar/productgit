<%@ page language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:choose>
<c:when test="${param.to eq 'dealerHome'}">
	<script>document.location.href= '/IMT/DealerHomeDisplayAction.go'</script>
</c:when>
<c:when test="${param.to eq 'flHome'}">
	<script>document.location.href= '/NextGen/SearchHomePage.go'</script>
</c:when>
<c:when test="${param.to eq 'drStoreAction'}">
	<script>document.location.href= '/PrivateLabel/StoreAction.go'</script>
</c:when>
<c:when test="${param.to eq 'fladmin'}">
	<script>document.location.href= '/fl-admin/start.do'</script>
</c:when>
<c:when test="${param.to eq 'drLogout'}">
	<script>document.location.href= '/PrivateLabel/LogoutAction.go?singleSignOut=false'</script>
</c:when>
<c:when test="${param.to eq 'closeSession'}">
	<script>document.location.href= '/NextGen/CloseSessionAction.go?forwardTo=/LogoutAction.go'</script>
</c:when>
<c:when test="${param.to eq 'aip'}">
	<c:redirect url="/InventoryPlan.go" context="/NextGen"/>
</c:when>
<c:when test="${param.to eq 'actionPlans'}">
	<c:redirect url="/ActionPlansReports.go" context="/NextGen"/>
</c:when>
<c:when test="${param.to eq 'viewDeals'}">
	<c:redirect url="/ViewDealsAction.go" context="/NextGen"/>
</c:when>
<c:when test="${param.to eq 'PingIIRedirectionAction'}">
	<script>
		var url = '/IMT/PingIIRedirectionAction.go?inventoryId=${inventoryId}&popup=true';
		window.open(url,'PingII', 'status=1,scrollbars=1,toolbar=0,width=1009,height=860');
		document.location.href= '/IMT/DealerHomeDisplayAction.go';
	</script>
</c:when>
<c:when test="${param.to eq 'edtReport'}">
	<c:redirect url="/ExceptionReportDisplayAction.go" context="/NextGen"/>
</c:when>
<c:when test="${param.to eq 'MAXHome'}">
	<script>document.location.href = '/home';</script>
</c:when>
<c:when test="${param.to eq 'MAXLogin'}">
	<script>document.location.href = '/login';</script>
</c:when>
<c:when test="${param.to eq 'MAXExitStore'}">
	<script>document.location.href = '/login/exit_store';</script>
</c:when>
<c:when test="${param.to eq 'performanceManagementCenter'}">
	<script>document.location.href = '/command_center/Default.aspx?token=${param.token}';</script>
</c:when>
<c:when test="${param.to eq 'PingII' or
				param.to eq 'EdmundsTmv' or
				param.to eq 'MarketStockingGuide' or
				param.to eq 'jdPowerUsedCarMarketDataTool'}">
	<!-- 
		CAS client forces you to www while Max is on the max host.  So we force the service
		parameter back to the max host.
	-->
	<script>
		function encodeIt(str) {
			if (typeof(encodeURIComponent) == 'function') {
            	return encodeURIComponent(str);
			}
			return escape(str);
		}
		var map = {
			token:                '${param.token}',
			stockNumber:          '${param.stockNumber}',
			q:                    '${param.q}',
			vehicleEntityTypeId:  '${param.vehicleEntityTypeId}',
			vehicleEntityId:      '${param.vehicleEntityId}',
			popup:                '${param.popup}',
			pageName:             '${param.pageName}',
			vehicleCatalogId:     '${param.vehicleCatalogId}',
			modelYear:            '${param.modelYear}'
		};
		var qs = '';
		for (var property in map) {
			if (map[property].length > 0) {
		        if (qs.length > 0)
		        	qs = qs + '&';
		        qs = qs + property + '=' + encodeIt(map[property]);
	        }
	    }
		var url = '/pricing/?' + qs;
		document.location.href = url;
	</script>
</c:when>
<c:when test="${param.to eq 'InTransitInventory'}">
	<script>
		
		function encodeIt(str) {
			if (typeof(encodeURIComponent) == 'function') {
            	return encodeURIComponent(str);
			}
			return escape(str);
		}
		
		var url = '/support/Default.aspx?pageName=${param.pageName}';
		var map = {appraisalId:'${param.appraisalId}'};		
		
		for (var property in map) {
			if (map[property].length > 0) {
		        url = url + '&' + property + '=' + encodeIt(map[property]);
	        }
	    }
		document.location.href = url;
	</script>
</c:when>				
<c:otherwise>
	<script>document.location.href = '${nextNavAction}';</script>
</c:otherwise>
</c:choose>