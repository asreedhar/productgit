<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%-- helpText is defined in tiles-defs.xml --%>
<c:if test="${!empty helpText}" var="bHasHelp"/>
<c:set var="sHelpText" value="${helpText}" scope="request"/>

<div id="title">
	<h3>${dealerForm.dealer.nickname}</h3>
	<h1 id="pageTitle">${pageTitle}<c:if test="${bHasHelp}"><a href="javascript:show('pageHelp');" id="pageHelpLink"><img src="<c:url value="/common/_images/icons/help-on52.gif"/>" width="18" height="18" class="help"/></a></c:if></h1>
<c:if test="${bHasHelp}"><c:import url="/common/_help.jsp"><c:param name="helpType" value="pageHelp"/></c:import></c:if>
	<c:if test="${isPrintable}">
<div class="print">
	<%--print version--%>
	<h2>${pageTitle} for ${sessionScope.dealerNick}</h2>
</div>
	</c:if>
</div>
