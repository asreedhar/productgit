<%@ taglib uri='/WEB-INF/taglibs/struts-bean.tld' prefix='bean' %>
<%@ taglib uri='/WEB-INF/taglibs/struts-logic.tld' prefix='logic' %>
<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<logic:present name="viewDeals">
<script type="text/javascript" language="javascript">
isViewDeals = true;
</script>
</logic:present>
<!-- ************* FIRST LOOK HEADER ********************************************************* -->
<table border="0" cellspacing="0" cellpadding="0" width="100%" id="headerSetupTable">
  <tr><td height="1" bgcolor="#000000"><img src="images/common/shim.gif" width="575" height="1" border="0"><br></td></tr>
  <tr>
    <td style="background-image:url(images/common/table_background.gif)">
      <table cellspacing="0" cellpadding="0" border="0" width="100%" id="headerMainTable">
        <tr>
          <td width="488">
            <table border="0" cellpadding="0" cellspacing="0" width="488" id="headerTable">
              <tr>
                <td width="122"><img src="images/common/fldn_logo.gif" width="112" height="25" border="0" hspace="5" vspace="5"><br></td>
                <td width="366"><img src="images/common/shim.gif" width="366" height="1" border="0"><br></td>
              </tr>
            </table>
          </td>
          <td width="10"><img src="images/common/shim.gif" width="10" height="1" border="0"><br></td>
          <td align="right">
            <table border="0" cellpadding="0" cellspacing="0" width="77" id="globalNavTable">
              <tr>
                <td width="3"><img src="images/common/shim.gif" width="3" height="1" border="0"><br></td>
                <td width="69"><a href="RefreshParentAndCloseChildAction.go"><img src="images/common/closeWindow_white.gif" width="69" height="11" border="0" id="closeWindow"></a><br></td>
                <td width="5"><img src="images/common/shim.gif" width="5" height="1"><br></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr><td height="1" bgcolor="#000000"><img src="images/common/shim.gif" width="575" height="1" border="0"><br></td></tr>
</table>
<!-- ************* END FIRST LOOK HEADER ********************************************************* -->

