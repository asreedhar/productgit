<%@ taglib uri='/WEB-INF/taglibs/firstlook.tld' prefix='firstlook' %>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix='logic' %>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="yellowLineTable"><!--  YELLOW LINE TABLE  -->
	<tr><td class="grayBg1"><img src="images/common/shim.gif" width="760" height="1"></td></tr>
</table><!--  END YELLOW LINE TABLE  -->

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="yellowLineTable"><!--  YELLOW LINE TABLE  -->
	<tr><td class="yelBg"><img src="images/common/shim.gif" width="760" height="1"></td></tr>
</table><!--  END YELLOW LINE TABLE  -->

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="shadowTable"><!--  SHADOW RULE TABLE  -->
  <tr>
    <td height="3" style="background-image:url(images/common/checker_drop_shdw.gif)">
      <img src="images/common/shim.gif" width="760" height="3"><br>
    </td>
  </tr>
</table><!--  END SHADOW RULE TABLE  -->

<!-- ************* FIRST LOOK FOOTER  ********************************************************* -->
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="spacerTable"><!-- *** SPACER TABLE *** -->
	<tr><td><img src="images/common/shim.gif" width="1" height="5" border="0"><br></td></tr>
</table>

<table border="0" cellspacing="0" cellpadding="0" width="100%" id="footerTable">
	<tr><!-- SPACER -->
		<td width="6" rowspan="3"><img src="images/common/shim.gif" width="6" height="1" border="0"><br></td>
		<td><img src="images/common/shim.gif" width="754" height="1" border="0"><br></td>
	</tr>
	<tr><td class="pageFooter"><logic:present name="firstlookSession"><b><i>SECURE AREA</i></b>&nbsp;&nbsp;|</logic:present>&nbsp;&nbsp;&copy;<firstlook:currentDate format="yyyy"/> <i>INCISENT</i> Technologies, Inc.</td></tr>
	<tr><td><img src="images/common/shim.gif" width="1" height="5" border="0"><br></td></tr><!-- SPACER -->
</table>
<!-- ************* END FIRST LOOK FOOTER  ********************************************************* -->
