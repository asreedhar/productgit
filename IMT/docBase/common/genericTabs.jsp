<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %> 
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-tiles.tld' prefix='tiles' %>
<tiles:importAttribute/>
<%-- Globals --%>
<c:set var="someXml"><tiles:get name="config"/></c:set>
<x:parse xml="${someXml}" var="parsed"/>

<%-- *** Start Top Level Configuration Variables *** --%>
<%-- Get Dead Link On Selected (True means if an item is selected, it is not clickable. Default: False --%>
<c:set var="deadLinkOnSelected" value="false"/>
<x:if select="$parsed/tabs[@deadLinkOnSelected = 'true']">
	<c:set var="deadLinkOnSelected" value="true"/>
</x:if>

<%-- Get Selected (The tabs/item/@id that should be in the highlighted state. All other items will not be highlited) --%> 
<c:set var="selected"><x:out select="$parsed/tabs/@selected"/></c:set>

<%-- Get Tab Style --%>
<c:set var="tabsStyleText"/>
<x:if select="$parsed/tabs[@style != '']">
	<c:set var="tabsStyleText"> style="<x:out select="$parsed/tabs/@style"/>"</c:set>
</x:if>

<style>
.genericTabsOn
{
	font-family:verdana,Arial,Helvetica,Sans-serif;
	font-size:9px;
	font-weight:bold;
	font-style:italic;
	color:#fc3;
	padding-left:8px;
	padding-right:8px;
	padding-top:0px;
	padding-bottom:3px;
	border-top:1px solid #fc3;
	background-color:#333;
	vertical-align:middle;
}
.genericTabsOn a{color:#fc3;text-decoration:none;}
.genericTabsOff
{
	font-family:verdana,Arial,Helvetica,Sans-serif;
	font-size:9px;
	font-weight:bold;
	color:#999;
	padding-left:8px;
	padding-right:8px;
	padding-top:0px;
	padding-bottom:3px;
	vertical-align:middle;
}
.genericTabsOff a{color:#999;text-decoration:none;}
</style>

<table width="100%" border="0" cellspacing="0" cellpadding="0" ${tabsStyleText} id="genericTabsTable">
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" border="0">
				<tr valign="bottom">
					<td><img src="images/common/shim.gif" width="6" height="32"><br></td>
					<td valign="bottom">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr valign="bottom">
								<td width="15"><img src="images/common/shim.gif" width="15" height="1" border="0"><br></td>
			<c:set var="index" value="1"/>
			<x:forEach var="item" select="$parsed/tabs/item">
				<c:set var="id"><x:out select="@id"/></c:set>
					<c:if test="${index > 1}">
								<td><img src="images/common/shim.gif" width="8" height="1" border="0"><br></td>
					</c:if>
				<c:choose>
					<c:when test="${selected == id}">
								<td><img src="images/dashboard/dashTabLeft7x23.gif" width="7" height="23" border="0"><br></td>
								<td nowrap class="genericTabsOn">
					<c:choose>
						<c:when test="${deadLinkOnSelected == 'true'}">
									<a href="<x:out select="@url"/>">
										<x:out select="@text"/>
									</a>
						</c:when>
						<c:otherwise>
									<x:out select="@text"/>
						</c:otherwise>
					</c:choose>
								</td>
								<td><img src="images/dashboard/dashTabRight7x23.gif" width="7" height="23" border="0"><br></td>
					</c:when>
					<c:otherwise>
								<td nowrap class="genericTabsOff">
									<a href="<x:out select="@url"/>">
										<x:out select="@text"/>
									</a>
								</td>
					</c:otherwise>
				</c:choose>
				<c:set var="index" value="${index + 1}"/>
			</x:forEach>
								<td width="15"><img src="images/common/shim.gif" width="15" height="1" border="0"><br></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>