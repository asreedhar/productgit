<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<jsp:useBean id="currentDate" class="java.util.Date"/>
<div id="footer">

<div id="footer-nav">
 <ul>
  <li>
<a href="/preferences/make_a_deal/preferences" hh:interaction="click:Bottom Navigation/Appraisal Preferences">Preferences</a>
 <span>|</span> </li>      
  <li><a onclick="window.open(this.href);return false;" href="/home/redirect_to_external_url?external_url=%2FIMT%2FReportCenterRedirectionAction.go">Performance Management Reports</a> <span>|</span> </li>
  
  <li><a href="/home/redirect_to_external_url?external_url=%2FNextGen%2FEquityAnalyzer.go">Loan Value - Book Value Calculator</a> <span>|</span> </li>      


  <li><a onclick="window.open(this.href,'popup','width=1000,height=615,scrollbars=yes,resizable=yes');return false;" href="/command_center/DealerWaterGroupReport.aspx?drillthrough=${sessionScope.firstlookSession.currentDealerId }&type=pmr&cp=dr&Id=E6A6F567-2E14-433A-ABCA-0E97DA8A1595">Water Report</a> <span>|</span> </li>

  <li><a href="/home/redirect_to_external_url?external_url=%2FIMT%2FPerformanceAnalyzerDisplayAction.go">Performance Analyzer</a> <span>|</span> </li>  

  <li><a onclick="window.open(this.href,'popup','width=470,height=615');return false;" href="/home/redirect_to_external_url?external_url=%2FIMT%2Fucbp%2FTileManagementCenter.go"><abbr title="Management">Mgmt.</abbr> Summary</a> <span>|</span> </li>
  <li><a href="/home/redirect_to_external_url?external_url=%2FNextGen%2FInventoryOverview.go">Inventory Overview</a></li>
 </ul>
</div>


	<span class="left">
	
		<em class="screen"> <div id = "appuser"> User: <b>${firstlookSession.member.login}</b>  | </div> SECURE AREA</em> <ins class="screen">|</ins> &nbsp;&nbsp;&copy; <fmt:formatDate value="${currentDate}" pattern="yyyy"/> First Look, LLC. All Rights Reserved
	</span>
	<img src="<c:url value="/view/max/firstlook_logo_tagline.gif"/>" class="logoFL" />
</div>
