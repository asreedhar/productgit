<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<tiles:importAttribute scope="request"/>

<%-- //// MAX OVERRIDES --%>
<c:remove var="helpText" />


	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
	<html>
	<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
	<title>${windowTitle}</title>
	<link rel="stylesheet" media="all" type="text/css" href="<c:url value="/common/_styles/global-maxTemp.css?buildNumber=${applicationScope.buildNumber}"/>"/>
	<c:if test="${!isAppraisalManager}" >
	
		<link rel="stylesheet" media="all" type="text/css" href="<c:url value="/common/_styles/max.css?buildNumber=${applicationScope.buildNumber}"/>"/>
		<link rel="stylesheet" media="all" type="text/css" href="goldfish/public/css/overcast/jquery-ui-1.8.23.custom.css"/>
	
	</c:if>
	<c:if test="${isAppraisalManager}" >
		<link rel="stylesheet" media="all" type="text/css" href="<c:url value="/common/_styles/max-appraisalmanager.css?buildNumber=${applicationScope.buildNumber}"/>"/>
		<link rel="stylesheet" media="all" type="text/css" href="goldfish/public/css/overcast/jquery-ui-1.8.23.custom2.css"/>
	</c:if>
	
	
	<c:forEach items="${addtlStyles}" var="aStyle">
	<link rel="stylesheet" media="${isPrintable?'screen,projector':'all'}" type="text/css" href="<c:url value="/common/_styles/${aStyle}?buildNumber=${applicationScope.buildNumber}"/>"/></c:forEach>
	<c:if test="${isPrintable}"><link rel="stylesheet" media="print" type="text/css" href="<c:url value="/common/_styles/global-print.css?buildNumber=${applicationScope.buildNumber}"/>"/><c:forEach items="${addtlPrintStyles}" var="aPrintStyle"><link rel="stylesheet" media="print" type="text/css" href="<c:url value="/common/_styles/${aPrintStyle}"/>"/></c:forEach></c:if>
	<c:import url="/common/_styles/_ie_max.jspf"/>
	
	<script language="JavaScript" type="text/javascript" src="<c:url value="/common/_scripts/prototype.js?buildNumber=${applicationScope.buildNumber}"/>"></script>
	<script language="JavaScript" type="text/javascript" src="<c:url value="/common/_scripts/menu.js?buildNumber=${applicationScope.buildNumber}"/>"></script>
	<c:if test="${isAppraisalManager}" >
		<script language="Javascript" type="text/javascript" src="<c:url value="/goldfish/public/javascript/jquery-1.8.0.min.js?buildNumber=${applicationScope.buildNumber}"/>"></script>
		<script language="Javascript" type="text/javascript" src="<c:url value="/goldfish/public/javascript/jquery-ui-1.8.23.custom.min.js?buildNumber=${applicationScope.buildNumber}"/>"></script>
	</c:if>
		

	
	
	<c:forEach items="${addtlScripts}" var="aScript"><script language="JavaScript" type="text/javascript" src="<c:url value="/common/_scripts/${aScript}"/>?buildNumber=${applicationScope.buildNumber}"></script></c:forEach>
	<script language="JavaScript" type="text/javascript" src="<c:url value="/common/_scripts/global.js"/>?buildNumber=${applicationScope.buildNumber}">
	</script>

	<c:import url="/common/hedgehog-script.jsp" />
	</head>
	<body id="${bIsPopup?'popup':'page'}" class="${sectionId}">
        <script type="text/javascript">
            (function() {
                var isiPad = navigator.userAgent.match(/iPad/);
                if (isiPad) document.body.className += " max-iPad-tm";
            })()
        </script>
	
		<div id="wrap" class="${pageId}">
		
			<c:import url="/common/header-global.jsp" />
			<c:import url="/common/title-global.jsp" />
			
	
	<tiles:insert attribute="tabs" ignore="true"/>
	
	
	<div id="content">
	<!-- //forces min-width for IE --><!--[if !IE 7]><table cellpadding="0" cellspacing="0" class="forcer"><tr><td><![endif]-->
	
		<tiles:insert attribute="content"/>
		
	
	<!--[if !IE 7]><img src="<c:url value="/common/_images/d.gif"/>" class="forcer"/></td><td class="forcer"><img src="<c:url value="/common/_images/d.gif"/>"/></td></tr></table><![endif]-->
	</div>
		<c:import url="/common/footer-global.jsp"/>
		</div>
	</body>
	</html>