<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<tiles:importAttribute scope="request"/><c:if test="${popup || param.popup || isPopup}" var="bIsPopup" scope="request"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true">
<head>

<c:if test="${metaTag}" >
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
</c:if>

	<title>${windowTitle}</title>
	<link rel="stylesheet" media="${isPrintable?'screen,projector':'all'}" type="text/css" href="<c:url value="/common/_styles/global.css?buildNumber=${applicationScope.buildNumber}"/>"/>
<c:forEach items="${addtlStyles}" var="aStyle"><link rel="stylesheet" media="${isPrintable?'screen,projector':'all'}" type="text/css" href="<c:url value="/common/_styles/${aStyle}?buildNumber=${applicationScope.buildNumber}"/>"/></c:forEach>
<c:if test="${isPrintable}"><link rel="stylesheet" media="print" type="text/css" href="<c:url value="/common/_styles/global-print.css"/>"/><c:forEach items="${addtlPrintStyles}" var="aPrintStyle"><link rel="stylesheet" media="print" type="text/css" href="<c:url value="/common/_styles/${aPrintStyle}"/>"/></c:forEach></c:if>
<c:import url="/common/_styles/_ie.jspf"/>
<c:choose><c:when test="${!bIsPopup}"><script language="JavaScript" type="text/javascript" src="<c:url value="/common/_scripts/menu.js?buildNumber=${applicationScope.buildNumber}"/>"></script></c:when><c:otherwise><script language="JavaScript" type="text/javascript">var isPopup = true; self.focus();</script></c:otherwise></c:choose>
<c:forEach items="${addtlScripts}" var="aScript"><script language="JavaScript" type="text/javascript" src="<c:url value="/common/_scripts/${aScript}?buildNumber=${applicationScope.buildNumber}"/>"></script></c:forEach>
<script language="JavaScript" type="text/javascript" src="<c:url value="/common/_scripts/global.js?buildNumber=${applicationScope.buildNumber}"/>"></script>
<script language="Javascript" type="text/javascript" src="<c:url value="/goldfish/public/javascript/jquery-1.8.0.min.js?buildNumber=${applicationScope.buildNumber}"/>"></script>
<script language="Javascript" type="text/javascript">
var $j = jQuery.noConflict();


</script>

<c:import url="/common/hedgehog-script.jsp" />
</head>
<body id="${bIsPopup?'popup':'page'}" class="${sectionId}">
	<div id="wrap" class="${pageId}">
	<c:if test="${!(bIsPopup && hideHeader)}">
		<c:import url="/common/header${bIsPopup?'-popup':'-global'}.jsp"/>
	</c:if>	
	<c:import url="/common/title${pageId=='estock'?'-estock':'-global'}.jsp"/>
<tiles:insert attribute="tabs" ignore="true"/>
<div id="content"><!-- //forces min-width for IE --><!--[if !IE 7]><table cellpadding="0" cellspacing="0" class="forcer"><tr><td><![endif]-->
	<tiles:insert attribute="content"/>
<!--[if !IE 7]><img src="<c:url value="/common/_images/d.gif"/>" class="forcer"/></td></tr></table><![endif]--></div>
	<c:import url="/common/footer-global.jsp"/>
	</div>
	
</body>
</html:html>