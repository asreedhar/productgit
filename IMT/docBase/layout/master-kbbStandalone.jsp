<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<html:html xhtml="true">
<head>
	<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles" %>
	<tiles:importAttribute />
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
	<c:if test="${pageId eq 'kbbValues'}" var="isKBBRipOff" />
	<c:set var="view" value="${isKBBRipOff ? 'kbb':'edge'}" scope="request" />
	<c:set var="pageTitle" scope="request" value="${isKBBRipOff ? 'Kelley Blue Book Consumer Values':'Select Kelley Blue Book Options'}" />
	
	<title>${pageTitle}</title>
	<link rel="stylesheet" media="all" type="text/css" 
		href="<c:url value="/view/common/_styles/core.css"/>" />	
	<link rel="stylesheet" media="all" type="text/css" 
		href="<c:url value="/view/${view}/_styles/layout.css"/>" />

<c:import url="/common/hedgehog-script.jsp" />
</head>
<body id="${pageId}" class="popup">
	<div id="header">
		<p class="dealer">${dealerForm.dealer.nickname}</p>
		<c:if test="${!empty stockNumber}">
			<p class="stockNumber">Stock #: ${stockNumber }</p>
		</c:if>
		<h1>${pageTitle}</h1>
	</div>
	<div id="content">
		<tiles:insert attribute="content" />
	</div>
</body>
</html:html>