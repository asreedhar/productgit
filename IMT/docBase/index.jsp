<%@ page language="java" %>
<%@ taglib uri='/WEB-INF/taglibs/struts-template.tld' prefix='template' %>

<template:insert template='/templates/masterDealerTemplate.jsp'>
  <template:put name='title' 	content='First Look' direct='true'/>
  <template:put name='mainClass' content='fiftyTwoMain' direct='true'/>
  <template:put name='main' 	content='/indexPage.jsp'/>
  <template:put name='bottomLine' content='/common/bottomLine772.jsp'/>
  <template:put name='footer' content='/common/footer_login_772.jsp'/>
</template:insert>

