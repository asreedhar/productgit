<%@ taglib uri="/WEB-INF/taglibs/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/taglibs/struts-logic.tld" prefix="logic"%>

<table width="100%" cellspacing="0" cellpadding="0" border="0" id="spaceTable">
	<tr>
		<td align="center" style="padding-right: 38px">

		<table width="100%" border="0" cellspacing="0" cellpadding="0" id="spacerTable">
			<!-- *** SPACER TABLE *** -->
			<tr>
				<td><img src="images/common/shim.gif" width="1" height="8"><br />
				</td>
			</tr>
		</table>

		<table border="0" cellspacing="0" cellpadding="0" id="pageNameInnerTable" width="100%">
			<tr>
				<td align="center">
				<img src="images/marketing/logo_235x50_52.gif" width="235" height="50" border="0" hspace="13"><br />
				</td>
			</tr>
			<tr>
				<td align="center">
				<img src="images/marketing/intelLogin_267x15.gif" width="267" height="15" border="0" hspace="10" vspace="3"><br />
				</td>
				<!--td align="center"><img src="images/common/dbiText_196x12.gif" width="196" height="12" border="0" hspace="10" vspace="3"><br></td-->
			</tr>
		</table>

		<table width="100%" border="0" cellspacing="0" cellpadding="0"
			id="spacerTable">
			<!-- *** SPACER TABLE *** -->
			<tr>
				<td><img src="images/common/shim.gif" width="710" height="1"><br>
				</td>
			</tr>
			<tr>
				<td><img src="images/common/shim.gif" width="1" height="15"><br>
				</td>
			</tr>
		</table>

		<table border="0" cellspacing="0" cellpadding="1" class="grayBg1"
			id="grayBorderLoginTable">
			<!-- Gray border on table -->
			<tr>
				<td>
				<table id="loginBorder" border="0" cellspacing="0" cellpadding="0"
					class="whtBgBlackBorder">
					<tr class="grayBg2">
						<td>
						<table id="login" width="339" border="0" cellspacing="0"
							cellpadding="0" bgcolor="#333333">
							<tr>
								<td width="23" rowspan="100"><img
									src="images/common/shim.gif" width="23" height="1"></td>
								<td width="293"><img src="images/common/shim.gif"
									width="293" height="1"></td>
								<td width="23" rowspan="100"><img
									src="images/common/shim.gif" width="23" height="1"></td>
							</tr>
							<tr>
								<td><img src="images/common/shim.gif" width="241" height="2"></td>
							</tr>
							<tr>
								<td style="text-align: center; font-size: 12px; color: #F00; padding-bottom: 5px; padding-top: 5px; font-weight: bold;">
									<strong><big>
									<logic:present name="LoginMsg">
									<bean:write	name="LoginMsg" />
									</logic:present>
									<logic:notPresent name="LoginMsg">
										An Error Has occurred.
									</logic:notPresent>
									</big></strong>
								</td>
							</tr>
							<tr>
								<td><img src="images/common/shim.gif" width="263" height="2"></td>
							</tr>
							<tr>
								<td class="blk" style="color: #faf3dc; padding-left: 5px" nowrap>
									<span style="color: #FC3">Please call Customer Service toll free: <bean:message	key="phone.support" /></span>
								</td>
							</tr>
							<tr><td style="text-align: center;"><a href="/IMT/LogoutAction.go" style="color: #FC3"><big>Logout<big></a></td></tr>
							<tr>
								<td><img src="images/common/shim.gif" width="1" height="18"><br /></td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>