<%@ tag body-content="empty" %><%@ attribute name="value" required="true" %><%@ attribute name="separator" required="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
(${fn:substring(value,0,3)})${fn:substring(value,3,6)}${empty separator?'&#45;':separator}${fn:substring(value,6,10)}