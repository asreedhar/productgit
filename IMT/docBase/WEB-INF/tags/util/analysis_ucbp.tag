<%@ attribute name="analysisItems" required="true" type="java.util.Collection" description="A Collection of Analysis items." %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
	.analysis-normal{ font-weight:bold }
	.analysis-bold{ font-weight:bold }
	.analysis-red{ font-weight:bold; color:red }
	.analysis-red-bold{ font-weight:bold; color:red }
</style>
<c:choose>
	<c:when test="${not empty analysisItems}">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<c:forEach var="analysis" items="${analysisItems}">
	<tr valign="baseline">
		<td class="scorecard-disc"><img src="images/common/scorecard_disc.gif"><br></td>
		<td width="99%" class="scorecard-data-${analysis.displayFormat.formatCode}">
			${analysis.text}<br>
			<c:forEach var="child" items="${analysis.children}">
			<span class="scorecard-data-${child.displayFormat.formatCode}">- ${child.text}</span><br>
			</c:forEach>
		</td>
	</tr>
		</c:forEach>
</table>
	</c:when>
	<c:otherwise>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td><img src="images/common/shim.gif" width="1" height="15"><br></td>
	</tr>
</table>
	</c:otherwise>
</c:choose>

