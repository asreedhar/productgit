<%@ tag body-content="empty" %>
<%@ attribute name="vin" required="true" rtexprvalue="true" %>
<%@ attribute name="mileage" required="false" rtexprvalue="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib tagdir="/WEB-INF/tags/view" prefix="view" %>
<jsp:useBean id="_pars" class="java.util.HashMap"/><c:set property="vin" value="${vin}" target="${_pars}" /><c:set property="mileage" value="${not empty mileage ? mileage : 0}" target="${_pars}" />

<%--  if you change this be sure to check out the saveAndSwitch function in estock.js --%>
<html:link styleId="kbbStandAloneLink" forward="kbbStandalone" name="_pars" styleClass="pop" onclick="window.open(this.href, 'kbb', 'width=950,height=600,location=no,status=yes,menubar=yes,toolbar=no,resizable=yes,scrollbars=yes');return false;" target="_blank">
	<img src="<view:img name="kbb_logo.gif"/>" id="kbb-logo"/>
	<c:if test="${hasKbbTradeIn}">
		Trade-In: ${kbbTradeInValue}<br />
	</c:if>
	<c:if test="${hasKbbRetail}">
		Retail: ${kbbRetailValue}<br />
	</c:if>
</html:link>