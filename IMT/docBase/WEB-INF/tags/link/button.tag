<%@ tag body-content="scriptless" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ attribute name="type" required="false" %>
<jsp:doBody var="link"/>
<span class="b">&nbsp;<span <c:if test="${!empty type}">class="${type}" </c:if>onclick="this.children[0].click()" onmouseover="this.children[0].style.textDecoration='underline'" onmouseout="this.children[0].style.textDecoration='none'">${fn:trim(link)}&nbsp;</span></span>