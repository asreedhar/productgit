<%@ tag body-content="scriptless" %><%@ attribute name="headerText" rtexprvalue="false" required="false" %><%@ attribute name="tagName" rtexprvalue="false" required="false" %><%@ attribute name="cols" rtexprvalue="true" required="true" %><%@ attribute name="count" rtexprvalue="true" required="true" %><%@ attribute name="total" rtexprvalue="true" required="true" %><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="_hold">
<c:if test="${empty sessionScope._cols}">
	<jsp:useBean id="_cols" class="java.util.HashMap" scope="session"/>
	<%-- //figure base number per column --%>
		<%-- fmt rounds so must get full value  --%><fmt:formatNumber value="${total div cols}" var="_actual" />
		<%-- get decimals  --%><fmt:formatNumber maxIntegerDigits="0" value="${_actual}" var="_dec" />
	<%-- subtract to get base value  --%>
	<c:set property="baseItems" target="${sessionScope._cols}">
		<fmt:formatNumber maxFractionDigits="0" value="${_actual - _dec}" />
	</c:set>
	<%-- figure num of columns with extra items --%>
	<c:set property="colWithAddtlItem" target="${sessionScope._cols}" value="${total mod cols}"/>
	<%-- variable properties --%>
	<c:set property="colNum" target="${sessionScope._cols}" value="1" />
	<c:set property="colItems" target="${sessionScope._cols}" value="1" />
	<c:set property="open" target="${sessionScope._cols}" value="true" />
	<c:set property="close" target="${sessionScope._cols}" value="false" />
</c:if>
<c:set var="_c" value="${sessionScope._cols}" />
<c:choose>
	<c:when test="${_c.colItems eq _c.baseItems}">
		<c:choose>
			<c:when test="${_c.colNum le _c.colWithAddtlItem}">
				<c:set property="close" target="${_c}" value="false" />
			</c:when>
			<c:otherwise>
				<c:set property="close" target="${_c}" value="true" />
			</c:otherwise>
		</c:choose>
	</c:when>
	<c:when test="${_c.colItems gt _c.baseItems}">
		<c:set property="close" target="${_c}" value="true" />
	</c:when>
</c:choose>

<c:if test="${_c.open eq true}">
	<${not empty tagName ? tagName : 'div'} class="col${_c.colNum eq 1 ? ' f' : _c.colNum eq cols  ? ' l' : ''}">
	<c:if test="${not empty headerText}"><h5>${headerText}</h5></c:if>
	<c:set property="open" target="${_c}" value="false" />
</c:if>

<jsp:doBody />

<c:choose>
	<c:when test="${_c.close eq true}">
		</${not empty tagName ? tagName : 'div'}>
		<c:set property="colNum" target="${_c}" value="${_c.colNum + 1}" />
		<c:set property="open" target="${_c}" value="true" />
		<c:set property="colItems" target="${_c}" value="1" />
		<c:set property="close" target="${_c}" value="false" />
	</c:when>
	<c:otherwise>
		<c:set property="colItems" target="${_c}" value="${_c.colItems + 1}" />
	</c:otherwise>
</c:choose>
<c:if test="${count eq total}"><c:remove var="_cols" scope="session" /></c:if>
</c:set>
${fn:trim(_hold)}