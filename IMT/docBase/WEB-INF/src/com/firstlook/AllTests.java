package com.firstlook;

import junit.awtui.TestRunner;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.firstlook.action.TestBaseActionForm;

public class AllTests extends TestCase
{

public AllTests( String arg1 )
{
    super( arg1 );
}

public static void main( String[] args )
{
    TestRunner runner = new TestRunner();
    runner.start( new String[] { "com.firstlook.AllTests" } );
}

public static Test suite()
{
    TestSuite suite = new TestSuite();

    suite.addTest( new TestSuite( TestBaseActionForm.class ) );
    suite.addTest( com.firstlook.action.components.AllTests.suite() );
    suite.addTest( com.firstlook.action.dealer.buy.AllTests.suite() );
    suite.addTest( com.firstlook.action.dealer.reports.AllTests.suite() );
    suite.addTest( com.firstlook.action.dealer.sell.AllTests.suite() );
    suite.addTest( com.firstlook.action.dealer.tools.AllTests.suite() );
    suite.addTest( com.firstlook.calculator.AllTests.suite() );
    suite.addTest( com.firstlook.comparator.AllTests.suite() );
    suite.addTest( com.firstlook.dealer.tools.AllTests.suite() );
    suite.addTest( com.firstlook.entity.AllTests.suite() );
    suite.addTest( com.firstlook.entity.form.AllTests.suite() );
    suite.addTest( com.firstlook.helper.AllTests.suite() );
    suite.addTest( com.firstlook.helper.action.AllTests.suite() );
    suite.addTest( com.firstlook.iterator.AllTests.suite() );
    suite.addTest( com.firstlook.persistence.pricepoints.AllTests.suite() );
    suite.addTest( com.firstlook.presentation.AllTests.suite() );
    suite.addTest( com.firstlook.report.AllTests.suite() );
    suite.addTest( com.firstlook.scorecard.AllTests.suite() );
    suite.addTest( com.firstlook.service.accountability.AllTests.suite() );
    suite.addTest( com.firstlook.service.accountability.aging.AllTests.suite() );
    suite.addTest( com.firstlook.service.accountability.purchasing.AllTests.suite() );
    suite.addTest( com.firstlook.service.accountability.tradein.AllTests.suite() );
    suite.addTest( com.firstlook.service.businessunit.AllTests.suite() );
    suite.addTest( com.firstlook.service.cia.AllTests.suite() );
    suite.addTest( com.firstlook.service.cia.transactional.AllTests.suite() );
    suite.addTest( com.firstlook.service.dealer.AllTests.suite() );
    suite.addTest( com.firstlook.service.groupingdescription.AllTests.suite() );
    suite.addTest( com.firstlook.service.inventory.AllTests.suite() );
    suite.addTest( com.firstlook.service.inventorystocking.AllTests.suite() );
    suite.addTest( com.firstlook.service.pap.AllTests.suite() );
    suite.addTest( com.firstlook.service.risklevel.AllTests.suite() );
    suite.addTest( com.firstlook.service.member.AllTests.suite() );
    suite.addTest( com.firstlook.service.pricepoints.AllTests.suite() );
    suite.addTest( com.firstlook.service.scorecard.AllTests.suite() );
    suite.addTest( com.firstlook.service.scorecardtargets.AllTests.suite() );
    suite.addTest( com.firstlook.service.tools.AllTests.suite() );
    suite.addTest( com.firstlook.service.vehiclesale.AllTests.suite() );
    suite.addTest( com.firstlook.taglib.AllTests.suite() );
    suite.addTest( com.firstlook.util.AllTests.suite() );
    suite.addTest( com.firstlook.email.AllTests.suite() );

    return suite;
}

}