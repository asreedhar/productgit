package com.firstlook.scorecard;

import java.util.Date;

import org.hibernate.HibernateException;

import com.firstlook.aet.aggregate.db.TotalInventory;
import com.firstlook.aet.aggregate.model.InventoryAgeBand;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.data.DatabaseException;

public class NewAgingScoreCardCalculator extends ScoreCardCalculator
{

private double currentTotalAgingInventory;
private double priorTotalAgingInventory;

public NewAgingScoreCardCalculator( Date processDate, int dealerId )
{
	super( processDate, dealerId, VehicleType.NEW_TYPE );
}

public double retrieveAgedOver90DaysCurrent()
{
	try
	{
		double currentValue = TotalInventory.calculateInventoryCountFor( dealerId, InventoryAgeBand.NEW_OVER_90BAND, VehicleType.NEW_TYPE,
																			getWeekEndDate() );
		double totalValue = retrieveCurrentAgedInventoryTotal();

		if ( totalValue != 0 )
		{
			return currentValue / totalValue;
		}
		else
		{
			return 0;
		}
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve Aged inventory for over 90 days current week; returning 0.", e );
		return 0;
	}
}

private double retrieveCurrentAgedInventoryTotal() throws HibernateException, DatabaseException
{
	if ( currentTotalAgingInventory == 0 )
	{
		currentTotalAgingInventory = TotalInventory.calculateFor( dealerId, VehicleType.NEW_TYPE, getWeekEndDate() );
	}
	return currentTotalAgingInventory;
}

private double retrievePriorAgedInventoryTotal() throws HibernateException, DatabaseException
{
	if ( priorTotalAgingInventory == 0 )
	{
		priorTotalAgingInventory = TotalInventory.calculateFor( dealerId, VehicleType.NEW_TYPE, getPreviousWeekEndDate() );
	}
	return priorTotalAgingInventory;
}

public double retrieveAgedOver90DaysPriorWeek()
{
	try
	{
		double value = TotalInventory.calculateInventoryCountFor( dealerId, InventoryAgeBand.NEW_OVER_90BAND, VehicleType.NEW_TYPE,
																	getPreviousWeekEndDate() );
		double totalValue = retrievePriorAgedInventoryTotal();

		if ( totalValue != 0 )
		{
			return value / totalValue;
		}
		else
		{
			return 0;
		}
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve Aged inventory for over 90 days previous week; returning 0.", e );
		return 0;
	}
}

public double retrieveAged76To90DaysCurrent()
{
	try
	{
		double value = TotalInventory.calculateInventoryCountFor( dealerId, InventoryAgeBand.NEW_76_90BAND, VehicleType.NEW_TYPE,
																	getWeekEndDate() );
		double totalValue = retrieveCurrentAgedInventoryTotal();

		if ( totalValue != 0 )
		{
			return value / totalValue;
		}
		else
		{
			return 0;
		}

	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve Aged inventory for 51 - 60 days current week; returning 0.", e );
		return 0;
	}
}

public double retrieveAged76To90DaysPriorWeek()
{
	try
	{
		double value = TotalInventory.calculateInventoryCountFor( dealerId, InventoryAgeBand.NEW_76_90BAND, VehicleType.NEW_TYPE,
																	getPreviousWeekEndDate() );
		double totalValue = retrievePriorAgedInventoryTotal();

		if ( totalValue != 0 )
		{
			return value / totalValue;
		}
		else
		{
			return 0;
		}
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve Aged inventory for 76 - 90 days previous week; returning 0.", e );
		return 0;
	}
}

public double retrieveAged61To75DaysCurrent()
{
	try
	{
		double value = TotalInventory.calculateInventoryCountFor( dealerId, InventoryAgeBand.NEW_61_75BAND, VehicleType.NEW_TYPE,
																	getWeekEndDate() );
		double totalValue = retrieveCurrentAgedInventoryTotal();

		if ( totalValue != 0 )
		{
			return value / totalValue;
		}
		else
		{
			return 0;
		}
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve Aged inventory for 61 - 75 days current week; returning 0.", e );
		return 0;
	}
}

public double retrieveAged61To75DaysPriorWeek()
{
	try
	{
		double value = TotalInventory.calculateInventoryCountFor( dealerId, InventoryAgeBand.NEW_61_75BAND, VehicleType.NEW_TYPE,
																	getPreviousWeekEndDate() );
		double totalValue = retrievePriorAgedInventoryTotal();

		if ( totalValue != 0 )
		{
			return value / totalValue;
		}
		else
		{
			return 0;
		}
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve Aged inventory for 61 - 75 days previous week; returning 0.", e );
		return 0;
	}
}

public double retrieveAged31To60DaysCurrent()
{
	try
	{
		double value = TotalInventory.calculateInventoryCountFor( dealerId, InventoryAgeBand.NEW_31_60BAND, VehicleType.NEW_TYPE,
																	getWeekEndDate() );
		double totalValue = retrieveCurrentAgedInventoryTotal();

		if ( totalValue != 0 )
		{
			return value / totalValue;
		}
		else
		{
			return 0;
		}
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve Aged inventory for 31 - 60 days current week; returning 0.", e );
		return 0;
	}
}

public double retrieveAged31To60DaysPriorWeek()
{
	try
	{
		double value = TotalInventory.calculateInventoryCountFor( dealerId, InventoryAgeBand.NEW_31_60BAND, VehicleType.NEW_TYPE,
																	getPreviousWeekEndDate() );
		double totalValue = retrievePriorAgedInventoryTotal();

		if ( totalValue != 0 )
		{
			return value / totalValue;
		}
		else
		{
			return 0;
		}
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve Aged inventory for 31 - 60 days previous week; returning 0.", e );
		return 0;
	}
}

public double retrieveAged0To30DaysCurrent()
{
	try
	{
		double value = TotalInventory.calculateInventoryCountFor( dealerId, InventoryAgeBand.NEW_0_30BAND, VehicleType.NEW_TYPE,
																	getWeekEndDate() );
		double totalValue = retrieveCurrentAgedInventoryTotal();

		if ( totalValue != 0 )
		{
			return value / totalValue;
		}
		else
		{
			return 0;
		}

	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve Aged inventory for 0 - 30 days current week; returning 0.", e );
		return 0;
	}
}

public double retrieveAged0To30DaysPriorWeek()
{
	try
	{
		double value = TotalInventory.calculateInventoryCountFor( dealerId, InventoryAgeBand.NEW_0_30BAND, VehicleType.NEW_TYPE,
																	getPreviousWeekEndDate() );
		double totalValue = retrievePriorAgedInventoryTotal();

		if ( totalValue != 0 )
		{
			return value / totalValue;
		}
		else
		{
			return 0;
		}
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve Aged inventory for 0 - 30 days previous week; returning 0.", e );
		return 0;
	}
}

}
