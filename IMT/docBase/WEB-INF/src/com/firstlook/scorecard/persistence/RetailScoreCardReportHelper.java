package com.firstlook.scorecard.persistence;


import com.firstlook.entity.InventorySalesAggregate;
import com.firstlook.persistence.inventory.InventorySalesAggregateService;
import com.firstlook.scorecard.ScoreCardReport;

public class RetailScoreCardReportHelper extends ScoreCardReportHelper
{

private InventorySalesAggregateService inventorySalesAggregateService;

public RetailScoreCardReportHelper()
{
    super();
}

//this is the same is all classes extending ScoreCardReportHelper. 
//it can't live in ScoreCardReportHelper because we need to inject InventorySalesAggregateService.
public ScoreCardReport findPerformance( int dealerId, int weeks, int inventoryType )
{
    ScoreCardReport report = new ScoreCardReport();
    InventorySalesAggregate aggregate = inventorySalesAggregateService.retrieveCachedInventorySalesAggregate( dealerId, weeks, 0, inventoryType );
    if ( aggregate != null )
    {
        populateReportWithAggregateData( aggregate, report );
    }

    return report;
}

protected void populateReportWithAggregateData( InventorySalesAggregate aggregate, ScoreCardReport report )
{
	report.setAverageGrossProfit( aggregate.getRetailAGP() );
	report.setDaysToSell( aggregate.getRetailAvgDaysToSale() );
	report.setSellThrough( aggregate.getSellThroughRate() );
	report.setWholeSalePerformance( aggregate.getWholesalePerformance() );
	report.setPurchaseRetailProfit( aggregate.getRetailPurchaseProfit() );
	report.setTradeRetailProfit( aggregate.getRetailTradeProfit() );
}

public InventorySalesAggregateService getInventorySalesAggregateService()
{
    return inventorySalesAggregateService;
}

public void setInventorySalesAggregateService( InventorySalesAggregateService inventorySalesAggregateService )
{
    this.inventorySalesAggregateService = inventorySalesAggregateService;
}


}
