package com.firstlook.scorecard.persistence;

import java.util.ArrayList;
import java.util.Collection;


import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventorySalesAggregate;
import com.firstlook.exception.ApplicationException;
import com.firstlook.scorecard.ScoreCardReport;

public abstract class ScoreCardReportHelper
{

public ScoreCardReportHelper()
{
    super();
}

public abstract ScoreCardReport findPerformance( int dealerId, int weeks,
                                        int inventoryType );

public Collection<ScoreCardReport> createScoreCardReports( Dealer dealer, int[] weeks,
        int inventoryType ) throws ApplicationException
{
    Collection<ScoreCardReport> scoreCardReports = new ArrayList<ScoreCardReport>();

    for (int i = 0; i < weeks.length; i++)
    {
        ScoreCardReport report = findPerformance(dealer.getDealerId()
                .intValue(), weeks[i], inventoryType);
        scoreCardReports.add(report);
    }

    return scoreCardReports;
}

protected abstract void populateReportWithAggregateData( InventorySalesAggregate aggregate, ScoreCardReport report );

}
