package com.firstlook.scorecard.persistence;


import com.firstlook.entity.InventorySalesAggregate;
import com.firstlook.persistence.inventory.InventorySalesAggregateService;
import com.firstlook.scorecard.ScoreCardReport;

public class TradeInScoreCardReportHelper extends ScoreCardReportHelper
{

private InventorySalesAggregateService inventorySalesAggregateService;

public TradeInScoreCardReportHelper()
{
    super();
}

//this is the same is all classes extending ScoreCardReportHelper. 
//it can't live in ScoreCardReportHelper because we need to inject InventorySalesAggregateService.
public ScoreCardReport findPerformance( int dealerId, int weeks, int inventoryType )
{
    ScoreCardReport report = new ScoreCardReport();
    InventorySalesAggregate aggregate = inventorySalesAggregateService.retrieveCachedInventorySalesAggregate( dealerId, weeks, 0, inventoryType );
    if ( aggregate != null )
    {
        populateReportWithAggregateData( aggregate, report );
    }

    return report;
}

protected void populateReportWithAggregateData( InventorySalesAggregate aggregate, ScoreCardReport report )
{
    report.setAverageGrossProfit( aggregate.getRetailTradeAGP() );
    report.setDaysToSell( aggregate.getRetailTradeAvgDaysToSale() );
    report.setSellThrough( aggregate.getRetailTradeSellThrough() );
    report.setAverageNoSaleLoss( aggregate.getRetailTradeAvgNoSaleLoss() );
    report.setPercentTradeAnalyzed( aggregate.getRetailTradePercentAnalyzed() );
    report.setAverageFlipLoss( aggregate.getRetailTradeAvgFlipLoss() );
}

public InventorySalesAggregateService getInventorySalesAggregateService()
{
    return inventorySalesAggregateService;
}

public void setInventorySalesAggregateService( InventorySalesAggregateService inventorySalesAggregateService )
{
    this.inventorySalesAggregateService = inventorySalesAggregateService;
}

}
