package com.firstlook.scorecard;

import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.model.VehicleType;

public class ScoreCardCalculator
{

protected static final Logger logger = Logger.getLogger( ScoreCardCalculator.class );

protected static final int DAYS_12WEEKS = 12 * 7;
protected static final int DAYS_26WEEKS = 26 * 7;
protected static final int ONE_MONTH = 1;
protected static final int THREE_MONTHS = 3;
protected Date processDate;
protected int dealerId;
protected VehicleType inventoryType;

public ScoreCardCalculator( Date processDate, int dealerId )
{
	this.dealerId = dealerId;
	this.processDate = processDate;
}

public ScoreCardCalculator( Date processDate, int dealerId, VehicleType inventoryType )
{
	this( processDate, dealerId );
	this.inventoryType = inventoryType;
}

Date retrievePreviousSaturday( Date processDate )
{
	return TimePeriod.CURRENT.getEndDate( processDate );
}

public Date getWeekEndDate()
{
	return retrievePreviousSaturday( getProcessDate() );
}

public Date getWeekEndDateZeros()
{
	Date previousSaturday = retrievePreviousSaturday( getProcessDate() );
	Calendar calendar = Calendar.getInstance();
	calendar.setTime( previousSaturday );
	calendar.set( Calendar.HOUR_OF_DAY, 0 );
	calendar.set( Calendar.MINUTE, 0 );
	calendar.set( Calendar.SECOND, 0 );
	calendar.set( Calendar.MILLISECOND, 0 );

	return calendar.getTime();
}

public Date getProcessDate()
{
	return processDate;
}

public void setProcessDate( Date date )
{
	processDate = date;
}

public Date getPreviousWeekEndDate()
{
	Calendar calendar = Calendar.getInstance();
	calendar.setTime( getWeekEndDate() );
	calendar.add( Calendar.WEEK_OF_YEAR, -1 );

	return calendar.getTime();
}

public int getWeekNumber()
{
	Calendar calendar = Calendar.getInstance();
	calendar.setTime( getWeekEndDate() );
	return calendar.get( Calendar.WEEK_OF_YEAR );
}

public VehicleType getInventoryType()
{
	return inventoryType;
}

public void setInventoryType( VehicleType type )
{
	inventoryType = type;
}

public int getDealerId()
{
	return dealerId;
}

public void setDealerId( int i )
{
	dealerId = i;
}

}
