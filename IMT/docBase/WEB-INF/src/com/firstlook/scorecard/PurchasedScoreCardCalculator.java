package com.firstlook.scorecard;

import java.util.Date;
import java.util.List;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.db.AverageDaysToSale;
import com.firstlook.aet.aggregate.db.AverageGrossProfitRetail;
import com.firstlook.aet.aggregate.db.AverageInventoryAge;
import com.firstlook.aet.aggregate.db.RetailUnitSales;
import com.firstlook.aet.aggregate.db.SummarySalesRetriever;
import com.firstlook.aet.aggregate.db.Winners;
import com.firstlook.aet.aggregate.model.AcquisitionType;
import com.firstlook.aet.aggregate.model.SummaryWinners;
import com.firstlook.aet.aggregate.model.VehicleType;

public class PurchasedScoreCardCalculator extends ScoreCardCalculator
{

private List salesTrend;
private List sales12Week;

public PurchasedScoreCardCalculator( Date processDate, int dealerId, VehicleType inventoryType )
{
	super( processDate, dealerId, inventoryType );

	try
	{
		salesTrend = SummarySalesRetriever.retrieve( dealerId, inventoryType, getProcessDate(), TimePeriod.SIX_WEEKS, AcquisitionType.PURCHASE );
		sales12Week = SummarySalesRetriever.retrieve( dealerId, inventoryType, getProcessDate(), TimePeriod.TWELVE_WEEKS_INCLUSIVE,
														AcquisitionType.PURCHASE );
	}
	catch ( Exception ex )
	{
		ex.printStackTrace();
	}
}

public double retrievePurchasedSellThroughTrend()
{
	double retVal = 0;

	try
	{
		double retailUnitSales = RetailUnitSales.calculateUnitSales( salesTrend );
		double noSales = com.firstlook.aet.aggregate.db.NoSales.calculateFor( dealerId, getProcessDate(), inventoryType,
																				AcquisitionType.PURCHASE_TYPE, TimePeriod.SIX_WEEKS );

		if ( ( retailUnitSales + noSales ) > 0 )
		{
			retVal = retailUnitSales / ( retailUnitSales + noSales );
		}
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve Purchase sell through trend; returning 0.", e );
	}

	return retVal;
}

public double retrievePurchasedSellThrough12Week()
{
	double retVal = 0;

	try
	{
		double retailUnitSales = RetailUnitSales.calculateUnitSales( sales12Week );
		double noSales = com.firstlook.aet.aggregate.db.NoSales.calculateFor( dealerId, getProcessDate(), inventoryType,
																				AcquisitionType.PURCHASE_TYPE,
																				TimePeriod.TWELVE_WEEKS_INCLUSIVE );

		if ( ( retailUnitSales + noSales ) > 0 )
		{
			retVal = retailUnitSales / ( retailUnitSales + noSales );
		}
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve Purchase sell through 12 weeks; returning 0.", e );
	}

	return retVal;
}

public double retrievePurchasedAvgDaysToSaleTrend()
{
	return AverageDaysToSale.calculateAverageDaysToSale( salesTrend );
}

public double retrievePurchasedAvgDaysToSale12Weeks()
{
	return AverageDaysToSale.calculateAverageDaysToSale( sales12Week );
}

public double retrievePurchasedPercentWinnersTrend()
{
	try
	{
		double all = Winners.calculateFor( dealerId, SummaryWinners.ALL, getProcessDate(), inventoryType, TimePeriod.SIX_WEEKS,
											AcquisitionType.PURCHASE.intValue() );
		double winners = Winners.calculateFor( dealerId, SummaryWinners.WINNERS, getProcessDate(), inventoryType, TimePeriod.SIX_WEEKS,
												AcquisitionType.PURCHASE.intValue() );

		if ( all > 0 )
		{
			return winners / all;
		}
		else
		{
			return 0;
		}
	}
	catch ( Exception he )
	{
		logger.error( "Unable to retrieve Purchased percent winners trend; returning 0.", he );
		return 0;
	}
}

public double retrievePurchasedPercentWinners12Week()
{
	try
	{
		double all = Winners.calculateFor( dealerId, SummaryWinners.ALL, getProcessDate(), inventoryType, TimePeriod.TWELVE_WEEKS_INCLUSIVE,
											AcquisitionType.PURCHASE.intValue() );
		double winners = Winners.calculateFor( dealerId, SummaryWinners.WINNERS, getProcessDate(), inventoryType,
												TimePeriod.TWELVE_WEEKS_INCLUSIVE, AcquisitionType.PURCHASE.intValue() );

		if ( all > 0 )
		{
			return winners / all;
		}
		else
		{
			return 0;
		}
	}
	catch ( Exception he )
	{
		logger.error( "Unable to retrieve Purchased percent winners 12 weeks; returning 0.", he );
		return 0;
	}
}

public double retrievePurchasedAverageGrossProfitTrend()
{
	return AverageGrossProfitRetail.calculateFrontEndAverageGrossProfit( salesTrend );
}

public double retrievePurchasedAverageGrossProfit12Week()
{
	return AverageGrossProfitRetail.calculateFrontEndAverageGrossProfit( sales12Week );
}

public double retrievePurchasedAvgInventoryAgeCurrent()
{
	try
	{
		return AverageInventoryAge.calculateFor( dealerId, VehicleType.USED_TYPE, getProcessDate(), TimePeriod.CURRENT,
													AcquisitionType.PURCHASE );
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve average inv age six weeks; returning 0.", e );
		return 0;
	}
}

public double retrievePurchasedAvgInventoryAgePrior()
{
	try
	{
		return AverageInventoryAge.calculateFor( dealerId, VehicleType.USED_TYPE, getProcessDate(), TimePeriod.PRIOR, AcquisitionType.PURCHASE );
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve average inv age twelve weeks; returning 0.", e );
		return 0;
	}
}

}
