package com.firstlook.scorecard;

import java.util.Date;

import org.apache.log4j.Logger;

import com.firstlook.aet.aggregate.TimePeriod;
import com.firstlook.aet.aggregate.db.AverageDaysToSale;
import com.firstlook.aet.aggregate.db.AverageGrossProfitRetail;
import com.firstlook.aet.aggregate.db.RetailUnitSales;
import com.firstlook.aet.aggregate.model.VehicleSegment;
import com.firstlook.aet.aggregate.model.VehicleType;

public class PerformanceSummaryScoreCardCalculator extends ScoreCardCalculator
{

private static Logger logger = Logger.getLogger( PerformanceSummaryScoreCardCalculator.class );

private Date processDate;

public PerformanceSummaryScoreCardCalculator( Date processDate, int dealerId, VehicleType inventoryType )
{
	super( processDate, dealerId, inventoryType );
	this.processDate = processDate;
}

public Integer retrieveAvgDaysToSaleTrend( VehicleSegment segment )
{
	return getAvgDaysToSale( segment, TimePeriod.SIX_WEEKS );
}

private Integer getAvgDaysToSale( VehicleSegment segment, TimePeriod timePeriod )
{
	double value;
	try
	{
		value = AverageDaysToSale.calculateFor( dealerId, inventoryType, processDate, timePeriod, segment );
	}
	catch ( Exception he )
	{
		logger.error( "Error retrieving avg days to sale", he );
		return null;
	}
	return new Integer( (int)Math.round( value ) );
}

public Integer retrieveAvgDaysToSale12Week( VehicleSegment segment )
{
	return getAvgDaysToSale( segment, TimePeriod.TWELVE_WEEKS_INCLUSIVE );
}

public Integer retrieveRetailAvgGrossProfitTrend( VehicleSegment segment )
{
	return getRetailAvgGrossProfit( segment, TimePeriod.SIX_WEEKS );
}

private Integer getRetailAvgGrossProfit( VehicleSegment segment, TimePeriod period )
{
	double value;
	try
	{
		value = AverageGrossProfitRetail.calculateFor( dealerId, inventoryType, processDate, period, segment );
	}
	catch ( Exception he )
	{
		logger.error( "Error retrieving avg gross profit", he );
		return null;
	}
	return new Integer( (int)Math.round( value ) );
}

public Integer retrieveRetailAvgGrossProfit12Week( VehicleSegment segment )
{
	return getRetailAvgGrossProfit( segment, TimePeriod.TWELVE_WEEKS_INCLUSIVE );
}

public Integer retrievePercentSalesTrend( VehicleSegment segment )
{
	return getPercentSales( segment, TimePeriod.SIX_WEEKS );
}

private Integer getPercentSales( VehicleSegment segment, TimePeriod period )
{
	double totalSales;
	try
	{
		totalSales = RetailUnitSales.calculateFor( dealerId, inventoryType, processDate, period );

		if ( totalSales == 0 )
		{
			return new Integer( 0 );
		}
		else
		{
			double segmentSales = RetailUnitSales.calculateFor( dealerId, inventoryType, processDate, period, segment );

			double percentSales = ( segmentSales / totalSales ) * 100;

			return new Integer( (int)Math.round( percentSales ) );
		}
	}
	catch ( Exception he )
	{
		logger.error( "Problem calculating retail unit sales" );
		return null;
	}
}

public Integer retrievePercentSales12Week( VehicleSegment segment )
{
	return getPercentSales( segment, TimePeriod.TWELVE_WEEKS_INCLUSIVE );
}

}
