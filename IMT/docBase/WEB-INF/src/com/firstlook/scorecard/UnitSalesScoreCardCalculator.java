package com.firstlook.scorecard;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

import com.firstlook.persistence.lite.VehicleSaleLitePersistence;
import com.firstlook.service.vehiclesale.VehicleSaleService;

public class UnitSalesScoreCardCalculator extends ScoreCardCalculator
{

private int inventoryTypeId;
private VehicleSaleLitePersistence vehicleSaleLitePeristence;
private VehicleSaleService vehicleSaleService;

public UnitSalesScoreCardCalculator( Date processDate, int dealerId, int inventoryType )
{
	super( processDate, dealerId );
	this.inventoryTypeId = inventoryType;
	vehicleSaleLitePeristence = new VehicleSaleLitePersistence();
}

public int retrieveMonthToDateUnitSales()
{
	Date endDate = getWeekEndDate();
	Calendar calendar = Calendar.getInstance();
	calendar.setTime( endDate );
	calendar.set( Calendar.DAY_OF_MONTH, 1 );

	Date startDate = calendar.getTime();

	Collection units;
	try
	{
		units = vehicleSaleLitePeristence.findByDealerIdAndDealDate( dealerId, startDate, endDate, inventoryTypeId );
		return units.size();
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve Unit sales month to date; returning 0.", e );
		return 0;
	}
}

public int retrieveLastMonthUnitSales()
{
	Date startDate = new Date();
	Date endDate = new Date();
	setScoreCardStartAndEndDate( startDate, endDate, 1, 1 );

	Collection units;
	try
	{
		units = vehicleSaleLitePeristence.findByDealerIdAndDealDate( dealerId, startDate, endDate, inventoryTypeId );
		return units.size();
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve Unit sales by month; returning 0.", e );
		return 0;
	}
}

public double retrieveThreeMonthUnitSales()
{
	Date startDate = new Date();
	Date endDate = new Date();
	setScoreCardStartAndEndDate( startDate, endDate, 4, 3 );

	Collection units;
	try
	{
		units = vehicleSaleLitePeristence.findByDealerIdAndDealDate( dealerId, startDate, endDate, inventoryTypeId );
		return units.size() / 3.0;
	}
	catch ( Exception e )
	{
		logger.error( "Unable to retrieve Unit sales by month; returning 0.", e );
		return 0;
	}
}

void setScoreCardStartAndEndDate( Date startDate, Date endDate, int numberOfMonthsPrior, int numberOfMonthsToInclude )
{
	Calendar calendar = Calendar.getInstance();
	calendar.setTime( getWeekEndDate() );

	calendar.add( Calendar.MONTH, -numberOfMonthsPrior );
	calendar.set( Calendar.DAY_OF_MONTH, 1 );
	calendar = DateUtils.truncate( calendar, Calendar.DATE );
	startDate.setTime( calendar.getTime().getTime() );

	calendar.add( Calendar.MONTH, numberOfMonthsToInclude );
	calendar.add( Calendar.DAY_OF_MONTH, -1 );
	calendar.set( Calendar.HOUR_OF_DAY, 23 );
	calendar.set( Calendar.MINUTE, 59 );
	calendar.set( Calendar.SECOND, 59 );
	calendar.set( Calendar.MILLISECOND, 999 );
	endDate.setTime( calendar.getTime().getTime() );
}

public VehicleSaleService getVehicleSaleService()
{
	return vehicleSaleService;
}

public void setVehicleSaleService( VehicleSaleService vehicleSaleService )
{
	this.vehicleSaleService = vehicleSaleService;
}
}
