package com.firstlook.scorecard;

import java.util.Calendar;
import java.util.Date;

import junit.framework.TestCase;

import com.firstlook.aet.aggregate.model.VehicleType;

public class TestOverallScorecardCalculator extends TestCase
{

private OverallScoreCardCalculator calculator;

public TestOverallScorecardCalculator( String name )
{
	super( name );
}

public void setUp()
{

	calculator = new OverallScoreCardCalculator( createDate( 2003, 12, 1 ), 100135, VehicleType.USED_TYPE );
}

public void testCalculateAverageDaysSupply()
{
	int unitsInStock = 100;
	int vehicleSales26Weeks = 50;
	int vehicleSales12Weeks = 50;
	double weight26Weeks = 0.30;
	double weight12Weeks = 0.70;

	double daysSupply = calculator.calculateAverageDaysSupply( unitsInStock, weight26Weeks, weight12Weeks, vehicleSales26Weeks,
																vehicleSales12Weeks );

	assertEquals( "Should be 200.4", 200.4, daysSupply, 0.1 );
}

private Date createDate( int year, int month, int day )
{
	Calendar cal = Calendar.getInstance();
	cal.set( Calendar.YEAR, year );
	cal.set( Calendar.MONTH, month - 1 ); // need to minus one b/c month
											// starts at
	// 0.
	cal.set( Calendar.DATE, day );
	cal.set( Calendar.HOUR_OF_DAY, 0 );
	cal.set( Calendar.MINUTE, 0 );
	cal.set( Calendar.MILLISECOND, 0 );
	cal.set( Calendar.SECOND, 0 );
	return cal.getTime();
}

}
