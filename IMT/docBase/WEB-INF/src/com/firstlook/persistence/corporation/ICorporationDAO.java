package com.firstlook.persistence.corporation;

import java.util.Collection;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Corporation;
import com.firstlook.exception.ApplicationException;

public interface ICorporationDAO
{
public Corporation findByPk( int corporationId );

public Collection<Corporation> findByName( String name );

public Collection<Corporation> findCorporationExists( String name );

public Collection<Corporation> findAll();

public void saveOrUpdate( Corporation corporation );

public void save( Corporation corporation );

public Collection<Corporation> findByCorporationCodeMultipleOrderByCorporationCodeDescending(
        String partialDealerCode );

public Corporation findCorporationByRegionId( int regionId ) throws ApplicationException, DatabaseException;

}