package com.firstlook.persistence.corporation;

import java.util.ArrayList;
import java.util.Collection;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Corporation;
import com.firstlook.exception.ApplicationException;

public class MockCorporationDAO implements ICorporationDAO
{

private Corporation corporation;	

public MockCorporationDAO()
{
}

public MockCorporationDAO( Corporation corp )
{
	corporation = corp;
}

public Corporation findByPk( int corporationId )
{
	if ( corporation.getCorporationId().intValue() == corporationId )
	{
		return corporation;
	}
	else
	{
		return null;
	}
}

public Collection<Corporation> findByName( String name )
{
	if ( corporation.getName().equals( name ) )
	{
		Collection<Corporation> collection = new ArrayList<Corporation>();
		collection.add( corporation );
		return collection;
	}
	else
	{
		return null;
	}
}

public Collection<Corporation> findCorporationExists( String name )
{
	if ( corporation.getName().equals( name ) )
	{
		Collection<Corporation> collection = new ArrayList<Corporation>();
		collection.add( corporation );
		return collection;
	}
	else
	{
		return null;
	}
}

public Collection<Corporation> findAll()
{
	Collection<Corporation> collection = new ArrayList<Corporation>();
	collection.add( corporation );
	return collection;
}

public void saveOrUpdate( Corporation corporation )
{

}

public void save( Corporation corporation )
{

}

public Collection<Corporation> findByCorporationCodeMultipleOrderByCorporationCodeDescending( String partialDealerCode )
{
	if ( corporation.getName().equals( partialDealerCode ) )
	{
		Collection<Corporation> collection = new ArrayList<Corporation>();
		collection.add( corporation );
		return collection;
	}
	else
	{
		return null;
	}
}

public Corporation findCorporationByRegionId( int regionId ) throws ApplicationException, DatabaseException
{
	return corporation;
}

public Corporation getCorporation()
{
	return corporation;
}

public void setCorporation( Corporation corporation )
{
	this.corporation = corporation;
}
}