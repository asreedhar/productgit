package com.firstlook.persistence;

import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;


import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.inventory.FindGeneralGroupingRetriever;
import com.firstlook.report.ReportGrouping;
import com.firstlook.service.inventory.InventoryService_Legacy;

public class ReportGroupingHelper
{
	
private FindGeneralGroupingRetriever findGeneralGroupingRetriever;
private FindGroupingByDealerIdAndWeeksRetriever findGroupingByDealerIdAndWeeksRetriever;
private InventoryService_Legacy inventoryService_Legacy;

public List<ReportGrouping> findByDealerIdAndWeeks( int dealerId, int weeks, int forecast, int mileage, int inventoryType, boolean useVehicleTrim ) throws ApplicationException
{
	return findGroupingByDealerIdAndWeeksRetriever.findByDealerIdAndWeeks( dealerId, weeks, forecast, mileage, inventoryType, useVehicleTrim);
}

public ReportGrouping findByMakeModelOrTrimOrBodyStyle( String make, String model, String trim, int dealerGroupInclude, int dealerId,
														int weeks, int inventoryType ) throws ApplicationException
{
	// KL - called by Trade Analyzer, Trade Manager, Vehicle Analyzer. Was being
	// called by Performance Search but that is currently inactive. 10-22-2004
	ReportGrouping grouping = new ReportGrouping();

	if ( trim == null || trim.equals( "" ) )
	{
		trim = null;
	}

	com.firstlook.entity.ReportGrouping newReportGrouping = findGeneralGroupingRetriever.findByGroupingDescription( make, model, trim, dealerGroupInclude,
																												dealerId, weeks, inventoryType );
	try
	{
		PropertyUtils.copyProperties( grouping, newReportGrouping );
		if ( grouping != null )
		{
			int specificUnitsInStock = inventoryService_Legacy.retrieveUnitsInStockByDealerIdOrDealerGroup( dealerId, dealerGroupInclude, trim,
																										inventoryType, make, model );
			grouping.setUnitsInStock( specificUnitsInStock );

			return grouping;
		}
		else
		{
			return new ReportGrouping();
		}
	}
	catch ( Exception e )
	{
		e.printStackTrace();
	}
	return grouping;
}

public void setFindGeneralGroupingRetriever(FindGeneralGroupingRetriever findGeneralGroupingRetriever) {
	this.findGeneralGroupingRetriever = findGeneralGroupingRetriever;
}

public void setFindGroupingByDealerIdAndWeeksRetriever(
		FindGroupingByDealerIdAndWeeksRetriever findGroupingByDealerIdAndWeeksRetriever) {
	this.findGroupingByDealerIdAndWeeksRetriever = findGroupingByDealerIdAndWeeksRetriever;
}

public void setInventoryService_Legacy(InventoryService_Legacy inventoryService) {
	this.inventoryService_Legacy = inventoryService;
}

}
