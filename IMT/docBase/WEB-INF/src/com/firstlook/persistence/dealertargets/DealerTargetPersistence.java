package com.firstlook.persistence.dealertargets;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import biz.firstlook.commons.util.DateUtilFL;

import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.DealerTarget;
import com.firstlook.entity.Target;

public class DealerTargetPersistence
{

private static Logger logger = Logger.getLogger( DealerTargetPersistence.class );

public DealerTargetPersistence()
{
}

public Map retrieveTargetMap( String sectionName, int inventoryType, int dealerId )
{
    Map map = new HashMap();

    Collection collection = null;

    collection = IMTDatabaseUtil.instance().find(
                                                  "from com.firstlook.entity.DealerTarget dt "
                                                          + "where dt.target.targetSection.name = ? "
                                                          + "  and dt.target.targetSection.inventoryType = ? " + "  and dt.businessUnitId = ? "
                                                          + "  and dt.active = ?",
                                                  new Object[] { sectionName, new Integer( inventoryType ), new Integer( dealerId ),
                                                          new Boolean( true ) },
                                                  new Type[] { Hibernate.STRING, Hibernate.INTEGER, Hibernate.INTEGER, Hibernate.BOOLEAN } );

    Iterator iterator = collection.iterator();
    while ( iterator.hasNext() )
    {
        DealerTarget dt = (DealerTarget)iterator.next();
        map.put( dt.getTarget().getName(), dt );
    }

    return map;
}

public Collection findActiveByBusinessUnitId( int dealerId )
{
    Collection collection = null;

    collection = IMTDatabaseUtil.instance().find(
                                                  "from com.firstlook.entity.DealerTarget dt "
                                                          + "where dt.businessUnitId = ? " + "  and dt.active = ?",
                                                  new Object[] { new Integer( dealerId ), new Boolean( true ) },
                                                  new Type[] { Hibernate.INTEGER, Hibernate.BOOLEAN } );
    return collection;
}

public void setTarget( int dealerId, int targetCode, int value )
{
    DealerTarget dealerTarget = findActiveByBusinessUnitIdAndTargetCode( dealerId, targetCode );
    if ( dealerTarget != null )
    {
        if ( dealerTarget.getValue() != value )
        {
            Date startDate = DateUtils.truncate( dealerTarget.getStart(), Calendar.DAY_OF_MONTH );
            Date todayDate = DateUtils.truncate( new Date(), Calendar.DAY_OF_MONTH );

            if ( DateUtilFL.dateInMillisWithDST( startDate ) == DateUtilFL.dateInMillisWithDST( todayDate ) )
            {
                dealerTarget.setValue( value );

                IMTDatabaseUtil.instance().saveOrUpdate( dealerTarget );
            }
            else
            {
                dealerTarget.setActive( false );
                DealerTarget newDealerTarget = new DealerTarget();
                try
                {
                    PropertyUtils.copyProperties( newDealerTarget, dealerTarget );
                }
                catch ( Exception e )
                {
                    logger.error( "This is never supposed to happen", e );
                }
                newDealerTarget.setStart( new Date() );
                newDealerTarget.setActive( true );
                newDealerTarget.setValue( value );
                newDealerTarget.setId( null );

                IMTDatabaseUtil.instance().saveOrUpdate( dealerTarget );
                IMTDatabaseUtil.instance().save( newDealerTarget );
            }
        }
    }
    else
    {
        Target target = new Target();
        target.setCode( targetCode );

        DealerTarget newDealerTarget = new DealerTarget();
        newDealerTarget.setTarget( target );
        newDealerTarget.setBusinessUnitId( dealerId );
        newDealerTarget.setStart( new Date() );
        newDealerTarget.setActive( true );
        newDealerTarget.setValue( value );
        newDealerTarget.setId( null );

        IMTDatabaseUtil.instance().save( newDealerTarget );
    }
}

public DealerTarget findActiveByBusinessUnitIdAndTargetCode( int dealerId, int targetCode )
{
    Collection collection = IMTDatabaseUtil.instance().find(
                                                             "from com.firstlook.entity.DealerTarget dt "
                                                                     + "where dt.businessUnitId = ? " + "  and dt.active = ? "
                                                                     + "  and dt.target.code = ?",
                                                             new Object[] { new Integer( dealerId ), new Boolean( true ),
                                                                     new Integer( targetCode ) },
                                                             new Type[] { Hibernate.INTEGER, Hibernate.BOOLEAN, Hibernate.INTEGER } );

    DealerTarget dealerTarget = null;
    Iterator iterator = collection.iterator();
    if ( iterator.hasNext() )
    {
        dealerTarget = (DealerTarget)iterator.next();
    }
    return dealerTarget;
}

}
