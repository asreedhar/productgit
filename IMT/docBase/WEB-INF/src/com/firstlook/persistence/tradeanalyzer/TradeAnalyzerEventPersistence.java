package com.firstlook.persistence.tradeanalyzer;

import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.TradeAnalyzerEvent;

public class TradeAnalyzerEventPersistence implements
        ITradeAnalyzerEventPersistence
{

public TradeAnalyzerEventPersistence()
{
    super();
}

public TradeAnalyzerEvent findByPk( Integer tradeAnalyzerEventId )
{
    TradeAnalyzerEvent tradeAnalyzerEvent = null;
    tradeAnalyzerEvent = (TradeAnalyzerEvent) IMTDatabaseUtil.instance().load(
            TradeAnalyzerEvent.class, tradeAnalyzerEventId);

    return tradeAnalyzerEvent;

}

public void save( TradeAnalyzerEvent tradeAnalzyerEvent )
{
    IMTDatabaseUtil.instance().save(tradeAnalzyerEvent);
}

public void delete( TradeAnalyzerEvent tradeAnalzyerEvent )
{
    IMTDatabaseUtil.instance().delete(tradeAnalzyerEvent);
}

}