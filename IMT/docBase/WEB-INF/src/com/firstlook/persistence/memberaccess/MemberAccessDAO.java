package com.firstlook.persistence.memberaccess;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.firstlook.entity.MemberAccess;

public class MemberAccessDAO extends HibernateDaoSupport implements IMemberAccessDAO
{

public MemberAccessDAO()
{
}

public Collection findByBusinessUnitId( int businessUnitId )
{
	return getAutoFlushHibernateTemplate().find( "from com.firstlook.entity.MemberAccess " + " where businessUnitId = ? ",
													new Integer( businessUnitId ) );
}

public Collection findByMemberId( int memberId )
{
	return getAutoFlushHibernateTemplate().find( "from com.firstlook.entity.MemberAccess " + " where memberId = ? ", new Integer( memberId ) );
}

public void deleteByMemberId( int memberId )
{
	Collection memberAccessCol = findByMemberId( memberId );
	Iterator memberAccessIt = memberAccessCol.iterator();

	while ( memberAccessIt.hasNext() )
	{
		getAutoFlushHibernateTemplate().delete( memberAccessIt.next() );
	}

}

public void deleteByBusinessUnitId( int businessUnitId )
{
	Collection memberAccessCol = findByBusinessUnitId( businessUnitId );
	Iterator memberAccessIt = memberAccessCol.iterator();

	while ( memberAccessIt.hasNext() )
	{
		delete( (MemberAccess)memberAccessIt.next() );
	}
}

public void delete( MemberAccess memberAccess )
{
	getAutoFlushHibernateTemplate().delete( memberAccess );
}

public void save( MemberAccess memberAccess )
{
	getAutoFlushHibernateTemplate().save( memberAccess );
}

public void saveOrUpdate( MemberAccess memberAccess )
{
	getAutoFlushHibernateTemplate().saveOrUpdate( memberAccess );
}

public void deleteByMemberAndBusinessUnit( int memberId, int businessUnitId )
{
	MemberAccess memberAccess = findByMemberAndBusinessUnit( memberId, businessUnitId );
	if ( memberAccess != null )
	{
		getAutoFlushHibernateTemplate().delete( memberAccess );
	}
}

public MemberAccess findByMemberAndBusinessUnit( int memberId, int businessUnitId )
{
	List results = getAutoFlushHibernateTemplate().find( "from com.firstlook.entity.MemberAccess where businessUnitId = ? and memberId = ?",
															new Object[] { new Integer( businessUnitId ), new Integer( memberId ) } );

	if ( !results.isEmpty() )
	{
		return (MemberAccess)results.get( 0 );
	}
	else
	{
		return null;
	}
}

private HibernateTemplate getAutoFlushHibernateTemplate()
{
	HibernateTemplate hibernateTemplate = super.getHibernateTemplate();
	hibernateTemplate.setFlushMode( HibernateAccessor.FLUSH_EAGER );
	return hibernateTemplate;
}

public Collection retrieveBusinessUnitCodesByMemberId( Integer memberId )
{
	StringBuffer hqlQuery = new StringBuffer();
	hqlQuery.append( "select buc from com.firstlook.entity.BusinessUnitCode as buc " );
	hqlQuery.append( " , com.firstlook.entity.MemberAccess ma " );
	hqlQuery.append( " where ma.memberId = :memberId " );
	hqlQuery.append( " and ma.businessUnitId = buc.businessUnitId " );

	Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
	Query query = session.createQuery( hqlQuery.toString() );
	query.setParameter( "memberId", memberId );

	return query.list();
}

}
