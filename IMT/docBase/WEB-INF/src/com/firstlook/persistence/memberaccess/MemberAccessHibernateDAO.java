package com.firstlook.persistence.memberaccess;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.firstlook.entity.MemberAccess;

public class MemberAccessHibernateDAO extends HibernateDaoSupport implements IMemberAccessDAO {

	public MemberAccessHibernateDAO() {}
	
	public Collection findByBusinessUnitId( int businessUnitId ) {
		return getHibernateTemplate().find(
	            "from com.firstlook.entity.MemberAccess "
	                    + " where businessUnitId = ? ", new Integer(businessUnitId) );
	}

	public Collection findByMemberId( int memberId ) {
		return getHibernateTemplate().find(
	            "from com.firstlook.entity.MemberAccess " + " where memberId = ? ",
	            new Integer(memberId) );
	}

	public void deleteByMemberId( int memberId )
	{
	    Collection memberAccessCol = findByMemberId(memberId);
	    Iterator memberAccessIt = memberAccessCol.iterator();

	    while (memberAccessIt.hasNext())
	    {
	        getHibernateTemplate().delete(memberAccessIt.next());
	    }

	}

	public void deleteByBusinessUnitId( int businessUnitId )
	{
	    Collection memberAccessCol = findByBusinessUnitId(businessUnitId);
	    Iterator memberAccessIt = memberAccessCol.iterator();

	    while (memberAccessIt.hasNext())
	    {
	        delete((MemberAccess) memberAccessIt.next());
	    }
	}

	public void delete( MemberAccess memberAccess )
	{
	    getHibernateTemplate().delete(memberAccess);
	}

	public void save( MemberAccess memberAccess )
	{
	    getHibernateTemplate().save(memberAccess);
	}

	public void saveOrUpdate( MemberAccess memberAccess )
	{
		getHibernateTemplate().saveOrUpdate( memberAccess );
	}

	public void deleteByMemberAndBusinessUnit(int memberId, int businessUnitId) {
		MemberAccess memberAccess = findByMemberAndBusinessUnit( memberId, businessUnitId );
		if( memberAccess != null ) {
			getHibernateTemplate().delete( memberAccess );
		}
	}

	public MemberAccess findByMemberAndBusinessUnit(int memberId, int businessUnitId) {
		List results = getHibernateTemplate().find( "from com.firstlook.entity.MemberAccess where businessUnitId = ? and memberId = ?",
				new Object[] { new Integer( businessUnitId ), new Integer( memberId ) } );
		
		if( !results.isEmpty() ) {
			return (MemberAccess) results.get(0);
		} else {
			return null;
		}
	}

	public Collection retrieveBusinessUnitCodesByMemberId( Integer memberId )
	{
		return null;
	}

}

