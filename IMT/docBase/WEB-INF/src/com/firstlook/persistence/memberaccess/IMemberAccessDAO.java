package com.firstlook.persistence.memberaccess;

import java.util.Collection;

import com.firstlook.entity.MemberAccess;

public interface IMemberAccessDAO {

	public Collection findByBusinessUnitId( int businessUnitId );
	public Collection findByMemberId( int memberId );
	public MemberAccess findByMemberAndBusinessUnit( int memberId, int businessUnitId );
	public void deleteByMemberId( int memberId );
	public void deleteByBusinessUnitId( int businessUnitId );
	public void deleteByMemberAndBusinessUnit( int memberId, int businessUnitId );
	public void delete( MemberAccess memberAccess );
	public void save( MemberAccess memberAccess );
	public void saveOrUpdate( MemberAccess memberAccess );
	public Collection retrieveBusinessUnitCodesByMemberId( Integer memberId );

}