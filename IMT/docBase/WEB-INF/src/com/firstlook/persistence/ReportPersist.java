package com.firstlook.persistence;


import com.firstlook.entity.InventorySalesAggregate;
import com.firstlook.persistence.inventory.InventorySalesAggregateService;
import com.firstlook.report.Report;

public class ReportPersist
{

private InventorySalesAggregateService inventorySalesAggregateService;

public ReportPersist()
{
}

public void findAllAverages( Report report, int dealerId, int weeks, int forecast, int inventoryType )
{
	report.setWeeks( weeks );
	
	InventorySalesAggregate aggregate = inventorySalesAggregateService.retrieveCachedInventorySalesAggregate( dealerId, weeks, forecast, inventoryType );
	
	if( aggregate != null )
	{
		populateReportWithAggregateData( report, aggregate );
	}
}

private void populateReportWithAggregateData( Report report, InventorySalesAggregate aggregate )
{
	report.setAverageBackEnd( new Integer( aggregate.getRetailAvgBackEnd() ) );
	report.setAverageFrontEnd( new Integer( aggregate.getRetailAGP() ) );

	if ( report.getAverageBackEnd() == null )
	{
		report.setAverageBackEnd( new Integer( 0 ) );
	}
	if ( report.getAverageFrontEnd() == null )
	{
		report.setAverageFrontEnd( new Integer( 0 ) );
	}

	report.setAverageTotalGross( new Integer( report.getAverageBackEnd().intValue() + report.getAverageFrontEnd().intValue() ) );

	// this is actually total units sold; we should change the names in SP
	// and Java code to be less confusing - kmm
	report.setUnitsSoldTotalAvg( aggregate.getRetailUnitsSold() );

	report.setGrossProfitTotalAvg( new Integer( aggregate.getRetailAGP() ) );
	report.setDaysToSaleTotalAvg( new Integer( aggregate.getRetailAvgDaysToSale() ) );
	report.setUnitsInStockTotalAvg( aggregate.getUnitsInStock() );
	report.setTotalSalesPrice( new Integer( aggregate.getRetailTotalSalesPrice() ) );
	report.setMileageTotalAvg( new Integer( aggregate.getRetailAvgMileage() ) );
	report.setNoSales( aggregate.getNoSaleUnits() );
	report.setTotalInventoryDollars( aggregate.getTotalInventoryDollars() );
	report.setTotalRevenue( aggregate.getRetailTotalRevenue() );
	report.setTotalBackEnd( aggregate.getRetailTotalBackEnd() );
	report.setTotalFrontEnd( aggregate.getRetailTotalFrontEnd() );
	report.setTotalGrossMargin( new Integer( report.getTotalFrontEnd() + report.getTotalBackEnd() ) );

}

public void setInventorySalesAggregateService( InventorySalesAggregateService inventorySalesAggregateService )
{
	this.inventorySalesAggregateService = inventorySalesAggregateService;
}



}
