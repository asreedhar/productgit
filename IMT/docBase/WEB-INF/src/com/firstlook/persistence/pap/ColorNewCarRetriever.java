package com.firstlook.persistence.pap;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.commons.sql.SqlParameterWithValue;
import biz.firstlook.commons.sql.StoredProcedureTemplate;
import biz.firstlook.commons.sql.StoredProcedureTemplateParameters;

import com.firstlook.entity.InventoryEntity;
import com.firstlook.report.PAPReportLineItem;

public class ColorNewCarRetriever extends StoredProcedureTemplate
{

public Collection<PAPReportLineItem> retrieveColorLineItems( int businessUnitId, int weeks,
        String make, String model, String vehicleTrim, int bodyTypeId,
        int forecast )
{
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); //faster inserts
	parameters.add( new SqlParameterWithValue( "@businessUnitId", Types.INTEGER,  new Integer(businessUnitId) ) );
	parameters.add( new SqlParameterWithValue( "@weeks", Types.INTEGER, new Integer(weeks) ) );
	parameters.add( new SqlParameterWithValue( "@make", Types.VARCHAR, make ) );
	parameters.add( new SqlParameterWithValue( "@model", Types.VARCHAR, model) );
	parameters.add( new SqlParameterWithValue( "@vehicleTrim", Types.VARCHAR, vehicleTrim ) );
	parameters.add( new SqlParameterWithValue( "@bodyTypeId", Types.INTEGER,  new Integer(bodyTypeId) ) );
	parameters.add( new SqlParameterWithValue( "@forecast", Types.INTEGER, new Integer(forecast) ) );
	parameters.add( new SqlParameterWithValue( "@groupingParam", Types.VARCHAR, PAPReportLineItem.COLOR_GROUPING_PARAMETER ) );
	parameters.add( new SqlParameterWithValue( "@inventoryType", Types.INTEGER, new Integer(InventoryEntity.NEW_CAR) ) );

	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();

	sprocRetrieverParams.setStoredProcedureCallString( "{? = call usp_getPerformanceAnalyzerNewCar (?, ?, ?, ?, ?, ?, ?, ?, ?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );

	Map results = this.call( sprocRetrieverParams, new ColorNewCarMapper() );
	LinkedList<PAPReportLineItem> resultsList = (LinkedList<PAPReportLineItem>)results.get( StoredProcedureTemplate.RESULT_SET );
	return ( resultsList != null && !resultsList.isEmpty()) ? resultsList : new ArrayList<PAPReportLineItem>();
}

private class ColorNewCarMapper implements RowMapper
{
	public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
	{
        PAPReportLineItem item = new PAPReportLineItem();
        item.setAvgMileage((Integer) rs.getObject("AverageMileage"));
        item.setGroupingColumn(rs.getString("groupingColumn"));
        item.setUnitsSold(rs.getInt("UnitsSold"));
        item.setAvgGrossProfit((Integer) rs.getObject("AverageGrossProfit"));
        item.setAvgDaysToSale((Integer) rs.getObject("AverageDays"));
        item.setUnitsInStock(rs.getInt("UnitsInStock"));
        item.setNoSales(rs.getInt("No_Sales"));
        item.setTotalGrossMargin(rs.getInt("TotalGrossMargin"));
        item.setTotalRevenue(rs.getInt("TotalRevenue"));
        item.setTotalInventoryDollars(rs.getInt("TotalInventoryDollars"));
        item.setTotalBackEnd(rs.getInt("TotalBackEndGross"));
        item.setAverageBackEnd((Integer) rs.getObject("AverageBackEndGrossProfit"));
        item.setTotalFrontEndGrossProfitUnrounded(new Double(rs.getDouble("TotalFrontEndGross")));
        item.setTotalBackEndGrossProfitUnrounded(new Double(rs.getDouble("TotalBackEndGross")));
        item.setTotalDaysToSaleUnrounded(new Double(rs.getDouble("TotalDays")));
        return item;
	}
}

}
