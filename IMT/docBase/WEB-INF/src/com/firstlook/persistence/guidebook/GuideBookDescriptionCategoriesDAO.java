package com.firstlook.persistence.guidebook;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;

import com.firstlook.data.RuntimeDatabaseException;
import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.GuideBookDescriptionCategories;

public class GuideBookDescriptionCategoriesDAO implements
        IGuideBookDescriptionCategoriesDAO
{

public GuideBookDescriptionCategoriesDAO()
{
}

public List findByGuideBookDescriptionId( Integer id )
{
    Session session = null;
    try
    {
        session = IMTDatabaseUtil.instance().retrieveSession();
        Criteria criteria = session
                .createCriteria(GuideBookDescriptionCategories.class);
        criteria.add(Expression.eq("guideBookDescriptionId", id));
        return criteria.list();
    } catch (HibernateException e)
    {
        throw new RuntimeDatabaseException(
                "Error retrieving GuideBookDescriptionCategories by GuideBookDescription id",
                e);
    } finally
    {
        IMTDatabaseUtil.instance().closeSession(session);
    }
}

}
