package com.firstlook.persistence.guidebook;

import java.util.ArrayList;
import java.util.List;

import com.firstlook.entity.GuideBookDescriptionCategories;

public class MockGuideBookDescriptionCategoriesPersistence implements
        IGuideBookDescriptionCategoriesDAO
{

private List guideBookDescriptionCategoriesList = new ArrayList();

public MockGuideBookDescriptionCategoriesPersistence()
{
}

public void add( GuideBookDescriptionCategories cat )
{
    guideBookDescriptionCategoriesList.add(cat);
}

public List findByGuideBookDescriptionId( Integer id )
{
    return guideBookDescriptionCategoriesList;
}

}
