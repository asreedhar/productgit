package com.firstlook.persistence.businessunit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.firstlook.data.RuntimeDatabaseException;
import com.firstlook.entity.BusinessUnitRelationship;

public class BusinessUnitRelationshipDAO extends HibernateDaoSupport implements IBusinessUnitRelationshipDAO
{

public BusinessUnitRelationshipDAO()
{
}

public BusinessUnitRelationship findByBusinessUnitId( Integer businessUnitId )
{
	Collection collection = getHibernateTemplate().find( "from com.firstlook.entity.BusinessUnitRelationship " + " where businessUnitId = ? ",
															new Object[] { businessUnitId } );
	if ( collection.size() > 0 )
	{
		return (BusinessUnitRelationship)collection.iterator().next();
	}
	else
	{
		return null;
	}
}

public BusinessUnitRelationship associateBusinessUnits( int parentId, int childId )
{
	BusinessUnitRelationship newRelation = new BusinessUnitRelationship();
	newRelation.setBusinessUnitId( new Integer( childId ) );
	newRelation.setParentId( new Integer( parentId ) );
	try
	{
		saveOrUpdate( newRelation );
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Error associating business units", e );
	}
	return newRelation;
}

public BusinessUnitRelationship updateParentIdForBusinessUnitId( int childId, int newParentId )
{
	BusinessUnitRelationship curRelation = findByBusinessUnitId( new Integer( childId ) );
	if ( curRelation.getParentId().intValue() != newParentId )
	{
		BusinessUnitRelationship newRelation = new BusinessUnitRelationship();
		newRelation.setBusinessUnitId( curRelation.getBusinessUnitId() );
		newRelation.setParentId( new Integer( newParentId ) );
		delete( curRelation );
		save( newRelation );
	}

	return curRelation;
}

public Collection findByParentId( Integer parentId )
{
	Collection collection = getHibernateTemplate().find( "from com.firstlook.entity.BusinessUnitRelationship " + " where parentId = ? ",
															new Object[] { parentId } );
	return collection;
}

public Collection findIdsByParentId( Integer parentId )
{
	Collection idList = new ArrayList();

	Collection topMostRelationships = findByParentId( parentId );

	if ( topMostRelationships != null && topMostRelationships.size() > 0 )
	{
		Iterator iter = topMostRelationships.iterator();
		while ( iter.hasNext() )
		{
			idList.add( ( (BusinessUnitRelationship)iter.next() ).getBusinessUnitId() );
		}
	}
	return idList;
}

public void delete( BusinessUnitRelationship rel )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
    getHibernateTemplate().delete( rel );
}

public boolean exists( BusinessUnitRelationship businessUnitRelationship )
{
	Collection ids = findIdsByParentId( businessUnitRelationship.getParentId() );
	boolean found = false;
	
	Iterator it = ids.iterator();
	while ( it.hasNext() )
	{
		found = ((Integer)it.next()).equals( businessUnitRelationship.getBusinessUnitId() );
		if ( found )
			break;
	}
	return found;
}

public void saveOrUpdate( BusinessUnitRelationship businessUnitRelationship )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );

	if ( exists(businessUnitRelationship) )
        getHibernateTemplate().update( businessUnitRelationship );
	else
        getHibernateTemplate().save( businessUnitRelationship );
}

public void save( BusinessUnitRelationship businessUnitRelationship )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
    getHibernateTemplate().save( businessUnitRelationship );
}

}
