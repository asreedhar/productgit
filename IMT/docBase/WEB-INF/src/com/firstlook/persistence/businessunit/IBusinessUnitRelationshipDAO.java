package com.firstlook.persistence.businessunit;

import java.util.Collection;

import com.firstlook.entity.BusinessUnitRelationship;

public interface IBusinessUnitRelationshipDAO
{
public BusinessUnitRelationship findByBusinessUnitId( Integer businessUnitId );

public BusinessUnitRelationship associateBusinessUnits( int parentId,
        int childId );

public BusinessUnitRelationship updateParentIdForBusinessUnitId( int childId,
        int newParentId );

public Collection findByParentId( Integer parentId );

public Collection findIdsByParentId( Integer parentId );

public void delete( BusinessUnitRelationship rel );

public void saveOrUpdate( BusinessUnitRelationship businessUnitRelationship );

public void save( BusinessUnitRelationship businessUnitRelationship );
}