package com.firstlook.persistence.businessunit;

import java.util.Collection;

import com.firstlook.data.DatabaseException;

public interface IBusinessUnitCodePersistence
{
public Collection findBusinessUnitCode( String name ) throws DatabaseException;
}
