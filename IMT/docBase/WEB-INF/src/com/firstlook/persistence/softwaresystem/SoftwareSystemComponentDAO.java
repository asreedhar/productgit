package com.firstlook.persistence.softwaresystem;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.firstlook.entity.SoftwareSystemComponent;

public class SoftwareSystemComponentDAO extends HibernateDaoSupport {

	public SoftwareSystemComponentDAO() {
		super();
	}
	
	@SuppressWarnings("unchecked")
	public SoftwareSystemComponent findByToken(String token) {
		StringBuffer hql = new StringBuffer();
		hql.append(" select c");
		hql.append(" from");
		hql.append(" com.firstlook.entity.SoftwareSystemComponent c");
		hql.append(" where");
		hql.append(" c.token = ?");
		List<SoftwareSystemComponent> components = (List<SoftwareSystemComponent>) getHibernateTemplate().find(
				hql.toString(),
				new Object[] { token } );
		SoftwareSystemComponent component = null;
		if (!components.isEmpty()) {
			if (components.size() != 1) {
				throw new IllegalStateException(String.format("Non-unique component: {0} {1} rows", token, components.size()));
			}
			else {
				component = components.get(0);
			}
		}
		return component;
	}
}
