package com.firstlook.persistence.softwaresystem;

import java.util.List;

import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.firstlook.entity.SoftwareSystemComponentState;

public class SoftwareSystemComponentStateDAO extends HibernateDaoSupport {

	public SoftwareSystemComponentStateDAO() {
		super();
	}

	@SuppressWarnings("unchecked")
	public SoftwareSystemComponentState findByLoginAndToken(String login, String token) {
		StringBuffer hql = new StringBuffer();
		hql.append("   select s");
		hql.append("\n from");
		hql.append("\n com.firstlook.entity.SoftwareSystemComponentState as s");
		hql.append("\n inner join s.softwareSystemComponent as c");
		hql.append("\n inner join s.authorizedMember as m");
		hql.append("\n where");
		hql.append("\n m.login = ?");
		hql.append("\n and c.token = ?");
		List<SoftwareSystemComponentState> stateItems = (List<SoftwareSystemComponentState>) getHibernateTemplate().find(
				hql.toString(),
				new Object[] { login, token } );
		SoftwareSystemComponentState state = null;
		if (!stateItems.isEmpty()) {
			if (stateItems.size() != 1) {
				throw new IllegalStateException(String.format("Non-unique state: [{0},{1}] {2} rows", login, token, stateItems.size()));
			}
			else {
				state = stateItems.get(0);
			}
		}
		return state;
	}
	
	public void saveOrUpdate(SoftwareSystemComponentState state) {
		getHibernateTemplate().setFlushMode(HibernateAccessor.FLUSH_EAGER);
		getHibernateTemplate().saveOrUpdate(state);
	}
}
