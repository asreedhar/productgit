package com.firstlook.persistence.pricepoints;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.commons.date.BasisPeriod;
import biz.firstlook.commons.sql.SqlParameterWithValue;
import biz.firstlook.commons.sql.StoredProcedureTemplate;
import biz.firstlook.commons.sql.StoredProcedureTemplateParameters;

import com.firstlook.report.PAPReportLineItem;
import com.firstlook.service.pricepoints.AbstractUnitCostPointArguments;
import com.firstlook.service.pricepoints.UnitCostPointArguments;

public class PricePointDetailRetriever extends StoredProcedureTemplate implements ICostPointRetriever
{

protected Object processResult( ResultSet rs ) throws SQLException
{
	PAPReportLineItem item = new PAPReportLineItem();
	if ( rs.next() )
	{
		item.setUnitsSold( rs.getInt( "UnitsSold" ) );
		item.setAvgGrossProfit( (Integer)rs.getObject( "AverageGrossProfit" ) );
		item.setAvgDaysToSale( (Integer)rs.getObject( "AverageDays" ) );
		item.setUnitsInStock( rs.getInt( "UnitsInStock" ) );
		item.setNoSales( rs.getInt( "NoSales" ) );
		item.setTotalGrossMargin( rs.getInt( "TotalGrossMargin" ) );
		item.setTotalInventoryDollars( rs.getInt( "TotalInventoryDollars" ) );
		item.setTotalRevenue( rs.getInt( "TotalRevenue" ) );
		item.setTotalBackEnd( rs.getInt( "TotalBackEnd" ) );
		item.setAverageBackEnd( (Integer)rs.getObject( "AverageBackEnd" ) );
		item.setTotalFrontEndGrossProfitUnrounded( new Double( rs.getDouble( "TotalFrontEndGross" ) ) );
		item.setTotalBackEndGrossProfitUnrounded( new Double( rs.getDouble( "TotalBackEndGross" ) ) );
		item.setTotalDaysToSaleUnrounded( new Double( rs.getDouble( "TotalDays" ) ) );
	}
	return item;
}

public PAPReportLineItem retrievePricePoint( AbstractUnitCostPointArguments unitCostPointArguments, int lowValue, int highValue )
{
	BasisPeriod basisPeriod = new BasisPeriod( unitCostPointArguments.getWeeks() * 7, unitCostPointArguments.getForecast() == 1 ? true : false );
	Timestamp startDate = new Timestamp(basisPeriod.getStartDate().getTime());
	Timestamp endDate = new Timestamp(basisPeriod.getEndDate().getTime());

	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); //faster inserts
	parameters.add( new SqlParameterWithValue( "@businessUnitId", Types.INTEGER, new Integer( unitCostPointArguments.getDealerId() ) ) );
	parameters.add( new SqlParameterWithValue( "@groupingDescriptionId", Types.INTEGER, new Integer( ( (UnitCostPointArguments)unitCostPointArguments ).getGroupingDescriptionId() )) );
	parameters.add( new SqlParameterWithValue( "@startDate", Types.TIMESTAMP, startDate ) );
	parameters.add( new SqlParameterWithValue( "@endDate", Types.TIMESTAMP, endDate ) );
	parameters.add( new SqlParameterWithValue( "@mileage", Types.INTEGER, new Integer( ( (UnitCostPointArguments)unitCostPointArguments ).getMileage() ) ) );
	parameters.add( new SqlParameterWithValue( "@inventoryType", Types.INTEGER, new Integer( unitCostPointArguments.getInventoryType() ) ) );
	parameters.add( new SqlParameterWithValue( "@lowValue", Types.INTEGER, new Integer( lowValue ) ) );
	parameters.add( new SqlParameterWithValue( "@highValue", Types.INTEGER, new Integer( highValue ) ) );

	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();
	sprocRetrieverParams.setStoredProcedureCallString( "{? = call GetPricePoints (?, ?, ?, ?, ?, ?, ?, ?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );

	Map results = this.call( sprocRetrieverParams, new PricePointDetailMapper() );

	LinkedList<PAPReportLineItem> resultsList = (LinkedList<PAPReportLineItem>)results.get( StoredProcedureTemplate.RESULT_SET );
	return ( resultsList != null && !resultsList.isEmpty()) ? resultsList.get(0) : new PAPReportLineItem();
}


private class PricePointDetailMapper implements RowMapper
{
	public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
	{
		PAPReportLineItem item = new PAPReportLineItem();
		item.setUnitsSold( rs.getInt( "UnitsSold" ) );
		item.setAvgGrossProfit( (Integer)rs.getObject( "AverageGrossProfit" ) );
		item.setAvgDaysToSale( (Integer)rs.getObject( "AverageDays" ) );
		item.setUnitsInStock( rs.getInt( "UnitsInStock" ) );
		item.setNoSales( rs.getInt( "NoSales" ) );
		item.setTotalGrossMargin( rs.getInt( "TotalGrossMargin" ) );
		item.setTotalInventoryDollars( rs.getInt( "TotalInventoryDollars" ) );
		item.setTotalRevenue( rs.getInt( "TotalRevenue" ) );
		item.setTotalBackEnd( rs.getInt( "TotalBackEnd" ) );
		item.setAverageBackEnd( (Integer)rs.getObject( "AverageBackEnd" ) );
		item.setTotalFrontEndGrossProfitUnrounded( new Double( rs.getDouble( "TotalFrontEndGross" ) ) );
		item.setTotalBackEndGrossProfitUnrounded( new Double( rs.getDouble( "TotalBackEndGross" ) ) );
		item.setTotalDaysToSaleUnrounded( new Double( rs.getDouble( "TotalDays" ) ) );
		return item;
	}

}


}
