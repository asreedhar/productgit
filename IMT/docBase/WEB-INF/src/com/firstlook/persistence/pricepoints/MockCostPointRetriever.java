package com.firstlook.persistence.pricepoints;

import java.util.Map;
import java.util.TreeMap;

import junit.framework.AssertionFailedError;

import com.firstlook.report.BaseReportLineItemForm;
import com.firstlook.report.PAPReportLineItem;
import com.firstlook.service.pricepoints.AbstractUnitCostPointArguments;
import com.firstlook.service.pricepoints.UnitCostPointService;

public class MockCostPointRetriever implements ICostPointRetriever
{

private Map papReportLineItemsMap;

public MockCostPointRetriever()
{
	super();
	papReportLineItemsMap = new TreeMap();
}

public void clearMap()
{
	papReportLineItemsMap = new TreeMap();
}

public void addCostPoint( int lowValue, int highValue, boolean lastPricePoint )
{
	PAPReportLineItem papReportLineItem = new PAPReportLineItem();
	boolean firstPricePoint = false;
	if ( papReportLineItemsMap.isEmpty() )
	{
		firstPricePoint = true;
	}

	UnitCostPointService service = new UnitCostPointService();
	service.setGroupingColumn( papReportLineItem, firstPricePoint, lastPricePoint, highValue, lowValue );
	papReportLineItemsMap.put( papReportLineItem.getGroupingColumn(), papReportLineItem );
}

public PAPReportLineItem retrievePricePoint( AbstractUnitCostPointArguments unitCostPointArguments, int lowValue, int highValue )
{
	String key = null;
	if ( lowValue == 0 )
	{
		key = "Under " + BaseReportLineItemForm.getFormattedPrice( highValue );
	}
	else if ( highValue == UnitCostPointService.HIGH_PRICE_POINT_VALUE )
	{
		key = "Over " + BaseReportLineItemForm.getFormattedPrice( lowValue );
	}
	else
	{
		key = BaseReportLineItemForm.getFormattedPrice( lowValue ) + " - " + BaseReportLineItemForm.getFormattedPrice( highValue );
	}

	PAPReportLineItem item = (PAPReportLineItem)papReportLineItemsMap.get( key );
	if ( item == null )
	{
		throw new AssertionFailedError( "The cost point " + lowValue + " - " + highValue + " could not be found in the mock database." );
	}
	return item;
}

public Map getPapReportLineItemsMap()
{
	return papReportLineItemsMap;
}

}
