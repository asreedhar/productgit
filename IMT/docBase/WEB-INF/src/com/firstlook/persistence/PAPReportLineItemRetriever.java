package com.firstlook.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.commons.date.BasisPeriod;
import biz.firstlook.commons.sql.SqlParameterWithValue;
import biz.firstlook.commons.sql.StoredProcedureTemplate;
import biz.firstlook.commons.sql.StoredProcedureTemplateParameters;

import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.MarketShare;
import com.firstlook.persistence.market.MarketShareDealsRetriever;
import com.firstlook.report.PAPReportLineItem;

public class PAPReportLineItemRetriever extends StoredProcedureTemplate
{

private MarketShareDealsRetriever marketShareDealsRetriever;	
private static final int RING_ID_ONE = 1;

public List findByDealerIdGroupingDescriptionIdWeeksGroupByColor( int dealerId,
        int groupingDescriptionId, int weeks, int forecast, int mileage,
        boolean hasMarketUpgrade, int lowerUnitCostThreshold )
{
    return retrievePerformanceAnalyzer(dealerId, groupingDescriptionId, weeks,
            forecast, PAPReportLineItem.COLOR_GROUPING_PARAMETER, mileage,
            hasMarketUpgrade, lowerUnitCostThreshold);
}

public List findByDealerIdGroupingDescriptionIdWeeksGroupByTrim( int dealerId,
        int groupingDescriptionId, int weeks, int forecast, int mileage,
        boolean hasMarketUpgrade, int lowerUnitCostThreshold )
{
    return retrievePerformanceAnalyzer(dealerId, groupingDescriptionId, weeks,
            forecast, PAPReportLineItem.TRIM_GROUPING_PARAMETER, mileage,
            hasMarketUpgrade, lowerUnitCostThreshold);
}

public List findByDealerIdGroupingDescriptionIdWeeksGroupByYear( int dealerId,
        int groupingDescriptionId, int weeks, int forecast, int mileage,
        boolean hasMarketUpgrade, int lowerUnitCostThreshold )
{
    return retrievePerformanceAnalyzer(dealerId, groupingDescriptionId, weeks,
            forecast, PAPReportLineItem.YEAR_GROUPING_PARAMETER, mileage,
            hasMarketUpgrade, lowerUnitCostThreshold);
}

public List retrievePerformanceAnalyzer( int dealerId,
        int groupingDescriptionId, int weeks, int forecast,
        String groupingParam, int mileage, boolean hasMarketUpgrade,
        int lowerUnitCostThreshold )
{
    BasisPeriod basisPeriod = new BasisPeriod(weeks * 7, forecast == 1 ? true
            : false);
    Timestamp startDate = new Timestamp(basisPeriod.getStartDate().getTime());
    Timestamp endDate = new Timestamp(basisPeriod.getEndDate().getTime());
    
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); //faster inserts
	parameters.add( new SqlParameterWithValue( "@dealerId", Types.INTEGER,  new Integer(dealerId) ) );
	parameters.add( new SqlParameterWithValue( "@lowerUnitCostThreshold", Types.INTEGER, new Integer(lowerUnitCostThreshold) ) );
	parameters.add( new SqlParameterWithValue( "@startDate", Types.TIMESTAMP, startDate) );
	parameters.add( new SqlParameterWithValue( "@endDate", Types.TIMESTAMP, endDate ) );
	parameters.add( new SqlParameterWithValue( "@groupingDescriptionId", Types.INTEGER,  new Integer(groupingDescriptionId) ) );
	parameters.add( new SqlParameterWithValue( "@groupingParam", Types.VARCHAR, groupingParam ) );
	parameters.add( new SqlParameterWithValue( "@mileage", Types.INTEGER, new Integer(mileage) ) );

	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();

	sprocRetrieverParams.setStoredProcedureCallString( "{? = call getPerformanceAnalyzer (?, ?, ?, ?, ?, ?, ?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );

	Map results = this.call( sprocRetrieverParams, new PAReportLineItemMapper() );

	LinkedList<PAPReportLineItem> resultsList = (LinkedList<PAPReportLineItem>)results.get( StoredProcedureTemplate.RESULT_SET );
	
    if ( groupingParam.equalsIgnoreCase("vehicleYear")
            && hasMarketUpgrade )
   {
       addCrossSellDataToResults(dealerId, groupingDescriptionId, startDate,
               endDate, initializeGroupingMap(resultsList), resultsList);
   }
	
	return ( resultsList != null && !resultsList.isEmpty()) ? resultsList : new ArrayList();    
}


private void addCrossSellDataToResults( int dealerId,
        int groupingDescriptionId, Date beginDate, Date endDate, HashMap map,
        Collection results )

{
    Collection crossSellCol = marketShareDealsRetriever.retrieveMarketShare(0,
            dealerId, RING_ID_ONE, groupingDescriptionId, new Timestamp(
                    beginDate.getTime()), new Timestamp(endDate.getTime()));
    if ( !crossSellCol.isEmpty() )
    {
        Iterator crossSellIt = crossSellCol.iterator();
        while (crossSellIt.hasNext())
        {
            MarketShare curMarketShare = (MarketShare) crossSellIt.next();
            PAPReportLineItem item = null;
            String curYear = String.valueOf(curMarketShare.getModelYear());
            if ( map.containsKey(curYear) )
            {
                item = (PAPReportLineItem) map.get(curYear);
            } else
            {
                item = new PAPReportLineItem();
                item.setGroupingColumn("" + curMarketShare.getModelYear());
                item.setNoValues(true);
                results.add(item);
            }
            item.setPercentageInMarket(new Double(curMarketShare
                    .getMarketShare()).floatValue());
        }
    }
}

private HashMap<String, PAPReportLineItem> initializeGroupingMap( Collection<PAPReportLineItem> results )
{
    HashMap<String, PAPReportLineItem> map = new HashMap<String, PAPReportLineItem>();
    Iterator<PAPReportLineItem> resultsIter = results.iterator();
    while (resultsIter.hasNext())
    {
        PAPReportLineItem item = (PAPReportLineItem) resultsIter.next();
        map.put(item.getGroupingColumn(), item);
    }

    return map;
}


private class PAReportLineItemMapper implements RowMapper
{

	public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
	{
	    PAPReportLineItem item = new PAPReportLineItem();
	    item.setAvgMileage((Integer) rs.getObject("AverageMileage"));
	    item.setGroupingColumn(rs.getString("groupingColumn"));
	    item.setUnitsSold(rs.getInt("UnitsSold"));
	    item.setAvgGrossProfit((Integer) rs.getObject("AverageGrossProfit"));
	    item.setAvgDaysToSale((Integer) rs.getObject("AverageDays"));
	    item.setUnitsInStock(rs.getInt("UnitsInStock"));
	    item.setNoSales(rs.getInt("NoSales"));
	    item.setTotalRevenue(rs.getInt("TotalRevenue"));
	    item.setTotalGrossMargin(rs.getInt("TotalGrossMargin"));
	    item.setTotalInventoryDollars(rs.getInt("TotalInventoryDollars"));
	    item.setAverageBackEnd((Integer) rs.getObject("AverageBackEnd"));
	    item.setTotalBackEnd(rs.getInt("TotalBackEndGross"));
	    item.setTotalFrontEndGrossProfitUnrounded(new Double(rs
	            .getDouble("TotalFrontEndGross")));
	    item.setTotalBackEndGrossProfitUnrounded(new Double(rs
	            .getDouble("TotalBackEndGross")));
	    item.setTotalDaysToSaleUnrounded(new Double(rs.getDouble("TotalDays")));
	    return item;
	}

}


public void setMarketShareDealsRetriever(
		MarketShareDealsRetriever marketShareDealsRetriever) {
	this.marketShareDealsRetriever = marketShareDealsRetriever;
}

}
