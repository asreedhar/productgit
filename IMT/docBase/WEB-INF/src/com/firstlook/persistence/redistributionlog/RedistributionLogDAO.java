package com.firstlook.persistence.redistributionlog;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.firstlook.entity.RedistributionLog;

public class RedistributionLogDAO extends HibernateDaoSupport
{
private TransactionTemplate transactionTemplate;

public void setTransactionTemplate( TransactionTemplate transactionTemplate )
{
	this.transactionTemplate = transactionTemplate;
}

public void save( final RedistributionLog redistributionLogEntry )
{
	
	
	transactionTemplate.execute( new TransactionCallbackWithoutResult()
	{
		@Override
		protected void doInTransactionWithoutResult( TransactionStatus arg0 )
		{
			getHibernateTemplate().save( redistributionLogEntry );
		}
	} );
}

}
