package com.firstlook.persistence.cia;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ReverseComparator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.cia.model.CIABodyTypeButton;
import biz.firstlook.commons.sql.SqlParameterWithValue;
import biz.firstlook.commons.sql.StoredProcedureTemplate;
import biz.firstlook.commons.sql.StoredProcedureTemplateParameters;
import biz.firstlook.transact.persist.model.Segment;

public class CIASegmentByContributionRetriever extends StoredProcedureTemplate 
{


public List retrieveCIABodyTypeDetails( int businessUnitId, boolean useNoSales, Timestamp basisPeriodDate, int ciaSummaryId )
{
	
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); //faster inserts
	parameters.add( new SqlParameterWithValue( "@businessUnitId", Types.INTEGER, businessUnitId) );
	parameters.add( new SqlParameterWithValue( "@includeNoSales", Types.BOOLEAN, useNoSales) );
	parameters.add( new SqlParameterWithValue( "@basisPeriodDate", Types.TIMESTAMP, basisPeriodDate) );
	parameters.add( new SqlParameterWithValue( "@ciaSummaryId", Types.INTEGER, ciaSummaryId) );

	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();

	sprocRetrieverParams.setStoredProcedureCallString( "{? = call GetDealerSegmentContribution(?, ?, ?, ?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );

	Map results = this.call( sprocRetrieverParams, new CIASegmentByContributionMapper() );
	LinkedList<CIABodyTypeButton> resultsList = (LinkedList<CIABodyTypeButton>)results.get( StoredProcedureTemplate.RESULT_SET );

	//sort and pack into ciaSegments
    return convertToCIASegments(resultsList);
}

private List<Segment> convertToCIASegments(LinkedList<CIABodyTypeButton> resultsList) {
	ReverseComparator comparator = new ReverseComparator( new BeanComparator( "percentContribution" ) );
    Collections.sort( resultsList, comparator );
    List<Segment> ciaSegments = new ArrayList<Segment>();
    Iterator<CIABodyTypeButton> buttons = resultsList.iterator();
    while ( buttons.hasNext() )
    {
    	CIABodyTypeButton ciaBodyTypeButton = (CIABodyTypeButton)buttons.next();
        ciaSegments.add( new Segment( ciaBodyTypeButton.getSegmentId().intValue(),
                                                      ciaBodyTypeButton.getSegment() ) );
    }
    return ciaSegments;
}

private class CIASegmentByContributionMapper implements RowMapper
{
	public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
	{
	    CIABodyTypeButton ciaBodyTypeButton = new CIABodyTypeButton();
	    ciaBodyTypeButton.setSegmentId( new Integer( rs.getInt( "SegmentId" ) ) );
	    ciaBodyTypeButton.setSegment( rs.getString( "Segment" ) );
	    ciaBodyTypeButton.setPercentContribution( new Double( rs.getDouble( "DealerContribution" ) ) );
	    return ciaBodyTypeButton;
	
	}
}


}
