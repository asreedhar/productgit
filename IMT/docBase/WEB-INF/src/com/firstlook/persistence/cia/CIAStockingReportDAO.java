package com.firstlook.persistence.cia;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.cia.model.CIACategory;
import biz.firstlook.cia.model.CIAGroupingItemDetailLevel;
import biz.firstlook.cia.model.CIAGroupingItemDetailType;
import biz.firstlook.cia.model.Status;

public class CIAStockingReportDAO extends HibernateDaoSupport implements ICIAStockingReportDAO
{

public CIAStockingReportDAO()
{
	super();
}

public List retrieveCIAGroupingItemsWithOptimalStockingLevel( Integer dealerId )
{
	String ciaCategoryIds = "( "
			+ CIACategory.POWERZONE_CATEGORY + ", " + CIACategory.WINNERS_CATEGORY + ", " + CIACategory.GOODBETS_CATEGORY + ") ";
	List toBeReturned = getHibernateTemplate().find(
														"select distinct ciaGroupingItem, bodytypeDetail.segment.segmentId "
																+ "from CIASummary summary "
																+ "	inner join summary.ciaBodyTypeDetails bodytypeDetail "
																+ "	inner join bodytypeDetail.ciaGroupingItems ciaGroupingItem "
																+ "	inner join ciaGroupingItem.ciaGroupingItemDetails groupingItemDetails"
																+ "	inner join ciaGroupingItem.groupingDescription groupingDescription"
																+ " where ( groupingItemDetails.ciaGroupingItemDetailLevel.ciaGroupingItemDetailLevelId = ? "
																+ " or groupingItemDetails.ciaGroupingItemDetailLevel.ciaGroupingItemDetailLevelId = ? ) "
																+ "	and groupingItemDetails.ciaGroupingItemDetailType.ciaGroupingItemDetailTypeId = ? and "
																+ "	summary.businessUnitId = ? and summary.status.statusId = ? "
																+ " and ciaGroupingItem.ciaCategory.ciaCategoryId IN " + ciaCategoryIds
																+ " order by ciaGroupingItem.ciaCategory.ciaCategoryId, ciaGroupingItem.valuation",
														new Object[] { new Integer( CIAGroupingItemDetailLevel.YEAR_DETAIL_LEVEL ),
																new Integer( CIAGroupingItemDetailLevel.GROUPING_DETAIL_LEVEL ),
																new Integer( CIAGroupingItemDetailType.OPTIMAL_STOCKING_LEVEL_TYPE ), dealerId,
																Status.CURRENT.getStatusId() } );

	return toBeReturned;
}

}
