package com.firstlook.persistence.cia;

import java.sql.SQLException;
import java.util.Collection;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.cia.model.CIAGroupingItemDetailType;

public class CIABuyingPlanReportDAO extends HibernateDaoSupport implements ICIABuyingPlanReportDAO
{

private final static String QUERY_W_SEGMENT;

static {
	StringBuilder querySegment = new StringBuilder();
	querySegment.append( " select distinct item, typeDetail.segment.segmentId ");
	querySegment.append( " from CIAGroupingItem as item, CIAGroupingItemDetail as itemDetail," );
	querySegment.append( " CIABodyTypeDetail as typeDetail, CIASummary as summary " );
	querySegment.append( " where summary.businessUnitId = :businessUnitId and summary.status.statusId = :statusId " );
	querySegment.append(" and itemDetail.ciaGroupingItemDetailType.ciaGroupingItemDetailTypeId = :ciaGroupingItemDetailTypeId ");
	querySegment.append(" and summary.ciaSummaryId = typeDetail.ciaSummaryId " );
	querySegment.append(" and item.ciaBodyTypeDetail.ciaBodyTypeDetailId = typeDetail.ciaBodyTypeDetailId ");
	querySegment.append(" and item.ciaGroupingItemId = itemDetail.ciaGroupingItem.ciaGroupingItemId ");
	querySegment.append(" order by item.ciaCategory.ciaCategoryId, item.valuation ");
	QUERY_W_SEGMENT = querySegment.toString();
}

@SuppressWarnings("unchecked")
public Collection<Object[]> findDistinctCIAGroupingItemsAndSegmentsWithBuyAmounts( final Integer businessUnitId, final Integer ciaSummaryStatusId )
{
	
	Collection<Object[]> items = (Collection<Object[]>)getHibernateTemplate().execute( new HibernateCallback(){
		public Object doInHibernate( Session session ) throws HibernateException, SQLException
		{
			Query query = session.createQuery( QUERY_W_SEGMENT );
			query.setInteger( "businessUnitId", businessUnitId );
			query.setInteger( "statusId", ciaSummaryStatusId );
			query.setInteger( "ciaGroupingItemDetailTypeId", CIAGroupingItemDetailType.BUY.getCiaGroupingItemDetailTypeId() );
			return query.list();
		}
		
	} );

	return items;
}

public Collection findDistinctCIAGroupingItemsWithBuyAmounts( Integer businessUnitId, Integer ciaSummaryStatusId )
{
    Collection items = getHibernateTemplate().find(
                                                    " select distinct item from CIAGroupingItem as item, CIAGroupingItemDetail as itemDetail,"
                                                            + " CIABodyTypeDetail as typeDetail, CIASummary as summary where"
                                                            + " summary.businessUnitId = ? and summary.status.statusId = ?"
                                                            + " and itemDetail.ciaGroupingItemDetailType.ciaGroupingItemDetailTypeId = ?"
                                                            + " and summary.ciaSummaryId = typeDetail.ciaSummaryId"
                                                            + " and item.ciaBodyTypeDetail.ciaBodyTypeDetailId = typeDetail.ciaBodyTypeDetailId"
                                                            + " and item.ciaGroupingItemId = itemDetail.ciaGroupingItem.ciaGroupingItemId"
                                                            + " order by item.ciaCategory.ciaCategoryId, item.valuation",
                                                    new Object[] { businessUnitId, ciaSummaryStatusId,
                                                            CIAGroupingItemDetailType.BUY.getCiaGroupingItemDetailTypeId() } );

    return items;

}

}
