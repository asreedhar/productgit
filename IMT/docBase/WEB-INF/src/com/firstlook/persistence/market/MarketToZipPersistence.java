package com.firstlook.persistence.market;

import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;

import com.firstlook.data.RuntimeDatabaseException;
import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.MarketToZip;

public class MarketToZipPersistence
{

public MarketToZipPersistence()
{
}

public Collection findByBusinessUnitIdAndRing( int businessUnitId, int ring )
{
    Session session = null;
    try
    {
        session = IMTDatabaseUtil.instance().retrieveSession();
        Criteria criteria = session.createCriteria(MarketToZip.class);
        criteria.add(Expression.eq("businessUnitId",
                new Integer(businessUnitId)));
        criteria.add(Expression.eq("ring", new Integer(ring)));

        return criteria.list();
    } catch (HibernateException e)
    {
        throw new RuntimeDatabaseException(
                "Error retrieving member access by composite key: ", e);
    } finally
    {
        IMTDatabaseUtil.instance().closeSession(session);
    }
}

public MarketToZip findByCK( int businessUnitId, int ring, String zipCode )
{
    Session session = null;
    try
    {
        session = IMTDatabaseUtil.instance().retrieveSession();
        Criteria criteria = session.createCriteria(MarketToZip.class);
        criteria.add(Expression.eq("businessUnitId",
                new Integer(businessUnitId)));
        criteria.add(Expression.eq("ring", new Integer(ring)));
        criteria.add(Expression.eq("zipCode", zipCode));
        List returnList = criteria.list();
        if ( returnList.size() > 0 )
        {
            return (MarketToZip) returnList.iterator().next();
        } else
        {
            return null;
        }
    } catch (HibernateException e)
    {
        throw new RuntimeDatabaseException(
                "Error retrieving market to zip by composite key: ", e);
    } finally
    {
        IMTDatabaseUtil.instance().closeSession(session);
    }
}

public void save( MarketToZip marketToZip )
{
    IMTDatabaseUtil.instance().save(marketToZip);
}

public void delete( MarketToZip marketToZip )
{
    IMTDatabaseUtil.instance().delete(marketToZip);
}

}
