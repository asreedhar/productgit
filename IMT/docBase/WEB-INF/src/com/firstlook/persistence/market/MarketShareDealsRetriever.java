package com.firstlook.persistence.market;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.commons.sql.SqlParameterWithValue;
import biz.firstlook.commons.sql.StoredProcedureTemplate;
import biz.firstlook.commons.sql.StoredProcedureTemplateParameters;

import com.firstlook.entity.MarketShare;

public class MarketShareDealsRetriever extends StoredProcedureTemplate
{


public Collection<MarketShare> retrieveMarketShare( int firstLookRegionId, int businessUnitId, int ringId, int groupingDescriptionId, Timestamp beginDate,
                                       Timestamp endDate )
{
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); //faster inserts
	parameters.add( new SqlParameterWithValue( "@firstLookRegionId", Types.INTEGER, new Integer( firstLookRegionId ) ) );
	parameters.add( new SqlParameterWithValue( "@businessUnitId", Types.INTEGER,  new Integer( businessUnitId ) ) );
	parameters.add( new SqlParameterWithValue( "@ringId", Types.INTEGER, new Integer( ringId ) ) );
	parameters.add( new SqlParameterWithValue( "@groupingDescriptionId", Types.INTEGER, new Integer( groupingDescriptionId )) );
	parameters.add( new SqlParameterWithValue( "@beginDate", Types.TIMESTAMP, new Timestamp(beginDate.getTime()) ) );
	parameters.add( new SqlParameterWithValue( "@endDate", Types.TIMESTAMP, new Timestamp(endDate.getTime() )) );

	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();

	sprocRetrieverParams.setStoredProcedureCallString( "{? = call [MARKET].[dbo].usp_GetCnt_Deals_MarketShare (?, ?, ?, ?, ?, ?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );

	Map results = this.call( sprocRetrieverParams, new MarketShareDealsMapper() );
	LinkedList<MarketShare> resultsList = (LinkedList<MarketShare>)results.get( StoredProcedureTemplate.RESULT_SET );
	return ( resultsList != null && !resultsList.isEmpty()) ? resultsList : new ArrayList<MarketShare>();
}


private class MarketShareDealsMapper implements RowMapper
{
	public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
	{
		MarketShare marketShare = new MarketShare();
		String value = rs.getString( "ModelYear" );
		if ( !value.equalsIgnoreCase( "TOTAL" ) )
		{
			marketShare = new MarketShare();
			marketShare.setModelYear( rs.getInt( "ModelYear" ) );
			marketShare.setNumDeals( rs.getInt( "Deals" ) );
			marketShare.setMarketShare( rs.getDouble( "MarketShare" ) );
		}
		return marketShare;
	}
}

}
