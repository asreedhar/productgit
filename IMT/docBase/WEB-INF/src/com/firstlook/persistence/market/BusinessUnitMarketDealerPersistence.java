package com.firstlook.persistence.market;

import java.util.Collection;
import java.util.Iterator;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.BusinessUnitMarketDealer;

public class BusinessUnitMarketDealerPersistence
{

public BusinessUnitMarketDealerPersistence()
{
}

public Collection findByBusinessUnitId( int businessUnitId )
{
    return IMTDatabaseUtil
            .instance()
            .find(
                    "from com.firstlook.entity.BusinessUnitMarketDealer where businessUnitId = ?",
                    new Object[]
                    { new Integer(businessUnitId) }, new Type[]
                    { Hibernate.INTEGER });
}

public void save( BusinessUnitMarketDealer businessUnitMarketDealer )
{
    IMTDatabaseUtil.instance().saveOrUpdate(businessUnitMarketDealer);
}

public void deleteByBusinessUnitId( int businessUnitId )
{
    Collection collection = findByBusinessUnitId(businessUnitId);
    Iterator iterator = collection.iterator();

    while (iterator.hasNext())
    {
        BusinessUnitMarketDealer marketDealer = (BusinessUnitMarketDealer) iterator
                .next();
        IMTDatabaseUtil.instance().delete(marketDealer);
    }
}

}
