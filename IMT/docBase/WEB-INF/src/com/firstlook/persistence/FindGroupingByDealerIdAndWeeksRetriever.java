package com.firstlook.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.commons.sql.SqlParameterWithValue;
import biz.firstlook.commons.sql.StoredProcedureTemplate;
import biz.firstlook.commons.sql.StoredProcedureTemplateParameters;

import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.report.ReportGrouping;
import com.firstlook.service.inventory.InventoryService_Legacy;

public class FindGroupingByDealerIdAndWeeksRetriever extends StoredProcedureTemplate
{

public List<ReportGrouping> findByDealerIdAndWeeks( int dealerId, int weeks, int forecast, int mileage ) throws ApplicationException
{
	return this.findByDealerIdAndWeeks( dealerId,weeks, forecast, mileage, 0, true );
}

public List<ReportGrouping> findByDealerIdAndWeeks( int dealerId, int weeks, int forecast, int mileage,  int inventoryType, boolean useBodyType  ) throws ApplicationException
{
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); //faster inserts
	parameters.add( new SqlParameterWithValue( "@dealerId", Types.INTEGER, new Integer( dealerId ) ) );
	parameters.add( new SqlParameterWithValue( "@weeks", Types.INTEGER,  new Integer( weeks ) ) );
	parameters.add( new SqlParameterWithValue( "@forecast", Types.INTEGER, new Integer( forecast ) ) );
	parameters.add( new SqlParameterWithValue( "@mileage", Types.INTEGER, new Integer( mileage )) );
	parameters.add( new SqlParameterWithValue( "@inventoryType", Types.INTEGER,  new Integer( inventoryType )));

	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();
	sprocRetrieverParams.setStoredProcedureCallString( "{? = call GetSalesHistoryReport (?, ?, ?, ?, ?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );

	Map results = this.call( sprocRetrieverParams, new FindGroupingByDealerIdAndWeekMapper(inventoryType, useBodyType) );

	LinkedList<ReportGrouping> resultsList = (LinkedList<ReportGrouping>)results.get( StoredProcedureTemplate.RESULT_SET );
	return ( resultsList != null && !resultsList.isEmpty()) ? resultsList : new ArrayList<ReportGrouping>();
}

private class FindGroupingByDealerIdAndWeekMapper implements RowMapper
{
	private int inventoryType;
	private boolean useBodyType; 
	
	public FindGroupingByDealerIdAndWeekMapper(int inventoryType, boolean useBodyType ) {
		this.inventoryType = inventoryType;
		this.useBodyType = useBodyType;
	}

	public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
	{
		ReportGrouping grouping = new ReportGrouping();

		grouping.setAvgDaysToSale( (Integer)rs.getObject( "AverageDays" ) );
		grouping.setAvgGrossProfit( (Integer)rs.getObject( "AverageGrossProfit" ) );
		grouping.setAvgMileage( (Integer)rs.getObject( "AverageMileage" ) );
		grouping.setUnitsInStock( rs.getInt( "UnitsInStock" ) );
		grouping.setUnitsSold( rs.getInt( "UnitsSold" ) );
		grouping.setNoSales( rs.getInt( "No_Sales" ) );
		grouping.setTotalRevenue( (Integer)rs.getObject( "TotalRevenue" ) );
		grouping.setTotalGrossMargin( (Integer)rs.getObject( "TotalGrossMargin" ) );
		grouping.setTotalInventoryDollars( (Integer)rs.getObject( "TotalInventoryDollars" ) );
		grouping.setAverageBackEnd( (Integer)rs.getObject( "AverageBackEnd" ) );
		grouping.setAverageFrontEnd( (Integer)rs.getObject( "AverageFrontEnd" ) );
		grouping.setTotalBackEnd( (Integer)rs.getObject( "TotalBackEnd" ) );
		grouping.setTotalFrontEnd( (Integer)rs.getObject( "TotalFrontEnd" ) );
		grouping.setTotalSalesPrice( (Integer)rs.getObject( "TotalRevenue" ) );

		if ( grouping.getAverageBackEnd() != null && grouping.getAverageFrontEnd() != null )
		{
			grouping.setAverageTotalGross( new Integer( grouping.getAverageBackEnd().intValue() + grouping.getAverageFrontEnd().intValue() ) );
		}

		if ( inventoryType == InventoryEntity.USED_CAR )
		{
			grouping.setGroupingName( rs.getString( "VehicleGroupingDescription" ) );
			grouping.setGroupingId( rs.getInt( "GroupingId" ) );
			int averageInventoryAge = rs.getInt( "AverageInvAge" );
			grouping.setAverageInventoryAge( new Integer( averageInventoryAge ) );
		}

		if ( inventoryType == InventoryEntity.NEW_CAR )
		{
			grouping.setMake( rs.getString( "Make" ) );
			grouping.setModel( rs.getString( "Model" ) );
			grouping.setVehicleTrim( rs.getString( "VehicleTrim" ) );
			grouping.setBodyType( rs.getString( "Description" ) );
			grouping.setGroupingName( InventoryService_Legacy.createGroupingString( grouping.getMake(), grouping.getModel(),
																				grouping.getVehicleTrim(), (useBodyType) ?
																				grouping.getBodyType(): null ) );
			grouping.setBodyTypeId( rs.getInt( "BodyTypeID" ) );
		}
		return grouping;
	}

}

}
