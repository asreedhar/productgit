package com.firstlook.persistence.applicationevent;

import com.firstlook.data.DatabaseException;
import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.ApplicationEvent;

public class ApplicationEventPersistence
{

public ApplicationEventPersistence()
{
    super();
}

public void save( ApplicationEvent applicationEvent ) throws DatabaseException
{
    IMTDatabaseUtil.instance().save(applicationEvent);
}

}
