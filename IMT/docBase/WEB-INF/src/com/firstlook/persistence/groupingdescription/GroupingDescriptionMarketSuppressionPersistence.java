package com.firstlook.persistence.groupingdescription;

import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.GroupingDescriptionMarketSuppression;

public class GroupingDescriptionMarketSuppressionPersistence
{

public GroupingDescriptionMarketSuppressionPersistence()
{
}

public void update( GroupingDescriptionMarketSuppression groupingDescription )
{
    IMTDatabaseUtil.instance().update(groupingDescription);
}

public GroupingDescriptionMarketSuppression findById( Integer id )
{
    return (GroupingDescriptionMarketSuppression) IMTDatabaseUtil.instance()
            .load(GroupingDescriptionMarketSuppression.class, id);
}

}
