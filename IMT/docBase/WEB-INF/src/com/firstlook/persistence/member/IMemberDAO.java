package com.firstlook.persistence.member;

import java.util.List;

import biz.firstlook.transact.persist.model.JobTitle;

import com.firstlook.entity.IMember;
import com.firstlook.entity.Member;

public interface IMemberDAO
{
public abstract Member findByPk( Integer memberId );

public Member findByLogin( String login );

public List<Member> findByMultipleMemberIds( List<Integer> memberIds );

public List<JobTitle> allJobTitles();

public void save( IMember member );

public void saveOrUpdate( IMember member );

public List< Member > findByBusinessUnitIdAndJobTitleId( int dealerId, int jobTitleId );
}