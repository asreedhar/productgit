package com.firstlook.persistence.member;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import biz.firstlook.transact.persist.model.JobTitle;

import com.firstlook.entity.IMember;
import com.firstlook.entity.Member;

public class MockMemberDAO implements IMemberDAO
{

private Map<Integer, IMember> memberMap = new HashMap<Integer, IMember>();

public MockMemberDAO()
{
	super();
}

public Member findByPk( Integer memberId )
{
	return (Member)memberMap.get( memberId );
}

public void store( IMember member )
{
	memberMap.put( member.getMemberId(), member );
}

public void add( Integer key, IMember member )
{
	memberMap.put( key, member );
}

public Member findByLogin( String login )
{
	return (Member)memberMap.get( login );
}

public List<Member> findByLastNameLike( String lastName )
{
	return null;
}

public List<Member> findByMultipleMemberIds( List<Integer> memberIds )
{
	return null;
}

public List<JobTitle> allJobTitles()
{
	return null;
}

public void save( IMember member )
{
	memberMap.put( member.getMemberId(), member );
}

public void saveOrUpdate( IMember member )
{
	memberMap.put( member.getMemberId(), member );
}

public List<Member> findByEitherNameLike( String searchStr )
{
	return null;
}

public List< Member > findByJobTitleId( int jobTitleId )
{
	// TODO Auto-generated method stub
	return null;
}

public List< Member > findByBusinessUnitIdAndJobTitleId( int dealerId, int jobTitleId )
{
	// TODO Auto-generated method stub
	return null;
}

public List< Member > findByBusinessUnitAndJobTitleId( Integer dealerId, int jobTitleId )
{
	// TODO Auto-generated method stub
	return null;
}

}
