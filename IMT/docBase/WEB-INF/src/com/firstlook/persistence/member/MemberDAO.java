package com.firstlook.persistence.member;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.commons.sql.RuntimeDatabaseException;
import biz.firstlook.transact.persist.model.JobTitle;

import com.firstlook.entity.IMember;
import com.firstlook.entity.Member;

public class MemberDAO extends HibernateDaoSupport implements IMemberDAO
{

public Member findByPk( Integer memberId )
{
	// this is needed bc the dealers collection on member needs to 
	// be force loaded to avoid Lazt init errors as we jump sessions
	// with members that can log into multiple stores (i.e. Act Reps and admins)
	int curFlushMode = getHibernateTemplate().getFlushMode();
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_ALWAYS);
	Member result = (Member)getHibernateTemplate().load( Member.class, memberId );
	getHibernateTemplate().setFlushMode(curFlushMode);
	return result;
}

public Member findByLogin( String login )
{
	Integer memberId = null;
	Member member = null;
	List<Integer> memberIds = (List<Integer>)getHibernateTemplate().find( "select m.memberId from com.firstlook.entity.Member m" + " where m.login = ?", login );
	if ( memberIds != null && memberIds.size() > 0 )
	{
		memberId = memberIds.get( 0 );
		member = findByPk( memberId );
	}
	return member;
}

public List<Member> findByMultipleMemberIds( List<Integer> memberIds )
{
	Session session = null;
	try
	{	
		// this is needed bc the dealers collection on member needs to 
		// be force loaded to avoid Lazt init errors as we jump sessions
		// with members that can log into multiple stores (i.e. Act Reps and admins)
		int curFlushMode = getHibernateTemplate().getFlushMode();
		getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_ALWAYS);
		session = getHibernateTemplate().getSessionFactory().getCurrentSession();
		Criteria criteria = session.createCriteria( Member.class );
		criteria.add( Expression.in( "memberId", memberIds ) );
		List<Member> result = criteria.list();
		getHibernateTemplate().setFlushMode(curFlushMode);
		return result;
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Error finding members by multiple member ids", e );
	}
}

public List<JobTitle> allJobTitles()
{
	return getHibernateTemplate().find( "from JobTitle" );
}

public List< Member > findByBusinessUnitIdAndJobTitleId( final int dealerId, final int jobTitleId )
{
	final StringBuffer hqlQuery = new StringBuffer();
	hqlQuery.append( "from " + Member.class.getName() + " as member " );
	hqlQuery.append( " where jobTitleId = :jobTitleId " );
	hqlQuery.append( " and :dealerId in ( select dealer.dealerId from member.dealersCollection as dealer) " );

	return (List< Member >)getHibernateTemplate().execute( new HibernateCallback(){

		public Object doInHibernate( org.hibernate.Session session ) throws HibernateException, SQLException
		{
			Query query = session.createQuery( hqlQuery.toString() );
			query.setParameter( "dealerId", dealerId );
			query.setParameter( "jobTitleId", jobTitleId );
			return query.list();
		} });
}

public void saveOrUpdate( final IMember member )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
	getHibernateTemplate().saveOrUpdate( member );
}

public void save( IMember member )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
	getHibernateTemplate().save( member );
}

public void delete( IMember member )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
	getHibernateTemplate().delete( member );
}


}
