package com.firstlook.persistence.member;

import java.sql.SQLException;
import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.firstlook.entity.Role;

public class RolePersistence extends HibernateDaoSupport
{

public static String USED_TYPE = "U";
public static String NEW_TYPE = "N";
public static String ADMIN_TYPE = "A";

public Role findByPk( Integer roleId )
{
	return (Role)getHibernateTemplate().load( Role.class, roleId );
}

@SuppressWarnings("unchecked")
public Collection<Role> findByType( final String type )
{
	return (Collection<Role>) getHibernateTemplate().execute( new HibernateCallback()
	{
		public Object doInHibernate( Session session ) throws HibernateException, SQLException
		{
			Criteria criteria = session.createCriteria( Role.class );
			criteria.add( Expression.eq( "type", type ) );
			return criteria.list();
		}
	} );
}

public Collection<Role> findUsed()
{
	return findByType( USED_TYPE );
}

public Collection<Role> findNew()
{
	return findByType( NEW_TYPE );
}

public Collection<Role> findAdmin()
{
	return findByType( ADMIN_TYPE );
}
}
