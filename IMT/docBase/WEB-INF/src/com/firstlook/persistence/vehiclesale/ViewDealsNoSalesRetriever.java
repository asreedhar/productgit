package com.firstlook.persistence.vehiclesale;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.commons.sql.SqlParameterWithValue;
import biz.firstlook.commons.sql.StoredProcedureTemplate;
import biz.firstlook.commons.sql.StoredProcedureTemplateParameters;

import com.firstlook.entity.VehicleSaleEntity;

public class ViewDealsNoSalesRetriever extends StoredProcedureTemplate
{

public List retrieveUnitCostPointsDetails( int businessUnitId,
        int dealerGroupId, int weeks, int forecast, String make, String model,
        String trim, int mileage, int inventoryType )
{
	
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); //faster inserts
	parameters.add( new SqlParameterWithValue( "@businessUnitId", Types.INTEGER, new Integer(businessUnitId) ) );
	parameters.add( new SqlParameterWithValue( "@dealerGroupId", Types.INTEGER, new Integer(dealerGroupId) ) );
	parameters.add( new SqlParameterWithValue( "@weeks", Types.INTEGER, new Integer(weeks) ) );
	parameters.add( new SqlParameterWithValue( "@forecast", Types.INTEGER,new Integer(forecast)) );
	parameters.add( new SqlParameterWithValue( "@make", Types.VARCHAR, make) );
	parameters.add( new SqlParameterWithValue( "@model", Types.VARCHAR, model ) );
	parameters.add( new SqlParameterWithValue( "@trim", Types.VARCHAR, trim ) );
	parameters.add( new SqlParameterWithValue( "@mileage", Types.INTEGER, new Integer(mileage) ) );
	parameters.add( new SqlParameterWithValue( "@inventoryType", Types.INTEGER, new Integer(inventoryType)) );

	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();
	sprocRetrieverParams.setStoredProcedureCallString( "{? = call GetViewDealsNoSales (?, ?, ?, ?, ?, ?, ?, ?, ?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );

	Map results = this.call( sprocRetrieverParams, new ViewDealsNoSalesMapper() );
	LinkedList<VehicleSaleEntity> resultsList = (LinkedList<VehicleSaleEntity>)results.get( StoredProcedureTemplate.RESULT_SET );
	return ( resultsList != null && !resultsList.isEmpty()) ? resultsList : new ArrayList();

}

private class ViewDealsNoSalesMapper implements RowMapper
{

	public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
	{
	    VehicleSaleEntity vehicleSale = new VehicleSaleEntity();
	
	    vehicleSale.setAnalyticalEngineStatus(rs
	            .getString("AnalyticalEngineStatus"));
	    vehicleSale.setDealDate(rs.getDate("DealDate"));
	    vehicleSale.setFinanceInsuranceDealNumber(rs
	            .getString("FinanceInsuranceDealNumber"));
	    vehicleSale.setFrontEndGross(rs.getDouble("FrontEndGross"));
	    int inventoryId = rs.getInt("InventoryId");
	    vehicleSale.setInventoryId(inventoryId);
	    vehicleSale.setLastPolledDate(rs.getDate("LastPolledDate"));
	    vehicleSale.setLeaseFlag(rs.getInt("LeaseFlag"));
	    vehicleSale.setNetFinancialInsuranceIncome(rs
	            .getDouble("NetFinancialInsuranceIncome"));
	    vehicleSale.setBackEndGross(rs.getDouble("BackEndGross"));
	    vehicleSale.setSaleDescription(rs.getString("SaleDescription"));
	    vehicleSale.setSalePrice(rs.getDouble("SalePrice"));
	    vehicleSale.setSalesReferenceNumber(rs
	            .getString("SalesReferenceNumber"));
	    vehicleSale.setVehicleMileage(rs.getInt("VehicleMileage"));
	    return vehicleSale;
	}

}

}
