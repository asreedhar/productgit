package com.firstlook.persistence.vehiclesale;

import java.util.Date;

import com.firstlook.entity.TradePurchaseEnum;


public class ViewDealsRow
{
private double salePrice;
private String baseColor;
private Date inventoryReceivedDate;
private Date dealDate;
private String dealerNickname;


private String dealNumber;
private double retailGrossProfit;
private String make;
private String model;
private String trim;
private String bodyStyle;
private int mileage;
private TradePurchaseEnum tradePurchase;
private double unitCost;
private int year;

public ViewDealsRow(double salePrice, String baseColor, double retailGrossProfit, Date inventoryReceivedDate, Date dealDate, String dealNumber,
                    String make, String model, String trim, String bodyStyle, int mileage, int tradePurchase, double unitCost, int year) 
{
    this.salePrice = salePrice;
    this.baseColor= baseColor;
    this.inventoryReceivedDate = inventoryReceivedDate;  
    this.dealDate = dealDate;
    this.dealNumber = dealNumber;
    this.retailGrossProfit = retailGrossProfit;
    this.make = make;
    this.model = model;
    if ( "N/A".equalsIgnoreCase(trim) )
    {
        this.trim = "";
    } else
    {
        this.trim = trim;
    }

    this.bodyStyle = bodyStyle;
    this.mileage = mileage; 
    this.tradePurchase= TradePurchaseEnum.getEnum( tradePurchase );
    this.unitCost = unitCost;
    this.year = year;
    
    
    
}


/**
 * this version of the constructor takes a dealer nickname, only will be used for show dealer group results - KDL
 */
public ViewDealsRow(double salePrice, String baseColor, double retailGrossProfit, Date inventoryReceivedDate, Date dealDate, String dealNumber,
                    String make, String model, String trim, String bodyStyle, int mileage, int tradePurchase, double unitCost, int year, String dealerNickname ) 
{
    this.salePrice = salePrice;
    this.baseColor= baseColor;
    this.dealerNickname =  dealerNickname;
    this.inventoryReceivedDate = inventoryReceivedDate;  
    this.dealDate = dealDate;
    this.dealNumber = dealNumber;
    this.retailGrossProfit = retailGrossProfit;
    this.make = make;
    this.model = model;
    if ( "N/A".equalsIgnoreCase(trim) )
    {
        this.trim = "";
    } else
    {
        this.trim = trim;
    }

    this.bodyStyle = bodyStyle;
    this.mileage = mileage; 
    this.tradePurchase= TradePurchaseEnum.getEnum( tradePurchase );
    this.unitCost = unitCost;
    this.year = year;
    
    
    
}

public String getBaseColor()
{
    return baseColor;
}
public void setBaseColor( String baseColor )
{
    this.baseColor = baseColor;
}
public String getBodyStyle()
{
    return bodyStyle;
}
public void setBodyStyle( String bodyStyle )
{
    this.bodyStyle = bodyStyle;
}
public Date getInventoryReceivedDate()
{
    return inventoryReceivedDate;
}
public void setInventoryReceivedDate( Date inventoryReceivedDate )
{
    this.inventoryReceivedDate = inventoryReceivedDate;
}
public Date getDealDate()
{
    return dealDate;
}
public void setDealDate( Date dealDate )
{
    this.dealDate = dealDate;
}

public String getDealNumber()
{
    return dealNumber;
}
public void setDealNumber( String dealNumber )
{
    this.dealNumber = dealNumber;
}
public String getMake()
{
    return make;
}
public void setMake( String make )
{
    this.make = make;
}
public int getMileage()
{
    return mileage;
}
public void setMileage( int mileage )
{
    this.mileage = mileage;
}
public String getModel()
{
    return model;
}
public void setModel( String model )
{
    this.model = model;
}
public double getRetailGrossProfit()
{
    return retailGrossProfit;
}
public void setRetailGrossProfit( double retailGrossProfit )
{
    this.retailGrossProfit = retailGrossProfit;
}
public double getSalePrice()
{
    return salePrice;
}
public void setSalePrice( double salePrice )
{
    this.salePrice = salePrice;
}
public TradePurchaseEnum getTradePurchase()
{
    return tradePurchase;
}
public void setTradePurchase( TradePurchaseEnum tradePurchase )
{
    this.tradePurchase = tradePurchase;
}
public String getTrim()
{
    return trim;
}
public void setTrim( String trim )
{
    this.trim = trim;
}
public double getUnitCost()
{
    return unitCost;
}
public void setUnitCost( double unitCost )
{
    this.unitCost = unitCost;
}
public int getYear()
{
    return year;
}
public void setYear( int year )
{
    this.year = year;
}
public String getDealerNickname()
{
	return dealerNickname;
}
public void setDealerNickname( String dealerNickname )
{
	this.dealerNickname = dealerNickname;
}
}
