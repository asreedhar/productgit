package com.firstlook.persistence.inventory;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.commons.sql.SqlParameterWithValue;
import biz.firstlook.commons.sql.StoredProcedureTemplate;
import biz.firstlook.commons.sql.StoredProcedureTemplateParameters;


public class FindMarketShareGroupingRetriever extends StoredProcedureTemplate
{

public MarketShareReturnObject findByGroupingDescription( Integer dealerId, Integer segmentId, Integer groupingDescriptionId ) 
{
	
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); //faster inserts
	parameters.add( new SqlParameterWithValue( "@businessUnitId", Types.INTEGER, dealerId ) );
	parameters.add( new SqlParameterWithValue( "@segmentId", Types.INTEGER, segmentId ) );
	parameters.add( new SqlParameterWithValue( "@groupingDescriptionId", Types.VARCHAR, groupingDescriptionId) );

	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();

	sprocRetrieverParams.setStoredProcedureCallString( "{? = call GetMarketShare(?, ?, ?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );

	Map results = this.call( sprocRetrieverParams, new FindMarketShareGroupingMapper() );

	LinkedList<MarketShareReturnObject> resultsList = (LinkedList<MarketShareReturnObject>)results.get( StoredProcedureTemplate.RESULT_SET );
	return ( resultsList != null && !resultsList.isEmpty()) ? resultsList.get(0) : new MarketShareReturnObject();
}


private class FindMarketShareGroupingMapper implements RowMapper
{

	public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
	{
		MarketShareReturnObject marketShareReturnObject = new MarketShareReturnObject((Integer) rs.getObject( "BusinessUnitID" ), 
	    			                                              (Integer) rs.getObject( "SegmentID" ), 
	    	                                                      (Integer) rs.getObject( "GroupingDescriptionID" ), (Integer) rs.getObject( "BodyTypeUnits" ),
	    	                                                      (Integer) rs.getObject( "GroupingUnits" ), (BigDecimal) rs.getBigDecimal( "MarketShare" ));
	    return marketShareReturnObject;
	}

}

}
