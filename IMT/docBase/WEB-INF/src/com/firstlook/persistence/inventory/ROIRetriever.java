package com.firstlook.persistence.inventory;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class ROIRetriever extends JdbcDaoSupport
{

public Map getROI( Integer businessUnitId )
{
	String sql = "SELECT  VH.VehicleGroupingID, VH.VehicleYear, AvgFrontEndGross / AvgUnitCost * 365 / AvgDaysToSale AnnualROI, "
			+ "AvgFrontEndGross, AvgUnitCost, AvgDaysToSale "
			+ "FROM    Pricing_A1 P JOIN VehicleHierarchy_D VH ON P.VehicleHierarchyID = VH.VehicleHierarchyID "
			+ "WHERE   P.PeriodID = 4 AND P.SaleTypeID = 1 AND P.MileageBucketID = 9 AND VH.Level = 3 "
			+ "AND P.BusinessUnitID = ? ORDER BY VH.VehicleGroupingID, VH.VehicleYear";
	final Object[] params = new Object[] { businessUnitId };
	final int[] types = new int[] { Types.INTEGER };
	return (Map)getJdbcTemplate().query( sql, params, types, new ROIExtractor() );
}

public Map getMakeModelROI( Integer businessUnitId, int vehicleGroupingId )
{
	String sql = "SELECT  VH.VehicleYear, AvgFrontEndGross / AvgUnitCost * 365 / AvgDaysToSale AnnualROI, "
			+ "AvgFrontEndGross, AvgUnitCost, AvgDaysToSale "
			+ "FROM    Pricing_A1 P JOIN VehicleHierarchy_D VH ON P.VehicleHierarchyID = VH.VehicleHierarchyID "
			+ "WHERE   P.PeriodID = 4 AND P.SaleTypeID = 1 AND P.MileageBucketID = 9 AND VH.Level = 3 "
			+ "AND P.BusinessUnitID = ? AND VH.VehicleGroupingID = ? ORDER BY VH.VehicleYear";
	final Object[] params = new Object[] { businessUnitId, new Integer( vehicleGroupingId ) };
	final int[] types = new int[] { Types.INTEGER, Types.INTEGER };
	return (Map)getJdbcTemplate().query( sql, params, types, new MakeModelROIExtractor() );
}

public BigDecimal getMakeModelYearROI( Integer businessUnitId, int vehicleGroupingId, int vehicleYear )
{
	String sql = "SELECT  AvgFrontEndGross / AvgUnitCost * 365 / AvgDaysToSale AnnualROI, "
			+ "AvgFrontEndGross, AvgUnitCost, AvgDaysToSale "
			+ "FROM    Pricing_A1 P JOIN VehicleHierarchy_D VH ON P.VehicleHierarchyID = VH.VehicleHierarchyID "
			+ "WHERE   P.PeriodID = 4 AND P.SaleTypeID = 1 AND P.MileageBucketID = 9 AND VH.Level = 3 "
			+ "AND P.BusinessUnitID = ? AND VH.VehicleGroupingID = ? AND VH.VehicleYear = ?";
	final Object[] params = new Object[] { businessUnitId, new Integer( vehicleGroupingId ), new Integer( vehicleYear ) };
	final int[] types = new int[] { Types.INTEGER, Types.INTEGER, Types.INTEGER };
	return (BigDecimal)getJdbcTemplate().query( sql, params, types, new MakeModelYearROIExtractor() );
}

private class MakeModelYearROIExtractor implements ResultSetExtractor
{

public Object extractData( ResultSet rs ) throws SQLException, DataAccessException
{
	BigDecimal annualRoi = null;

	if ( rs.next() )
	{ 
		annualRoi = (BigDecimal)rs.getObject( "AnnualRoi" );
		if( annualRoi == null )
		{
			annualRoi = new BigDecimal( Integer.MIN_VALUE );
		}
	}
	else
	{
		annualRoi = new BigDecimal( Integer.MIN_VALUE );
	}

	return annualRoi;
}
}

private class MakeModelROIExtractor implements ResultSetExtractor
{

public Object extractData( ResultSet rs ) throws SQLException, DataAccessException
{
	Map resultMap = new HashMap();
	BigDecimal annualRoi;

	while ( rs.next() )
	{
		annualRoi = (BigDecimal)rs.getObject( "AnnualRoi" );
		if( annualRoi == null )
		{
			annualRoi = new BigDecimal( Integer.MIN_VALUE );
		}
		resultMap.put( ((Integer)rs.getObject( "vehicleYear" )).toString(), annualRoi );
	}

	return resultMap;
}

}

private class ROIExtractor implements ResultSetExtractor
{

public Object extractData( ResultSet rs ) throws SQLException, DataAccessException
{
	Map resultMap = new HashMap();
	String key;
	BigDecimal annualRoi;

	while ( rs.next() )
	{
		annualRoi = (BigDecimal)rs.getObject( "AnnualRoi" );
		if( annualRoi == null )
		{
			annualRoi = new BigDecimal( Integer.MIN_VALUE );
		}
		key = rs.getObject( "vehicleGroupingID" ).toString() + "_" + rs.getObject( "vehicleYear" ).toString();
		resultMap.put( key, (BigDecimal)rs.getObject( "AnnualRoi" ) );
	}

	return resultMap;
}

}

}
