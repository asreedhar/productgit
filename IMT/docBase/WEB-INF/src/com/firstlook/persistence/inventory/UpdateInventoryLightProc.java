package com.firstlook.persistence.inventory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.object.StoredProcedure;

import biz.firstlook.transact.persist.model.Inventory;

public class UpdateInventoryLightProc extends StoredProcedure
{

private static final String STORED_PROC_NAME = "UpdateInventoryLights";
private static final String PARAM_RESULT_SET = "ResultSet";
private static final String PARAM_BUSINESS_UNIT_ID = "businessUnitId";
private static final String PARAM_RUN_DATE = "runDate";
private static final String PARAM_INVENTORY_ID = "inventoryId";


public UpdateInventoryLightProc( DataSource dataSource ) {
	super( dataSource, STORED_PROC_NAME );
	setFunction( false );
	declareParameter( new SqlReturnResultSet( PARAM_RESULT_SET, new UpdateInventoryLightRowMapper() ) );
	declareParameter( new SqlParameter( PARAM_BUSINESS_UNIT_ID, Types.INTEGER ) );
	declareParameter( new SqlParameter( PARAM_RUN_DATE, Types.DATE ) );
	declareParameter( new SqlParameter( PARAM_INVENTORY_ID, Types.INTEGER ) );	
	compile();
}

@SuppressWarnings("unchecked")
public Inventory calculateAgingPlanLightForSpecificInventory( int businessUnitId, Integer inventoryId )
{
	Map<String, Object> parameters = new HashMap<String, Object>();
	parameters.put( PARAM_INVENTORY_ID, inventoryId );
	parameters.put( PARAM_RUN_DATE, new Date() );
	parameters.put( PARAM_BUSINESS_UNIT_ID, businessUnitId );
	
	Map results = execute( parameters );
	List<Inventory> inventories = (List<Inventory>)results.get( PARAM_RESULT_SET );
	if( inventories != null || inventories.size() > 0 )
		return inventories.get( 0 );
	else
		return null;
}

class UpdateInventoryLightRowMapper implements RowMapper {

public Object mapRow( ResultSet rs, int count ) throws SQLException {
	Inventory returnMe = new Inventory();
	returnMe.setAcquisitionPrice( new Double( rs.getDouble("AcquisitionPrice")));
    returnMe.setCertified( rs.getBoolean("Certified"));
    returnMe.setCurrentVehicleLight( new Integer( rs.getInt( "CurrentVehicleLight")) );
    returnMe.setDealerId( rs.getInt( "BusinessUnitID"));
    returnMe.setDeleteDt( rs.getDate( "DeleteDt"));
    returnMe.setDmsReferenceDt( rs.getDate( "DMSReferenceDt"));
    returnMe.setInitialVehicleLight( new Integer( rs.getInt( "InitialVehicleLight") ));
    returnMe.setInventoryActive( rs.getBoolean("InventoryActive"));
    returnMe.setInventoryId( new Integer(rs.getInt("InventoryID")));
    returnMe.setInventoryReceivedDt( rs.getDate( "InventoryreceivedDate"));
    returnMe.setInventoryType(rs.getInt("InventoryType"));
    returnMe.setListPrice( new Double( rs.getDouble("ListPrice") ));
    returnMe.setLotLocation(rs.getString("VehicleLocation"));
    returnMe.setMileageReceived( new Integer( rs.getInt("MileageReceived") ));
    returnMe.setModifiedDT(rs.getDate("ModifiedDT"));
    returnMe.setPack( new Double( rs.getDouble("Pack") ));
    returnMe.setReconditionCost( new Double( rs.getDouble("ReconditionCost") ));
    returnMe.setStatusCode(rs.getInt("InventoryStatusCD"));
    returnMe.setStockNumber(rs.getString("StockNumber"));
    returnMe.setTradeOrPurchase(rs.getInt("TradeOrPurchase"));
    returnMe.setUnitCost(rs.getDouble("UnitCost"));
    returnMe.setUsedSellingPrice( new Double( rs.getDouble("UsedSellingPrice") ));
    returnMe.setVehicleId(rs.getInt("VehicleID"));
    return returnMe;
}

}

}
