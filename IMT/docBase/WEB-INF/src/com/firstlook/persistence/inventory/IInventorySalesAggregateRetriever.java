package com.firstlook.persistence.inventory;

import java.util.Map;

interface IInventorySalesAggregateRetriever
{

public static final int SUCCESS_RETURN_CODE = 0;
public static final String ResultSet = "ResultSet";
public static final String ReturnCode = "@RC";

Map call( Integer businessUnitId, Integer weeks, Integer forecast, Integer inventoryType );

}
