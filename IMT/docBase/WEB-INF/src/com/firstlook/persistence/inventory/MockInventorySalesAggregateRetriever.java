package com.firstlook.persistence.inventory;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class MockInventorySalesAggregateRetriever implements IInventorySalesAggregateRetriever
{

private Map< String, Object > resultSet = new HashMap<String, Object>();
private boolean nextNull = false;

public Map call( Integer businessUnitId, Integer weeks, Integer forecast, Integer inventoryType )
{
	if( nextNull ) {
		nextNull = false;
		return null;
	}
	return Collections.unmodifiableMap( resultSet );
}

public void setResultSetEntry( Object object )
{
	resultSet.put( IInventorySalesAggregateRetriever.ResultSet, object );
}

public void setReturnCode( Integer code )
{
	resultSet.put( IInventorySalesAggregateRetriever.ReturnCode, code );
}

public void reset()
{
	resultSet.clear();
}

public void nextResultNull()
{
	nextNull = true;
}

}
