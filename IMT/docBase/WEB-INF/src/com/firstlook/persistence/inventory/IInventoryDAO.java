package com.firstlook.persistence.inventory;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.firstlook.data.DatabaseException;
import com.firstlook.data.RuntimeDatabaseException;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;

public interface IInventoryDAO
{

public InventoryEntity findByPk( Integer inventoryId );

public String findStockNumberByVinAndDealerId( String vin, Integer dealerId );

public Collection findTopContributorsByDealerIdAndSaleDescriptionAndDealDateAndFrontEndGross( int riskLevelNumberOfWeeks,
																								int riskLevelNumberOfContributors, int dealerId )
		throws ApplicationException, DatabaseException;

public Collection findByDealerIdInventoryTypeAndUpperAndLowerUnitCostThreshold( int dealerId, int inventoryType, Integer lowerThreshold,
																				Integer upperThreshold );

public int countBy( int dealerId, int inventoryType, int groupingDescriptionId );

public int findCountByDealerIdAndInventoryType( int dealerId, int inventoryType );

public Collection findByDealerIdAndInventoryStatusAndAgingPolicyAndGreenLight( String groupingDescIdStr, Timestamp date, String dealersInGroupStr )
		throws ApplicationException;

public int findCountByDealerIdAndGroupingId( int dealerId, int groupingDescription );

public InventoryEntity findByDealerIdAndVehicleId( int vehicleId, int dealerId ) throws RuntimeDatabaseException;

public void save( InventoryEntity inventory );

public void saveOrUpdate( InventoryEntity inventory );

public void delete( InventoryEntity inventory );

public Collection findByVinReceivedDateExcludeDealerAndOrderByVehicleId( int dealerId, String vin, Timestamp receivedDate );

public Collection findActiveByDealerIdAndGroupingDescription( Integer dealerId, Integer groupingDescriptionId );

public Collection findByDealerIdOrderByReceivedDateStockNumber( int dealerId );

public Integer findIDByDealerIdAndStockNumber( int dealerId, String stockNumber );

public int findMakeModelCountByDealerIdAndInventoryType( int dealerId, int inventoryType );

public double findUnitCostTotalByDealerIdAndInventoryType( int dealerId, int inventoryType );

public int findInventoryCountByDealerIdAndCurrentVehicleLight( int dealerId, int currentVehicleLight );

public int findInventoryCountByDealerIdAndAgeBand( int dealerId, Date beginDate, Date endDate, int inventoryType );

public Collection findByDealerIdAndInventoryStatusAndAgingPolicy( Timestamp date, String dealersInGroupStr ) throws ApplicationException;

public Collection findByDealerIdAndOlderThanXDays( int dealerId, int days );

public Integer findCountByDealerIdInventoryTypeSegmentIdInventoryActive( Integer businessUnitId, Integer inventoryType,
																		Integer segmentId );

public Collection findByVehicleInDealerGroupInventoryConstraints( int dealerGroupId, int year, String make, String model, String trim,
																	String dealersInGroupStr );

public Collection findByDealerIdAndInventoryStatusAvailableEmptyBookout( int dealerId, String guideBookIds, int numberOfCatagories );

public void update( InventoryEntity inventory );

public int findCountByDealerIdGroupingIdAndYear( int dealerId, int groupingDescriptionId, int year, int inventoryType );

public List findActiveByGroupingDescription( Integer dealerId, Integer groupingDescriptionId, Integer inventoryType, Integer lowerUnitCost,
											Integer upperUnitCost );

public int findCountOfActiveByGroupingDescription( Integer dealerId, Integer groupingDescriptionId, Integer inventoryType,
													Integer lowerUnitCost, Integer upperUnitCost );

public Collection findGroupingDescriptionIdAndVehicleYearByDealerIdAndInventoryType( int dealerId, int inventoryType );

public Integer searchStockNumberInActiveInventory( Integer businessUnitId, String stockNumber );

public Integer searchStockNumberInInactiveInventory( Integer businessUnitId, String stockNumber, int searchInactiveInventoryDaysBackThreshold );

public Integer searchVinInActiveInventory( Integer businessUnitId, String vin );

public Integer searchVinInInactiveInventory( Integer businessUnitId, String vin, Integer searchInactiveInventoryDaysBackThreshold );

public String retrieveStockNumberByVinAndDealerId( String vin, Integer businessUnitId, Integer inactiveInventoryDaysBack );

}
