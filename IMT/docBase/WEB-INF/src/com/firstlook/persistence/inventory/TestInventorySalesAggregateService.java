package com.firstlook.persistence.inventory;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import com.firstlook.entity.InventorySalesAggregate;

public class TestInventorySalesAggregateService extends TestCase
{

InventorySalesAggregateService inventorySalesAggregateService = new InventorySalesAggregateService();
MockInventorySalesAggregateRetriever inventorySalesAggregateRetriever = new MockInventorySalesAggregateRetriever();
InventorySalesAggregate defaultExpectedData = new InventorySalesAggregate();

public TestInventorySalesAggregateService( String arg0 )
{
	super( arg0 );
	inventorySalesAggregateService.setInventorySalesAggregateRetriever( inventorySalesAggregateRetriever );
}

static InventorySalesAggregate createRandomAggregateData()
{
	InventorySalesAggregate data = new InventorySalesAggregate();
	data.setAvgInventoryAge( randomInt() );
	data.setDaysSupply( randomInt() );
	data.setDistinctModelsInStock( randomInt() );
	data.setNoSaleUnits( randomInt() );
	data.setRetailAGP( randomInt() );
	data.setRetailAvgBackEnd( randomInt() );
	//skip a couple setters....
	data.setRetailTotalBackEnd( randomInt() );
	data.setSellThroughRate( Math.random() );
	return data;
}

static int randomInt() {
	return (int)(Math.random() * 10);
}

@Override
protected void setUp() throws Exception
{
	inventorySalesAggregateRetriever.reset();
	defaultExpectedData = new InventorySalesAggregate();
}

private void setSuccessReturnCode() 
{
	inventorySalesAggregateRetriever.setReturnCode( IInventorySalesAggregateRetriever.SUCCESS_RETURN_CODE );
}

public void testRetreiverFailedReturnCode()
{
	inventorySalesAggregateRetriever.setReturnCode( InventorySalesAggregateRetriever.SUCCESS_RETURN_CODE + 1 );
	InventorySalesAggregate actual = inventorySalesAggregateService.retrieveCachedInventorySalesAggregate( 0, 0, 0, 0 );
	assertEquals( defaultExpectedData, actual );
}

public void testRetreiverNoReturnCode()
{	
	InventorySalesAggregate actual = inventorySalesAggregateService.retrieveCachedInventorySalesAggregate( 0, 0, 0, 0 );	
	assertEquals( defaultExpectedData, actual );
}

public void testNullRetrieverResult()
{
	setSuccessReturnCode();
	inventorySalesAggregateRetriever.nextResultNull();
	InventorySalesAggregate result = inventorySalesAggregateService.retrieveCachedInventorySalesAggregate( 0, 0, 0, 0 );
	assertEquals( defaultExpectedData, result );
}

public void testSuccessEmptyResultSet()
{
	setSuccessReturnCode();
	InventorySalesAggregate result = inventorySalesAggregateService.retrieveCachedInventorySalesAggregate( 0, 0, 0, 0 );
	assertEquals( defaultExpectedData, result );
}

public void testSuccessWithResultSetBadType()
{
	setSuccessReturnCode();
	inventorySalesAggregateRetriever.setResultSetEntry( Integer.MAX_VALUE );
	InventorySalesAggregate result = inventorySalesAggregateService.retrieveCachedInventorySalesAggregate( 0, 0, 0, 0 );
	assertEquals( defaultExpectedData, result );
}

public void testSuccessWithResultSetNoRows()
{
	setSuccessReturnCode();
	List< InventorySalesAggregate > results = new ArrayList< InventorySalesAggregate >();
	inventorySalesAggregateRetriever.setResultSetEntry( results );
	InventorySalesAggregate result = inventorySalesAggregateService.retrieveCachedInventorySalesAggregate( 0, 0, 0, 0 );
	assertEquals( defaultExpectedData, result );
}

public void testOriginalAssumptions()
{
	InventorySalesAggregate mockData1 = createRandomAggregateData();
	InventorySalesAggregate mockData2 = createRandomAggregateData();
	setSuccessReturnCode();
	List< InventorySalesAggregate > results = new ArrayList< InventorySalesAggregate >();
	results.add( mockData1 );
	results.add( mockData2 );
	inventorySalesAggregateRetriever.setResultSetEntry( results );
	
	InventorySalesAggregate result = inventorySalesAggregateService.retrieveCachedInventorySalesAggregate( 0, 0, 0, 0 );
	
	assertEquals( mockData1, result );
}

}
