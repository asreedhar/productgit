package com.firstlook.persistence.inventory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.commons.sql.SqlParameterWithValue;
import biz.firstlook.commons.sql.StoredProcedureTemplate;
import biz.firstlook.commons.sql.StoredProcedureTemplateParameters;

public class UnitsInStockRetriever  extends StoredProcedureTemplate
{

public int retrieveUnitsInStock( int businessUnitId, String vehicleTrim,
        int includeDealerGroup, int inventoryType, String make, String model )
{
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); //faster inserts
	parameters.add( new SqlParameterWithValue( "@businessUnitId", Types.INTEGER,  new Integer(businessUnitId) ) );
	parameters.add( new SqlParameterWithValue( "@vehicleTrim", Types.VARCHAR, vehicleTrim ) );
	parameters.add( new SqlParameterWithValue( "@includeDealerGroup", Types.INTEGER, new Integer(includeDealerGroup) ) );
	parameters.add( new SqlParameterWithValue( "@inventoryType", Types.INTEGER, new Integer(inventoryType)) );
	parameters.add( new SqlParameterWithValue( "@make", Types.VARCHAR, make) );
	parameters.add( new SqlParameterWithValue( "@model", Types.VARCHAR, model) );

	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();

	sprocRetrieverParams.setStoredProcedureCallString( "{? = call GetVehicleUnitsInStock (?, ?, ?, ?, ?, ?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );

	Map results = this.call( sprocRetrieverParams, new UnitsInStockMapper() );
	LinkedList<Integer> resultsList = (LinkedList<Integer>)results.get( StoredProcedureTemplate.RESULT_SET );
	return ( resultsList != null && !resultsList.isEmpty()) ? resultsList.get(0) : new Integer(0);	
}

private class UnitsInStockMapper implements RowMapper
{
	public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
	{
	    return (Integer) rs.getObject("UnitsInStock");
	}
}

}
