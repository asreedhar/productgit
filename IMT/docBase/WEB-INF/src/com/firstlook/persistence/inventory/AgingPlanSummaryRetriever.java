package com.firstlook.persistence.inventory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.CallableStatementCreatorFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;

/**
 * This executes the stored proc : GetAgingInventoryPlan. This is the same call
 * as in AgingPlanInventoryItemRetriever but! the results mode here is set to 1,
 * which means only a summary (range Ids and Units in stock for the range) is
 * returned.
 */
public class AgingPlanSummaryRetriever
{

private DataSource dataSource;
public static final int SUCCESS_RETURN_CODE = 0;
public static final String ResultSet = "ResultSet";
public static final String ReturnCode = "@RC";

public AgingPlanSummaryRetriever()
{
    super();
}

public Map call( int businessUnitId, String statusCodes, Integer inventoryBucketId )
{
    Map storedProcParameters = new HashMap();
    storedProcParameters.put( "@BusinessUnitID", new Integer( businessUnitId ) );
    storedProcParameters.put( "@StatusCodes", statusCodes );
    storedProcParameters.put( "@InventoryBucketId", inventoryBucketId );
    storedProcParameters.put( "@ResultsMode", new Integer( 1 ) );
    return call( storedProcParameters );
}

private Map call( Map sprocParameterValues ) throws DataAccessException
{
    List storedProcedureParameters = new ArrayList();
    /*
     * This is a set of return values. Typically when doing a prepared statment,
     * you get a ResultSet object. When getting Out parameters from a sproc, you
     * get a ResultSet object AND the return code, and that is why this is
     * needed. THIS MUST BE ADDED FIRST!
     */
    storedProcedureParameters.add( new SqlReturnResultSet( AgingPlanSummaryRetriever.ResultSet, new AgingPlanSummaryMapper() ) );

    // This stores the return code from a stored proc
    storedProcedureParameters.add( new SqlOutParameter( "@RC", Types.INTEGER ) );

    // Parameters of a stored proc
    storedProcedureParameters.add( new SqlParameter( "@BusinessUnitID", Types.INTEGER ) );
    storedProcedureParameters.add( new SqlParameter( "@StatusCodes", Types.VARCHAR ) );
    storedProcedureParameters.add( new SqlParameter( "@InventoryBucketId", Types.INTEGER ) );
    storedProcedureParameters.add( new SqlParameter( "@ResultsMode", Types.INTEGER ) );

    // the statement to execute
    CallableStatementCreatorFactory cscf = new CallableStatementCreatorFactory( "{? = call GetAgingInventoryPlan (?, ?, ?, ?)}",
                                                                                storedProcedureParameters );
    CallableStatementCreator csc = cscf.newCallableStatementCreator( sprocParameterValues );
    JdbcTemplate jdbcTemplate = new JdbcTemplate( dataSource );
    return jdbcTemplate.call( csc, storedProcedureParameters );
}

class AgingPlanSummaryMapper implements RowMapper
{
public Object mapRow( ResultSet rs, int rowNumber ) throws SQLException
{
    // each result row is combined into an Integer[] such that
    // the first index is the RangeId and the second is the number of Units
    ArrayList result = new ArrayList();
    result.add( new Integer[] { new Integer( rs.getInt( "RangeID" ) ), new Integer( rs.getInt( "Units" ) ) } );
    return result;
}
}

public DataSource getDataSource()
{
    return dataSource;
}

public void setDataSource( DataSource dataSource )
{
    this.dataSource = dataSource;
}

}
