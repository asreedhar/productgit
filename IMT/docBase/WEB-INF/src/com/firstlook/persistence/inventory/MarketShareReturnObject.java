package com.firstlook.persistence.inventory;

import java.math.BigDecimal;


public class MarketShareReturnObject
{
private Integer businessUnitId;
private Integer segmentId;
private Integer groupingDescriptionId;
private Integer bodyTypeUnits;
private Integer groupingUnits;
private BigDecimal marketShare;


public MarketShareReturnObject( ) 
{
}
public MarketShareReturnObject(Integer businessUnitId, Integer segmentId, Integer groupingDescriptionId, 
                               Integer bodyTypeUnits, Integer groupingUnits, BigDecimal marketShare)
{
this.businessUnitId = businessUnitId;
this.segmentId = segmentId;
this.groupingDescriptionId = groupingDescriptionId;
this.bodyTypeUnits = bodyTypeUnits;
this.groupingUnits = groupingUnits;
this.marketShare = marketShare;

}

public Integer getBodyTypeUnits()
{
	return bodyTypeUnits;
}
public void setBodyTypeUnits( Integer bodyTypeUnits )
{
	this.bodyTypeUnits = bodyTypeUnits;
}
public Integer getBusinessUnitId()
{
	return businessUnitId;
}
public void setBusinessUnitId( Integer businessUnitId )
{
	this.businessUnitId = businessUnitId;
}
public Integer getSegmentId()
{
	return segmentId;
}
public void setSegmentId( Integer segmentId )
{
	this.segmentId = segmentId;
}
public Integer getGroupingDescriptionId()
{
	return groupingDescriptionId;
}
public void setGroupingDescriptionId( Integer groupingDescriptionId )
{
	this.groupingDescriptionId = groupingDescriptionId;
}
public Integer getGroupingUnits()
{
	return groupingUnits;
}
public void setGroupingUnits( Integer groupingUnits )
{
	this.groupingUnits = groupingUnits;
}
public BigDecimal getMarketShare()
{
	return marketShare;
}
public void setMarketShare( BigDecimal marketShare )
{
	this.marketShare = marketShare;
}
}
