package com.firstlook.persistence.dealer;

import org.hibernate.ObjectNotFoundException;
import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.firstlook.entity.DealerRisk;

/**
 * This class should not be used directly. Read and write this through UCBPPreferenceService.
 * @author bfung
 * 
 */
public class DealerRiskPersistence extends HibernateDaoSupport
{

protected DealerRiskPersistence()
{
	super();
}

public DealerRisk findByBusinessUnitId( int businessUnitId )
{
	try
	{
		return (DealerRisk)getHibernateTemplate().load( DealerRisk.class, new Integer( businessUnitId ) );
	}
	catch ( ObjectNotFoundException onfe )
	{
		StringBuilder msg = new StringBuilder( "DealerRisk for " ).append( businessUnitId );
		msg.append(" was null! Returning NULL, caller should create default." );
		logger.warn( msg.toString() );
		return null;
	}
}

public void save( DealerRisk dealerRisk )
{
	getHibernateTemplate().save( dealerRisk );
}

public void update( DealerRisk dealerRisk )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
	getHibernateTemplate().update( dealerRisk );
}
}
