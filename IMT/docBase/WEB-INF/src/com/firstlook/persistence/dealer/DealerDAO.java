package com.firstlook.persistence.dealer;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.type.Type;
import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.commons.sql.RuntimeDatabaseException;

import com.firstlook.data.DatabaseException;
import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.BusinessUnitRelationship;
import com.firstlook.entity.BusinessUnitType;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerUpgrade;
import com.firstlook.entity.DealerUpgradeLookup;

public class DealerDAO extends HibernateDaoSupport implements IDealerDAO
{

public DealerDAO()
{
}

public Dealer findByPk( int dealerId )
{
	return (Dealer)getHibernateTemplate().load( Dealer.class, new Integer( dealerId ) );
}


@SuppressWarnings("unchecked")
public Collection<Dealer> findByDealerGroupId( int dealerGroupId )
{
	return getHibernateTemplate().find(
										"select dealer from com.firstlook.entity.Dealer dealer, com.firstlook.entity.BusinessUnitRelationship bur"
												+ " where bur.parentId = ? " + " and dealer.businessUnitId = bur.businessUnitId",
										new Integer( dealerGroupId ) );

}

@SuppressWarnings("unchecked")
public Collection<Dealer> findActiveGuideBookDealersWithSpring( int guidebookId )
{
	return (Collection<Dealer>)getHibernateTemplate().execute( new ActiveForBookCallback( guidebookId ) );
}

@SuppressWarnings("unchecked")
public Collection<DealerUpgrade> findUpgrades( int dealerId ) throws DatabaseException
{
	Session session = null;
	try
	{
		session = IMTDatabaseUtil.instance().retrieveSession();
//		Criteria criterion = session.createCriteria( DealerUpgrade.class );
//		criterion.add( Expression.eq( "dealerId", new Integer( dealerId ) ) );
//		List results = criterion.list();
		
		List results = session.createQuery("from com.firstlook.entity.DealerUpgrade where dealerId= :dealerId").setParameter("dealerId", dealerId).list();

		return results;
	}
	catch ( Exception e )
	{
		throw new RuntimeDatabaseException( "Problem retrieving dealerupgrade by dealerId ", e );
	}
	finally
	{
		IMTDatabaseUtil.instance().closeSession( session );
	}
}

private DealerUpgrade findUpgrade( int dealerId, int dealerUpgradeCode )
{
	DealerUpgrade query = new DealerUpgrade();
	query.setDealerId( dealerId );
	query.setDealerUpgradeCode( dealerUpgradeCode );
	return (DealerUpgrade)IMTDatabaseUtil.instance().load( DealerUpgrade.class, query );
}

public void saveOrUpdateUpgrade( DealerUpgrade upgrade ) throws DatabaseException
{
	try
	{
		if ( findUpgrade( upgrade.getDealerId(), upgrade.getDealerUpgradeCode() ) == null )
		{
			IMTDatabaseUtil.instance().save( upgrade );
		}
		else
		{
			IMTDatabaseUtil.instance().saveOrUpdate( upgrade );
		}
	}
	catch ( Exception e )
	{
		throw new DatabaseException( "Unable to save upgrade.", e );
	}
}

public void saveUpgrade( DealerUpgrade upgrade ) throws DatabaseException
{
	try
	{
		IMTDatabaseUtil.instance().save( upgrade );
	}
	catch ( Exception e )
	{
		throw new DatabaseException( "Unable to save upgrade.", e );
	}
}

public List findActiveDealersWithRedistributionBy( int dealerGroupId ) throws DatabaseException
{
	String query = "from com.firstlook.entity.Dealer dealer"
			+ " join com.firstlook.entity.BusinessUnitRelationship bur" + " join com.firstlook.entity.DealerUpgrade dealerUpgrade"
			+ " where bur.parentId = ?" + " and dealer.active = ?" + " and dealerUpgrade.dealerUpgradeCode = ?";
	try
	{
		Collection collection = getHibernateTemplate().find(
																query,
																new Object[] { new Integer( dealerGroupId ), Boolean.TRUE,
																		new Integer( DealerUpgradeLookup.REDISTRIBUTION_CODE ) } );

		return new ArrayList( collection );
	}
	catch ( Exception e )
	{
		throw new DatabaseException( "Unable to find dealers in group with redistribution.", e );
	}
}

public Collection findActiveDealersForRunDay( int runDayOfWeek ) throws DatabaseException
{
	Session session = null;
	try
	{
		session = IMTDatabaseUtil.instance().retrieveSession();
		Criteria crit = session.createCriteria( Dealer.class );
		crit.add( Expression.eq( "active", Boolean.TRUE ) );
		Criteria newCrit = crit.createCriteria( "dealerPreferences" );
		newCrit.add( Expression.eq( "runDayOfWeek", new Integer( runDayOfWeek ) ) );
		List list = crit.list();

		return list;
	}
	catch ( Exception e )
	{
		throw new DatabaseException( "Unable to find active dealers for run day " + runDayOfWeek, e );
	}
	finally
	{
		IMTDatabaseUtil.instance().closeSession( session );
	}
}

public List findActiveDealers()
{

	return (List)getHibernateTemplate().execute( new ActiveCallback() );
}

public Collection findByActiveAndProgramType( boolean active, int programTypeCD ) throws DatabaseException
{
	String query = "select d from com.firstlook.entity.Dealer as d, "
			+ "com.firstlook.entity.DealerPreference dp " + "where d.businessUnitId  = dp.dealer.businessUnitId " + " and d.active = ? "
			+ " and dp.programTypeCD = ?";
	Collection collection = IMTDatabaseUtil.instance().find( query, new Object[] { new Boolean( active ), new Integer( programTypeCD ) },
																new Type[] { Hibernate.BOOLEAN, Hibernate.INTEGER } );
	return collection;
}

public void saveOrUpdate( Dealer dealer )
{
	getHibernateTemplate().saveOrUpdate( dealer );
}

public void delete( Dealer dealer )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
	getHibernateTemplate().delete( dealer );
}

public void save( Dealer dealer )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
	getHibernateTemplate().save( dealer );
}

public List<Dealer> findByDealerCode( String dealerCode )
{
	return getHibernateTemplate().find( "select dealer from com.firstlook.entity.Dealer dealer" + " where dealer.dealerCode = ? ", dealerCode );
}

public Collection findDealersWithNoDRT( List dealerIds )
{
	String dealerIdStr = StringUtils.join( dealerIds.iterator(), "','" );

	String query = "select dealer from com.firstlook.entity.Dealer dealer, "
			+ " where dealer.active = ?" + " and dealer.dealerId not in ('" + dealerIdStr + "')";
	Collection collection = getHibernateTemplate().find( query, new Object[] { Boolean.TRUE } );

	return collection;
}

public Collection findByDealerCodeMultiple( String partialDealerCode )
{
	String query = "from com.firstlook.entity.Dealer d "
			+ " where d.dealerCode like ? " + "   and d.businessUnitTypeId = ? " + " order by d.name asc";

	return getHibernateTemplate().find(
										query,
										new Object[] { "%" + partialDealerCode + "%", new Integer( BusinessUnitType.BUSINESS_UNIT_DEALER_TYPE ) } );
}

public Collection findByName( String name )
{
	return getHibernateTemplate().find( "from com.firstlook.entity.Dealer d " + " where d.name like ? " + "   and d.businessUnitTypeId = ?",
										new Object[] { "%" + name + "%", new Integer( BusinessUnitType.BUSINESS_UNIT_DEALER_TYPE ) } );
}

public List findByDealerIds( Collection dealerIds )
{
	if ( dealerIds != null && !dealerIds.isEmpty() )
	{
		return (List)getHibernateTemplate().execute( new BusinessUnitIdsAndActiveCallback( dealerIds ) );
	}
	else
	{
		return Collections.EMPTY_LIST;
	}
}

// why do we need to find by name and nickname? - kmm
public Collection findByDealerGroupIdAndNameAndNickName( Collection businessRelationshipCol, String name, String nickName )
{
	ArrayList dealerIds = null;
	if ( businessRelationshipCol != null && businessRelationshipCol.size() > 0 )
	{
		dealerIds = new ArrayList();
		Iterator it = businessRelationshipCol.iterator();

		while ( it.hasNext() )
		{
			BusinessUnitRelationship curItem = (BusinessUnitRelationship)it.next();
			dealerIds.add( curItem.getBusinessUnitId() );
		}
		return (Collection)getHibernateTemplate().execute( new DealerIdNameAndNicknameCallback( dealerIds, name, nickName ) );
	}
	else
	{
		return Collections.EMPTY_LIST;
	}
}

private class DealerIdNameAndNicknameCallback implements HibernateCallback
{

private final Collection ids;
private final String name;
private final String nickName;

private DealerIdNameAndNicknameCallback( Collection ids, String name, String nickName )
{
	this.ids = ids;
	this.name = name;
	this.nickName = nickName;
}

public Object doInHibernate( org.hibernate.Session session ) throws HibernateException, SQLException
{
	Criteria crit = session.createCriteria( Dealer.class );
	crit.add( Expression.in( "businessUnitId", ids ) );
	crit.add( Expression.eq( "active", Boolean.TRUE ) );
	if ( name != null && name.trim().length() > 0 )
	{
		crit.add( Expression.like( "name", "%" + name + "%" ) );
	}
	if ( nickName != null && nickName.trim().length() > 0 )
	{
		crit.add( Expression.like( "nickname", "%" + nickName + "%" ) );
	}
	crit.addOrder( Order.asc( "name" ) );
	return crit.list();

}

}

private class BusinessUnitIdsAndActiveCallback implements HibernateCallback
{

private final Collection ids;

private BusinessUnitIdsAndActiveCallback( Collection ids )
{
	this.ids = ids;
}

public Object doInHibernate( org.hibernate.Session session ) throws HibernateException, SQLException
{
	Criteria crit = session.createCriteria( Dealer.class );
	crit.add( Expression.in( "businessUnitId", ids ) );
	crit.add( Expression.eq( "active", Boolean.TRUE ) );
	crit.addOrder( Order.asc( "name" ) );
	return crit.list();
}

}

private class ActiveCallback implements HibernateCallback
{

private ActiveCallback()
{
}

public Object doInHibernate( org.hibernate.Session session ) throws HibernateException, SQLException
{
	StringBuilder hql = new StringBuilder();
	hql.append( "from com.firstlook.entity.Dealer dealer" );
	hql.append( " left join fetch dealer.dealerPreferences");
	hql.append( " where dealer.active = :active " );
	hql.append( " order by dealer.name asc " );
	
	Query query = session.createQuery( hql.toString() );
	query.setParameter( "active", Boolean.TRUE );
	return query.list();
}

}
private class ActiveForBookCallback implements HibernateCallback
{
private final int guideBookId;

private ActiveForBookCallback( int guideBookId )
{
	this.guideBookId = guideBookId;
}

public Object doInHibernate( org.hibernate.Session session ) throws HibernateException, SQLException
{

	Criteria crit = session.createCriteria( Dealer.class );
	crit.add( Expression.eq( "active", Boolean.TRUE ) );
	Criteria newCrit = crit.createCriteria( "dealerPreferences" );
	newCrit.add( Expression.eq( "guideBookId", new Integer( guideBookId ) ) );
	newCrit.add( Expression.eq( "bookOut", Boolean.TRUE ) );

	return crit.list();
}

}

}