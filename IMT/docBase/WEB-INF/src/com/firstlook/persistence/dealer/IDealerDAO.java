package com.firstlook.persistence.dealer;

import java.util.Collection;
import java.util.List;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerUpgrade;

public interface IDealerDAO
{
public Dealer findByPk( int dealerId );

public List<Dealer> findByDealerCode( String dealerCode );

public Collection<Dealer> findByDealerGroupId( int dealerGroupId );

public Collection findActiveGuideBookDealersWithSpring( int guidebookId );

public Collection<DealerUpgrade> findUpgrades( int dealerId ) throws DatabaseException;

public List findActiveDealersWithRedistributionBy( int dealerGroupId )
        throws DatabaseException;

public void saveOrUpdateUpgrade( DealerUpgrade upgrade )
        throws DatabaseException;

public Collection findActiveDealersForRunDay( int runDay )
        throws DatabaseException;

public List findActiveDealers();

public Collection findByActiveAndProgramType( boolean active, int programType )
        throws DatabaseException;

public void saveOrUpdate( Dealer dealer );

public void delete( Dealer dealer );

public void save( Dealer dealer );

public Collection findDealersWithNoDRT( List dealerIds );

public Collection findByDealerCodeMultiple( String partialDealerCode );

public Collection findByName( String name );

public List findByDealerIds( Collection dealerIds );

public Collection findByDealerGroupIdAndNameAndNickName( Collection dealerGroupCol,
        String name, String nickName );

}