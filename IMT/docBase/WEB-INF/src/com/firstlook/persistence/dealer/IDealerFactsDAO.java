package com.firstlook.persistence.dealer;

import com.firstlook.entity.DealerFacts;

public interface IDealerFactsDAO
{
public DealerFacts findByBusinessUnitId( Integer businessUnitId );
}