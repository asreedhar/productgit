package com.firstlook.persistence.dealer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.jxpath.JXPathContext;

import com.firstlook.data.DatabaseException;
import com.firstlook.data.mock.MockDatabase;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerUpgrade;
import com.firstlook.entity.IMember;
import com.firstlook.exception.ApplicationException;

public class MockDealerDAO implements IDealerDAO
{

private Dealer dealer;
private List dealerList = new ArrayList();
private List dealerUpgrades;
private MockDatabase database;

public MockDealerDAO( MockDatabase database )
{
	super();
	this.database = database;
}

public Dealer findByPk( int dealerId )
{
	JXPathContext context = JXPathContext.newContext( dealerList );
	context.getVariables().declareVariable( "dealerId", new Integer( dealerId ) );
	Dealer matching = (Dealer)context.getValue( ".[dealerId = $dealerId]" );

	return matching;
}

public Collection<Dealer> findByDealerGroupId( int dealerGroupId )
{
	JXPathContext context = JXPathContext.newContext( dealerList );
	context.getVariables().declareVariable( "dealerGroupId", new Integer( dealerGroupId ) );
	Iterator matching = context.iterate( ".[dealerGroupId = $dealerGroupId]" );

	return IteratorUtils.toList( matching );
}

public void addDealer( Dealer dealer )
{
	dealerList.add( dealer );
}

public Collection findActiveBy( IMember member ) throws ApplicationException
{
	JXPathContext context = JXPathContext.newContext( dealerList );
	context.getVariables().declareVariable( "active", Boolean.TRUE );
	Iterator matching = context.iterate( ".[active = $active]" );

	return IteratorUtils.toList( matching );
}

public Collection findActiveDealers( Collection dealerIds ) throws ApplicationException
{
	return null;
}

public List findActiveGuideBookDealers( int guidebookId )
{
	return null;
}

public Collection findUpgrades( int dealerId ) throws DatabaseException
{
	return dealerUpgrades;
}

public void setDealerUpgrades( List list )
{
	dealerUpgrades = list;
}

public void saveOrUpdateUpgrade( DealerUpgrade upgrade ) throws DatabaseException
{

}

public List findActiveDealersWithRedistributionBy( int dealerGroupId )
{
	return null;
}

public Collection findActiveDealersForRunDay( int runDay ) throws DatabaseException
{
	Collection dealers = new ArrayList();
	dealers.add( new Dealer() );

	return dealers;
}

public List findActiveDealers()
{
	return null;
}

public Collection findByActiveAndProgramType( boolean active, int programType ) throws DatabaseException
{
	return null;
}

public void saveOrUpdate( Dealer dealer )
{
}

public void delete( Dealer dealer )
{
}

public void save( Dealer dealer )
{
}

public List<Dealer> findByDealerCode( String dealerCode )
{
	return null;
}

public Collection findDealersWithNoDRT( List dealerIds )
{
	return null;
}

public Collection findByDealerCodeMultiple( String partialDealerCode )
{
	return null;
}

public Collection findByName( String name )
{
	return null;
}

public List findByDealerIds( Collection dealerIds )
{
	return null;
}

public Collection findByDealerGroupIdAndNameAndNickName( int dealerGroupId, String name, String nickName )
{
	return null;
}

public Collection findByDealerGroupIdAndNameAndNickName( Collection dealerGroupCol, String name, String nickName )
{
	return null;
}

public Dealer getDealer()
{
	return dealer;
}

public void setDealer( Dealer dealer )
{
	this.dealer = dealer;
}

public Collection findActiveGuideBookDealersWithSpring( int guidebookId )
{
	return null;
}
}