package com.firstlook.persistence.dealer;

import java.util.Collection;
import java.util.Iterator;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.DealerFranchise;

public class DealerFranchiseDAO extends HibernateDaoSupport implements IDealerFranchiseDAO
{

public DealerFranchiseDAO()
{
	super();
}

/**
 * @deprecated - Spring refactoring
 */
public Collection findByDealerId( int dealerId )
{
	Collection collection = null;

	collection = IMTDatabaseUtil.instance().find(
													"from com.firstlook.entity.DealerFranchise dealerFranchise where dealerFranchise.dealerId = ?",
													new Object[] { new Integer( dealerId ) }, new Type[] { Hibernate.INTEGER } );

	return collection;

}

public Collection findByDealerIdWithSpring( int dealerId )
{
	Collection collection = (Collection)getHibernateTemplate().find(
										"from com.firstlook.entity.DealerFranchise dealerFranchise where dealerFranchise.dealerId = ?",
										new Integer( dealerId ) );

	return collection;

}

public void deleteByDealerId( int dealerId )
{

	Collection franchise = findByDealerId( dealerId );
	Iterator franchiseiter = franchise.iterator();
	while ( franchiseiter.hasNext() )
	{
		IMTDatabaseUtil.instance().delete( (DealerFranchise)franchiseiter.next() );
	}

}

}