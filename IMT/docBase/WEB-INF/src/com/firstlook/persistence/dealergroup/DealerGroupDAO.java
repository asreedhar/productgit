package com.firstlook.persistence.dealergroup;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.firstlook.entity.DealerGroup;

public class DealerGroupDAO extends HibernateDaoSupport implements IDealerGroupDAO
{
public DealerGroupDAO()
{
	super();
}

public DealerGroup findByDealerGroupId( int dealerGroupId )
{
	DealerGroup dealerGroup = (DealerGroup)getHibernateTemplate().load( DealerGroup.class, new Integer( dealerGroupId ) );

	return dealerGroup;
}

public Collection findByDealerGroupCode( String dealerGroupCode )
{

	String likeDealerGroupCode = "%" + dealerGroupCode + "%";
	return getHibernateTemplate().find(
										"select dealerGroup from com.firstlook.entity.DealerGroup dealerGroup "
												+ " where dealerGroup.dealerGroupCode like ? order by dealerGroup.dealerGroupCode asc",
										likeDealerGroupCode );
}

public Collection findByDealerGroupName( String dealerGroupName )
{
	String likeDealerGroupName = "%" + dealerGroupName + "%";
	return getHibernateTemplate().find(
										"select dealerGroup from com.firstlook.entity.DealerGroup dealerGroup "
												+ " where dealerGroup.name like ? order by dealerGroup.name asc", likeDealerGroupName );
}

public Collection findByActiveDealerGroups()
{
	String query = "from com.firstlook.entity.DealerGroup d " + " where d.active = ? " + " order by d.name asc";

	return getHibernateTemplate().find( query, Boolean.TRUE );
}

public Collection<DealerGroup> findInDealerGroupIdList( Collection idList )
{

	if ( idList != null && !idList.isEmpty() )
	{
		return (Collection<DealerGroup>)getHibernateTemplate().execute( new BusinessUnitIdsCallback( idList ) );
	}
	else
	{
		return Collections.EMPTY_LIST;
	}

}

public void saveOrUpdate( DealerGroup dealerGroup )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
	getHibernateTemplate().saveOrUpdate( dealerGroup );
}

public void delete( DealerGroup dealerGroup )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
	getHibernateTemplate().delete( dealerGroup );
}

public void save( DealerGroup dealerGroup )
{
	getHibernateTemplate().setFlushMode( HibernateAccessor.FLUSH_EAGER );
	getHibernateTemplate().save( dealerGroup );
}

private class BusinessUnitIdsCallback implements HibernateCallback
{

private final Collection ids;

private BusinessUnitIdsCallback( Collection ids )
{
	this.ids = ids;
}

public Object doInHibernate( org.hibernate.Session session ) throws HibernateException, SQLException
{
	Criteria crit = session.createCriteria( DealerGroup.class );
	crit.add( Expression.in( "businessUnitId", ids ) );
	return crit.list();
}

}

public DealerGroup findByDealerId( int dealerId )
{
	List dealerGroups = getHibernateTemplate().find(
														"select dg from com.firstlook.entity.DealerGroup dg, com.firstlook.entity.BusinessUnitRelationship bur"
																+ " where bur.businessUnitId = ? " + " and dg.businessUnitId = bur.parentId",
														new Integer( dealerId ) );

	if ( !dealerGroups.isEmpty() )
	{
		return (DealerGroup)dealerGroups.get( 0 );
	}
	else
	{
		return null;
	}
}

}