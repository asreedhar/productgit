package com.firstlook.persistence.dealergroup;

import java.util.Collection;

import biz.firstlook.commons.sql.RuntimeDatabaseException;

import com.firstlook.entity.DealerGroup;

public interface IDealerGroupDAO
{
public DealerGroup findByDealerGroupId( int dealerGroupId )
        throws RuntimeDatabaseException;

public DealerGroup findByDealerId( int dealerId ) throws RuntimeDatabaseException;

public Collection findByDealerGroupCode( String dealerGroupCode );

public Collection findByDealerGroupName( String dealerGroupName );

public Collection<DealerGroup> findInDealerGroupIdList( Collection idList );

public void saveOrUpdate( DealerGroup dealerGroup );

public void delete( DealerGroup dealerGroup );

public void save( DealerGroup dealerGroup );

public Collection findByActiveDealerGroups();

}