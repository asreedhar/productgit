package com.firstlook.persistence.dealergroup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.jxpath.JXPathContext;

import biz.firstlook.commons.sql.RuntimeDatabaseException;

import com.firstlook.data.mock.MockDatabase;
import com.firstlook.entity.DealerGroup;
import com.firstlook.exception.ApplicationException;

public class MockDealerGroupDAO implements IDealerGroupDAO
{

MockDatabase database;
List dealerGroupList = new ArrayList();

public MockDealerGroupDAO( MockDatabase database )
{
    super();
    this.database = database;
}

public Collection findByActiveAndProgramType( boolean active, int programTypeCD )
        throws ApplicationException
{

    JXPathContext context = JXPathContext.newContext(dealerGroupList);
    context.getVariables().declareVariable("active", new Boolean(active));
    Iterator activeDealerGroups = context.iterate(".[active = $active]");

    JXPathContext context2 = JXPathContext.newContext(activeDealerGroups);
    context2.getVariables().declareVariable("programTypeCD",
            new Integer(programTypeCD));
    Iterator matching = context2.iterate(".[programTypeCD = $programTypeCD]");

    return IteratorUtils.toList(matching);
}

public DealerGroup findByDealerGroupId( int dealerGroupId )
        throws RuntimeDatabaseException
{
	return null;
}

public void addDealerGroup( DealerGroup dealerGroup )
{
    dealerGroupList.add(dealerGroup);
}

public Collection findByDealerGroupCode( String dealerGroupCode )
{
    return null;
}

public Collection findByDealerGroupName( String dealerGroupName )
{
    return null;
}

public Collection findInDealerGroupIdList( Collection idList )
{
    return null;
}

public void saveOrUpdate( DealerGroup dealerGroup )
{
}

public void delete( DealerGroup dealerGroup )
{
}

public void save( DealerGroup dealerGroup )
{
}

public DealerGroup findByDealerId( int dealerId )
{
    Collection dealerGroupVector = database.getReturnObjects( "com.firstlook.entity.DealerGroup" );
    DealerGroup[] dealerGroups = (DealerGroup[])dealerGroupVector.toArray( new DealerGroup[dealerGroupVector.size()]);
    if( dealerGroups.length > 0 )
    {
        return dealerGroups[0];
    }
    else
    {
        return null;
    }
}

public List findByParentId( int dealerId )
{
	return null;
}

public Collection findByActiveDealerGroups()
{
	return null;
}

}
