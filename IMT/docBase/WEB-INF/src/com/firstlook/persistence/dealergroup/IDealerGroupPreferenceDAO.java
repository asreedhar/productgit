package com.firstlook.persistence.dealergroup;

import com.firstlook.entity.DealerGroupPreference;

public interface IDealerGroupPreferenceDAO
{

public DealerGroupPreference findByPk( Integer dealerGroupPreferenceId );

public DealerGroupPreference findByDealerGroupIdWithSpring( Integer dealerGroupId );

}