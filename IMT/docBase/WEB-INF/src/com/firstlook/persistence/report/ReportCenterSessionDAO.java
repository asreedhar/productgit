package com.firstlook.persistence.report;

import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.CacheMode;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.firstlook.entity.ReportCenterSession;

public class ReportCenterSessionDAO extends HibernateDaoSupport
{

private static final Log logger = LogFactory.getLog( ReportCenterSessionDAO.class );

/**
 * @param sessionId
 * @param businessUnitId
 * @param memberId
 * @return The current active ReportCenterSession row (doAuthentication = 1) for a MemberID
 */
public ReportCenterSession retrieveBy( final Integer memberId )
{
	final StringBuffer hql = new StringBuffer();
	hql.append( "from com.firstlook.entity.ReportCenterSession rcs" );
	hql.append( " where rcs.memberId = :memberId " );
	hql.append( " and rcs.doAuthentication = :doAuth " );
	hql.append( " order by rcs.accessTime desc, rcs.reportCenterSessionId desc" );

	//assumes that query is ordered by latest first
	return (ReportCenterSession)getHibernateTemplate().execute( new HibernateCallback()
	{
		public Object doInHibernate( Session session ) throws HibernateException, SQLException
		{
			Query query = session.createQuery( hql.toString() );
			query.setParameter( "memberId", memberId );
			query.setParameter( "doAuth", 1 );

			ScrollableResults scrollableRcsRows = query.setCacheMode( CacheMode.IGNORE ).scroll( ScrollMode.FORWARD_ONLY );
			ReportCenterSession rcs = null;

			//there should never be more than one row with doAuthentication = 1, assumes order is accessTime desc.
			int count = 0;
			while( scrollableRcsRows.next() ) {
				ReportCenterSession rcSession = (ReportCenterSession)scrollableRcsRows.get(0);
				
				if( count > 0 )
					rcSession.setDoAuthentication( 0 );
				else
					rcs = rcSession;
				
				if( ++count % 20 == 0 ) {
					//flush a batch of updates and release memory:
					session.flush();
					session.clear();
				}
			}
			return rcs;
		}
	} );
}

public void saveOrUpdate( final ReportCenterSession reportCenterSession )
{
	getHibernateTemplate().saveOrUpdate( reportCenterSession );
}

/**
 * The semantics for a delete is not to delete the row in the database, but to flip the doAuthentication flag as an (active/inactive) type of
 * "partitioning".
 * 
 * @param reportCenterSession
 */
public void delete( final ReportCenterSession reportCenterSession )
{
	reportCenterSession.setDoAuthentication( 0 );
	logger.debug( "\"Deleting\": " + reportCenterSession );
	saveOrUpdate( reportCenterSession );
}
}
