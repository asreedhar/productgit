package com.firstlook.persistence.report;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.commons.sql.SqlParameterWithValue;
import biz.firstlook.commons.sql.StoredProcedureTemplate;
import biz.firstlook.commons.sql.StoredProcedureTemplateParameters;

import com.firstlook.report.PricingListLineItem;

public class PricingListReportRetriever extends StoredProcedureTemplate  {

	public List<PricingListLineItem> retrievePricingListLineItems(Integer businessUnitId, Boolean retailOnly) {
		List<SqlParameter> parameters = new LinkedList<SqlParameter>(); //faster inserts
		parameters.add( new SqlParameterWithValue( "@BusinessunitID", Types.INTEGER, businessUnitId ) );
		parameters.add( new SqlParameterWithValue( "@ShowRepricing", Types.BOOLEAN, Boolean.TRUE) );
		parameters.add( new SqlParameterWithValue( "@RetailOnly", Types.BOOLEAN, retailOnly) );

		StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();
		// Externalize sproc name? can change with a app server reboot.
		sprocRetrieverParams.setStoredProcedureCallString( "{? = call dbo.GetPricingListReport (?,?,?)}" );
		sprocRetrieverParams.setSqlParametersWithValues( parameters );

		Map pricingListLineItemResults = this.call( sprocRetrieverParams, new PricingListReportRowMapper() );

		List<PricingListLineItem> lineItems = null;

		Integer returnCode = (Integer)pricingListLineItemResults.get( RETURN_CODE );
		if ( returnCode.intValue() == SUCCESS_RETURN_CODE )
		{
			lineItems = (List)pricingListLineItemResults.get( RESULT_SET );
		}

		return lineItems;
	}


	/**
	 * Inner Class for Mapping Result Set
	 * @author nkeen
	 */
	protected class PricingListReportRowMapper implements RowMapper {

		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			PricingListLineItem item = new PricingListLineItem();

			item.setVehicleDescription((String) rs
					.getObject("VehicleDescription"));
			item.setBaseColor((String) rs.getObject("Color"));
			item.setStockNumber((String) rs.getObject("StockNumber"));
			item.setVehicleYear(((Integer) rs.getObject("VehicleYear"))
					.intValue());
			item.setMileageReceived(((Integer) rs.getObject("Mileage"))
					.intValue());
			item.setListPrice(((BigDecimal) rs.getObject("Price"))
					.doubleValue());
			item.setLotPrice(rs.getInt("LotPrice"));
			item.setPricingNotes(rs.getString("PricingNotes"));
			item.setRepriced(((Boolean) rs.getObject("Repriced"))
					.booleanValue());
			item.setSegment(rs.getString("Segment"));
			return item;
		}

	}

}
