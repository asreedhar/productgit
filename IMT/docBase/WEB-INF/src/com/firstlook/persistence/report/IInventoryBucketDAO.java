package com.firstlook.persistence.report;

import com.firstlook.entity.InventoryBucket;

public interface IInventoryBucketDAO
{

public InventoryBucket findByRangeSetId( Integer rangeSetId );

}