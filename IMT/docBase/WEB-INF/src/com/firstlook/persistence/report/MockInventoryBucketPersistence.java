package com.firstlook.persistence.report;

import java.util.Collection;

import com.firstlook.data.mock.MockDatabase;
import com.firstlook.entity.InventoryBucket;

public class MockInventoryBucketPersistence implements IInventoryBucketDAO
{

private MockDatabase database;

public MockInventoryBucketPersistence( MockDatabase database )
{
    super();
    this.database = database;
}

public InventoryBucket findByRangeSetId( Integer rangeSetId )
{
    Collection inventoryBucketVector = database.getReturnObjects( "com.firstlook.entity.InventoryBucket" );
    InventoryBucket[] inventoryBuckets = (InventoryBucket[])inventoryBucketVector.toArray( new InventoryBucket[inventoryBucketVector.size()]);
    if( inventoryBuckets.length > 0 )
    {
        return inventoryBuckets[0];
    }
    else
    {
        return null;
    }
}

}
