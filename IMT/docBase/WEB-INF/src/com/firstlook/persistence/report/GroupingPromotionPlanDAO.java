package com.firstlook.persistence.report;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.firstlook.entity.GroupingPromotionPlan;

public class GroupingPromotionPlanDAO extends HibernateDaoSupport
{

private TransactionTemplate transactionTemplate;

public GroupingPromotionPlan findPlansByStatusAndGroupingDescriptionId( final Integer businessUnitId, final Integer groupingDescriptionId )
{
	return (GroupingPromotionPlan)getHibernateTemplate().execute( new HibernateCallback()
	{
		public Object doInHibernate( Session session ) throws HibernateException, SQLException
		{
			StringBuffer hqlQuery = new StringBuffer();
			hqlQuery.append( " select plan from com.firstlook.entity.GroupingPromotionPlan as plan" );
			hqlQuery.append( " where plan.businessUnitId = :businessUnitId " );
			hqlQuery.append( " and plan.groupingDescriptionId = :groupingDescriptionId " );
			// hack to get latest plan -bf.
			hqlQuery.append( " order by plan.groupingDescriptionId desc " );

			Query returnQuery = session.createQuery( hqlQuery.toString() );
			returnQuery.setParameter( "businessUnitId", businessUnitId );
			returnQuery.setParameter( "groupingDescriptionId", groupingDescriptionId );

			List results = returnQuery.list();
			if ( results != null && !results.isEmpty() )
			{
				return (GroupingPromotionPlan)results.get( 0 );
			}
			else
			{
				return null;
			}
		}
	} );
}


public void saveOrUpdate( final GroupingPromotionPlan groupingPromotionPlan )
{
	// transaction code should go into a service method call.
	transactionTemplate.execute( new TransactionCallbackWithoutResult()
	{
		public void doInTransactionWithoutResult( TransactionStatus status )
		{
			getHibernateTemplate().saveOrUpdate( groupingPromotionPlan );
		}
	} );
}

public TransactionTemplate getTransactionTemplate()
{
	return transactionTemplate;
}

public void setTransactionTemplate( TransactionTemplate transactionTemplate )
{
	this.transactionTemplate = transactionTemplate;
}

}
