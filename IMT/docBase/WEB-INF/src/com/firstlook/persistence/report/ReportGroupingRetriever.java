package com.firstlook.persistence.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.commons.sql.SqlParameterWithValue;
import biz.firstlook.commons.sql.StoredProcedureTemplate;
import biz.firstlook.commons.sql.StoredProcedureTemplateParameters;

import com.firstlook.data.DatabaseException;
import com.firstlook.report.ReportGrouping;

public class ReportGroupingRetriever extends StoredProcedureTemplate
{

public ReportGrouping retrieveReportgrouping( int businessUnitId,
        Date startDate, Date endDate, int groupingDescriptionId,
        int includeDealerGroup, int inventoryType, int mileage )
        throws DatabaseException
{
	
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); //faster inserts
	parameters.add( new SqlParameterWithValue( "@businessUnitId", Types.INTEGER,new Integer(businessUnitId) ) );
	parameters.add( new SqlParameterWithValue( "@startDate", Types.DATE, startDate ) );
	parameters.add( new SqlParameterWithValue( "@endDate", Types.DATE, endDate ) );
	parameters.add( new SqlParameterWithValue( "@groupingDescriptionId", Types.INTEGER, new Integer(groupingDescriptionId)) );
	parameters.add( new SqlParameterWithValue( "@includeDealerGroup", Types.INTEGER,  new Integer(includeDealerGroup) ) );
	parameters.add( new SqlParameterWithValue( "@invType", Types.INTEGER, new Integer(inventoryType) ) );
	parameters.add( new SqlParameterWithValue( "@mileage", Types.INTEGER,  new Integer(mileage) ) );

	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();
	sprocRetrieverParams.setStoredProcedureCallString( "{? = call usp_GetReportGrouping (?, ?, ?, ?, ?, ?, ?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );

	Map results = this.call( sprocRetrieverParams, new ReportGroupingMapper() );

	LinkedList<ReportGrouping> resultsList = (LinkedList<ReportGrouping>)results.get( StoredProcedureTemplate.RESULT_SET );
	return ( resultsList != null && !resultsList.isEmpty()) ? resultsList.get(0) : new ReportGrouping();
}

private class ReportGroupingMapper implements RowMapper
{

	public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
	{
	    ReportGrouping grouping = new ReportGrouping();
        grouping.setAvgDaysToSale((Integer) rs.getObject("AverageDays"));
        grouping.setAvgGrossProfit((Integer) rs.getObject("AverageGrossProfit"));
        grouping.setAvgMileage((Integer) rs.getObject("AverageMileage"));
        grouping.setTotalSalesPrice((Integer) rs.getObject("TotalSalesPrice"));
        grouping.setTotalGrossMargin((Integer) rs.getObject("TotalGrossMargin"));
        grouping.setUnitsSold(rs.getInt("UnitsSold"));
        grouping.setAverageBackEnd((Integer) rs.getObject("AverageBackEndGrossProfit"));
        grouping.setTotalBackEnd((Integer) rs.getObject("TotalBackEndGrossProfit"));
        grouping.setGroupingName(rs.getString("VehicleGroupingDescription"));
        grouping.setGroupingId(rs.getInt("GroupingId"));
        grouping.setNoSales(rs.getInt("No_Sales"));
        grouping.setAverageFrontEnd((Integer) rs.getObject("AverageGrossProfit"));
        grouping.setTotalRevenue((Integer) rs.getObject("TotalRevenue"));
        grouping.setTotalFrontEnd((Integer) rs.getObject("TotalFrontEnd"));
        grouping.setTotalInventoryDollars((Integer) rs.getObject("TotalInventoryDollars"));
        grouping.setUnitsInStock(rs.getInt("InventoryCount"));
        return grouping;
	}
}

}
