package com.firstlook.persistence.report;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;

import biz.firstlook.commons.sql.SqlParameterWithValue;
import biz.firstlook.commons.sql.StoredProcedureTemplate;
import biz.firstlook.commons.sql.StoredProcedureTemplateParameters;

import com.firstlook.entity.ReportGrouping;
import com.firstlook.entity.TradePurchaseEnum;
import com.firstlook.entity.form.SaleForm;

public class DealLogReportRetriever extends StoredProcedureTemplate
{

public List<ReportGrouping> retrieveDealLogLineItems( int dealerId, Date startDate, String saleDescription, int inventoryType )
{
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); //faster inserts
	parameters.add( new SqlParameterWithValue( "@dealerId", Types.INTEGER, new Integer( dealerId ) ) );
	parameters.add( new SqlParameterWithValue( "@startDate", Types.TIMESTAMP, startDate) );
	parameters.add( new SqlParameterWithValue( "@saleDescription", Types.VARCHAR, saleDescription ) );
	parameters.add( new SqlParameterWithValue( "@inventoryType", Types.INTEGER, new Integer( inventoryType )) );

	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();

	sprocRetrieverParams.setStoredProcedureCallString( "{? = call dbo.GetDealLogReport (?, ?, ?, ?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );

	Map results = this.call( sprocRetrieverParams, new DealLogReportMapper() );
	LinkedList<ReportGrouping> resultsList = (LinkedList<ReportGrouping>)results.get( StoredProcedureTemplate.RESULT_SET );
	return ( resultsList != null && !resultsList.isEmpty()) ? resultsList : new ArrayList<ReportGrouping>();
}

private class DealLogReportMapper implements RowMapper
{

	public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
	{
		SaleForm saleForm = new SaleForm();
		saleForm.setDealDate( new Date( rs.getTimestamp( "DealDate" ).getTime() ) );
		saleForm.setMake( (String)rs.getObject( "Make" ) );
		saleForm.setModel( (String)rs.getObject( "Model" ) );
		saleForm.setTrim( (String)rs.getObject( "VehicleTrim" ) );
		saleForm.setBodyStyle( (String)rs.getObject( "Description" ) );
		saleForm.setStockNumber( (String)rs.getObject( "StockNumber" ) );
		saleForm.setUnitCost( ( (BigDecimal)rs.getObject( "UnitCost" ) ).doubleValue() );
		saleForm.setMileage( ( (Integer)rs.getObject( "Mileage" ) ).intValue() );
		saleForm.setLight( ( (Integer)rs.getObject( "InitialLight" ) ).intValue() );
		saleForm.setYear( ( (Integer)rs.getObject( "VehicleYear" ) ).intValue() );
		saleForm.setBaseColor( (String)rs.getObject( "Color" ) );
		saleForm.setDealNumber( (String)rs.getObject( "DealNumber" ) );
		saleForm.setRetailGrossProfit( ( (BigDecimal)rs.getObject( "FrontEndGross" ) ).doubleValue() );
		saleForm.setFIGrossProfit( ( (BigDecimal)rs.getObject( "BackEndGross" ) ).doubleValue() );
		saleForm.setSalePrice( ( (BigDecimal)rs.getObject( "SalePrice" ) ).doubleValue() );
		saleForm.setDaysToSale( ( (Integer)rs.getObject( "DaysToSale" ) ).longValue() );
		saleForm.setTradePurchase( TradePurchaseEnum.getEnum( (String)rs.getObject( "T/P" ) ) );
		return saleForm;
	}

}

}
