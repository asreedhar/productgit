package com.firstlook.persistence.report;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import biz.firstlook.commons.sql.SqlParameterWithValue;
import biz.firstlook.commons.sql.StoredProcedureTemplate;
import biz.firstlook.commons.sql.StoredProcedureTemplateParameters;
import com.firstlook.report.DealerTradePurchaseLineItem;

public class PurchaseReportRetriever extends StoredProcedureTemplate
{

public List<DealerTradePurchaseLineItem> retrieveDealerTradePurchaseLineItems( int dealerId, int weeks )
{
	List<SqlParameter> parameters = new LinkedList<SqlParameter>(); //faster inserts
	parameters.add( new SqlParameterWithValue( "@businessUnitId", Types.INTEGER, dealerId ) );
	parameters.add( new SqlParameterWithValue( "@weeks", Types.INTEGER, weeks ) );
	parameters.add( new SqlParameterWithValue( "@BaseDate", Types.NULL, null) );

	StoredProcedureTemplateParameters sprocRetrieverParams = new StoredProcedureTemplateParameters();

	sprocRetrieverParams.setStoredProcedureCallString( "{? = call dbo.GetPurchaseReport (?, ?, ?)}" );
	sprocRetrieverParams.setSqlParametersWithValues( parameters );

	Map results = this.call( sprocRetrieverParams, new PurchaseReportMapper() );
	LinkedList<DealerTradePurchaseLineItem> resultsList = (LinkedList<DealerTradePurchaseLineItem>)results.get( StoredProcedureTemplate.RESULT_SET );
	return ( resultsList != null && !resultsList.isEmpty()) ? resultsList : new ArrayList();
}

private class PurchaseReportMapper implements RowMapper
{

	public Object mapRow( ResultSet rs, int arg1 ) throws SQLException
	{
        DealerTradePurchaseLineItem dealerTradePurchaseLineItem = new DealerTradePurchaseLineItem();
        dealerTradePurchaseLineItem.setReceivedDate(new Date(rs.getTimestamp("Received Date").getTime()));
        dealerTradePurchaseLineItem.setVin((String) rs.getObject("VIN"));
        dealerTradePurchaseLineItem.setVehicleDescription((String) rs.getObject("Vehicle Description"));
        dealerTradePurchaseLineItem.setUnitCost(((BigDecimal) rs.getObject("Unit Cost")).doubleValue());
        dealerTradePurchaseLineItem.setMileage(((Integer) rs.getObject("Mileage")).intValue());
        dealerTradePurchaseLineItem.setLight(((Integer) rs.getObject("Risk Level")).intValue());
        dealerTradePurchaseLineItem.setPriorOwner((String) rs.getObject("Prior Owner"));
        Timestamp ts = rs.getTimestamp("Date Purchase Analyzed");
        if ( ts == null )
        {
            dealerTradePurchaseLineItem.setTradeAnalyzerDate(ts);
        } else
        {
            dealerTradePurchaseLineItem.setTradeAnalyzerDate(new Date(ts.getTime()));
        }
        ts = rs.getTimestamp("First Received By Group");
        if ( ts == null )
        {
            dealerTradePurchaseLineItem.setDateGroupReceived(ts);
        } else
        {
            dealerTradePurchaseLineItem.setDateGroupReceived(new Date(ts.getTime()));
        }
        return dealerTradePurchaseLineItem;
	}

}

}
