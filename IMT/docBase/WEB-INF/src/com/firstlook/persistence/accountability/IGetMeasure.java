package com.firstlook.persistence.accountability;

public interface IGetMeasure {

    public abstract float assembleMeasure(int currentDealerId, String period, String section, String group, String set, String metric);

    public abstract String assembleCommentMeasure(int currentDealerId, String period, String section, String group, String set, String metric);

    public abstract String assembleFormattedMeasure(int currentDealerId, String period, String section, String group, String set, String metric);

}