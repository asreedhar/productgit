package com.firstlook.persistence.vehicle;

import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;
import com.firstlook.entity.OptionDetail;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.vehicle.OptionDetailService;

public class OptionDetailPersistence
{

private static Hashtable idToNameCache = null;
private static Hashtable nameToIdCache = null;
private static Object cacheSyncObj = new Object();

public OptionDetailPersistence()
{
    super();
}

public Collection findAll()
{
    Collection collection = null;

    collection = IMTDatabaseUtil.instance().find(
            "from com.firstlook.entity.OptionDetail ");

    return collection;
}

public OptionDetail findByName( String name )
{
    Collection coll;
    coll = IMTDatabaseUtil.instance().find(
            "from com.firstlook.entity.OptionDetail "
                    + " where optionName = ? ", new Object[]
            { new String(name) }, new Type[]
            { Hibernate.STRING });
    if ( coll != null && !coll.isEmpty() )
    {
        return (OptionDetail) (coll.toArray())[0];
    } else
    {
        return null;
    }
}

private Map getIdToNameCache() throws ApplicationException
{
    if ( idToNameCache == null )
    {
        initializeCache();
    }

    return idToNameCache;
}

private Map getNameToIdCache() throws ApplicationException
{
    if ( nameToIdCache == null )
    {
        initializeCache();
    }

    return nameToIdCache;
}

public String getNameById( int optionDetailId ) throws ApplicationException
{
    return (String) getIdToNameCache().get(new Integer(optionDetailId));
}

public int getIdByName( String optionDetailName ) throws ApplicationException
{
    Object idStr = getNameToIdCache().get(optionDetailName);
    if ( idStr != null )
    {
        return ((Integer) idStr).intValue();
    } else
    {
        return Integer.MIN_VALUE;
    }

}

public void clearCache()
{
    idToNameCache = null;
    nameToIdCache = null;
}

private void initializeCache() throws ApplicationException
{
    OptionDetailService service = new OptionDetailService();
    synchronized (cacheSyncObj)
    {
        idToNameCache = new Hashtable();
        nameToIdCache = new Hashtable();
        Collection optionDetailCol = service.retrieveAll();
        Iterator it = optionDetailCol.iterator();
        while (it.hasNext())
        {
            OptionDetail curVal = (OptionDetail) it.next();
            idToNameCache.put(new Integer(curVal.getOptionDetailId()), curVal
                    .getOptionName());
            nameToIdCache.put(curVal.getOptionName(), new Integer(curVal
                    .getOptionDetailId()));
        }
    }

}

}