package com.firstlook.persistence.vehicle;

import java.util.Collection;

import org.hibernate.Hibernate;
import org.hibernate.type.Type;

import com.firstlook.db.factory.hibernate.IMTDatabaseUtil;

public class VehicleOptionPersistence
{

public VehicleOptionPersistence()
{
    super();
}

public Collection findByVehicleId( int vehicleId )
{
    Collection collection = null;

    collection = IMTDatabaseUtil.instance().find(
            "from com.firstlook.entity.VehicleOption "
                    + " where vehicleId = ? ", new Object[]
            { new Integer(vehicleId) }, new Type[]
            { Hibernate.INTEGER });

    return collection;
}

}
