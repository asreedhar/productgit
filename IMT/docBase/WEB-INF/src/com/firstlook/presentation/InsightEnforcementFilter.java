package com.firstlook.presentation;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import biz.firstlook.transact.persist.model.UserRoleEnum;

import com.firstlook.entity.InventoryTypeEnum;
import com.firstlook.entity.Member;
import com.firstlook.entity.ProgramTypeEnum;
import com.firstlook.session.FirstlookSession;
import com.firstlook.session.Product;
import com.firstlook.session.ProductDao;

public class InsightEnforcementFilter implements Filter
{

private static Logger logger = Logger.getLogger( InsightEnforcementFilter.class );

private ProductDao productDao;

public void init( FilterConfig config ) throws ServletException
{
	WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext( config.getServletContext() );
	productDao = (ProductDao)wac.getBean( "productService" );
}

public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain ) throws ServletException, IOException
{
	HttpServletRequest httpRequest = (HttpServletRequest)request;
	HttpSession session = httpRequest.getSession( true );
	FirstlookSession firstlookSession = (FirstlookSession)session.getAttribute( FirstlookSession.FIRSTLOOK_SESSION );
	if ( firstlookSession != null )
	{
		Member member = firstlookSession.getMember();
		// Add a final pass to set Used if Insight - this runs on every request.
		try
		{
			boolean isAdmin = (member.getMemberType() != null && member.getMemberType().intValue() == Member.MEMBER_TYPE_ADMIN);
			boolean isInsight = (member.getProgramType() != null && member.getProgramType().equals(ProgramTypeEnum.INSIGHT));
			if ( !isAdmin && isInsight )
			{
				member.setInventoryType(InventoryTypeEnum.getEnum(UserRoleEnum.USED) );
				firstlookSession.setProduct( Product.EDGE );
			}

			// XXX: TODO: Hack to set variable since our application pages are not stateless
			if ( member.getInventoryType() == null  )
			{
				if( firstlookSession.getMode() != null )
				{
					Product product = productDao.findByName( firstlookSession.getMode() );
					firstlookSession.getMember().setUserRoleEnum( product.getUserRoleEnum() );
					session.setAttribute( FirstlookSession.FIRSTLOOK_SESSION, firstlookSession );
				}
				else
				{
					logger.warn( "FirstlookSession.getMode() was null, can't set UserRole!" );
				}
			}
		}
		catch ( Exception ae )
		{
			logger.error( "Problem while getting dealerGroup from member", ae );
		}
	}
	chain.doFilter( request, response );
}

public void destroy()
{
}

}
