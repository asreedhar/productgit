package com.firstlook.presentation;

import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.functors.AnyPredicate;
import org.apache.commons.collections.functors.EqualPredicate;
import org.apache.commons.lang.StringUtils;

import biz.firstlook.commons.util.PropertyLoader;

// TODO: We shouldn't be hardcoding "resource" in our code.  This should be generalized.
public class HostTypeResolver
{

private static HostTypeResolver instance;

public Predicate matchesInsight;
public Predicate matchesResource;

private HostTypeResolver()
{
	super();
}

public synchronized static HostTypeResolver getInstance()
{
	if ( instance == null )
	{
		instance = new HostTypeResolver();
		instance.init();
	}
	return instance;
}

public void init()
{
	configureInsightUrls();
	configureResourceUrls();
}

private void configureResourceUrls()
{
	String resourceUrlsString = PropertyLoader.getProperty( "firstlook.resourcevip.url" );
	String[] resourceUrls = resourceUrlsString.split( "," );
	Predicate[] resourcePredicates = new Predicate[resourceUrls.length];
	for ( int i = 0; i < resourceUrls.length; i++ )
	{
		resourcePredicates[i] = new EqualPredicate( resourceUrls[i].trim() );
	}
	matchesResource = new AnyPredicate( resourcePredicates );
}

private void configureInsightUrls()
{
	String insightUrlsString = PropertyLoader.getProperty( "firstlook.insight.url" );
	String[] insightUrls = insightUrlsString.split( "," );
	Predicate[] insightPredicates = new Predicate[insightUrls.length];
	for ( int i = 0; i < insightUrls.length; i++ )
	{
		insightPredicates[i] = new EqualPredicate( insightUrls[i].trim() );
	}
	matchesInsight = new AnyPredicate( insightPredicates );
}

public boolean isInsightHost( String hostname )
{
	return matchesInsight.evaluate( StringUtils.trim( StringUtils.lowerCase( hostname ) ) );
}

public boolean isResourceHost( String hostname )
{
	return matchesResource.evaluate( StringUtils.trim( StringUtils.lowerCase( hostname ) ) );
}

}
