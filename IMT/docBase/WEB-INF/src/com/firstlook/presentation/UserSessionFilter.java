package com.firstlook.presentation;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import biz.firstlook.commons.filter.RequestWithCookie;
import biz.firstlook.commons.util.CookieHelper;

import com.discursive.cas.extend.client.CASFilter;
import com.firstlook.entity.Member;
import com.firstlook.persistence.member.IMemberDAO;
import com.firstlook.session.FirstlookSession;
import com.firstlook.session.HalSessionMessage;
import com.firstlook.session.HalSessionMessageDao;
import com.firstlook.session.IFirstlookSessionManager;
import com.firstlook.session.Product;

/**
 * This class checks and constructs a FirstLook session for a user session.
 * 
 * This filter assumes that the user is authorized ( the CAS filter must run before this filter ).
 * 
 * This filter assumes that a Hibernate session is active ( the OpenSessionInViewFilter must run before this filter ).
 * 
 * An eventual goal is to replace this filter for a filter with authorization features (Acegi, anyone?)
 * 
 * @see {@link com.discursive.cas.extend.client.CASFilter}
 * @see org.springframework.orm.hibernate3.support.OpenSessionInViewFilter}
 * @author bfung
 * 
 */
public class UserSessionFilter implements Filter
{

public static final String HAL_SESSION_ID = "HALSESSIONID";

private Logger logger = Logger.getLogger( UserSessionFilter.class );

private IFirstlookSessionManager firstlookSessionManager;
private IMemberDAO memberDao;
private HalSessionMessageDao halSessionMessageDao;

public UserSessionFilter()
{
	super();
}

public void init( FilterConfig filterConfig ) throws ServletException
{
	WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext( filterConfig.getServletContext() );
	memberDao = (IMemberDAO)wac.getBean( "memberDAO" );
	firstlookSessionManager = (IFirstlookSessionManager)wac.getBean( "firstlookSessionManager" );
	halSessionMessageDao = (HalSessionMessageDao)wac.getBean( "halSessionMessageDao" );
}

public void doFilter( ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain ) throws ServletException,
		IOException
{
	HttpServletRequest request = (HttpServletRequest)servletRequest;
	HttpServletResponse response = (HttpServletResponse)servletResponse;
	HttpSession session = request.getSession();

	String memberLogin = (String)session.getAttribute( CASFilter.CAS_FILTER_USER );
	if ( memberLogin == null )
	{
		logger.error( "No CAS Receipt for session " + session.getId() + ", check that CAS filter is running before this filter" );
		// redirect probably.
	}

	FirstlookSession firstlookSession = (FirstlookSession)session.getAttribute( FirstlookSession.FIRSTLOOK_SESSION );
	// just gets dealerID from cookie... and does some max stuff
	Integer dealerId = processMemberContext( session, request );
	Member member = null;
	if ( firstlookSession == null || firstlookSession.getProgramType() == null)
	{
		member = memberDao.findByLogin( memberLogin );
		// does  not set dealerId unless already specified 
		firstlookSession = processApplicationSession( request, member, dealerId );

		session.setAttribute( FirstlookSession.FIRSTLOOK_SESSION, firstlookSession );
		logger.debug( "A new FirstlookSession session was created for " + memberLogin );
	}
	else //check businessunit, this should catch all "exit store" functionality.
	{
		if (member == null) {
			member = firstlookSession.getMember();
		}
		if(dealerId != null && (dealerId.intValue() != firstlookSession.getCurrentDealerId() )) {
            //Reestablish firstlookSession      
            member = memberDao.findByLogin( memberLogin );
      
            firstlookSession = processApplicationSession( request, member, dealerId );

            session.setAttribute( FirstlookSession.FIRSTLOOK_SESSION, firstlookSession );

            request = updateCookie( request, response, firstlookSession.getCurrentDealerId() );
		}
	}
	
	String login = getCookieValue(request, CookieHelper.TRUESIGHT_USERNAME_COOKIE_NAME);
	if(login == null && member != null) {
		response.addCookie(CookieHelper.createTrueSightMemberCookie(member.getLogin()));
	}		

	filterChain.doFilter( request, response );
}

/**
 * Overwrite the 'dealerId' cookie that only lives for 1 day.
 * @param request
 * @param response
 * @param dealerId
 * @return
 */
private HttpServletRequest updateCookie( HttpServletRequest request, HttpServletResponse response, Integer dealerId )
{
	Cookie cookie = new Cookie("dealerId", dealerId.toString());
	cookie.setPath("/");
	cookie.setMaxAge( 86400 ); //1 day = 24h/day * 60 min/h * 60 sec/min
	response.addCookie(cookie);
	return new RequestWithCookie( request, cookie);
}

private FirstlookSession processApplicationSession( HttpServletRequest request, Member member, Integer dealerId )
{
	FirstlookSession firstlookSession;
	if ( dealerId != null )
	{
		firstlookSession = firstlookSessionManager.constructFirstlookSession( dealerId.intValue(), member );
	}
	else
	{
		firstlookSession = firstlookSessionManager.constructMemberFirstlookSession( member );
	}
	
	if(isFromHal(request) != null)
	{
		//Cannot put product mode MAX because JSPs expect UCBP or VIP!
		firstlookSession.setProduct(Product.EDGE);
	}

	firstlookSession.getMember().setLoginEntryPoint( processLoginEntryPoint( request.getHeader( "host" ) ) );

	return firstlookSession;
}

/**
 * 
 * @param request
 * @return businessUnitId for the session
 */
private Integer processMemberContext( HttpSession session, HttpServletRequest request )
{
	// i'm already in MAX mode.
	long start = System.currentTimeMillis();
	String halSessionId = (String)session.getAttribute( HAL_SESSION_ID );
	if ( halSessionId == null )
	{
		// try getting it from the request or cookie.
		halSessionId = isFromHal( request );
		if ( halSessionId != null )
		{
			HalSessionMessage halSessionMessage = halSessionMessageDao.findBy( halSessionId );

			// handle this better by cancelling the session and redirecting out.
			if ( halSessionMessage != null ) {
				session.setAttribute( HAL_SESSION_ID, halSessionId );
				return halSessionMessage.getBusinessUnitId();
			}
		}
		//not hal, do normal stuff.
	}

	HostTypeResolver resolver = HostTypeResolver.getInstance();
	String hostname = request.getHeader( "host" );
	if ( resolver.isInsightHost( hostname ) || resolver.isResourceHost( hostname ) )
	{
		// user got out of MAX and went to EDGE or VIP
		session.removeAttribute( HAL_SESSION_ID );
	}

	// LithiaCarCenterDealerId is set in LithiaCarCenterFilter - will be empty on 99% of requests.
	String dealerIdString = ( request.getAttribute( "LithiaCarCenterDealerId" ) != null) ? 
			request.getAttribute( "LithiaCarCenterDealerId" ).toString() : "";
	
	if( StringUtils.isBlank( dealerIdString ) )
	{
		dealerIdString = getCookieValue( request, "dealerId" );
		if( logger.isDebugEnabled() )
		{
			logger.debug( "took " + (System.currentTimeMillis() - start) + "ms." );
		}	
	}	
	
	if( dealerIdString != null )
		return Integer.valueOf( dealerIdString );
	else
		return null;
}

/**
 * 
 * @param request
 * @return null if no hal session id, otherwise the halsessionid
 */
private String isFromHal( HttpServletRequest request )
{
	//try getting it from the request.
	String halSessionId = request.getParameter( HAL_SESSION_ID );
	if ( halSessionId == null )
	{
		// finally, try the cookie.
		halSessionId = getCookieValue( request, "_session_id" );
	}
	return halSessionId;
}

private String getCookieValue( HttpServletRequest request, String property )
{
	// Try to get the last DealerId the user was in.
	Cookie[] cookies = request.getCookies();
	if ( cookies != null )
	{
		// Java 5! for Cookie cookie in cookies
		for ( Cookie cookie : cookies )
		{
			if ( logger.isDebugEnabled() )
			{
				logger.debug( "Cookie[ Name: "
						+ cookie.getName() + ", Value: " + cookie.getValue() + ", Path: " + cookie.getPath() + ", Domain: "
						+ cookie.getDomain() + ", Max Age: " + cookie.getMaxAge() + "]" );
			}

			if ( cookie.getName().equalsIgnoreCase( property ) )
			{
				return cookie.getValue();
			}
		}
	}
	return null;
}

/**
 * Finds the login point for user: vip or edge
 * 
 * @param host
 * @return a static String from the User class
 */
public String processLoginEntryPoint( String host )
{
	if ( HostTypeResolver.getInstance().isResourceHost( host ) )
	{
		return Member.LOGIN_ENTRY_POINT_VIP;
	}
	else
	{
		return Member.LOGIN_ENTRY_POINT_INSIGHT;
	}
}

public void destroy()
{
	firstlookSessionManager = null;
	memberDao = null;
}

}
