package com.firstlook.presentation;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.discursive.cas.extend.client.CASFilter;
import com.firstlook.entity.Dealer;
import com.firstlook.service.dealer.IDealerService;
import com.firstlook.session.FirstlookSession;

/**
 * This Filter exists for the Lithia Car Center work flow.  It only gets hit on requests to the LithiCarCenterAction 
 * which is a direct link to a specific appraisal.   
 * 
 * Part of the requirements for the Lithia Car Center was that a user must be able to click a link
 * and hit the trade manager for a given appraisal - the appraisal can be in any store.  This filter
 * allows this to work.  
 * 
 * What the filter does is checks to see what state you are currently in, then reacts accordingly.  here are the 3 scenerios:
 * 1.  You don't yet have a firstlook session (first time logging in) -> set the current dealer Id field equal to the dealerId of the 
 * in coming appraisal, then forward to the user session filter, which will set up the firstlook session.
 * 2.  You already have a firstlook session and it is for the dealer whose appraisal you are going to view -> nothing needs to be done, 
 * so just a pass through
 * 3.  You alreayd have a firstlook session and it is for the wrong dealership -> we delete the current firstlook session, then set then
 * forward you to the user session filter passing along the correct dealerId.  User sessionfilter will give you a new firstlook session, 
 * for the correct dealership. 
 * 
 * @author dweintrop
 */
public class LithiaCarCenterFilter implements Filter
{

private IAppraisalService appraisalService;
private IDealerService dealerService;

public void init( FilterConfig filterConfig ) throws ServletException
{
	WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext( filterConfig.getServletContext() );
	appraisalService = (IAppraisalService)wac.getBean( "appraisalService" );
	dealerService = (IDealerService)wac.getBean( "imtDealerService" );
	
}

public void doFilter( ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain ) throws IOException, ServletException
{
	HttpServletRequest request = (HttpServletRequest)servletRequest;
	HttpServletResponse response = (HttpServletResponse)servletResponse;
	HttpSession session = request.getSession();
	
	int appraisalId = Integer.parseInt( request.getParameter( "LithiaCarCenterAppraisalId" ) );
	
	IAppraisal appraisal = appraisalService.findBy( appraisalId );
	
	// check if a session already exists. -> if doesn't exist, just set LithiaCarCenterDealerId attribute and continue
	FirstlookSession firstlookSession = (FirstlookSession)session.getAttribute( FirstlookSession.FIRSTLOOK_SESSION );
	if ( firstlookSession != null )	{

		// if current session is for a different dealer than the appraisal you're trying to view - blow away session
		if( firstlookSession.getCurrentDealerId() != appraisal.getBusinessUnitId().intValue() ) {
			cleanSession( session );
		}
	}
	
	Dealer dealer = dealerService.retrieveDealer(appraisal.getBusinessUnitId());
	session.setAttribute( "dealerNick", dealer.getNickname() );
	Cookie cookie = new Cookie("dealerId", appraisal.getBusinessUnitId().toString() );
	cookie.setPath( "/" );
	cookie.setMaxAge( 7 * 24 * 3600 * 1000 );
	response.addCookie( cookie );
	
	// set up attributse for user session filter and tradeManagerEditDisplayAction
	request.setAttribute( "LithiaCarCenterDealerId", appraisal.getBusinessUnitId() );
	request.setAttribute( "pageName", "bullpen" );
	request.setAttribute( "appraisalId", appraisal.getAppraisalId() );
	
	filterChain.doFilter( request, response );
}

private void cleanSession( HttpSession session )
{
	// iterate over all attrs in the session and delete them all except a few.
	Enumeration<String> attributes = session.getAttributeNames();
	while (attributes.hasMoreElements()) {
		String attr = attributes.nextElement();
		if( !CASFilter.CAS_FILTER_USER.equals(attr) &&
			!CASFilter.CAS_FILTER_RECEIPT.equals(attr) &&	
			!"perspective".equals(attr) &&	
			!"org.apache.struts.action.LOCALE".equals(attr)	) {
				session.removeAttribute( attr );
		}
	}
}

public void destroy()
{
	appraisalService = null;
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

}
