
package com.firstlook.presentation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import biz.firstlook.commons.servlet.http.HttpServletRequestHelper;

import com.firstlook.session.FirstlookSession;
import com.firstlook.session.Product;

/**
 * Note: In at least one case (AbstractTradeManagerAction.processTabs) the redirect on this clobbers a method in the middle of execution
 * 
 * Redirects a user to the NextGen implementation, coercing request parameters,
 * if they come from "The Edge".  I could not get this to quickly work with the
 * turnkey urlrewriter so I wrote it by hand.
 * @author swenmouth
 */
public class PerformancePlusFilter implements Filter {

	private Logger logger = Logger.getLogger( PerformancePlusFilter.class );
	public final static String JNDI_INIT_PARAM = "com.discursive.cas.extend.client.config.jndi";
	
	public void init( FilterConfig filterConfig ) throws ServletException {
	}
	
	public void destroy() {
	}
	
	private Pattern[] PATTERNS = new Pattern[] {
			Pattern.compile("^/IMT/(PopUp)PlusDisplayAction.go"),
			Pattern.compile("^/IMT/PlusDisplayAction.go"),
			Pattern.compile("^/IMT/PrintablePlusDisplayAction.go"),
			Pattern.compile("^/IMT/Printable(PopUp)PlusDisplayAction.go")
	};
	
	public void doFilter(ServletRequest requ, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request   = (HttpServletRequest) requ;
		HttpServletResponse response = (HttpServletResponse) resp;
		FirstlookSession flsession = (FirstlookSession) request.getSession().getAttribute(FirstlookSession.FIRSTLOOK_SESSION); 
		if (flsession == null || ( flsession.getMode() != null && !flsession.getMode().equalsIgnoreCase( Product.EDGE.getLegacyName() ))) {
			chain.doFilter(request, response);
		}
		else {
			final String requestURI = request.getRequestURI();
			List<Matcher> matchers = new ArrayList<Matcher>(PATTERNS.length);
			for (Pattern pattern : PATTERNS) {
				matchers.add(pattern.matcher(requestURI));
			}
			boolean useNextGenServlet = false;
			boolean isPopUpWindow = false;
			for (Matcher matcher : matchers) {
				if (matcher.find()) {
					useNextGenServlet = true;
					isPopUpWindow = (matcher.groupCount() > 0 && matcher.group(1) != null);
					break;
				}
			}
			if (useNextGenServlet) {
				int groupingDescriptionId = parseInt(request.getParameter("groupingDescriptionId"), 0);
				int weeks = parseInt(request.getParameter("weeks"), 26);
				boolean applyMileageFilter = parseBoolean(request.getParameter("mileageFilter"), false);
				boolean isForecast = parseBoolean(request.getParameter("forecast"), false);

				String host= HttpServletRequestHelper.getHost(request); 
				StringBuilder sb = new StringBuilder();
				sb.append( HttpServletRequestHelper.getScheme(request) );
				sb.append( "://").append(host);
				sb.append("/NextGen/PerformancePlus.go");
				sb.append("?groupingDescriptionId=").append(groupingDescriptionId);
				sb.append("&weeks=").append(weeks);
				sb.append("&isForecast=").append(isForecast);
				sb.append("&applyMileageFilter=").append(applyMileageFilter);
				sb.append("&isPopup=").append(isPopUpWindow);

				response.sendRedirect(response.encodeRedirectURL( sb.toString()));
			}
			else {
				chain.doFilter(request, response);
			}
		}
	}

	private int parseInt(String str, int def) {
		int i = def;
		try {
			i = Integer.parseInt(str);
		}
		catch (NumberFormatException nfe) {
			logger.warn("error parsing int", nfe);
		}
		return i;
	}
	
	private boolean parseBoolean(String str, boolean def) {
		boolean i = def;
		try {
			i = Boolean.valueOf(parseInt(str,0) != 0);
		}
		catch (NumberFormatException nfe) {
			logger.warn("error parsing boolean", nfe);
		}
		return i;
	}
}
