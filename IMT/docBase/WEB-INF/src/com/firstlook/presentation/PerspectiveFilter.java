package com.firstlook.presentation;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import biz.firstlook.transact.persist.model.UserRoleEnum;

import com.firstlook.entity.InventoryTypeEnum;
import com.firstlook.entity.Member;
import com.firstlook.session.FirstlookSession;
import com.firstlook.session.Product;
import com.firstlook.session.ProductService;

public class PerspectiveFilter implements Filter
{

private static Logger logger = Logger.getLogger( PerspectiveFilter.class );
private ProductService productService;

public PerspectiveFilter()
{
	super();
}

public void destroy()
{

}

public void doFilter( ServletRequest request, ServletResponse response, FilterChain filterChain ) throws ServletException, IOException
{
	HttpServletRequest httpRequest = (HttpServletRequest)request;
	HttpSession session = httpRequest.getSession( true );

	if ( session.getAttribute( "perspective" ) == null )
	{
		session.setAttribute( "perspective", new Perspective() );
	}

	Perspective perspective = (Perspective)session.getAttribute( "perspective" );
	FirstlookSession firstlookSession = (FirstlookSession)session.getAttribute( FirstlookSession.FIRSTLOOK_SESSION );
	Map paramMap = request.getParameterMap();
	Map perspectiveMap = new HashMap();

	Iterator suppliedParams = paramMap.keySet().iterator();
	while ( suppliedParams.hasNext() )
	{
		String name = (String)suppliedParams.next();
		if ( name.startsWith( "p." ) )
		{
			perspectiveMap.put( name.substring( 2 ), ( (String[])paramMap.get( name ) )[0] );
		}
	}

	if ( perspectiveMap.containsKey( "impactMode" ) )
	{
		processImpactMode( perspective, paramMap, perspectiveMap );
	}

	if ( perspectiveMap.containsKey( "productMode" ) )
	{
		processProductMode( perspective, paramMap, perspectiveMap, request );
	}

	if ( perspectiveMap.containsKey( "inventoryType" ) )
	{
		processInventoryType( firstlookSession, perspectiveMap);
	}

	filterChain.doFilter( request, response );
}

private void processInventoryType( FirstlookSession firstlookSession, Map perspectiveMap) throws ServletException
{
	Member member = firstlookSession.getMember();
	String inventoryType = (String)perspectiveMap.get( "inventoryType" );
	if ( !inventoryType.equalsIgnoreCase( "used" ) && !inventoryType.equalsIgnoreCase( "new" ) )
	{
		throw new ServletException( "Invalid inventoryType specified, " + inventoryType );
	}

	UserRoleEnum userRole = UserRoleEnum.getEnum( inventoryType );

	if (  member.getMemberType().intValue() ==  Member.MEMBER_TYPE_USER )
	{

		if ( member.getUserRoleEnum().equals( UserRoleEnum.USED ) )
		{
			if ( userRole.equals( UserRoleEnum.NEW ) )
			{
				throw new ServletException( "Current member is only allowed to access USED" );
			}
		}

		if ( member.getUserRoleEnum().equals( UserRoleEnum.NEW ) )
		{
			if ( userRole.equals( UserRoleEnum.USED ) )
			{
				throw new ServletException( "Current member is only allowed to access NEW" );
			}
		}

	}
	member.setInventoryType(InventoryTypeEnum.getEnum(userRole ));
}

private void processImpactMode( Perspective perspective, Map paramMap, Map perspectiveMap ) throws ServletException
{
	String impactMode = (String)perspectiveMap.get( "impactMode" );
	if ( !impactMode.equals( "standard" ) && !impactMode.equals( "percentage" ) )
	{
		throw new ServletException( "Invalid impact mode specified, " + impactMode );
	}
	try
	{
		PropertyUtils.setProperty( perspective, "impactMode", impactMode );
	}
	catch ( Exception e )
	{
		throw new ServletException( "A problem occurred when trying to populate the perspective object with the "
				+ "following parameters: " + paramMap );
	}
}

private void processProductMode( Perspective perspective, Map paramMap, Map perspectiveMap, ServletRequest request ) throws ServletException
{
	String productMode = (String)perspectiveMap.get( "productMode" );

	Product product = productService.retrieveByName( productMode );

	HttpSession session = ( (HttpServletRequest)request ).getSession( false );
	if( session == null )
		throw new ServletException( "Expected a session from the user!  Make sure the CASFilter is mapped before this filter!");
	
	FirstlookSession firstlookSession = (FirstlookSession)session.getAttribute( FirstlookSession.FIRSTLOOK_SESSION );
	
	//debug code
	if( logger.isDebugEnabled() )
	{
		if( firstlookSession.getMode() != null )
		{
			Product previousProductMode = productService.retrieveByName( firstlookSession.getMode() );
			if( previousProductMode.equals( Product.MAX ) && product.equals( Product.EDGE ))
				logger.error( "Edge is overwriting MAX product mode!" );
		}
	}
	firstlookSession.setProduct( product );
}

public void init( FilterConfig config ) throws ServletException
{
	logger.debug( "Initializing Perspective Filter" );
	WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext( config.getServletContext() );
	productService = (ProductService)wac.getBean( "productService" );
}

}
