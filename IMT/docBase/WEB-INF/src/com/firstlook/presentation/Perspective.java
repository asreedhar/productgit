package com.firstlook.presentation;

import com.firstlook.entity.ImpactModeEnum;

public class Perspective
{

private ImpactModeEnum impactModeEnum = ImpactModeEnum.STANDARD;

public Perspective()
{
    super();
}

public ImpactModeEnum getImpactModeEnum()
{
    return impactModeEnum;
}

public void setImpactModeEnum( ImpactModeEnum impactModeEnum )
{
    this.impactModeEnum = impactModeEnum;
}

public void setImpactMode( String pImpactMode )
{
    impactModeEnum = ImpactModeEnum.getEnum(pImpactMode);
}

}
