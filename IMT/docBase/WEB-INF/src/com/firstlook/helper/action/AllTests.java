package com.firstlook.helper.action;

import junit.awtui.TestRunner;
import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests extends junit.framework.TestCase
{

public AllTests( String arg1 )
{
	super( arg1 );
}

public static void main( String args[] )
{
	TestRunner runner = new TestRunner();
	runner.start( new String[] { "com.firstlook.helper.AllTests" } );
}

public static Test suite()
{
	TestSuite suite = new TestSuite();
	suite.addTest( new TestSuite( TestReportActionHelper.class ) );
	return suite;
}
}
