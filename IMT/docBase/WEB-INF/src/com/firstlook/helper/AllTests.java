package com.firstlook.helper;

import junit.awtui.TestRunner;
import junit.framework.Test;
import junit.framework.TestSuite;

import com.firstlook.helper.action.TestReportActionHelper;

public class AllTests extends junit.framework.TestCase
{

public AllTests( String arg1 )
{
    super(arg1);
}

public static void main( String args[] )
{
    TestRunner runner = new TestRunner();
    runner.start(new String[]
    { "com.firstlook.helper.AllTests" });
}

public static Test suite()
{
    TestSuite suite = new TestSuite();
    suite.addTest(new TestSuite(TestInventoryHelper.class));
    suite.addTest(new TestSuite(TestReportActionHelper.class));
    suite.addTest(new TestSuite(TestRequestHelper.class));
    suite.addTest(new TestSuite(TestSessionHelper.class));
    suite.addTest(new TestSuite(TestVehicleHelper.class));

    return suite;
}
}
