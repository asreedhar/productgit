package com.firstlook.helper;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.IllegalFormatException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;

import biz.firstlook.commons.util.Functions;

import com.firstlook.exception.ApplicationException;
import biz.firstlook.commons.gson.GsonExcludeAnnotationExclusionStrategy;
import biz.firstlook.commons.gson.GsonSpecificClassExclusionStrategy;
import com.google.gson.ExclusionStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class RequestHelper
{

public RequestHelper()
{
	super();
}

public static boolean getBoolean( HttpServletRequest request, String parameterName )
{
	return Boolean.valueOf( request.getParameter( parameterName ) ).booleanValue();
}

@SuppressWarnings("unchecked")
public static String getClickedButton( HttpServletRequest request )
{
	Enumeration<String> enumerator = request.getParameterNames();
	while ( enumerator.hasMoreElements() )
	{
		String parameter = enumerator.nextElement();
		if ( parameter.endsWith( ".x" ) )
		{
			return parameter.substring( 0, parameter.length() - 2 );
		}
	}
	return "";
}

/**
 * Gets a parameter from the request and converts the parameter value into an
 * int. If the variable name is not found in the parameter map, the attribute
 * map is checked and the associated attribute value is returned. The Exception
 * type should probably be changed to a more specific one.
 * 
 * @return a primitive <code>int</code> corresponding to a parameter name or
 *         an attribute name of the request.
 * @throws ApplicationException
 *             if the parameter or attribute name does not exist in the request.
 * @throws ApplicationException
 *             if the parameter or attribute value is not an Integer
 * @author Benson Fung
 * @since July 29, 2005
 * @deprecated July 31, 2007 SBW: This method is pure evil. Validate your input using
 *             struts-forms validation or manual validation. Do not let this method
 *             throw an exception and ruin a user experience with a unneeded DPP.
 */
public static int getInt( HttpServletRequest request, String parameterName ) throws ApplicationException
{
	// validate arguments
	if (request == null) {
		throw new IllegalArgumentException("DE981: Parameter 'request' cannot be null.");
	}
	if (parameterName == null) {
		throw new IllegalArgumentException("DE981: Parameter 'parameterName' cannot be null.");
	}
	// extract variables
	final String parameter = request.getParameter(parameterName);
	final Object attribute = request.getAttribute(parameterName);
	// try request parameter
	Integer integer = Functions.nullSafeInteger(parameter);
	// try request attribute
	if (integer == null) {
		integer = Functions.nullSafeInteger(attribute);
	}
	// throw an exception if neither produced an integer
	if (integer == null) {
		String msg = "DE981: Neither request-parameter nor request-attribute are integers";
		try {
			msg = String.format("DE981: Neither the request-parameter '%s' nor the request-attribute '%s' for the key '%s' are integers", parameter, attribute, parameterName);
		}
		catch (IllegalFormatException ex) {
			// ignore
		}
		throw new ApplicationException(msg);
	}
	// else return the int value
	else {
		return integer.intValue();
	}
}

public static long getLong( HttpServletRequest request, String parameterName ) throws ApplicationException
{
	try
	{
		return Long.parseLong( request.getParameter( parameterName ) );
	}
	catch ( NumberFormatException nfe )
	{
		throw new ApplicationException( "Could not parse request parameter: " + parameterName, nfe );
	}
}

/**
 * Gets a parameter from the request and returns the parameter value. If the
 * variable name is not found in the parameter map, the attribute map is checked
 * and the associated attribute value is returned.
 * 
 * @return the String value corresponding to a parameter name or an attribute
 *         name of the request. If the name doesn't exist in either maps, a
 *         <code>null</code> is returned.
 * @throws ApplicationException
 *             if the parameter or attribute name is not a String.
 * @author Benson Fung
 * @since July 29, 2005
 * @deprecated July 31, 2007 SBW: This method is pure evil. Validate your input using
 *             struts-forms validation or manual validation. Do not let this method
 *             throw an exception and ruin a user experience with a unneeded DPP.
 */
public static String getString( HttpServletRequest request, String parameterName ) throws ApplicationException
{
	// validate arguments
	if (request == null) {
		throw new IllegalArgumentException("DE981: Parameter 'request' cannot be null.");
	}
	if (parameterName == null) {
		throw new IllegalArgumentException("DE981: Parameter 'parameterName' cannot be null.");
	}
	// extract variables
	final String parameter = request.getParameter(parameterName);
	final Object attribute = request.getAttribute(parameterName);
	// get value
	String value = parameter;
	if (value == null) {
		if (attribute != null && (attribute instanceof String)) {
			value = attribute.toString();
		}
	}
	return value;
}

public static String toJson(HttpServletRequest request) throws JSONException{

	List<ExclusionStrategy> exclusionStrategies = new ArrayList<ExclusionStrategy>();
	exclusionStrategies.add(new GsonSpecificClassExclusionStrategy(org.apache.struts.action.RequestActionMapping.class));
	exclusionStrategies.add(new GsonSpecificClassExclusionStrategy(org.apache.struts.config.impl.ModuleConfigImpl.class));
	exclusionStrategies.add(new GsonSpecificClassExclusionStrategy(org.apache.struts.util.PropertyMessageResources.class));
	exclusionStrategies.add(new GsonSpecificClassExclusionStrategy(org.hibernate.proxy.HibernateProxy.class));
	exclusionStrategies.add(new GsonSpecificClassExclusionStrategy(com.firstlook.entity.form.DealerForm.class));
	exclusionStrategies.add(new GsonExcludeAnnotationExclusionStrategy());
	
	Gson gson = new GsonBuilder()
				    .setExclusionStrategies(exclusionStrategies.toArray(new ExclusionStrategy[exclusionStrategies.size()]))
				    .create();
	
	
	HashMap<String,Object> attributeMap = new HashMap<String,Object>();
	Enumeration attributeNames = request.getAttributeNames();
	
	List<String>  excludes = new ArrayList<String>();
	excludes.add("org.apache.struts.action.MODULE");
	excludes.add("javax.servlet.request.cipher_suite");
	excludes.add("javax.servlet.include.servlet_path");
	excludes.add("javax.servlet.forward.servlet_path");
	excludes.add("org.apache.struts.action.mapping.instance");
	excludes.add("javax.servlet.include.request_uri");
	excludes.add("javax.servlet.include.context_path");
	excludes.add("javax.servlet.forward.request_uri");
	excludes.add("javax.servlet.request.ssl_session");
	excludes.add("org.apache.struts.action.MESSAGE");
	excludes.add("javax.servlet.forward.context_path");
	excludes.add("javax.servlet.request.key_size");
	excludes.add("javax.servlet.request.ssl_session_mgr");
	//excludes.add("people");
	
	boolean debugOutput = false;
	
	String queryString = request.getQueryString();
	
	if ((queryString != null) && (queryString.contains("debug=please")))
	{
		debugOutput = true;
	}
	
	while(attributeNames.hasMoreElements()){
		String current = (String)attributeNames.nextElement();
		Object attribute = request.getAttribute(current);
		
		if(!excludes.contains(current)) {
			if (debugOutput == true)
			{
				System.out.println( "converting " + current + " to gson" );
			}
			String jsonTest = gson.toJson(attribute);
 			attributeMap.put(current, attribute);
 		}
	}
	
	String json = gson.toJson(attributeMap);
	// this is a hack to replace escape chars properly
	// problem: "ab\"c" is escaped by javascript engine as ab"c and then passed to JSON parser directly (which breaks json parcer). 
	//          If you need to pass ab"c to a parser, it should be "ab\\"c" to javascript, then ab\"c to JSON
	return json.replace("\\", "\\\\");
}

}



