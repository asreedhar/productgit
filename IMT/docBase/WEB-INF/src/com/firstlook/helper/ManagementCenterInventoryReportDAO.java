package com.firstlook.helper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.firstlook.entity.Dealer;
import com.firstlook.scorecard.ReportData;

/**
 * This class is the default implementation to retrieve the Current Inventory
 * Risk and Aging Inventory sections of the Management Center.
 * 
 * @author bfung
 * 
 */
public class ManagementCenterInventoryReportDAO extends JdbcDaoSupport {

	private static final String VEHICLE_LIGHT = "SELECT * FROM GetManagementCenterInventoryByVehicleLight(?)";

	private static final String AGE_BUCKET = "SELECT * FROM GetManagementCenterInventoryByAgeBucket(?)";

	/**
	 * Retrieves the ReportData (Target vs. Actuals in percents) for the Current
	 * Inventory Risk portionn of the Management Center.
	 * 
	 * @param dealer
	 * @return a map with the VehicleLight as the key and the data as values.
	 */
	@SuppressWarnings("unchecked")
	public Map<Integer, ReportData> getVehicleLightPercentages(Dealer dealer) {
		List<ReportData> reportDataItems = getJdbcTemplate().query(
				VEHICLE_LIGHT, new Object[] { dealer.getDealerId() },
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowIndex)
							throws SQLException {
						ReportData reportData = new ReportData(rs
								.getString("VehicleLightID"));
						reportData.setActual(rs.getInt("UnitsPercent"));
						reportData.setTarget(rs.getInt("TargetPercent"));
						return reportData;
					}
				});

		Map<Integer, ReportData> inventoryByLight = new HashMap<Integer, ReportData>();
		for (ReportData reportData : reportDataItems) {
			inventoryByLight.put(Integer.valueOf(reportData.getKey()),
					reportData);
		}

		return inventoryByLight;
	}

	/**
	 * Retrieves the ReportData (Target vs. Actuals in percents) for the Aging
	 * Inventory portion of the Management Center.
	 * 
	 * @param dealer
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<ReportData> getAgeBucketPercentages(Dealer dealer) {
		return getJdbcTemplate().query(AGE_BUCKET,
				new Object[] { dealer.getDealerId() }, new RowMapper() {
					public Object mapRow(ResultSet rs, int rowIndex)
							throws SQLException {
						ReportData reportData = new ReportData(rs
								.getString("Description"));
						reportData.setActual(rs.getInt("UnitsPercent"));
						reportData.setTarget(rs.getInt("TargetPercent"));
						return reportData;
					}
				});
	}

}
