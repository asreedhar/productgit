package com.firstlook.helper;

import java.util.Collection;

import com.firstlook.action.BaseActionForm;
import com.firstlook.iterator.BaseFormIterator;
import com.firstlook.scorecard.ReportData;

public class InventoryRiskHelperForm extends BaseActionForm
{

private static final long serialVersionUID = -591765695539643653L;
	
private ReportData redLight;
private ReportData yellowLight;
private ReportData greenLight;
private Collection<ReportData> ageBands;

public Collection<ReportData> getAgeBands()
{
    return ageBands;
}

public BaseFormIterator getAgeBandsIterator()
{
    return new BaseFormIterator<ReportData>(ageBands);
}

public String getActualGreenLightFormatted()
{
    return greenLight.getActualFormatted();
}

public int getTargetGreenLight()
{
    return greenLight.getTarget();
}

public String getActualYellowLightFormatted()
{
    return yellowLight.getActualFormatted();
}

public int getTargetYellowLight()
{
    return yellowLight.getTarget();
}

public String getActualRedLightFormatted()
{
    return redLight.getActualFormatted();
}

public int getTargetRedLight()
{
    return redLight.getTarget();
}

public void setAgeBands( Collection<ReportData> collection )
{
    ageBands = collection;
}

public void setGreenLight( ReportData data )
{
    greenLight = data;
}

public void setRedLight( ReportData data )
{
    redLight = data;
}

public void setYellowLight( ReportData data )
{
    yellowLight = data;
}

}
