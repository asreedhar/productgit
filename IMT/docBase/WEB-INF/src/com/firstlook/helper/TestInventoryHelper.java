package com.firstlook.helper;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.time.DateUtils;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.ObjectMother;
import com.firstlook.persistence.dealergroup.MockDealerGroupDAO;
import com.firstlook.service.dealergroup.DealerGroupService;

public class TestInventoryHelper extends BaseTestCase
{

private DealerGroupService dealerGroupService; // Leave this here - does not extend SecureBaseAction

public TestInventoryHelper( String arg1 )
{
    super( arg1 );
}

public void testDetermineAgingPolicyInventoryReceivedDateAgingLimitMinVal()
{
    Dealer dealer = ObjectMother.createDealer();
    dealer.setDealerId( new Integer( 1 ) );
    DealerGroup dealerGroup = ObjectMother.createDealerGroup();

    Set<DealerGroup> dealerGroupCol = new HashSet<DealerGroup>();
    dealerGroupCol.add( dealerGroup );

    dealerGroup.setInventoryExchangeAgeLimit( Integer.MIN_VALUE );

    mockDatabase.addReturnObject( dealerGroup );
    dealerGroupService = new DealerGroupService();
    dealerGroupService.setDealerGroupDAO( new MockDealerGroupDAO( mockDatabase ) );

    Calendar expectedCal = Calendar.getInstance();
    expectedCal = DateUtils.truncate( expectedCal, Calendar.DATE );

    Calendar returnedCal = Calendar.getInstance();
    returnedCal.setTime( InventoryHelper.determineAgingPolicyInventoryReceivedDate(
                                                                                    dealer,
                                                                                    dealerGroupService));
    returnedCal = DateUtils.truncate( returnedCal, Calendar.DATE );

    assertEquals( "Should be current date", expectedCal.getTime(), returnedCal.getTime() );
}

public void testDetermineAgingPolicyInventoryReceivedDate()
{
    Dealer dealer = ObjectMother.createDealer();
    dealer.setDealerId( new Integer( 1 ) );
    DealerGroup dealerGroup = ObjectMother.createDealerGroup();

    Set<DealerGroup> dealerGroupCol = new HashSet<DealerGroup>();
    dealerGroupCol.add( dealerGroup );

    dealerGroup.setInventoryExchangeAgeLimit( 30 );

    mockDatabase.addReturnObject( dealerGroup );

    Calendar expectedCal = Calendar.getInstance();
    expectedCal.add( Calendar.DAY_OF_YEAR, -30 );
    expectedCal = DateUtils.truncate( expectedCal, Calendar.DATE );

    dealerGroupService = new DealerGroupService();
    dealerGroupService.setDealerGroupDAO( new MockDealerGroupDAO( mockDatabase ) );
    
    Calendar returnedCal = Calendar.getInstance();
    returnedCal.setTime( InventoryHelper.determineAgingPolicyInventoryReceivedDate(
                                                                                    dealer,
                                                                                    dealerGroupService));
    
    returnedCal = DateUtils.truncate( returnedCal, Calendar.DATE );

    assertEquals( "Should be current date", expectedCal.getTime(), returnedCal.getTime() );
}

public DealerGroupService getDealerGroupService()
{
	return dealerGroupService;
}

public void setDealerGroupService( DealerGroupService dealerGroupService )
{
	this.dealerGroupService = dealerGroupService;
}

}
