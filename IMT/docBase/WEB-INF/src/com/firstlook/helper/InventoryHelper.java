package com.firstlook.helper;

import java.util.Date;

import org.apache.log4j.Logger;

import biz.firstlook.commons.sql.RuntimeDatabaseException;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.report.DisplayDateRange;
import com.firstlook.service.dealergroup.DealerGroupService;

public class InventoryHelper
{

private static final Logger logger = Logger.getLogger( InventoryHelper.class );


public InventoryHelper()
{
}

public static Date determineAgingPolicyInventoryReceivedDate( Dealer dealer, DealerGroupService dgService )
{
    
    DealerGroup group = null;
    try
    {
        group = dgService.retrieveByDealerId( dealer.getDealerId().intValue() );
    } catch ( RuntimeDatabaseException e )
    {
        logger.error( "Error retrieving dealergroup from db: ", e );
        return new Date( System.currentTimeMillis() );
    }

    
	if ( group.getInventoryExchangeAgeLimit() == Integer.MIN_VALUE )
	{
		return new Date( System.currentTimeMillis() );
	}
	else
	{
		long daysInMillis = group.getInventoryExchangeAgeLimit() * DisplayDateRange.DAY_IN_MILLIS;
		return new Date( System.currentTimeMillis() - daysInMillis );
	}
}



}
