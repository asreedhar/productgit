package com.firstlook.helper;

import javax.servlet.http.HttpServletRequest;

import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.form.TradeAnalyzerForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.ReportGroupingHelper;
import com.firstlook.report.ReportGrouping;
import com.firstlook.report.ReportGroupingForm;
import com.firstlook.session.FirstlookSession;

public class ReportGroupingRequestHelper
{

private ReportGroupingHelper reportGroupingHelper;
	
public ReportGroupingRequestHelper()
{
}

public void putReportGroupingFormsInRequest( HttpServletRequest request, TradeAnalyzerForm tradeAnalyzerForm, int weeks )
        throws ApplicationException
{
	//KL - uses FLDW data
	ReportGrouping specificReportGrouping = reportGroupingHelper.findByMakeModelOrTrimOrBodyStyle(
																									tradeAnalyzerForm.getMake(),
																									tradeAnalyzerForm.getModel(),
																									tradeAnalyzerForm.getTrim(),
																									tradeAnalyzerForm.getIncludeDealerGroup(),
																									( (FirstlookSession)request.getSession().getAttribute(
																																							FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId(),
																									weeks, InventoryEntity.USED_CAR );
	ReportGrouping generalReportGrouping = reportGroupingHelper.findByMakeModelOrTrimOrBodyStyle(
																									tradeAnalyzerForm.getMake(),
																									tradeAnalyzerForm.getModel(),
																									null,
																									tradeAnalyzerForm.getIncludeDealerGroup(),
																									( (FirstlookSession)request.getSession().getAttribute(
																																							FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId(),
																									weeks, InventoryEntity.USED_CAR );

	ReportGroupingForm specificReport = new ReportGroupingForm( specificReportGrouping );
	ReportGroupingForm generalReport = new ReportGroupingForm( generalReportGrouping );
	request.setAttribute( "specificReport", specificReport );
	request.setAttribute( "generalReport", generalReport );

	SessionHelper.setAttribute( request, "specificReport", specificReportGrouping );
	SessionHelper.keepAttribute( request, "specificReport" );
	SessionHelper.setAttribute( request, "generalReport", generalReportGrouping );
	SessionHelper.keepAttribute( request, "generalReport" );
}

public void setReportGroupingHelper(ReportGroupingHelper reportGroupingHelper) {
	this.reportGroupingHelper = reportGroupingHelper;
}


}
