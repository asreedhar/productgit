package com.firstlook.helper;

public class FullReportDataHelper
{

private int weeks;
private int dealerId;
private int forecast;

public FullReportDataHelper()
{
    super();
}

public int getDealerId()
{
    return dealerId;
}

public int getForecast()
{
    return forecast;
}

public int getWeeks()
{
    return weeks;
}

public void setDealerId( int i )
{
    dealerId = i;
}

public void setForecast( int i )
{
    forecast = i;
}

public void setWeeks( int i )
{
    weeks = i;
}

}
