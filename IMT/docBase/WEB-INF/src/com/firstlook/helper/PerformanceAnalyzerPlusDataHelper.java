package com.firstlook.helper;

public class PerformanceAnalyzerPlusDataHelper
{


private int lowRange;
private int highRange;
private boolean firstPricePoint;
private boolean lastPricePoint;
private int mileage;
private String cacheKey;
private int weeks;
private int timeout;
private int forecast;

public PerformanceAnalyzerPlusDataHelper()
{
    super();
}

public boolean isFirstPricePoint()
{
    return firstPricePoint;
}

public int getHighRange()
{
    return highRange;
}

public boolean isLastPricePoint()
{
    return lastPricePoint;
}

public int getLowRange()
{
    return lowRange;
}

public int getMileage()
{
    return mileage;
}

public void setFirstPricePoint( boolean b )
{
    firstPricePoint = b;
}

public void setHighRange( int i )
{
    highRange = i;
}

public void setLastPricePoint( boolean b )
{
    lastPricePoint = b;
}

public void setLowRange( int i )
{
    lowRange = i;
}

public void setMileage( int i )
{
    mileage = i;
}

public String getCacheKey()
{
    return cacheKey;
}

public int getForecast()
{
    return forecast;
}

public int getTimeout()
{
    return timeout;
}

public int getWeeks()
{
    return weeks;
}

public void setCacheKey( String string )
{
    cacheKey = string;
}

public void setForecast( int i )
{
    forecast = i;
}

public void setTimeout( int i )
{
    timeout = i;
}

public void setWeeks( int i )
{
    weeks = i;
}

}
