package com.firstlook.iterator;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public class TransposedColumnFormIterator extends SimpleFormIterator
{
private int numberOfColumns;
private int missingColumns;
private int currentRow;
private List ourCollection;

public TransposedColumnFormIterator( int columns,
        java.util.Collection newObjects, Class newFormClass )
{
     super(newObjects, newFormClass);
    if ( columns < 1 )
    {
        throw new IllegalArgumentException(
                "Transposed columned form iterator must have at least one column");
    }
    numberOfColumns = columns;
    missingColumns = (columns - (newObjects.size() % columns)) % columns;
    currentRow = 0;
    getSuperCollection();
}

private void getSuperCollection()
{
    ourCollection = super.objects;
}

/**
 * Insert the method's description here. Creation date: (2/8/2002 9:31:08 AM)
 * 
 * @return int
 */
public int getMissingColumns()
{
    return missingColumns;
}

/**
 * Insert the method's description here. Creation date: (1/17/2002 5:10:05 PM)
 * 
 * @return int
 */
public int getNumberOfColumns()
{
    return numberOfColumns;
}

public int getNumberOfRows()
{
    double dRows = ((double) objects.size()) / ((double) numberOfColumns);
    double dTotalRows = Math.ceil(dRows);
    int iTotalRows = new Double(dTotalRows).intValue();
    return iTotalRows;
}

public boolean hasNext()
{
    if ( currentRow < getNumberOfRows() )
    {
        return true;
    } else
    {
        return false;
    }
}

public Object next()
{
    Vector nestedCollection = new Vector();
    int columnSize = getNumberOfRows();
    int linearIndex;
    Object nextItem;

    for (int col = 0; col < numberOfColumns; col++)
    {
        linearIndex = currentRow + (col * columnSize);
        if ( linearIndex < ourCollection.size() )
        {
            nextItem = ourCollection.get(linearIndex);

            if ( nextItem instanceof TabOrder )
                ((TabOrder) nextItem).setTabOrder(linearIndex + 20);

            nestedCollection.add(nextItem);
        }
    }
    currentRow++;
    return nestedCollection;
}

/**
 * Insert the method's description here. Creation date: (1/17/2002 5:10:05 PM)
 * 
 * @param newNumberOfColumns
 *            int
 */
public void setNumberOfColumns( int newNumberOfColumns )
{
    numberOfColumns = newNumberOfColumns;
}

public Collection getRowsAsLinearCollection()
{
    Vector linearCollection = new Vector();
    Vector row;
    Iterator it;

    while (hasNext())
    {
        row = (Vector) next();
        it = row.iterator();
        while (it.hasNext())
            linearCollection.add(it.next());
    }
    return linearCollection;
}

public static void main( String[] args )
{
    Vector test = new Vector();
    int size = 34;
    for (int x = 0; x < size; x++)
    	test.add( new Integer(x) );

    TransposedColumnFormIterator tci = new TransposedColumnFormIterator(3,
            test, String.class);
    while (tci.hasNext())
    {
        Vector inner = (Vector) tci.next();
        Iterator it = inner.iterator();
        while (it.hasNext())
        {
            System.out.print((Integer) it.next() + "   ");
        }
        System.out.print("\n");
    }
}
}
