package com.firstlook.iterator;

import java.util.Vector;

import biz.firstlook.transact.persist.model.GroupingDescription;
import biz.firstlook.transact.persist.model.MakeModelGrouping;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.MockInventory;
import com.firstlook.mock.ObjectMother;
import com.firstlook.report.CustomIndicator;
import com.firstlook.report.Report;

public class TestVehicleFormIterator extends BaseTestCase
{
private VehicleFormIterator tmgfi;

public TestVehicleFormIterator( String arg1 )
{
    super(arg1);
}

public void setup() throws DatabaseException, ApplicationException
{
	InventoryEntity vehicle1 = ObjectMother.createInventory();
    vehicle1.setMakeModelGroupingId(31);
    InventoryEntity vehicle2 = ObjectMother.createInventory();
    vehicle2.setMakeModelGroupingId(35);
    InventoryEntity vehicle3 = ObjectMother.createInventory();
    vehicle3.setMakeModelGroupingId(35);
    InventoryEntity vehicle4 = ObjectMother.createInventory();
    vehicle4.setMakeModelGroupingId(75);

    GroupingDescription groupingDescription = ObjectMother
            .createGroupingDescription();
    mockDatabase.setReturnObjects(groupingDescription);

    MakeModelGrouping mmg1 = ObjectMother.createMakeModelGrouping();
    mmg1.setGroupingDescription(groupingDescription);
    mmg1.setMakeModelGroupingId(vehicle1.getMakeModelGroupingId());
    mmg1.getGroupingDescription().setGroupingDescription("FORD EXPLORER");
    vehicle1.setMakeModelGrouping(mmg1);

    MakeModelGrouping mmg2 = ObjectMother.createMakeModelGrouping();
    mmg2.setGroupingDescription(groupingDescription);
    mmg2.setMakeModelGroupingId(vehicle2.getMakeModelGroupingId());
    mmg2.getGroupingDescription().setGroupingDescription("FORD TAURUS");
    vehicle2.setMakeModelGrouping(mmg2);
    vehicle3.setMakeModelGrouping(mmg2);

    MakeModelGrouping mmg3 = ObjectMother.createMakeModelGrouping();
    mmg3.setGroupingDescription(groupingDescription);
    mmg3.setMakeModelGroupingId(vehicle4.getMakeModelGroupingId());
    mmg3.getGroupingDescription().setGroupingDescription("DESCRIPTION NO RANK");
    vehicle4.setMakeModelGrouping(mmg3);

    mockDatabase.setReturnObjects(mmg1);
    mockDatabase.setReturnObjects(mmg2);
    mockDatabase.setReturnObjects(mmg3);

    Vector vehicles = new Vector();
    vehicles.add(vehicle1);
    vehicles.add(vehicle2);
    vehicles.add(vehicle3);
    vehicles.add(vehicle4);

    Dealer dealer = ObjectMother.createDealer();
    dealer.setDealerId(new Integer(vehicle1.getDealerId()));

    Report report = new Report(ObjectMother.createReportGroupings());
    report.setUnitsSoldThreshold(5);
    CustomIndicator indicator = new CustomIndicator(dealer, report,
            CustomIndicator.TOP_TEN);

    tmgfi = new VehicleFormIterator(vehicles, indicator);
}

public void testGetCurrentVehicleLineItemNumber()
{
    tmgfi.next();
    assertEquals("1st Incorrect.", 1, tmgfi.getCurrentVehicleLineItemNumber());
    tmgfi.next();
    assertEquals("2nd Incorrect.", 2, tmgfi.getCurrentVehicleLineItemNumber());
    tmgfi.next();
    assertEquals("3rd Incorrect.", 3, tmgfi.getCurrentVehicleLineItemNumber());
}

public void testIsCurrentGroupingDescriptionChanged()
        throws ApplicationException
{
    MockInventory prevVehicle = new MockInventory();
    prevVehicle.setGroupingDescription("Grouping1");

    MockInventory currentVehicle = new MockInventory();
    currentVehicle.setGroupingDescription("Grouping1");

    tmgfi.setPreviousObject(prevVehicle);
    tmgfi.setCurrentObject(currentVehicle);

    assertTrue("should not change", !tmgfi.isGroupingDescriptionChanged());

    currentVehicle.setGroupingDescription("Grouping2");

    assertTrue("should change", tmgfi.isGroupingDescriptionChanged());

}

public void testIsCurrentGroupingDescriptionChangedWithNullPreviousObject()
        throws ApplicationException
{
    MockInventory currentVehicle = new MockInventory();
    currentVehicle.setGroupingDescription("Grouping1");

    tmgfi.setCurrentObject(currentVehicle);

    assertTrue("should change", tmgfi.isGroupingDescriptionChanged());

}

public void testIsCurrentVehicleLineItemNumberEven()
{
    tmgfi.next();
    assertTrue("1st Incorrect.", !tmgfi.isCurrentVehicleLineItemNumberEven());
    tmgfi.next();
    assertTrue("2nd Correct.", tmgfi.isCurrentVehicleLineItemNumberEven());
    tmgfi.next();
    assertTrue("3rd Incorrect.", !tmgfi.isCurrentVehicleLineItemNumberEven());
}
}
