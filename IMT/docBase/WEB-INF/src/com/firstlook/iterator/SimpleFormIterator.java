package com.firstlook.iterator;

import java.util.Vector;

import com.firstlook.action.BaseActionForm;

public class SimpleFormIterator extends BaseFormIterator
{
protected java.lang.Class formClass;
protected int originalSize;

/**
 * SimpleFormIterator constructor comment.
 */
public SimpleFormIterator()
{
    super();
}

public SimpleFormIterator( java.util.Collection newObjects, Class newFormClass )
{
    super( newObjects );
    formClass = newFormClass;
    originalSize = newObjects.size();
}

/**
 * 
 * @return int
 */
public int getOriginalSize()
{
    return originalSize;
}

public boolean isReset()
{
    index = 0;
    currentObject = null;
    previousObject = null;
    return true;
}

/**
 * Insert the method's description here. Creation date: (11/6/2001 4:51:22 PM)
 * 
 * @return java.lang.Object
 */
public Object next()
{
    try
    {
        Object newObject = super.next();
        if ( formClass != null )
        {
            BaseActionForm form = (BaseActionForm)formClass.newInstance();

            if ( newObject instanceof BlankObject )
            {
                form.setBlank( true );
            }
            else
            {
                form.setBusinessObject( newObject );
            }
            form.setIndex( super.index );
            return form;
        }
        else
            return newObject;

    }
    catch ( Exception e )
    {
        e.printStackTrace();
    }
    return null;
}

public void setCollectionSize( int desiredSize )
{
    if ( desiredSize != Integer.MIN_VALUE )
    {
        Vector newObjects = new Vector();
        for ( int i = 0; i < desiredSize; i++ )
        {
            if ( i < objects.size() )
            {
                newObjects.add( objects.get( i ) );
            }
            else
            {
                newObjects.add( new BlankObject() );
            }
        }

        objects = newObjects;
    }
}

/**
 * 
 * @param newOriginalSize
 *            int
 */
public void setOriginalSize( int newOriginalSize )
{
    originalSize = newOriginalSize;
}

public String toString()
{
	return "Iterator Size: " + getOriginalSize();
}

}
