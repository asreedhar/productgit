package com.firstlook.iterator;

import java.util.Collection;
import java.util.NoSuchElementException;

import biz.firstlook.transact.persist.service.appraisal.IAppraisal;

import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.form.InventoryForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.report.CustomIndicator;

public class VehicleFormIterator extends CustomIndicatorBaseIterator
{

private int currentVehicleLineItemNumber = 0;

public VehicleFormIterator( Collection newObjects )
{
    this(newObjects, new CustomIndicator());
}

public VehicleFormIterator( Collection newObjects, CustomIndicator indicator )
{
    this(newObjects, indicator, InventoryForm.class);
}

public VehicleFormIterator( Collection newObjects, CustomIndicator indicator,
        Class formClass )
{
    super(newObjects, indicator, formClass);
}

public int getCurrentVehicleLineItemNumber()
{
    return currentVehicleLineItemNumber;
}

protected Object getVehicle( Object object )
{
    return object;
}

public boolean isCurrentVehicleLineItemNumberEven()
{
    return currentVehicleLineItemNumber % 2 == 0;
}

public boolean isGroupingDescriptionChanged() throws ApplicationException
{
    String previousGroupingDescription;
    String currentGroupingDescription;
    if ( previousObject == null )
    {
        return true;
    } else
    {
        if ( previousObject instanceof InventoryEntity )
        {
            previousGroupingDescription = ((InventoryEntity) previousObject)
                    .getGroupingDescription();
            currentGroupingDescription = ((InventoryEntity) currentObject)
                    .getGroupingDescription();
        } else if ( previousObject instanceof IAppraisal )
        {
        	IAppraisal previousAppraisal = (IAppraisal)previousObject;
        	IAppraisal currentAppraisal = (IAppraisal)currentObject;
            previousGroupingDescription = previousAppraisal.getVehicle().getMakeModelGrouping().getGroupingDescription().getGroupingDescription();
            currentGroupingDescription = currentAppraisal.getVehicle().getMakeModelGrouping().getGroupingDescription().getGroupingDescription();
        } else
        {
            previousGroupingDescription = "UNKNOWN";
            currentGroupingDescription = "UNKNOWN";
            return false;
        }

        return !previousGroupingDescription.equals(currentGroupingDescription);
    }
}

public Object next()
{

    Object form = super.next();

    try
    {
        if ( isGroupingDescriptionChanged() )
        {
            currentVehicleLineItemNumber = 1;
        } else
        {
            currentVehicleLineItemNumber++;
        }
    } catch (ApplicationException e)
    {
        throw new NoSuchElementException(
                "problem retrieving grouping description");
    }

    return form;
}

protected void setCurrentGroupingDescription() throws ApplicationException
{
    currentGroupingDescription = "UNKNOWN";
    if ( currentObject instanceof InventoryEntity )
    {
        currentGroupingDescription = ((InventoryEntity) currentObject)
                .getGroupingDescription();
    } else if ( currentObject instanceof IAppraisal )
    {
    	IAppraisal currentAppraisal = (IAppraisal)currentObject;
        currentGroupingDescription = currentAppraisal.getVehicle().getMakeModelGrouping().getGroupingDescription().getGroupingDescription();
    }

}

}
