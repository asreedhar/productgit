package com.firstlook.iterator;

import java.util.Collection;
import java.util.Vector;

public class ColumnedFormIterator extends SimpleFormIterator
{
private int numberOfColumns;
private int missingColumns;
private int currentRow;

public ColumnedFormIterator( int columns, Collection newObjects,
        Class newFormClass )
{
    super(newObjects, newFormClass);
    if ( columns < 1 )
    {
        throw new IllegalArgumentException(
                "columned form iterator must have at least one column");
    }
    numberOfColumns = columns;
    missingColumns = (columns - (newObjects.size() % columns)) % columns;
    currentRow = 0;
}

public ColumnedFormIterator( TransposedColumnFormIterator tci,
        Class newFormClass )
{
    super(tci.getRowsAsLinearCollection(), newFormClass);

    int columns = tci.getNumberOfColumns();
    if ( columns < 1 )
    {
        throw new IllegalArgumentException(
                "columned form iterator must have at least one column");
    }
    numberOfColumns = columns;
    missingColumns = tci.getMissingColumns();
    currentRow = 0;
}

/**
 * Insert the method's description here. Creation date: (2/8/2002 9:31:08 AM)
 * 
 * @return int
 */
public int getMissingColumns()
{
    return missingColumns;
}

/**
 * Insert the method's description here. Creation date: (1/17/2002 5:10:05 PM)
 * 
 * @return int
 */
public int getNumberOfColumns()
{
    return numberOfColumns;
}

public boolean isCurrentRowLastRow()
{
    double dRows = ((double) objects.size()) / ((double) numberOfColumns);
    double dTotalRows = Math.ceil(dRows);
    int iTotalRows = new Double(dTotalRows).intValue();

    if ( currentRow == iTotalRows )
    {
        return true;
    } else
    {
        return false;
    }
}

public Object next()
{
    Vector nestedCollection = new Vector();

    for (int i = 0; i < numberOfColumns; i++)
    {
        if ( super.hasNext() )
        {
            nestedCollection.add(super.next());
        }
    }
    currentRow++;
    return nestedCollection;

}

/**
 * Insert the method's description here. Creation date: (1/17/2002 5:10:05 PM)
 * 
 * @param newNumberOfColumns
 *            int
 */
public void setNumberOfColumns( int newNumberOfColumns )
{
    numberOfColumns = newNumberOfColumns;
}
}
