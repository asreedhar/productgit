package com.firstlook.iterator;

import java.util.List;
import java.util.Vector;

import com.firstlook.action.BaseActionForm;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.form.DealerForm;

/**
 * Insert the type's description here. Creation date: (11/8/2001 3:48:33 PM)
 * 
 * @author: Extreme Developer
 */
public class TestSimpleFormIterator extends junit.framework.TestCase
{
/**
 * TestSimpleFormIterator constructor comment.
 * 
 * @param arg1
 *            java.lang.String
 */
public TestSimpleFormIterator( String arg1 )
{
    super(arg1);
}

public void testChangeCollectionToRightSizeCorrectInitialSize()
{
    Vector objects = new Vector();
    objects.add(new Object());
    objects.add(new Object());

    SimpleFormIterator iter = new SimpleFormIterator(objects,
            BaseActionForm.class);
    iter.setCollectionSize(2);

    List returnedObjects = iter.getObjects();
    assertEquals("Incorrect Size.", 2, returnedObjects.size());

}

public void testChangeCollectionToRightSizeInitialSizeTooBig()
{
    Vector objects = new Vector();
    objects.add(new Object());
    objects.add(new Object());

    SimpleFormIterator iter = new SimpleFormIterator(objects,
            BaseActionForm.class);
    iter.setCollectionSize(1);

    List returnedObjects = iter.getObjects();
    assertEquals("Incorrect Size.", 1, returnedObjects.size());

}

public void testChangeCollectionToRightSizeInitialSizeTooSmall()
{
    Vector objects = new Vector();
    objects.add(new Object());

    SimpleFormIterator iter = new SimpleFormIterator(objects,
            BaseActionForm.class);
    iter.setCollectionSize(2);

    List returnedObjects = iter.getObjects();
    assertEquals("Incorrect Size.", 2, returnedObjects.size());
    assertTrue("Should be blank", returnedObjects.get(1) instanceof BlankObject);
}

public void testFormIndex()
{
    Vector objVector = new Vector();
    objVector.add(new Object());
    objVector.add(new Object());
    objVector.add(new Object());

    SimpleFormIterator iterator = new SimpleFormIterator(objVector,
            BaseActionForm.class);

    int i = 1;
    while (iterator.hasNext())
    {
        assertEquals(i, ((BaseActionForm) iterator.next()).getIndex());
        i++;
    }
}

public void testIsReset()
{
    Vector v = new Vector();
    v.add(new Object());
    v.add(new Object());

    SimpleFormIterator si = new SimpleFormIterator(v, BaseActionForm.class);

    while (si.hasNext())
    {
        si.next();
    }
    si.isReset();

    assertTrue(si.hasNext());
    assertNull("current object should be null.", si.currentObject);
    assertNull("previous object should be null", si.previousObject);
}

public void testNextWithBlanks()
{
    Vector objects = new Vector();
    objects.add(new Dealer());

    SimpleFormIterator iter = new SimpleFormIterator(objects, DealerForm.class);
    iter.setCollectionSize(2);

    BaseActionForm form1 = (BaseActionForm) iter.next();
    BaseActionForm form2 = (BaseActionForm) iter.next();

    assertTrue("Should not be blank.", !form1.isBlank());
    assertTrue("Should be blank.", form2.isBlank());

}
}
