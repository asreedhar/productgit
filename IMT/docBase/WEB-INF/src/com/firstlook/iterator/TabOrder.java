package com.firstlook.iterator;

public interface TabOrder
{
public int getTabOrder();

public void setTabOrder( int i );
}
