package com.firstlook.iterator;

import junit.awtui.TestRunner;
import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests extends junit.framework.TestCase
{
// This is a test.
public AllTests( String arg1 )
{
    super(arg1);
}

public static void main( String[] args )
{
    TestRunner runner = new TestRunner();
    runner.start(new String[]
    { "com.firstlook.AllTests" });
}

public static Test suite()
{
    TestSuite suite = new TestSuite();

    suite.addTest(new TestSuite(TestBaseFormIterator.class));
    suite.addTest(new TestSuite(TestColumnedFormIterator.class));
    suite.addTest(new TestSuite(TestSimpleFormIterator.class));
    suite.addTest(new TestSuite(TestVehicleFormIterator.class));

    return suite;
}
}
