package com.firstlook.iterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.form.DealerForm;
import com.firstlook.entity.form.StringForm;
import com.firstlook.mock.ObjectMother;

/**
 * Insert the type's description here. Creation date: (1/17/2002 5:11:00 PM)
 * 
 * @author: Extreme Developer
 */
public class TestColumnedFormIterator extends com.firstlook.mock.BaseTestCase
{
private List<Dealer> dealers;

/**
 * TestColumnFormIterator constructor comment.
 * 
 * @param arg1
 *            java.lang.String
 */
public TestColumnedFormIterator( String arg1 )
{
    super(arg1);
}

public void setupDealers( int size )
{
    dealers = new ArrayList<Dealer>(size);
    for (int i = 0; i < size; i++) {
        dealers.add(ObjectMother.createDealer());
    }
}

public void testCantBeZeroColumns() throws java.lang.Exception
{
    try
    {
        new ColumnedFormIterator(0, new ArrayList<Dealer>(), DealerForm.class);
        fail("should have thrown an illegal argument exception");
    } catch (IllegalArgumentException iae)
    {

    }
}

public void testIsLastRow10x3()
{
    setupDealers(10);

    ColumnedFormIterator cfi = new ColumnedFormIterator(3, dealers,
            DealerForm.class);

    cfi.next();
    assertEquals("Row 1 should be false", false, cfi.isCurrentRowLastRow());

    cfi.next();
    assertEquals("Row 2 should be false", false, cfi.isCurrentRowLastRow());

    cfi.next();
    assertEquals("Row 3 should be false", false, cfi.isCurrentRowLastRow());

    cfi.next();
    assertEquals("Row 4 should be true", true, cfi.isCurrentRowLastRow());

}

public void testIsLastRow11x3()
{
    setupDealers(11);

    ColumnedFormIterator cfi = new ColumnedFormIterator(3, dealers,
            DealerForm.class);

    cfi.next();
    assertEquals("Row 1 should be false", false, cfi.isCurrentRowLastRow());

    cfi.next();
    assertEquals("Row 2 should be false", false, cfi.isCurrentRowLastRow());

    cfi.next();
    assertEquals("Row 3 should be false", false, cfi.isCurrentRowLastRow());

    cfi.next();
    assertEquals("Row 4 should be true", true, cfi.isCurrentRowLastRow());

}

public void testIsLastRow12x3()
{
    setupDealers(12);

    ColumnedFormIterator cfi = new ColumnedFormIterator(3, dealers,
            DealerForm.class);

    cfi.next();
    assertEquals("Row 1 should be false", false, cfi.isCurrentRowLastRow());

    cfi.next();
    assertEquals("Row 2 should be false", false, cfi.isCurrentRowLastRow());

    cfi.next();
    assertEquals("Row 3 should be false", false, cfi.isCurrentRowLastRow());

    cfi.next();
    assertEquals("Row 4 should be true", true, cfi.isCurrentRowLastRow());

}

/**
 * Insert the method's description here. Creation date: (2/11/2002 9:31:07 AM)
 */
public void testIsLastRow9x3()
{
    setupDealers(9);

    ColumnedFormIterator cfi = new ColumnedFormIterator(3, dealers,
            DealerForm.class);

    cfi.next();
    assertEquals("Row 1 should be false", false, cfi.isCurrentRowLastRow());

    cfi.next();
    assertEquals("Row 2 should be false", false, cfi.isCurrentRowLastRow());

    cfi.next();
    assertEquals("Row 3 should be true", true, cfi.isCurrentRowLastRow());

}

public void testMissingColumns() throws java.lang.Exception
{
    setupDealers(10);

    ColumnedFormIterator cfi = new ColumnedFormIterator(3, dealers,
            DealerForm.class);
    assertEquals("missing columns", 2, cfi.getMissingColumns());

    dealers.add(ObjectMother.createDealer());
    cfi = new ColumnedFormIterator(3, dealers, DealerForm.class);
    assertEquals("missing columns", 1, cfi.getMissingColumns());

    dealers.add(ObjectMother.createDealer());
    cfi = new ColumnedFormIterator(3, dealers, DealerForm.class);
    assertEquals("missing columns", 0, cfi.getMissingColumns());
}

public void testReturnedColumnsNotEnoughGiven() throws Exception
{
    int numberOfCols = 3;
    Vector vector = new Vector();
    vector.add("String1");
    vector.add("String2");

    ColumnedFormIterator cfi = new ColumnedFormIterator(numberOfCols, vector,
            StringForm.class);

    Vector returnedVector = (Vector) cfi.next();

    Iterator iterator = returnedVector.iterator();

    assertTrue(iterator.next() instanceof StringForm);
    assertTrue(iterator.next() instanceof StringForm);
    assertEquals(false, iterator.hasNext());

}

public void testReturnedCorrectNumberOfForms() throws Exception
{
    int numberOfCols = 3;
    Vector vector = new Vector();
    vector.add("String1");
    vector.add("String2");
    vector.add("String3");
    vector.add("String4");

    ColumnedFormIterator cfi = new ColumnedFormIterator(numberOfCols, vector,
            StringForm.class);

    Vector returnedVector = (Vector) cfi.next();
    assertEquals(numberOfCols, returnedVector.size());

}
}
