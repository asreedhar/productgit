package com.firstlook.iterator;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

import com.firstlook.exception.ApplicationException;
import com.firstlook.report.CustomIndicator;
import com.firstlook.report.ReportGrouping;
import com.firstlook.report.ReportGroupingForm;

public abstract class CustomIndicatorBaseIterator extends SimpleFormIterator
{

protected CustomIndicator indicator;
protected String currentGroupingDescription;

public CustomIndicatorBaseIterator( Collection newObjects,
        CustomIndicator indicator, Class newFormClass )
{
    super(newObjects, newFormClass);
    this.indicator = indicator;
}

public int getCurrentGroupFastestSellerRank()
{
    return indicator.getFastestSellerRank(currentGroupingDescription);
}

public int getCurrentGroupMostProfitableRank()
{
    return indicator.getMostProfitableSellerRank(currentGroupingDescription);
}

public int getCurrentGroupTopSellerRank()
{
    return indicator.getTopSellerRank(currentGroupingDescription);
}

public int getCurrentGroupUnitsInStock()
{
    return indicator.getUnitsInStock(currentGroupingDescription);
}

public ReportGroupingForm getCurrentReportGrouping()
{
    Iterator iterator = indicator.getReport().getReportGroupings().iterator();
    String holder;
    ReportGrouping result = null;
    while (iterator.hasNext())
    {
        ReportGrouping reportGroup = (ReportGrouping) iterator.next();
        holder = reportGroup.getGroupingName().trim();
        if ( holder.equalsIgnoreCase(currentGroupingDescription.trim() ) )
        {
            result = reportGroup;
            break;
        }
        
    }
    if (result != null) {
    	return new ReportGroupingForm(result);
    } else {
    	return null;
    }
}

public boolean isCurrentGroupInHotlist()
{
    return indicator.isInHotlist(currentGroupingDescription);
}

public boolean isCurrentGroupRanked()
{
    return indicator.isGroupRanked(currentGroupingDescription);
}

public boolean isReportGroupingAvailable()
{
    if ( getCurrentReportGrouping() == null)
    {
    	return false;
    } 	  
    
    if(getCurrentReportGrouping().getUnitsSold() > 0  || 
    		getCurrentReportGrouping().getNoSales() > 0)
    {
        return true;
    }
    
    return false;
}

public boolean isDisplayStockingInfo()
{
    return indicator.getReport().isDisplayStockingInfo(
            currentGroupingDescription);
}

public Object next()
{
    Object form = super.next();

    try
    {
        setCurrentGroupingDescription();
    } catch (ApplicationException e)
    {
        throw new NoSuchElementException("problem setting grouping description");
    }

    return form;

}

protected abstract void setCurrentGroupingDescription()
        throws ApplicationException;

}
