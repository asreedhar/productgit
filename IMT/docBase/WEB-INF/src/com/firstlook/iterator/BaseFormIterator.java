package com.firstlook.iterator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class BaseFormIterator<E> implements Iterator< E >
{

protected int index = 0;
protected List< E > objects = new ArrayList<E>();
protected E currentObject;
protected E previousObject;

public BaseFormIterator()
{
    super();
}

/**
 * 
 */
public BaseFormIterator( Collection<E> newObjects )
{
    super();
    objects = new ArrayList< E >( newObjects );
}

/**
 * 
 */
public BaseFormIterator( List< E > newObjects )
{
    super();
    this.setObjects(newObjects);
}

public int getCurrentIndex()
{
    return index + 1;
}

public int getCurrentIndexFixed()
{
    return index;
}

/**
 * Insert the method's description here. Creation date: (2/13/2002 3:06:12 PM)
 * 
 * @return java.lang.Object
 */
protected java.lang.Object getCurrentObject()
{
    return currentObject;
}

/**
 * 
 * @return java.util.Vector
 */
public List<E> getObjects()
{
    return objects;
}

protected java.lang.Object getPreviousObject()
{
    return previousObject;
}

/**
 * 
 * @return int
 */
public int getSize()
{
    return objects.size();
}

/**
 * Returns <tt>true</tt> if the iteration has more elements. (In other words,
 * returns <tt>true</tt> if <tt>next</tt> would return an element rather
 * than throwing an exception.)
 * 
 * @return <tt>true</tt> if the iterator has more elements.
 */
public boolean hasNext()
{
    if ( index < objects.size() )
    {
        return true;
    }
    return false;
}

public boolean isFirst()
{
    return index == 1;
}

public boolean isLast()
{
    return !hasNext();
}

public boolean isOdd()
{
    return index % 2 == 1;
}

/**
 * Returns the next element in the interation.
 * 
 * @returns the next element in the interation.
 * @exception NoSuchElementException
 *                iteration has no more elements.
 */
public E next()
{
    previousObject = currentObject;
    currentObject = objects.get(index);
    index++;
    return currentObject;
}

/**
 * 
 * Removes from the underlying collection the last element returned by the
 * iterator (optional operation). This method can be called only once per call
 * to <tt>next</tt>. The behavior of an iterator is unspecified if the
 * underlying collection is modified while the iteration is in progress in any
 * way other than by calling this method.
 * 
 * @exception UnsupportedOperationException
 *                if the <tt>remove</tt> operation is not supported by this
 *                Iterator.
 * 
 * @exception IllegalStateException
 *                if the <tt>next</tt> method has not yet been called, or the
 *                <tt>remove</tt> method has already been called after the
 *                last call to the <tt>next</tt> method.
 */
public void remove()
{
    objects.remove(--index);
}

protected void setCurrentObject( E newCurrentObject )
{
    currentObject = newCurrentObject;
}

/**
 * 
 * @param newObjects
 *            java.util.Vector
 */
public void setObjects( List<E> newObjects )
{
    objects = newObjects;
}

protected void setPreviousObject( E newPreviousObject )
{
    previousObject = newPreviousObject;
}

public void sort( Comparator<E> comparator )
{
    Collections.sort(objects, comparator);
}
}
