package com.firstlook.appraisal.trademanager;


import biz.firstlook.transact.persist.service.appraisal.AppraisalActionType;

public class TradesPostedDisplayAction extends AbstractTradeManagerDisplayAction
{

@Override
protected AppraisalActionType getAppraisalActionType()
{
	return AppraisalActionType.OFFER_TO_GROUP;
}

@Override
protected String getIteratorNameOnRequest()
{
	return "tradesPostedIterator";
}

@Override
protected String getPageName()
{
	return "TradesPosted";
}

}
