package com.firstlook.appraisal.trademanager;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * This action serves as a redirect based on the existance of the CRM upgrade.
 * @author bfung
 *
 */
public class TradeManagerDisplayAction extends AbstractTradeManagerAction
{

public final static String BULLPEN = "bullpen";
public final static String AWAITING_APPRAISAL = "waitingAppraisal";

@Override
public ActionForward execute( ActionMapping mapping, ActionForm form, TradeManagerContext context, HttpServletRequest request )
{
	String pageName = request.getParameter( "pageName" );
	String forward = BULLPEN;
	if( !overrideToBullpen(pageName) )
		forward = AWAITING_APPRAISAL;
	
	return mapping.findForward( forward );
}

private boolean overrideToBullpen( String pageName )
{
	return (pageName != null && pageName.equals( BULLPEN ) );
}

}
