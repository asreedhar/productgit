package com.firstlook.appraisal.trademanager.search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.commons.util.InvalidVinException;
import biz.firstlook.commons.util.VinUtility;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.LightDetails;
import biz.firstlook.transact.persist.service.appraisal.AppraisalSource;
import biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;

import com.firstlook.appraisal.trademanager.AbstractTradeManagerAction;
import com.firstlook.appraisal.trademanager.TradeManagerContext;
import com.firstlook.entity.Dealer;
import com.firstlook.helper.SessionHelper;

public class TradeManagerSearchAppraisalsSubmitAction extends AbstractTradeManagerAction
{

public static final String AppraisalsInSessionKey = "TradeManagerSearchAppraisals";

private SearchOlderTradesReport searchOlderTradesReport;

@Override
public ActionForward execute( ActionMapping mapping, ActionForm form, TradeManagerContext context, HttpServletRequest request )
{
	Map< String, List< TradeManagerSearchLineItem >> groupedResults = new HashMap<String, List<TradeManagerSearchLineItem>>(); 
	SearchVinForm searchVinForm = loadSearchvinForm(form, request);
		
	String vinPattern = searchVinForm.getVinPattern();
	
	
	vinPattern = (vinPattern != null ? vinPattern.trim() : "");
	if (vinPattern.length() >= 3 || request.getAttribute( "customerName" ) != null) {
		searchVinForm.setVinPattern( vinPattern );
		Dealer curDealer = getDealerDAO().findByPk(getFirstlookSessionFromRequest(request).getCurrentDealerId());
		groupedResults = createLineItemsByVin( context, searchVinForm,curDealer.getDealerPreference().getSearchAppraisalDaysBackThreshold() );
		if (groupedResults.isEmpty()) {
			String customerName = (String)request.getAttribute( "customerName" );
			if (customerName == null) {
				customerName = vinPattern; // sometimes a vin pattern can be a customer name
			} 
			if (customerName != null) {
				groupedResults = createLineItemsByCustomerName(context, request, customerName);
			}
		}
	}
	request.setAttribute( "groupings", new String[] { TradeManagerSearchLineItem.APPRAISED, TradeManagerSearchLineItem.IN_INVENTORY } );
	request.setAttribute( "groupedResults", groupedResults );
	return mapping.findForward( "success" );
}

private Map<String, List<TradeManagerSearchLineItem>> createLineItemsByCustomerName(TradeManagerContext context, HttpServletRequest request, String customerName) {
	Map<String, List<TradeManagerSearchLineItem>> groupedResults;
	List<IAppraisal> appraisals = (List<IAppraisal>)SessionHelper.getAttribute(request, AppraisalsInSessionKey);
	if (appraisals != null && !appraisals.isEmpty()) {
		SessionHelper.removeKey(request, AppraisalsInSessionKey);
	} else {
		DealerPreference dealerPreference = dealerPrefDAO.findByBusinessUnitId(getCurrentDealerId(request));
		appraisals = appraisalService.findByCustomerName( getCurrentDealerId(request), customerName, dealerPreference.getSearchAppraisalDaysBackThreshold());
	}
	groupedResults = mapTradeManagerSearchLineItem(appraisals, context);
	return groupedResults;
}

private SearchVinForm loadSearchvinForm(ActionForm form, HttpServletRequest request) {
	String vin = request.getParameter( "vin" );
	SearchVinForm searchVinForm = (SearchVinForm)form;
	if ( vin != null )
	{
		if ( searchVinForm.getVinPattern() == null )
			searchVinForm.setVinPattern( vin.trim() );
	}
	return searchVinForm;
}

public Map< String, List< TradeManagerSearchLineItem >> createLineItemsByVin( TradeManagerContext context, SearchVinForm form, Integer daysBack )
{
	List< TradeManagerSearchLineItem > lineItems = searchOlderTradesReport.search( context.getDealerId(), form.getVinPattern(),  DateUtilFL.getDateInPast( daysBack ) );
	Map< String, List< TradeManagerSearchLineItem >> groupedResults = new LinkedHashMap< String, List< TradeManagerSearchLineItem >>();
	HashMap<Integer,Integer> marketPrices= new HashMap<Integer,Integer>();
	marketPrices=appraisalService.getMarketPrices( context.getDealerId());
	for ( TradeManagerSearchLineItem item : lineItems )
	{
		int marketPercentage = 0;
		int targetSellingPrice = 0;
		IAppraisal appraisal = appraisalService.findBy(item.getAppraisalId());
		int targetGrossProft = appraisal.getTargetGrossProfit() == null? 0 :appraisal.getTargetGrossProfit();
		double estimatedReconCost = appraisal.getEstimatedReconditioningCost() == null ? 0 : appraisal.getEstimatedReconditioningCost();
		item.setReconCost(appraisal.getEstimatedReconditioningCost());
		if(appraisal.getSelectedBuyer()!=null)
			item.setBuyer(appraisal.getSelectedBuyer().getFullName());
		else 
			item.setBuyer("");
		item.setAppraisalTypeId(appraisal.getAppraisalTypeId());
		item.setAppraisalType(appraisal.getAppraisalTypeId()==1?"Trade":"Purchase");
		if(appraisal !=null && appraisal.getCustomer()!=null){
			item.setCustomerName(appraisal.getCustomer().getFirstName()+" "+appraisal.getCustomer().getLastName());
		}
		LightDetails light = getLightDetail( context, item.getYear(), item.getMileage(), item.getGroupingDescriptionId() );
		item.setLight( light.getLight() );
		if ( !groupedResults.containsKey( item.getGroupLabel() ) )
			groupedResults.put( item.getGroupLabel(), new ArrayList< TradeManagerSearchLineItem >() );

		if(((marketPrices.get(appraisal.getAppraisalId()))!=null) && (appraisal.getLatestAppraisalValue()!=null)){
			if(marketPrices.get(appraisal.getAppraisalId())!=0){
				targetSellingPrice = (int)(targetGrossProft + estimatedReconCost + appraisal.getLatestAppraisalValue().getValue());
				marketPercentage = Math.round((targetSellingPrice*100.0f)/(marketPrices.get(appraisal.getAppraisalId())));
				}
			}
		item.setMarketPercentage(marketPercentage);
		List< TradeManagerSearchLineItem > items = groupedResults.get( item.getGroupLabel() );
		items.add( item );
	}
	return groupedResults;
}


public Map< String, List< TradeManagerSearchLineItem >> mapTradeManagerSearchLineItem( List<IAppraisal> appraisals,TradeManagerContext context ) 
{
	HashMap<Integer,Integer> marketPrices= new HashMap<Integer,Integer>();

	marketPrices=appraisalService.getMarketPrices( context.getDealerId());
	List< TradeManagerSearchLineItem > lineItems = new ArrayList<TradeManagerSearchLineItem>();
	for (IAppraisal appraisal : appraisals) {
		int marketPercentage = 0;
		int targetSellingPrice = 0;
		int targetGrossProft = appraisal.getTargetGrossProfit() == null? 0 :appraisal.getTargetGrossProfit();
		double estimatedReconCost = appraisal.getEstimatedReconditioningCost() == null ? 0 : appraisal.getEstimatedReconditioningCost();
		TradeManagerSearchLineItem item = new TradeManagerSearchLineItem();
		item.setAppraisalId( appraisal.getAppraisalId() );
		item.setAppraisalActionId(appraisal.getAppraisalActionType().getAppraisalActionTypeId());
		item.setAppraisalActionDescription( appraisal.getAppraisalActionType().getDescription());
		if (appraisal.getLatestAppraisalValue() != null) {
			item.setAppraisalValue( appraisal.getLatestAppraisalValue().getValue());
			item.setAppraiser( appraisal.getLatestAppraisalValue().getAppraiserName());
		}
		item.setAppraisalType(appraisal.getAppraisalTypeId()==1?"Trade":"Purchase");
		item.setColor( appraisal.getColor() );
		item.setGroupingDescriptionId( appraisal.getVehicle().getMakeModelGrouping().getGroupingDescriptionId() );
		item.setMake( appraisal.getVehicle().getMake());
		item.setModel( appraisal.getVehicle().getModel() );
		item.setMileage( appraisal.getMileage());
		item.setVin( appraisal.getVehicle().getVin());
		item.setCustomerName(appraisal.getCustomer().getFirstName() + " " + appraisal.getCustomer().getLastName() );
		// item.setLight( lightDetail.getLight() ); //set in the action, unforunately.

		if (appraisal.getDealTrackSalesPerson() != null)
			item.setSalesPerson( appraisal.getDealTrackSalesPerson().getFullName());
		item.setTradeAnalyzerDate( appraisal.getDateModified());
		item.setTrim( appraisal.getVehicle().getVehicleTrim());
		item.setYear( appraisal.getVehicle().getVehicleYear());
		
		
		AppraisalSource source = appraisal.getAppraisalSource();
		
		if ( source.equals( AppraisalSource.WAVIS ) )
		{
			item.setWirelessAppraisal( true );
		}
		else if ( source.equals( AppraisalSource.AUTOBASE ) )
		{
			item.setCrmAppraisal( true );
		}
		item.setGroupLabel( TradeManagerSearchLineItem.APPRAISED );		
		if(((marketPrices.get(appraisal.getAppraisalId()))!=null) && (appraisal.getLatestAppraisalValue()!=null)){
			if(marketPrices.get(appraisal.getAppraisalId())!=0){
				targetSellingPrice = (int)(targetGrossProft + estimatedReconCost + appraisal.getLatestAppraisalValue().getValue());
				marketPercentage = Math.round((targetSellingPrice*100.0f)/(marketPrices.get(appraisal.getAppraisalId())));
				}
			}
		item.setMarketPercentage(marketPercentage);
		
		lineItems.add(item);
	}
	Map< String, List< TradeManagerSearchLineItem >> groupedResults = new LinkedHashMap< String, List< TradeManagerSearchLineItem >>();
	for ( TradeManagerSearchLineItem item : lineItems )
	{
		LightDetails light = getLightDetail( context, item.getYear(), item.getMileage(), item.getGroupingDescriptionId() );
		item.setLight( light.getLight() );
		if ( !groupedResults.containsKey( item.getGroupLabel() ) )
			groupedResults.put( item.getGroupLabel(), new ArrayList< TradeManagerSearchLineItem >() );

		List< TradeManagerSearchLineItem > items = groupedResults.get( item.getGroupLabel() );
		items.add( item );
	}
	return groupedResults;
}

public void setSearchOlderTradesReport( SearchOlderTradesReport searchOlderTradesReport )
{
	this.searchOlderTradesReport = searchOlderTradesReport;
}

}
