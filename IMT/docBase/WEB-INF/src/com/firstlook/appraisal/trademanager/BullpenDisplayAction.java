package com.firstlook.appraisal.trademanager;


import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.firstlook.entity.form.TradeAnalyzerPlusForm;
import com.firstlook.iterator.BaseFormIterator;

import biz.firstlook.transact.persist.service.appraisal.AppraisalActionType;
import biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;

public class BullpenDisplayAction extends AbstractTradeManagerDisplayAction
{

private final static String PAGE_NAME = "Bullpen";
private final static String ITERATOR_NAME = "bullpenIterator";

@Override
protected Iterator< TradeAnalyzerPlusForm > processBody( HttpServletRequest request, TradeManagerContext context )
{
	if(null != getBuildNumber())
		request.getSession().getServletContext().setAttribute("buildNumber", this.buildNumber);
	List<IAppraisal> appraisals = appraisalService.findBy( context.getDealerId(), context.getTradeManagerDaysFilter(), new AppraisalActionType[]{ getAppraisalActionType() }, AppraisalTypeEnum.TRADE_IN, context.getShowInactiveAppraisals() );

	List< TradeAnalyzerPlusForm > taPlusForms = populateTradeAnalyzerPlusForm( context, appraisals );
	Iterator<TradeAnalyzerPlusForm> iterator = new BaseFormIterator( taPlusForms );
	
	request.setAttribute( getIteratorNameOnRequest(), iterator );
	request.setAttribute( "pageName", getPageName() );
	
	return iterator;
}


@Override
protected AppraisalActionType getAppraisalActionType()
{
	return AppraisalActionType.DECIDE_LATER;
}

@Override
protected String getIteratorNameOnRequest()
{
	return ITERATOR_NAME;
}

@Override
protected String getPageName()
{
	return PAGE_NAME;
}

}