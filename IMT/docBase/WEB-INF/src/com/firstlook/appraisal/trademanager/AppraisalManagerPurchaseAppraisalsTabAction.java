package com.firstlook.appraisal.trademanager;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import biz.firstlook.transact.persist.service.appraisal.AppraisalActionType;
import biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;

import com.firstlook.entity.form.TradeAnalyzerPlusForm;
import com.firstlook.iterator.BaseFormIterator;

public class AppraisalManagerPurchaseAppraisalsTabAction extends AbstractTradeManagerDisplayAction {

	private final static String PAGE_NAME = "Purchase";
	
	private final static String ITERATOR_NAME = "DecideLaterIterator";
	private final static String ITERATOR_SERVICELANE = "ServiceLaneIterator";
	private final static String ITERATOR_WE_BUY = "WeBuyIterator";
	private final static String ITERATOR_AUCTION = "AuctionIterator";
	@Override
	protected Iterator< TradeAnalyzerPlusForm > processBody( HttpServletRequest request, TradeManagerContext context )
	{
		if(null != getBuildNumber())
			request.getSession().getServletContext().setAttribute("buildNumber", this.buildNumber);
		
		List<IAppraisal> appraisals = appraisalService.findByforAwaiting( context.getDealerId(), context.getTradeManagerDaysFilter(), new AppraisalActionType[]{ getAppraisalActionType(),AppraisalActionType.OFFER_TO_GROUP }, AppraisalTypeEnum.PURCHASE, context.getShowInactiveAppraisals() );
		List< TradeAnalyzerPlusForm > taPlusForms = populateTradeAnalyzerPlusForm( context, appraisals );
		Iterator<TradeAnalyzerPlusForm> iterator = new BaseFormIterator( taPlusForms );
		request.setAttribute( getIteratorNameOnRequest(), iterator );
		request.setAttribute("countDecideLater", taPlusForms.size());
		
		appraisals= appraisalService.findBy( context.getDealerId(), context.getTradeManagerDaysFilter(), new AppraisalActionType[]{ getAppraisalActionTypeWeBuy() }, AppraisalTypeEnum.PURCHASE, context.getShowInactiveAppraisals() );
		List< TradeAnalyzerPlusForm > taPlusFormswebuy = populateTradeAnalyzerPlusForm( context, appraisals );
		Iterator<TradeAnalyzerPlusForm> iteratorwebuy = new BaseFormIterator( taPlusFormswebuy );
		request.setAttribute( getIteratorWeBuy(), iteratorwebuy );
		request.setAttribute("countWeBuy", taPlusFormswebuy.size());
		
		appraisals= appraisalService.findBy( context.getDealerId(), context.getTradeManagerDaysFilter(), new AppraisalActionType[]{ getAppraisalActionTypeServiceLane() }, AppraisalTypeEnum.PURCHASE, context.getShowInactiveAppraisals() );
		List< TradeAnalyzerPlusForm > taPlusFormsServiceLane= populateTradeAnalyzerPlusForm( context, appraisals );
		Iterator<TradeAnalyzerPlusForm> iteratorServiceLane= new BaseFormIterator( taPlusFormsServiceLane );
		request.setAttribute( getIteratorServicelane(), iteratorServiceLane );
		request.setAttribute("countServiceLane", taPlusFormsServiceLane.size());
		
		appraisals= appraisalService.findBy( context.getDealerId(), context.getTradeManagerDaysFilter(), new AppraisalActionType[]{ getAppraisalActionTypeAuction() }, AppraisalTypeEnum.PURCHASE, context.getShowInactiveAppraisals() );
		List< TradeAnalyzerPlusForm > taPlusFormsAuction= populateTradeAnalyzerPlusForm( context, appraisals );
		Iterator<TradeAnalyzerPlusForm> iteratorAuction= new BaseFormIterator( taPlusFormsAuction );
		request.setAttribute( getIteratorAuction(), iteratorAuction );
		request.setAttribute("countAuction", taPlusFormsAuction.size());
				
		System.out.println("inside action");
		
		request.setAttribute("iteratorNames","DecideLater,ServiceLane,WeBuy,Auction");
		request.setAttribute("count", taPlusForms.size()+taPlusFormsAuction.size()+taPlusFormsServiceLane.size()+taPlusFormswebuy.size());
		request.setAttribute( "pageName", getPageName() );
		return iterator;
	}



	@Override
	protected String getIteratorNameOnRequest()
	{
		return ITERATOR_NAME;
	}
	protected static String getIteratorServicelane() {
		return ITERATOR_SERVICELANE;
	}
	protected static String getIteratorWeBuy() {
		return ITERATOR_WE_BUY;
	}
	protected static String getIteratorAuction(){
		return ITERATOR_AUCTION;
	}	



	@Override
	protected AppraisalActionType getAppraisalActionType()
	{
		return AppraisalActionType.DECIDE_LATER;
	}
	protected AppraisalActionType getAppraisalActionTypeServiceLane()
	{
		return AppraisalActionType.PURCHASE_SERVICE_LANE;
		
	}
	protected AppraisalActionType getAppraisalActionTypeWeBuy()
	{
		return AppraisalActionType.PURCHASE_WE_BUY;
		
	}
	protected AppraisalActionType getAppraisalActionTypeAuction(){
		return AppraisalActionType.WHOLESALE_OR_AUCTION;
	}
	
	@Override
	protected String getPageName()
	{
		return PAGE_NAME;
	}

}
