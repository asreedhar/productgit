package com.firstlook.appraisal.trademanager;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.service.appraisal.AppraisalActionType;

public class AppraisalManagerAction extends AbstractTradeManagerAction {

	@Override
	protected ActionForward execute(ActionMapping mapping, ActionForm form,
			TradeManagerContext context, HttpServletRequest request) {
		// TODO Auto-generated method stub
		if(request.getParameter("selectedTab")!=null)
			request.setAttribute("selectedTab", request.getParameter("selectedTab"));
		else
			request.setAttribute("selectedTab", "all");
		Map<String, Integer> countsOfTabs = appraisalService.countAppraisalsAndGroupByAppraisalType(context.getDealerId(), getDealerDAO().findByPk(context.getDealerId()).getDealerPreference().getTradeManagerDaysFilter() , context.getShowInactiveAppraisals());
		
		request.setAttribute("tradeCount", countsOfTabs.get("1")!=null?countsOfTabs.get("1"):0);
		request.setAttribute("purchaseCount", countsOfTabs.get("2")!=null?countsOfTabs.get("2"):0);
		request.setAttribute("allCount", ((Integer)(countsOfTabs.get("1")!=null?countsOfTabs.get("1"):0))+((Integer)(countsOfTabs.get("2")!=null?countsOfTabs.get("2"):0)));
		
		
		
		System.out.println(request.getAttribute("selectedTab")+"-----------");
		return mapping.findForward("success");
	}

}
