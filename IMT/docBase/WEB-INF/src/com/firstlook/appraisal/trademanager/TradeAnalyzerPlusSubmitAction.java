package com.firstlook.appraisal.trademanager;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.service.appraisal.AppraisalFormOptions;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.dealer.tools.AbstractTradeAnalyzerFormAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.form.TradeAnalyzerForm;
import com.firstlook.exception.ApplicationException;

public class TradeAnalyzerPlusSubmitAction extends AbstractTradeAnalyzerFormAction
{

private IAppraisalService appraisalService;

protected ActionForward analyzeIt(ActionMapping mapping, TradeAnalyzerForm tradeAnalyzerForm,	HttpServletRequest request, HttpServletResponse response, Dealer dealer)
	throws DatabaseException, ApplicationException
{
	putActionErrorsInRequest( request, tradeAnalyzerForm.validateGuideBook() );
	Collection<?> errors = (Collection<?>)request.getAttribute( "errors" );
	boolean saveBumpFlag = request.getParameter( "saveBumpFlag" ) != null;

	if ( errors == null || errors.isEmpty() )
	{
		Integer appraisalId = tradeAnalyzerForm.getAppraisalId();
		if (appraisalId == null) {
			return mapping.findForward( "failure" );
		}
		IAppraisal appraisal = getAppraisalService().findBy( appraisalId );
		if (appraisal == null) {
			return mapping.findForward( "failure" );
		}
		
		super.populateAppraisal( saveBumpFlag, getMemberFromRequest( request ), tradeAnalyzerForm, appraisal );
		//more jam it in there hacking.  need to save customer offer on appraisal form.
		AppraisalFormOptions formOptions = appraisal.getAppraisalFormOptions();
		if( tradeAnalyzerForm.getOfferEditInputName() != null )
			formOptions.setAppraisalFormOffer( tradeAnalyzerForm.getOfferEditInputName() );
		appraisalService.updateAppraisal( appraisal );
		
		String goToNext = request.getParameter( "goToNext" );
		if( goToNext != null )
		{
			if( goToNext.equalsIgnoreCase(TradeManagerDisplayAction.AWAITING_APPRAISAL) )
			{
				return mapping.findForward( TradeManagerDisplayAction.AWAITING_APPRAISAL );
			} else if (goToNext.equalsIgnoreCase("quick")) {				
				return mapping.findForward( "quick" );
			}
		}

		return mapping.findForward( "success" );
	}

	return mapping.findForward( "failure" );
}

public IAppraisalService getAppraisalService()
{
	return appraisalService;
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

}