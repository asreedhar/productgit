package com.firstlook.appraisal.trademanager.export;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.service.appraisal.IAppraisal;

import com.firstlook.appraisal.trademanager.AbstractTradeManagerAction;
import com.firstlook.appraisal.trademanager.TradeManagerContext;
import com.firstlook.appraisal.trademanager.TradeManagerLineItem;

public class TradeManagerExportAction extends AbstractTradeManagerAction
{

private TradeManagerExportReport tradeManagerExportReport;

@Override
protected ActionForward execute( ActionMapping mapping, ActionForm form, TradeManagerContext context, HttpServletRequest request )
{
	Integer redistributionAction = new Integer( request.getParameter( "redistributionAction" ) );
	
//	List tradeManagerLineItems = tradeManagerExportReport.retrieveTradeManagerExportReport( context.getDealerId(), redistributionAction,
//																								context.getTradeManagerDaysFilter() );
	List<IAppraisal> appraisals = appraisalService.findBy( context.getDealerId(), context.getTradeManagerDaysFilter(), context.getShowInactiveAppraisals() );
	HashMap<Integer,Integer> marketPrices= new HashMap<Integer,Integer>();
	marketPrices=appraisalService.getMarketPrices( context.getDealerId());
	List<TradeManagerLineItem> tradeManagerLineItems=new ArrayList<TradeManagerLineItem>();
	
	for(IAppraisal appraisal:appraisals)
	{
		int percentToMarket = 0;
		int targetSellingPrice = 0;
		int targetGrossProft = appraisal.getTargetGrossProfit() == null? 0 :appraisal.getTargetGrossProfit();
		double estimatedReconCost = appraisal.getEstimatedReconditioningCost() == null ? 0 : appraisal.getEstimatedReconditioningCost(); 
		TradeManagerLineItem tradeManagerLineItem= new TradeManagerLineItem();
		tradeManagerLineItem.setAppraisalTypeId(appraisal.getAppraisalTypeId());
		tradeManagerLineItem.setAppraisalType(biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum.getType(appraisal.getAppraisalTypeId()).toString());
		tradeManagerLineItem.setAppraisalCategory(appraisal.getAppraisalActionType().getDescription());
		tradeManagerLineItem.setTradeAnalyzerDate((appraisal).getDateModified());
		if(appraisal.getVehicle()!=null){
			
			tradeManagerLineItem.setVin((appraisal).getVehicle().getVin());
			tradeManagerLineItem.setYear((appraisal).getVehicle().getVehicleYear());
			tradeManagerLineItem.setMake((appraisal).getVehicle().getMake());
			tradeManagerLineItem.setModel((appraisal).getVehicle().getModel());
			tradeManagerLineItem.setTrim((appraisal).getVehicle().getVehicleTrim());
		}
		tradeManagerLineItem.setColor(appraisal.getColor());
		tradeManagerLineItem.setMileage((appraisal).getMileage());
		if(appraisal.getLatestAppraisalValue()!=null)
			tradeManagerLineItem.setAppraisalValue((appraisal).getLatestAppraisalValue().getValue());
		else
			tradeManagerLineItem.setAppraisalValue(0);
			
		if(appraisal.getDealTrackSalesPerson()!=null)
			tradeManagerLineItem.setSalesPerson(appraisal.getDealTrackSalesPerson().getFullName());
		else
			tradeManagerLineItem.setSalesPerson("");
		if(appraisal.getCustomer()!=null)
			tradeManagerLineItem.setCustomerName(appraisal.getCustomer().getFirstName()+" "+appraisal.getCustomer().getLastName());
		else
			tradeManagerLineItem.setCustomerName("");
		
		if(appraisal.getSelectedBuyer()!=null)
			tradeManagerLineItem.setBuyer(appraisal.getSelectedBuyer().getFullName());
		else
			tradeManagerLineItem.setBuyer("");
		
		if(appraisal.getLatestAppraisalValue()!=null)
			tradeManagerLineItem.setAppraiser(appraisal.getLatestAppraisalValue().getAppraiserName());
		else
			tradeManagerLineItem.setAppraiser("");
		if(appraisal.getEstimatedReconditioningCost()!=null)
			tradeManagerLineItem.setReconCost(appraisal.getEstimatedReconditioningCost());
		tradeManagerLineItem.setLight(getLightDetail(context, tradeManagerLineItem.getYear(), tradeManagerLineItem.getMileage(), appraisal.getVehicle().getMakeModelGrouping().getGroupingDescription().getGroupingDescriptionId().intValue()).getLight());
		
		if((marketPrices.get(appraisal.getAppraisalId()))!=null && appraisal.getLatestAppraisalValue()!=null){
			if(marketPrices.get(appraisal.getAppraisalId())!=0){
				targetSellingPrice = (int) (targetGrossProft+estimatedReconCost+appraisal.getLatestAppraisalValue().getValue());
				percentToMarket = Math.round((targetSellingPrice*100.0f)/(marketPrices.get(appraisal.getAppraisalId())));
			}
			
		}
		tradeManagerLineItem.setMarketPercentage(percentToMarket);
		tradeManagerLineItems.add(tradeManagerLineItem);
	}
	
	request.setAttribute( "tradeManagerLineItems", tradeManagerLineItems );

	return mapping.findForward( "success" );
}

public void setTradeManagerExportReport( TradeManagerExportReport tradeManagerExportReport )
{
	this.tradeManagerExportReport = tradeManagerExportReport;
}

}
