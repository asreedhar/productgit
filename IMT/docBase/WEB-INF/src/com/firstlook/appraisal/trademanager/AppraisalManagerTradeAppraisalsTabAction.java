package com.firstlook.appraisal.trademanager;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import biz.firstlook.transact.persist.service.appraisal.AppraisalActionType;
import biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;

import com.firstlook.entity.form.TradeAnalyzerPlusForm;
import com.firstlook.iterator.BaseFormIterator;

public class AppraisalManagerTradeAppraisalsTabAction extends AbstractTradeManagerDisplayAction {

	private final static String PAGE_NAME = "Trade";
	
	private final static String ITERATOR_NAME = "DecideLaterIterator";
	private final static String ITERATOR_RETAIL = "RetailIterator";
	private final static String ITERATOR_WHOLESALE= "WholesaleIterator";
	private final static String ITERATOR_NOTTRADEDIN= "NotTradedInIterator";
	@Override
	protected Iterator< TradeAnalyzerPlusForm > processBody( HttpServletRequest request, TradeManagerContext context )
	{
		if(null != getBuildNumber())
			request.getSession().getServletContext().setAttribute("buildNumber", this.buildNumber);
		
		List<IAppraisal> appraisals = appraisalService.findByforAwaiting( context.getDealerId(), context.getTradeManagerDaysFilter(), new AppraisalActionType[]{ getAppraisalActionType(),AppraisalActionType.OFFER_TO_GROUP }, AppraisalTypeEnum.TRADE_IN, context.getShowInactiveAppraisals() );
		List< TradeAnalyzerPlusForm > taPlusForms = populateTradeAnalyzerPlusForm( context, appraisals );
		Iterator<TradeAnalyzerPlusForm> iterator = new BaseFormIterator( taPlusForms );
		request.setAttribute( getIteratorNameOnRequest(), iterator );
		request.setAttribute("countDecideLater", taPlusForms.size());
		
		appraisals= appraisalService.findBy( context.getDealerId(), context.getTradeManagerDaysFilter(), new AppraisalActionType[]{ getAppraisalActionTypeWholesale() }, AppraisalTypeEnum.TRADE_IN, context.getShowInactiveAppraisals() );
		List< TradeAnalyzerPlusForm > taPlusFormswholesale = populateTradeAnalyzerPlusForm( context, appraisals );
		Iterator<TradeAnalyzerPlusForm> iteratorwholesale = new BaseFormIterator( taPlusFormswholesale );
		request.setAttribute( getIteratorWholesale() ,iteratorwholesale );
		request.setAttribute("countWholesale", taPlusFormswholesale.size());
		
		appraisals= appraisalService.findBy( context.getDealerId(), context.getTradeManagerDaysFilter(), new AppraisalActionType[]{ getAppraisalActionTypeRetail() }, AppraisalTypeEnum.TRADE_IN, context.getShowInactiveAppraisals() );
		List< TradeAnalyzerPlusForm > taPlusFormsRetail= populateTradeAnalyzerPlusForm( context, appraisals );
		Iterator<TradeAnalyzerPlusForm> iteratorRetail= new BaseFormIterator( taPlusFormsRetail );
		request.setAttribute( getIteratorRetail(), iteratorRetail );
		request.setAttribute("countRetail", taPlusFormsRetail.size());
		
		appraisals= appraisalService.findBy( context.getDealerId(), context.getTradeManagerDaysFilter(), new AppraisalActionType[]{ getAppraisalActionTypeNotTradedIn() }, AppraisalTypeEnum.TRADE_IN, context.getShowInactiveAppraisals() );
		List< TradeAnalyzerPlusForm > taPlusFormsNotTradedIn= populateTradeAnalyzerPlusForm( context, appraisals );
		Iterator<TradeAnalyzerPlusForm> iteratorNotTradedIn= new BaseFormIterator( taPlusFormsNotTradedIn );
		request.setAttribute( getIteratorNotTradedIn(), iteratorNotTradedIn );
		request.setAttribute("countNotTradedIn", taPlusFormsNotTradedIn.size());
				
		System.out.println("inside action");
		
		request.setAttribute("iteratorNames","DecideLater,Retail,Wholesale,NotTradedIn");
		request.setAttribute("count", taPlusForms.size()+taPlusFormsNotTradedIn.size()+taPlusFormsRetail.size()+taPlusFormswholesale.size());
		request.setAttribute( "pageName", getPageName() );
		return iterator;
	}



	@Override
	protected String getIteratorNameOnRequest()
	{
		return ITERATOR_NAME;
	}
	protected static String getIteratorRetail() {
		return ITERATOR_RETAIL;
	}
	protected static String getIteratorWholesale() {
		return ITERATOR_WHOLESALE;
	}
	protected static String getIteratorNotTradedIn(){
		return ITERATOR_NOTTRADEDIN;
	}	



	@Override
	protected AppraisalActionType getAppraisalActionType()
	{
		return AppraisalActionType.DECIDE_LATER;
	}
	protected AppraisalActionType getAppraisalActionTypeRetail()
	{
		return AppraisalActionType.PLACE_IN_RETAIL;
		
	}
	protected AppraisalActionType getAppraisalActionTypeWholesale()
	{
		return AppraisalActionType.WHOLESALE_OR_AUCTION;
		
	}
	protected AppraisalActionType getAppraisalActionTypeNotTradedIn(){
		return AppraisalActionType.NOT_TRADED_IN;
	}
	
	@Override
	protected String getPageName()
	{
		return PAGE_NAME;
	}

}
