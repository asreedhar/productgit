package com.firstlook.appraisal.trademanager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;

public class DeleteAppraisalAction  extends SecureBaseAction {
	
	private IAppraisalService appraisalService;

	@Override
	public ActionForward doIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws DatabaseException, ApplicationException {
		IAppraisal appraisal = appraisalService.findBy(new Integer(request.getParameter("appraisalId")));
		if (appraisal != null) {
			appraisalService.deleteAppraisal(appraisal);
		}
		return mapping.findForward( "success" );
	}
	
	public void setAppraisalService(IAppraisalService appraisalService) {
		this.appraisalService = appraisalService;
	}
}
