package com.firstlook.appraisal.trademanager;


import biz.firstlook.transact.persist.service.appraisal.AppraisalActionType;

public class NTIDisplayAction extends AbstractTradeManagerDisplayAction
{

private final static String PAGE_NAME = "NotTraded";
private final static String ITERATOR_NAME = "NTIIterator";

@Override
protected AppraisalActionType getAppraisalActionType()
{
	return AppraisalActionType.NOT_TRADED_IN;
}

@Override
protected String getIteratorNameOnRequest()
{
	return ITERATOR_NAME;
}

@Override
protected String getPageName()
{
	return PAGE_NAME;
}

}
