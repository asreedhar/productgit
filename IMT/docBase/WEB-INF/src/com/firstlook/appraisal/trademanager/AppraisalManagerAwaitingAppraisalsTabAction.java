package com.firstlook.appraisal.trademanager;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.util.LabelValueBean;

import com.firstlook.entity.form.TradeAnalyzerPlusForm;
import com.firstlook.iterator.BaseFormIterator;

import biz.firstlook.transact.persist.service.appraisal.AppraisalActionType;
import biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;

public class AppraisalManagerAwaitingAppraisalsTabAction extends
		AbstractTradeManagerDisplayAction {
	private final static String PAGE_NAME = "awaiting";
	private final static String ITERATOR_NAME = "awaitingIterator";
	private final static String TODAY = "Today";
	@Override
	protected AppraisalActionType getAppraisalActionType() {
		return AppraisalActionType.WAITING_REVIEW;
	}

	@Override
	protected String getIteratorNameOnRequest() {
		return ITERATOR_NAME;
	}

	@Override
	protected String getPageName() {
		return PAGE_NAME;
	}
	
	@Override
	protected Iterator<TradeAnalyzerPlusForm> processBody(
			HttpServletRequest request, TradeManagerContext context) {
		if(null != this.buildNumber && this.buildNumber.trim().length()> 0 )
			request.getSession().getServletContext().setAttribute("buildNumber", getBuildNumber());
		System.out.println("AppraisalManagerAwaitingAppraisalsAction: buildNumber="+ getBuildNumber());
		
		List<IAppraisal> appraisals = appraisalService.findBy( context.getDealerId(), context.getTradeManagerDaysFilter(), new AppraisalActionType[]{ getAppraisalActionType() }, AppraisalTypeEnum.TRADE_IN, context.getShowInactiveAppraisals() );
		//appraisals.addAll(appraisalService.findBy( context.getDealerId(), context.getTradeManagerDaysFilter(), new AppraisalActionType[]{ getAppraisalActionType() }, AppraisalTypeEnum.PURCHASE, context.getShowInactiveAppraisals() ));
		
		List< TradeAnalyzerPlusForm > taPlusForms = populateTradeAnalyzerPlusForm( context, appraisals );
		Iterator<TradeAnalyzerPlusForm> iterator = new BaseFormIterator( taPlusForms );
		
		
		
		Map<String, List<TradeAnalyzerPlusForm>> groupedResults = new HashMap<String, List<TradeAnalyzerPlusForm>>(); 
		List<LabelValueBean> groupHeadings = groupWaitingAppraisals(iterator, groupedResults);
		if (!groupHeadings.isEmpty()) {
			request.setAttribute("groupings", groupHeadings);
			request.setAttribute("groupedResults", groupedResults);
		}
		
		request.setAttribute("awaitingList", taPlusForms);
		request.setAttribute( getIteratorNameOnRequest(), iterator );
		request.setAttribute( "appraisalActionTypeId", getAppraisalActionType().getAppraisalActionTypeId() );
		request.setAttribute("iteratorNames", "awaiting");
		request.setAttribute("count", appraisals.size());
		request.setAttribute( "pageName", getPageName() );
		return iterator;
	}


	private List<LabelValueBean> groupWaitingAppraisals(Iterator<TradeAnalyzerPlusForm> waitingAppraisalsIterator, Map<String, List<TradeAnalyzerPlusForm>> groupedResults) {
		final DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		String today = format.format(new Date());
		
		Map<String, Integer> groupings = new HashMap<String, Integer>();
		while (waitingAppraisalsIterator.hasNext()) {
			TradeAnalyzerPlusForm lineItem = waitingAppraisalsIterator.next();
			String groupingLabel = format.format(lineItem.getAppraisal().getDateCreated());
			if (groupingLabel.equals(today)) {
				groupingLabel = TODAY;
			}

			// update grouping label and counts
			if (groupings.containsKey(groupingLabel)) {
				Integer groupCount = groupings.get(groupingLabel);
				groupings.put(groupingLabel, new Integer(groupCount.intValue() + 1));
			} else {
				groupings.put(groupingLabel, new Integer(1));
			}
			
			// add line item to corresponding grouping
			if (groupedResults.containsKey(groupingLabel)) {
				List<TradeAnalyzerPlusForm> groupedResult = groupedResults.get(groupingLabel);
				groupedResult.add(lineItem);
			} else {
				ArrayList<TradeAnalyzerPlusForm> newGroupOfLineItems = new ArrayList<TradeAnalyzerPlusForm>();
				newGroupOfLineItems.add(lineItem);
				groupedResults.put(groupingLabel, newGroupOfLineItems);
			}
		}

		// translate label and counts to LabelValue bean for display
		List<LabelValueBean> grouping = new ArrayList<LabelValueBean>();
		for (String label : groupings.keySet()) {
			grouping.add(new LabelValueBean(label, groupings.get(label).toString()));
		}
		
		//write a comparator for this, after cleaning up this algorithm.
		//this will have a performance hit.
		Collections.sort( grouping, new Comparator< LabelValueBean >(){
			public int compare( LabelValueBean bean1, LabelValueBean bean2 )
			{
				try
				{
					Date date1 = bean1.getLabel().equals( TODAY ) ? new Date() : format.parse( bean1.getLabel() ); 
					Date date2 = bean2.getLabel().equals( TODAY ) ? new Date() : format.parse( bean2.getLabel() );
					//sorted in descending order.
					return -(date1.compareTo( date2 ));
				}
				catch ( ParseException e )
				{
					//don't try and sort if the date can't be figured out.
					return 0;
				}
			}
		} );
		return grouping;
	}
}
