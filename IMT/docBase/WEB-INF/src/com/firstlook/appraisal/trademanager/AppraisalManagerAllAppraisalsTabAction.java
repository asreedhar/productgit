package com.firstlook.appraisal.trademanager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.entity.form.TradeAnalyzerPlusForm;
import com.firstlook.iterator.BaseFormIterator;

import biz.firstlook.transact.persist.model.LightDetails;
import biz.firstlook.transact.persist.service.appraisal.AppraisalActionType;
import biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum;
import biz.firstlook.transact.persist.service.appraisal.AppraisalValue;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;


public class AppraisalManagerAllAppraisalsTabAction extends
		AbstractTradeManagerAction {

	@Override
	protected ActionForward execute(ActionMapping mapping, ActionForm form,
			TradeManagerContext context, HttpServletRequest request) {
		
		if(null != this.buildNumber && this.buildNumber.trim().length()> 0 )
			request.getSession().getServletContext().setAttribute("buildNumber", getBuildNumber());
		System.out.println("AppraisalManagerAllAppraisalsAction: buildNumber="+ getBuildNumber());
		
		
		List<IAppraisal> appraisals = appraisalService.findByAll( context.getDealerId(), context.getTradeManagerDaysFilter(), context.getShowInactiveAppraisals() );
		List< TradeAnalyzerPlusForm > taPlusForms = populateTradeAnalyzerPlusForm( context, appraisals );
		Iterator<TradeAnalyzerPlusForm> iterator = new BaseFormIterator( taPlusForms );
		
		
		request.setAttribute("pageName", "all");
		request.setAttribute("count", appraisals.size());
		request.setAttribute("Iterator", iterator);
		
		return mapping.findForward("success");
	}

	protected List< TradeAnalyzerPlusForm > populateTradeAnalyzerPlusForm( TradeManagerContext context, List< IAppraisal > appraisals )
	{
		HashMap<Integer,Integer> marketPrices=appraisalService.getMarketPrices( context.getDealerId());
		
		List<TradeAnalyzerPlusForm> taPlusForms = new ArrayList< TradeAnalyzerPlusForm >();
		for( IAppraisal appraisal : appraisals )
		{
			int percentToMarket = 0;
			int targetSellingPrice = 0;
			int targetGrossProft = appraisal.getTargetGrossProfit() == null? 0 :appraisal.getTargetGrossProfit();
			double estimatedReconCost = appraisal.getEstimatedReconditioningCost() == null ? 0 : appraisal.getEstimatedReconditioningCost(); 
			int mileage = appraisal.getMileage();
			int year = appraisal.getVehicle().getVehicleYear().intValue();
			int gd = appraisal.getVehicle().getMakeModelGrouping().getGroupingDescription().getGroupingDescriptionId().intValue();
			LightDetails lightDetail = getLightDetail( context, year, mileage, gd );
			TradeAnalyzerPlusForm taPlusForm = new TradeAnalyzerPlusForm( appraisal );
			taPlusForm.setGuideBookId( context.getGuideBookId() );
			taPlusForm.setAction( appraisal.getAppraisalActionType().getDescription() );
			taPlusForm.setLight( lightDetail.getLight() );
			
			AppraisalValue valueObj = appraisal.getLatestAppraisalValue();
			taPlusForm.setAppraisalValue( valueObj == null ? 0 : valueObj.getValue() );
			if((marketPrices.get(appraisal.getAppraisalId()))!=null && valueObj!=null){
				if(marketPrices.get(appraisal.getAppraisalId())!=0){
					targetSellingPrice = (int) (targetGrossProft+estimatedReconCost+valueObj.getValue());
					percentToMarket = Math.round((targetSellingPrice*100.0f)/(marketPrices.get(appraisal.getAppraisalId())));
					}
				}
				taPlusForm.setPercentToMarket(percentToMarket);
			taPlusForms.add( taPlusForm );
		}
		
		return taPlusForms;
	}
	

}
