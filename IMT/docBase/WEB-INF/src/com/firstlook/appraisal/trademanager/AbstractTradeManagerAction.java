package com.firstlook.appraisal.trademanager;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.GroupingDescriptionLight;
import biz.firstlook.transact.persist.model.LightDetails;
import biz.firstlook.transact.persist.persistence.BusinessUnitCredentialDAO;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;
import biz.firstlook.transact.persist.service.appraisal.AppraisalActionType;
import biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.appraisal.trademanager.search.SearchVinForm;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerRisk;
import com.firstlook.entity.IDealerRisk;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.dealer.IUCBPPreferenceService;
import com.firstlook.service.risklevel.GroupingDescriptionLightService;

public abstract class AbstractTradeManagerAction extends SecureBaseAction
{

private static final Log logger = LogFactory.getLog( AbstractTradeManagerAction.class );


protected IAppraisalService appraisalService;
private BusinessUnitCredentialDAO businessUnitCredentialDAO;
private IUCBPPreferenceService ucbpPreferenceService;
private GroupingDescriptionLightService groupingDescriptionLightService;
protected DealerPreferenceDAO dealerPrefDAO;

public String getBuildNumber() {
	return super.getBuildNumber();
}

public void setBuildNumber(String buildNumber) {
	super.buildNumber = buildNumber;
}

@Override
public final ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	
	if(null != getBuildNumber())
		request.getSession().getServletContext().setAttribute("buildNumber", buildNumber);
	this.logger.debug("");
	long start = System.currentTimeMillis();
	TradeManagerContext context = getTradeManagerContext( request );
	if( logger.isDebugEnabled() ) {
		logger.debug( "Create TradeManagerContext took " + (System.currentTimeMillis() - start) + " ms.");
	}
	start = System.currentTimeMillis();
	processTabs( request, context );
	if( logger.isDebugEnabled() ) {
		logger.debug( "Create Tabs on TM took " + (System.currentTimeMillis() - start) + " ms.");
	}
	ActionForward forward = execute( mapping, (SearchVinForm)form, context, request ); 
	return forward;
}

protected abstract ActionForward execute( ActionMapping mapping, ActionForm form, TradeManagerContext context, HttpServletRequest request );

protected LightDetails getLightDetail( TradeManagerContext context, int vehicleYear, int mileage, int groupingDescriptionId )
{
	IDealerRisk dealerRisk = context.getDealerRisk();
	Map< Integer, GroupingDescriptionLight > lights = context.getLights();

	int yearOffset = ucbpPreferenceService.determineRiskLevelYearOffset( dealerRisk.getRiskLevelYearRollOverMonth(),
																			dealerRisk.getRiskLevelYearInitialTimePeriod(),
																			dealerRisk.getRiskLevelYearSecondaryTimePeriod(),
																			Calendar.getInstance() );

	GroupingDescriptionLight gdLight = lights.get( groupingDescriptionId );
	if(gdLight == null) {
		gdLight = new GroupingDescriptionLight();
	}
	LightDetails lightDetail = groupingDescriptionLightService.calculateLightDetails( dealerRisk, mileage, vehicleYear, yearOffset,
			gdLight.getLight() );
	
	return lightDetail;
}

private TradeManagerContext getTradeManagerContext( HttpServletRequest request )
{
	Dealer currentDealer = putDealerFormInRequest( request );
	DealerPreference dealerPreference = dealerPrefDAO.findByBusinessUnitId(currentDealer.getDealerId());
	DealerRisk dealerRisk = ucbpPreferenceService.retrieveDealerRisk( currentDealer.getDealerId() );
	Map< Integer, GroupingDescriptionLight > lights = groupingDescriptionLightService.retrieveForDealerGroupedByGroupingDescriptionId( currentDealer.getDealerId() );
	return new TradeManagerContextImpl( currentDealer, businessUnitCredentialDAO.hasCRMCredentials( currentDealer.getDealerId() ), dealerRisk, lights, dealerPreference.getShowInactiveAppraisals(), dealerPreference.getSearchAppraisalDaysBackThreshold() );
}

private void processTabs( HttpServletRequest request, TradeManagerContext context )
{
	// These lines do nothing, but will be needed in the future
	List< AppraisalActionType > redistributionActions = appraisalService.retrieveAppraisalActionTypes();
	request.setAttribute( "redistributionActions", redistributionActions );
	//

	Map< AppraisalActionType, Integer > tradeManagerTabCounts = appraisalService.countTradeInAppraisalsAndGroupByAppraisalActionType(
																															context.getDealerId(),
																															context.getTradeManagerDaysFilter(), context.getShowInactiveAppraisals() );
	request.setAttribute( "bullpenCount", tradeManagerTabCounts.get( AppraisalActionType.DECIDE_LATER ) );
	request.setAttribute( "tradesPostedCount", tradeManagerTabCounts.get( AppraisalActionType.OFFER_TO_GROUP ) );
	request.setAttribute( "wholesalerAuctionCount", tradeManagerTabCounts.get( AppraisalActionType.WHOLESALE_OR_AUCTION ) );
	request.setAttribute( "retailCount", tradeManagerTabCounts.get( AppraisalActionType.PLACE_IN_RETAIL ) );
	request.setAttribute( "NTICount", tradeManagerTabCounts.get( AppraisalActionType.NOT_TRADED_IN ) );

	request.setAttribute( "waitingAppraisalsCount", tradeManagerTabCounts.get( AppraisalActionType.WAITING_REVIEW ) );
	request.setAttribute( "hasCRM", context.hasCrm() );
	
	//Don't putting anything below this line. PerformancePlusFilter rolls in and clobbers it with a redirect.
	request.setAttribute( "purchasesCount",  appraisalService.findBy(context.getDealerId(), context.getTradeManagerDaysFilter(), new AppraisalActionType[]{AppraisalActionType.DECIDE_LATER,AppraisalActionType.PLACE_IN_RETAIL}, AppraisalTypeEnum.PURCHASE, context.getShowInactiveAppraisals() ).size());


}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

public void setBusinessUnitCredentialDAO( BusinessUnitCredentialDAO businessUnitCredentialDAO )
{
	this.businessUnitCredentialDAO = businessUnitCredentialDAO;
}

public void setGroupingDescriptionLightService( GroupingDescriptionLightService groupingDescriptionLightService )
{
	this.groupingDescriptionLightService = groupingDescriptionLightService;
}

public void setUcbpPreferenceService( IUCBPPreferenceService ucbpPreferenceService )
{
	this.ucbpPreferenceService = ucbpPreferenceService;
}

public void setDealerPrefDAO(DealerPreferenceDAO dealerPrefDAO) {
	this.dealerPrefDAO = dealerPrefDAO;
}


}
