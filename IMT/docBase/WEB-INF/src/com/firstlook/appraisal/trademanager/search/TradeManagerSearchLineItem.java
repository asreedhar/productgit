package com.firstlook.appraisal.trademanager.search;

import com.firstlook.appraisal.trademanager.TradeManagerLineItem;

public class TradeManagerSearchLineItem extends TradeManagerLineItem
{

private static final long serialVersionUID = -7484548544354312309L;

public static final String APPRAISED = "Appraised";
public static final String IN_INVENTORY = "Currently In Inventory";

private String groupLabel;
private Integer groupingDescriptionId;
private String customerName;

public String getGroupLabel()
{
	return groupLabel;
}

public void setGroupLabel( String groupLabel )
{
	this.groupLabel = groupLabel;
}

public Integer getGroupingDescriptionId()
{
	return groupingDescriptionId;
}

public void setGroupingDescriptionId( Integer groupingDescriptionId )
{
	this.groupingDescriptionId = groupingDescriptionId;
}

public String getCustomerName() {
	return customerName;
}

public void setCustomerName(String customerName) {
	this.customerName = customerName;
}



}
