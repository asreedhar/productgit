package com.firstlook.appraisal.trademanager.export;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.CallableStatementCreatorFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;
import org.springframework.jdbc.core.support.JdbcDaoSupport;


class TradeManagerExportLineItemRetriever extends JdbcDaoSupport
{

//These can be bumped up to a super class
protected static final String RESULTS = "@RESULTS";
protected static final String RETURN_CODE = "@RC";

//These should be kept private
private static final String DEALER_ID = "@BusinessUnitID";
//private static final String APPRAISAL_ACTION_ID = "@ActionID";
private static final String REPORT_DAYS_BACK = "@ReportDate";

@SuppressWarnings("unchecked")
public List<TradeManagerExportLineItem> retrieveTradeManagerLineItems( Integer dealerId, Date daysBackDate )
{
	List storedProcedureParameters = prepareStoredProcedureParameters( new TradeManagerExportRowMapper() );

	CallableStatementCreatorFactory cscf = new CallableStatementCreatorFactory( "{? = call GetTradeManagerExport (?,?)}", storedProcedureParameters );
	
	//Map storedProcParameters = populateStoredProcedureParameterMap( dealerId, appraisalActionId, daysBackDate );
	Map storedProcParameters = populateStoredProcedureParameterMap( dealerId, daysBackDate );
	
	CallableStatementCreator csc = cscf.newCallableStatementCreator( storedProcParameters );
	
	Map results = getJdbcTemplate().call( csc, storedProcedureParameters );
	
	List<TradeManagerExportLineItem> lineItems = new ArrayList<TradeManagerExportLineItem>();
	
	if ( results.containsKey( TradeManagerExportLineItemRetriever.RETURN_CODE ) )
	{
		Integer returnCode = (Integer)results.get( TradeManagerExportLineItemRetriever.RETURN_CODE );
		if ( returnCode.intValue() == 0 )
		{
			lineItems.addAll( (List<TradeManagerExportLineItem>)results.get( TradeManagerExportLineItemRetriever.RESULTS ) );
		}
		else
		{
			//Throw some nasty errors for fun.
		}
	}
	else
	{
		//Something is seriously wrong here
	}
	
	return lineItems;
}

private Map populateStoredProcedureParameterMap( Integer dealerId, Date daysBackDate )
{
	Map<String, Object> storedProcParameters = new HashMap<String, Object>();
	storedProcParameters.put( TradeManagerExportLineItemRetriever.DEALER_ID, dealerId );
	//storedProcParameters.put( TradeManagerExportLineItemRetriever.APPRAISAL_ACTION_ID, appraisalActionId );
	storedProcParameters.put( TradeManagerExportLineItemRetriever.REPORT_DAYS_BACK, new Timestamp( daysBackDate.getTime() ) );
	return storedProcParameters;
}

private List prepareStoredProcedureParameters( RowMapper rowMapper )
{
	List<SqlParameter> storedProcedureParameters = new ArrayList<SqlParameter>();

	/*
	 * This is a set of return values. Typically when doing a prepared statment,
	 * you get a ResultSet object. When getting Out parameters from a sproc, you
	 * get a ResultSet object AND the return code, and that is why this is
	 * needed.  THIS MUST BE ADDED FIRST!
	 */
	storedProcedureParameters.add( new SqlReturnResultSet( TradeManagerExportLineItemRetriever.RESULTS, rowMapper ) );
	
	// This stores the return code from a stored proc
	storedProcedureParameters.add( new SqlOutParameter( TradeManagerExportLineItemRetriever.RETURN_CODE, Types.INTEGER ) );

	// Parameters of a stored proc
	storedProcedureParameters.add( new SqlParameter( TradeManagerExportLineItemRetriever.DEALER_ID, Types.INTEGER ) );
	//storedProcedureParameters.add( new SqlParameter( TradeManagerExportLineItemRetriever.APPRAISAL_ACTION_ID, Types.INTEGER ) );
	storedProcedureParameters.add( new SqlParameter( TradeManagerExportLineItemRetriever.REPORT_DAYS_BACK, Types.TIMESTAMP ) );
	return storedProcedureParameters;
}

class TradeManagerExportRowMapper implements RowMapper
{

public Object mapRow( ResultSet rs, int rowNumber ) throws SQLException
{
	TradeManagerExportLineItem lineItem = new TradeManagerExportLineItem();
	
	lineItem.setTradeAnalyzerDate( rs.getDate( "TradeAnalyzerDate" ) );
	lineItem.setVin( rs.getString( "VIN" ) );
	lineItem.setYear( rs.getInt( "YEAR") );
	lineItem.setMake( rs.getString( "Make" ) );
	lineItem.setModel( rs.getString( "Model" ) );
	lineItem.setTrim( rs.getString( "Trim" ) );
	//lineItem.setBodyStyle( rs.getString( "Body" ) != null ? rs.getString( "Body" ) : "" );
	lineItem.setColor( rs.getString( "Color" ) );
	lineItem.setMileage( rs.getInt( "Mileage" ) );
	lineItem.setAppraiser(rs.getString("Appraisername"));
	lineItem.setAppraisalId(rs.getInt("AppraisalId"));
	lineItem.setSalesPerson(rs.getString("SalesPerson"));
	//lineItem.setGuideBookDescription( rs.getString( "GuideBookDescription") );
	//lineItem.setPrimaryBookValue1( rs.getFloat( "PrimaryBookValue1" ) );
	//lineItem.setPrimaryBookValue2( rs.getFloat( "PrimaryBookValue2"));
	//lineItem.setPrimaryBookValue3( rs.getFloat( "PrimaryBookValue3" ) );
	//lineItem.setPrimaryBookValue4( rs.getFloat( "PrimaryBookValue4" ) );
	lineItem.setAppraisalValue( rs.getInt( "AppraisalValue" ) );
	lineItem.setCustomerName( rs.getString("CustomerName"));
	lineItem.setAppraisalType(rs.getString("AppraisalType"));
	lineItem.setAppraisalCategory(rs.getString("AppraisalCategory"));
	
	return lineItem;
}

}

}
