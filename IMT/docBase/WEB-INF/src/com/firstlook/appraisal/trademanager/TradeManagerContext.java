package com.firstlook.appraisal.trademanager;

import java.util.Map;

import biz.firstlook.transact.persist.model.GroupingDescriptionLight;

import com.firstlook.entity.IDealerRisk;

public interface TradeManagerContext
{

public Integer getDealerId();
public Integer getTradeManagerDaysFilter();
public boolean hasCrm();

public Boolean getShowInactiveAppraisals();

public Integer getGuideBookId();
public Integer getSearchAppraisalDaysBackThreshold();
public IDealerRisk getDealerRisk();
public Map<Integer, GroupingDescriptionLight> getLights();
}
