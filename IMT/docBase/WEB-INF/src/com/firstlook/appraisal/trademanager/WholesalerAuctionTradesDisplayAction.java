package com.firstlook.appraisal.trademanager;


import biz.firstlook.transact.persist.service.appraisal.AppraisalActionType;

public class WholesalerAuctionTradesDisplayAction extends AbstractTradeManagerDisplayAction
{

private final static String PAGE_NAME = "WholesalerAuction";
private final static String ITERATOR_NAME = "wholesalerAuctionIterator";

@Override
protected AppraisalActionType getAppraisalActionType()
{
	return AppraisalActionType.WHOLESALE_OR_AUCTION;
}

@Override
protected String getIteratorNameOnRequest()
{
	return ITERATOR_NAME;
}

@Override
protected String getPageName()
{
	return PAGE_NAME;
}

}
