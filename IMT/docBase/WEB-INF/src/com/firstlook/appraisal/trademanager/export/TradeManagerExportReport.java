package com.firstlook.appraisal.trademanager.export;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;

import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;


public class TradeManagerExportReport
{

private TradeManagerExportLineItemRetriever tradeManagerExportLineItemRetriever;
private DealerPreferenceDAO dealerPrefDAO;

public List<TradeManagerExportLineItem> retrieveTradeManagerExportReport( Integer dealerId, Integer appraisalActionId, Integer numberOfDaysBack )
{
	Date daysBackDate = DateUtilFL.addDaysToDate(DateUtils.truncate( new Date(), Calendar.DATE ), -(numberOfDaysBack.intValue()) );
	List<TradeManagerExportLineItem> tradeManagerExportLineItems = tradeManagerExportLineItemRetriever.retrieveTradeManagerLineItems( dealerId,
																												daysBackDate );
	
	DealerPreference dealerPreference = dealerPrefDAO.findByBusinessUnitId( dealerId );
	int guideBookId = dealerPreference.getGuideBookIdAsInt();
	
	List<TradeManagerExportLineItem> constructedTradeManagerExportLineItems = new ArrayList<TradeManagerExportLineItem>();
	for( TradeManagerExportLineItem exportItem : tradeManagerExportLineItems )
	{
		TradeManagerExportLineItem displayLineItem = constructTradeManagerExportLineItem( guideBookId, exportItem );
		constructedTradeManagerExportLineItems.add( displayLineItem );
	}


	return constructedTradeManagerExportLineItems;
}

private TradeManagerExportLineItem constructTradeManagerExportLineItem( int guideBookId, TradeManagerExportLineItem displayLineItem )
{
	switch ( guideBookId )
	{
		case 1: // BlackBook
			// Column names are
			displayLineItem.setPrimaryBookValue1Description( ThirdPartyCategory.BLACKBOOK_EXTRA_CLEAN.getCategory() );
			displayLineItem.setPrimaryBookValue2Description( ThirdPartyCategory.BLACKBOOK_CLEAN.getCategory() );
			displayLineItem.setPrimaryBookValue3Description( ThirdPartyCategory.BLACKBOOK_AVERAGE.getCategory() );
			displayLineItem.setPrimaryBookValue4Description( ThirdPartyCategory.BLACKBOOK_ROUGH.getCategory() );
			break;
		case 2: // NADA
			displayLineItem.setPrimaryBookValue1Description( ThirdPartyCategory.NADA_CLEAN_RETAIL.getCategory() );
			displayLineItem.setPrimaryBookValue2Description( ThirdPartyCategory.NADA_CLEAN_TRADE_IN.getCategory() );
			displayLineItem.setPrimaryBookValue3Description( ThirdPartyCategory.NADA_AVERAGE_TRADE_IN.getCategory() );
			displayLineItem.setPrimaryBookValue4Description( ThirdPartyCategory.NADA_CLEAN_LOAN.getCategory() );
			break;
		case 3: // KBB
			displayLineItem.setPrimaryBookValue1Description( ThirdPartyCategory.KELLEY_WHOLESALE.getCategory() );
			displayLineItem.setPrimaryBookValue2Description( ThirdPartyCategory.KELLEY_RETAIL.getCategory() );
			displayLineItem.setPrimaryBookValue3Description( null );
			displayLineItem.setPrimaryBookValue4Description( null );
			break;
		default: // GALVES?
			break;
	}
	return displayLineItem;
}

public void setTradeManagerExportLineItemRetriever( TradeManagerExportLineItemRetriever tradeManagerExportRetriever )
{
	this.tradeManagerExportLineItemRetriever = tradeManagerExportRetriever;
}

public void setDealerPrefDAO( DealerPreferenceDAO dealerPrefDAO )
{
	this.dealerPrefDAO = dealerPrefDAO;
}

}
