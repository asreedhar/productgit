package com.firstlook.appraisal.trademanager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.service.appraisal.AppraisalActionType;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;

public class TradeManagerRemovedAction extends SecureBaseAction
{

private IAppraisalService appraisalService;

public ActionForward doIt( ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response ) throws DatabaseException,
        ApplicationException
{
    int appraisalId = RequestHelper.getInt(request,
            "appraisalId");

    IAppraisal appraisal = getAppraisalService().findBy( appraisalId );
    appraisal.updateInventoryDecision( AppraisalActionType.NOT_TRADED_IN );

    getAppraisalService().updateAppraisal( appraisal );

    return mapping.findForward("success");
}

public IAppraisalService getAppraisalService()
{
	return appraisalService;
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

}
