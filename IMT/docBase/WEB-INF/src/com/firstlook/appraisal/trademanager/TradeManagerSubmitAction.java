package com.firstlook.appraisal.trademanager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.model.JobTitle;
import biz.firstlook.transact.persist.service.appraisal.AppraisalActionType;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.dealer.tools.AbstractTradeAnalyzerAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.email.LithiaCarCenterAppraisalActionChangeEmailBuilder;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.TradeManagerRedistributionForm;
import com.firstlook.exception.ApplicationException;

/**
 * This class should be a child class of AbstractTradeManagerAction instead of AbstractTradeAnalyzerAction.
 * The redistribution method needs to get factored out nicely.  It is a big rat hole leading to package dependencies.
 * 
 * @author bfung
 */
public class TradeManagerSubmitAction extends AbstractTradeAnalyzerAction
{

protected static Logger logger = Logger.getLogger( TradeManagerSubmitAction.class );

private IAppraisalService appraisalService;

public ActionForward doIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws DatabaseException, ApplicationException 
{
	TradeManagerRedistributionForm redistributionForm = (TradeManagerRedistributionForm)form;
	Map<Integer, AppraisalActionType> appraisalsToUpdate = getAppraisalsToUpdate( redistributionForm.getAppraisalAndActionPairs() );
	
	Integer dealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	Integer memberId = getMemberFromRequest( request ).getMemberId();

	//This should get extracted into the appraisal service.
	//so much unholiness here.
	if( !appraisalsToUpdate.isEmpty() )
	{
		////////// Lithia Car Center Logic starts /////////////
		DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerId( dealerId );
		Dealer dealer = getDealerDAO().findByPk( dealerId );
		Member primaryContact = null;
		List<String> carCenterEmailAddresses = new ArrayList<String>();
		if( dealerGroup.isLithiaStore() ){
			List<Member> carCenterMembers = getMemberService().getMembersByBusinessUnitAndJobTitle( dealerId, JobTitle.LITHIA_CAR_CENTER_ID );

			for(Member contact : carCenterMembers) {
				if ( contact.getMemberId().equals( dealer.getDealerPreference().getDefaultLithiaCarCenterMemberId() ) ) {
					primaryContact = contact;
				} else {
					carCenterEmailAddresses.add( contact.getEmailAddress() );
				}
			}
			if( primaryContact == null) {
				logger.error( "Dealer: " + dealerId + " does not have a DefaultLithiaCarCenterMember - no e-mail was sent!" );
			}
		}
		//////////Lithia Car Center Logic ends /////////////
		
		for( Integer appraisalId : appraisalsToUpdate.keySet() )
		{
			IAppraisal appraisal = appraisalService.findBy( appraisalId );
			
			Integer makeModelGroupingId = appraisal.getVehicle().getMakeModelGroupingId();
			Integer mileage = appraisal.getMileage();
			Integer vehicleYear = appraisal.getVehicle().getVehicleYear();
			String vin = appraisal.getVehicle().getVin();
			AppraisalActionType oldAppraisalActionType = appraisal.getAppraisalActionType();
			AppraisalActionType newAppraisalActionType = appraisalsToUpdate.get( appraisalId );
			
			processRedistribution( dealerId, memberId, appraisalId, makeModelGroupingId, mileage, vehicleYear, vin, oldAppraisalActionType, newAppraisalActionType );
			appraisalService.updateAppraisalActions( appraisal, newAppraisalActionType );
			
			// if a lithia store - then send out e-mails for changed inventory decision
			if( dealerGroup.isLithiaStore() && primaryContact != null) {
				LithiaCarCenterAppraisalActionChangeEmailBuilder emailBuilder = 
					new LithiaCarCenterAppraisalActionChangeEmailBuilder(dealer.getNickname(),  
	                                 appraisal.getVehicle().getVehicleYear(),
	                                 appraisal.getVehicle().getMake(),
	                                 appraisal.getVehicle().getModel(),
	                                 appraisal.getMileage(), 
	                                 appraisal.getAppraisalId(),
									 primaryContact.getEmailAddress(),
									 carCenterEmailAddresses,
									 newAppraisalActionType.getDescription(), 
									 getMemberFromRequest( request ).getEmailAddress() );
				
						getEmailService().sendEmail( emailBuilder );
			}
		}
	}
	
	String pageName = request.getParameter("pageName");
	if ( !StringUtils.isEmpty( pageName) )
	{
		return mapping.findForward( pageName );
	}
	return mapping.findForward( "success" );
}



private Map<Integer, AppraisalActionType> getAppraisalsToUpdate( String appraisalActionPairs ) 
{
	Map<Integer, AppraisalActionType> toBeReturned = new HashMap<Integer, AppraisalActionType>();
	if (appraisalActionPairs == null || appraisalActionPairs.length() == 0) {
		return toBeReturned;
	}
	Integer appraisalId = null;
	Integer actionId = null;
	StringTokenizer st = new StringTokenizer( appraisalActionPairs, TradeManagerRedistributionForm.MAJOR_DELIMETER );
	String token = null;
	while (st.hasMoreTokens()) 
	{
		try {
			token = st.nextToken();
			appraisalId = new Integer( StringUtils.substringBefore(token, TradeManagerRedistributionForm.MINOR_DELIMETER) );
			actionId = new Integer( StringUtils.substringAfter(token, TradeManagerRedistributionForm.MINOR_DELIMETER) );
			toBeReturned.put( appraisalId, AppraisalActionType.getAppraisalActionType( actionId ) );
		}
		catch (NumberFormatException e) {
			// ignore the non-numbers
		}
	}
	return toBeReturned;
}



public IAppraisalService getAppraisalService() {
	return appraisalService;
}

public void setAppraisalService(IAppraisalService appraisalService) {
	this.appraisalService = appraisalService;
}

}
