package com.firstlook.appraisal.trademanager.export;

import com.firstlook.appraisal.trademanager.TradeManagerLineItem;


public class TradeManagerExportLineItem extends TradeManagerLineItem
{

private static final long serialVersionUID = 6884380554844799950L;

private String bodyStyle;
private String guideBookDescription;
private String primaryBookValue1Description;
private float primaryBookValue1;
private String primaryBookValue2Description;
private float primaryBookValue2;
private String primaryBookValue3Description;
private float primaryBookValue3;
private String primaryBookValue4Description;
private float primaryBookValue4;

public String getBodyStyle()
{
	return bodyStyle;
}

public void setBodyStyle( String bodyStyle )
{
	this.bodyStyle = bodyStyle;
}

public String getGuideBookDescription()
{
	return guideBookDescription;
}

public void setGuideBookDescription( String guideBookDescription )
{
	this.guideBookDescription = guideBookDescription;
}

public float getPrimaryBookValue1()
{
	return primaryBookValue1;
}

public void setPrimaryBookValue1( float primaryBookValue1 )
{
	this.primaryBookValue1 = primaryBookValue1;
}

public String getPrimaryBookValue1Description()
{
	return primaryBookValue1Description;
}

public void setPrimaryBookValue1Description( String primaryBookValue1Description )
{
	this.primaryBookValue1Description = primaryBookValue1Description;
}

public float getPrimaryBookValue2()
{
	return primaryBookValue2;
}

public void setPrimaryBookValue2( float primaryBookValue2 )
{
	this.primaryBookValue2 = primaryBookValue2;
}

public String getPrimaryBookValue2Description()
{
	return primaryBookValue2Description;
}

public void setPrimaryBookValue2Description( String primaryBookValue2Description )
{
	this.primaryBookValue2Description = primaryBookValue2Description;
}

public float getPrimaryBookValue3()
{
	return primaryBookValue3;
}

public void setPrimaryBookValue3( float primaryBookValue3 )
{
	this.primaryBookValue3 = primaryBookValue3;
}

public String getPrimaryBookValue3Description()
{
	return primaryBookValue3Description;
}

public void setPrimaryBookValue3Description( String primaryBookValue3Description )
{
	this.primaryBookValue3Description = primaryBookValue3Description;
}

public float getPrimaryBookValue4()
{
	return primaryBookValue4;
}

public void setPrimaryBookValue4( float primaryBookValue4 )
{
	this.primaryBookValue4 = primaryBookValue4;
}

public String getPrimaryBookValue4Description()
{
	return primaryBookValue4Description;
}

public void setPrimaryBookValue4Description( String primaryBookValue4Description )
{
	this.primaryBookValue4Description = primaryBookValue4Description;
}

}
