package com.firstlook.appraisal.trademanager.search;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.firstlook.entity.form.TradeAnalyzerPlusForm;

public class SearchVinForm extends ActionForm
{

private static final long serialVersionUID = 2687880996353012486L;

private String vinPattern;

public String getVinPattern()
{
	return vinPattern;
}

public void setVinPattern( String vinPattern )
{
	this.vinPattern = vinPattern != null ? vinPattern.trim() : null;
}

private Map<String, List<TradeAnalyzerPlusForm>> groupings;

public void setGroupings( Map<String, List<TradeAnalyzerPlusForm>> groupings )
{
	this.groupings = groupings; 
}

public Map<String, List<TradeAnalyzerPlusForm>> getGroupings()
{
	return this.groupings;
}

}
