package com.firstlook.appraisal.trademanager;

import java.util.Map;

import biz.firstlook.transact.persist.model.GroupingDescriptionLight;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.IDealerRisk;

public class TradeManagerContextImpl implements TradeManagerContext
{
//
private Integer dealerId;
private Integer guideBookId;
private Integer tradeManagerDaysFilter;
private boolean hasCrm;
private IDealerRisk dealerRisk;
private Map< Integer, GroupingDescriptionLight > storeModelLights;
private Boolean showInactiveAppraisals;
private Integer searchAppraisalDaysBackThreshold;

/**
 * Stores the required pieces of information to contruct the Trade Manager.
 * @param dealer
 * @param hasCrm
 * @param dealerRisk
 * @param lights - a map of GroupingDescriptionId => GroupingDescriptionLight
 */
public TradeManagerContextImpl( final Dealer dealer, final boolean hasCrm, final IDealerRisk dealerRisk, final Map<Integer, GroupingDescriptionLight> lights, Boolean showInactiveAppraisals, Integer searchAppraisalDaysBackThreshold )
{
	this.dealerId = dealer.getBusinessUnitId();
	this.guideBookId = dealer.getDealerPreference().getGuideBookIdAsInt();
	this.tradeManagerDaysFilter = dealer.getDealerPreference().getTradeManagerDaysFilter();
	this.hasCrm = hasCrm;
	this.dealerRisk = dealerRisk;
	this.storeModelLights = lights;
	this.showInactiveAppraisals = showInactiveAppraisals;
	this.searchAppraisalDaysBackThreshold = searchAppraisalDaysBackThreshold;
}

public Integer getDealerId()
{
	return dealerId;
}

public Integer getGuideBookId()
{
	return guideBookId;
}

public Integer getTradeManagerDaysFilter()
{
	return tradeManagerDaysFilter;
}

public boolean hasCrm()
{
	return hasCrm;
}

public IDealerRisk getDealerRisk()
{
	return dealerRisk;
}

public Map< Integer, GroupingDescriptionLight > getLights()
{
	return storeModelLights;
}

public Boolean getShowInactiveAppraisals() {
	return showInactiveAppraisals;
}

public void setShowInactiveAppraisals(Boolean showInactiveAppraisals) {
	this.showInactiveAppraisals = showInactiveAppraisals;
}

public Integer getSearchAppraisalDaysBackThreshold() {
	return searchAppraisalDaysBackThreshold;
}

public void setSearchAppraisalDaysBackThreshold(
		Integer searchAppraisalDaysBackThreshold) {
	this.searchAppraisalDaysBackThreshold = searchAppraisalDaysBackThreshold;
}

}
