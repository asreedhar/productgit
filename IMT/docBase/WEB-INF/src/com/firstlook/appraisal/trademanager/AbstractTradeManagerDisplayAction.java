package com.firstlook.appraisal.trademanager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.model.LightDetails;
import biz.firstlook.transact.persist.service.appraisal.AppraisalActionType;
import biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum;
import biz.firstlook.transact.persist.service.appraisal.AppraisalValue;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;

import com.firstlook.appraisal.trademanager.search.SearchVinForm;
import com.firstlook.entity.form.TradeAnalyzerPlusForm;
import com.firstlook.iterator.BaseFormIterator;

abstract class AbstractTradeManagerDisplayAction extends AbstractTradeManagerAction
{

@Override
protected ActionForward execute( ActionMapping mapping, ActionForm form, TradeManagerContext context, HttpServletRequest request )
{
	processBody( request, context );
	
	request.setAttribute( "appraisalActionTypeId", getAppraisalActionType().getAppraisalActionTypeId() );
	
	if( request.getAttribute("groupedResults" ) != null )
	{
		//((SearchVinForm)form).setGroupings( (Map<String, List<TradeAnalyzerPlusForm>>)request.getAttribute("groupedResults")  );
	}
	
	ActionForward forward = mapping.findForward( "success" );
	
	return forward;
}

protected abstract AppraisalActionType getAppraisalActionType();

protected abstract String getIteratorNameOnRequest();

protected abstract String getPageName();

@SuppressWarnings("unchecked")
protected Iterator< TradeAnalyzerPlusForm > processBody( HttpServletRequest request, TradeManagerContext context )
{
	List<IAppraisal> appraisals = appraisalService.findBy( context.getDealerId(), context.getTradeManagerDaysFilter(), new AppraisalActionType[]{ getAppraisalActionType() }, AppraisalTypeEnum.TRADE_IN, context.getShowInactiveAppraisals() );

	List< TradeAnalyzerPlusForm > taPlusForms = populateTradeAnalyzerPlusForm( context, appraisals );
	Iterator<TradeAnalyzerPlusForm> iterator = new BaseFormIterator( taPlusForms );
	
	request.setAttribute( getIteratorNameOnRequest(), iterator );
	request.setAttribute( "pageName", getPageName() );
	
	request.setAttribute( "appraisalActionTypeId", getAppraisalActionType().getAppraisalActionTypeId() );
	
	return iterator;
}

protected List< TradeAnalyzerPlusForm > populateTradeAnalyzerPlusForm( TradeManagerContext context, List< IAppraisal > appraisals )
{
	List<TradeAnalyzerPlusForm> taPlusForms = new ArrayList< TradeAnalyzerPlusForm >();
	HashMap<Integer,Integer> marketPrices= new HashMap<Integer,Integer>();
	marketPrices=appraisalService.getMarketPrices( context.getDealerId());
	for( IAppraisal appraisal : appraisals )
	{
		int percentToMarket = 0;
		int targetSellingPrice = 0;
		int targetGrossProft = appraisal.getTargetGrossProfit() == null? 0 :appraisal.getTargetGrossProfit();
		double estimatedReconCost = appraisal.getEstimatedReconditioningCost() == null ? 0 : appraisal.getEstimatedReconditioningCost(); 
		int mileage = appraisal.getMileage();
		int year = appraisal.getVehicle().getVehicleYear().intValue();
		int gd = appraisal.getVehicle().getMakeModelGrouping().getGroupingDescription().getGroupingDescriptionId().intValue();
		LightDetails lightDetail = getLightDetail( context, year, mileage, gd );
		TradeAnalyzerPlusForm taPlusForm = new TradeAnalyzerPlusForm( appraisal );
		taPlusForm.setGuideBookId( context.getGuideBookId() );
		taPlusForm.setAction( appraisal.getAppraisalActionType().getDescription() );
		taPlusForm.setLight( lightDetail.getLight() );
		
		AppraisalValue valueObj = appraisal.getLatestAppraisalValue();
		taPlusForm.setAppraisalValue( valueObj == null ? 0 : valueObj.getValue() );
		if((marketPrices.get(appraisal.getAppraisalId()))!=null && valueObj!=null){
			if(marketPrices.get(appraisal.getAppraisalId())!=0){
				targetSellingPrice = (int) (targetGrossProft+estimatedReconCost+valueObj.getValue());
				percentToMarket = Math.round((targetSellingPrice*100.0f)/(marketPrices.get(appraisal.getAppraisalId())));
				}
			}
			taPlusForm.setPercentToMarket(percentToMarket);
		
		taPlusForms.add( taPlusForm );
	}
	
	return taPlusForms;
}

}
