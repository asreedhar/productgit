/**
 * 
 */
package com.firstlook.appraisal.trademanager;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.firstlook.util.Formatter;

public class TradeManagerLineItem implements Serializable
{

private static final long serialVersionUID = -6585591993458640468L;

private Integer appraisalId;
private boolean wirelessAppraisal;
private boolean crmAppraisal;
private Integer appraisalTypeId;
private Integer light;
private Date tradeAnalyzerDate;
private Integer year;
private String make;
private String model;
private String trim;
private String color;
private Integer mileage;
private String salesPerson;
private Integer appraisalValue;
private String appraiser;
private Integer appraisalActionId;
private String appraisalActionDescription;
private String vin;
private String customerName; 
private String appraisalType;
private String appraisalCategory;
private Double reconCost;
private String buyer;
private Integer marketPercentage;


public String getTradeAnalyzerDateFormatted()
{
	SimpleDateFormat dateFormat = new SimpleDateFormat( "MM/dd/yyyy" );

	if ( tradeAnalyzerDate !=null)
	{
		return dateFormat.format( tradeAnalyzerDate);
	}

	return Formatter.NOT_AVAILABLE;
}

public String getAppraiserOrBuyer(){
	if(appraisalTypeId==1)
		return getAppraiser();
	else 
		return getBuyer();
	
}
public Integer getAppraisalTypeId() {
	return appraisalTypeId;
}

public void setAppraisalTypeId(Integer appraisalTypeId) {
	this.appraisalTypeId = appraisalTypeId;
}

public String getBuyer() {
	return buyer;
}

public void setBuyer(String buyer) {
	this.buyer = buyer;
}

public Double getReconCost() {
	return reconCost;
}

public void setReconCost(Double reconCost) {
	this.reconCost = reconCost;
}

public String getAppraisalType() {
	return appraisalType;
}

public void setAppraisalType(String appraisalType) {
	this.appraisalType = appraisalType;
}

public String getAppraisalCategory() {
	return appraisalCategory;
}

public void setAppraisalCategory(String appraisalCategory) {
	this.appraisalCategory = appraisalCategory;
}

public String getCustomerName() {
	return customerName;
}

public void setCustomerName(String customerName) {
	this.customerName = customerName;
}

public Integer getAppraisalActionId()
{
	return appraisalActionId;
}

public void setAppraisalActionId( Integer appraisalActionId )
{
	this.appraisalActionId = appraisalActionId;
}

public Integer getAppraisalValue()
{
	return appraisalValue;
}

public void setAppraisalValue( Integer appraisalValue )
{
	this.appraisalValue = appraisalValue;
}

public String getColor()
{
	return color;
}

public void setColor( String color )
{
	this.color = color;
}

public String getMake()
{
	return make;
}

public void setMake( String make )
{
	this.make = make;
}

public Integer getMileage()
{
	return mileage;
}

public void setMileage( Integer mileage )
{
	this.mileage = mileage;
}

public String getModel()
{
	return model;
}

public void setModel( String model )
{
	this.model = model;
}

public Integer getLight()
{
	return light;
}

public void setLight( Integer risk )
{
	this.light = risk;
}

public String getSalesPerson()
{
	return salesPerson;
}

public void setSalesPerson( String salesPerson )
{
	this.salesPerson = salesPerson;
}

public Date getTradeAnalyzerDate()
{
	return tradeAnalyzerDate;
}

public void setTradeAnalyzerDate( Date tradeAnalyzed )
{
	this.tradeAnalyzerDate = tradeAnalyzed;
}

public String getTrim()
{
	return trim;
}

public void setTrim( String trim )
{
	this.trim = trim;
}

public Integer getYear()
{
	return year;
}

public void setYear( Integer year )
{
	this.year = year;
}

public Integer getAppraisalId()
{
	return appraisalId;
}

public void setAppraisalId( Integer appraisalId )
{
	this.appraisalId = appraisalId;
}

public boolean isWirelessAppraisal()
{
	return wirelessAppraisal;
}

public void setWirelessAppraisal( boolean wirelessAppraisal )
{
	this.wirelessAppraisal = wirelessAppraisal;
}

public boolean isCrmAppraisal()
{
	return crmAppraisal;
}

public void setCrmAppraisal( boolean crmAppraisal )
{
	this.crmAppraisal = crmAppraisal;
}

public String getAppraiser()
{
	return appraiser;
}

public void setAppraiser( String appraiser )
{
	this.appraiser = appraiser;
}

public String getAppraisalActionDescription()
{
	return appraisalActionDescription;
}

public void setAppraisalActionDescription( String appraisalActionDescription )
{
	this.appraisalActionDescription = appraisalActionDescription;
}

public String getVin() {
	return vin;
}

public void setVin(String vin) {
	this.vin = vin;
}

public Integer getMarketPercentage() {
	return marketPercentage;
}

public void setMarketPercentage(Integer marketPercentage) {
	this.marketPercentage = marketPercentage;
}

}