package com.firstlook.appraisal.trademanager;


import biz.firstlook.transact.persist.service.appraisal.AppraisalActionType;

public class RetailTradesDisplayAction extends AbstractTradeManagerDisplayAction
{

@Override
protected AppraisalActionType getAppraisalActionType()
{
	return AppraisalActionType.PLACE_IN_RETAIL;
}

@Override
protected String getIteratorNameOnRequest()
{
	return "retailIterator";
}

@Override
protected String getPageName()
{
	return "Retail";
}

}