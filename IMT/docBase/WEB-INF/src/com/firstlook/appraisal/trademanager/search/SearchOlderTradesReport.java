package com.firstlook.appraisal.trademanager.search;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import biz.firstlook.transact.persist.person.Person;
import biz.firstlook.transact.persist.service.appraisal.AppraisalSource;


public class SearchOlderTradesReport extends JdbcDaoSupport
{

private static final Log logger = LogFactory.getLog( SearchOlderTradesReport.class );

private static String getQuery( String vinPattern )
{
	/* START OF NEW CHANGE FOR  30527 - Allow users to search Appraisal Manager by Customer Name */	
	String query="";
	if(vinPattern.contains(" ")){
		int total = vinPattern.length();
		int space = vinPattern.indexOf(" ");
		String firstword = vinPattern.substring(0, space);
		String secWord = vinPattern.substring(space+1,total);
		 query = " AND (v.vin like '%"+vinPattern+"%' OR appCust.FirstName like '%"+vinPattern+"%' OR appCust.LastName like '%"+vinPattern+"%' OR appCust.FirstName like '%"+firstword+"%' OR appCust.LastName like '%"+secWord+"%' OR appCust.FirstName like '%"+secWord+"%' OR appCust.LastName like '%"+firstword+"%') ";
		}
	else{
		 query = " AND (v.vin like '%"+vinPattern+"%' OR appCust.FirstName like '%"+vinPattern+"%' OR appCust.LastName like '%"+vinPattern+"%') ";
		}
	/* END OF NEW CHANGE  30527 - Allow users to search Appraisal Manager by Customer Name */
	
	StringBuilder queryBuilder = new StringBuilder();
	queryBuilder.append( " SELECT a.appraisalId " );
	queryBuilder.append( " , asource.Description as source" );
	queryBuilder.append( " , mmg.GroupingDescriptionId" );
	queryBuilder.append( " , v.vehicleYear, v.make, v.model" );
	queryBuilder.append( " , v.vehicleTrim, v.baseColor" );
	queryBuilder.append( " , v.vin" );
	queryBuilder.append( " , a.dateCreated, a.mileage" );
	queryBuilder.append( " , ACV.Value, ACV.AppraiserName" );
	queryBuilder.append( " , p.FirstName, p.LastName" );
	queryBuilder.append( " , aat.AppraisalActionTypeID, aat.Description as appraisalActionDescription" );
	queryBuilder.append( " , i.inventoryId" );
	queryBuilder.append( " FROM Appraisals a" );
	queryBuilder.append( " JOIN AppraisalSource asource on a.appraisalSourceId = asource.appraisalSourceId" );
	queryBuilder.append( " LEFT JOIN (  SELECT aValue.AppraisalID, aValue.Value, aValue.AppraiserName" );
	queryBuilder.append( "   FROM AppraisalValues aValue" );
	queryBuilder.append( "   JOIN (	SELECT AppraisalID, max(SequenceNumber) SequenceNumber " );
	queryBuilder.append( "     FROM AppraisalValues aValue2 GROUP BY AppraisalID " );
	queryBuilder.append( "   ) AC on aValue.AppraisalID = AC.AppraisalID and aValue.SequenceNumber = AC.SequenceNumber" );
	queryBuilder.append( " ) ACV on a.AppraisalID = ACV.AppraisalID" );
	queryBuilder.append( " LEFT JOIN Person p on a.DealTrackSalespersonID = p.PersonId" );
	queryBuilder.append( " LEFT JOIN AppraisalCustomer appCust on a.AppraisalID = appCust.AppraisalID");
	queryBuilder.append( " JOIN AppraisalActions aa on a.appraisalID = aa.appraisalID" );
	queryBuilder.append( " JOIN AppraisalActionTypes aat on aa.AppraisalActionTypeID = aat.AppraisalActionTypeID" );
	queryBuilder.append( " INNER JOIN Vehicle v ON a.VehicleId = v.vehicleId" );
	queryBuilder.append( " INNER JOIN MakeModelGrouping mmg on v.makeModelGroupingId = mmg.makeModelGroupingId" );
	queryBuilder.append( " LEFT JOIN Inventory i ON a.vehicleId = i.vehicleId" );
	queryBuilder.append( " AND a.businessUnitId = i.businessUnitId AND i.inventoryActive = 1" );
	queryBuilder.append( " WHERE a.businessUnitId = ?" );
	queryBuilder.append( " AND a.dateModified >= ? " );
	queryBuilder.append(" "+query+" ");
/*	this was old query - changed for 527
 * queryBuilder.append( " AND (v.vin like '%" ).append( vinPattern ).append( "%'" );
	queryBuilder.append(" OR appCust.FirstName like '%").append( vinPattern ).append( "%'" );
	queryBuilder.append(" OR appCust.LastName like '%").append( vinPattern ).append( "%')" );*/
	queryBuilder.append( " order by a.DateCreated desc" );
	return queryBuilder.toString();
}

/**
 * 
 * @param businessUnitId
 * @param vinPattern
 * @return
 */
@SuppressWarnings( "unchecked" )
public List< TradeManagerSearchLineItem > search( Integer businessUnitId, String vinPattern, final Date baseDate )
{
	long start = System.currentTimeMillis();
	List< TradeManagerSearchLineItem > lineItems = (List< TradeManagerSearchLineItem >)getJdbcTemplate().query(
																												getQuery( vinPattern ),
																												new Object[] { businessUnitId, baseDate },
																												new SearchOlderTradesRowMapper( businessUnitId ) );
	if( logger.isDebugEnabled())
		logger.debug( "Query and RowMapper took: " + (System.currentTimeMillis() - start) + " ms.");
	
	return lineItems;
}

class SearchOlderTradesRowMapper implements RowMapper
{

private Integer businessUnitId;

SearchOlderTradesRowMapper( Integer businessUnitId )
{
	this.businessUnitId = businessUnitId;
}

public Object mapRow( ResultSet rs, int index ) throws SQLException
{
	TradeManagerSearchLineItem item = new TradeManagerSearchLineItem();
	item.setAppraisalId( rs.getInt( "appraisalId" ) );
	item.setAppraisalActionId( rs.getInt( "AppraisalActionTypeID" ) );
	item.setAppraisalActionDescription( rs.getString( "appraisalActionDescription" ) );
	item.setAppraisalValue( rs.getInt( "Value" ) );
	item.setAppraiser( rs.getString( "AppraiserName" ) );
	item.setColor( rs.getString( "baseColor" ) );
	item.setGroupingDescriptionId( rs.getInt( "GroupingDescriptionId" ) );
	item.setMake( rs.getString( "make" ) );
	item.setModel( rs.getString( "model" ) );
	item.setMileage( rs.getInt( "mileage" ) );
	item.setVin( rs.getString( "vin" ) );
	// item.setLight( lightDetail.getLight() ); //set in the action, unforunately.

	Person salesPerson = new Person( businessUnitId, ( rs.getString( "LastName" ) == null ? "" : rs.getString( "LastName" ) ) );
	salesPerson.setFirstName( rs.getString( "FirstName" ) );
	item.setSalesPerson( salesPerson.getFullName() );

	item.setTradeAnalyzerDate( rs.getDate( "dateCreated" ) );
	item.setTrim( rs.getString( "vehicleTrim" ) );
	item.setYear( rs.getInt( "vehicleYear" ) );
	String source = rs.getString( "source" );
	if ( source.equals( AppraisalSource.WAVIS.getDescription() ) )
	{
		item.setWirelessAppraisal( true );
	}
	else if ( source.equals( AppraisalSource.AUTOBASE.getDescription() ) )
	{
		item.setCrmAppraisal( true );
	}

	String inventoryId = rs.getString( "inventoryId" );
	if ( inventoryId != null )
		item.setGroupLabel( TradeManagerSearchLineItem.IN_INVENTORY );
	else
		item.setGroupLabel( TradeManagerSearchLineItem.APPRAISED );

	return item;
}

}

}
