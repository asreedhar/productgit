package com.firstlook.appraisal.trademanager;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.service.appraisal.AppraisalBusinessUnitGroupDemandDAO;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;

public class TradeManagerSoldAction extends SecureBaseAction
{

private IAppraisalService appraisalService;
private AppraisalBusinessUnitGroupDemandDAO appraisalBusinessUnitGroupDemandDAO;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int appraisalId = RequestHelper.getInt( request, "appraisalId" );

	IAppraisal appraisal = getAppraisalService().findBy( appraisalId );
	appraisal.setDealCompleted( Boolean.TRUE );
	appraisal.setDateModified(new Date());
	appraisalService.updateAppraisal( appraisal );

	appraisalBusinessUnitGroupDemandDAO.removeAppraisalInGroupDemand( appraisalId );

	return mapping.findForward( "success" );
}

public IAppraisalService getAppraisalService()
{
	return appraisalService;
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

public void setAppraisalBusinessUnitGroupDemandDAO( AppraisalBusinessUnitGroupDemandDAO appraisalBusinessUnitGroupDemandDAO )
{
	this.appraisalBusinessUnitGroupDemandDAO = appraisalBusinessUnitGroupDemandDAO;
}

}