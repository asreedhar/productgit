package com.firstlook.display.cia;

import java.util.List;

public class CIAReportSegmentAndData
{

private String segment;
private List ciaReportDisplayDatas;

public CIAReportSegmentAndData( String segment,
        List ciaReportDisplayDatas )
{
    this.segment = segment;
    this.ciaReportDisplayDatas = ciaReportDisplayDatas;
}

public List getCiaReportDisplayDatas()
{
    return ciaReportDisplayDatas;
}

public String getSegment()
{
    return segment;
}

}
