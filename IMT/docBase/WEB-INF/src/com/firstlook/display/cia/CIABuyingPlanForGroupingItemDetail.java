package com.firstlook.display.cia;

public class CIABuyingPlanForGroupingItemDetail
{
	// YEAR: 2001, 2002, etc
	// COOR: Blue, Red, etc.
	// TRIM:
	private String value;
	
	
	private int buyAmount;

public CIABuyingPlanForGroupingItemDetail()
{
	super();
	value = null;
	buyAmount = -1;
}

public int getBuyAmount()
{
	return buyAmount;
}

public void setBuyAmount( int buyAmount )
{
	this.buyAmount = buyAmount;
}

public String getValue()
{
	return value;
}

public void setValue( String value )
{
	this.value = value;
}

}
