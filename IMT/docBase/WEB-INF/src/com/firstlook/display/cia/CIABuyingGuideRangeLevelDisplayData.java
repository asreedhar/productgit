package com.firstlook.display.cia;

public class CIABuyingGuideRangeLevelDisplayData
{

private int buyAmount;
private String unitCostPointRangeString;

public CIABuyingGuideRangeLevelDisplayData()
{
    super();
}

public int getBuyAmount()
{
    return buyAmount;
}

public String getUnitCostPointRangeString()
{
    return unitCostPointRangeString;
}

public void setBuyAmount( int i )
{
    buyAmount = i;
}

public void setUnitCostPointRangeString( String string )
{
    unitCostPointRangeString = string;
}

}
