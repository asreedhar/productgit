package com.firstlook.display.cia;

import java.util.List;
import java.util.Map;
import java.util.Set;

import biz.firstlook.cia.model.CIACategory;
import biz.firstlook.transact.persist.model.GroupingDescriptionLight;

public class CIAGroupingItemDisplayData
{

private Integer ciaGroupingItemId;
private Integer groupingDescriptionId;
private String notes;
private Double marketPenetration;
private String marketPenetrationFormatted;
private int unitsInStock;
private int targetInventory;
private Double marketShare;
private int valuation;
private GroupingDescriptionLight light;
private String groupingDescriptionString;
private Set ciaGroupingItemDetails;
private CIACategory ciaCategory;

private List understockedYears;

//list of years that are not understocked
private List yearsStringsList;

//Map of CIABuyingPlanForGroupingItemDetail.
// TODO reconcile this with yearsStringsList
private Map buyingPlanMap; 

// Map of annual ROI amounts for each year
private transient Map annualRoiMap;

public CIAGroupingItemDisplayData()
{
}

public CIAGroupingItemDisplayData(Integer ciaGroupingItemId,
                                  Integer groupingDescriptionId,
                                  String notes,
                                  Double marketPenetration,
                                  CIACategory ciaCategory,
                                  Double marketShare)
{
	this.ciaGroupingItemId = ciaGroupingItemId;
	this.groupingDescriptionId = groupingDescriptionId;
	this.notes = notes;
	this.marketPenetration = marketPenetration;
	this.ciaCategory = ciaCategory;
	this.marketShare = marketShare;
}

public Set getCiaGroupingItemDetails()
{
	return ciaGroupingItemDetails;
}

public void setCiaGroupingItemDetails( Set ciaGroupingItemDetails )
{
	this.ciaGroupingItemDetails = ciaGroupingItemDetails;
}

public Integer getCiaGroupingItemId()
{
	return ciaGroupingItemId;
}

public void setCiaGroupingItemId( Integer ciaGroupingItemId )
{
	this.ciaGroupingItemId = ciaGroupingItemId;
}

public String getGroupingDescriptionString()
{
	return groupingDescriptionString;
}

public void setGroupingDescriptionString( String groupingDescription )
{
	this.groupingDescriptionString = groupingDescription;
}

public Integer getGroupingDescriptionId()
{
	return groupingDescriptionId;
}

public void setGroupingDescriptionId( Integer groupingDescriptionId )
{
	this.groupingDescriptionId = groupingDescriptionId;
}

public GroupingDescriptionLight getLight()
{
	return light;
}

public void setLight( GroupingDescriptionLight light )
{
	this.light = light;
}

public Double getMarketPenetration()
{
	return marketPenetration;
}

public void setMarketPenetration( Double marketPenetration )
{
	this.marketPenetration = marketPenetration;
}

public String getMarketPenetrationFormatted()
{
	return marketPenetrationFormatted;
}

public void setMarketPenetrationFormatted( String marketPenetrationFormatted )
{
	this.marketPenetrationFormatted = marketPenetrationFormatted;
}

public Double getMarketShare()
{
	return marketShare;
}

public void setMarketShare( Double marketShare )
{
	this.marketShare = marketShare;
}

public String getNotes()
{
	return notes;
}

public void setNotes( String notes )
{
	this.notes = notes;
}

public int getTargetInventory()
{
	return targetInventory;
}

public void setTargetInventory( int targetInventory )
{
	this.targetInventory = targetInventory;
}

public int getUnitsInStock()
{
	return unitsInStock;
}

public void setUnitsInStock( int unitsInStock )
{
	this.unitsInStock = unitsInStock;
}

public CIACategory getCiaCategory()
{
	return ciaCategory;
}

public void setCiaCategory( CIACategory ciaCategory )
{
	this.ciaCategory = ciaCategory;
}


public List getYearsStringsList()
{
	return yearsStringsList;
}

public void setYearsStringsList( List yearsList )
{
	this.yearsStringsList = yearsList;
}

public List getUnderstockedYears()
{
	return understockedYears;
}

public void setUnderstockedYears( List understockedYears )
{
	this.understockedYears = understockedYears;
}

public Map getBuyingPlanMap()
{
	return buyingPlanMap;
}

public void setBuyingPlanMap( Map buyingPlanMap )
{
	this.buyingPlanMap = buyingPlanMap;
}

public int getValuation()
{
	return valuation;
}

public void setValuation( int valuation )
{
	this.valuation = valuation;
}

public Map getAnnualRoiMap()
{
	return annualRoiMap;
}

public void setAnnualRoiMap( Map annualRoiMap )
{
	this.annualRoiMap = annualRoiMap;
}

}

