package com.firstlook.display.cia;

import java.util.List;

public class CIABuyingGuideDisplayData
{

private Integer ciaGroupingItemId;
private Integer groupingDescriptionId;
private String groupingDescription;
private int totalBuyAmountForModel;
private List buyAmountDetails;
private List preferYears;
private List preferTrims;
private List preferColors;
private List avoidYears;
private List avoidTrims;
private List avoidColors;
private List avoidUnitCostPointRangeStrings;
private List preferUnitCostPointRangeStrings;
private String notes;

public CIABuyingGuideDisplayData()
{
    super();
}

public String getGroupingDescription()
{
    return groupingDescription;
}

public String getNotes()
{
    return notes;
}

public List getBuyAmountDetails()
{
    return buyAmountDetails;
}

public int getTotalBuyAmountForModel()
{
    return totalBuyAmountForModel;
}

public void setGroupingDescription( String string )
{
    groupingDescription = string;
}

public void setNotes( String string )
{
    notes = string;
}

public void setBuyAmountDetails( List list )
{
    buyAmountDetails = list;
}

public void setTotalBuyAmountForModel( int i )
{
    totalBuyAmountForModel = i;
}

public List getAvoidColors()
{
    return avoidColors;
}

public List getAvoidTrims()
{
    return avoidTrims;
}

public List getAvoidUnitCostPointRangeStrings()
{
    return avoidUnitCostPointRangeStrings;
}

public List getAvoidYears()
{
    return avoidYears;
}

public List getPreferColors()
{
    return preferColors;
}

public List getPreferTrims()
{
    return preferTrims;
}

public List getPreferYears()
{
    return preferYears;
}

public void setAvoidColors( List list )
{
    avoidColors = list;
}

public void setAvoidTrims( List list )
{
    avoidTrims = list;
}

public void setAvoidUnitCostPointRangeStrings( List list )
{
    avoidUnitCostPointRangeStrings = list;
}

public void setAvoidYears( List list )
{
    avoidYears = list;
}

public void setPreferColors( List list )
{
    preferColors = list;
}

public void setPreferTrims( List list )
{
    preferTrims = list;
}

public void setPreferYears( List list )
{
    preferYears = list;
}

public Integer getGroupingDescriptionId()
{
    return groupingDescriptionId;
}

public void setGroupingDescriptionId( Integer integer )
{
    groupingDescriptionId = integer;
}

public Integer getCiaGroupingItemId()
{
    return ciaGroupingItemId;
}

public void setCiaGroupingItemId( Integer integer )
{
    ciaGroupingItemId = integer;
}

public List getPreferUnitCostPointRangeStrings()
{
	return preferUnitCostPointRangeStrings;
}

public void setPreferUnitCostPointRangeStrings( List preferUnitCostPointRangeStrings )
{
	this.preferUnitCostPointRangeStrings = preferUnitCostPointRangeStrings;
}

}
