package com.firstlook.display.cia;

import java.util.List;

public class CIAOverUnderStockingReportDisplayData
{

private int ciaGroupingItemId;
private int groupingDescriptionId;
private String groupingDescription;
private int totalInStock;
private int totalTargetInventory;
private boolean hasNoUnderstockings;
private int unitsInStock;
private int targetInventory;

private List unitCostPointRanges;

public CIAOverUnderStockingReportDisplayData()
{
}

public int getCiaGroupingItemId()
{
	return ciaGroupingItemId;
}

public String getGroupingDescription()
{
	return groupingDescription;
}

public int getGroupingDescriptionId()
{
	return groupingDescriptionId;
}

public void setCiaGroupingItemId( int i )
{
	ciaGroupingItemId = i;
}

public void setGroupingDescription( String string )
{
	groupingDescription = string;
}

public void setGroupingDescriptionId( int i )
{
	groupingDescriptionId = i;
}

public List getUnitCostPointRanges()
{
	return unitCostPointRanges;
}

public void setUnitCostPointRanges( List list )
{
	unitCostPointRanges = list;
}

public int getTotalTargetInventory()
{
	return totalTargetInventory;
}

public int getTotalInStock()
{
	return totalInStock;
}

public void setTotalTargetInventory( int i )
{
	totalTargetInventory = i;
}

public void setTotalInStock( int i )
{
	totalInStock = i;
}

public boolean isHasNoUnderstockings()
{
	return hasNoUnderstockings;
}

public void setHasNoUnderstockings( boolean b )
{
	hasNoUnderstockings = b;
}

public int getUnitsInStock()
{
	return unitsInStock;
}

public void setUnitsInStock( int i )
{
	unitsInStock = i;
}

public int getTargetInventory()
{
	return targetInventory;
}

public void setTargetInventory( int i )
{
	targetInventory = i;
}

public boolean isModelLevelRecommendation()
{
	if ( unitCostPointRanges == null || unitCostPointRanges.isEmpty() )
	{
		return true;
	}
	else if ( unitCostPointRanges.size() == 1 )
	{
//		CIAUnitCostPointRangeDisplayData modelLevelRange = (CIAUnitCostPointRangeDisplayData)unitCostPointRanges.get( 0 );
//		if ( ( modelLevelRange.getLowerRange() == Integer.MIN_VALUE ) && ( modelLevelRange.getUpperRange() == Integer.MAX_VALUE ) )
//		{
//			return true;
//		}
//		else
//		{
//			return false;
//		}
		return true;
	}
	else
	{
		return false;
	}
}
}
