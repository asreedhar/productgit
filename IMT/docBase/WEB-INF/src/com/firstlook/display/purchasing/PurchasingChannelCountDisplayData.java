package com.firstlook.display.purchasing;

public class PurchasingChannelCountDisplayData
{

private Integer accessGroupId;
private int makeModelMatches;
private int totalMatches;
private String customerFacingDescription;
private Integer accessGroupTypeId;
private Integer publicGroup;

public Integer getAccessGroupId()
{
	return accessGroupId;
}

public void setAccessGroupId( Integer accessGroupId )
{
	this.accessGroupId = accessGroupId;
}

public int getMakeModelMatches()
{
	return makeModelMatches;
}

public void setMakeModelMatches( int potentialMatches )
{
	this.makeModelMatches = potentialMatches;
}

public String getCustomerFacingDescription()
{
	return customerFacingDescription;
}

public void setCustomerFacingDescription( String accessGroupDescription )
{
	this.customerFacingDescription = accessGroupDescription;
}

public int getTotalMatches()
{
	return totalMatches;
}

public void setTotalMatches( int totalMatches )
{
	this.totalMatches = totalMatches;
}

public Integer getAccessGroupTypeId()
{
	return accessGroupTypeId;
}

public void setAccessGroupTypeId( Integer accessGroupTypeId )
{
	this.accessGroupTypeId = accessGroupTypeId;
}

public Integer getPublicGroup()
{
	return publicGroup;
}

public void setPublicGroup( Integer publicGroup )
{
	this.publicGroup = publicGroup;
}
}
