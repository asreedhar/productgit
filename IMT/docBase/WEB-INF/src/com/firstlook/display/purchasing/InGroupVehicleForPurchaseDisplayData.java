package com.firstlook.display.purchasing;

import java.text.DecimalFormat;
import java.util.Date;

import biz.firstlook.commons.util.DateUtilFL;

public class InGroupVehicleForPurchaseDisplayData extends BaseVehicleForPurchaseDisplayData implements IVehicleDisplayData
{

private double unitCost;
private int age;
private String seller;
private int inventoryId;
private Date inventoryReceivedDate;
private int distanceFromDealer;

public InGroupVehicleForPurchaseDisplayData()
{
	super();
}

public int getAge()
{
	long dayDiff = DateUtilFL.calculateNumberOfCalendarDays( this.inventoryReceivedDate, new Date() );

	if ( dayDiff < 1 )
	{
		dayDiff = 1;
	}
	return (int)dayDiff;
}

public double getUnitCost()
{
	return unitCost;
}

public void setAge( int i )
{
	age = i;
}

public void setUnitCost( double d )
{
	unitCost = d;
}

public String getSeller()
{
	return seller;
}

public void setSeller( String seller )
{
	this.seller = seller;
}

public int getInventoryId()
{
	return inventoryId;
}

public void setInventoryId( int vehicleId )
{
	this.inventoryId = vehicleId;
}

public Date getInventoryReceivedDate()
{
	return inventoryReceivedDate;
}

public void setInventoryReceivedDate( Date inventoryReceivedDate )
{
	this.inventoryReceivedDate = inventoryReceivedDate;
}

public String getExpires()
{
	return "N/A";
}

public String getPrice()
{
	String moneyFormat = "$#,###,###,###,##0";
	DecimalFormat decimalFormat = new DecimalFormat( moneyFormat );

	if ( !isDisplayUnitCostToDealerGroup() )
	{
		return "N/A";
	}

	if ( this.unitCost > 0 )
	{
		return decimalFormat.format( this.unitCost );
	}
	else
	{
		return "N/A";
	}
}

public Date getReceivedDate()
{
	return this.inventoryReceivedDate;
}

public String getDecoratedDetailURLWithParams()
{
	StringBuffer completeURL = new StringBuffer();
	completeURL.append( "<a href=\"" );
	completeURL.append( getEncodedDetailURLWithParams() );
	completeURL.append( "\">View</a>" );
	return completeURL.toString();
}

public String getEncodedDetailURLWithParams()
{
	return "javascript:openDetailWindow('InGroupVehicleDetailDisplayAction.go?channelId="
			+ this.channelId + "&vin=" + this.vin + "&vehicleId=" + this.inventoryId + "&weeks=26', '" + this.inventoryId + "')";
}

public int getDistanceFromDealer()
{
	return distanceFromDealer;
}

public void setDistanceFromDealer( int distance )
{
	distanceFromDealer = distance;
}

public String getLocation()
{
	return seller;
}

public int getBuyNowPriceInt()
{
	if ( !isDisplayUnitCostToDealerGroup() )
	{
		return 0;
	}
	else
	{
		return new Double( this.unitCost ).intValue();
	}
}

}
