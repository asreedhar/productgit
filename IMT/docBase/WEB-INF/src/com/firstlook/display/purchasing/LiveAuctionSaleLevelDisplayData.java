package com.firstlook.display.purchasing;

import java.util.Date;

public class LiveAuctionSaleLevelDisplayData
{

private String title;
private int vehiclesMatched;
private String auctionName;
private String location;
private Date auctionDate;
private double distance;
private double score;
private int saleId;
private int auctionTypeCode;

public int getAuctionTypeCode()
{
	return auctionTypeCode;
}
public void setAuctionTypeCode( int auctionTypeCode )
{
	this.auctionTypeCode = auctionTypeCode;
}
public Date getAuctionDate()
{
	return auctionDate;
}
public void setAuctionDate( Date auctionDate )
{
	this.auctionDate = auctionDate;
}
public String getAuctionName()
{
	return auctionName;
}
public void setAuctionName( String auctionName )
{
	this.auctionName = auctionName;
}
public double getDistance()
{
	return distance;
}
public void setDistance( double distance )
{
	this.distance = distance;
}
public String getLocation()
{
	return location;
}
public void setLocation( String location )
{
	this.location = location;
}
public int getSaleId()
{
	return saleId;
}
public void setSaleId( int saleId )
{
	this.saleId = saleId;
}
public double getScore()
{
	return score;
}
public void setScore( double score )
{
	this.score = score;
}
public String getTitle()
{
	return title;
}
public void setTitle( String title )
{
	this.title = title;
}
public int getVehiclesMatched()
{
	return vehiclesMatched;
}
public void setVehiclesMatched( int vehiclesMatched )
{
	this.vehiclesMatched = vehiclesMatched;
}

}
