package com.firstlook.display.purchasing;

import java.text.DecimalFormat;

public class BaseVehicleForPurchaseDisplayData
{

private static DecimalFormat ROI_FORMAT = new DecimalFormat( "####.#%" );

protected int channelId;
protected int groupingDescriptionId;
protected int ciaGroupingItemId;
protected String year;
protected String groupingDescription;
protected String trim;
protected String color;
protected int mileage;
protected String vin;
protected String make;
protected String model;
protected String series;
protected String bodyStyle;
protected int searchScore;
protected Integer segmentId;
protected Integer ciaCategoryId;
protected boolean displayUnitCostToDealerGroup;
private double annualRoi;

public BaseVehicleForPurchaseDisplayData()
{
	super();
}

public String getColor()
{
	return color;
}

public String getGroupingDescription()
{
	return groupingDescription;
}

public String getVehicleDescription()
{
	String returnStr = this.make + " " + this.model;
	if ( this.trim != null )
	{
		returnStr += " " + this.trim;
	}
	return returnStr;
}

public int getMileage()
{
	return mileage;
}

public String getTrim()
{
	return trim;
}

public String getVin()
{
	return vin;
}

public String getYear()
{
	return year;
}

public void setColor( String string )
{
	color = string;
}

public void setGroupingDescription( String string )
{
	groupingDescription = string;
}

public void setMileage( int i )
{
	mileage = i;
}

public void setTrim( String string )
{
	trim = string;
}

public void setVin( String string )
{
	vin = string;
}

public void setYear( String string )
{
	year = string;
}

public int getChannelId()
{
	return channelId;
}

public void setChannelId( int channelId )
{
	this.channelId = channelId;
}

public int getGroupingDescriptionId()
{
	return groupingDescriptionId;
}

public void setGroupingDescriptionId( int groupingDescriptionId )
{
	this.groupingDescriptionId = groupingDescriptionId;
}

public String getMake()
{
	return make;
}

public void setMake( String make )
{
	this.make = make;
}

public String getModel()
{
	return model;
}

public void setModel( String model )
{
	this.model = model;
}

public String getBodyStyle()
{
	return bodyStyle;
}

public void setBodyStyle( String bodyStyle )
{
	this.bodyStyle = bodyStyle;
}

public String getSeries()
{
	return series;
}

public void setSeries( String series )
{
	this.series = series;
}

public int getSearchScore()
{
	return searchScore;
}

public void setSearchScore( int searchScore )
{
	this.searchScore = searchScore;
}

public Integer getCiaCategoryId()
{
	return ciaCategoryId;
}

public void setCiaCategoryId( Integer ciaCategoryId )
{
	this.ciaCategoryId = ciaCategoryId;
}

public Integer getSegmentId()
{
	return segmentId;
}

public void setSegmentId( Integer segmentId )
{
	this.segmentId = segmentId;
}

public int getCiaGroupingItemId()
{
	return ciaGroupingItemId;
}

public void setCiaGroupingItemId( int ciaGroupingItemId )
{
	this.ciaGroupingItemId = ciaGroupingItemId;
}

public boolean isDisplayUnitCostToDealerGroup()
{
	return displayUnitCostToDealerGroup;
}

public void setDisplayUnitCostToDealerGroup( boolean displayUnitCostToDealerGroup )
{
	this.displayUnitCostToDealerGroup = displayUnitCostToDealerGroup;
}

public double getAnnualRoi()
{
	return annualRoi;
}

public void setAnnualRoi( double annualRoi )
{
	this.annualRoi = annualRoi;
}

public String getAnnualRoiString()
{
	if ( this.annualRoi == Integer.MIN_VALUE )
	{
		return "N/A";
	}
	else
	{
		return ROI_FORMAT.format( this.annualRoi );
	}
}

}
