package com.firstlook.display.purchasing;

import java.util.Date;

public interface IVehicleDisplayData
{
public String getColor();

public String getGroupingDescription();

public String getExpires();

public String getLocation();

public int getMileage();

public String getPrice();

public String getTrim();

public String getVin();

public String getYear();

public int getChannelId();

public int getGroupingDescriptionId();

public String getMake();

public String getModel();

public String getBodyStyle();

public int getBuyNowPriceInt();

public String getSeries();

public int getSearchScore();

public Integer getCiaCategoryId();

public Integer getSegmentId();

public int getCiaGroupingItemId();

public Date getReceivedDate();

public String getEncodedDetailURLWithParams();

public String getDecoratedDetailURLWithParams();

public int getDistanceFromDealer();

public boolean isDisplayUnitCostToDealerGroup();

}