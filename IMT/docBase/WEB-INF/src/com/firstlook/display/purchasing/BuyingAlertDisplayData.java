package com.firstlook.display.purchasing;

import java.util.ArrayList;
import java.util.List;

import biz.firstlook.cia.model.CIACategory;
import biz.firstlook.cia.model.CIAGroupingItem;

public class BuyingAlertDisplayData
{

private List vehicleMatchDetails;
private String groupingDescription;
private int groupingDescriptionId;
private int buyAmount;
private int ciaCategoryId;
private int ciaGroupingItemId;
private int segmentRank;
private int segmentId;
private int numberOfMatches;
private double valuation;
private List ciaDetailLevelDescriptions;

public BuyingAlertDisplayData()
{
	super();
}

public BuyingAlertDisplayData( String groupingDescription, List buyingAlertsChannelDatas, int buyAmount )
{
	if ( buyingAlertsChannelDatas == null )
	{
		buyingAlertsChannelDatas = new ArrayList();
	}
	this.groupingDescription = groupingDescription;
	this.vehicleMatchDetails = buyingAlertsChannelDatas;
	this.numberOfMatches = this.vehicleMatchDetails.size();
	this.buyAmount = buyAmount;
}

public BuyingAlertDisplayData( CIAGroupingItem item )
{
	this.ciaGroupingItemId = item.getCiaGroupingItemId().intValue();
	this.ciaCategoryId = item.getCiaCategory().getCiaCategoryId().intValue();
	this.groupingDescriptionId = item.getGroupingDescription().getGroupingDescriptionId().intValue();
	this.ciaGroupingItemId = item.getCiaGroupingItemId().intValue();
	this.valuation = item.getValuation().doubleValue();
}

public void setVehicleMatchDetails( List buyingAlertsChannelDatas )
{
	this.vehicleMatchDetails = buyingAlertsChannelDatas;
}

public List getVehicleMatchDetails()
{
	return this.vehicleMatchDetails;
}

public int getNotEmpty() // for bean comparator
{
	if ( vehicleMatchDetails != null && vehicleMatchDetails.size() > 0 )
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

public String getGroupingDescription()
{
	return groupingDescription;
}

public void setGroupingDescription( String groupingDescription )
{
	this.groupingDescription = groupingDescription;
}

public int getNumberOfMatches()
{
	return numberOfMatches;
}

public void setNumberOfMatches( int numberOfMatches )
{
	this.numberOfMatches = numberOfMatches;
}

public int getBuyAmount()
{
	return buyAmount;
}

public void setBuyAmount( int lowBuyAmount )
{
	this.buyAmount = lowBuyAmount;
}

public int getCiaCategoryId()
{
	return ciaCategoryId;
}

public void setCiaCategoryId( int ciaCategoryId )
{
	this.ciaCategoryId = ciaCategoryId;
}

public int getSegmentRank()
{
	return segmentRank;
}

public void setSegmentRank( int segmentRank )
{
	this.segmentRank = segmentRank;
}

public int getSegmentId()
{
	return segmentId;
}

public void setSegmentId( int segmentId )
{
	this.segmentId = segmentId;
}

public int getCiaGroupingItemId()
{
	return ciaGroupingItemId;
}

public void setCiaGroupingItemId( int ciaGroupingItemId )
{
	this.ciaGroupingItemId = ciaGroupingItemId;
}

public int getGroupingDescriptionId()
{
	return groupingDescriptionId;
}

public void setGroupingDescriptionId( int groupingDescriptionId )
{
	this.groupingDescriptionId = groupingDescriptionId;
}

public String getMakeModelDropDownDescription()
{
	switch ( this.ciaCategoryId )
	{
		case CIACategory.POWERZONE_CATEGORY:
			return "PZ - " + this.groupingDescription;
		case CIACategory.WINNERS_CATEGORY:
			return "WN - " + this.groupingDescription;
		case CIACategory.GOODBETS_CATEGORY:
			return "GB - " + this.groupingDescription;
		case CIACategory.MARKETPERFORMERS_CATEGORY:
			return "MP - " + this.groupingDescription;
		default:
			return "MC - " + this.groupingDescription;
	}
}

public List getCiaDetailLevelDescriptions()
{
	return ciaDetailLevelDescriptions;
}

public void setCiaDetailLevelDescriptions( List ciaUnitCostPointRanges )
{
	this.ciaDetailLevelDescriptions = ciaUnitCostPointRanges;
}

public double getValuation()
{
	return valuation;
}

public void setValuation( double valuation )
{
	this.valuation = valuation;
}
}
