package com.firstlook.display.purchasing;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.Date;

public class ATCVehicleForPurchaseDisplayData extends BaseVehicleForPurchaseDisplayData implements IVehicleDisplayData
{

private Integer currentBid;

private Integer buyNowPrice;

private String expirationTime;

private String detailURL;

private String imageURL;

private int accessGroupId;

private int accessGroupTypeId;

private int distanceFromDealer;

private String location;

public ATCVehicleForPurchaseDisplayData()
{
	super();
}

public String getDetailURLWithParams()
{
	return "ATCVehicleFramesetDisplayAction.go?url="
			+ detailURL + "&channelId=" + getChannelId() + "&firstLookVin=" + getVin() + "&mileage=" + getMileage() + "&vehicleYear="
			+ getYear() + "&make=" + getMake() + "&model=" + getModel() + "&ciaGroupingItemId=" + getCiaGroupingItemId()
			+ "&groupingDescriptionId=" + getGroupingDescriptionId() + "&groupingDescription=" + getGroupingDescription() + "&location="
			+ getLocation() + "&accessGroupTypeId=" + getAccessGroupTypeId();
}

public String getEncodedDetailURLWithParams()
{
	String[] parms = ( (String)getDetailURLWithParams() ).split( "&firstLookVin=" );
	String url = parms[0];
	String otherParms = "&firstLookVin=" + parms[1];
	return "javascript:openDetailWindow('" + URLEncoder.encode( (String)url ) + otherParms + "', 'ATCVehicleDetail')";
}

public String getDecoratedDetailURLWithParams()
{
	StringBuffer completeURL = new StringBuffer();

	completeURL.append( "<a href=\"" );

	completeURL.append( getEncodedDetailURLWithParams() );

	if ( getAccessGroupTypeId() == 4 )

	{

		completeURL.append( "\">Bid List</a>" );

	}

	else

	{

		completeURL.append( "\">Buy Now</a>" );

	}

	return completeURL.toString();

}

public String getDetailURL()
{
	return detailURL;
}

public String getExpirationTime()
{
	return expirationTime;
}

public Integer getCurrentBid()
{
	return currentBid;
}

public int getCurrentBidInt()
{
	return currentBid.intValue();
}

public String getImageURL()
{
	return imageURL;
}

public void setDetailURL( String string )
{
	try
	{
		detailURL = URLEncoder.encode( (String)string, "US-ASCII" );
	}
	catch ( UnsupportedEncodingException uee )
	{
		uee.printStackTrace( System.err );
		detailURL = string; // better than nothing
	}
}

public void setExpirationTime( String string )
{
	expirationTime = string;
}

public void setCurrentBid( Integer highBid )
{
	this.currentBid = highBid;
}

public void setImageURL( String string )
{
	imageURL = string;
}

public Integer getBuyNowPrice()
{
	return buyNowPrice;
}

public int getBuyNowPriceInt()
{
	return buyNowPrice.intValue();
}

public void setBuyNowPrice( Integer buyNowPrice )
{
	this.buyNowPrice = buyNowPrice;
}

public int getAccessGroupId()
{
	return accessGroupId;
}

public void setAccessGroupId( int accessGroupId )
{
	this.accessGroupId = accessGroupId;
}

public String getExpires()
{
	return this.expirationTime;
}

public String getPrice()
{
	String moneyFormat = "$#,###,###,###,##0";
	DecimalFormat decimalFormat = new DecimalFormat( moneyFormat );

	String buyNowString = "";
	String currentBidString = "";
	if ( this.buyNowPrice != null && this.buyNowPrice.intValue() > 0 )
	{
		buyNowString += decimalFormat.format( this.buyNowPrice );
	}
	else
	{
		buyNowString += "(N/A)";
	}
	if ( this.currentBid != null && this.currentBid.intValue() > 0 )
	{
		currentBidString += decimalFormat.format( this.currentBid );
	}
	else
	{
		currentBidString += "(N/A)";
	}
	return currentBidString + " / " + buyNowString;
}

public Date getReceivedDate()
{
	return new Date();
}

public int getDistanceFromDealer()
{
	return distanceFromDealer;
}

public void setDistanceFromDealer( int distance )
{
	this.distanceFromDealer = distance;
}

public String getLocation()
{
	return location;
}

public void setLocation( String location )
{
	this.location = location;
}

public int getAccessGroupTypeId()
{
	return accessGroupTypeId;
}

public void setAccessGroupTypeId( int accessGroupTypeId )
{
	this.accessGroupTypeId = accessGroupTypeId;
}
}
