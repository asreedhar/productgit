package com.firstlook.display.purchasing;

public class LiveAuctionVehicleLevelDisplayData
{

public static final Integer RESULT_MODE_ALL = new Integer(1);
public static final Integer RESULT_MODE_MATCHES_ONLY = new Integer(2);


private String lotId;
private int runNumber;
private int year;
private String modelTrimBodyStyle;
private String engine;
private String drive;
private String color;
private int mileage;
private boolean potentialMatch;
private String vin;
private String consignor;
private String amsAnnouncements;
private String amsOptions;

public String getColor()
{
	return color;
}
public void setColor( String color )
{
	this.color = color;
}
public String getDrive()
{
	return drive;
}
public void setDrive( String drive )
{
	this.drive = drive;
}
public String getEngine()
{
	return engine;
}
public void setEngine( String engine )
{
	this.engine = engine;
}
public String getLotId()
{
	return lotId;
}
public void setLotId( String lotId )
{
	this.lotId = lotId;
}
public int getMileage()
{
	return mileage;
}
public void setMileage( int mileage )
{
	this.mileage = mileage;
}
public String getModelTrimBodyStyle()
{
	return modelTrimBodyStyle;
}
public void setModelTrimBodyStyle( String modelTrimBodyStyle )
{
	this.modelTrimBodyStyle = modelTrimBodyStyle;
}
public boolean isPotentialMatch()
{
	return potentialMatch;
}
public void setPotentialMatch( boolean potentialMatch )
{
	this.potentialMatch = potentialMatch;
}
public int getRunNumber()
{
	return runNumber;
}
public void setRunNumber( int runNumber )
{
	this.runNumber = runNumber;
}
public String getVin()
{
	return vin;
}
public void setVin( String vin )
{
	this.vin = vin;
}
public int getYear()
{
	return year;
}
public void setYear( int year )
{
	this.year = year;
}
public String getConsignor()
{
	return consignor;
}
public void setConsignor( String consignor )
{
	this.consignor = consignor;
}
public String getAmsAnnouncements()
{
	return amsAnnouncements;
}
public void setAmsAnnouncements( String amsAnnouncements )
{
	this.amsAnnouncements = amsAnnouncements;
}
public String getAmsOptions()
{
	return amsOptions;
}
public void setAmsOptions( String amsOptions )
{
	this.amsOptions = amsOptions;
}

public String getLotAndRunNumber()
{
	return lotId + "-" + runNumber;
}

public int getSortablePotentialMatch()
{
	if( potentialMatch )
	{
		return 1;
	}
	else
	{
		return 2;
	}
}

}
