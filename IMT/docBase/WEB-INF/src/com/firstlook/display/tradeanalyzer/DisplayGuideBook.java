package com.firstlook.display.tradeanalyzer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.struts.util.LabelValueBean;

import biz.firstlook.commons.gson.GsonExclude;
import biz.firstlook.module.vehicle.description.old.KBBConditionEnum;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.module.vehicle.description.old.VDP_NadaOptionAdjustmentsAdapter;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

import com.firstlook.entity.form.ThirdPartyVehicleOptionForm;
import com.firstlook.iterator.ColumnedFormIterator;
import com.firstlook.iterator.TransposedColumnFormIterator;

public class DisplayGuideBook implements Serializable
{

private static final long serialVersionUID = 280874624485647823L;

private boolean validVin;
private boolean success;
private String successMessage;
private Integer guideBookId;
private String guideBookName;
private String imageName;
private String bgColor; // Background color #RRGGBB format. Doing this for HH cuz they usually don't handle CSS.
private String make;
private String year;
@GsonExclude private List<VDP_GuideBookVehicle> displayGuideBookVehicles;
@GsonExclude private List<VDP_GuideBookVehicle> displayGuideBookModels;// added only for the exceptional scenario in kbb when vin deosnt decode to trim.
public Object[] getDisplayGuideBookModels() {
	return displayGuideBookModels==null?new Object[0]:displayGuideBookModels.toArray();
}

public List<VDP_GuideBookVehicle> getListDisplayGuideBookModels() {
	return displayGuideBookModels;
}

public void setDisplayGuideBookModels( List<VDP_GuideBookVehicle> collection )
{
	this.displayGuideBookModels = collection;
}

private String footer;
private String publishInfo;
private Integer mileageAdjustment;
private String[] selectedEquipmentOptionKeys;
private String selectedEngineKey;
private String selectedTransmissionKey;
private String selectedDrivetrainKey;
private String selectedVehicleKey = "";
private String selectedModelKey = "";
private String tradeAnalyzedDate;
private List<ThirdPartyVehicleOption> displayGuideBookOptions;
private List<VDP_GuideBookValue> displayGuideBookValues;
private List<VDP_GuideBookValue> displayGuideBookValuesComplete;
private List<VDP_GuideBookValue> displayGuideBookValuesNoMileage;
private boolean inAccurate = false;
private Date lastBookedOutDate;
public String getSelectedModelKey() {
	return selectedModelKey;
}

public void setSelectedModelKey(String selectedModelKey) {
	if(selectedModelKey!=null)
		this.selectedModelKey = selectedModelKey;
}

private boolean storedInfo;
private String region;
private Integer msrp;
private Integer weight;
private String model;
private String kelleyPublishMonth;
private String kelleyPublishState;
private Boolean hasErrors;
private Collection<ThirdPartyVehicleOption> selectedOptions;

// kelley specific fields
private VDP_GuideBookValue kelleyWholesaleBaseValue;
private VDP_GuideBookValue kelleyRetailBaseValue;
private VDP_GuideBookValue kelleyTradeInBaseValue;
private VDP_GuideBookValue kelleyWholesaleValueNoMileage;
private VDP_GuideBookValue kelleyRetailValueNoMileage;
private VDP_GuideBookValue kelleyTradeInValueNoMileage;
private VDP_GuideBookValue kelleyTradeInRangeLowValueNoMileage;
private VDP_GuideBookValue kelleyTradeInRangeHighValueNoMileage;
private VDP_GuideBookValue kelleyRetailValue;
private VDP_GuideBookValue kelleyWholesaleValue;
private VDP_GuideBookValue kelleyTradeInValue;
private VDP_GuideBookValue kelleyTradeInRangeLowValue;
private VDP_GuideBookValue kelleyTradeInRangeHighValue;
private VDP_GuideBookValue kelleyOptionsAdjustment;
private KBBConditionEnum condition;


private VDP_GuideBookValue guideBookOptionAdjustment;


private ArrayList<LabelValueBean> wirelessMessages = new ArrayList<LabelValueBean>();

public DisplayGuideBook()
{
	displayGuideBookVehicles = new ArrayList<VDP_GuideBookVehicle>();
	displayGuideBookModels = new ArrayList<VDP_GuideBookVehicle>();
	displayGuideBookOptions = new ArrayList<ThirdPartyVehicleOption>();
	displayGuideBookValues = new ArrayList<VDP_GuideBookValue>();
	hasErrors = Boolean.FALSE;
}

public Integer getGuideBookId()
{
	return guideBookId;
}

public String getGuideBookName()
{
	return guideBookName;
}

public Object[] getDisplayGuideBookVehicles()
{
	return displayGuideBookVehicles.toArray();
}


public List<VDP_GuideBookVehicle> getListOfDisplayGuideBookVehicles()
{
	return displayGuideBookVehicles;
}




public List<VDP_GuideBookVehicle> getDisplayGuideBookVehiclesCollection()
{
	return displayGuideBookVehicles;
}

public int getDisplayGuideBookVehiclesSize()
{
	if ( displayGuideBookVehicles != null )
	{
		return displayGuideBookVehicles.size();
	}
	return 0;
}

public void setGuideBookId( Integer i )
{
	guideBookId = i;
}

public void setGuideBookName( String string )
{
	guideBookName = string;
}

public String getSelectedVehicleKey()
{
	return selectedVehicleKey;
}

public void setSelectedVehicleKey( String selectedVehicleKey )
{
	this.selectedVehicleKey = selectedVehicleKey;
}

public void setDisplayGuideBookVehicles( List<VDP_GuideBookVehicle> collection )
{
	displayGuideBookVehicles = collection;
}

public String getMake()
{
	return make;
}

public String getFormattedMake()
{
	String formattedMake = make.toLowerCase();
	formattedMake = make.substring( 0, 1 ).toUpperCase() + formattedMake.substring( 1 );
	return formattedMake;
}

public void setMake( String string )
{
	make = string;
}

public String getFooter()
{
	return footer;
}

public void setFooter( String footer )
{
	this.footer = footer;
}

public String getSelectedDrivetrainKey()
{
	return selectedDrivetrainKey;
}

public void setSelectedDrivetrainKey( String selectedDrivetrainKey )
{
	this.selectedDrivetrainKey = selectedDrivetrainKey;
}

public String getSelectedEngineKey()
{
	return selectedEngineKey;
}

public void setSelectedEngineKey( String selectedEngineKey )
{
	this.selectedEngineKey = selectedEngineKey;
}

public String[] getSelectedEquipmentOptionKeys()
{
	return selectedEquipmentOptionKeys;
}

public void setSelectedEquipmentOptionKeys( String[] selectedOptionKeys )
{
	this.selectedEquipmentOptionKeys = selectedOptionKeys;
}

public String getSelectedTransmissionKey()
{
	return selectedTransmissionKey;
}

public void setSelectedTransmissionKey( String selectedTransmissionKey )
{
	this.selectedTransmissionKey = selectedTransmissionKey;
}

public int getDisplayThirdPartyVehiclesSize()
{
	return this.displayGuideBookVehicles.size();
}

public String getTradeAnalyzedDate()
{
	return tradeAnalyzedDate;
}

public void setTradeAnalyzedDate( String tradeAnalyzedDate )
{
	this.tradeAnalyzedDate = tradeAnalyzedDate;
}

public ColumnedFormIterator getDisplayEquipmentGuideBookOptionsInColumns()
{
	return getOptionsInColumns( getDisplayEquipmentGuideBookOptionForms() );
}

private ColumnedFormIterator getOptionsInColumns( List<?> options )
{
	return ( new ColumnedFormIterator( new TransposedColumnFormIterator( 3, options, null ), null ) );
}

public List<ThirdPartyVehicleOption> getDisplayGuideBookOptions()
{
	return displayGuideBookOptions;
}

public void setDisplayGuideBookOptions( List<ThirdPartyVehicleOption> displayGuideBookOptions )
{
	this.displayGuideBookOptions = displayGuideBookOptions;
}

public List<VDP_GuideBookValue> getDisplayGuideBookValues()
{
	return displayGuideBookValues;
}

public void setDisplayGuideBookValues( List<VDP_GuideBookValue> displayGuideBookValues )
{
	this.displayGuideBookValues = displayGuideBookValues;
}

public List<VDP_GuideBookValue> getDisplayGuideBookValuesComplete()
{
	return displayGuideBookValuesComplete;
}

public void setDisplayGuideBookValuesComplete( List<VDP_GuideBookValue> displayGuideBookValuesComplete )
{
	this.displayGuideBookValuesComplete = displayGuideBookValuesComplete;
}

public VDP_GuideBookVehicle determineSelectedVehicle( String selectedVehicleKey )
{
	VDP_GuideBookVehicle selectedVehicle = null;
	if ( this.displayGuideBookVehicles != null ) {
		Iterator<VDP_GuideBookVehicle> vehicleIter = this.displayGuideBookVehicles.iterator();
		while ( vehicleIter.hasNext() ) {
			VDP_GuideBookVehicle vehicle = vehicleIter.next();
			if ( vehicle.getKey().equals( selectedVehicleKey ) ) {
				selectedVehicle = vehicle;
				break;
			}
		}
	}

	return selectedVehicle;
}

public boolean isValidVin()
{
	return validVin;
}

public void setValidVin( boolean validVin )
{
	this.validVin = validVin;
}

public List<ThirdPartyVehicleOption> getDisplayEquipmentGuideBookOptions()
{
	return filterOptions( displayGuideBookOptions, ThirdPartyOptionType.EQUIPMENT );
}

public List<ThirdPartyVehicleOptionForm> getDisplayEquipmentGuideBookOptionForms()
{
	List<ThirdPartyVehicleOption> returnList = filterOptions( displayGuideBookOptions, ThirdPartyOptionType.EQUIPMENT );
	return transformOptionsIntoForms( returnList );
}

public List<ThirdPartyVehicleOption> getDisplayTransmissionGuideBookOptions()
{
	return filterOptions( displayGuideBookOptions, ThirdPartyOptionType.TRANSMISSION );
}

public List<ThirdPartyVehicleOptionForm> getDisplayTransmissionGuideBookOptionForms()
{
	List<ThirdPartyVehicleOption> returnList = filterOptions( displayGuideBookOptions, ThirdPartyOptionType.TRANSMISSION );
	return transformOptionsIntoForms( returnList );
}

public List<ThirdPartyVehicleOption> getDisplayEngineGuideBookOptions()
{
	return filterOptions( displayGuideBookOptions, ThirdPartyOptionType.ENGINE );
}

public List<ThirdPartyVehicleOptionForm> getDisplayEngineGuideBookOptionForms()
{
	List<ThirdPartyVehicleOption> returnList = filterOptions( displayGuideBookOptions, ThirdPartyOptionType.ENGINE );
	return transformOptionsIntoForms( returnList );
}

public List<ThirdPartyVehicleOption> getDisplayDrivetrainGuideBookOptions()
{
	return filterOptions( displayGuideBookOptions, ThirdPartyOptionType.DRIVETRAIN );
}

public List<ThirdPartyVehicleOptionForm> getDisplayDrivetrainGuideBookOptionForms()
{
	List<ThirdPartyVehicleOption> returnList = filterOptions( displayGuideBookOptions, ThirdPartyOptionType.DRIVETRAIN );
	return transformOptionsIntoForms( returnList );
}

public Integer getEquipmentOptionsLength()
{
	return getDisplayEquipmentGuideBookOptions().size();
}

public String getYear()
{
	return year;
}

public void setYear( String year )
{
	this.year = year;
}

public boolean getIsKelley()
{
	if ( guideBookId.intValue() == ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE)
	{
		return true;
	}
	else
	{
		return false;
	}
}

public boolean getIsNADA()
{
	if ( guideBookId.intValue() == ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE)
	{
		return true;
	}
	else
	{
		return false;
	}
}

public Integer getMileageAdjustment()
{
	return mileageAdjustment;
}

public void setMileageAdjustment( Integer mileageAdjustment )
{
	this.mileageAdjustment = mileageAdjustment;
}

public String getPublishInfo()
{
	if ( publishInfo != null )
	{
		return publishInfo;
	}
	else
	{
		return "";
	}
}

public void setPublishInfo( String publishInfo )
{
	this.publishInfo = publishInfo;
}

public List<VDP_GuideBookValue> getDisplayGuideBookValuesNoMileage()
{
	if ( displayGuideBookValuesNoMileage != null )
	{
		return displayGuideBookValuesNoMileage;
	}
	else
	{
		return new ArrayList<VDP_GuideBookValue>();
	}
}

public void setDisplayGuideBookValuesNoMileage( List<VDP_GuideBookValue> displayGuideBookValuesNoMileage )
{
	this.displayGuideBookValuesNoMileage = displayGuideBookValuesNoMileage;
}

public String getImageName()
{
	return imageName;
}

public void setImageName( String imageName )
{
	this.imageName = imageName;
}

public Date getLastBookedOutDate()
{
	return lastBookedOutDate;
}

public void setLastBookedOutDate( Date lastBookedOutDate )
{
	this.lastBookedOutDate = lastBookedOutDate;
}

private List<ThirdPartyVehicleOption> filterOptions( Collection<ThirdPartyVehicleOption> options, ThirdPartyOptionType optionType )
{
	ArrayList<ThirdPartyVehicleOption> matches = new ArrayList<ThirdPartyVehicleOption>();
	if(options != null) {
		for ( ThirdPartyVehicleOption option : options ){
			if ( option != null ) {
				if ( option.getOption().getThirdPartyOptionType().equals(optionType)) {
					matches.add( option );
				}
			}
		}
	}
	//DE1101 - only sort if the option type is Transmission type - this implies the book is kbb.
	if( optionType.equals(ThirdPartyOptionType.TRANSMISSION) ) {
		Collections.sort( matches, new Comparator<ThirdPartyVehicleOption>(){
			public int compare( ThirdPartyVehicleOption o1, ThirdPartyVehicleOption o2 ) {
				return o1.getOptionName().compareTo( o2.getOptionName() );
		}
		});
	}
	return matches;
}

private List<ThirdPartyVehicleOptionForm> transformOptionsIntoForms( List<ThirdPartyVehicleOption> guideBookOptions )
{
	List<ThirdPartyVehicleOptionForm> returnForms = new ArrayList<ThirdPartyVehicleOptionForm>();
	Iterator<ThirdPartyVehicleOption> optionsIter = guideBookOptions.iterator();
	ThirdPartyVehicleOption option;
	ThirdPartyVehicleOptionForm optionForm;
	while ( optionsIter.hasNext() )
	{
		option = optionsIter.next();
		if ( option != null )
		{
			optionForm = new ThirdPartyVehicleOptionForm( option );
			returnForms.add( optionForm );
		}
	}
	return returnForms;

}

public boolean isStoredInfo()
{
	return storedInfo;
}

public void setStoredInfo( boolean storedInfo )
{
	this.storedInfo = storedInfo;
}

public VDP_GuideBookValue getKelleyRetailBaseValue()
{
	return kelleyRetailBaseValue;
}

public void setKelleyRetailBaseValue( VDP_GuideBookValue kelleyRetailBaseValue )
{
	this.kelleyRetailBaseValue = kelleyRetailBaseValue;
}

public VDP_GuideBookValue getKelleyWholesaleBaseValue()
{
	return kelleyWholesaleBaseValue;
}

public void setKelleyWholesaleBaseValue( VDP_GuideBookValue kelleyWholesaleBaseValue )
{
	this.kelleyWholesaleBaseValue = kelleyWholesaleBaseValue;
}

public VDP_GuideBookValue getKelleyRetailValueNoMileage()
{
	return kelleyRetailValueNoMileage;
}

public void setKelleyRetailValueNoMileage( VDP_GuideBookValue kelleyRetailValueNoMileage )
{
	this.kelleyRetailValueNoMileage = kelleyRetailValueNoMileage;
}

public VDP_GuideBookValue getKelleyWholesaleValueNoMileage()
{
	return kelleyWholesaleValueNoMileage;
}

public void setKelleyWholesaleValueNoMileage( VDP_GuideBookValue kelleyWholesaleValueNoMileage )
{
	this.kelleyWholesaleValueNoMileage = kelleyWholesaleValueNoMileage;
}

public VDP_GuideBookValue getKelleyRetailValue()
{
	return kelleyRetailValue;
}

public void setKelleyRetailValue( VDP_GuideBookValue kelleyRetailValue )
{
	this.kelleyRetailValue = kelleyRetailValue;
}

public VDP_GuideBookValue getKelleyWholesaleValue()
{
	return kelleyWholesaleValue;
}

public void setKelleyWholesaleValue( VDP_GuideBookValue kelleyWholesaleValue )
{
	this.kelleyWholesaleValue = kelleyWholesaleValue;
}

public String getRegion()
{
	return region;
}

public void setRegion( String region )
{
	this.region = region;
}

public Integer getMsrp()
{
	return msrp;
}

public void setMsrp( Integer msrp )
{
	this.msrp = msrp;
}

public Integer getWeight()
{
	return weight;
}

public void setWeight( Integer weight )
{
	this.weight = weight;
}

public int getGuideBookOptionsTotal()
{
	if(guideBookOptionAdjustment != null) {
		return (guideBookOptionAdjustment.getValue() == null) ? 0 : guideBookOptionAdjustment.getValue().intValue();
	}
	
	Iterator<ThirdPartyVehicleOption> optionsIter = determineSelectedOptions().iterator();
	int totalOptionValue = 0;
	while ( optionsIter.hasNext() )	{
		ThirdPartyVehicleOption option = optionsIter.next();
		if ( option != null && (option.getValue() != Integer.MIN_VALUE ) ) {
			totalOptionValue += option.getValue();
		}
	}
	return totalOptionValue;
}

/**
 * Set the option adjustment value, otherwise, the call to getGuideBookOptionsTotal will sum the option values.
 * @param optionsAdjustment
 */
public void setGuideBookOptionAdjustment(VDP_GuideBookValue optionsAdjustment) {
	this.guideBookOptionAdjustment = optionsAdjustment;
}

public Collection<VDP_GuideBookValue> getNadaOptionAdjustmentTotals() {
	if(guideBookId.intValue() == ThirdPartyDataProvider.NADA.getThirdPartyId().intValue()
			&& guideBookOptionAdjustment instanceof VDP_NadaOptionAdjustmentsAdapter) {
		VDP_NadaOptionAdjustmentsAdapter adapter = (VDP_NadaOptionAdjustmentsAdapter)guideBookOptionAdjustment;
		return adapter.getOptionAdjustments();
	}
	return null;
}

public List<ThirdPartyVehicleOption> determineSelectedOptions()
{
	List<ThirdPartyVehicleOption> selectedOptions = new ArrayList<ThirdPartyVehicleOption>();
	Iterator<ThirdPartyVehicleOption> optionIter = getDisplayEquipmentGuideBookOptions().iterator();
	while ( optionIter.hasNext() ) {
		ThirdPartyVehicleOption option = optionIter.next();
		if ( option != null && option.isStatus() ) {
			selectedOptions.add( option );
		}
	}
	return selectedOptions;
}

public List<ThirdPartyVehicleOptionForm> determineKelleyPrintableOptions()
{
	ArrayList<ThirdPartyVehicleOptionForm> selectedOptions = new ArrayList<ThirdPartyVehicleOptionForm>();
	Iterator<ThirdPartyVehicleOptionForm> optionIter = getDisplayEquipmentGuideBookOptionForms().iterator();
	ThirdPartyVehicleOptionForm option;
	while ( optionIter.hasNext() )
	{
		option = optionIter.next();
		if ( option.isSelected() || option.isStandardOption() )
		{
			selectedOptions.add( option );
		}
	}
	return selectedOptions;
}

public String getModel()
{
	return model;
}

public void setModel( String model )
{
	this.model = model;
}

public String getKelleyPublishRange()
{
	if ( getPublishInfo() != null && !getPublishInfo().equals( "" ) )
	{
		return getPublishInfo().substring( 4, getPublishInfo().length() );
	}
	else
	{
		return "";
	}
}

public String getKelleyPublishState()
{
	return kelleyPublishState;
}

public void setKelleyPublishState( String kelleyPublishState )
{
	this.kelleyPublishState = kelleyPublishState;
}

public String toString()
{
	return guideBookName + " values for " + year + " " + make + " " + model;
}

public String getKelleyPublishMonth()
{
	return kelleyPublishMonth;
}

public void setKelleyPublishMonth( String kelleyPublishMonth )
{
	this.kelleyPublishMonth = kelleyPublishMonth;
}

public void setSuccess( boolean success )
{
	this.success = success;
}

public boolean isSuccess()
{
	return success;
}

public String getSuccessMessage()
{
	return successMessage;
}

public void setSuccessMessage( String successMessage )
{
	this.successMessage = successMessage;
}

public String getBgColor()
{
	return bgColor;
}

public void setBgColor( String bgColor )
{
	this.bgColor = bgColor;
}

public Boolean getHasErrors()
{
	return hasErrors;
}

public void setHasErrors( Boolean hasErrors )
{
	this.hasErrors = hasErrors;
}

public Collection<ThirdPartyVehicleOption> getSelectedOptions()
{
	return selectedOptions;
}

public void setSelectedOptions( Collection<ThirdPartyVehicleOption> selectedOptions )
{
	this.selectedOptions = selectedOptions;
}

public ArrayList<LabelValueBean> getWirelessMessages() {
	return wirelessMessages;
}

public void setWirelessMessages(ArrayList<LabelValueBean> wirelessMessages) {
	this.wirelessMessages = wirelessMessages;
}

public VDP_GuideBookValue getKelleyOptionsAdjustment()
{
	return kelleyOptionsAdjustment;
}

public void setKelleyOptionsAdjustment( VDP_GuideBookValue kelleyOptionsAdjustment )
{
	this.kelleyOptionsAdjustment = kelleyOptionsAdjustment;
}

public VDP_GuideBookValue getKelleyTradeInBaseValue()
{
	return kelleyTradeInBaseValue;
}

public void setKelleyTradeInBaseValue( VDP_GuideBookValue kelleyTradeInBaseValue )
{
	this.kelleyTradeInBaseValue = kelleyTradeInBaseValue;
}

public VDP_GuideBookValue getKelleyTradeInValue()
{
	return kelleyTradeInValue;
}

public void setKelleyTradeInValue( VDP_GuideBookValue kelleyTradeInValue )
{
	this.kelleyTradeInValue = kelleyTradeInValue;
}

public VDP_GuideBookValue getKelleyTradeInRangeLowValue() {
	return kelleyTradeInRangeLowValue;
}

public void setKelleyTradeInRangeLowValue(
		VDP_GuideBookValue kelleyTradeInRangeLowValue) {
	this.kelleyTradeInRangeLowValue = kelleyTradeInRangeLowValue;
}

public VDP_GuideBookValue getKelleyTradeInRangeHighValue() {
	return kelleyTradeInRangeHighValue;
}

public void setKelleyTradeInRangeHighValue(
		VDP_GuideBookValue kelleyTradeInRangeHighValue) {
	this.kelleyTradeInRangeHighValue = kelleyTradeInRangeHighValue;
}

public VDP_GuideBookValue getKelleyTradeInValueNoMileage()
{
	return kelleyTradeInValueNoMileage;
}

public void setKelleyTradeInValueNoMileage( VDP_GuideBookValue kelleyTradeInValueNoMileage )
{
	this.kelleyTradeInValueNoMileage = kelleyTradeInValueNoMileage;
}

public VDP_GuideBookValue getKelleyTradeInRangeLowValueNoMileage() {
	return kelleyTradeInRangeLowValueNoMileage;
}

public void setKelleyTradeInRangeLowValueNoMileage(
		VDP_GuideBookValue kelleyTradeInRangeLowValueNoMileage) {
	this.kelleyTradeInRangeLowValueNoMileage = kelleyTradeInRangeLowValueNoMileage;
}

public VDP_GuideBookValue getKelleyTradeInRangeHighValueNoMileage() {
	return kelleyTradeInRangeHighValueNoMileage;
}

public void setKelleyTradeInRangeHighValueNoMileage(
		VDP_GuideBookValue kelleyTradeInRangeHighValueNoMileage) {
	this.kelleyTradeInRangeHighValueNoMileage = kelleyTradeInRangeHighValueNoMileage;
}

public KBBConditionEnum getCondition()
{
	return condition;
}

public void setCondition( KBBConditionEnum condition )
{
	this.condition = condition;
}

public int getConditionId()
{
	if ( condition != null )
	{
		return condition.getId();
	}
	return 1;
}

public void setConditionId( int conditionId )
{
	this.condition = KBBConditionEnum.getKBBConditionEnumById( conditionId );
}

public boolean isInAccurate() {
	return inAccurate;
}

public void setInAccurate(boolean inAccurate) {
	this.inAccurate = inAccurate;
}

}
