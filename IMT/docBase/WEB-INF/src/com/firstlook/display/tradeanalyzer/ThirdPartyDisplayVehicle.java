package com.firstlook.display.tradeanalyzer;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import biz.firstlook.transact.persist.model.ThirdPartyOption;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;
import biz.firstlook.transact.persist.model.ThirdPartyVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

import com.firstlook.entity.form.ThirdPartyVehicleOptionForm;
import com.firstlook.iterator.ColumnedFormIterator;
import com.firstlook.iterator.TransposedColumnFormIterator;

public class ThirdPartyDisplayVehicle
{
private ThirdPartyVehicle vehicle;
private Set<ThirdPartyVehicleOption> thirdPartyVehicleEquipmentOptions;
private Set<ThirdPartyVehicleOption> thirdPartyVehicleDrivetrainOptions;
private Set<ThirdPartyVehicleOption> thirdPartyVehicleTransmissionOptions;
private Set<ThirdPartyVehicleOption> thirdPartyVehicleEngineOptions;

public ThirdPartyDisplayVehicle( ThirdPartyVehicle vehicle )
{
    this.vehicle = vehicle;
    this.thirdPartyVehicleDrivetrainOptions = new HashSet<ThirdPartyVehicleOption>();
    this.thirdPartyVehicleTransmissionOptions = new HashSet<ThirdPartyVehicleOption>();
    this.thirdPartyVehicleEquipmentOptions = new HashSet<ThirdPartyVehicleOption>();
    this.thirdPartyVehicleEngineOptions = new HashSet<ThirdPartyVehicleOption>();
    splitOptions(vehicle.getThirdPartyVehicleOptions());
}

private void splitOptions( Collection<ThirdPartyVehicleOption> options )
{
    if ( options == null ) {
        return;
    }
    Iterator<ThirdPartyVehicleOption> optionsIter = options.iterator();
    while (optionsIter.hasNext())
    {
        ThirdPartyVehicleOption option = optionsIter.next();
        final ThirdPartyOptionType optionType = option.getOption().getThirdPartyOptionType();
        if(ThirdPartyOptionType.EQUIPMENT.equals(optionType)) {
        	thirdPartyVehicleEquipmentOptions.add(option);	
        } else if (ThirdPartyOptionType.DRIVETRAIN.equals(optionType)) {
        	thirdPartyVehicleDrivetrainOptions.add(option);
        } else if (ThirdPartyOptionType.TRANSMISSION.equals(optionType)) {
        	thirdPartyVehicleTransmissionOptions.add(option);
        } else if (ThirdPartyOptionType.ENGINE.equals(optionType)) {
        	thirdPartyVehicleEngineOptions.add(option);
        } else {
        	//blow up and cry?
        }
    }
}

public String getThirdPartyVehicleCode()
{
    return vehicle.getThirdPartyVehicleCode();
}

public ColumnedFormIterator getEquipmentOptionsForDisplay()
{
    return (new ColumnedFormIterator(new TransposedColumnFormIterator(3,
            getThirdPartyVehicleEquipmentOptions(),
            ThirdPartyVehicleOptionForm.class),
            ThirdPartyVehicleOptionForm.class));
}

public ColumnedFormIterator getDrivetrainOptionsForDisplay()
{
    return (new ColumnedFormIterator(new TransposedColumnFormIterator(3,
            getThirdPartyVehicleDrivetrainOptions(),
            ThirdPartyVehicleOptionForm.class),
            ThirdPartyVehicleOptionForm.class));
}

public ColumnedFormIterator getTransmissionOptionsForDisplay()
{
    return (new ColumnedFormIterator(new TransposedColumnFormIterator(3,
            getThirdPartyVehicleTransmissionOptions(),
            ThirdPartyVehicleOptionForm.class),
            ThirdPartyVehicleOptionForm.class));
}

public ColumnedFormIterator getEngineOptionsForDisplay()
{
    return (new ColumnedFormIterator(new TransposedColumnFormIterator(3,
            getThirdPartyVehicleEngineOptions(),
            ThirdPartyVehicleOptionForm.class),
            ThirdPartyVehicleOptionForm.class));
}

public String getSelectedKeyDescription()
{
    return vehicle.getDescription();
}

public Set<ThirdPartyVehicleOption> getThirdPartyVehicleDrivetrainOptions()
{
    return thirdPartyVehicleDrivetrainOptions;
}

public Set<ThirdPartyVehicleOption> getThirdPartyVehicleEngineOptions()
{
    return thirdPartyVehicleEngineOptions;
}

public Set<ThirdPartyVehicleOption> getThirdPartyVehicleEquipmentOptions()
{
    return thirdPartyVehicleEquipmentOptions;
}

public Set<ThirdPartyVehicleOption> getThirdPartyVehicleTransmissionOptions()
{
    return thirdPartyVehicleTransmissionOptions;
}

public void setThirdPartyVehicleDrivetrainOptions( Set<ThirdPartyVehicleOption> set )
{
    thirdPartyVehicleDrivetrainOptions = set;
}

public void setThirdPartyVehicleEngineOptions( Set<ThirdPartyVehicleOption> set )
{
    thirdPartyVehicleEngineOptions = set;
}

public void setThirdPartyVehicleEquipmentOptions( Set<ThirdPartyVehicleOption> set )
{
    thirdPartyVehicleEquipmentOptions = set;
}

public void setThirdPartyVehicleTransmissionOptions( Set<ThirdPartyVehicleOption> set )
{
    thirdPartyVehicleTransmissionOptions = set;
}

public ThirdPartyVehicle getVehicle()
{
    return vehicle;
}

public void setVehicle( ThirdPartyVehicle vehicle )
{
    this.vehicle = vehicle;
}

public int getEquipmentOptionsSize()
{
    if ( thirdPartyVehicleEquipmentOptions != null )
    {
        return thirdPartyVehicleEquipmentOptions.size();
    }
    return 0;
}

public int getTransmissionOptionsSize()
{
    if ( thirdPartyVehicleTransmissionOptions != null )
    {
        return thirdPartyVehicleTransmissionOptions.size();
    }
    return 0;
}

public int getEngineOptionsSize()
{
    if ( thirdPartyVehicleEngineOptions != null )
    {
        return thirdPartyVehicleEngineOptions.size();
    }
    return 0;
}

public int getDrivetrainOptionsSize()
{
    if ( thirdPartyVehicleDrivetrainOptions != null )
    {
        return thirdPartyVehicleDrivetrainOptions.size();
    }
    return 0;
}

public boolean isSelected()
{
    return vehicle.isStatus();
}

public void setSelected( boolean selected )
{
    vehicle.setStatus(selected);
}

}
