package com.firstlook.display.util;

import java.text.DecimalFormat;

import org.displaytag.decorator.ColumnDecorator;
import org.displaytag.exception.DecoratorException;

public class PercentDecorator implements ColumnDecorator
{

public static String PERCENT_FORMAT = "#,###,###,###,##0%";

public String decorate( Object object ) throws DecoratorException
{
    try
    {
        if ( object instanceof String )
        {
            Number number = new Double((String) object);
            object = number;
        }
    } catch (NumberFormatException nfe)
    {
        return "";
    }

    if ( object instanceof Number )
    {
        Number number = (Number) object;
        DecimalFormat decimalFormat = new DecimalFormat(PERCENT_FORMAT);
        return decimalFormat.format(number.doubleValue());
    } else
    {
        throw new DecoratorException(PercentDecorator.class,
                "PercentDecorator expects a Number object.");
    }
}

}
