package com.firstlook.display.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.displaytag.decorator.ColumnDecorator;
import org.displaytag.exception.DecoratorException;

public class ShortDateDecorator implements ColumnDecorator
{

public static String DATE_FORMAT = "MM/dd/yyyy";

public String decorate( Object object ) throws DecoratorException
{
    if ( object instanceof Date )
    {
        Date date = (Date) object;
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        return dateFormat.format(date);
    } else
    {
        throw new DecoratorException(Date.class,
                "ShortDateDecorator expects a Date object.");
    }
}

}
