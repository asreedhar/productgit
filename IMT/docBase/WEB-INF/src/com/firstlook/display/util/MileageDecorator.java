package com.firstlook.display.util;

import java.text.DecimalFormat;

import org.displaytag.decorator.ColumnDecorator;
import org.displaytag.exception.DecoratorException;

public class MileageDecorator implements ColumnDecorator
{
public static String MILEAGE_FORMAT = "#,###,###,###,##0";

public String decorate( Object object ) throws DecoratorException
{
	if ( object == null )
	{
		return "0";
	}
	else
	{
		try
		{
			if ( object instanceof String )
			{
				Number number = new Integer( (String)object );
				object = number;
			}
		}
		catch ( NumberFormatException nfe )
		{
			return "0";
		}

		if ( object instanceof Number )
		{
			Number number = (Number)object;
			if ( number.intValue() > 0 )
			{
				DecimalFormat decimalFormat = new DecimalFormat( MILEAGE_FORMAT );
				return decimalFormat.format( number.intValue() );
			}
			else
			{
				return "N/A";
			}
		}
		else
		{
			throw new RuntimeException( "MileageDecorator expects a Number object." );
		}
	}
}

}
