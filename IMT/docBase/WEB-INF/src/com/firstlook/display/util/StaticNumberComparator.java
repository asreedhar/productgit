package com.firstlook.display.util;

import java.util.Comparator;

import org.displaytag.model.Cell;

/**
 * This whole class is just silly. Remove when we have time, low impact - BF.
 */
public class StaticNumberComparator implements Comparator
{

public int compare( Object arg0, Object arg1 )
{
    Cell cell0 = (Cell)arg0;
    Cell cell1 = (Cell)arg1;

    String static0 = cell0.getStaticValue().toString();
    String static1 = cell1.getStaticValue().toString();

    if ( static0.trim().equals( "" ) && static0.trim().equals( "" ) )
    {
        return 0;
    }
    else if ( static0.trim().equals( "" ) )
    {
        return 1;
    }
    else if ( static1.trim().equals( "" ) )
    {
        return -1;
    }

    Double staticNumber0 = null;
    Double staticNumber1 = null;
    try
    {
        staticNumber0 = new Double( Double.parseDouble( static0 ) );
        staticNumber1 = new Double( Double.parseDouble( static1 ) );
    }
    catch ( NumberFormatException nfe )
    {
        return 0;
    }

    return staticNumber0.compareTo( staticNumber1 );
}

}
