package com.firstlook.promotion;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import biz.firstlook.commons.util.PropertyLoader;

public class PromotionFilter implements Filter 
{
	private static final String _promotionRedirect = PropertyLoader.getProperty("firstlook.promotions.uri");
	
	public void destroy() 
	{
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain filterChain) throws IOException, ServletException
	{
		HttpServletRequest httpServletRequest = (HttpServletRequest)request; 
		Cookie[] cookies = httpServletRequest.getCookies();
        boolean foundCookie = false;

        for(int i = 0; i < cookies.length; i++) { 
            Cookie cookie = cookies[i];
            if (cookie.getName().equals("promotion")) {
                foundCookie = true;
            }
        }  

        if (!foundCookie) {
            Cookie cookie = new Cookie("promotion", "");
            cookie.setMaxAge(-1);
    		HttpServletResponse httpServletResponse = (HttpServletResponse)response; 
            httpServletResponse.addCookie(cookie);
            String serviceString = httpServletRequest.getQueryString();
            if( serviceString != null && serviceString != "" )
            	serviceString = httpServletRequest.getRequestURI() + "?" + serviceString;
            else
            	serviceString = httpServletRequest.getRequestURI();
    		httpServletResponse.sendRedirect(_promotionRedirect + "&service=" + URLEncoder.encode(serviceString,"UTF-8"));
        }
        else
        	filterChain.doFilter( request, response );
    }

	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
	}
}
