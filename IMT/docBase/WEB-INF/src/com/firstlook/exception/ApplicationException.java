package com.firstlook.exception;

public class ApplicationException extends Exception
{
private static final long serialVersionUID = -5153694367294276209L;

public ApplicationException()
{
    super();
}

public ApplicationException( String arg0 )
{
    super(arg0);
}

public ApplicationException( String arg0, Throwable arg1 )
{
    super(arg0, arg1);
}

public ApplicationException( Throwable arg0 )
{
    super(arg0);
}
}
