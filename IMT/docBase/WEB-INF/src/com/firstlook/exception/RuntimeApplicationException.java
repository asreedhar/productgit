package com.firstlook.exception;

public class RuntimeApplicationException extends RuntimeException
{
private static final long serialVersionUID = 8113248435319170448L;

public RuntimeApplicationException()
{
    super();
}

public RuntimeApplicationException( String arg0 )
{
    super(arg0);
}

public RuntimeApplicationException( String arg0, Throwable arg1 )
{
    super(arg0, arg1);
}

public RuntimeApplicationException( Throwable arg0 )
{
    super(arg0);
}
}
