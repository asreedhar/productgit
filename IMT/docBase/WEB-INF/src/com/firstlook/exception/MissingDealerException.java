package com.firstlook.exception;

/**
 * Indicates that the code expected a <code>currentDealerId</code> on the <code>FirstLookSession</code>
 * object but instead they got the pseudo-null zero value.
 * 
 * @author swenmouth
 */
public class MissingDealerException extends RuntimeException {

	private static final long serialVersionUID = -1461053471243905118L;

	public MissingDealerException() {
	}

	public MissingDealerException(String message) {
		super(message);
	}

	public MissingDealerException(Throwable cause) {
		super(cause);
	}

	public MissingDealerException(String message, Throwable cause) {
		super(message, cause);
	}
}
