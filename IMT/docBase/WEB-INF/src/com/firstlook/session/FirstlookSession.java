package com.firstlook.session;

import java.io.Serializable;

import com.firstlook.entity.Member;
import com.firstlook.entity.ProgramTypeEnum;

public class FirstlookSession implements Serializable
{

private static final long serialVersionUID = -4316152624911447649L;

private boolean includeAppraisal;
private boolean includeAgingPlan;
private boolean includeCIA;
private boolean includeRedistribution;
private boolean includeAuction;
private boolean includeAnnualRoi;
private boolean includeEquityAnalyzer;
private boolean includeMarket; // second tier upgrade. not needed for check isHasUpgrades
private boolean includeWindowSticker; // second tier upgrade. not needed for check isHasUpgrades
private boolean includeAppraisalLockout; // second tier upgrade. not needed for check isHasUpgrades
private boolean includeAppraisalCustomer; // second tier upgrade. not needed for check isHasUpgrades
private boolean includeKBBTradeInValues; // second tier upgrade. not needed for check isHasUpgrades
private boolean includeNADAValues;
private boolean includePing; // second tier upgrade. not needed for check isHasUpgrades
private boolean includePingII; //not sure what tier this is, but it doesn't show up in menu.
private boolean includeMakeADeal;
private boolean includeMarketStockingGuide; 
private boolean includeJDPowerUsedCarMarketData; //not sure what tier this is, but it doesn't show up in menu.
private boolean includePerformanceDashboard;
private boolean includePerformanceReports;
private boolean includeReportsMenu;
private boolean includeRedistributionMenu;
private boolean includePrintButton;
private boolean reducedToolsMenu;
private boolean showNewDepartment;
private boolean showUsedDepartment;
private boolean showUserAdmin;
private boolean activeInventoryToolIsEstock;
private boolean edmundsTmvUpgrade; //not sure what tier this is, but it doesn't show up in menu.
private boolean showInTransitInventoryForm;
private boolean includeFirstlook3_0;


private String programType;

private Product mode;
private int currentDealerId;
public static final String FIRSTLOOK_SESSION = "firstlookSession";

private Member member;

private boolean internetAdvertisersEnabled;



public boolean isIncludeNADAValues() {
	return includeNADAValues;
}

public void setIncludeNADAValues(boolean includeNADAValues) {
	this.includeNADAValues = includeNADAValues;
}

// This is only for NON second tier upgrades
public boolean isHasUpgrades()
{
	if ( isIncludeAgingPlan() || isIncludeCIA() || isIncludeRedistribution() || isIncludeAppraisal() || isIncludeAuction() )
	{
		return true;
	}
	else
	{
		return false;
	}
}

public boolean isIncludeAgingPlan()
{
	return includeAgingPlan;
}

public boolean isIncludeCIA()
{
	return includeCIA;
}

public boolean isIncludeRedistribution()
{
	return includeRedistribution;
}

public boolean isIncludeAppraisal()
{
	return includeAppraisal;
}

public void setIncludeAgingPlan( boolean b )
{
	includeAgingPlan = b;
}

public void setIncludeCIA( boolean b )
{
	includeCIA = b;
}

public void setIncludeRedistribution( boolean b )
{
	includeRedistribution = b;
}

public void setIncludeAppraisal( boolean b )
{
	includeAppraisal = b;
}

public void setIncludePerformanceDashboard( boolean b )
{
	includePerformanceDashboard = b;
}

public boolean isIncludeAuction()
{
	return includeAuction;
}

public void setIncludeAuction( boolean b )
{
	includeAuction = b;
}

public boolean isIncludeFirstlook_3_0(){	
	return includeFirstlook3_0;
}

public void setIncludeFirstlook3_0 (boolean b)
{
	includeFirstlook3_0 = b;
}

public boolean isIncludePerformanceDashboard()
{
	return includePerformanceDashboard;
}

public boolean isIncludeMarket()
{
	return includeMarket;
}

public boolean isFLStore()
{
	if (programType != null)
		return programType.equals( ProgramTypeEnum.FIRSTLOOK.getName() );
	else
		return false;
}

public void setIncludeMarket( boolean b )
{
	includeMarket = b;
}

public boolean isIncludeWindowSticker()
{
	return includeWindowSticker;
}

public void setIncludeWindowSticker( boolean b )
{
	includeWindowSticker = b;
}

public String getMode()
{
	if ( mode == null )
		return null;

	if ( mode.equals( Product.EDGE ) )
		return mode.getLegacyName();

	return mode.getName();
}

public void setProduct( Product product )
{
	if( product == null )
		throw new NullPointerException( "User can't be in a null product." );
	// coding to work withing current bounds, the JSPs read this value and compare to an external hard coded value;
	// remove for future refactoring. -bf.
	if ( product.equals( Product.MAX ) || product.equals( Product.DEALERS_RESOURCES ) )
		throw new IllegalStateException( "For JSP compatibility, " + product + " mode is not valid." );

	mode = product;
}

public boolean isIncludePerformanceReports()
{
	return includePerformanceReports;
}

public void setIncludePerformanceReports( boolean includePerformanceReports )
{
	this.includePerformanceReports = includePerformanceReports;
}

public boolean isIncludeReportsMenu()
{
	return includeReportsMenu;
}

public void setIncludeReportsMenu( boolean includeReportsMenu )
{
	this.includeReportsMenu = includeReportsMenu;
}

public boolean isReducedToolsMenu()
{
	return reducedToolsMenu;
}

public void setReducedToolsMenu( boolean reducedToolsMenu )
{
	this.reducedToolsMenu = reducedToolsMenu;
}

public boolean isIncludePrintButton()
{
	return includePrintButton;
}

public void setIncludePrintButton( boolean includePrintButton )
{
	this.includePrintButton = includePrintButton;
}

public boolean isIncludeRedistributionMenu()
{
	return includeRedistributionMenu;
}

public void setIncludeRedistributionMenu( boolean includeRedistributionMenu )
{
	this.includeRedistributionMenu = includeRedistributionMenu;
}

public boolean isShowNewDepartment()
{
	return showNewDepartment;
}

public void setShowNewDepartment( boolean showNewDepartment )
{
	this.showNewDepartment = showNewDepartment;
}

public boolean isShowUsedDepartment()
{
	return showUsedDepartment;
}

public void setShowUsedDepartment( boolean showUsedDepartment )
{
	this.showUsedDepartment = showUsedDepartment;
}

public boolean isShowUserAdmin()
{
	return showUserAdmin;
}

public void setShowUserAdmin( boolean showUserAdmin )
{
	this.showUserAdmin = showUserAdmin;
}

public int getCurrentDealerId()
{
	return currentDealerId;
}

public void setCurrentDealerId( int currentDealerId )
{
	this.currentDealerId = currentDealerId;
}

public Member getMember()
{
	return member;
}

public void setMember( Member user )
{
	this.member = user;
}

public String getProgramType()
{
	return programType;
}

public void setProgramType( String programType )
{
	this.programType = programType;
}

public boolean isIncludeAnnualRoi()
{
	return includeAnnualRoi;
}

public void setIncludeAnnualRoi( boolean includeAnnualRoi )
{
	this.includeAnnualRoi = includeAnnualRoi;
}

public boolean isIncludeAppraisalCustomer()
{
	return includeAppraisalCustomer;
}

public void setIncludeAppraisalCustomer( boolean includeAppraisalCustomer )
{
	this.includeAppraisalCustomer = includeAppraisalCustomer;
}

public boolean isIncludeAppraisalLockout()
{
	return includeAppraisalLockout;
}

public void setIncludeAppraisalLockout( boolean includeAppraisalLockout )
{
	this.includeAppraisalLockout = includeAppraisalLockout;
}

public boolean isIncludeEquityAnalyzer()
{
	return includeEquityAnalyzer;
}

public void setIncludeEquityAnalyzer( boolean includeEquityAnalyzer )
{
	this.includeEquityAnalyzer = includeEquityAnalyzer;
}

public boolean isInternetAdvertisersEnabled()
{
	return internetAdvertisersEnabled;
}

public void setInternetAdvertisersEnabled( boolean internetAdvertisersEnabled )
{
	this.internetAdvertisersEnabled = internetAdvertisersEnabled;
}

public boolean isIncludeKBBTradeInValues()
{
	return includeKBBTradeInValues;
}

public void setIncludeKBBTradeInValues( boolean includeKBBTradeInValues )
{
	this.includeKBBTradeInValues = includeKBBTradeInValues;
}

public boolean isIncludePing()
{
	return includePing;
}

public void setIncludePing( boolean includePing )
{
	this.includePing = includePing;
}

public boolean isIncludeJDPowerUsedCarMarketData()
{
	return includeJDPowerUsedCarMarketData;
}

public void setIncludeJDPowerUsedCarMarketData( boolean includeJDPowerUsedCarMarketData )
{
	this.includeJDPowerUsedCarMarketData = includeJDPowerUsedCarMarketData;
}

public boolean isIncludePingII() {
	return includePingII;
}

public void setIncludePingII(boolean includePingII) {
	this.includePingII = includePingII;
}

public boolean isIncludeMakeADeal() {
	return includeMakeADeal;
}

public void setIncludeMakeADeal(boolean includeMakeADeal) {
	this.includeMakeADeal = includeMakeADeal;
}

public boolean isIncludeMarketStockingGuide() {
	return includeMarketStockingGuide;
}

public void setIncludeMarketStockingGuide(boolean includeMarketStockingGuide) {
	this.includeMarketStockingGuide = includeMarketStockingGuide;
}

public void setEdmundsTmvUpgrade(boolean edmundsTmvUpgrade) {
	this.edmundsTmvUpgrade = edmundsTmvUpgrade;
}

public boolean hasEdmundsTmvUpgrade() {
	return this.edmundsTmvUpgrade;
}

public boolean isEdmundsTmvUpgrade() {
	return this.edmundsTmvUpgrade;
}

public boolean isActiveInventoryToolIsEstock() {
	return activeInventoryToolIsEstock;
}

public void setActiveInventoryToolIsEstock(boolean activeInventoryToolIsEstock) {
	this.activeInventoryToolIsEstock = activeInventoryToolIsEstock;
}

public boolean isShowInTransitInventoryForm() {
	return showInTransitInventoryForm;
}

public void setShowInTransitInventoryForm(boolean showInTransitInventoryForm) {
	this.showInTransitInventoryForm = showInTransitInventoryForm;
}

}