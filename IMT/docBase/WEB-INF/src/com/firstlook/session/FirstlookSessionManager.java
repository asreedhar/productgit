package com.firstlook.session;

import java.util.Collection;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerUpgrade;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.dealer.IDealerService;

public class FirstlookSessionManager implements IFirstlookSessionManager
{

private IDealerService dealerService;
private InternetAdvertisingBusinessUnitDao internetAdvertisingBusinessUnitDao;

public FirstlookSessionManager()
{
	super();
}

/**
 * Now performs the changeCurrentDealer functionality if the dealerId passed in is different than what was used before. - KL
 * 
 */
public FirstlookSession constructFirstlookSession( int dealerId, Member member )
{
	Dealer dealer = dealerService.retrieveDealer( dealerId );
	member.setProgramType( dealer.getProgramTypeEnum().getName() );	

	FirstlookSession firstlookSession = constructMemberFirstlookSession( member );
	firstlookSession.setCurrentDealerId( dealerId );
	
	Collection<DealerUpgrade> dealerUpgrades;
	try
	{
		dealerUpgrades = dealerService.findUpgrades( dealerId );
		populateDealerUpgradesInFirstlookSession( dealerUpgrades, firstlookSession );
		populateDealerPreferencesInFirstlookSession(firstlookSession, dealer);
	}
	catch ( ApplicationException e )
	{
		e.printStackTrace();
	}

	return firstlookSession;
}

/**
 * Populates information regarding Members to the FirstlookSession
 */
public FirstlookSession constructMemberFirstlookSession( Member member )
{
	FirstlookSession firstlookSession = new FirstlookSession();
	processMemberSpecificRights( member, firstlookSession );
	firstlookSession.setProgramType( member.getProgramType() );
	firstlookSession.setMember( member );
	return firstlookSession;
}

public void populateDealerPreferencesInFirstlookSession(FirstlookSession firstlookSession, Dealer dealer)
{
	firstlookSession.setShowInTransitInventoryForm(dealer.getDealerPreference().getShowInTransitInventoryForm());
}

public void populateDealerUpgradesInFirstlookSession( Collection<DealerUpgrade> dealerUpgrades, FirstlookSession firstlookSession )
{
	firstlookSession.setIncludeAppraisal( dealerService.hasAppraisalUpgrade( dealerUpgrades ) );
	firstlookSession.setIncludeCIA( dealerService.hasCIAUpgrade( dealerUpgrades ) );
	firstlookSession.setIncludeAgingPlan( dealerService.hasAgingPlanUpgrade( dealerUpgrades ) );
	firstlookSession.setIncludeRedistribution( dealerService.hasRedistributionUpgrade( dealerUpgrades ) );
	firstlookSession.setIncludeAuction( dealerService.hasAuctionUpgrade( dealerUpgrades ) );
	firstlookSession.setIncludeWindowSticker( dealerService.hasWindowStickerUpgrade( dealerUpgrades ) );
	firstlookSession.setIncludePerformanceDashboard( dealerService.hasPerformanceDashboardUpgrade( dealerUpgrades ) );
	firstlookSession.setIncludeMarket( dealerService.hasMarketUpgrade( dealerUpgrades ) );
	firstlookSession.setIncludeAnnualRoi( dealerService.hasAnnualRoiUpgrade( dealerUpgrades ) );
	firstlookSession.setIncludeAppraisalLockout( dealerService.hasAppraisalLockoutUpgrade( dealerUpgrades ) );
	firstlookSession.setIncludeEquityAnalyzer( dealerService.hasEquityAnalyzerUpgrade( dealerUpgrades ) );
	firstlookSession.setIncludeKBBTradeInValues( dealerService.hasKBBTradeInUpgrade( dealerUpgrades ) );
	firstlookSession.setIncludePing( dealerService.hasPingUpgrade( dealerUpgrades ) );
	firstlookSession.setInternetAdvertisersEnabled( internetAdvertisingBusinessUnitDao.hasActiveInternetAdvertisers( firstlookSession.getCurrentDealerId() ) );
	firstlookSession.setIncludeJDPowerUsedCarMarketData( dealerService.hasJDPowerUCMDataUpgrade( dealerUpgrades ) );
	firstlookSession.setIncludePingII(dealerService.hasPingIIUpgrade(dealerUpgrades));
	firstlookSession.setEdmundsTmvUpgrade(dealerService.hasEdmundsTmvUpgrade(dealerUpgrades));
	firstlookSession.setIncludeMakeADeal(dealerService.hasMakeADealUpgrade(dealerUpgrades));
	firstlookSession.setIncludeMarketStockingGuide(dealerService.hasMarketStockingGuideUpgrade(dealerUpgrades));
	firstlookSession.setIncludeFirstlook3_0(dealerService.hasFirstlook3_0Upgrade(dealerUpgrades));
	firstlookSession.setIncludeNADAValues(dealerService.hasNADAValuesUpgrade(dealerUpgrades));
}

private void processMemberSpecificRights( Member member, FirstlookSession firstlookSession )
{

	if ( member.getMemberRoleTester().isUsedManager() && firstlookSession.isIncludePerformanceDashboard() )
	{
		firstlookSession.setIncludePerformanceDashboard( true );
		firstlookSession.setIncludePerformanceReports( true );
	}
	else
	{
		firstlookSession.setIncludePerformanceDashboard( false );
		firstlookSession.setIncludePerformanceReports( false );
	}

	if ( member.getMemberRoleTester().isUsedAppraiser() || member.getMemberRoleTester().isUsedNoAccess() )
	{
		firstlookSession.setReducedToolsMenu( true );
		firstlookSession.setIncludeReportsMenu( false );
		firstlookSession.setIncludeRedistributionMenu( false );
		firstlookSession.setIncludePrintButton( false );
	}
	else
	{
		firstlookSession.setReducedToolsMenu( false );
		firstlookSession.setIncludeReportsMenu( true );
		firstlookSession.setIncludeRedistributionMenu( true );
		firstlookSession.setIncludePrintButton( true );
	}
	
	// 0 = estock, 1 = vpa. 0 is default
	firstlookSession.setActiveInventoryToolIsEstock(member.getActiveInventoryLaunchTool().intValue() == 0);

	firstlookSession.setShowUsedDepartment( !member.getMemberRoleTester().isUsedNoAccess() );
	firstlookSession.setShowNewDepartment( !member.getMemberRoleTester().isNewNoAccess() );

	firstlookSession.setShowUserAdmin( member.getMemberRoleTester().isAdminFull() );
}

public void setImtDealerService( IDealerService dealerService )
{
	this.dealerService = dealerService;
}

public void setInternetAdvertisingBusinessUnitDao( InternetAdvertisingBusinessUnitDao internetAdvertisingBusinessUnitDao )
{
	this.internetAdvertisingBusinessUnitDao = internetAdvertisingBusinessUnitDao;
}
}
