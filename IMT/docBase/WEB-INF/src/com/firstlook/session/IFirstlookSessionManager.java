package com.firstlook.session;


import java.util.Collection;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerUpgrade;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;

public interface IFirstlookSessionManager
{
/**
 * Now performs the changeCurrentDealer functionality if the dealerId passed in
 * is different than what was used before. - KL
 * @return TODO
 */
public abstract FirstlookSession constructFirstlookSession( int dealerId, Member member );

public void populateDealerPreferencesInFirstlookSession(FirstlookSession firstlookSession, Dealer dealer);

public void populateDealerUpgradesInFirstlookSession( Collection<DealerUpgrade> dealerUpgrades, FirstlookSession firstlookSession ) throws ApplicationException;

public FirstlookSession constructMemberFirstlookSession( Member member );

}