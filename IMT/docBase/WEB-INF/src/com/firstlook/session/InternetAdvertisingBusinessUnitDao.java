package com.firstlook.session;

import org.springframework.jdbc.core.JdbcTemplate;

public class InternetAdvertisingBusinessUnitDao
{

private JdbcTemplate jdbcTemplate;

public boolean hasActiveInternetAdvertisers(Integer businessUnitId) {
    int activeAdvertisers = jdbcTemplate.queryForInt( "select count(*) from InternetAdvertiserDealership where businessUnitId = ? and (IsLive = ? OR IncrementalExport = ?)", new Object[] {businessUnitId, Boolean.TRUE, Boolean.TRUE } );

    return (activeAdvertisers > 0 );
}

public void setJdbcTemplate( JdbcTemplate jdbcTemplate )
{
	this.jdbcTemplate = jdbcTemplate;
}

}
