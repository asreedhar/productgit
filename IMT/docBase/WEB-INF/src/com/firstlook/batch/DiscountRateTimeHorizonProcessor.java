package com.firstlook.batch;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import biz.firstlook.cia.calculator.ValuationCalculator;
import biz.firstlook.cia.service.ValuationService;
import biz.firstlook.commons.date.BasisPeriod;
import biz.firstlook.commons.util.BaseScriptJob;
import biz.firstlook.commons.util.ILogger;
import biz.firstlook.transact.persist.db.TransactDatabaseUtil;
import biz.firstlook.transact.persist.model.DealerValuation;

import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.dealer.IDealerService;

public class DiscountRateTimeHorizonProcessor extends BaseScriptJob implements ILogger
{

private int updateCount;
private int loggingMode;
private String businessUnitCode;
private boolean dailyMode;

private static Logger logger = Logger.getLogger( DiscountRateTimeHorizonProcessor.class );
private IDealerService dealerService;
private ApplicationContext applicationContextHibernate;
private ApplicationContext applicationContextPojo;
private String location = "applicationContext-hibernate.xml";
private String pojoLocation = "applicationContext-pojo.xml";

public static void main( String[] args )
{
	DiscountRateTimeHorizonProcessor processor = new DiscountRateTimeHorizonProcessor();
	processor.executeJob( args );
	System.exit( 0 );
}

protected String getJobStatus()
{
	return "batch updated " + updateCount + " DealerValuations";
}

protected String getUsage()
{
	return "usage: DiscountRateTimeHorizonProcessor [{-x, --dailymode}] [-d, --dealergroupiId] dealercode [-o, --level] loggingmode ";
}

public void start() throws Throwable
{
	
	Date today = new Date();
	applicationContextHibernate = new ClassPathXmlApplicationContext( location );
	applicationContextPojo = new ClassPathXmlApplicationContext( new String[] {pojoLocation}, applicationContextHibernate );
	
	SessionFactory sessionFactory = (SessionFactory)applicationContextHibernate.getBean( "sessionFactory" );
	Session session = SessionFactoryUtils.getSession( sessionFactory, true );
	TransactionSynchronizationManager.bindResource( sessionFactory, new SessionHolder( session ) );
	
	this.dealerService = (IDealerService)applicationContextPojo.getBean("dealerService");

	ValuationService service = (ValuationService)applicationContextPojo.getBean( "valuationService" );
	service.setLogger( this );
	service.setValuationCalculator( new ValuationCalculator( this ) );
	Collection dealers = initializeDealers();

	Iterator dealerIter = dealers.iterator();
	log( "*******DISCOUNT RATE TIME HORIZON PROCESSOR: begin run at " + today.toString() + "************************", 0 );
	while ( dealers != null && dealerIter.hasNext() )
	{
		Dealer dealer = (Dealer)dealerIter.next();
		DealerValuation dealerValuation = service.determineRateAndTime( dealer.getDealerId().intValue(), BasisPeriod.TWENTY_SIX_WEEKS );
		if ( dealerValuation != null )
		{
			TransactDatabaseUtil.instance().save( dealerValuation );
			updateCount++;
		}
	}
	today = new Date();
	log( "*******DISCOUNT RATE TIME HORIZON PROCESSOR: end run at " + today.toString() + "*************************", 0 );
	log( "\n", 0 );
	TransactionSynchronizationManager.unbindResource( sessionFactory );
}

public void digestArguments()
{
	String loggingModeStr = getOptions().getStringOption( OptionsParser.OPT_LOGGINGMODE, "0" );
	this.businessUnitCode = getOptions().getStringOption( OptionsParser.OPT_DEALERGROUPID, null );
	dailyMode = getOptions().getBooleanOption( OptionsParser.OPT_DAILYMODE );
	try
	{
		loggingMode = Integer.parseInt( loggingModeStr );
	}
	catch ( NumberFormatException e )
	{
		loggingMode = 0;
		log( "An invalid option " + loggingModeStr + " has been specified for the Discount Rate and Time Horizon Processor run.", 0 );
	}
}

public void log( String message, int logLevel )
{
	if ( logLevel <= loggingMode )
	{
		logger.info( message );
	}
}

private Collection initializeDealers() throws ApplicationException
{
	if ( dailyMode )
	{
		return dealerService.retrieveDealersWithNoDRT();
	}
	else if ( businessUnitCode != null )
	{
		return dealerService.retrieveByDealerCode( businessUnitCode );
	}
	else
	{
		return dealerService.retrieveActiveDealers();
	}
}

}
