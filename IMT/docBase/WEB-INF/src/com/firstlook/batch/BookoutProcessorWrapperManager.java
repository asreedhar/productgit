package com.firstlook.batch;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;

import biz.firstlook.commons.util.StringConstants;

import com.firstlook.batch.bookout.distributed.BookoutProcessorWrapperWorker;
import com.firstlook.batch.bookout.distributed.model.BookoutMode;
import com.firstlook.batch.bookout.distributed.model.BookoutProcessorArgs;
import com.firstlook.batch.bookout.distributed.model.BookoutProvider;
import com.firstlook.batch.bookout.distributed.model.BookoutProcessorArgsImpl;

public class BookoutProcessorWrapperManager
{

public static final int EXIT_CODE_NO_ERROR = 0;
public static final int EXIT_CODE_CLI_ERROR = 1;
public static final int EXIT_CODE_BOOKOUT_ERROR = 2;

private static Logger logger = Logger.getLogger( BookoutProcessorWrapperManager.class );

/**
 * An entry point for distributed and multithreaded bookout processor.
 * 
 * @param args
 *            numberOfThreads
 */
public static void main( String[] args )
{
	int exitCode = EXIT_CODE_NO_ERROR;
	final Options options = new Options();
	try
	{
		BookoutProcessorArgs processorArgs = buildContext(options, args);
		BookoutProcessorWrapperWorker worker = new BookoutProcessorWrapperWorker(processorArgs);
		worker.run();
	}
	catch (ParseException pe)
	{
		printUsage(options);
		exitCode = EXIT_CODE_CLI_ERROR;
	}
	catch ( Exception e )
	{
		e.printStackTrace();
		exitCode = EXIT_CODE_BOOKOUT_ERROR;
	}
	
	logger.info("Exiting with error code: " + exitCode);
	System.exit( exitCode );
}

private static void printUsage(Options options) {
	HelpFormatter formatter = new HelpFormatter();
	formatter.printHelp("java com.firstlook.batch.BookoutProcessorWrapperManager", options, true);
	StringBuffer usage = new StringBuffer();
	usage.append( "examples:").append(StringConstants.NEW_LINE);
	usage.append( "  java com.firstlook.batch.BookoutProcessorWrapperManager").append(StringConstants.NEW_LINE);
	usage.append( "  java com.firstlook.batch.BookoutProcessorWrapperManager -m incremental -b blackbook").append(StringConstants.NEW_LINE);
	usage.append( "  java com.firstlook.batch.BookoutProcessorWrapperManager -m f -b KBB").append(StringConstants.NEW_LINE);
	usage.append(StringConstants.NEW_LINE);
	System.out.println(usage.toString());
}

@SuppressWarnings("static-access")
static BookoutProcessorArgs buildContext(Options options, String[] args)
		throws ParseException {
	CommandLineParser cmdLineParser = new BasicParser();
	Option helpOption = OptionBuilder.withLongOpt("help")
									 .withDescription("prints the help description")
									 .create("h");
	
	StringBuilder modesLongDesc = new StringBuilder();
	modesLongDesc.append("sets the processor mode. When not specified, the processor is controlled by the database entry.").append(StringConstants.NEW_LINE);
	modesLongDesc.append("Valid modes are: ").append(StringConstants.NEW_LINE);
	for(BookoutMode mode : BookoutMode.values()) {
		if(!mode.equals(BookoutMode.ANY))
			modesLongDesc.append(mode.name()).append(StringConstants.NEW_LINE);
	}
	Option modeOption = OptionBuilder.withLongOpt("mode")
									 .withArgName("mode")
									 .hasArg()
									 .withDescription(modesLongDesc.toString())
									 .create("m");
	
	StringBuilder bookLongDesc = new StringBuilder();
	bookLongDesc.append("sets the book to process. When not specified, the processor is controlled by the database entry.").append(StringConstants.NEW_LINE);
	bookLongDesc.append("Valid books are: ").append(StringConstants.NEW_LINE);
	for(BookoutProvider provider : BookoutProvider.values()) {
		if(!provider.equals(BookoutProvider.ANY))
			bookLongDesc.append(provider.name()).append(StringConstants.NEW_LINE);
	}
	Option bookOption =  OptionBuilder.withLongOpt("book")
								.withArgName("bookname")
								.hasArg()
								.withDescription(bookLongDesc.toString())
								.create("b");
	
	Option timeoutOption =  OptionBuilder.withLongOpt("timeout")
								.withArgName("minutes")
								.hasArg()
								.withDescription("the amount of time in minutes the bookout processor is allowed to run for.  If the timeout value is reached, the processor will end after the completion of the dealership.")
								.create("t");

	
	options.addOption(helpOption);
	options.addOption(modeOption);
	options.addOption(bookOption);
	options.addOption(timeoutOption);
	CommandLine cmdLine = cmdLineParser.parse(options, args);
	if(cmdLine.hasOption(helpOption.getOpt())) {
		printUsage(options);
		System.exit(EXIT_CODE_NO_ERROR);
	}

	BookoutMode mode = parseBookoutMode(cmdLine, modeOption);
	
	BookoutProvider provider = parseBookoutProvider(cmdLine, bookOption);
	
	int timeout = parseTimeout(timeoutOption, cmdLine);

	return new BookoutProcessorArgsImpl(mode, provider, timeout);
}

/**
 * When the mode option is not specified, defaults to BookoutMode.ANY
 * @param cmdLine
 * @param modeOption
 * @return
 * @throws ParseException
 */
private static BookoutMode parseBookoutMode(CommandLine cmdLine, Option modeOption)
		throws ParseException {
	if(cmdLine.hasOption(modeOption.getOpt())) {
		String modeArg = cmdLine.getOptionValue(modeOption.getOpt());
		if(modeArg.equalsIgnoreCase(BookoutMode.INCREMENTAL.name())
				|| modeArg.equalsIgnoreCase("I")) {
			return BookoutMode.INCREMENTAL;
		} else if (modeArg.equalsIgnoreCase(BookoutMode.FULL.name())
				|| modeArg.equalsIgnoreCase("F")) {
			return BookoutMode.FULL;
		} else {
			throw new ParseException("Bad mode argument.");
		}
	} else {
		return BookoutMode.ANY;
	}
}

/**
 * When not specified in the command line, defaults to BookoutProvider.ANY
 * @param cmdLine
 * @param bookOption
 * @return 
 * @throws ParseException
 */
private static BookoutProvider parseBookoutProvider(CommandLine cmdLine, Option bookOption) throws ParseException {
	if(cmdLine.hasOption(bookOption.getOpt())) {
		String optValue = cmdLine.getOptionValue(bookOption.getOpt());
		for(BookoutProvider provider : BookoutProvider.values())
			if(optValue.equalsIgnoreCase(provider.name()))
				return provider;
		throw new ParseException("Unsupported bookout provider");
	} else {
		return BookoutProvider.ANY;
	}
}

/**
 * Default timeout is never.
 * @param timeoutOption
 * @param cmdLine
 * @return
 * @throws ParseException
 */
private static int parseTimeout(Option timeoutOption, CommandLine cmdLine) throws ParseException {
	int timeout = 0;
	if(cmdLine.hasOption(timeoutOption.getOpt())) {
		String minutes = cmdLine.getOptionValue(timeoutOption.getOpt());
		try {
			timeout = Integer.valueOf(minutes);
		} catch (NumberFormatException nfe) {
			throw new ParseException(nfe.getLocalizedMessage());
		}
	}
	return timeout;
}

}
