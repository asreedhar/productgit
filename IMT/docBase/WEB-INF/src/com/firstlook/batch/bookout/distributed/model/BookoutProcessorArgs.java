package com.firstlook.batch.bookout.distributed.model;

import java.util.Date;


public interface BookoutProcessorArgs {

	/**
	 * 
	 * @return the BookoutProvider, or null, meaning any provider is valid
	 */
	public abstract BookoutProvider getBookoutProvider();
	
	/**
	 * 
	 * @return the BookoutMode, or null, meaning any mode is valid
	 */
	public abstract BookoutMode getBookoutMode();
	
	/**
	 * 
	 * @return the date / time when the processor should stop processing new BookoutProcessorRun(s), null if not specified.
	 */
	public abstract Date getTimeoutDate();
}
