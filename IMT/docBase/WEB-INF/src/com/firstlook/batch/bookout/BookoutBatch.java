package com.firstlook.batch.bookout;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import biz.firstlook.module.vehicle.description.old.BookOutDatasetInfo;
import biz.firstlook.module.vehicle.description.old.BookOutError;
import biz.firstlook.module.vehicle.description.old.BookOutTypeEnum;
import biz.firstlook.module.vehicle.description.old.GBException;
import biz.firstlook.module.vehicle.description.old.GuideBookInput;
import biz.firstlook.module.vehicle.description.old.GuideBookMetaInfo;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.enumerator.BookoutStatusEnum;
import biz.firstlook.transact.persist.model.AuditBookOuts;
import biz.firstlook.transact.persist.model.BookOut;
import biz.firstlook.transact.persist.model.BookOutSource;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.InventoryWithBookOut;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.persistence.IInventoryBookOutDAO;
import biz.firstlook.transact.persist.persistence.InventoryBookOutStoredProcedure;

import com.firstlook.data.DatabaseException;
import com.firstlook.dealer.tools.VinNotFoundException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.bookout.BookOutService;
import com.firstlook.service.bookout.bean.AbstractBookOutState;
import com.firstlook.service.bookout.bean.BookValuesBookOutState;
import com.firstlook.service.bookout.bean.SelectedVehicleAndOptionsBookOutState;
import com.firstlook.service.bookout.bean.StoredBookOutState;
import com.firstlook.service.bookout.inventory.InventoryBookOutService;
import com.firstlook.service.dealer.IDealerService;

public class BookoutBatch implements IBookoutBatch
{

private static Logger logger = Logger.getLogger( BookoutBatch.class );
private static Logger vinNotFound_logger = Logger.getLogger( VinNotFoundException.class );
	
private BookOutService bookOutService;
private IInventoryBookOutDAO inventoryBookOutDAO;
private InventoryBookOutStoredProcedure inventoryBookOutStoredProcedure;
private InventoryBookOutService inventoryBookOutService;
private IDealerService dealerService;
private SessionFactory sessionFactory;

public enum BookoutUpdateCount
{
	SUCCESSFUL, FAILED
};

public BookoutBatch( BookOutService bookOutService, IInventoryBookOutDAO inventoryBookOutDAO, InventoryBookOutStoredProcedure inventoryBookOutStoredProcedure, InventoryBookOutService inventoryBookOutService, IDealerService dealerService, SessionFactory sessionFactory)
{
	this.bookOutService = bookOutService;
	this.inventoryBookOutDAO = inventoryBookOutDAO;
	this.inventoryBookOutStoredProcedure = inventoryBookOutStoredProcedure; 
	this.inventoryBookOutService = inventoryBookOutService;
	this.dealerService = dealerService;
	this.sessionFactory = sessionFactory;
}

public Map< BookoutUpdateCount, Integer > execute( final Integer dealerId, Integer thirdPartyId, final boolean incrementalMode ) throws DatabaseException, InterruptedException, ApplicationException,
		InterruptedIOException, IOException
{
	int successfulUpdateCount = 0, failedUpdateCount = 0;
	Map< BookoutUpdateCount, Integer > updateCounts = new HashMap< BookoutUpdateCount, Integer >();
	updateCounts.put( BookoutUpdateCount.SUCCESSFUL, successfulUpdateCount );
	updateCounts.put( BookoutUpdateCount.FAILED, failedUpdateCount );

	try
	{
		/* Initialize variables to avoid lazy loading issues */
		Session session = SessionFactoryUtils.getSession( sessionFactory, true );
		TransactionSynchronizationManager.bindResource( sessionFactory, new SessionHolder( session ) );
		
		Dealer dealer = dealerService.retrieveDealer(dealerId);
		String geographicalState = dealer.getState();
		
		DealerPreference dealerPreference = dealer.getDealerPreference();
		int nadaRegionCode = dealerPreference.getNadaRegionCode();
		boolean calculateAverageBookValue = dealerPreference.isCalculateAverageBookValue();
		boolean advertisingStatus = (dealerPreference.getAdvertisingStatus() == null) ? false : dealerPreference.getAdvertisingStatus();
		int searchInactiveInventoryDaysBackThreshold = dealerPreference.getSearchInactiveInventoryDaysBackThreshold();
		// nk - needed to retro fit the old way - default to their primary book
		if ( thirdPartyId == null )
		{
			thirdPartyId = dealerPreference.getGuideBookId();
		}

		TransactionSynchronizationManager.unbindResource( sessionFactory );
		SessionFactoryUtils.releaseSession( session, sessionFactory );
		session = null;
		
		logger.info( "Dealer " + dealerId + " initialized using " + book(thirdPartyId)+" with advertising status "+advertisingStatus);
		/* Dealer and DealerPreference information initialized*/


		if ( nadaRegionCode < 1 && thirdPartyId.intValue() == ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE )
		{
			logger.warn( "Skipping dealer: " + dealerId + " as it has an invalid NADA region code of: " + nadaRegionCode );
		}

		List<Integer> inventoryIds = inventoryBookOutStoredProcedure.findForInventoryIdsForBookout(dealerId, thirdPartyId, incrementalMode);
		int totalInventoryNumber = inventoryIds.size();

		for(int currentInventoryNumber = 0; currentInventoryNumber < totalInventoryNumber; currentInventoryNumber++)
		{
			Integer inventoryId = inventoryIds.get(currentInventoryNumber);
			boolean starting = true;
			boolean failed = false;
			long inventoryStart = System.currentTimeMillis();
			String trimId = null;
			logInventoryItem(currentInventoryNumber, totalInventoryNumber, inventoryId, starting, failed, 0);
			
			//Start transaction at inventory level!
			session = SessionFactoryUtils.getSession( sessionFactory, true );
			TransactionSynchronizationManager.bindResource( sessionFactory, new SessionHolder( session ) );
			try
			{
				InventoryWithBookOut inventory = inventoryBookOutDAO.findByPk(inventoryId);
									
				GuideBookInput basicInput = assembleGuideBookBasicInput( dealerId, inventory.getMileageReceived(), inventory.getVehicle().getVin(), nadaRegionCode, geographicalState );

				AbstractBookOutState abstractBookOutState = inventoryBookOutService.retrieveExistingBookOutInfoForProcessor(
																																	basicInput,
																																	thirdPartyId,
																																	inventory,
																																	searchInactiveInventoryDaysBackThreshold );

				// nk : DE: 623
				BookoutStatusEnum bookOutStatus = getStatusAndSetSource( thirdPartyId, inventory, abstractBookOutState  );

				if ( !abstractBookOutState.getBookOutErrors().isEmpty() )
				{
					handleBookOutErrors( basicInput, abstractBookOutState );
				}
				else
				{
					String selectedVehicleKey = abstractBookOutState.getSelectedVehicleKey();
					/*============ Start Case 32827 ===========================*/
					if ( selectedVehicleKey == null ) {
						try {
							trimId = inventoryBookOutDAO.getTrimId(inventoryId, dealerId, thirdPartyId);
							if (( trimId != null ) && ( trimId != "" ) && abstractBookOutState.getDropDownVehicles() != null && !abstractBookOutState.getDropDownVehicles().isEmpty()) {
								selectedVehicleKey = trimId;
							}

						} catch (SQLException e) {
							logger.warn("Error while getting trim Id based on chrome Id " + e.getMessage());
						}
					}
					
					if ( selectedVehicleKey != null ) // Vin decoded
						// to just one
					// vehicle
					{
						
						VDP_GuideBookVehicle selectedVehicle = BookOutService.determineSelectedVehicleByKey(
																												abstractBookOutState.getDropDownVehicles(),
																												selectedVehicleKey );

						inventoryBookOutService.updateBookValues( basicInput, selectedVehicle,
																		abstractBookOutState.getThirdPartyVehicleOptions(),
																		abstractBookOutState.getDropDownVehicles(),
																		AuditBookOuts.INVENTORY_BOOKOUT_ID, inventory, thirdPartyId,
																		abstractBookOutState.getBookOutSourceId(), bookOutStatus , advertisingStatus );
						successfulUpdateCount++;
					}
					else
					// decoded
					// to
					// mutiple
					// vehicles
					{
						// We only want to get the averaged value if the
						// dealer preference indicates that we should.
						// Otherwise, we don't generate any book value for
						// this vehicle - MH, 11/22/2005
						
						
						
							
						if ( calculateAverageBookValue )
						{
							Integer msrp = null;
							Integer weight = null;
							if (abstractBookOutState instanceof StoredBookOutState) {
								StoredBookOutState storedBookOutState = (StoredBookOutState) abstractBookOutState;
								msrp = storedBookOutState.getMsrp();
								weight = storedBookOutState.getWeight();
							}
							
							averageGuideBookValuesAndUpdateBookInfo( basicInput, inventory,
																		abstractBookOutState.getDropDownVehicles(),
																		abstractBookOutState.getMetaInfo(), thirdPartyId,
																		abstractBookOutState.getBookOutSourceId(),
																		msrp, weight, bookOutStatus);
							 
							
							successfulUpdateCount++;
						}
						
					}
				} // end else
			} // end try catch
			catch ( VinNotFoundException vnfe )
			{
				// so long as VinNotFoundException has a logging level > error
				// this will write out
				vinNotFound_logger.error( "VIN not found for InventoryId:  " + inventoryId + ". " + vnfe.toString());
				logger.warn("Actual error, InventoryID " + inventoryId + ", ", vnfe);
				failedUpdateCount++;
				failed = true;
			}
			catch ( Exception e )
			{
				logger.warn( "Problem encountered during BookOut with inventory: " + inventoryId, e );
				failedUpdateCount++;
				failed = true;
			} 
			finally
			{
				TransactionSynchronizationManager.unbindResource( sessionFactory );
				SessionFactoryUtils.releaseSession( session, sessionFactory );
				session = null;
				System.gc(); //this should bandaid the leaks.
		
				starting = false;
				logInventoryItem(currentInventoryNumber, totalInventoryNumber, inventoryId, starting, failed, (System.currentTimeMillis() - inventoryStart));
			}
		} // end while

		updateCounts.put( BookoutUpdateCount.SUCCESSFUL, successfulUpdateCount );
		updateCounts.put( BookoutUpdateCount.FAILED, failedUpdateCount );
		return updateCounts;
	}
	catch ( Exception e )
	{
		logger.fatal( "fatal Problem encountered during BookOut", e );
		return updateCounts;
	}
}

private static String book(int guideBookId) {
	switch(guideBookId) {
		case 1: return "BlackBook";
		case 2: return "NADA";
		case 3: return "KelleyBlueBook";
		case 4: return "Galves";
		default: return "Unknown book";
	}
} 

private static void logInventoryItem(int currentInventoryNumber, int totalInventoryNumber, Integer inventoryId, boolean starting, boolean failed, long timeInMilli) {
	StringBuilder msg = new StringBuilder();
	
	if(starting)
		msg.append("Starting ");
	else
		msg.append("Finished ");
	
	msg.append("InventoryID: ").append(inventoryId).append(", number ").append(currentInventoryNumber).append(" of ").append(totalInventoryNumber).append(", ");
	
	if(!starting) {
		msg.append("status: ");
		if(!failed) {
			msg.append("SUCCESS. ");
		} else {
			msg.append("FAILED. ");
		}
		msg.append("Took ").append(timeInMilli).append(" ms. ");
	}
	
	logger.info(msg.toString());
}

/**
 * nk : DE623 : 
 * 	If this Inv Item HAS NEVER been booked out before the status is NOT REVIEWED 
 *  if this Inv Item HAS been booked out before BUT the status is still = NOT REVIEWED If this Inv Item has been booked out before BUT the status is still = NOT REVIEWED HOWEVER - if an
 * Appraisal BookOut exists for this inventory item, the status is CLEAN(next block)
 * 
 * @param bookout
 * @param fromProcessor
 */
private BookoutStatusEnum getStatusAndSetSource( Integer thirdPartyId, InventoryWithBookOut inventory, AbstractBookOutState abstractBookOutState )
{
	BookoutStatusEnum bookoutStatus = BookoutStatusEnum.CLEAN;
	BookOut bookOut = inventory.getLatestBookOut( thirdPartyId );
	//Dealer dealer = dealerService.retrieveDealer(dealerId);
	// if this Inv Item has never been booked out before, the status is NOT REVIEWED OR
	// If this Inv Item has been booked out before BUT the status is NOT REVIEWED it stays NOT_REVIEWED
	if ( ( bookOut == null ) || ( bookOut.getBookoutStatusId().intValue() == BookoutStatusEnum.NOT_REVIEWED.getId().intValue() ) )
	{
			bookoutStatus = BookoutStatusEnum.NOT_REVIEWED;
	}else if ( bookOut.getBookoutStatusId().intValue() == BookoutStatusEnum.INGROUP_TRANSFER.getId().intValue() )
	{
		// if the BookOut was in group transfer then the status is INGROUP_TRANSFER
		bookoutStatus = BookoutStatusEnum.INGROUP_TRANSFER;
	}
	if ( (abstractBookOutState.getBookOutSourceId() == BookOutSource.BOOK_OUT_SOURCE_APPRAISAL) )
	{
		// HOWEVER - if an Appraisal BookOut exists for this inventory item, the status is CLEAN
		// 31/Mar/2015 -> adding isBookoutClean(fl-admin setting) condition as per the case 32827.
		//changing the name of the flag to advertising status as per case 34026
		bookoutStatus = BookoutStatusEnum.CLEAN;
	}
	
	abstractBookOutState.setBookOutSourceId( BookOutSource.BOOK_OUT_SOURCE_PROCESSOR );
	return bookoutStatus;
}

private void averageGuideBookValuesAndUpdateBookInfo( GuideBookInput basicInput, InventoryWithBookOut inventoryWithBookOut, List<VDP_GuideBookVehicle> dropDownVehicles,
														GuideBookMetaInfo metaInfo, Integer thirdPartyId, Integer bookOutSourceId, Integer msrp, Integer weight, BookoutStatusEnum bookOutStatus )
		throws ApplicationException, InterruptedIOException, DatabaseException
{
	List<VDP_GuideBookValue> guideBookValues = retrieveAverageBestMatchVehiclePrices( dropDownVehicles, basicInput, thirdPartyId );

	inventoryWithBookOut.clearIncrementalBookout(ThirdPartyDataProvider.getThirdPartyDataProvider(thirdPartyId));
	try{
		if (bookOutStatus.getId().intValue() != BookoutStatusEnum.INGROUP_TRANSFER.getId().intValue())	{
			bookOutStatus = BookoutStatusEnum.NOT_REVIEWED;
		}
		/*
		inventoryBookOutService.saveUpdatedBookInfo( basicInput, guideBookValues, Collections.<ThirdPartyVehicleOption>emptyList(), Collections.<VDP_GuideBookVehicle>emptyList(), "",
															thirdPartyId.intValue(), inventoryWithBookOut, 0, msrp, weight,
															metaInfo,
															AuditBookOuts.INVENTORY_BOOKOUT_ID, bookOutSourceId, BookoutStatusEnum.NOT_REVIEWED, null );
		*/
		inventoryBookOutService.saveUpdatedBookInfo( basicInput, guideBookValues, Collections.<ThirdPartyVehicleOption>emptyList(), Collections.<VDP_GuideBookVehicle>emptyList(), "",
				thirdPartyId.intValue(), inventoryWithBookOut, 0, msrp, weight,
				metaInfo,
				AuditBookOuts.INVENTORY_BOOKOUT_ID, bookOutSourceId, bookOutStatus, null , false );
	}
	catch(GBException gbe){
		throw new ApplicationException(gbe);
	}
}

private void handleBookOutErrors( GuideBookInput basicInput, AbstractBookOutState bookOutState )
{
	Iterator<Integer> it = bookOutState.getBookOutErrors().keySet().iterator();
	BookOutError error = null;
	List<BookOutError> errorList = null;
	while ( it.hasNext() )
	{
		errorList = bookOutState.getBookOutErrors().get( it.next() );
		error = (BookOutError)errorList.toArray()[0];
		logger.error( "BookOutProcessor error: VIN: " + basicInput.getVin() + " " + error.getErrorMessage() );
	}
}

private GuideBookInput assembleGuideBookBasicInput( Integer dealerId, Integer mileage, String vin, int nadaRegionCode, String geographicalState )
{
	GuideBookInput basicInput = new GuideBookInput();
	basicInput.setBusinessUnitId( dealerId );
	basicInput.setMemberId( Member.DUMMY_MEMBER_ID );
	basicInput.setMileage( checkMileage( mileage ) );
	basicInput.setNadaRegionCode( nadaRegionCode );
	basicInput.setState( geographicalState );
	basicInput.setVin( vin );
	return basicInput;
}

private List<VDP_GuideBookValue> retrieveAverageBestMatchVehiclePrices( List<VDP_GuideBookVehicle> bestMatchGuideBookVehicles, GuideBookInput basicInput,
													Integer thirdPartyId )
		throws InterruptedIOException, ApplicationException, DatabaseException
{
	Iterator<VDP_GuideBookVehicle> matchIter = bestMatchGuideBookVehicles.iterator();

	SelectedVehicleAndOptionsBookOutState optionsBookOutState;
	BookValuesBookOutState valuesBookOutState;
	Map<String, List<VDP_GuideBookValue>> totalBookValuesMap = new HashMap<String, List<VDP_GuideBookValue>>();

	try{
		while ( matchIter.hasNext() )
		{
			VDP_GuideBookVehicle guideBookVehicle = matchIter.next();
			optionsBookOutState = bookOutService.lookUpOptionsForSingleVehicle( basicInput, thirdPartyId,
																						guideBookVehicle, new BookOutDatasetInfo( BookOutTypeEnum.INVENTORY, basicInput.getBusinessUnitId())  );
	
				valuesBookOutState = bookOutService.updateBookValues( basicInput, thirdPartyId, guideBookVehicle, optionsBookOutState.getThirdPartyVehicleOptions(), new BookOutDatasetInfo( BookOutTypeEnum.INVENTORY, basicInput.getBusinessUnitId()) );
			sumBookValuesForAverages( totalBookValuesMap, valuesBookOutState.getBookValues() );
		}
	} catch(GBException gbe){
		throw new ApplicationException(gbe);
	}
	return calculateAverageValues( totalBookValuesMap );
}

static List<VDP_GuideBookValue> calculateAverageValues( Map<String, List<VDP_GuideBookValue>> totalBookValues )
{
	List<VDP_GuideBookValue> returnlist = new ArrayList<VDP_GuideBookValue>();
	Iterator<String> it = totalBookValues.keySet().iterator();
	while ( it.hasNext() )
	{
		String key = it.next();
		List<VDP_GuideBookValue> totalList = (List<VDP_GuideBookValue>)totalBookValues.get( key );
		VDP_GuideBookValue averageValue = calculateAverage( totalList );
		returnlist.add( averageValue );
	}
	return returnlist;
}

static VDP_GuideBookValue calculateAverage( List<VDP_GuideBookValue> totalList )
{
	VDP_GuideBookValue returnValue = (VDP_GuideBookValue)totalList.toArray()[0]; // KDL
	// this intializes the fields other than value.
	Iterator<VDP_GuideBookValue> totalsIter = totalList.iterator();
	int totalValue = 0;
	int numberOfValidValues = 0;
	while ( totalsIter.hasNext() )
	{
		VDP_GuideBookValue specificValue = totalsIter.next();
		if ( specificValue.getValue() != null )
		{
			totalValue += specificValue.getValue().intValue();
			numberOfValidValues++;
		}
	}
	if ( numberOfValidValues > 0 )
	{
		returnValue.setValue( new Integer( totalValue / numberOfValidValues ) );
	}
	else
	{
		returnValue.setValue( null );
	}
	return returnValue;
}

private void sumBookValuesForAverages( Map<String, List<VDP_GuideBookValue>> totalBookValues, List<VDP_GuideBookValue> bookValues )
{
	Iterator<VDP_GuideBookValue> it = bookValues.iterator();
	while ( it.hasNext() )
	{
		VDP_GuideBookValue latest = it.next();
		// the getValueType was added so that for kbb, mileage adjusted and
		// final don't get averaged together -dw 11/23/05
		String thirdPartyKey = "" + latest.getThirdPartyCategoryId() + " " + latest.getValueType();
		List<VDP_GuideBookValue> list = totalBookValues.get(thirdPartyKey);
		if(list == null) {
			list = new ArrayList<VDP_GuideBookValue>();
		}
		list.add(latest);
		totalBookValues.put( thirdPartyKey, list ); // KDL - no null
		// checking of the value
		// here. We need to make
		// sure
		// there is an entry of each value type for the guidebook so that we
		// have something to save, even if it is a null.
	}
}

Integer checkMileage( Integer mileage )
{
	if ( mileage == null || mileage.intValue() == Integer.MIN_VALUE )
	{
		return Integer.valueOf(0);
	}
	return mileage;
}

}
