package com.firstlook.batch.bookout.distributed.persistence;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.firstlook.batch.bookout.distributed.model.BookoutMode;
import com.firstlook.batch.bookout.distributed.model.BookoutProcessorRun;
import com.firstlook.batch.bookout.distributed.model.BookoutProvider;


public interface PendingBookoutProcessorRunDAO
{

/**
 * Gets the next queued up bookout processor context regarless of run mode or book
 * @param localHostName
 * @return
 */
public abstract BookoutProcessorRun getPendingBookoutProcessorRun( String localHostName );

/**
 * Gets the next queued up bookout processor job that is incremental regarless of book
 * @param localHostName
 * @return
 */
public abstract BookoutProcessorRun getPendingBookoutProcessorRun( String localHostName, BookoutMode mode );

/**
 * Gets the next queued up bookout processor job with thirdPartyId regarless of run mode
 * @param localHostName
 * @return
 */
public abstract BookoutProcessorRun getPendingBookoutProcessorRun( String localHostName, BookoutProvider provider );
/**
 * The stored proc is a 1 liner (implicit atomicity), but we can use java also to lock the table with <code>synchronized</code> if need be.
 * -bf Jan. 10, 2006
 * 
 * @param localHostName
 * @param mode TODO
 * @param provider TODO
 * @return a Map representing the return variable/value pair.
 */
public abstract BookoutProcessorRun getPendingBookoutProcessorRun( String localHostName, BookoutMode mode, BookoutProvider provider );

@Transactional(propagation=Propagation.REQUIRED)
public abstract void saveOrUpdate( BookoutProcessorRun bookoutProcessorRunLog );
}