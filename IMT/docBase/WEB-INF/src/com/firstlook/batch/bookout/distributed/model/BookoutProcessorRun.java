package com.firstlook.batch.bookout.distributed.model;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class BookoutProcessorRun implements Serializable
{

private static final long serialVersionUID = -5675015396212526937L;
private Integer bookoutProcessorRunId;
private Integer businessUnitId;
private Integer thirdPartyId;
private BookoutProcessorMode bookoutProcessorMode;
private BookoutProcessorStatus bookoutProcessorStatus;
private Date startTime;
private Date endTime;
private Integer successfulBookouts;
private Integer failedBookouts;

public BookoutProcessorMode getBookoutProcessorMode()
{
	return bookoutProcessorMode;
}

public void setBookoutProcessorMode( BookoutProcessorMode bookoutProcessorMode )
{
	this.bookoutProcessorMode = bookoutProcessorMode;
}

public Integer getBookoutProcessorRunId()
{
	return bookoutProcessorRunId;
}

public void setBookoutProcessorRunId( Integer bookoutProcessorRunId )
{
	this.bookoutProcessorRunId = bookoutProcessorRunId;
}

public BookoutProcessorStatus getBookoutProcessorStatus()
{
	return bookoutProcessorStatus;
}

public void setBookoutProcessorStatus( BookoutProcessorStatus bookoutProcessorStatus )
{
	this.bookoutProcessorStatus = bookoutProcessorStatus;
}

public Integer getBusinessUnitId()
{
	return businessUnitId;
}

public void setBusinessUnitId( Integer businessUnitId )
{
	this.businessUnitId = businessUnitId;
}

public Date getEndTime()
{
	return endTime;
}

public void setEndTime( Date endTime )
{
	this.endTime = endTime;
}

public Date getStartTime()
{
	return startTime;
}

public void setStartTime( Date startTime )
{
	this.startTime = startTime;
}

public Integer getThirdPartyId()
{
	return thirdPartyId;
}

public void setThirdPartyId( Integer thirdPartyId )
{
	this.thirdPartyId = thirdPartyId;
}

public String toString()
{
	ToStringBuilder.setDefaultStyle( ToStringStyle.SIMPLE_STYLE );
	return ToStringBuilder.reflectionToString( this );
}

public Integer getFailedBookouts()
{
	return failedBookouts;
}

public void setFailedBookouts( Integer failedBookouts )
{
	this.failedBookouts = failedBookouts;
}

public Integer getSuccessfulBookouts()
{
	return successfulBookouts;
}

public void setSuccessfulBookouts( Integer successfulBookouts )
{
	this.successfulBookouts = successfulBookouts;
}

}
