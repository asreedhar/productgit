package com.firstlook.batch.bookout.distributed;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import biz.firstlook.transact.persist.persistence.IInventoryBookOutDAO;
import biz.firstlook.transact.persist.persistence.InventoryBookOutStoredProcedure;

import com.firstlook.batch.bookout.BookoutBatch;
import com.firstlook.batch.bookout.IBookoutBatch;
import com.firstlook.batch.bookout.BookoutBatch.BookoutUpdateCount;
import com.firstlook.batch.bookout.distributed.model.BookoutMode;
import com.firstlook.batch.bookout.distributed.model.BookoutProcessorArgs;
import com.firstlook.batch.bookout.distributed.model.BookoutProcessorMode;
import com.firstlook.batch.bookout.distributed.model.BookoutProcessorRun;
import com.firstlook.batch.bookout.distributed.model.BookoutProcessorStatus;
import com.firstlook.batch.bookout.distributed.model.BookoutProvider;
import com.firstlook.batch.bookout.distributed.persistence.PendingBookoutProcessorRunDAO;
import com.firstlook.service.bookout.BookOutService;
import com.firstlook.service.bookout.inventory.InventoryBookOutService;
import com.firstlook.service.dealer.IDealerService;

/**
 * This class sets up a bookout run.
 * 
 * @author bfung
 */
public class BookoutProcessorWrapperWorker implements Runnable
{
private static Logger logger = Logger.getLogger(BookoutProcessorWrapperWorker.class);

private String serverName;
private PendingBookoutProcessorRunDAO pendingBookoutProcessorRunDAO;
private ConfigurableApplicationContext applicationContext;
private BookoutProcessorArgs args;

private Timer timeoutTimer;
private AtomicBoolean timedout = new AtomicBoolean(false);

/**
 * @param args
 * @throws UnknownHostException
 */
public BookoutProcessorWrapperWorker(BookoutProcessorArgs args) throws UnknownHostException {
	if (logger.isInfoEnabled())	{
		logger.info("Processor Initializing");
	}
	
	this.applicationContext = new ClassPathXmlApplicationContext( 
			new String[] { "applicationContext-hibernate.xml"
						 , "applicationContext-bookoutProcessor.xml"
						 , "applicationContext-transact-persist.xml"
						 , "applicationContext-vehicleDescription.xml" });
	this.args = args;
	this.serverName = InetAddress.getLocalHost().getHostName();
	this.pendingBookoutProcessorRunDAO = (PendingBookoutProcessorRunDAO)applicationContext.getBean("pendingBookoutProcessorRunDAO");
	
	if (logger.isInfoEnabled()) {
		logger.info("Processor Initialized for " + args.toString());
	}
}

public void run() {
	long start = System.currentTimeMillis();
	
	Date timeoutDate = args.getTimeoutDate(); 
	if(timeoutDate != null) {
		timeoutTimer = new Timer("Bookout Processor Timeout timer", true);
		timeoutTimer.schedule(new TimeoutTask(), timeoutDate);
		if(logger.isInfoEnabled()) {
			logger.info(MessageFormat.format("Processor set to timeout at {0}", timeoutDate));
		}
	}
	
	BookoutProcessorRun bookoutProcessorRun = pendingBookoutProcessorRunDAO.getPendingBookoutProcessorRun( serverName, args.getBookoutMode(), args.getBookoutProvider() );
	while ( bookoutProcessorRun != null && !timedout.get())	{
		try {
			
			if ( logger.isInfoEnabled() ) {
				logger.info( "Starting to process Run Id " + bookoutProcessorRun.getBookoutProcessorRunId());
			}
			//do the bookout
			Integer dealerId = bookoutProcessorRun.getBusinessUnitId();
			boolean incrementalMode = bookoutProcessorRun.getBookoutProcessorMode().equals( BookoutProcessorMode.INCREMENTAL_MODE );
			IBookoutBatch bookoutBatch = newIBookoutBatchInstance();
			Map<BookoutUpdateCount, Integer> updateCounts = bookoutBatch.execute(dealerId, bookoutProcessorRun.getThirdPartyId(), incrementalMode);
			bookoutBatch = null; //gc this motha!
			
			//record results summary
			if ( logger.isInfoEnabled() ) {
				logger.info( "Run Id: " + bookoutProcessorRun.getBookoutProcessorRunId() + ", Business Unit Id: " + dealerId.toString() + " " + "Successful: " + updateCounts.get( BookoutUpdateCount.SUCCESSFUL ) + " , Failed " + updateCounts.get( BookoutUpdateCount.FAILED ));
			}
			bookoutProcessorRun.setBookoutProcessorStatus( BookoutProcessorStatus.SUCCESSFUL );
			bookoutProcessorRun.setSuccessfulBookouts( updateCounts.get( BookoutUpdateCount.SUCCESSFUL ) );
			bookoutProcessorRun.setFailedBookouts( updateCounts.get( BookoutUpdateCount.FAILED ) );
		} catch ( Exception e ) {
			bookoutProcessorRun.setBookoutProcessorStatus( BookoutProcessorStatus.FAILURE );
			logger.error( e.getMessage() );
		}
		finally {
			if ( bookoutProcessorRun != null ) {
				//handling exception while updating bookout processor run log table. Case 92935
				//This was causing bookout processor execution to end abruptly whenever an exception was thrown
				try {
					pendingBookoutProcessorRunDAO.saveOrUpdate( bookoutProcessorRun );
					if ( logger.isInfoEnabled() ) {
						logger.info("Completed processing of Run Id " + bookoutProcessorRun.getBookoutProcessorRunId() + " successfully");
					}

				} catch (Exception e) {
					if ( logger.isInfoEnabled() ) {
						logger.info("Failed to update Bookout Processor Run Log : Run Id = " + bookoutProcessorRun.getBookoutProcessorRunId() + " for business unit " + bookoutProcessorRun.getBusinessUnitId() + " with status " + bookoutProcessorRun.getBookoutProcessorStatus());
					}
					logger.error("Error : ", e);
				}
			}
		}
		try {
			bookoutProcessorRun = getPendingBookoutProcessorRun( serverName, args.getBookoutMode(), args.getBookoutProvider() );
		} catch (Exception e) {
			if ( logger.isInfoEnabled() ) {
				logger.info("Failed to get next pending bookouts ");
			}
			logger.error("Error : ", e);
		}
	}
	
	if ( logger.isInfoEnabled() ) {
		if(timedout.get()) {
			logger.info("Timeout was signalled, shutting down...");
		} else {
			logger.info("No more bookouts queued for " + args.toString());
			logger.info( "Bookouts finished, took " + (System.currentTimeMillis() - start) + " ms, shutting down...");
		}
	}
	 
	applicationContext.close();
}

private BookoutProcessorRun getPendingBookoutProcessorRun(String localHostName, BookoutMode mode, BookoutProvider provider)	{
	
	BookoutProcessorRun bookoutProcessorRun = null;
	try {
		bookoutProcessorRun = pendingBookoutProcessorRunDAO.getPendingBookoutProcessorRun( serverName, args.getBookoutMode(), args.getBookoutProvider() );
	} catch (Exception e) {
		if ( logger.isInfoEnabled() ) {
			logger.info("Failed to get next pending bookouts trying again ");
		}
		logger.error("Error : ", e);
		bookoutProcessorRun = pendingBookoutProcessorRunDAO.getPendingBookoutProcessorRun( serverName, args.getBookoutMode(), args.getBookoutProvider() );
	}
	
	return bookoutProcessorRun;
}


private IBookoutBatch newIBookoutBatchInstance() {
	BookOutService bookOutService = (BookOutService)applicationContext.getBean("bookOutService");
	IInventoryBookOutDAO inventoryBookOutDAO = (IInventoryBookOutDAO)applicationContext.getBean("inventoryBookOutDAO");
	InventoryBookOutStoredProcedure inventoryBookOutStoredProcedure = (InventoryBookOutStoredProcedure)applicationContext.getBean("inventoryBookOutStoredProcedure");
	InventoryBookOutService inventoryBookOutService = (InventoryBookOutService)applicationContext.getBean("inventoryBookOutService");
	IDealerService dealerService = (IDealerService)applicationContext.getBean("dealerService");
	SessionFactory sessionFactory = (SessionFactory)applicationContext.getBean("sessionFactory");
	return new BookoutBatch(bookOutService, inventoryBookOutDAO, inventoryBookOutStoredProcedure, inventoryBookOutService, dealerService, sessionFactory); 
}

private class TimeoutTask extends TimerTask {
	public void run() {
		timedout.set(true);
		if(logger.isInfoEnabled()) {
			logger.info("Signalling timeout.");
		}
	}
}

}
