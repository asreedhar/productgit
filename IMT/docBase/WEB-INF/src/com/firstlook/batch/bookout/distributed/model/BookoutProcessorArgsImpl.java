package com.firstlook.batch.bookout.distributed.model;

import java.util.Calendar;
import java.util.Date;

public class BookoutProcessorArgsImpl implements BookoutProcessorArgs {

	private final BookoutMode mode;
	private final BookoutProvider provider;
	private final Date timeoutDate;

	/**
	 * @param mode
	 * @param provider
	 * @param timeout a value greater than zero indicates the timeout in hours or a value equal or less than zero indicating no timeout.
	 */
	public BookoutProcessorArgsImpl(BookoutMode mode, BookoutProvider provider, int timeout) {
		if(mode == null || provider == null)
			throw new IllegalStateException("BookoutMode or BookoutProvider cannot be null.");
		
		this.mode = mode;
		this.provider = provider;
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, timeout);
		if(timeout > 0) {
			timeoutDate = cal.getTime();
		} else {
			timeoutDate = null;
		}
	}
	
	public BookoutMode getBookoutMode() {
		return mode;
	}

	public BookoutProvider getBookoutProvider() {
		return provider;
	}

	public Date getTimeoutDate() {
		return timeoutDate;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(mode.name()).append(" mode, using ");
		sb.append(provider.name()).append(" book(s)");
		return sb.toString();
	}
}
