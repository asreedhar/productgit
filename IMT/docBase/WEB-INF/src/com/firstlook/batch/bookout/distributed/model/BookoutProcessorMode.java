package com.firstlook.batch.bookout.distributed.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class BookoutProcessorMode implements Serializable
{

private static final long serialVersionUID = -8732185666027686392L;
public static final BookoutProcessorMode FULL_BOOKOUT_MODE = new BookoutProcessorMode( new Integer( 0 ), "Full Bookout" );
public static final BookoutProcessorMode INCREMENTAL_MODE = new BookoutProcessorMode( new Integer( 1 ), "Incremental" );

private Integer bookoutProcessorModeId;
private String bookoutProcessorModeDescription;

public BookoutProcessorMode()
{
	super();
}

public BookoutProcessorMode( Integer bookoutProcessorModeId, String description )
{
	super();
	this.bookoutProcessorModeId = bookoutProcessorModeId;
	this.bookoutProcessorModeDescription = description;
}

public Integer getBookoutProcessorModeId()
{
	return bookoutProcessorModeId;
}

public void setBookoutProcessorModeId( Integer bookoutProcessorModeId )
{
	this.bookoutProcessorModeId = bookoutProcessorModeId;
}

public String getBookoutProcessorModeDescription()
{
	return bookoutProcessorModeDescription;
}

public void setBookoutProcessorModeDescription( String description )
{
	this.bookoutProcessorModeDescription = description;
}

public boolean equals( Object obj )
{
	return EqualsBuilder.reflectionEquals( this, obj );
}

public int hashCode()
{
	return HashCodeBuilder.reflectionHashCode( this );
}

public String toString()
{
	return ToStringBuilder.reflectionToString( this );
}

}
