package com.firstlook.batch.bookout.distributed.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.CallableStatementCreatorFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlReturnResultSet;

import com.firstlook.batch.bookout.distributed.model.BookoutMode;
import com.firstlook.batch.bookout.distributed.model.BookoutProcessorRun;
import com.firstlook.batch.bookout.distributed.model.BookoutProcessorStatus;
import com.firstlook.batch.bookout.distributed.model.BookoutProvider;

public class PendingBookoutProcessorRunJdbcDAO extends JdbcTemplate implements PendingBookoutProcessorRunDAO
{

public static final int SUCCESS_RETURN_CODE = 0;
public static final String RESULT_SET = "ResultSet";
public static final String RETURN_CODE = "@RC";

public static final String IN_PARAM_SERVER_NAME = "@ServerName";
public static final String OUT_PARAM_BOOKOUT_PROCESSOR_RUN_ID = "@BookoutProcessorRunId";
public static final String IN_PARAM_MODE_ID = "@BookoutProcessorModeId";
public static final String IN_PARAM_THIRD_PARTY_ID = "@ThirdPartyID";

private List<SqlParameter> storedProcedureParameters;
private BookoutProcessorRunLogDAO bookoutProcessorRunLogDAO;

public PendingBookoutProcessorRunJdbcDAO()
{
	storedProcedureParameters = new ArrayList<SqlParameter>();

	/*
	 * This is a set of return values. Typically when doing a prepared statment, you get a ResultSet object. When getting Out parameters from a
	 * sproc, you get a ResultSet object AND the return code, and that is why this is needed. THIS MUST BE ADDED FIRST!
	 */
	storedProcedureParameters.add( new SqlReturnResultSet( RESULT_SET, new RowMapper()
	{
		public Object mapRow( ResultSet rs, int rowNum ) throws SQLException
		{
			return null;
		}
	} ) );

	// This stores the return code from a stored proc
	storedProcedureParameters.add( new SqlOutParameter( RETURN_CODE, Types.INTEGER ) );

	// Parameters of a stored proc
	storedProcedureParameters.add( new SqlParameter( IN_PARAM_SERVER_NAME, Types.VARCHAR ) );
	storedProcedureParameters.add( new SqlOutParameter( OUT_PARAM_BOOKOUT_PROCESSOR_RUN_ID, Types.INTEGER ) );
	storedProcedureParameters.add( new SqlParameter( IN_PARAM_MODE_ID, Types.INTEGER ) );
	storedProcedureParameters.add( new SqlParameter( IN_PARAM_THIRD_PARTY_ID, Types.INTEGER) );
}

/* (non-Javadoc)
 * @see com.firstlook.batch.bookout.distributed.persistence.PendingBookoutProcessorRunDAO#getPendingBookoutProcessorRun(java.lang.String)
 */
@SuppressWarnings("unchecked")
public BookoutProcessorRun getPendingBookoutProcessorRun( String localHostName, BookoutMode mode, BookoutProvider provider )
{
	Map<String, Object> storedProcParametersValues = new HashMap<String, Object>();
	storedProcParametersValues.put( IN_PARAM_SERVER_NAME, localHostName );
	storedProcParametersValues.put( IN_PARAM_MODE_ID, mode.getId() );
	storedProcParametersValues.put( IN_PARAM_THIRD_PARTY_ID, provider.getId() );

	// the statement to execute
	CallableStatementCreatorFactory cscf = new CallableStatementCreatorFactory( "{ ? = call GetPendingBookoutProcessorRunID ( ?, ?, ?, ? ) }",
																				storedProcedureParameters );

	CallableStatementCreator csc = cscf.newCallableStatementCreator( storedProcParametersValues );
	
	Map results = call( csc, storedProcedureParameters );

	Integer bookoutProcessorRunId = null;
	if ( results.containsKey( OUT_PARAM_BOOKOUT_PROCESSOR_RUN_ID ) )
	{
		bookoutProcessorRunId = (Integer) results.get( OUT_PARAM_BOOKOUT_PROCESSOR_RUN_ID );
		if ( bookoutProcessorRunId == null || bookoutProcessorRunId <= 0 )
		{
			return null;
		}
		else
		{
			BookoutProcessorRun run = bookoutProcessorRunLogDAO.getById( bookoutProcessorRunId );
			run.setBookoutProcessorStatus( BookoutProcessorStatus.IN_PROGRESS );
			run.setStartTime( new Date() );
			saveOrUpdate( run ); 
			return run; 
		}
	}
	return null;
}

public BookoutProcessorRun getPendingBookoutProcessorRun(String localHostName) {
	return getPendingBookoutProcessorRun(localHostName, null, null);
}

public BookoutProcessorRun getPendingBookoutProcessorRun(String localHostName,
		BookoutMode isIncremental) {
	return getPendingBookoutProcessorRun(localHostName, isIncremental, null);
}

public BookoutProcessorRun getPendingBookoutProcessorRun(String localHostName,
		BookoutProvider provider) {
	return getPendingBookoutProcessorRun(localHostName, null, provider);
}

public void saveOrUpdate( BookoutProcessorRun bookoutProcessorRunLog ) {
	bookoutProcessorRunLog.setEndTime( new Date() );
	bookoutProcessorRunLogDAO.saveOrUpdate( bookoutProcessorRunLog );
}

public BookoutProcessorRunLogDAO getBookoutProcessorRunLogDAO() {
	return bookoutProcessorRunLogDAO;
}

public void setBookoutProcessorRunLogDAO( BookoutProcessorRunLogDAO bookoutProcessorRunLogDAO ) {
	this.bookoutProcessorRunLogDAO = bookoutProcessorRunLogDAO;
}

}
