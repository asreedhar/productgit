package com.firstlook.batch.bookout.distributed.persistence;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.firstlook.batch.bookout.distributed.model.BookoutProcessorRun;

public class BookoutProcessorRunLogHibernateDAO extends HibernateDaoSupport implements BookoutProcessorRunLogDAO
{

public BookoutProcessorRun getById( Integer bookoutProcessorRunId )
{
	
	return (BookoutProcessorRun)getHibernateTemplate().get( BookoutProcessorRun.class, bookoutProcessorRunId );
}

public void saveOrUpdate( BookoutProcessorRun bookoutProcessorRunLog )
{
	getHibernateTemplate().saveOrUpdate( bookoutProcessorRunLog );
}

}
