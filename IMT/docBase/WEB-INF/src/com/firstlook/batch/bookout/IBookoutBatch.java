package com.firstlook.batch.bookout;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.Map;

import com.firstlook.batch.bookout.BookoutBatch.BookoutUpdateCount;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;

public interface IBookoutBatch
{

/**
 * 
 * @throws DatabaseException
 * @throws InterruptedException
 * @throws ApplicationException
 * @throws InterruptedIOException
 * @throws IOException
 */
public abstract Map<BookoutUpdateCount, Integer> execute(Integer dealerId, Integer thirdPartyId, boolean incrementalMode) throws DatabaseException, InterruptedException, ApplicationException, InterruptedIOException, IOException;

}