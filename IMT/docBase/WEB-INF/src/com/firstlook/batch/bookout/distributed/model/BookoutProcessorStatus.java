package com.firstlook.batch.bookout.distributed.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class BookoutProcessorStatus implements Serializable
{

private static final long serialVersionUID = 1639911583212338943L;
// To be replaced by Java 5.0 enum(s) -bf Jan. 11, 2006
public static final BookoutProcessorStatus PENDING = new BookoutProcessorStatus( new Integer( 0 ), "Pending" );
public static final BookoutProcessorStatus LOADED = new BookoutProcessorStatus( new Integer( 1 ), "Loaded" );
public static final BookoutProcessorStatus IN_PROGRESS = new BookoutProcessorStatus( new Integer( 2 ), "In Progress" );
public static final BookoutProcessorStatus SUCCESSFUL = new BookoutProcessorStatus( new Integer( 3 ), "Successful" );
public static final BookoutProcessorStatus FAILURE = new BookoutProcessorStatus( new Integer( 4 ), "Failure" );

private Integer bookoutProcessorStatusId;
private String bookoutProcessorStatusDescription;

public BookoutProcessorStatus()
{
	super();
}

public BookoutProcessorStatus( Integer bookoutProcessorStatusId, String description )
{
	super();
	this.bookoutProcessorStatusId = bookoutProcessorStatusId;
	this.bookoutProcessorStatusDescription = description;
}

public Integer getBookoutProcessorStatusId()
{
	return bookoutProcessorStatusId;
}

public void setBookoutProcessorStatusId( Integer bookoutProcessorStatusId )
{
	this.bookoutProcessorStatusId = bookoutProcessorStatusId;
}

public String getBookoutProcessorStatusDescription()
{
	return bookoutProcessorStatusDescription;
}

public void setBookoutProcessorStatusDescription( String description )
{
	this.bookoutProcessorStatusDescription = description;
}

public boolean equals( Object obj )
{
    return EqualsBuilder.reflectionEquals(this, obj);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(this);
}

public String toString()
{
	return ToStringBuilder.reflectionToString( this );
}

}
