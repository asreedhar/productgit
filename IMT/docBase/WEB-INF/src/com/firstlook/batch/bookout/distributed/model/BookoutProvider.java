package com.firstlook.batch.bookout.distributed.model;

/**
 * Note: the BookoutProvider.ANY has a "null" id 
 * @author bfung
 *
 */
public enum BookoutProvider {
	ANY(-1), BLACKBOOK(1), NADA(2), KBB(3), GALVES(4);
	
	private final Integer id;
	private BookoutProvider(Integer id) {
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}
}
