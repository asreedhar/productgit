package com.firstlook.batch.bookout.distributed.model;

/**
 * BookoutMode.FULL indicates that every inventory item is to be booked out.
 * 
 * BookoutMode.INCREMENTAL indicates that only inventory items with BookoutRequired > 0 (true) is booked out.
 * 
 * BookoutMode.ANY will cause the processor to pick up FULL or INCREMENTALS.
 * 
 * @author bfung
 *
 */
public enum BookoutMode {
	ANY(-1), FULL(0), INCREMENTAL(1);
	
	private final Integer id;
	private BookoutMode(Integer id) {
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}
}
