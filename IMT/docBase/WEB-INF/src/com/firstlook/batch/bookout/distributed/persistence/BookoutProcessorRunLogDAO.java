package com.firstlook.batch.bookout.distributed.persistence;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.firstlook.batch.bookout.distributed.model.BookoutProcessorRun;

public interface BookoutProcessorRunLogDAO
{

@Transactional(propagation=Propagation.REQUIRED)
public abstract void saveOrUpdate( BookoutProcessorRun bookoutProcessorRunLog );

public abstract BookoutProcessorRun getById( Integer bookoutProcessorRunId );

}
