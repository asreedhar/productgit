package com.firstlook.batch;

import java.util.ArrayList;
import java.util.Iterator;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import biz.firstlook.commons.util.BaseScriptJob;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.NewCarScoreCard;
import com.firstlook.entity.UsedCarScoreCard;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.dealer.IDealerDAO;
import com.firstlook.service.dealer.IDealerService;
import com.firstlook.service.scorecard.ScoreCardFactory;
import com.firstlook.service.scorecard.ScoreCardService;

public class ScoreCardProcessor extends BaseScriptJob
{

public static String applicationContextLocation = "applicationContext-hibernate.xml";
public static String applicationContextPojoLocation = "applicationContext-pojo.xml";

private ApplicationContext applicationContext;
private IDealerDAO dealerDAO;

private int updateCount;
private Integer businessUnitId;
private boolean allDealers;
private IDealerService dealerService;

public static void main( String[] args ) throws Throwable
{
    ApplicationContext applicationContextHibernate = new ClassPathXmlApplicationContext( ScoreCardProcessor.applicationContextLocation );
    ApplicationContext applicationContext = new ClassPathXmlApplicationContext( new String[] { ScoreCardProcessor.applicationContextPojoLocation }, applicationContextHibernate );
    ScoreCardProcessor proc = new ScoreCardProcessor( applicationContext );
    proc.executeJob( args );
}

public ScoreCardProcessor( ApplicationContext applicationContext )
{
    super();
    this.applicationContext = applicationContext;
    this.dealerDAO = (IDealerDAO)applicationContext.getBean( "dealerDAO" );
    this.dealerService = (IDealerService)applicationContext.getBean( "dealerService" );
}

protected String getJobStatus()
{
    return "Updated " + updateCount + " dealers";
}

protected String getUsage()
{
    return "usage: ScoreCardProcessor [-d <businessUnitId>] [-a]";
}

public void start() throws Throwable
{

    ArrayList businessUnitIds = initializeDealers();

    for (int i = 0; i < businessUnitIds.size(); i++)
    {
        // IMT - businessUnit
        Integer dealerId = (Integer)businessUnitIds.get(i);

        ScoreCardFactory scoreCardFactory = new ScoreCardFactory( applicationContext, dealerId.intValue() );
        ScoreCardService scoreCardService = new ScoreCardService();

        // IMT - Read Used Score Card
        UsedCarScoreCard returnedUsedCarScoreCard = scoreCardService.retrieveActiveUsedCarScoreCard( dealerId );
        if ( returnedUsedCarScoreCard != null )
        {
            // IMT - Save used score card
            returnedUsedCarScoreCard.setActive( false );
            scoreCardService.saveOrUpdateUsed( returnedUsedCarScoreCard );
        }

        // IMT - AET
        UsedCarScoreCard usedCarScoreCard = scoreCardFactory.createUsedCarScoreCard();
        usedCarScoreCard.setBusinessUnitId( dealerId );

        // IMT
        scoreCardService.saveOrUpdateUsed( usedCarScoreCard );

        // IMT - Read New ScoreCard
        NewCarScoreCard returnedNewCarScoreCard = scoreCardService.retrieveActiveNewCarScoreCard( dealerId );
        if ( returnedNewCarScoreCard != null )
        {
            // IMT - Save new score card
            returnedNewCarScoreCard.setActive( false );
            scoreCardService.saveOrUpdateNew( returnedNewCarScoreCard );
        }

        // AET - IMT
        NewCarScoreCard newCarScoreCard = scoreCardFactory.createNewCarScoreCard();
        newCarScoreCard.setBusinessUnitId( dealerId );

        scoreCardService.saveOrUpdateNew( newCarScoreCard );

        updateCount++;
    }
}

private ArrayList initializeDealers() throws ApplicationException
{
    ArrayList businessUnitIds = new ArrayList();
    if ( businessUnitId != null )
    {
        businessUnitIds.add(  businessUnitId );
    }
    else if ( allDealers )
    {
        Iterator dealers = dealerService.retrieveActiveDealers().iterator();
        while (dealers.hasNext()) {
            Dealer dealer = (Dealer)dealers.next();
            businessUnitIds.add(dealer.getBusinessUnitId());
        }
    }
    else
    {
        throw new ApplicationException( "Either -d or -a must be specified on command line." );
    }
    
    return businessUnitIds;

}

public void digestArguments()
{
    String dealerGroupIdStr = getOptions().getStringOption( OptionsParser.OPT_DEALERGROUPID, null );
    try
    {
        this.businessUnitId = new Integer( Integer.parseInt( dealerGroupIdStr ) );
    }
    catch ( NumberFormatException e )
    {
        businessUnitId = null;
    }
    this.allDealers = getOptions().getBooleanOption( OptionsParser.OPT_ALL );
}


public IDealerDAO getDealerDAO()
{
    return dealerDAO;
}

public void setDealerDAO( IDealerDAO dealerDAO )
{
    this.dealerDAO = dealerDAO;
}

public ApplicationContext getApplicationContext()
{
    return applicationContext;
}

public void setApplicationContext( ApplicationContext applicationContext )
{
    this.applicationContext = applicationContext;
}

public String getApplicationContextLocation()
{
    return applicationContextLocation;
}

public static void setApplicationContextLocation( String applicationContextLocation )
{
    ScoreCardProcessor.applicationContextLocation = applicationContextLocation;
}

public void setImtDealerService( IDealerService dealerService )
{
    this.dealerService = dealerService;
}

}
