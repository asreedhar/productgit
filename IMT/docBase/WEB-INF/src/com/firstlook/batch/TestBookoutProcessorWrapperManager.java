package com.firstlook.batch;

import junit.framework.TestCase;

import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.firstlook.batch.bookout.distributed.model.BookoutMode;
import com.firstlook.batch.bookout.distributed.model.BookoutProcessorArgs;
import com.firstlook.batch.bookout.distributed.model.BookoutProvider;

public class TestBookoutProcessorWrapperManager extends TestCase {

	public TestBookoutProcessorWrapperManager() {
		super();
	}

	public TestBookoutProcessorWrapperManager(String name) {
		super(name);
	}
	
	public void testBuildContextNoArgs() {
		String[] args = new String[] {};
		Options options = new Options();
		BookoutProcessorArgs context = null;;
		try {
			context = BookoutProcessorWrapperManager.buildContext(options, args);
		} catch (ParseException e) {
			fail("Parsing should not have failed!");
		}
		
		assertEquals(BookoutMode.ANY, context.getBookoutMode());
		assertEquals(BookoutProvider.ANY, context.getBookoutProvider());
	}
	
	public void testBuildContextBookOptionNoArg() {
		String[] args = new String[] {"-b"};
		Options options = new Options();
		BookoutProcessorArgs context = null;
		try {
			context = BookoutProcessorWrapperManager.buildContext(options, args);
		} catch (ParseException e) {
			assertNull(context);
			return;
		}
		fail("Should have thrown a parse exception for not specifying a valid book!");
	}
	
	public void testBuildContextBookOptionBadOption() {
		String[] args = new String[] {"-b", "adf"};
		Options options = new Options();
		BookoutProcessorArgs context = null;
		try {
			context = BookoutProcessorWrapperManager.buildContext(options, args);
		} catch (ParseException e) {
			assertNull(context);
			return;
		}
		fail("Should have thrown a parse exception for not specifying a valid book!");
	}
	
	public void testBuildContextBookOption() {
		String[] args = new String[] {"-b", "blaCkbook"};
		Options options = new Options();
		BookoutProcessorArgs context = null;
		try {
			context = BookoutProcessorWrapperManager.buildContext(options, args);
		} catch (ParseException e) {
			fail("Should have parsed out blackbook");
		}
		
		assertEquals(BookoutMode.ANY, context.getBookoutMode());
		assertEquals(BookoutProvider.BLACKBOOK, context.getBookoutProvider());
	}
	
	public void testBuildContextBookoutMode() {
		String[] args = new String[] {"-m", "increMenTal"};
		Options options = new Options();
		BookoutProcessorArgs context = null;
		try {
			context = BookoutProcessorWrapperManager.buildContext(options, args);
		} catch (ParseException e) {
			fail("Should have parsed out incremental mode");
		}
		
		assertEquals(BookoutMode.INCREMENTAL, context.getBookoutMode());
		assertEquals(BookoutProvider.ANY, context.getBookoutProvider());
	}
	
	public void testBuildContextBookoutModeWithBook() {
		String[] args = new String[] {"-m", "fUll", "-b", "kbB"};
		Options options = new Options();
		BookoutProcessorArgs context = null;
		try {
			context = BookoutProcessorWrapperManager.buildContext(options, args);
		} catch (ParseException e) {
			fail("Should have parsed out incremental mode");
		}
		
		assertEquals(BookoutMode.FULL, context.getBookoutMode());
		assertEquals(BookoutProvider.KBB, context.getBookoutProvider());
	}
}
