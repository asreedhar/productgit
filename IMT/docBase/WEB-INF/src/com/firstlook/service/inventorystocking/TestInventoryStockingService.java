package com.firstlook.service.inventorystocking;

import java.util.Calendar;
import java.util.Date;

import junit.framework.TestCase;

import com.firstlook.entity.InventoryEntity;

public class TestInventoryStockingService extends TestCase
{

private InventoryStockingService service = new InventoryStockingService();

public TestInventoryStockingService( String arg0 )
{
    super(arg0);
}

public void testCalculateDaysSupplyRoundDown()
{
    int daysSupply = service.calculateDaysSupply(5, 45);

    assertEquals("Should be 3", 3, daysSupply);
}

public void testCalculateDaysSupplyRoundUp()
{
    int daysSupply = service.calculateDaysSupply(10, 60);

    assertEquals("Should be 7", 7, daysSupply);
}

public void testDetermineNumDaysUsed()
{
    int numDays = service.determineNumDays(InventoryEntity.USED_CAR);
    assertEquals("Should be 45", 45, numDays);
}

public void testDetermineNumDaysNew()
{
    int numDays = service.determineNumDays(InventoryEntity.NEW_CAR);
    assertEquals("Should be 60", 60, numDays);
}

public void testCreateRetailDate()
{
    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
    calendar.add(Calendar.WEEK_OF_YEAR, -12);

    Date date = service.createRetailDate();

    assertEquals(calendar.getTime(), date);

}

}
