package com.firstlook.service.dealergroup;

import java.util.Collection;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import biz.firstlook.transact.persist.model.BusinessUnitCredential;
import biz.firstlook.transact.persist.model.CredentialType;
import biz.firstlook.transact.persist.persistence.BusinessUnitCredentialDAO;
import biz.firstlook.transact.persist.retriever.UpdateInventoryBookoutLock;

import com.firstlook.entity.DealerGroup;
import com.firstlook.persistence.dealergroup.IDealerGroupDAO;

public class DealerGroupService
{

private IDealerGroupDAO dealerGroupDAO;
private BusinessUnitCredentialDAO businessUnitCredentialDAO;
private TransactionTemplate imtTransactionTemplate;
private UpdateInventoryBookoutLock updateInventoryBookoutLock;

public DealerGroup retrieveByDealerGroupId( int dealerGroupId )
{
	return dealerGroupDAO.findByDealerGroupId( dealerGroupId );
}

public DealerGroup retrieveByDealerId( int dealerId )
{
	return dealerGroupDAO.findByDealerId( dealerId );
}

public Collection retrieveByDealerGroupCode( String dealerGroupCode )
{
	return dealerGroupDAO.findByDealerGroupCode( dealerGroupCode );
}

public Collection retrieveByDealerGroupName( String dealerGroupName )
{
	return dealerGroupDAO.findByDealerGroupName( dealerGroupName );
}

public Collection<DealerGroup> retrieveInDealerGroupIdList( Collection idList )
{
	return dealerGroupDAO.findInDealerGroupIdList( idList );
}

public void saveOrUpdate( DealerGroup dealerGroup )
{
    dealerGroupDAO.saveOrUpdate( dealerGroup );
}

public void delete( DealerGroup dealerGroup )
{
    dealerGroupDAO.delete( dealerGroup );
}

public void saveOrUpdateBusinessUnitCredential( final BusinessUnitCredential businessUnitCredential )
{
	imtTransactionTemplate.execute( new TransactionCallbackWithoutResult()
	{
		@Override
		protected void doInTransactionWithoutResult( TransactionStatus arg0 )
		{
			businessUnitCredentialDAO.saveOrUpdateBusinessUnitCredential( businessUnitCredential );
		}
	} );
}

public BusinessUnitCredential retrieveBusinessUnitCredential( int businessUnitId, CredentialType credentialType )
{
	return businessUnitCredentialDAO.findCredentialByBusinessIdAndCredentialTypeId( new Integer( businessUnitId ), credentialType );	
}

public void save( DealerGroup dealerGroup )
{
    dealerGroupDAO.save( dealerGroup );
}

public void updateInventoryBookoutLock( Integer businessUnitId )
{
	updateInventoryBookoutLock.updateInventoryAppraisalBookoutLock( businessUnitId );
}

public IDealerGroupDAO getDealerGroupDAO()
{
	return dealerGroupDAO;
}


public void setDealerGroupDAO( IDealerGroupDAO dealerGroupDAO )
{
	this.dealerGroupDAO = dealerGroupDAO;
}

public BusinessUnitCredentialDAO getBusinessUnitCredentialDAO()
{
	return businessUnitCredentialDAO;
}

public void setBusinessUnitCredentialDAO( BusinessUnitCredentialDAO businessUnitCredentialDAO )
{
	this.businessUnitCredentialDAO = businessUnitCredentialDAO;
}

public TransactionTemplate getImtTransactionTemplate()
{
	return imtTransactionTemplate;
}

public void setImtTransactionTemplate( TransactionTemplate imtTransactionTemplate )
{
	this.imtTransactionTemplate = imtTransactionTemplate;
}

public UpdateInventoryBookoutLock getUpdateInventoryBookoutLock()
{
	return updateInventoryBookoutLock;
}

public void setUpdateInventoryBookoutLock( UpdateInventoryBookoutLock updateInventoryBookoutLock )
{
	this.updateInventoryBookoutLock = updateInventoryBookoutLock;
}

}
