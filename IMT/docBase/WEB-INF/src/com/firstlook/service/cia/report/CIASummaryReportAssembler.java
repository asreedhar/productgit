package com.firstlook.service.cia.report;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.MultiHashMap;
import org.apache.commons.collections.comparators.ReverseComparator;

import biz.firstlook.cia.model.CIACategory;
import biz.firstlook.cia.model.CIAGroupingItemDetailLevel;
import biz.firstlook.cia.model.CIAGroupingItemDetailType;
import biz.firstlook.cia.model.CIASummary;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.persistence.IDealerPreferenceDAO;

import com.firstlook.persistence.cia.ICIABuyingPlanReportDAO;

public class CIASummaryReportAssembler implements ICIASummaryReportAssembler
{

private CIASummaryReportDAO ciaSummaryReportDAO;
private IDealerPreferenceDAO dealerPrefDAO;
private ICIABuyingPlanReportDAO ciaBuyingPlanReportDAO;

public CIASummary retrieveCIASummary( Integer businessUnitId, Integer ciaSummaryStatusId )
{
	return getCiaSummaryReportDAO().retrieveCIASummary( businessUnitId, ciaSummaryStatusId );
}

public List retrieveCIASummaryPageDisplayData( Integer dealerId, Integer segmentId, CIACategory ciaCategory, List actualInventoryItems )
{
	MultiHashMap actualInventoryByYear = CIAReportingUtility.mapInventoryByYear( actualInventoryItems );
	MultiHashMap actualInventoryByGroupingDescriptionId = CIAReportingUtility.mapInventoryByGroupingDescription( actualInventoryItems );
	List ciaSummaryDisplayDatas = new ArrayList();

	List ciaSummaryReportDatas = getCiaSummaryReportDAO().retrieveCIASummaryDisplayData( dealerId, segmentId,
																							ciaCategory.getCiaCategoryId() );
	DealerPreference dealerPreference = dealerPrefDAO.findByBusinessUnitId(dealerId);

	Map ciaSummaryGroupingItemToDetails = new HashMap();
	if ( ciaCategory.equals( CIACategory.MARKET_PERFORMERS ) )
	{
		Iterator ciaSummaryReportDataIterator = ciaSummaryReportDatas.iterator();
		while ( ciaSummaryReportDataIterator.hasNext() )
		{
			Object[] reportData = (Object[])ciaSummaryReportDataIterator.next();

			String ciaCategoryString = (String)reportData[0];
			Integer groupingDescriptionId = (Integer)reportData[1];
			String groupingDescriptionString = (String)reportData[2];
			String segmentString = (String)reportData[7];
			Double valuation = (Double)reportData[8];

			CIASummaryMarketPerformersDisplayData ciaSummaryDisplayData = null;

			if ( !ciaSummaryGroupingItemToDetails.containsKey( groupingDescriptionId ) )
			{
				ciaSummaryDisplayData = new CIASummaryMarketPerformersDisplayData();

				ciaSummaryDisplayData.setCategoryString( ciaCategoryString );
				ciaSummaryDisplayData.setGroupingDescriptionId( groupingDescriptionId );
				ciaSummaryDisplayData.setGroupingDescriptionString( groupingDescriptionString );
				ciaSummaryDisplayData.setSegmentId( segmentId );
				ciaSummaryDisplayData.setSegmentString( segmentString );
				ciaSummaryDisplayData.setValuation( valuation );
			}
			else
			{
				ciaSummaryDisplayData = (CIASummaryMarketPerformersDisplayData)ciaSummaryGroupingItemToDetails.get( groupingDescriptionId );
			}

			Integer ciaGroupingItemDetailUnits = ( (Integer)reportData[5] );
			CIAGroupingItemDetailType ciaGroupingItemDetailType = (CIAGroupingItemDetailType)reportData[4];
			Double marketShare = (Double)reportData[6];

			if ( ciaGroupingItemDetailType.equals( CIAGroupingItemDetailType.OPTIMAL_STOCKING_LEVEL ) )
			{
				ciaSummaryDisplayData.setMarketShare( marketShare );
			}
			else if ( ciaGroupingItemDetailType.equals( CIAGroupingItemDetailType.BUY ) )
			{
				int plannedToBuyTotal = ciaSummaryDisplayData.getPlannedToBuyTotal().intValue();
				ciaSummaryDisplayData.setPlannedToBuyTotal( new Integer( plannedToBuyTotal += ciaGroupingItemDetailUnits.intValue() ) );
			}
			else
			{
				// shouldn't get here.
			}

			ciaSummaryGroupingItemToDetails.put( ciaSummaryDisplayData.getGroupingDescriptionId(), ciaSummaryDisplayData );
		}
	}
	else
	{
		Iterator ciaSummaryReportDataIterator = ciaSummaryReportDatas.iterator();
		while ( ciaSummaryReportDataIterator.hasNext() )
		{
			Object[] reportData = (Object[])ciaSummaryReportDataIterator.next();

			String ciaCategoryString = (String)reportData[0];
			Integer groupingDescriptionId = (Integer)reportData[1];
			String groupingDescriptionString = (String)reportData[2];
			String segmentString = (String)reportData[7];
			Double valuation = (Double)reportData[8];
			Integer level = (Integer)reportData[9];

			CIASummaryYearRecommendationDisplayData ciaSummaryDisplayData = null;
			if ( !ciaSummaryGroupingItemToDetails.containsKey( groupingDescriptionId ) )
			{
				ciaSummaryDisplayData = new CIASummaryYearRecommendationDisplayData();

				ciaSummaryDisplayData.setCategoryString( ciaCategoryString );
				ciaSummaryDisplayData.setGroupingDescriptionId( groupingDescriptionId );
				ciaSummaryDisplayData.setGroupingDescriptionString( groupingDescriptionString );
				ciaSummaryDisplayData.setTargetInventory( new Integer( 0 ) );
				ciaSummaryDisplayData.setPlannedToBuyTotal( new Integer( 0 ) );
				ciaSummaryDisplayData.setSegmentId( segmentId );
				ciaSummaryDisplayData.setSegmentString( segmentString );
				ciaSummaryDisplayData.setValuation( valuation );
			}
			else
			{
				ciaSummaryDisplayData = (CIASummaryYearRecommendationDisplayData)ciaSummaryGroupingItemToDetails.get( groupingDescriptionId );
			}

			Integer ciaGroupingItemDetailUnits = ( (Integer)reportData[5] );
			String ciaGroupingItemDetailValue = (String)reportData[3];

			ciaSummaryDisplayData.setUnitsInStock( CIAReportingUtility.retrieveUnitsInStockForGroupDescription(
																												groupingDescriptionId.toString(),
																												actualInventoryByGroupingDescriptionId ) );

			CIAGroupingItemDetailType ciaGroupingItemDetailType = (CIAGroupingItemDetailType)reportData[4];

			if ( ciaGroupingItemDetailType.equals( CIAGroupingItemDetailType.OPTIMAL_STOCKING_LEVEL ) )
			{
				int targetInventory = ciaSummaryDisplayData.getTargetInventory().intValue() + ciaGroupingItemDetailUnits.intValue();
				ciaSummaryDisplayData.setTargetInventory( new Integer( targetInventory ) );
			}
			else if ( ciaGroupingItemDetailType.equals( CIAGroupingItemDetailType.BUY ) )
			{
				int plannedToBuy;
				if ( ciaGroupingItemDetailUnits != null )
				{
					plannedToBuy = ciaSummaryDisplayData.getPlannedToBuyTotal().intValue() + ciaGroupingItemDetailUnits.intValue();
				}
				else
				{
					ciaGroupingItemDetailUnits = new Integer( 0 );
					plannedToBuy = ciaSummaryDisplayData.getPlannedToBuyTotal().intValue();
				}
				ciaSummaryDisplayData.setPlannedToBuyTotal( new Integer( plannedToBuy ) );
			}
			else
			{
				// shouldn't get here
			}

			// Create the table with "understocked model year" and "planned to
			// buy" data.
			CIASummaryTableDisplayPair ciaSummaryTableDisplayPair = constructCiaSummaryTableDisplayPair( dealerId, dealerPreference,
																											groupingDescriptionId,
																											ciaGroupingItemDetailUnits,
																											ciaGroupingItemDetailValue,
																											ciaGroupingItemDetailType,
																											level,
																											ciaSummaryDisplayData,
																											actualInventoryByYear,
																											actualInventoryByGroupingDescriptionId );

			// Save the display pair to the summaryDisplayData, if right
			// criteria is met.
			saveTableDisplayPairToDisplayData( ciaCategory, ciaSummaryDisplayData, ciaGroupingItemDetailValue, ciaSummaryTableDisplayPair );

			// Add list of display data for each vehicle to the list of vehicles
			// in each category
			ciaSummaryGroupingItemToDetails.put( ciaSummaryDisplayData.getGroupingDescriptionId(), ciaSummaryDisplayData );
		}
		
		
	}

	ciaSummaryDisplayDatas.addAll( ciaSummaryGroupingItemToDetails.values() );

	// Hack to remove grouping items with no understocking display pairs.
	cullGroupingItemsWithNoPairedData(ciaSummaryDisplayDatas, ciaCategory);
	
	// Sort by valuation.
	// Manager's choice will have null values for valuation. This is a temporary
	// work around (read: hack) -- BF.
	try
	{
		ReverseComparator comparator = new ReverseComparator( new BeanComparator( "valuation" ) );
		Collections.sort( ciaSummaryDisplayDatas, comparator );
	}
	catch ( Exception e )
	{
		//logger.warn( "Tried to sort items by valuation when valuation was null." );
		System.out.println( "Tried to sort items by valuation when valuation was null." );
	}
	

	return ciaSummaryDisplayDatas;
}

// This is a hack around some bad data processing happening in an earlier stage.
// We are getting display data without a single "1999 understocked by 2" type of object.
// The front end is diplaying badly when this happens.
// So we remove those grouping items from the list who are missing understocking data.
// TODO - fix this in another place, where data is being extracted.
void cullGroupingItemsWithNoPairedData(List ciaSummaryDisplayDatas, CIACategory category)
{
	
	if(category.equals(CIACategory.MARKET_PERFORMERS))
		return;

	
	CIASummaryYearRecommendationDisplayData displayData=null;
	CIASummaryYearRecommendationDisplayData removeDisplayData;
	
	boolean done=false;
	
	// Need this outer loop because we can't delete an item in a list
	// while iterating thru that list.
	while(!done)
	{
		removeDisplayData = null;
		Iterator groupingItemIter = ciaSummaryDisplayDatas.iterator();
		done = true;
		Map map=null;
		while(groupingItemIter.hasNext())
		{
			displayData = (CIASummaryYearRecommendationDisplayData)groupingItemIter.next();
			// Looking for empy maps of paireddata
			if(displayData.getPairedDataMap() != null)
			{
				map = displayData.getPairedDataMap();
				if(map.isEmpty())
				{
					removeDisplayData = displayData;
					done = false;
					break;
				}
			}
		}
		
		// No paired data:
		// 1) Powerzone wants to display vehicles anyway, so we add a dummy recommendation with text message.
		// 2) Goodbets and Winners don't need to display anything.  Remove the display data from list.
		if(removeDisplayData != null)
		{
			// Add a dummy item with text description.
			if(category == CIACategory.POWERZONE)
			{
				CIASummaryTableDisplayPair pair = new CIASummaryTableDisplayPair();
				pair.setAlternateText("No years understocked");
				
				// Add a placeholder display pair
				map.put("1", pair );
			}
			// Need to remove this guy when not looping thru a list.
			else
			{
				ciaSummaryDisplayDatas.remove(removeDisplayData);
			}
		}
	}	
}


// There are some cases where we do not save the display pair.
// - displayPair is null
// - Inventory Gap for CIA recommendations is zero
// - Duplicate Year data.
private void saveTableDisplayPairToDisplayData( CIACategory ciaCategory, CIASummaryYearRecommendationDisplayData ciaSummaryDisplayData,
												String ciaGroupingItemDetailValue, CIASummaryTableDisplayPair ciaSummaryTableDisplayPair )
{
	// Don't bother if display pair is null
	if ( ciaSummaryTableDisplayPair == null )
	{
		return;
	}

	// POWERZONE
	if ( ciaCategory.getCiaCategoryId().intValue() == CIACategory.POWERZONE_CATEGORY )
	{
		if (ciaSummaryTableDisplayPair.getInventoryGap() == null || ciaSummaryTableDisplayPair.getInventoryGap().intValue() > 0 )
		{
			ciaSummaryDisplayData.getPairedDataMap().put( ciaGroupingItemDetailValue, ciaSummaryTableDisplayPair );
		}
		return;
	}
	// GOOD BETS, WINNERS, MARKET PERFORMERS.
	else
	{
		// No Duplicates
		// TODO look into why we get duplicates at all.
		if ( ciaSummaryDisplayData.getPairedDataMap().get( ciaGroupingItemDetailValue ) != null )
		{
			return;
		}

		// Only care to display data where inventory gap is understocked by 1 or
		// more
		if ( ciaSummaryTableDisplayPair.getPlannedToBuyAmount().intValue() < 0 )
		{
			return;
		}

		// Handle entries from the CIA recommendations.
		if ( ciaSummaryTableDisplayPair.getInventoryGap() != null )
		{
			if ( ciaSummaryTableDisplayPair.getInventoryGap().intValue() > 0 )
				ciaSummaryDisplayData.getPairedDataMap().put( ciaGroupingItemDetailValue, ciaSummaryTableDisplayPair );
		}
		// Handle entries from Manager's choice selections
		else
		{
			if ( ciaSummaryTableDisplayPair.isManagerChoice() )
			{
				ciaSummaryDisplayData.getPairedDataMap().put( ciaGroupingItemDetailValue, ciaSummaryTableDisplayPair );
				// Set the Manager's Choice flag to display the "* Manager
				// selected;, not a system recommended model year" string.
				ciaSummaryDisplayData.setManagersChoice( true );
			}
		}
	}
}

private CIASummaryTableDisplayPair constructCiaSummaryTableDisplayPair( Integer dealerId, DealerPreference dealerPreference,
																		Integer groupingDescriptionId, Integer ciaGroupingItemDetailUnits,
																		String year, CIAGroupingItemDetailType ciaGroupingItemDetailType,
																		Integer ciaGroupingItemDetailLevelId,
																		CIASummaryYearRecommendationDisplayData summaryDisplayData,
																		MultiHashMap actualInventoryMapByYear,
																		MultiHashMap actualInventoryMapByModel )
{
	if ( ciaGroupingItemDetailLevelId.intValue() == CIAGroupingItemDetailLevel.GROUPING_DETAIL_LEVEL )
	{
		year = groupingDescriptionId.toString();
	}
	if ( ( year == null ) || year.equals( "" ) )
		return null;

	CIASummaryTableDisplayPair ciaSummaryTableDisplayPair = null;
	if ( ( year == null ) || !summaryDisplayData.getPairedDataMap().containsKey( year ) )
	{
		ciaSummaryTableDisplayPair = new CIASummaryTableDisplayPair();

		if ( year == null )
		{
			year = "";
		}
		// If the detail level value is a grouping description id instead of a
		// year (model level rec)
		if ( ciaGroupingItemDetailLevelId.intValue() == CIAGroupingItemDetailLevel.GROUPING_DETAIL_LEVEL )
		{
			ciaSummaryTableDisplayPair.setCiaGroupingItemDetailLevelDescription( "Model" );
		}
		else
		{
			ciaSummaryTableDisplayPair.setCiaGroupingItemDetailLevelDescription( year.toString() );
		}
		if ( ciaGroupingItemDetailType.equals( CIAGroupingItemDetailType.OPTIMAL_STOCKING_LEVEL ) )
		{
			if ( ciaGroupingItemDetailLevelId.intValue() == CIAGroupingItemDetailLevel.GROUPING_DETAIL_LEVEL )
			{
				ciaSummaryTableDisplayPair.setInventoryGap( calculateInventoryGapByModel( dealerId, dealerPreference, groupingDescriptionId,
																							ciaGroupingItemDetailUnits,
																							actualInventoryMapByModel ) );
			}
			else
			{
				ciaSummaryTableDisplayPair.setInventoryGap( calculateInventoryGapByYear( dealerId, dealerPreference, groupingDescriptionId,
																							ciaGroupingItemDetailUnits, year,
																							actualInventoryMapByYear ) );
			}

			ciaSummaryTableDisplayPair.setManagerChoice( false );
		}
		else
		{
			ciaSummaryTableDisplayPair.setPlannedToBuyAmount( ciaGroupingItemDetailUnits );
			ciaSummaryTableDisplayPair.setManagerChoice( true );
		}
	}
	else
	{
		ciaSummaryTableDisplayPair = (CIASummaryTableDisplayPair)summaryDisplayData.getPairedDataMap().get( year );
		if ( ciaGroupingItemDetailType.equals( CIAGroupingItemDetailType.OPTIMAL_STOCKING_LEVEL ) )
		{
			ciaSummaryTableDisplayPair.setInventoryGap( calculateInventoryGapByYear( dealerId, dealerPreference, groupingDescriptionId,
																						ciaGroupingItemDetailUnits, year,
																						actualInventoryMapByYear ) );
			ciaSummaryTableDisplayPair.setManagerChoice( false );
		}
		else
		{
			ciaSummaryTableDisplayPair.setPlannedToBuyAmount( ciaGroupingItemDetailUnits );
			ciaSummaryTableDisplayPair.setManagerChoice( false );
		}
	}
	int yearAsInt;
	try
	{
		yearAsInt = Integer.parseInt( year );
	}
	catch ( NumberFormatException nfe )
	{
		yearAsInt = 0;
	}
	ciaSummaryTableDisplayPair.setYear( yearAsInt );
	return ciaSummaryTableDisplayPair;
}

private Integer calculateInventoryGapByYear( Integer dealerId, DealerPreference dealerPreference, Integer groupingDescriptionId,
											Integer optimalStockingLevel, String year, MultiHashMap actualInventoryMap )
{
	int activeInventoryForYear = CIAReportingUtility.retrieveUnitsInStockForGroupDescriptionAndYear( year, groupingDescriptionId.toString(),
																										actualInventoryMap );
	int inventoryGap = optimalStockingLevel.intValue() - activeInventoryForYear;

	return new Integer( inventoryGap );
}

private Integer calculateInventoryGapByModel( Integer dealerId, DealerPreference dealerPreference, Integer groupingDescriptionId,
												Integer optimalStockingLevel, MultiHashMap actualInventoryMap )
{
	int activeInventoryForYear = CIAReportingUtility.retrieveUnitsInStockForGroupDescription( groupingDescriptionId.toString(),
																								actualInventoryMap );
	int inventoryGap = optimalStockingLevel.intValue() - activeInventoryForYear;

	return new Integer( inventoryGap );
}

public CIASummaryReportDAO getCiaSummaryReportDAO()
{
	return ciaSummaryReportDAO;
}

public void setCiaSummaryReportDAO( CIASummaryReportDAO ciaSummaryReportDao )
{
	this.ciaSummaryReportDAO = ciaSummaryReportDao;
}

public IDealerPreferenceDAO getDealerPrefDAO()
{
	return dealerPrefDAO;
}

public void setDealerPrefDAO( IDealerPreferenceDAO dealerPrefDao )
{
	this.dealerPrefDAO = dealerPrefDao;
}

public ICIABuyingPlanReportDAO getCiaBuyingPlanReportDAO()
{
	return ciaBuyingPlanReportDAO;
}

public void setCiaBuyingPlanReportDAO( ICIABuyingPlanReportDAO ciaBuyingPlanReportDAO )
{
	this.ciaBuyingPlanReportDAO = ciaBuyingPlanReportDAO;
}

}