package com.firstlook.service.cia.report;

import java.text.DecimalFormat;

public class CIASummaryMarketPerformersDisplayData implements ICIASummaryDisplayData
{

// These 2 represent the same information: "Honda Accord", "Nissan Sentra", etc.
private Integer groupingDescriptionId;
private String groupingDescriptionString;
private String categoryString; // "Powerzone", "Winners", or "Good Bets"
private Integer segmentId; // representing CONVERTIBLE, SUV, VAN, etc.
private String segmentString;

private Double marketShare;
private Integer plannedToBuyTotal;
private Double valuation;

public CIASummaryMarketPerformersDisplayData()
{
	super();
	plannedToBuyTotal = new Integer( 0 );
}

public String getCategoryString()
{
	return categoryString;
}

public void setCategoryString( String categoryString )
{
	this.categoryString = categoryString;
}

public Integer getSegmentId()
{
	return segmentId;
}

public void setSegmentId( Integer segmentId )
{
	this.segmentId = segmentId;
}

public Integer getGroupingDescriptionId()
{
	return groupingDescriptionId;
}

public void setGroupingDescriptionId( Integer groupingDescriptionId )
{
	this.groupingDescriptionId = groupingDescriptionId;
}

public String getGroupingDescriptionString()
{
	return groupingDescriptionString;
}

public void setGroupingDescriptionString( String groupingDescriptionString )
{
	this.groupingDescriptionString = groupingDescriptionString;
}

public Double getMarketShare()
{
	return marketShare;
}

public void setMarketShare( Double marketShare )
{
	this.marketShare = marketShare;
}

public Integer getPlannedToBuyTotal()
{
	return plannedToBuyTotal;
}

public void setPlannedToBuyTotal( Integer plannedToBuy )
{
	this.plannedToBuyTotal = plannedToBuy;
}

public String getSegmentString()
{
	return segmentString;
}

public void setSegmentString( String segmentString )
{
	this.segmentString = segmentString;
}

public String getTextString()
{
	if ( marketShare != null )
	{
		DecimalFormat decimalFormat = new DecimalFormat( "####.##%" );
		String marketShareString = decimalFormat.format( marketShare.doubleValue() );
		return marketShareString + " of " + segmentString + " Sales";
	}
	else
	{
		return "Error, no market data present";
	}
}

/**
 * duplicated Logic in {@see CIASummaryTableDisplayPair}
 * @return
 */
public String getAmountString()
{
	if ( plannedToBuyTotal.intValue() == 0 )
	{
		return "--";
	}
	else
	{
		return plannedToBuyTotal.toString();
	}
}

public Double getValuation()
{
	return valuation;
}

public void setValuation( Double valuation )
{
	this.valuation = valuation;
}


}
