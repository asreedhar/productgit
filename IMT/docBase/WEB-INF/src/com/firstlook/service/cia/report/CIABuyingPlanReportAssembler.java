package com.firstlook.service.cia.report;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.MultiHashMap;

import biz.firstlook.cia.calculator.CIAGroupingItemDetailCalculator;
import biz.firstlook.cia.model.CIAGroupingItem;
import biz.firstlook.cia.model.CIAGroupingItemDetailLevel;
import biz.firstlook.cia.model.CIAGroupingItemDetailType;
import biz.firstlook.cia.model.Status;
import biz.firstlook.cia.persistence.ICIASummaryDAO;
import biz.firstlook.transact.persist.model.Segment;

import com.firstlook.display.cia.CIABuyingGuideDisplayData;
import com.firstlook.display.cia.CIAReportSegmentAndData;
import com.firstlook.persistence.cia.CIASegmentByContributionRetriever;
import com.firstlook.persistence.cia.ICIABuyingPlanReportDAO;

public class CIABuyingPlanReportAssembler implements ICIABuyingPlanReportAssembler
{

private ICIABuyingPlanReportDAO ciaBuyingPlanReportDAO;
private ICIASummaryDAO ciaSummaryDAO;
private CIASegmentByContributionRetriever ciaSegmentByContributionRetriever;

public CIABuyingPlanReportAssembler()
{
	super();
}

public List retrieveCIABuyingPlanData( Integer dealerId, Integer ciaSummaryStatusId )
{
	MultiHashMap buyingPlanItemsByBodyTypeDetail = new MultiHashMap();
	Collection<Object[]> ciaBuyingPlanItems = getCiaBuyingPlanReportDAO().findDistinctCIAGroupingItemsAndSegmentsWithBuyAmounts( dealerId,
																																ciaSummaryStatusId );
	Integer ciaSummaryId = getCiaSummaryDAO().retrieveCIASummaryId( dealerId, Status.CURRENT );
	List ciaBuyingPlanDisplayItems = new ArrayList();
	if(ciaSummaryId != null) {
		Iterator<Object[]> buyingPlanItemIter = ciaBuyingPlanItems.iterator();
		
		Integer segmentId;
		CIAGroupingItem groupingItem;
		while ( buyingPlanItemIter.hasNext() ) {
			Object[] items = buyingPlanItemIter.next();
			groupingItem = (CIAGroupingItem)items[0];
			segmentId = (Integer)items[1];
			buyingPlanItemsByBodyTypeDetail.put( segmentId, groupingItem );
		}
		ciaBuyingPlanDisplayItems.addAll(constructCIABuyingPlanDisplayItems( buyingPlanItemsByBodyTypeDetail, dealerId, ciaSummaryId.intValue() ));
	}
	return ciaBuyingPlanDisplayItems;
}

private List constructCIABuyingPlanDisplayItems( MultiHashMap buyingItemsByBodyTypeDetail, Integer dealerId, int ciaSummaryId )
{
	List segments = ciaSegmentByContributionRetriever.retrieveCIABodyTypeDetails( dealerId.intValue(), true, new Timestamp( new Date().getTime() ),
																								ciaSummaryId );
	Segment segment;
	List ciaBuyingPlanDisplayItems = new ArrayList();

	CIAReportSegmentAndData reportDisplayAndData;
	Iterator segmentIter = segments.iterator();
	List buyingGuideItems;
	Object ciaGroupingItems;
	while ( segmentIter.hasNext() )
	{
		segment = (Segment)segmentIter.next();

		ciaGroupingItems = buyingItemsByBodyTypeDetail.get( segment.getSegmentId() );
		if ( ciaGroupingItems != null )
		{
			buyingGuideItems = constructBuyingGuideDisplayItemsFromGroupingItems( (List)ciaGroupingItems );
		}
		else
		{
			buyingGuideItems = new ArrayList();
		}
		reportDisplayAndData = new CIAReportSegmentAndData( segment.getSegment(), buyingGuideItems );
		ciaBuyingPlanDisplayItems.add( reportDisplayAndData );
	}

	return ciaBuyingPlanDisplayItems;
}

private List constructBuyingGuideDisplayItemsFromGroupingItems( List groupingItems )
{
	List buyingGuideDisplayItems = new ArrayList();
	CIABuyingGuideDisplayData buyingGuideDisplayData;
	CIAGroupingItem groupingItem;
	Iterator groupingItemIter = groupingItems.iterator();
	List buyDetails;
	while ( groupingItemIter.hasNext() )
	{
		buyDetails = new ArrayList();
		groupingItem = (CIAGroupingItem)groupingItemIter.next();
		buyingGuideDisplayData = new CIABuyingGuideDisplayData();
		buyingGuideDisplayData.setCiaGroupingItemId( groupingItem.getCiaGroupingItemId() );
		buyingGuideDisplayData.setGroupingDescription( groupingItem.getGroupingDescription().getGroupingDescription() );
		buyingGuideDisplayData.setGroupingDescriptionId( groupingItem.getGroupingDescription().getGroupingDescriptionId() );
		buyingGuideDisplayData.setNotes( groupingItem.getNotes() );

		buyingGuideDisplayData.setAvoidColors( CIAGroupingItemDetailCalculator.filterDetailValuesByTypeAndLevel(
																													CIAGroupingItemDetailType.AVOID.getCiaGroupingItemDetailTypeId().intValue(),
																													CIAGroupingItemDetailLevel.COLOR.getCiaGroupingItemDetailLevelId().intValue(),
																													groupingItem.getCiaGroupingItemDetails() ) );
		buyingGuideDisplayData.setAvoidTrims( CIAGroupingItemDetailCalculator.filterDetailValuesByTypeAndLevel(
																												CIAGroupingItemDetailType.AVOID.getCiaGroupingItemDetailTypeId().intValue(),
																												CIAGroupingItemDetailLevel.TRIM.getCiaGroupingItemDetailLevelId().intValue(),
																												groupingItem.getCiaGroupingItemDetails() ) );
		buyingGuideDisplayData.setAvoidYears( CIAGroupingItemDetailCalculator.filterDetailValuesByTypeAndLevel(
																												CIAGroupingItemDetailType.AVOID.getCiaGroupingItemDetailTypeId().intValue(),
																												CIAGroupingItemDetailLevel.YEAR.getCiaGroupingItemDetailLevelId().intValue(),
																												groupingItem.getCiaGroupingItemDetails() ) );
		buyingGuideDisplayData.setAvoidUnitCostPointRangeStrings( CIAGroupingItemDetailCalculator.filterUnitCostRangesByType(
																																CIAGroupingItemDetailType.AVOID.getCiaGroupingItemDetailTypeId().intValue(),
																																groupingItem.getCiaGroupingItemDetails() ) );
		buyingGuideDisplayData.setPreferColors( CIAGroupingItemDetailCalculator.filterDetailValuesByTypeAndLevel(
																													CIAGroupingItemDetailType.PREFER.getCiaGroupingItemDetailTypeId().intValue(),
																													CIAGroupingItemDetailLevel.COLOR.getCiaGroupingItemDetailLevelId().intValue(),
																													groupingItem.getCiaGroupingItemDetails() ) );
		buyingGuideDisplayData.setPreferTrims( CIAGroupingItemDetailCalculator.filterDetailValuesByTypeAndLevel(
																													CIAGroupingItemDetailType.PREFER.getCiaGroupingItemDetailTypeId().intValue(),
																													CIAGroupingItemDetailLevel.TRIM.getCiaGroupingItemDetailLevelId().intValue(),
																													groupingItem.getCiaGroupingItemDetails() ) );
		buyingGuideDisplayData.setPreferYears( CIAGroupingItemDetailCalculator.filterDetailValuesByTypeAndLevel(
																													CIAGroupingItemDetailType.PREFER.getCiaGroupingItemDetailTypeId().intValue(),
																													CIAGroupingItemDetailLevel.YEAR.getCiaGroupingItemDetailLevelId().intValue(),
																													groupingItem.getCiaGroupingItemDetails() ) );
		buyingGuideDisplayData.setPreferUnitCostPointRangeStrings( CIAGroupingItemDetailCalculator.filterUnitCostRangesByType(
																																CIAGroupingItemDetailType.PREFER.getCiaGroupingItemDetailTypeId().intValue(),
																																groupingItem.getCiaGroupingItemDetails() ) );
		buyDetails = ( CIAGroupingItemDetailCalculator.filterDetailsByTypeAndLevel(
																					CIAGroupingItemDetailType.BUY.getCiaGroupingItemDetailTypeId().intValue(),
																					CIAGroupingItemDetailLevel.YEAR.getCiaGroupingItemDetailLevelId().intValue(),
																					groupingItem.getCiaGroupingItemDetails() ) );
		buyDetails.addAll( CIAGroupingItemDetailCalculator.filterDetailsByTypeAndLevel(
																					CIAGroupingItemDetailType.BUY.getCiaGroupingItemDetailTypeId().intValue(),
																					CIAGroupingItemDetailLevel.GROUPING.getCiaGroupingItemDetailLevelId().intValue(),
																					groupingItem.getCiaGroupingItemDetails() ) );
		buyingGuideDisplayData.setBuyAmountDetails( buyDetails );
		buyingGuideDisplayData.setTotalBuyAmountForModel( CIAGroupingItemDetailCalculator.sumBuyAmounts( buyingGuideDisplayData.getBuyAmountDetails() ) );
		

		buyingGuideDisplayItems.add( buyingGuideDisplayData );
	}

	return buyingGuideDisplayItems;
}

public ICIABuyingPlanReportDAO getCiaBuyingPlanReportDAO()
{
	return ciaBuyingPlanReportDAO;
}

public void setCiaBuyingPlanReportDAO( ICIABuyingPlanReportDAO ciaBuyingPlanReportDAO )
{
	this.ciaBuyingPlanReportDAO = ciaBuyingPlanReportDAO;
}

public ICIASummaryDAO getCiaSummaryDAO()
{
	return ciaSummaryDAO;
}

public void setCiaSummaryDAO( ICIASummaryDAO ciaSummaryDAO )
{
	this.ciaSummaryDAO = ciaSummaryDAO;
}

public void setCiaSegmentByContributionRetriever(
		CIASegmentByContributionRetriever ciaSegmentByContributionRetriever) {
	this.ciaSegmentByContributionRetriever = ciaSegmentByContributionRetriever;
}

}
