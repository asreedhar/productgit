package com.firstlook.service.cia.transactional;

import java.util.List;
import java.util.Map;

public class CIADetailPageDisplayItemWithPrefs
{
private List ciaDetailPageDisplayItems;
private Map groupingPreferences;

public CIADetailPageDisplayItemWithPrefs()
{
	super();
}

public List getCiaDetailPageDisplayItems()
{
	return ciaDetailPageDisplayItems;
}

public void setCiaDetailPageDisplayItems( List ciaDetailPageDisplayItems )
{
	this.ciaDetailPageDisplayItems = ciaDetailPageDisplayItems;
}

public Map getGroupingPreferences()
{
	return groupingPreferences;
}

public void setGroupingPreferences( Map groupingPreferences )
{
	this.groupingPreferences = groupingPreferences;
}

}
