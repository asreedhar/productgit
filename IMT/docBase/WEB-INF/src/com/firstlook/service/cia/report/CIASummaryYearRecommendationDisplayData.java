package com.firstlook.service.cia.report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CIASummaryYearRecommendationDisplayData implements ICIASummaryDisplayData
{

// These 2 represent the same information: "Honda Accord", "Nissan Sentra", etc.
private Integer groupingDescriptionId;
private String groupingDescriptionString;
private String categoryString; // "Powerzone", "Winners", or "Good Bets"
private Integer segmentId; // representing CONVERTIBLE, SUV, VAN, etc.
private String segmentString;

// A list of paired strings
// Using the 1st string for recommendations or % of market shares
// Using the 2nd string for plannedToBuy
// Example; key: 2005, value: "2005 understocked by 3, (planned to buy) 4"
private Map pairedDataMap;

/* This is necessary to display the list of values ordered by year on the summary page */
private List pairedDataList;

// These 3 are displayed together on the lower left.
private int unitsInStock;
private Integer targetInventory;
private Integer plannedToBuyTotal;
private boolean managersChoice;
private Double valuation;

public CIASummaryYearRecommendationDisplayData()
{
	super();
	pairedDataMap = new HashMap();
	pairedDataList = new ArrayList();
}

public String getCategoryString()
{
	return categoryString;
}

public void setCategoryString( String categoryString )
{
	this.categoryString = categoryString;
}

public Integer getSegmentId()
{
	return segmentId;
}

public void setSegmentId( Integer segmentId )
{
	this.segmentId = segmentId;
}

public Integer getGroupingDescriptionId()
{
	return groupingDescriptionId;
}

public void setGroupingDescriptionId( Integer groupingDescriptionId )
{
	this.groupingDescriptionId = groupingDescriptionId;
}

public String getGroupingDescriptionString()
{
	return groupingDescriptionString;
}

public void setGroupingDescriptionString( String groupingDescriptionString )
{
	this.groupingDescriptionString = groupingDescriptionString;
}

public Map getPairedDataMap()
{
	return pairedDataMap;
}

public void setPairedDataMap( Map pairedData )
{
	this.pairedDataMap = pairedData;
}

public Integer getPlannedToBuyTotal()
{
	return plannedToBuyTotal;
}

public void setPlannedToBuyTotal( Integer plannedToBuyTotal )
{
	this.plannedToBuyTotal = plannedToBuyTotal;
}

public Integer getTargetInventory()
{
	return targetInventory;
}

public void setTargetInventory( Integer targetInventory )
{
	this.targetInventory = targetInventory;
}

public int getUnitsInStock()
{
	return unitsInStock;
}

public void setUnitsInStock( int unitsInStock )
{
	this.unitsInStock = unitsInStock;
}

public String getSegmentString()
{
	return segmentString;
}

public void setSegmentString( String segmentString )
{
	this.segmentString = segmentString;
}

public boolean isManagersChoice()
{
	return managersChoice;
}

public void setManagersChoice( boolean managersChoice )
{
	this.managersChoice = managersChoice;
}

public List getPairedDataList()
{
	return pairedDataList;
}

public void setPairedDataList( List pairedDataList )
{
	this.pairedDataList = pairedDataList;
}

public Double getValuation()
{
	return valuation;
}

public void setValuation( Double valuation )
{
	this.valuation = valuation;
}

}