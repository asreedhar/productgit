package com.firstlook.service.cia;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.firstlook.service.cia.report.TestCIAStockingCalculator;

public class AllTests extends TestCase
{

public AllTests( String arg0 )
{
    super(arg0);
}

public static Test suite()
{
    TestSuite suite = new TestSuite();

    suite.addTestSuite(TestCIAStockingCalculator.class);
    return suite;
}
}
