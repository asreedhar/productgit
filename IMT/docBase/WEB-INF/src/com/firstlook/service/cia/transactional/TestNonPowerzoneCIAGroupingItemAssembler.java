package com.firstlook.service.cia.transactional;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;
import biz.firstlook.cia.model.CIAGroupingItemDetail;
import biz.firstlook.cia.model.CIAGroupingItemDetailLevel;
import biz.firstlook.cia.model.CIAGroupingItemDetailType;

public class TestNonPowerzoneCIAGroupingItemAssembler extends TestCase
{

List detailList;

public TestNonPowerzoneCIAGroupingItemAssembler( String arg0 )
{
	super( arg0 );
	
	detailList = new ArrayList();
	CIAGroupingItemDetailLevel yearLevel = CIAGroupingItemDetailLevel.YEAR;
	CIAGroupingItemDetailType recType = CIAGroupingItemDetailType.OPTIMAL_STOCKING_LEVEL;
	
	for (int i = 0; i < 5; i++) 
	{
		CIAGroupingItemDetail detail = new CIAGroupingItemDetail();
		buildGroupingItemDetail( detail, i, yearLevel, recType, i-3 );
		detailList.add(i,detail);
	}
}

public void testDetermineUnderstockedYears()
{
	CIANonPowerzoneDetailPageDisplayItemAssembler testAssembler = new CIANonPowerzoneDetailPageDisplayItemAssembler();
	List testList = testAssembler.determineUnderstockedYears(detailList);
	
	assertEquals("Size of list should be 1",1,testList.size());
	assertEquals("ID should be 4",4,((CIAGroupingItemDetail)testList.get(0)).getCiaGroupingItemDetailId().intValue());
}

public void testDetermineOverstockedYears()
{
	CIANonPowerzoneDetailPageDisplayItemAssembler testAssembler = new CIANonPowerzoneDetailPageDisplayItemAssembler();
	List testList = testAssembler.determineOverstockedYears(detailList);
	
	assertEquals("Size of list should be 3",3,testList.size());
	for (int i = 0; i < 3; i++)
	{
		assertEquals("ID should be " + i,i,((CIAGroupingItemDetail)testList.get(i)).getCiaGroupingItemDetailId().intValue());
	}
}

public void testDetermineNonUnderstockedYears()
{
	CIANonPowerzoneDetailPageDisplayItemAssembler testAssembler = new CIANonPowerzoneDetailPageDisplayItemAssembler();
	List testList = testAssembler.determineNonUnderstockedYears(detailList);
	
	assertEquals("Size of list should be 4",4,testList.size());
	for (int i = 0; i < 4; i++)
	{
		assertEquals("ID should be " + i,i,((CIAGroupingItemDetail)testList.get(i)).getCiaGroupingItemDetailId().intValue());
	}
}

private void buildGroupingItemDetail( CIAGroupingItemDetail detail, 
                                      int groupingItemDetailId,
                                      CIAGroupingItemDetailLevel yearLevel, 
                                      CIAGroupingItemDetailType recType, 
                                      int understocked )
{
	detail.setCiaGroupingItemDetailId( new Integer(groupingItemDetailId) );
	detail.setCiaGroupingItemDetailLevel( yearLevel );
	detail.setCiaGroupingItemDetailType( recType );
	detail.setUnderstocked( understocked );
}

}
