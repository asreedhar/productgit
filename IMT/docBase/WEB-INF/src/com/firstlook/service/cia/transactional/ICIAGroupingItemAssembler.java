package com.firstlook.service.cia.transactional;

import java.util.List;
import java.util.Map;

import biz.firstlook.cia.model.CIACategory;
import biz.firstlook.cia.model.CIAGroupingItem;

public interface ICIAGroupingItemAssembler
{

public List<CIAGroupingItem> getCIAGroupingItems( int businessUnitId, int segmentId, int ciaCategoryId );

public CIAGroupingItem getCIAGroupingItemByGroupingDescription( Integer segmentId, Integer ciaCategoryId, Integer groupingDescriptionId, Integer dealerId );

public CIAGroupingItem constructCIAGroupingItemFromBuyingPreferences( CIAGroupingItem groupingItem, Map parameterMap, CIACategory ciaCategory,
																Integer ciaBodyTypeDetailId );

public CIAGroupingItem constructCIAGroupingItemFromPreferences( Integer ciaGroupingItemId, Integer groupingDescriptionId, Map parameterMap,
																CIACategory ciaCategory, Integer ciaBodyTypeDetailId );

public CIAGroupingItem constructCIAGroupingItemFromBuyAmountAndPreferences( Integer ciaGroupingItemId, Integer groupingDescriptionId,
																			Map parameterMap, CIACategory ciaCategory, int buyAmount,
																			Integer ciaBodyTypeDetailId );

}
