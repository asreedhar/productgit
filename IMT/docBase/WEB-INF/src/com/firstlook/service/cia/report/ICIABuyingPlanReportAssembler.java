package com.firstlook.service.cia.report;

import java.util.List;

public interface ICIABuyingPlanReportAssembler
{

public List retrieveCIABuyingPlanData( Integer dealerId, Integer ciaSummaryStatusId );

}