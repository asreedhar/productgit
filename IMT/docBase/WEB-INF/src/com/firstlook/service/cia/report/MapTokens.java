package com.firstlook.service.cia.report;

import biz.firstlook.cia.model.CIAGroupingItemDetail;
import biz.firstlook.cia.model.CIAGroupingItemDetailLevel;

//TODO: This whole class should probably be a static Utility
public class MapTokens
{

private static final String DELIMITER = "_";

private Integer ciaGroupingItemId;
private String level;
private String key;
private String directive;

public MapTokens parse( String checkBoxKey )
{
	String[] tokens = checkBoxKey.split( DELIMITER );
	MapTokens mapTokens = new MapTokens();
	mapTokens.ciaGroupingItemId = new Integer( tokens[1] );
	mapTokens.level = tokens[2];

	String realKey = TrimFormattingUtility.escapeJS( tokens[3] );
	mapTokens.key = realKey;
	mapTokens.directive = tokens[4];
	return mapTokens;
}

public MapTokens create( CIAGroupingItemDetail detail, Integer ciaGroupingItemId )
{
	MapTokens mapTokens = new MapTokens();
	mapTokens.ciaGroupingItemId = ciaGroupingItemId;
	mapTokens.level = detail.getCiaGroupingItemDetailLevel().getDescription();
	if (detail.getCiaGroupingItemDetailLevel().getCiaGroupingItemDetailLevelId().intValue() == CIAGroupingItemDetailLevel.COLOR_DETAIL_LEVEL)
	{
		mapTokens.key = detail.getDetailLevelValue().toUpperCase();
	}
	else if( detail.getCiaGroupingItemDetailLevel().getCiaGroupingItemDetailLevelId().intValue() == CIAGroupingItemDetailLevel.UNITCOST_DETAIL_LEVEL )
	{
		mapTokens.key = detail.getLowRange() + "_" + detail.getHighRange();
	}
	else
	{
		mapTokens.key = detail.getDetailLevelValue();
	}
	mapTokens.directive = detail.getCiaGroupingItemDetailType().getDescription();
	return mapTokens;
}

public String constructCompositeKey()
{
	return ciaGroupingItemId + DELIMITER + level + DELIMITER + key + DELIMITER + directive;
}

}
