package com.firstlook.service.cia.transactional;



import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AllTests extends TestCase
{

public AllTests( String arg0 )
{
    super(arg0);
}

public static Test suite()
{
    TestSuite suite = new TestSuite();

    suite.addTestSuite(TestAbstractCIAGroupingItemAssembler.class);
    suite.addTestSuite(TestNonPowerzoneCIAGroupingItemAssembler.class);
    

    return suite;
}
}
