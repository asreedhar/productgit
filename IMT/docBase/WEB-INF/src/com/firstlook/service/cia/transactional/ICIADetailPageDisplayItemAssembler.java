package com.firstlook.service.cia.transactional;

import java.util.List;

import com.firstlook.display.cia.CIAGroupingItemDisplayData;

public interface ICIADetailPageDisplayItemAssembler
{
public boolean determineIncludeItem( CIAGroupingItemDisplayData displayGroupingItem );

public void filterYearsByStocking( CIAGroupingItemDisplayData displayGroupingItem, List yearDetailsWithRecs );

public List getCIADetailPageDisplayItems( int businessUnitId, List ciaGroupingItems );
}
