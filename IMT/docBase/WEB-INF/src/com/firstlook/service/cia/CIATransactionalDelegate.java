package com.firstlook.service.cia;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import biz.firstlook.cia.model.CIABodyTypeDetail;
import biz.firstlook.cia.model.CIACategory;
import biz.firstlook.cia.model.CIAGroupingItem;
import biz.firstlook.cia.model.Status;
import biz.firstlook.cia.persistence.CIABodyTypeDetailPersistence;
import biz.firstlook.cia.persistence.ICIAGroupingItemDAO;
import biz.firstlook.transact.persist.service.MakeModelGroupingService;

import com.firstlook.service.cia.transactional.CIAMarketPerformerDetailPageDisplayAssembler;
import com.firstlook.service.cia.transactional.ICIADetailPageDisplayItemAssembler;
import com.firstlook.service.cia.transactional.ICIAGroupingItemAssembler;

public class CIATransactionalDelegate
{
private ICIAGroupingItemAssembler ciaGroupingItemAssembler;
private ICIADetailPageDisplayItemAssembler ciaPowerzoneDetailPageDisplayItemAssembler;
private ICIADetailPageDisplayItemAssembler ciaNonPowerzoneDetailPageDisplayItemAssembler;
private CIAMarketPerformerDetailPageDisplayAssembler ciaMarketPerformerDetailPageDisplayAssembler;
private ICIAGroupingItemDAO ciaGroupingItemDAO;
private MakeModelGroupingService makeModelGroupingService;

public CIATransactionalDelegate()
{
	super();
}

public List<CIAGroupingItem> getCIAGroupingItems( int businessUnitId, int segmentId, int ciaCategoryId )
{
	return getCiaGroupingItemAssembler().getCIAGroupingItems( businessUnitId, segmentId, ciaCategoryId );
}

public void constructCIAGroupingItemFromBuyingPreferences( List groupingItems, Map parameterMap,
																			CIACategory ciaCategory, Integer ciaBodyTypeDetailId )
{
	Iterator groupingItemIter = groupingItems.iterator();
	CIAGroupingItem groupingItem;
	while( groupingItemIter.hasNext() )
	{
		groupingItem = (CIAGroupingItem)groupingItemIter.next();
		getCiaGroupingItemAssembler().constructCIAGroupingItemFromBuyingPreferences( groupingItem, parameterMap,
																							ciaCategory, ciaBodyTypeDetailId );
		saveCIAGroupingItem( groupingItem );
	}
	
}


public CIAGroupingItem constructCIAGroupingItemFromBuyAmountAndPreferences( Integer ciaGroupingItemId, Integer groupingDescriptionId,
																			Map parameterMap, CIACategory ciaCategory, int buyAmount,
																			Integer ciaBodyTypeDetailId )
{
	return getCiaGroupingItemAssembler().constructCIAGroupingItemFromBuyAmountAndPreferences( ciaGroupingItemId, groupingDescriptionId,
																								parameterMap, ciaCategory, buyAmount,
																								ciaBodyTypeDetailId );
}

public CIABodyTypeDetail getBodyTypeDetailByDealerIdStatusAndSegment( Integer dealerId, Status status, Integer segmentId )
{
	CIABodyTypeDetailPersistence persistence = new CIABodyTypeDetailPersistence();
	return persistence.findByDealerIdStatusAndSegment( dealerId, status, segmentId );
}

public void saveCIAGroupingItem( CIAGroupingItem groupingItem )
{
	getCiaGroupingItemDAO().saveOrUpdate( groupingItem );
}

public List getCIADetailPageDisplayItems( int businessUnitId, List ciaGroupingItems, int ciaCategoryId )
{
	List returnList = null;
	switch ( ciaCategoryId )
	{
		case CIACategory.POWERZONE_CATEGORY:
			returnList = getCIAPowerzoneDetailPageDisplayItemAssembler().getCIADetailPageDisplayItems( businessUnitId, ciaGroupingItems );
			break;
		case CIACategory.MARKETPERFORMERS_CATEGORY:
			returnList = getCiaMarketPerformerDetailPageDisplayAssembler().createCIADetailPageGroupingItemDisplayData( businessUnitId, ciaGroupingItems );
			break;
		default:
			returnList = getCIANonPowerzoneDetailPageDisplayItemAssembler().getCIADetailPageDisplayItems( businessUnitId, ciaGroupingItems );
			break;
	}

	return returnList;
}

public CIAGroupingItem getCIAGroupingItemByGroupingDescriptionId( Integer segmentId, Integer ciaCategoryId,
																	Integer groupingDescriptionId, Integer dealerId )
{
	return getCiaGroupingItemAssembler().getCIAGroupingItemByGroupingDescription( segmentId, ciaCategoryId, groupingDescriptionId, dealerId );
}

public List getManagersChoiceItemsBySegmentId( Integer segmentId )
{
	return getMakeModelGroupingService().retrieveMakeModelGroupingsBySegmentIdOrderedByMakeModel( segmentId );
}

public void setCiaGroupingItemAssembler( ICIAGroupingItemAssembler ciaGroupingItemAssembler )
{
	this.ciaGroupingItemAssembler = ciaGroupingItemAssembler;
}

public ICIAGroupingItemAssembler getCiaGroupingItemAssembler()
{
	return ciaGroupingItemAssembler;
}

public ICIADetailPageDisplayItemAssembler getCIAPowerzoneDetailPageDisplayItemAssembler()
{
	return ciaPowerzoneDetailPageDisplayItemAssembler;
}

public void setCIAPowerzoneDetailPageDisplayItemAssembler( ICIADetailPageDisplayItemAssembler ciaPowerzoneDetailPageDisplayItemAssembler )
{
	this.ciaPowerzoneDetailPageDisplayItemAssembler = ciaPowerzoneDetailPageDisplayItemAssembler;
}

public ICIADetailPageDisplayItemAssembler getCIANonPowerzoneDetailPageDisplayItemAssembler()
{
	return ciaNonPowerzoneDetailPageDisplayItemAssembler;
}

public void setCIANonPowerzoneDetailPageDisplayItemAssembler( ICIADetailPageDisplayItemAssembler ciaNonPowerzoneDetailPageDisplayItemAssembler )
{
	this.ciaNonPowerzoneDetailPageDisplayItemAssembler = ciaNonPowerzoneDetailPageDisplayItemAssembler;
}

public ICIADetailPageDisplayItemAssembler getCiaPowerzoneDetailPageDisplayItemAssembler()
{
	return ciaPowerzoneDetailPageDisplayItemAssembler;
}

public void setCiaPowerzoneDetailPageDisplayItemAssembler( ICIADetailPageDisplayItemAssembler ciaPowerzoneDetailPageDisplayItemAssembler )
{
	this.ciaPowerzoneDetailPageDisplayItemAssembler = ciaPowerzoneDetailPageDisplayItemAssembler;
}

public ICIADetailPageDisplayItemAssembler getCiaNonPowerzoneDetailPageDisplayItemAssembler()
{
	return ciaNonPowerzoneDetailPageDisplayItemAssembler;
}

public void setCiaNonPowerzoneDetailPageDisplayItemAssembler( ICIADetailPageDisplayItemAssembler ciaNonPowerzoneDetailPageDisplayItemAssembler )
{
	this.ciaNonPowerzoneDetailPageDisplayItemAssembler = ciaNonPowerzoneDetailPageDisplayItemAssembler;
}

public ICIAGroupingItemDAO getCiaGroupingItemDAO()
{
	return ciaGroupingItemDAO;
}

public void setCiaGroupingItemDAO( ICIAGroupingItemDAO ciaGroupingItemDAO )
{
	this.ciaGroupingItemDAO = ciaGroupingItemDAO;
}

public CIAMarketPerformerDetailPageDisplayAssembler getCiaMarketPerformerDetailPageDisplayAssembler()
{
	return ciaMarketPerformerDetailPageDisplayAssembler;
}

public void setCiaMarketPerformerDetailPageDisplayAssembler(
															CIAMarketPerformerDetailPageDisplayAssembler ciaMarketPerformerDetailPageDisplayAssembler )
{
	this.ciaMarketPerformerDetailPageDisplayAssembler = ciaMarketPerformerDetailPageDisplayAssembler;
}

public MakeModelGroupingService getMakeModelGroupingService()
{
	return makeModelGroupingService;
}

public void setMakeModelGroupingService( MakeModelGroupingService makeModelGroupingService )
{
	this.makeModelGroupingService = makeModelGroupingService;
}

}
