package com.firstlook.service.cia.transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import biz.firstlook.cia.model.CIABodyTypeDetail;
import biz.firstlook.cia.model.CIACategory;
import biz.firstlook.cia.model.CIAGroupingItem;
import biz.firstlook.cia.model.CIAGroupingItemDetail;
import biz.firstlook.cia.model.CIAGroupingItemDetailLevel;
import biz.firstlook.cia.model.CIAGroupingItemDetailType;
import biz.firstlook.cia.model.Status;
import biz.firstlook.cia.persistence.CIASummaryDAO;
import biz.firstlook.cia.persistence.ICIAGroupingItemDAO;
import biz.firstlook.transact.persist.model.GroupingDescription;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;
import biz.firstlook.transact.persist.persistence.GroupingDescriptionDAO;

import com.firstlook.action.dealer.cia.ParamFilterPredicate;
import com.firstlook.service.cia.report.CIAReportingUtility;

public class CIAGroupingItemAssembler implements ICIAGroupingItemAssembler
{
private CIASummaryDAO ciaSummaryDAO;
private ICIAGroupingItemDAO ciaGroupingItemDAO;
private DealerPreferenceDAO dealerPrefDAO;
private GroupingDescriptionDAO groupingDescriptionDAO;

private Logger logger = Logger.getLogger( CIAGroupingItemAssembler.class );

public List<CIAGroupingItem> getCIAGroupingItems( int businessUnitId, int segmentId, int ciaCategoryId )
{
	Integer summaryId = getCiaSummaryDAO().retrieveCIASummaryId( new Integer( businessUnitId ), Status.CURRENT );
	return getCiaGroupingItemDAO().findBy( summaryId, new Integer( segmentId ), new Integer( ciaCategoryId ) );
}

public CIAGroupingItem getCIAGroupingItemByGroupingDescription( Integer segmentId, Integer ciaCategoryId, Integer groupingDescriptionId, Integer dealerId )
{
	return getCiaGroupingItemDAO().findByGroupingDescriptionSegmentAndCIACategory( segmentId, ciaCategoryId, groupingDescriptionId, dealerId );
}

public CIAGroupingItem constructCIAGroupingItemFromBuyAmountAndPreferences( Integer ciaGroupingItemId, Integer groupingDescriptionId,
																			Map parameterMap, CIACategory ciaCategory, int buyAmount,
																			Integer ciaBodyTypeDetailId )
{
	CIAGroupingItem groupingItem = constructCIAGroupingItemFromPreferences( ciaGroupingItemId, groupingDescriptionId, parameterMap,
																			ciaCategory, ciaBodyTypeDetailId );
	constructNewCIAGroupingItemDetailForBuyAmount( buyAmount, groupingItem );

	return groupingItem;
}

public CIAGroupingItem constructCIAGroupingItemFromPreferences( Integer ciaGroupingItemId, Integer groupingDescriptionId, Map parameterMap,
																CIACategory ciaCategory, Integer ciaBodyTypeDetailId )
{
	CIAGroupingItem groupingItem = getCiaGroupingItemDAO().findNonLazyByCIAGroupingItemId( ciaGroupingItemId );
	if ( groupingItem == null )
	{
		groupingItem = constructNewCIAGroupingItem( groupingDescriptionId, ciaCategory, ciaBodyTypeDetailId );
	}

	List groupingItems = new ArrayList();
	groupingItems.add( groupingItem );

	Set paramNameSet = parameterMap.keySet();
	Collection noteKeys = CollectionUtils.select( paramNameSet, new ParamFilterPredicate( "notes" ) );

	try
	{
		CIAReportingUtility.updateDetailItemsBasedOnSelectedPreferences( parameterMap, groupingItems );
	}
	catch ( Exception e )
	{
		logger.warn( "The cia preferences for grouping item "
				+ groupingItem.getCiaGroupingItemId() + " could not be added due to a failure to parse the request parameters", e );
		return null;
	}
	CIAReportingUtility.createNotes( noteKeys, parameterMap, groupingItems );
	groupingItem = (CIAGroupingItem)groupingItems.get( 0 );

	return groupingItem;

}

public CIAGroupingItem constructCIAGroupingItemFromBuyingPreferences( CIAGroupingItem groupingItem, Map parameterMap, CIACategory ciaCategory,
																		Integer ciaBodyTypeDetailId )
{

	Set paramNameSet = parameterMap.keySet();

	// TODO change this ucp stuff which used to stand for "Unit Cost Prefs" to
	// something else.
	Collection noteKeys = CollectionUtils.select( paramNameSet, new ParamFilterPredicate( "notes" ) );
	
	List tempList = new ArrayList();
	tempList.add( groupingItem );

	try
	{
		CIAReportingUtility.updateDetailItemsBasedOnBuyingPreferences( parameterMap, tempList,
																		groupingItem.getCiaGroupingItemId() );
	}
	catch ( Exception e )
	{
		logger.warn( "The cia preferences for grouping item "
				+ groupingItem.getCiaGroupingItemId() + " could not be added due to a failure to parse the request parameters", e );
		return null;
	}
	CIAReportingUtility.createNotes( noteKeys, parameterMap, tempList );
	groupingItem = (CIAGroupingItem)tempList.get( 0 );

	return groupingItem;

}

private void constructNewCIAGroupingItemDetailForBuyAmount( int buyAmount, CIAGroupingItem groupingItem )
{
	CIAGroupingItemDetail buyDetail = new CIAGroupingItemDetail();
	buyDetail.setDateCreated( new Date() );
	buyDetail.setCiaGroupingItem( groupingItem );
	buyDetail.setUnits( new Integer( buyAmount ) );
	buyDetail.setDetailLevelValue(null);
	buyDetail.setCiaGroupingItemDetailLevel( CIAGroupingItemDetailLevel.GROUPING );
	buyDetail.setCiaGroupingItemDetailType( CIAGroupingItemDetailType.BUY );
	groupingItem.addCiaGroupingItemDetail( buyDetail );
}

private CIAGroupingItem constructNewCIAGroupingItem( Integer groupingDescriptionId, CIACategory ciaCategory, Integer ciaBodyTypeDetailId )
{
	CIAGroupingItem groupingItem;
	GroupingDescription gd = new GroupingDescription();
	gd.setGroupingDescriptionId( groupingDescriptionId );
	gd.setGroupingDescription( "" );
	groupingItem = new CIAGroupingItem();
	groupingItem.setCiaGroupingItemId( new Integer( 0 ) );
	groupingItem.setCiaCategory( ciaCategory );
	groupingItem.setGroupingDescription( gd );
	CIABodyTypeDetail bodyTypeDetail = new CIABodyTypeDetail();
	bodyTypeDetail.setCiaBodyTypeDetailId( ciaBodyTypeDetailId );
	groupingItem.setCiaBodyTypeDetail( bodyTypeDetail );
	return groupingItem;
}

public ICIAGroupingItemDAO getCiaGroupingItemDAO()
{
	return ciaGroupingItemDAO;
}

public void setCiaGroupingItemDAO( ICIAGroupingItemDAO ciaGroupingItemRetriever )
{
	this.ciaGroupingItemDAO = ciaGroupingItemRetriever;
}

public DealerPreferenceDAO getDealerPrefDAO()
{
	return dealerPrefDAO;
}

public void setDealerPrefDAO( DealerPreferenceDAO dealerPreferenceDAO )
{
	this.dealerPrefDAO = dealerPreferenceDAO;
}

public CIASummaryDAO getCiaSummaryDAO()
{
	return ciaSummaryDAO;
}

public void setCiaSummaryDAO( CIASummaryDAO ciaSummaryDAO )
{
	this.ciaSummaryDAO = ciaSummaryDAO;
}

public GroupingDescriptionDAO getGroupingDescriptionDAO()
{
	return groupingDescriptionDAO;
}

public void setGroupingDescriptionDAO( GroupingDescriptionDAO groupingDescriptionDAO )
{
	this.groupingDescriptionDAO = groupingDescriptionDAO;
}

}
