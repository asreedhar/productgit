package com.firstlook.service.cia.report;

import java.util.List;

import biz.firstlook.cia.model.CIACategory;
import biz.firstlook.cia.model.CIASummary;

public interface ICIASummaryReportAssembler
{

public List retrieveCIASummaryPageDisplayData( Integer dealerId, Integer segmentId, CIACategory ciaCategory,
												List actualInventoryItems );

public CIASummary retrieveCIASummary( Integer businessUnitId, Integer ciaSummaryStatusId );

}