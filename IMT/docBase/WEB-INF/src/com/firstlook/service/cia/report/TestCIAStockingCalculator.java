package com.firstlook.service.cia.report;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.apache.commons.collections.MultiHashMap;

import biz.firstlook.cia.model.CIAGroupingItem;
import biz.firstlook.cia.model.CIAGroupingItemDetail;
import biz.firstlook.transact.persist.model.GroupingDescription;
import biz.firstlook.transact.persist.model.MakeModelGrouping;
import biz.firstlook.transact.persist.model.Vehicle;

import com.firstlook.entity.InventoryEntity;

public class TestCIAStockingCalculator extends TestCase
{

public void testMulitHashMapCreationAndMapping()
{
	List inventoryList = new ArrayList();
	
	for (int x = 0; x < 3; x++)
	{
		inventoryList.add(getNewInventoryObject(5, 1999));
	}
	inventoryList.add(getNewInventoryObject(5, 2001));
	
	inventoryList.add(getNewInventoryObject(7, 1999));

	inventoryList.add(getNewInventoryObject(7, 2001));
	inventoryList.add(getNewInventoryObject(7, 2001));
	
	MultiHashMap mappedInventory = CIAReportingUtility.mapInventoryByYear(inventoryList);
	
	
	CIAGroupingItem groupingItem = new CIAGroupingItem();
	CIAGroupingItemDetail groupingItemDetail = new CIAGroupingItemDetail();
	
	GroupingDescription groupingDescription = new GroupingDescription();
	groupingDescription.setGroupingDescriptionId(new Integer(5));
	groupingItem.setGroupingDescription(groupingDescription);
	groupingItemDetail.setDetailLevelValue("1999");
	assertEquals(3, CIAReportingUtility.retrieveUnitsInStockForGroupDescriptionAndYear(groupingItem, groupingItemDetail, mappedInventory));
	
	groupingDescription.setGroupingDescriptionId(new Integer(7));
	groupingItem.setGroupingDescription(groupingDescription);
	assertEquals(1, CIAReportingUtility.retrieveUnitsInStockForGroupDescriptionAndYear(groupingItem, groupingItemDetail, mappedInventory));

	groupingItemDetail.setDetailLevelValue("2001");
	assertEquals(2, CIAReportingUtility.retrieveUnitsInStockForGroupDescriptionAndYear(groupingItem, groupingItemDetail, mappedInventory));

	groupingDescription.setGroupingDescriptionId(new Integer(5));
	groupingItem.setGroupingDescription(groupingDescription);
	assertEquals(1, CIAReportingUtility.retrieveUnitsInStockForGroupDescriptionAndYear(groupingItem, groupingItemDetail, mappedInventory));

}

private InventoryEntity getNewInventoryObject( int mmgroupingValue, int year )
{
	InventoryEntity inventory = new InventoryEntity();
	inventory.setVehicle(setMMGroupingAndYear(mmgroupingValue, year));
	return inventory;
}

private Vehicle setMMGroupingAndYear(int mmgrouingValue, int year)
{
	Vehicle vehicle = new Vehicle();
	GroupingDescription groupingDescription = new GroupingDescription();
	groupingDescription.setGroupingDescriptionId(new Integer(mmgrouingValue));
	MakeModelGrouping mmgrouping = new MakeModelGrouping();
	mmgrouping.setGroupingDescription(groupingDescription);
	vehicle.setMakeModelGrouping(mmgrouping);
	vehicle.setVehicleYear( new Integer( year ) );
	return vehicle;
}

}
