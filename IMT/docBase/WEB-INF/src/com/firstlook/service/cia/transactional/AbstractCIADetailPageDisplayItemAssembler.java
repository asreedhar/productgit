package com.firstlook.service.cia.transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.MultiHashMap;
import org.apache.commons.collections.comparators.ReverseComparator;
import org.apache.log4j.Logger;

import biz.firstlook.cia.model.CIAGroupingItem;
import biz.firstlook.cia.model.CIAGroupingItemDetail;
import biz.firstlook.cia.model.CIAGroupingItemDetailLevel;
import biz.firstlook.cia.model.CIAGroupingItemDetailType;
import biz.firstlook.commons.services.vehicleCatalog.IVehicleCatalogService;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogServiceException;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.GroupingDescription;
import biz.firstlook.transact.persist.model.Inventory;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;
import biz.firstlook.transact.persist.persistence.GroupingDescriptionDAO;
import biz.firstlook.transact.persist.persistence.IInventoryDAO;

import com.firstlook.display.cia.CIABuyingPlanForGroupingItemDetail;
import com.firstlook.display.cia.CIAGroupingItemDisplayData;
import com.firstlook.util.Formatter;

/**
 * Implements common functionality between powerzone and non-powerzone implementations for assembling data for detail page display items
 * returned to the CIAService.
 */

public abstract class AbstractCIADetailPageDisplayItemAssembler implements ICIADetailPageDisplayItemAssembler
{

private static final Logger logger = Logger.getLogger( AbstractCIADetailPageDisplayItemAssembler.class );

private static final int OVERSTOCKED = 1;
private static final int UNDERSTOCKED = 2;
private static final int NON_OVERSTOCKED = 3;
private static final int NON_UNDERSTOCKED = 4;
protected DealerPreferenceDAO dealerPrefDAO;
protected GroupingDescriptionDAO groupingDescriptionDAO;
protected IInventoryDAO inventoryDAO;
protected IVehicleCatalogService vehicleCatalogService;

public AbstractCIADetailPageDisplayItemAssembler()
{
	super();
}

public List getCIADetailPageDisplayItems( int businessUnitId, List ciaGroupingItems )
{
	return createCIADetailPageGroupingItemDisplayData( businessUnitId, ciaGroupingItems );
}

protected List determineUnderstockedYears( List yearDetailsWithRecs )
{
	return determineYearsWithRecType( yearDetailsWithRecs, UNDERSTOCKED );
}

protected List determineNonUnderstockedYears( List yearDetailsWithRecs )
{
	return determineYearsWithRecType( yearDetailsWithRecs, NON_UNDERSTOCKED );
}

protected List determineOverstockedYears( List yearDetailsWithRecs )
{
	return determineYearsWithRecType( yearDetailsWithRecs, OVERSTOCKED );
}

private List determineYearsWithRecType( List yearDetailsWithRecs, int stockingRecType )
{
	List<CIAGroupingItemDetail> returnList = new ArrayList<CIAGroupingItemDetail>();

	Iterator yearsIter = yearDetailsWithRecs.iterator();
	CIAGroupingItemDetail yearDetail = null;
	while ( yearsIter.hasNext() )
	{
		yearDetail = (CIAGroupingItemDetail)yearsIter.next();
		switch ( stockingRecType )
		{
			case OVERSTOCKED:
				if ( yearDetail.isHasOverstocked() )
				{
					returnList.add( yearDetail );
				}
				break;
			case UNDERSTOCKED:
				if ( yearDetail.isHasUnderstocked() )
				{
					returnList.add( yearDetail );
				}
				break;
			case NON_OVERSTOCKED:
				if ( !yearDetail.isHasOverstocked() )
				{
					returnList.add( yearDetail );
				}
				break;
			case NON_UNDERSTOCKED:
				if ( !yearDetail.isHasUnderstocked() )
				{
					returnList.add( yearDetail );
				}
				break;
		}
	}

	return returnList;
}

private List createCIADetailPageGroupingItemDisplayData( int businessUnitId, List ciaGroupingItems )
{
	int unitsInStock = 0;
	boolean includeItem = false;
	Iterator ciaGroupingIter = ciaGroupingItems.iterator();
	CIAGroupingItemDisplayData displayGroupingItem = null;
	List<CIAGroupingItemDisplayData> displayGroupingItems = new ArrayList<CIAGroupingItemDisplayData>();
	List<CIAGroupingItemDetail> yearDetailsWithRecs = null;
	while ( ciaGroupingIter.hasNext() )
	{
		CIAGroupingItem ciaGroupingItem = (CIAGroupingItem)ciaGroupingIter.next();

		displayGroupingItem = new CIAGroupingItemDisplayData(	ciaGroupingItem.getCiaGroupingItemId(),
																ciaGroupingItem.getGroupingDescription().getGroupingDescriptionId(),
																ciaGroupingItem.getNotes(),
																ciaGroupingItem.getMarketPenetration(),
																ciaGroupingItem.getCiaCategory(),
																ciaGroupingItem.getMarketShare() );

		// Get list of years with under/over stocking recommendations and sum
		// total.
		// Example: "2005 understocked by 2"
		yearDetailsWithRecs = new ArrayList<CIAGroupingItemDetail>();
		int totalTargetInventory = sumTotalTargetInventoryAndDetermineYearsWithStocking( ciaGroupingItem.getCiaGroupingItemDetails(),
																							yearDetailsWithRecs );
		displayGroupingItem.setTargetInventory( totalTargetInventory );
		displayGroupingItem.setValuation( ciaGroupingItem.getValuation().intValue() );

		DealerPreference dealerPreference = getDealerPrefDAO().findByBusinessUnitId( new Integer( businessUnitId ) );
		calculateStockingOnYearRanges( businessUnitId, dealerPreference, displayGroupingItem.getGroupingDescriptionId(), yearDetailsWithRecs );

		// filters list to only include only years with appropriate stocking
		// recommendations, sets dropdown years for Winners/Goodbets
		// need to get at CIADetailPageDisplayItemAssembler here for these
		// methods

		filterYearsByStocking( displayGroupingItem, yearDetailsWithRecs );

		// Get list of years this grouping Item was manufactured/sold
		// TODO reconcile this list with the buyingPlan map which should have
		// similar year information.
		List yearsList = getValidYearsCarWasSold( ciaGroupingItem.getGroupingDescription().getGroupingDescriptionId().intValue(), 10 );
		displayGroupingItem.setYearsStringsList( yearsList );

		// Get any existing buying plan based on years
		// TODO actuall get an existing buying plan.
		Map buyingPlanMap = this.getBuyingPlanMap( ciaGroupingItem.getCiaGroupingItemDetails(), yearsList );
		displayGroupingItem.setBuyingPlanMap( buyingPlanMap );

		// Goodbets and winners will only display items with understocked ranges
		includeItem = determineIncludeItem( displayGroupingItem );

		if ( includeItem )
		{
			// Ignore lower unit cost threshold: this is getting the active count of inventory as the user sees the page.
			unitsInStock = inventoryDAO.findCountOfActiveByGroupingDescription( new Integer( businessUnitId ),
																				displayGroupingItem.getGroupingDescriptionId(),
																				Inventory.USED_CAR, Integer.MIN_VALUE,
																				dealerPreference.getUnitCostThresholdUpper() );
			displayGroupingItem.setUnitsInStock( unitsInStock );

			// manage bits and pieces
			GroupingDescription groupingDescription = getGroupingDescriptionDAO().findByID( displayGroupingItem.getGroupingDescriptionId() );
			displayGroupingItem.setGroupingDescriptionString( groupingDescription.getGroupingDescription() );
			displayGroupingItem.setMarketPenetrationFormatted( Formatter.toPercentOneDecimal( displayGroupingItem.getMarketPenetration() != null ? displayGroupingItem.getMarketPenetration().doubleValue()
					: 0.0d ) );
			displayGroupingItems.add( displayGroupingItem );
		}
	}
	// Sort by valuation, highest to lowest value
	ReverseComparator valuationComparator = new ReverseComparator( new BeanComparator( "valuation" ) );
	Collections.sort( displayGroupingItems, valuationComparator );

	return displayGroupingItems;
}

protected Map getBuyingPlanMap( Set ciaGroupingItemDetails, List yearsList )
{
	Map<String, CIABuyingPlanForGroupingItemDetail> buyingPlanMap = new HashMap<String, CIABuyingPlanForGroupingItemDetail>();

	String year;
	Iterator iterator = yearsList.iterator();
	CIABuyingPlanForGroupingItemDetail planForDetail = null;
	int buyAmount;
	while ( iterator.hasNext() )
	{
		year = (String)iterator.next();

		planForDetail = new CIABuyingPlanForGroupingItemDetail();
		planForDetail.setValue( year );
		buyAmount = getBuyAmountFromGroupingItemDetails( ciaGroupingItemDetails, year );
		planForDetail.setBuyAmount( buyAmount );

		buyingPlanMap.put( year, planForDetail );
	}

	return buyingPlanMap;
}

private int getBuyAmountFromGroupingItemDetails( Set details, String year )
{
	if ( details == null )
		return 0;

	Iterator iterator = details.iterator();
	CIAGroupingItemDetail detail;
	while ( iterator.hasNext() )
	{
		detail = (CIAGroupingItemDetail)iterator.next();

		if ( !detail.getCiaGroupingItemDetailLevel().equals( CIAGroupingItemDetailLevel.YEAR ) )
			continue;

		if ( !detail.getDetailLevelValue().equals( year ) )
			continue;

		if ( !( detail.getCiaGroupingItemDetailType().getCiaGroupingItemDetailTypeId().intValue() == CIAGroupingItemDetailType.BUY_TYPE ) )
			continue;

		if ( detail.getUnits() == null )
			continue;

		if ( detail.getUnits().intValue() <= 0 )
			continue;

		return detail.getUnits().intValue();
	}

	return 0;
}

List getValidYearsCarWasSold( int groupingId, int howManyYearsPrevious )
{
	List yearsList;
	try
	{
		// Get all the years this vehicle was manufactured, in descending order
		yearsList = vehicleCatalogService.retrieveModelYears( groupingId, false );
	}
	catch ( VehicleCatalogServiceException e )
	{
		yearsList = new ArrayList();
		logger.error( "Error retrieving model years from vehicle catalog for grouping id:" + groupingId );
	}

	// Only return the last 10 years.
	String year;
	Iterator iterator = yearsList.iterator();
	int oldestYear = ( Calendar.getInstance().get(Calendar.YEAR) ) - howManyYearsPrevious;
	List<String> editedYearsList = new ArrayList<String>();
	while ( iterator.hasNext() )
	{
		year = (String)iterator.next();
		if ( Integer.parseInt( year ) >= oldestYear )
			editedYearsList.add( year );
	}

	return editedYearsList;
}

/**
 * Populates yearsWithRecs argument with ciaGroupingItemDetails, and returns sum of all vehicles for all years.
 */
private int sumTotalTargetInventoryAndDetermineYearsWithStocking( Set ciaGroupingItemDetails, List<CIAGroupingItemDetail> yearsWithRecs )
{
	int totalTargetInventory = 0;
	Iterator ciaGroupingItemDetailsIter = ciaGroupingItemDetails.iterator();
	while ( ciaGroupingItemDetailsIter.hasNext() )
	{
		CIAGroupingItemDetail detail = (CIAGroupingItemDetail)ciaGroupingItemDetailsIter.next();
		if ( ( detail.getCiaGroupingItemDetailLevel().getCiaGroupingItemDetailLevelId().intValue() == CIAGroupingItemDetailLevel.YEAR.getCiaGroupingItemDetailLevelId().intValue() || detail.getCiaGroupingItemDetailLevel().getCiaGroupingItemDetailLevelId().intValue() == CIAGroupingItemDetailLevel.GROUPING.getCiaGroupingItemDetailLevelId().intValue() )
				&& detail.getCiaGroupingItemDetailType().getCiaGroupingItemDetailTypeId().intValue() == CIAGroupingItemDetailType.OPTIMAL_STOCKING_LEVEL.getCiaGroupingItemDetailTypeId().intValue() )
		{
			totalTargetInventory += detail.getUnits().intValue();
			yearsWithRecs.add( detail );
		}
	}

	return totalTargetInventory;
}

private void calculateStockingOnYearRanges( int businessUnitId, DealerPreference dealerPreference, Integer groupingDescriptionId,
											List yearsWithStocking )
{
	List allInventoryPlusYears = inventoryDAO.findActiveByGroupingDescription( new Integer( businessUnitId ), groupingDescriptionId,
																				Inventory.USED_CAR,
																				dealerPreference.getUnitCostThresholdLower(),
																				dealerPreference.getUnitCostThresholdUpper() );

	calculateUnitsInStockAndInventoryGaps( allInventoryPlusYears, yearsWithStocking );

}

private void calculateUnitsInStockAndInventoryGaps( List allInventoryPlusYears, List yearsWithRecs )
{
	MultiHashMap yearInventoryMap = new MultiHashMap();
	Iterator allInventoryPlusYearsIter = null;
	Iterator yearsIterator = yearsWithRecs.iterator();
	Inventory inventory = null;
	CIAGroupingItemDetail yearDetail = null;
	int inventoryGap = 0;
	Integer year = null;
	Object[] inventoryWithYear = null;

	allInventoryPlusYearsIter = allInventoryPlusYears.iterator();
	while ( allInventoryPlusYearsIter.hasNext() )
	{
		inventoryWithYear = (Object[])allInventoryPlusYearsIter.next();
		inventory = (Inventory)inventoryWithYear[0];
		year = (Integer)inventoryWithYear[1];

		yearInventoryMap.put( year, inventory );
	}

	int activeInventoryForYear = 0;
	while ( yearsIterator.hasNext() )
	{
		yearDetail = (CIAGroupingItemDetail)yearsIterator.next();

		// For model level rec
		if ( yearDetail.getCiaGroupingItemDetailLevel().getCiaGroupingItemDetailLevelId().intValue() == CIAGroupingItemDetailLevel.GROUPING_DETAIL_LEVEL )
		{
			activeInventoryForYear = allInventoryPlusYears.size();
			yearDetail.setDetailLevelValue( "Model" );
		}
		else
		{
			year = new Integer( yearDetail.getDetailLevelValue() );
			if ( yearInventoryMap.get( year ) != null )
			{
				activeInventoryForYear = ( (ArrayList)yearInventoryMap.get( year ) ).size();
			}
			else
			{
				activeInventoryForYear = 0;
			}
		}
		inventoryGap = yearDetail.getUnits().intValue() - activeInventoryForYear;
		yearDetail.setUnderstocked( inventoryGap );
		yearDetail.setUnitsInStock( activeInventoryForYear );
	}

}

public abstract boolean determineIncludeItem( CIAGroupingItemDisplayData displayGroupingItem );

public abstract void filterYearsByStocking( CIAGroupingItemDisplayData displayGroupingItem, List yearDetailsWithRecs );

public GroupingDescriptionDAO getGroupingDescriptionDAO()
{
	return groupingDescriptionDAO;
}

public void setGroupingDescriptionDAO( GroupingDescriptionDAO groupingDescriptionDAO )
{
	this.groupingDescriptionDAO = groupingDescriptionDAO;
}

public DealerPreferenceDAO getDealerPrefDAO()
{
	return dealerPrefDAO;
}

public void setDealerPrefDAO( DealerPreferenceDAO dealerPreferenceDAO )
{
	this.dealerPrefDAO = dealerPreferenceDAO;
}

public IInventoryDAO getInventoryDAO()
{
	return inventoryDAO;
}

public void setInventoryDAO( IInventoryDAO inventoryDAO )
{
	this.inventoryDAO = inventoryDAO;
}

public void setVehicleCatalogService( IVehicleCatalogService vehicleCatalogService )
{
	this.vehicleCatalogService = vehicleCatalogService;
}

}
