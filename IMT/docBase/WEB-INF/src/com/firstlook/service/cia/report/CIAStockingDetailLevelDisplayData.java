package com.firstlook.service.cia.report;

public class CIAStockingDetailLevelDisplayData implements Comparable<CIAStockingDetailLevelDisplayData>
{

private String detailLevelValue;
private int targetInventory;
private int unitsInStock;
private int understocked;
private int overstocked;

public CIAStockingDetailLevelDisplayData()
{
	super();
}

public boolean getIsOverStocked()
{
	return ( unitsInStock > targetInventory );
}

public boolean getIsUnderStocked()
{
	return ( unitsInStock < targetInventory );
}

public int getTargetInventory()
{
	return targetInventory;
}

public void setTargetInventory( int targetInventory )
{
	this.targetInventory = targetInventory;
}

public int getUnitsInStock()
{
	return unitsInStock;
}

public void setUnitsInStock( int unitsInStock )
{
	this.unitsInStock = unitsInStock;
}

public String getDetailLevelValue()
{
	return detailLevelValue;
}

public void setDetailLevelValue( String year )
{
	this.detailLevelValue = year;
}

public void setOverUnderStock()
{
	if ( getIsOverStocked() )
	{
		overstocked = unitsInStock - targetInventory;
		understocked = 0;
	}
	else if ( getIsUnderStocked() )
	{
		understocked = targetInventory - unitsInStock;
		overstocked = 0;
	}
}

public int getStockingDifference()
{
	return unitsInStock - targetInventory;
}

public int getOverstocked()
{
	return overstocked;
}

public void setOverstocked( int overstocked )
{
	this.overstocked = overstocked;
}

public int getUnderstocked()
{
	return understocked;
}

public void setUnderstocked( int understocked )
{
	this.understocked = understocked;
}

public int compareTo( CIAStockingDetailLevelDisplayData inData )
{
	return inData.detailLevelValue.compareTo( detailLevelValue );
}

}
