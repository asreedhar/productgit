package com.firstlook.service.cia.transactional;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;
import biz.firstlook.cia.model.CIAGroupingItemDetail;
import biz.firstlook.cia.model.CIAGroupingItemDetailLevel;
import biz.firstlook.cia.model.CIAGroupingItemDetailType;

public class TestAbstractCIAGroupingItemAssembler extends TestCase
{

List understockedYears;
CIANonPowerzoneDetailPageDisplayItemAssembler assembler;

public TestAbstractCIAGroupingItemAssembler( String arg0 )
{
	super( arg0 );
	assembler = new CIANonPowerzoneDetailPageDisplayItemAssembler();
 	understockedYears = new ArrayList();
	CIAGroupingItemDetailLevel yearLevel = CIAGroupingItemDetailLevel.YEAR;
	CIAGroupingItemDetailType recType = CIAGroupingItemDetailType.OPTIMAL_STOCKING_LEVEL;
	
	for (int i = 0; i < 3; i++) 
	{
		CIAGroupingItemDetail detail = new CIAGroupingItemDetail();
		buildGroupingItemDetail( detail, "200" + i, yearLevel, recType );
		understockedYears.add(i,detail);
	}
}

public void testRemoveUnderstockedYears()
{
	List allYears = new ArrayList();
	for ( int i = 0; i < 5 ; i++ )
	{
		allYears.add( "200" + i );
	}

	
	assembler.removeUnderstockedYears( allYears, understockedYears );
	assertEquals( "should be size 2", 2, allYears.size() );
	assertEquals( "should be 2003", "2003", allYears.get(0));
	assertEquals( "should be 2004", "2004", allYears.get(1));
}

private void buildGroupingItemDetail( CIAGroupingItemDetail detail, 
                                      String detailLevelValue,
                                      CIAGroupingItemDetailLevel yearLevel, 
                                      CIAGroupingItemDetailType recType )
{
	detail.setDetailLevelValue( detailLevelValue );
	detail.setCiaGroupingItemDetailLevel( yearLevel );
	detail.setCiaGroupingItemDetailType( recType );
}

}
