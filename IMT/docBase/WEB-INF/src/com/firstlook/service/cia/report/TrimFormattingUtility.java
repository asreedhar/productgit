package com.firstlook.service.cia.report;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import biz.firstlook.commons.services.vehicleCatalog.IVehicleCatalogService;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogServiceException;
import biz.firstlook.transact.persist.model.MakeModelGrouping;
import biz.firstlook.transact.persist.service.MakeModelGroupingService;
import biz.firstlook.transact.persist.service.TrimLineItem;

public class TrimFormattingUtility
{

private static final Logger logger = Logger.getLogger( TrimFormattingUtility.class );
	
private static final String SPACE_HOLDER = "xSPACEx";
private static final String APOS_HOLDER = "xAPOSx";
private static final String DASH_HOLDER = "xDASHx";
private static final String FSLASH_HOLDER = "xFSLASHx";

private IVehicleCatalogService vehicleCatalogService;
private MakeModelGroupingService makeModelGroupingService;


/**
 * THIS CLASS IS NOT SPRING MANAGED - BOTH SPRING MANAGED SERVICES WERE INJECTED AT THE ACTION LEVEL
 */
public TrimFormattingUtility()
{
	super();
}

public List retrieveAllTrimsJSEscaped( int groupingDescriptionId )
{
	Collection<String> trims = retrieveUniqueTrimsByGroupingDescriptionId( groupingDescriptionId );
	return createFormattedTrimLineItems( trims );
}


public Set<String> retrieveUniqueTrimsByGroupingDescriptionId( int groupingDescriptionId )
{
	Set<String> returnSet = new HashSet<String>();
	List<MakeModelGrouping> makeModelGroupingNoBodyTypes = makeModelGroupingService.retrieveMakeModelGroupings( groupingDescriptionId );
	List uniqueMakeModels = makeUnique( makeModelGroupingNoBodyTypes );
	Iterator makeModelsIter = uniqueMakeModels.iterator();
	MakeModelGrouping mmg = null;
	try {
		while ( makeModelsIter.hasNext() )
		{
			mmg = (MakeModelGrouping)makeModelsIter.next();
			returnSet.addAll( vehicleCatalogService.retrieveAllModelTrims( mmg.getMake(), mmg.getModel() ) );
		}
	} catch (VehicleCatalogServiceException e) {
		logger.error("Error retreiving all model trims from vehicle catalog");
	}

	return returnSet;
}

List<MakeModelGrouping> makeUnique( List<MakeModelGrouping> makeModelGroupingNoBodyTypes )
{
	List<MakeModelGrouping> uniqueMakeModels = new ArrayList<MakeModelGrouping>();
	MakeModelGrouping mmg = null;

	Iterator<MakeModelGrouping> iter = makeModelGroupingNoBodyTypes.iterator();
	while ( iter.hasNext() )
	{
		MakeModelGrouping makeModel = new MakeModelGrouping();
		mmg = iter.next();
		makeModel.setMake( mmg.getMake() );
		makeModel.setModel( mmg.getModel() );
		if ( !uniqueMakeModels.contains( makeModel ) )
		{
			uniqueMakeModels.add( makeModel );
		}
	}

	return uniqueMakeModels;
}





public List<TrimLineItem> createFormattedTrimLineItems( Collection<String> trims )
{
	List<TrimLineItem> trimsFormatted = new ArrayList<TrimLineItem>();
	Iterator<String> trimsIter = trims.iterator();
	while ( trimsIter.hasNext() )
	{
		String trim = trimsIter.next();
		String trimFormatted = escapeJS( trim );

		TrimLineItem lineItem = new TrimLineItem();
		lineItem.setTrim( trim );
		lineItem.setTrimFormatted( trimFormatted );

		trimsFormatted.add( lineItem );
	}

	Collections.sort( trimsFormatted, new BeanComparator( "trimFormatted" ) );
	return trimsFormatted;
}

public static String escapeJS( String trim )
{
	trim = StringUtils.replace( trim, "-", DASH_HOLDER );
	trim = StringUtils.replace( trim, " ", SPACE_HOLDER );
	trim = StringUtils.replace( trim, "'", APOS_HOLDER );
	trim = StringUtils.replace( trim, "/", FSLASH_HOLDER );

	return trim;
}

public static String unEscapeJS( String trim )
{
	trim = StringUtils.replace( trim, DASH_HOLDER, "-" );
	trim = StringUtils.replace( trim, SPACE_HOLDER, " " );
	trim = StringUtils.replace( trim, APOS_HOLDER, "'" );
	trim = StringUtils.replace( trim, FSLASH_HOLDER, "/" );

	return trim;
}

public static List transformTrimNames( List<String> trimNames )
{
	Iterator<String> trimNameIter = trimNames.iterator();
	List<String> newTrims = new ArrayList<String>();
	String trim;
	while ( trimNameIter.hasNext() )
	{
		trim = trimNameIter.next();
		trim = unEscapeJS( trim );
		newTrims.add( trim );
	}

	return newTrims;
}

public void setVehicleCatalogService( IVehicleCatalogService vehicleCatalogService )
{
	this.vehicleCatalogService = vehicleCatalogService;
}

public void setMakeModelGroupingService( MakeModelGroupingService makeModelGroupingService )
{
	this.makeModelGroupingService = makeModelGroupingService;
}

}
