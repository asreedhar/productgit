package com.firstlook.service.cia.report;

import java.util.Collection;
import java.util.List;

import biz.firstlook.cia.model.CIACategory;
import biz.firstlook.cia.model.CIASummary;

public class CIAReportingDelegate
{

private ICIASummaryReportAssembler ciaSummaryReportAssembler;
private ICIABuyingPlanReportAssembler ciaBuyingPlanReportAssembler;
private ICIAStockingReportAssembler ciaStockingReportAssembler;
private ICIAPurchasingDataAssembler ciaPurchasingDataAssembler;

public CIAReportingDelegate()
{
	super();
}

public List getCIASummaryDisplayData( Integer dealerId, Integer segmentId, CIACategory ciaCategory, List actualInventoryItems )
{
	return getCiaSummaryReportAssembler().retrieveCIASummaryPageDisplayData( dealerId, segmentId,
	  																			ciaCategory, actualInventoryItems );
}

public List getCIABuyingPlanData( Integer dealerId, Integer ciaSummaryStatusId )
{
	return getCiaBuyingPlanReportAssembler().retrieveCIABuyingPlanData( dealerId, ciaSummaryStatusId );
}

public Collection getCIABuyingPlanRawData( Integer dealerId, Integer ciaSummaryStatusId )
{
	return getCiaPurchasingDataAssembler().retrieveCIABuyingPlanData( dealerId, ciaSummaryStatusId );
}

public Collection getUnderstockedCIARecommendationsByCategoryType( Integer businessUnitId, Integer ciaSummaryStatusId, Integer ciaCategoryId )
{
    return getCiaPurchasingDataAssembler().retrieveUnderstockedCIARecommendationsByCategoryType( businessUnitId, ciaSummaryStatusId, ciaCategoryId );
}

public CIASummary getCIASummary( Integer businessUnitId, Integer ciaSummaryStatusId )
{
	return getCiaSummaryReportAssembler().retrieveCIASummary( businessUnitId, ciaSummaryStatusId );
}

public ICIASummaryReportAssembler getCiaSummaryReportAssembler()
{
	return ciaSummaryReportAssembler;
}

public void setCiaSummaryReportAssembler( ICIASummaryReportAssembler ciaSummaryReportService )
{
	this.ciaSummaryReportAssembler = ciaSummaryReportService;
}

public ICIABuyingPlanReportAssembler getCiaBuyingPlanReportAssembler()
{
	return ciaBuyingPlanReportAssembler;
}

public void setCiaBuyingPlanReportAssembler( ICIABuyingPlanReportAssembler ciaBuyingPlanReportAssembler )
{
	this.ciaBuyingPlanReportAssembler = ciaBuyingPlanReportAssembler;
}

public List getCIAStockingData(Integer businessUnitId, int stockingType)
{
	return ciaStockingReportAssembler.retrieveCIAStockingData(businessUnitId, stockingType);
}

//public abstract List getCIAStockingData(Integer businessUnitId, String stockingType) throws ApplicationException;

public ICIAStockingReportAssembler getCiaStockingReportAssembler()
{
	return ciaStockingReportAssembler;
}

public void setCiaStockingReportAssembler( ICIAStockingReportAssembler ciaOverUnderStockingReportAssembler )
{
	this.ciaStockingReportAssembler = ciaOverUnderStockingReportAssembler;
}

public ICIAPurchasingDataAssembler getCiaPurchasingDataAssembler()
{
	return ciaPurchasingDataAssembler;
}
public void setCiaPurchasingDataAssembler( ICIAPurchasingDataAssembler ciaDataAssembler )
{
	this.ciaPurchasingDataAssembler = ciaDataAssembler;
}

}
