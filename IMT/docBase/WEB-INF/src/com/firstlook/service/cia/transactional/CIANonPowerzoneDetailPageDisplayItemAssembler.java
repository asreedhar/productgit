package com.firstlook.service.cia.transactional;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import biz.firstlook.cia.model.CIAGroupingItemDetail;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogServiceException;

import com.firstlook.display.cia.CIAGroupingItemDisplayData;

public class CIANonPowerzoneDetailPageDisplayItemAssembler extends AbstractCIADetailPageDisplayItemAssembler
{

private static final Logger logger = Logger.getLogger( CIANonPowerzoneDetailPageDisplayItemAssembler.class );	
	
public void filterYearsByStocking( CIAGroupingItemDisplayData displayGroupingItem, List yearDetailsWithRecs )
{
	
	//Retrieve all valid years for a given vehicle
	List allYears;
	try {
		allYears = vehicleCatalogService.retrieveModelYears( displayGroupingItem.getGroupingDescriptionId().intValue(), true );
	} catch (VehicleCatalogServiceException e) {
		// TODO Auto-generated catch block
		logger.error("Error retieving Model Years from vehicle catalog for grouping description: " + displayGroupingItem.getGroupingDescriptionId());
		allYears = new ArrayList();
	}

	//Retrieve the list of all years within a grouping item that are understocked relative to the recommendation
	List understockedYears = determineUnderstockedYears( yearDetailsWithRecs );
	
	//Remove any years that are understocked from the list returned 
	removeUnderstockedYears( allYears, understockedYears );
	
	//Populate the displayGroupingItem object with the list of understocked years
	//and the filtered list of list of all years (now contains any years that are NOT understocked)
	displayGroupingItem.setUnderstockedYears( understockedYears );
	displayGroupingItem.setYearsStringsList( allYears );
	
}

void removeUnderstockedYears( List allYears, List understockedYears )
{
	Iterator understockedYearsIter = understockedYears.iterator();
	CIAGroupingItemDetail yearDetail = null;
	int index;
	String detailValue;
	while( understockedYearsIter.hasNext() )
	{
		yearDetail = (CIAGroupingItemDetail)understockedYearsIter.next();
		detailValue = (String)yearDetail.getDetailLevelValue();
		index = allYears.indexOf( detailValue );
		if(index >= 0)
			allYears.remove( index );
	}
}

public boolean determineIncludeItem( CIAGroupingItemDisplayData displayGroupingItem )
{
	List yearList = displayGroupingItem.getUnderstockedYears();
	return yearList.size() > 0;
}


}
