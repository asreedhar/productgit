package com.firstlook.service.cia.report;

import java.util.List;

public class CIAStockingReportGroupingDisplayData
{

String groupingDescription;
List detailLevelDisplayDataItems;
boolean modelLevelRecommendation = false;

public CIAStockingReportGroupingDisplayData()
{
	super();
}

public String getGroupingDescription()
{
	return groupingDescription;
}

public void setGroupingDescription( String groupingDescription )
{
	this.groupingDescription = groupingDescription;
}

public List getDetailLevelDisplayDataItems()
{
	return detailLevelDisplayDataItems;
}

public void setDetailLevelDisplayDataItems( List yearDisplayDataItems )
{
	this.detailLevelDisplayDataItems = yearDisplayDataItems;
}

public boolean isModelLevelRecommendation() {
	return modelLevelRecommendation;
}

public void setModelLevelRecommendation(boolean modelLevelRecommendation) {
	this.modelLevelRecommendation = modelLevelRecommendation;
}

}
