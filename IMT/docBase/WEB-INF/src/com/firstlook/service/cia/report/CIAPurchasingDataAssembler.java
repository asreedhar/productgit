package com.firstlook.service.cia.report;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.MultiHashMap;

import biz.firstlook.cia.model.CIAGroupingItem;
import biz.firstlook.cia.model.CIAGroupingItemDetail;
import biz.firstlook.cia.model.CIAGroupingItemDetailType;
import biz.firstlook.cia.persistence.ICIAGroupingItemDAO;

import com.firstlook.entity.InventoryEntity;
import com.firstlook.persistence.cia.ICIABuyingPlanReportDAO;
import com.firstlook.service.inventory.InventoryService_Legacy;

//MH - 09/22 - this class retrieves CIA-related data for the purchasing center
//             Requests not from the purchasing center should use another assembler
public class CIAPurchasingDataAssembler implements ICIAPurchasingDataAssembler
{
private ICIABuyingPlanReportDAO ciaBuyingPlanReportDAO;
private ICIAGroupingItemDAO ciaGroupingItemDAO;
private InventoryService_Legacy inventoryService_Legacy;

public Collection retrieveCIABuyingPlanData( Integer dealerId, Integer ciaSummaryStatusId )
{
	return getCiaBuyingPlanReportDAO().findDistinctCIAGroupingItemsWithBuyAmounts( dealerId, ciaSummaryStatusId );
}

public Collection<CIAGroupingItem> retrieveUnderstockedCIARecommendationsByCategoryType( Integer businessUnitId, Integer ciaSummaryStatusId,
																		Integer ciaCategoryId )
{
	Collection<CIAGroupingItem> returnList = new ArrayList<CIAGroupingItem>();
	List<InventoryEntity> inventoryItems = (List<InventoryEntity>)inventoryService_Legacy.retrieveGroupingDescriptionIdAndVehicleYearByDealerIdAndInventoryType(
																														businessUnitId.intValue(),
																														InventoryEntity.USED_CAR );
	MultiHashMap actualInventoryMap = CIAReportingUtility.mapInventoryByYear( inventoryItems );
	Collection<CIAGroupingItem> optimalGroupingItems = getCiaGroupingItemDAO().findDistinctCIAGroupingItemsWithRecommendationsByCIACategory( businessUnitId,
																															ciaSummaryStatusId,
																															ciaCategoryId );
	CIAGroupingItem optimalItem;
	Iterator<CIAGroupingItem> optimalItemIter = optimalGroupingItems.iterator();
	Iterator<CIAGroupingItemDetail> detailItemIter;
	CIAGroupingItemDetail itemDetail;
	int unitsInStock;
	int differenceInStock;
	while ( optimalItemIter.hasNext() )
	{
		optimalItem = optimalItemIter.next();
		detailItemIter = optimalItem.getCiaGroupingItemDetails().iterator();
		while ( detailItemIter.hasNext() )
		{
			itemDetail = detailItemIter.next();
			if ( itemDetail.getCiaGroupingItemDetailType().getCiaGroupingItemDetailTypeId().intValue() == CIAGroupingItemDetailType.OPTIMAL_STOCKING_LEVEL_TYPE )
			{
				unitsInStock = CIAReportingUtility.retrieveUnitsInStockForGroupDescriptionAndYear( optimalItem, itemDetail, actualInventoryMap );
				differenceInStock = itemDetail.getUnits().intValue() - unitsInStock;
				if ( differenceInStock > 0 )
				{
					returnList.add( optimalItem );
					break;
				}
			}
		}
	}
	return returnList;
}

public ICIABuyingPlanReportDAO getCiaBuyingPlanReportDAO()
{
	return ciaBuyingPlanReportDAO;
}

public void setCiaBuyingPlanReportDAO( ICIABuyingPlanReportDAO ciaBuyingPlanReportDao )
{
	this.ciaBuyingPlanReportDAO = ciaBuyingPlanReportDao;
}

public ICIAGroupingItemDAO getCiaGroupingItemDAO()
{
	return ciaGroupingItemDAO;
}

public void setCiaGroupingItemDAO( ICIAGroupingItemDAO ciaGroupingItemDao )
{
	this.ciaGroupingItemDAO = ciaGroupingItemDao;
}

public void setInventoryService_Legacy(InventoryService_Legacy inventoryService) {
	this.inventoryService_Legacy = inventoryService;
}


}
