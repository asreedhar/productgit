package com.firstlook.service.cia.transactional;

import java.util.List;

import com.firstlook.display.cia.CIAGroupingItemDisplayData;
/**
 * Provides powerzone specific implementations of the functionality for filtering grouping item details by stocking
 * recommendation and for determining whether or not an item should be included in the list returned to the CIA Service
 */
public class CIAPowerzoneDetailPageDisplayItemAssembler extends AbstractCIADetailPageDisplayItemAssembler
{

public void filterYearsByStocking( CIAGroupingItemDisplayData displayGroupingItem, List yearDetailsWithRecs )
{
	displayGroupingItem.setUnderstockedYears( determineUnderstockedYears( yearDetailsWithRecs ) );
}

public boolean determineIncludeItem( CIAGroupingItemDisplayData displayGroupingItem )
{
	return true;
}

}
