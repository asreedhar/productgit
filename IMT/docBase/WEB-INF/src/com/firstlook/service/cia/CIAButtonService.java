package com.firstlook.service.cia;

import java.util.ArrayList;
import java.util.List;

import biz.firstlook.cia.service.CIABodyTypeDetailService;

public class CIAButtonService
{

private CIABodyTypeDetailService ciaBodyTypeDetailService;
	
public CIAButtonService()
{
}

public List retrieveBodyTypeDetailButtons( int dealerId, int ciaSummaryId )
{
	List ciaBodyTypeDetailButtons = new ArrayList();
	ciaBodyTypeDetailButtons = ciaBodyTypeDetailService.retrieveCIABodyTypeDetailOrderedByContribution( dealerId, ciaSummaryId );
	return ciaBodyTypeDetailButtons;
}

public void setCiaBodyTypeDetailService(
		CIABodyTypeDetailService ciaBodyTypeDetailService) {
	this.ciaBodyTypeDetailService = ciaBodyTypeDetailService;
}

}
