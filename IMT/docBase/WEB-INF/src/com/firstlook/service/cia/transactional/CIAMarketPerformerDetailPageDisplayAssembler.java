package com.firstlook.service.cia.transactional;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import biz.firstlook.cia.model.CIAGroupingItem;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.GroupingDescription;
import biz.firstlook.transact.persist.model.GroupingDescriptionLight;
import biz.firstlook.transact.persist.model.Inventory;

import com.firstlook.display.cia.CIAGroupingItemDisplayData;
import com.firstlook.service.risklevel.GroupingDescriptionLightService;

public class CIAMarketPerformerDetailPageDisplayAssembler extends AbstractCIADetailPageDisplayItemAssembler
{

private GroupingDescriptionLightService groupingDescriptionLightService;

public List createCIADetailPageGroupingItemDisplayData( int businessUnitId, List ciaGroupingItems )
{
	int unitsInStock = 0;
	Iterator ciaGroupingIter = ciaGroupingItems.iterator();
	CIAGroupingItemDisplayData displayGroupingItem = null;
	GroupingDescriptionLight light;
	List displayGroupingItems = new ArrayList();
	while ( ciaGroupingIter.hasNext() )
	{
		CIAGroupingItem ciaGroupingItem = (CIAGroupingItem)ciaGroupingIter.next();
		displayGroupingItem = new CIAGroupingItemDisplayData(	ciaGroupingItem.getCiaGroupingItemId(),
																ciaGroupingItem.getGroupingDescription().getGroupingDescriptionId(),
																ciaGroupingItem.getNotes(),
																ciaGroupingItem.getMarketPenetration(),
																ciaGroupingItem.getCiaCategory(),
																ciaGroupingItem.getMarketShare() );

		light = getGroupingDescriptionLightService().retrieveGroupingDescriptionLight(
																						businessUnitId,
																						ciaGroupingItem.getGroupingDescription().getGroupingDescriptionId().intValue() );
		displayGroupingItem.setLight( light );

		List yearsList = getValidYearsCarWasSold( ciaGroupingItem.getGroupingDescription().getGroupingDescriptionId().intValue(), 10 );
		displayGroupingItem.setYearsStringsList( yearsList );

		// Get any existing buying plan based on years
		// TODO actuall get an existing buying plan.
		Map buyingPlanMap = this.getBuyingPlanMap( ciaGroupingItem.getCiaGroupingItemDetails(), yearsList );
		displayGroupingItem.setBuyingPlanMap( buyingPlanMap );

		DealerPreference dealerPreference = getDealerPrefDAO().findByBusinessUnitId( new Integer( businessUnitId ) );

		//Ignore Lower Unit cost threshold for diplaying current Inventory units in stock
		unitsInStock = inventoryDAO.findCountOfActiveByGroupingDescription( new Integer( businessUnitId ),
																			displayGroupingItem.getGroupingDescriptionId(), Inventory.USED_CAR,
																			Integer.MIN_VALUE,
																			dealerPreference.getUnitCostThresholdUpper() );
		displayGroupingItem.setUnitsInStock( unitsInStock );

		// manage bits and pieces
		GroupingDescription groupingDescription = getGroupingDescriptionDAO().findByIDWithSpring( displayGroupingItem.getGroupingDescriptionId() );
		displayGroupingItem.setGroupingDescriptionString( groupingDescription.getGroupingDescription() );
		displayGroupingItems.add( displayGroupingItem );
	}
	return displayGroupingItems;
}

public boolean determineIncludeItem( CIAGroupingItemDisplayData displayGroupingItem )
{
	return false;
}

public void filterYearsByStocking( CIAGroupingItemDisplayData displayGroupingItem, List yearDetailsWithRecs )
{

}

public GroupingDescriptionLightService getGroupingDescriptionLightService()
{
	return groupingDescriptionLightService;
}

public void setGroupingDescriptionLightService( GroupingDescriptionLightService groupingDescriptionLightService )
{
	this.groupingDescriptionLightService = groupingDescriptionLightService;
}

}
