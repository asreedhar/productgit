package com.firstlook.service.cia.report;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import biz.firstlook.cia.model.CIAGroupingItemDetailLevel;
import biz.firstlook.cia.model.CIAGroupingItemDetailType;
import biz.firstlook.cia.model.CIASummary;
import biz.firstlook.cia.model.Status;

import com.firstlook.service.cia.ICIAService;

public class CIASummaryReportDAO extends HibernateDaoSupport
{
private ICIAService ciaService;

public CIASummaryReportDAO()
{
	super();
}

/**
 * This method retrieves data for the CIASummary Page.
 * 
 * @param dealerId
 *            the dealer's primary key identifier in the database
 * @param segmentId
 *            the primary key from dbo.Segment
 * @param ciaCategoryId
 *            the primary key from dbo.CIACategories. Values are 1 to 5,
 *            Powerzone, Winners, Good Bets, Market Performer, and Manager's
 *            Choice respectively.
 * @return a list Object[] with the fields populated according the the HQL
 *         query.
 */
public List retrieveCIASummaryDisplayData( Integer dealerId, Integer segmentId, Integer ciaCategoryId )
{
	StringBuffer sqlStatement = new StringBuffer();
	sqlStatement.append( " select ciaCategory.ciaCategoryDescription, groupingDescription.groupingDescriptionId " );
	sqlStatement.append( " , groupingDescription.groupingDescription, ciaGroupingItemDetail.detailLevelValue " );
	sqlStatement.append( " , ciaGroupingItemDetailType, ciaGroupingItemDetail.units " );
	sqlStatement.append( " , ciaGroupingItem.marketShare, segment.segment " );
	sqlStatement.append( " , ciaGroupingItem.valuation, ciaGroupingItemDetail.ciaGroupingItemDetailLevel.ciaGroupingItemDetailLevelId " );

	sqlStatement.append( " from biz.firstlook.cia.model.CIASummary ciaSummary " );
	sqlStatement.append( " inner join ciaSummary.ciaBodyTypeDetails ciaBodyTypeDetail " );
	sqlStatement.append( " left join ciaBodyTypeDetail.segment segment " );
	sqlStatement.append( " inner join ciaBodyTypeDetail.ciaGroupingItems ciaGroupingItem " );
	sqlStatement.append( " inner join ciaGroupingItem.groupingDescription groupingDescription " );
	sqlStatement.append( " inner join ciaGroupingItem.ciaCategory ciaCategory " );
	sqlStatement.append( " left join ciaGroupingItem.ciaGroupingItemDetails ciaGroupingItemDetail " );
	sqlStatement.append( " left join ciaGroupingItemDetail.ciaGroupingItemDetailType ciaGroupingItemDetailType " );

	sqlStatement.append( " where ciaSummary.businessUnitId = ? " );
	sqlStatement.append( " and ciaSummary.status.statusId = ? " );
	sqlStatement.append( " and ciaBodyTypeDetail.segment.segmentId = ? " );
	sqlStatement.append( " and ciaGroupingItem.ciaCategory.ciaCategoryId = ? " );
	sqlStatement.append( " and ( ciaGroupingItemDetail.ciaGroupingItemDetailLevel.ciaGroupingItemDetailLevelId = " );
	sqlStatement.append( CIAGroupingItemDetailLevel.GROUPING.getCiaGroupingItemDetailLevelId() );
	sqlStatement.append( " or ciaGroupingItemDetail.ciaGroupingItemDetailLevel.ciaGroupingItemDetailLevelId = " );
	sqlStatement.append( CIAGroupingItemDetailLevel.YEAR.getCiaGroupingItemDetailLevelId() + " )");
	sqlStatement.append( " and ( ciaGroupingItemDetailType.ciaGroupingItemDetailTypeId = " );
	sqlStatement.append( CIAGroupingItemDetailType.OPTIMAL_STOCKING_LEVEL.getCiaGroupingItemDetailTypeId() );
	sqlStatement.append( " or ciaGroupingItemDetailType.ciaGroupingItemDetailTypeId = " );
	sqlStatement.append( CIAGroupingItemDetailType.BUY.getCiaGroupingItemDetailTypeId() ).append( " ) " );

	Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();

	Query hibernateQuery = session.createQuery( sqlStatement.toString() );
	hibernateQuery.setInteger( 0, dealerId.intValue() );
	hibernateQuery.setInteger( 1, Status.CURRENT.getStatusId().intValue() );
	hibernateQuery.setInteger( 2, segmentId.intValue() );
	hibernateQuery.setInteger( 3, ciaCategoryId.intValue() );

	List ciaSummaryDisplayData = null;
	try
	{
		ciaSummaryDisplayData = hibernateQuery.list();
	}
	catch ( HibernateException e )
	{
		ciaSummaryDisplayData = new ArrayList();
	}
	return ciaSummaryDisplayData;
}

public CIASummary retrieveCIASummary( Integer businessUnitId, Integer ciaSummaryStatusId )
{
	Collection collection = getHibernateTemplate().find(
															"select ciaSummary from biz.firstlook.cia.model.CIASummary ciaSummary where ciaSummary.businessUnitId = ?"
																	+ " and ciaSummary.status.statusId = ?",
															new Object[] { businessUnitId, ciaSummaryStatusId } );
	if ( collection != null && !collection.isEmpty() )
	{
		return (CIASummary)collection.toArray()[0];
	}
	return null;
}

public ICIAService getCiaService()
{
	return ciaService;
}

public void setCiaService( ICIAService ciaService )
{
	this.ciaService = ciaService;
}

}
