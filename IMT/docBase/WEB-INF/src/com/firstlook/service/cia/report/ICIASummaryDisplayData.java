package com.firstlook.service.cia.report;

public interface ICIASummaryDisplayData
{

public String getCategoryString();

public void setCategoryString( String categoryString );

public Integer getSegmentId();

public void setSegmentId( Integer segmentId );

public Integer getGroupingDescriptionId();

public void setGroupingDescriptionId( Integer groupingDescriptionId );

public String getGroupingDescriptionString();

public void setGroupingDescriptionString( String groupingDescriptionString );

public Integer getPlannedToBuyTotal();

public void setPlannedToBuyTotal( Integer plannedToBuyTotal );

public String getSegmentString();

public void setSegmentString( String segmentString );

}