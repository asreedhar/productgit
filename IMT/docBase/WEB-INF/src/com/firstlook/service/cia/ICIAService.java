package com.firstlook.service.cia;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import biz.firstlook.cia.model.CIACategory;
import biz.firstlook.cia.model.CIAGroupingItem;
import biz.firstlook.transact.persist.model.GroupingDescription;


public interface ICIAService
{

public List getCIASummaryDisplayData( Integer dealerId, Integer segmentId, CIACategory ciaCategory, List actualInventoryList );

public List getCIAStockingData(Integer businessUnitId, int stockingType);

public Collection getCIABuyingPlanRawData( Integer dealerId, Integer ciaSummaryStatusId );

public List<CIAGroupingItem> getCIAGroupingItems(int businessUnitId, int segmentId, int ciaCategoryId );

public List getCIAPowerzoneDetailPageDisplayItems(int businessUnitId, List ciaGroupingItems );

public List getCIANonPowerzoneDetailPageDisplayItems(int businessUnitId, List ciaGroupingItems );

public Collection getUnderstockedCIARecommendationsByCategoryType( Integer dealerId, Integer ciaSummaryStatusId, Integer ciaCategoryId );

public CIAGroupingItem getCIAGroupingItemByGroupingDescriptionId( Integer segmentId, CIACategory ciaCategory,
																	GroupingDescription groupingDescription, Integer dealerId );

public List getManagersChoiceItemsBySegmentId( Integer segmentId );

public CIAGroupingItem constructCIAGroupingItemFromBuyAmountAndPreferences(
                                                                           	Integer ciaGroupingItemId,
                                                                            Integer groupingDescriptionId,
                                                                            Map parameterMap,
                                                                            CIACategory ciaCategory,
                                                                            int buyAmount,
                                                                            Integer ciaBodyTypeDetailId
                                                                            );
public void constructCIAGroupingItemFromBuyingPreferences(
                                                                  List groupingItems,
                                                                  Map parameterMap,
                                                                  CIACategory ciaCategory,
                                                                  Integer ciaBodyTypeDetailId
                                                                 );

public void saveCIAGroupingItem( CIAGroupingItem groupingItem );

}
