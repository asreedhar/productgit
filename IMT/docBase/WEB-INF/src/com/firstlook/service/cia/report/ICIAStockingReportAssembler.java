package com.firstlook.service.cia.report;

import java.util.List;

public interface ICIAStockingReportAssembler
{

public List retrieveCIAStockingData( Integer dealerId, int stockingType );

}
