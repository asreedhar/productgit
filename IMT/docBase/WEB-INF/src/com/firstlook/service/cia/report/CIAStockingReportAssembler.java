package com.firstlook.service.cia.report;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.MultiHashMap;

import biz.firstlook.cia.calculator.CIAGroupingItemDetailCalculator;
import biz.firstlook.cia.model.CIAGroupingItem;
import biz.firstlook.cia.model.CIAGroupingItemDetail;
import biz.firstlook.cia.model.CIAGroupingItemDetailLevel;
import biz.firstlook.cia.model.CIAGroupingItemDetailType;
import biz.firstlook.cia.model.CIASummary;
import biz.firstlook.cia.model.Status;
import biz.firstlook.cia.persistence.ICIASummaryDAO;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.Segment;
import biz.firstlook.transact.persist.persistence.IDealerPreferenceDAO;

import com.firstlook.entity.InventoryEntity;
import com.firstlook.persistence.cia.CIASegmentByContributionRetriever;
import com.firstlook.persistence.cia.ICIAStockingReportDAO;
import com.firstlook.persistence.inventory.IInventoryDAO;

public class CIAStockingReportAssembler implements ICIAStockingReportAssembler
{

private ICIAStockingReportDAO ciaStockingReportDAO;
private ICIASummaryDAO ciaSummaryDAO;
private IDealerPreferenceDAO dealerPrefDAO;
private IInventoryDAO inventoryPersistence;
private CIASegmentByContributionRetriever ciaByContributionRetriever;

public static final int UNDER = 1;
public static final int OVER = 2;
public static final int OPTIMAL = 4;
public static final int OVER_UNDER = 3;

public CIAStockingReportAssembler()
{
	super();

}

public List retrieveCIAStockingData( Integer businessUnitId, int stockingType )
{
	return generateReport( businessUnitId, stockingType );
}

public ICIAStockingReportDAO getCiaStockingReportDAO()
{
	return ciaStockingReportDAO;
}

public void setCiaStockingReportDAO( ICIAStockingReportDAO ciaOverUnderStockingReportDelegate )
{
	this.ciaStockingReportDAO = ciaOverUnderStockingReportDelegate;
}

private List generateReport( Integer businessUnitId, int requestedReportType )
{
	List ciaReportDataItemsList = new ArrayList();

	List ciaGroupingItemsOptimal = getCiaStockingReportDAO().retrieveCIAGroupingItemsWithOptimalStockingLevel( businessUnitId );
	DealerPreference dealerPref = getDealerPrefDAO().findByBusinessUnitId( businessUnitId );
	if ( ciaGroupingItemsOptimal == null )
	{
		return new ArrayList();
	}

	//find units in stock here.  this method really sucks.  boot to FLDW eventually.
	List inventoryItems = (List)inventoryPersistence.findByDealerIdInventoryTypeAndUpperAndLowerUnitCostThreshold(
	                                                                                                         businessUnitId.intValue(),
	                                                                                                         InventoryEntity.USED_CAR,
	                                                                                                         dealerPref.getUnitCostThresholdLower(),
	                                                                                                         dealerPref.getUnitCostThresholdUpper() );
	MultiHashMap actualInventoryMapByModel = CIAReportingUtility.mapInventoryByGroupingDescription( inventoryItems );

	if ( requestedReportType == OVER || requestedReportType == UNDER || requestedReportType == OVER_UNDER )
	{
		MultiHashMap actualInventoryMapByYear = CIAReportingUtility.mapInventoryByYear( inventoryItems );
		ciaReportDataItemsList = iterateOptimalAndActualResultSet( ciaGroupingItemsOptimal, actualInventoryMapByYear,
																	actualInventoryMapByModel, requestedReportType, businessUnitId );

	}
	else
	{
		ciaReportDataItemsList = iterateOptimalResultSet( ciaGroupingItemsOptimal, businessUnitId, actualInventoryMapByModel );
	}

	return ciaReportDataItemsList;
}

private List iterateOptimalResultSet( List ciaGroupingItemsOptimal, Integer businessUnitId, MultiHashMap ciaGroupingItemsActualByModel )
{
	List<CIAStockingReportBodyTypeDisplayData> toBeReturned = new ArrayList<CIAStockingReportBodyTypeDisplayData>();
	List<CIAStockingDetailLevelDisplayData> ciaDetailLevelDisplayDataItems;
	List optimalGroupingItemDetails;
	List optimalGroupingItemDetailsByModel;
	CIAStockingDetailLevelDisplayData detailLevelDisplayData;
	MultiHashMap ciaGroupingDisplayItems = new MultiHashMap();
	Iterator ciaGroupingItemsOptimalIter = ciaGroupingItemsOptimal.iterator();

	CIAStockingReportGroupingDisplayData groupingDisplayData;

	// once for each grouping item
	while ( ciaGroupingItemsOptimalIter.hasNext() )
	{
		groupingDisplayData = new CIAStockingReportGroupingDisplayData();

		ciaDetailLevelDisplayDataItems = new ArrayList<CIAStockingDetailLevelDisplayData>();
		Object[] groupingItemOptimalReturn = (Object[])ciaGroupingItemsOptimalIter.next();
		CIAGroupingItem groupingItem = (CIAGroupingItem)groupingItemOptimalReturn[0];
		Integer groupingItemsSegment = (Integer)groupingItemOptimalReturn[1];

		optimalGroupingItemDetails = CIAGroupingItemDetailCalculator.filterDetailsByTypeAndLevel(
																									CIAGroupingItemDetailType.OPTIMAL_STOCKING_LEVEL_TYPE,
																									CIAGroupingItemDetailLevel.YEAR_DETAIL_LEVEL,
																									groupingItem.getCiaGroupingItemDetails() );

		Iterator groupingItemIter = optimalGroupingItemDetails.iterator();

		// once for each year with stocking
		while ( groupingItemIter.hasNext() )
		{
			detailLevelDisplayData = new CIAStockingDetailLevelDisplayData();
			CIAGroupingItemDetail groupingItemDetail = (CIAGroupingItemDetail)groupingItemIter.next();

			detailLevelDisplayData.setTargetInventory( groupingItemDetail.getUnits().intValue() );
			detailLevelDisplayData.setDetailLevelValue( groupingItemDetail.getDetailLevelValue() );
			if ( detailLevelDisplayData.getTargetInventory() != 0 )
			{
				ciaDetailLevelDisplayDataItems.add( detailLevelDisplayData );
			}
		}
		
		optimalGroupingItemDetailsByModel = CIAGroupingItemDetailCalculator.filterDetailsByTypeAndLevel( CIAGroupingItemDetailType.OPTIMAL_STOCKING_LEVEL_TYPE,
																										 CIAGroupingItemDetailLevel.GROUPING_DETAIL_LEVEL,
																										 groupingItem.getCiaGroupingItemDetails() );

		Iterator itemDetailsByModelIter = optimalGroupingItemDetailsByModel.iterator();
	
		// once for each year with stocking
		while ( itemDetailsByModelIter.hasNext() )
		{
			detailLevelDisplayData = new CIAStockingDetailLevelDisplayData();
			CIAGroupingItemDetail groupingItemDetail = (CIAGroupingItemDetail)itemDetailsByModelIter.next();
	
			detailLevelDisplayData.setTargetInventory( groupingItemDetail.getUnits().intValue() );
			detailLevelDisplayData.setUnitsInStock( CIAReportingUtility.retrieveUnitsInStockForGroupDescription(	groupingItem.getGroupingDescription().getGroupingDescriptionId().toString(),
																													ciaGroupingItemsActualByModel ) );
			detailLevelDisplayData.setDetailLevelValue( "Model" );
			detailLevelDisplayData.setOverUnderStock();
			ciaDetailLevelDisplayDataItems.add( detailLevelDisplayData );
		}


		groupingDisplayData.setGroupingDescription( groupingItem.getGroupingDescription().getGroupingDescription() );
		Collections.sort( ciaDetailLevelDisplayDataItems );
		groupingDisplayData.setDetailLevelDisplayDataItems( ciaDetailLevelDisplayDataItems );
		if ( groupingDisplayData.getDetailLevelDisplayDataItems().size() > 0 )
		{
			ciaGroupingDisplayItems.put( groupingItemsSegment, groupingDisplayData );
		}
	}

	// get bodyTypeId's and descriptions
	Segment bodyTypeDetail;


	CIASummary summaryID = getCiaSummaryDAO().retrieveCIASummary( businessUnitId, Status.CURRENT );
	List ciaBodyTypes = ciaByContributionRetriever.retrieveCIABodyTypeDetails( businessUnitId.intValue(), true, new Timestamp( new Date().getTime() ),
																		summaryID.getCiaSummaryId().intValue() );

	Iterator ciaBodyTypesIter = ciaBodyTypes.iterator();
	CIAStockingReportBodyTypeDisplayData bodyTypeDisplayData;

	while ( ciaBodyTypesIter.hasNext() )
	{
		bodyTypeDetail = (Segment)ciaBodyTypesIter.next();
		bodyTypeDisplayData = new CIAStockingReportBodyTypeDisplayData();
		bodyTypeDisplayData.setSegment( bodyTypeDetail.getSegment() );
		bodyTypeDisplayData.setCiaReportDisplayDatas( (List)ciaGroupingDisplayItems.get( bodyTypeDetail.getSegmentId() ) );
		toBeReturned.add( bodyTypeDisplayData );
	}

	return toBeReturned;

}

// TODO: check old display objects to see if we can delete them
private List iterateOptimalAndActualResultSet( List ciaGroupingItemsOptimal, MultiHashMap ciaGroupingItemsActualByYear,
												MultiHashMap ciaGroupingItemsActualByModel, int requestedReportType, Integer businessUnitId )
{
	List<CIAStockingReportBodyTypeDisplayData> toBeReturned = new ArrayList<CIAStockingReportBodyTypeDisplayData>();
	MultiHashMap ciaGroupingDisplayItems = new MultiHashMap();
	List<CIAStockingDetailLevelDisplayData> ciaDetailLevelDisplayDataItems;
	CIAStockingDetailLevelDisplayData detailLevelDisplayData;

	Iterator ciaGroupingItemsOptimalIter = ciaGroupingItemsOptimal.iterator();

	CIAStockingReportGroupingDisplayData groupingDisplayData;
	List optimalGroupingItemDetailsByYear;
	List optimalGroupingItemDetailsByModel;
	CIAGroupingItem groupingItem;

	// once for each grouping item
	while ( ciaGroupingItemsOptimalIter.hasNext() )
	{
		groupingDisplayData = new CIAStockingReportGroupingDisplayData();
		ciaDetailLevelDisplayDataItems = new ArrayList<CIAStockingDetailLevelDisplayData>();

		Object[] groupingItemOptimalReturn = (Object[])ciaGroupingItemsOptimalIter.next();
		groupingItem = (CIAGroupingItem)groupingItemOptimalReturn[0];
		Integer groupingItemsSegment = (Integer)groupingItemOptimalReturn[1];

		optimalGroupingItemDetailsByYear = CIAGroupingItemDetailCalculator.filterDetailsByTypeAndLevel(
																										CIAGroupingItemDetailType.OPTIMAL_STOCKING_LEVEL_TYPE,
																										CIAGroupingItemDetailLevel.YEAR_DETAIL_LEVEL,
																										groupingItem.getCiaGroupingItemDetails() );

		Iterator itemDetailsByYearIter = optimalGroupingItemDetailsByYear.iterator();

		// once for each year with stocking
		while ( itemDetailsByYearIter.hasNext() )
		{
			detailLevelDisplayData = new CIAStockingDetailLevelDisplayData();
			CIAGroupingItemDetail groupingItemDetail = (CIAGroupingItemDetail)itemDetailsByYearIter.next();

			detailLevelDisplayData.setTargetInventory( groupingItemDetail.getUnits().intValue() );
			detailLevelDisplayData.setUnitsInStock( CIAReportingUtility.retrieveUnitsInStockForGroupDescriptionAndYear( groupingItem,
																														groupingItemDetail,
																														ciaGroupingItemsActualByYear ) );
			detailLevelDisplayData.setDetailLevelValue( groupingItemDetail.getDetailLevelValue() );
			detailLevelDisplayData.setOverUnderStock();
			addDetailLevelDisplayDataToList( detailLevelDisplayData, ciaDetailLevelDisplayDataItems, requestedReportType );
		}

		optimalGroupingItemDetailsByModel = CIAGroupingItemDetailCalculator.filterDetailsByTypeAndLevel(
																											CIAGroupingItemDetailType.OPTIMAL_STOCKING_LEVEL_TYPE,
																											CIAGroupingItemDetailLevel.GROUPING_DETAIL_LEVEL,
																											groupingItem.getCiaGroupingItemDetails() );

		Iterator itemDetailsByModelIter = optimalGroupingItemDetailsByModel.iterator();

		// once for each year with stocking
		while ( itemDetailsByModelIter.hasNext() )
		{
			detailLevelDisplayData = new CIAStockingDetailLevelDisplayData();
			CIAGroupingItemDetail groupingItemDetail = (CIAGroupingItemDetail)itemDetailsByModelIter.next();

			detailLevelDisplayData.setTargetInventory( groupingItemDetail.getUnits().intValue() );
			detailLevelDisplayData.setUnitsInStock( CIAReportingUtility.retrieveUnitsInStockForGroupDescription(
																													groupingItem.getGroupingDescription().getGroupingDescriptionId().toString(),
																													ciaGroupingItemsActualByModel ) );
			detailLevelDisplayData.setDetailLevelValue( "Model" );
			detailLevelDisplayData.setOverUnderStock();
			addDetailLevelDisplayDataToList( detailLevelDisplayData, ciaDetailLevelDisplayDataItems, requestedReportType );
		}

		groupingDisplayData.setGroupingDescription( groupingItem.getGroupingDescription().getGroupingDescription() );
		Collections.sort( ciaDetailLevelDisplayDataItems );
		groupingDisplayData.setDetailLevelDisplayDataItems( ciaDetailLevelDisplayDataItems );
		if ( groupingDisplayData.getDetailLevelDisplayDataItems().size() > 0 )
		{
			ciaGroupingDisplayItems.put( groupingItemsSegment, groupingDisplayData );
		}
	}

	// get bodyTypeId's and descriptions
	Segment bodyTypeDetail;


	CIASummary summaryID = getCiaSummaryDAO().retrieveCIASummary( businessUnitId, Status.CURRENT );
	List ciaBodyTypes = ciaByContributionRetriever.retrieveCIABodyTypeDetails( businessUnitId.intValue(), true, new Timestamp( new Date().getTime() ),
																		summaryID.getCiaSummaryId().intValue() );
	Iterator ciaBodyTypesIter = ciaBodyTypes.iterator();
	CIAStockingReportBodyTypeDisplayData bodyTypeDisplayData;

	while ( ciaBodyTypesIter.hasNext() )
	{
		bodyTypeDetail = (Segment)ciaBodyTypesIter.next();
		bodyTypeDisplayData = new CIAStockingReportBodyTypeDisplayData();
		bodyTypeDisplayData.setSegment( bodyTypeDetail.getSegment());
		bodyTypeDisplayData.setCiaReportDisplayDatas( (List)ciaGroupingDisplayItems.get( bodyTypeDetail.getSegmentId()) );
		toBeReturned.add( bodyTypeDisplayData );
	}

	return toBeReturned;
}

private void addDetailLevelDisplayDataToList( CIAStockingDetailLevelDisplayData yearDisplayData, List<CIAStockingDetailLevelDisplayData> ciaYearDisplayDataItems,
												int requestedReportType )
{
	if ( yearDisplayData.getStockingDifference() != 0 )
	{
		switch ( requestedReportType )
		{
			case OVER:
				if ( yearDisplayData.getIsOverStocked() )
				{
					ciaYearDisplayDataItems.add( yearDisplayData );
				}
				break;
			case UNDER:
				if ( !yearDisplayData.getIsOverStocked() && yearDisplayData.getStockingDifference() != 0 )
				{
					ciaYearDisplayDataItems.add( yearDisplayData );
				}
				break;
			case OVER_UNDER:
				ciaYearDisplayDataItems.add( yearDisplayData );
				break;
		}
	}
}

public ICIASummaryDAO getCiaSummaryDAO()
{
	return ciaSummaryDAO;
}

public void setCiaSummaryDAO( ICIASummaryDAO ciaSummaryDAO )
{
	this.ciaSummaryDAO = ciaSummaryDAO;
}

public IDealerPreferenceDAO getDealerPrefDAO()
{
	return dealerPrefDAO;
}

public void setDealerPrefDAO( IDealerPreferenceDAO dealerPrefDAO )
{
	this.dealerPrefDAO = dealerPrefDAO;
}

public IInventoryDAO getInventoryPersistence() {
	return inventoryPersistence;
}

public void setInventoryPersistence(IInventoryDAO inventoryPersistence) {
	this.inventoryPersistence = inventoryPersistence;
}

public void setCiaByContributionRetriever(
		CIASegmentByContributionRetriever ciaByContributionRetriever) {
	this.ciaByContributionRetriever = ciaByContributionRetriever;
}

}
