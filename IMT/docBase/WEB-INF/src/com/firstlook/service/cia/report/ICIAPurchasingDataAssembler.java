package com.firstlook.service.cia.report;

import java.util.Collection;


public interface ICIAPurchasingDataAssembler
{

public Collection retrieveCIABuyingPlanData( Integer dealerId, Integer ciaSummaryStatusId );

public Collection retrieveUnderstockedCIARecommendationsByCategoryType( Integer businessUnitId, Integer ciaSummaryStatusId, Integer ciaCategoryId );

}