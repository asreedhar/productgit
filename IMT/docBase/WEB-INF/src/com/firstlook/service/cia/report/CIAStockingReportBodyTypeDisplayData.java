package com.firstlook.service.cia.report;

import java.util.List;

public class CIAStockingReportBodyTypeDisplayData
{

private String segment;
private List ciaReportDisplayDatas;

public CIAStockingReportBodyTypeDisplayData()
{
	super();
}

public List getCiaReportDisplayDatas()
{
	return ciaReportDisplayDatas;
}

public void setCiaReportDisplayDatas( List ciaReportDisplayDatas )
{
	this.ciaReportDisplayDatas = ciaReportDisplayDatas;
}

public String getSegment()
{
	return segment;
}

public void setSegment( String segment )
{
	this.segment = segment;
}

}
