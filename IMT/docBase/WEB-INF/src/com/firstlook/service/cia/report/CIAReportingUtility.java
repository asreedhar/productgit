package com.firstlook.service.cia.report;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MultiHashMap;
import org.apache.log4j.Logger;

import biz.firstlook.cia.model.CIAGroupingItem;
import biz.firstlook.cia.model.CIAGroupingItemDetail;
import biz.firstlook.cia.model.CIAGroupingItemDetailLevel;
import biz.firstlook.cia.model.CIAGroupingItemDetailType;

import com.firstlook.action.dealer.cia.ParamFilterPredicate;
import com.firstlook.entity.InventoryEntity;

/**
 * This class contains static utility methods for constructing CIA data
 */
public class CIAReportingUtility
{

private static Logger logger = Logger.getLogger( CIAReportingUtility.class );
	
public static int retrieveUnitsInStockForGroupDescriptionAndYear( CIAGroupingItem groupingItem, CIAGroupingItemDetail groupingItemDetail,
																	MultiHashMap ciaGroupingItemsActual )
{
	String keyString = groupingItem.getGroupingDescription().getGroupingDescriptionId().toString()
			+ "_" + groupingItemDetail.getDetailLevelValue();
	int toBeReturned;
	if ( ciaGroupingItemsActual.get( keyString ) == null )
	{
		toBeReturned = 0;
	}
	else
	{
		toBeReturned = ( (List)( ciaGroupingItemsActual.get( keyString ) ) ).size();
	}
	return toBeReturned;
}

public static int retrieveUnitsInStockForGroupDescriptionAndYear( String detailLevelValue, String groupingDescriptionId,
																	MultiHashMap ciaGroupingItemsActual )
{
	String keyString = groupingDescriptionId + "_" + detailLevelValue;
	int toBeReturned;
	if ( ciaGroupingItemsActual.get( keyString ) == null )
	{
		toBeReturned = 0;
	}
	else
	{
		toBeReturned = ( (List)( ciaGroupingItemsActual.get( keyString ) ) ).size();
	}
	return toBeReturned;
}

public static int retrieveUnitsInStockForGroupDescription( String groupingDescriptionId,
																	MultiHashMap ciaGroupingItemsActual )
{
	String keyString = groupingDescriptionId;
	int toBeReturned;
	if ( ciaGroupingItemsActual.get( keyString ) == null )
	{
		toBeReturned = 0;
	}
	else
	{
		toBeReturned = ( (List)( ciaGroupingItemsActual.get( keyString ) ) ).size();
	}
	return toBeReturned;
}


public static MultiHashMap mapInventoryByYear( List<InventoryEntity> inventoryItemList )
{
	Iterator<InventoryEntity> it = inventoryItemList.iterator();
	MultiHashMap map = new MultiHashMap();

	while ( it.hasNext() )
	{
		// TODO: Check this type
		InventoryEntity inventoryItem = it.next();

		// TODO: check getVehicleYear method call for accuracy
		String keyString = inventoryItem.getVehicle().getMakeModelGrouping().getGroupingDescription().getGroupingDescriptionId().intValue()
				+ "_" + inventoryItem.getVehicle().getVehicleYear();

		map.put( keyString, inventoryItem );
	}
	return map;
}

public static MultiHashMap mapInventoryByGroupingDescription( List inventoryItemList )
{
	Iterator it = inventoryItemList.iterator();
	MultiHashMap map = new MultiHashMap();

	while ( it.hasNext() )
	{
		// TODO: Check this type
		InventoryEntity inventoryItem = (InventoryEntity)it.next();

		// TODO: check getVehicleYear method call for accuracy
		String keyString = inventoryItem.getVehicle().getMakeModelGrouping().getGroupingDescription().getGroupingDescriptionId().toString();

		map.put( keyString, inventoryItem );
	}
	return map;
}

/**
 * Parses the list of groupingItems and populates a map of Composite Key Strings
 * e.g.: ciaPref_Color_Silver_Prefer_.... used for display.
 * 
 * public scoped because it is used in the Action.
 */
public static Map createPreferencesMapForCIA( List<CIAGroupingItem> groupingItems )
{
	Map<String, Comparable> resultMap = new HashMap<String, Comparable>();
	Iterator<CIAGroupingItem> groupingItemsIter = groupingItems.iterator();
	Iterator<CIAGroupingItemDetail> detailsIter = null;
	Set<CIAGroupingItemDetail> groupingItemDetails = null;
	CIAGroupingItem groupingItem = null;
	CIAGroupingItemDetail detail = null;
	Integer ciaGroupingItemId = null;
	MapTokens mapTokens = null;

	while ( groupingItemsIter.hasNext() )
	{
		groupingItem = groupingItemsIter.next();
		ciaGroupingItemId = groupingItem.getCiaGroupingItemId();
		groupingItemDetails = groupingItem.getCiaGroupingItemDetails();
		detailsIter = groupingItemDetails.iterator();
		while ( detailsIter.hasNext() )
		{
			detail = detailsIter.next();
			detail.setDetailLevelValue( TrimFormattingUtility.escapeJS( detail.getDetailLevelValue() ) );

			mapTokens = new MapTokens();
			MapTokens tokens = mapTokens.create( detail, ciaGroupingItemId );
			if ( detail.getCiaGroupingItemDetailType().getCiaGroupingItemDetailTypeId().intValue() == CIAGroupingItemDetailType.BUY_TYPE )
			{
				resultMap.put( tokens.constructCompositeKey(), detail.getUnits() );
			}
			if ( detail.getCiaGroupingItemDetailType().getCiaGroupingItemDetailTypeId().intValue() == CIAGroupingItemDetailType.PREFER_TYPE
					|| detail.getCiaGroupingItemDetailType().getCiaGroupingItemDetailTypeId().intValue() == CIAGroupingItemDetailType.AVOID_TYPE )
			{
				String compKey = tokens.constructCompositeKey();
				resultMap.put( compKey, new Boolean( true ) );
			}
		}

	}

	return resultMap;
}

public static String updateDetailItemsBasedOnSelectedPreferences( Map parameterMap, List<CIAGroupingItem> ciaGroupingItems ) throws IllegalAccessException,
		InvocationTargetException, NoSuchMethodException
{
	HashMap<Integer, CIAGroupingItem> ciaGroupingItemsMap = new HashMap<Integer, CIAGroupingItem>();
	Set paramNameSet = parameterMap.keySet();
	Collection<String> ciaGroupingItemKeys = CollectionUtils.select( paramNameSet, new ParamFilterPredicate( "ciaPref" ) );
	Collection<String> unitCostPointRangeKeys = CollectionUtils.select( paramNameSet, new ParamFilterPredicate( "ucp_") );

	CIAGroupingItem item = null;
	Iterator<CIAGroupingItem> itemsIter = ciaGroupingItems.iterator();
	while ( itemsIter.hasNext() )
	{
		item = (CIAGroupingItem)itemsIter.next();
		item.setCiaGroupingItemDetails( cleanCIAGroupingItemDetailPreferences( item.getCiaGroupingItemDetails() ) );
		constructUnitCostItemDetails( unitCostPointRangeKeys, parameterMap, item );

		ciaGroupingItemsMap.put( item.getCiaGroupingItemId(), item );
	}

	String errorMessage = constructCIAPreferenceDetails( parameterMap, ciaGroupingItemsMap, ciaGroupingItemKeys );
	return errorMessage;
}

private static String constructCIAPreferenceDetails( Map<String, String[]> parameterMap, HashMap<Integer, CIAGroupingItem> ciaGroupingItemsMap, Collection<String> ciaGroupingItemKeys )
{
	String errorMessage = null;
	CIAGroupingItem groupingItem = null;
	Iterator<String> keyIter = ciaGroupingItemKeys.iterator();
	while ( keyIter.hasNext() )
	{
		String key = (String)keyIter.next();
		String[] keyTokens = key.split( "_" );
		String[] values = parameterMap.get( key );

		/* expect to be string representations of true or a buy amount */
		String propertyValue = values[0];

		Integer groupingItemId = new Integer( keyTokens[1] ); /*
																 * Zeroth
																 * element is
																 * "ciapref"
																 */

		// this was to determine int representation of color, unitcost etc.
		// "level"
		CIAGroupingItemDetailLevel detailLevel = CIAGroupingItemDetailLevel.constructLevelFromDescription( keyTokens[2] );

		String value = TrimFormattingUtility.unEscapeJS( keyTokens[3] );
		// value associated with level silver, 2005, 328si etc.

		CIAGroupingItemDetailType detailType = CIAGroupingItemDetailType.constructTypeFromDescription( keyTokens[4] );

		CIAGroupingItemDetail detail = new CIAGroupingItemDetail();
		detail.setCiaGroupingItemDetailLevel( detailLevel );
		detail.setCiaGroupingItemDetailType( detailType );
		detail.setDateCreated( new Date() );
		detail.setDetailLevelValue( value );

		if ( detail.getCiaGroupingItemDetailType().equals( CIAGroupingItemDetailType.BUY ) )
		{
			if ( propertyValue.length() > 0 )
			{
				try
				{
					int buyAmount = Integer.parseInt( propertyValue );
					if( buyAmount >= 1 )
					{
						detail.setUnits( new Integer( buyAmount ) );
					}
					else
					{
						logger.warn("User-selected buy amount must be greater than zero - Please re-enter");
						continue; // no need to save a 0 or negative number
					}
				}
				catch( NumberFormatException nfe )
				{
					logger.warn("User-selected buy amount must be numerical - Please re-enter");
					continue; // no need to save a non-number
				}
			}
			else
			{
				continue; // no need to save a blank buy number
			}
		}

		groupingItem = ciaGroupingItemsMap.get( groupingItemId );
		detail.setCiaGroupingItem( groupingItem );
		groupingItem.getCiaGroupingItemDetails().add( detail );
	}
	return errorMessage;
}

public static void updateDetailItemsBasedOnBuyingPreferences( Map<String, String[]> parameterMap, 
                                                              List<CIAGroupingItem> ciaGroupingItems,
																Integer groupingItemId ) throws IllegalAccessException,
		InvocationTargetException, NoSuchMethodException
{
	Set<String> paramNameSet = parameterMap.keySet();
	HashMap<Integer, CIAGroupingItem> ciaGroupingItemsMap = new HashMap<Integer, CIAGroupingItem>();
	Collection<String> ucpGroupingItemKeys = CollectionUtils.select( paramNameSet, new ParamFilterPredicate( "ucp_"
	                                                                                     			+ groupingItemId ) );
	Collection<String> ciaPrefGroupingItemKeys = CollectionUtils.select( paramNameSet, new ParamFilterPredicate( "ciaPref_"
	 	                                                                                     			+ groupingItemId ) );

	CIAGroupingItem item = null;
	Iterator<CIAGroupingItem> itemsIter = ciaGroupingItems.iterator();
	while ( itemsIter.hasNext() )
	{
		item = itemsIter.next();
		item.setCiaGroupingItemDetails( cleanCIAGroupingItemDetailPreferences( item.getCiaGroupingItemDetails() ) );

		ciaGroupingItemsMap.put( item.getCiaGroupingItemId(), item );
	}

	/* propertyValue_ciapref_ciaGroupingItemId_ciaGroupingItemDetailLevel_value_ciaGroupingItemDetailType */

	constructCIAPreferenceDetails( parameterMap, ciaGroupingItemsMap, ciaPrefGroupingItemKeys );
	CIAGroupingItem groupingItem = null;
	Iterator<String> keyIter = ucpGroupingItemKeys.iterator();
	while ( keyIter.hasNext() )
	{
		String key = (String)keyIter.next();
		String[] keyTokens = key.split( "_" );
		String value = ((String[]) parameterMap.get( key ))[0];

		// skip values not for this group
		if (groupingItemId.intValue() != Integer.parseInt(keyTokens[1]) )
			continue;

		// skip buy amounts of zero.
		int buyAmount = 0;
		try {
			if (value != null) {
				value = value.trim();
				if (value.length() > 0) {
					buyAmount = Integer.parseInt(value);
				}
			}
		}
		catch (NumberFormatException nfe) {
			logger.warn("Failed to parse buy amount! Changes not saved.", nfe);
			buyAmount = -1;
		}
		
		if (buyAmount <= 0) {
			continue;
		}
		
		// value associated with level silver, 2005, 328si etc.		
		String detailLevelValue = TrimFormattingUtility.unEscapeJS( keyTokens[2] );

		CIAGroupingItemDetail detail = new CIAGroupingItemDetail();
		detail.setCiaGroupingItemDetailLevel( CIAGroupingItemDetailLevel.YEAR );
		detail.setCiaGroupingItemDetailType( CIAGroupingItemDetailType.BUY );
		detail.setDateCreated( new Date() );
		detail.setDetailLevelValue( detailLevelValue );
		detail.setUnits( new Integer( buyAmount ) );

		groupingItem = (CIAGroupingItem)ciaGroupingItemsMap.get( groupingItemId );
		detail.setCiaGroupingItem( groupingItem );
		groupingItem.getCiaGroupingItemDetails().add( detail );
	}
}

public static void createNotes( Collection<String> noteKeys, Map<String, String[]> parameterMap, Collection<CIAGroupingItem> ciaGroupingItems )
{
	Iterator<String> keyIter = noteKeys.iterator();
	Iterator<CIAGroupingItem> itemsIter = null;
	CIAGroupingItem item = null;
	while ( keyIter.hasNext() )
	{
		String key = keyIter.next();
		String[] keyTokens = key.split( "_" );
		String[] values = parameterMap.get( key );
		String notes = values[0];

		int ciaGroupingItemId = Integer.parseInt( keyTokens[1] );
		itemsIter = ciaGroupingItems.iterator();
		while ( itemsIter.hasNext() )
		{
			item = itemsIter.next();
			if ( item.getCiaGroupingItemId().intValue() == ciaGroupingItemId )
			{
				item.setNotes( notes );
				break;
			}
		}
	}

}

private static Set<CIAGroupingItemDetail> cleanCIAGroupingItemDetailPreferences( Set<CIAGroupingItemDetail> ciaGroupingItemDetails )
{
	CIAGroupingItemDetail detail = null;

	List<CIAGroupingItemDetail> removeDetails = new ArrayList<CIAGroupingItemDetail>();
	Iterator<CIAGroupingItemDetail> detailsIter = ciaGroupingItemDetails.iterator();
	while ( detailsIter.hasNext() )
	{
		detail = detailsIter.next();
		if ( detail.getCiaGroupingItemDetailType().equals( CIAGroupingItemDetailType.AVOID )
				|| detail.getCiaGroupingItemDetailType().equals( CIAGroupingItemDetailType.PREFER )
				|| detail.getCiaGroupingItemDetailType().equals( CIAGroupingItemDetailType.BUY ) )
		{
			removeDetails.add( detail );
		}
	}

	ciaGroupingItemDetails.removeAll( removeDetails );

	return ciaGroupingItemDetails;
}

public static void constructUnitCostItemDetails( Collection<String> preferenceKeys, Map<String, String[]> parameterMap, CIAGroupingItem groupingItem )
{
	CIAGroupingItemDetail itemDetail;
	Boolean detailTypeIndicator;
	CIAGroupingItemDetailLevel detailLevel;
	CIAGroupingItemDetailType detailType;
	String detailTypeDesc;
	if ( preferenceKeys != null && !preferenceKeys.isEmpty() )
	{
		Iterator<String> keyIter = preferenceKeys.iterator();
		while ( keyIter.hasNext() )
		{
			String key = keyIter.next();
			String[] keyTokens = key.split( "_" );
			String[] values = parameterMap.get( key );
			String propertyValue = values[0];
			detailTypeIndicator = new Boolean( propertyValue );

			int groupingDescriptionId = Integer.parseInt( keyTokens[1] );
			if ( groupingDescriptionId == groupingItem.getCiaGroupingItemId().intValue() )
			{
				int lowRange = Integer.parseInt( keyTokens[3] );
				int highRange = Integer.parseInt( keyTokens[4] );
				detailTypeDesc = keyTokens[5];

				if ( detailTypeIndicator.booleanValue() )
				{
					itemDetail = new CIAGroupingItemDetail();
					detailType = CIAGroupingItemDetailType.constructTypeFromDescription( detailTypeDesc );
					itemDetail.setCiaGroupingItemDetailType( detailType );

					detailLevel = CIAGroupingItemDetailLevel.UNIT_COST;
					itemDetail.setCiaGroupingItemDetailLevel( detailLevel );

					itemDetail.setLowRange( new Integer( lowRange ) );
					itemDetail.setHighRange( new Integer( highRange ) );
					itemDetail.setCiaGroupingItem( groupingItem );
					groupingItem.addCiaGroupingItemDetail( itemDetail );
				}

			}
		}
	}
}

}
