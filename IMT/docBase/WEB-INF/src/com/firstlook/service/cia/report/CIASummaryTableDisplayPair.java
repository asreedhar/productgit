package com.firstlook.service.cia.report;

// Utility class to pair a string and another string representing an integer amount.
public class CIASummaryTableDisplayPair
{

private int year;
private String ciaGroupingItemDetailLevelDescription;
private Integer inventoryGap;
private boolean managerChoice;

private Integer plannedToBuyAmount;

private String alternateText;

public CIASummaryTableDisplayPair()
{
	super();
	ciaGroupingItemDetailLevelDescription = "";
	inventoryGap = null;
	managerChoice = false;
	plannedToBuyAmount = new Integer( 0 );
	
	alternateText = null;
}

public String getAmountString()
{
	if ( plannedToBuyAmount.intValue() == 0 )
	{
		return "--";
	}
	else
	{
		return plannedToBuyAmount.toString();
	}
}

// Constructs something like "2003 understocked by 2"
// Optional " * " if it is a manager's choice.
public String constructDefaultTextString()
{
	StringBuffer buffer = new StringBuffer();

	buffer.append( ciaGroupingItemDetailLevelDescription );

	if ( inventoryGap == null || managerChoice )
	{
		buffer.append( " * " );
	}
	else
	{
		if ( inventoryGap.intValue() >= 0 )
		{
			buffer.append( " understocked by " );
			buffer.append( inventoryGap.intValue() );
		}
		else
		{
			buffer.append( " overstocked by " );
			buffer.append( -inventoryGap.intValue() );
		}
	}
	return buffer.toString();	
}

public String getTextString()
{
	if(alternateText == null)
		return this.constructDefaultTextString();
	else
		return alternateText;
}

public Integer getInventoryGap()
{
	return inventoryGap;
}

public void setInventoryGap( Integer inventoryGap )
{
	this.inventoryGap = inventoryGap;
}

public String getCiaGroupingItemDetailLevelDescription()
{
	return ciaGroupingItemDetailLevelDescription;
}

public void setCiaGroupingItemDetailLevelDescription( String ciaGroupingItemDetailLevelDescription )
{
	this.ciaGroupingItemDetailLevelDescription = ciaGroupingItemDetailLevelDescription;
}

public Integer getPlannedToBuyAmount()
{
	return plannedToBuyAmount;
}

public void setPlannedToBuyAmount( Integer plannedToBuyAmount )
{
	this.plannedToBuyAmount = plannedToBuyAmount;
}

public boolean isManagerChoice()
{
	return managerChoice;
}

public void setManagerChoice( boolean userSelected )
{
	this.managerChoice = userSelected;
}

public int getYear()
{
	return year;
}

public void setYear( int year )
{
	this.year = year;
}

public String getAlternateText()
{
	return alternateText;
}

public void setAlternateText( String alternateText )
{
	this.alternateText = alternateText;
}

}
