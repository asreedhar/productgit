package com.firstlook.service.tools;

public class GrossProfitTotals
{

private double totalFrontEndGrossProfit;
private double totalBackEndGrossProfit;

public GrossProfitTotals()
{
}

public double getTotalBackEndGrossProfit()
{
    return totalBackEndGrossProfit;
}

public double getTotalFrontEndGrossProfit()
{
    return totalFrontEndGrossProfit;
}

public void setTotalBackEndGrossProfit( double d )
{
    totalBackEndGrossProfit = d;
}

public void setTotalFrontEndGrossProfit( double d )
{
    totalFrontEndGrossProfit = d;
}

}
