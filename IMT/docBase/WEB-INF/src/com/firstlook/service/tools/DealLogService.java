package com.firstlook.service.tools;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;

import com.firstlook.entity.form.SaleForm;

public class DealLogService
{

private int dealerId;
private int inventoryType;

public static final String SALETYPE_RETAIL = "retail";
public static final String SALETYPE_WHOLESALE = "wholesale";
public static final int DEFAULT_WEEKS = 4;

public DealLogService( int dealerId, int inventoryType )
{
    setDealerId(dealerId);
    setInventoryType(inventoryType);
}

public int getDealerId()
{
    return dealerId;
}

public int getInventoryType()
{
    return inventoryType;
}

public void setDealerId( int i )
{
    dealerId = i;
}

public void setInventoryType( int i )
{
    inventoryType = i;
}

public int determineWeeks( String weeksStr )
{
    int weeks;
    if ( weeksStr == null )
    {
        weeks = DEFAULT_WEEKS;
    } else
    {
        weeks = Integer.parseInt(weeksStr);
        if ( weeks > 8 )
        {
            weeks = 8;
        }
    }
    return weeks;
}

public Date generateDateFromWeeks( int weeks )
{
    Calendar startDate = Calendar.getInstance();
    if ( weeks != 0 )
    {
        startDate.add( Calendar.DAY_OF_YEAR, ( -weeks * 7 ) );
    }
    else
    {
        startDate.set( Calendar.DAY_OF_MONTH, 1 );
    }
    return DateUtils.truncate( startDate.getTime(), Calendar.DATE );
}

public GrossProfitTotals sumGrossProfit( List saleForms )
{
    double totalFrontEndGrossProfit = 0.0;
    double totalBackEndGrossProfit = 0.0;

    SaleForm saleForm;
    Iterator saleFormIter = saleForms.iterator();
    while (saleFormIter.hasNext())
    {
        saleForm = (SaleForm) saleFormIter.next();
        totalFrontEndGrossProfit += saleForm.getRetailGrossProfit();
        totalBackEndGrossProfit += saleForm.getFIGrossProfit();
    }

    GrossProfitTotals grossProfitTotals = new GrossProfitTotals();
    grossProfitTotals.setTotalFrontEndGrossProfit(totalFrontEndGrossProfit);
    grossProfitTotals.setTotalBackEndGrossProfit(totalBackEndGrossProfit);

    return grossProfitTotals;
}
}