package com.firstlook.service.tools;

import junit.framework.TestSuite;

public class AllTests
{

public AllTests()
{
    super();
}

public static TestSuite suite()
{
    TestSuite suite = new TestSuite();
    suite.addTest(new TestSuite(TestVehicleAnalyzerService.class));

    return suite;
}
}
