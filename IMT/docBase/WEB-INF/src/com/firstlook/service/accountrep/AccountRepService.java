package com.firstlook.service.accountrep;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;

import com.firstlook.accountrep.DealersToDealerGroupHolder;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.IMember;
import com.firstlook.entity.MemberAccess;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.dealer.IDealerDAO;
import com.firstlook.persistence.dealergroup.IDealerGroupDAO;
import com.firstlook.service.dealer.IDealerService;
import com.firstlook.service.dealergroup.DealerGroupService;
import com.firstlook.service.member.IMemberService;
import com.firstlook.service.memberaccess.IMemberAccessService;

public class AccountRepService
{

private IDealerGroupDAO dealerGroupDAO;
private IDealerDAO dealerDAO;
private IMemberAccessService memberAccessService;
private IMemberService memberService;

public Collection retrieveDealerToDealerGroupHolders( int[] dealerGroupIds, IDealerDAO dealerPersistence, IDealerGroupDAO dealerGroupPersistence )
		throws ApplicationException
{
	Collection<DealersToDealerGroupHolder> dealerToDealerGroupHolders = new ArrayList<DealersToDealerGroupHolder>();
	for ( int i = 0; i < dealerGroupIds.length; i++ )
	{
		Collection dealers = dealerPersistence.findByDealerGroupId( dealerGroupIds[i] );
		DealerGroup dealerGroup = dealerGroupPersistence.findByDealerGroupId( dealerGroupIds[i] );
		DealersToDealerGroupHolder ddgh = new DealersToDealerGroupHolder();
		ddgh.setDealerGroup( dealerGroup );
		ddgh.setDealers( dealers );
		dealerToDealerGroupHolders.add( ddgh );
	}
	return dealerToDealerGroupHolders;
}

public Collection representativeDealers( int memberId ) throws ApplicationException
{
	Collection<Dealer> dealers = new ArrayList<Dealer>();
    IMember member = memberService.retrieveMember( memberId );
    
	if ( !member.isAccountRep() )
	{
		throw new SecurityException( "Member is not an Account Representative" );
	}

    Collection memberAccessRecords = memberAccessService.retrieveByMemberId(memberId);
	Iterator memberAccessIter = memberAccessRecords.iterator();

	while ( memberAccessIter.hasNext() )
	{
		MemberAccess memberAccess = (MemberAccess)memberAccessIter.next();
		Dealer currDealer = dealerDAO.findByPk( memberAccess.getBusinessUnitId().intValue() );
		dealers.add( currDealer );
	}
	return dealers;
}

public Collection findActiveDealerGroups() throws ApplicationException
{
	return dealerGroupDAO.findByActiveDealerGroups();
}

public int[] retrieveBusinessUnitIdArray( int memberId, boolean isDealer, IDealerService dealerService, DealerGroupService dealerGroupService )
		throws ApplicationException, DatabaseException
{
	int[] businessUnitIdArray = null;
	if ( memberId != 0 )
	{
		Collection memberAccessCol = memberAccessService.retrieveByMemberId( memberId );

		if ( memberAccessCol.size() > 0 )
		{
			Collection<Integer> businessUnitCol = new ArrayList<Integer>();
			Iterator memberAccessIt = memberAccessCol.iterator();
			while ( memberAccessIt.hasNext() )
			{
				MemberAccess memberAccess = (MemberAccess)memberAccessIt.next();

				addToBusinessUnitCollection( isDealer, businessUnitCol, memberAccess, dealerService, dealerGroupService );
			}
			businessUnitIdArray = ArrayUtils.toPrimitive( (Integer[])businessUnitCol.toArray( new Integer[0] ) );
		}
	}
	return businessUnitIdArray;
}

public void updateDealerRelationships( int memberId, int[] dealerIdArray ) throws ApplicationException
{
	memberAccessService.deleteByMemberId( memberId );
	insertMemberAccess( memberId, dealerIdArray );
}

private void insertMemberAccess( int memberId, int[] dealerIdArray ) throws ApplicationException
{
	try
	{
		for ( int i = 0; i < dealerIdArray.length; i++ )
		{
			MemberAccess access = new MemberAccess();
			access.setMemberId( new Integer( memberId ) );
			access.setBusinessUnitId( new Integer( dealerIdArray[i] ) );
			memberAccessService.save( access );
		}
	}
	catch ( Exception e )
	{
		throw new ApplicationException( "Unable to insert Member Access record.", e );
	}
}

private void addToBusinessUnitCollection( boolean isDealer, Collection<Integer> businessUnitCol, MemberAccess memberAccess, IDealerService dealerService,
											DealerGroupService dgService ) throws DatabaseException, ApplicationException
{
	if ( isDealer )
	{
		businessUnitCol.add( memberAccess.getBusinessUnitId() );
	}
	else
	{
		Dealer dealer = dealerService.retrieveDealer( memberAccess.getBusinessUnitId().intValue() );
		DealerGroup group = dgService.retrieveByDealerId( dealer.getDealerId().intValue() );

		if ( !businessUnitCol.contains( group.getDealerGroupId() ) )
		{
			businessUnitCol.add( group.getDealerGroupId() );
		}
	}
}

public List<DealerGroup> retriveDealerGroups( Collection dealers, DealerGroupService dgService ) throws ApplicationException
{
	List<DealerGroup> dealerGroups = new ArrayList<DealerGroup>();
	Iterator dealersIter = dealers.iterator();

	while ( dealersIter.hasNext() )
	{
		Dealer dealer = (Dealer)dealersIter.next();
		DealerGroup group = dgService.retrieveByDealerId( dealer.getDealerId().intValue() );
		if ( !dealerGroups.contains( group ) )
		{
			dealerGroups.add( group );
		}
	}

	return dealerGroups;
}

public void setMemberAccessService( IMemberAccessService mockMemberAccessService )
{
	this.memberAccessService = mockMemberAccessService;
}

public void setDealerDAO( IDealerDAO dealerDAO )
{
	this.dealerDAO = dealerDAO;
}

public void setDealerGroupDAO( IDealerGroupDAO dealerGroupDAO )
{
	this.dealerGroupDAO = dealerGroupDAO;
}

public void setMemberService( IMemberService memberService )
{
	this.memberService = memberService;
}



}
