package com.firstlook.service.groupingdescription;

import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

public class TestGroupingDescriptionService extends TestCase
{

private GroupingDescriptionService service;

private GroupingDescriptionValuationAndSalesCount grouping;
private GroupingDescriptionValuationAndSalesCount grouping2;
private Map valuationMap;

public TestGroupingDescriptionService( String name )
{
    super(name);
}

public void setUp()
{
    service = new GroupingDescriptionService();

    grouping = new GroupingDescriptionValuationAndSalesCount();
    grouping2 = new GroupingDescriptionValuationAndSalesCount();
    valuationMap = new HashMap();
}

public void testAddGroupingToMap()
{
    Integer groupingDescriptionId = new Integer(1);

    grouping.setCount(2);
    grouping.setValuation(500.2);

    grouping2.setCount(1);
    grouping2.setValuation(3500.2);

    valuationMap.put(groupingDescriptionId, grouping);

    service.addGroupingToMap(valuationMap, grouping2, groupingDescriptionId);

    GroupingDescriptionValuationAndSalesCount grouping3 = (GroupingDescriptionValuationAndSalesCount) valuationMap
            .get(groupingDescriptionId);

    assertEquals(3, grouping3.getCount());
    assertEquals(1500.2, grouping3.getValuation(), 0.01);
}

public void testAddGroupingToMapSingle()
{
    Integer groupingDescriptionId = new Integer(1);

    grouping.setCount(2);
    grouping.setValuation(500.3);

    service.addGroupingToMap(valuationMap, grouping, groupingDescriptionId);

    GroupingDescriptionValuationAndSalesCount grouping3 = (GroupingDescriptionValuationAndSalesCount) valuationMap
            .get(groupingDescriptionId);

    assertEquals(2, grouping3.getCount());
    assertEquals(500.3, grouping3.getValuation(), 0.0);
}

}
