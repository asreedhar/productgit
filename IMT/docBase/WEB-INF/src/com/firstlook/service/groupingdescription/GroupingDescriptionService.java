package com.firstlook.service.groupingdescription;

import java.util.Collection;
import java.util.Map;

import biz.firstlook.transact.persist.model.GroupingDescription;
import biz.firstlook.transact.persist.persistence.GroupingDescriptionDAO;

public class GroupingDescriptionService
{

private GroupingDescriptionDAO groupingDescriptionDAO;

public GroupingDescriptionService()
{
    groupingDescriptionDAO = new GroupingDescriptionDAO();
}

public Collection retrieveAllGroupingDescriptions()
{
    return groupingDescriptionDAO.findAll();
}

public Collection retrieveMarketSuppressedGroupingDescriptions()
{
    return groupingDescriptionDAO.findMarketSuppressed();
}

public GroupingDescription retrieveById( Integer groupingId )
{
    return groupingDescriptionDAO.findByID(groupingId);
}

public String retrieveDescriptionById( Integer groupingDescriptionId )
{
    GroupingDescription gd = retrieveById(groupingDescriptionId);
    return gd.getGroupingDescription();
}

void addGroupingToMap( Map valuationAndSalesMap,
        GroupingDescriptionValuationAndSalesCount gdvsc,
        Integer groupingDescriptionId )
{
    if ( valuationAndSalesMap.containsKey(groupingDescriptionId) )
    {
        GroupingDescriptionValuationAndSalesCount retrievedGdvsc = (GroupingDescriptionValuationAndSalesCount) valuationAndSalesMap
                .get(groupingDescriptionId);
        int unitsSold = retrievedGdvsc.getCount();
        double valuation = retrievedGdvsc.getValuation();
        double sumValuation = valuation * unitsSold;

        int secondUnitsSold = gdvsc.getCount();
        double secondValuation = gdvsc.getValuation();
        double sumSecondValuation = secondValuation * secondUnitsSold;

        int totalUnitsSold = unitsSold + secondUnitsSold;
        double averageValuation = (sumValuation + sumSecondValuation)
                / totalUnitsSold;
        gdvsc.setCount(totalUnitsSold);
        gdvsc.setValuation(averageValuation);
    }
    valuationAndSalesMap.put(groupingDescriptionId, gdvsc);
}

public void save( GroupingDescription groupingDesc )
{
    groupingDescriptionDAO.save(groupingDesc);
}

public void saveOrUpdate( GroupingDescription groupingDesc )
{
    groupingDescriptionDAO.saveOrUpdate(groupingDesc);
}

public void delete( GroupingDescription groupingDesc )
{
    groupingDescriptionDAO.delete(groupingDesc);
}

public String retrieveDescriptionByMakeModel( String make, String model )
{
    // KL - this is the mmg make and model
    return groupingDescriptionDAO.findDescriptionByMakeModel(make, model);
}

public int retrieveIDByMakeModel( String make, String model )
{
    // KL - this is the mmg make and model
    return groupingDescriptionDAO.findIDByMakeModel(make, model);
}

public GroupingDescription retrieveByMakeModel( String make, String model )
{
    // KL - this is the mmg make and model
    return groupingDescriptionDAO.findByMakeModel(make, model);
}

}
