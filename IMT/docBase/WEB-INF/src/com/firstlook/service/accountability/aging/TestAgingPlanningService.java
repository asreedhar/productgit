package com.firstlook.service.accountability.aging;

import junit.framework.TestCase;

import com.firstlook.persistence.accountability.MockGetMeasure;
import com.firstlook.service.accountability.reports.aging.AgingPlanningReport;

public class TestAgingPlanningService extends TestCase {
    public TestAgingPlanningService(String name) {
        super(name);
    }

    public void testAgingPlanningService() {
        MockGetMeasure getMeasure = new MockGetMeasure();
        AgingPlanningService service = new AgingPlanningService();
        service.setGetMeasure(getMeasure);

        // Watchlist (Units, Strategies, Weeks)
        getMeasure.addMeasure(30);
        getMeasure.addMeasure(20);
        getMeasure.addMeasure(3);

        // Sixty+ (Units, Strategies, Weeks)
        getMeasure.addMeasure(40);
        getMeasure.addMeasure(20);
        getMeasure.addMeasure(2);

        // Fifty (Units, Strategies, Weeks)
        getMeasure.addMeasure(10);
        getMeasure.addMeasure(8);
        getMeasure.addMeasure(4);

        // Forty (Units, Strategies, Weeks)
        getMeasure.addMeasure(43);
        getMeasure.addMeasure(23);
        getMeasure.addMeasure(3);

        // Thirty (Units, Strategies, Weeks)
        getMeasure.addMeasure(27);
        getMeasure.addMeasure(24);
        getMeasure.addMeasure(1);

        AgingPlanningReport report = (AgingPlanningReport) service.calculate(0);

        // Validate Percentages
        assertEquals(89, report.getThirtyP().intValue());
        assertEquals(53, report.getFortyP().intValue());
        assertEquals(80, report.getFiftyP().intValue());
        assertEquals(50, report.getSixtyP().intValue());
        assertEquals(67, report.getWatchListP().intValue());

        assertEquals(25, report.getThirtyWeeksP().intValue());
        assertEquals(75, report.getFortyWeeksP().intValue());
        assertEquals(100, report.getFiftyWeeksP().intValue());
        assertEquals(50, report.getSixtyWeeksP().intValue());
        assertEquals(75, report.getWatchListWeeksP().intValue());
    }

    public void testAgingPlanningServiceZeros() {
        MockGetMeasure getMeasure = new MockGetMeasure();
        AgingPlanningService service = new AgingPlanningService();
        service.setGetMeasure(getMeasure);

        // Watchlist (Units, Strategies, Weeks)
        getMeasure.addMeasure(0);
        getMeasure.addMeasure(0);
        getMeasure.addMeasure(0);

        // Sixty+ (Units, Strategies, Weeks)
        getMeasure.addMeasure(0);
        getMeasure.addMeasure(0);
        getMeasure.addMeasure(0);

        // Fifty (Units, Strategies, Weeks)
        getMeasure.addMeasure(0);
        getMeasure.addMeasure(0);
        getMeasure.addMeasure(0);

        // Forty (Units, Strategies, Weeks)
        getMeasure.addMeasure(0);
        getMeasure.addMeasure(0);
        getMeasure.addMeasure(0);

        // Thirty (Units, Strategies, Weeks)
        getMeasure.addMeasure(0);
        getMeasure.addMeasure(0);
        getMeasure.addMeasure(0);

        AgingPlanningReport report = (AgingPlanningReport) service.calculate(0);

        // Validate Percentages
        assertEquals(0, report.getThirtyP().intValue());
        assertEquals(0, report.getFortyP().intValue());
        assertEquals(0, report.getFiftyP().intValue());
        assertEquals(0, report.getSixtyP().intValue());
        assertEquals(0, report.getWatchListP().intValue());

        assertEquals(0, report.getThirtyWeeksP().intValue());
        assertEquals(0, report.getFortyWeeksP().intValue());
        assertEquals(0, report.getFiftyWeeksP().intValue());
        assertEquals(0, report.getSixtyWeeksP().intValue());
        assertEquals(0, report.getWatchListWeeksP().intValue());
    }
    
    public void testAgingPlanningServiceNoStrategies() {
        MockGetMeasure getMeasure = new MockGetMeasure();
        AgingPlanningService service = new AgingPlanningService();
        service.setGetMeasure(getMeasure);

        // Watchlist (Units, Strategies, Weeks)
        getMeasure.addMeasure(20);
        getMeasure.addMeasure(0);
        getMeasure.addMeasure(0);

        // Sixty+ (Units, Strategies, Weeks)
        getMeasure.addMeasure(20);
        getMeasure.addMeasure(0);
        getMeasure.addMeasure(0);

        // Fifty (Units, Strategies, Weeks)
        getMeasure.addMeasure(20);
        getMeasure.addMeasure(0);
        getMeasure.addMeasure(0);

        // Forty (Units, Strategies, Weeks)
        getMeasure.addMeasure(30);
        getMeasure.addMeasure(0);
        getMeasure.addMeasure(0);

        // Thirty (Units, Strategies, Weeks)
        getMeasure.addMeasure(59);
        getMeasure.addMeasure(0);
        getMeasure.addMeasure(0);

        AgingPlanningReport report = (AgingPlanningReport) service.calculate(0);

        // Validate Percentages
        assertEquals(0, report.getThirtyP().intValue());
        assertEquals(0, report.getFortyP().intValue());
        assertEquals(0, report.getFiftyP().intValue());
        assertEquals(0, report.getSixtyP().intValue());
        assertEquals(0, report.getWatchListP().intValue());

        assertEquals(0, report.getThirtyWeeksP().intValue());
        assertEquals(0, report.getFortyWeeksP().intValue());
        assertEquals(0, report.getFiftyWeeksP().intValue());
        assertEquals(0, report.getSixtyWeeksP().intValue());
        assertEquals(0, report.getWatchListWeeksP().intValue());
    }

    public void testAgingPlanningServiceBogusData() {
        MockGetMeasure getMeasure = new MockGetMeasure();
        AgingPlanningService service = new AgingPlanningService();
        service.setGetMeasure(getMeasure);

        // Watchlist (Units, Strategies, Weeks)
        getMeasure.addMeasure(-20);
        getMeasure.addMeasure(Float.NEGATIVE_INFINITY);
        getMeasure.addMeasure(42);

        // Sixty+ (Units, Strategies, Weeks)
        getMeasure.addMeasure(Float.NaN);
        getMeasure.addMeasure(-3.22f);
        getMeasure.addMeasure(5);

        // Fifty (Units, Strategies, Weeks)
        getMeasure.addMeasure(0);
        getMeasure.addMeasure(24);
        getMeasure.addMeasure(3.0f);

        // Forty (Units, Strategies, Weeks)
        getMeasure.addMeasure(30);
        getMeasure.addMeasure(Float.POSITIVE_INFINITY);
        getMeasure.addMeasure(0.23f);

        // Thirty (Units, Strategies, Weeks)
        getMeasure.addMeasure(59);
        getMeasure.addMeasure(2);
        getMeasure.addMeasure(2);

        AgingPlanningReport report = (AgingPlanningReport) service.calculate(0);

        // Validate Percentages
        assertEquals(3, report.getThirtyP().intValue());
        assertEquals(2147483647, report.getFortyP().intValue());
        assertEquals(0, report.getFiftyP().intValue());
        assertEquals(0, report.getSixtyP().intValue());
        assertEquals(0, report.getWatchListP().intValue());

        assertEquals(50, report.getThirtyWeeksP().intValue());
        assertEquals(6, report.getFortyWeeksP().intValue());
        assertEquals(75, report.getFiftyWeeksP().intValue());
        assertEquals(125, report.getSixtyWeeksP().intValue());
        assertEquals(1050, report.getWatchListWeeksP().intValue());
    }
}
