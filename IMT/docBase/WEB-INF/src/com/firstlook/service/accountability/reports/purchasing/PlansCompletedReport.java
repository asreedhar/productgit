package com.firstlook.service.accountability.reports.purchasing;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.firstlook.service.accountability.reports.EdgeScorecardReport;

public class PlansCompletedReport extends EdgeScorecardReport {

    private Integer week1;
    private Integer week2;
    private Integer week3;
    private Integer week4;

    private String week1P;
    private String week2P;
    private String week3P;
    private String week4P;

    private String week1C;
    private String week2C;
    private String week3C;
    private String week4C;
    
    public Integer getWeek1() {
        return week1;
    }
    
    public void setWeek1(Integer week1) {
        this.week1 = week1;
    }
    
    public String getWeek1C() {
        return week1C;
    }
    
    public void setWeek1C(String week1C) {
        this.week1C = week1C;
    }
    
    public String getWeek1P() {
        return week1P;
    }
    
    public void setWeek1P(String week1P) {
        this.week1P = week1P;
    }
    
    public Integer getWeek2() {
        return week2;
    }
    
    public void setWeek2(Integer week2) {
        this.week2 = week2;
    }
    
    public String getWeek2C() {
        return week2C;
    }
    
    public void setWeek2C(String week2C) {
        this.week2C = week2C;
    }
    
    public String getWeek2P() {
        return week2P;
    }
    
    public void setWeek2P(String week2P) {
        this.week2P = week2P;
    }
    
    public Integer getWeek3() {
        return week3;
    }
    
    public void setWeek3(Integer week3) {
        this.week3 = week3;
    }
    
    public String getWeek3C() {
        return week3C;
    }
    
    public void setWeek3C(String week3C) {
        this.week3C = week3C;
    }
    
    public String getWeek3P() {
        return week3P;
    }
    
    public void setWeek3P(String week3P) {
        this.week3P = week3P;
    }
    
    public Integer getWeek4() {
        return week4;
    }
    
    public void setWeek4(Integer week4) {
        this.week4 = week4;
    }
    
    public String getWeek4C() {
        return week4C;
    }
    
    public void setWeek4C(String week4C) {
        this.week4C = week4C;
    }
    
    public String getWeek4P() {
        return week4P;
    }
    
    public void setWeek4P(String week4P) {
        this.week4P = week4P;
    }
    
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }    
}
