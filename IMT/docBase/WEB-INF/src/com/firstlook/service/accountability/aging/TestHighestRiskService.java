package com.firstlook.service.accountability.aging;

import junit.framework.TestCase;

import com.firstlook.persistence.accountability.MockGetMeasure;
import com.firstlook.service.accountability.reports.aging.HighestRiskReport;

public class TestHighestRiskService extends TestCase {

    public TestHighestRiskService(String name) {
        super(name);
    }
    
    public void testHighestRiskServiceRedWarning() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        HighestRiskService service = new HighestRiskService();
        service.setGetMeasure( getMeasure );

        // Red / Yellow
        getMeasure.addMeasure( 30f );
        getMeasure.addMeasure( 20f );
        
        // All
        getMeasure.addMeasure( 400f );
        
        getMeasure.addFormattedMeasure( "TEST" );
        getMeasure.addFormattedMeasure( "TEST" );
        getMeasure.addFormattedMeasure( "TEST" );
        getMeasure.addFormattedMeasure( "TEST" );
        
        HighestRiskReport report = (HighestRiskReport) service.calculate( 0 );
        
        assertEquals( new Integer( 30 ), report.getRedOver15Units() );
        assertEquals( "TEST", report.getRedOver15Inv() );
        assertEquals( "TEST", report.getRedOver15Water() );
        assertEquals( "<span style=\"color:#900;font-weight:bold\">Danger: 30</span> Highest Risk vehicles aging in inventory.",
                      report.getRedOutput() );
        assertEquals( new Integer( 20 ), report.getYellowOver30Units() );
        assertEquals( "TEST", report.getYellowOver30Inv() );
        assertEquals( "TEST", report.getYellowOver30Water() );
        assertEquals( "Management of Highest Risk vehicles is good.", report.getYellowOutput() );
    }

    public void testHighestRiskServiceYellowWarning() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        HighestRiskService service = new HighestRiskService();
        service.setGetMeasure( getMeasure );

        // Red / Yellow
        getMeasure.addMeasure( 20f );
        getMeasure.addMeasure( 50f );
        
        // All
        getMeasure.addMeasure( 400f );
        
        getMeasure.addFormattedMeasure( "TEST" );
        getMeasure.addFormattedMeasure( "TEST" );
        getMeasure.addFormattedMeasure( "TEST" );
        getMeasure.addFormattedMeasure( "TEST" );
        
        HighestRiskReport report = (HighestRiskReport) service.calculate( 0 );
        
        assertEquals( new Integer( 20 ), report.getRedOver15Units() );
        assertEquals( "TEST", report.getRedOver15Inv() );
        assertEquals( "TEST", report.getRedOver15Water() );
        assertEquals( "Management of Highest Risk vehicles is good.",
                      report.getRedOutput() );
        assertEquals( new Integer( 50 ), report.getYellowOver30Units() );
        assertEquals( "TEST", report.getYellowOver30Inv() );
        assertEquals( "TEST", report.getYellowOver30Water() );
        assertEquals( "<span style=\"font-weight:bold\">Caution: 50</span> Higher Risk vehicles aging in inventory.", report.getYellowOutput() );
    }

    public void testHighestRiskServiceBothWarning() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        HighestRiskService service = new HighestRiskService();
        service.setGetMeasure( getMeasure );

        // Red / Yellow
        getMeasure.addMeasure( 30f );
        getMeasure.addMeasure( 50f );
        
        // All
        getMeasure.addMeasure( 400f );
        
        getMeasure.addFormattedMeasure( "TEST" );
        getMeasure.addFormattedMeasure( "TEST" );
        getMeasure.addFormattedMeasure( "TEST" );
        getMeasure.addFormattedMeasure( "TEST" );
        
        HighestRiskReport report = (HighestRiskReport) service.calculate( 0 );
        
        assertEquals( new Integer( 30 ), report.getRedOver15Units() );
        assertEquals( "TEST", report.getRedOver15Inv() );
        assertEquals( "TEST", report.getRedOver15Water() );
        assertEquals( "<span style=\"color:#900;font-weight:bold\">Danger: 30</span> Highest Risk vehicles aging in inventory.",
                      report.getRedOutput() );
        assertEquals( new Integer( 50 ), report.getYellowOver30Units() );
        assertEquals( "TEST", report.getYellowOver30Inv() );
        assertEquals( "TEST", report.getYellowOver30Water() );
        assertEquals( "<span style=\"font-weight:bold\">Caution: 50</span> Higher Risk vehicles aging in inventory.", report.getYellowOutput() );
    }
}
