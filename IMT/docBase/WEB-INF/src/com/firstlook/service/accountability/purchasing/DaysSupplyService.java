package com.firstlook.service.accountability.purchasing;

import com.firstlook.service.accountability.EdgeScorecardService;
import com.firstlook.service.accountability.reports.EdgeScorecardReport;
import com.firstlook.service.accountability.reports.purchasing.DaysSupplyReport;

public class DaysSupplyService extends EdgeScorecardService {

    public DaysSupplyService() {
        super();
    }
    
    public EdgeScorecardReport calculate(int dealerId) {
       
        float sedanDS = getMeasure.assembleMeasure(dealerId,"Composite Basis Period","Purchasing","Current Inventory","Days Supply by Segment","Sedan");
        float suvDS = getMeasure.assembleMeasure(dealerId,"Composite Basis Period","Purchasing","Current Inventory","Days Supply by Segment","SUV");
        float truckDS = getMeasure.assembleMeasure(dealerId,"Composite Basis Period","Purchasing","Current Inventory","Days Supply by Segment","Truck");
        float coupeDS = getMeasure.assembleMeasure(dealerId,"Composite Basis Period","Purchasing","Current Inventory","Days Supply by Segment","Coupe");
        float vanDS = getMeasure.assembleMeasure(dealerId,"Composite Basis Period","Purchasing","Current Inventory","Days Supply by Segment","Van");
        float wagonDS = getMeasure.assembleMeasure(dealerId,"Composite Basis Period","Purchasing","Current Inventory","Days Supply by Segment","Wagon");
        float convertibleDS = getMeasure.assembleMeasure(dealerId,"Composite Basis Period","Purchasing","Current Inventory","Days Supply by Segment","Convertible");

        DaysSupplyReport report = new DaysSupplyReport();
        report.setSedanDS(new Integer( Math.round( sedanDS ) ) );
        report.setSuvDS(new Integer( Math.round( suvDS ) ) );
        report.setTruckDS(new Integer( Math.round( truckDS ) ) );
        report.setCoupeDS(new Integer( Math.round( coupeDS ) ) );
        report.setVanDS(new Integer( Math.round( vanDS ) ) );
        report.setWagonDS(new Integer( Math.round( wagonDS ) ) );
        report.setConvertibleDS(new Integer( Math.round( convertibleDS ) ) );

        return report;
    }

    
}
