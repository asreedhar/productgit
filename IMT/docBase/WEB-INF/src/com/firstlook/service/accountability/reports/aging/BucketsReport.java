package com.firstlook.service.accountability.reports.aging;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.firstlook.service.accountability.reports.EdgeScorecardReport;

public class BucketsReport extends EdgeScorecardReport {

    private String watchListColor;
    private String sixtyColor;
    private String fiftyColor;
    private String fortyColor;
    private String thirtyColor;

    private String sixtyPColor;
    private String fiftyPColor;
    private String fortyPColor;
    private String thirtyPColor;

    private Integer watchListP;
    private Integer sixtyPlusP;
    private Integer fifty59P;
    private Integer forty49P;
    private Integer thirty39P;

    private String watchListWater;
    private String sixtyPlusWater;
    private String fifty59Water;
    private String forty49Water;
    private String thirty39Water;
    public Integer getFifty59P() {
        return fifty59P;
    }
    
    public void setFifty59P(Integer fifty59P) {
        this.fifty59P = fifty59P;
    }
    
    public String getFifty59Water() {
        return fifty59Water;
    }
    
    public void setFifty59Water(String fifty59Water) {
        this.fifty59Water = fifty59Water;
    }
    
    public String getFiftyPColor() {
        return fiftyPColor;
    }
    
    public void setFiftyPColor(String fiftyPColor) {
        this.fiftyPColor = fiftyPColor;
    }
    
    public Integer getForty49P() {
        return forty49P;
    }
    
    public void setForty49P(Integer forty49P) {
        this.forty49P = forty49P;
    }
    
    public String getForty49Water() {
        return forty49Water;
    }
    
    public void setForty49Water(String forty49Water) {
        this.forty49Water = forty49Water;
    }
    
    public String getFortyPColor() {
        return fortyPColor;
    }
    
    public void setFortyPColor(String fortyPColor) {
        this.fortyPColor = fortyPColor;
    }
    
    public String getSixtyPColor() {
        return sixtyPColor;
    }
    
    public void setSixtyPColor(String sixtyPColor) {
        this.sixtyPColor = sixtyPColor;
    }
    
    public Integer getSixtyPlusP() {
        return sixtyPlusP;
    }
    
    public void setSixtyPlusP(Integer sixtyPlusP) {
        this.sixtyPlusP = sixtyPlusP;
    }
    
    public String getSixtyPlusWater() {
        return sixtyPlusWater;
    }
    
    public void setSixtyPlusWater(String sixtyPlusWater) {
        this.sixtyPlusWater = sixtyPlusWater;
    }
    
    public Integer getThirty39P() {
        return thirty39P;
    }
    
    public void setThirty39P(Integer thirty39P) {
        this.thirty39P = thirty39P;
    }
    
    public String getThirty39Water() {
        return thirty39Water;
    }
    
    public void setThirty39Water(String thirty39Water) {
        this.thirty39Water = thirty39Water;
    }
    
    public String getThirtyPColor() {
        return thirtyPColor;
    }
    
    public void setThirtyPColor(String thirtyPColor) {
        this.thirtyPColor = thirtyPColor;
    }
    
    public String getWatchListColor() {
        return watchListColor;
    }
    
    public void setWatchListColor(String watchListColor) {
        this.watchListColor = watchListColor;
    }
    
    public Integer getWatchListP() {
        return watchListP;
    }
    
    public void setWatchListP(Integer watchListP) {
        this.watchListP = watchListP;
    }
    
    public String getWatchListWater() {
        return watchListWater;
    }
    
    public void setWatchListWater(String watchListWater) {
        this.watchListWater = watchListWater;
    }

    
    
    public String getFiftyColor() {
        return fiftyColor;
    }
    

    public void setFiftyColor(String fiftyColor) {
        this.fiftyColor = fiftyColor;
    }
    

    public String getFortyColor() {
        return fortyColor;
    }
    

    public void setFortyColor(String fortyColor) {
        this.fortyColor = fortyColor;
    }
    

    public String getSixtyColor() {
        return sixtyColor;
    }
    

    public void setSixtyColor(String sixtyColor) {
        this.sixtyColor = sixtyColor;
    }
    

    public String getThirtyColor() {
        return thirtyColor;
    }
    

    public void setThirtyColor(String thirtyColor) {
        this.thirtyColor = thirtyColor;
    }
    

    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
    
    
}
