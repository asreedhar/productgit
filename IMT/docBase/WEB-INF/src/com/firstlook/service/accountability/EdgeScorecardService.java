package com.firstlook.service.accountability;

import java.io.StringWriter;
import java.util.Iterator;
import java.util.Map;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import com.firstlook.persistence.accountability.IGetMeasure;
import com.firstlook.service.accountability.reports.EdgeScorecardReport;

public abstract class EdgeScorecardService {
    
    protected IGetMeasure getMeasure;

    public EdgeScorecardService() {
        initializeVelocity();
    }
    
    public abstract EdgeScorecardReport calculate(int dealerId);
    
    private void initializeVelocity() {
        try {
            Velocity.init();
        } catch( Exception e ) {
            throw new RuntimeException( "Error initializing velocity" );
        }
    }

    public IGetMeasure getGetMeasure() {
        return getMeasure;
    }

    public void setGetMeasure(IGetMeasure getMeasure) {
        this.getMeasure = getMeasure;
    }

    protected int percent(float total, float portion) {
        int percentage = 0;
        if( total != 0 ) {
            percentage = (total > 0) ? Math.round(portion / total * 100) : 0;
        }
        return percentage;
    }
    
    protected String evaluateTemplate(Map params, String template) {
        VelocityContext context = new VelocityContext();
        Iterator keyIter = params.keySet().iterator();
        while( keyIter.hasNext() ) {
            String key = (String) keyIter.next();
            Object value = params.get( key );
            context.put( key, value );
        }
        
        StringWriter writer = new StringWriter();
        
        try {
            Velocity.evaluate( context, writer, "EdgeScorecard", template);
        } catch( Exception e ) {
            throw new RuntimeException( "Error merging percent template for EdgeScorecard" );
        }
        return writer.toString();
    }

}
