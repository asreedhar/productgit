package com.firstlook.service.accountability.reports.purchasing;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.firstlook.service.accountability.reports.EdgeScorecardReport;

public class PurchasingDisciplineReport extends EdgeScorecardReport {

    private Integer redLights;
    private Integer understock;
    private Integer overstock;

    private String redSentence;

    public Integer getOverstock() {
        return overstock;
    }
    

    public void setOverstock(Integer overstock) {
        this.overstock = overstock;
    }
    

    public Integer getRedLights() {
        return redLights;
    }
    

    public void setRedLights(Integer redLights) {
        this.redLights = redLights;
    }
    

    public String getRedSentence() {
        return redSentence;
    }
    

    public void setRedSentence(String redSentence) {
        this.redSentence = redSentence;
    }
    

    public Integer getUnderstock() {
        return understock;
    }
    

    public void setUnderstock(Integer understock) {
        this.understock = understock;
    }
    
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
