package com.firstlook.service.accountability.aging;

import junit.framework.TestCase;

import com.firstlook.persistence.accountability.MockGetMeasure;
import com.firstlook.service.accountability.reports.aging.BucketsReport;

public class TestBucketsService extends TestCase {
    
    public TestBucketsService(String name) {
        super( name );
    }
    
    public void testBucketsService() {
        MockGetMeasure getMeasure = new MockGetMeasure();
        BucketsService service = new BucketsService();
        service.setGetMeasure( getMeasure );
        
        // All Inventory Units
        getMeasure.addMeasure( 120 );

        // Watchlist (Units, NF)
        getMeasure.addMeasure( 10 );
        getMeasure.addMeasure( -1 );
        
        // Sixty (Units, NF)
        getMeasure.addMeasure( 30 );
        getMeasure.addMeasure( 0 );
        
        // Fifty (Units, NF)
        getMeasure.addMeasure( 40 );
        getMeasure.addMeasure( -1 );
        
        // Forty (Units, NF)
        getMeasure.addMeasure( 20 );
        getMeasure.addMeasure( 0 );
        
        // Thirty (Units, NF)
        getMeasure.addMeasure( 20 );
        getMeasure.addMeasure( 0 );

        getMeasure.addFormattedMeasure( "TEST" );
        getMeasure.addFormattedMeasure( "TEST" );
        getMeasure.addFormattedMeasure( "TEST" );
        getMeasure.addFormattedMeasure( "TEST" );
        getMeasure.addFormattedMeasure( "TEST" );

        BucketsReport report = (BucketsReport) service.calculate(0);
        
        assertEquals( "#900", report.getWatchListColor() );
        assertEquals( "#000", report.getSixtyColor() );
        assertEquals( "#900", report.getFiftyColor() );
        assertEquals( "#000", report.getFortyColor() );
        assertEquals( "#000", report.getThirtyColor() );

        assertEquals( "#900", report.getSixtyPColor() );
        assertEquals( "#900", report.getFiftyPColor() );
        assertEquals( "#900", report.getFortyPColor() );
        assertEquals( "#000", report.getThirtyPColor() );
        
        assertEquals( 8, report.getWatchListP().intValue() );
        assertEquals( 25, report.getSixtyPlusP().intValue() );
        assertEquals( 33, report.getFifty59P().intValue() );
        assertEquals( 17, report.getForty49P().intValue() );
        assertEquals( 17, report.getThirty39P().intValue() );
        
        assertEquals( "TEST", report.getWatchListWater() );
        assertEquals( "TEST", report.getSixtyPlusWater() );
        assertEquals( "TEST", report.getFifty59Water() );
        assertEquals( "TEST", report.getForty49Water() );
        assertEquals( "TEST", report.getThirty39Water() );
    }

}
