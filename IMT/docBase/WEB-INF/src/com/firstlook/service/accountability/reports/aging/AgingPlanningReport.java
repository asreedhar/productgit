package com.firstlook.service.accountability.reports.aging;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.firstlook.service.accountability.reports.EdgeScorecardReport;

public class AgingPlanningReport extends EdgeScorecardReport {

    private Integer watchListWeeksP;
    private Integer sixtyWeeksP;
    private Integer fiftyWeeksP;
    private Integer fortyWeeksP;
    private Integer thirtyWeeksP;
                
    private Integer watchListWeeks;
    private Integer sixtyWeeks;
    private Integer fiftyWeeks;
    private Integer fortyWeeks;
    private Integer thirtyWeeks;
    
    private Integer watchListP;
    private Integer sixtyP;
    private Integer fiftyP;
    private Integer fortyP;
    private Integer thirtyP;

    private Integer watchListStrategies;
    private Integer sixtyStrategies;
    private Integer fiftyStrategies;
    private Integer fortyStrategies;
    private Integer thirtyStrategies;

    private Integer watchListUnits;
    private Integer sixtyUnits;
    private Integer fiftyUnits;
    private Integer fortyUnits;
    private Integer thirtyUnits;

    public Integer getFiftyP() {
        return fiftyP;
    }
    
    public void setFiftyP(Integer fiftyP) {
        this.fiftyP = fiftyP;
    }
    
    public Integer getFiftyStrategies() {
        return fiftyStrategies;
    }
    
    public void setFiftyStrategies(Integer fiftyStrategies) {
        this.fiftyStrategies = fiftyStrategies;
    }
    
    public Integer getFiftyUnits() {
        return fiftyUnits;
    }
    
    public void setFiftyUnits(Integer fiftyUnits) {
        this.fiftyUnits = fiftyUnits;
    }
    
    public Integer getFiftyWeeks() {
        return fiftyWeeks;
    }
    
    public void setFiftyWeeks(Integer fiftyWeeks) {
        this.fiftyWeeks = fiftyWeeks;
    }
    
    public Integer getFiftyWeeksP() {
        return fiftyWeeksP;
    }
    
    public void setFiftyWeeksP(Integer fiftyWeeksP) {
        this.fiftyWeeksP = fiftyWeeksP;
    }
    
    public Integer getFortyP() {
        return fortyP;
    }
    
    public void setFortyP(Integer fortyP) {
        this.fortyP = fortyP;
    }
    
    public Integer getFortyStrategies() {
        return fortyStrategies;
    }
    
    public void setFortyStrategies(Integer fortyStrategies) {
        this.fortyStrategies = fortyStrategies;
    }
    
    public Integer getFortyUnits() {
        return fortyUnits;
    }
    
    public void setFortyUnits(Integer fortyUnits) {
        this.fortyUnits = fortyUnits;
    }
    
    public Integer getFortyWeeks() {
        return fortyWeeks;
    }
    
    public void setFortyWeeks(Integer fortyWeeks) {
        this.fortyWeeks = fortyWeeks;
    }
    
    public Integer getFortyWeeksP() {
        return fortyWeeksP;
    }
    
    public void setFortyWeeksP(Integer fortyWeeksP) {
        this.fortyWeeksP = fortyWeeksP;
    }
    
    public Integer getSixtyP() {
        return sixtyP;
    }
    
    public void setSixtyP(Integer sixtyP) {
        this.sixtyP = sixtyP;
    }
    
    public Integer getSixtyStrategies() {
        return sixtyStrategies;
    }
    
    public void setSixtyStrategies(Integer sixtyStrategies) {
        this.sixtyStrategies = sixtyStrategies;
    }
    
    public Integer getSixtyUnits() {
        return sixtyUnits;
    }
    
    public void setSixtyUnits(Integer sixtyUnits) {
        this.sixtyUnits = sixtyUnits;
    }
    
    public Integer getSixtyWeeks() {
        return sixtyWeeks;
    }
    
    public void setSixtyWeeks(Integer sixtyWeeks) {
        this.sixtyWeeks = sixtyWeeks;
    }
    
    public Integer getSixtyWeeksP() {
        return sixtyWeeksP;
    }
    
    public void setSixtyWeeksP(Integer sixtyWeeksP) {
        this.sixtyWeeksP = sixtyWeeksP;
    }
    
    public Integer getThirtyP() {
        return thirtyP;
    }
    
    public void setThirtyP(Integer thirtyP) {
        this.thirtyP = thirtyP;
    }
    
    public Integer getThirtyStrategies() {
        return thirtyStrategies;
    }
    
    public void setThirtyStrategies(Integer thirtyStrategies) {
        this.thirtyStrategies = thirtyStrategies;
    }
    
    public Integer getThirtyUnits() {
        return thirtyUnits;
    }
    
    public void setThirtyUnits(Integer thirtyUnits) {
        this.thirtyUnits = thirtyUnits;
    }
    
    public Integer getThirtyWeeks() {
        return thirtyWeeks;
    }
    
    public void setThirtyWeeks(Integer thirtyWeeks) {
        this.thirtyWeeks = thirtyWeeks;
    }
    
    public Integer getThirtyWeeksP() {
        return thirtyWeeksP;
    }
    
    public void setThirtyWeeksP(Integer thirtyWeeksP) {
        this.thirtyWeeksP = thirtyWeeksP;
    }
    
    public Integer getWatchListP() {
        return watchListP;
    }
    
    public void setWatchListP(Integer watchListP) {
        this.watchListP = watchListP;
    }
    
    public Integer getWatchListStrategies() {
        return watchListStrategies;
    }
    
    public void setWatchListStrategies(Integer watchListStrategies) {
        this.watchListStrategies = watchListStrategies;
    }
    
    public Integer getWatchListUnits() {
        return watchListUnits;
    }
    
    public void setWatchListUnits(Integer watchListUnits) {
        this.watchListUnits = watchListUnits;
    }
    
    public Integer getWatchListWeeks() {
        return watchListWeeks;
    }
    
    public void setWatchListWeeks(Integer watchListWeeks) {
        this.watchListWeeks = watchListWeeks;
    }
    
    public Integer getWatchListWeeksP() {
        return watchListWeeksP;
    }
    
    public void setWatchListWeeksP(Integer watchListWeeksP) {
        this.watchListWeeksP = watchListWeeksP;
    }
    
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
    
}
