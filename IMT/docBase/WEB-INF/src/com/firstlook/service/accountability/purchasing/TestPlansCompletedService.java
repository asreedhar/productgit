package com.firstlook.service.accountability.purchasing;

import junit.framework.TestCase;

import com.firstlook.persistence.accountability.MockGetMeasure;
import com.firstlook.service.accountability.reports.purchasing.PlansCompletedReport;

public class TestPlansCompletedService extends TestCase {
    
    public TestPlansCompletedService(String name) {
        super(name);
    }
    
    public void testPlansCompletedService() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        PlansCompletedService service = new PlansCompletedService();
        service.setGetMeasure( getMeasure );
        
        // WEEK 1,2,3,4 Plans 
        getMeasure.addMeasure( 1 );
        getMeasure.addMeasure( 0 );
        getMeasure.addMeasure( 1 );
        getMeasure.addMeasure( 0 );
        
        PlansCompletedReport report = (PlansCompletedReport) service.calculate( 0 );
        
        assertEquals( new Integer( 1 ), report.getWeek1() );
        assertEquals( new Integer( 0 ), report.getWeek2() );
        assertEquals( new Integer( 1 ), report.getWeek3() );
        assertEquals( new Integer( 0 ), report.getWeek4() );
        assertEquals( "PLAN", report.getWeek1P() );
        assertEquals( "NO PLAN", report.getWeek2P() );
        assertEquals( "PLAN", report.getWeek3P() );
        assertEquals( "NO PLAN", report.getWeek4P() );
        assertEquals( "#060", report.getWeek1C() );
        assertEquals( "#900", report.getWeek2C() );
        assertEquals( "#060", report.getWeek3C() );
        assertEquals( "#900", report.getWeek4C() );
    }

    public void testPlansCompletedServiceNaN() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        PlansCompletedService service = new PlansCompletedService();
        service.setGetMeasure( getMeasure );
        
        // WEEK 1,2,3,4 Plans 
        getMeasure.addMeasure( Float.NaN );
        getMeasure.addMeasure( Float.NaN );
        getMeasure.addMeasure( Float.NaN );
        getMeasure.addMeasure( Float.NaN );
        
        PlansCompletedReport report = (PlansCompletedReport) service.calculate( 0 );
        
        assertEquals( new Integer( 0 ), report.getWeek1() );
        assertEquals( new Integer( 0 ), report.getWeek2() );
        assertEquals( new Integer( 0 ), report.getWeek3() );
        assertEquals( new Integer( 0 ), report.getWeek4() );
        assertEquals( "NO PLAN", report.getWeek1P() );
        assertEquals( "NO PLAN", report.getWeek2P() );
        assertEquals( "NO PLAN", report.getWeek3P() );
        assertEquals( "NO PLAN", report.getWeek4P() );
        assertEquals( "#900", report.getWeek1C() );
        assertEquals( "#900", report.getWeek2C() );
        assertEquals( "#900", report.getWeek3C() );
        assertEquals( "#900", report.getWeek4C() );
    }

}
