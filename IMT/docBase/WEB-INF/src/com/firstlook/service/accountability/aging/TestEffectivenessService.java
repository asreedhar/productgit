package com.firstlook.service.accountability.aging;

import junit.framework.TestCase;

import com.firstlook.persistence.accountability.MockGetMeasure;
import com.firstlook.service.accountability.reports.aging.EffectivenessReport;

public class TestEffectivenessService extends TestCase {
    
    public TestEffectivenessService(String name) {
        super(name);
    }
    
    public void testEffectivenessServiceNormal() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        EffectivenessService service = new EffectivenessService();
        service.setGetMeasure( getMeasure );

        // Spiff
        getMeasure.addMeasure( 20 );
        getMeasure.addMeasure( 2 );
        
        // Promote
        getMeasure.addMeasure( 20 );
        getMeasure.addMeasure( 10 );

        // Advert
        getMeasure.addMeasure( 2000 );
        getMeasure.addMeasure( 1500 );

        // Other
        getMeasure.addMeasure( 3 );
        getMeasure.addMeasure( 2 );

        // Reprice
        getMeasure.addMeasure( 100 );
        getMeasure.addMeasure( 12 );

        // Wholesaler
        getMeasure.addMeasure( 1e3f );
        getMeasure.addMeasure( 1e2f );

        // Auction
        getMeasure.addMeasure( 10 );
        getMeasure.addMeasure( 10 );
        
        EffectivenessReport report = (EffectivenessReport) service.calculate( 0 );
        
        assertEquals( "10% (2 of 20 units)", report.getSpiffStr() );
        assertEquals( "50% (10 of 20 units)", report.getPromotStr() );
        assertEquals( "75% (1500 of 2000 units)", report.getAdvertStr() );
        assertEquals( "67% (2 of 3 units)", report.getOtherStr() );
        assertEquals( "12% (12 of 100 units)", report.getRepriceStr() );
        assertEquals( "10% (100 of 1000 units)", report.getWholesalerStr() );
        assertEquals( "100% (10 of 10 units)", report.getAuctionStr() );
    }

    public void testEffectivenessServiceZeros() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        EffectivenessService service = new EffectivenessService();
        service.setGetMeasure( getMeasure );

        // Spiff
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        
        // Promot
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );

        // Advert
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );

        // Other
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );

        // Reprice
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );

        // Wholesaler
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );

        // Auction
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        
        EffectivenessReport report = (EffectivenessReport) service.calculate( 0 );
        
        assertEquals( "0% (0 of 0 units)", report.getRepriceStr() );
        assertEquals( "0% (0 of 0 units)", report.getAdvertStr() );
        assertEquals( "0% (0 of 0 units)", report.getPromotStr() );
        assertEquals( "0% (0 of 0 units)", report.getSpiffStr() );
        assertEquals( "0% (0 of 0 units)", report.getOtherStr() );
        assertEquals( "0% (0 of 0 units)", report.getWholesalerStr() );
        assertEquals( "0% (0 of 0 units)", report.getAuctionStr() );
    }

    public void testEffectivenessServiceNaN() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        EffectivenessService service = new EffectivenessService();
        service.setGetMeasure( getMeasure );

        // Spiff
        getMeasure.addMeasure( Float.NaN );
        getMeasure.addMeasure( Float.NaN );
        
        // Promot
        getMeasure.addMeasure( Float.NaN );
        getMeasure.addMeasure( Float.NaN );

        // Advert
        getMeasure.addMeasure( Float.NaN );
        getMeasure.addMeasure( Float.NaN );

        // Other
        getMeasure.addMeasure( Float.NaN );
        getMeasure.addMeasure( Float.NaN );

        // Reprice
        getMeasure.addMeasure( Float.NaN );
        getMeasure.addMeasure( Float.NaN );

        // Wholesaler
        getMeasure.addMeasure( Float.NaN );
        getMeasure.addMeasure( Float.NaN );

        // Auction
        getMeasure.addMeasure( Float.NaN );
        getMeasure.addMeasure( Float.NaN );
        
        EffectivenessReport report = (EffectivenessReport) service.calculate( 0 );
        
        assertEquals( "0% (0 of 0 units)", report.getRepriceStr() );
        assertEquals( "0% (0 of 0 units)", report.getAdvertStr() );
        assertEquals( "0% (0 of 0 units)", report.getPromotStr() );
        assertEquals( "0% (0 of 0 units)", report.getSpiffStr() );
        assertEquals( "0% (0 of 0 units)", report.getOtherStr() );
        assertEquals( "0% (0 of 0 units)", report.getWholesalerStr() );
        assertEquals( "0% (0 of 0 units)", report.getAuctionStr() );
    }

}
