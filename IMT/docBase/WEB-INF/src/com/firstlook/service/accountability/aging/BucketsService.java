package com.firstlook.service.accountability.aging;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import com.firstlook.service.accountability.EdgeScorecardService;
import com.firstlook.service.accountability.reports.EdgeScorecardReport;
import com.firstlook.service.accountability.reports.aging.BucketsReport;

public class BucketsService extends EdgeScorecardService {

    private Configuration config;

    // Global Configuration for BucketsService
    private int sixtyPlusThreshold;
    private int fifty59Threshold;
    private int forty49Threshold;
    private int thirty39Threshold;
    private int pThreshold;
    private String normalColor;
    private String exceededColor;

    public BucketsService() {
        super();
        loadConfig();
        initializeConstants();
    }

    private void loadConfig() {
        try {
            config = new PropertiesConfiguration( getClass().getResource("BucketService.properties"));
        } catch (ConfigurationException e) {
            throw new RuntimeException( "Configuration for BucketsService could not be loaded" );
        }
    }

    private void initializeConstants() {
        sixtyPlusThreshold = config.getInt("sixtyPlusThreshold");
        fifty59Threshold = config.getInt("fifty59Threshold");
        forty49Threshold = config.getInt("forty49Threshold");
        thirty39Threshold = config.getInt("thirty39Threshold");
        pThreshold = config.getInt("pThreshold");
        normalColor = config.getString("normalColor");
        exceededColor = config.getString("exceededColor");
    }

    public EdgeScorecardReport calculate(int dealerId) {

        float allInventoryUnits = getMeasure.assembleMeasure(dealerId,"Current","Aging Inventory","Aging Inventory Scorecard","All Inventory","Units");

        float watchListUnits = getMeasure.assembleMeasure(dealerId,"Current","Aging Inventory","Aging Inventory Scorecard","Watch List (High Risk Under 30 Days)","Units");
        String watchListWater = getMeasure.assembleFormattedMeasure(dealerId,"Current","Aging Inventory","Aging Inventory Scorecard","Watch List (High Risk Under 30 Days)","Under Water");
        float watchListWaterNF = getMeasure.assembleMeasure(dealerId,"Current","Aging Inventory","Aging Inventory Scorecard","Watch List (High Risk Under 30 Days)","Under Water");
        String watchListColor = (watchListWaterNF < 0) ? exceededColor : normalColor;

        float sixtyPlusUnits = getMeasure.assembleMeasure(dealerId,"Current","Aging Inventory","Aging Inventory Scorecard","Off Wholesale Cliff (60 Plus Days)","Units");
        String sixtyPlusWater = getMeasure.assembleFormattedMeasure(dealerId,"Current","Aging Inventory","Aging Inventory Scorecard","Off Wholesale Cliff (60 Plus Days)","Under Water");
        float sixtyPlusWaterNF = getMeasure.assembleMeasure(dealerId,"Current","Aging Inventory","Aging Inventory Scorecard","Off Wholesale Cliff (60 Plus Days)","Under Water");
        String sixtyColor = (sixtyPlusWaterNF < 0) ? exceededColor : normalColor;

        float fifty59Units = getMeasure.assembleMeasure(dealerId,"Current","Aging Inventory","Aging Inventory Scorecard","Off Retail Cliff (50-59 Days)","Units");
        String fifty59Water = getMeasure.assembleFormattedMeasure(dealerId,"Current","Aging Inventory","Aging Inventory Scorecard","Off Retail Cliff (50-59 Days)","Under Water");
        float fifty59WaterNF = getMeasure.assembleMeasure(dealerId,"Current","Aging Inventory","Aging Inventory Scorecard","Off Retail Cliff (50-59 Days)","Under Water");
        String fiftyColor = (fifty59WaterNF < 0) ? exceededColor : normalColor;

        float forty49Units = getMeasure.assembleMeasure(dealerId,"Current","Aging Inventory","Aging Inventory Scorecard","On Retail Cliff (40-49 Days)","Units");
        String forty49Water = getMeasure.assembleFormattedMeasure(dealerId,"Current","Aging Inventory","Aging Inventory Scorecard","On Retail Cliff (40-49 Days)","Under Water");
        float forty49WaterNF = getMeasure.assembleMeasure(dealerId,"Current","Aging Inventory","Aging Inventory Scorecard","On Retail Cliff (40-49 Days)","Under Water");
        String fortyColor = (forty49WaterNF < 0) ? exceededColor : normalColor;

        float thirty39Units = getMeasure.assembleMeasure(dealerId,"Current","Aging Inventory","Aging Inventory Scorecard","Approaching Retail Cliff (30-39 Days)","Units");
        String thirty39Water = getMeasure.assembleFormattedMeasure(dealerId,"Current","Aging Inventory","Aging Inventory Scorecard","Approaching Retail Cliff (30-39 Days)","Under Water");
        float thirty39WaterNF = getMeasure.assembleMeasure(dealerId,"Current","Aging Inventory","Aging Inventory Scorecard","Approaching Retail Cliff (30-39 Days)","Under Water");
        String thirtyColor = (thirty39WaterNF < 0) ? exceededColor : normalColor;

        int watchListP = percent( allInventoryUnits, watchListUnits );
        int sixtyPlusP = percent( allInventoryUnits, sixtyPlusUnits );
        int fifty59P = percent( allInventoryUnits, fifty59Units );
        int forty49P = percent( allInventoryUnits, forty49Units );
        int thirty39P = percent( allInventoryUnits, thirty39Units );

        // TODO: Get Color OUT of the service layer!
        String sixtyPColor = ((sixtyPlusP - sixtyPlusThreshold) > pThreshold) ? exceededColor : normalColor;
        String fiftyPColor = ((fifty59P - fifty59Threshold) > pThreshold) ? exceededColor : normalColor;
        String fortyPColor = ((forty49P - forty49Threshold) > pThreshold) ? exceededColor : normalColor;
        String thirtyPColor = ((thirty39P - thirty39Threshold) > pThreshold) ? exceededColor : normalColor;

        BucketsReport report = new BucketsReport();
        
        report.setWatchListColor( watchListColor );
        report.setSixtyColor( sixtyColor );
        report.setFiftyColor( fiftyColor );
        report.setFortyColor( fortyColor );
        report.setThirtyColor( thirtyColor );

        report.setSixtyPColor( sixtyPColor );
        report.setFiftyPColor( fiftyPColor );
        report.setFortyPColor( fortyPColor );
        report.setThirtyPColor( thirtyPColor );

        report.setWatchListP( new Integer( watchListP ) );
        report.setSixtyPlusP( new Integer( sixtyPlusP ) );
        report.setFifty59P( new Integer( fifty59P ) );
        report.setForty49P( new Integer( forty49P ) );
        report.setThirty39P( new Integer( thirty39P ) );

        report.setWatchListWater( watchListWater );
        report.setSixtyPlusWater( sixtyPlusWater );
        report.setFifty59Water( fifty59Water );
        report.setForty49Water( forty49Water );
        report.setThirty39Water( thirty39Water );

        return report;
    }
}
