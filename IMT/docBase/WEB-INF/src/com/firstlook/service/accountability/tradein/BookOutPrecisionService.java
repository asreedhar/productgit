package com.firstlook.service.accountability.tradein;

import com.firstlook.service.accountability.EdgeScorecardService;
import com.firstlook.service.accountability.reports.EdgeScorecardReport;
import com.firstlook.service.accountability.reports.tradein.BookOutPrecisionReport;

public class BookOutPrecisionService extends EdgeScorecardService {

    public BookOutPrecisionService() {
        super();
    }
    
    public EdgeScorecardReport calculate(int dealerId) {
        float thirtyDayOptionsRaw = getMeasure.assembleMeasure(dealerId, "30 Days","Trade-Ins","Book Out Precision","Analyzer Details","Adds/Deducts for Options");
        int thirtyDayOptions = Math.round( thirtyDayOptionsRaw * 100 );
        
        float thirtyDayMileageRaw = getMeasure.assembleMeasure(dealerId, "30 Days","Trade-Ins","Book Out Precision","Analyzer Details","Mileage Adjustment");
        int thirtyDayMileage = Math.round( thirtyDayMileageRaw * 100 );
        
        float thirtyDayAppraisalRaw = getMeasure.assembleMeasure(dealerId, "30 Days","Trade-Ins","Book Out Precision","Analyzer Details","Appraisal Value");
        int thirtyDayAppraisal = Math.round( thirtyDayAppraisalRaw * 100 );
        
        float thirtyDayReconRaw = getMeasure.assembleMeasure(dealerId, "30 Days","Trade-Ins","Book Out Precision","Analyzer Details","Estimated Reconditioning Cost");
        int thirtyDayRecon = Math.round( thirtyDayReconRaw * 100 );

        BookOutPrecisionReport report = new BookOutPrecisionReport();
        report.setThirtyDayOptions(new Integer( thirtyDayOptions ) );
        report.setThirtyDayMileage(new Integer( thirtyDayMileage ) );
        report.setThirtyDayAppraisal(new Integer( thirtyDayAppraisal ) );
        report.setThirtyDayRecon(new Integer( thirtyDayRecon ) );
        
        return report;
    }
}
