package com.firstlook.service.accountability.purchasing;

import junit.framework.TestCase;

import com.firstlook.persistence.accountability.MockGetMeasure;
import com.firstlook.service.accountability.reports.purchasing.RecsEvaldReport;

public class TestRecsEvaldService extends TestCase {
    
    public TestRecsEvaldService(String name) {
        super(name);
    }
    
    public void testRecsEvaldService() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        RecsEvaldService service = new RecsEvaldService();
        service.setGetMeasure( getMeasure );
        
        // WEEK 1,2,3,4 Plans 
        getMeasure.addMeasure( 1 );
        getMeasure.addMeasure( 1 );
        getMeasure.addMeasure( 1 );
        getMeasure.addMeasure( 1 );
        
        // WEEK 1,2,3,4 Opps and Buy 
        getMeasure.addMeasure( 12 );
        getMeasure.addMeasure( 1 );
        getMeasure.addMeasure( 5 );
        getMeasure.addMeasure( 7 );

        // WEEK 1,2,3,4 Opps
        getMeasure.addMeasure( 15 );
        getMeasure.addMeasure( 2 );
        getMeasure.addMeasure( 10 );
        getMeasure.addMeasure( 11 );

        RecsEvaldReport report = (RecsEvaldReport) service.calculate( 0 );
        
        //System.out.println( report );
        assertEquals( new Integer( 66 ), report.getEvalRatio() );
        assertEquals( new Integer( 25 ), report.getTotalNumerator() );
        assertEquals( new Integer( 38 ), report.getTotalDenominator() );
        assertEquals( new Integer( 3 ), report.getNumPlans() );
        assertEquals( new Integer( 34 ), report.getNotEvalRatio() );
    }

}
