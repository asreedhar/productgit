package com.firstlook.service.accountability.tradein;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import com.firstlook.service.accountability.EdgeScorecardService;
import com.firstlook.service.accountability.reports.EdgeScorecardReport;
import com.firstlook.service.accountability.reports.tradein.AnalyzeRateReport;

public class AnalyzeRateService extends EdgeScorecardService {

    private Configuration config;
    
    private int twelveWeekNonAnalyzedThreshold;
    private int twelveWeekAnalyzedDaysUnitsThreshold;
    private int twelveWeekNonAnalyzedDaysUnitsThreshold;
    
    private String sellsFasterTemplate;
    private String moreProfitableTemplate;
    private String bothTemplate;

    public AnalyzeRateService() {
        super();
        loadConfig();
        initializeConstants();
    }

    private void loadConfig() {
        try {
            config = new PropertiesConfiguration(getClass().getResource("AnalyzeRateService.properties"));
        } catch (ConfigurationException e) {
            throw new RuntimeException("Configuration for AnalyzedRateService could not be loaded");
        }
    }

    private void initializeConstants() {
        twelveWeekNonAnalyzedThreshold = config.getInt("twelveWeekNonAnalyzedThreshold");
        twelveWeekAnalyzedDaysUnitsThreshold = config.getInt("twelveWeekAnalyzedDaysUnitsThreshold");
        twelveWeekNonAnalyzedDaysUnitsThreshold = config.getInt("twelveWeekNonAnalyzedDaysUnitsThreshold");

        sellsFasterTemplate = config.getString("sellsFasterTemplate");
        moreProfitableTemplate = config.getString("moreProfitableTemplate");
        bothTemplate = config.getString("bothTemplate");
    }

    public EdgeScorecardReport calculate(int dealerId) {
        float sixWeekNonAnalyzedRaw = getMeasure.assembleMeasure(dealerId, "Trend", "Trade-Ins", "Are There Trade-Ins Not Analyzed?", "Trade-In Analysis", "% Trade-Ins Analyzed");
        float sixWeekNonAnalyzed = (100 - (sixWeekNonAnalyzedRaw * 100));
        float sixWeekNumAnalyzed = getMeasure.assembleMeasure(dealerId, "Trend", "Trade-Ins", "Are There Trade-Ins Not Analyzed?", "Trade-In Analysis", "Trade-Ins Analyzed");
        float sixWeekNumTotal = getMeasure.assembleMeasure(dealerId, "Trend", "Trade-Ins", "Are There Trade-Ins Not Analyzed?", "Trade-In Analysis", "Trade-Ins");
        float sixWeekNumNotAnalyzed = sixWeekNumTotal - sixWeekNumAnalyzed;

        float twelveWeekNonAnalyzedRaw = getMeasure.assembleMeasure(dealerId, "12Wk Avg", "Trade-Ins", "Are There Trade-Ins Not Analyzed?", "Trade-In Analysis", "% Trade-Ins Analyzed");
        float twelveWeekNonAnalyzed = (100 - (twelveWeekNonAnalyzedRaw * 100));

        float twelveWeekAnalyzedGross = getMeasure.assembleMeasure(dealerId, "12Wk Avg", "Trade-Ins", "Are There Trade-Ins Not Analyzed?", "Trade-In Analysis", "Analyzed Trade-Ins Gross Profit");
        float twelveWeekNonAnalyzedGross = getMeasure.assembleMeasure(dealerId, "12Wk Avg", "Trade-Ins", "Are There Trade-Ins Not Analyzed?", "Trade-In Analysis", "Trade-Ins Unanalyzed Gross Profit");

        float twelveWeekAnalyzedDays = getMeasure.assembleMeasure(dealerId, "12Wk Avg", "Trade-Ins", "Are There Trade-Ins Not Analyzed?", "Trade-In Analysis", "Analyzed Trade-Ins Days-To-Sale");
        float twelveWeekNonAnalyzedDays = getMeasure.assembleMeasure(dealerId, "12Wk Avg", "Trade-Ins", "Are There Trade-Ins Not Analyzed?", "Trade-In Analysis", "Trade-Ins Unanalyzed Days-To-Sale");

        boolean moreProfitable = ((twelveWeekAnalyzedGross - twelveWeekNonAnalyzedGross) > 100) ? true : false;
        float howMuchMoreProfitable = twelveWeekAnalyzedGross - twelveWeekNonAnalyzedGross;
       
        boolean sellsFaster = (((twelveWeekNonAnalyzedDays / twelveWeekAnalyzedDays) * 100) > 110) ? true : false;
        float howMuchFaster = twelveWeekNonAnalyzedDays - twelveWeekAnalyzedDays;

        float twelveWeekAnalyzedDaysUnits = getMeasure.assembleMeasure(dealerId, "12Wk Avg", "Trade-Ins", "Are There Trade-Ins Not Analyzed?", "Trade-In Analysis", "Analyzed Units (Sales Based)");
        float twelveWeekNonAnalyzedDaysUnits = getMeasure.assembleMeasure(dealerId, "12Wk Avg", "Trade-Ins", "Are There Trade-Ins Not Analyzed?", "Trade-In Analysis", "Unanalyzed Units (Sales Based)");

        String strAnalyzerPerformance = "";
        if ((twelveWeekNonAnalyzed < twelveWeekNonAnalyzedThreshold) && 
            (twelveWeekAnalyzedDaysUnits > twelveWeekAnalyzedDaysUnitsThreshold) && 
            (twelveWeekNonAnalyzedDaysUnits > twelveWeekNonAnalyzedDaysUnitsThreshold)) {
            strAnalyzerPerformance = createPerformanceString(moreProfitable, howMuchMoreProfitable, sellsFaster, howMuchFaster);
        }

        AnalyzeRateReport report = new AnalyzeRateReport();
        report.setMoreProfitable(new Boolean(moreProfitable));
        report.setSellsFaster(new Boolean(sellsFaster));
        report.setSixWeekNonAnalyzed(new Integer(Math.round(sixWeekNonAnalyzed)));
        report.setSixWeekNumNotAnalyzed(new Integer(Math.round(sixWeekNumNotAnalyzed)));
        report.setTwelveWeekNonAnalyzed(new Integer(Math.round(twelveWeekNonAnalyzed)));
        report.setStrAnalyzerPerformance(strAnalyzerPerformance);

        return report;
    }

    private String createPerformanceString(boolean moreProfitable, float howMuchMoreProfitable, boolean sellsFaster, float howMuchFaster) {
        Map params = new HashMap();
        params.put( "howMuchFaster", new Integer( Math.round( howMuchFaster ) ) );
        params.put( "howMuchMoreProfitable", new Integer( Math.round( howMuchMoreProfitable ) ) );
        
        String template = "";
        if (moreProfitable && sellsFaster) {
            template = bothTemplate;
        } else if (moreProfitable) {
            template = moreProfitableTemplate;
        } else if (sellsFaster) {
            template = sellsFasterTemplate;
        }
        return evaluateTemplate( params, template );
    }
}
