package com.firstlook.service.accountability.reports.aging;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.firstlook.service.accountability.reports.EdgeScorecardReport;

public class HighestRiskReport extends EdgeScorecardReport {

    private Integer redOver15Units;
    private String redOver15Inv;
    private String redOver15Water;
    private String redOutput;
    
    private Integer yellowOver30Units;
    private String yellowOver30Inv;
    private String yellowOver30Water;
    private String yellowOutput;
    
    public String getRedOutput() {
        return redOutput;
    }
    
    public void setRedOutput(String redOutput) {
        this.redOutput = redOutput;
    }
    
    public String getRedOver15Inv() {
        return redOver15Inv;
    }
    
    public void setRedOver15Inv(String redOver15Inv) {
        this.redOver15Inv = redOver15Inv;
    }
    
    public Integer getRedOver15Units() {
        return redOver15Units;
    }
    
    public void setRedOver15Units(Integer redOver15Units) {
        this.redOver15Units = redOver15Units;
    }
    
    public String getRedOver15Water() {
        return redOver15Water;
    }
    
    public void setRedOver15Water(String redOver15Water) {
        this.redOver15Water = redOver15Water;
    }
    
    public String getYellowOutput() {
        return yellowOutput;
    }
    
    public void setYellowOutput(String yellowOutput) {
        this.yellowOutput = yellowOutput;
    }
    
    public String getYellowOver30Inv() {
        return yellowOver30Inv;
    }
    
    public void setYellowOver30Inv(String yellowOver30Inv) {
        this.yellowOver30Inv = yellowOver30Inv;
    }
    
    public Integer getYellowOver30Units() {
        return yellowOver30Units;
    }
    
    public void setYellowOver30Units(Integer yellowOver30Units) {
        this.yellowOver30Units = yellowOver30Units;
    }
    
    public String getYellowOver30Water() {
        return yellowOver30Water;
    }
    
    public void setYellowOver30Water(String yellowOver30Water) {
        this.yellowOver30Water = yellowOver30Water;
    }
    
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}
