package com.firstlook.service.accountability.reports.tradein;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.firstlook.service.accountability.reports.EdgeScorecardReport;


public class ClosingRateReport extends EdgeScorecardReport {

    private Integer sixWeekRate;
    private Integer twelveWeekRate;
    
    public Integer getSixWeekRate() {
        return sixWeekRate;
    }
    
    public void setSixWeekRate(Integer sixWeekRate) {
        this.sixWeekRate = sixWeekRate;
    }
    
    public Integer getTwelveWeekRate() {
        return twelveWeekRate;
    }
    
    public void setTwelveWeekRate(Integer twelveWeekRate) {
        this.twelveWeekRate = twelveWeekRate;
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}
