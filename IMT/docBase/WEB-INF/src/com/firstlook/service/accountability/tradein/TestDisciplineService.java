package com.firstlook.service.accountability.tradein;

import junit.framework.TestCase;

import com.firstlook.persistence.accountability.MockGetMeasure;
import com.firstlook.service.accountability.reports.tradein.DisciplineReport;

// TODO: Test Exhaustively
public class TestDisciplineService extends TestCase {
    
    public TestDisciplineService(String name) {
        super(name);
    }
    
    public void testDaysSupplyService() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        DisciplineService service = new DisciplineService();
        service.setGetMeasure( getMeasure );

        getMeasure.addCommentMeasure( "TEST" );
        
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );

        DisciplineReport report = (DisciplineReport) service.calculate(0);
        
        assertEquals( "NA", report.getSixWeekBumpRate() );
        assertEquals( "NA", report.getTwelveWeekBumpRate() );
        assertEquals( "NA", report.getSixWeekAMinusBFormatted() );
        assertEquals( "NA", report.getTwelveWeekAMinusBFormatted() );
        assertEquals( "over ", report.getBackOrOver() ); 
        assertEquals( "TEST", report.getBookName() );
        assertEquals( "<i>Estimated Reconditioning Cost</i> not entered into the System. <i>Actual Reconditioning Cost</i> not available from the DMS.",
                      report.getSixWeekActMinusEstFormatted() );
        assertEquals( "NA", report.getSixWeekActMinusEstNum() );
        assertEquals( "NA", report.getTwelveWeekActMinusEstNum() );
        assertEquals( new Integer(0), report.getSixWeekAppraisalP() );

    }
}
