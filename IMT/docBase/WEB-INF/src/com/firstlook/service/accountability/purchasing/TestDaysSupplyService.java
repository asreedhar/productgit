package com.firstlook.service.accountability.purchasing;

import junit.framework.TestCase;

import com.firstlook.persistence.accountability.MockGetMeasure;
import com.firstlook.service.accountability.reports.purchasing.DaysSupplyReport;

public class TestDaysSupplyService extends TestCase {
    
    public TestDaysSupplyService(String name) {
        super(name);
    }
    
    public void testDaysSupplyService() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        DaysSupplyService service = new DaysSupplyService();
        service.setGetMeasure( getMeasure );
        
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );

        DaysSupplyReport report = (DaysSupplyReport) service.calculate(0);
        
        assertEquals( new Integer( 0 ), report.getSedanDS() );
        assertEquals( new Integer( 0 ), report.getSuvDS() );
        assertEquals( new Integer( 0 ), report.getTruckDS() );
        assertEquals( new Integer( 0 ), report.getCoupeDS() );
        assertEquals( new Integer( 0 ), report.getVanDS() );
        assertEquals( new Integer( 0 ), report.getWagonDS() );
        assertEquals( new Integer( 0 ), report.getConvertibleDS() );
    }

}
