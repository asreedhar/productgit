package com.firstlook.service.accountability.purchasing;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import com.firstlook.service.accountability.EdgeScorecardService;
import com.firstlook.service.accountability.reports.EdgeScorecardReport;
import com.firstlook.service.accountability.reports.purchasing.PurchasingDisciplineReport;

public class PurchasingDisciplineService extends EdgeScorecardService {

    private Configuration config;

    private String redDanger;
    private String redCaution;
    private String redNoWarn;
    
    public PurchasingDisciplineService() {
        super();
        loadConfig();
        initializeConstants();
    }
    
    private void loadConfig() {
        try {
            config = new PropertiesConfiguration( getClass().getResource("PurchasingDisciplineService.properties"));
        } catch (ConfigurationException e) {
            throw new RuntimeException( "Configuration for PlansCompletedService could not be loaded" );
        }
    }

    private void initializeConstants() {
        redDanger = config.getString("redDanger");
        redCaution = config.getString("redCaution");
        redNoWarn = config.getString("redNoWarn");
    }
    
    public EdgeScorecardReport calculate(int dealerId) {
       
        int redLights = (int) getMeasure.assembleMeasure(dealerId,"Four Weeks","Purchasing","Buying Discipline","Vehicles","Red Lights");
        String redSentence = createRedSentence(redLights);

        int understock = (int) getMeasure.assembleMeasure(dealerId,"Current","Purchasing","Buying Discipline ","Potential Missed Sales","Units Understocked");
        int overstock = (int) getMeasure.assembleMeasure(dealerId,"Current","Purchasing","Buying Discipline ","Potential Missed Sales","Units Overstocked");
        
        PurchasingDisciplineReport report = new PurchasingDisciplineReport();

        report.setRedLights( new Integer( redLights ) );
        report.setRedSentence( redSentence );
        report.setUnderstock( new Integer( understock ) );
        report.setOverstock( new Integer( overstock ) );

        return report;
    }

    String createRedSentence(int redLights) {
        Map param = new HashMap();
        param.put( "redLights", new Integer( redLights ) );
        String template = redCaution;
        if(redLights == 0) {
            template = redNoWarn;
        } else if(redLights > 3) {
            template = redDanger;
        }
        String redSentence = evaluateTemplate( param, template );
        return redSentence;
    }

    
}
