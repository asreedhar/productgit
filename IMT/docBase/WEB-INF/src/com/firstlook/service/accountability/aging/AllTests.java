package com.firstlook.service.accountability.aging;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AllTests extends TestCase
{

public AllTests( String arg0 )
{
    super(arg0);
}

public static Test suite()
{
    TestSuite suite = new TestSuite();

    suite.addTestSuite(TestAgingPlanningService.class);
    suite.addTestSuite(TestBucketsService.class);
    suite.addTestSuite(TestEffectivenessService.class);
    suite.addTestSuite(TestHighestRiskService.class);
    
    return suite;
}
}
