package com.firstlook.service.accountability.reports.tradein;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.firstlook.service.accountability.reports.EdgeScorecardReport;


public class DisciplineReport extends EdgeScorecardReport {

    private String sixWeekBumpRate;
    private String twelveWeekBumpRate;
    private String sixWeekAMinusBFormatted;
    private String twelveWeekAMinusBFormatted;
    private String backOrOver;
    private String bookName;
    
    private String sixWeekActMinusEstFormatted;
    private String sixWeekActMinusEstNum;
    private String twelveWeekActMinusEstNum;
    private Integer sixWeekAppraisalP;
    
    public String getBackOrOver() {
        return backOrOver;
    }
    
    public void setBackOrOver(String backOrOver) {
        this.backOrOver = backOrOver;
    }
    
    public String getBookName() {
        return bookName;
    }
    
    public void setBookName(String bookName) {
        this.bookName = bookName;
    }
    
    public String getSixWeekActMinusEstFormatted() {
        return sixWeekActMinusEstFormatted;
    }
    
    public void setSixWeekActMinusEstFormatted(String sixWeekActMinusEstFormatted) {
        this.sixWeekActMinusEstFormatted = sixWeekActMinusEstFormatted;
    }
    
    public String getSixWeekActMinusEstNum() {
        return sixWeekActMinusEstNum;
    }
    
    public void setSixWeekActMinusEstNum(String sixWeekActMinusEstNum) {
        this.sixWeekActMinusEstNum = sixWeekActMinusEstNum;
    }
    
    public String getSixWeekAMinusBFormatted() {
        return sixWeekAMinusBFormatted;
    }
    
    public void setSixWeekAMinusBFormatted(String sixWeekAMinusBFormatted) {
        this.sixWeekAMinusBFormatted = sixWeekAMinusBFormatted;
    }
    
    public Integer getSixWeekAppraisalP() {
        return sixWeekAppraisalP;
    }
    
    public void setSixWeekAppraisalP(Integer sixWeekAppraisalP) {
        this.sixWeekAppraisalP = sixWeekAppraisalP;
    }
    
    public String getSixWeekBumpRate() {
        return sixWeekBumpRate;
    }
    
    public void setSixWeekBumpRate(String sixWeekBumpRate) {
        this.sixWeekBumpRate = sixWeekBumpRate;
    }
    
    public String getTwelveWeekActMinusEstNum() {
        return twelveWeekActMinusEstNum;
    }
    
    public void setTwelveWeekActMinusEstNum(String twelveWeekActMinusEstNum) {
        this.twelveWeekActMinusEstNum = twelveWeekActMinusEstNum;
    }
    
    public String getTwelveWeekAMinusBFormatted() {
        return twelveWeekAMinusBFormatted;
    }
    
    public void setTwelveWeekAMinusBFormatted(String twelveWeekAMinusBFormatted) {
        this.twelveWeekAMinusBFormatted = twelveWeekAMinusBFormatted;
    }
    
    public String getTwelveWeekBumpRate() {
        return twelveWeekBumpRate;
    }
    
    public void setTwelveWeekBumpRate(String twelveWeekBumpRate) {
        this.twelveWeekBumpRate = twelveWeekBumpRate;
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    
    
}
