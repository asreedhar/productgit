package com.firstlook.service.accountability;

import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import com.firstlook.service.accountability.reports.EdgeScorecardReport;

public class TestEdgeScorecardService extends TestCase {
    
    public TestEdgeScorecardService(String name) {
        super( name );
    }
    
    public void testPercentNormal() throws Exception {
        TestingEdgeScorecardService service = new TestingEdgeScorecardService();
        
        assertEquals( 20, service.percent( 100f, 20f ) );
        assertEquals( 10, service.percent( 1200f, 120f ) );
        assertEquals( 50, service.percent( 40f, 20f ) );
        assertEquals( 1, service.percent( 300f, 3f ) );
        assertEquals( 120, service.percent( 100f, 120f ) );
        assertEquals( 200, service.percent( 10f, 20f ) );
        assertEquals( 67, service.percent( 3f, 2f ) );
    }

    public void testPercentZero() throws Exception {
        TestingEdgeScorecardService service = new TestingEdgeScorecardService();
        
        assertEquals( 0, service.percent( 0f, 0f ) );
    }

    public void testPercentNaN() throws Exception {
        TestingEdgeScorecardService service = new TestingEdgeScorecardService();
        
        assertEquals( 0, service.percent( Float.NaN, Float.NaN ) );
    }

    public void testPercentInfinity() throws Exception {
        TestingEdgeScorecardService service = new TestingEdgeScorecardService();
        
        assertEquals( 0, service.percent( Float.POSITIVE_INFINITY, 0 ) );
        assertEquals( 0, service.percent( Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY ) );
        assertEquals( 0, service.percent( Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY ) );
    }

    public void testPercentNegatives() throws Exception {
        TestingEdgeScorecardService service = new TestingEdgeScorecardService();
        
        assertEquals( 0, service.percent( -100f, -20f ) );
        assertEquals( 0, service.percent( -100f, 20f ) );
    }
    
    public void testEvaluateTemplate() throws Exception {
        TestingEdgeScorecardService service = new TestingEdgeScorecardService();

        assertEquals( "This is a test", service.evaluateTemplate( new HashMap(), "This is a test" ) );
        Map map = new HashMap();
        map.put( "color", "blue" );
        assertEquals( "This is a test blue", service.evaluateTemplate( map, "This is a test ${color}" ) );
        
    }

    public class TestingEdgeScorecardService extends EdgeScorecardService {
        
        public EdgeScorecardReport calculate(int dealerId) {
            return null;
        }
        
    }
}
