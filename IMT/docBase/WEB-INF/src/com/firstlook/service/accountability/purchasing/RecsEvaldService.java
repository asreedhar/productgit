package com.firstlook.service.accountability.purchasing;

import com.firstlook.service.accountability.EdgeScorecardService;
import com.firstlook.service.accountability.reports.EdgeScorecardReport;
import com.firstlook.service.accountability.reports.purchasing.RecsEvaldReport;

public class RecsEvaldService extends EdgeScorecardService {

    public RecsEvaldService() {
        super();
    }
    
    public EdgeScorecardReport calculate(int dealerId) {
       
        float week1 = getMeasure.assembleMeasure(dealerId,"Last Week (Plan Base Date)","Purchasing","Buying Plans Completed","Plan Completed?","Cost Pofloat Plan Completed");
        float week2 = getMeasure.assembleMeasure(dealerId,"Two Weeks Ago","Purchasing","Buying Plans Completed","Plan Completed?","Cost Pofloat Plan Completed");
        float week3 = getMeasure.assembleMeasure(dealerId,"Three Weeks Ago","Purchasing","Buying Plans Completed","Plan Completed?","Cost Pofloat Plan Completed");
        float week4 = getMeasure.assembleMeasure(dealerId,"Four Weeks Ago","Purchasing","Buying Plans Completed","Plan Completed?","Cost Pofloat Plan Completed");

        float modelsWithOppsAndBuyWk1 = getMeasure.assembleMeasure(dealerId,"Week One of Four","Purchasing","Buying Plan Recommendations - Unit Cost","Power Zone & Winners","Models w/Understock Opportunities & Plans");
        float modelsWithOppsAndBuyWk2 = getMeasure.assembleMeasure(dealerId,"Week Two of Four","Purchasing","Buying Plan Recommendations - Unit Cost","Power Zone & Winners","Models w/Understock Opportunities & Plans");
        float modelsWithOppsAndBuyWk3 = getMeasure.assembleMeasure(dealerId,"Week Three of Four","Purchasing","Buying Plan Recommendations - Unit Cost","Power Zone & Winners","Models w/Understock Opportunities & Plans");
        float modelsWithOppsAndBuyWk4 = getMeasure.assembleMeasure(dealerId,"Week Four of Four","Purchasing","Buying Plan Recommendations - Unit Cost","Power Zone & Winners","Models w/Understock Opportunities & Plans");

        /*  Denominators Second */
        float modelsWithOppsWk1 = getMeasure.assembleMeasure(dealerId,"Week One of Four","Purchasing","Buying Plan Recommendations - Unit Cost","Power Zone & Winners","Models w/Understock Opportunities");
        float modelsWithOppsWk2 = getMeasure.assembleMeasure(dealerId,"Week Two of Four","Purchasing","Buying Plan Recommendations - Unit Cost","Power Zone & Winners","Models w/Understock Opportunities");
        float modelsWithOppsWk3 = getMeasure.assembleMeasure(dealerId,"Week Three of Four","Purchasing","Buying Plan Recommendations - Unit Cost","Power Zone & Winners","Models w/Understock Opportunities");
        float modelsWithOppsWk4 = getMeasure.assembleMeasure(dealerId,"Week Four of Four","Purchasing","Buying Plan Recommendations - Unit Cost","Power Zone & Winners","Models w/Understock Opportunities");

        /*  If the numeraotr is 0, there is no plan so we set the denominator to 0 as well, otherwise to itself*/
        int wk1Denom = (week4 == 0) ? 0 : Math.round( modelsWithOppsWk1 );
        int wk2Denom = (week3 == 0) ? 0 : Math.round( modelsWithOppsWk2 );
        int wk3Denom = (week2 == 0) ? 0 : Math.round( modelsWithOppsWk3 );
        int wk4Denom = (week1 == 0) ? 0 : Math.round( modelsWithOppsWk4 );

        int numPlans = 0;
        int[] denoms = new int[] { wk1Denom, wk2Denom, wk3Denom, wk4Denom };
        for(int i=1;i<denoms.length;i++) {
            if(denoms[i] != 0) {
                numPlans += 1;
            }
        }

        float totalNumerator = modelsWithOppsAndBuyWk1 + modelsWithOppsAndBuyWk2 + modelsWithOppsAndBuyWk3 + modelsWithOppsAndBuyWk4;
        float totalDenominator = wk1Denom + wk2Denom + wk3Denom + wk4Denom;

        int evalRatio = percent( totalDenominator, totalNumerator );
        int notEvalRatio = 100 - evalRatio;

        RecsEvaldReport report = new RecsEvaldReport();
        report.setEvalRatio( new Integer( evalRatio ) );
        report.setTotalNumerator( new Integer( Math.round( totalNumerator ) ) );
        report.setTotalDenominator( new Integer( Math.round( totalDenominator ) ) );
        report.setNumPlans( new Integer( numPlans ) );
        report.setNotEvalRatio( new Integer( notEvalRatio ) );

        return report;
    }

    
}
