package com.firstlook.service.accountability.reports.purchasing;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.firstlook.service.accountability.reports.EdgeScorecardReport;

public class PrecisionGaugeReport extends EdgeScorecardReport {

    private Integer goodRatio;
    private Integer badRatio;
    
    public Integer getBadRatio() {
        return badRatio;
    }
    
    public void setBadRatio(Integer badRatio) {
        this.badRatio = badRatio;
    }
    
    public Integer getGoodRatio() {
        return goodRatio;
    }
    
    public void setGoodRatio(Integer goodRatio) {
        this.goodRatio = goodRatio;
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
