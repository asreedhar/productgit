package com.firstlook.service.accountability.reports.purchasing;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.firstlook.service.accountability.reports.EdgeScorecardReport;

public class CurrentInventoryReport extends EdgeScorecardReport {

    private Integer purchInvUnits;
    private String inventoryDollars;
    private Integer overallDaysSupply;
    
    public String getInventoryDollars() {
        return inventoryDollars;
    }
    
    public void setInventoryDollars(String inventoryDollars) {
        this.inventoryDollars = inventoryDollars;
    }
    
    public Integer getOverallDaysSupply() {
        return overallDaysSupply;
    }
    
    public void setOverallDaysSupply(Integer overallDaysSupply) {
        this.overallDaysSupply = overallDaysSupply;
    }
    
    public Integer getPurchInvUnits() {
        return purchInvUnits;
    }
    
    public void setPurchInvUnits(Integer purchInvUnits) {
        this.purchInvUnits = purchInvUnits;
    }
    
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}
