package com.firstlook.service.accountability.tradein;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AllTests extends TestCase
{

public AllTests( String arg0 )
{
    super(arg0);
}

public static Test suite()
{
    TestSuite suite = new TestSuite();

    suite.addTestSuite(TestBookOutPrecisionService.class);
    suite.addTestSuite(TestClosingRateService.class);
    suite.addTestSuite(TestDisciplineService.class);
    suite.addTestSuite(TestFlipAGPService.class);
    
    return suite;
}
}
