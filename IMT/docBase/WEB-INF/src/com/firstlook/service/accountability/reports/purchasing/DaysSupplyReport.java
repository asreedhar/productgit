package com.firstlook.service.accountability.reports.purchasing;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.firstlook.service.accountability.reports.EdgeScorecardReport;

public class DaysSupplyReport extends EdgeScorecardReport {

    private Integer sedanDS;
    private Integer suvDS;
    private Integer truckDS;
    private Integer coupeDS;
    private Integer vanDS;
    private Integer wagonDS;
    private Integer convertibleDS;
    public Integer getConvertibleDS() {
        return convertibleDS;
    }
    
    public void setConvertibleDS(Integer convertibleDS) {
        this.convertibleDS = convertibleDS;
    }
    
    public Integer getCoupeDS() {
        return coupeDS;
    }
    
    public void setCoupeDS(Integer coupeDS) {
        this.coupeDS = coupeDS;
    }
    
    public Integer getSedanDS() {
        return sedanDS;
    }
    
    public void setSedanDS(Integer sedanDS) {
        this.sedanDS = sedanDS;
    }
    
    public Integer getSuvDS() {
        return suvDS;
    }
    
    public void setSuvDS(Integer suvDS) {
        this.suvDS = suvDS;
    }
    
    public Integer getTruckDS() {
        return truckDS;
    }
    
    public void setTruckDS(Integer truckDS) {
        this.truckDS = truckDS;
    }
    
    public Integer getVanDS() {
        return vanDS;
    }
    
    public void setVanDS(Integer vanDS) {
        this.vanDS = vanDS;
    }
    
    public Integer getWagonDS() {
        return wagonDS;
    }
    
    public void setWagonDS(Integer wagonDS) {
        this.wagonDS = wagonDS;
    }
    
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    } 
}
