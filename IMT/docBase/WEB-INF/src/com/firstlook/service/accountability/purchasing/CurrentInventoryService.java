package com.firstlook.service.accountability.purchasing;

import com.firstlook.service.accountability.EdgeScorecardService;
import com.firstlook.service.accountability.reports.EdgeScorecardReport;
import com.firstlook.service.accountability.reports.purchasing.CurrentInventoryReport;

public class CurrentInventoryService extends EdgeScorecardService {

    public CurrentInventoryService() {
        super();
    }
    
    public EdgeScorecardReport calculate(int dealerId) {
       
        int purchInvUnits = (int) getMeasure.assembleMeasure(dealerId,"12Wk Avg","Purchasing","Current Inventory","Vehicles in Inventory","Units");
        String inventoryDollars = getMeasure.assembleFormattedMeasure(dealerId,"12Wk Avg","Purchasing","Current Inventory","Vehicles in Inventory","Unit Cost Sum");
        int overallDaysSupply = (int) getMeasure.assembleMeasure(dealerId,"Composite Basis Period","Purchasing","Current Inventory","Vehicles in Inventory","Days Supply");
        overallDaysSupply = Math.round(overallDaysSupply);

        CurrentInventoryReport report = new CurrentInventoryReport();
        report.setPurchInvUnits( new Integer( purchInvUnits ) );
        report.setInventoryDollars( inventoryDollars );
        report.setOverallDaysSupply( new Integer( overallDaysSupply ) );

        return report;
    }

    
}
