package com.firstlook.service.accountability.purchasing;

import junit.framework.TestCase;

import com.firstlook.persistence.accountability.MockGetMeasure;
import com.firstlook.service.accountability.reports.purchasing.PrecisionGaugeReport;

public class TestPrecisionGaugeService extends TestCase {
    
    public TestPrecisionGaugeService(String name) {
        super(name);
    }
    
    public void testDaysSupplyService() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        PrecisionGaugeService service = new PrecisionGaugeService();
        service.setGetMeasure( getMeasure );
        
        // pz detail, total
        getMeasure.addMeasure( 1.0f );
        getMeasure.addMeasure( 4.0f );

        // z detail, total
        getMeasure.addMeasure( 1.0f );
        getMeasure.addMeasure( 4.0f );

        PrecisionGaugeReport report = (PrecisionGaugeReport) service.calculate(0);
        
        assertEquals( new Integer( 25 ), report.getGoodRatio() );
        assertEquals( new Integer( 75 ), report.getBadRatio() );
    }
    
    public void testDaysSupplyServiceZero() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        PrecisionGaugeService service = new PrecisionGaugeService();
        service.setGetMeasure( getMeasure );
        
        // pz detail, total
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );

        // z detail, total
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );

        PrecisionGaugeReport report = (PrecisionGaugeReport) service.calculate(0);
        
        assertEquals( new Integer( 0 ), report.getGoodRatio() );
        assertEquals( new Integer( 100 ), report.getBadRatio() );
    }

    public void testDaysSupplyServiceUnrealistic() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        PrecisionGaugeService service = new PrecisionGaugeService();
        service.setGetMeasure( getMeasure );
        
        // pz detail, total
        getMeasure.addMeasure( 5.0f );
        getMeasure.addMeasure( 6.0f );

        // z detail, total
        getMeasure.addMeasure( 10.0f );
        getMeasure.addMeasure( 4.0f );

        PrecisionGaugeReport report = (PrecisionGaugeReport) service.calculate(0);
        
        assertEquals( new Integer( 150 ), report.getGoodRatio() );
        assertEquals( new Integer( -50 ), report.getBadRatio() );
    }

}
