package com.firstlook.service.accountability.purchasing;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AllTests extends TestCase
{

public AllTests( String arg0 )
{
    super(arg0);
}

public static Test suite()
{
    TestSuite suite = new TestSuite();

    suite.addTestSuite(TestCurrentInventoryService.class);
    suite.addTestSuite(TestDaysSupplyService.class);
    suite.addTestSuite(TestPlansCompletedService.class);
    suite.addTestSuite(TestPrecisionGaugeService.class);
    suite.addTestSuite(TestPurchasingDisciplineService.class);
    suite.addTestSuite(TestRecsEvaldService.class);
    
    return suite;
}
}
