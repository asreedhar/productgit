package com.firstlook.service.accountability.tradein;

import com.firstlook.service.accountability.EdgeScorecardService;
import com.firstlook.service.accountability.reports.EdgeScorecardReport;
import com.firstlook.service.accountability.reports.tradein.DisciplineReport;

// TODO: This logic could use some heavy refactoring
public class DisciplineService extends EdgeScorecardService {

    public DisciplineService() {
        super();
    }
    
    public EdgeScorecardReport calculate(int dealerId) {

        String bookName = getMeasure.assembleCommentMeasure(dealerId, "Current","Trade-Ins","Appraisal Discipline","Analysis","Book Name");

        // Six Week
        float sixWeekAppraisalRaw = getMeasure.assembleMeasure(dealerId, "Trend","Trade-Ins","Book Out Precision","Analyzer Details","Appraisal Value");
        int sixWeekAppraisalP = Math.round(sixWeekAppraisalRaw * 100);

        float sixWeekBumpRateRaw = getMeasure.assembleMeasure(dealerId,"Trend","Trade-Ins","Appraisal Discipline","Appraisals Bumped","Appraisal Bump Percentage");
        String sixWeekBumpRate = "";
        if(sixWeekAppraisalP > 3) {
            sixWeekBumpRate = Math.round(sixWeekBumpRateRaw * 100) + "";
            sixWeekBumpRate += "%";
        } else {
            sixWeekBumpRate += "NA";
        }

        float sixWeekAppraisal = getMeasure.assembleMeasure(dealerId,"Trend","Trade-Ins","Appraisal Discipline","Analysis","Average Appraisal");
        float sixWeekBook = getMeasure.assembleMeasure(dealerId,"Trend","Trade-Ins","Appraisal Discipline","Analysis","Book-Out Average");
        int sixWeekBookMinusAppraisalNum = Math.round(sixWeekBook - sixWeekAppraisal);
        String sixWeekBookMinusAppraisalString = sixWeekBookMinusAppraisalNum + "";

        if(sixWeekBookMinusAppraisalString.indexOf("-") != -1) {
            String[] tempStrArray = sixWeekBookMinusAppraisalString.split("-");
            sixWeekBookMinusAppraisalString = tempStrArray[1] + "";
        }

        String sixWeekBookMinusAppraisal = "";
        String sixWeekAMinusBFormatted = "";
        String backOrOver = (sixWeekBookMinusAppraisalNum > 0) ? "back of " : "over ";

        if(sixWeekAppraisalP > 3) {

            if(sixWeekBookMinusAppraisalString.length()> 3) {
                for(int i=0;i<sixWeekBookMinusAppraisalString.length();i++) {
                    if(i==0) {
                        for(int j=0;j<sixWeekBookMinusAppraisalString.length()% 3;j++) {
                            sixWeekBookMinusAppraisal += sixWeekBookMinusAppraisalString.charAt(j);
                        }
                    sixWeekBookMinusAppraisal += ",";
                    }
                    if(i!=0) {
                        sixWeekBookMinusAppraisal += sixWeekBookMinusAppraisalString.charAt(i);
                    }
                }
            } else {
                sixWeekBookMinusAppraisal = sixWeekBookMinusAppraisalString;
            }


            if(backOrOver.equals("over ")) {
                sixWeekAMinusBFormatted += "$" + sixWeekBookMinusAppraisal;
            } else {
                sixWeekAMinusBFormatted += "($" + sixWeekBookMinusAppraisal + ")";
            }


        } else {
            sixWeekAMinusBFormatted += "NA";
        }

        int sixWeekReconAandIUnits = (int) getMeasure.assembleMeasure(dealerId,"Trend","Trade-Ins","Appraisal Discipline","Reconditioning Cost","Appraisal & Inventory Units");
        int sixWeekReconActualUnits = (int) getMeasure.assembleMeasure(dealerId,"Trend","Trade-Ins","Appraisal Discipline","Reconditioning Cost","Inventory Reconditioning Cost Units");
        int sixWeekReconEstimateUnits = (int) getMeasure.assembleMeasure(dealerId,"Trend","Trade-Ins","Appraisal Discipline","Reconditioning Cost","Appraisal Reconditioning Notes Units");
        int sixWeekReconActual = (int) getMeasure.assembleMeasure(dealerId,"Trend","Trade-Ins","Appraisal Discipline","Reconditioning Cost","Inventory Reconditioning Cost Average");
        int sixWeekReconEstimate = (int) getMeasure.assembleMeasure(dealerId,"Trend","Trade-Ins","Appraisal Discipline","Reconditioning Cost","Appraisal Reconditioning Notes Average");

        int sixWeekPercentEstimated = 0;
        if( sixWeekReconAandIUnits != 0 ) {
            sixWeekPercentEstimated = sixWeekReconEstimateUnits / sixWeekReconAandIUnits * 100;
        }

        int sixWeekPercentActual = 0;
        if( sixWeekReconEstimateUnits != 0 ) {
            sixWeekPercentActual = sixWeekReconActualUnits / sixWeekReconEstimateUnits * 100;
        }

        // Twelve Week
        float twelveWeekAppraisalRaw = getMeasure.assembleMeasure(dealerId,"12Wk Avg","Trade-Ins","Book Out Precision","Analyzer Details","Appraisal Value");
        int twelveWeekAppraisalP = Math.round(twelveWeekAppraisalRaw * 100);

        float twelveWeekBumpRateRaw = getMeasure.assembleMeasure(dealerId,"12Wk Avg","Trade-Ins","Appraisal Discipline","Appraisals Bumped","Appraisal Bump Percentage");
        String twelveWeekBumpRate = "";
        if(twelveWeekAppraisalP > 3) {
            twelveWeekBumpRate = Math.round(twelveWeekBumpRateRaw * 100) + "";
            twelveWeekBumpRate += "%";
        } else {
            twelveWeekBumpRate += "NA";
        }

        float twelveWeekAppraisal = getMeasure.assembleMeasure(dealerId,"12Wk Avg","Trade-Ins","Appraisal Discipline","Analysis","Average Appraisal");
        float twelveWeekBook = getMeasure.assembleMeasure(dealerId,"12Wk Avg","Trade-Ins","Appraisal Discipline","Analysis","Book-Out Average");


        int twelveWeekBookMinusAppraisalNum = Math.round(twelveWeekBook - twelveWeekAppraisal);
        String twelveWeekBookMinusAppraisalString = twelveWeekBookMinusAppraisalNum + "";
        String twelveWeekBookMinusAppraisal = "";
        String twelveWeekAMinusBFormatted = "";
        String backOrOverTwelve = (twelveWeekBookMinusAppraisalNum > 0) ? "back of" : "over";

        if(twelveWeekBookMinusAppraisalString.indexOf("-") != -1) {
            String[] tempStrArray = twelveWeekBookMinusAppraisalString.split("-");
            twelveWeekBookMinusAppraisalString = tempStrArray[1] + "";
        }

        if(twelveWeekAppraisalP > 3) {

            if(twelveWeekBookMinusAppraisalString.length()> 3) {
                for(int i=0;i<twelveWeekBookMinusAppraisalString.length();i++) {
                    if(i==0) {
                        for(int j=0;j<twelveWeekBookMinusAppraisalString.length()% 3;j++) {
                            twelveWeekBookMinusAppraisal += twelveWeekBookMinusAppraisalString.charAt(j);
                        }
                    twelveWeekBookMinusAppraisal += ",";
                    }
                    if(i!=0) {
                        twelveWeekBookMinusAppraisal += twelveWeekBookMinusAppraisalString.charAt(i);
                    }
                }
            } else {
                twelveWeekBookMinusAppraisal = twelveWeekBookMinusAppraisalString;
            }

            if(backOrOverTwelve == "over") {
                twelveWeekAMinusBFormatted += "$" + twelveWeekBookMinusAppraisal;
            } else {
                twelveWeekAMinusBFormatted += "($" + twelveWeekBookMinusAppraisal + ")";
            }

        } else {
            twelveWeekAMinusBFormatted += "NA";
        }

        int twelveWeekReconAandIUnits = (int) getMeasure.assembleMeasure(dealerId,"12Wk Avg","Trade-Ins","Appraisal Discipline","Reconditioning Cost","Appraisal & Inventory Units");
        int twelveWeekReconActualUnits = (int) getMeasure.assembleMeasure(dealerId,"12Wk Avg","Trade-Ins","Appraisal Discipline","Reconditioning Cost","Inventory Reconditioning Cost Units");
        int twelveWeekReconEstimateUnits = (int) getMeasure.assembleMeasure(dealerId,"12Wk Avg","Trade-Ins","Appraisal Discipline","Reconditioning Cost","Appraisal Reconditioning Notes Units");
        int twelveWeekReconActual = (int) getMeasure.assembleMeasure(dealerId,"12Wk Avg","Trade-Ins","Appraisal Discipline","Reconditioning Cost","Inventory Reconditioning Cost Average");
        int twelveWeekReconEstimate = (int) getMeasure.assembleMeasure(dealerId,"12Wk Avg","Trade-Ins","Appraisal Discipline","Reconditioning Cost","Appraisal Reconditioning Notes Average");

        int twelveWeekPercentEstimated = 0;
        if( twelveWeekReconAandIUnits != 0 ) {
            twelveWeekPercentEstimated = twelveWeekReconEstimateUnits / twelveWeekReconAandIUnits * 100;
        }
        int twelveWeekPercentActual = 0;
        if( twelveWeekReconEstimateUnits != 0 ) {
            twelveWeekPercentActual = twelveWeekReconActualUnits / twelveWeekReconEstimateUnits * 100;
        }

        DisciplineReport report = new DisciplineReport();
        report.setSixWeekBumpRate(sixWeekBumpRate);
        report.setTwelveWeekBumpRate(twelveWeekBumpRate);
        report.setSixWeekAMinusBFormatted( sixWeekAMinusBFormatted );
        report.setTwelveWeekAMinusBFormatted( twelveWeekAMinusBFormatted );
        report.setBackOrOver( backOrOver );
        report.setBookName( bookName );
        
        report.setSixWeekActMinusEstFormatted( getSixWeekActMinusEstFormatted("sentence", sixWeekPercentEstimated, sixWeekReconEstimateUnits, sixWeekPercentActual, sixWeekReconActualUnits, sixWeekReconActual, sixWeekReconEstimate ) );
        
        report.setSixWeekActMinusEstNum( getSixWeekActMinusEstFormatted("num", sixWeekPercentEstimated, sixWeekReconEstimateUnits, sixWeekPercentActual, sixWeekReconActualUnits, sixWeekReconActual, sixWeekReconEstimate ) );
        report.setTwelveWeekActMinusEstNum( getTwelveWeekActMinusEstFormatted("num", twelveWeekPercentEstimated, twelveWeekReconEstimateUnits, twelveWeekPercentActual, twelveWeekReconActualUnits, twelveWeekReconActual, twelveWeekReconEstimate ) );
        
        report.setSixWeekAppraisalP( new Integer(sixWeekAppraisalP) );
        
        return report;
    }
    
    private String getTwelveWeekActMinusEstFormatted(String numOrSentence, int twelveWeekPercentEstimated, int twelveWeekReconEstimateUnits, int twelveWeekPercentActual, int twelveWeekReconActualUnits, int twelveWeekReconActual, int twelveWeekReconEstimate) {
        String twelveWeekActMinusEstFormattedNum = "";
        String strOutput = "";

        if(twelveWeekPercentEstimated > 10 && twelveWeekReconEstimateUnits > 10 && twelveWeekPercentActual > 25 && twelveWeekReconActualUnits > 5) {
                twelveWeekActMinusEstFormattedNum = getTwelveWeekActMinusEstFormattedNum(twelveWeekReconActual, twelveWeekReconEstimate);
                int twelveWeekActualMinusEstReconNum = Math.round(twelveWeekReconActual - twelveWeekReconEstimate);
                String lessOrMore = (twelveWeekActualMinusEstReconNum > 0) ? "less" : "more";  /*<*/
                strOutput = "<i>Est. Reconditioning Cost</i> is " + twelveWeekActMinusEstFormattedNum + " " + lessOrMore + " than Actual.";

        } else if(twelveWeekPercentEstimated > 10 && twelveWeekReconEstimateUnits > 10) {
            twelveWeekActMinusEstFormattedNum = "NA";
            strOutput = "<i>Actual Reconditioning Cost</i> not available from the DMS.";
        } else {
            twelveWeekActMinusEstFormattedNum = "NA";
            strOutput = "<i>Estimated Reconditioning Cost</i> not entered into the System. <i>Actual Reconditioning Cost</i> not available from the DMS.";
        }

        if(numOrSentence.equals("num")) {
            return twelveWeekActMinusEstFormattedNum;
        } else {
            return strOutput;
        }
    }

    private String getTwelveWeekActMinusEstFormattedNum(int twelveWeekReconActual, int twelveWeekReconEstimate) {
        int twelveWeekActualMinusEstReconAbsNum = Math.abs(Math.round(twelveWeekReconActual - twelveWeekReconEstimate));
        int twelveWeekActualMinusEstReconNum = Math.round(twelveWeekReconActual - twelveWeekReconEstimate);
        String twelveWeekActualMinusEstReconString = twelveWeekActualMinusEstReconAbsNum + "";
        String twelveWeekActualMinusEstRecon = "";
        String twelveWeekActMinusEstFormatted = "";

        if(twelveWeekActualMinusEstReconString.length() > 3) {
            for(int i=0;i<twelveWeekActualMinusEstReconString.length();i++) {
                if(i==0) {
                    for(int j=0;j<twelveWeekActualMinusEstReconString.length() % 3;j++) {
                        twelveWeekActualMinusEstRecon += twelveWeekActualMinusEstReconString.charAt(j);
                    }
                twelveWeekActualMinusEstRecon += ",";
                }
                if(i!=0) {
                    twelveWeekActualMinusEstRecon += twelveWeekActualMinusEstReconString.charAt(i);
                }
            }
        } else {
            twelveWeekActualMinusEstRecon = twelveWeekActualMinusEstReconString;
        }
        String actOrEst = (twelveWeekActualMinusEstReconNum > 0) ? "less" : "more";

        if(actOrEst.equals("less")) {
            twelveWeekActMinusEstFormatted += "($" + twelveWeekActualMinusEstRecon + ")";
        } else {
            twelveWeekActMinusEstFormatted += "$" + twelveWeekActualMinusEstRecon;
        }
        return twelveWeekActMinusEstFormatted;
    }
    
    private String getSixWeekActMinusEstFormatted(String numOrSentence, int sixWeekPercentEstimated, int sixWeekReconEstimateUnits, int sixWeekPercentActual, int sixWeekReconActualUnits, int sixWeekReconActual, int sixWeekReconEstimate) {
        String sixWeekActMinusEstFormattedNum = "";
        String strOutput = "";

        if(sixWeekPercentEstimated > 10 && sixWeekReconEstimateUnits > 10 && sixWeekPercentActual > 25 && sixWeekReconActualUnits > 5) {
                sixWeekActMinusEstFormattedNum = getSixWeekActMinusEstFormattedNum(sixWeekReconActual, sixWeekReconEstimateUnits);
                int sixWeekActualMinusEstReconNum = Math.round(sixWeekReconActual - sixWeekReconEstimate);
                String lessOrMore = (sixWeekActualMinusEstReconNum > 0) ? "less" : "more";
                strOutput = "<i>Est. Reconditioning Cost</i> is " + sixWeekActMinusEstFormattedNum + " " + lessOrMore + " than Actual.";

        } else if(sixWeekPercentEstimated > 10 && sixWeekReconEstimateUnits > 10) {
            sixWeekActMinusEstFormattedNum = "NA";
            strOutput = "<i>Actual Reconditioning Cost</i> not available from the DMS.";
        } else {
            sixWeekActMinusEstFormattedNum = "NA";
            strOutput = "<i>Estimated Reconditioning Cost</i> not entered into the System. <i>Actual Reconditioning Cost</i> not available from the DMS.";
        }

        if(numOrSentence.equals("num")) {
            return sixWeekActMinusEstFormattedNum;
        } else {
            return strOutput;
        }
    }

    private String getSixWeekActMinusEstFormattedNum(int sixWeekReconActual, int sixWeekReconEstimate) {
        int sixWeekActualMinusEstReconAbsNum = Math.abs(Math.round(sixWeekReconActual - sixWeekReconEstimate));
        int sixWeekActualMinusEstReconNum = Math.round(sixWeekReconActual - sixWeekReconEstimate);
        String sixWeekActualMinusEstReconString = sixWeekActualMinusEstReconAbsNum + "";
        String sixWeekActualMinusEstRecon = "";
        String sixWeekActMinusEstFormatted = "";


        if(sixWeekActualMinusEstReconString.length() > 3) {
            for(int i=0;i<sixWeekActualMinusEstReconString.length();i++) {
                if(i==0) {
                    for(int j=0;j<sixWeekActualMinusEstReconString.length() % 3;j++) {
                        sixWeekActualMinusEstRecon += sixWeekActualMinusEstReconString.charAt(j);
                    }
                sixWeekActualMinusEstRecon += ",";
                }
                if(i!=0) {
                    sixWeekActualMinusEstRecon += sixWeekActualMinusEstReconString.charAt(i);
                }
            }
        } else {
            sixWeekActualMinusEstRecon = sixWeekActualMinusEstReconString;
        }
        String actOrEst = (sixWeekActualMinusEstReconNum > 0) ? "less" : "more";

        if(actOrEst.equals("less")) {
            sixWeekActMinusEstFormatted += "($" + sixWeekActualMinusEstRecon + ")";
        } else {
            sixWeekActMinusEstFormatted += "$" + sixWeekActualMinusEstRecon;
        }
        return sixWeekActMinusEstFormatted;
    }

}
