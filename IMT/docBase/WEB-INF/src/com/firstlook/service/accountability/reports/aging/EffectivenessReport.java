package com.firstlook.service.accountability.reports.aging;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.firstlook.service.accountability.reports.EdgeScorecardReport;

public class EffectivenessReport extends EdgeScorecardReport {

    private String repriceStr;
    private String advertStr;
    private String promotStr;
    private String spiffStr;
    private String otherStr;
    private String wholesalerStr;
    private String auctionStr;
    public String getAdvertStr() {
        return advertStr;
    }
    
    public void setAdvertStr(String advertStr) {
        this.advertStr = advertStr;
    }
    
    public String getAuctionStr() {
        return auctionStr;
    }
    
    public void setAuctionStr(String auctionStr) {
        this.auctionStr = auctionStr;
    }
    
    public String getOtherStr() {
        return otherStr;
    }
    
    public void setOtherStr(String otherStr) {
        this.otherStr = otherStr;
    }
    
    public String getPromotStr() {
        return promotStr;
    }
    
    public void setPromotStr(String promotStr) {
        this.promotStr = promotStr;
    }
    
    public String getRepriceStr() {
        return repriceStr;
    }
    
    public void setRepriceStr(String repriceStr) {
        this.repriceStr = repriceStr;
    }
    
    public String getSpiffStr() {
        return spiffStr;
    }
    
    public void setSpiffStr(String spiffStr) {
        this.spiffStr = spiffStr;
    }
    
    public String getWholesalerStr() {
        return wholesalerStr;
    }
    
    public void setWholesalerStr(String wholesalerStr) {
        this.wholesalerStr = wholesalerStr;
    }
    
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}
