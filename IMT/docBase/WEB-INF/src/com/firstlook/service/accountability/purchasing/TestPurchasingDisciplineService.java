package com.firstlook.service.accountability.purchasing;

import junit.framework.TestCase;

import com.firstlook.persistence.accountability.MockGetMeasure;
import com.firstlook.service.accountability.reports.purchasing.PurchasingDisciplineReport;

public class TestPurchasingDisciplineService extends TestCase {

    public TestPurchasingDisciplineService(String name) {
        super( name );
    }
    
    public void testPurchasingDisciplineService() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        PurchasingDisciplineService service = new PurchasingDisciplineService();
        service.setGetMeasure( getMeasure );
        
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );

        PurchasingDisciplineReport report = (PurchasingDisciplineReport) service.calculate( 0 );
        
        assertEquals( new Integer(0), report.getRedLights() );
        assertEquals( new Integer(0), report.getUnderstock() );
        assertEquals( new Integer(0), report.getOverstock() );
        assertEquals( "0", report.getRedSentence() );
    }
    
    public void testPurchasingDisciplineServiceCaution() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        PurchasingDisciplineService service = new PurchasingDisciplineService();
        service.setGetMeasure( getMeasure );
        
        getMeasure.addMeasure( 2.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );

        PurchasingDisciplineReport report = (PurchasingDisciplineReport) service.calculate( 0 );
        
        assertEquals( new Integer(2), report.getRedLights() );
        assertEquals( new Integer(0), report.getUnderstock() );
        assertEquals( new Integer(0), report.getOverstock() );
        assertEquals( "Caution: 2", report.getRedSentence() );
    }

    public void testPurchasingDisciplineServiceDanger() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        PurchasingDisciplineService service = new PurchasingDisciplineService();
        service.setGetMeasure( getMeasure );
        
        getMeasure.addMeasure( 4.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );

        PurchasingDisciplineReport report = (PurchasingDisciplineReport) service.calculate( 0 );
        
        assertEquals( new Integer(4), report.getRedLights() );
        assertEquals( new Integer(0), report.getUnderstock() );
        assertEquals( new Integer(0), report.getOverstock() );
        assertEquals( "Danger: 4", report.getRedSentence() );
    }
}
