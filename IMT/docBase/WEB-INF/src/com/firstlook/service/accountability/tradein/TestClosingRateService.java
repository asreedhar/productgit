package com.firstlook.service.accountability.tradein;

import junit.framework.TestCase;

import com.firstlook.persistence.accountability.MockGetMeasure;
import com.firstlook.service.accountability.reports.tradein.ClosingRateReport;

public class TestClosingRateService extends TestCase {
    
    public TestClosingRateService(String name) {
        super(name);
    }
    
    public void testDaysSupplyService() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        ClosingRateService service = new ClosingRateService();
        service.setGetMeasure( getMeasure );
        
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );

        ClosingRateReport report = (ClosingRateReport) service.calculate(0);
        
        assertEquals( new Integer(0), report.getSixWeekRate() );
        assertEquals( new Integer(0), report.getTwelveWeekRate() );
    }

    public void testDaysSupplyServiceNormal() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        ClosingRateService service = new ClosingRateService();
        service.setGetMeasure( getMeasure );
        
        getMeasure.addMeasure( 10.0f );
        getMeasure.addMeasure( 20.0f );

        ClosingRateReport report = (ClosingRateReport) service.calculate(0);
        
        assertEquals( new Integer(1000), report.getSixWeekRate() );
        assertEquals( new Integer(2000), report.getTwelveWeekRate() );
    }

    public void testDaysSupplyServiceNaN() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        ClosingRateService service = new ClosingRateService();
        service.setGetMeasure( getMeasure );
        
        getMeasure.addMeasure( Float.NaN );
        getMeasure.addMeasure( Float.NaN );

        ClosingRateReport report = (ClosingRateReport) service.calculate(0);
        
        assertEquals( new Integer(0), report.getSixWeekRate() );
        assertEquals( new Integer(0), report.getTwelveWeekRate() );
    }

}
