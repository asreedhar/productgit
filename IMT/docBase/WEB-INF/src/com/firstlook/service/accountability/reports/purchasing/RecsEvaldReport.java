package com.firstlook.service.accountability.reports.purchasing;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.firstlook.service.accountability.reports.EdgeScorecardReport;

public class RecsEvaldReport extends EdgeScorecardReport {

    private Integer evalRatio;
    private Integer totalNumerator;
    private Integer totalDenominator;
    private Integer numPlans;
    private Integer notEvalRatio;
    
    public Integer getEvalRatio() {
        return evalRatio;
    }
    
    public void setEvalRatio(Integer evalRatio) {
        this.evalRatio = evalRatio;
    }
    
    public Integer getNotEvalRatio() {
        return notEvalRatio;
    }
    
    public void setNotEvalRatio(Integer notEvalRatio) {
        this.notEvalRatio = notEvalRatio;
    }
    
    public Integer getNumPlans() {
        return numPlans;
    }
    
    public void setNumPlans(Integer numPlans) {
        this.numPlans = numPlans;
    }
    
    public Integer getTotalDenominator() {
        return totalDenominator;
    }
    
    public void setTotalDenominator(Integer totalDenominator) {
        this.totalDenominator = totalDenominator;
    }
    
    public Integer getTotalNumerator() {
        return totalNumerator;
    }
    
    public void setTotalNumerator(Integer totalNumerator) {
        this.totalNumerator = totalNumerator;
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
