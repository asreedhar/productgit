package com.firstlook.service.accountability.reports.tradein;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.firstlook.service.accountability.reports.EdgeScorecardReport;


public class AnalyzeRateReport extends EdgeScorecardReport {

    private Boolean moreProfitable;
    private Boolean sellsFaster;
    private Integer sixWeekNonAnalyzed;
    private Integer sixWeekNumNotAnalyzed;
    private Integer twelveWeekNonAnalyzed;
    private String strAnalyzerPerformance;
    
    public Boolean getMoreProfitable() {
        return moreProfitable;
    }
    
    public void setMoreProfitable(Boolean moreProfitable) {
        this.moreProfitable = moreProfitable;
    }
    
    public Boolean getSellsFaster() {
        return sellsFaster;
    }
    
    public void setSellsFaster(Boolean sellsFaster) {
        this.sellsFaster = sellsFaster;
    }
    
    public Integer getSixWeekNonAnalyzed() {
        return sixWeekNonAnalyzed;
    }
    
    public void setSixWeekNonAnalyzed(Integer sixWeekNonAnalyzed) {
        this.sixWeekNonAnalyzed = sixWeekNonAnalyzed;
    }
    
    public Integer getSixWeekNumNotAnalyzed() {
        return sixWeekNumNotAnalyzed;
    }
    
    public void setSixWeekNumNotAnalyzed(Integer sixWeekNumNotAnalyzed) {
        this.sixWeekNumNotAnalyzed = sixWeekNumNotAnalyzed;
    }
    
    public String getStrAnalyzerPerformance() {
        return strAnalyzerPerformance;
    }
    
    public void setStrAnalyzerPerformance(String strAnalyzerPerformance) {
        this.strAnalyzerPerformance = strAnalyzerPerformance;
    }
    
    public Integer getTwelveWeekNonAnalyzed() {
        return twelveWeekNonAnalyzed;
    }
    
    public void setTwelveWeekNonAnalyzed(Integer twelveWeekNonAnalyzed) {
        this.twelveWeekNonAnalyzed = twelveWeekNonAnalyzed;
    }
    
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
 
    
    
}

