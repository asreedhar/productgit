package com.firstlook.service.accountability.reports.tradein;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.firstlook.service.accountability.reports.EdgeScorecardReport;


public class BookOutPrecisionReport extends EdgeScorecardReport {

    private Integer thirtyDayOptions;
    private Integer thirtyDayMileage;
    private Integer thirtyDayAppraisal;
    private Integer thirtyDayRecon;
    
    public Integer getThirtyDayAppraisal() {
        return thirtyDayAppraisal;
    }
    
    public void setThirtyDayAppraisal(Integer thirtyDayAppraisal) {
        this.thirtyDayAppraisal = thirtyDayAppraisal;
    }
    
    public Integer getThirtyDayMileage() {
        return thirtyDayMileage;
    }
    
    public void setThirtyDayMileage(Integer thirtyDayMileage) {
        this.thirtyDayMileage = thirtyDayMileage;
    }
    
    public Integer getThirtyDayOptions() {
        return thirtyDayOptions;
    }
    
    public void setThirtyDayOptions(Integer thirtyDayOptions) {
        this.thirtyDayOptions = thirtyDayOptions;
    }
    
    public Integer getThirtyDayRecon() {
        return thirtyDayRecon;
    }
    
    public void setThirtyDayRecon(Integer thirtyDayRecon) {
        this.thirtyDayRecon = thirtyDayRecon;
    }
    
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
    
    

    
}
