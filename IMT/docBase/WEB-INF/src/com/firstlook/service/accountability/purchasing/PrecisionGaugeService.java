package com.firstlook.service.accountability.purchasing;

import com.firstlook.service.accountability.EdgeScorecardService;
import com.firstlook.service.accountability.reports.EdgeScorecardReport;
import com.firstlook.service.accountability.reports.purchasing.PrecisionGaugeReport;

public class PrecisionGaugeService extends EdgeScorecardService {

    public PrecisionGaugeService() {
        super();
    }
    
    public EdgeScorecardReport calculate(int dealerId) {
       
        float pzDetail = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Purchasing","Buying Plan Recommendations - Trim, Year and Color","Powerzone","Quadrant Score");
        float pzDetailTotal = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Purchasing","Buying Plan Recommendations - Trim, Year and Color","Powerzone","Quadrant Points");

        float wDetail = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Purchasing","Buying Plan Recommendations - Trim, Year and Color","Winners","Quadrant Score");
        float wDetailTotal = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Purchasing","Buying Plan Recommendations - Trim, Year and Color","Winners","Quadrant Points");

        int goodRatio = percent( pzDetailTotal + wDetailTotal, pzDetail + wDetail );
        int badRatio = 100 - goodRatio;
        
        PrecisionGaugeReport report = new PrecisionGaugeReport();
        report.setGoodRatio( new Integer( goodRatio ) );
        report.setBadRatio( new Integer( badRatio ) );

        return report;
    }

    
}
