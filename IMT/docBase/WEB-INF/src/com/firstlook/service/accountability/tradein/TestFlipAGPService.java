package com.firstlook.service.accountability.tradein;

import junit.framework.TestCase;

import com.firstlook.persistence.accountability.MockGetMeasure;
import com.firstlook.service.accountability.reports.tradein.FlipAGPReport;

public class TestFlipAGPService extends TestCase {
    
    public TestFlipAGPService(String name) {
        super(name);
    }
    
    public void testDaysSupplyService() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        FlipAGPService service = new FlipAGPService();
        service.setGetMeasure( getMeasure );
        
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );

        FlipAGPReport report = (FlipAGPReport) service.calculate(0);
        
        assertEquals( new Integer(0), report.getSixWeekFlipProfit() );
        assertEquals( new Integer(0), report.getTwelveWeekFlipProfit() );
        assertEquals( new Integer(0), report.getSixWeekHeight() );
        assertEquals( new Integer(0), report.getTwelveWeekHeight() );
    }

    public void testDaysSupplyServiceNormal() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        FlipAGPService service = new FlipAGPService();
        service.setGetMeasure( getMeasure );
        
        getMeasure.addMeasure( 20.0f );
        getMeasure.addMeasure( 30.0f );

        FlipAGPReport report = (FlipAGPReport) service.calculate(0);
        
        assertEquals( new Integer(20), report.getSixWeekFlipProfit() );
        assertEquals( new Integer(30), report.getTwelveWeekFlipProfit() );
        assertEquals( new Integer(5), report.getSixWeekHeight() );
        assertEquals( new Integer(8), report.getTwelveWeekHeight() );
    }

    public void testDaysSupplyServiceNegative() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        FlipAGPService service = new FlipAGPService();
        service.setGetMeasure( getMeasure );
        
        getMeasure.addMeasure( -20.0f );
        getMeasure.addMeasure( -30.0f );

        FlipAGPReport report = (FlipAGPReport) service.calculate(0);
        
        assertEquals( new Integer(-20), report.getSixWeekFlipProfit() );
        assertEquals( new Integer(-30), report.getTwelveWeekFlipProfit() );
        assertEquals( new Integer(-5), report.getSixWeekHeight() );
        assertEquals( new Integer(-7), report.getTwelveWeekHeight() );
    }

    public void testDaysSupplyServiceOneZero() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        FlipAGPService service = new FlipAGPService();
        service.setGetMeasure( getMeasure );
        
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 30.0f );

        FlipAGPReport report = (FlipAGPReport) service.calculate(0);
        
        assertEquals( new Integer(0), report.getSixWeekFlipProfit() );
        assertEquals( new Integer(30), report.getTwelveWeekFlipProfit() );
        assertEquals( new Integer(0), report.getSixWeekHeight() );
        assertEquals( new Integer(8), report.getTwelveWeekHeight() );
    }
}
