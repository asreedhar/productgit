package com.firstlook.service.accountability.tradein;

import com.firstlook.service.accountability.EdgeScorecardService;
import com.firstlook.service.accountability.reports.EdgeScorecardReport;
import com.firstlook.service.accountability.reports.tradein.FlipAGPReport;

public class FlipAGPService extends EdgeScorecardService {

    public FlipAGPService() {
        super();
    }
    
    public EdgeScorecardReport calculate(int dealerId) {
        float sixWeekFlipProfit = getMeasure.assembleMeasure(dealerId, "Trend","Trade-Ins","Profit When Wholesaled Immediately","Immediate Wholesale Profit","Average Flip Profit");
        float twelveWeekFlipProfit = getMeasure.assembleMeasure(dealerId, "12Wk Avg","Trade-Ins","Profit When Wholesaled Immediately","Immediate Wholesale Profit","Average Flip Profit");

        float baseHeightAdjustorSeed = 4, baseHeightAdjustor;
        if( Math.max( Math.abs(sixWeekFlipProfit), Math.abs(twelveWeekFlipProfit) ) < (baseHeightAdjustorSeed * 110) ) {
            baseHeightAdjustor = baseHeightAdjustorSeed;
        } else {
            baseHeightAdjustor = baseHeightAdjustorSeed + (Math.max( Math.abs(sixWeekFlipProfit), Math.abs(twelveWeekFlipProfit) ) / (baseHeightAdjustorSeed * 110));
        }
        int sixWeekHeight = Math.round(sixWeekFlipProfit / baseHeightAdjustor);
        int twelveWeekHeight = Math.round(twelveWeekFlipProfit / baseHeightAdjustor);
        
        FlipAGPReport report = new FlipAGPReport();
        report.setSixWeekFlipProfit( new Integer( Math.round( sixWeekFlipProfit ) ) );
        report.setTwelveWeekFlipProfit( new Integer( Math.round( twelveWeekFlipProfit ) ) );
        report.setSixWeekHeight( new Integer( sixWeekHeight ) );
        report.setTwelveWeekHeight( new Integer( twelveWeekHeight ) );
        
        return report;
    }
}
