package com.firstlook.service.accountability.tradein;

import junit.framework.TestCase;

import com.firstlook.persistence.accountability.MockGetMeasure;
import com.firstlook.service.accountability.reports.tradein.AnalyzeRateReport;

public class TestAnalyzeRateService extends TestCase {
    
    public TestAnalyzeRateService(String name) {
        super(name);
    }
    
    public void testDaysSupplyService() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        AnalyzeRateService service = new AnalyzeRateService();
        service.setGetMeasure( getMeasure );
        
        
        // Six Weeks (analyzed raw, analyzed, total)
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        
        // 12 Weeks Non-Analyzed Raw
        getMeasure.addMeasure( 0.0f );

        // 12 Weeks Analyzed and Non-analyzed Gross
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        
        // 12 Weeks Analyzed and Non-analyzed Days
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        
        // 12 Weeks Analyzed and Non-analyzed Days Units
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );

        AnalyzeRateReport report = (AnalyzeRateReport) service.calculate(0);
    
        assertEquals( new Boolean(false), report.getMoreProfitable() );
        assertEquals( new Boolean(false), report.getSellsFaster() );
        assertEquals( new Integer( 100 ), report.getSixWeekNonAnalyzed() );
        assertEquals( new Integer( 0 ), report.getSixWeekNumNotAnalyzed() );
        assertEquals( new Integer( 100 ) , report.getTwelveWeekNonAnalyzed() );
        assertEquals( "", report.getStrAnalyzerPerformance() );
    }

    public void testDaysSupplyServiceNormalMoreProfitable() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        AnalyzeRateService service = new AnalyzeRateService();
        service.setGetMeasure( getMeasure );
        
        // Six Weeks (analyzed raw, analyzed, total)
        getMeasure.addMeasure( 20.0f );
        getMeasure.addMeasure( 70.0f );
        getMeasure.addMeasure( 90.0f );
        
        // 12 Weeks Non-Analyzed Raw
        getMeasure.addMeasure( 0.3f );

        // 12 Weeks Analyzed and Non-analyzed Gross
        getMeasure.addMeasure( 130.0f );
        getMeasure.addMeasure( 20.0f );
        
        // 12 Weeks Analyzed and Non-analyzed Days
        getMeasure.addMeasure( 22.0f );
        getMeasure.addMeasure( 10.0f );
        
        // 12 Weeks Analyzed and Non-analyzed Days Units
        getMeasure.addMeasure( 11.0f );
        getMeasure.addMeasure( 11.0f );

        AnalyzeRateReport report = (AnalyzeRateReport) service.calculate(0);
    
        assertEquals( new Boolean(true), report.getMoreProfitable() );
        assertEquals( new Boolean(false), report.getSellsFaster() );
        assertEquals( new Integer( -1900 ), report.getSixWeekNonAnalyzed() );
        assertEquals( new Integer( 20 ), report.getSixWeekNumNotAnalyzed() );
        assertEquals( new Integer( 70 ) , report.getTwelveWeekNonAnalyzed() );
        assertEquals( "Analyzed trade-ins are $110 more profitable than non-analyzed.", report.getStrAnalyzerPerformance() );
    }

    public void testDaysSupplyServiceNormalSellFaster() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        AnalyzeRateService service = new AnalyzeRateService();
        service.setGetMeasure( getMeasure );
        
        // Six Weeks (analyzed raw, analyzed, total)
        getMeasure.addMeasure( 20.0f );
        getMeasure.addMeasure( 70.0f );
        getMeasure.addMeasure( 90.0f );
        
        // 12 Weeks Non-Analyzed Raw
        getMeasure.addMeasure( 0.3f );

        // 12 Weeks Analyzed and Non-analyzed Gross
        getMeasure.addMeasure( 130.0f );
        getMeasure.addMeasure( 50.0f );
        
        // 12 Weeks Analyzed and Non-analyzed Days
        getMeasure.addMeasure( 10.0f );
        getMeasure.addMeasure( 22.0f );
        
        // 12 Weeks Analyzed and Non-analyzed Days Units
        getMeasure.addMeasure( 11.0f );
        getMeasure.addMeasure( 11.0f );

        AnalyzeRateReport report = (AnalyzeRateReport) service.calculate(0);
    
        assertEquals( new Boolean(false), report.getMoreProfitable() );
        assertEquals( new Boolean(true), report.getSellsFaster() );
        assertEquals( new Integer( -1900 ), report.getSixWeekNonAnalyzed() );
        assertEquals( new Integer( 20 ), report.getSixWeekNumNotAnalyzed() );
        assertEquals( new Integer( 70 ) , report.getTwelveWeekNonAnalyzed() );
        assertEquals( "Analyzed trade-ins sell 12 days faster than non-analyzed trade-ins.", report.getStrAnalyzerPerformance() );
    }

    public void testDaysSupplyServiceNormalSellFasterAndMoreProfitable() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        AnalyzeRateService service = new AnalyzeRateService();
        service.setGetMeasure( getMeasure );
        
        // Six Weeks (analyzed raw, analyzed, total)
        getMeasure.addMeasure( 20.0f );
        getMeasure.addMeasure( 70.0f );
        getMeasure.addMeasure( 90.0f );
        
        // 12 Weeks Non-Analyzed Raw
        getMeasure.addMeasure( 0.3f );

        // 12 Weeks Analyzed and Non-analyzed Gross
        getMeasure.addMeasure( 130.0f );
        getMeasure.addMeasure( 20.0f );
        
        // 12 Weeks Analyzed and Non-analyzed Days
        getMeasure.addMeasure( 10.0f );
        getMeasure.addMeasure( 22.0f );
        
        // 12 Weeks Analyzed and Non-analyzed Days Units
        getMeasure.addMeasure( 11.0f );
        getMeasure.addMeasure( 11.0f );

        AnalyzeRateReport report = (AnalyzeRateReport) service.calculate(0);
    
        assertEquals( new Boolean(true), report.getMoreProfitable() );
        assertEquals( new Boolean(true), report.getSellsFaster() );
        assertEquals( new Integer( -1900 ), report.getSixWeekNonAnalyzed() );
        assertEquals( new Integer( 20 ), report.getSixWeekNumNotAnalyzed() );
        assertEquals( new Integer( 70 ) , report.getTwelveWeekNonAnalyzed() );
        assertEquals( "Analyzed trade-ins are $110 more profitable than non-analyzed and sell 12 days faster.", report.getStrAnalyzerPerformance() );
    }

    public void testDaysSupplyServiceNaN() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        AnalyzeRateService service = new AnalyzeRateService();
        service.setGetMeasure( getMeasure );
        
        // Six Weeks (analyzed raw, analyzed, total)
        getMeasure.addMeasure( Float.NaN );
        getMeasure.addMeasure( Float.NaN );
        getMeasure.addMeasure( Float.NaN );
        
        // 12 Weeks Non-Analyzed Raw
        getMeasure.addMeasure( Float.NaN );

        // 12 Weeks Analyzed and Non-analyzed Gross
        getMeasure.addMeasure( Float.NaN );
        getMeasure.addMeasure( Float.NaN );
        
        // 12 Weeks Analyzed and Non-analyzed Days
        getMeasure.addMeasure( Float.NaN );
        getMeasure.addMeasure( Float.NaN );
        
        // 12 Weeks Analyzed and Non-analyzed Days Units
        getMeasure.addMeasure( Float.NaN );
        getMeasure.addMeasure( Float.NaN );

        AnalyzeRateReport report = (AnalyzeRateReport) service.calculate(0);

        assertEquals( new Boolean(false), report.getMoreProfitable() );
        assertEquals( new Boolean(false), report.getSellsFaster() );
        assertEquals( new Integer( 0 ), report.getSixWeekNonAnalyzed() );
        assertEquals( new Integer( 0 ), report.getSixWeekNumNotAnalyzed() );
        assertEquals( new Integer( 0 ) , report.getTwelveWeekNonAnalyzed() );
        assertEquals( "", report.getStrAnalyzerPerformance() );
    }
}
