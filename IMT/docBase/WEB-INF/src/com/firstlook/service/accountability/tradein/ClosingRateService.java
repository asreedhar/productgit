package com.firstlook.service.accountability.tradein;

import com.firstlook.service.accountability.EdgeScorecardService;
import com.firstlook.service.accountability.reports.EdgeScorecardReport;
import com.firstlook.service.accountability.reports.tradein.ClosingRateReport;

public class ClosingRateService extends EdgeScorecardService {

    public ClosingRateService() {
        super();
    }
    
    public EdgeScorecardReport calculate(int dealerId) {
        float sixWeekClosingRateDetail = getMeasure.assembleMeasure(dealerId, "Trend","Trade-Ins","Closing Rate Trend","Closing Rate","Closing Rate Percentage");
        float twelveWeekClosingRateDetail = getMeasure.assembleMeasure(dealerId, "12Wk Avg","Trade-Ins","Closing Rate Trend","Closing Rate","Closing Rate Percentage");

        int sixWeekRateInt = (int) Math.round( sixWeekClosingRateDetail * 100.0f );
        int twelveWeekRateInt = (int) Math.round( twelveWeekClosingRateDetail * 100.0f );

        ClosingRateReport closingRateReport = new ClosingRateReport();
        closingRateReport.setSixWeekRate( new Integer( sixWeekRateInt ) );
        closingRateReport.setTwelveWeekRate( new Integer( twelveWeekRateInt ) );
        
        return closingRateReport;
    }
}
