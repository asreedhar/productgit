package com.firstlook.service.accountability.aging;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import com.firstlook.service.accountability.EdgeScorecardService;
import com.firstlook.service.accountability.reports.EdgeScorecardReport;
import com.firstlook.service.accountability.reports.aging.EffectivenessReport;

public class EffectivenessService extends EdgeScorecardService {

    private Configuration config;

    private String percentTemplate;
    
    public EffectivenessService() {
        super();
        loadConfig();
        initializeConstants();
    }

    private void loadConfig() {
        try {
            config = new PropertiesConfiguration( getClass().getResource("EffectivenessService.properties"));
        } catch (ConfigurationException e) {
            throw new RuntimeException( "Configuration for EffectivenessService could not be loaded" );
        }
    }

    private void initializeConstants() {
        percentTemplate = config.getString("percentTemplate");
    }
    
    public EdgeScorecardReport calculate(int dealerId) {

        float sixWeekSpiffUnits = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Effectiveness","Retail Spiffs","Units");
        float sixWeekSpiffUnitsSold = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Effectiveness","Retail Spiffs","Units Sold");

        float sixWeekPromoteUnits = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Effectiveness","Retail Lot Promote","Units");
        float sixWeekPromoteUnitsSold = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Effectiveness","Retail Lot Promote","Units Sold");

        float sixWeekAdvertUnits = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Effectiveness","Retail Advertise","Units");
        float sixWeekAdvertUnitsSold  = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Effectiveness","Retail Advertise","Units Sold");

        float sixWeekOtherUnits = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Effectiveness","Retail Other","Units");
        float sixWeekOtherUnitsSold = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Effectiveness","Retail Other","Units Sold");

        float sixWeekRepriceUnits = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Effectiveness","Retail Reprice","Units");
        float sixWeekRepriceUnitsSold = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Effectiveness","Retail Reprice","Units Sold");

        float sixWeekWholesalerUnits = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Effectiveness","Wholesale Wholesaler","Units");
        float sixWeekWholesalerUnitsSold = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Effectiveness","Wholesale Wholesaler","Units Sold");

        float sixWeekAuctionUnits = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Effectiveness","Wholesale Auction","Units");
        float sixWeekAuctionUnitsSold = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Effectiveness","Wholesale Auction","Units Sold");

        int spiffP = percent( sixWeekSpiffUnits, sixWeekSpiffUnitsSold );
        int promoteP = percent( sixWeekPromoteUnits, sixWeekPromoteUnitsSold );
        int advertP = percent( sixWeekAdvertUnits, sixWeekAdvertUnitsSold );
        int otherP = percent( sixWeekOtherUnits, sixWeekOtherUnitsSold );
        int repriceP = percent( sixWeekRepriceUnits, sixWeekRepriceUnitsSold );
        int wholesalerP = percent( sixWeekWholesalerUnits, sixWeekWholesalerUnitsSold );
        int auctionP = percent( sixWeekAuctionUnits, sixWeekAuctionUnitsSold );
        
        String spiffStr = createPercentStr( spiffP, sixWeekSpiffUnitsSold, sixWeekSpiffUnits );
        String promoteStr = createPercentStr( promoteP, sixWeekPromoteUnitsSold, sixWeekPromoteUnits );
        String advertStr = createPercentStr( advertP, sixWeekAdvertUnitsSold, sixWeekAdvertUnits );
        String otherStr = createPercentStr( otherP, sixWeekOtherUnitsSold, sixWeekOtherUnits );
        String repriceStr = createPercentStr( repriceP, sixWeekRepriceUnitsSold, sixWeekRepriceUnits );
        String wholesalerStr = createPercentStr( wholesalerP, sixWeekWholesalerUnitsSold, sixWeekWholesalerUnits );
        String auctionStr = createPercentStr( auctionP, sixWeekAuctionUnitsSold, sixWeekAuctionUnits );

        EffectivenessReport report = new EffectivenessReport();
        report.setRepriceStr( repriceStr );
        report.setAdvertStr( advertStr );
        report.setPromotStr( promoteStr );
        report.setSpiffStr( spiffStr );
        report.setOtherStr( otherStr );
        report.setWholesalerStr( wholesalerStr );
        report.setAuctionStr( auctionStr );
        
        return report;
    }
    
    protected String createPercentStr(int percent, float sold, float units) {
        Map params = new HashMap();
        params.put( "percent", new Integer( percent ) );
        params.put( "sold", new Integer( Math.round( sold ) ) );
        params.put( "units", new Integer( Math.round( units ) ) );
        
        return evaluateTemplate( params, percentTemplate );
    }
}
