package com.firstlook.service.accountability.aging;

import com.firstlook.service.accountability.EdgeScorecardService;
import com.firstlook.service.accountability.reports.EdgeScorecardReport;
import com.firstlook.service.accountability.reports.aging.AgingPlanningReport;

public class AgingPlanningService extends EdgeScorecardService {

    public AgingPlanningService() {
        super();
    }
    
    public EdgeScorecardReport calculate(int dealerId) {

        float watchListUnits = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Planning","Watch List","Units");
        float watchListStrategies = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Planning","Watch List","Units with Strategies");
        int watchListP = percent(watchListUnits, watchListStrategies);

        float watchListWeeks = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Planning","Watch List","Weeks with Plans");
        int watchListWeeksP = Math.round( watchListWeeks / 4 * 100 );

        float sixtyUnits = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Planning","60 Plus Days","Units");
        float sixtyStrategies = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Planning","60 Plus Days","Units with Strategies");
        int sixtyP = percent(sixtyUnits, sixtyStrategies);

        float sixtyWeeks = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Planning","60 Plus Days","Weeks with Plans");
        int sixtyWeeksP = Math.round( sixtyWeeks / 4 * 100 );

        float fiftyUnits = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Planning","50-59 Days","Units");
        float fiftyStrategies = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Planning","50-59 Days","Units with Strategies");
        int fiftyP = percent(fiftyUnits, fiftyStrategies);

        float fiftyWeeks = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Planning","50-59 Days","Weeks with Plans");
        int fiftyWeeksP = Math.round( fiftyWeeks / 4 * 100 );

        float fortyUnits = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Planning","40-49 Days","Units");
        float fortyStrategies = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Planning","40-49 Days","Units with Strategies");
        int fortyP = percent(fortyUnits, fortyStrategies);

        float fortyWeeks = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Planning","40-49 Days","Weeks with Plans");
        int fortyWeeksP = Math.round( fortyWeeks / 4 * 100 );

        float thirtyUnits = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Planning","30-39 Days","Units");
        float thirtyStrategies = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Planning","30-39 Days","Units with Strategies");
        int thirtyP = percent( thirtyUnits, thirtyStrategies);

        float thirtyWeeks = getMeasure.assembleMeasure(dealerId,"Four-Week (Plan Base Date)","Aging Inventory","Aging Inventory Planning","30-39 Days","Weeks with Plans");
        int thirtyWeeksP = Math.round( thirtyWeeks / 4 * 100 );

        int maxWeeksP = Math.max( watchListWeeksP,Math.max( sixtyWeeksP, Math.max( fiftyWeeksP, Math.max( fortyWeeksP, thirtyWeeksP ) ) ) );
        int maxVehicleP = Math.max( watchListP, Math.max( sixtyP, Math.max( fiftyP, Math.max( fortyP,thirtyP ) ) ) );

        AgingPlanningReport report = new AgingPlanningReport();
        
        report.setWatchListWeeksP( new Integer( watchListWeeksP ) );
        report.setSixtyWeeksP( new Integer( sixtyWeeksP ) );
        report.setFiftyWeeksP( new Integer( fiftyWeeksP ) );
        report.setFortyWeeksP( new Integer( fortyWeeksP ) );
        report.setThirtyWeeksP( new Integer( thirtyWeeksP ) );
                    
        report.setWatchListWeeks( new Integer( Math.round( watchListWeeks ) ) );
        report.setSixtyWeeks( new Integer( Math.round( sixtyWeeks ) ) );
        report.setFiftyWeeks( new Integer( Math.round( fiftyWeeks ) ) );
        report.setFortyWeeks( new Integer( Math.round( fortyWeeks ) ) );
        report.setThirtyWeeks( new Integer( Math.round( thirtyWeeks ) ) );
        
        report.setWatchListP( new Integer( watchListP ) );
        report.setSixtyP( new Integer( sixtyP ) );
        report.setFiftyP( new Integer( fiftyP ) );
        report.setFortyP( new Integer( fortyP ) );
        report.setThirtyP( new Integer( thirtyP ) );

        report.setWatchListStrategies( new Integer( Math.round( watchListStrategies ) ) );
        report.setSixtyStrategies( new Integer( Math.round( sixtyStrategies ) ) );
        report.setFiftyStrategies( new Integer( Math.round( fiftyStrategies ) ) );
        report.setFortyStrategies( new Integer( Math.round( fortyStrategies ) ) );
        report.setThirtyStrategies( new Integer( Math.round( thirtyStrategies ) ) );

        report.setWatchListUnits( new Integer( Math.round( watchListUnits ) ) );
        report.setSixtyUnits( new Integer( Math.round( sixtyUnits ) ) );
        report.setFiftyUnits( new Integer( Math.round( fiftyUnits ) ) );
        report.setFortyUnits( new Integer( Math.round( fortyUnits ) ) );
        report.setThirtyUnits( new Integer( Math.round( thirtyUnits ) ) );
        
        return report;
    }
    
}
