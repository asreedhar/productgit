package com.firstlook.service.accountability.tradein;

import junit.framework.TestCase;

import com.firstlook.persistence.accountability.MockGetMeasure;
import com.firstlook.service.accountability.reports.tradein.BookOutPrecisionReport;

public class TestBookOutPrecisionService extends TestCase {
    
    public TestBookOutPrecisionService(String name) {
        super(name);
    }
    
    public void testDaysSupplyService() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        BookOutPrecisionService service = new BookOutPrecisionService();
        service.setGetMeasure( getMeasure );
        
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );

        BookOutPrecisionReport report = (BookOutPrecisionReport) service.calculate(0);
        
        assertEquals( new Integer( 0 ), report.getThirtyDayOptions() );
        assertEquals( new Integer( 0 ), report.getThirtyDayMileage() );
        assertEquals( new Integer( 0 ), report.getThirtyDayAppraisal() );
        assertEquals( new Integer( 0 ), report.getThirtyDayRecon() );
    }
    
    public void testDaysSupplyServiceNormal() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        BookOutPrecisionService service = new BookOutPrecisionService();
        service.setGetMeasure( getMeasure );
        
        getMeasure.addMeasure( 10.0f );
        getMeasure.addMeasure( 2.0f );
        getMeasure.addMeasure( 7.0f );
        getMeasure.addMeasure( 24.0f );

        BookOutPrecisionReport report = (BookOutPrecisionReport) service.calculate(0);
        
        assertEquals( new Integer( 1000 ), report.getThirtyDayOptions() );
        assertEquals( new Integer( 200 ), report.getThirtyDayMileage() );
        assertEquals( new Integer( 700 ), report.getThirtyDayAppraisal() );
        assertEquals( new Integer( 2400 ), report.getThirtyDayRecon() );
    }

    public void testDaysSupplyServiceNaN() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        BookOutPrecisionService service = new BookOutPrecisionService();
        service.setGetMeasure( getMeasure );
        
        getMeasure.addMeasure( Float.NaN );
        getMeasure.addMeasure( Float.NaN );
        getMeasure.addMeasure( Float.NaN );
        getMeasure.addMeasure( Float.NaN );

        BookOutPrecisionReport report = (BookOutPrecisionReport) service.calculate(0);
        
        assertEquals( new Integer( 0 ), report.getThirtyDayOptions() );
        assertEquals( new Integer( 0 ), report.getThirtyDayMileage() );
        assertEquals( new Integer( 0 ), report.getThirtyDayAppraisal() );
        assertEquals( new Integer( 0 ), report.getThirtyDayRecon() );
    }

}
