package com.firstlook.service.accountability.purchasing;

import junit.framework.TestCase;

import com.firstlook.persistence.accountability.MockGetMeasure;
import com.firstlook.service.accountability.reports.purchasing.CurrentInventoryReport;

public class TestCurrentInventoryService extends TestCase {
    
    public TestCurrentInventoryService(String name) {
        super( name );
    }
    
    public void testCurrentInventoryService() throws Exception {
        MockGetMeasure getMeasure = new MockGetMeasure();
        CurrentInventoryService service = new CurrentInventoryService();
        service.setGetMeasure( getMeasure );
        
        getMeasure.addMeasure( 0.0f );
        getMeasure.addMeasure( 0.0f );

        getMeasure.addFormattedMeasure( "TEST" );
        
        CurrentInventoryReport report = (CurrentInventoryReport) service.calculate( 0 );
        
        assertEquals( new Integer( 0 ), report.getPurchInvUnits() );
        assertEquals( "TEST", report.getInventoryDollars() );
        assertEquals( new Integer( 0 ), report.getOverallDaysSupply() );
    }

}
