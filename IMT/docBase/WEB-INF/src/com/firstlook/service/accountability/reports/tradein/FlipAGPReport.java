package com.firstlook.service.accountability.reports.tradein;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.firstlook.service.accountability.reports.EdgeScorecardReport;


public class FlipAGPReport extends EdgeScorecardReport {

    private Integer sixWeekFlipProfit;
    private Integer twelveWeekFlipProfit;
    private Integer sixWeekHeight;
    private Integer twelveWeekHeight;
    
    public Integer getSixWeekFlipProfit() {
        return sixWeekFlipProfit;
    }
    
    public void setSixWeekFlipProfit(Integer sixWeekFlipProfit) {
        this.sixWeekFlipProfit = sixWeekFlipProfit;
    }
    
    public Integer getSixWeekHeight() {
        return sixWeekHeight;
    }
    
    public void setSixWeekHeight(Integer sixWeekHeight) {
        this.sixWeekHeight = sixWeekHeight;
    }
    
    public Integer getTwelveWeekFlipProfit() {
        return twelveWeekFlipProfit;
    }
    
    public void setTwelveWeekFlipProfit(Integer twelveWeekFlipProfit) {
        this.twelveWeekFlipProfit = twelveWeekFlipProfit;
    }
    
    public Integer getTwelveWeekHeight() {
        return twelveWeekHeight;
    }
    
    public void setTwelveWeekHeight(Integer twelveWeekHeight) {
        this.twelveWeekHeight = twelveWeekHeight;
    }
    
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
    
    
}
