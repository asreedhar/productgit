package com.firstlook.service.accountability.aging;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import com.firstlook.service.accountability.EdgeScorecardService;
import com.firstlook.service.accountability.reports.EdgeScorecardReport;
import com.firstlook.service.accountability.reports.aging.HighestRiskReport;

public class HighestRiskService extends EdgeScorecardService {

    private Configuration config;
    
    private String redSentenceWarningTemplate;
    private String redSentenceGoodTemplate;
    private String yellowSentenceWarningTemplate;
    private String yellowSentenceGoodTemplate;
    private int redWarningThreshold;
    private int yellowWarningThreshold;

    public HighestRiskService() {
        super();
        loadConfig();
        initializeConstants();
    }
    
    private void loadConfig() {
        try {
            config = new PropertiesConfiguration( getClass().getResource("HighestRiskService.properties"));
        } catch (ConfigurationException e) {
            throw new RuntimeException( "Configuration for EffectivenessService could not be loaded" );
        }
    }

    private void initializeConstants() {
        redSentenceWarningTemplate = config.getString( "redSentenceWarningTemplate" );
        redSentenceGoodTemplate = config.getString( "redSentenceGoodTemplate" );
        yellowSentenceWarningTemplate = config.getString( "yellowSentenceWarningTemplate" );
        yellowSentenceGoodTemplate = config.getString( "yellowSentenceGoodTemplate" );
        redWarningThreshold = config.getInt("redWarningThreshold");
        yellowWarningThreshold = config.getInt("yellowWarningThreshold");
    }
    
    public EdgeScorecardReport calculate(int dealerId) {

        float redOver15Units = getMeasure.assembleMeasure(dealerId,"Current","Aging Inventory","Highest Risk Inventory","Red Light Over 15 Days","Units");
        float yellowOver30Units = getMeasure.assembleMeasure(dealerId,"Current","Aging Inventory","Highest Risk Inventory","Yellow Light Over 30 Days","Units");

        String redOver15Inv = getMeasure.assembleFormattedMeasure(dealerId,"Current","Aging Inventory","Highest Risk Inventory","Red Light Over 15 Days","Sum of Unit Cost");
        String yellowOver30Inv = getMeasure.assembleFormattedMeasure(dealerId,"Current","Aging Inventory","Highest Risk Inventory","Yellow Light Over 30 Days","Sum of Unit Cost");
        String redOver15Water = getMeasure.assembleFormattedMeasure(dealerId,"Current","Aging Inventory","Highest Risk Inventory","Red Light Over 15 Days","Under Water");
        String yellowOver30Water = getMeasure.assembleFormattedMeasure(dealerId,"Current","Aging Inventory","Highest Risk Inventory","Yellow Light Over 30 Days","Under Water");
        
        float allUnits = getMeasure.assembleMeasure(dealerId,"Current","Aging Inventory","Highest Risk Inventory","Vehicles in Inventory","Unit Count");

        int redPercentofAll = percent( allUnits, redOver15Units );
        int yellowPercentofAll = percent( allUnits, yellowOver30Units );

        HighestRiskReport report = new HighestRiskReport();
        
        report.setRedOver15Units( new Integer( Math.round( redOver15Units ) ) );
        report.setRedOver15Inv( redOver15Inv );
        report.setRedOver15Water( redOver15Water );
        report.setRedOutput( getRedOutput( redPercentofAll, Math.round( redOver15Units ) ) );
        
        report.setYellowOver30Units( new Integer( Math.round( yellowOver30Units ) ) );
        report.setYellowOver30Inv( yellowOver30Inv );
        report.setYellowOver30Water( yellowOver30Water );
        report.setYellowOutput( getYellowOutput( yellowPercentofAll, Math.round( yellowOver30Units ) ) );

        return report;

    }

    String getRedOutput(int redPercentofAll, int redOver15Units) {
        Map params = new HashMap();
        params.put( "redOver15Units", new Integer( redOver15Units ) );
        
        StringWriter writer = new StringWriter();
        
        String template = redSentenceGoodTemplate;
        if(redPercentofAll > redWarningThreshold) {
            template = redSentenceWarningTemplate;
        } 

        return evaluateTemplate( params, template );
    }

    String getYellowOutput(int yellowPercentofAll, int yellowOver30Units) {
        Map params = new HashMap();
        params.put( "yellowOver30Units", new Integer( yellowOver30Units ) );
        
        StringWriter writer = new StringWriter();
        
        String template = yellowSentenceGoodTemplate;
        if(yellowPercentofAll > yellowWarningThreshold) {
            template = yellowSentenceWarningTemplate;
        } 
        
        return evaluateTemplate( params, template );
    }

}
