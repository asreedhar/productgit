package com.firstlook.service.accountability.purchasing;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import com.firstlook.service.accountability.EdgeScorecardService;
import com.firstlook.service.accountability.reports.EdgeScorecardReport;
import com.firstlook.service.accountability.reports.purchasing.PlansCompletedReport;

public class PlansCompletedService extends EdgeScorecardService {

    private Configuration config;
    
    private String planText;
    private String noPlanText;
    private String completedColor;
    private String notCompletedColor;
    
    public PlansCompletedService() {
        super();
        loadConfig();
        initializeConstants();
    }
    
    private void loadConfig() {
        try {
            config = new PropertiesConfiguration( getClass().getResource("PlansCompletedService.properties"));
        } catch (ConfigurationException e) {
            throw new RuntimeException( "Configuration for PlansCompletedService could not be loaded" );
        }
    }

    private void initializeConstants() {
        planText = config.getString("planText");
        noPlanText = config.getString("noPlanText");
        completedColor = config.getString("completedColor");
        notCompletedColor = config.getString("notCompletedColor");
    }
    
    public EdgeScorecardReport calculate(int dealerId) {
       
        int week1 = (int) getMeasure.assembleMeasure(dealerId,"Last Week (Plan Base Date)","Purchasing","Buying Plans Completed","Plan Completed?","Cost Point Plan Completed");
        int week2 = (int) getMeasure.assembleMeasure(dealerId,"Two Weeks Ago","Purchasing","Buying Plans Completed","Plan Completed?","Cost Point Plan Completed");
        int week3 = (int) getMeasure.assembleMeasure(dealerId,"Three Weeks Ago","Purchasing","Buying Plans Completed","Plan Completed?","Cost Point Plan Completed");
        int week4 = (int) getMeasure.assembleMeasure(dealerId,"Four Weeks Ago","Purchasing","Buying Plans Completed","Plan Completed?","Cost Point Plan Completed");

        String week1P = (week1 == 1) ? planText : noPlanText;
        String week2P = (week2 == 1) ? planText : noPlanText;
        String week3P = (week3 == 1) ? planText : noPlanText;
        String week4P = (week4 == 1) ? planText : noPlanText;
        
        String week1C = (week1 == 1) ? completedColor : notCompletedColor;
        String week2C = (week2 == 1) ? completedColor : notCompletedColor;
        String week3C = (week3 == 1) ? completedColor : notCompletedColor;
        String week4C = (week4 == 1) ? completedColor : notCompletedColor;
  
        PlansCompletedReport report = new PlansCompletedReport();
        
        report.setWeek1( new Integer( week1 ) );
        report.setWeek2( new Integer( week2 ) );
        report.setWeek3( new Integer( week3 ) );
        report.setWeek4( new Integer( week4 ) );

        report.setWeek1P( week1P );
        report.setWeek2P( week2P );
        report.setWeek3P( week3P );
        report.setWeek4P( week4P );

        report.setWeek1C( week1C );
        report.setWeek2C( week2C );
        report.setWeek3C( week3C );
        report.setWeek4C( week4C );

        return report;
    }

    
}
