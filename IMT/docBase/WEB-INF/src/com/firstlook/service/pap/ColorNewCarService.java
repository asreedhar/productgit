package com.firstlook.service.pap;

import java.util.Collection;

import com.firstlook.persistence.pap.ColorNewCarRetriever;
import com.firstlook.report.PAPReportLineItem;

public class ColorNewCarService
{

private ColorNewCarRetriever colorNewCarRetriever;

public Collection retrieveColorItems( int businessUnitId, int weeks,
        String make, String model, String vehicleTrim, int bodyTypeId,
        int forecast )
{
    vehicleTrim = PAPReportLineItem.determineVehicleTrim(vehicleTrim);
    return colorNewCarRetriever.retrieveColorLineItems(businessUnitId, weeks, make, model,
            vehicleTrim, bodyTypeId, forecast);
}

public void setColorNewCarRetriever(ColorNewCarRetriever colorNewCarRetriever) {
	this.colorNewCarRetriever = colorNewCarRetriever;
}

}
