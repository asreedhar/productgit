package com.firstlook.service.redistributionlog;

import java.sql.Timestamp;
import java.util.Date;

import com.firstlook.entity.RedistributionLog;
import com.firstlook.persistence.redistributionlog.RedistributionLogDAO;

public class RedistributionLogService
{

private RedistributionLogDAO redistributionLogDAO;

public void save( int memberId, int originatingDealerId,
        int destinationDealerId, String stockNumber, String vin,
        int redistributionSourceId )
{
    RedistributionLog log = new RedistributionLog();
    log.setMemberId(memberId);
    log.setOriginatingDealerId(originatingDealerId);
    log.setDestinationDealerId(destinationDealerId);
    log.setOriginatingDealerStockNumber(stockNumber);
    log.setRedistributionSourceId(redistributionSourceId);
    log.setTransactionDate(new Timestamp ( new Date().getTime() ) );
    log.setVin(vin);

    redistributionLogDAO.save(log);
}

public void setRedistributionLogDAO( RedistributionLogDAO redistributionLogDAO )
{
	this.redistributionLogDAO = redistributionLogDAO;
}

}
