package com.firstlook.service.vehiclesale;

import java.util.Map;

public class UsedCarDaysSupplyService extends AbstractDaysSupplyService
{

private int groupingDescriptionId;
private Map priorPeriodMap;
private Map forecastPeriodMap;

public UsedCarDaysSupplyService( int groupingDescriptionId, Map priorPeriodMap,
        Map forecastPeriodMap )
{
    this.groupingDescriptionId = groupingDescriptionId;
    this.priorPeriodMap = priorPeriodMap;
    this.forecastPeriodMap = forecastPeriodMap;
}

public Integer getPriorUnits()
{
    return (Integer) priorPeriodMap.get(new Integer(groupingDescriptionId));
}

public Integer getForecastUnits()
{
    return (Integer) forecastPeriodMap.get(new Integer(groupingDescriptionId));
}

}
