package com.firstlook.service.vehiclesale;

public class UnitsSoldAndMakeModel
{

private String make;
private String model;
private Integer unitsSold;

public UnitsSoldAndMakeModel()
{
}

public Integer getUnitsSold()
{
    return unitsSold;
}

public void setUnitsSold( Integer i )
{
    unitsSold = i;
}

public String getMake()
{
    return make;
}

public String getModel()
{
    return model;
}

public void setMake( String string )
{
    make = string;
}

public void setModel( String string )
{
    model = string;
}

}
