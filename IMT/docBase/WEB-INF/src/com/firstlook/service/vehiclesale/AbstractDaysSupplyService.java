package com.firstlook.service.vehiclesale;

public abstract class AbstractDaysSupplyService
{

public abstract Integer getPriorUnits();

public abstract Integer getForecastUnits();

public Integer calculateDaysSupplyForGrouping( int unitsInStock, int weeks,
        int inventoryType )
{
    int priorUnitsSold = 0;
    int forecastUnitsSold = 0;

    Integer priorUnits = getPriorUnits();
    if ( priorUnits != null )
    {
        priorUnitsSold = priorUnits.intValue();
    }
    Integer forecastUnits = getForecastUnits();
    if ( forecastUnits != null )
    {
        forecastUnitsSold = forecastUnits.intValue();
    }

    return calculateDaysSupply(unitsInStock, weeks, priorUnitsSold,
            forecastUnitsSold);
}

Integer calculateDaysSupply( int unitsInStock, int weeks, int priorUnitsSold,
        int forecastUnitsSold )
{
    int days = ((weeks * 7) * 2);
    int unitsSold = priorUnitsSold + forecastUnitsSold;
    double rateOfSale = (double) unitsSold / (double) days;
    if ( rateOfSale > 0 )
    {
        return new Integer((int) (Math.round(unitsInStock / rateOfSale)));
    } else
    {
        return new Integer(0);
    }
}
}
