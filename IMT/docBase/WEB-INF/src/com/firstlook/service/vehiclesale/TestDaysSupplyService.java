package com.firstlook.service.vehiclesale;

import junit.framework.TestCase;

public class TestDaysSupplyService extends TestCase
{

private AbstractDaysSupplyService service;

public TestDaysSupplyService( String name )
{
    super(name);
}

public void setUp()
{
    service = new MockDaysSupplyService();
}

public void testCalculateDaysSupplyZeroRateOfSale()
{
    int weeks = 13;
    int unitsInStock = 4;
    int priorUnitsSold = 0;
    int forecastUnitsSold = 0;

    Integer daysSupply = service.calculateDaysSupply(unitsInStock, weeks,
            priorUnitsSold, forecastUnitsSold);

    assertEquals(0, daysSupply.intValue());
}

public void testCalculateDaysSupplyRoundUp()
{
    int weeks = 10;
    int unitsInStock = 3;
    int priorUnitsSold = 140;
    int forecastUnitsSold = 140;

    Integer daysSupply = service.calculateDaysSupply(unitsInStock, weeks,
            priorUnitsSold, forecastUnitsSold);

    assertEquals(2, daysSupply.intValue());
}

public void testCalculateDaysSupplyRoundDown()
{
    int weeks = 10;
    int unitsInStock = 2;
    int priorUnitsSold = 140;
    int forecastUnitsSold = 140;

    Integer daysSupply = service.calculateDaysSupply(unitsInStock, weeks,
            priorUnitsSold, forecastUnitsSold);

    assertEquals(1, daysSupply.intValue());
}

}
