package com.firstlook.service.inventory;

import biz.firstlook.transact.persist.model.MakeModelGrouping;
import biz.firstlook.transact.persist.service.MakeModelGroupingService;

public class MockMakeModelGroupingService extends MakeModelGroupingService
{

private MakeModelGrouping makeModelGrouping;

public MockMakeModelGroupingService()
{
	super();
}

/**
 * Use this to set the field which will be returned by 
 * @param makeModelGrouping
 */
public void setMakeModelGroupingToReturn( MakeModelGrouping makeModelGrouping )
{
	this.makeModelGrouping = makeModelGrouping;
}


public MakeModelGrouping retrieveMakeModelGroupingUsingVin( String vin,
                                                            String make, String model ) {
	
	return this.makeModelGrouping;
}


}
