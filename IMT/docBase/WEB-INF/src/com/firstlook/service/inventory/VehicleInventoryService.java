package com.firstlook.service.inventory;

import biz.firstlook.transact.persist.model.Inventory;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.persistence.IInventoryDAO;
import biz.firstlook.transact.persist.service.vehicle.VehicleDAO;

import com.firstlook.action.components.vehicledetailpage.VehicleInventory;

public class VehicleInventoryService
{

private VehicleDAO vehicleDAO;
private IInventoryDAO inventoryDAO;

public VehicleInventory retrieveVehicleInventory( int dealerId, Integer inventoryId )
{
    Inventory inventory = inventoryDAO.findBy( inventoryId );
    VehicleInventory vehicleInventory = null;
    if( inventory != null )
    {
    	Vehicle vehicle = vehicleDAO.findByPk( new Integer( inventory.getVehicleId() ) );

    	vehicleInventory = new VehicleInventory( inventory, vehicle );
    }

    return vehicleInventory;
}

public void saveOrUpdateVehicle( Vehicle vehicle )
{
	vehicleDAO.saveOrUpdate( vehicle );
}

public void saveOrUpdateInventory( Inventory inventory )
{
	inventoryDAO.saveOrUpdate( inventory );
}

public VehicleDAO getVehicleDAO()
{
	return vehicleDAO;
}

public void setVehicleDAO( VehicleDAO vehicleDAO )
{
	this.vehicleDAO = vehicleDAO;
}

public IInventoryDAO getInventoryDAO()
{
	return inventoryDAO;
}

public void setInventoryDAO( IInventoryDAO inventoryDAO )
{
	this.inventoryDAO = inventoryDAO;
}

public Integer findMileage(Integer inventoryId){
	Inventory inventory = inventoryDAO.findMileage( inventoryId );
	Integer mileage = inventory.getMileageReceived();
	if(mileage == null){
		mileage = 0;
	}
	return mileage;
}
}
