package com.firstlook.service.inventory;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import biz.firstlook.transact.persist.model.GroupingDescription;
import biz.firstlook.transact.persist.model.GroupingDescriptionLight;
import biz.firstlook.transact.persist.model.MakeModelGrouping;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.retriever.BookVsUnitCostSummaryDisplayBean;
import biz.firstlook.transact.persist.retriever.BookVsUnitCostSummaryRetriever;
import biz.firstlook.transact.persist.service.MakeModelGroupingService;

import com.firstlook.comparator.InventoryComparator;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.InventoryStatusCD;
import com.firstlook.entity.Member;
import com.firstlook.entity.lite.IInventory;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.inventory.AgingPlanInventoryItemRetriever;
import com.firstlook.persistence.inventory.IInventoryDAO;
import com.firstlook.persistence.inventory.InventoryStatusCDPersistence;
import com.firstlook.persistence.inventory.UnitsInStockRetriever;
import com.firstlook.service.dealer.IDealerService;
import com.firstlook.service.dealergroup.DealerGroupService;
import com.firstlook.service.risklevel.GroupingDescriptionLightService;

public class InventoryService_Legacy
{
private IInventoryDAO inventoryPersistence;
private MakeModelGroupingService makeModelGroupingService;
private GroupingDescriptionLightService groupingDescriptionLightService;
private AgingPlanInventoryItemRetriever agingPlanInventoryItemRetriever;
private UnitsInStockRetriever unitsInStockRetriever;
private BookVsUnitCostSummaryRetriever bookVsUnitCostSummaryRetriever;

public InventoryService_Legacy() {}

// PLS DON'T USE THIS 
public InventoryService_Legacy(IInventoryDAO inventoryDAO)
{
	this.inventoryPersistence = inventoryDAO;
}

public InventoryEntity retrieveInventoryForValidMember( int memberType, int inventoryId, int currentDealerId ) throws ApplicationException
{
	InventoryEntity inventory = null;
	inventory = inventoryPersistence.findByPk( new Integer( inventoryId ) );

	if ( currentDealerId != inventory.getDealerId() && memberType != Member.MEMBER_TYPE_ADMIN )
	{
		throw new ApplicationException( "Security Error: Trying to access a vehicle not associated with member's current dealer." );
	}
	return inventory;
}

public Collection<InventoryEntity> retrieveGroupingDescriptionIdAndVehicleYearByDealerIdAndInventoryType( int dealerId, int inventoryType )
{
	Collection<InventoryEntity> returnList = new ArrayList<InventoryEntity>();
	Collection objs = inventoryPersistence.findGroupingDescriptionIdAndVehicleYearByDealerIdAndInventoryType( dealerId, inventoryType );
	Iterator objIter = objs.iterator();
	Object[] objArray;
	Integer groupingDescriptionId;
	Integer vehicleYear;
	InventoryEntity inventory;
	Vehicle vehicle;
	MakeModelGrouping mmg;
	GroupingDescription gd;
	while ( objIter.hasNext() )
	{
		objArray = (Object[])objIter.next();
		groupingDescriptionId = (Integer)objArray[0];
		vehicleYear = (Integer)objArray[1];
		inventory = new InventoryEntity();
		vehicle = new Vehicle();
		gd = new GroupingDescription();
		mmg = new MakeModelGrouping();

		gd.setGroupingDescriptionId( groupingDescriptionId );
		vehicle.setVehicleYear( vehicleYear );
		mmg.setGroupingDescription( gd );
		vehicle.setMakeModelGrouping( mmg );
		inventory.setVehicle( vehicle );

		returnList.add( inventory );
	}
	return returnList;
}
/**
 * nk - this is a legacy method from the old IMP. It is still used for the Inventory Manager.
 * 
 * @param businessUnitId
 * @param statusCodes
 * @param rangeSetId
 * @return
 */
public Map<Integer, List<IInventory>> retrieveBucketedInventoryItems( int businessUnitId, String statusCodes, Integer rangeSetId )
{
	Map<Integer, List<IInventory>> bucketedInventoryItems = new HashMap<Integer, List<IInventory>>();

	List<InventoryEntity> inventoryItems = agingPlanInventoryItemRetriever.retrieveAgingPlanInventoryItems( businessUnitId, statusCodes, rangeSetId );
	Iterator<InventoryEntity> inventoryIter = inventoryItems.iterator();
	while ( inventoryIter.hasNext() ) {
		InventoryEntity inventory = inventoryIter.next();
		Integer rangeId = inventory.getRangeId();
		
		List<IInventory> inventories = bucketedInventoryItems.get(rangeId);
		if(inventories == null) {
			inventories = new ArrayList<IInventory>();
		}
		inventories.add(inventory);
		bucketedInventoryItems.put( rangeId, inventories );
	}

	return bucketedInventoryItems;
}



public InventoryEntity retrieveInventory( int inventoryId )
{
	return inventoryPersistence.findByPk( new Integer( inventoryId ) );
}

public int countInventoriesBy( int dealerId, int inventoryType, int groupingDescriptionId )
{
	return inventoryPersistence.countBy( dealerId, inventoryType, groupingDescriptionId );
}

public static int calculateTotalBookValue( Collection inventories, int guideBookPreferenceId )
{
	int totalBookValue = 0;
	Iterator inventoryIter = inventories.iterator();
	Integer guideBookValue;

	while ( inventoryIter.hasNext() )
	{
		InventoryEntity inventory = (InventoryEntity)inventoryIter.next();
		guideBookValue = inventory.retriveGuideBookValue( guideBookPreferenceId );
		if ( guideBookValue != null )
		{
			totalBookValue += guideBookValue.intValue();
		}
	}

	return totalBookValue;
}

public static int calculateTotalUnitCost( Collection inventories, int guideBookPreferenceId )
{
	double totalUnitCost = 0;
	Iterator inventoryIter = inventories.iterator();

	while ( inventoryIter.hasNext() )
	{
		InventoryEntity inventory = (InventoryEntity)inventoryIter.next();
		totalUnitCost += inventory.getUnitCost();
	}

	return (int)Math.round( totalUnitCost );
}

public static int calculateTotalUnitCostForBook( Collection inventories, int guideBookPreferenceId )
{
	double totalUnitCost = 0;
	Iterator inventoryIter = inventories.iterator();
	Integer guideBookValue;

	while ( inventoryIter.hasNext() )
	{
		InventoryEntity inventory = (InventoryEntity)inventoryIter.next();
		guideBookValue = inventory.retriveGuideBookValue( guideBookPreferenceId );
		if ( guideBookValue != null && guideBookValue.doubleValue() != 0)
		{
			totalUnitCost += inventory.getUnitCost();
		}
	}

	return (int)Math.round( totalUnitCost );
}

public int retrieveInventoryCountUsingDealerAndInventoryType( int dealerId, int inventoryType )
{
	return inventoryPersistence.findCountByDealerIdAndInventoryType( dealerId, inventoryType );
}

/**
 * Used to get the information for display on the Book vs. Cost tile. This can probably be improved using predicate filters.
 * 
 * @param dealerId
 * @param bookOutPreferenceId -
 *            same as ThirdPartyCategory
 * @return the information of book vs. cost for the third party category.
 */
public BookVsUnitCostSummaryDisplayBean findBookVsUnitCostSummary( int dealerId, int bookOutPreferenceId )
{
	List list = bookVsUnitCostSummaryRetriever.call( new Integer( dealerId ), new Integer( bookOutPreferenceId ) );
	if ( !list.isEmpty() )
	{
		return (BookVsUnitCostSummaryDisplayBean)list.get( 0 );
	}
	else
	{
		BookVsUnitCostSummaryDisplayBean emptyBean = new BookVsUnitCostSummaryDisplayBean();
		emptyBean.setTotalInventoryBookCost( new Integer( 0 ) );
		emptyBean.setTotalInventoryUnitCost( new Float( 0 ) );
		return emptyBean;
	}
}

/**
 * Used to get the information for display on the Book vs. Cost tile.
 * 
 * @param dealerId
 * @param bookOutPreferenceId -
 *            same as ThirdPartyCategory
 * @return the information of book vs. cost for the third party category.
 */
public List<BookVsUnitCostSummaryDisplayBean> findBookVsUnitCostSummary( int currentDealerId )
{
	return bookVsUnitCostSummaryRetriever.call( new Integer( currentDealerId ), null );
}

public static double calculatePercentageOfTotalUnitCost( Collection vehicles, double sumUnitCost )
{
	double ageBandTotalUnitCost = 0.0;

	Iterator<InventoryEntity> vehiclesIter = vehicles.iterator();
	while ( vehiclesIter.hasNext() )
	{
		InventoryEntity inventory = vehiclesIter.next();
		ageBandTotalUnitCost += inventory.getUnitCost();
	}
	if ( sumUnitCost != 0.0 )
	{
		return ageBandTotalUnitCost / sumUnitCost;
	}
	else
	{
		return 0.0;
	}
}

public static double calculatePercentageOfInventory( Collection ageBandInventory, int inventoryCount )
{
	if ( inventoryCount > 0 )
	{
		return (double)ageBandInventory.size() / (double)inventoryCount;
	}
	else
	{
		return 0.0;
	}
}

public Collection<InventoryEntity> retrieveInventoryByDealerGroupIdAndLight( Dealer dealer, int light, DealerGroupService dgService, IDealerService dealerService )
		throws ApplicationException
{
	List<GroupingDescriptionLight> lightList = groupingDescriptionLightService.retrieveByDealerIdAndLight( dealer.getDealerId().intValue(), light );
	List<Integer> groupingDescriptionIds = groupingDescriptionLightService.createGroupingDescriptionIdList( lightList );
	String groupingDescIdStr = StringUtils.join( groupingDescriptionIds.iterator(), "','" );

	Date date = com.firstlook.helper.InventoryHelper.determineAgingPolicyInventoryReceivedDate( dealer, dgService );
	DealerGroup group = dgService.retrieveByDealerId( dealer.getDealerId().intValue() );
	Collection<Dealer> dealersInGroup = dealerService.retrieveByDealerGroupId( group.getBusinessUnitIdInt() );
	dealersInGroup.remove( dealer );
	List<Integer> dealersInGroupIds = createDealerIdList( dealersInGroup );
	String dealersInGroupStr = StringUtils.join( dealersInGroupIds.iterator(), "','" );

	Collection inventories = inventoryPersistence.findByDealerIdAndInventoryStatusAndAgingPolicyAndGreenLight( groupingDescIdStr, new Timestamp( date.getTime() ),
																												dealersInGroupStr );

	List<InventoryEntity> sortedInventory = new ArrayList<InventoryEntity>( inventories );
	Collections.sort( sortedInventory, new InventoryComparator() );

	return sortedInventory;
}

private List<Integer> createDealerIdList( Collection<Dealer> dealersInGroup )
{
	List<Integer> dealersInGroupIds = new ArrayList<Integer>();

	Dealer dealer;

	Iterator<Dealer> iter = dealersInGroup.iterator();
	while ( iter.hasNext() )
	{
		dealer = iter.next();
		dealersInGroupIds.add( dealer.getDealerId() );
	}

	return dealersInGroupIds;
}

public void saveOrUpdate( InventoryEntity inventory )
{
	inventoryPersistence.saveOrUpdate( inventory );
}

public Map<Integer, InventoryStatusCD> retrieveInventoryStatusCodes()
{
	InventoryStatusCDPersistence statusCodePersistence = new InventoryStatusCDPersistence();

	Map<Integer, InventoryStatusCD> statusMap = new HashMap<Integer, InventoryStatusCD>();
	List<InventoryStatusCD> statusCodes = statusCodePersistence.findAll();

	Iterator<InventoryStatusCD> statusCodesIter = statusCodes.iterator();
	InventoryStatusCD statusCode = null;

	while ( statusCodesIter.hasNext() )	{
		statusCode = (InventoryStatusCD)statusCodesIter.next();
		statusMap.put( statusCode.getInventoryStatusCD(), statusCode );
	}

	return statusMap;
}

public void populateStatusDescription( Collection inventories )
{
	Map statusCodes = retrieveInventoryStatusCodes();
	Iterator inventoryIter = inventories.iterator();
	while ( inventoryIter.hasNext() )
	{
		InventoryEntity inventory = (InventoryEntity)inventoryIter.next();
		inventory.setStatusDescription( ( (InventoryStatusCD)statusCodes.get( new Integer( inventory.getStatusCode() ) ) ).getShortDescription() );
	}
}

public static String createGroupingString( String make, String model, String vehicleTrim, String bodyType )
{
	String rtrnStr = make + " " + model;
	if ( bodyType == null || bodyType.equalsIgnoreCase( "Unknown" ) )
	{
		if ( !vehicleTrim.equalsIgnoreCase( "" ) )
		{
			rtrnStr += " " + vehicleTrim;
		}
	}
	else
	{
		if ( vehicleTrim.equalsIgnoreCase( "" ) )
		{
			rtrnStr += " " + bodyType;
		}
		else
		{
			rtrnStr += " " + vehicleTrim + " " + bodyType;
		}
	}
	return rtrnStr.trim();
}

public InventoryEntity createEmptyInventoryWithYearMileageMakeModelUsingVin( String year, int mileage, String vin, String make, String model )
		throws DatabaseException, ApplicationException
{
	InventoryEntity inventory = new InventoryEntity();
	if ( year != null )
	{
		inventory.setVehicleYear( Integer.parseInt( year ) );
	}
	else
	{
		inventory.setVehicleYear( 0 );
	}
	inventory.setMileageReceived( new Integer( mileage ) );
	MakeModelGrouping makeModelGrouping = makeModelGroupingService.retrieveMakeModelGroupingUsingVin( vin, make, model );
	if ( makeModelGrouping != null )
	{
		inventory.setMakeModelGroupingId( makeModelGrouping.getMakeModelGroupingId() );
		inventory.setMakeModelGrouping( makeModelGrouping );
	}

	if ( inventory.getMakeModelGrouping() == null )
	{
		setMakeModelGroupingOnInventory( make, model, inventory );
	}

	return inventory;
}

public InventoryEntity createEmptyInventoryWithYearMileageMakeModel( String year, int mileage, String make, String model )
{
	InventoryEntity inventory = new InventoryEntity();
	if ( year != null )
	{
		inventory.setVehicleYear( Integer.parseInt( year ) );
	}
	else
	{
		inventory.setVehicleYear( 0 );
	}
	inventory.setMileageReceived( new Integer( mileage ) );
	MakeModelGrouping makeModelGrouping = makeModelGroupingService.retrieveByMakeModel( make, model );
	if ( makeModelGrouping != null )
	{
		inventory.setMakeModelGroupingId( makeModelGrouping.getMakeModelGroupingId() );
		inventory.setMakeModelGrouping( makeModelGrouping );
	}

	return inventory;
}

private void setMakeModelGroupingOnInventory( String make, String model, InventoryEntity inventory )
{
	MakeModelGrouping makeModelGrouping = makeModelGroupingService.retrieveByMakeModel( make, model );
	if ( makeModelGrouping != null )
	{
		inventory.setMakeModelGroupingId( makeModelGrouping.getMakeModelGroupingId() );
		inventory.setMakeModelGrouping( makeModelGrouping );
	}
}

public Collection retrieveByDealerIdOrderByReceivedDateStockNumber( int dealerId )
{
	return inventoryPersistence.findByDealerIdOrderByReceivedDateStockNumber( dealerId );
}

public int retrieveInventoryCountByDealerIdAndCurrentVehicleLight( int dealerId, int currentVehicleLight )
{
	return inventoryPersistence.findInventoryCountByDealerIdAndCurrentVehicleLight( dealerId, currentVehicleLight );
}

public int retrieveInventoryCountByDealerIdAndAgeBand( int dealerId, Date beginDate, Date endDate, int inventoryType )
{
	return inventoryPersistence.findInventoryCountByDealerIdAndAgeBand( dealerId, beginDate, endDate, inventoryType );
}

public int retrieveUnitsInStockByDealerIdOrDealerGroup( int dealerId, int includeDealerGroup, String trim, int inventoryType, String make,
														String model )
{
	return unitsInStockRetriever.retrieveUnitsInStock( dealerId, trim, includeDealerGroup, inventoryType, make, model );
}

public Collection retrieveByDealerIdAndInventoryStatusAndAgingPolicy( Dealer dealer, DealerGroupService dgService, IDealerService dealerService )
		throws ApplicationException
{
	Date date = com.firstlook.helper.InventoryHelper.determineAgingPolicyInventoryReceivedDate( dealer, dgService );
	DealerGroup group = dgService.retrieveByDealerId( dealer.getDealerId().intValue() );
	Collection dealersInGroup = dealerService.retrieveByDealerGroupId( group.getBusinessUnitIdInt() );
	dealersInGroup.remove( dealer );
	List dealersInGroupIds = createDealerIdList( dealersInGroup );
	String dealersInGroupStr = StringUtils.join( dealersInGroupIds.iterator(), "','" );
	return inventoryPersistence.findByDealerIdAndInventoryStatusAndAgingPolicy( new Timestamp( date.getTime() ), dealersInGroupStr );
}

public Collection retrieveByDealerIdAndOlderThanXDays( int dealerId, int days )
{
	return inventoryPersistence.findByDealerIdAndOlderThanXDays( dealerId, days );
}

public Integer retrieveCountByDealerIdInventoryTypeSegmentIdInventoryActive( Integer businessUnitId, Integer inventoryType,
																				Integer segmentId )
{
	return inventoryPersistence.findCountByDealerIdInventoryTypeSegmentIdInventoryActive( businessUnitId, inventoryType, segmentId );
}

public Collection retrieveByVehicleInDealerGroupInventoryConstraints( Dealer dealer, int dealerGroupId, int year, String make, String model,
																		String trim, IDealerService dealerService )
{
	Collection dealersInGroup = dealerService.retrieveByDealerGroupId( dealerGroupId );
	dealersInGroup.remove( dealer );
	List dealersInGroupIds = createDealerIdList( dealersInGroup );
	String dealersInGroupStr = StringUtils.join( dealersInGroupIds.iterator(), "','" );
	return inventoryPersistence.findByVehicleInDealerGroupInventoryConstraints( dealerGroupId, year, make, model, trim, dealersInGroupStr );
}



public Boolean isRepriceConfirmationNeeded( int inventoryId, int newPrice, Integer priceChangeThreshold )
{
	InventoryEntity inventory = inventoryPersistence.findByPk( inventoryId );
	
	if (inventory.getListPrice() == null){
		return true;
	}
	
	double percentChange = (inventory.getListPrice() - new Double( newPrice )) / inventory.getListPrice();
	
	if ( ( Math.abs( percentChange ) * 100 ) >= priceChangeThreshold )
	{
		return true;
	}
	
	return false;
}

public String retrieveStockNumberByVinAndDealerId( String vin, Integer businessUnitId, Integer inactiveInventoryDaysBack )
{
	return inventoryPersistence.retrieveStockNumberByVinAndDealerId(vin, businessUnitId, inactiveInventoryDaysBack );
}

public void setInventoryPersistence( IInventoryDAO inventoryPersistence )
{
	this.inventoryPersistence = inventoryPersistence;
}

public void setMakeModelGroupingService( MakeModelGroupingService makeModelGroupingService )
{
	this.makeModelGroupingService = makeModelGroupingService;
}

public void setBookVsUnitCostSummaryRetriever( BookVsUnitCostSummaryRetriever bookVsUnitCostSummaryExecutor )
{
	this.bookVsUnitCostSummaryRetriever = bookVsUnitCostSummaryExecutor;
}

public void setGroupingDescriptionLightService( GroupingDescriptionLightService groupingDescriptionLightService )
{
	this.groupingDescriptionLightService = groupingDescriptionLightService;
}

public void setAgingPlanInventoryItemRetriever(
		AgingPlanInventoryItemRetriever agingPlanInventoryItemRetriever) {
	this.agingPlanInventoryItemRetriever = agingPlanInventoryItemRetriever;
}

public void setUnitsInStockRetriever(UnitsInStockRetriever unitsInStockRetriever) {
	this.unitsInStockRetriever = unitsInStockRetriever;
}

}
