package com.firstlook.service.inventory;

import java.util.ArrayList;
import java.util.Collection;

import biz.firstlook.transact.persist.model.MakeModelGrouping;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.Member;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.MockInventory;
import com.firstlook.mock.MockMember;
import com.firstlook.mock.ObjectMother;
import com.firstlook.persistence.inventory.MockInventoryPersistence;
import com.firstlook.service.member.MockMemberService;

public class TestInventoryService extends BaseTestCase
{

private MockMember mockMember;
private MockMemberService memberService;
private InventoryService_Legacy inventoryService_Legacy;
private MockInventoryPersistence persistence;

public TestInventoryService( String name )
{
	super( name );
}

public void setup()
{
	mockMember = new MockMember();
	mockMember.setCurrentDealer( new Dealer() );

	memberService = new MockMemberService();
	persistence = new MockInventoryPersistence();

	inventoryService_Legacy = new InventoryService_Legacy( persistence );
}

public void testGetInventoryForValidMemberSuccess() throws java.lang.Exception
{
	InventoryEntity inventory = ObjectMother.createInventory();
	inventory.setDealerId( 1 );
	int inventoryId = 0;
	if ( inventory.getInventoryId() != null )
	{
		inventoryId = inventory.getInventoryId().intValue();
	}

	persistence.add( inventoryId, inventory );

	InventoryEntity returnedVehicle = inventoryService_Legacy.retrieveInventoryForValidMember( Member.MEMBER_TYPE_USER, inventoryId, 1 );
	assertNotNull( returnedVehicle );
}

public void testGetInventoryForValidMemberWhoIsAnAdmin() throws java.lang.Exception
{
	InventoryEntity inventory = ObjectMother.createInventory();
	inventory.setDealerId( 1 );
	int inventoryId = 0;
	if ( inventory.getInventoryId() != null )
	{
		inventoryId = inventory.getInventoryId().intValue();
	}
	persistence.add( inventoryId, inventory );
	memberService.setAdmin( true );

	InventoryEntity returnedInventory = inventoryService_Legacy.retrieveInventoryForValidMember( Member.MEMBER_TYPE_USER, inventoryId, 1 );
	assertNotNull( returnedInventory );
}

public void testCalculateTotalBookValue()
{
	Collection inventories = new ArrayList();
	MockInventory inventory = new MockInventory();
	inventory.setBlackBookClean( new Integer( 20 ) );

	MockInventory inventory2 = new MockInventory();
	inventory2.setBlackBookClean( new Integer( 30 ) );

	inventories.add( inventory );
	inventories.add( inventory2 );

	assertEquals( 50, InventoryService_Legacy.calculateTotalBookValue( inventories, ThirdPartyCategory.BLACKBOOK_CLEAN_TYPE ) );
}

public void testCalculatePercentageOfUnitSales()
{

	double sumUnitCost = 10.0;

	Collection ageBandCollection = new ArrayList();
	InventoryEntity inventory3 = ObjectMother.createInventory();
	inventory3.setUnitCost( 2 );

	InventoryEntity inventory4 = ObjectMother.createInventory();
	inventory4.setUnitCost( 3 );

	ageBandCollection.add( inventory3 );
	ageBandCollection.add( inventory4 );

	assertEquals( "Wrong unitcost%", .5, InventoryService_Legacy.calculatePercentageOfTotalUnitCost( ageBandCollection, sumUnitCost ), 0.01 );
}

public void testCalculatePercentageOfUnitSalesZeroes()
{
	Collection ageBandCollection = new ArrayList();
	InventoryEntity inventory3 = ObjectMother.createInventory();
	inventory3.setUnitCost( 2 );

	InventoryEntity inventory4 = ObjectMother.createInventory();
	inventory4.setUnitCost( 3 );

	ageBandCollection.add( inventory3 );
	ageBandCollection.add( inventory4 );

	assertEquals( "Wrong unitcost%", 0.0, InventoryService_Legacy.calculatePercentageOfTotalUnitCost( ageBandCollection, 0.0 ), 0.01 );
}

public void testCalculatePercentageOfInventory()
{

	int inventoryCount = 2;

	Collection ageBandCollection = new ArrayList();
	InventoryEntity inventory3 = ObjectMother.createInventory();
	inventory3.setUnitCost( 2 );

	ageBandCollection.add( inventory3 );

	assertEquals( "Wrong %OfInventory", .5, InventoryService_Legacy.calculatePercentageOfInventory( ageBandCollection, inventoryCount ), 0.01 );
}

public void testCalculatePercentageOfInventoryZeroes()
{
	Collection ageBandCollection = new ArrayList();
	InventoryEntity inventory3 = ObjectMother.createInventory();
	inventory3.setUnitCost( 2 );

	InventoryEntity inventory4 = ObjectMother.createInventory();
	inventory4.setUnitCost( 3 );

	ageBandCollection.add( inventory3 );
	ageBandCollection.add( inventory4 );

	assertEquals( "Wrong %OfInventory", 0.0, InventoryService_Legacy.calculatePercentageOfInventory( ageBandCollection, 0 ), 0.01 );
}

public void testCalculateTotalUnitCostForBookRoundsUp()
{
	Collection inventories = new ArrayList();
	MockInventory inventory = new MockInventory();
	inventory.setUnitCost( 20.5 );
	inventory.setBlackBookClean( new Integer( 20 ) );

	MockInventory inventory2 = new MockInventory();
	inventory2.setUnitCost( 30.1 );
	inventory2.setBlackBookClean( new Integer( 20 ) );
	inventories.add( inventory );
	inventories.add( inventory2 );

	assertEquals( 51, InventoryService_Legacy.calculateTotalUnitCostForBook( inventories, ThirdPartyCategory.BLACKBOOK_CLEAN_TYPE ) );
}

public void testCalculateTotalUnitCostForBookRoundsDown()
{
	Collection inventories = new ArrayList();
	MockInventory inventory = new MockInventory();
	inventory.setUnitCost( 20.1 );
	inventory.setBlackBookClean( new Integer( 20 ) );

	MockInventory inventory2 = new MockInventory();
	inventory2.setUnitCost( 30.1 );
	inventory2.setBlackBookClean( new Integer( 20 ) );
	inventories.add( inventory );
	inventories.add( inventory2 );

	assertEquals( 50, InventoryService_Legacy.calculateTotalUnitCostForBook( inventories, ThirdPartyCategory.BLACKBOOK_CLEAN_TYPE ) );
}

public void testCreateReportGroupingDescription()
{
	String make = "make";
	String model = "model";
	String trim = "trim";
	String bodyStyle = "bodyStyle";

	assertEquals( "make model trim bodyStyle", InventoryService_Legacy.createGroupingString( make, model, trim, bodyStyle ) );
}

public void testCreateReportGroupingDescriptionEmptyTrim()
{
	String make = "make";
	String model = "model";
	String trim = "";
	String bodyStyle = "bodyStyle";

	assertEquals( "make model bodyStyle", InventoryService_Legacy.createGroupingString( make, model, trim, bodyStyle ) );
}

public void testCreateReportGroupingDescriptionNoTrim()
{
	String make = "make";
	String model = "model";
	String trim = "";
	String bodyStyle = null;

	assertEquals( "make model", InventoryService_Legacy.createGroupingString( make, model, trim, bodyStyle ) );
}

public void testCreateReportGroupingDescriptionNullBodyStyleTrimSpace()
{
	String make = "make";
	String model = "model";
	String trim = "trim ";
	String bodyStyle = null;

	assertEquals( "make model trim", InventoryService_Legacy.createGroupingString( make, model, trim, bodyStyle ) );
}

public void testCreateReportGroupingDescriptionUnknownBodyStyle()
{
	String make = "make";
	String model = "model";
	String trim = "trim";
	String bodyStyle = "UNKNOWN";

	assertEquals( "make model trim", InventoryService_Legacy.createGroupingString( make, model, trim, bodyStyle ) );
}

public void testCreateVehicleWithYearMileageMakeModelUsingVin() throws Exception
{
	int mileage = 1000;
	String year = "2000";
	MakeModelGrouping mmg = ObjectMother.createMakeModelGrouping();
//	mockDatabase.setReturnObjects( mmg );
	MockMakeModelGroupingService mockMakeModelGroupingService = new MockMakeModelGroupingService();
	mockMakeModelGroupingService.setMakeModelGroupingToReturn(mmg);
	inventoryService_Legacy.setMakeModelGroupingService(mockMakeModelGroupingService);

	InventoryEntity vehicle = new InventoryEntity();
	vehicle.setMileageReceived( new Integer( mileage ));
	vehicle.setVehicleYear( Integer.parseInt( year ) );
	vehicle.setMakeModelGroupingId( mmg.getMakeModelGroupingId() );

	InventoryEntity returnedVehicle = inventoryService_Legacy.createEmptyInventoryWithYearMileageMakeModelUsingVin( year, mileage, "", "", "" );

	assertEquals( "year", vehicle.getVehicleYear(), returnedVehicle.getVehicleYear() );
	assertEquals( "mileage", vehicle.getMileageReceived(), returnedVehicle.getMileageReceived() );
	assertEquals( "makemodelGroupingId", vehicle.getMakeModelGroupingId(), returnedVehicle.getMakeModelGroupingId() );
}

public void testCreateVehicleWithYearMileageMakeModelYearNullUsingVin() throws Exception
{
	int mileage = 1000;
	String year = null;

	InventoryEntity inventory = new InventoryEntity();
	inventory.setMileageReceived( new Integer( mileage ));
	inventory.setVehicleYear( 0 );

	MakeModelGrouping makeModelGrouping = ObjectMother.createMakeModelGrouping();
	MockMakeModelGroupingService mockMakeModelGroupingService = new MockMakeModelGroupingService();
	mockMakeModelGroupingService.setMakeModelGroupingToReturn(makeModelGrouping);
	
	inventoryService_Legacy.setMakeModelGroupingService(mockMakeModelGroupingService);
	InventoryEntity returnedInventory = inventoryService_Legacy.createEmptyInventoryWithYearMileageMakeModelUsingVin( year, mileage, "", "", "" );

	assertEquals( "year", inventory.getVehicleYear(), returnedInventory.getVehicleYear() );
	assertEquals( "mileage", inventory.getMileageReceived(), returnedInventory.getMileageReceived() );
}

public void testCreateGuideBookIdStringOnlyOne()
{
	String guideBookIdStr = createGuideBookIdString( new int[] { 1 } );
	assertEquals( "'1'", guideBookIdStr );
}

public void testCreateGuideBookIdString()
{
	String guideBookIdStr = createGuideBookIdString( new int[] { 1, 2, 5, 3 } );
	assertEquals( "'1','2','5','3'", guideBookIdStr );
}

public void testIsRepriceConfirmationNeeded()
{
	MockInventoryPersistence inventoryPersistence = new MockInventoryPersistence();

	InventoryEntity inv1 = new InventoryEntity();
	inv1.setListPrice( 10000.0 );
	inventoryPersistence.add( 1, inv1 );
	
	inventoryService_Legacy.setInventoryPersistence( inventoryPersistence );
	
	assertTrue( inventoryService_Legacy.isRepriceConfirmationNeeded( 1, 11000, 10 ) );
	assertTrue( inventoryService_Legacy.isRepriceConfirmationNeeded( 1, 9000, 10 ) );
	assertTrue( inventoryService_Legacy.isRepriceConfirmationNeeded( 1, 5000, 10 ) );
	assertTrue( inventoryService_Legacy.isRepriceConfirmationNeeded( 1, 15000, 10 ) );
	assertTrue( inventoryService_Legacy.isRepriceConfirmationNeeded( 1, 14000, 10 ) );
	assertFalse( inventoryService_Legacy.isRepriceConfirmationNeeded( 1, 10200, 10 ) );
	assertFalse( inventoryService_Legacy.isRepriceConfirmationNeeded( 1, 9800, 10 ) );
	assertFalse( inventoryService_Legacy.isRepriceConfirmationNeeded( 1, 10999, 10 ) );
	assertFalse( inventoryService_Legacy.isRepriceConfirmationNeeded( 1, 9001, 10 ) );
	
}


private String createGuideBookIdString( int[] guideBookIds )
{
	StringBuffer buffer = new StringBuffer();
	for ( int i = 0; i < guideBookIds.length; i++ )
	{
		buffer.append( "'" );
		buffer.append( guideBookIds[i] );
		if ( i + 1 != guideBookIds.length )
		{
			buffer.append( "'," );
		}
		else
		{
			buffer.append( "'" );
		}
	}
	return buffer.toString();
}

}
