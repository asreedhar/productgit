package com.firstlook.service.inventory;

import javax.servlet.http.HttpServletRequest;

import com.firstlook.entity.InventorySalesAggregate;


/**
 * This class should belong in a common package that has javax.servlet available. This class is duplicated in IMT and private-label.
 * 
 * @author bfung
 * 
 */
public class InventorySalesAggregateHelper
{

public static void putMakeModelCountInRequest( HttpServletRequest request, InventorySalesAggregate aggregateData )
{
    int makeModelCount = aggregateData.getDistinctModelsInStock();

    request.setAttribute( "makeModelTotal", String.valueOf( makeModelCount ) );
}

public static void putTotalDaysSupplyInRequest( HttpServletRequest request, InventorySalesAggregate aggregateData )
{
    int totalDaysSupply = aggregateData.getDaysSupply();

    request.setAttribute( "totalDaysSupply", String.valueOf( totalDaysSupply ) );
}

public static void putDealerTotalDollarsInRequest( HttpServletRequest request, InventorySalesAggregate aggregateData )
{
    int totalDollars = aggregateData.getTotalInventoryDollars();

    request.setAttribute( "makeModelTotalDollars", new Integer( totalDollars ) );
}

public static void putVehicleCountInRequest( HttpServletRequest request, InventorySalesAggregate aggregateData )
{
    int vehicleCount = aggregateData.getUnitsInStock();

    request.setAttribute( "vehicleCount", String.valueOf( vehicleCount ) );
}

public static void putAverageInventoryAgeInRequest( HttpServletRequest request, InventorySalesAggregate aggregateData )
{
    int days = aggregateData.getAvgInventoryAge();

    request.setAttribute( "averageInventoryAge", String.valueOf( days ) );
}

}
