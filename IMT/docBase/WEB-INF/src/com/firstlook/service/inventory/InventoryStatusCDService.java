package com.firstlook.service.inventory;

import java.util.List;

import org.apache.struts.action.ActionForm;

import com.firstlook.action.dealer.reports.InventoryStatusFilterForm;
import com.firstlook.entity.InventoryStatusCD;
import com.firstlook.persistence.inventory.InventoryStatusCDPersistence;

public class InventoryStatusCDService
{

public InventoryStatusCDService()
{
}

public InventoryStatusCD retrieveByInventoryStatusCodeId( int inventoryStatusCodeId )
{
	InventoryStatusCDPersistence statusCodePersistence = new InventoryStatusCDPersistence();
	return statusCodePersistence.findByPk( inventoryStatusCodeId );
}

public List<InventoryStatusCD> retrieveAll()
{
	InventoryStatusCDPersistence statusCodePersistence = new InventoryStatusCDPersistence();
	return statusCodePersistence.findAll();
}

public String[] determineStatusCodes( ActionForm form, String[] statusCodes, String inventoryStatusCdStr )
{
	InventoryStatusFilterForm filterForm = null;
	if ( form != null )
	{
		filterForm = (InventoryStatusFilterForm)form;
		statusCodes = filterForm.getInventoryStatusCD();
	}
	else
	{
		if ( inventoryStatusCdStr != null && !inventoryStatusCdStr.equals( "" ) )
		{
			statusCodes = inventoryStatusCdStr.split( "," );
		}
	}
	return statusCodes;
}

public boolean isStatusCodeAny( String[] statusCodes )
{
	boolean isAny = false;
	for ( int i = 0; i < statusCodes.length && !isAny; i++ )
	{
		if ( statusCodes[i].equals( "-1" ) )
		{
			isAny = true;
		}
	}
	return isAny;
}

public List<InventoryStatusCD> determineStatusList()
{
	InventoryStatusCDService inventoryStatusCDService = new InventoryStatusCDService();
	List<InventoryStatusCD> status = inventoryStatusCDService.retrieveAll();

	InventoryStatusCD anyStatus = new InventoryStatusCD();
	anyStatus.setInventoryStatusCD( new Integer( -1 ) );
	anyStatus.setShortDescription( "" );
	anyStatus.setLongDescription( "ANY" );
	status.add( 0, anyStatus );

	return status;
}

}
