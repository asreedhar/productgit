package com.firstlook.service.report;

import java.sql.Timestamp;

import biz.firstlook.commons.date.BasisPeriod;

import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.report.ReportGroupingRetriever;
import com.firstlook.report.ReportGrouping;
import com.firstlook.service.inventory.InventoryService_Legacy;

public class ReportService
{

private ReportGroupingRetriever reportGroupingRetriever;	
private InventoryService_Legacy inventoryService_Legacy;
public ReportGrouping retrieveReportGrouping( int dealerId, int groupingDescriptionId, int weeks, int mileage, int includeDealerGroup,
												int forecast ) throws ApplicationException
{
	int inventoryType = InventoryEntity.USED_CAR;

	try
	{
		BasisPeriod basisPeriod = new BasisPeriod( weeks * 7, forecast == 1 ? true : false );
		ReportGrouping reportGrouping = reportGroupingRetriever.retrieveReportgrouping( dealerId, new Timestamp( basisPeriod.getStartDate().getTime() ), new Timestamp( basisPeriod.getEndDate().getTime() ),
																			groupingDescriptionId, includeDealerGroup, inventoryType, mileage );

		if ( reportGrouping == null )
		{
			reportGrouping = new ReportGrouping();
			int inventoryCount = inventoryService_Legacy.countInventoriesBy( dealerId, inventoryType, groupingDescriptionId );
			reportGrouping.setUnitsInStock( inventoryCount );
		}

		return reportGrouping;
	}
	catch ( Exception e )
	{
		throw new ApplicationException( "Problem retrieving reportgrouping", e );
	}
}
public void setReportGroupingRetriever(
		ReportGroupingRetriever reportGroupingRetriever) {
	this.reportGroupingRetriever = reportGroupingRetriever;
}

}
