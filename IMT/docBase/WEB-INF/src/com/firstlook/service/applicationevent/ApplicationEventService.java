package com.firstlook.service.applicationevent;

import java.sql.Timestamp;
import java.util.Date;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.ApplicationEvent;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.applicationevent.ApplicationEventPersistence;

public class ApplicationEventService implements IApplicationEventService
{

public ApplicationEventService()
{
    super();
}

public void saveApplicationEvent( Member member, boolean isLogin )
        throws ApplicationException
{
    int eventType = determineEventType(isLogin);
    String eventDescription = determineEventDescription(isLogin);

    ApplicationEvent appEvent = new ApplicationEvent();
    appEvent.setEventType(eventType);
    appEvent.setMemberId(member.getMemberId().intValue());
    appEvent.setEventDescription(member.getLogin() + eventDescription);
    appEvent.setCreateTimestamp(new Timestamp(new Date(System.currentTimeMillis()).getTime() ) );

    try
    {
        ApplicationEventPersistence persistence = new ApplicationEventPersistence();
        persistence.save(appEvent);
    } catch (DatabaseException e)
    {
        throw new ApplicationException(
                "Error saving Login Application Event: ", e);
    }
}

private int determineEventType( boolean isLogin )
{
    if ( isLogin )
    {
        return ApplicationEvent.EVENT_TYPE_LOGIN;
    } else
    {
        return ApplicationEvent.EVENT_TYPE_LOGOUT;
    }
}

private String determineEventDescription( boolean isLogin )
{
    if ( isLogin )
    {
        return " logged in.";
    } else
    {
        return " logged out.";
    }
}

}
