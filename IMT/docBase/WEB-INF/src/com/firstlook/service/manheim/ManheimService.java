package com.firstlook.service.manheim;

import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.methods.GetMethod;

public class ManheimService
{

public ManheimService()
{
	super();
}

public void run()
{
	HttpClient httpClient = new HttpClient();
	HostConfiguration conf = new HostConfiguration();

	conf.setHost( "www.manheim.com", 443, "https" );

	httpClient.setHostConfiguration( conf );

	// Authenticaion
	// http://svn.apache.org/repos/asf/jakarta/commons/proper/httpclient/branches/HTTPCLIENT_2_0_BRANCH/src/examples/BasicAuthenticationExample.java	

	httpClient.getState().setAuthenticationPreemptive( true );
	httpClient.getState().setCredentials( null, null, new UsernamePasswordCredentials( "pstobbs", "firstlook" ) );

	GetMethod method = new GetMethod( "https://www.manheim.com/members/mmr/" );

	method.setDoAuthentication( true );

	int httpStatusCode = 0;
	try
	{
		httpStatusCode = httpClient.executeMethod( httpClient.getHostConfiguration(), method, httpClient.getState() );

		method.releaseConnection();
		Cookie[] cookies = httpClient.getState().getCookies();
		System.out.println( "Present cookies: " );
		for ( int i = 0; i < cookies.length; i++ )
		{
			System.out.println( " - " + cookies[i].toExternalForm() );
		}
		if ( httpStatusCode == 301 )
		{
			Header locationHeader = method.getResponseHeader( "location" );
			if ( locationHeader != null )
			{
				method = new GetMethod( "http://www.manheim.com/members/mmr/" );
				httpStatusCode = httpClient.executeMethod( httpClient.getHostConfiguration(), method, httpClient.getState() );
				cookies = httpClient.getState().getCookies();
				System.out.println( "Present cookies: " );
				for ( int i = 0; i < cookies.length; i++ )
				{
					System.out.println( " - " + cookies[i].toExternalForm() );
				}
			}
			else
			{
				System.out.println( "Manheim Sucks and doesn't know what they're doing." );
				System.exit( 1 );
			}
		}

		String returnString = method.getResponseBodyAsString();

		System.out.println( httpStatusCode );
		System.out.println( returnString );

	}
	catch ( Exception e )
	{
		e.printStackTrace();
	}
	finally {
		method.releaseConnection();
	}
}

public static void main( String[] args )
{
	ManheimService service = new ManheimService();
	service.run();
}

}
