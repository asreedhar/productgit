package com.firstlook.service.vehicle;

import biz.firstlook.transact.persist.model.Vehicle;

public interface IVehicleService
{

public void createNewVehicle( Vehicle vehicle );

public Vehicle loadVehicle( String vin );

public void updateVehicle( Vehicle vehicle );

}
