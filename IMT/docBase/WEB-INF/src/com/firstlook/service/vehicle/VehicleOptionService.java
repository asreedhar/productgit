package com.firstlook.service.vehicle;

import java.util.Collection;

import com.firstlook.persistence.vehicle.VehicleOptionPersistence;

public class VehicleOptionService
{

private VehicleOptionPersistence persistence = new VehicleOptionPersistence();

public VehicleOptionService()
{
    super();
}

public Collection retrieveByVehicleId( int vehicleId )
{
    return persistence.findByVehicleId(vehicleId);
}

}
