package com.firstlook.service.vehicle;

import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.persistence.IVehicleDAO;

public class VehicleService implements IVehicleService
{

private IVehicleDAO vehicleDAO;

public void createNewVehicle( Vehicle vehicle )
{
	vehicleDAO.saveOrUpdate( vehicle );
}

public Vehicle loadVehicle( String vin )
{
	return vehicleDAO.findByVin( vin );
}

public void updateVehicle( Vehicle vehicle )
{
	vehicleDAO.saveOrUpdate( vehicle );
}

//***********  Getters and Setters from here to end of class  *************************
public void setVehicleDAO( IVehicleDAO vehicleDAO )
{
	this.vehicleDAO = vehicleDAO;
}

/**
 * A special utility method. All the Catalog attributes of the vehicleSource parameter- i.e. those 
 * from the catalog that can be unqiuely identified by a VIN and a StyleKey are copied to the vehicleToUpdate.
 * 
 * NOTE: DOES NOT COPY: VIN, STYLEKEY, INTERIOR DESC, INTERIOR COLOR, BASE COLOR, AND CATALOGKEY!!!
 * 
 * @param vehicleToUpdate
 * @param vehicleSource
 */
public static void copyVehicleAttributes(Vehicle vehicleToUpdate, Vehicle vehicleSource) {
	vehicleToUpdate.setVehicleTrim(vehicleSource.getVehicleTrim()); 
	vehicleToUpdate.setMake(vehicleSource.getMake()); 
	vehicleToUpdate.setOriginalMake(vehicleSource.getMake()); 
	vehicleToUpdate.setModel(vehicleSource.getModel()); 
	vehicleToUpdate.setOriginalModel(vehicleSource.getOriginalModel()); 
	vehicleToUpdate.setBodyType(vehicleSource.getBodyType());
	vehicleToUpdate.setVehicleYear(vehicleSource.getVehicleYear()); 
	vehicleToUpdate.setOriginalYear(vehicleSource.getVehicleYear());
	vehicleToUpdate.setSegment(vehicleSource.getSegment()); 
	vehicleToUpdate.setSegmentId(vehicleSource.getSegmentId());
	vehicleToUpdate.setVehicleTrim(vehicleSource.getVehicleTrim()); 
	vehicleToUpdate.setOriginalTrim(vehicleSource.getVehicleTrim()); 
	vehicleToUpdate.setMakeModelGroupingId(vehicleSource.getMakeModelGroupingId());
	vehicleToUpdate.setDoorCount(vehicleSource.getDoorCount());
	vehicleToUpdate.setFuelType(vehicleSource.getFuelType());
	vehicleToUpdate.setVehicleEngine(vehicleSource.getVehicleEngine());
	vehicleToUpdate.setVehicleDriveTrain(vehicleSource.getVehicleDriveTrain());
	vehicleToUpdate.setVehicleTransmission(vehicleSource.getVehicleTransmission());
	vehicleToUpdate.setCylinderCount(vehicleSource.getCylinderCount());
}

}
