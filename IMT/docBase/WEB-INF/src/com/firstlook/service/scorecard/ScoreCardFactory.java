package com.firstlook.service.scorecard;

import java.util.Date;

import org.springframework.context.ApplicationContext;

import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;

import com.firstlook.aet.aggregate.model.VehicleSegment;
import com.firstlook.aet.aggregate.model.VehicleType;
import com.firstlook.entity.AbstractCarScoreCard;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.NewCarScoreCard;
import com.firstlook.entity.UsedCarScoreCard;
import com.firstlook.scorecard.NewAgingScoreCardCalculator;
import com.firstlook.scorecard.OverallScoreCardCalculator;
import com.firstlook.scorecard.PerformanceSummaryScoreCardCalculator;
import com.firstlook.scorecard.PurchasedScoreCardCalculator;
import com.firstlook.scorecard.TradeInScoreCardCalculator;
import com.firstlook.scorecard.UnitSalesScoreCardCalculator;
import com.firstlook.scorecard.UsedAgingScoreCardCalculator;
import com.firstlook.service.vehiclesale.VehicleSaleService;

public class ScoreCardFactory
{

private ApplicationContext applicationContext;
private int dealerId;
private DealerPreferenceDAO dealerPrefDAO;

public ScoreCardFactory( ApplicationContext applicationContext, int dealerId )
{
	this.dealerId = dealerId;
	this.applicationContext = applicationContext;
	dealerPrefDAO = (DealerPreferenceDAO)applicationContext.getBean( "dealerPrefDAO" );
}

public NewCarScoreCard createNewCarScoreCard()
{
	int inventoryType = InventoryEntity.NEW_CAR;
	NewCarScoreCard newCarScoreCard = new NewCarScoreCard();
	newCarScoreCard.setActive( true );

	PerformanceSummaryScoreCardCalculator performanceSummaryCalculator = new PerformanceSummaryScoreCardCalculator( new Date(),
																													dealerId,
																													VehicleType.getType( inventoryType ) );
	populatePerformanceSummaryScoreCardData( newCarScoreCard, performanceSummaryCalculator );

	NewAgingScoreCardCalculator agingCalculator = new NewAgingScoreCardCalculator( new Date(), dealerId );
	populateNewAgingScoreCardData( newCarScoreCard, agingCalculator );

	OverallScoreCardCalculator overallCalculator = new OverallScoreCardCalculator( new Date(), dealerId, VehicleType.getType( inventoryType ) );
	populateOverallScoreCardData( newCarScoreCard, overallCalculator );

	UnitSalesScoreCardCalculator unitSalesCalculator = new UnitSalesScoreCardCalculator( new Date(), dealerId, inventoryType );
	VehicleSaleService vehicleSaleService = (VehicleSaleService)applicationContext.getBean( "vehicleSaleService" );
	unitSalesCalculator.setVehicleSaleService( vehicleSaleService );
	populateUnitSalesScoreCardData( newCarScoreCard, unitSalesCalculator );

	return newCarScoreCard;
}

public UsedCarScoreCard createUsedCarScoreCard()
{
	int inventoryType = InventoryEntity.USED_CAR;
	UsedCarScoreCard usedCarScoreCard = new UsedCarScoreCard();
	usedCarScoreCard.setActive( true );

	OverallScoreCardCalculator overallCalculator = new OverallScoreCardCalculator( new Date(), dealerId, VehicleType.getType( inventoryType ) );
	populateOverallScoreCardData( usedCarScoreCard, overallCalculator );
	usedCarScoreCard.setOverallPercentSellThroughTrend( overallCalculator.retrieveOverallSellThroughTrend() );
	usedCarScoreCard.setOverallPercentSellThrough12Week( overallCalculator.retrieveOverallSellThrough12Week() );

	UsedAgingScoreCardCalculator agingCalculator = new UsedAgingScoreCardCalculator( new Date(), dealerId );
	populateAgingScoreCardData( usedCarScoreCard, agingCalculator );

	PurchasedScoreCardCalculator purchasedCalculator = new PurchasedScoreCardCalculator(	new Date(),
																							dealerId,
																							VehicleType.getType( inventoryType ) );
	populatePurchasedScoreCardData( usedCarScoreCard, purchasedCalculator );

	UnitSalesScoreCardCalculator unitSalesCalculator = new UnitSalesScoreCardCalculator( new Date(), dealerId, inventoryType );
	VehicleSaleService vehicleSaleService = (VehicleSaleService)applicationContext.getBean( "vehicleSaleService" );
	unitSalesCalculator.setVehicleSaleService( vehicleSaleService );
	populateUnitSalesScoreCardData( usedCarScoreCard, unitSalesCalculator );

	TradeInScoreCardCalculator tradeInCalculator = new TradeInScoreCardCalculator( new Date(), dealerId, VehicleType.getType( inventoryType ) );
	populateTradeInScoreCardData( usedCarScoreCard, tradeInCalculator );

	return usedCarScoreCard;
}

private void populateNewAgingScoreCardData( NewCarScoreCard newCarScoreCard, NewAgingScoreCardCalculator agingCalculator )
{
	newCarScoreCard.setAging0To30Current( agingCalculator.retrieveAged0To30DaysCurrent() );
	newCarScoreCard.setAging0To30Prior( agingCalculator.retrieveAged0To30DaysPriorWeek() );
	newCarScoreCard.setAging31To60Current( agingCalculator.retrieveAged31To60DaysCurrent() );
	newCarScoreCard.setAging31To60Prior( agingCalculator.retrieveAged31To60DaysPriorWeek() );
	newCarScoreCard.setAging61To75Current( agingCalculator.retrieveAged61To75DaysCurrent() );
	newCarScoreCard.setAging61To75Prior( agingCalculator.retrieveAged61To75DaysPriorWeek() );
	newCarScoreCard.setAging76To90Current( agingCalculator.retrieveAged76To90DaysCurrent() );
	newCarScoreCard.setAging76To90Prior( agingCalculator.retrieveAged76To90DaysPriorWeek() );
	newCarScoreCard.setAgingOver90Current( agingCalculator.retrieveAgedOver90DaysCurrent() );
	newCarScoreCard.setAgingOver90Prior( agingCalculator.retrieveAgedOver90DaysPriorWeek() );
}

private void populatePerformanceSummaryScoreCardData( NewCarScoreCard newCarScoreCard, PerformanceSummaryScoreCardCalculator calculator )
{
	newCarScoreCard.setCoupeRetailAvgDaysToSaleTrend( calculator.retrieveAvgDaysToSaleTrend( VehicleSegment.COUPE ) );
	newCarScoreCard.setCoupeRetailAvgDaysToSale12Week( calculator.retrieveAvgDaysToSale12Week( VehicleSegment.COUPE ) );
	newCarScoreCard.setCoupeRetailAvgGrossProfitTrend( calculator.retrieveRetailAvgGrossProfitTrend( VehicleSegment.COUPE ) );
	newCarScoreCard.setCoupeRetailAvgGrossProfit12Week( calculator.retrieveRetailAvgGrossProfit12Week( VehicleSegment.COUPE ) );
	newCarScoreCard.setCoupePercentOfSalesTrend( calculator.retrievePercentSalesTrend( VehicleSegment.COUPE ) );
	newCarScoreCard.setCoupePercentOfSales12Week( calculator.retrievePercentSales12Week( VehicleSegment.COUPE ) );

	newCarScoreCard.setSedanRetailAvgDaysToSaleTrend( calculator.retrieveAvgDaysToSaleTrend( VehicleSegment.SEDAN ) );
	newCarScoreCard.setSedanRetailAvgDaysToSale12Week( calculator.retrieveAvgDaysToSale12Week( VehicleSegment.SEDAN ) );
	newCarScoreCard.setSedanRetailAvgGrossProfitTrend( calculator.retrieveRetailAvgGrossProfitTrend( VehicleSegment.SEDAN ) );
	newCarScoreCard.setSedanRetailAvgGrossProfit12Week( calculator.retrieveRetailAvgGrossProfit12Week( VehicleSegment.SEDAN ) );
	newCarScoreCard.setSedanPercentOfSalesTrend( calculator.retrievePercentSalesTrend( VehicleSegment.SEDAN ) );
	newCarScoreCard.setSedanPercentOfSales12Week( calculator.retrievePercentSales12Week( VehicleSegment.SEDAN ) );

	newCarScoreCard.setSuvRetailAvgDaysToSaleTrend( calculator.retrieveAvgDaysToSaleTrend( VehicleSegment.SUV ) );
	newCarScoreCard.setSuvRetailAvgDaysToSale12Week( calculator.retrieveAvgDaysToSale12Week( VehicleSegment.SUV ) );
	newCarScoreCard.setSuvRetailAvgGrossProfitTrend( calculator.retrieveRetailAvgGrossProfitTrend( VehicleSegment.SUV ) );
	newCarScoreCard.setSuvRetailAvgGrossProfit12Week( calculator.retrieveRetailAvgGrossProfit12Week( VehicleSegment.SUV ) );
	newCarScoreCard.setSuvPercentOfSalesTrend( calculator.retrievePercentSalesTrend( VehicleSegment.SUV ) );
	newCarScoreCard.setSuvPercentOfSales12Week( calculator.retrievePercentSales12Week( VehicleSegment.SUV ) );

	newCarScoreCard.setTruckRetailAvgDaysToSaleTrend( calculator.retrieveAvgDaysToSaleTrend( VehicleSegment.TRUCK ) );
	newCarScoreCard.setTruckRetailAvgDaysToSale12Week( calculator.retrieveAvgDaysToSale12Week( VehicleSegment.TRUCK ) );
	newCarScoreCard.setTruckRetailAvgGrossProfitTrend( calculator.retrieveRetailAvgGrossProfitTrend( VehicleSegment.TRUCK ) );
	newCarScoreCard.setTruckRetailAvgGrossProfit12Week( calculator.retrieveRetailAvgGrossProfit12Week( VehicleSegment.TRUCK ) );
	newCarScoreCard.setTruckPercentOfSalesTrend( calculator.retrievePercentSalesTrend( VehicleSegment.TRUCK ) );
	newCarScoreCard.setTruckPercentOfSales12Week( calculator.retrievePercentSales12Week( VehicleSegment.TRUCK ) );

	newCarScoreCard.setVanRetailAvgDaysToSaleTrend( calculator.retrieveAvgDaysToSaleTrend( VehicleSegment.VAN ) );
	newCarScoreCard.setVanRetailAvgDaysToSale12Week( calculator.retrieveAvgDaysToSale12Week( VehicleSegment.VAN ) );
	newCarScoreCard.setVanRetailAvgGrossProfitTrend( calculator.retrieveRetailAvgGrossProfitTrend( VehicleSegment.VAN ) );
	newCarScoreCard.setVanRetailAvgGrossProfit12Week( calculator.retrieveRetailAvgGrossProfit12Week( VehicleSegment.VAN ) );
	newCarScoreCard.setVanPercentOfSalesTrend( calculator.retrievePercentSalesTrend( VehicleSegment.VAN ) );
	newCarScoreCard.setVanPercentOfSales12Week( calculator.retrievePercentSales12Week( VehicleSegment.VAN ) );
}

private void populateTradeInScoreCardData( UsedCarScoreCard usedCarScoreCard, TradeInScoreCardCalculator calculator )
{
	usedCarScoreCard.setTradeInRetailAvgGrossProfitTrend( calculator.retrieveTradeInAverageGrossProfitTrend() );
	usedCarScoreCard.setTradeInRetailAvgGrossProfit12Week( calculator.retrieveTradeInAverageGrossProfit12Week() );
	usedCarScoreCard.setTradeInPercentSellThroughTrend( calculator.retrieveTradeInSellThroughTrend() );
	usedCarScoreCard.setTradeInPercentSellThrough12Week( calculator.retrieveTradeInSellThrough12Week() );
	usedCarScoreCard.setTradeInAvgDaysToSaleTrend( calculator.retrieveTradeInAvgDaysToSaleTrend() );
	usedCarScoreCard.setTradeInAvgDaysToSale12Week( calculator.retrieveTradeInAvgDaysToSale12Weeks() );

	usedCarScoreCard.setTradeInAvgInventoryAgeCurrent( calculator.retrieveAverageInventoryAgeCurrent() );
	usedCarScoreCard.setTradeInAvgInventoryAgePrior( calculator.retrieveAverageInventoryAgePrior() );

	usedCarScoreCard.setWholesaleImmediateAvgGrossProfitTrend( calculator.retrieveImmediateWholesaleAvgGrossProfitTrend() );
	usedCarScoreCard.setWholesaleImmediateAvgGrossProfit12Week( calculator.retrieveImmediateWholesaleAvgGrossProfit12Week() );

	usedCarScoreCard.setWholesaleAgedInventoryAvgGrossProfitTrend( calculator.retrieveAgedInventoryWholesaleAvgGrossProfitTrend() );
	usedCarScoreCard.setWholesaleAgedInventoryAvgGrossProfit12Week( calculator.retrieveAgedInventoryWholesaleAvgGrossProfit12Week() );

	usedCarScoreCard.setTradeInPercentNonWinnersWholesaledTrend( calculator.retrieveTradeInPercentNonWinnersTrend() );
	usedCarScoreCard.setTradeInPercentNonWinnersWholesaled12Week( calculator.retrieveTradeInPercentNonWinners12Week() );
}

private void populateUnitSalesScoreCardData( AbstractCarScoreCard usedCarScoreCard, UnitSalesScoreCardCalculator calculator )
{
	usedCarScoreCard.setOverallUnitSaleCurrent( calculator.retrieveMonthToDateUnitSales() );
	usedCarScoreCard.setOverallUnitSalePrior( calculator.retrieveLastMonthUnitSales() );
	usedCarScoreCard.setOverallUnitSaleThreeMonth( calculator.retrieveThreeMonthUnitSales() );
	usedCarScoreCard.setWeekEnding( calculator.getWeekEndDateZeros() );
	usedCarScoreCard.setWeekNumber( calculator.getWeekNumber() );
}

private void populatePurchasedScoreCardData( UsedCarScoreCard usedCarScoreCard, PurchasedScoreCardCalculator calculator )
{
	usedCarScoreCard.setPurchasedRetailAvgGrossProfitTrend( calculator.retrievePurchasedAverageGrossProfitTrend() );
	usedCarScoreCard.setPurchasedRetailAvgGrossProfit12Week( calculator.retrievePurchasedAverageGrossProfit12Week() );
	usedCarScoreCard.setPurchasedPercentSellThroughTrend( calculator.retrievePurchasedSellThroughTrend() );
	usedCarScoreCard.setPurchasedPercentSellThrough12Week( calculator.retrievePurchasedSellThrough12Week() );
	usedCarScoreCard.setPurchasedAvgDaysToSaleTrend( calculator.retrievePurchasedAvgDaysToSaleTrend() );
	usedCarScoreCard.setPurchasedAvgDaysToSale12Week( calculator.retrievePurchasedAvgDaysToSale12Weeks() );
	usedCarScoreCard.setPurchasedAvgInventoryAgeCurrent( calculator.retrievePurchasedAvgInventoryAgeCurrent() );
	usedCarScoreCard.setPurchasedAvgInventoryAgePrior( calculator.retrievePurchasedAvgInventoryAgePrior() );
	usedCarScoreCard.setPurchasedPercentWinnersTrend( calculator.retrievePurchasedPercentWinnersTrend() );
	usedCarScoreCard.setPurchasedPercentWinners12Week( calculator.retrievePurchasedPercentWinners12Week() );
}

private void populateAgingScoreCardData( UsedCarScoreCard usedCarScoreCard, UsedAgingScoreCardCalculator calculator )
{
	usedCarScoreCard.setAging60PlusCurrent( calculator.retrieveAged60PlusDaysCurrent() );
	usedCarScoreCard.setAging60PlusPrior( calculator.retrieveAged60PlusDaysPriorWeek() );
	usedCarScoreCard.setAging50To59Current( calculator.retrieveAged50To59DaysCurrent() );
	usedCarScoreCard.setAging50To59Prior( calculator.retrieveAged50To59DaysPriorWeek() );
	usedCarScoreCard.setAging40To49Current( calculator.retrieveAged40To49DaysCurrent() );
	usedCarScoreCard.setAging40To49Prior( calculator.retrieveAged40To49DaysPriorWeek() );
	usedCarScoreCard.setAging30To39Current( calculator.retrieveAged30To39DaysCurrent() );
	usedCarScoreCard.setAging30To39Prior( calculator.retrieveAged30To39DaysPriorWeek() );
}

private void populateOverallScoreCardData( AbstractCarScoreCard usedCarScoreCard, OverallScoreCardCalculator calculator )
{
	DealerPreference dealerPreference = dealerPrefDAO.findByBusinessUnitId(dealerId);
	int daysSupply12WeekWeight = dealerPreference.getDaysSupply12WeekWeightAsInt();
	int daysSupply26WeekWeight = dealerPreference.getDaysSupply26WeekWeightAsInt();
	usedCarScoreCard.setOverallCurrentDaysSupplyCurrent( calculator.retrieveAverageDaysSupplyCurrent( daysSupply12WeekWeight,
																										daysSupply26WeekWeight ) );
	usedCarScoreCard.setOverallCurrentDaysSupplyPrior( calculator.retrieveAverageDaysSupplyPrior( daysSupply12WeekWeight,
																									daysSupply26WeekWeight ) );
	usedCarScoreCard.setOverallRetailAvgGrossProfitTrend( calculator.retrieveOverallRetailAverageGrossProfitTrend() );
	usedCarScoreCard.setOverallRetailAvgGrossProfit12Week( calculator.retrieveOverallRetailAverageGrossProfit12Week() );
	usedCarScoreCard.setOverallFAndIAvgGrossProfitTrend( calculator.retrieveOverallBackEndAverageGrossProfitTrend() );
	usedCarScoreCard.setOverallFAndIAvgGrossProfit12Week( calculator.retrieveOverallBackEndAverageGrossProfit12Week() );
	usedCarScoreCard.setOverallAvgDaysToSaleTrend( calculator.retrieveOverallAvgDaysToSaleTrend() );
	usedCarScoreCard.setOverallAvgDaysToSale12Week( calculator.retrieveOverallAvgDaysToSale12Weeks() );
	usedCarScoreCard.setOverallAvgInventoryAgeCurrent( calculator.retrieveOverallAverageInventoryAgeCurrent() );
	usedCarScoreCard.setOverallAvgInventoryAgePrior( calculator.retrieveOverallAverageInventoryAge12Prior() );
}

public ApplicationContext getApplicationContext()
{
	return applicationContext;
}

public void setApplicationContext( ApplicationContext applicationContext )
{
	this.applicationContext = applicationContext;
}

public DealerPreferenceDAO getDealerPrefDAO()
{
	return dealerPrefDAO;
}

public void setDealerPrefDAO( DealerPreferenceDAO dealerPrefDAO )
{
	this.dealerPrefDAO = dealerPrefDAO;
}

}
