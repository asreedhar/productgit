package com.firstlook.service.scorecard;

import java.sql.Timestamp;
import java.util.Date;

import com.firstlook.entity.NewCarScoreCard;
import com.firstlook.entity.UsedCarScoreCard;
import com.firstlook.persistence.scorecard.ScoreCardPersistence;

public class ScoreCardService
{

private ScoreCardPersistence persistence;

public ScoreCardService()
{
    persistence = new ScoreCardPersistence();
}

public void saveOrUpdateUsed( UsedCarScoreCard usedCarScoreCard )
{
    persistence.saveOrUpdateUsed(usedCarScoreCard);
}

public void saveOrUpdateNew( NewCarScoreCard newCarScoreCard )
{
    persistence.saveOrUpdateNew(newCarScoreCard);
}

public NewCarScoreCard retrieveNewCarScoreCard( int dealerId, Date weekEndDate )
{
    NewCarScoreCard newCarScoreCard = persistence.findNewCarBy(dealerId,
            new Timestamp( weekEndDate.getTime() ) );
    if ( newCarScoreCard == null )
    {
        newCarScoreCard = new NewCarScoreCard();
    }

    return newCarScoreCard;
}

public UsedCarScoreCard retrieveUsedCarScoreCard( int dealerId, Date weekEndDate )
{
    UsedCarScoreCard usedCarScoreCard = persistence.findUsedCarBy(dealerId,
            new Timestamp( weekEndDate.getTime() ) );
    if ( usedCarScoreCard == null )
    {
        usedCarScoreCard = new UsedCarScoreCard();
    }

    return usedCarScoreCard;
}

public NewCarScoreCard retrieveActiveNewCarScoreCard( Integer dealerId )
{
    return persistence.findActiveNewCarBy(dealerId);
}

public UsedCarScoreCard retrieveActiveUsedCarScoreCard( Integer dealerId )
{
    return persistence.findActiveUsedCarBy(dealerId);
}

}
