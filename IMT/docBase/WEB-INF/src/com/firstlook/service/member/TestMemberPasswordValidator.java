package com.firstlook.service.member;

import junit.framework.TestCase;
import biz.firstlook.commons.util.security.ShaHexPasswordEncoder;

import com.firstlook.entity.IMember;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;

public class TestMemberPasswordValidator extends TestCase
{

private MemberPasswordValidator passwordValidator;
private ShaHexPasswordEncoder passwordEncoder = new ShaHexPasswordEncoder();
private IMember member;
private Integer memberId = new Integer( 0 );

public TestMemberPasswordValidator( String name )
{
	super( name );
	passwordValidator = new MemberPasswordValidator( passwordEncoder );
}

public void setUp()
{
	member = new Member();
	member.setMemberId( memberId );
}

public void testChangePasswordNoOldMatch() throws ApplicationException
{
	String oldPassword = "test123";
	String newPassword = "demo123";
	String confirmPassword = "demo123";
	member.setPassword( "notOld" );

	try
	{
		passwordValidator.validate( member, oldPassword, newPassword, confirmPassword );
		fail( "Old passwords don't match -- this should have failed!" );
	}
	catch ( MemberServiceException e )
	{
		assertEquals( MemberPasswordValidator.OLD_PASSWORD_DOES_NOT_MATCH_KEY, e.getErrorKey() );
	}
}

public void testChangePasswordNewSameAsOld() throws ApplicationException
{
	String oldPassword = "test123";
	String newPassword = "test123";
	String confirmPassword = "test123";
	member.setPassword( passwordEncoder.encode( oldPassword ) );

	try
	{
		passwordValidator.validate( member, oldPassword, newPassword, confirmPassword );
		fail( "New password is the same as the old -- this should have failed!" );
	}
	catch ( MemberServiceException e )
	{
		assertEquals( MemberPasswordValidator.OLD_PASSWORD_MATCHES_NEW_KEY, e.getErrorKey() );
	}
}

public void testChangePasswordNewNotSameAsConfirm() throws ApplicationException
{
	String oldPassword = "test123";
	String newPassword = "demo123";
	String confirmPassword = "demo456";
	member.setPassword( passwordEncoder.encode( oldPassword ) );

	try
	{
		passwordValidator.validate( member, oldPassword, newPassword, confirmPassword );
		fail( "New password is not the same as the confirm -- this should have failed!" );
	}
	catch ( MemberServiceException e )
	{
		assertEquals( MemberPasswordValidator.PASSWORD_NOT_CONFIRMED_ERROR_MESSAGE_KEY, e.getErrorKey() );
	}
}

public void testChangePasswordOldIsTooSmall() throws ApplicationException
{
	String oldPassword = "old1";
	String newPassword = "demo123";
	String confirmPassword = "demo456";
	member.setPassword( passwordEncoder.encode( oldPassword ) );

	try
	{
		passwordValidator.validate( member, oldPassword, newPassword, confirmPassword );
		fail( "Old password is too small -- this should have failed!" );
	}
	catch ( MemberServiceException e )
	{
		assertEquals( MemberPasswordValidator.PASSWORD_ERROR_MESSAGE_KEY, e.getErrorKey() );
	}
}

public void testChangePasswordValidated()
{
	String oldPassword = "old123";
	String newPassword = "demo123";
	String confirmPassword = "demo123";
	member.setPassword( passwordEncoder.encode( oldPassword ) );

	try
	{
		assertTrue( passwordValidator.validate( member, oldPassword, newPassword, confirmPassword ) );
	}
	catch ( MemberServiceException e )
	{
		fail( e.getMessage() );
	}
}

}
