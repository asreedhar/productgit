package com.firstlook.service.member;

import java.util.Iterator;
import java.util.Set;

import com.firstlook.entity.Role;

public class MemberRoleIdentifier {

    Set roles;
    
    public MemberRoleIdentifier(Set roles) {
        this.roles = roles;
    }
    
    public boolean contains(Role role) {
        Iterator i = roles.iterator();
        while( i.hasNext() ) {
            Role test = (Role) i.next();
            
            if( test.getCode().trim().equals( role.getCode().trim() ) &&
                test.getType().trim().equals( role.getType().trim() ) ) {
                return true;
            }
        }
        return false;
    }
    
    public boolean isUsedNoAccess() {
        return( !contains( Role.USED_APPRAISER ) && !contains( Role.USED_STANDARD ) && !contains( Role.USED_MANAGER ) );
    }

    public boolean isUsedAppraiser() {
        return( contains( Role.USED_APPRAISER ) );
    }
    
    public boolean isUsedStandard() {
        return( contains( Role.USED_STANDARD ) );
    }
    
    public boolean isUsedManager() {
        return( contains( Role.USED_MANAGER ) );
    }
    
    public boolean isNewNoAccess() {
        return( !contains( Role.NEW_STANDARD ) );
    }
    
    public boolean isNewStandard() {
        return( contains( Role.NEW_STANDARD ) ) ;
    }

    public boolean isAdminNone() {
        return( contains( Role.ADMIN_NONE ) ) ;
    }

    public boolean isAdminFull() {
        return( contains( Role.ADMIN_FULL ) ) ;
    }
}
