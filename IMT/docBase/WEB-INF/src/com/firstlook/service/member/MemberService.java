package com.firstlook.service.member;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import biz.firstlook.commons.util.security.GeneratePasswordService;
import biz.firstlook.commons.util.security.PasswordEncoder;
import biz.firstlook.commons.util.security.ShaHexPasswordEncoder;
import biz.firstlook.transact.persist.model.Credential;
import biz.firstlook.transact.persist.model.CredentialType;
import biz.firstlook.transact.persist.model.JobTitle;

import com.firstlook.entity.IMember;
import com.firstlook.entity.Member;
import com.firstlook.persistence.member.IMemberDAO;

public class MemberService implements IMemberService
{

private IMemberDAO memberDAO;

public static Credential getCredential( Set<Credential> credentials, CredentialType credentialType )
{
	Credential result = null;
	if ( credentials != null )
	{
		Iterator<Credential> credentialsItr = credentials.iterator();
		while ( credentialsItr.hasNext() )
		{
			Credential credential = credentialsItr.next();
			if ( credential.getCredentialTypeId().intValue() == credentialType.getId().intValue() )
			{
				result = credential;
				break;
			}
		}
	}
	return result;
}

public List<JobTitle> allJobTitles()
{
	return memberDAO.allJobTitles();
}

public Member retrieveMember( Integer memberId )
{
	return memberDAO.findByPk( memberId );
}

public List< Member > getMembersByBusinessUnitAndJobTitle( Integer dealerId, int jobTitleId )
{
	return memberDAO.findByBusinessUnitIdAndJobTitleId(dealerId, jobTitleId );
}

public Member retrieveMember( String login )
{
	return memberDAO.findByLogin( login );
}

public void saveOrUpdate( IMember member )
{
	memberDAO.saveOrUpdate( member );	
}

public String issueNewPassword( IMember member ) throws MemberServiceException
{
	GeneratePasswordService generatePassword = new GeneratePasswordService();
	String newPassword = generatePassword.generate();
	member.setPassword( getMemberPasswordEncoder().encode( newPassword ) );
	member.setLoginStatus( Member.MEMBER_LOGIN_STATUS_NEW );
	memberDAO.saveOrUpdate( member );
	return newPassword;
}

public void changePassword( IMember member, String newPassword, String confirmPassword ) throws MemberServiceException
{
	PasswordEncoder encoder = getMemberPasswordEncoder();
	MemberPasswordValidator validator = new MemberPasswordValidator( encoder );
	validator.validate( newPassword, confirmPassword );
	member.setPassword( encoder.encode( newPassword ) );
	member.setLoginStatus( Member.MEMBER_LOGIN_STATUS_OLD );
	memberDAO.saveOrUpdate( member );
}

public void changePassword( IMember member, String oldPassword, String newPassword, String confirmPassword ) throws MemberServiceException
{
	PasswordEncoder encoder = getMemberPasswordEncoder();
	MemberPasswordValidator validator = new MemberPasswordValidator( encoder );
	validator.validate( member, oldPassword, newPassword, confirmPassword );
	member.setPassword( encoder.encode( newPassword ) );
	member.setLoginStatus( Member.MEMBER_LOGIN_STATUS_OLD );
	memberDAO.saveOrUpdate( member );
}

public PasswordEncoder getMemberPasswordEncoder()
{
	return new ShaHexPasswordEncoder();
}

public void setMemberDAO( IMemberDAO memberDAO )
{
	this.memberDAO = memberDAO;
}

}