package com.firstlook.service.member;

import java.util.Iterator;
import java.util.Set;

import com.firstlook.entity.Member;
import com.firstlook.entity.Role;
import com.firstlook.persistence.member.RolePersistence;

public class RoleService {
	
	private RoleService() {
		//static methods only.
	}

    public static Role getTypeRole(Member member, String type) {
       Set<Role> roles = member.getRoles();
       if( roles != null ) {
           Iterator<Role> roleIter = roles.iterator();
           while( roleIter.hasNext() ) {
               Role role = roleIter.next();
               if(role.getType() !=null &&  role.getType().equalsIgnoreCase( type ) ) {
                   return role;
               }
           }
       }
       return null;
    }
    
    public static Role getUsedRole(Member member) {
        return getTypeRole( member, RolePersistence.USED_TYPE );
    }
    
    public static Role getNewRole(Member member) {
        return getTypeRole( member, RolePersistence.NEW_TYPE );
    }
    
    public static Role getAdminRole(Member member) {
        return getTypeRole( member, RolePersistence.ADMIN_TYPE );
    }
}
