package com.firstlook.service.member;

import java.util.List;

import biz.firstlook.commons.util.security.PasswordEncoder;
import biz.firstlook.transact.persist.model.JobTitle;

import com.firstlook.entity.IMember;
import com.firstlook.entity.Member;

public interface IMemberService
{

public abstract List<JobTitle> allJobTitles();

public Member retrieveMember( Integer memberId );

public Member retrieveMember( String login );

public void saveOrUpdate( IMember member );

//the below are service methods.  The above methods need to be audited. -bf.
public String issueNewPassword( IMember member ) throws MemberServiceException;

public void changePassword( IMember member, String newPassword, String confirmPassword ) throws MemberServiceException;

public void changePassword( IMember member, String oldPassword, String newPassword, String confirmPassword ) throws MemberServiceException;

/**
 * A Factory method to return the PasswordEncoder used by this service.
 * @return an instance of the PasswordEncoder used by this service.
 */
public PasswordEncoder getMemberPasswordEncoder();

public List< Member > getMembersByBusinessUnitAndJobTitle( Integer dealerId, int jobTitleId );

}