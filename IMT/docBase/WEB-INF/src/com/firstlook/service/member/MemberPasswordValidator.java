package com.firstlook.service.member;

import biz.firstlook.commons.util.security.PasswordEncoder;

import com.firstlook.entity.IMember;

class MemberPasswordValidator
{
static final String OLD_PASSWORD_MATCHES_NEW_KEY = "error.admin.member.oldpasswordmatchnew";
static final String OLD_PASSWORD_DOES_NOT_MATCH_KEY = "error.admin.member.oldpassword";
static final String PASSWORD_ERROR_MESSAGE_KEY = "error.admin.member.password";
static final String PASSWORD_NOT_CONFIRMED_ERROR_MESSAGE_KEY = "error.admin.member.passwordnotconfirmed";
// static final String PASSWORDCONFIRM_ERROR_MESSAGE_KEY = "error.admin.member.passwordconfirm";
static final int PASSWORD_MIN_LENGTH = 6;
static final int PASSWORD_MAX_LENGTH = 80;

private PasswordEncoder encoder;

public MemberPasswordValidator( PasswordEncoder passwordEncoder )
{
	this.encoder = passwordEncoder;
}

/**
 * Used to confirm new passwords through the system.
 * @param newPassword
 * @param confirmPassword
 * @return
 * @throws MemberServiceException
 */
public boolean validate( String newPassword, String confirmPassword ) throws MemberServiceException
{
	return validate( newPassword )
			&& assertion( newPassword, confirmPassword, true,
							new MemberServiceException( "Unable to change password.  New password does not match confirm password.",
														PASSWORD_NOT_CONFIRMED_ERROR_MESSAGE_KEY ) );
}

/**
 * Used when a member actively changes their password.
 * 
 * @param member
 * @param oldPassword
 * @param newPassword
 * @param confirmPassword
 * @return
 * @throws MemberServiceException
 */
public boolean validate( IMember member, String oldPassword, String newPassword, String confirmPassword ) throws MemberServiceException
{
	validate( oldPassword );

	assertion( encoder.encode( oldPassword ), member.getPassword(), true,
				new MemberServiceException( "Unable to change password.  Old passwords do not match.", OLD_PASSWORD_DOES_NOT_MATCH_KEY ) );
	assertion( encoder.encode( newPassword ), member.getPassword(), false,
				new MemberServiceException( "Unable to change password.  New password matches old.", OLD_PASSWORD_MATCHES_NEW_KEY ) );
	validate( newPassword, confirmPassword );

	return true;
}

private boolean validate( String password ) throws MemberServiceException
{
	if ( password == null || password.equals( "" ) || password.length() < PASSWORD_MIN_LENGTH || password.length() > PASSWORD_MAX_LENGTH )
	{
		throw new MemberServiceException( "Unable to change password.  Password is not valid.", PASSWORD_ERROR_MESSAGE_KEY );
	}
	return true;
}

private boolean assertion( String password1, String password2, boolean expected, MemberServiceException exception )
		throws MemberServiceException
{
	if ( password1.equals( password2 ) != expected )
		throw exception;
	else
		return true;
}

}
