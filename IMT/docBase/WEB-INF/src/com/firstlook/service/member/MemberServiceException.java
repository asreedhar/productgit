package com.firstlook.service.member;

import com.firstlook.exception.ApplicationException;

public class MemberServiceException extends ApplicationException
{

private static final long serialVersionUID = -279420265988403198L;
private String errorKey;

public MemberServiceException( String message, String errorKey )
{
    super(message);
    this.errorKey = errorKey;
}

public String getErrorKey()
{
    return errorKey;
}

public void setErrorKey( String key )
{
    errorKey = key;
}


}
