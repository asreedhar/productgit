package com.firstlook.service.businessunit;

import java.util.Collection;

import com.firstlook.entity.BusinessUnitRelationship;
import com.firstlook.persistence.businessunit.BusinessUnitRelationshipDAO;
import com.firstlook.persistence.businessunit.IBusinessUnitRelationshipDAO;

public class BusinessUnitRelationshipService
{

private IBusinessUnitRelationshipDAO businessUnitRelationshipDAO;

/**
 * @deprecated - Spring refactoring
 */
public BusinessUnitRelationshipService()
{
	businessUnitRelationshipDAO = new BusinessUnitRelationshipDAO();
}

public BusinessUnitRelationshipService( IBusinessUnitRelationshipDAO burDAO )
{
	businessUnitRelationshipDAO = burDAO;
}

public BusinessUnitRelationship retrieveByBusinessUnitId( Integer businessUnitId )
{
    return businessUnitRelationshipDAO.findByBusinessUnitId(businessUnitId);
}

public BusinessUnitRelationship updateParentIdForBusinessUnitId( int childId,
        int newParentId )
{
    return businessUnitRelationshipDAO.updateParentIdForBusinessUnitId(childId, newParentId);
}

public BusinessUnitRelationship associateBusinessUnits( int parentId,
        int childId )
{
    return businessUnitRelationshipDAO.associateBusinessUnits(parentId, childId);
}

public Collection retrieveIdsByParentId( Integer parentId )
{
    return businessUnitRelationshipDAO.findIdsByParentId( parentId );
}

public Collection retrieveByParentId( Integer parentId )
{
    return businessUnitRelationshipDAO.findByParentId(parentId);
}


}
