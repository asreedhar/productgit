package com.firstlook.service.businessunit;

import java.util.Collection;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.BusinessUnitCode;
import com.firstlook.entity.CodeGenerator;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.businessunit.BusinessUnitCodePersistence;
import com.firstlook.persistence.businessunit.IBusinessUnitCodePersistence;

public class BusinessUnitCodeService
{

private IBusinessUnitCodePersistence persistence;

public BusinessUnitCodeService()
{
    persistence = new BusinessUnitCodePersistence();
}

public BusinessUnitCodeService(
        IBusinessUnitCodePersistence ibusinessUnitPersistence )
{
    persistence = ibusinessUnitPersistence;
}

public Collection retrieveByBusinessUnitCode( String name )
        throws ApplicationException
{
    try
    {
        return persistence.findBusinessUnitCode(name);
    } catch (DatabaseException e)
    {
        throw new ApplicationException("Problem retrieving businessUnitcode", e);
    }
}

public String createBusinessUnitCode( String name ) throws ApplicationException
{
    String codePrefix = CodeGenerator.getCodePrefix(name,
            BusinessUnitCode.BUSINESSUNIT_CODEPREFIX_SIZE);
    Collection businessUnitCodes = retrieveByBusinessUnitCode(codePrefix);
    String codeSuffix = "01";
    if ( !businessUnitCodes.isEmpty() )
    {
        String lastBusinessUnitCode = ((BusinessUnitCode) businessUnitCodes
                .iterator().next()).getBusinessUnitCode();
        codeSuffix = CodeGenerator.getNextCodeSuffix(
                BusinessUnitCode.BUSINESSUNIT_CODEPREFIX_SIZE,
                lastBusinessUnitCode, 2);
    }

    return codePrefix + codeSuffix;
}

}
