package com.firstlook.service.scorecardtargets;

import org.apache.struts.action.ActionErrors;

import com.firstlook.entity.Target;
import com.firstlook.mock.BaseTestCase;

public class TestScoreCardTargetService extends BaseTestCase
{

private ScorecardTargetService service;
private ActionErrors errors;
private Target target;

public TestScoreCardTargetService( String arg1 )
{
    super(arg1);
}

public void setup()
{
    service = new ScorecardTargetService();
    errors = new ActionErrors();

    target = new Target();
    target.setMax(new Integer(100));
    target.setMin(new Integer(0));
}

public void testValidateMinAndMaxNoErrors()
{
    int value = 50;

    assertTrue(!service.validateMinAndMax(errors, target, value));
    assertTrue(!isInActionErrors(errors, "error.target.lessthan"));
    assertTrue(!isInActionErrors(errors, "error.target.greaterthan"));
}

public void testValidateMinAndMaxLessThan()
{
    int value = -1;

    assertTrue(service.validateMinAndMax(errors, target, value));
    assertTrue(!isInActionErrors(errors, "error.target.lessthan"));
    assertTrue(isInActionErrors(errors, "error.target.greaterthan"));
}

public void testValidateMinAndMaxGreaterThan()
{
    int value = 101;

    assertTrue(service.validateMinAndMax(errors, target, value));
    assertTrue(isInActionErrors(errors, "error.target.lessthan"));
    assertTrue(!isInActionErrors(errors, "error.target.greaterthan"));
}
}
