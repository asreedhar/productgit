package com.firstlook.service.pricepoints;

public class UnitCostPointArguments extends AbstractUnitCostPointArguments
{

public int groupingDescriptionId;
public int mileage;

public UnitCostPointArguments()
{
	super();
}

public int getGroupingDescriptionId()
{
	return groupingDescriptionId;
}

public int getMileage()
{
	return mileage;
}

public void setGroupingDescriptionId( int i )
{
	groupingDescriptionId = i;
}

public void setMileage( int i )
{
	mileage = i;
}

}
