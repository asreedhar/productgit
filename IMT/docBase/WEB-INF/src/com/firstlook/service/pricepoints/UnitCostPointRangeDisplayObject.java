package com.firstlook.service.pricepoints;

public class UnitCostPointRangeDisplayObject
{

private int lower;
private int upper;

public UnitCostPointRangeDisplayObject()
{
    super();
}

public UnitCostPointRangeDisplayObject( int lower, int upper )
{
    this.lower = lower;
    this.upper = upper;
}

public int getLower()
{
    return lower;
}

public int getUpper()
{
    return upper;
}

public void setLower( int i )
{
    lower = i;
}

public void setUpper( int i )
{
    upper = i;
}

public String getUnitCostRangeString()
{
	int lower = getLower();
	int upper = getUpper();
	if ( lower <= 0 )
	{
		return ( "Under $" + upper );
	}
	else if ( upper == UnitCostPointService.HIGH_PRICE_POINT_VALUE )
	{
		if ( lower == Integer.MIN_VALUE )
		{
			return "Model ";
		}
		else
		{
			return ( "Over $" + lower );
		}
	}
	else
	{
		return ( "$" + lower + " - $" + upper );
	}
}

}
