package com.firstlook.service.pricepoints;

public class UnitCostPointNewCarArguments extends AbstractUnitCostPointArguments
{

public String make;
public String model;
public int bodyTypeId;
public String trim;

public UnitCostPointNewCarArguments()
{
	super();
}

public String getTrim()
{
	return trim;
}

public int getBodyTypeId()
{
	return bodyTypeId;
}

public void setTrim( String string )
{
	trim = string;
}

public void setBodyTypeId( int i )
{
	bodyTypeId = i;
}

public String getMake()
{
	return make;
}

public String getModel()
{
	return model;
}

public void setMake( String string )
{
	make = string;
}

public void setModel( String string )
{
	model = string;
}

}
