package com.firstlook.service.tradeanalyzer;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.TradeAnalyzerEvent;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.tradeanalyzer.ITradeAnalyzerEventPersistence;
import com.firstlook.persistence.tradeanalyzer.TradeAnalyzerEventPersistence;

public class TradeAnalyzerEventService
{
private ITradeAnalyzerEventPersistence tradeAnalyzerEventPersistence;

public TradeAnalyzerEventService()
{
    tradeAnalyzerEventPersistence = new TradeAnalyzerEventPersistence();
}

public TradeAnalyzerEventService( ITradeAnalyzerEventPersistence persistence )
{
    tradeAnalyzerEventPersistence = persistence;
}

public TradeAnalyzerEvent retrieveByPK( Integer tradeAnalyzerEventId )
        throws ApplicationException
{
    try
    {
        return tradeAnalyzerEventPersistence.findByPk(tradeAnalyzerEventId);
    } catch (DatabaseException de)
    {
        throw new ApplicationException(
                "Unable to retreive TradeAnalyzerEvent by pk", de);
    }
}

public void store( TradeAnalyzerEvent tradeAnalyzerEvent )
        throws ApplicationException
{
    try
    {
        tradeAnalyzerEventPersistence.save(tradeAnalyzerEvent);
    } catch (DatabaseException de)
    {
        throw new ApplicationException("Unable to store TradeAnalyzerEvent ",
                de);
    }
}

public void delete( TradeAnalyzerEvent tradeAnalyzerEvent )
        throws ApplicationException
{
    try
    {
        tradeAnalyzerEventPersistence.delete(tradeAnalyzerEvent);
    } catch (DatabaseException de)
    {
        throw new ApplicationException("Unable to delete TradeAnalyzerEvent ",
                de);
    }
}
}
