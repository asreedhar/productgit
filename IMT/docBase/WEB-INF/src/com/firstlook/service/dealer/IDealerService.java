package com.firstlook.service.dealer;

import java.util.Collection;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import biz.firstlook.transact.persist.model.BusinessUnitCredential;
import biz.firstlook.transact.persist.model.CredentialType;
import biz.firstlook.transact.persist.model.DemandDealer;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerUpgrade;
import com.firstlook.exception.ApplicationException;

public interface IDealerService
{
public void save( Dealer dealer );

public void saveOrUpdate( Dealer dealer);

public void saveOrUpdateBusinessUnitCredential( BusinessUnitCredential businessUnitCredential);

public void delete( Dealer dealer );

public boolean hasCIAUpgrade( Collection<DealerUpgrade> dealerUpgrades );

public boolean hasAgingPlanUpgrade( Collection<DealerUpgrade> dealerUpgrades );

public boolean hasAppraisalUpgrade( Collection<DealerUpgrade> dealerUpgrades );

public boolean hasRedistributionUpgrade( Collection<DealerUpgrade> dealerUpgrades );

public boolean hasAuctionUpgrade( Collection<DealerUpgrade> dealerUpgrades );

public boolean hasAnnualRoiUpgrade( Collection<DealerUpgrade> dealerUpgrades );

public boolean hasEquityAnalyzerUpgrade( Collection<DealerUpgrade> dealerUpgrades );

public boolean hasAppraisalLockoutUpgrade( Collection<DealerUpgrade> dealerUpgrades );

public boolean hasKBBTradeInUpgrade( Collection<DealerUpgrade> dealerUpgrades );

public boolean hasPingUpgrade( Collection<DealerUpgrade> dealerUpgrades );

public boolean hasPingIIUpgrade( Collection<DealerUpgrade> dealerUpgrades );

public boolean hasEdmundsTmvUpgrade( Collection<DealerUpgrade> dealerUpgrades );

public boolean hasJDPowerUCMDataUpgrade(Collection<DealerUpgrade> dealerUpgrades);

public boolean hasMarketUpgrade( Collection<DealerUpgrade> dealerUpgrades );

public boolean hasNADAValuesUpgrade(Collection<DealerUpgrade> dealerUpgrades);

public boolean hasMakeADealUpgrade( Collection<DealerUpgrade> dealerUpgrades );

public boolean hasMarketStockingGuideUpgrade( Collection<DealerUpgrade> dealerUpgrades );

public boolean hasPerformanceDashboardUpgrade( Collection<DealerUpgrade> dealerUpgrades );

public boolean hasWindowStickerUpgrade( Collection<DealerUpgrade> dealerUpgrades );

public boolean hasFirstlook3_0Upgrade (Collection<DealerUpgrade> dealerUpgrades);

public Collection<DealerUpgrade> findUpgrades( int dealerId ) throws ApplicationException;

public void saveUpgrades( Collection<DealerUpgrade> upgrades ) throws ApplicationException;

public void createDefaultInsightUpgrades( Dealer dealer ) throws ApplicationException;

@Transactional(readOnly=true)
public Dealer retrieveDealer( int dealerId );

public BusinessUnitCredential retrieveBusinessUnitCredential( int businessUnitId, CredentialType credentialType );

public List<Dealer> retrieveByDealerCode( String businessUnitCode );

public List findInGroupDealersWithRedistribution( int dealerGroupId ) throws ApplicationException;

public Collection retrieveActiveDealersRunDayToday() throws ApplicationException;

public List retrieveActiveDealers();

public boolean isInsight( Dealer dealer );

public boolean isVIP( Dealer dealer );

public boolean isFirstlook( Dealer dealer );

public Collection retrieveMembers( int dealerId );

public Integer determineDemandDealerSortOrder(Integer businessUnitID, Integer sortByUnderstock, Integer sortByROI);

public List<DemandDealer> retrieveDemandDealers( Integer dealerId, Integer groupingId, Integer mileage, Integer year, Integer distance, Integer sortByROI ) throws ApplicationException;

public void populateDealerWithDefaults( Dealer dealer ) throws DatabaseException, ApplicationException;

public Collection retrieveDealersWithNoDRT();

public Collection retrieveByDealerCodeMultiple( String partialDealerCode );

public Collection retrieveByName( String name );

public List retrieveByDealerIds( Collection dealerIds );

public Collection retrieveByDealerGroupIdAndNameAndNickName( Collection dealerGroupCol, String name, String nickName );

public Collection<Dealer> retrieveByDealerGroupId( int dealerGroupId );

public void updateInventoryBookoutLock( Integer businessUnitId );

public boolean showLithiaRoi( int businessUnitId );

public String retrieveDealerLogo( Integer businessUnitId );
}