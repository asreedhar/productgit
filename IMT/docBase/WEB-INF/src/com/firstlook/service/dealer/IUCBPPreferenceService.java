package com.firstlook.service.dealer;

import java.util.Calendar;
import java.util.Collection;

import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import biz.firstlook.transact.persist.model.CIACompositeTimePeriod;
import biz.firstlook.transact.persist.model.CIAPreferences;

import com.firstlook.entity.DealerRisk;
import com.firstlook.entity.IDealerRisk;
import com.firstlook.exception.ApplicationException;

public interface IUCBPPreferenceService
{

public DealerRisk retrieveDealerRisk( int dealerId );

public CIAPreferences retrieveCIAPreferences( int dealerId );

public int determineRiskLevelYearOffset( int riskLevelYearRollOverMonth, int riskLevelYearInitialTimePeriod,
                                        int riskLevelYearSecondaryTimePeriod, Calendar today );

public void updateUCBPPreferences( DealerRisk dealerRisk, CIAPreferences ciaPreference ) throws ApplicationException;

public Collection retrieveCIATimePeriods();

public CIAPreferences createDefaultCIAPreference( int dealerId );

public void updateCIAPreference( CIAPreferences ciaPreference );

@Transactional( rollbackFor = DataAccessException.class )
public void updateDealerRisk( DealerRisk dealerRisk );

public IDealerRisk createDefaultDealerRisk( int dealerId );

public CIACompositeTimePeriod retrieveCIACompositeTimePeriod( Integer ciaCompositeTimePeriodId );

public Collection retrieveCIACompositeTimePeriod();

}
