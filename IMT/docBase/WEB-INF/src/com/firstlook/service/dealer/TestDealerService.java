package com.firstlook.service.dealer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerUpgrade;
import com.firstlook.entity.DealerUpgradeLookup;
import com.firstlook.exception.ApplicationException;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.persistence.dealer.MockDealerDAO;

public class TestDealerService extends BaseTestCase
{
private MockDealerDAO persistence;
private IDealerService dealerService;
private int dealerId = 1; // this doesn't matter

public TestDealerService( String arg0 )
{
    super( arg0 );
}

public void setup()
{
    persistence = new MockDealerDAO( mockDatabase );
    dealerService = new DealerService( persistence );
}

public void testHasCIAUpgrade() throws ApplicationException
{
    List<DealerUpgrade> dealerUpgrades = new ArrayList<DealerUpgrade>();
    DealerUpgrade ciaUpgrade = createUpgrade( true, DealerUpgradeLookup.CIA.getCode() );
    DealerUpgrade apUpgrade = createUpgrade( false, DealerUpgradeLookup.AGING_PLAN.getCode() );
    DealerUpgrade redistUpgrade = createUpgrade( true, DealerUpgradeLookup.REDISTRIBUTION.getCode() );
    DealerUpgrade taUpgrade = createUpgrade( false, DealerUpgradeLookup.APPRAISAL.getCode() );
    dealerUpgrades.add( ciaUpgrade );
    dealerUpgrades.add( apUpgrade );
    dealerUpgrades.add( redistUpgrade );
    dealerUpgrades.add( taUpgrade );
    persistence.setDealerUpgrades( dealerUpgrades );

    assertTrue( dealerService.hasCIAUpgrade( dealerUpgrades ) );
}

public void testHasNoCIAUpgrade() throws ApplicationException
{
    List<DealerUpgrade> dealerUpgrades = new ArrayList<DealerUpgrade>();
    DealerUpgrade apUpgrade = createUpgrade( false, DealerUpgradeLookup.AGING_PLAN.getCode() );
    DealerUpgrade redistUpgrade = createUpgrade( true, DealerUpgradeLookup.REDISTRIBUTION.getCode() );
    DealerUpgrade taUpgrade = createUpgrade( false, DealerUpgradeLookup.APPRAISAL.getCode() );
    dealerUpgrades.add( apUpgrade );
    dealerUpgrades.add( redistUpgrade );
    dealerUpgrades.add( taUpgrade );
    persistence.setDealerUpgrades( dealerUpgrades );

    assertTrue( !dealerService.hasCIAUpgrade( dealerUpgrades ) );
}

public void testHasCIAUpgradeNotActive() throws ApplicationException
{
    List<DealerUpgrade> dealerUpgrades = new ArrayList<DealerUpgrade>();
    DealerUpgrade ciaUpgrade = createUpgrade( false, DealerUpgradeLookup.CIA.getCode() );
    DealerUpgrade apUpgrade = createUpgrade( false, DealerUpgradeLookup.AGING_PLAN.getCode() );
    DealerUpgrade redistUpgrade = createUpgrade( true, DealerUpgradeLookup.REDISTRIBUTION.getCode() );
    DealerUpgrade taUpgrade = createUpgrade( false, DealerUpgradeLookup.APPRAISAL.getCode() );
    dealerUpgrades.add( ciaUpgrade );
    dealerUpgrades.add( apUpgrade );
    dealerUpgrades.add( redistUpgrade );
    dealerUpgrades.add( taUpgrade );
    persistence.setDealerUpgrades( dealerUpgrades );

    assertTrue( !dealerService.hasCIAUpgrade( dealerUpgrades ) );
}

public void testHasAgingPlanUpgrade() throws ApplicationException
{
    List<DealerUpgrade> dealerUpgrades = new ArrayList<DealerUpgrade>();
    DealerUpgrade ciaUpgrade = createUpgrade( false, DealerUpgradeLookup.CIA.getCode() );
    DealerUpgrade apUpgrade = createUpgrade( true, DealerUpgradeLookup.AGING_PLAN.getCode() );
    DealerUpgrade redistUpgrade = createUpgrade( true, DealerUpgradeLookup.REDISTRIBUTION.getCode() );
    DealerUpgrade taUpgrade = createUpgrade( false, DealerUpgradeLookup.APPRAISAL.getCode() );
    dealerUpgrades.add( ciaUpgrade );
    dealerUpgrades.add( apUpgrade );
    dealerUpgrades.add( redistUpgrade );
    dealerUpgrades.add( taUpgrade );
    persistence.setDealerUpgrades( dealerUpgrades );

    assertTrue( dealerService.hasAgingPlanUpgrade( dealerUpgrades ) );
}

public void testHasNoAgingPlanUpgrade() throws ApplicationException
{
    List<DealerUpgrade> dealerUpgrades = new ArrayList<DealerUpgrade>();
    DealerUpgrade ciaUpgrade = createUpgrade( false, DealerUpgradeLookup.CIA.getCode() );
    DealerUpgrade redistUpgrade = createUpgrade( true, DealerUpgradeLookup.REDISTRIBUTION.getCode() );
    DealerUpgrade taUpgrade = createUpgrade( false, DealerUpgradeLookup.APPRAISAL.getCode() );
    dealerUpgrades.add( ciaUpgrade );
    dealerUpgrades.add( redistUpgrade );
    dealerUpgrades.add( taUpgrade );
    persistence.setDealerUpgrades( dealerUpgrades );

    assertTrue( !dealerService.hasAgingPlanUpgrade( dealerUpgrades ) );
}

public void testHasAgingPlanUpgradeNotActive() throws ApplicationException
{
    List<DealerUpgrade> dealerUpgrades = new ArrayList<DealerUpgrade>();
    DealerUpgrade ciaUpgrade = createUpgrade( false, DealerUpgradeLookup.CIA.getCode() );
    DealerUpgrade apUpgrade = createUpgrade( false, DealerUpgradeLookup.AGING_PLAN.getCode() );
    DealerUpgrade redistUpgrade = createUpgrade( true, DealerUpgradeLookup.REDISTRIBUTION.getCode() );
    DealerUpgrade taUpgrade = createUpgrade( false, DealerUpgradeLookup.APPRAISAL.getCode() );
    dealerUpgrades.add( ciaUpgrade );
    dealerUpgrades.add( apUpgrade );
    dealerUpgrades.add( redistUpgrade );
    dealerUpgrades.add( taUpgrade );
    persistence.setDealerUpgrades( dealerUpgrades );

    assertTrue( !dealerService.hasAgingPlanUpgrade( dealerUpgrades ) );
}

public void testHasTradeAnalyzerUpgrade() throws ApplicationException
{
    List<DealerUpgrade> dealerUpgrades = new ArrayList<DealerUpgrade>();
    DealerUpgrade ciaUpgrade = createUpgrade( false, DealerUpgradeLookup.CIA.getCode() );
    DealerUpgrade apUpgrade = createUpgrade( true, DealerUpgradeLookup.AGING_PLAN.getCode() );
    DealerUpgrade redistUpgrade = createUpgrade( true, DealerUpgradeLookup.REDISTRIBUTION.getCode() );
    DealerUpgrade taUpgrade = createUpgrade( true, DealerUpgradeLookup.APPRAISAL.getCode() );
    dealerUpgrades.add( ciaUpgrade );
    dealerUpgrades.add( apUpgrade );
    dealerUpgrades.add( redistUpgrade );
    dealerUpgrades.add( taUpgrade );
    persistence.setDealerUpgrades( dealerUpgrades );

    assertTrue( dealerService.hasAppraisalUpgrade( dealerUpgrades ) );
}

public void testHasNoTradeAnalyzerUpgrade() throws ApplicationException
{
    List<DealerUpgrade> dealerUpgrades = new ArrayList<DealerUpgrade>();
    DealerUpgrade ciaUpgrade = createUpgrade( false, DealerUpgradeLookup.CIA.getCode() );
    DealerUpgrade redistUpgrade = createUpgrade( true, DealerUpgradeLookup.REDISTRIBUTION.getCode() );
    DealerUpgrade apUpgrade = createUpgrade( false, DealerUpgradeLookup.AGING_PLAN.getCode() );
    dealerUpgrades.add( ciaUpgrade );
    dealerUpgrades.add( redistUpgrade );
    dealerUpgrades.add( apUpgrade );
    persistence.setDealerUpgrades( dealerUpgrades );

    assertTrue( !dealerService.hasAppraisalUpgrade( dealerUpgrades ) );
}

public void testHasTradeAnalyzerNotActive() throws ApplicationException
{
    List<DealerUpgrade> dealerUpgrades = new ArrayList<DealerUpgrade>();
    DealerUpgrade ciaUpgrade = createUpgrade( false, DealerUpgradeLookup.CIA.getCode() );
    DealerUpgrade apUpgrade = createUpgrade( false, DealerUpgradeLookup.AGING_PLAN.getCode() );
    DealerUpgrade redistUpgrade = createUpgrade( true, DealerUpgradeLookup.REDISTRIBUTION.getCode() );
    DealerUpgrade taUpgrade = createUpgrade( false, DealerUpgradeLookup.APPRAISAL.getCode() );
    dealerUpgrades.add( ciaUpgrade );
    dealerUpgrades.add( apUpgrade );
    dealerUpgrades.add( redistUpgrade );
    dealerUpgrades.add( taUpgrade );
    persistence.setDealerUpgrades( dealerUpgrades );

    assertTrue( !dealerService.hasAppraisalUpgrade( dealerUpgrades ) );
}

public void testHasRedistributionUpgrade() throws ApplicationException
{
    List<DealerUpgrade> dealerUpgrades = new ArrayList<DealerUpgrade>();
    DealerUpgrade ciaUpgrade = createUpgrade( false, DealerUpgradeLookup.CIA.getCode() );
    DealerUpgrade apUpgrade = createUpgrade( true, DealerUpgradeLookup.AGING_PLAN.getCode() );
    DealerUpgrade redistUpgrade = createUpgrade( true, DealerUpgradeLookup.REDISTRIBUTION.getCode() );
    DealerUpgrade taUpgrade = createUpgrade( false, DealerUpgradeLookup.APPRAISAL.getCode() );
    dealerUpgrades.add( ciaUpgrade );
    dealerUpgrades.add( apUpgrade );
    dealerUpgrades.add( redistUpgrade );
    dealerUpgrades.add( taUpgrade );
    persistence.setDealerUpgrades( dealerUpgrades );

    assertTrue( dealerService.hasRedistributionUpgrade( dealerUpgrades ) );
}

public void testHasNoRedistributionUpgrade() throws ApplicationException
{
    List<DealerUpgrade> dealerUpgrades = new ArrayList<DealerUpgrade>();
    DealerUpgrade ciaUpgrade = createUpgrade( false, DealerUpgradeLookup.CIA.getCode() );
    DealerUpgrade taUpgrade = createUpgrade( true, DealerUpgradeLookup.APPRAISAL.getCode() );
    DealerUpgrade apUpgrade = createUpgrade( false, DealerUpgradeLookup.AGING_PLAN.getCode() );
    dealerUpgrades.add( ciaUpgrade );
    dealerUpgrades.add( taUpgrade );
    dealerUpgrades.add( apUpgrade );
    persistence.setDealerUpgrades( dealerUpgrades );

    assertTrue( !dealerService.hasRedistributionUpgrade( dealerUpgrades ) );
}

public void testHasRedistributionNotActive() throws ApplicationException
{
    List<DealerUpgrade> dealerUpgrades = new ArrayList<DealerUpgrade>();
    DealerUpgrade ciaUpgrade = createUpgrade( false, DealerUpgradeLookup.CIA.getCode() );
    DealerUpgrade apUpgrade = createUpgrade( false, DealerUpgradeLookup.AGING_PLAN.getCode() );
    DealerUpgrade redistUpgrade = createUpgrade( false, DealerUpgradeLookup.REDISTRIBUTION.getCode() );
    DealerUpgrade taUpgrade = createUpgrade( false, DealerUpgradeLookup.APPRAISAL.getCode() );
    dealerUpgrades.add( ciaUpgrade );
    dealerUpgrades.add( apUpgrade );
    dealerUpgrades.add( redistUpgrade );
    dealerUpgrades.add( taUpgrade );
    persistence.setDealerUpgrades( dealerUpgrades );

    assertTrue( !dealerService.hasRedistributionUpgrade( dealerUpgrades ) );
}

public void testUpgradesNone() throws ApplicationException
{
    List<DealerUpgrade> dealerUpgrades = new ArrayList<DealerUpgrade>();
    persistence.setDealerUpgrades( dealerUpgrades );

    assertTrue( !dealerService.hasCIAUpgrade( dealerUpgrades ) );
    assertTrue( !dealerService.hasAgingPlanUpgrade( dealerUpgrades ) );
    assertTrue( !dealerService.hasAppraisalUpgrade( dealerUpgrades ) );
    assertTrue( !dealerService.hasRedistributionUpgrade( dealerUpgrades ) );
}

private DealerUpgrade createUpgrade( boolean active, int code )
{
    DealerUpgrade ciaUpgrade = new DealerUpgrade();
    ciaUpgrade.setActive( active );
    ciaUpgrade.setDealerUpgradeCode( code );
    return ciaUpgrade;
}

public void testRetrieveActiveDealersRunDayToday() throws ApplicationException
{
    Collection dealers = dealerService.retrieveActiveDealersRunDayToday();
    assertEquals( "Wrong size", 1, dealers.size() );
    assertTrue( "Wrong type returned", dealers.iterator().next() instanceof Dealer );

}
}
