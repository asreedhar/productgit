package com.firstlook.service.dealer;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import biz.firstlook.transact.persist.model.BusinessUnitCredential;
import biz.firstlook.transact.persist.model.CredentialType;
import biz.firstlook.transact.persist.model.DemandDealer;
import biz.firstlook.transact.persist.persistence.BusinessUnitCredentialDAO;
import biz.firstlook.transact.persist.persistence.DealerUpgradeDAO;
import biz.firstlook.transact.persist.persistence.DealerValuationPersistence;
import biz.firstlook.transact.persist.persistence.IInventoryDAO;
import biz.firstlook.transact.persist.retriever.InGroupDemandRetriever;
import biz.firstlook.transact.persist.retriever.UpdateInventoryBookoutLock;

import com.firstlook.action.estockcard.NextGenIMTReportDAO;
import com.firstlook.data.DatabaseException;
import com.firstlook.dealer.tools.GuideBook;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.DealerUpgrade;
import com.firstlook.entity.DealerUpgradeLookup;
import com.firstlook.entity.MemberAccess;
import com.firstlook.entity.ProgramTypeEnum;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.dealer.IDealerDAO;
import com.firstlook.persistence.dealergroup.DealerGroupDAO;
import com.firstlook.persistence.member.IMemberDAO;
import com.firstlook.service.memberaccess.MemberAccessService;

public class DealerService implements IDealerService
{

private IDealerDAO dealerDAO;
private MemberAccessService memberAccessService;
private IMemberDAO memberDAO;
private DealerUpgradeDAO dealerUpgradeDAO;
private DealerGroupDAO dealerGroupDAO;
private IInventoryDAO inventoryDAO;
private BusinessUnitCredentialDAO businessUnitCredentialDAO;
private NextGenIMTReportDAO nextGenIMTReportDAO;
private InGroupDemandRetriever inGroupDemandRetriever;
private UpdateInventoryBookoutLock updateInventoryBookoutLock;

public DealerService()
{
	super();
}

/**
 * @param dealerPersistence
 * @deprecated This service should be spring managed, in which case DealerDAO will be injected by spring, rendering this constructor useless.
 */
public DealerService( IDealerDAO dealerPersistence )
{
	this.dealerDAO = dealerPersistence;
}

public void save( Dealer dealer )
{
	dealerDAO.save( dealer );
}

@Transactional(propagation=Propagation.REQUIRED)
public void saveOrUpdate( final Dealer dealer )
{
	dealerDAO.saveOrUpdate( dealer );
	//businessUnitCredentialDAO.saveOrUpdateBusinessUnitCredential( businessUnitCredentialDAO );
}

@Transactional(propagation=Propagation.REQUIRED)
public void saveOrUpdateBusinessUnitCredential( final BusinessUnitCredential businessUnitCredential )
{
	businessUnitCredentialDAO.saveOrUpdateBusinessUnitCredential( businessUnitCredential );
}

public void delete( Dealer dealer )
{
	dealerDAO.delete( dealer );
}

public boolean hasCIAUpgrade( Collection<DealerUpgrade> dealerUpgrades )
{
	return hasUpgrade( DealerUpgradeLookup.CIA.getCode(), dealerUpgrades );
}

public boolean hasAgingPlanUpgrade( Collection<DealerUpgrade> dealerUpgrades )
{
	return hasUpgrade( DealerUpgradeLookup.AGING_PLAN.getCode(), dealerUpgrades );
}

public boolean hasAnnualRoiUpgrade( Collection<DealerUpgrade> dealerUpgrades )
{
	return hasUpgrade( DealerUpgradeLookup.ANNUAL_ROI.getCode(), dealerUpgrades );
}

public boolean hasEquityAnalyzerUpgrade( Collection<DealerUpgrade> dealerUpgrades )
{
	return hasUpgrade( DealerUpgradeLookup.EQUITY_ANALYZER_CODE, dealerUpgrades );
}

public boolean hasAppraisalLockoutUpgrade( Collection<DealerUpgrade> dealerUpgrades )
{
	return hasUpgrade( DealerUpgradeLookup.APPRAISAL_LOCKOUT_CODE, dealerUpgrades );
}

public boolean hasAppraisalUpgrade( Collection<DealerUpgrade> dealerUpgrades )
{
	return hasUpgrade( DealerUpgradeLookup.APPRAISAL.getCode(), dealerUpgrades );
}

public boolean hasRedistributionUpgrade( Collection<DealerUpgrade> dealerUpgrades )
{
	return hasUpgrade( DealerUpgradeLookup.REDISTRIBUTION.getCode(), dealerUpgrades );
}

public boolean hasAuctionUpgrade( Collection<DealerUpgrade> dealerUpgrades )
{
	return hasUpgrade( DealerUpgradeLookup.AUCTION_DATA.getCode(), dealerUpgrades );
}

public boolean hasMarketUpgrade( Collection<DealerUpgrade> dealerUpgrades )
{
	return hasUpgrade( DealerUpgradeLookup.MARKETDATA.getCode(), dealerUpgrades );
}

public boolean hasPerformanceDashboardUpgrade( Collection<DealerUpgrade> dealerUpgrades )
{
	return hasUpgrade( DealerUpgradeLookup.PERFORMANCEDASHBOARD.getCode(), dealerUpgrades );
}
public boolean hasNADAValuesUpgrade(Collection<DealerUpgrade> dealerUpgrades){
	return hasActiveUpgrade(DealerUpgradeLookup.NADA_VALUES_CODE, dealerUpgrades);
	
}
public boolean hasWindowStickerUpgrade( Collection<DealerUpgrade> dealerUpgrades )
{
	return hasUpgrade( DealerUpgradeLookup.WINDOWSTICKER.getCode(), dealerUpgrades );
}

public boolean hasKBBTradeInUpgrade( Collection<DealerUpgrade> dealerUpgrades )
{
	return hasUpgrade( DealerUpgradeLookup.KBB_TRADE_IN_CODE, dealerUpgrades );
}

public boolean hasMakeADealUpgrade( Collection<DealerUpgrade> dealerUpgrades )
{
	return hasActiveUpgrade( DealerUpgradeLookup.MAKE_A_DEAL_CODE, dealerUpgrades );
}

public boolean hasFirstlook3_0Upgrade( Collection<DealerUpgrade> dealerUpgrades ){
	
	return true;
	
}

public boolean hasMarketStockingGuideUpgrade( Collection<DealerUpgrade> dealerUpgrades )
{
	return hasActiveUpgrade( DealerUpgradeLookup.MARKET_STOCKING_GUIDE_CODE, dealerUpgrades );
}

// NOTE: ping is the only upgrade that checks start/end date
public boolean hasPingUpgrade( Collection<DealerUpgrade> dealerUpgrades )
{
	for( DealerUpgrade upgrade : dealerUpgrades ) {
		if ( upgrade.getDealerUpgradeCode() == DealerUpgradeLookup.PING_CODE && upgrade.isActive() ) {
			Date today = Calendar.getInstance().getTime();
			if ( upgrade.getStartDate() == null || upgrade.getStartDate().before( today ) ) {
				if (  upgrade.getEndDate() == null || upgrade.getEndDate().after( today ) ) {
					return true;
				}
			}
		}
	}
	return false;
}

//TODO: confirm check for date
public boolean hasPingIIUpgrade(Collection<DealerUpgrade> dealerUpgrades)
{
	//TODO Refactor this to generic method for checking date
	for( DealerUpgrade upgrade : dealerUpgrades ) {
		if ( upgrade.getDealerUpgradeCode() == DealerUpgradeLookup.PING_II_CODE && upgrade.isActive() ) {
			Date today = Calendar.getInstance().getTime();
			if ( upgrade.getStartDate() == null || upgrade.getStartDate().before( today ) ) {
				if (  upgrade.getEndDate() == null || upgrade.getEndDate().after( today ) ) {
					return true;
				}
			}
		}
	}
	return false;
}

public boolean hasEdmundsTmvUpgrade(Collection<DealerUpgrade> dealerUpgrades)
{
	//if the End date is filled in, the Upgrade is in Demo mode.  Demo ends after the End Date.
	//if the End date is not filled in, the customer has purchased the Upgrade
	DealerUpgrade u = getUpgrade(DealerUpgradeLookup.EDMUNDS_TMV_CODE, dealerUpgrades);
	if(u != null) {
		Date today = DateUtils.truncate(new Date(), Calendar.DATE);
		Date startDate = u.getStartDate();
		Date endDate = u.getEndDate(); 
		if(startDate != null && today.after(startDate)) {
			if(endDate == null || (today.before(endDate) || today.equals(endDate))) {
				return true;
			}
		}
	}
	return false;
}

public boolean hasJDPowerUCMDataUpgrade(Collection<DealerUpgrade> dealerUpgrades)
{
	//if the End date is filled in, the Upgrade is in Demo mode.  Demo ends after the End Date.
	//if the End date is not filled in, the customer has purchased the Upgrade
	return hasActiveUpgrade( DealerUpgradeLookup.JDPOWER_USED_CAR_MARKET_DATA_CODE, dealerUpgrades );
}

private static boolean hasUpgrade( int dealerUpgradeCode, Collection<DealerUpgrade> upgrades )
{
	DealerUpgrade u = getUpgrade(dealerUpgradeCode, upgrades);
	return (u != null && u.isActive());
}

private static boolean hasActiveUpgrade( int dealerUpgradeCode, Collection<DealerUpgrade> upgrades )
{
	DealerUpgrade u = getUpgrade(dealerUpgradeCode, upgrades);
	if (u != null) {
		if (u.isActive()) {
			Date today = DateUtils.truncate(new Date(), Calendar.DATE);
			Date startDate = u.getStartDate();
			Date endDate = u.getEndDate();
			if (startDate == null || today.after(startDate)) {
				if (endDate == null || (today.before(endDate) || today.equals(endDate))) {
					return true;
				}
			}
		}
	}
	return false;
}

private static DealerUpgrade getUpgrade(final int dealerUpgradeCode, final Collection<DealerUpgrade> dealerUpgrades) {
	if(dealerUpgrades != null) {
		for(DealerUpgrade u : dealerUpgrades) {
			if(u.getDealerUpgradeCode() == dealerUpgradeCode) {
				return u;
			}
		}
	}
	return null;
}

public Collection<DealerUpgrade> findUpgrades( int dealerId ) throws ApplicationException
{
	try
	{
		return (Collection<DealerUpgrade>) dealerDAO.findUpgrades( dealerId );
	}
	catch ( DatabaseException e )
	{
		throw new ApplicationException( "Unable to find upgrades for dealer " + dealerId, e );
	}
}

public void saveUpgrades( Collection<DealerUpgrade> upgrades ) throws ApplicationException
{
	try
	{
		Iterator<DealerUpgrade> it = upgrades.iterator();
		while ( it.hasNext() )
		{
			dealerDAO.saveOrUpdateUpgrade( it.next() );
		}
	}
	catch ( DatabaseException e )
	{
		throw new ApplicationException( "Unable to save upgrades.", e );
	}
}

public void createDefaultInsightUpgrades( Dealer dealer ) throws ApplicationException
{
	DealerUpgrade cia = createDealerUpgrade( dealer, DealerUpgradeLookup.CIA_CODE );
	DealerUpgrade tradeAnalyzer = createDealerUpgrade( dealer, DealerUpgradeLookup.APPRAISAL_CODE );
	DealerUpgrade redistribution = createDealerUpgrade( dealer, DealerUpgradeLookup.REDISTRIBUTION_CODE );
	DealerUpgrade agingPlan = createDealerUpgrade( dealer, DealerUpgradeLookup.AGING_PLAN_CODE );

	ArrayList<DealerUpgrade> upgrades = new ArrayList<DealerUpgrade>();
	upgrades.add( cia );
	upgrades.add( tradeAnalyzer );
	upgrades.add( redistribution );
	upgrades.add( agingPlan );

	saveUpgrades( upgrades );
}

private DealerUpgrade createDealerUpgrade( Dealer dealer, int dealerUpgradeCode )
{
	DealerUpgrade upgrade = new DealerUpgrade();
	upgrade.setDealerId( dealer.getDealerId().intValue() );
	upgrade.setDealerUpgradeCode( dealerUpgradeCode );
	upgrade.setStartDate( new Timestamp( new Date().getTime() ) );
	upgrade.setEndDate( new Timestamp( new Date().getTime() ) );
	upgrade.setActive( true );
	return upgrade;
}

public Dealer retrieveDealer( int dealerId )
{
	return dealerDAO.findByPk( dealerId );
}

public BusinessUnitCredential retrieveBusinessUnitCredential( int businessUnitId, CredentialType credentialType )
{
	return businessUnitCredentialDAO.findCredentialByBusinessIdAndCredentialTypeId( new Integer( businessUnitId ), credentialType);	
}

public List<Dealer> retrieveByDealerCode( String businessUnitCode )
{
	return dealerDAO.findByDealerCode( businessUnitCode );
}

public List findInGroupDealersWithRedistribution( int dealerGroupId ) throws ApplicationException
{
	try
	{
		return dealerDAO.findActiveDealersWithRedistributionBy( dealerGroupId );
	}
	catch ( DatabaseException e )
	{
		throw new ApplicationException( "Unable to find dealers in group with redistribution.", e );
	}
}

public Collection retrieveActiveDealersRunDayToday() throws ApplicationException
{
	Calendar today = Calendar.getInstance();
	try
	{
		return dealerDAO.findActiveDealersForRunDay( today.get( Calendar.DAY_OF_WEEK ) );
	}
	catch ( DatabaseException e )
	{
		throw new ApplicationException( "Unable to find active dealers for RunDay.", e );
	}
}

public List retrieveActiveDealers()
{
	return dealerDAO.findActiveDealers();
}

public boolean isInsight( Dealer dealer )
{
	return checkProgramType( dealer, ProgramTypeEnum.INSIGHT );
}

public boolean isVIP( Dealer dealer )
{
	return checkProgramType( dealer, ProgramTypeEnum.VIP );
}
public boolean isFirstlook( Dealer dealer )
{
    return checkProgramType( dealer, ProgramTypeEnum.FIRSTLOOK);
}

private boolean checkProgramType( Dealer dealer, ProgramTypeEnum expected )
{
	if ( dealer != null )
	{
		return expected.equals( dealer.getProgramTypeEnum() );
	}
	else
	{
		return false;
	}
}

public Collection retrieveMembers( int dealerId )
{
	Collection memberAccesses;

	memberAccesses = getMemberAccessService().retrieveByBusinessUnitId( dealerId );

	if ( memberAccesses != null && memberAccesses.size() > 0 )
	{
		List<Integer> memberIds = new ArrayList<Integer>();
		Iterator memberAccessIter = memberAccesses.iterator();
		while ( memberAccessIter.hasNext() )
		{
			memberIds.add( ( (MemberAccess)memberAccessIter.next() ).getMemberId() );
		}

		return memberDAO.findByMultipleMemberIds( memberIds );
	}
	else
	{
		return new ArrayList();
	}
}

public Integer determineDemandDealerSortOrder(Integer businessUnitID, Integer sortByUnderstock, Integer sortByROI){
	
	Integer sortBy = new Integer(0);
	
	if(sortByUnderstock != 0 && ((sortByUnderstock <= sortByROI && sortByROI != 0) || sortByROI == 0)){
		sortBy = 1; 
	}
	else if(sortByROI != 0){
		
		try
		{
			if(hasAnnualRoiUpgrade(findUpgrades(businessUnitID))){
				sortBy = 2;
			}
		}
		catch ( ApplicationException ae )
		{
			sortBy = 0;
		}
		
	}
	
	return sortBy;
}

public List<DemandDealer> retrieveDemandDealers( Integer dealerId, Integer groupingId, Integer mileage, Integer year, Integer distance, Integer sortByROI ) throws ApplicationException
{
	return inGroupDemandRetriever.getDemandDealers( dealerId, groupingId, mileage, year, distance, sortByROI );
}

public void updateInventoryBookoutLock( Integer businessUnitId )
{
	updateInventoryBookoutLock.updateInventoryAppraisalBookoutLock( businessUnitId );
}

public void populateDealerWithDefaults( Dealer dealer ) throws DatabaseException, ApplicationException
{
	dealer.setUnitsSoldThreshold4Wks( Dealer.UNITS_SOLD_THRESHOLD_4_WEEK_DEFAULT );
	dealer.setUnitsSoldThreshold8Wks( Dealer.UNITS_SOLD_THRESHOLD_8_WEEK_DEFAULT );
	dealer.setUnitsSoldThreshold13Wks( Dealer.UNITS_SOLD_THRESHOLD_12_WEEK_DEFAULT );
	dealer.setUnitsSoldThreshold26Wks( Dealer.UNITS_SOLD_THRESHOLD_26_WEEK_DEFAULT );
	dealer.setUnitsSoldThreshold52Wks( Dealer.UNITS_SOLD_THRESHOLD_52_WEEK_DEFAULT );
	dealer.setDealerCode( dealer.createDealerCode() );
	dealer.setGuideBookId( GuideBook.GUIDE_TYPE_BLACKBOOK );
	dealer.setProgramTypeCD( ProgramTypeEnum.VIP_VAL );
}

public Collection retrieveDealersWithNoDRT()
{
	DealerValuationPersistence dealerValuationPeristence = new DealerValuationPersistence();
	List dealerIdList = dealerValuationPeristence.findUniqueDealerIds();

	List<Integer> dealerIds = new ArrayList<Integer>();

	Iterator dealerIdListIter = dealerIdList.iterator();
	while ( dealerIdListIter.hasNext() )
	{
		Integer dealerId = (Integer)dealerIdListIter.next();
		dealerIds.add( dealerId );
	}

	return dealerDAO.findDealersWithNoDRT( dealerIds );
}

public boolean showLithiaRoi( int businessUnitId )
{
	boolean showLithiaRoi = false;
	DealerGroup dealerGroup = getDealerGroupDAO().findByDealerId( businessUnitId );
	try
	{
		if ( dealerGroup.getDealerGroupPreference().isLithiaStore() || hasAnnualRoiUpgrade( findUpgrades( businessUnitId ) ) )
		{
			showLithiaRoi = true;
		}
	}
	catch ( ApplicationException ae )
	{
		showLithiaRoi = false;
	}

	return showLithiaRoi;
}

public String retrieveDealerLogo( Integer businessUnitId )
{
	Object[] pars = { businessUnitId };
	String sql = "SELECT * FROM dbo.Photos p " +
	 "JOIN dbo.BusinessUnitPhotos bup on bup.photoId=p.photoId " +
	 "WHERE bup.businessUnitId= ? " +
	 "AND PhotoTypeID = 3 "; //3 is enum for logo photo

	List results = nextGenIMTReportDAO.getResults( sql, pars );
	
	Map dealerLogo = new HashMap();

	if ( !results.isEmpty() && ( (Map)results.get( 0 ) ).get( "PhotoID" ) != null )
	{
		dealerLogo = (Map)results.get( 0 );
		return (String)dealerLogo.get("PhotoURL");
	}
	
	return null;
		
}

public Collection retrieveByDealerCodeMultiple( String partialDealerCode )
{
	return dealerDAO.findByDealerCodeMultiple( partialDealerCode );
}

public Collection retrieveByName( String name )
{
	return dealerDAO.findByName( name );
}

public List retrieveByDealerIds( Collection dealerIds )
{
	return dealerDAO.findByDealerIds( dealerIds );
}

public Collection retrieveByDealerGroupIdAndNameAndNickName( Collection dealerGroupCol, String name, String nickName )
{
	return dealerDAO.findByDealerGroupIdAndNameAndNickName( dealerGroupCol, name, nickName );
}

public Collection<Dealer> retrieveByDealerGroupId( int dealerGroupId )
{
	return dealerDAO.findByDealerGroupId( dealerGroupId );
}

public MemberAccessService getMemberAccessService()
{
	return memberAccessService;
}

public void setMemberAccessService( MemberAccessService memberAccessService )
{
	this.memberAccessService = memberAccessService;
}

public IMemberDAO getMemberDAO()
{
	return memberDAO;
}

public void setMemberDAO( IMemberDAO memberDAO )
{
	this.memberDAO = memberDAO;
}

public Collection getUpgradesByBusinessUnitId( Integer firstBusinessUnitId )
{
	return dealerUpgradeDAO.getUpgradesByBusinessUnitId( firstBusinessUnitId );
}

public DealerUpgradeDAO getDealerUpgradeDAO()
{
	return dealerUpgradeDAO;
}

public void setDealerUpgradeDAO( DealerUpgradeDAO dealerUpgradeDAO )
{
	this.dealerUpgradeDAO = dealerUpgradeDAO;
}

public IDealerDAO getDealerDAO()
{
	return dealerDAO;
}

public void setDealerDAO( IDealerDAO dealerDAO )
{
	this.dealerDAO = dealerDAO;
}

public InGroupDemandRetriever getInGroupDemandRetriever()
{
	return inGroupDemandRetriever;
}

public void setInGroupDemandRetriever( InGroupDemandRetriever inGroupDemandRetriever )
{
	this.inGroupDemandRetriever = inGroupDemandRetriever;
}

public DealerGroupDAO getDealerGroupDAO()
{
	return dealerGroupDAO;
}

public void setDealerGroupDAO( DealerGroupDAO dealerGroupDAO )
{
	this.dealerGroupDAO = dealerGroupDAO;
}

public BusinessUnitCredentialDAO getBusinessUnitCredentialDAO()
{
	return businessUnitCredentialDAO;
}

public void setBusinessUnitCredentialDAO( BusinessUnitCredentialDAO businessUnitCredentialDAO )
{
	this.businessUnitCredentialDAO = businessUnitCredentialDAO;
}

public IInventoryDAO getInventoryDAO()
{
	return inventoryDAO;
}

public void setInventoryDAO( IInventoryDAO inventoryDAO )
{
	this.inventoryDAO = inventoryDAO;
}

public UpdateInventoryBookoutLock getUpdateInventoryBookoutLock()
{
	return updateInventoryBookoutLock;
}

public void setUpdateInventoryBookoutLock( UpdateInventoryBookoutLock updateInventoryBookoutLock )
{
	this.updateInventoryBookoutLock = updateInventoryBookoutLock;
}

public NextGenIMTReportDAO getNextGenIMTReportDAO()
{
	return nextGenIMTReportDAO;
}

public void setNextGenIMTReportDAO( NextGenIMTReportDAO nextGenIMTReportDAO )
{
	this.nextGenIMTReportDAO = nextGenIMTReportDAO;
}

}