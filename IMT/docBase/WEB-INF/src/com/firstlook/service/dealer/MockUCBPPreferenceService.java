package com.firstlook.service.dealer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import biz.firstlook.transact.persist.model.CIACompositeTimePeriod;
import biz.firstlook.transact.persist.model.CIAPreferences;

import com.firstlook.entity.DealerRisk;
import com.firstlook.exception.ApplicationException;

public class MockUCBPPreferenceService implements IUCBPPreferenceService
{

private DealerRisk dealerRisk;
	
public MockUCBPPreferenceService()
{
    super();
}

public DealerRisk retrieveDealerRisk( int dealerId )
{
	if( dealerRisk == null )
		return new DealerRisk();
	else
		return dealerRisk;
}

public CIAPreferences retrieveCIAPreferences( int dealerId )
{
    return new CIAPreferences();
}

public int determineRiskLevelYearOffset( int riskLevelYearRollOverMonth, int riskLevelYearInitialTimePeriod,
                                        int riskLevelYearSecondaryTimePeriod, Calendar today )
{
	if ( today.get( Calendar.MONTH ) < riskLevelYearRollOverMonth )
	{
		return -1 * riskLevelYearInitialTimePeriod;
	}
	else
	{
		return -1 * riskLevelYearSecondaryTimePeriod;
	}
}

public void updateUCBPPreferences( DealerRisk dealerRisk, CIAPreferences ciaPreference ) throws ApplicationException
{
}

public Collection retrieveCIATimePeriods()
{
    return new ArrayList();
}

public CIAPreferences createDefaultCIAPreference( int dealerId )
{
    return new CIAPreferences();
}

public void updateCIAPreference( CIAPreferences ciaPreference )
{
}

public void updateDealerRisk( DealerRisk dealerRisk )
{
}

public DealerRisk createDefaultDealerRisk( int dealerId )
{
    return new DealerRisk();
}

public CIACompositeTimePeriod retrieveCIACompositeTimePeriod( Integer ciaCompositeTimePeriodId )
{
    return new CIACompositeTimePeriod();
}

public Collection retrieveCIACompositeTimePeriod()
{
    return new ArrayList();
}

public void setDealerRisk(DealerRisk dealerRisk) {
	this.dealerRisk = dealerRisk;
}

}
