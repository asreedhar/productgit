package com.firstlook.service.softwaresystem;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import biz.firstlook.commons.util.Functions;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.Member;
import com.firstlook.entity.SoftwareSystemComponent;
import com.firstlook.entity.SoftwareSystemComponentState;
import com.firstlook.persistence.dealer.DealerDAO;
import com.firstlook.persistence.dealergroup.DealerGroupDAO;
import com.firstlook.persistence.member.MemberDAO;
import com.firstlook.persistence.softwaresystem.SoftwareSystemComponentDAO;
import com.firstlook.persistence.softwaresystem.SoftwareSystemComponentStateDAO;

public class SoftwareSystemStateService {

	public static final String DEALER_GROUP_SYSTEM_COMPONENT = "DEALER_GROUP_SYSTEM_COMPONENT";
	public static final String DEALER_SYSTEM_COMPONENT = "DEALER_SYSTEM_COMPONENT";

	private static final int ADMINISTRATOR = 1;
	private static final int USER = 2;
	private static final int ACCOUNT_REPRESENTATIVE = 3;
	
	private TransactionTemplate transactionTemplate;
	private MemberDAO memberDAO;
	private DealerDAO dealerDAO;
	private DealerGroupDAO dealerGroupDAO;
	private SoftwareSystemComponentDAO softwareSystemComponentDAO;
	private SoftwareSystemComponentStateDAO softwareSystemComponentStateDAO;
	
	public SoftwareSystemStateService() {
		super();
	}

	public SoftwareSystemComponentState findOrCreateSoftwareSystemComponentState(String login, String token, Integer dealerGroupId, Integer dealerId) {
		SoftwareSystemComponentState state = getSoftwareSystemComponentStateDAO().findByLoginAndToken(login, token);
		Member member = getMemberDAO().findByLogin(login);
		if (allowAccessToSoftwareSystemComponent(token, member)) {
			if (member.isAssociatedWithDealer(dealerId)) {
				if (state == null) {
					state = new SoftwareSystemComponentState();
				}
				state.setAuthorizedMember(member);
				state.setSoftwareSystemComponent(getSoftwareSystemComponent(token));
				state.setDealerGroup(getDealerGroup(dealerGroupId));
				state.setDealer(getDealer(dealerId));
				state.setMember(null);
				saveOrUpdate(state);
			}
			else{
				throw new IllegalStateException(String.format("Member %s is not associated with Dealer %d", login, dealerId ));
			}
		}
		return state;
	}

	public SoftwareSystemComponentState findOrCreateSoftwareSystemComponentState(String login, String token) {
		SoftwareSystemComponentState state = getSoftwareSystemComponentStateDAO().findByLoginAndToken(login, token);
		if (state == null) {
			Member member = getMemberDAO().findByLogin(login);
			if (member.getMemberType() == USER) {
				if (allowAccessToSoftwareSystemComponent(token, member)) {
					Dealer dealer = (Dealer) member.getDealersCollection().iterator().next();
					state = new SoftwareSystemComponentState();
					state.setAuthorizedMember(member);
					state.setSoftwareSystemComponent(getSoftwareSystemComponent(token));
					state.setDealerGroup(getDealerGroupDAO().findByDealerId(dealer.getBusinessUnitId()));
					state.setDealer(dealer);
					state.setMember(null);
					saveOrUpdate(state);
				}
			}
		}
		return state;
	}
	
	private SoftwareSystemComponent getSoftwareSystemComponent(String token) {
		SoftwareSystemComponent component = getSoftwareSystemComponentDAO().findByToken(token);
		return component;
	}
	
	private DealerGroup getDealerGroup(Integer dealerGroupId) {
		DealerGroup dealerGroup = null;
		if (dealerGroupId != null) {
			dealerGroup = getDealerGroupDAO().findByDealerGroupId(dealerGroupId);
		}
		return dealerGroup;
	}
	
	private Dealer getDealer(Integer dealerId) {
		Dealer dealer = null;
		if (dealerId != null) {
			dealer = getDealerDAO().findByPk(dealerId);
		}
		return dealer;
	}
	
	private boolean allowAccessToSoftwareSystemComponent(String token, Member member) {
		boolean allow = false;
		switch (member.getDealersCollection().size()) {
		case 0:
			allow |= member.getMemberType() == ADMINISTRATOR;
			allow |= member.getMemberType() == ACCOUNT_REPRESENTATIVE;
			break;
		case 1:
			allow = Functions.nullSafeEquals(token, DEALER_SYSTEM_COMPONENT);
			break;
		default:
			allow |= Functions.nullSafeEquals(token, DEALER_GROUP_SYSTEM_COMPONENT);
			allow |= Functions.nullSafeEquals(token, DEALER_SYSTEM_COMPONENT);
			break;
		}
		return allow;
	}
	
	private void saveOrUpdate(final SoftwareSystemComponentState entity) {
		getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus arg0) {
				getSoftwareSystemComponentStateDAO().saveOrUpdate(entity);
				return null;
			}
		});
	}
	
	public TransactionTemplate getTransactionTemplate() {
		return transactionTemplate;
	}

	public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
		this.transactionTemplate = transactionTemplate;
	}

	public MemberDAO getMemberDAO() {
		return memberDAO;
	}

	public void setMemberDAO(MemberDAO memberDAO) {
		this.memberDAO = memberDAO;
	}

	public DealerDAO getDealerDAO() {
		return dealerDAO;
	}

	public void setDealerDAO(DealerDAO dealerDAO) {
		this.dealerDAO = dealerDAO;
	}

	public DealerGroupDAO getDealerGroupDAO() {
		return dealerGroupDAO;
	}

	public void setDealerGroupDAO(DealerGroupDAO dealerGroupDAO) {
		this.dealerGroupDAO = dealerGroupDAO;
	}

	public SoftwareSystemComponentDAO getSoftwareSystemComponentDAO() {
		return softwareSystemComponentDAO;
	}

	public void setSoftwareSystemComponentDAO(SoftwareSystemComponentDAO softwareSystemComponentDAO) {
		this.softwareSystemComponentDAO = softwareSystemComponentDAO;
	}

	public SoftwareSystemComponentStateDAO getSoftwareSystemComponentStateDAO() {
		return softwareSystemComponentStateDAO;
	}

	public void setSoftwareSystemComponentStateDAO(SoftwareSystemComponentStateDAO softwareSystemComponentStateDAO) {
		this.softwareSystemComponentStateDAO = softwareSystemComponentStateDAO;
	}
	
}
