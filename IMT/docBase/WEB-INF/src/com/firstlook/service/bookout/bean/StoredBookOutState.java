package com.firstlook.service.bookout.bean;

import java.util.Date;
import java.util.List;

import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

import com.firstlook.display.tradeanalyzer.DisplayGuideBook;

public abstract class StoredBookOutState extends AbstractBookOutState 
{

private String vin;
private String make;
private String model;
private String vehicleYear;
private List<VDP_GuideBookValue> bookValues;
private List<ThirdPartyVehicleOption> thirdPartyVehicleOptions;
private Date bookOutDate;
private List<VDP_GuideBookVehicle> dropDownVehicles;
private List<VDP_GuideBookVehicle> dropDownModels;
private Integer mileageAdjustment;
private Integer bookoutMileage;
private Integer msrp;
private Integer weight;
private String selectedVehicleKey;
private String selectedModelKey;

public abstract DisplayGuideBook transformBookOutToDisplayData( DisplayGuideBook displayGuideBook );

public List<VDP_GuideBookVehicle> getDropDownModels() {
	return dropDownModels;
}

public void setDropDownModels(List<VDP_GuideBookVehicle> dropDownModels) {
	this.dropDownModels = dropDownModels;
}

public Integer getBookoutMileage()
{
	return bookoutMileage;
}

public void setBookoutMileage( Integer bookoutMileage )
{
	this.bookoutMileage = bookoutMileage;
}

public List<VDP_GuideBookValue> getBookValues()
{
	return bookValues;
}

public void setBookValues( List<VDP_GuideBookValue> bookValues )
{
	this.bookValues = bookValues;
}

public List<ThirdPartyVehicleOption> getThirdPartyVehicleOptions()
{
	return thirdPartyVehicleOptions;
}

public void setThirdPartyVehicleOptions( List<ThirdPartyVehicleOption> guideBookOptions )
{
	this.thirdPartyVehicleOptions = guideBookOptions;
}

public String getMake()
{
	return make;
}

public void setMake( String make )
{
	this.make = make;
}

public Date getBookOutDate()
{
	return bookOutDate;
}

public void setBookOutDate( Date tradeAnalyzedDate )
{
	this.bookOutDate = tradeAnalyzedDate;
}

public List<VDP_GuideBookVehicle> getDropDownVehicles()
{
	return dropDownVehicles;
}

public void setDropDownVehicles( List<VDP_GuideBookVehicle> dropDownVehicles )
{
	this.dropDownVehicles = dropDownVehicles;
}

public String getVehicleYear()
{
	return vehicleYear;
}

public void setVehicleYear( String vehicleYear )
{
	this.vehicleYear = vehicleYear;
}

public Integer getMileageAdjustment()
{
	return mileageAdjustment;
}

public void setMileageAdjustment( Integer mileageAdjustment )
{
	this.mileageAdjustment = mileageAdjustment;
}

public String getVin()
{
	return vin;
}

public void setVin( String vin )
{
	this.vin = vin;
}

public Integer getMsrp()
{
	return msrp;
}

public void setMsrp( Integer msrp )
{
	this.msrp = msrp;
}

public Integer getWeight()
{
	return weight;
}

public void setWeight( Integer weight )
{
	this.weight = weight;
}

public String getModel()
{
	return model;
}

public void setModel( String model )
{
	this.model = model;
}

public String getSelectedVehicleKey()
{
	return selectedVehicleKey;
}

public void setSelectedVehicleKey( String selectedVehicleKey )
{
	this.selectedVehicleKey = selectedVehicleKey;
}

public String getSelectedModelKey() {
	return selectedModelKey;
}

public void setSelectedModelKey(String selectedModelKey) {
	this.selectedModelKey = selectedModelKey;
}

}
