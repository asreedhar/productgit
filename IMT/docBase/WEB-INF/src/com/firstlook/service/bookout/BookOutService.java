package com.firstlook.service.bookout;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanComparator;

import biz.firstlook.commons.util.InvalidVinException;
import biz.firstlook.commons.util.VinUtility;
import biz.firstlook.module.bookout.IThirdPartyBookoutService;
import biz.firstlook.module.vehicle.description.old.BookOutDatasetInfo;
import biz.firstlook.module.vehicle.description.old.GBException;
import biz.firstlook.module.vehicle.description.old.GuideBookInput;
import biz.firstlook.module.vehicle.description.old.GuideBookMetaInfo;
import biz.firstlook.module.vehicle.description.old.KBBBookoutService;
import biz.firstlook.module.vehicle.description.old.KBBConditionEnum;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.enumerator.BookoutStatusEnum;
import biz.firstlook.transact.persist.enumerator.ThirdPartySubCategoryEnum;
import biz.firstlook.transact.persist.exception.SelectedThirdPartyVehicleNotFoundException;
import biz.firstlook.transact.persist.model.BookOut;
import biz.firstlook.transact.persist.model.BookOutThirdPartyCategory;
import biz.firstlook.transact.persist.model.BookOutValue;
import biz.firstlook.transact.persist.model.BookOutValueType;
import biz.firstlook.transact.persist.model.DealerUpgrade;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;
import biz.firstlook.transact.persist.model.ThirdPartyVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.StagingBookOut;
import biz.firstlook.transact.persist.service.appraisal.StagingBookOutOption;

import com.firstlook.dealer.tools.VinNotFoundException;
import com.firstlook.service.bookout.bean.AbstractBookOutState;
import com.firstlook.service.bookout.bean.BookValuesBookOutState;
import com.firstlook.service.bookout.bean.CompleteStoredBookOutState;
import com.firstlook.service.bookout.bean.IncompleteStoredBookOutState;
import com.firstlook.service.bookout.bean.OneVehicleAndOptionsBookOutState;
import com.firstlook.service.bookout.bean.SelectedVehicleAndOptionsBookOutState;
import com.firstlook.service.bookout.bean.StoredBookOutState;
import com.firstlook.service.bookout.bean.VehiclesNoOptionsBookOutState;
import com.firstlook.thirdparty.book.ThirdPartyBookoutServiceFactory;
import com.firstlook.thirdparty.book.old.VDP_SelectedKeys;

public class BookOutService
{

private BookOutAssembler bookOutAssembler;
private ThirdPartyBookoutServiceFactory thirdPartyBookoutServiceFactory;
private KBBBookoutService kBBBookoutService;
private DealerPreferenceDAO dealerPrefDAO;

public BookOutService()
{
}

/**
 * Initial call to guide book
 */
public AbstractBookOutState doVinLookup( GuideBookInput input, Integer thirdPartyId,
											BookOutDatasetInfo bookoutDatasetInfo, StagingBookOut stagingBookOut ) throws GBException
{
	IThirdPartyBookoutService thirdPartyBookoutService = getThirdPartyBookoutServiceFactory().retrieveThirdPartyBookoutService( thirdPartyId );
	List<VDP_GuideBookVehicle> vdp_guideBookVehicles = thirdPartyBookoutService.doVinLookup( input.getRegion( thirdPartyId.intValue() ), input.getVin(),
																		bookoutDatasetInfo );
	
	if ( vdp_guideBookVehicles.size() == 0 )
	{
		if(thirdPartyId == 3)
		{
			VehiclesNoOptionsBookOutState multiVehiclesState = getKBBTrimsByModel(thirdPartyBookoutService,vdp_guideBookVehicles,input,thirdPartyId,bookoutDatasetInfo);
			return multiVehiclesState;
		}
		else{
			GuideBookMetaInfo bookMetaInfo = thirdPartyBookoutService.getMetaInfo( input.getRegion( thirdPartyId.intValue() ), bookoutDatasetInfo );
			  VinNotFoundException exception = new VinNotFoundException( input.getVin(),
			                 input.getBusinessUnitId(),
			                 bookMetaInfo.getGuideBookTitle(),
			                 new Date() );
			  throw exception;
		}
	}

	VDP_GuideBookVehicle vehicle = (VDP_GuideBookVehicle)vdp_guideBookVehicles.get( 0 );
	String make = vehicle.getMake();

	String regionDescription = thirdPartyBookoutService.retrieveRegionDescription( input );
	GuideBookMetaInfo footer = thirdPartyBookoutService.getMetaInfo( regionDescription, bookoutDatasetInfo );
	
	// More than one vehicle, init a multiVehicleState
	if ( vdp_guideBookVehicles.size() > 1 && stagingBookOut != null && selectedVehicleKey(stagingBookOut, vdp_guideBookVehicles) != null) {
		SelectedVehicleAndOptionsBookOutState selectedState = new SelectedVehicleAndOptionsBookOutState();
		selectedState.setDropDownVehicles(vdp_guideBookVehicles);
		selectedState.setMetaInfo( footer );
		selectedState.setRegionDescription( regionDescription );
		selectedState.setMake( make );
		selectedState.setVehicleYear( vehicle.getYear() );
		selectedState.setPendingBookOut(stagingBookOut);
		selectedState.setSelectedVehicleKey(stagingBookOut.getThirdPartyVehicleCode());
		List<ThirdPartyVehicleOption> options = retrieveOptionsForVehicle( input, selectedVehicleKey(stagingBookOut, vdp_guideBookVehicles), thirdPartyId, bookoutDatasetInfo );
		selectedState.setThirdPartyVehicleOptions( options );
		return selectedState;
	}
	else if ( vdp_guideBookVehicles.size() > 1)
	{
		VehiclesNoOptionsBookOutState multiVehiclesState = new VehiclesNoOptionsBookOutState();
		multiVehiclesState.setDropDownVehicles( vdp_guideBookVehicles );
		multiVehiclesState.setMetaInfo( footer );
		multiVehiclesState.setRegionDescription( regionDescription );
		multiVehiclesState.setMake( make );
		multiVehiclesState.setVehicleYear( vehicle.getYear() );
		multiVehiclesState.setModel( vehicle.getModel() );
		return multiVehiclesState;
	}
	// One vehicle, init a oneVehicleState
	else
	// TODO: check error handling here...what if no GBVs are returned - KDL
	{
		OneVehicleAndOptionsBookOutState oneVehicleState = new OneVehicleAndOptionsBookOutState();
		VDP_GuideBookVehicle vdp_guideBookVehicle = (VDP_GuideBookVehicle)vdp_guideBookVehicles.toArray()[0];
		oneVehicleState.setGuideBookVehicle( vdp_guideBookVehicle );
		List<ThirdPartyVehicleOption> options = thirdPartyBookoutService.retrieveOptions( input, vdp_guideBookVehicle, bookoutDatasetInfo );
		oneVehicleState.setThirdPartyVehicleOptions( options );
		oneVehicleState.setMetaInfo( footer );
		oneVehicleState.setRegionDescription( regionDescription );
		oneVehicleState.setMake( make );
		oneVehicleState.setModel( vehicle.getModel() );
		oneVehicleState.setVehicleYear( vehicle.getYear() );
		oneVehicleState.setSelectedVehicleKey( vehicle.getKey() );
		return oneVehicleState;
	}
}

private VDP_GuideBookVehicle selectedVehicleKey(StagingBookOut stagingBookOut, List<VDP_GuideBookVehicle> vehicles )
{
	for (VDP_GuideBookVehicle vehicle : vehicles){
		if (vehicle.getKey().equals(stagingBookOut.getThirdPartyVehicleCode())){
			return vehicle;
		}
	}
	
	return null;
}

public List< ThirdPartyVehicle > constructDropDownVehicles( GuideBookInput input, Integer thirdPartyId,
															ThirdPartyVehicle selectedThirdPartyVehicle, BookOutDatasetInfo bookoutDatasetInfo )
		throws GBException
{
	IThirdPartyBookoutService thirdPartyBookoutService = getThirdPartyBookoutServiceFactory().retrieveThirdPartyBookoutService( thirdPartyId );
														
	List< VDP_GuideBookVehicle > vdp_guideBookVehicles = thirdPartyBookoutService.doVinLookup( input.getRegion( thirdPartyId.intValue() ),
																								input.getVin(), bookoutDatasetInfo );
	if ( vdp_guideBookVehicles.size() == 0 )
	{
		if(thirdPartyId == 3)
		{
			VehiclesNoOptionsBookOutState multiVehiclesState = getKBBTrimsByModel(thirdPartyBookoutService,vdp_guideBookVehicles,input,thirdPartyId,bookoutDatasetInfo);
			vdp_guideBookVehicles = multiVehiclesState.getDropDownVehicles();
		}
		else
		{	VinNotFoundException exception = new VinNotFoundException(	input.getVin(),
																	input.getBusinessUnitId(),
																	ThirdPartyDataProvider.getThirdPartyDataProviderDescription( thirdPartyId ),
																	new Date() );
			throw exception;
		}
	}

	List< ThirdPartyVehicle > returnList = new ArrayList< ThirdPartyVehicle >();
	VDP_GuideBookVehicle vehicleFromGuideBook;
	Iterator< VDP_GuideBookVehicle > vehiclesFromGuideBookIter = vdp_guideBookVehicles.iterator();

	while ( vehiclesFromGuideBookIter.hasNext() )
	{
		vehicleFromGuideBook = vehiclesFromGuideBookIter.next();
		if ( selectedThirdPartyVehicle != null && vehicleFromGuideBook.getKey().equals( selectedThirdPartyVehicle.getThirdPartyVehicleCode() ) )
		{
			if ( thirdPartyId.intValue() == ThirdPartyDataProvider.GUIDEBOOK_GALVES_TYPE )
			{
				populateGalvesThirdPartyVehicleFields( selectedThirdPartyVehicle, vehicleFromGuideBook );
			}
			returnList.add( selectedThirdPartyVehicle );
		}
		else
		{
			ThirdPartyVehicle thirdPartyVehicle = new ThirdPartyVehicle();
			thirdPartyVehicle.setDateCreated( new Date() );
			thirdPartyVehicle.setDescription( vehicleFromGuideBook.getDescription() );
			thirdPartyVehicle.setStatus( false );
			thirdPartyVehicle.setThirdPartyId( thirdPartyId );
			thirdPartyVehicle.setThirdPartyVehicleCode( vehicleFromGuideBook.getKey() );
			thirdPartyVehicle.setMakeCode( vehicleFromGuideBook.getMakeId() );
			thirdPartyVehicle.setModelCode( vehicleFromGuideBook.getModelId() );
			if ( thirdPartyId.intValue() == ThirdPartyDataProvider.GUIDEBOOK_GALVES_TYPE )
			{
				populateGalvesThirdPartyVehicleFields( thirdPartyVehicle, vehicleFromGuideBook );
			}
			returnList.add( thirdPartyVehicle );
		}
	}

	return returnList;
}

/**
 * A special method for Galves vehicles.
 * 
 * Galves Bookout uses year, make, model, engine and body as a composite index to retreive a vehicle from the DB. Because of our UNREFACTORED
 * bookout process here, we need to manhandle galves specific data into the exsisting bookout beans that we use.
 * 
 * FOR THE REFACTORING: ThirdPartyVehicle should be an abstract class that each guideBook inherits from to create their instance.
 * 
 * @param selectedThirdPartyVehicle
 * @param vehicleFromGuideBook
 */
private void populateGalvesThirdPartyVehicleFields( ThirdPartyVehicle selectedThirdPartyVehicle, VDP_GuideBookVehicle vehicleFromGuideBook )
{
	selectedThirdPartyVehicle.setMake( vehicleFromGuideBook.getMake() );
	selectedThirdPartyVehicle.setModel( vehicleFromGuideBook.getModel() );
	selectedThirdPartyVehicle.setEngineType( vehicleFromGuideBook.getEngineType() );
	selectedThirdPartyVehicle.setBody( vehicleFromGuideBook.getBody() );
}

public BookValuesBookOutState updateBookValues( GuideBookInput input, Integer thirdPartyId,
												VDP_GuideBookVehicle vehicle, List< ThirdPartyVehicleOption > incomingOptionList,
												BookOutDatasetInfo bookoutDatasetInfo ) throws GBException
{
	
	IThirdPartyBookoutService thirdPartyBookoutService = getThirdPartyBookoutServiceFactory().retrieveThirdPartyBookoutService( thirdPartyId );

	/*
	 * 1) NADA and Kelley needs option selection validation of some sort 2) Blackbook doesn't need to validate option conflicts (???)
	 */

	BookValuesBookOutState state = new BookValuesBookOutState();
	List< VDP_GuideBookValue > bookValuesList = thirdPartyBookoutService.retrieveBookValues( input.getRegion( thirdPartyId.intValue() ),
																								input.getMileage().intValue(),
																								incomingOptionList, vehicle,
																								input.getVin(), bookoutDatasetInfo );

	VDP_GuideBookValue guideBookValue = null;
	if(bookValuesList != null && !bookValuesList.isEmpty())	{
		guideBookValue = (VDP_GuideBookValue)bookValuesList.get( 0 );
	}
	
	if ( guideBookValue == null || guideBookValue.isHasErrors() ) {
		if(bookValuesList != null) {
			Iterator<VDP_GuideBookValue> errorsIter = bookValuesList.iterator();
			while ( errorsIter.hasNext() )
			{
				guideBookValue = (VDP_GuideBookValue)errorsIter.next();
				if ( guideBookValue.getError() != null ) {
					state.addBookOutError( guideBookValue.getError() );
				}
			}
		}
		String regionDescription = thirdPartyBookoutService.retrieveRegionDescription( input );
		GuideBookMetaInfo footer = thirdPartyBookoutService.getMetaInfo( regionDescription, bookoutDatasetInfo );
		state.setMsrp(vehicle.getMsrp());
		state.setWeight(vehicle.getWeight());
		state.setMetaInfo( footer );
		state.setLastBookedOutDate( new Date() );
		state.setOptionsFromBook( incomingOptionList );
	}
	else
	{
		Integer mileageAdjustment = null;
		if(bookValuesList != null) {
			// Extract the mileage adjustment from the bookValues output
			mileageAdjustment = getMileageAdjustmentFromBookValues( bookValuesList );
			state.setBookValues( bookValuesList );
		}
		state.setMsrp(vehicle.getMsrp());
		state.setWeight(vehicle.getWeight());
		
		state.setMileageAdjustment( mileageAdjustment );
		String regionDescription = thirdPartyBookoutService.retrieveRegionDescription( input );
		GuideBookMetaInfo footer = thirdPartyBookoutService.getMetaInfo( regionDescription, bookoutDatasetInfo );
		state.setMetaInfo( footer );
		state.setLastBookedOutDate( new Date() );
		state.setOptionsFromBook( incomingOptionList );
	}
	return state;
}

private Integer getMileageAdjustmentFromBookValues( List<VDP_GuideBookValue> bookValuesList )
{

	Integer mileageAdjustment = null;
	VDP_GuideBookValue gbValue = null, gbRemoveMe = null;
	if(bookValuesList != null) {
		Iterator<VDP_GuideBookValue> iterator = bookValuesList.iterator();
	
		while ( iterator.hasNext() ) {
			gbValue = (VDP_GuideBookValue)iterator.next();
	
			if ( gbValue.getValueType() == BookOutValueType.BOOK_OUT_MILEAGE_ADJUSTMENT ) {
				mileageAdjustment = gbValue.getValue();
				gbRemoveMe = gbValue;
				break;
			}
		}
	
		if ( gbRemoveMe != null )
			bookValuesList.remove( gbRemoveMe );
	}

	return mileageAdjustment;
}

public List< ThirdPartyVehicleOption > retrieveOptionsForVehicle( GuideBookInput input,
																	VDP_GuideBookVehicle guideBookVehicle, Integer thirdPartyId,
																	BookOutDatasetInfo bookoutDatasetInfo ) throws GBException
{

	IThirdPartyBookoutService thirdPartyBookoutService = getThirdPartyBookoutServiceFactory().retrieveThirdPartyBookoutService( thirdPartyId );

	List< ThirdPartyVehicleOption > options = thirdPartyBookoutService.retrieveOptions( input, guideBookVehicle, bookoutDatasetInfo );

	return options;
}

public SelectedVehicleAndOptionsBookOutState lookUpOptionsForSingleVehicle( GuideBookInput input, 
																			Integer thirdPartyId, VDP_GuideBookVehicle guideBookVehicle,
																			BookOutDatasetInfo bookoutDatasetInfo ) throws GBException
{
	SelectedVehicleAndOptionsBookOutState state = new SelectedVehicleAndOptionsBookOutState();
	List<ThirdPartyVehicleOption> options = retrieveOptionsForVehicle( input, guideBookVehicle, thirdPartyId, bookoutDatasetInfo );

	state.setThirdPartyVehicleOptions( options );
	state.setMake( guideBookVehicle.getMake() );
	state.setVehicleYear( guideBookVehicle.getYear() );
	state.setSelectedVehicleKey( guideBookVehicle.getKey() );
	return state;
}

// TODO: a candidate for unit testing
public static String[] determineSelectedEquipmentOptionKeysArray( Collection<ThirdPartyVehicleOption> options )
{
	Collection< String > selectedOptions = determineSelectedOptionKeysForType( options, ThirdPartyOptionType.EQUIPMENT );
	return (String[])selectedOptions.toArray( new String[selectedOptions.size()] );
}

// TODO: a candidate for unit testing
public static Collection< String > determineSelectedOptionKeysForType( Collection<ThirdPartyVehicleOption> options, ThirdPartyOptionType optionType )
{
	List< String > selectedOptions = new ArrayList< String >();
	if ( options != null && !options.isEmpty() )
	{
		Iterator<ThirdPartyVehicleOption> optionsIter = options.iterator();
		ThirdPartyVehicleOption option;
		while ( optionsIter.hasNext() )
		{
			option = optionsIter.next();
			// Hack for something wrong with ThirdPartyVehicleOptions being
			// retrieved but with nulls in the array
			if ( option != null )
			{
				if ( option.getOption() != null )
					if ( option.getOption().getThirdPartyOptionType() != null )
						if ( option.getOption().getThirdPartyOptionType().equals(optionType) && option.isStatus() )
						{
							selectedOptions.add( option.getOptionKey() );
						}
			}
		}
	}
	return selectedOptions;
}

// TODO: a candidate for unit testing
public static List< ThirdPartyVehicleOption > determineSelectedOptionsForType( Collection<ThirdPartyVehicleOption> options, ThirdPartyOptionType optionType )
{
	List< ThirdPartyVehicleOption > selectedOptions = new ArrayList< ThirdPartyVehicleOption >();
	if ( options != null && !options.isEmpty() )
	{
		Iterator<ThirdPartyVehicleOption> optionsIter = options.iterator();
		ThirdPartyVehicleOption option;
		while ( optionsIter.hasNext() )
		{
			option = optionsIter.next();
			// Hack for something wrong with ThirdPartyVehicleOptions being
			// retrieved but with nulls in the array
			if ( option != null )
			{
				if ( option.getOption() != null )
					if ( option.getOption().getThirdPartyOptionType() != null )
						if ( option.getOption().getThirdPartyOptionType().equals(optionType) && option.isStatus() )
						{
							selectedOptions.add( option );
						}
			}
		}
	}
	return selectedOptions;
}

public static Collection< ThirdPartyVehicleOption > determineSelectedOrStandardOptionsForType( Collection<ThirdPartyVehicleOption> options, ThirdPartyOptionType optionType,
																								int thirdPartyId )
{
	List< ThirdPartyVehicleOption > selectedOptions = new ArrayList< ThirdPartyVehicleOption >();
	if ( options != null && !options.isEmpty() )
	{
		Iterator<ThirdPartyVehicleOption> optionsIter = options.iterator();
		ThirdPartyVehicleOption option;
		while ( optionsIter.hasNext() )
		{
			option = optionsIter.next();
			// Hack for something wrong with ThirdPartyVehicleOptions being
			// retrieved but with nulls in the array
			if ( option != null )
			{
				if ( option.getOption() != null )
					if ( option.getOption().getThirdPartyOptionType() != null )
						if ( option.getOption().getThirdPartyOptionType().equals(optionType) )
						{
							// for KBB standard and status are not interchangable!
							if ( ( thirdPartyId != ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE )
									&& ( option.isStatus() || option.isStandardOption() ) )
							{
								selectedOptions.add( option );
							}
							else if ( ( thirdPartyId == ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE ) && option.isStatus() )
							{
								selectedOptions.add( option );
							}
						}
			}
		}
	}
	return selectedOptions;
}

public static String determineSelectedOptionByType( Collection<ThirdPartyVehicleOption> options, ThirdPartyOptionType optionType )
{
	Collection<String> selectedOptions = determineSelectedOptionKeysForType( options, optionType );
	if ( selectedOptions != null && !selectedOptions.isEmpty() )
	{
		return (String)selectedOptions.toArray()[0];
	}
	return null;
}

public static GuideBookInput createGuideBookInput( String vin, Integer mileage, int currentDealerId, String state,
																	int nadaRegionCode, Integer memberId )
{
	GuideBookInput input = new GuideBookInput();
	input.setVin( vin );
	input.setBusinessUnitId( currentDealerId );
	input.setMileage( mileage );
	input.setState( state );
	input.setNadaRegionCode( nadaRegionCode );
	input.setMemberId( memberId );
	return input;
}

public static VDP_GuideBookVehicle determineSelectedVehicleByKey( Collection<VDP_GuideBookVehicle> vehicles, String selectedVehicleKey )
{
	VDP_GuideBookVehicle selectedVehicle = null;
	if ( vehicles != null )
	{
		Iterator<VDP_GuideBookVehicle> vehicleIter = vehicles.iterator();
		VDP_GuideBookVehicle vehicle;
		while ( vehicleIter.hasNext() )
		{
			vehicle = vehicleIter.next();

				if ( vehicle.getKey().equals( selectedVehicleKey ) )
				{
					selectedVehicle = vehicle;
					break;
				}
		}
	}
	return selectedVehicle;
}

/*
 * Populates the selected keys based on the book and those options selected in the pendingAppraisal.
 * 
 */
public static VDP_SelectedKeys retrieveSelectedKeysAndSetSelectedFalse( List< ThirdPartyVehicleOption > allOptions,
																		StagingBookOut pendingBookout )
{
	VDP_SelectedKeys selectedKeys = new VDP_SelectedKeys();
	
	// TODO:  This is added so that allOptions doesn't have to be looped through dozens of times below.  However, all the logic
	// in all the called methods isn't going to be refactored just for my little bug.  In the future, though, where allOptions
	// is being used (especially on the excessive looping), allOptionsMap should be used.
	HashMap<String, ThirdPartyVehicleOption> allOptionsMap = new HashMap<String, ThirdPartyVehicleOption>();
	
	for (ThirdPartyVehicleOption tvo : allOptions)
	{
		if ((pendingBookout != null) && (pendingBookout.getAppraisalSource().isMobile()) &&
				(pendingBookout.getThirdPartyDataProvider().getThirdPartyId() == ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE))
		{
			allOptionsMap.put(tvo.getTransientOptionKey(), tvo);
		}
		else
		{
			allOptionsMap.put(tvo.getOptionKey(), tvo);
		}
	}

	ArrayList< String > selectedEquipmentOptions = new ArrayList< String >();
	String selectedDriveTrainOptions = null;
	String selectedTransmissionOptions = null;
	String selectedEngineOptions = null;
	
	Collection< String > selectedEquipmentOptionsFromBook = null;
	String selectedDriveTrainOptionFromBook = null;
	String selectedTransmissionOptionFromBook = null;
	String selectedEngineOptionFromBook = null;
	
	// Fill in the selected options for non-mobile appraisals.
	if ((pendingBookout == null) || (pendingBookout.getAppraisalSource().isMobile() == false))
	{
		selectedEquipmentOptionsFromBook = determineSelectedOptionKeysForType(
																									allOptions,
																									ThirdPartyOptionType.EQUIPMENT );
		selectedDriveTrainOptionFromBook = determineSelectedOptionByType( allOptions, ThirdPartyOptionType.DRIVETRAIN );
		selectedTransmissionOptionFromBook = determineSelectedOptionByType( allOptions,
																					ThirdPartyOptionType.TRANSMISSION );
		selectedEngineOptionFromBook = determineSelectedOptionByType( allOptions, ThirdPartyOptionType.ENGINE );
	}
	
	// This section of code is for mobile and wavis (and perhaps something else).
	if (pendingBookout != null)
	{
		// Fill in the selected options for mobile appraisals.  Mobile is sending the options it wants set
		// in the pendingBookout object.  Just set  the options to that. 
		// By the way, DriveTrain, Transmission and Engine only gets set for KBB.
		if (pendingBookout.getAppraisalSource().isMobile())
		{
			selectedEquipmentOptionsFromBook = new ArrayList< String >();
			
			for (StagingBookOutOption sbo : pendingBookout.getOptions())
			{
				ThirdPartyVehicleOption tvo = allOptionsMap.get(sbo.getOptionKey());
				
				if (tvo == null)
				{
					System.err.println("BookOutService::retrieveSelectedKeysAndSetSelectedFalse - Got invalid key in staging bookout option: " + sbo.getOptionKey());
					continue;  // This should be an error.  Is there a way to log one?
				}
				
				// The StagingBookOutOption has a thirdPartyOptionType, but it is never set.  Check the
				// thirdPartyOptionType in the accompanying allOptions map to find out what the third party option is.
				if (tvo.getOption().getThirdPartyOptionType() == ThirdPartyOptionType.EQUIPMENT)
				{
					selectedEquipmentOptionsFromBook.add(sbo.getOptionKey());
				}
				else if (tvo.getOption().getThirdPartyOptionType() == ThirdPartyOptionType.DRIVETRAIN)
				{
					selectedDriveTrainOptionFromBook = sbo.getOptionKey();
				}
				else if (tvo.getOption().getThirdPartyOptionType() == ThirdPartyOptionType.TRANSMISSION)
				{
					selectedTransmissionOptionFromBook = sbo.getOptionKey();
				}
				else if (tvo.getOption().getThirdPartyOptionType() == ThirdPartyOptionType.ENGINE)
				{
					selectedEngineOptionFromBook = sbo.getOptionKey();
				}
			}
		}
		
		// Used by mobile, wavis (and others?)
		selectedEquipmentOptions = matchPendingOptions( allOptions, pendingBookout, selectedEquipmentOptionsFromBook,
														ThirdPartyOptionType.EQUIPMENT );
		selectedDriveTrainOptions = matchPendingOption( allOptions, pendingBookout, selectedDriveTrainOptionFromBook,
														ThirdPartyOptionType.DRIVETRAIN );
		selectedTransmissionOptions = matchPendingOption( allOptions, pendingBookout, selectedTransmissionOptionFromBook,
															ThirdPartyOptionType.TRANSMISSION );
		selectedEngineOptions = matchPendingOption( allOptions, pendingBookout, selectedEngineOptionFromBook,
													ThirdPartyOptionType.ENGINE );
		
		// Do this goofiness if this is not a (new as of 2013) mobile appraisal
		if (pendingBookout.getAppraisalSource().isMobile() == false)
		{
			selectedEquipmentOptions.addAll( selectedEquipmentOptionsFromBook );			
		}
	}
	else
	{
		selectedDriveTrainOptions = selectedDriveTrainOptionFromBook;
		selectedTransmissionOptions = selectedTransmissionOptionFromBook;
		selectedEngineOptions = selectedEngineOptionFromBook;
		selectedEquipmentOptions.addAll( selectedEquipmentOptionsFromBook );
	}

	if ( pendingBookout != null )
	{
		// remove any default options that have been set to FALSE in pending
		for ( String selectedGBOption : selectedEquipmentOptionsFromBook )
		{
			for ( StagingBookOutOption pendingOption : pendingBookout.getOptions() )
			{
				if ( !pendingOption.getIsSelected() && pendingOption.getOptionKey().equalsIgnoreCase(selectedGBOption) )
				{
					selectedEquipmentOptions.remove( selectedGBOption );
				}
			}
		}
	}

	selectedKeys.setSelectedEquipmentOptionKeys( (String[])selectedEquipmentOptions.toArray( new String[selectedEquipmentOptions.size()] ) );
	selectedKeys.setSelectedDrivetrainKey( selectedDriveTrainOptions );
	selectedKeys.setSelectedTransmissionKey( selectedTransmissionOptions );
	selectedKeys.setSelectedEngineKey( selectedEngineOptions );

	return selectedKeys;
}

/**
 * This is similar to below but is for a single string value.
 * 
 * @param allOptions
 * @param pendingBookout
 * @param selectedDriveTrainOptionFromBook
 * @param drivetrain_option_type
 * @return
 */
private static String matchPendingOption( List< ThirdPartyVehicleOption > allOptions, StagingBookOut pendingBookout,
											String selectedDriveTrainOptionFromBook, ThirdPartyOptionType optionType )
{
	String matchedOption = selectedDriveTrainOptionFromBook;  //Set a default option
	
	for ( StagingBookOutOption stagingOption : pendingBookout.getOptions() )
	{
		String pendingOptionKey = stagingOption.getOptionKey();
		
		if ( stagingOption.getIsSelected() && stagingOption.getThirdPartyOptionType().getThirdPartyOptionTypeId() == optionType.getThirdPartyOptionTypeId() )
		{
			for ( ThirdPartyVehicleOption gbOption : allOptions )
			{
				String gbOptionKey = gbOption.getOptionKey();
				
				// For black book, the allOptions list has it's option keys set to the description.
				// Black book has an issue with a key having multiple descriptions, so it had to use the
				// description as the key.  However, the pendingBookout has the keys from VVG, which is the
				// blackbook key (from the xml file) prepended with some VVG silliness.  The BlackBookBookoutService
				// (with a little help) is nice enough to provide this key in another member.
				if ((pendingBookout.getAppraisalSource().isMobile()) &&
						(pendingBookout.getThirdPartyDataProvider().getThirdPartyId() == ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE))
				{
					gbOptionKey = gbOption.getTransientOptionKey();
				}
				
				if ( pendingOptionKey.equalsIgnoreCase( gbOptionKey ) )
				{
					matchedOption = pendingOptionKey;
				}
			}
		}
	}
	return matchedOption;
}

private static ArrayList< String > matchPendingOptions( List< ThirdPartyVehicleOption > allOptions, StagingBookOut pendingBookout,
														Collection< String > selectedEquipmentOptionsFromBook, ThirdPartyOptionType optionType)
{
	ArrayList< String > pendingOptionsToAdd = new ArrayList< String >();
	for ( StagingBookOutOption stagingOption : pendingBookout.getOptions() )
	{
		String pendingOptionKey = stagingOption.getOptionKey();
		
		if ( stagingOption.getThirdPartyOptionType().getThirdPartyOptionTypeId() == optionType.getThirdPartyOptionTypeId() )
		{
			for ( ThirdPartyVehicleOption gbOption : allOptions )
			{
				String gbOptionKey = gbOption.getOptionKey();
				
				// For black book, the allOptions list has it's option keys set to the description.
				// Black book has an issue with a key having multiple descriptions, so it had to use the
				// description as the key.  However, the pendingBookout has the keys from VVG, which is the
				// blackbook key (from the xml file) prepended with some VVG silliness.  The BlackBookBookoutService
				// (with a little help) is nice enough to provide this key in another member.
				if ((pendingBookout.getAppraisalSource().isMobile()) &&
					(pendingBookout.getThirdPartyDataProvider().getThirdPartyId() == ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE))
				{
					gbOptionKey = gbOption.getTransientOptionKey();
				}

				if ( pendingOptionKey.equalsIgnoreCase( gbOptionKey ) )
				{
					// Don't do the goofy if selected check if this is a (new of 2013) mobile appraisal.
					// Otherwise:
					// if selected on pending - always select
					// if not selected on pending, but selected by default - DO NOT SELECT
					// if not selected on pending, and not selected by default - nothing
					if ((pendingBookout.getAppraisalSource().isMobile()) || ( !isSelectedByDefault( selectedEquipmentOptionsFromBook, pendingOptionKey )))
					{
						if ( stagingOption.getIsSelected() )
						{ // selected on pending
							pendingOptionsToAdd.add( gbOption.getOptionKey() );
							break;
						}
					}
				}
			}
		}
	}
	return pendingOptionsToAdd;
}

private static boolean isSelectedByDefault( Collection< String > selectedEquipmentOptionsFromBook, String pendingOptionKey )
{
	boolean result = false;
	for ( String fromBook : selectedEquipmentOptionsFromBook )
	{
		if ( fromBook.equalsIgnoreCase( pendingOptionKey ) )
		{
			result = true;
			break;
		}
	}
	return result;
}

public static VDP_SelectedKeys retrieveSelectedKeysAndSetSelectedFalse( List< ThirdPartyVehicleOption > allOptions )
{
	VDP_SelectedKeys selectedKeys = new VDP_SelectedKeys();
	selectedKeys.setSelectedEquipmentOptionKeys( determineSelectedEquipmentOptionKeysArray( allOptions ) );
	selectedKeys.setSelectedDrivetrainKey( determineSelectedOptionByType( allOptions, ThirdPartyOptionType.DRIVETRAIN ) );
	selectedKeys.setSelectedTransmissionKey( determineSelectedOptionByType( allOptions,
																			ThirdPartyOptionType.TRANSMISSION ) );
	selectedKeys.setSelectedEngineKey( determineSelectedOptionByType( allOptions, ThirdPartyOptionType.ENGINE ) );
	return selectedKeys;

}

public static BookOutValue determineAppropriateBookOutValue( BookOut bookOut, int thirdPartyCategoryId ) {
	if ( bookOut != null ) {
		BookOutThirdPartyCategory category = bookOut.getBookOutThirdPartyCategory(thirdPartyCategoryId);
		if ( category != null )	{
			BookOutValue value = category.getFinalBookValue();
			if ( value != null ) {
				return value;
			}
		}
	}
	return null;
}

public static void populateSelectedOptionsList( int guideBookId, List< ThirdPartyVehicleOption > appraisalGuideBookOptions,
												List< ThirdPartyVehicleOption > appraisalGuideBookEngines,
												List< ThirdPartyVehicleOption > appraisalGuideBookDrivetrains,
												List< ThirdPartyVehicleOption > appraisalGuideBookTransmissions, IAppraisal appraisal )
{
	BookOut bookOut = appraisal.getLatestBookOut(guideBookId);
		if (bookOut != null) {
			ThirdPartyVehicle selectedVehicle;
			try {
				selectedVehicle = bookOut.getSelectedThirdPartyVehicle();
				if (guideBookId == ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE) {
					appraisalGuideBookOptions
							.addAll(determineSelectedOrStandardOptionsForType(
									selectedVehicle
											.getThirdPartyVehicleOptions(),
									ThirdPartyOptionType.EQUIPMENT,
									ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE));
					appraisalGuideBookEngines
							.addAll(determineSelectedOrStandardOptionsForType(
									selectedVehicle
											.getThirdPartyVehicleOptions(),
									ThirdPartyOptionType.ENGINE,
									ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE));
					appraisalGuideBookDrivetrains
							.addAll(determineSelectedOrStandardOptionsForType(
									selectedVehicle
											.getThirdPartyVehicleOptions(),
									ThirdPartyOptionType.DRIVETRAIN,
									ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE));
					appraisalGuideBookTransmissions
							.addAll(determineSelectedOrStandardOptionsForType(
									selectedVehicle
											.getThirdPartyVehicleOptions(),
									ThirdPartyOptionType.TRANSMISSION,
									ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE));

				} else {

					appraisalGuideBookOptions
							.addAll(determineSelectedOptionsForType(
									selectedVehicle
											.getThirdPartyVehicleOptions(),
									ThirdPartyOptionType.EQUIPMENT));
					appraisalGuideBookEngines
							.addAll(determineSelectedOptionsForType(
									selectedVehicle
											.getThirdPartyVehicleOptions(),
									ThirdPartyOptionType.ENGINE));
					appraisalGuideBookDrivetrains
							.addAll(determineSelectedOptionsForType(
									selectedVehicle
											.getThirdPartyVehicleOptions(),
									ThirdPartyOptionType.DRIVETRAIN));
					appraisalGuideBookTransmissions
							.addAll(determineSelectedOptionsForType(
									selectedVehicle
											.getThirdPartyVehicleOptions(),
									ThirdPartyOptionType.TRANSMISSION));

				}
			} catch (SelectedThirdPartyVehicleNotFoundException e) {

			}
		}
}

public static Set< BookOutValue > determineAppropriateBookOutValues( BookOut bookOut )
{
	Set< BookOutValue > bookValues = new HashSet< BookOutValue >();
	if ( bookOut != null ) {
		Iterator<BookOutThirdPartyCategory> categoryIter = bookOut.getBookOutThirdPartyCategories().iterator();
		while ( categoryIter.hasNext() ) {
			BookOutThirdPartyCategory category = categoryIter.next();
			BookOutValue value = category.getFinalBookValue();
			if ( value != null ) {
				bookValues.add( value );
			}
		}
	}
	return bookValues;
}

public static List< BookOutValue > determineAppropriateBookOutValuesByType( BookOut bookOut, int bookOutValueType )
{
	List< BookOutValue > bookValues = new ArrayList< BookOutValue >();
	if ( bookOut != null )
	{
		BookOutThirdPartyCategory category;
		Iterator<BookOutThirdPartyCategory> categoryIter = bookOut.getBookOutThirdPartyCategories().iterator();

		// Sort third party categories here by id.
		// BlackBook: ExtraClean/Clean/Average/Rough
		// NADA: Retail/Trade-in/Loan
		Set< BookOutThirdPartyCategory > thirdPartyCategories = bookOut.getBookOutThirdPartyCategories();
		if ( !thirdPartyCategories.isEmpty() )
		{
			ArrayList< BookOutThirdPartyCategory > sortedCategories = new ArrayList< BookOutThirdPartyCategory >( thirdPartyCategories );
			BeanComparator comp = new BeanComparator( "thirdPartyCategory.thirdPartyCategoryId" );
			Collections.sort( sortedCategories, comp );
			categoryIter = sortedCategories.iterator();
		}

		while ( categoryIter.hasNext() ) {
			category = categoryIter.next();
			bookValues.addAll(category.getBookOutValues(bookOutValueType));
		}
	}
	return bookValues;
}

public StoredBookOutState constructCompleteStoredBookOutInfo( final BookOut bookOut, final Vehicle vehicle, Collection<ThirdPartyVehicle> thirdPartyVehicles,
																ThirdPartyVehicle selectedVehicle, Integer thirdPartyId, Integer mileage,
																KBBConditionEnum defaultCondition ) throws GBException
{
	CompleteStoredBookOutState state = new CompleteStoredBookOutState();
	GuideBookMetaInfo metaInfo = new GuideBookMetaInfo();
	metaInfo.setPublishInfo( bookOut.getDatePublished() );
	metaInfo.setGuideBookId( thirdPartyId.intValue() );
	metaInfo.setGuideBookTitle( bookOut.getThirdPartyName() );
	metaInfo.setImageName( bookOut.getThirdPartyDataProvider().getImageName() );
	metaInfo.setFooter( bookOut.getThirdPartyDataProvider().getDefaultFooter() );
	// TODO: account for difference between saved vehicle key and new vehicles
	// from guidebook -KDL

	if ( selectedVehicle != null )
	{
		List< ThirdPartyVehicleOption > storedVehicleOptions = new ArrayList< ThirdPartyVehicleOption >();
		storedVehicleOptions.addAll( selectedVehicle.getThirdPartyVehicleOptions() );
		state.setSelectedVehicleKey( selectedVehicle.getThirdPartyVehicleCode() );
		state.setThirdPartyVehicleOptions( storedVehicleOptions );
	}
	state.setMileageAdjustment( bookOut.getMileageCostAdjustment() );
	state.setMake( vehicle.getMake() );
	state.setModel( vehicle.getModel() );
	state.setVin( vehicle.getVin() );
	// nk - these drop down vehicles are used later to retreive options
	state.setDropDownVehicles( getBookOutAssembler().transformThirdPartyVehiclesToGuideBookVehicles( bookOut, thirdPartyVehicles,
																										vehicle.getVehicleYear(),
																										defaultCondition ) );
	state.setVehicleYear( vehicle.getVehicleYear().toString() );
	state.setMetaInfo( metaInfo );
	state.setBookOutDate( bookOut.getDateCreated() );
	state.setBookoutMileage( mileage );
	state.setBookoutStatus(BookoutStatusEnum.getStatus(bookOut.getBookoutStatusId()));
	state.setMsrp( bookOut.getMsrp() );
	state.setWeight( bookOut.getWeight() );
	state.setBookValues( getBookOutAssembler().transformThirdPartyCategoriesToGuideBookValues( bookOut.getBookOutThirdPartyCategories() ) );
	return state;
}

public StoredBookOutState constructIncompleteStoredBookOutInfo( BookOut bookOut, Vehicle vehicle, List<VDP_GuideBookVehicle> guideBookVehicles,
																List<ThirdPartyVehicleOption> guideBookOptions, Integer thirdPartyId, Integer mileage )
		throws GBException
{
	IncompleteStoredBookOutState state = new IncompleteStoredBookOutState();
	GuideBookMetaInfo metaInfo = new GuideBookMetaInfo();
	metaInfo.setPublishInfo( bookOut.getDatePublished() );
	metaInfo.setGuideBookId( thirdPartyId.intValue() );
	metaInfo.setGuideBookTitle( bookOut.getThirdPartyName() );
	metaInfo.setImageName( bookOut.getThirdPartyDataProvider().getImageName() );
	metaInfo.setFooter( bookOut.getThirdPartyDataProvider().getDefaultFooter() );
	state.setMileageAdjustment( bookOut.getMileageCostAdjustment() );
	state.setMake( vehicle.getMake() );
	state.setModel( vehicle.getModel() );
	state.setVin( vehicle.getVin() );
	state.setDropDownVehicles( guideBookVehicles );
	if ( guideBookVehicles.size() == 1 )
	{
		VDP_GuideBookVehicle selectedVehicle = (VDP_GuideBookVehicle)guideBookVehicles.get( 0 );
		state.setSelectedVehicleKey( selectedVehicle.getKey() );
	}
	else
	{
		state.setSelectedVehicleKey( "" );
	}
	state.setThirdPartyVehicleOptions( guideBookOptions );
	state.setVehicleYear( vehicle.getVehicleYear().toString() );
	state.setMetaInfo( metaInfo );
	state.setBookOutDate( bookOut.getDateCreated() );
	state.setBookoutMileage( mileage );
	state.setBookoutStatus(BookoutStatusEnum.getStatus(bookOut.getBookoutStatusId()));
	state.setMsrp( bookOut.getMsrp() );
	state.setWeight( bookOut.getWeight() );
	state.setBookValues( getBookOutAssembler().transformThirdPartyCategoriesToGuideBookValues( bookOut.getBookOutThirdPartyCategories() ) );
	return state;
}

/**
 * If the most recent bookout for the inventory item and the thirdparty id = NOT REVIEWED, DMS_MILEAGE_CHANGE or USER_MILEAGE_CHANGE then set
 * the status to CLEAN
 * 
 * @param primaryBookId
 * @param inventoryWithBookout
 */
public static void cleanBookOutStatus( int primaryBookId, Set< BookOut > bookouts )
{
	BookOut primaryBookOut = null;
	for( BookOut bookout : bookouts ) {
		if( bookout.getThirdPartyDataProviderId().equals( primaryBookId ) ) {
			primaryBookOut = bookout;
		}
	}
	if ( primaryBookOut.getBookoutStatusId().intValue() == BookoutStatusEnum.NOT_REVIEWED.getId().intValue()
			|| primaryBookOut.getBookoutStatusId().intValue() == BookoutStatusEnum.DMS_MILEAGE_CHANGE.getId().intValue()
			|| primaryBookOut.getBookoutStatusId().intValue() == BookoutStatusEnum.USER_MILEAGE_CHANGE.getId().intValue()
			||primaryBookOut.getBookoutStatusId().intValue() == BookoutStatusEnum.INGROUP_TRANSFER.getId().intValue())
	{
		primaryBookOut.setBookoutStatusId( BookoutStatusEnum.CLEAN.getId() );
	}
}

public static BookOut constructNewBookOut( List<VDP_GuideBookValue> guideBookValues, int guideBookId, Date today, Integer mileage, Integer mileageAdjustment,
											Integer msrp, Integer weight, GuideBookMetaInfo footer, Integer bookOutSourceId )
{
	// Appraisal BookOut Serv
	BookOut bookOut = new BookOut();
	bookOut.setBookOutSourceId( bookOutSourceId );
	bookOut.setDateCreated( today );
	bookOut.setMileageCostAdjustment( mileageAdjustment );
	bookOut.setDatePublished( footer.getPublishInfo() );
	bookOut.setThirdPartyId( Integer.valueOf( guideBookId ) );
	bookOut.setMsrp(msrp);
	bookOut.setWeight(weight);
	ThirdPartyDataProvider thirdParty = ThirdPartyDataProvider.getThirdPartyDataProvider(guideBookId);
	bookOut.setThirdPartyDataProvider( thirdParty );
	// DEFAULT Status.
	bookOut.setBookoutStatusId( BookoutStatusEnum.NOT_REVIEWED.getId() );

	Map<ThirdPartyCategory, BookOutThirdPartyCategory> categoriesCache = new HashMap<ThirdPartyCategory, BookOutThirdPartyCategory>();
	
	if(guideBookValues != null) {
		Iterator<VDP_GuideBookValue> valueIter = guideBookValues.iterator();
		while ( valueIter.hasNext() )
		{
			VDP_GuideBookValue value = valueIter.next();
			
			ThirdPartyCategory thirdPartyCategory = ThirdPartyCategory.fromId(value.getThirdPartyCategoryId());
			BookOutThirdPartyCategory bookOutThirdPartyCategory = null;
			if(!categoriesCache.containsKey(thirdPartyCategory)) {
				bookOutThirdPartyCategory = new BookOutThirdPartyCategory();
				bookOutThirdPartyCategory.setThirdPartyCategory( thirdPartyCategory );
				bookOutThirdPartyCategory.setDateCreated( today );
				bookOutThirdPartyCategory.setBookOut( bookOut );
				
				categoriesCache.put(thirdPartyCategory, bookOutThirdPartyCategory);
				bookOut.addBookOutThirdPartyCategory( bookOutThirdPartyCategory );
			} else {
				bookOutThirdPartyCategory = categoriesCache.get(thirdPartyCategory);
			}
			
			BookOutValueType bookOutValueType = new BookOutValueType();
			bookOutValueType.setBookOutValueTypeId( Integer.valueOf( value.getValueType() ) );
		
			ThirdPartySubCategoryEnum subCategory = ThirdPartySubCategoryEnum.getThirdPartySubCategoryById(value.getThirdPartySubCategoryId());
			BookOutValue bookOutValue = new BookOutValue(bookOutThirdPartyCategory, subCategory, bookOutValueType, value.getValue());
	
			bookOutThirdPartyCategory.addBookOutValue( bookOutValue );
		}
	}
	return bookOut;
}

// Created because the printable pages need a bookout to print.
// Sometimes a bookout is not generated when the guidebook has errors or cannot locate a VIN.
// Dealer/User might still want a printout, which we were unable to do before.
public static BookOut createEmptyBookOutForPrinting( int guideBookId, Integer bookOutSourceId )
{
	BookOut result = constructNewBookOut( new ArrayList<VDP_GuideBookValue>(), guideBookId, new Date(), 
			Integer.valueOf( 0 ), // miles
			Integer.valueOf( 0 ), //weight
			Integer.valueOf( 0 ), // mileageAdj
			Integer.valueOf( 0 ), 
			new GuideBookMetaInfo(), 
			bookOutSourceId );
	result.setBookoutStatusId( BookoutStatusEnum.CLEAN.getId() );
	return result;

}

public Set< ThirdPartyVehicle > createNewThirdPartyVehicles( List< ThirdPartyVehicleOption > options,
																List< VDP_GuideBookVehicle > dropDownVehicles, String selectedVehicleKey,
																int guideBookId, Date today )
{
	Set< ThirdPartyVehicle > thirdPartyVehicles = new HashSet< ThirdPartyVehicle >();
	if ( dropDownVehicles != null )
	{
		for ( VDP_GuideBookVehicle guideBookVehicle : dropDownVehicles )
		{
			ThirdPartyVehicle thirdPartyVehicle = new ThirdPartyVehicle();
			createSingleThirdPartyVehicle( thirdPartyVehicle, selectedVehicleKey, guideBookId, today, guideBookVehicle );
			if ( thirdPartyVehicle.getThirdPartyVehicleCode().equals( selectedVehicleKey ) )
			{
				thirdPartyVehicle.setStatus( true );
				if ( options != null )
				{
					getBookOutAssembler().createNewThirdPartyVehicleOptions( options, thirdPartyVehicle );
				}
			}
			thirdPartyVehicles.add( thirdPartyVehicle );
		}
	}
	return thirdPartyVehicles;
}

private void createSingleThirdPartyVehicle( ThirdPartyVehicle thirdPartyVehicle, String selectedVehicleKey, int guideBookId, Date today,
											VDP_GuideBookVehicle guideBookVehicle )
{
	thirdPartyVehicle.setDateCreated( today );
	thirdPartyVehicle.setThirdPartyId( Integer.valueOf( guideBookId ) );
	thirdPartyVehicle.setThirdPartyVehicleCode( guideBookVehicle.getKey() );
	thirdPartyVehicle.setDescription( guideBookVehicle.getDescription() );
	thirdPartyVehicle.setMakeCode( guideBookVehicle.getMakeId() );
	thirdPartyVehicle.setModelCode( guideBookVehicle.getModelId() );
	thirdPartyVehicle.setModel( guideBookVehicle.getModel() );
	thirdPartyVehicle.setMake( guideBookVehicle.getMake() );
	thirdPartyVehicle.setEngineType( guideBookVehicle.getEngineType() );
	thirdPartyVehicle.setBody( guideBookVehicle.getBody() );
}

/**
 * Make new ThirdPartyVehicles and copy data from the old ThirdPartyVehicles
 * 
 * @param options
 * @param selectedVehicleKey
 * @param today
 * @param thirdPartyVehicles
 * @param guideBookId
 * @param dropDownVehicles
 * @return TODO
 */
public Set< ThirdPartyVehicle > updateExistingThirdPartyVehicles( List< ThirdPartyVehicleOption > options, String selectedVehicleKey,
																	Date today, final Collection< ThirdPartyVehicle > thirdPartyVehicles,
																	int guideBookId, List< VDP_GuideBookVehicle > dropDownVehicles )
{
	Set< ThirdPartyVehicle > updatedThirdPartyVehicles = new HashSet< ThirdPartyVehicle >();
	boolean foundMatch = false;
	KBBConditionEnum condition = ( dropDownVehicles != null && !dropDownVehicles.isEmpty() ) ? ( (VDP_GuideBookVehicle)dropDownVehicles.get( 0 ) ).getCondition()
			: KBBConditionEnum.EXCELLENT;
	if(condition==null)
		condition= KBBConditionEnum.EXCELLENT;
	for ( ThirdPartyVehicle oldThirdPartyVehicle : thirdPartyVehicles )
	{
		ThirdPartyVehicle thirdPartyVehicle = new ThirdPartyVehicle( oldThirdPartyVehicle );
		updatedThirdPartyVehicles.add( thirdPartyVehicle );
		if ( thirdPartyVehicle.getThirdPartyId().intValue() == guideBookId )
		{
			if ( thirdPartyVehicle.getThirdPartyVehicleCode().equals( selectedVehicleKey ) )
			{
				thirdPartyVehicle.setStatus( true );
				foundMatch = true;
				getBookOutAssembler().updateThirdPartyVehicleOptions( options, thirdPartyVehicle, condition );
			}
			else
			{
				thirdPartyVehicle.setStatus( false );
			}
			thirdPartyVehicle.setDateModified( today );
		}
	}

	// this is to handle the migration from vehicle code moving from index in
	// drop down list (stupid)
	// to model code (smart) for KBB - DW 1/9/06
	if ( !foundMatch && guideBookId == ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE )
	{
		ThirdPartyVehicle thirdPartyVehicle = null;
		Iterator<ThirdPartyVehicle> tpIter = thirdPartyVehicles.iterator();
		while ( tpIter.hasNext() )
		{
			thirdPartyVehicle = tpIter.next();
			if ( thirdPartyVehicle.isStatus() ) {
				break;
			}
		}

		Iterator<VDP_GuideBookVehicle> ddvIter = dropDownVehicles.iterator();
		VDP_GuideBookVehicle dropDownVehicle;
		while ( ddvIter.hasNext() ) {
			dropDownVehicle = ddvIter.next();
			if ( dropDownVehicle.getModelId().equals( selectedVehicleKey )) {
				if ( thirdPartyVehicle == null ) {
					thirdPartyVehicle = new ThirdPartyVehicle();
				}
				createSingleThirdPartyVehicle( thirdPartyVehicle, selectedVehicleKey, guideBookId, today, dropDownVehicle );
				thirdPartyVehicle.setStatus( true );
				getBookOutAssembler().updateThirdPartyVehicleOptions( options, thirdPartyVehicle, condition );
			} else if (dropDownVehicle.getKey().equals(selectedVehicleKey)) {
				for(ThirdPartyVehicle vehicles : updatedThirdPartyVehicles) {
					vehicles.setStatus(false);
				}
				
				ThirdPartyVehicle v = new ThirdPartyVehicle();
				v.setDateCreated( today );
				v.setThirdPartyId( Integer.valueOf( guideBookId ) );
				v.setThirdPartyVehicleCode( dropDownVehicle.getKey() );
				v.setDescription( dropDownVehicle.getDescription() );
				v.setMakeCode( dropDownVehicle.getMakeId() );
				v.setModelCode( dropDownVehicle.getModelId() );
				v.setModel( dropDownVehicle.getModel() );
				v.setMake( dropDownVehicle.getMake() );
				v.setEngineType( dropDownVehicle.getEngineType() );
				v.setBody( dropDownVehicle.getBody() );
				v.setStatus( true );
				getBookOutAssembler().updateThirdPartyVehicleOptions( options, thirdPartyVehicle, condition );
				
				updatedThirdPartyVehicles.add(v);
			}
		}
	}
	return updatedThirdPartyVehicles;
}

public void flagBookoutIfInaccurateAndSaveIfSo( Integer inventoryMileage, BookOut bookOut, StoredBookOutState state )
{
	if ( state != null )
	{
		state.setBookoutStatus(BookoutStatusEnum.getStatus(bookOut.getBookoutStatusId()));
	}
	// TODO: write unit test
	if ( state instanceof CompleteStoredBookOutState && !state.isInAccurate() )
	{
		if ( inventoryMileage != null )
		{
			if ( state.getBookoutMileage() == null ) {
				state.setBookoutMileage( Integer.valueOf( 0 ) );
			}
			
			if ( state.getBookoutMileage().intValue() != inventoryMileage.intValue() )
			{
				if ( state != null )
				{
					state.setBookoutStatus(BookoutStatusEnum.USER_MILEAGE_CHANGE );
				}
			}
		}
	}
}

public BookOutAssembler getBookOutAssembler()
{
	return bookOutAssembler;
}

public void setBookOutAssembler( BookOutAssembler bookOutAssembler )
{
	this.bookOutAssembler = bookOutAssembler;
}

public ThirdPartyBookoutServiceFactory getThirdPartyBookoutServiceFactory()
{
	return thirdPartyBookoutServiceFactory;
}

public void setThirdPartyBookoutServiceFactory( ThirdPartyBookoutServiceFactory thirdPartyBookoutServiceFactory )
{
	this.thirdPartyBookoutServiceFactory = thirdPartyBookoutServiceFactory;
}

//Method will get models on the basis of year and make. This model will further encode to the different trims used only by the KBB Service//
private VehiclesNoOptionsBookOutState getKBBTrimsByModel(IThirdPartyBookoutService thirdPartyBookoutService, List<VDP_GuideBookVehicle> vdp_guideBookVehicles,
			 GuideBookInput input, Integer thirdPartyId,
				BookOutDatasetInfo bookoutDatasetInfo) throws GBException 
	{
		GuideBookMetaInfo bookMetaInfo = thirdPartyBookoutService.getMetaInfo( input.getRegion( thirdPartyId.intValue() ), bookoutDatasetInfo );
	//	vdp_guideBookVehicles= thirdPartyBookoutService.retrieveModels(year, makeId, bookoutDatasetInfo);
		
		String regionDescription = thirdPartyBookoutService.retrieveRegionDescription( input );
		GuideBookMetaInfo footer = thirdPartyBookoutService.getMetaInfo( regionDescription, bookoutDatasetInfo );
		VehiclesNoOptionsBookOutState multiVehiclesState = new VehiclesNoOptionsBookOutState();
		List<VDP_GuideBookVehicle> vdp_guideBookVehicleTrims= new ArrayList<VDP_GuideBookVehicle>();
		
		Integer year = null;
		try	{
			year = VinUtility.getLatestModelYear( input.getVin() );
		}
		catch (InvalidVinException e) {
			throw new GBException( "VIN contained invalid year character.");
		}
		
		vdp_guideBookVehicles = thirdPartyBookoutService.retrieveModelsByMakeIDAndYear( input.getVin(),year,bookoutDatasetInfo ); 
		if(vdp_guideBookVehicles == null || vdp_guideBookVehicles.size()==0){
			  VinNotFoundException exception = new VinNotFoundException( input.getVin(),
			                 input.getBusinessUnitId(),
			                 bookMetaInfo.getGuideBookTitle(),
			                 new Date() );
			  throw exception;
		}
		if(input.getSelectedModelId()!="" ){
			String makeCode=null;
			String model="";
			if(vdp_guideBookVehicles!=null && !vdp_guideBookVehicles.isEmpty()){
			for (VDP_GuideBookVehicle vdpGuideBookVehicle : vdp_guideBookVehicles) {
				if((vdpGuideBookVehicle.getModelId()).equals(input.getSelectedModelId())){
					makeCode = vdpGuideBookVehicle.getMakeId();
					model= vdpGuideBookVehicle.getModel();
					break;
					}
	
				}
			}
			List<VDP_GuideBookVehicle> vdp_guideBookVehicleTrimsTemp= thirdPartyBookoutService.retrieveTrims( year+"",null, input.getSelectedModelId(),  bookoutDatasetInfo );
			//List<VDP_GuideBookVehicle> vdp_guideBookVehicleTrimsTemp = thirdPartyBookoutService.retrieveTrimsWithoutYear( null, input.getSelectedModelId(),  bookoutDatasetInfo );
			
			if(vdp_guideBookVehicleTrimsTemp!=null && !vdp_guideBookVehicleTrimsTemp.isEmpty()){
				vdp_guideBookVehicleTrims= new ArrayList<VDP_GuideBookVehicle>();
				for (VDP_GuideBookVehicle vdpGuideBookVehicle : vdp_guideBookVehicleTrimsTemp) {
					vdpGuideBookVehicle.setMake(input.getMake());
					vdpGuideBookVehicle.setModel(model);
					vdpGuideBookVehicle.setYear(year+"");
					vdpGuideBookVehicle.setMakeId(makeCode);
					vdpGuideBookVehicle.setModelId(input.getSelectedModelId());
					vdpGuideBookVehicle.setVin(input.getVin());
					vdpGuideBookVehicle.setSeries(input.getModel()+ " " +vdpGuideBookVehicle.getTrim());
					vdp_guideBookVehicleTrims.add(vdpGuideBookVehicle);
				}
			}
		}
		
		multiVehiclesState.setDropDownVehicles( vdp_guideBookVehicleTrims );
		multiVehiclesState.setDropDownModels(vdp_guideBookVehicles);
		multiVehiclesState.setMetaInfo( footer );
		multiVehiclesState.setRegionDescription( regionDescription );
		multiVehiclesState.setMake( input.getMake());
		multiVehiclesState.setVehicleYear( year+"" );
		multiVehiclesState.setModel( input.getModel() );
		
		return multiVehiclesState;

	}


public boolean findUpgrade(int currentdealerID, int upgradeCD)
{
	System.out.println("C "+currentdealerID+" "+"U "+upgradeCD);
	DealerUpgrade dealerUpgrade = dealerPrefDAO.findUpgrade(currentdealerID,upgradeCD);
	boolean hasKBB = false;
	if(dealerUpgrade!=null){
		
		hasKBB=dealerUpgrade.isActive();
	}
	return hasKBB;
}
public DealerPreferenceDAO getDealerPrefDAO()
{
	return dealerPrefDAO;
}

public void setDealerPrefDAO( DealerPreferenceDAO dealerPrefDAO )
{
	this.dealerPrefDAO = dealerPrefDAO;
}

}
