package com.firstlook.service.bookout.bean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import biz.firstlook.module.vehicle.description.old.BookOutError;
import biz.firstlook.module.vehicle.description.old.GuideBookMetaInfo;
import biz.firstlook.module.vehicle.description.old.KBBConditionEnum;
import biz.firstlook.module.vehicle.description.old.NadaPriceConditionMarketTypeComparator;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.enumerator.BookoutStatusEnum;
import biz.firstlook.transact.persist.enumerator.ThirdPartySubCategoryEnum;
import biz.firstlook.transact.persist.model.BookOutValueType;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.service.appraisal.StagingBookOut;

import com.firstlook.display.tradeanalyzer.DisplayGuideBook;
import com.firstlook.persistence.states.StatesDAO;

public abstract class AbstractBookOutState
{

private String regionDescription;
private GuideBookMetaInfo metaInfo;
private Map<Integer, List<BookOutError>> bookOutErrors = new HashMap<Integer, List<BookOutError>>();
private BookoutStatusEnum bookoutStatus = BookoutStatusEnum.CLEAN;
private int bookOutSourceId;

public abstract DisplayGuideBook transformBookOutToDisplayData( DisplayGuideBook displayGuideBook );
public abstract String getSelectedVehicleKey();
public abstract List<ThirdPartyVehicleOption> getThirdPartyVehicleOptions();
public abstract List<VDP_GuideBookVehicle> getDropDownVehicles();
public abstract List<VDP_GuideBookVehicle> getDropDownModels();

public static void determineKelleyBaseValues( DisplayGuideBook displayGuideBook, List<VDP_GuideBookValue> bookValues )
{
	List<VDP_GuideBookValue> kelleyBaseValues = retrieveThirdPartyValuesByValueType( bookValues, BookOutValueType.BOOK_OUT_VALUE_BASE_VALUE );
	Iterator<VDP_GuideBookValue> baseValueIter = kelleyBaseValues.iterator();
	VDP_GuideBookValue value;
	while ( baseValueIter.hasNext() ) {
		value = baseValueIter.next();
		if ( value.getThirdPartyCategoryId() == ThirdPartyCategory.KELLEY_RETAIL_TYPE )
		{
			displayGuideBook.setKelleyRetailBaseValue( value );
		}
		if ( value.getThirdPartyCategoryId() == ThirdPartyCategory.KELLEY_WHOLESALE_TYPE )
		{
			displayGuideBook.setKelleyWholesaleBaseValue( value );
		}
		if ( value.getThirdPartyCategoryId() == ThirdPartyCategory.KELLEY_TRADEIN_TYPE )
		{
			displayGuideBook.setKelleyTradeInBaseValue( value );
		}
	}
}

public static void determineKelleyNoMileageValues( DisplayGuideBook displayGuideBook, List<VDP_GuideBookValue> bookValues  )
{
	List<VDP_GuideBookValue> kelleyNoMileageValues = retrieveThirdPartyValuesByValueType( bookValues, BookOutValueType.BOOK_OUT_VALUE_MILEAGE_INDEPENDENT );
	Iterator<VDP_GuideBookValue> noMileageValueIter = kelleyNoMileageValues.iterator();
	VDP_GuideBookValue value;
	while ( noMileageValueIter.hasNext() )
	{
		value = noMileageValueIter.next();
		if ( value.getThirdPartyCategoryId() == ThirdPartyCategory.KELLEY_RETAIL_TYPE )
		{
			displayGuideBook.setKelleyRetailValueNoMileage( value );
		}
		if ( value.getThirdPartyCategoryId() == ThirdPartyCategory.KELLEY_WHOLESALE_TYPE )
		{
			displayGuideBook.setKelleyWholesaleValueNoMileage( value );
		}
		if ( value.getThirdPartyCategoryId() == ThirdPartyCategory.KELLEY_TRADEIN_TYPE )
		{
			displayGuideBook.setKelleyTradeInValueNoMileage( value );
		}
		if ( value.getThirdPartyCategoryId() == ThirdPartyCategory.KELLY_TRADEIN_RANGELOW_TYPE )
		{
			displayGuideBook.setKelleyTradeInRangeLowValueNoMileage( value );
		}
		if ( value.getThirdPartyCategoryId() == ThirdPartyCategory.KELLY_TRADEIN_RANGEHIGH_TYPE )
		{
			displayGuideBook.setKelleyTradeInRangeHighValueNoMileage( value );
		}
	}
}

public static void determineKelleyFinalValues( DisplayGuideBook displayGuideBook, List<VDP_GuideBookValue> bookValues  )
{
	List<VDP_GuideBookValue> kelleyFinalValues = retrieveThirdPartyValuesByValueType( bookValues, BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE );
	Iterator<VDP_GuideBookValue> finalValueIter = kelleyFinalValues.iterator();
	VDP_GuideBookValue value;
	while ( finalValueIter.hasNext() ) {
		value = finalValueIter.next();
		if ( value.getThirdPartyCategoryId() == ThirdPartyCategory.KELLEY_RETAIL_TYPE )
		{
			displayGuideBook.setKelleyRetailValue( value );
		}
		if ( value.getThirdPartyCategoryId() == ThirdPartyCategory.KELLEY_WHOLESALE_TYPE )
		{
			displayGuideBook.setKelleyWholesaleValue( value );
		}
		if ( value.getThirdPartyCategoryId() == ThirdPartyCategory.KELLEY_TRADEIN_TYPE )
		{
			displayGuideBook.setKelleyTradeInValue( value );
		}
		if ( value.getThirdPartyCategoryId() == ThirdPartyCategory.KELLY_TRADEIN_RANGELOW_TYPE )
		{
			displayGuideBook.setKelleyTradeInRangeLowValue( value );
		}
		if ( value.getThirdPartyCategoryId() == ThirdPartyCategory.KELLY_TRADEIN_RANGEHIGH_TYPE )
		{
			displayGuideBook.setKelleyTradeInRangeHighValue( value );
		}
	}
}

public static String determineKelleyPublishState( String publishInfo )
{
	if( publishInfo != null && !publishInfo.equals(""))
	{
		StatesDAO statesPersistence = new StatesDAO();
		return statesPersistence.findStateLongName( publishInfo.substring(0, 2) ) ;
	}
	else
	{
		return "";
	}
}

protected static KBBConditionEnum getConditionFromValue( VDP_GuideBookValue kelleyTradeInValue )
{
	if ( kelleyTradeInValue == null )
	{
		return KBBConditionEnum.EXCELLENT;
	}
	if ( kelleyTradeInValue.getThirdPartySubCategoryId() == ThirdPartySubCategoryEnum.KBB_TRADEIN_EXCELLENT.getThirdPartySubCategoryId().intValue() )	{
		return KBBConditionEnum.EXCELLENT;
	} else if ( kelleyTradeInValue.getThirdPartySubCategoryId() == ThirdPartySubCategoryEnum.KBB_TRADEIN_GOOD.getThirdPartySubCategoryId().intValue() )	{
		return KBBConditionEnum.GOOD;
	} else if ( kelleyTradeInValue.getThirdPartySubCategoryId() == ThirdPartySubCategoryEnum.KBB_TRADEIN_FAIR.getThirdPartySubCategoryId().intValue() )	{
		return KBBConditionEnum.FAIR;
	} 
	return null;
}

public Map<Integer, List<BookOutError>> getBookOutErrors() {
	return bookOutErrors;
}

public void setBookOutErrors( Map<Integer, List<BookOutError>> bookOutErrors )
{
	this.bookOutErrors = bookOutErrors;
}

public void addBookOutError( BookOutError error ) {
	Integer type = Integer.valueOf( error.getErrorType() );
	List<BookOutError> errors = bookOutErrors.get(type);
	if(errors == null) {
		errors = new ArrayList<BookOutError>();
	}
	errors.add(error);
	bookOutErrors.put( type, errors );
}

public GuideBookMetaInfo getMetaInfo()
{
	return metaInfo;
}

public void setMetaInfo( GuideBookMetaInfo footer ) {
	this.metaInfo = footer;
}

public static List<VDP_GuideBookValue> sortThirdPartyValues( ThirdPartyDataProvider thirdPartyDataProvider, List<VDP_GuideBookValue> thirdPartyValues )
{
	List<VDP_GuideBookValue> sortedThirdPartyVehicles = new ArrayList<VDP_GuideBookValue>();
	sortedThirdPartyVehicles.addAll( thirdPartyValues );
	
	Comparator<VDP_GuideBookValue> comparator = new Comparator<VDP_GuideBookValue>(){
		public int compare(final VDP_GuideBookValue v0, final VDP_GuideBookValue v1) {
			if(v0 == null || v1 == null)
				throw new NullPointerException("Cannot compare null VDP_GuideBookValues!");
			
			return Integer.valueOf(v0.getThirdPartyCategoryId()).compareTo(v1.getThirdPartyCategoryId());
		}
	};
	if(ThirdPartyDataProvider.NADA.equals(thirdPartyDataProvider)) {
		comparator = new NadaPriceConditionMarketTypeComparator();
	}
	
	Collections.sort( sortedThirdPartyVehicles, comparator); 
	
	return sortedThirdPartyVehicles;
}

public static List<VDP_GuideBookValue> retrieveThirdPartyValuesByValueType( List<VDP_GuideBookValue> thirdPartyValues, int thirdPartyValueType )
{
	List<VDP_GuideBookValue> thirdPartyValuesByType = new ArrayList<VDP_GuideBookValue>();
	for( VDP_GuideBookValue value : thirdPartyValues )
	{
		if( value.getValueType() == thirdPartyValueType )
		{
			thirdPartyValuesByType.add( value );
		}
	}
	return thirdPartyValuesByType;
}


protected VDP_GuideBookValue getOptionsValue( List<VDP_GuideBookValue> bookValues )
{
	for( VDP_GuideBookValue value: bookValues )
	{
		if ( value.getValueType() == BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE )
		{
			return value;
		}
	}
	return null;
}

public String getRegionDescription()
{
	return regionDescription;
}

public void setRegionDescription( String regionDescription )
{
	this.regionDescription = regionDescription;
}

public boolean isInAccurate()
{
	if( bookoutStatus.getId().intValue() == BookoutStatusEnum.DMS_MILEAGE_CHANGE.getId().intValue() 
			|| bookoutStatus.getId().intValue() == BookoutStatusEnum.NOT_REVIEWED.getId().intValue() 
			|| bookoutStatus.getId().intValue() == BookoutStatusEnum.USER_MILEAGE_CHANGE.getId().intValue()
			|| bookoutStatus.getId().intValue() == BookoutStatusEnum.INGROUP_TRANSFER.getId().intValue())
	{
		return true;
	}
	else
	{
		return false;
	}
}
public int getBookOutSourceId()
{
	return bookOutSourceId;
}
public void setBookOutSourceId( int bookOutSourceId )
{
	this.bookOutSourceId = bookOutSourceId;
}
public BookoutStatusEnum getBookoutStatus() {
	return bookoutStatus;
}
public void setBookoutStatus(BookoutStatusEnum bookoutStatus) {
	this.bookoutStatus = bookoutStatus;
}

public void setStagingBookOut(StagingBookOut bookout){
	
}

}
