package com.firstlook.service.bookout.bean;

import java.util.Collection;
import java.util.List;

import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.model.BookOutValueType;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

import com.firstlook.display.tradeanalyzer.DisplayGuideBook;
import com.firstlook.service.bookout.BookOutService;
import com.firstlook.thirdparty.book.old.VDP_SelectedKeys;

public class IncompleteStoredBookOutState extends StoredBookOutState
{

public DisplayGuideBook transformBookOutToDisplayData( DisplayGuideBook displayGuideBook )
{
	displayGuideBook.setGuideBookName( getMetaInfo().getGuideBookTitle() );
	if ( getMetaInfo().getPublishInfo() != null )
	{
		displayGuideBook.setPublishInfo( getMetaInfo().getPublishInfo() );
	}
	else
	{
		displayGuideBook.setPublishInfo( "" );
	}
	displayGuideBook.setFooter( getMetaInfo().getFooter() );
	displayGuideBook.setImageName( getMetaInfo().getImageName() );
	displayGuideBook.setBgColor( getMetaInfo().getBgColor() );
	displayGuideBook.setRegion( super.getRegionDescription() );
	displayGuideBook.setMsrp( getMsrp() );
	displayGuideBook.setWeight( getWeight() );
	displayGuideBook.setGuideBookId( new Integer( getMetaInfo().getGuideBookId() ) );
	displayGuideBook.setMake( getMake() );
	displayGuideBook.setModel( getModel() );
	displayGuideBook.setYear( getVehicleYear() );
	displayGuideBook.setTradeAnalyzedDate( getBookOutDate().toString() );
	List<ThirdPartyVehicleOption> allOptions = getThirdPartyVehicleOptions();
	VDP_SelectedKeys selectedKeys = BookOutService.retrieveSelectedKeysAndSetSelectedFalse( allOptions );
	displayGuideBook.setDisplayGuideBookOptions( allOptions );
	displayGuideBook.setSelectedDrivetrainKey( selectedKeys.getSelectedDrivetrainKey() );
	displayGuideBook.setSelectedEngineKey( selectedKeys.getSelectedEngineKey() );
	displayGuideBook.setSelectedTransmissionKey( selectedKeys.getSelectedTransmissionKey() );
	displayGuideBook.setSelectedEquipmentOptionKeys( selectedKeys.getSelectedEquipmentOptionKeys() );
	displayGuideBook.setSuccess( true );
	
	Collection<ThirdPartyVehicleOption> selectedEquipmentOptions = BookOutService.determineSelectedOrStandardOptionsForType( allOptions, ThirdPartyOptionType.EQUIPMENT,
	                                                                                                getMetaInfo().getGuideBookId());
	displayGuideBook.setSelectedOptions( selectedEquipmentOptions );
	
	final int guideBookId = displayGuideBook.getGuideBookId().intValue();
	ThirdPartyDataProvider provider = ThirdPartyDataProvider.getThirdPartyDataProvider(guideBookId);
	if( ThirdPartyDataProvider.KELLEY.equals(provider) ) {
		determineKelleyBaseValues( displayGuideBook, getBookValues() );
		determineKelleyNoMileageValues( displayGuideBook, getBookValues() );
		determineKelleyFinalValues( displayGuideBook, getBookValues() );
		displayGuideBook.setKelleyPublishState( determineKelleyPublishState( displayGuideBook.getPublishInfo() ) );
		displayGuideBook.setKelleyOptionsAdjustment( getOptionsValue( getBookValues() ) );
		displayGuideBook.setCondition( getConditionFromValue( displayGuideBook.getKelleyTradeInValue() ) );
	} else if (ThirdPartyDataProvider.NADA.equals(provider)) {
		displayGuideBook.setGuideBookOptionAdjustment(getOptionsValue(getBookValues()));
	}
	
	List<VDP_GuideBookValue> displayGuideBookValues = retrieveThirdPartyValuesByValueType( getBookValues(), BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE );
	List<VDP_GuideBookValue> displayGuideBookValuesNoMileage = retrieveThirdPartyValuesByValueType( getBookValues(),
																				BookOutValueType.BOOK_OUT_VALUE_MILEAGE_INDEPENDENT );
	displayGuideBook.setKelleyOptionsAdjustment( getOptionsValue( getBookValues() ) );
	displayGuideBook.setDisplayGuideBookValues( sortThirdPartyValues( provider, displayGuideBookValues ) );
	if ( displayGuideBookValuesNoMileage != null )
	{
		displayGuideBook.setDisplayGuideBookValuesNoMileage( sortThirdPartyValues( provider, displayGuideBookValuesNoMileage ) );
	}

	displayGuideBook.setDisplayGuideBookVehicles( getDropDownVehicles() );
	displayGuideBook.setDisplayGuideBookModels(getDropDownModels());
	displayGuideBook.setGuideBookId( Integer.valueOf( getMetaInfo().getGuideBookId() ) );
	displayGuideBook.setMileageAdjustment( getMileageAdjustment() );
	displayGuideBook.setValidVin( true );
    displayGuideBook.setInAccurate( isInAccurate() );
	displayGuideBook.setLastBookedOutDate( getBookOutDate() );
	displayGuideBook.setSelectedVehicleKey( getSelectedVehicleKey() );
	displayGuideBook.setSelectedModelKey(getSelectedModelKey());

	displayGuideBook.setStoredInfo( true );
	
	return displayGuideBook;
}

public VDP_GuideBookVehicle getSelectedVehicle()
{
	return null;
}



}
