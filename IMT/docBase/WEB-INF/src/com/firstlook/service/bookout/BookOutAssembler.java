package com.firstlook.service.bookout;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import biz.firstlook.module.vehicle.description.old.KBBConditionEnum;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.enumerator.ThirdPartySubCategoryEnum;
import biz.firstlook.transact.persist.model.BookOut;
import biz.firstlook.transact.persist.model.BookOutThirdPartyCategory;
import biz.firstlook.transact.persist.model.BookOutValue;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOptionValue;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOptionValueType;

import com.firstlook.dealer.tools.GuideBook;

public class BookOutAssembler
{

public BookOutAssembler()
{
	super();
}

// nk - This is te guideBookVehicle is used in GalvesService to retreive options.
// To work it needs Make, Model, EngType, Year and Manuf
public List<VDP_GuideBookVehicle> transformThirdPartyVehiclesToGuideBookVehicles( BookOut bookout, Collection<ThirdPartyVehicle> thirdPartyVehicles, Integer year, KBBConditionEnum defaultCondition )
{
	// this is for KBB to get conditions to carry over for Trade-In bookouts.
	KBBConditionEnum condition = null;
	if ( bookout.getThirdPartyId() == ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE )
	{
		Set<ThirdPartySubCategoryEnum> thirdPartyCategoryIds = getThirdPartySubCategoryIds( bookout );
		if ( thirdPartyCategoryIds.contains( ThirdPartySubCategoryEnum.KBB_TRADEIN_EXCELLENT ) )
		{
			condition = KBBConditionEnum.EXCELLENT;
		} else if ( thirdPartyCategoryIds.contains( ThirdPartySubCategoryEnum.KBB_TRADEIN_GOOD ) )
		{
			condition = KBBConditionEnum.GOOD;
		} else if ( thirdPartyCategoryIds.contains( ThirdPartySubCategoryEnum.KBB_TRADEIN_FAIR ) )
		{
			condition = KBBConditionEnum.FAIR;
		} else {
			condition = defaultCondition; // excllent
		}
	}
	
	List<VDP_GuideBookVehicle> guideBookVehicles = new ArrayList<VDP_GuideBookVehicle>();
	if ( thirdPartyVehicles != null )
	{
		Iterator<ThirdPartyVehicle> vehicleIter = thirdPartyVehicles.iterator();
		ThirdPartyVehicle vehicle;
		VDP_GuideBookVehicle guideBookVehicle;
		List<ThirdPartyVehicleOption> guideBookOptions;
		while ( vehicleIter.hasNext() )
		{
			vehicle = vehicleIter.next();
			guideBookOptions = new ArrayList<ThirdPartyVehicleOption>();
			guideBookOptions.addAll( vehicle.getThirdPartyVehicleOptions() );
			guideBookVehicle = new VDP_GuideBookVehicle();
			guideBookVehicle.setKey( vehicle.getThirdPartyVehicleCode() );
			guideBookVehicle.setDescription( vehicle.getDescription() );
			guideBookVehicle.setMakeId( vehicle.getMakeCode() );
			guideBookVehicle.setModelId( vehicle.getModelCode() );
			guideBookVehicle.setYear( year.toString() );
			guideBookVehicle.setGuideBookOptions( guideBookOptions );
			
			// nk - only galves will have these - other books will just leave 
			// these values empty. Necessary bc galves indexes on these.
			guideBookVehicle.setMake(vehicle.getMake());
			guideBookVehicle.setModel(vehicle.getModel());
			guideBookVehicle.setEngineType(vehicle.getEngineType());
			guideBookVehicle.setBody(vehicle.getBody());
			// end galves specific logic 
			
			// KBB specific
			guideBookVehicle.setCondition( condition );
			
			guideBookVehicles.add( guideBookVehicle );
		}
	}

	return guideBookVehicles;
}

private Set< ThirdPartySubCategoryEnum > getThirdPartySubCategoryIds( BookOut bookout )
{
	Set< ThirdPartySubCategoryEnum > tpcs = new HashSet< ThirdPartySubCategoryEnum >();
	for ( BookOutThirdPartyCategory tpc : bookout.getBookOutThirdPartyCategories() )
	{
		if ( tpc.getThirdPartyCategoryId().intValue() == ThirdPartyCategory.KELLEY_TRADEIN_TYPE )
		{
			for ( BookOutValue value : tpc.getBookOutValues() )
			{
				tpcs.add( ThirdPartySubCategoryEnum.getThirdPartySubCategoryById( value.getThirdPartySubCategoryId() ) );
			}
		}
	}
	return tpcs;
}

protected List<VDP_GuideBookValue> transformThirdPartyCategoriesToGuideBookValues( Collection<BookOutThirdPartyCategory> thirdPartyCategories )
{
	List<VDP_GuideBookValue> guideBookValues = new ArrayList<VDP_GuideBookValue>();
	if ( thirdPartyCategories != null )	{
		for(BookOutThirdPartyCategory category : thirdPartyCategories) {
			Set<BookOutValue> bookoutValues = category.getBookOutValues();
			for (BookOutValue bookOutValue : bookoutValues ) {
				VDP_GuideBookValue guideBookValue = new VDP_GuideBookValue(
						bookOutValue.getValue(),
						category.getThirdPartyCategoryId().intValue(),
						category.getThirdPartyCategory().getCategory(),
						bookOutValue.getBookOutValueType().getBookOutValueTypeId().intValue(),
						bookOutValue.getThirdPartySubCategoryId());
				guideBookValues.add( guideBookValue );
			}
		}
	}

	return guideBookValues;
}

public void createNewThirdPartyVehicleOptions( List<ThirdPartyVehicleOption> options, ThirdPartyVehicle thirdPartyVehicle )
{
	List<ThirdPartyVehicleOption> thirdPartyVehicleOptions = new ArrayList<ThirdPartyVehicleOption>();
	Date today = new Date();
	for(ThirdPartyVehicleOption thirdPartyVehicleOption : options) {
		thirdPartyVehicleOption.setDateCreated( today );
		thirdPartyVehicleOption.setThirdPartyVehicle( thirdPartyVehicle );
		thirdPartyVehicleOptions.add( thirdPartyVehicleOption );
	}
	thirdPartyVehicle.setThirdPartyVehicleOptions( thirdPartyVehicleOptions );
}

public void updateThirdPartyVehicleOptions( List<ThirdPartyVehicleOption> options, ThirdPartyVehicle thirdPartyVehicle, KBBConditionEnum condition )
{
	Date today = new Date();
	Map<String, ThirdPartyVehicleOption> guideBookOptionsMap = constructMapOfThirdPartyVehicleOptions( options );
	Map<String, ThirdPartyVehicleOption> persistedOptionsMap = constructMapOfThirdPartyVehicleOptions( thirdPartyVehicle.getThirdPartyVehicleOptions() );
	for( ThirdPartyVehicleOption option : options )
	{
		ThirdPartyVehicleOption persistedOption = (ThirdPartyVehicleOption)persistedOptionsMap.get( option.getOptionKey() );
		if ( persistedOption != null )
		{
			updateThirdPartyVehicleOption( thirdPartyVehicle, option, persistedOption, today, condition );
		}
		else
		{
			createNewThirdPartyVehicleOption( thirdPartyVehicle, option, today );
		}
	}
	
	// remove options that no longer exist for the vehicle
	Iterator<String> thirdPartyVehicleOptionsIter = persistedOptionsMap.keySet().iterator();
	while ( thirdPartyVehicleOptionsIter.hasNext() ) {
		String key = thirdPartyVehicleOptionsIter.next();
		ThirdPartyVehicleOption persistedOption = (ThirdPartyVehicleOption)persistedOptionsMap.get( key );
		ThirdPartyVehicleOption option = (ThirdPartyVehicleOption)guideBookOptionsMap.get( persistedOption.getOptionKey() );
		if ( option == null )
		{
			thirdPartyVehicle.getThirdPartyVehicleOptions().remove( persistedOption );
		}
	}
	int count = 0;
	for ( ThirdPartyVehicleOption option : thirdPartyVehicle.getThirdPartyVehicleOptions() )
	{
		option.setSortOrder(count);
		count++;
	}
}

private void createNewThirdPartyVehicleOption( ThirdPartyVehicle thirdPartyVehicle, ThirdPartyVehicleOption option, Date today )
{
	option.setDateCreated( today );
	option.setThirdPartyVehicle( thirdPartyVehicle );
	thirdPartyVehicle.addThirdPartyVehicleOption( option );
}

/**
 * TODO: processing the options will need to be book specific at some point in the future.  Nov. 15, 2005
 * @param thirdPartyVehicle
 * @param option
 * @param persistedOption
 * @param today
 * @param condition 
 */
private void updateThirdPartyVehicleOption( ThirdPartyVehicle thirdPartyVehicle, ThirdPartyVehicleOption option,
											ThirdPartyVehicleOption persistedOption, Date today, KBBConditionEnum condition )
{
	if ( thirdPartyVehicle.getThirdPartyId().intValue() == GuideBook.GUIDE_TYPE_KELLEYBLUEBOOK ) {
		ThirdPartyVehicleOptionValue thirdPartyVehicleOptionValueRetail = persistedOption.determineValueByType( ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE );
		if(thirdPartyVehicleOptionValueRetail == null) {
			persistedOption.addOptionValue(new ThirdPartyVehicleOptionValue(
					persistedOption, 
					ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE, 
					option.getRetailValue()));
		} else {
			thirdPartyVehicleOptionValueRetail.setValue(option.getRetailValue());
		}
		
		ThirdPartyVehicleOptionValue thirdPartyVehicleOptionValueWholesale = persistedOption.determineValueByType( ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE );
		if(thirdPartyVehicleOptionValueWholesale == null) {
			persistedOption.addOptionValue(new ThirdPartyVehicleOptionValue(
					persistedOption, 
					ThirdPartyVehicleOptionValueType.WHOLESALE_OPTION_VALUE_TYPE, 
					option.getWholesaleValue()));
		} else {
			thirdPartyVehicleOptionValueWholesale.setValue(option.getWholesaleValue());
		}
		
		ThirdPartyVehicleOptionValue thirdPartyVehicleOptionValueTradeIn = persistedOption.getTradeInRawValue();
		
		// this is for the initial adding of trade in type - when values are introduced - current vehicles will have not complete set of values!
		if ( thirdPartyVehicleOptionValueTradeIn == null ) {
			ThirdPartyVehicleOptionValueType optValueType = null;
			switch(condition) {
				case EXCELLENT:  
					optValueType = ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT;
					break;
				case GOOD:
					optValueType = ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD;
					break;
				default:
					optValueType = ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR;
					break;
			}
			
			thirdPartyVehicleOptionValueTradeIn = new ThirdPartyVehicleOptionValue(
					option,
					optValueType,
					null);

			persistedOption.addOptionValue( thirdPartyVehicleOptionValueTradeIn );
		} 
		else {
			if (condition != null) {
				ThirdPartyVehicleOptionValue tradeInValue = option.getTradeInValueByConditionId( condition.getId() );
				thirdPartyVehicleOptionValueTradeIn.setValue( tradeInValue == null ? 0 : tradeInValue.getValue() );
			}
		}
		persistedOption.setStandardOption( option.isStandardOption() );
	}
	else
	{
		ThirdPartyVehicleOptionValue thirdPartyVehicleOptionValue = persistedOption.determineValueByType( ThirdPartyVehicleOptionValueType.NA_OPTION_VALUE_TYPE );
		if(thirdPartyVehicleOptionValue != null) {
			thirdPartyVehicleOptionValue.setValue( Integer.valueOf( option.getValue() ) );
		}
		
	}
	
	persistedOption.setStatus( option.isStatus() );
}

private Map<String, ThirdPartyVehicleOption> constructMapOfThirdPartyVehicleOptions( Collection<ThirdPartyVehicleOption> vehicleOptions ) {
	Map<String, ThirdPartyVehicleOption> map = new HashMap<String, ThirdPartyVehicleOption>();
	for(ThirdPartyVehicleOption option : vehicleOptions) {
		map.put(option.getOptionKey(), option);
	}
	return map;
}

}
