package com.firstlook.service.bookout.bean;

import java.util.Date;
import java.util.List;

import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

import com.firstlook.display.tradeanalyzer.DisplayGuideBook;

public class BookOutStateErrorsOnly extends AbstractBookOutState
{

public DisplayGuideBook transformBookOutToDisplayData( DisplayGuideBook displayGuideBook )
{
	displayGuideBook.setValidVin( false );
	displayGuideBook.setSuccess( true );
	displayGuideBook.setGuideBookName( getMetaInfo().getGuideBookTitle() );
	if ( getMetaInfo().getPublishInfo() != null )
	{
		displayGuideBook.setPublishInfo( getMetaInfo().getPublishInfo() );
	}
	else
	{
		displayGuideBook.setPublishInfo( "" );
	}
	displayGuideBook.setFooter( getMetaInfo().getFooter() );
	displayGuideBook.setImageName( getMetaInfo().getImageName() );
	displayGuideBook.setGuideBookId( new Integer( getMetaInfo().getGuideBookId() ) );
	displayGuideBook.setLastBookedOutDate( new Date() );
	
	return displayGuideBook;
}

public String getSelectedVehicleKey()
{
	// No selected vehicle in this case
	return null;
}

public List<ThirdPartyVehicleOption> getThirdPartyVehicleOptions()
{
	// TODO Auto-generated method stub
	return null;
}

public List<VDP_GuideBookVehicle> getDropDownVehicles()
{
	// TODO Auto-generated method stub
	return null;
}

public List<VDP_GuideBookVehicle> getDropDownModels(){
	// TODO Auto-generated method stub
	return null;
}

}
