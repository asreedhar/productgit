package com.firstlook.service.bookout.bean;

import java.util.Date;
import java.util.List;

import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.service.appraisal.StagingBookOut;

import com.firstlook.display.tradeanalyzer.DisplayGuideBook;
import com.firstlook.service.bookout.BookOutService;
import com.firstlook.thirdparty.book.old.VDP_SelectedKeys;

public class SelectedVehicleAndOptionsBookOutState extends AbstractBookOutState
{

private String make;
private String vehicleYear;
private List<ThirdPartyVehicleOption> thirdPartyVehicleOptions;
private String selectedVehicleKey;
private List<VDP_GuideBookVehicle> dropDownVehicles;
private StagingBookOut pendingBookOut;

public SelectedVehicleAndOptionsBookOutState()
{
	super();
}

public DisplayGuideBook transformBookOutToDisplayData( DisplayGuideBook displayGuideBook )
{
	displayGuideBook.setYear( getVehicleYear() );
	
    List<ThirdPartyVehicleOption> allOptions = getThirdPartyVehicleOptions();
    VDP_SelectedKeys selectedKeys = BookOutService.retrieveSelectedKeysAndSetSelectedFalse( allOptions, pendingBookOut ); 
	displayGuideBook.setDisplayGuideBookOptions( allOptions );
    displayGuideBook.setSelectedDrivetrainKey( selectedKeys.getSelectedDrivetrainKey() );
    displayGuideBook.setSelectedEngineKey( selectedKeys.getSelectedEngineKey() );
    displayGuideBook.setSelectedTransmissionKey( selectedKeys.getSelectedTransmissionKey() );
    displayGuideBook.setSelectedEquipmentOptionKeys( selectedKeys.getSelectedEquipmentOptionKeys() );
	displayGuideBook.setSelectedVehicleKey( getSelectedVehicleKey() );
	displayGuideBook.setLastBookedOutDate( new Date() );
	
	if (getMetaInfo() != null){
		displayGuideBook.setGuideBookName( getMetaInfo().getGuideBookTitle() );
		displayGuideBook.setPublishInfo( getMetaInfo().getPublishInfo() );
		displayGuideBook.setFooter( getMetaInfo().getFooter() );
		displayGuideBook.setImageName( getMetaInfo().getImageName() );
		displayGuideBook.setBgColor( getMetaInfo().getBgColor() );
		displayGuideBook.setGuideBookId( new Integer( getMetaInfo().getGuideBookId() ) );
	}

	displayGuideBook.setMake( getMake() );
	
	if (getDropDownVehicles() != null){
		displayGuideBook.setDisplayGuideBookVehicles( getDropDownVehicles() );
	}

	displayGuideBook.setValidVin( true );
	displayGuideBook.setSuccess( true );
	
	return displayGuideBook;
	
}

public List<VDP_GuideBookVehicle> getDropDownVehicles()
{
	return dropDownVehicles;
}

public void setDropDownVehicles( List<VDP_GuideBookVehicle> dropDownVehicles )
{
	this.dropDownVehicles = dropDownVehicles;
}

public List<ThirdPartyVehicleOption> getThirdPartyVehicleOptions()
{
	return thirdPartyVehicleOptions;
}

public void setThirdPartyVehicleOptions( List<ThirdPartyVehicleOption> guideBookOptions )
{
	this.thirdPartyVehicleOptions = guideBookOptions;
}

public String getMake()
{
	return make;
}

public void setMake( String make )
{
	this.make = make;
}

public String getSelectedVehicleKey()
{
	return selectedVehicleKey;
}

public void setSelectedVehicleKey( String selectedVehicleKey )
{
	this.selectedVehicleKey = selectedVehicleKey;
}

public String getVehicleYear()
{
	return vehicleYear;
}

public void setVehicleYear( String vehicleYear )
{
	this.vehicleYear = vehicleYear;
}

public StagingBookOut getPendingBookOut() {
	return pendingBookOut;
}

public void setPendingBookOut(StagingBookOut pendingBookOut) {
	this.pendingBookOut = pendingBookOut;
}

public List<VDP_GuideBookVehicle> getDropDownModels(){
	// TODO Auto-generated method stub
	return null;
}
}
