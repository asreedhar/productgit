package com.firstlook.service.bookout.inventory;

import java.io.StringWriter;
import java.text.MessageFormat;
import java.util.Collection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import biz.firstlook.transact.persist.exception.SelectedThirdPartyVehicleNotFoundException;
import biz.firstlook.transact.persist.model.BookOut;
import biz.firstlook.transact.persist.model.BookOutThirdPartyCategory;
import biz.firstlook.transact.persist.model.BookOutValue;
import biz.firstlook.transact.persist.model.InventoryWithBookOut;
import biz.firstlook.transact.persist.model.ThirdPartyVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOptionValue;

class BookOutXmlHelper {
	
	private static final transient Logger log = Logger.getLogger( InventoryBookOutService.class );
	
	public static String toInsertXml(InventoryWithBookOut inventoryWithBookOut, int guideBookId) {
		long start = System.currentTimeMillis();
		try {
			DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document document = builder.newDocument();
			Element root = document.createElement("boTransaction");
			
			Element inventory = document.createElement("inventory");
			inventory.setAttribute("id", toNullOrString(inventoryWithBookOut.getInventoryId()));
			root.appendChild(inventory);
			
			BookOut latestBookOut = inventoryWithBookOut.getLatestBookOut(guideBookId);		
			BookOutXmlHelper.addBookOut(document, root, latestBookOut);
			
			try {
				ThirdPartyVehicle tpv = latestBookOut.getSelectedThirdPartyVehicle();
				BookOutXmlHelper.addThirdPartyVehicle(document, root, tpv);
			} catch (SelectedThirdPartyVehicleNotFoundException e) {
				log.info(MessageFormat.format("The bookout for inventoryid {0} does not have a select ThirdPartyVehicle", inventoryWithBookOut.getInventoryId().toString()));
			}
			
			StringWriter w = new StringWriter();
			TransformerFactory.newInstance().newTransformer().transform(
					new DOMSource(root),
					new StreamResult(w));
			if( log.isDebugEnabled() ) {
				log.debug( "InventoryBookOutService.toInsertXml: " + (System.currentTimeMillis() - start) + " ms" );
			}
			return w.toString();
		} catch (ParserConfigurationException e) {
			throw new RuntimeException(e);
		} catch (TransformerConfigurationException e) {
			throw new RuntimeException(e);
		} catch (TransformerException e) {
			throw new RuntimeException(e);
		} catch (TransformerFactoryConfigurationError e) {
			throw new RuntimeException(e);
		} 
	}

	public static void addBookOut(final Document document, final Node parent, final BookOut bookOut) {
		
		Element bookOutElment = document.createElement("bookOut");
		
		//Required attributes
		bookOutElment.setAttribute("bookOutSourceId", toNullOrString(bookOut.getBookOutSourceId()));
		bookOutElment.setAttribute("bookOutStatusId", toNullOrString(bookOut.getBookoutStatusId()));
		bookOutElment.setAttribute("isAccurate", toNullOrString(bookOut.isAccurate()));
		bookOutElment.setAttribute("thirdPartyId", toNullOrString(bookOut.getThirdPartyId()));
		bookOutElment.setAttribute("AdvertisingStatus", toNullOrString(bookOut.isAdvertisingStatus()));
		
		//Nullable fields
		bookOutElment.setAttribute("region", bookOut.getRegion());
		bookOutElment.setAttribute("datePublished", bookOut.getDatePublished());
		bookOutElment.setAttribute("bookPrice", toNullOrString(bookOut.getBookPrice()));
		bookOutElment.setAttribute("weight", toNullOrString(bookOut.getWeight()));
		bookOutElment.setAttribute("mileageCostAdjustment", toNullOrString(bookOut.getMileageCostAdjustment()));
		bookOutElment.setAttribute("msrp", toNullOrString(bookOut.getMsrp()));
		
		addBookOutThirdPartyCategories(document, bookOutElment, bookOut.getBookOutThirdPartyCategories());
		
		parent.appendChild(bookOutElment);
	}
	
	private static void addBookOutThirdPartyCategories(final Document document, final  Node parent, final Collection<BookOutThirdPartyCategory> bookOutThirdPartyCategories) {
		for(BookOutThirdPartyCategory btpc : bookOutThirdPartyCategories) {
			Integer thirdPartyCategoryId = btpc.getThirdPartyCategory().getThirdPartyCategoryId();
			Element btpcElement = document.createElement("bookOutThirdPartyCategory");
			btpcElement.setAttribute("thirdPartyCategoryId", toNullOrString(thirdPartyCategoryId));
					
			addBookOutValues(document, btpcElement, btpc.getBookOutValues());
			
			parent.appendChild(btpcElement);
		}
	}
	
	private static void addBookOutValues(final Document document, final Node parent, final Collection<BookOutValue> bookOutValues) {
		for(BookOutValue boValue : bookOutValues) {
			Element boValueElement = document.createElement("bookOutValue");
			boValueElement.setAttribute("bookOutValueTypeId", toNullOrString(boValue.getBookOutValueType().getBookOutValueTypeId()));
			boValueElement.setAttribute("thirdPartySubCategoryId", toNullOrString(boValue.getThirdPartySubCategoryId()));
			boValueElement.setAttribute("value", toNullOrString(boValue.getValue()));
			
			parent.appendChild(boValueElement);
		}
	}
	
	public static void addThirdPartyVehicle(final Document document, final Node parent, final ThirdPartyVehicle thirdPartyVehicle) {
		Element tpvElement = document.createElement("thirdPartyVehicle");
		
		tpvElement.setAttribute("thirdPartyId", toNullOrString(thirdPartyVehicle.getThirdPartyId()));
		tpvElement.setAttribute("thirdPartyVehicleCode", thirdPartyVehicle.getThirdPartyVehicleCode());
		tpvElement.setAttribute("description", thirdPartyVehicle.getDescription());
		tpvElement.setAttribute("makeCode", toNullOrString(thirdPartyVehicle.getMakeCode()));
		tpvElement.setAttribute("modelCode", toNullOrString(thirdPartyVehicle.getModelCode()));
		tpvElement.setAttribute("status", toNullOrString(thirdPartyVehicle.isStatus()));
		tpvElement.setAttribute("engineType", thirdPartyVehicle.getEngineType());
		tpvElement.setAttribute("make", thirdPartyVehicle.getMake());
		tpvElement.setAttribute("model", thirdPartyVehicle.getModel());
		tpvElement.setAttribute("body", thirdPartyVehicle.getBody());
		
		addThirdPartyVehicleOptions(document, tpvElement, thirdPartyVehicle.getThirdPartyVehicleOptions());
		
		parent.appendChild(tpvElement);
	}
	
	private static void addThirdPartyVehicleOptions(final Document document, final Node parent, final Collection<ThirdPartyVehicleOption> thirdPartyVehicleOptions) {
		for(ThirdPartyVehicleOption tpvOption : thirdPartyVehicleOptions) {
			Element tpvOptionElement = document.createElement("thirdPartyVehicleOption");
			tpvOptionElement.setAttribute("thirdPartyOptionId", toNullOrString(tpvOption.getThirdPartyOptionId()));
			tpvOptionElement.setAttribute("isStandardOption", Boolean.toString(tpvOption.isStandardOption()));
			tpvOptionElement.setAttribute("status", Boolean.toString(tpvOption.isStatus()));
			tpvOptionElement.setAttribute("sortOrder", toNullOrString(tpvOption.getSortOrder()));
			
			addThirdPartyVehicleOptionValues(document, tpvOptionElement, tpvOption.getOptionValues());
			
			parent.appendChild(tpvOptionElement);
		}
	}
	
	private static void addThirdPartyVehicleOptionValues(final Document document, final Node parent, final Collection<ThirdPartyVehicleOptionValue> thirdPartyVehicleOptionValues) {
		for(ThirdPartyVehicleOptionValue tpvOptionValue : thirdPartyVehicleOptionValues) {
			Element tpvOptionElement = document.createElement("thirdPartyVehicleOptionValue");
			tpvOptionElement.setAttribute("thirdPartyOptionValueTypeId", toNullOrString(tpvOptionValue.getThirdPartyVehicleOptionValueType().getThirdPartyOptionValueTypeId()));
			tpvOptionElement.setAttribute("value", toNullOrString(tpvOptionValue.getValue()));
			
			parent.appendChild(tpvOptionElement);
		}
	}
	
	static String toNullOrString(Object obj) {
		return (obj == null) ? null : obj.toString();
	}
}
