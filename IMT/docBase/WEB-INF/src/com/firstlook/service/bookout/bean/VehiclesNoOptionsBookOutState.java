package com.firstlook.service.bookout.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

import com.firstlook.display.tradeanalyzer.DisplayGuideBook;

public class VehiclesNoOptionsBookOutState extends AbstractBookOutState
{

public List<VDP_GuideBookVehicle> getDropDownModels() {
		return dropDownModels;
	}

	public void setDropDownModels(List<VDP_GuideBookVehicle> dropDownModels) {
		this.dropDownModels = dropDownModels;
	}

private String model;
private String make;
private String vehicleYear;
private List<VDP_GuideBookVehicle> dropDownVehicles;
private List<VDP_GuideBookVehicle> dropDownModels;



public DisplayGuideBook transformBookOutToDisplayData(DisplayGuideBook displayGuideBook)
{
	displayGuideBook.setGuideBookName( getMetaInfo().getGuideBookTitle() );
	displayGuideBook.setPublishInfo( getMetaInfo().getPublishInfo() );
	displayGuideBook.setFooter( getMetaInfo().getFooter() );
	displayGuideBook.setImageName( getMetaInfo().getImageName() );
	displayGuideBook.setBgColor( getMetaInfo().getBgColor() );
	displayGuideBook.setMake( getMake() );
	displayGuideBook.setYear( getVehicleYear() );
	displayGuideBook.setDisplayGuideBookVehicles( getDropDownVehicles()  );
	displayGuideBook.setDisplayGuideBookModels(getDropDownModels());
	displayGuideBook.setGuideBookId( new Integer( getMetaInfo().getGuideBookId() ) );
	displayGuideBook.setLastBookedOutDate( new Date() );
	displayGuideBook.setValidVin( true );
	displayGuideBook.setSuccess( true );
	return displayGuideBook;
}

public List<VDP_GuideBookVehicle> getDropDownVehicles()
{
	return dropDownVehicles;
}

public void setDropDownVehicles( List<VDP_GuideBookVehicle> dropDownVehicles )
{
	this.dropDownVehicles = dropDownVehicles;
}

public String getMake()
{
	return make;
}

public void setMake( String make )
{
	this.make = make;
}

public String getVehicleYear()
{
	return vehicleYear;
}

public void setVehicleYear( String vehicleYear )
{
	this.vehicleYear = vehicleYear;
}

public String getModel()
{
	return model;
}

public void setModel( String model )
{
	this.model = model;
}

public String getSelectedVehicleKey()
{
	//There won't be a selected vehicle in this case
	return null;
}

public List<ThirdPartyVehicleOption> getThirdPartyVehicleOptions()
{
	return new ArrayList<ThirdPartyVehicleOption>();
}

}
