package com.firstlook.service.bookout.appraisal;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import biz.firstlook.module.vehicle.description.old.BookOutDatasetInfo;
import biz.firstlook.module.vehicle.description.old.BookOutTypeEnum;
import biz.firstlook.module.vehicle.description.old.GBException;
import biz.firstlook.module.vehicle.description.old.GuideBookInput;
import biz.firstlook.module.vehicle.description.old.GuideBookMetaInfo;
import biz.firstlook.module.vehicle.description.old.KBBConditionEnum;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.enumerator.BookoutStatusEnum;
import biz.firstlook.transact.persist.exception.SelectedThirdPartyVehicleNotFoundException;
import biz.firstlook.transact.persist.model.BookOut;
import biz.firstlook.transact.persist.model.BookOutSource;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.service.AuditBookOutsService;
import biz.firstlook.transact.persist.service.ThirdPartyVehicleOptionService;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;
import biz.firstlook.transact.persist.service.appraisal.StagingBookOut;

import com.firstlook.service.bookout.BookOutAssembler;
import com.firstlook.service.bookout.BookOutService;
import com.firstlook.service.bookout.bean.AbstractBookOutState;
import com.firstlook.service.bookout.bean.BookValuesBookOutState;
import com.firstlook.service.bookout.bean.CompleteStoredBookOutState;
import com.firstlook.service.bookout.bean.StoredBookOutState;

public class AppraisalBookOutService
{

private IAppraisalService appraisalService;
private BookOutService bookOutService;
private BookOutAssembler bookOutAssembler;
private AuditBookOutsService auditBookOutsService;

public AbstractBookOutState retrieveExistingBookOutInfoOrBeginNewBookOut( GuideBookInput input, Integer thirdPartyId, Integer searchAppraisalDaysBackThreshold, StagingBookOut stagingBookout) throws GBException
{
	String modelId = input.getSelectedModelId(); 
	StoredBookOutState storedState = constructStoredInfoFromAppraisalBookOut( input, thirdPartyId, searchAppraisalDaysBackThreshold );
	if(modelId!=null && !modelId.equals("") && modelId !=input.getSelectedModelId() && thirdPartyId==3)
	{
		storedState= null;
		input.setSelectedModelId(modelId);
	}
	if ( storedState == null )
	{
		AbstractBookOutState abstractState;
		BookOutDatasetInfo bookOutDatasetInfo =  new BookOutDatasetInfo( BookOutTypeEnum.APPRAISAL, input.getBusinessUnitId());
		abstractState = getBookOutService().doVinLookup( input, thirdPartyId,bookOutDatasetInfo , stagingBookout );

		return abstractState;
	}
	else
	{
		VDP_GuideBookVehicle guideBookVehicle = BookOutService.determineSelectedVehicleByKey( storedState.getDropDownVehicles(),
																								storedState.getSelectedVehicleKey() );
		List<ThirdPartyVehicleOption> optionsFromBook = new ArrayList<ThirdPartyVehicleOption>();
		try
		{
			optionsFromBook = getBookOutService().retrieveOptionsForVehicle( input, guideBookVehicle, thirdPartyId, new BookOutDatasetInfo( BookOutTypeEnum.APPRAISAL, input.getBusinessUnitId()) );
		}
		catch ( GBException e )
		{
			optionsFromBook = storedState.getThirdPartyVehicleOptions();
		}

		storedState.setThirdPartyVehicleOptions( ThirdPartyVehicleOptionService.refreshNewOptions( optionsFromBook,
																									storedState.getThirdPartyVehicleOptions() ) );
		// new logic for getting the models drop down too on coming back from the 3rd page/inventory
		if(thirdPartyId == 3){
			AbstractBookOutState abstractState;
			BookOutDatasetInfo bookOutDatasetInfo =  new BookOutDatasetInfo( BookOutTypeEnum.APPRAISAL, input.getBusinessUnitId());
			abstractState = getBookOutService().doVinLookup( input, thirdPartyId,bookOutDatasetInfo , stagingBookout );
			storedState.setSelectedModelKey(input.getSelectedModelId());
			storedState.setDropDownModels(abstractState.getDropDownModels());
		}
		
		
	}
	return storedState;
}

public BookValuesBookOutState updateBookValues( GuideBookInput input,
												VDP_GuideBookVehicle vehicle, List<ThirdPartyVehicleOption> incomingOptionList, List<VDP_GuideBookVehicle> dropDownVehicles,
												Integer bookOutType, IAppraisal appraisal, Integer thirdPartyId ) throws GBException
{
	
	BookValuesBookOutState bookValuesState = getBookOutService().updateBookValues( input, thirdPartyId, vehicle, incomingOptionList, new BookOutDatasetInfo(BookOutTypeEnum.APPRAISAL, input.getBusinessUnitId()) );

	if(dropDownVehicles!=null&&vehicle!=null&& vehicle.getVin()!=null&&dropDownVehicles.get(0).getCondition()==null && dropDownVehicles.get(0).getVin().equalsIgnoreCase(vehicle.getVin())){
		dropDownVehicles.get(0).setCondition(vehicle.getCondition());
		
	}

	
	if ( bookValuesState.getBookOutErrors().isEmpty() )
	{
		saveUpdatedBookInfo( input, bookValuesState.getBookValues(), bookValuesState.getOptionsFromBook(), dropDownVehicles, vehicle.getKey(),
								thirdPartyId.intValue(), appraisal, bookValuesState.getMileageAdjustment(), bookValuesState.getMsrp(),
								vehicle.getWeight(), bookValuesState.getMetaInfo(), bookOutType, vehicle.getVic() );
	}

	return bookValuesState;
}

/**
 * TODO: These save methods should probably not be persisting Appraisal
 * information, but rather the book out information only. Refactor during next
 * iteration - MH, NK, 11/09/2005
 * @param msrp TODO
 * @param vic TODO
 */
public void saveUpdatedBookInfo( GuideBookInput input, List<VDP_GuideBookValue> bookValues, List<ThirdPartyVehicleOption> options, List<VDP_GuideBookVehicle> dropDownVehicles,
								String selectedVehicleKey, int guideBookId, IAppraisal appraisal, Integer mileageAdjustment,
								Integer msrp, Integer weight, GuideBookMetaInfo metaInfo, Integer bookOutType, String vic) throws GBException
{
	Date today = new Date();

	// Trade Analyzer book outs are always accurate
	BookOut bookOut = BookOutService.constructNewBookOut( bookValues, guideBookId, today, input.getMileage(), mileageAdjustment, msrp, weight, metaInfo, BookOutSource.BOOK_OUT_SOURCE_APPRAISAL);
	bookOut.setBookoutStatusId(BookoutStatusEnum.CLEAN.getId());

	Set<ThirdPartyVehicle> thirdPartyVehicles = new HashSet<ThirdPartyVehicle>();
	BookOut previousBookout = appraisal.getLatestBookOut(guideBookId );
	if( previousBookout != null ) {
		thirdPartyVehicles.addAll( previousBookout.getThirdPartyVehicles());
		if(msrp == null) {
			bookOut.setMsrp(previousBookout.getMsrp());
		}
		
		if(weight == null) {
			bookOut.setWeight(previousBookout.getWeight());
		}
	}

	if ( thirdPartyVehicles != null && !thirdPartyVehicles.isEmpty() )
	{
		Set<ThirdPartyVehicle> tpvs = getBookOutService().updateExistingThirdPartyVehicles( options, selectedVehicleKey, today, thirdPartyVehicles,
																guideBookId, dropDownVehicles );
		bookOut.setThirdPartyVehicles( tpvs );
	}
	else
	{
		Set<ThirdPartyVehicle> tpvs = getBookOutService().createNewThirdPartyVehicles( options, dropDownVehicles, selectedVehicleKey, guideBookId, today );
		bookOut.setThirdPartyVehicles( tpvs );
	}
	appraisal.addBookOut( bookOut );

	// persist the VIC if it is known
	if ( StringUtils.isNotBlank( vic ) ) {
		appraisal.getVehicle().setVic( vic );
	}
	
	// nk - need to save appraisal with mileage just used to bookout vehicle
	appraisal.setMileage(input.getMileage());
	appraisal.setDateModified(new Date());
	
	saveUpdatedAppraisalWithBookOut( input, appraisal, guideBookId, bookOutType, bookOut );
}

private void saveUpdatedAppraisalWithBookOut( GuideBookInput input, IAppraisal appraisal, int guideBookId, Integer bookOutType,
												BookOut bookOut )
{
	appraisalService.updateAppraisal( appraisal );

	getAuditBookOutsService().logBookOuts( new Integer( input.getBusinessUnitId() ), input.getMemberId(), bookOutType,
														input.getVin(), new Integer( guideBookId ), bookOut.getBookOutId() );
}

public StoredBookOutState constructStoredInfoFromAppraisalBookOut( GuideBookInput input, Integer thirdPartyId, Integer searchAppraisalDaysBackThreshold ) throws GBException
{
	//TODO: performance problem here.  There is a loop over the display guide books and this causes N+1 select and N+1 updates (appraisals should be detached instances).
	IAppraisal appraisal;
	if (searchAppraisalDaysBackThreshold == null) {
		appraisal = appraisalService.findBy(input.getBusinessUnitId(), input.getVin()).get(0);
	} else {
		appraisal = appraisalService.findLatest( input.getBusinessUnitId(), input.getVin(), searchAppraisalDaysBackThreshold );
	}
	if ( appraisal != null && !appraisal.getAppraisalSource().isCRMOrMobile() )
	{
		Vehicle vehicle = appraisal.getVehicle();
		BookOut bookOut = appraisal.getLatestBookOut( thirdPartyId );
		if ( bookOut != null )
		{
			ThirdPartyVehicle selectedVehicle = null;
			try {
				selectedVehicle = bookOut.getSelectedThirdPartyVehicle();
			} catch (SelectedThirdPartyVehicleNotFoundException e1) {
				return null;
			}
			String modelCode = selectedVehicle.getModelCode();  // fetching the model id in case of the exceptional scenario of kbb where vin doesn't decodes to trim
			if(modelCode == null){
				modelCode = "";
				}
			String make = selectedVehicle.getMake();
			if(make == null){
				make = "";
				}
			input.setMake(make);
			input.setSelectedModelId(modelCode);
			List<ThirdPartyVehicle> thirdPartyVehiclesFromBook = new ArrayList<ThirdPartyVehicle>();
			try
			{
				thirdPartyVehiclesFromBook = getBookOutService().constructDropDownVehicles( input, thirdPartyId, selectedVehicle, new BookOutDatasetInfo( BookOutTypeEnum.APPRAISAL, input.getBusinessUnitId()) );
			}
			catch ( GBException e )
			{
				//if we don't return null, we get a blank bookout tile with no book values!
				//FB 6115
				//thirdPartyVehiclesFromBook.add( selectedVehicle );
				return null;
			}
			ThirdPartyVehicle selectedVehicleFromBook = null;
			try {
				selectedVehicleFromBook = BookOut.getSelectedThirdPartyVehicle( thirdPartyVehiclesFromBook );
				StoredBookOutState state = getBookOutService().constructCompleteStoredBookOutInfo( bookOut, vehicle,
						thirdPartyVehiclesFromBook,
						selectedVehicleFromBook, thirdPartyId,
						Integer.valueOf( appraisal.getMileage() ),
						KBBConditionEnum.GOOD );
				return state;
			} catch (SelectedThirdPartyVehicleNotFoundException e) {
				return null;
			}
		}
	}
	return null;
}

public StoredBookOutState constructAppraisalBookOutForProcessor( GuideBookInput input, Integer thirdPartyId, int searchInactiveInventoryDaysBackThreshold ) throws GBException
{
	StoredBookOutState state = null;
	IAppraisal appraisal = appraisalService.findLatest( input.getBusinessUnitId(), input.getVin(), searchInactiveInventoryDaysBackThreshold );
	if ( appraisal != null )
	{
		BookOut bookout = appraisal.getLatestBookOut( thirdPartyId );
		if ( bookout != null )
		{
			ThirdPartyVehicle selectedVehicleFromAppraisal;
			try {
				selectedVehicleFromAppraisal = bookout.getSelectedThirdPartyVehicle();
				List<ThirdPartyVehicle> thirdPartyVehiclesFromBook = (List<ThirdPartyVehicle>)getBookOutService().constructDropDownVehicles( input, thirdPartyId, selectedVehicleFromAppraisal, 
                         new BookOutDatasetInfo(BookOutTypeEnum.APPRAISAL, input.getBusinessUnitId()) );

				ThirdPartyVehicle selectedVehicleFromBook = BookOut.getSelectedThirdPartyVehicle( thirdPartyVehiclesFromBook );
				state = new CompleteStoredBookOutState();
				
				// only look up condtion if this is a KBB processor run
				KBBConditionEnum condition = null;
				if ( thirdPartyId.intValue() == ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE )
				{
				condition = KBBConditionEnum.GOOD;
				}
				
				state.setDropDownVehicles( getBookOutAssembler().transformThirdPartyVehiclesToGuideBookVehicles( bookout, thirdPartyVehiclesFromBook,
														 appraisal.getVehicle().getVehicleYear(), 
														 condition ) );
				state.setSelectedVehicleKey( selectedVehicleFromBook.getThirdPartyVehicleCode() );
				state.setThirdPartyVehicleOptions( selectedVehicleFromBook.getThirdPartyVehicleOptions() );
				state.setBookOutSourceId( BookOutSource.BOOK_OUT_SOURCE_APPRAISAL );
				
			} catch (SelectedThirdPartyVehicleNotFoundException e) {
				return state;
			}
		}
	}
	return state;
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

public BookOutService getBookOutService()
{
	return bookOutService;
}

public void setBookOutService( BookOutService bookOutService )
{
	this.bookOutService = bookOutService;
}

public BookOutAssembler getBookOutAssembler()
{
	return bookOutAssembler;
}

public void setBookOutAssembler( BookOutAssembler bookOutAssembler )
{
	this.bookOutAssembler = bookOutAssembler;
}

public AuditBookOutsService getAuditBookOutsService()
{
	return auditBookOutsService;
}

public void setAuditBookOutsService( AuditBookOutsService auditBookOutsService )
{
	this.auditBookOutsService = auditBookOutsService;
}

}
