package com.firstlook.service.bookout.bean;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.module.vehicle.description.old.VDP_NadaOptionAdjustmentsAdapter;
import biz.firstlook.transact.persist.model.BookOutValueType;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

import com.firstlook.display.tradeanalyzer.DisplayGuideBook;
import com.firstlook.service.bookout.BookOutService;
import com.firstlook.thirdparty.book.old.VDP_SelectedKeys;

public class CompleteStoredBookOutState extends StoredBookOutState implements ISingleVehicleState
{

public DisplayGuideBook transformBookOutToDisplayData( DisplayGuideBook displayGuideBook )
{
	displayGuideBook.setGuideBookName( getMetaInfo().getGuideBookTitle() );
	if ( getMetaInfo().getPublishInfo() != null )
	{
		displayGuideBook.setPublishInfo( getMetaInfo().getPublishInfo() );
	}
	else
	{
		displayGuideBook.setPublishInfo( "" );
	}
	displayGuideBook.setFooter( getMetaInfo().getFooter() );
	displayGuideBook.setImageName( getMetaInfo().getImageName() );
	displayGuideBook.setBgColor( getMetaInfo().getBgColor() );
	displayGuideBook.setRegion( super.getRegionDescription() );
	displayGuideBook.setMsrp( getMsrp() );
	displayGuideBook.setWeight( getWeight() );
	displayGuideBook.setGuideBookId(getMetaInfo().getGuideBookId());
	displayGuideBook.setMake( getMake() );
	displayGuideBook.setModel( getModel() );
	displayGuideBook.setYear( getVehicleYear() );
	displayGuideBook.setSelectedVehicleKey( getSelectedVehicleKey() );
	displayGuideBook.setSelectedModelKey(getSelectedModelKey());
	displayGuideBook.setTradeAnalyzedDate( getBookOutDate().toString() );
	
    List<ThirdPartyVehicleOption> allOptions = getThirdPartyVehicleOptions();
	VDP_SelectedKeys selectedKeys = BookOutService.retrieveSelectedKeysAndSetSelectedFalse( allOptions );
	displayGuideBook.setDisplayGuideBookOptions( allOptions );
	displayGuideBook.setSelectedDrivetrainKey( selectedKeys.getSelectedDrivetrainKey() );
	displayGuideBook.setSelectedEngineKey( selectedKeys.getSelectedEngineKey() );
	displayGuideBook.setSelectedTransmissionKey( selectedKeys.getSelectedTransmissionKey() );
	displayGuideBook.setSelectedEquipmentOptionKeys( selectedKeys.getSelectedEquipmentOptionKeys() );

	Collection<ThirdPartyVehicleOption> selectedEquipmentOptions = BookOutService.determineSelectedOrStandardOptionsForType( allOptions, ThirdPartyOptionType.EQUIPMENT,
	                                                                                                getMetaInfo().getGuideBookId());
	displayGuideBook.setSelectedOptions( selectedEquipmentOptions );
	
	final int guideBookId = displayGuideBook.getGuideBookId().intValue();
	ThirdPartyDataProvider provider = ThirdPartyDataProvider.getThirdPartyDataProvider(guideBookId);
	if( ThirdPartyDataProvider.KELLEY.equals(provider) )
	{
		determineKelleyBaseValues( displayGuideBook, getBookValues() );
		determineKelleyNoMileageValues( displayGuideBook, getBookValues() );
		determineKelleyFinalValues( displayGuideBook, getBookValues() );
		displayGuideBook.setKelleyPublishState( determineKelleyPublishState( displayGuideBook.getPublishInfo() ) );
		displayGuideBook.setKelleyOptionsAdjustment( getOptionsValue( getBookValues() ) );
		displayGuideBook.setCondition( getConditionFromValue( displayGuideBook.getKelleyTradeInValue() ) );
	} else if (ThirdPartyDataProvider.NADA.equals(provider)) {
		displayGuideBook.setGuideBookOptionAdjustment(getNadaOptionsValues());
	}
	
	// TODO: remove this array after 3.0 is fully deployed
	List<VDP_GuideBookValue> displayGuideBookValues = retrieveThirdPartyValuesByValueType( getBookValues(), BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE );
	
	int bookOutValueTypeId = guideBookId != ThirdPartyDataProvider.KELLEY.getThirdPartyId() ? BookOutValueType.BOOK_OUT_VALUE_BASE_VALUE : BookOutValueType.BOOK_OUT_VALUE_MILEAGE_INDEPENDENT;
	List<VDP_GuideBookValue> displayGuideBookValuesNoMileage = retrieveThirdPartyValuesByValueType( getBookValues(), bookOutValueTypeId );
	displayGuideBook.setDisplayGuideBookValues( sortThirdPartyValues( provider, displayGuideBookValues ) );
	displayGuideBook.setDisplayGuideBookValuesComplete( getBookValues() );
	if ( displayGuideBookValuesNoMileage != null )
	{
		displayGuideBook.setDisplayGuideBookValuesNoMileage( sortThirdPartyValues( provider, displayGuideBookValuesNoMileage ) );
	}

	displayGuideBook.setDisplayGuideBookVehicles( getDropDownVehicles() );
	displayGuideBook.setDisplayGuideBookModels(getDropDownModels());
	displayGuideBook.setGuideBookId( getMetaInfo().getGuideBookId());
	displayGuideBook.setMileageAdjustment( getMileageAdjustment() );
	displayGuideBook.setValidVin( true );
    displayGuideBook.setInAccurate( isInAccurate() );
	displayGuideBook.setLastBookedOutDate( getBookOutDate() );
	displayGuideBook.setSuccess( true );
	displayGuideBook.setStoredInfo( true );
	
	return displayGuideBook;
}

public VDP_GuideBookVehicle getSelectedVehicle() {
	Iterator<VDP_GuideBookVehicle> it = getDropDownVehicles().iterator();
	while ( it.hasNext() ) {
		VDP_GuideBookVehicle vehicle = it.next();
		if ( vehicle.getKey().equals( getSelectedVehicleKey() ) )
			return vehicle;
	}
	return null;
}

protected VDP_NadaOptionAdjustmentsAdapter getNadaOptionsValues() {
	VDP_NadaOptionAdjustmentsAdapter optionAdjustments = new VDP_NadaOptionAdjustmentsAdapter();
	List<VDP_GuideBookValue> bookValues = getBookValues();
	for( VDP_GuideBookValue value: bookValues )	{
		if ( value.getValueType() == BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE ) {
			optionAdjustments.add(value);
		}
	}
	return optionAdjustments;
}


}
