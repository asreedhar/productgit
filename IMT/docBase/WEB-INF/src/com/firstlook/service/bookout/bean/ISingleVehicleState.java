package com.firstlook.service.bookout.bean;


import java.util.List;

import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

public interface ISingleVehicleState
{

public List<ThirdPartyVehicleOption> getThirdPartyVehicleOptions();


public VDP_GuideBookVehicle getSelectedVehicle();


public List<VDP_GuideBookVehicle> getDropDownVehicles();

}