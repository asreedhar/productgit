package com.firstlook.service.bookout.bean;

import java.util.Date;
import java.util.List;

import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.model.BookOutValueType;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

import com.firstlook.display.tradeanalyzer.DisplayGuideBook;
import com.firstlook.service.bookout.BookOutService;

public class BookValuesBookOutState extends AbstractBookOutState
{

private Date lastBookedOutDate;
private List<VDP_GuideBookValue> bookValues;
private Integer mileageAdjustment;
private Integer msrp;
private Integer weight;
private List<ThirdPartyVehicleOption> optionsFromBook;

public DisplayGuideBook transformBookOutToDisplayData( DisplayGuideBook displayGuideBook )
{
	displayGuideBook.setGuideBookId( getMetaInfo().getGuideBookId() );
	List<VDP_GuideBookValue> displayGuideBookValues = retrieveThirdPartyValuesByValueType( getBookValues(), BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE );
	List<VDP_GuideBookValue> displayGuideBookValuesNoMileage = retrieveThirdPartyValuesByValueType( getBookValues(),
																				BookOutValueType.BOOK_OUT_VALUE_MILEAGE_INDEPENDENT );
	
	int guideBookId = displayGuideBook.getGuideBookId().intValue();
	ThirdPartyDataProvider provider = ThirdPartyDataProvider.getThirdPartyDataProvider(guideBookId);
	if( ThirdPartyDataProvider.KELLEY.equals(provider) ) {
		determineKelleyBaseValues( displayGuideBook, getBookValues() );
		determineKelleyNoMileageValues( displayGuideBook, getBookValues() );
		determineKelleyFinalValues( displayGuideBook, getBookValues() );
		displayGuideBook.setKelleyPublishState( determineKelleyPublishState( displayGuideBook.getPublishInfo() ) );
		displayGuideBook.setKelleyOptionsAdjustment( getOptionsValue( getBookValues() ) );
		displayGuideBook.setCondition( getConditionFromValue( displayGuideBook.getKelleyTradeInValue() ) );
	} else if (ThirdPartyDataProvider.NADA.equals(provider)) {
		displayGuideBook.setGuideBookOptionAdjustment(getOptionsValue(getBookValues()));
	}
	
	displayGuideBook.setDisplayGuideBookValues( sortThirdPartyValues( provider, displayGuideBookValues ) );

    List<ThirdPartyVehicleOption> allOptions = getThirdPartyVehicleOptions();
	List<ThirdPartyVehicleOption> selectedEquipmentOptions = (List<ThirdPartyVehicleOption>)BookOutService.determineSelectedOrStandardOptionsForType( allOptions, ThirdPartyOptionType.EQUIPMENT,
	                                                                                                	getMetaInfo().getGuideBookId());
	displayGuideBook.setSelectedOptions( selectedEquipmentOptions );
	
	if ( displayGuideBookValuesNoMileage != null )
	{
		displayGuideBook.setDisplayGuideBookValuesNoMileage( sortThirdPartyValues(provider, displayGuideBookValuesNoMileage ) );
	}
	displayGuideBook.setMsrp( getMsrp() );
	displayGuideBook.setWeight( getWeight() );
	displayGuideBook.setMileageAdjustment( getMileageAdjustment() );
	displayGuideBook.setLastBookedOutDate( getLastBookedOutDate() );
	displayGuideBook.setStoredInfo( true );
	displayGuideBook.setSuccess( true );
	displayGuideBook.setPublishInfo(getMetaInfo().getPublishInfo());
	displayGuideBook.setHasErrors(Boolean.FALSE);
		
	return displayGuideBook;
}

public List<VDP_GuideBookValue> getBookValues()
{
	return bookValues;
}

public void setBookValues( List<VDP_GuideBookValue> bookValues )
{
	this.bookValues = bookValues;
}

public Integer getMileageAdjustment()
{
	return mileageAdjustment;
}

public void setMileageAdjustment( Integer mileageAdjustment )
{
	this.mileageAdjustment = mileageAdjustment;
}

public Date getLastBookedOutDate()
{
	return lastBookedOutDate;
}

public void setLastBookedOutDate( Date lastBookedOutDate )
{
	this.lastBookedOutDate = lastBookedOutDate;
}

public Integer getMsrp()
{
	return msrp;
}

public void setMsrp( Integer msrp )
{
	this.msrp = msrp;
}

public Integer getWeight()
{
	return weight;
}

public void setWeight( Integer weight )
{
	this.weight = weight;
}

public List<ThirdPartyVehicleOption> getOptionsFromBook()
{
	return optionsFromBook;
}

public void setOptionsFromBook( List<ThirdPartyVehicleOption> freshOptionsFromBook )
{
	this.optionsFromBook = freshOptionsFromBook;
}

public String getSelectedVehicleKey()
{
	// We don't care at this stage what the selected vehicle key is (should go away with refactoring)
	// 11/15/2005 - MH
	return null;
}

public List<ThirdPartyVehicleOption> getThirdPartyVehicleOptions()
{
	return optionsFromBook;
}

public List<VDP_GuideBookVehicle> getDropDownVehicles()
{
	// TODO Auto-generated method stub
	return null;
}

public List<VDP_GuideBookVehicle> getDropDownModels(){
	// TODO Auto-generated method stub
	return null;
}
}
