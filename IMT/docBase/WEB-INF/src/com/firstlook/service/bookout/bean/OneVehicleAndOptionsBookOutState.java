package com.firstlook.service.bookout.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.service.appraisal.StagingBookOut;

import com.firstlook.display.tradeanalyzer.DisplayGuideBook;
import com.firstlook.service.bookout.BookOutService;
import com.firstlook.thirdparty.book.old.VDP_SelectedKeys;

public class OneVehicleAndOptionsBookOutState extends AbstractBookOutState implements ISingleVehicleState
{

private String model;
private String make;
private String vehicleYear;
private List<ThirdPartyVehicleOption> thirdPartyVehicleOptions;
private VDP_GuideBookVehicle guideBookVehicle;
private String selectedVehicleKey;
private StagingBookOut stagingBookOut;


public DisplayGuideBook transformBookOutToDisplayData( DisplayGuideBook displayGuideBook )
{

	displayGuideBook.setMake( getMake() );
	displayGuideBook.setModel( getModel() );
	displayGuideBook.setYear( getVehicleYear() );
	
	
	displayGuideBook.setGuideBookName( getMetaInfo().getGuideBookTitle() );
	displayGuideBook.setPublishInfo( getMetaInfo().getPublishInfo() );
	displayGuideBook.setFooter( getMetaInfo().getFooter() );
	displayGuideBook.setImageName( getMetaInfo().getImageName() );
	displayGuideBook.setBgColor( getMetaInfo().getBgColor() );

	VDP_SelectedKeys selectedKeys;
	
    List<ThirdPartyVehicleOption> allOptions = getThirdPartyVehicleOptions();
    
    if (stagingBookOut != null){
    	selectedKeys = BookOutService.retrieveSelectedKeysAndSetSelectedFalse( allOptions, stagingBookOut ); 
    }
    else{
    	selectedKeys = BookOutService.retrieveSelectedKeysAndSetSelectedFalse( allOptions ); 
    }
    
    displayGuideBook.setDisplayGuideBookOptions( allOptions );
    displayGuideBook.setSelectedDrivetrainKey( selectedKeys.getSelectedDrivetrainKey() );
    displayGuideBook.setSelectedEngineKey( selectedKeys.getSelectedEngineKey() );
    displayGuideBook.setSelectedTransmissionKey( selectedKeys.getSelectedTransmissionKey() );
    displayGuideBook.setSelectedEquipmentOptionKeys( selectedKeys.getSelectedEquipmentOptionKeys() );
    
	displayGuideBook.setGuideBookId( Integer.valueOf( getMetaInfo().getGuideBookId() ) );
	
	List<VDP_GuideBookVehicle> dropDownVehicles = new ArrayList<VDP_GuideBookVehicle>();
	VDP_GuideBookVehicle vehicle = getGuideBookVehicle();
	dropDownVehicles.add( vehicle );
	
	displayGuideBook.setDisplayGuideBookVehicles( dropDownVehicles );
	displayGuideBook.setSelectedVehicleKey( getSelectedVehicleKey() );
	displayGuideBook.setWeight(vehicle.getWeight());
	displayGuideBook.setLastBookedOutDate( new Date() );
	displayGuideBook.setValidVin( true );
	displayGuideBook.setSuccess( true );
	
	return displayGuideBook;
}

public List<ThirdPartyVehicleOption> getThirdPartyVehicleOptions()
{
	return thirdPartyVehicleOptions;
}

public void setThirdPartyVehicleOptions( List<ThirdPartyVehicleOption> guideBookOptions )
{
	this.thirdPartyVehicleOptions = guideBookOptions;
}

public String getMake()
{
	return make;
}

public void setMake( String make )
{
	this.make = make;
}

public String getSelectedVehicleKey()
{
	return selectedVehicleKey;
}

public void setSelectedVehicleKey( String selectedVehicleKey )
{
	this.selectedVehicleKey = selectedVehicleKey;
}

public String getVehicleYear()
{
	return vehicleYear;
}

public void setVehicleYear( String vehicleYear )
{
	this.vehicleYear = vehicleYear;
}

public VDP_GuideBookVehicle getGuideBookVehicle()
{
	return guideBookVehicle;
}

public void setGuideBookVehicle( VDP_GuideBookVehicle vehicle )
{
	this.guideBookVehicle = vehicle;
}

public List<VDP_GuideBookVehicle> getDropDownVehicles()
{
    List<VDP_GuideBookVehicle> vehicles = new ArrayList<VDP_GuideBookVehicle>();
    vehicles.add( this.guideBookVehicle );
    return vehicles;
}

public VDP_GuideBookVehicle getSelectedVehicle()
{
	return this.guideBookVehicle;
}

public String getModel()
{
	return model;
}

public void setModel( String model )
{
	this.model = model;
}

public StagingBookOut getStagingBookOut() {
	return stagingBookOut;
}

public void setStagingBookOut(StagingBookOut stagingBookOut) {
	this.stagingBookOut = stagingBookOut;
}

public List<VDP_GuideBookVehicle> getDropDownModels(){
	// TODO Auto-generated method stub
	return null;
}
}
