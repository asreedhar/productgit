package com.firstlook.service.bookout.inventory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import biz.firstlook.module.bookout.IThirdPartyBookoutService;
import biz.firstlook.module.vehicle.description.old.BookOutDatasetInfo;
import biz.firstlook.module.vehicle.description.old.BookOutTypeEnum;
import biz.firstlook.module.vehicle.description.old.GBException;
import biz.firstlook.module.vehicle.description.old.GuideBookInput;
import biz.firstlook.module.vehicle.description.old.GuideBookMetaInfo;
import biz.firstlook.module.vehicle.description.old.KBBConditionEnum;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.enumerator.BookoutStatusEnum;
import biz.firstlook.transact.persist.exception.SelectedThirdPartyVehicleNotFoundException;
import biz.firstlook.transact.persist.model.BookOut;
import biz.firstlook.transact.persist.model.BookOutSource;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.InventoryWithBookOut;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.persistence.IDealerPreferenceDAO;
import biz.firstlook.transact.persist.persistence.IInventoryBookOutDAO;
import biz.firstlook.transact.persist.persistence.InventoryBookOutXmlStoredProcedure;
import biz.firstlook.transact.persist.service.AuditBookOutsService;
import biz.firstlook.transact.persist.service.ThirdPartyVehicleOptionService;

import com.firstlook.batch.bookout.distributed.model.BookoutProcessorStatus;
import com.firstlook.display.tradeanalyzer.DisplayGuideBook;
import com.firstlook.service.bookout.BookOutAssembler;
import com.firstlook.service.bookout.BookOutService;
import com.firstlook.service.bookout.appraisal.AppraisalBookOutService;
import com.firstlook.service.bookout.bean.AbstractBookOutState;
import com.firstlook.service.bookout.bean.BookValuesBookOutState;
import com.firstlook.service.bookout.bean.CompleteStoredBookOutState;
import com.firstlook.service.bookout.bean.IncompleteStoredBookOutState;
import com.firstlook.service.bookout.bean.OneVehicleAndOptionsBookOutState;
import com.firstlook.service.bookout.bean.StoredBookOutState;
import com.firstlook.service.bookout.bean.VehiclesNoOptionsBookOutState;
import com.firstlook.thirdparty.book.ThirdPartyBookoutServiceFactory;

public class InventoryBookOutService
{

private static final Logger logger = Logger.getLogger(InventoryBookOutService.class.toString());
	
private BookOutService bookOutService;
private BookOutAssembler bookOutAssembler;
private IInventoryBookOutDAO inventoryBookOutDAO;
private AppraisalBookOutService appraisalBookOutService;
private AuditBookOutsService auditBookOutsService;
private ThirdPartyBookoutServiceFactory thirdPartyBookoutServiceFactory;
private IDealerPreferenceDAO dealerPrefDAO;
private InventoryBookOutXmlStoredProcedure inventoryBookOutXmlStoredProcedure;

public AbstractBookOutState retrieveExistingBookOutInfoOrBeginNewBookOut( GuideBookInput input, Integer thirdPartyId, Integer inventoryId, 
																			int searchAppraisalDaysBackThreshold, Boolean forceBookoutFromVin) throws GBException
{
	String modelId = input.getSelectedModelId(); 	
	AbstractBookOutState freshState = null;
	StoredBookOutState storedState = retrieveStoredBookOutInfo( input, thirdPartyId, inventoryId, searchAppraisalDaysBackThreshold );
	//-----------new logic for selecting the modelId for the KBB exceptional scenario where vin doesn't decode to any trim----//
	if(storedState != null && thirdPartyId==3){
		String SelectedmodelId= "";
		List<VDP_GuideBookVehicle> dropDownVehicles = storedState.getDropDownVehicles();
		if(dropDownVehicles != null && !dropDownVehicles.isEmpty()){
			SelectedmodelId = dropDownVehicles.get(0).getModelId();
		}
		//select the modelID for the execution of exceptional scenario of KBB where VIn doesn't decode to trim
		input.setSelectedModelId(SelectedmodelId);
		input.setMake(storedState.getMake());
		input.setModel(storedState.getModel());
	}
	if(modelId!=null && !modelId.equals("") && modelId !=input.getSelectedModelId() && thirdPartyId==3)
	{
		storedState= null;
		input.setSelectedModelId(modelId);
	}
	//--------new logic ends--------------------//
	
	if ( storedState == null )
	{
		freshState = getBookOutService().doVinLookup( input, thirdPartyId, new BookOutDatasetInfo( BookOutTypeEnum.INVENTORY, input.getBusinessUnitId()), null );

		return freshState;
	}
	else
	{
		
		// rebook from VIN if forced
		// to ensure the BO is using the most up to date options/trims/values
		if (forceBookoutFromVin) {
			freshState = getBookOutService().doVinLookup( input, thirdPartyId, new BookOutDatasetInfo( BookOutTypeEnum.INVENTORY, input.getBusinessUnitId()), null );
			if (thirdPartyId == ThirdPartyDataProvider.KELLEY.getThirdPartyId().intValue()) {
				for (VDP_GuideBookVehicle newVehicle : freshState.getDropDownVehicles()) {
					boolean conditionSet = false;
					for (VDP_GuideBookVehicle oldVehicle : storedState.getDropDownVehicles()) {
						if (newVehicle.getKey().equals(oldVehicle.getKey())) {
							newVehicle.setCondition(oldVehicle.getCondition());
							conditionSet = true;
							break;
						}
					}
					if (!conditionSet) {
						newVehicle.setCondition(KBBConditionEnum.EXCELLENT);
					}
				}
			}
			storedState.setDropDownVehicles(freshState.getDropDownVehicles());
		}

		VDP_GuideBookVehicle guideBookVehicle = BookOutService.determineSelectedVehicleByKey( storedState.getDropDownVehicles(),
																								storedState.getSelectedVehicleKey() );
		
		// what if selected is no longer in the fresh? - this is not good. But possible.
		if (guideBookVehicle == null && freshState != null && forceBookoutFromVin) {
			// no way to recover. Just get the user the latest data and let them rebook.
			return freshState;
		}
		
		List<ThirdPartyVehicleOption> optionsFromBook = new ArrayList<ThirdPartyVehicleOption>();
		try
		{
			optionsFromBook = getBookOutService().retrieveOptionsForVehicle( input, guideBookVehicle, thirdPartyId, new BookOutDatasetInfo( BookOutTypeEnum.INVENTORY, input.getBusinessUnitId()) );
		}
		catch ( Exception e )
		{
			optionsFromBook = storedState.getThirdPartyVehicleOptions();
		}
		storedState.setThirdPartyVehicleOptions( ThirdPartyVehicleOptionService.refreshNewOptions( optionsFromBook,
																									storedState.getThirdPartyVehicleOptions() ) );
		
		// new logic for getting the models drop down too on coming back from the 3rd page/inventory
		if(thirdPartyId == 3){
			AbstractBookOutState abstractState;
			BookOutDatasetInfo bookOutDatasetInfo =  new BookOutDatasetInfo( BookOutTypeEnum.APPRAISAL, input.getBusinessUnitId());
			abstractState = getBookOutService().doVinLookup( input, thirdPartyId,bookOutDatasetInfo , null );
			storedState.setSelectedModelKey(input.getSelectedModelId());
			storedState.setDropDownModels(abstractState.getDropDownModels());
		}
	}
	return storedState;
}

/**
 * 
 * @param input
 * @param thirdPartyId
 * @param inventoryWithBookOut
 * @return AbstractBookOutState:
 *         <ul>
 *         <li>1) VehicleNoOptionsBookoutState - multiple vehicles</li>
 *         <li> 2) OneVehicleAndOptionsBookoutState - get all info about a vehicle from book.</li>
 *         <li> 3) StoredBookoutState - has correct information.</li>
 *         </ul>
 * @throws GBException
 * @throws SQLException 
 */
//@SuppressWarnings("unchecked")
public AbstractBookOutState retrieveExistingBookOutInfoForProcessor(GuideBookInput input, Integer thirdPartyId, InventoryWithBookOut inventoryWithBookOut, int searchInactiveInventoryDaysBackThreshold )
		throws GBException, SQLException
{ 
	StoredBookOutState bookoutState = retrieveStoredBookOutInfoForProcessor( input, thirdPartyId, inventoryWithBookOut, searchInactiveInventoryDaysBackThreshold );

	if ( bookoutState == null || bookoutState.getSelectedVehicleKey() == null)
	{
		AbstractBookOutState abstractBookOutState = null;
		abstractBookOutState = getBookOutService().doVinLookup( input, thirdPartyId, new BookOutDatasetInfo( BookOutTypeEnum.INVENTORY, input.getBusinessUnitId()), null );
		abstractBookOutState.setBookoutStatus(BookoutStatusEnum.NOT_REVIEWED );
		abstractBookOutState.setBookOutSourceId( BookOutSource.BOOK_OUT_SOURCE_PROCESSOR );
		
		if ( ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE == thirdPartyId.intValue() && abstractBookOutState != null )
		{
			setFefaultConditionForDropDownVehicles( abstractBookOutState.getDropDownVehicles(), input.getBusinessUnitId() );
		}
		
		return abstractBookOutState;
	}
	else
	{
		VDP_GuideBookVehicle guideBookVehicle = BookOutService.determineSelectedVehicleByKey( bookoutState.getDropDownVehicles(),
																								bookoutState.getSelectedVehicleKey() );
		List<ThirdPartyVehicleOption> optionsFromBook = getBookOutService().retrieveOptionsForVehicle( input, guideBookVehicle, thirdPartyId, new BookOutDatasetInfo( BookOutTypeEnum.INVENTORY, input.getBusinessUnitId()) );
		logger.debug("Status " + bookoutState.getBookoutStatus().getId());
		
		// =================== starting of case 31410 =======================================
		
		BookOut bookOut = inventoryWithBookOut.getLatestBookOut( thirdPartyId );
		if(bookOut != null && bookOut.getBookoutStatusId().intValue() == BookoutStatusEnum.PARTIAL.getId().intValue())
		{
			List<ThirdPartyVehicleOption> listOfThirdPartyVehicleOptions = inventoryBookOutDAO.getInventoryExternalValuation(inventoryWithBookOut.getInventoryId(), thirdPartyId);
				if(!listOfThirdPartyVehicleOptions.isEmpty()){
					//bookoutState.getThirdPartyVehicleOptions().addAll(listOfThirdPartyVehicleOptions);
					bookoutState.setThirdPartyVehicleOptions(listOfThirdPartyVehicleOptions);
				}
		}
		bookOut = null;
		//============ end of case 31410 ===============================
		
		
		bookoutState.setThirdPartyVehicleOptions( ThirdPartyVehicleOptionService.refreshNewOptions( optionsFromBook,
																									bookoutState.getThirdPartyVehicleOptions() ) );
	}
	return bookoutState;
}

/**
 * Special 'light' method for determining if a bookout exists for the given inventoryId for the give thirdPartyId
 * 
 * @param thirdPartyId
 * @param inventoryId
 * @return
 */
public boolean hasBookOutAvailable( Integer thirdPartyId, Integer inventoryId )
{
	boolean result = false;
	InventoryWithBookOut inventoryWithBookOut = getInventoryBookOutDAO().findByPkWithThirdPartyCategories( inventoryId );
	BookOut bookOutFromInventory = inventoryWithBookOut.getLatestBookOut( thirdPartyId );
	if ( bookOutFromInventory != null )
	{
		result = true;
	}
	return result;
}

/**
 * Returns either the StoredBookOutState for the InventoryBookOut or, if an InventoryBookOut doesn't exist
 * the StoredBookOutState for the AppraisalBookOut
 * 
 * 
 * @param input
 * @param thirdPartyId
 * @param inventoryId
 * @param extraInfo
 * @param searchInactiveInventoryDaysBackThreshold
 * @return
 * @throws GBException
 */
public StoredBookOutState retrieveStoredBookOutInfo( GuideBookInput input, Integer thirdPartyId, Integer inventoryId,
													int searchInactiveInventoryDaysBackThreshold ) throws GBException
{
	StoredBookOutState state = null;
	InventoryWithBookOut inventoryWithBookOut = getInventoryBookOutDAO().findByPkWithThirdPartyCategories( inventoryId );
	BookOut bookOutFromInventory = inventoryWithBookOut.getLatestBookOut( thirdPartyId );

	if ( bookOutFromInventory != null )
	{
		state = constructStoredInfoFromInventoryBookOut( input, thirdPartyId, inventoryWithBookOut, bookOutFromInventory );
		getBookOutService().flagBookoutIfInaccurateAndSaveIfSo( inventoryWithBookOut.getMileageReceived(), bookOutFromInventory, state );
	}
	if ( state == null )
	{
		state = getAppraisalBookOutService().constructStoredInfoFromAppraisalBookOut( input, thirdPartyId, searchInactiveInventoryDaysBackThreshold );
	}
	return state;
}

public StoredBookOutState retrieveStoredBookOutInfoForProcessor( GuideBookInput input, Integer thirdPartyId,
																InventoryWithBookOut inventoryWithBookOut, int searchInactiveInventoryDaysBackThreshold )
		throws GBException
{
	StoredBookOutState state = null;
	BookOut bookOut = inventoryWithBookOut.getLatestBookOut( thirdPartyId );

	if ( bookOut != null )
	{
		ThirdPartyVehicle selectedVehicleFromBook = null;
		Collection<ThirdPartyVehicle> thirdPartyVehiclesFromDB = null;
		//only need condition for kbb runs of processor
		KBBConditionEnum condition = null;
		try {
			selectedVehicleFromBook = bookOut.getSelectedThirdPartyVehicle();
			
			thirdPartyVehiclesFromDB = bookOut.getThirdPartyVehicles();
			
			if ( thirdPartyId.intValue() == ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE )
			{
				condition = findDefaultInventoryCondition( input.getBusinessUnitId() );
			}
			
			state = new CompleteStoredBookOutState();
			state.setDropDownVehicles( getBookOutAssembler().transformThirdPartyVehiclesToGuideBookVehicles( bookOut, thirdPartyVehiclesFromDB,
																											 inventoryWithBookOut.getVehicle().getVehicleYear(),
																											 condition ) );
			state.setSelectedVehicleKey( selectedVehicleFromBook.getThirdPartyVehicleCode() );
			state.setThirdPartyVehicleOptions( selectedVehicleFromBook.getThirdPartyVehicleOptions() );
			
		} catch (SelectedThirdPartyVehicleNotFoundException e) {
			
			state = new IncompleteStoredBookOutState();
			state.setDropDownVehicles( getBookOutAssembler().transformThirdPartyVehiclesToGuideBookVehicles( bookOut, thirdPartyVehiclesFromDB,
																											 inventoryWithBookOut.getVehicle().getVehicleYear(),
																											 condition ) );
			IThirdPartyBookoutService thirdPartyBookoutService;
			thirdPartyBookoutService = getThirdPartyBookoutServiceFactory().retrieveThirdPartyBookoutService( thirdPartyId );

			String region = thirdPartyBookoutService.retrieveRegionDescription( input );
			GuideBookMetaInfo footer = thirdPartyBookoutService.getMetaInfo( region, new BookOutDatasetInfo(BookOutTypeEnum.INVENTORY, inventoryWithBookOut.getDealerId() ) );
			state.setMetaInfo( footer );
		}
		
		
		
		if ( bookOut != null )
		{
			state.setBookOutSourceId( bookOut.getBookOutSourceId() );
			if ( bookOut.getBookOutSourceId() == BookOutSource.BOOK_OUT_SOURCE_PROCESSOR )
			{
				state.setBookoutStatus(BookoutStatusEnum.getStatus(bookOut.getBookoutStatusId()));
			}
		}
		else
		{
			state.setBookOutSourceId( BookOutSource.BOOK_OUT_SOURCE_PROCESSOR );
		}
		
	}

	if ( state == null )
	{
		state = getAppraisalBookOutService().constructAppraisalBookOutForProcessor( input, thirdPartyId, searchInactiveInventoryDaysBackThreshold );

		if ( ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE == thirdPartyId.intValue() && state != null )
		{
			setFefaultConditionForDropDownVehicles( state.getDropDownVehicles(), input.getBusinessUnitId() );
		}
		
	}
	return state;
}

private void setFefaultConditionForDropDownVehicles( List<VDP_GuideBookVehicle> dropDownVehicles, Integer businessUnitId )
{
	Iterator< VDP_GuideBookVehicle > iter = dropDownVehicles.iterator();
	while ( iter.hasNext() )
	{
		VDP_GuideBookVehicle vehicle = (VDP_GuideBookVehicle)iter.next();
		vehicle.setCondition( findDefaultInventoryCondition( businessUnitId ) );
	}
}

private StoredBookOutState constructStoredInfoFromInventoryBookOut( GuideBookInput input, Integer thirdPartyId, InventoryWithBookOut inventoryBookOut, BookOut bookOut )
		throws GBException
{
	BookOut bookout = inventoryBookOut.getLatestBookOut( thirdPartyId );
	try {
		if( bookout != null ) {
			ThirdPartyVehicle selectedVehicleFromInventory = bookout.getSelectedThirdPartyVehicle();
			//only want to lookup default condition if we're doing a KBB bookout
			KBBConditionEnum condition = null;
			if ( thirdPartyId.intValue() == ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE ) {
				condition = findDefaultInventoryCondition( input.getBusinessUnitId() );
			}

			Collection<ThirdPartyVehicle> thirdPartyVehicles = bookout.getThirdPartyVehicles();
			return getBookOutService().constructCompleteStoredBookOutInfo( 
					bookOut, inventoryBookOut.getVehicle(), thirdPartyVehicles,
					selectedVehicleFromInventory, thirdPartyId, inventoryBookOut.getMileageReceived(), 
					condition );			
		}
	} catch (SelectedThirdPartyVehicleNotFoundException e) {
		logger.info("No ThirdPartyVehicles found for Bookout Id: " + (bookout != null ? bookout.getBookOutId() : null));
	}

	List<VDP_GuideBookVehicle> guideBookVehicles = new ArrayList<VDP_GuideBookVehicle>();
	List<ThirdPartyVehicleOption> guideBookOptions = new ArrayList<ThirdPartyVehicleOption>();

	AbstractBookOutState bookOutState = getBookOutService().doVinLookup( input, thirdPartyId, new BookOutDatasetInfo( BookOutTypeEnum.INVENTORY, input.getBusinessUnitId()), null );

	if ( bookOutState instanceof OneVehicleAndOptionsBookOutState ) {
		OneVehicleAndOptionsBookOutState oneVehicleAndOptionsBookOutState = (OneVehicleAndOptionsBookOutState)bookOutState;
		guideBookVehicles = oneVehicleAndOptionsBookOutState.getDropDownVehicles();
		guideBookOptions = oneVehicleAndOptionsBookOutState.getThirdPartyVehicleOptions();
	} else {
		VehiclesNoOptionsBookOutState vehiclesNoOptionsState = (VehiclesNoOptionsBookOutState)bookOutState;
		guideBookVehicles = vehiclesNoOptionsState.getDropDownVehicles();
	}
	return getBookOutService().constructIncompleteStoredBookOutInfo( 
			bookOut, inventoryBookOut.getVehicle(), guideBookVehicles,
			guideBookOptions, thirdPartyId, inventoryBookOut.getMileageReceived() );
}

public BookValuesBookOutState updateBookValues( GuideBookInput input, 
												VDP_GuideBookVehicle vehicle, List<ThirdPartyVehicleOption> incomingOptionList, List<VDP_GuideBookVehicle> dropDownVehicles,
												Integer bookOutType, InventoryWithBookOut inventoryWithBookOut,
												Integer thirdPartyId, Integer bookOutSourceId, BookoutStatusEnum bookOutStatus , Boolean advertisingStatus) throws GBException
{
	advertisingStatus = (advertisingStatus == null) ? false : advertisingStatus;
	BookValuesBookOutState bookValuesState = getBookOutService().updateBookValues( input, thirdPartyId, vehicle, incomingOptionList, new BookOutDatasetInfo( BookOutTypeEnum.INVENTORY, input.getBusinessUnitId()) );

	if ( bookValuesState.getBookOutErrors().isEmpty() )
	{
		// nk : S672 - Remove flag so incremental BO does not pick this guy up again
		inventoryWithBookOut.clearIncrementalBookout(ThirdPartyDataProvider.getThirdPartyDataProvider(thirdPartyId));
		saveUpdatedBookInfo( input, bookValuesState.getBookValues(), bookValuesState.getOptionsFromBook(), dropDownVehicles, vehicle.getKey(),
								thirdPartyId.intValue(), inventoryWithBookOut, bookValuesState.getMileageAdjustment(),
								bookValuesState.getMsrp(), bookValuesState.getWeight(), bookValuesState.getMetaInfo(), bookOutType, bookOutSourceId, bookOutStatus, vehicle.getVic(), advertisingStatus );
	}
	return bookValuesState;
}


/**
 * TODO: These save methods should probably not be persisting Inventory information, but rather the book out information only. Refactor during
 * next iteration - MH, NK, 11/09/2005
 * 
 * Refactoring is a must. The InventoryWithBookOut loses it's reference to the bookout set somehow... then the persisting gets messed up. - BF.
 * @param msrp TODO
 * @param vic TODO
 */
public void saveUpdatedBookInfo( GuideBookInput input, List<VDP_GuideBookValue> bookValues, List<ThirdPartyVehicleOption> options, List<VDP_GuideBookVehicle> dropDownVehicles,
								String selectedVehicleKey, int guideBookId, InventoryWithBookOut inventoryWithBookOut,
								Integer mileageAdjustment, Integer msrp, Integer weight, GuideBookMetaInfo metaInfo, Integer bookOutType, Integer bookOutSourceId, BookoutStatusEnum bookOutStatus, String vic , Boolean advertisingStatus ) throws GBException
{
	Date today = new Date();
	
	BookOut bookOut = BookOutService.constructNewBookOut( bookValues, guideBookId, today, input.getMileage(), mileageAdjustment, msrp, weight, metaInfo, bookOutSourceId );
	// nk : DE623
	bookOut.setBookoutStatusId(bookOutStatus.getId());
	
	advertisingStatus = (advertisingStatus == null) ? false : advertisingStatus;
	bookOut.setAdvertisingStatus(advertisingStatus);
	
	Set<ThirdPartyVehicle> thirdPartyVehicles = new HashSet<ThirdPartyVehicle>();
	BookOut previousBookout = inventoryWithBookOut.getLatestBookOut( guideBookId );
	if( previousBookout != null )
		thirdPartyVehicles.addAll( previousBookout.getThirdPartyVehicles() );
	if ( thirdPartyVehicles != null && !thirdPartyVehicles.isEmpty() && selectedThirdPartyVehicleExistsInList(selectedVehicleKey, thirdPartyVehicles))
	{
		Set<ThirdPartyVehicle> tpvs = getBookOutService().updateExistingThirdPartyVehicles( options, selectedVehicleKey, today, thirdPartyVehicles,
																guideBookId, dropDownVehicles );
		bookOut.setThirdPartyVehicles( tpvs );
	}
	else
	{
		Set<ThirdPartyVehicle> tpvs = getBookOutService().createNewThirdPartyVehicles( options, dropDownVehicles, selectedVehicleKey, guideBookId, today );
		bookOut.setThirdPartyVehicles( tpvs );
	}
	inventoryWithBookOut.addBookOut( bookOut );
	
	// persist the VIC if it can be decoded
	if ( StringUtils.isNotBlank( vic ) ) { // bookout processor will pass in "" if it can't decode to unique key
		inventoryWithBookOut.getVehicle().setVic( vic );
	}
	
	saveUpdatedInventoryWithBookOut( input, inventoryWithBookOut, guideBookId, bookOutType, bookOut );
}

// DE1680 - when books change and remove TPVs, we can't rely on saved TPV values 
private boolean selectedThirdPartyVehicleExistsInList(String selectedVehicleKey, Set<ThirdPartyVehicle> thirdPartyVehicles) {
	boolean result = false;
	Iterator<ThirdPartyVehicle> tpvs = thirdPartyVehicles.iterator();
	while(tpvs.hasNext()) {
		ThirdPartyVehicle tpv = tpvs.next();
		if (tpv.getThirdPartyVehicleCode().equals(selectedVehicleKey)) {
			result = true;
			break;
		}
	}
	return result;
}

public List<VDP_GuideBookValue> constructMileageAdjustedValues( DisplayGuideBook displayGuideBook )
{
	List<VDP_GuideBookValue> mileageAdjustedValues = new ArrayList<VDP_GuideBookValue>();
	// hack to get mileage adjusted values onto request
	if ( displayGuideBook.getMileageAdjustment() != null )
	{
		VDP_GuideBookValue adjustedValue = null;
		Iterator<VDP_GuideBookValue> iter = displayGuideBook.getDisplayGuideBookValues().iterator();
		while ( iter.hasNext() )
		{
			VDP_GuideBookValue value = iter.next();
			adjustedValue = new VDP_GuideBookValue();
			adjustedValue.setHasErrors( value.isHasErrors() );
			adjustedValue.setError( value.getError() );
			adjustedValue.setThirdPartyCategoryDescription( value.getThirdPartyCategoryDescription() );
			adjustedValue.setThirdPartyCategoryId( value.getThirdPartyCategoryId() );
			if ( value.getValue() != null )
			{
				adjustedValue.setValue( new Integer( value.getValue().intValue() - displayGuideBook.getMileageAdjustment().intValue() ) );
			}
			else
			{
				adjustedValue.setValue( null );
			}
			mileageAdjustedValues.add( adjustedValue );
		}

		return mileageAdjustedValues;
	}
	else
	{
		return null;
	}

}

private void saveUpdatedInventoryWithBookOut( GuideBookInput input, InventoryWithBookOut inventoryWithBookOut, int guideBookId,
												Integer bookOutType, BookOut bookOut )
{
	String xml = BookOutXmlHelper.toInsertXml(inventoryWithBookOut, guideBookId);
	Integer bookoutId = null;
	try {
		bookoutId = inventoryBookOutXmlStoredProcedure.save(xml);
	} catch(Exception e) {
		logger.error("Failed Saving Bookout: " + xml);
		throw new RuntimeException(e);
	}
	
	if(bookoutId != null) {
		getAuditBookOutsService().logBookOuts( input.getBusinessUnitId(), input.getMemberId(), bookOutType,
				input.getVin(), guideBookId, bookoutId );
	}
}

private KBBConditionEnum findDefaultInventoryCondition( Integer businessUnitId )
{
	DealerPreference dp = dealerPrefDAO.findByBusinessUnitId( businessUnitId );
	return KBBConditionEnum.getKBBConditionEnumById( dp.getKbbInventoryDefaultCondition() );
}

public BookOutService getBookOutService()
{
	return bookOutService;
}

public void setBookOutService( BookOutService bookOutService )
{
	this.bookOutService = bookOutService;
}

public IInventoryBookOutDAO getInventoryBookOutDAO()
{
	return inventoryBookOutDAO;
}

public void setInventoryBookOutDAO( IInventoryBookOutDAO inventoryBookOutDAO )
{
	this.inventoryBookOutDAO = inventoryBookOutDAO;
}

public AuditBookOutsService getAuditBookOutsService()
{
	return auditBookOutsService;
}

public void setAuditBookOutsService( AuditBookOutsService auditBookOutsService )
{
	this.auditBookOutsService = auditBookOutsService;
}

public BookOutAssembler getBookOutAssembler()
{
	return bookOutAssembler;
}

public void setBookOutAssembler( BookOutAssembler bookOutAssembler )
{
	this.bookOutAssembler = bookOutAssembler;
}

public AppraisalBookOutService getAppraisalBookOutService()
{
	return appraisalBookOutService;
}

public void setAppraisalBookOutService( AppraisalBookOutService appraisalBookOutService )
{
	this.appraisalBookOutService = appraisalBookOutService;
}

public ThirdPartyBookoutServiceFactory getThirdPartyBookoutServiceFactory()
{
	return thirdPartyBookoutServiceFactory;
}

public void setThirdPartyBookoutServiceFactory( ThirdPartyBookoutServiceFactory thirdPartyBookoutServiceFactory )
{
	this.thirdPartyBookoutServiceFactory = thirdPartyBookoutServiceFactory;
}

public void setDealerPrefDAO(IDealerPreferenceDAO dealerPrefDAO) {
	this.dealerPrefDAO = dealerPrefDAO;
}

public void setInventoryBookOutXmlStoredProcedure(
		InventoryBookOutXmlStoredProcedure inventoryBookOutXmlStoredProcedure) {
	this.inventoryBookOutXmlStoredProcedure = inventoryBookOutXmlStoredProcedure;
}

}