package com.firstlook.service.risklevel;

import java.util.Calendar;

import junit.framework.TestCase;
import biz.firstlook.commons.enumeration.LightAndCIATypeDescriptor;
import biz.firstlook.transact.persist.model.GroupingDescriptionLight;
import biz.firstlook.transact.persist.model.LightDetails;
import biz.firstlook.transact.persist.persistence.MockGroupingDescriptionLightPersistence;

import com.firstlook.entity.DealerRisk;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.dealer.MockUCBPPreferenceService;

public class TestGroupingDescriptionLightService extends TestCase
{

private GroupingDescriptionLightService service;
private MockGroupingDescriptionLightPersistence persistence;

public TestGroupingDescriptionLightService( String name )
{
	super( name );
	persistence = new MockGroupingDescriptionLightPersistence();
	service = new GroupingDescriptionLightService();
	service.setGroupingDescriptionLightPersistence( persistence );
	service.setUcbpPreferenceService( new MockUCBPPreferenceService() );
}

public void setUp()
{

}

public void tearDown()
{
	persistence.clear();
}

public void testCheckHighMileageFailNextYear()
{
	int highMileageThreshold = 80000;
	int highMileagePerYearThreshold = 40000;

	Calendar calendar = Calendar.getInstance();
	calendar.add( Calendar.YEAR, 1 );

	int year = calendar.get( Calendar.YEAR );
	int mileage = 50000;

	assertEquals( "Should be highMileage", true, service.checkHighMileage( mileage, year, highMileagePerYearThreshold, highMileageThreshold ) );
}

public void testCheckHighMileagePass()
{
	int highMileageThreshold = 80000;
	int highMileagePerYearThreshold = 40000;

	Calendar calendar = Calendar.getInstance();
	calendar.add( Calendar.YEAR, -1 );

	int year = calendar.get( Calendar.YEAR );
	int mileage = 10000;

	assertEquals( "Should NOT be highMileage", false, service.checkHighMileage( mileage, year, highMileagePerYearThreshold, highMileageThreshold ) );
}

public void testCheckHighMileageFailOverall()
{
	int highMileageThreshold = 80000;
	int highMileagePerYearThreshold = 20000;

	Calendar calendar = Calendar.getInstance();
	calendar.add( Calendar.YEAR, -3 );

	int year = calendar.get( Calendar.YEAR );
	int mileage = 90000;

	assertEquals( "Should be highMileage", true, service.checkHighMileage( mileage, year, highMileagePerYearThreshold, highMileageThreshold ) );
}

public void testCheckHighMileageFailYear()
{
	int highMileageThreshold = 80000;
	int highMileageYear = 40000;

	Calendar calendar = Calendar.getInstance();
	calendar.add( Calendar.YEAR, -1 );

	int year = calendar.get( Calendar.YEAR );
	int mileage = 50000;

	assertEquals( "Should be highMileage", true, service.checkHighMileage( mileage, year, highMileageYear, highMileageThreshold ) );
}

public void testOldCarTrue()
{
	int yearOffset = -6;

	Calendar calendar = Calendar.getInstance();
	calendar.add( Calendar.YEAR, ( yearOffset - 1 ) );
	int vehicleYear = calendar.get( Calendar.YEAR );

	assertTrue( service.checkOldCar( yearOffset, vehicleYear ) );
}

public void testOldCarFalse()
{
	int yearOffset = -5;

	Calendar calendar = Calendar.getInstance();
	calendar.add( Calendar.YEAR, yearOffset );
	int vehicleYear = calendar.get( Calendar.YEAR );

	assertTrue( !service.checkOldCar( yearOffset, vehicleYear ) );
}

public void testCheckHistoricalPerformanceWeak()
{
	assertEquals( "Should be weak", "Weak Performing Vehicle", service.checkHistoricalPerformance( LightAndCIATypeDescriptor.RED_LIGHT, false,
																									0, 0, 0 ) );
}

public void testCheckHistoricalPerformanceLevel4()
{
	assertEquals( "Should be blank", "", service.checkHistoricalPerformance( LightAndCIATypeDescriptor.RED_LIGHT, true, 0, 0, 0 ) );
}

public void testCheckHistoricalPerformanceNoSales()
{
	int numWeeks = 10;

	assertEquals( "Should be no", "No Sales History (" + numWeeks + " wks)",
					service.checkHistoricalPerformance( LightAndCIATypeDescriptor.GREEN_LIGHT, false, 0, 0, numWeeks ) );
}

public void testCheckHistoricalPerformanceLowSales()
{
	int weeks = 10;

	assertEquals( "Should be no", "Low Sales History (" + weeks + " wks)",
					service.checkHistoricalPerformance( LightAndCIATypeDescriptor.GREEN_LIGHT, false, 2, 3, weeks ) );
}

public void testCheckHistoricalPerformanceNone()
{
	int weeks = 10;

	assertEquals( "Should be null", "", service.checkHistoricalPerformance( LightAndCIATypeDescriptor.GREEN_LIGHT, false, 5, 3, weeks ) );
}

/**
 * All data in this test comes from reference data on an actual store. -bfung
 * 
 * This test is more a fixture test than a unit test.
 */
public void testRetrieveSpecificVehicleLightGreenLight()
{
	DealerRisk dealerRisk = new DealerRisk();
	dealerRisk.setHighMileagePerYearThreshold( 70000 );
	dealerRisk.setHighMileageThreshold(70000 );
	dealerRisk.setExcessiveMileageThreshold(100000 );
	dealerRisk.setRiskLevelYearRollOverMonth( 9 );
	dealerRisk.setRiskLevelYearInitialTimePeriod( 5 );
	dealerRisk.setRiskLevelYearSecondaryTimePeriod( 5 );
	
	int dealerId = 100215; // Not actually used in this test - reference data from DB.
	int groupingDescriptionId = 102431; // Not actually used in this test - reference data from DB; 2005 Toyota Camary.
	int vehicleMileage = 69000;
	
	Calendar cal = Calendar.getInstance();
	int vehicleYear = cal.get( Calendar.YEAR ) - dealerRisk.getRiskLevelYearInitialTimePeriod();
	if ( cal.get( Calendar.MONTH ) < dealerRisk.getRiskLevelYearRollOverMonth() )
		vehicleYear = cal.get( Calendar.YEAR ) - dealerRisk.getRiskLevelYearSecondaryTimePeriod();	

	GroupingDescriptionLight gdLight = new GroupingDescriptionLight();
	gdLight.setDealerId( dealerId );
	gdLight.setGroupingDescriptionId( groupingDescriptionId );
	gdLight.setLight( 3 ); // Expected Green Light

	persistence.save( gdLight ); // fake save; setup data

	LightDetails lightDetails = null;
	try
	{
		lightDetails = service.retrieveSpecificVehicleLight( dealerRisk, dealerId, groupingDescriptionId, vehicleMileage, vehicleYear );
	}
	catch ( ApplicationException e )
	{
		fail( e.getMessage() );
	}

	assertEquals( "Result did not return a green light", 3, lightDetails.getLight() );
}

}
