package com.firstlook.service.risklevel;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AllTests extends TestCase
{

public AllTests( String arg0 )
{
    super(arg0);
}

public static Test suite()
{
    TestSuite suite = new TestSuite();

    suite.addTestSuite(TestGroupingDescriptionLightService.class);
    suite.addTestSuite(TestConvertGridValuesService.class);
    return suite;
}
}
