package com.firstlook.service.risklevel;

import org.apache.log4j.Logger;

import biz.firstlook.commons.enumeration.LightAndCIATypeDescriptor;

public class ConvertGridValuesService
{

private static final Logger logger = Logger
        .getLogger(ConvertGridValuesService.class);

public ConvertGridValuesService()
{
}

public LightAndCIATypeDescriptor determineId( int lightValue, int ciaTypeValue )
{
    if ( lightValue == LightAndCIATypeDescriptor.RED_LIGHT )
    {
        return LightAndCIATypeDescriptor.RED_LIGHT_MC;
    } else if ( lightValue == LightAndCIATypeDescriptor.YELLOW_LIGHT )
    {
        if ( ciaTypeValue == LightAndCIATypeDescriptor.MARKET_PERFORMER_OR_MANAGERS_CHOICE )
        {
            return LightAndCIATypeDescriptor.YELLOW_LIGHT_MC_MP;
        } else if ( ciaTypeValue == LightAndCIATypeDescriptor.POWERZONE )
        {
            return LightAndCIATypeDescriptor.YELLOW_LIGHT_POWERZONE;
        }
    } else if ( lightValue == LightAndCIATypeDescriptor.GREEN_LIGHT )
    {
        if ( ciaTypeValue == LightAndCIATypeDescriptor.MARKET_PERFORMER_OR_OTHER_GREEN )
        {
            return LightAndCIATypeDescriptor.GREEN_LIGHT_MP_OG;
        } else if ( ciaTypeValue == LightAndCIATypeDescriptor.GOOD_BETS )
        {
            return LightAndCIATypeDescriptor.GREEN_LIGHT_GOOD_BETS;
        } else if ( ciaTypeValue == LightAndCIATypeDescriptor.WINNERS )
        {
            return LightAndCIATypeDescriptor.GREEN_LIGHT_WINNER;
        } else if ( ciaTypeValue == LightAndCIATypeDescriptor.POWERZONE )
        {
            return LightAndCIATypeDescriptor.GREEN_LIGHT_POWERZONE;
        }
    }
    logger.warn("There was an illegal combination of light and CIAClassType");
    return LightAndCIATypeDescriptor.RED_LIGHT_MC;

}

public LightAndCIATypeDescriptor retrieveById( int id )
{
    LightAndCIATypeDescriptor descriptor = LightAndCIATypeDescriptor.RED_LIGHT_MC;
    switch (id)
    {
        case 1:
            descriptor = LightAndCIATypeDescriptor.RED_LIGHT_MC;
            break;
        case 2:
            descriptor = LightAndCIATypeDescriptor.YELLOW_LIGHT_MC_MP;
            break;
        case 3:
            descriptor = LightAndCIATypeDescriptor.YELLOW_LIGHT_POWERZONE;
            break;
        case 4:
            descriptor = LightAndCIATypeDescriptor.GREEN_LIGHT_MP_OG;
            break;
        case 5:
            descriptor = LightAndCIATypeDescriptor.GREEN_LIGHT_GOOD_BETS;
            break;
        case 6:
            descriptor = LightAndCIATypeDescriptor.GREEN_LIGHT_WINNER;
            break;
        case 7:
            descriptor = LightAndCIATypeDescriptor.GREEN_LIGHT_POWERZONE;
            break;
        default:
            logger.warn(" There was an illegal LightAndCiaTypeDescriptorId ");
    }
    return descriptor;
}
}
