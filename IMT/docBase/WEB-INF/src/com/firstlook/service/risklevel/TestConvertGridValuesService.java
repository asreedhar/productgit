package com.firstlook.service.risklevel;

import junit.framework.TestCase;
import biz.firstlook.commons.enumeration.LightAndCIATypeDescriptor;

public class TestConvertGridValuesService extends TestCase
{

private ConvertGridValuesService service;

public TestConvertGridValuesService( String arg0 )
{
    super(arg0);
}

public void setUp()
{
    service = new ConvertGridValuesService();
}

public void testDetermineID()
{
    LightAndCIATypeDescriptor descriptor1 = service.determineId(
            LightAndCIATypeDescriptor.RED_LIGHT,
            LightAndCIATypeDescriptor.MANAGERS_CHOICE);
    LightAndCIATypeDescriptor descriptor2 = service.determineId(
            LightAndCIATypeDescriptor.YELLOW_LIGHT,
            LightAndCIATypeDescriptor.MARKET_PERFORMER_OR_MANAGERS_CHOICE);
    LightAndCIATypeDescriptor descriptor3 = service.determineId(
            LightAndCIATypeDescriptor.YELLOW_LIGHT,
            LightAndCIATypeDescriptor.POWERZONE);
    LightAndCIATypeDescriptor descriptor4 = service.determineId(
            LightAndCIATypeDescriptor.GREEN_LIGHT,
            LightAndCIATypeDescriptor.MARKET_PERFORMER_OR_OTHER_GREEN);
    LightAndCIATypeDescriptor descriptor5 = service.determineId(
            LightAndCIATypeDescriptor.GREEN_LIGHT,
            LightAndCIATypeDescriptor.GOOD_BETS);
    LightAndCIATypeDescriptor descriptor6 = service.determineId(
            LightAndCIATypeDescriptor.GREEN_LIGHT,
            LightAndCIATypeDescriptor.WINNERS);
    LightAndCIATypeDescriptor descriptor7 = service.determineId(
            LightAndCIATypeDescriptor.GREEN_LIGHT,
            LightAndCIATypeDescriptor.POWERZONE);

    assertEquals(LightAndCIATypeDescriptor.RED_LIGHT_MC, descriptor1);
    assertEquals(LightAndCIATypeDescriptor.YELLOW_LIGHT_MC_MP, descriptor2);
    assertEquals(LightAndCIATypeDescriptor.YELLOW_LIGHT_POWERZONE, descriptor3);
    assertEquals(LightAndCIATypeDescriptor.GREEN_LIGHT_MP_OG, descriptor4);
    assertEquals(LightAndCIATypeDescriptor.GREEN_LIGHT_GOOD_BETS, descriptor5);
    assertEquals(LightAndCIATypeDescriptor.GREEN_LIGHT_WINNER, descriptor6);
    assertEquals(LightAndCIATypeDescriptor.GREEN_LIGHT_POWERZONE, descriptor7);
}

public void testRetrieveById()
{
    LightAndCIATypeDescriptor descriptor1 = service.retrieveById(1);
    LightAndCIATypeDescriptor descriptor2 = service.retrieveById(2);
    LightAndCIATypeDescriptor descriptor3 = service.retrieveById(3);
    LightAndCIATypeDescriptor descriptor4 = service.retrieveById(4);
    LightAndCIATypeDescriptor descriptor5 = service.retrieveById(5);
    LightAndCIATypeDescriptor descriptor6 = service.retrieveById(6);
    LightAndCIATypeDescriptor descriptor7 = service.retrieveById(7);

    assertEquals(LightAndCIATypeDescriptor.RED_LIGHT_MC, descriptor1);
    assertEquals(LightAndCIATypeDescriptor.YELLOW_LIGHT_MC_MP, descriptor2);
    assertEquals(LightAndCIATypeDescriptor.YELLOW_LIGHT_POWERZONE, descriptor3);
    assertEquals(LightAndCIATypeDescriptor.GREEN_LIGHT_MP_OG, descriptor4);
    assertEquals(LightAndCIATypeDescriptor.GREEN_LIGHT_GOOD_BETS, descriptor5);
    assertEquals(LightAndCIATypeDescriptor.GREEN_LIGHT_WINNER, descriptor6);
    assertEquals(LightAndCIATypeDescriptor.GREEN_LIGHT_POWERZONE, descriptor7);
}
}
