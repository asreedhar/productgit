package com.firstlook.service.risklevel;

import biz.firstlook.commons.dealergrid.IDealerGridPreference;
import biz.firstlook.transact.persist.model.DealerGridPreference;

public class DealerLightData implements IDealerGridPreference
{

private DealerGridPreference dealerLightPreference;

public DealerLightData()
{
    super();
}

public DealerLightData( DealerGridPreference dlp )
{
    dealerLightPreference = dlp;
}

public int getFirstDealThreshold()
{
    return dealerLightPreference.getFirstDealThreshold();
}

public int getFirstValuationThreshold()
{
    return dealerLightPreference.getFirstValuationThreshold();
}

public int getGridCIATypeValue( int grid )
{
    return ((Integer) dealerLightPreference.getGridCIATypeValues().get(grid))
            .intValue();
}

public int getGridLightValue( int grid )
{
    return ((Integer) dealerLightPreference.getGridLightValues().get(grid))
            .intValue();
}

public int getSecondDealThreshold()
{
    return dealerLightPreference.getSecondDealThreshold();
}

public int getSecondValuationThreshold()
{
    return dealerLightPreference.getSecondValuationThreshold();
}

public int getThirdDealThreshold()
{
    return dealerLightPreference.getThirdDealThreshold();
}

public int getThirdValuationThreshold()
{
    return dealerLightPreference.getThirdValuationThreshold();
}

public int getDealerId()
{
    return dealerLightPreference.getDealerId();
}

public int getFourthDealThreshold()
{
    return dealerLightPreference.getFourthDealThreshold();
}

public int getFourthValuationThreshold()
{
    return dealerLightPreference.getFourthValuationThreshold();
}

}
