package com.firstlook.accountrep;

import java.util.Collection;

import com.firstlook.entity.DealerGroup;

public class DealersToDealerGroupHolder
{

private DealerGroup dealerGroup;
private Collection dealers;

public DealersToDealerGroupHolder()
{
    super();
}

public DealerGroup getDealerGroup()
{
    return dealerGroup;
}

public Collection getDealers()
{
    return dealers;
}

public void setDealerGroup( DealerGroup group )
{
    dealerGroup = group;
}

public void setDealers( Collection collection )
{
    dealers = collection;
}

}
