package com.firstlook.thirdparty.book.kbb;

import java.util.Collection;

import com.firstlook.entity.form.ThirdPartyVehicleOptionForm;

import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

public class KelleyWindowSticker
{

private Integer blueBookSuggestedRetailValue;
private String bottomLineDisplay;
private Integer bottomLineValue;
private String dealerName;
private String year;
private String make;
private String guideBookDescription;
private String vin;
private String color;
private String stockNumber;
private Collection<ThirdPartyVehicleOptionForm> appraisalGuideBookOptions;
private Collection<ThirdPartyVehicleOption> appraisalGuideBookEngines;
private Collection<ThirdPartyVehicleOption> appraisalGuideBookDrivetrains;
private Collection<ThirdPartyVehicleOption> appraisalGuideBookTransmissions;
private int mileage;
private String kelleyPublishInfo;
private String publishState;

public KelleyWindowSticker()
{
    super();
}

public Collection<ThirdPartyVehicleOption> getAppraisalGuideBookDrivetrains()
{
    return appraisalGuideBookDrivetrains;
}

public Collection<ThirdPartyVehicleOption> getAppraisalGuideBookEngines()
{
    return appraisalGuideBookEngines;
}

public Collection<ThirdPartyVehicleOptionForm> getAppraisalGuideBookOptions()
{
    return appraisalGuideBookOptions;
}

public Collection<ThirdPartyVehicleOption> getAppraisalGuideBookTransmissions()
{
    return appraisalGuideBookTransmissions;
}

public Integer getBlueBookSuggestedRetailValue()
{
    return blueBookSuggestedRetailValue;
}

public String getBottomLineDisplay()
{
    return bottomLineDisplay;
}

public String getDealerName()
{
    return dealerName;
}

public String getMake()
{
    return make;
}

public String getStockNumber()
{
    return stockNumber;
}

public String getYear()
{
    return year;
}

public void setAppraisalGuideBookDrivetrains( Collection<ThirdPartyVehicleOption> collection )
{
    appraisalGuideBookDrivetrains = collection;
}

public void setAppraisalGuideBookEngines( Collection<ThirdPartyVehicleOption> collection )
{
    appraisalGuideBookEngines = collection;
}

public void setAppraisalGuideBookOptions( Collection<ThirdPartyVehicleOptionForm> collection )
{
    appraisalGuideBookOptions = collection;
}

public void setAppraisalGuideBookTransmissions( Collection<ThirdPartyVehicleOption> collection )
{
    appraisalGuideBookTransmissions = collection;
}

public void setBlueBookSuggestedRetailValue( Integer integer )
{
    blueBookSuggestedRetailValue = integer;
}

public void setBottomLineDisplay( String string )
{
    bottomLineDisplay = string;
}

public void setDealerName( String string )
{
    dealerName = string;
}

public void setMake( String string )
{
    make = string;
}

public void setStockNumber( String string )
{
    stockNumber = string;
}

public void setYear( String i )
{
    year = i;
}

public String getVin()
{
    return vin;
}

public void setVin( String string )
{
    vin = string;
}

public String getColor()
{
    return color;
}

public void setColor( String string )
{
    color = string;
}

public int getMileage()
{
    return mileage;
}

public void setMileage( int i )
{
    mileage = i;
}

public Integer getBottomLineValue()
{
    return bottomLineValue;
}

public void setBottomLineValue( Integer integer )
{
    bottomLineValue = integer;
}

public String getGuideBookDescription()
{
	return guideBookDescription;
}
public void setGuideBookDescription( String guideBookDescription )
{
	this.guideBookDescription = guideBookDescription;
}

public void setKelleyPublishInfo( String kelleyPublishInfo )
{
	this.kelleyPublishInfo = kelleyPublishInfo;
}

public String getPublishMonth()
{
	if ( kelleyPublishInfo != null )
	{
		return kelleyPublishInfo.substring(4, 11);
	}
	else
	{
		return null;
	}
}

public String getPublishYear()
{
	if ( kelleyPublishInfo != null )
	{
		return kelleyPublishInfo.substring( 12 );
	}
	else
	{
		return null;
	}
}

public String getPublishState()
{
	return publishState;
	}

public void setPublishState( String kelleyPublishStateLongName )
	{
	this.publishState = kelleyPublishStateLongName;
	}
}

