
package com.firstlook.thirdparty.book.newnada;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetMileageAdjustmentResult" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getMileageAdjustmentResult"
})
@XmlRootElement(name = "GetMileageAdjustmentResponse")
public class GetMileageAdjustmentResponse {

    @XmlElement(name = "GetMileageAdjustmentResult", required = true)
    protected BigDecimal getMileageAdjustmentResult;

    /**
     * Gets the value of the getMileageAdjustmentResult property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getGetMileageAdjustmentResult() {
        return getMileageAdjustmentResult;
    }

    /**
     * Sets the value of the getMileageAdjustmentResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setGetMileageAdjustmentResult(BigDecimal value) {
        this.getMileageAdjustmentResult = value;
    }

}
