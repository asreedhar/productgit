
package com.firstlook.thirdparty.book.newnada.client;

import java.math.BigDecimal;

import javax.jws.WebService;

import com.firstlook.thirdparty.book.newnada.DoVinLookupResponse.DoVinLookupResult;
import com.firstlook.thirdparty.book.newnada.GetEquipmentOptionsNoVinResponse.GetEquipmentOptionsNoVinResult;
import com.firstlook.thirdparty.book.newnada.GetEquipmentOptionsResponse.GetEquipmentOptionsResult;
import com.firstlook.thirdparty.book.newnada.GetMakesResponse.GetMakesResult;
import com.firstlook.thirdparty.book.newnada.GetModelResponse.GetModelResult;
import com.firstlook.thirdparty.book.newnada.GetMutuallyExclusiveOptionsListResponse.GetMutuallyExclusiveOptionsListResult;
import com.firstlook.thirdparty.book.newnada.GetNADARegionsResponse.GetNADARegionsResult;
import com.firstlook.thirdparty.book.newnada.GetPackageIncludedOptionsListResponse.GetPackageIncludedOptionsListResult;
import com.firstlook.thirdparty.book.newnada.GetTrimResponse.GetTrimResult;
import com.firstlook.thirdparty.book.newnada.GetValuesNoVinResponse.GetValuesNoVinResult;
import com.firstlook.thirdparty.book.newnada.GetValuesResponse.GetValuesResult;
import com.firstlook.thirdparty.book.newnada.GetYearsResponse.GetYearsResult;

@WebService(serviceName = "NadaWebService", targetNamespace = "http://www.firstlook.biz/NadaWebService", endpointInterface = "biz.firstlook.nadawebservice.NadaWebServiceSoap")
public class NadaWebServiceImpl
    implements NadaWebServiceSoap
{
    public GetMakesResult getMakes(int year) {
        throw new UnsupportedOperationException();
    }

    public GetYearsResult getYears() {
        throw new UnsupportedOperationException();
    }

    public String getNADAPublishPeriod() {
        throw new UnsupportedOperationException();
    }

    public GetValuesNoVinResult getValues_NoVin(int trimId, int regionId, int mileage) {
        throw new UnsupportedOperationException();
    }

    public GetModelResult getModel(int year, int makeId) {
        throw new UnsupportedOperationException();
    }

    public GetEquipmentOptionsResult getEquipmentOptions(int regionId, int uid, String vin) {
        throw new UnsupportedOperationException();
    }

    public GetTrimResult getTrim(int year, int makeId, int modelId) {
        throw new UnsupportedOperationException();
    }

    public GetNADARegionsResult getNADARegions() {
        throw new UnsupportedOperationException();
    }

    public GetEquipmentOptionsNoVinResult getEquipmentOptions_NoVin(int trimId, int regionId) {
        throw new UnsupportedOperationException();
    }

    public DoVinLookupResult doVinLookup(String vin) {
        throw new UnsupportedOperationException();
    }

    public GetValuesResult getValues(int uid, int mileage, int regionId) {
        throw new UnsupportedOperationException();
    }

	public BigDecimal getMileageAdjustment(int uid, int mileage, int regionId) {
		 throw new UnsupportedOperationException();
	}

	public GetMutuallyExclusiveOptionsListResult getMutuallyExclusiveOptionsList(int uid) {
		throw new UnsupportedOperationException();
	}

	public GetPackageIncludedOptionsListResult getPackageIncludedOptionsList(int uid) {
		throw new UnsupportedOperationException();
	}

	
}
