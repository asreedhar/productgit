package com.firstlook.thirdparty.book.nada;

import com.firstlook.thirdparty.book.newnada.data.VehicleUIData.Accessories;

public class NADAOptionConflictException extends NADAException {

	private static final long serialVersionUID = -7301309255740474815L;
	
	private String message;
	private String selectedOptionDescription;
	private String conflictingOptionDescription;
	
	public NADAOptionConflictException(Accessories selected, Accessories conflictsWith) {
		super();
		
		StringBuilder errorMsgBdr = new StringBuilder();
		errorMsgBdr.append("Selected option '").append(selected.getAccCodeDescr()).append("'");
		errorMsgBdr.append("conflicts with another selected option: ");
		if(conflictsWith != null) {
			errorMsgBdr.append("'").append(conflictsWith.getAccCodeDescr()).append("'");
		} else {
			errorMsgBdr.append("ERROR");
		}
		this.message = errorMsgBdr.toString();
		this.selectedOptionDescription = selected.getAccCodeDescr();
		this.conflictingOptionDescription = conflictsWith.getAccCodeDescr();
	}
	
	@Override
	public String getMessage() {
		return message;
	}

	public String getSelectedOptionDescription() {
		return selectedOptionDescription;
	}

	public String getConflictingOptionDescription() {
		return conflictingOptionDescription;
	}
}
