package com.firstlook.thirdparty.book.nada;

import biz.firstlook.module.vehicle.description.old.GBException;

public class NADAException extends GBException
{

private static final long serialVersionUID = -6321648444447266543L;

public NADAException()
{
    super();
}

public NADAException( String arg0 )
{
    super(arg0);
}

public NADAException( Throwable arg0 )
{
    super(arg0);
}

public NADAException( String arg0, Throwable arg1 )
{
    super(arg0, arg1);
}

}
