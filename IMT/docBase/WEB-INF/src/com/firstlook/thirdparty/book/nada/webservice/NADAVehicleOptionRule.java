/**
 * NADAVehicleOptionRule.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.firstlook.thirdparty.book.nada.webservice;

public class NADAVehicleOptionRule  implements java.io.Serializable {
    private static final long serialVersionUID = -4454680585120854910L;
	private java.lang.String accessoryCodeKey;
    private java.lang.String accessoryCodeValue;

    public NADAVehicleOptionRule() {
    }

    public NADAVehicleOptionRule(
           java.lang.String accessoryCodeKey,
           java.lang.String accessoryCodeValue) {
           this.accessoryCodeKey = accessoryCodeKey;
           this.accessoryCodeValue = accessoryCodeValue;
    }


    /**
     * Gets the accessoryCodeKey value for this NADAVehicleOptionRule.
     * 
     * @return accessoryCodeKey
     */
    public java.lang.String getAccessoryCodeKey() {
        return accessoryCodeKey;
    }


    /**
     * Sets the accessoryCodeKey value for this NADAVehicleOptionRule.
     * 
     * @param accessoryCodeKey
     */
    public void setAccessoryCodeKey(java.lang.String accessoryCodeKey) {
        this.accessoryCodeKey = accessoryCodeKey;
    }


    /**
     * Gets the accessoryCodeValue value for this NADAVehicleOptionRule.
     * 
     * @return accessoryCodeValue
     */
    public java.lang.String getAccessoryCodeValue() {
        return accessoryCodeValue;
    }


    /**
     * Sets the accessoryCodeValue value for this NADAVehicleOptionRule.
     * 
     * @param accessoryCodeValue
     */
    public void setAccessoryCodeValue(java.lang.String accessoryCodeValue) {
        this.accessoryCodeValue = accessoryCodeValue;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof NADAVehicleOptionRule)) return false;
        NADAVehicleOptionRule other = (NADAVehicleOptionRule) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.accessoryCodeKey==null && other.getAccessoryCodeKey()==null) || 
             (this.accessoryCodeKey!=null &&
              this.accessoryCodeKey.equals(other.getAccessoryCodeKey()))) &&
            ((this.accessoryCodeValue==null && other.getAccessoryCodeValue()==null) || 
             (this.accessoryCodeValue!=null &&
              this.accessoryCodeValue.equals(other.getAccessoryCodeValue())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAccessoryCodeKey() != null) {
            _hashCode += getAccessoryCodeKey().hashCode();
        }
        if (getAccessoryCodeValue() != null) {
            _hashCode += getAccessoryCodeValue().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(NADAVehicleOptionRule.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "NADAVehicleOptionRule"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accessoryCodeKey");
        elemField.setXmlName(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "accessoryCodeKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accessoryCodeValue");
        elemField.setXmlName(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "accessoryCodeValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
