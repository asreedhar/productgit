package com.firstlook.thirdparty.book.nada;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import biz.firstlook.module.bookout.AbstractBookoutService;
import biz.firstlook.module.vehicle.description.old.BookOutDatasetInfo;
import biz.firstlook.module.vehicle.description.old.GBException;
import biz.firstlook.module.vehicle.description.old.GuideBookInput;
import biz.firstlook.module.vehicle.description.old.GuideBookMetaInfo;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.model.BookOutValueType;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyOption;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOptionValue;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOptionValueType;

import com.firstlook.thirdparty.book.nada.AccessoryAdjustmentCalculator.AccessorySelectionState;
import com.firstlook.thirdparty.book.nada.webservice.ArrayOfNADARegionStateAndCode;
import com.firstlook.thirdparty.book.nada.webservice.NADABookoutValues;
import com.firstlook.thirdparty.book.nada.webservice.NADARegionStateAndCode;
import com.firstlook.thirdparty.book.nada.webservice.NADAVehicle;
import com.firstlook.thirdparty.book.newnada.data.VehicleUIData.Accessories;
import com.firstlook.thirdparty.book.newnada.data.VehicleUIData.ExclusiveAccessories;
import com.firstlook.thirdparty.book.newnada.data.VehicleUIData.InclusiveAccessories;
import com.firstlook.thirdparty.book.newnada.service.NewNadaAdapter;

public class NADABookoutService extends AbstractBookoutService
{

private NewNadaAdapter nadaAdapter;
private GuideBookMetaInfo metaInfo;
static private ArrayOfNADARegionStateAndCode regionStateCodeArray;

public NADABookoutService()
{
	super();
	nadaAdapter = null;
	regionStateCodeArray = null;
}

private void init() throws GBException
{
	try
	{
		nadaAdapter = new NewNadaAdapter();
		nadaAdapter.initializeNADAService();

		regionStateCodeArray = nadaAdapter.getNADARegionsStatesAndCodes();
		// Has to happen after the nadaAdapter is initialized
		populateMetaInfo();
	}
	catch ( NADAException e )
	{
		throw new GBException( "Error Connecting to NADA Web Service" );
	}
}

/**
 * @return a list of vdp_GuideBookVehicles
 */
public List<VDP_GuideBookVehicle> doVinLookup( String region, String vin, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	if ( nadaAdapter == null )
	{
		init();
	}

	try
	{
		int regionCode = Integer.parseInt( region );
		NADAVehicle[] nadaVehicles = nadaAdapter.doVinLookup( regionCode, vin );
		List< VDP_GuideBookVehicle > guideBookVehicles = new ArrayList< VDP_GuideBookVehicle >();

		for ( int i = 0; i < nadaVehicles.length; i++ )
		{
			NADAVehicle nadaVehicle = nadaVehicles[i];
			VDP_GuideBookVehicle guideBookVehicle = new VDP_GuideBookVehicle();

			guideBookVehicle.setMake( nadaVehicle.getMake() );
			// guideBookVehicle.setModelCode( nadaVehicle.getSeries() + " " +
			// nadaVehicle.getBody() );
			guideBookVehicle.setModel( nadaVehicle.getSeries() );
			guideBookVehicle.setTrim( nadaVehicle.getBody() );
			guideBookVehicle.setBody( nadaVehicle.getBody() );
			guideBookVehicle.setSeries( nadaVehicle.getSeries() );
			guideBookVehicle.setKey( nadaVehicle.getVehicleUID() );
			guideBookVehicle.setYear( nadaVehicle.getYear() );
			guideBookVehicle.setDescription( nadaVehicle.getSeries() + " " + nadaVehicle.getBody() );
			guideBookVehicle.setVic(nadaVehicle.getVic());
			guideBookVehicle.setMsrp(nadaVehicle.getMsrp());
			guideBookVehicle.setWeight(nadaVehicle.getCurbWeight());
			guideBookVehicles.add( guideBookVehicle );
		}
		return guideBookVehicles;
	}
	catch ( NADAException e )
	{
		throw new GBException( e.getMessage() );
	}
}

/* 
 * retrieves the vehicle makes for the given year
 */
public List<VDP_GuideBookVehicle> retrieveMakes( String year, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	if ( nadaAdapter == null )
	{
		init();
	}

	NADAVehicle[] make = null;

	try
	{
		make = nadaAdapter.loadMakes( year );
	}
	catch ( NADAException e )
	{
		throw new GBException( e.getMessage() );
	}

	//the manual bookout action does not handle exceptions gracefully
	//and rewriting the javascript for it is basically rewriting both Manual bookout pages.
	List< VDP_GuideBookVehicle > makes = new ArrayList< VDP_GuideBookVehicle >();
	if (make != null) {
		for ( int i = 0; i < make.length; i++ )
		{
			makes.add( convertNADAVehicleToVDP_GuidebookVehicleForManualBookout( make[i] ) );
		}
	}
	return makes;
}

/* 
 * retrieves the vehicle models for the given year and make
 */
public List<VDP_GuideBookVehicle> retrieveModels( String year, String makeId, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	if ( nadaAdapter == null )
	{
		init();
	}

	NADAVehicle[] model = null;
	int makeCD = Integer.parseInt( makeId );
	
	try
	{
		model = nadaAdapter.loadSeries( year, makeCD );
	}
	catch ( NADAException e )
	{
		throw new GBException( e.getMessage() );
	}

	List< VDP_GuideBookVehicle > models = new ArrayList< VDP_GuideBookVehicle >();
	for ( int i = 0; i < model.length; i++ )
	{
		models.add( convertNADAVehicleToVDP_GuidebookVehicleForManualBookout( model[i] ) );
	}
	return models;
}

/* 
 * retrieves the vehicle trims for the given year, make and model
 */
public List<VDP_GuideBookVehicle> retrieveTrims( String year, String makeId, String modelId, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	if ( nadaAdapter == null )
	{
		init();
	}

	NADAVehicle[] trim = null;

	int makeCD = Integer.parseInt( makeId );
	int modelCD = Integer.parseInt( modelId );
	
	try
	{
		trim = nadaAdapter.loadBodies( year, makeCD, modelCD );
	}
	catch ( NADAException e )
	{
		throw new GBException( e.getMessage() );
	}
	ArrayList< VDP_GuideBookVehicle > trims = new ArrayList< VDP_GuideBookVehicle >();
	if (trim != null)
	{
		for ( int i = 0; i < trim.length; i++ )
		{
			trims.add( convertNADAVehicleToVDP_GuidebookVehicleForManualBookout( trim[i] ) );
		}
	}
	return trims;
}

/**
 * FIXME: TODO: XXX: need to reconcile the thirdPartyCategories/thirdPartyVehicleOptionsValues!!!
 * 
 * @return a list of ThirdPartyVehicleOption
 */
@Override
protected List<ThirdPartyVehicleOption> retrieveOptionsInternal( GuideBookInput input, VDP_GuideBookVehicle guideBookVehicle, BookOutDatasetInfo bookoutInfo) throws GBException
{
	if ( nadaAdapter == null )
	{
		init();
	}
	List< ThirdPartyVehicleOption > options = new ArrayList< ThirdPartyVehicleOption >();
	try
	{
		List<Accessories> accessories = nadaAdapter.getNADAVehicleOptions( input.getNadaRegionCode(), input.getVin(),
																				guideBookVehicle.getKey() );
		if ( accessories != null ) {
			for ( int i = 0; i < accessories.size(); i++ ) {
				options.add( getThirdPartyVehicleOptions( accessories.get(i), i ) );
			}
		}
	}
	catch ( NADAException e )
	{
		throw new GBException( e );
	}

	return options;
}

@Override
protected List<ThirdPartyVehicleOption> retrieveOptionsInternal( String regionCode, String year, String makeId, String modelId, String trimId, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	if ( nadaAdapter == null )
	{
		init();
	}
	List< ThirdPartyVehicleOption > options = new ArrayList< ThirdPartyVehicleOption >();
	int makeCD = Integer.parseInt( makeId );
	int modelCD = Integer.parseInt( modelId );
	int trimCD = Integer.parseInt( trimId );
	
	try
	{
		List<Accessories> accessories = nadaAdapter.getNADAVehicleOptions( Integer.parseInt(regionCode), year, makeCD, modelCD, trimCD );
		if ( accessories != null ) {
			for ( int i = 0; i < accessories.size(); i++ ) {
				options.add( getThirdPartyVehicleOptions( accessories.get(i), i ) );
			}
		}
	}
	catch ( NADAException e )
	{
		throw new GBException( e );
	}
	
	return options;
}

/*
 * Extraced from retrieve options.  Sets the third party options based on the given nadaVehicleOptions
 */
public ThirdPartyVehicleOption getThirdPartyVehicleOptions( Accessories accessory, int count )
{
	ThirdPartyOption thirdPartyOption = new ThirdPartyOption(
			accessory.getAccCodeDescr(),
			accessory.getAccCode(),
			ThirdPartyOptionType.EQUIPMENT);

	ThirdPartyVehicleOption thirdPartyVehicleOption = new ThirdPartyVehicleOption(
			thirdPartyOption,
			accessory.isAccIsIncluded(),
			count);
	thirdPartyVehicleOption.setStatus( accessory.isAccIsAdded() ); //new NADA schema

	// Display value
	ThirdPartyVehicleOptionValue thirdPartyVehicleOptionValueDisplay = new ThirdPartyVehicleOptionValue(
		thirdPartyVehicleOption,
		ThirdPartyVehicleOptionValueType.NA_OPTION_VALUE_TYPE,
		accessory.getAccLoan());
	
	// Loan
	ThirdPartyVehicleOptionValue thirdPartyVehicleOptionValueLoan = new ThirdPartyVehicleOptionValue( 
		thirdPartyVehicleOption, 
		ThirdPartyVehicleOptionValueType.LOAN_OPTION_VALUE_TYPE, 
		accessory.getAccLoan());
	
	// Trade-In
	ThirdPartyVehicleOptionValue thirdPartyVehicleOptionValueTradeIn = new ThirdPartyVehicleOptionValue(
		thirdPartyVehicleOption,
		ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE,
		accessory.getAccTradeIn());

	// Retail
	ThirdPartyVehicleOptionValue thirdPartyVehicleOptionValueRetail = new ThirdPartyVehicleOptionValue(
		thirdPartyVehicleOption,
		ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE,
		accessory.getAccRetail());
	
	thirdPartyVehicleOption.addOptionValue( thirdPartyVehicleOptionValueLoan );
	thirdPartyVehicleOption.addOptionValue( thirdPartyVehicleOptionValueTradeIn );
	thirdPartyVehicleOption.addOptionValue( thirdPartyVehicleOptionValueRetail );
	thirdPartyVehicleOption.addOptionValue( thirdPartyVehicleOptionValueDisplay );
	

	return thirdPartyVehicleOption;
}


private VDP_GuideBookVehicle convertNADAVehicleToVDP_GuidebookVehicleForManualBookout( NADAVehicle nadaVehicle )
{
	VDP_GuideBookVehicle vehicle = new VDP_GuideBookVehicle();
	//used for make
	vehicle.setMake( nadaVehicle.getMake() );
	vehicle.setMakeId(nadaVehicle.getMakeId() + "");
	
	// used for model
	vehicle.setModel( nadaVehicle.getSeries() );
	vehicle.setModelId( nadaVehicle.getSeriesId() + "");
	
	// used for trim 
	vehicle.setTrim( nadaVehicle.getBody() );
	vehicle.setKey( nadaVehicle.getBodyId() + "" );
	
	vehicle.setVic(nadaVehicle.getVic());
	
	vehicle.setMsrp(nadaVehicle.getMsrp());
	vehicle.setWeight(nadaVehicle.getCurbWeight());
	
	return vehicle;
}

/**
 * @return a list of VDP_GuideBookValue(s). We only use final values for NADA. Loan, Trade-in, and Retail.
 */
public List<VDP_GuideBookValue> retrieveBookValues( String regionCode, int mileage, List<ThirdPartyVehicleOption> thirdPartyVehicleOptions, 
								VDP_GuideBookVehicle vehicle, String vin, BookOutDatasetInfo bookoutInfo ) throws GBException
{
	if ( nadaAdapter == null )
	{
		init();
	}
	List<VDP_GuideBookValue> guideBookValues = new ArrayList<VDP_GuideBookValue>();
	NADABookoutValues nadaBookoutValues = null;
	List<Accessories> referenceOptions = null;
	List<InclusiveAccessories> referencePackageIncludedOptions = null;
	List<ExclusiveAccessories> referenceMutuallyExclusiveOptions = null;
	int uid = Integer.parseInt(vehicle.getKey());
	try
	{
		int nadaRegionCode = Integer.parseInt( regionCode );
		nadaBookoutValues = nadaAdapter.getNADABaseBookoutValues( vehicle.getKey(), nadaRegionCode, mileage );
		referenceOptions = nadaAdapter.getNADAVehicleOptions(nadaRegionCode, vehicle.getVin(), vehicle.getKey());
		referencePackageIncludedOptions = nadaAdapter.getPackagedIncludedOptions(uid);
		referenceMutuallyExclusiveOptions = nadaAdapter.getMutuallyExclusiveOptions(uid);
		guideBookValues = getAdjustedValues( uid, nadaBookoutValues, thirdPartyVehicleOptions, referenceOptions, referencePackageIncludedOptions, referenceMutuallyExclusiveOptions );
	}
	catch ( NADAException e )
	{
		throw new GBException( e );
	}

	return guideBookValues;
}

/**
 * @return a list of VDP_GuideBookValue(s). We only use final values for NADA. Loan, Trade-in, and Retail.
 * Uses the year, make, model, trim, and mileage to get values.
 */
public List<VDP_GuideBookValue> retrieveBookValues( String regionCode, int mileage, List<ThirdPartyVehicleOption> thirdPartyVehicleOptions
								, String year, String makeId, String modelId, String trimId, BookOutDatasetInfo bookoutInfo )
		throws GBException
{
	if ( nadaAdapter == null )
	{
		init();
	}
	List<VDP_GuideBookValue> guideBookValues = new ArrayList<VDP_GuideBookValue>();
	NADABookoutValues nadaBookoutValues = null;
	List<Accessories> referenceOptions = null;
	List<InclusiveAccessories> referencePackageIncludedOptions = null;
	List<ExclusiveAccessories> referenceMutuallyExclusiveOptions = null;
	int makeCD = Integer.parseInt( makeId );
	int modelCD = Integer.parseInt( modelId );
	int trimCD = Integer.parseInt( trimId );
	try
	{
		final int regionId = Integer.parseInt(regionCode);
		nadaBookoutValues = nadaAdapter.getNADABaseBookoutValues( regionId, year, makeCD, modelCD, trimCD, mileage );
		referenceOptions = nadaAdapter.getNADAVehicleOptions(regionId, year, makeCD, modelCD, trimCD);
		referencePackageIncludedOptions = nadaAdapter.getPackagedIncludedOptions(trimCD);
		referenceMutuallyExclusiveOptions = nadaAdapter.getMutuallyExclusiveOptions(trimCD);
		guideBookValues = getAdjustedValues( trimCD, nadaBookoutValues, thirdPartyVehicleOptions, referenceOptions, referencePackageIncludedOptions, referenceMutuallyExclusiveOptions );
	}
	catch ( NADAException e )
	{
		throw new GBException( e );
	}

	return guideBookValues;
}

/*
 * Extracted from retrieveBookValues.  Adjusts the nadaBookValues based on the selected options.
 */
private List<VDP_GuideBookValue> getAdjustedValues( int uid, 
		final NADABookoutValues nadaBookoutValues, 
		final List<ThirdPartyVehicleOption> thirdPartyVehicleOptions, 
		final List<Accessories> referenceOptions, 
		final List<InclusiveAccessories> referencePackageIncludedOptions, 
		final List<ExclusiveAccessories> referenceMutuallyExclusiveOptions ) throws NADAException
{
	List< VDP_GuideBookValue > guideBookValues = new ArrayList< VDP_GuideBookValue >();
	VDP_GuideBookValue finalBookoutValueLoan;
	VDP_GuideBookValue finalBookoutValueTradeIn;
	VDP_GuideBookValue finalBookoutValueAverageTradeIn;
	VDP_GuideBookValue finalBookoutValueRoughTradeIn;
	VDP_GuideBookValue finalBookoutValueRetail;
	Integer calculatedBookOutValueLoan;
	Integer calculatedBookOutValueTradeIn;
	Integer calculatedBookOutValueAverageTradeIn;
	Integer calculatedBookOutValueRoughTradeIn;
	Integer calculatedBookOutValueRetail;

	int valueSum = nadaBookoutValues.getBasePriceLoan() + nadaBookoutValues.getBasePriceTradeIn() + nadaBookoutValues.getBasePriceRetail();

	// 01/11/2006 - MH - Only apply the options/mileage adjustment if the base
	// values total more than zero
	List<VDP_GuideBookValue> optionAdjustments = new ArrayList<VDP_GuideBookValue>();
	if ( valueSum > 0 )
	{

		// Loop thru each of the options and get the price adjustment for each
		// of
		// the 5 categories, Load, TradeIn, Retail, RoughTradeIn, AverageTradeIn
		AccessoryAdjustmentCalculator calculator = new AccessoryAdjustmentCalculator(
				referenceOptions, referencePackageIncludedOptions, referenceMutuallyExclusiveOptions);
		
		try {
			for(ThirdPartyVehicleOption option : thirdPartyVehicleOptions) {
				if(option.getStatus()) {
					calculator.select(option.getOptionKey());
				}
			}
		} catch (NADAOptionConflictException ce) {
			guideBookValues.add(AbstractBookoutService.createOptionConflictError( ce.getSelectedOptionDescription(), ce.getConflictingOptionDescription()));
		}
		
		//Package Included options in NADA have their value included in another option's value.
		for(ThirdPartyVehicleOption option : thirdPartyVehicleOptions) {
			AccessorySelectionState selectionState = calculator.queryAccessorySelectionState(option.getOptionKey());
			Set<ThirdPartyVehicleOptionValue> values = option.getOptionValues();
			for(ThirdPartyVehicleOptionValue value : values) {
				if(AccessorySelectionState.INCLUDED.equals(selectionState)) {					
					value.zeroOutValue();
				}
			}
		}
		
		//base
		guideBookValues.add( new VDP_GuideBookValue(
				nadaBookoutValues.getRoughPriceTradeIn(), 
				ThirdPartyCategory.NADA_ROUGH_TRADE_IN_TYPE, 
				ThirdPartyCategory.NADA_ROUGH_TRADE_IN_DESCRIPTION, 
				BookOutValueType.BOOK_OUT_VALUE_BASE_VALUE));
		
		guideBookValues.add( new VDP_GuideBookValue(
				nadaBookoutValues.getAveragePriceTradeIn(), 
				ThirdPartyCategory.NADA_AVERAGE_TRADE_IN_TYPE, 
				ThirdPartyCategory.NADA_AVERAGE_TRADE_IN_DESCRIPTION, 
				BookOutValueType.BOOK_OUT_VALUE_BASE_VALUE));
		
		guideBookValues.add( new VDP_GuideBookValue(
				nadaBookoutValues.getBasePriceTradeIn(), 
				ThirdPartyCategory.NADA_CLEAN_TRADE_IN_TYPE, 
				ThirdPartyCategory.NADA_CLEAN_TRADE_IN_DESCRIPTION, 
				BookOutValueType.BOOK_OUT_VALUE_BASE_VALUE));
		
		guideBookValues.add( new VDP_GuideBookValue(
				nadaBookoutValues.getBasePriceLoan(), 
				ThirdPartyCategory.NADA_CLEAN_LOAN_TYPE, 
				ThirdPartyCategory.NADA_CLEAN_LOAN_DESCRIPTION, 
				BookOutValueType.BOOK_OUT_VALUE_BASE_VALUE));
		
		guideBookValues.add( new VDP_GuideBookValue(
				nadaBookoutValues.getBasePriceRetail(), 
				ThirdPartyCategory.NADA_CLEAN_RETAIL_TYPE, 
				ThirdPartyCategory.NADA_CLEAN_RETAIL_DESCRIPTION, 
				BookOutValueType.BOOK_OUT_VALUE_BASE_VALUE));
		
		//options
		AccessoryAdjustmentValue accessoryAdjustment = calculator.getValues();
		
		optionAdjustments.add( new VDP_GuideBookValue(
				accessoryAdjustment.getLoan(), 
				ThirdPartyCategory.NADA_CLEAN_LOAN_TYPE, 
				ThirdPartyCategory.NADA_CLEAN_LOAN_DESCRIPTION, 
				BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE));
		
		optionAdjustments.add( new VDP_GuideBookValue(
				accessoryAdjustment.getTradeIn(), 
				ThirdPartyCategory.NADA_CLEAN_TRADE_IN_TYPE, 
				ThirdPartyCategory.NADA_CLEAN_TRADE_IN_DESCRIPTION, 
				BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE));
		
		optionAdjustments.add( new VDP_GuideBookValue(
				accessoryAdjustment.getRetail(), 
				ThirdPartyCategory.NADA_CLEAN_RETAIL_TYPE, 
				ThirdPartyCategory.NADA_CLEAN_RETAIL_DESCRIPTION, 
				BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE));
		
		optionAdjustments.add( new VDP_GuideBookValue(
				accessoryAdjustment.getAverageTradeIn(), 
				ThirdPartyCategory.NADA_AVERAGE_TRADE_IN_TYPE, 
				ThirdPartyCategory.NADA_AVERAGE_TRADE_IN_DESCRIPTION, 
				BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE));
		
		optionAdjustments.add( new VDP_GuideBookValue(
				accessoryAdjustment.getRoughTradeIn(), 
				ThirdPartyCategory.NADA_ROUGH_TRADE_IN_TYPE, 
				ThirdPartyCategory.NADA_ROUGH_TRADE_IN_DESCRIPTION, 
				BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE));

		calculatedBookOutValueLoan = Integer.valueOf( nadaBookoutValues.getBasePriceLoan()
				+ accessoryAdjustment.getLoan() + nadaBookoutValues.getMileageAdjustment() );
		calculatedBookOutValueTradeIn = Integer.valueOf( nadaBookoutValues.getBasePriceTradeIn()
				+ accessoryAdjustment.getTradeIn() + nadaBookoutValues.getMileageAdjustment() );
		calculatedBookOutValueAverageTradeIn = Integer.valueOf( nadaBookoutValues.getAveragePriceTradeIn()
				+ accessoryAdjustment.getAverageTradeIn() + nadaBookoutValues.getMileageAdjustment() );
		calculatedBookOutValueRoughTradeIn = Integer.valueOf( nadaBookoutValues.getRoughPriceTradeIn()
				+ accessoryAdjustment.getRoughTradeIn() + nadaBookoutValues.getMileageAdjustment() );
		calculatedBookOutValueRetail = Integer.valueOf( nadaBookoutValues.getBasePriceRetail()
				+ accessoryAdjustment.getRetail() + nadaBookoutValues.getMileageAdjustment() );
	}
	else
	{
		calculatedBookOutValueLoan = Integer.valueOf( nadaBookoutValues.getBasePriceLoan() );
		calculatedBookOutValueTradeIn = Integer.valueOf( nadaBookoutValues.getBasePriceTradeIn() );
		calculatedBookOutValueAverageTradeIn = Integer.valueOf( nadaBookoutValues.getAveragePriceTradeIn() );
		calculatedBookOutValueRoughTradeIn = Integer.valueOf( nadaBookoutValues.getRoughPriceTradeIn());
		calculatedBookOutValueRetail = Integer.valueOf( nadaBookoutValues.getBasePriceRetail() );

	}

	finalBookoutValueLoan = new VDP_GuideBookValue( calculatedBookOutValueLoan,
													ThirdPartyCategory.NADA_CLEAN_LOAN_TYPE,
													ThirdPartyCategory.NADA_CLEAN_LOAN_DESCRIPTION,
													BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE );

	finalBookoutValueTradeIn = new VDP_GuideBookValue(	calculatedBookOutValueTradeIn,
														ThirdPartyCategory.NADA_CLEAN_TRADE_IN_TYPE,
														ThirdPartyCategory.NADA_CLEAN_TRADE_IN_DESCRIPTION,
														BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE );
	
	finalBookoutValueAverageTradeIn = new VDP_GuideBookValue(	calculatedBookOutValueAverageTradeIn,
			ThirdPartyCategory.NADA_AVERAGE_TRADE_IN_TYPE,
			ThirdPartyCategory.NADA_AVERAGE_TRADE_IN_DESCRIPTION,
			BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE );
	
	finalBookoutValueRoughTradeIn = new VDP_GuideBookValue(	calculatedBookOutValueRoughTradeIn,
			ThirdPartyCategory.NADA_ROUGH_TRADE_IN_TYPE,
			ThirdPartyCategory.NADA_ROUGH_TRADE_IN_DESCRIPTION,
			BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE );
	
	finalBookoutValueRetail = new VDP_GuideBookValue(	calculatedBookOutValueRetail,
														ThirdPartyCategory.NADA_CLEAN_RETAIL_TYPE,
														ThirdPartyCategory.NADA_CLEAN_RETAIL_DESCRIPTION,
														BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE );
	
	VDP_GuideBookValue finalBookoutMileageAdjustment = new VDP_GuideBookValue(	Integer.valueOf( nadaBookoutValues.getMileageAdjustment() ),
																				-1,
																				"Mileage Adjustment",
																				BookOutValueType.BOOK_OUT_MILEAGE_ADJUSTMENT );

	guideBookValues.add( finalBookoutValueRetail );
	guideBookValues.add( finalBookoutValueTradeIn );
	guideBookValues.add( finalBookoutValueAverageTradeIn );
	guideBookValues.add( finalBookoutValueRoughTradeIn );
	guideBookValues.add( finalBookoutValueLoan );
	guideBookValues.add( finalBookoutMileageAdjustment );
	guideBookValues.addAll(optionAdjustments);

	return guideBookValues;
}

private void populateMetaInfo() throws GBException
{
	if ( metaInfo == null )
		metaInfo = new GuideBookMetaInfo();

	metaInfo.init( ThirdPartyDataProvider.FOOTER_NADA, "No publishInfo", ThirdPartyDataProvider.NAME_NADA,
					ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE, ThirdPartyDataProvider.IMAGE_NAME_NADA, ThirdPartyDataProvider.BGCOLOR_NADA );
	// metaInfo.setFooter( "All NADA values are reprinted with permission of
	// N.A.D.A Official Used Car Guide &#174; Company &#169; NADASC 2000" );
	// metaInfo.setGuideBookId( ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE );
	// metaInfo.setGuideBookTitle( "NADA" );
	// metaInfo.setImageName( ThirdPartyDataProvider.IMAGE_NAME_NADA );
}

public NewNadaAdapter getNadaAdapter()
{
	return nadaAdapter;
}

public void setNadaAdapter( NewNadaAdapter nadaAdapter )
{
	this.nadaAdapter = nadaAdapter;
}

public GuideBookMetaInfo getMetaInfo( String region, BookOutDatasetInfo bookoutDataset ) throws GBException
{
	if ( nadaAdapter == null )
	{
		init();
	}

	GuideBookMetaInfo completeMetaInfo = new GuideBookMetaInfo();
	completeMetaInfo = metaInfo;
	String publishInfo;
	try
	{
		publishInfo = nadaAdapter.getPeriod();
		if ( publishInfo == null )
		{
			publishInfo = "";
		}
	}
	catch ( NADAException e )
	{
		throw new GBException( e );
	}
	completeMetaInfo.setPublishInfo( publishInfo + " " + region );
	return completeMetaInfo;
}

public void setMetaInfo( GuideBookMetaInfo metaInfo )
{
	this.metaInfo = metaInfo;
}

public String retrieveRegionDescription( GuideBookInput input ) throws GBException
{
	if ( nadaAdapter == null || regionStateCodeArray == null )
	{
		init();
	}

	try
	{
		Calendar calendar = Calendar.getInstance();
		int dayOfMonth = calendar.get( Calendar.DAY_OF_MONTH );

		// refresh the region mappings on the first of every month - DW 12/5/05
		if ( ( regionStateCodeArray == null ) || ( dayOfMonth == 1 ) )
		{
			regionStateCodeArray = nadaAdapter.getNADARegionsStatesAndCodes();
		}

		// the - 1 is becuase the behind the scenes structure is a 0 based array
		NADARegionStateAndCode stateAndCode = regionStateCodeArray.getNADARegionStateAndCode( input.getNadaRegionCode() - 1 );
		return stateAndCode.getRegionOrStateName();
	}
	catch ( NADAException e )
	{
		throw new GBException( e );
	}
}

public String getPublishInfo() throws GBException
{
	return null;
}

public List<Integer> retrieveModelYears(BookOutDatasetInfo bookoutInfo)
		throws GBException {
	if ( nadaAdapter == null ) {
		init();
	}
	
	List<Integer> modelYears = nadaAdapter.loadModelYears();
	
	return modelYears;
}

public List<VDP_GuideBookVehicle> retrieveModelsByMakeIDAndYear(String vin,
		Integer year, BookOutDatasetInfo bookoutInfo) throws GBException {
	// TODO Auto-generated method stub
	return null;
}

public List<VDP_GuideBookVehicle> retrieveTrimsWithoutYear(String makeId,
		String modelId, BookOutDatasetInfo bookoutInfo) throws GBException {
	// TODO Auto-generated method stub
	return null;
}

}
