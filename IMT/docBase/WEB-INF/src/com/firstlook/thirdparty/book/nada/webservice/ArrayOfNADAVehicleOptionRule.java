/**
 * ArrayOfNADAVehicleOptionRule.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.firstlook.thirdparty.book.nada.webservice;

public class ArrayOfNADAVehicleOptionRule  implements java.io.Serializable {
    private static final long serialVersionUID = 3367296843213613556L;
	private com.firstlook.thirdparty.book.nada.webservice.NADAVehicleOptionRule[] NADAVehicleOptionRule;

    public ArrayOfNADAVehicleOptionRule() {
    }

    public ArrayOfNADAVehicleOptionRule(
           com.firstlook.thirdparty.book.nada.webservice.NADAVehicleOptionRule[] NADAVehicleOptionRule) {
           this.NADAVehicleOptionRule = NADAVehicleOptionRule;
    }


    /**
     * Gets the NADAVehicleOptionRule value for this ArrayOfNADAVehicleOptionRule.
     * 
     * @return NADAVehicleOptionRule
     */
    public com.firstlook.thirdparty.book.nada.webservice.NADAVehicleOptionRule[] getNADAVehicleOptionRule() {
        return NADAVehicleOptionRule;
    }


    /**
     * Sets the NADAVehicleOptionRule value for this ArrayOfNADAVehicleOptionRule.
     * 
     * @param NADAVehicleOptionRule
     */
    public void setNADAVehicleOptionRule(com.firstlook.thirdparty.book.nada.webservice.NADAVehicleOptionRule[] NADAVehicleOptionRule) {
        this.NADAVehicleOptionRule = NADAVehicleOptionRule;
    }

    public com.firstlook.thirdparty.book.nada.webservice.NADAVehicleOptionRule getNADAVehicleOptionRule(int i) {
        return this.NADAVehicleOptionRule[i];
    }

    public void setNADAVehicleOptionRule(int i, com.firstlook.thirdparty.book.nada.webservice.NADAVehicleOptionRule _value) {
        this.NADAVehicleOptionRule[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ArrayOfNADAVehicleOptionRule)) return false;
        ArrayOfNADAVehicleOptionRule other = (ArrayOfNADAVehicleOptionRule) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.NADAVehicleOptionRule==null && other.getNADAVehicleOptionRule()==null) || 
             (this.NADAVehicleOptionRule!=null &&
              java.util.Arrays.equals(this.NADAVehicleOptionRule, other.getNADAVehicleOptionRule())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNADAVehicleOptionRule() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getNADAVehicleOptionRule());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getNADAVehicleOptionRule(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ArrayOfNADAVehicleOptionRule.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "ArrayOfNADAVehicleOptionRule"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NADAVehicleOptionRule");
        elemField.setXmlName(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "NADAVehicleOptionRule"));
        elemField.setXmlType(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "NADAVehicleOptionRule"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
