package com.firstlook.thirdparty.book.newnada.client;

import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.codehaus.xfire.MessageContext;
import org.codehaus.xfire.util.STAXUtils;
import org.codehaus.xfire.util.dom.DOMInHandler;
import org.codehaus.xfire.util.stax.W3CDOMStreamReader;
import org.w3c.dom.Document;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

public class NADADirectResponseHandler extends DOMInHandler {
	private static Logger log = Logger.getLogger(NADADirectResponseHandler.class);
	@Override
	public void invoke(MessageContext context) throws Exception {			
		Document doc = null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setValidating(false);
        dbf.setIgnoringComments(false);
        dbf.setIgnoringElementContentWhitespace(false);
        dbf.setNamespaceAware(true);
        dbf.setCoalescing(false);
        
        doc = STAXUtils.read(dbf.newDocumentBuilder(), context.getInMessage().getXMLStreamReader(), false);
        
        StringWriter sw = new StringWriter();
        XMLSerializer ser = new XMLSerializer(sw, new OutputFormat(doc));
        ser.serialize(doc.getDocumentElement());
        
        if(log.isDebugEnabled()) {
	        String response = sw.toString().replaceFirst("\n", "");
	        log.debug("NADA Web Service response: " + response);
        }
        
        context.getInMessage().setProperty(DOM_MESSAGE, doc);
        context.getInMessage().setXMLStreamReader(new W3CDOMStreamReader(doc.getDocumentElement()));
       
	}
	
}