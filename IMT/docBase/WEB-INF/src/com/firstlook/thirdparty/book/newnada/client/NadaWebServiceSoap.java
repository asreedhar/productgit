
package com.firstlook.thirdparty.book.newnada.client;

import java.math.BigDecimal;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import com.firstlook.thirdparty.book.newnada.DoVinLookupResponse.DoVinLookupResult;
import com.firstlook.thirdparty.book.newnada.GetEquipmentOptionsNoVinResponse.GetEquipmentOptionsNoVinResult;
import com.firstlook.thirdparty.book.newnada.GetEquipmentOptionsResponse.GetEquipmentOptionsResult;
import com.firstlook.thirdparty.book.newnada.GetMakesResponse.GetMakesResult;
import com.firstlook.thirdparty.book.newnada.GetModelResponse.GetModelResult;
import com.firstlook.thirdparty.book.newnada.GetMutuallyExclusiveOptionsListResponse.GetMutuallyExclusiveOptionsListResult;
import com.firstlook.thirdparty.book.newnada.GetNADARegionsResponse.GetNADARegionsResult;
import com.firstlook.thirdparty.book.newnada.GetPackageIncludedOptionsListResponse.GetPackageIncludedOptionsListResult;
import com.firstlook.thirdparty.book.newnada.GetTrimResponse.GetTrimResult;
import com.firstlook.thirdparty.book.newnada.GetValuesNoVinResponse.GetValuesNoVinResult;
import com.firstlook.thirdparty.book.newnada.GetValuesResponse.GetValuesResult;
import com.firstlook.thirdparty.book.newnada.GetYearsResponse.GetYearsResult;

@WebService(name = "NadaWebServiceSoap", targetNamespace = "http://www.firstlook.biz/NadaWebService")
@SOAPBinding(use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public interface NadaWebServiceSoap {


    @WebMethod(operationName = "GetMakes", action = "http://www.firstlook.biz/NadaWebService/GetMakes")
    @WebResult(name = "GetMakesResult", targetNamespace = "http://www.firstlook.biz/NadaWebService")
    public GetMakesResult getMakes(
        @WebParam(name = "year", targetNamespace = "http://www.firstlook.biz/NadaWebService")
        int year);

    @WebMethod(operationName = "GetYears", action = "http://www.firstlook.biz/NadaWebService/GetYears")
    @WebResult(name = "GetYearsResult", targetNamespace = "http://www.firstlook.biz/NadaWebService")
    public GetYearsResult getYears();

    @WebMethod(operationName = "GetNADAPublishPeriod", action = "http://www.firstlook.biz/NadaWebService/GetNADAPublishPeriod")
    @WebResult(name = "GetNADAPublishPeriodResult", targetNamespace = "http://www.firstlook.biz/NadaWebService")
    public String getNADAPublishPeriod();

    @WebMethod(operationName = "GetValues_NoVin", action = "http://www.firstlook.biz/NadaWebService/GetValues_NoVin")
    @WebResult(name = "GetValues_NoVinResult", targetNamespace = "http://www.firstlook.biz/NadaWebService")
    public GetValuesNoVinResult getValues_NoVin(
        @WebParam(name = "trimId", targetNamespace = "http://www.firstlook.biz/NadaWebService")
        int trimId,
        @WebParam(name = "regionId", targetNamespace = "http://www.firstlook.biz/NadaWebService")
        int regionId,
        @WebParam(name = "mileage", targetNamespace = "http://www.firstlook.biz/NadaWebService")
        int mileage);

    @WebMethod(operationName = "GetModel", action = "http://www.firstlook.biz/NadaWebService/GetModel")
    @WebResult(name = "GetModelResult", targetNamespace = "http://www.firstlook.biz/NadaWebService")
    public GetModelResult getModel(
        @WebParam(name = "year", targetNamespace = "http://www.firstlook.biz/NadaWebService")
        int year,
        @WebParam(name = "makeId", targetNamespace = "http://www.firstlook.biz/NadaWebService")
        int makeId);

    @WebMethod(operationName = "GetEquipmentOptions", action = "http://www.firstlook.biz/NadaWebService/GetEquipmentOptions")
    @WebResult(name = "GetEquipmentOptionsResult", targetNamespace = "http://www.firstlook.biz/NadaWebService")
    public GetEquipmentOptionsResult getEquipmentOptions(
        @WebParam(name = "regionId", targetNamespace = "http://www.firstlook.biz/NadaWebService")
        int regionId,
        @WebParam(name = "uid", targetNamespace = "http://www.firstlook.biz/NadaWebService")
        int uid,
        @WebParam(name = "vin", targetNamespace = "http://www.firstlook.biz/NadaWebService")
        String vin);

    @WebMethod(operationName = "GetTrim", action = "http://www.firstlook.biz/NadaWebService/GetTrim")
    @WebResult(name = "GetTrimResult", targetNamespace = "http://www.firstlook.biz/NadaWebService")
    public GetTrimResult getTrim(
        @WebParam(name = "year", targetNamespace = "http://www.firstlook.biz/NadaWebService")
        int year,
        @WebParam(name = "makeId", targetNamespace = "http://www.firstlook.biz/NadaWebService")
        int makeId,
        @WebParam(name = "modelId", targetNamespace = "http://www.firstlook.biz/NadaWebService")
        int modelId);

    @WebMethod(operationName = "GetNADARegions", action = "http://www.firstlook.biz/NadaWebService/GetNADARegions")
    @WebResult(name = "GetNADARegionsResult", targetNamespace = "http://www.firstlook.biz/NadaWebService")
    public GetNADARegionsResult getNADARegions();

    @WebMethod(operationName = "GetEquipmentOptions_NoVin", action = "http://www.firstlook.biz/NadaWebService/GetEquipmentOptions_NoVin")
    @WebResult(name = "GetEquipmentOptions_NoVinResult", targetNamespace = "http://www.firstlook.biz/NadaWebService")
    public GetEquipmentOptionsNoVinResult getEquipmentOptions_NoVin(
        @WebParam(name = "trimId", targetNamespace = "http://www.firstlook.biz/NadaWebService")
        int trimId,
        @WebParam(name = "regionId", targetNamespace = "http://www.firstlook.biz/NadaWebService")
        int regionId);

    @WebMethod(operationName = "DoVinLookup", action = "http://www.firstlook.biz/NadaWebService/DoVinLookup")
    @WebResult(name = "DoVinLookupResult", targetNamespace = "http://www.firstlook.biz/NadaWebService")
    public DoVinLookupResult doVinLookup(
        @WebParam(name = "vin", targetNamespace = "http://www.firstlook.biz/NadaWebService")
        String vin);

    @WebMethod(operationName = "GetValues", action = "http://www.firstlook.biz/NadaWebService/GetValues")
    @WebResult(name = "GetValuesResult", targetNamespace = "http://www.firstlook.biz/NadaWebService")
    public GetValuesResult getValues(
        @WebParam(name = "uid", targetNamespace = "http://www.firstlook.biz/NadaWebService")
        int uid,
        @WebParam(name = "mileage", targetNamespace = "http://www.firstlook.biz/NadaWebService")
        int mileage,
        @WebParam(name = "regionId", targetNamespace = "http://www.firstlook.biz/NadaWebService")
        int regionId);

    @WebMethod(operationName = "GetMileageAdjustment", action = "http://www.firstlook.biz/NadaWebService/GetMileageAdjustment")
    @WebResult(name = "GetMileageAdjustmentResult", targetNamespace = "http://www.firstlook.biz/NadaWebService")
    public BigDecimal getMileageAdjustment(
    	@WebParam(name = "uid", targetNamespace = "http://www.firstlook.biz/NadaWebService")
    	int uid,
    	@WebParam(name = "mileage", targetNamespace = "http://www.firstlook.biz/NadaWebService")
    	int mileage,
    	@WebParam(name = "regionId", targetNamespace = "http://www.firstlook.biz/NadaWebService")
    	int regionId);
    
    @WebMethod(operationName = "GetMutuallyExclusiveOptionsList", action="http://www.firstlook.biz/NadaWebService/GetMutuallyExclusiveOptionsList")
    @WebResult(name = "GetMutuallyExclusiveOptionsListResult", targetNamespace = "http://www.firstlook.biz/NadaWebService")
    public GetMutuallyExclusiveOptionsListResult getMutuallyExclusiveOptionsList(
    		@WebParam(name = "uid", targetNamespace = "http://www.firstlook.biz/NadaWebService")
    		int uid);
    
    @WebMethod(operationName = "GetPackageIncludedOptionsList", action="http://www.firstlook.biz/NadaWebService/GetPackageIncludedOptionsList")
    @WebResult(name = "GetPackageIncludedOptionsListResult", targetNamespace = "http://www.firstlook.biz/NadaWebService")
    public GetPackageIncludedOptionsListResult getPackageIncludedOptionsList(
    		@WebParam(name = "uid", targetNamespace = "http://www.firstlook.biz/NadaWebService")
    		int uid);
}
