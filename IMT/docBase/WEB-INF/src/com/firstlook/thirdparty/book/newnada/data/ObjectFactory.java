
package com.firstlook.thirdparty.book.newnada.data;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the biz.firstlook.nadawebservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: biz.firstlook.nadawebservice
     * 
     */
    public ObjectFactory() {
    }
  
    /**
     * Create an instance of {@link VehicleUIData.Vehicles }
     * 
     */
    public VehicleUIData.Vehicles createVehicleUIDataVehicles() {
        return new VehicleUIData.Vehicles();
    }

  /*
     * Create an instance of {@link VehicleUIData.Years }
     * 
     */
    public VehicleUIData.Years createVehicleUIDataYears() {
        return new VehicleUIData.Years();
    }

  

    /**
     * Create an instance of {@link VehicleUIData.VehicleValues }
     * 
     */
    public VehicleUIData.VehicleValues createVehicleUIDataVehicleValues() {
        return new VehicleUIData.VehicleValues();
    }

    /**
     * Create an instance of {@link VehicleUIData }
     * 
     */
  //  @XmlElementDecl(namespace = "NADAVehicle_Data", name = "VehicleUI_Data") 
    public VehicleUIData createVehicleUIData() {
        return new VehicleUIData();
    }

    /**
     * Create an instance of {@link VehicleUIData.InclusiveAccessories }
     * 
     */
    public VehicleUIData.InclusiveAccessories createVehicleUIDataInclusiveAccessories() {
        return new VehicleUIData.InclusiveAccessories();
    }

   
    /**
     * Create an instance of {@link VehicleUIData.Bodies }
     * 
     */
    public VehicleUIData.Bodies createVehicleUIDataBodies() {
        return new VehicleUIData.Bodies();
    }

    /**
     * Create an instance of {@link VehicleUIData.RegionStates }
     * 
     */
    public VehicleUIData.RegionStates createVehicleUIDataRegionStates() {
        return new VehicleUIData.RegionStates();
    }

   

    /**
     * Create an instance of {@link VehicleUIData.MileageValues }
     * 
     */
    public VehicleUIData.MileageValues createVehicleUIDataMileageValues() {
        return new VehicleUIData.MileageValues();
    }

   
    /**
     * Create an instance of {@link VehicleUIData.ExclusiveAccessories }
     * 
     */
    public VehicleUIData.ExclusiveAccessories createVehicleUIDataExclusiveAccessories() {
        return new VehicleUIData.ExclusiveAccessories();
    }

    /**
     * Create an instance of {@link VehicleUIData.Accessories }
     * 
     */
    public VehicleUIData.Accessories createVehicleUIDataAccessories() {
        return new VehicleUIData.Accessories();
    }

    /**
     * Create an instance of {@link VehicleUIData.Series }
     * 
     */
    public VehicleUIData.Series createVehicleUIDataSeries() {
        return new VehicleUIData.Series();
    }

    /**
     * Create an instance of {@link VehicleUIData.Makes }
     * 
     */
    public VehicleUIData.Makes createVehicleUIDataMakes() {
        return new VehicleUIData.Makes();
    }
}
