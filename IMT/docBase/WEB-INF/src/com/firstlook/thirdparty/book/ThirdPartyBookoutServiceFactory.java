package com.firstlook.thirdparty.book;

import biz.firstlook.module.bookout.IThirdPartyBookoutService;
import biz.firstlook.module.vehicle.description.old.GBException;
import biz.firstlook.module.vehicle.description.old.KBBBookoutService;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;

import com.firstlook.thirdparty.book.blackbook.BlackBookBookoutService;
import com.firstlook.thirdparty.book.galves.GalvesService;
import com.firstlook.thirdparty.book.nada.NADABookoutService;

/**
 * Factory class with Spring instantiated services.
 * @author bfung
 *
 */
public class ThirdPartyBookoutServiceFactory
{

private NADABookoutService nadaBookoutService;
private BlackBookBookoutService blackBookBookoutService;
private KBBBookoutService kbbBookoutService;
private GalvesService galvesService;

public ThirdPartyBookoutServiceFactory()
{
	super();
}

public IThirdPartyBookoutService retrieveThirdPartyBookoutService( Integer thirdPartyId ) throws GBException
{
	IThirdPartyBookoutService thirdPartyBookoutService = null;
	switch( thirdPartyId.intValue() )
	{
		case ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE:
			thirdPartyBookoutService = getBlackBookBookoutService();
			break;
		case ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE:
			thirdPartyBookoutService = getNadaBookoutService();
			break;
		case ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE:
			thirdPartyBookoutService = getKbbBookoutService();
			break;
		case ThirdPartyDataProvider.GUIDEBOOK_GALVES_TYPE:
			thirdPartyBookoutService = getGalvesService();
			break;
		default://some error here
			break;
	}
	return thirdPartyBookoutService;
}

public NADABookoutService getNadaBookoutService()
{
	return nadaBookoutService;
}

public void setNadaBookoutService( NADABookoutService nadaBookoutService )
{
	this.nadaBookoutService = nadaBookoutService;
}

public BlackBookBookoutService getBlackBookBookoutService()
{
	return blackBookBookoutService;
}

public void setBlackBookBookoutService( BlackBookBookoutService blackBookBookoutService )
{
	this.blackBookBookoutService = blackBookBookoutService;
}

public KBBBookoutService getKbbBookoutService()
{
	return kbbBookoutService;
}

public void setKbbBookoutService( KBBBookoutService kelleyBookoutService )
{
	this.kbbBookoutService = kelleyBookoutService;
}

public GalvesService getGalvesService()
{
	return galvesService;
}

public void setGalvesService( GalvesService galvesService )
{
	this.galvesService = galvesService;
}

}
