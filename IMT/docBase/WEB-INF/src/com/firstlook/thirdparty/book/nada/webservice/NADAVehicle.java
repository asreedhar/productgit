/**
 * NADAVehicle.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.firstlook.thirdparty.book.nada.webservice;

public class NADAVehicle  implements java.io.Serializable {
    private static final long serialVersionUID = 3040287858162992600L;
	private String vic;
    private String vehicleUID;
    private String make;
    private String year;
    private String series;
    private String body;
    private int makeId;
    private int seriesId;
    private int bodyId;
    
    private String mileageId;
    private Integer curbWeight;
    private Integer gvw;
    private Integer gcw;
    private Integer msrp;

    public NADAVehicle() {
    }

    public NADAVehicle(
    		String vic, String vehicleUID, String make, String year,
    		String series, String body, int makeId, int seriesId, 
    		int bodyId, String mileageId, Integer curbWeight, Integer gvw, 
    		Integer gcw, Integer msrp) {
           this.vic = vic;
           this.vehicleUID = vehicleUID;
           this.make = make;
           this.year = year;
           this.series = series;
           this.body = body;
           this.makeId = makeId;
           this.seriesId = seriesId;
           this.bodyId = bodyId;
           
           this.mileageId = mileageId;
           this.curbWeight = curbWeight;
           this.gvw = gvw;
           this.gcw = gcw;
           this.msrp = msrp;
    }


    /**
     * Gets the vic value for this NADAVehicle.
     * 
     * @return vic
     */
    public String getVic() {
        return vic;
    }


    /**
     * Sets the vic value for this NADAVehicle.
     * 
     * @param vic
     */
    public void setVic(String vic) {
        this.vic = vic;
    }


    /**
     * Gets the vehicleUID value for this NADAVehicle.
     * 
     * @return vehicleUID
     */
    public String getVehicleUID() {
        return vehicleUID;
    }


    /**
     * Sets the vehicleUID value for this NADAVehicle.
     * 
     * @param vehicleUID
     */
    public void setVehicleUID(String vehicleUID) {
        this.vehicleUID = vehicleUID;
    }


    /**
     * Gets the make value for this NADAVehicle.
     * 
     * @return make
     */
    public String getMake() {
        return make;
    }


    /**
     * Sets the make value for this NADAVehicle.
     * 
     * @param make
     */
    public void setMake(String make) {
        this.make = make;
    }


    /**
     * Gets the year value for this NADAVehicle.
     * 
     * @return year
     */
    public String getYear() {
        return year;
    }


    /**
     * Sets the year value for this NADAVehicle.
     * 
     * @param year
     */
    public void setYear(String year) {
        this.year = year;
    }


    /**
     * Gets the series value for this NADAVehicle.
     * 
     * @return series
     */
    public String getSeries() {
        return series;
    }


    /**
     * Sets the series value for this NADAVehicle.
     * 
     * @param series
     */
    public void setSeries(String series) {
        this.series = series;
    }


    /**
     * Gets the body value for this NADAVehicle.
     * 
     * @return body
     */
    public String getBody() {
        return body;
    }


    /**
     * Sets the body value for this NADAVehicle.
     * 
     * @param body
     */
    public void setBody(String body) {
        this.body = body;
    }


    /**
     * Gets the makeId value for this NADAVehicle.
     * 
     * @return makeId
     */
    public int getMakeId() {
        return makeId;
    }


    /**
     * Sets the makeId value for this NADAVehicle.
     * 
     * @param makeId
     */
    public void setMakeId(int makeId) {
        this.makeId = makeId;
    }


    /**
     * Gets the seriesId value for this NADAVehicle.
     * 
     * @return seriesId
     */
    public int getSeriesId() {
        return seriesId;
    }


    /**
     * Sets the seriesId value for this NADAVehicle.
     * 
     * @param seriesId
     */
    public void setSeriesId(int seriesId) {
        this.seriesId = seriesId;
    }


    /**
     * Gets the bodyId value for this NADAVehicle.
     * 
     * @return bodyId
     */
    public int getBodyId() {
        return bodyId;
    }


    /**
     * Sets the bodyId value for this NADAVehicle.
     * 
     * @param bodyId
     */
    public void setBodyId(int bodyId) {
        this.bodyId = bodyId;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof NADAVehicle)) return false;
        NADAVehicle other = (NADAVehicle) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.vic==null && other.getVic()==null) || 
             (this.vic!=null &&
              this.vic.equals(other.getVic()))) &&
            ((this.vehicleUID==null && other.getVehicleUID()==null) || 
             (this.vehicleUID!=null &&
              this.vehicleUID.equals(other.getVehicleUID()))) &&
            ((this.make==null && other.getMake()==null) || 
             (this.make!=null &&
              this.make.equals(other.getMake()))) &&
            ((this.year==null && other.getYear()==null) || 
             (this.year!=null &&
              this.year.equals(other.getYear()))) &&
            ((this.series==null && other.getSeries()==null) || 
             (this.series!=null &&
              this.series.equals(other.getSeries()))) &&
            ((this.body==null && other.getBody()==null) || 
             (this.body!=null &&
              this.body.equals(other.getBody()))) &&
            this.makeId == other.getMakeId() &&
            this.seriesId == other.getSeriesId() &&
            this.bodyId == other.getBodyId();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getVic() != null) {
            _hashCode += getVic().hashCode();
        }
        if (getVehicleUID() != null) {
            _hashCode += getVehicleUID().hashCode();
        }
        if (getMake() != null) {
            _hashCode += getMake().hashCode();
        }
        if (getYear() != null) {
            _hashCode += getYear().hashCode();
        }
        if (getSeries() != null) {
            _hashCode += getSeries().hashCode();
        }
        if (getBody() != null) {
            _hashCode += getBody().hashCode();
        }
        _hashCode += getMakeId();
        _hashCode += getSeriesId();
        _hashCode += getBodyId();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(NADAVehicle.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "NADAVehicle"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vic");
        elemField.setXmlName(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "vic"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vehicleUID");
        elemField.setXmlName(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "vehicleUID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("make");
        elemField.setXmlName(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "make"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("year");
        elemField.setXmlName(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "year"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("series");
        elemField.setXmlName(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "series"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("body");
        elemField.setXmlName(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "body"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("makeId");
        elemField.setXmlName(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "makeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seriesId");
        elemField.setXmlName(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "seriesId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bodyId");
        elemField.setXmlName(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "bodyId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType, 
           Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType, 
           Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

	public String getMileageId() {
		return mileageId;
	}

	public void setMileageId(String mileageId) {
		this.mileageId = mileageId;
	}

	public Integer getCurbWeight() {
		return curbWeight;
	}

	public void setCurbWeight(Integer curbWeight) {
		this.curbWeight = curbWeight;
	}

	public Integer getGvw() {
		return gvw;
	}

	public void setGvw(Integer gvw) {
		this.gvw = gvw;
	}

	public Integer getGcw() {
		return gcw;
	}

	public void setGcw(Integer gcw) {
		this.gcw = gcw;
	}

	public Integer getMsrp() {
		return msrp;
	}

	public void setMsrp(Integer msrp) {
		this.msrp = msrp;
	}

}
