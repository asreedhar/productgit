
package com.firstlook.thirdparty.book.newnada;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.firstlook.thirdparty.book.newnada.data.VehicleUIData;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetEquipmentOptionsResult" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="VehicleUI_Data" type="{http://www.firstlook.biz/NadaWebService}VehicleUI_Data" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getEquipmentOptionsResult"
})
@XmlRootElement(name = "GetEquipmentOptionsResponse")
public class GetEquipmentOptionsResponse {

    @XmlElement(name = "GetEquipmentOptionsResult")
    protected GetEquipmentOptionsResponse.GetEquipmentOptionsResult getEquipmentOptionsResult;

    /**
     * Gets the value of the getEquipmentOptionsResult property.
     * 
     * @return
     *     possible object is
     *     {@link GetEquipmentOptionsResponse.GetEquipmentOptionsResult }
     *     
     */
    public GetEquipmentOptionsResponse.GetEquipmentOptionsResult getGetEquipmentOptionsResult() {
        return getEquipmentOptionsResult;
    }

    /**
     * Sets the value of the getEquipmentOptionsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetEquipmentOptionsResponse.GetEquipmentOptionsResult }
     *     
     */
    public void setGetEquipmentOptionsResult(GetEquipmentOptionsResponse.GetEquipmentOptionsResult value) {
        this.getEquipmentOptionsResult = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="VehicleUI_Data" type="{http://www.firstlook.biz/NadaWebService}VehicleUI_Data" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "vehicleUIData"
    })
    public static class GetEquipmentOptionsResult {

        @XmlElementRef(name = "VehicleUI_Data", namespace = "NADAVehicle_Data", type = VehicleUIData.class)
        protected VehicleUIData vehicleUIData;

		public VehicleUIData getVehicleUIData() {
			return vehicleUIData;
		}

		public void setVehicleUIData(VehicleUIData vehicleUIData) {
			this.vehicleUIData = vehicleUIData;
		}

    }

}

