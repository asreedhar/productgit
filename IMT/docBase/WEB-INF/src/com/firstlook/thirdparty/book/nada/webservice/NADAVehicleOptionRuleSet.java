/**
 * NADAVehicleOptionRuleSet.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.firstlook.thirdparty.book.nada.webservice;

public class NADAVehicleOptionRuleSet  implements java.io.Serializable {
    private static final long serialVersionUID = -2567521674135413937L;
	private com.firstlook.thirdparty.book.nada.webservice.ArrayOfNADAVehicleOptionRule packageIncludedAccessories;
    private com.firstlook.thirdparty.book.nada.webservice.ArrayOfNADAVehicleOptionRule mutuallyExclusiveAccessories;

    public NADAVehicleOptionRuleSet() {
    }

    public NADAVehicleOptionRuleSet(
           com.firstlook.thirdparty.book.nada.webservice.ArrayOfNADAVehicleOptionRule packageIncludedAccessories,
           com.firstlook.thirdparty.book.nada.webservice.ArrayOfNADAVehicleOptionRule mutuallyExclusiveAccessories) {
           this.packageIncludedAccessories = packageIncludedAccessories;
           this.mutuallyExclusiveAccessories = mutuallyExclusiveAccessories;
    }


    /**
     * Gets the packageIncludedAccessories value for this NADAVehicleOptionRuleSet.
     * 
     * @return packageIncludedAccessories
     */
    public com.firstlook.thirdparty.book.nada.webservice.ArrayOfNADAVehicleOptionRule getPackageIncludedAccessories() {
        return packageIncludedAccessories;
    }


    /**
     * Sets the packageIncludedAccessories value for this NADAVehicleOptionRuleSet.
     * 
     * @param packageIncludedAccessories
     */
    public void setPackageIncludedAccessories(com.firstlook.thirdparty.book.nada.webservice.ArrayOfNADAVehicleOptionRule packageIncludedAccessories) {
        this.packageIncludedAccessories = packageIncludedAccessories;
    }


    /**
     * Gets the mutuallyExclusiveAccessories value for this NADAVehicleOptionRuleSet.
     * 
     * @return mutuallyExclusiveAccessories
     */
    public com.firstlook.thirdparty.book.nada.webservice.ArrayOfNADAVehicleOptionRule getMutuallyExclusiveAccessories() {
        return mutuallyExclusiveAccessories;
    }


    /**
     * Sets the mutuallyExclusiveAccessories value for this NADAVehicleOptionRuleSet.
     * 
     * @param mutuallyExclusiveAccessories
     */
    public void setMutuallyExclusiveAccessories(com.firstlook.thirdparty.book.nada.webservice.ArrayOfNADAVehicleOptionRule mutuallyExclusiveAccessories) {
        this.mutuallyExclusiveAccessories = mutuallyExclusiveAccessories;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof NADAVehicleOptionRuleSet)) return false;
        NADAVehicleOptionRuleSet other = (NADAVehicleOptionRuleSet) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.packageIncludedAccessories==null && other.getPackageIncludedAccessories()==null) || 
             (this.packageIncludedAccessories!=null &&
              this.packageIncludedAccessories.equals(other.getPackageIncludedAccessories()))) &&
            ((this.mutuallyExclusiveAccessories==null && other.getMutuallyExclusiveAccessories()==null) || 
             (this.mutuallyExclusiveAccessories!=null &&
              this.mutuallyExclusiveAccessories.equals(other.getMutuallyExclusiveAccessories())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPackageIncludedAccessories() != null) {
            _hashCode += getPackageIncludedAccessories().hashCode();
        }
        if (getMutuallyExclusiveAccessories() != null) {
            _hashCode += getMutuallyExclusiveAccessories().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(NADAVehicleOptionRuleSet.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "NADAVehicleOptionRuleSet"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("packageIncludedAccessories");
        elemField.setXmlName(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "packageIncludedAccessories"));
        elemField.setXmlType(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "ArrayOfNADAVehicleOptionRule"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mutuallyExclusiveAccessories");
        elemField.setXmlName(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "mutuallyExclusiveAccessories"));
        elemField.setXmlType(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "ArrayOfNADAVehicleOptionRule"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
