package com.firstlook.thirdparty.book.old;

public class VDP_SelectedKeys
{
private String[] selectedEquipmentOptionKeys = new String[0];

private String selectedEngineKey;
private String selectedTransmissionKey;
private String selectedDrivetrainKey;
public String getSelectedDrivetrainKey()
{
    return selectedDrivetrainKey;
}
public void setSelectedDrivetrainKey( String selectedDrivetrainKey )
{
    this.selectedDrivetrainKey = selectedDrivetrainKey;
}
public String getSelectedEngineKey()
{
    return selectedEngineKey;
}
public void setSelectedEngineKey( String selectedEngineKey )
{
    this.selectedEngineKey = selectedEngineKey;
}
public String[] getSelectedEquipmentOptionKeys()
{
    return selectedEquipmentOptionKeys;
}
public void setSelectedEquipmentOptionKeys( String[] selectedOptionKeys )
{
    this.selectedEquipmentOptionKeys = selectedOptionKeys;
}
public String getSelectedTransmissionKey()
{
    return selectedTransmissionKey;
}
public void setSelectedTransmissionKey( String selectedTransmissionKey )
{
    this.selectedTransmissionKey = selectedTransmissionKey;
}
}
