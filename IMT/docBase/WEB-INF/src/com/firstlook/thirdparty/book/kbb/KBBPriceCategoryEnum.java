package com.firstlook.thirdparty.book.kbb;

import org.springframework.core.Ordered;

import biz.firstlook.transact.persist.model.ThirdPartyCategory;

public enum KBBPriceCategoryEnum implements Ordered
{

WHOLESALE( ThirdPartyCategory.KELLEY_WHOLESALE_TYPE, 0, ThirdPartyCategory.KELLEY_WHOLESALE_DESCRIPTION), 
RETAIL( ThirdPartyCategory.KELLEY_RETAIL_TYPE, 1, ThirdPartyCategory.KELLEY_RETAIL_DESCRIPTION),
TRADE_IN( ThirdPartyCategory.KELLEY_TRADEIN_TYPE, 2, ThirdPartyCategory.KELLEY_TRADEIN_DESCRIPTION);

private Integer id;
private int displayOrder;
private String description;

private KBBPriceCategoryEnum( Integer id, int displayOrder, String description )
{
	this.id = id;
	this.displayOrder = displayOrder;
	this.description = description;
}

public String getDescription()
{
	return description;
}

public int getOrder()
{
	return displayOrder;
}

public Integer getId()
{
	return id;
}

public static KBBPriceCategoryEnum valueOfDescription( String description )
{
	if( description == null)
		return null;
	
	if( description.equalsIgnoreCase( WHOLESALE.description ) )
		return WHOLESALE;
	else if ( description.equalsIgnoreCase( RETAIL.description ))
		return RETAIL;
	else if ( description.equalsIgnoreCase( TRADE_IN.description ))
		return TRADE_IN;
	else
		return null;
}

}