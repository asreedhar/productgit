
package com.firstlook.thirdparty.book.newnada;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.firstlook.thirdparty.book.newnada.data.VehicleUIData;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetValuesResult" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="VehicleUI_Data" type="{http://www.firstlook.biz/NadaWebService}VehicleUI_Data" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getValuesResult"
})
@XmlRootElement(name = "GetValuesResponse")
public class GetValuesResponse {

    @XmlElement(name = "GetValuesResult")
    protected GetValuesResponse.GetValuesResult getValuesResult;

    /**
     * Gets the value of the getValuesResult property.
     * 
     * @return
     *     possible object is
     *     {@link GetValuesResponse.GetValuesResult }
     *     
     */
    public GetValuesResponse.GetValuesResult getGetValuesResult() {
        return getValuesResult;
    }

    /**
     * Sets the value of the getValuesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetValuesResponse.GetValuesResult }
     *     
     */
    public void setGetValuesResult(GetValuesResponse.GetValuesResult value) {
        this.getValuesResult = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="VehicleUI_Data" type="{http://www.firstlook.biz/NadaWebService}VehicleUI_Data" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "vehicleUIData"
    })
    public static class GetValuesResult {

        @XmlElementRef(name = "VehicleUI_Data", namespace = "NADAVehicle_Data", type = VehicleUIData.class)
        protected VehicleUIData vehicleUIData;

		public VehicleUIData getVehicleUIData() {
			return vehicleUIData;
		}

		public void setVehicleUIData(VehicleUIData vehicleUIData) {
			this.vehicleUIData = vehicleUIData;
		}
    }

}