/**
 * NADABookoutValues.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.firstlook.thirdparty.book.nada.webservice;

public class NADABookoutValues  implements java.io.Serializable {
    private static final long serialVersionUID = 4296981424031564379L;
	private int mileageAdjustment;
    private int msrp;
    private int basePriceTradeIn;
    private int averagePriceTradeIn;
    private int roughPriceTradeIn;
    private int basePriceLoan;
    private int basePriceRetail;

    public NADABookoutValues() {
    }

    public NADABookoutValues(
           int mileageAdjustment,
           int msrp,
           int basePriceTradeIn,
           int basePriceLoan,
           int basePriceRetail) {
           this.mileageAdjustment = mileageAdjustment;
           this.msrp = msrp;
           this.basePriceTradeIn = basePriceTradeIn;
           this.basePriceLoan = basePriceLoan;
           this.basePriceRetail = basePriceRetail;
    }


    /**
     * Gets the mileageAdjustment value for this NADABookoutValues.
     * 
     * @return mileageAdjustment
     */
    public int getMileageAdjustment() {
        return mileageAdjustment;
    }


    /**
     * Sets the mileageAdjustment value for this NADABookoutValues.
     * 
     * @param mileageAdjustment
     */
    public void setMileageAdjustment(int mileageAdjustment) {
        this.mileageAdjustment = mileageAdjustment;
    }


    /**
     * Gets the msrp value for this NADABookoutValues.
     * 
     * @return msrp
     */
    public int getMsrp() {
        return msrp;
    }


    /**
     * Sets the msrp value for this NADABookoutValues.
     * 
     * @param msrp
     */
    public void setMsrp(int msrp) {
        this.msrp = msrp;
    }


    /**
     * Gets the basePriceTradeIn value for this NADABookoutValues.
     * 
     * @return basePriceTradeIn
     */
    public int getBasePriceTradeIn() {
        return basePriceTradeIn;
    }


    /**
     * Sets the basePriceTradeIn value for this NADABookoutValues.
     * 
     * @param basePriceTradeIn
     */
    public void setBasePriceTradeIn(int basePriceTradeIn) {
        this.basePriceTradeIn = basePriceTradeIn;
    }


    /**
     * Gets the basePriceLoan value for this NADABookoutValues.
     * 
     * @return basePriceLoan
     */
    public int getBasePriceLoan() {
        return basePriceLoan;
    }


    /**
     * Sets the basePriceLoan value for this NADABookoutValues.
     * 
     * @param basePriceLoan
     */
    public void setBasePriceLoan(int basePriceLoan) {
        this.basePriceLoan = basePriceLoan;
    }


    /**
     * Gets the basePriceRetail value for this NADABookoutValues.
     * 
     * @return basePriceRetail
     */
    public int getBasePriceRetail() {
        return basePriceRetail;
    }


    /**
     * Sets the basePriceRetail value for this NADABookoutValues.
     * 
     * @param basePriceRetail
     */
    public void setBasePriceRetail(int basePriceRetail) {
        this.basePriceRetail = basePriceRetail;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof NADABookoutValues)) return false;
        NADABookoutValues other = (NADABookoutValues) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.mileageAdjustment == other.getMileageAdjustment() &&
            this.msrp == other.getMsrp() &&
            this.basePriceTradeIn == other.getBasePriceTradeIn() &&
            this.basePriceLoan == other.getBasePriceLoan() &&
            this.basePriceRetail == other.getBasePriceRetail();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getMileageAdjustment();
        _hashCode += getMsrp();
        _hashCode += getBasePriceTradeIn();
        _hashCode += getBasePriceLoan();
        _hashCode += getBasePriceRetail();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(NADABookoutValues.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "NADABookoutValues"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mileageAdjustment");
        elemField.setXmlName(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "mileageAdjustment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msrp");
        elemField.setXmlName(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "msrp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("basePriceTradeIn");
        elemField.setXmlName(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "basePriceTradeIn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("basePriceLoan");
        elemField.setXmlName(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "basePriceLoan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("basePriceRetail");
        elemField.setXmlName(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "basePriceRetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

	public int getAveragePriceTradeIn() {
		return averagePriceTradeIn;
	}

	public void setAveragePriceTradeIn(int averagePriceTradeIn) {
		this.averagePriceTradeIn = averagePriceTradeIn;
	}

	public int getRoughPriceTradeIn() {
		return roughPriceTradeIn;
	}

	public void setRoughPriceTradeIn(int roughPriceTradeIn) {
		this.roughPriceTradeIn = roughPriceTradeIn;
	}

}
