package com.firstlook.thirdparty.book.nada;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.firstlook.thirdparty.book.newnada.data.VehicleUIData.Accessories;
import com.firstlook.thirdparty.book.newnada.data.VehicleUIData.ExclusiveAccessories;
import com.firstlook.thirdparty.book.newnada.data.VehicleUIData.InclusiveAccessories;

/**
 * Calulates the accessory adjustment values taking into account package included accessories.
 * 
 * Follows the builder pattern.
 * 
 * @author bfung
 *
 */
class AccessoryAdjustmentCalculator {
	
	public enum AccessorySelectionState {
		ADDED, INCLUDED, EXCLUDED;
	}

	private final Map<String, Accessories> accessories = new HashMap<String, Accessories>();
	private final Map<String, List<Accessories>> includesReferenceList = new HashMap<String, List<Accessories>>();
	private final Map<String, List<Accessories>> excludesReferenceList = new HashMap<String, List<Accessories>>();
	private final Map<String, List<Accessories>> excludesRevserseReferenceList = new HashMap<String, List<Accessories>>();
	
	private Map<String, Accessories> excluded = new HashMap<String, Accessories>();
	
	public AccessoryAdjustmentCalculator(List<Accessories> options, List<InclusiveAccessories> includes, List<ExclusiveAccessories> excludes) {
		for(Accessories accessory : options) {
			accessories.put(accessory.getAccCode(), accessory);
		}
		
		for(InclusiveAccessories includeRule : includes) {
			final String key = includeRule.getAccCode();
			List<Accessories> includedAccessories;
			if(includesReferenceList.containsKey(key)) {
				includedAccessories = includesReferenceList.get(key);
			} else {
				includedAccessories = new ArrayList<Accessories>();
				includesReferenceList.put(key, includedAccessories);
			}
			includedAccessories.add(accessories.get(includeRule.getAccInclCode()));
		}
		
		for(ExclusiveAccessories excludeRule : excludes) {
			final String key = excludeRule.getAccCode();
			List<Accessories> conflicts;
			if(excludesReferenceList.containsKey(key)) {
				conflicts = excludesReferenceList.get(key);
			} else {
				conflicts = new ArrayList<Accessories>();
				excludesReferenceList.put(key, conflicts);
			}
			conflicts.add(accessories.get(excludeRule.getAccExcludeCode()));
			
			//build revserseLookup
			final String revserseKey = excludeRule.getAccExcludeCode();
			List<Accessories> reverseLookup;
			if(excludesRevserseReferenceList.containsKey(revserseKey)) {
				reverseLookup = excludesRevserseReferenceList.get(revserseKey);
			} else {
				reverseLookup = new ArrayList<Accessories>();
				excludesRevserseReferenceList.put(revserseKey, reverseLookup);
			}
			reverseLookup.add(accessories.get(excludeRule.getAccCode()));
		}
	}
	
	/**
	 * Selects the accessory and updates package included options and excluded options.
	 * 
	 * Not thread safe!
	 * @param accessoryCode
	 * @throws NADAException if an option conflict occurs (mutually exclusive options) or the accessory code is not part of the set when initializing this object.
	 */
	public void select(String accessoryCode) throws NADAOptionSelectionException, NADAOptionConflictException {
		if(excluded.containsKey(accessoryCode)) {
			Accessories selected = excluded.get(accessoryCode);
			Accessories conflictWith = null;
			if(excludesRevserseReferenceList.containsKey(accessoryCode)) {
				List<Accessories> conflictsWith = excludesRevserseReferenceList.get(accessoryCode);
				for(Accessories conflict : conflictsWith) {
					if(conflict.isAccIsAdded()) {
						conflictWith = conflict;
						break;
					}
				}
			}
			
			throw new NADAOptionConflictException(selected, conflictWith);
		} else if (accessories.containsKey(accessoryCode)) {
			Accessories selected = accessories.get(accessoryCode);
			selected.setAccIsAdded(Boolean.TRUE);
			
			//process excludes
			if(excludesReferenceList.containsKey(accessoryCode)) {
				List<Accessories> excludes = excludesReferenceList.get(accessoryCode);
				for(Accessories exclude : excludes) {
					excluded.put(exclude.getAccCode(), exclude);
					accessories.remove(exclude);
				}
			}
			
			//process includes
			if(includesReferenceList.containsKey(accessoryCode)) {
				List<Accessories> includes = includesReferenceList.get(accessoryCode);
				for(Accessories include : includes) {
					include.setAccIsIncluded(Boolean.TRUE);
				}
			}
		} else {
			//not in exclude list, not in accessories.... rogue accessoryCode
			throw new NADAOptionSelectionException(MessageFormat.format("AccessoryCode {0} does not belong to vehicle!", accessoryCode));
		}
	}
	
	public AccessoryAdjustmentValue getValues() {
		int retail = 0;
		int loan = 0;
		int tradeIn = 0;
		Collection<Accessories> options = accessories.values();
		for(Accessories option : options) {
			if(option.isAccIsAdded() && !excluded.containsKey(option.getAccCode())) {
				if(!option.isAccIsIncluded()) {
					retail += option.getAccRetail();
					loan += option.getAccLoan();
					tradeIn += option.getAccTradeIn();
				}
			}
		}
		return new AccessoryAdjustmentValue(retail, loan, tradeIn);
	}
	
	public AccessorySelectionState queryAccessorySelectionState(String accessoryCode) throws NADAOptionSelectionException {
		AccessorySelectionState state = null;
		if(excluded.containsKey(accessoryCode)) {
			state = AccessorySelectionState.EXCLUDED;
		} else if (accessories.containsKey(accessoryCode)) {
			Accessories queried = accessories.get(accessoryCode);
			if(queried.isAccIsIncluded()) {
				state = AccessorySelectionState.INCLUDED;
			} else {
				state = AccessorySelectionState.ADDED;
			}
		} else {
			throw new NADAOptionSelectionException(MessageFormat.format("AccessoryCode {0} does not belong in the set!", accessoryCode));
		}
		return state;
	}
}