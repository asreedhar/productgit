package com.firstlook.thirdparty.book.newnada.service;

import java.lang.reflect.Proxy;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.codehaus.xfire.client.Client;
import org.codehaus.xfire.client.XFireProxy;
import org.codehaus.xfire.transport.http.EasySSLProtocolSocketFactory;
import org.codehaus.xfire.util.dom.DOMInHandler;
import org.codehaus.xfire.util.dom.DOMOutHandler;

import com.firstlook.thirdparty.book.nada.NADAException;
import com.firstlook.thirdparty.book.nada.webservice.ArrayOfNADARegionStateAndCode;
import com.firstlook.thirdparty.book.nada.webservice.NADABookoutValues;
import com.firstlook.thirdparty.book.nada.webservice.NADARegionStateAndCode;
import com.firstlook.thirdparty.book.nada.webservice.NADAVehicle;
import com.firstlook.thirdparty.book.newnada.DoVinLookupResponse.DoVinLookupResult;
import com.firstlook.thirdparty.book.newnada.GetEquipmentOptionsNoVinResponse.GetEquipmentOptionsNoVinResult;
import com.firstlook.thirdparty.book.newnada.GetEquipmentOptionsResponse.GetEquipmentOptionsResult;
import com.firstlook.thirdparty.book.newnada.GetMakesResponse.GetMakesResult;
import com.firstlook.thirdparty.book.newnada.GetModelResponse.GetModelResult;
import com.firstlook.thirdparty.book.newnada.GetMutuallyExclusiveOptionsListResponse.GetMutuallyExclusiveOptionsListResult;
import com.firstlook.thirdparty.book.newnada.GetNADARegionsResponse.GetNADARegionsResult;
import com.firstlook.thirdparty.book.newnada.GetPackageIncludedOptionsListResponse.GetPackageIncludedOptionsListResult;
import com.firstlook.thirdparty.book.newnada.GetTrimResponse.GetTrimResult;
import com.firstlook.thirdparty.book.newnada.GetValuesNoVinResponse.GetValuesNoVinResult;
import com.firstlook.thirdparty.book.newnada.GetValuesResponse.GetValuesResult;
import com.firstlook.thirdparty.book.newnada.GetYearsResponse.GetYearsResult;
import com.firstlook.thirdparty.book.newnada.client.NADADirectResponseHandler;
import com.firstlook.thirdparty.book.newnada.client.NadaWebServiceClient;
import com.firstlook.thirdparty.book.newnada.client.NadaWebServiceSoap;
import com.firstlook.thirdparty.book.newnada.client.RequestLoggingHandler;
import com.firstlook.thirdparty.book.newnada.data.VehicleUIData;
import com.firstlook.thirdparty.book.newnada.data.VehicleUIData.Accessories;
import com.firstlook.thirdparty.book.newnada.data.VehicleUIData.Bodies;
import com.firstlook.thirdparty.book.newnada.data.VehicleUIData.ExclusiveAccessories;
import com.firstlook.thirdparty.book.newnada.data.VehicleUIData.InclusiveAccessories;
import com.firstlook.thirdparty.book.newnada.data.VehicleUIData.Makes;
import com.firstlook.thirdparty.book.newnada.data.VehicleUIData.RegionStates;
import com.firstlook.thirdparty.book.newnada.data.VehicleUIData.Series;
import com.firstlook.thirdparty.book.newnada.data.VehicleUIData.VehicleValues;
import com.firstlook.thirdparty.book.newnada.data.VehicleUIData.Vehicles;
import com.firstlook.thirdparty.book.newnada.data.VehicleUIData.Years;

public class NewNadaAdapter {

	private NadaWebServiceSoap service = null;
	
	public void initializeNADAService(){
		Protocol protocol = new Protocol("https", (ProtocolSocketFactory) new EasySSLProtocolSocketFactory(), 443);
		Protocol.registerProtocol("https", protocol);

		NadaWebServiceClient nadaWebServiceClient = new NadaWebServiceClient();
		service = nadaWebServiceClient.getNadaWebServiceSoap();
		
		Client client =  ((XFireProxy)
				Proxy.getInvocationHandler(service)).getClient();
		client.addInHandler(new DOMInHandler());
		client.addInHandler(new NADADirectResponseHandler());
		
		client.addOutHandler(new DOMOutHandler());
		client.addOutHandler(new RequestLoggingHandler());
	}
	
	public NADAVehicle[] doVinLookup( int regionCode, String vin ) throws NADAException
	{
		DoVinLookupResult data = service.doVinLookup(vin);
		if(data != null) {
			VehicleUIData nadaVehicleData = data.getVehicleUIData();
			if (nadaVehicleData == null) 
				return new NADAVehicle[0];
				
			List<Vehicles> vehicles = nadaVehicleData.getVehicles();
			NADAVehicle[] nadaVehicles = new NADAVehicle[vehicles.size()];
			for(int i = 0; i < vehicles.size(); i++){
				nadaVehicles[i] = new NADAVehicle();
				Vehicles vehicle = vehicles.get(i);
				nadaVehicles[i].setMakeId(vehicle.getMakeCode());
				nadaVehicles[i].setMake(vehicle.getMakeDescr());
				nadaVehicles[i].setSeriesId(vehicle.getSeriesCode());
				nadaVehicles[i].setSeries(vehicle.getSeriesDescr());
				nadaVehicles[i].setBodyId(vehicle.getBodyCode());
				nadaVehicles[i].setBody(vehicle.getBodyDescr());
				nadaVehicles[i].setYear(vehicle.getVehicleYear().toString());
				nadaVehicles[i].setVehicleUID(vehicle.getUid().toString());
				
				nadaVehicles[i].setMileageId(vehicle.getMileageCode());
				nadaVehicles[i].setCurbWeight(vehicle.getCurbWeight());
				nadaVehicles[i].setGvw(vehicle.getGVW());
				nadaVehicles[i].setGcw(vehicle.getGCW());
				nadaVehicles[i].setMsrp(vehicle.getMSRP());
			}
			return nadaVehicles;
		}
		throw new NADAException("Vin Not found");
	}
	
	public List<Integer> loadModelYears() throws NADAException
	{
		GetYearsResult yearsResult = service.getYears();
		List<Integer> modelYears = new ArrayList<Integer>();
		if(yearsResult != null) {
			VehicleUIData nadaVehicleData = yearsResult.getVehicleUIData();
			if (nadaVehicleData == null) 
				return modelYears;
			
			List<Years> yearsList = nadaVehicleData.getYears();
			for(Years year : yearsList) {
				modelYears.add(year.getVehicleYear());
			}
		}
		return modelYears;
	}

	public NADAVehicle[] loadMakes( String year ) throws NADAException
	{
		GetMakesResult getMakesResults = service.getMakes(Integer.valueOf(year));
		if(getMakesResults != null) {
			VehicleUIData nadaVehicleData = getMakesResults.getVehicleUIData();
			if (nadaVehicleData == null) 
				return new NADAVehicle[0];
			
			List<Makes> makes = nadaVehicleData.getMakes();
			NADAVehicle[] nadaVehicles = new NADAVehicle[makes.size()];
			for(int i = 0; i < makes.size(); i++){
				nadaVehicles[i] = new NADAVehicle();
				Makes make = makes.get(i);
				
				nadaVehicles[i].setMakeId(make.getMakeCode());
				nadaVehicles[i].setMake(make.getMakeDescr());
			}
			
			return nadaVehicles;
		}
		throw new NADAException(MessageFormat.format("No Makes found for ModelYear:{0}", year));
	}

	public NADAVehicle[] loadSeries( String year, int makeId ) throws NADAException
	{
		GetModelResult getModelResults = service.getModel(Integer.valueOf(year), makeId);
		if(getModelResults != null) {
			VehicleUIData nadaVehicleData = getModelResults.getVehicleUIData();
			if (nadaVehicleData == null) 
				return new NADAVehicle[0];
			
			List<Series> seriesList = nadaVehicleData.getSeries();
			NADAVehicle[] nadaVehicles = new NADAVehicle[seriesList.size()];
			
			for(int i = 0; i < seriesList.size(); i++){
				nadaVehicles[i] = new NADAVehicle();
				Series series = seriesList.get(i);
				
				nadaVehicles[i].setSeriesId(series.getSeriesCode());
				nadaVehicles[i].setSeries(series.getSeriesDescr());
			}
			
			return nadaVehicles;
		}
		throw new NADAException(MessageFormat.format("No series found for ModelYear:{0}, MakeID:{1}", year, Integer.valueOf(makeId).toString()));
	}

	public NADAVehicle[] loadBodies( String year, int makeId, int seriesId ) throws NADAException
	{
		GetTrimResult getTrimResult = service.getTrim(Integer.valueOf(year), makeId, seriesId);
		if(getTrimResult != null) {
			VehicleUIData nadaVehicleData = getTrimResult.getVehicleUIData();
			List<Bodies> trimList = nadaVehicleData.getBodies();
			if (nadaVehicleData == null) 
				return new NADAVehicle[0];
			
			NADAVehicle[] nadaVehicles = new NADAVehicle[trimList.size()];
			
			for(int i = 0; i < trimList.size(); i++){
				nadaVehicles[i] = new NADAVehicle();
				Bodies body = trimList.get(i);
				
				nadaVehicles[i].setBodyId(body.getBodyCode());
				nadaVehicles[i].setBody(body.getBodyDescr());
			}
			
			return nadaVehicles;
		}
		throw new NADAException(MessageFormat.format("No series found for ModelYear:{0}, MakeID:{1}, SeriesID:{2}", 
				year, Integer.valueOf(makeId).toString(), Integer.valueOf(seriesId).toString()));
	}

	/**
	 * @param regionCode
	 * @param vin - this parameter is not used at all!
	 * @param uid
	 * @return
	 * @throws NADAException
	 */
	public List<Accessories> getNADAVehicleOptions( int regionCode, String vin, String uid) throws NADAException
	{
		List<Accessories> accessories = new ArrayList<Accessories>();
		GetEquipmentOptionsResult optionsResults = service.getEquipmentOptions(regionCode, Integer.valueOf(uid), vin);
		if(optionsResults != null) {
			VehicleUIData data = optionsResults.getVehicleUIData();
			if(data!= null) {
				accessories.addAll(data.getAccessories());
			}
		}
		return accessories;
	}

	public List<Accessories> getNADAVehicleOptions( int regionCode, String year, int makeCode, int seriesCode, int bodyCode ) throws NADAException
	{
		List<Accessories> accessories = new ArrayList<Accessories>();
		GetEquipmentOptionsNoVinResult results = service.getEquipmentOptions_NoVin(bodyCode, regionCode);
		if(results != null) {
			VehicleUIData data = results.getVehicleUIData();
			if(data!= null) {
				accessories.addAll(data.getAccessories());
			}
		}
		return accessories;
	}
	
	public List<InclusiveAccessories> getPackagedIncludedOptions(int uid) {
		List<InclusiveAccessories> accessories = new ArrayList<InclusiveAccessories>();
		GetPackageIncludedOptionsListResult results = service.getPackageIncludedOptionsList(uid);
		if(results != null) {
			VehicleUIData data = results.getVehicleUIData();
			if(data != null) {
				accessories.addAll(data.getInclusiveAccessories());
			}
		}
		return accessories;
	}
	
	public List<ExclusiveAccessories> getMutuallyExclusiveOptions(int uid) {
		List<ExclusiveAccessories> accessories = new ArrayList<ExclusiveAccessories>();
		GetMutuallyExclusiveOptionsListResult results = service.getMutuallyExclusiveOptionsList(uid);
		if(results != null) {
			VehicleUIData data = results.getVehicleUIData();
			if(data != null) {
				accessories.addAll(data.getExclusiveAccessories());
			}
		}
		return accessories;
	}

	public NADABookoutValues getNADABaseBookoutValues(String uid, int regionCode, int mileage ) throws NADAException
	{
		NADABookoutValues bookoutValues = new NADABookoutValues();
		GetValuesResult getValuesResult = service.getValues(Integer.valueOf(uid), mileage, regionCode);
		
		VehicleUIData nadaVehicleData = getValuesResult.getVehicleUIData();
		if (nadaVehicleData == null) 
			return bookoutValues;
		
		VehicleValues vehicleValues = nadaVehicleData.getVehicleValues().get(0);
		
		bookoutValues.setBasePriceLoan(vehicleValues.getLoan());
		bookoutValues.setBasePriceRetail(vehicleValues.getRetail());
		bookoutValues.setBasePriceTradeIn(vehicleValues.getTradeIn());
		bookoutValues.setAveragePriceTradeIn(vehicleValues.getAvgTradeIn());
		bookoutValues.setRoughPriceTradeIn(vehicleValues.getRoughTradeIn());
		
		BigDecimal mileageAdj = service.getMileageAdjustment(Integer.valueOf(uid), mileage, regionCode);
		bookoutValues.setMileageAdjustment(mileageAdj.intValue());
		
		return bookoutValues;
	}

	public NADABookoutValues getNADABaseBookoutValues( int regionCode, String year, int makeCode, int seriesCode, int bodyCode, int mileage ) throws NADAException
	{
		NADABookoutValues bookoutValues = new NADABookoutValues();
		GetValuesNoVinResult getValuesNoVinResult = service.getValues_NoVin(bodyCode, regionCode, mileage);		
		VehicleUIData nadaVehicleData = getValuesNoVinResult.getVehicleUIData();
		if (nadaVehicleData == null) 
			return bookoutValues;
		
		VehicleValues vehicleValues = nadaVehicleData.getVehicleValues().get(0);
		
		bookoutValues.setBasePriceLoan(vehicleValues.getLoan());
		bookoutValues.setBasePriceRetail(vehicleValues.getRetail());
		bookoutValues.setBasePriceTradeIn(vehicleValues.getTradeIn());
		bookoutValues.setAveragePriceTradeIn(vehicleValues.getAvgTradeIn());
		bookoutValues.setRoughPriceTradeIn(vehicleValues.getRoughTradeIn());
		
		Integer uid = nadaVehicleData.getVehicles().get(0).getUid();
		BigDecimal mileageAdj = service.getMileageAdjustment(uid, mileage, regionCode);
		bookoutValues.setMileageAdjustment(mileageAdj.intValue());
		
		return bookoutValues;
	}

	public String getPeriod() throws NADAException
	{
		String getNADAPublishPeriodResponse = service.getNADAPublishPeriod();
		return getNADAPublishPeriodResponse;
	}

	public ArrayOfNADARegionStateAndCode getNADARegionsStatesAndCodes() throws NADAException
	{
		GetNADARegionsResult getNADARegionsResult =  service.getNADARegions();
		
		VehicleUIData nadaVehicleData = getNADARegionsResult.getVehicleUIData();
		if (nadaVehicleData == null) 
			return new ArrayOfNADARegionStateAndCode();
		
		List<RegionStates> regionStateAndCodeList = nadaVehicleData.getRegionStates();
		
		NADARegionStateAndCode[] nadaRegionStateAndCode = new NADARegionStateAndCode[regionStateAndCodeList.size()];
		
		for(int i = 0; i < regionStateAndCodeList.size(); i++){
			
			nadaRegionStateAndCode[i] = new NADARegionStateAndCode();
			RegionStates regionState = regionStateAndCodeList.get(i);
			nadaRegionStateAndCode[i].setRegionCode(regionState.getRegionStatesId());
			nadaRegionStateAndCode[i].setRegionOrStateName(regionState.getRegionStatesDescr());
		}
		
		return new ArrayOfNADARegionStateAndCode(nadaRegionStateAndCode);
	}
	
	
}
