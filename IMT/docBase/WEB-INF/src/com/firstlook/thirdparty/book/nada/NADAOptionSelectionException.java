package com.firstlook.thirdparty.book.nada;


public class NADAOptionSelectionException extends NADAException {

	private static final long serialVersionUID = 4836687764247301333L;

	public NADAOptionSelectionException() {
		super();
	}

	public NADAOptionSelectionException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public NADAOptionSelectionException(String arg0) {
		super(arg0);
	}

	public NADAOptionSelectionException(Throwable arg0) {
		super(arg0);
	}
}
