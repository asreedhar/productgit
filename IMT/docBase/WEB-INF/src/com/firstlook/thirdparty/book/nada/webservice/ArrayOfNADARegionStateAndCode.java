/**
 * ArrayOfNADARegionStateAndCode.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.firstlook.thirdparty.book.nada.webservice;

public class ArrayOfNADARegionStateAndCode  implements java.io.Serializable {
    private static final long serialVersionUID = -6710397583204545111L;
	private com.firstlook.thirdparty.book.nada.webservice.NADARegionStateAndCode[] NADARegionStateAndCode;

    public ArrayOfNADARegionStateAndCode() {
    }

    public ArrayOfNADARegionStateAndCode(
           com.firstlook.thirdparty.book.nada.webservice.NADARegionStateAndCode[] NADARegionStateAndCode) {
           this.NADARegionStateAndCode = NADARegionStateAndCode;
    }


    /**
     * Gets the NADARegionStateAndCode value for this ArrayOfNADARegionStateAndCode.
     * 
     * @return NADARegionStateAndCode
     */
    public com.firstlook.thirdparty.book.nada.webservice.NADARegionStateAndCode[] getNADARegionStateAndCode() {
        return NADARegionStateAndCode;
    }


    /**
     * Sets the NADARegionStateAndCode value for this ArrayOfNADARegionStateAndCode.
     * 
     * @param NADARegionStateAndCode
     */
    public void setNADARegionStateAndCode(com.firstlook.thirdparty.book.nada.webservice.NADARegionStateAndCode[] NADARegionStateAndCode) {
        this.NADARegionStateAndCode = NADARegionStateAndCode;
    }

    public com.firstlook.thirdparty.book.nada.webservice.NADARegionStateAndCode getNADARegionStateAndCode(int i) {
        return this.NADARegionStateAndCode[i];
    }

    public void setNADARegionStateAndCode(int i, com.firstlook.thirdparty.book.nada.webservice.NADARegionStateAndCode _value) {
        this.NADARegionStateAndCode[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ArrayOfNADARegionStateAndCode)) return false;
        ArrayOfNADARegionStateAndCode other = (ArrayOfNADARegionStateAndCode) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.NADARegionStateAndCode==null && other.getNADARegionStateAndCode()==null) || 
             (this.NADARegionStateAndCode!=null &&
              java.util.Arrays.equals(this.NADARegionStateAndCode, other.getNADARegionStateAndCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNADARegionStateAndCode() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getNADARegionStateAndCode());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getNADARegionStateAndCode(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ArrayOfNADARegionStateAndCode.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "ArrayOfNADARegionStateAndCode"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NADARegionStateAndCode");
        elemField.setXmlName(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "NADARegionStateAndCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "NADARegionStateAndCode"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
