package com.firstlook.thirdparty.book.newnada;

import javax.xml.bind.annotation.XmlRegistry;

import com.firstlook.thirdparty.book.newnada.DoVinLookupResponse.DoVinLookupResult;
import com.firstlook.thirdparty.book.newnada.GetEquipmentOptionsNoVinResponse.GetEquipmentOptionsNoVinResult;
import com.firstlook.thirdparty.book.newnada.GetEquipmentOptionsResponse.GetEquipmentOptionsResult;
import com.firstlook.thirdparty.book.newnada.GetMakesResponse.GetMakesResult;
import com.firstlook.thirdparty.book.newnada.GetModelResponse.GetModelResult;
import com.firstlook.thirdparty.book.newnada.GetMutuallyExclusiveOptionsListResponse.GetMutuallyExclusiveOptionsListResult;
import com.firstlook.thirdparty.book.newnada.GetNADARegionsResponse.GetNADARegionsResult;
import com.firstlook.thirdparty.book.newnada.GetPackageIncludedOptionsListResponse.GetPackageIncludedOptionsListResult;
import com.firstlook.thirdparty.book.newnada.GetTrimResponse.GetTrimResult;
import com.firstlook.thirdparty.book.newnada.GetYearsResponse.GetYearsResult;
import com.firstlook.thirdparty.book.newnada.data.VehicleUIData;

/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the biz.firstlook.nadawebservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

	/**
	 * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: biz.firstlook.nadawebservice
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link GetValuesNoVin }
	 * 
	 */
	public GetValuesNoVin createGetValuesNoVin() {
		return new GetValuesNoVin();
	}

	/**
	 * Create an instance of {@link GetModel }
	 * 
	 */
	public GetModel createGetModel() {
		return new GetModel();
	}

	/**
	 * Create an instance of {@link GetNADARegions }
	 * 
	 */
	public GetNADARegions createGetNADARegions() {
		return new GetNADARegions();
	}

	/**
	 * Create an instance of {@link GetNADAPublishPeriod }
	 * 
	 */
	public GetNADAPublishPeriod createGetNADAPublishPeriod() {
		return new GetNADAPublishPeriod();
	}

	/**
	 * Create an instance of {@link VehicleUIData.Vehicles }
	 * 
	 */
	public VehicleUIData.Vehicles createVehicleUIDataVehicles() {
		return new VehicleUIData.Vehicles();
	}

	/**
	 * Create an instance of {@link GetYearsResponse }
	 * 
	 */
	public GetYearsResponse createGetYearsResponse() {
		return new GetYearsResponse();
	}

	/**
	 * Create an instance of {@link VehicleUIData.Years }
	 * 
	 */
	public VehicleUIData.Years createVehicleUIDataYears() {
		return new VehicleUIData.Years();
	}

	/**
	 * Create an instance of {@link GetTrimResponse }
	 * 
	 */
	public GetTrimResponse createGetTrimResponse() {
		return new GetTrimResponse();
	}

	/**
	 * Create an instance of {@link GetEquipmentOptionsNoVinResponse }
	 * 
	 */
	public GetValuesNoVinResponse createGetEquipmentOptionsNoVinResponse() {
		return new GetValuesNoVinResponse();
	}

	/**
	 * Create an instance of {@link GetNADARegionsResponse }
	 * 
	 */
	public GetNADARegionsResponse createGetNADARegionsResponse() {
		return new GetNADARegionsResponse();
	}

	/**
	 * Create an instance of {@link GetYears }
	 * 
	 */
	public GetYears createGetYears() {
		return new GetYears();
	}

	/**
	 * Create an instance of {@link VehicleUIData.VehicleValues }
	 * 
	 */
	public VehicleUIData.VehicleValues createVehicleUIDataVehicleValues() {
		return new VehicleUIData.VehicleValues();
	}

	/**
	 * Create an instance of {@link GetEquipmentOptionsNoVin }
	 * 
	 */
	public GetEquipmentOptionsNoVin createGetEquipmentOptionsNoVin() {
		return new GetEquipmentOptionsNoVin();
	}

	/**
	 * Create an instance of {@link VehicleUIData }
	 * 
	 */
	//   @XmlElementDecl(namespace = "NADAVehicle_Data", name = "VehicleUI_Data") 
	public VehicleUIData createVehicleUIData() {
		return new VehicleUIData();
	}

	/**
	 * Create an instance of {@link VehicleUIData.InclusiveAccessories }
	 * 
	 */
	public VehicleUIData.InclusiveAccessories createVehicleUIDataInclusiveAccessories() {
		return new VehicleUIData.InclusiveAccessories();
	}

	/**
	 * Create an instance of {@link GetNADAPublishPeriodResponse }
	 * 
	 */
	public GetNADAPublishPeriodResponse createGetNADAPublishPeriodResponse() {
		return new GetNADAPublishPeriodResponse();
	}

	/**
	 * Create an instance of {@link GetValues }
	 * 
	 */
	public GetValues createGetValues() {
		return new GetValues();
	}

	/**
	 * Create an instance of {@link GetTrim }
	 * 
	 */
	public GetTrim createGetTrim() {
		return new GetTrim();
	}

	/**
	 * Create an instance of {@link VehicleUIData.Bodies }
	 * 
	 */
	public VehicleUIData.Bodies createVehicleUIDataBodies() {
		return new VehicleUIData.Bodies();
	}

	/**
	 * Create an instance of {@link VehicleUIData.RegionStates }
	 * 
	 */
	public VehicleUIData.RegionStates createVehicleUIDataRegionStates() {
		return new VehicleUIData.RegionStates();
	}

	/**
	 * Create an instance of {@link DoVinLookup }
	 * 
	 */
	public DoVinLookup createDoVinLookup() {
		return new DoVinLookup();
	}

	/**
	 * Create an instance of {@link GetValuesResponse }
	 * 
	 */
	public GetValuesResponse createGetValuesResponse() {
		return new GetValuesResponse();
	}

	/**
	 * Create an instance of {@link GetEquipmentOptions }
	 * 
	 */
	public GetEquipmentOptions createGetEquipmentOptions() {
		return new GetEquipmentOptions();
	}

	/**
	 * Create an instance of {@link VehicleUIData.MileageValues }
	 * 
	 */
	public VehicleUIData.MileageValues createVehicleUIDataMileageValues() {
		return new VehicleUIData.MileageValues();
	}

	/**
	 * Create an instance of {@link GetMakes }
	 * 
	 */
	public GetMakes createGetMakes() {
		return new GetMakes();
	}

	/**
	 * Create an instance of {@link VehicleUIData.ExclusiveAccessories }
	 * 
	 */
	public VehicleUIData.ExclusiveAccessories createVehicleUIDataExclusiveAccessories() {
		return new VehicleUIData.ExclusiveAccessories();
	}

	/**
	 * Create an instance of {@link VehicleUIData.Accessories }
	 * 
	 */
	public VehicleUIData.Accessories createVehicleUIDataAccessories() {
		return new VehicleUIData.Accessories();
	}

	/**
	 * Create an instance of {@link DoVinLookupResponse }
	 * 
	 */
	public DoVinLookupResponse createDoVinLookupResponse() {
		return new DoVinLookupResponse();
	}

	/**
	 * Create an instance of {@link DoVinLookupResult}
	 * 
	 */
	public DoVinLookupResult createDoVinLookupResult() {
		return new DoVinLookupResult();
	}

	/**
	 * Create an instance of {@link GetEquipmentOptionsResponse }
	 * 
	 */
	public GetNADARegionsResponse createGetEquipmentOptionsResponse() {
		return new GetNADARegionsResponse();
	}

	/**
	 * Create an instance of {@link VehicleUIData.Series }
	 * 
	 */
	public VehicleUIData.Series createVehicleUIDataSeries() {
		return new VehicleUIData.Series();
	}

	/**
	 * Create an instance of {@link GetModelResponse }
	 * 
	 */
	public GetModelResponse createGetModelResponse() {
		return new GetModelResponse();
	}

	/**
	 * Create an instance of {@link GetValuesNoVinResponse }
	 * 
	 */
	public GetValuesNoVinResponse createGetValuesNoVinResponse() {
		return new GetValuesNoVinResponse();
	}

	/**
	 * Create an instance of {@link VehicleUIData.Makes }
	 * 
	 */
	public VehicleUIData.Makes createVehicleUIDataMakes() {
		return new VehicleUIData.Makes();
	}

	/**
	 * Create an instance of {@link GetMakesResponse }
	 * 
	 */
	public GetMakesResponse createGetMakesResponse() {
		return new GetMakesResponse();
	}

	public GetEquipmentOptionsResult createGetEquipmentOptionsResult() {
		return new GetEquipmentOptionsResult();
	}

	public GetYearsResult createGetYearsResult() {
		return new GetYearsResult();
	}

	public GetTrimResult createGetTrimResult() {
		return new GetTrimResult();
	}

	public GetNADARegionsResult createGetNADARegionsResult() {
		return new GetNADARegionsResult();
	}

	public GetModelResult createGetModelResult() {
		return new GetModelResult();
	}

	public GetMakesResult createGetMakesResult() {
		return new GetMakesResult();
	}

	public GetEquipmentOptionsNoVinResult createGetEquipmentOptionsNoVinResult() {
		return new GetEquipmentOptionsNoVinResult();
	}
	
	public GetMutuallyExclusiveOptionsList createGetMutuallyExclusiveOptionsList() {
		return new GetMutuallyExclusiveOptionsList();
	}
	
	public GetMutuallyExclusiveOptionsListResponse createGetMutuallyExclusiveOptionsListResponse() {
		return new GetMutuallyExclusiveOptionsListResponse();
	}
	
	public GetMutuallyExclusiveOptionsListResult createGetMutuallyExclusiveOptionsListResult() {
		return new GetMutuallyExclusiveOptionsListResult();
	}
	
	public GetPackageIncludedOptionsList createGetPackageIncludedOptionsList() {
		return new GetPackageIncludedOptionsList();
	}
	
	public GetPackageIncludedOptionsListResponse createGetPackageIncludedOptionsListResponse() {
		return new GetPackageIncludedOptionsListResponse();
	}
	
	public GetPackageIncludedOptionsListResult createGetPackageIncludedOptionsListResult() {
		return new GetPackageIncludedOptionsListResult();
	}
}
