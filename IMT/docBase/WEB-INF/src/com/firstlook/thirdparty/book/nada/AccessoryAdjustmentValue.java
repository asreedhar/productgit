package com.firstlook.thirdparty.book.nada;

import java.io.Serializable;

public class AccessoryAdjustmentValue implements Serializable {

	private static final long serialVersionUID = 2682473735034479545L;
	
	private final int retail;
	private final int loan;
	private final int tradeIn;
	
	public AccessoryAdjustmentValue(int retail, int loan, int tradeIn) {
		this.retail = retail;
		this.loan = loan;
		this.tradeIn = tradeIn;
	}
	
	public int getRetail() {
		return retail;
	}
	
	public int getTradeIn() {
		return tradeIn;
	}
	
	public int getAverageTradeIn() {
		return tradeIn;
	}
	
	public int getRoughTradeIn() {
		return tradeIn;
	}
	
	public int getLoan() {
		return loan;
	}
}
