package com.firstlook.thirdparty.book.nada;

import java.lang.reflect.InvocationTargetException;
import java.util.Comparator;

import org.apache.commons.beanutils.PropertyUtils;

import biz.firstlook.module.vehicle.description.old.NadaConditionMarketType;

public class NADACategoryComparator< T > implements Comparator< T >
{

public int compare( T o1, T o2 )
{
	try
	{
		String category1 = (String)PropertyUtils.getProperty( o1, "category" );
		NadaConditionMarketType cat1 = NadaConditionMarketType.valueOfDescription( category1 );
		
		String category2 = (String)PropertyUtils.getProperty( o2, "category" );
		NadaConditionMarketType cat2 = NadaConditionMarketType.valueOfDescription( category2 );
		
		if( cat1.getOrder() == cat2.getOrder())
			return 0;
		else if( cat1.getOrder() > cat2.getOrder())
			return 1;
		else
			return -1;
			
	}
	catch ( IllegalAccessException e )
	{
		e.printStackTrace();
	}
	catch ( InvocationTargetException e )
	{
		e.printStackTrace();
	}
	catch ( NoSuchMethodException e )
	{
		e.printStackTrace();
	}
	return 0;
}

}
