/**
 * ArrayOfNADAVehicle.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.firstlook.thirdparty.book.nada.webservice;

public class ArrayOfNADAVehicle  implements java.io.Serializable {
    private static final long serialVersionUID = 5398390789523377596L;
	private com.firstlook.thirdparty.book.nada.webservice.NADAVehicle[] NADAVehicle;

    public ArrayOfNADAVehicle() {
    }

    public ArrayOfNADAVehicle(
           com.firstlook.thirdparty.book.nada.webservice.NADAVehicle[] NADAVehicle) {
           this.NADAVehicle = NADAVehicle;
    }


    /**
     * Gets the NADAVehicle value for this ArrayOfNADAVehicle.
     * 
     * @return NADAVehicle
     */
    public com.firstlook.thirdparty.book.nada.webservice.NADAVehicle[] getNADAVehicle() {
        return NADAVehicle;
    }


    /**
     * Sets the NADAVehicle value for this ArrayOfNADAVehicle.
     * 
     * @param NADAVehicle
     */
    public void setNADAVehicle(com.firstlook.thirdparty.book.nada.webservice.NADAVehicle[] NADAVehicle) {
        this.NADAVehicle = NADAVehicle;
    }

    public com.firstlook.thirdparty.book.nada.webservice.NADAVehicle getNADAVehicle(int i) {
        return this.NADAVehicle[i];
    }

    public void setNADAVehicle(int i, com.firstlook.thirdparty.book.nada.webservice.NADAVehicle _value) {
        this.NADAVehicle[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ArrayOfNADAVehicle)) return false;
        ArrayOfNADAVehicle other = (ArrayOfNADAVehicle) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.NADAVehicle==null && other.getNADAVehicle()==null) || 
             (this.NADAVehicle!=null &&
              java.util.Arrays.equals(this.NADAVehicle, other.getNADAVehicle())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNADAVehicle() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getNADAVehicle());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getNADAVehicle(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ArrayOfNADAVehicle.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "ArrayOfNADAVehicle"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NADAVehicle");
        elemField.setXmlName(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "NADAVehicle"));
        elemField.setXmlType(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "NADAVehicle"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
