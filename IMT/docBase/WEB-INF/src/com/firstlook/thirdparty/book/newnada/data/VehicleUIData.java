
package com.firstlook.thirdparty.book.newnada.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VehicleUI_Data complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VehicleUI_Data">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="RegionStates">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="RegionStatesId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="RegionStatesDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RegionStateDefaultSort" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Years">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="VehicleYear" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="VehicleYearDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Makes">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="MakeCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="MakeDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Series">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SeriesCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="SeriesDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Bodies">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="BodyCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="BodyDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Vehicles">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Uid" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="VehicleYear" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="MakeCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="MakeDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SeriesCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="SeriesDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="BodyCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="BodyDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MileageCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CurbWeight" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="GVW" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="GCW" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="MSRP" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="VehicleValues">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Uid" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="Retail" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="TradeIn" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="Loan" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="RoughTradeIn" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="AvgTradeIn" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="MileageValues">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="MileageCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="Low" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="High" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="Adjustment" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="AdjustmentRate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="AdjustmentPercent" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="Descr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Accessories">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Uid" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="AccCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="AccCodeDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="AccRetail" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="AccTradeIn" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="AccLoan" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="AccIsAdded" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                   &lt;element name="AccIsIncluded" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ExclusiveAccessories">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Uid" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="AccCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="AccExcludeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="InclusiveAccessories">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Uid" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="AccCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="AccInclCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehicleUI_Data", propOrder = {
		"regionStates", "years", "makes", "series", "bodies", "vehicles","vehicleValues", "mileageValues",
		"accessories", "exclusiveAccessories", "inclusiveAccessories"
})
@XmlRootElement(name = "VehicleUI_Data")
public class VehicleUIData {


    @XmlElement(name = "Years", required = true, type = VehicleUIData.Years.class)
    protected List<Years> years;
    
    @XmlElement(name = "Vehicles", required = true, type = VehicleUIData.Vehicles.class)
    protected List<Vehicles> vehicles;
    
    @XmlElement(name = "Bodies", required = true, type = VehicleUIData.Bodies.class)
    protected List<Bodies> bodies;
    
    @XmlElement(name = "InclusiveAccessories", required = true, type = VehicleUIData.InclusiveAccessories.class)
    protected List<InclusiveAccessories> inclusiveAccessories;
    
    @XmlElement(name = "ExclusiveAccessories", required = true, type = VehicleUIData.ExclusiveAccessories.class)
    protected List<ExclusiveAccessories> exclusiveAccessories;
    
    @XmlElement(name = "Accessories", required = true, type = VehicleUIData.Accessories.class)
    protected List<Accessories> accessories;
    
    @XmlElement(name = "VehicleValues", required = true, type = VehicleUIData.VehicleValues.class)
    protected List<VehicleValues> vehicleValues;
    
    @XmlElement(name = "Makes", required = true, type = VehicleUIData.Makes.class)
    protected List<Makes> makes;
    
    @XmlElement(name = "MileageValues", required = true, type = VehicleUIData.MileageValues.class)
    protected List<MileageValues> mileageValues;
    
    @XmlElement(name = "Series", required = true, type = VehicleUIData.Series.class)
    protected List<Series> series;
    
    @XmlElement(name = "RegionStates", required = true, type = VehicleUIData.RegionStates.class)
    protected List<RegionStates> regionStates;
    
    
   


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Uid" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="AccCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="AccCodeDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="AccRetail" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="AccTradeIn" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="AccLoan" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="AccIsAdded" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *         &lt;element name="AccIsIncluded" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "uid",
        "accCode",
        "accCodeDescr",
        "accRetail",
        "accTradeIn",
        "accLoan",
        "accIsAdded",
        "accIsIncluded"
    })
    public static class Accessories {

        @XmlElement(name = "Uid")
        protected Integer uid;
        @XmlElement(name = "AccCode")
        protected String accCode;
        @XmlElement(name = "AccCodeDescr")
        protected String accCodeDescr;
        @XmlElement(name = "AccRetail")
        protected Integer accRetail;
        @XmlElement(name = "AccTradeIn")
        protected Integer accTradeIn;
        @XmlElement(name = "AccLoan")
        protected Integer accLoan;
        @XmlElement(name = "AccIsAdded")
        protected Boolean accIsAdded;
        @XmlElement(name = "AccIsIncluded")
        protected Boolean accIsIncluded;

        /**
         * Gets the value of the uid property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getUid() {
            return uid;
        }

        /**
         * Sets the value of the uid property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setUid(Integer value) {
            this.uid = value;
        }

        /**
         * Gets the value of the accCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccCode() {
            return accCode;
        }

        /**
         * Sets the value of the accCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccCode(String value) {
            this.accCode = value;
        }

        /**
         * Gets the value of the accCodeDescr property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccCodeDescr() {
            return accCodeDescr;
        }

        /**
         * Sets the value of the accCodeDescr property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccCodeDescr(String value) {
            this.accCodeDescr = value;
        }

        /**
         * Gets the value of the accRetail property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getAccRetail() {
            return accRetail;
        }

        /**
         * Sets the value of the accRetail property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setAccRetail(Integer value) {
            this.accRetail = value;
        }

        /**
         * Gets the value of the accTradeIn property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getAccTradeIn() {
            return accTradeIn;
        }

        /**
         * Sets the value of the accTradeIn property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setAccTradeIn(Integer value) {
            this.accTradeIn = value;
        }

        /**
         * Gets the value of the accLoan property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getAccLoan() {
            return accLoan;
        }

        /**
         * Sets the value of the accLoan property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setAccLoan(Integer value) {
            this.accLoan = value;
        }

        /**
         * Gets the value of the accIsAdded property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isAccIsAdded() {
            return accIsAdded;
        }

        /**
         * Sets the value of the accIsAdded property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setAccIsAdded(Boolean value) {
            this.accIsAdded = value;
        }

        /**
         * Gets the value of the accIsIncluded property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isAccIsIncluded() {
            return accIsIncluded;
        }

        /**
         * Sets the value of the accIsIncluded property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setAccIsIncluded(Boolean value) {
            this.accIsIncluded = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="BodyCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="BodyDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "bodyCode",
        "bodyDescr"
    })
    public static class Bodies {

        @XmlElement(name = "BodyCode")
        protected Integer bodyCode;
        @XmlElement(name = "BodyDescr")
        protected String bodyDescr;

        /**
         * Gets the value of the bodyCode property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getBodyCode() {
            return bodyCode;
        }

        /**
         * Sets the value of the bodyCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setBodyCode(Integer value) {
            this.bodyCode = value;
        }

        /**
         * Gets the value of the bodyDescr property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBodyDescr() {
            return bodyDescr;
        }

        /**
         * Sets the value of the bodyDescr property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBodyDescr(String value) {
            this.bodyDescr = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Uid" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="AccCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="AccExcludeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "uid",
        "accCode",
        "accExcludeCode"
    })
    public static class ExclusiveAccessories {

        @XmlElement(name = "Uid")
        protected Integer uid;
        @XmlElement(name = "AccCode")
        protected String accCode;
        @XmlElement(name = "AccExcludeCode")
        protected String accExcludeCode;

        /**
         * Gets the value of the uid property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getUid() {
            return uid;
        }

        /**
         * Sets the value of the uid property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setUid(Integer value) {
            this.uid = value;
        }

        /**
         * Gets the value of the accCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccCode() {
            return accCode;
        }

        /**
         * Sets the value of the accCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccCode(String value) {
            this.accCode = value;
        }

        /**
         * Gets the value of the accExcludeCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccExcludeCode() {
            return accExcludeCode;
        }

        /**
         * Sets the value of the accExcludeCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccExcludeCode(String value) {
            this.accExcludeCode = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Uid" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="AccCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="AccInclCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "uid",
        "accCode",
        "accInclCode"
    })
    public static class InclusiveAccessories {

        @XmlElement(name = "Uid")
        protected Integer uid;
        @XmlElement(name = "AccCode")
        protected String accCode;
        @XmlElement(name = "AccInclCode")
        protected String accInclCode;

        /**
         * Gets the value of the uid property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getUid() {
            return uid;
        }

        /**
         * Sets the value of the uid property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setUid(Integer value) {
            this.uid = value;
        }

        /**
         * Gets the value of the accCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccCode() {
            return accCode;
        }

        /**
         * Sets the value of the accCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccCode(String value) {
            this.accCode = value;
        }

        /**
         * Gets the value of the accInclCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccInclCode() {
            return accInclCode;
        }

        /**
         * Sets the value of the accInclCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccInclCode(String value) {
            this.accInclCode = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="MakeCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="MakeDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "makeCode",
        "makeDescr"
    })
    public static class Makes {

        @XmlElement(name = "MakeCode")
        protected Integer makeCode;
        @XmlElement(name = "MakeDescr")
        protected String makeDescr;

        /**
         * Gets the value of the makeCode property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getMakeCode() {
            return makeCode;
        }

        /**
         * Sets the value of the makeCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setMakeCode(Integer value) {
            this.makeCode = value;
        }

        /**
         * Gets the value of the makeDescr property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMakeDescr() {
            return makeDescr;
        }

        /**
         * Sets the value of the makeDescr property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMakeDescr(String value) {
            this.makeDescr = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="MileageCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="Low" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="High" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="Adjustment" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="AdjustmentRate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="AdjustmentPercent" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="Descr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mileageCode",
        "low",
        "high",
        "adjustment",
        "adjustmentRate",
        "adjustmentPercent",
        "descr"
    })
    public static class MileageValues {

        @XmlElement(name = "MileageCode")
        protected Integer mileageCode;
        @XmlElement(name = "Low")
        protected Integer low;
        @XmlElement(name = "High")
        protected Integer high;
        @XmlElement(name = "Adjustment")
        protected Integer adjustment;
        @XmlElement(name = "AdjustmentRate")
        protected BigDecimal adjustmentRate;
        @XmlElement(name = "AdjustmentPercent")
        protected BigDecimal adjustmentPercent;
        @XmlElement(name = "Descr")
        protected String descr;

        /**
         * Gets the value of the mileageCode property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getMileageCode() {
            return mileageCode;
        }

        /**
         * Sets the value of the mileageCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setMileageCode(Integer value) {
            this.mileageCode = value;
        }

        /**
         * Gets the value of the low property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getLow() {
            return low;
        }

        /**
         * Sets the value of the low property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setLow(Integer value) {
            this.low = value;
        }

        /**
         * Gets the value of the high property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getHigh() {
            return high;
        }

        /**
         * Sets the value of the high property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setHigh(Integer value) {
            this.high = value;
        }

        /**
         * Gets the value of the adjustment property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getAdjustment() {
            return adjustment;
        }

        /**
         * Sets the value of the adjustment property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setAdjustment(Integer value) {
            this.adjustment = value;
        }

        /**
         * Gets the value of the adjustmentRate property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAdjustmentRate() {
            return adjustmentRate;
        }

        /**
         * Sets the value of the adjustmentRate property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAdjustmentRate(BigDecimal value) {
            this.adjustmentRate = value;
        }

        /**
         * Gets the value of the adjustmentPercent property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAdjustmentPercent() {
            return adjustmentPercent;
        }

        /**
         * Sets the value of the adjustmentPercent property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAdjustmentPercent(BigDecimal value) {
            this.adjustmentPercent = value;
        }

        /**
         * Gets the value of the descr property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescr() {
            return descr;
        }

        /**
         * Sets the value of the descr property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescr(String value) {
            this.descr = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="RegionStatesId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="RegionStatesDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RegionStateDefaultSort" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "regionStatesId",
        "regionStatesDescr",
        "regionStateDefaultSort"
    })
    public static class RegionStates {

        @XmlElement(name = "RegionStatesId")
        protected Integer regionStatesId;
        @XmlElement(name = "RegionStatesDescr")
        protected String regionStatesDescr;
        @XmlElement(name = "RegionStateDefaultSort")
        protected Integer regionStateDefaultSort;

        /**
         * Gets the value of the regionStatesId property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getRegionStatesId() {
            return regionStatesId;
        }

        /**
         * Sets the value of the regionStatesId property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setRegionStatesId(Integer value) {
            this.regionStatesId = value;
        }

        /**
         * Gets the value of the regionStatesDescr property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegionStatesDescr() {
            return regionStatesDescr;
        }

        /**
         * Sets the value of the regionStatesDescr property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegionStatesDescr(String value) {
            this.regionStatesDescr = value;
        }

        /**
         * Gets the value of the regionStateDefaultSort property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getRegionStateDefaultSort() {
            return regionStateDefaultSort;
        }

        /**
         * Sets the value of the regionStateDefaultSort property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setRegionStateDefaultSort(Integer value) {
            this.regionStateDefaultSort = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SeriesCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="SeriesDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "seriesCode",
        "seriesDescr"
    })
    public static class Series {

        @XmlElement(name = "SeriesCode")
        protected Integer seriesCode;
        @XmlElement(name = "SeriesDescr")
        protected String seriesDescr;

        /**
         * Gets the value of the seriesCode property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getSeriesCode() {
            return seriesCode;
        }

        /**
         * Sets the value of the seriesCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setSeriesCode(Integer value) {
            this.seriesCode = value;
        }

        /**
         * Gets the value of the seriesDescr property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSeriesDescr() {
            return seriesDescr;
        }

        /**
         * Sets the value of the seriesDescr property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSeriesDescr(String value) {
            this.seriesDescr = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Uid" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="Retail" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="TradeIn" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="Loan" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="RoughTradeIn" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="AvgTradeIn" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "uid",
        "retail",
        "tradeIn",
        "loan",
        "roughTradeIn",
        "avgTradeIn"
    })
    public static class VehicleValues {

        @XmlElement(name = "Uid")
        protected Integer uid;
        @XmlElement(name = "BaseRetail")
        protected Integer retail;
        @XmlElement(name = "BaseTradeIn")
        protected Integer tradeIn;
        @XmlElement(name = "BaseLoan")
        protected Integer loan;
        @XmlElement(name = "BaseRoughTradein")
        protected Integer roughTradeIn;
        @XmlElement(name = "BaseAvgTradein")
        protected Integer avgTradeIn;

        /**
         * Gets the value of the uid property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getUid() {
            return uid;
        }

        /**
         * Sets the value of the uid property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setUid(Integer value) {
            this.uid = value;
        }

        /**
         * Gets the value of the retail property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getRetail() {
            return retail;
        }

        /**
         * Sets the value of the retail property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setRetail(Integer value) {
            this.retail = value;
        }

        /**
         * Gets the value of the tradeIn property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getTradeIn() {
            return tradeIn;
        }

        /**
         * Sets the value of the tradeIn property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setTradeIn(Integer value) {
            this.tradeIn = value;
        }

        /**
         * Gets the value of the loan property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getLoan() {
            return loan;
        }

        /**
         * Sets the value of the loan property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setLoan(Integer value) {
            this.loan = value;
        }

        /**
         * Gets the value of the roughTradeIn property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getRoughTradeIn() {
            return roughTradeIn;
        }

        /**
         * Sets the value of the roughTradeIn property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setRoughTradeIn(Integer value) {
            this.roughTradeIn = value;
        }

        /**
         * Gets the value of the avgTradeIn property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getAvgTradeIn() {
            return avgTradeIn;
        }

        /**
         * Sets the value of the avgTradeIn property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setAvgTradeIn(Integer value) {
            this.avgTradeIn = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Uid" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="VehicleYear" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="MakeCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="MakeDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SeriesCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="SeriesDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="BodyCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="BodyDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MileageCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CurbWeight" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="GVW" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="GCW" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="MSRP" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "uid",
        "vehicleYear",
        "makeCode",
        "makeDescr",
        "seriesCode",
        "seriesDescr",
        "bodyCode",
        "bodyDescr",
        "mileageCode",
        "curbWeight",
        "gvw",
        "gcw",
        "msrp"
    })
    public static class Vehicles {

        @XmlElement(name = "Uid")
        protected Integer uid;
        @XmlElement(name = "VehicleYear")
        protected Integer vehicleYear;
        @XmlElement(name = "MakeCode")
        protected Integer makeCode;
        @XmlElement(name = "MakeDescr")
        protected String makeDescr;
        @XmlElement(name = "SeriesCode")
        protected Integer seriesCode;
        @XmlElement(name = "SeriesDescr")
        protected String seriesDescr;
        @XmlElement(name = "BodyCode")
        protected Integer bodyCode;
        @XmlElement(name = "BodyDescr")
        protected String bodyDescr;
        @XmlElement(name = "MileageCode")
        protected String mileageCode;
        @XmlElement(name = "CurbWeight")
        protected Integer curbWeight;
        @XmlElement(name = "GVW")
        protected Integer gvw;
        @XmlElement(name = "GCW")
        protected Integer gcw;
        @XmlElement(name = "MSRP")
        protected Integer msrp;

        /**
         * Gets the value of the uid property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getUid() {
            return uid;
        }

        /**
         * Sets the value of the uid property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setUid(Integer value) {
            this.uid = value;
        }

        /**
         * Gets the value of the vehicleYear property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getVehicleYear() {
            return vehicleYear;
        }

        /**
         * Sets the value of the vehicleYear property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setVehicleYear(Integer value) {
            this.vehicleYear = value;
        }

        /**
         * Gets the value of the makeCode property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getMakeCode() {
            return makeCode;
        }

        /**
         * Sets the value of the makeCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setMakeCode(Integer value) {
            this.makeCode = value;
        }

        /**
         * Gets the value of the makeDescr property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMakeDescr() {
            return makeDescr;
        }

        /**
         * Sets the value of the makeDescr property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMakeDescr(String value) {
            this.makeDescr = value;
        }

        /**
         * Gets the value of the seriesCode property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getSeriesCode() {
            return seriesCode;
        }

        /**
         * Sets the value of the seriesCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setSeriesCode(Integer value) {
            this.seriesCode = value;
        }

        /**
         * Gets the value of the seriesDescr property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSeriesDescr() {
            return seriesDescr;
        }

        /**
         * Sets the value of the seriesDescr property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSeriesDescr(String value) {
            this.seriesDescr = value;
        }

        /**
         * Gets the value of the bodyCode property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getBodyCode() {
            return bodyCode;
        }

        /**
         * Sets the value of the bodyCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setBodyCode(Integer value) {
            this.bodyCode = value;
        }

        /**
         * Gets the value of the bodyDescr property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBodyDescr() {
            return bodyDescr;
        }

        /**
         * Sets the value of the bodyDescr property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBodyDescr(String value) {
            this.bodyDescr = value;
        }

        /**
         * Gets the value of the mileageCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMileageCode() {
            return mileageCode;
        }

        /**
         * Sets the value of the mileageCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMileageCode(String value) {
            this.mileageCode = value;
        }

        /**
         * Gets the value of the curbWeight property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getCurbWeight() {
            return curbWeight;
        }

        /**
         * Sets the value of the curbWeight property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setCurbWeight(Integer value) {
            this.curbWeight = value;
        }

        /**
         * Gets the value of the gvw property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getGVW() {
            return gvw;
        }

        /**
         * Sets the value of the gvw property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setGVW(Integer value) {
            this.gvw = value;
        }

        /**
         * Gets the value of the gcw property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getGCW() {
            return gcw;
        }

        /**
         * Sets the value of the gcw property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setGCW(Integer value) {
            this.gcw = value;
        }

        /**
         * Gets the value of the msrp property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getMSRP() {
            return msrp;
        }

        /**
         * Sets the value of the msrp property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setMSRP(Integer value) {
            this.msrp = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="VehicleYear" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="VehicleYearDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "vehicleYear",
        "vehicleYearDescr"
    })
    public static class Years {

        @XmlElement(name = "VehicleYear")
        protected Integer vehicleYear;
        @XmlElement(name = "VehicleYearDescr")
        protected String vehicleYearDescr;

        /**
         * Gets the value of the vehicleYear property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getVehicleYear() {
            return vehicleYear;
        }

        /**
         * Sets the value of the vehicleYear property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setVehicleYear(Integer value) {
            this.vehicleYear = value;
        }

        /**
         * Gets the value of the vehicleYearDescr property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVehicleYearDescr() {
            return vehicleYearDescr;
        }

        /**
         * Sets the value of the vehicleYearDescr property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVehicleYearDescr(String value) {
            this.vehicleYearDescr = value;
        }

    }


	public List<Accessories> getAccessories() {
		if(accessories == null)
			this.accessories = new ArrayList<Accessories>();
		return this.accessories;
	}


	public void setAccessories(List<Accessories> accessories) {
		this.accessories = accessories;
	}


	public List<Bodies> getBodies() {
		if(bodies == null)
			this.bodies = new ArrayList<Bodies>();
		return bodies;
	}


	public void setBodies(List<Bodies> bodies) {
		this.bodies = bodies;
	}


	public List<ExclusiveAccessories> getExclusiveAccessories() {
		if(exclusiveAccessories == null)
			this.exclusiveAccessories = new ArrayList<ExclusiveAccessories>();
		return exclusiveAccessories;
	}


	public void setExclusiveAccessories(
			List<ExclusiveAccessories> exclusiveAccessories) {
		this.exclusiveAccessories = exclusiveAccessories;
	}


	public List<InclusiveAccessories> getInclusiveAccessories() {
		if(inclusiveAccessories == null)
			this.inclusiveAccessories = new ArrayList<InclusiveAccessories>();
		return inclusiveAccessories;
	}


	public void setInclusiveAccessories(
			List<InclusiveAccessories> inclusiveAccessories) {
		this.inclusiveAccessories = inclusiveAccessories;
	}


	public List<Makes> getMakes() {
		if(makes == null)
			this.makes = new ArrayList<Makes>();
		return makes;
	}


	public void setMakes(List<Makes> makes) {
		this.makes = makes;
	}


	public List<MileageValues> getMileageValues() {
		if(mileageValues == null)
			this.mileageValues = new ArrayList<MileageValues>();
		return mileageValues;
	}


	public void setMileageValues(List<MileageValues> mileageValues) {
		this.mileageValues = mileageValues;
	}


	public List<RegionStates> getRegionStates() {
		if(regionStates == null)
			this.regionStates = new ArrayList<RegionStates>();
		return regionStates;
	}


	public void setRegionStates(List<RegionStates> regionStates) {
		this.regionStates = regionStates;
	}


	public List<Series> getSeries() {
		if(series == null)
			this.series = new ArrayList<Series>();
		return series;
	}


	public void setSeries(List<Series> series) {
		this.series = series;
	}


	public List<Vehicles> getVehicles() {
		if(vehicles == null)
			this.vehicles = new ArrayList<Vehicles>();
		return vehicles;
	}


	public void setVehicles(List<Vehicles> vehicles) {
		 this.vehicles = vehicles;
	}


	public List<VehicleValues> getVehicleValues() {
		if(vehicleValues == null)
			this.vehicleValues = new ArrayList<VehicleValues>();
		return vehicleValues;
	}


	public void setVehicleValues(List<VehicleValues> vehicleValues) {
		this.vehicleValues = vehicleValues;
	}


	public List<Years> getYears() {
		if(years == null)
			this.years = new ArrayList<Years>();
		return years;
	}


	public void setYears(List<Years> years) {
		this.years = years;
	}

}
