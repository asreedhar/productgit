
package com.firstlook.thirdparty.book.newnada;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="trimId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="regionId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "trimId",
    "regionId"
})
@XmlRootElement(name = "GetEquipmentOptions_NoVin")
public class GetEquipmentOptionsNoVin {

    protected int trimId;
    protected int regionId;

    /**
     * Gets the value of the trimId property.
     * 
     */
    public int getTrimId() {
        return trimId;
    }

    /**
     * Sets the value of the trimId property.
     * 
     */
    public void setTrimId(int value) {
        this.trimId = value;
    }

    /**
     * Gets the value of the regionId property.
     * 
     */
    public int getRegionId() {
        return regionId;
    }

    /**
     * Sets the value of the regionId property.
     * 
     */
    public void setRegionId(int value) {
        this.regionId = value;
    }

}
