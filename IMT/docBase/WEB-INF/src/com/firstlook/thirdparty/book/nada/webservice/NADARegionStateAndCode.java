/**
 * NADARegionStateAndCode.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package com.firstlook.thirdparty.book.nada.webservice;

public class NADARegionStateAndCode  implements java.io.Serializable {
    private static final long serialVersionUID = -6308967597347218181L;
	private java.lang.String regionOrStateName;
    private int regionCode;

    public NADARegionStateAndCode() {
    }

    public NADARegionStateAndCode(
           java.lang.String regionOrStateName,
           int regionCode) {
           this.regionOrStateName = regionOrStateName;
           this.regionCode = regionCode;
    }


    /**
     * Gets the regionOrStateName value for this NADARegionStateAndCode.
     * 
     * @return regionOrStateName
     */
    public java.lang.String getRegionOrStateName() {
        return regionOrStateName;
    }


    /**
     * Sets the regionOrStateName value for this NADARegionStateAndCode.
     * 
     * @param regionOrStateName
     */
    public void setRegionOrStateName(java.lang.String regionOrStateName) {
        this.regionOrStateName = regionOrStateName;
    }


    /**
     * Gets the regionCode value for this NADARegionStateAndCode.
     * 
     * @return regionCode
     */
    public int getRegionCode() {
        return regionCode;
    }


    /**
     * Sets the regionCode value for this NADARegionStateAndCode.
     * 
     * @param regionCode
     */
    public void setRegionCode(int regionCode) {
        this.regionCode = regionCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof NADARegionStateAndCode)) return false;
        NADARegionStateAndCode other = (NADARegionStateAndCode) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.regionOrStateName==null && other.getRegionOrStateName()==null) || 
             (this.regionOrStateName!=null &&
              this.regionOrStateName.equals(other.getRegionOrStateName()))) &&
            this.regionCode == other.getRegionCode();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRegionOrStateName() != null) {
            _hashCode += getRegionOrStateName().hashCode();
        }
        _hashCode += getRegionCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(NADARegionStateAndCode.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "NADARegionStateAndCode"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("regionOrStateName");
        elemField.setXmlName(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "regionOrStateName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("regionCode");
        elemField.setXmlName(new javax.xml.namespace.QName("webservice.nada.firstlook.com", "regionCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
