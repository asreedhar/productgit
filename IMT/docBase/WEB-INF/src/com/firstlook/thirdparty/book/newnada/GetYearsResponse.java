
package com.firstlook.thirdparty.book.newnada;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.firstlook.thirdparty.book.newnada.data.VehicleUIData;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetYearsResult" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="VehicleUI_Data" type="{http://www.firstlook.biz/NadaWebService}VehicleUI_Data" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getYearsResult"
})
@XmlRootElement(name = "GetYearsResponse")
public class GetYearsResponse {

    @XmlElement(name = "GetYearsResult")
    protected GetYearsResponse.GetYearsResult getYearsResult;

    /**
     * Gets the value of the getYearsResult property.
     * 
     * @return
     *     possible object is
     *     {@link GetYearsResponse.GetYearsResult }
     *     
     */
    public GetYearsResponse.GetYearsResult getGetYearsResult() {
        return getYearsResult;
    }

    /**
     * Sets the value of the getYearsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetYearsResponse.GetYearsResult }
     *     
     */
    public void setGetYearsResult(GetYearsResponse.GetYearsResult value) {
        this.getYearsResult = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="VehicleUI_Data" type="{http://www.firstlook.biz/NadaWebService}VehicleUI_Data" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "vehicleUIData"
    })
    public static class GetYearsResult {

        @XmlElementRef(name = "VehicleUI_Data", namespace = "NADAVehicle_Data", type = VehicleUIData.class)
        protected VehicleUIData vehicleUIData;

		public VehicleUIData getVehicleUIData() {
			return vehicleUIData;
		}

		public void setVehicleUIData(VehicleUIData vehicleUIData) {
			this.vehicleUIData = vehicleUIData;
		}

    }

}
