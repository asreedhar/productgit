package com.firstlook.thirdparty.book.newnada.client;

import java.lang.reflect.Proxy;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.List;

import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.codehaus.xfire.client.Client;
import org.codehaus.xfire.client.XFireProxy;
import org.codehaus.xfire.transport.http.EasySSLProtocolSocketFactory;
import org.codehaus.xfire.util.dom.DOMInHandler;
import org.codehaus.xfire.util.dom.DOMOutHandler;

import com.firstlook.thirdparty.book.newnada.DoVinLookupResponse.DoVinLookupResult;
import com.firstlook.thirdparty.book.newnada.GetEquipmentOptionsNoVinResponse.GetEquipmentOptionsNoVinResult;
import com.firstlook.thirdparty.book.newnada.GetEquipmentOptionsResponse.GetEquipmentOptionsResult;
import com.firstlook.thirdparty.book.newnada.GetMakesResponse.GetMakesResult;
import com.firstlook.thirdparty.book.newnada.GetModelResponse.GetModelResult;
import com.firstlook.thirdparty.book.newnada.GetMutuallyExclusiveOptionsListResponse.GetMutuallyExclusiveOptionsListResult;
import com.firstlook.thirdparty.book.newnada.GetPackageIncludedOptionsListResponse.GetPackageIncludedOptionsListResult;
import com.firstlook.thirdparty.book.newnada.GetTrimResponse.GetTrimResult;
import com.firstlook.thirdparty.book.newnada.GetValuesNoVinResponse.GetValuesNoVinResult;
import com.firstlook.thirdparty.book.newnada.GetValuesResponse.GetValuesResult;
import com.firstlook.thirdparty.book.newnada.GetYearsResponse.GetYearsResult;
import com.firstlook.thirdparty.book.newnada.data.VehicleUIData;
import com.firstlook.thirdparty.book.newnada.data.VehicleUIData.Accessories;
import com.firstlook.thirdparty.book.newnada.data.VehicleUIData.ExclusiveAccessories;
import com.firstlook.thirdparty.book.newnada.data.VehicleUIData.InclusiveAccessories;
import com.firstlook.thirdparty.book.newnada.data.VehicleUIData.Makes;

public class NadaWebServiceClientTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Protocol protocol = new Protocol("https", (ProtocolSocketFactory) new EasySSLProtocolSocketFactory(), 443);
		Protocol.registerProtocol("https", protocol);
		
		NadaWebServiceClient nadaWebServiceClient = new NadaWebServiceClient();
		NadaWebServiceSoap service = nadaWebServiceClient.getNadaWebServiceSoap();
		
		Client client =  ((XFireProxy)
				Proxy.getInvocationHandler(service)).getClient();
		client.addInHandler(new DOMInHandler());
		client.addInHandler(new NADADirectResponseHandler());
		
		client.addOutHandler(new DOMOutHandler());
		client.addOutHandler(new RequestLoggingHandler());
		client.setUrl("http://localhost:4997/NadaWebService.asmx");
		
		System.out.println("calling do vin lookup");
		String vin = "3GNEC16Z75G282222"; //"2C3KA63H07H612199"; //"1G1YY26E265101992";
		DoVinLookupResult data = service.doVinLookup(vin);
		
		int uid = data.getVehicleUIData().getVehicles().get(0).getUid();
		int vehicleYear = data.getVehicleUIData().getVehicles().get(0).getVehicleYear();

		System.out.println();
		System.out.println("Get All Years");
		GetYearsResult getYearResults = service.getYears();
		int year = getYearResults.getVehicleUIData().getYears().get(0).getVehicleYear();
		
		System.out.println("Get All Makes");
		GetMakesResult getMakesResults = service.getMakes(vehicleYear);
		
		int makeId = getMakesResults.getVehicleUIData().getMakes().get(0).getMakeCode();
		System.out.println("Get All Model");
		GetModelResult getModelResults = service.getModel(year, makeId);
		
		int modelId = getModelResults.getVehicleUIData().getSeries().get(0).getSeriesCode();
		System.out.println("Get All Trims");
		GetTrimResult getTrimResult = service.getTrim(year, makeId, modelId);
		
		System.out.println("Get options for VIN");
		GetEquipmentOptionsResult optionsResults = service.getEquipmentOptions(1, uid, vin);
		
		int trimId = getTrimResult.getVehicleUIData().getBodies().get(0).getBodyCode();
		System.out.println("Get EquipmentOptionsNoVinResults");
		GetEquipmentOptionsNoVinResult getEquipmentOptionsNoVinResult = service.getEquipmentOptions_NoVin(trimId, 1);
		
		System.out.println("Get Period");
		String getNADAPublishPeriodResponse = service.getNADAPublishPeriod();
		
		System.out.println("Get Vehicle Values");
		GetValuesResult getValuesResult = service.getValues(uid, 50000, 1);
		
		System.out.println("Get Vehicle Values No Vin");
		GetValuesNoVinResult getValuesNoVinResult = service.getValues_NoVin(trimId, 1, 50000);
		
		System.out.println("Get Mileage Adjustment Value");
		BigDecimal mileageAdj = service.getMileageAdjustment(uid, 50000, 1);
		
		System.out.println("Get Package Inclusive Options");
		GetPackageIncludedOptionsListResult packageInclusionResult = service.getPackageIncludedOptionsList(uid);
		
		System.out.println("Get MutuallyExclusive Options");
		GetMutuallyExclusiveOptionsListResult mutuallyExclusiveOptionsListResult = 
			service.getMutuallyExclusiveOptionsList(uid);
		
		System.out.println(MessageFormat.format("Model Year:{0}", Integer.valueOf(year).toString()));
		printMakes(getMakesResults);
		printEquipmentOptions(optionsResults);
		printMutuallyExclusiveOptions(mutuallyExclusiveOptionsListResult);
		printInclusiveEquipmentOptions(packageInclusionResult);

		System.out.println("finished");
	}

	private static void printMakes(GetMakesResult result) {
		VehicleUIData data = result.getVehicleUIData();
		List<Makes> makes = data.getMakes();
		System.out.println("======== Makes ========");
		for(Makes make : makes) {
			System.out.println(MessageFormat.format("Code:{0}, Description:{1}", make.getMakeCode(), make.getMakeDescr()));
		}
		System.out.println("-----------------------");
	}
	
	private static void printEquipmentOptions(GetEquipmentOptionsResult result) {
		VehicleUIData data = result.getVehicleUIData();
		List<Accessories> accessories = data.getAccessories();
		System.out.println("======== Accessories ========");
		for(Accessories accessory : accessories) {
			System.out.println(
					MessageFormat.format("AccCode:{0}, AccDesc:{1}, AccIsIncluded:{2}, AccIsAdded:{3}, AccTradeIn:{4,number,currency}, AccRetail:{5,number,currency}, AccLoan:{6,number,currency}", 
							accessory.getAccCode(), accessory.getAccCodeDescr(), accessory.isAccIsIncluded(), accessory.isAccIsAdded(), accessory.getAccTradeIn(), accessory.getAccRetail(), accessory.getAccLoan()));
		}
		System.out.println("-----------------------------");
	}
	
	private static void printInclusiveEquipmentOptions(GetPackageIncludedOptionsListResult result) {
		if(result != null) {
			VehicleUIData data = result.getVehicleUIData();
			if(data != null) {
				List<InclusiveAccessories> accessories = data.getInclusiveAccessories();
				System.out.println("======== Included Accessories ========");
				for(InclusiveAccessories accessory : accessories) {
					System.out.println(MessageFormat.format("AccCode:{0}, AccIncl:{1}", accessory.getAccCode(), accessory.getAccInclCode()));
				}
				System.out.println("--------------------------------------");
			}
		}
	}
	
	private static void printMutuallyExclusiveOptions(GetMutuallyExclusiveOptionsListResult result) {
		if(result != null) {
			VehicleUIData data = result.getVehicleUIData();
			if(data != null) {
				List<ExclusiveAccessories> accessories = data.getExclusiveAccessories();
				System.out.println("======== Exclusive Accessories ========");
				for(ExclusiveAccessories accessory : accessories) {
					System.out.println(MessageFormat.format("AccCode:{0}, AccIncl:{1}", accessory.getAccCode(), accessory.getAccExcludeCode()));
				}
				System.out.println("---------------------------------------");
			}
		}
	}
}
