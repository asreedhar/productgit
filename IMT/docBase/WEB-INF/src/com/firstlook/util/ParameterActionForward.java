package com.firstlook.util;

import java.util.HashMap;

import org.apache.struts.action.ActionForward;

public final class ParameterActionForward extends ActionForward {

	private static final long serialVersionUID = 8384383223134700997L;

	private static final String questionMark = "?";

	private static final String ampersand = "&";

	private static final String equals = "=";

	private HashMap<String,String> parameters = new HashMap<String,String>();

	public ParameterActionForward(ActionForward forward) {
		setName(forward.getName());
		setPath(forward.getPath());
		setRedirect(forward.getRedirect());
		// setContextRelative(forward.getContextRelative());
	}

	public String getPath() {

		StringBuffer sb = new StringBuffer(super.getPath());
		
		boolean firstTimeThrough = true;

		if (parameters != null && !parameters.isEmpty()) {
			
			if (sb.indexOf(questionMark) >= 0) {
				if (sb.charAt(sb.length()-1) != '?') {
					sb.append(ampersand);
				}
			}
			else {
				sb.append(questionMark);
			}

			for (String paramName : parameters.keySet()) {
				
				String paramValue = parameters.get(paramName);

				if (firstTimeThrough) {
					firstTimeThrough = false;
				}
				else {
					sb.append(ampersand);
				}

				sb.append(paramName);
				sb.append(equals);
				sb.append(paramValue);
			}
		}

		return sb.toString();
	}

	public void addParameter(String paramName, String paramValue) {
		parameters.put(paramName, paramValue);
	}

}
