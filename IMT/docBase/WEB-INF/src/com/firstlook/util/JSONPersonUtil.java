package com.firstlook.util;

import java.util.Collection;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import biz.firstlook.transact.persist.person.IPerson;
import biz.firstlook.transact.persist.person.IPosition;

public abstract class JSONPersonUtil {
	
	private static Logger logger = Logger.getLogger( JSONPersonUtil.class );
		
	public static JSONArray convertPeopleToJSON( Collection< IPerson > c )
	{
		JSONArray json = new JSONArray();
		try
		{
			Iterator<IPerson> iter = c.iterator();
			while ( iter.hasNext() )  //convert each person to a json object and place on the json array
			{
				IPerson p = iter.next();
				json.put( convertPersonToJSON( p ) );

			}
		}
		catch ( JSONException e )
		{
			logger.error( "Error creating people list JSON" );

		}
		return json;
	}

	public static JSONObject convertPersonToJSON( IPerson p ) throws JSONException
	{

		JSONObject json = new JSONObject();
		json.put( "personId", p.getPersonId() );
		json.put( "firstName", p.getFirstName() );
		json.put( "lastName", p.getLastName() );
		json.put( "positions", convertPositionsToJSON( p.getPositions() ) );

		return json;

	}

	public static JSONArray convertPositionsToJSON( Collection<IPosition> c ) throws JSONException
	{
		JSONArray jsonArray = new JSONArray(); 
		JSONObject json;
		try
		{
			Iterator<IPosition> iter = c.iterator();
			while ( iter.hasNext() )  //create position objects and place them on the postion json array
			{
				IPosition p = (IPosition)iter.next();
				json = new JSONObject();
				json.put( "positionId", p.getPositionId() );
				jsonArray.put( json );
			}
		}
		catch ( JSONException e )
		{
			logger.error( "Error creating position list JSON" );

		}
		return jsonArray;
	}	
}
