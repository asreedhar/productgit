package com.firstlook.util;

import com.firstlook.mock.BaseTestCase;

public class TestMathUtil extends BaseTestCase
{

public TestMathUtil( String arg1 )
{
    super(arg1);
}

public void testIntegerToDoubleNull()
{
    assertTrue("should be NaN", Double.isNaN(MathUtil.toDouble(null)));
}

public void testIntegerToDouble()
{
    Integer value = new Integer(50);

    assertEquals("should be 50.0", 50.0, MathUtil.toDouble(value), 0);
}

public void testToIntPercent()
{
    double value = .25;

    assertEquals("Value should be 25", 25, MathUtil.toIntPercent(value));
}

public void testToIntPercentRoundingUp()
{
    double value = .256;

    assertEquals("Value should be 26", 26, MathUtil.toIntPercent(value));
}

public void testToIntPercentRoundingDown()
{
    double value = .254;

    assertEquals("Value should be 25", 25, MathUtil.toIntPercent(value));
}

public void testToIntPercentNan()
{
    double value = Double.NaN;

    assertEquals("Value should be MIN_VALUE", Integer.MIN_VALUE, MathUtil
            .toIntPercent(value));
}

public void testToRoundedTwoPlaceDecimalRoundUp()
{
    double value = 1.3351;
    double result = MathUtil.toRoundedTwoPlaceDecimal(value);

    assertEquals("Should be 1.34", 1.34, result, 0);
}

public void testToRoundedTwoPlaceDecimalRoundDown()
{
    double value = 1.334;
    double result = MathUtil.toRoundedTwoPlaceDecimal(value);

    assertEquals("Should be 1.33", 1.33, result, 0);
}
/**
 * Takes the input value and rounds it to the nearest 25.
 * Eg: 	19.27 = 25.00
 * 		35870.00 = 35875.00
 * 		0.5484	= 0.00
 * 		12.45 	= 0.00
 * 		12.50	= 25.00
 **/
public void testRoundToNearestTwentyFive() 
{
	double input = 19.27;
	assertEquals("Should be 25", 25, MathUtil.roundToNearestTwentyFive(input), 0);

	input = 35870.00;
	assertEquals("Should be 35875", 35875, MathUtil.roundToNearestTwentyFive(input), 0);

	input = 0.5484;
	assertEquals("Should be 0", 0, MathUtil.roundToNearestTwentyFive(input), 0);

	input = 12.45;
	assertEquals("Should be 0", 0, MathUtil.roundToNearestTwentyFive(input), 0);

	input = 12.5;
	assertEquals("Should be 25", 25, MathUtil.roundToNearestTwentyFive(input), 0);

	input = 362.49999;
	assertEquals("Should be 350", 350, MathUtil.roundToNearestTwentyFive(input), 0);

	input = 362.5;
	assertEquals("Should be 375", 375, MathUtil.roundToNearestTwentyFive(input), 0);

	input = 663.46;
	assertEquals("Should be 675", 675, MathUtil.roundToNearestTwentyFive(input), 0);

	input = -663.46;
	assertEquals("Should be -675", -675, MathUtil.roundToNearestTwentyFive(input), 0);

}


}
