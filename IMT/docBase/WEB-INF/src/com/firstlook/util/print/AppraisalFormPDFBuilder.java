package com.firstlook.util.print;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.Context;
import javax.naming.NamingException;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.services.princexml.v1.ContentType;
import biz.firstlook.services.princexml.v1.Input;
import biz.firstlook.services.princexml.v1.OutputType;
import biz.firstlook.services.princexml.v1.OutputTypeCode;
import biz.firstlook.services.princexml.v1.PrinceXmlRequest;
import biz.firstlook.services.princexml.v1.PrinceXmlResponse;
import biz.firstlook.services.princexml.v1.PrinceXmlWebService;
import biz.firstlook.services.princexml.v1.PrinceXmlWebServiceClient;
import biz.firstlook.transact.persist.model.Photo;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.persistence.IThirdPartyCategoryDAO;
import biz.firstlook.transact.persist.persistence.ThirdPartyCategoryDAO;
import biz.firstlook.transact.persist.service.appraisal.AppraisalFormOptions;
import biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum;
import biz.firstlook.transact.persist.service.appraisal.Customer;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;

import com.firstlook.dealer.tools.GuideBook;
import com.firstlook.display.tradeanalyzer.DisplayGuideBook;
import com.firstlook.email.PhoneNumber;
import com.firstlook.email.PhoneNumberFormatException;
import com.firstlook.entity.Dealer;
import com.firstlook.util.Formatter;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Cell;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Graphic;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPCellEvent;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPTableEvent;
import com.lowagie.text.pdf.PdfWriter;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class AppraisalFormPDFBuilder {

	private static final Logger logger = Logger
			.getLogger(AppraisalFormPDFBuilder.class);

	// for the photo root we don't want to hit https (we don't need to and it
	// caused some keystore problems)
	private static final String PHOTO_LOC = PropertyLoader
			.getProperty("firstlook.photos.web.root.http");
	private static final String CHECK_BOX_IMAGE = "/common/_images/icons/checkBox.gif";
	private static final String CHECK_BG_IMAGE = "/common/_images/icons/check_bkgrd.gif";
	private static final Font HELVET_22_BOLD = new Font(Font.HELVETICA, 22,
			Font.BOLD);
	private static final Font HELVET_21_BOLD = new Font(Font.HELVETICA, 21,
			Font.BOLD);
	private static final Font HELVET_19_BOLD = new Font(Font.HELVETICA, 19,
			Font.BOLD);
	private static final Font HELVET_16_BOLD = new Font(Font.HELVETICA, 16,
			Font.BOLD);
	private static final Font HELVET_15_BOLD = new Font(Font.HELVETICA, 15,
			Font.BOLD);
	private static final Font HELVET_15_BOLD_GREY = new Font(Font.HELVETICA, 15,
			Font.BOLD,new Color(158,158,170));
	
	private static final Font HELVET_15_BOLD_BLUE = new Font(Font.HELVETICA, 15,
			Font.BOLD,new Color(52,96,171));
	
	private static final Font HELVET_14_BOLD = new Font(Font.HELVETICA, 14,
			Font.BOLD);
	private static final Font HELVET_12_BOLD = new Font(Font.HELVETICA, 12,
			Font.BOLD);
	private static final Font HELVET_12_BOLD_BLUE = new Font(Font.HELVETICA, 12,
			Font.BOLD,new Color(52,96,171));
	private static final Font HELVET_11_BOLD = new Font(Font.HELVETICA, 11,
			Font.BOLD);
	private static final Font HELVET_10_BOLD = new Font(Font.HELVETICA, 10,
			Font.BOLD);
	private static final Font HELVET_10_NORMAL = new Font(Font.HELVETICA, 10,
			Font.NORMAL);
	private static final Font HELVET_10_NORMAL_GREY = new Font(Font.HELVETICA, 10,
			Font.NORMAL,new Color(158,158,170));
	private static final Font HELVET_8_NORMAL = new Font(Font.HELVETICA, 8,
			Font.NORMAL);
	private static final Font HELVET_8_NORMAL_GREY = new Font(Font.HELVETICA, 8,
			Font.NORMAL,new Color(158,158,170));
	private static final Font HELVET_8_NORMAL_GREEN = new Font(Font.HELVETICA, 8,
			Font.NORMAL,new Color(20,175,48));
	private static final Font HELVET_8_NORMAL_RED = new Font(Font.HELVETICA, 8,
			Font.NORMAL,new Color(175,23,22));
	private static final Font HELVET_8_BOLD= new Font(Font.HELVETICA, 8,
			Font.BOLD);
	
	private static final Font HELVET_8_BOLD_GREY= new Font(Font.HELVETICA, 8,
			Font.BOLD,new Color(158,158,170));
	private static final Font HELVET_6_NORMAL = new Font(Font.HELVETICA, 6,
			Font.NORMAL);
	private static final Font HELVET_6_NORMAL_GREY = new Font(Font.HELVETICA, 6,
			Font.NORMAL,new Color(158,158,170));
	private static final Color TABLE_BG_COLOR = new Color(205, 226, 244);
	private static final Color TABLE_BG_COLOR2 = new Color(255, 255, 255);
	private static final String EMPTY_STRING = "";
	private static final Integer LOGO_WIDTH = 80;

	private IThirdPartyCategoryDAO thirdPartyCategoryDAO;
	public IThirdPartyCategoryDAO getThirdPartyCategoryDAO() {
		return thirdPartyCategoryDAO;
	}

	public void setThirdPartyCategoryDAO(
			IThirdPartyCategoryDAO thirdPartyCategoryDAO) {
		this.thirdPartyCategoryDAO = thirdPartyCategoryDAO;
	}
	private IAppraisal appraisal;
	private AppraisalFormOptions appraisalFormOptions;
	private Dealer dealer;
	private Vehicle vehicle;
	private Customer customer;
	private StringBuilder stringBuilder;
	private String realPath;
	private String currentDate;
	private String expireDate;
	private String dealerLogoURL;
	/**
	 * This constructor is being used in test only!
	 * 
	 * @param appraisal
	 */
	protected AppraisalFormPDFBuilder(IAppraisal appraisal) {
		this.appraisal = appraisal;
		dealer = new Dealer();
		this.realPath = EMPTY_STRING;
	}

	public AppraisalFormPDFBuilder(IAppraisal appraisal, Dealer dealer,
			Vehicle vehicle, Customer customer,
			AppraisalFormOptions appraisalFormOptions, String realPath) {
		this.appraisal = appraisal;
		this.dealer = dealer;
		this.vehicle = vehicle;
		this.customer = customer;
		this.appraisalFormOptions = appraisalFormOptions;
		this.realPath = realPath;
		thirdPartyCategoryDAO = new ThirdPartyCategoryDAO();
	}

	@SuppressWarnings("deprecation")
	public String buildAppraisalFormPDF(ByteArrayOutputStream baos,
			boolean showEquipmentOptions, boolean showPhotos,
			Integer[] categoriesToDisplay, DisplayGuideBook dgb1,
			DisplayGuideBook dgb2, String dealerLogoUrl,
			boolean includeMileageAdjustment, Integer kbbConsumerValue,
			boolean showCheck) {

		Document doc = new Document(PageSize.A4, 20, 20, 20, 20);

		Integer offer = null;
		float infoHeight, offsetHeight = 0;

		if (appraisalFormOptions != null) {
			offer = appraisalFormOptions.getAppraisalFormOffer();
		}

		Date now = new Date();
		Date offerExpirationDate = new Date();
		offerExpirationDate.setDate(now.getDate()
				+ dealer.getDealerPreference().getAppraisalFormValidDate());
		DateFormat dateFormatter = DateFormat.getDateInstance(DateFormat.SHORT);
		DateFormat dateTimeFormatter = DateFormat.getDateTimeInstance(
				DateFormat.SHORT, DateFormat.SHORT);
		String currentDate = dateFormatter.format(now);
		String currentDateTime = dateTimeFormatter.format(now);
		String expireDate = dateFormatter.format(offerExpirationDate);

		boolean showValues = false;

		Graphic horizontalLine = new Graphic();
		horizontalLine.setHorizontalLine(1f, 100f);
		try {
			PdfWriter writer = PdfWriter.getInstance(doc, baos);

			doc.open();

			// Top Section

			topSectionBuilder(doc, writer, dealerLogoUrl);

			// Customer Section
			String customerFirstName = convertNullToEmptyString(customer,
					"firstName");
			String customerLastName = convertNullToEmptyString(customer,
					"lastName");
			String customerPhoneNumber = convertNullToEmptyString(customer,
					"phoneNumber");
			String customerEmail = convertNullToEmptyString(customer, "email");
			customerSectionBuilder(doc, customerFirstName, customerLastName,
					customerPhoneNumber, customerEmail);

			// Info Section
			infoHeight = infoSectionBuilder(doc, writer, currentDateTime,
					expireDate, offer, dgb1, showEquipmentOptions);
			offsetHeight = infoHeight - 94;

			// Photo Section
			if (showPhotos) {
				doc.add(horizontalLine);
				showPhotos = photoSection(doc, writer);
			}

			if ((categoriesToDisplay != null && categoriesToDisplay.length > 0 && (dgb1 != null || dgb2 != null))
					|| (kbbConsumerValue != null)) {
				doc.add(horizontalLine);
				bookValueSection(doc, writer, dgb1, dgb2, categoriesToDisplay,
						includeMileageAdjustment, kbbConsumerValue);
				showValues = true;
			}

			// Vehicle Representation Section
			doc.add(horizontalLine);
			vehicleRepresentationSection(doc, writer);

			// Check Section /
			if (showCheck) {
				checkSectionBuilder(doc, writer, currentDate, expireDate,
						offer, showValues, showPhotos, showEquipmentOptions,
						offsetHeight);
			}

			doc.close();

		} catch (DocumentException e) {
			logger.error(e);
			return "creating the pdf";
		} catch (MalformedURLException e) {
			logger.error(e);
			return "loading the images";
		} catch (IOException e) {
			logger.error(e);
			return "loading the images";
		} catch (NullPointerException e) {
			logger.error(e);
			return e.getMessage();
		}

		return null;
	}

	public String buildAppraisalFormPDFWithPrinceXML(ByteArrayOutputStream baos,
			boolean showEquipmentOptions, boolean showPhotos,
			Integer[] categoriesToDisplay, DisplayGuideBook dgb1,
			DisplayGuideBook dgb2, String dealerLogoUrl,
			boolean includeMileageAdjustment, Integer kbbConsumerValue,
			boolean showCheck,boolean showReconCost,FreeMarkerConfigurer configurer){
			
			Date offerExpirationDate = new Date();
			offerExpirationDate.setDate((new Date()).getDate()	+ dealer.getDealerPreference().getAppraisalFormValidDate());
			SimpleDateFormat sdf= new SimpleDateFormat("MM-dd-yyyy");
			expireDate=sdf.format(offerExpirationDate);
			Integer offer=null;
			if(appraisal!=null&&appraisal.getAppraisalFormOptions()!=null)
			offer=appraisal.getAppraisalFormOptions().getAppraisalFormOffer();
			String serverURLImage="",serverURLBookImage;
			String server="",princeXMLServiceEndpoint="";
			Context initialContext ;
			Context ctx;
			try {
				initialContext = new javax.naming.InitialContext(); 
				ctx = (Context) initialContext.lookup("java:comp/env");
				server = (String) ctx.lookup("ServerName");
			} catch (NamingException e1) {
			
				e1.printStackTrace();
			}
	    	
	    	
	    	
	    	if (server.equalsIgnoreCase("prod")){
	    		serverURLImage="https://max.firstlook.biz";
	    		serverURLBookImage="https://max.firstlook.biz/";
	    		princeXMLServiceEndpoint="https://www.firstlook.biz/firstlook-queue-ws/services/PrinceXmlWebService";
	    	}
			else if (server.equalsIgnoreCase("beta")){
				serverURLImage="https://beta.firstlook.biz/";
				serverURLBookImage="https://betamax.firstlook.biz/";
				princeXMLServiceEndpoint="https://beta.firstlook.biz/firstlook-queue-ws/services/PrinceXmlWebService";
			}
			else if (server.equalsIgnoreCase("intb")){
				serverURLImage="https://betamax.firstlook.biz/";
				serverURLBookImage="https://betamax.firstlook.biz/";
				princeXMLServiceEndpoint="https://www-b.int.firstlook.biz/firstlook-queue-ws/services/PrinceXmlWebService";
			}
			else {
				serverURLImage="https://beta.firstlook.biz/";
				serverURLBookImage="https://betamax.firstlook.biz/";
				princeXMLServiceEndpoint="https://beta.firstlook.biz/firstlook-queue-ws/services/PrinceXmlWebService";
			}
			 
	    	String html="";
			
			Configuration cfg = configurer.getConfiguration();
	        try {
	            //Load template from source folder
	            Template template = cfg.getTemplate("offer-html.ftl");
	             
	            // Build the data-model
	            Map<String, Object> data = new HashMap<String, Object>();
	           // data.put("message", "Hello World!");

	            //Putting Default Values
	            data.put("showRecon", showReconCost);
	            data.put("dealerLogoOrImage","");
        		data.put("dealerLogoOrImageSmall","");
        		data.put("validMiles","");
        		data.put("vehicleOwner", "");
            	data.put("phone","");
            	data.put("email", "");
            	data.put("vehicle", "");
            	data.put("vin", "");
            	data.put("exteriorColor", "");
            	data.put("mileage", "");
            	data.put("reconCost", "");
        		data.put("reconNotes", "");
        		data.put("appraiserName", "");
        		data.put("offerAmount","");
        		data.put("showEquipmentOptions", false);
        		data.put("offerAmountWords","");
        		data.put("validDate", "");
        		data.put("showCheck",false );
        		data.put("smallCheckText", false);
        		data.put("logo1size", false);
        		data.put("logo2size", false);
        		

        		
        		//Put Actual Values
        		
        		
	            data.put("showRecon", showReconCost);
	            if(dealer!=null){
	            	
	            	
	            	
	            	data.put("dealer", dealer);
	            	
	            	if(existsURL(serverURLImage+"/digitalimages/"+dealerLogoUrl)){
	            		data.put("dealerLogoOrImage","<img src='"+serverURLImage+"/digitalimages/"+dealerLogoUrl+"' alt='"+dealer.getName()+"' onError='this.style.height=30;this.style.width=400'/>");
	            		data.put("dealerLogoOrImageSmall","<img src='"+serverURLImage+"/digitalimages/"+dealerLogoUrl+"' alt='"+dealer.getName()+"' width='85' height='50' onError='this.style.height=30;this.style.width=85'/>");
	            		
	            	}
	            	else{
	            		data.put("dealerLogoOrImage","<b>"+dealer.getName()+"</b>");
	            		data.put("dealerLogoOrImageSmall","<b>"+dealer.getName()+"</b>");
	            		
	            	}
	            	if(dealer.getDealerPreference()!=null)
	            		data.put("validMiles", dealer.getDealerPreference().getAppraisalFormValidMileage());
	            	
	            }
            	if(customer!=null){

            		data.put("vehicleOwner", customer.getFirstName()+" "+customer.getLastName());
	            	data.put("phone", customer.getPhoneNumber());
	            	data.put("email", customer.getEmail());
            	}
            	if(vehicle!=null){
            		
	            	data.put("vehicle", vehicle.getVehicleYear()+" "+vehicle.getMake()+" "+vehicle.getModel());
	            	data.put("vin", vehicle.getVin());
	            	data.put("exteriorColor", vehicle.getBaseColor());
	            	data.put("mileage", appraisal.getMileage());
            	}
            	if(appraisal!=null){
            		
            		data.put("reconCost", "$"+Formatter.formatNumberToString(appraisal.getEstimatedReconditioningCost().intValue()));
            		data.put("reconNotes", StringEscapeUtils.escapeHtml(appraisal.getConditionDescription()));
            		if(appraisal.getAppraisalTypeId().equals(AppraisalTypeEnum.TRADE_IN.getId())){
            			
	            		if(appraisal.getLatestAppraisalValue()!=null)
	            			data.put("appraiserName", appraisal.getLatestAppraisalValue().getAppraiserName());
            		}
            		else{
            			if(appraisal.getSelectedBuyer()!=null)
            				data.put("appraiserName", appraisal.getSelectedBuyer().getFullName());
            			
            		}
            		if(appraisal.getAppraisalFormOptions()!=null)
            			data.put("offerAmount", appraisal.getAppraisalFormOptions().getAppraisalFormOffer());
            		
            	}
            	data.put("showEquipmentOptions", showEquipmentOptions);
            	
            	if (showEquipmentOptions && dgb1 != null && dgb1.getSelectedOptions() != null && dgb1.getSelectedOptions().size()>0){
            		
            		data.put("equipmentOptions", dgb1.getSelectedOptions());
            		data.put("equipmentOptionsSize", dgb1.getSelectedOptions().size());
            		if(!(dgb1.getSelectedOptions().size()>0)){
            			data.put("showEquipmentOptions", false);
            		}
            	}
            	if(expireDate!=null)
            		data.put("validDate", expireDate);
            	
            	data.put("todayDate", sdf.format(new Date()));
            	sdf = new SimpleDateFormat("MMMM d, yyyy");
            	data.put("todayDateWords", sdf.format(new Date()));
            	
            	NumberWordConverter nc= new NumberWordConverter();
            	data.put("offerAmountWords",nc.convert(offer)+" and 00/100");
            	
            	if(offer>90000)
        			data.put("smallCheckText", true);
            	//Book Section 
            	//Book 1
            	boolean showGuideBook1=false;
            	if (dgb1 != null) {
            		
        			List<VDP_GuideBookValue> values = new ArrayList<VDP_GuideBookValue>();
        			if (ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE == dgb1
        					.getGuideBookId().intValue() ){
        				
        				data.put("logo1size", true);
        			}
        			
        			if (ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE == dgb1
        					.getGuideBookId().intValue() && !includeMileageAdjustment) {
        				values = dgb1.getDisplayGuideBookValuesNoMileage();
        				
        			} else {
        				values = dgb1.getDisplayGuideBookValues();
        			}
        			
        			List<VDP_GuideBookValue> finalValues= new ArrayList<VDP_GuideBookValue>();
        			Iterator<VDP_GuideBookValue> valuesItr = values.iterator();
        			
        			while (valuesItr.hasNext()) {
        				
        				VDP_GuideBookValue value = valuesItr.next();
        				for (int i = 0; i < categoriesToDisplay.length; i++){
        					if(value!=null && categoriesToDisplay[i] == value.getThirdPartyCategoryId())
        					{
        						finalValues.add(value);
        					
        					
        					}
        				}
        			
        			}
        			if(finalValues.size()>0){
        				showGuideBook1=true;
        				 data.put("displayGuideBook1Values",finalValues);
        			}
        			data.put("guideBook1Name", dgb1.getGuideBookName());
        			data.put("guideBook1Image", getLogoPath(dealer.getGuideBookId(), serverURLBookImage+"IMT"));

            	}
            	data.put("showGuideBook1", showGuideBook1);

            	//Book 2
            	boolean showGuideBook2=false;
            	if (dgb2 != null) {
            		
        			List<VDP_GuideBookValue> values = new ArrayList<VDP_GuideBookValue>();

        			if (ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE == dgb2
        					.getGuideBookId().intValue() ){
        				
        				data.put("logo2size", true);
        			}
        			if (ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE == dgb2
        					.getGuideBookId().intValue() && !includeMileageAdjustment) {
        				values = dgb2.getDisplayGuideBookValuesNoMileage();
        			} else {
        				values = dgb2.getDisplayGuideBookValues();
        			}
        			
        			List<VDP_GuideBookValue> finalValues= new ArrayList<VDP_GuideBookValue>();
        			Iterator<VDP_GuideBookValue> valuesItr = values.iterator();
        			
        			while (valuesItr.hasNext()) {
        				VDP_GuideBookValue value = valuesItr.next();
        				for (int i = 0; i < categoriesToDisplay.length; i++){
	        				if(value!=null && categoriesToDisplay[i] == value.getThirdPartyCategoryId())
	        				{	
	        					finalValues.add(value);
	        					
	        					
	        				}
        				}
        			
        			}
        			if(finalValues.size()>0){
        				showGuideBook2=true;
        				 data.put("displayGuideBook2Values",finalValues);
        			}
        			data.put("guideBook2Name", dgb2.getGuideBookName());
        			data.put("guideBook2Image", getLogoPath(dealer.getGuideBook2Id(), serverURLBookImage+"IMT"));

            	}
            	data.put("showGuideBook2", showGuideBook2);            	
            	
            	//KBB CONSUMER VALUE
            	boolean showGuideBook3=false;
            	if (kbbConsumerValue != null) {
        			
            		VDP_GuideBookValue value= new VDP_GuideBookValue();
            		value.setValue(kbbConsumerValue);
            		value.setThirdPartyCategoryDescription("Trade-In");
            		
            		List<VDP_GuideBookValue> finalValues= new ArrayList<VDP_GuideBookValue>();
            		finalValues.add(value);
        			
            		        			
            		if(finalValues.size()>0){
        				showGuideBook3=true;
        				 data.put("displayGuideBook3Values",finalValues);
        			}
        			data.put("guideBook3Image", getLogoPath(VehicleDescriptionProviderEnum.KBB.getThirdPartyId(), serverURLBookImage+"IMT"));
        			data.put("guideBook3Name", VehicleDescriptionProviderEnum.KBB.getName());
        			
        			
        		} 
            	data.put("showGuideBook3", showGuideBook3);
            	
            	
            	
            	
            	data.put("showCheck",showCheck );
            	data.put("checkImageURL", serverURLBookImage+"IMT"+CHECK_BG_IMAGE);
            	
            	
	            // Console output
	            //Writer out = new OutputStreamWriter(System.out);
	            //template.process(data, out);
	            //out.flush();
	 
	            
	            
	            // File output
//	            Writer file = new FileWriter (new File("C:\\FTL_helloworld.html"));
//	            template.process(data, file);
//	            file.flush();
//	            file.close();
            	
            	html=FreeMarkerTemplateUtils.processTemplateIntoString(template, data);
	             
	        } catch (IOException e) {
	            e.printStackTrace();
	        } catch (TemplateException e) {
	            e.printStackTrace();
	        } 
		
		
		
		Input input = new Input();
		OutputType out= new OutputType();
		out.setOutputTypeCode(OutputTypeCode.BYTE_STREAM);
		
		input.setContent(html);
		input.setContentType(ContentType.HTML);
		
		PrinceXmlRequest req= new PrinceXmlRequest();
		req.setInput(input);
		req.setOutputType(out);
		
		PrinceXmlWebServiceClient client= new PrinceXmlWebServiceClient();
		PrinceXmlWebService service= client.getPrinceXmlWebServiceSOAP(princeXMLServiceEndpoint);
		PrinceXmlResponse resp = null;
		try{
		resp=service.generate(req);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		try {
			if(resp!=null)
			baos.write(resp.getOutput().getByteStream());
		} catch (IOException e) {
		
			e.printStackTrace();
		}
		
		
		
		return null;
	}
	
	@SuppressWarnings("deprecation")
	public String buildAppraisalFormPDFNew(ByteArrayOutputStream baos,
			boolean showEquipmentOptions, boolean showPhotos,
			Integer[] categoriesToDisplay, DisplayGuideBook dgb1,
			DisplayGuideBook dgb2, String dealerLogoUrl,
			boolean includeMileageAdjustment, Integer kbbConsumerValue,
			boolean showCheck,boolean showReconCost) {
		
		Document doc = new Document(PageSize.A4, 20, 20, 20, 20);

		Integer offer = null;
		float infoHeight, offsetHeight = 0;

		if (appraisalFormOptions != null) {
			offer = appraisalFormOptions.getAppraisalFormOffer();
		}

		Date now = new Date();
		Date offerExpirationDate = new Date();
		offerExpirationDate.setDate(now.getDate()
				+ dealer.getDealerPreference().getAppraisalFormValidDate());
		DateFormat dateFormatter = DateFormat.getDateInstance(DateFormat.SHORT);
		DateFormat dateTimeFormatter = DateFormat.getDateTimeInstance(
				DateFormat.SHORT, DateFormat.SHORT);
		currentDate = dateFormatter.format(now);
		String currentDateTime = dateTimeFormatter.format(now);
		expireDate = dateFormatter.format(offerExpirationDate);
		dealerLogoURL= dealerLogoUrl;
		boolean showValues = false;

		Graphic horizontalLine = new Graphic();
		horizontalLine.setHorizontalLine(1f, 100f);
		try {
			PdfWriter writer = PdfWriter.getInstance(doc, baos);

			doc.open();

			// Top Section

			topSectionBuilderNew(doc, writer, dealerLogoUrl);

		
			
			infoHeight = infoSectionBuilderNew(doc, writer, currentDateTime,
					expireDate, offer, dgb1, showEquipmentOptions,showReconCost);
			offsetHeight = infoHeight - 94;

			// Photo Section
			//if (showPhotos) {
			//	doc.add(horizontalLine);
			//	showPhotos = photoSectionNew(doc, writer);
			//}
			doc.add(Chunk.NEWLINE);
			if ((categoriesToDisplay != null && categoriesToDisplay.length > 0 && (dgb1 != null || dgb2 != null))
					|| (kbbConsumerValue != null)) {
				doc.add(horizontalLine);
				bookValueSectionNew(doc, writer, dgb1, dgb2, categoriesToDisplay,
						includeMileageAdjustment, kbbConsumerValue);
				showValues = true;
			}

			
			// Vehicle Representation Section
			//doc.add(horizontalLine);
			vehicleRepresentationSectionNew(doc, writer);

			// Check Section /
			if (showCheck) {
				doc.add(Chunk.NEWLINE);
				checkSectionBuilderNew(doc, writer, currentDate, expireDate,
						offer, showValues, showPhotos, showEquipmentOptions,
						offsetHeight);
			}

			doc.close();

		} catch (DocumentException e) {
			logger.error(e);
			return "creating the pdf";
		} catch (MalformedURLException e) {
			logger.error(e);
			return "loading the images";
		} catch (IOException e) {
			logger.error(e);
			return "loading the images";
		} catch (NullPointerException e) {
			logger.error(e);
			return e.getMessage();
		}

		return null;
	}

	private void checkSectionBuilder(Document doc, PdfWriter writer,
			String currentDate, String expireDate, Integer offer,
			boolean showValues, boolean showPhotos,
			boolean showEquipmentOptions, float offsetHeight)
			throws MalformedURLException, IOException, DocumentException {
		float spacingForAlignment = writer.getVerticalPosition(false) - 225; // check
																				// 195
																				// +
																				// 30
																				// bottom
																				// margin
		if (showValues == false) {
			spacingForAlignment -= 40;
		}
		if (spacingForAlignment <= 0) {
			spacingForAlignment = 0;
		}

		Paragraph spacerParagraph = new Paragraph();
		spacerParagraph.setSpacingBefore(spacingForAlignment);
		doc.add(spacerParagraph);

		Image checkImage = Image.getInstance(realPath + CHECK_BG_IMAGE);
		checkImage.setAlignment(Image.UNDERLYING);
		checkImage.scaleAbsolute(535, 190);
		doc.add(checkImage);

		PdfPCell pdfCell;

		float[] checkTableWidths = new float[] { 70, 30 };
		PdfPTable checkTable = new PdfPTable(checkTableWidths);
		checkTable.setWidthPercentage(100);

		pdfCell = new PdfPCell();
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setPaddingTop(10);
		pdfCell.setPaddingLeft(10);
		pdfCell.addElement(new Paragraph(dealer.getName(), HELVET_10_BOLD));
		pdfCell.addElement(new Paragraph(dealer.getAddress1(), HELVET_8_NORMAL));

		stringBuilder = new StringBuilder(dealer.getCity());
		stringBuilder.append(", ");
		stringBuilder.append(dealer.getState());
		stringBuilder.append(" ");
		stringBuilder.append(dealer.getZipcode());
		pdfCell.addElement(new Paragraph(stringBuilder.toString(),
				HELVET_8_NORMAL));

		pdfCell.addElement(new Paragraph(dealer.getOfficePhoneNumber(),
				HELVET_8_NORMAL));
		checkTable.addCell(pdfCell);

		pdfCell = new PdfPCell();
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setPaddingTop(23);
		pdfCell.setPaddingLeft(5);
		Paragraph currentDatePag = new Paragraph(currentDate, HELVET_10_NORMAL);
		currentDatePag.setIndentationLeft(45);
		pdfCell.addElement(currentDatePag);

		stringBuilder = new StringBuilder("Offer expires after ");
		stringBuilder.append(expireDate);
		stringBuilder.append(" or ");
		stringBuilder.append(dealer.getDealerPreference()
				.getAppraisalFormValidMileage());
		stringBuilder.append(" miles");
		pdfCell.addElement(new Paragraph(stringBuilder.toString(), new Font(
				Font.HELVETICA, 6, Font.ITALIC)));

		checkTable.addCell(pdfCell);

		pdfCell = new PdfPCell();
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setPaddingTop(16);
		pdfCell.setPaddingLeft(130);
		stringBuilder = new StringBuilder(convertNullToEmptyString(customer,
				"firstName"));
		stringBuilder.append(" ");
		stringBuilder.append(convertNullToEmptyString(customer, "lastName"));
		pdfCell.addElement(new Paragraph(stringBuilder.toString(),
				HELVET_14_BOLD));
		checkTable.addCell(pdfCell);

		pdfCell = new PdfPCell();
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setPaddingLeft(40);
		pdfCell.setPaddingTop(7);
		pdfCell.addElement(new Paragraph("$"
				+ Formatter.formatNumberToString(offer), HELVET_16_BOLD));
		checkTable.addCell(pdfCell);

		pdfCell = new PdfPCell();
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setPaddingLeft(15);
		pdfCell.setPaddingTop(55);
		pdfCell.setColspan(2);
		pdfCell.addElement(new Paragraph(dealer.getDealerPreference()
				.getAppraisalFormMemo(), new Font(Font.HELVETICA, 8,
				Font.ITALIC)));
		pdfCell.addElement(new Paragraph("VIN# " + vehicle.getVin(),
				HELVET_8_NORMAL));
		pdfCell.addElement(new Paragraph("Not a draft", HELVET_8_NORMAL));
		checkTable.addCell(pdfCell);

		doc.add(checkTable);
	}

	private void checkSectionBuilderNew(Document doc, PdfWriter writer,
			String currentDate, String expireDate, Integer offer,
			boolean showValues, boolean showPhotos,
			boolean showEquipmentOptions, float offsetHeight)
			throws MalformedURLException, IOException, DocumentException {
		float spacingForAlignment = writer.getVerticalPosition(false) - 225; // check
																				// 195
																				// +
																				// 30
																				// bottom
																				// margin
		if (showValues == false) {
			spacingForAlignment -= 40;
		}
		if (spacingForAlignment <= 0) {
			spacingForAlignment = 0;
		}

		Paragraph spacerParagraph = new Paragraph();
		spacerParagraph.setSpacingBefore(spacingForAlignment);
		doc.add(spacerParagraph);

		Image checkImage = Image.getInstance(realPath + CHECK_BG_IMAGE);
		checkImage.setAlignment(Image.UNDERLYING);
		checkImage.scaleAbsolute(535, 190);
		//doc.add(checkImage);

		PdfPCell pdfCell;

		
			
		float[] checkTableWidths = new float[] { 60,40 };
		PdfPTable checkTable = new PdfPTable(checkTableWidths);
		checkTable.setWidthPercentage(100);

		pdfCell = new PdfPCell();
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setPaddingTop(10);
		pdfCell.setPaddingLeft(125);
		//pdfCell.addElement(new Paragraph(dealer.getName(), HELVET_10_BOLD));
		pdfCell.addElement(new Paragraph(dealer.getAddress1(), HELVET_8_NORMAL));

		stringBuilder = new StringBuilder(dealer.getCity());
		if(dealer.getCity()!=null&&!dealer.getCity().equals(""))
			stringBuilder.append(", ");
		stringBuilder.append(dealer.getState());
		stringBuilder.append(" ");
		stringBuilder.append(dealer.getZipcode());
		pdfCell.addElement(new Paragraph(stringBuilder.toString(),
				HELVET_8_NORMAL));

		pdfCell.addElement(new Paragraph(dealer.getOfficePhoneNumber(),
				HELVET_8_NORMAL));
		checkTable.addCell(pdfCell);

		pdfCell = new PdfPCell();
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setPaddingTop(45);
		pdfCell.setPaddingLeft(12);
		Paragraph currentDatePag = new Paragraph(currentDate, HELVET_10_NORMAL);
		currentDatePag.setIndentationLeft(45);
		pdfCell.addElement(currentDatePag);

		stringBuilder = new StringBuilder(" ");
		stringBuilder.append(expireDate);
		stringBuilder.append(" and/or ");
		stringBuilder.append(dealer.getDealerPreference()
				.getAppraisalFormValidMileage());
		stringBuilder.append(" miles");
		pdfCell.addElement(new Paragraph(stringBuilder.toString(), new Font(
				Font.HELVETICA, 6, Font.ITALIC)));

		checkTable.addCell(pdfCell);

		pdfCell = new PdfPCell();
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setPaddingTop(0);
		pdfCell.setPaddingLeft(100);
		stringBuilder = new StringBuilder(convertNullToEmptyString(customer,
				"firstName"));
		stringBuilder.append(" ");
		stringBuilder.append(convertNullToEmptyString(customer, "lastName"));
		pdfCell.addElement(new Paragraph(stringBuilder.toString(),
				HELVET_14_BOLD));

		checkTable.addCell(pdfCell);

		pdfCell = new PdfPCell();
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setPaddingLeft(120);
		pdfCell.setPaddingTop(0);
		pdfCell.addElement(new Paragraph("$"
				+ Formatter.formatNumberToString(offer), HELVET_16_BOLD));
		checkTable.addCell(pdfCell);

		pdfCell = new PdfPCell();
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setPaddingTop(7);
		pdfCell.setPaddingLeft(26);
		pdfCell.setColspan(2);
		NumberWordConverter nc= new NumberWordConverter();
		pdfCell.addElement(new Paragraph(nc.convert(offer),
				HELVET_10_BOLD));
		checkTable.addCell(pdfCell);
		
		
		
		pdfCell = new PdfPCell();
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setPaddingLeft(85);
		pdfCell.setPaddingTop(35);
		pdfCell.setColspan(2);
//		pdfCell.addElement(new Paragraph(dealer.getDealerPreference()
//				.getAppraisalFormMemo(), new Font(Font.HELVETICA, 8,
//				Font.ITALIC)));
		pdfCell.addElement(new Paragraph("Vehicle Purchase Offer",
				HELVET_14_BOLD));
		
		pdfCell.addElement(new Paragraph("" + vehicle.getVin(),
				HELVET_8_NORMAL));
//		pdfCell.addElement(new Paragraph("Not a draft", HELVET_8_NORMAL));
		checkTable.addCell(pdfCell);
		
		
		//checkTable.setTableEvent(new CheckTableBG());
		
		PdfPTable temp= new PdfPTable(1);
		temp.setWidthPercentage(100);
		
		
		pdfCell= new PdfPCell(checkTable);
		pdfCell.setCellEvent(new CheckCellBG());
		pdfCell.setBorder(0);
		
		temp.addCell(pdfCell);
		
		doc.add(temp);
	}
	
	private void vehicleRepresentationSection(Document doc, PdfWriter writer)
			throws DocumentException, MalformedURLException, IOException {
		Paragraph vehicleRepSection = new Paragraph();
		Paragraph checkBoxes = new Paragraph();
		Chunk checkBox = new Chunk(
				Image.getInstance(realPath + CHECK_BOX_IMAGE), 0, -3);
		Chunk yes = new Chunk("Yes ", HELVET_10_NORMAL);
		Chunk no = new Chunk("No   ", HELVET_10_NORMAL);
		Phrase yesOrNo = new Phrase();
		yesOrNo.add(checkBox);
		yesOrNo.add("Yes ");
		yesOrNo.add(checkBox);
		yesOrNo.add("No  ");

		checkBoxes.add(new Chunk("Flood Damage ", HELVET_11_BOLD));
		checkBoxes.add(checkBox);
		checkBoxes.add(yes);
		checkBoxes.add(checkBox);
		checkBoxes.add(no);
		checkBoxes.add(new Chunk("  Frame Damage ", HELVET_11_BOLD));
		checkBoxes.add(checkBox);
		checkBoxes.add(yes);
		checkBoxes.add(checkBox);
		checkBoxes.add(no);
		checkBoxes.add(new Chunk("  Accident ", HELVET_11_BOLD));
		checkBoxes.add(checkBox);
		checkBoxes.add(yes);
		checkBoxes.add(checkBox);
		checkBoxes.add(no);

		float[] vehRepTableWidths = new float[] { 20, 80 };
		PdfPTable vehRepTable = new PdfPTable(vehRepTableWidths);
		vehRepTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		vehRepTable.setWidthPercentage(100);
		vehRepTable.addCell(new Paragraph("Vehicle Representation: ",
				HELVET_11_BOLD));
		vehRepTable.addCell(checkBoxes);
		vehRepTable.addCell("");
		vehRepTable.addCell(new Paragraph(dealer.getDealerPreference()
				.getAppraisalFormDisclaimer(), HELVET_10_NORMAL));

		vehicleRepSection.add(vehRepTable);
		vehicleRepSection.add(Chunk.NEWLINE);

		float[] sigSectionTableWidths = new float[] { 30, 5, 30, 5, 30 };
		PdfPTable sigSectionTable = new PdfPTable(sigSectionTableWidths);
		sigSectionTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		sigSectionTable.getDefaultCell().setBorderWidthTop(1);
		sigSectionTable.setWidthPercentage(100);
		sigSectionTable
				.addCell(new Paragraph("Vehicle Owner", HELVET_10_NORMAL));
		PdfPCell spacerCell = new PdfPCell();
		spacerCell.setBorder(Rectangle.NO_BORDER);
		sigSectionTable.addCell(spacerCell);
		sigSectionTable
				.addCell(new Paragraph("Sales Manager", HELVET_10_NORMAL));
		sigSectionTable.addCell(spacerCell);
		sigSectionTable.addCell(new Paragraph("Appraiser", HELVET_10_NORMAL));

		vehicleRepSection.add(sigSectionTable);
		doc.add(vehicleRepSection);

	}

	private void vehicleRepresentationSectionNew(Document doc, PdfWriter writer)
			throws DocumentException, MalformedURLException, IOException {
		Paragraph vehicleRepSection = new Paragraph();

		PdfPCell spacerCell = new PdfPCell();
		spacerCell.setBorder(Rectangle.NO_BORDER);

		float[] vehRepTableWidths = new float[] { 40,20, 40 };
		PdfPTable vehRepTable = new PdfPTable(vehRepTableWidths);
		vehRepTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		vehRepTable.setWidthPercentage(100);
		
		
		PdfPCell pdfCell;
		
		pdfCell= new PdfPCell();
		pdfCell.setBorder(Rectangle.NO_BORDER);
		Paragraph offerParagraph = new Paragraph("",HELVET_6_NORMAL_GREY);
		offerParagraph.add("Offer valid until "+expireDate+" and/or "+dealer.getDealerPreference().getAppraisalFormValidMileage()+" miles :");
		//offerParagraph.add(Chunk.NEWLINE);
		offerParagraph.add("comparison values as of "+ currentDate);
		pdfCell.addElement(offerParagraph);
		
		vehRepTable.addCell(pdfCell);
		vehRepTable.addCell(spacerCell);
		
		Integer offer = null;
				if (appraisalFormOptions != null) {
			offer = appraisalFormOptions.getAppraisalFormOffer();
		}
		
		Paragraph topLine = new Paragraph();
		Chunk topLineText = new Chunk("PURCHASE OFFER : ", HELVET_15_BOLD_GREY);
		topLineText.setTextRise(2);
		topLine.add(topLineText);
		topLineText= new Chunk("$"+ Formatter.formatNumberToString(offer), HELVET_15_BOLD_BLUE);
		topLineText.setTextRise(2);
		topLine.add(topLineText);
		topLine.setAlignment(Element.ALIGN_RIGHT);
		
		pdfCell= new PdfPCell(topLine);
		pdfCell.setBorder(Rectangle.NO_BORDER);
		vehRepTable.addCell(pdfCell);
		
		
		
		
		vehicleRepSection.add(vehRepTable);
		vehicleRepSection.add(Chunk.NEWLINE);

		float[] sigSectionTableWidths = new float[] { 30, 5, 30, 5, 30 };
		PdfPTable sigSectionTable = new PdfPTable(sigSectionTableWidths);
		sigSectionTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		sigSectionTable.getDefaultCell().setBorderWidthTop(1);
		sigSectionTable.getDefaultCell().setBorderColor(new Color(158,158,170));
		
		sigSectionTable.getDefaultCell().setPaddingLeft(5);
		sigSectionTable.setWidthPercentage(100);
		//row 1
		sigSectionTable.addCell(spacerCell);
		sigSectionTable.addCell(spacerCell);
		sigSectionTable.addCell(spacerCell);
		sigSectionTable.addCell(spacerCell);
		
		PdfPCell tempCell = new PdfPCell();
		tempCell.setBorder(Rectangle.NO_BORDER);
		if(appraisal.getAppraisalTypeId().equals(AppraisalTypeEnum.TRADE_IN.getId()))
			tempCell.addElement(new Paragraph(appraisal.getLatestAppraisalValue().getAppraiserName(), HELVET_10_NORMAL));
		if(appraisal.getAppraisalTypeId() .equals(AppraisalTypeEnum.PURCHASE.getId() )&& appraisal.getSelectedBuyer() !=null)
			tempCell.addElement(new Paragraph(appraisal.getSelectedBuyer().getFullName(), HELVET_10_NORMAL));
		sigSectionTable.addCell(tempCell);
		//row 2
		
		sigSectionTable
				.addCell(new Paragraph("Vehicle Owner", HELVET_10_NORMAL_GREY));
		sigSectionTable.addCell(spacerCell);
		sigSectionTable
				.addCell(new Paragraph("Sales Manager", HELVET_10_NORMAL_GREY));
		sigSectionTable.addCell(spacerCell);
		sigSectionTable.addCell(new Paragraph("Appraiser", HELVET_10_NORMAL_GREY));

		vehicleRepSection.add(sigSectionTable);
		
		PdfPTable temp = new PdfPTable(1);
		temp.setWidthPercentage(100);		
		
		temp.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		temp.addCell(vehicleRepSection);
		
		doc.add(temp);
		

	}
	
	private void bookValueSection(Document doc, PdfWriter writer,
			DisplayGuideBook dgb1, DisplayGuideBook dgb2,
			Integer[] categoriesToDisplay, boolean includeMileageAdjustment,
			Integer kbbConsumerValue) throws DocumentException,
			MalformedURLException, IOException {
		Paragraph bookValueSection = new Paragraph();
		PdfPTable bookValuesTable = new PdfPTable(6);
		PdfPCell bookValueCell;
		bookValuesTable.getDefaultCell().setFixedHeight(70f);
		bookValuesTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		bookValuesTable.setWidthPercentage(100);
		boolean primaryValueDisplayed = false;
		boolean secondaryValueDisplayed = false;

		if (dgb1 != null) {
			bookValueCell = new PdfPCell();
			bookValueCell.setBorder(Rectangle.NO_BORDER);
			List<VDP_GuideBookValue> values = new ArrayList<VDP_GuideBookValue>();

			if (ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE == dgb1
					.getGuideBookId().intValue() && !includeMileageAdjustment) {
				values = dgb1.getDisplayGuideBookValuesNoMileage();
			} else {
				values = dgb1.getDisplayGuideBookValues();
			}

			Iterator<VDP_GuideBookValue> valuesItr = values.iterator();

			while (valuesItr.hasNext()) {
				VDP_GuideBookValue value = valuesItr.next();

				for (int i = 0; i < categoriesToDisplay.length; i++) {
					if (categoriesToDisplay[i] == value
							.getThirdPartyCategoryId()) {
						Paragraph bookValueLabel = new Paragraph(
								value.getThirdPartyCategoryDescription(),
								HELVET_8_NORMAL);
						Chunk bookValue;
						if (value.getValue() != null && value.getValue() != 0) {
							bookValue = new Chunk(": $"
									+ Formatter.formatNumberToString(value
											.getValue()), HELVET_10_BOLD);
						} else {
							bookValue = new Chunk(": N/A", HELVET_10_BOLD);
						}
						bookValueLabel.add(bookValue);
						bookValueCell.addElement(bookValueLabel);
						primaryValueDisplayed = true;
					}
				}
			}
			if (primaryValueDisplayed) { // this is so the cell is only
											// displayed when there is data to
											// display
				Image logoImage = Image.getInstance(getLogoPath(
						dealer.getGuideBookId(), realPath));
				logoImage.scaleAbsoluteWidth(LOGO_WIDTH);
				Chunk logo = new Chunk(logoImage, 0, 0);
				bookValuesTable.addCell(new Paragraph(logo));
				bookValuesTable.addCell(bookValueCell);
			}
		}

		if (dgb2 != null) {
			bookValueCell = new PdfPCell();
			bookValueCell.setBorder(Rectangle.NO_BORDER);

			List<VDP_GuideBookValue> values = new ArrayList<VDP_GuideBookValue>();

			if (ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE == dgb2
					.getGuideBookId().intValue() && !includeMileageAdjustment) {
				values = dgb2.getDisplayGuideBookValuesNoMileage();
			} else {
				values = dgb2.getDisplayGuideBookValues();
			}

			Iterator<VDP_GuideBookValue> valuesItr = values.iterator();
			while (valuesItr.hasNext()) {
				VDP_GuideBookValue value = valuesItr.next();
				for (int i = 0; i < categoriesToDisplay.length; i++) {
					if (categoriesToDisplay[i] == value
							.getThirdPartyCategoryId()) {
						Paragraph bookValueLabel = new Paragraph(
								value.getThirdPartyCategoryDescription(),
								HELVET_8_NORMAL);
						Chunk bookValue;
						if (value.getValue() != null) {
							bookValue = new Chunk(": $"
									+ Formatter.formatNumberToString(value
											.getValue()), HELVET_10_BOLD);
						} else {
							bookValue = new Chunk(": N/A", HELVET_10_BOLD);
						}
						bookValueLabel.add(bookValue);
						bookValueCell.addElement(bookValueLabel);
						secondaryValueDisplayed = true;
					}
				}
			}
			if (secondaryValueDisplayed) {
				Image logoImage = Image.getInstance(getLogoPath(
						dealer.getGuideBook2Id(), realPath));
				logoImage.scaleAbsoluteWidth(LOGO_WIDTH);
				Chunk logo = new Chunk(logoImage, 0, 0);
				bookValuesTable.addCell(new Paragraph(logo));
				bookValuesTable.addCell(bookValueCell);
			}
		}

		if (kbbConsumerValue != null) {
			bookValueCell = new PdfPCell();
			bookValueCell.setBorder(Rectangle.NO_BORDER);

			Paragraph bookValueLabel = new Paragraph("Trade-In",
					HELVET_8_NORMAL);
			Chunk bookValue = new Chunk(": $"
					+ Formatter.formatNumberToString(kbbConsumerValue),
					HELVET_10_BOLD);
			bookValueLabel.add(bookValue);
			bookValueCell.addElement(bookValueLabel);

			Image logoImage = Image.getInstance(getLogoPath(
					VehicleDescriptionProviderEnum.KBB.getThirdPartyId(),
					realPath));
			logoImage.scaleAbsoluteWidth(LOGO_WIDTH);
			Chunk logo = new Chunk(logoImage, 0, 0);
			bookValuesTable.addCell(new Paragraph(logo));
			bookValuesTable.addCell(bookValueCell);
		} else {
			bookValuesTable.addCell("");
			bookValuesTable.addCell("");
		}

		// If no values are checked for one of the books fill the table with
		// empty cells.
		if (!primaryValueDisplayed) {
			bookValuesTable.addCell("");
			bookValuesTable.addCell("");
		}
		if (!secondaryValueDisplayed) {
			bookValuesTable.addCell("");
			bookValuesTable.addCell("");
		}

		bookValueSection.add(new Paragraph("Book Values: ", HELVET_10_BOLD));
		bookValueSection.add(bookValuesTable);
		doc.add(bookValueSection);
		doc.add(Chunk.NEWLINE);
	}

	private void bookValueSectionNew(Document doc, PdfWriter writer,
			DisplayGuideBook dgb1, DisplayGuideBook dgb2,
			Integer[] categoriesToDisplay, boolean includeMileageAdjustment,
			Integer kbbConsumerValue) throws DocumentException,
			MalformedURLException, IOException {
		Paragraph bookValueSection = new Paragraph();
		PdfPTable bookValuesTable = new PdfPTable(1);
		PdfPCell bookValueCell;
		bookValuesTable.getDefaultCell().setFixedHeight(70f);
		bookValuesTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		bookValuesTable.setWidthPercentage(100);
		boolean primaryValueDisplayed = false;
		boolean secondaryValueDisplayed = false;

		
		Integer offer = null;
		

		if (appraisalFormOptions != null) {
			offer = appraisalFormOptions.getAppraisalFormOffer();
		}
		if (dgb1 != null) {
			bookValueCell = new PdfPCell();
			bookValueCell.setBorder(Rectangle.NO_BORDER);
			List<VDP_GuideBookValue> values = new ArrayList<VDP_GuideBookValue>();

			if (ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE == dgb1
					.getGuideBookId().intValue() && !includeMileageAdjustment) {
				values = dgb1.getDisplayGuideBookValuesNoMileage();
			} else {
				values = dgb1.getDisplayGuideBookValues();
			}

			Iterator<VDP_GuideBookValue> valuesItr = values.iterator();

			while (valuesItr.hasNext()) {
				VDP_GuideBookValue value = valuesItr.next();
				if(value!=null)
				{
					PdfPTable tempTable = new PdfPTable(3);
					tempTable.setWidths(new float[]{50,25,25});
					tempTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
					tempTable.setWidthPercentage(100);
					for (int i = 0; i < categoriesToDisplay.length; i++) {
						if (categoriesToDisplay[i] == value
								.getThirdPartyCategoryId()) {
	
							tempTable.addCell(new Paragraph(value.getThirdPartyCategoryDescription(),HELVET_8_NORMAL_GREY));
							Integer difference= offer-value.getValue();
							if(difference>0)
								tempTable.addCell(new Paragraph("$"+Formatter.formatNumberToString(difference)+"",HELVET_8_NORMAL_GREEN));
							else
								tempTable.addCell(new Paragraph("($"+Formatter.formatNumberToString(Math.abs(difference))+")",HELVET_8_NORMAL_RED));
							
							
							tempTable.addCell(new Paragraph("$"+Formatter.formatNumberToString(value.getValue())+"",HELVET_8_NORMAL));
	//						Paragraph bookValueLabel = new Paragraph(
	//								value.getThirdPartyCategoryDescription()+ " "+value.getThirdPartyCategoryId()+"-"+value.getThirdPartySubCategoryDescription(),
	//								HELVET_8_NORMAL);
	//						Chunk bookValue;
							//if (value.getValue() != null && value.getValue() != 0) {
	//							bookValue = new Chunk(": $"
	//									+ Formatter.formatNumberToString(value
	//											.getValue()), HELVET_10_BOLD);
	//						} else {
	//							bookValue = new Chunk(": N/A", HELVET_10_BOLD);
	//						}
	//						bookValueLabel.add(bookValue);
						}
						primaryValueDisplayed = true;
					}
					bookValueCell.addElement(tempTable);
				}
			}
			if (primaryValueDisplayed) { // this is so the cell is only
											// displayed when there is data to
											// display
				Image logoImage = Image.getInstance(getLogoPath(
						dealer.getGuideBookId(), realPath));
				logoImage.scaleAbsoluteWidth(LOGO_WIDTH);
				Chunk logo = new Chunk(logoImage, 0, 0);
				PdfPTable book1ValuesTable = new PdfPTable(2);
				book1ValuesTable.setWidths(new float[]{30,70});
				book1ValuesTable.getDefaultCell().setFixedHeight(70f);
				book1ValuesTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
				book1ValuesTable.setWidthPercentage(100);
				book1ValuesTable.addCell(new Paragraph(logo));
				book1ValuesTable.addCell(bookValueCell);
				
				
				PdfPCell pdfPCell= new PdfPCell(book1ValuesTable);
				pdfPCell.setBackgroundColor(TABLE_BG_COLOR2);
				pdfPCell.setBorderWidth(1f);
				pdfPCell.setBorderColor(new Color(158,158,170));
				bookValuesTable.addCell(pdfPCell);
				
				pdfPCell = new PdfPCell();
				pdfPCell.setBackgroundColor(TABLE_BG_COLOR2);
				pdfPCell.setFixedHeight(10f);
				pdfPCell.setBorder(Rectangle.NO_BORDER);
				bookValuesTable.addCell(pdfPCell);
			}
		}

		if (dgb2 != null) {
			bookValueCell = new PdfPCell();
			bookValueCell.setBorder(Rectangle.NO_BORDER);

			List<VDP_GuideBookValue> values = new ArrayList<VDP_GuideBookValue>();

			if (ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE == dgb2
					.getGuideBookId().intValue() && !includeMileageAdjustment) {
				values = dgb2.getDisplayGuideBookValuesNoMileage();
			} else {
				values = dgb2.getDisplayGuideBookValues();
			}

			Iterator<VDP_GuideBookValue> valuesItr = values.iterator();
			while (valuesItr.hasNext()) {
				VDP_GuideBookValue value = valuesItr.next();
				if(value!=null){
					PdfPTable tempTable = new PdfPTable(3);
					tempTable.setWidths(new float[]{50,25,25});
					tempTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
					tempTable.setWidthPercentage(100);
					
					for (int i = 0; i < categoriesToDisplay.length; i++) {
						if (categoriesToDisplay[i] == value
								.getThirdPartyCategoryId()) {
							tempTable.addCell(new Paragraph(value.getThirdPartyCategoryDescription(),HELVET_8_NORMAL_GREY));
							Integer difference= offer-value.getValue();
							if(difference>0)
								tempTable.addCell(new Paragraph("$"+Formatter.formatNumberToString(difference)+"",HELVET_8_NORMAL_GREEN));
							else
								tempTable.addCell(new Paragraph("($"+Formatter.formatNumberToString(Math.abs(difference))+")",HELVET_8_NORMAL_RED));
							tempTable.addCell(new Paragraph("$"+Formatter.formatNumberToString(value.getValue())+"",HELVET_8_NORMAL));
	//						Paragraph bookValueLabel = new Paragraph(
	//								value.getThirdPartyCategoryDescription(),
	//								HELVET_8_NORMAL);
	//						Chunk bookValue;
	//						if (value.getValue() != null) {
	//							bookValue = new Chunk(": $"
	//									+ Formatter.formatNumberToString(value
	//											.getValue()), HELVET_10_BOLD);
	//						} else {
	//							bookValue = new Chunk(": N/A", HELVET_10_BOLD);
	//						}
	//						bookValueLabel.add(bookValue);
						}
					}
					bookValueCell.addElement(tempTable);
					secondaryValueDisplayed = true;
				}
			}
			if (secondaryValueDisplayed) {
				Image logoImage = Image.getInstance(getLogoPath(
						dealer.getGuideBook2Id(), realPath));
				logoImage.scaleAbsoluteWidth(LOGO_WIDTH);
				Chunk logo = new Chunk(logoImage, 0, 0);
				PdfPTable book2ValuesTable = new PdfPTable(2);
				book2ValuesTable.setWidths(new float[]{30,70});
				book2ValuesTable.getDefaultCell().setFixedHeight(70f);
				book2ValuesTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
				book2ValuesTable.setWidthPercentage(100);
				book2ValuesTable.addCell(new Paragraph(logo));
				book2ValuesTable.addCell(bookValueCell);
				
				
				PdfPCell pdfPCell= new PdfPCell(book2ValuesTable);
				pdfPCell.setBackgroundColor(TABLE_BG_COLOR2);
				pdfPCell.setBorderWidth(1f);
				pdfPCell.setBorderColor(new Color(158,158,170));
				bookValuesTable.addCell(pdfPCell);
				
				pdfPCell = new PdfPCell();
				pdfPCell.setBackgroundColor(TABLE_BG_COLOR2);
				pdfPCell.setFixedHeight(10f);
				pdfPCell.setBorder(Rectangle.NO_BORDER);
				bookValuesTable.addCell(pdfPCell);
			}
		}

		if (kbbConsumerValue != null) {
			bookValueCell = new PdfPCell();
			bookValueCell.setBorder(Rectangle.NO_BORDER);

			Paragraph bookValueLabel = new Paragraph("Trade-In",
					HELVET_8_NORMAL);
			Chunk bookValue = new Chunk(": $"
					+ Formatter.formatNumberToString(kbbConsumerValue),
					HELVET_10_BOLD);
			bookValueLabel.add(bookValue);
			//bookValueCell.addElement(bookValueLabel);

			PdfPTable tempTable = new PdfPTable(3);
			tempTable.setWidths(new float[]{50,25,25});
			tempTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			tempTable.setWidthPercentage(100);
			
			tempTable.addCell(new Paragraph("Trade-In",HELVET_8_NORMAL_GREY));
			Integer difference= offer-kbbConsumerValue;
			if(difference>0)
				tempTable.addCell(new Paragraph("$"+Formatter.formatNumberToString(difference)+"",HELVET_8_NORMAL_GREEN));
			else
				tempTable.addCell(new Paragraph("($"+Formatter.formatNumberToString(Math.abs(difference))+")",HELVET_8_NORMAL_RED));
			tempTable.addCell(new Paragraph("$"+Formatter.formatNumberToString(kbbConsumerValue)+"",HELVET_8_NORMAL));
			
			
			//
			Image logoImage = Image.getInstance(getLogoPath(
					VehicleDescriptionProviderEnum.KBB.getThirdPartyId(),
					realPath));
			logoImage.scaleAbsoluteWidth(LOGO_WIDTH);
			Chunk logo = new Chunk(logoImage, 0, 0);
			bookValueCell.addElement(tempTable);
			
			PdfPTable book3ValuesTable = new PdfPTable(2);
			book3ValuesTable.setWidths(new float[]{30,70});
			book3ValuesTable.getDefaultCell().setFixedHeight(70f);
			book3ValuesTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			book3ValuesTable.setWidthPercentage(100);
			book3ValuesTable.addCell(new Paragraph(logo));
			book3ValuesTable.addCell(bookValueCell);
			
			
			PdfPCell pdfPCell= new PdfPCell(book3ValuesTable);
			pdfPCell.setBackgroundColor(TABLE_BG_COLOR2);
			pdfPCell.setFixedHeight(70f);
			pdfPCell.setBorderWidth(1f);
			pdfPCell.setBorderColor(new Color(158,158,170));
			
			bookValuesTable.addCell(pdfPCell);
			
			pdfPCell = new PdfPCell();
			pdfPCell.setBackgroundColor(TABLE_BG_COLOR2);
			pdfPCell.setFixedHeight(10f);
			pdfPCell.setBorder(Rectangle.NO_BORDER);
			bookValuesTable.addCell(pdfPCell);
		} 

		// If no values are checked for one of the books fill the table with
		// empty cells.
		if (!primaryValueDisplayed) {
			//bookValuesTable.addCell("");
			//bookValuesTable.addCell("");
		}
		if (!secondaryValueDisplayed) {
			//bookValuesTable.addCell("");
			//bookValuesTable.addCell("");
		}

//		bookValueSection.add(new Paragraph("Book Values: ", HELVET_10_BOLD));
		bookValueSection.add(bookValuesTable);
		doc.add(bookValueSection);
		//doc.add(Chunk.NEWLINE);
	}
	
	private boolean photoSection(Document doc, PdfWriter writer)
			throws DocumentException, MalformedURLException, IOException {
		Paragraph photoSection = new Paragraph("Photos: ", HELVET_11_BOLD);
		PdfPTable imageTable = new PdfPTable(5);
		imageTable.setWidthPercentage(100);
		imageTable.getDefaultCell().setFixedHeight(100);
		imageTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		imageTable.getDefaultCell().setPaddingBottom(5);
		imageTable.getDefaultCell()
				.setVerticalAlignment(Rectangle.ALIGN_MIDDLE);
		imageTable.getDefaultCell().setHorizontalAlignment(
				Rectangle.ALIGN_CENTER);

		Set<Photo> appraisalPhotos = appraisal.getPhotos();
		int numberOfPhotos = appraisalPhotos.size();
		Iterator<Photo> photosItr = appraisalPhotos.iterator();
		while (photosItr.hasNext()) {
			Photo photo = photosItr.next();
			imageTable.addCell(Image.getInstance(PHOTO_LOC
					+ photo.getPhotoUrl()));
		}
		// Add blank cells as needed if there is not 5 photos
		while (numberOfPhotos < 5) {
			imageTable.addCell("");
			numberOfPhotos++;
		}
		photoSection.add(imageTable);

		if (appraisalPhotos != null && !appraisalPhotos.isEmpty()) {
			doc.add(photoSection);
			return true;
		} else {
			return false;
		}
	}



	
	private float infoSectionBuilder(Document doc, PdfWriter writer,
			String currentDateTime, String expireDate, Integer offer,
			DisplayGuideBook dgb1, boolean showEquipmentOptions)
			throws DocumentException {
		PdfPCell pdfCell;
		PdfPTable infoTable = new PdfPTable(1);
		infoTable.setSpacingBefore(10);
		infoTable.setSpacingAfter(5);
		infoTable.setWidthPercentage(100);

		PdfPTable vehicleInfoTable = new PdfPTable(2);
		vehicleInfoTable.setWidths(new int[] { 64, 36 });

		stringBuilder = new StringBuilder();
		stringBuilder.append(vehicle.getVehicleYear());
		stringBuilder.append(" ");
		stringBuilder.append(vehicle.getMake());
		stringBuilder.append(" ");
		stringBuilder.append(vehicle.getModel());
		stringBuilder.append(" ");
		stringBuilder.append(vehicle.getVehicleTrim());
		Paragraph topSection = new Paragraph(stringBuilder.toString(),
				HELVET_15_BOLD);
		topSection.setAlignment(Rectangle.ALIGN_LEFT);
		pdfCell = new PdfPCell(topSection);
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setFixedHeight(30);
		pdfCell.setPadding(5);
		pdfCell.setBackgroundColor(TABLE_BG_COLOR);
		vehicleInfoTable.addCell(pdfCell);

		Chunk colorLabel = new Chunk("Color: ", HELVET_10_NORMAL);
		String vehicleColor = appraisal.getColor();
		if (vehicleColor == null || "".equals(vehicleColor)) {
			vehicleColor = "UNKNOWN";
		}
		Chunk color = new Chunk(vehicleColor, HELVET_11_BOLD);
		Chunk mileageLabel = new Chunk("     Mileage: ", HELVET_10_NORMAL);
		Chunk mileage = new Chunk(Formatter.formatNumberToString(appraisal
				.getMileage()), HELVET_11_BOLD);
		Paragraph colorAndMileage = new Paragraph();
		colorAndMileage.add(colorLabel);
		colorAndMileage.add(color);
		colorAndMileage.add(mileageLabel);
		colorAndMileage.add(mileage);
		pdfCell = new PdfPCell(colorAndMileage);
		pdfCell.setHorizontalAlignment(Rectangle.ALIGN_RIGHT);
		pdfCell.setVerticalAlignment(Rectangle.ALIGN_MIDDLE);
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setFixedHeight(30);
		pdfCell.setPadding(5);
		pdfCell.setBackgroundColor(TABLE_BG_COLOR);
		vehicleInfoTable.addCell(pdfCell);

		pdfCell = new PdfPCell(vehicleInfoTable);
		infoTable.addCell(pdfCell);

		PdfPTable optionsTable = new PdfPTable(2);
		optionsTable.setWidths(new int[] { 55, 45 });

		pdfCell = new PdfPCell();
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setPadding(5);
		pdfCell.setBackgroundColor(TABLE_BG_COLOR);
		pdfCell.addElement(new Paragraph("VIN #" + vehicle.getVin(),
				HELVET_8_NORMAL));
		if (showEquipmentOptions && dgb1 != null
				&& dgb1.getSelectedOptions() != null) {
			int i = 0;
			Paragraph equipmentOptions = new Paragraph("EQUIPMENT OPTIONS: ",
					HELVET_8_NORMAL);
			Iterator<ThirdPartyVehicleOption> optionsItr = dgb1
					.getSelectedOptions().iterator();
			while (optionsItr.hasNext() && i < 17) {
				ThirdPartyVehicleOption listItem = optionsItr.next();
				if (!optionsItr.hasNext()) {
					equipmentOptions.add(listItem.getOptionName());
				} else if (i == 16 && optionsItr.hasNext()) {
					equipmentOptions.add(listItem.getOptionName() + "...");
				} else {
					equipmentOptions.add(listItem.getOptionName() + ", ");
				}
				i++;
			}
			pdfCell.addElement(equipmentOptions);
		}
		if (appraisal.getConditionDescription() != null
				&& !appraisal.getConditionDescription().equalsIgnoreCase("")) {
			String conditionDesription = appraisal.getConditionDescription();
			if (conditionDesription.length() > 1000) {
				conditionDesription = conditionDesription.substring(0, 1000)
						.concat("...");
			}
			pdfCell.addElement(new Paragraph("CONDITION DESCRIPTION: "
					+ convertNullToEmptyString(conditionDesription),
					HELVET_8_NORMAL));
		}
		optionsTable.addCell(pdfCell);

		Paragraph appraisedValueLabel = new Paragraph("APPRAISED VALUE:  ",
				HELVET_14_BOLD);
		Chunk appraisedValue = new Chunk("$"
				+ Formatter.formatNumberToString(offer), HELVET_19_BOLD);
		appraisedValueLabel.add(appraisedValue);
		pdfCell = new PdfPCell();
		pdfCell.setPadding(5);
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setBackgroundColor(TABLE_BG_COLOR);
		pdfCell.addElement(appraisedValueLabel);

		String expiration = "This appraisal is valid until " + expireDate
				+ " or "
				+ dealer.getDealerPreference().getAppraisalFormValidMileage()
				+ " miles.";
		Paragraph validFor = new Paragraph(expiration, new Font(Font.HELVETICA,
				6, Font.ITALIC));
		validFor.setIndentationLeft(70);
		pdfCell.addElement(validFor);
		if (appraisal.getAppraisalTypeId() == 1) {
			if (appraisal.getLatestAppraisalValue() != null) {
				pdfCell.addElement(new Paragraph("APPRAISED BY: "
						+ convertNullToEmptyString(
								appraisal.getLatestAppraisalValue(),
								"appraiserName"), HELVET_8_NORMAL));
			}
		} else if (appraisal.getAppraisalTypeId() == 2) {
			pdfCell.addElement(new Paragraph("APPRAISED BY: "
					+ (appraisal.getSelectedBuyer() != null ? appraisal
							.getSelectedBuyer().getFullName() : ""),
					HELVET_8_NORMAL));
		}
		String salespersonName = "";

		if (appraisal.getDealTrackSalesPerson() != null) {
			salespersonName = appraisal.getDealTrackSalesPerson().getFullName();
		}
		pdfCell.addElement(new Paragraph("APPRAISAL DATE: " + currentDateTime,
				HELVET_8_NORMAL));
		pdfCell.addElement(new Paragraph("SALESPERSON: " + salespersonName,
				HELVET_8_NORMAL));

		optionsTable.addCell(pdfCell);

		pdfCell = new PdfPCell(optionsTable);
		infoTable.addCell(pdfCell);

		doc.add(infoTable);
		return infoTable.getTotalHeight();
	}

	private float infoSectionBuilderNew(Document doc, PdfWriter writer,
			String currentDateTime, String expireDate, Integer offer,
			DisplayGuideBook dgb1, boolean showEquipmentOptions,boolean showReconCost)
			throws DocumentException {
		
		Graphic horizontalLine = new Graphic();
		horizontalLine.setHorizontalLine(1f, 100f);
		
		PdfPCell pdfCell;
		PdfPTable infoTable = new PdfPTable(1);
		infoTable.setSpacingBefore(10);
		infoTable.setSpacingAfter(5);
		infoTable.setWidthPercentage(100);

		PdfPTable custInfoTable= new PdfPTable(3);
		custInfoTable.setWidths(new int[] { 33, 33,34 });
		
		stringBuilder = new StringBuilder();
		stringBuilder.append("Vehicle Owner : ");
		stringBuilder.append(customer.getFirstName()+" "+customer.getLastName());
		Paragraph topSection = new Paragraph(stringBuilder.toString(),
				HELVET_8_NORMAL_GREY);
		topSection.setAlignment(Rectangle.ALIGN_CENTER);
		pdfCell = new PdfPCell(topSection);
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setFixedHeight(30);
		pdfCell.setPadding(5);
		pdfCell.setBackgroundColor(TABLE_BG_COLOR2);
		custInfoTable.addCell(pdfCell);
		
		stringBuilder = new StringBuilder();
		stringBuilder.append("Phone : ");
		stringBuilder.append(customer.getPhoneNumber());
		topSection = new Paragraph(stringBuilder.toString(),
				HELVET_8_NORMAL_GREY);
		topSection.setAlignment(Rectangle.ALIGN_CENTER);
		pdfCell = new PdfPCell(topSection);
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setFixedHeight(30);
		pdfCell.setPadding(5);
		pdfCell.setBackgroundColor(TABLE_BG_COLOR2);
		custInfoTable.addCell(pdfCell);
		
		stringBuilder = new StringBuilder();
		stringBuilder.append("Email : ");
		stringBuilder.append(customer.getEmail());
		topSection = new Paragraph(stringBuilder.toString(),
				HELVET_8_NORMAL_GREY);
		topSection.setAlignment(Rectangle.ALIGN_CENTER);
		pdfCell = new PdfPCell(topSection);
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setFixedHeight(30);
		pdfCell.setPadding(5);
		pdfCell.setBackgroundColor(TABLE_BG_COLOR2);
		custInfoTable.addCell(pdfCell);
		
		pdfCell= new PdfPCell(custInfoTable);
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setBorderColorBottom(new Color(158,158,170));
		pdfCell.setBorderWidthBottom(1f);
		infoTable.addCell(pdfCell);
		
		PdfPTable vehicleInfoTable = new PdfPTable(3);
		vehicleInfoTable.setWidths(new int[] { 33, 33,34 });

		
		stringBuilder = new StringBuilder();
		stringBuilder.append(vehicle.getVehicleYear());
		stringBuilder.append(" ");
		stringBuilder.append(vehicle.getMake());
		stringBuilder.append(" ");
		stringBuilder.append(vehicle.getModel());
		stringBuilder.append(" ");
		stringBuilder.append(vehicle.getVehicleTrim());
		topSection = new Paragraph(stringBuilder.toString(),
				HELVET_12_BOLD_BLUE);
		topSection.setAlignment(Rectangle.ALIGN_CENTER);
		pdfCell = new PdfPCell(topSection);
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setFixedHeight(30);
		pdfCell.setPadding(5);
		pdfCell.setBackgroundColor(TABLE_BG_COLOR2);
		vehicleInfoTable.addCell(pdfCell);
		
		
		pdfCell= new PdfPCell();
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setFixedHeight(30);
		pdfCell.setPadding(5);
		pdfCell.setBackgroundColor(TABLE_BG_COLOR2);
		vehicleInfoTable.addCell(pdfCell);
		
		stringBuilder = new StringBuilder();
		stringBuilder.append("VIN :  ");
		String vin=" - ";
		if(appraisal!=null&&appraisal.getVehicle()!=null)
			vin=appraisal.getVehicle().getVin();
		stringBuilder.append(vin);
		topSection = new Paragraph(stringBuilder.toString(),
				HELVET_8_BOLD_GREY);
		topSection.setAlignment(Rectangle.ALIGN_CENTER);
		pdfCell = new PdfPCell(topSection);
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setFixedHeight(30);
		pdfCell.setPadding(5);
		pdfCell.setBackgroundColor(TABLE_BG_COLOR2);
		vehicleInfoTable.addCell(pdfCell);
		
		
		stringBuilder = new StringBuilder();
		stringBuilder.append("Exterior Color :  ");
		
		if(appraisal !=null&&appraisal.getVehicle()!=null)
			stringBuilder.append(appraisal.getVehicle().getBaseColor());
		else 
			stringBuilder.append(" - ");
		topSection = new Paragraph(stringBuilder.toString(),
				HELVET_8_BOLD_GREY);
		topSection.setAlignment(Rectangle.ALIGN_CENTER);
		pdfCell = new PdfPCell(topSection);
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setFixedHeight(30);
		pdfCell.setPadding(5);
		pdfCell.setBackgroundColor(TABLE_BG_COLOR2);
		vehicleInfoTable.addCell(pdfCell);
		
		
		stringBuilder = new StringBuilder();
		stringBuilder.append("Interior Color :  ");
		
		if(appraisal !=null&& appraisal.getVehicle()!=null)
			stringBuilder.append(appraisal.getVehicle().getInteriorColor());
		else 
			stringBuilder.append(" - ");
		topSection = new Paragraph(stringBuilder.toString(),
				HELVET_8_BOLD_GREY);
		topSection.setAlignment(Rectangle.ALIGN_CENTER);
		pdfCell = new PdfPCell(topSection);
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setFixedHeight(30);
		pdfCell.setPadding(5);
		pdfCell.setBackgroundColor(TABLE_BG_COLOR2);
		vehicleInfoTable.addCell(pdfCell);
		
		stringBuilder = new StringBuilder();
		stringBuilder.append("Mileage :  ");
		
		if(appraisal !=null)
			stringBuilder.append(appraisal.getMileage());
		else 
			stringBuilder.append(" - ");
		topSection = new Paragraph(stringBuilder.toString(),
				HELVET_8_BOLD_GREY);
		topSection.setAlignment(Rectangle.ALIGN_CENTER);
		pdfCell = new PdfPCell(topSection);
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setFixedHeight(30);
		pdfCell.setPadding(5);
		pdfCell.setBackgroundColor(TABLE_BG_COLOR2);
		vehicleInfoTable.addCell(pdfCell);
		
		
		stringBuilder = new StringBuilder();
		stringBuilder.append("Engine :  ");
		
		if(appraisal !=null && appraisal.getVehicle()!=null)
			stringBuilder.append(appraisal.getVehicle().getVehicleEngine());
		else 
			stringBuilder.append(" - ");
		topSection = new Paragraph(stringBuilder.toString(),
				HELVET_8_BOLD_GREY);
		topSection.setAlignment(Rectangle.ALIGN_CENTER);
		pdfCell = new PdfPCell(topSection);
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setFixedHeight(30);
		pdfCell.setPadding(5);
		pdfCell.setBackgroundColor(TABLE_BG_COLOR2);
		vehicleInfoTable.addCell(pdfCell);
		
		stringBuilder = new StringBuilder();
		stringBuilder.append("Transmission :  ");
		
		if(appraisal !=null& appraisal.getVehicle()!=null)
			stringBuilder.append(appraisal.getVehicle().getVehicleTransmission());
		else 
			stringBuilder.append(" - ");
		topSection = new Paragraph(stringBuilder.toString(),
				HELVET_8_BOLD_GREY);
		topSection.setAlignment(Rectangle.ALIGN_CENTER);
		pdfCell = new PdfPCell(topSection);
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setFixedHeight(30);
		pdfCell.setPadding(5);
		pdfCell.setBackgroundColor(TABLE_BG_COLOR2);
		vehicleInfoTable.addCell(pdfCell);
		
		stringBuilder = new StringBuilder();
		stringBuilder.append("Drive Train :  ");
		
		if(appraisal !=null)
			stringBuilder.append(appraisal.getVehicle().getVehicleDriveTrain());
		else 
			stringBuilder.append(" - ");
		topSection = new Paragraph(stringBuilder.toString(),
				HELVET_8_BOLD_GREY);
		topSection.setAlignment(Rectangle.ALIGN_CENTER);
		pdfCell = new PdfPCell(topSection);
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setFixedHeight(30);
		pdfCell.setPadding(5);
		pdfCell.setBackgroundColor(TABLE_BG_COLOR2);
		vehicleInfoTable.addCell(pdfCell);
		vehicleInfoTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);

		pdfCell = new PdfPCell(vehicleInfoTable);
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setBorderColorBottom(new Color(158,158,170));
		pdfCell.setBorderWidthBottom(1f);
		infoTable.addCell(pdfCell);

		if (showEquipmentOptions && dgb1 != null
				&& dgb1.getSelectedOptions() != null && dgb1.getSelectedOptions().size()>0) {

			Paragraph equipmentOption = new Paragraph("EQUIPMENT OPTIONS: ",
					HELVET_15_BOLD_BLUE);
			pdfCell = new PdfPCell(equipmentOption);
			pdfCell.setBorder(Rectangle.NO_BORDER);
			pdfCell.setPadding(5);
			pdfCell.setBackgroundColor(TABLE_BG_COLOR2);
			infoTable.addCell(pdfCell);
			
			PdfPTable optionsTable = new PdfPTable(3);
			optionsTable.setWidths(new int[] { 33, 33,34 });
			optionsTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			
			int i=0;
			Iterator<ThirdPartyVehicleOption> optionsItr = dgb1
					.getSelectedOptions().iterator();
			//while (optionsItr.hasNext()&& i<18) {
			while (optionsItr.hasNext()) {
				pdfCell = new PdfPCell();
				pdfCell.setBorder(Rectangle.NO_BORDER);
				pdfCell.setPadding(5);
				pdfCell.setBackgroundColor(TABLE_BG_COLOR2);
				
				Paragraph equipmentOptions = new Paragraph("",
						HELVET_8_NORMAL_GREY);
				ThirdPartyVehicleOption listItem = optionsItr.next();
				
				equipmentOptions.add(listItem.getOptionName());
			
				//if(dgb1.getSelectedOptions().size()>17&& i==17){
//					equipmentOptions.add("          ...");
	//			}
				pdfCell.addElement(equipmentOptions);
				optionsTable.addCell(pdfCell);
				i++;
			}
			pdfCell = new PdfPCell(optionsTable);
			pdfCell.setBorder(Rectangle.NO_BORDER);
			pdfCell.setBorderColorBottom(new Color(158,158,170));
			pdfCell.setBorderWidthBottom(1f);
			infoTable.addCell(pdfCell);
		}


		PdfPTable reconTable = new PdfPTable(1);
		
		pdfCell=new PdfPCell();
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setPadding(5);
		pdfCell.setBackgroundColor(TABLE_BG_COLOR2);
		
		Paragraph reconCost= new Paragraph("Estimated Reconditioning Cost : ",HELVET_8_NORMAL_GREY);
		
		reconCost.add("$"+Formatter.formatNumberToString(appraisal.getEstimatedReconditioningCost().intValue()));
		pdfCell.addElement(reconCost);
		
		reconTable.addCell(pdfCell);
		
		
		PdfPTable reconNotesTable= new PdfPTable(2);
		reconNotesTable.setWidths(new int[] { 13,87 });
		
		pdfCell=new PdfPCell();
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setBackgroundColor(TABLE_BG_COLOR2);
		Paragraph reconNotes= new Paragraph("Recon Notes : ",HELVET_8_NORMAL_GREY);
		pdfCell.addElement(reconNotes);
		reconNotesTable.addCell(pdfCell);
		
		pdfCell=new PdfPCell();
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setBackgroundColor(TABLE_BG_COLOR2);
		Paragraph reconNotes2= new Paragraph(appraisal.getConditionDescription(),HELVET_8_NORMAL_GREY);
		pdfCell.addElement(reconNotes2);
		reconNotesTable.addCell(pdfCell);
		

		pdfCell=new PdfPCell(reconNotesTable);
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setPadding(5);
		pdfCell.setBackgroundColor(TABLE_BG_COLOR2);
		
		reconTable.addCell(pdfCell);
		
		if(showReconCost){
		pdfCell=new PdfPCell(reconTable);
		pdfCell.setBorder(Rectangle.NO_BORDER);
		pdfCell.setPadding(5);
		pdfCell.setBackgroundColor(TABLE_BG_COLOR2);
		infoTable.addCell(pdfCell);
		}
		
		
		PdfPTable tempTable = new PdfPTable(1);
		
		pdfCell= new PdfPCell(infoTable);
		pdfCell.setBorderWidth(1f);
		pdfCell.setBorderColor(new Color(158,158,170));
		pdfCell.setPaddingLeft(10f);
		pdfCell.setPaddingRight(10f);
		tempTable.addCell(pdfCell);
		tempTable.setSpacingBefore(10);
		tempTable.setSpacingAfter(10);
		tempTable.setWidthPercentage(100);
		
		
		doc.add(tempTable);
		return tempTable.getTotalHeight();
	}
	
	private void customerSectionBuilder(Document doc,
			final String customerFirstName, final String customerLastName,
			final String customerPhoneNumber, final String customerEmail)
			throws DocumentException {
		Cell cell;

		float[] customerInfoTableWidths = new float[] { 40, 20, 40 };
		Table customerInfoTable = new Table(3);
		customerInfoTable.setWidth(100);
		customerInfoTable.setPadding(5);
		customerInfoTable.setWidths(customerInfoTableWidths);

		stringBuilder = new StringBuilder();
		stringBuilder.append(customerFirstName);
		stringBuilder.append(" ");
		stringBuilder.append(customerLastName);
		Paragraph customerName = new Paragraph("Owner: ", HELVET_10_NORMAL);
		Chunk customerNameChunk = new Chunk(stringBuilder.toString(),
				HELVET_12_BOLD);
		customerName.add(customerNameChunk);
		cell = new Cell(customerName);
		cell.setBackgroundColor(TABLE_BG_COLOR);
		cell.setBorderWidthRight(Rectangle.NO_BORDER);
		customerInfoTable.addCell(cell);

		cell = new Cell(new Paragraph("Phone: " + customerPhoneNumber,
				HELVET_10_NORMAL));
		cell.setBackgroundColor(TABLE_BG_COLOR);
		cell.setBorderWidthRight(Rectangle.NO_BORDER);
		cell.setBorderWidthLeft(Rectangle.NO_BORDER);
		customerInfoTable.addCell(cell);

		cell = new Cell(new Paragraph("Email: " + customerEmail,
				HELVET_10_NORMAL));
		cell.setBackgroundColor(TABLE_BG_COLOR);
		cell.setBorderWidthLeft(Rectangle.NO_BORDER);
		customerInfoTable.addCell(cell);
		doc.add(customerInfoTable);

	}

	
	private void topSectionBuilder(Document doc, PdfWriter writer,
			String dealerLogoUrl) throws DocumentException {
		Paragraph topLine = new Paragraph();
		Chunk topLineText = new Chunk("APPRAISAL", HELVET_10_BOLD);
		topLineText.setTextRise(2);
		topLine.add(topLineText);
		topLine.setAlignment(Element.ALIGN_RIGHT);
		doc.add(topLine);
		if (dealerLogoUrl != null) { // Add the logo image as a top layer.
			PdfContentByte cb = writer.getDirectContent();
			Image logo;
			try {// If logo is in database but actual image is bad, do not add
					// it to page.
				System.out.println("loading logo " + PHOTO_LOC + dealerLogoUrl
						+ " at " + new Date());
				logo = Image.getInstance(PHOTO_LOC + dealerLogoUrl);
				System.out.println("finished loading logo at " + new Date());
				logo.scaleToFit(115, 80);
				float logoHeight = logo.height();
				// 806 is the y-loc of the line, since iText does positions from
				// the bottom left as (0, 0) need
				// to do this to align with top of the image with the top line.
				logoHeight = 806 - logoHeight / 2;
				logo.setAbsolutePosition(20, logoHeight);
				cb.addImage(logo);
				cb.fill();
			} catch (MalformedURLException e) {
				logger.info(new StringBuilder(
						"Photo not found for Appraisal Form, appraisalId: ")
						.append(appraisal.getAppraisalId()), e);
			} catch (IOException e) {
				logger.info(new StringBuilder(
						"Photo not found for Appraisal Form, appraisalId: ")
						.append(appraisal.getAppraisalId()), e);
			}
		}

		Graphic horizontalLine = new Graphic();
		horizontalLine.setHorizontalLine(1f, 100f);
		horizontalLine.setColorFill(new Color(153, 153, 153));
		doc.add(horizontalLine);

		Paragraph header = new Paragraph();
		header.setSpacingBefore(5);
		header.add(new Paragraph(dealer.getName(), HELVET_22_BOLD));
		header.add(new Paragraph("Appraisal", HELVET_21_BOLD));
		stringBuilder = new StringBuilder();
		stringBuilder.append(vehicle.getVehicleYear());
		stringBuilder.append(" ");
		stringBuilder.append(vehicle.getMake());
		stringBuilder.append(" ");
		stringBuilder.append(vehicle.getModel());
		stringBuilder.append(" ");
		stringBuilder.append(vehicle.getVehicleTrim());
		header.add(new Paragraph(stringBuilder.toString(), HELVET_12_BOLD));
		header.add(new Paragraph("(VIN #" + vehicle.getVin() + ")",
				HELVET_8_NORMAL));
		header.setAlignment(Element.ALIGN_CENTER);

		doc.add(header);
	}
	
	private void topSectionBuilderNew(Document doc, PdfWriter writer,
			String dealerLogoUrl) throws DocumentException {
		boolean flag=false;
		if (dealerLogoUrl != null) { // Add the logo image as a top layer.
			PdfContentByte cb = writer.getDirectContent();
			Image logo;
			try {// If logo is in database but actual image is bad, do not add
					// it to page.
				System.out.println("loading logo " + PHOTO_LOC + dealerLogoUrl
						+ " at " + new Date());
				logo = Image.getInstance(PHOTO_LOC + dealerLogoUrl);
				System.out.println("finished loading logo at " + new Date());
				logo.scaleToFit(115, 80);
				float logoHeight = logo.height();
				// 806 is the y-loc of the line, since iText does positions from
				// the bottom left as (0, 0) need
				// to do this to align with top of the image with the top line.
				logoHeight = PageSize.A4.height() - logoHeight / 2;
				logo.setAbsolutePosition(20, logoHeight);
				cb.addImage(logo);
				cb.fill();
			} catch (MalformedURLException e) {
				flag=true;
				logger.info(new StringBuilder(
						"Photo not found for Appraisal Form, appraisalId: ")
						.append(appraisal.getAppraisalId()), e);
			} catch (IOException e) {
				flag=true;
				logger.info(new StringBuilder(
						"Photo not found for Appraisal Form, appraisalId: ")
						.append(appraisal.getAppraisalId()), e);
			}
		}
		if(flag)
		{
			PdfPTable table = new PdfPTable(2);
			table.setWidthPercentage(100);
			table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			
			table.addCell(new Paragraph(dealer.getName(),HELVET_15_BOLD_GREY));
			
			Paragraph topLine = new Paragraph();
			Chunk topLineText = new Chunk("PURCHASE OFFER", HELVET_15_BOLD_BLUE);
			topLineText.setTextRise(2);
			topLine.add(topLineText);
			topLine.setAlignment(Element.ALIGN_RIGHT);
			PdfPCell cell = new PdfPCell(topLine);
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.addCell(cell);
			
			doc.add(table);
			
			
			
		}
		else{
			
			Paragraph topLine = new Paragraph();
			Chunk topLineText = new Chunk("PURCHASE OFFER", HELVET_15_BOLD_BLUE);
			topLineText.setTextRise(2);
			topLine.add(topLineText);
			topLine.setAlignment(Element.ALIGN_RIGHT);
			doc.add(topLine);
		}


	}

	public String formatPhoneNumber(String number) {
		if (number == null)// || number.length() != 10 )
		{
			return null;
		} else {
			PhoneNumber phoneNum = null;
			try {
				phoneNum = PhoneNumber.parse(number);
			} catch (PhoneNumberFormatException e) {
				logger.error("Invalid Phone Number: " + number, e);
			}

			return phoneNum.toPrettyString();
		}
	}

	public String getLogoPath(int guideBookId, String realPath) {
		String BB_LOGO = "/common/_images/logos/appraisalForm_bb_logo.png";
		String KBB_LOGO = "/common/_images/logos/appraisalForm_kbb_logo.png";
		String NADA_LOGO = "/common/_images/logos/appraisalForm_nada_logo.png";
		String GALVES_LOGO = "/common/_images/logos/appraisalForm_galves_logo.png";

		switch (guideBookId) {
		case GuideBook.GUIDE_TYPE_BLACKBOOK:
			return realPath + BB_LOGO;
		case GuideBook.GUIDE_TYPE_NADA:
			return realPath + NADA_LOGO;
		case GuideBook.GUIDE_TYPE_KELLEYBLUEBOOK:
			return realPath + KBB_LOGO;
		case GuideBook.GUIDE_TYPE_GALVES:
			return realPath + GALVES_LOGO;
		default:
			return null;
		}
	}

	private String convertNullToEmptyString(Object obj, String property) {
		try {
			if (obj != null) {
				Object prop;

				prop = PropertyUtils.getProperty(obj, property);
				if (prop != null)
					return convertNullToEmptyString(prop.toString());
				else
					return EMPTY_STRING;
			}
		} catch (IllegalAccessException e) {
			logger.debug(e);
		} catch (InvocationTargetException e) {
			logger.debug(e);
		} catch (NoSuchMethodException e) {
			logger.debug(e);
		}
		return EMPTY_STRING;
	}

	private String convertNullToEmptyString(String string) {
		if (string == null)
			return EMPTY_STRING;

		return string;
	}
	
	class CheckTableBG implements PdfPTableEvent{

		public void tableLayout(PdfPTable arg0, float[][] arg1, float[] arg2,
				int arg3, int arg4, PdfContentByte[] arg5) {
			PdfContentByte pdfContentByte = arg5[PdfPTable.LINECANVAS];
            
			try {
				Image checkImage = Image.getInstance(realPath + CHECK_BG_IMAGE);
				checkImage.scaleAbsolute(565, 230);
				
				
				pdfContentByte.addImage(checkImage,true);
			} catch (BadElementException e) {
				
				e.printStackTrace();
			} catch (MalformedURLException e) {
				
				e.printStackTrace();
			} catch (IOException e) {
				
				e.printStackTrace();
			} catch (DocumentException e) {
				
				e.printStackTrace();
			}
			
		}
	}
	
	class CheckCellBG implements PdfPCellEvent{

		public void cellLayout(PdfPCell arg0, Rectangle arg1,
				PdfContentByte[] arg2) {
			PdfContentByte pdfContentByte = arg2[PdfPTable.BACKGROUNDCANVAS];
			try {
				Image checkImage = Image.getInstance(realPath + CHECK_BG_IMAGE);
				checkImage.scaleAbsolute(565, 230);
				checkImage.setAbsolutePosition(arg1.left(),arg1.bottom()-34);
				
				pdfContentByte.addImage(checkImage,true);
			} catch (BadElementException e) {
				
				e.printStackTrace();
			} catch (MalformedURLException e) {
				
				e.printStackTrace();
			} catch (IOException e) {
				
				e.printStackTrace();
			} catch (DocumentException e) {
				
				e.printStackTrace();
			}
			
			
			PdfContentByte cb= arg2[PdfPTable.TEXTCANVAS];
			boolean flag=false;
			Image logo;
			try {// If logo is in database but actual image is bad, do not add
					// it to page.
				System.out.println("loading logo " + PHOTO_LOC + dealerLogoURL
						+ " at " + new Date());
				logo = Image.getInstance(PHOTO_LOC + dealerLogoURL);
				System.out.println("finished loading logo at " + new Date());
				logo.scaleToFit(95, 40);
				float logoHeight = logo.height();
				// 806 is the y-loc of the line, since iText does positions from
				// the bottom left as (0, 0) need
				// to do this to align with top of the image with the top line.
				logoHeight = PageSize.A4.height() - arg1.top()-30 ;
				logo.setAbsolutePosition(40, logoHeight);
				cb.addImage(logo);
				cb.fill();
				
			} catch (MalformedURLException e) {
				flag=true;
				logger.info(new StringBuilder(
						"Photo not found for Appraisal Form, appraisalId: ")
						.append(appraisal.getAppraisalId()), e);
			} catch (IOException e) {
				flag=true;
				logger.info(new StringBuilder(
						"Photo not found for Appraisal Form, appraisalId: ")
						.append(appraisal.getAppraisalId()), e);
			} catch (DocumentException e) {
				flag=true;
				e.printStackTrace();
			}
			if(flag)
			{
				Paragraph ph=new Paragraph(dealer.getName(), HELVET_10_BOLD);
				try {
					cb.setFontAndSize(BaseFont.createFont(BaseFont.HELVETICA, BaseFont.WINANSI, false), 10);
					
					 cb.beginText();
				     cb.showTextAligned(Element.ALIGN_LEFT, dealer.getName(), 50,arg1.top()-30, 0f);
				     cb.endText();
				     cb.stroke();
				} catch (DocumentException e) {
				
					e.printStackTrace();
				} catch (IOException e) {
					
					e.printStackTrace();
				}
				
			}
			
			
		}

		
	}
	class NumberWordConverter
	{
	  protected String pw(int n,String ch)
	  {
	    String  one[]={" "," one"," two"," three"," four"," five"," six"," seven"," eight"," nine"," ten"," eleven"," twelve"," thirteen"," fourteen","fifteen"," sixteen"," seventeen"," eighteen"," nineteen"};
	 
	    String ten[]={" "," "," twenty"," thirty"," forty"," fifty"," sixty","seventy"," eighty"," ninety"};
	    
	    
	    
	 
	    StringBuilder strBuilder= new StringBuilder();
	   
	    if(n>99){
	    	int m=n/100;
	    	strBuilder.append(one[m]+" hundred");
	    }
	    n=n%100;
	    if(n>19 ) { strBuilder.append(ten[n/10]+" "+one[n%10]);} else { strBuilder.append(one[n]);}
	    if(n>0)strBuilder.append(ch);
	    
	    return strBuilder.toString();
	  }
	  public String convert(Integer n)
	  {
	    
	 
	    if(n<=0)
    	{ 
	    	System.out.println("Enter numbers greater than 0");
	    	return "";
		}
	    else
	    {
	    	NumberWordConverter a = new NumberWordConverter();
	    	StringBuilder strBuilder= new StringBuilder();
	    	
	    	strBuilder.append(a.pw(((n/1000000000)%1000)," bilion"));
	    	//strBuilder.append(a.pw(((n/100000000)%100)," hundred"));
	    	strBuilder.append(a.pw(((n/1000000)%1000)," million"));
	    	//strBuilder.append(a.pw(((n/100000)%100)," hundred"));
	    	strBuilder.append(a.pw(((n/1000)%1000)," thousand"));
	    	strBuilder.append(a.pw(((n/100)%10)," hundred"));
	    	strBuilder.append(a.pw((n%100)," "));
	    	
	    	
	    	return strBuilder.toString();
	    }
	  }
	}
	
	public static boolean existsURL(String URLName){
	    try {
	      HttpURLConnection.setFollowRedirects(false);
	      // note : you may also need
	      //        HttpURLConnection.setInstanceFollowRedirects(false)
	      HttpURLConnection con =
	         (HttpURLConnection) new URL(URLName).openConnection();
	      con.setRequestMethod("HEAD");
	      return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
	    }
	    catch (Exception e) {
	       e.printStackTrace();
	       return false;
	    }
	  }
	
		
		
		
	

}
