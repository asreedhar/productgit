package com.firstlook.util.print;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import biz.firstlook.commons.util.PropertyLoader;

import com.firstlook.session.FirstlookSession;

public class PDFServlet extends javax.servlet.http.HttpServlet
{

private static final long serialVersionUID = 5298361607001058376L;
private final static int BUFFER_SIZE = 1024;
private final static java.lang.String PATH_PRINTER_FRIENDLY = PropertyLoader.getProperty( "firstlook.content.pdfviewer.directory" );

public PDFServlet()
{
    super();
}

protected void doGet( HttpServletRequest req, HttpServletResponse resp ) throws ServletException, IOException
{
    ServletOutputStream out = null;
    BufferedInputStream in = null;

    try
    {
        resp.setContentType( "application/pdf" );

        out = resp.getOutputStream();
        in = new BufferedInputStream( new FileInputStream( getFile( req ) ) );

        int result = 0;
        byte[] inBytes = new byte[BUFFER_SIZE];

        while ( ( result = in.read( inBytes ) ) != -1 )
        {
            out.write( inBytes, 0, result );
        }
    }
    finally
    {
        if ( in != null )
        {
            in.close();
        }
        if ( out != null )
        {
            out.close();
        }
    }
}

protected void doPost( HttpServletRequest req, HttpServletResponse resp ) throws ServletException, IOException
{
    doGet( req, resp );
}

protected File getFile( HttpServletRequest request )
{
    String filename = request.getParameter( "fileName" );

    int currentDealerId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();

    return new File( PATH_PRINTER_FRIENDLY + currentDealerId + "_" + filename );
}

}
