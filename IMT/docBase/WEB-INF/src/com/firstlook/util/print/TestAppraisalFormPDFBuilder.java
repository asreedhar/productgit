package com.firstlook.util.print;

import junit.framework.TestCase;

import com.firstlook.dealer.tools.GuideBook;

public class TestAppraisalFormPDFBuilder extends TestCase
{

public TestAppraisalFormPDFBuilder()
{
	super();
}

public void testGetLogoPath()
{
	AppraisalFormPDFBuilder tester = new AppraisalFormPDFBuilder( null );

	String expectedBBLogoPath = "/common/_images/logos/appraisalForm_bb_logo.gif";
	String expectedKBBLogoPath = "/common/_images/logos/appraisalForm_kbb_logo.gif";
	String expectedNADALogoPath = "/common/_images/logos/appraisalForm_nada_logo.gif";
	String expectedGLogoPath = "/common/_images/logos/appraisalForm_galves_logo.gif";

	assertEquals( expectedBBLogoPath, tester.getLogoPath( GuideBook.GUIDE_TYPE_BLACKBOOK, "" ) );
	assertEquals( expectedKBBLogoPath, tester.getLogoPath( GuideBook.GUIDE_TYPE_KELLEYBLUEBOOK, "" ) );
	assertEquals( expectedGLogoPath, tester.getLogoPath( GuideBook.GUIDE_TYPE_GALVES, "" ) );
	assertEquals( expectedNADALogoPath, tester.getLogoPath( GuideBook.GUIDE_TYPE_NADA, "" ) );
}

public void testFormatPhoneNumber()
{
	AppraisalFormPDFBuilder tester = new AppraisalFormPDFBuilder( null );

	String input = "1234567890";
	String expectedOutput = "(123)456-7890";
	String actualOutput = tester.formatPhoneNumber( input );

	assertEquals( expectedOutput, actualOutput );
}

}
