package com.firstlook.util;

public class PrimitiveUtil
{

public PrimitiveUtil()
{
}

public static int getIntValue( Integer value )
{
    int returnValue = 0;

    if ( value != null )
    {
        returnValue = value.intValue();
    }

    return returnValue;
}

}
