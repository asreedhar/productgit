package com.firstlook.util;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;

public class HttpClientTemplate
{

private HttpClient httpClient;
private MultiThreadedHttpConnectionManager connectionManager;
private int socket_timeout = 3500;
private int connection_timeout = 5000;
private int max_connections_per_host = 2;
private int max_total_connections = 20;

public HttpClientTemplate()
{
    HttpConnectionManagerParams httpConnectionManagerParams = new HttpConnectionManagerParams();
    httpConnectionManagerParams.setDefaultMaxConnectionsPerHost( max_connections_per_host );
    httpConnectionManagerParams.setMaxTotalConnections( max_total_connections );
    httpConnectionManagerParams.setSoTimeout(socket_timeout); 
    httpConnectionManagerParams.setConnectionTimeout(connection_timeout); //


    connectionManager = new MultiThreadedHttpConnectionManager();
    connectionManager.setParams( httpConnectionManagerParams );
	
	
	httpClient = new HttpClient( connectionManager );
}

public <T> T executeMethodWithCallback( HttpMethod method, HttpClientCallback<T> callback ) throws HttpException
{
	try
	{
		int statusCode = httpClient.executeMethod( method );
		return callback.doInHttpClient( statusCode, method );
	}
	catch ( HttpException e )
	{
		throw new HttpException( e.getMessage(), e );
	}
	catch ( IOException e )
	{
		throw new HttpException( e.getMessage(), e );
	}
	finally
	{
		method.releaseConnection();
	}
}

public void setConnection_timeout(int connection_timeout) {
	this.connection_timeout = connection_timeout;
}

public void setMax_connections_per_host(int max_connections_per_host) {
	this.max_connections_per_host = max_connections_per_host;
}

public void setMax_total_connections(int max_total_connections) {
	this.max_total_connections = max_total_connections;
}

public void setSocket_timeout(int socket_timeout) {
	this.socket_timeout = socket_timeout;
}


}
