package com.firstlook.util;

import java.util.Date;

import biz.firstlook.commons.util.DateUtilFL;

import com.firstlook.persistence.vehiclesale.VehicleSaleDAO;


public class SaleUtility
{
private VehicleSaleDAO imt_vehicleSaleDAO;

public SaleUtility()
{
	super();
}



public static long calculateDaysToSale(Date receivedDate, Date dealDate )
{
	if(receivedDate == null)
		return 0;
	
	if(dealDate == null)
		return 0;
	
	
	double milliDifference = ( dealDate.getTime() - receivedDate.getTime() );
	double dayDifference = ( milliDifference / DateUtilFL.MILLISECONDS_PER_DAY );

	long dayDiff = Math.round( dayDifference );

	if ( dayDiff < 1 )
	{
		return 1;
	}
	else
	{
		return dayDiff;
	}
}



public VehicleSaleDAO getImt_vehicleSaleDAO()
{
	return imt_vehicleSaleDAO;
}

public void setImt_vehicleSaleDAO( VehicleSaleDAO imt_vehicleSaleDAO )
{
	this.imt_vehicleSaleDAO = imt_vehicleSaleDAO;
}


}
