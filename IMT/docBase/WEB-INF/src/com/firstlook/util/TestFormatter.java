package com.firstlook.util;

import com.firstlook.mock.BaseTestCase;

public class TestFormatter extends BaseTestCase
{

public TestFormatter( String arg0 )
{
    super(arg0);
}

public void testFormatDoubleToPercent()
{
    double value = .25;

    assertEquals("Value should be 25%", "25%", Formatter.toPercent(value));
}

public void testFormatDoubleToPercentNAN()
{
    double value = Double.NaN;

    assertEquals("Value should be N/A", Formatter.NOT_AVAILABLE, Formatter
            .toPercent(value));
}

public void testFormatDoubleToPercentMultipleDecimalPlaces()
{
    double value = .256;

    assertEquals("Value should be 26%", "26%", Formatter.toPercent(value));
}

public void testToString()
{
    int value = 25;

    assertEquals("Should be 25", "25", Formatter.toString(value));
}

public void testToStringMinVal()
{
    int value = Integer.MIN_VALUE;

    assertEquals("Should be n/a", "n/a", Formatter.toString(value));
}

public void testToPrice()
{
    assertEquals("$1,000", Formatter.toPrice(1000));
}

public void testToPriceNegative()
{
    assertEquals("($1,000)", Formatter.toPrice(-1000));
}

public void testToPriceDouble()
{
    double value = 1000.0;
    assertEquals("$1,000", Formatter.toPrice(value));
}

public void testToPriceNegativeDouble()
{
    double value = -1000.0;
    assertEquals("($1,000)", Formatter.toPrice(value));
}

public void testToPercentRoundedUpLong()
{
    double value = .125555555555;
    assertEquals("Should be 13%", "13%", Formatter.toPercentRounded(value));
}

public void testToPercentRoundedUp()
{
    double value = .125;
    assertEquals("Should be 13%", "13%", Formatter.toPercentRounded(value));
}

public void testToPercentRoundedDown()
{
    double value = .121;
    assertEquals("Should be 12%", "12%", Formatter.toPercentRounded(value));
}

public void testFormatNumberToString()
{
    assertEquals("7,000", Formatter.formatNumberToString(7000));
}

public void testFormatNumberToStringWithMinVal()
{
    assertEquals("", Formatter.formatNumberToString(Integer.MIN_VALUE));
}

}
