package com.firstlook.util;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.firstlook.util.print.TestAppraisalFormPDFBuilder;

public class AllTests extends com.firstlook.mock.BaseTestCase
{

public AllTests( String arg1 )
{
	super( arg1 );
}

public static Test suite()
{
	TestSuite suite = new TestSuite();
	suite.addTest( new TestSuite( TestFormatter.class ) );
	suite.addTest( new TestSuite( TestHttpParameter.class ) );
	suite.addTest( new TestSuite( TestMathUtil.class ) );
	suite.addTest( new TestSuite( TestPageBreakHelper.class ) );
	suite.addTest( new TestSuite( TestPrimitiveUtil.class ) );
	suite.addTest( new TestSuite( TestPropertyLoader.class ) );
	suite.addTest( new TestSuite( TestStringUtilsFL.class ) );
	suite.addTest( new TestSuite( TestAppraisalFormPDFBuilder.class ) );

	return suite;
}
}
