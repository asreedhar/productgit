package com.firstlook.util;

import biz.firstlook.commons.util.PropertyLoader;

public class PageBreakHelper
{

private int totalPageUnits;
private int groupingUnits;
private int recordUnits;
private int usedPageUnits = 0;
private int betweenGroupingUnits;
private int topBottomUnits;
private boolean emptyHeadersAllowed = false;

public final static int GROUPING_UNITS_DEFAULT = 4;
public final static int RECORD_UNITS_DEFAULT = 2;
public final static int FIRST_PAGE_HEADER_UNITS_DEFAULT = 15;
public final static int TOTAL_UNITS_DEFAULT = 20;
public final static int TOP_BOTTOM_UNITS_DEFAULT = 1;
public final static int BETWEEN_GROUPING_UNITS_DEFAULT = 1;
public final static int NON_FIRST_PAGE_HEADER_UNITS_DEFAULT = 0;
public final static int FOOTER_UNITS_DEFAULT = 0;

public final static String CBG_PDF = "cbg.pdf";
public final static String CBG_FAXABLE = "cbg.faxable";
public final static String CBG_ONLINE = "cbg.online";
public final static String TMG_FAXABLE = "tmg.faxable";
public final static String CAP_FAXABLE = "cap.faxable";
public final static String DAY_OF_CAP_FAXABLE = "dayofcap.faxable";
public final static String DCAP_FAXABLE = "dcap.faxable";
public final static String TOTAL_EXCHANGE = "totalexchange.printable";
public final static String TAP_FAXABLE = "tap.faxable";
public final static String FULL_REPORTS_ONLINE = "fullreports.online";
public final static String AGING_ONLINE = "aging.online";
public final static String INVOVERVIEW_ONLINE = "invoverview.online";
public final static String PAP_ONLINE = "pap.online";
public final static String LOCATOR_ONLINE = "locator.online";
public final static String TA_ONLINE = "ta.online";
public final static String VD_ONLINE = "vd.online";
public final static String AGING_PLAN_ONLINE = "agingplan.online";
public final static String VD_PAP_ONLINE = "vd.pap.online";
public final static String DEAL_LOG = "deal.log";
public final static String PRICING_LIST = "pricinglist.printable";

private int firstPageHeaderUnits;
private java.lang.String type;
private boolean groupingAtTopOfPage;
private int nonFirstPageHeaderUnits;
private int footerUnits;

public PageBreakHelper( String pageType )
{
    this(pageType, null);
}

public PageBreakHelper( String pageType, String programType )
{
    super();

    groupingAtTopOfPage = true;
    type = pageType;
    firstPageHeaderUnits = PropertyLoader.getIntProperty(
            "firstlook.printing.firstpageheader." + pageType + "."
                    + programType, PropertyLoader.getIntProperty(
                    "firstlook.printing.firstpageheader." + pageType,
                    FIRST_PAGE_HEADER_UNITS_DEFAULT));
    groupingUnits = PropertyLoader.getIntProperty(
            "firstlook.printing.groupingunits." + pageType + "." + programType,
            PropertyLoader.getIntProperty("firstlook.printing.groupingunits."
                    + pageType, GROUPING_UNITS_DEFAULT));
    recordUnits = PropertyLoader.getIntProperty(
            "firstlook.printing.recordunits." + pageType + "." + programType,
            PropertyLoader.getIntProperty("firstlook.printing.recordunits."
                    + pageType, RECORD_UNITS_DEFAULT));
    totalPageUnits = PropertyLoader.getIntProperty(
            "firstlook.printing.totalunits." + pageType + "." + programType,
            PropertyLoader.getIntProperty("firstlook.printing.totalunits."
                    + pageType, TOTAL_UNITS_DEFAULT));
    topBottomUnits = PropertyLoader.getIntProperty(
            "firstlook.printing.topbottomunits." + pageType,
            TOP_BOTTOM_UNITS_DEFAULT);
    betweenGroupingUnits = PropertyLoader.getIntProperty(
            "firstlook.printing.betweengroupingunits." + pageType,
            BETWEEN_GROUPING_UNITS_DEFAULT);
    nonFirstPageHeaderUnits = PropertyLoader.getIntProperty(
            "firstlook.printing.nonfirstpageheaderunits." + pageType,
            NON_FIRST_PAGE_HEADER_UNITS_DEFAULT);
    footerUnits = PropertyLoader.getIntProperty(
            "firstlook.printing.footerunits." + pageType + "." + programType,
            PropertyLoader.getIntProperty("firstlook.printing.footerunits."
                    + pageType, FOOTER_UNITS_DEFAULT));
    totalPageUnits = totalPageUnits - (2 * topBottomUnits) - footerUnits;
    usedPageUnits = firstPageHeaderUnits;

}

public int getBetweenGroupingUnits()
{
    return betweenGroupingUnits;
}

public int getFirstPageHeaderUnits()
{
    return firstPageHeaderUnits;
}

public int getFooterUnits()
{
    return footerUnits;
}

public int getGroupingUnits()
{
    return groupingUnits;
}

public int getNonFirstPageHeaderUnits()
{
    return nonFirstPageHeaderUnits;
}

public int getRecordUnits()
{
    return recordUnits;
}

public int getTopBottomUnits()
{
    return topBottomUnits;
}

public int getTotalPageUnits()
{
    return totalPageUnits;
}

public java.lang.String getType()
{
    return type;
}

public int getUsedPageUnits()
{
    return usedPageUnits;
}

public boolean isEmptyHeadersAllowed()
{
    return emptyHeadersAllowed;
}

public boolean isLastPageRecord()
{
    return (usedPageUnits + recordUnits) > totalPageUnits;
}

public boolean isNewPageHeader()
{
    int requiredBetweenGroupingUnits;

    if ( groupingAtTopOfPage )
    {
        requiredBetweenGroupingUnits = 0;
    } else
    {
        requiredBetweenGroupingUnits = betweenGroupingUnits;
    }

    groupingAtTopOfPage = false;

    int unitsRequired = usedPageUnits + groupingUnits
            + requiredBetweenGroupingUnits;
    if ( !emptyHeadersAllowed )
    {
        unitsRequired = unitsRequired + recordUnits;
    }

    if ( unitsRequired > totalPageUnits )
    {
        startNewPageWithGroupingHeader();
        return true;
    }
    usedPageUnits = usedPageUnits + groupingUnits
            + requiredBetweenGroupingUnits;

    return false;
}

public boolean isNewPageRecord()
{
    if ( usedPageUnits + recordUnits > totalPageUnits )
    {
        usedPageUnits = nonFirstPageHeaderUnits;
        usedPageUnits = usedPageUnits + recordUnits;
        return true;
    }
    usedPageUnits = usedPageUnits + recordUnits;
    return false;
}

public boolean isNewPageRecordWithGrouping()
{

    if ( usedPageUnits + recordUnits > totalPageUnits )
    {
        usedPageUnits = nonFirstPageHeaderUnits;
        usedPageUnits = usedPageUnits + groupingUnits + recordUnits;
        return true;
    }
    usedPageUnits = usedPageUnits + recordUnits;
    return false;
}

public boolean isOnline()
{
    if ( type.endsWith(".online") )
    {
        return true;
    } else
    {
        return false;
    }
}

public void setBetweenGroupingUnits( int newBetweenGroupingUnits )
{
    betweenGroupingUnits = newBetweenGroupingUnits;
}

public void setEmptyHeadersAllowed( boolean newEmptyHeadersAllowed )
{
    emptyHeadersAllowed = newEmptyHeadersAllowed;
}

public void setFirstPageHeaderUnits( int newFirstPageHeaderUnits )
{
    firstPageHeaderUnits = newFirstPageHeaderUnits;
}

public void setFooterUnits( int newFooterUnits )
{
    footerUnits = newFooterUnits;
}

public void setGroupingUnits( int newGroupingUnits )
{
    groupingUnits = newGroupingUnits;
}

public void setNonFirstPageHeaderUnits( int newNonFirstPageHeaderUnits )
{
    nonFirstPageHeaderUnits = newNonFirstPageHeaderUnits;
}

public void setRecordUnits( int newRecordUnits )
{
    recordUnits = newRecordUnits;
}

public void setTopBottomUnits( int newTopBottomUnits )
{
    topBottomUnits = newTopBottomUnits;
}

public void setTotalPageUnits( int newTotalPageUnits )
{
    totalPageUnits = newTotalPageUnits;
}

public void setType( String newType )
{
    type = newType;
}

public void setUsedPageUnits( int newUsedPageUnits )
{
    usedPageUnits = newUsedPageUnits;
}

public int getResetUsedPageUnits()
{
    usedPageUnits = firstPageHeaderUnits;
    return usedPageUnits;
}

private void startNewPageWithGroupingHeader()
{
    usedPageUnits = nonFirstPageHeaderUnits;
    usedPageUnits = usedPageUnits + groupingUnits;
    groupingAtTopOfPage = true;
}

}
