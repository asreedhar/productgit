package com.firstlook.util;

import java.io.UnsupportedEncodingException;

public class TestHttpParameter extends com.firstlook.mock.BaseTestCase
{
private HttpParameter parameter;

/**
 * TestHttpParameter constructor comment.
 * 
 * @param arg1
 *            java.lang.String
 */
public TestHttpParameter( String arg1 )
{
    super(arg1);
}

public void setup()
{
    parameter = new HttpParameter("name", "value");
}

public void testConstructor()
{
    assertEquals("name", parameter.getName());
    assertEquals("value", parameter.getValue());
}

public void testGetURL() throws UnsupportedEncodingException
{
    String url = parameter.getURL();

    assertEquals(parameter.getName() + "=" + parameter.getValue(), url);
}
}
