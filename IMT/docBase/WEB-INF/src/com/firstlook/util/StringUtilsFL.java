package com.firstlook.util;

import java.util.StringTokenizer;

public class StringUtilsFL
{

public static int getNumberOfSubstringMatches( String str1, String str2 )
{
    int matches = 0;
    StringTokenizer str1Tokenizer = new StringTokenizer(str1);

    while (str1Tokenizer.hasMoreTokens())
    {
        StringTokenizer str2Tokenizer = new StringTokenizer(str2);
        String strToMatch = str1Tokenizer.nextToken().toUpperCase();

        while (str2Tokenizer.hasMoreTokens())
        {
            if ( strToMatch.indexOf(str2Tokenizer.nextToken().toUpperCase()) != -1 )
            {
                matches++;
            }
        }
    }

    return matches;
}
}
