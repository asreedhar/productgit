package com.firstlook.util;

import java.math.BigDecimal;

public class MathUtil
{

public MathUtil()
{
    super();
}

public static double toDouble( Integer value )
{
    if ( value != null )
    {
        return value.doubleValue();
    } else
    {
        return Double.NaN;
    }
}

public static int toIntPercent( double value )
{
    if ( Double.isNaN(value) )
    {
        return Integer.MIN_VALUE;
    } else
    {
        double percentValue = Math.round(value * 100);
        Double newValue = new Double(percentValue);

        return newValue.intValue();
    }
}

public static double toRoundedTwoPlaceDecimal( double value )
{
    BigDecimal bigDecimal = new BigDecimal(value);
    bigDecimal = bigDecimal.divide(new BigDecimal("1"), 2,
            BigDecimal.ROUND_HALF_UP);

    return bigDecimal.doubleValue();
}


/**
 * Takes the input value and rounds it to the nearest 25. 
 * Eg: 	19.27 = 25.00
 * 		35870.00 = 35875.00
 * 		0.5484	= 0.00
 * 		12.45 	= 0.00
 * 		12.50	= 25.00
 * 		-663.46 = -675
 * 
 * @param input
 * @return
 */
public static double roundToNearestTwentyFive(double input) {
	// use pos value for calc - apply sign last
	double inputToUse = Math.abs(input);
	// strip decimals
	int numberOfWholeTimes25GoesIntoInput = (int)inputToUse/25;
	
	double result = 25 * numberOfWholeTimes25GoesIntoInput;
	
	// round
	double remainder = (inputToUse-result) / 12.5;
	if (remainder >= 1) {
		result += 25;
	}

	// apply sign
	if (input < 0) {
		result = -result;
	}
	
	return result;
}

}
