package com.firstlook.util;

import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;

public interface HttpClientCallback<T>
{

public abstract T doInHttpClient( final int statusCode, final HttpMethod method ) throws HttpException;

}
