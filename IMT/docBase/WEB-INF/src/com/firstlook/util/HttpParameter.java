package com.firstlook.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class HttpParameter
{
private java.lang.String name;
private java.lang.String value;

public HttpParameter()
{
    super();
}

public HttpParameter( String name, String value )
{
    super();
    this.name = name;
    this.value = value;
}

public java.lang.String getName()
{
    return name;
}

public String getURL()
{
    try
    {
        return URLEncoder.encode(name, "UTF-8") + "="
                + URLEncoder.encode(value, "UTF-8");
    } catch (UnsupportedEncodingException e)
    {
        return "";
    }
}

public java.lang.String getValue()
{
    return value;
}

public void setName( java.lang.String newName )
{
    name = newName;
}

public void setValue( java.lang.String newValue )
{
    value = newValue;
}

}
