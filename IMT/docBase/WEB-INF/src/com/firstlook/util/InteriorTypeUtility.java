package com.firstlook.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class InteriorTypeUtility
{

private static final List<String> options;

static {
	options = new ArrayList<String>();
	options.add( "UNKNOWN" );
	options.add( "LEATHER" );
	options.add("LEATHERETTE" );
	options.add("VINYL" );
	options.add( "CLOTH" );
}

public static List<String> retrieveInteriorTypesForDropdowns() {
	return Collections.unmodifiableList(options);
}}
