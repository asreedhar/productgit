package com.firstlook.util;

import junit.framework.TestCase;

public class TestPageBreakHelper extends TestCase
{
private PageBreakHelper pageBreaker;
private int topBottomUnits;
private int totalUnits;
private int betweenGroupingUnits = 20;
private int footerUnits;

/**
 * TestPageBreakHelper constructor comment.
 * 
 * @param arg1
 *            java.lang.String
 */
public TestPageBreakHelper( String arg1 )
{
    super(arg1);
}

private void assertPageBreakHelperMethods( PageBreakHelper pageBreaker )
{
    assertEquals("grouping", 3, pageBreaker.getGroupingUnits());
    assertEquals("record", 1, pageBreaker.getRecordUnits());
    assertEquals("topbottomunits", topBottomUnits, pageBreaker
            .getTopBottomUnits());
    assertEquals("totalunits", totalUnits - (2 * topBottomUnits) - footerUnits,
            pageBreaker.getTotalPageUnits());
    assertTrue("emptyheader", !pageBreaker.isEmptyHeadersAllowed());
}

public void setUp()
{
    totalUnits = 13;
    topBottomUnits = 1;
    footerUnits = 1;

    pageBreaker = new PageBreakHelper(PageBreakHelper.CBG_ONLINE);
    pageBreaker.setTotalPageUnits(10); // totalPageUnits - ( 2 * topBottomUnits
    // ) - footerUnits
    pageBreaker.setTopBottomUnits(1);
    pageBreaker.setBetweenGroupingUnits(1);
    pageBreaker.setGroupingUnits(3);
    pageBreaker.setRecordUnits(2);
    pageBreaker.setEmptyHeadersAllowed(false);
    pageBreaker.setUsedPageUnits(0);
}

private void setupProperties( String pageType )
{
    System.setProperty("firstlook.printing.firstpageheader." + pageType, "5");
    System.setProperty("firstlook.printing.groupingunits." + pageType, "3");
    System.setProperty("firstlook.printing.recordunits." + pageType, "1");
    System.setProperty("firstlook.printing.totalunits." + pageType, String
            .valueOf(totalUnits));
    System.setProperty("firstlook.printing.topbottomunits." + pageType, String
            .valueOf(topBottomUnits));
    System.setProperty("firstlook.printing.betweengroupingunits." + pageType,
            String.valueOf(betweenGroupingUnits));
    System.setProperty("firstlook.printing.footerunits." + pageType, String
            .valueOf(footerUnits));
}

public void testBetweenGroupings()
{
    assertTrue("Should be new page", !pageBreaker.isNewPageHeader());
    assertTrue("Group1, Record1", !pageBreaker.isNewPageRecord());
    assertTrue("Group2 should be new page", pageBreaker.isNewPageHeader());
}

public void testConstructorCBGFaxable()
{
    setupProperties(PageBreakHelper.CBG_FAXABLE);

    pageBreaker = new PageBreakHelper(PageBreakHelper.CBG_FAXABLE);
    assertEquals("faxable used page units", 5, pageBreaker.getUsedPageUnits());
    assertEquals("faxable first page header units", 5, pageBreaker
            .getFirstPageHeaderUnits());
    assertEquals("faxable between groupings", betweenGroupingUnits, pageBreaker
            .getBetweenGroupingUnits());
    assertTrue("faxable", !pageBreaker.isOnline());
    assertPageBreakHelperMethods(pageBreaker);
}

public void testConstructorCBGOnline()
{
    setupProperties(PageBreakHelper.CBG_ONLINE);

    pageBreaker = new PageBreakHelper(PageBreakHelper.CBG_ONLINE);
    assertEquals("online used page units", 5, pageBreaker.getUsedPageUnits());
    assertEquals("online first page header units", 5, pageBreaker
            .getFirstPageHeaderUnits());
    assertTrue("online", pageBreaker.isOnline());
    assertPageBreakHelperMethods(pageBreaker);
}

public void testConstructorCBGPdf()
{

    setupProperties(PageBreakHelper.CBG_PDF);
    pageBreaker = new PageBreakHelper(PageBreakHelper.CBG_PDF);

    assertEquals("pdf used page units", 5, pageBreaker.getUsedPageUnits());
    assertEquals("pdf first page header units", 5, pageBreaker
            .getFirstPageHeaderUnits());
    assertTrue("pdf", !pageBreaker.isOnline());
    assertPageBreakHelperMethods(pageBreaker);
}

public void testConstructorTMGFaxable()
{
    setupProperties(PageBreakHelper.TMG_FAXABLE);

    pageBreaker = new PageBreakHelper(PageBreakHelper.TMG_FAXABLE);
    assertEquals("faxable used page units", 5, pageBreaker.getUsedPageUnits());
    assertEquals("faxable first page header units", 5, pageBreaker
            .getFirstPageHeaderUnits());
    assertTrue("faxable", !pageBreaker.isOnline());
    assertPageBreakHelperMethods(pageBreaker);
}

public void testFirstRecord()
{
    assertTrue("Should not be a new page on 1st header", !pageBreaker
            .isNewPageHeader());
    assertTrue("Should not be a new page on 1st record", !pageBreaker
            .isNewPageRecord());

}

public void testIsLastPageRecord()
{
    for (int i = 0; i < 5; i++)
    {
        assertTrue("should not be last record on page", !pageBreaker
                .isLastPageRecord());
        pageBreaker.isNewPageRecord();
    }
    assertTrue("5th record should be last record on page", pageBreaker
            .isLastPageRecord());
}

public void testStartNewPageRecord_WithNonFirstPageHeader()
{
    pageBreaker.setNonFirstPageHeaderUnits(2);

    for (int i = 0; i < 5; i++)
    {
        assertTrue("record " + i + "should not be a new Page", !pageBreaker
                .isNewPageRecord());
    }
    assertTrue("6th record should be a new page", pageBreaker.isNewPageRecord());

    for (int i = 0; i < 3; i++)
    {
        assertTrue("record should not be new page", !pageBreaker
                .isNewPageRecord());
    }
    assertTrue("10th record should be a new page", pageBreaker
            .isNewPageRecord());
}

public void testStartNewPageRecordNoEmptyHeader()
{
    pageBreaker.setEmptyHeadersAllowed(false);
    pageBreaker.setTotalPageUnits(9);
    for (int i = 0; i < 2; i++)
    {
        assertTrue("header should not be a new page", !pageBreaker
                .isNewPageHeader());
    }
    assertTrue(
            "3rd header should be a new page because it would leave an empty header",
            pageBreaker.isNewPageHeader());
}

public void testStartNewPageRecordWith2Pages20Records()
{
    for (int i = 0; i < 5; i++)
    {
        assertTrue("record " + i + "should not be a new Page", !pageBreaker
                .isNewPageRecord());
    }
    assertTrue("6th record should be a new page", pageBreaker.isNewPageRecord());

    for (int i = 0; i < 4; i++)
    {
        assertTrue("record should not be new page", !pageBreaker
                .isNewPageRecord());
    }
    assertTrue("11th record should be a new page", pageBreaker
            .isNewPageRecord());
}

public void testStartNewPageRecordWith5Records()
{
    for (int i = 0; i < 5; i++)
    {
        assertTrue("Should not be new Page", !pageBreaker.isNewPageRecord());
    }
    assertTrue("6th Record should be a new Page", pageBreaker.isNewPageRecord());

}

public void testStartNewPageRecordWithHeaders()
{
    for (int i = 0; i < 2; i++)
    {
        assertTrue("header should not be a new page", !pageBreaker
                .isNewPageHeader());
    }
    assertTrue("3rd header should be a new page", pageBreaker.isNewPageHeader());
}

public void testStartNewPageRecordWithHeaders_WithNonFirstPageHeader()
{
    pageBreaker.setNonFirstPageHeaderUnits(3);

    for (int i = 0; i < 2; i++)
    {
        assertTrue("1st 2 headers should not be a new page", !pageBreaker
                .isNewPageHeader());
    }
    assertTrue("3rd header should be a new page", pageBreaker.isNewPageHeader());

    assertTrue("4th header should be a new page", pageBreaker.isNewPageHeader());
}

public void testStartNewPageRecordWithHeaders2Pages()
{
    for (int i = 0; i < 2; i++)
    {
        assertTrue("1 header should not be a new page", !pageBreaker
                .isNewPageHeader());
    }
    assertTrue("3rd header should be a new page", pageBreaker.isNewPageHeader());

    for (int i = 0; i < 1; i++)
    {
        assertTrue("2 header should not be a new page", !pageBreaker
                .isNewPageHeader());
    }
    assertTrue("6th header should be a new page", pageBreaker.isNewPageHeader());
}

public void testStartNewPageRecordWithRecordsAndHeaders()
{
    assertTrue("1st Header should not be a new page", !pageBreaker
            .isNewPageHeader());
    for (int i = 0; i < 3; i++)
    {
        assertTrue("record should not be a new page", !pageBreaker
                .isNewPageRecord());
    }
    assertTrue("3rd record should be a new page", pageBreaker.isNewPageRecord());
}
}
