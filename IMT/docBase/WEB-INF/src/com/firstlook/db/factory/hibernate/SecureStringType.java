package com.firstlook.db.factory.hibernate;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.apache.commons.codec.binary.Base64;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;

public class SecureStringType implements UserType, Serializable
{

private static final long serialVersionUID = 555285171389199484L;

public SecureStringType()
{
    super();
}

public int[] sqlTypes()
{
    return new int[]
    { Types.VARCHAR };
}

public Class returnedClass()
{
    return String.class;
}

public boolean equals( Object arg0, Object arg1 ) throws HibernateException
{
    return ((String) arg0).equals((String) arg1);
}

public Object nullSafeGet( ResultSet rs, String[] names, Object arg2 )
        throws HibernateException, SQLException
{
    String storedValue = rs.getString(names[0]);

    // Encryption Hook
    return new String(Base64.decodeBase64(storedValue.getBytes()));
}

public void nullSafeSet( PreparedStatement arg0, Object string, int arg2 )
        throws HibernateException, SQLException
{
    String encoded = null;
    if ( string != null && string instanceof String )
    {
        encoded = new String(Base64.encodeBase64(((String) string).getBytes()));
    }

    Hibernate.STRING.nullSafeSet(arg0, encoded, arg2);
}

public Object deepCopy( Object arg0 ) throws HibernateException
{
    return (String) arg0;
}

public boolean isMutable()
{
    return false;
}

public int hashCode( Object arg0 ) throws HibernateException
{
    return 0;
}

public Serializable disassemble( Object arg0 ) throws HibernateException
{
    return null;
}

public Object assemble( Serializable arg0, Object arg1 )
        throws HibernateException
{
    return null;
}

public Object replace( Object arg0, Object arg1, Object arg2 )
        throws HibernateException
{
    return null;
}

}
