package com.firstlook.db.factory.hibernate;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;

public class DoubleDoubleType implements UserType, Serializable
{

private static final long serialVersionUID = -9061800714513441120L;

public DoubleDoubleType()
{
    super();
}

public int[] sqlTypes()
{
    return new int[]
    { Types.DOUBLE };
}

public Class returnedClass()
{
    return double.class;
}

public boolean equals( Object arg0, Object arg1 ) throws HibernateException
{
    return false;
}

public Object nullSafeGet( ResultSet rs, String[] names, Object arg2 )
        throws HibernateException, SQLException
{
    double doubleVal = rs.getDouble(names[0]);
    return new Double(doubleVal);
}

public void nullSafeSet( PreparedStatement arg0, Object arg1, int arg2 )
        throws HibernateException, SQLException
{
    Hibernate.DOUBLE.nullSafeSet(arg0, arg1, arg2);
}

public Object deepCopy( Object arg0 ) throws HibernateException
{
    return null;
}

public boolean isMutable()
{
    return false;
}

public int hashCode( Object arg0 ) throws HibernateException
{
    return 0;
}

public Serializable disassemble( Object arg0 ) throws HibernateException
{
    return null;
}

public Object assemble( Serializable arg0, Object arg1 )
        throws HibernateException
{
    return null;
}

public Object replace( Object arg0, Object arg1, Object arg2 )
        throws HibernateException
{
    return null;
}

}
