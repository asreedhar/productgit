package com.firstlook.db.factory.hibernate;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;

public class IntegerIntType implements UserType, Serializable
{

private static final long serialVersionUID = -5598089316001922448L;

public IntegerIntType()
{
    super();
}

public int[] sqlTypes()
{
    return new int[]
    { Types.INTEGER };
}

public Class returnedClass()
{
    return int.class;
}

public boolean equals( Object arg0, Object arg1 ) throws HibernateException
{
    if ( arg0 == arg1 )
    {
        return true;
    } else if ( arg0 == null || arg1 == null )
    {
        return false;
    } else
    {
        return arg0.equals(arg1);
    }
}

public Object nullSafeGet( ResultSet rs, String[] names, Object arg2 )
        throws HibernateException, SQLException
{
    int intVal = rs.getInt(names[0]);
    return new Integer(intVal);
}

public void nullSafeSet( PreparedStatement arg0, Object arg1, int arg2 )
        throws HibernateException, SQLException
{
    Hibernate.INTEGER.nullSafeSet(arg0, arg1, arg2);
}

public Object deepCopy( Object arg0 ) throws HibernateException
{
    if ( arg0 == null )
    {
        return null;
    } else
    {
        Integer newInt = (Integer) arg0;
        return new Integer(newInt.intValue());
    }
}

public boolean isMutable()
{
    return false;
}

public int hashCode( Object arg0 ) throws HibernateException
{
    return 0;
}

public Serializable disassemble( Object arg0 ) throws HibernateException
{
    return null;
}

public Object assemble( Serializable arg0, Object arg1 )
        throws HibernateException
{
    return null;
}

public Object replace( Object arg0, Object arg1, Object arg2 )
        throws HibernateException
{
    return null;
}

}
