package com.firstlook.email;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.io.Resource;

import biz.firstlook.commons.email.EmailContextBuilder;
import biz.firstlook.commons.email.EmailService.EmailFormat;
import biz.firstlook.commons.util.PropertyLoader;

public abstract class LithiaCarCenterBaseEmailBuilder implements EmailContextBuilder {

protected String dealerName;
protected String year;
protected String make;
protected String model;
protected int mileage;
protected int appraisalId;
protected String toEmailAddress;
protected List<String> ccEmailAddresses;
protected String replyToEmailAddress;

protected LithiaCarCenterBaseEmailBuilder( String dealerName, int year, String make, String model,
                                                         int mileage, int appraisalId, String toEmailAddress, 
                                                         List<String> ccEmailAddresses, String replyToEmailAddress) {
	this.dealerName = dealerName;
	this.year = year + "";
	this.make = make;
	this.model = model;
	this.mileage = mileage;
	this.appraisalId = appraisalId;
	this.toEmailAddress = toEmailAddress;
	this.ccEmailAddresses = ccEmailAddresses;
	this.replyToEmailAddress = replyToEmailAddress;
}

public Map< String, EmailFormat > getEmailFormats()
{
	Map< String, EmailFormat > emailFormats = new HashMap< String, EmailFormat >();
	emailFormats.put( toEmailAddress, EmailFormat.PLAIN_TEXT );
	return emailFormats;
}

public abstract String getSubject();
public abstract String getTemplateName();
public abstract Map< String, Map< String, Object > > getRecipientsAndContext();

public List< String > getCcEmailAddresses()
{
	return ccEmailAddresses;
}

protected String getDateString() {
	SimpleDateFormat dayFormat = new SimpleDateFormat("MMM dd, yyyy");
	String day = dayFormat.format( Calendar.getInstance().getTime() );
	SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");
	String time = timeFormat.format( Calendar.getInstance().getTime() );
	return day + " at " + time;
}

protected String getLithiaCarCenterLink() {
	return PropertyLoader.getProperty( "edge.url" ) + "LithiaCarCenterAction.go?LithiaCarCenterAppraisalId=" + appraisalId;
}

public Map< String, Resource > getEmbeddedResources()
{
	return null;
}

public Map< String, String > getEmbeddedResourcesContentTypes()
{
	return null;
}

public String getReplyTo()
{
	return replyToEmailAddress;
}

}
