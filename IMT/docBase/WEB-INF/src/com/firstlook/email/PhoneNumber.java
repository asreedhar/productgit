package com.firstlook.email;

import java.text.DecimalFormat;

/**
 * Represents a phone number and it's fields. The minimum representation contains area code, exchange, and the subscriber number. Only supports
 * US & Canadian phone numbers. Extensions are not supported.
 * 
 * ex: 3122791245 or 312-279-1245 is a minimum phone number.
 * 
 * This class can be enhanced for later as a persistant component class for Member, Dealer, and so forth.
 * 
 * @author bfung
 */
public class PhoneNumber
{

private Integer countryCode;
private Integer areaCode;
private Integer exchange;
private Integer subscriberNumber;

/**
 * Defaults the country code to US, which is 1.
 * 
 * ex: 1-312-279-1245
 * 
 * @param areaCode
 *            the three digits following the country code
 * @param exchange
 *            the three digits following the area code
 * @param subscriberNumber
 *            the last four digits
 */
public PhoneNumber( final Integer areaCode, final Integer exchange, final Integer subscriberNumber )
{
	this.countryCode = new Integer( 1 );
	this.areaCode = areaCode;
	this.exchange = exchange;
	this.subscriberNumber = subscriberNumber;
}

public Integer getAreaCode()
{
	return areaCode;
}

public Integer getCountryCode()
{
	return countryCode;
}

public Integer getExchange()
{
	return exchange;
}

public Integer getSubscriberNumber()
{
	return subscriberNumber;
}

/**
 * Parses a string into a PhoneNumber object. Only supports US & Canadian phone numbers. Extensions are not supported.
 * 
 * @param phoneNumber
 * @return a PhoneNumber object constructed from the parsed value.
 */
public static PhoneNumber parse( final String phoneNumber ) throws PhoneNumberFormatException
{
	Integer area = null;
	Integer exch = null;
	Integer num = null;

	int characterCount = phoneNumber.length();

	if ( characterCount == 14 || characterCount == 12 )
	{
		String[] fields = phoneNumber.split( "-" );
		if ( fields.length == 3 || fields.length == 4 )
		{
			int offset = fields.length - 3;
			try
			{
				area = new Integer( fields[0 + offset] );
				exch = new Integer( fields[1 + offset] );
				num = new Integer( fields[2 + offset] );
			}
			catch ( NumberFormatException nfe )
			{
				throw new PhoneNumberFormatException( "Invalid phone number ", nfe );
			}
		}
		else
		{
			StringBuilder sb = new StringBuilder( "The phone number " ).append( phoneNumber ).append( " contains too many hypens." );
			throw new PhoneNumberFormatException( sb.toString() );
		}
	}
	else if ( characterCount == 11 || characterCount == 10 )
	{
		// disallow country code other than 1
		int offset = characterCount - 10;
		try
		{
			area = new Integer( phoneNumber.substring( 0 + offset, 3 + offset ) );
			exch = new Integer( phoneNumber.substring( 3 + offset, 6 + offset ) );
			num = new Integer( phoneNumber.substring( 6 + offset ) );
		}
		catch ( NumberFormatException nfe )
		{
			throw new PhoneNumberFormatException( "Invalid phone number ", nfe );
		}
	}
	else
	{
		StringBuilder sb = new StringBuilder( "The phone number " ).append( phoneNumber ).append(
																									" did not contain an area code, exchange, and subscriber number." );
		throw new PhoneNumberFormatException( sb.toString() );
	}

	PhoneNumber newNumber = new PhoneNumber( area, exch, num );

	return newNumber;
}

/**
 * @return a naive concatentaion of all fields of the PhoneNumber. ex: 13122791245
 */
public String toString()
{
	return new StringBuilder().append( countryCode ).append( areaCode ).append( exchange ).append( subscriberNumber ).toString();
}

/**
 * @return a concatentation of fields of the PhoneNumber seperated by hypens. ex: 1-312-279-1245
 */
public String toPrettyString()
{
	String subscriberNumberStr = new DecimalFormat("0000").format(subscriberNumber);
	return new StringBuilder().append( countryCode ).append( "-" ).append( areaCode ).append( "-" ).append( exchange ).append( "-" ).append( subscriberNumberStr ).toString();
}

// Eclipse generated code
@Override
public int hashCode()
{
	final int PRIME = 31;
	int result = 1;
	result = PRIME * result + ( ( areaCode == null ) ? 0 : areaCode.hashCode() );
	result = PRIME * result + ( ( countryCode == null ) ? 0 : countryCode.hashCode() );
	result = PRIME * result + ( ( exchange == null ) ? 0 : exchange.hashCode() );
	result = PRIME * result + ( ( subscriberNumber == null ) ? 0 : subscriberNumber.hashCode() );
	return result;
}

// Eclipse generated code
@Override
public boolean equals( Object obj )
{
	if ( this == obj )
		return true;
	if ( obj == null )
		return false;
	if ( getClass() != obj.getClass() )
		return false;
	final PhoneNumber other = (PhoneNumber)obj;
	if ( areaCode == null )
	{
		if ( other.areaCode != null )
			return false;
	}
	else if ( !areaCode.equals( other.areaCode ) )
		return false;
	if ( countryCode == null )
	{
		if ( other.countryCode != null )
			return false;
	}
	else if ( !countryCode.equals( other.countryCode ) )
		return false;
	if ( exchange == null )
	{
		if ( other.exchange != null )
			return false;
	}
	else if ( !exchange.equals( other.exchange ) )
		return false;
	if ( subscriberNumber == null )
	{
		if ( other.subscriberNumber != null )
			return false;
	}
	else if ( !subscriberNumber.equals( other.subscriberNumber ) )
		return false;
	return true;
}

}
