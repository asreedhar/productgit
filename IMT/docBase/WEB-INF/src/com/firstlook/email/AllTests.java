package com.firstlook.email;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests extends TestSuite
{

public AllTests( String arg0 )
{
	super( arg0 );
}

public static Test suite()
{
    TestSuite suite = new TestSuite();
    
    suite.addTestSuite( TestPhoneNumber.class );

    return suite;
}

}
