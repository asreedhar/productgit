package com.firstlook.email;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This email builder is used to generate the e-mails that get sent to the Lithia Car Center when a user has changed the 
 * Appraisal Action (aka inventory decision) in the Trade Manager.  It extands the LithiaCarCenterBaseEmailBuilder which has the
 * fields common to all car center e-mails.
 * 
 * @author dweintrop
 */
public class LithiaCarCenterAppraisalActionChangeEmailBuilder extends LithiaCarCenterBaseEmailBuilder
{

private String appraisalAction;

public LithiaCarCenterAppraisalActionChangeEmailBuilder( String dealerName, int year, String make, String model, int mileage,
																int appraisalId, String toEmailAddress, List< String > ccEmailAddresses,
																String appraisalAction, String replyToEmailAddress )
	{
		super( dealerName, year, make, model, mileage, appraisalId, toEmailAddress, ccEmailAddresses, replyToEmailAddress );
		this.appraisalAction = appraisalAction;
	}

public Map< String, Map< String, Object >> getRecipientsAndContext()
{
	Map< String, Object > vehicle = new HashMap< String, Object >();
	vehicle.put( "year", year );
	vehicle.put( "make", make );
	vehicle.put( "model", model );
	vehicle.put( "mileage", mileage );
	
	Map< String, Object > smsContext = new HashMap< String, Object >();
	smsContext.put( "vehicle", vehicle );
	smsContext.put( "lithiaCarCenterLink", getLithiaCarCenterLink() );
	smsContext.put( "date", getDateString() );
	smsContext.put( "dealerName", dealerName );
	smsContext.put( "appraisalAction", appraisalAction );
	
	Map< String, Map< String, Object > > recipientsAndContexts = new HashMap< String, Map< String, Object > >();
	recipientsAndContexts.put( toEmailAddress, smsContext );
	
	return recipientsAndContexts;
}

public String getSubject()
{
	return "Appraisal Change Alert from " + dealerName + " - " + getDateString();
}

public String getTemplateName()
{
	return "lithiaCarCenterAppraisalActionChangeNotification";
}

}
