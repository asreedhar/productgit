package com.firstlook.email;

import junit.framework.TestCase;

public class TestPhoneNumber extends TestCase
{

private PhoneNumber expectedPhoneNumber;

public TestPhoneNumber( String arg0 )
{
	super( arg0 );
}

public void setUp()
{
	expectedPhoneNumber = new PhoneNumber( 312, 279, 1245 );
}

public void tearDown()
{
	expectedPhoneNumber = null;
}

public void testParseInvalidPhoneNumber()
{
	boolean exceptionCaught = false;
	try
	{
		PhoneNumber.parse( "" );
	}
	catch ( PhoneNumberFormatException e )
	{
		exceptionCaught = true;
	}
	
	assertTrue( exceptionCaught );
}

public void testParseShortPhoneNumber()
{
	PhoneNumber newNumber = null;
	try
	{
		// chop off the country code
		newNumber = PhoneNumber.parse( "3122791245" );
	}
	catch ( PhoneNumberFormatException e )
	{
		fail( "The Phone Number: 3122791245 did not parse correctly." );
	}

	assertEquals( expectedPhoneNumber, newNumber  );
}

public void testParseShortPrettyPhoneNumber()
{
	PhoneNumber newNumber = null;
	try
	{
		// chop off the country code
		newNumber = PhoneNumber.parse( "312-279-1245" );
	}
	catch ( PhoneNumberFormatException e )
	{
		fail( "The Phone Number: 312-279-1245 did not parse correctly." );
	}

	assertEquals( expectedPhoneNumber, newNumber  );
}

public void testParseLongPhoneNumber()
{
	PhoneNumber newNumber = null;
	try
	{
		newNumber = PhoneNumber.parse( "13122791245" );
	}
	catch ( PhoneNumberFormatException e )
	{
		fail( "The Phone Number: 13122791245 did not parse correctly." );
	}

	assertEquals( expectedPhoneNumber, newNumber  );
}

public void testParseLongPrettyPhoneNumber()
{
	PhoneNumber newNumber = null;
	try
	{
		newNumber = PhoneNumber.parse( "1-312-279-1245" );
	}
	catch ( PhoneNumberFormatException e )
	{
		fail( "The Phone Number: 1-312-279-1245 did not parse correctly." );
	}

	assertEquals( expectedPhoneNumber, newNumber );
}

public void testToString()
{
	assertEquals( "13122791245", expectedPhoneNumber.toString() );
}

public void testToPrettyString()
{
	assertEquals( "1-312-279-1245", expectedPhoneNumber.toPrettyString() );
}

}
