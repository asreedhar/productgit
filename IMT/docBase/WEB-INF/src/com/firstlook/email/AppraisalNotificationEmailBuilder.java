package com.firstlook.email;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.core.io.Resource;

import biz.firstlook.commons.email.EmailContextBuilder;
import biz.firstlook.commons.email.EmailService.EmailFormat;

public class AppraisalNotificationEmailBuilder implements EmailContextBuilder
{

private String fromMemberPreferredFirstName;
private String fromMemberFirstName;
private String fromMemberLastName;
private int vehicleYear;
private String vehicleMake;
private String vehicleModel;
private String vehicleMileage;
private String[] toManagers;

public AppraisalNotificationEmailBuilder( String fromMemberPreferredFirstName, String fromMemberFirstName, String fromMemberLastName,
											int vehicleYear, String vehicleMake, String vehicleModel, String vehicleMileage,
											String... toManagers )
{
	super();
	this.fromMemberPreferredFirstName = fromMemberPreferredFirstName;
	this.fromMemberFirstName = fromMemberFirstName;
	this.fromMemberLastName = fromMemberLastName;
	this.vehicleYear = vehicleYear;
	this.vehicleMake = vehicleMake;
	this.vehicleModel = vehicleModel;
	this.vehicleMileage = vehicleMileage;
	this.toManagers = toManagers;
}

private String getFirstName()
{
	return StringUtils.isBlank( fromMemberPreferredFirstName ) ? fromMemberFirstName : fromMemberPreferredFirstName;
}

public Map< String, Map< String, Object > > getRecipientsAndContext()
{
	Map< String, Object > vehicle = new HashMap< String, Object >();
	vehicle.put( "year", vehicleYear );
	vehicle.put( "make", vehicleMake );
	vehicle.put( "model", vehicleModel );
	vehicle.put( "mileage", vehicleMileage );

	Map< String, Object > appraiser = new HashMap< String, Object >();
	appraiser.put( "firstName", getFirstName() );
	appraiser.put( "lastName", fromMemberLastName );

	Map< String, Object > smsContext = new HashMap< String, Object >();
	smsContext.put( "vehicle", vehicle );
	smsContext.put( "appraiser", appraiser );

	Map< String, Map< String, Object > > recipientsAndContexts = new HashMap< String, Map< String, Object > >();
	for ( String emailAddress : toManagers )
	{
		recipientsAndContexts.put( emailAddress, smsContext );
	}
	return recipientsAndContexts;
}

public String getSubject()
{
	StringBuffer subject = new StringBuffer();
	subject.append( "Appraisal Alert from " );
	subject.append( getFirstName() ).append( " " ).append( fromMemberLastName );
	return subject.toString();
}

public String getTemplateName()
{
	return "appraisalNotification";
}

public Map< String, EmailFormat > getEmailFormats()
{
	Map< String, EmailFormat > emailFormats = new HashMap< String, EmailFormat >();
	for ( String emailAddress : toManagers )
	{
		emailFormats.put( emailAddress, EmailFormat.PLAIN_TEXT );
	}
	return emailFormats;
}

public Map< String, Resource > getEmbeddedResources()
{
	return null;
}

public Map< String, String > getEmbeddedResourcesContentTypes()
{
	return null;
}

public String getReplyTo()
{
	return null;
}

public List< String > getCcEmailAddresses()
{
	return Collections.EMPTY_LIST;
}

}
