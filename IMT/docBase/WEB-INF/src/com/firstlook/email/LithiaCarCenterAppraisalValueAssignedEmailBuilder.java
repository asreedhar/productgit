package com.firstlook.email;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

public class LithiaCarCenterAppraisalValueAssignedEmailBuilder extends LithiaCarCenterBaseEmailBuilder
{

private String appraiserName;
private int appraisalValue;

public LithiaCarCenterAppraisalValueAssignedEmailBuilder( String dealerName, int year, String make, String model, int mileage,
																	int appraisalId, String toEmailAddress, List< String > ccEmailAddresses,
																	String appraiserName, int appraisalValue, String replyToEmailAddress)
	{
		super( dealerName, year, make, model, mileage, appraisalId, toEmailAddress, ccEmailAddresses, replyToEmailAddress );
		this.appraiserName = appraiserName;
		this.appraisalValue = appraisalValue;
	}

@Override
public Map< String, Map< String, Object >> getRecipientsAndContext()
{
	Map< String, Object > vehicle = new HashMap< String, Object >();
	vehicle.put( "year", year );
	vehicle.put( "make", make );
	vehicle.put( "model", model );
	vehicle.put( "mileage", mileage );
	
	Map< String, Object > smsContext = new HashMap< String, Object >();
	smsContext.put( "vehicle", vehicle );
	smsContext.put( "lithiaCarCenterLink", getLithiaCarCenterLink() );
	smsContext.put( "date", getDateString() );
	smsContext.put( "dealerName", dealerName );
	smsContext.put( "appraiserName", StringUtils.isBlank( appraiserName ) ? "Unknown" : appraiserName );
	smsContext.put( "appraisalValue", appraisalValue );
	
	Map< String, Map< String, Object > > recipientsAndContexts = new HashMap< String, Map< String, Object > >();
	recipientsAndContexts.put( toEmailAddress, smsContext );
	
	return recipientsAndContexts;
}

@Override
public String getSubject() {
	return "Completed Appraisal Alert for " + year + " " + make + " " + model + " - " + getDateString();
}

@Override
public String getTemplateName()
{
	return "lithiaCarCenterAppraisalValueAssigned";
}

}
