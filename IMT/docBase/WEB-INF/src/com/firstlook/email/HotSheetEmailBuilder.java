package com.firstlook.email;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.io.Resource;

import biz.firstlook.commons.email.EmailContextBuilder;
import biz.firstlook.commons.email.EmailService.EmailFormat;

/**
 * The HotSheetHelper class actually does most of the work. Refactor it to merge with this class?
 * 
 * @author bfung
 * 
 */
public class HotSheetEmailBuilder implements EmailContextBuilder
{

private Map< String, Map< String, Object > > recipients;
private Map< String, EmailFormat > emailFormats;
private Map< String, Resource > resources;
private Map< String, String > resourceContentTypes;

public HotSheetEmailBuilder( final Map< String, Map< String, Object > > recipients, final Map< String, EmailFormat > emailFormats )
{
	super();
	this.recipients = recipients;
	this.emailFormats = emailFormats;
	resources = new HashMap< String, Resource >();
	resourceContentTypes = new HashMap<String, String>();
}

public Map< String, Map< String, Object > > getRecipientsAndContext()
{
	return recipients;
}

public String getSubject()
{
	return "HotSheet Alert";
}

public String getTemplateName()
{
	return "hotsheet";
}

public Map< String, EmailFormat > getEmailFormats()
{
	return emailFormats;
}

public Map< String, Resource > getEmbeddedResources()
{
	return resources;
}

public void putResource( String name, Resource resource, String contentType )
{
	resources.put( name, resource );
	resourceContentTypes.put( name, contentType );
}

public Map< String, String > getEmbeddedResourcesContentTypes()
{
	return resourceContentTypes;
}

public String getReplyTo()
{
	// TODO Auto-generated method stub
	return null;
}

public List< String > getCcEmailAddresses()
{
	return Collections.EMPTY_LIST;
}

}
