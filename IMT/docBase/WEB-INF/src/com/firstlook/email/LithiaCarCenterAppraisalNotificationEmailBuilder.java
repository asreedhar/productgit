package com.firstlook.email;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This email builder is used to generate the e-mails that get sent to the Lithia Car Center after a user has selected the options
 * for a new appraisal.  It extands the LithiaCarCenterBaseEmailBuilder which has the fields common to all car center e-mails.
 * 
 * @author dweintrop
 */
public class LithiaCarCenterAppraisalNotificationEmailBuilder extends LithiaCarCenterBaseEmailBuilder
{

public LithiaCarCenterAppraisalNotificationEmailBuilder( String dealerName, int year, String make, String model, int mileage,
																int appraisalId, String toEmailAddress, List< String > ccEmailAddresses, 
																String replyToEmailAddress )
{
	super( dealerName, year, make, model, mileage, appraisalId, toEmailAddress, ccEmailAddresses, replyToEmailAddress );
}

public Map< String, Map< String, Object > > getRecipientsAndContext()
{
	Map< String, Object > vehicle = new HashMap< String, Object >();
	vehicle.put( "year", year );
	vehicle.put( "make", make );
	vehicle.put( "model", model );
	vehicle.put( "mileage", mileage );
	
	Map< String, Object > smsContext = new HashMap< String, Object >();
	smsContext.put( "vehicle", vehicle );
	smsContext.put( "lithiaCarCenterLink", getLithiaCarCenterLink() );
	smsContext.put( "date", getDateString() );
	smsContext.put( "dealerName", dealerName );
	
	Map< String, Map< String, Object > > recipientsAndContexts = new HashMap< String, Map< String, Object > >();
	recipientsAndContexts.put( toEmailAddress, smsContext );
	
	return recipientsAndContexts;
}

public String getSubject()
{
	return "Appraisal Alert from " + dealerName + " - " + getDateString();
}

public String getTemplateName()
{
	return "lithiaCarCenterAppraisalNotification";
}
}
