package com.firstlook.email;

public class PhoneNumberFormatException extends Exception
{

private static final long serialVersionUID = 1499192806821699618L;

public PhoneNumberFormatException()
{
}

public PhoneNumberFormatException( String message )
{
	super( message );
}

public PhoneNumberFormatException( Throwable cause )
{
	super( cause );
}

public PhoneNumberFormatException( String message, Throwable cause )
{
	super( message, cause );
}

}
