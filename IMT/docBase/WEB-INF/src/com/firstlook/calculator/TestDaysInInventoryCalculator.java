package com.firstlook.calculator;

import java.util.Calendar;

import junit.framework.TestCase;

import org.apache.commons.lang.time.DateUtils;

public class TestDaysInInventoryCalculator extends TestCase
{

private DaysInInventoryCalculator calculator;

public TestDaysInInventoryCalculator( String arg0 )
{
    super( arg0 );
}

public void setUp()
{
    calculator = new DaysInInventoryCalculator();
}

public void testGetDaysInInventoryAsLong()
{
    Calendar cal = Calendar.getInstance();

    cal.add( Calendar.DAY_OF_YEAR, -5 );

    cal = DateUtils.truncate( cal, Calendar.DATE );

    long days = calculator.calculate( cal.getTime() );

    assertEquals( 5, days );
}

public void testGetDaysInInventoryAsLongWhereDatesSame()
{
    Calendar cal = Calendar.getInstance();
    cal = DateUtils.truncate( cal, Calendar.DATE );

    long days = calculator.calculate( cal.getTime() );

    assertEquals( 1, days );
}

}
