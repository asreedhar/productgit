package com.firstlook.bookout.kbb.tradeIn;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.vehicle.description.MutableVehicleDescription;
import biz.firstlook.module.vehicle.description.exception.OptionsConflictException;
import biz.firstlook.module.vehicle.description.old.KBBConditionEnum;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;

public class KbbConsumerValueVehicleBookoutStateSubmitAction extends SecureBaseAction {

	private KbbConsumerValueService kbbConsumerValueService;
	
	@Override
	public ActionForward doIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
			throws ApplicationException {
		// these are needed becuase the consumer tool is available from the TA results pages
		SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
		SessionHelper.keepAttribute( request, "bookoutDetailForm" );
		
		KbbStandAloneOptionsForm kbbForm = (KbbStandAloneOptionsForm)form;
		kbbForm.clearErrors();
		
		if ( kbbForm.trimDropDownChanged()  ) {
			kbbForm.setFullSelectedOptionsSet( kbbForm.getVehicleDescription( kbbForm.getVehicleId() ) );
			return mapping.findForward("optionsSelect");
		}
		
		try {
			MutableVehicleDescription description = kbbForm.getVehicleDescription( kbbForm.getVehicleId());

			description.setSelectedVehicleOptionsByKeys( Arrays.asList( kbbForm.getAllSelectedOptions() ) );
			
			kbbConsumerValueService.saveVehicleBookoutState( description, kbbForm.getVin(), 
			                                                 getFirstlookSessionFromRequest(request).getCurrentDealerId(), 
			                                                 KBBConditionEnum.getKBBConditionEnumById( kbbForm.getSelectedCondition()) );
		} catch( OptionsConflictException oce ) {
			kbbForm.setErrors( oce.getOptionConflicts() );
			return mapping.findForward("optionsSelect");
		}

		return mapping.findForward("success");
	}

	public void setKbbConsumerValueService( KbbConsumerValueService kbbConsumerValueService ) {
		this.kbbConsumerValueService = kbbConsumerValueService;
	}

}
