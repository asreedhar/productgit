package com.firstlook.bookout.kbb.tradeIn;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.InvalidVinException;
import biz.firstlook.commons.util.VinUtility;
import biz.firstlook.module.vehicle.description.VehicleOption;
import biz.firstlook.module.vehicle.description.provider.kbb.KbbVehicleBookoutState;
import biz.firstlook.module.vehicle.description.provider.kbb.KbbVehicleDescriptionImpl;
import biz.firstlook.transact.persist.model.Dealer;
import biz.firstlook.transact.persist.model.DealerPreferenceKBBConsumerTool;
import biz.firstlook.transact.persist.persistence.DealerPreferenceKBBConsumerToolDAO;
import biz.firstlook.transact.persist.service.DealerService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;

public class KbbConsumerValueLandingAction extends SecureBaseAction {

	private DealerService dealerService;
	private DealerPreferenceKBBConsumerToolDAO dealerPreferenceKBBConsumerToolDAO;	
	private KbbConsumerValueService kbbConsumerValueService;
	
	@Override
	public ActionForward doIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
			throws ApplicationException {
		// these are needed becuase the consumer tool is available from the TA results pages
		SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
		SessionHelper.keepAttribute( request, "bookoutDetailForm" );

		KbbStandAloneOptionsForm kbbForm = (KbbStandAloneOptionsForm)form;
		kbbForm.clear();
		
		String vin = request.getParameter( "vin" );
		Integer mileage = Integer.valueOf( request.getParameter( "mileage" ) );
		Dealer dealer = dealerService.findByDealerId( getFirstlookSessionFromRequest( request ).getCurrentDealerId() );
		
		DealerPreferenceKBBConsumerTool kbbPrefs = dealerPreferenceKBBConsumerToolDAO.findByBusinessUnitId(dealer.getDealerId());
		kbbForm.setDealerKbbPreferences( kbbPrefs );
		
		
		kbbForm.setHasKbbAsBook( dealer );

		String forward = "";
		
		try {
			KbbVehicleBookoutState state = kbbConsumerValueService.retrieveVehicleBookoutState( dealer, vin );
			populateForm( kbbForm, state, mileage );
			forward = ( state.getVehicleDescription() != null ) ? "contextExists" : "buildContext";
		} catch ( KbbDealerWithoutBookoutException e ) {
			return mapping.findForward("kbbDealerWithoutBookout");
		}
		
		
		return mapping.findForward( forward );
	}
	
	private void populateForm(KbbStandAloneOptionsForm form, KbbVehicleBookoutState state, Integer mileage) {
		form.setVin( state.getVin() );
		form.setMileage( mileage );
		
		KbbVehicleDescriptionImpl vDesc = state.getVehicleDescription();
		if ( vDesc == null ) {
			return;
		}
		
		form.setMake( vDesc.getMake() );
		form.setModel( vDesc.getModel() );
		form.setVehicleId( vDesc.getVehicleId());
		form.setSelectedCondition( state.getCondition().getId() );
		try { form.setYear( VinUtility.getLatestModelYear( state.getVin() ) ); } 
			catch (InvalidVinException e) { e.printStackTrace(); }
		
		form.setSelectedDrivetrain( getSelectedSet( vDesc.getDrivetrainOptions() )[0] );
		form.setSelectedEngine( getSelectedSet( vDesc.getEngineOptions() )[0] );
		form.setSelectedTransmission( getSelectedSet( vDesc.getTransmissionOptions() )[0] );
		form.setSelectedEquipmentOptions( getSelectedSet( vDesc.getVehicleOptions() ) );
		form.setSelectedVehicleDescription(vDesc);
	}

	private String[] getSelectedSet(List<VehicleOption> options) {
		List<String> selectedOptions = new ArrayList<String>();
		for ( VehicleOption option : options ) {
			if ( option.isSelected() ) {
				selectedOptions.add( option.getOptionKey() );
			}
		}
		return selectedOptions.toArray( new String[ selectedOptions.size() ] );
	}
	
	public void setDealerService(DealerService dealerService) {
		this.dealerService = dealerService;
	}

	public void setDealerPreferenceKBBConsumerToolDAO(DealerPreferenceKBBConsumerToolDAO dealerPreferenceKBBConsumerToolDAO) {
		this.dealerPreferenceKBBConsumerToolDAO = dealerPreferenceKBBConsumerToolDAO;
	}

	public void setKbbConsumerValueService(
			KbbConsumerValueService kbbConsumerValueService) {
		this.kbbConsumerValueService = kbbConsumerValueService;
	}

}
