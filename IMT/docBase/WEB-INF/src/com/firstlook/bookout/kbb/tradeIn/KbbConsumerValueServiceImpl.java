package com.firstlook.bookout.kbb.tradeIn;

import java.util.Collection;

import biz.firstlook.module.bookout.kbb.KbbBookValueContainer;
import biz.firstlook.module.vehicle.description.MutableVehicleDescription;
import biz.firstlook.module.vehicle.description.VehicleDescription;
import biz.firstlook.module.vehicle.description.VehicleDescriptionContext;
import biz.firstlook.module.vehicle.description.VehicleDescriptionDAO;
import biz.firstlook.module.vehicle.description.VehicleDescriptionProvider;
import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;
import biz.firstlook.module.vehicle.description.exception.OptionsConflictException;
import biz.firstlook.module.vehicle.description.exception.ValuesRetrievalException;
import biz.firstlook.module.vehicle.description.impl.VehicleDescriptionContextFactory;
import biz.firstlook.module.vehicle.description.old.KBBConditionEnum;
import biz.firstlook.module.vehicle.description.provider.kbb.KbbVehicleBookoutState;
import biz.firstlook.transact.persist.model.Dealer;

public class KbbConsumerValueServiceImpl implements KbbConsumerValueService {
	
	private VehicleDescriptionDAO vehicleDescriptionDAO;

	KbbConsumerValueServiceImpl( VehicleDescriptionDAO vehicleDescriptionDAO ) {
		this.vehicleDescriptionDAO = vehicleDescriptionDAO;
	}
	
	public Collection<MutableVehicleDescription> getVehicleDescriptions(String vin, Dealer dealer) {
		VehicleDescriptionContext context = VehicleDescriptionContextFactory.getInstance().getKbbConsumerValuesContext( dealer );
		VehicleDescriptionProvider provider = context.getDescriptionProvider( getVehicleDescriptionProviderEnum() );
		Collection<MutableVehicleDescription> vehicleDescriptions = provider.getVehicleDescription( vin );
		return vehicleDescriptions;
	}


	private VehicleDescriptionProviderEnum getVehicleDescriptionProviderEnum() {
		return VehicleDescriptionProviderEnum.KBB;
	}

	public void saveVehicleBookoutState(VehicleDescription description, String vin, Integer businessUnitId, KBBConditionEnum condition) 
						throws OptionsConflictException {

		description.validate();
		
		// save
		KbbVehicleBookoutState vbs = (KbbVehicleBookoutState)vehicleDescriptionDAO.getVehicleBookoutState( vin, businessUnitId, getVehicleDescriptionProviderEnum() );
		
		// if null - it's the first time user has hit consumer value tool for this vin - so make a new vehicleBookoutState
		if ( vbs == null ) {
			vbs = new KbbVehicleBookoutState( vin, businessUnitId, null );
		}
		
		vbs.setVehicleDescription( description );
		vbs.setCondition( condition );
		vehicleDescriptionDAO.saveOrUpdate(vbs);
	}

	public KbbVehicleBookoutState retrieveVehicleBookoutState(Dealer dealer, String vin ) 
				throws KbbDealerWithoutBookoutException {
		
		boolean hasKbb = ( VehicleDescriptionProviderEnum.KBB.getThirdPartyId().equals( dealer.getDealerPreference().getGuideBookId() ) ) ||
					 ( VehicleDescriptionProviderEnum.KBB.getThirdPartyId().equals( dealer.getDealerPreference().getGuideBook2Id() ) ) 
					 	? true : false;
		
		KbbConsumerValueContext context = KbbConsumerValueContextFactory.getConsumerValueContext( hasKbb );
		
		KbbVehicleBookoutState state = context.find( vin, dealer );
		
		if ( hasKbb && ( state == null || state.getVehicleDescription() == null ) ) {
			throw new KbbDealerWithoutBookoutException();
		}
		
		return state;
	}

	public KbbBookValueContainer getBookValueContainer(VehicleDescription vehicleDescription, Integer mileage) 
				throws ValuesRetrievalException {
		return (KbbBookValueContainer) vehicleDescription.getBookValueContainer(mileage);
	}
	
}
