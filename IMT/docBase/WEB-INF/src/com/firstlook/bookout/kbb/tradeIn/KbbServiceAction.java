package com.firstlook.bookout.kbb.tradeIn;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.bookout.kbb.KbbBookValueContainer;
import biz.firstlook.module.vehicle.description.old.BookOutDatasetInfo;
import biz.firstlook.module.vehicle.description.old.BookOutTypeEnum;
import biz.firstlook.module.vehicle.description.old.GuideBookMetaInfo;
import biz.firstlook.module.vehicle.description.old.KBBBookoutService;
import biz.firstlook.module.vehicle.description.provider.kbb.KbbVehicleBookoutState;
import biz.firstlook.module.vehicle.description.provider.kbb.KbbVehicleDescriptionImpl;
import biz.firstlook.transact.persist.model.Dealer;
import biz.firstlook.transact.persist.service.DealerService;

import com.firstlook.action.BaseAction;
import com.firstlook.exception.ApplicationException;

public class KbbServiceAction extends BaseAction {

	private DealerService dealerService;
	private KbbConsumerValueService kbbConsumerValueService;
	private KBBBookoutService kbbBookoutService;
	
	public ActionForward justDoIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
	throws ApplicationException
	{
		String forward = "";
		try 
		{
			String vin = request.getParameter("vin");
			Integer mileage = Integer.parseInt(request.getParameter("mileage"));
			Dealer dealer = dealerService.findByDealerId(Integer.parseInt(request.getParameter("dealerId")));
			
			KbbVehicleBookoutState state = kbbConsumerValueService.retrieveVehicleBookoutState( dealer, vin );
			KbbVehicleDescriptionImpl vehicleDescription = state.getVehicleDescription();
			KbbBookValueContainer bookValueContainer = kbbConsumerValueService.getBookValueContainer(vehicleDescription, mileage);

			String stateCode = bookValueContainer.getRegionalIdentifier().getStateCode();
			GuideBookMetaInfo meta = kbbBookoutService.getMetaInfo(stateCode, new BookOutDatasetInfo(BookOutTypeEnum.APPRAISAL, dealer.getDealerId()));
			String pubInfo = meta.getPublishInfo();
			
			KbbPublishInfoParser publishInfo = new KbbPublishInfoParser(stateCode, pubInfo);
			
			KbbServiceForm kbbServiceForm = (KbbServiceForm)form;
			kbbServiceForm.setVin(vin);
			kbbServiceForm.setMileage(mileage);
			kbbServiceForm.setKbbBookValueContainer(bookValueContainer);
			kbbServiceForm.setCondition(state.getCondition());
			kbbServiceForm.setValidFrom(publishInfo.getValidFrom());
			kbbServiceForm.setValidUntil(publishInfo.getValidUntil());
			
			forward = "success";
		}
		catch (Exception e)
		{
			forward = "failure";
		}
		
		return mapping.findForward( forward );
	}
	
	public void setDealerService(DealerService dealerService)
	{
		this.dealerService = dealerService;
	}

	public void setKbbConsumerValueService(KbbConsumerValueService kbbConsumerValueService)
	{
		this.kbbConsumerValueService = kbbConsumerValueService;
	}

	public void setKbbBookoutService(KBBBookoutService kbbBookoutService)
	{
		this.kbbBookoutService = kbbBookoutService;
	}
}
