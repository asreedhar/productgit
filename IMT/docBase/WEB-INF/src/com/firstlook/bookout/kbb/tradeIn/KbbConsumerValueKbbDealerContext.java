package com.firstlook.bookout.kbb.tradeIn;

import biz.firstlook.module.vehicle.description.MutableVehicleDescription;
import biz.firstlook.module.vehicle.description.VehicleDescriptionContext;
import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;
import biz.firstlook.module.vehicle.description.impl.VehicleDescriptionContextFactory;
import biz.firstlook.module.vehicle.description.old.KBBConditionEnum;
import biz.firstlook.module.vehicle.description.provider.kbb.KbbTPV_VehicleAdapter;
import biz.firstlook.module.vehicle.description.provider.kbb.KbbVehicleBookoutState;
import biz.firstlook.module.vehicle.description.provider.kbb.KbbVehicleDescriptionImpl;
import biz.firstlook.transact.persist.exception.SelectedThirdPartyVehicleNotFoundException;
import biz.firstlook.transact.persist.model.BookOut;
import biz.firstlook.transact.persist.model.Dealer;
import biz.firstlook.transact.persist.model.InventoryWithBookOut;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.model.ThirdPartyVehicle;
import biz.firstlook.transact.persist.persistence.IInventoryBookOutDAO;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

public class KbbConsumerValueKbbDealerContext implements KbbConsumerValueContext {

	private IInventoryBookOutDAO inventoryBookOutDAO;
	private IAppraisalService appraisalService;
	
	public KbbConsumerValueKbbDealerContext( IInventoryBookOutDAO inventoryBookOutDAO, IAppraisalService appraisalService ) {
		this.inventoryBookOutDAO = inventoryBookOutDAO;
		this.appraisalService = appraisalService;
	}
	
	public KbbVehicleBookoutState find(String vin, Dealer dealer ) {
		BookOut bookout = null;
		Integer year = null;
		
		InventoryWithBookOut ib = inventoryBookOutDAO.findByVinAndBusinessUnitIDAndInInactiveInventory(vin, dealer.getDealerId(), dealer.getDealerPreference().getSearchInactiveInventoryDaysBackThreshold());
		if ( ib != null ) { // inventory bookout existed - use it	
			bookout = ib.getLatestBookOut( VehicleDescriptionProviderEnum.KBB.getThirdPartyId() );
			year = ib.getVehicle().getVehicleYear();
		} else {
			
			IAppraisal appraisal = appraisalService.findLatest( dealer.getDealerId(), vin, dealer.getDealerPreference().getTradeManagerDaysFilter() );
			if ( appraisal != null ) { // appraisal bookout exists - use it
				bookout = appraisal.getLatestBookOut( VehicleDescriptionProviderEnum.KBB.getThirdPartyId() );
				year = appraisal.getVehicle().getVehicleYear();
			}
		}
		
		KbbVehicleBookoutState state = null;
		if ( bookout == null ) {
			state = new KbbVehicleBookoutState(vin, dealer.getDealerId(), null );
		} 
		else {
			ThirdPartyVehicle tpv;
			try {
				tpv = bookout.getSelectedThirdPartyVehicle();
			} catch (SelectedThirdPartyVehicleNotFoundException e) {
				return state; // this is return null
			}
			
			MutableVehicleDescription vDescAndOptions = new KbbTPV_VehicleAdapter().adaptItem( tpv );
			vDescAndOptions.setYear( year );
			VehicleDescriptionContext cxt = VehicleDescriptionContextFactory.getInstance().getKbbConsumerValuesContext( dealer );
			vDescAndOptions.setVehicleDescriptionProvider( cxt.getDescriptionProvider( VehicleDescriptionProviderEnum.KBB ) );
			state = new KbbVehicleBookoutState(vin, dealer.getDealerId(), 
												(KbbVehicleDescriptionImpl)vDescAndOptions );
			KBBConditionEnum condition = KBBConditionEnum.getKBBConditionEnum( bookout.getThirdPartySubCategory( ThirdPartyCategory.KELLEY_TRADEIN_TYPE ) );
			state.setCondition( condition );
		}	
		return state;
	}
}