/**
 * 
 */
package com.firstlook.bookout.kbb.tradeIn;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Helper class to parse the publishInfo of Kbb.
 * @author bfung
 *
 */
class KbbPublishInfoParser {
	
	private String stateCode;
	private Date validFrom;
	private Date validUntil;
	
	public KbbPublishInfoParser(String stateCode, String publishInfo) throws ParseException {
		this.stateCode = stateCode;
		parsePublishInfo(publishInfo);
	}
	
	public String getStateCode() {
		return stateCode;
	}
	
	public Date getValidFrom() {
		return validFrom;
	}
	
	public Date getValidUntil() {
		return validUntil;
	}
	
	private void parsePublishInfo(String publishInfo) throws ParseException {
		boolean success = false;
		success = parseStringMonthNameStyle(publishInfo);
		if(success) {
			return;
		}
		
		success = parseNumericDateStyle(publishInfo);
		if(!success) {
			throw new ParseException(publishInfo, 0);
		}
	}

	private boolean parseStringMonthNameStyle(String publishInfo) throws ParseException {
		//assumed pattern is "SS. BBB-EEE YYYY" 
		//where S is stateCode
		//BBB is begin month, EEE is end month and YYYY is year
		String monthsRegex = "(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)";
		StringBuilder regexPattern = new StringBuilder();
		regexPattern.append(monthsRegex);
		regexPattern.append("\\-");
		regexPattern.append(monthsRegex);
		regexPattern.append("\\s");
		regexPattern.append("([1-9][0-9][0-9][0-9])");
		
		Pattern p = Pattern.compile(regexPattern.toString());
		Matcher m = p.matcher(publishInfo);
		
		if(m.find()) {
			String startMonthString = m.group(1);
			String endMonthString = m.group(2);
			int year = -1;
			
			try {
				year = Integer.parseInt(m.group(3));
			} catch (NumberFormatException nfe) {
				throw new ParseException(publishInfo, m.start(3));
			}

			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.YEAR, year);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			
			if(startMonthString != null) {
				cal.set(Calendar.MONTH, parseMonth(startMonthString));
				cal.set(Calendar.DAY_OF_MONTH, 1);
				validFrom = cal.getTime();
			}
			
			if(endMonthString != null) {
				cal.set(Calendar.MONTH, parseMonth(endMonthString));
				cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
				validUntil = cal.getTime();
			}
			
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * @param month three character abbreviation denoting the month.
	 * @return Returns an int corresponding to the month constant in the java.util.Calendar class.
	 * @throws ParseException if the month abbreviation could not be found.
	 */
	private static int parseMonth(String month) throws ParseException {
		int monthInt = -1;
		if (month.equalsIgnoreCase("Jan")) {
			monthInt = Calendar.JANUARY;
		} else if (month.equalsIgnoreCase("Feb")) {
			monthInt = Calendar.FEBRUARY;
		} else if (month.equalsIgnoreCase("Mar")) {
			monthInt = Calendar.MARCH;
		} else if (month.equalsIgnoreCase("Apr")) {
			monthInt = Calendar.APRIL;
		} else if (month.equalsIgnoreCase("May")) {
			monthInt = Calendar.MAY;
		} else if (month.equalsIgnoreCase("Jun")) {
			monthInt = Calendar.JUNE;
		} else if (month.equalsIgnoreCase("Jul")) {
			monthInt = Calendar.JULY;
		} else if (month.equalsIgnoreCase("Aug")) {
			monthInt = Calendar.AUGUST;
		} else if (month.equalsIgnoreCase("Sep")) {
			monthInt = Calendar.SEPTEMBER;
		} else if (month.equalsIgnoreCase("Oct")) {
			monthInt = Calendar.OCTOBER;
		} else if (month.equalsIgnoreCase("Nov")) {
			monthInt = Calendar.NOVEMBER;
		} else if (month.equalsIgnoreCase("Dec")) {
			monthInt = Calendar.DECEMBER;
		} else {
			throw new ParseException(MessageFormat.format("Unknown month name '{0}'", month), 0);
		}
		
		return monthInt;
	}
	
	private boolean parseNumericDateStyle(String publishInfo) throws ParseException {
		//assumed pattern is "SS. MM/DD/YYYY-MM/DD/YYYY" 
		//where S is stateCode
		String monthsRegex = "([0-1][0-9]\\/[0-3]?[0-9]\\/[1-9][0-9][0-9][0-9])";
		StringBuilder regexPattern = new StringBuilder();
		regexPattern.append(monthsRegex);
		regexPattern.append("\\s");
		regexPattern.append("\\-");
		regexPattern.append("\\s");
		regexPattern.append(monthsRegex);
		
		Pattern p = Pattern.compile(regexPattern.toString());
		Matcher m = p.matcher(publishInfo);
		
		if(m.find()) {
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			validFrom = dateFormat.parse(m.group(1));
			validUntil = dateFormat.parse(m.group(2));
			return true;
		} else {
			return false;
		}
	}
}