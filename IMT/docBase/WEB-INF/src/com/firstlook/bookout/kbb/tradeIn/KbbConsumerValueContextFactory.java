package com.firstlook.bookout.kbb.tradeIn;


public class KbbConsumerValueContextFactory {

	private static KbbConsumerValueKbbDealerContext kbbDealerContext;
	private static KbbConsumerValueNonKbbDealerContext nonKbbDealerContext;
	
	private KbbConsumerValueContextFactory( KbbConsumerValueKbbDealerContext kbbDealerContext, 
											KbbConsumerValueNonKbbDealerContext nonKbbDealerContext ) {
		KbbConsumerValueContextFactory.kbbDealerContext = kbbDealerContext;
		KbbConsumerValueContextFactory.nonKbbDealerContext = nonKbbDealerContext;
	}
	
	public static KbbConsumerValueContext getKbbConsumerValueNonKbbDealerContext() {
		return nonKbbDealerContext;
	}
	
	public static KbbConsumerValueContext getConsumerValueContext( boolean hasKbb ) {
		if ( hasKbb ) {
			return kbbDealerContext;
		} else {
			return nonKbbDealerContext;
		}
	}
}
