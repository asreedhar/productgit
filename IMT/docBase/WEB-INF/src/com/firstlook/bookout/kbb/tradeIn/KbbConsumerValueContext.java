package com.firstlook.bookout.kbb.tradeIn;

import biz.firstlook.module.vehicle.description.provider.kbb.KbbVehicleBookoutState;
import biz.firstlook.transact.persist.model.Dealer;

public interface KbbConsumerValueContext {
	public KbbVehicleBookoutState find( String vin, Dealer dealer );
}
