package com.firstlook.bookout.kbb.tradeIn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;

import biz.firstlook.module.bookout.kbb.KbbBookValue;
import biz.firstlook.module.vehicle.description.VehicleOption;
import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;
import biz.firstlook.module.vehicle.description.exception.BaseError;
import biz.firstlook.module.vehicle.description.old.KBBConditionEnum;
import biz.firstlook.module.vehicle.description.provider.kbb.KbbRegionalIdentifier;
import biz.firstlook.module.vehicle.description.provider.kbb.KbbVehicleDescriptionImpl;
import biz.firstlook.module.vehicle.description.provider.kbb.KbbVehicleOption;
import biz.firstlook.transact.persist.model.Dealer;
import biz.firstlook.transact.persist.model.DealerPreferenceKBBConsumerTool;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;

public class KbbStandAloneOptionsForm extends ActionForm
{
private static final long serialVersionUID = 48658123549135845L;
	
private String vin;
private Integer year;
private String make;
private String model;

private Integer mileage;

private String vehicleId;	//This the display
private String selectedEngine;
private String selectedTransmission;
private String selectedDrivetrain;
private Integer selectedCondition;

private String initialVehicleId;

private String[] selectedEquipmentOptions;
private Map<String, KbbVehicleDescriptionImpl> vehicleDescriptions = new HashMap<String, KbbVehicleDescriptionImpl>();
private List<? extends BaseError> errors = new ArrayList<BaseError>();

private List<KbbBookValue> tradeInValues = new ArrayList<KbbBookValue>();
private List<KbbBookValue> tradeInLowValues = new ArrayList<KbbBookValue>();
private List<KbbBookValue> tradeInHighValues = new ArrayList<KbbBookValue>();
private KbbBookValue retailValue = null;
private Boolean showTradeIn;
private Boolean showRetail;

private String publishInfo;
private KbbRegionalIdentifier regionalIdentifier;

private boolean hasKbbAsBook;

public void clear() {
	this.vin = null;
	this.year = null;
	this.make = null;
	this.model = null;
	this.mileage = null;
	this.vehicleId = null;
	this.selectedCondition = null;
	this.selectedDrivetrain = null;
	this.selectedEngine = null;
	this.selectedEquipmentOptions = null;
	this.tradeInValues.clear();
	this.tradeInHighValues.clear();
	this.tradeInLowValues.clear();
	this.retailValue = null;
	this.errors.clear();
	this.selectedTransmission = null;
	this.initialVehicleId = null;
	this.vehicleDescriptions.clear();
}

public String getVin()
{
	return vin;
}

public void setVin( String vin )
{
	this.vin = vin;
}

public Integer getYear()
{
	return year;
}

public void setYear( Integer year )
{
	this.year = year;
}

public String getMake()
{
	return make;
}

public void setMake( String make )
{
	this.make = make;
}

public String getModel()
{
	return model;
}

public void setModel( String model )
{
	this.model = model;
}

public Integer getMileage() {
	return mileage;
}

public void setMileage( Integer mileage ) {
	this.mileage = mileage;
}

public String getVehicleId()
{
	return vehicleId;
}

public Integer getNumTrims() {
	return vehicleDescriptions.size();
}

public String getTrim() {
	if( vehicleId != null ) {
		KbbVehicleDescriptionImpl v = vehicleDescriptions.get( vehicleId );
		return v.getTrim();
	}
	return null;
}

public void setVehicleId( String vehicleId)
{
	this.vehicleId = vehicleId;
}

public KbbVehicleDescriptionImpl getVehicleDescription( String vehicleId) {
	return vehicleDescriptions.get( vehicleId );
}

public List<KbbVehicleDescriptionImpl> getVehicleDescriptions() {
	return new ArrayList< KbbVehicleDescriptionImpl >( vehicleDescriptions.values() );
}

public KbbVehicleDescriptionImpl getSelectedVehicleDescription() {
	if( vehicleId != null ) {
		return vehicleDescriptions.get( vehicleId );
	}
	return null;
}

public void setSelectedVehicleDescription( KbbVehicleDescriptionImpl vehicleDescription ) {
	setVehicleId( vehicleDescription.getVehicleId());
	addVehicleDescription( vehicleDescription );
}

public void addVehicleDescription( KbbVehicleDescriptionImpl vehicleDescription ) {
	if( selectedCondition != null ) {
		vehicleDescription.setKbbCondition( KBBConditionEnum.getKBBConditionEnumById( selectedCondition ) );
	}
	this.vehicleDescriptions.put(vehicleDescription.getVehicleId(), vehicleDescription );
}

public String[] getSelectedEquipmentOptions() {
	return selectedEquipmentOptions;
}

public void setSelectedEquipmentOptions(String[] selectedOptions) {
	this.selectedEquipmentOptions = selectedOptions;
}


public void setRetailValue(KbbBookValue value) {
	this.retailValue = value;
}


public void setShowTradeIn(Boolean showTradeIn) {
	this.showTradeIn = showTradeIn;
}
public void setShowRetail(Boolean showRetail) {
	this.showRetail = showRetail;
}



public String[] getAllSelectedOptions() {
	List<String> allSelectedOptions = new ArrayList<String>();  //explicit - we don't want to change the selected EquipmentOptions array
	allSelectedOptions.addAll( Arrays.asList( this.selectedEquipmentOptions ) );
	allSelectedOptions.add( this.selectedDrivetrain );
	allSelectedOptions.add( this.selectedTransmission );
	allSelectedOptions.add( this.selectedEngine );
	return allSelectedOptions.toArray( new String[ allSelectedOptions.size() ] );
}

public String getSelectedDrivetrain() {
	if ( StringUtils.isNotBlank( this.selectedDrivetrain ) ) {
		return this.selectedDrivetrain;
	}
	
	if ( vehicleDescriptions.get( vehicleId ) != null ) {
		for ( VehicleOption option: this.vehicleDescriptions.get( vehicleId ).getDrivetrainOptions() ) {
			if ( option.isSelected() ) {
				return option.getOptionKey();
			}
		}
	}
	return "";
}

public String getSelectedDrivetrainDisplayName() {
	if ( vehicleDescriptions.get( vehicleId ) != null ) {
		for ( VehicleOption option: this.vehicleDescriptions.get( vehicleId ).getDrivetrainOptions() ) {
			if ( option.isSelected() ) {
				return option.getDisplayName();
			}
		}
	}
	return "";
}

public void setSelectedDrivetrain(String selectedDrivetrain) {
	this.selectedDrivetrain = selectedDrivetrain;
}

public String getSelectedEngine() {
	if ( StringUtils.isNotBlank( this.selectedEngine ) ) {
		return this.selectedEngine;
	}
	
	if ( vehicleDescriptions.get( vehicleId ) != null ) {
		for ( VehicleOption option: this.vehicleDescriptions.get( vehicleId ).getEngineOptions() ) {
			if ( option.isSelected() ) {
				return option.getOptionKey();
			}
		}
	}
	return "";
}

public String getSelectedEngineDisplayName() {
	if ( vehicleDescriptions.get( vehicleId ) != null ) {
		for ( VehicleOption option: this.vehicleDescriptions.get( vehicleId ).getEngineOptions() ) {
			if ( option.isSelected() ) {
				return option.getDisplayName();
			}
		}
	}
	return "";
}

public void setSelectedEngine(String selectedEngine) {
	this.selectedEngine = selectedEngine;
}

public String getSelectedTransmission() {
	if ( StringUtils.isNotBlank( this.selectedTransmission ) ) {
		return this.selectedTransmission;
	}
	
	if ( vehicleDescriptions.get( vehicleId ) != null ) {
		for ( VehicleOption option: this.vehicleDescriptions.get( vehicleId ).getTransmissionOptions() ) {
			if ( option.isSelected() ) {
				return option.getOptionKey();
			}
		}
	}
	return "";
}

public String getSelectedTransmissionDisplayName() {
	if ( vehicleDescriptions.get( vehicleId ) != null ) {
		for ( VehicleOption option: this.vehicleDescriptions.get( vehicleId ).getTransmissionOptions() ) {
			if ( option.isSelected() ) {
				return option.getDisplayName();
			}
		}
	}
	return "";
}

public void setSelectedTransmission(String selectedTransmission) {
	this.selectedTransmission = selectedTransmission;
}

public Integer getSelectedCondition() {
	return selectedCondition;
}

public void setSelectedCondition(Integer selectedCondition) {
	KBBConditionEnum conditionEnum = KBBConditionEnum.getKBBConditionEnumById( selectedCondition );
	for( KbbVehicleDescriptionImpl v : vehicleDescriptions.values() ) {
		v.setKbbCondition( conditionEnum );
	}
	this.selectedCondition = selectedCondition;
}

public List<KBBConditionEnum> getConditions() {
	return KBBConditionEnum.getConditionsForDisplay();
}

public List<? extends BaseError> getErrors() {
	return errors;
}

public void setErrors(List<? extends BaseError> errors) {
	this.errors = errors;
}

public List<KbbBookValue> getTradeInValues() {
	return tradeInValues;
}


public List<KbbBookValue> getTradeInLowValues() {
	return tradeInLowValues;
}

public List<KbbBookValue> getTradeInHighValues() {
	return tradeInHighValues;
}

public KbbBookValue getRetailValue() {
	return retailValue;
}


public Boolean getShowTradeIn()
{
	return showTradeIn;
}
public Boolean getShowRetail()
{
	return showRetail;
}


public String getInitialVehicleId() {
	return initialVehicleId;
}

public void setInitialVehicleId(String vehicleId) {
	this.initialVehicleId = vehicleId;
}

public List<String> getSelectedOptionalEquipmentOptions() {
	return filterEquipmentOptions(false);
}

public List<String> getSelectedStandardEquipmentOptions() {
	return filterEquipmentOptions(true);
}

public String getState() {
	return regionalIdentifier.getDisplayName();
}

public void setRegionalIdentifier( KbbRegionalIdentifier regionalIdentifier ) {
	this.regionalIdentifier = regionalIdentifier;
}

private List<String> filterEquipmentOptions(boolean wantStandardOptions){
	List<String> options = new ArrayList<String>();
	List<VehicleOption> selectedVehicleOptions = this.vehicleDescriptions.get(this.vehicleId).getSelectedEquipmentVehicleOptions();
	for(VehicleOption option : selectedVehicleOptions ) {
		KbbVehicleOption kbbOption = (KbbVehicleOption) option;
		if( kbbOption.isSelected() && kbbOption.isStandardOption() == wantStandardOptions ){
			options.add(kbbOption.getDisplayName());
		}
	}
	return options;
}

public String getPublishInfo() {
	return publishInfo;
}

public void setPublishInfo(String publishInfo) {
	this.publishInfo = publishInfo;
}

public boolean trimDropDownChanged(){
	return	getVehicleId() != null && vehicleDescriptions != null &&
			!getVehicleId().equals( getInitialVehicleId());
}

public void clearErrors() {
	this.errors.clear();
}

public boolean isHasKbbAsBook()
{
	return hasKbbAsBook;
}

public void setHasKbbAsBook( Dealer dealer )
{
	this.hasKbbAsBook = ( VehicleDescriptionProviderEnum.KBB.getThirdPartyId().equals( dealer.getDealerPreference().getGuideBookId() ) ) ||
	 					( VehicleDescriptionProviderEnum.KBB.getThirdPartyId().equals( dealer.getDealerPreference().getGuideBook2Id() ) ) 
	 	? true : false;
}

public void setFullSelectedOptionsSet( KbbVehicleDescriptionImpl kbbVehicleDesc ) {
	this.selectedEquipmentOptions = getSelectedOptionsSetByType( kbbVehicleDesc, ThirdPartyOptionType.EQUIPMENT );
	this.selectedDrivetrain = getSelectedOptionsSetByType(kbbVehicleDesc, ThirdPartyOptionType.DRIVETRAIN)[0];
	this.selectedEngine = getSelectedOptionsSetByType(kbbVehicleDesc, ThirdPartyOptionType.ENGINE)[0];
	this.selectedTransmission = getSelectedOptionsSetByType(kbbVehicleDesc, ThirdPartyOptionType.TRANSMISSION)[0];
}

//hmm, why is the ThirdPartyOptionType param not usable or VehicleOption implemented not well?
private String[] getSelectedOptionsSetByType(KbbVehicleDescriptionImpl kbbVehicleDesc, ThirdPartyOptionType optionType) {
	List<String> selectedOptions = new ArrayList<String>();

	//fix for the ThirdPartyOptionType param not being used?
	boolean castable = false;
	List<VehicleOption> options = kbbVehicleDesc.getSelectedVehicleOptions();
	if(options != null && !options.isEmpty()) {
		VehicleOption vo = options.get(0);
		if(vo instanceof KbbVehicleOption) {
			castable = true;
		}
	}
	
	if(castable) {
		for( VehicleOption option : options ) {
			if(option instanceof KbbVehicleOption) {
				KbbVehicleOption kbbVo = (KbbVehicleOption)option;
				ThirdPartyOptionType tpot = ThirdPartyOptionType.fromId(kbbVo.getThirdPartyOptionTypeId());
				if(tpot.equals(optionType)) {
					selectedOptions.add( kbbVo.getOptionKey() );
				}
			}
		}
	} else {
		for( VehicleOption option : options ) {
			selectedOptions.add( option.getOptionKey() );
		}
	}
	
	return (String[])selectedOptions.toArray(new String[]{});
}


public void setDealerKbbPreferences(DealerPreferenceKBBConsumerTool prefs)
{
	this.showRetail = prefs.getShowRetail();
	this.showTradeIn = prefs.getShowTradeIn();
}

}
