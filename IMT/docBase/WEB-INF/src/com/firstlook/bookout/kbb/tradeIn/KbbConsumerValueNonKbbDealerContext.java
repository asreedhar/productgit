package com.firstlook.bookout.kbb.tradeIn;

import biz.firstlook.module.vehicle.description.VehicleDescriptionContext;
import biz.firstlook.module.vehicle.description.VehicleDescriptionDAO;
import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;
import biz.firstlook.module.vehicle.description.impl.VehicleDescriptionContextFactory;
import biz.firstlook.module.vehicle.description.provider.kbb.KbbVehicleBookoutState;
import biz.firstlook.module.vehicle.description.provider.kbb.KbbVehicleDescriptionImpl;
import biz.firstlook.transact.persist.model.Dealer;

public class KbbConsumerValueNonKbbDealerContext implements KbbConsumerValueContext {

	private VehicleDescriptionDAO vehicleDescriptionDAO;
	
	public KbbConsumerValueNonKbbDealerContext( VehicleDescriptionDAO vehicleDescriptionDAO ) {
		this.vehicleDescriptionDAO = vehicleDescriptionDAO;
	}
	
	public KbbVehicleBookoutState find(String vin, Dealer dealer) {
		VehicleDescriptionContext cxt = VehicleDescriptionContextFactory.getInstance().getKbbConsumerValuesContext( dealer );
		
		KbbVehicleBookoutState state = (KbbVehicleBookoutState)vehicleDescriptionDAO.getVehicleBookoutState(vin, dealer.getDealerId(), VehicleDescriptionProviderEnum.KBB);
		if ( state == null ) {
			state = new KbbVehicleBookoutState(vin, dealer.getDealerId(), null );
			return state;
		}
		
		KbbVehicleDescriptionImpl desc = (KbbVehicleDescriptionImpl)state.getVehicleDescription();
		desc.setVehicleDescriptionProvider(cxt.getDescriptionProvider( VehicleDescriptionProviderEnum.KBB ));
		state.setBusinessUnitId( dealer.getDealerId() );
		state.setVin( vin );
		return state;
	}

}
