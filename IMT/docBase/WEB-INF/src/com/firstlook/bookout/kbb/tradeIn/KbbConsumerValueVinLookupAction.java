package com.firstlook.bookout.kbb.tradeIn;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.vehicle.description.MutableVehicleDescription;
import biz.firstlook.module.vehicle.description.provider.kbb.KbbVehicleDescriptionImpl;
import biz.firstlook.transact.persist.model.Dealer;
import biz.firstlook.transact.persist.service.DealerService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;

public class KbbConsumerValueVinLookupAction extends SecureBaseAction
{

private DealerService dealerService;
private KbbConsumerValueService kbbConsumerValueService;

@Override
public final ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	// these are needed becuase the consumer tool is available from the TA results pages
	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );
	
	KbbStandAloneOptionsForm kbbForm = (KbbStandAloneOptionsForm)form;
	

	Dealer dealer = dealerService.findByDealerId( getFirstlookSessionFromRequest( request ).getCurrentDealerId() );
	
	Collection<MutableVehicleDescription> vehicleDescriptions = kbbConsumerValueService.getVehicleDescriptions(kbbForm.getVin(), dealer); 
	
	populateForm( kbbForm, vehicleDescriptions );
	return mapping.findForward( "success" );
}

private void populateForm(KbbStandAloneOptionsForm kbbForm, Collection<MutableVehicleDescription> vehicleDescriptions) {
	
	for(MutableVehicleDescription vehicleDesc : vehicleDescriptions ) {
		KbbVehicleDescriptionImpl kbbVehicleDesc = (KbbVehicleDescriptionImpl)vehicleDesc;
		if ( kbbForm.getYear() == null ) {
			kbbForm.setYear( kbbVehicleDesc.getYear() );
			kbbForm.setMake( kbbVehicleDesc.getMake() );
			kbbForm.setModel( kbbVehicleDesc.getModel() );
		}
		
		kbbForm.addVehicleDescription( kbbVehicleDesc );
		
		// if we know what set of options to display, and the user has not already selected a set of options - display defaults
		if( vehicleDescriptions.size() == 1 && kbbForm.getSelectedEquipmentOptions() == null ) {
			kbbForm.setVehicleId( kbbVehicleDesc.getVehicleId());
			kbbForm.setFullSelectedOptionsSet( kbbVehicleDesc );
		}
	}
	
}

public void setDealerService( DealerService dealerService )
{
	this.dealerService = dealerService;
}

public void setKbbConsumerValueService( KbbConsumerValueService kbbConsumerValueService ) {
	this.kbbConsumerValueService = kbbConsumerValueService;
}

}
