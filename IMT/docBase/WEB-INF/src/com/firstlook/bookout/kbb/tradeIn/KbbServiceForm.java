package com.firstlook.bookout.kbb.tradeIn;


import java.util.Date;

import org.apache.struts.action.ActionForm;

import biz.firstlook.module.bookout.kbb.KbbBookValueContainer;
import biz.firstlook.module.vehicle.description.old.KBBConditionEnum;


public class KbbServiceForm extends ActionForm
{
	private static final long serialVersionUID = 48658123549135845L;
		
	private String vin;
	private Integer mileage;
	private KbbBookValueContainer kbbBookValueContainer;
	private KBBConditionEnum condition;
	private Date validFrom;
	private Date validUntil;
	
	public String getVin()
	{
		return vin;
	}
	public void setVin( String vin )
	{
		this.vin = vin;
	}
	public Integer getMileage()
	{
		return mileage;
	}
	public void setMileage( Integer mileage )
	{
		this.mileage = mileage;
	}
	public KbbBookValueContainer getKbbBookValueContainer()
	{
		return kbbBookValueContainer;
	}
	public void setKbbBookValueContainer(KbbBookValueContainer kbbBookValueContainer)
	{
		this.kbbBookValueContainer = kbbBookValueContainer;
	}

	public KBBConditionEnum getCondition()
	{
		return condition;
	}
	public void setCondition(KBBConditionEnum condition)
	{
		this.condition = condition;
	}
	public Date getValidFrom()
	{
		return validFrom;
	}
	public void setValidFrom(Date validFrom)
	{
		this.validFrom = validFrom;
	}

	public Date getValidUntil()
	{
		return validUntil;
	}
	public void setValidUntil(Date validUntil)
	{
		this.validUntil = validUntil;
	}
}
