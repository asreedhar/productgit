package com.firstlook.bookout.kbb.tradeIn;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.bookout.kbb.KbbBookValue;
import biz.firstlook.module.bookout.kbb.KbbBookValueContainer;
import biz.firstlook.module.vehicle.description.exception.ValuesRetrievalException;
import biz.firstlook.module.vehicle.description.old.KBBCategoryEnum;
import biz.firstlook.module.vehicle.description.old.KBBConditionEnum;
import biz.firstlook.transact.persist.model.Inventory;
import biz.firstlook.transact.persist.persistence.IInventoryDAO;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;

public class KbbConsumerValueToolDisplayAction extends SecureBaseAction {

private KbbConsumerValueService kbbConsumerValueService;
private IInventoryDAO inventoryDAO;
	
@Override
public final ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException {
	// these are needed becuase the consumer tool is available from the TA results pages
	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );

	KbbStandAloneOptionsForm kbbForm = (KbbStandAloneOptionsForm) form;

	try{
		KbbBookValueContainer bookValueContainer = kbbConsumerValueService.getBookValueContainer( kbbForm.getVehicleDescription( kbbForm.getVehicleId() ),kbbForm.getMileage() );
		buildForm(kbbForm, bookValueContainer );
	}
	catch(ValuesRetrievalException exception){
		System.out.println("values retrieval exception getting book values");
	}

	// for inventory items - need to display stock number on values page and print page 
	Inventory i = inventoryDAO.searchVinInActiveInventory( getFirstlookSessionFromRequest( request ).getCurrentDealerId(), kbbForm.getVin() );
	request.setAttribute( "stockNumber", (i != null ) ? i.getStockNumber() : "" );
	/*//case 31964 Save KBB Retail consumer value to FLDW.dbo.InventoryKbbConsumerValue_F*/
	if(kbbForm.getShowRetail()){
		if(i!=null && kbbForm!=null && kbbForm.getRetailValue()!=null)
			inventoryDAO.updateRetailKbbConsumerValue(getFirstlookSessionFromRequest(request).getCurrentDealerId(), i.getInventoryId(), kbbForm.getRetailValue().getValue());
	}
	/*//case 31964 end*/
	return mapping.findForward( "success" );
}

private void buildForm( KbbStandAloneOptionsForm form, KbbBookValueContainer kbbBookValueContainer ){
	Collection<KbbBookValue> bookValues = kbbBookValueContainer.getValues(KBBCategoryEnum.TRADEIN);
	
	if( bookValues != null ) {
		form.getTradeInValues().clear();
		form.getTradeInValues().addAll( bookValues );
	}

	bookValues = kbbBookValueContainer.getValues(KBBCategoryEnum.TRADEINRANGEHIGH);
	
	if( bookValues != null ) {
		
		form.getTradeInHighValues().addAll( bookValues );
	}
	
	bookValues = kbbBookValueContainer.getValues(KBBCategoryEnum.TRADEINRANGELOW);
	
	if( bookValues != null ) {
		
		form.getTradeInLowValues().addAll( bookValues );
	}
	
	form.setRetailValue(kbbBookValueContainer.getValue(KBBCategoryEnum.RETAIL, KBBConditionEnum.N_A));	

	form.setRegionalIdentifier( kbbBookValueContainer.getRegionalIdentifier() );
}

public void setKbbConsumerValueService( KbbConsumerValueService kbbConsumerValueService ) {
	this.kbbConsumerValueService = kbbConsumerValueService;
}

public void setInventoryDAO( IInventoryDAO inventoryDAO ) {
	this.inventoryDAO = inventoryDAO;
}

}
