package com.firstlook.bookout.kbb.tradeIn;

import java.util.Collection;

import biz.firstlook.module.bookout.kbb.KbbBookValueContainer;
import biz.firstlook.module.vehicle.description.MutableVehicleDescription;
import biz.firstlook.module.vehicle.description.VehicleDescription;
import biz.firstlook.module.vehicle.description.exception.OptionsConflictException;
import biz.firstlook.module.vehicle.description.exception.ValuesRetrievalException;
import biz.firstlook.module.vehicle.description.old.KBBConditionEnum;
import biz.firstlook.module.vehicle.description.provider.kbb.KbbVehicleBookoutState;
import biz.firstlook.transact.persist.model.Dealer;

public interface KbbConsumerValueService {

	public Collection<MutableVehicleDescription> getVehicleDescriptions( String vin, Dealer dealer );

	public void saveVehicleBookoutState( VehicleDescription description, String vin, Integer businessUnitId, 
													KBBConditionEnum condition) throws OptionsConflictException;

	public KbbVehicleBookoutState retrieveVehicleBookoutState(Dealer dealer, String vin) throws KbbDealerWithoutBookoutException;

	public KbbBookValueContainer getBookValueContainer(VehicleDescription description, Integer mileage) throws ValuesRetrievalException;
}