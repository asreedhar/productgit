package com.firstlook.bookout.kbb.tradeIn;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import junit.framework.TestCase;

public class TestKbbPublishInfoParser extends TestCase {

	public TestKbbPublishInfoParser() {
		super();
	}

	public TestKbbPublishInfoParser(String name) {
		super(name);
	}
	
	public void testIllinoisSeptOct2008OldStyle() {
		String stateCode = "IL";
		String info = "IL. Sep-Oct 2008";
		
		KbbPublishInfoParser parser = null;
		try {
			 parser = new KbbPublishInfoParser(stateCode, info);
		} catch (ParseException e) {
			fail();
		}
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 2008);
		cal.set(Calendar.MONTH, Calendar.SEPTEMBER);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
	
		assertEquals(cal.getTime(), parser.getValidFrom());
		
		cal.set(Calendar.MONTH, Calendar.OCTOBER);
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		assertEquals(cal.getTime(), parser.getValidUntil());
	}

	public void testNorthCarolinaNewStyle() {
		String stateCode = "IL";
		String info = "NC. 12/5/2008 - 12/11/2008";
		
		KbbPublishInfoParser parser = null;
		try {
			 parser = new KbbPublishInfoParser(stateCode, info);
		} catch (ParseException e) {
			fail(e.getMessage());
		}
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 2008);
		cal.set(Calendar.MONTH, Calendar.DECEMBER);
		cal.set(Calendar.DAY_OF_MONTH, 5);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		Date theTime = cal.getTime();
		
		assertEquals(theTime, parser.getValidFrom());
		
		cal.set(Calendar.DAY_OF_MONTH, 11);
		assertEquals(cal.getTime(), parser.getValidUntil());
	}
	
	public void testPensylvaniaNewStyle() {
		String stateCode = "PA";
		String info = "PA. 12/26/2008 - 01/01/2009";
		
		KbbPublishInfoParser parser = null;
		try {
			 parser = new KbbPublishInfoParser(stateCode, info);
		} catch (ParseException e) {
			fail(e.getMessage());
		}
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 2008);
		cal.set(Calendar.MONTH, Calendar.DECEMBER);
		cal.set(Calendar.DAY_OF_MONTH, 26);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
	
		assertEquals(cal.getTime(), parser.getValidFrom());
		
		cal.set(Calendar.YEAR, 2009);
		cal.set(Calendar.MONTH, Calendar.JANUARY);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		assertEquals(cal.getTime(), parser.getValidUntil());
	}
}
