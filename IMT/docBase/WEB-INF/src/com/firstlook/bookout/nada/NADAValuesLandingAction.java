package com.firstlook.bookout.nada;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.vehicle.description.VehicleOption;
import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;
import biz.firstlook.module.vehicle.description.provider.nada.NADAVehicleBookoutState;
import biz.firstlook.services.vehicleConfigandValuations.entities.BookValuationRequests;
import biz.firstlook.services.vehicleConfigandValuations.entities.Configs;
import biz.firstlook.services.vehicleConfigandValuations.entities.Equipment;
import biz.firstlook.services.vehicleConfigandValuations.entities.EquipmentExplicitActions;
import biz.firstlook.services.vehicleConfigandValuations.entities.ValuationsRequest;
import biz.firstlook.services.vehicleConfigandValuations.entities.ValuationsResponse;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;

public class NADAValuesLandingAction extends SecureBaseAction {

	NadaValuesService nadaValuesService;
	
	
	public NadaValuesService getNadaValuesService() {
		return nadaValuesService;
	}


	public void setNadaValuesService(NadaValuesService nadaValuesService) {
		this.nadaValuesService = nadaValuesService;
	}


	@Override
	public ActionForward doIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws DatabaseException, ApplicationException {

		NADAStandAloneOptionsForm nadaForm= (NADAStandAloneOptionsForm) form;
		
		List<Configs> configs=nadaForm.getConfigs();
		
		configs= getNadaValuesService().getVehicles(getFirstlookSessionFromRequest( request ).getCurrentDealerId()+"", nadaForm.getVin());
		
		NADAVehicleBookoutState state= (NADAVehicleBookoutState) getNadaValuesService().getState(getFirstlookSessionFromRequest( request ).getCurrentDealerId()+"", nadaForm.getVin());
		
		String forward="error";
		if(state==null){
			checkDefaultAsSelected(configs);
			forward="options";
			
			
		}
		else{
			String[][] explicitActions=getNadaValuesService().getEquipmentActions(state,configs);
			
			nadaForm.setExplicitActions(explicitActions);
			
			configs= getNadaValuesService().mergeStateWithConfig(state, configs);
			
			
			
			if(state.getVehicleDescription()!=null &&state.getVehicleDescription().getSelectedVehicleOptions()!=null){
			
				
				ValuationsRequest req= new ValuationsRequest();
				
				BookValuationRequests bookReq= new BookValuationRequests();
				 
				//Setting request Parameters from the vehicle state
				bookReq.setMileage(nadaForm.getMileage());
				bookReq.setTrimId(state.getVehicleDescription().getVehicleId());
				bookReq.setBook(VehicleDescriptionProviderEnum.NADA.getName().toLowerCase());
				bookReq.setReturnId(UUID.randomUUID().toString());
				 
				//Setting equipment Selected
				bookReq.setEquipmentSelected(getEquipmentIdsAsArray(state.getVehicleDescription().getSelectedVehicleOptions()));

				//Setting Equipment Actions
				EquipmentExplicitActions[] actions=new EquipmentExplicitActions[explicitActions.length];
				for(int i=0;i<explicitActions.length;i++){
					actions[i]= new EquipmentExplicitActions(explicitActions[i][0], explicitActions[i][1]);
				}
				bookReq.setEquipmentExplicitActions(actions);
				 
				req.setBookValuationRequests(new BookValuationRequests[]{bookReq});
				 
				//Call to Valuations Service
				ValuationsResponse resp=getNadaValuesService().getNadaValues(getFirstlookSessionFromRequest( request ).getCurrentDealerId()+"",req);
				 
				//Setting Response in the form
				nadaForm.setValuations(resp);
				
				
				if(nadaForm.isOptionsPageDisplay()){
					nadaForm.setIsOptionsPageDisplay(false);
					forward="options";
				}
				else{
					forward="values";
				}
			}

			
		}
		
		nadaForm.setConfigs(configs);
		request.setAttribute("vin", nadaForm.getVin());
		request.setAttribute("mileage", nadaForm.getMileage());
		return mapping.findForward(forward);
	}


	private void checkDefaultAsSelected(List<Configs> configs) {
		
		for(Configs config:configs){
			
			for(Equipment eq:config.getEquipment()){
				
				if(eq.getDefaults())
					eq.setStatus(true);
			}
			
			
		}
		
		
	}


	private String[] getEquipmentIdsAsArray(
			List<VehicleOption> selectedVehicleOptions) {
		List<String> list= new ArrayList<String>();
		
		for(VehicleOption opt:selectedVehicleOptions){
			if(opt.isSelected())
				list.add(opt.getOptionKey());
		}
		String[] array = new String[list.size()];
		for(int i=0;i<list.size();i++){
			array[i]=list.get(i);
		}
		return array;
	}

}
