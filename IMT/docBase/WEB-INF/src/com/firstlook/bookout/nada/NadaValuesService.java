package com.firstlook.bookout.nada;

import java.util.List;

import biz.firstlook.module.vehicle.description.VehicleBookoutState;
import biz.firstlook.module.vehicle.description.provider.nada.NADAVehicleBookoutState;
import biz.firstlook.services.vehicleConfigandValuations.entities.Configs;
import biz.firstlook.services.vehicleConfigandValuations.entities.ValuationsRequest;
import biz.firstlook.services.vehicleConfigandValuations.entities.ValuationsResponse;

public interface NadaValuesService {

	public ValuationsResponse getNadaValues(String dealerId,ValuationsRequest req);
	
	public List<Configs> getVehicles(String dealerId, String vin) ;
	
	public VehicleBookoutState getState(String dealerId,String vin);
	
	public void save(NADAVehicleBookoutState state);
	
	public List<Configs> mergeStateWithConfig(NADAVehicleBookoutState state, List<Configs> configs);

	public String[][] getEquipmentActions(NADAVehicleBookoutState state,
			List<Configs> configs);
}
