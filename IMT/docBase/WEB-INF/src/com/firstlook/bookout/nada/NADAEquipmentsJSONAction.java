package com.firstlook.bookout.nada;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


import biz.firstlook.services.vehicleConfigandValuations.entities.Configs;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;

public class NADAEquipmentsJSONAction extends SecureBaseAction {
	
	@Override
	public ActionForward doIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws DatabaseException, ApplicationException {

		try {	
				NADAStandAloneOptionsForm nadaForm = (NADAStandAloneOptionsForm)form;
				List<Configs> configList = nadaForm.getConfigs();
				Gson gson = new Gson();
				response.setContentType("application/json");
				response.getWriter().write(gson.toJson(configList));
			}
		catch (IOException e) {
			e.printStackTrace();
		}
		
		return mapping.findForward("success");

}
	
}
