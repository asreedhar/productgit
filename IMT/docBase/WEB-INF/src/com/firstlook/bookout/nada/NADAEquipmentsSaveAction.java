package com.firstlook.bookout.nada;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.vehicle.description.VehicleOption;
import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;
import biz.firstlook.module.vehicle.description.provider.nada.NADATPV_VehicleAdapter;
import biz.firstlook.module.vehicle.description.provider.nada.NADAVehicleDescriptionImpl;
import biz.firstlook.module.vehicle.description.provider.nada.NADAVehicleBookoutState;
import biz.firstlook.services.vehicleConfigandValuations.entities.BookValuationRequests;
import biz.firstlook.services.vehicleConfigandValuations.entities.Configs;
import biz.firstlook.services.vehicleConfigandValuations.entities.Equipment;
import biz.firstlook.services.vehicleConfigandValuations.entities.EquipmentExplicitActions;
import biz.firstlook.services.vehicleConfigandValuations.entities.ValuationsRequest;
import biz.firstlook.transact.persist.model.ThirdPartyOption;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;
import biz.firstlook.transact.persist.model.ThirdPartyVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.service.ThirdPartyOptionService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;

public class NADAEquipmentsSaveAction extends SecureBaseAction {
	
	NadaValuesService nadaValuesService;
	ThirdPartyOptionService thirdPartyOptionService;
	
	
	
	public ThirdPartyOptionService getThirdPartyOptionService() {
		return thirdPartyOptionService;
	}


	public void setThirdPartyOptionService(
			ThirdPartyOptionService thirdPartyOptionService) {
		this.thirdPartyOptionService = thirdPartyOptionService;
	}


	public NadaValuesService getNadaValuesService() {
		return nadaValuesService;
	}


	public void setNadaValuesService(NadaValuesService nadaValuesService) {
		this.nadaValuesService = nadaValuesService;
	}
	
	public ActionForward doIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws DatabaseException, ApplicationException {

	
		
		NADAStandAloneOptionsForm nadaForm= (NADAStandAloneOptionsForm) form;
		
		if(nadaForm.getConfigs()==null || nadaForm.getConfigs().size()==0)
			nadaForm.setConfigs(getNadaValuesService().getVehicles(getFirstlookSessionFromRequest( request ).getCurrentDealerId()+"", nadaForm.getVin()));
		
		String forward="";
		NADAVehicleBookoutState state= (NADAVehicleBookoutState) getNadaValuesService().getState(getFirstlookSessionFromRequest( request ).getCurrentDealerId()+"", nadaForm.getVin());
		


		forward="values";
		
		state= constructVehicleBookoutState(nadaForm, nadaForm.getConfigs(), request,state);
		
		getNadaValuesService().save(state);
			
		
		ValuationsRequest req= constructValuationsRequest(nadaForm,state);
			
			
		nadaForm.setValuations(getNadaValuesService().getNadaValues(getFirstlookSessionFromRequest( request ).getCurrentDealerId()+"",req));
		
		request.setAttribute("vin", nadaForm.getVin());
		request.setAttribute("mileage", nadaForm.getMileage());
		return mapping.findForward(forward);
		
	}
	

	private ValuationsRequest constructValuationsRequest(
			NADAStandAloneOptionsForm nadaForm, NADAVehicleBookoutState state) {

		ValuationsRequest req= new ValuationsRequest();
		
		
		BookValuationRequests bookReq= new BookValuationRequests();
		 
		//Setting request Parameters from the vehicle state
		bookReq.setMileage(nadaForm.getMileage());
		bookReq.setTrimId(state.getVehicleDescription().getVehicleId());
		bookReq.setBook(VehicleDescriptionProviderEnum.NADA.getName().toLowerCase());
		bookReq.setReturnId(UUID.randomUUID().toString());
		 
		//Setting equipment Selected
		bookReq.setEquipmentSelected(getEquipmentIdsAsArray(state.getVehicleDescription().getSelectedVehicleOptions()));

		//Setting Equipment Actions
		EquipmentExplicitActions[] actions=new EquipmentExplicitActions[nadaForm.getExplicitActions()!=null?nadaForm.getExplicitActions().length:0];
		for(int i=0;nadaForm.getExplicitActions()!=null&&i<nadaForm.getExplicitActions().length;i++){
			actions[i]= new EquipmentExplicitActions(nadaForm.getExplicitActions()[i][0], nadaForm.getExplicitActions()[i][1]);
		}
		bookReq.setEquipmentExplicitActions(actions);
		 
		req.setBookValuationRequests(new BookValuationRequests[]{bookReq});
		
		
		return req;
	}


	private NADAVehicleBookoutState constructVehicleBookoutState(NADAStandAloneOptionsForm nadaForm, List<Configs> configs,HttpServletRequest request,NADAVehicleBookoutState stateOrig){
		NADAVehicleBookoutState state=stateOrig;
		
		NADAVehicleDescriptionImpl nadaVehicle= new NADAVehicleDescriptionImpl();
		
		nadaVehicle.setMileage(nadaForm.getMileage());
		nadaVehicle.setTrim(nadaForm.getSelectedTrim());
		nadaVehicle.setVehicleId(nadaForm.getSelectedTrim());
		
		if(configs!=null  && configs.size()>0){
			
			nadaVehicle.setMake(configs.get(0).getMake());
			nadaVehicle.setModel(configs.get(0).getModel());
			
		}
		
		Set<ThirdPartyVehicle> tpvs= new HashSet<ThirdPartyVehicle>();
		ThirdPartyVehicle selectedTpv=null; 
		
		for(Configs config:configs){
			
			ThirdPartyVehicle tpv = new ThirdPartyVehicle();
			
			tpv.setThirdPartyId(VehicleDescriptionProviderEnum.NADA.getThirdPartyId());
			tpv.setDateCreated(new Date());
			tpv.setThirdPartyVehicleCode(config.getTrimId());
			tpv.setDescription(config.getTrim());
			tpv.setMake(config.getMake());
			tpv.setModel(config.getModel());
			List<ThirdPartyVehicleOption> tpvos= new ArrayList<ThirdPartyVehicleOption>();
			
			List<ThirdPartyOption> tpos= new ArrayList<ThirdPartyOption>();
			
			for(Equipment e:config.getEquipment()){
				@SuppressWarnings("deprecation")
				ThirdPartyVehicleOption tpvo= new ThirdPartyVehicleOption();
				
				tpvo.setStatus(e.isStatus());
				tpvo.setStandardOption(e.getDefaults());
				tpvo.setDateCreated(new Date());
				tpvo.setSortOrder(Integer.parseInt(e.getSortOrder()));
				
				ThirdPartyOption tpo= new ThirdPartyOption(e.getDesc(), e.getId(), ThirdPartyOptionType.EQUIPMENT); 
				tpos.add(tpo);
				tpvo.setOption(tpo);
				
				if(config.getTrimId().equals(nadaForm.getSelectedTrim())){

					tpv.setStatus(true);
					tpvo.setStatus(Arrays.asList(nadaForm.getSelectedOptions()).contains(e.getId()));
					selectedTpv=tpv;
					
				}
				tpvo.setThirdPartyVehicle(tpv);
				tpvos.add(tpvo);
				
				
				
				
			}
			tpv.setThirdPartyVehicleOptions(tpvos);
			
			tpos = thirdPartyOptionService.getPersistedOption(tpos);
			
			for( ThirdPartyVehicleOption tpvo : tpvos )
			{
				ThirdPartyOption unsynchedOption = tpvo.getOption();
				for (ThirdPartyOption synchedOption : tpos) {
					if (unsynchedOption.getOptionKey().equalsIgnoreCase(synchedOption.getOptionKey())) 
					{
						tpvo.setOption(synchedOption);
						break;
					}
				}
			}
			
			
			tpvs.add(tpv);
			
			
			
		}
		
		nadaVehicle= (NADAVehicleDescriptionImpl) new NADATPV_VehicleAdapter().adaptItem(selectedTpv);
		if(state==null)
			state= new NADAVehicleBookoutState(nadaForm.getVin(), getFirstlookSessionFromRequest( request ).getCurrentDealerId(), nadaVehicle);
		
		state.setVehicleDescription(nadaVehicle);
		
		state.setThirdPartyVehicles(tpvs);
		return state;
	}
	
	
	private String[] getEquipmentIdsAsArray(
			List<VehicleOption> selectedVehicleOptions) {
		List<String> list= new ArrayList<String>();
		
		for(VehicleOption opt:selectedVehicleOptions){
			if(opt.isSelected())
				list.add(opt.getOptionKey());
		}
		String[] array= new String[list.size()];
		
		for(int i=0;i<list.size();i++){
			array[i]=(String)list.get(i);
		}
		
		return array;
	}
}
