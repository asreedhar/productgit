package com.firstlook.bookout.nada;

import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.services.vehicleConfigandValuations.entities.ValuationData;
import biz.firstlook.services.vehicleConfigandValuations.entities.ValuationsResponse;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.google.gson.Gson;

public class NADAValuesJSONAction extends SecureBaseAction {
	
	
	
	@Override
	public ActionForward doIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws DatabaseException, ApplicationException {

		try {	
				NADAStandAloneOptionsForm nadaForm = (NADAStandAloneOptionsForm)form;
				ValuationsResponse vResp = nadaForm.getValuations();
				
				Arrays.sort(vResp.getBookValuations()[0].getValuationData(),new ValuationsComparator());
				
				
				Gson gson = new Gson();
				response.setContentType("application/json");
				response.getWriter().write(gson.toJson(vResp));
			} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		return mapping.findForward("success");

}

	
	
}
class ValuationsComparator implements Comparator<ValuationData>{

	public int compare(ValuationData o2, ValuationData o1) {
		if(o1.getMarket().equalsIgnoreCase("retail")){
			return -1;
		}
		else{
			String condition= o1.getCondition();
			
			if(condition.equalsIgnoreCase("clean")){
				
				if(o2.getCondition().equalsIgnoreCase("loan") ||o2.getCondition().equalsIgnoreCase("average") || o2.getCondition().equalsIgnoreCase("rough"))
					return +1;
				else return -1;
				
				
				
			}
			
			if(condition.equalsIgnoreCase("average")){
				if(o2.getCondition().equalsIgnoreCase("loan") || o2.getCondition().equalsIgnoreCase("rough"))
					return +1;
				else return -1;
				
				
			}
			
			if(condition.equalsIgnoreCase("rough")){
				if(o2.getCondition().equalsIgnoreCase("loan"))
					return +1;
				else return -1;
							
				
				
			}
			
			if(condition.equalsIgnoreCase("loan")){
				
				return -1;
				
			}
			

			
		}
		
		return 0;
	}
	
	
}