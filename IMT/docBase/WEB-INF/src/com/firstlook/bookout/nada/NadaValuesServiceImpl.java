package com.firstlook.bookout.nada;

import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.NamingException;

import biz.firstlook.commons.util.BasicAuthAdapter;
import biz.firstlook.module.vehicle.description.VehicleDescriptionDAO;
import biz.firstlook.module.vehicle.description.VehicleOption;
import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;
import biz.firstlook.module.vehicle.description.provider.nada.NADAVehicleBookoutState;
import biz.firstlook.services.vehicleConfigandValuations.VehicleConfigAndValuationsServiceClient;
import biz.firstlook.services.vehicleConfigandValuations.entities.Configs;
import biz.firstlook.services.vehicleConfigandValuations.entities.Equipment;
import biz.firstlook.services.vehicleConfigandValuations.entities.ValuationsRequest;
import biz.firstlook.services.vehicleConfigandValuations.entities.ValuationsResponse;

public class NadaValuesServiceImpl implements NadaValuesService {

	private VehicleConfigAndValuationsServiceClient client;
	
	private VehicleDescriptionDAO vehicleDescriptionDAO;

	
	
	public NadaValuesServiceImpl(VehicleDescriptionDAO vehicleDescriptionDAO) {
		super();
		this.vehicleDescriptionDAO = vehicleDescriptionDAO;
	}

	public VehicleConfigAndValuationsServiceClient getClient() {
		return client;
	}

	public void setClient(VehicleConfigAndValuationsServiceClient client) {
		this.client = client;
	}

	public VehicleDescriptionDAO getVehicleDescriptionDAO() {
		return vehicleDescriptionDAO;
	}

	public void setVehicleDescriptionDAO(VehicleDescriptionDAO vehicleDescriptionDAO) {
		this.vehicleDescriptionDAO = vehicleDescriptionDAO;
	}

	public ValuationsResponse getNadaValues(String dealerId,ValuationsRequest req) {
		if(client==null){
			String basicAuthString="";
			
			Context initialContext;
			try {
				initialContext = new javax.naming.InitialContext();
				Context ctx = (Context) initialContext.lookup("java:comp/env");
				String user= (String) ctx.lookup("userNameForAdmin");
				String password= (String) ctx.lookup("passwordForAdmin");
				String urlBase = (String) ctx.lookup("flServiceHostUrlBase");
				
				BasicAuthAdapter baa = new BasicAuthAdapter(user, password);
				basicAuthString= baa.basicAuthString();
				
				client= new VehicleConfigAndValuationsServiceClient(urlBase, basicAuthString);
				
			
			} catch (NamingException e1) {
				e1.printStackTrace();
			} 
		}
		ValuationsResponse resp=null;
		try {
			resp = client.getValuations(dealerId, req);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return resp ;
	}

	public List<Configs> getVehicles(String dealerId, String vin) {
		if(client==null){
			String basicAuthString="";
			
			Context initialContext;
			try {
				initialContext = new javax.naming.InitialContext();
				Context ctx = (Context) initialContext.lookup("java:comp/env");
				String user= (String) ctx.lookup("userNameForAdmin");
				String password= (String) ctx.lookup("passwordForAdmin");
				String urlBase = (String) ctx.lookup("flServiceHostUrlBase");
				
				BasicAuthAdapter baa = new BasicAuthAdapter(user, password);
				basicAuthString= baa.basicAuthString();
				
				client= new VehicleConfigAndValuationsServiceClient(urlBase, basicAuthString);
				
			
			} catch (NamingException e1) {
				e1.printStackTrace();
			} 
			
			
		}
		
		Configs[] configs=null;
		try {
			configs = client.getVehicleConfig(dealerId, vin).getConfigs();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		List<Configs> toBeReturned= new ArrayList<Configs>();
		
		for(Configs config:configs){
			if(config.getBook().equalsIgnoreCase(VehicleDescriptionProviderEnum.NADA.getName())){
				toBeReturned.add(config);
			}
		}
		
		return toBeReturned; 
	}

	public NADAVehicleBookoutState getState(String dealerId, String vin) {

		NADAVehicleBookoutState state=
				(NADAVehicleBookoutState)vehicleDescriptionDAO.getVehicleBookoutState(vin, Integer.parseInt(dealerId), VehicleDescriptionProviderEnum.NADA);
		
		
		return state;
	
	}
	public List<Configs> mergeStateWithConfig(NADAVehicleBookoutState state, List<Configs> configs)
	{
		for(Configs config:configs){
			if(config.getTrimId().equalsIgnoreCase(state.getVehicleDescription().getVehicleId())){
				config.setIsSelectedTrim(true);
				config.setExplicitActions(getEquipmentActions(state, configs));
				List<VehicleOption> options= state.getVehicleDescription().getSelectedVehicleOptions();
				
				for(VehicleOption option:options){
					
					for(Equipment e:config.getEquipment()){
						
						if(e.getId().equalsIgnoreCase(option.getOptionKey())&&option.isSelected())
							e.setStatus(true);
						
					}
					
				}
				
			}
			
			
		}
		
		
		return configs;
	}

	

	public void save(NADAVehicleBookoutState state) {
		
		
		// save
		//NadaVehicleBookoutState vbs = (NadaVehicleBookoutState)vehicleDescriptionDAO.getVehicleBookoutState( vin, businessUnitId, getVehicleDescriptionProviderEnum() );
		
		// if null - it's the first time user has hit consumer value tool for this vin - so make a new vehicleBookoutState
		//if ( vbs == null ) {
		//	vbs = new NadaVehicleBookoutState( vin, businessUnitId, null );
		//}
		
		//vbs.setVehicleDescription( description );
		
		
		vehicleDescriptionDAO.saveOrUpdate(state);
		
		
	}

	public String[][] getEquipmentActions(NADAVehicleBookoutState state,
			List<Configs> configs) {
		
		List<String[]> stringList= new ArrayList<String[]>();
		String [][] actions=null;
		for(Configs config:configs){
			if(config.getTrimId().equalsIgnoreCase(state.getVehicleDescription().getVehicleId())){
				
				List<VehicleOption> options= state.getVehicleDescription().getAllOptions();
				
				for(VehicleOption option:options){
					if(option.isSelected()){
						Equipment e=config.getEquipmentById(option.getOptionKey());
						if(e!=null&&!e.isStatus()){
							stringList.add(new String[]{"A",option.getOptionKey()});
						}
						
						
					}
					else{
						Equipment e=config.getEquipmentById(option.getOptionKey());
						if(e!=null&&e.isStatus()){
							stringList.add(new String[]{"R",option.getOptionKey()});
						}
						
					}
					
					
				}
				
			}
		}
		
		actions=new String[stringList.size()][2];
		
		for(int i=0;i<stringList.size();i++){
			actions[i][0]=stringList.get(i)[0];
			actions[i][1]=stringList.get(i)[1];
		}
		
		
		return actions;
	}

	

}
