package com.firstlook.bookout.nada;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.services.vehicleConfigandValuations.entities.Configs;
import biz.firstlook.services.vehicleConfigandValuations.entities.ValuationsResponse;

public class NADAStandAloneOptionsForm extends ActionForm {

	
	private static final long serialVersionUID = 3524580359470399014L;
	
	private Boolean isOptionsPageDisplay;
	private String vin;
	private String mileage;
	private List<Configs> configs;
	private String selectedTrim;
	private String[] selectedOptions;
	private ValuationsResponse valuations;
	private String[][] explicitActions= new String[0][0]; 
	
	
	public String getSelectedTrim() {
		return selectedTrim;
	}

	public void setSelectedTrim(String selectedTrim) {
		this.selectedTrim = selectedTrim;
	}

	public String getMileage() {
		return mileage;
	}

	public void setMileage(String mileage) {
		this.mileage = mileage;
	}

	public String[][] getExplicitActions() {
		return explicitActions;
	}

	public void setExplicitActions(String[][] explicitActions) {
		this.explicitActions = explicitActions;
	}

	public String[] getSelectedOptions() {
		return selectedOptions;
	}

	public void setSelectedOptions(String[] selectedOptions) {
		this.selectedOptions = selectedOptions;
	}

	public List<Configs> getConfigs() {
		return configs;
	}

	public void setConfigs(List<Configs> configs) {
		this.configs = configs;
	}

	public ValuationsResponse getValuations() {
		return valuations;
	}

	public void setValuations(ValuationsResponse valuations) {
		this.valuations = valuations;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public Boolean getIsOptionsPageDisplay() {
		return isOptionsPageDisplay;
	}

	public Boolean isOptionsPageDisplay() {
		return isOptionsPageDisplay;
	}

	public void setIsOptionsPageDisplay(Boolean isOptionsPageDisplay) {
		this.isOptionsPageDisplay = isOptionsPageDisplay;
	}
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		setIsOptionsPageDisplay(Boolean.FALSE);
		
		super.reset(mapping, request);
	}
	
	
}
