package com.firstlook.action.redirection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.SoftwareSystemComponentState;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.softwaresystem.SoftwareSystemStateService;
import com.firstlook.util.ParameterActionForward;

public class ReportCenterRedirectionAction extends BaseRedirectionAction {

	public ActionForward doIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws DatabaseException, ApplicationException {
		
		String token = SoftwareSystemStateService.DEALER_SYSTEM_COMPONENT;
		
		if (request.getParameterMap().containsKey(DEALER_GROUP_ID)) {
			token = SoftwareSystemStateService.DEALER_GROUP_SYSTEM_COMPONENT;
		}
		
		SoftwareSystemComponentState state = findOrCreateSoftwareSystemComponentState(
				request,
				token);
		
		ActionForward forward = null;
		
		if (state != null) {
			ParameterActionForward success = new ParameterActionForward(mapping.findForward("success"));
			success.addParameter("token", token);
			forward = success;
		}
		else {
			forward = mapping.findForward("failure");
		}
		
		return forward;
	}
	
}
