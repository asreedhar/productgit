package com.firstlook.action.redirection;

import javax.servlet.http.HttpServletRequest;

import biz.firstlook.commons.util.Functions;

import com.discursive.cas.extend.client.CASFilter;
import com.firstlook.action.SecureBaseAction;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.SoftwareSystemComponentState;
import com.firstlook.service.softwaresystem.SoftwareSystemStateService;

public abstract class BaseRedirectionAction extends SecureBaseAction {

	protected static final String DEALER_GROUP_ID = "dealerGroupId";

	private SoftwareSystemStateService softwareSystemStateService;

	protected SoftwareSystemComponentState findOrCreateSoftwareSystemComponentState(HttpServletRequest request) {
		return findOrCreateSoftwareSystemComponentState(request, SoftwareSystemStateService.DEALER_SYSTEM_COMPONENT);
	}
	
	protected SoftwareSystemComponentState findOrCreateSoftwareSystemComponentState(HttpServletRequest request, String token) {
		SoftwareSystemComponentState state = getSoftwareSystemStateService().findOrCreateSoftwareSystemComponentState(
				getLogin(request),
				token,
				getDealerGroupId(request),
				getCurrentDealerId(request));
		return state;
	}
	
	protected boolean isPopup(HttpServletRequest request) {
		return notNullOrEmpty(Functions.nullSafeToString(request.getParameter("popup")));
	}
	
	protected boolean notNullOrEmpty(String value) {
		return !isNullOrEmpty(value);
	}
	
	protected boolean isNullOrEmpty(String value) {
		return (value == null || value.equals("null") || value.equals(""));
	}
	
	protected String getLogin(HttpServletRequest request) {
		return (String) request.getSession().getAttribute(CASFilter.CAS_FILTER_USER);
	}
	
	protected Integer getDealerGroupId(HttpServletRequest request) {
		Integer dealerGroupId = Functions.nullSafeInteger(request.getParameter(DEALER_GROUP_ID));
		if (dealerGroupId == null) {
			DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerId(getCurrentDealerId(request));
			if (dealerGroup != null) {
				dealerGroupId = dealerGroup.getDealerGroupId();
			}
		}
		return dealerGroupId;
	}

	public SoftwareSystemStateService getSoftwareSystemStateService() {
		return softwareSystemStateService;
	}

	public void setSoftwareSystemStateService(SoftwareSystemStateService softwareSystemStateService) {
		this.softwareSystemStateService = softwareSystemStateService;
	}
}
