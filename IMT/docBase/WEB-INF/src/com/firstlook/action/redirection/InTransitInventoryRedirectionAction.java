package com.firstlook.action.redirection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.Functions;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.SoftwareSystemComponentState;
import com.firstlook.exception.ApplicationException;
import com.firstlook.util.ParameterActionForward;

public class InTransitInventoryRedirectionAction extends BaseRedirectionAction 
{
	private static final String APPRAISALID = "appraisalId";	
	
	public ActionForward doIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws DatabaseException, ApplicationException {
		
		SoftwareSystemComponentState state = findOrCreateSoftwareSystemComponentState(request);
		
		ActionForward forward = null;
		
		if (state != null) {
			
			ParameterActionForward success = new ParameterActionForward(mapping.findForward("success"));
			success.addParameter("pageName", "Pages/Inventory/");	
			
			final String appraisalId = Functions.nullSafeToString(request.getParameter(APPRAISALID));
			if (!isNullOrEmpty(appraisalId))
			{
				success.addParameter(APPRAISALID, appraisalId);
			}
			
			if (isPopup(request)) {
				success.addParameter("popup", "true");
			}
			
			forward = success;
		}
		else {
			
			forward = mapping.findForward("failure");
			
		}
		
		return forward;
	}
}
