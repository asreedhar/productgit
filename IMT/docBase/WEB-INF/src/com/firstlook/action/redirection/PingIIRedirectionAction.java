package com.firstlook.action.redirection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import biz.firstlook.commons.util.Functions;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.SoftwareSystemComponentState;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.softwaresystem.SoftwareSystemStateService;
import com.firstlook.util.ParameterActionForward;

public class PingIIRedirectionAction extends BaseRedirectionAction {
	
	public ActionForward doIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws DatabaseException, ApplicationException {
		
		SoftwareSystemComponentState state = findOrCreateSoftwareSystemComponentState(request);
		
		ActionForward forward = null;
		
		if (state != null) {

			final String inventoryId = Functions.nullSafeToString(request.getParameter("inventoryId"));
			
			final String appraisalId = Functions.nullSafeToString(request.getParameter("appraisalId"));
			
			final String stockNum = Functions.nullSafeToString(request.getParameter("stockNumber"));
			
			final String vehicleEntityTypeId = Functions.nullSafeToString(request.getParameter("vehicleEntityTypeId"));
			
			final String vehicleEntityId = Functions.nullSafeToString(request.getParameter("vehicleEntityId"));
			
			final String q = Functions.nullSafeToString(request.getParameter("q"));
			
			ParameterActionForward success = new ParameterActionForward(mapping.findForward("success"));
			
			success.addParameter("token", SoftwareSystemStateService.DEALER_SYSTEM_COMPONENT);

			if (!isNullOrEmpty(vehicleEntityTypeId) && !isNullOrEmpty(vehicleEntityId)) {
				success.addParameter("vehicleEntityTypeId", vehicleEntityTypeId);
				success.addParameter("vehicleEntityId", vehicleEntityId);
			}
			
			if(!isNullOrEmpty(inventoryId)){
				success.addParameter("vehicleEntityTypeId", "1");
				success.addParameter("vehicleEntityId", inventoryId);
			}
			
			if(!isNullOrEmpty(appraisalId)){
				success.addParameter("vehicleEntityTypeId", "2");
				success.addParameter("vehicleEntityId", appraisalId);
			}
				
			if(!isNullOrEmpty(stockNum)) {
				success.addParameter("stockNumber", stockNum);
			}
			
			if(!isNullOrEmpty(q)) {
				String safeQ = q.replaceAll("'", "\\\\'");
				try {
					success.addParameter("q", URLEncoder.encode(safeQ, "UTF-8"));
				} catch (UnsupportedEncodingException e) {
					success.addParameter("q", safeQ);
				}
			}
			
			if(isPopup(request)){
				success.addParameter("popup", "true");
			}
							
			forward = success;
		}
		else {
			forward = mapping.findForward("failure");
		}
		
		return forward;
	}

}
