package com.firstlook.action.redirection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.Functions;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.SoftwareSystemComponentState;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.softwaresystem.SoftwareSystemStateService;
import com.firstlook.util.ParameterActionForward;

public class JdPowerRedirectionAction extends BaseRedirectionAction {

	private static final String URL = "JdPowerRedirectionAction.go?";
	private static final String VEHICLE_CATALOG_ID = "vehicleCatalogId";
	private static final String MODELYEAR = "modelYear";
	private static final String PAGE_NAME = "Pages/JDPower/JDPower.aspx";

	public ActionForward doIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws DatabaseException, ApplicationException {

		SoftwareSystemComponentState state = findOrCreateSoftwareSystemComponentState(request);

		ActionForward forward = null;

		if (state != null) {
			final String vehicleCatalogId = Functions.nullSafeToString(request.getParameter(VEHICLE_CATALOG_ID));
			final String modelYear = Functions.nullSafeToString(request.getParameter(MODELYEAR));
			ParameterActionForward success = new ParameterActionForward(mapping.findForward("success"));
			success.addParameter("token", SoftwareSystemStateService.DEALER_SYSTEM_COMPONENT);
			success.addParameter("pageName", PAGE_NAME);
			success.addParameter(VEHICLE_CATALOG_ID, vehicleCatalogId);
			success.addParameter(MODELYEAR, modelYear);
			forward = success;
		}
		else {
			forward = mapping.findForward("failure");
		}

		return forward;
	}

	public static String getRelativeUrl(Integer vehicleCatalogId, Integer modelYear) {
		StringBuffer jdPowerUrl = new StringBuffer(JdPowerRedirectionAction.URL);
		jdPowerUrl.append(JdPowerRedirectionAction.VEHICLE_CATALOG_ID).append("=").append(vehicleCatalogId);
		jdPowerUrl.append("&").append(JdPowerRedirectionAction.MODELYEAR).append("=").append(modelYear);
		return jdPowerUrl.toString();
	}

}
