package com.firstlook.action.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.form.DealerGroupDealerForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.service.accountrep.AccountRepService;

public class EditAccountRepSubmitAction extends AdminBaseAction
{

private AccountRepService accountRepService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int memberId = RequestHelper.getInt( request, "memberId" );
	DealerGroupDealerForm dealerForm = (DealerGroupDealerForm)form;
	int[] dealerIdArray = dealerForm.getDealerIdArray();

	getAccountRepService().updateDealerRelationships( memberId, dealerIdArray );

	request.setAttribute( "memberId", new Integer( memberId ) );
	return mapping.findForward( "success" );
}

public AccountRepService getAccountRepService()
{
    return accountRepService;
}

public void setAccountRepService( AccountRepService accountRepService )
{
    this.accountRepService = accountRepService;
}

}
