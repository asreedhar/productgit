package com.firstlook.action.admin;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.accountrep.AccountRepService;
import com.firstlook.session.FirstlookSession;

public class AccountRepDealerListAction extends SecureBaseAction
{

private AccountRepService accountRepService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	FirstlookSession firstlookSession = (FirstlookSession)request.getSession( false ).getAttribute( FirstlookSession.FIRSTLOOK_SESSION );
	Member memberSessionState = firstlookSession.getMember();

	Collection dealers = getAccountRepService().representativeDealers( memberSessionState.getMemberId().intValue() );
	List dealerGroups = getAccountRepService().retriveDealerGroups( dealers, getDealerGroupService() );

	Collections.sort( dealerGroups, new BeanComparator( "name" ) );

	request.setAttribute( "dealerGroups", dealerGroups );

	return mapping.findForward( "success" );
}

public AccountRepService getAccountRepService()
{
    return accountRepService;
}

public void setAccountRepService( AccountRepService accountRepService )
{
    this.accountRepService = accountRepService;
}


}
