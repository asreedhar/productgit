package com.firstlook.action.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.MemberForm;
import com.firstlook.exception.ApplicationException;

public class ChangePasswordDisplayAction extends SecureBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    MemberForm memberForm = (MemberForm)form;
    
    Member member = getMemberFromRequest(request);
    memberForm.setBusinessObject( member );
    request.setAttribute( "memberForm", memberForm );
    request.setAttribute( "changePasswordStatus", "false" );

    String loginEntryType = member.getLoginEntryPoint();

    if ( loginEntryType != null && loginEntryType.equals( Member.LOGIN_ENTRY_POINT_INSIGHT ) )
    {
        return mapping.findForward( "successInsight" );
    }
    else
    {
        return mapping.findForward( "successVIP" );
    }

}

}
