package com.firstlook.action.admin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanComparator;

import biz.firstlook.transact.persist.persistence.InventoryBookOutDAO;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Corporation;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.IBusinessUnit;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.InventoryForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.persistence.businessunit.IBusinessUnitDAO;
import com.firstlook.persistence.corporation.ICorporationDAO;
import com.firstlook.persistence.inventory.IInventoryDAO;
import com.firstlook.service.businessunit.BusinessUnitRelationshipService;
import com.firstlook.service.businessunit.BusinessUnitService;
import com.firstlook.service.corporation.CorporationService;
import com.firstlook.service.dealer.IDealerService;
import com.firstlook.service.inventory.InventoryService_Legacy;

public abstract class AdminBaseAction extends SecureBaseAction
{

private IBusinessUnitDAO buDAO;
private ICorporationDAO corpDAO;
private IInventoryDAO inventoryPersistence;
private InventoryBookOutDAO inventoryBookOutDAO;

public void setIsPrintableInRequest( HttpServletRequest request ) throws ApplicationException
{
    super.setIsPrintableInRequest( request );

    Member member = getMemberFromRequest( request );

    if ( !member.isAdmin() && !member.isAccountRep()) {
    	throw new ApplicationException( "You are not authorized to view this page." );
    }
}

protected Collection<IBusinessUnit> returnAllDealergroupsAndCorporations() throws ApplicationException, DatabaseException
{
	BusinessUnitService buService = new BusinessUnitService( buDAO );
	BusinessUnitRelationshipService relationshipService = new BusinessUnitRelationshipService( getBurDAO() );
	CorporationService corpService = new CorporationService( corpDAO );

	List<IBusinessUnit> allTopLevels = new ArrayList<IBusinessUnit>();
	Collection<Corporation> corporations = corpService.retrieveAllCorporations();
	allTopLevels.addAll( corporations );

	int firstLookId = buService.retrieveFirstLookBusinessUnitId();

	Collection dealerGroupRelationships = relationshipService.retrieveIdsByParentId( new Integer( firstLookId ) );

	if ( dealerGroupRelationships != null && !dealerGroupRelationships.isEmpty() )
	{
		Collection<DealerGroup> dealergroups = getDealerGroupService().retrieveInDealerGroupIdList( dealerGroupRelationships );
		
		//Make sure name starts with uppercase character.  For sorting a few lines further down.		
		dealerNamesUppercaseFirst(dealergroups);
				
		allTopLevels.addAll( dealergroups );
	}

	BeanComparator comparator = new BeanComparator( "name" );
	Collections.sort( allTopLevels, comparator );

	return allTopLevels;

}

/**
 * This method should not be in the Action, but in the JSP.
 * @param dealergroups
 */
private void dealerNamesUppercaseFirst(Collection<DealerGroup> dealergroups)
{
	if(dealergroups == null)
		return;
	
	Iterator<DealerGroup> iter = dealergroups.iterator();
	DealerGroup dg = null;
	String name=null;
	char firstLetter;
	while(iter.hasNext())
	{
		dg = iter.next();
		name = dg.getName();
		firstLetter = name.charAt(0);
		if(Character.isLowerCase( name.charAt(0)) )
		{
			String newName = Character.toString(Character.toUpperCase(firstLetter)) + name.substring(1);
			dg.setName(newName);
		}
	}
	
}

protected Collection findDealersByTopLevelId( int parentId ) throws DatabaseException, ApplicationException
{
	Collection childIds = getImtDealerService().retrieveByDealerGroupId( parentId );

	return getImtDealerService().retrieveByDealerIds( childIds );
}

protected String retrieveDealerGroupName( int memberType, String dealerGroupName )
{
	String returnedDealerGroupName = "Dealer Group Not Assigned";

	if ( memberType == Member.MEMBER_TYPE_USER )
	{
		returnedDealerGroupName = dealerGroupName;
	}
	else if ( memberType == Member.MEMBER_TYPE_ADMIN )
	{
		returnedDealerGroupName = "N/A for Admin";
	}
	else if ( memberType == Member.MEMBER_TYPE_ACCOUNT_REP )
	{
		returnedDealerGroupName = "N/A for Account Rep";
	}

	return returnedDealerGroupName;
}

/**
 * Made for compatibility reasons. This section needs to somehow be consolidated with VehicleDisplayBaseAction.
 * @param request
 * @param dealerService
 * @return
 * @throws ApplicationException
 */
protected InventoryEntity putVehicleInRequest( HttpServletRequest request, IDealerService dealerService) throws ApplicationException
{
	int inventoryId = RequestHelper.getInt( request, "vehicleId" );
	return putVehicleInRequest( request, inventoryId, dealerService );
}

protected InventoryEntity putVehicleInRequest( HttpServletRequest request, Integer inventoryId, IDealerService dealerService) throws ApplicationException
{	
	InventoryService_Legacy inventoryService = new InventoryService_Legacy( inventoryPersistence );
    InventoryEntity inventory = inventoryService.retrieveInventory( inventoryId );
    Dealer owningDealer = dealerService.retrieveDealer( inventory.getDealerId() );
	Collection options = inventoryBookOutDAO.findSelectedOptionsOfAllTypesByInventoryId(
																										inventory.getInventoryId(),
																										owningDealer.getDealerPreference().getGuideBookIdAsInt() );

    InventoryForm vehicleForm = new InventoryForm( inventory );
    vehicleForm.setDealer( owningDealer );

    request.setAttribute( "selectedOptions", options );
    request.setAttribute( "vehicleForm", vehicleForm );
    return inventory;
}

public ICorporationDAO getCorpDAO()
{
	warnIfNoDAO( corpDAO, "corpDao" );	
	return corpDAO;
}

public void setCorpDAO( ICorporationDAO corpDAO )
{
	this.corpDAO = corpDAO;
}

public IBusinessUnitDAO getBuDAO()
{
	warnIfNoDAO( buDAO, "buDao" );	
	return buDAO;
}

public void setBuDAO( IBusinessUnitDAO buDAO )
{
	this.buDAO = buDAO;
}

public IInventoryDAO getInventoryPersistence()
{
	return inventoryPersistence;
}

public void setInventoryPersistence( IInventoryDAO inventoryPersistence )
{
	this.inventoryPersistence = inventoryPersistence;
}

public InventoryBookOutDAO getInventoryBookOutDAO()
{
	return inventoryBookOutDAO;
}

public void setInventoryBookOutDAO( InventoryBookOutDAO inventoryBookOutDAO )
{
	this.inventoryBookOutDAO = inventoryBookOutDAO;
}


}
