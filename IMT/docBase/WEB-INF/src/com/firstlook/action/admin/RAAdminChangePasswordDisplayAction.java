package com.firstlook.action.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.form.MemberForm;
import com.firstlook.exception.ApplicationException;

public class RAAdminChangePasswordDisplayAction extends SecureBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    MemberForm memberForm = (MemberForm)form;
    memberForm.setBusinessObject( getMemberFromRequest(request) );
    request.setAttribute( "memberForm", memberForm );
    request.setAttribute( "changePasswordStatus", "false" );

    return mapping.findForward( "success" );
}

}
