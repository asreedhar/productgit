package com.firstlook.action.admin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.DSTDealerSearchForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.accountrep.AccountRepService;
import com.firstlook.service.businessunit.BusinessUnitRelationshipService;
import com.firstlook.session.FirstlookSession;

public class AccountRepDealerSearchSubmitAction extends AdminBaseAction
{

private AccountRepService accountRepService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	DSTDealerSearchForm dsForm = (DSTDealerSearchForm)form;
	Collection<Dealer> accessibleDealers = new ArrayList<Dealer>();

	BusinessUnitRelationshipService relationshipService = new BusinessUnitRelationshipService( getBurDAO() );
	Collection businessRelationshipCol = relationshipService.retrieveByParentId( new Integer( dsForm.getTopLevelBusinessUnitId() ) );

	Collection dealers = getImtDealerService().retrieveByDealerGroupIdAndNameAndNickName( businessRelationshipCol, dsForm.getName(),
																					dsForm.getNickname() );
	Iterator dealersIter = dealers.iterator(); // all dealerships in
	// dealergroup

	FirstlookSession firstlookSession = (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION );
	Member memberSessionState = firstlookSession.getMember();

	Collection associatedDealers = getAccountRepService().representativeDealers( memberSessionState.getMemberId().intValue() ); // all
	// dealers
	// associated
	// with
	// member

	while ( dealersIter.hasNext() )
	{
		Dealer dealer = (Dealer)dealersIter.next();
		if ( associatedDealers.contains( dealer ) )
		{
			accessibleDealers.add( dealer );
		}
	}

	request.setAttribute( "dealers", accessibleDealers );
	return mapping.findForward( "success" );
}

public AccountRepService getAccountRepService()
{
    return accountRepService;
}

public void setAccountRepService( AccountRepService accountRepService )
{
    this.accountRepService = accountRepService;
}

}
