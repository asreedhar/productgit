package com.firstlook.action.components.auction;

public class AuctionTileForm
{

private String make;
private String model;
private Integer mileage;
private Integer modelYear;
private String vin;

public String getMake()
{
	return make;
}
public void setMake( String make )
{
	this.make = make;
}
public Integer getMileage()
{
	return mileage;
}
public void setMileage( Integer mileage )
{
	this.mileage = mileage;
}
public String getModel()
{
	return model;
}
public void setModel( String model )
{
	this.model = model;
}
public String getVin()
{
	return vin;
}
public void setVin( String vin )
{
	this.vin = vin;
}
public Integer getModelYear()
{
	return modelYear;
}
public void setModelYear( Integer modelYear )
{
	this.modelYear = modelYear;
}

}
