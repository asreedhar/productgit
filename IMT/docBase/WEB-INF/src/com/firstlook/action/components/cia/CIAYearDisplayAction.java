package com.firstlook.action.components.cia;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.tiles.ComponentContext;

import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.GroupingDescription;
import biz.firstlook.transact.persist.persistence.IDealerPreferenceDAO;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.components.VestigialItemUtil;
import com.firstlook.action.dealer.reports.UsedCarPerformanceAnalyzerPlusDisplayHelper;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.form.GroupingForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.groupingdescription.GroupingDescriptionService;

public class CIAYearDisplayAction extends SecureBaseAction
{

private IDealerPreferenceDAO dealerPrefDAO;
private UsedCarPerformanceAnalyzerPlusDisplayHelper usedCarPerformanceAnalyzerPlusDisplayHelper;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	ComponentContext context = ComponentContext.getContext( request );
	int ciaGroupingItemId = Integer.parseInt( (String)context.getAttribute( "ciaGroupingItemId" ) );
	int groupingDescriptionId = Integer.parseInt( (String)context.getAttribute( "groupingDescriptionId" ) );
	int weeks = Integer.parseInt( (String)context.getAttribute( "weeks" ) );
	int forecast = Integer.parseInt( (String)context.getAttribute( "forecast" ) );
	int mileageFilter = Integer.parseInt( (String)context.getAttribute( "mileageFilter" ) );

	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();

	int mileageThreshold = usedCarPerformanceAnalyzerPlusDisplayHelper.retrieveMileageThreshold( mileageFilter, currentDealerId );

	GroupingDescriptionService gdService = new GroupingDescriptionService();
	GroupingDescription groupingDescription = gdService.retrieveById( new Integer( groupingDescriptionId ) );

	request.setAttribute( "groupingForm", new GroupingForm( groupingDescription ) );

	DealerPreference dealerPreference = dealerPrefDAO.findByBusinessUnitId( currentDealerId );
	int lowerUnitCostThreshold = dealerPreference.getUnitCostThresholdLowerAsInt();
	usedCarPerformanceAnalyzerPlusDisplayHelper.putYearItemsInRequest( request, getMemberFromRequest( request ).getProgramType(), weeks, forecast, mileageThreshold,
										groupingDescriptionId, getFirstlookSessionFromRequest( request ).isIncludeMarket(),
										lowerUnitCostThreshold, ciaGroupingItemId, new Integer( currentDealerId ) );

	if ( getImtDealerService().showLithiaRoi( currentDealerId ) )
	{
		request.setAttribute( "showAnnualRoi", Boolean.TRUE );
	}
	else
	{
		request.setAttribute( "showAnnualRoi", Boolean.FALSE );
	}

	VestigialItemUtil viUtil = new VestigialItemUtil();
	viUtil.populateVestigialItemsBasedOnPreferences( request, "year", false );
	putDealerFormInRequest( request, 0 );
	return mapping.findForward( "success" );
}

public IDealerPreferenceDAO getDealerPrefDAO()
{
	return dealerPrefDAO;
}

public void setDealerPrefDAO( IDealerPreferenceDAO dealerPrefDAO )
{
	this.dealerPrefDAO = dealerPrefDAO;
}

public void setUsedCarPerformanceAnalyzerPlusDisplayHelper(
															UsedCarPerformanceAnalyzerPlusDisplayHelper usedCarPerformanceAnalyzerPlusDisplayHelper )
{
	this.usedCarPerformanceAnalyzerPlusDisplayHelper = usedCarPerformanceAnalyzerPlusDisplayHelper;
}

}
