package com.firstlook.action.components;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.model.JobTitle;
import biz.firstlook.transact.persist.person.IPerson;
import biz.firstlook.transact.persist.person.PositionType;
import biz.firstlook.transact.persist.service.appraisal.AppraisalActionType;
import biz.firstlook.transact.persist.service.appraisal.AppraisalValue;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.dealer.redistribution.TradeManagerEditSubmitAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.email.LithiaCarCenterAppraisalActionChangeEmailBuilder;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.TradeManagerEditBumpForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.SessionHelper;
import com.firstlook.session.FirstlookSession;

public class AppraisalBumpDisplayAction extends SecureBaseAction
{
protected static Logger logger = Logger.getLogger( AppraisalBumpDisplayAction.class );	

private IAppraisalService appraisalService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int appraisalId = RequestHelper.getInt( request, "appraisalId" );

	boolean dealerHasAuctionData = getFirstlookSessionFromRequest( request ).isIncludeAuction();
	request.setAttribute( "dealerHasAuctionData", ( new Boolean( dealerHasAuctionData ) ) );
	putBumpsInRequest( request, new Integer( appraisalId ) );
	return mapping.findForward( "success" );
}

private void putBumpsInRequest( HttpServletRequest request, Integer appraisalId ) throws DatabaseException, ApplicationException
{
	IAppraisal appraisal = appraisalService.findBy( appraisalId );
	List<AppraisalValue> appraisalValues = new ArrayList<AppraisalValue>();
	appraisalValues.addAll( appraisal.getAppraisalValues() );
	Collections.reverse( appraisalValues );
	TradeManagerEditBumpForm tradeManagerEditBumpForm = new TradeManagerEditBumpForm();
	String dateString="";
	if ( !appraisalValues.isEmpty() )
	{
		AppraisalValue firstValue = (AppraisalValue)appraisalValues.get( 0 );
		tradeManagerEditBumpForm.setAppraiserName( firstValue.getAppraiserName() );
		dateString = firstValue.getDateCreated().toLocaleString();
		tradeManagerEditBumpForm.setAppraisalBumpDate(dateString);
		tradeManagerEditBumpForm.setAppraisalValue( firstValue.getValue() );
	}
	tradeManagerEditBumpForm.setVehicleGuideBookBumpCol( appraisalValues );
	tradeManagerEditBumpForm.setAppraisalId( appraisalId.intValue() );
	if ( !appraisalValues.isEmpty() )
	{
		request.setAttribute( "numberOfAppraisal", appraisalValues.size() );
	}
	FirstlookSession flSession = (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION );
	int currentDealerId = ( flSession ).getCurrentDealerId();
	DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerId( currentDealerId );
	Dealer dealer = getDealerDAO().findByPk( currentDealerId );
	
	request.setAttribute("displayTMV", flSession.hasEdmundsTmvUpgrade());
	
	if( dealerGroup.isLithiaStore()) {
		loadLithiaCarCenterAttributes(request, dealer);
	}
	
    checkInGroupAppraisals(appraisal.getBusinessUnitId(), dealerGroup.getDealerGroupId(),appraisal.getVehicle().getVin(), request);
    
    request.setAttribute("appraisersList", getPersonService().getBusinessUnitPeopleByPosition(currentDealerId,PositionType.APPRAISER));
    
	request.setAttribute("isLithia", new Boolean( dealerGroup.isLithiaStore() ));
	request.setAttribute( "tradeManagerEditBumpForm", tradeManagerEditBumpForm );
    request.setAttribute("vin", appraisal.getVehicle().getVin());
	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );
	SessionHelper.keepAttribute( request, "auctionData" );
}

private void loadLithiaCarCenterAttributes( HttpServletRequest request, Dealer dealer ) {
	List<Member> carCenterMembers = getMemberService().getMembersByBusinessUnitAndJobTitle(dealer.getDealerId(), JobTitle.LITHIA_MERCH_MANAGER_ID);
	request.setAttribute( "lithiaCarCenterContacts", carCenterMembers );
	request.setAttribute( "defaultLithiaCarCenterContact", dealer.getDealerPreference().getDefaultLithiaCarCenterMemberId() );
}

private void formatPeople(List<IPerson> people){
	String temp;
	for (IPerson person:people){
		if (person.getFirstName() != null && !person.getFirstName().equals("")) {
			temp = person.getFirstName();
			person.setFirstName( Character.toUpperCase(temp.charAt(0)) + temp.substring(1, temp.length()));
		} else {
			person.setFirstName("");
		}
		if (person.getLastName() != null && !person.getLastName().equals("")) {
			temp = person.getLastName();
			person.setLastName( Character.toUpperCase(temp.charAt(0)) + temp.substring(1, temp.length()));
		} else {
			person.setLastName("");
		}
	}
}

@SuppressWarnings("unchecked")
private void checkInGroupAppraisals(Integer businessUnitId, Integer parentId, String vin, HttpServletRequest request) {
    List<Map> results = appraisalService.retrieveInGroupAppraisals(businessUnitId, parentId,vin);
    boolean hasGroupAppraisals = (results.isEmpty() ? false:true);
    request.setAttribute("hasGroupAppraisals", hasGroupAppraisals);
    if (hasGroupAppraisals) {
        request.setAttribute("created",results.get(0).get("created"));
    } 
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}


}