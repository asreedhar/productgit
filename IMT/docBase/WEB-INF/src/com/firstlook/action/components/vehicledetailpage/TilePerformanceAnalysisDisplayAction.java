package com.firstlook.action.components.vehicledetailpage;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.tiles.ComponentContext;

import com.firstlook.action.SecureTileAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;

public class TilePerformanceAnalysisDisplayAction extends SecureTileAction
{

	public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response ) throws DatabaseException, ApplicationException
	{
		ComponentContext context = ComponentContext.getContext( request );
		
		String make = (String)context.getAttribute( "make" );
		String model = (String)context.getAttribute( "model" );
		String trim = (String)context.getAttribute( "trim" );
		String includeDealerGroup = (String)context.getAttribute( "includeDealerGroup" ); // showDealer or showDealerGroup
		String vin = (String)context.getAttribute( "vin" );
		String groupingDescription = (String)context.getAttribute( "groupingDescription" );
		String mileage = (String)context.getAttribute( "mileage" );
		String year = (String)context.getAttribute( "year" );
		String weeks = (String)context.getAttribute( "weeks" );

		request.setAttribute( "make", make );
		request.setAttribute( "model", model );
		request.setAttribute( "trim", trim );
		request.setAttribute( "includeDealerGroup", includeDealerGroup );
		request.setAttribute( "vin", vin );
		request.setAttribute( "groupingDescription", groupingDescription );
		request.setAttribute( "mileage", mileage );
		request.setAttribute( "year", year );
		request.setAttribute( "weeks", weeks );

		return mapping.findForward( "success" );
	}

}
