package com.firstlook.action.components;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.tiles.ComponentContext;

import biz.firstlook.transact.persist.model.MakeModelGrouping;
import biz.firstlook.transact.persist.service.MakeModelGroupingService;

import com.firstlook.action.SecureTileAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.ReportGroupingHelper;
import com.firstlook.persistence.ReportPersist;
import com.firstlook.report.Report;
import com.firstlook.report.ReportGrouping;
import com.firstlook.report.ReportGroupingForm;
import com.firstlook.session.FirstlookSession;

/**
 * currently only used in UCBP
 */
public class ReportGroupingDisplayAction extends SecureTileAction
{

private MakeModelGroupingService makeModelGroupingService;
private ReportPersist reportPersist;
private ReportGroupingHelper reportGroupingHelper;


public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	ComponentContext context = ComponentContext.getContext( request );

	String mmgStr = (String)context.getAttribute( "makeModelGroupingId" );
	int makeModelGroupingId = -1;
	int groupingDescriptionId = -1;
	if (mmgStr != null && mmgStr.trim().length() > 0) {
		makeModelGroupingId = Integer.parseInt(mmgStr);
	} else {
		String groupingDescriptionIdStr = (String)context.getAttribute( "groupingDescriptionId" );
		groupingDescriptionId = Integer.parseInt( groupingDescriptionIdStr );		
	}
	
	String weeksStr = (String)context.getAttribute( "weeks" );
	String includeDealerGroupStr = (String)context.getAttribute( "includeDealerGroup" );
	String forecastStr = (String)context.getAttribute( "forecast" );
	String make = (String)context.getAttribute( "make" );
	String model = (String)context.getAttribute( "model" );
	int forecast = Integer.parseInt( forecastStr );
	int weeks = Integer.parseInt( weeksStr );
	int includeDealerGroup = Integer.parseInt( includeDealerGroupStr );
	String[] makeModel = new String[2];
	makeModel = determineMakeAndModelIfNotPassed( make, model, groupingDescriptionId, makeModelGroupingId );

	ReportGrouping generalReportGrouping = reportGroupingHelper.findByMakeModelOrTrimOrBodyStyle( makeModel[0], makeModel[1], null,
																									includeDealerGroup,
																									((FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION )).getCurrentDealerId(), weeks,
																									InventoryEntity.USED_CAR );
	if ( generalReportGrouping != null )
	{
		Report report = new Report();
		reportPersist.findAllAverages( report, ((FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION )).getCurrentDealerId(), weeks, forecast, InventoryEntity.USED_CAR );
		generalReportGrouping.calculateOptimixPercentages( report );
	}

	request.setAttribute( "reportGroupingForm", new ReportGroupingForm( generalReportGrouping ) );

	return mapping.findForward( "success" );
}

/**
 * 4-18-05 KL - The reason I am doing this is that the current version of
 * FLDW..GetTradeAnalyzerReport takes a make and model. Calling this stored proc
 * with a trim of null, as is the case here, causes the proc to roll up results
 * to the grouping description level anyway. So it doesn't matter that the
 * actual make and model may not line up with the original vehicle - they will
 * all line up under the grouping description. Do not alter this method without
 * changing these comments.
 */
private String[] determineMakeAndModelIfNotPassed( String make, String model, int groupingDescriptionId, int makeModelGroupingId )
{
	String[] makeModel = new String[2];
	if (makeModelGroupingId != -1 && makeModelGroupingId > 0) {
		MakeModelGrouping makeModelGrouping = (MakeModelGrouping)getMakeModelGroupingService().retrieveByPk( new Integer( makeModelGroupingId ) );
		makeModel[0] = makeModelGrouping.getMake();
		makeModel[1] = makeModelGrouping.getModel();
	}
	else if ( ( make == null || make.trim().length() == 0 ) && groupingDescriptionId > 0 )
	{
		List makeModelGroupings = getMakeModelGroupingService().retrieveMakeModelGroupings( new Integer( groupingDescriptionId ) );
		MakeModelGrouping sampleMakeModel = (MakeModelGrouping)makeModelGroupings.get( 0 );
		makeModel[0] = sampleMakeModel.getMake();
		makeModel[1] = sampleMakeModel.getModel();
	}
	else
	{
		makeModel[0] = make;
		makeModel[1] = model;

	}
	return makeModel;
}

public MakeModelGroupingService getMakeModelGroupingService()
{
	return makeModelGroupingService;
}

public void setMakeModelGroupingService( MakeModelGroupingService makeModelGroupingService )
{
	this.makeModelGroupingService = makeModelGroupingService;
}

public void setReportPersist( ReportPersist reportPersist )
{
	this.reportPersist = reportPersist;
}

public void setReportGroupingHelper(ReportGroupingHelper reportGroupingHelper) {
	this.reportGroupingHelper = reportGroupingHelper;
}
}
