package com.firstlook.action.components.cia;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.tiles.ComponentContext;

import biz.firstlook.transact.persist.model.GroupingDescription;
import biz.firstlook.transact.persist.persistence.IDealerPreferenceDAO;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.components.VestigialItemUtil;
import com.firstlook.action.dealer.reports.UsedCarPerformanceAnalyzerPlusDisplayHelper;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.form.GroupingForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.groupingdescription.GroupingDescriptionService;
import com.firstlook.session.FirstlookSession;

public class CIATrimDisplayAction extends SecureBaseAction
{

private IDealerPreferenceDAO dealerPrefDAO;
private UsedCarPerformanceAnalyzerPlusDisplayHelper usedCarPerformanceAnalyzerPlusDisplayHelper;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	ComponentContext context = ComponentContext.getContext( request );
	int ciaGroupingItemId = Integer.parseInt( (String)context.getAttribute( "ciaGroupingItemId" ) );
	int groupingDescriptionId = Integer.parseInt( (String)context.getAttribute( "groupingDescriptionId" ) );
	int weeks = Integer.parseInt( (String)context.getAttribute( "weeks" ) );
	int forecast = Integer.parseInt( (String)context.getAttribute( "forecast" ) );
	int mileageFilter = Integer.parseInt( (String)context.getAttribute( "mileageFilter" ) );
	int currentDealerId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();
	
	int mileageThreshold = usedCarPerformanceAnalyzerPlusDisplayHelper.retrieveMileageThreshold( mileageFilter, currentDealerId );

	GroupingDescriptionService gdService = new GroupingDescriptionService();
	GroupingDescription groupingDescription = gdService.retrieveById( new Integer( groupingDescriptionId ) );

	request.setAttribute( "groupingForm", new GroupingForm( groupingDescription ) );

	int lowerUnitCostThreshold = 0;
	usedCarPerformanceAnalyzerPlusDisplayHelper.putTrimItemsInRequest( request, getMemberFromRequest( request ).getProgramType(), weeks, forecast,

	                                  mileageThreshold, groupingDescriptionId, false, lowerUnitCostThreshold, ciaGroupingItemId );

	VestigialItemUtil viUtil = new VestigialItemUtil();
	viUtil.populateVestigialItemsBasedOnPreferences( request, "trim", false );
	putDealerFormInRequest( request, 0 );
	return mapping.findForward( "success" );
}

public IDealerPreferenceDAO getDealerPrefDAO()
{
    return dealerPrefDAO;
}
public void setDealerPrefDAO( IDealerPreferenceDAO dealerPrefDAO )
{
    this.dealerPrefDAO = dealerPrefDAO;
}

public void setUsedCarPerformanceAnalyzerPlusDisplayHelper(
		UsedCarPerformanceAnalyzerPlusDisplayHelper usedCarPerformanceAnalyzerPlusDisplayHelper) {
	this.usedCarPerformanceAnalyzerPlusDisplayHelper = usedCarPerformanceAnalyzerPlusDisplayHelper;
}
}
