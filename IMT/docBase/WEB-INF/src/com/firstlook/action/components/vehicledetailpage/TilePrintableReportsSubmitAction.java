package com.firstlook.action.components.vehicledetailpage;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.vehicle.description.old.GBException;
import biz.firstlook.module.vehicle.description.old.GuideBookInput;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.dealer.vehicledetailpage.PrintableVehicleDetailForm;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;
import com.firstlook.service.bookout.bean.StoredBookOutState;
import com.firstlook.service.bookout.inventory.InventoryBookOutService;
import com.firstlook.service.inventory.VehicleInventoryService;

public class TilePrintableReportsSubmitAction extends SecureBaseAction
{

private DealerPreferenceDAO dealerPrefDAO;
private InventoryBookOutService inventoryBookOutService;
private VehicleInventoryService vehicleInventoryService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	Dealer dealer = getImtDealerService().retrieveDealer( currentDealerId );
	DealerPreference dealerPref = dealer.getDealerPreference();
	PrintableVehicleDetailForm printableVehicleDetailForm = (PrintableVehicleDetailForm)form;

	String appraisalId = request.getParameter("appraisalId");
	
	Integer inventoryId = null;
	try {
		inventoryId = Integer.parseInt(request.getParameter("inventoryId"));
	} catch(NumberFormatException nfe) {
		//TODO: log it?
	}

	GuideBookInput input = new GuideBookInput();

	if (inventoryId != null){
		VehicleInventory vehicleInventory = vehicleInventoryService.retrieveVehicleInventory( currentDealerId, inventoryId );
		input.setVin( vehicleInventory.getVehicle().getVin() );		
	}else{
		input.setVin( request.getParameter("vin"));
	}
	
	input.setBusinessUnitId( getFirstlookSessionFromRequest( request ).getCurrentDealerId() );
	input.setState( dealer.getState() );
	input.setNadaRegionCode( dealerPref.getNadaRegionCode() );	
	input.setBusinessUnitId( getFirstlookSessionFromRequest( request ).getCurrentDealerId() );
	input.setState( dealer.getState() );
	input.setNadaRegionCode( dealerPref.getNadaRegionCode() );

	boolean bookValuesAvailable = false;
	StoredBookOutState state = null;
	try {
		if (inventoryId != null){
			state = getInventoryBookOutService().retrieveStoredBookOutInfo(input,new Integer( getGuideBookTypeId(printableVehicleDetailForm) ), inventoryId, dealerPref.getSearchInactiveInventoryDaysBackThreshold());
		}
	} catch ( GBException gbe)
	{
		throw new ApplicationException( gbe );
	}
	
	if (state != null) {
		// save this for BookOutPrintPageDisplayAction
		SessionHelper.setAttribute(request, "bookOutState", state);
		SessionHelper.keepAttribute( request, "bookOutState" );
		bookValuesAvailable = true;
	}
	
	if (appraisalId != null){
		bookValuesAvailable = true;
	}
	
	request.setAttribute( "showGalvesPrintPage", printableVehicleDetailForm.isGalvesReport() );
	request.setAttribute( "showNadaValues", printableVehicleDetailForm.isNadaValues() );
	request.setAttribute( "showNadaTradeValues", printableVehicleDetailForm.isNadaTradeValues() );
	request.setAttribute( "showNadaAppraisal", printableVehicleDetailForm.isNadaAppraisal() );
	request.setAttribute( "showNadaVehicleDescription", printableVehicleDetailForm.isNadaVehicleDescription() );
	request.setAttribute( "showNadaLoanSummary", printableVehicleDetailForm.isNadaLoanSummary() );
	request.setAttribute( "showNadaWholesale", printableVehicleDetailForm.isNadaWholesale() );
	request.setAttribute( "showBlackBookPrintPage", printableVehicleDetailForm.isBlackBookReport() );
	request.setAttribute( "showKelleyRetailBreakdown", printableVehicleDetailForm.isKelleyRetailBreakdown() );
	request.setAttribute( "showKelleyWholesaleBreakdown", printableVehicleDetailForm.isKelleyWholesaleBreakdown() );
	request.setAttribute( "showKelleyEquityBreakdown", printableVehicleDetailForm.isKelleyEquityBreakdown() );
	request.setAttribute( "showKelleyWholesaleRetail", printableVehicleDetailForm.isKelleyWholesaleRetail() );
	request.setAttribute( "showKelleyTradeInReport", printableVehicleDetailForm.isKelleyTradeInReport() );
	request.setAttribute( "showViewDeals", printableVehicleDetailForm.isViewDealsReport());
	request.setAttribute( "showFirstLookAppraisalReport", printableVehicleDetailForm.isFirstlookBookOutReport() );
	request.setAttribute( "showKelleyWindowSticker",  printableVehicleDetailForm.isWindowStickerReport() );
	request.setAttribute( "showNAAAAuctionReport",printableVehicleDetailForm.isAuctionReport() );
	request.setAttribute( "showFirstLookPrintPage",printableVehicleDetailForm.isFirstlookPrintPage());
	request.setAttribute( "inventoryId", inventoryId );
	request.setAttribute( "bookValuesAvailable", bookValuesAvailable);
	request.setAttribute( "currentYear", Calendar.getInstance().get(Calendar.YEAR) );
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );
	return mapping.findForward( "success" );
}

/**
 * Gets the GBid based on the form. Returns -1 if nether KBB, BB or NADA is specified. 
 * 
 * @param form
 * @return
 */
private int getGuideBookTypeId(PrintableVehicleDetailForm form) {
	int guideBookId = -1;
	if (form.isBlackBookReport()) {
		guideBookId = ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE;
	} else if (form.isKelleyRetailBreakdown() || form.isKelleyEquityBreakdown() || form.isKelleyWholesaleRetail() ||
			form.isKelleyWholesaleBreakdown() || form.isKelleyTradeInReport()) {
		guideBookId = ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE;
	}else if (form.isNadaValues() || form.isNadaTradeValues() || form.isNadaAppraisal() ||
			form.isNadaVehicleDescription() || form.isNadaLoanSummary() || form.isNadaWholesale()) {
		guideBookId = ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE;
	}else if (form.isGalvesReport()) {
		guideBookId = ThirdPartyDataProvider.GUIDEBOOK_GALVES_TYPE;
	}
	return guideBookId;
}

public DealerPreferenceDAO getDealerPrefDAO()
{
	return dealerPrefDAO;
}

public void setDealerPrefDAO( DealerPreferenceDAO dealerPrefDAO )
{
	this.dealerPrefDAO = dealerPrefDAO;
}

public InventoryBookOutService getInventoryBookOutService()
{
	return inventoryBookOutService;
}

public void setInventoryBookOutService( InventoryBookOutService inventoryBookOutService )
{
	this.inventoryBookOutService = inventoryBookOutService;
}

public VehicleInventoryService getVehicleInventoryService()
{
	return vehicleInventoryService;
}

public void setVehicleInventoryService( VehicleInventoryService vehicleInventoryService )
{
	this.vehicleInventoryService = vehicleInventoryService;
}

}
