package com.firstlook.action.components;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.services.insights.Insight;
import biz.firstlook.commons.services.insights.InsightParameters;
import biz.firstlook.commons.services.insights.InsightPresentationAdapter;
import biz.firstlook.commons.services.insights.InsightService;
import biz.firstlook.commons.services.insights.InsightServiceException;
import biz.firstlook.commons.services.vehicleCatalog.IVehicleCatalogService;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogEntry;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogServiceException;

import com.firstlook.action.SecureTileAction;
import com.firstlook.action.redirection.JdPowerRedirectionAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.form.GroupingForm;
import com.firstlook.entity.form.TradeAnalyzerForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.SessionHelper;

public class PerformanceAnalysisDisplayAction extends SecureTileAction
{

protected PerformanceSummaryHelperService performanceSummaryHelperService;
protected InsightService insightService;
protected IVehicleCatalogService vehicleCatalogService;

public PerformanceAnalysisDisplayAction()
{
	super();
}

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	TradeAnalyzerForm tradeAnalyzerForm = (TradeAnalyzerForm)form;
	if ( tradeAnalyzerForm == null || tradeAnalyzerForm.getVin() == null)
	{
		tradeAnalyzerForm = (TradeAnalyzerForm)SessionHelper.getAttribute( request, "tradeAnalyzerForm" );
	}
	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	int includeDealerGroup;
	try
	{
		includeDealerGroup = RequestHelper.getInt( request, "includeDealerGroup" );
	}
	catch ( Exception e )
	{
		includeDealerGroup = 0;
	}

	tradeAnalyzerForm.setIncludeDealerGroup( includeDealerGroup );
	putDealerFormInRequest( request, 0 );

	int weeks = RequestHelper.getInt( request, "weeks" );

	getPerformanceSummaryHelperService().putGroupingDescriptionFormInRequest( request, tradeAnalyzerForm.getGroupingDescriptionId() );

	getPerformanceSummaryHelperService().setReportGroupingsOnRequest( request, tradeAnalyzerForm, weeks );

	List<Insight> insights = Collections.emptyList();
	try {
		InsightParameters parameters = new InsightParameters(
				currentDealerId,
				tradeAnalyzerForm.getGroupingDescriptionId(),
				2,
				weeks,
				tradeAnalyzerForm.getTrim(),
				tradeAnalyzerForm.getYear(),
				tradeAnalyzerForm.getMileage());
		insights = getInsightService().getInsights(parameters);	
	}
	catch (InsightServiceException ise) {
		logger.error("Failed to get Insights", ise);
	}
	
	InsightPresentationAdapter insightDisplay = new InsightPresentationAdapter(insights);
	
	request.setAttribute("storeInsights",insightDisplay.getStoreInsights().size() );
	request.setAttribute("insightPresentationAdapter", insightDisplay);
	request.setAttribute("showAnnualRoi", getImtDealerService().showLithiaRoi(currentDealerId));
	double annualROI = Integer.valueOf(insightDisplay.getAnnualROI()).doubleValue();
	request.setAttribute("annualRoi", annualROI == 0.0d ? 0.0d : annualROI / 100.0d);
	request.setAttribute("showMarketShare", getFirstlookSessionFromRequest(request).isIncludeMarket());
	
	request.setAttribute("groupingDescription", ((GroupingForm) request.getAttribute("groupingDescriptionForm")).getDescription());
	
	request.setAttribute( "weeks", new Integer( weeks ) );
	request.setAttribute( "mileageMax", new Integer( Integer.MAX_VALUE ) );
	request.setAttribute( "tradeAnalyzerForm", tradeAnalyzerForm );
	SessionHelper.setAttribute( request, "tradeAnalyzerForm", tradeAnalyzerForm );
	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
	
	try
	{
		VehicleCatalogEntry vce = vehicleCatalogService.retrieveVehicleCatalogEntry( tradeAnalyzerForm.getVin(), tradeAnalyzerForm.getCatalogKeyId() );
		
		request.setAttribute("showJDPower", getFirstlookSessionFromRequest(request).isIncludeJDPowerUsedCarMarketData());
		request.setAttribute("JDPowerUrl", JdPowerRedirectionAction.getRelativeUrl( vce.getCatalogKey().getVehicleCatalogId(), vce.getModelYear()) );
	}
	catch ( VehicleCatalogServiceException e )
	{
		logger.warn( MessageFormat.format( "Could not find vehicle catalogid for vin: {0}, disabling JDPowerMarketAnalyzer.", tradeAnalyzerForm.getVin()));
	}
	return mapping.findForward( "success" );
}

public InsightService getInsightService() {
	return insightService;
}

public void setInsightService(InsightService insightService) {
	this.insightService = insightService;
}

public PerformanceSummaryHelperService getPerformanceSummaryHelperService()
{
	return performanceSummaryHelperService;
}

public void setPerformanceSummaryHelperService( PerformanceSummaryHelperService performanceSummaryHelperService )
{
	this.performanceSummaryHelperService = performanceSummaryHelperService;
}

public IVehicleCatalogService getVehicleCatalogService()
{
	return vehicleCatalogService;
}

public void setVehicleCatalogService( IVehicleCatalogService vehicleCatalogService )
{
	this.vehicleCatalogService = vehicleCatalogService;
}

}
