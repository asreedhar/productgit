package com.firstlook.action.components;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.tiles.ComponentContext;

import biz.firstlook.commons.services.insights.Insight;
import biz.firstlook.commons.services.insights.InsightParameters;
import biz.firstlook.commons.services.insights.InsightUtils;
import biz.firstlook.commons.services.insights.PerformanceAnalysisBean;

import com.firstlook.action.SecureTileAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.ReportGroupingHelper;
import com.firstlook.persistence.inventory.ROIRetriever;
import com.firstlook.report.ReportGrouping;
import com.firstlook.service.inventory.InventoryService_Legacy;
import com.firstlook.session.FirstlookSession;

public class PerformanceAnalysisLightsDisplayAction extends SecureTileAction
{

private InventoryService_Legacy inventoryService_Legacy;
private ROIRetriever annualRoiDAO;
private ReportGroupingHelper reportGroupingHelper;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	ComponentContext context = ComponentContext.getContext( request );

	int mileage = Integer.parseInt( (String)context.getAttribute( "mileage" ) );
	int weeks = Integer.parseInt( (String)context.getAttribute( "weeks" ) );
	String year = (String)context.getAttribute( "year" );
	String make = (String)context.getAttribute( "make" );
	String model = (String)context.getAttribute( "model" );
	String vin = (String)context.getAttribute( "vin" );
	String groupingDescription = (String)context.getAttribute( "groupingDescription" );

	InventoryEntity inventory = inventoryService_Legacy.createEmptyInventoryWithYearMileageMakeModelUsingVin( year, mileage, vin, make, model );

	ReportGrouping generalReportGrouping = reportGroupingHelper.findByMakeModelOrTrimOrBodyStyle(
																									make,
																									model,
																									null,
																									0,
																									( (FirstlookSession)request.getSession().getAttribute(
																																							FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId(),
																									weeks, InventoryEntity.USED_CAR );

	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	
	int groupingDescriptionId = generalReportGrouping.getGroupingId();
	// KL - copy of the code that was in the original method being called. Not
	// sure why this logic is needed.
	if ( groupingDescriptionId == 0 )
	{
		groupingDescriptionId = inventory.getMakeModelGrouping().getGroupingDescription().getGroupingDescriptionId().intValue();
	}

	List<Insight> insights = InsightUtils.getInsights(new InsightParameters(currentDealerId, groupingDescriptionId, 2, weeks, inventory.getTrim(), inventory.getVehicleYear(), inventory.getMileageReceived()));
	
	Double annualROI = null;
	if (getImtDealerService().showLithiaRoi(currentDealerId)) {
		if (InsightUtils.containsAnnualROI(insights)) {
			annualROI = new Double(InsightUtils.getAnnualROI(insights).getInsightValue());
			if (annualROI.doubleValue() != 0.0d) {
				annualROI = new Double(annualROI.doubleValue() / 100.0d);
			}
		}
		else {
			/* fallback code: should never be hit */
			annualROI = new Double(getAnnualRoiDAO().getMakeModelYearROI(
					new Integer(currentDealerId),
					groupingDescriptionId,
					Integer.parseInt(year)).doubleValue());
		}
	}
	
	if (annualROI == null) {
		request.setAttribute("showAnnualRoi", Boolean.FALSE);
	}
	else {
		request.setAttribute( "annualRoi", annualROI);
		request.setAttribute( "showAnnualRoi", Boolean.TRUE );
	}
	
	request.setAttribute( "performanceAnalysisItem", new PerformanceAnalysisBean(insights) );
	request.setAttribute( "groupingDescription", groupingDescription );

	return mapping.findForward( "success" );
}

public void setInventoryService_Legacy( InventoryService_Legacy inventoryService )
{
	this.inventoryService_Legacy = inventoryService;
}

public ROIRetriever getAnnualRoiDAO()
{
	return annualRoiDAO;
}

public void setAnnualRoiDAO( ROIRetriever annualRoiDAO )
{
	this.annualRoiDAO = annualRoiDAO;
}

public void setReportGroupingHelper(ReportGroupingHelper reportGroupingHelper) {
	this.reportGroupingHelper = reportGroupingHelper;
}

}