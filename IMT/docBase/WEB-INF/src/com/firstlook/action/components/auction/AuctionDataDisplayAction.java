package com.firstlook.action.components.auction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.entity.AuctionData;
import biz.firstlook.service.IAuctionDataService;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureTileAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.form.AuctionDataForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.SessionHelper;
import com.firstlook.service.vehicle.IVehicleService;

public class AuctionDataDisplayAction extends SecureTileAction
{

private IAppraisalService appraisalService;
private IAuctionDataService auctionDataService;
private IVehicleService vehicleService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	boolean isCriteriaPresent = false;
	boolean isAppraisal = RequestHelper.getBoolean( request, "isAppraisal" );
	AuctionDataForm auctionDataForm = (AuctionDataForm)form;
	String vin = request.getParameter( "vin" );
	
	if ( isAppraisal )
	{
		int appraisalId = RequestHelper.getInt( request, "appraisalId" );
		IAppraisal appraisal = appraisalService.findBy( appraisalId );
		if ( appraisal != null )
		{
			auctionDataForm.setMileage( appraisal.getMileage() );
		}

	}
	if ( !StringUtils.isBlank( auctionDataForm.getVic() ) ) {
		isCriteriaPresent = true;
	} 
	else if( !StringUtils.isBlank( request.getParameter("vic") ) ) {
		isCriteriaPresent = true;
		auctionDataForm.setVic( request.getParameter("vic") );
	}

	if ( auctionDataForm.getAreaId() == 0 )
	{
		if ( !StringUtils.isBlank( request.getParameter( "areaId" ) ))
		{
			auctionDataForm.setAreaId( RequestHelper.getInt( request, "areaId" ) );
		}
	}
	if ( auctionDataForm.getTimePeriodId() == 0 )
	{
		if ( !StringUtils.isBlank( request.getParameter( "periodId" ) ))
		{
			auctionDataForm.setTimePeriodId( RequestHelper.getInt( request, "periodId" ) );
		}
	}
	if ( auctionDataForm.getModelYear() == 0 )
	{
		if ( !StringUtils.isBlank( request.getParameter( "modelYear" ) ))
		{
			auctionDataForm.setModelYear( RequestHelper.getInt( request, "modelYear" ) );
		}
	}
	
	if ( isCriteriaPresent )
	{
		AuctionData data = auctionDataService.retrieveAuctionData( auctionDataForm.getVic(),
																auctionDataForm.getAreaId(),
																auctionDataForm.getTimePeriodId(),
																auctionDataForm.getMileage(),
																auctionDataForm.getUsingVIC(),
																auctionDataForm.getModelYear() );
		request.setAttribute( "auctionData", data );
	}

	// save VIC onto vehicle
	Vehicle vehicle = vehicleService.loadVehicle( vin );
	if ( vehicle != null && 
		 (auctionDataForm.getVic() != null && !auctionDataForm.getVic().equals( vehicle.getVic()  ) &&
		 auctionDataForm.getUsingVIC() ) // don't want to save MMG id onto vehicle
		 && !"0".equals( auctionDataForm.getVic() ) )
	{
		vehicle.setVic( auctionDataForm.getVic() );
		vehicleService.updateVehicle( vehicle );
	}
	
	// reset the form cuz somebody is storing it.
	auctionDataForm.setAreaId( 0 );
	auctionDataForm.setVic( "" );
	auctionDataForm.setTimePeriodId( 0 );

	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );

	request.setAttribute( "isCriteriaPresent", Boolean.valueOf( isCriteriaPresent ) );
	return mapping.findForward( "success" );
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

public void setAuctionDataService( IAuctionDataService auctionDataService )
{
	this.auctionDataService = auctionDataService;
}

public void setVehicleService( IVehicleService vehicleService )
{
	this.vehicleService = vehicleService;
}

}
