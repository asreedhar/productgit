package com.firstlook.action.components.cia;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.tiles.ComponentContext;

import biz.firstlook.cia.model.CIACategory;
import biz.firstlook.cia.model.CIAGroupingItem;
import biz.firstlook.cia.persistence.ICIAGroupingItemDAO;
import biz.firstlook.commons.services.vehicleCatalog.IVehicleCatalogService;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogServiceException;
import biz.firstlook.commons.util.ColorUtility;
import biz.firstlook.transact.persist.service.MakeModelGroupingService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.cia.report.TrimFormattingUtility;

public class CIAPreferencesDisplayAction extends SecureBaseAction
{

private static final Logger logger = Logger.getLogger( CIAPreferencesDisplayAction.class );
	
private ICIAGroupingItemDAO ciaGroupingItemDAO;
private IVehicleCatalogService vehicleCatalogService;
private MakeModelGroupingService makeModelGroupingService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	ComponentContext context = ComponentContext.getContext( request );

	int groupingDescriptionId = Integer.parseInt( (String)context.getAttribute( "groupingDescriptionId" ) );

	TrimFormattingUtility trimService = new TrimFormattingUtility();
	trimService.setVehicleCatalogService( vehicleCatalogService );
	trimService.setMakeModelGroupingService( makeModelGroupingService );
	List trimsFormatted = trimService.retrieveAllTrimsJSEscaped( groupingDescriptionId );
	request.setAttribute( "trims", trimsFormatted );

	// Show a years with checkboxes table for Manager's Choice and not the other categories
	Integer ciaCategoryId = new Integer( request.getParameter( "ciaCategory" ) );
	String showYearsPrefTable = getShowYearsPrefTableFlag(ciaCategoryId);
	request.setAttribute( "showYearsPrefTable", showYearsPrefTable );
	if(showYearsPrefTable.equals("true"))
	{
		try {
			List<String> years = vehicleCatalogService.retrieveModelYears( groupingDescriptionId, true );
			request.setAttribute( "years", years );
		} catch (VehicleCatalogServiceException e) {
			// error
			logger.error("Returning empty years list after error retreiving years from vehicle catalog.");
			request.setAttribute( "years", new ArrayList<String>());
		}
	}
	
	request.setAttribute( "colors", ColorUtility.retrieveStandardColors() );

	int ciaGroupingItemId = Integer.parseInt( (String)context.getAttribute( "ciaGroupingItemId" ) );

	CIAGroupingItem ciaGroupingItem = getCiaGroupingItemDAO().findNonLazyByCIAGroupingItemId( new Integer( ciaGroupingItemId ) );
	String notes = "";
	if ( ciaGroupingItem != null )
	{
		notes = ciaGroupingItem.getNotes();
	}

		
	
	request.setAttribute( "ciaGroupingItemId", new Integer( ciaGroupingItemId ) );
	request.setAttribute( "notes", notes );

	return mapping.findForward( "success" );
}

// Show the years with checkboxes table
String getShowYearsPrefTableFlag(Integer ciaCategoryId)
{
	if(ciaCategoryId != null)
	{
		if(ciaCategoryId.intValue() == CIACategory.MANAGERSCHOICE_CATEGORY)
			return "true";
		else
			return "false";
	}
	else
	{
		return null;
	}
}

public ICIAGroupingItemDAO getCiaGroupingItemDAO()
{
	return ciaGroupingItemDAO;
}

public void setCiaGroupingItemDAO( ICIAGroupingItemDAO ciaGroupingItemDAO )
{
	this.ciaGroupingItemDAO = ciaGroupingItemDAO;
}

public void setVehicleCatalogService( IVehicleCatalogService vehicleCatalogService )
{
	this.vehicleCatalogService = vehicleCatalogService;
}

public void setMakeModelGroupingService( MakeModelGroupingService makeModelGroupingService )
{
	this.makeModelGroupingService = makeModelGroupingService;
}

}
