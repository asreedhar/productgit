package com.firstlook.action.components;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.tiles.ComponentContext;

import biz.firstlook.commons.services.insights.Insight;
import biz.firstlook.commons.services.insights.InsightParameters;
import biz.firstlook.commons.services.insights.InsightUtils;
import biz.firstlook.commons.services.insights.PerformanceAnalysisBean;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;

public class RiskLevelDisplayAction extends SecureBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    ComponentContext context = ComponentContext.getContext( request );

    List<Insight> insights = InsightUtils.getInsights(new InsightParameters(
    		getFirstlookSessionFromRequest( request ).getCurrentDealerId(),
    		Integer.parseInt((String) context.getAttribute("groupingDescriptionId")),
    		2,
    		Integer.parseInt((String) context.getAttribute("weeks")),
    		null,
    		null,
    		Integer.parseInt((String) context.getAttribute("mileage"))));

	request.setAttribute( "performanceAnalysisItem", new PerformanceAnalysisBean(insights) );

    return mapping.findForward( "success" );
}

}
