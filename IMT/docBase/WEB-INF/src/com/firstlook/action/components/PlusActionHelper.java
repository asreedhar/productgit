package com.firstlook.action.components;

import java.util.List;

import biz.firstlook.transact.persist.model.BodyType;
import biz.firstlook.transact.persist.service.vehicle.BodyTypeDAO;

import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.ReportGroupingHelper;
import com.firstlook.report.Report;
import com.firstlook.report.ReportGrouping;
import com.firstlook.service.inventory.InventoryService_Legacy;

public class PlusActionHelper
{

private BodyTypeDAO bodyTypeDAO;
private ReportGroupingHelper reportGroupingHelper;

public PlusActionHelper()
{
}

public ReportGrouping retrieveReportGroupingNewCar( int dealerId, String make,
        String model, String trim, int bodyTypeId, int weeks, int forecast,
        int mileage, int inventoryType ) throws ApplicationException
{

    BodyType bodyType = bodyTypeDAO.findByPk( bodyTypeId);

    Report report = new Report();
    List reportGroupings = reportGroupingHelper.findByDealerIdAndWeeks(
            dealerId, weeks, forecast, mileage, inventoryType, true);
    report.setReportGroupings(reportGroupings);

    String groupingDescription = createReportGroupingDescription(make, model,
            trim, bodyType.getBodyType());

    ReportGrouping reportGrouping = report
            .getReportGrouping(groupingDescription);

    return reportGrouping;
}

public String createReportGroupingDescription( String make, String model,
        String vehicleTrim, String bodyType )
{
    return InventoryService_Legacy.createGroupingString(make, model, vehicleTrim,
            bodyType);
}

public void setBodyTypeDAO( BodyTypeDAO bodyTypeDAO )
{
	this.bodyTypeDAO = bodyTypeDAO;
}

public void setReportGroupingHelper(ReportGroupingHelper reportGroupingHelper) {
	this.reportGroupingHelper = reportGroupingHelper;
}

}
