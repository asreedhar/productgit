package com.firstlook.action.components;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.cia.service.BasisPeriodService;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.CIAPreferences.TimePeriod;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;
import biz.firstlook.transact.persist.retriever.BookVsUnitCostSummaryDisplayBean;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.InventorySalesAggregate;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.inventory.InventorySalesAggregateService;
import com.firstlook.service.inventory.InventorySalesAggregateHelper;
import com.firstlook.service.inventory.InventoryService_Legacy;

public class InventorySummaryDisplayAction extends SecureBaseAction
{

private DealerPreferenceDAO dealerPrefDAO;
private InventoryService_Legacy inventoryService_Legacy;
private BasisPeriodService basisPeriodService;
private InventorySalesAggregateService inventorySalesAggregateService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	int weeks = basisPeriodService.calculateNumberOfWeeksInBasisPeriodByType( new Integer( currentDealerId ),
																				TimePeriod.SALES_HISTORY_DISPLAY, 0 );

	int inventoryType = getMemberFromRequest( request ).getInventoryType().getValue();

	InventorySalesAggregate aggregate = inventorySalesAggregateService.retrieveCachedInventorySalesAggregate( currentDealerId, weeks, 0,
																												inventoryType );

	InventorySalesAggregateHelper.putMakeModelCountInRequest( request, aggregate );
	InventorySalesAggregateHelper.putTotalDaysSupplyInRequest( request, aggregate );
	InventorySalesAggregateHelper.putDealerTotalDollarsInRequest( request, aggregate );
	InventorySalesAggregateHelper.putVehicleCountInRequest( request, aggregate );
	InventorySalesAggregateHelper.putAverageInventoryAgeInRequest( request, aggregate );

	putBookvsCostInRequest( request );

	return mapping.findForward( "success" );
}

private void putBookvsCostInRequest( HttpServletRequest request ) throws ApplicationException
{
	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	DealerPreference dealerPreference = getDealerPrefDAO().findByBusinessUnitId(currentDealerId);

	BookVsUnitCostSummaryDisplayBean bookValues = inventoryService_Legacy.findBookVsUnitCostSummary( currentDealerId, dealerPreference.getBookOutPreferenceId() );
	if(bookValues != null)
		request.setAttribute( "bookVsUnitCostTotal", bookValues.getBookVsCost() );

}

public DealerPreferenceDAO getDealerPrefDAO()
{
	return dealerPrefDAO;
}

public void setDealerPrefDAO( DealerPreferenceDAO dealerPrefDAO )
{
	this.dealerPrefDAO = dealerPrefDAO;
}

public void setInventoryService_Legacy( InventoryService_Legacy inventoryService )
{
	this.inventoryService_Legacy = inventoryService;
}

public BasisPeriodService getBasisPeriodService()
{
	return basisPeriodService;
}

public void setBasisPeriodService( BasisPeriodService basisPeriodService )
{
	this.basisPeriodService = basisPeriodService;
}

public InventorySalesAggregateService getInventorySalesAggregateService()
{
    return inventorySalesAggregateService;
}

public void setInventorySalesAggregateService( InventorySalesAggregateService inventorySalesAggregateService )
{
    this.inventorySalesAggregateService = inventorySalesAggregateService;
}
}
