package com.firstlook.action.components;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;
import biz.firstlook.transact.persist.retriever.BookVsUnitCostSummaryDisplayBean;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.GuideBook;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.inventory.InventoryService_Legacy;

public class GuideBookSummaryDisplayAction extends SecureBaseAction
{

private DealerPreferenceDAO dealerPrefDAO;
private InventoryService_Legacy inventoryService_Legacy;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	DealerPreference dealerPreference = dealerPrefDAO.findByBusinessUnitId( new Integer( currentDealerId ) );

	request.setAttribute( "guideBookName", ThirdPartyDataProvider.getThirdPartyDataProviderDescription( dealerPreference.getGuideBookIdAsInt() ) );

	List<BookVsUnitCostSummaryDisplayBean> bookVsUnitCostSummaryDisplayBean = inventoryService_Legacy.findBookVsUnitCostSummary( currentDealerId );

	if ( !bookVsUnitCostSummaryDisplayBean.isEmpty() )
	{
		Iterator<BookVsUnitCostSummaryDisplayBean> bookVsUnitCostSummaryDisplayBeanIter = bookVsUnitCostSummaryDisplayBean.iterator();
		while ( bookVsUnitCostSummaryDisplayBeanIter.hasNext() )
		{
			BookVsUnitCostSummaryDisplayBean bookVsCostBean = bookVsUnitCostSummaryDisplayBeanIter.next();
			if ( bookVsCostBean.getThirdPartyCategoryId().intValue() == dealerPreference.getBookOutPreferenceId() )
			{
				request.setAttribute( "bookOut", new Boolean( dealerPreference.isBookOut() ) );
				request.setAttribute( "bookVsUnitCostTotal", bookVsCostBean.getBookVsCost() );
				request.setAttribute( "bookTotal", bookVsCostBean.getTotalInventoryBookCost() );
				request.setAttribute( "bookUnitCost", bookVsCostBean.getTotalInventoryUnitCost() );
				request.setAttribute( "bookOutPreference", bookVsCostBean.getThirdPartyCategoryDescription() );
			}
			else if ( bookVsCostBean.getThirdPartyCategoryId().intValue() == dealerPreference.getBookOutPreferenceSecondIdAsInt() )
			{
				request.setAttribute( "bookVsUnitCostTotalSecond", bookVsCostBean.getBookVsCost() );
				request.setAttribute( "bookTotalSecond", bookVsCostBean.getTotalInventoryBookCost() );
				request.setAttribute( "bookUnitCostSecond", bookVsCostBean.getTotalInventoryUnitCost() );
				request.setAttribute( "bookOutPreferenceSecond", bookVsCostBean.getThirdPartyCategoryDescription() );
			}
		}

		// set fla to show second book Values
		if ( dealerPreference.getBookOutPreferenceSecondIdAsInt() != GuideBook.NO_GUIDEBOOK_SELECTED )
		{
			request.setAttribute( "secondBookOut", Boolean.TRUE );
		}
		else
		{
			request.setAttribute( "secondBookOut", Boolean.FALSE );
		}

	}
	else
	{
		// Want to show the page with an 'error result' if no bookout
		// summary
		request.setAttribute( "bookOut", new Boolean( true ) );
		request.setAttribute( "bookVsUnitCostTotal", new Integer( 0 ) );
		request.setAttribute( "bookTotal", new Integer( 0 ) );
		request.setAttribute( "bookUnitCost", new Integer( 0 ) );
		request.setAttribute( "bookOutPreference", "No Bookout Summary Found" );

		// dont show secondGuideBookValues since we dont have any
		request.setAttribute( "secondBookOut", Boolean.FALSE );
	}

	return mapping.findForward( "success" );
}

public void setInventoryService_Legacy( InventoryService_Legacy inventoryService )
{
	this.inventoryService_Legacy = inventoryService;
}

public void setDealerPrefDAO( DealerPreferenceDAO dealerPrefDao )
{
	this.dealerPrefDAO = dealerPrefDao;
}

}