package com.firstlook.action.components;

import javax.servlet.http.HttpServletRequest;

import biz.firstlook.transact.persist.model.GroupingDescription;
import biz.firstlook.transact.persist.model.MakeModelGrouping;
import biz.firstlook.transact.persist.model.Segment;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.persistence.SegmentDAO;
import biz.firstlook.transact.persist.service.MakeModelGroupingService;

import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.form.GroupingForm;
import com.firstlook.entity.form.TradeAnalyzerForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.ReportGroupingHelper;
import com.firstlook.persistence.inventory.FindMarketShareGroupingRetriever;
import com.firstlook.persistence.inventory.MarketShareReturnObject;
import com.firstlook.report.ReportGrouping;
import com.firstlook.service.groupingdescription.GroupingDescriptionService;
import com.firstlook.service.vehicle.IVehicleService;
import com.firstlook.session.FirstlookSession;

public class PerformanceSummaryHelperService
{

private SegmentDAO segmentDAO;
private IVehicleService vehicleService;
private MakeModelGroupingService makeModelGroupingService;
private ReportGroupingHelper reportGroupingHelper;
private FindMarketShareGroupingRetriever findMarketShareGroupingRetriever;

public void setReportGroupingsOnRequest( HttpServletRequest request, TradeAnalyzerForm tradeAnalyzerForm, int weeks ) throws ApplicationException
{
	setReportGroupingsOnRequestGeneral(request, tradeAnalyzerForm, weeks );
	setReportGroupingsOnRequestSpecific(request, tradeAnalyzerForm, weeks );
}
public void setReportGroupingsOnRequestGeneral ( HttpServletRequest request, TradeAnalyzerForm tradeAnalyzerForm, int weeks ) throws ApplicationException
{
	ReportGrouping generalReportGrouping = reportGroupingHelper.findByMakeModelOrTrimOrBodyStyle(
			tradeAnalyzerForm.getMake(),
			tradeAnalyzerForm.getModel(),
			null,
			tradeAnalyzerForm.getIncludeDealerGroup(),
			( (FirstlookSession)request.getSession().getAttribute(
					FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId(),
					weeks, InventoryEntity.USED_CAR );
	request.setAttribute( "generalReport", generalReportGrouping );
}

public void setReportGroupingsOnRequestSpecific( HttpServletRequest request, TradeAnalyzerForm tradeAnalyzerForm, int weeks ) throws ApplicationException
{
	ReportGrouping specificReportGrouping = reportGroupingHelper.findByMakeModelOrTrimOrBodyStyle(
			tradeAnalyzerForm.getMake(),
			tradeAnalyzerForm.getModel(),
			tradeAnalyzerForm.getTrim(),
			tradeAnalyzerForm.getIncludeDealerGroup(),
			( (FirstlookSession)request.getSession().getAttribute(
					FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId(),
					weeks, InventoryEntity.USED_CAR );
	request.setAttribute( "specificReport", specificReportGrouping );
}

public void determineMarketShare( boolean isIncludeMarket, String vin, HttpServletRequest request, int groupingId, Integer dealerId )
{
	Vehicle vehicle = getVehicleService().loadVehicle( vin );
	if ( isIncludeMarket )
	{
		if ( vehicle != null )
		{
			if ( vehicle.getMakeModelGrouping() == null )
			{
				MakeModelGrouping mmg = getMakeModelGroupingService().retrieveByPk( vehicle.getMakeModelGroupingId() );
				if ( mmg != null )
				{
					putMarketShareInRequest( request, dealerId, mmg.getSegmentId(), groupingId );
				}
			}
			else
			{
				putMarketShareInRequest( request, dealerId, vehicle.getMakeModelGrouping().getSegmentId(), groupingId );
			}
		}
	}
}

public void putGroupingDescriptionFormInRequest( HttpServletRequest request, Integer groupingDescriptionId ) throws ApplicationException
{
	GroupingDescriptionService groupingDescriptionService = new GroupingDescriptionService();
	GroupingDescription groupingDescription = groupingDescriptionService.retrieveById( groupingDescriptionId );
	request.setAttribute( "groupingDescriptionForm", new GroupingForm( groupingDescription ) );
}

public void putMarketShareInRequest( HttpServletRequest request, Integer dealerId, int segmentId, int groupingId )
{
	MarketShareReturnObject marketShare = findMarketShareGroupingRetriever.findByGroupingDescription( dealerId, new Integer( segmentId ),
																							new Integer( groupingId ) );

	if ( marketShare != null && marketShare.getMarketShare() != null )
	{
		request.setAttribute( "marketShare", new Float( 100.0f * marketShare.getMarketShare().floatValue() ) );
		Segment dbt = getSegmentDAO().findById( new Integer( segmentId ) );
		request.setAttribute( "segment", dbt.getSegment() );
	}
}

public SegmentDAO getSegmentDAO()
{
	return segmentDAO;
}

public void setSegmentDAO( SegmentDAO segmentDAO )
{
	this.segmentDAO = segmentDAO;
}

public MakeModelGroupingService getMakeModelGroupingService()
{
	return makeModelGroupingService;
}

public void setMakeModelGroupingService( MakeModelGroupingService makeModelGroupingService )
{
	this.makeModelGroupingService = makeModelGroupingService;
}

public IVehicleService getVehicleService()
{
	return vehicleService;
}

public void setVehicleService( IVehicleService vehicleService )
{
	this.vehicleService = vehicleService;
}


public void setReportGroupingHelper(ReportGroupingHelper reportGroupingHelper) {
	this.reportGroupingHelper = reportGroupingHelper;
}
public void setFindMarketShareGroupingRetriever(
		FindMarketShareGroupingRetriever findMarketShareGroupingRetriever) {
	this.findMarketShareGroupingRetriever = findMarketShareGroupingRetriever;
}


}
