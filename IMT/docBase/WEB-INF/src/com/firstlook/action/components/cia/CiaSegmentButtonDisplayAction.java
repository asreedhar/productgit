package com.firstlook.action.components.cia;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.cia.model.CIASummary;
import biz.firstlook.cia.model.Status;
import biz.firstlook.cia.persistence.ICIASummaryDAO;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.cia.CIAButtonService;

public class CiaSegmentButtonDisplayAction extends SecureBaseAction
{

private ICIASummaryDAO ciaSummaryDAO;
private CIAButtonService ciaButtonService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
    
    CIASummary ciaSummary = getCiaSummaryDAO().retrieveCIASummary( currentDealerId, Status.CURRENT );
    if(ciaSummary != null) {
	    List ciaBodyTypeDetailButtons = ciaButtonService.retrieveBodyTypeDetailButtons( currentDealerId, ciaSummary.getCiaSummaryId().intValue() );
	
	    request.setAttribute( "ciaBodyTypeDetails", ciaBodyTypeDetailButtons );
	
	    Dealer dealer = getImtDealerService().retrieveDealer( currentDealerId );
	    Boolean includeBackEndGrossProfit = dealer.getDealerPreference().getIncludeBackEndInValuation();
	    request.setAttribute( "includeBackEndInValuation", includeBackEndGrossProfit );
    }

    return mapping.findForward( "success" );
}

public ICIASummaryDAO getCiaSummaryDAO()
{
	return ciaSummaryDAO;
}

public void setCiaSummaryDAO( ICIASummaryDAO ciaSummaryDAO )
{
	this.ciaSummaryDAO = ciaSummaryDAO;
}

public void setCiaButtonService(CIAButtonService ciaButtonService) {
	this.ciaButtonService = ciaButtonService;
}

}
