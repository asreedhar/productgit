package com.firstlook.action.components;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

public class TestVestigialItemUtil extends TestCase
{

public TestVestigialItemUtil( String name )
{
    super(name);
}

public void testFilterDistinct()
{
    Map entryMap = new HashMap();
    entryMap.put("2_year_2000_prefer", new Object());
    entryMap.put("2_year_2000_avoid", new Object());
    entryMap.put("2_year_2001_prefer", new Object());
    entryMap.put("2_trim_2003_prefer", new Object());

    VestigialItemUtil util = new VestigialItemUtil();
    List yearList = util.filterDistinct(entryMap, "year");

    assertEquals(2, yearList.size());
    assertTrue(yearList.contains("2000"));
    assertTrue(yearList.contains("2001"));
    assertTrue(!yearList.contains("2003"));

}

}
