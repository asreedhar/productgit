package com.firstlook.action.components.cia;

import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.tiles.ComponentContext;

import biz.firstlook.cia.service.CIAGroupingItemService;
import biz.firstlook.transact.persist.model.Inventory;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.components.PricePointActionUtil;
import com.firstlook.action.dealer.reports.PricePointBucketHolder;
import com.firstlook.action.dealer.reports.UsedCarPerformanceAnalyzerPlusDisplayHelper;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.VehicleSaleEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.iterator.SimpleFormIterator;
import com.firstlook.report.PAPReportLineItem;
import com.firstlook.report.PAPReportLineItemForm;
import com.firstlook.service.cia.report.TrimFormattingUtility;
import com.firstlook.service.pricepoints.UnitCostPointArguments;
import com.firstlook.service.pricepoints.UnitCostPointService;

public class CIAUnitCostPointDisplayAction extends SecureBaseAction
{

private CIAGroupingItemService ciaGroupingItemService;
private UnitCostPointService unitCostPointService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	ComponentContext context = ComponentContext.getContext( request );

	int ciaGroupingItemId = Integer.parseInt( (String)context.getAttribute( "ciaGroupingItemId" ) );
	int groupingDescriptionId = Integer.parseInt( (String)context.getAttribute( "groupingDescriptionId" ) );
	int weeks = Integer.parseInt( (String)context.getAttribute( "weeks" ) );
	int forecast = Integer.parseInt( (String)context.getAttribute( "forecast" ) );

	UnitCostPointArguments unitCostPointArguments = new UnitCostPointArguments();
	unitCostPointArguments.setGroupingDescriptionId( groupingDescriptionId );
	unitCostPointArguments.setBucketWeeks( 52 );
	unitCostPointArguments.setWeeks( weeks );
	unitCostPointArguments.setForecast( forecast );
	unitCostPointArguments.setInventoryType( Inventory.USED_CAR.intValue() );
	unitCostPointArguments.setDealerId( getFirstlookSessionFromRequest( request ).getCurrentDealerId() );
	unitCostPointArguments.setMileage( VehicleSaleEntity.MILEAGE_MAXVAL );

	PricePointBucketHolder bucketHolder = unitCostPointService.calculatePricePointBuckets( unitCostPointArguments, Inventory.USED_CAR );
	Collection pricePointBuckets = bucketHolder.getPricePointBuckets();
	UsedCarPerformanceAnalyzerPlusDisplayHelper.setCIAGroupingIdOnItems( pricePointBuckets, ciaGroupingItemId );

	Iterator itemsIter = pricePointBuckets.iterator();
	while ( itemsIter.hasNext() )
	{
		PAPReportLineItem lineItem = (PAPReportLineItem)itemsIter.next();
		lineItem.setGroupingColumnFormatted( TrimFormattingUtility.escapeJS( lineItem.getGroupingColumn() ) );
	}
	PricePointActionUtil pricePointActionUtil = new PricePointActionUtil();
	PAPReportLineItem overall = pricePointActionUtil.calculateOverallLineItem( pricePointBuckets );
	SimpleFormIterator iterator = new SimpleFormIterator( pricePointBuckets, PAPReportLineItemForm.class );
	request.setAttribute( "pricePointDetails", iterator );
	request.setAttribute( "pricePointBucketItems", iterator );
	request.setAttribute( "overall", overall );

	return mapping.findForward( "success" );
}

public CIAGroupingItemService getCiaGroupingItemService()
{
	return ciaGroupingItemService;
}

public void setCiaGroupingItemService( CIAGroupingItemService ciaGroupingItemService )
{
	this.ciaGroupingItemService = ciaGroupingItemService;
}

public UnitCostPointService getUnitCostPointService()
{
	return unitCostPointService;
}

public void setUnitCostPointService( UnitCostPointService pricePointService )
{
	this.unitCostPointService = pricePointService;
}

}
