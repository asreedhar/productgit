package com.firstlook.action.components;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.tiles.ComponentContext;

import biz.firstlook.commons.util.ColorUtility;
import biz.firstlook.transact.persist.service.appraisal.AppraisalActionType;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.DealerGroup;
import com.firstlook.exception.ApplicationException;

public class InventoryDecisionDisplayAction extends SecureBaseAction
{

private IAppraisalService appraisalService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	ComponentContext context = ComponentContext.getContext( request );
	String hasDemandDealersStr = (String)context.getAttribute( "hasDemandDealers" );
	boolean hasDemandDealers = new Boolean( hasDemandDealersStr ).booleanValue();
	boolean hasRedistribution = getFirstlookSessionFromRequest( request ).isIncludeRedistribution();
	putRedistributionActionsInRequest( request, hasDemandDealers && hasRedistribution );

	DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerId( getFirstlookSessionFromRequest( request ).getCurrentDealerId() );
	request.setAttribute( "isLithia", dealerGroup.isLithiaStore() );
	
	request.setAttribute("colors", ColorUtility.retrieveStandardColors());

	return mapping.findForward( "success" );
}

private void putRedistributionActionsInRequest( HttpServletRequest request, boolean dealerHasRedistribution ) throws ApplicationException
{
	List< AppraisalActionType > redistributionActions = appraisalService.retrieveRedistributionActions(dealerHasRedistribution, true);
	request.setAttribute( "distributionActions", redistributionActions );
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

}