package com.firstlook.action.components.auction;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.InvalidVinException;
import biz.firstlook.commons.util.VinUtility;
import biz.firstlook.entity.Area;
import biz.firstlook.entity.Period;
import biz.firstlook.entity.VICBodyStyle;
import biz.firstlook.service.IAuctionDataService;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.persistence.IDealerPreferenceDAO;
import biz.firstlook.transact.persist.service.vehicle.VehicleService;

import com.firstlook.action.SecureTileAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.form.AuctionDataForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.SessionHelper;

public class AuctionDataTitleBarAction extends SecureTileAction
{

private static final Logger logger = Logger.getLogger( AuctionDataTitleBarAction.class );
	
private IDealerPreferenceDAO dealerPrefDAO;
private IAuctionDataService auctionDataService; 
private VehicleService vehicleService; 

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	String make = RequestHelper.getString( request, "make" );
	String model = RequestHelper.getString( request, "model" );
	String mileage = RequestHelper.getString( request, "mileage" );
	Integer modelYear = RequestHelper.getInt( request, "modelYear" );
	String vin = RequestHelper.getString( request,  "vin" );
	Boolean isAppraisal = RequestHelper.getBoolean( request,  "isAppraisal" );

	if ( request.getParameter( "appraisalId" ) != null)
	{
		int appraisalId = RequestHelper.getInt( request, "appraisalId" );
		request.setAttribute( "appraisalId", new Integer( appraisalId ) );
	}
	

	List<Period> timePeriods = auctionDataService.retrieveAllTimePeriods();
	List<Area> areas = auctionDataService.retrieveAllAreas();
	int mileageInt;
	try
	{
		mileageInt = Integer.parseInt( mileage );
	}
	catch ( NumberFormatException nfe )
	{
		mileageInt = 0;
	}

	Integer businessUnitId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	DealerPreference dealerPreference = dealerPrefDAO.findByBusinessUnitId( businessUnitId );
	
	Integer year = null;
	try {
		year = VinUtility.getLatestModelYear( vin );
	} catch (InvalidVinException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	int areaId = dealerPreference.getAuctionAreaId();
	int timePeriodId = dealerPreference.getAuctionTimePeriodId();
	
	if(request.getParameter("areaId") != null) {
		areaId = Integer.parseInt(request.getParameter("areaId"));
	}
	if(request.getParameter("timePeriodId") != null) {
		timePeriodId = Integer.parseInt(request.getParameter("timePeriodId"));
	}

	AuctionDataForm auctionDataForm = new AuctionDataForm();
	auctionDataForm.setAreaId( areaId );
	auctionDataForm.setTimePeriodId( timePeriodId );
	auctionDataForm.setMileage( mileageInt );
	auctionDataForm.setModelYear( modelYear );
	
	
	List<VICBodyStyle> uidBodyStyles = new ArrayList<VICBodyStyle>();
	try {
		uidBodyStyles = auctionDataService.retrieveUIDs( vin, year );
	} catch (Exception e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}

	ArrayList<Integer> uidArray = new ArrayList<Integer>();
	for (VICBodyStyle uidBodyStyle : uidBodyStyles) {
		uidArray.add(uidBodyStyle.getUid());
	}

	String uidList = uidArray.toString();
	String finalUidList = uidList.substring(uidList.indexOf("[")+1, uidList.indexOf("]"));
	

	List<VICBodyStyle> uidAndVicList= auctionDataService.retrieveVICs(finalUidList);
	
	List< VICBodyStyle > vicBodyStyles = new ArrayList<VICBodyStyle>();
	
	for (VICBodyStyle uidBodyStyle : uidBodyStyles) {
		 int uid = uidBodyStyle.getUid();
		 for (VICBodyStyle uidAndVic : uidAndVicList) {
			 if(uid == uidAndVic.getUid()){
				 VICBodyStyle temp = new VICBodyStyle();
				 temp.setVic(uidAndVic.getVic());
				 temp.setBodyStyleDescription(uidBodyStyle.getBodyStyleDescription());
				 vicBodyStyles.add(temp);
				 break;
			 }
			
		}
	}
	

	Boolean usingVICs = Boolean.TRUE;
//	List< VICBodyStyle > vicBodyStyles = auctionDataService.retrieveVICBodyStyles( vin );
	
	// check to see if this vehicle already has a VIC selected
	Vehicle vehicle = null;
	try
	{
		// this hits vehicle table - if nothing found calls vehicleAttributeRetriever
		vehicle = vehicleService.identifyVehicle( vin );
	}
	catch ( Exception e )
	{
		logger.error("Unable to identify vehicle with VIN : " + vin + " in Vehicle Catalog Service");
	}
	
	if ( vehicle != null )
	{
		//vic was clobbered by nada uid.  reset vic if possible.
		if (!StringUtils.isBlank(vehicle.getVic())){
			if(vicBodyStyles != null && vicBodyStyles.size() == 1) {
				String vic = vicBodyStyles.get( 0 ).getVic();
				if (!vehicle.getVic().equals(vic)) {
					vehicle.setVic(vic);
				}
			}
			auctionDataForm.setVic( vehicle.getVic() );
		}
		else if(vicBodyStyles != null && vicBodyStyles.size() == 1) {
			auctionDataForm.setVic( vicBodyStyles.get( 0 ).getVic() );
		}
	}
	
	// if no VIC was found - pass down makemodelGroupingID
	if ( vicBodyStyles.isEmpty() )
	{
		usingVICs = Boolean.FALSE;
		VICBodyStyle bodyStyle = null;
		if ( vehicle != null && vehicle.getMakeModelGrouping() != null )
		{
			bodyStyle = new VICBodyStyle( vehicle.getMakeModelGroupingId(), vehicle.getMakeModelGrouping().getGroupingDescription().getGroupingDescription() );
		}
		else
		{
			bodyStyle = new VICBodyStyle( vehicle.getMakeModelGroupingId(), make + " " + model );
		}
		auctionDataForm.setVic( bodyStyle.getVic() );
		vicBodyStyles.add( bodyStyle );
	}
	
	request.setAttribute( "isAppraisal", isAppraisal );

	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );

	request.setAttribute( "usingVIC", usingVICs );
	request.setAttribute( "auctionDataForm", auctionDataForm );
	request.setAttribute( "timePeriods", timePeriods );
	request.setAttribute( "areas", areas );
	request.setAttribute( "vicBodyStylesSize", vicBodyStyles.size() );
	request.setAttribute( "vicBodyStyles", vicBodyStyles );
	request.setAttribute( "mileage", mileage );
	request.setAttribute( "vin", vin );

	request.setAttribute( "make", make );
	request.setAttribute( "model", model );
	request.setAttribute( "modelYear", modelYear );
	
	return mapping.findForward( "success" );
}

public void setDealerPrefDAO( IDealerPreferenceDAO dealerPrefDAO )
{
	this.dealerPrefDAO = dealerPrefDAO;
}

public void setAuctionDataService( IAuctionDataService auctionService )
{
	this.auctionDataService = auctionService;
}

public void setVehicleService( VehicleService vehicleService )
{
	this.vehicleService = vehicleService;
}

}
