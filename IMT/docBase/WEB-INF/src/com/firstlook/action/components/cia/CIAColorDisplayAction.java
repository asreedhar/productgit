package com.firstlook.action.components.cia;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.tiles.ComponentContext;

import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.GroupingDescription;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.components.VestigialItemUtil;
import com.firstlook.action.dealer.reports.UsedCarPerformanceAnalyzerPlusDisplayHelper;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.GroupingForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.groupingdescription.GroupingDescriptionService;

public class CIAColorDisplayAction extends SecureBaseAction
{
	
private UsedCarPerformanceAnalyzerPlusDisplayHelper usedCarPerformanceAnalyzerPlusDisplayHelper;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	ComponentContext context = ComponentContext.getContext( request );
	Member member = getMemberFromRequest( request );
	Dealer dealer = putDealerFormInRequest( request, 0 );

	int groupingDescriptionId = Integer.parseInt( (String)context.getAttribute( "groupingDescriptionId" ) );
	int ciaGroupingItemId = Integer.parseInt( (String)context.getAttribute( "ciaGroupingItemId" ) );
	int weeks = Integer.parseInt( (String)context.getAttribute( "weeks" ) );
	int forecast = Integer.parseInt( (String)context.getAttribute( "forecast" ) );
	int mileageFilter = Integer.parseInt( (String)context.getAttribute( "mileageFilter" ) );

	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	int mileageThreshold = usedCarPerformanceAnalyzerPlusDisplayHelper.retrieveMileageThreshold( mileageFilter, currentDealerId );

	GroupingDescriptionService gdService = new GroupingDescriptionService();
	GroupingDescription groupingDescription = gdService.retrieveById( new Integer( groupingDescriptionId ) );
	request.setAttribute( "groupingForm", new GroupingForm( groupingDescription ) );

	DealerPreference dealerPreference = dealer.getDealerPreference();
	int lowerUnitCostThreshold = dealerPreference.getUnitCostThresholdLowerAsInt();
	usedCarPerformanceAnalyzerPlusDisplayHelper.putColorItemsInRequest( request, member.getProgramType(), weeks, forecast, mileageThreshold, groupingDescriptionId,
										getFirstlookSessionFromRequest( request ).isIncludeMarket(), lowerUnitCostThreshold, ciaGroupingItemId );

	VestigialItemUtil viUtil = new VestigialItemUtil();
	viUtil.populateVestigialItemsBasedOnPreferences( request, "color", true );

	return mapping.findForward( "success" );
}

public void setUsedCarPerformanceAnalyzerPlusDisplayHelper(
		UsedCarPerformanceAnalyzerPlusDisplayHelper usedCarPerformanceAnalyzerPlusDisplayHelper) {
	this.usedCarPerformanceAnalyzerPlusDisplayHelper = usedCarPerformanceAnalyzerPlusDisplayHelper;
}



}
