package com.firstlook.action.components.vehicledetailpage;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.tiles.ComponentContext;

import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;

import com.firstlook.action.SecureTileAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.session.FirstlookSession;

public class TilePrintableReportsDisplayAction extends SecureTileAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    ComponentContext context = ComponentContext.getContext( request );  
    int currentDealerId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();
    Dealer currentDealer = getImtDealerService().retrieveDealer( currentDealerId );
    setPrintableReportFlagsOnRequest( request, currentDealer );
    if (context != null){
        Object inventory = context.getAttribute( "inventoryId" );
        if (inventory != null){
            Integer inventoryId = new Integer( inventory.toString() );
            request.setAttribute( "inventoryId", inventoryId );        	
            return mapping.findForward( "success" );
        }    	
    }
    return mapping.findForward( "noninventorysuccess" );
}

private void setPrintableReportFlagsOnRequest( HttpServletRequest request, Dealer currentDealer )
{
    List<Integer> thirdPartyIds = currentDealer.getDealerPreference().getOrderedGuideBookIds(); 
    boolean isGalves = thirdPartyIds.contains(ThirdPartyDataProvider.GUIDEBOOK_GALVES_TYPE);
    boolean isKelley = thirdPartyIds.contains(ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE );
    boolean isNADA = thirdPartyIds.contains(ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE);
    boolean isBlackBook = thirdPartyIds.contains(ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE );

    boolean isAuction = getFirstlookSessionFromRequest( request ).isIncludeAuction();
    boolean isWindowSticker = getFirstlookSessionFromRequest( request ).isIncludeWindowSticker();

    request.setAttribute( "isKelley", isKelley );
    request.setAttribute( "isNADA", isNADA );
    request.setAttribute( "isBlackBook", isBlackBook );
    request.setAttribute( "isGalves", isGalves );
    request.setAttribute( "isAuction", isAuction );
    request.setAttribute( "isWindowSticker", isWindowSticker );
}

}
