package com.firstlook.action.components;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.tiles.ComponentContext;

import com.firstlook.action.SecureTileAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.ReportPersist;
import com.firstlook.report.Report;
import com.firstlook.report.ReportGrouping;
import com.firstlook.report.ReportGroupingForm;
import com.firstlook.service.report.ReportService;

public class ReportGroupingToggleDisplayAction extends SecureTileAction
{

private ReportPersist reportPersist;
private ReportService reportService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	ComponentContext context = ComponentContext.getContext( request );

	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();

	String groupingDescriptionIdStr = (String)context.getAttribute( "groupingDescriptionId" );
	String weeksStr = (String)context.getAttribute( "weeks" );
	String mileageStr = (String)context.getAttribute( "mileage" );
	String includeDealerGroupStr = (String)context.getAttribute( "includeDealerGroup" );
	String forecastStr = (String)context.getAttribute( "forecast" );

	int forecast = Integer.parseInt( forecastStr );
	int groupingDescriptionId = Integer.parseInt( groupingDescriptionIdStr );
	int weeks = Integer.parseInt( weeksStr );
	int mileage = Integer.parseInt( mileageStr );
	int includeDealerGroup = Integer.parseInt( includeDealerGroupStr );

	ReportGrouping reportGrouping = reportService.retrieveReportGrouping( currentDealerId, groupingDescriptionId, weeks, mileage, includeDealerGroup,
																	forecast );
	if ( reportGrouping != null )
	{
		Report report = new Report();
		reportPersist.findAllAverages( report, currentDealerId, weeks, forecast, InventoryEntity.USED_CAR );
		reportGrouping.calculateOptimixPercentages( report );
	}

	request.setAttribute( "reportGroupingForm", new ReportGroupingForm( reportGrouping ) );

	return mapping.findForward( "success" );
}

public void setReportPersist( ReportPersist reportPersist )
{
	this.reportPersist = reportPersist;
}

public void setReportService(ReportService reportService) {
	this.reportService = reportService;
}
}
