package com.firstlook.action.components;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.tiles.ComponentContext;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.dealer.reports.PricePointBucketHolder;
import com.firstlook.action.dealer.reports.UsedCarPerformanceAnalyzerPlusDisplayHelper;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.pricepoints.UnitCostPointArguments;
import com.firstlook.service.pricepoints.UnitCostPointService;
import com.firstlook.session.FirstlookSession;

public class PricePointDisplayAction extends SecureBaseAction
{

private UnitCostPointService unitCostPointService;
private UsedCarPerformanceAnalyzerPlusDisplayHelper usedCarPerformanceAnalyzerPlusDisplayHelper;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    putDealerFormInRequest( request, 0 );
    ComponentContext context = ComponentContext.getContext( request );

    int groupingDescriptionId = Integer.parseInt( (String)context.getAttribute( "groupingDescriptionId" ) );
    int currentDealerId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();
    int inventoryType = getMemberFromRequest( request ).getInventoryType().getValue();
    int weeks = Integer.parseInt( (String)context.getAttribute( "weeks" ) );
    int forecast = Integer.parseInt( (String)context.getAttribute( "forecast" ) );
    int mileageFilter = Integer.parseInt( (String)context.getAttribute( "mileageFilter" ) );
    int mileageThreshold = usedCarPerformanceAnalyzerPlusDisplayHelper.retrieveMileageThreshold( mileageFilter, currentDealerId );
    
    UnitCostPointArguments unitCostPointArguments = new UnitCostPointArguments();
	unitCostPointArguments.setGroupingDescriptionId( groupingDescriptionId );
	unitCostPointArguments.setBucketWeeks( 52 );
	unitCostPointArguments.setWeeks( weeks );
	unitCostPointArguments.setForecast( forecast );
	unitCostPointArguments.setInventoryType( inventoryType );
	unitCostPointArguments.setDealerId( getFirstlookSessionFromRequest( request ).getCurrentDealerId() );
	unitCostPointArguments.setMileage( mileageThreshold );

    PricePointBucketHolder bucketHolder = unitCostPointService.calculatePricePointBuckets( unitCostPointArguments, new Integer( inventoryType ) );

    PricePointActionUtil pricePointActionUtil = new PricePointActionUtil();
    pricePointActionUtil.putPricePointInformationInRequest( request, bucketHolder );

    return mapping.findForward( "success" );
}

public UnitCostPointService getUnitCostPointService()
{
	return unitCostPointService;
}

public void setUnitCostPointService( UnitCostPointService unitCostPointService )
{
	this.unitCostPointService = unitCostPointService;
}

public void setUsedCarPerformanceAnalyzerPlusDisplayHelper(
		UsedCarPerformanceAnalyzerPlusDisplayHelper usedCarPerformanceAnalyzerPlusDisplayHelper) {
	this.usedCarPerformanceAnalyzerPlusDisplayHelper = usedCarPerformanceAnalyzerPlusDisplayHelper;
}



}
