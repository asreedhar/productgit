package com.firstlook.action.components.vehicledetailpage;

import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.tiles.ComponentContext;

import biz.firstlook.module.vehicle.description.old.GBException;
import biz.firstlook.module.vehicle.description.old.GuideBookInput;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.persistence.IInventoryBookOutDAO;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.dealer.vehicledetailpage.BookoutDetailForm;
import com.firstlook.data.DatabaseException;
import com.firstlook.display.tradeanalyzer.DisplayGuideBook;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;
import com.firstlook.service.bookout.appraisal.AppraisalBookOutService;
import com.firstlook.service.bookout.bean.AbstractBookOutState;
import com.firstlook.service.bookout.bean.StoredBookOutState;
import com.firstlook.service.bookout.inventory.InventoryBookOutService;
import com.firstlook.service.inventory.VehicleInventoryService;

public class BookOutPrintPageDisplayAction extends SecureBaseAction
{

private IInventoryBookOutDAO inventoryBookOutDAO;
private InventoryBookOutService inventoryBookOutService;
private VehicleInventoryService vehicleInventoryService;
private IAppraisalService appraisalService;
private AppraisalBookOutService appraisalBookOutService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	Dealer dealer = getImtDealerService().retrieveDealer( currentDealerId );
	DealerPreference preference = dealer.getDealerPreference();

	ComponentContext context = ComponentContext.getContext( request );

	Object contextInventoryId = context.getAttribute("inventoryId");
	Integer inventoryId = null;
	boolean isInventory = false;
	try {
		inventoryId = Integer.parseInt(contextInventoryId.toString());
		isInventory = true;
	} catch (NumberFormatException nfe) {
		isInventory = false;
	}
	
	ThirdPartyDataProvider provider = null;
	StoredBookOutState storedBookoutState = null;
	GuideBookInput guideBookInput = null;
	int vehicleMileage = 0;
	if(isInventory) {
		int guideBookId = Integer.parseInt( context.getAttribute( "guideBookType" ).toString() );
		
		VehicleInventory vehicleInventory = vehicleInventoryService.retrieveVehicleInventory( currentDealerId, inventoryId );
		guideBookInput = buildGuideBookInput(dealer, vehicleInventory.getVehicle());
		provider = ThirdPartyDataProvider.getThirdPartyDataProvider(guideBookId);
		try {
			storedBookoutState = getInventoryBookOutService().retrieveStoredBookOutInfo( 
					guideBookInput, 
					provider.getThirdPartyId(), 
					inventoryId, 
					preference.getSearchInactiveInventoryDaysBackThreshold() );
		} catch (GBException e) {
			e.printStackTrace();
		}
		vehicleMileage = vehicleInventory.getInventory().getMileageReceived();
		request.setAttribute( "stockNumber", vehicleInventory.getInventory().getStockNumber());
	} else {
		int appraisalId = Integer.parseInt(request.getParameter("appraisalId"));
		
		int guideBookId;
		
		if (context.getAttribute( "guideBookType" ) != null){
			guideBookId = Integer.parseInt(context.getAttribute( "guideBookType" ).toString()); 
		}
		else{
			boolean guideBookPrimary = Boolean.parseBoolean(request.getParameter("guideBookPrimary"));
			guideBookId = !guideBookPrimary ?  dealer.getGuideBook2Id() : dealer.getGuideBookId();	
		}

		IAppraisal appraisal = getAppraisalService().findBy( appraisalId );
		guideBookInput = buildGuideBookInput(dealer, appraisal.getVehicle());
		provider = ThirdPartyDataProvider.getThirdPartyDataProvider(guideBookId);
		AbstractBookOutState bookOutState;
		try {
			bookOutState = appraisalBookOutService.retrieveExistingBookOutInfoOrBeginNewBookOut(
					guideBookInput, 
					provider.getThirdPartyId(), 
					preference.getSearchInactiveInventoryDaysBackThreshold(), 
					null);
		
			if (bookOutState instanceof StoredBookOutState) {
				storedBookoutState = (StoredBookOutState) bookOutState;
			} else {
				//error, vehicle not booked out.
			}
		} catch (GBException e) {
			e.printStackTrace();
		}
		vehicleMileage = appraisal.getMileage();
	}
	request.setAttribute("mileage", vehicleMileage);
	
	if(storedBookoutState != null && provider != null && guideBookInput != null) {
		BookoutDetailForm bookoutForm = new BookoutDetailForm();
		boolean bookValuesAvailable = false;
	
		if ( storedBookoutState != null )
		{
			bookValuesAvailable = true;
			DisplayGuideBook displayGuideBook = new DisplayGuideBook();
			bookoutForm.setVin( storedBookoutState.getVin().toUpperCase() );
			bookoutForm.setMileage( storedBookoutState.getBookoutMileage() );
			storedBookoutState.setRegionDescription(guideBookInput.getState());
			storedBookoutState.transformBookOutToDisplayData( displayGuideBook );
			removeUnselectedDisplayGuideBookVehicles(displayGuideBook);
			bookoutForm.addDisplayGuideBook( displayGuideBook );
			if ( ThirdPartyDataProvider.KELLEY.equals(provider) ) {
				request.setAttribute( "equipmentOptions", displayGuideBook.determineKelleyPrintableOptions() );
			} else if ( ThirdPartyDataProvider.NADA.equals(provider)) {
				request.setAttribute( "nadaEquipmentOptions", displayGuideBook.determineSelectedOptions() );
				request.setAttribute("NADAReportType", context.getAttribute("NADAReportType") );
			} else if ( ThirdPartyDataProvider.BLACKBOOK.equals(provider)) {
				request.setAttribute( "bbEquipmentOptions", displayGuideBook.determineSelectedOptions() );
            } else if ( ThirdPartyDataProvider.GALVES.equals(provider)) {
				request.setAttribute( "galvesEquipmentOptions", displayGuideBook.determineSelectedOptions() );
			}
		}
		
		request.setAttribute( "bookValuesAvailable", bookValuesAvailable );
		SessionHelper.keepAttribute( request, "bookoutDetailForm" );
		request.setAttribute( "bookoutForm", bookoutForm );
		request.setAttribute("IsInventory", isInventory);
		request.setAttribute( "nickname", dealer.getNickname() );
	}
	
	return mapping.findForward( "success" );
}

private GuideBookInput buildGuideBookInput(Dealer dealer, Vehicle vehicle) {
	GuideBookInput input = new GuideBookInput();
	input.setBusinessUnitId( dealer.getDealerId() );
	input.setVin( vehicle.getVin() );
	input.setState( dealer.getState() );
	input.setNadaRegionCode( dealer.getDealerPreference().getNadaRegionCode() );
	return input;
}

/**
 * Print page uses the data in the first element of displayGuideBookVehicles for printing.
 * The order of models with multiple trims is not always garaunteed so we filter out all
 * but the selected vehicle. 
 * @param displayGuideBook
 */
private void removeUnselectedDisplayGuideBookVehicles(DisplayGuideBook displayGuideBook) {
	ArrayList<VDP_GuideBookVehicle> selectedVehicle = new ArrayList<VDP_GuideBookVehicle>();
	Iterator<VDP_GuideBookVehicle> allVehicles = displayGuideBook.getDisplayGuideBookVehiclesCollection().iterator();
	while (allVehicles.hasNext()) {
		VDP_GuideBookVehicle vehicle = allVehicles.next();
		if (vehicle.getKey().equalsIgnoreCase(displayGuideBook.getSelectedVehicleKey())) {
			selectedVehicle.add(vehicle);
			break;
		}
	}
	displayGuideBook.setDisplayGuideBookVehicles(selectedVehicle);
}

public IAppraisalService getAppraisalService()
{
	return appraisalService;
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

public InventoryBookOutService getInventoryBookOutService()
{
	return inventoryBookOutService;
}

public IInventoryBookOutDAO getInventoryBookOutDAO()
{
	return inventoryBookOutDAO;
}

public void setInventoryBookOutDAO( IInventoryBookOutDAO inventoryBookOutDAO )
{
	this.inventoryBookOutDAO = inventoryBookOutDAO;
}

public void setInventoryBookOutService( InventoryBookOutService inventoryBookOutService )
{
	this.inventoryBookOutService = inventoryBookOutService;
}

public VehicleInventoryService getVehicleInventoryService()
{
	return vehicleInventoryService;
}

public void setVehicleInventoryService( VehicleInventoryService vehicleInventoryService )
{
	this.vehicleInventoryService = vehicleInventoryService;
}

public void setAppraisalBookOutService(
		AppraisalBookOutService appraisalBookOutService) {
	this.appraisalBookOutService = appraisalBookOutService;
}

}
