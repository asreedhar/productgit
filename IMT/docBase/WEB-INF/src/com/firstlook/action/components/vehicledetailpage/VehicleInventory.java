package com.firstlook.action.components.vehicledetailpage;

import biz.firstlook.transact.persist.model.Inventory;
import biz.firstlook.transact.persist.model.Vehicle;

public class VehicleInventory
{

private Inventory inventory;
private Vehicle vehicle;

public VehicleInventory( Inventory i, Vehicle v )
{
    this.vehicle = v;
    this.inventory = i;
}

public Inventory getInventory()
{
    return inventory;
}

public void setInventory( Inventory inventory )
{
    this.inventory = inventory;
}

public Vehicle getVehicle()
{
    return vehicle;
}

public void setVehicle( Vehicle vehicle )
{
    this.vehicle = vehicle;
}

}
