package com.firstlook.action.components;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.tiles.ComponentContext;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.dealer.reports.PricePointBucketHolder;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.report.PAPReportLineItem;
import com.firstlook.service.pricepoints.UnitCostPointNewCarArguments;
import com.firstlook.service.pricepoints.UnitCostPointService;

public class PricePointNewCarDisplayAction extends SecureBaseAction
{

private UnitCostPointService unitCostPointService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	putDealerFormInRequest( request, 0 );
	ComponentContext context = ComponentContext.getContext( request );

	String make = request.getParameter( "make" );
	String model = request.getParameter( "model" );
	String trim = (String)context.getAttribute( "trim" );
	int bodyStyleId = Integer.parseInt( (String)context.getAttribute( "bodyStyleId" ) );
	int inventoryType = InventoryEntity.NEW_CAR;
	int weeks = Integer.parseInt( (String)context.getAttribute( "weeks" ) );
	int forecast = Integer.parseInt( (String)context.getAttribute( "forecast" ) );

	trim = PAPReportLineItem.determineVehicleTrim( trim );

	UnitCostPointNewCarArguments unitCostPointNewCarArguments = populateUnitCostPointNewCarArguments(
																										make,
																										model,
																										trim,
																										bodyStyleId,
																										inventoryType,
																										weeks,
																										forecast,
																										getFirstlookSessionFromRequest( request ).getCurrentDealerId() );

	PricePointBucketHolder bucketHolder = unitCostPointService.calculatePricePointBuckets( unitCostPointNewCarArguments,
																							new Integer( inventoryType ) );

	PricePointActionUtil pricePointActionUtil = new PricePointActionUtil();
	pricePointActionUtil.putPricePointInformationInRequest( request, bucketHolder );

	return mapping.findForward( "success" );
}

private UnitCostPointNewCarArguments populateUnitCostPointNewCarArguments( String make, String model, String trim, int bodyStyleId,
																			int inventoryType, int weeks, int forecast, int currentDealerId )
{
	UnitCostPointNewCarArguments unitCostPointNewCarArguments = new UnitCostPointNewCarArguments();
	unitCostPointNewCarArguments.setDealerId( currentDealerId );
	unitCostPointNewCarArguments.setMake( make );
	unitCostPointNewCarArguments.setModel( model );
	unitCostPointNewCarArguments.setTrim( trim );
	unitCostPointNewCarArguments.setBodyTypeId( bodyStyleId );
	unitCostPointNewCarArguments.setWeeks( weeks );
	unitCostPointNewCarArguments.setBucketWeeks( 52 );
	unitCostPointNewCarArguments.setForecast( forecast );
	unitCostPointNewCarArguments.setInventoryType( inventoryType );
	return unitCostPointNewCarArguments;
}

public UnitCostPointService getUnitCostPointService()
{
	return unitCostPointService;
}

public void setUnitCostPointService( UnitCostPointService unitCostPointService )
{
	this.unitCostPointService = unitCostPointService;
}

}
