package com.firstlook.action.components.vehicledetailpage;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.vehicle.description.old.BookOutDatasetInfo;
import biz.firstlook.module.vehicle.description.old.BookOutError;
import biz.firstlook.module.vehicle.description.old.BookOutTypeEnum;
import biz.firstlook.module.vehicle.description.old.GBException;
import biz.firstlook.module.vehicle.description.old.GuideBookInput;
import biz.firstlook.transact.persist.model.BookOutSource;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.dealer.vehicledetailpage.BookoutDetailForm;
import com.firstlook.data.DatabaseException;
import com.firstlook.display.tradeanalyzer.DisplayGuideBook;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.DealerUpgradeLookup;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.SessionHelper;
import com.firstlook.service.bookout.BookOutService;
import com.firstlook.service.bookout.appraisal.AppraisalBookOutService;
import com.firstlook.service.bookout.bean.AbstractBookOutState;
import com.firstlook.service.bookout.bean.StoredBookOutState;
import com.firstlook.service.bookout.inventory.InventoryBookOutService;
import com.firstlook.service.dealer.DealerService;
import com.firstlook.service.inventory.InventoryService_Legacy;

public class BookOutDisplayAction extends SecureBaseAction
{

private DealerPreferenceDAO dealerPrefDAO;
private InventoryBookOutService inventoryBookOutService;
private AppraisalBookOutService appraisalBookOutService;
private BookOutService bookOutService;
private InventoryService_Legacy inventoryService_Legacy;


public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	String vin = request.getParameter( "vin" );
	Integer identifier = new Integer( request.getParameter( "identifier" ) );
	Integer mileage = new Integer( Integer.parseInt( request.getParameter( "mileage" ) ) );
	Integer bookOutSourceId = new Integer( Integer.parseInt( request.getParameter( "bookOutSourceId" ) ) );
	Boolean isActive = new Boolean( request.getParameter( "isActive" ) );
	Boolean editableMileage = new Boolean( request.getParameter( "editableMileage" ) );
	Boolean displayValues = new Boolean( request.getParameter( "displayValues" ) );
	Boolean displayOptions = new Boolean( request.getParameter( "displayOptions" ) );
	Boolean allowUpdate = new Boolean( request.getParameter( "allowUpdate" ) );
	Boolean refreshBookData = false;
	if (request.getParameter("refreshBookData") != null) {
		refreshBookData = RequestHelper.getBoolean( request, "refreshBookData" ) ;
	}
	
	
	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	boolean isKBBTradeInEnabled = getBookOutService().findUpgrade(currentDealerId, DealerUpgradeLookup.KBB_TRADE_IN_CODE);
	Dealer dealer = getImtDealerService().retrieveDealer( currentDealerId );
	DealerPreference dealerPref = dealer.getDealerPreference();	
	int mainBookPreference = dealer.getBookOutPreferenceId();
	List<Integer> thirdPartyIds = dealerPref.getOrderedGuideBookIds();

	GuideBookInput input = BookOutService.createGuideBookInput( vin, mileage, currentDealerId, dealer.getState(), dealerPref.getNadaRegionCode(), getMemberFromRequest( request ).getMemberId() );

	BookoutDetailForm bookoutForm = new BookoutDetailForm();

	populateDisplayGuideBooks( request, vin, identifier, mileage, thirdPartyIds, input, bookoutForm, bookOutSourceId, dealerPref.getSearchInactiveInventoryDaysBackThreshold(), refreshBookData );

	request.setAttribute("isKBBTradeInEnabled", isKBBTradeInEnabled);
	request.setAttribute( "isActive", isActive );
	request.setAttribute( "editableMileage", editableMileage );
	request.setAttribute( "identifier", identifier );
	request.setAttribute( "displayValues", displayValues );
	request.setAttribute( "displayOptions", displayOptions );
	request.setAttribute( "allowUpdate", allowUpdate );
	request.setAttribute( "bookOutSourceId", bookOutSourceId );
	request.setAttribute( "bookOutWasSaved", new Boolean( false ) );
	request.setAttribute( "bookoutPreferenceId", mainBookPreference );
	request.setAttribute( "primaryGuideBookId", dealerPref.getGuideBookId() );
	request.setAttribute( "secondaryGuideBookId", dealerPref.getGuideBook2Id() );
	request.setAttribute( "bookoutDetailForm", bookoutForm );
	
	boolean apppraisalLockoutEnabled = getFirstlookSessionFromRequest( request ).isIncludeAppraisalLockout();
	DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerId( currentDealerId );
	if (dealerGroup.isLithiaStore() )
	{
		apppraisalLockoutEnabled = true;
	}
	request.setAttribute( "bookoutLockoutEnabled", apppraisalLockoutEnabled );
	
	SessionHelper.setAttribute( request, "bookoutDetailForm", bookoutForm );
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );
	
	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
	ActionForward forward = mapping.findForward( "success"); 
	
	return forward; 
}

private void populateDisplayGuideBooks( HttpServletRequest request, String vin, Integer identifier, Integer mileage, List<Integer> thirdPartyIds,
										GuideBookInput input, BookoutDetailForm bookoutForm, Integer bookOutSourceId, int searchAppraisalDaysBackThreshold, Boolean refreshBookData )
{
	Iterator<Integer> thirdPartyIdsIter = thirdPartyIds.iterator();

	bookoutForm.setVin( vin );
	bookoutForm.setMileage( mileage );
	ActionErrors guideBookErrors = new ActionErrors();
	AbstractBookOutState bookOutState = null;
	while ( thirdPartyIdsIter.hasNext() )
	{

		Integer thirdPartyBookoutServiceId = thirdPartyIdsIter.next();
		
		try 
		{
			bookOutState = constructBookOutState( identifier, input, bookOutSourceId,bookOutState, thirdPartyBookoutServiceId, searchAppraisalDaysBackThreshold, refreshBookData );

			constructDisplayGuideBooks( request, bookoutForm, guideBookErrors, bookOutState, thirdPartyBookoutServiceId );
		} catch (Exception e)
		{
			DisplayGuideBook guideBook = new DisplayGuideBook();
			guideBook.setSuccess( false );
			Throwable t = e.getCause();
			guideBook.setSuccessMessage( t == null ? e.getMessage() : t.getMessage() );
			guideBook.setValidVin( false );
			guideBook.setGuideBookName( ThirdPartyDataProvider.getThirdPartyDataProviderDescription( thirdPartyBookoutServiceId.intValue() ));
			bookoutForm.addDisplayGuideBook( guideBook );
		}
	}
}

public static void constructDisplayGuideBooks( HttpServletRequest request, BookoutDetailForm bookoutForm, ActionErrors guideBookErrors, 
										AbstractBookOutState bookOutState, Integer thirdPartyBookoutServiceId )
{
	DisplayGuideBook displayGuideBook;
	Map<Integer, List<BookOutError>> bookOutErrorsByType = bookOutState.getBookOutErrors();
	if(bookOutErrorsByType != null) {
		if ( bookOutErrorsByType.get(Integer.valueOf( BookOutError.INVALID_VIN_BAD_FORMAT)) != null ) {
			guideBookErrors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.dealer.tools.vehiclelocatorsearch.vin" ) );
			request.setAttribute( "guideBookError", Boolean.TRUE);
			putActionErrorsInRequest( request, guideBookErrors );
		}
		else if ( bookOutErrorsByType.get( new Integer( BookOutError.INVALID_VIN_NOT_IN_GUIDEBOOK ) ) != null ) {
			DisplayGuideBook invalidVinGuideBook = new DisplayGuideBook();
			bookOutState.transformBookOutToDisplayData( invalidVinGuideBook );
			bookoutForm.addDisplayGuideBook( invalidVinGuideBook );
		}
		else {
			displayGuideBook = new DisplayGuideBook();
			bookOutState.transformBookOutToDisplayData( displayGuideBook );
			
			// this compares the mileage from the page with the mileage that was used for the bookout
			// if they do not match, the user will get an alert informing them
			if (bookOutState instanceof StoredBookOutState) {
				StoredBookOutState storedBookOutState = (StoredBookOutState) bookOutState;
				if ( !storedBookOutState.getBookoutMileage().equals(bookoutForm.getMileage()) ) {
					displayGuideBook.setInAccurate( true );
				}
			}
			bookoutForm.addDisplayGuideBook( displayGuideBook );
		}
	}
}

private AbstractBookOutState constructBookOutState( Integer identifier, GuideBookInput input, Integer bookOutSourceId,
													AbstractBookOutState bookOutState, Integer thirdPartyId, 
													int searchAppraisalDaysBackThreshold, Boolean refreshBookData )
		throws GBException
{
	if( bookOutSourceId.intValue() == BookOutSource.BOOK_OUT_SOURCE_VEHICLE_DETAIL.intValue() )
	{
		bookOutState = getInventoryBookOutService().retrieveExistingBookOutInfoOrBeginNewBookOut( input, thirdPartyId, identifier, searchAppraisalDaysBackThreshold, refreshBookData );
	}
	else if( bookOutSourceId.intValue() == BookOutSource.BOOK_OUT_SOURCE_APPRAISAL.intValue() )
	{
		// nk - DE 740 - We may be viewing an appraisal for an Inventory item. If so, we do not use
		// the daysBackThreshold.
		String stockNumber = inventoryService_Legacy.retrieveStockNumberByVinAndDealerId(input.getVin(),input.getBusinessUnitId(), null);
		if (stockNumber != null) {
			// appraisal for an inventory item
			bookOutState = getAppraisalBookOutService().retrieveExistingBookOutInfoOrBeginNewBookOut( input, thirdPartyId, null, null );
		} else {
			bookOutState = getAppraisalBookOutService().retrieveExistingBookOutInfoOrBeginNewBookOut( input, thirdPartyId, searchAppraisalDaysBackThreshold, null );
		}
	}
	else
	{
		BookOutTypeEnum bookoutType = (bookOutSourceId.intValue() == BookOutSource.BOOK_OUT_SOURCE_VEHICLE_DETAIL.intValue() ) ? BookOutTypeEnum.INVENTORY : BookOutTypeEnum.APPRAISAL;
		bookOutState = getBookOutService().doVinLookup( input, thirdPartyId, new BookOutDatasetInfo( bookoutType, input.getBusinessUnitId()), null );
	}
	return bookOutState;
}

public DealerPreferenceDAO getDealerPrefDAO()
{
	return dealerPrefDAO;
}

public void setDealerPrefDAO( DealerPreferenceDAO dealerPrefDAO )
{
	this.dealerPrefDAO = dealerPrefDAO;
}

public InventoryBookOutService getInventoryBookOutService()
{
	return inventoryBookOutService;
}

public void setInventoryBookOutService( InventoryBookOutService inventoryBookOutService )
{
	this.inventoryBookOutService = inventoryBookOutService;
}

public AppraisalBookOutService getAppraisalBookOutService()
{
	return appraisalBookOutService;
}

public void setAppraisalBookOutService( AppraisalBookOutService appraisalBookOutService )
{
	this.appraisalBookOutService = appraisalBookOutService;
}

public BookOutService getBookOutService()
{
	return bookOutService;
}

public void setBookOutService( BookOutService bookOutService )
{
	this.bookOutService = bookOutService;
}

public void setInventoryService_Legacy(InventoryService_Legacy inventoryService) {
	this.inventoryService_Legacy = inventoryService;
}
}
