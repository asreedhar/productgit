package com.firstlook.action.components;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.tiles.ComponentContext;

import biz.firstlook.cia.service.BasisPeriodService;
import biz.firstlook.transact.persist.model.CIAPreferences.TimePeriod;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.InventorySalesAggregate;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.inventory.InventorySalesAggregateService;
import com.firstlook.service.inventory.InventorySalesAggregateHelper;
import com.firstlook.session.FirstlookSession;

public class CIAInventorySummaryDisplayAction extends SecureBaseAction
{

private BasisPeriodService basisPeriodService;
private InventorySalesAggregateService inventorySalesAggregateService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	ComponentContext context = ComponentContext.getContext( request );
	int dealerId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();
	int inventoryType = getMemberFromRequest( request ).getInventoryType().getValue();

	int weeks = basisPeriodService.calculateNumberOfWeeksInBasisPeriodByType(
																				new Integer( dealerId ),
																				TimePeriod.SALES_HISTORY_DISPLAY, 0 );

	InventorySalesAggregate aggregate = inventorySalesAggregateService.retrieveCachedInventorySalesAggregate( dealerId, weeks, 0, inventoryType );
	InventorySalesAggregateHelper.putMakeModelCountInRequest( request, aggregate );
	request.setAttribute( "totalDaysSupply", context.getAttribute( "totalDaysSupply" ) );
	request.setAttribute( "vehicleCount", context.getAttribute( "vehicleCount" ) );

	return mapping.findForward( "success" );
}


public BasisPeriodService getBasisPeriodService()
{
	return basisPeriodService;
}


public void setBasisPeriodService( BasisPeriodService basisPeriodService )
{
	this.basisPeriodService = basisPeriodService;
}


public InventorySalesAggregateService getInventorySalesAggregateService()
{
    return inventorySalesAggregateService;
}


public void setInventorySalesAggregateService( InventorySalesAggregateService inventorySalesAggregateService )
{
    this.inventorySalesAggregateService = inventorySalesAggregateService;
}

}
