package com.firstlook.action;

import java.io.IOException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.servlet.http.HttpServletRequestHelper;
import biz.firstlook.commons.util.PropertyLoader;

import com.discursive.cas.extend.client.CASFilter;
import com.discursive.cas.extend.client.CASReceipt;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Member;
import com.firstlook.entity.ProgramTypeEnum;
import com.firstlook.exception.ApplicationException;
import com.firstlook.presentation.HostTypeResolver;

public class LogoutAction extends SecureBaseAction
{

private Log logger = LogFactory.getLog( getClass() );

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	final HttpSession session = request.getSession();
	if (session.getAttribute(RemoteEntryAction.FLAG_REMOTE_ENTRY) != null) {
		session.invalidate();
		return mapping.findForward("exitStoreConfirmationPage");
	}
	
	CASReceipt receipt = (CASReceipt)session.getAttribute(CASFilter.CAS_FILTER_RECEIPT);
	
	Member member = getMemberFromRequest( request );
	
	boolean redirectSingleSignOut = false;
	String login = member.getLogin();
	if( member.getProgramType() != null && member.getProgramType().equals( ProgramTypeEnum.DEALERS_RESOURCES.getName() ))
	{
		logger.debug( "User ProgramType was " + member.getProgramType() );
		redirectSingleSignOut = true;
	}
	String forward = "success";
	session.invalidate();

	// service url for CAS
	String applicationUrl = null;
	
	if( request.getParameter( "serviceUrl" ) != null )
		applicationUrl = request.getParameter( "serviceUrl" );
	else
		applicationUrl = HttpServletRequestHelper.getServiceUrl(request);
	
	Cookie dotNetCookie = new Cookie(".ASPXAUTH", "");
	dotNetCookie.setMaxAge(0);
	dotNetCookie.setPath("/");
	response.addCookie(dotNetCookie);
	
	
	request.setAttribute( "service", applicationUrl );

	// add this to casConfig?? - planned for single sign out in CAS v3.1
	String casLogoutUrl = receipt.getCasLogoutUrl();

	request.setAttribute( "casLogoutUrl", casLogoutUrl );
	logger.debug( login + " logged out of " + applicationUrl );
	if( redirectSingleSignOut )
	{
		try
		{
			logger.debug( "Redirecting to log out of PrivateLabel servlet." );
			//DealersResource and it's using www.firstlook.biz host
			if( HostTypeResolver.getInstance().isInsightHost( request.getHeader( "host" ) ) )
				return mapping.findForward( "drSuccess" );
			else
				response.sendRedirect( PropertyLoader.getProperty( "firstlook.privateLabel.logout.url" ) );
		}
		catch ( IOException e )
		{
			return mapping.findForward( forward );
		}
		return null;
	}
	else
	{
		return mapping.findForward( forward );
	}
}

}