package com.firstlook.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.servlet.http.HttpServletRequestHelper;
import biz.firstlook.commons.util.Functions;
import biz.firstlook.commons.util.PropertyLoader;

import com.discursive.cas.extend.client.CASFilter;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.presentation.HostTypeResolver;
import com.firstlook.presentation.UserSessionFilter;
import com.firstlook.service.dealer.IDealerService;
import com.firstlook.service.member.IMemberService;
import com.firstlook.session.FirstlookSession;
import com.firstlook.session.IFirstlookSessionManager;
import com.firstlook.session.Product;
import com.firstlook.session.ProductService;
import biz.firstlook.commons.util.BasicAuthAdapter;

public class LoginAction extends BaseAction {

	public static final String PARAM_DIRECT_TO = "directTo";
	public static final String PARAM_PRODUCT_MODE = "productMode";
	public static final String PARAM_DEALER_ID = "dealerId";

	static final String INSIGHT_LOGIN_FAILURE = "insightFailure";
	static final String VIP_LOGIN_FAILURE = "resourceFailure";
	static final String LOGIN_FAILURE_FORWARD = "loginFailure";
	static final String LOGIN_CHANGE_PASSWORD = "loginChangePassword";
	static final String INSIGHT_HOMEPAGE = "insightHomepage";

	private IDealerService dealerService;
	private IMemberService memberService;
	private IFirstlookSessionManager firstlookSessionManager;
	private ProductService productService;
	private String buildNumber;

	public String getBuildNumber() {
		return buildNumber;
	}

	public void setBuildNumber(String buildNumber) {
		this.buildNumber = buildNumber;
	}

	public ActionForward justDoIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws DatabaseException, ApplicationException {
		String host = request.getHeader("Host");
		String directTo = request.getParameter(PARAM_DIRECT_TO);
		String dealerId = request.getParameter(PARAM_DEALER_ID);
		String productMode = request.getParameter(PARAM_PRODUCT_MODE);

		// Do not create a new session, it should've been done already.
		HttpSession session = request.getSession(false);
		
		/// #23804 Adding this to the session so that the JSP pages can append this value to the asset urls
		//session.setAttribute("buildNumber", buildNumber );
		if ( this.buildNumber != null )
			session.getServletContext().setAttribute("buildNumber", buildNumber );
		String memberLogin = (String) session
				.getAttribute(CASFilter.CAS_FILTER_USER);

		// firstlookSession should never be null due to Filter handling of it.
		FirstlookSession firstlookSession = (FirstlookSession) session
				.getAttribute(FirstlookSession.FIRSTLOOK_SESSION);

		String forward = initializeDefaultRequestForward(host);

		Member authenticatedMember = getMemberService().retrieveMember(
				memberLogin);

		/// #27398 Create the BasicAuthAdapter
		BasicAuthAdapter baa = new BasicAuthAdapter(
				authenticatedMember.getLogin(), 
				authenticatedMember.getPassword());
		/// save it for later when we need to talk to BasicAuth services on behalf of this user
//		session.getServletContext().setAttribute(BasicAuthAdapter.NAME_IN_SESSION, baa.basicAuthString() );
//		session.setAttribute( BasicAuthAdapter.NAME_IN_SESSION, baa.basicAuthString() );

		// this is needed to keep you on https when hoping over to fladmin
		request.setAttribute( "scheme", HttpServletRequestHelper.getScheme(request) );

		// 3) forward user to appropriate page
		forward = findAccessLevelForward(host, authenticatedMember);
		forward = processLoginRedirection(request, authenticatedMember,
				firstlookSession, forward, directTo, dealerId, productMode);
		if (forward == LOGIN_FAILURE_FORWARD) {
			request.setAttribute("LoginMsg", PropertyLoader
					.getProperty("firstlook.login.nodealers"));
			forward = findForwardForFailure(host);
		} else if (forward.equalsIgnoreCase(LOGIN_CHANGE_PASSWORD)) {
			// Pass information onto change password pages
			// The other pages currently aren't functional with redirect.
			request.setAttribute(PARAM_DIRECT_TO, directTo);
			request.setAttribute(PARAM_DEALER_ID, dealerId);
			request.setAttribute(PARAM_PRODUCT_MODE, productMode);
		}

		if (forward.equalsIgnoreCase(directTo)) {
			return new ActionForward(directTo, false);
		}

		return mapping.findForward(forward);
	}

	private String processLoginRedirection(HttpServletRequest request,
			Member member, FirstlookSession firstlookSession, String forward,
			String directTo, String dealerId, String productMode)
			throws ApplicationException {
		try {
			if (member.getLoginStatus() == Member.MEMBER_LOGIN_STATUS_NEW) {
			    
			    String halSessionId = request.getParameter( UserSessionFilter.HAL_SESSION_ID );
				if ( halSessionId != null )
				{
					request.getSession(false).setAttribute( UserSessionFilter.HAL_SESSION_ID, halSessionId );
				}

				return LOGIN_CHANGE_PASSWORD;
			} else {
				if (StringUtils.isNotBlank(directTo)) {
					int dealerIdAsInt = Integer.parseInt(dealerId);

					if (member.isAssociatedWithDealer(dealerIdAsInt)) {
						if (firstlookSession != null) {
							getFirstlookSessionManager()
									.populateDealerUpgradesInFirstlookSession(
											dealerService.findUpgrades(
													dealerIdAsInt),
											firstlookSession);
							// getFirstlookSessionManager().processMemberSpecificRights(
							// member, firstlookSession );
							firstlookSession.setCurrentDealerId(dealerIdAsInt);

							Dealer dealer = dealerService.retrieveDealer(
									dealerIdAsInt);
							firstlookSession.getMember().setProgramType(
									dealer.getProgramTypeEnum().getName());
							firstlookSession.setProgramType(dealer
									.getProgramTypeEnum().getName());

							if (!Functions.isNullOrEmpty(productMode)) {
								Product product = productService
										.retrieveByName(productMode);
								if (product != null) {
									firstlookSession.setProduct(product);
								}
							}

							if (firstlookSession.getMode() == null) {
								// ensures that if the product mode has not been
								// set,
								// forward to the users homepage.
								directTo = forward;
							}

							request.getSession().setAttribute(
									FirstlookSession.FIRSTLOOK_SESSION,
									firstlookSession);
							return directTo;
						} else {
							// Something went wrong with FirstlookSession
							// creation,
							// go home.
							return forward;
						}
					} else {
						// Member is not part of the dealership. Send them home!
						return forward;
					}
				} else {
					// No redirect requested, send them to original home page.
					return forward;
				}
			}
		} catch (NumberFormatException nfe) {
			return forward;
		}
	}

	private String initializeDefaultRequestForward(String host) {
		if (HostTypeResolver.getInstance().isResourceHost(host)) {
			return VIP_LOGIN_FAILURE;
		} else {
			return INSIGHT_HOMEPAGE;
		}
	}

	private String findForwardForFailure(String host) {
		if (HostTypeResolver.getInstance().isResourceHost(host)) {
			return VIP_LOGIN_FAILURE;
		} else {
			return INSIGHT_LOGIN_FAILURE;
		}
	}

	private String findAccessLevelForward(String host, Member member)
			throws ApplicationException {

		String forwardName = LOGIN_FAILURE_FORWARD;

		// nk - mob - for acc Rep and User level, this should forward to Vin
		// entry
		switch (member.getMemberType()) {
		case Member.MEMBER_TYPE_ADMIN:
			forwardName = "adminHome";
			break;
		case Member.MEMBER_TYPE_ACCOUNT_REP:
			forwardName = "accountRepHome";
			break;
		case Member.MEMBER_TYPE_USER:
			forwardName = findUserLevelForward(host, member);
			break;
		default:
			forwardName = LOGIN_FAILURE_FORWARD;
			break;
		}

		return forwardName;
	}

	private String findUserLevelForward(String host, Member member)
			throws ApplicationException {
		if (member.belongsInADealership()) {
			if (HostTypeResolver.getInstance().isResourceHost(host)) {
				return "vipHome";
			} else {
				return "insightHome";
			}
		} else {
			return LOGIN_FAILURE_FORWARD;
		}
	}

	public IFirstlookSessionManager getFirstlookSessionManager() {
		return firstlookSessionManager;
	}

	public void setFirstlookSessionManager(
			IFirstlookSessionManager firstlookSessionManager) {
		this.firstlookSessionManager = firstlookSessionManager;
	}

	public IMemberService getMemberService() {
		return memberService;
	}

	public void setMemberService(IMemberService memeberService) {
		this.memberService = memeberService;
	}

	public void setImtDealerService(IDealerService dealerService) {
		this.dealerService = dealerService;
	}

	public void setProductService(ProductService productService) {
		this.productService = productService;
	}
}
