package com.firstlook.action.autocheck;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.services.vehiclehistoryreport.AutoCheckException;
import biz.firstlook.commons.services.vehiclehistoryreport.AutoCheckService;
import biz.firstlook.services.vehiclehistoryreport.autocheck.AutoCheckReportInspectionTO;
import biz.firstlook.services.vehiclehistoryreport.autocheck.AutoCheckReportTO;
import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;
import com.firstlook.entity.form.TradeAnalyzerForm;

public class AutoCheckReportAction extends SecureBaseAction {

	private static Logger logger = Logger.getLogger( AutoCheckReportAction.class );
	
	@Override
	public ActionForward doIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws DatabaseException, ApplicationException {
		String vin = request.getParameter( "vin" );
		Member member = getMemberFromRequest( request );
		int currentDealerId = getFirstlookSessionFromRequest(request).getCurrentDealerId();
		
		SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
		
		TradeAnalyzerForm tradeAnalyzerForm = (TradeAnalyzerForm) SessionHelper.getAttribute(request, "tradeAnalyzerForm");		
		
		try {

			AutoCheckService autoCheckService = AutoCheckService.getInstance();
			
			Map<String,String> properties = autoCheckService.getAutoCheckReportProperties(
					currentDealerId,
					member.getLogin(),
					vin);
			for (Map.Entry<String, String> entry : properties.entrySet()) {
				request.setAttribute(entry.getKey(), entry.getValue());
			}
			
			List<AutoCheckReportInspectionTO> autoCheckReportInspections = Collections.emptyList();
			
			AutoCheckReportTO autoCheckReportTO = autoCheckService.getAutoCheckReport(currentDealerId, member.getLogin(), vin);
			
			if (autoCheckReportTO != null){

				
				if (tradeAnalyzerForm != null)
				{
					tradeAnalyzerForm.setAutocheckReport(autoCheckReportTO);	
				}

				autoCheckReportInspections = autoCheckService.getAutoCheckReportInspectionTO(autoCheckReportTO);
			}
			
			request.setAttribute("autoCheckReportInspections", autoCheckReportInspections);

		} catch (AutoCheckException ace) {
			return mapping.findForward( "failure" );
		}		
		request.setAttribute("vin", vin);
		
		return mapping.findForward( "success" );
	}
}