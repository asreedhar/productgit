package com.firstlook.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.Functions;

import com.discursive.cas.extend.client.CASFilter;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.SoftwareSystemComponentState;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.softwaresystem.SoftwareSystemStateService;
import com.firstlook.util.ParameterActionForward;

public class RemoteEntryAction extends SecureBaseAction {

	public static final String FLAG_REMOTE_ENTRY = "FLAG_REMOTE_ENTRY";
	
	private SoftwareSystemStateService softwareSystemStateService;
	
	public ActionForward doIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws DatabaseException, ApplicationException {

		final String login = (String) request.getSession().getAttribute(CASFilter.CAS_FILTER_USER);
		
		final String token = SoftwareSystemStateService.DEALER_SYSTEM_COMPONENT;

		final ParameterActionForward forward = new ParameterActionForward(mapping.findForward("success"));
		
		forward.addParameter(LoginAction.PARAM_DIRECT_TO, "/DealerHomeDisplayAction.go");
		
		SoftwareSystemComponentState state = getSoftwareSystemStateService().findOrCreateSoftwareSystemComponentState(login, token);
		
		if (state != null) {
			forward.addParameter(LoginAction.PARAM_DEALER_ID, Functions.nullSafeToString(state.getDealer().getDealerId()));
		}
		
		final String productMode = request.getParameter(LoginAction.PARAM_PRODUCT_MODE);
		
		if (!Functions.isNullOrEmpty(productMode)) {
			forward.addParameter(LoginAction.PARAM_PRODUCT_MODE, productMode);
		}
		
		request.getSession().setAttribute(FLAG_REMOTE_ENTRY, Boolean.TRUE);
		
		return forward;
	}

	public SoftwareSystemStateService getSoftwareSystemStateService() {
		return softwareSystemStateService;
	}

	public void setSoftwareSystemStateService(SoftwareSystemStateService softwareSystemStateService) {
		this.softwareSystemStateService = softwareSystemStateService;
	}
	
}
