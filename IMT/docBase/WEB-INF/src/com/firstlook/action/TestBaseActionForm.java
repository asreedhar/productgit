package com.firstlook.action;

import java.util.Date;

public class TestBaseActionForm extends com.firstlook.mock.BaseTestCase
{

private BaseActionForm form;

public TestBaseActionForm( String arg1 )
{
	super( arg1 );
}

public void setup()
{
	form = new BaseActionForm();
}

public void testCheckIsNumericFalse()
{
	String invalidNumber = "ABC";
	assertEquals( "Invalid number.", false, BaseActionForm.checkIsNumeric( invalidNumber ) );
}

public void testGetIntValueMinValue()
{
	Integer value = new Integer( 10 );
	assertEquals( "Should return 10", 10, BaseActionForm.getIntValueMinValue( value ) );
}

public void testGetIntValueMinValueNull()
{
	Integer value = null;
	assertEquals( "Should return MIN_VAL", Integer.MIN_VALUE, BaseActionForm.getIntValueMinValue( value ) );
}

public void testCheckIsNumericTrue()
{
	String validNumber = "9999999999";
	assertEquals( "Valid number.", true, BaseActionForm.checkIsNumeric( validNumber ) );
}

public void testCheckIsNumericWithNull()
{
	String nullNumber = null;
	assertEquals( "Null number.", false, BaseActionForm.checkIsNumeric( nullNumber ) );
}

public void testConvertEmptyToNull()
{
	String loadedString = "loadedString";
	assertEquals( "loadedString", form.convertEmptyToNull( loadedString ) );
}

public void testConvertEmptyToNullEmptyString()
{
	String emptyString = " ";
	assertNull( "Should return null", form.convertEmptyToNull( emptyString ) );
}

public void testFormatPriceToString()
{
	assertEquals( "$1,000", form.formatPriceToString( 1000.0 ) );
}

public void testFormatPriceToStringWithDefault()
{
	assertEquals( "$1,000", form.formatPriceToString( 1000.0, "100" ) );
}

public void testFormatIntPriceToStringWithDefault()
{
	assertEquals( "$1,000", BaseActionForm.formatIntPriceToString( 1000, "100" ) );
}

public void testFormatIntPriceToStringNonZeroWithZero()
{
	assertEquals( "n/a", form.formatIntPriceToStringNonZero( new Integer( 0 ), "n/a" ) );
}

public void testFormatPriceToStringWithDefaultNAN()
{
	assertEquals( "100", form.formatPriceToString( Double.NaN, "100" ) );
}

public void testFormatPriceToStringNegative()
{
	assertEquals( "($1,000)", form.formatPriceToString( -1000.0 ) );
}

public void testGetDoubleFromString()
{
	assertEquals( 1.23, form.getDoubleFromString( "1.23" ), 0.001 );
}

public void testGetIntFromString()
{
	assertEquals( 1000, form.getIntFromString( "1000" ) );
}

public void testGetDoubleFromStringWithEmptyString()
{
	assertTrue( Double.isNaN( form.getDoubleFromString( "" ) ) );
}

public void testGetIntFromStringWithEmptyString()
{
	assertEquals( Integer.MIN_VALUE, form.getIntFromString( "" ) );
}

public void testGetValidStringNull()
{
	String test = form.getValidString( null );

	assertEquals( "", test );
}

public void testGetValidStringValidString()
{
	String test = form.getValidString( "hello" );

	assertEquals( "hello", test );
}

public void testIsNullOrEmptyFalse()
{
	String item = "text";
	assertTrue( !form.isNullOrEmpty( item ) );
}

public void testIsNullOrEmptyIsEmpty()
{
	String item = "";
	assertTrue( form.isNullOrEmpty( item ) );
}

public void testIsNullOrEmptyIsNull()
{
	assertTrue( form.isNullOrEmpty( null ) );
}

public void testIsNullOrEmptyOrWhiteSpace()
{
	String item = "  ";
	assertTrue( form.isNullOrEmptyOrWhiteSpace( item ) );
}

public void testRemoveComma()
{
	assertEquals( "String incorrect as non-formated string", "45678", form.removeComma( "45,678" ) );
}

public void testRemoveCommaWithOutComma()
{
	assertEquals( "String incorrect as non-formated string", "45678", form.removeComma( "45678" ) );
}

public void testTrimStringEmpty()
{
	String str = "";
	assertEquals( "", form.trimString( str, 3 ) );
}

public void testTrimStringLengthHigherThanIndex()
{
	String str = "ABCDEF";
	assertEquals( "ABC", form.trimString( str, 3 ) );

}

public void testTrimStringLengthLowerThanIndex()
{
	String str = "ABCDEF";
	assertEquals( "ABCDEF", form.trimString( str, 8 ) );

}

public void testTrimStringNull()
{
	String str = null;
	assertEquals( "", form.trimString( str, 3 ) );
}

public void testValidateIsNotNumeric()
{
	assertTrue( form.isNullOrEmpty( null ) );
}

public void testValidateStringLength()
{
	String testString = "333";
	int count = 3;
	boolean result = false;

	result = form.validateStringLength( testString, count );

	assertTrue( "result", result );
}

public void testValidateStringLengthTooLong()
{
	String testString = "12345678901";
	int count = 3;
	boolean result = false;

	result = form.validateStringLength( testString, count );

	assertTrue( "result", !result );
}

public void testFormatDateToString()
{
	Date date = new Date();
	date.setTime( 1000 );

	String expectedDate = "12/31/1969";

	assertEquals( "Date should be: ", expectedDate, BaseActionForm.formatDateToString( date ) );
}

public void testFormatDateToStringNullDate()
{
	Date date = null;

	String expectedDate = "&nbsp;";

	assertEquals( "Date should be: ", expectedDate, BaseActionForm.formatDateToString( date ) );
}

public void testFormatIntegerToIntValueNullInteger()
{
	Integer integer = null;

	assertEquals( "Should be 0", 0, form.formatIntegerToIntValue( integer ) );
}

public void testFormatIntegerToIntValueNotNullInteger()
{
	Integer integer = new Integer( 3 );

	assertEquals( "Should be 3", 3, form.formatIntegerToIntValue( integer ) );
}

}
