package com.firstlook.action;

import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.session.FirstlookSession;
import com.sun.mail.iap.ConnectionException;

public class ExceptionHandlerAction extends Action
{

@SuppressWarnings("unchecked")
public ActionForward execute( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
{
 	String forwardName = "default";
	StringBuffer errorMessage = new StringBuffer();

	Enumeration enumeration = request.getAttributeNames();
	while ( enumeration.hasMoreElements() )
	{
		String attrName = (String)enumeration.nextElement();
		Object attribute = request.getAttribute( attrName );

		if ( attrName.equalsIgnoreCase( "actionPath" ) )
		{
			errorMessage.append( attribute.toString() ).append( " " );
		}

		if ( attribute instanceof ConnectionException )
		{
			forwardName = "connection";
			break;
		}
		if ( attribute instanceof Throwable )
		{
			Throwable error = (Throwable)attribute;

			errorMessage.append( "ERROR: " ).append( error.getMessage() ).append( " TYPE: " ).append( attrName ).append( " " ).append(
																																		attribute.getClass().toString() );

//			Logger.getLogger( this.getClass() ).error( errorMessage, error );
			if ( attribute instanceof ServletException )
			{
				ServletException hasRoot = (ServletException)attribute;
				Throwable rootCause = hasRoot.getRootCause();
				if ( rootCause != null )
				{
					errorMessage.append( "ROOTCAUSE: " ).append( rootCause.getMessage() );
				}
			}
		}
	}

	// logging error, no need to create a new session
	HttpSession session = request.getSession( false );
	if ( session != null )
	{
		FirstlookSession firstlookSession = (FirstlookSession)session.getAttribute( FirstlookSession.FIRSTLOOK_SESSION );
		if (firstlookSession != null && firstlookSession.getMember() != null) {
			errorMessage.append( " MemberId: " ).append(  firstlookSession.getMember().getMemberId() );
		}
	}

//	logger.error( "Exception Context: " + " SystemErrorId: " + request.getAttribute("SystemErrorId") + " : " + errorMessage.toString() );

	// reset value - sometimes we get redirected through struts
	request.setAttribute("SystemErrorId", request.getAttribute("SystemErrorId"));
	return mapping.findForward( forwardName );
}

}
