package com.firstlook.action.auction.mmr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.BasicAuthAdapter;
import biz.firstlook.services.mmr.MMRServiceClient;
import biz.firstlook.services.mmr.entity.MMRArea;
import biz.firstlook.services.mmr.entity.MMRTrim;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;

public class MMRAuctionDataTileAction extends SecureBaseAction {

	@Override
	public ActionForward doIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws DatabaseException, ApplicationException {
		
		Integer year = Integer.valueOf( request.getParameter( "year" ) );
		

		String vin = request.getParameter( "vin" );
		String basicAuthString = (String) request.getSession().getServletContext().getAttribute(BasicAuthAdapter.NAME_IN_SESSION);
		//String basicAuthString="cHNhd2FudDpjNTU2MWMwN2RkNzYwNmNhMGYxNGQ2YmU1OTE2ZGQxZDQ2YjNkNmIz";
		
		
		MMRServiceClient mmrServiceClient;
		MMRArea[] areas= null;
		MMRTrim[] trims=null; 
		try {
			mmrServiceClient = new MMRServiceClient(basicAuthString);
			areas=mmrServiceClient.getMMRReference();
			
	        trims=mmrServiceClient.getMMRTrims(vin, areas[0].getId());		
	        
	        request.setAttribute("trimId", trims[0].getTrimId());
	        request.setAttribute("area", areas[0].getId());
	        request.setAttribute("mmrAreas", areas);
	        request.setAttribute("mmrTrims", trims);

		} catch (Exception e) {
			request.setAttribute("mmrErrorMsg", e.getMessage());
			e.printStackTrace();
		}
		
		
		request.setAttribute( "year", year );
		request.setAttribute( "vin", vin );

		
		
		
		
		return mapping.findForward("success");
	}

}
