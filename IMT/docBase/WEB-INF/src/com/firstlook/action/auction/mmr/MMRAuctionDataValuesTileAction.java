package com.firstlook.action.auction.mmr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.BasicAuthAdapter;
import biz.firstlook.services.mmr.MMRServiceClient;
import biz.firstlook.services.mmr.entity.MMRTransactions;
import biz.firstlook.services.mmr.entity.MMRTrim;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;

public class MMRAuctionDataValuesTileAction extends SecureBaseAction {
	String trimId;
	String area;
	String vin;
	String basicAuthString;
	MMRServiceClient mmrServiceClient;
	@Override
	public ActionForward doIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws DatabaseException, ApplicationException {
		
		basicAuthString = (String) request.getSession().getServletContext().getAttribute(BasicAuthAdapter.NAME_IN_SESSION);
		Integer year = Integer.valueOf( request.getParameter( "year" ) );
		trimId = request.getParameter( "trimId" );
		area=request.getParameter( "area" );
		vin= request.getParameter("vin");
		
		
		MMRServiceClient mmrServiceClient;
		MMRTransactions mmrTransactions=new MMRTransactions(); 
		try {
			mmrServiceClient = new MMRServiceClient(basicAuthString);
			mmrTransactions=mmrServiceClient.getMMRTransactions(trimId, year, area);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
		
		
		MMRTrim mmrTrim = getMmrTrimSelected();
		
		
		request.setAttribute("mmrTrim", mmrTrim);
		request.setAttribute( "mmrTransactions", mmrTransactions );
		return mapping.findForward("success");
	}

	public MMRTrim getMmrTrimSelected() {
		if (trimId!= null) {
			MMRTrim[] trims=null; 
			try {
				mmrServiceClient = new MMRServiceClient(basicAuthString);
				trims=mmrServiceClient.getMMRTrims(vin, area);
		        		

			} catch (Exception e) {
				
				e.printStackTrace();
			}
			for (MMRTrim trim : trims) {
				if (trim.getTrimId().equals(trimId)) {
					return trim;
				}
			}
		}
		return null;
	}

}
