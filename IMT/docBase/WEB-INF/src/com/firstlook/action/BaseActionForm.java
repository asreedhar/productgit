package com.firstlook.action;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

import org.apache.struts.action.ActionForm;

import biz.firstlook.commons.gson.GsonExclude;

import com.firstlook.util.Formatter;

@GsonExclude
public class BaseActionForm extends ActionForm
{
private static final long serialVersionUID = -6049186374688697578L;

public final static String BUTTON_DEALERGROUP_RESULTS = "showDealerGroup";
public final static String BUTTON_DEALER_RESULTS = "showDealer";
private int index;
private static final DecimalFormat priceFormatter = new DecimalFormat("#,##0");
public final static String UNKNOWN_VAL = "UNKNOWN";
private boolean blank = false;

public BaseActionForm()
{

}

public static boolean checkIsNumeric( String value )
{
    try
    {
        Long.parseLong(value);
        return true;
    } catch (NumberFormatException nfe)
    {
        return false;
    }
}

public static int getIntValueMinValue( Integer value )
{
    int returnValue = Integer.MIN_VALUE;

    if ( value != null )
    {
        returnValue = value.intValue();
    }

    return returnValue;
}

public String convertEmptyToNull( String s )
{
    if ( isNullOrEmptyOrWhiteSpace(s) )
    {
        return null;
    } else
    {
        return s;
    }
}

public static String formatDateToString( Date date )
{
    String dateFormat = "&nbsp;";

    if ( date != null )
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
        dateFormat = simpleDateFormat.format(date);
    }

    return dateFormat;

}

public String formatPriceToString( double price )
{
    String formattedPrice = "";
    if ( !Double.isNaN(price) )
    {
        formattedPrice = priceFormatter.format(price);
        if ( price < 0 )
        {
            formattedPrice = "($" + formattedPrice.substring(1) + ")";
        } else
        {
            formattedPrice = "$" + formattedPrice;
        }

    }
    return formattedPrice;
}

public String formatPriceToString( double price, String defaultValue )
{
    String formattedPrice = formatPriceToString(price);

    if ( formattedPrice.equals("") )
    {
        formattedPrice = defaultValue;
    }

    return formattedPrice;
}

public static String formatIntPriceToString( int price, String defaultValue )
{
    String formattedPrice = Formatter.toPrice(price);

    if ( formattedPrice.equals("") )
    {
        formattedPrice = defaultValue;
    }

    return formattedPrice;
}

public String formatIntPriceToStringNonZero( Integer price, String defaultValue )
{
    String formattedPrice = Formatter.toPrice(price);

    if ( formattedPrice.equals("") || formattedPrice.equals("$0") )
    {
        formattedPrice = defaultValue;
    }

    return formattedPrice;
}

public double getDoubleFromString( String value )
{
    double returnValue = 0.0;
    try
    {
        returnValue = Double.parseDouble(value);
    } catch (NumberFormatException nfe)
    {
        returnValue = Double.NaN;
    }

    return returnValue;
}

public int getIntFromString( String value )
{
    int returnValue = 0;
    try
    {
        returnValue = Integer.parseInt(value);
    } catch (NumberFormatException nfe)
    {
        returnValue = Integer.MIN_VALUE;
    }

    return returnValue;
}

public int getIndex()
{
    return index;
}

public int getIntegerMinValue()
{
    return Integer.MIN_VALUE;
}

protected String getValidString( String string )
{
    if ( string == null )
    {
        return "";
    } else
    {
        return string;
    }
}

public boolean isBlank()
{
    return blank;
}

public boolean isNullOrEmpty( String item )
{
    if ( item == null || item.equals("") )
    {
        return true;
    }
    return false;
}

public boolean isNullOrEmptyOrWhiteSpace( String item )
{
    if ( isNullOrEmpty(item) )
    {
        return true;
    } else
    {
        String s = item.trim();
        if ( s.length() == 0 )
        {
            return true;
        } else
        {
            return false;
        }
    }
}

public String removeComma( String s )
{
    StringTokenizer st = new StringTokenizer(s, ",");
    StringBuilder temp = new StringBuilder();
    while (st.hasMoreElements())
    {
        temp.append( st.nextElement() );
    }

    return temp.toString();
}

public void setBlank( boolean newBlank )
{
    blank = newBlank;
}

public void setBusinessObject( Object bo )
{
}

public void setIndex( int newIndex )
{
    index = newIndex;
}

public String trimString( String string, int length )
{
    if ( isNullOrEmptyOrWhiteSpace(string) )
    {
        return "";
    } else if ( string.length() < length )
    {
        return string;
    } else
    {
        return string.substring(0, length);
    }
}

protected boolean validateStringLength( String theString, int length )
{
    if ( theString.length() > length )
    {
        return false;
    } else
    {
        return true;
    }
}

public boolean isWhiteSpace( String item )
{
    if ( item != null )
    {
        String s = item.trim();
        if ( s.length() == 0 )
        {
            return true;
        } else
        {
            return false;
        }
    } else
    {
        return false;
    }
}

public int formatIntegerToIntValue( Integer integer )
{
    if ( integer == null )
    {
        return 0;
    } else
    {
        return integer.intValue();
    }
}

}
