package com.firstlook.action;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.servlet.http.HttpServletRequestHelper;
import biz.firstlook.commons.util.CookieHelper;
import biz.firstlook.commons.util.Functions;
import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.transact.persist.person.IPersonService;

import com.firstlook.action.dealer.redistribution.PrintableFlashReportDisplayAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.DealerForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.exception.MissingDealerException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.persistence.businessunit.IBusinessUnitRelationshipDAO;
import com.firstlook.persistence.dealer.IDealerDAO;
import com.firstlook.persistence.dealer.IDealerFactsDAO;
import com.firstlook.persistence.dealer.IDealerFranchiseDAO;
import com.firstlook.presentation.HostTypeResolver;
import com.firstlook.presentation.UserSessionFilter;
import com.firstlook.service.dealer.DealerFranchiseService;
import com.firstlook.service.dealer.IDealerService;
import com.firstlook.service.dealergroup.DealerGroupService;
import com.firstlook.session.FirstlookSession;
import com.firstlook.session.IFirstlookSessionManager;

public abstract class SecureBaseAction extends BaseAction
{

public final static String CURRENT_DEALER_ID_PARAMETER = "currentDealerId";
protected final static String THEME_ATTRIBUTE = "isMaxView";

protected final static String GOOGLE_ANALYTICS_ACCOUNT = "google.analytics.account";
protected final static String GOOGLE_ANALYTICS_ACCOUNT_NO = PropertyLoader.getProperty(GOOGLE_ANALYTICS_ACCOUNT);

private static final Logger logger = Logger.getLogger( SecureBaseAction.class );

private IDealerDAO dealerDAO;
private IDealerService dealerService;
private IPersonService personService;
private IBusinessUnitRelationshipDAO burDAO;
private IDealerFranchiseDAO dealerFranchiseDAO;
private IDealerFactsDAO dealerFactsDAO;
protected DealerGroupService dealerGroupService;
protected IFirstlookSessionManager firstlookSessionManager;
protected String buildNumber;

public SecureBaseAction() {
	super();
}

public String getBuildNumber() {
	return buildNumber; 
}

public void setBuildNumber(String buildNumber) {
	this.buildNumber = buildNumber;
}

public abstract ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException;

public final ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException{
	
	System.out.println("SecureBaseAction: justDoIt(): buildNumber:"+buildNumber);
	request.setAttribute("googleAnalyticsAccount",GOOGLE_ANALYTICS_ACCOUNT_NO); //setting the Google account number in the request in base class
	if(null == request.getSession().getServletContext().getAttribute("buildNumber") )
		request.getSession().getServletContext().setAttribute("buildNumber", buildNumber);
	try
	{
		setIsPrintableInRequest( request );
	}
	catch ( ApplicationException ae )
	{
		if ( !RequestHelper.getBoolean( request, "isFromMarketing" ) )
		{
			request.setAttribute( "LoginMsg", PropertyLoader.getProperty( "firstlook.login.timeout" ) );
		}
		logger.error( "SECURE BASE ACTION: Error thrown during authentication ---  User Session Likely Timed Out --- Redirecting to Login" );
		return mapping.findForward( "sessionTimeout" );
	}

	redirectVipWithEdge( request );
	processProductTheme( request );
	
	ActionForward actionForward = null;
	try {
		actionForward = doIt( mapping, form, request, response );
	}
	catch (MissingDealerException e) {
		removeDealerCookie(request, response);
		try {
			response.sendRedirect(response.encodeRedirectURL(loginURL(request)));
		}
		catch (IOException i) {
			throw new ApplicationException("Failed to issue redirect to login action", i);
		}
	}

	return actionForward;
}

/**
* put the flag for skin onto the request. Spring ThemeResolver does this better, but hack for now.
* 
* @param request
*/
private void processProductTheme( HttpServletRequest request )
{
	String halSessionId = getHalSessionId(request);
	if ( halSessionId == null ) {
		//try getting it from the request.
		halSessionId = request.getParameter( UserSessionFilter.HAL_SESSION_ID );
	}	
	request.setAttribute( THEME_ATTRIBUTE, Boolean.valueOf(halSessionId != null) );
}

private String loginURL(HttpServletRequest request) {
	String host = HttpServletRequestHelper.getHost(request);
	String scheme = HttpServletRequestHelper.getScheme(request);
	return new StringBuilder(scheme).append("://").append(host).append("/IMT/LoginAction.go").toString();
}

/**
 * 
 * @param forward
 * @param request
 * @return the original value of <code>forward</code> if the user is not using MAX.  
 * 		   If the user is using MAX, returns the forward name for the MAX page.
 */
protected String adjustMaxForward( String forward, HttpServletRequest request )
{
	StringBuilder theForward = new StringBuilder(forward);
	String halSessionId = getHalSessionId(request);
	if( halSessionId != null ) {
			theForward.append("MAX");
	}

	return theForward.toString();
}

/**
 * Safe to make this protected instead of private.
 * @param request
 * @return Returns the halSessionId if it exists, meaning that the user is using MAX and not Edge.
 */
private String getHalSessionId(HttpServletRequest request) {
	String halSessionId = null;
	HttpSession session = request.getSession( false );
	if (session != null) {
		halSessionId = (String)session.getAttribute( UserSessionFilter.HAL_SESSION_ID );
	}
	return halSessionId;
}

/**
 * sets flags onto the jsps for redirecting Vip stores with Edge upgrades.
 * 
 * @param request
 */
private void redirectVipWithEdge( HttpServletRequest request )
{
	// Nice big hack right here. Catches VIP stores with Edge upgrades and gives them a nice fat popup to change their login page.
	Boolean needRedirect = Boolean.FALSE;
	String edgeUrl = PropertyLoader.getProperty( "firstlook.vip.to.edge.url" );
	if ( HostTypeResolver.getInstance().isResourceHost( request.getHeader( "Host" ) ) )
	{
		FirstlookSession flSession = getFirstlookSessionFromRequest( request );
		if ( flSession.isHasUpgrades() )
		{
			needRedirect = Boolean.TRUE;
			Member member = flSession.getMember();

			// VIP user is trying to go to Edge from www.resourcevip.com
			// popup a dialog and tell them to redirect to resourcevip.firstlook.biz
			if ( member.getMemberType() != null && member.getMemberType().intValue() == Member.MEMBER_TYPE_ACCOUNT_REP )
			{
				edgeUrl = PropertyLoader.getProperty( "firstlook.resourcevip.firstlook.login.url" );
			}
			else
			{
				edgeUrl = PropertyLoader.getProperty( "firstlook.edge.url" );
			}
		}
	}
	request.setAttribute( "needRedirect", needRedirect );
	request.setAttribute( "edgeUrl", edgeUrl );
}

/**
 * Super legacy code. This method only throws an ApplicationException so that the child class can do the same.
 * 
 * @param request
 * @throws ApplicationException
 */
protected void setIsPrintableInRequest( HttpServletRequest request ) throws ApplicationException
{
	if ( this instanceof PrintableFlashReportDisplayAction )
		request.setAttribute( "isPrintable", "true" );
}

/**
 * The Dealer Nick Name is better handled by setting it on session with dealerId
 */
protected void putDealerNickNameInRequest( HttpServletRequest request )
{
	IDealerService dealerService = getImtDealerService();
	Dealer currentDealer = dealerService.retrieveDealer( getCurrentDealerId(request) );
	request.setAttribute( "dealerNickName", currentDealer.getNickname() );
}

/**
 * This method was made because MasterTemplate.jsp expects a DealerForm.
 * 
 * @see {@link #putDealerNickNameInRequest(HttpServletRequest)}
 */
protected Dealer putDealerFormInRequest( HttpServletRequest request )
{
	return putSpecificDealerFormInRequest( request, getCurrentDealerId(request) );
}

protected Dealer putSpecificDealerFormInRequest( HttpServletRequest request, int dealerId )
{
	if (dealerId == 0) {
		throw new MissingDealerException();
	}
	IDealerService dealerService = getImtDealerService();
	Dealer currentDealer = dealerService.retrieveDealer( dealerId );
	DealerForm dealerForm = new DealerForm( currentDealer );
	request.setAttribute( "dealerForm", dealerForm );
	return currentDealer;
}

protected Dealer putDealerFormInRequest( HttpServletRequest request, int weeks ) throws ApplicationException
{
	final int currentDealerId = getCurrentDealerId(request);
	IDealerService dealerService = getImtDealerService();
	Dealer currentDealer = dealerService.retrieveDealer( currentDealerId );
	DealerFranchiseService dfService = new DealerFranchiseService( getDealerFranchiseDAO() );
	DealerForm dealerForm = new DealerForm( currentDealer );

	dealerForm.setFranchises( dfService.retrieveByDealerIdWithSpring( currentDealer.getDealerId().intValue() ) );
	dealerForm.setUnitsSoldThreshold13Wks( currentDealer.getUnitsSoldThresholdByWeeks( weeks ) );

	DealerGroup dg = getDealerGroupService().retrieveByDealerId( currentDealer.getDealerId().intValue() );
	String name = "";
	if ( dg != null )
	{
		name = dg.getName();
	}
	request.setAttribute( "dealerGroupName", name );
	request.setAttribute( "dealerForm", dealerForm );
	request.setAttribute( "currentDealerId", currentDealer.getDealerId() );
	return currentDealer;
}

/**
 * BN - 10/19/2005 This is a semi-hack to get specific procedure inputs to log when either the 'buy' or 'view' links are triggered from the
 * buying alerts tile in the FLUSAN. This method is only used in ATCVehicleFramesetDisplayAction (extends SecureBaseAction) and
 * VehicleAndSellerDisplayAction (extends VehicleDisplayBaseAction->SecureBaseAction). SecureBaseAction was the highest level common superclass
 * for the 2 implementations, as such I present...
 */
protected Map<String, Object> extractProcedureInputsFromRequest( HttpServletRequest request )
{
	Map<String, Object> parameters = new HashMap<String, Object>();

	final int currentDealerId = getCurrentDealerId(request);

	// extract FL session from request to easily access user and bus. unit ID
	// values
	FirstlookSession session = getFirstlookSessionFromRequest( request );

	// populate HashMap for stored proc call
	parameters.put( "url", request.getParameter( "url" ) );

	try
	{
		if ( session.getMember() != null )
		{
			parameters.put( "memberId", session.getMember().getMemberId() );
		}

		parameters.put( "businessUnitId", new Integer( currentDealerId ) );

		String channelIdStr = (String)request.getParameter( "channelId" );
		if ( channelIdStr != null && !channelIdStr.equals( "" ) )
		{
			int channelIdInt = Integer.parseInt( channelIdStr );
			Integer channelIdInteger = new Integer( channelIdInt );
			parameters.put( "channelId", channelIdInteger );
		}
	}
	catch ( Exception e )
	{
		logger.error( "Unable to find User from session or channelId from request. " );
		logger.error( e );
	}

	parameters.put( "vin", request.getParameter( "firstLookVin" ) );

	return parameters;
}

protected int getCurrentDealerId(HttpServletRequest request) {
	final FirstlookSession firstlookSession = getFirstlookSessionFromRequest(request);
	if (firstlookSession == null) {
		throw new MissingDealerException();
	}
	final int currentDealerId = firstlookSession.getCurrentDealerId();
	if (currentDealerId == 0) {
		throw new MissingDealerException();
	}
	return currentDealerId;
}

public void setImtDealerService(IDealerService dealerService) {
	this.dealerService = dealerService;
}

public IDealerService getImtDealerService() {
	return dealerService;
}

public IDealerDAO getDealerDAO()
{
	warnIfNoDAO( dealerDAO, "dealerDAO" );
	return dealerDAO;
}

public void setDealerDAO( IDealerDAO dealerDAO )
{
	this.dealerDAO = dealerDAO;
}

public IBusinessUnitRelationshipDAO getBurDAO()
{
	warnIfNoDAO( burDAO, "burDAO" );
	return burDAO;
}

public void setBurDAO( IBusinessUnitRelationshipDAO burDAO )
{
	this.burDAO = burDAO;
}

public IDealerFranchiseDAO getDealerFranchiseDAO()
{
	warnIfNoDAO( dealerFranchiseDAO, "dealerFranchiseDAO" );
	return dealerFranchiseDAO;
}

public void setDealerFranchiseDAO( IDealerFranchiseDAO dealerFranchiseDAO )
{
	this.dealerFranchiseDAO = dealerFranchiseDAO;
}

public IDealerFactsDAO getDealerFactsDAO()
{
	warnIfNoDAO( dealerFactsDAO, "dealerFactsDAO" );
	return dealerFactsDAO;
}

public void setDealerFactsDAO( IDealerFactsDAO dealerFactsDAO )
{
	this.dealerFactsDAO = dealerFactsDAO;
}

protected void warnIfNoDAO( Object dao, String daoName )
{
	if ( dao == null )
		logger.warn( "********** DAO "
				+ daoName + " is being referenced but has not been set for class " + this.getClass().getName()
				+ ".\nPlease check action-servlet.xml." );
}

public IFirstlookSessionManager getFirstlookSessionManager()
{
	return firstlookSessionManager;
}

public void setFirstlookSessionManager( IFirstlookSessionManager firstlookSessionManager )
{
	this.firstlookSessionManager = firstlookSessionManager;
}

public DealerGroupService getDealerGroupService()
{
	return dealerGroupService;
}

public void setDealerGroupService( DealerGroupService dealerGroupService )
{
	this.dealerGroupService = dealerGroupService;
}

public IPersonService getPersonService()
{
	return personService;
}

public void setPersonService( IPersonService personService )
{
	this.personService = personService;
}

protected void addDealerCookie(HttpServletResponse response, int currentDealerId) {
    Cookie cookie = new Cookie("dealerId", Integer.toString(currentDealerId));
	cookie.setPath("/");
	cookie.setMaxAge(60 * 60 * 24); // single day cookie (value in seconds)
	response.addCookie(cookie);
	
	Dealer dealer = getDealerDAO().findByPk(currentDealerId);
	Cookie trueSightDealerCookie = CookieHelper.createTrueSightDealerCookie(dealer.getName());
	response.addCookie(trueSightDealerCookie);

	DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerId(currentDealerId);
	Cookie trueSightDealerGroupCookie = CookieHelper.createTrueSightDealerGroupCookie(dealerGroup.getName());
	response.addCookie(trueSightDealerGroupCookie);
}

protected void removeDealerCookie(HttpServletRequest request, HttpServletResponse response) {
	for (Cookie cookie : request.getCookies()) {
		if (Functions.nullSafeEquals("dealerId", cookie.getName())) {
			Cookie replace = new Cookie(cookie.getName(), cookie.getValue());
			replace.setPath("/");
			replace.setMaxAge(0);
			response.addCookie(replace);
		} else if (Functions.nullSafeEquals(CookieHelper.TRUESIGHT_DEALER_COOKIE_NAME, cookie.getName())
				|| Functions.nullSafeEquals(CookieHelper.TRUESIGHT_DEALERGROUP_COOKIE_NAME, cookie.getName()))
		{
			Cookie replace = new Cookie(cookie.getName(), cookie.getValue());
			replace.setPath("/");
			replace.setMaxAge(-1);
			response.addCookie(replace);
		}
	}
}

}