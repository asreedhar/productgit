package com.firstlook.action.dealer.redistribution;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.services.distribution.client.eleads.DistributorClient;
import biz.firstlook.services.distribution.client.eleads.DistributorSoap;
import biz.firstlook.services.distribution.client.eleads.types.ArrayOfBodyPart;
import biz.firstlook.services.distribution.client.eleads.types.Message;
import biz.firstlook.services.distribution.client.eleads.types.MessageBody;
import biz.firstlook.services.distribution.client.eleads.types.Price;
import biz.firstlook.services.distribution.client.eleads.types.PriceType;
import biz.firstlook.services.distribution.client.eleads.types.UserIdentity;
import biz.firstlook.services.distribution.client.eleads.types.Vehicle;
import biz.firstlook.services.distribution.client.eleads.types.VehicleType;
import biz.firstlook.transact.persist.model.JobTitle;
import biz.firstlook.transact.persist.model.LightDetails;
import biz.firstlook.transact.persist.service.appraisal.AppraisalActionType;
import biz.firstlook.transact.persist.service.appraisal.AppraisalFormOptions;
import biz.firstlook.transact.persist.service.appraisal.AppraisalValue;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;
import biz.firstlook.transact.persist.service.distribution.IDistributionAccountsDao;

import com.firstlook.action.dealer.tools.AbstractTradeAnalyzerFormAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.email.LithiaCarCenterAppraisalActionChangeEmailBuilder;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.IDealerRisk;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.TradeAnalyzerForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;
import com.firstlook.service.dealer.IUCBPPreferenceService;
import com.firstlook.service.risklevel.GroupingDescriptionLightService;

public class TradeManagerEditSubmitAction extends AbstractTradeAnalyzerFormAction
{

protected static Logger logger = Logger.getLogger( TradeManagerEditSubmitAction.class );	

private IAppraisalService appraisalService;
private IUCBPPreferenceService ucbpPreferenceService;
private GroupingDescriptionLightService groupingDescriptionLightService;
private IDistributionAccountsDao distributionAccountsDAO;

public IDistributionAccountsDao getDistributionAccountsDAO() {
	return distributionAccountsDAO;
}

public void setDistributionAccountsDAO(
		IDistributionAccountsDao distributionAccountsDAO) {
	this.distributionAccountsDAO = distributionAccountsDAO;
}

protected ActionForward analyzeIt(ActionMapping mapping, TradeAnalyzerForm tradeAnalyzerForm,	HttpServletRequest request, HttpServletResponse response, Dealer dealer)
	throws DatabaseException, ApplicationException
{
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );
	
	Integer currentDealerId = new Integer( getFirstlookSessionFromRequest( request ).getCurrentDealerId() );

	Integer appraisalId = tradeAnalyzerForm.getAppraisalId();
	if (appraisalId == null) {
		return mapping.findForward( "failure" );
	}
	
	IAppraisal appraisal = appraisalService.findBy( appraisalId );
	if (appraisal == null) {
		return mapping.findForward( "failure" );
	}
	
	// for lithia car center - if inventory decision has changed - sent an e-mail to car center users
	DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerId( currentDealerId );
	if( dealerGroup.isLithiaStore() ) {
		if( appraisal.getAppraisalActionType().getAppraisalActionTypeId().intValue() != tradeAnalyzerForm.getActionId()) {
			buildAndSendLithiaCarCenterAppraisalActionChangedEmail( currentDealerId, tradeAnalyzerForm,
			                                                        getMemberFromRequest( request ).getEmailAddress());
		}
	}
	
	// We need to be able to save appraisal bumps from here if necessary, so changing to true from false
	// I ran through TA and TM functionality for new vins and existing vins and don't see an evident problem
	// based on time constraints I'm going to assume this is ok.
	super.populateAppraisal( true, getMemberFromRequest( request ), tradeAnalyzerForm, appraisal );
	
	//more jam it in there hacking.  need to save customer offer on appraisal form.
	AppraisalFormOptions formOptions = appraisal.getAppraisalFormOptions();
	if( tradeAnalyzerForm.getOfferEditInputName() != null )
		formOptions.setAppraisalFormOffer( tradeAnalyzerForm.getOfferEditInputName() );
	
	appraisalService.updateAppraisal( appraisal );
	

	
	
	if(distributionAccountsDAO.isAccountActiveEleads(dealer.getDealerId())){
		//Distribution Service
		System.out.println("--------Calling Distribution Web Service---------");
		DistributorClient dc = new DistributorClient();
		DistributorSoap ds = dc.getDistributorSoap();
		
		Message message = new Message();
		
	
		
		biz.firstlook.services.distribution.client.eleads.types.Dealer dealer2 = new biz.firstlook.services.distribution.client.eleads.types.Dealer();
		
		
		dealer2.setId(dealer.getDealerId()); //dealer id needs to be set here
		
		Vehicle vehicle = new Vehicle();
		vehicle.setId(appraisal.getAppraisalId()); // vehicle id needs to be set here, if vehicle type also needs to be set then create VehicleType object
		vehicle.setVehicleType(VehicleType.APPRAISAL);
		
		ArrayOfBodyPart arrayOfBodyParts = new ArrayOfBodyPart();
		
		biz.firstlook.services.distribution.client.eleads.types.AppraisalValue appraisalValue= new biz.firstlook.services.distribution.client.eleads.types.AppraisalValue(); 
		appraisalValue.setValue(appraisal.getLatestAppraisalValue().getValue());
		arrayOfBodyParts.getBodyPart().add(appraisalValue);
		MessageBody messageBody = new MessageBody();
		messageBody.setParts(arrayOfBodyParts);
		
		
		message.setFrom(dealer2);
		message.setSubject(vehicle);
		message.setBody(messageBody);
		
		String username=(String)request.getSession().getAttribute("com.discursive.cas.extend.client.filter.user");
		
		UserIdentity userIdentity = new UserIdentity();
		userIdentity.setUserName(username);
		try{
			ds.distribute(message, userIdentity);
			System.out.println("--------Called Distribution Web Service----------");
		}
		catch(Exception e){
			e.printStackTrace();;
		}
	}

	//Distribution Web Service Call Ends
	
	
	
	

	IDealerRisk dealerRisk = ucbpPreferenceService.retrieveDealerRisk( currentDealerId.intValue() );

	int groupingId = appraisal.getVehicle().getMakeModelGrouping().getGroupingDescription().getGroupingDescriptionId().intValue();
	
	LightDetails lightDetails = groupingDescriptionLightService.retrieveSpecificVehicleLight( dealerRisk, currentDealerId.intValue(), groupingId,
																			tradeAnalyzerForm.getMileage().intValue(), tradeAnalyzerForm.getYear() );

	tradeAnalyzerForm.setHighMileageOrOlder( lightDetails.isHighMileage() || lightDetails.isOldCar() );
	
	String forward = "success"; 
	
	final String pageName = request.getParameter("pageName");
	if ( pageName != null && pageName.equalsIgnoreCase( "fromIMP" ) )
	{
		forward = "imp";
		request.setAttribute( "nextNavAction", "/NextGen/InventoryPlan.go" );
	}
	if(pageName.contains("AppraisalManager"))
	{
		return mapping.findForward(pageName);
	}
	return mapping.findForward( forward );
}

private void buildAndSendLithiaCarCenterAppraisalActionChangedEmail(int currentDealerId, TradeAnalyzerForm tradeAnalyzerForm,
                                                                    String memberEmailAddress ) {
	Dealer dealer = getDealerDAO().findByPk( currentDealerId );
	List<Member> carCenterMembers = getMemberService().getMembersByBusinessUnitAndJobTitle( currentDealerId, JobTitle.LITHIA_CAR_CENTER_ID );
	Member primaryContact = null;
	List<String> carCenterEmailAddresses = new ArrayList<String>();
	
	for(Member contact : carCenterMembers) {
		if ( contact.getMemberId().equals( dealer.getDealerPreference().getDefaultLithiaCarCenterMemberId() )) {
			primaryContact = contact;
		} else {
			carCenterEmailAddresses.add( contact.getEmailAddress() );
		}
	}
	
	if( primaryContact == null) {
		logger.error( "Dealer: " + currentDealerId + " does not have a DefaultLithiaCarCenterMember - no e-mail was sent!" );
	} else {
		LithiaCarCenterAppraisalActionChangeEmailBuilder emailBuilder = 
			new LithiaCarCenterAppraisalActionChangeEmailBuilder(dealer.getNickname(),  
	                        tradeAnalyzerForm.getYear(),
							tradeAnalyzerForm.getMake(),
							tradeAnalyzerForm.getModel(),
							tradeAnalyzerForm.getMileage(), 
							tradeAnalyzerForm.getAppraisalId(),
							primaryContact.getEmailAddress(),
							carCenterEmailAddresses,
							AppraisalActionType.getAppraisalActionType( tradeAnalyzerForm.getActionId() ).getDescription(),
							memberEmailAddress );
		
		getEmailService().sendEmail( emailBuilder );
	}
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

public void setUcbpPreferenceService( IUCBPPreferenceService ucbpPreferenceService )
{
	this.ucbpPreferenceService = ucbpPreferenceService;
}

public void setGroupingDescriptionLightService( GroupingDescriptionLightService groupingDescriptionLightService )
{
	this.groupingDescriptionLightService = groupingDescriptionLightService;
}

}