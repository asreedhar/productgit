package com.firstlook.action.dealer.buy;

import com.firstlook.data.mock.MockDatabase;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.ObjectMother;
import com.firstlook.persistence.dealergroup.IDealerGroupDAO;
import com.firstlook.persistence.dealergroup.MockDealerGroupDAO;
import com.firstlook.service.dealergroup.DealerGroupService;

public class TestVehicleDetailActionHelper extends BaseTestCase
{

private VehicleDetailActionHelper helper;
private Dealer sellingDealer;
public InventoryEntity vehicle;
private Dealer viewDealer;
private DealerGroupService dealerGroupService; 

public TestVehicleDetailActionHelper( String arg1 )
{
	super( arg1 );
}

public void setup()
{
	helper = new VehicleDetailActionHelper();

	mockDatabase = new MockDatabase();
	IDealerGroupDAO dealerGroupDAO = new MockDealerGroupDAO( mockDatabase );
	
	dealerGroupService = new DealerGroupService();
	dealerGroupService.setDealerGroupDAO( dealerGroupDAO );
	
	viewDealer = ObjectMother.createDealer();
	sellingDealer = ObjectMother.createDealer();

	viewDealer.setDealerId( new Integer( 1 ) );
	sellingDealer.setDealerId( new Integer( 2 ) );

	vehicle = ObjectMother.createInventory();
	vehicle.setDealer( sellingDealer );

	DealerGroup dealerGroup = ObjectMother.createDealerGroup();
	dealerGroup.setDealerGroupId( new Integer( 1 ) );
	mockDatabase.setReturnObjects( dealerGroup );
}

public void testSetDealerNameMaskedMemberIsAnAdmin() throws Exception
{
	DealerGroup dealerGroup1 = ObjectMother.createDealerGroup();
	dealerGroup1.setDealerGroupId( new Integer( 1 ) );
	mockDatabase.addReturnObject( dealerGroup1 );

	DealerGroup dealerGroup2 = ObjectMother.createDealerGroup();
	dealerGroup2.setDealerGroupId( new Integer( 2 ) );
	mockDatabase.addReturnObject( dealerGroup2 );

	Dealer currentDealer = ObjectMother.createDealer();
	currentDealer.setDealerId( new Integer( 2 ) );

	sellingDealer.setDealerType( Dealer.DEALER_TYPE_FIRSTLOOK );

	helper.setDealerNameMasking( currentDealer, sellingDealer, dealerGroupService );

	assertTrue( "Should not mask name.", !sellingDealer.isDealerNameMasked() );
}

public void testSetDealerNameMaskingDifferentDealerGroup() throws ApplicationException
{
	DealerGroup dealerGroup1 = ObjectMother.createDealerGroup();
	dealerGroup1.setDealerGroupId( new Integer( 1 ) );
	mockDatabase.addReturnObject( dealerGroup1 );

	DealerGroup dealerGroup2 = ObjectMother.createDealerGroup();
	dealerGroup2.setDealerGroupId( new Integer( 2 ) );
	mockDatabase.addReturnObject( dealerGroup2 );
	
	helper.setDealerNameMasking( viewDealer, sellingDealer, dealerGroupService );

	assertTrue( "Should be masked.", sellingDealer.isDealerNameMasked() );
}

public void testSetDealerNameMaskingFirstlookDealer() throws ApplicationException
{
	DealerGroup dealerGroup1 = ObjectMother.createDealerGroup();
	dealerGroup1.setDealerGroupId( new Integer( 1 ) );
	mockDatabase.addReturnObject( dealerGroup1 );

	DealerGroup dealerGroup2 = ObjectMother.createDealerGroup();
	dealerGroup2.setDealerGroupId( new Integer( 2 ) );
	mockDatabase.addReturnObject( dealerGroup2 );

	sellingDealer.setDealerType( Dealer.DEALER_TYPE_FIRSTLOOK );

	helper.setDealerNameMasking( viewDealer, sellingDealer, dealerGroupService );

	assertTrue( "Should not be masked.", !sellingDealer.isDealerNameMasked() );
}

public void testSetDealerNameMaskingInstitutionalDealer() throws ApplicationException
{
	DealerGroup dealerGroup1 = ObjectMother.createDealerGroup();
	dealerGroup1.setDealerGroupId( new Integer( 1 ) );
	mockDatabase.addReturnObject( dealerGroup1 );

	DealerGroup dealerGroup2 = ObjectMother.createDealerGroup();
	dealerGroup2.setDealerGroupId( new Integer( 2 ) );
	mockDatabase.addReturnObject( dealerGroup2 );

	sellingDealer.setDealerType( Dealer.DEALER_TYPE_INSTITUTIONAL );

	helper.setDealerNameMasking( viewDealer, sellingDealer, dealerGroupService );

	assertTrue( "Should not be masked.", !sellingDealer.isDealerNameMasked() );
}

public void testSetDealerNameMaskingSameDealerGroup() throws ApplicationException
{
	DealerGroup dealerGroup1 = ObjectMother.createDealerGroup();
	dealerGroup1.setDealerGroupId( new Integer( 1 ) );
	mockDatabase.addReturnObject( dealerGroup1 );

	DealerGroup dealerGroup2 = ObjectMother.createDealerGroup();
	dealerGroup2.setDealerGroupId( new Integer( 1 ) );
	mockDatabase.addReturnObject( dealerGroup2 );
	
	helper.setDealerNameMasking( viewDealer, sellingDealer, dealerGroupService );

	assertTrue( "Should not be masked.", !sellingDealer.isDealerNameMasked() );
}

}
