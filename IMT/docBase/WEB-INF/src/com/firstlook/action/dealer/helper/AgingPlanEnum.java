package com.firstlook.action.dealer.helper;

public enum AgingPlanEnum {

	OFF_THE_CLIFF(1, "OFF_THE_CLIFF"),
	AGGRESSIVELY_PURSUE_WHOLESALE_OPTIONS(2, "AGGRESSIVELY_PURSUE_WHOLESALE_OPTIONS"),
	FINAL_PUSH(2, "FINAL_PUSH"),
	PURSUE_AGGRESSIVE(2, "PURSUE_AGGRESSIVE"),
	LOW_RISK(2, "LOW_RISK"),
	HIGH_RISK_TRADE_IN(2, "HIGH_RISK_TRADE_IN"),
	HIGH_RISK(2, "HIGH_RISK"),
	WATCH_LIST(2, "WATCH_LIST"),
	REPORTS(2, "REPORTS");
	

	
	private int id;
	private String description;
	
	private  AgingPlanEnum(int id, String desc) {
		this.id = id;
		this.description = desc;
	}

	public int getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}
	
	
	
	
}
