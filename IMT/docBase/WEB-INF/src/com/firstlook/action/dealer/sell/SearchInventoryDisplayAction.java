package com.firstlook.action.dealer.sell;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.form.VehicleSearchForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.DealerHelper;
import com.firstlook.service.dealer.DealerFranchiseService;

public class SearchInventoryDisplayAction extends SelectVehiclesBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    DealerHelper.putCurrentDealerFormInRequest( request, getImtDealerService(),
                                                getDealerGroupService(),
                                                new DealerFranchiseService( getDealerFranchiseDAO() ) );
    request.setAttribute( "vehicleSearchForm", new VehicleSearchForm() );
    return mapping.findForward( "success" );
}


}
