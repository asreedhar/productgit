package com.firstlook.action.dealer.redistribution;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.dealer.buy.BaseBuyingGuideDisplayAction;
import com.firstlook.comparator.TradeAnalyzerPlusComparator;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.form.TradeAnalyzerPlusForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.iterator.VehicleFormIterator;
import com.firstlook.report.CustomIndicator;
import com.firstlook.service.dealer.IDealerService;

public class ShowroomDisplayAction extends BaseBuyingGuideDisplayAction
{

private IAppraisalService appraisalService;

protected Collection getVehiclesToDisplay( Dealer dealer, CustomIndicator indicator ) throws DatabaseException, ApplicationException
{
    List<IAppraisal> col = getAppraisalService().retrieveAppraisalUsingInGroupDemandDealerId( dealer.getDealerId().intValue(),
																						dealer.getDealerPreference().getShowroomDaysFilter().intValue() );
    return col;
}

protected void setDealersOnCollectionForSpring( Collection collection, IDealerService dealerService )
{
	throw new NotImplementedException();
}

public void putFormIteratorInRequest( HttpServletRequest request, Collection vehiclesToDisplay, CustomIndicator indicator )
        throws DatabaseException, ApplicationException
{
	Collections.sort( (List)vehiclesToDisplay, new TradeAnalyzerPlusComparator() );
	VehicleFormIterator vehicles = new VehicleFormIterator( vehiclesToDisplay, indicator, TradeAnalyzerPlusForm.class );
    request.setAttribute("submittedVehicles", vehicles);
}

public IAppraisalService getAppraisalService()
{
	return appraisalService;
}
public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

}