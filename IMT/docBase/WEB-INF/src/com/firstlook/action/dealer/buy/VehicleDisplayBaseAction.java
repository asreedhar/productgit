package com.firstlook.action.dealer.buy;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.persistence.InventoryBookOutDAO;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.form.InventoryForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.service.dealer.IDealerService;
import com.firstlook.service.inventory.InventoryService_Legacy;

public class VehicleDisplayBaseAction extends com.firstlook.action.SecureBaseAction
{

private InventoryService_Legacy inventoryService_Legacy;
private InventoryBookOutDAO inventoryBookOutDAO;

protected ActionForward getActionForward( ActionMapping mapping )
{
    return mapping.findForward( "success" );
}

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    putVehicleInRequest( request, getImtDealerService() );
    return getActionForward( mapping );
}

/**
 * Made for compatibility reasons.
 * @param request
 * @param dealerService
 * @return
 * @throws ApplicationException
 */
protected InventoryEntity putVehicleInRequest( HttpServletRequest request, IDealerService dealerService) throws ApplicationException
{
	int inventoryId = RequestHelper.getInt( request, "vehicleId" );
	return putVehicleInRequest( request, inventoryId, dealerService );
}

protected InventoryEntity putVehicleInRequest( HttpServletRequest request, Integer inventoryId, IDealerService dealerService) throws ApplicationException
{	
    InventoryEntity inventory = inventoryService_Legacy.retrieveInventory( inventoryId );
    Dealer owningDealer = dealerService.retrieveDealer( inventory.getDealerId() );
	Collection<ThirdPartyVehicleOption> options = 
		inventoryBookOutDAO.findSelectedOptionsOfAllTypesByInventoryId(
				inventory.getInventoryId(),
				owningDealer.getDealerPreference().getGuideBookIdAsInt() );

    InventoryForm vehicleForm = new InventoryForm( inventory );
    vehicleForm.setDealer( owningDealer );

    request.setAttribute( "selectedOptions", options );
    request.setAttribute( "vehicleForm", vehicleForm );
    return inventory;
}



public InventoryBookOutDAO getInventoryBookOutDAO()
{
	return inventoryBookOutDAO;
}

public void setInventoryBookOutDAO( InventoryBookOutDAO inventoryBookOutDAO )
{
	this.inventoryBookOutDAO = inventoryBookOutDAO;
}

public void setInventoryService_Legacy(InventoryService_Legacy inventoryService) {
	this.inventoryService_Legacy = inventoryService;
}



}
