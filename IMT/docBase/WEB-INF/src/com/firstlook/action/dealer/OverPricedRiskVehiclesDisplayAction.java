package com.firstlook.action.dealer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.retriever.OverPricingRiskCountRetriever;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;

public class OverPricedRiskVehiclesDisplayAction extends SecureBaseAction{

	private OverPricingRiskCountRetriever overPricingRiskCountRetriever;
	
	@Override
	public ActionForward doIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws DatabaseException, ApplicationException {
		int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();

		request.setAttribute( "dealerId", "" + currentDealerId );
		
		//set attribute
		Integer overpriceNum = null;
		try {
			overpriceNum = overPricingRiskCountRetriever.getOverPricingRiskCount(currentDealerId);
		} catch(DatabaseException de) {
			overpriceNum = Integer.valueOf(0);
		}
		request.setAttribute( "overpricedNum", overpriceNum);
		
		return mapping.findForward( "success" );
	}

	public void setOverPricingRiskCountRetriever(
			OverPricingRiskCountRetriever overPricingRiskCountRetriever) {
		this.overPricingRiskCountRetriever = overPricingRiskCountRetriever;
	}
}
