package com.firstlook.action.dealer.tools;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.BasicAuthAdapter;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.form.TradeAnalyzerForm;
import com.firstlook.exception.ApplicationException;


public class MMRTrimOnStep2Action extends AbstractTradeAnalyzerFormAction
{
	
private static final Logger logger = Logger.getLogger( MMRTrimOnStep2Action.class );

private IAppraisalService appraisalService;

public IAppraisalService getAppraisalService()
{
	return appraisalService;
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}	

public static Logger getLogger() {
	return logger;
}



protected ActionForward analyzeIt(ActionMapping mapping, TradeAnalyzerForm tradeAnalyzerForm,	HttpServletRequest request, HttpServletResponse response, Dealer dealer)
	throws DatabaseException, ApplicationException
{
	
	if(tradeAnalyzerForm.getAppraisalId()!=null){
		IAppraisal appraisal= getAppraisalService().findBy(tradeAnalyzerForm.getAppraisalId());
		
		if(appraisal!=null && tradeAnalyzerForm.getSelectedMmrVehicle()==null)
			tradeAnalyzerForm.setSelectedMmrVehicle(appraisal.getMmrVehicle());
	}
	if (tradeAnalyzerForm.getMmrTrims().length < 1)
	{
		String basicAuthString = (String) request.getSession().getServletContext().getAttribute(BasicAuthAdapter.NAME_IN_SESSION);
		
		tradeAnalyzerForm.populateMMRAreas(basicAuthString);
		tradeAnalyzerForm.populateMMRTrims(basicAuthString);
	}
	
	
	ActionForward result = mapping.findForward( "success" ); 
	request.setAttribute("tradeAnalyzerForm", tradeAnalyzerForm);
	return result; 
}

}