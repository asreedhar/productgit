package com.firstlook.action.dealer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.entity.Corporation;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.DealerGroupPreference;
import com.firstlook.entity.HomePageEnum;
import com.firstlook.entity.Member;
import com.firstlook.entity.ProgramTypeEnum;
import com.firstlook.entity.form.DealerForm;
import com.firstlook.entity.form.DealerGroupForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.iterator.ColumnedFormIterator;
import com.firstlook.persistence.corporation.ICorporationDAO;
import com.firstlook.persistence.dealergroup.IDealerGroupDAO;
import com.firstlook.service.corporation.CorporationService;
import com.firstlook.session.FirstlookSession;
import com.firstlook.session.Product;
import com.firstlook.util.ParameterActionForward;

public class DealerGroupHomeDisplayAction extends SecureBaseAction

{
private ICorporationDAO corpDAO;
private IDealerGroupDAO dealerGroupDAO;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws ApplicationException
{
	String forward = "";

	Member member = getMemberFromRequest(request);
	Collection<Dealer> dealers = member.getDealersCollection();
	if ( dealers.isEmpty() )
	{
		forward = "failure";
		// TODO reconcile this 'LoginMsg' with the other messages sent to handheld pages.
		request.setAttribute( "LoginMsg", "Internal Error: no dealers in collection." );
		return mapping.findForward(  forward );
	}

	List<Dealer> dealersList = new ArrayList<Dealer>( dealers );
	Collections.sort( dealersList, new Comparator<Dealer>(){
		public int compare(Dealer d1, Dealer d2) {
			String nick1 = d1.getNickname();
			if(StringUtils.isBlank(nick1)) {
				nick1 = d1.getName();
			}
			
			String nick2 = d2.getNickname();
			if(StringUtils.isBlank(nick2)) {
				nick2 = d2.getName();
			}
			return nick1.compareTo(nick2);
		}
	});
	
	ColumnedFormIterator dealerForms = new ColumnedFormIterator( 2, dealersList, DealerForm.class );
	request.setAttribute( "dealers", dealerForms );

	Dealer dealer = dealersList.get( 0 );

	if ( dealer == null )
	{
		forward = "failure";
		request.setAttribute( "LoginMsg", "Internal Error: dealer is null" );
		return mapping.findForward( forward );
	}

	if ( dealer.getDealerId() == null )
	{
		forward = "failure";
		request.setAttribute( "LoginMsg", "Internal Error: dealerId is null" );
		return mapping.findForward( forward );
	}

	int currentDealerId = dealer.getDealerId().intValue();

	DealerGroup group = getDealerGroupService().retrieveByDealerId( currentDealerId );
	if ( group == null )
	{
		forward = "failure";
		request.setAttribute( "LoginMsg", "Internal Error: dealer group is null." );
		return mapping.findForward( forward );
	}

	int dealerGroupId = group.getDealerGroupId().intValue();

	member.setDealerGroupId( dealerGroupId );

	DealerGroupForm dealerGroupForm = new DealerGroupForm();
	dealerGroupForm.setDealerGroup( group );
	CorporationService corpService = new CorporationService( getCorpDAO() );
	Collection<Corporation> corporationList = corpService.retrieveAllCorporations();
	dealerGroupForm.setCorporationList( corporationList );

	request.setAttribute( "dealerGroupForm", dealerGroupForm );
	request.setAttribute( "dealerGroupId", dealerGroupId ); //added dealerGroupId to page request for performance management popup use from dealer group HP 
		
	FirstlookSession firstlookSession = getFirstlookSessionFromRequest( request );
	if( member.isAssociatedWithDealer( currentDealerId )) {
		firstlookSession = getFirstlookSessionManager().constructFirstlookSession( currentDealerId, member );
		request.getSession( false ).setAttribute( FirstlookSession.FIRSTLOOK_SESSION, firstlookSession );
	}

	if ( member.hasMultipleDealerships() )
	{
		return findForwardForMultipleDealerships( mapping, firstlookSession, request );
	}
	else
	{
		return findForwardForSingleDealership( mapping, member, firstlookSession );
	}
}

private ActionForward findForwardForSingleDealership( ActionMapping mapping, Member member, FirstlookSession firstlookSession )
{
	String mappingName = null;
	String programType = member.getProgramType();
	if ( programType.equals( ProgramTypeEnum.INSIGHT.getName() ) )
	{
			mappingName = "insightOneDealer";
	}
    else if ( programType.equals( Member.LOGIN_ENTRY_POINT_FIRSTLOOK ) ) {
        firstlookSession.setProduct(Product.FIRSTLOOK);
        mappingName = "flOneDealer";
    }
    else if( programType.equalsIgnoreCase( Product.DEALERS_RESOURCES.getName() )) {
    	mappingName = "dealersResourcesOneDealer";
    }
	else
	{
			mappingName = "vipOneDealer";
	}

	return mapping.findForward(mappingName);
}

ActionForward findForwardForMultipleDealerships( ActionMapping mapping, FirstlookSession firstlookSession, HttpServletRequest request )
{
	DealerGroup dealerGroup = getDealerGroupDAO().findByDealerId(firstlookSession.getCurrentDealerId());
	DealerGroupPreference dealerGroupPreference = dealerGroup.getDealerGroupPreference();
	
	String mappingName = null;
	
	if ( firstlookSession.getMember().getLoginEntryPoint().equals( Member.LOGIN_ENTRY_POINT_VIP ) )
	{
		mappingName = "vipMultipleDealers";
	}
	else
	{
		if (dealerGroupPreference != null && dealerGroupPreference.isIncludePerformanceManagementCenter() && firstlookSession.getMember().getGroupHomePageId() == HomePageEnum.PMC) {
			mappingName = "performanceManagementCenter";
		}
		else {
			mappingName = adjustMaxForward( "multipleDealers", request);
		}
	}
	
	ActionForward forward = mapping.findForward(mappingName);
	
	if (mappingName.equals("performanceManagementCenter")) {
		ParameterActionForward modify = new ParameterActionForward(forward);
		modify.addParameter("dealerGroupId", dealerGroup.getDealerGroupId().toString());
		forward = modify;
	}
	
	return forward;
}

public ICorporationDAO getCorpDAO()
{
	return corpDAO;
}

public void setCorpDAO( ICorporationDAO corpDAO )
{
	this.corpDAO = corpDAO;
}

public IDealerGroupDAO getDealerGroupDAO() {
	return dealerGroupDAO;
}

public void setDealerGroupDAO(IDealerGroupDAO dealerGroupDAO) {
	this.dealerGroupDAO = dealerGroupDAO;
}

}