package com.firstlook.action.dealer.cia;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.cia.calculator.CIAGroupingItemDetailCalculator;
import biz.firstlook.cia.model.CIACategory;
import biz.firstlook.cia.model.CIAGroupingItem;
import biz.firstlook.cia.model.CIAGroupingItemDetailLevel;
import biz.firstlook.cia.model.CIAGroupingItemDetailType;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.service.cia.ICIAService;

public class CIAManagersChoiceSubmitAction extends SecureBaseAction
{

private ICIAService ciaService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	Map parameterMap = request.getParameterMap();
	String errorMessage;

	Integer ciaGroupingItemId = new Integer( RequestHelper.getInt( request, "ciaGroupingItemId" ) );
	Integer groupingDescriptionId = new Integer( RequestHelper.getInt( request, "groupingDescriptionId" ) );
	Integer ciaBodyTypeDetailId = new Integer( RequestHelper.getInt( request, "ciaBodyTypeDetailId" ) );
	try
	{
		String buyAmountStr = request.getParameter( "buyAmount" );
		int buyAmount = Integer.parseInt( buyAmountStr );
		CIAGroupingItem groupingItem = ciaService.constructCIAGroupingItemFromBuyAmountAndPreferences( ciaGroupingItemId,
																										groupingDescriptionId, parameterMap,
																										CIACategory.MANAGERS_CHOICE, buyAmount,
																										ciaBodyTypeDetailId );

		if ( groupingItem != null )
		{
			boolean wasYearSelected = determineIfYearWasSelected( groupingItem );
			if ( wasYearSelected )
			{
				if ( groupingItem.getCiaGroupingItemId().intValue() <= 0 )
				{
					groupingItem.setCiaGroupingItemId( null );
				}
				getCiaService().saveCIAGroupingItem( groupingItem );
			}
			else
			{
				errorMessage = "Error:  At least one year must be selected - Please re-enter";
				return putErrorInRequest( mapping, request, ciaGroupingItemId, groupingDescriptionId, ciaBodyTypeDetailId, errorMessage );
			}
		}
		return mapping.findForward( "success" );
	}
	catch ( NumberFormatException nfe )
	{
		errorMessage = "Error:  User-selected buy amount must be numerical - Please re-enter";
		return putErrorInRequest( mapping, request, ciaGroupingItemId, groupingDescriptionId, ciaBodyTypeDetailId, errorMessage );
	}

}

private ActionForward putErrorInRequest( ActionMapping mapping, HttpServletRequest request, Integer ciaGroupingItemId,
										Integer groupingDescriptionId, Integer ciaBodyTypeDetailId, String errorMessage )
{
	request.setAttribute( "error", errorMessage );
	request.setAttribute( "groupingDescriptionId", groupingDescriptionId );
	request.setAttribute( "ciaGroupingItemId", ciaGroupingItemId );
	request.setAttribute( "ciaBodyTypeDetailId", ciaBodyTypeDetailId );
	putActionErrorInRequest( request, errorMessage );
	return mapping.findForward( "failure-MC" );
}

private boolean determineIfYearWasSelected( CIAGroupingItem groupingItem )
{
	boolean wasYearSelected = false;
	List yearPreferences = CIAGroupingItemDetailCalculator.filterDetailsByTypeAndLevel( CIAGroupingItemDetailType.PREFER_TYPE,
																						CIAGroupingItemDetailLevel.YEAR_DETAIL_LEVEL,
																						groupingItem.getCiaGroupingItemDetails() );
	if ( yearPreferences != null && !yearPreferences.isEmpty() )
	{
		wasYearSelected = true;
	}
	return wasYearSelected;
}

public ICIAService getCiaService()
{
	return ciaService;
}

public void setCiaService( ICIAService ciaService )
{
	this.ciaService = ciaService;
}

}
