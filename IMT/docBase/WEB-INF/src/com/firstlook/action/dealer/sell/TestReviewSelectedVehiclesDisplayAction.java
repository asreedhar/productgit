package com.firstlook.action.dealer.sell;

import java.util.Vector;

import com.firstlook.entity.Dealer;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.DummyActionMapping;
import com.firstlook.mock.DummyHttpRequest;
import com.firstlook.mock.MockMember;
import com.firstlook.mock.ObjectMother;
import com.firstlook.persistence.dealer.DealerDAO;
import com.firstlook.service.dealer.DealerService;
import com.firstlook.service.dealer.IDealerService;

public class TestReviewSelectedVehiclesDisplayAction extends BaseTestCase
{

private MockMember member;
private DummyHttpRequest request;
private ReviewSelectedVehiclesDisplayAction action;
private DummyActionMapping mapping;
private Dealer dealer;
private IDealerService dealerService;

public TestReviewSelectedVehiclesDisplayAction( String arg1 )
{
    super(arg1);
}

public void setup()
{
    action = new ReviewSelectedVehiclesDisplayAction();

    dealerService = new DealerService( new DealerDAO() );
    dealer = ObjectMother.createDealer();
    dealer.setDealerId(new Integer(0));

    member = new MockMember();

    Vector vehicles = new Vector();
    vehicles.addElement(ObjectMother.createVehicle());
    vehicles.addElement(ObjectMother.createVehicle());

    request = new DummyHttpRequest();
    mapping = new DummyActionMapping();

    mockDatabase.setReturnObjects(vehicles);
}

public void testMappingForReviewSuccess() throws Exception
{
    assertEquals("reviewSuccess", action.getForwardMapping(request));
}

public void testMappingForSubmissionSuccess() throws Exception
{
    request.setParameter("saveSubmission.x", "9");
    assertEquals("submissionSuccess", action.getForwardMapping(request));
}

public void tearDown()
{
}

}
