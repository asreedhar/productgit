package com.firstlook.action.dealer.cia;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;

public class CIAViewDealsDisplayAction extends SecureBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response ) throws DatabaseException,
        ApplicationException
{	
	Dealer currentDealer = getImtDealerService().retrieveDealer( getFirstlookSessionFromRequest( request ).getCurrentDealerId() );
    request.setAttribute("nickname", currentDealer.getNickname());

    int groupingDescriptionId = RequestHelper.getInt(request,
            "groupingDescriptionId");
    int weeks = RequestHelper.getInt(request, "weeks");
    int mileage = RequestHelper.getInt(request, "mileage");

    request.setAttribute("weeks", new Integer(weeks));
    request.setAttribute("groupingDescriptionId", new Integer(
            groupingDescriptionId));
    request.setAttribute("mileage", new Integer(mileage));

    return mapping.findForward("success");
}

}
