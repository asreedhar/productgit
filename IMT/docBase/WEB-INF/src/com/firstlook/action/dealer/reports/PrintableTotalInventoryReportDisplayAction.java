package com.firstlook.action.dealer.reports;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.retriever.BookVsUnitCostSummaryDisplayBean;
import biz.firstlook.transact.persist.service.ThirdPartyCategoryService;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.util.PageBreakHelper;

public class PrintableTotalInventoryReportDisplayAction extends TotalInventoryReportDisplayAction
{

private ThirdPartyCategoryService thirdPartyCategoryService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	// TODO Auto-generated method stub
	putPageBreakHelperInRequest( PageBreakHelper.AGING_ONLINE, request, getMemberFromRequest( request ).getProgramType() );

	Dealer currentDealer = putDealerFormInRequest( request, 0 );

	createGuideBook( request, currentDealer );
	if ( currentDealer.getBookOutPreferenceSecondId() != 0 )
	{
		createSecondGuideBook( request, currentDealer );
	}
	else
	{
		request.setAttribute( "secondBookOut", Boolean.FALSE );
	}

	return super.doIt( mapping, form, request, response );
}

private void createGuideBook( HttpServletRequest request, Dealer dealer ) throws ApplicationException
{
	ThirdPartyCategory category = getThirdPartyCategoryService().retrieveById( new Integer( dealer.getBookOutPreferenceId() ) );

	if ( category != null )
	{
		request.setAttribute( "bookOutPreference", category.getCategory() );
		request.setAttribute( "guideBookName", category.getThirdPartyTitle() );
	}
	else
	{
		request.setAttribute( "bookOutPreference", "NA" );
		request.setAttribute( "guideBookName", "NA" );
	}

}

private void createSecondGuideBook( HttpServletRequest request, Dealer dealer ) throws ApplicationException
{
	ThirdPartyCategory category = getThirdPartyCategoryService().retrieveById( new Integer( dealer.getBookOutPreferenceSecondId() ) );
	String categoryName = category.getCategory();
	request.setAttribute( "bookOutPreferenceSecond", categoryName );
	request.setAttribute( "secondBookOut", Boolean.TRUE );

	BookVsUnitCostSummaryDisplayBean bookValues = inventoryService_Legacy.findBookVsUnitCostSummary( dealer.getDealerId().intValue(),
																									dealer.getBookOutPreferenceSecondId() );
	if ( bookValues != null )
	{
		request.setAttribute( "bookVsUnitCostTotalSecond", bookValues.getBookVsCost() );
		request.setAttribute( "bookTotalSecond", bookValues.getTotalInventoryBookCost() );
	}
	else
	{
		request.setAttribute( "bookVsUnitCostTotalSecond", new Integer( 0 ) );
		request.setAttribute( "bookTotalSecond", new Integer( 0 ) );
	}
}

public ThirdPartyCategoryService getThirdPartyCategoryService()
{
	return thirdPartyCategoryService;
}

public void setThirdPartyCategoryService( ThirdPartyCategoryService thirdPartyCategoryService )
{
	this.thirdPartyCategoryService = thirdPartyCategoryService;
}

}
