package com.firstlook.action.dealer.reports;

import com.firstlook.helper.action.ReportActionHelper;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.DummyHttpRequest;
import com.firstlook.mock.ObjectMother;
import com.firstlook.presentation.Perspective;

public class TestFullReportDisplayAction extends BaseTestCase
{

private DummyHttpRequest request;
private Perspective perspective;

public TestFullReportDisplayAction( String arg1 )
{
    super(arg1);
}

public void setup() throws Exception
{
    request = new DummyHttpRequest();
    perspective = ObjectMother.createPerspective();
    request.getSession(true).setAttribute("perspective", perspective);
}

public void testFailureMapping() throws Exception
{
    String forwardName = ReportActionHelper.getForwardName(request);
    assertEquals("error", forwardName);
}

public void testFastestSellerMapping() throws Exception
{
    request.setParameter("ReportType", "FASTESTSELLER");
    String forwardName = ReportActionHelper.getForwardName(request);
    assertEquals("fastestSellerFullReport", forwardName);
}

public void testMostProfitableMapping() throws Exception
{
    request.setParameter("ReportType", "MOSTPROFITABLE");
    String forwardName = ReportActionHelper.getForwardName(request);
    assertEquals("mostProfitableFullReport", forwardName);
}

public void testTopSellerMapping() throws Exception
{
    request.setParameter("ReportType", "TOPSELLER");
    String forwardName = ReportActionHelper.getForwardName(request);
    assertEquals("topSellerFullReport", forwardName);
}

}
