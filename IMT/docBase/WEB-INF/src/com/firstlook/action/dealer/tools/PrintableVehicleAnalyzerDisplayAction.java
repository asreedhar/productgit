package com.firstlook.action.dealer.tools;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.cia.service.BasisPeriodService;
import biz.firstlook.commons.services.insights.Insight;
import biz.firstlook.commons.services.insights.InsightParameters;
import biz.firstlook.commons.services.insights.InsightUtils;
import biz.firstlook.commons.services.insights.PerformanceAnalysisBean;
import biz.firstlook.transact.persist.model.CIAPreferences.TimePeriod;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.form.VehicleAnalyzerForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.action.ReportActionHelper;
import com.firstlook.service.dealer.IUCBPPreferenceService;
import com.firstlook.service.inventory.InventoryService_Legacy;
import com.firstlook.service.tools.VehicleAnalyzerService;
import com.firstlook.session.FirstlookSession;

public class PrintableVehicleAnalyzerDisplayAction extends SecureBaseAction
{

private BasisPeriodService basisPeriodService;
private InventoryService_Legacy inventoryService_Legacy;
private ReportActionHelper reportActionHelper;
private IUCBPPreferenceService ucbpPreferenceService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();

	int weeks = basisPeriodService.calculateNumberOfWeeksInBasisPeriodByType(
																				new Integer( getFirstlookSessionFromRequest( request ).getCurrentDealerId() ),
																				TimePeriod.SALES_HISTORY_DISPLAY, 0 );

	


	VehicleAnalyzerForm analyzerForm = (VehicleAnalyzerForm)form;

	if ( analyzerForm.getMake() != null )
	{
		String clickedButton = RequestHelper.getClickedButton( request );
		String groupMake = request.getParameter( "groupMake" );
		String groupModel = request.getParameter( "groupModel" );
		String groupTrim = request.getParameter( "groupTrim" );

		VehicleAnalyzerService service = new VehicleAnalyzerService();
		service.setDataOnForm( analyzerForm, clickedButton, groupMake, groupModel, groupTrim );

		InventoryEntity inventory = inventoryService_Legacy.createEmptyInventoryWithYearMileageMakeModel( null, 0, analyzerForm.getMake(),
																									analyzerForm.getModel() );

		ReportGroupingForms reportGroupingForms = reportActionHelper.createReportGroupingForms( analyzerForm.getMake(),
																								analyzerForm.getModel(),
																								analyzerForm.getTrim(),
																								analyzerForm.getIncludeDealerGroup(),
																								inventory, InventoryEntity.USED_CAR, weeks,
																								currentDealerId );

		List<Insight> insights = InsightUtils.getInsights(new InsightParameters(currentDealerId, inventory.getGroupingDescriptionId(), InventoryEntity.USED_CAR, weeks, analyzerForm.getTrim(), null, null));

		reportGroupingForms.setPerformanceAnalysisBean(new PerformanceAnalysisBean(insights));

		request.setAttribute( "specificReport", reportGroupingForms.getSpecificReportGrouping() );
		request.setAttribute( "generalReport", reportGroupingForms.getGeneralReportGrouping() );
		request.setAttribute( "performanceAnalysisItem", reportGroupingForms.getPerformanceAnalysisBean() );
		request.setAttribute( "analyzerForm", analyzerForm );
		request.setAttribute( "dataPresent", new Boolean( true ) );
	}
	else
	{
		request.setAttribute( "dataPresent", new Boolean( false ) );
	}
	request.setAttribute( "fromCIA", new Boolean( isFromCIA( request ) ) );
	putDealerFormInRequest( request, 0 );

	Integer mileage = new Integer( ucbpPreferenceService.retrieveDealerRisk(
																( (FirstlookSession)request.getSession().getAttribute(
																														FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId() ).getHighMileageThreshold());
	request.setAttribute( "mileage", mileage );
	request.setAttribute( "weeks", new Integer( weeks ) );

	return mapping.findForward( "success" );
}

private boolean isFromCIA( HttpServletRequest request )
{
	if ( request.getParameter( "fromCIA" ) != null )
	{
		return true;
	}
	else
	{
		return false;
	}
}

public void setBasisPeriodService( BasisPeriodService basisPeriodService )
{
	this.basisPeriodService = basisPeriodService;
}

public void setInventoryService_Legacy( InventoryService_Legacy inventoryService )
{
	this.inventoryService_Legacy = inventoryService;
}

public void setReportActionHelper( ReportActionHelper reportActionHelper )
{
	this.reportActionHelper = reportActionHelper;
}

public void setUcbpPreferenceService( IUCBPPreferenceService ucbpPreferenceService )
{
	this.ucbpPreferenceService = ucbpPreferenceService;
}

}