package com.firstlook.action.dealer.buy;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.dealergroup.DealerGroupService;

public class VehicleDetailActionHelper
{

public VehicleDetailActionHelper()
{
	super();
}

void setDealerNameMasking( Dealer viewingDealer, Dealer sellingDealer, DealerGroupService dgService ) throws ApplicationException
{
	DealerGroup vgroup = dgService.retrieveByDealerId( viewingDealer.getDealerId().intValue() );
	DealerGroup sgroup = dgService.retrieveByDealerId( sellingDealer.getDealerId().intValue() );

	if ( sellingDealer.getDealerType() == Dealer.DEALER_TYPE_FIRSTLOOK || sellingDealer.getDealerType() == Dealer.DEALER_TYPE_INSTITUTIONAL )
	{
		sellingDealer.setDealerNameMasked( false );
	}
	else if ( vgroup.getDealerGroupId().intValue() == sgroup.getDealerGroupId().intValue() )
	{
		sellingDealer.setDealerNameMasked( false );
	}
	else
	{
		sellingDealer.setDealerNameMasked( true );
	}
}

void setDealerNameMasking( Dealer sellingDealer, boolean isAdmin, Dealer currentDealer, DealerGroupService dgService ) throws com.firstlook.exception.ApplicationException
{
	if ( isAdmin )
	{
		sellingDealer.setDealerNameMasked( false );
	}
	else
	{
		setDealerNameMasking( currentDealer, sellingDealer, dgService );
	}
}

}
