package com.firstlook.action.dealer.tools;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.comparator.SaleComparator;
import com.firstlook.comparator.service.BaseComparatorService;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.SaleForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.report.DealLogReportRetriever;
import com.firstlook.service.tools.DealLogService;
import com.firstlook.service.tools.GrossProfitTotals;
import com.firstlook.session.FirstlookSession;
import com.firstlook.util.PageBreakHelper;

public class DealLogExportAction extends SecureBaseAction{
	
	private DealLogReportRetriever dealLogReportRetriever; 
	private String DEFAULT_ORDER = "dealDate";
	private String SALE_TYPE_RETAIL ="retail";
	private String SALE_TYPE_WHOLESALE ="wholesale";
	
	protected static Logger logger = Logger.getLogger(DealLogExportAction.class);
	
	DecimalFormat df = new DecimalFormat("#.##");
	
	@SuppressWarnings("unchecked")
	public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
			throws DatabaseException, ApplicationException
	{
		int currentDealerId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();
		Member member = getMemberFromRequest( request );
		int inventoryType = member.getInventoryType().getValue();
		DealLogService dealLogService = new DealLogService( currentDealerId, inventoryType);
		int weeks = dealLogService.determineWeeks( request.getParameter( "weeks" ) );
		request.setAttribute( "weeks", new Integer( weeks ) );

		BaseComparatorService baseComparatorService = new BaseComparatorService( DEFAULT_ORDER );
		String orderBy = baseComparatorService.retrieveOrderBy( request.getParameter( "orderBy" ) );

		request.setAttribute( "orderBy", orderBy );
		Date startDate = dealLogService.generateDateFromWeeks( weeks );
		List<SaleForm> saleFormsforRetail=new ArrayList<SaleForm>();
		List<SaleForm> saleFormsforWholesale=new ArrayList<SaleForm>();
		
		saleFormsforRetail = retrieveDeals( currentDealerId, startDate, orderBy, SALE_TYPE_RETAIL, inventoryType );
		request.setAttribute( "saleForms", saleFormsforRetail );
		
		saleFormsforWholesale = retrieveDeals( currentDealerId, startDate, orderBy, SALE_TYPE_WHOLESALE, inventoryType );
		request.setAttribute( "saleForms", saleFormsforWholesale );
	
		//Calculating gross total for retail
		GrossProfitTotals grossProfitTotalsforRetail = dealLogService.sumGrossProfit( saleFormsforRetail );
		request.setAttribute( "totalFrontEndGrossProfitRetail", new Double( grossProfitTotalsforRetail.getTotalFrontEndGrossProfit() ) );
		request.setAttribute( "totalBackEndGrossProfitRetail", new Double( grossProfitTotalsforRetail.getTotalBackEndGrossProfit() ) );

		//Calculating gross total for Wholesale
		GrossProfitTotals grossProfitTotalsforWholesale = dealLogService.sumGrossProfit( saleFormsforWholesale );
		request.setAttribute( "totalFrontEndGrossProfitWholesale", new Double( grossProfitTotalsforWholesale.getTotalFrontEndGrossProfit() ) );
		request.setAttribute( "totalBackEndGrossProfitWholesale", new Double( grossProfitTotalsforWholesale.getTotalBackEndGrossProfit() ) );
		
		putDealerFormInRequest( request, 0 );
		Dealer currentDealer = getImtDealerService().retrieveDealer( currentDealerId );

		request.setAttribute( "nickname", currentDealer.getNickname() );
		request.setAttribute("showLogo", true);

		putPageBreakHelperInRequest( PageBreakHelper.DEAL_LOG, request, getMemberFromRequest( request ).getProgramType() );
		
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFWorkbook workbook = generateDealLogReport(request, saleFormsforRetail, SALE_TYPE_RETAIL, wb );
		HSSFWorkbook finalWorkbook = generateDealLogReport(request, saleFormsforWholesale, SALE_TYPE_WHOLESALE, workbook);
		
     	 response.setContentType("application/vnd.ms-excel");
		 response.setHeader("Content-Disposition", "attachment; filename = DealLogreport.xls");
		 ServletOutputStream out;
		 try {
			  out = response.getOutputStream();
			  finalWorkbook.write(out); 
			  out.flush(); 
			  out.close();
		}
		catch (IOException excelError) 
		{
			logger.error("Failed to create excel" + excelError.getMessage());
		} 

		 return null;	// no forward for this action
	}

	private HSSFWorkbook generateDealLogReport(HttpServletRequest Request,List<SaleForm> saleForms, String saleType, HSSFWorkbook book){
		
		String sheetName;
		Double totalFrontEndGrossProfit;
		Double totalBackEndGrossProfit;
		
		if(saleType.equals("retail")){
			sheetName = "Retail";
			totalFrontEndGrossProfit = (Double) Request.getAttribute( "totalFrontEndGrossProfitRetail");
			totalBackEndGrossProfit =  (Double) Request.getAttribute( "totalBackEndGrossProfitRetail");
		}
		else {
			sheetName ="Wholesale";
			totalFrontEndGrossProfit = (Double) Request.getAttribute( "totalFrontEndGrossProfitWholesale");
			totalBackEndGrossProfit =  (Double) Request.getAttribute( "totalBackEndGrossProfitWholesale");
		}

		HSSFSheet hssfSheet = book.createSheet(sheetName);
		HSSFRow hrow = hssfSheet.createRow(0);
		HSSFCell firstCell = hrow.createCell(0);
		firstCell.setCellValue("Deal Date");
		HSSFCell secondCell = hrow.createCell(1);
		secondCell.setCellValue("Year/Make/Model/Trim/Body Style");
		HSSFCell thirdCell = hrow.createCell(2);
		thirdCell.setCellValue("Color");
		HSSFCell fourCell = hrow.createCell(3);
		fourCell.setCellValue("Deal Number");
		HSSFCell fifthCell = hrow.createCell(4);
		fifthCell.setCellValue("Stock Number");
		HSSFCell sixthCell = hrow.createCell(5);
		sixthCell.setCellValue("Days To Sale");
		HSSFCell seventhCell = hrow.createCell(6);
		if(saleType.equals("retail"))
			seventhCell.setCellValue("Retail Gross Profit");
		else
			seventhCell.setCellValue("Wholesale Gross Profit");
		if(saleType.equals("retail")){
			HSSFCell eighthCell = hrow.createCell(7);
			eighthCell.setCellValue("F&I Gross Profit");
			HSSFCell ninthCell = hrow.createCell(8);
			ninthCell.setCellValue("Sale Price");
			HSSFCell tenCell = hrow.createCell(9);
			tenCell.setCellValue("Unit Cost");
			HSSFCell eleventhCell = hrow.createCell(10);
			eleventhCell.setCellValue("T/P");
			HSSFCell twelfthCell = hrow.createCell(11);
			twelfthCell.setCellValue("Mileage");
			HSSFCell thirteenthCell = hrow.createCell(12);
			thirteenthCell.setCellValue("Initial Light");
		}
		else
		{
			HSSFCell ninthCell = hrow.createCell(7);
			ninthCell.setCellValue("Sale Price");
			HSSFCell tenCell = hrow.createCell(8);
			tenCell.setCellValue("Unit Cost");
			HSSFCell eleventhCell = hrow.createCell(9);
			eleventhCell.setCellValue("T/P");
			HSSFCell twelfthCell = hrow.createCell(10);
			twelfthCell.setCellValue("Mileage");
			HSSFCell thirteenthCell = hrow.createCell(11);
			thirteenthCell.setCellValue("Initial Light");
		}
	
		if(!saleForms.isEmpty())
		{
		 int rowNumber = 1;
		 
		  for (SaleForm s : saleForms)
		  {
		 
		 HSSFRow nextrow = hssfSheet.createRow(rowNumber);
		 try {
			nextrow.createCell(0).setCellValue(formatDate(s.getDealDate().toString(), "MM/dd/yy", "EEE MMM dd hh:mm:ss z yyyy"));
		} catch (ParseException e) {
			logger.error("Date format error" + e.getMessage());
		}
		 nextrow.createCell(1).setCellValue(s.getYear()+" "+s.getMake()+" "+s.getModel()+" "+s.getTrim()+" "+s.getBodyStyle());     
		 nextrow.createCell(2).setCellValue(s.getBaseColor());
		 nextrow.createCell(3).setCellValue(s.getDealNumber());
		 nextrow.createCell(4).setCellValue(s.getStockNumber());
		 nextrow.createCell(5).setCellValue(s.getDaysToSale());
		 nextrow.createCell(6).setCellValue(s.getRetailGrossProfit());
		 if(saleType.equals("retail")){
		 nextrow.createCell(7).setCellValue(s.getFIGrossProfit());
		 nextrow.createCell(8).setCellValue("$"+s.getSalePrice());
		 nextrow.createCell(9).setCellValue("$"+s.getUnitCost());
		 nextrow.createCell(10).setCellValue(s.getTradeOrPurchase()== 1 ? "Purchase" : "Trade"); 
		 nextrow.createCell(11).setCellValue(s.getMileage());
		if(s.getLight()==1)
			 {
				 nextrow.createCell(12).setCellValue("Red");
			 }
		if(s.getLight()==2)
			 {
				 nextrow.createCell(12).setCellValue("Yellow");
			 }
		if(s.getLight()==3)
			 {
				 nextrow.createCell(12).setCellValue("Green");
			 }
		 }
		 else
		 {
		 nextrow.createCell(7).setCellValue("$"+s.getSalePrice());
		 nextrow.createCell(8).setCellValue("$"+s.getUnitCost());
		 nextrow.createCell(9).setCellValue(s.getTradeOrPurchase() == 1 ? "Purchase" : "Trade"); 
		 nextrow.createCell(10).setCellValue(s.getMileage());
		 if(s.getLight()==1)
			 {
			 nextrow.createCell(11).setCellValue("Red");
			 }
		 if(s.getLight()==2)
			 {
			 nextrow.createCell(11).setCellValue("Yellow");
			 }
		 if(s.getLight()==3)
		 	{
			 nextrow.createCell(11).setCellValue("Green");
		 	}
		 }
		 
		 rowNumber++;
		}
		  HSSFRow totalRow = hssfSheet.createRow(rowNumber+1);
		  totalRow.createCell(0).setCellValue("");
		  totalRow.createCell(1).setCellValue("");      
		  totalRow.createCell(2).setCellValue("");
		  totalRow.createCell(3).setCellValue("");
		  totalRow.createCell(4).setCellValue("");
		  totalRow.createCell(5).setCellValue("Total :");
		  if(saleType.equals("retail"))
		  {
		  totalRow.createCell(6).setCellValue("$"+df.format(totalFrontEndGrossProfit));
		  totalRow.createCell(7).setCellValue("$"+df.format(totalBackEndGrossProfit));
		  }
		  else
		  {
			  totalRow.createCell(6).setCellValue("$"+df.format(totalFrontEndGrossProfit));
		  }
		}
		return book;
	}
	
	 private String formatDate(String myDate, String returnFormat, String myFormat) throws java.text.ParseException
     {
         DateFormat dateFormat = new SimpleDateFormat(returnFormat);
         Date date=null;
         String returnValue="";
         date = (Date) new SimpleDateFormat(myFormat, Locale.ENGLISH).parse(myDate);
         returnValue = dateFormat.format(date);

     return returnValue;
     }
		
	//fetch line items for different time windows
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private List retrieveDeals( int dealerId, Date startDate, String orderBy, String saleType, int inventoryType ) throws ApplicationException
	{
		List saleForms = dealLogReportRetriever.retrieveDealLogLineItems( dealerId, new Timestamp( startDate.getTime() ), saleType.substring( 0, 1 ), inventoryType );// dealLogService.retrieveWholeSaleDeals(weeks);
		Collections.sort( saleForms, new SaleComparator( orderBy ) );
		return saleForms;
	}

	public void setDealLogReportRetriever(
			DealLogReportRetriever dealLogReportRetriever) {
		this.dealLogReportRetriever = dealLogReportRetriever;
	}

}
