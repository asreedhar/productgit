package com.firstlook.action.dealer.buy;

import org.apache.struts.action.ActionForward;

import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.DummyActionMapping;
import com.firstlook.mock.ObjectMother;

public class TestVehicleDisplayBaseAction extends BaseTestCase
{

private VehicleDisplayBaseAction action;
private com.firstlook.entity.InventoryEntity vehicle;

public TestVehicleDisplayBaseAction( String arg1 )
{
    super(arg1);
}

public void setup()
{
    action = new VehicleAndSellerDisplayAction();
    vehicle = ObjectMother.createInventory();
}

public void testGetActionForward()
{
    VehicleDisplayBaseAction action = new VehicleDisplayBaseAction();
    DummyActionMapping mapping = new DummyActionMapping();
    ActionForward forward = action.getActionForward(mapping);
    assertEquals("success", forward.getName());
}

}
