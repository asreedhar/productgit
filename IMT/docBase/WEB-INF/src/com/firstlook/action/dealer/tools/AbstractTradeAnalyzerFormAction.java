package com.firstlook.action.dealer.tools;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.ColorUtility;
import biz.firstlook.transact.persist.model.InventoryTypeEnum;
import biz.firstlook.transact.persist.model.MMRVehicle;
import biz.firstlook.transact.persist.person.IPerson;
import biz.firstlook.transact.persist.service.appraisal.AppraisalActionType;
import biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum;
import biz.firstlook.transact.persist.service.appraisal.AppraisalValue;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.TradeAnalyzerForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;

public abstract class AbstractTradeAnalyzerFormAction extends AbstractTradeAnalyzerAction {

/**
 * This Action should evolve into a template like pattern for the child classes.
 */
@Override
public final ActionForward doIt(ActionMapping mapping, ActionForm form,	HttpServletRequest request, HttpServletResponse response)
		throws DatabaseException, ApplicationException 
{
	//some child classes need the DealerForm in request for dealer name, dealer id, dealergroup name.
	Dealer dealer = super.putDealerFormInRequest(request, 0);
	
	TradeAnalyzerForm tradeAnalyzerForm = (TradeAnalyzerForm)form;
	
	request.setAttribute("selectedColor", tradeAnalyzerForm.getColor());
	request.setAttribute("colors", ColorUtility.retrieveStandardColors() );
	request.setAttribute( "appraisalValue", tradeAnalyzerForm.getAppraisalValue() );
	
	DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerId(dealer.getDealerId());
	request.setAttribute("hasTransferPricing", dealerGroup.getDealerGroupPreference().isIncludeTransferPricing());
	
	ActionForward forward = analyzeIt(mapping, tradeAnalyzerForm, request, response, dealer);
	
	SessionHelper.setAttribute( request, "tradeAnalyzerForm", tradeAnalyzerForm );
	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
	
	return forward;
}

protected abstract ActionForward analyzeIt(ActionMapping mapping, TradeAnalyzerForm tradeAnalyzerForm,	HttpServletRequest request, HttpServletResponse response, Dealer dealer)
	throws DatabaseException, ApplicationException;

protected void populateTradeAnalyzerForm( final IAppraisal appraisal, final TradeAnalyzerForm tradeAnalyzerForm )
{
	tradeAnalyzerForm.setAppraisalId( appraisal.getAppraisalId() );
	tradeAnalyzerForm.setColor( appraisal.getColor() );

	String salesPersonIdString = null;
	
	if ( appraisal.getDealTrackSalesPerson() != null )
	{
		tradeAnalyzerForm.setDealTrackSalesperson( appraisal.getDealTrackSalesPerson().getFullName() );
		salesPersonIdString = appraisal.getDealTrackSalesPerson().getPersonId().toString();
		tradeAnalyzerForm.setDealTrackSalespersonID(salesPersonIdString);
		
	

	}
	
	if(appraisal!=null && appraisal.getVehicle()!=null && appraisal.getVehicle().getVehicleCatalogKey()!=null){
		tradeAnalyzerForm.setCatalogKey(appraisal.getVehicle().getVehicleCatalogKey());
	}
	
	//AppraiaslType Appraisal? ... If so then we may have a potentialDeal
	if (appraisal.getAppraisalTypeId() != null && appraisal.getAppraisalTypeId() == 1){
		tradeAnalyzerForm.setDealTrackSalespersonID( salesPersonIdString );
		tradeAnalyzerForm.setDealTrackSalespersonClientID( -1 );		
		tradeAnalyzerForm.setStockNumber( appraisal.getDealTrackStockNumber() );
		tradeAnalyzerForm.setDealTrackNewOrUsed(InventoryTypeEnum.getInventoryTypeInteger(appraisal.getDealTrackNewOrUsed()));

	}
	//Otherwise AppraisalType is Purchase (in which case buyer is relevant) 
	else if(appraisal.getAppraisalTypeId() != null && appraisal.getAppraisalTypeId() == 2)
	{
		tradeAnalyzerForm.setAppraisalTypeEnum(AppraisalTypeEnum.PURCHASE);
		if (tradeAnalyzerForm.getBuyerId()!=null&&appraisal.getSelectedBuyer() != null) 
			tradeAnalyzerForm.setBuyerId(appraisal.getSelectedBuyer().getPersonId());
	}
	
	tradeAnalyzerForm.setMileage( new Integer( appraisal.getMileage() ) );
	tradeAnalyzerForm.setActionId( appraisal.getAppraisalActionType().getAppraisalActionTypeId() );
	tradeAnalyzerForm.setConditionDisclosure( appraisal.getConditionDescription() );
	tradeAnalyzerForm.setReconditioningCost( appraisal.getEstimatedReconditioningCost() );
	tradeAnalyzerForm.setTargetGrossProfit(appraisal.getTargetGrossProfit());
	tradeAnalyzerForm.setNotes(appraisal.getNotes());
	tradeAnalyzerForm.setPrice( ( appraisal.getWholesalePrice() != null ? appraisal.getWholesalePrice().intValue() : 0 ) );
	
	AppraisalValue valueObj = appraisal.getLatestAppraisalValue();
	tradeAnalyzerForm.setAppraisalValue( ( valueObj == null ) ? 0 : valueObj.getValue() );
	tradeAnalyzerForm.setTransferPrice(appraisal.getTransferPrice());

	if(appraisal.getMmrVehicle() != null)
	{
		MMRVehicle mmrVehicle = appraisal.getMmrVehicle();
		tradeAnalyzerForm.setSelectedMmrVehicle(mmrVehicle); 
	}
}

protected void populateAppraisal( boolean saveBump, Member member, TradeAnalyzerForm tradeAnalyzerForm, IAppraisal appraisal )
{
	Integer memberId = member.getMemberId();
	// save the important things first
	appraisal.setMemberId( memberId );
	appraisal.setColor( tradeAnalyzerForm.getColor() == null ? appraisal.getColor() : tradeAnalyzerForm.getColor() );
	appraisal.setMileage( tradeAnalyzerForm.getMileage() );

	// Purchase Appraisals need customers too !!
	// if (AppraisalTypeEnum.TRADE_IN.getId().intValue() == appraisal.getAppraisalTypeId())
	{
		appraisal.setCustomer( tradeAnalyzerForm.getCustomerFirstName(), tradeAnalyzerForm.getCustomerLastName(),
								tradeAnalyzerForm.getCustomerGender(), tradeAnalyzerForm.getCustomerEmail(),
								tradeAnalyzerForm.getCustomerPhoneNumber() );
	}

	appraisal.setEdmundsTMV(tradeAnalyzerForm.getEdmundsTMV());
	
	if ( saveBump )
	{
		// There used to be a bunch of checks for appraisal value and whether this is a trade or purchase.
		// Now we don't care - just save the appraiser name and value.
		appraisal.bump( tradeAnalyzerForm.getAppraisalValue(), tradeAnalyzerForm.getAppraiserName(),member.getFirstName()+" "+member.getLastName() );
	}
	IPerson salesPerson = null;
	IPerson buyer = null;
	try
	{
		if (tradeAnalyzerForm.getDealTrackSalespersonID() != null ) {
			salesPerson = getPersonService().getPerson( Integer.parseInt( tradeAnalyzerForm.getDealTrackSalespersonID() ) );			
		}else
		  {
			   salesPerson =  appraisal.getDealTrackSalesPerson();
			  }
		
		if (tradeAnalyzerForm.getBuyerId() != null && tradeAnalyzerForm.getBuyerId() != 0) {
			buyer = getPersonService().getPerson( tradeAnalyzerForm.getBuyerId() );						
		}
	} catch ( NumberFormatException nfe ){}
	
	appraisal.setPotentialDeal( salesPerson, tradeAnalyzerForm.getDealTrackStockNumber(),
			InventoryTypeEnum.getInventoryTypeEnum( tradeAnalyzerForm.getDealTrackNewOrUsed() ) );
	//appraisal.setSelectedBuyer(buyer);
	
	// This is a stick it in there job. I don't know of a way to make this work w/o overhauling a lot of code to an Subject -> Observer pattern,
	// or even the simple Service Layer.
	AppraisalActionType oldAppraisalActionType = appraisal.getAppraisalActionType();
	appraisal.updateInventoryDecision( AppraisalActionType.getAppraisalActionType( tradeAnalyzerForm.getActionId() ),
										tradeAnalyzerForm.getReconditioningCost(), tradeAnalyzerForm.getTargetGrossProfit(), tradeAnalyzerForm.getConditionDisclosure(),
										Double.valueOf( tradeAnalyzerForm.getPrice() ), tradeAnalyzerForm.getTransferPrice(), tradeAnalyzerForm.isTransferForRetailOnly() );

	// this goes last, in case the appraisal blows up. If it does, then we won't reach here and no hotsheets will be sent erroneously.
	processRedistribution( appraisal.getBusinessUnitId(), memberId, appraisal.getAppraisalId(), tradeAnalyzerForm.getGroupingDescriptionId(),
							tradeAnalyzerForm.getMileage(), tradeAnalyzerForm.getYear(), tradeAnalyzerForm.getVin(), oldAppraisalActionType,
							appraisal.getAppraisalActionType() );
	appraisal.setDateModified(new Date());
	//#27751
//	MMRVehicle mmrVehicle = appraisal.getMmrVehicle();
//	if (mmrVehicle == null)
//		mmrVehicle = new MMRVehicle();
//	mmrVehicle.setMid(tradeAnalyzerForm.getSelectedMmrTrim());
//	mmrVehicle.setRegionCode(tradeAnalyzerForm.getSelectedMmrArea());
//	appraisal.setMmrVehicle(mmrVehicle);
}

//hack below
@Override
protected final Dealer putDealerFormInRequest(HttpServletRequest request, int weeks)
{
	throw new RuntimeException("Do not call putDealerFormInRequest(HttpServletRequest, int)!");
}

}
