package com.firstlook.action.dealer.redistribution;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import biz.firstlook.commons.email.EmailService;
import biz.firstlook.commons.email.EmailService.EmailFormat;
import biz.firstlook.module.bookout.kbb.KbbBookValue;
import biz.firstlook.module.bookout.kbb.KbbBookValueContainer;
import biz.firstlook.module.vehicle.description.exception.ValuesRetrievalException;
import biz.firstlook.module.vehicle.description.old.BookOutDatasetInfo;
import biz.firstlook.module.vehicle.description.old.BookOutTypeEnum;
import biz.firstlook.module.vehicle.description.old.GBException;
import biz.firstlook.module.vehicle.description.old.GuideBookInput;
import biz.firstlook.module.vehicle.description.old.KBBCategoryEnum;
import biz.firstlook.module.vehicle.description.provider.kbb.KbbVehicleBookoutState;
import biz.firstlook.transact.persist.model.BookOutSource;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.service.ThirdPartyCategoryService;
import biz.firstlook.transact.persist.service.appraisal.AppraisalFormOptions;
import biz.firstlook.transact.persist.service.appraisal.Customer;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.components.vehicledetailpage.BookOutDisplayAction;
import com.firstlook.action.dealer.vehicledetailpage.BookoutDetailForm;
import com.firstlook.bookout.kbb.tradeIn.KbbConsumerValueContext;
import com.firstlook.bookout.kbb.tradeIn.KbbConsumerValueContextFactory;
import com.firstlook.data.DatabaseException;
import com.firstlook.display.tradeanalyzer.DisplayGuideBook;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.bookout.BookOutService;
import com.firstlook.service.bookout.appraisal.AppraisalBookOutService;
import com.firstlook.service.bookout.bean.AbstractBookOutState;
import com.firstlook.service.vehicle.VehicleService;
import com.firstlook.util.print.AppraisalFormPDFBuilder;

public class PDFEmailAction extends SecureBaseAction
{

private EmailService emailService;
private IAppraisalService appraisalService;
private ThirdPartyCategoryService thirdPartyCategoryService;
private BookOutService bookOutService;
private AppraisalBookOutService appraisalBookOutService;
private VehicleService vehicleService;
private FreeMarkerConfigurer configurer;
public FreeMarkerConfigurer getConfigurer() {
	return configurer;
}

public void setConfigurer(FreeMarkerConfigurer configurer) {
	this.configurer = configurer;
}

private static Logger logger = Logger.getLogger( PDFEmailAction.class );

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{

	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	int appraisalId = Integer.parseInt( request.getParameter( "appraisalId" ) );
	int mileage = Integer.parseInt( request.getParameter( "mileage" ) );
	String vin = request.getParameter( "vin" );
	// Needed because iText uses absolute paths to load images.
	String realPath = getServlet().getServletContext().getRealPath( "/" );

	boolean showOptions = ( request.getParameter( "options" ) == null ? false : true );
	boolean showReconCost=( request.getParameter( "reconCost" ) == null ? false : true );
	boolean showPhotos = ( request.getParameter( "photos" ) == null ? false : true );
	boolean showKbbConsumerValue = request.getParameter( "kbbConsumerValue" ) == null ? false : true;
	
	boolean includeMileageAdjustment = true; 
	if ( request.getParameter( "includeMileageAdjustment" ) != null ) {
		includeMileageAdjustment = (request.getParameter( "includeMileageAdjustment" ).equals( "false" )) ? false : true;
	}

	String[] categoriesToDisplayString = request.getParameterValues( "categoriesToDisplay" );
	Integer[] categoriesToDisplay = null;
	if ( categoriesToDisplayString != null ) {
		categoriesToDisplay = new Integer[categoriesToDisplayString.length ]; 
		for (int c = 0; c < categoriesToDisplayString.length; c++ ) {
			categoriesToDisplay[c] = Integer.parseInt( categoriesToDisplayString[c] );
		}
	}
	
	IAppraisal appraisal = getAppraisalService().findBy( appraisalId );
	AppraisalFormOptions formOptions = appraisal.getAppraisalFormOptions();
	Dealer dealer = getImtDealerService().retrieveDealer( currentDealerId );
	Vehicle vehicle = appraisal.getVehicle();
	Customer customer = appraisal.getCustomer();

	if ( formOptions != null && formOptions.getAppraisalFormId() != null )
	{
		showOptions = formOptions.isShowEquipmentOptions();
		showReconCost= formOptions.isShowReconCost();
		showPhotos = formOptions.isShowPhotos();
		showKbbConsumerValue = formOptions.isShowKbbConsumerValue();
		includeMileageAdjustment  = formOptions.isIncludeMileageAdjustment();
		
		if ( request.getParameter( "offer" ) != null )
		{//Saving the offer here because on the TA printing does not go through the save action.
			Integer offer = formatOffer( ( request.getParameter( "offer" ) ) );
			formOptions.setAppraisalFormOffer( offer );
			appraisalService.updateAppraisal( appraisal );
		}

		Integer categoriesToDisplayBitMask = null;
		if(formOptions.getCategoriesToDisplayBitMask() == null) {  //if this is null, user just clicked Print from TA/TM so use default values
			
			categoriesToDisplayBitMask = dealer.getDealerPreference().getAppraisalFormValuesTPCBitMask();
			showOptions = dealer.getDealerPreference().getAppraisalFormShowOptions();
			
			showPhotos = dealer.getDealerPreference().getAppraisalFormShowPhotos();
			categoriesToDisplay = AppraisalFormOptions.getThirdPartyCategoriesFromBitMask( categoriesToDisplayBitMask );
		}
		else{
			categoriesToDisplay = AppraisalFormOptions.getThirdPartyCategoriesFromBitMask( formOptions.getCategoriesToDisplayBitMask() );
		}
	}
	else
	{
		showOptions = dealer.getDealerPreference().getAppraisalFormShowOptions();
		showReconCost= true;
		showPhotos = dealer.getDealerPreference().getAppraisalFormShowPhotos();
		includeMileageAdjustment = true; // include mileage adjustment by deafult
		
		categoriesToDisplay = AppraisalFormOptions.getThirdPartyCategoriesFromBitMask( dealer.getDealerPreference().getAppraisalFormValuesTPCBitMask() );
		
		if ( request.getParameter( "offer" ) != null )
		{
			Integer offer = Integer.parseInt( request.getParameter( "offer" ) );
			formOptions.setAppraisalFormOffer( offer );
			appraisalService.updateAppraisal( appraisal );
		}
	}

	boolean showCheck = dealer.getDealerPreference().isShowCheckOnAppraisalForm();
	
	List< Integer > thirdPartyIds = retrieveThirdPartyBookoutServiceIds( dealer );

	GuideBookInput input = BookOutService.createGuideBookInput( vin, mileage, currentDealerId, dealer.getState(),
																				dealer.getDealerPreference().getNadaRegionCode(),
																				getMemberFromRequest( request ).getMemberId() );
	BookoutDetailForm bookoutForm = new BookoutDetailForm();

	populateDisplayGuideBooks( request, vin, appraisalId, mileage, thirdPartyIds, input, bookoutForm, BookOutSource.BOOK_OUT_SOURCE_APPRAISAL,
								dealer.getDealerPreference().getSearchInactiveInventoryDaysBackThreshold() );

	ByteArrayOutputStream baos = new ByteArrayOutputStream();

//	 if show consumerValue - then lookup the value, if not (or can't find the value) set value to null
	Integer kbbConsumerValue = showKbbConsumerValue ? getKbbConsumerValue( vin, dealer, mileage ) : null;
	
	AppraisalFormPDFBuilder formBuilder = new AppraisalFormPDFBuilder( appraisal, dealer, vehicle, customer, formOptions, realPath );
	String forward = formBuilder.buildAppraisalFormPDFWithPrinceXML( baos, showOptions, showPhotos, categoriesToDisplay,
														bookoutForm.findDisplayGuideBookByGuideBookId( dealer.getGuideBookId() ),
														bookoutForm.findDisplayGuideBookByGuideBookId( dealer.getGuideBook2Id() ),
														retrieveDealerLogoUrl( currentDealerId ), includeMileageAdjustment, kbbConsumerValue,
														showCheck ,showReconCost, configurer);

	if ( forward != null )
	{
		try {
			response.getOutputStream().print("Error");
			response.getOutputStream().flush();
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	String toEmails=request.getParameter("emailTo");
	String replyto=request.getParameter("emailReplyTo");
	String subject=request.getParameter("emailSubject");
	String body=request.getParameter("emailBody");
	
	if(replyto.equals(""))
		replyto=null;
	Resource resource= new ByteArrayResource(baos.toByteArray());
	
	
//	File file;
//	try {
//		file = File.createTempFile("tempfile", ".pdf");
//		file.deleteOnExit();
//		
//		FileOutputStream fos= new FileOutputStream(file);
//		fos.write(baos.toByteArray());
//		fos.close();
//		
//		resource = new FileSystemResource(file);
//		
//	} catch (IOException e1) {
//		
//		e1.printStackTrace();
//	} 
	
	
	List<String> emails= Arrays.asList(toEmails.split(","));
	Map<String,String> emailContext= new HashMap<String, String>();
	emailContext.put("body", body);
	
	Map<String,Resource> resources = new HashMap<String, Resource>();
	resources.put("CustOfferPdf", resource);
	
	Map<String,String> resourceContentTypes= new HashMap<String, String>();
	resourceContentTypes.put("CustOfferPdf", "application/pdf");
	
	List<String> ccList = new ArrayList<String>();
	ccList.add("support@firstlook.biz");
	
	Iterator<String> itr= emails.iterator();
	
	while(itr.hasNext()){
	
		emailService.sendSynchEmail(itr.next(), ccList, emailContext, "EmailBody", subject, replyto, resources, resourceContentTypes, EmailFormat.HTML_MULTIPART);
	}
	
	
	
	try {
		response.getOutputStream().print("Success");
		response.getOutputStream().flush();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	

	//response.setContentType( "application/pdf" );
	//response.setContentLength( baos.size() );


	return null;

}

private Integer getKbbConsumerValue( String vin, Dealer dealer, int mileage ) {
	biz.firstlook.transact.persist.model.Dealer tpDealer = new biz.firstlook.transact.persist.model.Dealer( dealer.getBusinessUnitId(), 
																	dealer.isActive(), dealer.getDealerCode(), dealer.getShortName(), 
																	dealer.getState(), dealer.getDealerPreference() );
	
	KbbConsumerValueContext context = KbbConsumerValueContextFactory.getKbbConsumerValueNonKbbDealerContext();
	KbbVehicleBookoutState state = (KbbVehicleBookoutState) context.find(vin, tpDealer);


	//when finding state, if context cannot find it returns a new one
	//unfortunately, veh desc is null
	//check here to avoid null pointer in below try block
	if ( state.getVehicleDescription() == null ) {
		return null;
	}
	
	Collection<KbbBookValue> values = null;
	if (state != null) {
		try {
			KbbBookValueContainer valuesContainer = (KbbBookValueContainer)state.getVehicleDescription().getBookValueContainer( mileage ); 
			values = valuesContainer.getValues(KBBCategoryEnum.TRADEIN);
		} catch (ValuesRetrievalException e) {
			logger.error( e );
		}
	}
	
	if ( values == null || values.isEmpty() ) {
		return null;
	}
	
	KbbBookValue theValue = new KbbBookValue(null);
	for ( KbbBookValue value : values ) {
		if ( value.getCondition().equals( state.getCondition() ) ) {
			theValue = value;
			break;
		}
	}
	return theValue.getValue();
}

private void populateDisplayGuideBooks( HttpServletRequest request, String vin, Integer identifier, Integer mileage, 
                                        List<Integer> thirdPartyIds, GuideBookInput input, BookoutDetailForm bookoutForm, 
										Integer bookOutSourceId, int searchAppraisalDaysBackThreshold )
{
	Iterator< Integer > thirdPartyIdsIter = thirdPartyIds.iterator();

	bookoutForm.setVin( vin );
	bookoutForm.setMileage( mileage );
	@SuppressWarnings( "unused" )
	DisplayGuideBook displayGuideBook;
	ActionErrors guideBookErrors = new ActionErrors();
	AbstractBookOutState bookOutState = null;
	while ( thirdPartyIdsIter.hasNext() )
	{

		Integer thirdPartyBookoutServiceId = (Integer)thirdPartyIdsIter.next();

		try
		{
			bookOutState = constructBookOutState( identifier, input, bookOutSourceId, bookOutState, thirdPartyBookoutServiceId,
													searchAppraisalDaysBackThreshold );

			BookOutDisplayAction.constructDisplayGuideBooks( request, bookoutForm, guideBookErrors, bookOutState,thirdPartyBookoutServiceId );
		}
		catch ( GBException e )
		{
			DisplayGuideBook guideBook = new DisplayGuideBook();
			guideBook.setSuccess( false );
			guideBook.setSuccessMessage( e.getMessage() );
			guideBook.setValidVin( false );
			guideBook.setGuideBookName( ThirdPartyDataProvider.getThirdPartyDataProviderDescription( thirdPartyBookoutServiceId.intValue() ) );
			bookoutForm.addDisplayGuideBook( guideBook );
		}
	}
}

private AbstractBookOutState constructBookOutState( Integer identifier, GuideBookInput input, Integer bookOutSourceId, AbstractBookOutState bookOutState,
													Integer thirdPartyId, int searchAppraisalDaysBackThreshold ) throws GBException
{

	if ( bookOutSourceId.intValue() == BookOutSource.BOOK_OUT_SOURCE_APPRAISAL.intValue() )
	{
		bookOutState = getAppraisalBookOutService().retrieveExistingBookOutInfoOrBeginNewBookOut( input, thirdPartyId, searchAppraisalDaysBackThreshold, null );
	}
	else
	{
		bookOutState = getBookOutService().doVinLookup( input, thirdPartyId, new BookOutDatasetInfo( BookOutTypeEnum.APPRAISAL, input.getBusinessUnitId()), null );
	}
	return bookOutState;
}

private int formatOffer( String offer )
{
	int indexOfPeriod = offer.indexOf( "." );
	if ( indexOfPeriod >= 0 )
	{
		offer = offer.substring( 0, indexOfPeriod );
	}
	int indexOfComma = offer.indexOf( "," );
	if ( indexOfComma >= 0 )
	{
		offer = offer.substring( 0, indexOfComma ) + offer.substring( ( indexOfComma + 1 ), offer.length() );
	}
	int indexOfDollar = offer.indexOf( "$" );
	if ( indexOfDollar >= 0 )
	{
		offer = offer.substring( ( indexOfDollar + 1 ), offer.length() );
	}

	return Integer.parseInt( offer );
}

/**
 * @see ThirdPartyDataProvider
 * @return This method returns a list of ThirdPartyIds.
 */
private List< Integer > retrieveThirdPartyBookoutServiceIds( Dealer dealer )
{
	List< Integer > guideBookServices = new ArrayList< Integer >();
	guideBookServices.add( dealer.getGuideBookId() );
	if ( dealer.getGuideBook2Id() != 0 )
	{
		guideBookServices.add( dealer.getGuideBook2Id() );
	}
	return guideBookServices;
}

private String retrieveDealerLogoUrl( Integer businessUnitId )
{
	return getImtDealerService().retrieveDealerLogo( businessUnitId );
}

public IAppraisalService getAppraisalService()
{
	return appraisalService;
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

public ThirdPartyCategoryService getThirdPartyCategoryService()
{
	return thirdPartyCategoryService;
}

public void setThirdPartyCategoryService( ThirdPartyCategoryService thirdPartyCategoryService )
{
	this.thirdPartyCategoryService = thirdPartyCategoryService;
}

public BookOutService getBookOutService()
{
	return bookOutService;
}

public void setBookOutService( BookOutService bookOutService )
{
	this.bookOutService = bookOutService;
}

public AppraisalBookOutService getAppraisalBookOutService()
{
	return appraisalBookOutService;
}

public void setAppraisalBookOutService( AppraisalBookOutService appraisalBookOutService )
{
	this.appraisalBookOutService = appraisalBookOutService;
}

public VehicleService getVehicleService()
{
	return vehicleService;
}

public void setVehicleService( VehicleService vehicleService )
{
	this.vehicleService = vehicleService;
}

public EmailService getEmailService() {
	return emailService;
}

public void setEmailService(EmailService emailService) {
	this.emailService = emailService;
}

}
