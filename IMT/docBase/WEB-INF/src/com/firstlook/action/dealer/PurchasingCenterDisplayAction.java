package com.firstlook.action.dealer;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.services.vehicleCatalog.IVehicleCatalogService;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogServiceException;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;

public class PurchasingCenterDisplayAction extends SecureBaseAction
{

private IVehicleCatalogService vehicleCatalogService;
	
public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	Member member = getMemberFromRequest(request );
	
	//not a one liner to clarify that the predicate logic of the two variables equals a business decision, 
	//should get encapsulated somehow in a future refactor.
	boolean purchasingCenterEnabled = !(member.getMemberRoleTester().isUsedAppraiser() 
										|| member.getMemberRoleTester().isUsedNoAccess());
	
	request.setAttribute("purchasingCenterEnabled", purchasingCenterEnabled );
	
	//flashlocate
	List<String> makes = null;
	try {
		makes = vehicleCatalogService.retrieveAllMakes();
		request.setAttribute("makes", makes);
	} catch (VehicleCatalogServiceException e) {
		throw new ApplicationException(e); //dpp
	}
	
    return mapping.findForward( "success" );
}

public void setVehicleCatalogService( IVehicleCatalogService vehicleCatalogService )
{
	this.vehicleCatalogService = vehicleCatalogService;
}

}
