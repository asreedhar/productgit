package com.firstlook.action.dealer.tools;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

import biz.firstlook.commons.util.IPropertyFinder;
import biz.firstlook.commons.util.MockPropertyFinder;
import biz.firstlook.commons.util.PropertyLoader;

import com.firstlook.action.BaseAction;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.DummyHttpRequest;

/**
 * @author cvs
 * 
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates. To enable and disable the creation of type
 * comments go to Window>Preferences>Java>Code Generation.
 */
public class TestGuideBookStepThreeSubmitAction extends BaseTestCase
{
DummyHttpRequest request;
ActionErrors errors;
GuideBookStepThreeSubmitAction action;
String pageName;

private static final String FAILURE = "failure";
private static final String SUCCESS = "success";

public TestGuideBookStepThreeSubmitAction( String arg1 )
{
    super(arg1);
}

public void setup()
{
    request = new DummyHttpRequest();
    action = new GuideBookStepThreeSubmitAction();
    errors = new ActionErrors();
    List<IPropertyFinder> list = new ArrayList<IPropertyFinder>();
    MockPropertyFinder finder = new MockPropertyFinder();
    finder.setStringProperty("error.dealer.tools.tradeanalyzer.make", "foo");
    list.add(finder);
    PropertyLoader.setPropertyFinders(list);
}

public void tearDown()
{
    PropertyLoader.setPropertyFinders(null);
}

public void testGetMappingForwardActionErrorValidateFalse()
{
    errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
            "error.dealer.tools.tradeanalyzer.make"));
    BaseAction.putActionErrorsInRequest(request, errors);

    assertEquals("Should be failure", FAILURE, action.getMappingForward(
            request, pageName));
}

public void testGetMappingForwardNoActionErrorValidateFalse()
{
    assertEquals("Should be success", SUCCESS, action.getMappingForward(
            request, pageName));
}


}
