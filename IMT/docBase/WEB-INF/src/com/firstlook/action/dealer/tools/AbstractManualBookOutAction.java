package com.firstlook.action.dealer.tools;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.transact.persist.model.BookOutValueType;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.session.FirstlookSession;

public abstract class AbstractManualBookOutAction extends SecureBaseAction {
	
	protected abstract String processManualRequest(HttpServletRequest request, Dealer currentDealer, String selected) throws ApplicationException;
	protected abstract String getGuideBookName();
	
	public ActionForward doIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws DatabaseException, ApplicationException {
		Dealer currentDealer = getImtDealerService().retrieveDealer( ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId() );
		String forward = processManualRequest(request,currentDealer, request.getParameter( "selected" ));
		addBookDisplayTagsToRequest(request, currentDealer);
		request.setAttribute("dealerNickName", currentDealer.getNickname());
		request.setAttribute("guideBookName", getGuideBookName());
		return mapping.findForward(forward);
	}
	
	public static List<VDP_GuideBookValue> getFinalValues(List<VDP_GuideBookValue> values) {
		List<VDP_GuideBookValue> finalValues = new ArrayList<VDP_GuideBookValue>();
		for(VDP_GuideBookValue value : values) {
			if(value.getValueType() == BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE) {
				finalValues.add(value);
			}
		}
		return finalValues;
	}
	
	public static VDP_GuideBookValue getMileageAdjustmentValue(List<VDP_GuideBookValue> values) {
		for(VDP_GuideBookValue value : values) {
			if(value.getValueType() == BookOutValueType.BOOK_OUT_MILEAGE_ADJUSTMENT) {
				return value;
			}
		}
		return null;
	}

	private void addBookDisplayTagsToRequest(HttpServletRequest request, Dealer currentDealer) {
		if ( currentDealer.getDealerPreference().getGuideBookIdAsInt() == 1 || currentDealer.getDealerPreference().getGuideBook2IdAsInt() == 1 )
		{
			request.setAttribute( "hasBlackBook", Boolean.TRUE );
		}
		if ( currentDealer.getDealerPreference().getGuideBookIdAsInt() == 2 || currentDealer.getDealerPreference().getGuideBook2IdAsInt() == 2 )
		{
			request.setAttribute( "hasNADA", Boolean.TRUE );
		}
		if ( currentDealer.getDealerPreference().getGuideBookIdAsInt() == 3 || currentDealer.getDealerPreference().getGuideBook2IdAsInt() == 3 )
		{
			request.setAttribute( "hasKelley", Boolean.TRUE );
		}
	}
	
}
