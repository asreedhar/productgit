package com.firstlook.action.dealer.redistribution;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.person.IPerson;
import biz.firstlook.transact.persist.person.PositionType;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.form.ManagePeopleForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;

public class ManagePeopleEditAction extends SecureBaseAction {

    @SuppressWarnings("unchecked")
    public ActionForward doIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws DatabaseException, ApplicationException {
        ManagePeopleForm theForm = (ManagePeopleForm) form;
        
        int businessUnitId = getFirstlookSessionFromRequest(request).getCurrentDealerId();
        PositionType type = PositionType.valueFromPositionString(theForm.getPosition());
        String personId = request.getParameter("personId");
        
        List<IPerson> people = new ArrayList<IPerson>();
        people = (List<IPerson>) getPersonService().getBusinessUnitPeopleByPosition(businessUnitId,type);
        
        for (IPerson person : people) {
            if (person.getPersonId() == Integer.parseInt(personId)){
                person.setEditMode(true);
            }
        }
        
        theForm.setEditMode(true);
        
        SessionHelper.keepAttribute(request, "tradeAnalyzerForm");
        SessionHelper.keepAttribute( request, "bookoutDetailForm" );
        SessionHelper.keepAttribute( request, "auctionData" );
       
        return mapping.findForward("success");
    }

}
