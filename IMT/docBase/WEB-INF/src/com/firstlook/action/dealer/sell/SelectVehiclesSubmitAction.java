package com.firstlook.action.dealer.sell;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.SelectVehiclesForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.service.inventory.InventoryService_Legacy;
import com.firstlook.session.FirstlookSession;

public class SelectVehiclesSubmitAction extends SecureBaseAction
{

private InventoryService_Legacy inventoryService_Legacy;

String getForwardMapping( HttpServletRequest request )
{
    String forwardName = "error";

    String goToSearch = request.getParameter( "search.x" );

    String goToSearchSubmit = request.getParameter( "searchSubmit.x" );
    if ( goToSearchSubmit == null && RequestHelper.getBoolean( request, "searchPressed" ) )
    {
        goToSearchSubmit = "true";
    }
    String goToWorksheet = request.getParameter( "worksheet.x" );
    String goToReview = request.getParameter( "review.x" );
    String goToSelect = request.getParameter( "select.x" );
    String goToSaveAndComeBack = request.getParameter( "saveAndComeBack.x" );
    String goToFullInventory = request.getParameter( "fullinventory.x" );
    String goToAgedInventory = request.getParameter( "agedinventory.x" );

    if ( goToSearch != null )
    {
        forwardName = "search";
    }
    else if ( goToSearchSubmit != null )
    {
        forwardName = "searchSubmit";
    }
    else if ( goToWorksheet != null )
    {
        forwardName = "worksheet";
    }
    else if ( goToReview != null )
    {
        forwardName = "review";
    }
    else if ( goToSelect != null )
    {
        forwardName = "select";
    }
    else if ( goToSaveAndComeBack != null )
    {
        forwardName = "saveAndComeBack";
    }
    else if ( goToFullInventory != null )
    {
        forwardName = "fullInventory";
    }
    else if ( goToAgedInventory != null )
    {
        forwardName = "agedInventory";
    }
    else
    {
        forwardName = "success";
    }

    return forwardName;
}

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    SelectVehiclesForm selectVehiclesForm = (SelectVehiclesForm)form;

    List<Integer> newlySelected = selectVehiclesForm.getNewlyCheckedVehicleIds();
    List<Integer> unSelected = selectVehiclesForm.getNewlyUncheckedVehicleIds();
    int currentDealerId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();

    Member member = getMemberFromRequest( request );
    updateVehiclesFromForm( member.getMemberType().intValue(), newlySelected, InventoryEntity.SUBMISSION_STATUS_SELECTED, currentDealerId );
    updateVehiclesFromForm( member.getMemberType().intValue(), unSelected, InventoryEntity.SUBMISSION_STATUS_NOT_SELECTED, currentDealerId );

    return mapping.findForward( getForwardMapping( request ) );
}

private void updateVehiclesFromForm( int memberType, List<Integer> vehiclesToUpdate, int submissionStatus, int currentDealerId )
        throws ApplicationException, DatabaseException
{
    Iterator<Integer> vehicleIds = vehiclesToUpdate.iterator();
    while ( vehicleIds.hasNext() )
    {
        InventoryEntity inventory = inventoryService_Legacy.retrieveInventoryForValidMember( memberType, vehicleIds.next(), currentDealerId );
        inventoryService_Legacy.saveOrUpdate( inventory );
    }
}

public void setInventoryService_Legacy( InventoryService_Legacy service )
{
    inventoryService_Legacy = service;
}


}
