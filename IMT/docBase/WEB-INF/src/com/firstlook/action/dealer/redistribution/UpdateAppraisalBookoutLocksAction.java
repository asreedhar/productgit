package com.firstlook.action.dealer.redistribution;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.transact.persist.model.BusinessUnitCredential;
import biz.firstlook.transact.persist.model.CredentialType;
import biz.firstlook.transact.persist.model.Inventory;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.DealerGroup;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.SessionHelper;
import com.firstlook.service.dealergroup.DealerGroupService;
import com.firstlook.service.inventory.VehicleInventoryService;

public class UpdateAppraisalBookoutLocksAction extends SecureBaseAction
{

private VehicleInventoryService vehicleInventoryService;
private IAppraisalService appraisalService;
private DealerGroupService dealerGroupService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerId( currentDealerId );
	BusinessUnitCredential businessUnitCredential = null;
	if ( getFirstlookSessionFromRequest( request ).isIncludeAppraisalLockout() )
	{
		businessUnitCredential = getImtDealerService().retrieveBusinessUnitCredential( currentDealerId,
																				CredentialType.APPRAISALLOCKOUT );
	}

	if ( dealerGroup != null && dealerGroup.isLithiaStore() && businessUnitCredential == null )
	{
		businessUnitCredential = getImtDealerService().retrieveBusinessUnitCredential( dealerGroup.getDealerGroupId(),
																				CredentialType.APPRAISALLOCKOUT );
	}
	Boolean locked = new Boolean( request.getParameter( "locked" ) );
	String comingFrom = request.getParameter( "comingFrom" );
	Integer id = new Integer( request.getParameter( "id" ) );

	String password = RequestHelper.getString( request, "lockoutPass" );
	boolean invalidPass = checkPassword( request, businessUnitCredential.getPassword(), password );

	Inventory inventory = null;

	Date now = new Date();
	if ( locked ) // Set lock time ahead 24 hours
	{
		now = DateUtilFL.addDaysToDate( now, 1 );
	}
	else
	// Setting lock time back a minute
	{
		now.setMinutes( now.getMinutes() - 2 );
	}

	if ( invalidPass == false )
	{
		if ( comingFrom.equals( "tm" ) )
		{
			IAppraisal appraisal = appraisalService.findBy( id );
			appraisal.setBookoutLocked( locked );
			appraisalService.updateAppraisal( appraisal );
		}
		else
		{
			inventory = vehicleInventoryService.getInventoryDAO().findBy( id );
			inventory.setDateBookoutLocked( now );
			vehicleInventoryService.getInventoryDAO().saveOrUpdate( inventory );
		}
	}

	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );

	return mapping.findForward( "success" );

}

private boolean checkPassword( HttpServletRequest request, String dbPass, String submittedPass )
{
	if ( submittedPass.equals( dbPass ) )
	{
		request.setAttribute( "invalidPass", new Boolean( false ) );
		return false;
	}
	else
	{
		request.setAttribute( "invalidPass", new Boolean( true ) );
		return true;
	}

}

public IAppraisalService getAppraisalService()
{
	return appraisalService;
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

public VehicleInventoryService getVehicleInventoryService()
{
	return vehicleInventoryService;
}

public void setVehicleInventoryService( VehicleInventoryService vehicleInventoryService )
{
	this.vehicleInventoryService = vehicleInventoryService;
}

public DealerGroupService getDealerGroupService()
{
	return dealerGroupService;
}

public void setDealerGroupService( DealerGroupService dealerGroupService )
{
	this.dealerGroupService = dealerGroupService;
}

}
