package com.firstlook.action.dealer;

import com.firstlook.helper.InventoryRiskHelperForm;
import com.firstlook.scorecard.ScoreCardReportForm;

public class ManagementCenterForm
{

private InventoryRiskHelperForm inventoryRiskForm;
private ScoreCardReportForm retailScoreCardForm;
private ScoreCardReportForm purchaseScoreCardForm;
private ScoreCardReportForm tradeInScoreCardForm;

public ManagementCenterForm( InventoryRiskHelperForm inventoryForm,
        ScoreCardReportForm retailScoreCard,
        ScoreCardReportForm purchaseScoreCard,
        ScoreCardReportForm tradeInScoreCard )
{
    this.inventoryRiskForm = inventoryForm;
    this.retailScoreCardForm = retailScoreCard;
    this.purchaseScoreCardForm = purchaseScoreCard;
    this.tradeInScoreCardForm = tradeInScoreCard;
}

public InventoryRiskHelperForm getInventoryRiskForm()
{
    return inventoryRiskForm;
}

public void setInventoryRiskForm( InventoryRiskHelperForm inventoryRiskForm )
{
    this.inventoryRiskForm = inventoryRiskForm;
}

public ScoreCardReportForm getPurchaseScoreCardForm()
{
    return purchaseScoreCardForm;
}

public void setPurchaseScoreCardForm( ScoreCardReportForm purchaseScoreCardForm )
{
    this.purchaseScoreCardForm = purchaseScoreCardForm;
}

public ScoreCardReportForm getRetailScoreCardForm()
{
    return retailScoreCardForm;
}

public void setRetailScoreCardForm( ScoreCardReportForm retailScoreCardForm )
{
    this.retailScoreCardForm = retailScoreCardForm;
}

public ScoreCardReportForm getTradeInScoreCardForm()
{
    return tradeInScoreCardForm;
}

public void setTradeInScoreCardForm( ScoreCardReportForm tradeInScoreCardForm )
{
    this.tradeInScoreCardForm = tradeInScoreCardForm;
}
}
