package com.firstlook.action.dealer.vehicledetailpage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.module.vehicle.description.old.BookOutDatasetInfo;
import biz.firstlook.module.vehicle.description.old.BookOutError;
import biz.firstlook.module.vehicle.description.old.BookOutTypeEnum;
import biz.firstlook.module.vehicle.description.old.GBException;
import biz.firstlook.module.vehicle.description.old.GuideBookInput;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.enumerator.BookoutStatusEnum;
import biz.firstlook.transact.persist.model.AuditBookOuts;
import biz.firstlook.transact.persist.model.BookOut;
import biz.firstlook.transact.persist.model.BookOutSource;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.InventoryWithBookOut;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;
import biz.firstlook.transact.persist.persistence.IInventoryBookOutDAO;
import biz.firstlook.transact.persist.service.AuditBookOutsService;
import biz.firstlook.transact.persist.service.ThirdPartyVehicleOptionService;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.display.tradeanalyzer.DisplayGuideBook;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.SessionHelper;
import com.firstlook.service.bookout.BookOutService;
import com.firstlook.service.bookout.appraisal.AppraisalBookOutService;
import com.firstlook.service.bookout.bean.BookValuesBookOutState;
import com.firstlook.service.bookout.bean.SelectedVehicleAndOptionsBookOutState;
import com.firstlook.service.bookout.inventory.InventoryBookOutService;

public class BookoutDetailSubmitAction extends SecureBaseAction
{

private DealerPreferenceDAO dealerPrefDAO;
private IInventoryBookOutDAO inventoryBookOutDAO;
private IAppraisalService appraisalService;
private InventoryBookOutService inventoryBookOutService;
private AppraisalBookOutService appraisalBookOutService;
private AuditBookOutsService auditBookOutsService;
private BookOutService bookOutService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	Dealer dealer = putDealerFormInRequest( request );
	BookoutDetailForm bookoutForm = (BookoutDetailForm)form;
	String whatToDo = (String)request.getParameter( "whatToDo" );
	boolean isActive = RequestHelper.getBoolean( request, "isActive" );
	Boolean editableMileage = new Boolean( request.getParameter( "editableMileage" ) );
	Boolean displayValues = new Boolean( request.getParameter( "displayValues" ) );
	Boolean displayOptions = new Boolean( request.getParameter( "displayOptions" ) );
	Boolean allowUpdate = new Boolean( request.getParameter( "allowUpdate" ) );

	Integer identifier = new Integer( request.getParameter( "identifier" ) );
	Integer bookOutSourceId = new Integer( request.getParameter( "bookOutSourceId" ) );
	BookOutTypeEnum bookoutType = ( bookOutSourceId.intValue() == BookOutSource.BOOK_OUT_SOURCE_APPRAISAL.intValue() ) ? BookOutTypeEnum.APPRAISAL : BookOutTypeEnum.INVENTORY;
	
	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	DealerPreference dealerPref = dealer.getDealerPreference();
	
	Member member = getMemberFromRequest( request );
	GuideBookInput input = new GuideBookInput();
	input.setVin( bookoutForm.getVin() );
	input.setBusinessUnitId( currentDealerId );
	input.setMileage( bookoutForm.getMileage() );
	input.setState( dealer.getState() );
	input.setMemberId( member.getMemberId() );
	input.setNadaRegionCode( dealerPref.getNadaRegionCode() );
	if ( whatToDo.equalsIgnoreCase( "changeDropdowns" ) )
	{
		final String parameter = request.getParameter("guideBookId");
		final Object attribute = request.getAttribute("guideBookId");
		Integer guideBookId = Functions.nullSafeInteger(parameter);
		if (guideBookId == null)
		{
			guideBookId = Functions.nullSafeInteger(attribute);
		}
		if (guideBookId != null)
		{
			DisplayGuideBook guideBook = bookoutForm.findDisplayGuideBookByGuideBookId( guideBookId );
	
			if (guideBook != null)
			{
				// this was added to make second bookout drop down work correctly - DW
				
				String selectedKey = (String)request.getParameter( "selectedKey" );
				
				guideBook.setSelectedVehicleKey( selectedKey );
				
				VDP_GuideBookVehicle guideBookVehicle = guideBook.determineSelectedVehicle( selectedKey );
		
				if ( !guideBook.getSelectedVehicleKey().equals( "0" ) )
				{
					SelectedVehicleAndOptionsBookOutState state;
					try{
					if ( guideBook.isStoredInfo() )
					{
						VDP_GuideBookVehicle vehicle = guideBook.determineSelectedVehicle( guideBook.getSelectedVehicleKey() );
						if ( vehicle != null && vehicle.getGuideBookOptions() != null && !vehicle.getGuideBookOptions().isEmpty() )
						{
							state = new SelectedVehicleAndOptionsBookOutState();
							state.setSelectedVehicleKey( guideBook.getSelectedVehicleKey() );
							state.setThirdPartyVehicleOptions( vehicle.getGuideBookOptions() );
							state.setVehicleYear( vehicle.getYear() );
						}
						else
						{
							state = getBookOutService().lookUpOptionsForSingleVehicle( input, new Integer(guideBookId), guideBookVehicle, new BookOutDatasetInfo( bookoutType, input.getBusinessUnitId()) );
						}
					}
					else
					{
						state = getBookOutService().lookUpOptionsForSingleVehicle( input, new Integer(guideBookId), guideBookVehicle, new BookOutDatasetInfo( bookoutType, input.getBusinessUnitId()) );
					}
					state.transformBookOutToDisplayData( guideBook );
						}
					catch(GBException gbe){
						throw new ApplicationException(gbe);
					}
				}
				// No Selection.  "Please select a model..."
				else
				{
					// No model selection.
					// Should not display any options.
					guideBook.setDisplayGuideBookOptions(new ArrayList<ThirdPartyVehicleOption>());
				}
				guideBook.setDisplayGuideBookValues( new ArrayList<VDP_GuideBookValue>() );
				
				// this makes sure the standard options are still selected
				Collection<DisplayGuideBook> guideBooks = bookoutForm.getDisplayGuideBooks();
				for(DisplayGuideBook displayGuideBook : guideBooks ) {	
					displayGuideBook.setSelectedEquipmentOptionKeys(
						ThirdPartyVehicleOptionService.setAndUpdateSelectedOptionsUsingArray( guideBook.getDisplayEquipmentGuideBookOptions(),
																				guideBook.getSelectedEquipmentOptionKeys() ));
				}
				
				request.setAttribute( "bookOutWasSaved", new Boolean( false ) );
			}
		}
	}
	if ( whatToDo.equalsIgnoreCase( "submitAll" ) )
	{
		bookoutAndSaveInformation( bookoutForm, input, request, identifier, bookOutSourceId, dealerPref.getSearchInactiveInventoryDaysBackThreshold() );
		updateAccuracyOfBooks( bookoutForm );
		//populateNotUpdatedBooks( bookoutForm, guideBookId );
	}

	request.setAttribute( "isActive", new Boolean( isActive ) );
	request.setAttribute( "identifier", identifier );
	request.setAttribute( "bookOutSourceId", bookOutSourceId );
	request.setAttribute( "editableMileage", editableMileage );
	request.setAttribute( "displayValues", displayValues );
	request.setAttribute( "displayOptions", displayOptions );
	request.setAttribute( "allowUpdate", allowUpdate );

	SessionHelper.setAttribute( request, "bookoutDetailForm", bookoutForm );
	request.setAttribute( "bookoutDetailForm", bookoutForm );

	SessionHelper.keepAttribute( request, "bookoutDetailForm" );
	SessionHelper.keepAttribute( request, "vehicleInformationForm" );
	SessionHelper.keepAttribute( request, "auctionForm" );
	SessionHelper.keepAttribute(request, "tradeAnalyzerForm");
	
	return mapping.findForward( "success" );
}

private void updateAccuracyOfBooks( BookoutDetailForm bookoutForm )
{
	Collection<DisplayGuideBook> guideBooks = bookoutForm.getDisplayGuideBooks();
	for(DisplayGuideBook guideBook : guideBooks ) {
		guideBook.setInAccurate( false );
	}
}
private void bookoutAndSaveInformation( BookoutDetailForm bookoutForm, GuideBookInput input, HttpServletRequest request,
										Integer identifier, Integer bookOutSourceId, int searchInactiveInventoryDaysBackThreshold )
{
	ActionErrors guideBookErrors = new ActionErrors();

	DisplayGuideBook guideBook;
	VDP_GuideBookVehicle vehicle;
	String selectedKey;

	Collection<DisplayGuideBook> guideBooks = bookoutForm.getDisplayGuideBooks();
	Iterator<DisplayGuideBook> guideBookIter = guideBooks.iterator();
	while (guideBookIter.hasNext() )
	{	
		guideBook = guideBookIter.next();
		try 
		{
			if ( guideBook.isValidVin() )
			{
				selectedKey = guideBook.getSelectedVehicleKey();
				vehicle = guideBook.determineSelectedVehicle( selectedKey );
				if ( vehicle != null )
				{
					vehicle.setCondition( guideBook.getCondition() );
					guideBook.setSelectedEquipmentOptionKeys(
							ThirdPartyVehicleOptionService.setAndUpdateSelectedOptionsUsingArray( guideBook.getDisplayEquipmentGuideBookOptions(),
																					guideBook.getSelectedEquipmentOptionKeys() ));
					// these three are for Kelly book outs only
					ThirdPartyVehicleOptionService.setSelectedOption( guideBook.getDisplayEngineGuideBookOptions(),
																		guideBook.getSelectedEngineKey() );
					ThirdPartyVehicleOptionService.setSelectedOption( guideBook.getDisplayTransmissionGuideBookOptions(),
																		guideBook.getSelectedTransmissionKey() );
					ThirdPartyVehicleOptionService.setSelectedOption( guideBook.getDisplayDrivetrainGuideBookOptions(),
																		guideBook.getSelectedDrivetrainKey() );
					BookValuesBookOutState state = updateBookValuesState( identifier, input, vehicle,
																			guideBook.getDisplayGuideBookOptions(),
																			guideBook.getDisplayGuideBookVehiclesCollection(),
																			guideBook.isStoredInfo(), bookOutSourceId, guideBook.getGuideBookId(), searchInactiveInventoryDaysBackThreshold );
					if(!state.getBookOutErrors().isEmpty())
					{
						if(guideBook.getHasErrors().booleanValue() == false)
						{
							guideBook.setHasErrors(Boolean.TRUE);
						}
					}
					if ( state.getBookOutErrors().get( new Integer( BookOutError.OPTIONS_CONFLICT ) ) != null )
					{
						List<BookOutError> bookOutErrors = state.getBookOutErrors().get( BookOutError.OPTIONS_CONFLICT );
						for(BookOutError bookOutError : bookOutErrors ) {
							guideBookErrors.add( ActionErrors.GLOBAL_ERROR, new ActionError(	"error.kelley.conflict",
																								bookOutError.getFirstOptionDescription(),
																								bookOutError.getSecondOptionDescription() ) );					
						}
						
						request.setAttribute( "guideBookError", new Boolean( true ) );
						putActionErrorsInRequest( request, guideBookErrors );
					}
					else
					{
						state.transformBookOutToDisplayData( guideBook );
					}
				}
			}
		}
		catch (GBException e)
		{
			guideBook.setSuccess( false );
				guideBook.setSuccessMessage( e.getMessage() );
			guideBook.setGuideBookName( ThirdPartyDataProvider.getThirdPartyDataProviderDescription( guideBook.getGuideBookId().intValue() ));
		}
	}
	
	request.setAttribute( "bookOutWasSaved", new Boolean( true ) );

}

private BookValuesBookOutState updateBookValuesState( Integer identifier, GuideBookInput input, 
														VDP_GuideBookVehicle selectedVehicle, List<ThirdPartyVehicleOption> options, List<VDP_GuideBookVehicle> vehicles,
														boolean isStoredInfo, Integer bookOutSourceId, Integer thirdPartyId, int searchAppraisalDaysBackThreshold )
		throws GBException
{
	DealerPreference prefs= dealerPrefDAO.findByBusinessUnitId(input.getBusinessUnitId());
	Boolean advertisingStatus= (prefs.getAdvertisingStatus() == null) ? false : prefs.getAdvertisingStatus();
	
	BookValuesBookOutState bookOutState = null;
	if ( bookOutSourceId.intValue() == BookOutSource.BOOK_OUT_SOURCE_VEHICLE_DETAIL.intValue() )
	{
		InventoryWithBookOut inventory = getInventoryBookOutDAO().findByPk( identifier );
		bookOutState = getInventoryBookOutService().updateBookValues( input, selectedVehicle, options, vehicles,
																		AuditBookOuts.MANUAL_BOOKOUT_ID, inventory,
																		thirdPartyId, bookOutSourceId, BookoutStatusEnum.CLEAN ,advertisingStatus);
	}
	else if ( bookOutSourceId.intValue() == BookOutSource.BOOK_OUT_SOURCE_APPRAISAL.intValue() )
	{
		IAppraisal appraisal = appraisalService.findLatest( input.getBusinessUnitId(), input.getVin(), searchAppraisalDaysBackThreshold );
		bookOutState = getAppraisalBookOutService().updateBookValues( input, selectedVehicle, options, vehicles,
																		AuditBookOuts.APPRAISAL_BOOKOUT_ID, appraisal, thirdPartyId );
	}
	else
	{
		bookOutState = getBookOutService().updateBookValues( input, thirdPartyId, selectedVehicle, options, new BookOutDatasetInfo( BookOutTypeEnum.APPRAISAL, input.getBusinessUnitId()) );
		getAuditBookOutsService().logBookOuts( new Integer( input.getBusinessUnitId() ), input.getMemberId(),
															AuditBookOuts.FLUSAN_BOOKOUT_ID, input.getVin(), thirdPartyId,
															BookOut.DUMMY_BOOK_OUT );
	}
	return bookOutState;
}

public DealerPreferenceDAO getDealerPrefDAO()
{
	return dealerPrefDAO;
}

public void setDealerPrefDAO( DealerPreferenceDAO dealerPrefDAO )
{
	this.dealerPrefDAO = dealerPrefDAO;
}

public IInventoryBookOutDAO getInventoryBookOutDAO()
{
	return inventoryBookOutDAO;
}

public void setInventoryBookOutDAO( IInventoryBookOutDAO inventoryBookOutDAO )
{
	this.inventoryBookOutDAO = inventoryBookOutDAO;
}

public InventoryBookOutService getInventoryBookOutService()
{
	return inventoryBookOutService;
}

public void setInventoryBookOutService( InventoryBookOutService inventoryBookOutService )
{
	this.inventoryBookOutService = inventoryBookOutService;
}

public BookOutService getBookOutService()
{
	return bookOutService;
}

public void setBookOutService( BookOutService bookOutService )
{
	this.bookOutService = bookOutService;
}

public AppraisalBookOutService getAppraisalBookOutService()
{
	return appraisalBookOutService;
}

public void setAppraisalBookOutService( AppraisalBookOutService appraisalBookOutService )
{
	this.appraisalBookOutService = appraisalBookOutService;
}

protected IAppraisalService getAppraisalService()
{
	return appraisalService;
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

public AuditBookOutsService getAuditBookOutsService()
{
	return auditBookOutsService;
}

public void setAuditBookOutsService( AuditBookOutsService auditBookOutsService )
{
	this.auditBookOutsService = auditBookOutsService;
}

}
