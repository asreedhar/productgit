package com.firstlook.action.dealer.scorecard;

import com.firstlook.aet.aggregate.model.VehicleSegment;

public class VehicleSegmentScoreCardData extends ScoreCardData
{

private static final String PERCENT_SALES = "Percent Of Sales";
private static final String RETAIL_AVG_GROSS_SALES = "Retail Average Gross Sales";
private static final String AVG_DAYS_SALE = "Average Days to Sale";

private VehicleSegment vehicleSegment;
private Integer percentSalesTrend = new Integer(2);
private Integer percentSales12Week = new Integer(3);
private Integer retailAvgGrossProfitTrend = new Integer(5);
private Integer retailAvgGrossProfit12Week = new Integer(6);
private Integer avgDaysToSaleTrend = new Integer(8);
private Integer avgDaysToSale12Week = new Integer(9);

public VehicleSegmentScoreCardData( VehicleSegment vehicleSegment )
{
    this.vehicleSegment = vehicleSegment;
}

public Integer getAvgDaysToSale12Week()
{
    return avgDaysToSale12Week;
}

public Integer getAvgDaysToSaleTarget()
{
    return getTargetValue(vehicleSegment.getDescription() + " " + AVG_DAYS_SALE);
}

public Integer getAvgDaysToSaleTrend()
{
    return avgDaysToSaleTrend;
}

public Integer getPercentSalesTarget()
{
    return getTargetValue(vehicleSegment.getDescription() + " " + PERCENT_SALES);
}

public Integer getPercentSalesTrend()
{
    return percentSalesTrend;
}

public Integer getPercentSales12Week()
{
    return percentSales12Week;
}

public Integer getRetailAvgGrossProfit12Week()
{
    return retailAvgGrossProfit12Week;
}

public Integer getRetailAvgGrossProfitTarget()
{
    return getTargetValue(vehicleSegment.getDescription() + " "
            + RETAIL_AVG_GROSS_SALES);
}

public Integer getRetailAvgGrossProfitTrend()
{
    return retailAvgGrossProfitTrend;
}

public void setAvgDaysToSale12Week( Integer integer )
{
    avgDaysToSale12Week = integer;
}

public void setAvgDaysToSaleTrend( Integer integer )
{
    avgDaysToSaleTrend = integer;
}

public void setPercentSalesTrend( Integer integer )
{
    percentSalesTrend = integer;
}

public void setPercentSales12Week( Integer integer )
{
    percentSales12Week = integer;
}

public void setRetailAvgGrossProfit12Week( Integer integer )
{
    retailAvgGrossProfit12Week = integer;
}

public void setRetailAvgGrossProfitTrend( Integer integer )
{
    retailAvgGrossProfitTrend = integer;
}

public VehicleSegment getVehicleSegment()
{
    return vehicleSegment;
}

}
