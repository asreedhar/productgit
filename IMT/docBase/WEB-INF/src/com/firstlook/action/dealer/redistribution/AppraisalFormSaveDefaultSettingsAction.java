package com.firstlook.action.dealer.redistribution;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.service.appraisal.AppraisalFormOptions;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;
import com.firstlook.service.dealer.IDealerService;

public class AppraisalFormSaveDefaultSettingsAction extends SecureBaseAction
{	
	@Override
	public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
			throws DatabaseException, ApplicationException
	{
		IDealerService dealerService = getImtDealerService();
		SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
		SessionHelper.keepAttribute( request, "bookoutDetailForm" );
		
		int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
		Dealer dealer = dealerService.retrieveDealer( currentDealerId );

		dealer.getDealerPreference().setAppraisalFormShowOptions( request.getParameter( "options" ) == null ? false : true );
		dealer.getDealerPreference().setAppraisalFormShowPhotos( request.getParameter( "photos" ) == null ? false : true );
		String[] categoriesToDisplayString = request.getParameterValues( "categoriesToDisplay" );
		Integer[] categoriesToDisplay = null;
		if ( categoriesToDisplayString != null ) {
			categoriesToDisplay = new Integer[categoriesToDisplayString.length ]; 
			for (int c = 0; c < categoriesToDisplayString.length; c++ ) {
				categoriesToDisplay[c] = Integer.parseInt( categoriesToDisplayString[c] );
			}
			
		}
		dealer.getDealerPreference().setAppraisalFormValuesTPCBitMask( AppraisalFormOptions.getBitMaskFromThirdPartyCategories( categoriesToDisplay ) );
		dealer.getDealerPreference().setAppraisalFormIncludeMileageAdjustment( request.getParameter( "includeMileageAdjustment" ) == null ? false : true );
		
		dealerService.save( dealer );
		return null;
	}
}
