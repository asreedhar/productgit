package com.firstlook.action.dealer.tools;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.vehicle.description.old.BookOutDatasetInfo;
import biz.firstlook.module.vehicle.description.old.BookOutTypeEnum;
import biz.firstlook.module.vehicle.description.old.KBBBookoutService;
import biz.firstlook.module.vehicle.description.old.KBBConditionEnum;
import biz.firstlook.transact.persist.enumerator.ThirdPartySubCategoryEnum;
import biz.firstlook.transact.persist.exception.SelectedThirdPartyVehicleNotFoundException;
import biz.firstlook.transact.persist.model.BookOut;
import biz.firstlook.transact.persist.model.BookOutThirdPartyCategory;
import biz.firstlook.transact.persist.model.BookOutValue;
import biz.firstlook.transact.persist.model.BookOutValueType;
import biz.firstlook.transact.persist.model.InventoryTypeEnum;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;
import biz.firstlook.transact.persist.model.ThirdPartyVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOptionValue;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOptionValueType;
import biz.firstlook.transact.persist.person.IPerson;
import biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum;
import biz.firstlook.transact.persist.service.appraisal.AppraisalWindowSticker;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.dealer.tools.GuideBookReturnValue;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.form.AppraisalForm;
import com.firstlook.entity.form.ThirdPartyVehicleOptionForm;
import com.firstlook.entity.form.TradeAnalyzerForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;
import com.firstlook.persistence.states.IStatesDAO;
import com.firstlook.persistence.states.StatesDAO;
import com.firstlook.report.Report;
import com.firstlook.service.bookout.BookOutService;

public class PrintableTradeAnalyzerDisplayAction extends SecureBaseAction
{

private IAppraisalService appraisalService;

// this is needed for figuring out which KBB options were used in computing the final options adjustment
private KBBBookoutService kbbBookoutService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws ApplicationException, DatabaseException
{
	int appraisalId = Integer.parseInt(request.getParameter("appraisalId"));
	boolean guideBookPrimary = Boolean.parseBoolean(request.getParameter("guideBookPrimary"));

	AppraisalWindowSticker appraisalWindowSticker = null;
	TradeAnalyzerForm tradeAnalyzerForm = (TradeAnalyzerForm)form;
	setIncludeDealerGroupInfo( request, tradeAnalyzerForm );

	IAppraisal appraisal = getAppraisalService().findBy( appraisalId );
	List< ThirdPartyVehicleOption > appraisalGuideBookOptions = new ArrayList< ThirdPartyVehicleOption >();
	List< ThirdPartyVehicleOption > appraisalGuideBookEngines = new ArrayList< ThirdPartyVehicleOption >();
	List< ThirdPartyVehicleOption > appraisalGuideBookDrivetrains = new ArrayList< ThirdPartyVehicleOption >();
	List< ThirdPartyVehicleOption > appraisalGuideBookTransmissions = new ArrayList< ThirdPartyVehicleOption >();

	Dealer dealer = getImtDealerService().retrieveDealer( appraisal.getBusinessUnitId().intValue() );
	int guideBookId;
	if ( guideBookPrimary )
	{
		guideBookId = dealer.getGuideBookId();
	}
	else
	{
		guideBookId = dealer.getGuideBook2Id();
	}
	createGuideBookFormFromAppraisal( appraisal, tradeAnalyzerForm, request, guideBookId );

	BookOutService.populateSelectedOptionsList( guideBookId, appraisalGuideBookOptions, appraisalGuideBookEngines,
												appraisalGuideBookDrivetrains, appraisalGuideBookTransmissions, appraisal );

	// so the above method does not handle KBB's apply negative values when standard option is missing logic.
	// don't want to change the above method since it is used correctly for display default selected options
	// this method below compiles the list of options whose values were applied - meaning
	// non-standard selected options (like DVD players) and missing standard options ( like -300 for no air conditioning)
	if ( guideBookId == ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE )
	{
		BookOut previousBookout = appraisal.getLatestBookOut( guideBookId );
		ThirdPartyVehicle selectedVehicle = null;
		if( previousBookout != null ){
			try {
				selectedVehicle = previousBookout.getSelectedThirdPartyVehicle();
		
				KBBConditionEnum condition = getConditionByOffLatestBookout( appraisal );
				appraisalGuideBookOptions = updateKelleysGuideBookOptions( selectedVehicle.getThirdPartyVehicleOptions(),
																			appraisal.getBusinessUnitId() );

				// remove not selected conditions so when we print trade-in values - we only have the tradein value associaated with the condition
				// instead of all 3
				removeNonSelectedConditionsValues( appraisalGuideBookOptions, condition );
				removeNonSelectedConditionsValues( appraisalGuideBookEngines, condition );
				removeNonSelectedConditionsValues( appraisalGuideBookTransmissions, condition );
				removeNonSelectedConditionsValues( appraisalGuideBookDrivetrains, condition );
			} catch (SelectedThirdPartyVehicleNotFoundException e) {

			}		
		}
	}

	List<ThirdPartyVehicleOption> allOptions = createAllOptionsList( appraisalGuideBookOptions, appraisalGuideBookEngines, appraisalGuideBookDrivetrains,
											appraisalGuideBookTransmissions );
	appraisalWindowSticker = appraisal.getAppraisalWindowSticker();

	if ( appraisalWindowSticker != null )
	{
		request.setAttribute( "windowStickerId", appraisalWindowSticker.getAppraisalWindowStickerId() );
		request.setAttribute( "stockNumber", appraisalWindowSticker.getStockNumber() );
		request.setAttribute( "sellingPrice", appraisalWindowSticker.getSellingPrice() );
	}
	setIncludeDealerGroup( tradeAnalyzerForm, request );

	putOptionsInRequest( request, appraisal, appraisalGuideBookOptions, appraisalGuideBookEngines, appraisalGuideBookDrivetrains,
							appraisalGuideBookTransmissions, allOptions, guideBookId );
	request.setAttribute( "nickname", dealer.getNickname() );
	
	//big hack here to get print pages working. i'm sorry. -bf
	ThirdPartyDataProvider provider = ThirdPartyDataProvider.getThirdPartyDataProvider(guideBookId);
	if(ThirdPartyDataProvider.BLACKBOOK.equals(provider)) {
		request.setAttribute( "showBlackBookPrintPage", true );
	} else if (ThirdPartyDataProvider.NADA.equals(provider)) {
		
		String NADAReportType = request.getParameter("NADAReportType");
		if ( NADAReportType == null ) {
			NADAReportType = "NADAValues";
		}

		request.setAttribute( "NADAReportType", NADAReportType );

		if(NADAReportType.equals("nadaValues")) {
			request.setAttribute("showNadaValues", true);
		}
		
		if(NADAReportType.equals("nadaTradeValues")) {
			request.setAttribute("showNadaTradeValues", true);
		}

		if(NADAReportType.equals("nadaAppraisal")) {
			request.setAttribute("showNadaAppraisal", true);
		}

		if(NADAReportType.equals("nadaVehicleDescription")) {
			request.setAttribute("showNadaVehicleDescription", true);
		}

		if(NADAReportType.equals("nadaLoanSummary")) {
			request.setAttribute("showNadaLoanSummary", true);
		}

		if(NADAReportType.equals("nadaWholesale")) {
			request.setAttribute("showNadaWholesale", true);
		}
		
		request.setAttribute( "showNADAPrintPage", true );
	} else if (ThirdPartyDataProvider.KELLEY.equals(provider)) {
		
		String kbbReportType = request.getParameter("KBBreportType");
		if ( kbbReportType == null ) {
			kbbReportType = "retail_wholesale";
		}

		request.setAttribute( "KBBReportType", kbbReportType );

		
		if(kbbReportType.equals("retail_wholesale")) {
			request.setAttribute( "showKelleyWholesaleRetail", true );
		}
		if(kbbReportType.equals("tradeIn")) {
			request.setAttribute( "showKelleyTradeInReport", true );
		}
		if(kbbReportType.equals("equity")) {
			request.setAttribute( "showKelleyEquityBreakdown", true );
		}
		if(kbbReportType.equals("wholesale")) {
			request.setAttribute( "showKelleyWholesaleBreakdown", true );
		}
		if(kbbReportType.equals("retail")) {
			request.setAttribute( "showKelleyRetailBreakdown", true );
		}
	} else if (ThirdPartyDataProvider.GALVES.equals(provider)) {
		request.setAttribute( "showGalvesPrintPage", true );
	} else {
		//error, unknown book specified!
	}
	
	request.setAttribute( "currentYear", Calendar.getInstance().get(Calendar.YEAR) );
	
	SessionHelper.setAttribute( request, "tradeAnalyzerForm", tradeAnalyzerForm );
	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );
	return mapping.findForward( "success" );
}

private void removeNonSelectedConditionsValues( List<ThirdPartyVehicleOption> options, KBBConditionEnum condition )
{
	Iterator<ThirdPartyVehicleOption> optionsIter = options.iterator();
	ThirdPartyVehicleOption option;
	List< ThirdPartyVehicleOptionValue > valuesToRemove = null;

	while ( optionsIter.hasNext() )
	{
		option = (ThirdPartyVehicleOption)optionsIter.next();
		valuesToRemove = new ArrayList< ThirdPartyVehicleOptionValue >();

		Iterator<ThirdPartyVehicleOptionValue> optionValuesIter = option.getOptionValues().iterator();
		ThirdPartyVehicleOptionValue value;
		while ( optionValuesIter.hasNext() )
		{
			value = (ThirdPartyVehicleOptionValue)optionValuesIter.next();
			if ( ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_EXCELLENT.equals(value.getThirdPartyVehicleOptionValueType())
					&& !KBBConditionEnum.EXCELLENT.equals( condition ) ) {
				valuesToRemove.add( value );
			}
			else if ( ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_GOOD.equals(value.getThirdPartyVehicleOptionValueType())
					&& !KBBConditionEnum.GOOD.equals( condition ) ) {
				valuesToRemove.add( value );
			}
			else if ( ThirdPartyVehicleOptionValueType.TRADE_IN_OPTION_VALUE_TYPE_FAIR.equals(value.getThirdPartyVehicleOptionValueType())
					&& !KBBConditionEnum.FAIR.equals( condition ) )
			{
				valuesToRemove.add( value );
			}
		}
		option.getOptionValues().removeAll( valuesToRemove );
	}
}

private KBBConditionEnum getConditionByOffLatestBookout( IAppraisal appraisal )
{
	BookOut latestBookout = appraisal.getLatestBookOut( ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE );

	// this can happen when vehicle is not in KBB
	if ( latestBookout == null || latestBookout.getBookOutThirdPartyCategories() == null )
	{
		return KBBConditionEnum.GOOD;
	}
	Iterator<BookOutThirdPartyCategory> btpcIter = latestBookout.getBookOutThirdPartyCategories().iterator();

	while ( btpcIter.hasNext() )
	{
		BookOutThirdPartyCategory btpc = (BookOutThirdPartyCategory)btpcIter.next();
		if ( btpc.getThirdPartyCategory().getThirdPartyCategoryId().intValue() == ThirdPartyCategory.KELLEY_TRADEIN_TYPE )
		{
			Iterator<BookOutValue> bvIter = btpc.getBookOutValues().iterator();
			while ( bvIter.hasNext() )
			{
				BookOutValue bv = (BookOutValue)bvIter.next();
				if ( bv.getThirdPartySubCategoryId() != null )
				{
					if ( bv.getThirdPartySubCategoryId().intValue() == ThirdPartySubCategoryEnum.KBB_TRADEIN_EXCELLENT.getThirdPartySubCategoryId().intValue() )
					{
						return KBBConditionEnum.EXCELLENT;
					}
					else if ( bv.getThirdPartySubCategoryId().intValue() == ThirdPartySubCategoryEnum.KBB_TRADEIN_GOOD.getThirdPartySubCategoryId().intValue() )
					{
						return KBBConditionEnum.GOOD;
					}
					else if ( bv.getThirdPartySubCategoryId().intValue() == ThirdPartySubCategoryEnum.KBB_TRADEIN_FAIR.getThirdPartySubCategoryId().intValue() )
					{
						return KBBConditionEnum.FAIR;
					}
				}
			}
		}
	}
	return KBBConditionEnum.GOOD;
}

private List< ThirdPartyVehicleOption > updateKelleysGuideBookOptions( List< ThirdPartyVehicleOption > thirdPartyVehicleOptions, Integer businessUnitId )
{
	// use set to make sure option can't be added (displayed) twice
	Set< ThirdPartyVehicleOption > uniqueSetOfOptionsToDisplay = new LinkedHashSet< ThirdPartyVehicleOption >();

	Iterator< ThirdPartyVehicleOption > optionsIter = thirdPartyVehicleOptions.iterator();
	ThirdPartyVehicleOption option;

	List< String > optionsWhoseValuesWereIncludedInFinalOptionsAdjustment = kbbBookoutService.getOptionSelections(
																																	thirdPartyVehicleOptions,
																																	new BookOutDatasetInfo( BookOutTypeEnum.APPRAISAL,
																																							businessUnitId ) );

	while ( optionsIter.hasNext() )
	{
		option = optionsIter.next();
		if(option.getOption().getThirdPartyOptionType().equals(ThirdPartyOptionType.EQUIPMENT)) {
			// add all options whose adjustments were added
			if (optionsWhoseValuesWereIncludedInFinalOptionsAdjustment.contains(option.getOptionKey()))	{
				uniqueSetOfOptionsToDisplay.add( option );
			}
	
			// also added selected options that are standars (these are displayed as 'included' on printout
			if (option.isStandardOption() && option.isStatus() ) {
				uniqueSetOfOptionsToDisplay.add( option );
			}
		}
	}

	List< ThirdPartyVehicleOption > optionsToDisplayOnPrintout = new ArrayList< ThirdPartyVehicleOption >();
	optionsToDisplayOnPrintout.addAll( uniqueSetOfOptionsToDisplay );
	return optionsToDisplayOnPrintout;
}

private void setIncludeDealerGroupInfo( HttpServletRequest request, TradeAnalyzerForm tradeAnalyzerForm )
{
	if ( request.getParameter( "includeDealerGroup" ) != null )
	{
		if ( Integer.parseInt( request.getParameter( "includeDealerGroup" ) ) == Report.DEALERGROUP_INCLUDE_TRUE )
		{
			tradeAnalyzerForm.setIncludeDealerGroup( Report.DEALERGROUP_INCLUDE_TRUE );
		}
		else if ( Integer.parseInt( request.getParameter( "includeDealerGroup" ) ) == Report.DEALERGROUP_INCLUDE_FALSE )
		{
			tradeAnalyzerForm.setIncludeDealerGroup( Report.DEALERGROUP_INCLUDE_FALSE );
		}
	}
}

private void putOptionsInRequest( HttpServletRequest request, IAppraisal appraisal, Collection<ThirdPartyVehicleOption> appraisalGuideBookOptions,
									Collection<ThirdPartyVehicleOption> appraisalGuideBookEngines, Collection<ThirdPartyVehicleOption> appraisalGuideBookDrivetrains,
									Collection<ThirdPartyVehicleOption> appraisalGuideBookTransmissions, List<ThirdPartyVehicleOption> allOptions, int guideBookId ) throws ApplicationException
{
	BookOut bookOut = appraisal.getLatestBookOut( guideBookId );
	request.setAttribute( "guideBookOptions", getFormCollection( appraisalGuideBookOptions ) );
	request.setAttribute( "guideBookEngines", getFormCollection( appraisalGuideBookEngines ) );
	request.setAttribute( "guideBookDrivetrains", getFormCollection( appraisalGuideBookDrivetrains ) );
	request.setAttribute( "guideBookTransmissions", getFormCollection( appraisalGuideBookTransmissions ) );
	if ( guideBookId != ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE )
	{
		request.setAttribute( "guideBookOptionsTotal", calculateOptionsTotal( allOptions ) );
	}

	IStatesDAO statesPersistence = new StatesDAO();
	AppraisalForm appraisalForm = new AppraisalForm( appraisal, guideBookId );
	if ( bookOut != null )
	{
		if ( guideBookId == ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE && bookOut.getDatePublished() != null )
		{
			appraisalForm.setPublishState( statesPersistence.findStateLongName( bookOut.getDatePublished().substring( 0, 2 ) ) );			
		}
		
		boolean bookValuesAvailable = false;
		Set<BookOutThirdPartyCategory> thirdPartyCategories = bookOut.getBookOutThirdPartyCategories();
		if(thirdPartyCategories != null) {
			for(BookOutThirdPartyCategory category : thirdPartyCategories) {
				Set<BookOutValue> values = category.getBookOutValues();
				if(values != null && !values.isEmpty()) {
					bookValuesAvailable = true;
					break;
				}
			}
		}
		request.setAttribute( "bookValuesAvailable", bookValuesAvailable);
	}
	request.setAttribute( "appraisalForm", appraisalForm );
}

private List<ThirdPartyVehicleOptionForm> getFormCollection( Collection<ThirdPartyVehicleOption> options )
{
	Iterator<ThirdPartyVehicleOption> iter = options.iterator();
	List<ThirdPartyVehicleOptionForm> rtrnList = new ArrayList<ThirdPartyVehicleOptionForm>();
	ThirdPartyVehicleOption option;
	ThirdPartyVehicleOptionForm form;
	while ( iter.hasNext() )
	{
		option = iter.next();
		form = new ThirdPartyVehicleOptionForm( option );
		rtrnList.add( form );
	}
	return rtrnList;
}

private Integer calculateOptionsTotal( Collection<ThirdPartyVehicleOption> appraisalGuideBookOptions ) throws ApplicationException
{
	Iterator<ThirdPartyVehicleOption> optionsIter = appraisalGuideBookOptions.iterator();
	int totalOptionValue = 0;
	ThirdPartyVehicleOption option;
	ThirdPartyVehicleOptionValue optionValue;
	while ( optionsIter.hasNext() )
	{
		option = (ThirdPartyVehicleOption)optionsIter.next();
		optionValue = option.determineValueByType( ThirdPartyVehicleOptionValueType.NA_OPTION_VALUE_TYPE );
		if ( optionValue != null && optionValue.getValue().intValue() != Integer.MIN_VALUE )
		{
			totalOptionValue += optionValue.getValue().intValue();
		}
	}
	return new Integer( totalOptionValue );
}

private void createGuideBookFormFromAppraisal( IAppraisal appraisal, TradeAnalyzerForm tradeAnalyzerForm, HttpServletRequest request,
												int guideBookId ) throws ApplicationException, DatabaseException
{
	BookOut bookOut = appraisal.getLatestBookOut( guideBookId );
	tradeAnalyzerForm.setActionId( appraisal.getAppraisalActionType().getAppraisalActionTypeId() );
	tradeAnalyzerForm.setAppraisalInitials( appraisal.getLatestAppraisalValue() != null ? appraisal.getLatestAppraisalValue().getAppraiserName()
			: "" );
	tradeAnalyzerForm.setAppraisalValue( appraisal.getLatestAppraisalValue() != null ? appraisal.getLatestAppraisalValue().getValue() : null );
	tradeAnalyzerForm.setColor( appraisal.getColor() );
	tradeAnalyzerForm.setConditionDisclosure( appraisal.getConditionDescription() );
	//purchasing appraisal does not have dealtrack
	if (AppraisalTypeEnum.TRADE_IN == AppraisalTypeEnum.getType(appraisal.getAppraisalTypeId())) {
		tradeAnalyzerForm.setDealTrackNewOrUsed( appraisal.getDealTrackNewOrUsed().ordinal() );
	}

	IPerson salesPerson = null;
	try
	{
		salesPerson = getPersonService().getPerson( Integer.parseInt( tradeAnalyzerForm.getDealTrackSalespersonID() ) );
	}
	catch ( NumberFormatException nfe )
	{
		// do nothing, leave null
	}
	appraisal.setPotentialDeal( salesPerson, tradeAnalyzerForm.getDealTrackStockNumber(),
								InventoryTypeEnum.getInventoryTypeEnum( tradeAnalyzerForm.getDealTrackNewOrUsed() ) );
	appraisal.setDateModified(new Date());

	tradeAnalyzerForm.setDealTrackStockNumber( appraisal.getDealTrackStockNumber() );

	tradeAnalyzerForm.setMileage( new Integer( appraisal.getMileage() ) );
	tradeAnalyzerForm.setVin( appraisal.getVehicle().getVin() );
	tradeAnalyzerForm.setPrice( appraisal.getWholesalePrice().intValue() );
	tradeAnalyzerForm.setReconditioningCost( appraisal.getEstimatedReconditioningCost() );
	tradeAnalyzerForm.setTargetGrossProfit(appraisal.getTargetGrossProfit());
	tradeAnalyzerForm.setYear( appraisal.getVehicle().getVehicleYear().intValue() );
	String make = tradeAnalyzerForm.getMake();
	if ( make == null || make == "")
	{
		make = appraisal.getVehicle().getMakeModelGrouping().getMake();
		tradeAnalyzerForm.setMake( make );
	}
	String model = determineModel( appraisal );
	tradeAnalyzerForm.setModel( model );
	tradeAnalyzerForm.setTrim( appraisal.getVehicle().getVehicleTrim() );
	if ( make != null && model != null )
	{
		tradeAnalyzerForm.setMakeModelGroupingId( appraisal.getVehicle().getMakeModelGrouping().getMakeModelGroupingId() );
		tradeAnalyzerForm.setGroupingDescriptionId( appraisal.getVehicle().getMakeModelGrouping().getGroupingDescription().getGroupingDescriptionId().intValue() );
	}

	Collection<BookOutValue> appraisalGuideBookFinalValues = BookOutService.determineAppropriateBookOutValuesByType(
																										bookOut,
																										BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE );
	Iterator<BookOutValue> finalValuesIter = appraisalGuideBookFinalValues.iterator();
	List<GuideBookReturnValue> guideBookValues = new ArrayList<GuideBookReturnValue>( appraisalGuideBookFinalValues.size() );
	BookOutValue value;
	while ( finalValuesIter.hasNext() )
	{
		value = (BookOutValue)finalValuesIter.next();
		GuideBookReturnValue gbv = new GuideBookReturnValue();
		gbv.setTitle( value.getThirdPartyCategory().getThirdPartyCategory().getCategory() );
		gbv.setValue( value.getValue() );

		// list needs to be sorted by Extra Clean, Clean, Average, Rough so
		// jsp displays properly
		int newIndex = getIndexBasedOnThirdPartyCategory( guideBookValues, value );
		guideBookValues.add( newIndex, gbv );

		if ( guideBookId == ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE )
		{
			setKelleyFinalValues( request, gbv );
			setKelleyNoMileageValues( request, gbv, value.getValue(), bookOut.getMileageCostAdjustment().intValue() );
		}
	}

	Collection<BookOutValue> appraisalGuideBookBaseValues = BookOutService.determineAppropriateBookOutValuesByType(
																										bookOut,
																										BookOutValueType.BOOK_OUT_VALUE_BASE_VALUE );

	if ( ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE == guideBookId )
	{
		Iterator<BookOutValue> baseValuesIter = appraisalGuideBookBaseValues.iterator();
		while ( baseValuesIter.hasNext() )
		{
			value = (BookOutValue)baseValuesIter.next();
			GuideBookReturnValue gbv = new GuideBookReturnValue();
			gbv.setTitle( value.getThirdPartyCategory().getThirdPartyCategory().getCategory() );
			gbv.setValue( value.getValue() );
			setKelleyBaseValues( request, gbv );
		}
	}

	request.setAttribute( "guideBookValues", guideBookValues );
	request.setAttribute( "guideBookValueAvailable", Boolean.TRUE );
	request.setAttribute( "guideBookValuesSize", new Integer( guideBookValues.size() ) );

}

private int getIndexBasedOnThirdPartyCategory( List<GuideBookReturnValue> guideBookValues, BookOutValue value )
{
	boolean needToInsert = false;
	int newIndex = 0;
	for ( int i = 0; i < guideBookValues.size(); i++ )
	{
		GuideBookReturnValue currentGBV = (GuideBookReturnValue)guideBookValues.get( i );
		int currentCategoryId = ThirdPartyCategory.getThirdPartyCategoryIdFromDescription( currentGBV.getTitle() );
		// new one is less than current index
		if ( value.getThirdPartyCategory().getThirdPartyCategory().getThirdPartyCategoryId().intValue() < currentCategoryId )
		{
			newIndex = i;
			needToInsert = true;
			break;
		}
	}
	if ( !needToInsert )
	{
		newIndex = guideBookValues.size();
	}
	return newIndex;
}

private List<ThirdPartyVehicleOption> createAllOptionsList( Collection<ThirdPartyVehicleOption> options, Collection<ThirdPartyVehicleOption> engines, Collection<ThirdPartyVehicleOption> drivetrains, Collection<ThirdPartyVehicleOption> transmissions )
{
	List<ThirdPartyVehicleOption> returnList = new ArrayList<ThirdPartyVehicleOption>();
	returnList.addAll( options );
	returnList.addAll( engines );
	returnList.addAll( drivetrains );
	returnList.addAll( transmissions );

	return returnList;
}

private void setKelleyBaseValues( HttpServletRequest request, GuideBookReturnValue gbv )
{
	if ( gbv.getTitle().equals( ThirdPartyCategory.KELLEY_WHOLESALE_DESCRIPTION ) )
	{
		request.setAttribute( "baseValueWholesale", setToMinValIfNull( gbv.getValue() ) );
	}
	else if ( gbv.getTitle().equals( ThirdPartyCategory.KELLEY_RETAIL_DESCRIPTION ) )
	{
		request.setAttribute( "baseValueRetail", setToMinValIfNull( gbv.getValue() ) );
	}
	else if ( gbv.getTitle().equals( ThirdPartyCategory.KELLEY_TRADEIN_DESCRIPTION ) )
	{
		request.setAttribute( "baseValueTradeIn", setToMinValIfNull( gbv.getValue() ) );
	}
}

// This is a common private method between this and PrintableTradeAnalyzerDisplayAction
private void setKelleyNoMileageValues( HttpServletRequest request, GuideBookReturnValue gbv, Integer bookValue, int mileageAdjustment )
{
	Integer localBookValue = null;
	if ( bookValue != null )
	{
		localBookValue = new Integer( bookValue.intValue() - mileageAdjustment );
	}

	if ( gbv.getTitle().equals( ThirdPartyCategory.KELLEY_WHOLESALE_DESCRIPTION ) )
	{
		request.setAttribute( "noMileageValueWholesale", setToMinValIfNull( localBookValue ) );
	}
	else if ( gbv.getTitle().equals( ThirdPartyCategory.KELLEY_RETAIL_DESCRIPTION ) )
	{
		request.setAttribute( "noMileageValueRetail", setToMinValIfNull( localBookValue ) );
	}
	else if ( gbv.getTitle().equals( ThirdPartyCategory.KELLEY_TRADEIN_DESCRIPTION ) )
	{
		request.setAttribute( "noMileageValueTradeIn", setToMinValIfNull( localBookValue ) );
	}
}

private Integer setToMinValIfNull( Integer value )
{
	if ( value == null )
	{
		return new Integer( Integer.MIN_VALUE );
	}
	return value;
}

private void setKelleyFinalValues( HttpServletRequest request, GuideBookReturnValue gbv )
{
	if ( gbv.getTitle().equals( ThirdPartyCategory.KELLEY_WHOLESALE_DESCRIPTION ) )
	{
		request.setAttribute( "finalValueWholesale", setToMinValIfNull( setToMinValIfNull( gbv.getValue() ) ) );
	}
	else if ( gbv.getTitle().equals( ThirdPartyCategory.KELLEY_RETAIL_DESCRIPTION ) )
	{
		request.setAttribute( "finalValueRetail", setToMinValIfNull( setToMinValIfNull( gbv.getValue() ) ) );
	}
	else if ( gbv.getTitle().equals( ThirdPartyCategory.KELLEY_TRADEIN_DESCRIPTION ) )
	{
		request.setAttribute( "finalValueTradeIn", setToMinValIfNull( setToMinValIfNull( gbv.getValue() ) ) );
	}
}

private String determineModel( IAppraisal appraisal )
{
	String model;
	if ( appraisal.getVehicle().getMakeModelGrouping().getModel() == null )
	{
		model = appraisal.getVehicle().getModel();
	}
	else
	{
		model = appraisal.getVehicle().getMakeModelGrouping().getModel();
	}
	return model;
}

private void setIncludeDealerGroup( TradeAnalyzerForm tradeAnalyzerForm, HttpServletRequest request )
{
	if ( request.getParameter( "showDealerGroup.x" ) != null )
	{
		tradeAnalyzerForm.setIncludeDealerGroup( Report.DEALERGROUP_INCLUDE_TRUE );
	}
	else if ( request.getParameter( "showDealer.x" ) != null )
	{
		tradeAnalyzerForm.setIncludeDealerGroup( Report.DEALERGROUP_INCLUDE_FALSE );
	}

}

public IAppraisalService getAppraisalService()
{
	return appraisalService;
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

public void setKbbBookoutService( KBBBookoutService kbBookoutService )
{
	this.kbbBookoutService = kbBookoutService;
}

}
