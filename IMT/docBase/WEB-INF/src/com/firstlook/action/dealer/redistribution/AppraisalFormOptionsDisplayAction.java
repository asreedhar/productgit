package com.firstlook.action.dealer.redistribution;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.module.vehicle.description.VehicleDescriptionDAO;
import biz.firstlook.module.vehicle.description.Enum.VehicleDescriptionProviderEnum;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.service.ThirdPartyCategoryService;
import biz.firstlook.transact.persist.service.appraisal.AppraisalFormOptions;
import biz.firstlook.transact.persist.service.appraisal.AppraisalValue;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;

public class AppraisalFormOptionsDisplayAction extends SecureBaseAction
{

private IAppraisalService appraisalService;
private ThirdPartyCategoryService thirdPartyCategoryService;
private VehicleDescriptionDAO vehicleDescriptionDAO;

// Look away I'm hideous.
@SuppressWarnings( "unchecked" )
public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );
	
	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	int appraisalId = Integer.parseInt( request.getParameter( "appraisalId" ) );
	int mileage = Integer.parseInt( request.getParameter( "mileage" ) );
	int offer = formatOffer( ( request.getParameter( "offer" ) ) );
	int initialAppraisal = formatOffer( ( request.getParameter( "initialAppraisal" ) ) );
	boolean enableGuideBook1 = request.getParameter( "primaryError" ) == null || request.getParameter( "primaryError" ).equals("false");
	boolean enableGuideBook2 =  request.getParameter( "secondaryError" ) == null || request.getParameter( "secondaryError" ).equals("false");
	
	boolean hasKBBTradeInUpgrade = getFirstlookSessionFromRequest( request ).isIncludeKBBTradeInValues();
	
	String vin = request.getParameter( "vin" );
	String photosWebAppRootURL = PropertyLoader.getProperty( "firstlook.photos.war.web.root" );

	Dealer dealer = getImtDealerService().retrieveDealer( currentDealerId );
	IAppraisal appraisal = getAppraisalService().findBy( appraisalId );
	AppraisalFormOptions formOptions = appraisal.getAppraisalFormOptions();

	Integer customerOffer = null;
	Integer previousOffer = null;
	boolean showPhotos, showOptions,showReconCost;
	
	boolean showKbbConsumerValue = false;
	boolean includeMileageAdjustment = dealer.getDealerPreference().getAppraisalFormIncludeMileageAdjustment();
	Integer[] categoriesToDisplay = null;

	if ( formOptions != null )	{
		if ( offer == -1 ) // if the offer passed in is garbage, try to get it from initial appraisal
		{ 
			if(initialAppraisal == -1) {
				AppraisalValue a =  (AppraisalValue)appraisal.getLatestAppraisalValue();
				if( a != null ){    
					customerOffer = a.getValue();
				}else{  //there is no preexisting appraisal value
					customerOffer = 0;			
				}
			}else {				
				customerOffer = initialAppraisal;
			}
		} else { // use the passed in customer appraisal value
			customerOffer = offer;
		}
		previousOffer = formOptions.getAppraisalFormHistory();
		showPhotos = formOptions.isShowPhotos();
		showOptions = formOptions.isShowEquipmentOptions();
		showReconCost= formOptions.isShowReconCost();
		showKbbConsumerValue = formOptions.isShowKbbConsumerValue();
		includeMileageAdjustment = formOptions.isIncludeMileageAdjustment();
		
		Integer categoriesToDisplayBitMask = formOptions.getCategoriesToDisplayBitMask();
	
		// means you have an appraisal but have never saved an appraisal form expilcitly so use defaults
		if ( categoriesToDisplayBitMask == null ) {
			showPhotos = dealer.getDealerPreference().getAppraisalFormShowPhotos();
			showOptions = dealer.getDealerPreference().getAppraisalFormShowOptions();
			
			
			
			categoriesToDisplayBitMask = dealer.getDealerPreference().getAppraisalFormValuesTPCBitMask();
			includeMileageAdjustment = dealer.getDealerPreference().getAppraisalFormIncludeMileageAdjustment();
		}
		
		categoriesToDisplay = AppraisalFormOptions.getThirdPartyCategoriesFromBitMask( categoriesToDisplayBitMask );
	} else	{
		if ( offer == -1 )
		{ // passed in offer is garbage so retrieve stored value
			if(initialAppraisal == -1) {
				customerOffer = appraisal.getLatestAppraisalValue().getValue();
			}else {
				customerOffer = initialAppraisal;
				
			}
		}
		else
		{
			customerOffer = offer;
		}
		showPhotos = dealer.getDealerPreference().getAppraisalFormShowPhotos();
		showOptions = dealer.getDealerPreference().getAppraisalFormShowOptions();
		showReconCost=true;
		categoriesToDisplay = AppraisalFormOptions.getThirdPartyCategoriesFromBitMask( dealer.getDealerPreference().getAppraisalFormValuesTPCBitMask() );
	}

	int primaryBookId = dealer.getGuideBookId();
	int secondaryBookId = dealer.getGuideBook2Id();

	String primaryBook = ThirdPartyDataProvider.getThirdPartyDataProviderDescription( primaryBookId );
	String secondaryBook = ThirdPartyDataProvider.getThirdPartyDataProviderDescription( secondaryBookId );
	List<ThirdPartyCategory> thirdPartyCategories = thirdPartyCategoryService.retrieveAll();
	thirdPartyCategories.remove(ThirdPartyCategory.KELLEY_TRADEIN_RANGEHIGH);
	thirdPartyCategories.remove(ThirdPartyCategory.KELLEY_TRADEIN_RANGELOW);
	Collections.sort( thirdPartyCategories, new BeanComparator( "thirdPartyCategoryId" ) );
	
	// logic to remove private party from list and also turn 3 trade-in thirdparty categories into one
	trimThirdPartyCategoryies( thirdPartyCategories, hasKBBTradeInUpgrade );
	
	boolean hasKbbConsumerValueUpgrade = (primaryBookId == ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE || secondaryBookId == ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE) 
											? false : getFirstlookSessionFromRequest(request).isIncludeKBBTradeInValues() ;
	boolean consumerValueIsSelectable = (vehicleDescriptionDAO.getVehicleBookoutState(vin, currentDealerId, VehicleDescriptionProviderEnum.KBB) != null) ? true : false;
	request.setAttribute( "hasKbbConsumerValueUpgrade", hasKbbConsumerValueUpgrade);
	request.setAttribute( "consumerValueIsSelectable", consumerValueIsSelectable );
	request.setAttribute( "showKbbConsumerValue", showKbbConsumerValue );
	
	request.setAttribute( "photosWebAppRootURL", photosWebAppRootURL );
	request.setAttribute( "customerOffer", customerOffer );
	request.setAttribute( "previousOffer", previousOffer );
	request.setAttribute( "showOptions", showOptions );
	request.setAttribute("showReconCost", showReconCost);
	request.setAttribute( "showPhotos", showPhotos );
	request.setAttribute( "includeMileageAdjustment", includeMileageAdjustment );
	request.setAttribute( "bookCategories", thirdPartyCategories );
	request.setAttribute( "appraisalId", appraisalId );
	request.setAttribute( "mileage", mileage );
	request.setAttribute( "error", (String)SessionHelper.getAttribute( request, "error" ) );
	request.setAttribute( "vin", vin );

	request.setAttribute( "enablePrimaryBook", enableGuideBook1 );
	request.setAttribute( "enableSecondaryBook", enableGuideBook2 );
	request.setAttribute( "primaryBook", primaryBook );
	request.setAttribute( "secondaryBook", secondaryBook );
	request.setAttribute( "primaryBookId", primaryBookId );
	request.setAttribute( "secondaryBookId", secondaryBookId );
	request.setAttribute( "categoriesToDisplay", categoriesToDisplay );
	SimpleDateFormat sdf= new SimpleDateFormat("MM-dd-yyyy");
	if(appraisal.getCustomer()!=null){
		request.setAttribute("toEmail", appraisal.getCustomer().getEmail());
		request.setAttribute("subjectEmail", "Vehicle Purchase Offer - "+sdf.format(new Date()));
	}
	if ( request.getParameter( "onTM" ) != null && request.getParameter( "onTM" ).equalsIgnoreCase( "true" ) )
	{
		request.setAttribute( "onTM", 1 );
	}
	else
	{
		request.setAttribute( "onTM", 0 );
	}

	return mapping.findForward( "success" );
}

private void trimThirdPartyCategoryies( List< ThirdPartyCategory > thirdPartyCategories, boolean hasKBBTradeInUpgrade )
{
	List< ThirdPartyCategory > toBeRemoved = new ArrayList< ThirdPartyCategory >();
	for ( ThirdPartyCategory tpc : thirdPartyCategories )
	{
		if ( tpc.getThirdPartyId().intValue() == ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE )
		{
			if ( tpc.getThirdPartyCategoryId().intValue() == ThirdPartyCategory.KELLEY_PRIVATE_PARTY_TYPE )
			{
				toBeRemoved.add( tpc );
			}
			if ( !hasKBBTradeInUpgrade && tpc.getThirdPartyCategoryId().intValue() == ThirdPartyCategory.KELLEY_TRADEIN_TYPE )
			{
				toBeRemoved.add( tpc );
			}
		}
	}
	for ( ThirdPartyCategory tpc : toBeRemoved )
	{
		thirdPartyCategories.remove( tpc );
	}
}

private int formatOffer( String offer )
{
	if ( offer == null || offer == "" )
	{
		return -1;
	}
	int indexOfPeriod = offer.indexOf( "." );
	int returnValue = -1;
	if ( indexOfPeriod >= 0 )
	{
		offer = offer.substring( 0, indexOfPeriod );
	}
	int indexOfComma = offer.indexOf( "," );
	if ( indexOfComma >= 0 )
	{
		offer = offer.substring( 0, indexOfComma ) + offer.substring( ( indexOfComma + 1 ), offer.length() );
	}
	int indexOfDollar = offer.indexOf( "$" );
	if ( indexOfDollar >= 0 )
	{
		offer = offer.substring( ( indexOfDollar + 1 ), offer.length() );
	}

	
	try
	{
		returnValue = Integer.parseInt( offer );
	}
	catch ( NumberFormatException exe )
	{
		returnValue = -1;
	}

	return returnValue;
}

public IAppraisalService getAppraisalService()
{
	return appraisalService;
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

public void setThirdPartyCategoryService( ThirdPartyCategoryService thirdPartyCategoryService )
{
	this.thirdPartyCategoryService = thirdPartyCategoryService;
}

public void setVehicleDescriptionDAO(VehicleDescriptionDAO vehicleDescriptionDAO) {
	this.vehicleDescriptionDAO = vehicleDescriptionDAO;
}

}