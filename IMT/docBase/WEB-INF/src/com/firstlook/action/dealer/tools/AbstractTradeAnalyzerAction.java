package com.firstlook.action.dealer.tools;

import java.util.List;

import biz.firstlook.commons.email.EmailService;
import biz.firstlook.transact.persist.model.DemandDealer;
import biz.firstlook.transact.persist.retriever.InGroupDemandRetriever;
import biz.firstlook.transact.persist.service.appraisal.AppraisalActionType;
import biz.firstlook.transact.persist.service.appraisal.AppraisalBusinessUnitGroupDemand;
import biz.firstlook.transact.persist.service.appraisal.AppraisalBusinessUnitGroupDemandDAO;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.dealer.redistribution.HotSheetHelper;
import com.firstlook.email.HotSheetEmailBuilder;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.RedistributionLog;
import com.firstlook.service.dealer.IDealerService;
import com.firstlook.service.redistributionlog.RedistributionLogService;

public abstract class AbstractTradeAnalyzerAction extends SecureBaseAction
{

private AppraisalBusinessUnitGroupDemandDAO appraisalBusinessUnitGroupDemandDAO;
private RedistributionLogService redistributionLogService;
private InGroupDemandRetriever inGroupDemandRetriever;
private HotSheetHelper hotSheetHelper;
private EmailService emailService;

// Until projects/packages are refactored, getting the EmailService injected into the AppraisalService wouldn't be easy at all.
protected void processRedistribution( Integer dealerId, Integer memberId, Integer appraisalId, Integer groupingDescriptionId, Integer mileage,
										Integer vehicleYear, String vin, AppraisalActionType oldAppraisalActionType,
										AppraisalActionType newAppraisalActionType )
{
	// 3 cases:
	// appraisal moved out of OfferToGroup
	if ( oldAppraisalActionType.equals( AppraisalActionType.OFFER_TO_GROUP )
			&& !newAppraisalActionType.equals( AppraisalActionType.OFFER_TO_GROUP ) )
	{
		appraisalBusinessUnitGroupDemandDAO.removeAppraisalInGroupDemand( appraisalId );
	} // appraisal moved into OfferToGroup
	else if ( !oldAppraisalActionType.equals( AppraisalActionType.OFFER_TO_GROUP )
			&& newAppraisalActionType.equals( AppraisalActionType.OFFER_TO_GROUP ) )
	{
		IDealerService dealerService = getImtDealerService();
		Dealer currentDealer = dealerService.retrieveDealer( dealerId );
		Integer distance = currentDealer.getDealerPreference().getRedistributionDealerDistance();
		Integer sortBy = dealerService.determineDemandDealerSortOrder(currentDealer.getBusinessUnitId(), currentDealer.getDealerPreference().getRedistributionUnderstock(), currentDealer.getDealerPreference().getRedistributionROI());
		// add them to showroom
		final List< DemandDealer > demandDealers = inGroupDemandRetriever.getDemandDealers( dealerId, groupingDescriptionId, mileage, vehicleYear, distance, sortBy );

		for ( DemandDealer dealer : demandDealers )
		{
			AppraisalBusinessUnitGroupDemand appraisalGuideBookInGroupDemand = new AppraisalBusinessUnitGroupDemand();
			appraisalGuideBookInGroupDemand.setBusinessUnitId( Integer.valueOf( dealer.getBusinessUnitId() ) );
			appraisalGuideBookInGroupDemand.setAppraisalId( appraisalId );

			appraisalBusinessUnitGroupDemandDAO.insertAppraisalInGroupDemand( appraisalGuideBookInGroupDemand );

			redistributionLogService.save( memberId.intValue(), dealerId.intValue(), dealer.getBusinessUnitId(), "", vin,
											RedistributionLog.APPRAISAL_SOURCE_ID );
		}

		// send out hotsheets
		HotSheetEmailBuilder hotSheetEmailBuilder = hotSheetHelper.getEmail( dealerId, memberId, vehicleYear, groupingDescriptionId, mileage,
																				appraisalId, demandDealers );

		emailService.sendEmail( hotSheetEmailBuilder );
	} // appraisal action (didn't)changed but not into or out of OfferToGroup
	else
	{
		// do nothing
	}
}

public void setHotSheetHelper( HotSheetHelper hotSheetHelper )
{
	this.hotSheetHelper = hotSheetHelper;
}

public void setEmailService( EmailService emailService )
{
	this.emailService = emailService;
}

public EmailService getEmailService() {
	return emailService;
}

public void setRedistributionLogService( RedistributionLogService redistributionLogService )
{
	this.redistributionLogService = redistributionLogService;
}

public void setInGroupDemandRetriever( InGroupDemandRetriever inGroupDemandRetriever )
{
	this.inGroupDemandRetriever = inGroupDemandRetriever;
}

public void setAppraisalBusinessUnitGroupDemandDAO( AppraisalBusinessUnitGroupDemandDAO appraisalBusinessUnitGroupDemandDAO )
{
	this.appraisalBusinessUnitGroupDemandDAO = appraisalBusinessUnitGroupDemandDAO;
}

}
