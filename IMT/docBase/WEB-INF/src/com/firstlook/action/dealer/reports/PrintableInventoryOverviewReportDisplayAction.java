package com.firstlook.action.dealer.reports;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.util.PageBreakHelper;

public class PrintableInventoryOverviewReportDisplayAction extends
		InventoryOverviewReportDisplayAction {

	public ActionForward doIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws DatabaseException, ApplicationException {
		putPageBreakHelperInRequest( PageBreakHelper.INVOVERVIEW_ONLINE, request, getMemberFromRequest( request ).getProgramType() );
		return super.doIt(mapping, form, request, response);
	}

}
