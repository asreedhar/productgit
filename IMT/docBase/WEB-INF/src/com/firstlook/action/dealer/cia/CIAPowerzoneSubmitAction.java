package com.firstlook.action.dealer.cia;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.cia.model.Status;
import biz.firstlook.cia.persistence.ICIASummaryDAO;
import biz.firstlook.cia.service.CIAGroupingItemService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.service.cia.report.CIAReportingUtility;

public class CIAPowerzoneSubmitAction extends SecureBaseAction
{
private CIAGroupingItemService ciaGroupingItemService;
private ICIASummaryDAO ciaSummaryDAO;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	Map parameterMap = request.getParameterMap();

	Set paramNameSet = parameterMap.keySet();

	Collection noteKeys = CollectionUtils.select( paramNameSet, new ParamFilterPredicate( "notes" ) );

	Integer summaryId = getCiaSummaryDAO().retrieveCIASummaryId( new Integer( getFirstlookSessionFromRequest( request ).getCurrentDealerId() ),
																	Status.CURRENT );

	Integer segmentId = new Integer( RequestHelper.getInt( request, "segmentId" ) );
	Integer ciaCategoryId = new Integer( RequestHelper.getInt( request, "ciaCategory" ) );
	Integer groupingDescriptionId = new Integer( RequestHelper.getInt( request, "groupingDescriptionId" ) );

	try
	{
		List ciaGroupingItems = ciaGroupingItemService.retrieveBy( summaryId, segmentId, ciaCategoryId );
		String errorMessage = CIAReportingUtility.updateDetailItemsBasedOnSelectedPreferences( parameterMap, ciaGroupingItems );
		if ( errorMessage == null )
		{
			CIAReportingUtility.createNotes( noteKeys, parameterMap, ciaGroupingItems );

			ciaGroupingItemService.save( ciaGroupingItems );
			
			return mapping.findForward( "success" );
		}
		else
		{
			return putErrorInRequest( mapping, request, ciaCategoryId, groupingDescriptionId, segmentId, errorMessage );
		}

	}
	catch ( Exception e )
	{
		e.printStackTrace();
		return putErrorInRequest( mapping, request, ciaCategoryId, groupingDescriptionId, segmentId, "" );
	}
}

private ActionForward putErrorInRequest( ActionMapping mapping, HttpServletRequest request, Integer ciaCategoryId,
 										Integer groupingDescriptionId, Integer segmentId, String errorMessage )
 {
 	request.setAttribute( "error", errorMessage );
 	request.setAttribute( "groupingDescriptionId", groupingDescriptionId );
 	request.setAttribute( "ciaCategory", ciaCategoryId );
 	request.setAttribute( "segmentId", segmentId );
 	putActionErrorInRequest( request, errorMessage );
 	return mapping.findForward( "failure" );
 }

public CIAGroupingItemService getCiaGroupingItemService()
{
	return ciaGroupingItemService;
}

public void setCiaGroupingItemService( CIAGroupingItemService ciaGroupingItemService )
{
	this.ciaGroupingItemService = ciaGroupingItemService;
}

public ICIASummaryDAO getCiaSummaryDAO()
{
	return ciaSummaryDAO;
}

public void setCiaSummaryDAO( ICIASummaryDAO ciaSummaryDAO )
{
	this.ciaSummaryDAO = ciaSummaryDAO;
}
}