package com.firstlook.action.dealer.helper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import biz.firstlook.commons.services.vehiclehistoryreport.CarfaxException;
import biz.firstlook.commons.services.vehiclehistoryreport.CarfaxService;
import biz.firstlook.services.vehiclehistoryreport.carfax.CarfaxReportTO;
import biz.firstlook.services.vehiclehistoryreport.carfax.VehicleEntityType;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryBucket;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.InventorySalesAggregate;
import com.firstlook.entity.lite.IInventory;
import com.firstlook.exception.ApplicationException;
import com.firstlook.report.AgingReport;
import com.firstlook.report.BaseAgingReport;
import com.firstlook.service.inventory.InventoryService_Legacy;

public class AgingReportHelper
{

// This is crappy.
// This class is not spring managed, but any callers should be and need to 
// pass in the spring managed inventory service to use.
private InventoryService_Legacy inventoryService_Legacy; 

public AgingReportHelper(InventoryService_Legacy inventoryService)
{
	this.inventoryService_Legacy = inventoryService;
}

/***
 * NK - 11/28/2005
 * If you are calling this method - especially if you are using lite = true - check and
 * make sure you can't INSTEAD use AgingReportLite. It just contains RangeIds and InventoryCounts -
 * but if that's all you need...
 * 
 * Used in TIR and Inventory Manager(Dashboard)
 */
public void putAgingReportRangesInRequest( Dealer dealer, HttpServletRequest request, boolean lite, String[] statusCodes,
        InventoryBucket rangeSet, InventorySalesAggregate aggregate ) throws DatabaseException, ApplicationException {
	putAgingReportRangesInRequest(dealer, request, lite, statusCodes, rangeSet, aggregate, null);
}

/**
 * Overload of the method above used by TIR to load Carfax reports.
 */
public void putAgingReportRangesInRequest( Dealer dealer, HttpServletRequest request, boolean lite, String[] statusCodes,
                                          InventoryBucket rangeSet, InventorySalesAggregate aggregate,
                                          String userName) throws DatabaseException, ApplicationException
{
    BaseAgingReport report = new AgingReport( dealer, lite, statusCodes, rangeSet, aggregate, inventoryService_Legacy );
    if (userName != null) {
    	Integer dealerId = dealer.getDealerId();
    	try {
    		CarfaxService service = CarfaxService.getInstance();
    		if (service.hasAccount(dealerId, userName)) {
    			List<CarfaxReportTO> carfaxReports = service.getCarfaxReports(dealerId, userName, VehicleEntityType.INVENTORY);
    	    	Map<Integer, List<IInventory>> vehicleMap = report.getInventories();
    		    for (List<IInventory> vehicles : vehicleMap.values()) {
    		    	for (IInventory vehicle : vehicles) {
    		    		if (vehicle instanceof InventoryEntity) {
    		    			InventoryEntity entity = (InventoryEntity) vehicle;
    		    			try {
    		    				for (CarfaxReportTO carfaxReport : carfaxReports) {
    		    					/**
    		    					 * 8/15/2013 Changed .equals(...) to .equalsIgnoreCase(...) because some VINS
    		    					 * were stored in the DB in lowercase and they where not getting their carfax 
    		    					 * reports.
    		    					 */
    		    					if ( carfaxReport.getVin().equalsIgnoreCase( entity.getVin() ) ) {
    		    						entity.setCarfaxReport(carfaxReport);
    		    						break;
    		    					}
    		    				}
    		    			}
    		    			catch (Exception ex) {
    		    				// Null Is Fine, PM & SW
    		    			}
    		    		}
    		    	}
    		    }
        	}
    	} catch (CarfaxException ce) {
    		Logger.getLogger( AgingReportHelper.class ).error("No CARFAX", ce);
    	}
    }
    request.setAttribute( "agingReport", report );
}

}
