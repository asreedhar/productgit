package com.firstlook.action.dealer.buy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.Inventory;
import biz.firstlook.transact.persist.model.Photo;
import biz.firstlook.transact.persist.persistence.IDealerPreferenceDAO;
import biz.firstlook.transact.persist.persistence.IInventoryDAO;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.Member;
import com.firstlook.entity.RedistributionLog;
import com.firstlook.entity.form.CustomIndicatorForm;
import com.firstlook.entity.form.DealerForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.action.ReportActionHelper;
import com.firstlook.report.CustomIndicator;
import com.firstlook.service.redistributionlog.RedistributionLogService;
import biz.firstlook.services.inventoryPhotos.InventoryPhotosWebServiceClient;

public class VehicleAndSellerDisplayAction extends VehicleDisplayBaseAction
{

private IDealerPreferenceDAO dealerPrefDAO;
private RedistributionLogService redistributionLogService;
private ReportActionHelper reportActionHelper;
private IInventoryDAO inventoryDAO;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    int vehicleId = RequestHelper.getInt( request, "vehicleId" );
    InventoryEntity inventory = putVehicleInRequest( request, vehicleId, getImtDealerService());
    final int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();

    // TODO: cannot use the dealer from inventory until Inventory is
    // spring-managed as well - 05/26
    Dealer sellingDealer = getImtDealerService().retrieveDealer( inventory.getDealerId() );

    DealerForm sellingDealerForm = new DealerForm( sellingDealer );
    request.setAttribute( "sellingDealerForm", sellingDealerForm );

    putViewingDealerInfoInRequest( request, inventory, sellingDealer );
    
	
	redistributionLogService.save( getMemberFromRequest(request).getMemberId().intValue(), inventory.getDealerId(),
                  currentDealerId,
                  inventory.getStockNumber(), inventory.getVin(), RedistributionLog.CIE_SOURCE_ID );

	DealerPreference dealerPreference = dealerPrefDAO.findByBusinessUnitId( currentDealerId );
    request.setAttribute( "displayUnitCostToDealerGroup", dealerPreference.isDisplayUnitCostToDealerGroup());

    //using inventory entity from transact persist to get photos
    String businessUnitCode = sellingDealer.getDealerCode();
    String vin = inventory.getVin();
    List<String> inventoryPhotoUrls = InventoryPhotosWebServiceClient.getInstance().GetInventoryPhotoUrls(businessUnitCode, vin);
    
	request.setAttribute("photos", inventoryPhotoUrls);
	request.setAttribute("numPhotos",  inventoryPhotoUrls.size());
    
    return getActionForward( mapping );
}

void putCustomIndicatorFormInRequest( Dealer dealer, InventoryEntity vehicle, HttpServletRequest request ) throws DatabaseException,
        ApplicationException
{
    int weeks = ReportActionHelper.DEFAULT_NUMBER_OF_WEEKS;
    try
    {
        weeks = Integer.parseInt( request.getParameter( "weeks" ) );
    }
    catch ( NumberFormatException nfe )
    {
        // default to DEFAULT_NUMBER_OF_WEEKS
    }
    CustomIndicator customIndicator = reportActionHelper.getCustomIndicator( dealer, InventoryEntity.USED_CAR, weeks, true );
    CustomIndicatorForm form = new CustomIndicatorForm( customIndicator );
    form.setGroupingDescription( vehicle.getGroupingDescription() );
    request.setAttribute( "customIndicator", form );
}

class PrimaryPhotoComparator implements Comparator<Boolean> {
    public int compare(Boolean b0, Boolean b1) {
        if (b0 == null) {
            if (b1 == null) {
                return 0;
            }
            return 1;
        }
        else {
            if (b1 == null) {
                return -1;
            }
            return b1.compareTo(b0);
        }
    }
}

private Dealer putViewingDealerInfoInRequest( HttpServletRequest request, InventoryEntity inventory, Dealer sellingDealer )
        throws ApplicationException, DatabaseException
{
    Dealer viewingDealer = null;

    if ( !RequestHelper.getBoolean( request, "ignoreDealer" ) )
    {
        viewingDealer = putDealerFormInRequest( request, 0 );
        VehicleDetailActionHelper helper = new VehicleDetailActionHelper();
    	Dealer currentDealer = getImtDealerService().retrieveDealer( getFirstlookSessionFromRequest( request ).getCurrentDealerId() );
    	boolean isAdmin = (getMemberFromRequest(request).getMemberType().intValue() == Member.MEMBER_TYPE_ADMIN);
        helper.setDealerNameMasking( sellingDealer, isAdmin, currentDealer, getDealerGroupService() );

        putCustomIndicatorFormInRequest( viewingDealer, inventory, request );
    }

    return viewingDealer;
}

public IDealerPreferenceDAO getDealerPrefDAO()
{
    return dealerPrefDAO;
}

public void setDealerPrefDAO( IDealerPreferenceDAO dealerPrefDAO )
{
    this.dealerPrefDAO = dealerPrefDAO;
}

public void setRedistributionLogService( RedistributionLogService redistributionLogService )
{
	this.redistributionLogService = redistributionLogService;
}

public void setReportActionHelper( ReportActionHelper reportActionHelper )
{
	this.reportActionHelper = reportActionHelper;
}

public IInventoryDAO getInventoryDAO() {
	return inventoryDAO;
}

public void setInventoryDAO(IInventoryDAO inventoryDAO) {
	this.inventoryDAO = inventoryDAO;
}

}
