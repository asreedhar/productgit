package com.firstlook.action.dealer.buy;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.cia.service.BasisPeriodService;
import biz.firstlook.transact.persist.model.CIAPreferences.TimePeriod;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.MemberForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.action.ReportActionHelper;
import com.firstlook.iterator.VehicleFormIterator;
import com.firstlook.report.CustomIndicator;
import com.firstlook.service.dealer.IUCBPPreferenceService;
import com.firstlook.session.FirstlookSession;
import com.firstlook.util.PageBreakHelper;

public abstract class BaseBuyingGuideDisplayAction extends
        com.firstlook.action.SecureBaseAction
{

private BasisPeriodService basisPeriodService;
private IUCBPPreferenceService ucbpPreferenceService;
private ReportActionHelper reportActionHelper;

protected abstract Collection<InventoryEntity> getVehiclesToDisplay( Dealer dealer,
        CustomIndicator indicator ) throws DatabaseException,
        ApplicationException;

public ActionForward doIt( ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response ) throws DatabaseException,
        ApplicationException
{
    Dealer currentDealer = putDealerFormInRequest( request, 0 );
    request.setAttribute( "dealerGroup", getDealerGroupService().retrieveByDealerId( currentDealer.getDealerId().intValue() ) );
	Member member = getMemberFromRequest(request);
    request.setAttribute("memberForm", new MemberForm(member));

    int weeks = basisPeriodService.calculateNumberOfWeeksInBasisPeriodByType(
            new Integer(((FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION )).getCurrentDealerId()),
            TimePeriod.SALES_HISTORY_DISPLAY, 0);
    CustomIndicator indicator = reportActionHelper.getCustomIndicator(currentDealer,
            InventoryEntity.USED_CAR, weeks, true);

    Collection<InventoryEntity> vehiclesToDisplay = getVehiclesToDisplay(currentDealer,
            indicator);

    putFormIteratorInRequest(request, vehiclesToDisplay, indicator);

    putPageBreakHelperInRequest(PageBreakHelper.TOTAL_EXCHANGE, request, getMemberFromRequest( request ).getProgramType() );

    Integer mileage = new Integer(ucbpPreferenceService.retrieveDealerRisk(
            ((FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION )).getCurrentDealerId()).getHighMileageThreshold());
    request.setAttribute("mileage", mileage);
    request.setAttribute("weeks", new Integer(weeks));

    return mapping.findForward("success");
}


public void putFormIteratorInRequest( HttpServletRequest request,
        Collection<InventoryEntity> vehiclesToDisplay, CustomIndicator indicator )
        throws DatabaseException, ApplicationException
{
	VehicleFormIterator submittedVehicles = new VehicleFormIterator( vehiclesToDisplay, indicator );
	request.setAttribute( "submittedVehicles", submittedVehicles );

}

public BasisPeriodService getBasisPeriodService()
{
	return basisPeriodService;
}

public void setBasisPeriodService( BasisPeriodService basisPeriodService )
{
	this.basisPeriodService = basisPeriodService;
}

public void setUcbpPreferenceService( IUCBPPreferenceService ucbpPreferenceService )
{
	this.ucbpPreferenceService = ucbpPreferenceService;
}

public void setReportActionHelper( ReportActionHelper reportActionHelper ) {
	this.reportActionHelper = reportActionHelper;
}

}
