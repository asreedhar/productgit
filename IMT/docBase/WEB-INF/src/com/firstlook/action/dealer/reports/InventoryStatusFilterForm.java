package com.firstlook.action.dealer.reports;

import org.apache.commons.lang.StringUtils;

import com.firstlook.action.BaseActionForm;

public class InventoryStatusFilterForm extends BaseActionForm
{

private static final long serialVersionUID = -8329523130613649959L;
private String[] inventoryStatusCD;

public InventoryStatusFilterForm()
{
    super();
}

public String[] getInventoryStatusCD()
{
    return inventoryStatusCD;
}

public void setInventoryStatusCD( String[] i )
{
    inventoryStatusCD = i;
}

public String getInventoryStatusCDStr()
{
    if ( inventoryStatusCD != null )
    {
        return StringUtils.join(inventoryStatusCD, ",");
    }
    return "";
}

}
