package com.firstlook.action.dealer.cia;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.cia.model.CIACategory;
import biz.firstlook.cia.persistence.ICIASummaryDAO;
import biz.firstlook.transact.persist.persistence.IDealerPreferenceDAO;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.cia.CIAService;
import com.firstlook.service.cia.transactional.CIADetailPageDisplayItemWithPrefs;

public class CIADetailDisplayAction extends SecureBaseAction
{

private IDealerPreferenceDAO dealerPrefDAO;
private ICIASummaryDAO ciaSummaryDAO;
private CIAService ciaService;

// Handles Winners and Good Bets, but not Powerzone
public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int businessUnitId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	Integer segmentId = new Integer( request.getParameter( "segmentId" ) );
	Integer ciaCategoryId = new Integer( request.getParameter( "ciaCategory" ) );
	Integer groupingDescriptionId = new Integer( request.getParameter( "groupingDescriptionId" ) ); // possible
																									// remove

	CIADetailPageDisplayItemWithPrefs ciaDetailPageDisplayItemWithPrefs = ciaService.getCIADetailPageDisplayItemsWithPrefs(
																															businessUnitId,
																															segmentId.intValue(),
																															ciaCategoryId.intValue() );
	
	if ( getImtDealerService().showLithiaRoi( businessUnitId ) )
	{
		getCiaService().setAnnualROIOnGroupingItem( ciaDetailPageDisplayItemWithPrefs.getCiaDetailPageDisplayItems(),
													new Integer( businessUnitId ) );
		request.setAttribute( "showAnnualRoi", Boolean.TRUE );
	}
	else
	{
		request.setAttribute( "showAnnualRoi", Boolean.FALSE );
	}

	// TODO get the map/prefs out of this class.
	request.setAttribute( "ciaPlans", ciaDetailPageDisplayItemWithPrefs.getGroupingPreferences() );
	Dealer currentDealer = getImtDealerService().retrieveDealer( businessUnitId );

	request.setAttribute( "groupingDescriptionId", groupingDescriptionId );
	request.setAttribute( "displayItemWithPrefs", ciaDetailPageDisplayItemWithPrefs );
	request.setAttribute( "weeks", new Integer( 26 ) );
	request.setAttribute( "mileage", new Integer( Integer.MAX_VALUE ) );
	request.setAttribute( "includeDealerGroup", new Integer( 0 ) );
	request.setAttribute( "nickname", currentDealer.getNickname() );
	request.setAttribute( "segmentId", segmentId );
	request.setAttribute( "ciaCategory", ciaCategoryId );
	request.setAttribute( "ciaCategoryDescription", CIACategory.retrieveCIACategory( ciaCategoryId).getCiaCategoryDescription() );
	return mapping.findForward( "success" );
}

public IDealerPreferenceDAO getDealerPrefDAO()
{
	return dealerPrefDAO;
}

public CIAService getCiaService()
{
	return ciaService;
}

public void setCiaService( CIAService ciaService )
{
	this.ciaService = ciaService;
}

public ICIASummaryDAO getCiaSummaryDAO()
{
	return ciaSummaryDAO;
}

public void setCiaSummaryDAO( ICIASummaryDAO ciaSummaryDAO )
{
	this.ciaSummaryDAO = ciaSummaryDAO;
}

public void setDealerPrefDAO( IDealerPreferenceDAO dealerPrefDAO )
{
	this.dealerPrefDAO = dealerPrefDAO;
}

}