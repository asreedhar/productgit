package com.firstlook.action.dealer.sell;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.dealer.buy.VehicleDisplayBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.InventoryForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.DealerHelper;

public class SecureVehicleAndSellerEditAction extends VehicleDisplayBaseAction
{

void checkMemberIsAssociatedWithDealer( Member member, int dealerId ) throws ApplicationException, DatabaseException
{
    if ( !member.isAssociatedWithDealer( dealerId ) )
    {
        throw new ApplicationException( "Vehicle does not belong to member's dealership" );
    }
}

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    InventoryEntity vehicle = putVehicleInRequest( request, form );
    int dealerId = vehicle.getDealerId();
    checkMemberIsAssociatedWithDealer( getMemberFromRequest(request), dealerId );
    DealerHelper.putDealerInRequest( request, dealerId, getImtDealerService() );
    return getActionForward( mapping );
}

InventoryEntity putVehicleInRequest( HttpServletRequest request, ActionForm form ) throws DatabaseException, ApplicationException
{
    InventoryEntity vehicle;
    if ( hasValidationErrors( request ) && !hasValidationError( request, InventoryForm.ERROR_KEY_CONCURRENCY_STAMP ) )
    {
        vehicle = ( (InventoryForm)form ).getVehicle();
    }
    else
    {
        vehicle = putVehicleInRequest( request, getImtDealerService() );
    }

    return vehicle;
}

}
