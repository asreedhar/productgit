package com.firstlook.action.dealer.redistribution;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.core.io.Resource;

import biz.firstlook.commons.email.EmailContextBuilder;
import biz.firstlook.commons.email.EmailService;
import biz.firstlook.commons.email.EmailService.EmailFormat;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;

public class LithiaCarCenterRedistributionEmailAction extends SecureBaseAction
{

private EmailService emailService;

@Override
public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	String toAddrsString = request.getParameter( "to" );
	String[] toAddrs = StringUtils.split( toAddrsString, ';' );
	
	for( String to: toAddrs) {
		LithiaCarCenterRedistributionEmailBuilder emailBuilder = new LithiaCarCenterRedistributionEmailBuilder(
												to, 
												request.getParameter( "replyto"),
												request.getParameter("subject"),
												request.getParameter("body") );
		emailService.sendEmail( emailBuilder );
	}
	return null;
}

public void setEmailService( EmailService emailService )
{
	this.emailService = emailService;
}

private class LithiaCarCenterRedistributionEmailBuilder implements EmailContextBuilder{

	private String to;
	private String replyTo;
	private String subject;
	private String body;

	private LithiaCarCenterRedistributionEmailBuilder(String to, String replyTo, String subject, String body) {
		this.to = to;
		this.replyTo = replyTo;
		this.subject = subject;
		this.body = body;
	}

	public Map< String, EmailFormat > getEmailFormats()	{
		Map< String, EmailFormat > emailFormats = new HashMap< String, EmailFormat >();
		emailFormats.put( to, EmailFormat.PLAIN_TEXT );
		return emailFormats;
	}

	public Map< String, Map< String, Object >> getRecipientsAndContext() {
		Map< String, Object > smsContext = new HashMap< String, Object >();
		smsContext.put( "body", body );
		Map< String, Map< String, Object > > recipientsAndContexts = new HashMap< String, Map< String, Object > >();
		recipientsAndContexts.put( to, smsContext );
		return recipientsAndContexts;
	}

	public String getReplyTo() { return replyTo; }
	public String getSubject() { return subject; }
	public String getTemplateName()	{ return "lithiaCarCenterRedistributionEmailAction"; }

	public List< String > getCcEmailAddresses()	{ return Collections.EMPTY_LIST; }
	public Map< String, Resource > getEmbeddedResources() { return null; }
	public Map< String, String > getEmbeddedResourcesContentTypes()	{ return null; }
}

}
