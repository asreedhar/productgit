package com.firstlook.action.dealer.redistribution;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.service.appraisal.AppraisalValue;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.form.TradeManagerEditBumpForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;

public class TradeManagerEditBumpDisplayAction extends SecureBaseAction
{

private IAppraisalService appraisalService;

public ActionForward doIt( ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response ) throws DatabaseException,
        ApplicationException
{

    int appraisalId = RequestHelper
            .getInt(request, "appraisalId");

    List<AppraisalValue> appraisalGuideBookValues;
    try
    {
    	IAppraisal appraisal = appraisalService.findBy( appraisalId );
        appraisalGuideBookValues = appraisal.getAppraisalValues(); 
    } catch (Exception e)
    {
        throw new DatabaseException("Unable to retrieve AppraisalBump ", e);
    }

    TradeManagerEditBumpForm tradeManagerEditBumpForm = new TradeManagerEditBumpForm();
    double initialValue = 0;
    if( !appraisalGuideBookValues.isEmpty() )
    {
    	AppraisalValue firstValue = (AppraisalValue)appraisalGuideBookValues.get( 0 );
    	initialValue = firstValue.getValue();
    	tradeManagerEditBumpForm.setAppraisalInitials( firstValue.getAppraiserName() );
    }
    tradeManagerEditBumpForm.setAppraisalValue( initialValue );
    tradeManagerEditBumpForm
            .setVehicleGuideBookBumpCol(appraisalGuideBookValues);
    tradeManagerEditBumpForm.setAppraisalId(appraisalId);

    request.setAttribute("tradeManagerEditBumpForm", tradeManagerEditBumpForm);

    return mapping.findForward("success");
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

}
