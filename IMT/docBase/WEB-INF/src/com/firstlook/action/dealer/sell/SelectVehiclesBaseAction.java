package com.firstlook.action.dealer.sell;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.form.InventoryForm;
import com.firstlook.entity.form.SelectVehiclesForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.DealerHelper;
import com.firstlook.iterator.SimpleFormIterator;
import com.firstlook.service.dealer.DealerFranchiseService;
import com.firstlook.service.dealergroup.DealerGroupService;

public class SelectVehiclesBaseAction extends com.firstlook.action.SecureBaseAction
{
private DealerGroupService dealerGroupService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    return null;
}

protected void putFormsInRequest( Collection vehicles, HttpServletRequest request ) throws ApplicationException
{
    putSelectVehiclesFormInRequest( vehicles, request );
    SimpleFormIterator si = new SimpleFormIterator( vehicles, InventoryForm.class );
    DealerHelper.putCurrentDealerFormInRequest( request, getImtDealerService(), dealerGroupService,
                                                new DealerFranchiseService( getDealerFranchiseDAO() ) );
    request.setAttribute( "vehicleList", si );

}

protected void putSelectVehiclesFormInRequest( Collection vehicles, HttpServletRequest request ) throws ApplicationException
{
    SelectVehiclesForm checkboxListForm = new SelectVehiclesForm();
    checkboxListForm.setVehicleIdArray( vehicles, "isSelected" );

    request.setAttribute( "checkboxList", checkboxListForm );
}

public DealerGroupService getDealerGroupService()
{
	return dealerGroupService;
}

public void setDealerGroupService( DealerGroupService dealerGroupService )
{
	this.dealerGroupService = dealerGroupService;
}

}
