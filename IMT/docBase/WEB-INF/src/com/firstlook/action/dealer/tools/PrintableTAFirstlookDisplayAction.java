package com.firstlook.action.dealer.tools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.services.insights.Insight;
import biz.firstlook.commons.services.insights.InsightParameters;
import biz.firstlook.commons.services.insights.InsightUtils;
import biz.firstlook.commons.services.insights.PerformanceAnalysisBean;
import biz.firstlook.transact.persist.model.BookOut;
import biz.firstlook.transact.persist.model.BookOutValue;
import biz.firstlook.transact.persist.model.BookOutValueType;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOptionValue;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOptionValueType;
import biz.firstlook.transact.persist.service.appraisal.AppraisalWindowSticker;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.dealer.tools.GuideBook;
import com.firstlook.dealer.tools.GuideBookReturnValue;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.form.AppraisalForm;
import com.firstlook.entity.form.ThirdPartyVehicleOptionForm;
import com.firstlook.entity.form.TradeAnalyzerForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.SessionHelper;
import com.firstlook.helper.action.ReportActionHelper;
import com.firstlook.persistence.inventory.IInventoryDAO;
import com.firstlook.report.Report;
import com.firstlook.service.bookout.BookOutService;
import com.firstlook.service.inventory.InventoryService_Legacy;
import com.firstlook.session.FirstlookSession;
import com.firstlook.util.PageBreakHelper;

@SuppressWarnings("unchecked")
public class PrintableTAFirstlookDisplayAction extends SecureBaseAction
{

private InventoryService_Legacy inventoryService_Legacy;
private IAppraisalService appraisalService;

private ReportActionHelper reportActionHelper;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws ApplicationException, DatabaseException
{
	int appraisalId = validateNotNull( request, "appraisalId" );
	int weeks = validateNotNull( request, "weeks" );
	int currentDealerId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();
	Dealer dealer = getImtDealerService().retrieveDealer( currentDealerId );
	int guideBookId = dealer.getGuideBookId();
	int guideBookId2 = dealer.getGuideBook2Id();

	AppraisalWindowSticker appraisalWindowSticker = null;
	TradeAnalyzerForm tradeAnalyzerForm = (TradeAnalyzerForm)form;
	setIncludeDealerGroupInfo( request, tradeAnalyzerForm );
	if ( appraisalId > 0 )
	{
		IAppraisal appraisal = appraisalService.findBy( appraisalId );
		List appraisalGuideBookOptions = new ArrayList();
		List appraisalGuideBookEngines = new ArrayList();
		List appraisalGuideBookDrivetrains = new ArrayList();
		List appraisalGuideBookTransmissions = new ArrayList();

		BookOutService.populateSelectedOptionsList( guideBookId, appraisalGuideBookOptions, appraisalGuideBookEngines,
													appraisalGuideBookDrivetrains, appraisalGuideBookTransmissions, appraisal );

		createGuideBookFormFromAppraisal( appraisal, tradeAnalyzerForm, request, guideBookId, true );

		List allOptions = createAllOptionsList( appraisalGuideBookOptions, appraisalGuideBookEngines, appraisalGuideBookDrivetrains,
												appraisalGuideBookTransmissions );
		appraisalWindowSticker = appraisal.getAppraisalWindowSticker();

		if ( appraisalWindowSticker != null )
		{
			request.setAttribute( "windowStickerId", appraisalWindowSticker.getAppraisalWindowStickerId() );
			request.setAttribute( "stockNumber", appraisalWindowSticker.getStockNumber() );
			request.setAttribute( "sellingPrice", appraisalWindowSticker.getSellingPrice() );
		}

		putPrimaryOptionsInRequest( request, appraisal, appraisalGuideBookOptions, appraisalGuideBookEngines, appraisalGuideBookDrivetrains,
									appraisalGuideBookTransmissions, allOptions, guideBookId );
		if ( guideBookId2 > 0 )
		{
			List appraisalGuideBookOptionsSecondary = new ArrayList();
			List appraisalGuideBookEnginesSecondary = new ArrayList();
			List appraisalGuideBookDrivetrainsSecondary = new ArrayList();
			List appraisalGuideBookTransmissionsSecondary = new ArrayList();

			BookOutService.populateSelectedOptionsList( guideBookId2, appraisalGuideBookOptionsSecondary, appraisalGuideBookEnginesSecondary,
														appraisalGuideBookDrivetrainsSecondary, appraisalGuideBookTransmissionsSecondary,
														appraisal );
			List allOptionsSecondary = createAllOptionsList( appraisalGuideBookOptionsSecondary, appraisalGuideBookEnginesSecondary,
																appraisalGuideBookDrivetrainsSecondary,
																appraisalGuideBookTransmissionsSecondary );

			putSecondaryOptionsInRequest( request, appraisal, appraisalGuideBookOptionsSecondary, appraisalGuideBookEnginesSecondary,
											appraisalGuideBookDrivetrainsSecondary, appraisalGuideBookTransmissionsSecondary,
											allOptionsSecondary, guideBookId2 );
			if ( appraisalWindowSticker != null )
			{
				request.setAttribute( "windowStickerId", appraisalWindowSticker.getAppraisalWindowStickerId() );
				request.setAttribute( "stockNumber", appraisalWindowSticker.getStockNumber() );
				request.setAttribute( "sellingPrice", appraisalWindowSticker.getSellingPrice() );
			}

			createGuideBookFormFromAppraisal( appraisal, tradeAnalyzerForm, request, guideBookId2, false );
		}
	}

	InventoryEntity inventory = inventoryService_Legacy.createEmptyInventoryWithYearMileageMakeModelUsingVin(
																											new Integer( tradeAnalyzerForm.getYear() ).toString(),
																											tradeAnalyzerForm.getMileage().intValue(),
																											tradeAnalyzerForm.getVin(),
																											tradeAnalyzerForm.getMake(),
																											tradeAnalyzerForm.getModel() );

	ReportGroupingForms reportGroupingForms = reportActionHelper.createReportGroupingForms( tradeAnalyzerForm.getMake(),
																							tradeAnalyzerForm.getModel(),
																							tradeAnalyzerForm.getTrim(),
																							tradeAnalyzerForm.getIncludeDealerGroup(),
																							inventory, InventoryEntity.USED_CAR, weeks,
																							currentDealerId );

	List<Insight> insights = InsightUtils.getInsights(new InsightParameters(currentDealerId, inventory.getGroupingDescriptionId(), InventoryEntity.USED_CAR, weeks, tradeAnalyzerForm.getTrim(), inventory.getVehicleYear(), inventory.getMileageReceived()));

	reportGroupingForms.setPerformanceAnalysisBean(new PerformanceAnalysisBean(insights));

	request.setAttribute( "specificReport", reportGroupingForms.getSpecificReportGrouping() );
	request.setAttribute( "generalReport", reportGroupingForms.getGeneralReportGrouping() );
	request.setAttribute( "performanceAnalysisItem", reportGroupingForms.getPerformanceAnalysisBean() );
	int groupingId = reportGroupingForms.getGroupingDescriptionId();

	request.setAttribute( "nickname", dealer.getNickname() );
	DealerGroup group = getDealerGroupService().retrieveByDealerId( ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId() );

	request.setAttribute( "dealerGroupName", group.getName() );
	putPageBreakHelperInRequest( PageBreakHelper.TA_ONLINE, request, getMemberFromRequest( request ).getProgramType() );
	ReportActionHelper.putNumberOfDealerPagesInRequest( request );

	Integer distance = dealer.getDealerPreference().getRedistributionDealerDistance();
	Integer sortBy = getImtDealerService().determineDemandDealerSortOrder(dealer.getBusinessUnitId(), dealer.getDealerPreference().getRedistributionUnderstock(), dealer.getDealerPreference().getRedistributionROI());
	
	List demandDealers = (List)getImtDealerService().retrieveDemandDealers( dealer.getDealerId(), new Integer( groupingId ),
																	tradeAnalyzerForm.getMileage(), new Integer( tradeAnalyzerForm.getYear() ), distance, sortBy);
	putDemandDealersInRequest( request, demandDealers );

	request.setAttribute( "isFirstlookAppraisal", new Boolean( true ) );
	request.setAttribute( "tradeAnalyzerForm", tradeAnalyzerForm );
	SessionHelper.setAttribute( request, "tradeAnalyzerForm", tradeAnalyzerForm );
	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );
	request.setAttribute( "weeks", new Integer( weeks ) );
	return mapping.findForward( "success" );
}

private void setIncludeDealerGroupInfo( HttpServletRequest request, TradeAnalyzerForm tradeAnalyzerForm )
{
	if ( request.getParameter( "includeDealerGroup" ) != null )
	{
		if ( Integer.parseInt( request.getParameter( "includeDealerGroup" ) ) == Report.DEALERGROUP_INCLUDE_TRUE )
		{
			tradeAnalyzerForm.setIncludeDealerGroup( Report.DEALERGROUP_INCLUDE_TRUE );
		}
		else if ( Integer.parseInt( request.getParameter( "includeDealerGroup" ) ) == Report.DEALERGROUP_INCLUDE_FALSE )
		{
			tradeAnalyzerForm.setIncludeDealerGroup( Report.DEALERGROUP_INCLUDE_FALSE );
		}
	}
}

private int validateNotNull( HttpServletRequest request, String parm ) throws ApplicationException
{
	if ( request.getParameter( parm ) == null || request.getParameter( parm ).length() < 1 )
	{
		return 0;
	}
	return RequestHelper.getInt( request, parm );
}

private void putSecondaryOptionsInRequest( HttpServletRequest request, IAppraisal appraisal, Collection appraisalGuideBookOptionsSecondary,
											Collection appraisalGuideBookEnginesSecondary, Collection appraisalGuideBookDrivetrainsSecondary,
											Collection appraisalGuideBookTransmissionsSecondary, List allOptionsSecondary, int guideBookId )
		throws ApplicationException
{
	request.setAttribute( "guideBookOptionsSecondary", getFormCollection( appraisalGuideBookOptionsSecondary ) );
	request.setAttribute( "guideBookEnginesSecondary", getFormCollection( appraisalGuideBookEnginesSecondary ) );
	request.setAttribute( "guideBookDrivetrainsSecondary", getFormCollection( appraisalGuideBookDrivetrainsSecondary ) );
	request.setAttribute( "guideBookTransmissionsSecondary", getFormCollection( appraisalGuideBookTransmissionsSecondary ) );
	request.setAttribute( "guideBookOptionsTotalSecondary", calculateOptionsTotal( allOptionsSecondary, guideBookId ) );
	AppraisalForm appraisalForm = new AppraisalForm( appraisal );
	appraisalForm.setGuideBookId( guideBookId );
	request.setAttribute( "appraisalFormSecondary", appraisalForm );

}

private void putPrimaryOptionsInRequest( HttpServletRequest request, IAppraisal appraisal, Collection appraisalGuideBookOptions,
										Collection appraisalGuideBookEngines, Collection appraisalGuideBookDrivetrains,
										Collection appraisalGuideBookTransmissions, List allOptions, int guideBookId )
		throws ApplicationException
{
	request.setAttribute( "guideBookOptions", getFormCollection( appraisalGuideBookOptions ) );
	request.setAttribute( "guideBookEngines", getFormCollection( appraisalGuideBookEngines ) );
	request.setAttribute( "guideBookDrivetrains", getFormCollection( appraisalGuideBookDrivetrains ) );
	request.setAttribute( "guideBookTransmissions", getFormCollection( appraisalGuideBookTransmissions ) );
	request.setAttribute( "guideBookOptionsTotal", calculateOptionsTotal( allOptions, guideBookId ) );
	AppraisalForm appraisalForm = new AppraisalForm( appraisal );
	appraisalForm.setGuideBookId( guideBookId );
	request.setAttribute( "appraisalForm", appraisalForm );
}

private Object getFormCollection( Collection options )
{
	Iterator iter = options.iterator();
	List rtrnList = new ArrayList();
	ThirdPartyVehicleOption option;
	ThirdPartyVehicleOptionForm form;
	while ( iter.hasNext() )
	{
		option = (ThirdPartyVehicleOption)iter.next();
		form = new ThirdPartyVehicleOptionForm( option );
		rtrnList.add( form );
	}
	return rtrnList;
}

private List createAllOptionsList( Collection options, Collection engines, Collection drivetrains, Collection transmissions )
{
	List returnList = new ArrayList();
	returnList.addAll( options );
	returnList.addAll( engines );
	returnList.addAll( drivetrains );
	returnList.addAll( transmissions );

	return returnList;
}

private Integer calculateOptionsTotal( Collection appraisalGuideBookOptions, int guideBookId ) throws ApplicationException
{
	Iterator optionsIter = appraisalGuideBookOptions.iterator();
	int totalOptionValue = 0;
	ThirdPartyVehicleOption option;
	ThirdPartyVehicleOptionValue optionValue;
	while ( optionsIter.hasNext() )
	{
		option = (ThirdPartyVehicleOption)optionsIter.next();
		if ( guideBookId == ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE )
		{
			optionValue = option.determineValueByType( ThirdPartyVehicleOptionValueType.RETAIL_OPTION_VALUE_TYPE );
		}
		else
		{
			optionValue = option.determineValueByType( ThirdPartyVehicleOptionValueType.NA_OPTION_VALUE_TYPE );
		}
		if ( optionValue.getValue().intValue() != Integer.MIN_VALUE )
		{
			totalOptionValue += optionValue.getValue().intValue();
		}
	}
	return new Integer( totalOptionValue );
}

private void createGuideBookFormFromAppraisal( IAppraisal appraisal, TradeAnalyzerForm tradeAnalyzerForm, HttpServletRequest request,
												int guideBookId, boolean isPrimary ) throws ApplicationException, DatabaseException
{
	BookOut bookOut = appraisal.getLatestBookOut( guideBookId );
	tradeAnalyzerForm.setActionId( appraisal.getAppraisalActionType().getAppraisalActionTypeId().intValue() );
	tradeAnalyzerForm.setAppraisalInitials( appraisal.getLatestAppraisalValue() != null ? appraisal.getLatestAppraisalValue().getAppraiserName() : "" );
	tradeAnalyzerForm.setAppraisalValue( appraisal.getLatestAppraisalValue() != null ? appraisal.getLatestAppraisalValue().getValue() : null);
	tradeAnalyzerForm.setColor( appraisal.getColor() );
	tradeAnalyzerForm.setConditionDisclosure( appraisal.getConditionDescription() );
	
	tradeAnalyzerForm.setMileage( new Integer( appraisal.getMileage() ) );

	tradeAnalyzerForm.setVin( appraisal.getVehicle().getVin() );
	tradeAnalyzerForm.setPrice( appraisal.getWholesalePrice().intValue() );
	tradeAnalyzerForm.setReconditioningCost( appraisal.getEstimatedReconditioningCost() );
	tradeAnalyzerForm.setTargetGrossProfit(appraisal.getTargetGrossProfit());
	tradeAnalyzerForm.setYear( appraisal.getVehicle().getVehicleYear().intValue() );
	String make = tradeAnalyzerForm.getMake();
	if ( make == null )
	{
		make = appraisal.getVehicle().getMakeModelGrouping().getMake();
		tradeAnalyzerForm.setMake( make );
	}
	String model = determineModel( appraisal );
	tradeAnalyzerForm.setModel( model );
	tradeAnalyzerForm.setTrim( appraisal.getVehicle().getVehicleTrim() );
	if ( make != null && model != null )
	{
		tradeAnalyzerForm.setMakeModelGroupingId( appraisal.getVehicle().getMakeModelGrouping().getMakeModelGroupingId() );
		tradeAnalyzerForm.setGroupingDescriptionId( appraisal.getVehicle().getMakeModelGrouping().getGroupingDescription().getGroupingDescriptionId().intValue() );
	}

	Collection appraisalGuideBookFinalValues = BookOutService.determineAppropriateBookOutValuesByType(bookOut,
																										BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE );
	Iterator finalValuesIter = appraisalGuideBookFinalValues.iterator();
	List guideBookValues = new ArrayList();
	BookOutValue bookoutValue;
	while ( finalValuesIter.hasNext() )
	{
		bookoutValue = (BookOutValue)finalValuesIter.next();
		GuideBookReturnValue gbv = new GuideBookReturnValue();
		gbv.setTitle( bookoutValue.getThirdPartyCategory().getThirdPartyCategory().getCategory() );
		gbv.setValue( bookoutValue.getValue() );
		guideBookValues.add( gbv );

		if ( guideBookId == ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE )
		{
			setKelleyFinalValues( request, gbv );
			if ( bookOut != null )
			{
				setKelleyNoMileageValues(
										request,
										gbv,
										bookoutValue.getValue(), bookOut.getMileageCostAdjustment().intValue() );
			}
			else
			{
				setKelleyNoMileageValues( request, gbv, new Integer (0), 0 );
			}
		}
	}
	// get options adjustment values for kelly
	if ( guideBookId == GuideBook.GUIDE_TYPE_KELLEYBLUEBOOK )
	{
		List<BookOutValue> appraisalOptionValues = BookOutService.determineAppropriateBookOutValuesByType(
				appraisal.getLatestBookOut( guideBookId ),
				BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE );
		if (appraisalOptionValues.size() > 0) { request.setAttribute( "kellyOptionsAdjustment", appraisalOptionValues.get(0) ); }
	}
	
	Collection appraisalGuideBookBaseValues = BookOutService.determineAppropriateBookOutValuesByType(bookOut,
																										BookOutValueType.BOOK_OUT_VALUE_BASE_VALUE );

	Iterator baseValuesIter = appraisalGuideBookBaseValues.iterator();
	while ( baseValuesIter.hasNext() )
	{
		bookoutValue = (BookOutValue)baseValuesIter.next();
		GuideBookReturnValue gbv = new GuideBookReturnValue();
		gbv.setTitle( bookoutValue.getThirdPartyCategory().getThirdPartyCategory().getCategory() );
		gbv.setValue( bookoutValue.getValue() );

		if ( guideBookId == GuideBook.GUIDE_TYPE_KELLEYBLUEBOOK )
		{
			setKelleyBaseValues( request, gbv );
		}
	}

	if ( isPrimary )
	{
		request.setAttribute( "guideBookValues", guideBookValues );
		request.setAttribute( "guideBookValueAvailable", Boolean.TRUE );
		request.setAttribute( "guideBookValuesSize", new Integer( guideBookValues.size() ) );
	}
	else
	{
		request.setAttribute( "guideBookValuesSecondary", guideBookValues );
		request.setAttribute( "guideBookValueAvailableSecondary", Boolean.TRUE );
		request.setAttribute( "guideBookValuesSecondarySize", new Integer( guideBookValues.size() ) );
	}
}

private void setKelleyBaseValues( HttpServletRequest request, GuideBookReturnValue gbv )
{
	if ( gbv.getTitle().equals( ThirdPartyCategory.KELLEY_WHOLESALE_DESCRIPTION ) )
	{
		request.setAttribute( "baseValueWholesale", setToMinValIfNull( gbv.getValue() ) );
	}
	else
	{
		request.setAttribute( "baseValueRetail", setToMinValIfNull( gbv.getValue() ) );
	}
}

//This is a common private method between this and PrintableTradeAnalyzerDisplayAction
private void setKelleyNoMileageValues( HttpServletRequest request, GuideBookReturnValue gbv, Integer bookValue, int mileageAdjustment )
{
	Integer localBookValue = null;
	if ( bookValue != null )
	{
		localBookValue = new Integer( bookValue.intValue() - mileageAdjustment );
	}

	if ( gbv.getTitle().equals( ThirdPartyCategory.KELLEY_WHOLESALE_DESCRIPTION ) )
	{
		request.setAttribute( "noMileageValueWholesale", setToMinValIfNull( localBookValue ) );
	}
	else if ( gbv.getTitle().equals( ThirdPartyCategory.KELLEY_RETAIL_DESCRIPTION ) )
	{
		request.setAttribute( "noMileageValueRetail", setToMinValIfNull( localBookValue ) );
	}
	else if ( gbv.getTitle().equals( ThirdPartyCategory.KELLEY_TRADEIN_DESCRIPTION ) )
	{
		request.setAttribute( "noMileageValueTradeIn", setToMinValIfNull( localBookValue ) );
	}
}

private Integer setToMinValIfNull( Integer value )
{
	if ( value == null )
	{
		return new Integer( Integer.MIN_VALUE );
	}
	return value;
}

private void setKelleyFinalValues( HttpServletRequest request, GuideBookReturnValue gbv )
{
	if ( gbv.getTitle().equals( ThirdPartyCategory.KELLEY_WHOLESALE_DESCRIPTION ) )
	{
		request.setAttribute( "finalValueWholesale", setToMinValIfNull( gbv.getValue() ) );
	}
	else if ( gbv.getTitle().equals( ThirdPartyCategory.KELLEY_RETAIL_DESCRIPTION ) )
	{
		request.setAttribute( "finalValueRetail", setToMinValIfNull( gbv.getValue() ) );
	}
	else if ( gbv.getTitle().equals( ThirdPartyCategory.KELLEY_TRADEIN_DESCRIPTION ) )
	{
		request.setAttribute( "finalValueTradeIn", setToMinValIfNull( gbv.getValue() ) );
	}
}

private String determineModel( IAppraisal appraisal )
{
	String model;
	if ( appraisal.getVehicle().getMakeModelGrouping().getModel() == null )
	{
		model = appraisal.getVehicle().getModel();
	}
	else
	{
		model = appraisal.getVehicle().getMakeModelGrouping().getModel();
	}
	return model;
}

private void putDemandDealersInRequest( HttpServletRequest request, List dealerDemand )
{
	if ( dealerDemand != null && dealerDemand.size() > 0 )
	{
		request.setAttribute( "demandDealers", dealerDemand );
	}
}

public void setInventoryService_Legacy( InventoryService_Legacy inventoryService )
{
	this.inventoryService_Legacy = inventoryService;
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

public void setReportActionHelper( ReportActionHelper reportActionHelper )
{
	this.reportActionHelper = reportActionHelper;
}

}