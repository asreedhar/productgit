package com.firstlook.action.dealer.reports;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ReverseComparator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.report.PricingListReportRetriever;
import com.firstlook.report.PricingListLineItem;
import com.firstlook.util.PageBreakHelper;

public class PricingListDisplayAction extends SecureBaseAction
{

// flags used in pricingListPage.jsp to toggle retailFilter on and off
protected static String RETAIL_ONLY = "showRetailOnly";

private static final String SHOW_ALL = "showAll";
private static final String DISPLAY_TAG_TABLE_ID = "row";
private static final Log log = LogFactory.getLog( PricingListReportRetriever.class );

private PricingListReportRetriever pricingListReportRetriever;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();

	// check retailOnly filter and set on query and value on request
	String retailFiler = shouldIncludeRetailOnly( request );
	Boolean includeRetailOnly = Boolean.FALSE;
	if ( retailFiler.equals( RETAIL_ONLY ) )
	{
		includeRetailOnly = Boolean.TRUE;
	}
	
	List<PricingListLineItem> vehicles = pricingListReportRetriever.retrievePricingListLineItems( new Integer( currentDealerId ), includeRetailOnly );
	sort(request, vehicles);
	
	Dealer currentDealer = getImtDealerService().retrieveDealer( currentDealerId );

	request.setAttribute( "vehicles", vehicles );
	request.setAttribute( "nickname", currentDealer.getNickname() );
	request.setAttribute( "useLotPrice", currentDealer.getDealerPreference().getUseLotPrice() );
	request.setAttribute( "today", new Date() );
	request.setAttribute( "retailFilter", retailFiler );
	request.setAttribute( "show", request.getParameter("show") );
	request.setAttribute( "pricingNotes", request.getParameter("pricingNotes") );
	request.setAttribute("printParamNamesCSV", getPrintParamNamesCSV(request));
	
	putPageBreakHelperInRequest( PageBreakHelper.PRICING_LIST, request, getMemberFromRequest( request ).getProgramType() );

	return mapping.findForward( "success" );
}

@SuppressWarnings("unchecked")
private static String getPrintParamNamesCSV(HttpServletRequest request) {
	StringBuilder paramNamesCSV = new StringBuilder();
	Enumeration<String> paramNamesEnum = request.getParameterNames();
	while(paramNamesEnum.hasMoreElements()) {
		paramNamesCSV.append(paramNamesEnum.nextElement());
		if(paramNamesEnum.hasMoreElements()) {
			paramNamesCSV.append(",");
		}
	}
	return paramNamesCSV.toString();
}

@SuppressWarnings("unchecked")
private void sort(HttpServletRequest request, List<PricingListLineItem> vehicles) {
	boolean isAscending = true;

	ParamEncoder paramEncoder =  new ParamEncoder(DISPLAY_TAG_TABLE_ID);
	String sortProperty = request.getParameter(paramEncoder.encodeParameterName(TableTagParameters.PARAMETER_SORT));
	//DisplayTag, Descending is 2, Ascending is 1, so decrement to fit action's assumptions.
	String sortDirection = request.getParameter(paramEncoder.encodeParameterName(TableTagParameters.PARAMETER_ORDER));
	if(StringUtils.isNotBlank(sortDirection)) {
		try {
			isAscending = Integer.valueOf(sortDirection).equals(1);
		} catch (NumberFormatException nfe ) {
			log.warn("Sort Direction parameter is invalid, defaulting to ascending.");
		}
	}
	
	if(StringUtils.isNotBlank(sortProperty)) {
		Comparator<PricingListLineItem> comparator = new BeanComparator(sortProperty);
		if(!isAscending) {
			comparator = new ReverseComparator(comparator);
		}
		try {
			Collections.sort(vehicles, comparator);
		} catch (Exception e) {
			log.warn(e.getMessage() + ", check the sortName property on the jsp.");
		}
	}
}

private String shouldIncludeRetailOnly( HttpServletRequest request )
{
	String retailOnly = SHOW_ALL;
	String retailOnlyFilter = (String)request.getParameter( "retailFilter" );
	if ( retailOnlyFilter != null )
	{
		if ( retailOnlyFilter.equalsIgnoreCase( RETAIL_ONLY ) )
		{
			retailOnly = RETAIL_ONLY;
		}
	}
	return retailOnly;
}

public PricingListReportRetriever getPricingListReportRetriever()
{
	return pricingListReportRetriever;
}

public void setPricingListReportRetriever( PricingListReportRetriever pricingListReportRetriever )
{
	this.pricingListReportRetriever = pricingListReportRetriever;
}

}
