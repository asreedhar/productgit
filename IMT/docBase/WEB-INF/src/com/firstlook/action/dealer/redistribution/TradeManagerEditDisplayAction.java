package com.firstlook.action.dealer.redistribution;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.comparators.ComparatorChain;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.cia.service.BasisPeriodService;
import biz.firstlook.commons.services.vehicleCatalog.IVehicleCatalogService;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogEntry;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogServiceException;
import biz.firstlook.commons.services.vehiclehistoryreport.AutoCheckException;
import biz.firstlook.commons.services.vehiclehistoryreport.AutoCheckService;
import biz.firstlook.commons.services.vehiclehistoryreport.CarfaxException;
import biz.firstlook.commons.services.vehiclehistoryreport.CarfaxService;
import biz.firstlook.commons.util.BasicAuthAdapter;
import biz.firstlook.commons.util.Functions;
import biz.firstlook.services.vehiclehistoryreport.autocheck.AutoCheckReportInspectionTO;
import biz.firstlook.services.vehiclehistoryreport.autocheck.AutoCheckReportTO;
import biz.firstlook.services.vehiclehistoryreport.carfax.CarfaxReportInspectionTO;
import biz.firstlook.services.vehiclehistoryreport.carfax.CarfaxReportTO;
import biz.firstlook.services.vehiclehistoryreport.carfax.VehicleEntityType;
import biz.firstlook.transact.persist.model.BookOutSource;
import biz.firstlook.transact.persist.model.CIAPreferences.TimePeriod;
import biz.firstlook.transact.persist.model.DealerUpgrade;
import biz.firstlook.transact.persist.model.DemandDealer;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;
import biz.firstlook.transact.persist.persistence.DealerUpgradeDAO;
import biz.firstlook.transact.persist.person.IPerson;
import biz.firstlook.transact.persist.person.PositionType;
import biz.firstlook.transact.persist.service.appraisal.AppraisalFormOptions;
import biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum;
import biz.firstlook.transact.persist.service.appraisal.AppraisalValue;
import biz.firstlook.transact.persist.service.appraisal.Customer;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.BaseActionForm;
import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.redirection.JdPowerRedirectionAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.TradeAnalyzerForm;
import com.firstlook.entity.form.TradeManagerEditBumpForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.ReportGroupingRequestHelper;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.SessionHelper;
import com.firstlook.report.Report;
import com.firstlook.service.dealer.IDealerService;
import com.firstlook.service.inventory.InventoryService_Legacy;
import com.firstlook.session.FirstlookSession;


public class TradeManagerEditDisplayAction extends SecureBaseAction {
	private static final Logger logger = Logger
			.getLogger(TradeManagerEditDisplayAction.class);
	private IAppraisalService appraisalService;
	private BasisPeriodService basisPeriodService;
	private InventoryService_Legacy inventoryService_Legacy;
	private DealerPreferenceDAO dealerPrefDAO;
	private DealerUpgradeDAO dealerUpgradeDAO;
	private ReportGroupingRequestHelper reportGroupingRequestHelper;
	
	
	protected IVehicleCatalogService vehicleCatalogService;

	public void setBuildNumber(String buildNumber) {
		this.buildNumber = buildNumber;
	}
	
	public String getBuildNumber() {
		return this.buildNumber;
	}
	
	public ActionForward doIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws DatabaseException, ApplicationException {

		
		int currentDealerId = getFirstlookSessionFromRequest(request)
				.getCurrentDealerId();

		request.setAttribute("dealerGroupId", getDealerGroupService()
				.retrieveByDealerId(currentDealerId));
		Dealer dealer = putDealerFormInRequest(request, 0);
		Boolean includeBackEndGrossProfit = dealer.getDealerPreference()
				.getIncludeBackEndInValuation();
		request.setAttribute("includeBackEndInValuation",
				includeBackEndGrossProfit);

		String completeVinFromParameter = null;
		TradeAnalyzerForm tradeAnalyzerForm = (TradeAnalyzerForm) form;
		
		// Adding this to 3.0 to get the dealer preferences onto the AppraisalReview.jsp page.

		tradeAnalyzerForm.setAppraisalRequirement(dealer.getDealerPreference().getAppraisalRequirementLevel());
		tradeAnalyzerForm.setRequireNameOnAppraisals(         dealer.getDealerPreference().getRequireNameOnAppraisals());
		tradeAnalyzerForm.setRequireEstReconCostOnAppraisals( dealer.getDealerPreference().getRequireEstReconCostOnAppraisals() );		
		tradeAnalyzerForm.setRequireReconNotesOnAppraisals(   dealer.getDealerPreference().getRequireReconNotesOnAppraisals() );
		
        List<IPerson> appraisers = new ArrayList<IPerson>();
        PositionType appraiserType = PositionType.valueFromPositionString("Appraiser");
        if (appraiserType != null) {
            appraisers = (List<IPerson>) getPersonService().getBusinessUnitPeopleByPosition(currentDealerId, appraiserType);
            //response.addHeader("posDesc", type.getPosition());
        }
        
        //sortPeople(people, theForm.getSortColumn(), theForm.getSortDir());
        //theForm.setPeople(people);
        tradeAnalyzerForm.setAppraisers(appraisers);
    	request.setAttribute( "appraisers", appraisers );
        
		int weeks = basisPeriodService
				.calculateNumberOfWeeksInBasisPeriodByType(new Integer(
						currentDealerId), TimePeriod.SALES_HISTORY_DISPLAY, 0);

		// The edit trade page needs to pass this on the request back to
		// the SearchOlderTradesSubmitAction - MH, October 14, 2005
		/* Start */
		completeVinFromParameter = request.getParameter("vin");
		if (completeVinFromParameter != null) {
			String lastSixDigitsOfVin = completeVinFromParameter.substring(11,
					17);
			request.setAttribute("lastSixDigitsOfVin", lastSixDigitsOfVin);
		}
		/* End */

		Member member = getMemberFromRequest(request);
		
		// get the appraisal id (return if not present)
		final String parameter = request.getParameter("appraisalId");
		final Object attribute = request.getAttribute("appraisalId");
		Integer appraisalId = Functions.nullSafeInteger(parameter);
		if (appraisalId == null) {
			appraisalId = Functions.nullSafeInteger(attribute);
		}
		if (appraisalId == null) {
			return mapping.findForward( "failure" );
		}
		
		putBumpsInRequest( request, new Integer( appraisalId ) );
		
		Object bumpForm = request.getAttribute("tradeManagerEditBumpForm");
		
		Integer selectedAppraiserIndex = -1;
        Integer i = 0;
        
		if (bumpForm != null){
			
			String appraiserName =  ((TradeManagerEditBumpForm)bumpForm).getAppraiserName();
			
			tradeAnalyzerForm.setAppraiserName(appraiserName);
		
			if (appraiserName != null)
			{
		        for (IPerson appraiser:appraisers){
		            if (appraiser.getFullName().compareTo(appraiserName) == 0) {
		                selectedAppraiserIndex = i;
		                break;
		            }
		            i++;
		        }
			}
		}
		String appraiserName = null;
        tradeAnalyzerForm.setSelectedAppraiserIndex(selectedAppraiserIndex);
        //getSelectedRow(people, response);
        //checking appraisal is from crm or mobile
        if(!(appraisalService.findBy(appraisalId).getAppraisalSource().isCRMOrMobile())){
        	AppraisalValue appraisalValue = appraisalService.findBy(appraisalId).getLatestAppraisalValue();
        	if(appraisalValue!=null)
        	{
        		appraiserName = appraisalValue.getAppraiserName();
        	}
        	tradeAnalyzerForm.setAppraiserName(appraiserName == null ? "":appraiserName);
        	//tradeAnalyzerForm.setAppraiserName(appraisalService.findBy(appraisalId).getLatestAppraisalValue().getAppraiserName());
        }
    	request.setAttribute( "selectedAppraiserIndex", selectedAppraiserIndex );
		
		// load the appraisal (return if not present)
		IAppraisal appraisal = appraisalService.findBy( appraisalId );
		if (appraisal == null) {
			return mapping.findForward( "failure" );
		}
		
		constructTradeAnalyzerForm(appraisal, tradeAnalyzerForm, dealer
				.getDealerPreference().getGuideBookIdAsInt(), request);
		
		//Check if dealer has KBBConsumerTool
		//to display link on trade manager detail page
		DealerUpgrade KBBConsumerToolUpgrade = dealerUpgradeDAO.findUpgrade(currentDealerId, 17);
		tradeAnalyzerForm.setHasKBBConsumerTool((KBBConsumerToolUpgrade != null) ? KBBConsumerToolUpgrade.isActive() : Boolean.FALSE);
		
		populateCustomerInformation(tradeAnalyzerForm);
		
		// If it exists, add the mmrVehicle to the tradeAnalyzerForm
		if (appraisal.getMmrVehicle() != null)
		{
			tradeAnalyzerForm.setSelectedMmrVehicle(appraisal.getMmrVehicle());			
			//String basicAuthString = (String) request.getSession().getServletContext().getAttribute(BasicAuthAdapter.NAME_IN_SESSION);
			
			//tradeAnalyzerForm.populateMMRAreas(basicAuthString);
			//tradeAnalyzerForm.populateMMRTrims(basicAuthString);				
			//tradeAnalyzerForm.populateMMRTransactions(basicAuthString);
		}

		try {

			CarfaxService carFaxService = CarfaxService.getInstance();
			
			Map<String,String> properties = carFaxService.getCarfaxReportProperties(
					currentDealerId,
					member.getLogin(),
					appraisal.getVehicle().getVin(),
					VehicleEntityType.APPRAISAL);
			for (Map.Entry<String, String> entry : properties.entrySet()) {
				request.setAttribute(entry.getKey(), entry.getValue());
			}
			
			List<CarfaxReportInspectionTO> carfaxReportInspections = Collections.emptyList();
			
			CarfaxReportTO carfaxReportTO = carFaxService.getCarfaxReport(currentDealerId, member.getLogin(),
					appraisal.getVehicle().getVin());
			
			if (carfaxReportTO != null){
				
				tradeAnalyzerForm.setCarfaxReport(carfaxReportTO);
				
				carfaxReportInspections = carFaxService.getCarfaxReportInspections(carfaxReportTO);
				
				String carfaxReportUrl = CarfaxService.getInstance().reportUrl(carfaxReportTO);
				request.setAttribute("carfaxReportUrl", carfaxReportUrl);
				
			}
			
			request.setAttribute("carfaxReportInspections", carfaxReportInspections);

		} catch (CarfaxException ce) {
			request.setAttribute("hasCarfaxError", true);
			request.setAttribute("problem", ce.getResponseCode());
		}
		
		try {

			AutoCheckService autoCheckService = AutoCheckService.getInstance();
			
			Map<String,String> properties = autoCheckService.getAutoCheckReportProperties(
					currentDealerId,
					member.getLogin(),
					appraisal.getVehicle().getVin());
			for (Map.Entry<String, String> entry : properties.entrySet()) {
				request.setAttribute(entry.getKey(), entry.getValue());
			}
			
			List<AutoCheckReportInspectionTO> autoCheckReportInspections = Collections.emptyList();
			
			AutoCheckReportTO autoCheckReportTO = autoCheckService.getAutoCheckReport(currentDealerId, member.getLogin(),
					appraisal.getVehicle().getVin());
			
			if (autoCheckReportTO != null){

				tradeAnalyzerForm.setAutocheckReport(autoCheckReportTO);

				autoCheckReportInspections = autoCheckService.getAutoCheckReportInspectionTO(autoCheckReportTO);
			}
			
			request.setAttribute("autoCheckReportInspections", autoCheckReportInspections);

		} catch (AutoCheckException ace) {
			//prevent rest of page blowing up, what to do for autocheck error???
		}
		
		checkDealerGroupInclude(request, tradeAnalyzerForm);

		reportGroupingRequestHelper.putReportGroupingFormsInRequest(request,
				tradeAnalyzerForm, weeks);

		setAttributesOnAction(request, appraisal, tradeAnalyzerForm, weeks,
				dealer.getDealerPreference().getGuideBookIdAsInt(), dealer
						.getDealerPreference()
						.getSearchInactiveInventoryDaysBackThreshold());

		String fwd = request.getParameter("update");
		String forwardName = "update";

		if (fwd == null || fwd.equals("false")) {
			forwardName = "loadFirst";
		}
		
		// crm appraisals go to step 1 of the appraisal process
		if ( appraisal.getAppraisalSource().isCRMOrMobile()) {
			forwardName = "isWirelessAppraisal";			
		}

		List<DemandDealer> demandDealers = retrieveDemandDealers(
				tradeAnalyzerForm, appraisal.getVehicle()
						.getMakeModelGrouping().getGroupingDescription()
						.getGroupingDescriptionId().intValue(), dealer);
		boolean hasDemandDealers = false;
		if (demandDealers != null && !demandDealers.isEmpty()) {
			request.setAttribute("demandDealers", demandDealers);
			tradeAnalyzerForm.setDemandDealers(demandDealers);
			Integer numDealers = dealer.getDealerPreference()
					.getRedistributionNumTopDealers();
			request.setAttribute("numDemandDealers", numDealers);
			hasDemandDealers = true;
		}
		setBookInformationForPrintMenu(request, dealer);

		AppraisalFormOptions formOptions = appraisal.getAppraisalFormOptions();
		int customerOffer = formOptions.getAppraisalFormOffer();
		boolean useAppraisalValue = formOptions.isOfferAppraisalValue();

		request.setAttribute("customerOffer", customerOffer);
		request.setAttribute("useAppraisalValue", useAppraisalValue);

		request.setAttribute("showAppraisalForm", dealer.getDealerPreference()
				.getShowAppraisalForm());
		request.setAttribute("locked", appraisal.isBookoutLocked());
		request.setAttribute("hasDemandDealers", new Boolean(hasDemandDealers));
		request.setAttribute("isTradeManager", Boolean.TRUE);
		request.setAttribute("tradeAnalyzerForm", tradeAnalyzerForm);
		SessionHelper.setAttribute(request, "tradeAnalyzerForm",
				tradeAnalyzerForm);
		SessionHelper.keepAttribute(request, "tradeAnalyzerForm");
		SessionHelper.keepAttribute(request, "auctionData");

		boolean apppraisalLockoutEnabled = getFirstlookSessionFromRequest(
				request).isIncludeAppraisalLockout();
		DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerId(
				currentDealerId);
		if (dealerGroup.isLithiaStore()) {
			request.setAttribute("isLithia", true);
			apppraisalLockoutEnabled = true;
			request.setAttribute("replyToAddr", member.getEmailAddress());
		}
		request.setAttribute("hasTransferPricing", dealerGroup
				.getDealerGroupPreference().isIncludeTransferPricing());
		try {
			VehicleCatalogEntry vce = vehicleCatalogService
					.retrieveVehicleCatalogEntry(tradeAnalyzerForm.getVin(),
							tradeAnalyzerForm.getCatalogKeyId());

			request.setAttribute("showJDPower", getFirstlookSessionFromRequest(
					request).isIncludeJDPowerUsedCarMarketData());
			request.setAttribute("JDPowerUrl", JdPowerRedirectionAction
					.getRelativeUrl(vce.getCatalogKey().getVehicleCatalogId(),
							vce.getModelYear()));
		} catch (VehicleCatalogServiceException e) {
			logger
					.warn(MessageFormat
							.format(
									"Could not find vehicle catalogid for vin: {0}, disabling JDPowerMarketAnalyzer.",
									tradeAnalyzerForm.getVin()));
		}

		request.setAttribute("bookoutLockoutEnabled", apppraisalLockoutEnabled);
		request.setAttribute("displayTMV", getFirstlookSessionFromRequest(
				request).hasEdmundsTmvUpgrade());
		request.setAttribute("twixURL", dealer.getDealerPreference()
				.getTwixURL());

		request.setAttribute("displayPing", getFirstlookSessionFromRequest(
				request).isIncludePing());
		request.setAttribute("displayPingII", getFirstlookSessionFromRequest(
				request).isIncludePingII());
		request.setAttribute("zipcode", dealer.getZipcode());

		String pageName = RequestHelper.getString(request, "pageName");
		if (request.getParameter("fromIMP") != null
				&& request.getParameter("fromIMP").equalsIgnoreCase("true")) {
			pageName = "fromIMP";
		}
		
		request.setAttribute("pageName", pageName);
		
		ActionForward forward = mapping.findForward(forwardName); 
		
		return forward; 
	}

	private void putBumpsInRequest( HttpServletRequest request, Integer appraisalId ) throws DatabaseException, ApplicationException
	{
		IAppraisal appraisal = appraisalService.findBy( appraisalId );
		List<AppraisalValue> appraisalValues = new ArrayList<AppraisalValue>();
		appraisalValues.addAll( appraisal.getAppraisalValues() );
		Collections.reverse( appraisalValues );
		TradeManagerEditBumpForm tradeManagerEditBumpForm = new TradeManagerEditBumpForm();
		String dateString="";
		if ( !appraisalValues.isEmpty() )
		{
			AppraisalValue firstValue = (AppraisalValue)appraisalValues.get( 0 );
			tradeManagerEditBumpForm.setAppraiserName( firstValue.getAppraiserName() );
			dateString = firstValue.getDateCreated().toLocaleString();
			tradeManagerEditBumpForm.setAppraisalBumpDate(dateString);
			tradeManagerEditBumpForm.setAppraisalValue( firstValue.getValue() );
		}
		tradeManagerEditBumpForm.setVehicleGuideBookBumpCol( appraisalValues );
		tradeManagerEditBumpForm.setAppraisalId( appraisalId.intValue() );
		if ( !appraisalValues.isEmpty() )
		{
			request.setAttribute( "numberOfAppraisal", appraisalValues.size() );
		}
		FirstlookSession flSession = (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION );
		int currentDealerId = ( flSession ).getCurrentDealerId();
		DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerId( currentDealerId );
		Dealer dealer = getDealerDAO().findByPk( currentDealerId );
		
		request.setAttribute("displayTMV", flSession.hasEdmundsTmvUpgrade());
		
//		if( dealerGroup.isLithiaStore()) {
//			loadLithiaCarCenterAttributes(request, dealer);
//		}
//		
	    checkInGroupAppraisals(appraisal.getBusinessUnitId(), dealerGroup.getDealerGroupId(),appraisal.getVehicle().getVin(), request);
	    
	    request.setAttribute("appraisersList", getPersonService().getBusinessUnitPeopleByPosition(currentDealerId,PositionType.APPRAISER));
	    
		request.setAttribute("isLithia", new Boolean( dealerGroup.isLithiaStore() ));
		request.setAttribute( "tradeManagerEditBumpForm", tradeManagerEditBumpForm );
	    request.setAttribute("vin", appraisal.getVehicle().getVin());
		SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
		SessionHelper.keepAttribute( request, "bookoutDetailForm" );
		SessionHelper.keepAttribute( request, "auctionData" );
	}

	@SuppressWarnings("unchecked")
	private void checkInGroupAppraisals(Integer businessUnitId, Integer parentId, String vin, HttpServletRequest request) {
	    List<Map> results = appraisalService.retrieveInGroupAppraisals(businessUnitId, parentId,vin);
	    boolean hasGroupAppraisals = (results.isEmpty() ? false:true);
	    request.setAttribute("hasGroupAppraisals", hasGroupAppraisals);
	    if (hasGroupAppraisals) {
	        request.setAttribute("created",results.get(0).get("created"));
	    } 
	}

	private void setBookInformationForPrintMenu(HttpServletRequest request,
			Dealer dealer) {
		boolean hasSecondaryGuideBook = false;
		boolean isKelleyGuideBook = false;
		boolean isKelleySecondaryGuideBook = false;
		String title = "";
		String titleSecondary = "";
		List<Integer> thirdPartyBookIds = dealer.getDealerPreference()
				.getOrderedGuideBookIds();
		Integer guideBookId = (Integer) thirdPartyBookIds.get(0);
		if (guideBookId.intValue() == ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE) {
			isKelleyGuideBook = true;
			title = "KBB";
		} else {
			title = ThirdPartyDataProvider
					.getThirdPartyDataProviderDescription(guideBookId
							.intValue());
		}
		if (thirdPartyBookIds.size() > 1) {
			hasSecondaryGuideBook = true;
			Integer guideBookId2 = (Integer) thirdPartyBookIds.get(1);
			if (guideBookId2.intValue() == ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE) {
				isKelleySecondaryGuideBook = true;
				titleSecondary = "KBB";
			} else {
				titleSecondary = ThirdPartyDataProvider
						.getThirdPartyDataProviderDescription(guideBookId2
								.intValue());
			}
		}
		request.setAttribute("isKelleyGuideBook",
				new Boolean(isKelleyGuideBook));
		request.setAttribute("isKelleySecondaryGuideBook", new Boolean(
				isKelleySecondaryGuideBook));
		request.setAttribute("hasSecondaryGuideBook", new Boolean(
				hasSecondaryGuideBook));
		request.setAttribute("title", title);
		request.setAttribute("titleSecondary", titleSecondary);
	}

	private void constructTradeAnalyzerForm(IAppraisal appraisal,
			TradeAnalyzerForm tradeAnalyzerForm, int thirdPartyId,
			HttpServletRequest request) {
		tradeAnalyzerForm.setCatalogKey(appraisal.getVehicle()
				.getVehicleCatalogKey());
		tradeAnalyzerForm.setColor(appraisal.getColor());
		tradeAnalyzerForm.setMake(appraisal.getVehicle().getMake());
		tradeAnalyzerForm.setModel(appraisal.getVehicle().getModel());
		tradeAnalyzerForm.setTrim(appraisal.getVehicle().getVehicleTrim());
		tradeAnalyzerForm.setAppraisalId(appraisal.getAppraisalId());
		tradeAnalyzerForm.setAppraisalTypeEnum(AppraisalTypeEnum
				.getType(appraisal.getAppraisalTypeId()));
		tradeAnalyzerForm.setMileage(new Integer(appraisal.getMileage()));
		tradeAnalyzerForm.setEdmundsTMV(appraisal.getEdmundsTMV());

		tradeAnalyzerForm.setConditionDisclosure(appraisal
				.getConditionDescription());
		tradeAnalyzerForm.setActionId(appraisal.getAppraisalActionType()
				.getAppraisalActionTypeId());
		tradeAnalyzerForm.setReconditioningCost(appraisal
				.getEstimatedReconditioningCost());
		tradeAnalyzerForm.setTargetGrossProfit(appraisal.getTargetGrossProfit());
		tradeAnalyzerForm.setPrice((appraisal.getWholesalePrice() == null ? 0
				: appraisal.getWholesalePrice().intValue()));

		tradeAnalyzerForm.setVin(appraisal.getVehicle().getVin());
		tradeAnalyzerForm.setYear(appraisal.getVehicle().getVehicleYear()
				.intValue());
		SimpleDateFormat sdf = new SimpleDateFormat("MMM d, yyyy");
		tradeAnalyzerForm.setTradeAnalyzerDate(sdf.format(appraisal
				.getDateCreated()));
		tradeAnalyzerForm.setMakeModelGroupingId(appraisal.getVehicle()
				.getMakeModelGrouping().getMakeModelGroupingId());
		tradeAnalyzerForm.setGroupingDescriptionId(appraisal.getVehicle()
				.getMakeModelGrouping().getGroupingDescriptionId().intValue());

		tradeAnalyzerForm.setDealTrackNewOrUsed(appraisal
				.getDealTrackNewOrUsed() != null ? appraisal
				.getDealTrackNewOrUsed().ordinal() : null);
		// nk - for those appraisals of type Purchase
		if (appraisal.getSelectedBuyer() != null) {
			tradeAnalyzerForm.setBuyerId(appraisal.getSelectedBuyer()
					.getPersonId());
			tradeAnalyzerForm.setBuyerName(appraisal.getSelectedBuyer().getFullName());
		}

		tradeAnalyzerForm.setDealTrackStockNumber(appraisal
				.getDealTrackStockNumber());
		if (appraisal.getDealTrackSalesPerson() != null) {
			tradeAnalyzerForm.setDealTrackSalesperson(appraisal
					.getDealTrackSalesPerson().getFullName());
			tradeAnalyzerForm.setDealTrackSalespersonID(appraisal
					.getDealTrackSalesPerson().getPersonId() == null ? ""
					: appraisal.getDealTrackSalesPerson().getPersonId()
							.toString());
		}
		
		AppraisalValue valueObj = appraisal.getLatestAppraisalValue();
		tradeAnalyzerForm.setAppraisalValue( valueObj == null ? 0 : valueObj.getValue() );
		
		// nk - refactor this
		tradeAnalyzerForm
				.setDealTrackSalespeopleDropdownList(getDealerMembersPlusOthersList(request));
		tradeAnalyzerForm.setTransferPrice(appraisal.getTransferPrice());
		tradeAnalyzerForm.setTransferForRetailOnly(appraisal
				.isTransferForRetailOnly());
	}

	private void populateCustomerInformation(TradeAnalyzerForm tradeAnalyzerForm) {

		IAppraisal appraisal = appraisalService.findBy(tradeAnalyzerForm
				.getAppraisalId());
		Customer customer = appraisal.getCustomer();
		if (customer != null) {
			// Customer's PK is appraisalId. Customer = AppraisalCustomer
			tradeAnalyzerForm.setCustomerId(appraisal.getAppraisalId());
			tradeAnalyzerForm.setCustomerFirstName(customer.getFirstName());
			tradeAnalyzerForm.setCustomerLastName(customer.getLastName());
			tradeAnalyzerForm.setCustomerGender(customer.getGender());
			tradeAnalyzerForm.setCustomerPhoneNumber(customer.getPhoneNumber());
			tradeAnalyzerForm.setCustomerEmail(customer.getEmail());
		}

	}

	// A collection of strings of salespeople for dealer track.
	// Start by adding list of dealer members.
	// The plus others refers to another list of names added from the dealer
	// track tile.
	@SuppressWarnings("unchecked")
	private List<String> getDealerMembersPlusOthersList(
			HttpServletRequest request) {
		IDealerService dealerService = getImtDealerService();

		int dealerId = ((FirstlookSession) request.getSession().getAttribute(
				FirstlookSession.FIRSTLOOK_SESSION)).getCurrentDealerId();
		Collection dealerMembers = dealerService.retrieveMembers(dealerId);

		List members = Arrays.asList(dealerMembers.toArray());

		// Sort by last name
		Comparator lastName = new BeanComparator("lastName");
		Comparator firstName = new BeanComparator("firstName");
		Comparator comparator = new ComparatorChain(Arrays
				.asList(new Comparator[] { lastName, firstName }));
		Collections.sort(members, comparator);

		List<String> arrayList = new ArrayList<String>();

		// Only need a list of firstnames and lastnames
		Member member;
		Iterator iterator = members.iterator();
		String tmpString;
		while (iterator.hasNext()) {
			member = (Member) iterator.next();
			tmpString = member.getFirstName() + " " + member.getLastName();
			arrayList.add(tmpString);
		}

		return arrayList;
	}

	private List<DemandDealer> retrieveDemandDealers(
			TradeAnalyzerForm tradeAnalyzerForm, int groupingId, Dealer dealer)
			throws ApplicationException, DatabaseException {
		int mileage = tradeAnalyzerForm.getMileage().intValue();
		int year = tradeAnalyzerForm.getYear();
		Integer distance = dealer.getDealerPreference()
				.getRedistributionDealerDistance();
		Integer sortBy = getImtDealerService().determineDemandDealerSortOrder(
				dealer.getBusinessUnitId(),
				dealer.getDealerPreference().getRedistributionUnderstock(),
				dealer.getDealerPreference().getRedistributionROI());

		List<DemandDealer> demandDealers = getImtDealerService()
				.retrieveDemandDealers(dealer.getDealerId(),
						new Integer(groupingId), new Integer(mileage),
						new Integer(year), distance, sortBy);

		return demandDealers;

	}

	private void setAttributesOnAction(HttpServletRequest request,
			IAppraisal appraisal, TradeAnalyzerForm tradeAnalyzerForm,
			int weeks, int guideBookId, Integer inactiveInventoryDaysBack)
			throws ApplicationException {
		String stockNumber = inventoryService_Legacy
				.retrieveStockNumberByVinAndDealerId(
						tradeAnalyzerForm.getVin(), appraisal
								.getBusinessUnitId(), inactiveInventoryDaysBack);

		if (stockNumber != null) {
			request.setAttribute("stockNumber", stockNumber);
		}

		request.setAttribute("includeDealerGroup", new Integer(
				tradeAnalyzerForm.getIncludeDealerGroup()));

		request.setAttribute("mileage", new Integer(appraisal.getMileage()));
		request.setAttribute("weeks", new Integer(weeks));
		request.setAttribute("groupingDescriptionId", appraisal.getVehicle()
				.getMakeModelGrouping().getGroupingDescription()
				.getGroupingDescriptionId());

		// MH - stuff for book out tile
		request.setAttribute("currentDate",new Date());
		request.setAttribute("vin", appraisal.getVehicle().getVin());
		request.setAttribute("appraisalId", appraisal.getAppraisalId());
		request.setAttribute("bookOutSourceId",
				BookOutSource.BOOK_OUT_SOURCE_APPRAISAL);

		request.setAttribute("enablePrintMenuForTradeAnalyzerOrTradeManager",
				Boolean.TRUE);
		request.setAttribute("enablePrint", "true");
	}

	private void checkDealerGroupInclude(HttpServletRequest request,
			TradeAnalyzerForm tradeAnalyzerForm) throws ApplicationException,
			DatabaseException {
		if (RequestHelper.getClickedButton(request).equalsIgnoreCase(
				BaseActionForm.BUTTON_DEALERGROUP_RESULTS)) {
			tradeAnalyzerForm
					.setIncludeDealerGroup(Report.DEALERGROUP_INCLUDE_TRUE);
		}
		if (RequestHelper.getClickedButton(request).equalsIgnoreCase(
				BaseActionForm.BUTTON_DEALER_RESULTS)) {
			tradeAnalyzerForm
					.setIncludeDealerGroup(Report.DEALERGROUP_INCLUDE_FALSE);
		}
	}

	// Available in newer version of Commons Collections.
	class BeanPredicate implements Predicate {
		private String property;
		private Predicate predicate;

		public BeanPredicate(String property, Predicate predicate) {
			this.property = property;
			this.predicate = predicate;
		}

		public boolean evaluate(Object obj) {
			Object propValue;
			try {
				propValue = PropertyUtils.getProperty(obj, property);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
			return predicate.evaluate(propValue);
		}
	}

	public BasisPeriodService getBasisPeriodService() {
		return basisPeriodService;
	}

	public void setBasisPeriodService(BasisPeriodService basisPeriodService) {
		this.basisPeriodService = basisPeriodService;
	}

	public void setInventoryService_Legacy(
			InventoryService_Legacy inventoryService) {
		this.inventoryService_Legacy = inventoryService;
	}

	public DealerPreferenceDAO getDealerPrefDAO() {
		return dealerPrefDAO;
	}

	public void setDealerPrefDAO(DealerPreferenceDAO dealerPrefDAO) {
		this.dealerPrefDAO = dealerPrefDAO;
	}


	public DealerUpgradeDAO getDealerUpgradeDAO() {
		return dealerUpgradeDAO;
	}

	public void setDealerUpgradeDAO(DealerUpgradeDAO dealerUpgradeDAO) {
		this.dealerUpgradeDAO = dealerUpgradeDAO;
	}
	
	public void setAppraisalService(IAppraisalService appraisalService) {
		this.appraisalService = appraisalService;
	}

	public void setReportGroupingRequestHelper(
			ReportGroupingRequestHelper reportGroupingRequestHelper) {
		this.reportGroupingRequestHelper = reportGroupingRequestHelper;
	}

	public void setVehicleCatalogService(
			IVehicleCatalogService vehicleCatalogService) {
		this.vehicleCatalogService = vehicleCatalogService;
	}

}