package com.firstlook.action.dealer.sell;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.inventory.InventoryService_Legacy;
import com.firstlook.session.FirstlookSession;

public class ReviewSelectedVehiclesDisplayAction extends
        SelectVehiclesBaseAction
{
private InventoryService_Legacy inventoryService_Legacy;

String getForwardMapping( HttpServletRequest request )
{
    String whereToGo = request.getParameter("saveSubmission.x");

    if ( whereToGo != null )
    {
        return "submissionSuccess";
    } else
    {
        return "reviewSuccess";
    }
}

public ActionForward doIt( ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response ) throws DatabaseException,
        ApplicationException
{
    Collection vehicles = inventoryService_Legacy
            .retrieveByDealerIdOrderByReceivedDateStockNumber(((FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION )).getCurrentDealerId());

    putFormsInRequest(vehicles, request);

    return mapping.findForward(getForwardMapping(request));
}

public void setInventoryService_Legacy(InventoryService_Legacy inventoryService) {
	this.inventoryService_Legacy = inventoryService;
}



}
