package com.firstlook.action.dealer.buy;

import java.util.Collection;

import biz.firstlook.commons.enumeration.LightAndCIATypeDescriptor;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.report.CustomIndicator;
import com.firstlook.service.inventory.InventoryService_Legacy;

public class InventoryExchangeDisplayAction extends BaseBuyingGuideDisplayAction
{

private InventoryService_Legacy inventoryService_Legacy;
	
protected Collection<InventoryEntity> getVehiclesToDisplay( Dealer dealer,
        CustomIndicator indicator ) throws DatabaseException,
        ApplicationException
{
    Collection<InventoryEntity> vehiclesToDisplay = inventoryService_Legacy.retrieveInventoryByDealerGroupIdAndLight(dealer,
            LightAndCIATypeDescriptor.GREEN_LIGHT, getDealerGroupService(), getImtDealerService() );
    return vehiclesToDisplay;
}

public void setInventoryService_Legacy( InventoryService_Legacy inventoryService )
{
	this.inventoryService_Legacy = inventoryService;
}

}
