package com.firstlook.action.dealer.scorecard;

import java.util.Calendar;
import java.util.Date;

public class UnitSalesScoreCardData
{

private Date reportDate;
private int monthToDateUnitSales;
private int lastMonthUnitSales;
private double threeMonthAverageUnitSales;

public UnitSalesScoreCardData()
{
    super();
}

public int getLastMonthUnitSales()
{
    return lastMonthUnitSales;
}

public int getMonthToDateUnitSales()
{
    return monthToDateUnitSales;
}

public double getThreeMonthAverageUnitSales()
{
    return threeMonthAverageUnitSales;
}

public int getThreeMonthAverageUnitSalesFormatted()
{
    return (int) Math.round(threeMonthAverageUnitSales);
}

public void setLastMonthUnitSales( int i )
{
    lastMonthUnitSales = i;
}

public void setMonthToDateUnitSales( int i )
{
    monthToDateUnitSales = i;
}

public void setThreeMonthAverageUnitSales( double d )
{
    threeMonthAverageUnitSales = d;
}

public Date getReportDate()
{
    return reportDate;
}

public void setReportDate( Date date )
{
    reportDate = date;
}

public Date getLastMonth()
{
    Calendar cal = Calendar.getInstance();

    if ( reportDate != null )
    {
        cal.setTime(reportDate);
        cal.add(Calendar.MONTH, -1);
        return cal.getTime();
    } else
        return null;
}

public Date getThreeMonthStart()
{
    Calendar cal = Calendar.getInstance();

    if ( reportDate != null )
    {
        cal.setTime(reportDate);
        cal.add(Calendar.MONTH, -4);
        return cal.getTime();
    } else
        return null;
}

public Date getThreeMonthEnd()
{
    Calendar cal = Calendar.getInstance();

    if ( reportDate != null )
    {
        cal.setTime(reportDate);
        cal.add(Calendar.MONTH, -2);
        return cal.getTime();
    } else
        return null;
}

}
