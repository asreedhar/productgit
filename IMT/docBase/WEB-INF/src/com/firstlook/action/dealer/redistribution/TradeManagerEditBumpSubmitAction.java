package com.firstlook.action.dealer.redistribution;

import java.util.Collections;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import biz.firstlook.commons.email.EmailService;
import biz.firstlook.commons.util.Functions;
import biz.firstlook.transact.persist.person.IPerson;
import biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum;
import biz.firstlook.transact.persist.service.appraisal.AppraisalValue;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.email.LithiaCarCenterAppraisalValueAssignedEmailBuilder;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;

public class TradeManagerEditBumpSubmitAction extends SecureBaseAction
{
private static Log logger = LogFactory.getLog( TradeManagerEditBumpSubmitAction.class );

private IAppraisalService appraisalService;
private EmailService emailService;
private TransactionTemplate transactionTemplate;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int bumpValue = 0;	
	boolean noBumpValue = false;
	boolean updateTMV = false;
	try
	{
		final String newBumpValue = request.getParameter( "newBumpValue" );
		if (newBumpValue == null || newBumpValue == "")
		{
			noBumpValue = true;	
		}
		else if (newBumpValue.indexOf('.') > -1) {            
            bumpValue = (int)Double.parseDouble(newBumpValue);
        } else {
        	bumpValue = Integer.parseInt( newBumpValue );
        }
	}
	catch ( NumberFormatException nfe )
	{
		bumpValue = 0;
		logger.warn( "Unable to parse appraisal amount to valid value. Setting appraisal amount to 0. ", nfe );
	}

	String dateMillis = request.getParameter("date");
	Date createDate = new Date();
	try {
		createDate = new Date( Long.parseLong( dateMillis ) );	
	}catch ( NumberFormatException nfe ) {
		logger.warn( "Unable to parse client date.  Using server side date ", nfe );
	}
	
	String edmundsParam= request.getParameter( "edmundsTMV");
	double edmundsTMV  = 0;
	if(edmundsParam != null && edmundsParam != ""){
	 edmundsTMV = Double.parseDouble(edmundsParam);
	 updateTMV = true;
	}
	// do not store empty appraisals
	if( !(bumpValue > 0) && !updateTMV)
	{
		return mapping.findForward( "success" );
	}

	// get the appraisal id (return if not present)
	final String parameter = request.getParameter("appraisalId");
	final Object attribute = request.getAttribute("appraisalId");
	Integer appraisalId = Functions.nullSafeInteger(parameter);
	if (appraisalId == null) {
		appraisalId = Functions.nullSafeInteger(attribute);
	}
	if (appraisalId == null) {
		return mapping.findForward( "failure" );
	}
	
	// load the appraisal (return if not present)
	IAppraisal appraisal = appraisalService.findBy( appraisalId );
	if (appraisal == null) {
		return mapping.findForward( "failure" );
	}
	Member member=getFirstlookSessionFromRequest(request).getMember();
	
	performBump(request, bumpValue, noBumpValue,
			updateTMV, createDate, appraisal, edmundsTMV,member.getFirstName()+" "+member.getLastName());
	
	Dealer dealer = putDealerFormInRequest(request, 0);
	request.setAttribute( "showAppraisalForm", dealer.getDealerPreference().getShowAppraisalForm() );
	request.setAttribute( "appraisalId", Integer.valueOf(appraisalId) );
	request.setAttribute( "appraisalType", appraisal.getAppraisalTypeId());
	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );
	return mapping.findForward( "success" );
}

private void performBump(final HttpServletRequest request, final int bumpValue, final boolean noBumpValue
		, final boolean updateTMV, final Date createDate, final IAppraisal appraisal, final double edmundsTMV, final String memberName) {
	transactionTemplate.execute(new TransactionCallback(){

		public Object doInTransaction(TransactionStatus arg0) {
			
			AppraisalValue currentAppraisalValue = appraisal.getLatestAppraisalValue();
		
			String bumperName = "";
			if (AppraisalTypeEnum.TRADE_IN == AppraisalTypeEnum.getType(appraisal.getAppraisalTypeId())) {
				bumperName = request.getParameter( "appraiserName" );
				// this is to fix weird bug where changing the drop down away from the empy string, then back to it give syou a weird char
				if ( bumperName != null && !StringUtils.isAlphanumeric( bumperName ) && bumperName.length() == 2 ) {
					bumperName = "";
				}
			} else {
				Integer buyerId = Functions.nullSafeInteger(request.getParameter("buyerId"));
				if (buyerId != null && buyerId > 0) {
					IPerson buyer = getPersonService().getPerson(buyerId);
					if (buyer != null)
						bumperName = buyer.getFullName();
				}
			}
		
			
			if(updateTMV){
				appraisal.setEdmundsTMV(edmundsTMV);
			}
			
			
			if(noBumpValue && updateTMV){
				//just update edmunds value if no appraisal value was given
				appraisalService.updateAppraisal( appraisal );
			}
			// don't allow the same appraisal bump to get recorded consecutively
			// this is done becuase refreshing a page after an appraisal submission cuases
			// a the exact same appraisal bump to get recoreded a second time - DW 1/26/06
			else if ( currentAppraisalValue == null ||
				 new Double(currentAppraisalValue.getValue()).intValue() != bumpValue || 
				 !currentAppraisalValue.getAppraiserName().equals( bumperName ))
			{
				appraisal.bump( bumpValue, bumperName, createDate, memberName );
				appraisalService.updateAppraisal( appraisal );
				
				// if send notification was checked (which means the dealer was lithia - then send an e-mail
				if( request.getParameter( "sendNotification" ) != null && "on".equals( request.getParameter( "sendNotification" ) )) {
					sendLithiaCarCenterNotification( appraisal, request.getParameter("lithiaCarCenterContact")+"", bumperName, bumpValue,
					                                 getMemberFromRequest( request ).getEmailAddress() );
				}
				
			}
			else if(updateTMV){
				//in case appraisal value or appraiser name are the same, still need to update TMV value
				appraisalService.updateAppraisal( appraisal );
			}
			return appraisal;
		}
		
	});
}

private void sendLithiaCarCenterNotification( IAppraisal appraisal, String contactEmailAddress, String appraiserName, int appraisalValue, 
                                              String memberEmailAddress ) {
	
	Dealer dealer = getDealerDAO().findByPk( appraisal.getBusinessUnitId() );
	LithiaCarCenterAppraisalValueAssignedEmailBuilder emailBuilder = 
		new LithiaCarCenterAppraisalValueAssignedEmailBuilder(
	                                    dealer.getNickname(),
	                                    appraisal.getVehicle().getVehicleYear(),
	                                    appraisal.getVehicle().getMake(),
	                                    appraisal.getVehicle().getModel(),
	                                    appraisal.getMileage(),
	                                    appraisal.getAppraisalId(),
	                                    contactEmailAddress,
	                                    Collections.EMPTY_LIST,
	                                    appraiserName, 
	                                    appraisalValue, 
	                                    memberEmailAddress);
	                                    
	emailService.sendEmail( emailBuilder );
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

public void setEmailService( EmailService emailService )
{
	this.emailService = emailService;
}

public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
	this.transactionTemplate = transactionTemplate;
}

}
