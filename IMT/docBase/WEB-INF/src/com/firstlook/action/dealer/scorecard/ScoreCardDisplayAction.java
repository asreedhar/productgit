package com.firstlook.action.dealer.scorecard;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import biz.firstlook.transact.persist.model.UserRoleEnum;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.InventoryTypeEnum;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.DealerHelper;
import com.firstlook.service.dealer.DealerFranchiseService;
import com.firstlook.session.FirstlookSession;
import com.firstlook.session.ProductService;

public class ScoreCardDisplayAction extends AbstractScoreCardAction
{

private ProductService productService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    DynaActionForm dForm = (DynaActionForm)form;
    String department = (String)dForm.get( "department" );
    if ( !department.equalsIgnoreCase( "None Specified" ) )
    {
        UserRoleEnum userRoleEnum = UserRoleEnum.getEnum( department );
        getMemberFromRequest( request ).setInventoryType(InventoryTypeEnum.getEnum(userRoleEnum) );
    }

    setModeOnSession( request );

    DealerFranchiseService dfService = new DealerFranchiseService( getDealerFranchiseDAO() );
    DealerHelper.putCurrentDealerFormInRequest( request, getImtDealerService(), getDealerGroupService() ,
                                                dfService );
    return super.doIt( mapping, form, request, response );
}

private void setModeOnSession( HttpServletRequest request )
{
    String mode = request.getParameter( "mode" );
    if ( mode == null )
    {
        mode = (String)request.getAttribute( "mode" );
    }
    if ( mode != null )
    {
        FirstlookSession firstlookSession = (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION );
        firstlookSession.setProduct( productService.retrieveByName( mode ) );
    }
}

protected String determineForward( int inventoryType )
{
    return "success";
}

public void setProductService( ProductService productService )
{
	this.productService = productService;
}

}
