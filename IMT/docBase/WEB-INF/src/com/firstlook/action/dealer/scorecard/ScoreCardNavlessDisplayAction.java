package com.firstlook.action.dealer.scorecard;

import javax.servlet.http.HttpServletRequest;

import com.firstlook.entity.InventoryTypeEnum;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;

public class ScoreCardNavlessDisplayAction extends AbstractScoreCardAction
{

protected String determineForward( int inventoryType )
{
    if ( inventoryType == InventoryTypeEnum.NEW_VAL )
    {
        return "new";
    }
    else
    {
        return "used";
    }
}

protected int determineInventoryType( HttpServletRequest request ) throws ApplicationException
{
    return RequestHelper.getInt( request, "inventoryType" );
}

protected int determineDealerId( HttpServletRequest request ) throws ApplicationException
{
    return RequestHelper.getInt( request, "dealerId" );
}

}
