package com.firstlook.action.dealer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.model.UserRoleEnum;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.InventoryTypeEnum;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.session.FirstlookSession;

public class SetCurrentDealerAndProgramTypeAction extends SecureBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int dealerId = Integer.parseInt(request.getParameter(CURRENT_DEALER_ID_PARAMETER));
	Member member = getMemberFromRequest(request);
	DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerId( dealerId );

	request.setAttribute( "dealerId", String.valueOf( dealerId ) );
	request.setAttribute( "dealerGroupId", dealerGroup.getDealerGroupId() );
	
	if( member.isAssociatedWithDealer( dealerId ) ) {
		FirstlookSession firstlookSession = getFirstlookSessionManager().constructFirstlookSession( dealerId, member );
		request.getSession().setAttribute( FirstlookSession.FIRSTLOOK_SESSION, firstlookSession );
	}

	addDealerCookie(response, dealerId);
	
	Member user = getMemberFromRequest( request );
	user.setInventoryType(InventoryTypeEnum.getEnum(UserRoleEnum.USED));

	String forward = "success";
    if ("Firstlook".equals(user.getProgramType())) {
           forward = "flSuccess";
        }
	
	return mapping.findForward(forward);
}


}
