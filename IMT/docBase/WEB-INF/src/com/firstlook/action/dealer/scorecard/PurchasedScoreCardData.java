package com.firstlook.action.dealer.scorecard;

import com.firstlook.util.Formatter;

public class PurchasedScoreCardData extends ScoreCardData
{

private static final String PERCENT_WINNERS = "Percent Winners";
private static final String AVG_GROSS_PROFIT = "Retail Average Gross Profit";
private static final String PERCENT_SELL_THROUGH = "Percent Sell Through";
private static final String AVG_DAYS_SALE = "Average Days to Retail Sale";
private static final String AVERAGE_IVENTORY_AGE = "Average Inventory Age";

private double percentWinnersTrend;
private double percentWinners12Week;

private double aGPTrend;
private double aGP12Week;

private double avgDaysToSaleTrend;
private double avgDaysToSale12Week;

private double percentSellThroughTrend;
private double percentSellThrough12Week;

private double avgInventoryAgeCurrent;
private double avgInventoryAgePrior;

public PurchasedScoreCardData()
{
    super();
}

public double getAGP12Week()
{
    return aGP12Week;
}

public String getAGP12WeekFormatted()
{
    return Formatter.toPrice(aGP12Week);
}

public Integer getAGPTarget()
{
    return getTargetValue(AVG_GROSS_PROFIT);
}

public double getAGPTrend()
{
    return aGPTrend;
}

public String getAGPTrendFormatted()
{
    return Formatter.toPrice(aGPTrend);
}

public double getAvgDaysToSale12Week()
{
    return avgDaysToSale12Week;
}

public int getAvgDaysToSale12WeekFormatted()
{
    return (int) Math.round(avgDaysToSale12Week);
}

public Integer getAvgDaysToSaleTarget()
{
    return getTargetValue(AVG_DAYS_SALE);
}

public double getAvgDaysToSaleTrend()
{
    return avgDaysToSaleTrend;
}

public int getAvgDaysToSaleTrendFormatted()
{
    return (int) Math.round(avgDaysToSaleTrend);
}

public double getPercentSellThrough12Week()
{
    return percentSellThrough12Week;
}

public String getPercentSellThrough12WeekFormatted()
{
    return Formatter.toPercent(percentSellThrough12Week);
}

public Integer getPercentSellThroughTarget()
{
    return getTargetValue(PERCENT_SELL_THROUGH);
}

public double getPercentSellThroughTrend()
{
    return percentSellThroughTrend;
}

public String getPercentSellThroughTrendFormatted()
{
    return Formatter.toPercent(percentSellThroughTrend);
}

public double getPercentWinners12Week()
{
    return percentWinners12Week;
}

public String getPercentWinners12WeekFormatted()
{
    return Formatter.toPercent(percentWinners12Week);
}

public Integer getPercentWinnersTarget()
{
    return getTargetValue(PERCENT_WINNERS);
}

public double getPercentWinnersTrend()
{
    return percentWinnersTrend;
}

public String getPercentWinnersTrendFormatted()
{
    return Formatter.toPercent(percentWinnersTrend);
}

public void setAGP12Week( double i )
{
    aGP12Week = i;
}

public void setAGPTrend( double i )
{
    aGPTrend = i;
}

public void setAvgDaysToSale12Week( double d )
{
    avgDaysToSale12Week = d;
}

public void setAvgDaysToSaleTrend( double d )
{
    avgDaysToSaleTrend = d;
}

public void setPercentSellThrough12Week( double d )
{
    percentSellThrough12Week = d;
}

public void setPercentSellThroughTrend( double d )
{
    percentSellThroughTrend = d;
}

public void setPercentWinners12Week( double d )
{
    percentWinners12Week = d;
}

public void setPercentWinnersTrend( double d )
{
    percentWinnersTrend = d;
}

public Integer getAvgInventoryAgeTarget()
{
    return getTargetValue(AVERAGE_IVENTORY_AGE);
}

public double getAvgInventoryAgePrior()
{
    return avgInventoryAgePrior;
}

public double getAvgInventoryAgeCurrent()
{
    return avgInventoryAgeCurrent;
}

public void setAvgInventoryAgePrior( double d )
{
    avgInventoryAgePrior = d;
}

public void setAvgInventoryAgeCurrent( double d )
{
    avgInventoryAgeCurrent = d;
}

}
