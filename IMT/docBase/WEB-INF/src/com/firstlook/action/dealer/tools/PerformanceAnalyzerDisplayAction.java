package com.firstlook.action.dealer.tools;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.ImpactModeEnum;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.DealerHelper;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.action.ReportActionHelper;
import com.firstlook.iterator.SimpleFormIterator;
import com.firstlook.persistence.ReportPersist;
import com.firstlook.report.Report;
import com.firstlook.report.ReportForm;
import com.firstlook.service.dealer.IUCBPPreferenceService;

public class PerformanceAnalyzerDisplayAction extends SecureBaseAction
{

public final static String TREND_TYPE = "trendType";
public final static String TREND_TYPE_TOP_SELLERS_STRING = "topSellers";
public final static String TREND_TYPE_MOST_PROFITABLE_STRING = "mostProfitable";
public final static String TREND_TYPE_FASTEST_SELLERS_STRING = "fastestSellers";
public final static String PERFORMANCE_ANALYZER_FORECAST_ITERATOR = "forecastReport";
public final static String PERFORMANCE_ANALYZER_TREND_ITERATOR = "trendReport";
public final static String PERFORMANCE_ANALYZER_DEFAULT_ITERATOR = "defaultReport";

private ReportActionHelper reportActionHelper;
private ReportPersist reportPersist;
private IUCBPPreferenceService ucbpPreferenceService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	Dealer dealer = DealerHelper.putDealerInRequest( request, getFirstlookSessionFromRequest( request ).getCurrentDealerId(), getImtDealerService() );

	int currentDealerId = dealer.getDealerId();
	Dealer currentDealer = getImtDealerService().retrieveDealer( currentDealerId );

	int trendType = getTrendType( request, currentDealer );
	Member member = getMemberFromRequest( request );

	putAnalyzerReportsInRequest( request, member.getInventoryType().getValue(), trendType, dealer, member.getDashboardRowDisplay() );

	Integer mileage = new Integer( ucbpPreferenceService.retrieveDealerRisk( currentDealerId ).getHighMileageThreshold() );
	request.setAttribute( "mileage", mileage );

	return mapping.findForward( "success" );
}

public void putAnalyzerReportsInRequest( HttpServletRequest request, int inventoryType, int trendType, Dealer dealer,
										int dashboardRowDisplay ) throws ApplicationException, DatabaseException
{
	int mode = retrievePerspective( request ).getImpactModeEnum().getValue();

	putTrendReportsInRequest( request, dashboardRowDisplay, inventoryType, trendType, mode, dealer );
	request.setAttribute( TREND_TYPE, String.valueOf( trendType ) );
	putTrendReportLengthInRequest( dashboardRowDisplay, request );

	putReportAveragesInRequest( request, dealer, inventoryType );
}

private void putReportAveragesInRequest( HttpServletRequest request, Dealer dealer, int inventoryType ) throws ApplicationException
{
	int dealerId = dealer.getDealerId().intValue();

	Report defaultReportAvgs = new Report();

	reportPersist.findAllAverages( defaultReportAvgs, dealerId, ReportActionHelper.DEFAULT_NUMBER_OF_WEEKS, Report.FORECAST_FALSE,
									inventoryType );
	request.setAttribute( "defaultReportAvgs", new ReportForm( defaultReportAvgs ) );

	Report trendReportAvgs = new Report();
	reportPersist.findAllAverages( trendReportAvgs, dealerId, dealer.getDefaultTrendingWeeks(), Report.FORECAST_FALSE, inventoryType );
	request.setAttribute( "trendReportAvgs", new ReportForm( trendReportAvgs ) );

	Report forecastReportAvgs = new Report();
	reportPersist.findAllAverages( forecastReportAvgs, dealerId, dealer.getDefaultForecastingWeeks(), Report.FORECAST_TRUE, inventoryType );
	request.setAttribute( "forecastReportAvgs", new ReportForm( forecastReportAvgs ) );
}

private void putTrendReportsInRequest( HttpServletRequest request, int dashboardRowDisplay, int inventoryType,
										int trendType, int mode, Dealer currentDealer ) throws ApplicationException, DatabaseException
{
	Report defaultReport = reportActionHelper.createTrendReport( ReportActionHelper.DEFAULT_NUMBER_OF_WEEKS, 0, inventoryType, trendType, currentDealer );
	putReportIteratorInRequest( request, dashboardRowDisplay, defaultReport, trendType, PERFORMANCE_ANALYZER_DEFAULT_ITERATOR, mode );

	Report trendReport = reportActionHelper.createTrendReport( currentDealer.getDefaultTrendingWeeks(), 0, inventoryType, trendType, currentDealer );
	putReportIteratorInRequest( request, dashboardRowDisplay, trendReport, trendType, PERFORMANCE_ANALYZER_TREND_ITERATOR, mode );

	Report forecastReport = reportActionHelper.createTrendReport( currentDealer.getDefaultForecastingWeeks(), 1, inventoryType, trendType, currentDealer );
	putReportIteratorInRequest( request, dashboardRowDisplay, forecastReport, trendType, PERFORMANCE_ANALYZER_FORECAST_ITERATOR, mode );

}

private void putReportIteratorInRequest( HttpServletRequest request, int dashboardRowDisplay, Report report, int trendType, String beanName, int mode )
{
	ReportForm reportForm = new ReportForm( report );
	SimpleFormIterator iterator = getTrendTypeIterator( trendType, reportForm, mode );
	iterator.setCollectionSize( dashboardRowDisplay );
	request.setAttribute( beanName, iterator );
}

int getTrendType( HttpServletRequest request, Dealer dealer )
{
	int output = 0;
	try
	{
		output = RequestHelper.getInt( request, TREND_TYPE );
	}
	catch ( ApplicationException e )
	{
		output = dealer.getDefaultTrendingView();
	}
	return output;
}

String getTrendTypeString( int trendType ) throws ApplicationException
{
	String specificTrendType = "";

	if ( ( trendType == 1 ) || ( trendType == 0 ) )
	{
		specificTrendType = TREND_TYPE_TOP_SELLERS_STRING;
	}
	else if ( trendType == 2 )
	{
		specificTrendType = TREND_TYPE_FASTEST_SELLERS_STRING;
	}
	else if ( trendType == 3 )
	{
		specificTrendType = TREND_TYPE_MOST_PROFITABLE_STRING;
	}

	return specificTrendType;
}

SimpleFormIterator getTrendTypeIterator( int trendType, ReportForm form, int mode )
{

	switch ( trendType )
	{
		case Dealer.TRENDING_VIEW_TOP_SELLER:
		{
			if ( mode == ImpactModeEnum.PERCENTAGE_VAL )
			{
				return form.getTopSellerReportGroupingsInOptimix();
			}
			else
			{
				return form.getTopSellerReportGroupings();
			}
		}
		case Dealer.TRENDING_VIEW_FASTEST_SELLER:
		{
			return form.getFastestSellerReportGroupings();
		}
		case Dealer.TRENDING_VIEW_MOST_PROFITABLE:
		{
			if ( mode == ImpactModeEnum.PERCENTAGE_VAL )
			{
				return form.getMostProfitableReportGroupingsInOptimix();
			}
			else
			{
				return form.getMostProfitableReportGroupings();
			}
		}
		default:
		{
			return null;
		}
	}
}

void putTrendReportLengthInRequest( int preferenceSize, HttpServletRequest request )
{
	// KL - this doesn't really work since even if one
	// report has zero length, the others determine which length is
	// sent to the page
	SimpleFormIterator iterator = (SimpleFormIterator)request.getAttribute( PERFORMANCE_ANALYZER_FORECAST_ITERATOR );
	int reportLength = iterator.getOriginalSize();

	iterator = (SimpleFormIterator)request.getAttribute( PERFORMANCE_ANALYZER_TREND_ITERATOR );
	reportLength = Math.max( reportLength, iterator.getOriginalSize() );

	iterator = (SimpleFormIterator)request.getAttribute( PERFORMANCE_ANALYZER_DEFAULT_ITERATOR );
	reportLength = Math.max( reportLength, iterator.getOriginalSize() );

	reportLength = Math.min( reportLength, preferenceSize );

	request.setAttribute( "reportLength", new Integer( reportLength ) );
}

public void setReportPersist( ReportPersist reportPersist )
{
	this.reportPersist = reportPersist;
}

public void setReportActionHelper( ReportActionHelper reportActionHelper )
{
	this.reportActionHelper = reportActionHelper;
}

public void setUcbpPreferenceService( IUCBPPreferenceService ucbpPreferenceService )
{
	this.ucbpPreferenceService = ucbpPreferenceService;
}

}