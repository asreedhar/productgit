package com.firstlook.action.dealer.redistribution;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;
@SuppressWarnings("unused")
public class InGroupAppraisalsDisplayAction extends SecureBaseAction {

    private IAppraisalService appraisalService;
    
    @Override
    public ActionForward doIt(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)
            throws DatabaseException, ApplicationException {
        int businessUnitId = getFirstlookSessionFromRequest(request).getCurrentDealerId();
        int parentId = getDealerGroupService().retrieveByDealerId(businessUnitId).getBusinessUnitId();
        String vin = request.getParameter("vin");
        
        List results = appraisalService.retrieveInGroupAppraisals(businessUnitId,parentId,vin);
        
        SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
        
        request.setAttribute("appCount", results.size());
        request.setAttribute("appraisals", results);
        return mapping.findForward("success");
    }
    
    public void setAppraisalService( IAppraisalService appraisalService )
    {
        this.appraisalService = appraisalService;
    }
    
}
