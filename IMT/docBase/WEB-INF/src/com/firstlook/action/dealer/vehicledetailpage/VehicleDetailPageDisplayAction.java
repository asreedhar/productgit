package com.firstlook.action.dealer.vehicledetailpage;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.services.vehiclehistoryreport.AutoCheckException;
import biz.firstlook.commons.services.vehiclehistoryreport.AutoCheckService;
import biz.firstlook.commons.services.vehiclehistoryreport.CarfaxException;
import biz.firstlook.commons.services.vehiclehistoryreport.CarfaxService;
import biz.firstlook.commons.util.BasicAuthAdapter;
import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.services.mmr.MMRServiceClient;
import biz.firstlook.services.mmr.entity.MMRArea;
import biz.firstlook.services.mmr.entity.MMRTransactions;
import biz.firstlook.services.mmr.entity.MMRTrim;
import biz.firstlook.services.vehiclehistoryreport.carfax.VehicleEntityType;
import biz.firstlook.transact.persist.model.BookOutSource;
import biz.firstlook.transact.persist.model.Inventory;
import biz.firstlook.transact.persist.model.MMRVehicle;
import biz.firstlook.transact.persist.model.VehicleSale;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;
import biz.firstlook.transact.persist.persistence.IInventoryBookOutDAO;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.components.auction.AuctionTileForm;
import com.firstlook.action.components.vehicledetailpage.VehicleInventory;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.SessionHelper;
import com.firstlook.persistence.vehiclesale.VehicleSaleDAO;
import com.firstlook.service.inventory.VehicleInventoryService;
import com.firstlook.util.SaleUtility;

public class VehicleDetailPageDisplayAction extends SecureBaseAction
{

private static Log logger = LogFactory.getLog( VehicleDetailPageDisplayAction.class );

private DealerPreferenceDAO dealerPrefDAO;
private IInventoryBookOutDAO inventoryBookOutDAO;
private VehicleInventoryService vehicleInventoryService;
private IAppraisalService appraisalService;
private VehicleSaleDAO imt_vehicleSaleDAO;


private MMRArea[] mmrAreas = {};
private MMRTrim[] mmrTrims = {};
private MMRTransactions mmrTransactions;
private String mmrErrorMsg = "";
private MMRVehicle mmrVehicle;







public MMRTrim getMmrTrimSelected() {
	if (this.getSelectedMmrVehicle() != null) {
		MMRVehicle mmrVeh = this.getSelectedMmrVehicle();
		for (MMRTrim trim : mmrTrims) {
			if (trim.getTrimId().equals(mmrVeh.getMid())) {
				return trim;
			}
		}
	}
	return null;
}
public String getMmrErrorMsg() {
	return mmrErrorMsg;
}

public void setMmrErrorMsg(String mmrErrorMsg) {
	this.mmrErrorMsg = mmrErrorMsg;
}

public MMRVehicle getSelectedMmrVehicle() {
	return mmrVehicle;
}

public void setSelectedMmrVehicle(MMRVehicle mmrVehicle) {
	if (this.mmrVehicle != null) {
		if (mmrVehicle != null) {
			this.mmrVehicle.setMid(mmrVehicle.getMid());
			this.mmrVehicle.setRegionCode(mmrVehicle.getRegionCode());
		} else {
			this.mmrVehicle = null;
		}
	} else
		this.mmrVehicle = mmrVehicle;
}

public MMRArea[] getMmrAreas() {
	return mmrAreas;
}

public void setMmrAreas(MMRArea[] mmrAreas) {
	this.mmrAreas = mmrAreas;
}

public MMRTrim[] getMmrTrims() {
	return mmrTrims;
}

public void setMmrTrims(MMRTrim[] mmrTrims) {
	this.mmrTrims = mmrTrims;
}

public MMRTransactions getMmrTransactions() {
	return mmrTransactions;
}

public void setMmrTransactions(MMRTransactions mmrTransactions) {
	this.mmrTransactions = mmrTransactions;
}


public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	String forward = "success";
	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	Integer inventoryId = RequestHelper.getInt(request, "inventoryId");
	VehicleInventory vehicleInventory = vehicleInventoryService.retrieveVehicleInventory( currentDealerId, Integer.valueOf( inventoryId ) );
	//System.out.println( vehicleInventory.getVehicle().getVehicleId() );

	Dealer dealer = getImtDealerService().retrieveDealer( currentDealerId );

	String vin = vehicleInventory.getVehicle().getVin();
	Inventory inventory = vehicleInventory.getInventory();
	Integer appraisalDaysBack = dealer.getDealerPreference().getSearchAppraisalDaysBackThreshold();
	//search 2 weeks back from InventoryReceived date to find an appraisal linked to the inventory item.
	//ignore the dealer preference because this search preference is not correct in this usage context!
	//also, the preference should be AppraisalLookBackPeriod!!! -bf (April 24, 2009)
	if(appraisalDaysBack.intValue() < 14) { 
		appraisalDaysBack = 14;
	}
	Integer daysFilter = appraisalDaysBack + DateUtilFL.calculateNumberOfCalendarDays(new Date(), inventory.getInventoryReceivedDt());
	IAppraisal appraisal = getAppraisalService().findLatest(currentDealerId, vin, daysFilter);

	putAppraisalInfoInRequest( appraisal, vehicleInventory, request );
	StringBuffer appraisalLink = new StringBuffer();
	if ( appraisal == null )
	{
		// Create appraisal
		appraisalLink.append( "SearchStockNumberOrVinAction.go" );
		appraisalLink.append( "?override=tradeAnalyzer" );
		appraisalLink.append( "&vin=" ).append( vin );
		request.setAttribute( "appraisalButtonAction", "createAppraisal" );
		request.setAttribute( "appraisalLink", appraisalLink.toString() );
	}
	else
	{
		// view appraisal
		appraisalLink.append( "SearchStockNumberOrVinAction.go" );
		appraisalLink.append( "?override=tradeManager" );
		appraisalLink.append( "&appraisalId=" ).append( appraisal.getAppraisalId() );
		if( request.getParameter( "fromIMP" ) != null && request.getParameter( "fromIMP" ).equalsIgnoreCase( "true" ) )
		{
			appraisalLink.append( "&fromIMP=true" );
		}
		request.setAttribute( "appraisalButtonAction", "viewAppraisal" );
		request.setAttribute( "appraisalLink", appraisalLink.toString() );
	}

	if ( vehicleInventory != null )
	{

		boolean isActive = vehicleInventory.getInventory().isInventoryActive();

		boolean isPopup = RequestHelper.getBoolean( request, "isPopup" );
		putDealerFormInRequest( request );

		AuctionTileForm auctionForm = new AuctionTileForm();
		auctionForm.setMake( vehicleInventory.getVehicle().getMake() );
		auctionForm.setModel( vehicleInventory.getVehicle().getModel() );
		auctionForm.setMileage( vehicleInventory.getInventory().getMileageReceived() );
		auctionForm.setModelYear( vehicleInventory.getVehicle().getVehicleYear() );
		auctionForm.setVin( vehicleInventory.getVehicle().getVin() );

		List<Integer> guideBookIds = getGuideBookIds( dealer );
		
		SessionHelper.setAttribute( request, "auctionForm", auctionForm );
		SessionHelper.keepAttribute( request, "auctionForm" );
		request.setAttribute( "guideBookIds", guideBookIds);
		request.setAttribute( "inventoryId", new Integer( inventoryId ) );
		request.setAttribute( "inventoryMileage", vehicleInventory.getInventory().getMileageReceived() );
		request.setAttribute( "bookOutSourceId", BookOutSource.BOOK_OUT_SOURCE_VEHICLE_DETAIL );
		// If the inventory record is inactive, then the book out tile data will
		// display as read only
		request.setAttribute( "isActive", new Boolean( isActive ) );
		request.setAttribute( "isPopup", new Boolean( isPopup ) );
		request.setAttribute( "age", getAgeDescription( vehicleInventory, request ) );
		// add the current vehicle light
		logger.debug("currentVehicleLight=" + vehicleInventory.getInventory().getCurrentVehicleLight());
		request.setAttribute("currentVehicleLight", vehicleInventory.getInventory().getCurrentVehicleLight());
		
		Format formatter = new SimpleDateFormat( "MM/dd/yyyy" );
		request.setAttribute( "receivedDate", formatter.format( vehicleInventory.getInventory().getInventoryReceivedDt() ) );
		if ( vehicleInventory.getInventory().getTradeOrPurchase() == 1 )
		{
			request.setAttribute( "tradeOrPurchase", "Purchase" );
		}
		else
		{
			request.setAttribute( "tradeOrPurchase", "Trade" );
		}
		request.setAttribute( "vin", vehicleInventory.getVehicle().getVin() );
		request.setAttribute( "stockNumber", vehicleInventory.getInventory().getStockNumber() );
		Integer groupingDescId = vehicleInventory.getVehicle().getMakeModelGrouping().getGroupingDescriptionId();
		request.setAttribute( "groupingDescriptionId", groupingDescId );
	}
	// if the vehicle is not in dealer's inventory, then forward to the stock
	// number not found page
	else
	{
		putDealerFormInRequest( request );
		forward = "failure";
	}

	Member member = getMemberFromRequest( request );

	DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerId( currentDealerId );
	request.setAttribute( "isLithiaStore", dealerGroup.isLithiaStore());
	request.setAttribute("hasTransferPricing", dealerGroup.getDealerGroupPreference().isIncludeTransferPricing());

	try {
		Map<String,String> properties = CarfaxService.getInstance().getCarfaxReportProperties(
				currentDealerId,
				member.getLogin(),
				vin,
				VehicleEntityType.INVENTORY);
		for (Map.Entry<String, String> entry : properties.entrySet()) {
			request.setAttribute(entry.getKey(), entry.getValue());
		}
	} catch (CarfaxException ce) {
		request.setAttribute("hasCarfaxError", true);
		request.setAttribute("problem", ce.getResponseCode());
	}

	try {
		Map<String,String>	properties = AutoCheckService.getInstance().getAutoCheckReportProperties(
			currentDealerId,
			member.getLogin());
		for (Map.Entry<String, String> entry : properties.entrySet()) {
			request.setAttribute(entry.getKey(), entry.getValue());
		}
	} catch (AutoCheckException ace) {
		//prevent rest of page blowing up, what to do for autocheck error???
	}
	
	String basicAuthString = (String) request.getSession().getServletContext().getAttribute(BasicAuthAdapter.NAME_IN_SESSION);
	

	
	
	setMmrErrorMsg("");
	populateMMRAreas(basicAuthString);
	populateMMRTrims(basicAuthString,vin);
	
	
	request.setAttribute("mmrAreas", getMmrAreas());
	request.setAttribute("mmrTrims", getMmrTrims());
	
	request.setAttribute("inventoryformmr",inventory.getInventoryId() );
	request.setAttribute("mmrErrorMsg", getMmrErrorMsg());

	if(inventory.getMmrVehicle()!=null){
		SimpleDateFormat sf= new SimpleDateFormat("yyyy.MM.dd HH:mm");
		if(inventory.getMmrVehicle().getDateUpdated()!=null)
		{
			request.setAttribute("lastUpdatedDate", sf.format(inventory.getMmrVehicle().getDateUpdated()));
		}
		else
		{
			request.setAttribute("lastUpdatedDate", "");
		}
		request.setAttribute("mmrAverage", inventory.getMmrVehicle().getAveragePrice());
		request.setAttribute("selectedMmrTrim", inventory.getMmrVehicle().getMid());
		request.setAttribute("selectedMmrArea", inventory.getMmrVehicle().getRegionCode());
	
	}
	if(inventory.getMmrVehicle()==null)
	{
		request.setAttribute("isExclamation", "exclamation");
	}
	
	return mapping.findForward( forward );
}


public void populateMMRAreas(String basicAuthString) {
	if (mmrAreas.length < 1) {
		try {
			MMRServiceClient mmrServiceClient = new MMRServiceClient(
					basicAuthString);
			mmrAreas = mmrServiceClient.getMMRReference();
		} catch (Exception e) {
			// Exception already logged. Just keep whatever the last area
			// list was (or empty list).
		}
	}
}

public void populateMMRTrims(String basicAuthString,String vin) {
	try {
		MMRServiceClient mmrServiceClient = new MMRServiceClient(
				basicAuthString);
		MMRVehicle mmrVeh = getSelectedMmrVehicle();
		String area = mmrVeh == null ? "" : mmrVeh.getRegionCode();
		mmrTrims = mmrServiceClient.getMMRTrims(vin, area);

		if (mmrTrims.length == 1) {
			if (mmrVeh == null) {
				mmrVeh = new MMRVehicle(mmrTrims[0].getTrimId(), area);
			}
			mmrVeh.setMid(mmrTrims[0].getTrimId());
			// selectedMmrTrim = mmrTrims[0].getTrimId();
			setSelectedMmrVehicle(mmrVeh);
		}
	} catch (Exception e) {
		setMmrErrorMsg(e.getMessage());
		// Exception already logged. Just return whatever the last trim list
		// was (or empty list).
	}
}

public void populateMMRTransactions(String basicAuthString) {
	try {
		MMRVehicle mmrVeh = getSelectedMmrVehicle();
		MMRTrim mmrTrim = getMmrTrimSelected();
		if (mmrTrim != null) {
			String area = mmrVeh == null ? "" : mmrVeh.getRegionCode();
			MMRServiceClient mmrServiceClient = new MMRServiceClient(
					basicAuthString);
			mmrTransactions = mmrServiceClient.getMMRTransactions(mmrTrim
					.getTrimId(), mmrTrim.getYear(), area);
		}
	} catch (Exception e) {
		// Set the mmrTransactions back to null, since there aren't any.
		mmrTransactions = null;
		setMmrErrorMsg(e.getMessage());
		// Exception already logged. Just return whatever the last
		// transactions were (or empty list).
	}
}



private List<Integer> getGuideBookIds( Dealer dealer )
{
	List<Integer> guideBookIds = new ArrayList<Integer>();
	guideBookIds.add( dealer.getDealerPreference().getGuideBookId() );
	if ( (dealer.getDealerPreference().getGuideBook2Id() != null) && 
			(dealer.getDealerPreference().getGuideBook2Id().intValue() != 0))
	{
		guideBookIds.add( dealer.getDealerPreference().getGuideBook2Id() );
	}
	return guideBookIds;
}

private void putAppraisalInfoInRequest( IAppraisal appraisal, VehicleInventory vehicleInventory, HttpServletRequest request )
{
	if ( appraisal == null )
	{
		request.setAttribute( "appraisalValue", Double.valueOf( 0.0d ) );
	}
	else
	{
		Double appraisalValue = Double.valueOf( 0.0d );
		String appraisalInitials = "";
		if ( appraisal.getAppraisalValues() != null && !appraisal.getAppraisalValues().isEmpty()) {
			appraisalValue = new Double( appraisal.getLatestAppraisalValue().getValue() );
			appraisalInitials = appraisal.getLatestAppraisalValue().getAppraiserName();
		}
		request.setAttribute( "appraisalValue", appraisalValue );
		request.setAttribute( "appraisalInitials", appraisalInitials );
		request.setAttribute( "appraisalDate", appraisal.getDateCreated() );
		request.setAttribute( "estRecon", appraisal.getEstimatedReconditioningCost() );
		request.setAttribute( "appraisalNotes", appraisal.getConditionDescription() );
		request.setAttribute( "actualRecon", vehicleInventory.getInventory().getReconditionCost() );
	}
}

// Creates the string with format something like "XX Days Old"
// Sold/Inactive vehicles use daysToSale value and append "(Sold)"
private String getAgeDescription( VehicleInventory vehicleInventory, HttpServletRequest request )
{

	Inventory inventory = vehicleInventory.getInventory();
	Integer days = new Integer( (int)inventory.getDaysInInventoryLongValue() );
	String ageDescription = "Unable to determine days to sale.";
	if ( inventory.getInventoryActive() )
	{
		ageDescription = days.toString() + " Days Old";
	}
	// Inactive/Sold vehicles
	else
	{
		// Coming from the Deal Log we get 'daysToSale' passed thru.
		String daysToSale = request.getParameter( "daysToSale" );
		if ( daysToSale != null && !daysToSale.equals( "" ) )
		{
			ageDescription = daysToSale + " Days Old (Sold)";
		}
		// Otherwise we have to calculate it.
		else
		{
			// Can get a case where we have an InventoryId but no VehicleSale
			// record.
			// BillH is aware of this problem.
			VehicleSale vehicleSale = getImt_vehicleSaleDAO().findByPK( inventory.getInventoryId() );
			if ( vehicleSale == null )
				return ageDescription;

			daysToSale = SaleUtility.calculateDaysToSale( inventory.getInventoryReceivedDt(), vehicleSale.getDealDate() ) + "";

			ageDescription = daysToSale + " Days Old (Sold)";
		}
	}

	return ageDescription;
}

public DealerPreferenceDAO getDealerPrefDAO()
{
	return dealerPrefDAO;
}

public void setDealerPrefDAO( DealerPreferenceDAO dealerPrefDAO )
{
	this.dealerPrefDAO = dealerPrefDAO;
}

public IInventoryBookOutDAO getInventoryBookOutDAO()
{
	return inventoryBookOutDAO;
}

public void setInventoryBookOutDAO( IInventoryBookOutDAO inventoryBookOutDAO )
{
	this.inventoryBookOutDAO = inventoryBookOutDAO;
}

public VehicleInventoryService getVehicleInventoryService()
{
	return vehicleInventoryService;
}

public void setVehicleInventoryService( VehicleInventoryService vehicleInventoryService )
{
	this.vehicleInventoryService = vehicleInventoryService;
}

public IAppraisalService getAppraisalService()
{
	return appraisalService;
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

public VehicleSaleDAO getImt_vehicleSaleDAO()
{
	return imt_vehicleSaleDAO;
}

public void setImt_vehicleSaleDAO( VehicleSaleDAO imt_vehicleSaleDAO )
{
	this.imt_vehicleSaleDAO = imt_vehicleSaleDAO;
}

}
