package com.firstlook.action.dealer.buy;

import java.util.Collection;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.report.CustomIndicator;
import com.firstlook.service.inventory.InventoryService_Legacy;

public class TotalMarketplaceGuideDisplayAction extends BaseBuyingGuideDisplayAction
{
private InventoryService_Legacy inventoryService_Legacy; 

protected Collection getVehiclesToDisplay( Dealer dealer, CustomIndicator indicator ) throws DatabaseException, ApplicationException
{
    return inventoryService_Legacy.retrieveByDealerIdAndInventoryStatusAndAgingPolicy( dealer, getDealerGroupService(), getImtDealerService() );
}

public void setInventoryService_Legacy(InventoryService_Legacy inventoryService) {
	this.inventoryService_Legacy = inventoryService;
}


}