package com.firstlook.action.dealer.tools;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.comparator.SaleComparator;
import com.firstlook.comparator.service.BaseComparatorService;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.report.DealLogReportRetriever;
import com.firstlook.service.tools.DealLogService;
import com.firstlook.service.tools.GrossProfitTotals;
import com.firstlook.session.FirstlookSession;
import com.firstlook.util.PageBreakHelper;

public class DealLogDisplayAction extends SecureBaseAction
{

private DealLogReportRetriever dealLogReportRetriever; 
private String DEFAULT_ORDER = "dealDate";

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int currentDealerId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();
	Member member = getMemberFromRequest( request );
	int inventoryType = member.getInventoryType().getValue();
	DealLogService dealLogService = new DealLogService( currentDealerId, inventoryType);
	int weeks = dealLogService.determineWeeks( request.getParameter( "weeks" ) );
	request.setAttribute( "weeks", new Integer( weeks ) );

	String saleType = determineSaleType( request.getParameter( "saleType" ) );
	request.setAttribute( "saleType", saleType );

	BaseComparatorService baseComparatorService = new BaseComparatorService( DEFAULT_ORDER );
	String orderBy = baseComparatorService.retrieveOrderBy( request.getParameter( "orderBy" ) );

	request.setAttribute( "orderBy", orderBy );
	Date startDate = dealLogService.generateDateFromWeeks( weeks );
	List saleForms = null;
	if ( saleType.equalsIgnoreCase( DealLogService.SALETYPE_WHOLESALE ) )
	{
		saleForms = retrieveDeals( currentDealerId, startDate, orderBy, saleType, inventoryType );
		request.setAttribute( "wholeSales", saleForms );
	}
	else
	{
		saleForms = retrieveDeals( currentDealerId, startDate, orderBy, saleType, inventoryType );
		request.setAttribute( "retailSales", saleForms );
	}

	GrossProfitTotals grossProfitTotals = dealLogService.sumGrossProfit( saleForms );
	request.setAttribute( "totalFrontEndGrossProfit", new Double( grossProfitTotals.getTotalFrontEndGrossProfit() ) );
	request.setAttribute( "totalBackEndGrossProfit", new Double( grossProfitTotals.getTotalBackEndGrossProfit() ) );

	putDealerFormInRequest( request, 0 );
	Dealer currentDealer = getImtDealerService().retrieveDealer( currentDealerId );

	request.setAttribute( "nickname", currentDealer.getNickname() );
	request.setAttribute("showLogo", true);

	putPageBreakHelperInRequest( PageBreakHelper.DEAL_LOG, request, getMemberFromRequest( request ).getProgramType() );

	return mapping.findForward( "success" );
}

private String determineSaleType( String saleType )
{
	if ( saleType == null )
	{
		saleType = DealLogService.SALETYPE_RETAIL;
	}
	return saleType;
}

private List retrieveDeals( int dealerId, Date startDate, String orderBy, String saleType, int inventoryType ) throws ApplicationException
{
	List saleForms = dealLogReportRetriever.retrieveDealLogLineItems( dealerId, new Timestamp( startDate.getTime() ), saleType.substring( 0, 1 ), inventoryType );// dealLogService.retrieveWholeSaleDeals(weeks);
	Collections.sort( saleForms, new SaleComparator( orderBy ) );
	return saleForms;
}

public void setDealLogReportRetriever(
		DealLogReportRetriever dealLogReportRetriever) {
	this.dealLogReportRetriever = dealLogReportRetriever;
}

}