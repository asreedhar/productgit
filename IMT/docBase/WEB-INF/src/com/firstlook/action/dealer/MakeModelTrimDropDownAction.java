package com.firstlook.action.dealer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.services.vehicleCatalog.IVehicleCatalogService;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogKey;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogServiceException;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;

public class MakeModelTrimDropDownAction extends SecureBaseAction
{

private static final Logger logger = Logger.getLogger( MakeModelTrimDropDownAction.class );

private IVehicleCatalogService vehicleCatalogService;


/**
 * Make is the same whether we are doing an appraisal or looking up performance info. 
 * 
 * Model refers to the Analytic definition of Model : LINE + TRIM. E.g) TAHOE LS, TAHOE LT. 
 * It is the same value whether we are doing an appraisal or looking up performance info.
 * 
 * Trim is different. For Performance info, Trim is aka Series. For example: Base or Sport.
 * 
 * For Appraisal purposes, trim is actually the 'CatalogKey', or the garaunteed 
 * Unique Key used with Make and Model to identify a complete vehicle description in the Catalog. 
 * This unqiuely identified value is then what we write to currently to the Vehicle Table 
 * in GuideBookDetailSubmitAction: populateCatalogInfoWithoutVin().
 * 
 */
public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	String forward = "makeSuccess";
	boolean isAppraisal = new Boolean((String)request.getParameter( "forAppraisal" )).booleanValue();
	String dropDownToPopulate = (String)request.getParameter( "dropDown" );
	if ( dropDownToPopulate.equalsIgnoreCase( "make" ) )
	{
		loadMakes( request );
	}
	else if ( dropDownToPopulate.equalsIgnoreCase( "model" ) )
	{
		forward = loadModels( request );
	}
	else if ( dropDownToPopulate.equalsIgnoreCase( "trim" ) )
	{
		forward = loadTrims( request, isAppraisal );
	}
	request.setAttribute( "isAppraisal", isAppraisal );
	return mapping.findForward( forward );
}

private String loadTrims( HttpServletRequest request, boolean isAppraisal )
{
	String result = "failure";
	String selectedMake = (String)request.getParameter( "make");
	String selectedModel = (String)request.getParameter( "model");
	String selectedYear = (String)request.getParameter( "year");

	List<VDP_GuideBookVehicle> trims = new ArrayList< VDP_GuideBookVehicle >();
	
	List<String> styleKeysList = null;
	try {
		if (!isAppraisal) {
			// FOR VEHICLE ANALYZER 
			// use analytics data (Derived from catalog data. Can query analytics tables.) 
			styleKeysList =  vehicleCatalogService.retrieveAllModelTrims(selectedMake, selectedModel); 
			if (styleKeysList != null) {
				Collections.sort( styleKeysList );
				Iterator<String> styleKeys = styleKeysList.iterator();
				// populate request with trims
				while (styleKeys.hasNext()) {
					String styleKey = styleKeys.next(); 
					VDP_GuideBookVehicle vehicle = new VDP_GuideBookVehicle();
					vehicle.setKey( styleKey );
					vehicle.setTrim( styleKey );
					trims.add( vehicle );
				}
			}
		} else {
			// raw catalog data (Can be used to create a Vehicle row.)
			// rather than classic 'trim' these are CatalogKeys, the same values on the second page of TA. 
			List<VehicleCatalogKey> catalogKeys = vehicleCatalogService.retrieveVehicleCatalogMultiEntry( selectedMake, selectedModel, new Integer(selectedYear) ).getCatalogKeys();
			if (catalogKeys != null) {
				for(VehicleCatalogKey key : catalogKeys) {
					VDP_GuideBookVehicle vehicle = new VDP_GuideBookVehicle();
					vehicle.setKey( key.getVehicleCatalogId().toString());
					vehicle.setTrim( key.getVehicleCatalogDescription() );
					trims.add( vehicle );
				}
			}
		}
		request.setAttribute( "trims", trims );
		result = "trimSuccess";
	} catch (Exception e) {
		logger.error("Error generating styleKeyList");
		request.setAttribute( "trims", new ArrayList<VDP_GuideBookVehicle>());
	} 
	
	return result;
}

private String loadModels( HttpServletRequest request )
{
	String selectedMake = (String)request.getParameter( "make" );
	String selectedYear = (String)request.getParameter( "year" );
	List<VDP_GuideBookVehicle> models = new ArrayList< VDP_GuideBookVehicle >();
	
	try {
		List<String> modelsList = vehicleCatalogService.retrieveAllModels(selectedMake, selectedYear); 
		Collections.sort( modelsList );
		Iterator<String> modelsIter = modelsList.iterator();
		while (modelsIter.hasNext()) {
			String model = modelsIter.next();
			VDP_GuideBookVehicle vehicle = new VDP_GuideBookVehicle();
			vehicle.setModel( model );
			vehicle.setModelId( model );
			models.add( vehicle );
		}
	} catch (VehicleCatalogServiceException e) {
		logger.error("Error retrieving all models from Vehicle Catalog. Returning an empty list.");
	}
	
	request.setAttribute( "models", models );
	return "modelSuccess";
}

private void loadMakes( HttpServletRequest request )
{
	try {
		//Sort the list of makes.
		List<String> makes = vehicleCatalogService.retrieveAllMakes();
		
		//for compatibility of the shared jsp, and since we don't really have an oo object...
		List<Map<String, String>> uiModel = new ArrayList<Map<String,String>>();
		if(makes != null) {
			for(String make : makes) {
				Map<String, String> uiObj = new HashMap<String, String>();
				uiObj.put("makeId", make);
				uiObj.put("make", make);
				uiModel.add(uiObj);
			}
		}
		request.setAttribute( "makes", uiModel );
	} catch (VehicleCatalogServiceException e) {
		logger.error("Error retrieving all makes from Vehicle Catalog. Returning an empty list.");
		request.setAttribute( "makes", Collections.<VDP_GuideBookVehicle>emptyList() );
	}
}

public void setVehicleCatalogService( IVehicleCatalogService vehicleCatalogService )
{
	this.vehicleCatalogService = vehicleCatalogService;
}

}
