package com.firstlook.action.dealer.tools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.module.vehicle.description.old.BookOutDatasetInfo;
import biz.firstlook.module.vehicle.description.old.BookOutTypeEnum;
import biz.firstlook.module.vehicle.description.old.GBException;
import biz.firstlook.module.vehicle.description.old.GuideBookInput;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.JobTitle;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;
import biz.firstlook.transact.persist.person.IPerson;
import biz.firstlook.transact.persist.person.PositionType;
import biz.firstlook.transact.persist.service.appraisal.AppraisalValue;
import biz.firstlook.transact.persist.service.appraisal.Customer;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.StagingBookOut;
import biz.firstlook.transact.persist.service.appraisal.StagingBookOutOption;

import com.firstlook.data.DatabaseException;
import com.firstlook.display.tradeanalyzer.DisplayGuideBook;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.TradeAnalyzerForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.exception.ConnectionException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.SessionHelper;
import com.firstlook.service.bookout.BookOutService;
import com.firstlook.service.bookout.bean.SelectedVehicleAndOptionsBookOutState;
import com.firstlook.session.FirstlookSession;

public class GuideBookStepTwoSubmitAction extends AbstractTradeAnalyzerFormAction {

	protected static Logger logger = Logger.getLogger(GuideBookStepTwoSubmitAction.class);

	private DealerPreferenceDAO dealerPrefDAO;

	private BookOutService bookOutService;

	protected ActionForward analyzeIt(ActionMapping mapping, TradeAnalyzerForm tradeAnalyzerForm,	HttpServletRequest request, HttpServletResponse response, Dealer dealer)
		throws DatabaseException, ApplicationException
	{
		try {
			int currentDealerId = ((FirstlookSession) request.getSession().getAttribute(FirstlookSession.FIRSTLOOK_SESSION)).getCurrentDealerId();
			DealerPreference dealerPref = dealer.getDealerPreference();

			ArrayList<Integer> guideBookIds = new ArrayList<Integer>();
			ArrayList<String> selectedGuideBookVehicleKeys = new ArrayList<String>();
			ArrayList<StagingBookOut> pendingBookOuts = null;

			IAppraisal pendingAppraisal = tradeAnalyzerForm.getPendingAppraisal();
			if (pendingAppraisal != null) {
				
				populateDisplayGuideBookWithPendingAppraisalData(pendingAppraisal, tradeAnalyzerForm);
				
				// If we have staging bookouts we have a FULL (aka Wireless) Pending Appraisal
				if (pendingAppraisal.getStagingBookOuts() != null && !pendingAppraisal.getStagingBookOuts().isEmpty()) {
					pendingBookOuts = new ArrayList<StagingBookOut>();
					for (StagingBookOut pendingBookout : pendingAppraisal.getStagingBookOuts()) {
						Integer thirdPartyId = pendingBookout.getThirdPartyDataProvider().getThirdPartyId();
						guideBookIds.add(thirdPartyId);
						if (request.getParameter("gbid") != null && Integer.parseInt(request.getParameter("gbid")) == thirdPartyId) {
							selectedGuideBookVehicleKeys.add(request.getParameter("selectedKey"));
						} else {
							selectedGuideBookVehicleKeys.add(pendingBookout.getThirdPartyVehicleCode());
						}
						pendingBookOuts.add(pendingBookout);
					}
					// flag to show wirlessTile on TA page - we don't need it for CRM (Small) Appraisals
					request.setAttribute("isWirelessAppraisal", true);
				} else {
					request.setAttribute("isCRMAppraisal", true);
				}
				
				if (request.getParameter("selectedKey") != null){
					// This is for reload of Stage 2 when an option is selected from a GBDrop down
					// and we only have a CRM appraisal with only basic info
					// We need to process same as usual
					guideBookIds.clear(); // we are only updating the one GB
					guideBookIds.add(new Integer(RequestHelper.getInt(request, "gbid")));
					selectedGuideBookVehicleKeys = getDropdownSelections(request);
				}
			} else {
				guideBookIds.add(new Integer(RequestHelper.getInt(request, "gbid")));
				selectedGuideBookVehicleKeys = getDropdownSelections(request);
			}

			Member member = getMemberFromRequest(request);
			GuideBookInput input = populateInput(currentDealerId, tradeAnalyzerForm, dealerPref, dealer, member);
			

			// this work by keeping the values in the three arrays: guideBookIds, selectedGuideBookVehicleKeys, pendingBookOuts in order
			for (int i = 0; i < guideBookIds.size(); i++) {
				Integer guideBookId = (Integer) guideBookIds.get(i);
				if (guideBookId == null)
					continue;

				String ddSelection = (String) selectedGuideBookVehicleKeys.get(i);
				if (ddSelection == null)
					continue;

				DisplayGuideBook guideBook = tradeAnalyzerForm.findDisplayGuideBookByGuideBookId(guideBookId.intValue());
				if( guideBook == null ) {
					String msg = String.format( "TradeAnalyzerForm returned a null DisplayGuideBook for guideBookId: %s, vin: %s, with displayGuideBooks %s" , guideBookId, tradeAnalyzerForm.getVin(), tradeAnalyzerForm.getDisplayGuideBooks() );
					logger.error( msg );
					//this is done in GuideBookDetailSubmitAciton to "throw" the error to display an error tile and not DPP completely.
					guideBook = new DisplayGuideBook();
					guideBook.setSuccess( false );
					guideBook.setSuccessMessage( "Error trying to select the trim; Please restart this Trade Analyzer run from the Home Page." );
					guideBook.setValidVin( true );
					guideBook.setGuideBookId( guideBookId );
					guideBook.setGuideBookName( ThirdPartyDataProvider.getThirdPartyDataProviderDescription( guideBookId ) );
					tradeAnalyzerForm.addDisplayGuideBook( guideBook );
					continue; //errored out, continue to next iteration.
				}

				guideBook.setSelectedVehicleKey(ddSelection);

				StagingBookOut pendingBookOut = null;
				if (pendingBookOuts != null) {
					pendingBookOut = pendingBookOuts.get(i);
				}
				
				VDP_GuideBookVehicle guideBookVehicle = guideBook.determineSelectedVehicle(ddSelection);

				// Populate options if something other than 1st default 0
				// dropdown is selected.
				if (!guideBook.getSelectedVehicleKey().equals("0")) {
					populateOptionInfo(guideBookId.intValue(), input, guideBook, guideBookVehicle, pendingBookOut);
				}

				// clear the old
				guideBook.setWirelessMessages(new ArrayList<LabelValueBean>());
				// set wireless Option messages
				if (pendingBookOut != null && pendingBookOut.getThirdPartyVehicleCode().equalsIgnoreCase(ddSelection))
					setWirelesslessOptionMessages(guideBook, pendingBookOut);

			}

			// save changes to people
			Collection<IPerson> people = getPersonService().saveOrUpdate(request.getParameter("manage.changes"), currentDealerId);

			// track selection of new people
			tradeAnalyzerForm.setDealTrackSalesPersonID(people);
			request.setAttribute("people", people);
			
			if(!request.getParameter("BuyerList").equals("")){
			tradeAnalyzerForm.setBuyerId(Integer.parseInt(request.getParameter("BuyerList")));  
			}
			

			SessionHelper.keepAttribute(request, "people");
			SessionHelper.setAttribute(request, "tradeAnalyzerForm", tradeAnalyzerForm);
			SessionHelper.keepAttribute(request, "tradeAnalyzerForm");

			DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerId( getFirstlookSessionFromRequest( request ).getCurrentDealerId() );
			
			List<Member> contactsToEmailList = null;
			Integer jobTitleToEmail = null;
			if (dealerGroup.isLithiaStore()) {
				jobTitleToEmail = JobTitle.LITHIA_CAR_CENTER_ID;
			} else {
				jobTitleToEmail =JobTitle.UCM_ID;
			}
			
			contactsToEmailList = getMemberService().getMembersByBusinessUnitAndJobTitle( currentDealerId, jobTitleToEmail );
			
			request.setAttribute( "defaultContactId", dealerPref.getDefaultLithiaCarCenterMemberId() );
			request.setAttribute( "contactsToEmailList", contactsToEmailList );
			request.setAttribute( "isLithia", dealerGroup.isLithiaStore() );
		}
		catch (ConnectionException ce) {
			putErrorMessageInRequest(request, ce.getMessage());
		}
		
		ActionForward forward = mapping.findForward("success");

		//Appraiser Name DropDown
		List<IPerson> appraisers = new ArrayList<IPerson>();
        PositionType appraiserType = PositionType.valueFromPositionString("Appraiser");
        if (appraiserType != null) {
            appraisers = (List<IPerson>) getPersonService().getBusinessUnitPeopleByPosition(getFirstlookSessionFromRequest( request ).getCurrentDealerId(), appraiserType);
            
        }
        if(tradeAnalyzerForm.getBuyerId()!=0)
        	tradeAnalyzerForm.setBuyerName(getPersonService().getPerson(tradeAnalyzerForm.getBuyerId()).getFullName());
        tradeAnalyzerForm.setAppraisers(appraisers);
    	request.setAttribute( "appraisers", appraisers );
    	tradeAnalyzerForm.setRequireNameOnAppraisals(dealer.getDealerPreference().getRequireNameOnAppraisals());
    	
    	return forward;
	}

	
	/**
	 * Populates valid values from the pending appraisal into the tradeanalyzer form so it gets
	 * persisted when the appraisal is saved.
	 * 
	 * @param pendingAppraisal
	 * @param tradeAnalyzerForm
	 */
	private void populateDisplayGuideBookWithPendingAppraisalData(IAppraisal pendingAppraisal, TradeAnalyzerForm tradeAnalyzerForm) {
		populateTradeAnalyzerForm(pendingAppraisal, tradeAnalyzerForm);
		populateCustomerInformation(tradeAnalyzerForm, pendingAppraisal);
		deleteInvalidBookOutsFromPendingAppraisal(tradeAnalyzerForm);

		// appraisal value and appraiser name
		if (pendingAppraisal.getAppraisalValues() != null
				&& !pendingAppraisal.getAppraisalValues().isEmpty()) {
			AppraisalValue appraisalValue = pendingAppraisal.getAppraisalValues().get(0);
			tradeAnalyzerForm.setAppraisalValue(appraisalValue.getValue());
			tradeAnalyzerForm.setAppraiserName(appraisalValue.getAppraiserName());
		}

		// recon description
		if (pendingAppraisal.getConditionDescription() != null) {
			tradeAnalyzerForm.setConditionDisclosure(pendingAppraisal.getConditionDescription());
		}

		// recon cost
		if (pendingAppraisal.getEstimatedReconditioningCost() != null) {
			tradeAnalyzerForm.setReconditioningCost(pendingAppraisal.getEstimatedReconditioningCost());
		}
		
		// target gross profit
		if (pendingAppraisal.getTargetGrossProfit() != null) {
			tradeAnalyzerForm.setTargetGrossProfit(pendingAppraisal.getTargetGrossProfit());
		}
		
		// deal type
		if (pendingAppraisal.getDealTrackNewOrUsed() != null) {
			tradeAnalyzerForm.setDealTrackNewOrUsed(pendingAppraisal.getDealTrackNewOrUsed().ordinal());
		}
		
	}

	/**
	 * Determines if we can use the pendingAppraisals bookout data. Checks to see that the VehicleKeys
	 * for the StagingBookOuts in the pending Appraisal indeed match GuideBook keys for this VIN.
	 * 
	 * If the wireless partner is "on the same page as us", i.e. booking out with the same data, this
	 * should almost always return true. It will only NOT if the pending appraisal has vehicleKeys for
	 * books which do not match vehicle keys for that VIN according to the books. In the case that
	 * there is no match, that staging bookout is removed from the pending appraisal.
	 * 
	 * @param tradeAnalyzerForm
	 * @return
	 */
	private void deleteInvalidBookOutsFromPendingAppraisal(TradeAnalyzerForm tradeAnalyzerForm) {
		Collection<DisplayGuideBook> guideBooks = tradeAnalyzerForm.getDisplayGuideBooks();
		Set<StagingBookOut> stagingBookOuts = tradeAnalyzerForm.getPendingAppraisal().getStagingBookOuts();
		for (StagingBookOut stagingBookOut : stagingBookOuts) {
			DisplayGuideBook matchedGuideBook = getGuideBookWithMatchingVehicleKey(guideBooks, stagingBookOut);
			if (matchedGuideBook == null) {
				// set error display messages
				DisplayGuideBook guideBook = getGuideBookForStagingBookOut(guideBooks, stagingBookOut);
				if (guideBook != null) {
					for (StagingBookOutOption invalidOption : stagingBookOut.getOptions()) {
						guideBook.getWirelessMessages().add(new LabelValueBean(invalidOption.getOptionName(), "0"));
					}
				}
				// remove staging bookout, because we can't use it
				tradeAnalyzerForm.getPendingAppraisal().removeStagingBookOut(stagingBookOut.getStagingBookOutId());
			}
		}
	}

	/**
	 * Returns the DisplayGuideBook that contains a DiaplayGuideBookVehicle matching the vehicleKey in
	 * the stagingBookout. If none of the DisplayGuideBooks contain a match, null is returned.
	 * 
	 * @param guideBooks
	 * @param stagingBookOut
	 * @return
	 */
	private DisplayGuideBook getGuideBookWithMatchingVehicleKey(Collection<DisplayGuideBook> guideBooks, StagingBookOut stagingBookOut) {
		DisplayGuideBook guideBook = getGuideBookForStagingBookOut(guideBooks, stagingBookOut);
		if (guideBook != null) {
			// compare each vehicleKey in the displayGuideBook with this stagingBooksOut's vehicle key
			Object[] guideBookVehicles = (Object[]) guideBook.getDisplayGuideBookVehicles();
			for (Object gbVehicleObj : guideBookVehicles) {
				VDP_GuideBookVehicle gbVehicle = (VDP_GuideBookVehicle) gbVehicleObj;
				// if there is a match - return the GuideBook
				if (stagingBookOut.getThirdPartyVehicleCode().equalsIgnoreCase(gbVehicle.getKey())) {
					return guideBook;
				}
			}
		}
		return null;
	}

	/**
	 * Returns the displayGuideBook that is for the same book as the one in the staging bookout.
	 * Reutrns null otherwise.
	 * 
	 * @param guideBooks
	 * @param stagingBookOut
	 * @return
	 */
	private DisplayGuideBook getGuideBookForStagingBookOut(Collection<DisplayGuideBook> guideBooks, StagingBookOut stagingBookOut) {
		for (DisplayGuideBook guideBook : guideBooks) {
			// if the display guide book is for the same book as the pending appraisal
			if (stagingBookOut.getThirdPartyDataProvider().getThirdPartyId().intValue() == guideBook.getGuideBookId().intValue()) {
				// compare each vehicleKey in the displayGuideBook withe this stagingBooksOut's vehicle key
				return guideBook;
			}
		}
		return null;
	}

	/**
	 * Populates customer info on TAForm with the values on the pending Appraisal.
	 * 
	 * @param tradeAnalyzerForm
	 * @param pendingAppraisal
	 */
	private void populateCustomerInformation(TradeAnalyzerForm tradeAnalyzerForm, IAppraisal pendingAppraisal) {

		Customer customer = pendingAppraisal.getCustomer();
		if (customer != null) {
			// Customer's PK is appraisalId. Customer = AppraisalCustomer
			tradeAnalyzerForm.setCustomerId(pendingAppraisal.getAppraisalId());
			tradeAnalyzerForm.setCustomerFirstName(customer.getFirstName());
			tradeAnalyzerForm.setCustomerLastName(customer.getLastName());
			tradeAnalyzerForm.setCustomerGender(customer.getGender());
			tradeAnalyzerForm.setCustomerPhoneNumber(customer.getPhoneNumber());
			tradeAnalyzerForm.setCustomerEmail(customer.getEmail());
		}
	}

	/**
	 * Iterates over the stagingBookOut options and the options now set on the DisplayGuideBook. The
	 * wirelessMessage bean on the displayGuideBook is set and communicates which options from the
	 * pending appraisal were successfully matched and those which were not.
	 * 
	 * @param guideBook
	 * @param pendingBookOut
	 */
	private void setWirelesslessOptionMessages(DisplayGuideBook guideBook, StagingBookOut pendingBookOut) {
		for (StagingBookOutOption pendingOption : pendingBookOut.getOptions()) {
			boolean matched = false;
			boolean ignore = false; // if true - not selected on pending and not select on GB
			if (pendingOption.getIsSelected()) {
				// set on pending and set on guidebook - green
				if (optionIsSelected(guideBook, pendingOption.getOptionKey())) {
					matched = true;
				}
			} else {
				// Set by book by default 
				// not selected on pending
				// should not be set on guide book and we should mark green
				Iterator allGuideBookOptions = guideBook.getDisplayGuideBookOptions().iterator();
				while (allGuideBookOptions.hasNext()) {  
					ThirdPartyVehicleOption gbOption = (ThirdPartyVehicleOption)allGuideBookOptions.next();
					if (pendingOption.getOptionKey().equalsIgnoreCase(gbOption.getOptionKey())) {
						if (gbOption.isStatus()) { // set by default
							if (!optionIsSelected(guideBook, pendingOption.getOptionKey())) { // not selected here - correct
								matched = true;
								break;
							}
						} else {
							// not set on pending
							// not set by default
							ignore = true;
							break;
						}
					}
				}
			}
			if (!ignore) {
				if (!matched) {
					guideBook.getWirelessMessages().add(new LabelValueBean(pendingOption.getOptionName(), "0"));
				} else {
					guideBook.getWirelessMessages().add(new LabelValueBean(pendingOption.getOptionName(), "1"));
				}
			}
		}
	}

	private boolean optionIsSelected(DisplayGuideBook guideBook, String optionKey) {
		boolean selected = false;
		for (String selectedOption : guideBook.getSelectedEquipmentOptionKeys()) {
			if (optionKey.equalsIgnoreCase(selectedOption)) {
				// in the previous method: populateOptionInfo both default options and SET Pending Options have
				// been set on the guideBook 
				selected = true;
				break;
			}
		}
		return selected;
	}

	// Only 1 selection for full desktop version.
	// Up to 2 for mobile device version
	private ArrayList<String> getDropdownSelections(HttpServletRequest request) {
		ArrayList<String> list = new ArrayList<String>();

		String key = (String) request.getParameter("selectedKey");
		if (key != null)
			list.add(key);

		return list;
	}

	private void populateOptionInfo(int seriesDropDownGuideBookId, GuideBookInput input,  DisplayGuideBook guideBook, VDP_GuideBookVehicle guideBookVehicle, StagingBookOut pendingBookout) throws ApplicationException {
		SelectedVehicleAndOptionsBookOutState state;
		try {
		if (guideBook.isStoredInfo()) {
			VDP_GuideBookVehicle vehicle = guideBook.determineSelectedVehicle(guideBook.getSelectedVehicleKey());
			if (vehicle.getGuideBookOptions() != null && !vehicle.getGuideBookOptions().isEmpty()) {
				state = new SelectedVehicleAndOptionsBookOutState();
				state.setSelectedVehicleKey(guideBook.getSelectedVehicleKey());
				state.setThirdPartyVehicleOptions(vehicle.getGuideBookOptions());
				state.setVehicleYear(vehicle.getYear());
			} else {
				state = getBookOutService().lookUpOptionsForSingleVehicle(input,  new Integer(seriesDropDownGuideBookId), guideBookVehicle, new BookOutDatasetInfo( BookOutTypeEnum.APPRAISAL, input.getBusinessUnitId()) );
			}
		} else {
			state = getBookOutService().lookUpOptionsForSingleVehicle(input,  new Integer(seriesDropDownGuideBookId), guideBookVehicle, new BookOutDatasetInfo( BookOutTypeEnum.APPRAISAL, input.getBusinessUnitId()));
		}
		state.setPendingBookOut(pendingBookout);
		state.transformBookOutToDisplayData(guideBook);
		} catch( GBException gbe ) 
		{
			throw new ApplicationException( gbe );
		}
	}

	private GuideBookInput populateInput(int currentDealerId, TradeAnalyzerForm tradeAnalyzerForm, DealerPreference dealerPref, Dealer dealer, Member member) {
		GuideBookInput input = new GuideBookInput();
		input.setVin(tradeAnalyzerForm.getVin());
		input.setBusinessUnitId(currentDealerId);
		input.setMileage(tradeAnalyzerForm.getMileage());
		input.setState(dealer.getState());
		input.setMemberId(member.getMemberId());
		input.setNadaRegionCode(dealerPref.getNadaRegionCode());
		return input;
	}

	private void putErrorMessageInRequest(HttpServletRequest request, String error) {
		request.setAttribute("GuideBookError1", error);
		request.setAttribute("GuideBookOptionsError", PropertyLoader.getProperty("firstlook.errors.GuideBookOptionsError"));
	}

	public DealerPreferenceDAO getDealerPrefDAO() {
		return dealerPrefDAO;
	}

	public void setDealerPrefDAO(DealerPreferenceDAO dealerPrefDAO) {
		this.dealerPrefDAO = dealerPrefDAO;
	}

	public BookOutService getBookOutService() {
		return bookOutService;
	}

	public void setBookOutService(BookOutService bookOutService) {
		this.bookOutService = bookOutService;
	}
}
