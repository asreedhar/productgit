package com.firstlook.action.dealer.tools;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.BasicAuthAdapter;
import biz.firstlook.services.mmr.entity.MMRArea;
import biz.firstlook.services.mmr.entity.MMRTransactions;
import biz.firstlook.services.mmr.entity.MMRTrim;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.form.TradeAnalyzerForm;
import com.firstlook.exception.ApplicationException;
import com.google.gson.Gson;


public class MMRTransactionsOnStep3Action extends AbstractTradeAnalyzerFormAction
{
	
private static final Logger logger = Logger.getLogger( MMRTransactionsOnStep3Action.class );

private IAppraisalService appraisalService;

public IAppraisalService getAppraisalService()
{
	return appraisalService;
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}	

public static Logger getLogger() {
	return logger;
}



protected ActionForward analyzeIt(ActionMapping mapping, TradeAnalyzerForm tradeAnalyzerForm,	HttpServletRequest request, HttpServletResponse response, Dealer dealer)
	throws DatabaseException, ApplicationException
{
	
	
	String basicAuthString = (String) request.getSession().getServletContext().getAttribute(BasicAuthAdapter.NAME_IN_SESSION);
	
	IAppraisal appraisal= appraisalService.findBy(Integer.parseInt(request.getParameter("appraisalId")));
	
	populateTradeAnalyzerForm(appraisal, tradeAnalyzerForm);
	if(tradeAnalyzerForm.getVin()==null&& appraisal!=null && appraisal.getVehicle()!=null)
		tradeAnalyzerForm.setVin(appraisal.getVehicle().getVin());
	tradeAnalyzerForm.populateMMRAreas(basicAuthString);
	tradeAnalyzerForm.populateMMRTrims(basicAuthString);
	tradeAnalyzerForm.populateMMRTransactions(basicAuthString);
	
	
	response.setContentType("application/json");
	try {
		
		Gson gson= new Gson();
		MMRTransactionsResponse resp= new MMRTransactionsResponse();
		
		resp.setMmrAreas(tradeAnalyzerForm.getMmrAreas());
		resp.setMmrTransactions(tradeAnalyzerForm.getMmrTransactions());
		resp.setMmrTrims(tradeAnalyzerForm.getMmrTrims());
		resp.setSelectedMMRTrim(tradeAnalyzerForm.getMmrTrimSelected());
		
		response.getWriter().write(gson.toJson(resp));
	
	} catch (IOException e) {

		e.printStackTrace();
	}
	
	ActionForward result = mapping.findForward( "success" ); 

	return result; 
}

class MMRTransactionsResponse{
	
	private MMRArea[] mmrAreas = {};
	private MMRTrim[] mmrTrims = {};
	private MMRTransactions mmrTransactions;
	private MMRTrim selectedMMRTrim;
	
	
	public MMRTrim getSelectedMMRTrim() {
		return selectedMMRTrim;
	}
	public void setSelectedMMRTrim(MMRTrim selectedMMRTrim) {
		this.selectedMMRTrim = selectedMMRTrim;
	}
	public MMRArea[] getMmrAreas() {
		return mmrAreas;
	}
	public void setMmrAreas(MMRArea[] mmrAreas) {
		this.mmrAreas = mmrAreas;
	}
	public MMRTrim[] getMmrTrims() {
		return mmrTrims;
	}
	public void setMmrTrims(MMRTrim[] mmrTrims) {
		this.mmrTrims = mmrTrims;
	}
	public MMRTransactions getMmrTransactions() {
		return mmrTransactions;
	}
	public void setMmrTransactions(MMRTransactions mmrTransactions) {
		this.mmrTransactions = mmrTransactions;
	}
	
	
}

}