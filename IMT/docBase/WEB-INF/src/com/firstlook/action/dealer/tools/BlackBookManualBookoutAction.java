package com.firstlook.action.dealer.tools;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import biz.firstlook.module.vehicle.description.old.BookOutDatasetInfo;
import biz.firstlook.module.vehicle.description.old.BookOutError;
import biz.firstlook.module.vehicle.description.old.BookOutTypeEnum;
import biz.firstlook.module.vehicle.description.old.GBException;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.model.BookOutValueType;
import biz.firstlook.transact.persist.service.ThirdPartyVehicleOptionService;


import com.firstlook.display.tradeanalyzer.DisplayGuideBook;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.thirdparty.book.blackbook.BlackBookBookoutService;

public class BlackBookManualBookoutAction extends AbstractManualBookOutAction
{

private BlackBookBookoutService blackBookBookoutService;

protected String processManualRequest(HttpServletRequest request, Dealer currentDealer, String selected) throws ApplicationException {
	int businessUnitId= currentDealer.getBusinessUnitId();
	String forward = loadYears(request, businessUnitId);
	if ( selected.equalsIgnoreCase( "make" ) )
	{
		forward = loadMakes( request, businessUnitId );
	}
	else if ( selected.equalsIgnoreCase( "model" ) )
	{
		forward = loadModels( request, businessUnitId );
	}
	else if ( selected.equalsIgnoreCase( "trim" ) )
	{
		// trim = series for BlackBook
		forward = loadSeries( request, businessUnitId );
	}
	else if ( selected.equalsIgnoreCase( "style" ) )
	{
		forward = loadStyles( request, businessUnitId );
	}
	else if ( selected.equalsIgnoreCase( "getOptions" ) )
	{
		try{
			forward = loadOptions( request, businessUnitId, currentDealer.getState() );
		}
		catch (GBException gbe) {
			throw new ApplicationException(gbe);
		}
	}
	else if ( selected.equalsIgnoreCase( "getValues" ) )
	{
		forward = loadValues( request, businessUnitId, currentDealer.getState() );
	}
	else if ( selected.equalsIgnoreCase( "print" ) ) {
		loadValues( request, businessUnitId, currentDealer.getState() );
		forward = "print";
	} 
	return forward;
}


@Override
protected String getGuideBookName() {
	return "BlackBook";
}

private static BookOutDatasetInfo getBookOutDatasetInfo(Integer businessUnitId) {
	return new BookOutDatasetInfo( BookOutTypeEnum.APPRAISAL, businessUnitId);
}

private String loadYears(HttpServletRequest request, Integer businessUnitId) {
	List<Integer> years = null;
	try {
		years = blackBookBookoutService.retrieveModelYears(null);
	} catch (GBException e ) {
		request.setAttribute( "error", Boolean.TRUE );
		request.setAttribute( "showOptions", Boolean.FALSE );
		request.setAttribute( "showValues", Boolean.FALSE );
	}
	request.setAttribute( "years", years );
	return "success";
}

private String loadMakes( HttpServletRequest request, Integer businessUnitId )
{
	String selectedYear = request.getParameter( "year" );
	List< VDP_GuideBookVehicle > makes = new ArrayList< VDP_GuideBookVehicle >();
	try
	{
		makes = (List< VDP_GuideBookVehicle >)blackBookBookoutService.retrieveMakes( selectedYear, null );
	}
	catch ( GBException e )
	{
		request.setAttribute( "error", Boolean.TRUE );
		request.setAttribute( "showOptions", Boolean.FALSE );
		request.setAttribute( "showValues", Boolean.FALSE );
		return "optionSuccess";
	}
	request.setAttribute( "makes", makes );
	return "makeSuccess";
}

private String loadModels( HttpServletRequest request, Integer businessUnitId )
{
	String selectedYear = request.getParameter( "year" );
	String selectedMake = request.getParameter( "make" );
	List< VDP_GuideBookVehicle > models = new ArrayList< VDP_GuideBookVehicle >();
	try
	{
		models = (List< VDP_GuideBookVehicle >)blackBookBookoutService.retrieveModels( selectedYear, selectedMake, null );
	}
	catch ( GBException e )
	{
		request.setAttribute( "error", Boolean.TRUE );
		request.setAttribute( "showOptions", Boolean.FALSE );
		request.setAttribute( "showValues", Boolean.FALSE );
		return "optionSuccess";
	}
	request.setAttribute( "models", models );
	return "modelSuccess";
}

private String loadSeries( HttpServletRequest request, Integer businessUnitId )
{
	String selectedYear = request.getParameter( "year" );
	String selectedMake = request.getParameter( "make" );
	String selectedModel = request.getParameter( "model" );
	List< VDP_GuideBookVehicle > series = new ArrayList< VDP_GuideBookVehicle >();
	try
	{
		series = (List< VDP_GuideBookVehicle >)blackBookBookoutService.retrieveTrims( selectedYear, selectedMake, selectedModel, null );
	}
	catch ( GBException e )
	{
		request.setAttribute( "error", Boolean.TRUE );
		request.setAttribute( "showOptions", Boolean.FALSE );
		request.setAttribute( "showValues", Boolean.FALSE );
		return "optionSuccess";
	}
	
	request.setAttribute( "series", series );
	return "seriesSuccess";
}

private String loadStyles( HttpServletRequest request, Integer businessUnitId )
{
	String selectedYear = request.getParameter( "year" );
	String selectedMake = request.getParameter( "make" );
	String selectedModel = request.getParameter( "model" );
	String selectedSeries = request.getParameter( "series" ); // can be null
	if (selectedSeries.equals("0")) 
		selectedSeries = null;
	List< VDP_GuideBookVehicle > styles = new ArrayList< VDP_GuideBookVehicle >();
	try
	{
		styles = (List< VDP_GuideBookVehicle >)blackBookBookoutService.retrieveStyles( selectedYear, selectedMake, selectedModel, selectedSeries);
	}
	catch ( GBException e )
	{
		request.setAttribute( "error", Boolean.TRUE );
		request.setAttribute( "showOptions", Boolean.FALSE );
		request.setAttribute( "showValues", Boolean.FALSE );
		return "optionSuccess";
	}
	
	request.setAttribute( "styles", styles );
	return "styleSuccess";
}



private String loadOptions( HttpServletRequest request, Integer businessUnitId, String stateCD ) throws GBException
{
	String selectedYear = request.getParameter( "year" );
	String selectedMake = request.getParameter( "make" );
	String selectedModel = request.getParameter( "model" );
	
	String selectedSeries = request.getParameter("series");
	String selectedKey = "";
	if (selectedSeries == null || selectedSeries.equals("0")) {
		selectedSeries = null;
	} else {
		selectedKey += selectedSeries;
	}
	String selectedStyle = request.getParameter("style");
	if (selectedStyle == null || selectedStyle.equals("0")) {
		selectedStyle = null;
	} else {
		selectedKey += " " + selectedStyle;
	}
	
	
	DisplayGuideBook displayGuideBook = new DisplayGuideBook();
	try
	{
		final List<ThirdPartyVehicleOption> bookValues = blackBookBookoutService.retrieveOptionsManual(stateCD, selectedYear, selectedMake, selectedModel, selectedSeries, selectedStyle);
		displayGuideBook.setDisplayGuideBookOptions(bookValues);
		displayGuideBook.setYear(selectedYear);
		displayGuideBook.setMake(selectedMake);
		displayGuideBook.setModel(selectedModel);
		displayGuideBook.setSelectedVehicleKey(selectedKey);
	}
	catch ( GBException e )
	{
		request.setAttribute( "error", Boolean.TRUE );
		request.setAttribute( "showOptions", Boolean.FALSE );
		request.setAttribute( "showValues", Boolean.FALSE );
	}
	
	setSelectedEquipmentOptionKeys( displayGuideBook);
	
	setDisplayGuideBookInfo(displayGuideBook, businessUnitId, stateCD);
	
	request.setAttribute( "showOptions", Boolean.TRUE );
	request.setAttribute( "success", Boolean.TRUE);
	request.setAttribute( "showValues", Boolean.FALSE );
	request.setAttribute( "displayGuideBook", displayGuideBook );
	return "optionSuccess";
}

private void setSelectedEquipmentOptionKeys( DisplayGuideBook displayGuideBook )
{
	Iterator<ThirdPartyVehicleOption> iter = displayGuideBook.getDisplayGuideBookOptions().iterator();
	List<String> selectedOptions = new ArrayList< String >();
	while ( iter.hasNext() )
	{
		ThirdPartyVehicleOption option = iter.next();
		if ( option.isStatus() )
		{
			selectedOptions.add( option.getOptionKey() );
		}
	}
	displayGuideBook.setSelectedEquipmentOptionKeys( (String[])selectedOptions.toArray(new String[selectedOptions.size()]) );
}

private String loadValues( HttpServletRequest request, Integer businessUnitId, String stateCD ) throws ApplicationException
{
	String selectedYear = request.getParameter( "year" );
	String selectedMake = request.getParameter( "make" );
	String selectedModel = request.getParameter( "model" );
	String selectedSeries = request.getParameter("series");
	String mileage = request.getParameter( "mileage" );
	request.setAttribute("make", selectedMake);
	request.setAttribute("model", selectedModel);
	request.setAttribute("series", selectedSeries);
	request.setAttribute("mileage", mileage);
	request.setAttribute("notes", request.getParameter("notes_s"));
	Dealer dealer = putDealerFormInRequest(request);
	
	String selectedKey = "";
	if (selectedSeries == null || selectedSeries.equals("0")) {
		selectedSeries = null;
	} else {
		selectedKey += selectedSeries;
	}
	String selectedStyle = request.getParameter("style");
	if (selectedStyle == null || selectedStyle.equals("0")) {
		selectedStyle = null;
	} else {
		selectedKey += " " + selectedStyle;
	}
	
	String[] selectedEquipmentOptionKeys = getSelectedEquipmentOptionKeys(request);
	DisplayGuideBook displayGuideBook = new DisplayGuideBook();
	boolean hasErrors = false;
	
	try
	{
		displayGuideBook.setDisplayGuideBookOptions( blackBookBookoutService.retrieveOptionsManual(stateCD, selectedYear, selectedMake, selectedModel, selectedSeries, selectedStyle));
		displayGuideBook.setYear(selectedYear);
		displayGuideBook.setMake(selectedMake);
		displayGuideBook.setModel(selectedModel);
		displayGuideBook.setSelectedVehicleKey(selectedKey);
		
		if ( selectedEquipmentOptionKeys != null )
		{
			displayGuideBook.setSelectedEquipmentOptionKeys( ThirdPartyVehicleOptionService.setAndUpdateSelectedOptionsUsingArray(
																																	displayGuideBook.getDisplayEquipmentGuideBookOptions(),
																																	selectedEquipmentOptionKeys ) );
		}

		int miles = Integer.parseInt( mileage );
		if(miles <= 0) {
			//bug in manual bookout in dealing w/mileage = 0.
			//The right behavior is to parse the blackbook xml correctly and treat 0, null, and "xml element not exists" more distinctly;
			//quick fix for release tonight, Oct. 6, 2009 -bf.
			miles = 1; 
		}
		final List<VDP_GuideBookValue> bookValues = blackBookBookoutService.retrieveBookValuesManual(stateCD, miles, 
		                                                displayGuideBook.getDisplayGuideBookOptions(), selectedYear, selectedMake, selectedModel, selectedSeries, selectedStyle);
		displayGuideBook.setDisplayGuideBookValues(getFinalValues(bookValues));
		displayGuideBook.getDisplayGuideBookValues().add(getMileageAdjustmentValue(bookValues));
		displayGuideBook.setGuideBookOptionAdjustment(getOptionAdjustmentValue(bookValues, dealer.getDealerPreference()));

		VDP_GuideBookValue errorMarker = null;
		List<VDP_GuideBookValue> displayGuideBookValues = displayGuideBook.getDisplayGuideBookValues();
		if(displayGuideBookValues != null && !displayGuideBookValues.isEmpty()) {
			errorMarker = displayGuideBookValues.iterator().next();
			if (errorMarker != null && errorMarker.isHasErrors())
			{
				List<BookOutError> errors = new ArrayList< BookOutError >();
				Iterator<VDP_GuideBookValue> iter = displayGuideBook.getDisplayGuideBookValues().iterator();
				while ( iter.hasNext() )
				{
					VDP_GuideBookValue error = iter.next();
					if ( error.isHasErrors() )
					{
						errors.add( error.getError() );
					}
				}
				request.setAttribute( "errors", errors );
				request.setAttribute( "showOptions", Boolean.TRUE );
				request.setAttribute( "showValues", Boolean.FALSE );
				request.setAttribute( "hasConflict", Boolean.TRUE );
				hasErrors = true;
			}
		}
	}
	catch ( GBException e )
	{
		request.setAttribute( "error", Boolean.TRUE );
		request.setAttribute( "showOptions", Boolean.FALSE );
		request.setAttribute( "showValues", Boolean.FALSE );
		hasErrors = true;
	}

	try{
		setDisplayGuideBookInfo(displayGuideBook, businessUnitId, stateCD);
	}
	catch(GBException gbe){
		throw new ApplicationException(gbe);
	}
	
	if (!hasErrors)
	{
		request.setAttribute( "showOptions", Boolean.TRUE );
		request.setAttribute( "showValues", Boolean.TRUE );
	}
	request.setAttribute( "displayGuideBook", displayGuideBook );
	request.setAttribute( "guideBookMileageAdjustmentType", BookOutValueType.BOOK_OUT_MILEAGE_ADJUSTMENT );
	return "optionSuccess";
}


private VDP_GuideBookValue getOptionAdjustmentValue(
		List<VDP_GuideBookValue> bookValues, DealerPreference dealerPreference) {
	int thirdPartyCategoryId;
	if(ThirdPartyDataProvider.BLACKBOOK.getThirdPartyId().equals(dealerPreference.getGuideBook2Id())) {
		thirdPartyCategoryId = dealerPreference.getGuideBook2BookOutPreferenceId();
	} else { //assume 1st book is blackbook
		thirdPartyCategoryId = dealerPreference.getBookOutPreferenceId();
	}
	
	VDP_GuideBookValue optionAdjustment = null;
	for(VDP_GuideBookValue value : bookValues) {
		if(value.getValueType() == BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE
				&& value.getThirdPartyCategoryId() == thirdPartyCategoryId) {
			optionAdjustment = value;
			break;
		}
	}
	return optionAdjustment;
}


private String[] getSelectedEquipmentOptionKeys(HttpServletRequest request) {
	String[] selectedEquipmentOptionKeys = request.getParameterValues( "selectedEquipmentOptionKeys" );
	try {
		for (int i = 0; i < selectedEquipmentOptionKeys.length; i++) {
			selectedEquipmentOptionKeys[i] = URLDecoder.decode(selectedEquipmentOptionKeys[i], "UTF-8");
		}
	} catch (Exception e) {}
	return selectedEquipmentOptionKeys;
}


private void setDisplayGuideBookInfo( DisplayGuideBook displayGuideBook, Integer businessUnitId, String region ) throws GBException
{
	displayGuideBook.setGuideBookId( ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE );
	displayGuideBook.setImageName( ThirdPartyDataProvider.IMAGE_NAME_BLACKBOOK );
	displayGuideBook.setGuideBookName( "BlackBook" );
	displayGuideBook.setValidVin(Boolean.TRUE);
	int year = Calendar.getInstance().get(Calendar.YEAR);
	String footer = "All Black Book values are reprinted with permission of Black Book \u00ae. NV. Copyright \u00a9 " + year +  " Hearst Business Media Corp";
	displayGuideBook.setFooter( footer );
	BookOutDatasetInfo bookoutInfo = getBookOutDatasetInfo(businessUnitId);
	displayGuideBook.setPublishInfo( blackBookBookoutService.getMetaInfo( region, bookoutInfo ).getPublishInfo() );
}

public void setBlackBookBookoutService(
		BlackBookBookoutService blackBookBookoutService) {
	this.blackBookBookoutService = blackBookBookoutService;
}




}
