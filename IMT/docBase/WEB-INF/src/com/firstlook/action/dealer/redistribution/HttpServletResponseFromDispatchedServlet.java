package com.firstlook.action.dealer.redistribution;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Locale;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is a nice little hack to get the ouputStream of a response.
 * 
 * @author bfung
 * 
 */
public class HttpServletResponseFromDispatchedServlet implements HttpServletResponse
{

private int status;
private ServletOutputStream servletOutputStream;
private PrintWriter printWriter;

public HttpServletResponseFromDispatchedServlet( PrintWriter printWriter )
{
	super();
	this.printWriter = printWriter;
}

public void setStatus( int arg0 )
{
	status = arg0;
}

public void sendError( int arg0, String arg1 ) throws IOException
{
	status = arg0;
}

public void sendError( int arg0 ) throws IOException
{
	status = arg0;

}

public void sendRedirect( String arg0 ) throws IOException
{

}

public void setHeader( String arg0, String arg1 )
{

}

public void addHeader( String arg0, String arg1 )
{

}

public boolean containsHeader( String arg0 )
{
	return false;
}

public void setDateHeader( String arg0, long arg1 )
{

}

public void addDateHeader( String arg0, long arg1 )
{

}

public void setIntHeader( String arg0, int arg1 )
{

}

public void addIntHeader( String arg0, int arg1 )
{

}

public void addCookie( Cookie arg0 )
{

}

public String encodeURL( String arg0 )
{

	return null;
}

public String encodeRedirectURL( String arg0 )
{

	return null;
}

public void setStatus( int arg0, String arg1 )
{
	status = arg0;
}

public String encodeUrl( String arg0 )
{

	return null;
}

public String encodeRedirectUrl( String arg0 )
{

	return null;
}

public void setContentType( String arg0 )
{

}

public String getContentType()
{

	return null;
}

public String getCharacterEncoding()
{

	return null;
}

public void setCharacterEncoding( String arg0 )
{

}

public void setLocale( Locale arg0 )
{

}

public Locale getLocale()
{
	return null;
}

public ServletOutputStream getOutputStream() throws IOException
{
	return servletOutputStream;
}

public PrintWriter getWriter() throws IOException
{

	return printWriter;
}

public void setBufferSize( int arg0 )
{

}

public int getBufferSize()
{
	return 0;
}

public void flushBuffer() throws IOException
{
	servletOutputStream.flush();
}

public boolean isCommitted()
{

	return false;
}

public void reset()
{

}

public void resetBuffer()
{
}

public void setContentLength( int arg0 )
{

}

@Override
public String getHeader(String arg0) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public Collection<String> getHeaderNames() {
	// TODO Auto-generated method stub
	return null;
}

@Override
public Collection<String> getHeaders(String arg0) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public int getStatus() {
	// TODO Auto-generated method stub
	return 0;
}

}
