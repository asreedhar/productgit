package com.firstlook.action.dealer.reports;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;


import com.firstlook.data.DatabaseException;
import com.firstlook.entity.VehicleSaleEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.iterator.SimpleFormIterator;
import com.firstlook.persistence.PAPReportLineItemRetriever;
import com.firstlook.persistence.inventory.ROIRetriever;
import com.firstlook.report.PAPReportLineItem;
import com.firstlook.report.PAPReportLineItemForm;
import com.firstlook.service.cia.report.TrimFormattingUtility;
import com.firstlook.service.dealer.IUCBPPreferenceService;
import com.firstlook.service.pap.PAPReportLineItemService;
import com.firstlook.session.FirstlookSession;

public class UsedCarPerformanceAnalyzerPlusDisplayHelper
{

public static final int MILEAGEFILTER_OFF = 0;
public static final int MILEAGEFILTER_ON = 1;
private ROIRetriever annualRoiDAO;
private IUCBPPreferenceService ucbpPreferenceService;
private PAPReportLineItemRetriever papReportLineItemRetriever;

public int retrieveMileageThreshold( int mileageFilter, int currentDealerId ) throws ApplicationException, DatabaseException
{
	int mileageThreshold = 0;

	if ( mileageFilter == MILEAGEFILTER_ON )
	{
		mileageThreshold = ucbpPreferenceService.retrieveDealerRisk( currentDealerId ).getHighMileageThreshold();
	}
	else
	{
		mileageThreshold = VehicleSaleEntity.MILEAGE_MAXVAL;
	}

	return mileageThreshold;
}

public void putColorItemsInRequest( HttpServletRequest request, String memberProgramType, int weeks, int forecast, int mileageThreshold,
									int groupingDescriptionId, boolean hasMarketUpgrade, int lowerUnitCostThreshold, int ciaGroupingItemId )
{
	List items = papReportLineItemRetriever.findByDealerIdGroupingDescriptionIdWeeksGroupByColor(
																							( (FirstlookSession)request.getSession().getAttribute(
																																					FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId(),
																							groupingDescriptionId, weeks, forecast,
																							mileageThreshold, hasMarketUpgrade, lowerUnitCostThreshold );
	setCIAGroupingIdOnItems( items, ciaGroupingItemId );
	SimpleFormIterator iterator = new SimpleFormIterator( items, PAPReportLineItemForm.class );
	iterator.setCollectionSize( PAPReportLineItemForm.ITEMCOUNT );

	PAPReportLineItemService lineItemService = new PAPReportLineItemService();
	PAPReportLineItem overall = lineItemService.calculateOverall( items );
	PAPReportLineItem.generateOptimixPercentages( overall, items );

	request.setAttribute( "overall", overall );
	request.setAttribute( "colorItems", iterator );
}

public static void setCIAGroupingIdOnItems( List items, int id )
{
	PAPReportLineItem item;
	Iterator itemsIter = items.iterator();
	while ( itemsIter.hasNext() )
	{
		item = (PAPReportLineItem)itemsIter.next();
		item.setCiaGroupingItemId( id );
	}
}

public void putTrimItemsInRequest( HttpServletRequest request, String memberProgramTypeEnum, int weeks, int forecast, int mileageThreshold,
									int groupingDescriptionId, boolean hasMarketUpgrade, int lowerUnitCostThreshold, int ciaGroupingItemId )
{

	List items = papReportLineItemRetriever.findByDealerIdGroupingDescriptionIdWeeksGroupByTrim(
																							( (FirstlookSession)request.getSession().getAttribute(
																																					FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId(),
																							groupingDescriptionId, weeks, forecast,
																							mileageThreshold, hasMarketUpgrade, lowerUnitCostThreshold );
	setCIAGroupingIdOnItems( items, ciaGroupingItemId );
	Iterator itemsIter = items.iterator();
	while ( itemsIter.hasNext() )
	{
		PAPReportLineItem lineItem = (PAPReportLineItem)itemsIter.next();
		lineItem.setGroupingColumnFormatted( TrimFormattingUtility.escapeJS( lineItem.getGroupingColumn() ) );
	}

	SimpleFormIterator iterator = new SimpleFormIterator( items, PAPReportLineItemForm.class );
	iterator.setCollectionSize( PAPReportLineItemForm.ITEMCOUNT );

	PAPReportLineItemService lineItemService = new PAPReportLineItemService();
	PAPReportLineItem overall = lineItemService.calculateOverall( items );
	PAPReportLineItem.generateOptimixPercentages( overall, items );

	request.setAttribute( "overall", overall );
	request.setAttribute( "trimItems", iterator );
}

public void putYearItemsInRequest( HttpServletRequest request, String memberProgramTypeEnum, int weeks, int forecast, int mileageThreshold,
									int groupingDescriptionId, boolean hasMarketUpgrade, int lowerUnitCostThreshold, int ciaGroupingItemId,
									Integer dealerId )
{
	List items = papReportLineItemRetriever.findByDealerIdGroupingDescriptionIdWeeksGroupByYear(
																							( (FirstlookSession)request.getSession().getAttribute(
																																					FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId(),
																							groupingDescriptionId, weeks, forecast,
																							mileageThreshold, hasMarketUpgrade, lowerUnitCostThreshold );
	setCIAGroupingIdOnItems( items, ciaGroupingItemId );
	setROIOnItems( items, dealerId, groupingDescriptionId );
	SimpleFormIterator iterator = new SimpleFormIterator( items, PAPReportLineItemForm.class );
	iterator.setCollectionSize( PAPReportLineItemForm.ITEMCOUNT );

	PAPReportLineItemService lineItemService = new PAPReportLineItemService();
	PAPReportLineItem overall = lineItemService.calculateOverall( items );
	PAPReportLineItem.generateOptimixPercentages( overall, items );

	request.setAttribute( "overall", overall );
	request.setAttribute( "yearItems", iterator );
}

private void setROIOnItems( List items, Integer dealerId, int groupingDescriptionId )
{
	Map annualRoiMap = getAnnualRoiDAO().getMakeModelROI( dealerId, groupingDescriptionId );
	Iterator itemsIter = items.iterator();
	PAPReportLineItem item;
	BigDecimal annualRoi;
	while ( itemsIter.hasNext() )
	{
		item = (PAPReportLineItem)itemsIter.next();
		annualRoi = (BigDecimal)annualRoiMap.get( item.getGroupingColumn() );
		if ( annualRoi != null )
		{
			item.setAnnualRoi( new Double( annualRoi.doubleValue() ) );
		}
		else
		{
			item.setAnnualRoi( new Double( Integer.MIN_VALUE ) );
		}
	}
}

public static void setCIAGroupingIdOnItems( Collection items, int id )
{
	PAPReportLineItem item;
	Iterator itemsIter = items.iterator();
	while ( itemsIter.hasNext() )
	{
		item = (PAPReportLineItem)itemsIter.next();
		item.setCiaGroupingItemId( id );
	}
}

public ROIRetriever getAnnualRoiDAO()
{
	return annualRoiDAO;
}
public void setAnnualRoiDAO( ROIRetriever annualRoiDAO )
{
	this.annualRoiDAO = annualRoiDAO;
}

public void setUcbpPreferenceService( IUCBPPreferenceService ucbpPreferenceService )
{
	this.ucbpPreferenceService = ucbpPreferenceService;
}

public void setPapReportLineItemRetriever(
		PAPReportLineItemRetriever papReportLineItemRetriever) {
	this.papReportLineItemRetriever = papReportLineItemRetriever;
}

}
