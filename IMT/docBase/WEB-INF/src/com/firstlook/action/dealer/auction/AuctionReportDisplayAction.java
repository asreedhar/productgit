package com.firstlook.action.dealer.auction;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.entity.Area;
import biz.firstlook.entity.AuctionReport;
import biz.firstlook.entity.Period;
import biz.firstlook.service.IAuctionDataService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.SessionHelper;

public class AuctionReportDisplayAction extends SecureBaseAction
{

private IAuctionDataService auctionDataService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	Integer periodId = new Integer( request.getParameter( "timePeriodId" ) );
	Integer areaId = new Integer( request.getParameter( "areaId" ) );
	String vic = request.getParameter( "vic" );
	Integer modelYear = RequestHelper.getInt( request, "modelYear" );
	Boolean usingVIC = RequestHelper.getBoolean( request, "usingVIC" );
	
	String make = request.getParameter( "make" );
	String model = request.getParameter( "model" );
	String vicBodyStyleName = request.getParameter( "vicBodyStyleName" );
	request.setAttribute( "make", make );
	request.setAttribute( "model", model );
	request.setAttribute( "modelYear", modelYear );
	request.setAttribute( "vicBodyStyleName", vicBodyStyleName );

	List<AuctionReport> auctionReportData = auctionDataService.retrieveAuctionReport( periodId, vic, areaId, usingVIC, modelYear );

	request.setAttribute( "auctionReportData", auctionReportData );
	request.setAttribute( "vicBodyStyleId", vic );

	Period period = auctionDataService.retrieveByPeriodId( periodId );
	request.setAttribute( "periodDescription", period.getDescription() );
	request.setAttribute( "timePeriodId", period.getPeriodId() );
	
	Area area = auctionDataService.retrieveByAreaId( areaId );
	request.setAttribute( "areaName", area.getAreaName() );
	request.setAttribute( "areaId", area.getAreaId() );

	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );

	// this allows up to use ajax:displayTag out of the box
	if ( request.getParameter( "refresh" ) != null )
	{
		return mapping.findForward( "refresh" );
	}
	
	return mapping.findForward( "success" );
}

public void setAuctionDataService( IAuctionDataService auctionDataService )
{
	this.auctionDataService = auctionDataService;
}

}