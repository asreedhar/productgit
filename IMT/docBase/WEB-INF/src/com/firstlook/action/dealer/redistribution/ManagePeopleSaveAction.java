package com.firstlook.action.dealer.redistribution;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.person.IPerson;
import biz.firstlook.transact.persist.person.IPosition;
import biz.firstlook.transact.persist.person.Person;
import biz.firstlook.transact.persist.person.PositionType;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.form.ManagePeopleForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;

public class ManagePeopleSaveAction extends SecureBaseAction {
	public ActionForward doIt(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)
			throws DatabaseException, ApplicationException {
        int businessUnitId = getFirstlookSessionFromRequest(request).getCurrentDealerId();
        ManagePeopleForm theForm = (ManagePeopleForm) form;
        List<IPerson> peopleToSave = new ArrayList<IPerson>();
        peopleToSave.addAll(theForm.getPeople());
        
        Integer personId = null;
        if (request.getParameter("personId") != null) {
            personId = Integer.parseInt(request.getParameter("personId"));
        }
        IPosition pos = PositionType.getPosFromDesc(theForm.getPosition());
        
        IPerson person = createNewPerson(request, businessUnitId);
        
        checkDuplicates(peopleToSave, person);
        
        if (person != null) {
    		if (!hasPosition(person, pos)){
    			person.getPositions().add(pos);
    		}
            peopleToSave.add(person);
        }
        
        getPersonService().saveOrUpdatePeople(peopleToSave);
        
        theForm.setEditMode(false);
        
        for (IPerson pers:theForm.getPeople()) {
            pers.setEditMode(false);
            if (pers.getPersonId().equals(personId)) {
                pers.setSelected(true);
            }
        }
        
		SessionHelper.keepAttribute(request, "tradeAnalyzerForm");
        SessionHelper.keepAttribute( request, "bookoutDetailForm" );
        SessionHelper.keepAttribute( request, "auctionData" );
        request.setAttribute("personId", null);
		return mapping.findForward("success");
	}


    private IPerson createNewPerson(HttpServletRequest request, int businessUnitId) {
        String firstName = request.getParameter("firstName");
        if (firstName != null && !firstName.equals("")) {
            String lastName = request.getParameter("lastName");
            String email = request.getParameter("email");
            String phoneNumber = request.getParameter("phoneNumber");
            if (phoneNumber != null && phoneNumber.endsWith("?")) {
                phoneNumber = phoneNumber.substring(0, phoneNumber.length()-1);
                if (phoneNumber.length() == 1) {
                    phoneNumber = "";
                }
            }
            IPerson person = checkExistingPerson(firstName, lastName, businessUnitId);
            if (person == null) {
                person = new Person();
                person.setFirstName(firstName);
                person.setLastName(lastName);
                person.setBusinessUnitId(businessUnitId);
            }
            person.setEmail(email);
            person.setPhoneNumber(phoneNumber);
            person.setSelected(true);
            return person;
        }
        return null;
    }

    private void checkDuplicates(List<IPerson>people, IPerson person){
    	List<IPerson> peopleToSave = new ArrayList<IPerson>();
    	for (IPerson p:people){
    		if (person == null || !p.getPersonId().equals(person.getPersonId())){
    			peopleToSave.add(p);
    		}
    	}
    	people.clear();
    	people.addAll(peopleToSave);
    }
    
    private boolean hasPosition(IPerson person, IPosition pos){
    	for (IPosition p:person.getPositions()){
    		if (p.getPositionId().equals(pos.getPositionId())){
    			return true;
    		}
    	}
    	return false;
    }
    
    private IPerson checkExistingPerson(String firstName, String lastName, int businessUnitId) {
        return getPersonService().getPersonByName(firstName, lastName, businessUnitId);
    }
}
