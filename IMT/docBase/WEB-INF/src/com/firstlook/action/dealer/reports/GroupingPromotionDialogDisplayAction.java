package com.firstlook.action.dealer.reports;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.GroupingPromotionPlan;
import com.firstlook.entity.form.GroupingPromotionDialogForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.report.GroupingPromotionPlanDAO;

public class GroupingPromotionDialogDisplayAction extends SecureBaseAction
{

private GroupingPromotionPlanDAO groupingPromotionPlanDAO;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	Integer businessUnitId = new Integer( getFirstlookSessionFromRequest( request ).getCurrentDealerId() );
	Integer groupingDescriptionId = new Integer( request.getParameter( "groupingDescriptionId" ) );

	GroupingPromotionPlan plan = groupingPromotionPlanDAO.findPlansByStatusAndGroupingDescriptionId( businessUnitId, groupingDescriptionId );
	GroupingPromotionDialogForm dialogForm;

	if ( plan != null )
	{
		dialogForm = new GroupingPromotionDialogForm( plan );
	}
	else
	{
		dialogForm = new GroupingPromotionDialogForm();
		dialogForm.setGroupingDescriptionId( groupingDescriptionId );
	}

	request.setAttribute( "groupingPromotionDialogForm", dialogForm );
	return mapping.findForward( "success" );
}

public GroupingPromotionPlanDAO getGroupingPromotionPlanDAO()
{
	return groupingPromotionPlanDAO;
}

public void setGroupingPromotionPlanDAO( GroupingPromotionPlanDAO groupingPromotionPlanDAO )
{
	this.groupingPromotionPlanDAO = groupingPromotionPlanDAO;
}
}