package com.firstlook.action.dealer.redistribution;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.services.insights.Insight;
import biz.firstlook.commons.services.insights.InsightParameters;
import biz.firstlook.commons.services.insights.InsightUtils;
import biz.firstlook.commons.services.insights.PerformanceAnalysisBean;
import biz.firstlook.transact.persist.model.BookOutValue;
import biz.firstlook.transact.persist.model.MakeModelGrouping;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.service.MakeModelGroupingService;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.dealer.tools.ReportGroupingForms;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.DealerForm;
import com.firstlook.entity.form.MemberForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.action.ReportActionHelper;
import com.firstlook.service.bookout.BookOutService;
import com.firstlook.service.inventory.InventoryService_Legacy;

public class PrintableFlashReportDisplayAction extends SecureBaseAction
{

private MakeModelGroupingService makeModelGroupingService;
private IAppraisalService appraisalService;
private InventoryService_Legacy inventoryService_Legacy;
private ReportActionHelper reportActionHelper;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{

	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();

	Dealer dealer = putDealerFormInRequest( request, 0 );
	int guideBookId = dealer.getDealerPreference().getGuideBookIdAsInt();

	putFaxEmailFormInRequest( request );
	
	IAppraisal appraisal = getAppraisalService().findBy( RequestHelper.getInt( request, "appraisalId" ) );
	

	Collection< BookOutValue > appraisalGuideBookValues = BookOutService.determineAppropriateBookOutValues( appraisal.getLatestBookOut( guideBookId ));

	Iterator<BookOutValue> bookOutValuesIter = appraisalGuideBookValues.iterator();
	
	//remove KBB trade-in value from Flash Report
	while(bookOutValuesIter.hasNext()){
		BookOutValue value = bookOutValuesIter.next();
		if(value.getThirdPartyCategory().getThirdPartyCategoryId() == ThirdPartyCategory.KELLEY_TRADEIN_TYPE){
			bookOutValuesIter.remove();
		}
	}
	
	MakeModelGrouping makeModelGrouping = appraisal.getVehicle().getMakeModelGrouping();

	inventoryService_Legacy.setMakeModelGroupingService(getMakeModelGroupingService());
	InventoryEntity inventory = inventoryService_Legacy.createEmptyInventoryWithYearMileageMakeModelUsingVin(
																										appraisal.getVehicle().getVehicleYear().toString(),
																										appraisal.getMileage(),
																										appraisal.getVehicle().getVin(),
																										appraisal.getVehicle().getMakeModelGrouping().getMake(),
																										appraisal.getVehicle().getMakeModelGrouping().getModel() );

	ReportGroupingForms reportGroupingForms = reportActionHelper.createReportGroupingForms( makeModelGrouping.getMake(),
																							makeModelGrouping.getModel(),
																							appraisal.getVehicle().getVehicleTrim(), 0,
																							inventory, InventoryEntity.USED_CAR,
																							ReportActionHelper.DEFAULT_NUMBER_OF_WEEKS,
																							currentDealerId );

	request.setAttribute( "guideBookId", new Integer( guideBookId ) );
	
	List<Insight> insights = InsightUtils.getInsights(new InsightParameters(
			currentDealerId,
			makeModelGrouping.getGroupingDescriptionId(),
			InventoryEntity.USED_CAR,
			ReportActionHelper.DEFAULT_NUMBER_OF_WEEKS,
			appraisal.getVehicle().getVehicleTrim(),
			appraisal.getVehicle().getVehicleYear(),
			appraisal.getMileage()
			));
	
	reportGroupingForms.setPerformanceAnalysisBean(new PerformanceAnalysisBean(insights));

	request.setAttribute( "appraisal", appraisal );
	request.setAttribute( "appraisalGuideBookValues", appraisalGuideBookValues );
	request.setAttribute( "valuesSize", new Integer( appraisalGuideBookValues.size() ) );
	request.setAttribute( "specificReport", reportGroupingForms.getSpecificReportGrouping() );
	request.setAttribute( "generalReport", reportGroupingForms.getGeneralReportGrouping() );
	request.setAttribute( "performanceAnalysisItem", reportGroupingForms.getPerformanceAnalysisBean() );
	request.setAttribute( "make", makeModelGrouping.getMake() );
	request.setAttribute( "model", makeModelGrouping.getModel() );
	request.setAttribute( "weeks", new Integer( ReportActionHelper.DEFAULT_NUMBER_OF_WEEKS ) );
	request.setAttribute( "mileage", inventory.getMileageReceived());

	putFromMemberFormInRequest( request, appraisal );
	putFromDealerFormInRequest( request, appraisal );

	SimpleDateFormat dateFormat = new SimpleDateFormat( "MM/dd/yyyy" );
	request.setAttribute( "postedDate", dateFormat.format( appraisal.getDateCreated() ) );

	return mapping.findForward( "success" );
}

void putFromDealerFormInRequest( HttpServletRequest request, IAppraisal appraisal ) throws ApplicationException
{
	try
	{
		Dealer fromDealer;

		if ( request.getParameter( "fromDealerId" ) == null )
		{
			fromDealer = getImtDealerService().retrieveDealer( appraisal.getBusinessUnitId().intValue() );
		}
		else
		{
			fromDealer = getImtDealerService().retrieveDealer( RequestHelper.getInt( request, "fromDealerId" ) );
		}

		request.setAttribute( "fromDealerForm", new DealerForm( fromDealer ) );
	}
	catch ( Exception de )
	{
		throw new ApplicationException( de );
	}
}

void putFromMemberFormInRequest( HttpServletRequest request, IAppraisal appraisal ) throws ApplicationException
{
	Member fromMember;

	if ( request.getParameter( "fromMemberId" ) == null )
	{
		fromMember = getMemberService().retrieveMember( appraisal.getMemberId() );
	}
	else
	{
		fromMember = getMemberService().retrieveMember( RequestHelper.getInt( request, "fromMemberId" ) );
	}
	request.setAttribute( "fromMemberForm", new MemberForm( fromMember ) );
}

public MakeModelGroupingService getMakeModelGroupingService()
{
	return makeModelGroupingService;
}

public void setMakeModelGroupingService( MakeModelGroupingService makeModelGroupingService )
{
	this.makeModelGroupingService = makeModelGroupingService;
}

public IAppraisalService getAppraisalService()
{
	return appraisalService;
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

public void setInventoryService_Legacy( InventoryService_Legacy inventoryService )
{
	this.inventoryService_Legacy = inventoryService;
}

public ReportActionHelper getReportActionHelper()
{
	return reportActionHelper;
}

public void setReportActionHelper( ReportActionHelper reportActionHelper )
{
	this.reportActionHelper = reportActionHelper;
}

}
