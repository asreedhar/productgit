package com.firstlook.action.dealer.tools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.services.vehicleCatalog.IVehicleCatalogService;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogEntry;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogKey;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogMultiEntry;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogServiceException;
import biz.firstlook.commons.util.BasicAuthAdapter;
import biz.firstlook.module.vehicle.description.old.BookOutError;
import biz.firstlook.module.vehicle.description.old.GuideBookInput;
import biz.firstlook.module.vehicle.description.old.KBBConditionEnum;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.JobTitle;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.person.IPerson;
import biz.firstlook.transact.persist.person.PositionType;
import biz.firstlook.transact.persist.service.MakeModelGroupingService;
import biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum;
import biz.firstlook.transact.persist.service.appraisal.Customer;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;
import biz.firstlook.transact.persist.service.appraisal.StagingBookOut;

import com.firstlook.data.DatabaseException;
import com.firstlook.display.tradeanalyzer.DisplayGuideBook;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.TradeAnalyzerForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.bookout.BookOutService;
import com.firstlook.service.bookout.appraisal.AppraisalBookOutService;
import com.firstlook.service.bookout.bean.AbstractBookOutState;


public class GuideBookDetailSubmitAction extends AbstractTradeAnalyzerFormAction
{
	
private static final Logger logger = Logger.getLogger( GuideBookDetailSubmitAction.class );

	
private AppraisalBookOutService appraisalBookOutService;
private IAppraisalService appraisalService;
private MakeModelGroupingService makeModelGroupingService;
private IVehicleCatalogService vehicleCatalogService;

protected ActionForward analyzeIt(ActionMapping mapping, TradeAnalyzerForm tradeAnalyzerForm,	HttpServletRequest request, HttpServletResponse response, Dealer dealer)
	throws DatabaseException, ApplicationException
{
	
	ActionErrors errors = new ActionErrors();
	boolean notInCatalog = false;
	
	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	DealerPreference dealerPref = dealer.getDealerPreference();
	
	List<Integer> thirdPartyIds = dealerPref.getOrderedGuideBookIds();
	if ( !tradeAnalyzerForm.isCatalogKeyKnown() )
	{
		notInCatalog = populateCatalogInformation( errors, tradeAnalyzerForm, request );
	}
	else
	{
		notInCatalog = populateCatalogInfoWithoutVin( errors, tradeAnalyzerForm, request );
	}


/*	if (tradeAnalyzerForm.getMmrTrims().length < 1)
	{
		//String basicAuthString = (String) request.getSession().getServletContext().getAttribute(BasicAuthAdapter.NAME_IN_SESSION);
		
		//tradeAnalyzerForm.populateMMRAreas(basicAuthString);
		//tradeAnalyzerForm.populateMMRTrims(basicAuthString);
	}
*/	
	String forward = "failure";
	IAppraisal appraisal = null;
	IAppraisal bufferedAppraisal = null;
	
	if ( !notInCatalog )
	{
		appraisal = appraisalService.findLatest( currentDealerId, tradeAnalyzerForm.getVin(), dealerPref.getSearchAppraisalDaysBackThreshold() );
		
		//needed to establish finer grain for appraisal types in JSP
		if( appraisal != null ) {
		
			if ( appraisal.getAppraisalSource().isMobile() ){
				request.setAttribute("isMobileAppraisal", true);
			}
			else if( appraisal.getAppraisalSource().isCrmNotWavis() ){
				request.setAttribute("isCRMAppraisal", true);
			}
			else {
				request.setAttribute("isWirelessAppraisal", true);
			}
		
			if ( appraisal.getAppraisalSource().isCRMOrMobile()) { //confusingly I believe this covers CRM and Wavis/Wireless
				
				// NOTE: workaround due to lazy instantiation - because we changed the workflow for 3.0 we are not instantiating all the variables 
				//			we need (by drawing them out on a page) thus this bandaid to instantiate appraisal value array.
				appraisal.getAppraisalValues().size();
				
				tradeAnalyzerForm.setPendingAppraisal(appraisal);
				
				populateCustomerInformation(tradeAnalyzerForm, appraisal);
				
				populateTradeAnalyzerForm(appraisal, tradeAnalyzerForm);
				
				bufferedAppraisal = appraisal;
				
				appraisal = null; // cause it not a real appraisal... yet. 
			}
		
		}
		
		if(tradeAnalyzerForm.getAppraisalId()!=null){
			IAppraisal tempAppr=appraisalService.findBy(tradeAnalyzerForm.getAppraisalId());
			if(!tempAppr.getAppraisalSource().isCRMOrMobile()){
				
				appraisal=tempAppr;
				populateTradeAnalyzerForm(appraisal, tradeAnalyzerForm);
			}
			
		}
		
		GuideBookInput input = BookOutService.createGuideBookInput( tradeAnalyzerForm.getVin(), tradeAnalyzerForm.getMileage(),
				currentDealerId, dealer.getState(),
				dealerPref.getNadaRegionCode(),
				getMemberFromRequest( request ).getMemberId() );
		
		populateDisplayGuideBooks( request, thirdPartyIds, appraisal, input, tradeAnalyzerForm, dealerPref, bufferedAppraisal);
		
		checkMakeModelOverrideFromDropDowns( request, tradeAnalyzerForm, appraisal );

		
		forward = getMappingForward( tradeAnalyzerForm, request );
	
	}

	DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerId( getFirstlookSessionFromRequest( request ).getCurrentDealerId() );
	request.setAttribute( "isLithia", dealerGroup.isLithiaStore() );
	
	List<Member> contactsToEmailList = null;
	Integer jobTitleToEmail = null;
	if (dealerGroup.isLithiaStore()) {
		jobTitleToEmail = JobTitle.LITHIA_CAR_CENTER_ID;
	} else {
		jobTitleToEmail =JobTitle.UCM_ID;
	}
	contactsToEmailList = getMemberService().getMembersByBusinessUnitAndJobTitle( currentDealerId, jobTitleToEmail );

	request.setAttribute( "defaultContactId", dealerPref.getDefaultLithiaCarCenterMemberId() );
	request.setAttribute( "contactsToEmailList", contactsToEmailList );

	if(appraisal!=null)
	populateCustomerInformation(tradeAnalyzerForm, appraisal);


	
	if (tradeAnalyzerForm.getAppraisalTypeEnum() == null) {
		setAppraisalType(tradeAnalyzerForm, request, appraisal);
	}
	
	
	//Appraiser Name DropDown
	List<IPerson> appraisers = new ArrayList<IPerson>();
    PositionType appraiserType = PositionType.valueFromPositionString("Appraiser");
    if (appraiserType != null) {
        appraisers = (List<IPerson>) getPersonService().getBusinessUnitPeopleByPosition(getFirstlookSessionFromRequest( request ).getCurrentDealerId(), appraiserType);
        
    }
    tradeAnalyzerForm.setAppraisers(appraisers);
    request.setAttribute( "appraisers", appraisers );
    
    Integer selectedAppraiserIndex = -1;
    Integer i = 0;
    if(appraisal!=null){
	    if(appraisal.getLatestAppraisalValue()!=null)	
	    {
	    	String appraiserName = appraisal.getLatestAppraisalValue().getAppraiserName();
	    	
	    	if(appraiserName!=null)
	    	{
	    			for (IPerson appraiser:appraisers){
	    				if ((appraisal.getLatestAppraisalValue() ==null) || (appraisal.getLatestAppraisalValue()!=null && appraiser.getFullName()!=null && (appraiser.getFullName().trim().equalsIgnoreCase(appraisal.getLatestAppraisalValue().getAppraiserName().trim())))) {
	    					selectedAppraiserIndex = i;
	    					tradeAnalyzerForm.setAppraiserName(appraiser.getFullName());
	    					appraiser.setSelected(true);
	    					break;
	    				}	
	    				i++;
	    			}
	    	}
	    }
	    if(appraisal.getSelectedBuyer()!=null){	
	    	tradeAnalyzerForm.setBuyerId(appraisal.getSelectedBuyer().getPersonId());
	    	tradeAnalyzerForm.setBuyerName(appraisal.getSelectedBuyer().getFullName());
	    }
    }
    tradeAnalyzerForm.setRequireNameOnAppraisals(dealer.getDealerPreference().getRequireNameOnAppraisals());
    tradeAnalyzerForm.setSelectedAppraiserIndex(selectedAppraiserIndex);
    request.setAttribute("selectedAppraiserIndex", selectedAppraiserIndex);
	
	ActionForward result = mapping.findForward( forward ); 
		
	return result; 
}

private void setAppraisalType(TradeAnalyzerForm tradeAnalyzerForm, HttpServletRequest request, IAppraisal appraisal) {
	if (tradeAnalyzerForm.getContextEnum() == TradeAnalyzerForm.TradeAnalyzerContextEnum.SEARCH_AQUISITION) {
		tradeAnalyzerForm.setAppraisalTypeEnum(AppraisalTypeEnum.PURCHASE);		
	} else {
		AppraisalTypeEnum appraisalType = getAppraisalType(request, appraisal);
		tradeAnalyzerForm.setAppraisalTypeEnum(appraisalType);
	}
}

private AppraisalTypeEnum getAppraisalType(HttpServletRequest request, IAppraisal appraisal) {
	AppraisalTypeEnum appraisalType = AppraisalTypeEnum.TRADE_IN; 
	if (appraisal != null) {
		if (AppraisalTypeEnum.PURCHASE == AppraisalTypeEnum.getType(appraisal.getAppraisalTypeId())) {
			appraisalType = AppraisalTypeEnum.PURCHASE;
		}
	} else {
		String appraisalTypeParam = request.getParameter("appraisalType");
		if (appraisalTypeParam != null) {
			if (appraisalTypeParam.equalsIgnoreCase("purchase")) {
				appraisalType = AppraisalTypeEnum.PURCHASE;
			} 
		}
	}
	return appraisalType;
}

private void populateCustomerInformation(TradeAnalyzerForm tradeAnalyzerForm, IAppraisal appraisal) {

	//IAppraisal appraisal = appraisalService.findBy(tradeAnalyzerForm
			//.getAppraisalId());
	Customer customer = appraisal.getCustomer();
	if (customer != null) {
		// Customer's PK is appraisalId. Customer = AppraisalCustomer
		tradeAnalyzerForm.setCustomerId(appraisal.getAppraisalId());
		tradeAnalyzerForm.setCustomerFirstName(customer.getFirstName());
		tradeAnalyzerForm.setCustomerLastName(customer.getLastName());
		tradeAnalyzerForm.setCustomerGender(customer.getGender());
		tradeAnalyzerForm.setCustomerPhoneNumber(customer.getPhoneNumber());
		tradeAnalyzerForm.setCustomerEmail(customer.getEmail());
	}

}

private void checkMakeModelOverrideFromDropDowns( HttpServletRequest request, TradeAnalyzerForm tradeAnalyzerForm, IAppraisal appraisal )
{
	String fromDropDown = request.getParameter( "fromDropDown" );
	if ( fromDropDown != null && fromDropDown.equals( "true" ) )
	{
		if ( tradeAnalyzerForm.getTrim().equalsIgnoreCase( "Please Select Trim" ) )
		{
			tradeAnalyzerForm.setTrim( "Base" );
		}
		else
		{
			tradeAnalyzerForm.setTrim( tradeAnalyzerForm.getTrim() );
		}

		if ( appraisal != null )
		{
			if ( appraisal.getVehicle() != null )
			{
				appraisal.getVehicle().setMake( tradeAnalyzerForm.getMake() );
				appraisal.getVehicle().setModel( tradeAnalyzerForm.getModel() );

				appraisal.getVehicle().setOriginalMake( tradeAnalyzerForm.getMake() );
				appraisal.getVehicle().setOriginalModel( tradeAnalyzerForm.getModel() );

				appraisal.getVehicle().setOriginalTrim( tradeAnalyzerForm.getTrim() );
			}
		}
	}
}

private void populateDisplayGuideBooks( HttpServletRequest request, List<Integer> thirdPartyIds, IAppraisal appraisal, GuideBookInput input,
										TradeAnalyzerForm tradeAnalyzerForm, DealerPreference dealerPref, IAppraisal bufferedAppraisal) throws ApplicationException
{
	Iterator<Integer> thirdPartyIdsIter = thirdPartyIds.iterator();
	tradeAnalyzerForm.setMileage( input.getMileage() );
	AbstractBookOutState bookOutState = null;

	if ( appraisal != null ){
		super.populateTradeAnalyzerForm( appraisal, tradeAnalyzerForm );
	}

	Collection<DisplayGuideBook> displayGuideBooks = tradeAnalyzerForm.getDisplayGuideBooks();
	List<DisplayGuideBook> displList = (List<DisplayGuideBook>) displayGuideBooks;
	if ( ( displayGuideBooks == null ) || displayGuideBooks.isEmpty() || (displList.get(0).getDisplayGuideBookModels()!=null) )
	{

		while ( thirdPartyIdsIter.hasNext() )
		{
			Integer thirdPartyBookoutServiceId = thirdPartyIdsIter.next();

			try
			{

				StagingBookOut sBookOut = null;
				
				if (bufferedAppraisal != null && (bufferedAppraisal.getAppraisalSource() != null && bufferedAppraisal.getAppraisalSource().isCRMOrMobile())){
					for (StagingBookOut stagingBookOut : bufferedAppraisal.getStagingBookOuts()) {
						Integer thirdPartyId = stagingBookOut.getThirdPartyDataProvider().getThirdPartyId();
						if (thirdPartyId.intValue() == thirdPartyBookoutServiceId.intValue()){
							sBookOut = stagingBookOut;
						}
					}
				}
				//selects the modelID once the model is selected in the dropdown for kbb exceptional scenario
				List<DisplayGuideBook> disList = (List<DisplayGuideBook>)tradeAnalyzerForm.getDisplayGuideBooks();
				String selectedModelId = "";
				for (DisplayGuideBook displayGuideBook : disList) {
					if(displayGuideBook.getListDisplayGuideBookModels()!=null && displayGuideBook.getListDisplayGuideBookModels().size()!=0){
						selectedModelId = displayGuideBook.getSelectedModelKey();
					}
				}
				input.setSelectedModelId(selectedModelId);
				input.setModel(tradeAnalyzerForm.getModel());
				input.setMake(tradeAnalyzerForm.getMake());
				bookOutState = getAppraisalBookOutService().retrieveExistingBookOutInfoOrBeginNewBookOut( input, thirdPartyBookoutServiceId, 
																											dealerPref.getSearchInactiveInventoryDaysBackThreshold(), sBookOut);
				
				bookOutState.setStagingBookOut(sBookOut);
				
				constructDisplayGuideBooks( request, tradeAnalyzerForm, bookOutState, thirdPartyBookoutServiceId, dealerPref);
				
			}
			catch ( Exception e )
			{
				DisplayGuideBook guideBook = new DisplayGuideBook();
				guideBook.setSuccess( false );
				Throwable t = e.getCause();
				guideBook.setSuccessMessage( t == null ? e.getMessage() : t.getMessage() );
				guideBook.setValidVin( true );
				guideBook.setGuideBookId( thirdPartyBookoutServiceId.intValue() );
				guideBook.setGuideBookName( ThirdPartyDataProvider.getThirdPartyDataProviderDescription( thirdPartyBookoutServiceId.intValue() ) );
				tradeAnalyzerForm.addDisplayGuideBook( guideBook );

			}
		}
	}

	/*--------------   New Logic for removing of the extra displayGuideBooks which will come for the exceptional scenario for the KBB.
	 * Please note a new dropdown i.e dropdownModels has been introduced for this in DisplayGuidebook      --------------------------*/
	
	List<DisplayGuideBook> existingDisplayGuideBooks =  (List<DisplayGuideBook>)tradeAnalyzerForm.getDisplayGuideBooks();
	int kbbModelIndex = -1;
	String bookNames="";
	List<DisplayGuideBook> removableDisplayGuideBooks = new ArrayList<DisplayGuideBook>(0);
	
	/*For Loop to check which DisplayGuidebook contains Trim Drop Down and thereby merge the trim drop down 
	to the index position of model drop down for kbb only*/
 
	for (DisplayGuideBook displayGuideBook : existingDisplayGuideBooks) {
		if(displayGuideBook.getListDisplayGuideBookModels()!=null && displayGuideBook.getListDisplayGuideBookModels().size()>0 && kbbModelIndex== -1)
		{
			kbbModelIndex=existingDisplayGuideBooks.indexOf(displayGuideBook);
		}
		if(displayGuideBook.getListOfDisplayGuideBookVehicles()!=null && displayGuideBook.getListOfDisplayGuideBookVehicles().size()>0)
		{
			if(kbbModelIndex != -1 && displayGuideBook.getGuideBookId()==3)
			{
				existingDisplayGuideBooks.get(kbbModelIndex).setDisplayGuideBookVehicles(displayGuideBook.getListOfDisplayGuideBookVehicles());
			}
		}
		if(bookNames.contains(displayGuideBook.getGuideBookName()))
		{
			removableDisplayGuideBooks.add(displayGuideBook);
		}
		bookNames = bookNames + displayGuideBook.getGuideBookName();

	}
	existingDisplayGuideBooks.removeAll(removableDisplayGuideBooks); // removing of the redundant displayGuideBooks which will be there only in the KBB exceptional scenario

		
	// ----------------------------------------New Logic End-------------------------------------------------------------------------------//
}

/**
 * Populates catalog info to the TradeAnalyzerForm when a VIN is not known. Currently used when a VIN is not found in the Catalog and a user
 * enters : Year, Make, Model and CatalogKey individually
 * 
 * @param edmundsErrors
 * @param tradeAnalyzerForm
 */
private boolean populateCatalogInfoWithoutVin( ActionErrors edmundsErrors, TradeAnalyzerForm tradeAnalyzerForm, HttpServletRequest request )
{
	boolean hasErrors = true;

	VehicleCatalogEntry vcEntry = null;
	try {
		vcEntry = vehicleCatalogService.retrieveVehicleCatalogEntry( tradeAnalyzerForm.getMake(), tradeAnalyzerForm.getModel(),
		                                                                                 tradeAnalyzerForm.getYear(), tradeAnalyzerForm.getCatalogKeyId());
	} catch (VehicleCatalogServiceException e) {
		logger.info("Unable to find vehicle in catalog using make, model, year and catalog key. Forwarding to VINless appraisal.");
	}
	
	if ( vcEntry != null )
	{
		tradeAnalyzerForm.setMake( vcEntry.getMake() );
		tradeAnalyzerForm.setModel( vcEntry.getModel() ); // IMPORTANT. This == Model from MMG. Model from the catalog is set on OriginalModel
		tradeAnalyzerForm.setSegment( vcEntry.getSegment() );
		tradeAnalyzerForm.setYear( vcEntry.getModelYear() );
		tradeAnalyzerForm.setMakeModelGroupingId( vcEntry.getMakeModelGroupingId() );
		tradeAnalyzerForm.setTrim( vcEntry.getSeries() );
		tradeAnalyzerForm.setCatalogKey(vcEntry.getCatalogKey());
		hasErrors = false;
	}
	else
	{
		// Uggg, we can't find the Y/M/M in the Catalog. Bad news bears.
		edmundsErrors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.dealer.tools.tradeanalyzer.vinnotinfirstlook" ) );
		request.setAttribute( "edmundsError", new Boolean( true ) );
		putActionErrorsInRequest( request, edmundsErrors );
	}
	return hasErrors;
}

private boolean populateCatalogInformation( ActionErrors errors, TradeAnalyzerForm tradeAnalyzerForm, HttpServletRequest request )
{
	boolean hasErrors = true;

	VehicleCatalogMultiEntry vcMultiEntry = null;
	
	try {
		vcMultiEntry = vehicleCatalogService.retrieveVehicleCatalogMultiEntry( tradeAnalyzerForm.getVin() );
	} catch (VehicleCatalogServiceException e) {
		logger.info("Unable to find vehicle in catalog using VIN. Forwarding to VINless appraisal.");
	}
	
	if ( vcMultiEntry != null )
	{
		tradeAnalyzerForm.setMake( vcMultiEntry.getMake() );
		tradeAnalyzerForm.setModel( vcMultiEntry.getModel() ); // IMPORTANT. This == Model from MMG. Model from the catalog is set on OriginalModel
		tradeAnalyzerForm.setSegment( vcMultiEntry.getSegment() );
		tradeAnalyzerForm.setYear( vcMultiEntry.getModelYear() );
		tradeAnalyzerForm.setMakeModelGroupingId( vcMultiEntry.getMakeModelGroupingId() );
		List< VehicleCatalogKey > catalogKeys = vcMultiEntry.getCatalogKeys();
		tradeAnalyzerForm.setCatalogKeys( catalogKeys );
		if ( catalogKeys.size() == 1 )
		{
			tradeAnalyzerForm.setCatalogKey( catalogKeys.get( 0 ) );
			List<String> trims = vcMultiEntry.getTrims();
			if (trims != null && trims.size() > 0) { 
				tradeAnalyzerForm.setTrim( trims.get(  0 ) );
			}
		}
		hasErrors = false;
	}
	else
	{
		// Uggg, we can't find the VIN in the Catalog. Bad news bears. Time for a vinless appraisal.
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.dealer.tools.tradeanalyzer.vinnotinfirstlook" ) );
		request.setAttribute( "edmundsError", new Boolean( true ) );
		putActionErrorsInRequest( request, errors );
	}
	return hasErrors;
}

private void constructDisplayGuideBooks( HttpServletRequest request, TradeAnalyzerForm tradeAnalyzerForm, AbstractBookOutState bookOutState, Integer thirdPartyBookoutServiceId,
										DealerPreference dealerPref )
{
	
	DisplayGuideBook displayGuideBook;
	if ( bookOutState.getBookOutErrors().get( new Integer( BookOutError.INVALID_VIN_NOT_IN_GUIDEBOOK ) ) != null )
	{
		DisplayGuideBook invalidVinGuideBook = new DisplayGuideBook();
		bookOutState.transformBookOutToDisplayData( invalidVinGuideBook );
		tradeAnalyzerForm.addDisplayGuideBook( invalidVinGuideBook );
	}
	else
	{ 
		displayGuideBook = new DisplayGuideBook();
		bookOutState.transformBookOutToDisplayData( displayGuideBook );
		VDP_GuideBookVehicle selectedVehicle;
		
		
		// if your KBB and you've yet to set a condition value - apply the dealers default condition
		if(bookOutState.getDropDownModels()!= null && !bookOutState.getDropDownModels().isEmpty() && bookOutState.getDropDownVehicles().size()== 0){
			 selectedVehicle = BookOutService.determineSelectedVehicleByKey( bookOutState.getDropDownModels(), bookOutState.getSelectedVehicleKey() );
		}
		else{
		selectedVehicle = BookOutService.determineSelectedVehicleByKey( bookOutState.getDropDownVehicles(), bookOutState.getSelectedVehicleKey() );
		}
	
		if( thirdPartyBookoutServiceId.intValue() == ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE ) 
		{
			if ( selectedVehicle == null )
			{
				displayGuideBook.setCondition( KBBConditionEnum.getKBBConditionEnumById( dealerPref.getKbbInventoryDefaultCondition() ) );
			} else if (selectedVehicle != null && selectedVehicle.getCondition() == null )
			{
				selectedVehicle.setCondition( KBBConditionEnum.getKBBConditionEnumById( dealerPref.getKbbInventoryDefaultCondition() ) );
				displayGuideBook.setCondition( KBBConditionEnum.getKBBConditionEnumById( dealerPref.getKbbInventoryDefaultCondition() ) );
			} else if (selectedVehicle != null && selectedVehicle.getCondition() != null )
			{
				displayGuideBook.setCondition( selectedVehicle.getCondition() );
			}
		}
		tradeAnalyzerForm.addDisplayGuideBook( displayGuideBook );
	}
}

String getMappingForward( TradeAnalyzerForm tradeAnalyzerForm, HttpServletRequest request )
{

	String mappingForward = "success";
	ActionErrors guideBookErrors = new ActionErrors();
	Iterator<DisplayGuideBook> displayGuideBookIter = tradeAnalyzerForm.getDisplayGuideBooks().iterator();
	DisplayGuideBook displayGuideBook;
	boolean allVinsInvalid = true;
	while ( displayGuideBookIter.hasNext() )
	{
		displayGuideBook = displayGuideBookIter.next();
		if ( displayGuideBook.isValidVin() )
		{
			allVinsInvalid = false;
		}
	}
	if ( allVinsInvalid )
	{
		mappingForward = "failure";
		guideBookErrors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.dealer.tools.tradeanalyzer.vinnotfound" ) );
		request.setAttribute( "guideBookError", new Boolean( true ) );
		putActionErrorsInRequest( request, guideBookErrors );
	}
	return mappingForward;
}

public AppraisalBookOutService getAppraisalBookOutService()
{
	return appraisalBookOutService;
}

public void setAppraisalBookOutService( AppraisalBookOutService appraisalBookOutService )
{
	this.appraisalBookOutService = appraisalBookOutService;
}

public MakeModelGroupingService getMakeModelGroupingService()
{
	return makeModelGroupingService;
}

public void setMakeModelGroupingService( MakeModelGroupingService makeModelGroupingService )
{
	this.makeModelGroupingService = makeModelGroupingService;
}

public IAppraisalService getAppraisalService()
{
	return appraisalService;
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

public void setVehicleCatalogService( IVehicleCatalogService vehicleCatalogService )
{
	this.vehicleCatalogService = vehicleCatalogService;
}

}