package com.firstlook.action.dealer.scorecard;

import com.firstlook.util.Formatter;

public class TradeInScoreCardData extends ScoreCardData
{

private static final String PERCENT_NOWIN_WHOLESALE = "Percent Non-Winners Wholesaled";
private static final String RETAIL_AVERAGE_GROSS_PROFIT = "Retail Average Gross Profit";
private static final String PERCENT_SELL_THROUGH = "Percent Sell Through";
private static final String AVERAGE_DAYS_SALE = "Average Days to Retail Sale";
private static final String AVERAGE_INVENTORY_AGE = "Average Inventory Age";
private static final String IMMEDIATE_WHOLESALE_AVG_GROSS_PROFIT = "Immediate Wholesale Avg. Gross Profit";
private static final String AGED_INVENTORY_WHOLESALE_AVG_GROSS_PROFIT = "Aged Inventory Wholesale Avg. Gross Profit";

private double percentNonWinnersWholesaledTrend;
private double percentNonWinnersWholesaled12Week;;
private double retailAGPTrend;
private double retailAGP12Week;

private double avgDaysToSaleTrend;
private double avgDaysToSale12Week;

private double percentSellThroughTrend;
private double percentSellThrough12Week;

private double averageInventoryAgeCurrent;
private double averageInventoryAgePrior;

private double immediateWholesaleAvgGrossProfitTrend;
private double immediateWholesaleAvgGrossProfit12Week;

private double agedInventoryWholesaleAvgGrossProfitTrend;
private double agedInventoryWholesaleAvgGrossProfit12Week;

public TradeInScoreCardData()
{
    super();
}

public double getAvgDaysToSale12Week()
{
    return avgDaysToSale12Week;
}

public int getAvgDaysToSale12WeekFormatted()
{
    return (int) Math.round(avgDaysToSale12Week);
}

public Integer getAvgDaysToSaleTarget()
{
    return getTargetValue(AVERAGE_DAYS_SALE);
}

public double getAvgDaysToSaleTrend()
{
    return avgDaysToSaleTrend;
}

public int getAvgDaysToSaleTrendFormatted()
{
    return (int) Math.round(avgDaysToSaleTrend);
}

public Integer getAverageInventoryAgeTarget()
{
    return getTargetValue(AVERAGE_INVENTORY_AGE);
}

public double getPercentNonWinnersWholesaled12Week()
{
    return percentNonWinnersWholesaled12Week;
}

public String getPercentNonWinnersWholesaled12WeekFormatted()
{
    return Formatter.toPercent(percentNonWinnersWholesaled12Week);
}

public Integer getPercentNonWinnersWholesaledTarget()
{
    return getTargetValue(PERCENT_NOWIN_WHOLESALE);
}

public double getPercentNonWinnersWholesaledTrend()
{
    return percentNonWinnersWholesaledTrend;
}

public String getPercentNonWinnersWholesaledTrendFormatted()
{
    return Formatter.toPercent(percentNonWinnersWholesaledTrend);
}

public double getPercentSellThrough12Week()
{
    return percentSellThrough12Week;
}

public String getPercentSellThrough12WeekFormatted()
{
    return Formatter.toPercent(percentSellThrough12Week);
}

public Integer getPercentSellThroughTarget()
{
    return getTargetValue(PERCENT_SELL_THROUGH);
}

public double getPercentSellThroughTrend()
{
    return percentSellThroughTrend;
}

public String getPercentSellThroughTrendFormatted()
{
    return Formatter.toPrice(percentSellThroughTrend);
}

public double getRetailAGP12Week()
{
    return retailAGP12Week;
}

public String getRetailAGP12WeekFormatted()
{
    return Formatter.toPrice(retailAGP12Week);
}

public Integer getRetailAGPTarget()
{
    return getTargetValue(RETAIL_AVERAGE_GROSS_PROFIT);
}

public double getRetailAGPTrend()
{
    return retailAGPTrend;
}

public String getRetailAGPTrendFormatted()
{
    return Formatter.toPrice(retailAGPTrend);
}

public void setAvgDaysToSale12Week( double d )
{
    avgDaysToSale12Week = d;
}

public void setAvgDaysToSaleTrend( double d )
{
    avgDaysToSaleTrend = d;
}

public void setPercentNonWinnersWholesaled12Week( double d )
{
    percentNonWinnersWholesaled12Week = d;
}

public void setPercentNonWinnersWholesaledTrend( double d )
{
    percentNonWinnersWholesaledTrend = d;
}

public void setPercentSellThrough12Week( double d )
{
    percentSellThrough12Week = d;
}

public void setPercentSellThroughTrend( double d )
{
    percentSellThroughTrend = d;
}

public void setRetailAGP12Week( double i )
{
    retailAGP12Week = i;
}

public void setRetailAGPTrend( double i )
{
    retailAGPTrend = i;
}

public double getAgedInventoryWholesaleAvgGrossProfit12Week()
{
    return agedInventoryWholesaleAvgGrossProfit12Week;
}

public double getAgedInventoryWholesaleAvgGrossProfitTrend()
{
    return agedInventoryWholesaleAvgGrossProfitTrend;
}

public Integer getAgedInventoryWholesaleAvgGrossProfitTarget()
{
    return getTargetValue(AGED_INVENTORY_WHOLESALE_AVG_GROSS_PROFIT);
}

public double getAverageInventoryAgePrior()
{
    return averageInventoryAgePrior;
}

public double getAverageInventoryAgeCurrent()
{
    return averageInventoryAgeCurrent;
}

public double getImmediateWholesaleAvgGrossProfit12Week()
{
    return immediateWholesaleAvgGrossProfit12Week;
}

public double getImmediateWholesaleAvgGrossProfitTrend()
{
    return immediateWholesaleAvgGrossProfitTrend;
}

public Integer getImmediateWholesaleAvgGrossProfitTarget()
{
    return getTargetValue(IMMEDIATE_WHOLESALE_AVG_GROSS_PROFIT);
}

public void setAgedInventoryWholesaleAvgGrossProfit12Week( double d )
{
    agedInventoryWholesaleAvgGrossProfit12Week = d;
}

public void setAgedInventoryWholesaleAvgGrossProfitTrend( double d )
{
    agedInventoryWholesaleAvgGrossProfitTrend = d;
}

public void setAverageInventoryAgePrior( double d )
{
    averageInventoryAgePrior = d;
}

public void setAverageInventoryAgeCurrent( double d )
{
    averageInventoryAgeCurrent = d;
}

public void setImmediateWholesaleAvgGrossProfit12Week( double d )
{
    immediateWholesaleAvgGrossProfit12Week = d;
}

public void setImmediateWholesaleAvgGrossProfitTrend( double d )
{
    immediateWholesaleAvgGrossProfitTrend = d;
}

}
