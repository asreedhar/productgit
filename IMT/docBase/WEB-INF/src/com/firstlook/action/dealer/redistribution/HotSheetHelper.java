package com.firstlook.action.dealer.redistribution;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import biz.firstlook.commons.email.EmailService.EmailFormat;
import biz.firstlook.commons.enumeration.LightAndCIATypeDescriptor;
import biz.firstlook.commons.services.insights.Insight;
import biz.firstlook.commons.services.insights.InsightParameters;
import biz.firstlook.commons.services.insights.InsightUtils;
import biz.firstlook.commons.services.insights.PerformanceAnalysisBean;
import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.transact.persist.model.BookOut;
import biz.firstlook.transact.persist.model.BookOutThirdPartyCategory;
import biz.firstlook.transact.persist.model.BookOutValue;
import biz.firstlook.transact.persist.model.BookOutValueType;
import biz.firstlook.transact.persist.model.DemandDealer;
import biz.firstlook.transact.persist.model.MakeModelGrouping;
import biz.firstlook.transact.persist.model.SubscriptionDeliveryType;
import biz.firstlook.transact.persist.model.SubscriptionType;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.persistence.InGroupMemberWithSubscriptionDAO;
import biz.firstlook.transact.persist.retriever.MemberSubscriptionInfoBean;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.data.DatabaseException;
import com.firstlook.email.HotSheetEmailBuilder;
import com.firstlook.email.PhoneNumber;
import com.firstlook.email.PhoneNumberFormatException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.Member;
import com.firstlook.entity.ReportGrouping;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.action.ReportActionHelper;
import com.firstlook.persistence.inventory.FindGeneralGroupingRetriever;
import com.firstlook.service.dealer.IDealerService;
import com.firstlook.service.dealergroup.DealerGroupService;
import com.firstlook.service.inventory.InventoryService_Legacy;
import com.firstlook.service.member.IMemberService;
import com.firstlook.thirdparty.book.blackbook.BlackBookCategoryComparator;
import com.firstlook.thirdparty.book.kbb.KBBCategoryComparator;
import com.firstlook.thirdparty.book.nada.NADACategoryComparator;

public class HotSheetHelper implements ApplicationContextAware
{

private static final Logger logger = Logger.getLogger( HotSheetHelper.class );
private IAppraisalService appraisalService;
private DealerGroupService dealerGroupService;
private FindGeneralGroupingRetriever findGeneralGroupingRetriever;
private IDealerService dealerService;
private IMemberService memberService;
private InGroupMemberWithSubscriptionDAO inGroupMemberWithSubscriptionDAO;
private InventoryService_Legacy inventoryService_Legacy;

private NullUtil nullUtil;

/**
 * The faxMail address for sending faxes. This should probably live in EmailService.
 */
private String faxMailHost;

private ApplicationContext applicationContext;

public HotSheetHelper()
{
	nullUtil = new NullUtil( "N/A" );
}

public HotSheetEmailBuilder getEmail( final Integer currentDealerId, final Integer memberId, final Integer year,
										final Integer groupingDescriptionId, final Integer mileage, final Integer appraisalId,
										final List demandDealers )
{
	logger.debug( "Starting HotSheet Context generation." );
	Dealer currentDealer = dealerService.retrieveDealer( currentDealerId.intValue() );
	DealerGroup dealerGroup = dealerGroupService.retrieveByDealerId( currentDealerId.intValue() );
	Member currentMember = memberService.retrieveMember( memberId );
	IAppraisal appraisal = appraisalService.findBy( appraisalId );

	Vehicle vehicle = appraisal.getVehicle();

	Map< String, Map< String, Object > > recipients = new HashMap< String, Map< String, Object > >();

	Map< String, Object > hotSheetContext = new HashMap< String, Object >();
	hotSheetContext.put( "dealerGroupName", StringUtils.defaultString( dealerGroup.getName() ) );
	hotSheetContext.put( "date", new Date() );

	Map< String, Object > appraisalContext = new HashMap< String, Object >();
	appraisalContext.put( "price", nullUtil.replaceIfNull( appraisal.getWholesalePrice() ) );
	appraisalContext.put( "estimatedRecon", StringUtils.defaultString( appraisal.getEstimatedReconditioningCost().toString() ) );
	appraisalContext.put( "mileage", nullUtil.replaceIfNull( appraisal.getMileage() ) );
	appraisalContext.put( "color", StringUtils.defaultString( appraisal.getColor() ) );
	appraisalContext.put( "conditionDescription", StringUtils.defaultString( appraisal.getConditionDescription() ) );

	Map< String, Object > vehicleModelVC = new HashMap< String, Object >();
	vehicleModelVC.put( "year", nullUtil.replaceIfNull( vehicle.getVehicleYear().toString() ) );
	vehicleModelVC.put( "make", StringUtils.defaultString( vehicle.getMake() ) );
	vehicleModelVC.put( "model", StringUtils.defaultString( vehicle.getModel() ) );
	vehicleModelVC.put( "trim", StringUtils.defaultString( vehicle.getVehicleTrim() ) );
	vehicleModelVC.put( "vin", StringUtils.defaultString( vehicle.getVin() ) );

	appraisalContext.put( "vehicle", nullUtil.replaceIfNull( vehicleModelVC ) );

	hotSheetContext.put( "appraisal", appraisalContext );

	Map< Integer, Map< String, Object > > demandDealerCache = buildDemandDealerContextCache( year, mileage, demandDealers, vehicle );
	
	final BookOut primaryBook_BookOut = appraisal.getLatestBookOut( currentDealer.getDealerPreference().getGuideBookId() );

	Map< String, Object > guideBook = new HashMap< String, Object >();
	guideBook.put( "name", nullUtil.replaceIfNull( ( primaryBook_BookOut != null ? primaryBook_BookOut.getThirdPartyDescription() : null ) ) );
	guideBook.put(
					"footer",
					StringUtils.defaultString( ( primaryBook_BookOut != null ? primaryBook_BookOut.getThirdPartyDataProvider().getDefaultFooter()
							: null ) ) );

	List< Map< String, Object > > guideBookValues = new ArrayList< Map< String, Object > >();
	Collection< BookOutThirdPartyCategory > bookOutThirdPartyCategories = ( primaryBook_BookOut != null ? primaryBook_BookOut.getBookOutThirdPartyCategories()
			: new ArrayList< BookOutThirdPartyCategory >() );
	for ( BookOutThirdPartyCategory bookOutThirdPartyCategory : bookOutThirdPartyCategories )
	{
		Set< BookOutValue > bookOutValues = bookOutThirdPartyCategory.getBookOutValues();
		if ( bookOutValues != null )
		{
			for ( BookOutValue bookOutValue : bookOutValues )
			{
				if ( bookOutValue.getBookOutValueType().getBookOutValueTypeId().intValue() == BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE )
				{
					Map< String, Object > guideBookValue = new LinkedHashMap< String, Object >();
					String tpcTitle = bookOutThirdPartyCategory.getThirdPartyCategory().getCategory();
					guideBookValue.put( "category", StringUtils.defaultString( tpcTitle ) );
					guideBookValue.put( "categoryPrice", nullUtil.replaceIfNull( bookOutValue.getValue() ) );
					guideBookValues.add( guideBookValue );
				}
			}
		}
	}

	if ( primaryBook_BookOut != null )
	{
		Comparator< Map > comparator = null;
		switch ( primaryBook_BookOut.getThirdPartyId().intValue() )
		{
			case ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE:
				comparator = new BlackBookCategoryComparator< Map >();
				break;
			case ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE:
				comparator = new NADACategoryComparator< Map >();
				break;
			case ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE:
				comparator = new KBBCategoryComparator< Map >();
				break;
			case ThirdPartyDataProvider.GUIDEBOOK_GALVES_TYPE:
				comparator = null;
				break;
			default:
				comparator = null;
		}

		if ( comparator != null )
			Collections.sort( guideBookValues, comparator );
	}
	guideBook.put( "guideBookValues", guideBookValues );

	hotSheetContext.put( "guideBook", guideBook );

	Map< String, String > contactInfo = new HashMap< String, String >();
	contactInfo.put( "seller", StringUtils.defaultString( currentDealer.getName() ) );
	contactInfo.put( "contact", StringUtils.defaultString( currentMember.getFirstName() ) );
	contactInfo.put( "telephone", StringUtils.defaultString( currentMember.getOfficePhoneNumber() ) );
	contactInfo.put( "email", StringUtils.defaultString( currentMember.getEmailAddress() ) );

	hotSheetContext.put( "contactInformation", contactInfo );

	List< MemberSubscriptionInfoBean > inGroupMemberWithSubscriptions = inGroupMemberWithSubscriptionDAO.getInGroupMembersWithSubscription(
																																			currentDealerId,
																																			SubscriptionType.HOT_SHEETZ );
	Map< String, EmailFormat > emailFormats = new HashMap< String, EmailFormat >();
	for ( MemberSubscriptionInfoBean memberInfo : inGroupMemberWithSubscriptions )
	{
		boolean isFax = false;
		StringBuffer emailInfo = new StringBuffer();
		if ( memberInfo.getDeliveryType().equals( SubscriptionDeliveryType.FAX ) )
		{
			isFax = true;
			PhoneNumber faxNumber = null;
			try
			{
				faxNumber = PhoneNumber.parse( memberInfo.getContactInformation() );
				emailInfo.append( faxNumber.toString() ).append( "@" ).append( faxMailHost );
				emailFormats.put( emailInfo.toString(), EmailFormat.HTML_EXTERNAL_LINKS );
			}
			catch ( PhoneNumberFormatException e )
			{
				logger.error( "Error trying to construct faxmail address: ", e );
			}
		}
		else
		{
			isFax = false;
			emailInfo.append( memberInfo.getContactInformation() );
			if ( memberInfo.getDeliveryType().equals( SubscriptionDeliveryType.TEXT_E_MAIL ) )
			{
				emailFormats.put( emailInfo.toString(), EmailFormat.PLAIN_TEXT );
			}
			else
			{
				emailFormats.put( emailInfo.toString(), EmailFormat.HTML_MULTIPART );
			}
		}
		
		hotSheetContext.put( "isFax", isFax );

		// For this member, overwrite the key demandDealer for the store they are associated with.
		Map< String, Object > demandDealerSection = demandDealerCache.get( memberInfo.getBusinessUnitId() );
		if ( demandDealerSection != null )
		{
			if( isFax )
			{
				//overwrite resources to external links for EasyLink faxmail service.
				Map< String, Object> firstlookBottomLine =  (Map< String, Object>)demandDealerSection.get( "firstlookBottomLine" );
				Integer lightEnum = (Integer)firstlookBottomLine.get( "lightEnum" );
				switch( lightEnum.intValue() )
				{
					case LightAndCIATypeDescriptor.RED_LIGHT:
						firstlookBottomLine.put( "light", PropertyLoader.getProperty( "firstlook.hotsheet.fax.light.red" ) );
						break;
					case LightAndCIATypeDescriptor.YELLOW_LIGHT:
						firstlookBottomLine.put( "light", PropertyLoader.getProperty( "firstlook.hotsheet.fax.light.yellow" ) );
						break;
					case LightAndCIATypeDescriptor.GREEN_LIGHT:
						firstlookBottomLine.put( "light", PropertyLoader.getProperty( "firstlook.hotsheet.fax.light.green" ) );
						break;
					default:
						firstlookBottomLine.put( "light", PropertyLoader.getProperty( "firstlook.hotsheet.fax.light.yellow" ) );
						break;
				}
				
				hotSheetContext.put( "fldnLogoGIF", PropertyLoader.getProperty( "firstlook.hotsheet.fax.logo" ) );
			}
			else
			{
				hotSheetContext.put( "fldnLogoGIF", "cid:fldnLogoGIF" );
			}
			
			//a hotsheet for a user contains specific information for that user and store they belong to.
			Map< String, Object > userHotsheet = new HashMap<String, Object>( hotSheetContext );
			userHotsheet.put( "demandDealer", demandDealerSection );

			recipients.put( emailInfo.toString(), userHotsheet );
		}

	}

	logger.debug( recipients );
	logger.debug( "Finished HotSheet generation." );

	HotSheetEmailBuilder email = new HotSheetEmailBuilder( recipients, emailFormats );
	//Inline the CSS in the template file.  Mail readers and faxmail don't like external sheets.
	//email.putResource( "styleSheet", applicationContext.getResource( "css/faxSheet.css" ), "text/css" );
	email.putResource( "fldnLogoGIF", applicationContext.getResource( "images/fax/fldn_logo.gif" ), "image/gif" );
	email.putResource( "trafficLight1", applicationContext.getResource( "images/tools/stoplight_31x80_red.gif" ), "image/gif" );
	email.putResource( "trafficLight2", applicationContext.getResource( "images/tools/stoplight_31x80_yellow.gif" ), "image/gif" );
	email.putResource( "trafficLight3", applicationContext.getResource( "images/tools/stoplight_31x80_green.gif" ), "image/gif" );

	return email;
}

/**
 * Builds the context for each dealer to recieve the hotsheet.
 * 
 * @param year
 * @param mileage
 * @param demandDealers
 * @param vehicle
 * @param isFax
 * @return
 */
private Map< Integer, Map< String, Object > > buildDemandDealerContextCache( final Integer year, final Integer mileage,
																				final List<DemandDealer> demandDealers, Vehicle vehicle )
{
	InventoryEntity inventoryEntity = createEmptyInventoryWithYearMileageMakeModelUsingVin( year, mileage, vehicle.getVin(), vehicle.getMake(),
																							vehicle.getModel(), vehicle.getMakeModelGrouping() );

	Map< Integer, Map< String, Object > > sisterStores = new HashMap< Integer, Map< String, Object > >();
	Iterator<DemandDealer> demandDealersIter = demandDealers.iterator();
	while ( demandDealersIter.hasNext() )
	{
		DemandDealer demandDealer = demandDealersIter.next();

		sisterStores.put( demandDealer.getBusinessUnitId(), buildPerformanceSection( demandDealer, vehicle.getMake(), vehicle.getModel(),
																						vehicle.getVehicleTrim(), inventoryEntity,
																						ReportActionHelper.DEFAULT_NUMBER_OF_WEEKS ) );
	}

	return sisterStores;
}

/**
 * HACK. Take a look at the glory of PrintableFlashReportDisplayAction. -bf.
 * 
 * @param year
 * @param mileage
 * @param vin
 * @param make
 * @param model
 * @param makeModelGrouping
 * @return
 * @throws DatabaseException
 * @throws ApplicationException
 */
private InventoryEntity createEmptyInventoryWithYearMileageMakeModelUsingVin( Integer year, Integer mileage, String vin, String make,
																				String model, MakeModelGrouping makeModelGrouping )
{
	InventoryEntity inventory = new InventoryEntity();
	if ( year != null )
	{
		inventory.setVehicleYear( year.intValue() );
	}
	else
	{
		inventory.setVehicleYear( 0 );
	}
	inventory.setMileageReceived( mileage );
	if ( makeModelGrouping != null )
	{
		inventory.setMakeModelGroupingId( makeModelGrouping.getMakeModelGroupingId() );
		inventory.setMakeModelGrouping( makeModelGrouping );
	}

	return inventory;
}

/**
 * More PrintableFlashReportAction glory. -bf.
 * 
 * @param dealer
 * @param make
 * @param model
 * @param trim
 * @param inventoryEntity
 * @param reportPeriodInWeeks
 * @param isFax
 */
private Map< String, Object > buildPerformanceSection( DemandDealer dealer, String make, String model, String trim,
														InventoryEntity inventoryEntity, int reportPeriodInWeeks )
{
	int dealerId = dealer.getBusinessUnitId();
	int dealerGroupInclude = 0;
	int inventoryType = InventoryEntity.USED_CAR;

	ReportGrouping reportGroupingSpecific = findGeneralGroupingRetriever.findByGroupingDescription( make, model, trim, dealerGroupInclude,
																									dealerId, reportPeriodInWeeks,
																									inventoryType );

	ReportGrouping reportGroupingGeneral = findGeneralGroupingRetriever.findByGroupingDescription( make, model, null, dealerGroupInclude,
																									dealerId, reportPeriodInWeeks,
																									inventoryType );

	List<Insight> insights = InsightUtils.getInsights(new InsightParameters(dealerId, inventoryEntity.getGroupingDescriptionId(), inventoryType, reportPeriodInWeeks, trim, inventoryEntity.getVehicleYear(), inventoryEntity.getMileageReceived()));
	PerformanceAnalysisBean performanceAnalysisBean = new PerformanceAnalysisBean(insights);

	int unitsInStockSpecific = inventoryService_Legacy.retrieveUnitsInStockByDealerIdOrDealerGroup( dealerId, dealerGroupInclude, trim, inventoryType,
																								make, model );

	int unitsInStockGeneral = inventoryService_Legacy.retrieveUnitsInStockByDealerIdOrDealerGroup( dealerId, dealerGroupInclude, null, inventoryType,
																							make, model );

	Map< String, Object > emailedStoreInfo = new HashMap< String, Object >();
	emailedStoreInfo.put( "dealerName", StringUtils.defaultString( dealer.getName() ) );

	Map< String, Object > trimPerformance = new HashMap< String, Object >();
	trimPerformance.put( "vehicleDescription", make + " " + model + " " + trim );
	trimPerformance.put( "averageGrossProfit", nullUtil.replaceIfNull( reportGroupingSpecific.getAvgGrossProfit() ) );
	trimPerformance.put( "unitsSold", nullUtil.replaceIfNull( reportGroupingSpecific.getUnitsSold() ) );
	trimPerformance.put( "averageDaysToSale", nullUtil.replaceIfNull( reportGroupingSpecific.getAvgDaysToSale() ) );
	trimPerformance.put( "averageMileage", nullUtil.replaceIfNull( reportGroupingSpecific.getAvgMileage() ) );
	trimPerformance.put( "noSales", nullUtil.replaceIfNull( reportGroupingSpecific.getNoSales() ) );
	trimPerformance.put( "unitsInStock", nullUtil.replaceIfNull( unitsInStockSpecific ) );
	emailedStoreInfo.put( "trimPerformance", trimPerformance );
	StringBuffer performanceSectionXML = new StringBuffer();
	performanceSectionXML.append( "<performanceInfo weeks=\"" ).append( reportPeriodInWeeks ).append( "\" />" );

	Map< String, Object > groupingDescriptionPerformance = new HashMap< String, Object >();
	groupingDescriptionPerformance.put( "vehicleDescription", make + " " + model );
	groupingDescriptionPerformance.put( "averageGrossProfit", nullUtil.replaceIfNull( reportGroupingGeneral.getAvgGrossProfit() ) );
	groupingDescriptionPerformance.put( "unitsSold", nullUtil.replaceIfNull( reportGroupingGeneral.getUnitsSold() ) );
	groupingDescriptionPerformance.put( "averageDaysToSale", nullUtil.replaceIfNull( reportGroupingGeneral.getAvgDaysToSale() ) );
	groupingDescriptionPerformance.put( "averageMileage", nullUtil.replaceIfNull( reportGroupingGeneral.getAvgMileage() ) );
	groupingDescriptionPerformance.put( "noSales", nullUtil.replaceIfNull( reportGroupingGeneral.getNoSales() ) );
	groupingDescriptionPerformance.put( "unitsInStock", nullUtil.replaceIfNull( unitsInStockGeneral ) );
	emailedStoreInfo.put( "groupingDescriptionPerformance", groupingDescriptionPerformance );

	Map< String, Object > firstlookBottomLine = new HashMap< String, Object >();
	
	String lightContentString = "cid:trafficLight" + performanceAnalysisBean.getLight();
	// ok hack to determine traffic light picture. - bf.
	firstlookBottomLine.put( "light", lightContentString );
	firstlookBottomLine.put( "lightEnum", new Integer( performanceAnalysisBean.getLight() ) );
	firstlookBottomLine.put( "descriptors", nullUtil.replaceIfNull( performanceAnalysisBean.getDescriptors(), "" ) );

	emailedStoreInfo.put( "firstlookBottomLine", firstlookBottomLine );

	return emailedStoreInfo;
}

/**
 * Mini hack.
 * 
 * @author bfung
 * 
 */
private class NullUtil
{

private final Object replaceWith;

public NullUtil( final Object replaceWith )
{
	this.replaceWith = replaceWith;
}

public Object replaceIfNull( Object object )
{
	return object == null ? replaceWith : object;
}

public Object replaceIfNull( Object object, Object replacement )
{
	return object == null ? replacement : object;
}

}

public IAppraisalService getAppraisalService()
{
	return appraisalService;
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

public DealerGroupService getDealerGroupService()
{
	return dealerGroupService;
}

public void setDealerGroupService( DealerGroupService dealerGroupService )
{
	this.dealerGroupService = dealerGroupService;
}

public void setImtDealerService( IDealerService dealerService )
{
	this.dealerService = dealerService;
}

public IMemberService getMemberService()
{
	return memberService;
}

public void setMemberService( IMemberService memberService )
{
	this.memberService = memberService;
}

public FindGeneralGroupingRetriever getFindGeneralGroupingRetriever()
{
	return findGeneralGroupingRetriever;
}

public void setFindGeneralGroupingRetriever( FindGeneralGroupingRetriever findGeneralGroupingRetriever )
{
	this.findGeneralGroupingRetriever = findGeneralGroupingRetriever;
}

public InGroupMemberWithSubscriptionDAO getInGroupMemberWithSubscriptionDAO()
{
	return inGroupMemberWithSubscriptionDAO;
}

public void setInGroupMemberWithSubscriptionDAO( InGroupMemberWithSubscriptionDAO inGroupMemberWithSubscriptionDAO )
{
	this.inGroupMemberWithSubscriptionDAO = inGroupMemberWithSubscriptionDAO;
}

public void setInventoryService_Legacy( InventoryService_Legacy inventoryService )
{
	this.inventoryService_Legacy = inventoryService;
}

public void setApplicationContext( ApplicationContext applicationContext ) throws BeansException
{
	this.applicationContext = applicationContext;
}

public void setFaxMailHost( String faxMailHost )
{
	this.faxMailHost = faxMailHost;
}

}
