package com.firstlook.action.dealer.tools;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * This class start out as a BaseManualBookoutAction to put common functionality from NADAManualBookoutAction and KBBManualBookoutAction into a
 * parent class, however, a common api for manual bookout is unknown; the api's are currently book specific.
 * 
 * Refactor when we have time.
 * 
 * @author bfung
 * 
 */
public class ManualBookoutUtils
{

private static ManualBookoutUtils instance = new ManualBookoutUtils();

private static List< Integer > years;

private int initializedYear;

private ManualBookoutUtils()
{
	initializedYear = Calendar.getInstance().get( Calendar.YEAR );
	years = generate( initializedYear );
}

public static ManualBookoutUtils getInstance()
{
	return instance;
}

/**
 * Generates a list of years given the current year.
 * 
 * @param currentYear
 * @return A list of years in order of most current to least current. The starting year will be 1 year after <code>currentYear</code> and
 *         ending year will be 20 years after the starting year.
 */
private static List< Integer > generate( int currentYear )
{
	List< Integer > years = new ArrayList< Integer >( 20 );
	int startYear = currentYear + 1 - 20;
	int endYear = currentYear + 1;
	for ( int i = endYear; i > startYear; i-- )
	{
		years.add( Integer.valueOf( i ) );
	}
	return years;
}

/**
 * Checks to see if the list of years makes sense with the current calendar year and generates a new list if necessary.
 * 
 * @return list of years
 * @see #generate(int)
 */
protected synchronized List< Integer > getYears()
{
	int currentYear = Calendar.getInstance().get( Calendar.YEAR );
	if ( initializedYear != currentYear )
	{
		initializedYear = currentYear;
		years = generate( currentYear );
	}
	return years;
}

}
