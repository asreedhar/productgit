package com.firstlook.action.dealer.tools;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import biz.firstlook.commons.util.BasicAuthAdapter;
import biz.firstlook.services.photos.PhotosServiceClient;
import biz.firstlook.services.photos.entity.PhotosUploadResponse;
import biz.firstlook.transact.persist.service.appraisal.AppraisalPhotoKeys;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.form.PhotoUploadForm;
import com.firstlook.exception.ApplicationException;
import com.google.gson.Gson;

public class SavePhotosAction extends SecureBaseAction {

	IAppraisalService appaisalService;
	
	
	
	public IAppraisalService getAppraisalService() {
		return appaisalService;
	}

	public void setAppraisalService(IAppraisalService appaisalService) {
		this.appaisalService = appaisalService;
	}
	
	@Override
	public ActionForward doIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws DatabaseException, ApplicationException {
	
		System.out.println("elo");
		PhotoUploadForm photoUploadForm= (PhotoUploadForm) form;
		
		FormFile imageFile= photoUploadForm.getImageFile();
		
		

		
		byte[] encoded=null;
		try {
		
			encoded = Base64.encodeBase64(imageFile.getFileData());
		
		
		
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		String encodedFile = new String(encoded);
		String fileName=imageFile.getFileName();
		
		String basicAuthString = (String) request.getSession().getServletContext().getAttribute(BasicAuthAdapter.NAME_IN_SESSION);
		
		
		Context initialContext;
		try {
			initialContext = new javax.naming.InitialContext();
			Context ctx = (Context) initialContext.lookup("java:comp/env");
			String user= (String) ctx.lookup("userNameForAdmin");
			String password= (String) ctx.lookup("passwordForAdmin");
			
			
			BasicAuthAdapter baa = new BasicAuthAdapter(user, password);
			basicAuthString= baa.basicAuthString();	
			
		
		} catch (NamingException e1) {
			e1.printStackTrace();
		} 

		try {
			Gson gson= new Gson();
			PhotosServiceClient psc= new PhotosServiceClient(basicAuthString);
			
			//psc.getPhotos(getFirstlookSessionFromRequest(request).getCurrentDealerId()+"", request.getParameter("appraisalId"));
			PhotosUploadResponse resp=psc.uploadPhotos(getFirstlookSessionFromRequest(request).getCurrentDealerId()+"", request.getParameter("appraisalId"), 1, fileName, (photoUploadForm.getSequenceNumber()), encodedFile);
			
			response.setContentType("text/html");
			response.getWriter().write(gson.toJson(resp));
			
			
			IAppraisal appraisal=appaisalService.findBy(Integer.parseInt(request.getParameter("appraisalId")));
			
			if(appraisal.getAppraisalPhotoKeys()==null){
				appraisal.setAppraisalPhotoKeys(((List<AppraisalPhotoKeys>)new ArrayList<AppraisalPhotoKeys>()));
			}
			if(appraisal!=null&&resp!=null&&resp.getThumbnailKey()!=null)
			appraisal.getAppraisalPhotoKeys().add(new AppraisalPhotoKeys(appraisal, resp.getThumbnailKey(), resp.getPhotoKey(),photoUploadForm.getSequenceNumber()));
			appaisalService.save(appraisal);
			
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		

		return null;
	}
	
	 
}
