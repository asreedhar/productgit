package com.firstlook.action.dealer.reports;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.cia.service.BasisPeriodService;
import biz.firstlook.commons.services.vehiclehistoryreport.CarfaxException;
import biz.firstlook.commons.services.vehiclehistoryreport.CarfaxService;
import biz.firstlook.transact.persist.model.CIAPreferences.TimePeriod;

import com.discursive.cas.extend.client.CASFilter;
import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.dealer.helper.AgingReportHelper;
import com.firstlook.action.dealer.helper.DealerFactsHelper;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryBucket;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.InventorySalesAggregate;
import com.firstlook.entity.InventoryStatusCD;
import com.firstlook.entity.lite.InventoryLite;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.inventory.InventorySalesAggregateService;
import com.firstlook.persistence.report.IInventoryBucketDAO;
import com.firstlook.report.BaseAgingReport;
import com.firstlook.service.dealer.IUCBPPreferenceService;
import com.firstlook.service.inventory.InventorySalesAggregateHelper;
import com.firstlook.service.inventory.InventoryService_Legacy;
import com.firstlook.service.inventory.InventoryStatusCDService;
import com.firstlook.service.member.MemberToInventoryStatusFilterCodeService;
import com.firstlook.session.FirstlookSession;

public class TotalInventoryReportDisplayAction extends SecureBaseAction
{

protected InventoryService_Legacy inventoryService_Legacy;
private BasisPeriodService basisPeriodService;
private InventorySalesAggregateService inventorySalesAggregateService;
private IInventoryBucketDAO inventoryBucketDAO;
private IUCBPPreferenceService ucbpPreferenceService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	String[] statusCodes = null;
	Integer memberId = getMemberFromRequest( request ).getMemberId();
	String inventoryStatusCdStr = request.getParameter( "inventoryStatusCdStr" );
	String inventoryStatusCodeStr = "";
	FirstlookSession firstlookSession = getFirstlookSessionFromRequest( request );
	int currentDealerId = firstlookSession.getCurrentDealerId();

	Dealer currentDealer = putDealerFormInRequest( request, 0 );

	InventoryStatusCDService inventoryStatusCDService = new InventoryStatusCDService();
	MemberToInventoryStatusFilterCodeService inventoryStatusFilterCodeService = new MemberToInventoryStatusFilterCodeService();

	if ( currentDealer.getDealerPreference().isShowLotLocationStatus() )
	{
		statusCodes = inventoryStatusFilterCodeService.determineSelectedStatusCodes(
																						memberId,
																						form,
																						inventoryStatusCdStr,
																						statusCodes,
																						inventoryStatusCDService,
																						currentDealer.getDealerPreference().isShowLotLocationStatus() );
	}
	int weeks = basisPeriodService.calculateNumberOfWeeksInBasisPeriodByType( currentDealerId, TimePeriod.SALES_HISTORY_DISPLAY, 0 );

	int inventoryType = getMemberFromRequest( request ).getInventoryType().getValue();

	Integer rangeSetId = null;
	if ( inventoryType == InventoryEntity.USED_CAR )
	{
		rangeSetId = BaseAgingReport.USED_TOTAL_INVENTORY_REPORT_RANGE_SET;
	}
	else
	{
		rangeSetId = BaseAgingReport.NEW_TOTAL_INVENTORY_REPORT_RANGE_SET;
	}

	InventoryBucket rangeSet = inventoryBucketDAO.findByRangeSetId( rangeSetId );
	InventorySalesAggregate aggregate = inventorySalesAggregateService.retrieveCachedInventorySalesAggregate(
																												currentDealer.getDealerId().intValue(),
																												weeks, 0, inventoryType );
	
	String memberUserName = (String) request.getSession(false).getAttribute(CASFilter.CAS_FILTER_USER);

	AgingReportHelper agingReportHelper = new AgingReportHelper(inventoryService_Legacy);
	agingReportHelper.putAgingReportRangesInRequest( currentDealer, request, InventoryLite.FALSE, statusCodes, rangeSet, aggregate, memberUserName );

	DealerFactsHelper dealerFactsHelper = new DealerFactsHelper( getDealerFactsDAO() );
	dealerFactsHelper.putVehicleMaxPolledDateInRequest( currentDealerId, request, inventoryType );

	InventorySalesAggregateHelper.putMakeModelCountInRequest( request, aggregate );
	InventorySalesAggregateHelper.putTotalDaysSupplyInRequest( request, aggregate );
	InventorySalesAggregateHelper.putDealerTotalDollarsInRequest( request, aggregate );

	Integer mileage = determineMileage( currentDealerId );
	request.setAttribute( "mileage", mileage );
	request.setAttribute( "weeks", new Integer( weeks ) );

	if ( currentDealer.getDealerPreference().isShowLotLocationStatus() )
	{
		List<InventoryStatusCD> status = inventoryStatusCDService.determineStatusList();

		InventoryStatusFilterForm inventoryStatusFilterForm = new InventoryStatusFilterForm();
		inventoryStatusFilterForm.setInventoryStatusCD( statusCodes );
		request.setAttribute( "inventoryStatusFilterForm", inventoryStatusFilterForm );
		request.setAttribute( "statusCollection", status );
		inventoryStatusCodeStr = inventoryStatusFilterForm.getInventoryStatusCDStr();
	}

	request.setAttribute( "displayStatusSelect", new Boolean( currentDealer.getDealerPreference().isShowLotLocationStatus() ) );
	request.setAttribute( "inventoryStatusCdStr", inventoryStatusCodeStr );
	request.setAttribute( "printRef", "PrintableTotalInventoryReportDisplayAction.go" );
	
	try {
		Map<String,String> properties = CarfaxService.getInstance().getCarfaxReportProperties(currentDealerId, memberUserName);
		for (Map.Entry<String, String> entry : properties.entrySet()) {
			request.setAttribute(entry.getKey(), entry.getValue());
		}
	} catch (CarfaxException ce) {
		request.setAttribute("hasCarfaxError", true);
		request.setAttribute("problem", ce.getResponseCode());
	}
	
	return mapping.findForward( "success" );
}

private Integer determineMileage( int currentDealerId ) throws ApplicationException
{
	return new Integer( ucbpPreferenceService.retrieveDealerRisk( currentDealerId ).getHighMileageThreshold());
}

public void setInventoryService_Legacy( InventoryService_Legacy inventoryService )
{
	this.inventoryService_Legacy = inventoryService;
}

public void setBasisPeriodService( BasisPeriodService basisPeriodService )
{
	this.basisPeriodService = basisPeriodService;
}

public void setInventorySalesAggregateService( InventorySalesAggregateService inventorySalesAggregateService )
{
	this.inventorySalesAggregateService = inventorySalesAggregateService;
}

public void setInventoryBucketDAO( IInventoryBucketDAO inventoryBucketDAO )
{
	this.inventoryBucketDAO = inventoryBucketDAO;
}

public void setUcbpPreferenceService( IUCBPPreferenceService ucbpPreferenceService )
{
	this.ucbpPreferenceService = ucbpPreferenceService;
}

}
