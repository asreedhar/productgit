package com.firstlook.action.dealer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;

public class RedistributionCenterDisplayAction extends SecureBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	Member member = getMemberFromRequest( request );
	
    if( member.getMemberRoleTester().isUsedAppraiser() ||
        member.getMemberRoleTester().isUsedNoAccess() ) {
        request.setAttribute("redistributionCenterEnabled", new Boolean(false) );
    } else {
        request.setAttribute("redistributionCenterEnabled", new Boolean(true) );
    }

    return mapping.findForward( "success" );
}

}
