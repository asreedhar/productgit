package com.firstlook.action.dealer.tools;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import antlr.collections.Enumerator;
import biz.firstlook.cia.service.BasisPeriodService;
import biz.firstlook.commons.services.vehiclehistoryreport.AutoCheckException;
import biz.firstlook.commons.services.vehiclehistoryreport.AutoCheckService;
import biz.firstlook.commons.services.vehiclehistoryreport.CarfaxException;
import biz.firstlook.commons.services.vehiclehistoryreport.CarfaxService;
import biz.firstlook.services.vehiclehistoryreport.carfax.VehicleEntityType;
import biz.firstlook.transact.persist.model.BookOutSource;
import biz.firstlook.transact.persist.model.DemandDealer;
import biz.firstlook.transact.persist.model.GroupingDescription;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.CIAPreferences.TimePeriod;
import biz.firstlook.transact.persist.service.MakeModelGroupingService;
import biz.firstlook.transact.persist.service.appraisal.AppraisalActionType;
import biz.firstlook.transact.persist.service.appraisal.AppraisalFormOptions;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.BaseActionForm;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.GroupingForm;
import com.firstlook.entity.form.TradeAnalyzerForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.ReportGroupingRequestHelper;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.SessionHelper;
import com.firstlook.helper.action.ReportActionHelper;
import com.firstlook.report.Report;
import com.firstlook.service.groupingdescription.GroupingDescriptionService;
import com.firstlook.session.FirstlookSession;
import com.firstlook.util.PageBreakHelper;

public class TradeAnalyzerSubmitAction extends AbstractTradeAnalyzerFormAction
{

private BasisPeriodService basisPeriodService;
private MakeModelGroupingService makeModelGroupingService;
private IAppraisalService appraisalService;
private ReportGroupingRequestHelper reportGroupingRequestHelper;

protected ActionForward analyzeIt(ActionMapping mapping, TradeAnalyzerForm tradeAnalyzerForm,	HttpServletRequest request, HttpServletResponse response, Dealer dealer)
		throws DatabaseException, ApplicationException
{
	putActionErrorsInRequest( request, tradeAnalyzerForm.validate( mapping, request ) );

	IAppraisal appraisal = appraisalService.findBy( tradeAnalyzerForm.getAppraisalId() );
	if ( appraisal != null )
	{
		populateTradeAnalyzerForm( appraisal, tradeAnalyzerForm );
		// there was a previous appraisal, attempt to set Initial Appraisal Value
		if ( tradeAnalyzerForm.getAppraisalValue() == null )
		{
			if ( appraisal.getAppraisalValues() != null && !appraisal.getAppraisalValues().isEmpty() )
			{
				tradeAnalyzerForm.setAppraisalValue( appraisal.getAppraisalValues().get( 0 ).getValue() );
			}
		}
		request.setAttribute( "locked", appraisal.isBookoutLocked() );
	}

	checkDealerGroupInclude( request, tradeAnalyzerForm );
	int currentDealerId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();
	int weeks = basisPeriodService.calculateNumberOfWeeksInBasisPeriodByType( new Integer( currentDealerId ), TimePeriod.SALES_HISTORY_DISPLAY,
																				0 );

	request.setAttribute( "currentDealerId", Integer.valueOf( currentDealerId ) );
	request.setAttribute( "dealerGroupId", getDealerGroupService().retrieveByDealerId( currentDealerId ) );

	tradeAnalyzerForm.setAppraisalRequirement( dealer.getDealerPreference().getAppraisalRequirementLevel() );
	tradeAnalyzerForm.setRequireNameOnAppraisals(         dealer.getDealerPreference().getRequireNameOnAppraisals());
	tradeAnalyzerForm.setRequireEstReconCostOnAppraisals( dealer.getDealerPreference().getRequireEstReconCostOnAppraisals() );		
	tradeAnalyzerForm.setRequireReconNotesOnAppraisals(   dealer.getDealerPreference().getRequireReconNotesOnAppraisals() );


	Member member = getMemberFromRequest( request );

	reportGroupingRequestHelper.putReportGroupingFormsInRequest( request, tradeAnalyzerForm, weeks );

	GroupingDescriptionService groupingDescriptionService = new GroupingDescriptionService();
	GroupingDescription groupingDescription = groupingDescriptionService.retrieveByMakeModel( tradeAnalyzerForm.getMake(),
																								tradeAnalyzerForm.getModel() );
	// GroupingDescription groupingDescription = groupingDescriptionService.retrieveById(tradeAnalyzerForm.getMakeModelGroupingId());

	putGroupingDescriptionFormInRequest( request, groupingDescription );
	int groupingId = groupingDescription.getGroupingDescriptionId().intValue();
	setIncludeDealerGroup( tradeAnalyzerForm, request );

	Boolean includeBackEndGrossProfit = dealer.getDealerPreference().getIncludeBackEndInValuation();
	request.setAttribute( "includeBackEndInValuation", includeBackEndGrossProfit );

	putPageBreakHelperInRequest( PageBreakHelper.TA_ONLINE, request, getMemberFromRequest( request ).getProgramType() );
	ReportActionHelper.putNumberOfDealerPagesInRequest( request );

	List<DemandDealer> demandDealers = retrieveDemandDealers( tradeAnalyzerForm, groupingId, dealer );
	boolean hasDemandDealers = false;
	if ( demandDealers != null && !demandDealers.isEmpty() )
	{
		request.setAttribute( "demandDealers", demandDealers );
		Integer numDealers = dealer.getDealerPreference().getRedistributionNumTopDealers();
		request.setAttribute("numDemandDealers", numDealers);
		hasDemandDealers = true;
		SessionHelper.setAttribute( request, "demandDealers", demandDealers );
		SessionHelper.keepAttribute( request, "demandDealers" );
	}

	Collection<AppraisalActionType> redistributionActions = retrieveRedistributionActions( request,
																		currentDealerId,
																		hasDemandDealers );
	request.setAttribute( "redistributionActions", redistributionActions );

	try {
		Map<String,String> properties = CarfaxService.getInstance().getCarfaxReportProperties(
				currentDealerId,
				member.getLogin(),
				tradeAnalyzerForm.getVin(),
				VehicleEntityType.APPRAISAL);
		for (Map.Entry<String, String> entry : properties.entrySet()) {
			request.setAttribute(entry.getKey(), entry.getValue());
		}
	} catch (CarfaxException ce) {
		request.setAttribute("hasCarfaxError", true);
		request.setAttribute("problem", ce.getResponseCode());
	}
	
	try {
		Map<String,String> properties = AutoCheckService.getInstance().getAutoCheckReportProperties(
				currentDealerId,
				member.getLogin());
		for (Map.Entry<String, String> entry : properties.entrySet()) {
			request.setAttribute(entry.getKey(), entry.getValue());
		}
	} catch (AutoCheckException ace) {
		//prevent rest of page blowing up, what to do for autocheck error???
	}
	
	setBookInformationForPrintMenu( request, dealer );
	// nk - this needs to be fixed!!!
	AppraisalFormOptions formOptions = appraisal.getAppraisalFormOptions();
	int customerOffer = formOptions.getAppraisalFormOffer();
	// nk - this needs to be fixed!!!

	request.setAttribute( "customerOffer", customerOffer );
	request.setAttribute( "make", tradeAnalyzerForm.getMake() );
	request.setAttribute( "model", tradeAnalyzerForm.getModel() );
	request.setAttribute( "trim", tradeAnalyzerForm.getTrim() );
	request.setAttribute( "bookOutSourceId", BookOutSource.BOOK_OUT_SOURCE_APPRAISAL );
	request.setAttribute( "includeDealerGroup", new Integer( tradeAnalyzerForm.getIncludeDealerGroup() ) );
	request.setAttribute( "mileage", new Integer( Integer.MAX_VALUE ) );
	request.setAttribute( "mileageMax", new Integer( Integer.MAX_VALUE ) );
	request.setAttribute( "weeks", new Integer( weeks ) );
	request.setAttribute( "groupingDescriptionId", new Integer( groupingId ) );
	request.setAttribute( "tradeAnalyzerForm", tradeAnalyzerForm );
	request.setAttribute( "appraisalId", tradeAnalyzerForm.getAppraisalId() );
	request.setAttribute( "onTA", Boolean.TRUE );
	request.setAttribute( "showAppraisalForm", dealer.getDealerPreference().getShowAppraisalForm() );
	request.setAttribute( "twixURL", dealer.getDealerPreference().getTwixURL());

	boolean apppraisalLockoutEnabled = getFirstlookSessionFromRequest( request ).isIncludeAppraisalLockout();
	DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerId( currentDealerId );
	if ( dealerGroup.isLithiaStore() )
	{
		apppraisalLockoutEnabled = true;
		request.setAttribute( "replyToAddr", member.getEmailAddress() );
	}
	request.setAttribute( "bookoutLockoutEnabled", apppraisalLockoutEnabled );
	
	Map<AppraisalActionType, Integer> countsOfTabs = appraisalService.countTradeInAppraisalsAndGroupByAppraisalActionType( dealer.getDealerId(), dealer.getDealerPreference().getTradeManagerDaysFilter(), dealer.getDealerPreference().getShowInactiveAppraisals() );
	request.setAttribute( "waitingAppraisalsCount", countsOfTabs.get( AppraisalActionType.WAITING_REVIEW ) );

    checkInGroupAppraisals(appraisal.getBusinessUnitId(), dealerGroup.getDealerGroupId(), appraisal.getVehicle().getVin(),request);
    
    java.util.Enumeration it = request.getAttributeNames();
    
    while( it.hasMoreElements() )
    {
    	String attrName = (String)it.nextElement();
    	System.out.println(attrName);
    }
    
	//request.removeAttribute("tradeAnalyzerForm");

    ActionForward forward = mapping.findForward( "success" ); 
    
	return forward;
}


@SuppressWarnings("unchecked")
private void checkInGroupAppraisals(Integer businessUnitId, Integer parentId, String vin, HttpServletRequest request) {
    List<Map> results = appraisalService.retrieveInGroupAppraisals(businessUnitId, parentId,vin);
    boolean hasGroupAppraisals = (results.isEmpty() ? false:true);
    request.setAttribute("hasGroupAppraisals", hasGroupAppraisals);
    if (hasGroupAppraisals) {
        request.setAttribute("created",results.get(0).get("created"));
    } 
}

private void setBookInformationForPrintMenu( HttpServletRequest request, Dealer dealer )
{
	boolean hasSecondaryGuideBook = false;
	boolean isKelleyGuideBook = false;
	boolean isKelleySecondaryGuideBook = false;
	String title = "";
	String titleSecondary = "";
	List<Integer> thirdPartyBookIds = dealer.getDealerPreference().getOrderedGuideBookIds();
	Integer guideBookId = thirdPartyBookIds.get( 0 );
	if ( guideBookId.intValue() == ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE )
	{
		isKelleyGuideBook = true;
		title = "KBB";
	}
	else
	{
		title = ThirdPartyDataProvider.getThirdPartyDataProviderDescription( guideBookId.intValue() );
	}
	if ( thirdPartyBookIds.size() > 1 )
	{
		hasSecondaryGuideBook = true;
		Integer guideBookId2 = (Integer)thirdPartyBookIds.get( 1 );
		if ( guideBookId2.intValue() == ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE )
		{
			isKelleySecondaryGuideBook = true;
			titleSecondary = "KBB";
		}
		else
		{
			titleSecondary = ThirdPartyDataProvider.getThirdPartyDataProviderDescription( guideBookId2.intValue() );
		}
	}
	request.setAttribute( "isKelleyGuideBook", new Boolean( isKelleyGuideBook ) );
	request.setAttribute( "isKelleySecondaryGuideBook", new Boolean( isKelleySecondaryGuideBook ) );
	request.setAttribute( "hasSecondaryGuideBook", new Boolean( hasSecondaryGuideBook ) );
	request.setAttribute( "title", title );
	request.setAttribute( "titleSecondary", titleSecondary );
}

@SuppressWarnings("unchecked")
private List<DemandDealer> retrieveDemandDealers( TradeAnalyzerForm tradeAnalyzerForm, int groupingId, Dealer dealer ) throws ApplicationException,
		DatabaseException
{
	int mileage = tradeAnalyzerForm.getMileage().intValue();
	int year = tradeAnalyzerForm.getYear();
	
	Integer distance = dealer.getDealerPreference().getRedistributionDealerDistance();
	Integer sortBy = getImtDealerService().determineDemandDealerSortOrder(dealer.getBusinessUnitId(), dealer.getDealerPreference().getRedistributionUnderstock(), dealer.getDealerPreference().getRedistributionROI());
	
	tradeAnalyzerForm.setGroupingDescriptionId( groupingId );
	List<DemandDealer> demandDealers = (List)getImtDealerService().retrieveDemandDealers( dealer.getBusinessUnitId(), new Integer( groupingId ),
																	new Integer( mileage ), new Integer( year ), distance, sortBy );

	return demandDealers;

}

public void checkDealerGroupInclude( HttpServletRequest request, TradeAnalyzerForm form ) throws ApplicationException, DatabaseException
{
	if ( RequestHelper.getClickedButton( request ).equalsIgnoreCase( BaseActionForm.BUTTON_DEALERGROUP_RESULTS ) )
	{
		form.setIncludeDealerGroup( Report.DEALERGROUP_INCLUDE_TRUE );
	}
	else
	{
		form.setIncludeDealerGroup( Report.DEALERGROUP_INCLUDE_FALSE );
	}
}

private Collection<AppraisalActionType> retrieveRedistributionActions( HttpServletRequest request, int dealerId, boolean hasDemandDealers )
		throws ApplicationException
{
	boolean hasRedistribution = getFirstlookSessionFromRequest( request ).isIncludeRedistribution();
	return appraisalService.retrieveRedistributionActions( hasRedistribution && hasDemandDealers, true );
}

private void putGroupingDescriptionFormInRequest( HttpServletRequest request, GroupingDescription groupingDescription )
		throws ApplicationException
{
	request.setAttribute( "groupingDescriptionForm", new GroupingForm( groupingDescription ) );
}

private void setIncludeDealerGroup( TradeAnalyzerForm tradeAnalyzerForm, HttpServletRequest request )
{
	if ( request.getParameter( "showDealerGroup.x" ) != null )
	{
		tradeAnalyzerForm.setIncludeDealerGroup( Report.DEALERGROUP_INCLUDE_TRUE );
	}
	else if ( request.getParameter( "showDealer.x" ) != null )
	{
		tradeAnalyzerForm.setIncludeDealerGroup( Report.DEALERGROUP_INCLUDE_FALSE );
	}

}

public BasisPeriodService getBasisPeriodService()
{
	return basisPeriodService;
}

public void setBasisPeriodService( BasisPeriodService basisPeriodService )
{
	this.basisPeriodService = basisPeriodService;
}

public MakeModelGroupingService getMakeModelGroupingService()
{
	return makeModelGroupingService;
}

public void setMakeModelGroupingService( MakeModelGroupingService makeModelGroupingService )
{
	this.makeModelGroupingService = makeModelGroupingService;
}

public IAppraisalService getAppraisalService()
{
	return appraisalService;
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}


public void setReportGroupingRequestHelper(
		ReportGroupingRequestHelper reportGroupingRequestHelper) {
	this.reportGroupingRequestHelper = reportGroupingRequestHelper;
}

}