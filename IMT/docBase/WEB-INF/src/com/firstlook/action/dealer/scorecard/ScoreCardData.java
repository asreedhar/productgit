package com.firstlook.action.dealer.scorecard;

import java.util.List;
import java.util.Map;

import com.firstlook.entity.DealerTarget;

public abstract class ScoreCardData
{
private List analyses;
protected Map targets;

public List getAnalyses()
{
    return analyses;
}

public void setAnalyses( List list )
{
    analyses = list;
}

public Map getTargets()
{
    return targets;
}

public void setTargets( Map map )
{
    targets = map;
}

protected Integer getTargetValue( String targetName )
{
    DealerTarget dt = (DealerTarget) targets.get(targetName);
    if ( dt != null )
    {
        return new Integer(dt.getValue());
    } else
    {
        return null;
    }
}

}
