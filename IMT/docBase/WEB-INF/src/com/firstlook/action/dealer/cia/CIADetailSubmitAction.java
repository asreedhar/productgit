package com.firstlook.action.dealer.cia;

import java.util.Collection;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.cia.model.CIACategory;
import biz.firstlook.cia.model.Status;
import biz.firstlook.cia.persistence.ICIASummaryDAO;
import biz.firstlook.cia.service.CIAGroupingItemService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.service.cia.ICIAService;

public class CIADetailSubmitAction extends SecureBaseAction
{

private ICIASummaryDAO ciaSummaryDAO;
private CIAGroupingItemService ciaGroupingItemService;
private ICIAService ciaService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	Map parameterMap = request.getParameterMap();

	Integer segmentId = new Integer( RequestHelper.getInt( request, "segmentId" ) );
	
	Integer ciaCategoryId = new Integer( RequestHelper.getInt( request, "ciaCategory" ) );

	Integer summaryId = getCiaSummaryDAO().retrieveCIASummaryId(
			new Integer( getFirstlookSessionFromRequest( request ).getCurrentDealerId() ),
			Status.CURRENT );

	ActionErrors errors = validateBuyAmounts(request);
	
	String forward = "success";
	
	if (errors.isEmpty()) {
		try {
		getCiaService().constructCIAGroupingItemFromBuyingPreferences(
				ciaGroupingItemService.retrieveBy( summaryId, segmentId, ciaCategoryId ),
				parameterMap,
				CIACategory.retrieveCIACategory( ciaCategoryId.intValue() ),
				segmentId );
		}
		catch (Throwable t) {
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.dealer.tools.cia.detail.unknown"));
			forward = "failure-GB";
		}
	}
	else { 
		saveErrors(request, errors);
		forward = "failure-GB";
	}
	
	return mapping.findForward( forward );
}

@SuppressWarnings("unchecked")
private ActionErrors validateBuyAmounts(HttpServletRequest request) {
	ActionErrors errors = new ActionErrors();
	Collection<String> amountKeys = (Collection<String>) CollectionUtils.select(
			request.getParameterMap().keySet(),
			new Predicate() {
				public boolean evaluate(Object arg0) {
					return (arg0 != null && arg0 instanceof String )
						? ((String)arg0).startsWith("ucp_")
						: false;
				} } );
	Pattern integer = Pattern.compile("^\\d+$");
	for (String amountKey : amountKeys) {
		String amountValue = request.getParameter(amountKey);
		if (amountValue != null) {
			amountValue = amountValue.trim();
			if (amountValue.length() > 0 && !integer.matcher(amountValue).matches()) {
				errors.add(amountKey, new ActionError("error.dealer.tools.cia.detail.amount"));
			}
		}
	}
	return errors;
}

public ICIASummaryDAO getCiaSummaryDAO()
{
	return ciaSummaryDAO;
}

public void setCiaSummaryDAO( ICIASummaryDAO ciaSummaryDAO )
{
	this.ciaSummaryDAO = ciaSummaryDAO;
}

public CIAGroupingItemService getCiaGroupingItemService()
{
	return ciaGroupingItemService;
}

public void setCiaGroupingItemService( CIAGroupingItemService ciaGroupingItemService )
{
	this.ciaGroupingItemService = ciaGroupingItemService;
}

public ICIAService getCiaService()
{
	return ciaService;
}

public void setCiaService( ICIAService ciaService )
{
	this.ciaService = ciaService;
}

}
