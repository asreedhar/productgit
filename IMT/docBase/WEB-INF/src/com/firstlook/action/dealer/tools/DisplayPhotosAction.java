package com.firstlook.action.dealer.tools;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;

public class DisplayPhotosAction extends SecureBaseAction {

	IAppraisalService appaisalService;
	
	
	
	public IAppraisalService getAppraisalService() {
		return appaisalService;
	}

	public void setAppraisalService(IAppraisalService appaisalService) {
		this.appaisalService = appaisalService;
	}


	@Override
	public ActionForward doIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws DatabaseException, ApplicationException {

//		System.out.println(request.getParameter("appraisalId"));
		
		
		request.setAttribute("dealerId", getFirstlookSessionFromRequest(request).getCurrentDealerId());
		
		request.setAttribute("appraisalId", RequestHelper.getInt(request, "appraisalId"));
		
		
		
		
		return mapping.findForward("success");
	}


	
}
