package com.firstlook.action.dealer.sell;

import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.inventory.InventoryService_Legacy;
import com.firstlook.session.FirstlookSession;

public class SubmitVehiclesSubmitAction extends SecureBaseAction
{

private InventoryService_Legacy inventoryService_Legacy;
	
public ActionForward doIt( ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response ) throws DatabaseException,
        ApplicationException
{
    Collection<InventoryEntity> vehicles = inventoryService_Legacy
            .retrieveByDealerIdOrderByReceivedDateStockNumber(((FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION )).getCurrentDealerId());

    updateVehicles(vehicles);

    return mapping.findForward("success");
}

public void updateVehicles( Collection<InventoryEntity> inventories )
        throws DatabaseException, ApplicationException
{
    Iterator<InventoryEntity> iterator = inventories.iterator();
    while (iterator.hasNext())
    {
        InventoryEntity inventory =  iterator.next();
        if ( inventory.isReadyForSubmission() )
        {
            inventoryService_Legacy.saveOrUpdate(inventory);
        }
    }
}

public void setInventoryService_Legacy(InventoryService_Legacy inventoryService) {
	this.inventoryService_Legacy = inventoryService;
}


}
