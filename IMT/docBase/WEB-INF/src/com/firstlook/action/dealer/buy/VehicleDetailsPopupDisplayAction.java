package com.firstlook.action.dealer.buy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;

public class VehicleDetailsPopupDisplayAction extends VehicleDisplayBaseAction {
	
	public ActionForward doIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws DatabaseException, ApplicationException {
			
		String photoURLs = request.getParameter("url");
		request.setAttribute("photo", photoURLs);

		putVehicleInRequest( request, getImtDealerService() );
		return getActionForward(mapping);
	}

}
