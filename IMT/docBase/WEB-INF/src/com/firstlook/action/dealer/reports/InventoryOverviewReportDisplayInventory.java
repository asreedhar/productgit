package com.firstlook.action.dealer.reports;

import java.util.Date;

import biz.firstlook.commons.util.DateUtilFL;

import com.firstlook.entity.InventoryEntity;

public class InventoryOverviewReportDisplayInventory
{

private int groupingDescriptionId;
private String groupingDescription;
private Date inventoryRecivedDate;
private int year;
private String make;
private String model;
private String trim;
private String bodyType;
private String color;
private Integer mileage;
private Integer unitCost;
private Integer listPrice;
private Integer currentBookValue;
private Integer tradeOrPurchase;
private String stockNumber;
private String vin;
private String lotLocationAndStatus;
private boolean accurate;

public InventoryOverviewReportDisplayInventory()
{
	super();
}

public InventoryOverviewReportDisplayInventory( String groupingDescription, Date inventoryRecivedDate, int year, String make, String model,
												String trim, String bodyType, String color, Integer mileage, Integer unitCost, Integer listPrice,
												Integer currentBookValue, String stockNumber, String vin )
{
	super();
	this.groupingDescription = groupingDescription;
	this.inventoryRecivedDate = inventoryRecivedDate;
	this.year = year;
	this.make = make;
	this.model = model;
	this.trim = trim;
	this.bodyType = bodyType;
	this.color = color;
	this.mileage = mileage;
	this.unitCost = unitCost;
	this.listPrice = listPrice;
	this.currentBookValue = currentBookValue;
	this.stockNumber = stockNumber;
	this.vin = vin;
}

public int getAge()
{
	return DateUtilFL.calculateNumberOfCalendarDays( inventoryRecivedDate, new Date() );
}

public Integer getBookVsCost()
{
	if( currentBookValue == null || unitCost == null )
	{
		return null;		
	}
	if ( currentBookValue.intValue() == 0 )
	{
		return new Integer( 0 );
	}
	else
	{
		return new Integer( currentBookValue.intValue() - unitCost.intValue() );
	}
}

public Integer getCurrentBookValue()
{
	return currentBookValue;
}

public void setCurrentBookValue( Integer currentBookValue )
{
	this.currentBookValue = currentBookValue;
}

public String getColor()
{
	return color;
}

public void setColor( String color )
{
	this.color = color;
}

public Integer getListPrice()
{
	return listPrice;
}

public void setListPrice( Integer listPrice )
{
	this.listPrice = listPrice;
}

public String getMake()
{
	return make;
}

public void setMake( String make )
{
	this.make = make;
}

public Integer getMileage()
{
	return mileage;
}

public void setMileage( Integer mileage )
{
	this.mileage = mileage;
}

public String getModel()
{
	return model;
}

public void setModel( String model )
{
	this.model = model;
}

public String getStockNumber()
{
	return stockNumber;
}

public void setStockNumber( String stockNumber )
{
	this.stockNumber = stockNumber;
}

public String getTrim()
{
	return trim;
}

public void setTrim( String trim )
{
	this.trim = trim;
}

public Integer getUnitCost()
{
	return unitCost;
}

public void setUnitCost( Integer unitCost )
{
	this.unitCost = unitCost;
}

public String getBodyType()
{
	return bodyType;
}

public void setBodyType( String bodyType )
{
	this.bodyType = bodyType;
}

public String getVin()
{
	return vin;
}

public void setVin( String vin )
{
	this.vin = vin;
}

public int getYear()
{
	return year;
}

public void setYear( int year )
{
	this.year = year;
}

public String getGroupingDescription()
{
	return groupingDescription;
}

public void setGroupingDescription( String groupingDescription )
{
	this.groupingDescription = groupingDescription;
}

public Date getInventoryRecivedDate()
{
	return inventoryRecivedDate;
}

public void setInventoryRecivedDate( Date inventoryRecivedDate )
{
	this.inventoryRecivedDate = inventoryRecivedDate;
}

public Integer getTradeOrPurchase()
{
	return tradeOrPurchase;
}

public String getTradeOrPurchaseString()
{   
	if( tradeOrPurchase ==null )
	{
		return "U";
	}
	if ( tradeOrPurchase.intValue() == InventoryEntity.TRADEORPURCHASE_PURCHASE )
	{
		return "P";
	}
	else if ( tradeOrPurchase.intValue() == InventoryEntity.TRADEORPURCHASE_TRADE )
	{
		return "T";
	}
	else
	{
		return "U"; // Unknown
	}
}

public void setTradeOrPurchase( Integer tradeOrPurchase )
{
	this.tradeOrPurchase = tradeOrPurchase;
}

public int getGroupingDescriptionId()
{
	return groupingDescriptionId;
}

public void setGroupingDescriptionId( int groupingDescriptionId )
{
	this.groupingDescriptionId = groupingDescriptionId;
}

public String getLotLocationAndStatus()
{
	return lotLocationAndStatus;
}

public void setLotLocationAndStatus( String lotLocationAndStatus )
{
	this.lotLocationAndStatus = lotLocationAndStatus;
}

public boolean isAccurate()
{
	return accurate;
}

public void setAccurate( boolean accurate )
{
	this.accurate = accurate;
}

}
