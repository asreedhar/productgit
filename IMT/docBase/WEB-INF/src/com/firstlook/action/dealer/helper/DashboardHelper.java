package com.firstlook.action.dealer.helper;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;


import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryBucket;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.InventorySalesAggregate;
import com.firstlook.entity.lite.InventoryLite;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.action.ReportActionHelper;
import com.firstlook.persistence.dealer.IDealerFactsDAO;
import com.firstlook.persistence.inventory.InventorySalesAggregateService;
import com.firstlook.persistence.report.IInventoryBucketDAO;
import com.firstlook.report.FormattedDateForm;
import com.firstlook.service.inventory.InventoryService_Legacy;

public class DashboardHelper
{


private InventorySalesAggregateService inventorySalesAggregateService;
private ReportActionHelper reportActionHelper;
private InventoryService_Legacy inventoryService_Legacy; 

public DashboardHelper()
{
}

public void putPrintableDashboardInRequest( int inventoryType, IDealerFactsDAO dealerfactsDAO, HttpServletRequest request, Dealer dealer,
											IInventoryBucketDAO inventoryBucketDAO ) throws ApplicationException, DatabaseException
{
	int weeks = ReportActionHelper.determineWeeksAndSetOnRequest( request );
	int forecast = ReportActionHelper.determineForecastAndSetOnRequest( request );

	Integer rangeSetId = null;
	if ( inventoryType == InventoryEntity.USED_CAR )
	{
		rangeSetId = AgingPlanReportRangeSetEnum.USED_TOTAL_INVENTORY_REPORT_RANGE_SET.getId();
	}
	else
	{
		rangeSetId = AgingPlanReportRangeSetEnum.NEW_TOTAL_INVENTORY_REPORT_RANGE_SET.getId();
	}

	reportActionHelper.putReportInRequest( dealer, weeks, forecast, request, inventoryType );
	reportActionHelper.putAveragesInRequest( dealer.getDealerId().intValue(), weeks, forecast, request, inventoryType );

	InventoryBucket rangeSet = inventoryBucketDAO.findByRangeSetId( rangeSetId );

	InventorySalesAggregate aggregate = inventorySalesAggregateService.retrieveCachedInventorySalesAggregate( dealer.getDealerId().intValue(),
																												weeks, 0, inventoryType );

	AgingReportHelper agingReportHelper = new AgingReportHelper(inventoryService_Legacy);
	agingReportHelper.putAgingReportRangesInRequest( dealer, request, InventoryLite.TRUE, null, rangeSet, aggregate );

	DealerFactsHelper dealerFactsHelper = new DealerFactsHelper( dealerfactsDAO );
	dealerFactsHelper.putSaleMaxPolledDateInRequest( dealer.getDealerId().intValue(), request );
	dealerFactsHelper.putVehicleMaxPolledDateInRequest( dealer.getDealerId().intValue(), request, inventoryType );

	FormattedDateForm currentDate = new FormattedDateForm( new Date( System.currentTimeMillis() ), "MM/dd/yyyy" );
	request.setAttribute( "currentDate", currentDate );
}

public InventorySalesAggregateService getInventorySalesAggregateService()
{
    return inventorySalesAggregateService;
}
public void setInventorySalesAggregateService( InventorySalesAggregateService inventorySalesAggregateService )
{
    this.inventorySalesAggregateService = inventorySalesAggregateService;
}

public void setReportActionHelper( ReportActionHelper reportActionHelper )
{
	this.reportActionHelper = reportActionHelper;
}

public void setInventoryService_Legacy(InventoryService_Legacy inventoryService) {
	this.inventoryService_Legacy = inventoryService;
}
}
