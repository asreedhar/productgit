package com.firstlook.action.dealer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.servlet.http.HttpServletRequestHelper;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.session.Product;

public class DealerHomeDisplayAction extends SecureBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response ) throws DatabaseException,
        ApplicationException
{
    int currentDealerId = getCurrentDealerId(request);
	
	Member member = getMemberFromRequest(request);
    String forward = getMappingForward(member, currentDealerId, request);

    if (!forward.equalsIgnoreCase("success"))
    {
        return mapping.findForward( adjustMaxForward( forward, request ) );
    }

   	getFirstlookSessionFromRequest( request ).setProduct( Product.EDGE );

	addDealerCookie(response, currentDealerId);
	
    request.setAttribute("dealerId", "" + currentDealerId);

    putDealerFormInRequest( request, 0 );
    
    // service url for CAS
    request.setAttribute( "service", HttpServletRequestHelper.getServiceUrl(request) );

    return mapping.findForward(adjustMaxForward( forward, request ));
}

String getMappingForward( Member member, int currentDealerId, HttpServletRequest request)
{
    String forward = null;

    if (currentDealerId == -1) // WTF!
    {
        if (member.getDealerGroupId() != 0)
        {
            forward = "dealerGroup";
        }
        else
        {
            forward = "login";
        }
    }
    else
    {
    	forward = "success";
    }

    return forward;
}

}
