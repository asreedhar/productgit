package com.firstlook.action.dealer.sell;

import junit.framework.TestSuite;

public class AllTests
{

public AllTests()
{
    super();
}

public static TestSuite suite()
{
    TestSuite suite = new TestSuite();
    suite.addTest( new TestSuite( TestReviewSelectedVehiclesDisplayAction.class ) );
    suite.addTest( new TestSuite( TestSelectVehiclesSubmitAction.class ) );

    return suite;
}

}
