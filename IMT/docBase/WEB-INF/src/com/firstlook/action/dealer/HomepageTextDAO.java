package com.firstlook.action.dealer;

import java.util.ArrayList;
import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.firstlook.entity.MessageCenter;

public class HomepageTextDAO extends HibernateDaoSupport
{

@SuppressWarnings("unchecked")
List<MessageCenter> getMessages()
{
	if(getHibernateTemplate() == null)
	{
		return null;
	}

	List<Object[]> messageCenters = getHibernateTemplate().find("select ordering,title,body,hasPriority from com.firstlook.entity.MessageCenter order by ordering");
	
	List<MessageCenter> messages = new ArrayList<MessageCenter>();
	for(Object[] messageCenter : messageCenters) {
		MessageCenter message = new MessageCenter();
		message.setOrdering((Integer)messageCenter[0]);
		message.setTitle((String)messageCenter[1]);
		message.setBody((String)messageCenter[2]);
		message.setHasPriority((Boolean)messageCenter[3]);
		messages.add(message);
	}
	
	return messages;
}

}
