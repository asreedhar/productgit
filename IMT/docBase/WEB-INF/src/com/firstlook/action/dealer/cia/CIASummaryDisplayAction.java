package com.firstlook.action.dealer.cia;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ReverseComparator;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.cia.model.CIACategory;
import biz.firstlook.cia.model.Status;
import biz.firstlook.cia.persistence.ICIASummaryDAO;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.Segment;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.cia.CIASegmentByContributionRetriever;
import com.firstlook.persistence.inventory.InventoryPersistence;
import com.firstlook.service.cia.ICIAService;
import com.firstlook.service.cia.report.CIASummaryTableDisplayPair;
import com.firstlook.service.cia.report.CIASummaryYearRecommendationDisplayData;
import com.firstlook.service.cia.report.ICIASummaryDisplayData;

public class CIASummaryDisplayAction extends SecureBaseAction
{

private ICIAService ciaService;
private ICIASummaryDAO ciaSummaryDAO;
private DealerPreferenceDAO dealerPrefDAO;
private InventoryPersistence inventoryPersistence;
private CIASegmentByContributionRetriever ciaSegmentByContributionRetriever;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	Integer businessUnitId = Integer.valueOf( getFirstlookSessionFromRequest( request ).getCurrentDealerId() );

	// Get the segment/ciaBodyTypeDetail (SUV, SEDAN) we are currently
	// looking at.
	Integer segmentId;
	try
	{
		segmentId = Integer.valueOf( request.getParameter( "segmentId" ) );
	}
	catch ( NumberFormatException nfe )
	{
		segmentId = selectHighestPercentContribution( businessUnitId );
	}

	DealerPreference dealerPref = dealerPrefDAO.findByBusinessUnitId( businessUnitId );
	List inventoryItems = (List)inventoryPersistence.findByDealerIdInventoryTypeAndUpperAndLowerUnitCostThreshold(
																										businessUnitId.intValue(),
																										InventoryEntity.USED_CAR,
																										dealerPref.getUnitCostThresholdLower(),
																										dealerPref.getUnitCostThresholdUpper() );

	List<? extends ICIASummaryDisplayData> listPZ = constructOrderedDisplayList( ciaService.getCIASummaryDisplayData( businessUnitId, segmentId, CIACategory.POWERZONE,
																					inventoryItems ) );
	List<? extends ICIASummaryDisplayData> listWZ = constructOrderedDisplayList( ciaService.getCIASummaryDisplayData( businessUnitId, segmentId, CIACategory.WINNERS,
																					inventoryItems ) );
	List<? extends ICIASummaryDisplayData> listGB = constructOrderedDisplayList( ciaService.getCIASummaryDisplayData( businessUnitId, segmentId, CIACategory.GOODBETS,
																					inventoryItems ) );
	List<? extends ICIASummaryDisplayData> listMP = ciaService.getCIASummaryDisplayData( businessUnitId, segmentId, CIACategory.MARKET_PERFORMERS, inventoryItems );
	List<? extends ICIASummaryDisplayData> listMC = constructOrderedDisplayList( ciaService.getCIASummaryDisplayData( businessUnitId, segmentId,
																					CIACategory.MANAGERS_CHOICE, inventoryItems ) );

	Integer buyAmountTotalForSegment = calculateBuyAmountTotal( listPZ, listWZ, listGB, listMP, listMC );

	request.setAttribute( "displayDataOfPowerZones", listPZ );
	request.setAttribute( "displayDataOfWinners", listWZ );
	request.setAttribute( "displayDataOfGoodBets", listGB );
	request.setAttribute( "displayDataOfMarketPerformers", listMP );
	request.setAttribute( "displayDataOfManagersChoice", listMC );
	request.setAttribute( "buyAmountTotalForSegment", buyAmountTotalForSegment );

	request.setAttribute( "segmentId", segmentId );

	// List of vehicles a dealer can add to their buying plan
	List managersChoiceItems = ciaService.getManagersChoiceItemsBySegmentId( segmentId );
	request.setAttribute( "ciaManagersChoiceMakeModels", managersChoiceItems );

	Dealer currentDealer = getImtDealerService().retrieveDealer( businessUnitId.intValue() );
	request.setAttribute( "nickname", currentDealer.getNickname() );

	return mapping.findForward( "success" );
}

private Integer calculateBuyAmountTotal( List<? extends ICIASummaryDisplayData> listPZ,
                                         List<? extends ICIASummaryDisplayData> listWZ, 
                                         List<? extends ICIASummaryDisplayData> listGB, 
                                         List<? extends ICIASummaryDisplayData> listMP, 
                                         List<? extends ICIASummaryDisplayData> listMC )
{
	List<ICIASummaryDisplayData> allSummaryItems = new ArrayList<ICIASummaryDisplayData>();
	allSummaryItems.addAll( listPZ );
	allSummaryItems.addAll( listWZ );
	allSummaryItems.addAll( listGB );
	allSummaryItems.addAll( listMP );
	allSummaryItems.addAll( listMC );

	Iterator<ICIASummaryDisplayData> allItemsIter = allSummaryItems.iterator();
	ICIASummaryDisplayData displayData;
	int buyAmountTotal = 0;
	while ( allItemsIter.hasNext() )
	{
		displayData = allItemsIter.next();
		buyAmountTotal += displayData.getPlannedToBuyTotal().intValue();
	}

	Integer buyAmountTotalForSegment = new Integer( buyAmountTotal );
	return buyAmountTotalForSegment;
}

private List<CIASummaryYearRecommendationDisplayData> constructOrderedDisplayList( List<CIASummaryYearRecommendationDisplayData> ciaSummaryDisplayDatas )
{
	CIASummaryYearRecommendationDisplayData summaryDisplayData;
	CIASummaryTableDisplayPair displayPair;
	Iterator<CIASummaryYearRecommendationDisplayData> displayDataIter = ciaSummaryDisplayDatas.iterator();
	Iterator<String> keyIter;
	String key;
	List<CIASummaryTableDisplayPair> orderedDisplayList;
	while ( displayDataIter.hasNext() )
	{
		orderedDisplayList = new ArrayList<CIASummaryTableDisplayPair>();
		summaryDisplayData = displayDataIter.next();
		keyIter = summaryDisplayData.getPairedDataMap().keySet().iterator();
		while ( keyIter.hasNext() )
		{
			key = keyIter.next();
			displayPair = (CIASummaryTableDisplayPair)summaryDisplayData.getPairedDataMap().get( key );
			orderedDisplayList.add( displayPair );
		}
		ReverseComparator yearComparator = new ReverseComparator( new BeanComparator( "year" ) );
		Collections.sort( orderedDisplayList, yearComparator );
		summaryDisplayData.setPairedDataList( orderedDisplayList );
	}
	return ciaSummaryDisplayDatas;
}

private Integer selectHighestPercentContribution( Integer businessUnitId )
{
	Integer ciaSummaryId = ciaSummaryDAO.retrieveCIASummaryId( businessUnitId, Status.CURRENT );
	Segment segment = null;
	if(ciaSummaryId != null) {
		List<Segment> segments = ciaSegmentByContributionRetriever.retrieveCIABodyTypeDetails( 
				businessUnitId.intValue(), 
				true, 
				new Timestamp( new Date().getTime() ),
				ciaSummaryId.intValue() );
		
		if(segments != null ) {
			segment = segments.get( 0 );
		} else {
			segment = Segment.SEDAN;
		}
	} else {
		segment = Segment.SEDAN;
	}
	return segment.getSegmentId();
}

public void setCiaService( ICIAService ciaService )
{
	this.ciaService = ciaService;
}

public void setCiaSummaryDAO( ICIASummaryDAO ciaSummaryDAO )
{
	this.ciaSummaryDAO = ciaSummaryDAO;
}

public void setDealerPrefDAO( DealerPreferenceDAO dealerPrefDAO )
{
	this.dealerPrefDAO = dealerPrefDAO;
}

public void setInventoryPersistence( InventoryPersistence inventoryPersistence )
{
	this.inventoryPersistence = inventoryPersistence;
}

public void setCiaSegmentByContributionRetriever(
		CIASegmentByContributionRetriever ciaSegmentByContributionRetriever) {
	this.ciaSegmentByContributionRetriever = ciaSegmentByContributionRetriever;
}

}