package com.firstlook.action.dealer.tools;

import junit.framework.TestSuite;

public class AllTests
{

public AllTests()
{
    super();
}

public static TestSuite suite()
{
    TestSuite suite = new TestSuite();
    suite.addTest(new TestSuite(TestGuideBookStepThreeSubmitAction.class));
    suite.addTest(new TestSuite(TestPerformanceAnalyzerDisplayAction.class));
    suite.addTest(new TestSuite(TestTradeAnalyzerSubmitAction.class));

    return suite;
}
}
