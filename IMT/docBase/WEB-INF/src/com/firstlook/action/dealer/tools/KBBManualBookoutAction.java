package com.firstlook.action.dealer.tools;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import biz.firstlook.module.bookout.IThirdPartyBookoutService;
import biz.firstlook.module.vehicle.description.old.BookOutDatasetInfo;
import biz.firstlook.module.vehicle.description.old.BookOutError;
import biz.firstlook.module.vehicle.description.old.BookOutTypeEnum;
import biz.firstlook.module.vehicle.description.old.GBException;
import biz.firstlook.module.vehicle.description.old.KBBConditionEnum;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.model.BookOutValueType;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.service.ThirdPartyVehicleOptionService;

import com.firstlook.display.tradeanalyzer.DisplayGuideBook;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.service.bookout.BookOutService;
import com.firstlook.service.bookout.bean.AbstractBookOutState;
import com.firstlook.thirdparty.book.old.VDP_SelectedKeys;

public class KBBManualBookoutAction extends AbstractManualBookOutAction
{

private IThirdPartyBookoutService kbbBookoutService;
private static final String FOOTER_TEXT = "All Kelley Blue Book values are reprinted with permission of Kelley Blue Book.";

@Override
protected String getGuideBookName() {
	return "KBB";
}

@Override
protected String processManualRequest(HttpServletRequest request, Dealer currentDealer, String selected) throws ApplicationException {
	int businessUnitId = currentDealer.getBusinessUnitId();
	String forward = loadYears(request, businessUnitId);
	if ( selected.equalsIgnoreCase( "make" ) )
	{
		forward = loadMakes( request, businessUnitId );

	}
	else if ( selected.equalsIgnoreCase( "model" ) )
	{
		forward = loadModels( request, businessUnitId );
	}
	else if ( selected.equalsIgnoreCase( "trim" ) )
	{
		forward = loadTrims( request, businessUnitId );
	}
	else if ( selected.equalsIgnoreCase( "getOptions" ) )
	{
		try{
			forward = loadOptions( request, businessUnitId, currentDealer.getState() );
		}
		catch (GBException gbe) {
			throw new ApplicationException(gbe);
		}
	}
	else if ( selected.equalsIgnoreCase( "getValues" ) )
	{
		forward = loadValues( request, businessUnitId, currentDealer.getState() );
	}
	return forward;
}


private static BookOutDatasetInfo getBookOutDatasetInfo(Integer businessUnitId) {
	return new BookOutDatasetInfo( BookOutTypeEnum.APPRAISAL, businessUnitId);
}

private String loadYears(HttpServletRequest request, Integer businessUnitId) {
	List<Integer> years = null;
	try {
		years = kbbBookoutService.retrieveModelYears(getBookOutDatasetInfo(businessUnitId));
	} catch (GBException e ) {
		request.setAttribute( "error", Boolean.TRUE );
		request.setAttribute( "showOptions", Boolean.FALSE );
		request.setAttribute( "showValues", Boolean.FALSE );
		return "optionSuccess";
	}
	request.setAttribute( "years", years );
	return "success";
}

private String loadMakes( HttpServletRequest request, Integer businessUnitId )
{
	String selectedYear = request.getParameter( "year" );
	List< VDP_GuideBookVehicle > makes = new ArrayList< VDP_GuideBookVehicle >();
	try
	{
		makes = kbbBookoutService.retrieveMakes( selectedYear, getBookOutDatasetInfo(businessUnitId) );
	}
	catch ( GBException e )
	{
		request.setAttribute( "error", Boolean.TRUE );
		request.setAttribute( "showOptions", Boolean.FALSE );
		request.setAttribute( "showValues", Boolean.FALSE );
		return "optionSuccess";
	}
	request.setAttribute( "makes", makes );
	return "makeSuccess";
}

private String loadModels( HttpServletRequest request, Integer businessUnitId )
{
	String selectedYear = request.getParameter( "year" );
	String selectedMake = request.getParameter( "make" );
	List< VDP_GuideBookVehicle > models = new ArrayList< VDP_GuideBookVehicle >();
	try
	{
		models = (List< VDP_GuideBookVehicle >)kbbBookoutService.retrieveModels( selectedYear, selectedMake, getBookOutDatasetInfo(businessUnitId) );
	}
	catch ( GBException e )
	{
		request.setAttribute( "error", Boolean.TRUE );
		request.setAttribute( "showOptions", Boolean.FALSE );
		request.setAttribute( "showValues", Boolean.FALSE );
		return "optionSuccess";
	}
	request.setAttribute( "models", models );
	return "modelSuccess";
}

private String loadTrims( HttpServletRequest request, Integer businessUnitId )
{
	String selectedYear = request.getParameter( "year" );
	String selectedMake = request.getParameter( "make" );
	String selectedModel = request.getParameter( "model" );
	List< VDP_GuideBookVehicle > trims = new ArrayList< VDP_GuideBookVehicle >();
	try
	{
		trims = (List< VDP_GuideBookVehicle >)kbbBookoutService.retrieveTrims( selectedYear, selectedMake, selectedModel, getBookOutDatasetInfo(businessUnitId) );
		if(trims.size() < 1) {
			throw new GBException("No trims found for the selected Year, Make, and Model, please choose a different Model.");
		}
	}
	catch ( GBException e )
	{
		request.setAttribute( "error", Boolean.TRUE );
		request.setAttribute("errorMessage", e.getMessage());
		request.setAttribute( "showOptions", Boolean.FALSE );
		request.setAttribute( "showValues", Boolean.FALSE );
		return "optionSuccess";
	}
	
	request.setAttribute( "trims", trims );
	return "trimSuccess";
}

private String loadOptions( HttpServletRequest request, Integer businessUnitId, String stateCD ) throws GBException
{
	String selectedYear = request.getParameter( "year" );
	String selectedMake = request.getParameter( "make" );
	String selectedModel = request.getParameter( "model" );
	String selectedTrim = request.getParameter( "catalogKeyId" );
	
	
	DisplayGuideBook displayGuideBook = new DisplayGuideBook();
	try
	{
		List<ThirdPartyVehicleOption> options = kbbBookoutService.retrieveOptions( "0", selectedYear,
				selectedMake, selectedModel, selectedTrim, getBookOutDatasetInfo(businessUnitId) );
		displayGuideBook.setDisplayGuideBookOptions( options );
	}
	catch ( GBException e )
	{
		request.setAttribute( "error", Boolean.TRUE );
		request.setAttribute( "showOptions", Boolean.FALSE );
		request.setAttribute( "showValues", Boolean.FALSE );
	}
	
	setSelectedEquipmentOptionKeys( displayGuideBook);
	
	VDP_SelectedKeys selectedKeys = BookOutService.retrieveSelectedKeysAndSetSelectedFalse( (List<ThirdPartyVehicleOption>) displayGuideBook.getDisplayGuideBookOptions() );
	displayGuideBook.setSelectedDrivetrainKey( selectedKeys.getSelectedDrivetrainKey() );
	displayGuideBook.setSelectedEngineKey( selectedKeys.getSelectedEngineKey() );
	displayGuideBook.setSelectedTransmissionKey( selectedKeys.getSelectedTransmissionKey() );
	
	
	setDisplayGuideBookInfo(displayGuideBook, businessUnitId, stateCD);
	
	request.setAttribute( "showOptions", Boolean.TRUE );
	request.setAttribute( "showValues", Boolean.FALSE );
	request.setAttribute( "displayGuideBook", displayGuideBook );
	return "optionSuccess";
}

private void setSelectedEquipmentOptionKeys( DisplayGuideBook displayGuideBook )
{
	Iterator<ThirdPartyVehicleOption> iter = displayGuideBook.getDisplayGuideBookOptions().iterator();
	List<String> selectedOptions = new ArrayList< String >();
	while ( iter.hasNext() )
	{
		ThirdPartyVehicleOption option = iter.next();
		if ( option.isStatus() )
		{
			selectedOptions.add( option.getOptionKey() );
		}
	}
	displayGuideBook.setSelectedEquipmentOptionKeys( (String[])selectedOptions.toArray(new String[selectedOptions.size()]) );
}

private String loadValues( HttpServletRequest request, Integer businessUnitId, String stateCD ) throws ApplicationException
{
	String selectedYear = request.getParameter( "year" );
	String selectedMake = request.getParameter( "make" );
	String selectedModel = request.getParameter( "model" );
	String selectedTrim = request.getParameter( "catalogKeyId" );
	String[] selectedEquipmentOptionKeys = request.getParameterValues( "selectedEquipmentOptionKeys" );
	String selectedEngineKey = RequestHelper.getString( request, "selectedEngineKey" );
	String selectedDrivetrainKey = RequestHelper.getString( request, "selectedDrivetrainKey" );
	String selectedTransmissionKey = RequestHelper.getString( request, "selectedTransmissionKey" );
	DisplayGuideBook displayGuideBook = new DisplayGuideBook();
	boolean hasErrors = false;
	
	int miles = parseMileage(request.getParameter( "mileage" ));
	
	try
	{
		displayGuideBook.setDisplayGuideBookOptions( kbbBookoutService.retrieveOptions( "0", selectedYear,
																								selectedMake, selectedModel, selectedTrim, getBookOutDatasetInfo(businessUnitId) ) );

		if ( selectedEquipmentOptionKeys != null )
		{
			displayGuideBook.setSelectedEquipmentOptionKeys( 
					ThirdPartyVehicleOptionService.setAndUpdateSelectedOptionsUsingArray(
							displayGuideBook.getDisplayEquipmentGuideBookOptions(),
							selectedEquipmentOptionKeys ) );
		}
		displayGuideBook.setSelectedDrivetrainKey( selectedDrivetrainKey );
		displayGuideBook.setSelectedEngineKey( selectedEngineKey );
		displayGuideBook.setSelectedTransmissionKey( selectedTransmissionKey );
		updateEngineTransmissionDriveTrainOptions( displayGuideBook );
		

		VDP_GuideBookVehicle vehicle = new VDP_GuideBookVehicle();
		vehicle.setYear( selectedYear );
		vehicle.setKey(selectedTrim); // KBB.vehicleId maps 1 to 1 with a trim
		
		vehicle.setCondition( KBBConditionEnum.getKBBConditionEnumById( RequestHelper.getInt( request, "conditionId" ) ) );
		displayGuideBook.setDisplayGuideBookValues( kbbBookoutService.retrieveBookValues(stateCD, miles, 
		                                                displayGuideBook.getDisplayGuideBookOptions(), vehicle, null, 
		                                                new BookOutDatasetInfo( BookOutTypeEnum.APPRAISAL, businessUnitId)) );

		List<VDP_GuideBookValue> someValues = displayGuideBook.getDisplayGuideBookValues();
		VDP_GuideBookValue check = null;
		if(!someValues.isEmpty()) {
			check = someValues.iterator().next();
		}
		
		if (check != null && check.isHasErrors())
		{
			List<BookOutError> errors = new ArrayList< BookOutError >();
			Iterator<VDP_GuideBookValue> iter = displayGuideBook.getDisplayGuideBookValues().iterator();
			while ( iter.hasNext() )
			{
				VDP_GuideBookValue error = iter.next();
				if ( error.isHasErrors() )
				{
					errors.add( error.getError() );
				}
			}
			request.setAttribute( "errors", errors );
			request.setAttribute( "showOptions", Boolean.TRUE );
			request.setAttribute( "showValues", Boolean.FALSE );
			request.setAttribute( "hasConflict", Boolean.TRUE );
			hasErrors = true;
		}
		else
		{
			AbstractBookOutState.determineKelleyBaseValues( displayGuideBook, displayGuideBook.getDisplayGuideBookValues() );
			AbstractBookOutState.determineKelleyNoMileageValues( displayGuideBook, displayGuideBook.getDisplayGuideBookValues() );
			AbstractBookOutState.determineKelleyFinalValues( displayGuideBook, displayGuideBook.getDisplayGuideBookValues() );
			List<VDP_GuideBookValue> optionValues = AbstractBookOutState.retrieveThirdPartyValuesByValueType( displayGuideBook.getDisplayGuideBookValues(),BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE );
			if (optionValues != null && !optionValues.isEmpty()) {
				displayGuideBook.setKelleyOptionsAdjustment( ((VDP_GuideBookValue)optionValues.get(0) ) );
			}
			List<VDP_GuideBookValue> mileageAdjustmentValues = AbstractBookOutState.retrieveThirdPartyValuesByValueType( displayGuideBook.getDisplayGuideBookValues(),BookOutValueType.BOOK_OUT_MILEAGE_ADJUSTMENT);
			if (mileageAdjustmentValues != null && !mileageAdjustmentValues.isEmpty()) {				
				displayGuideBook.setMileageAdjustment(mileageAdjustmentValues.get(0).getValue() );
			}
		}
	}
	catch ( GBException e )
	{
		request.setAttribute( "error", Boolean.TRUE );
		request.setAttribute( "showOptions", Boolean.FALSE );
		request.setAttribute( "showValues", Boolean.FALSE );
		hasErrors = true;
	}

	displayGuideBook.setConditionId( RequestHelper.getInt( request, "conditionId" ) );
	displayGuideBook.setYear( selectedYear );
	try{
		setDisplayGuideBookInfo(displayGuideBook, businessUnitId, stateCD);
	}
	catch(GBException gbe){
		throw new ApplicationException(gbe);
	}
	
	if (!hasErrors)
	{
		request.setAttribute( "showOptions", Boolean.TRUE );
		request.setAttribute( "showValues", Boolean.TRUE );
	}
	
	request.setAttribute( "displayGuideBook", displayGuideBook );
	return "optionSuccess";
}

private int parseMileage(String mileage) throws ApplicationException {
	if(mileage == null) {
		throw new ApplicationException("Expected a number for Mileage.");
	}
	
	Double uncleanMiles = null;
	
	mileage = mileage.trim();
	try {
		uncleanMiles = Double.parseDouble(mileage);
	} catch (NumberFormatException nfe) {
		throw new ApplicationException("Mileage was not a valid number!");
	}
	
	if(uncleanMiles == null) {
		throw new ApplicationException("Mileage was not a valid number!");
	}
	
	int miles = uncleanMiles.intValue();
	
	return miles;
}

private void updateEngineTransmissionDriveTrainOptions( DisplayGuideBook displayGuideBook )
{
	Iterator<ThirdPartyVehicleOption> iter = displayGuideBook.getDisplayDrivetrainGuideBookOptions().iterator();
	while ( iter.hasNext() )
	{
		ThirdPartyVehicleOption option = iter.next();
		if ( displayGuideBook.getSelectedDrivetrainKey() != null && displayGuideBook.getSelectedDrivetrainKey().equals( option.getOptionKey() ))
		{
			option.setStatus( true );
		} else {
			option.setStatus( false );
		}
	}
	
	iter = displayGuideBook.getDisplayEngineGuideBookOptions().iterator();
	while ( iter.hasNext() )
	{
		ThirdPartyVehicleOption option = (ThirdPartyVehicleOption)iter.next();
		if ( displayGuideBook.getSelectedEngineKey() != null && displayGuideBook.getSelectedEngineKey().equals( option.getOptionKey() ))
		{
			option.setStatus( true );
		} else {
			option.setStatus( false );
		}
	}
	
	iter = displayGuideBook.getDisplayTransmissionGuideBookOptions().iterator();
	while ( iter.hasNext() )
	{
		ThirdPartyVehicleOption option = (ThirdPartyVehicleOption)iter.next();
		if ( displayGuideBook.getSelectedTransmissionKey() != null && displayGuideBook.getSelectedTransmissionKey().equals( option.getOptionKey() ))
		{
			option.setStatus( true );
		} else {
			option.setStatus( false );
		}
	}
}

private void setDisplayGuideBookInfo( DisplayGuideBook displayGuideBook, Integer businessUnitId, String region ) throws GBException
{
	displayGuideBook.setGuideBookId( ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE );
	displayGuideBook.setImageName( ThirdPartyDataProvider.IMAGE_NAME_KELLEY );
	displayGuideBook.setGuideBookName( "KBB" );
	displayGuideBook.setFooter( FOOTER_TEXT );
	BookOutDatasetInfo bookoutInfo = getBookOutDatasetInfo(businessUnitId);
	displayGuideBook.setPublishInfo( kbbBookoutService.getMetaInfo( region, bookoutInfo ).getPublishInfo() );
}

public void setKbbBookoutService( IThirdPartyBookoutService kbbBookoutService )
{
	this.kbbBookoutService = kbbBookoutService;
}



}
