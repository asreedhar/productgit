package com.firstlook.action.dealer.redistribution;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.appraisal.trademanager.TradeManagerDisplayAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;

public class SendPrintableTradeAnalyzerPlus extends SecureBaseAction
{

/**
 * This action does nothing, but it's a pain to remove right now. This action does what struts does, but not as robust! -bf.
 */
// TODO: DELETE This action. Make sure the forward logic is moved the correct location.
public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	return validateActionForward( request, mapping );
}

private ActionForward validateActionForward( HttpServletRequest request, ActionMapping mapping )
{
	String pageName = request.getParameter( "pageName" );
	if ( ( pageName != null ) && 
			(  pageName.equals( TradeManagerDisplayAction.BULLPEN ) 
			|| pageName.equals( "tradesPosted" )
			|| pageName.equals( "retail" ) || pageName.equals( "notTraded" )
			|| pageName.equals( "wholesalerAuction" ) || pageName.equals( "oldTrades" )
			|| pageName.equals( TradeManagerDisplayAction.AWAITING_APPRAISAL )
            || pageName.equalsIgnoreCase("search"))) {
	
		return mapping.findForward( pageName );
	}
   return mapping.findForward( "success" );
}

}