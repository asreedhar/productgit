package com.firstlook.action.dealer.reports;

import java.util.List;

public class InventoryOverviewReportDisplayData
{

private String groupingDescription;
private int groupingDescriptionId;

private int averageInventoryAge;
private int averageInvnetoryAgeThreshold;

private int averageDaysSupply;
private int averageDaysSupplyThreshold;

private int retailAverageGrossProfit;
private int unitsSold;
private int averageDaysToSale;
private int noSales;
private int unitSoldThreshold;

private List inventoryList;

public InventoryOverviewReportDisplayData( int unitSoldThreshold, int averageInventoryAgeThreshold, int averageDaysSupplyThreshold )
{
	this.unitSoldThreshold = unitSoldThreshold;
	this.averageInvnetoryAgeThreshold = averageInventoryAgeThreshold;
	this.averageDaysSupplyThreshold = averageDaysSupplyThreshold;
}

public boolean isAverageDaysSupplyRed()
{
	return averageDaysSupply > averageDaysSupplyThreshold;
}

public boolean isShowSalesBubble()
{
	return ( ( retailAverageGrossProfit > 0 ) || ( unitsSold > 0 ) || ( averageDaysToSale > 0 ) || ( noSales > 0 ) );
}

public boolean isShowDaysSupply()
{
	return unitsSold > unitSoldThreshold;
}

public boolean isAverageInventoryAgeRed()
{
	return averageInventoryAge > averageInvnetoryAgeThreshold;
}

public int getAverageDaysSupply()
{
	return averageDaysSupply;
}

public int getAverageInventoryAge()
{
	return averageInventoryAge;
}

public String getGroupingDescription()
{
	return groupingDescription;
}

public void setGroupingDescription( String groupingDescription )
{
	this.groupingDescription = groupingDescription;
}

public int getGroupingDescriptionId()
{
	return groupingDescriptionId;
}

public void setGroupingDescriptionId( int groupingDescriptionId )
{
	this.groupingDescriptionId = groupingDescriptionId;
}

public int getAverageDaysToSale()
{
	return averageDaysToSale;
}

public void setAverageDaysToSale( int averageDaysToSale )
{
	this.averageDaysToSale = averageDaysToSale;
}

public List getInventoryList()
{
	return inventoryList;
}

public void setInventoryList( List inventoryList )
{
	this.inventoryList = inventoryList;
}

public int getNoSales()
{
	return noSales;
}

public void setNoSales( int noSales )
{
	this.noSales = noSales;
}

public int getRetailAverageGrossProfit()
{
	return retailAverageGrossProfit;
}

public void setRetailAverageGrossProfit( int retailAverageGrossProfit )
{
	this.retailAverageGrossProfit = retailAverageGrossProfit;
}

public int getUnitsSold()
{
	return unitsSold;
}

public void setUnitsSold( int unitsSold )
{
	this.unitsSold = unitsSold;
}

public void setAverageDaysSupply( int averageDaysSupply )
{
	this.averageDaysSupply = averageDaysSupply;
}

public void setAverageInventoryAge( int averageInventoryAge )
{
	this.averageInventoryAge = averageInventoryAge;
}
}
