package com.firstlook.action.dealer.sell;

import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.DummyHttpRequest;
import com.firstlook.mock.MockMember;

public class TestSelectVehiclesSubmitAction extends BaseTestCase
{

private SelectVehiclesSubmitAction action;
private com.firstlook.mock.DummyHttpRequest request;
private com.firstlook.mock.MockMember mockMember;

public TestSelectVehiclesSubmitAction( String arg1 )
{
    super(arg1);
}

public void setup()
{
    action = new SelectVehiclesSubmitAction();
    request = new DummyHttpRequest();
    mockMember = new MockMember();
}

public void testAgedInventoryForward()
{
    request.setParameter("agedinventory.x", "agedinventory");
    String agedInventoryForward = action.getForwardMapping(request);
    assertEquals("agedInventory", agedInventoryForward);
}

public void testErrorForward()
{
    String searchForward = action.getForwardMapping(request);
    assertEquals("success", searchForward);
}

public void testFullInventoryForward()
{
    request.setParameter("fullinventory.x", "fullinventory");
    String fullInventoryForward = action.getForwardMapping(request);
    assertEquals("fullInventory", fullInventoryForward);
}

public void testReviewForward()
{
    request.setParameter("review.x", "review");
    String searchForward = action.getForwardMapping(request);
    assertEquals("review", searchForward);
}

public void testSaveAndComeBackForward()
{
    request.setParameter("saveAndComeBack.x", "saveAndComeBack");
    String saveAndComeBackForward = action.getForwardMapping(request);
    assertEquals("saveAndComeBack", saveAndComeBackForward);
}

public void testSearchForward()
{
    request.setParameter("search.x", "search");
    String searchForward = action.getForwardMapping(request);
    assertEquals("search", searchForward);
}

public void testSearchSubmitForwardEnterPressed()
{
    request.setParameter("searchPressed", "true");
    String searchSubmitForward = action.getForwardMapping(request);
    assertEquals("searchSubmit", searchSubmitForward);
}

public void testSearchSubmitForwardImageClicked()
{
    request.setParameter("searchSubmit.x", "searchSubmit");
    request.setParameter("searchPressed", "false");
    String searchSubmitForward = action.getForwardMapping(request);
    assertEquals("searchSubmit", searchSubmitForward);
}

public void testSelectForward()
{
    request.setParameter("select.x", "select");
    String searchForward = action.getForwardMapping(request);
    assertEquals("select", searchForward);
}

public void testWorksheetForward()
{
    request.setParameter("worksheet.x", "worksheet");
    String searchForward = action.getForwardMapping(request);
    assertEquals("worksheet", searchForward);
}

}
