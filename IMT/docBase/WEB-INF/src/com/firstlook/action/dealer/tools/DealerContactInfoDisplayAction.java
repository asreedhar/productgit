package com.firstlook.action.dealer.tools;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.dealer.buy.VehicleDisplayBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.Member;
import com.firstlook.entity.RedistributionLog;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.redistributionlog.RedistributionLogService;

public class DealerContactInfoDisplayAction extends VehicleDisplayBaseAction
{

private RedistributionLogService redistributionLogService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	InventoryEntity inventory = putVehicleInRequest( request, getImtDealerService() );
	Dealer contact = putSpecificDealerFormInRequest(request, inventory.getDealerId());
	request.setAttribute("displayUnitCost", contact.getDealerPreference().getDisplayUnitCostToDealerGroup());
	
	Member member = getMemberFromRequest( request );

	redistributionLogService.save( member.getMemberId().intValue(), inventory.getDealerId(),
					getFirstlookSessionFromRequest( request ).getCurrentDealerId(),
					inventory.getStockNumber(), inventory.getVin(), RedistributionLog.FLASH_LOCATE_SOURCE_ID );

	return mapping.findForward( "success" );
}

public void setRedistributionLogService( RedistributionLogService redistributionLogService )
{
	this.redistributionLogService = redistributionLogService;
}
}