package com.firstlook.action.dealer.redistribution;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.person.IPerson;
import biz.firstlook.transact.persist.person.IPosition;
import biz.firstlook.transact.persist.person.PositionType;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.form.ManagePeopleForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;

public class ManagePeopleRemoveAction extends SecureBaseAction{

    @Override
    public ActionForward doIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws DatabaseException, ApplicationException {
        ManagePeopleForm theForm = (ManagePeopleForm) form;
        
        Integer personId = Integer.parseInt(request.getParameter("personId"));
        PositionType type = PositionType.valueFromPositionString(theForm.getPosition());
        
        IPerson person = getPersonService().getPerson(personId);
        Iterator<IPosition> posItr = person.getPositions().iterator();
        while (posItr.hasNext()) {
            IPosition pos = posItr.next();
            if (pos.getDescription().equals(theForm.getPosition())) {
            	posItr.remove();
            }
        }
        
        getPersonService().savePerson(person);
        
        theForm.setEditMode(false);
        
        SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
        response.addHeader( "posId", type.getId().toString() );
        return mapping.findForward("success");
    }

}
