package com.firstlook.action.dealer.redistribution;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.service.appraisal.AppraisalFormOptions;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;

public class AppraisalFormSaveAction extends SecureBaseAction
{

private IAppraisalService appraisalService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );
	
	if (!NumberUtils.isDigits(request.getParameter("appraisalId"))) {
		return null;
	}
	
	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	int appraisalId = Integer.parseInt( request.getParameter( "appraisalId" ) );
	String offer = request.getParameter( "offer" );
	int customerOffer = formatOffer( offer );
	boolean includeMileageAdjustment = request.getParameter( "includeMileageAdjustment" ) == null ? false : true;
	boolean showKBBConsumerValues = request.getParameter( "kbbConsumerValue" ) == null ? false : true;
	boolean showOptions = request.getParameter( "options" ) == null ? false : true;
	boolean showReconCost=request.getParameter( "reconCost" ) == null ? false : true;
	boolean showPhotos = request.getParameter( "photos" ) == null ? false : true;
	Integer bitMaskFromThirdPartyCategories = getBitMaskFromThirdPartyCetegories(request);
	
	IAppraisal appraisal = appraisalService.findBy( appraisalId );
	AppraisalFormOptions formOptions = appraisal.getAppraisalFormOptions(); 
	
	if (updateAppraisalModifiedDate(customerOffer, includeMileageAdjustment, showKBBConsumerValues, showOptions, showPhotos, bitMaskFromThirdPartyCategories, formOptions)) 
		appraisal.setDateModified(new Date());
	
	if ( formOptions.getAppraisalFormId() == null ||  formOptions.getAppraisalFormId().intValue() == 0) // new FormOptions
	{	// init with preferences if first form
		Dealer currentDealer = getImtDealerService().retrieveDealer( currentDealerId );
		formOptions.setShowPhotos( currentDealer.getDealerPreference().getAppraisalFormShowPhotos() );
		formOptions.setShowEquipmentOptions( currentDealer.getDealerPreference().getAppraisalFormShowOptions() );
		formOptions.setShowReconCost(showReconCost);
	}

	// Only change if new offer is entered.
	if ( formOptions.getAppraisalFormOffer() == null || formOptions.getAppraisalFormOffer() != customerOffer )
	{
		formOptions.setAppraisalFormHistory( formOptions.getAppraisalFormOffer() );
		formOptions.setAppraisalFormOffer( customerOffer );
	}

	if ( request.getParameter( "inline" ) == null || !request.getParameter( "inline" ).equalsIgnoreCase( "true" ) )
	{	// don't save this stuff if its this is an inline request
		formOptions.setShowEquipmentOptions( showOptions );
		formOptions.setShowReconCost(showReconCost);
		formOptions.setShowPhotos( showPhotos );
		formOptions.setIncludeMileageAdjustment( includeMileageAdjustment );
		formOptions.setShowKbbConsumerValue( showKBBConsumerValues );
		formOptions.setCategoriesToDisplayBitMask( bitMaskFromThirdPartyCategories );
	}

	getAppraisalService().updateAppraisal( appraisal );
	return null;
}

private boolean updateAppraisalModifiedDate(int customerOffer, boolean includeMileageAdjustment, boolean showKBBConsumerValues, boolean showOptions, boolean showPhotos, Integer bitMaskFromThirdPartyCategories, AppraisalFormOptions formOptions) {
	boolean updated = false;
	if (formOptions.getAppraisalFormOffer().intValue() != customerOffer) {
		updated = true;
	} else if (formOptions.isShowPhotos() != showPhotos) {
		updated = true;		
	} else if (formOptions.isShowKbbConsumerValue() != showKBBConsumerValues) {
		updated = true;				
	} else if (formOptions.isIncludeMileageAdjustment() != includeMileageAdjustment) {
		updated = true;				
	} else if (formOptions.isShowEquipmentOptions() != showOptions) {
		updated = true;				
	} else if (formOptions.getCategoriesToDisplayBitMask().intValue() != bitMaskFromThirdPartyCategories) {
		updated = true;
	}
	return updated;
}

private Integer getBitMaskFromThirdPartyCetegories(HttpServletRequest request) {
	String[] categoriesToDisplayString = request.getParameterValues( "categoriesToDisplay" );
	Integer[] categoriesToDisplay = null;
	if ( categoriesToDisplayString != null ) {
		categoriesToDisplay = new Integer[categoriesToDisplayString.length ]; 
		for (int c = 0; c < categoriesToDisplayString.length; c++ ) {
			categoriesToDisplay[c] = Integer.parseInt( categoriesToDisplayString[c] );
		}
		
	}
	Integer bitMaskFromThirdPartyCategories = AppraisalFormOptions.getBitMaskFromThirdPartyCategories( categoriesToDisplay );
	return bitMaskFromThirdPartyCategories;
}

private int formatOffer( String offer )
{
	int indexOfPeriod = offer.indexOf( "." );
	if ( indexOfPeriod >= 0 )
	{
		offer = offer.substring( 0, indexOfPeriod );
	}
	int indexOfComma = offer.indexOf( "," );
	if ( indexOfComma >= 0 )
	{
		offer = offer.substring( 0, indexOfComma ) + offer.substring( ( indexOfComma + 1 ), offer.length() );
	}
	int indexOfDollar = offer.indexOf( "$" );
	if ( indexOfDollar >= 0 )
	{
		offer = offer.substring( ( indexOfDollar + 1 ), offer.length() );
	}

	return Integer.parseInt( offer );
}

public IAppraisalService getAppraisalService()
{
	return appraisalService;
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

}
