package com.firstlook.action.dealer.reports.performance;

import org.displaytag.decorator.ColumnDecorator;
import org.displaytag.exception.DecoratorException;

public class InitialsColumnDecorator implements ColumnDecorator
{

public String decorate( Object initialsColumnValue ) throws DecoratorException
{
    String columnString = initialsColumnValue == null ? "&nbsp;"
            : initialsColumnValue.toString() + "&nbsp;";
    return columnString;
}

}
