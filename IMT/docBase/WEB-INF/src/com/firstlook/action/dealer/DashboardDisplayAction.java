package com.firstlook.action.dealer;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.dealer.helper.AgingPlanReportRangeSetEnum;
import com.firstlook.action.dealer.helper.AgingReportHelper;
import com.firstlook.action.dealer.helper.DealerFactsHelper;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryBucket;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.InventorySalesAggregate;
import com.firstlook.entity.Member;
import com.firstlook.entity.ProgramTypeEnum;
import com.firstlook.entity.VehicleSaleEntity;
import com.firstlook.entity.lite.InventoryLite;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.action.ReportActionHelper;
import com.firstlook.persistence.inventory.InventorySalesAggregateService;
import com.firstlook.persistence.report.IInventoryBucketDAO;
import com.firstlook.report.InventoryStockingReport;
import com.firstlook.report.Report;
import com.firstlook.report.ReportForm;
import com.firstlook.service.dealer.IUCBPPreferenceService;
import com.firstlook.service.inventory.InventoryService_Legacy;
import com.firstlook.service.inventorystocking.InventoryStockingService;
import com.firstlook.session.FirstlookSession;

public class DashboardDisplayAction extends SecureBaseAction
{

private InventoryStockingService inventoryStockingService;
private InventorySalesAggregateService inventorySalesAggregateService;
private IInventoryBucketDAO inventoryBucketDAO;
private ReportActionHelper reportActionHelper;
private IUCBPPreferenceService ucbpPreferenceService;
private InventoryService_Legacy inventoryService_Legacy; 

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	FirstlookSession firstlookSession = getFirstlookSessionFromRequest( request ); 

	int inventoryType = getMemberFromRequest(request).getInventoryType().getValue();
	int forecast = ReportActionHelper.determineForecastAndSetOnRequest( request );
	int currentDealerId = firstlookSession.getCurrentDealerId();
	Dealer currentDealer = getImtDealerService().retrieveDealer( currentDealerId );
	Member member = getMemberFromRequest(request );
	int weeks = ReportActionHelper.determineWeeksAndSetOnRequest( request, forecast, currentDealer );
	String programType = member.getProgramType();
	
	request.setAttribute( "member", member );
	request.setAttribute( "dealerId", "" + currentDealerId );
	putDealerFormInRequest( request, 0 );


	putReportInRequest( currentDealer, weeks, forecast, request, inventoryType );
	if ( programType.equals( ProgramTypeEnum.VIP.getName() ) )
	{
        Integer rangeSetId = null;
        if ( inventoryType == InventoryEntity.USED_CAR )
        {
            rangeSetId = AgingPlanReportRangeSetEnum.USED_TOTAL_INVENTORY_REPORT_RANGE_SET.getId();
        }
        else
        {
            rangeSetId = AgingPlanReportRangeSetEnum.NEW_TOTAL_INVENTORY_REPORT_RANGE_SET.getId();
        }
        
        InventoryBucket rangeSet = inventoryBucketDAO.findByRangeSetId( rangeSetId );        
        InventorySalesAggregate aggregate = inventorySalesAggregateService.retrieveCachedInventorySalesAggregate( currentDealerId, weeks, 0,
                                                                                                                  inventoryType );
		AgingReportHelper agingReportHelper = new AgingReportHelper(inventoryService_Legacy);
        agingReportHelper.putAgingReportRangesInRequest( currentDealer,
                request, InventoryLite.TRUE, null, rangeSet, aggregate );
		putStockingReportInRequest( currentDealerId, inventoryType, request );
	}
	else
	{
		DealerFactsHelper helper = new DealerFactsHelper( getDealerFactsDAO() );
		helper.putSaleMaxPolledDateInRequest( currentDealerId, request );
	}

	Integer mileage = new Integer( ucbpPreferenceService.retrieveDealerRisk(
																( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId() ).getHighMileageThreshold());
	request.setAttribute( "mileage", mileage );


	return mapping.findForward( "success" );
}

private void putReportInRequest( Dealer dealer, int weeks, int forecast, HttpServletRequest request, int inventoryType )
		throws DatabaseException, ApplicationException
{
	Report report = reportActionHelper.createSalesReport( dealer, weeks, forecast, VehicleSaleEntity.MILEAGE_MAXVAL, inventoryType );

	ReportForm reportForm = new ReportForm( report );
	request.setAttribute( "reportAverages", reportForm );
	request.setAttribute( "report", reportForm );
}

private void putStockingReportInRequest( int currentDealerId, int inventoryType, HttpServletRequest request )
{
	Collection<InventoryStockingReport> inventoryStockingCollection = inventoryStockingService.retrieveInventoryStockingReport( currentDealerId, inventoryType );
	request.setAttribute( "inventoryStockingReport", inventoryStockingCollection );
}

public InventoryStockingService getInventoryStockingService()
{
    return inventoryStockingService;
}

public void setInventoryStockingService( InventoryStockingService inventoryStockingService )
{
    this.inventoryStockingService = inventoryStockingService;
}

public InventorySalesAggregateService getInventorySalesAggregateService()
{
    return inventorySalesAggregateService;
}

public void setInventorySalesAggregateService( InventorySalesAggregateService inventorySalesAggregateService )
{
    this.inventorySalesAggregateService = inventorySalesAggregateService;
}

public IInventoryBucketDAO getInventoryBucketDAO()
{
	return inventoryBucketDAO;
}

public void setInventoryBucketDAO( IInventoryBucketDAO inventoryBucketDAO )
{
	this.inventoryBucketDAO = inventoryBucketDAO;
}

public void setReportActionHelper( ReportActionHelper reportActionHelper )
{
	this.reportActionHelper = reportActionHelper;
}

public void setUcbpPreferenceService( IUCBPPreferenceService ucbpPreferenceService )
{
	this.ucbpPreferenceService = ucbpPreferenceService;
}

public void setInventoryService_Legacy(InventoryService_Legacy inventoryService) {
	this.inventoryService_Legacy = inventoryService;
}

}