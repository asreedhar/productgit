package com.firstlook.action.dealer.tools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.model.BookOut;
import biz.firstlook.transact.persist.model.BookOutValue;
import biz.firstlook.transact.persist.model.BookOutValueType;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.form.ThirdPartyVehicleOptionForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.SessionHelper;
import com.firstlook.persistence.states.StatesDAO;
import com.firstlook.service.bookout.BookOutService;
import com.firstlook.session.FirstlookSession;
import com.firstlook.thirdparty.book.kbb.KelleyWindowSticker;

public class PrintableWindowStickerDisplayAction extends SecureBaseAction
{

private IAppraisalService appraisalService;

public PrintableWindowStickerDisplayAction()
{
	super();
}

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	KelleyWindowSticker windowStickerDataHolder = new KelleyWindowSticker();

	int appraisalId = RequestHelper.getInt( request, "appraisalId" );

	int businessunitId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();

	String stockNumber = (String)request.getParameter( "stockNumber" );

	Integer sellingPrice = retrieveSellingPrice( request );

	Integer marketValue = Integer.valueOf( 0 );
	int suggestedRetailValue = Integer.MIN_VALUE;

	Dealer dealer = getImtDealerService().retrieveDealer( businessunitId );

	int guideBookId = ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE;

	IAppraisal appraisal = getAppraisalService().findBy( appraisalId );
	if ( sellingPrice != null || stockNumber != null )
	{
		appraisal.updateWindowSticker( stockNumber, sellingPrice );
	}
	List<ThirdPartyVehicleOption> appraisalGuideBookOptions = new ArrayList<ThirdPartyVehicleOption>();
	List<ThirdPartyVehicleOption> appraisalGuideBookEngines = new ArrayList<ThirdPartyVehicleOption>();
	List<ThirdPartyVehicleOption> appraisalGuideBookDrivetrains = new ArrayList<ThirdPartyVehicleOption>();
	List<ThirdPartyVehicleOption> appraisalGuideBookTransmissions = new ArrayList<ThirdPartyVehicleOption>();
	BookOutService.populateSelectedOptionsList( guideBookId, appraisalGuideBookOptions, appraisalGuideBookEngines,
												appraisalGuideBookDrivetrains, appraisalGuideBookTransmissions, appraisal );

	BookOut bookOut = appraisal.getLatestBookOut( guideBookId );
	String bottomLineDisplay = "";
	Integer bottomLineValue = Integer.valueOf( 0 );
	Boolean hasBookOut = Boolean.FALSE;
	if ( bookOut != null ) {
		hasBookOut = Boolean.TRUE;
		suggestedRetailValue = determineRetailValue( suggestedRetailValue, bookOut );
		marketValue = Integer.valueOf( suggestedRetailValue - bookOut.getMileageCostAdjustment().intValue() );

		//IStatesDAO statePersistence = new StatesDAO();
		if ( bookOut.getRegion() != null )
		{
			//appraisal.setKelleyPublishState( statePersistence.findStateLongName( bookOut.getRegion().substring( 0, 2 ) ) );
		}
	}

	bottomLineDisplay = determineBottomLineDisplay( sellingPrice, marketValue );
	bottomLineValue = determineBottomeLineValue( sellingPrice, marketValue );
	populateWindowStickerBean( windowStickerDataHolder, stockNumber, dealer.getName(), appraisal, appraisalGuideBookOptions,
								appraisalGuideBookEngines, appraisalGuideBookDrivetrains, appraisalGuideBookTransmissions, bottomLineDisplay,
								bottomLineValue, suggestedRetailValue, guideBookId, bookOut );

	request.setAttribute( "windowStickerData", windowStickerDataHolder );
	request.setAttribute( "hasBookOut", hasBookOut );
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );
	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
	return mapping.findForward( "success" );
}

private Integer retrieveSellingPrice( HttpServletRequest request )
{
	String sellingPriceString = (String)request.getParameter( "sellingPrice" );
	Integer sellingPrice = null;
	if ( sellingPriceString != null && !sellingPriceString.equals( "" ) )
	{
		sellingPrice = new Integer( sellingPriceString );
	}
	return sellingPrice;
}

private Integer determineBottomeLineValue( Integer sellingPrice, Integer marketValue )
{
	if ( sellingPrice != null )
	{
		return sellingPrice;
	}
	else
	{
		return marketValue;
	}
}

private int determineRetailValue( int suggestedRetailValue, BookOut bookOut ) throws DatabaseException
{
	Set<BookOutValue> values = BookOutService.determineAppropriateBookOutValues(bookOut);
	BookOutValue retailValue = PrintableWindowStickerDisplayAction.retrieveBookOutValueByType( values, BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE );

	if ( retailValue != null && retailValue.getValue() != null )
	{
		suggestedRetailValue = retailValue.getValue().intValue();
	}
	return suggestedRetailValue;
}

//odd assumption here
private static BookOutValue retrieveBookOutValueByType( Set<BookOutValue> bookOutValues, int bookOutValueType )
{
    Iterator<BookOutValue> bookOutValuesIter = bookOutValues.iterator();
    BookOutValue bookOutValue;
    while (bookOutValuesIter.hasNext())
    {
        bookOutValue = bookOutValuesIter.next();
        if( bookOutValue.getBookOutValueType().getBookOutValueTypeId().intValue() == bookOutValueType )
        {
        	return bookOutValue;
        }
    }

    return null;
}


private Collection<ThirdPartyVehicleOptionForm> wrapOptionsInForm( Collection<ThirdPartyVehicleOption> appraisalGuideBookOptions )
{
	Collection<ThirdPartyVehicleOptionForm> appraisalGuideBookOptionsForms = new ArrayList<ThirdPartyVehicleOptionForm>();

	Iterator<ThirdPartyVehicleOption> appGuideBookOptionsIter = appraisalGuideBookOptions.iterator();
	ThirdPartyVehicleOption option;
	while ( appGuideBookOptionsIter.hasNext() )
	{
		option = appGuideBookOptionsIter.next();
		appraisalGuideBookOptionsForms.add( new ThirdPartyVehicleOptionForm( option ) );
	}

	return appraisalGuideBookOptionsForms;
}

private void populateWindowStickerBean( KelleyWindowSticker windowStickerDataHolder, String stockNumber, String name, IAppraisal appraisal,
										Collection<ThirdPartyVehicleOption> appraisalGuideBookOptions, Collection<ThirdPartyVehicleOption> appraisalGuideBookEngines,
										Collection<ThirdPartyVehicleOption> appraisalGuideBookDrivetrains, Collection<ThirdPartyVehicleOption> appraisalGuideBookTransmissions,
										String bottomLineDisplay, Integer bottomLineValue, int suggestedRetailValue, int guideBookId,
										BookOut bookOut )
{
	windowStickerDataHolder.setDealerName( name );
	windowStickerDataHolder.setStockNumber( stockNumber );
	windowStickerDataHolder.setYear( appraisal.getVehicle().getVehicleYear().toString() );
	windowStickerDataHolder.setMake( appraisal.getVehicle().getMake() );
	windowStickerDataHolder.setGuideBookDescription( appraisal.getVehicle().getModel() );
	windowStickerDataHolder.setVin( appraisal.getVehicle().getVin() );
	windowStickerDataHolder.setColor( appraisal.getColor() );
	windowStickerDataHolder.setAppraisalGuideBookEngines( appraisalGuideBookEngines );
	windowStickerDataHolder.setAppraisalGuideBookTransmissions( appraisalGuideBookTransmissions );
	windowStickerDataHolder.setAppraisalGuideBookDrivetrains( appraisalGuideBookDrivetrains );
	windowStickerDataHolder.setMileage( appraisal.getMileage() );
	windowStickerDataHolder.setAppraisalGuideBookOptions( wrapOptionsInForm( appraisalGuideBookOptions ) );
	windowStickerDataHolder.setBlueBookSuggestedRetailValue( new Integer( suggestedRetailValue ) );
	windowStickerDataHolder.setBottomLineDisplay( bottomLineDisplay );
	windowStickerDataHolder.setBottomLineValue( bottomLineValue );
	if ( bookOut != null )
	{
		windowStickerDataHolder.setKelleyPublishInfo( bookOut.getDatePublished() );
		windowStickerDataHolder.setPublishState( determineKelleyPublishState( bookOut.getDatePublished() ) );
	}
}

private String determineBottomLineDisplay( Integer sellingPrice, Integer marketValue )
{
	if ( sellingPrice != null )
	{
		return "Dealer's Selling Price ";
	}
	else
	{
		return "Market Value: ";
	}

}

private String determineKelleyPublishState( String publishInfo )
{
	if ( publishInfo != null && !publishInfo.equals( "" ) )
	{
		StatesDAO statesPersistence = new StatesDAO();
		return statesPersistence.findStateLongName( publishInfo.substring( 0, 2 ) );
	}
	else
	{
		return "";
	}
}

public IAppraisalService getAppraisalService()
{
	return appraisalService;
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

}
