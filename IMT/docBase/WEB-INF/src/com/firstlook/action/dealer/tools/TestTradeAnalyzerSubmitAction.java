package com.firstlook.action.dealer.tools;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.TradeAnalyzerEvent;
import com.firstlook.entity.form.TradeAnalyzerForm;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.DummyHttpRequest;
import com.firstlook.mock.MockMember;
import com.firstlook.mock.ObjectMother;
import com.firstlook.report.Report;

public class TestTradeAnalyzerSubmitAction extends BaseTestCase
{

private DummyHttpRequest request;
private TradeAnalyzerSubmitAction action;
private MockMember member;
private TradeAnalyzerForm form;
private TradeAnalyzerEvent tradeAnalyzerEvent;

public TestTradeAnalyzerSubmitAction( String arg1 )
{
    super(arg1);
}

public void setup()
{
    request = new DummyHttpRequest();
    action = new TradeAnalyzerSubmitAction();
    form = new TradeAnalyzerForm();

    member = new MockMember();
    Dealer dealer = ObjectMother.createDealer();
    dealer.setDealerId(new Integer(0));
    member.setCurrentDealer(dealer);

    tradeAnalyzerEvent = ObjectMother.createTradeAnalyzerEvent();
    mockDatabase.setReturnObjects(tradeAnalyzerEvent);
}

public void testCheckDealerGroupIncludeIsFalse() throws Exception
{
    form.setIncludeDealerGroup(Report.DEALERGROUP_INCLUDE_TRUE);
    request.setParameter("showDealer.x", "showDealer");
    action.checkDealerGroupInclude(request, form);

    assertEquals("Dealer was changed to false",
            Report.DEALERGROUP_INCLUDE_FALSE, form.getIncludeDealerGroup());

}

public void testCheckDealerGroupIncludeIsTrue() throws Exception
{
    request.setParameter("showDealerGroup.x", "showDealerGroup");
    action.checkDealerGroupInclude(request, form);

    assertEquals("Dealer Group was changed ", Report.DEALERGROUP_INCLUDE_TRUE,
            form.getIncludeDealerGroup());

}

}
