package com.firstlook.action.dealer.tools;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import biz.firstlook.module.vehicle.description.old.BookOutDatasetInfo;
import biz.firstlook.module.vehicle.description.old.BookOutTypeEnum;
import biz.firstlook.module.vehicle.description.old.GBException;
import biz.firstlook.module.vehicle.description.old.GuideBookInput;
import biz.firstlook.module.vehicle.description.old.GuideBookMetaInfo;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.model.BookOutValueType;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.service.ThirdPartyVehicleOptionService;

import com.firstlook.display.tradeanalyzer.DisplayGuideBook;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.thirdparty.book.nada.NADABookoutService;

public class NADAManualBookoutAction extends AbstractManualBookOutAction {

	private NADABookoutService nadaBookoutService;

	protected String getGuideBookName() {
		return "NADA";
	}

	protected String processManualRequest(HttpServletRequest request, Dealer currentDealer, String selected) throws ApplicationException {
		String forward = "success";
		List<Integer> modelYears = null;
		try {
			modelYears = nadaBookoutService.retrieveModelYears(null);
		} catch (Exception e) {
			modelYears = ManualBookoutUtils.getInstance().getYears();
		}
		request.setAttribute("years", modelYears);
		if (selected.equalsIgnoreCase("make")) {
			forward = loadMakes(request);
		} else if (selected.equalsIgnoreCase("model")) {
			forward = loadModels(request);
		} else if (selected.equalsIgnoreCase("trim")) {
			forward = loadTrims(request);
		} else if (selected.equalsIgnoreCase("getOptions")) {
			forward = loadOptions(request, currentDealer);
		} else if (selected.equalsIgnoreCase("getValues")) {
			forward = loadValues(request, currentDealer);
		} else if (selected.equalsIgnoreCase("print")) {
			print(request, currentDealer);
			forward = "print";
		} 
		return forward;
	}
	
	public String loadMakes(HttpServletRequest request) {
		String selectedYear = request.getParameter("year");
		List<VDP_GuideBookVehicle> makes = new ArrayList<VDP_GuideBookVehicle>();
		try {
			makes = nadaBookoutService.retrieveMakes(selectedYear, null);
		} catch (Exception e) {
			request.setAttribute("error", Boolean.TRUE);
			request.setAttribute("showOptions", Boolean.FALSE);
			request.setAttribute("showValues", Boolean.FALSE);
			return "optionSuccess";
		}

		request.setAttribute("makes", makes);
		return "makeSuccess";
	}

	public String loadModels(HttpServletRequest request) {
		String selectedYear = request.getParameter("year");
		String selectedMake = request.getParameter("make");
		List<VDP_GuideBookVehicle> models = new ArrayList<VDP_GuideBookVehicle>();
		try {
			models = nadaBookoutService.retrieveModels(selectedYear,
					selectedMake, null);
		} catch (Exception e) {
			request.setAttribute("error", Boolean.TRUE);
			request.setAttribute("showOptions", Boolean.FALSE);
			request.setAttribute("showValues", Boolean.FALSE);
			return "optionSuccess";
		}
		request.setAttribute("models", models);
		return "modelSuccess";
	}

	public String loadTrims(HttpServletRequest request) {
		String selectedYear = request.getParameter("year");
		String selectedMake = request.getParameter("make");
		String selectedModel = request.getParameter("model");
		List<VDP_GuideBookVehicle> trims = new ArrayList<VDP_GuideBookVehicle>();
		try {
			trims = nadaBookoutService.retrieveTrims(selectedYear,
					selectedMake, selectedModel, null);
		} catch (Exception e) {
			request.setAttribute("error", Boolean.TRUE);
			request.setAttribute("showOptions", Boolean.FALSE);
			request.setAttribute("showValues", Boolean.FALSE);
			return "optionSuccess";
		}
		request.setAttribute("trims", trims);
		return "trimSuccess";
	}

	public String loadOptions(HttpServletRequest request, Dealer currentDealer) {
		String selectedYear = request.getParameter("year");
		String selectedMake = request.getParameter("make");
		String selectedModel = request.getParameter("model");
		String mileage = request.getParameter("mileage");
		String selectedTrim = request.getParameter("catalogKeyId");
		String[] selectedEquipmentOptionKeys = request
				.getParameterValues("selectedEquipmentOptionKeys");

		DisplayGuideBook displayGuideBook = new DisplayGuideBook();
		
		GuideBookMetaInfo publishInfo = null;
		String region = "";
		
		try {
			buildDisplayGuideBook(currentDealer, selectedYear, selectedMake,
					selectedModel, mileage, selectedTrim,
					selectedEquipmentOptionKeys, displayGuideBook);
			
			GuideBookInput input = new GuideBookInput();
			input.setNadaRegionCode(currentDealer.getNadaRegionCode());
			region = nadaBookoutService.retrieveRegionDescription(input);
			
			publishInfo = nadaBookoutService.getMetaInfo(region,
					new BookOutDatasetInfo(BookOutTypeEnum.APPRAISAL,
							currentDealer.getDealerId()));
		} catch (Exception e) {
			request.setAttribute("error", Boolean.TRUE);
		}

		displayGuideBook.setYear(selectedYear);
		displayGuideBook.setGuideBookId(2);
		displayGuideBook.setGuideBookName("NADA");
		
		request.setAttribute("showOptions", Boolean.TRUE);
		request.setAttribute("showValues", Boolean.FALSE);
		request.setAttribute("publishInfo", publishInfo);
		displayGuideBook.setFooter("All NADA values are reprinted with permission of N.A.D.A Official Used Car Guide Company NADASC 2000");
		request.setAttribute("displayGuideBook", displayGuideBook);
		return "optionSuccess";
	}
	
	private VDP_GuideBookValue getOptionAdjustmentValue(List<VDP_GuideBookValue> values, DealerPreference dealerPref) {
		int thirdPartyCategoryId = ThirdPartyCategory.NADA_CLEAN_LOAN.getThirdPartyId();
		
		//The app defaults to the loan value no matter what.
//		if(ThirdPartyDataProvider.NADA.getThirdPartyId().equals(dealerPref.getGuideBook2Id())) {
//			thirdPartyCategoryId = dealerPref.getGuideBook2BookOutPreferenceId();
//		} else { //assume 1st book is nada
//			thirdPartyCategoryId = dealerPref.getBookOutPreferenceId();
//		}
		VDP_GuideBookValue optionAdjustment = null;
		for(VDP_GuideBookValue value : values) {
			if(value.getValueType() == BookOutValueType.BOOK_OUT_VALUE_OPTIONS_VALUE
					&& value.getThirdPartyCategoryId() == thirdPartyCategoryId) {
				optionAdjustment = value;
				break;
			}
		}
		return optionAdjustment;
	}

	public void print(HttpServletRequest request, Dealer currentDealer) {
		request.setAttribute("make", request.getParameter("make_s"));
		request.setAttribute("model", request.getParameter("model_s"));
		request.setAttribute("trim", request.getParameter("trim_s"));
		request.setAttribute("notes", request.getParameter("notes_s"));
		String selectedYear = request.getParameter("year");
		String selectedMake = request.getParameter("make");
		String selectedModel = request.getParameter("model");
		String selectedTrim = request.getParameter("catalogKeyId");
		String mileage = request.getParameter("mileage");
		String[] selectedEquipmentOptionKeys = request
				.getParameterValues("selectedEquipmentOptionKeys");
		DisplayGuideBook displayGuideBook = new DisplayGuideBook();
		String region = "";

		GuideBookMetaInfo publishInfo = null;
		try {
			GuideBookInput input = new GuideBookInput();
			input.setNadaRegionCode(currentDealer.getNadaRegionCode());
			region = nadaBookoutService.retrieveRegionDescription(input);

			publishInfo = nadaBookoutService.getMetaInfo(region,
					new BookOutDatasetInfo(BookOutTypeEnum.APPRAISAL,
							currentDealer.getDealerId()));
			
			buildDisplayGuideBook(currentDealer, selectedYear, selectedMake,
					selectedModel, mileage, selectedTrim,
					selectedEquipmentOptionKeys, displayGuideBook);
			
		} catch (Exception e) {
			request.setAttribute("error", Boolean.TRUE);
			request.setAttribute("showOptions", Boolean.FALSE);
			request.setAttribute("showValues", Boolean.FALSE);
		}

		displayGuideBook.setYear(selectedYear);
		displayGuideBook.setFooter("All NADA values are reprinted with permission of N.A.D.A Official Used Car Guide Company NADASC 2000");
		displayGuideBook.setRegion(region);
		request.setAttribute("publishInfo", publishInfo);
		request.setAttribute("displayGuideBook", displayGuideBook);

	}

	public String loadValues(HttpServletRequest request, Dealer currentDealer) {
		String selectedYear = request.getParameter("year");
		String selectedMake = request.getParameter("make");
		String selectedModel = request.getParameter("model");
		String selectedTrim = request.getParameter("catalogKeyId");
		String mileage = request.getParameter("mileage");
		String[] selectedEquipmentOptionKeys = request
				.getParameterValues("selectedEquipmentOptionKeys");
		DisplayGuideBook displayGuideBook = new DisplayGuideBook();
		
		GuideBookMetaInfo publishInfo = null;
		String region = "";

		try {
			buildDisplayGuideBook(currentDealer, selectedYear, selectedMake,
					selectedModel, mileage, selectedTrim,
					selectedEquipmentOptionKeys, displayGuideBook);
			
			GuideBookInput input = new GuideBookInput();
			input.setNadaRegionCode(currentDealer.getNadaRegionCode());
			region = nadaBookoutService.retrieveRegionDescription(input);
			
			publishInfo = nadaBookoutService.getMetaInfo(region,
					new BookOutDatasetInfo(BookOutTypeEnum.APPRAISAL,
							currentDealer.getDealerId()));
		} catch (Exception e) {
			request.setAttribute("error", Boolean.TRUE);
			request.setAttribute("showOptions", Boolean.TRUE);
			request.setAttribute("showValues", Boolean.TRUE);
			//total hack here.  NADA doesn't have option conflict implemented when it should have!
			request.setAttribute("errorMessage", e.getMessage());
		}

		displayGuideBook.setYear(selectedYear);
		displayGuideBook
				.setFooter("All NADA values are reprinted with permission of N.A.D.A Official Used Car Guide Company NADASC 2000");		
		request.setAttribute("showOptions", Boolean.TRUE);
		request.setAttribute("showValues", Boolean.TRUE);
		request.setAttribute("publishInfo", publishInfo);
		request.setAttribute("displayGuideBook", displayGuideBook);
		return "optionSuccess";
	}
	
	private void buildDisplayGuideBook(Dealer currentDealer,
			String selectedYear, String selectedMake, String selectedModel,
			String mileage, String selectedTrim,
			String[] selectedEquipmentOptionKeys,
			DisplayGuideBook displayGuideBook) throws GBException {
		displayGuideBook.setDisplayGuideBookOptions(nadaBookoutService
				.retrieveOptions(String.valueOf(currentDealer.getNadaRegionCode()),
						selectedYear, selectedMake, selectedModel,
						selectedTrim, null));
		
		if (selectedEquipmentOptionKeys != null) {
			displayGuideBook
					.setSelectedEquipmentOptionKeys(ThirdPartyVehicleOptionService
							.setAndUpdateSelectedOptionsUsingArray(
									displayGuideBook
											.getDisplayEquipmentGuideBookOptions(),
									selectedEquipmentOptionKeys));
		}
		
		List<VDP_GuideBookValue> values = nadaBookoutService
		.retrieveBookValues(String.valueOf(currentDealer.getNadaRegionCode()),
				Integer.parseInt(mileage), displayGuideBook
						.getDisplayGuideBookOptions(),
				selectedYear, selectedMake, selectedModel,
				selectedTrim, null);

		//this is a hack to get things to work. I am not proud. -bf.
		if(!values.isEmpty()) {
			VDP_GuideBookValue isError = values.get(0);
			if(isError.isHasErrors()) {
				throw new GBException(isError.getError().getErrorMessage());
			}
		}
		
		displayGuideBook.setDisplayGuideBookValues(getFinalValues(values));
		displayGuideBook.setGuideBookOptionAdjustment(getOptionAdjustmentValue(values, currentDealer.getDealerPreference()));
		VDP_GuideBookValue mileageAdjustment = getMileageAdjustmentValue(values);
		if(mileageAdjustment != null) {
			displayGuideBook.setMileageAdjustment(mileageAdjustment.getValue());
		}
	}

	public void setDisplayGuideBookInfo(DisplayGuideBook displayGuideBook) {
		displayGuideBook
				.setGuideBookId(ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE);
		displayGuideBook.setImageName(ThirdPartyDataProvider.IMAGE_NAME_NADA);
		displayGuideBook.setGuideBookName("NADA");

	}

	public void setNadaBookoutService(
			NADABookoutService nadaBookoutService) {
		this.nadaBookoutService = nadaBookoutService;
	}

}
