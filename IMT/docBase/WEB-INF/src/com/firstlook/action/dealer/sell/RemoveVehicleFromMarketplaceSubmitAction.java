package com.firstlook.action.dealer.sell;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.service.inventory.InventoryService_Legacy;
import com.firstlook.session.FirstlookSession;

public class RemoveVehicleFromMarketplaceSubmitAction extends SecureBaseAction
{

private InventoryService_Legacy inventoryService_Legacy;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    int currentDealerId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();
    int vehicleId = RequestHelper.getInt( request, "vehicleId" );

    Member member = getMemberFromRequest( request );
    InventoryEntity inventory = inventoryService_Legacy.retrieveInventoryForValidMember( member.getMemberType().intValue(), vehicleId, currentDealerId );

    inventoryService_Legacy.saveOrUpdate( inventory );

    return mapping.findForward( "success" );
}

public void setInventoryService_Legacy(InventoryService_Legacy inventoryService) {
	this.inventoryService_Legacy = inventoryService;
}

}
