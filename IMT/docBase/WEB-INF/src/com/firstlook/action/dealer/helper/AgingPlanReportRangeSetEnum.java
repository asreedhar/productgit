package com.firstlook.action.dealer.helper;

public enum AgingPlanReportRangeSetEnum {

	AGING_INVENTORY_REPORT_RANGE_SET(1, "AGING_INVENTORY_REPORT_RANGE_SET"),
	NEW_TOTAL_INVENTORY_REPORT_RANGE_SET(2, "NEW_TOTAL_INVENTORY_REPORT_RANGE_SET"),
	USED_TOTAL_INVENTORY_REPORT_RANGE_SET(3, "USED_TOTAL_INVENTORY_REPORT_RANGE_SET");
	
	private int id;
	private String description;
	
	private  AgingPlanReportRangeSetEnum(int id, String desc) {
		this.id = id;
		this.description = desc;
	}

	public int getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}
	
	
	
	
}
