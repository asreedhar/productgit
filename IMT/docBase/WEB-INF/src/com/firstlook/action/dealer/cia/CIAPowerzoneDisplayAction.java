package com.firstlook.action.dealer.cia;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ReverseComparator;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.cia.model.CIACategory;
import biz.firstlook.cia.model.CIAGroupingItem;
import biz.firstlook.cia.service.BasisPeriodService;
import biz.firstlook.cia.service.CIAGroupingItemService;
import biz.firstlook.cia.service.CIASummaryService;
import biz.firstlook.transact.persist.model.CIAPreferences.TimePeriod;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.service.cia.ICIAService;
import com.firstlook.service.cia.report.CIAReportingUtility;

public class CIAPowerzoneDisplayAction extends SecureBaseAction
{
private CIAGroupingItemService ciaGroupingItemService;
private CIASummaryService ciaSummaryService;

// new stuff
// TODO - add to spring!!!
private ICIAService ciaService;
private BasisPeriodService basisPeriodService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	// get from the request
	int businessUnitId = ( getFirstlookSessionFromRequest( request ) ).getCurrentDealerId();

	int segmentId = RequestHelper.getInt( request, "segmentId" );
	int ciaCategory = RequestHelper.getInt( request, "ciaCategory" );  //?? this is just for powerzone anyways
	int groupingDescriptionId = RequestHelper.getInt( request, "groupingDescriptionId" ); //possible remove

	// call the service and to prepare results for display
	//TODO: refactor this out to use ciaService.getCIADetailPageDIsplayItemWithPrefs()
	
	List<CIAGroupingItem> ciaGroupingItems = ciaService.getCIAGroupingItems( businessUnitId, segmentId, CIACategory.POWERZONE_CATEGORY );
	List displayItems = ciaService.getCIAPowerzoneDetailPageDisplayItems(businessUnitId, ciaGroupingItems);
	Map groupingPreferences  = CIAReportingUtility.createPreferencesMapForCIA(ciaGroupingItems);
	
	// MH - 02/24/2005 - We will eventually implement the ability to toggle
	// between the forecast and prior period views of the pap report line
	// items. For now, this parameter will always be set to false
	int isForecast = 0;
	int weeks = basisPeriodService.calculateNumberOfWeeksInBasisPeriodByType( new Integer( businessUnitId ),
																				TimePeriod.CIA_CORE_MODEL_DETERMINATION, isForecast );

	Dealer currentDealer = getImtDealerService().retrieveDealer( getFirstlookSessionFromRequest( request ).getCurrentDealerId() );
	
	ReverseComparator valuationComparator = new ReverseComparator( new BeanComparator( "valuation" ) );
	Collections.sort( displayItems, valuationComparator );

	// populate request for display
	request.setAttribute( "ciaGroupingItems", displayItems );
	request.setAttribute( "groupingPreferences", groupingPreferences );
	request.setAttribute( "groupingDescriptionId", new Integer( groupingDescriptionId ) );
	request.setAttribute( "weeks", new Integer( weeks ) );
	request.setAttribute( "forecast", new Integer( isForecast ) );
	request.setAttribute( "mileageFilter", new Integer( 0 ) );
	request.setAttribute( "nickname", currentDealer.getNickname() );
	request.setAttribute( "segmentId", new Integer( segmentId ) );
	request.setAttribute( "ciaCategory", new Integer( ciaCategory ) );

	return mapping.findForward( "success" );
}

public CIAGroupingItemService getCiaGroupingItemService()
{
	return ciaGroupingItemService;
}

public void setCiaGroupingItemService( CIAGroupingItemService ciaGroupingItemService )
{
	this.ciaGroupingItemService = ciaGroupingItemService;
}

public CIASummaryService getCiaSummaryService()
{
	return ciaSummaryService;
}

public void setCiaSummaryService( CIASummaryService ciaSummaryService )
{
	this.ciaSummaryService = ciaSummaryService;
}

public ICIAService getCiaService()
{
	return ciaService;
}

public void setCiaService( ICIAService ciaService )
{
	this.ciaService = ciaService;
}

public BasisPeriodService getBasisPeriodService()
{
	return basisPeriodService;
}

public void setBasisPeriodService( BasisPeriodService basisPeriodService )
{
	this.basisPeriodService = basisPeriodService;
}

}