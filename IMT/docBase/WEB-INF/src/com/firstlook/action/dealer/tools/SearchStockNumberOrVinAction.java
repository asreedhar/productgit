package com.firstlook.action.dealer.tools;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.VinUtility;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.Inventory;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;
import biz.firstlook.transact.persist.persistence.IInventoryDAO;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.dealer.vehicledetailpage.SearchStockNumberOrVinForm;
import com.firstlook.appraisal.trademanager.search.TradeManagerSearchAppraisalsSubmitAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.form.TradeAnalyzerForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.SessionHelper;
import com.firstlook.persistence.dealer.IDealerDAO;
import com.firstlook.persistence.dealergroup.IDealerGroupDAO;

public class SearchStockNumberOrVinAction extends SecureBaseAction
{

private final static String ESTOCKCARD_FORWARD = "vehicleDetailPage";
private final static String VPA_FORWARD = "pingII_vpa";

private IDealerDAO dealerDAO;
private DealerPreferenceDAO dealerPrefDAO;
private IAppraisalService appraisalService;
private IDealerGroupDAO dealerGroupDAO;
private IInventoryDAO inventoryDAO;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	String pendingAppraisalId = request.getParameter("pendingAppraisalId");
	SearchStockNumberOrVinForm searchStockNumberOrVinForm;
	if (pendingAppraisalId != null) {
		searchStockNumberOrVinForm = new SearchStockNumberOrVinForm();
		searchStockNumberOrVinForm.setStockNumberOrVin(request.getParameter("vin"));
	} else {
		searchStockNumberOrVinForm = (SearchStockNumberOrVinForm)form;
	}

	Integer dealerId = new Integer( getFirstlookSessionFromRequest( request ).getCurrentDealerId() );

	putDealerFormInRequest( request );
	Dealer dealer = getImtDealerService().retrieveDealer( dealerId.intValue() );
	DealerPreference dealerPreference = dealer.getDealerPreference();

	// Handle Override forward
	String forwardName = forwardOverride( request, searchStockNumberOrVinForm.getStockNumberOrVin() );
	if ( forwardName != null)
	{
		return mapping.findForward( forwardName);
	}


	String stockNumberOrVinOrCustomerName = "";
	if ( searchStockNumberOrVinForm.getStockNumberOrVin() != null &&  searchStockNumberOrVinForm.getStockNumberOrVin().trim().length() > 0)
	{
		stockNumberOrVinOrCustomerName = searchStockNumberOrVinForm.getStockNumberOrVin().trim();
	} else if (request.getParameter("vin") != null && request.getParameter("vin").trim().length() > 0){
		stockNumberOrVinOrCustomerName = request.getParameter("vin").trim();
	}

	if (stockNumberOrVinOrCustomerName != null && stockNumberOrVinOrCustomerName.trim().length() >0) {
		if (VinUtility.isValidFirstLookVin( stockNumberOrVinOrCustomerName )){
			forwardName = searchByVin( request, dealerId, dealerPreference, stockNumberOrVinOrCustomerName );
		} else {
			forwardName = searchByStockNumber( request, dealerId, dealerPreference, stockNumberOrVinOrCustomerName );
			if (forwardName == null) {
				
				// try searching by Customer name
				List<IAppraisal> appraisals = appraisalService.findByCustomerName( dealerId, stockNumberOrVinOrCustomerName, dealerPreference.getSearchAppraisalDaysBackThreshold());
				if ( appraisals != null && !appraisals.isEmpty())
				{
					forwardName = "searchTrades";
					request.setAttribute( "customerName", stockNumberOrVinOrCustomerName);
					// save this so the searchTrades action doesn't have to refind
					SessionHelper.setAttribute(request, TradeManagerSearchAppraisalsSubmitAction.AppraisalsInSessionKey, appraisals);
					SessionHelper.keepAttribute(request, "appraisalsByCustomer");
				}
			}
		}	
    }
	request.setAttribute( "fromIMP", request.getParameter( "fromIMP" ) == null ? false : true );
	if (forwardName == null) {
		forwardName = "stockNumberNotFoundOrBadVin";
	}

	ActionForward forward = mapping.findForward( checkForEstockSource( request, forwardName, stockNumberOrVinOrCustomerName ) ); 

	return forward; 
}

private String checkForEstockSource( HttpServletRequest request, String finalForward, String inVinOrStock )
{
	// since estock card is making this requst through ajax, we just want to know if
	// the vin/stock number was found our not -DW/DM 4/20/06
	if ( RequestHelper.getBoolean( request, "ajax" ) )
	{
		if ( finalForward.equals( "stockNumberNotFoundOrBadVin" ) )
		{
			request.setAttribute( "result", "notFound" );
			finalForward = "notFoundByAjax";
		}
		else
		{
			Boolean closePopUp = new Boolean( ( finalForward.equals( "vehicleDetailPage" ) ) ? false : true );

			request.setAttribute( "closePopUp", closePopUp );
			request.setAttribute( "result", "found" );
			finalForward = "successByAjax";
		}
	}
	return finalForward;
}

private String searchByStockNumber( HttpServletRequest request, Integer dealerId,
		DealerPreference dealerPreference, String stockNumberOrVin )
{
	String forwardName = null;
	// if in active inv, go to eStock
	Inventory inventory = inventoryDAO.searchStockNumberInActiveInventory( dealerId, stockNumberOrVin );
	if ( inventory != null )
	{
		if (getFirstlookSessionFromRequest(request).isActiveInventoryToolIsEstock()) {
			forwardName = estockCardForward( request, inventory.getInventoryId(), true );
		} else {			
			request.setAttribute("inventoryId", inventory.getInventoryId());
			forwardName = VPA_FORWARD;
		}
	}
	else
	{
		// if in Inactive inventory, go to eStock
		inventory = inventoryDAO.searchStockNumberInInactiveInventory(
				dealerId,
				stockNumberOrVin,
				dealerPreference.getSearchInactiveInventoryDaysBackThreshold() );
		if ( inventory != null )
		{
			forwardName = estockCardForward( request, inventory.getInventoryId(), false );
		}
		else if ( getFirstlookSessionFromRequest( request ).isIncludeWindowSticker() )
		{
			// search appraisal window sticker
			IAppraisal appraisal = appraisalService.findByWindowStickerStockNumber( dealerId, stockNumberOrVin, dealerPreference.getSearchAppraisalDaysBackThreshold());
			if ( appraisal != null )
			{
				forwardName = "tradeManager";
				request.setAttribute( "pageName", "bullpen" );
				request.setAttribute( "appraisalId", appraisal.getAppraisalId() );
			}
		}
	}
	return forwardName;
}

private String searchByVin( HttpServletRequest request, Integer dealerId, DealerPreference dealerPreference, String stockNumberOrVin )
{
	String forwardName;
	// if in active inv, go to eStock
	Inventory inventory = inventoryDAO.searchVinInActiveInventory( dealerId, stockNumberOrVin );
	if ( inventory != null )
	{
		if (getFirstlookSessionFromRequest(request).isActiveInventoryToolIsEstock()) {
			forwardName = estockCardForward( request, inventory.getInventoryId(), true );
		} else {			
			request.setAttribute("inventoryId", inventory.getInventoryId());
			forwardName = VPA_FORWARD;
		}
	}
	else
	{

		// if in Inactive inventory, go to eStock
		inventory = inventoryDAO.searchVinInInactiveInventory(
																				dealerId,
																				stockNumberOrVin,
																				new Integer( dealerPreference.getSearchInactiveInventoryDaysBackThreshold() ) );
		if ( inventory != null )
		{
			forwardName = estockCardForward( request, inventory.getInventoryId(), false );
		}
		else
		{
			// see if an appraisal for this VIN/dealer exists in the system that has a last modified date < 'SearchAppraisalDaysBackThreshold' days ago
			List<IAppraisal> appraisal = appraisalService.findBy(dealerId, stockNumberOrVin, dealerPreference.getSearchAppraisalDaysBackThreshold() );
			if ( appraisal != null && !appraisal.isEmpty())
			{
				if (!appraisal.get(0).getAppraisalSource().isCRMOrMobile()) { 
					forwardName = forwardToTradeManager( request, appraisal.get(0).getAppraisalId(), stockNumberOrVin );
				} else {
					forwardName = forwardToTradeAnalyzer( request, stockNumberOrVin );
				}
			}
			else
			{
				forwardName = forwardToTradeAnalyzer( request, stockNumberOrVin );
			}
		}
	}

	return forwardName;
}

private String estockCardForward( HttpServletRequest request, Integer inventoryId, boolean editable )
{
	request.setAttribute( "inventoryId", inventoryId );
	request.setAttribute( "editable", editable );

	return ESTOCKCARD_FORWARD;
}

private String forwardOverride( HttpServletRequest request, String stockNumberOrVin ) throws ApplicationException
{
	// parameter overrides
	String override = request.getParameter( "override" );
	if ( override != null )
	{
		if ( override.equalsIgnoreCase( "tradeManager" ) )
		{
			Integer appraisalId = new Integer( request.getParameter( "appraisalId" ) );
			if ( appraisalId == null )
			{
				throw new ApplicationException( "No appraisalId when trying to direct to Trade Manager." );
			}

			return forwardToTradeManager( request, appraisalId, stockNumberOrVin );
		}
		else if ( override.equalsIgnoreCase( "tradeAnalyzer" ) )
		{
			String vin = request.getParameter( "vin" );
			if ( vin == null )
			{
				throw new ApplicationException( "No Vin passed into Trade Analyzer" );
			}

			return forwardToTradeAnalyzer( request, vin );
		}
	}
	return null;
}

private String forwardToTradeManager( HttpServletRequest request, Integer appraisalId, String vin )
{
	String forwardName;
	forwardName = "tradeManager";
	request.setAttribute( "pageName", "bullpen" );
	request.setAttribute( "appraisalId", appraisalId );
	TradeAnalyzerForm tradeAnalyzerForm = new TradeAnalyzerForm();
	tradeAnalyzerForm.setVin( vin );
	tradeAnalyzerForm.setAppraisalId( appraisalId );
	request.setAttribute( "tradeAnalyzerForm", tradeAnalyzerForm );
	SessionHelper.setAttribute( request, "tradeAnalyzerForm", tradeAnalyzerForm );
	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
	return forwardName;
}

private String forwardToTradeAnalyzer( HttpServletRequest request, String stockNumberOrVin )
{
	TradeAnalyzerForm tradeAnalyzerForm = new TradeAnalyzerForm();
	tradeAnalyzerForm.setVin( stockNumberOrVin );
	request.setAttribute( "tradeAnalyzerForm", tradeAnalyzerForm );
	SessionHelper.setAttribute( request, "tradeAnalyzerForm", tradeAnalyzerForm );
	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
	return "tradeAnalyzer";
}

public DealerPreferenceDAO getDealerPrefDAO()
{
	return dealerPrefDAO;
}

public void setDealerPrefDAO( DealerPreferenceDAO dealerPrefDAO )
{
	this.dealerPrefDAO = dealerPrefDAO;
}

public IDealerDAO getDealerDAO()
{
	return dealerDAO;
}

public void setDealerDAO( IDealerDAO dealerDAO )
{
	this.dealerDAO = dealerDAO;
}

public IDealerGroupDAO getDealerGroupDAO()
{
	return dealerGroupDAO;
}

public void setDealerGroupDAO( IDealerGroupDAO dealerGroupDAO )
{
	this.dealerGroupDAO = dealerGroupDAO;
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

public void setInventoryDAO( IInventoryDAO inventoryDAO )
{
	this.inventoryDAO = inventoryDAO;
}

}
