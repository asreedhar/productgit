package com.firstlook.action.dealer;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.enumeration.LightAndCIATypeDescriptor;
import biz.firstlook.commons.util.PropertyLoader;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.InventoryRiskHelperForm;
import com.firstlook.helper.ManagementCenterInventoryReportDAO;
import com.firstlook.iterator.BaseFormIterator;
import com.firstlook.scorecard.ReportData;
import com.firstlook.scorecard.ScoreCardReport;
import com.firstlook.scorecard.ScoreCardReportForm;
import com.firstlook.scorecard.persistence.PurchasedScoreCardReportHelper;
import com.firstlook.scorecard.persistence.RetailScoreCardReportHelper;
import com.firstlook.scorecard.persistence.ScoreCardReportHelper;
import com.firstlook.scorecard.persistence.TradeInScoreCardReportHelper;
import com.firstlook.session.FirstlookSession;

public class ManagementCenterDisplayAction extends SecureBaseAction
{
private TradeInScoreCardReportHelper tradeInScoreCardReportHelper;
private PurchasedScoreCardReportHelper purchasedScoreCardReportHelper;
private RetailScoreCardReportHelper retailScoreCardReportHelper;
private ManagementCenterInventoryReportDAO managementCenterInventoryReportDAO;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	String cacheKey = "ManagementCenter"
			+ ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();

	int timeout = Integer.parseInt( PropertyLoader.getProperty( "firstlook.cache.timeout.ManagementCenter", "1" ) );
	int inventoryType = getMemberFromRequest( request ).getInventoryType().getValue();
	int currentDealerId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();
	ManagementCenterForm managementForm = retrieveManagementCenterForm( cacheKey, timeout, inventoryType, currentDealerId );

	request.setAttribute( "inventoryRiskHelperForm", managementForm.getInventoryRiskForm() );
	request.setAttribute( "retailPerformanceForm", managementForm.getRetailScoreCardForm() );
	request.setAttribute( "tradeInPerformanceForm", managementForm.getTradeInScoreCardForm() );
	request.setAttribute( "purchasedVehiclePerformanceForm", managementForm.getPurchaseScoreCardForm() );
	request.setAttribute( "managementCenterEnabled", new Boolean( true ) );

	return mapping.findForward( "success" );
}

private ManagementCenterForm retrieveManagementCenterForm( String cacheKey, int timeout, int inventoryType, int currentDealerId ) throws ApplicationException
{
	ManagementCenterForm managementForm = null;
	Dealer currentDealer = getImtDealerService().retrieveDealer( currentDealerId );

	InventoryRiskHelperForm inventoryForm = retrieveInventoryRiskHelperForm( currentDealer );
	ScoreCardReportForm retailForm = retrieveRetailPerformanceForm( currentDealer, inventoryType );
	ScoreCardReportForm tradeInForm = retrieveTradeInPerformanceForm( currentDealer, inventoryType );
	ScoreCardReportForm purchaseForm = retrievePurchasedVehiclePerformanceForm( currentDealer, inventoryType );
	managementForm = new ManagementCenterForm( inventoryForm, retailForm, purchaseForm, tradeInForm );

	return managementForm;
}

void putPerformanceInRequest( HttpServletRequest request, Dealer dealer, int inventoryType, int weeks[], ScoreCardReportHelper scoreCardHelper,
								String attributeName ) throws ApplicationException
{
	Collection<ScoreCardReport> scoreCardReports = scoreCardHelper.createScoreCardReports( dealer, weeks, inventoryType );

	request.setAttribute( attributeName, new BaseFormIterator<ScoreCardReport>( scoreCardReports ) );
}

ScoreCardReportForm retrieveRetailPerformanceForm( Dealer dealer, int inventoryType ) throws ApplicationException
{
	int[] weeks = { Integer.parseInt( PropertyLoader.getProperty( "firstlook.report.secondweeks", "8" ) ),
			Integer.parseInt( PropertyLoader.getProperty( "firstlook.report.fourthweeks", "26" ) ) };

    Collection<ScoreCardReport> scoreCardReports = retailScoreCardReportHelper.createScoreCardReports( dealer, weeks, inventoryType );
	Iterator<ScoreCardReport> scoreCardReportIt = scoreCardReports.iterator();

	ScoreCardReport[] scoreCardReport = new ScoreCardReport[2];

	for ( int i = 0; i < 2; i++ )
	{
		if ( scoreCardReportIt.hasNext() )
		{
			scoreCardReport[i] = (ScoreCardReport)scoreCardReportIt.next();
		}
	}

	return new ScoreCardReportForm( scoreCardReport[0], scoreCardReport[1] );
}

ScoreCardReportForm retrievePurchasedVehiclePerformanceForm( Dealer dealer, int inventoryType ) throws ApplicationException
{
	int[] weeks = { Integer.parseInt( PropertyLoader.getProperty( "firstlook.report.secondweeks", "8" ) ),
			Integer.parseInt( PropertyLoader.getProperty( "firstlook.report.fourthweeks", "26" ) ) };


    Collection<ScoreCardReport> scoreCardReports = purchasedScoreCardReportHelper.createScoreCardReports( dealer, weeks, inventoryType );
	Iterator<ScoreCardReport> scoreCardReportIt = scoreCardReports.iterator();

	ScoreCardReport[] scoreCardReport = new ScoreCardReport[2];

	for ( int i = 0; i < 2; i++ )
	{
		if ( scoreCardReportIt.hasNext() )
		{
			scoreCardReport[i] = (ScoreCardReport)scoreCardReportIt.next();
		}
	}

	return new ScoreCardReportForm( scoreCardReport[0], scoreCardReport[1] );
}

ScoreCardReportForm retrieveTradeInPerformanceForm( Dealer dealer, int inventoryType ) throws ApplicationException
{
	int secondWeeks = Integer.parseInt( PropertyLoader.getProperty( "firstlook.report.secondweeks", "8" ) );
	int thirdWeeks = Integer.parseInt( PropertyLoader.getProperty( "firstlook.report.fourthweeks", "26" ) );
	int[] weeks = { secondWeeks, thirdWeeks };


    Collection<ScoreCardReport> scoreCardReports = tradeInScoreCardReportHelper.createScoreCardReports( dealer, weeks, inventoryType );
	Iterator<ScoreCardReport> scoreCardReportIt = scoreCardReports.iterator();

	ScoreCardReport[] scoreCardReport = new ScoreCardReport[2];

	for ( int i = 0; i < 2; i++ )
	{
		if ( scoreCardReportIt.hasNext() )
		{
			scoreCardReport[i] = (ScoreCardReport)scoreCardReportIt.next();
		}
	}

	return new ScoreCardReportForm( scoreCardReport[0], scoreCardReport[1] );
}

private InventoryRiskHelperForm retrieveInventoryRiskHelperForm( Dealer dealer )
{
	Map<Integer, ReportData> risks = managementCenterInventoryReportDAO.getVehicleLightPercentages( dealer );

	InventoryRiskHelperForm form = new InventoryRiskHelperForm();
	form.setGreenLight( risks.get(Integer.valueOf(LightAndCIATypeDescriptor.GREEN_LIGHT)));
	form.setYellowLight( risks.get(Integer.valueOf(LightAndCIATypeDescriptor.YELLOW_LIGHT)));
	form.setRedLight( risks.get(Integer.valueOf(LightAndCIATypeDescriptor.RED_LIGHT)));
	form.setAgeBands( managementCenterInventoryReportDAO.getAgeBucketPercentages( dealer ));

	return form;
}

public PurchasedScoreCardReportHelper getPurchasedScoreCardReportHelper()
{
    return purchasedScoreCardReportHelper;
}

public void setPurchasedScoreCardReportHelper( PurchasedScoreCardReportHelper purchasedScoreCardReportHelper )
{
    this.purchasedScoreCardReportHelper = purchasedScoreCardReportHelper;
}

public TradeInScoreCardReportHelper getTradeInScoreCardReportHelper()
{
    return tradeInScoreCardReportHelper;
}

public void setTradeInScoreCardReportHelper( TradeInScoreCardReportHelper tradeInScoreCardReportHelper )
{
    this.tradeInScoreCardReportHelper = tradeInScoreCardReportHelper;
}

public RetailScoreCardReportHelper getRetailScoreCardReportHelper()
{
	return retailScoreCardReportHelper;
}

public void setRetailScoreCardReportHelper( RetailScoreCardReportHelper retailScoreCardReportHelper )
{
	this.retailScoreCardReportHelper = retailScoreCardReportHelper;
}

public void setManagementCenterInventoryReportDAO(ManagementCenterInventoryReportDAO managementCenterInventoryReportDAO) {
	this.managementCenterInventoryReportDAO = managementCenterInventoryReportDAO;
}


}
