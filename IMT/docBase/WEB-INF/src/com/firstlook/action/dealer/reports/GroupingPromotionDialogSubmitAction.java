package com.firstlook.action.dealer.reports;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.GroupingPromotionPlan;
import com.firstlook.entity.form.GroupingPromotionDialogForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.report.GroupingPromotionPlanDAO;

public class GroupingPromotionDialogSubmitAction extends SecureBaseAction
{

private GroupingPromotionPlanDAO groupingPromotionPlanDAO;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	GroupingPromotionDialogForm groupingPromotionDialogForm = (GroupingPromotionDialogForm)form;
	GroupingPromotionPlan groupingPromotionPlan = populateGroupingPromotionPlan(
																					getFirstlookSessionFromRequest( request ).getCurrentDealerId(),
																					groupingPromotionDialogForm );

	groupingPromotionPlanDAO.saveOrUpdate( groupingPromotionPlan );

	return mapping.findForward( "success" );
}

private GroupingPromotionPlan populateGroupingPromotionPlan( int businessunitId, GroupingPromotionDialogForm groupingPromotionDialogForm )
		throws ApplicationException
{
	GroupingPromotionPlan groupingPromotionPlan = new GroupingPromotionPlan();
	groupingPromotionPlan.setGroupingPromotionPlanId( groupingPromotionDialogForm.getGroupingPromotionPlanId() );
	groupingPromotionPlan.setBusinessUnitId( businessunitId );

	groupingPromotionPlan.setGroupingDescriptionId( groupingPromotionDialogForm.getGroupingDescriptionId() );
	groupingPromotionPlan.setNotes( groupingPromotionDialogForm.getNotes() );

	try
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat( "MM/dd/yyyy" );
		groupingPromotionPlan.setPromotionStartDate( dateFormat.parse( groupingPromotionDialogForm.getStartDate() ) );
		groupingPromotionPlan.setPromotionEndDate( dateFormat.parse( groupingPromotionDialogForm.getEndDate() ) );
	}
	catch ( ParseException pe )
	{
		throw new ApplicationException( "Front End validation for date has failed." );
	}

	return groupingPromotionPlan;
}

public void setGroupingPromotionPlanDAO( GroupingPromotionPlanDAO groupingPromotionPlanDAO )
{
	this.groupingPromotionPlanDAO = groupingPromotionPlanDAO;
}
}
