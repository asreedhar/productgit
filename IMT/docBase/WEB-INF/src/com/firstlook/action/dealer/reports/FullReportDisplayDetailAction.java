package com.firstlook.action.dealer.reports;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.VehicleSaleEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.action.ReportActionHelper;
import com.firstlook.report.Report;
import com.firstlook.report.ReportForm;
import com.firstlook.service.dealer.IUCBPPreferenceService;
import com.firstlook.session.FirstlookSession;
import com.firstlook.util.PageBreakHelper;

public class FullReportDisplayDetailAction extends SecureBaseAction
{

private ReportActionHelper reportActionHelper;
private IUCBPPreferenceService ucbpPreferenceService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    int inventoryType = getMemberFromRequest( request ).getInventoryType().getValue();
    int dealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
    Dealer currentDealer = getImtDealerService().retrieveDealer( dealerId );

    Report report = reportActionHelper.createSalesReport( currentDealer, ReportActionHelper.determineWeeksAndSetOnRequest( request ),
                                              ReportActionHelper.determineForecastAndSetOnRequest( request ), VehicleSaleEntity.MILEAGE_MAXVAL,
                                              inventoryType );

    request.setAttribute( "report", new ReportForm( report ) );
    putDealerFormInRequest( request, 0 );

    putPageBreakHelperInRequest( PageBreakHelper.FULL_REPORTS_ONLINE, request, getMemberFromRequest( request ).getProgramType() );

    String forwardName = getForwardName( request );

    Integer mileage = new Integer( ucbpPreferenceService.retrieveDealerRisk(
                                                               ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId() ).getHighMileageThreshold() );
    request.setAttribute( "mileage", mileage );

    return mapping.findForward( forwardName );
}

String getForwardName( HttpServletRequest request )
{
    String forwardName = "error";
    String reportType = (String)request.getParameter( "ReportType" );

    if ( reportType != null )
    {
        if ( reportType.startsWith( "TOPSELLER" ) )
        {
            forwardName = "topSellerDetail";
        }
        if ( reportType.startsWith( "MOSTPROFITABLE" ) )
        {
            forwardName = "mostProfitableDetail";
        }
        if ( reportType.startsWith( "FASTESTSELLER" ) )
        {
            forwardName = "fastestSellerDetail";
        }
    }
    return forwardName;
}

public void setReportActionHelper( ReportActionHelper reportActionHelper )
{
	this.reportActionHelper = reportActionHelper;
}

public void setUcbpPreferenceService( IUCBPPreferenceService ucbpPreferenceService )
{
	this.ucbpPreferenceService = ucbpPreferenceService;
}

}
