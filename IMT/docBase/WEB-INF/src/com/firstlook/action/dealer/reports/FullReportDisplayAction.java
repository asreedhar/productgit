package com.firstlook.action.dealer.reports;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.dealer.helper.DealerFactsHelper;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.FullReportDataHelper;
import com.firstlook.helper.action.ReportActionHelper;

public class FullReportDisplayAction extends SecureBaseAction
{

private ReportActionHelper reportActionHelper;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    FullReportDataHelper fullReportDataHelper = new FullReportDataHelper();

    int inventoryType = getMemberFromRequest( request ).getInventoryType().getValue();
    int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();

    putAveragesAndDateInRequest( request, fullReportDataHelper, currentDealerId, inventoryType );

    String forwardName = ReportActionHelper.getForwardName( request );
    return mapping.findForward( forwardName );
}

void putAveragesAndDateInRequest( HttpServletRequest request, FullReportDataHelper fullReportDataHelper, int dealerId, int inventoryType )
        throws ApplicationException, DatabaseException
{
    fullReportDataHelper.setDealerId( dealerId );
    fullReportDataHelper.setWeeks( ReportActionHelper.determineWeeksAndSetOnRequest( request ) );
    fullReportDataHelper.setForecast( ReportActionHelper.determineForecastAndSetOnRequest( request ) );
    putDealerFormInRequest( request, fullReportDataHelper.getWeeks() );
    reportActionHelper.putAveragesInRequest( fullReportDataHelper.getDealerId(), fullReportDataHelper.getWeeks(), fullReportDataHelper.getForecast(),
                                 request, inventoryType );

    DealerFactsHelper dealerFactsHelper = new DealerFactsHelper( getDealerFactsDAO() );
    dealerFactsHelper.putSaleMaxPolledDateInRequest( fullReportDataHelper.getDealerId(), request );
}

public void setReportActionHelper( ReportActionHelper reportActionHelper )
{
	this.reportActionHelper = reportActionHelper;
}
}
