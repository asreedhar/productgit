package com.firstlook.action.dealer.cia;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.cia.model.CIASummary;
import biz.firstlook.cia.model.Status;
import biz.firstlook.commons.util.DateUtilFL;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.service.cia.CIAService;
import com.firstlook.session.FirstlookSession;

public class CIABuyingGuideDisplayAction extends SecureBaseAction
{

private CIAService ciaService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int businessUnitId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();
	int priorWeek = RequestHelper.getInt( request, "priorWeek" );
	Status currentOrPrior = Status.CURRENT;
	if ( priorWeek == 1 )
	{
		currentOrPrior = Status.PRIOR;
	}

	List buyingGuideItems = getCiaService().getCIABuyingPlanData( Integer.valueOf( businessUnitId ), currentOrPrior.getStatusId() );

	request.setAttribute( "ciaBuyingGuideOrderedShoppingList", buyingGuideItems );
	Dealer currentDealer = getImtDealerService().retrieveDealer( getFirstlookSessionFromRequest( request ).getCurrentDealerId() );
	request.setAttribute( "nickname", currentDealer.getNickname() );
	request.setAttribute( "priorWeek", new Integer( priorWeek ) );
	request.setAttribute( "startAndEndDates", getStartAndEndDates( new Integer( businessUnitId ), currentOrPrior ) );

    if (request.getParameter("from") != null && request.getParameter("from").equals("FL")) {
        request.setAttribute("backToFL", true);
    }
    
	return mapping.findForward( "success" );
}

private String getStartAndEndDates( Integer businessUnitId, Status currentOrPrior )
{
	String startAndEndDates = "";
	CIASummary ciaSummary = getCiaService().retrieveCIASummary( businessUnitId, currentOrPrior.getStatusId() );
	
	// Handle case where no summary for the week is available.
	if(ciaSummary == null)
	{		
		return "Not Available";
	}
	
	if ( ciaSummary.getDateCreated() != null )
	{
		Date startDate = ciaSummary.getDateCreated();

		Date endDate = DateUtilFL.addDaysToDate( startDate, 7 );

		SimpleDateFormat dateFormat = new SimpleDateFormat( "MM/dd/yyyy" );
		startAndEndDates = "(" + dateFormat.format( startDate ) + " - " + dateFormat.format( endDate ) + ")";
	}
	return startAndEndDates;
}

public CIAService getCiaService()
{
	return ciaService;
}

public void setCiaService( CIAService ciaService )
{
	this.ciaService = ciaService;
}
}
