package com.firstlook.action.dealer.tools;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.form.TradeAnalyzerForm;
import com.firstlook.exception.ApplicationException;

public class VehicleLocatorDisplayAction extends SecureBaseAction
{
public ActionForward doIt( ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response ) throws DatabaseException,
        ApplicationException
{
    TradeAnalyzerForm searchForm = (TradeAnalyzerForm) form;

    request.setAttribute("vehicleSearchForm", searchForm);

    return mapping.findForward("success");
}
}
