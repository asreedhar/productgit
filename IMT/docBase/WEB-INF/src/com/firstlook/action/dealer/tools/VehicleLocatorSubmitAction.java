package com.firstlook.action.dealer.tools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.comparator.InventoryComparator;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.form.VehicleSearchForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.iterator.SimpleFormIterator;
import com.firstlook.iterator.VehicleFormIterator;
import com.firstlook.service.inventory.InventoryService_Legacy;
import com.firstlook.session.FirstlookSession;
import com.firstlook.util.PageBreakHelper;

public class VehicleLocatorSubmitAction extends SecureBaseAction
{

private InventoryService_Legacy inventoryService_Legacy;	

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	VehicleSearchForm searchForm = (VehicleSearchForm)form;
	ActionErrors errors = searchForm.validateMakeErrors();
	putActionErrorsInRequest( request, errors );
	putDealerFormInRequest( request, 0 );
	putPageBreakHelperInRequest( PageBreakHelper.LOCATOR_ONLINE, request, getMemberFromRequest( request ).getProgramType() );
	if ( errors.size() > 0 )
	{
		return mapping.findForward( "success" );
	}
	int currentDealerId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();
 
	DealerGroup group = getDealerGroupService().retrieveByDealerId( currentDealerId );
	int dealerGroupId = group.getDealerGroupId().intValue();
	
	Dealer dealer = getImtDealerService().retrieveDealer( currentDealerId );
	Collection<InventoryEntity> inventoryCol = inventoryService_Legacy.retrieveByVehicleInDealerGroupInventoryConstraints( dealer, dealerGroupId, searchForm.getYear(),
																									searchForm.getMake(),
																									searchForm.getModel(),
																									searchForm.getTrimFiltered(), getImtDealerService() );
	
	Map<Integer,Integer> dealerToUnitCostDelay = new HashMap<Integer,Integer>();
	final boolean dispMyUnitCost = dealer.getDealerPreference().getDisplayUnitCostToDealerGroup();
	dealerToUnitCostDelay.put(dealer.getDealerId(), dispMyUnitCost ? dealer.getDealerPreference().getFlashLocatorHideUnitCostDays() : 0);
	for (InventoryEntity it : inventoryCol) {
		final int otherDealerId = it.getDealerId();
		if (!dealerToUnitCostDelay.containsKey(otherDealerId)) {
			final Dealer otherDealer = getImtDealerService().retrieveDealer(otherDealerId);
			final boolean displayUnitCost = otherDealer.getDealerPreference().getDisplayUnitCostToDealerGroup();
				dealerToUnitCostDelay.put(otherDealerId, displayUnitCost ? otherDealer.getDealerPreference().getFlashLocatorHideUnitCostDays() : 0);
		}
	}
	request.setAttribute("unitCostDelayMap", dealerToUnitCostDelay);
	
	InventoryComparator comparator = new InventoryComparator();
	comparator.setSortYearsDescending( true );

	List<InventoryEntity> inventoryAsList = new ArrayList<InventoryEntity>( inventoryCol );
	Collections.sort( inventoryAsList, comparator );

	SimpleFormIterator vehicleForm = new VehicleFormIterator( inventoryAsList );

	request.setAttribute( "vehicles", vehicleForm );
	request.setAttribute( "vehicleSearchForm", searchForm );

	return mapping.findForward( "success" );
}

public void setInventoryService_Legacy(InventoryService_Legacy inventoryService) {
	this.inventoryService_Legacy = inventoryService;
}

}