package com.firstlook.action.dealer.cia;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.exception.RuntimeApplicationException;
import com.firstlook.service.cia.ICIAService;
import com.firstlook.service.cia.report.CIAStockingReportAssembler;

public class CIAStockingGuideDisplayAction extends
        SecureBaseAction
{

private ICIAService ciaService;

public ActionForward doIt( ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response ) throws DatabaseException,
        ApplicationException
{
    int businessUnitId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
    int stockingType = parseStockingType(request.getParameter("stockingType"));
    
    List ciaReportDataItemsList = getCiaService().getCIAStockingData(new Integer(businessUnitId), stockingType);
    
	Dealer currentDealer = getImtDealerService().retrieveDealer( getFirstlookSessionFromRequest( request ).getCurrentDealerId() );
	
	request.setAttribute( "lowerUnitCostThreshold", currentDealer.getDealerPreference().getUnitCostThresholdLower() );
    request.setAttribute("ciaReportDataItemsList", ciaReportDataItemsList);
    request.setAttribute("nickname", currentDealer.getNickname());
    request.setAttribute("stockingType", new Integer(stockingType));

    return mapping.findForward("success");
}

public ICIAService getCiaService()
{
	return ciaService;
}

public void setCiaService( ICIAService ciaService )
{
	this.ciaService = ciaService;
}

private int parseStockingType(String stockingType)
{
	int stockingValue;
	if ( stockingType == null )
	{
		stockingValue = CIAStockingReportAssembler.OVER_UNDER;
	}
	else
	{
		try
		{
			stockingValue = Integer.parseInt( stockingType );
		}
		catch ( NumberFormatException nfe )
		{
			throw new RuntimeApplicationException( "Could not convertstocking type: " + stockingType + " to int", nfe );
		}
	}
	return stockingValue;
}

}
