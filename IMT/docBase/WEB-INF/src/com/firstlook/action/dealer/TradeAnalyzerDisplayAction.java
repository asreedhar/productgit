package com.firstlook.action.dealer;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.persistence.BusinessUnitCredentialDAO;
import biz.firstlook.transact.persist.service.appraisal.AppraisalActionType;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.dealer.tools.GuideBook;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.TradeAnalyzerForm;
import com.firstlook.exception.ApplicationException;

public class TradeAnalyzerDisplayAction extends SecureBaseAction
{

private IAppraisalService appraisalService;
private BusinessUnitCredentialDAO businessUnitCredentialDAO;
//
public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	Member member = getMemberFromRequest( request );
	Dealer currentDealer = getImtDealerService().retrieveDealer( getFirstlookSessionFromRequest( request ).getCurrentDealerId() );
	
	TradeAnalyzerForm tradeAnalyzerForm = (TradeAnalyzerForm)form;
	
	int dealerGuideBookPreference = currentDealer.getDealerPreference().getGuideBookIdAsInt();
	int secondaryDealerGuideBookPreference = currentDealer.getDealerPreference().getGuideBook2IdAsInt();
	Boolean isBlackBook = new Boolean( dealerGuideBookPreference == GuideBook.GUIDE_TYPE_BLACKBOOK
			|| secondaryDealerGuideBookPreference == GuideBook.GUIDE_TYPE_BLACKBOOK );
	Boolean isKelley = new Boolean( dealerGuideBookPreference == GuideBook.GUIDE_TYPE_KELLEYBLUEBOOK
			|| secondaryDealerGuideBookPreference == GuideBook.GUIDE_TYPE_KELLEYBLUEBOOK );
	Boolean isNADA = new Boolean( dealerGuideBookPreference == GuideBook.GUIDE_TYPE_NADA
			|| secondaryDealerGuideBookPreference == GuideBook.GUIDE_TYPE_NADA );
	
	boolean hasCRM = businessUnitCredentialDAO.hasCRMCredentials( currentDealer.getDealerId() );
	request.setAttribute( "hasCRM", hasCRM );
	/*if( hasCRM )
	{*/
		Map<AppraisalActionType, Integer> countsOfTabs = appraisalService.countTradeInAppraisalsAndGroupByAppraisalActionType( currentDealer.getDealerId(), currentDealer.getDealerPreference().getTradeManagerDaysFilter(),  currentDealer.getDealerPreference().getShowInactiveAppraisals() );
		request.setAttribute( "waitingAppraisalsCount", countsOfTabs.get( AppraisalActionType.WAITING_REVIEW ) );
	//}
	request.setAttribute( "dealerGuideBookPreference", dealerGuideBookPreference );
	request.setAttribute( "isBlackBook", isBlackBook );
	request.setAttribute( "isKelley", isKelley );
	request.setAttribute( "isNADA", isNADA );
	
	

	if ( member.getMemberRoleTester().isUsedNoAccess() )
	{
		request.setAttribute( "tradeAnalyzerEnabled", new Boolean( false ) );
	}
	else
	{
		request.setAttribute( "tradeAnalyzerEnabled", new Boolean( true ) );
	}
	
	ActionForward forward = mapping.findForward( "success" ); 

	return forward; 
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

public void setBusinessUnitCredentialDAO( BusinessUnitCredentialDAO businessUnitCredentialDAO )
{
	this.businessUnitCredentialDAO = businessUnitCredentialDAO;
}

}
