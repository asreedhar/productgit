package com.firstlook.action.dealer.vehicledetailpage;

import org.apache.struts.action.ActionForm;

public class PrintableVehicleDetailForm extends ActionForm
{

private static final long serialVersionUID = 3346892034849566921L;
private boolean viewDealsReport;
private boolean blackBookReport;
private boolean galvesReport;
private boolean nadaValues;
private boolean nadaTradeValues;
private boolean nadaAppraisal;
private boolean nadaVehicleDescription;
private boolean nadaLoanSummary;
private boolean nadaWholesale;
private boolean kelleyRetailBreakdown;
private boolean kelleyEquityBreakdown;
private boolean kelleyWholesaleBreakdown;
private boolean kelleyWholesaleRetail;
private boolean kelleyTradeInReport;
private boolean firstlookBookOutReport;
private boolean windowStickerReport;
private boolean auctionReport;
private boolean firstlookPrintPage;

public PrintableVehicleDetailForm()
{
	super();
}

public boolean isFirstlookPrintPage()
{
	return firstlookPrintPage;
}

public void setFirstlookPrintPage( boolean firstlookPrintPage )
{
	this.firstlookPrintPage = firstlookPrintPage;
}

public boolean isAuctionReport()
{
	return auctionReport;
}

public void setAuctionReport( boolean auctionReport )
{
	this.auctionReport = auctionReport;
}

public boolean isBlackBookReport()
{
	return blackBookReport;
}

public void setBlackBookReport( boolean blackBookReport )
{
	this.blackBookReport = blackBookReport;
}

public boolean isFirstlookBookOutReport()
{
	return firstlookBookOutReport;
}

public void setFirstlookBookOutReport( boolean firstlookBookOutReport )
{
	this.firstlookBookOutReport = firstlookBookOutReport;
}


public boolean isViewDealsReport()
{
	return viewDealsReport;
}

public void setViewDealsReport( boolean viewDealsReport )
{
	this.viewDealsReport = viewDealsReport;
}

public boolean isWindowStickerReport()
{
	return windowStickerReport;
}

public void setWindowStickerReport( boolean windowStickerReport )
{
	this.windowStickerReport = windowStickerReport;
}

public boolean isGalvesReport() {
	return galvesReport;
}

public void setGalvesReport(boolean galvesReport) {
	this.galvesReport = galvesReport;
}

public boolean isNadaValues() {
	return nadaValues;
}

public void setNadaValues(boolean nadaValues) {
	this.nadaValues = nadaValues;
}

public boolean isNadaTradeValues() {
	return nadaTradeValues;
}

public void setNadaTradeValues(boolean nadaTradeValues) {
	this.nadaTradeValues = nadaTradeValues;
}

public boolean isNadaAppraisal() {
	return nadaAppraisal;
}

public void setNadaAppraisal(boolean nadaAppraisal) {
	this.nadaAppraisal = nadaAppraisal;
}

public boolean isNadaVehicleDescription() {
	return nadaVehicleDescription;
}

public void setNadaVehicleDescription(boolean nadaVehicleDescription) {
	this.nadaVehicleDescription = nadaVehicleDescription;
}

public boolean isNadaLoanSummary() {
	return nadaLoanSummary;
}

public void setNadaLoanSummary(boolean nadaLoanSummary) {
	this.nadaLoanSummary = nadaLoanSummary;
}

public boolean isNadaWholesale() {
	return nadaWholesale;
}

public void setNadaWholesale(boolean nadaWholesale) {
	this.nadaWholesale = nadaWholesale;
}

public boolean isKelleyRetailBreakdown() {
	return kelleyRetailBreakdown;
}

public void setKelleyRetailBreakdown(boolean kelleyRetailBreakdown) {
	this.kelleyRetailBreakdown = kelleyRetailBreakdown;
}

public boolean isKelleyEquityBreakdown() {
	return kelleyEquityBreakdown;
}

public void setKelleyEquityBreakdown(boolean kelleyEquityBreakdown) {
	this.kelleyEquityBreakdown = kelleyEquityBreakdown;
}

public boolean isKelleyWholesaleBreakdown() {
	return kelleyWholesaleBreakdown;
}

public void setKelleyWholesaleBreakdown(boolean kelleyWholesaleBreakdown) {
	this.kelleyWholesaleBreakdown = kelleyWholesaleBreakdown;
}

public boolean isKelleyWholesaleRetail() {
	return kelleyWholesaleRetail;
}

public void setKelleyWholesaleRetail(boolean kelleyWholesaleRetail) {
	this.kelleyWholesaleRetail = kelleyWholesaleRetail;
}

public boolean isKelleyTradeInReport()
{
	return kelleyTradeInReport;
}

public void setKelleyTradeInReport( boolean kelleyTradeInReport )
{
	this.kelleyTradeInReport = kelleyTradeInReport;
}

}