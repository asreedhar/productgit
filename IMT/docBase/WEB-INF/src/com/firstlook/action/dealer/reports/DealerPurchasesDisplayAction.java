package com.firstlook.action.dealer.reports;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.comparator.BaseComparator;
import com.firstlook.comparator.service.BaseComparatorService;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.iterator.SimpleFormIterator;
import com.firstlook.persistence.report.PurchaseReportRetriever;
import com.firstlook.report.DealerTradePurchaseLineItemForm;

public class DealerPurchasesDisplayAction extends SecureBaseAction
{

private String DEFAULT_ORDER = "receivedDate";
private  PurchaseReportRetriever purchaseReportRetriever;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
    String weekStr = request.getParameter( "weeks" );
	Integer weeks = new Integer( weekStr );
	request.setAttribute( "weeks", weeks );

    String orderBy = request.getParameter( "orderBy" );
    String isAscending = request.getParameter( "isAscending" );

    int dealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
    putDealerFormInRequest(request);

    List purchases = purchaseReportRetriever.retrieveDealerTradePurchaseLineItems( dealerId, weeks );

    BaseComparatorService baseComparatorService = new BaseComparatorService( DEFAULT_ORDER );
    BaseComparator comparator = new BaseComparator( baseComparatorService.retrieveOrderBy( orderBy ) );
    comparator.setSortAscending( ( baseComparatorService.retrieveIsAscending( isAscending ) ).booleanValue() );

    DealerTradePurchaseLineItemForm lineItemForm = null;
    Collection purchasesLineItemForms = new ArrayList();
    Collections.sort( purchases, comparator );
    SimpleFormIterator purchasesIterator = new SimpleFormIterator( purchases, DealerTradePurchaseLineItemForm.class );

    while ( purchasesIterator.hasNext() )
    {
        lineItemForm = (DealerTradePurchaseLineItemForm)purchasesIterator.next();
        purchasesLineItemForms.add( lineItemForm );
    }

    request.setAttribute( "purchasesLineItems", purchasesLineItemForms );
    request.setAttribute( "purchasesLineItemsSize", new Integer( purchasesLineItemForms.size() ) );
    request.setAttribute( "purchasesIterator", purchasesIterator );

    return mapping.findForward( "success" );
}

public void setPurchaseReportRetriever(
		PurchaseReportRetriever purchaseReportRetriever) {
	this.purchaseReportRetriever = purchaseReportRetriever;
}
}