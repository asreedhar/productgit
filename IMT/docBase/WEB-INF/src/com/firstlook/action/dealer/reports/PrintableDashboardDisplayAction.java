package com.firstlook.action.dealer.reports;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.dealer.helper.DashboardHelper;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.form.DealerForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.persistence.report.IInventoryBucketDAO;
import com.firstlook.report.InventoryStockingReport;
import com.firstlook.service.inventorystocking.InventoryStockingService;

public class PrintableDashboardDisplayAction extends SecureBaseAction
{

private InventoryStockingService inventoryStockingService;
private DashboardHelper dashboardHelper;
private IInventoryBucketDAO inventoryBucketDAO;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	int inventoryType = getMemberFromRequest( request ).getInventoryType().getValue();

	Dealer dealer = getImtDealerService().retrieveDealer( RequestHelper.getInt( request, "dealerId" ) );
	DealerForm dealerForm = new DealerForm( dealer );
	request.setAttribute( "dealerForm", dealerForm );
	Boolean includeBackEndGrossProfit = dealer.getDealerPreference().getIncludeBackEndInValuation();
	request.setAttribute( "includeBackEndInValuation", includeBackEndGrossProfit );

	dashboardHelper.putPrintableDashboardInRequest( inventoryType, getDealerFactsDAO(), request, dealer, inventoryBucketDAO );

	putFaxEmailFormInRequest( request );

	putStockingReportInRequest( currentDealerId, inventoryType, request );

	return mapping.findForward( "success" );
}

private void putStockingReportInRequest( int currentDealerId, int inventoryType, HttpServletRequest request )
{
	Collection<InventoryStockingReport> inventoryStockingCollection = inventoryStockingService.retrieveInventoryStockingReport( currentDealerId, inventoryType );
	request.setAttribute( "inventoryStockingReport", inventoryStockingCollection );
}

public InventoryStockingService getInventoryStockingService()
{
	return inventoryStockingService;
}

public void setInventoryStockingService( InventoryStockingService inventoryStockingService )
{
	this.inventoryStockingService = inventoryStockingService;
}

public DashboardHelper getDashboardHelper()
{
    return dashboardHelper;
}

public void setDashboardHelper( DashboardHelper dashboardHelper )
{
    this.dashboardHelper = dashboardHelper;
}

public IInventoryBucketDAO getInventoryBucketDAO()
{
	return inventoryBucketDAO;
}

public void setInventoryBucketDAO( IInventoryBucketDAO inventoryBucketDAO )
{
	this.inventoryBucketDAO = inventoryBucketDAO;
}
}
