package com.firstlook.action.dealer.vehicledetailpage;

import org.apache.struts.action.ActionForm;

public class SearchStockNumberOrVinForm extends ActionForm
{

private static final long serialVersionUID = 6324019526858700292L;
private String stockNumberOrVin = "";

public SearchStockNumberOrVinForm() {
	super();
}

public String getStockNumberOrVin()
{
    return stockNumberOrVin;
}

public void setStockNumberOrVin( String stockNumberOrVin )
{
    this.stockNumberOrVin = stockNumberOrVin.toUpperCase();
}

}
