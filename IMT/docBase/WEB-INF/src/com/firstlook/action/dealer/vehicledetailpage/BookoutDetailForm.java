package com.firstlook.action.dealer.vehicledetailpage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;

import com.firstlook.display.tradeanalyzer.DisplayGuideBook;

public class BookoutDetailForm extends ActionForm
{

private static final long serialVersionUID = 5082368090940304545L;
private String vin;
private Integer mileage;
private Collection<DisplayGuideBook> displayGuideBooks = new ArrayList<DisplayGuideBook>();

public BookoutDetailForm()
{
}

public Collection<DisplayGuideBook> getDisplayGuideBooks() {
	return displayGuideBooks;
}

public void setDisplayGuideBooks( Collection<DisplayGuideBook> displayGuideBooks ) {
	this.displayGuideBooks = displayGuideBooks;
}

public void addDisplayGuideBook( DisplayGuideBook displayGuideBook ) {
	this.displayGuideBooks.add( displayGuideBook );
}

public DisplayGuideBook findDisplayGuideBookByGuideBookId( int guideBookId )
{
    if ( displayGuideBooks != null )
    {
        Iterator<DisplayGuideBook> iter = displayGuideBooks.iterator();

        while (iter.hasNext())
        {
            DisplayGuideBook displayGuideBook = iter.next();
            if ( displayGuideBook.getGuideBookId() != null && displayGuideBook.getGuideBookId().intValue() == guideBookId )
            {
                return displayGuideBook;
            }
        }
    }
    return null;
}

public Integer getMileage()
{
	return mileage;
}

public void setMileage( Integer mileage )
{
	this.mileage = mileage;
}

public String getVin()
{
	return vin;
}

public void setVin( String vin )
{
	this.vin = vin;
}

public void setConditionId( int conditionId )
{
	if( displayGuideBooks != null )
	{
		Iterator<DisplayGuideBook> guideBookIter = displayGuideBooks.iterator();
		DisplayGuideBook guideBook;
		while ( guideBookIter.hasNext() )
		{
			guideBook = (DisplayGuideBook)guideBookIter.next();
			if ( guideBook.getGuideBookId() != null && guideBook.getGuideBookId().intValue() == ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE )
			{
				guideBook.setConditionId( conditionId );
			}
		}
	}
}

public void reset( ActionMapping mapping, HttpServletRequest request )
{
	if( displayGuideBooks != null )
	{
		Iterator<DisplayGuideBook> guideBookIter = displayGuideBooks.iterator();
		DisplayGuideBook guideBook;
		while ( guideBookIter.hasNext() )
		{
			guideBook = guideBookIter.next();
			guideBook.setSelectedEquipmentOptionKeys( new String[0] );
		}
	}
}

}
