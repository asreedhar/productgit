package com.firstlook.action.dealer.cia;

import org.apache.commons.collections.Predicate;

public class ParamFilterPredicate implements Predicate
{

private String prefix = "";

public ParamFilterPredicate( String prefix )
{
    this.prefix = prefix;
}

public boolean evaluate( Object value )
{
    if ( value != null && value instanceof String )
    {
        String string = (String) value;
        return string.startsWith(prefix);
    } else
    {
        return false;
    }
}

}
