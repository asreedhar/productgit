package com.firstlook.action.dealer.scorecard;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ReportDatesScoreCardData extends ScoreCardData
{

private Date reportDate;
private int weekNumber;

public ReportDatesScoreCardData()
{
    super();
}

public Date getReportDate()
{
    return reportDate;
}

public String getReportDateFormatted()
{
    SimpleDateFormat formatter = new SimpleDateFormat("MMMMM dd, yyyy");

    if ( reportDate != null )
        return formatter.format(reportDate);
    else
        return null;
}

public int getWeekNumber()
{
    return weekNumber;
}

public void setReportDate( Date date )
{
    reportDate = date;
}

public void setWeekNumber( int i )
{
    weekNumber = i;
}

}
