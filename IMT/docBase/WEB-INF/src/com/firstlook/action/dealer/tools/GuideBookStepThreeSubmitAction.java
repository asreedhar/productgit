package com.firstlook.action.dealer.tools;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.email.EmailContextBuilder;
import biz.firstlook.commons.services.vehicleCatalog.IVehicleCatalogService;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogEntry;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogServiceException;
import biz.firstlook.commons.util.BasicAuthAdapter;
import biz.firstlook.module.vehicle.description.old.BookOutError;
import biz.firstlook.module.vehicle.description.old.GBException;
import biz.firstlook.module.vehicle.description.old.GuideBookInput;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.services.photos.PhotosServiceClient;
import biz.firstlook.services.photos.entity.PhotoListObjects;
import biz.firstlook.services.photos.entity.PhotosListResponse;
import biz.firstlook.transact.persist.model.AuditBookOuts;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.Dealership;
import biz.firstlook.transact.persist.model.InventoryTypeEnum;
import biz.firstlook.transact.persist.model.JobTitle;
import biz.firstlook.transact.persist.model.MMRVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.persistence.HandleDAO;
import biz.firstlook.transact.persist.person.IPerson;
import biz.firstlook.transact.persist.person.PositionType;
import biz.firstlook.transact.persist.service.DealerService;
import biz.firstlook.transact.persist.service.ThirdPartyVehicleOptionService;
import biz.firstlook.transact.persist.service.appraisal.AppraisalActionType;
import biz.firstlook.transact.persist.service.appraisal.AppraisalFactory;
import biz.firstlook.transact.persist.service.appraisal.AppraisalPhotoKeys;
import biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum;
import biz.firstlook.transact.persist.service.appraisal.AppraisalValue;
import biz.firstlook.transact.persist.service.appraisal.Customer;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;
import biz.firstlook.transact.persist.service.appraisal.IDeal;
import biz.firstlook.transact.persist.service.vehicle.VehicleDAO;

import com.firstlook.action.redirection.JdPowerRedirectionAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.display.tradeanalyzer.DisplayGuideBook;
import com.firstlook.email.AppraisalNotificationEmailBuilder;
import com.firstlook.email.LithiaCarCenterAppraisalNotificationEmailBuilder;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.TradeAnalyzerForm;
import com.firstlook.entity.form.TradeManagerEditBumpForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;
import com.firstlook.service.bookout.appraisal.AppraisalBookOutService;
import com.firstlook.service.bookout.bean.BookValuesBookOutState;
import com.firstlook.service.vehicle.VehicleService;
import com.firstlook.session.FirstlookSession;

public class GuideBookStepThreeSubmitAction extends AbstractTradeAnalyzerFormAction
{

protected static Logger logger = Logger.getLogger( GuideBookStepThreeSubmitAction.class );	

private AppraisalBookOutService appraisalBookOutService;
private IAppraisalService appraisalService;
private AppraisalFactory appraisalFactory;
private VehicleService vehicleService;
private DealerService transactPersistDealerService;
private IVehicleCatalogService vehicleCatalogService;
private HandleDAO handleDao;
private VehicleDAO vehicleDao;


protected ActionForward analyzeIt(ActionMapping mapping, TradeAnalyzerForm tradeAnalyzerForm,	HttpServletRequest request, HttpServletResponse response, Dealer dealer)
	throws DatabaseException, ApplicationException
{
	
	boolean dealerHasAuctionData = false;
		String pageName = (String)request.getParameter( "pageName" );
    clearCheckBoxes(tradeAnalyzerForm, request);
    
	int currentDealerId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();
	DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerId( currentDealerId );
	
	request.setAttribute( "ownerHandle", handleDao.getOwnerHandle(new Integer(currentDealerId)) );
	request.setAttribute( "vehicleHandle", handleDao.getVehicleHandle(vehicleDao,tradeAnalyzerForm.getVin()));
	List<Member> contactsToEmailList = null;
	Integer jobTitleToEmail = null;
	if (dealerGroup.isLithiaStore()) {
		jobTitleToEmail = JobTitle.LITHIA_CAR_CENTER_ID;
	} else {
		jobTitleToEmail =JobTitle.UCM_ID;
	}
	
	contactsToEmailList = getMemberService().getMembersByBusinessUnitAndJobTitle( currentDealerId, jobTitleToEmail );

	request.setAttribute( "contactsToEmailList", contactsToEmailList );
	
	//String basicAuthString = (String) request.getSession().getServletContext().getAttribute(BasicAuthAdapter.NAME_IN_SESSION);
//	String basicAuthString = (String) request.getSession().getAttribute(BasicAuthAdapter.NAME_IN_SESSION);
	
	//tradeAnalyzerForm.populateMMRAreas(basicAuthString);
	//tradeAnalyzerForm.populateMMRTrims(basicAuthString);	
	//tradeAnalyzerForm.populateMMRTransactions(basicAuthString);
	
	ActionErrors guideBookErrors = tradeAnalyzerForm.validateGuideBook();
	if ( guideBookErrors != null && !guideBookErrors.isEmpty() )
	{
		putActionErrorsInRequest( request, tradeAnalyzerForm.validateGuideBook() );
	}
	else
	{

		Dealer currentDealer = getDealerDAO().findByPk( currentDealerId );
		DealerPreference dealerPref = currentDealer.getDealerPreference();

		// save changes to people
		Collection< IPerson > people = getPersonService().saveOrUpdate( request.getParameter( "selectedPersonId" ), currentDealerId );

		if (AppraisalTypeEnum.TRADE_IN == tradeAnalyzerForm.getAppraisalTypeEnum()) {
			// track selection of new sales person
			tradeAnalyzerForm.setDealTrackSalesPersonID( people );
		} 

        List<IPerson> appraisers = new ArrayList<IPerson>();
        PositionType appraiserType = PositionType.valueFromPositionString("Appraiser");
        if (appraiserType != null) {
            appraisers = (List<IPerson>) getPersonService().getBusinessUnitPeopleByPosition(currentDealerId, appraiserType);
            //response.addHeader("posDesc", type.getPosition());
        }
        
        //sortPeople(people, theForm.getSortColumn(), theForm.getSortDir());
        //theForm.setPeople(people);
        tradeAnalyzerForm.setAppraisers(appraisers);
    	request.setAttribute( "appraisers", appraisers );
        
        Integer selectedAppraiserIndex = -1;
        Integer i = 0;
        for (IPerson appraiser:appraisers){
            if (appraiser.getSelected()) {
                selectedAppraiserIndex = i;
                break;
            }
            i++;
        }
        
        tradeAnalyzerForm.setSelectedAppraiserIndex(selectedAppraiserIndex);
        //getSelectedRow(people, response);
        
    	request.setAttribute( "selectedAppraiserIndex", selectedAppraiserIndex );
        
		Member member = getMemberFromRequest( request );
		GuideBookInput input = new GuideBookInput();
		input.setVin( tradeAnalyzerForm.getVin() );
		input.setBusinessUnitId( currentDealerId );
		input.setMileage( tradeAnalyzerForm.getMileage() );
		input.setState( currentDealer.getState() );
		input.setMemberId( member.getMemberId() );
		input.setNadaRegionCode( dealerPref.getNadaRegionCode() );

		request.setAttribute( "defaultContactId", dealerPref.getDefaultLithiaCarCenterMemberId() );
		request.setAttribute( "isLithia", dealerGroup.isLithiaStore() );

		
		// Check form for properly selected dropdowns BEFORE we call bookoutAndSaveInformation
		if ( !tradeAnalyzerForm.isValidDescription() )
		{
			return mapping.findForward( "failure" );
		}

		// everything happens here
		boolean allDropdownsSelected = bookoutAndSaveInformation( tradeAnalyzerForm, input, request,
																	currentDealerId, tradeAnalyzerForm.getVin(),
																	dealerPref.getSearchAppraisalDaysBackThreshold() );
		
		
		
		String basicAuthString2=(String) request.getSession().getServletContext().getAttribute(BasicAuthAdapter.NAME_IN_SESSION);;
		Context initialContext;
		try {
			initialContext = new javax.naming.InitialContext();
			Context ctx = (Context) initialContext.lookup("java:comp/env");
			String user= (String) ctx.lookup("userNameForAdmin");
			String password= (String) ctx.lookup("passwordForAdmin");
			
			
			BasicAuthAdapter baa = new BasicAuthAdapter(user, password);
			basicAuthString2= baa.basicAuthString();	
			
		
		} catch (NamingException e1) {
			e1.printStackTrace();
		} 

		
		
		// delete the pending now that we have a real edge one
		//before deletion copy the photos in db and the service
		if(allDropdownsSelected){
			IAppraisal pendingAppraisal = tradeAnalyzerForm.getPendingAppraisal();
			PhotosListResponse resp=null;
			if (pendingAppraisal != null) {
				IAppraisal toDelete = appraisalService.findBy(pendingAppraisal.getAppraisalId());
				
				try {
					PhotosServiceClient psc= new PhotosServiceClient(basicAuthString2);
					if(toDelete!=null&&toDelete.getUniqueId()!=null && !toDelete.getUniqueId().equals(""))
						resp=psc.copyPhotos(currentDealerId+"", toDelete.getUniqueId(), 7, tradeAnalyzerForm.getAppraisalId()+"");
					
				}catch(Exception e){
					e.printStackTrace();
					logger.error("Exception Occured while deleting Photo",e);
				}
				
				IAppraisal appraisal= appraisalService.findBy(tradeAnalyzerForm.getAppraisalId());
				if(resp!=null&& resp.getAppraisalPhotos()!=null){
					
					Iterator<PhotoListObjects> itr = resp.getAppraisalPhotos().iterator();
					
					while(itr.hasNext()){
						if(appraisal.getAppraisalPhotoKeys()==null){
							appraisal.setAppraisalPhotoKeys(new ArrayList<AppraisalPhotoKeys>());
						}
						PhotoListObjects photo= itr.next();
						if(photo.getsequenceNo()!=null&&!photo.getsequenceNo().equalsIgnoreCase(""))
							appraisal.getAppraisalPhotoKeys().add(new AppraisalPhotoKeys(appraisal, photo.getThumbUrl(), photo.getPhotoURl(), Integer.parseInt(photo.getsequenceNo())));
						
					}
				}
				
				
				appraisalService.save(appraisal);
				
				appraisalService.deleteAppraisal(toDelete);
				tradeAnalyzerForm.setPendingAppraisal(null);
			}
			IAppraisal appraisal= appraisalService.findBy(tradeAnalyzerForm.getAppraisalId());

			if(appraisal.getAppraisalValues()!=null && appraisal.getAppraisalValues().size()>0 &&appraisal.getLatestAppraisalValue()!=null)
			{
				if(!tradeAnalyzerForm.getAppraiserName().equals(appraisal.getLatestAppraisalValue().getAppraiserName())){
					 
					appraisal.bump(appraisal.getLatestAppraisalValue().getValue(),(tradeAnalyzerForm.getAppraiserName()!= null ? tradeAnalyzerForm.getAppraiserName() : ""),member.getFirstName()+" "+member.getLastName());
				}
			}
			else
			{
//				It has been handled in populate appraisal
//				appraisal.bump(0,(tradeAnalyzerForm.getAppraiserName()!= null ? tradeAnalyzerForm.getAppraiserName() : ""));
			}
			appraisalService.save(appraisal);
			
		}
		else {
			return mapping.findForward( "failure" );
		}
		
		
		putBumpsInRequest( request, tradeAnalyzerForm.getAppraisalId() );

		dealerHasAuctionData = getFirstlookSessionFromRequest( request ).isIncludeAuction();
		request.setAttribute( "dealerHasAuctionData", ( new Boolean( dealerHasAuctionData ) ) );
		request.setAttribute( "enablePrintMenuForTradeAnalyzerOrTradeManager", Boolean.TRUE );
		request.setAttribute( "people", people );
		
		request.setAttribute("displayTMV", getFirstlookSessionFromRequest(request).hasEdmundsTmvUpgrade());
		request.setAttribute( "twixURL", currentDealer.getDealerPreference().getTwixURL());
		
		request.setAttribute("displayPing", getFirstlookSessionFromRequest( request ).isIncludePing());
		request.setAttribute( "zipcode", dealer.getZipcode() );
		request.setAttribute("displayPingII", getFirstlookSessionFromRequest( request ).isIncludePingII());
	}
	
	try
	{
		VehicleCatalogEntry vce = vehicleCatalogService.retrieveVehicleCatalogEntry( tradeAnalyzerForm.getVin(), tradeAnalyzerForm.getCatalogKeyId() );
		
		request.setAttribute("showJDPower", getFirstlookSessionFromRequest(request).isIncludeJDPowerUsedCarMarketData());
		request.setAttribute("JDPowerUrl", JdPowerRedirectionAction.getRelativeUrl( vce.getCatalogKey().getVehicleCatalogId(), vce.getModelYear()) );
	}
	catch ( VehicleCatalogServiceException e )
	{
		logger.warn( MessageFormat.format( "Could not find vehicle catalogid for vin: {0}, disabling JDPowerMarketAnalyzer.", tradeAnalyzerForm.getVin()));
	}

	boolean smsNotify = tradeAnalyzerForm.isSmsNotify();

	String forward = getMappingForward( request, pageName );
	
	if ( smsNotify && !forward.equalsIgnoreCase("failure"))
	{
		Member member = getMemberFromRequest(request);
		EmailContextBuilder appraisalNotification = null;
		
		if( dealerGroup.isLithiaStore() ){
			
			Member contactToEmail = null;
			
			for( Member contact : contactsToEmailList) {
				if (Arrays.asList( tradeAnalyzerForm.getManagers() ).contains( contact.getMemberId() +"" ) ) {
					contactToEmail = contact;
					break;
				}
			}
			
			if( contactToEmail == null) {
				logger.error( "dealer: " + currentDealerId + " does not have a DefaultLithiaCarCenterMember set - no e-mail was sent!!! " );
			} else {
				contactsToEmailList.remove( contactToEmail );
				
				List<String> contactsToEmailAddresses = new ArrayList< String >();
				for(Member contact : contactsToEmailList) {
					contactsToEmailAddresses.add( contact.getEmailAddress() );
				}
				
				appraisalNotification = new LithiaCarCenterAppraisalNotificationEmailBuilder( dealer.getNickname(),  
				                                                                              tradeAnalyzerForm.getYear(),
																								tradeAnalyzerForm.getMake(),
																								tradeAnalyzerForm.getModel(),
																								tradeAnalyzerForm.getMileage(), 
																								tradeAnalyzerForm.getAppraisalId(),
																								contactToEmail.getEmailAddress(),
																								contactsToEmailAddresses,
																								member.getEmailAddress() );
			}
			                                                                              
		} else {
			appraisalNotification = new AppraisalNotificationEmailBuilder(	member.getPreferredFirstName(),
																											member.getFirstName(),
																											member.getLastName(),
																											tradeAnalyzerForm.getYear(),
																											tradeAnalyzerForm.getMake(),
																											tradeAnalyzerForm.getModel(),
																											tradeAnalyzerForm.getMileageFormatted(),
																											tradeAnalyzerForm.getManagers() );
		}
		if ( appraisalNotification != null) {
			getEmailService().sendEmail( appraisalNotification );
		}
	}
	
	String showResults = "true";
	SessionHelper.setAttribute( request, "showResults", showResults );
	SessionHelper.keepAttribute( request, "showResults" );
	
	Map<AppraisalActionType, Integer> countsOfTabs = appraisalService.countTradeInAppraisalsAndGroupByAppraisalActionType( dealer.getDealerId(), dealer.getDealerPreference().getTradeManagerDaysFilter(), dealer.getDealerPreference().getShowInactiveAppraisals() );
	request.setAttribute( "waitingAppraisalsCount", countsOfTabs.get( AppraisalActionType.WAITING_REVIEW ) );
	//request.removeAttribute("tradeAnalyzerForm");
	ActionForward actionForward = mapping.findForward( forward );
	request.setAttribute("currentDate",new Date());

	return actionForward; 
}

@SuppressWarnings("deprecation")
private void putBumpsInRequest( HttpServletRequest request, Integer appraisalId ) throws DatabaseException, ApplicationException
{
	IAppraisal appraisal = appraisalService.findBy( appraisalId );
	List<AppraisalValue> appraisalValues = new ArrayList<AppraisalValue>();
	appraisalValues.addAll( appraisal.getAppraisalValues() );
	Collections.reverse( appraisalValues );
	TradeManagerEditBumpForm tradeManagerEditBumpForm = new TradeManagerEditBumpForm();
	String dateString = "";
	if ( !appraisalValues.isEmpty() )
	{
		AppraisalValue firstValue = (AppraisalValue)appraisalValues.get( 0 );
		tradeManagerEditBumpForm.setAppraiserName( firstValue.getAppraiserName() );
		dateString = firstValue.getDateCreated().toLocaleString();
		tradeManagerEditBumpForm.setAppraisalBumpDate(dateString);
		tradeManagerEditBumpForm.setAppraisalValue( firstValue.getValue() );
	}
	tradeManagerEditBumpForm.setVehicleGuideBookBumpCol( appraisalValues );
	tradeManagerEditBumpForm.setAppraisalId( appraisalId.intValue() );
	if ( !appraisalValues.isEmpty() )
	{
		request.setAttribute( "numberOfAppraisal", appraisalValues.size() );
	}
	FirstlookSession flSession = (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION );
	int currentDealerId = ( flSession ).getCurrentDealerId();
	DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerId( currentDealerId );
	
	request.setAttribute("displayTMV", flSession.hasEdmundsTmvUpgrade());
	
//	if( dealerGroup.isLithiaStore()) {
//		loadLithiaCarCenterAttributes(request, dealer);
//	}
//	
    checkInGroupAppraisals(appraisal.getBusinessUnitId(), dealerGroup.getDealerGroupId(),appraisal.getVehicle().getVin(), request);
    
    request.setAttribute("appraisersList", getPersonService().getBusinessUnitPeopleByPosition(currentDealerId,PositionType.APPRAISER));
    
	request.setAttribute("isLithia", new Boolean( dealerGroup.isLithiaStore() ));
	request.setAttribute( "tradeManagerEditBumpForm", tradeManagerEditBumpForm );
    request.setAttribute("vin", appraisal.getVehicle().getVin());
	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );
	SessionHelper.keepAttribute( request, "auctionData" );
}

@SuppressWarnings({ "unchecked", "rawtypes" })
private void checkInGroupAppraisals(Integer businessUnitId, Integer parentId, String vin, HttpServletRequest request) {
    List<Map> results = appraisalService.retrieveInGroupAppraisals(businessUnitId, parentId,vin);
    boolean hasGroupAppraisals = (results.isEmpty() ? false:true);
    request.setAttribute("hasGroupAppraisals", hasGroupAppraisals);
    if (hasGroupAppraisals) {
        request.setAttribute("created",results.get(0).get("created"));
    } 
}

private boolean bookoutAndSaveInformation( TradeAnalyzerForm tradeAnalyzerForm, GuideBookInput input,HttpServletRequest request, int businessUnitId, String vin,
											int searchAppraisalDaysBackThreshhold ) throws ApplicationException
{
	ActionErrors guideBookErrors = new ActionErrors();
	boolean allKeysSelected = true;

	IAppraisal appraisal;
	try {
		appraisal = populateAppraisal( tradeAnalyzerForm, input, businessUnitId, vin, searchAppraisalDaysBackThreshhold );
	} catch (VehicleCatalogServiceException e1) {
		logger.error("Fatal Error: Can not build appraisal. Unable to identify vehicle in vehicle catalog. ");
		throw new ApplicationException("Fatal Error: Can not build appraisal. Unable to identify vehicle in vehicle catalog. ");
	}
	tradeAnalyzerForm.setAppraisalId( appraisal.getAppraisalId() );
	
	DisplayGuideBook guideBook;
	VDP_GuideBookVehicle vehicle;
	String selectedKey;

	List<DisplayGuideBook> guideBooks = (List<DisplayGuideBook>)tradeAnalyzerForm.getDisplayGuideBooks();
	Iterator<DisplayGuideBook> guideBookIter;
	allKeysSelected = validateAllKeysSelected( request, guideBookErrors, allKeysSelected, guideBooks );
	
	if ( allKeysSelected == false ){
		return false;
	}

	guideBookIter = guideBooks.iterator();
	
	while ( guideBookIter.hasNext() )
	{
		guideBook = guideBookIter.next();
		selectedKey = guideBook.getSelectedVehicleKey();
		if ( guideBook.isValidVin() )
		{
			vehicle = guideBook.determineSelectedVehicle( selectedKey );
			if ( vehicle != null )
			{
				vehicle.setCondition( guideBook.getCondition() );
				guideBook.setSelectedEquipmentOptionKeys( ThirdPartyVehicleOptionService.setAndUpdateSelectedOptionsUsingArray(
																																guideBook.getDisplayEquipmentGuideBookOptions(),
																																guideBook.getSelectedEquipmentOptionKeys() ) );
				// these three are for Kelly book outs only
				ThirdPartyVehicleOptionService.setSelectedOption( guideBook.getDisplayEngineGuideBookOptions(),
																	guideBook.getSelectedEngineKey() );
				ThirdPartyVehicleOptionService.setSelectedOption( guideBook.getDisplayTransmissionGuideBookOptions(),
																	guideBook.getSelectedTransmissionKey() );
				ThirdPartyVehicleOptionService.setSelectedOption( guideBook.getDisplayDrivetrainGuideBookOptions(),
																	guideBook.getSelectedDrivetrainKey() );

				try
				{
					BookValuesBookOutState bookOutState = getAppraisalBookOutService().updateBookValues(
																											input,
																											vehicle,
																											guideBook.getDisplayGuideBookOptions(),
																											(List<VDP_GuideBookVehicle>)guideBook.getDisplayGuideBookVehiclesCollection(),
																											AuditBookOuts.APPRAISAL_BOOKOUT_ID,
																											appraisal,
																											guideBook.getGuideBookId() );
					
					Map<Integer, List<BookOutError>> bookOutErrorsByType = bookOutState.getBookOutErrors();
					List<BookOutError> bookOutErrors = bookOutErrorsByType.get(Integer.valueOf( BookOutError.OPTIONS_CONFLICT));
					if ( bookOutErrors != null )
					{
						Iterator<BookOutError> bookOutErrorsIter = bookOutErrors.iterator();
						while ( bookOutErrorsIter.hasNext() )
						{
							BookOutError bookOutError = bookOutErrorsIter.next();
							guideBookErrors.add( ActionErrors.GLOBAL_ERROR, new ActionError(	"error.kelley.conflict",
																								bookOutError.getFirstOptionDescription(),
																								bookOutError.getSecondOptionDescription() ) );
						}

						request.setAttribute( "guideBookError", Boolean.TRUE);
						putActionErrorsInRequest( request, guideBookErrors );
						guideBook.setHasErrors( Boolean.TRUE );
						return true;
					}
					else {
						appraisalService.save( appraisal );
					}
				}
				catch ( GBException e ){
					if( appraisal.getAppraisalId() != null ){
						appraisalService.deleteAppraisal(appraisal);
					}
					return false;
				}
			}
			else
			// this is the case when you want to save an appraisal without book values (third party connectivity errors)
			{
				appraisalService.save( appraisal );
			}
			//no vin found for book - should just see one book if vin is found for the other one
		}
	}
	
	return true;
}

private boolean validateAllKeysSelected( HttpServletRequest request, ActionErrors guideBookErrors, boolean allKeysSelected, List<DisplayGuideBook> guideBooks )
{
	DisplayGuideBook guideBook;
	String selectedKey;
	Iterator<DisplayGuideBook> guideBookIter = guideBooks.iterator();
	while ( guideBookIter.hasNext() )
	{
		guideBook = guideBookIter.next();

		// Re-initialize 'hasErrors' flag each time we pass thru here.
		guideBook.setHasErrors( Boolean.FALSE );

		// If connection or vin cannot be found, do not continue testing the selected dropdowns.
		if ( guideBook.isSuccess() == false )
			continue;

		selectedKey = guideBook.getSelectedVehicleKey();
		if ( selectedKey.equals( "0" ) || selectedKey.equals( "" ) )
		{
			if ( guideBookErrors != null )
			{
				guideBookErrors.add(
										ActionErrors.GLOBAL_ERROR,
										new ActionError(	"error.modelmanagement.selectmodel",
															ThirdPartyDataProvider.getThirdPartyDataProviderDescription( guideBook.getGuideBookId().intValue() ) ) );
				request.setAttribute( "guideBookError", new Boolean( true ) );
				putActionErrorsInRequest( request, guideBookErrors );
				guideBook.setHasErrors( Boolean.TRUE );
			}
			allKeysSelected = false;
		}
	}
	return allKeysSelected;
}

private IAppraisal populateAppraisal( TradeAnalyzerForm tradeAnalyzerForm, GuideBookInput input, int businessUnitId, String vin,
										int searchAppraisalDaysBackThreshhold ) throws VehicleCatalogServiceException
{
	IAppraisal appraisal = appraisalService.findLatest( businessUnitId, vin, searchAppraisalDaysBackThreshhold );
	Vehicle vehicle = createNewVehicleOrUpdate( tradeAnalyzerForm );
	
	IPerson buyer = null;
	if (tradeAnalyzerForm.getBuyerId() != null && tradeAnalyzerForm.getBuyerId() > 0) {
		buyer = getPersonService().getPerson(tradeAnalyzerForm.getBuyerId());
		tradeAnalyzerForm.setBuyerName(buyer.getFullName());
	}
	else{
		tradeAnalyzerForm.setBuyerName("");
	}
	
	
	if ( appraisal == null || appraisal.getAppraisalSource().isCRMOrMobile())
	{
		
		Integer dealTrackSalespersonID = null;
		try
		{
			dealTrackSalespersonID = Integer.parseInt( tradeAnalyzerForm.getDealTrackSalespersonID() );
		}
		catch ( NumberFormatException nfe ){}
		
		Customer customer = null;
		IDeal deal  = null;
		if (tradeAnalyzerForm.getAppraisalTypeEnum().equals(AppraisalTypeEnum.TRADE_IN)) {
			
			customer = appraisalFactory.createCustomer( tradeAnalyzerForm.getCustomerFirstName(), tradeAnalyzerForm.getCustomerLastName(),
																tradeAnalyzerForm.getCustomerGender(), tradeAnalyzerForm.getCustomerEmail(),
																tradeAnalyzerForm.getCustomerPhoneNumber() );
			deal = appraisalFactory.createDeal( dealTrackSalespersonID,
					InventoryTypeEnum.getInventoryTypeEnum( tradeAnalyzerForm.getDealTrackNewOrUsed() ),
					tradeAnalyzerForm.getDealTrackStockNumber() );
		}


		Dealership dealer = transactPersistDealerService.findByDealerId( businessUnitId );
		Member member= getMemberService().retrieveMember(input.getMemberId());
		AppraisalValue appraisalValue = null;
		if (tradeAnalyzerForm.getAppraisalValue() != null) {
			appraisalValue = appraisalFactory.createAppraisalValue(tradeAnalyzerForm.getAppraisalValue(), tradeAnalyzerForm.getAppraiserName());
		}
		if(appraisalValue!=null){
			appraisalValue.setMemberName(member.getFirstName()+" "+member.getLastName());
			appraisalValue.setAppraiserName(tradeAnalyzerForm.getAppraiserName());
		}
		MMRVehicle mmrVehicle = (tradeAnalyzerForm.getSelectedMmrVehicle() == null) 
			? null 
			: tradeAnalyzerForm.getSelectedMmrVehicle();
		if ( appraisal != null && appraisal.getAppraisalSource().isCRMOrMobile()){
			   
			   MMRVehicle newMMRVehicle= new MMRVehicle();
			   if(mmrVehicle!=null)
			   {
				   newMMRVehicle.setRegionCode(mmrVehicle.getRegionCode());
				   newMMRVehicle.setMid(mmrVehicle.getMid());
				   newMMRVehicle.setAveragePrice(mmrVehicle.getAveragePrice());
				   newMMRVehicle.setDateUpdated(new Date());
				   tradeAnalyzerForm.setSelectedMmrVehicle(newMMRVehicle);   
			   }
			   else
			   {
				   newMMRVehicle = null;
			   }
			   
			   mmrVehicle = newMMRVehicle;
			  }
		// no notes gui yet.
		appraisal = appraisalService.createAppraisal( dealer, vehicle, tradeAnalyzerForm.getMileage().intValue(),
														tradeAnalyzerForm.getReconditioningCost(), tradeAnalyzerForm.getTargetGrossProfit(), tradeAnalyzerForm.getConditionDisclosure(),
														new Double( tradeAnalyzerForm.getPrice() ), appraisalValue, deal, customer, tradeAnalyzerForm.getNotes(),tradeAnalyzerForm.getAppraisalTypeEnum(), mmrVehicle);
		
		appraisal.setColor( tradeAnalyzerForm.getColor() );
		appraisal.setMemberId( input.getMemberId() );
		appraisal.updateInventoryDecision( AppraisalActionType.DECIDE_LATER, tradeAnalyzerForm.getReconditioningCost(), tradeAnalyzerForm.getTargetGrossProfit(),
										tradeAnalyzerForm.getConditionDisclosure(), Double.valueOf( tradeAnalyzerForm.getPrice() ), tradeAnalyzerForm.getTransferPrice(), tradeAnalyzerForm.isTransferForRetailOnly() );
		appraisal.setSelectedBuyer(buyer);
		appraisal.setDateModified(new Date());
		appraisalService.updateAppraisal( appraisal );
		
		
		//hack to keep compatibility with more update calls in the same hibernate Session.
		appraisal = appraisalService.findBy( appraisal.getAppraisalId() );
	} 
    // We still have to update the appraisal with the values from the new values from the form.  E.g. 
    // someone does correct vehicle style on the TM and also changes the stock number.
    else {
        appraisal.setMileage(tradeAnalyzerForm.getMileage());
        appraisal.setColor(tradeAnalyzerForm.getColor());
       /* AppraisalValue appraisalValue= appraisal.getLatestAppraisalValue();
        if(appraisalValue!=null && appraisalValue.getAppraiserName()==null)
    		appraisalValue.setAppraiserName("");
        
        if(appraisalValue!=null)
        {
        	if(!appraisalValue.getAppraiserName().equalsIgnoreCase(tradeAnalyzerForm.getAppraiserName())){
        		appraisalValue.setDateCreated(new Date());
        		appraisalValue.setAppraiserName(appraisalValue.getAppraiserName());
        		appraisal.getAppraisalValues().add(appraisalValue);
        	}
        }*/
        
        // case where they are correctign vehicle style and switching type
        if (!tradeAnalyzerForm.getAppraisalTypeEnum().equals(AppraisalTypeEnum.getType(appraisal.getAppraisalTypeId()))){
        	appraisal.setAppraisalTypeId(tradeAnalyzerForm.getAppraisalTypeEnum().getId());
        }
        
        //if (tradeAnalyzerForm.getAppraisalTypeEnum().equals(AppraisalTypeEnum.TRADE_IN)) {
	        Integer dealTrackSalespersonID = null;
	        IPerson salesPerson = null;
	        try
	        {
	            dealTrackSalespersonID = Integer.parseInt( tradeAnalyzerForm.getDealTrackSalespersonID() );
	        }
	        catch ( NumberFormatException nfe ){}
	
	        if (dealTrackSalespersonID != null) {
	            salesPerson = getPersonService().getPerson(dealTrackSalespersonID);
	        }
	        appraisal.setPotentialDeal(salesPerson, tradeAnalyzerForm.getDealTrackStockNumber(), 
	                InventoryTypeEnum.getInventoryTypeEnum(tradeAnalyzerForm.getDealTrackNewOrUsed()));
        //} 
        
	        MMRVehicle vehicleMMR=tradeAnalyzerForm.getSelectedMmrVehicle();
	                if(appraisal.getMmrVehicle()!=null){
	                	vehicleMMR= appraisal.getMmrVehicle();
	                	vehicleMMR.setMid(tradeAnalyzerForm.getSelectedMmrVehicle().getMid());
	                	vehicleMMR.setAveragePrice(tradeAnalyzerForm.getSelectedMmrVehicle().getAveragePrice());
	                	vehicleMMR.setRegionCode(tradeAnalyzerForm.getSelectedMmrVehicle().getRegionCode());
	                }
	        	        
	                appraisal.setMmrVehicle(vehicleMMR);
        
        appraisal.setSelectedBuyer(buyer);
        appraisal.updateInventoryDecision( appraisal.getAppraisalActionType(), tradeAnalyzerForm.getReconditioningCost(), tradeAnalyzerForm.getTargetGrossProfit(),
                tradeAnalyzerForm.getConditionDisclosure(), appraisal.getWholesalePrice(), appraisal.getTransferPrice(), appraisal.isTransferForRetailOnly());
        appraisal.setDateModified(new Date());        
    }
	
	appraisal.setCustomer(tradeAnalyzerForm.getCustomerFirstName(), tradeAnalyzerForm.getCustomerLastName(), tradeAnalyzerForm.getCustomerGender(), tradeAnalyzerForm.getCustomerEmail()	, tradeAnalyzerForm.getCustomerPhoneNumber());
	return appraisal;
}

private Vehicle createNewVehicleOrUpdate( TradeAnalyzerForm tradeAnalyzerForm ) throws VehicleCatalogServiceException
{
	Vehicle vehicle = null;
	if ( !tradeAnalyzerForm.isCatalogKeyKnown() )
	{ // if not a VIN-less appraisal
		vehicle = vehicleService.loadVehicle( tradeAnalyzerForm.getVin() );
		// if no vehicle exists in the Vehicle table or the stored CatalogKey is different than the one selected
		if ( vehicle == null )
		{
			VehicleCatalogEntry vcEntry = vehicleCatalogService.retrieveVehicleCatalogEntry( tradeAnalyzerForm.getVin(), tradeAnalyzerForm.getCatalogKey().getVehicleCatalogId() );
			vehicle = new Vehicle( vcEntry );
			updateBaseColor( tradeAnalyzerForm, vehicle );
			vehicle.setCreateDate( new Date() );
			vehicleService.createNewVehicle( vehicle );

			// update TA form
			tradeAnalyzerForm.setModel( vehicle.getModel() );
		}
		else if ( ( vehicle.getVehicleCatalogKey() == null ) || (vehicle.getVehicleCatalogKey().getVehicleCatalogId().intValue() != tradeAnalyzerForm.getCatalogKey().getVehicleCatalogId().intValue() ) )
		{
			// Modify the existing vehicle to have the attributes that correspond to the new styleKey
			VehicleCatalogEntry vcEntry = vehicleCatalogService.retrieveVehicleCatalogEntry( tradeAnalyzerForm.getVin(), tradeAnalyzerForm.getCatalogKey().getVehicleCatalogId() );
			Vehicle vehicleLookUp = new Vehicle( vcEntry );
			vehicle.setVehicleCatalogKey( tradeAnalyzerForm.getCatalogKey() );
			VehicleService.copyVehicleAttributes( vehicle, vehicleLookUp );

			// Don't update InteriorColor or InteriorDescription. But update base color.
			updateBaseColor( tradeAnalyzerForm, vehicle );
			
			// update in DB
			vehicleService.updateVehicle( vehicle );

			// update TA form with Model. Used for Analytics Queries
			tradeAnalyzerForm.setModel( vehicleLookUp.getModel() );
		}
		else
		{
			// the only thing that could have been updated was color
			updateBaseColor( tradeAnalyzerForm, vehicle );
			
			vehicleService.updateVehicle( vehicle );
		}
	}
	else
	{ // VIN less appraisal 
		vehicle = vehicleService.loadVehicle( tradeAnalyzerForm.getVin() );
		if (vehicle == null) {
			VehicleCatalogEntry vcEntry = vehicleCatalogService.retrieveVehicleCatalogEntry( tradeAnalyzerForm.getMake(), tradeAnalyzerForm.getModel(),
																					tradeAnalyzerForm.getYear(), tradeAnalyzerForm.getCatalogKey().getVehicleCatalogId() );
			vehicle = new Vehicle( vcEntry );
			vehicle.setVin( tradeAnalyzerForm.getVin() ); // we want to write the VIN do we have it next time
			vehicle.setCreateDate( new Date() );
		}
		updateBaseColor( tradeAnalyzerForm, vehicle );
		vehicleService.createNewVehicle( vehicle );
	}
	// update tradeAnalyzerForm with the new trim
	tradeAnalyzerForm.setTrim( vehicle.getVehicleTrim() ); // used for analytics

	return vehicle;
}


private void clearCheckBoxes(TradeAnalyzerForm theForm, HttpServletRequest request) {
    String[] primaryBookOptions = request.getParameterValues( "displayGuideBooks[0].selectedEquipmentOptionKeys" );
    String[] secondaryBookOptions = request.getParameterValues( "displayGuideBooks[1].selectedEquipmentOptionKeys" );
    
    if (primaryBookOptions == null) {
        theForm.clearGuideBookOptions(0);
    }
    if (secondaryBookOptions == null) {
        theForm.clearGuideBookOptions(1);
    }
}

private void updateBaseColor( TradeAnalyzerForm tradeAnalyzerForm, Vehicle vehicle )
{
	if ( tradeAnalyzerForm.getColor() != null )
	{
		vehicle.setBaseColor( tradeAnalyzerForm.getColor() );
	}
	else
	{
		vehicle.setBaseColor( "UNKNOWN" );
	}
}

protected String getMappingForward( HttpServletRequest request, String pageName )
{
	String forward = "";

	if ( hasValidationErrors( request ) )
	{
		forward += "failure";
	}
	else
	{
		forward += "success";
	}

	return forward;
}

public AppraisalBookOutService getAppraisalBookOutService()
{
	return appraisalBookOutService;
}

public void setAppraisalBookOutService( AppraisalBookOutService appraisalBookOutService )
{
	this.appraisalBookOutService = appraisalBookOutService;
}

public void setVehicleService( VehicleService vehicleService )
{
	this.vehicleService = vehicleService;
}

public IAppraisalService getAppraisalService()
{
	return appraisalService;
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

public void setAppraisalFactory( AppraisalFactory appraisalFactory )
{
	this.appraisalFactory = appraisalFactory;
}

public void setTransactPersistDealerService( DealerService transactPersistDealerService )
{
	this.transactPersistDealerService = transactPersistDealerService;
}

public void setHandleDao( HandleDAO handleDao )
{
	this.handleDao = handleDao;
}
public void setVehicleDao( VehicleDAO handleDao )
{
	this.vehicleDao = handleDao;
}
public void setVehicleCatalogService( IVehicleCatalogService vehicleCatalogService )
{
	this.vehicleCatalogService = vehicleCatalogService;
}

}