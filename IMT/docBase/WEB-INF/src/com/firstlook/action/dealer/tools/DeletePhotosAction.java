package com.firstlook.action.dealer.tools;

import java.util.Iterator;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.BasicAuthAdapter;
import biz.firstlook.services.photos.PhotosServiceClient;
import biz.firstlook.transact.persist.service.appraisal.AppraisalPhotoKeys;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.google.gson.Gson;

public class DeletePhotosAction extends SecureBaseAction {

	IAppraisalService appaisalService;
	
	
	
	public IAppraisalService getAppraisalService() {
		return appaisalService;
	}

	public void setAppraisalService(IAppraisalService appaisalService) {
		this.appaisalService = appaisalService;
	}


	@Override
	public ActionForward doIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws DatabaseException, ApplicationException {


		
		
		
		String basicAuthString = (String) request.getSession().getServletContext().getAttribute(BasicAuthAdapter.NAME_IN_SESSION);
		
		Context initialContext;
		try {
			initialContext = new javax.naming.InitialContext();
			Context ctx = (Context) initialContext.lookup("java:comp/env");
			String user= (String) ctx.lookup("userNameForAdmin");
			String password= (String) ctx.lookup("passwordForAdmin");
			
			
			BasicAuthAdapter baa = new BasicAuthAdapter(user, password);
			basicAuthString= baa.basicAuthString();	
			
		
		} catch (NamingException e1) {
			e1.printStackTrace();
		} 
	     
		
		
		try {
			PhotosServiceClient psc= new PhotosServiceClient(basicAuthString);
			
			Gson gson= new Gson();
			Integer seqId= Integer.parseInt(request.getParameter("sequenceNumber"));
			Integer sourceId= Integer.parseInt(request.getParameter("sourceId"));
			//=psc.getPhotos("101620", "0a979252-2814-465b-8743-ac716b084c58");
			Boolean resp=psc.deletePhotos(getFirstlookSessionFromRequest(request).getCurrentDealerId()+"", request.getParameter("appraisalId"),sourceId,seqId);
			
			
			IAppraisal appraisal=getAppraisalService().findBy(Integer.parseInt(request.getParameter("appraisalId")));
			
			if(appraisal.getAppraisalPhotoKeys()!=null){
				
				Iterator<AppraisalPhotoKeys> itr= appraisal.getAppraisalPhotoKeys().iterator();
				while(itr.hasNext()){
					AppraisalPhotoKeys photo= itr.next();
					
					if(resp && photo!=null && photo.getSequenceNo()!=null &&photo.getSequenceNo().equals(Integer.parseInt(request.getParameter("sequenceNumber")))){
						itr.remove();
					}
					
				}
				
				getAppraisalService().save(appraisal);
				
			}
			
			response.setContentType("application/json");
			response.getWriter().write(gson.toJson(resp));
			
		} catch (Exception e) {

			e.printStackTrace();
		}
		
		
		return null;
	}


	
}
