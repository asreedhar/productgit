package com.firstlook.action.dealer.redistribution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ComparatorChain;
import org.apache.commons.collections.comparators.ReverseComparator;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.person.IPerson;
import biz.firstlook.transact.persist.person.PositionType;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.form.ManagePeopleForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;

public class ManagePeopleDisplayAction extends SecureBaseAction {
	
    public ActionForward doIt(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)
            throws DatabaseException, ApplicationException {
        ManagePeopleForm theForm = (ManagePeopleForm) form;
        
        int businessUnitId = getFirstlookSessionFromRequest(request).getCurrentDealerId();
        
        if (request.getParameter("pos") != null) {
            theForm.setPosition(request.getParameter("pos"));
        }
        if (request.getParameter("editMode") != null) {
            theForm.setEditMode(Boolean.parseBoolean(request.getParameter("editMode")));
        }
        if (request.getParameter("sortDir") != null) {
            theForm.setSortDir(request.getParameter("sortDir"));
        }
        if (request.getParameter("sortColumn") != null) {
            theForm.setSortColumn(request.getParameter("sortColumn"));
        }
        
        List<IPerson> people = new ArrayList<IPerson>();
        PositionType type = PositionType.valueFromPositionString(theForm.getPosition());
        if (type != null) {
            people = (List<IPerson>) getPersonService().getBusinessUnitPeopleByPosition(businessUnitId, type);
            response.addHeader("posDesc", type.getPosition());
        }
        
        sortPeople(people, theForm.getSortColumn(), theForm.getSortDir());
        theForm.setPeople(people);
        
        getSelectedRow(people, response);
        
        SessionHelper.keepAttribute(request,"tradeAnalyzerForm");
        SessionHelper.keepAttribute( request, "bookoutDetailForm" );
        SessionHelper.keepAttribute( request, "auctionData" );
        
        
        request.setAttribute("selectedPersonId", request.getParameter("selectedPersonId"));
        
        ActionForward forward;
        
        if (request.getParameter("updateDropDown") != null) {
            forward = mapping.findForward("updateDropDown");
        }
        else{
        	forward = mapping.findForward("success");
        }
        
        return forward;
    }

    @SuppressWarnings("unchecked")
    private void sortPeople(List<IPerson> people, String sortColumn, String sortDir) {
    	formatPeople(people);
    	Comparator primaryComp;
    	Comparator secondaryComp;
    	if (sortDir.equals("desc")) {
    		primaryComp = new ReverseComparator(new BeanComparator(sortColumn, new personComparator()));
    		secondaryComp = new ReverseComparator(new BeanComparator("firstName", new personComparator()));
        } else {
        	primaryComp = new BeanComparator(sortColumn, new personComparator());
        	secondaryComp = new BeanComparator("firstName", new personComparator());
        }
    	Comparator comparator = new ComparatorChain( Arrays.asList( new Comparator[] { primaryComp, secondaryComp } ) );
    	Collections.sort(people, comparator);
    }
    
    class personComparator implements Comparator<String> {
		public int compare(String s0, String s1) {
			
			if (s0 == null || s0.equals("")) {
				if (s1 == null || s1.equals("")) {
					return 0;
				}
				return 1;
			}
			else {
				if (s1 == null || s1.equals("")) {
					return -1;
				}
				return s0.toUpperCase().compareTo(s1.toUpperCase());
			}
		}
    	
    }
    
    
    private void formatPeople(List<IPerson> people){
    	String temp;
    	for (IPerson person:people){
    		if (person.getFirstName() != null && !person.getFirstName().equals("")) {
    			temp = person.getFirstName();
    			person.setFirstName( Character.toUpperCase(temp.charAt(0)) + temp.substring(1, temp.length()));
    		} 
			if (person.getLastName() != null && !person.getLastName().equals("")) {
				temp = person.getLastName();
				person.setLastName( Character.toUpperCase(temp.charAt(0)) + temp.substring(1, temp.length()));
			}
			if (person.getEmail() != null && !person.getEmail().equals("")) {
				temp = person.getEmail();
				person.setEmail( Character.toUpperCase(temp.charAt(0)) + temp.substring(1, temp.length()));
			} 
			if (person.getPhoneNumber() == null ) {
				person.setPhoneNumber("");
			}
    	}
    }
    
    private void getSelectedRow(List<IPerson> people, HttpServletResponse response) {
        Integer i = 1;
        for (IPerson person:people){
            if (person.getSelected()) {
                response.addHeader("selectedRow", person.getPersonId().toString());
            }
            i++;
        }
    }
}
