package com.firstlook.action.dealer;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.retriever.OverPricingRiskCountRetriever;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.dealer.helper.DealerFactsHelper;
import com.firstlook.action.estockcard.NextGenIMTReportDAO;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;

/**
 * This does the tile on the home page.
 * 
 * @author nkeen
 *
 */
public class AgingInventoryDisplayAction extends SecureBaseAction
{

private NextGenIMTReportDAO nextGenIMTReportDAO;
private OverPricingRiskCountRetriever overPricingRiskCountRetriever;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{

	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();

	request.setAttribute( "dealerId", "" + currentDealerId );

	DealerFactsHelper helper = new DealerFactsHelper( getDealerFactsDAO() );
	helper.putSaleMaxPolledDateInRequest( currentDealerId, request );
	helper.putVehicleMaxPolledDateInRequest( currentDealerId, request, InventoryEntity.USED_CAR );

	List<Map<String, Object>> results = getInventoryInformation( currentDealerId );
	
	Map<String, Object> replanningRow = results.get(0);
	request.setAttribute( "dueForReplanning", replanningRow );
	results.remove( replanningRow );
	
	// trim white space from description 
	for (Map<String, Object>row : results)
	{
		row.put( "formattedDescription", row.get( "Description").toString().replaceAll( " ", "" ) );
	}
	
	request.setAttribute( "inventoryBuckets", results );

	// set overpried num
	if (getFirstlookSessionFromRequest( request ).isIncludePingII()) {
		try {
			request.setAttribute( "overpricedNum", overPricingRiskCountRetriever.getOverPricingRiskCount(currentDealerId));
		} catch(DatabaseException de) {
			request.setAttribute("isOverpricedNumError", Boolean.TRUE);
		}
	}

	Member member = getMemberFromRequest(request);
	if ( member.getMemberRoleTester().isUsedAppraiser() || member.getMemberRoleTester().isUsedNoAccess() )
	{
		request.setAttribute( "agingInventoryCenterEnabled", new Boolean( false ) );
	}
	else
	{
		request.setAttribute( "agingInventoryCenterEnabled", new Boolean( true ) );
	}
	return mapping.findForward( "success" );
}

@SuppressWarnings("unchecked")
private List<Map<String, Object>> getInventoryInformation( int dealerId )
{
	StringBuilder sql = new StringBuilder( "Select Units, Description from GetAgingInventoryPlanSummary ( ");
	sql.append( dealerId ).append( ", null, 3, '" );
	sql.append( new Date( new java.util.Date().getTime() ) );
	sql.append( "', 0, 1 ) order by sortOrder ");
	List<Map<String, Object>> results = nextGenIMTReportDAO.getResults( sql.toString(), null );
	
	if ( results.isEmpty() || results.size() == 1 )
	{
		List<Map<String, Object>> fakeResults = new ArrayList< Map<String,Object> >();
		Map<String, Object> fakeResult = new HashMap< String, Object >();
		fakeResult.put( "Units", "N/A" );
		fakeResults.add( fakeResult );
		for (int x=0; x<6; x++)
		{
			fakeResult = new HashMap< String, Object >();
			fakeResult.put( "Description", "-" );
			fakeResult.put( "Units", "-" );
			fakeResults.add( fakeResult );
		}
		return fakeResults;
	}
	return results;
}

public void setNextGenIMTReportDAO( NextGenIMTReportDAO nextGenIMTReportDAO )
{
	this.nextGenIMTReportDAO = nextGenIMTReportDAO;
}

public void setOverPricingRiskCountRetriever(
		OverPricingRiskCountRetriever overPricingRiskCountRetriever) {
	this.overPricingRiskCountRetriever = overPricingRiskCountRetriever;
}

}
