package com.firstlook.action.dealer;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.MessageCenter;
import com.firstlook.exception.ApplicationException;

public class HomepageTextDisplayAction extends SecureBaseAction
{

private HomepageTextDAO homepageTextDao;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	List<MessageCenter> messages = homepageTextDao.getMessages();
    request.setAttribute( "custCenterItems",  messages);
		
	return mapping.findForward( "success" );
}

public HomepageTextDAO getHomepageTextDao() {
	return homepageTextDao;
}

public void setHomepageTextDao(HomepageTextDAO homepageTextDao) {
	this.homepageTextDao = homepageTextDao;
}

}
