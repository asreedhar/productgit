package com.firstlook.action.dealer.cia;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.cia.calculator.CIAGroupingItemDetailCalculator;
import biz.firstlook.cia.model.CIACategory;
import biz.firstlook.cia.model.CIAGroupingItem;
import biz.firstlook.cia.model.CIAGroupingItemDetail;
import biz.firstlook.cia.model.CIAGroupingItemDetailLevel;
import biz.firstlook.cia.model.CIAGroupingItemDetailType;
import biz.firstlook.cia.persistence.ICIASummaryDAO;
import biz.firstlook.transact.persist.model.GroupingDescription;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.persistence.inventory.ROIRetriever;
import com.firstlook.service.cia.ICIAService;
import com.firstlook.service.cia.report.CIAReportingUtility;
import com.firstlook.service.groupingdescription.GroupingDescriptionService;

public class CIAManagersChoiceDisplayAction extends SecureBaseAction
{

private ICIASummaryDAO ciaSummaryDAO;
private ICIAService ciaService;
private ROIRetriever annualRoiDAO;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int businessUnitId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	Dealer currentDealer = getImtDealerService().retrieveDealer( businessUnitId );

	Integer segmentId = new Integer( RequestHelper.getInt( request, "segmentId" ) );
	Integer groupingDescriptionId = new Integer( RequestHelper.getInt( request, "groupingDescriptionId" ) );

	GroupingDescriptionService groupingDescriptionService = new GroupingDescriptionService();
	GroupingDescription groupingDescription = groupingDescriptionService.retrieveById( groupingDescriptionId );

	CIAGroupingItem ciaGroupingItem = getCiaService().getCIAGroupingItemByGroupingDescriptionId( segmentId,
																									CIACategory.MANAGERS_CHOICE,
																									groupingDescription,
																									new Integer( businessUnitId ) );
	List<CIAGroupingItem> groupingItems = new ArrayList<CIAGroupingItem>();
	groupingItems.add( ciaGroupingItem );

	Map preferences = CIAReportingUtility.createPreferencesMapForCIA( groupingItems );

	CIAGroupingItemDetail buyDetail = null;
	List buyDetails = CIAGroupingItemDetailCalculator.filterDetailsByTypeAndLevel( CIAGroupingItemDetailType.BUY_TYPE,
																					CIAGroupingItemDetailLevel.GROUPING_DETAIL_LEVEL,
																					ciaGroupingItem.getCiaGroupingItemDetails() );

	if ( buyDetails != null && !buyDetails.isEmpty() )
	{
		buyDetail = (CIAGroupingItemDetail)buyDetails.get( 0 );
	}

	if ( getImtDealerService().showLithiaRoi( currentDealer.getDealerId().intValue() ) )
	{
		Map annualRoiMap = getAnnualRoiDAO().getMakeModelROI( new Integer( businessUnitId ),
																groupingDescription.getGroupingDescriptionId().intValue() );
		request.setAttribute( "annualRoiMap", annualRoiMap );
		request.setAttribute( "showAnnualRoi", Boolean.TRUE );
	}
	else
	{
		request.setAttribute( "showAnnualRoi", Boolean.FALSE );
	}
	request.setAttribute( "ciaGroupingItemId", ciaGroupingItem.getCiaGroupingItemId() );
	request.setAttribute( "ciaBodyTypeDetailId", ciaGroupingItem.getCiaBodyTypeDetail().getCiaBodyTypeDetailId() );
	request.setAttribute( "buyDetail", buyDetail );
	request.setAttribute( "ciaPlans", preferences );

	request.setAttribute( "weeks", new Integer( 26 ) );
	request.setAttribute( "mileage", new Integer( Integer.MAX_VALUE ) );
	request.setAttribute( "includeDealerGroup", new Integer( 0 ) );
	request.setAttribute( "groupingDescriptionId", groupingDescriptionId );
	request.setAttribute( "groupingDescription", groupingDescription );
	request.setAttribute( "segmentId", segmentId );
	request.setAttribute( "nickname", currentDealer.getNickname() );
	return mapping.findForward( "success" );
}

public ICIASummaryDAO getCiaSummaryDAO()
{
	return ciaSummaryDAO;
}

public void setCiaSummaryDAO( ICIASummaryDAO ciaSummaryDAO )
{
	this.ciaSummaryDAO = ciaSummaryDAO;
}

public ICIAService getCiaService()
{
	return ciaService;
}

public void setCiaService( ICIAService ciaService )
{
	this.ciaService = ciaService;
}

public ROIRetriever getAnnualRoiDAO()
{
	return annualRoiDAO;
}

public void setAnnualRoiDAO( ROIRetriever annualRoiDAO )
{
	this.annualRoiDAO = annualRoiDAO;
}

}