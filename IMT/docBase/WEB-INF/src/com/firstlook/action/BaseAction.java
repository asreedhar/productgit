package com.firstlook.action;

import java.net.InetAddress;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.Globals;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.commons.util.SystemErrorBean;
import biz.firstlook.commons.util.SystemErrorUtil;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Member;
import com.firstlook.entity.form.FaxEmailForm;
import com.firstlook.entity.form.PhoneNumberFormHelper;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;
import com.firstlook.presentation.Perspective;
import com.firstlook.presentation.UserSessionFilter;
import com.firstlook.service.member.IMemberService;
import com.firstlook.session.FirstlookSession;
import com.firstlook.util.HttpParameter;
import com.firstlook.util.PageBreakHelper;

public abstract class BaseAction extends Action
{
private static final Logger logger = Logger.getLogger( BaseAction.class );
private IMemberService memberService;

public abstract ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
throws DatabaseException, ApplicationException;

public ActionForward execute( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
throws Exception
{
	long start = System.currentTimeMillis();
	
	String sessionId = getSessionId( request );

	ActionForward forward = mapping.findForward( "login" );
	final FirstlookSession firstlookSessionFromRequest = getFirstlookSessionFromRequest( request );
	final String halSessionId = (String)request.getSession( false ).getAttribute( UserSessionFilter.HAL_SESSION_ID );
	try
	{
		forward = justDoIt( mapping, form, request, response );
		if ( logger.isDebugEnabled() )
		{
			if ( firstlookSessionFromRequest != null && firstlookSessionFromRequest.getMember() != null )
			{
				SystemErrorBean systemError = new SystemErrorBean(
						null,
						firstlookSessionFromRequest.getCurrentDealerId() == 0 ? null : new Integer(firstlookSessionFromRequest.getCurrentDealerId()),
						firstlookSessionFromRequest.getMember().getMemberId(),
						firstlookSessionFromRequest.getMode(),
						sessionId + " on " + InetAddress.getLocalHost(),
						halSessionId != null ? halSessionId : null,
						request.getRequestURI() );
				logger.debug( systemError.getContext() );
			}
		}
	}
	catch ( Throwable th )
	{
		// log error to the DB SytemError table
		
		SystemErrorBean systemError = null;
		if ( firstlookSessionFromRequest != null && firstlookSessionFromRequest.getMember() != null )
		{
			systemError = new SystemErrorBean(th, 
					firstlookSessionFromRequest.getCurrentDealerId() == 0 ? null : new Integer(firstlookSessionFromRequest.getCurrentDealerId()),
					firstlookSessionFromRequest.getMember().getMemberId(),
					firstlookSessionFromRequest.getMode(),
					sessionId + " on " + InetAddress.getLocalHost(),
					request.getSession( false ).getAttribute( UserSessionFilter.HAL_SESSION_ID ) != null ? request.getSession( false ).getAttribute( UserSessionFilter.HAL_SESSION_ID ).toString() : null,
					request.getRequestURI() );
		}
		else
		{
			systemError = new SystemErrorBean(
					th, null, null, null, sessionId + " on " + InetAddress.getLocalHost(),
			        request.getSession( false ).getAttribute( UserSessionFilter.HAL_SESSION_ID ) + "",
			        request.getRequestURI() );
		}
		
		Integer systemErrorId = new Integer(0);
		try {
			// save the exception in the fault database by making the call to the web service
			systemErrorId = SystemErrorUtil.save(systemError, request);
		}
		catch (Throwable t) {
			// ignore
		}
		
		request.setAttribute( "SystemErrorId", "FL" + systemErrorId );
		request.setAttribute( "UncaughtException", th );
		logger.error( systemError.toString() );
		response.setStatus(500);
		forward = mapping.findForward( "error" );
	}
	
	request.setAttribute( "actionPath", request.getServletPath().substring( 1 ) );
	if ( request.getSession( false ) != null )
		SessionHelper.cleanup( request );
	addNoCacheHeader( response );
	
	long total = System.currentTimeMillis() - start;
	
	logger.info("<!-------  " + this.getClass().getName() + "  TOOK :::  " + total);
	
	return forward;
}


void addNoCacheHeader( HttpServletResponse response )
{
	response.addHeader( "Cache-Control", "no-cache" );
	response.addHeader( "Pragma", "no-cache" );
}

protected ActionForward addParameterToForward( ActionForward forward, HttpParameter parameter )
{
	ActionForward returnForward = new ActionForward();
	returnForward.setName( forward.getName() );
	returnForward.setPath( forward.getPath() );
	returnForward.setRedirect( forward.getRedirect() );

	String urlPath = returnForward.getPath();

	if ( urlPath.indexOf( "?" ) < 0 )
	{
		urlPath = urlPath + "?" + parameter.getURL();
	}
	else
	{
		urlPath = urlPath + "&" + parameter.getURL();
	}

	returnForward.setPath( urlPath );

	return returnForward;
}

protected ActionForward getDestinationActionForward( ActionMapping mapping, HttpServletRequest request ) throws ApplicationException
{
	String destination = request.getParameter( "destination" );
	ActionForward forward;

	if ( destination == null )
	{
		throw new ApplicationException( "destination parameter is null" );
	}

	forward = mapping.findForward( destination );
	if ( forward == null )
	{
		throw new ApplicationException( "destination parameter is not valid" );
	}

	return forward;
}

@SuppressWarnings("unchecked")
public boolean hasValidationError( HttpServletRequest request, String errorKey )
{
	ActionErrors errors = (ActionErrors)request.getAttribute( Globals.ERROR_KEY );

	if ( errors != null )
	{
		Iterator iterator = errors.get( ActionErrors.GLOBAL_ERROR );

		while ( iterator.hasNext() )
		{
			ActionError error = (ActionError)iterator.next();
			if ( error.getKey().equals( errorKey ) )
			{
				return true;
			}
		}
	}
	return false;
}

public boolean hasValidationErrors( HttpServletRequest request )
{
	ActionErrors errors = (ActionErrors)request.getAttribute( Globals.ERROR_KEY );

	return ( ( errors != null ) && ( errors.size() > 0 ) );
}

public void putActionErrorInRequest( HttpServletRequest request, String errorKey )
{
	ActionErrors errors = new ActionErrors();
	errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( errorKey ) );
	request.setAttribute( Globals.ERROR_KEY, errors );
}

// TODO: Struts 1.1 made this method obsolete. We don't need this in BaseAction
// - TO 12/24/2003
@SuppressWarnings("unchecked")
public static void putActionErrorsInRequest( HttpServletRequest request, ActionErrors errors )
{
	if ( errors != null && !errors.isEmpty() )
	{
		Iterator iterator = errors.get();
		Collection<String> collection = new ArrayList<String>();
		while ( iterator.hasNext() )
		{
			// TODO: This message bundle is *already* available to us as the
			// default Struts message bundle, this code is not needed - TO
			// 12/24/2003
			ActionError error = (ActionError)iterator.next();
			String value = PropertyLoader.getProperty( error.getKey() );
			Object[] values = error.getValues();

			MessageFormat messageFormat = new MessageFormat( value );
			collection.add( messageFormat.format( values ) );
		}
		request.setAttribute( "errors", collection );
		request.setAttribute( Globals.ERROR_KEY, errors );
	}
}

protected void putFaxEmailFormInRequest( HttpServletRequest request ) throws ApplicationException
{
	String faxNumber = request.getParameter( "faxNumber" );
	String faxToName = request.getParameter( "faxToName" );
	if ( ( faxNumber != null ) && ( faxToName != null ) )
	{
		FaxEmailForm form = new FaxEmailForm();
		PhoneNumberFormHelper helper = new PhoneNumberFormHelper( faxNumber );
		form.setFaxToName( faxToName );
		form.setFaxNumber( helper );
		request.setAttribute( "faxHeader", form );
	}
}

public void putPageBreakHelperInRequest( String pageType, HttpServletRequest request, String programType )
{
	PageBreakHelper pageBreakHelper = new PageBreakHelper( pageType, programType );
	request.setAttribute( "pageBreakHelper", pageBreakHelper );
}

/**
 * This method can be refactored into the page that can display optimix mode.
 * 
 * @deprecated
 */
protected Perspective retrievePerspective( HttpServletRequest request )
{
	Perspective perspective = (Perspective)request.getSession().getAttribute( "perspective" );

	return perspective;
}

protected Member getMemberFromRequest( HttpServletRequest request )
{
	if ( getFirstlookSessionFromRequest( request ) != null )
		return getFirstlookSessionFromRequest( request ).getMember();
	else
		return null;
}

protected FirstlookSession getFirstlookSessionFromRequest( HttpServletRequest request )
{
	if ( request.getSession( false ) == null )
		return null;

	return (FirstlookSession)request.getSession( false ).getAttribute( FirstlookSession.FIRSTLOOK_SESSION );
}

protected String getSessionId( HttpServletRequest request )
{
	HttpSession httpSession = request.getSession( false );
	if ( httpSession == null )
	{
		logger.error( "No http session!" );
		return null;
	}
	else
	{
		return httpSession.getId();
	}
}

public IMemberService getMemberService()
{
	return memberService;
}

public void setMemberService( IMemberService memberService )
{
	this.memberService = memberService;
}

}
