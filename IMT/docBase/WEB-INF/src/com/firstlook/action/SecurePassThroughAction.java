package com.firstlook.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;

public class SecurePassThroughAction extends SecureBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{ 
    putDealerFormInRequest( request );

    StringBuffer forward = new StringBuffer();
    
    if ( request.getParameter( "mappingForward" ) != null )
    {
    	forward.append( request.getParameter( "mappingForward" ) );
    }
    else
    {
        forward.append( "success" );
    }
    
	return mapping.findForward( forward.toString() );
}

}
