package com.firstlook.action.estockcard;

import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.exception.SelectedThirdPartyVehicleNotFoundException;
import biz.firstlook.transact.persist.model.BookOut;
import biz.firstlook.transact.persist.model.BookOutValueType;
import biz.firstlook.transact.persist.model.DealerPreferenceWindowSticker;
import biz.firstlook.transact.persist.model.InventoryWithBookOut;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyOption;
import biz.firstlook.transact.persist.model.ThirdPartyVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.model.WindowStickerTemplate;
import biz.firstlook.transact.persist.persistence.IDealerPreferenceDAO;
import biz.firstlook.transact.persist.persistence.InventoryBookOutDAO;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;

public class PrintWindowStickerAction extends SecureBaseAction {

	private InventoryBookOutDAO inventoryBookOutDAO;
	private IDealerPreferenceDAO dealerPrefDAO;
	
	@Override
	public ActionForward doIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws DatabaseException, ApplicationException {
		
		final int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
		Dealer dealer = getDealerDAO().findByPk(currentDealerId);
		request.setAttribute("dealerState", dealer.getState());
		DealerPreferenceWindowSticker stickerPref = dealerPrefDAO.findWindowStickerPrefs(currentDealerId);
		
		Boolean isBuyingGuide = Boolean.valueOf(request.getParameter("buyersGuide"));
		request.setAttribute("isBuyingGuide", isBuyingGuide);
		request.setAttribute("buyingGuideFormat", stickerPref.getBuyersGuideTemplate().getFormatName());
		
		Integer inventoryId = null;
		try {
			inventoryId = Integer.valueOf(request.getParameter("inventoryId"));
		} catch (NumberFormatException e) {
			throw new ApplicationException("Expected an integer inventoryId");
		}
		
		InventoryWithBookOut inventoryWithBookOut = inventoryBookOutDAO.findByPk(inventoryId);
		request.setAttribute("inventory", inventoryWithBookOut);
		request.setAttribute("useLotPrice", dealer.getDealerPreference().getUseLotPrice());
		
		if(!isBuyingGuide) {
			Integer windowStickerTemplateId = null;
			try {
				windowStickerTemplateId = Integer.valueOf((request.getParameter("windowStickerTemplate")));
			} catch (NumberFormatException e) {
				throw new ApplicationException("Expected an integer windowStickerTemplate id");
			}
			WindowStickerTemplate template = WindowStickerTemplate.getById(windowStickerTemplateId);
			switch(template) {
				case LITHIAVALUEAUTO:
				case MIDDLEKAUFF:
				case TUTTLE:
				case KBB:
				case KBB2009:
				case THOROUGHBRED:
				case SANTAN:
				case PARKS:
				case KBBSANRETAIL:
					setKBBOptions(request, inventoryWithBookOut);
					if (dealer.getGuideBookId() != ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE) // this check is done for backwards compatibility, PM
						setNADAOptions(request, inventoryWithBookOut);
					request.setAttribute("format", template.getFormat());
					break;
				case LITHIA:
					//do nothing, handled outside of isBuyingGuide.
					break;
				default:
					break;
			}
		}
		request.setAttribute( "nickname", dealer.getNickname());
		String forward = "success";
		final Boolean isLithia = Boolean.valueOf(request.getParameter("isLithia"));
		if (isLithia.booleanValue()) {
			forward="lithiaSuccess";
		}
		
		return mapping.findForward(forward);
	}

	/**
	 * Find the KBB bookout on the inventory item and sets its thirdpartyVehicleOptions
	 * on the request.
	 * 
	 * Nothing is set if a KBB Bookout or corresponding ThirdPartyVehicle/Options are not present.
	 * 
	 * @param request
	 * @param inventoryWithBookOut
	 */
	private void setKBBOptions(HttpServletRequest request,
			InventoryWithBookOut inventoryWithBookOut) {
		BookOut bookout = inventoryWithBookOut.getLatestBookOut(ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE);
		if (bookout != null) {
			if (bookout.getThirdPartyDataProvider().getThirdPartyId().intValue() == ThirdPartyDataProvider.KELLEY.getThirdPartyId().intValue()) {
					setOptions(request, bookout);
					request.setAttribute("kbbSuggestedRetailPrice", bookout.getThirdPartyCategoryValue(ThirdPartyCategory.KELLEY_RETAIL_TYPE, BookOutValueType.BOOK_OUT_VALUE_FINAL_VALUE));
					request.setAttribute("bookDate", bookout.getDatePublished());
					request.setAttribute("currentYear", Calendar.getInstance().get(Calendar.YEAR));
			}
		}
	}	
	
	/**
	 * Find the NADA bookout on the inventory item and sets its thirdpartyVehicleOptions
	 * on the request.
	 * 
	 * Nothing is set if a NADA Bookout or corresponding ThirdPartyVehicle/Options are not present.
	 * 
	 * @param request
	 * @param inventoryWithBookOut
	 */
	private void setNADAOptions(HttpServletRequest request,
			InventoryWithBookOut inventoryWithBookOut) {
		BookOut bookout = inventoryWithBookOut.getLatestBookOut(ThirdPartyDataProvider.GUIDEBOOK_NADA_TYPE);
		if (bookout != null) {
			if (bookout.getThirdPartyDataProvider().getThirdPartyId().intValue() == ThirdPartyDataProvider.NADA.getThirdPartyId().intValue()) {
					setOptions(request, bookout);
			}
		}
	}
	
	/**
	 * @param request
	 * @param bookout
	 */
	private void setOptions(HttpServletRequest request, BookOut bookout) {
		ThirdPartyVehicle thirdPartyVehicle;
		try {
			thirdPartyVehicle = bookout.getSelectedThirdPartyVehicle();
		} catch (SelectedThirdPartyVehicleNotFoundException e) {
			// nk - this is a hack until we can figure out why BOs are being written with ZERO TPVs
			return;
			// nk - end hack
		}
		
		ArrayList<ThirdPartyOption> selectedOptions = new ArrayList<ThirdPartyOption>();
		for (ThirdPartyVehicleOption option : thirdPartyVehicle.getThirdPartyVehicleOptions()) {
			if (option.isStatus()) {
				selectedOptions.add(option.getOption());
			}
		}

		request.setAttribute("options", selectedOptions);
	}

	public void setInventoryBookOutDAO(InventoryBookOutDAO inventoryBookOutDAO) {
		this.inventoryBookOutDAO = inventoryBookOutDAO;
	}

	public void setDealerPrefDAO(IDealerPreferenceDAO dealerPreferenceDAO) {
		this.dealerPrefDAO = dealerPreferenceDAO;
	}



}
