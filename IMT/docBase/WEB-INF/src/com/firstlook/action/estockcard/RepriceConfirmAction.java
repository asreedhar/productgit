package com.firstlook.action.estockcard;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.service.inventory.InventoryService_Legacy;

public class RepriceConfirmAction extends SecureBaseAction
{

private NextGenIMTReportDAO nextGenIMTReportDAO;
private InventoryService_Legacy inventoryService_Legacy;

@Override
public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int newPrice = RequestHelper.getInt( request, "price" );
	int inventoryId = RequestHelper.getInt( request, "inventoryId" );
	int businessUnitId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();

	Boolean doConfirmation = doConfirmation( businessUnitId );
	
	if ( doConfirmation)
	{
		Integer priceChangeThreshold = getPriceChangeThreshold( businessUnitId );
		Boolean repriceConfirmationNeeded = inventoryService_Legacy.isRepriceConfirmationNeeded( inventoryId, newPrice, priceChangeThreshold);
	
		response.addHeader( "confirm", repriceConfirmationNeeded.toString() );
		request.setAttribute( "percentChangeThreshold", priceChangeThreshold );
	}
	else
	{
		response.addHeader( "confirm", Boolean.FALSE.toString() );
	}
		return mapping.findForward( "success" );
}

@SuppressWarnings("unchecked")
private Integer getPriceChangeThreshold( int businessUnitId )
{
	StringBuilder sb = new StringBuilder("select RepricePercentChangeThreshold from DealerPreference");
	sb.append(" where businessUnitID = " + businessUnitId );
	List<Map<String, Object>> results = nextGenIMTReportDAO.getResults( sb.toString(), null );
	
	// null check that just returns the default
	if (results.isEmpty() || results.get( 0 ).get( "RepricePercentChangeThreshold" ) == null )
	{
		return new Integer(10);
	}
	return (Integer)results.get( 0 ).get( "RepricePercentChangeThreshold" );
}


@SuppressWarnings("unchecked")
public Boolean doConfirmation( int businessUnitId )
{
	StringBuilder sb = new StringBuilder(" select	temp.advertisingOutlets, dp.RepriceConfirmation ");
	sb.append( "from (select count(*) as advertisingOutlets from internetAdvertiserDealership ad ");
	sb.append( " where	ad.businessunitid = " + businessUnitId + " ) as temp,");
	sb.append( "dealerPreference dp where	dp.businessunitid = "+ businessUnitId);
	
	Map<String, Object> results = (Map<String, Object>)nextGenIMTReportDAO.getResults( sb.toString(), null ).get(0);
	
	return ( ((Integer)results.get("advertisingOutlets") > 0) ||
		    ((Integer)results.get( "RepriceConfirmation") == 1 ) );
		  
}

public void setInventoryService_Legacy( InventoryService_Legacy inventoryService )
{
	this.inventoryService_Legacy = inventoryService;
}

public void setNextGenIMTReportDAO( NextGenIMTReportDAO nextGenIMTReportDAO )
{
	this.nextGenIMTReportDAO = nextGenIMTReportDAO;
}

}
