package com.firstlook.action.estockcard;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.services.insights.Insight;
import biz.firstlook.commons.services.insights.InsightParameters;
import biz.firstlook.commons.services.insights.InsightUtils;
import biz.firstlook.commons.services.insights.PerformanceAnalysisBean;
import biz.firstlook.commons.services.vehicleCatalog.IVehicleCatalogService;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogEntry;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogServiceException;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.components.PerformanceSummaryHelperService;
import com.firstlook.action.components.vehicledetailpage.VehicleInventory;
import com.firstlook.action.redirection.JdPowerRedirectionAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.form.TradeAnalyzerForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.SessionHelper;
import com.firstlook.helper.action.ReportActionHelper;
import com.firstlook.service.inventory.InventoryService_Legacy;
import com.firstlook.service.inventory.VehicleInventoryService;

public class VehiclePerformanceSummaryTileAction extends SecureBaseAction
{
private static final Logger logger = Logger.getLogger( VehiclePerformanceSummaryTileAction.class );

	private PerformanceSummaryHelperService performanceSummaryHelperService;
	private VehicleInventoryService vehicleInventoryService;
	private NextGenFLDWReportDAO nextGenFLDWReportDAO;
	private InventoryService_Legacy inventoryService_Legacy;
	protected IVehicleCatalogService vehicleCatalogService;
	
public VehiclePerformanceSummaryTileAction()
{
	super();
}

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response ) throws DatabaseException, ApplicationException
{
	
	int inventoryId = RequestHelper.getInt( request, "inventoryId");
	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	int includeDealerGroup = 0;
	try
	{
		includeDealerGroup = RequestHelper.getInt( request, "includeDealerGroup");
	}
	catch( Exception e )
	{
		includeDealerGroup = 0;
	}
	
	// this has been stubbed out and everything is based of the inventoryID
	// this was done in an effort to enable this tile to be on the new eStockCard -DW 4/11/06
	TradeAnalyzerForm tradeAnalyzerForm = new TradeAnalyzerForm();
	
	VehicleInventory vehicleInventory = vehicleInventoryService.retrieveVehicleInventory( currentDealerId, new Integer( inventoryId) );
	
	tradeAnalyzerForm.setVin( vehicleInventory.getVehicle().getVin() );
	tradeAnalyzerForm.setCatalogKeyId( vehicleInventory.getVehicle().getVehicleCatalogID() );
	tradeAnalyzerForm.setMake( vehicleInventory.getVehicle().getMakeModelGrouping().getMake() );
	tradeAnalyzerForm.setModel( vehicleInventory.getVehicle().getMakeModelGrouping().getModel() );
	tradeAnalyzerForm.setTrim( vehicleInventory.getVehicle().getVehicleTrim() ); // this is for DB
	tradeAnalyzerForm.setTrim( vehicleInventory.getVehicle().getVehicleTrim() ); // this is for front end
	tradeAnalyzerForm.setMakeModelGroupingId( vehicleInventory.getVehicle().getMakeModelGrouping().getMakeModelGroupingId());
	tradeAnalyzerForm.setGroupingDescriptionId(vehicleInventory.getVehicle().getMakeModelGrouping().getGroupingDescriptionId().intValue());
	tradeAnalyzerForm.setColor( vehicleInventory.getVehicle().getBaseColor() );
	
	tradeAnalyzerForm.setIncludeDealerGroup( includeDealerGroup );
	
	putDealerFormInRequest( request, 0 );
	
	int weeks = RequestHelper.getInt( request, "weeks" );
	boolean isIncludeMarket = getFirstlookSessionFromRequest( request ).isIncludeMarket();

	performanceSummaryHelperService.determineMarketShare( isIncludeMarket, tradeAnalyzerForm.getVin(), request, tradeAnalyzerForm.getGroupingDescriptionId(),
							new Integer( currentDealerId ) );

	performanceSummaryHelperService.putGroupingDescriptionFormInRequest( request, new Integer( tradeAnalyzerForm.getGroupingDescriptionId() ) );
	
	
	performanceSummaryHelperService.setReportGroupingsOnRequest( request, tradeAnalyzerForm, weeks );

	Map nextGenInfo = useNextGenAggregatesToGetInfo( tradeAnalyzerForm.getGroupingDescriptionId(), currentDealerId );

	getInsights( request, currentDealerId, vehicleInventory.getVehicle().getVehicleYear(), vehicleInventory.getInventory().getMileageReceived(), tradeAnalyzerForm );
	
	request.setAttribute( "daysSupply", (Integer)nextGenInfo.get("DaysSupply") );
	request.setAttribute( "avgInventoryAge", (Integer)nextGenInfo.get("AvgInventoryAge") );
	request.setAttribute( "inventoryId", new Integer ( inventoryId ) );
	request.setAttribute( "weeks", new Integer( weeks ) );
	request.setAttribute( "mileageMax", new Integer( Integer.MAX_VALUE ) );
	request.setAttribute( "tradeAnalyzerForm", tradeAnalyzerForm );
	SessionHelper.setAttribute( request, "tradeAnalyzerForm", tradeAnalyzerForm );
	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );

	try
	{
		VehicleCatalogEntry vce = vehicleCatalogService.retrieveVehicleCatalogEntry( tradeAnalyzerForm.getVin(), tradeAnalyzerForm.getCatalogKeyId() );
		
		request.setAttribute("showJDPower", getFirstlookSessionFromRequest(request).isIncludeJDPowerUsedCarMarketData());
		request.setAttribute("JDPowerUrl", JdPowerRedirectionAction.getRelativeUrl( vce.getCatalogKey().getVehicleCatalogId(), vce.getModelYear()) );
	}
	catch ( VehicleCatalogServiceException e )
	{
		logger.warn( MessageFormat.format( "Could not find vehicle catalogid for vin: {0}, disabling JDPowerMarketAnalyzer.", tradeAnalyzerForm.getVin()));
	}
	//return mapping.findForward( adjustForward("success", request) );
	return mapping.findForward( "success");
}

private void getInsights( HttpServletRequest request, int currentDealerId, Integer year, Integer mileage, TradeAnalyzerForm tradeAnalyzerForm ) throws ApplicationException
{
	int inventoryType = getMemberFromRequest( request ).getUserRoleEnum().getValue();

	InventoryEntity inventory = inventoryService_Legacy.createEmptyInventoryWithYearMileageMakeModel( year.toString(), mileage.intValue(), tradeAnalyzerForm.getMake(),
																								tradeAnalyzerForm.getModel() );
	
	List<Insight> insights = InsightUtils.getInsights(new InsightParameters(
			currentDealerId,
			inventory.getGroupingDescriptionId(),
			inventoryType,
			ReportActionHelper.DEFAULT_NUMBER_OF_WEEKS,
			tradeAnalyzerForm.getTrim(),
			year,
			mileage));
	
	request.setAttribute( "performanceAnalysisItem", new PerformanceAnalysisBean(insights) );
}

private Map useNextGenAggregatesToGetInfo( int groupingId, int currentDealerId )
{
	// 4 = twenty six weeks
	String sql = "select * from inventorySales_A2 where businessUnitID = " + currentDealerId
					+ " and periodID = 4 and vehicleGroupingID = " + groupingId;
	List results = nextGenFLDWReportDAO.getResults( sql, null );
	if ( results == null || results.isEmpty() )
	{
		return new HashMap();
	}
	return (Map)results.get( 0 );
}

public void setPerformanceSummaryHelperService( PerformanceSummaryHelperService performanceSummaryHelperService )
{
	this.performanceSummaryHelperService = performanceSummaryHelperService;
}

public void setVehicleInventoryService( VehicleInventoryService vehicleInventoryService )
{
	this.vehicleInventoryService = vehicleInventoryService;
}

public void setNextGenFLDWReportDAO( NextGenFLDWReportDAO nextGenReportDAO )
{
	this.nextGenFLDWReportDAO = nextGenReportDAO;
}

public void setInventoryService_Legacy( InventoryService_Legacy inventoryService )
{
	this.inventoryService_Legacy = inventoryService;
}

public IVehicleCatalogService getVehicleCatalogService()
{
	return vehicleCatalogService;
}

public void setVehicleCatalogService( IVehicleCatalogService vehicleCatalogService )
{
	this.vehicleCatalogService = vehicleCatalogService;
}



}
