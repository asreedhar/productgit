package com.firstlook.action.estockcard;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogKey;
import biz.firstlook.commons.util.Functions;
import biz.firstlook.transact.persist.model.Inventory;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.persistence.InventoryDAO;

import com.discursive.cas.extend.client.CASFilter;
import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.components.PerformanceSummaryHelperService;
import com.firstlook.action.components.vehicledetailpage.VehicleInventory;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.form.TradeAnalyzerForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.SessionHelper;
import com.firstlook.service.inventory.VehicleInventoryService;

public class SaveVehicleInformationAction extends SecureBaseAction
{

protected final Logger log = Logger.getLogger( getClass() );
private static final List< String > doubleFormatPatterns = new ArrayList< String >();

static
{
	doubleFormatPatterns.add( "$#,##0.00" );
	doubleFormatPatterns.add( "#,##0.00" );
}

private InventoryInformationService inventoryInformationService;
private InventoryDAO inventoryDAO;
private VehicleInventoryService vehicleInventoryService;
private PerformanceSummaryHelperService performanceSummaryHelperService;

public ActionForward doIt( ActionMapping mapping, ActionForm inForm, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	int dealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	int weeks = RequestHelper.getInt( request, "weeks" );
	int includeDealerGroup = 0;
	int certificationNumber= 0;
	try
	{
		includeDealerGroup = RequestHelper.getInt( request, "includeDealerGroup" );
	}
	catch ( Exception e )
	{
		includeDealerGroup = 0;
	}

	try
	{
		boolean mileageChanged = RequestHelper.getBoolean( request, "mileageChanged" );
		boolean listPriceChanged = RequestHelper.getBoolean( request, "listPriceChanged" );
		String repriceConfirm = request.getParameter( "repriceConfirm" );
		boolean catalogKeyUpdated = Boolean.parseBoolean( RequestHelper.getString( request, "updatedCatalogKey" ) );
		boolean transferPriceChanged = Boolean.valueOf(request.getParameter("transferPriceChanged")).booleanValue();
		String login = (String)request.getSession().getAttribute( CASFilter.CAS_FILTER_USER );

		VehicleInventory vehicleInventoryFromPage = bindRequestVariables( request, dealerId, catalogKeyUpdated );
		VehicleInventory updatedVehicleInventory = inventoryInformationService.updateInventoryAndVehicle( vehicleInventoryFromPage,
																											mileageChanged, listPriceChanged,
																											catalogKeyUpdated, login,
																											repriceConfirm,transferPriceChanged );

		// inputFromPage.put( "EdmundsTMVChanged", RequestHelper.getBoolean( request, "edmundsTMVChanged" ) );
		
		request.setAttribute("selectedCertificate2", getInventoryInformationService().getCertificates(dealerId, vehicleInventoryFromPage.getInventory().getInventoryId()).get(request.getParameter("certificate")));
		
		Map<String,String> dropdownList = new HashMap<String,String>();
		dropdownList = inventoryDAO.fetchCertificates(dealerId, vehicleInventoryFromPage.getInventory().getInventoryId());
		int count = 0;
		String cert = "";
		String certKey = "";
		
		for (Map.Entry<String, String> entry : dropdownList.entrySet())
		{
			if(entry.getKey().contains("MFR")){
			count++;
			cert = entry.getValue();
			certKey = entry.getKey();
					
			}
			
		}
		
		if(count == 1 && request.getParameter("certificate").contains("CERT")){
			request.setAttribute("selectedCertificate2", cert );
		}
		
		if (request.getParameter("certifiedId") != null && request.getParameter("certifiedId") != "" && request.getParameter("certifiedId").length() > 0 ){
			certificationNumber =  Integer.parseInt(request.getParameter("certifiedId"));
		}
		
		String make =  updatedVehicleInventory.getVehicle().getMakeModelGrouping().getMake();
		request.setAttribute("make", make);
		
		TradeAnalyzerForm tradeAnalyzerForm = new TradeAnalyzerForm();
		tradeAnalyzerForm.setMake( updatedVehicleInventory.getVehicle().getMakeModelGrouping().getMake() );
		tradeAnalyzerForm.setModel( updatedVehicleInventory.getVehicle().getMakeModelGrouping().getModel() );
		tradeAnalyzerForm.setTrim( updatedVehicleInventory.getVehicle().getVehicleTrim() );
		tradeAnalyzerForm.setMakeModelGroupingId( updatedVehicleInventory.getVehicle().getMakeModelGrouping().getMakeModelGroupingId() );
		tradeAnalyzerForm.setGroupingDescriptionId(updatedVehicleInventory.getVehicle().getMakeModelGrouping().getGroupingDescriptionId().intValue());
		tradeAnalyzerForm.setIncludeDealerGroup( includeDealerGroup );
		getPerformanceSummaryHelperService().setReportGroupingsOnRequestSpecific( request, tradeAnalyzerForm, weeks );
		 Integer FLAG =0;
		String certificateValue = request.getParameter("certificate")!=null?request.getParameter("certificate").substring(0,request.getParameter("certificate").indexOf("|")):"";
		if(request.getParameter("certificate") != null && request.getParameter("certificate").contains("MFR")){
			FLAG = 1;
		}
			
		if(request.getParameter("certificate") != null && request.getParameter("certificate").contains("GRP")){
			FLAG=2;
			certificationNumber = 0;
		}
		
		if(request.getParameter("certificate") != null && request.getParameter("certificate").contains("CERT")){
			FLAG=3;
			certificationNumber = 0;
			if(count == 1 && request.getParameter("certificate").contains("CERT")){
				request.setAttribute( "selectedValue", cert);
				
/*			certificateValue = certKey !=null?certKey.substring(0,certKey.indexOf("|")):"";*/

			}

			
		}
		if(request.getParameter("certificate") != null && request.getParameter("certificate").contains("NOT")){
			FLAG = 4;
			certificationNumber =0;
		}
		request.setAttribute( "currentVehicleLight", updatedVehicleInventory.getInventory().getCurrentVehicleLight() );
		request.setAttribute( "tradeAnalyzerForm", tradeAnalyzerForm );
		request.setAttribute( "mileageChanged", mileageChanged );
		/*Integer certifiedId = 000 ;*/
		java.util.Date utilDate = new java.util.Date();
		java.sql.Date certifiedProgramAutoSavedDate = new java.sql.Date(utilDate.getTime());
		int certifiedProgramId = Integer.parseInt(certificateValue);
		/* inventoryDAO.saveCertificates(vehicleInventoryFromPage.getInventory().getInventoryId(),certifiedId,login,certifiedProgramId,certifiedProgramAutoSavedDate,FLAG); */
		inventoryDAO.saveCertificates(vehicleInventoryFromPage.getInventory().getInventoryId(),certificationNumber,login,certifiedProgramId,FLAG);
		
	
	}
	catch ( Exception e )
	{
		// didn't save so display error text instead of success text
		log.debug( "didn't save inventory info via ajax", e );
		return mapping.findForward( "failure" );
	}

	SessionHelper.keepAttribute( request, "bookoutDetailForm" );

	request.setAttribute( "weeks", weeks );
	request.setAttribute( "success", Boolean.TRUE );
	return mapping.findForward( "success" );
}

@SuppressWarnings( "unchecked" )
private VehicleInventory bindRequestVariables( HttpServletRequest request, int dealerId, boolean catalogKeyUpdated ) throws ApplicationException
{
	Inventory inventoryFromPage = new Inventory();
	inventoryFromPage.setDealerId( dealerId );
	inventoryFromPage.setInventoryId( getAsInt((String)request.getParameter("inventoryId" )));
	inventoryFromPage.setSpecialFinance( Boolean.parseBoolean( (request.getParameter("specialfinance") != null) ? (String)request.getParameter("specialfinance") : null) );
	inventoryFromPage.setLotLocation( (request.getParameter("lotlocation") != null) ? (String)request.getParameter("lotlocation") : null) ;
	inventoryFromPage.setListPrice( getAsDouble((String)request.getParameter("list" )) );
	inventoryFromPage.setMileageReceived( getMileage( (request.getParameter("mileage") != null) ? (String)request.getParameter("mileage") : null) );
	/* inventoryFromPage.setCertified( Boolean.parseBoolean( (request.getParameter("certified") != null) ? (String)request.getParameter("certified") : null) ); */
	inventoryFromPage.setEdmundsTMV( getAsDouble((String)request.getParameter("edmundsTMV" )));
	inventoryFromPage.setLotPrice( getAsInt((String)request.getParameter("lotPrice" )));
	
	Inventory invState = inventoryDAO.findBy(inventoryFromPage.getInventoryId());
	//this line must happen before setRetailOnlyTransfer
	
	//Check Lot Location coming from screen and Location in DB , if location in db is null and location from screen is blank that means no change is done
	if(inventoryFromPage.getLotLocation()!=null && inventoryFromPage.getLotLocation().equals("") && invState.getLotLocation()==null)
		inventoryFromPage.setLotLocation(null);
	inventoryFromPage.setLotLocationLock(invState.getLotLocationLock());
	if(inventoryFromPage.getLotLocation()!=null && !inventoryFromPage.getLotLocation().equals(invState.getLotLocation()))
		inventoryFromPage.setLotLocationLock(1);
	
	inventoryFromPage.setTransferForRetailOnlyEnabled(invState.isTransferForRetailOnlyEnabled());
	
	inventoryFromPage.setTransferPrice(Functions.nullSafeFloat(request.getParameter("transferPrice")));
	boolean transferForRetailOnly = request.getParameter("transferForRetailOnly") == null ? false : true;
	inventoryFromPage.setRetailOnlyTransfer(transferForRetailOnly);
	
	Vehicle vehicleFromPage = new Vehicle();
	vehicleFromPage.setBaseColor( (request.getParameter("BaseColor") != null) ? request.getParameter("BaseColor") : null );
	vehicleFromPage.setInteriorColor((request.getParameter("intcolor") != null) ? request.getParameter("intcolor") : null );
	vehicleFromPage.setInteriorDescription( (request.getParameter("InteriorDescription") != null) ? request.getParameter("InteriorDescription") : null  );

	if ( catalogKeyUpdated ) // styleKey has been changed
	{
		vehicleFromPage.setVehicleCatalogKey(new VehicleCatalogKey(getAsInt(request.getParameter("catalogKeyId" )),  null));
	}

	VehicleInventory vi = new VehicleInventory( inventoryFromPage, vehicleFromPage );
	return vi;
}

/**
 * Simple util method for casting the string to an Integer
 * Also performs basic null checks.
 * Replaces dangerous use of old RequestHelper.getInt
 * 
 * @param intString
 * @return
 */
private Integer getAsInt(String intString) {
	if (intString == null || intString.trim().length() == 0) {
		return null;
	} else {
		return new Integer(intString);
	}
}
/**
 * Simple util method for casting the string to an Integer
 * Also performs basic null checks.
 * Replaces dangerous use of old RequestHelper.getInt
 * 
 * @param intString
 * @return
 */
private Double getAsDouble(String doubleString) {
	if (doubleString == null || doubleString.trim().length() == 0) {
		return null;
	} else {
		return new Double(doubleString);
	}
}
private Integer getMileage( String mileageInput ) throws ApplicationException
{
	DecimalFormat df = new DecimalFormat();
	df.applyPattern( "#,##0" );
	Integer mileage = null;
	try
	{
		mileage = new Integer( df.parse( mileageInput ).intValue() );
	}
	catch ( ParseException e )
	{
		throw new ApplicationException( "couldn't parse mileage string " + mileageInput + " into an Integer Object" );
	}
	return mileage;
}

public InventoryInformationService getInventoryInformationService()
{
	return inventoryInformationService;
}

public void setInventoryInformationService( InventoryInformationService inventoryInformationService )
{
	this.inventoryInformationService = inventoryInformationService;
}

public InventoryDAO getInventoryDAO() {
	return inventoryDAO;
}

public void setInventoryDAO(InventoryDAO inventoryDAO) {
	this.inventoryDAO = inventoryDAO;
}

public PerformanceSummaryHelperService getPerformanceSummaryHelperService()
{
	return performanceSummaryHelperService;
}

public void setPerformanceSummaryHelperService( PerformanceSummaryHelperService performanceSummaryHelperService )
{
	this.performanceSummaryHelperService = performanceSummaryHelperService;
}

public VehicleInventoryService getVehicleInventoryService()
{
	return vehicleInventoryService;
}

public void setVehicleInventoryService( VehicleInventoryService vehicleInventoryService )
{
	this.vehicleInventoryService = vehicleInventoryService;
}

}
