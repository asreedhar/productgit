package com.firstlook.action.estockcard;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.BasicAuthAdapter;
import biz.firstlook.services.mmr.ManheimQueueClient;
import biz.firstlook.services.mmr.ManheimQueueSoap;
import biz.firstlook.services.mmr.MMRServiceClient;
import biz.firstlook.services.mmr.entity.MMRArea;
import biz.firstlook.services.mmr.entity.MMRTransactions;
import biz.firstlook.services.mmr.entity.MMRTrim;
import biz.firstlook.services.mmr.objects.MMRInventoryRequest;
import biz.firstlook.services.mmr.objects.MMRResult;
import biz.firstlook.services.mmr.objects.QueueStatus;
import biz.firstlook.services.mmr.objects.UserIdentity;
import biz.firstlook.transact.persist.model.Inventory;
import biz.firstlook.transact.persist.model.MMRVehicle;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.persistence.IMMRVehicleDao;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.inventory.VehicleInventoryService;

public class MMRSaveDataAction extends SecureBaseAction {

	IMMRVehicleDao mmrVehicleDao;
	private VehicleInventoryService vehicleInventoryService;

	
	
	
	
	public IMMRVehicleDao getMmrVehicleDao() {
		return mmrVehicleDao;
	}

	public void setMmrVehicleDao(IMMRVehicleDao mmrVehicleDao) {
		this.mmrVehicleDao = mmrVehicleDao;
	}
	
	

	

	public VehicleInventoryService getVehicleInventoryService() {
		return vehicleInventoryService;
	}

	public void setVehicleInventoryService(
			VehicleInventoryService vehicleInventoryService) {
		this.vehicleInventoryService = vehicleInventoryService;
	}

	
	private MMRArea[] mmrAreas = {};
	private MMRTrim[] mmrTrims = {};
	private String mmrServiceError="";
	private String mmrErrorMsg = "";
	
	
	
	public String getMmrServiceError() {
		return mmrServiceError;
	}

	public void setMmrServiceError(String mmrServiceError) {
		this.mmrServiceError = mmrServiceError;
	}

	public String getMmrErrorMsg() {
		return mmrErrorMsg;
	}

	public void setMmrErrorMsg(String mmrErrorMsg) {
		this.mmrErrorMsg = mmrErrorMsg;
	}



	

	public MMRArea[] getMmrAreas() {
		return mmrAreas;
	}

	public void setMmrAreas(MMRArea[] mmrAreas) {
		this.mmrAreas = mmrAreas;
	}

	public MMRTrim[] getMmrTrims() {
		return mmrTrims;
	}

	public void setMmrTrims(MMRTrim[] mmrTrims) {
		this.mmrTrims = mmrTrims;
	}

	
	
	
	@Override
	public ActionForward doIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws DatabaseException, ApplicationException {
		
		setMmrErrorMsg("");
		setMmrServiceError("");
		Integer inventoryId= Integer.parseInt(request.getParameter("InventoryID"));
		System.out.println("InventoryID" + inventoryId);
		String mid=request.getParameter("selectedMmrTrim");
		String area=request.getParameter("selectedMmrArea");
		
		Inventory inventory = vehicleInventoryService.getInventoryDAO().findBy(inventoryId);
		MMRVehicle mmrVehicle = inventory.getMmrVehicle();
		Date dateUpdated=null;
		if(mmrVehicle!=null)
			dateUpdated= mmrVehicle.getDateUpdated();
		Double averagePrice= null;	
		Vehicle vehicle= vehicleInventoryService.getVehicleDAO().findByPk(inventory.getVehicleId());
		
		
		

	
		//if(request.getParameter("isDBUpdate").equalsIgnoreCase("false")){
			
			//******** MMR Service Call
			
			ManheimQueueClient mClient = new ManheimQueueClient();
			ManheimQueueSoap mSoap = mClient.getManheimQueueSoap();
			String username=(String)request.getSession().getAttribute("com.discursive.cas.extend.client.filter.user");
	
			MMRInventoryRequest mmrRequest= new MMRInventoryRequest();
			UserIdentity userIdentity= new UserIdentity();
			userIdentity.setUserName(username);
			
			mmrRequest.setMID(mid);
			mmrRequest.setRegionCode(area);
			mmrRequest.setInventoryId(inventoryId);
			mmrRequest.setVehicleYear(vehicle.getVehicleYear());
			mmrRequest.setIsDbUpdateRequired(false);
			try{
				
				MMRResult result = mSoap.updateAveragePrice(mmrRequest, userIdentity);
				averagePrice=(Double.parseDouble(result.getAveragePrice()+""));
				if(result.getStatus().equals(QueueStatus.ERROR)||result.getStatus().equals(QueueStatus.MMR_ERROR))
				{
					setMmrServiceError(result.getErrorMessage());
					if(result.getStatus().equals(QueueStatus.MMR_ERROR)){
						setMmrServiceError("Manheim does not have values for this trim");
					}
				}
				 
			}
			catch(Exception e){
				e.printStackTrace();
				System.out.println("Exception Occured");
				setMmrServiceError(e.getMessage());
			}
		//}
		
		//Saving MMR Vehicle in DB
		if(request.getParameter("isDBUpdate").equalsIgnoreCase("true")){
			
			dateUpdated= new Date();
			//averagePrice= Double.parseDouble(request.getParameter("auctionaverage"));
			if(mmrVehicle==null)
			{
				mmrVehicle=new MMRVehicle();
				if(mid!=null)
					mmrVehicle.setMid(mid);
				if(area!=null)
					mmrVehicle.setRegionCode(area);
				if(averagePrice!=null)
					mmrVehicle.setAveragePrice(averagePrice);
			}
			else{
				if(mid!=null)
					mmrVehicle.setMid(mid);
				if(area!=null)
					mmrVehicle.setRegionCode(area);
				if(averagePrice!=null)
					mmrVehicle.setAveragePrice(averagePrice);
			}
			
			mmrVehicle.setDateUpdated(dateUpdated);
			
			mmrVehicleDao.save(mmrVehicle);
			
			if(inventory!=null)
				inventory.setMmrVehicle(mmrVehicle);
			
			vehicleInventoryService.getInventoryDAO().saveOrUpdate(inventory);
			inventory = vehicleInventoryService.getInventoryDAO().findBy(inventoryId);
			
		}
		
		
		
		
		String basicAuthString = (String) request.getSession().getServletContext().getAttribute(BasicAuthAdapter.NAME_IN_SESSION);
		

		String vin= vehicle.getVin();
		
		populateMMRAreas(basicAuthString);
		populateMMRTrims(basicAuthString,vin,area);
		
		
		
		request.setAttribute("mmrAreas", getMmrAreas());
		request.setAttribute("mmrTrims", getMmrTrims());
		
		request.setAttribute("inventoryformmr",inventory.getInventoryId() );
		request.setAttribute("mmrErrorMsg", getMmrErrorMsg());
		SimpleDateFormat sf= new SimpleDateFormat("yyyy.MM.dd HH:mm");
		if(dateUpdated!=null)
			request.setAttribute("lastUpdatedDate",sf.format(dateUpdated));
		request.setAttribute("mmrAverage", averagePrice);
		request.setAttribute("selectedMmrTrim", mid);
		request.setAttribute("selectedMmrArea", area);
		request.setAttribute("mmrServiceError", getMmrServiceError());
		
		if(inventory.getMmrVehicle()==null)
		{
			request.setAttribute("isExclamation", "exclamation");
		}
		
		
		
		return mapping.findForward("success");
	}
	
	
	
	
	
	
	


	public void populateMMRAreas(String basicAuthString) {
		if (mmrAreas.length < 1) {
			try {
				MMRServiceClient mmrServiceClient = new MMRServiceClient(
						basicAuthString);
				mmrAreas = mmrServiceClient.getMMRReference();
			} catch (Exception e) {
				// Exception already logged. Just keep whatever the last area
				// list was (or empty list).
			}
		}
	}

	public void populateMMRTrims(String basicAuthString,String vin,String area) {
		try {
			MMRServiceClient mmrServiceClient = new MMRServiceClient(
					basicAuthString);
			
			mmrTrims = mmrServiceClient.getMMRTrims(vin, area);

			
		} catch (Exception e) {
			setMmrErrorMsg(e.getMessage());
			// Exception already logged. Just return whatever the last trim list
			// was (or empty list).
		}
	}

	
}


	
	
	
	

