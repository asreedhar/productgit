package com.firstlook.action.estockcard;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import biz.firstlook.commons.enumeration.RepriceSouce;
import biz.firstlook.commons.services.inventory.IInventoryService;
import biz.firstlook.commons.services.vehicleCatalog.IVehicleCatalogService;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogEntry;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogKey;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogMultiEntry;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogServiceException;
import biz.firstlook.transact.persist.enumerator.BookoutStatusEnum;
import biz.firstlook.transact.persist.enumerator.EdmundsValueEnum;
import biz.firstlook.transact.persist.model.BookOut;
import biz.firstlook.transact.persist.model.BookOutThirdPartyCategory;
import biz.firstlook.transact.persist.model.BookOutValue;
import biz.firstlook.transact.persist.model.Inventory;
import biz.firstlook.transact.persist.model.InventoryWithBookOut;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.Vehicle;
import biz.firstlook.transact.persist.persistence.InventoryBookOutDAO;
import biz.firstlook.transact.persist.persistence.InventoryDAO;
import biz.firstlook.transact.persist.persistence.InventoryTransferPriceStoredProcedure;


import com.firstlook.action.components.vehicledetailpage.VehicleInventory;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.inventory.UpdateInventoryLightProc;
import com.firstlook.service.dealer.IDealerService;
import com.firstlook.service.inventory.VehicleInventoryService;
import com.firstlook.service.vehicle.VehicleService;
import biz.firstlook.services.inventoryPhotos.InventoryPhotosWebServiceClient;

public class InventoryInformationService
{

private static final Logger logger = Logger.getLogger( InventoryInformationService.class );
private static final String CATALOG_KEYS = "CatalogKeys";


public static final String IIDB_INVENTORY_INFORMATION = "InventoryInformation";
public static final String IIDB_VEHICLE_INFORMATION = "VehicleInformation";
public static final String IIDB_UNDER_HOOD = "UnderTheHoodInformation";

public static final String VI_BASE_COLOR = "BaseColor";

private VehicleInventoryService vehicleInventoryService;
private IDealerService dealerService;
private VehicleService vehicleService;
private InventoryBookOutDAO inventoryBookOutDAO;
private IInventoryService inventoryService;
private UpdateInventoryLightProc updateInventoryLightProc;
private InventoryDAO inventoryDAO;
private IVehicleCatalogService vehicleCatalogService;
private InventoryTransferPriceStoredProcedure inventoryTransferPriceStoredProcedure;

@SuppressWarnings( "unchecked" )
public Map<String, Object> getInventoryInformationForInfoTile( int currentDealerId, Integer inventoryId, String userName, String userFirstName, String userLastName ) throws ApplicationException
{
	Dealer dealer = dealerService.retrieveDealer( currentDealerId );
	boolean showLotLocationAndStatus = dealer.getDealerPreference().isShowLotLocationStatus();
	int primaryGuideBookPreferenceId = dealer.getBookOutPreferenceId();
	ThirdPartyDataProvider bookoutProvider = 
		ThirdPartyDataProvider.getThirdPartyDataProvider(dealer.getDealerPreference().getGuideBookId());

	String guideBookName = ThirdPartyCategory.fromId(primaryGuideBookPreferenceId).getThirdPartyCategoryFullName();

	// sorry this is ugly but "kelley Blue Book" was too long so we had to
	// swap it out with KBB
	if ( guideBookName.startsWith( "Kelley Blue Book" ) )
	{
		Pattern p = Pattern.compile( "Kelley Blue Book" );
		Matcher m = p.matcher( guideBookName ); // get a matcher object
		guideBookName = m.replaceAll( "KBB" );
	}
	VehicleInventory vehicleInventory = vehicleInventoryService.retrieveVehicleInventory( currentDealerId, inventoryId );
	
	Vehicle vehicle = vehicleInventory.getVehicle();
	Inventory inventory = vehicleInventory.getInventory();
	Map<String, Object> inventoryInformationDisplayBean = new HashMap<String, Object>();

	Map<String, Object> inventoryInformation = getInventoryInformation( inventory, bookoutProvider.getThirdPartyId(), primaryGuideBookPreferenceId );
	Map<String, Object> vehicleInformation = getVehicleInformation( vehicle, showLotLocationAndStatus );
	Map<String, Object> underTheHood= new HashMap<String, Object>();
	try {
		underTheHood = getUnderTheHoodStuff( vehicle.getVin() );
	
	} catch (VehicleCatalogServiceException e) {
		logger.error("Vehicle Catalog Exception while trying to get vehicle information for VIN : " + vehicle.getVin());
	}
	
	inventoryInformationDisplayBean.put( IIDB_INVENTORY_INFORMATION, inventoryInformation );
	inventoryInformationDisplayBean.put( IIDB_VEHICLE_INFORMATION, vehicleInformation );
	inventoryInformationDisplayBean.put( IIDB_UNDER_HOOD, underTheHood );
	inventoryInformationDisplayBean.put( "primaryGuideBookString", guideBookName );
	inventoryInformationDisplayBean.put( "useLotPrice", dealer.getDealerPreference().getUseLotPrice());
	inventoryInformationDisplayBean.put( "LastRepriced", "stub" );
	inventoryInformationDisplayBean.put( "selectedCatalogKeyIndex", getSelectedCatalogKeyIndex( vehicle, (ArrayList)underTheHood.get( CATALOG_KEYS ) ) );
	inventoryInformationDisplayBean.put( "Locked", inventory.getBookoutLocked() );
	inventoryInformationDisplayBean.put( "isActive", inventory.isInventoryActive() );
	inventoryInformationDisplayBean.put("make",vehicleInventory.getVehicle().getMake());
	
	List catalogKeys = (ArrayList)underTheHood.get( CATALOG_KEYS );
	Integer selectedCatalogKeyIndex = (Integer) inventoryInformationDisplayBean.get("selectedCatalogKeyIndex") == -1 ? 0 : (Integer) inventoryInformationDisplayBean.get("selectedCatalogKeyIndex");
	VehicleCatalogKey vehicleCatalogKey = (VehicleCatalogKey) catalogKeys.get(selectedCatalogKeyIndex);
	
	inventoryInformationDisplayBean.put("catalogKeySelected", vehicleCatalogKey.getVehicleCatalogId());
	
	
		
	String businessUnitCode = dealer.getDealerCode();
	String vin = vehicle.getVin();
	List<String> photos = InventoryPhotosWebServiceClient.getInstance().GetInventoryPhotoUrls(businessUnitCode, vin);
	inventoryInformationDisplayBean.put( "hasPhotos", ! photos.isEmpty() );

	String photoMgrUrl = InventoryPhotosWebServiceClient.getInstance().GetPhotoManagerUrl(userName, userFirstName, userLastName, businessUnitCode, vin, inventory.getStockNumber());
	inventoryInformationDisplayBean.put( "photoManagerUrl", photoMgrUrl );	
	
	return inventoryInformationDisplayBean;
}

public VehicleInventory updateInventoryAndVehicle( final VehicleInventory vehicleInventoryFromPage, final boolean mileageChanged,
													final boolean listPriceChanged, final boolean updateCatalogKey, String login,
													String repriceConfirm, boolean transferPriceChanged )
		throws ApplicationException, VehicleCatalogServiceException
{
	Integer currentDealerId = vehicleInventoryFromPage.getInventory().getDealerId();

	Dealer dealer = dealerService.retrieveDealer( currentDealerId );
	int primaryGuideBook = dealer.getGuideBookId();
	int secondaryGuideBook = dealer.getGuideBook2Id();

	// Must update vehicle first. the Light is calculated inside updateInventory and we need to have the right make/model for light. -bf.
	Vehicle vehicle = updateVehicle( vehicleInventoryFromPage, updateCatalogKey );
	Inventory inventory = updateInventory( vehicleInventoryFromPage.getInventory(), primaryGuideBook, secondaryGuideBook, mileageChanged,
											listPriceChanged, login, repriceConfirm, transferPriceChanged );
	vehicleInventoryFromPage.setInventory( inventory );
	vehicleInventoryFromPage.setVehicle( vehicle );
	return vehicleInventoryFromPage;
}

private Map<String, Object> getVehicleInformation( Vehicle vehicle, boolean showLotLocationAndStatus )
{
	Map<String, Object> vehicleInformation = new HashMap<String, Object>();
	vehicleInformation.put( VI_BASE_COLOR, vehicle.getBaseColor() );
	vehicleInformation.put( "DoorCount", vehicle.getDoorCount() );
	vehicleInformation.put( "InteriorColor", vehicle.getInteriorColor() );
	vehicleInformation.put( "InteriorDescription", vehicle.getInteriorDescription() );
	vehicleInformation.put( "CylinderCount", vehicle.getCylinderCount() );
	vehicleInformation.put( "VehicleDriveTrain", vehicle.getVehicleDriveTrain() );
	vehicleInformation.put( "VehicleTransmission", vehicle.getVehicleTransmission() );
	vehicleInformation.put( "VehicleEngine", vehicle.getVehicleEngine() );
	vehicleInformation.put( "VehicleTrim", vehicle.getVehicleTrim() );
	vehicleInformation.put( "VehicleYear", vehicle.getVehicleYear() );
	vehicleInformation.put( "MakeModelGroupingId", vehicle.getMakeModelGroupingId() );

	return vehicleInformation;
}

private Map< String, Object > getInventoryInformation( Inventory inventory, Integer thirdPartyId, Integer thirdPartyCategoryId ) {
	Map< String, Object > inventoryInformation = new HashMap< String, Object >();
	
	InventoryWithBookOut ib = inventoryBookOutDAO.findByPk(inventory.getInventoryId());
	double estockAutoPopulateValue = inventoryBookOutDAO.getTrueMarketValue(inventory.getInventoryId(), inventory.getDealerId(), EdmundsValueEnum.TRADEIN);//33026 | auto-populate in e-stock
	BookOut bookout = ib.getLatestBookOut(thirdPartyId);
	if(bookout != null) {
		BookOutThirdPartyCategory category = bookout.getBookOutThirdPartyCategory(thirdPartyCategoryId);
		if(category != null) {
			BookOutValue value = category.getFinalBookValue();
			if(value != null) {
				Integer book = value.getValue();				
				inventoryInformation.put("PrimaryGuideBookValue", book);

				if(book != null) {
					Double diff = Double.valueOf(book.doubleValue() - inventory.getUnitCost());
					inventoryInformation.put( "BookVsCost", diff.intValue() );
				}
				
			} else {
				inventoryInformation.put("PrimaryGuideBookValue", null);
				inventoryInformation.put("BookVsCost", null );
			}
		}
		inventoryInformation.put("BookoutIsAccurate", bookout.isAccurate());
	}

	inventoryInformation.put( "StockNumber", inventory.getStockNumber() );
	inventoryInformation.put( "UnitCost", BigDecimal.valueOf( inventory.getUnitCost() ) );
	inventoryInformation.put( "Mileage", inventory.getMileageReceived() );
	inventoryInformation.put( "ListPrice", inventory.getListPrice() );
	inventoryInformation.put( "LotPrice", inventory.getLotPrice() );
	inventoryInformation.put( "InventoryID", inventory.getInventoryId() );
	inventoryInformation.put( "SpecialFinance", inventory.isSpecialFinance() );
	inventoryInformation.put( "Certified", inventory.isCertified() );
	inventoryInformation.put( "TradeOrPurchaseDesc", ( inventory.getTradeOrPurchase() == 2 ) ? "Trade" : ( inventory.getTradeOrPurchase() == 3  ? "Unknown Source":"Purchase") );
	inventoryInformation.put( "LotLocation", inventory.getLotLocation() );
	inventoryInformation.put( "EdmundsTMV", inventory.getEdmundsTMV());
	inventoryInformation.put( "InventoryActive", inventory.getInventoryActive());
	inventoryInformation.put( "EstockAutoPopulateValue", estockAutoPopulateValue );//Case 33026 | auto-populate in e-stock
	inventoryInformation.put( "transferPrice", inventory.getTransferPrice());
	inventoryInformation.put( "transferForRetailOnly", inventory.getRetailOnlyTransfer());
	inventoryInformation.put( "transferForRetailOnlyEnabled", inventory.isTransferForRetailOnlyEnabled());

	return inventoryInformation;
}

@SuppressWarnings( "unchecked" )
private Map<String, Object> getUnderTheHoodStuff( String vin ) throws ApplicationException, VehicleCatalogServiceException
{
	Map<String, Object> result = new HashMap<String, Object>();
	ArrayList catalogKeys = new ArrayList();
	
	Vehicle vehicle = vehicleService.loadVehicle( vin );
	VehicleCatalogKey currentCatalogKey = vehicle.getVehicleCatalogKey();
	
	boolean catalogKeyAdded = false;
	// add current catalog values
	VehicleCatalogMultiEntry vcMultiEntry = null;
	try {
		vcMultiEntry = vehicleCatalogService.retrieveVehicleCatalogMultiEntry( vin );
	} catch (VehicleCatalogServiceException e) {} // the code handles the null value so ignoring the exception is OK
	if ( vcMultiEntry != null )
	{
		for ( int i = catalogKeys.size(); i < vcMultiEntry.getCatalogKeys().size(); i++ )
		{
			VehicleCatalogKey catalogKey = vcMultiEntry.getCatalogKeys().get( i );
			catalogKeys.add( i, catalogKey );
			if (currentCatalogKey.getVehicleCatalogId().intValue() == catalogKey.getVehicleCatalogId().intValue()) {
				catalogKeyAdded = true;
			}
		}
	} 
	
	// an old legacy vehicle which has been resolved to a level 1 catalogKey
	if (!catalogKeyAdded && currentCatalogKey.getVehicleCatalogDescription() != null && currentCatalogKey.getVehicleCatalogDescription().trim().length() > 0) {
		catalogKeys.add(0 , currentCatalogKey);
	}

	result.put( CATALOG_KEYS, catalogKeys );
	return result;
}

private Integer getSelectedCatalogKeyIndex( Vehicle vehicle, ArrayList<VehicleCatalogKey> catalogKeys )
{
	for ( int i = 0; i < catalogKeys.size(); i++ )
	{
		if ( vehicle.getVehicleCatalogID() == catalogKeys.get( i ).getVehicleCatalogId().intValue()) 
		{
			return new Integer( i );
		}
	}
	// this is an OK scenario for when vehicles come into inventory and don't uniquely decode to an catalogKey so the user must select one 
	return new Integer( -1 );
}

private Vehicle updateVehicle( VehicleInventory vehicleInventoryFromPage, boolean updateCatalogKey ) throws VehicleCatalogServiceException
{

	VehicleInventory vehicleInventory = vehicleInventoryService.retrieveVehicleInventory(
																							vehicleInventoryFromPage.getInventory().getDealerId(),
																							vehicleInventoryFromPage.getInventory().getInventoryId() );
	Vehicle vehicle = vehicleInventory.getVehicle();
	Vehicle vehicleFromPage = vehicleInventoryFromPage.getVehicle();
	// S7672 :
	// If color is updated - we need to set flag on eStock lock on inventory
	if (updateCatalogKey || !vehicle.getBaseColor().equalsIgnoreCase(vehicleFromPage.getBaseColor())) {
		Inventory  inventory = vehicleInventory.getInventory();
		inventory.seteStockCardLock(Boolean.TRUE);
		//case 32980 : updating BaseColorLock column when there is change.
		if (!vehicle.getBaseColor().equalsIgnoreCase(vehicleFromPage.getBaseColor())) { 
			inventory.setBaseColorLock(Boolean.TRUE);
		}
		vehicleInventoryService.saveOrUpdateInventory(inventory);		
	}
	
	vehicle.setBaseColor( vehicleFromPage.getBaseColor() );
	vehicle.setInteriorColor( vehicleFromPage.getInteriorColor() );
	vehicle.setInteriorDescription( vehicleFromPage.getInteriorDescription() );
	// only update this stuff if it has changed
	if ( updateCatalogKey )
	{
		// Modify the existing vehicle to have the attributes that correspond to the new catalogKey
		VehicleCatalogEntry vcEntry = vehicleCatalogService.retrieveVehicleCatalogEntry( vehicle.getVin(), vehicleFromPage.getVehicleCatalogKey().getVehicleCatalogId() );
		Vehicle vehicleLookUp = new Vehicle( vcEntry );
		vehicle.setVehicleCatalogKey( vehicleLookUp.getVehicleCatalogKey() );
		VehicleService.copyVehicleAttributes( vehicle, vehicleLookUp );
	}

	vehicleInventoryService.saveOrUpdateVehicle( vehicle );
	
	return vehicle;
}

private Inventory updateInventory( Inventory inventoryFromPage, int primaryBookId, int secondaryBookId, boolean mileageChanged,
									boolean listPriceChanged, String login, String repriceConfirm, boolean transferPriceChanged )
{
	InventoryWithBookOut inventoryWithBookout = inventoryBookOutDAO.findByPk( inventoryFromPage.getInventoryId() );
	inventoryWithBookout.setSpecialFinance( inventoryFromPage.isSpecialFinance() );
	inventoryWithBookout.setLotLocation( inventoryFromPage.getLotLocation() );
	inventoryWithBookout.getLotLocationLock();
	inventoryWithBookout.setLotLocationLock(inventoryFromPage.getLotLocationLock());
	inventoryWithBookout.setEdmundsTMV( inventoryFromPage.getEdmundsTMV() );
	if (inventoryFromPage.getLotPrice() != null) {
		inventoryWithBookout.setLotPrice(inventoryFromPage.getLotPrice() );
	}
	if ( listPriceChanged || (inventoryWithBookout.isCertified() != inventoryFromPage.isCertified())) {
		inventoryWithBookout.seteStockCardLock( Boolean.TRUE );		
	}
	/* inventoryWithBookout.setCertified( inventoryFromPage.isCertified() ); */

	if ( mileageChanged )
	{
		BookOut secondaryBookBookout = null;
		BookOut primaryBookBookout = updateBookOutStatus(inventoryWithBookout.getLatestBookOut( primaryBookId ));
		if (secondaryBookId > 0) 
			secondaryBookBookout = updateBookOutStatus(inventoryWithBookout.getLatestBookOut(secondaryBookId));

		if ( logger.isDebugEnabled() )
		{
			if ( primaryBookBookout == null )
			{
				logger.debug( "Could not update Primary Book accuracy for inventoryId: " + inventoryWithBookout.getInventoryId() );
			}

			if ( secondaryBookBookout == null )
			{
				logger.debug( "Could not update Secondary Book accuracy for inventoryId: " + inventoryWithBookout.getInventoryId() );
			}
		}
		
		//must update the mileage before light can be calculated
		inventoryWithBookout.setMileageReceived( inventoryFromPage.getMileageReceived() );
		// nk : S762 : Override Lock - prevents DMS from updating mileage. Otherwise DMS overrides
		inventoryWithBookout.setMileageReceivedLock(Boolean.TRUE);

		// nk : S762 : Sets bookoutRequired field so that the incremental BO Processor will pick up this vehicle
		// nk : DE623 : BookOut status has been changed to USER_MILEAGE CHANGE
		// bf : need to be able to distinguish which book we are marking for incremental!
		inventoryWithBookout.markIncrementalBookout(ThirdPartyDataProvider.getThirdPartyDataProvider(primaryBookId));
		if (secondaryBookId > 0)
			inventoryWithBookout.markIncrementalBookout(ThirdPartyDataProvider.getThirdPartyDataProvider(secondaryBookId));
		
		//need this here because our light logic sits within the database!!!
		inventoryBookOutDAO.saveOrUpdate( inventoryWithBookout );
		Inventory updatedLightInventory = updateInventoryLightProc.calculateAgingPlanLightForSpecificInventory( inventoryFromPage.getDealerId(),
																												inventoryFromPage.getInventoryId() );

		Integer light = updatedLightInventory.getCurrentVehicleLight();
		inventoryWithBookout.setCurrentVehicleLight( light );
		inventoryFromPage.setCurrentVehicleLight( light );
	} 
	
	if ( listPriceChanged )
	{
		boolean repriceConfirmed = (repriceConfirm != null && repriceConfirm.contains( "true" ));

		Integer origListPrice = null;
		
		Integer newListPrice = null;
		
		if (inventoryWithBookout.getListPrice() != null){
			origListPrice = inventoryWithBookout.getListPrice().intValue();
		}
		
		if (inventoryFromPage.getListPrice() != null){
			newListPrice = inventoryFromPage.getListPrice().intValue();
		}
		
		inventoryService.repriceInventory( inventoryFromPage.getInventoryId(), new Date(), new Date(), origListPrice, newListPrice, repriceConfirmed, login, new Date(), inventoryFromPage.getDealerId(), RepriceSouce.ESTOCK);
		inventoryWithBookout.setListPrice( inventoryFromPage.getListPrice() );
		inventoryWithBookout.setListPriceLock(Boolean.TRUE);
		
	}
	
	inventoryWithBookout.setTransferForRetailOnly(inventoryFromPage.getRetailOnlyTransfer());
	inventoryBookOutDAO.saveOrUpdate( inventoryWithBookout );
	if(transferPriceChanged) {
		inventoryWithBookout.setTransferPrice(inventoryFromPage.getTransferPrice());
		inventoryTransferPriceStoredProcedure.save(inventoryFromPage, inventoryFromPage.getTransferPrice(), login);
	}
	
	//	Copy inventory values from page to inventory object in Hibernate session when new values are entered in the estock card
	//needed in this case to display updated values on the estock card since when loading information InventoryDAO is used, 
	// not InventoryWithBookoutDAO
	Inventory inventoryInHibernateSession = inventoryDAO.findBy(inventoryFromPage.getInventoryId());
	copyInventoryData(inventoryWithBookout, inventoryInHibernateSession);
	copyInventoryData(inventoryWithBookout, inventoryFromPage);
	return inventoryFromPage;
}


/**
 * Helper function to copy a subset of fields from the inventory values on the page
 * to the inventory object in the hibernate session.  The fields that can be updated on the 
 * eStockCard are copied.
 * @param inventory
 * @param inventoryInHibernateSession
 */
private void copyInventoryData(InventoryWithBookOut inventory, Inventory inventoryInHibernateSession ){
	inventoryInHibernateSession.setCertified( inventory.isCertified() );
	inventoryInHibernateSession.setSpecialFinance( inventory.isSpecialFinance() );
	inventoryInHibernateSession.setLotLocation( inventory.getLotLocation() );
	inventoryInHibernateSession.setLotLocationLock(inventory.getLotLocationLock());
	inventoryInHibernateSession.seteStockCardLock( inventory.geteStockCardLock() );
	inventoryInHibernateSession.setEdmundsTMV( inventory.getEdmundsTMV() );
	inventoryInHibernateSession.setMileageReceived(inventory.getMileageReceived());
	inventoryInHibernateSession.setCurrentVehicleLight(inventory.getCurrentVehicleLight());
	inventoryInHibernateSession.setListPrice(inventory.getListPrice());
	inventoryInHibernateSession.setLotPrice(inventory.getLotPrice());
	inventoryInHibernateSession.setTransferPrice(inventory.getTransferPrice());
	inventoryInHibernateSession.setRetailOnlyTransfer(inventory.getTransferForRetailOnly());
}

/**
 * Sets the BookStatus on the most recent Bookout in the list for the given thirdPartyId
 * If the current status = NOT REVIEWED, no change is made. The only way this status can change is
 * if a user first books out a vehicle. Just changing the mileage does nothing.
 * 
 * If the current status is anything but NOT REVIEWED or INGROUP TRANSFER, the new status = USER MILEAGE CHANGE
 *  
 * @param primaryBookId
 * @param inventoryWithBookout
 * @return the Bookout record. This is returned so a null check can be made to determine if the status flag was set or not.
 */
private BookOut updateBookOutStatus( BookOut bookout )
{
	if ( bookout != null )
	{
		if (bookout.getBookoutStatusId().intValue() != BookoutStatusEnum.NOT_REVIEWED.getId().intValue() && bookout.getBookoutStatusId().intValue() != BookoutStatusEnum.INGROUP_TRANSFER.getId().intValue()) {
			bookout.setBookoutStatusId(BookoutStatusEnum.USER_MILEAGE_CHANGE.getId());			
		}
	}
	return bookout;
}

public void setImtDealerService( IDealerService dealerService )
{
	this.dealerService = dealerService;
}

public void setVehicleInventoryService( VehicleInventoryService vehicleInventoryService )
{
	this.vehicleInventoryService = vehicleInventoryService;
}

public void setInventoryBookOutDAO( InventoryBookOutDAO inventoryBookOutDAO )
{
	this.inventoryBookOutDAO = inventoryBookOutDAO;
}

public void setVehicleService( VehicleService vehicleService )
{
	this.vehicleService = vehicleService;
}

public void setUpdateInventoryLightProc( UpdateInventoryLightProc updateInventoryLightProc )
{
	this.updateInventoryLightProc = updateInventoryLightProc;
}

public void setInventoryDAO(InventoryDAO inventoryDAO) {
	this.inventoryDAO = inventoryDAO;
}

public Integer getselectedCertificates(Integer inventoryId) throws SQLException
{
 Integer selectedCertificates = inventoryDAO.getSelectedCertificates(inventoryId);
 return selectedCertificates;
}



public Map<String,String> getCertificates(Integer businessUnitId,Integer inventoryId)
{
 Map<String,String> certificates = inventoryDAO.fetchCertificates(businessUnitId, inventoryId);
 return certificates;
}

public int getCertifiedId(Integer inventoryId) throws SQLException
{
	Integer certifiedId = inventoryDAO.getCertifiedId(inventoryId);
	return certifiedId;
}

public void setVehicleCatalogService( IVehicleCatalogService vehicleCatalogService )
{
	this.vehicleCatalogService = vehicleCatalogService;
}

public void setInventoryService( IInventoryService inventoryService )
{
	this.inventoryService = inventoryService;
}

public void setInventoryTransferPriceStoredProcedure(
		InventoryTransferPriceStoredProcedure inventoryTransferPriceStoredProcedure) {
	this.inventoryTransferPriceStoredProcedure = inventoryTransferPriceStoredProcedure;
}
}