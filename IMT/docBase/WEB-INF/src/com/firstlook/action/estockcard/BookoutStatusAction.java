package com.firstlook.action.estockcard;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.components.vehicledetailpage.VehicleInventory;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.service.inventory.VehicleInventoryService;

public class BookoutStatusAction extends SecureBaseAction 
{
	private VehicleInventoryService vehicleInventoryService;

	@Override
	public ActionForward doIt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws DatabaseException, ApplicationException {

		String bookOutValue = "ACCURATE"; 

		int previousMileage = Integer.parseInt(request.getParameter("oldMileage"));
		Integer inventoryId = new Integer( RequestHelper.getInt( request, "inventoryId" ) );

		int newMileage = vehicleInventoryService.findMileage(inventoryId);
		
		if(previousMileage != newMileage ){
			bookOutValue = "INACCURATE";
		}
		try {
			response.getWriter().write(bookOutValue);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public VehicleInventoryService getVehicleInventoryService()
	{
		return vehicleInventoryService;
	}

	public void setVehicleInventoryService( VehicleInventoryService vehicleInventoryService )
	{
		this.vehicleInventoryService = vehicleInventoryService;
	}

}
