package com.firstlook.action.estockcard;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.vehicle.description.old.BookOutDatasetInfo;
import biz.firstlook.module.vehicle.description.old.BookOutError;
import biz.firstlook.module.vehicle.description.old.BookOutTypeEnum;
import biz.firstlook.module.vehicle.description.old.GBException;
import biz.firstlook.module.vehicle.description.old.GuideBookInput;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.enumerator.BookoutStatusEnum;
import biz.firstlook.transact.persist.model.AuditBookOuts;
import biz.firstlook.transact.persist.model.BookOut;
import biz.firstlook.transact.persist.model.BookOutSource;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.InventoryWithBookOut;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;
import biz.firstlook.transact.persist.persistence.IInventoryBookOutDAO;
import biz.firstlook.transact.persist.service.AuditBookOutsService;
import biz.firstlook.transact.persist.service.ThirdPartyVehicleOptionService;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;
import biz.firstlook.transact.persist.service.appraisal.IAppraisalService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.dealer.vehicledetailpage.BookoutDetailForm;
import com.firstlook.data.DatabaseException;
import com.firstlook.display.tradeanalyzer.DisplayGuideBook;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.SessionHelper;
import com.firstlook.service.bookout.BookOutService;
import com.firstlook.service.bookout.appraisal.AppraisalBookOutService;
import com.firstlook.service.bookout.bean.BookValuesBookOutState;
import com.firstlook.service.bookout.inventory.InventoryBookOutService;

public class UpdateBookoutTileAction extends SecureBaseAction
{

private static final String MAJOR_DELIMETER = "|";

private DealerPreferenceDAO dealerPrefDAO;
private IInventoryBookOutDAO inventoryBookOutDAO;
private IAppraisalService appraisalService;
private InventoryBookOutService inventoryBookOutService;
private AppraisalBookOutService appraisalBookOutService;
private AuditBookOutsService auditBookOutsService;
private BookOutService bookOutService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	Dealer dealer = putDealerFormInRequest( request );
	String selectedEquipmentString = RequestHelper.getString( request, "selectedEquipmentOption" );

	Boolean displayValues = Boolean.valueOf( request.getParameter( "displayValues" ) );
	Boolean displayOptions = Boolean.valueOf( request.getParameter( "displayOptions" ) );
	Boolean locked = Boolean.valueOf( request.getParameter( "locked" ) );
	Boolean refreshBookData = Boolean.valueOf(request.getParameter("refreshBookData"));

	Integer inventoryId = Integer.valueOf( request.getParameter( "inventoryId" ) );
	Integer bookOutSourceId = Integer.valueOf( request.getParameter( "bookOutSourceId" ) );

	int guideBookId = getGuideBookIdFromRequest( request );
	request.setAttribute( "guideBookId", Integer.valueOf( guideBookId ) );

	BookoutDetailForm bookoutForm = (BookoutDetailForm)form;

	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	DealerPreference dealerPref = dealerPrefDAO.findByBusinessUnitId(currentDealerId);

	Member member = getMemberFromRequest( request );
	GuideBookInput input = new GuideBookInput();
	input.setVin( bookoutForm.getVin() );
	input.setBusinessUnitId( currentDealerId );
	input.setMileage( new Integer( RequestHelper.getInt( request, "inventoryMileage" ) ) );
	input.setState( dealer.getState() );
	input.setMemberId( member.getMemberId() );
	input.setNadaRegionCode( dealerPref.getNadaRegionCode() );

	checkThatTheRightFormWasInSession( request, bookoutForm, input,
															dealerPref.getSearchInactiveInventoryDaysBackThreshold(), refreshBookData );
	Iterator<?> iter = bookoutForm.getDisplayGuideBooks().iterator();
	DisplayGuideBook d = (DisplayGuideBook)iter.next();
	List<String> selectedOptionsList = new ArrayList<String>();
	StringTokenizer st = new StringTokenizer( selectedEquipmentString, MAJOR_DELIMETER );
	String token = null;
	while ( st.hasMoreTokens() )
	{
		token = st.nextToken();
		selectedOptionsList.add( token );
	}
	d.setSelectedEquipmentOptionKeys( selectedOptionsList.toArray( new String[selectedOptionsList.size()] ) );

	// if a vehicle has multiple options for the engin/transmision/drive
	// train then it will be in the request
	// if not, there is only 1 option, so it does not need to be changed
	if ( guideBookId == ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE )
	{
		if ( RequestHelper.getString( request, "driveTrain" ) != null )
		{
			d.setSelectedDrivetrainKey( RequestHelper.getString( request, "driveTrain" ) );
		}
		if ( RequestHelper.getString( request, "engine" ) != null )
		{
			d.setSelectedEngineKey( RequestHelper.getString( request, "engine" ) );
		}
		if ( RequestHelper.getString( request, "transmission" ) != null )
		{
			d.setSelectedTransmissionKey( RequestHelper.getString( request, "transmission" ) );
		}
		if ( RequestHelper.getString( request, "condition" ) != null )
		{
			d.setConditionId( RequestHelper.getInt( request, "condition" ) );
		}
	}
	bookoutForm.setMileage( new Integer( RequestHelper.getInt( request, "inventoryMileage" ) ) );

	boolean hasConflictingOptions = bookoutAndSaveInformation( bookoutForm, input, dealerPref.getBookOutPreferenceId(), request,
																inventoryId, bookOutSourceId,
																dealerPref.getSearchInactiveInventoryDaysBackThreshold() );
	updateAccuracyOfBooks( bookoutForm );

	DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerId( currentDealerId );
	request.setAttribute( "isLithiaStore", dealerGroup.isLithiaStore() );

	request.setAttribute( "inventoryId", inventoryId );
	request.setAttribute( "locked", locked );
	request.setAttribute( "bookOutSourceId", bookOutSourceId );
	request.setAttribute( "displayValues", displayValues );
	request.setAttribute( "displayOptions", displayOptions );
	request.setAttribute( "isActive", Boolean.TRUE );
	request.setAttribute( "inventoryMileage", bookoutForm.getMileage() );
	request.setAttribute( "vin", bookoutForm.getVin() );
	
	SessionHelper.setAttribute( request, "bookoutDetailForm", bookoutForm );
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );
	request.setAttribute( "bookoutDetailForm", bookoutForm );

	SessionHelper.keepAttribute( request, "vehicleInformationForm" );
	SessionHelper.keepAttribute( request, "auctionForm" );

	if ( hasConflictingOptions )
	{
		return mapping.findForward( "optionsConflict" );
	}

	return mapping.findForward( "success" );
}

private void updateAccuracyOfBooks( BookoutDetailForm bookoutForm )
{
	Collection<DisplayGuideBook> guideBooks = bookoutForm.getDisplayGuideBooks();
	Iterator<DisplayGuideBook> guideBookIter = guideBooks.iterator();
	while ( guideBookIter.hasNext() ) {
		DisplayGuideBook guideBook = guideBookIter.next();
		guideBook.setInAccurate( false );
	}
}

private boolean bookoutAndSaveInformation( BookoutDetailForm bookoutForm, GuideBookInput input, int primaryGuideBookId,
											HttpServletRequest request, Integer identifier, Integer bookOutSourceId,
											int searchInactiveInventoryDaysBackThreshold ) 
{
	boolean hasConflictingOptions = false;

	ActionErrors guideBookErrors = new ActionErrors();

	DisplayGuideBook guideBook = null;
	VDP_GuideBookVehicle vehicle;
	String selectedKey;

	List<DisplayGuideBook> guideBooks = (List<DisplayGuideBook>)bookoutForm.getDisplayGuideBooks();
	Iterator<DisplayGuideBook> guideBookIter = guideBooks.iterator();
	while ( guideBookIter.hasNext() )
	{
		guideBook = guideBookIter.next();
		try
		{
			if ( guideBook.isValidVin() )
			{
				selectedKey = guideBook.getSelectedVehicleKey();
				vehicle = guideBook.determineSelectedVehicle( selectedKey );
				if ( vehicle != null )
				{
					vehicle.setCondition( guideBook.getCondition() );
					guideBook.setSelectedEquipmentOptionKeys( ThirdPartyVehicleOptionService.setAndUpdateSelectedOptionsUsingArray(
																																	guideBook.getDisplayEquipmentGuideBookOptions(),
																																	guideBook.getSelectedEquipmentOptionKeys() ) );
					// these three are for Kelly book outs only
					ThirdPartyVehicleOptionService.setSelectedOption( guideBook.getDisplayEngineGuideBookOptions(),
																		guideBook.getSelectedEngineKey() );
					ThirdPartyVehicleOptionService.setSelectedOption( guideBook.getDisplayTransmissionGuideBookOptions(),
																		guideBook.getSelectedTransmissionKey() );
					ThirdPartyVehicleOptionService.setSelectedOption( guideBook.getDisplayDrivetrainGuideBookOptions(),
																		guideBook.getSelectedDrivetrainKey() );
					BookValuesBookOutState state = updateBookValuesState( identifier, input, vehicle,
																			guideBook.getDisplayGuideBookOptions(),
																			(List<VDP_GuideBookVehicle>) guideBook.getDisplayGuideBookVehiclesCollection(),
																			guideBook.isStoredInfo(), bookOutSourceId,
																			guideBook.getGuideBookId(),
																			searchInactiveInventoryDaysBackThreshold );
					if ( !state.getBookOutErrors().isEmpty() )
					{
						if ( guideBook.getHasErrors().booleanValue() == false )
						{
							guideBook.setHasErrors( Boolean.TRUE );
						}
					}
					else
					{
						request.setAttribute( "success", "true" );
					}
					if ( state.getBookOutErrors().get( new Integer( BookOutError.OPTIONS_CONFLICT ) ) != null )
					{
						ArrayList<?> bookOutErrors = (ArrayList<?>)state.getBookOutErrors().get( Integer.valueOf( BookOutError.OPTIONS_CONFLICT ) );

						Iterator<?> bookOutErrorsIter = bookOutErrors.iterator();

						BookOutError bookOutError;
						while ( bookOutErrorsIter.hasNext() )
						{
							bookOutError = (BookOutError)bookOutErrorsIter.next();
							guideBookErrors.add( ActionErrors.GLOBAL_ERROR, new ActionError(	"error.kelley.conflict",
																								bookOutError.getFirstOptionDescription(),
																								bookOutError.getSecondOptionDescription() ) );
						}

						request.setAttribute( "guideBookError", new Boolean( true ) );
						putActionErrorsInRequest( request, guideBookErrors );
						hasConflictingOptions = true;
					}
					else
					{
						state.transformBookOutToDisplayData( guideBook );
					}
				}
			}

			List<VDP_GuideBookValue> mileageAdjustedValues = getInventoryBookOutService().constructMileageAdjustedValues( guideBook );
			request.setAttribute( "mileageAdjustedValues", mileageAdjustedValues );

		}
		catch ( Exception e )
		{
			guideBook.setSuccess( false );
			Throwable t = e.getCause();
			guideBook.setSuccessMessage( t == null ? e.getMessage() : t.getMessage() );
			guideBook.setGuideBookName( ThirdPartyDataProvider.getThirdPartyDataProviderDescription( guideBook.getGuideBookId().intValue() ) );
		}
	}

	request.setAttribute( "bookOutWasSaved", new Boolean( true ) );
	request.setAttribute( "displayGuideBook", guideBook );

	if ( RequestHelper.getBoolean( request, "fromEstock" ) )
	{
		checkIfPrimaryGuideBookAndComingFromEstockCard( request, guideBook, primaryGuideBookId );
	}

	return hasConflictingOptions;
}

private void checkIfPrimaryGuideBookAndComingFromEstockCard( HttpServletRequest request, DisplayGuideBook guideBook, int primaryGuideBookId )
{
	Iterator<VDP_GuideBookValue> valuesIter = guideBook.getDisplayGuideBookValues().iterator();
	while ( valuesIter.hasNext() ) {
		VDP_GuideBookValue value = valuesIter.next();
		if ( value.getThirdPartyCategoryId() == primaryGuideBookId ) {
			request.setAttribute( "primaryGuideBookValue", value.getValue() );
			request.setAttribute( "updatedPrimaryGuideBook", Boolean.TRUE );
			return;
		}
	}
	return;
}

private BookValuesBookOutState updateBookValuesState( Integer identifier, GuideBookInput input, 
														VDP_GuideBookVehicle selectedVehicle, List<ThirdPartyVehicleOption> options, List<VDP_GuideBookVehicle> vehicles,
														boolean isStoredInfo, Integer bookOutSourceId, Integer thirdPartyId,
														int searchInactiveInventoryDaysBackThreshold ) throws GBException
{
	BookValuesBookOutState bookOutState = null;
	if ( bookOutSourceId.intValue() == BookOutSource.BOOK_OUT_SOURCE_VEHICLE_DETAIL.intValue() )
	{
		DealerPreference prefs= dealerPrefDAO.findByBusinessUnitId(input.getBusinessUnitId());
		Boolean advertisingStatus= (prefs.getAdvertisingStatus() == null) ? false : prefs.getAdvertisingStatus();
		
		InventoryWithBookOut inventory = getInventoryBookOutDAO().findByPk( identifier );
		// nk DE623:
		// Always clean
		bookOutState = getInventoryBookOutService().updateBookValues( input,  selectedVehicle, options, vehicles,
																		AuditBookOuts.MANUAL_BOOKOUT_ID, inventory,
																		thirdPartyId, bookOutSourceId, BookoutStatusEnum.CLEAN ,advertisingStatus);
	}
	else if ( bookOutSourceId.intValue() == BookOutSource.BOOK_OUT_SOURCE_APPRAISAL.intValue() )
	{
		IAppraisal appraisal = appraisalService.findLatest( input.getBusinessUnitId(), input.getVin(),
																				searchInactiveInventoryDaysBackThreshold );
		bookOutState = getAppraisalBookOutService().updateBookValues( input, selectedVehicle, options, vehicles,
																		AuditBookOuts.APPRAISAL_BOOKOUT_ID, appraisal, thirdPartyId );
	}
	else
	{
		bookOutState = getBookOutService().updateBookValues( input, thirdPartyId, selectedVehicle, options, new BookOutDatasetInfo( BookOutTypeEnum.APPRAISAL, input.getBusinessUnitId()) );
		getAuditBookOutsService().logBookOuts( Integer.valueOf( input.getBusinessUnitId() ), input.getMemberId(),
															AuditBookOuts.FLUSAN_BOOKOUT_ID, input.getVin(), thirdPartyId,
															BookOut.DUMMY_BOOK_OUT );
	}
	return bookOutState;
}

private int getGuideBookIdFromRequest( HttpServletRequest request ) throws ApplicationException {
	int guideBookId = 0;
	guideBookId = RequestHelper.getInt( request, "guideBookId" );
	return guideBookId;
}

// warning: this is a really big hack to get the new estock card to work
// smoothly on the old architecture.
// the problem was that only 1 of the 2 guide books is ever in session, so
// after you change mileage and want to rebook
// the vehicle straight from the page, there is a 50% chance the wrong book
// is in session, this checks to see if which
// book is in sessino and if its the wrong one, it will go and build the
// right form. ask Dave W if you have a question - dw 4/28/06
//
// nk - 07/13/07 wow.... 
private void checkThatTheRightFormWasInSession( HttpServletRequest request, BookoutDetailForm bookoutForm, GuideBookInput input,
												int searchAppraisalDaysBackThreshold, Boolean refreshBookData) throws ApplicationException
{

	Iterator<DisplayGuideBook> iter = bookoutForm.getDisplayGuideBooks().iterator();

	DisplayGuideBook firstGuideBook = null;
	if ( iter.hasNext() ) {
		firstGuideBook = iter.next();
	}

	if ( firstGuideBook == null || firstGuideBook.getGuideBookId() == null
			|| firstGuideBook.getGuideBookId().intValue() != getGuideBookIdFromRequest( request ) )
	{
		BookoutTileAction action = new BookoutTileAction();
		action.setAppraisalBookOutService( appraisalBookOutService );
		action.setBookOutService( bookOutService );
		action.setInventoryBookOutService( inventoryBookOutService );

		action.populateDisplayGuideBooks( request, bookoutForm.getVin(), new Integer( request.getParameter( "inventoryId" ) ),
															bookoutForm.getMileage(), new Integer( getGuideBookIdFromRequest( request ) ),
															input, bookoutForm, new Integer( request.getParameter( "bookOutSourceId" ) ),
															searchAppraisalDaysBackThreshold, refreshBookData );

		// this method will just append the new guide book onto the list of
		// guide books
		// so we have to remove the first one (the one we don't want)
		bookoutForm.getDisplayGuideBooks().remove( firstGuideBook );
	}
}

public DealerPreferenceDAO getDealerPrefDAO()
{
	return dealerPrefDAO;
}

public void setDealerPrefDAO( DealerPreferenceDAO dealerPrefDAO )
{
	this.dealerPrefDAO = dealerPrefDAO;
}

public IInventoryBookOutDAO getInventoryBookOutDAO()
{
	return inventoryBookOutDAO;
}

public void setInventoryBookOutDAO( IInventoryBookOutDAO inventoryBookOutDAO )
{
	this.inventoryBookOutDAO = inventoryBookOutDAO;
}

public InventoryBookOutService getInventoryBookOutService()
{
	return inventoryBookOutService;
}

public void setInventoryBookOutService( InventoryBookOutService inventoryBookOutService )
{
	this.inventoryBookOutService = inventoryBookOutService;
}

public BookOutService getBookOutService()
{
	return bookOutService;
}

public void setBookOutService( BookOutService bookOutService )
{
	this.bookOutService = bookOutService;
}

public AppraisalBookOutService getAppraisalBookOutService()
{
	return appraisalBookOutService;
}

public void setAppraisalBookOutService( AppraisalBookOutService appraisalBookOutService )
{
	this.appraisalBookOutService = appraisalBookOutService;
}

protected IAppraisalService getAppraisalService()
{
	return appraisalService;
}

public void setAppraisalService( IAppraisalService appraisalService )
{
	this.appraisalService = appraisalService;
}

public AuditBookOutsService getAuditBookOutsService()
{
	return auditBookOutsService;
}

public void setAuditBookOutsService( AuditBookOutsService auditBookOutsService )
{
	this.auditBookOutsService = auditBookOutsService;
}

}
