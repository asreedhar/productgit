package com.firstlook.action.estockcard;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

/**
 * use nextGenreportDAO instead of me
 */
public class NextGenFLDWReportDAO extends JdbcDaoSupport
{

public List getResults( String sql, Object[] parameterValues )
{
	return (List)getJdbcTemplate().query( sql, parameterValues, new ReportDisplayBeanRowMapper( ) );
}



/**
 * inner class that maps the results form the query to our display bean
 */
class ReportDisplayBeanRowMapper implements ResultSetExtractor
{

public ReportDisplayBeanRowMapper()
{
}

public Object extractData( ResultSet rs ) throws SQLException, DataAccessException
{
	List<Map<String,Object>> reportDisplayBeans = new ArrayList<Map<String,Object>>();

	ResultSetMetaData rsMetaData = rs.getMetaData();

	int columnCount = rsMetaData.getColumnCount();

	while ( rs.next() )
	{
		Map<String,Object> rowResult = new LinkedHashMap<String,Object>();
		// 1 based for M$ SQL Server
		for ( int columnIndex = 1; columnIndex <= columnCount; columnIndex++ )
		{
			String name = rsMetaData.getColumnName( columnIndex );
			rowResult.put( name, rs.getObject( columnIndex ) );
		}
		
		reportDisplayBeans.add( rowResult );
	}

	return reportDisplayBeans;
}
}





}
