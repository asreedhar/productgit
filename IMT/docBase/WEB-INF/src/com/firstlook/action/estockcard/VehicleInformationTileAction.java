package com.firstlook.action.estockcard;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.ColorUtility;
import biz.firstlook.transact.persist.model.DealerPreferenceWindowSticker;
import biz.firstlook.transact.persist.persistence.IDealerPreferenceDAO;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.SessionHelper;
import com.firstlook.util.InteriorTypeUtility;
import com.firstlook.entity.Member;
import com.lowagie.text.pdf.events.IndexEvents.Entry;

public class VehicleInformationTileAction extends SecureBaseAction
{

private InventoryInformationService inventoryInformationService;
private IDealerPreferenceDAO dealerPrefDAO;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	// TODO Auto-generated method stub
	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	Integer inventoryId = new Integer( RequestHelper.getInt( request, "inventoryId" ) );
	
	Member member = getFirstlookSessionFromRequest( request ).getMember();
	
	String userName = member.getLogin();
	String userFirstName = member.getFirstName();
	String userLastName = member.getLastName();
	
	Map<String, Object> inventoryInformationDisplayBean = getInventoryInformationService().getInventoryInformationForInfoTile( currentDealerId, inventoryId, userName, userFirstName, userLastName);

	String make =  inventoryInformationDisplayBean.get("make").toString();
	
	// added becuase this action is kicked off ona successful ajax save, this flag displays the saved successfully message
	request.setAttribute( "success", RequestHelper.getBoolean( request, "saved" ) );
	request.setAttribute( "weeks", Integer.valueOf( RequestHelper.getInt( request, "weeks" ) ) );
	request.setAttribute( "InventoryDisplayBean", inventoryInformationDisplayBean );
	request.setAttribute( "inventoryId", inventoryId );
	request.setAttribute( "make",make);
	
	Map<String, Object> vehicleInformation = (Map<String,Object>)inventoryInformationDisplayBean.get( InventoryInformationService.IIDB_VEHICLE_INFORMATION );
	String baseColor = ColorUtility.formatColor( (String)vehicleInformation.get( InventoryInformationService.VI_BASE_COLOR ) );
	List< String > colors = prependNonStandardColor( baseColor );
	request.setAttribute( "colors", colors );
	request.setAttribute( "interiorTypes", InteriorTypeUtility.retrieveInteriorTypesForDropdowns() );
	
	Map<String,String> certificates = getInventoryInformationService().getCertificates(currentDealerId, inventoryId);
	  request.setAttribute( "certificates", certificates);
	  Integer selectedCertificates;
		 String selectedValue = "";
		 String certifiedChecker ;
		 boolean visibility = false;
		 try {
		  selectedCertificates = getInventoryInformationService().getselectedCertificates(inventoryId);
		  
		  for (Map.Entry<String, String> entry : certificates.entrySet())
		  {
		   String keyString = entry.getKey().substring(0, entry.getKey().indexOf("|"));
		   if(keyString.equals(selectedCertificates.toString()))
		   {
		    selectedValue = entry.getKey();
		    certifiedChecker = entry.getKey().substring(entry.getKey().indexOf(":"));
			   if (certifiedChecker.equals(":1")){
				   visibility = true; 
					}
			   int certifiedId = getInventoryInformationService().getCertifiedId(inventoryId);
				if (Integer.toString(certifiedId) != null && Integer.toString(certifiedId) != "" ){
					request.setAttribute("certifiedId",certifiedId);			
			   }
				else{
					request.setAttribute("certifiedId","");
				}
		   }
			   
			   
		   request.setAttribute("visibility",visibility);
		   
		  }
		  
		  request.setAttribute( "selectedValue", selectedValue);
		  
		  
		 
		 
		 }
		 catch (SQLException e) {
		  e.printStackTrace();
		 }
	
	
	 request.setAttribute("selectedCertificate2", certificates.get(selectedValue));
	 
		int count = 0;
		String cert = "";
		String certKey = "";
		
		for (Map.Entry<String, String> entry : certificates.entrySet())
		{
			if(entry.getKey().contains("MFR")){
			count++;
			cert = entry.getValue();
			certKey = entry.getKey();
			certifiedChecker = entry.getKey().substring(entry.getKey().indexOf(":"));
			   if (certifiedChecker.equals(":1")){
				   visibility = true;
				   try {
				   int certifiedId = getInventoryInformationService().getCertifiedId(inventoryId);
					if (Integer.toString(certifiedId) != null && Integer.toString(certifiedId) != "" ){
						if  (certifiedId == 0 || certifiedId == -1 ){
							request.setAttribute("certifiedId","");
						}else{
							request.setAttribute("certifiedId",certifiedId);	
						}
						
						
					}
					else
					{ 
						request.setAttribute("certifiedId","");	
						request.setAttribute("visibility",visibility);
						}
			}
			 
			   catch (SQLException e) {
					  e.printStackTrace();
					 }
			
		}
		}
		}
		
		
		if(count == 1 && selectedValue != null && selectedValue.contains("CERT")){
			request.setAttribute("selectedCertificate2", cert );
			  request.setAttribute( "selectedValue", certKey);
			  request.setAttribute( "visibility", true);
		}
	 
	boolean apppraisalLockoutEnabled = getFirstlookSessionFromRequest( request ).isIncludeAppraisalLockout();
			
	
	
	
	DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerId( currentDealerId );
	if (dealerGroup.isLithiaStore() )
	{
		apppraisalLockoutEnabled = true;
	}
	
	Dealer currentDealer = getImtDealerService().retrieveDealer( currentDealerId );
	
	Map<String, Object> inventoryInformation = (Map<String,Object>)inventoryInformationDisplayBean.get( InventoryInformationService.IIDB_INVENTORY_INFORMATION);
	
	
	DealerPreferenceWindowSticker stickerPrefs = dealerPrefDAO.findWindowStickerPrefs(currentDealerId);
	
	
	
	request.setAttribute( "hasTransferPricing", dealerGroup.getDealerGroupPreference().isIncludeTransferPricing());
	request.setAttribute( "bookoutLockoutEnabled", apppraisalLockoutEnabled );
	request.setAttribute( "isLithiaStore", new Boolean( dealerGroup.isLithiaStore() ) );
	request.setAttribute( "windowStickerTemplate", stickerPrefs.getWindowStickerTemplateId());
	request.setAttribute( "displayTMV", getFirstlookSessionFromRequest(request).hasEdmundsTmvUpgrade());
	request.setAttribute( "displayPing", getFirstlookSessionFromRequest( request ).isIncludePing() );
	request.setAttribute( "displayPingII", getFirstlookSessionFromRequest( request ).isIncludePingII() && (Boolean) inventoryInformation.get("InventoryActive"));
	request.setAttribute( "zipcode", currentDealer.getZipcode() );
	request.setAttribute( "isActive", inventoryInformationDisplayBean.get("isActive") );
	SessionHelper.keepAttribute( request, "weeks" );
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );

	return mapping.findForward( "success" );
}

private List< String > prependNonStandardColor( String baseColor )
{
	List<String> colors = new ArrayList<String>();
	//don't clobber colors that come in from the dms!
	// the FE drop down on eStock will contain non-standard colors.
	if( !ColorUtility.retrieveStandardColors().contains( baseColor ) ) {
		colors.add( baseColor );
	}
	colors.addAll( ColorUtility.retrieveStandardColors() );
	return colors;
}

public InventoryInformationService getInventoryInformationService()
{
	return inventoryInformationService;
}

public void setInventoryInformationService( InventoryInformationService inventoryInformationHelper )
{
	this.inventoryInformationService = inventoryInformationHelper;
}

public void setDealerPrefDAO(IDealerPreferenceDAO dealerPrefDAO) {
	this.dealerPrefDAO = dealerPrefDAO;
}

}
