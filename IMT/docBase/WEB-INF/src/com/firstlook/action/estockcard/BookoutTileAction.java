 package com.firstlook.action.estockcard;

import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.vehicle.description.old.BookOutDatasetInfo;
import biz.firstlook.module.vehicle.description.old.BookOutError;
import biz.firstlook.module.vehicle.description.old.BookOutTypeEnum;
import biz.firstlook.module.vehicle.description.old.GBException;
import biz.firstlook.module.vehicle.description.old.GuideBookInput;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.transact.persist.model.BookOutSource;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.Inventory;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;

import com.firstlook.action.SecureTileAction;
import com.firstlook.action.dealer.vehicledetailpage.BookoutDetailForm;
import com.firstlook.data.DatabaseException;
import com.firstlook.display.tradeanalyzer.DisplayGuideBook;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.RequestHelper;
import com.firstlook.helper.SessionHelper;
import com.firstlook.service.bookout.BookOutService;
import com.firstlook.service.bookout.appraisal.AppraisalBookOutService;
import com.firstlook.service.bookout.bean.AbstractBookOutState;
import com.firstlook.service.bookout.bean.StoredBookOutState;
import com.firstlook.service.bookout.inventory.InventoryBookOutService;
import com.firstlook.service.inventory.VehicleInventoryService;

public class BookoutTileAction extends SecureTileAction
{

// TODO: Refactor this to use only the bookOutService once the data model has
// been changed - NK, MH, 11/09/2005
private VehicleInventoryService vehicleInventoryService;
private InventoryBookOutService inventoryBookOutService;
private AppraisalBookOutService appraisalBookOutService;
private BookOutService bookOutService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	Integer guideBookId = new Integer( RequestHelper.getInt( request, "guideBookId" ) );
	String vin = RequestHelper.getString( request, "vin" );
	Integer inventoryId = new Integer( RequestHelper.getInt( request, "inventoryId" ) );
	Integer mileage = new Integer( RequestHelper.getInt( request, "inventoryMileage" ) );
	Integer bookOutSourceId = new Integer( RequestHelper.getInt( request, "bookOutSourceId" ) );
	Boolean refreshBookData = false; 
	String selectedModelKey="";
	if (request.getParameter("refreshBookData") != null) {
		refreshBookData = RequestHelper.getBoolean( request, "refreshBookData" ) ;
	}
	if (request.getParameter("selectedModelKey")!=null){
		selectedModelKey= request.getParameter("selectedModelKey");
	}

	Boolean isActive = (Boolean)request.getAttribute( "isActive" );
	if ( isActive == null )
	{
		if ( RequestHelper.getBoolean( request, "isActive" ) )
		{
			isActive = Boolean.TRUE;
		}
		else
		{
			isActive = Boolean.FALSE;
		}
	}

	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	Dealer dealer = getImtDealerService().retrieveDealer( currentDealerId );
	DealerPreference dealerPref = dealer.getDealerPreference();
	
	
	DealerGroup dealerGroup = getDealerGroupService().retrieveByDealerId( currentDealerId );
	request.setAttribute( "isLithiaStore", dealerGroup.isLithiaStore() );
	
	
	Inventory inventory = vehicleInventoryService.getInventoryDAO().findBy( inventoryId );
	
	GuideBookInput input = BookOutService.createGuideBookInput( vin, mileage, currentDealerId, dealer.getState(),
																				dealerPref.getNadaRegionCode(),
																				getMemberFromRequest( request ).getMemberId() );

	input.setSelectedModelId(selectedModelKey);
	BookoutDetailForm bookoutForm = new BookoutDetailForm();

	populateDisplayGuideBooks( request, vin, inventoryId, mileage, guideBookId, input, bookoutForm, bookOutSourceId, dealerPref.getSearchInactiveInventoryDaysBackThreshold(), refreshBookData );

	Collection<DisplayGuideBook> books = bookoutForm.getDisplayGuideBooks();
	boolean success = true;
	if ( books.iterator().hasNext() )
	{
		DisplayGuideBook dgb = books.iterator().next();
		success = dgb.isSuccess();
	}
	
	request.setAttribute( "success", success );
	request.setAttribute( "isActive", isActive );
	request.setAttribute( "vin", vin );
	request.setAttribute( "inventoryId", inventoryId );
	request.setAttribute( "bookOutSourceId", bookOutSourceId );
	request.setAttribute( "guideBookId", guideBookId );
	request.setAttribute( "inventoryMileage", mileage );
	request.setAttribute( "bookOutWasSaved", new Boolean( false ) );
	request.setAttribute( "primaryGuideBookId", dealerPref.getGuideBookId() );
	request.setAttribute( "locked", inventory.getBookoutLocked() );
	request.setAttribute( "inventoryId", new Integer(inventoryId ) );
	
	boolean apppraisalLockoutEnabled = getFirstlookSessionFromRequest( request ).isIncludeAppraisalLockout();
	if (dealerGroup.isLithiaStore() )
	{
		apppraisalLockoutEnabled = true;
	}
	request.setAttribute( "bookoutLockoutEnabled", apppraisalLockoutEnabled );

	SessionHelper.setAttribute( request, "bookoutDetailForm", bookoutForm );
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );

	if ( RequestHelper.getBoolean( request, "editOptions" ) )
	{
		return mapping.findForward( "editOptions" );
	}

	return mapping.findForward( "success" );
}

/**
 * nk - 01/07/08
 * ForceBookOutFromVin : This was added as a temporary patch a legacy issue. Previously, we stored ALL Third Party Vehicles associated with 
 * 		a Bookout, i.e. all the ThirdPartyVehicles that are possible given the VIN (these are the values in the book drop downs on TA). We then
 * 		RE-USED these saved ThirdPartyVehicle values every time a user came back to rebook the vehicle on the eStock. This was found to cause 
 * 		serious errors when book Third Party Vehicle data changed.
 * 		This new ForceBookOutFromVin flag was introduced to force the Bookout to ignore any saved TPV values and to instead retrieve the newest
 * 		and most correct values directly from the book with the VIN.
 * 
 * @param request
 * @param vin
 * @param inventoryId
 * @param mileage
 * @param thirdPartyId
 * @param input
 * @param bookoutForm
 * @param bookOutSourceId
 * @param searchAppraisalDaysBackThreshold
 * @param forceBookoutFromVin
 * @return
 */
public void populateDisplayGuideBooks( HttpServletRequest request, String vin, Integer inventoryId, Integer mileage, Integer thirdPartyId,
										GuideBookInput input, BookoutDetailForm bookoutForm, Integer bookOutSourceId, int searchAppraisalDaysBackThreshold,
										Boolean forceBookoutFromVin)
{
	bookoutForm.setVin( vin );
	bookoutForm.setMileage( mileage );
	ActionErrors guideBookErrors = new ActionErrors();

	try
	{
		AbstractBookOutState bookOutState = constructBookOutState( inventoryId, input, bookOutSourceId, thirdPartyId, searchAppraisalDaysBackThreshold, forceBookoutFromVin );

		constructDisplayGuideBooks( request, bookoutForm, guideBookErrors, bookOutState, thirdPartyId );
		
	}
	catch ( Exception e )
	{
		DisplayGuideBook guideBook = new DisplayGuideBook();
		guideBook.setSuccess( false );
		Throwable t = e.getCause();
		guideBook.setSuccessMessage( t == null ? e.getMessage() : t.getMessage() );
		guideBook.setValidVin( false );
		guideBook.setGuideBookName( ThirdPartyDataProvider.getThirdPartyDataProviderDescription( thirdPartyId.intValue() ) );
		bookoutForm.addDisplayGuideBook( guideBook );
	}

}

private void constructDisplayGuideBooks( HttpServletRequest request, BookoutDetailForm bookoutForm,
										ActionErrors guideBookErrors, AbstractBookOutState bookOutState,
										Integer thirdPartyBookoutServiceId )
{
	DisplayGuideBook displayGuideBook;
	String selectedModelKey="";
	if ( bookOutState.getBookOutErrors().get( new Integer( BookOutError.INVALID_VIN_BAD_FORMAT ) ) != null )
	{
		guideBookErrors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.dealer.tools.vehiclelocatorsearch.vin" ) );
		request.setAttribute( "guideBookError", new Boolean( true ) );
		putActionErrorsInRequest( request, guideBookErrors );
	}
	else if ( bookOutState.getBookOutErrors().get( new Integer( BookOutError.INVALID_VIN_NOT_IN_GUIDEBOOK ) ) != null )
	{
		DisplayGuideBook invalidVinGuideBook = new DisplayGuideBook();
		bookOutState.transformBookOutToDisplayData( invalidVinGuideBook );
		bookoutForm.addDisplayGuideBook( invalidVinGuideBook );
		request.setAttribute( "displayGuideBook", invalidVinGuideBook );
	}
	else
	{
		displayGuideBook = new DisplayGuideBook();
		bookOutState.transformBookOutToDisplayData( displayGuideBook );

		List<VDP_GuideBookValue> mileageAdjustedValues = getInventoryBookOutService().constructMileageAdjustedValues( displayGuideBook );
		request.setAttribute( "mileageAdjustedValues", mileageAdjustedValues );

		// this compares the mileage from the page with the mileage that was used for the bookout
		// if they do not match, the user will get an alert informing them
		if ( bookOutState instanceof StoredBookOutState )
		{
			StoredBookOutState storedBookOutState = (StoredBookOutState)bookOutState;
			if ( !storedBookOutState.getBookoutMileage().equals( bookoutForm.getMileage() ) )
			{
				displayGuideBook.setInAccurate( true );
			}
		}

		if(thirdPartyBookoutServiceId == 3){
			if (request.getParameter("selectedModelKey")!=null){
				selectedModelKey= request.getParameter("selectedModelKey");
				displayGuideBook.setSelectedModelKey(selectedModelKey);
			}
		}
		
		request.setAttribute( "displayGuideBook", displayGuideBook );
		bookoutForm.addDisplayGuideBook( displayGuideBook );
	}
}

private AbstractBookOutState constructBookOutState( Integer inventoryId, GuideBookInput input, Integer bookOutSourceId,
													Integer thirdPartyId, int searchAppraisalDaysBackThreshold, Boolean forceBookoutFromVin )
		throws GBException
{
	AbstractBookOutState bookOutState = null;
	if ( bookOutSourceId.intValue() == BookOutSource.BOOK_OUT_SOURCE_VEHICLE_DETAIL.intValue() )
	{
		bookOutState = getInventoryBookOutService().retrieveExistingBookOutInfoOrBeginNewBookOut( input, thirdPartyId, inventoryId, searchAppraisalDaysBackThreshold, forceBookoutFromVin );
	}
	else if ( bookOutSourceId.intValue() == BookOutSource.BOOK_OUT_SOURCE_APPRAISAL.intValue() )
	{
		bookOutState = getAppraisalBookOutService().retrieveExistingBookOutInfoOrBeginNewBookOut( input, thirdPartyId, searchAppraisalDaysBackThreshold, null );
	}
	else
	{
		BookOutTypeEnum bookoutType = (bookOutSourceId.intValue() == BookOutSource.BOOK_OUT_SOURCE_APPRAISAL.intValue() ) ? BookOutTypeEnum.APPRAISAL : BookOutTypeEnum.INVENTORY;
		bookOutState = getBookOutService().doVinLookup( input, thirdPartyId, new BookOutDatasetInfo( bookoutType, input.getBusinessUnitId()), null );
	}
	return bookOutState;
}

public InventoryBookOutService getInventoryBookOutService()
{
	return inventoryBookOutService;
}

public void setInventoryBookOutService( InventoryBookOutService inventoryBookOutService )
{
	this.inventoryBookOutService = inventoryBookOutService;
}

public AppraisalBookOutService getAppraisalBookOutService()
{
	return appraisalBookOutService;
}

public void setAppraisalBookOutService( AppraisalBookOutService appraisalBookOutService )
{
	this.appraisalBookOutService = appraisalBookOutService;
}

public BookOutService getBookOutService()
{
	return bookOutService;
}

public void setBookOutService( BookOutService bookOutService )
{
	this.bookOutService = bookOutService;
}

public VehicleInventoryService getVehicleInventoryService()
{
	return vehicleInventoryService;
}

public void setVehicleInventoryService( VehicleInventoryService vehicleInventoryService )
{
	this.vehicleInventoryService = vehicleInventoryService;
}

}
