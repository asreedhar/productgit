package com.firstlook.action.estockcard;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.Functions;
import biz.firstlook.transact.persist.model.Inventory;
import biz.firstlook.transact.persist.model.TransferPriceValidation;
import biz.firstlook.transact.persist.persistence.InventoryDAO;
import biz.firstlook.transact.persist.persistence.InventoryTransferPriceStoredProcedure;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;

public class TransferPriceConfirmAction extends SecureBaseAction {

	private InventoryDAO inventoryDAO;
	private InventoryTransferPriceStoredProcedure inventoryTransferPriceStoredProcedure;
	
	@Override
	public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
			throws DatabaseException, ApplicationException {
		Float newPrice = Functions.nullSafeFloat(request.getParameter("transferPrice"));
		Integer inventoryId = Functions.nullSafeInteger(request.getParameter("inventoryId" ));
		int businessUnitId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();

		Inventory inventory = inventoryDAO.findBy(inventoryId);
		if(inventory.getDealerId() != businessUnitId) {
			throw new ApplicationException("Access violation occurred, Inventory item does not belong to businessunit!");
		}
		
		TransferPriceValidation validation = inventoryTransferPriceStoredProcedure.validate(inventory, newPrice);
		if(!validation.isPassed())
			request.setAttribute( "errorMessage", validation.getMessage() );
		
		response.addHeader( "confirm", Boolean.valueOf(!validation.isPassed()).toString() );

		return mapping.findForward( "success" );
	}

	public void setInventoryDAO(InventoryDAO inventoryDAO) {
		this.inventoryDAO = inventoryDAO;
	}

	public void setInventoryTransferPriceStoredProcedure(
			InventoryTransferPriceStoredProcedure inventoryTransferPriceStoredProcedure) {
		this.inventoryTransferPriceStoredProcedure = inventoryTransferPriceStoredProcedure;
	}
}
