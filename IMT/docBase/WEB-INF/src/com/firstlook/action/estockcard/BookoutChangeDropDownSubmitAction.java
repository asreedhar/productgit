package com.firstlook.action.estockcard;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.module.vehicle.description.old.BookOutDatasetInfo;
import biz.firstlook.module.vehicle.description.old.BookOutTypeEnum;
import biz.firstlook.module.vehicle.description.old.GBException;
import biz.firstlook.module.vehicle.description.old.GuideBookInput;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookValue;
import biz.firstlook.module.vehicle.description.old.VDP_GuideBookVehicle;
import biz.firstlook.transact.persist.model.BookOutSource;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;
import biz.firstlook.transact.persist.persistence.DealerPreferenceDAO;
import biz.firstlook.transact.persist.service.ThirdPartyVehicleOptionService;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.action.dealer.vehicledetailpage.BookoutDetailForm;
import com.firstlook.data.DatabaseException;
import com.firstlook.display.tradeanalyzer.DisplayGuideBook;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;
import com.firstlook.service.bookout.BookOutService;
import com.firstlook.service.bookout.bean.SelectedVehicleAndOptionsBookOutState;

public class BookoutChangeDropDownSubmitAction extends SecureBaseAction
{

private DealerPreferenceDAO dealerPrefDAO;
private BookOutService bookOutService;

public BookOutService getBookOutService()
{
	return bookOutService;
}

public void setBookOutService( BookOutService bookOutService )
{
	this.bookOutService = bookOutService;
}

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
throws DatabaseException, ApplicationException
{
	Dealer dealer = putDealerFormInRequest( request );
	
	int guideBookId;
	
	Integer inventoryId = null;
	try {
		 inventoryId = Integer.valueOf(request.getParameter( "inventoryId" ));
	} catch (NumberFormatException e) {
		throw new ApplicationException(MessageFormat.format("Expected inventoryId but found {0} instead.", request.getParameter( "inventoryId" )));
	}
	
	BookoutDetailForm bookoutForm  = (BookoutDetailForm)SessionHelper.getAttribute( request, "bookoutDetailForm");
	
	Integer bookoutSourceId = new Integer( request.getParameter( "bookOutSourceId" ) );
	BookOutTypeEnum bookoutType = (bookoutSourceId.intValue() == BookOutSource.BOOK_OUT_SOURCE_APPRAISAL.intValue()) ? BookOutTypeEnum.APPRAISAL : BookOutTypeEnum.INVENTORY;
	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	DealerPreference dealerPreference = dealerPrefDAO.findByBusinessUnitId( currentDealerId );
	
	Member member = getMemberFromRequest( request );
	GuideBookInput input = new GuideBookInput();
	input.setVin( bookoutForm.getVin() );
	input.setBusinessUnitId( currentDealerId );
	input.setMileage( bookoutForm.getMileage() );
	input.setState( dealer.getState() );
	input.setMemberId( member.getMemberId() );
	input.setNadaRegionCode( dealerPreference.getNadaRegionCode() );
	
	guideBookId = getGuideBookIdFromRequest( request );
	String selectedKey = (String)request.getParameter( "selectedKey" );
	DisplayGuideBook guideBook = bookoutForm.findDisplayGuideBookByGuideBookId( guideBookId );
	
	if(guideBook != null) {
		guideBook.setSelectedVehicleKey( selectedKey );
		VDP_GuideBookVehicle guideBookVehicle = guideBook.determineSelectedVehicle( selectedKey );
		
		if ( !selectedKey.equals( "0" ) )
		{
	
			SelectedVehicleAndOptionsBookOutState state;
			
			if ( guideBookVehicle.getGuideBookOptions() != null && !guideBookVehicle.getGuideBookOptions().isEmpty() )
			{
				state = new SelectedVehicleAndOptionsBookOutState();
				state.setSelectedVehicleKey( selectedKey );
				state.setThirdPartyVehicleOptions( guideBookVehicle.getGuideBookOptions() );
				state.setVehicleYear( guideBookVehicle.getYear() );
			}
			else
			{
				try{
					state = getBookOutService().lookUpOptionsForSingleVehicle( input, new Integer(guideBookId), guideBookVehicle, new BookOutDatasetInfo( bookoutType, currentDealerId)  );
				}
				catch (GBException gbe) {
					throw new ApplicationException(gbe);
				}
			}
			state.transformBookOutToDisplayData( guideBook );
		}
		// No Selection.  "Please select a model..."
		else
		{
			// No model selection.
			// Should not display any options.
			guideBook.setDisplayGuideBookOptions(Collections.<ThirdPartyVehicleOption>emptyList());
		}
		guideBook.setDisplayGuideBookValues( Collections.<VDP_GuideBookValue>emptyList() );
	}
	
	// this makes sure the standard options are still selected
	Collection<DisplayGuideBook> guideBooks = bookoutForm.getDisplayGuideBooks();
	if(guideBooks != null) {
		Iterator<DisplayGuideBook> guideBookIter = guideBooks.iterator();
		while (guideBookIter.hasNext() ) {
			//wow, bad code! not sure what would happen if i changed this to a loop local variable. -bf.
			guideBook = guideBookIter.next();
		
			guideBook.setSelectedEquipmentOptionKeys(
				ThirdPartyVehicleOptionService.setAndUpdateSelectedOptionsUsingArray( guideBook.getDisplayEquipmentGuideBookOptions(),
																		guideBook.getSelectedEquipmentOptionKeys() ));
		}
		
		guideBook.setHasErrors( Boolean.FALSE );
	}
	
	
	request.setAttribute( "primaryGuideBookId", dealerPreference.getGuideBookId() );
	request.setAttribute( "selectedKey", selectedKey );
	request.setAttribute( "bookOutWasSaved", Boolean.FALSE );
	request.setAttribute( "isActive", Boolean.TRUE );
	request.setAttribute( "inventoryMileage", request.getParameter("inventoryMileage"));
	request.setAttribute( "displayGuideBook", guideBook );
	request.setAttribute( "guideBookId", guideBook.getGuideBookId() );
	request.setAttribute( "inventoryId", inventoryId );
	request.setAttribute( "bookOutSourceId", bookoutSourceId );
	request.setAttribute( "vin", request.getParameter( "vin" ) );
	
	SessionHelper.setAttribute( request, "bookoutDetailForm", bookoutForm );
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );
	
	SessionHelper.keepAttribute( request, "vehicleInformationForm" );
	SessionHelper.keepAttribute( request, "auctionForm" );
	return mapping.findForward( "success" );
}

private int getGuideBookIdFromRequest( HttpServletRequest request ) throws ApplicationException
{
	Integer guideBookId = null;
	String guideBookIdParam = request.getParameter("guideBookId");
	if(guideBookIdParam != null) {
		guideBookId = Integer.parseInt(guideBookIdParam.trim());
	} else {
		//replicate behavior of RequestHelper.java
		Object guideBookIdAttr = request.getAttribute("guideBookId");
		if(guideBookIdAttr != null) {
			guideBookId = Integer.parseInt(guideBookIdAttr.toString().trim());
		} else {
			guideBookId = Integer.valueOf(0);
		}
	}
	
	return guideBookId;
}

public DealerPreferenceDAO getDealerPrefDAO()
{
	return dealerPrefDAO;
}

public void setDealerPrefDAO( DealerPreferenceDAO dealerPrefDAO )
{
	this.dealerPrefDAO = dealerPrefDAO;
}

}
