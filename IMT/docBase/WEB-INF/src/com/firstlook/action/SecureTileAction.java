package com.firstlook.action;

import java.net.InetAddress;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public abstract class SecureTileAction extends SecureBaseAction
{

protected final static Logger logger = Logger.getLogger( SecureTileAction.class );

public final ActionForward execute( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response ) throws Exception
{
	logger.debug( "Start " + this.getClass().getSimpleName() );
	long requestStart = System.currentTimeMillis();

	String sessionId = getSessionId( request );

	ActionForward forward = mapping.findForward( "login" );
	try
	{
		forward = justDoIt( mapping, form, request, response );
	}
	catch ( Throwable th )
	{
		request.setAttribute( "UncaughtException", th );
		logger.error( th.toString() );
		forward = mapping.findForward( "error" );
	}
	
	if ( logger.isDebugEnabled() )
	{
		long responseStart = System.currentTimeMillis();
		StringBuilder msg = new StringBuilder();
		msg.append( this.getClass().getSimpleName() ).append( "," ).append( ( responseStart - requestStart ) );
		if ( getFirstlookSessionFromRequest( request ) != null )
		{
			if ( getFirstlookSessionFromRequest( request ).getMember() != null )
			{
				msg.append( " memberId: " ).append( getFirstlookSessionFromRequest( request ).getMember().getMemberId() );
			}
		}
		logger.debug( msg.append( " HttpSessionID: " ).append( sessionId ).append( " on " ).append(InetAddress.getLocalHost() ).toString() );
	}
	
	if (request.getServletPath().length() == 0) {
		request.setAttribute( "actionPath", "/*" );
	}
	else {
		request.setAttribute( "actionPath", request.getServletPath().substring( 1 ) );
	}

	addNoCacheHeader( response );
	
	return forward;
}
}
