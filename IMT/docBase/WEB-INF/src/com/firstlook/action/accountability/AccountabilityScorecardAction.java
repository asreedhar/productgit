package com.firstlook.action.accountability;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.dealer.IDealerService;

public class AccountabilityScorecardAction extends SecureBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
        throws DatabaseException, ApplicationException
{
	IDealerService dealerService = getImtDealerService();
    Dealer dealer = dealerService.retrieveDealer( getFirstlookSessionFromRequest( request ).getCurrentDealerId() );
    request.setAttribute( "dealer", dealer );

    Date now = new Date();
    request.setAttribute( "now", now );

    ActionForward forward = mapping.findForward( "success" );
    return forward;
}

}
