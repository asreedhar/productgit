package com.firstlook.action.accountability;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.service.accountability.reports.EdgeScorecardReport;

public class WebComponentAction extends ComponentAction {

    public WebComponentAction() {
        super();
    }

    public ActionForward createReport( String component, EdgeScorecardReport report, ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response ) {
        request.setAttribute("report", report);
        return mapping.findForward("success");
    }

}
