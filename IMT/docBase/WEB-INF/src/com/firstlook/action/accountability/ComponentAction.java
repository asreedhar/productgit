package com.firstlook.action.accountability;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.accountability.EdgeScorecardService;
import com.firstlook.service.accountability.reports.EdgeScorecardReport;
import com.firstlook.session.FirstlookSession;

public abstract class ComponentAction extends SecureBaseAction
{

protected Configuration config;

private EdgeScorecardService service;

public ComponentAction()
{
	super();
}

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	String component = getComponent( mapping );

	int dealerId = ( (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION ) ).getCurrentDealerId();
	EdgeScorecardReport report = service.calculate( dealerId );

	ActionForward forward = createReport( component, report, mapping, form, request, response );
	return forward;
}

private String getComponent( ActionMapping mapping )
{
	String pathTranslated = mapping.getPath();
	String[] tokens = StringUtils.split( pathTranslated, "/" );
	String component = tokens[0];
	return component;
}

public abstract ActionForward createReport( String component, EdgeScorecardReport report, ActionMapping mapping, ActionForm form,
											HttpServletRequest request, HttpServletResponse response ) throws ApplicationException;

public EdgeScorecardService getService()
{
	return service;
}

public void setService( EdgeScorecardService service )
{
	this.service = service;
}

}
