package com.firstlook.action.user.member;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.services.vehiclehistoryreport.CarfaxService;
import biz.firstlook.transact.persist.model.CIAPreferences;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.JobTitle;
import biz.firstlook.transact.persist.persistence.IDealerPreferenceDAO;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.InventoryStatusCD;
import com.firstlook.entity.Member;
import com.firstlook.entity.MemberToInventoryStatusFilterCode;
import com.firstlook.entity.form.MemberForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;
import com.firstlook.service.dealer.IUCBPPreferenceService;
import com.firstlook.service.inventory.InventoryStatusCDService;
import com.firstlook.service.member.MemberToInventoryStatusFilterCodeService;

public class EditMemberProfileAction extends SecureBaseAction
{
	
private IDealerPreferenceDAO dealerPrefDAO;
private IUCBPPreferenceService ucbpPreferenceService;
private SubscriptionDisplayService subscriptionDisplayService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws ApplicationException, DatabaseException
{
	Member member = getMemberFromRequest( request );
	MemberForm memberForm = new MemberForm( member );

	subscriptionDisplayService.setSubscriptionDataOnMemberForm( request, memberForm, member.getMemberId(), Member.MEMBER_TYPE_USER );
	MemberToInventoryStatusFilterCodeService inventoryStatusFilterCodeService = new MemberToInventoryStatusFilterCodeService();
	List<MemberToInventoryStatusFilterCode> filterMappings = inventoryStatusFilterCodeService.retrieveMemberToInvStatusFilterMappings( member.getMemberId() );
	memberForm.setInventoryStatusCDPresets( inventoryStatusFilterCodeService.constructFilterMappingIds( filterMappings ) );
	
	int dealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	//0 because of int, but usually get null if member hasn't entered a dealership yet.
	if(dealerId != 0) {
		addUCBPPreferenceToRequest( request, dealerId );
		addDealerPreferenceToRequest( request, dealerId );
		addInventoryStatusListToRequest( request );
		
		request.setAttribute( "jobTitles", getJobTitleList( dealerId ) );
		
		memberForm.setCanUseDealerCarfaxCredentials(
				CarfaxService.getInstance().canPurchaseReport(
						dealerId,
						member.getLogin()));
	}
	else {
		request.setAttribute("disableDealerFields", Boolean.TRUE);
	}
	
	request.setAttribute("hasCarfaxDealerCredentials", CarfaxService.getInstance().hasAccount(
			dealerId,
			member.getLogin()));
	
	SessionHelper.setAttribute( request, "memberForm", memberForm );
	SessionHelper.keepAttribute( request, "memberForm" );
	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );
	request.setAttribute( "memberForm", memberForm );

	return mapping.findForward( "form" );
}

private List< JobTitle > getJobTitleList( int dealerId )
{
	DealerGroup dg = getDealerGroupService().retrieveByDealerId( dealerId );
	List<JobTitle> jobTitles = getMemberService().allJobTitles();
	if( !dg.isLithiaStore() ) {
		List<JobTitle> lithiaOnlyJobs = new ArrayList<JobTitle>();
		for ( JobTitle jobTitle : jobTitles )
		{
			if( JobTitle.isLithiaOnlyJobTitle( jobTitle) ) {
				lithiaOnlyJobs.add( jobTitle );
			}
		}
		jobTitles.removeAll( lithiaOnlyJobs );
	}
	return jobTitles;
}

private void addUCBPPreferenceToRequest( HttpServletRequest request, int dealerId )
{
	// TODO: Retrieve target days supply add object directly to request
	// NOTE: this avoids mangling the member form as it is directly tied to the
	// member
	// object in a way difficult to alter without difficulty
	try {
		CIAPreferences ciaPreference = ucbpPreferenceService.retrieveCIAPreferences( dealerId );
		request.setAttribute( "targetDaysSupply", Integer.valueOf( ciaPreference.getTargetDaysSupply() ) );
	} catch(Exception e) {
		//default
		request.setAttribute( "targetDaysSupply", 45 );
	}
}

private void addDealerPreferenceToRequest( HttpServletRequest request, int dealerId )
{
	try {
		DealerPreference dealerPreference = dealerPrefDAO.findByBusinessUnitId( dealerId );
		request.setAttribute( "showLotLocationAndStatus", dealerPreference.isShowLotLocationStatus() );
	} catch(Exception e) {
		//silently fail...
	}
}

private void addInventoryStatusListToRequest( HttpServletRequest request )
{
	InventoryStatusCDService inventoryStatusService = new InventoryStatusCDService();
	List<InventoryStatusCD> inventoryStatusList = inventoryStatusService.retrieveAll();
	request.setAttribute( "inventoryStatusList", inventoryStatusList );
}

public void setDealerPrefDAO( IDealerPreferenceDAO dealerPrefDAO )
{
	this.dealerPrefDAO = dealerPrefDAO;
}

public void setUcbpPreferenceService( IUCBPPreferenceService ucbpPreferenceService )
{
	this.ucbpPreferenceService = ucbpPreferenceService;
}

public void setSubscriptionDisplayService( SubscriptionDisplayService subscriptionDisplayService )
{
	this.subscriptionDisplayService = subscriptionDisplayService;
}

}
