package com.firstlook.action.user.member;

import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.model.CIAPreferences;
import biz.firstlook.transact.persist.model.Credential;
import biz.firstlook.transact.persist.model.CredentialType;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Member;
import com.firstlook.entity.MemberToInventoryStatusFilterCode;
import com.firstlook.entity.form.MemberForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;
import com.firstlook.service.dealer.IUCBPPreferenceService;
import com.firstlook.service.member.MemberServiceException;
import com.firstlook.service.member.MemberToInventoryStatusFilterCodeService;
import com.firstlook.session.FirstlookSession;

public class SaveMemberProfileAction extends SecureBaseAction
{

private IUCBPPreferenceService ucbpPreferenceService;
private SubscriptionDisplayService subscriptionDisplayService;

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws ApplicationException, DatabaseException
{
	MemberForm memberForm = (MemberForm)form;

	Member memberFromPage = memberForm.getMember();
	Member memberFromDB = getMemberService().retrieveMember( memberFromPage.getMemberId() );

	subscriptionDisplayService.updateMemberSubscriptions( memberForm.getSubscriptionData(), memberFromPage, Member.MEMBER_TYPE_USER );

	FirstlookSession firstlookSession = getFirstlookSessionFromRequest( request );
	
	insertMember( memberFromDB, memberForm, getMemberFromRequest( request ) );

	Boolean showLotLocationAndStatusFromRequest = Boolean.valueOf( request.getParameter( "showLotLocationAndStatus" ) );
	if ( showLotLocationAndStatusFromRequest.booleanValue() )
	{
		updateMemberToInventoryStatusCodeFilterMappings( memberForm, memberFromDB );
	}

	String targetDaysSupplyParam = request.getParameter( "targetDaysSupply" );
	int currentDealerId = firstlookSession.getCurrentDealerId();
	if(currentDealerId != 0 && targetDaysSupplyParam != null) {
		saveTargetDaysSupply( targetDaysSupplyParam, currentDealerId );
	}

	SessionHelper.setAttribute( request, "memberForm", memberForm );
	SessionHelper.keepAttribute( request, "memberForm" );
	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );

	request.setAttribute( "saved", "true" );

	ActionForward forward = mapping.findForward( "saved" );
	return forward;
}

private void updateMemberToInventoryStatusCodeFilterMappings( MemberForm memberForm, Member savedMember )
{
	MemberToInventoryStatusFilterCodeService inventoryStatusFilterCodeService = new MemberToInventoryStatusFilterCodeService();
	Set<MemberToInventoryStatusFilterCode> mappings = inventoryStatusFilterCodeService.constructInventoryStatusCodeMappings( savedMember, memberForm.getInventoryStatusCDPresets() );
	inventoryStatusFilterCodeService.deleteExistingAndSaveNewInventoryStatusFilterCodes( mappings, savedMember.getMemberId() );
}

private void saveTargetDaysSupply( String targetDaysSupplyParam, int currentDealerId )
{
	if ( targetDaysSupplyParam != null && !targetDaysSupplyParam.trim().equals( "" ) )
	{
		if ( StringUtils.isNumeric( targetDaysSupplyParam ) )
		{
			CIAPreferences ciaPreference = ucbpPreferenceService.retrieveCIAPreferences( currentDealerId );
			Integer targetDaysSupply = new Integer( targetDaysSupplyParam );
			ciaPreference.setTargetDaysSupply( targetDaysSupply.intValue() );
			ucbpPreferenceService.updateCIAPreference( ciaPreference );
		}
	}
}

void insertMember( Member memberFromDB, MemberForm memberForm, Member member ) throws MemberServiceException
{
	if( !memberForm.getPassword().equals( MemberForm.ENCODED_PASSWORD ))
		getMemberService().changePassword( memberFromDB, memberForm.getPassword(), memberForm.getPasswordConfirm() );

	defaultPreferredName( memberFromDB );
	setPhoneNumbers( memberFromDB, memberForm );
	memberFromDB.setSalutation( memberForm.getSalutation() );
	memberFromDB.setFirstName( memberForm.getFirstName() );
	memberFromDB.setPreferredFirstName( memberForm.getPreferredFirstName() );
	memberFromDB.setMiddleInitial( memberForm.getMiddleInitial() );
	memberFromDB.setLastName( memberForm.getLastName() );
	memberFromDB.setJobTitleId( new Integer( memberForm.getJobTitleId() ) );
	memberFromDB.setEmailAddress( memberForm.getEmailAddress() );
	memberFromDB.setSmsAddress( memberForm.getSmsAddress() );
	memberFromDB.setReportMethod( memberForm.getReportMethod() );
	memberFromDB.setInventoryOverviewSortOrderType( memberForm.getInventoryOverviewSortOrderType());
	
	// credentials are a composite key and require special logic bc of hibernate
	updateMemberCredentials( memberFromDB.getCredentials(), memberForm );

	// this call updates the user variable in session so credentials take effect immediatly
	member.setCredentials( memberFromDB.getCredentials() );

	getMemberService().saveOrUpdate( memberFromDB );
}


/**
 * @see AddMemberSubmitAction.  same code. -bf. feb 07, 2006
 * 
 * @param memberFromDB
 * @param memberForm
 */
private void updateMemberCredentials( Set<Credential> credentials, MemberForm memberForm)
{
	int memberId = memberForm.getMemberId();

	manageCredential(credentials, CredentialType.GMAC, memberId,
			memberForm.getGmSmartAuctionUsername(),
			memberForm.getGmSmartAuctionPassword());
}

private static void manageCredential(Set<Credential> credentials,
		CredentialType credentialType, int memberId, String userName,
		String password) {
	Credential credential;
	if (isValidCredential(credentialType, userName, password)) {
		credential = getCredential(credentials, credentialType);
		if (credential != null) {
			updateCredential(credential, userName, password);
		}
		else {
			credentials.add(createCredential(credentialType, userName, password, memberId));
		}
	}
	else {
		removeCredential(credentials, credentialType);
	}
}

private static void removeCredential(Set<Credential> credentials, CredentialType credentialType) {
	Iterator<Credential> it = credentials.iterator();
	while (it.hasNext()) {
		Credential credential = it.next();
		if (credential.getCredentialTypeId().equals(credentialType.getId())) {
			it.remove();
		}
	}
}

private static Credential createCredential(CredentialType credentialType, String userName, String password, int memberId) {
	Credential credential = new Credential();
	credential.setCredentialTypeId(credentialType.getId());
	credential.setUsername(userName);
	credential.setPassword(password);
	credential.setMemberId(memberId);
	return credential;
}

private static void updateCredential(Credential credential, String userName, String password) {
	credential.setUsername(userName);
	credential.setPassword(password);
}

private static Credential getCredential(Set<Credential> credentials, CredentialType credentialType) {
	for (Credential credential : credentials) {
		if (credential.getCredentialTypeId().equals(credentialType.getId())) {
			return credential;
		}
	}
	return null;
}

private static boolean isValidCredential(CredentialType credentialType, String userName, String password) {
	if (StringUtils.isNotBlank(userName)) {
		return (StringUtils.isNotBlank(password));
	}
	return false;
}

private void setPhoneNumbers( Member memberFromDB, MemberForm memberForm )
{	
	memberFromDB.setOfficePhoneNumber( memberForm.getOfficePhoneNumber() );
	memberFromDB.setOfficeFaxNumber( memberForm.getOfficeFaxNumber() );
	memberFromDB.setOfficePhoneExtension( memberForm.getOfficePhoneExtension() );
	memberFromDB.setMobilePhoneNumber( memberForm.getMobilePhoneNumber() );
	memberFromDB.setPagerNumber( memberForm.getPagerNumber() );
	
	//Case 217: have to synch the phone numbers on the Member object with the MemberForm because they have not been set with the new values up 
	//until this point. (i.e. the phone numbers on the MemberForm are different than it's member object because use of PhoneNumberHelper)    
	Member memberFromPage = memberForm.getMember();
	memberFromPage.setOfficePhoneNumber(memberForm.getOfficePhoneNumber());
	memberFromPage.setOfficeFaxNumber(memberForm.getOfficeFaxNumber());
	memberFromPage.setOfficePhoneExtension(memberForm.getOfficePhoneExtension());
	memberFromPage.setMobilePhoneNumber(memberForm.getMobilePhoneNumber());
	memberFromPage.setPagerNumber(memberForm.getPagerNumber());
}

private void defaultPreferredName( Member memberFromPage )
{
	if ( memberFromPage.getPreferredFirstName() == null || memberFromPage.getPreferredFirstName().equals( "" ) )
	{
		memberFromPage.setPreferredFirstName( memberFromPage.getFirstName() );
	}
}

void updateMemberPreferedFirstName( Member memberFromDB, Member memberFromPage )
{
	if ( ( memberFromPage.getPreferredFirstName() == null ) || memberFromPage.getPreferredFirstName().trim().equals( "" ) )
	{
		memberFromDB.setPreferredFirstName( memberFromPage.getFirstName() );
	}
	else
	{
		memberFromDB.setPreferredFirstName( memberFromPage.getPreferredFirstName() );
	}
}

public void setUcbpPreferenceService( IUCBPPreferenceService ucbpPreferenceService )
{
	this.ucbpPreferenceService = ucbpPreferenceService;
}

public SubscriptionDisplayService getSubscriptionDisplayService()
{
	return subscriptionDisplayService;
}

public void setSubscriptionDisplayService( SubscriptionDisplayService subscriptionDisplayService )
{
	this.subscriptionDisplayService = subscriptionDisplayService;
}

}
