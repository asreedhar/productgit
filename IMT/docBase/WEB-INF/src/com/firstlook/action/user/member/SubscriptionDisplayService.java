package com.firstlook.action.user.member;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.map.MultiValueMap;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import biz.firstlook.transact.persist.model.BusinessUnit;
import biz.firstlook.transact.persist.model.SubscriberType;
import biz.firstlook.transact.persist.model.Subscription;
import biz.firstlook.transact.persist.model.SubscriptionDeliveryType;
import biz.firstlook.transact.persist.model.SubscriptionFrequency;
import biz.firstlook.transact.persist.model.SubscriptionFrequencyDetail;
import biz.firstlook.transact.persist.model.SubscriptionType;
import biz.firstlook.transact.persist.service.SubscriptionService;

import com.firstlook.entity.Member;
import com.firstlook.entity.form.MemberForm;
import com.firstlook.service.memberaccess.MemberAccessService;

public class SubscriptionDisplayService
{

private MemberAccessService memberAccessService;
private SubscriptionService subscriptionService;
private TransactionTemplate transactionTemplate;

public SubscriptionDisplayService()
{
	super();
}

@SuppressWarnings("unchecked")
public void setSubscriptionDataOnMemberForm( HttpServletRequest request, final MemberForm memberForm, Integer memberId, int memberType )
{
	List<SubscriptionDisplayData> subscriptionData = new ArrayList<SubscriptionDisplayData>();
	SubscriptionDisplayData displaySubscriptionData;

	Collection<Subscription> subscriptions = subscriptionService.retrieveActiveSubscriptionsByMemberId( new Integer( memberForm.getMemberId() ) );

	Iterator<Subscription> subscriptionIter = subscriptions.iterator();
	Subscription subscription;
	MultiValueMap subscriptionsMap = new MultiValueMap();
	while ( subscriptionIter.hasNext() )
	{
		subscription = (Subscription)subscriptionIter.next();
		subscriptionsMap.put( subscription.getSubscriptionType().getSubscriptionTypeId(), subscription );
	}
	
	MultiValueMap availableSubscriptions = subscriptionService.getAvailableSubscriptionsByMemberId( new Integer( memberForm.getMemberId() ) );
	Iterator<SubscriptionType> availableSubscriptionsIter = availableSubscriptions.keySet().iterator();
	
	SubscriptionType availableSubscription;
	List< BusinessUnit > businessUnits = null;

	while ( availableSubscriptionsIter.hasNext() )
	{
		availableSubscription = availableSubscriptionsIter.next();
		businessUnits = (List< BusinessUnit >) availableSubscriptions.get( availableSubscription );
		
		if ( subscriptionsMap.containsKey( availableSubscription.getSubscriptionTypeId() ) )
		{
			displaySubscriptionData = dealersSubscriptionPreferences( subscriptionsMap, availableSubscription );
		}
		else
		{
			displaySubscriptionData = defaultSubscriptionSettings( availableSubscription );
		}
		displaySubscriptionData.setDealershipNames(  businessUnits );
		displaySubscriptionData.setSubscriptionType( availableSubscription );
		// nk -- this shoudl only get those requencies avaible for this subscription type
		displaySubscriptionData.setAlertFrequency( subscriptionService.retrieveAllFrequenciesForSubscriptionType( availableSubscription.getSubscriptionTypeId() ) );
		// nk -- this shoudl only get those delivery formats/types avaible for this subscription type
		displaySubscriptionData.setDeliveryFormats(  subscriptionService.retrieveAllDeliveryFormatsForSubscriptionType(availableSubscription.getSubscriptionTypeId()) );
		
		subscriptionData.add( displaySubscriptionData );
	}

	BeanComparator subscriptionDisplayDataComparator = new BeanComparator( "subscriptionType" );
	Collections.<SubscriptionDisplayData>sort( subscriptionData, subscriptionDisplayDataComparator );

	memberForm.setSubscriptionData( subscriptionData );
}

// populates display data with default values
private SubscriptionDisplayData defaultSubscriptionSettings( SubscriptionType availableSubscription )
{
	SubscriptionDisplayData displayData = new SubscriptionDisplayData();
	displayData.setSelectedFrequencyType( getDefaultFrequency( availableSubscription ) );
	String[] defaultFormat = { SubscriptionDeliveryType.HTML_E_MAIL.getSubscriptionDeliveryTypeID().toString() };
	displayData.setSelectedDeliveryFormats( defaultFormat );
	return displayData;
}

private Integer getDefaultFrequency( SubscriptionType availableSubscription )
{
	Integer toBeReturned = null;
	if ( availableSubscription.getDefaultSubscriptionFrequencyDetail().getSubscriptionFrequency().equals( SubscriptionFrequency.WEEKLY ) )
	{
		toBeReturned = availableSubscription.getDefaultSubscriptionFrequencyDetail().getSubscriptionFrequencyDetailId();
	}
	else
	{
		toBeReturned = availableSubscription.getDefaultSubscriptionFrequencyDetail().getSubscriptionFrequencyDetailId();
	}
	return toBeReturned;
}

// populates display data with already selected data
@SuppressWarnings("unchecked")
private SubscriptionDisplayData dealersSubscriptionPreferences( MultiValueMap subscriptionsMap, SubscriptionType availableSubscription)
{
	List<Subscription> subscriptions = (List)subscriptionsMap.get( availableSubscription.getSubscriptionTypeId() );
	String[] subscribedDealers = getSubscribedDealers( subscriptions, availableSubscription.getSubscriptionTypeId() );
	SubscriptionDisplayData displayData = new SubscriptionDisplayData();
	displayData.setSelectedDealershipNames( subscribedDealers );

	// get the first subscription entry since subscription delivery type will be
	// teh same across all
	Subscription subscription = (Subscription)subscriptions.get( 0 );

	if ( subscription.getSubscriptionFrequencyDetail().getSubscriptionFrequency().getSubscriptionFrequencyId().equals(
																														SubscriptionFrequency.WEEKLY.getSubscriptionFrequencyId() ) )
	{
		displayData.setSelectedFrequencyType( subscription.getSubscriptionFrequencyDetail().getSubscriptionFrequencyDetailId() );
	}
	else
	{
		displayData.setSelectedFrequencyType( subscription.getSubscriptionFrequencyDetail().getSubscriptionFrequencyDetailId() );
	}

	String[] deliveryFormats = getDeliveryFormats( subscription.getBusinessUnitId(), subscriptions );
	displayData.setSelectedDeliveryFormats( deliveryFormats );
	return displayData;
}

private String[] getDeliveryFormats( Integer businessUnitId, List<Subscription> subscriptions )
{
	List<String> deliveryFormatStrings = new ArrayList<String>();
	Iterator<Subscription> iter = subscriptions.iterator();
	while ( iter.hasNext() )
	{
		Subscription subscription = iter.next();
		if ( subscription.getBusinessUnitId().equals( businessUnitId ) )
		{
			deliveryFormatStrings.add( subscription.getSubscriptionDeliveryType().getSubscriptionDeliveryTypeID() + "" );
		}
	}
	return (String[])deliveryFormatStrings.toArray( new String[deliveryFormatStrings.size()] );
}

private String[] getSubscribedDealers( List subscriptions, Integer subscriptionTypeId )
{
	Iterator iter = subscriptions.iterator();
	Subscription subscription;
	List subscribedBusinessUnitIds = new ArrayList();

	// get a list of all businessUnitId's that have subscribed to this alert
	// type (no dups)
	while ( iter.hasNext() )
	{
		subscription = (Subscription)iter.next();
		if ( subscription.getSubscriptionType().getSubscriptionTypeId().equals( subscriptionTypeId ) )
		{
			subscribedBusinessUnitIds.add( subscription.getBusinessUnitId() + "" );
		}
	}

	return (String[])subscribedBusinessUnitIds.toArray( new String[subscribedBusinessUnitIds.size()] );
}

public void updateMemberSubscriptions( Collection subscriptions, Member memberFromPage, int memberType )
{
	// if the user was not shown any alerts in the first place, you can skip this whole step
	if ( subscriptions.isEmpty() )
	{
		return;
	}

	SubscriptionDisplayData displaySubscription;
	Subscription subscription;
	Collection subscriptionsToPersist = new ArrayList();
	String[] deliveryTypes;
	String[] subscribedDealers;
	Iterator subscriptionIter = subscriptions.iterator();
	while ( subscriptionIter.hasNext() )
	{
		displaySubscription = (SubscriptionDisplayData)subscriptionIter.next();
		deliveryTypes = displaySubscription.getSelectedDeliveryFormats();
		subscribedDealers = displaySubscription.getSelectedDealershipNames();
		if ( deliveryTypes != null
				&& subscribedDealers != null
				&& displaySubscription.getSelectedFrequencyType().intValue() != SubscriptionFrequencyDetail.NO_ALERTS )
		{
			for ( int x = 0; x < deliveryTypes.length; x++ )
			{
				for ( int y = 0; y < subscribedDealers.length; y++ )
				{
					subscription = new Subscription();
					subscription.setBusinessUnitId( new Integer( Integer.parseInt( subscribedDealers[y] ) ) );
					subscription.setSubscriberId( memberFromPage.getMemberId() );
					subscription.setSubscriberType( SubscriberType.MEMBER );
					subscription.setSubscriptionType( displaySubscription.getSubscriptionType() );
					subscription.setSubscriptionDeliveryType( SubscriptionDeliveryType.getDeliveryTypeByID( Integer.parseInt( deliveryTypes[x] ) ) );
					subscription.setSubscriptionFrequencyDetail( SubscriptionFrequencyDetail.getSubscriptionFrequencyDetailById( 
					                                                          displaySubscription.getSelectedFrequencyType().intValue() ) );

					subscriptionsToPersist.add( subscription );
				}
			}
		}
	}
	
	deleteThenSaveInXaction( memberFromPage.getMemberId(), subscriptionsToPersist, memberType );
}

private void deleteThenSaveInXaction( final Integer memberId, final Collection subscriptionsToPersist, final int memberType )
{
	transactionTemplate.execute( new TransactionCallbackWithoutResult()
	{
		protected void doInTransactionWithoutResult( TransactionStatus status )
		{
			if ( memberType == Member.MEMBER_TYPE_ADMIN )
			{
				subscriptionService.adminBulkDelete( memberId );
			}
			else
			{
				subscriptionService.userBulkDelete( memberId );
			}
			subscriptionService.saveSubscriptions( subscriptionsToPersist );
		}
	} );
}

public SubscriptionService getSubscriptionService()
{
	return subscriptionService;
}

public void setSubscriptionService( SubscriptionService subscriptionService )
{
	this.subscriptionService = subscriptionService;
}

public TransactionTemplate getTransactionTemplate()
{
	return transactionTemplate;
}

public void setTransactionTemplate( TransactionTemplate transactionTemplate )
{
	this.transactionTemplate = transactionTemplate;
}

public MemberAccessService getMemberAccessService()
{
	return memberAccessService;
}

public void setMemberAccessService( MemberAccessService memberAccessService )
{
	this.memberAccessService = memberAccessService;
}

}
