package com.firstlook.action.user.member;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import biz.firstlook.commons.email.EmailService;
import biz.firstlook.commons.email.EmailService.EmailFormat;

import com.firstlook.action.BaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;

/**
 * This action is not currently used in production.
 * @author bfung
 *
 */
public class ForgotPasswordAction extends BaseAction
{

private EmailService emailService;

public ActionForward justDoIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws ApplicationException, DatabaseException
{

	ActionForward forward = mapping.findForward( "failure" );

	String login = (String)( (DynaActionForm)form ).get( "login" );

	Member member = getMemberService().retrieveMember( login );

	request.setAttribute( "login", login );

	if ( member != null )
	{
		String newPassword = getMemberService().issueNewPassword( member );
		Map<String, String> emailContext = new HashMap<String, String>();
		emailContext.put( "firstName", member.getFirstName() );
		emailContext.put( "lastName", member.getLastName() );
		emailContext.put( "password", newPassword );
		
		emailService.sendEmail( member.getEmailAddress(), Collections.EMPTY_LIST, emailContext,  "changePassword", "First Look Password Reset", null, null, null, EmailFormat.PLAIN_TEXT );
		request.setAttribute( "email", member.getEmailAddress() );
		forward = mapping.findForward( "success" );
	}
	else
	{
		request.setAttribute( "passwordMessage", "y" );
	}

	return forward;
}

public void setEmailService( EmailService emailService )
{
	this.emailService = emailService;
}
}
