package com.firstlook.action.user.member;

import java.io.Serializable;
import java.util.Collection;

import biz.firstlook.transact.persist.model.BusinessUnit;
import biz.firstlook.transact.persist.model.SubscriptionDeliveryType;
import biz.firstlook.transact.persist.model.SubscriptionFrequencyDetail;
import biz.firstlook.transact.persist.model.SubscriptionType;

public class SubscriptionDisplayData implements Serializable
{

private static final long serialVersionUID = 2049240393747055059L;

private SubscriptionType subscriptionType;
private Collection<SubscriptionFrequencyDetail> alertFrequency;
private Collection<SubscriptionDeliveryType> deliveryFormats;
private Collection<BusinessUnit> dealershipNames;
private Integer selectedFrequencyType;
private String[] selectedDeliveryFormats;
private String[] selectedDealershipNames;

public SubscriptionDisplayData()
{
	super();
}

public Collection<SubscriptionFrequencyDetail> getAlertFrequency()
{
	return alertFrequency;
}

public void setAlertFrequency( Collection<SubscriptionFrequencyDetail> alertFrequency )
{
	this.alertFrequency = alertFrequency;
}

public Object[] getDeliveryFormats()
{
	return deliveryFormats.toArray();
}

public String[] getSelectedDealershipNames()
{
	return selectedDealershipNames;
}

public Integer getNumberOfSelectedDealerships()
{
	if ( selectedDealershipNames == null)
	{
		return new Integer( 0 );
	}
	else
	{
		return new Integer( selectedDealershipNames.length );
	}
}

public void setSelectedDealershipNames( String[] selectedDealershipNames )
{
	this.selectedDealershipNames = selectedDealershipNames;
}

public String[] getSelectedDeliveryFormats()
{
	return selectedDeliveryFormats;
}

public void setSelectedDeliveryFormats( String[] selectedDeliveryFormats )
{
	this.selectedDeliveryFormats = selectedDeliveryFormats;
}

public Integer getSelectedFrequencyType()
{
	return selectedFrequencyType;
}

public void setSelectedFrequencyType( Integer selectedFrequencyType )
{
	this.selectedFrequencyType = selectedFrequencyType;
}

public SubscriptionType getSubscriptionType()
{
	return subscriptionType;
}

public void setSubscriptionType( SubscriptionType subscriptionType )
{
	this.subscriptionType = subscriptionType;
}

public Integer getDealershipNamesSize()
{
	return new Integer( dealershipNames.size() );
}

public void setDealershipNames( Collection<BusinessUnit> dealershipNames )
{
	this.dealershipNames = dealershipNames;
}

public void setDeliveryFormats( Collection<SubscriptionDeliveryType> deliveryFormat )
{
	this.deliveryFormats = deliveryFormat;
}

public Collection<BusinessUnit> getDealershipNames()
{
	return dealershipNames;
}

}
