package com.firstlook.action.manheim;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.Security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.manheim.ManheimService;

public class ManheimDisplayAction extends SecureBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response ) throws DatabaseException,
        ApplicationException
{

    ManheimService service = new ManheimService();
    service.run();

    System.setProperty("java.protocol.handler.pkgs",
            "com.sun.net.ssl.internal.www.protocol");
    Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
    URL url = null;
    String url1 = "https://www.manheim.com/members/internetmmr";
    try
    {
        url = new URL("https://www.manheim.com/members/internetmmr");

    } catch (MalformedURLException e)
    {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }

    // SSLException thrown here if server certificate is invalid
    try
    {
        String inputLine;
        BufferedReader in = new BufferedReader(new InputStreamReader(url
                .openStream()));
        while ((inputLine = in.readLine()) != null)
            System.out.println(inputLine);
        in.close();
        // con = url.openConnection();
    } catch (IOException e1)
    {
        // TODO Auto-generated catch block
        e1.printStackTrace();
    } 
    // try
    // {
    // InputStream stream = con.getInputStream();
    // }
    // catch( IOException e2 )
    // {
    // e2.printStackTrace();
    // }

    String urlencoded = response.encodeRedirectURL(url1);
    try
    {
        response.sendRedirect(urlencoded);
    } catch (IOException e1)
    {
        e1.printStackTrace();
    }

    return null;
}

}
