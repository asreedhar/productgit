package com.firstlook.action.carfax;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.services.vehiclehistoryreport.AutoCheckService;
import biz.firstlook.commons.services.vehiclehistoryreport.CarfaxException;
import biz.firstlook.commons.services.vehiclehistoryreport.CarfaxService;
import biz.firstlook.services.vehiclehistoryreport.carfax.CarfaxReportTO;
import biz.firstlook.services.vehiclehistoryreport.carfax.CarfaxReportType;
import biz.firstlook.services.vehiclehistoryreport.carfax.VehicleEntityType;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.helper.SessionHelper;
import com.firstlook.entity.form.TradeAnalyzerForm;

public class CarfaxGetReportAction extends SecureBaseAction {

	private static Logger logger = Logger.getLogger( CarfaxGetReportAction.class );
	
	@Override
	public ActionForward doIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws DatabaseException, ApplicationException {
		String vin = request.getParameter( "vin" );
		String reportType = request.getParameter( "reportType" );
		Boolean cccFlag = getFlagValue(request.getParameter( "cccFlagFromTile" ));
		Member member = getMemberFromRequest( request );
		Boolean isNavigator = getFlagValue(request.getParameter("isNavigator"));
		Boolean isAppraisal = getFlagValue(request.getParameter("isAppraisal"));
		Boolean isTotalInventoryReport = getFlagValue(request.getParameter("isTIR"));
		Boolean isRunAllForDealer = getFlagValue(request.getParameter("isRunAll"));
		
		SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
		SessionHelper.keepAttribute( request, "bookoutDetailForm" );
		
		TradeAnalyzerForm tradeAnalyzerForm = (TradeAnalyzerForm) SessionHelper.getAttribute(request, "tradeAnalyzerForm");
		
		CarfaxService carfaxService = CarfaxService.getInstance();
		
		int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
		
		CarfaxReportTO carfaxReport = null;
		
		CarfaxReportType carfaxReportType = CarfaxReportType.fromValue(reportType);
		
		if (isTotalInventoryReport != null && Boolean.valueOf(isTotalInventoryReport && isRunAllForDealer != null && Boolean.valueOf(isRunAllForDealer))){
			try {
				carfaxService.purchaseCarfaxReports(
						currentDealerId,
						member.getLogin(),
						VehicleEntityType.INVENTORY,
						carfaxReportType,
						cccFlag);
			} catch (CarfaxException e) {
				logger.error( "Error in carfax popup action when retrieving report for VIN: " + vin + " for MemberId: " + member.getMemberId(), e );
				request.setAttribute( "problem", e.getMessage() );
				request.setAttribute( "hasCarfaxError", true );
				if (isTotalInventoryReport != null && Boolean.valueOf(isTotalInventoryReport)){
					return mapping.findForward("failureTIR");
				}
				return mapping.findForward( "failure" );
			}
		}
		else {
			try {
				carfaxReport = carfaxService.purchaseCarfaxReport(
						currentDealerId,
						member.getLogin(),
						vin,
						carfaxReportType,
						cccFlag);
			}
			catch (Exception e) {
				logger.error( "Error in carfax popup action when retrieving report for VIN: " + vin + " for MemberId: " + member.getMemberId(), e );
				request.setAttribute( "problem", e.getMessage() );
				request.setAttribute( "hasCarfaxError", true );
				if (isTotalInventoryReport != null && Boolean.valueOf(isTotalInventoryReport)){
					return mapping.findForward("failureTIR");
				}
				return mapping.findForward( "failure" );
			}
		}
		
		Map<String,String> properties = carfaxService.getCarfaxReportProperties(currentDealerId, member.getLogin());
		for (Map.Entry<String, String> entry : properties.entrySet()) {
			request.setAttribute(entry.getKey(), entry.getValue());
		}
		
		properties = AutoCheckService.getInstance().getAutoCheckReportProperties(currentDealerId, member.getLogin());
		for (Map.Entry<String, String> entry : properties.entrySet()) {
			request.setAttribute(entry.getKey(), entry.getValue());
		}
		
		request.setAttribute("hasCarfax", true);
		request.setAttribute("reportAvailable", true);
		request.setAttribute("reportType", reportType);
		request.setAttribute("vin", vin);
		request.setAttribute("CCCFlag",cccFlag );
		
		if (carfaxReport != null){
			String carfaxReportUrl = carfaxService.reportUrl(carfaxReport);
			request.setAttribute("carfaxReportUrl", carfaxReportUrl);
			
			request.setAttribute("carfaxReportOK", carfaxService.isOkay(carfaxReport));
			request.setAttribute("hasOneOwner", carfaxReport.getOwnerCount() == 1);
			
			if (tradeAnalyzerForm != null)
				tradeAnalyzerForm.setCarfaxReport(carfaxReport);
		}
		if (isTotalInventoryReport != null && Boolean.valueOf(isTotalInventoryReport)) {
			return mapping.findForward("successTIR");
		}
		
		if (isAppraisal != null && Boolean.valueOf(isAppraisal)) {
			return mapping.findForward( "successAppraisal" );
		}
		
		if (isNavigator != null && Boolean.valueOf(isNavigator)) {
			return mapping.findForward( "successNavigator" );
		}
		
		ActionForward forward = mapping.findForward( "success" ); 
		
		return forward;
	}
	
	private Boolean getFlagValue( String flag )
	{
		if ( flag != null )
			return Boolean.valueOf( flag );

		return Boolean.FALSE;
	}
	
}