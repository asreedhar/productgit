package com.firstlook.action.carfax;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.services.vehiclehistoryreport.CarfaxService;
import biz.firstlook.services.vehiclehistoryreport.carfax.CarfaxReportTO;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.entity.Member;
import com.firstlook.helper.SessionHelper;

public class CarfaxPopupAction extends SecureBaseAction
{

private static Logger logger = Logger.getLogger( CarfaxPopupAction.class );

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
{
	SessionHelper.keepAttribute( request, "tradeAnalyzerForm" );
	SessionHelper.keepAttribute( request, "bookoutDetailForm" );
	String vin = request.getParameter( "vin" );
	
	// get a copy of the report for our DB
	int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
	Member member = getMemberFromRequest(request);
	CarfaxReportTO carfaxReport = null;
	try
	{
		carfaxReport = CarfaxService.getInstance().getCarfaxReport(currentDealerId, member.getLogin(), vin);
		
		if (carfaxReport == null)
		{
			request.setAttribute( "problem", "Report not available!" );
			
			return mapping.findForward( "failure" );
		}
	}
	catch ( Exception e )
	{
		logger.error( "Error in carfax popup action when retrieving report for VIN: " + vin + " for businessUnit: " + currentDealerId, e );
		request.setAttribute( "problem", e.getMessage() );
		return mapping.findForward( "failure" );
	}

	// send the user to CARFAX to view the report
	String url = CarfaxService.getInstance().reportUrl(carfaxReport);
	try
	{
		response.sendRedirect( response.encodeRedirectURL( url ) );
	}
	catch ( IOException e )
	{
		logger.error("Error sending car fax report reidrect", e);
	}

	return null;
}

}
