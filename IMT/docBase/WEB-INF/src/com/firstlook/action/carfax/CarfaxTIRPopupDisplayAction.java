package com.firstlook.action.carfax;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.services.vehiclehistoryreport.CarfaxService;
import biz.firstlook.services.vehiclehistoryreport.carfax.CarfaxReportPreferenceTO;
import biz.firstlook.services.vehiclehistoryreport.carfax.CarfaxReportType;
import biz.firstlook.services.vehiclehistoryreport.carfax.VehicleEntityType;

import com.firstlook.action.SecureBaseAction;
import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;

public class CarfaxTIRPopupDisplayAction extends SecureBaseAction {

	@Override
	public ActionForward doIt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws DatabaseException, ApplicationException {
		String vin = request.getParameter( "vin" );
		Boolean isRunAllReport = getFlagValue(request.getParameter("isRunAll"));
		
		int currentDealerId = getFirstlookSessionFromRequest( request ).getCurrentDealerId();
		
		Member member = getMemberFromRequest(request);
		
		CarfaxReportPreferenceTO preference = CarfaxService.getInstance().getCarfaxReportPreferences(
				currentDealerId,
				member.getLogin(),
				VehicleEntityType.INVENTORY);
		
		if (preference != null){
			request.setAttribute("reportType", preference.getReportType());
			request.setAttribute("CCCFlag", preference.isDisplayInHotListings());
		}
		else {
			request.setAttribute("reportType", CarfaxReportType.BTC);
			request.setAttribute("CCCFlag", false);
		}
		
		request.setAttribute("vin", vin);
		request.setAttribute("isRunAll", isRunAllReport);
		
		return mapping.findForward( "success" );
	}

	private Boolean getFlagValue( String flag )
	{
		if ( flag != null )
			return Boolean.valueOf( flag );

		return Boolean.FALSE;
	}
}