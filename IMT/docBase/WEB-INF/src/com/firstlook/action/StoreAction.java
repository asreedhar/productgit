package com.firstlook.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import biz.firstlook.transact.persist.model.UserRoleEnum;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryTypeEnum;
import com.firstlook.entity.Member;
import com.firstlook.exception.ApplicationException;
import com.firstlook.session.FirstlookSession;
import com.firstlook.session.Product;

public class StoreAction extends SecureBaseAction
{

public ActionForward doIt( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response )
		throws DatabaseException, ApplicationException
{
	Member member = getMemberFromRequest( request );
	DynaActionForm storeForm = (DynaActionForm)form;
	Integer dealerId = (Integer)storeForm.get( "dealerId" );
FirstlookSession firstlookSession = (FirstlookSession)request.getSession().getAttribute( FirstlookSession.FIRSTLOOK_SESSION );
	if (dealerId == null || dealerId.intValue() == 0) {
		dealerId = firstlookSession.getCurrentDealerId();
	}

	if ( dealerId != null && dealerId.intValue() != 0 )
	{	// update current session so it is associated with the selected dealer 
		// this will be a new value if the user is a Dealer Group Principal selecting from a list of dealers
		// His initial session will be set for the first dealership in his list of dealers (see DealerGroupHomeDisplayAction)
		// This new session needs to be associated with the dealer he just selected
		if( member.isAssociatedWithDealer( dealerId )) {
			firstlookSession = getFirstlookSessionManager().constructFirstlookSession( dealerId.intValue(), member );
			request.getSession().setAttribute( FirstlookSession.FIRSTLOOK_SESSION, firstlookSession );
		}
	}

	if ( member.isAdmin() )
	{
		member.setUserRoleEnum( UserRoleEnum.ALL );
	}

	String forward = retrieveForwardAndSetRole( member, request );
	Dealer currentDealer = getImtDealerService().retrieveDealer( dealerId );

	request.setAttribute( "currentDealer", currentDealer );

	return mapping.findForward(  forward  );
}

protected String retrieveForwardAndSetRole( Member member, HttpServletRequest request)
{
	String forward = null;
	if ( member.getUserRoleEnum().getName().equalsIgnoreCase( "All" ) || member.isAccountRep() )
	{
		forward = "department";
	}
	else
	{	
		member.setInventoryType(InventoryTypeEnum.getEnum(member.getUserRoleEnum()));
		forward = "scorecard";
		request.setAttribute( "mode", Product.VIP.getName() );
	}
	return forward;
}

}
