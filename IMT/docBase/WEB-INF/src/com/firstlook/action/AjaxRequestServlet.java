package com.firstlook.action;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import biz.firstlook.transact.persist.person.IPerson;
import biz.firstlook.transact.persist.person.IPersonService;

import com.firstlook.util.JSONPersonUtil;

/**
 * This is a *thin* servlet which can process Async Ajax requests that we do not need/want
 * to go through struts. For example, when we are using Struts forms within a page that makes
 * multiple requests (you may lose the form on the session).
 * 
 * Right now this usess SpingMVC cause it was super simple and using a spring DispatcherServlet 
 * ensured we had access to all the other beans in our IMT webcontext.
 * 
 * @author nkeen
 *
 */
public class AjaxRequestServlet  implements Controller {

	private static final long serialVersionUID = -2305212462293539482L;
	
	private IPersonService personService;
	
	public void setPersonService(IPersonService personService) {
		this.personService = personService;
	}

	/**
	 * Strips off important info from the request and marshalls accordingly.
	 */
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView result = null;
		String action = request.getParameter("action");
		int dealerId = Integer.parseInt(request.getParameter("dealerId"));

		if (action != null) {
			if (action.equalsIgnoreCase("getPeopleList")) {
				result = doPeopleList(dealerId);
			}
		}
		return result;
	}
	
	/**
	 * Handles a request for the list of sales people in a specific businessUnit
	 */
	private ModelAndView doPeopleList(int dealerId) {
		Collection< IPerson > people = personService.getPeopleInBusinessUnit( dealerId );
		String peopleJSON = JSONPersonUtil.convertPeopleToJSON( people ).toString();
		return new ModelAndView("/dealer/tools/includes/_dealTrackingGetPeople", "people",  peopleJSON);
	}

}
