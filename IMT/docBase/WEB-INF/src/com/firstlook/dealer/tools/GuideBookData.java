package com.firstlook.dealer.tools;

public class GuideBookData
{

private GuideBook guideBook;
private boolean validVin;
private String selectedKey;
private String errorMessage;

public GuideBookData()
{
    super();
}

public GuideBook getGuideBook()
{
    return guideBook;
}

public String getSelectedKey()
{
    return selectedKey;
}

public boolean isValidVin()
{
    return validVin;
}

public void setGuideBook( GuideBook book )
{
    guideBook = book;
}

public void setSelectedKey( String string )
{
    selectedKey = string;
}

public void setValidVin( boolean b )
{
    validVin = b;
}

public String getErrorMessage()
{
    return errorMessage;
}

public void setErrorMessage( String string )
{
    errorMessage = string;
}

}
