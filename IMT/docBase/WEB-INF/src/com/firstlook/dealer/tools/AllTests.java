package com.firstlook.dealer.tools;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests extends junit.framework.TestCase
{

public AllTests( String arg1 )
{
    super(arg1);
}

public static Test suite()
{
    TestSuite suite = new TestSuite();
    suite.addTest(new TestSuite(TestBlackBookGuideBook.class));
    suite.addTest(new TestSuite(TestGuideBook.class));
    suite.addTest(new TestSuite(TestGuideBookOption.class));
    suite.addTest(new TestSuite(TestNADAGuideBook.class));

    return suite;
}
}
