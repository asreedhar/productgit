package com.firstlook.dealer.tools;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import biz.firstlook.module.vehicle.description.old.GBException;

public class VinNotFoundException extends GBException {

	private static Logger logger = Logger.getLogger( VinNotFoundException.class );
	
	private static final long serialVersionUID = 6529264931286408024L;
	
	private String vin;
	private Integer businessUnitId;
	private String guideBookName;
	private Date date;
	
	private static SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");

	public VinNotFoundException() {
		super();
	}
	
	public VinNotFoundException(String vin, Integer businessUnitId, String guideBookName, Date date) {
		super();
		this.vin = vin;
		this.businessUnitId = businessUnitId;
		this.guideBookName = guideBookName;
		this.date = date;
	}
	
	public VinNotFoundException(String message) {
		super(message);
	}

	public String getMessage() {
		return this.toString();
	}

	public String toString() {
		String message = "VIN: " + vin + " could not be found in GuideBook: " + guideBookName + " for BusinessUnit: " + businessUnitId + ".  TIME: " + formatter.format(date);
		logger.error(message);
		return message;
	}
	
	public Integer getBusinessUnitId() {
		return businessUnitId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getGuideBookName() {
		return guideBookName;
	}

	public void setGuideBookName(String guideBookName) {
		this.guideBookName = guideBookName;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public void setBusinessUnitId(Integer businessUnitId) {
		this.businessUnitId = businessUnitId;
	}
	
	
	
	
}
