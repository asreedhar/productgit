package com.firstlook.dealer.tools;

import java.util.ArrayList;

public class TestGuideBook extends com.firstlook.mock.BaseTestCase
{

public TestGuideBook( String arg1 )
{
    super(arg1);
}

public void testGetTitle()
{
    assertEquals("blackbook", GuideBook.GUIDE_TITLE_BLACKBOOK, GuideBook
            .getTitle(GuideBook.GUIDE_TYPE_BLACKBOOK));
    assertEquals("nada", GuideBook.GUIDE_TITLE_NADA, GuideBook
            .getTitle(GuideBook.GUIDE_TYPE_NADA));
    assertEquals("galves", GuideBook.GUIDE_TITLE_GALVES, GuideBook
            .getTitle(GuideBook.GUIDE_TYPE_GALVES));
    assertEquals("kellybluebook", GuideBook.GUIDE_TITLE_KELLEYBLUEBOOK,
            GuideBook.getTitle(GuideBook.GUIDE_TYPE_KELLEYBLUEBOOK));
}

public void testHasNonZeroGuideBookValues()
{
    GuideBook guideBook = new GuideBook(0, GuideBook.NOT_SET, "");

    assertTrue("no values set", !guideBook.hasNonZeroGuideBookValues());

    ArrayList values = new ArrayList();
    guideBook.setGuideBookValues(values);

    assertTrue("empty array list", !guideBook.hasNonZeroGuideBookValues());

    GuideBookReturnValue value = new GuideBookReturnValue();
    values.add(value);

    assertTrue("one zero value in list", !guideBook.hasNonZeroGuideBookValues());

    value = new GuideBookReturnValue();
    value.setValue(new Integer(1));
    values.add(value);

    assertTrue("got a non zero value", guideBook.hasNonZeroGuideBookValues());
}

}
