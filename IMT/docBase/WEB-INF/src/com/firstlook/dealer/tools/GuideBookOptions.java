package com.firstlook.dealer.tools;

import java.util.Collection;

public class GuideBookOptions
{
private Collection equipmentOptions;
private Collection engineOptions;
private Collection transmissionOptions;
private Collection driveTrainOptions;

public GuideBookOptions()
{
    super();
}

public Collection getDriveTrainOptions()
{
    return driveTrainOptions;
}

public Collection getEngineOptions()
{
    return engineOptions;
}

public Collection getEquipmentOptions()
{
    return equipmentOptions;
}

public Collection getTransmissionOptions()
{
    return transmissionOptions;
}

public void setDriveTrainOptions( Collection collection )
{
    driveTrainOptions = collection;
}

public void setEngineOptions( Collection collection )
{
    engineOptions = collection;
}

public void setEquipmentOptions( Collection collection )
{
    equipmentOptions = collection;
}

public void setTransmissionOptions( Collection collection )
{
    transmissionOptions = collection;
}

}
