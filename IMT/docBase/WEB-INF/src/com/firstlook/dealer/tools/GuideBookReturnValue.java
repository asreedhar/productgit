package com.firstlook.dealer.tools;

public class GuideBookReturnValue
{

private String title;
private Integer value;

public GuideBookReturnValue()
{
    super();
}

public GuideBookReturnValue( String title, Integer value )
{
    setTitle(title);
    setValue(value);
}

public String getTitle()
{
    return title;
}

public Integer getValue()
{
    return value;
}

public void setTitle( String newTitle )
{
    title = newTitle;
}

public void setValue( Integer newValue )
{
    value = newValue;
}

public boolean isValid()
{
    return value != null;
}
}
