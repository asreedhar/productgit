package com.firstlook.dealer.tools;

public class TestBlackBookGuideBook extends com.firstlook.mock.BaseTestCase
{

public TestBlackBookGuideBook( String arg1 )
{
    super(arg1);
}

public void testConstructor()
{
    BlackBookGuideBook gb = new BlackBookGuideBook();

    assertEquals("Title", "BlackBook", gb.getTitle());
}

public void testGetDescriptionFormatted()
{
    String model = "modelTest";
    String series = "seriesTest";
    String trim = "trimTest";
    GuideBookVehicle blackBookVehicle = new GuideBookVehicle();
    blackBookVehicle.setModel(model);
    blackBookVehicle.setSeries(series);
    blackBookVehicle.setTrim(trim);

    BlackBookGuideBook blackBook = new BlackBookGuideBook();

    assertEquals("Formatted description: model series", (trim + " " + series),
            blackBook.getDescriptionFormatted(blackBookVehicle));

}
}
