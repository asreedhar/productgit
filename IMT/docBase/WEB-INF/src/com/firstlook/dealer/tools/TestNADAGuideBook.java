package com.firstlook.dealer.tools;

public class TestNADAGuideBook extends com.firstlook.mock.BaseTestCase
{

public TestNADAGuideBook( String arg1 )
{
    super(arg1);
}

public void testConstructor()
{
    NADAGuideBook gb = new NADAGuideBook();

    assertEquals("Title", "NADA", gb.getTitle());
}

public void testGetDescriptionFormatted()
{
    String body = "bodyTest";
    String series = "seriesTest";
    GuideBookVehicle nadaVehicle = new GuideBookVehicle();
    nadaVehicle.setBody(body);
    nadaVehicle.setSeries(series);

    NADAGuideBook nadaGuideBook = new NADAGuideBook();

    assertEquals("Formatted description value should be  seriesTest bodyTest",
            (series + " " + body), nadaGuideBook
                    .getDescriptionFormatted(nadaVehicle));

}
}
