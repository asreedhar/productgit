package com.firstlook.dealer.tools;

import com.firstlook.mock.BaseTestCase;

public class TestGuideBookOption extends BaseTestCase
{

public TestGuideBookOption( String arg1 )
{
    super(arg1);
}

public void testEquals()
{
    GuideBookOption gbv1 = new GuideBookOption();
    gbv1.setKey("KEY");
    gbv1.setDescription("DESC");
    gbv1.setValue(100);

    GuideBookOption gbv2 = new GuideBookOption();
    gbv2.setKey("KEY");
    gbv2.setDescription("DESC");
    gbv2.setValue(100);

    GuideBookOption gbv3 = new GuideBookOption();
    gbv3.setKey("KEY");
    gbv3.setDescription("DESC");
    gbv3.setValue(101);

    GuideBookOption gbv4 = new GuideBookOption();
    gbv4.setKey("KEY4");
    gbv4.setDescription("DESC");
    gbv4.setValue(100);

    GuideBookOption gbv5 = new GuideBookOption();
    gbv5.setKey("KEY");
    gbv5.setDescription("DESC5");
    gbv5.setValue(100);

    GuideBookOption gbv6 = new GuideBookOption();
    gbv6.setKey("KEY6");
    gbv6.setDescription("DESC6");
    gbv6.setValue(106);

    assertTrue("True test", gbv1.equals(gbv2));

}

}
