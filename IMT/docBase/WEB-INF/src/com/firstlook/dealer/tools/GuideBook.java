package com.firstlook.dealer.tools;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.struts.action.ActionErrors;

/**
 * This class is a holder object sitting over the returned results from the call
 * to the guidebook. Post-refactor:keep
 */

public class GuideBook
{

private String title;
private List guideBookValues;
private List guideBookBaseValues;
private int mileage;
private int mileageTotal;
private int accessoriesTotal;
private Integer weight;
private Integer msrp;
private String period;
private String regionName;
private String vin;
private List guideBookVehicleList = new ArrayList();
private String year;
private String make;
private List guideBookOptionsList = new ArrayList();
private List guideBookEngineList = new ArrayList();
private List guideBookTransmissionList = new ArrayList();
private List guideBookDrivetrainList = new ArrayList();
private List guideBookNoMileageValues;
private int guideBookType;
private String publishInfo;
private String[] selectedOptionKeys = new String[0];
private String selectedEngineKey;
private String selectedTransmissionKey;
private String selectedDrivetrainKey;
private ActionErrors optionErrors;
private String imageName;

public final static int GUIDE_TYPE_NOT_SET = 0;
public final static int GUIDE_TYPE_BLACKBOOK = 1;
public final static int GUIDE_TYPE_NADA = 2;
public final static int GUIDE_TYPE_KELLEYBLUEBOOK = 3;
public final static int GUIDE_TYPE_GALVES = 4;

public final static String GUIDE_TITLE_BLACKBOOK = "BlackBook";
public final static String GUIDE_TITLE_NADA = "NADA";
public final static String GUIDE_TITLE_KELLEYBLUEBOOK = "Kelley Blue Book";
public final static String GUIDE_TITLE_GALVES = "Galves";
public final static String NOT_SET = "Not Set";

private String footer;

public GuideBook()
{
}

public GuideBook( int guideBookType, String title, String imageName )
{
    setGuideBookType(guideBookType);
    setTitle(title);
    setImageName(imageName);
}

public String getFooter()
{
    return footer;
}

public List getGuideBookOptionsList()
{
    return guideBookOptionsList;
}

public void setGuideBookEngineList( java.util.List newGuideBookEngineList )
{
    guideBookEngineList = newGuideBookEngineList;
}

public List getGuideBookEngineList()
{
    return guideBookEngineList;
}

public void setGuideBookTransmissionList(
        java.util.List newGuideBookTransmissionList )
{
    guideBookTransmissionList = newGuideBookTransmissionList;
}

public List getGuideBookTransmissionList()
{
    return guideBookTransmissionList;
}

public void setGuideBookDrivetrainList(
        java.util.List newGuideBookDrivetrainList )
{
    guideBookDrivetrainList = newGuideBookDrivetrainList;
}

public List getGuideBookDrivetrainList()
{
    return guideBookDrivetrainList;
}

public List getGuideBookValues()
{
    return guideBookValues;
}

public List getGuideBookVehicleList()
{
    return guideBookVehicleList;
}

public String getMake()
{
    return make;
}

public int getMileage()
{
    return mileage;
}

public String getTitle()
{
    return title;
}

public static String getTitle( int guideType )
{
    switch (guideType)
    {
        case GUIDE_TYPE_BLACKBOOK:
            return GUIDE_TITLE_BLACKBOOK;
        case GUIDE_TYPE_GALVES:
            return GUIDE_TITLE_GALVES;
        case GUIDE_TYPE_KELLEYBLUEBOOK:
            return GUIDE_TITLE_KELLEYBLUEBOOK;
        case GUIDE_TYPE_NADA:
            return GUIDE_TITLE_NADA;
        default:
            return NOT_SET;
    }

}

public Class getVehicleFormClass()
{
    throw new RuntimeException("Not implemented");
}

public String getVin()
{
    return vin;
}

public String getYear()
{
    return year;
}

public boolean hasNonZeroGuideBookValues()
{
    if ( guideBookValues != null )
    {
        Iterator iter = guideBookValues.iterator();
        while (iter.hasNext())
        {
            GuideBookReturnValue value = (GuideBookReturnValue) iter.next();
            if ( value.getValue() != null && value.getValue().intValue() > 0 )
            {
                return true;
            }
        }
    }

    return false;
}

public void setFooter( String newFooter )
{
    footer = newFooter;
}

public void setGuideBookOptionsList( List newGuideBookOptionsList )
{
    guideBookOptionsList = newGuideBookOptionsList;
}

public void setGuideBookValues( List newValues )
{
    guideBookValues = newValues;
}

public void setGuideBookVehicleList( List newGuideBookVehicleList )
{
    guideBookVehicleList = newGuideBookVehicleList;
}

public void setMake( String newMake )
{
    make = newMake;
}

public void setMileage( int newMileage )
{
    mileage = newMileage;
}

public void setTitle( String newTitle )
{
    title = newTitle;
}

public void setVin( String newVin )
{
    vin = newVin;
}

public void setYear( String newYear )
{
    year = newYear;
}

public int getWeight()
{
    return weight;
}

public void setWeight( Integer weight )
{
    this.weight = weight;
}

public int getMsrp()
{
    return msrp;
}

public void setMsrp( Integer msrp )
{
    this.msrp = msrp;
}

public int getAccessoriesTotal()
{
    return accessoriesTotal;
}

public int getMileageTotal()
{
    return mileageTotal;
}

public void setAccessoriesTotal( int accessoriesTotal )
{
    this.accessoriesTotal = accessoriesTotal;
}

public void setMileageTotal( int mileageTotal )
{
    this.mileageTotal = mileageTotal;
}

public List getGuideBookBaseValues()
{
    return guideBookBaseValues;
}

public void setGuideBookBaseValues( List guideBookBaseValues )
{
    this.guideBookBaseValues = guideBookBaseValues;
}

public String getRegionName()
{
    return regionName;
}

public void setRegionName( String regionName )
{
    this.regionName = regionName;
}

public String getPeriod()
{
    return period;
}

public void setPeriod( String period )
{
    this.period = period;
}

public String getDescriptionFormatted( GuideBookVehicle vehicle )
{
    return vehicle.toString();
}

public String getDescriptionFormattedWithBreak( GuideBookVehicle vehicle )
{
    return vehicle.toString();
}

public int getGuideBookType()
{
    return guideBookType;
}

public void setGuideBookType( int i )
{
    guideBookType = i;
}

public String getPublishInfo()
{
    return publishInfo;
}

public void setPublishInfo( String string )
{
    publishInfo = string;
}

public List getGuideBookNoMileageValues()
{
    return guideBookNoMileageValues;
}

public void setGuideBookNoMileageValues( List list )
{
    guideBookNoMileageValues = list;
}

public String getSelectedDrivetrainKey()
{
    return selectedDrivetrainKey;
}

public String getSelectedEngineKey()
{
    return selectedEngineKey;
}

public String[] getSelectedOptionKeys()
{
    return selectedOptionKeys;
}

public String getSelectedTransmissionKey()
{
    return selectedTransmissionKey;
}

public void setSelectedDrivetrainKey( String string )
{
    selectedDrivetrainKey = string;
}

public void setSelectedEngineKey( String string )
{
    selectedEngineKey = string;
}

public void setSelectedOptionKeys( String[] strings )
{
    selectedOptionKeys = strings;
}

public void setSelectedTransmissionKey( String string )
{
    selectedTransmissionKey = string;
}

public ActionErrors getOptionErrors()
{
    return optionErrors;
}

public void setOptionErrors( ActionErrors optionErrors )
{
    this.optionErrors = optionErrors;
}

public String getImageName()
{
	return imageName;
}

public void setImageName( String imageName )
{
	this.imageName = imageName;
}

}
