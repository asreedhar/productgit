package com.firstlook.dealer.tools;

public class GuideBookVehicle
{

private String vin;
private String year;
private String make;
private int mileage;
private String model;
private String trim;
private String series;
private String body;
private int mileageAdjustment;
private String key;
private String modelCode;
private String makeCode;

public GuideBookVehicle()
{
    super();
}

public java.lang.String getBody()
{
    return body;
}

public java.lang.String getKey()
{
    return key;
}

public java.lang.String getMake()
{
    return make;
}

public int getMileage()
{
    return mileage;
}

public int getMileageAdjustment()
{
    return mileageAdjustment;
}

public java.lang.String getModel()
{
    return model;
}

public java.lang.String getSeries()
{
    return series;
}

public java.lang.String getTrim()
{
    return trim;
}

public java.lang.String getVin()
{
    return vin;
}

public java.lang.String getYear()
{
    return year;
}

public void setBody( java.lang.String newBody )
{
    body = newBody;
}

public void setKey( java.lang.String newKey )
{
    key = newKey;
}

public void setMake( java.lang.String newMake )
{
    make = newMake;
}

public void setMileage( int newMileage )
{
    mileage = newMileage;
}

public void setMileageAdjustment( int newMileageAdjustment )
{
    mileageAdjustment = newMileageAdjustment;
}

public void setModel( java.lang.String newModel )
{
    model = newModel;
}

public void setSeries( java.lang.String newSeries )
{
    series = newSeries;
}

public void setTrim( java.lang.String newTrim )
{
    trim = newTrim;
}

public void setVin( java.lang.String newVin )
{
    vin = newVin;
}

public void setYear( java.lang.String newYear )
{
    year = newYear;
}

public java.lang.String getModelCode()
{
    return modelCode;
}

public void setModelCode( java.lang.String string )
{
    modelCode = string;
}

public String getMakeCode()
{
    return makeCode;
}

public void setMakeCode( String string )
{
    makeCode = string;
}

}
