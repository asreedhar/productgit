package com.firstlook.dealer.tools;

import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;

import com.firstlook.entity.form.BlackBookGuideBookVehicleForm;

public class BlackBookGuideBook extends GuideBook
{

public final static String TITLE_EXTRA_CLEAN = "Extra Clean";
public final static String TITLE_CLEAN = "Clean";
public final static String TITLE_AVERAGE = "Average";
public final static String TITLE_ROUGH = "Rough";

public BlackBookGuideBook()
{
    super(GuideBook.GUIDE_TYPE_BLACKBOOK, GuideBook.GUIDE_TITLE_BLACKBOOK, ThirdPartyDataProvider.IMAGE_NAME_BLACKBOOK);
}

public String getDescriptionFormatted( GuideBookVehicle vehicle )
{
    return (vehicle.getTrim() + " " + vehicle.getSeries());
}

public String getDescriptionFormattedWithBreak( GuideBookVehicle vehicle )
{
    return null;
}

public Class getVehicleFormClass()
{
    return BlackBookGuideBookVehicleForm.class;
}

}
