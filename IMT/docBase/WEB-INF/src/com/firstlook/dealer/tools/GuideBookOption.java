package com.firstlook.dealer.tools;

import java.util.Collection;
import java.util.Iterator;

import com.firstlook.iterator.TabOrder;

public class GuideBookOption implements TabOrder
{

private String description;
private int value;
private int wholesaleValue;
private String key;
private boolean selected;
private int tabOrder;
private boolean standardOption = false;
private int optionType;

public static final int EQUIPMENT_OPTION = 1;
public static final int ENGINE_OPTION = 2;
public static final int DRIVETRAIN_OPTION = 3;
public static final int TRANSMISSION_OPTION = 4;

public GuideBookOption()
{
	super();
}

public GuideBookOption( String key, int value, String description )
{
	setKey( key );
	setValue( value );
	setDescription( description );
}

public GuideBookOption( String key, int value, int wholesaleValue, String description )
{
	setKey( key );
	setValue( value );
	setWholesaleValue( wholesaleValue );
	setDescription( description );
}

public GuideBookOption( String key, int value, int wholesaleValue, String description, int optionType )
{
	setKey( key );
	setValue( value );
	setWholesaleValue( wholesaleValue );
	setDescription( description );
	setOptionType( optionType );
}

public java.lang.String getDescription()
{
	return description;
}

public java.lang.String getKey()
{
	return key;
}

public int getValue()
{
	return value;
}

public void setDescription( java.lang.String newDescription )
{
	description = newDescription;
}

public void setKey( java.lang.String newKey )
{
	key = newKey;
}

public void setValue( int newValue )
{
	value = newValue;
}

public boolean equals( Object guideBookOption )
{
	if ( !( guideBookOption instanceof GuideBookOption ) )
	{
		return false;
	}

	GuideBookOption gbo = (GuideBookOption)guideBookOption;

	if ( !( getKey().equals( gbo.getKey() ) ) )
	{
		return false;
	}

	if ( !( getDescription().equals( gbo.getDescription() ) ) )
	{
		return false;
	}

	return true;
}

public boolean isSelected()
{
	return selected;
}

public void setSelected( boolean b )
{
	selected = b;
}

public int getWholesaleValue()
{
	return wholesaleValue;
}

public void setWholesaleValue( int i )
{
	wholesaleValue = i;
}

public static GuideBookOption findInArrayListByKey( Collection guideBookOptions, String key )
{
	Iterator iterator = guideBookOptions.iterator();
	GuideBookOption gbo = null;
	boolean found = false;

	while ( iterator.hasNext() )
	{
		gbo = (GuideBookOption)iterator.next();
		if ( gbo.getKey() != null && gbo.getKey().equals( key ) )
		{
			found = true;
			break;
		}
	}

	if ( found )
		return gbo;
	else
		return null;
}

public int getTabOrder()
{
	return tabOrder;
}

public void setTabOrder( int i )
{
	tabOrder = i;
}

public boolean isStandardOption()
{
	return standardOption;
}

public void setStandardOption( boolean standardOption )
{
	this.standardOption = standardOption;
}

public int getOptionType()
{
	return optionType;
}

public void setOptionType( int optionType )
{
	this.optionType = optionType;
}

public String toString()
{
	return "GUIDE BOOK OPTION: Description: "
			+ description + " Key: " + key + " Selected: " + selected + " Value: " + value + " Type: " + optionType;
}
}
