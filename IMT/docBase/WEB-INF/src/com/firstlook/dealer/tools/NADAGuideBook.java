package com.firstlook.dealer.tools;

import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;

import com.firstlook.entity.form.NADAGuideBookVehicleForm;

public class NADAGuideBook extends GuideBook
{

public NADAGuideBook()
{
    super(GuideBook.GUIDE_TYPE_NADA, GuideBook.GUIDE_TITLE_NADA, ThirdPartyDataProvider.IMAGE_NAME_NADA);
}

public String getDescriptionFormatted( GuideBookVehicle vehicle )
{
    return (vehicle.getSeries() + " " + vehicle.getBody());
}

public String getDescriptionFormattedWithBreak( GuideBookVehicle vehicle )
{
    return (vehicle.getSeries() + "<br>" + vehicle.getBody());
}

public Class getVehicleFormClass()
{
    return NADAGuideBookVehicleForm.class;
}

}
