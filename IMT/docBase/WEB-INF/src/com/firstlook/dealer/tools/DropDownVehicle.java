package com.firstlook.dealer.tools;

public class DropDownVehicle
{

private String year;
private String make;
private String model;
private String trim;
private String series;
private String makeCode;
private String modelCode;

public DropDownVehicle()
{
    super();
}

public java.lang.String getMake()
{
    return make;
}

public java.lang.String getModel()
{
    return model;
}

public java.lang.String getSeries()
{
    return series;
}

public java.lang.String getTrim()
{
    return trim;
}

public java.lang.String getYear()
{
    return year;
}

public void setMake( String newMake )
{
    make = newMake;
}

public void setModel( String newModel )
{
    model = newModel;
}

public void setSeries( String newSeries )
{
    series = newSeries;
}

public void setTrim( String newTrim )
{
    trim = newTrim;
}

public void setYear( String newYear )
{
    year = newYear;
}

public String getMakeCode()
{
	return makeCode;
}

public void setMakeCode( String makeCode )
{
	this.makeCode = makeCode;
}

public String getModelCode()
{
	return modelCode;
}

public void setModelCode( String modelCode )
{
	this.modelCode = modelCode;
}

}
