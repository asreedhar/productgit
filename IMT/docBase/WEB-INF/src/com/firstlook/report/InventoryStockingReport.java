package com.firstlook.report;

public class InventoryStockingReport
{

private int segmentId;
private int units;
private int daySupplyTarget;
private int unitsOverUnder;
private String segment;

public InventoryStockingReport()
{
    super();
}

public int getDaySupplyTarget()
{
    return daySupplyTarget;
}

public int getUnits()
{
    return units;
}

public int getUnitsOverUnder()
{
    return unitsOverUnder;
}

public void setDaySupplyTarget( int i )
{
    daySupplyTarget = i;
}

public void setUnits( int i )
{
    units = i;
}

public void setUnitsOverUnder( int i )
{
    unitsOverUnder = i;
}

public String getSegment() {
	return segment;
}

public void setSegment(String segment) {
	this.segment = segment;
}

public int getSegmentId() {
	return segmentId;
}

public void setSegmentId(int segmentId) {
	this.segmentId = segmentId;
}



}
