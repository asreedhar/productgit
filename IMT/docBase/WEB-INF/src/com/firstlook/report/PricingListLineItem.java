package com.firstlook.report;


public class PricingListLineItem
{

private String vehicleDescription;
private int vehicleYear;
private int mileageReceived;
private String stockNumber;
private double listPrice;
private int lotPrice;
private String pricingNotes;
private String baseColor;
private String segment;
private boolean repriced;


public PricingListLineItem()
{
}

public String getBaseColor()
{
	return baseColor;
}
public void setBaseColor( String baseColor )
{
	this.baseColor = baseColor;
}

public double getListPrice()
{
	return listPrice;
}
public void setListPrice( double listPrice )
{
	this.listPrice = listPrice;
}
public int getMileageReceived()
{
	return mileageReceived;
}
public void setMileageReceived( int mileageReceived )
{
	this.mileageReceived = mileageReceived;
}
public String getStockNumber()
{
	return stockNumber;
}
public void setStockNumber( String stockNumber )
{
	this.stockNumber = stockNumber;
}
public String getVehicleDescription()
{
	return vehicleDescription;
}
public void setVehicleDescription( String vehicleDescription )
{
	this.vehicleDescription = vehicleDescription;
}
public int getVehicleYear()
{
	return vehicleYear;
}
public void setVehicleYear( int vehicleYear )
{
	this.vehicleYear = vehicleYear;
}
public boolean isRepriced()
{
	return repriced;
}
public void setRepriced( boolean repriced )
{
	this.repriced = repriced;
}

public int getLotPrice() {
	return lotPrice;
}

public void setLotPrice(int lotPrice) {
	this.lotPrice = lotPrice;
}

public String getPricingNotes() {
	return pricingNotes;
}

public void setPricingNotes(String pricingNotes) {
	this.pricingNotes = pricingNotes;
}

public String getSegment() {
	return segment;
}

public void setSegment(String segment) {
	this.segment = segment;
}


}
