package com.firstlook.report;

import java.util.Iterator;
import java.util.List;
import java.util.Map;


import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryBucket;
import com.firstlook.entity.InventorySalesAggregate;
import com.firstlook.entity.InventoryStatusCD;
import com.firstlook.entity.lite.IInventory;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.inventory.InventoryService_Legacy;

public class AgingReport extends BaseAgingReport {

	public AgingReport() {super(); }
	/**
	 * used in TIR and Inventory Manager(Dashboard)
	 * @param dealer
	 * @param lite
	 * @param statusCodes
	 * @param rangeSet
	 * @param aggregate
	 * @throws DatabaseException
	 * @throws ApplicationException
	 */
	public AgingReport(Dealer dealer, boolean lite, String[] statusCodes,
			InventoryBucket rangeSet, InventorySalesAggregate aggregate, InventoryService_Legacy inventoryService_Legacy)
			throws DatabaseException, ApplicationException {
		super(dealer, lite, statusCodes, rangeSet, aggregate, inventoryService_Legacy);
		// weeks arg is NOT used in this implementation of
		// createAgingReportLineItems so we set to -1
		createAgingReportLineItems(-1);
	}

	public void createAgingReportLineItems(int weeks) {
		Map<Integer, InventoryStatusCD> statusCodes = inventoryService_Legacy.retrieveInventoryStatusCodes();
		for (int rangeIndex = 0; rangeIndex < ranges.size(); rangeIndex++) {
			DisplayDateRange<IInventory> range = (DisplayDateRange<IInventory>) ranges.elementAt(rangeIndex);
			List<IInventory> inventoryItems = (List<IInventory>) inventories.get(range.getRangeId());
			if (inventoryItems != null) {
				Iterator<IInventory> iterator = inventoryItems.iterator();
				while (iterator.hasNext()) {
					IInventory inventory = iterator.next();
					InventoryStatusCD statusCd = ((InventoryStatusCD) statusCodes.get(inventory.getStatusCode()));
					inventory.setStatusDescription(statusCd.getShortDescription());
					range.vehicles.add(inventory);
				}
			}
		}
	}

	public void setRange(int rangeId) { }

}
