package com.firstlook.report;

import java.text.NumberFormat;
import java.util.List;

import com.firstlook.iterator.SimpleFormIterator;
import com.firstlook.util.Formatter;

public class ReportForm extends com.firstlook.action.BaseActionForm
{
private static final long serialVersionUID = -3378756033292741034L;
private Report report = new Report();

public ReportForm( Report newReport )
{
    super();
    report = newReport;
}

public String getDaysToSaleTotalAvg()
{
    Integer daysToSale = report.getDaysToSaleTotalAvg();
    if ( daysToSale != null )
    {
        return daysToSale.toString();
    } else
    {
        return "n/a";
    }
}

public SimpleFormIterator getFastestSellerReportGroupings()
{
    return report.getFastestSellerReportGroupingsFormIterator();
}

public SimpleFormIterator getReportGroupingSortedPercentageBackEnd()
{
    return report.getReportGroupingsFormIteratorSorted("totalBackEnd");
}

public SimpleFormIterator getReportGroupingSortedPercentageFrontEnd()
{
    return report.getReportGroupingsFormIteratorSorted("totalFrontEnd");
}

public SimpleFormIterator getReportGroupingSortedPercentageTotalRevenue()
{
    return report.getReportGroupingsFormIteratorSorted("totalRevenue");
}

public SimpleFormIterator getReportGroupingSortedPercentageTotalGrossProfit()
{
    return report.getReportGroupingsFormIteratorSorted("totalGrossProfit");
}

public SimpleFormIterator getReportGroupingsSortedAvgFrontEnd()
{
    return report.getReportGroupingsFormIteratorSorted("averageFrontEnd");
}

public SimpleFormIterator getReportGroupingsSortedAvgBackEnd()
{
    return report.getReportGroupingsFormIteratorSorted("averageBackEnd");
}

public SimpleFormIterator getReportGroupingsSortedAvgTotalGross()
{
    return report.getReportGroupingsFormIteratorSorted("averageTotalGross");
}

public java.lang.String getGrossProfitTotalAvg()
{
    Integer average = report.getGrossProfitTotalAvg();
    if ( average != null )
    {
        NumberFormat formatter = NumberFormat.getInstance();
        formatter.setGroupingUsed(true);
        String profit = "$" + formatter.format(average);
        return profit;

    } else
    {
        return "n/a";
    }
}

public String getMileageTotalAvg()
{
    Integer mileage = report.getMileageTotalAvg();
    if ( mileage != null )
    {
        return Formatter.formatNumberToString(mileage.intValue());
    } else
    {
        return "n/a";
    }

}

public SimpleFormIterator getMostProfitableReportGroupings()
{
    return report.getMostProfitableReportGroupingsFormIterator();
}

public SimpleFormIterator getMostProfitableReportGroupingsInOptimix()
{
    return report
            .getMostProfitableReportGroupingsFormIterator(Report.OPTIMIX_MODE);
}

public int getNoSales()
{
    return report.getNoSales();
}

public Report getReport()
{
    return report;
}

public List getReportGroupings()
{
    return report.getReportGroupings();
}

public SimpleFormIterator getTopSellerReportGroupings()
{
    return report.getTopSellerReportGroupingsFormIterator(Report.STANDARD_MODE);
}

public SimpleFormIterator getTopSellerReportGroupingsInOptimix()
{
    return report.getTopSellerReportGroupingsFormIterator(Report.OPTIMIX_MODE);
}

public int getUnitsInStockTotalAvg()
{
    return report.getUnitsInStockTotalAvg();
}

public int getUnitsSoldTotalAvg()
{
    return report.getUnitsSoldTotalAvg();
}

public String getUnitsSoldFormatted()
{
    return Formatter.formatNumberToString(getUnitsSoldTotalAvg());
}

public int getWeeks()
{
    return report.getWeeks();
}

public void setBusinessObject( Object bo )
{
    report = (Report) bo;
}

public void setDaysToSaleTotalAvg( String newDaysToSaleTotalAvg )
{
    report.setDaysToSaleTotalAvg(Integer.getInteger(newDaysToSaleTotalAvg));
}

public void setGrossProfitTotalAvg( String newGrossProfitTotalAvg )
{
    report.setGrossProfitTotalAvg(Integer.getInteger(newGrossProfitTotalAvg));
}

public void setMileageTotalAvg( Integer newMileageTotalAvg )
{
    report.setMileageTotalAvg(newMileageTotalAvg);
}

public void setMileageTotalAvg( String newMileageTotalAvg )
{
    report.setMileageTotalAvg(Integer.getInteger(newMileageTotalAvg));
}

public void setNoSales( int newNoSales )
{
    report.setNoSales(newNoSales);
}

public void setReport( Report newReport )
{
    report = newReport;
}

public void setReportGroupings( java.util.Vector newReportGroupings )
{
    report.setReportGroupings(newReportGroupings);
}

public void setUnitsInStockTotalAvg( int newUnitsInStockTotalAvg )
{
    report.setUnitsInStockTotalAvg(newUnitsInStockTotalAvg);
}

public void setUnitsSoldTotalAvg( int newUnitsSoldTotalAvg )
{
    report.setUnitsSoldTotalAvg(newUnitsSoldTotalAvg);
}

public void setWeeks( int newWeeks )
{
    report.setWeeks(newWeeks);
}

public Integer getAverageFrontEnd()
{
    return report.getAverageFrontEnd();
}

public String getAverageFrontEndFormatted()
{
    return Formatter.toPrice(formatIntegerToIntValue(getAverageFrontEnd()));
}

public void setAverageFrontEnd( Integer pAverageFrontEnd )
{
    report.setAverageFrontEnd(pAverageFrontEnd);
}

public Integer getAverageBackEnd()
{
    return report.getAverageBackEnd();
}

public String getAverageBackEndFormatted()
{

    return Formatter.toPrice(getAverageBackEnd().intValue());
}

public void setAverageBackEnd( Integer pAverageBackEnd )
{
    report.setAverageBackEnd(pAverageBackEnd);
}

public Integer getAverageTotalGross()
{
    return report.getAverageTotalGross();
}

public String getAverageTotalGrossFormatted()
{
    return Formatter.toPrice(formatIntegerToIntValue(getAverageTotalGross()));
}

public void setAverageTotalGross( Integer pAverageTotalGross )
{
    report.setAverageTotalGross(pAverageTotalGross);
}

public void setTotalInventoryDollars( int totalInventoryDollars )
{
    report.setTotalInventoryDollars(totalInventoryDollars);
}

public void setTotalRevenue( int totalRevenue )
{
    report.setTotalRevenue(totalRevenue);
}

public void setTotalGrossMargin( Integer totalGrossMargin )
{
    report.setTotalGrossMargin(totalGrossMargin);
}

public int getTotalInventoryDollars()
{
    return report.getTotalInventoryDollars();
}

public String getTotalInventoryDollarsFormatted()
{
    return Formatter.toPrice(getTotalInventoryDollars());
}

public int getTotalRevenue()
{
    return report.getTotalRevenue();
}

public String getTotalRevenueFormatted()
{
    return Formatter.toPrice(getTotalRevenue());
}

public Integer getTotalGrossMargin()
{
    return report.getTotalGrossMargin();
}

public String getTotalGrossMarginFormatted()
{
    return Formatter.toPrice(formatIntegerToIntValue(getTotalGrossMargin()));
}

public String getTotalFrontEndFormatted()
{
    return formatPriceToString(report.getTotalFrontEnd());
}

public String getTotalBackEndFormatted()
{
    return formatPriceToString(report.getTotalBackEnd());
}

public int getUnitsSoldThreshold()
{
    return report.getUnitsSoldThreshold();
}

}
