package com.firstlook.report;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests extends junit.framework.TestCase
{

public AllTests( String arg1 )
{
    super(arg1);
}

public static Test suite()
{
    TestSuite suite = new TestSuite();
    suite.addTest(new TestSuite(TestBaseReportLineItemForm.class));
    suite.addTest(new TestSuite(TestCustomIndicator.class));
    suite.addTest(new TestSuite(TestFormattedDateForm.class));
    suite.addTest(new TestSuite(TestPAPReportLineItem.class));
    suite.addTest(new TestSuite(TestPAPReportLineItemForm.class));
    suite.addTest(new TestSuite(TestReport.class));
    suite.addTest(new TestSuite(TestReportForm.class));
    suite.addTest(new TestSuite(TestReportGrouping.class));
    suite.addTest(new TestSuite(TestReportGroupingForm.class));
    suite.addTest(new TestSuite(TestSalesReport.class));

    return suite;
}
}
