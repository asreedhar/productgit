package com.firstlook.report;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Vector;

import com.firstlook.mock.BaseTestCase;

public class TestCustomIndicator extends BaseTestCase
{

private CustomIndicator indicator;
private int threshold;
private java.util.ArrayList topSellerList;
private java.util.ArrayList mostProfitableList;
private java.util.ArrayList fastestSellerList;
private ArrayList hotlist;

public TestCustomIndicator( String arg1 )
{
    super(arg1);
}

public void setup()
{
    threshold = 10;

    indicator = new CustomIndicator();

    hotlist = new ArrayList(setupGroupingStringVector("Hotlist", 5));
    indicator.setHotlist(hotlist);

    topSellerList = new ArrayList(setupGroupingStringVector("TopSeller",
            threshold));
    indicator.setTopSellerList(topSellerList);

    fastestSellerList = new ArrayList(setupGroupingStringVector(
            "FastestSeller", threshold));
    indicator.setFastestSellerList(fastestSellerList);

    mostProfitableList = new ArrayList(setupGroupingStringVector(
            "MostProfitable", threshold));
    indicator.setMostProfitableSellerList(mostProfitableList);

}

private Vector setupGroupingStringVector( String groupPrefix, int size )
{
    Vector groupings = new Vector();

    for (int i = 0; i < size; i++)
    {
        groupings.add(groupPrefix + "Group" + i);
    }

    return groupings;
}

public Vector setupGroupingVector( int size )
{
    Vector groupings = new Vector();

    for (int i = 0; i < size; i++)
    {
        ReportGrouping group = new ReportGrouping();
        group.setGroupingName("Group" + i);
        groupings.add(group);
    }

    return groupings;
}

public void testGetFastestSellerNoRank()
{
    assertEquals(0, indicator.getFastestSellerRank("noRank"));
}

public void testGetFastestSellerRank()
{
    for (int i = 0; i < fastestSellerList.size(); i++)
    {
        int rank = indicator.getFastestSellerRank((String) fastestSellerList
                .get(i));
        assertEquals((String) fastestSellerList.get(i) + " -- wrong ranking ",
                i + 1, rank);
    }
}

public void testGetMostProfitableSellerNoRank()
{
    assertEquals(0, indicator.getMostProfitableSellerRank("noRank"));
}

public void testGetMostProfitableSellerRank()
{
    for (int i = 0; i < mostProfitableList.size(); i++)
    {
        int rank = indicator
                .getMostProfitableSellerRank((String) mostProfitableList.get(i));
        assertEquals((String) mostProfitableList.get(i) + " -- wrong ranking ",
                i + 1, rank);
    }
}

public void testGetRankedGroupingDescriptions()
{
    String extraTopSellerGroupingDescription = "ExtraTopSellerGroupingDescription";
    topSellerList = new ArrayList(setupGroupingStringVector("Duplicate",
            threshold));
    topSellerList.add(extraTopSellerGroupingDescription);
    indicator.setTopSellerList(topSellerList);

    String extraFastestSellerGroupingDescription = "ExtraFastestSellerGroupingDescription";
    fastestSellerList = new ArrayList(setupGroupingStringVector("Duplicate",
            threshold));
    fastestSellerList.add(extraFastestSellerGroupingDescription);
    indicator.setFastestSellerList(fastestSellerList);

    String extraMostProfitableGroupingDescription = "ExtraMostProfitableGroupingDescription";
    mostProfitableList = new ArrayList(setupGroupingStringVector("Duplicate",
            threshold));
    mostProfitableList.add(extraMostProfitableGroupingDescription);
    indicator.setMostProfitableSellerList(mostProfitableList);

    Collection rankedGroupingDescriptions = indicator
            .getRankedGroupingDescriptions();
    Vector vector = new Vector(rankedGroupingDescriptions);

    assertEquals("size", threshold + 3, vector.size());
    assertTrue("contains all TopSellers", vector.containsAll(topSellerList));
    assertTrue("contains all FastestSellers", vector
            .containsAll(fastestSellerList));
    assertTrue("contains all MostProfitable", vector
            .containsAll(mostProfitableList));
}

public void testGetTopSellerNoRank()
{

    assertEquals(0, indicator.getTopSellerRank("Not Found"));

}

public void testGetTopSellerRank()
{
    for (int i = 0; i < topSellerList.size(); i++)
    {
        int rank = indicator.getTopSellerRank((String) topSellerList.get(i));
        assertEquals((String) topSellerList.get(i) + " -- wrong ranking ",
                i + 1, rank);
    }
}

public void testGetUnitsInStockWithGroupNameNotFound()
{
    assertEquals(0, indicator.getUnitsInStock("groupNameNotFound"));
}

public void testGroupIsRanked()
{
    assertTrue(indicator.isGroupRanked((String) topSellerList.get(1)));
}

public void testGroupNotRanked()
{
    assertTrue(!indicator.isGroupRanked("not ranked "));
}

public void testIsInHotlistFalse()
{
    assertTrue(!indicator.isInHotlist("NotInHotlistGroup"));
}

public void testIsInHotlistTrue()
{
    assertTrue(indicator.isInHotlist("HotlistGroup1"));
}

public void testLoadReportGroupingList()
{
    Vector groupings = setupGroupingVector(threshold);

    ArrayList list = indicator.loadReportGroupingList(groupings);

    assertEquals("list Size is incorrect", indicator.getThreshold(), list
            .size());

    for (int j = 0; j < list.size(); j++)
    {
        ReportGrouping testGroup = (ReportGrouping) groupings.elementAt(j);
        assertEquals(testGroup.getGroupingName(), (String) list.get(j));
    }
}

public void testLoadReportGroupingListWithEmptyCollection()
{
    Vector groupings = new Vector();

    ArrayList list = indicator.loadReportGroupingList(groupings);

    assertEquals("list Size is incorrect", 0, list.size());

    assertEquals(-1, list.indexOf("nothing"));
}

public void testLoadUnitsInStockTable()
{
    Vector groupings = setupGroupingVector(10);

    Hashtable stockTable = indicator.loadUnitsInStockTable(groupings);

    assertEquals("list Size is incorrect", 10, stockTable.size());

}
}
