package com.firstlook.report.service;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import com.firstlook.entity.ReportCenterSession;
import com.firstlook.persistence.report.ReportCenterSessionDAO;

public class ReportCenterSessionManager
{

private TransactionTemplate transactionTemplate;
private ReportCenterSessionDAO reportCenterSessionDAO;

public String transferToReportCenter( final String sessionId, final String rcSessionId, final Integer businessUnitId, final Integer memberId )
{
	return (String)transactionTemplate.execute( new TransactionCallback()
	{
		public Object doInTransaction( TransactionStatus status )
		{
			ReportCenterSession reportCenterSession = reportCenterSessionDAO.retrieveBy( memberId );
			// if its not null, session exists, set do authentication to 0 then create new session
			if ( reportCenterSession != null )
			{
				// set do authentication to 0
				reportCenterSession.setDoAuthentication( new Integer( 0 ) );
				reportCenterSessionDAO.saveOrUpdate( reportCenterSession );
			}
			reportCenterSession = new ReportCenterSession( sessionId, rcSessionId, businessUnitId, memberId );
			reportCenterSessionDAO.saveOrUpdate( reportCenterSession );
			return "New report center session created";

		}
	} );
}

public void exitStore( final String sessionId, final Integer businessUnitId, final Integer memberId )
{
	transactionTemplate.execute( new TransactionCallback()
	{
		public Object doInTransaction( TransactionStatus status )
		{
			ReportCenterSession reportCenterSession = reportCenterSessionDAO.retrieveBy( memberId );

			if ( reportCenterSession != null ) {
				reportCenterSessionDAO.delete( reportCenterSession );
			}

			return null;
		}
	} );
}

public ReportCenterSessionDAO getReportCenterSessionDAO()
{
	return reportCenterSessionDAO;
}

public void setReportCenterSessionDAO( ReportCenterSessionDAO reportCenterSessionDAO )
{
	this.reportCenterSessionDAO = reportCenterSessionDAO;
}

public TransactionTemplate getTransactionTemplate()
{
	return transactionTemplate;
}

public void setTransactionTemplate( TransactionTemplate transactionTemplate )
{
	this.transactionTemplate = transactionTemplate;
}
}
