package com.firstlook.report;

import com.firstlook.mock.BaseTestCase;

public class TestPAPReportLineItem extends BaseTestCase
{

private PAPReportLineItem item;

public TestPAPReportLineItem( String arg1 )
{
    super(arg1);
}

public void setup()
{
    item = new PAPReportLineItem();
}

public void testDetermineVehicleTrimNull()
{
    String vehicleTrim = null;

    assertEquals("N/A", PAPReportLineItem.determineVehicleTrim(vehicleTrim));
}

public void testDetermineVehicleTrimEmptyString()
{
    String vehicleTrim = "";

    assertEquals("N/A", PAPReportLineItem.determineVehicleTrim(vehicleTrim));
}

public void testDetermineVehicleTrim()
{
    String vehicleTrim = "vehicleTrim";

    assertEquals("vehicleTrim", PAPReportLineItem
            .determineVehicleTrim(vehicleTrim));
}

}
