package com.firstlook.report;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.exception.ApplicationException;

public class CustomIndicator implements ICustomIndicator
{

private int threshold;
private ArrayList topSellerList;
public final static int TOP_TEN = 10;
public final static int NOT_RANKED = 0;
private ArrayList fastestSellerList;
private ArrayList mostProfitableSellerList;
private ArrayList hotlist = new ArrayList();
private Hashtable unitsInStockTable = new Hashtable();
private Report report;

public CustomIndicator()
{
}

public CustomIndicator( Dealer dealer, Report report, int lowestRank )
        throws DatabaseException, ApplicationException
{
    this();
    this.threshold = lowestRank;
    this.report = report;

    topSellerList = loadReportGroupingList(report.getTopSellerReportGroupings());
    fastestSellerList = loadReportGroupingList(report
            .getFastestSellerReportGroupings());
    mostProfitableSellerList = loadReportGroupingList(report
            .getMostProfitableReportGroupings());
    unitsInStockTable = loadUnitsInStockTable(report.getReportGroupings());
}

public ArrayList getFastestSellerList()
{
    return fastestSellerList;
}

public int getFastestSellerRank( String groupName )
{
    return fastestSellerList.indexOf(groupName) + 1;
}

public ArrayList getHotlist()
{
    return hotlist;
}

public ArrayList getMostProfitableSellerList()
{
    return mostProfitableSellerList;
}

public int getMostProfitableSellerRank( String groupName )
{
    return mostProfitableSellerList.indexOf(groupName) + 1;
}

public Collection getRankedGroupingDescriptions()
{
    SortedSet rankedGroupingDescriptions = new TreeSet();
    rankedGroupingDescriptions.addAll(topSellerList);
    rankedGroupingDescriptions.addAll(fastestSellerList);
    rankedGroupingDescriptions.addAll(mostProfitableSellerList);
    return (Collection) rankedGroupingDescriptions;
}

public Report getReport()
{
    return report;
}

public int getThreshold()
{
    return threshold;
}

public ArrayList getTopSellerList()
{
    return topSellerList;
}

public int getTopSellerRank( String groupName )
{
    return topSellerList.indexOf(groupName) + 1;
}

public int getUnitsInStock( String groupName )
{
    int unitsInStock = 0;
    Object stockItem = unitsInStockTable.get(groupName);
    if ( stockItem != null )
    {
        unitsInStock = ((Integer) stockItem).intValue();
    }

    return unitsInStock;
}

public boolean isGroupRanked( String group )
{
    return (getFastestSellerRank(group) + getMostProfitableSellerRank(group)
            + getTopSellerRank(group) > 0);
}

public boolean isInHotlist( String groupingDescription )
{
    return (hotlist.indexOf(groupingDescription) >= 0);
}

ArrayList loadReportGroupingList( Collection groupings )
{
    Iterator iterator = groupings.iterator();

    ArrayList list = new ArrayList(threshold);

    for (int i = 0; i < threshold && iterator.hasNext(); i++)
    {
        ReportGrouping group = (ReportGrouping) iterator.next();
        list.add(group.getGroupingName());
    }

    return list;
}

Hashtable loadUnitsInStockTable( Collection groupings )
{
    Iterator iterator = groupings.iterator();
    Hashtable unitsInStockTable = new Hashtable();
    while (iterator.hasNext())
    {
        ReportGrouping grouping = (ReportGrouping) iterator.next();
        if ( grouping.getGroupingName() != null )
        {
            unitsInStockTable.put(grouping.getGroupingName(), new Integer(
                    grouping.getUnitsInStock()));
        }
    }
    return unitsInStockTable;
}

public void setFastestSellerList( ArrayList newFastestSellerList )
{
    fastestSellerList = newFastestSellerList;
}

public void setHotlist( ArrayList newHotlist )
{
    hotlist = newHotlist;
}

public void setMostProfitableSellerList( ArrayList newMostProfitableSellerList )
{
    mostProfitableSellerList = newMostProfitableSellerList;
}

public void setThreshold( int newThreshold )
{
    threshold = newThreshold;
}

public void setTopSellerList( ArrayList newTopSellerList )
{
    topSellerList = newTopSellerList;
}

}
