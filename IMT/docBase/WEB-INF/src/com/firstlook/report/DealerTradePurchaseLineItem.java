package com.firstlook.report;

import java.util.Date;

public class DealerTradePurchaseLineItem
{

private Date receivedDate;
private String vin;
private String vehicleDescription;
private int mileage;
private double unitCost;
private Date tradeAnalyzerDate;
private String priorOwner;
private Date dateGroupReceived;
private String tradeOrPurchase;
private int light;
private transient boolean taAfterReceivedDate;
private String disposition;
private Integer daysInInventory;

public DealerTradePurchaseLineItem()
{
    super();
}

public Date getDateGroupReceived()
{
    return dateGroupReceived;
}

public int getMileage()
{
    return mileage;
}

public String getPriorOwner()
{
    return priorOwner;
}

public Date getReceivedDate()
{
    return receivedDate;
}

public Date getTradeAnalyzerDate()
{
    return tradeAnalyzerDate;
}

public String getTradeOrPurchase()
{
    return tradeOrPurchase;
}

public double getUnitCost()
{
    return unitCost;
}

public String getVehicleDescription()
{
    return vehicleDescription;
}

public String getVin()
{
    return vin;
}

public void setDateGroupReceived( Date date )
{
    dateGroupReceived = date;
}

public void setMileage( int i )
{
    mileage = i;
}

public void setPriorOwner( String string )
{
    priorOwner = string;
}

public void setReceivedDate( Date date )
{
    receivedDate = date;
}

public void setTradeAnalyzerDate( Date date )
{
    tradeAnalyzerDate = date;
}

public void setTradeOrPurchase( String string )
{
    tradeOrPurchase = string;
}

public void setUnitCost( double d )
{
    unitCost = d;
}

public void setVehicleDescription( String string )
{
    vehicleDescription = string;
}

public void setVin( String string )
{
    vin = string;
}

public int getLight()
{
    return light;
}

public void setLight( int i )
{
    light = i;
}

public boolean isTaAfterReceivedDate()
{
    return taAfterReceivedDate;
}

public void setTaAfterReceivedDate( boolean b )
{
    taAfterReceivedDate = b;
}

public String getTaAfterReceivedDate()
{
    Boolean taAfterRecvdDate = new Boolean(taAfterReceivedDate);
    return taAfterRecvdDate.toString();
}

public String getDisposition()
{
    return disposition;
}

public void setDisposition( String string )
{
    disposition = string;
}

public Integer getDaysInInventory()
{
    return daysInInventory;
}

public void setDaysInInventory( Integer integer)
{
    daysInInventory = integer;
}

}
