package com.firstlook.report;

import com.firstlook.action.BaseActionForm;

public class DealerTradePurchaseLineItemForm extends BaseActionForm
{

private static final long serialVersionUID = 4464578366988653157L;
private DealerTradePurchaseLineItem item;

public DealerTradePurchaseLineItemForm()
{
	super();
}

public DealerTradePurchaseLineItemForm( DealerTradePurchaseLineItem lineItem )
{
	super();
	setBusinessObject( lineItem );
}

public void setBusinessObject( Object bo )
{
	item = (DealerTradePurchaseLineItem)bo;
}

public String getDateGroupReceivedFormatted()
{
	if ( item.getDateGroupReceived() != null )
	{
		return formatDateToString( item.getDateGroupReceived() );
	}
	else
	{
		return null;
	}
}

public String getMileage()
{
	return String.valueOf( item.getMileage() );
}

public String getPriorOwner()
{
	return item.getPriorOwner();
}

public String getReceivedDate()
{
	return formatDateToString( item.getReceivedDate() );
}

public String getTradeAnalyzerDate()
{
	return formatDateToString( item.getTradeAnalyzerDate() );
}

public boolean isTaAfterReceivedDate()
{
	return item.isTaAfterReceivedDate();
}

public String getTaAfterReceivedDate()
{
	return item.getTaAfterReceivedDate();
}

public String getTradeOrPurchase()
{
	return item.getTradeOrPurchase();
}

public String getTradeOrPurchaseFormatted()
{
	if ( item.getTradeOrPurchase().equals( "true" ) )
	{
		return "P";
	}
	else if ( item.getTradeOrPurchase().equals( "false" ) )
	{
		return "T";
	}
	else
	{
		return null;
	}
}

public String getUnitCostFormatted()
{
	return formatPriceToString( item.getUnitCost() );
}

public String getVehicleDescription()
{
	return item.getVehicleDescription();
}

public String getVin()
{
	return item.getVin();
}

public int getLight()
{
	return item.getLight();
}

public String getDisposition()
{
	return item.getDisposition();
}

public Integer getDaysInInventory()
{
	return item.getDaysInInventory();
}

}