package com.firstlook.report;

import java.util.Collection;

public interface IAgeBandRange
{

public int getInventoryType();

public Collection getAgeBands();

}
