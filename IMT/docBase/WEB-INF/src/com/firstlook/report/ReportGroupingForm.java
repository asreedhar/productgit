package com.firstlook.report;

import com.firstlook.util.Formatter;

public class ReportGroupingForm extends BaseReportLineItemForm
{

private static final long serialVersionUID = 6098154032224437876L;
private ReportGrouping reportGrouping = new ReportGrouping();

public ReportGroupingForm()
{
}

public ReportGroupingForm( ReportGrouping newReportGrouping )
{
    reportGrouping = newReportGrouping;
}

public Integer getAvgDaysToSale()
{
    return reportGrouping.getAvgDaysToSale();
}

public Integer getPricePointAvgDaysToSale()
{
    return reportGrouping.getPricePointAvgDaysToSale();
}

public Integer getAvgGrossProfit()
{
    return reportGrouping.getAvgGrossProfit();
}

public Integer getPricePointAvgGrossProfit()
{
    return reportGrouping.getPricePointAvgGrossProfit();
}

public Integer getAvgMileage()
{
    return reportGrouping.getAvgMileage();
}

public Integer getAverageInventoryAge()
{
    return reportGrouping.getAverageInventoryAge();
}

public Integer getDaysSupply()
{
    return reportGrouping.getDaysSupply();
}

public int getGroupingId()
{
    return reportGrouping.getGroupingId();
}

public int getNoSales()
{
    return reportGrouping.getNoSales();
}

public String getGroupingName()
{
    return reportGrouping.getGroupingName();
}

public ReportGrouping getReportGrouping()
{
    return reportGrouping;
}

public int getUnitsInStock()
{
    return reportGrouping.getUnitsInStock();
}

public int getUnitsSold()
{
    return reportGrouping.getUnitsSold();
}

public int getPricePointUnitsSold()
{
    return reportGrouping.getPricePointUnitsSold();
}

public int getUnitsInMarket()
{
    return reportGrouping.getUnitsInMarket();
}

public void setAvgDaysToSale( String newAvgDaysToSale )
{
    reportGrouping.setAvgDaysToSale(Integer.getInteger(newAvgDaysToSale));
}

public void setAvgGrossProfit( String newAvgGrossProfit )
{
    reportGrouping.setAvgGrossProfit(Integer.getInteger(newAvgGrossProfit));
}

public void setAvgMileage( String newAvgMileage )
{
    reportGrouping.setAvgMileage(new Integer(newAvgMileage));
}

public void setAverageInventoryAge( String newAverageInventoryAge )
{
    reportGrouping.setAverageInventoryAge(new Integer(newAverageInventoryAge));
}

public void setDaysSupply( String newDaysSupply )
{
    reportGrouping.setDaysSupply(new Integer(newDaysSupply));
}

public void setBusinessObject( Object bo )
{
    reportGrouping = (ReportGrouping) bo;
}

public void setGroupingId( int newGroupingId )
{
    reportGrouping.setGroupingId(newGroupingId);
}

public void setNoSales( int newNoSales )
{
    reportGrouping.setNoSales(newNoSales);
}

public void setGroupingName( String newGroupingName )
{
    reportGrouping.setGroupingName(newGroupingName);
}

public void setReportGrouping( ReportGrouping newReportGrouping )
{
    reportGrouping = newReportGrouping;
}

public void setUnitsInStock( int newUnitsInStock )
{
    reportGrouping.setUnitsInStock(newUnitsInStock);
}

public void setUnitsSold( int newUnitsSold )
{
    reportGrouping.setUnitsSold(newUnitsSold);
}

public void setAverageFrontEnd( Integer integer )
{
    reportGrouping.setAverageFrontEnd(integer);
}

public void setAverageBackEnd( Integer integer )
{
    reportGrouping.setAverageBackEnd(integer);
}

public void setAverageTotalGross( Integer integer )
{
    reportGrouping.setAverageTotalGross(integer);
}

public Integer getAverageFrontEnd()
{
    return reportGrouping.getAverageFrontEnd();
}

public String getAverageFrontEndFormatted()
{

    return Formatter.toPrice(Formatter.getIntValue(getAverageFrontEnd()));
}

public Integer getAverageBackEnd()
{
    return reportGrouping.getAverageBackEnd();
}

public String getAverageBackEndFormatted()
{

    return Formatter.toPrice(Formatter.getIntValue(getAverageBackEnd()));
}

public Integer getAverageTotalGross()
{
    return reportGrouping.getAverageTotalGross();
}

public String getAverageTotalGrossFormatted()
{
    return Formatter.toPrice(Formatter.getIntValue(getAverageTotalGross()));
}

public Integer getTotalInventoryDollars()
{
    return reportGrouping.getTotalInventoryDollars();
}

public String getTotalInventoryDollarsFormatted()
{
    return Formatter.toPrice(Formatter.getIntValue(getTotalInventoryDollars()));
}

public Integer getTotalRevenue()
{
    return reportGrouping.getTotalRevenue();
}

public String getTotalRevenueFormatted()
{
    return Formatter.toPrice(Formatter.getIntValue(getTotalRevenue()));
}

public Integer getTotalGrossMargin()
{
    return reportGrouping.getTotalGrossMargin();
}

public String getTotalGrossMarginFormatted()
{
    return Formatter.toPrice(Formatter.getIntValue(getTotalGrossMargin()));
}

public void setTotalInventoryDollars( Integer totalInventoryDollars )
{
    reportGrouping.setTotalInventoryDollars(totalInventoryDollars);
}

public void setTotalRevenue( Integer totalRevenue )
{
    reportGrouping.setTotalRevenue(totalRevenue);
}

public void setTotalGrossMargin( Integer totalGrossMargin )
{
    reportGrouping.setTotalGrossMargin(totalGrossMargin);
}

public String getModel()
{
    return reportGrouping.getModel();
}

public String getMake()
{
    return reportGrouping.getMake();
}

public String getBodyType()
{
    return reportGrouping.getBodyType();
}

public String getVehicleTrim()
{
    return reportGrouping.getVehicleTrim();
}

public void setMake( String make )
{
    reportGrouping.setMake(make);
}

public void setModel( String string )
{
    reportGrouping.setModel(string);
}

public void setBodyType( String string )
{
    reportGrouping.setBodyType(string);
}

public void setVehicleTrim( String string )
{
    reportGrouping.setVehicleTrim(string);
}

public double getPercentageTotalGrossProfit()
{
    return reportGrouping.getPercentageTotalGrossProfit();
}

public double getPercentageTotalInventoryDollars()
{
    return reportGrouping.getPercentageTotalInventoryDollars();
}

public double getPercentageTotalRevenue()
{
    return reportGrouping.getPercentageTotalRevenue();
}

public double getPercentageFrontEnd()
{
    return reportGrouping.getPercentageFrontEnd();
}

public double getPercentageBackEnd()
{
    return reportGrouping.getPercentageBackEnd();
}

public String getPercentageTotalGrossProfitFormatted()
{
    return Formatter.toPercentOneDecimal(reportGrouping
            .getPercentageTotalGrossProfit());
}

public String getPercentageTotalInventoryDollarsFormatted()
{
    return Formatter.toPercentOneDecimal(reportGrouping
            .getPercentageTotalInventoryDollars());
}

public String getPercentageTotalRevenueFormatted()
{
    return Formatter.toPercentOneDecimal(reportGrouping
            .getPercentageTotalRevenue());
}

public String getPercentageFrontEndFormatted()
{
    return Formatter
            .toPercentOneDecimal(reportGrouping.getPercentageFrontEnd());
}

public String getPercentageBackEndFormatted()
{
    return Formatter.toPercentOneDecimal(reportGrouping.getPercentageBackEnd());
}

public int getBodyTypeId()
{
    return reportGrouping.getBodyTypeId();
}

}
