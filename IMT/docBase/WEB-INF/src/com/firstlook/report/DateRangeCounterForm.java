package com.firstlook.report;

import com.firstlook.action.BaseActionForm;
import com.firstlook.exception.ApplicationException;
import com.firstlook.iterator.BaseFormIterator;
import com.firstlook.iterator.InventoryFormIterator;
import com.firstlook.iterator.SimpleFormIterator;

public class DateRangeCounterForm extends BaseActionForm
{

private static final long serialVersionUID = -3057235034133690564L;
private DisplayDateRange dateRangeCounter;

public DateRangeCounterForm()
{
	super();
}

public String getCount()
{
	return Integer.toString( dateRangeCounter.getCount() );
}

public String getName()
{
	return dateRangeCounter.getName();
}

public SimpleFormIterator getVehicles()
{
	return new InventoryFormIterator( dateRangeCounter.getVehicles(), dateRangeCounter.getDealer() );
}

public BaseFormIterator getLineItems()
{
	return new BaseFormIterator( dateRangeCounter.getVehicles() );
}

public void setBusinessObject( Object object )
{
	dateRangeCounter = (DisplayDateRange)object;
}

public String getCustomName()
{
	return dateRangeCounter.getCustomName();
}

public int getTotalBookValue()
{
	return dateRangeCounter.getTotalBookValue();
}

public int getTotalBookValueSecond()
{
	return dateRangeCounter.getTotalBookValueSecond();
}

public int getTotalUnitCost()
{
	return dateRangeCounter.getTotalUnitCost();
}

public double getPercentageOfUnitCost() throws ApplicationException
{
	return dateRangeCounter.getPercentageOfUnitCost();
}

public double getPercentageOfInventory() throws ApplicationException
{
	return dateRangeCounter.getPercentageOfInventory();
}

public int getBook1VsCost()
{
	return getTotalBookValue() - dateRangeCounter.getTotalUnitCostForBook1();
}

public int getBook2VsCost()
{
	return getTotalBookValueSecond() - dateRangeCounter.getTotalUnitCostForBook2();
}

public boolean isHasCurrentVehiclePlan()
{
	return dateRangeCounter.isHasCurrentVehiclePlan();
}

}
