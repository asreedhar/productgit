package com.firstlook.report;

import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.commons.util.PropertyLoader;

import com.firstlook.data.DatabaseException;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryBucket;
import com.firstlook.entity.InventoryBucketRange;
import com.firstlook.entity.InventorySalesAggregate;
import com.firstlook.entity.lite.IInventory;
import com.firstlook.exception.ApplicationException;
import com.firstlook.iterator.SimpleFormIterator;
import com.firstlook.service.inventory.InventoryService_Legacy;
import com.firstlook.service.inventory.InventoryStatusCDService;

public abstract class BaseAgingReport
{

protected InventoryBucket rangeSet;
protected Vector<DisplayDateRange<IInventory>> ranges = new Vector<DisplayDateRange<IInventory>>();
protected Map<Integer, List<IInventory>> inventories;
protected int weekId;
private Dealer dealer;
private boolean prepopulated;
protected boolean applyPriorAgingNotes;

public static final Integer AGING_INVENTORY_REPORT_RANGE_SET = Integer.valueOf( 1 ); //Has watchlist
public static final Integer NEW_TOTAL_INVENTORY_REPORT_RANGE_SET = Integer.valueOf( 2 );
public static final Integer USED_TOTAL_INVENTORY_REPORT_RANGE_SET = Integer.valueOf( 3 ); //does not process watchlist logic

public static final String OFF_THE_CLIFF = PropertyLoader.getProperty( "firstlook.report.aginginventoryplanningreport.level1",
                                                                       "OFF THE CLIFF - EFFICIENTLY LIQUIDATE INVENTORY" );
public static final String AGGRESSIVELY_PURSUE_WHOLESALE_OPTIONS = PropertyLoader.getProperty(
                                                                                               "firstlook.report.aginginventoryplanningreport.level2",
                                                                                               "AGGRESSIVELY PURSUE WHOLESALE OPTIONS" );
public static final String FINAL_PUSH = PropertyLoader.getProperty( "firstlook.report.aginginventoryplanningreport.level3",
                                                                    "FINAL PUSH - AGGRESSIVE RETAIL STRATEGY<br> BEGIN PURSUING WHOLESALE OPTIONS" );
public static final String PURSUE_AGGRESSIVE = PropertyLoader.getProperty( "firstlook.report.aginginventoryplanningreport.level4",
                                                                           "PURSUE AGGRESSIVE RETAIL STRATEGIES" );
public static final String LOW_RISK = PropertyLoader.getProperty( "firstlook.report.aginginventoryplanningreport.level5",
                                                                  "LOW RISK VEHICLES (0-29 DAYS OLD)" );
public static final String HIGH_RISK_TRADE_IN = PropertyLoader.getProperty( "firstlook.report.aginginventoryplanningreport.level6",
                                                                            "HIGH RISK VEHICLES (0-15 DAYS)" );
public static final String HIGH_RISK = PropertyLoader.getProperty( "firstlook.report.aginginventoryplanningreport.level7",
                                                                   "HIGH RISK VEHICLES (16-29 DAYS OLD)" );
public static final int OFF_THE_CLIFF_ID = 1;
public static final int AGGRESSIVELY_PURSUE_WHOLESALE_OPTIONS_ID = 2;
public static final int FINAL_PUSH_ID = 3;
public static final int PURSUE_AGGRESSIVE_ID = 4;
public static final int LOW_RISK_ID = 5;
public static final int HIGH_RISK_TRADE_IN_ID = 6;
public static final int HIGH_RISK_ID = 7;

public static final int WATCH_LIST_ID = 6;
public static final int REPORTS_ID = 7;

public static final int NEW_CAR_NINETY_PLUS_ID = 1;
public static final int NEW_CAR_SEVENTY_SIX_TO_NINETY_ID = 2;
public static final int NEW_CAR_SIXTY_ONE_TO_SEVENTY_FIVE_ID = 3;
public static final int NEW_CAR_THIRTY_ONE_TO_SIXTY_ID = 4;
public static final int NEW_CAR_ZERO_TO_THIRTY_ID = 5;

// This is crappy.
// This class is not spring managed, but any callers should be and need to 
// pass in the spring managed inventory service to use.
protected InventoryService_Legacy inventoryService_Legacy; 

public BaseAgingReport(  )
{
}

/**
 * used in AIP, TIR and Inventory Manager(Dashboard)
 * @param dealer
 * @param lite
 * @param statusCodes
 * @param rangeSet
 * @param aggregate
 * @throws DatabaseException
 * @throws ApplicationException
 */
public BaseAgingReport( Dealer dealer, boolean lite, String[] statusCodes, InventoryBucket rangeSet,
                       InventorySalesAggregate aggregate, InventoryService_Legacy inventoryService ) throws DatabaseException, ApplicationException
{
    super();
    this.rangeSet = rangeSet;
    this.dealer = dealer;
    this.inventoryService_Legacy = inventoryService;
    setRanges( aggregate );
    setVehicles( dealer.getDealerId().intValue(), statusCodes);
}

/**
 * used in AIP
 * @param dealer
 * @param lite
 * @param statusCodes
 * @param rangeSet
 * @param aggregate
 * @throws DatabaseException
 * @throws ApplicationException
 */
public BaseAgingReport( Dealer dealer, int weekId, int rangeId, String[] statusCodes, boolean applyPriorAgingNotes,
                       InventoryBucket rangeSet, InventorySalesAggregate aggregate ) throws DatabaseException, ApplicationException
{
    super();
    this.weekId = weekId;
    this.applyPriorAgingNotes = applyPriorAgingNotes;
    this.rangeSet = rangeSet;
    chooseRangesForTab( rangeId, aggregate );
    setVehicles( dealer.getDealerId().intValue(), statusCodes);
    this.dealer = dealer;
}

/**
 * Used in AIP
 * @param dealer
 * @param weekId
 * @param rangeId
 * @param onlyVehicles
 * @param rangeSet
 * @throws DatabaseException
 * @throws ApplicationException
 */
public BaseAgingReport( Dealer dealer, int weekId, int rangeId, boolean onlyVehicles, InventoryBucket rangeSet ) throws DatabaseException,
                                                                                                                ApplicationException
{
    super();
    this.weekId = weekId;
    this.rangeSet = rangeSet;
    setRange( rangeId );
    setDealer( dealer );
    setVehicles( dealer.getDealerId().intValue(), null);
}

public abstract void createAgingReportLineItems( int weeks ) throws ApplicationException, DatabaseException;

public abstract void setRange( int rangeId );

public void setRanges( InventorySalesAggregate aggregate )
{
    Calendar calendar = Calendar.getInstance();
    calendar = DateUtils.truncate( calendar, Calendar.DATE );
    long now = DateUtilFL.dateInMillisWithDST( calendar.getTime() ) + calendar.get( Calendar.DST_OFFSET );

    Iterator<InventoryBucketRange> agingReportRangeIter = rangeSet.getInventoryBucketRanges().iterator();
    while ( agingReportRangeIter.hasNext() ) {
    	InventoryBucketRange agingRange = agingReportRangeIter.next();
    	DisplayDateRange<IInventory> dateRange = new DisplayDateRange<IInventory>( now, agingRange, dealer, aggregate.getTotalInventoryDollars(), aggregate.getUnitsInStock() );
        ranges.add( dateRange );
    }
    Collections.<DisplayDateRange<IInventory>>sort( ranges, new Comparator<DisplayDateRange<IInventory>>(){
		public int compare(DisplayDateRange<IInventory> arg0,
				DisplayDateRange<IInventory> arg1) {
			if(arg0 == null || arg1 == null)
				throw new NullPointerException("Cannot compare null DisplayDateRanges!");
			
			Integer rangeId0 = arg0.getRangeId();
			Integer rangeId1 = arg1.getRangeId();
			
			if( rangeId0 == null || rangeId1 == null)
				throw new NullPointerException("Cannot compare null RangeId on DisplayDateRanges!");
			
			return rangeId0.compareTo(rangeId1);
		}
	});
}

void chooseRangesForTab( int rangeId, InventorySalesAggregate aggregate )
{
    if ( rangeId == 0 )
    {
        setRanges( aggregate );
    }
    else
    {
        setRange( rangeId );
    }
}

public int getRangeCount()
{
    return ranges.size();
}

public int getRangeCountCheck()
{
    int count = 0;
    Iterator<DisplayDateRange<IInventory>> rangesIter = ranges.iterator();
    while ( rangesIter.hasNext() ) {
    	DisplayDateRange<IInventory> counter = rangesIter.next();
        if ( counter.getCount() > 0 ) {
            count++;
        }
    }
    return count;
}

public Vector<DisplayDateRange<IInventory>> getRanges()
{
    return ranges;
}

public InventoryBucketRange getSpecificRange( int rangeId )
{
    InventoryBucketRange range = null;
    Iterator<InventoryBucketRange> rangeIter = rangeSet.getInventoryBucketRanges().iterator();
    while ( rangeIter.hasNext() )
    {
        range = rangeIter.next();
        if ( range.getRangeId().intValue() == rangeId )
        {
            break;
        }
    }
    return range;
}

public SimpleFormIterator getRangesIterator()
{
    SimpleFormIterator rangeIterator = new SimpleFormIterator( ranges, DateRangeCounterForm.class );
    return rangeIterator;
}

public int getVehicleCount()
{
    int total = 0;
    Iterator<Integer> invIter = inventories.keySet().iterator();
    while ( invIter.hasNext() )
    {
        Integer id = invIter.next();
        List<IInventory> items = inventories.get( id );
        if(items != null) {
        	total += items.size();
        }
    }
    return total;
}

public Map<Integer, List<IInventory>> getInventories()
{
    return inventories;
}

public List<IInventory> getCurrentRangeVehicles( Integer rangeId )
{
    return inventories.get( rangeId );
}

protected void setVehicles( int dealerId, String[] statusCodes) throws DatabaseException, ApplicationException
{
    String statusCodesStr = null;

    InventoryStatusCDService inventoryStatusCdService = new InventoryStatusCDService();
    if ( statusCodes != null && statusCodes.length > 0 )
    {
        boolean isAny = inventoryStatusCdService.isStatusCodeAny( statusCodes );
        if ( !isAny )
        {
            statusCodesStr = StringUtils.join( statusCodes, "," );
        }
    }

    inventories = inventoryService_Legacy.retrieveBucketedInventoryItems( dealerId, statusCodesStr, rangeSet.getInventoryBucketId());

}

public void setDealer( Dealer dealer )
{
    this.dealer = dealer;
}

public Dealer getDealer()
{
    return dealer;
}

public boolean isPrepopulated()
{
    return prepopulated;
}

public void setPrepopulated( boolean b )
{
    prepopulated = b;
}

public void setApplyPriorAgingNotes( boolean applyPriorAgingNotes )
{
    this.applyPriorAgingNotes = applyPriorAgingNotes;
}


}
