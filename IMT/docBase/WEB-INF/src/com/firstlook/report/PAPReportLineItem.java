package com.firstlook.report;

import java.util.Collection;
import java.util.Iterator;

public class PAPReportLineItem
{
public final static boolean BLANK_TRUE = true;
public final static boolean BLANK_FALSE = false;

public final static String TRIM_GROUPING_PARAMETER = "vehicleTrim";
public final static String TRIM_JOIN_PARAMETER = "trim";
public final static String YEAR_GROUPING_PARAMETER = "vehicleYear";
public final static String YEAR_JOIN_PARAMETER = "vehicleYear";
public final static String COLOR_GROUPING_PARAMETER = "baseColor";
public final static String COLOR_JOIN_PARAMETER = "baseColor";
public final static String NO_COLOR = "NOCOLOR";

private int unitsInMarket;
private int ciaGroupingItemId;
private String groupingColumn;
private String groupingColumnFormatted;
private int lowRange;
private int highRange;
private int unitsSold;
private Integer avgGrossProfit;
private Integer avgDaysToSale;
private float percentageInMarket;
private int unitsInStock;
private Integer avgMileage;
private int noSales;
private boolean blankItem;
private int pricePointUnitsSold;
private Integer pricePointAvgGrossProfit;
private Integer pricePointAvgDaysToSale;
private int pricePointNoSales;
private int pricePointUnitsInStock;
private int totalRevenue;
private int totalGrossMargin;
private int totalInventoryDollars;
private double percentageTotalRevenue;
private double percentageTotalGrossProfit;
private double percentageTotalInventoryDollars;
private Integer averageBackEnd;
private int totalBackEnd;
private double percentageTotalBackEnd;
private boolean noValues;
private Integer ciaUnitCostPointRangeId;
private Double totalDaysToSaleUnrounded;
private Double totalFrontEndGrossProfitUnrounded;
private Double totalBackEndGrossProfitUnrounded;
private Double annualRoi;

public PAPReportLineItem()
{
    this(BLANK_FALSE);
}

public PAPReportLineItem( boolean blank )
{
    setBlankItem(blank);
}

public Integer getAvgDaysToSale()
{
    return avgDaysToSale;
}

public Integer getAvgGrossProfit()
{
    return avgGrossProfit;
}

public Integer getAvgMileage()
{
    return avgMileage;
}

public String getGroupingColumn()
{
    return groupingColumn;
}


public int getUnitsInStock()
{
    return unitsInStock;
}

public int getUnitsSold()
{
    return unitsSold;
}

public boolean isBlankItem()
{
    return blankItem;
}

public void setAvgDaysToSale( Integer newAvgDaysToSale )
{
    avgDaysToSale = newAvgDaysToSale;
}

public void setAvgGrossProfit( Integer newAvgGrossProfit )
{
    avgGrossProfit = newAvgGrossProfit;
}

public void setAvgMileage( Integer newAvgMileage )
{
    avgMileage = newAvgMileage;
}

public void setBlankItem( boolean newBlankItem )
{
    blankItem = newBlankItem;
}

public void setGroupingColumn( java.lang.String newGroupingColumn )
{
    groupingColumn = newGroupingColumn;
}
public String getGroupingColumnFormatted()
{
	return groupingColumnFormatted;
}
public void setGroupingColumnFormatted( String groupingColumnFormatted )
{
	this.groupingColumnFormatted = groupingColumnFormatted;
}
public void setUnitsInStock( int newUnitsInStock )
{
    unitsInStock = newUnitsInStock;
}

public void setUnitsSold( int newUnitsSold )
{
    unitsSold = newUnitsSold;
}

public int getNoSales()
{
    return noSales;
}

public void setNoSales( int noSales )
{
    this.noSales = noSales;
}

public float getPercentageInMarket()
{
    return percentageInMarket;
}

public void setPercentageInMarket( float percentageInMarket )
{
    this.percentageInMarket = percentageInMarket;
}

public void setUnitsInMarket( int unitsInMarket )
{
    this.unitsInMarket = unitsInMarket;
}

public int getUnitsInMarket()
{
    return unitsInMarket;
}

public Integer getPricePointAvgGrossProfit()
{
    return pricePointAvgGrossProfit;
}

public int getPricePointNoSales()
{
    return pricePointNoSales;
}

public int getPricePointUnitsInStock()
{
    return pricePointUnitsInStock;
}

public int getPricePointUnitsSold()
{
    return pricePointUnitsSold;
}

public void setPricePointAvgGrossProfit( Integer integer )
{
    pricePointAvgGrossProfit = integer;
}

public void setPricePointNoSales( int i )
{
    pricePointNoSales = i;
}

public void setPricePointUnitsInStock( int i )
{
    pricePointUnitsInStock = i;
}

public void setPricePointUnitsSold( int i )
{
    pricePointUnitsSold = i;
}

public Integer getPricePointAvgDaysToSale()
{
    return pricePointAvgDaysToSale;
}

public void setPricePointAvgDaysToSale( Integer integer )
{
    pricePointAvgDaysToSale = integer;
}

public int getTotalGrossMargin()
{
    return totalGrossMargin;
}

public int getTotalInventoryDollars()
{
    return totalInventoryDollars;
}

public int getTotalRevenue()
{
    return totalRevenue;
}

public void setTotalGrossMargin( int i )
{
    totalGrossMargin = i;
}

public void setTotalInventoryDollars( int i )
{
    totalInventoryDollars = i;
}

public void setTotalRevenue( int i )
{
    totalRevenue = i;
}

public static String determineVehicleTrim( String vehicleTrim )
{
    if ( vehicleTrim == null || vehicleTrim.equals("") )
    {
        return "N/A";
    } else
    {
        return vehicleTrim;
    }
}

public void calculateOptimixPercentages( PAPReportLineItem overall )
{
    int dealerTotalGrossMargin = overall.getTotalGrossMargin();
    int dealerTotalRevenue = overall.getTotalRevenue();
    int dealerTotalInventoryDollars = overall.getTotalInventoryDollars();
    int dealerTotalBackEnd = overall.getTotalBackEnd();

    if ( dealerTotalRevenue == 0 || getTotalRevenue() == 0 )
    {
        setPercentageTotalRevenue(0.0);
    } else
    {
        double percentTotalRevenue = ((double) getTotalRevenue())
                / ((double) dealerTotalRevenue);
        setPercentageTotalRevenue(percentTotalRevenue);
    }

    if ( dealerTotalGrossMargin == 0 || getTotalGrossMargin() == 0 )
    {
        setPercentageTotalGrossProfit(0.0);
    } else
    {
        double percentTotalGrossProfit = ((double) getTotalGrossMargin())
                / ((double) dealerTotalGrossMargin);
        setPercentageTotalGrossProfit(percentTotalGrossProfit);
    }

    if ( dealerTotalInventoryDollars == 0 || getTotalInventoryDollars() == 0 )
    {
        setPercentageTotalInventoryDollars(0.0);
    } else
    {
        double percentTotalInventoryDollars = ((double) getTotalInventoryDollars())
                / ((double) dealerTotalInventoryDollars);
        setPercentageTotalInventoryDollars(percentTotalInventoryDollars);
    }

    if ( dealerTotalBackEnd == 0 )
    {
        setPercentageTotalBackEnd(0.0);
    } else
    {
        double percentTotalBackEnd = ((double) getTotalBackEnd())
                / ((double) dealerTotalBackEnd);
        setPercentageTotalBackEnd(percentTotalBackEnd);
    }

}

public double getPercentageTotalGrossProfit()
{
    return percentageTotalGrossProfit;
}

public double getPercentageTotalInventoryDollars()
{
    return percentageTotalInventoryDollars;
}

public double getPercentageTotalRevenue()
{
    return percentageTotalRevenue;
}

public void setPercentageTotalGrossProfit( double d )
{
    percentageTotalGrossProfit = d;
}

public void setPercentageTotalInventoryDollars( double d )
{
    percentageTotalInventoryDollars = d;
}

public void setPercentageTotalRevenue( double d )
{
    percentageTotalRevenue = d;
}

public static void generateOptimixPercentages( PAPReportLineItem overall,
        Collection papReportLineItems )
{
    Iterator iter = papReportLineItems.iterator();
    while (iter.hasNext())
    {
        PAPReportLineItem papReportLineItem = (PAPReportLineItem) iter.next();

        papReportLineItem.calculateOptimixPercentages(overall);
    }
}

public Integer getAverageBackEnd()
{
    return averageBackEnd;
}

public void setAverageBackEnd( Integer integer )
{
    averageBackEnd = integer;
}

public double getPercentageTotalBackEnd()
{
    return percentageTotalBackEnd;
}

public int getTotalBackEnd()
{
    return totalBackEnd;
}

public void setPercentageTotalBackEnd( double d )
{
    percentageTotalBackEnd = d;
}

public void setTotalBackEnd( int i )
{
    totalBackEnd = i;
}

public boolean isNoValues()
{
    return noValues;
}

public void setNoValues( boolean b )
{
    noValues = b;
}

public Integer getCiaUnitCostPointRangeId()
{
    return ciaUnitCostPointRangeId;
}

public void setCiaUnitCostPointRangeId( Integer integer )
{
    ciaUnitCostPointRangeId = integer;
}

public Double getTotalBackEndGrossProfitUnrounded()
{
    return totalBackEndGrossProfitUnrounded;
}

public Double getTotalDaysToSaleUnrounded()
{
    return totalDaysToSaleUnrounded;
}

public Double getTotalFrontEndGrossProfitUnrounded()
{
    return totalFrontEndGrossProfitUnrounded;
}

public void setTotalBackEndGrossProfitUnrounded( Double d )
{
    totalBackEndGrossProfitUnrounded = d;
}

public void setTotalDaysToSaleUnrounded( Double d )
{
    totalDaysToSaleUnrounded = d;
}

public void setTotalFrontEndGrossProfitUnrounded( Double d )
{
    totalFrontEndGrossProfitUnrounded = d;
}

public int getCiaGroupingItemId()
{
	return ciaGroupingItemId;
}

public void setCiaGroupingItemId( int ciaGroupingItemId )
{
	this.ciaGroupingItemId = ciaGroupingItemId;
}

public int getHighRange()
{
	return highRange;
}

public void setHighRange( int highRange )
{
	this.highRange = highRange;
}

public int getLowRange()
{
	return lowRange;
}

public void setLowRange( int lowRange )
{
	this.lowRange = lowRange;
}

public Double getAnnualRoi()
{
	return annualRoi;
}

public void setAnnualRoi( Double annualRoi )
{
	this.annualRoi = annualRoi;
}


}
