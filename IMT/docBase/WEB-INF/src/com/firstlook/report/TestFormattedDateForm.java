package com.firstlook.report;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TestFormattedDateForm extends com.firstlook.mock.BaseTestCase
{
/**
 * TestPolledDateForm constructor comment.
 * 
 * @param arg1
 *            java.lang.String
 */
public TestFormattedDateForm( String arg1 )
{
    super(arg1);
}

public void testDateStringFormat()
{
    FormattedDateForm form = new FormattedDateForm(new Date(0));

    assertEquals("Wednesday, December 31, 1969", form.getFormattedDate());
}

public void testShortDateStringFormat()
{
    FormattedDateForm form = new FormattedDateForm(new Date(0), "MM/dd/yyyy");
    assertEquals("12/31/1969", form.getFormattedDate());
}

public void testNullDateStringFormat()
{
    FormattedDateForm form = new FormattedDateForm(null);
    SimpleDateFormat format = new SimpleDateFormat(form.dateFormat);
    assertEquals(format.format(new Date()), form.getFormattedDate());
}
}
