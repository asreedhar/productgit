package com.firstlook.report;

/**
 * Insert the type's description here. Creation date: (11/6/2001 5:21:49 PM)
 * 
 * @author: Extreme Developer
 */
public class TestReportForm extends junit.framework.TestCase
{
/**
 * TestReportForm constructor comment.
 * 
 * @param arg1
 *            java.lang.String
 */
public TestReportForm( String arg1 )
{
    super(arg1);
}

public void testGrossProfitTotalAvgStrFormat()
{
    Report report = new Report();
    report.setMileageTotalAvg(new Integer(1000));
    ReportForm reportForm = new ReportForm(report);
    assertEquals("1,000", reportForm.getMileageTotalAvg());
}

public void testMileageAvgStrFormat()
{
    Report report = new Report();
    report.setGrossProfitTotalAvg(new Integer(1000));
    ReportForm reportForm = new ReportForm(report);
    assertEquals("$1,000", reportForm.getGrossProfitTotalAvg());
}
}
