package com.firstlook.report;

import java.util.Iterator;
import java.util.List;

public class UnitCostPointLineItem
{
private int unitsSold;
private int avgGrossProfit;
private int avgDaysToSale;
private int noSales;
private int unitsInStock;
private String groupingColumn;
private int minAmount;
private int maxAmount;

public UnitCostPointLineItem()
{
}

public int getAvgDaysToSale()
{
    return avgDaysToSale;
}

public int getAvgGrossProfit()
{
    return avgGrossProfit;
}

public String getGroupingColumn()
{
    return groupingColumn;
}

public int getMaxAmount()
{
    return maxAmount;
}

public int getMinAmount()
{
    return minAmount;
}

public int getNoSales()
{
    return noSales;
}

public int getUnitsInStock()
{
    return unitsInStock;
}

public int getUnitsSold()
{
    return unitsSold;
}

public void setAvgDaysToSale( int i )
{
    avgDaysToSale = i;
}

public void setAvgGrossProfit( int i )
{
    avgGrossProfit = i;
}

public void setGroupingColumn( String string )
{
    groupingColumn = string;
}

public void setMaxAmount( int i )
{
    maxAmount = i;
}

public void setMinAmount( int i )
{
    minAmount = i;
}

public void setNoSales( int i )
{
    noSales = i;
}

public void setUnitsInStock( int i )
{
    unitsInStock = i;
}

public void setUnitsSold( int i )
{
    unitsSold = i;
}

public String getGroupingColumnFormatted()
{
    return groupingColumn.replaceAll("-", "xDASHx");
}

public static UnitCostPointLineItem calculateOverall( List lineItems )
{
    UnitCostPointLineItem overallLineItem = new UnitCostPointLineItem();

    int totalUnitsSold = 0;
    int totalRetailAverageGrossProfit = 0;
    int totalDaysToSale = 0;
    int totalNoSales = 0;
    int totalInStock = 0;
    Iterator lineItemsIter = lineItems.iterator();

    while (lineItemsIter.hasNext())
    {
        UnitCostPointLineItem lineItem = (UnitCostPointLineItem) lineItemsIter
                .next();
        totalUnitsSold += lineItem.getUnitsSold();
        totalRetailAverageGrossProfit += lineItem.getAvgGrossProfit()
                * lineItem.getUnitsSold();
        totalDaysToSale += lineItem.getAvgDaysToSale()
                * lineItem.getUnitsSold();
        totalNoSales += lineItem.getNoSales();
        totalInStock += lineItem.getUnitsInStock();
    }
    if ( totalUnitsSold > 0 )
    {
        overallLineItem.setUnitsSold(totalUnitsSold);
        overallLineItem
                .setAvgGrossProfit((int) Math
                        .round(totalRetailAverageGrossProfit
                                / (double) totalUnitsSold));
        overallLineItem.setAvgDaysToSale((int) Math.round(totalDaysToSale
                / (double) totalUnitsSold));
        overallLineItem.setNoSales(totalNoSales);
        overallLineItem.setUnitsInStock(totalInStock);
    }
    return overallLineItem;
}

}
