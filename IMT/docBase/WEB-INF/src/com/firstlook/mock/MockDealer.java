package com.firstlook.mock;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerGroup;

public class MockDealer extends Dealer
{

private static final long serialVersionUID = -1942018784059744599L;

private DealerGroup dealerGroup;
private int dealerGroupId;

public MockDealer()
{
    super();
}

public DealerGroup getDealerGroup()
{
    return dealerGroup;
}

public void setDealerGroup( DealerGroup group )
{
    dealerGroup = group;
}

public int getDealerGroupId()
{
    return dealerGroupId;
}

public void setDealerGroupId( int i )
{
    dealerGroupId = i;
}

}
