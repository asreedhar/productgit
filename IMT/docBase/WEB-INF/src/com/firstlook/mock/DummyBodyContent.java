package com.firstlook.mock;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;


public class DummyBodyContent extends BodyContent
{
    String content;
    
public DummyBodyContent( JspWriter jspWriter, String content )
{
    super( jspWriter );
    this.content = content;
}

public void clearBody()
{
}

public Reader getReader()
{
    return null;
}

/**
 *
 */

public String getString()
{
    return content;
}

/**
 *
 */

public void writeOut( Writer arg0 ) throws IOException
{
}

/**
 *
 */

public void print( boolean arg0 ) throws IOException
{
}

/**
 *
 */

public void print( char arg0 ) throws IOException
{
}

/**
 *
 */

public void print( int arg0 ) throws IOException
{
}

/**
 *
 */

public void print( long arg0 ) throws IOException
{
}

/**
 *
 */

public void print( float arg0 ) throws IOException
{
}

/**
 *
 */

public void print( double arg0 ) throws IOException
{
}

/**
 *
 */

public void print( char[] arg0 ) throws IOException
{
}

/**
 *
 */

public void print( String arg0 ) throws IOException
{
}

/**
 *
 */

public void print( Object arg0 ) throws IOException
{
}

/**
 *
 */

public void newLine() throws IOException
{
}

/**
 *
 */

public void println() throws IOException
{
}

/**
 *
 */

public void println( boolean arg0 ) throws IOException
{
}

/**
 *
 */

public void println( char arg0 ) throws IOException
{
}

/**
 *
 */

public void println( int arg0 ) throws IOException
{
}

/**
 *
 */

public void println( long arg0 ) throws IOException
{
}

/**
 *
 */

public void println( float arg0 ) throws IOException
{
}

/**
 *
 */

public void println( double arg0 ) throws IOException
{
}

/**
 *
 */

public void println( char[] arg0 ) throws IOException
{
}

/**
 *
 */

public void println( String arg0 ) throws IOException
{
}

/**
 *
 */

public void println( Object arg0 ) throws IOException
{
}

/**
 *
 */

public void clear() throws IOException
{
}

/**
 *
 */

public void clearBuffer() throws IOException
{
}

/**
 *
 */

public int getRemaining()
{
    return 0;
}

/**
 *
 */

public void close() throws IOException
{
}

/**
 *
 */

public void write( char[] arg0, int arg1, int arg2 ) throws IOException
{
}

}
