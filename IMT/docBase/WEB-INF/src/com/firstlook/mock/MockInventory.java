package com.firstlook.mock;

import com.firstlook.entity.IMakeModelGrouping;
import com.firstlook.entity.InventoryEntity;

public class MockInventory extends InventoryEntity implements IMakeModelGrouping
{

private String groupingDescription;
private int groupingDescriptionId;
private String vehicleTrim;
private String bodyType;
private Integer blackBookClean;

public MockInventory()
{
    super();
    setGroupingDescription("mock grouping description");
    setVehicleTrim("mock trim");
    setBodyType("mock bodystyle");
}

public String getGroupingDescription()
{
    return groupingDescription;
}

public void setGroupingDescription( String newGroupingDescription )
{
    groupingDescription = newGroupingDescription;
}

public String getVehicleTrim()
{
    return vehicleTrim;
}

public String getBodyType()
{
    return bodyType;
}

public void setVehicleTrim( String string )
{
    vehicleTrim = string;
}

public void setBodyType( String string )
{
    bodyType = string;
}

public int getGroupingDescriptionId()
{
    return groupingDescriptionId;
}

public void setGroupingDescriptionId( int i )
{
    groupingDescriptionId = i;
}

public Integer getBlackBookClean()
{
    return blackBookClean;
}

public void setBlackBookClean( Integer i )
{
    blackBookClean = i;
}

}
