package com.firstlook.mock;

import java.util.ArrayList;
import java.util.Iterator;

import junit.framework.TestCase;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

import biz.firstlook.commons.util.IPropertyFinder;
import biz.firstlook.commons.util.MockPropertyFinder;
import biz.firstlook.commons.util.PropertyLoader;

import com.firstlook.data.factory.hibernate.SessionFactory;
import com.firstlook.data.factory.hibernate.mock.MockSessionFactory;
import com.firstlook.data.mock.MockDatabase;
import com.firstlook.data.mock.MockDatabaseFactory;

public abstract class BaseTestCase extends TestCase
{

public MockDatabase mockDatabase;
protected MockPropertyFinder propFinder;

public BaseTestCase( String name )
{
    super(name);
}

public void setup() throws Exception
{
}

protected void teardown() throws Exception
{
}

public final void setUp() throws Exception
{
    propFinder = new MockPropertyFinder();
    propFinder.setStringProperty(
            "firstlook.report.aginginventoryplanningreport.level1",
            "OFF THE CLIFF - EFFICIENTLY LIQUIDATE INVENTORY");
    propFinder.setStringProperty(
            "firstlook.report.aginginventoryplanningreport.level2",
            "AGGRESSIVELY PURSUE WHOLESALE OPTIONS");
    propFinder
            .setStringProperty(
                    "firstlook.report.aginginventoryplanningreport.level3",
                    "FINAL PUSH - AGGRESSIVE RETAIL STRATEGY<br> BEGIN PURSUING WHOLESALE OPTIONS");
    propFinder.setStringProperty(
            "firstlook.report.aginginventoryplanningreport.level4",
            "PURSUE AGGRESSIVE RETAIL STRATEGIES");
    propFinder.setStringProperty(
            "firstlook.report.aginginventoryplanningreport.level5",
            "HIGH RISK TRADE-IN VEHICLES (LAST 15 DAYS)");
    propFinder.setStringProperty(
            "firstlook.report.aginginventoryplanningreport.level6",
            "HIGH RISK VEHICLES (16-29 DAYS OLD)");
    propFinder.setStringProperty("firstlook.makemodeltemplate.location",
            "c:\\projects\\imt");

    ArrayList<IPropertyFinder> list = new ArrayList<IPropertyFinder>();
    list.add(propFinder);
    PropertyLoader.setPropertyFinders(list);
    SessionFactory.setSessionFactory(MockSessionFactory.getInstance());

    mockDatabase = (MockDatabase) MockDatabaseFactory.getInstance()
            .getDatabase();
    mockDatabase.resetAll();

    setup();
}

public void tearDown() throws Exception
{
    teardown();
    PropertyLoader.setPropertyFinders(null);
}

@SuppressWarnings("unchecked")
public static boolean isInActionErrors( ActionErrors errors, String errorKey )
{
    if ( errors == null )
    {
        return false;
    }

    Iterator<ActionError> errorIter = errors.get(ActionErrors.GLOBAL_ERROR);
    while (errorIter.hasNext())
    {
        ActionError actErr = errorIter.next();
        if ( actErr.getKey().equals(errorKey) )
        {
            return true;
        }
    }
    return false;
}

protected MockDatabase getDatabase()
{
    return mockDatabase;
}

}
