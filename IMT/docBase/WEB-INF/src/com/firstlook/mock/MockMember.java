package com.firstlook.mock;

import java.util.Collection;
import java.util.HashMap;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.Member;

public class MockMember extends Member
{

private static final long serialVersionUID = 6975346038638638005L;

public boolean isAssociatedWithDealerFlag;
private Dealer currentDealer;
private HashMap dealers;

public MockMember()
{
    super();
    isAssociatedWithDealerFlag = true;
    dealers = new HashMap();
    setMemberId(new Integer(0));
}

public Dealer getCurrentDealer()
{
    return currentDealer;
}

public Collection getDealers()
{
    return dealers.values();
}

public boolean isAssociatedWithDealer( int dealerId )
{
    return isAssociatedWithDealerFlag;
}

public void setCurrentDealer( Dealer newCurrentDealer )
{
    currentDealer = newCurrentDealer;
}

public void setDealers( HashMap newDealers )
{
    dealers = newDealers;
}

}
