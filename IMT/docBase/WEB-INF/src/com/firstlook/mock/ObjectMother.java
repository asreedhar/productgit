package com.firstlook.mock;

import java.util.Collections;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;

import biz.firstlook.transact.persist.model.BodyType;
import biz.firstlook.transact.persist.model.DealerPreference;
import biz.firstlook.transact.persist.model.GroupingDescription;
import biz.firstlook.transact.persist.model.MakeModelGrouping;
import biz.firstlook.transact.persist.model.UserRoleEnum;
import biz.firstlook.transact.persist.model.Vehicle;

import com.firstlook.dealer.tools.GuideBook;
import com.firstlook.entity.ApplicationEvent;
import com.firstlook.entity.BusinessUnit;
import com.firstlook.entity.BusinessUnitRelationship;
import com.firstlook.entity.BusinessUnitType;
import com.firstlook.entity.ChangedVics;
import com.firstlook.entity.Corporation;
import com.firstlook.entity.DataSource;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerFranchise;
import com.firstlook.entity.DealerGroup;
import com.firstlook.entity.DealerGroupPotentialEvent;
import com.firstlook.entity.DealerGroupPreference;
import com.firstlook.entity.DealerRisk;
import com.firstlook.entity.FirstLookRegion;
import com.firstlook.entity.FirstLookRegionToZip;
import com.firstlook.entity.Franchise;
import com.firstlook.entity.GroupingPromotionPlan;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.MarketToZip;
import com.firstlook.entity.Member;
import com.firstlook.entity.MemberAccess;
import com.firstlook.entity.OptionDetail;
import com.firstlook.entity.ProgramTypeEnum;
import com.firstlook.entity.Region;
import com.firstlook.entity.SiteAuditEvent;
import com.firstlook.entity.TradeAnalyzerEvent;
import com.firstlook.entity.VehicleOption;
import com.firstlook.entity.VehicleOptions;
import com.firstlook.entity.VehicleSaleEntity;
import com.firstlook.presentation.Perspective;
import com.firstlook.report.DisplayDateRange;
import com.firstlook.report.ReportGrouping;

public class ObjectMother
{

public ObjectMother()
{
	super();
}

public static ChangedVics createChangedVics()
{
	ChangedVics vics = new ChangedVics();

	vics.setNewVic( "123456" );
	vics.setOldVic( "654321" );

	return vics;
}

public static DummyResultSet createDashboardAveragesResultSet()
{
	DummyResultSet drs = new DummyResultSet();
	Hashtable<String, Integer> firstRow = new Hashtable<String, Integer>();
	firstRow.put( "AverageUnitsSold", new Integer( 7 ) );
	firstRow.put( "AverageGrossProfit", new Integer( 70 ) );
	firstRow.put( "AverageDaysToSale", new Integer( 9 ) );
	firstRow.put( "AverageUnitsInStock", new Integer( 9 ) );
	firstRow.put( "AverageMileage", new Integer( 7 ) );
	firstRow.put( "No_Sales", new Integer( 1 ) );
	firstRow.put( "TotalInventoryDollars", new Integer( 1000 ) );
	firstRow.put( "TotalRevenue", new Integer( 1500 ) );
	firstRow.put( "TotalGrossMargin", new Integer( 2000 ) );
	firstRow.put( "AverageFrontEnd", new Integer( 2000 ) );
	firstRow.put( "AverageBackEnd", new Integer( 2000 ) );
	firstRow.put( "TotalFrontEnd", new Integer( 2000 ) );
	firstRow.put( "TotalBackEnd", new Integer( 2000 ) );
	drs.addRow( firstRow );
	return drs;
}

public static DummyResultSet createDashboardResultSet()
{
	DummyResultSet drs2 = new DummyResultSet();
	Hashtable<String, Object> tbl2 = new Hashtable<String, Object>();
	tbl2.put( "AverageDays", new Integer( 7 ) );
	tbl2.put( "AverageGrossProfit", new Integer( 70 ) );
	tbl2.put( "AverageMileage", new Integer( 9 ) );
	tbl2.put( "VehicleGroupingDescription", "Honda" );
	tbl2.put( "GroupingId", new Integer( 1 ) );
	tbl2.put( "UnitsInStock", new Integer( 7 ) );
	tbl2.put( "UnitsSold", new Integer( 7 ) );
	tbl2.put( "No_Sales", new Integer( 1 ) );
	tbl2.put( "TotalBackEnd", new Integer( 5 ) );
	tbl2.put( "TotalFrontEnd", new Integer( 6 ) );
	tbl2.put( "AverageInvAge", new Integer( 10 ) );
	drs2.addRow( tbl2 );
	return drs2;
}

public static GroupingPromotionPlan createGroupingPromotionPlan()
{
	GroupingPromotionPlan plan = new GroupingPromotionPlan();
	plan.setBusinessUnitId( new Integer( 100215 ) );
	plan.setGroupingPromotionPlanId( new Integer( 1 ) );

	return plan;
}

public static Dealer createDealer()
{
	Dealer mockDealer = new Dealer();
	mockDealer.setDealerCode( "JimsMomsDealerCode" );
	mockDealer.setName( "Jims Moms Dealer Name" );
	mockDealer.setNickname( " JimsMoms Dealer" );
	mockDealer.setDealerCode( "DEALER01" );
	mockDealer.setName( "Jims Moms Dealer" );
	mockDealer.setAddress1( "Jims Moms Dealers Street1" );
	mockDealer.setAddress2( "Jims Moms Dealers Street2" );
	mockDealer.setCity( "Chicago" );
	mockDealer.setState( "IL" );
	mockDealer.setZipcode( "60661" );
	mockDealer.setOfficePhoneNumber( "5554445555" );
	mockDealer.setOfficeFaxNumber( "5554445556" );
	mockDealer.setDealerType( BusinessUnitType.BUSINESS_UNIT_DEALER_TYPE );
	mockDealer.setCustomHomePageMessage( "Custom Message." );
	mockDealer.setDashboardColumnDisplayPreference( Dealer.DASHBOARD_DISPLAY_AVERAGE_MILEAGE );
	mockDealer.setUnitsSoldThreshold4Wks( 4 );
	mockDealer.setUnitsSoldThreshold8Wks( 8 );
	mockDealer.setUnitsSoldThreshold13Wks( 12 );
	mockDealer.setUnitsSoldThreshold26Wks( 26 );
	mockDealer.setUnitsSoldThreshold52Wks( 52 );
	mockDealer.setActive( true );
	mockDealer.setGuideBookId( GuideBook.GUIDE_TYPE_NADA );
	mockDealer.setDefaultForecastingWeeks( 8 );
	mockDealer.setDefaultTrendingView( 1 );
	mockDealer.setDefaultTrendingWeeks( 8 );
	mockDealer.setNadaRegionCode( 56 );
	mockDealer.setInceptionDate( new Date( 0 ) );
	mockDealer.setPackAmount( 120 );
	mockDealer.setStockOrVinPreference( true );
	mockDealer.setUnitCostOrListPricePreference( true );
	mockDealer.setBookOutPreference( true );
	mockDealer.setProgramTypeCD( ProgramTypeEnum.INSIGHT_VAL );
	mockDealer.getDealerPreference().setUnitCostThreshold( new Integer( 1000 ) );
	mockDealer.getDealerPreference().setUnwindDaysThreshold( new Integer( 50 ) );
	mockDealer.getDealerPreference().setUnitCostUpdateOnSale( true );
	mockDealer.getDealerPreference().setSellThroughRate( new Integer( 90 ) );
	mockDealer.getDealerPreference().setIncludeBackEndInValuation( Boolean.TRUE );
    mockDealer.getDealerPreference().setVehicleSaleThresholdForCoreMarketPenetration( new Double(10d) );

	return mockDealer;
}

public static Dealer createDealerWithoutPreferences()
{
	Dealer mockDealer = new Dealer();
	mockDealer.setNickname( "NickName" );
	mockDealer.setDealerCode( "DEALER01" );
	mockDealer.setName( "Test Dealer" );
	mockDealer.setAddress1( "Street1" );
	mockDealer.setAddress2( "Street2" );
	mockDealer.setCity( "Chicago" );
	mockDealer.setState( "IL" );
	mockDealer.setZipcode( "60661" );
	mockDealer.setOfficePhoneNumber( "5554445555" );
	mockDealer.setOfficeFaxNumber( "5554445556" );
	mockDealer.setDealerType( BusinessUnitType.BUSINESS_UNIT_DEALER_TYPE );
	mockDealer.setActive( true );
	mockDealer.setProgramTypeCD( ProgramTypeEnum.INSIGHT_VAL );
	mockDealer.getDealerPreference().setUnitCostThreshold( new Integer( 1000 ) );
	mockDealer.getDealerPreference().setUnwindDaysThreshold( new Integer( 50 ) );
	mockDealer.getDealerPreference().setUnitCostUpdateOnSale( true );
	mockDealer.getDealerPreference().setIncludeBackEndInValuation( new Boolean( true ) );
	mockDealer.getDealerPreference().setSellThroughRate( new Integer( 2 ) );
    mockDealer.getDealerPreference().setVehicleSaleThresholdForCoreMarketPenetration( new Double( 10d ) );

	return mockDealer;
}

public static DealerGroup createDealerGroup()
{
	DealerGroup testDealerGroup = new DealerGroup();

	testDealerGroup.setDealerGroupCode( "ABC001" );
	testDealerGroup.setName( "Smith's Auto Barn" );
	testDealerGroup.setNickname( "S Auto Barn" );
	testDealerGroup.setAddress1( "123 Main St" );
	testDealerGroup.setAddress2( "Suite 567" );
	testDealerGroup.setCity( "Chicago" );
	testDealerGroup.setState( "IL" );
	testDealerGroup.setZipcode( "60666" );
	testDealerGroup.setOfficePhoneNumber( "555-678-0954" );
	testDealerGroup.setOfficeFaxNumber( "555-890-7089" );
	testDealerGroup.setAgingPolicy( new Integer( 45 ) );
	testDealerGroup.setAgingPolicyAdjustment( 21 );
	testDealerGroup.setInventoryExchangeAgeLimit( 45 );

	return testDealerGroup;
}

public static DealerGroupPreference createDealerGroupPreference()
{
	DealerGroupPreference testDealerGroupPreference = new DealerGroupPreference();

	testDealerGroupPreference.setAgingPolicy( new Integer( 45 ) );
	testDealerGroupPreference.setAgingPolicyAdjustment( 21 );
	testDealerGroupPreference.setInventoryExchangeAgeLimit( 45 );
	testDealerGroupPreference.setPricePointDealsThreshold( 8 );

	return testDealerGroupPreference;
}

public static DealerGroup createDealerGroupBusinessUnit()
{
	DealerGroup testDealerGroup = new DealerGroup();

	testDealerGroup.setDealerGroupCode( "ABC001" );
	testDealerGroup.setName( "Jeff's Auto Haus" );
	testDealerGroup.setNickname( "S Auto Barn" );
	testDealerGroup.setAddress1( "123 Main St" );
	testDealerGroup.setAddress2( "Suite 567" );
	testDealerGroup.setCity( "Chicago" );
	testDealerGroup.setState( "IL" );
	testDealerGroup.setZipcode( "60666" );
	testDealerGroup.setOfficePhoneNumber( "555-678-0954" );
	testDealerGroup.setActive( true );

	return testDealerGroup;
}

public static ApplicationEvent createApplicationEvent()
{
	ApplicationEvent event = new ApplicationEvent();

	event.setApplicationEventId( 1 );
	event.setEventType( 1 );
	event.setEventDescription( "test event" );
	event.setMemberId( 1 );
	event.setCreateTimestamp( new Date( 0 ) );

	return event;
}

public static GroupingDescription createGroupingDescription()
{
	GroupingDescription group = new GroupingDescription();
	group.setGroupingDescription( "Mock Description" );

	return group;
}

public static MakeModelGrouping createMakeModelGrouping()
{
	MakeModelGrouping grouping = new MakeModelGrouping();
	grouping.setMake( "MockMake" );
	grouping.setModel( "MockModel" );

	return grouping;
}

public static Member createMember()
{
	Member mockMember = new Member();
	mockMember.setLogin( "mockLogin" );
	mockMember.setPassword( "memberPassword" );
	mockMember.setSalutation( "Mr." );
	mockMember.setLoginStatus( 1 );
	mockMember.setFirstName( "Robert" );
	mockMember.setPreferredFirstName( "Bob" );
	mockMember.setMiddleInitial( "J" );
	mockMember.setLastName( "Smith" );
	mockMember.setOfficePhoneNumber( "5551234567" );
	mockMember.setOfficePhoneExtension( "1234" );
	mockMember.setOfficeFaxNumber( "5557654321" );
	mockMember.setMobilePhoneNumber( "5551237654" );
	mockMember.setEmailAddress( "test@test.com" );
	mockMember.setPagerNumber( "5555555555" );
	mockMember.setReportMethod( "Fax" );
	mockMember.setNotes( "This is a test." );
	mockMember.setDashboardRowDisplay( new Integer( 10 ) );
	// mockMember.setReportPreference( 8 );
	mockMember.setMemberType( Member.MEMBER_TYPE_USER );
	mockMember.setUserRoleEnum(UserRoleEnum.NEW );

	return mockMember;
}

public static MockInventory createMockInventory()
{
	MockInventory mockInventory = new MockInventory();

	mockInventory.setGroupingDescription( "mock grouping desc" );
	mockInventory.setInventoryReceivedDt( new Date( 0 ) );
	mockInventory.setMake( "mock make" );
	mockInventory.setModel( "mock model" );
	mockInventory.setStockNumber( "mock stock number" );
	mockInventory.setTrim( "mock trim" );
	mockInventory.setBodyType( "mock body" );

	return mockInventory;
}

public static ReportGrouping createReportGrouping()
{
	ReportGrouping rg1 = new ReportGrouping();
	rg1.setGroupingName( "ACURA INTEGRA" );
	rg1.setUnitsSold( 23 );
	rg1.setAvgGrossProfit( new Integer( 10000 ) );
	rg1.setAverageFrontEnd( new Integer( 4873 ) );
	rg1.setAvgDaysToSale( new Integer( 17 ) );

	return rg1;
}

public static Vector<ReportGrouping> createReportGroupings()
{
	ReportGrouping rg1 = new ReportGrouping();
	ReportGrouping rg2 = new ReportGrouping();
	ReportGrouping rg3 = new ReportGrouping();
	ReportGrouping rg4 = new ReportGrouping();
	ReportGrouping rg5 = new ReportGrouping();
	ReportGrouping rg6 = new ReportGrouping();
	ReportGrouping rg7 = new ReportGrouping();
	ReportGrouping rg8 = new ReportGrouping();

	rg1.setGroupingName( "ACURA INTEGRA" );
	rg1.setUnitsSold( 1 );
	rg1.setAvgGrossProfit( new Integer( 0 ) );
	rg1.setAverageFrontEnd( new Integer( 0 ) );
	rg1.setAvgDaysToSale( new Integer( 1 ) );

	rg2.setGroupingName( "AUDI S6" );
	rg2.setUnitsSold( 1 );
	rg2.setAvgGrossProfit( new Integer( 2000 ) );
	rg2.setAverageFrontEnd( new Integer( 2000 ) );
	rg2.setAvgDaysToSale( new Integer( 1 ) );

	rg3.setGroupingName( "BMW K75" );
	rg3.setUnitsSold( 1 );
	rg3.setAvgGrossProfit( new Integer( 1500 ) );
	rg3.setAverageFrontEnd( new Integer( 1500 ) );
	rg3.setAvgDaysToSale( new Integer( 1 ) );

	rg4.setGroupingName( "FORD ESCAPE" );
	rg4.setUnitsSold( 2 );
	rg4.setAvgGrossProfit( new Integer( 2750 ) );
	rg4.setAverageFrontEnd( new Integer( 2750 ) );
	rg4.setAvgDaysToSale( new Integer( 1 ) );

	rg5.setGroupingName( "FORD ESCORT" );
	rg5.setUnitsSold( 1 );
	rg5.setAvgGrossProfit( null );
	rg5.setAverageFrontEnd( null );
	rg5.setAvgDaysToSale( null );

	rg6.setGroupingName( "FORD EXPLORER" );
	rg6.setUnitsSold( 8 );
	rg6.setAvgGrossProfit( new Integer( 4343 ) );
	rg6.setAverageFrontEnd( new Integer( 4343 ) );
	rg6.setAvgDaysToSale( new Integer( 3 ) );

	rg7.setGroupingName( "FORD TAURUS" );
	rg7.setUnitsSold( 4 );
	rg7.setAvgGrossProfit( new Integer( 8250 ) );
	rg7.setAverageFrontEnd( new Integer( 8250 ) );
	rg7.setAvgDaysToSale( new Integer( 3 ) );

	rg8.setGroupingName( "HONDA ACCORD" );
	rg8.setUnitsSold( 3 );
	rg8.setAvgGrossProfit( new Integer( 2800 ) );
	rg8.setAverageFrontEnd( new Integer( 2800 ) );
	rg8.setAvgDaysToSale( new Integer( 25 ) );
	rg8.setTotalBackEnd( new Integer( 1300 ) );
	rg8.setTotalFrontEnd( new Integer( 71820 ) );
	rg8.setTotalInventoryDollars( new Integer( 358132 ) );
	rg8.setTotalRevenue( new Integer( 600781 ) );

	Vector<ReportGrouping> groupings = new Vector<ReportGrouping>();
	groupings.add( rg1 );
	groupings.add( rg2 );
	groupings.add( rg3 );
	groupings.add( rg4 );
	groupings.add( rg5 );
	groupings.add( rg6 );
	groupings.add( rg7 );
	groupings.add( rg8 );

	return groupings;
}

/**
 * 
 * @return java.util.Vector
 */
public static Vector<ReportGrouping> createReportGroupingsNotAlphabetical()
{
	ReportGrouping rg1 = new ReportGrouping();
	ReportGrouping rg2 = new ReportGrouping();
	ReportGrouping rg3 = new ReportGrouping();
	ReportGrouping rg4 = new ReportGrouping();
	ReportGrouping rg5 = new ReportGrouping();
	ReportGrouping rg6 = new ReportGrouping();
	ReportGrouping rg7 = new ReportGrouping();
	ReportGrouping rg8 = new ReportGrouping();

	rg1.setGroupingName( "FORD EXPLORER" );
	rg1.setUnitsSold( 8 );
	rg1.setAvgGrossProfit( new Integer( 4343 ) );
	rg1.setAvgDaysToSale( new Integer( 3 ) );

	rg2.setGroupingName( "AUDI S6" );
	rg2.setUnitsSold( 1 );
	rg2.setAvgGrossProfit( new Integer( 2000 ) );
	rg2.setAvgDaysToSale( new Integer( 1 ) );

	rg3.setGroupingName( "BMW K75" );
	rg3.setUnitsSold( 1 );
	rg3.setAvgGrossProfit( new Integer( 1500 ) );
	rg3.setAvgDaysToSale( new Integer( 1 ) );

	rg4.setGroupingName( "FORD ESCAPE" );
	rg4.setUnitsSold( 2 );
	rg4.setAvgGrossProfit( new Integer( 2750 ) );
	rg4.setAvgDaysToSale( new Integer( 1 ) );

	rg5.setGroupingName( "FORD ESCORT" );
	rg5.setUnitsSold( 1 );
	rg5.setAvgGrossProfit( null );
	rg5.setAvgDaysToSale( null );

	rg6.setGroupingName( "ACURA INTEGRA" );
	rg6.setUnitsSold( 8 );
	rg6.setAvgGrossProfit( new Integer( 0 ) );
	rg6.setAvgDaysToSale( new Integer( 1 ) );

	rg7.setGroupingName( "FORD TAURUS" );
	rg7.setUnitsSold( 4 );
	rg7.setAvgGrossProfit( new Integer( 8250 ) );
	rg7.setAvgDaysToSale( new Integer( 3 ) );

	rg8.setGroupingName( "HONDA ACCORD" );
	rg8.setUnitsSold( 3 );
	rg8.setAvgGrossProfit( new Integer( 2800 ) );
	rg8.setAvgDaysToSale( new Integer( 25 ) );

	Vector<ReportGrouping> groupings = new Vector<ReportGrouping>();
	groupings.add( rg1 );
	groupings.add( rg2 );
	groupings.add( rg3 );
	groupings.add( rg4 );
	groupings.add( rg5 );
	groupings.add( rg6 );
	groupings.add( rg7 );
	groupings.add( rg8 );

	return groupings;
}

public static String createTestBlackBookXMLFile()
{
	return "<?xml version=\"1.0\"  ?>  <cars>    <car>      <year>2000</year> <uvc>1234</uvc>      <make>KIA</make>      <model>SEPHIA</model>      <series />      <body_style>4D LS PWR PKG</body_style>      <vin>KNAFB121Y</vin>      <mileage />      <msrp>12570</msrp>      <loan_value>6850</loan_value>      <residual_value>        <resid12>5350</resid12>        <resid24>4275</resid24>        <resid30>3875</resid30>        <resid36>3500</resid36>        <resid42>3100</resid42>        <resid48>2725</resid48>      </residual_value>      <tradein_value>        <extra_clean />        <clean>7925</clean>        <average>7040</average>        <rough>5105</rough>      </tradein_value>      <wholesale_value>        <extra_clean>8275</extra_clean>        <clean>7775</clean>        <average>6725</average>        <rough>5375</rough>      </wholesale_value>      <retail_value>        <extra_clean>10725</extra_clean>        <clean>10000</clean>        <average>8575</average>        <rough>6950</rough>      </retail_value>      <mileage_adjustments>        <mileage_range>          <begin_range>1</begin_range>          <end_range>18000</end_range>          <loan_value>300</loan_value>          <extra_clean>75</extra_clean>          <clean>175</clean>          <average>300</average>          <rough>0</rough>        </mileage_range>        <mileage_range>          <begin_range>18001</begin_range>          <end_range>24000</end_range>          <loan_value>125</loan_value>          <extra_clean>-100</extra_clean>          <clean>0</clean>          <average>125</average>          <rough>0</rough>        </mileage_range>        <mileage_range>          <begin_range>24001</begin_range>          <end_range>30000</end_range>          <loan_value>0</loan_value>          <extra_clean>-400</extra_clean>          <clean>-125</clean>          <average>0</average>          <rough>0</rough>        </mileage_range>        <mileage_range>          <begin_range>30001</begin_range>          <end_range>40000</end_range>          <loan_value>-125</loan_value>          <extra_clean>-750</extra_clean>          <clean>-400</clean>          <average>-150</average>          <rough>-100</rough>        </mileage_range>        <mileage_range>          <begin_range>40001</begin_range>          <end_range>50000</end_range>          <loan_value>-325</loan_value>          <extra_clean />          <clean>-700</clean>          <average>-375</average>          <rough>-175</rough>        </mileage_range>        <mileage_range>          <begin_range>50001</begin_range>          <end_range>60000</end_range>          <loan_value>-575</loan_value>          <extra_clean />          <clean />          <average>-650</average>          <rough>-325</rough>        </mileage_range>        <mileage_range>          <begin_range>60001</begin_range>          <end_range>70000</end_range>          <loan_value>-800</loan_value>          <extra_clean />          <clean />          <average>-900</average>          <rough>-600</rough>        </mileage_range>        <mileage_range>          <begin_range>70001</begin_range>          <end_range>80000</end_range>          <loan_value>-1275</loan_value>          <extra_clean />          <clean />          <average>-1400</average>          <rough>-800</rough>        </mileage_range>        <mileage_range>          <begin_range>80001</begin_range>          <end_range>999999</end_range>          <loan_value>-1850</loan_value>          <extra_clean />          <clean />          <average>-2000</average>          <rough>-1500</rough>        </mileage_range>      </mileage_adjustments>      <price_includes>AC AT</price_includes>      <options>        <option>          <description>CD</description>          <add_deduct>Add</add_deduct>          <amount>175</amount>          <resid12>150</resid12>          <resid24>125</resid24>          <resid30>0</resid30>          <resid36>100</resid36>          <resid42>0</resid42>          <resid48>0</resid48>        </option>        <option>          <description>WITHOUT F/AIR</description>          <add_deduct>Deduct</add_deduct>          <amount>825</amount>          <resid12>750</resid12>          <resid24>675</resid24>          <resid30>0</resid30>          <resid36>600</resid36>          <resid42>0</resid42>          <resid48>525</resid48>        </option>        <option>          <description>W/O AUTO TRANS</description>          <add_deduct>Deduct</add_deduct>          <amount>450</amount>          <resid12>400</resid12>          <resid24>350</resid24>          <resid30>0</resid30>          <resid36>300</resid36>          <resid42>0</resid42>          <resid48>250</resid48>        </option>      </options>    </car> </cars>";

}

public static TradeAnalyzerEvent createTradeAnalyzerEvent()
{
	TradeAnalyzerEvent mockTradeAnalyzerEvent = new TradeAnalyzerEvent();
	mockTradeAnalyzerEvent.setTradeAnalyzerEventId( new Integer( 56 ) );
	mockTradeAnalyzerEvent.setBusinessUnitId( 2 );
	mockTradeAnalyzerEvent.setMemberId( 3 );
	mockTradeAnalyzerEvent.setVin( "12345678901234567" );
	mockTradeAnalyzerEvent.setMileage( 9600 );
	mockTradeAnalyzerEvent.setMake( "HONDA" );
	mockTradeAnalyzerEvent.setModel( "ACCORD" );
	mockTradeAnalyzerEvent.setTrim( "EX" );
	mockTradeAnalyzerEvent.setGuideBookId( GuideBook.GUIDE_TYPE_GALVES );
	mockTradeAnalyzerEvent.setGuideBookYear( 2001 );
	mockTradeAnalyzerEvent.setGuideBookMake( "HONDA" );
	mockTradeAnalyzerEvent.setGuideBookSeries( "ACCORD EX" );
	mockTradeAnalyzerEvent.setGuideBookOptions( "Captain's Chairs +2000" );
	mockTradeAnalyzerEvent.setGeneralAverageMileage( 4500 );
	mockTradeAnalyzerEvent.setGeneralAverageDaysToSale( 5 );
	mockTradeAnalyzerEvent.setGeneralAverageGrossProfit( 600 );
	mockTradeAnalyzerEvent.setGeneralUnitsInStock( 4 );
	mockTradeAnalyzerEvent.setGeneralUnitsSold( 35 );
	mockTradeAnalyzerEvent.setGeneralNoSales( 2 );
	mockTradeAnalyzerEvent.setSpecificAverageMileage( 3500 );
	mockTradeAnalyzerEvent.setSpecificAverageDaysToSale( 4 );
	mockTradeAnalyzerEvent.setSpecificAverageGrossProfit( 500 );
	mockTradeAnalyzerEvent.setSpecificUnitsInStock( 3 );
	mockTradeAnalyzerEvent.setSpecificUnitsSold( 25 );
	mockTradeAnalyzerEvent.setSpecificNoSales( 1 );
	mockTradeAnalyzerEvent.setViewDealsDealerGroupOverall( true );
	mockTradeAnalyzerEvent.setViewDealsDealerGroupTrim( true );
	mockTradeAnalyzerEvent.setViewDealsDealerShipOverall( true );
	mockTradeAnalyzerEvent.setViewDealsDealerShipTrim( true );
	mockTradeAnalyzerEvent.setShowDealerGroupResults( true );
	mockTradeAnalyzerEvent.setHighGrossProfit( true );
	mockTradeAnalyzerEvent.setAvgGrossProfit( true );
	mockTradeAnalyzerEvent.setLowGrossProfit( true );
	mockTradeAnalyzerEvent.setFasterDaysToSell( true );
	mockTradeAnalyzerEvent.setAvgDaysToSell( true );
	mockTradeAnalyzerEvent.setSlowerDaysToSell( true );
	mockTradeAnalyzerEvent.setFrequentNoSales( true );
	mockTradeAnalyzerEvent.setHighMargin( true );
	mockTradeAnalyzerEvent.setAvgMargin( true );
	mockTradeAnalyzerEvent.setLowMargin( true );
	mockTradeAnalyzerEvent.setNoHistoricalPerformance( true );
	mockTradeAnalyzerEvent.setLimitedHistoricalPeformance( true );
	mockTradeAnalyzerEvent.setCreateTimestamp( new Date( 0 ) );

	return mockTradeAnalyzerEvent;
}

public static SiteAuditEvent createSiteAuditEvent()
{
	SiteAuditEvent mockSiteAuditEvent = new SiteAuditEvent();
	mockSiteAuditEvent.setSiteAuditEventId( new Integer( 101 ) );
	mockSiteAuditEvent.setBusinessUnitId( 2 );
	mockSiteAuditEvent.setMemberId( 3 );
	mockSiteAuditEvent.setEventName( "Vehicle Locator" );
	mockSiteAuditEvent.setEventData1( "Data1" );
	mockSiteAuditEvent.setEventData2( "Data2" );
	mockSiteAuditEvent.setEventData3( "Data3" );
	mockSiteAuditEvent.setEventData4( "Data4" );
	mockSiteAuditEvent.setEventData5( "Data5" );
	mockSiteAuditEvent.setEventData6( "Data6" );
	mockSiteAuditEvent.setTimeStamp( new Date( 0 ) );

	return mockSiteAuditEvent;
}

public static DealerGroupPotentialEvent createDealerGroupPotentialEvent()
{
	DealerGroupPotentialEvent dealerGroupPotentialEvent = new DealerGroupPotentialEvent();
	dealerGroupPotentialEvent.setBusinessUnitId( 2 );
	dealerGroupPotentialEvent.setCreateTimestamp( new Date( 0 ) );
	dealerGroupPotentialEvent.setTradeAnalyzerEventId( 56 );
	dealerGroupPotentialEvent.setUnitsInStock( 5 );

	return dealerGroupPotentialEvent;
}

public static BusinessUnitRelationship createBusinessUnitRelationship()
{
	BusinessUnitRelationship businessUnitRelationship = new BusinessUnitRelationship();

	businessUnitRelationship.setBusinessUnitId( new Integer( 1 ) );
	businessUnitRelationship.setParentId( new Integer( 2 ) );

	return businessUnitRelationship;
}

public static InventoryEntity createInventory()
{
	InventoryEntity inventory = new InventoryEntity();

	inventory.setVin( "testVin" );
	inventory.setStockNumber( "testStockNumber" );
	inventory.setVehicleYear( 2002 );
	inventory.setMake( "testMake" );
	inventory.setModel( "testModel" );
	inventory.setVehicleEngine( "2.4L I4 DOHC" );
	inventory.setVehicleDriveTrain( "4X4" );
	inventory.setVehicleTransmission( "Manual 5SPD" );
	inventory.setBaseColor( "Black" );
	inventory.setInteriorColor( "WHITE" );
	inventory.setInterior( "Leather" );
	inventory.setInventoryActive( true );
	inventory.setUnitCost( 1235 );
	inventory.setInventoryReceivedDt( new java.util.Date( 0 ) );
	inventory.setModifiedDT( new Date( 9000 ) );
	inventory.setDmsReferenceDt( new Date( 0 ) );
	inventory.setDmiVehicleClassification( 1 );
	inventory.setAcquisitionPrice( new Double( 1985.43 ) );
	inventory.setMileageReceived( new Integer( 12345 ) );
	inventory.setCylinderCount( 6 );
	inventory.setDoorCount( 4 );
	inventory.setDmiVehicleType( Vehicle.VEHICLE_TYPE_C.intValue() );
	inventory.setFuelType( "G" );
	inventory.setPack( new Double( 1589.65 ) );
	inventory.setInitialVehicleLight( new Integer( InventoryEntity.RED_LIGHT ) );
	inventory.setCurrentVehicleLight( new Integer( InventoryEntity.GREEN_LIGHT ) );
	inventory.setListPrice( new Double( 2235.98 ) );
	inventory.setCreateDt( new Date() );
	inventory.setTradeOrPurchase( InventoryEntity.TRADEORPURCHASE_TRADE );
	inventory.setInventoryType( InventoryEntity.NEW_CAR );
	inventory.setReconditionCost( new Double( 1500.55 ) );
	inventory.setUsedSellingPrice( new Double( 15000.12 ) );
	inventory.setCertified( true );
	inventory.setDeleteDt( new Date( 5000 ) );
	inventory.setRecommendationsFollowed( 0 );

	return inventory;
}

public static Vehicle createVehicle()
{
	Vehicle vehicle = new Vehicle();
	vehicle.setBaseColor( "Mock Color" );
	vehicle.setCreateDate( new Date() );
	vehicle.setCylinderCount( new Integer( 3 ) );
	vehicle.setDmiVehicleClassificationCD( new Integer( 1 ) );
	vehicle.setDmiVehicleTypeCD( new Integer( 1 ) );
	vehicle.setDoorCount( new Integer( 4 ) );
	vehicle.setFuelType( "3" );
	vehicle.setInteriorColor( "Color" );
	vehicle.setInteriorDescription( "Interior Descripton" );
	vehicle.setMake( "Make" );
	vehicle.setModel( "Model" );
	vehicle.setVehicleDriveTrain( "drivetrain" );
	vehicle.setVehicleEngine( "engine" );
	vehicle.setVehicleTransmission( "trans" );
	vehicle.setVehicleYear( new Integer( 2003 ) );
	vehicle.setVin( "123456" );
	vehicle.setVehicleTrim( "Mock Trim" );
	vehicle.setBodyType(createBodyType());
	return vehicle;
}

private static InventoryEntity createVehicleForAgingReport( long now, int day, int dealerId )
{

	InventoryEntity vehicle = createInventory();
	Date date = new Date( now - day * DisplayDateRange.DAY_IN_MILLIS );
	vehicle.setDealerId( dealerId );
	vehicle.setInventoryReceivedDt( date );

	return vehicle;
}

public static VehicleOptions createVehicleOptions()
{
	VehicleOptions options = new VehicleOptions( -1, Collections.<VehicleOption>emptyList() );
	options.setAirConditioning( true );

	return options;

}

public static VehicleOptions createVehicleOptions( boolean setValue )
{
	VehicleOptions options = new VehicleOptions();

	options.setAirConditioning( setValue );
	options.setAudioAMFM( setValue );
	options.setAudioCassette( setValue );
	options.setAudioCD( setValue );
	options.setAudioCDChanger( setValue );
	options.setBedLiner( setValue );
	options.setBedLinerSprayOn( setValue );
	options.setBrakesABS( setValue );
	options.setCaptainChairs2( setValue );
	options.setCaptainChairs4( setValue );
	options.setConvertible( setValue );
	options.setCruise( setValue );
	options.setFiberglassCap( setValue );
	options.setGPS( setValue );
	options.setLeather( setValue );
	options.setLuggageRack( setValue );
	options.setManualSunRoof( setValue );
	options.setPowerLocks( setValue );
	options.setPowerSeats( setValue );
	options.setPowerSunRoof( setValue );
	options.setPowerWindows( setValue );
	options.setRearAC( setValue );
	options.setRunningBoards( setValue );
	options.setTheftDetection( setValue );
	options.setThirdRowSeating( setValue );
	options.setTiltSteering( setValue );
	options.setTireOffRoad( setValue );
	options.setTrailerTowing( setValue );
	options.setWheelAlloy( setValue );
	options.setWheelChrome( setValue );

	return options;
}

public static Vector<InventoryEntity> createVehiclesForAgingReport( int dealerId )
{
	Vector<InventoryEntity> vehicles = new Vector<InventoryEntity>();

	long now = System.currentTimeMillis();
	vehicles.add( createVehicleForAgingReport( now, 0, dealerId ) );

	vehicles.add( createVehicleForAgingReport( now, 16, dealerId ) );
	vehicles.add( createVehicleForAgingReport( now, 17, dealerId ) );

	vehicles.add( createVehicleForAgingReport( now, 31, dealerId ) );
	vehicles.add( createVehicleForAgingReport( now, 32, dealerId ) );
	vehicles.add( createVehicleForAgingReport( now, 33, dealerId ) );

	vehicles.add( createVehicleForAgingReport( now, 46, dealerId ) );
	vehicles.add( createVehicleForAgingReport( now, 47, dealerId ) );
	vehicles.add( createVehicleForAgingReport( now, 48, dealerId ) );
	vehicles.add( createVehicleForAgingReport( now, 49, dealerId ) );

	vehicles.add( createVehicleForAgingReport( now, 61, dealerId ) );
	vehicles.add( createVehicleForAgingReport( now, 62, dealerId ) );
	vehicles.add( createVehicleForAgingReport( now, 63, dealerId ) );
	vehicles.add( createVehicleForAgingReport( now, 64, dealerId ) );
	vehicles.add( createVehicleForAgingReport( now, 65, dealerId ) );

	vehicles.add( createVehicleForAgingReport( now, 91, dealerId ) );
	vehicles.add( createVehicleForAgingReport( now, 92, dealerId ) );
	vehicles.add( createVehicleForAgingReport( now, 93, dealerId ) );
	vehicles.add( createVehicleForAgingReport( now, 94, dealerId ) );
	vehicles.add( createVehicleForAgingReport( now, 95, dealerId ) );
	vehicles.add( createVehicleForAgingReport( now, 96, dealerId ) );

	return vehicles;
}

public static FirstLookRegion createFirstLookRegion()
{
	FirstLookRegion firstLookRegion = new FirstLookRegion();
	firstLookRegion.setFirstLookRegionName( "FirstLookRegion" );

	return firstLookRegion;
}

public static FirstLookRegionToZip createFirstLookRegionToZip()
{
	FirstLookRegionToZip firstLookRegionToZip = new FirstLookRegionToZip();
	firstLookRegionToZip.setFirstLookRegionId( 1 );
	firstLookRegionToZip.setZipCode( "60148" );

	return firstLookRegionToZip;
}

public static MarketToZip createMarketToZip()
{
	MarketToZip marketToZip = new MarketToZip();
	marketToZip.setBusinessUnitId( 1 );
	marketToZip.setRing( 1 );
	marketToZip.setZipCode( "60201" );
	return marketToZip;
}

public static Franchise createFranchise()
{
	Franchise franchise = new Franchise();
	franchise.setFranchiseId( new Integer( -1 ) );
	franchise.setFranchiseDescription( "Test Description" );
	return franchise;

}

public static DealerFranchise createDealerFranchise()
{

	DealerFranchise dealerFranchise = new DealerFranchise();

	dealerFranchise.setDealerFranchiseId( -1 );
	dealerFranchise.setDealerId( -1 );
	dealerFranchise.setFranchiseId( -1 );
	return dealerFranchise;

}

public static BodyType createBodyType()
{
	BodyType bodyType = new BodyType();
	bodyType.setBodyTypeId( new Integer( 111 ) );
	bodyType.setBodyType( "Desc friday" );

	return bodyType;
}

public static DealerPreference createDealerPreference()
{
	DealerPreference dealerPreference = new DealerPreference();

    dealerPreference.setAgingInventoryTrackingDisplayPref( Boolean.TRUE );
	dealerPreference.setCustomHomePageMessage( "Test CustomeHomePageMessage" );
	dealerPreference.setDashboardColumnDisplayPreference( -1 );
    dealerPreference.setDefaultForecastingWeeks( new Integer(-1) );
    dealerPreference.setDefaultTrendingView( new Integer(-1) );
    dealerPreference.setDefaultTrendingWeeks( new Integer(-1) );
    dealerPreference.setGuideBookId( new Integer(1) );
	dealerPreference.setInceptionDate( new Date() );
	dealerPreference.setMaxSalesHistoryDate( new Date() );
	dealerPreference.setNadaRegionCode( 1 );
	dealerPreference.setPackAmount( 100 );
    dealerPreference.setStockOrVinPreference( Boolean.TRUE );
	dealerPreference.setUnitsSoldThreshold13Wks( 1 );
	dealerPreference.setUnitsSoldThreshold26Wks( 2 );
	dealerPreference.setUnitsSoldThreshold4Wks( 1 );
	dealerPreference.setUnitsSoldThreshold52Wks( 1 );
	dealerPreference.setUnitsSoldThreshold8Wks( 8 );
    dealerPreference.setAgeBandTarget1( new Integer(1) );
    dealerPreference.setAgeBandTarget2( new Integer(2) );
    dealerPreference.setAgeBandTarget3( new Integer(3) );
    dealerPreference.setAgeBandTarget4( new Integer(4) );
    dealerPreference.setAgeBandTarget5( new Integer(5) );
    dealerPreference.setAgeBandTarget6(new Integer( 6) );
    dealerPreference.setDaysSupply12WeekWeight( new Integer(70) );
    dealerPreference.setDaysSupply26WeekWeight(new Integer( 30) );
	dealerPreference.setBookOut( true );
	dealerPreference.setBookOutPreferenceId( 5 );
	dealerPreference.setRunDayOfWeek( 5 );
    dealerPreference.setUnitCostThresholdLower(new Integer( 1) );
    dealerPreference.setUnitCostThresholdUpper( new Integer(10) );
    dealerPreference.setFeGrossProfitThreshold( new Integer(10) );
    dealerPreference.setMarginPercentile( new Integer(10) );
    dealerPreference.setDaysToSalePercentile( new Integer(10) );
    dealerPreference.setProgramTypeCD( new Integer(ProgramTypeEnum.INSIGHT_VAL) );
	dealerPreference.setUnitCostThreshold( new Integer( 1000 ) );
	dealerPreference.setUnitCostUpdateOnSale( true );
	dealerPreference.setUnwindDaysThreshold( new Integer( 50 ) );
	dealerPreference.setIncludeBackEndInValuation( Boolean.TRUE );
	dealerPreference.setSellThroughRate( new Integer( 90 ) );
    dealerPreference.setVehicleSaleThresholdForCoreMarketPenetration( new Double(5.0d) );

	return dealerPreference;
}

public static MemberAccess createMemberAccess()
{
	MemberAccess memberAccess = new MemberAccess();
	memberAccess.setBusinessUnitId( new Integer( -1 ) );
	memberAccess.setMemberId( new Integer( -1 ) );

	return memberAccess;
}

public static Corporation createCorporation()
{
	Corporation corp = new Corporation();
	corp.setNickname( "NickName" );
	corp.setCorporationCode( "CORP01" );
	corp.setName( "Test Corporation" );
	corp.setAddress1( "Street1" );
	corp.setAddress2( "Street2" );
	corp.setCity( "Chicago" );
	corp.setState( "IL" );
	corp.setZipcode( "60661" );
	corp.setOfficePhone( "5554445555" );
	corp.setOfficeFax( "5554445556" );
	corp.setActive( true );

	return corp;

}

public static Region createRegion()
{
	Region region = new Region();
	region.setNickname( "NickName" );
	region.setRegionCode( "Region01" );
	region.setName( "Test Region" );
	region.setAddress1( "Street1" );
	region.setAddress2( "Street2" );
	region.setCity( "Chicago" );
	region.setState( "IL" );
	region.setZipcode( "60661" );
	region.setOfficePhone( "5554445555" );
	region.setOfficeFax( "5554445556" );
	region.setActive( true );

	return region;

}

public static DataSource createDataSource()
{
	DataSource dataSource = new DataSource();
	dataSource.setDataSource( "Mock Data Source" );
	return dataSource;
}

public static com.firstlook.entity.GuideBook createGuideBook()
{
	com.firstlook.entity.GuideBook newGuideBook = new com.firstlook.entity.GuideBook();
	newGuideBook.setGuideBookName( "Mock Guide Book" );
	newGuideBook.setGuideBookCategory( "Mock Category" );
	return newGuideBook;
}

public static OptionDetail createOptionDetail()
{
	OptionDetail newOptionDetail = new OptionDetail();
	newOptionDetail.setDataSourceId( -1 );
	newOptionDetail.setOptionKey( -1 );
	newOptionDetail.setOptionName( "Mock Option" );

	return newOptionDetail;
}

public static VehicleOption createVehicleOption()
{
	VehicleOption vehicleOption = new VehicleOption();
	vehicleOption.setCreateDt( new Date() );
	vehicleOption.setOptionDetailId( -1 );
	vehicleOption.setOptionValue( true );
	vehicleOption.setVehicleId( -1 );

	return vehicleOption;
}

public static VehicleSaleEntity createVehicleSale()
{
	VehicleSaleEntity vehicleSale = new VehicleSaleEntity();
	vehicleSale.setDealDate( new Date() );
	vehicleSale.setFinanceInsuranceDealNumber( "-1" );
	vehicleSale.setFrontEndGross( 1200.0 );
	vehicleSale.setLastPolledDate( new Date() );
	vehicleSale.setLeaseFlag( 0 );
	vehicleSale.setNetFinancialInsuranceIncome( 500.0 );
	vehicleSale.setBackEndGross( 500.0 );
	vehicleSale.setSaleDescription( VehicleSaleEntity.VEHICLESALE_TYPE_RETAIL );
	vehicleSale.setSalePrice( 5000.0 );
	vehicleSale.setSalesReferenceNumber( "salesref" );
	vehicleSale.setVehicleMileage( 500000 );
	vehicleSale.setAnalyticalEngineStatus( VehicleSaleEntity.VEHICLESALE_ANALYTICAL_ENGINE_STATUS_ADDED );
	vehicleSale.setPack( 15.15 );
	vehicleSale.setAfterMarketGross( 1200.12 );
	vehicleSale.setTotalGross( 1000.12 );
	vehicleSale.setVehiclePromotionAmount( 500.50 );

	return vehicleSale;

}

public static Perspective createPerspective()
{
	Perspective perspective = new Perspective();

	return perspective;
}

public static BusinessUnit createBusinessUnit()
{
	BusinessUnit businessUnit = new BusinessUnit();
	businessUnit.setBusinessUnitTypeId( BusinessUnitType.BUSINESS_UNIT_CORPORATE_TYPE );

	return businessUnit;
}

public static DealerRisk createDealerRisk()
{
	DealerRisk dealerRisk = new DealerRisk();
	dealerRisk.setGreenLightDaysPercentage( 1 );
	dealerRisk.setGreenLightGrossProfitThreshold( 2.0 );
	dealerRisk.setGreenLightMarginThreshold( 3.0 );
	dealerRisk.setGreenLightNoSaleThreshold( 4 );
	dealerRisk.setGreenLightTarget( 5 );
	dealerRisk.setHighMileageThreshold(6 );
	dealerRisk.setHighMileagePerYearThreshold( 7 );
	dealerRisk.setExcessiveMileageThreshold(17 );
	dealerRisk.setRedLightGrossProfitThreshold( 8 );
	dealerRisk.setRedLightNoSaleThreshold( 9 );
	dealerRisk.setRedLightTarget( 10 );
	dealerRisk.setRiskLevelDealsThreshold( 11 );
	dealerRisk.setRiskLevelNumberOfContributors( 12 );
	dealerRisk.setRiskLevelNumberOfWeeks( 13 );
	dealerRisk.setRiskLevelYearInitialTimePeriod( 14 );
	dealerRisk.setRiskLevelYearRollOverMonth( 15 );
	dealerRisk.setRiskLevelYearSecondaryTimePeriod( 16 );

	return dealerRisk;
}
}