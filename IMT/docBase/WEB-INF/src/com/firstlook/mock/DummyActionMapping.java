package com.firstlook.mock;

import org.apache.struts.action.ActionForward;

public class DummyActionMapping extends org.apache.struts.action.ActionMapping
{

private static final long serialVersionUID = 6960551910954554513L;
private boolean returnNull;

public DummyActionMapping()
{

}

public ActionForward findForward( String forwardName )
{
    if ( returnNull )
    {
        return null;
    }

    ActionForward actionForward = new ActionForward();
    actionForward.setName(forwardName);
    actionForward.setPath("/" + forwardName);
    return actionForward;
}

public void setFindForwardToReturnNull( boolean returnNull )
{
    this.returnNull = returnNull;
}
}
