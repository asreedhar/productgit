package com.firstlook.taglib;

import com.firstlook.entity.form.TradeAnalyzerForm;
import com.firstlook.mock.DummyHttpRequest;
import com.firstlook.mock.DummyPageContext;

public class TestYearsOptionsTag extends com.firstlook.mock.BaseTestCase
{
private YearsOptionsTag tag;
private DummyHttpRequest request;
private com.firstlook.mock.DummyPageContext pageContext;

/**
 * TestYearsOptionsTag constructor comment.
 * 
 * @param arg1
 *            java.lang.String
 */
public TestYearsOptionsTag( String arg1 )
{
    super(arg1);
}

public void setup()
{
    tag = new YearsOptionsTag();
    request = new DummyHttpRequest();
    pageContext = new DummyPageContext();
    pageContext.setDummyRequest(request);
    tag.setPageContext(pageContext);
}

public void testCreateOptionTagNotSelected()
{
    String label = "All";
    String value = "0";
    String optionTag = "<option value=\"" + value + "\">" + label
            + "</option>\n";

    tag.setSelectedValueFromRequestOrBean();

    assertEquals(optionTag, tag.createOptionTag(label, value));
}

public void testCreateOptionTagSelected()
{
    String label = "All";
    String value = "0";
    String optionTag = "<option value=\"" + value + "\""
            + YearsOptionsTag.SELECTED + ">" + label + "</option>\n";

    tag.setRequestParamName("year");
    request.setParameter("year", value);

    tag.setSelectedValueFromRequestOrBean();

    assertEquals(optionTag, tag.createOptionTag(label, value));
}

public void testIsSelectedFalse()
{
    String value = "0";
    request.setParameter("year", value + "x");

    tag.setRequestParamName("year");

    tag.setSelectedValueFromRequestOrBean();

    assertTrue(!tag.isSelected(value));
}

public void testIsSelectedNullParamName()
{
    String value = "0";
    request.setParameter("year", value + "x");

    tag.setRequestParamName(null);

    tag.setSelectedValueFromRequestOrBean();

    assertTrue(!tag.isSelected(value));
}

public void testIsSelectedNullValue()
{
    String value = "0";

    tag.setRequestParamName("year");

    tag.setSelectedValueFromRequestOrBean();

    assertTrue(!tag.isSelected(value));
}

public void testIsSelectedTrue()
{
    String value = "0";
    request.setParameter("year", value);

    tag.setRequestParamName("year");

    tag.setSelectedValueFromRequestOrBean();

    assertTrue(tag.isSelected(value));
}

public void testSetSelectedValueFromRequestOrBean_FromBeanAsString()
{
    request.setAttribute("myBean", "32");

    tag.setBeanName("myBean");

    tag.setSelectedValueFromRequestOrBean();

    assertEquals("32", tag.getSelectedValue());
}

public void testSetSelectedValueFromRequestOrBean_FromBeanWithProperty()
{
    TradeAnalyzerForm form = new TradeAnalyzerForm();
    form.setYear(32);
    request.setAttribute("myBean", form);

    tag.setBeanName("myBean");
    tag.setProperty("year");

    tag.setSelectedValueFromRequestOrBean();

    assertEquals("32", tag.getSelectedValue());
}

public void testSetSelectedValueFromRequestOrBean_FromRequest()
{
    request.setParameter("paramName", "paramValue");

    tag.setRequestParamName("paramName");

    tag.setSelectedValueFromRequestOrBean();

    assertEquals("paramValue", tag.getSelectedValue());
}
}
