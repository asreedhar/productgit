package com.firstlook.taglib;

import com.firstlook.mock.DummyHttpRequest;
import com.firstlook.mock.DummyJspWriter;
import com.firstlook.mock.DummyPageContext;

public class TestMenuItemIndicatorTag extends com.firstlook.mock.BaseTestCase
{
private MenuItemIndicatorTag menuItemIndicator;
private com.firstlook.mock.DummyPageContext pageContext;
private com.firstlook.mock.DummyHttpRequest httpRequest;

/**
 * TestMenuItemIndicatorTag constructor comment.
 * 
 * @param arg1
 *            java.lang.String
 */
public TestMenuItemIndicatorTag( String arg1 )
{
    super(arg1);
}

public void setup()
{
    menuItemIndicator = new MenuItemIndicatorTag();
    pageContext = new DummyPageContext();
    httpRequest = new DummyHttpRequest();
    pageContext.setDummyRequest(httpRequest);
    menuItemIndicator.setPageContext(pageContext);

}

public void testDoStartWithMenuButWrongItem() throws Exception
{
    pageContext.getRequest().setAttribute("menu1", "item1");

    menuItemIndicator.setMenu("menu1");
    menuItemIndicator.setItem("item2");

    menuItemIndicator.doStartTag();
    String jspoutput = ((DummyJspWriter) pageContext.getOut()).getJSPOutput();
    assertEquals("menuitem should be off", "off", jspoutput);

}

public void testDoStartWithMenuCorrectItem() throws Exception
{
    pageContext.getRequest().setAttribute("menu1", "item1");

    menuItemIndicator.setMenu("menu1");
    menuItemIndicator.setItem("item1");

    menuItemIndicator.doStartTag();
    String jspoutput = ((DummyJspWriter) pageContext.getOut()).getJSPOutput();
    assertEquals("menuitem should be off", "on", jspoutput);

}

public void testDoStartWithOutMenuItem() throws Exception
{
    menuItemIndicator.doStartTag();
    String jspoutput = ((DummyJspWriter) pageContext.getOut()).getJSPOutput();
    assertEquals("menuitem should be off", "off", jspoutput);

}
}
