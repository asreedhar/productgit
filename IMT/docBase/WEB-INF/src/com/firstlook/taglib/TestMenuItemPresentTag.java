package com.firstlook.taglib;

import javax.servlet.jsp.tagext.TagSupport;

public class TestMenuItemPresentTag extends com.firstlook.mock.BaseTestCase
{
private MenuItemPresentTag tag;

public TestMenuItemPresentTag( String arg1 )
{
    super(arg1);
}

public void setup()
{
    tag = new MenuItemPresentTag();
}

public void testGetTagReturnValueFalse()
{
    assertEquals(TagSupport.SKIP_BODY, tag.getTagReturnValue(false));
}

public void testGetTagReturnValueTrue()
{
    assertEquals(TagSupport.EVAL_BODY_INCLUDE, tag.getTagReturnValue(true));
}
}
