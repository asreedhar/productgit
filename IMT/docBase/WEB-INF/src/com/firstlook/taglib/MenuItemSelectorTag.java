package com.firstlook.taglib;

public class MenuItemSelectorTag extends javax.servlet.jsp.tagext.TagSupport
{
private static final long serialVersionUID = 259235391913664579L;
private java.lang.String menu;
private java.lang.String item;

/**
 * MenuItemSelector constructor comment.
 */
public MenuItemSelectorTag()
{
    super();
}

public int doStartTag()
{
    pageContext.getRequest().setAttribute(menu, item);
    return SKIP_BODY;
}

/**
 * 
 * @return java.lang.String
 */
public java.lang.String getItem()
{
    return item;
}

/**
 * 
 * @return java.lang.String
 */
public java.lang.String getMenu()
{
    return menu;
}

/**
 * 
 * @param newItem
 *            java.lang.String
 */
public void setItem( java.lang.String newItem )
{
    item = newItem;
}

/**
 * 
 * @param newMenu
 *            java.lang.String
 */
public void setMenu( java.lang.String newMenu )
{
    menu = newMenu;
}
}
