package com.firstlook.taglib;

import javax.servlet.jsp.JspException;

import org.apache.struts.Globals;

public class ErrorsPresentTag extends javax.servlet.jsp.tagext.TagSupport
{
private static final long serialVersionUID = -790192392601681882L;
private boolean present = true;

/**
 * HasErrorsTag constructor comment.
 */
public ErrorsPresentTag()
{
    super();
}

public int doStartTag() throws JspException
{
    if ( hasErrors() && !present )
    {
        return SKIP_BODY;
    } else if ( hasErrors() && present )
    {
        return EVAL_BODY_INCLUDE;
    } else if ( !hasErrors() && present )
    {
        return SKIP_BODY;
    } else if ( !hasErrors() && !present )
    {
        return EVAL_BODY_INCLUDE;
    }
    throw new JspException("will never get here");
}

public java.lang.String getPresent()
{
    return String.valueOf(present);
}

public boolean hasErrors()
{
    return pageContext.getRequest().getAttribute(Globals.ERROR_KEY) != null;
}

public void setPresent( java.lang.String newPresent )
{
    present = Boolean.valueOf(newPresent).booleanValue();
}
}
