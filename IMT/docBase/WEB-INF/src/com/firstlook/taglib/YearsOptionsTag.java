package com.firstlook.taglib;

import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.servlet.jsp.JspException;

public class YearsOptionsTag extends javax.servlet.jsp.tagext.TagSupport
{
private static final long serialVersionUID = 8369750534286171456L;

public final static int NUMBER_OF_YEARS = 13;

public final static String ALL_VALUE = "0";;
public final static java.lang.String ALL_LABEL = "ALL";;
private java.lang.String requestParamName;
public final static java.lang.String SELECTED = " selected";;
private java.lang.String beanName;
private java.lang.String property;
private java.lang.String selectedValue;

/**
 * YearsOptionsTag constructor comment.
 */
public YearsOptionsTag()
{
    super();
}

protected String createOptionTag( String label )
{
    return createOptionTag(label, label);
}

protected String createOptionTag( String label, String value )
{
    StringBuffer sb = new StringBuffer();

    sb.append("<option value=\"" + value + "\"");
    if ( isSelected(value) )
    {
        sb.append(SELECTED);
    }

    sb.append(">" + label + "</option>\n");

    return sb.toString();
}

public int doStartTag() throws JspException
{
    setSelectedValueFromRequestOrBean();

    String optionTags = getOptionTags();

    try
    {
        pageContext.getOut().write(optionTags);
    } catch (Exception e)
    {
        e.printStackTrace();
    }

    return SKIP_BODY;
}

/**
 * 
 * @return java.lang.String
 */
public java.lang.String getBeanName()
{
    return beanName;
}

protected String getOptionTags()
{
    StringBuffer sb = new StringBuffer();

    sb.append(createOptionTag(ALL_LABEL, ALL_VALUE));

    GregorianCalendar calendar = new GregorianCalendar();
    int startingYear = calendar.get(Calendar.YEAR) + 1;

    for (int i = 0; i < NUMBER_OF_YEARS; i++)
    {
        int currentYear = startingYear - i;
        sb.append(createOptionTag(String.valueOf(currentYear)));
    }

    return sb.toString();
}

/**
 * 
 * @return java.lang.String
 */
public java.lang.String getProperty()
{
    return property;
}

/**
 * 
 * @return java.lang.String
 */
public java.lang.String getRequestParamName()
{
    return requestParamName;
}

protected String getSelectedValue()
{
    return selectedValue;
}

protected boolean isSelected( String value )
{
    return selectedValue.equals(value);
}

/**
 * 
 * @param newBeanName
 *            java.lang.String
 */
public void setBeanName( java.lang.String newBeanName )
{
    beanName = newBeanName;
}

/**
 * 
 * @param newProperty
 *            java.lang.String
 */
public void setProperty( java.lang.String newProperty )
{
    property = newProperty;
}

/**
 * 
 * @param newRequestParamName
 *            java.lang.String
 */
public void setRequestParamName( java.lang.String newRequestParamName )
{
    requestParamName = newRequestParamName;
}

protected void setSelectedValueFromRequestOrBean()
{
    if ( getRequestParamName() != null )
    {
        selectedValue = pageContext.getRequest().getParameter(
                getRequestParamName());
    } else if ( getBeanName() != null )
    {
        Object bean = pageContext.findAttribute(getBeanName());

        if ( getProperty() != null )
        {
            String methodName = "get"
                    + getProperty().substring(0, 1).toUpperCase()
                    + getProperty().substring(1);
            try
            {
                Method method = bean.getClass().getMethod(methodName, (Class[])null);
                selectedValue = method.invoke(bean, (Object[])null).toString();
            } catch (Exception e)
            {
                throw new RuntimeException(
                        "Error attempting to access method: "
                                + bean.getClass().getName() + "." + methodName
                                + " in YearsOptionsTag", e);
            }
        } else
        {
            selectedValue = bean.toString();
        }
    }

    selectedValue = (selectedValue == null) ? "" : selectedValue;
}
}
