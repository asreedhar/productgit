package com.firstlook.taglib;

import java.io.File;

import junit.framework.TestCase;

public class TestFileIncludeTag extends TestCase
{
private FileIncludeTag tag;
private java.lang.String staticDirectory;

public TestFileIncludeTag( String arg1 )
{
    super(arg1);
}

public void setUp()
{
    tag = new FileIncludeTag();
}

public void testGetFullFileNameWithFileSeparatorOnProperty()
{
    staticDirectory = "E:\\directory1\\directory2";
    System.setProperty(FileIncludeTag.STATIC_FILE_DIRECTORY, staticDirectory);

    tag.setFileName("static.jsp");

    assertEquals("Wrong full name.", staticDirectory + File.separator + tag.getFileName(), tag
            .getFullFileName());
}
 
public void testGetFullFileNameWithoutFileSeparatorOnProperty()
{
    staticDirectory = "E:\\directory1\\directory2";
    System.setProperty(FileIncludeTag.STATIC_FILE_DIRECTORY, staticDirectory);

    tag.setFileName("static.jsp");

    assertEquals("Wrong full name.", staticDirectory + File.separator
            + tag.getFileName(), tag.getFullFileName());
}
}
