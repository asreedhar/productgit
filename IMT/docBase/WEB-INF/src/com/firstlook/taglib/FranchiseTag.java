package com.firstlook.taglib;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.firstlook.service.dealer.DealerFranchiseService;

public class FranchiseTag extends javax.servlet.jsp.tagext.TagSupport
{

private static final long serialVersionUID = -3636528050985918698L;
private static Logger logger = Logger.getLogger(FranchiseTag.class);

public FranchiseTag()
{
    super();
}
/**
 * FranchiseTag needs to die a long, slow, hoorible, painful death. It does database access and is called
 * straight from a jsp - AB, 6/7/05
 */
public int doStartTag() throws javax.servlet.jsp.JspTagException
{
    try
    {
        DealerFranchiseService dfService = new DealerFranchiseService();
        Collection franchises = dfService.retrieveAllFranchises();
        pageContext.setAttribute("franchises", franchises);

    } catch (Exception e)
    {
        logger.error("FranchiseTag encountered problems", e);
    }

    return SKIP_BODY;

}
}
