package com.firstlook.taglib;

public class TestConstantTag extends com.firstlook.mock.BaseTestCase
{
public final static int TEST_CONSTANT_INT = 1;
public final static String TEST_CONSTANT_STRING = "test";

/**
 * TestConstantTag constructor comment.
 * 
 * @param arg1
 *            java.lang.String
 */
public TestConstantTag( String arg1 )
{
    super(arg1);
}

public void testGetConstantValueInt()
{
    ConstantTag tag = new ConstantTag();
    tag.setClassName("com.firstlook.taglib.TestConstantTag");
    tag.setConstantName("TEST_CONSTANT_INT");

    assertEquals(String.valueOf(TEST_CONSTANT_INT), tag.getConstantValue());
}

public void testGetConstantValueString()
{
    ConstantTag tag = new ConstantTag();
    tag.setClassName("com.firstlook.taglib.TestConstantTag");
    tag.setConstantName("TEST_CONSTANT_STRING");

    assertEquals(TEST_CONSTANT_STRING, tag.getConstantValue());
}
}
