package com.firstlook.taglib;

import java.util.ArrayList;
import java.util.Collection;
import java.util.StringTokenizer;

abstract public class BaseParamsTag extends javax.servlet.jsp.tagext.TagSupport
{

private java.lang.String parameterNames;

public BaseParamsTag()
{
    super();
}

protected Collection constructParamsCollection( String parameterNames )
{
    Collection params = new ArrayList();

    if ( parameterNames != null )
    {
        StringTokenizer tokenizer = new StringTokenizer(parameterNames, ",");

        while (tokenizer.hasMoreTokens())
        {
            params.add(tokenizer.nextToken().trim());
        }
    }

    return params;
}

public java.lang.String getParameterNames()
{
    return parameterNames;
}

public void setParameterNames( java.lang.String newParameterNames )
{
    parameterNames = newParameterNames;
}

}
