package com.firstlook.taglib;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.firstlook.mock.DummyHttpRequest;
import com.firstlook.mock.DummyPageContext;

/**
 * Insert the type's description here. Creation date: (6/11/2002 10:39:26 AM)
 * 
 * @author: Extreme Developer
 */
public class TestPrintablePageTag extends com.firstlook.mock.BaseTestCase
{
private PrintablePageTag tag;
private com.firstlook.mock.DummyHttpRequest request;

/**
 * TestPrintablePageTag constructor comment.
 * 
 * @param arg1
 *            java.lang.String
 */
public TestPrintablePageTag( String arg1 )
{
    super(arg1);
}

public void setup()
{
    DummyPageContext pageContext = new DummyPageContext();
    request = new DummyHttpRequest();
    pageContext.setDummyRequest(request);

    tag = new PrintablePageTag();
    tag.setPageContext(pageContext);
}

public void testGetUrlWithQueryStringParameters_NoParameters()
        throws UnsupportedEncodingException
{
    String url = "someaction.go";
    tag.setUrl(url);

    assertEquals(url, tag.getUrlWithQueryStringParameters());
}

public void testGetUrlWithQueryStringParameters_WithParameters()
        throws UnsupportedEncodingException
{
    String url = "someaction.go";
    String param1 = "param1";
    String param2 = "param2";
    String value1 = "value1 and more";
    String value2 = "value2";

    tag.setUrl(url);
    tag.setParameterNames(param1 + ", " + param2);

    request.setAttribute(param1, value1);
    request.setAttribute(param2, value2);

    String expectedUrl = url + "?" + param1 + "="
            + URLEncoder.encode(value1, "UTF-8") + "&" + param2 + "="
            + URLEncoder.encode(value2, "UTF-8");

    assertEquals(expectedUrl, tag.getUrlWithQueryStringParameters());
}

public void testGetUrlWithQueryStringParameters_WithParametersAndExistingQueryString()
        throws UnsupportedEncodingException
{
    String url = "someaction.go?param=value";
    String param1 = "param1";
    String param2 = "param2";
    String value1 = "value1 and more";
    String value2 = "value2";

    tag.setUrl(url);
    tag.setParameterNames(param1 + ", " + param2);

    request.setAttribute(param1, value1);
    request.setAttribute(param2, value2);

    String expectedUrl = url + "&" + param1 + "="
            + URLEncoder.encode(value1, "UTF-8") + "&" + param2 + "="
            + URLEncoder.encode(value2, "UTF-8");

    assertEquals(expectedUrl, tag.getUrlWithQueryStringParameters());
}

public void testPutBeanInRequest()
{
    String url = "someaction.go";

    tag.putBeanInRequest(url);

    assertNotNull("bean is null", request.getAttribute("printRef"));
    assertEquals("wrong value", url, (String) request.getAttribute("printRef"));
}
}
