package com.firstlook.taglib;

import javax.servlet.ServletRequest;

import biz.firstlook.commons.util.PropertyLoader;

public class ContentURLTag extends javax.servlet.jsp.tagext.TagSupport
{
private static final long serialVersionUID = -7282462532559322372L;
private java.lang.String fileType;

public ContentURLTag()
{
    super();
}

public int doStartTag() throws javax.servlet.jsp.JspTagException
{
    try
    {
        String urlString = getURLString();
        pageContext.getOut().write(urlString);
    } catch (Exception e)
    {
        e.printStackTrace();
    }
    return SKIP_BODY;
}

public java.lang.String getFileType()
{
    return fileType;
}

String getProtocol( ServletRequest request )
{
    return request.getScheme() + "://";
}

String getUrlPath()
{
    String propertyRoot = "firstlook.content.url";
    String property = propertyRoot + "." + getFileType();
    return PropertyLoader.getProperty(property);
}

public String getURLString()
{
    return getProtocol(pageContext.getRequest()) + getUrlPath();
}

public void setFileType( java.lang.String newFileType )
{
    fileType = newFileType;
}

}
