package com.firstlook.taglib;

import javax.servlet.jsp.JspException;

public class MenuItemNotPresentTag extends MenuItemBaseTag
{
private static final long serialVersionUID = 3398718244095823903L;

/**
 * MenuItemEvaluatorTag constructor comment.
 */
public MenuItemNotPresentTag()
{
    super();
}

public int doStartTag() throws JspException
{
    return getTagReturnValue(isMenuItemPresent());
}

int getTagReturnValue( boolean isMenuItemPresent )
{
    return isMenuItemPresent ? SKIP_BODY : EVAL_BODY_INCLUDE;
}
}
