package com.firstlook.taglib;

import javax.servlet.jsp.JspException;

public class MenuItemPresentTag extends MenuItemBaseTag
{
private static final long serialVersionUID = 8112193330686662052L;

/**
 * MenuItemEvaluatorTag constructor comment.
 */
public MenuItemPresentTag()
{
    super();
}

public int doStartTag() throws JspException
{
    return getTagReturnValue(isMenuItemPresent());
}

int getTagReturnValue( boolean isMenuItemPresent )
{
    return isMenuItemPresent ? EVAL_BODY_INCLUDE : SKIP_BODY;
}
}
