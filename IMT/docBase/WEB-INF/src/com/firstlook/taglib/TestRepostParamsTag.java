package com.firstlook.taglib;

import java.util.ArrayList;
import java.util.Collection;

import com.firstlook.mock.DummyHttpRequest;

public class TestRepostParamsTag extends com.firstlook.mock.BaseTestCase
{
private DummyHttpRequest request;

private RepostParamsTag repostParamsTag;

/**
 * TestWebUtil constructor comment.
 * 
 * @param arg1
 *            java.lang.String
 */
public TestRepostParamsTag( String arg1 )
{
    super(arg1);
}

public void setup() throws Exception
{
    repostParamsTag = new RepostParamsTag();

    request = new DummyHttpRequest();
    request.setParameter("param1", "value1");
}

public void testRepostParameters()
{
    assertEquals("<INPUT TYPE='HIDDEN' NAME='param1' VALUE='value1'>\n",
            repostParamsTag.repostParameters(request, new ArrayList()));
}

public void testRepostParametersExclude()
{
    repostParamsTag.setInclude(false);
    request.setParameter("excludedParam", "excludedValue");

    Collection excludedParams = new ArrayList();
    excludedParams.add("excludedParam");

    assertEquals("<INPUT TYPE='HIDDEN' NAME='param1' VALUE='value1'>\n",
            repostParamsTag.repostParameters(request, excludedParams));
}

public void testRepostParametersInclude()
{
    repostParamsTag.setInclude(true);
    request.setParameter("includedParam", "includedValue");

    Collection includedParams = new ArrayList();
    includedParams.add("includedParam");

    assertEquals(
            "<INPUT TYPE='HIDDEN' NAME='includedParam' VALUE='includedValue'>\n",
            repostParamsTag.repostParameters(request, includedParams));
}

public void testRepostParametersMultivalue()
{
    request.setParameter("param1", "anotherValueForParam1");
    String expected = "<INPUT TYPE='HIDDEN' NAME='param1' VALUE='value1'>\n<INPUT TYPE='HIDDEN' NAME='param1' VALUE='anotherValueForParam1'>\n";
    assertEquals(expected, repostParamsTag.repostParameters(request,
            new ArrayList()));
}

public void testRepostParametersWithIncludeTrue()
{
    repostParamsTag.setInclude(true);
    assertEquals("", repostParamsTag.repostParameters(request, new ArrayList()));
}
}
