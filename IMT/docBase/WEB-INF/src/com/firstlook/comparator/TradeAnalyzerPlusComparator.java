package com.firstlook.comparator;

import biz.firstlook.transact.persist.service.appraisal.IAppraisal;

import com.firstlook.exception.ApplicationException;

public class TradeAnalyzerPlusComparator extends BaseComparator
{

public TradeAnalyzerPlusComparator()
{
    super();
}

public int compare( Object o1, Object o2 )
{
    try
    {
        IAppraisal t1 = (IAppraisal) o1;
        IAppraisal t2 = (IAppraisal) o2;

        return compare(t1, t2);
    } catch (Exception e)
    {
        throw new RuntimeException(e.getMessage(), e);
    }
}

public int compare( IAppraisal tradeAnalyzerPlus1, IAppraisal tradeAnalyzerPlus2 )
        throws ApplicationException
{
    int retVal = compareGroupingDescription(tradeAnalyzerPlus1,
            tradeAnalyzerPlus2);
    if ( retVal == 0 )
    {
        retVal = compareYear(tradeAnalyzerPlus1, tradeAnalyzerPlus2);
    }

    return retVal;
}

protected int compareGroupingDescription( IAppraisal t1, IAppraisal t2 )
        throws ApplicationException
{
    String groupingDescription1 = t1.getVehicle().getMakeModelGrouping().getGroupingDescription().getGroupingDescription();
    String groupingDescription2 = t2.getVehicle().getMakeModelGrouping().getGroupingDescription().getGroupingDescription();
    return compareStringsAlphabetical(groupingDescription1,
            groupingDescription2);
}

protected int compareYear( IAppraisal v1, IAppraisal v2 )
{
    return compareIntValues(v1.getVehicle().getVehicleYear().intValue(), v2.getVehicle().getVehicleYear().intValue());
}

}
