package com.firstlook.comparator;

import com.firstlook.entity.InventoryEntity;

public class InventoryComparator extends BaseVehicleComparator
{

private boolean sortYearsDescending;

public InventoryComparator()
{
    super();
}

public int compare( InventoryEntity inventory1, InventoryEntity inventory2 )
{
    int retVal = compareGroupingDescription(inventory1, inventory2);

    if ( retVal == 0 )
    {
        if ( isSortYearsDescending() )
        {
            retVal = compareYear(inventory2, inventory1);
        } else
        {
            retVal = compareYear(inventory1, inventory2);
        }
        if ( retVal == 0 )
        {
            retVal = compareTrim(inventory1, inventory2);
        }
    }

    return retVal;
}

public boolean isSortYearsDescending()
{
    return sortYearsDescending;
}

public void setSortYearsDescending( boolean newSortYearsDescending )
{
    sortYearsDescending = newSortYearsDescending;
}

}
