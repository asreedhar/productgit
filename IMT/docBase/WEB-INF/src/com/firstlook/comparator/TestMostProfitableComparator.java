package com.firstlook.comparator;

import java.util.Vector;

import junit.framework.TestCase;

import com.firstlook.entity.Dealer;
import com.firstlook.mock.ObjectMother;
import com.firstlook.report.Report;
import com.firstlook.report.ReportGrouping;

public class TestMostProfitableComparator extends TestCase
{

private int threshold = Dealer.UNITS_SOLD_THRESHOLD_DEFAULT;
private MostProfitableComparator mpc;
private ReportGrouping rg1;
private ReportGrouping rg2;

public TestMostProfitableComparator( String arg1 )
{
    super(arg1);
}

public void setUp()
{
    mpc = new MostProfitableComparator(threshold, Report.STANDARD_MODE);

    Vector groupings = ObjectMother.createReportGroupings();

    rg1 = (ReportGrouping) groupings.elementAt(0);
    rg2 = (ReportGrouping) groupings.elementAt(1);
}

public void testCompareEqualToBothAboveThreshold()
{
    rg1.setUnitsSold(threshold + 1);
    rg1.setGroupingName("GROUP1");
    rg2.setUnitsSold(threshold + 1);
    rg2.setGroupingName("GROUP1");

    rg1.setAverageFrontEnd(new Integer(65743));
    rg2.setAverageFrontEnd(new Integer(65743));
    assertEquals(0, mpc.compare(rg1, rg2));

    mpc.setDisplayMode(Report.OPTIMIX_MODE);
    rg1.setTotalFrontEnd(new Integer(65743));
    rg2.setTotalFrontEnd(new Integer(65743));
    assertEquals(0, mpc.compare(rg1, rg2));

}

public void testCompareEqualToBothAboveThresholdAlphabetical()
{
    rg1.setUnitsSold(threshold + 1);
    rg1.setGroupingName("GROUP2");
    rg2.setUnitsSold(threshold + 1);
    rg2.setGroupingName("GROUP1");

    rg1.setAverageFrontEnd(new Integer(65743));
    rg2.setAverageFrontEnd(new Integer(65743));
    assertEquals(1, mpc.compare(rg1, rg2));

    mpc.setDisplayMode(Report.OPTIMIX_MODE);
    rg1.setTotalFrontEnd(new Integer(65743));
    rg2.setTotalFrontEnd(new Integer(65743));
    assertEquals(1, mpc.compare(rg1, rg2));

}

public void testCompareEqualToBothBelowThreshold()
{
    rg1.setUnitsSold(threshold - 1);
    rg1.setGroupingName("GROUP1");
    rg2.setUnitsSold(threshold - 1);
    rg2.setGroupingName("GROUP1");

    rg1.setAverageFrontEnd(new Integer(65743));
    rg2.setAverageFrontEnd(new Integer(65743));
    assertEquals(0, mpc.compare(rg1, rg2));

    mpc.setDisplayMode(Report.OPTIMIX_MODE);
    rg1.setTotalFrontEnd(new Integer(65743));
    rg2.setTotalFrontEnd(new Integer(65743));
    assertEquals(0, mpc.compare(rg1, rg2));

}

public void testCompareEqualToBothBelowThresholdAlphabetical()
{
    rg1.setUnitsSold(threshold - 1);
    rg1.setGroupingName("GROUP1");
    rg2.setUnitsSold(threshold - 1);
    rg2.setGroupingName("GROUP2");

    rg1.setAverageFrontEnd(new Integer(65743));
    rg2.setAverageFrontEnd(new Integer(65743));
    assertEquals(-1, mpc.compare(rg1, rg2));

    mpc.setDisplayMode(Report.OPTIMIX_MODE);
    rg1.setTotalFrontEnd(new Integer(65743));
    rg2.setTotalFrontEnd(new Integer(65743));
    assertEquals(-1, mpc.compare(rg1, rg2));

}

public void testCompareGreaterThanAboveThreshold()
{
    rg1.setUnitsSold(threshold + 1);
    rg1.setAverageFrontEnd(new Integer(65743));

    rg2.setUnitsSold(threshold + 1);
    rg2.setAverageFrontEnd(new Integer(45633));

    assertEquals(-1, mpc.compare(rg1, rg2));

    mpc.setDisplayMode(Report.OPTIMIX_MODE);
    rg1.setTotalFrontEnd(new Integer(65743));
    rg2.setTotalFrontEnd(new Integer(65733));
    assertEquals(-1, mpc.compare(rg1, rg2));

}

public void testCompareLessThanAboveThreshold()
{
    rg1.setUnitsSold(threshold + 1);
    rg1.setAverageFrontEnd(new Integer(23733));

    rg2.setAverageFrontEnd(new Integer(45633));
    rg2.setUnitsSold(threshold + 1);

    assertEquals(1, mpc.compare(rg1, rg2));

    mpc.setDisplayMode(Report.OPTIMIX_MODE);
    rg1.setTotalFrontEnd(new Integer(65723));
    rg2.setTotalFrontEnd(new Integer(65733));
    assertEquals(1, mpc.compare(rg1, rg2));
}

public void testCompareNullValues()
{
    rg1.setUnitsSold(threshold + 1);
    rg2.setUnitsSold(threshold + 1);

    rg1.setAverageFrontEnd(new Integer(2563));
    rg2.setAverageFrontEnd(null);

    assertEquals(-1, mpc.compare(rg1, rg2));
}

public void testCompareOneAboveOneBelowThreshold()
{
    rg1.setUnitsSold(threshold + 1);
    rg2.setUnitsSold(threshold);

    assertEquals(1, mpc.compare(rg2, rg1));
}

public void testCompareOneBelowOneAboveThreshold()
{
    rg1.setUnitsSold(threshold);
    rg2.setUnitsSold(threshold + 1);

    assertEquals(-1, mpc.compare(rg2, rg1));
}

public void testMostProfitableComparatorConstructor()
{
    assertEquals(threshold, mpc.getUnitsSoldThreshold());
}

}
