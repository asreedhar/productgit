package com.firstlook.comparator;

import com.firstlook.report.Report;
import com.firstlook.report.ReportGrouping;

public class TopSellerComparator extends ReportBaseComparator
{

private int displayMode;

public TopSellerComparator( int threshold, int displayMode )
{
    super(threshold);
    this.displayMode = displayMode;
}

protected int compare( ReportGrouping rg1, ReportGrouping rg2 )
{
    int val1 = 0;
    int val2 = 0;
    double dval1 = 0;
    double dval2 = 0;

    int returnedVal = 0;

    if ( displayMode == Report.STANDARD_MODE )
    {
        val1 = rg1.getUnitsSold();
        val2 = rg2.getUnitsSold();

        returnedVal = compareIntValues(val1, val2);
    } else if ( displayMode == Report.OPTIMIX_MODE )
    {
        dval1 = rg1.getPercentageTotalRevenue();
        dval2 = rg2.getPercentageTotalRevenue();

        returnedVal = compareDoubleValues(dval1, dval2);
    }

    if ( returnedVal == 0 )
    {
        int mostProfitVal = compareMostProfitableInts(rg1.getAvgGrossProfit(),
                rg2.getAvgGrossProfit());
        if ( mostProfitVal == 0 )
        {
            return compareStringsAlphabetical(rg1.getGroupingName(), rg2
                    .getGroupingName());
        }
        return mostProfitVal;
    }
    return returnedVal;
}

protected int compareIntValues( int val1, int val2 )
{
    return (val1 < val2 ? 1 : (val1 == val2 ? 0 : -1));
}

protected int compareDoubleValues( double val1, double val2 )
{
    return (val1 < val2 ? 1 : (val1 == val2 ? 0 : -1));
}

protected Integer[] getIntegersToCompare( ReportGrouping rg1, ReportGrouping rg2 )
{
    return null;
}

}
