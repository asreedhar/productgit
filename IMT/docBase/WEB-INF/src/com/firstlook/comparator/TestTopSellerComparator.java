package com.firstlook.comparator;

import com.firstlook.entity.Dealer;
import com.firstlook.report.Report;
import com.firstlook.report.ReportGrouping;

public class TestTopSellerComparator extends junit.framework.TestCase
{

private TopSellerComparator tsc;
private com.firstlook.report.ReportGrouping rg1;
private com.firstlook.report.ReportGrouping rg2;

public TestTopSellerComparator( String arg1 )
{
    super(arg1);
}

public void setUp()
{
    tsc = new TopSellerComparator(Dealer.UNITS_SOLD_THRESHOLD_DEFAULT,
            Report.STANDARD_MODE);
    rg1 = new ReportGrouping();
    rg2 = new ReportGrouping();

    rg1.setAvgGrossProfit(new Integer(500));
    rg2.setAvgGrossProfit(new Integer(500));
}

// TODO: put in tests for optimix sorting
public void testCompareEqualTo()
{
    rg1.setUnitsSold(59);
    rg1.setGroupingName("GROUP1");

    rg2.setUnitsSold(59);
    rg2.setGroupingName("GROUP1");

    assertEquals(0, tsc.compare(rg2, rg1));
}

public void testCompareEqualToAlphabetical()
{
    rg1.setUnitsSold(59);
    rg1.setGroupingName("GROUP1");

    rg2.setUnitsSold(59);
    rg2.setGroupingName("GROUP2");

    assertEquals(1, tsc.compare(rg2, rg1));
}

public void testCompareGreaterThan()
{
    rg1.setUnitsSold(59);

    rg2.setUnitsSold(27);

    assertEquals(-1, tsc.compare(rg1, rg2));
}

public void testCompareLessThan()
{
    rg1.setUnitsSold(59);

    rg2.setUnitsSold(27);

    assertEquals(-1, tsc.compare(rg1, rg2));
}

public void testCompareMostProfitableSort()
{
    rg1.setUnitsSold(Dealer.UNITS_SOLD_THRESHOLD_DEFAULT + 1);
    rg1.setGroupingName("GROUP1");
    rg2.setUnitsSold(Dealer.UNITS_SOLD_THRESHOLD_DEFAULT + 1);
    rg2.setGroupingName("GROUP1");

    rg1.setAvgGrossProfit(new Integer(500));
    rg2.setAvgGrossProfit(new Integer(1000));

    assertEquals(1, tsc.compare(rg1, rg2));
}

public void testComparePercentageTotalRevenueSort()
{
    tsc = new TopSellerComparator(Dealer.UNITS_SOLD_THRESHOLD_DEFAULT,
            Report.OPTIMIX_MODE);

    ReportGrouping grouping = new ReportGrouping();
    grouping.setTotalRevenue(new Integer(10));
    grouping.setPercentageTotalRevenue(.250);
    ReportGrouping grouping2 = new ReportGrouping();
    grouping2.setTotalRevenue(new Integer(2));
    grouping2.setPercentageTotalRevenue(.010);

    assertEquals(-1, tsc.compare(grouping, grouping2));
}

}
