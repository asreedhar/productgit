package com.firstlook.comparator;

import java.util.Comparator;

import com.firstlook.display.purchasing.IVehicleDisplayData;

public class VehicleDisplayDataComparator implements Comparator
{

public VehicleDisplayDataComparator()
{
	super();
}

public int compare( Object arg0, Object arg1 )
{
	IVehicleDisplayData displayData1 = (IVehicleDisplayData)arg0;
	IVehicleDisplayData displayData2 = (IVehicleDisplayData)arg1;
	
	if( displayData1.getSearchScore() < displayData2.getSearchScore() )
	{
		return -1;
	}
	else if( displayData1.getSearchScore() > displayData2.getSearchScore() )
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

}
