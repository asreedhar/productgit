package com.firstlook.comparator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import biz.firstlook.transact.persist.model.BodyType;
import biz.firstlook.transact.persist.model.Vehicle;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.MockInventory;
import com.firstlook.mock.ObjectMother;

public class TestBaseVehicleComparator extends BaseTestCase
{

private BaseVehicleComparator comp;
private InventoryEntity inventory1;
private InventoryEntity inventory2;
private InventoryEntity inventory3;
final static int DEALER_1 = 1;
final static int DEALER_2 = 2;
final static int DEALER_3 = 3;
final static int DEALER_4 = 4;
private Vehicle vehicle1;
private Vehicle vehicle2;
private Vehicle vehicle3;
private BodyType bodyType1;
private BodyType bodyType2;
private BodyType bodyType3;

public TestBaseVehicleComparator( String arg1 )
{
    super(arg1);
}

public void setup() throws ApplicationException
{
    Collection col = new ArrayList();

    comp = new BaseVehicleComparatorImpl();

    bodyType1 = ObjectMother.createBodyType();
    bodyType1.setBodyTypeId(new Integer(1));
    bodyType1.setBodyType("a");

    bodyType2 = ObjectMother.createBodyType();
    bodyType2.setBodyTypeId(new Integer(2));
    bodyType2.setBodyType("b");

    bodyType3 = ObjectMother.createBodyType();
    bodyType3.setBodyTypeId(new Integer(3));
    bodyType3.setBodyType("c");

    Dealer dealer1 = ObjectMother.createDealer();
    dealer1.setDealerId(new Integer(1));
    dealer1.setName("a");
    dealer1.setNickname("a");
    dealer1.setDealerType(Dealer.DEALER_TYPE_SELLER);

    Dealer dealer2 = ObjectMother.createDealer();
    dealer2.setDealerId(new Integer(2));
    dealer2.setNickname("b");
    dealer2.setName("b");
    dealer2.setDealerType(Dealer.DEALER_TYPE_INSTITUTIONAL);

    Dealer dealer3 = ObjectMother.createDealer();
    dealer3.setDealerId(new Integer(3));
    dealer3.setName("c");
    dealer3.setNickname("c");
    dealer3.setDealerType(Dealer.DEALER_TYPE_FIRSTLOOK);

    Dealer dealer4 = ObjectMother.createDealer();
    dealer4.setDealerId(new Integer(4));
    dealer4.setName("d");
    dealer4.setNickname("d");
    dealer4.setDealerType(Dealer.DEALER_TYPE_FIRSTLOOK);

    col.add(bodyType3);
    col.add(bodyType2);
    col.add(bodyType1);
    mockDatabase.setReturnObjects(col);

    inventory1 = ObjectMother.createInventory();
    inventory1.setDealer(dealer1);
    inventory1.setDealerId(dealer1.getDealerId().intValue());
    inventory1.setVehicle(vehicle1);

    inventory2 = ObjectMother.createInventory();
    inventory2.setDealer(dealer2);
    inventory2.setDealerId(dealer2.getDealerId().intValue());
    inventory2.setVehicle(vehicle2);

    inventory3 = ObjectMother.createInventory();
    inventory3.setDealer(dealer3);
    inventory3.setDealerId(dealer3.getDealerId().intValue());
    inventory3.setVehicle(vehicle3);

    inventory1.setInventoryId(new Integer(1));
    inventory1.setMake("a");
    inventory1.setModel("a");
    inventory1.setVehicleYear(1);
    inventory1.setVehicleTrim("a");
    inventory1.setDmiVehicleType(Vehicle.VEHICLE_TYPE_C.intValue());
    inventory1.setStockNumber("a");
    inventory1.setDealerId(DEALER_1);
    inventory1.setInventoryReceivedDt(new Date());

    inventory2.setInventoryId(new Integer(2));
    inventory2.setMake("b");
    inventory2.setModel("b");
    inventory2.setVehicleYear(2);
    inventory2.setVehicleTrim("b");
    inventory2.setDmiVehicleType(Vehicle.VEHICLE_TYPE_M.intValue());
    inventory2.setStockNumber("b");
    inventory2.setDealerId(DEALER_2);
    inventory2
            .setInventoryReceivedDt(new Date(System.currentTimeMillis() + 100));

    inventory3.setInventoryId(new Integer(3));
    inventory3.setMake("c");
    inventory3.setModel("c");
    inventory3.setVehicleYear(3);
    inventory3.setVehicleTrim("c");
    inventory3.setDmiVehicleType(Vehicle.VEHICLE_TYPE_C.intValue());
    inventory3.setStockNumber("c");
    inventory3.setDealerId(DEALER_3);

}

public void testCompareDealerName() throws Exception
{
    assertTrue("compareDealerName failed", comp.compareDealerName(inventory1,
            inventory2) < 0);
}

public void testCompareDealerNickname() throws Exception
{
    assertTrue("compareDealerNickname failed", comp.compareDealerNickname(
            inventory1, inventory2) < 0);
}

public void testCompareDealerType() throws Exception
{
    assertEquals("Dealer Type Comparison v1v2 Failed", -1, comp
            .compareDealerType(inventory1, inventory2));
    assertEquals("Dealer Type Comparison v2v3 Failed", -1, comp
            .compareDealerType(inventory2, inventory3));
    assertEquals("Dealer Type Comparison v1v3 Failed", -1, comp
            .compareDealerType(inventory1, inventory3));
    assertEquals("Dealer Type Comparison v3v1 Failed", 1, comp
            .compareDealerType(inventory3, inventory1));
    assertEquals("Dealer Type SELLER Equality Failed", 0, comp
            .compareDealerType(inventory1, inventory1));
    assertEquals("Dealer Type INST Equality Failed", 0, comp.compareDealerType(
            inventory2, inventory2));
    assertEquals("Dealer Type FIRSTLOOK Equality Failed", 0, comp
            .compareDealerType(inventory3, inventory3));
}

public void testCompareGroupingDescription()
{
    MockInventory mock1 = new MockInventory();
    MockInventory mock2 = new MockInventory();

    mock1.setGroupingDescription("DDDDD");
    mock2.setGroupingDescription("TTTTT");

    assertTrue("compareGroupingDescription failed", comp
            .compareGroupingDescription(mock1, mock2) < 0);
}

public void testCompareListing()
{
    assertTrue("compareListing failed", comp.compareListing(inventory1,
            inventory2) < 0);
}

public void testCompareMake()
{
    assertTrue("compareMake failed",
            comp.compareMake(inventory1, inventory2) < 0);
}

public void testCompareModel()
{
    assertTrue("compareModel failed",
            comp.compareModel(inventory1, inventory2) < 0);
}

public void testCompareReceivedDate()
{
    assertTrue(comp.compareReceivedDate(inventory1, inventory2) < 0);
}

public void testCompareStockNumber()
{
    assertTrue("compareStockNumber failed", comp.compareStockNumber(inventory1,
            inventory2) < 0);
}

public void testCompareTrim()
{
    assertTrue("compareTrim failed",
            comp.compareTrim(inventory1, inventory2) < 0);
}

public void testCompareVehicleType() throws Exception
{
    assertEquals("Vehicle Type Comparison v1v2 Failed", -1, comp
            .compareVehicleType(inventory1, inventory2));
    assertEquals("Vehicle Type Comparison v2v1 Failed", 1, comp
            .compareVehicleType(inventory2, inventory1));
    assertEquals("Vehicle Type Comparison v1v1 Failed", 0, comp
            .compareVehicleType(inventory1, inventory1));
    assertEquals("Vehicle Type Comparison v2v2 Failed", 0, comp
            .compareVehicleType(inventory2, inventory2));
}

public void testCompareYear()
{
    assertTrue("compareYear failed",
            comp.compareYear(inventory1, inventory2) <= 0);
}

}
