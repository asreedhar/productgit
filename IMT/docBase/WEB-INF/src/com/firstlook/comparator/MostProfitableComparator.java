package com.firstlook.comparator;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;

import com.firstlook.report.Report;
import com.firstlook.report.ReportGrouping;

public class MostProfitableComparator extends ReportBaseComparator
{

private static Logger logger = Logger.getLogger(MostProfitableComparator.class);

private int displayMode;
private String property = "DEFAULT";

public MostProfitableComparator( int unitsSoldThreshold, int mode,
        String property )
{
    super(unitsSoldThreshold);
    this.displayMode = mode;
    this.property = property;
}

public MostProfitableComparator( int unitsSoldThreshold, int mode )
{
    super(unitsSoldThreshold);
    this.displayMode = mode;
}

protected int compare( ReportGrouping rg1, ReportGrouping rg2 )
{
    int thresholdVal = isBothInSameThreshold(rg1, rg2);
    if ( thresholdVal == 0 )
    {
        int returnedVal = compareBothAboveOrBothBelowThreshold(rg1, rg2);
        if ( returnedVal == 0 )
        {
            return compareStringsAlphabetical(rg1.getGroupingName(), rg2
                    .getGroupingName());
        }
        return returnedVal;
    } else
    {
        return thresholdVal;
    }
}

protected int compareIntValues( int val1, int val2 )
{
    return (val1 < val2 ? 1 : (val1 == val2 ? 0 : -1));
}

protected Integer[] getIntegersToCompare( ReportGrouping rg1, ReportGrouping rg2 )
{
    Integer[] ints = new Integer[2];

    if ( property.equals("DEFAULT") )
    {
        if ( displayMode == Report.OPTIMIX_MODE )
        {
            ints[0] = rg1.getTotalFrontEnd();
            ints[1] = rg2.getTotalFrontEnd();
        } else if ( displayMode == Report.STANDARD_MODE )
        {
            ints[0] = rg1.getAverageFrontEnd();
            ints[1] = rg2.getAverageFrontEnd();
        }
    } else
    {
        try
        {
            ints[0] = (Integer) PropertyUtils.getProperty(rg1, property);
            ints[1] = (Integer) PropertyUtils.getProperty(rg2, property);
        } catch (Exception e)
        {
            logger.error("Error getting property " + property
                    + " from ReportGrouping");
        }
    }
    return ints;
}

public int getDisplayMode()
{
    return displayMode;
}

public void setDisplayMode( int i )
{
    displayMode = i;
}

}
