package com.firstlook.comparator;

import com.firstlook.entity.InventoryEntity;

public class UsedInventoryVehicleComparator extends BaseVehicleComparator
{

public UsedInventoryVehicleComparator()
{
    super();
}

public int compare( InventoryEntity v1, InventoryEntity v2 ) throws Exception
{
    int retVal = compareGroupingDescription(v1, v2);

    if ( retVal == 0 )
    {
        retVal = compareReceivedDate(v1, v2);
        if ( retVal == 0 )
        {
            retVal = compareMake(v1, v2);
            if ( retVal == 0 )
            {
                retVal = compareModel(v1, v2);
                if ( retVal == 0 )
                {
                    retVal = compareStockNumber(v1, v2);
                }
            }
        }
    }

    return retVal;
}

}
