package com.firstlook.comparator;

import java.util.Comparator;

import com.firstlook.entity.form.SaleForm;

public class SaleComparator extends BaseComparator implements Comparator
{

public SaleComparator( String sortField )
{
    setSortField(sortField);
}

public int compare( Object obj1, Object obj2 )
{
    SaleForm sf1 = (SaleForm) obj1;
    SaleForm sf2 = (SaleForm) obj2;
    if ( sortField.equals("getMileage") )
    {
        if ( sf1.getMileage() == Integer.MIN_VALUE
                && sf2.getMileage() != Integer.MIN_VALUE )
        {
            return 1;
        } else if ( sf2.getMileage() == Integer.MIN_VALUE
                && sf1.getMileage() != Integer.MIN_VALUE )
        {
            return -1;
        } else if ( sf2.getMileage() == Integer.MIN_VALUE
                && sf1.getMileage() == Integer.MIN_VALUE )
        {
            return 0;
        }
    }
    if ( sortField.equals("getGrossMargin") )
    {
        if ( Double.isNaN(sf1.getGrossMargin())
                && !Double.isNaN(sf2.getGrossMargin()) )
        {
            return 1;
        } else if ( Double.isNaN(sf2.getGrossMargin())
                && !Double.isNaN(sf1.getGrossMargin()) )
        {
            return -1;
        } else if ( Double.isNaN(sf2.getGrossMargin())
                && Double.isNaN(sf1.getGrossMargin()) )
        {
            return 0;
        }
    }
    if ( sortField.equals("getUnitCost") )
    {
        setSortAscending(false);
        if ( Double.isNaN(sf1.getUnitCost())
                && !Double.isNaN(sf2.getUnitCost()) )
        {
            return 1;
        } else if ( Double.isNaN(sf2.getUnitCost())
                && !Double.isNaN(sf1.getUnitCost()) )
        {
            return -1;
        } else if ( Double.isNaN(sf2.getUnitCost())
                && Double.isNaN(sf1.getUnitCost()) )
        {
            return 0;
        }
    }

    return super.compare(sf1, sf2);
}

public void setSortField( String sortField )
{
    this.sortField = "get" + firstCharToUpper(sortField);

    if ( sortField.equalsIgnoreCase("year")
            || sortField.equalsIgnoreCase("dealDate")
            || sortField.equalsIgnoreCase("grossMargin")
            || sortField.equalsIgnoreCase("salePrice")
            || sortField.equalsIgnoreCase("fiGrossProfit") )
    {
        setSortAscending(false);
    }
}

}
