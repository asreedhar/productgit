package com.firstlook.requestprocessor;

import com.firstlook.session.FirstlookSession;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.*;

import org.apache.struts.tiles.*;

import org.springframework.web.struts.DelegatingTilesRequestProcessor;

public class ThemeDelegatingRequestProcessor extends DelegatingTilesRequestProcessor{

	//DIRECTORY_PATH_3_0
	//This is the directory where FirstLook 3.0 versions of tiles and/or jspXs will reside.
	//The directory structure must match the structure outside of the /theme2012/2_0_replacements directory.
	//For example let's say that the tradeManager_tabs.jsp will have a 3.0 version,
	// this jsp currently exists in the content/trades directory under docBase, therefore
	// the 3.0 version of this page will need to exist under /theme2012/2_0_replacements/content/trades.
	//This defined structure will make it easier to generically switch to 3.0 pages if they exist.
	private final String DIRECTORY_PATH_3_0 = "/goldfish/replacements_for_2_0";
	
	protected void doForward(String uri, HttpServletRequest request, HttpServletResponse response) 
		throws java.io.IOException, javax.servlet.ServletException{
		
		//Grab the firstlook session to check for the 3.0 upgrade
		FirstlookSession session = getFirstlookSessionFromRequest(request);
		
		URI rsrc = null;
		
		String path = null;
		
		//Instantiate a URI object if we can from the URI string
		try {
			rsrc = new URI(uri);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//If we have a valid URI object then get the decoded path component of this URI, that is what we'll need to do the path checking against.
		if (rsrc != null){
			path = rsrc.getPath();
		}
		
		//Is there a session? If so, is this a dealer that has the 3.0 upgrade? And are we dealing with a valid URI?
		if (session != null && session.isIncludeFirstlook_3_0() && path != null){			
			
			//Check to see if this has already been defined as a 3.0 resource (which implies it is most likely from tile processing)
			if (!path.startsWith(DIRECTORY_PATH_3_0)){
			
				//Is this some form of jsp / jspf resource being requested?
				int extensionLen = 4;  //Default to length of 4 chars ("jspf")

				int index = path.indexOf("jspf");

				if (index < 0 ){
					
					index = path.indexOf("jsp");
					
					extensionLen = 3;  //Length of "jsp"
				}
			
				if (index >= 0){
					
					//Yes it is ... Get actual jsp \ jspf resource minus the query string
					String jspXResource = path.substring(0, index + extensionLen);
					
					//Since it hasn't been defined as a 3.0 resource, then check to see if a 3.0 version of the resource exists 
					URL resource__3_0 = request.getSession().getServletContext().getResource(DIRECTORY_PATH_3_0.concat(jspXResource));
					
					//If it does then update the uri to use the resource from the 3.0 directory, otherwise continue to use 
					// requested resource
					if (resource__3_0 != null){
						
						uri = DIRECTORY_PATH_3_0.concat(uri);
					
					}			
				}
			}
		}
	
		//Forward the request
		super.doForward(uri, request, response);
	}
	
	protected boolean processTilesDefinition(String definitionName, boolean contextRelative, HttpServletRequest request,
            HttpServletResponse response)
	throws java.io.IOException, javax.servlet.ServletException{

		//Grab the firstlook session to check for the 3.0 upgrade
		FirstlookSession session = getFirstlookSessionFromRequest(request);

		//If no session, then proceed as normal
		if (session !=  null){
		
			try {
				
				//Check to see if this is a tile definition
				ComponentDefinition tileDef = definitionsFactory.getDefinition(definitionName, request, request.getSession().getServletContext());	
				
				//If it is a tile definition then we have some more details to look at
				if (tileDef != null){
					
					//Analyze Path
					String tilePath = tileDef.getPath();	//Get tile path if exists

					//Let's check to see if this path has already been processed (converted to 3.0 version)				
					if (!tilePath.startsWith(DIRECTORY_PATH_3_0) && (tilePath.endsWith(".jsp") || tilePath.endsWith(".jspf")) && session.isIncludeFirstlook_3_0()){
						
						URL resource__3_0 = request.getSession().getServletContext().getResource(DIRECTORY_PATH_3_0.concat(tilePath));
						
						//The 3.0 version of the resource exists, so update the path to reflect that.
						if (resource__3_0 != null){
							
							tileDef.setPath(DIRECTORY_PATH_3_0.concat(tilePath));
						
						}						
					}
					//Otherwise, this else will handle if the dealer has been stripped of the 3.0 upgrade. It should revert to the original resource if it exists.
					else if (tilePath.startsWith(DIRECTORY_PATH_3_0) && !session.isIncludeFirstlook_3_0()){
						
						URL resource__pre3_0 = request.getSession().getServletContext().getResource(tilePath.substring(DIRECTORY_PATH_3_0.length()));
						
						//If there isn't a 3.0 version of the resource, then just use the 3.0 version.
						if (resource__pre3_0 != null){
							
							tileDef.setPath(tilePath.substring(DIRECTORY_PATH_3_0.length()));
						
						}		
					}
					
					//Analyze Page				
					String tilePage = tileDef.getPage();	//Get tile page if exists

					//Let's check to see if this page has already been processed (converted to 3.0 version)				
					if (!tilePage.startsWith(DIRECTORY_PATH_3_0) && (tilePage.endsWith(".jsp") || tilePage.endsWith(".jspf")) && session.isIncludeFirstlook_3_0() ){
						
						URL resource__3_0 = request.getSession().getServletContext().getResource(DIRECTORY_PATH_3_0.concat(tilePage));
						
						//The 3.0 version of the resource exists, so update the page to reflect that.
						if (resource__3_0 != null){
							
							tileDef.setPage(DIRECTORY_PATH_3_0.concat(tilePage));
						
						}						
					}
					//Otherwise, this else will handle if the dealer has been stripped of the 3.0 upgrade. It should revert to the original resource if it exists.					
					else if (tilePage.startsWith(DIRECTORY_PATH_3_0) && !session.isIncludeFirstlook_3_0()){
						
						URL resource__pre3_0 = request.getSession().getServletContext().getResource(tilePage.substring(DIRECTORY_PATH_3_0.length()));
						
						if (resource__pre3_0 != null){
							
							tileDef.setPage(tilePage.substring(DIRECTORY_PATH_3_0.length()));							
						
						}		
					}
					
					//Analyze attributes of tile
					Map<String, String> attributeMap = tileDef.getAttributes();
					
					Set keySet = attributeMap.keySet();
					
					//Are there any attributes?
					if (keySet != null && keySet.size() > 0){
						
						Object[] map = keySet.toArray();

						//If so loop over the keys looking for resource paths that need to be updated
						for (int i = 0; i < map.length; i++){
							
							Object o = attributeMap.get(map[i]);
							
							//We only care about updating resource paths which are reflected as Strings in the map.
							if (o instanceof String){
							
								//Get the value associated key from the map
								String val = o.toString();
								
								//Check if its a jsp or jspf, and if so that it hasn't already been marked as a 3.0 resource
								if (!val.startsWith(DIRECTORY_PATH_3_0) && (val.endsWith(".jsp") || val.endsWith(".jspf")) && session.isIncludeFirstlook_3_0()){
									
									//If it is a jsp / jspf and not defined as a 3.0 resource then check to see if one exists
									URL resource__3_0 = request.getSession().getServletContext().getResource(DIRECTORY_PATH_3_0.concat(val));
									
									if (resource__3_0 != null){
										//If one exists then update the path in the map.
										attributeMap.put(map[i].toString(), DIRECTORY_PATH_3_0.concat(val));	
									}
								}
								//Otherwise, this else will handle if the dealer has been stripped of the 3.0 upgrade. It should revert to the original resource if it exists.
								else if (val.startsWith(DIRECTORY_PATH_3_0) && !session.isIncludeFirstlook_3_0()){
									
									URL resource__pre3_0 = request.getSession().getServletContext().getResource(val.substring(DIRECTORY_PATH_3_0.length()));
									
									if (resource__pre3_0 != null){
										
										attributeMap.put(map[i].toString(), val.substring(DIRECTORY_PATH_3_0.length()));
									
									}
								}								
							}
						}						
					}
				}
			} catch (NoSuchDefinitionException e) {		
				// This is not a ComponentDefinition so proceed as normal.
				return super.processTilesDefinition(definitionName, contextRelative, request, response);
				
			} catch (DefinitionsFactoryException e) {
				
				e.printStackTrace();
			
			}
		}
		
		return super.processTilesDefinition(definitionName, contextRelative, request, response);
	}
	
	protected FirstlookSession getFirstlookSessionFromRequest( HttpServletRequest request )
	{
		if ( request.getSession( false ) == null )			
			return null;

		return (FirstlookSession)request.getSession( false ).getAttribute( FirstlookSession.FIRSTLOOK_SESSION );
	}
}
