package com.firstlook.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;




/**
 * 
 * @author dpatton
 *
 * Servlet used to save the basic auth string into the session. 
 */public class SaveBAStringInSession   extends HttpServlet {
	 static Logger log = Logger.getLogger( SaveBAStringInSession.class );
	/**
	 * 
	 */
	private static final long serialVersionUID = -8583128929355571901L;
	private static final String NAME_IN_SESSION = ".basicAuthString.";

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    log.debug( "########################################################################################################" );
	    log.debug( "We are done!!!!! ");
		
		String basicAuthString = request.getParameter( "basicAuth" );
		
		log.debug( "BasicAuth=" + basicAuthString );
//		HttpSession session = request.getSession();
		
		request.getSession().getServletContext().getContext("/IMT").setAttribute(SaveBAStringInSession.NAME_IN_SESSION, basicAuthString);

		
	//	session.setAttribute(SaveBAStringInSession.NAME_IN_SESSION, basicAuthString );
	    log.debug( "########################################################################################################" );
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)	throws ServletException, IOException {
	    log.debug( "########################################################################################################" );
	    log.debug( "We are done!!!!! ");
		
		String basicAuthString = request.getParameter( "basicAuth" );
		
		log.debug( "BasicAuth=" + basicAuthString );
//		HttpSession session = request.getSession();
		
		request.getSession().getServletContext().getContext("/IMT").setAttribute(SaveBAStringInSession.NAME_IN_SESSION, basicAuthString);

		
	//	session.setAttribute(SaveBAStringInSession.NAME_IN_SESSION, basicAuthString );
	    log.debug( "########################################################################################################" );
	}

}
