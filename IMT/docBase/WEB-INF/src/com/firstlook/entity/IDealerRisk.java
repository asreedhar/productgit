package com.firstlook.entity;

public interface IDealerRisk
{

int getBusinessUnitId();

int getGreenLightDaysPercentage();

double getGreenLightGrossProfitThreshold();

double getGreenLightMarginThreshold();

int getGreenLightNoSaleThreshold();

int getGreenLightTarget();

int getHighMileageThreshold();

int getHighMileagePerYearThreshold();

int getRedLightGrossProfitThreshold();

int getRedLightNoSaleThreshold();

int getRedLightTarget();

int getRiskLevelDealsThreshold();

int getRiskLevelNumberOfContributors();

int getRiskLevelNumberOfWeeks();

int getRiskLevelYearInitialTimePeriod();

int getRiskLevelYearRollOverMonth();

int getRiskLevelYearSecondaryTimePeriod();

int getYellowLightTarget();

int getExcessiveMileageThreshold();

}