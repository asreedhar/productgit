package com.firstlook.entity;

public class NewCarScoreCard extends AbstractCarScoreCard
{

private Integer newCarScoreCardId;

private double agingOver90Current;
private double agingOver90Prior;
private double aging76To90Current;
private double aging76To90Prior;
private double aging61To75Current;
private double aging61To75Prior;
private double aging31To60Current;
private double aging31To60Prior;
private double aging0To30Current;
private double aging0To30Prior;
private Integer coupePercentOfSalesTrend;
private Integer coupePercentOfSales12Week;
private Integer coupeRetailAvgGrossProfitTrend;
private Integer coupeRetailAvgGrossProfit12Week;
private Integer coupeRetailAvgDaysToSaleTrend;
private Integer coupeRetailAvgDaysToSale12Week;
private Integer sedanPercentOfSalesTrend;
private Integer sedanPercentOfSales12Week;
private Integer sedanRetailAvgGrossProfitTrend;
private Integer sedanRetailAvgGrossProfit12Week;
private Integer sedanRetailAvgDaysToSaleTrend;
private Integer sedanRetailAvgDaysToSale12Week;
private Integer suvPercentOfSalesTrend;
private Integer suvPercentOfSales12Week;
private Integer suvRetailAvgGrossProfitTrend;
private Integer suvRetailAvgGrossProfit12Week;
private Integer suvRetailAvgDaysToSaleTrend;
private Integer suvRetailAvgDaysToSale12Week;
private Integer truckPercentOfSalesTrend;
private Integer truckPercentOfSales12Week;
private Integer truckRetailAvgGrossProfitTrend;
private Integer truckRetailAvgGrossProfit12Week;
private Integer truckRetailAvgDaysToSaleTrend;
private Integer truckRetailAvgDaysToSale12Week;
private Integer vanPercentOfSalesTrend;
private Integer vanPercentOfSales12Week;
private Integer vanRetailAvgGrossProfitTrend;
private Integer vanRetailAvgGrossProfit12Week;
private Integer vanRetailAvgDaysToSaleTrend;
private Integer vanRetailAvgDaysToSale12Week;

public NewCarScoreCard()
{
}

public double getAging0To30Current()
{
    return aging0To30Current;
}

public double getAging0To30Prior()
{
    return aging0To30Prior;
}

public double getAging31To60Current()
{
    return aging31To60Current;
}

public double getAging31To60Prior()
{
    return aging31To60Prior;
}

public double getAging61To75Current()
{
    return aging61To75Current;
}

public double getAging61To75Prior()
{
    return aging61To75Prior;
}

public double getAging76To90Current()
{
    return aging76To90Current;
}

public double getAging76To90Prior()
{
    return aging76To90Prior;
}

public double getAgingOver90Current()
{
    return agingOver90Current;
}

public double getAgingOver90Prior()
{
    return agingOver90Prior;
}

public Integer getNewCarScoreCardId()
{
    return newCarScoreCardId;
}

public void setAging0To30Current( double d )
{
    aging0To30Current = d;
}

public void setAging0To30Prior( double d )
{
    aging0To30Prior = d;
}

public void setAging31To60Current( double d )
{
    aging31To60Current = d;
}

public void setAging31To60Prior( double d )
{
    aging31To60Prior = d;
}

public void setAging61To75Current( double d )
{
    aging61To75Current = d;
}

public void setAging61To75Prior( double d )
{
    aging61To75Prior = d;
}

public void setAging76To90Current( double d )
{
    aging76To90Current = d;
}

public void setAging76To90Prior( double d )
{
    aging76To90Prior = d;
}

public void setAgingOver90Current( double d )
{
    agingOver90Current = d;
}

public void setAgingOver90Prior( double d )
{
    agingOver90Prior = d;
}

public void setNewCarScoreCardId( Integer integer )
{
    newCarScoreCardId = integer;
}

public Integer getCoupePercentOfSales12Week()
{
    return coupePercentOfSales12Week;
}

public Integer getCoupePercentOfSalesTrend()
{
    return coupePercentOfSalesTrend;
}

public Integer getCoupeRetailAvgDaysToSale12Week()
{
    return coupeRetailAvgDaysToSale12Week;
}

public Integer getCoupeRetailAvgDaysToSaleTrend()
{
    return coupeRetailAvgDaysToSaleTrend;
}

public Integer getCoupeRetailAvgGrossProfit12Week()
{
    return coupeRetailAvgGrossProfit12Week;
}

public Integer getCoupeRetailAvgGrossProfitTrend()
{
    return coupeRetailAvgGrossProfitTrend;
}

public Integer getSedanPercentOfSales12Week()
{
    return sedanPercentOfSales12Week;
}

public Integer getSedanPercentOfSalesTrend()
{
    return sedanPercentOfSalesTrend;
}

public Integer getSedanRetailAvgDaysToSale12Week()
{
    return sedanRetailAvgDaysToSale12Week;
}

public Integer getSedanRetailAvgDaysToSaleTrend()
{
    return sedanRetailAvgDaysToSaleTrend;
}

public Integer getSedanRetailAvgGrossProfit12Week()
{
    return sedanRetailAvgGrossProfit12Week;
}

public Integer getSedanRetailAvgGrossProfitTrend()
{
    return sedanRetailAvgGrossProfitTrend;
}

public Integer getSuvPercentOfSales12Week()
{
    return suvPercentOfSales12Week;
}

public Integer getSuvPercentOfSalesTrend()
{
    return suvPercentOfSalesTrend;
}

public Integer getSuvRetailAvgDaysToSale12Week()
{
    return suvRetailAvgDaysToSale12Week;
}

public Integer getSuvRetailAvgDaysToSaleTrend()
{
    return suvRetailAvgDaysToSaleTrend;
}

public Integer getSuvRetailAvgGrossProfit12Week()
{
    return suvRetailAvgGrossProfit12Week;
}

public Integer getSuvRetailAvgGrossProfitTrend()
{
    return suvRetailAvgGrossProfitTrend;
}

public Integer getTruckPercentOfSales12Week()
{
    return truckPercentOfSales12Week;
}

public Integer getTruckPercentOfSalesTrend()
{
    return truckPercentOfSalesTrend;
}

public Integer getTruckRetailAvgDaysToSale12Week()
{
    return truckRetailAvgDaysToSale12Week;
}

public Integer getTruckRetailAvgDaysToSaleTrend()
{
    return truckRetailAvgDaysToSaleTrend;
}

public Integer getTruckRetailAvgGrossProfit12Week()
{
    return truckRetailAvgGrossProfit12Week;
}

public Integer getTruckRetailAvgGrossProfitTrend()
{
    return truckRetailAvgGrossProfitTrend;
}

public Integer getVanPercentOfSales12Week()
{
    return vanPercentOfSales12Week;
}

public Integer getVanPercentOfSalesTrend()
{
    return vanPercentOfSalesTrend;
}

public Integer getVanRetailAvgDaysToSale12Week()
{
    return vanRetailAvgDaysToSale12Week;
}

public Integer getVanRetailAvgDaysToSaleTrend()
{
    return vanRetailAvgDaysToSaleTrend;
}

public Integer getVanRetailAvgGrossProfit12Week()
{
    return vanRetailAvgGrossProfit12Week;
}

public Integer getVanRetailAvgGrossProfitTrend()
{
    return vanRetailAvgGrossProfitTrend;
}

public void setCoupePercentOfSales12Week( Integer integer )
{
    coupePercentOfSales12Week = integer;
}

public void setCoupePercentOfSalesTrend( Integer integer )
{
    coupePercentOfSalesTrend = integer;
}

public void setCoupeRetailAvgDaysToSale12Week( Integer integer )
{
    coupeRetailAvgDaysToSale12Week = integer;
}

public void setCoupeRetailAvgDaysToSaleTrend( Integer integer )
{
    coupeRetailAvgDaysToSaleTrend = integer;
}

public void setCoupeRetailAvgGrossProfit12Week( Integer integer )
{
    coupeRetailAvgGrossProfit12Week = integer;
}

public void setCoupeRetailAvgGrossProfitTrend( Integer integer )
{
    coupeRetailAvgGrossProfitTrend = integer;
}

public void setSedanPercentOfSales12Week( Integer integer )
{
    sedanPercentOfSales12Week = integer;
}

public void setSedanPercentOfSalesTrend( Integer integer )
{
    sedanPercentOfSalesTrend = integer;
}

public void setSedanRetailAvgDaysToSale12Week( Integer integer )
{
    sedanRetailAvgDaysToSale12Week = integer;
}

public void setSedanRetailAvgDaysToSaleTrend( Integer integer )
{
    sedanRetailAvgDaysToSaleTrend = integer;
}

public void setSedanRetailAvgGrossProfit12Week( Integer integer )
{
    sedanRetailAvgGrossProfit12Week = integer;
}

public void setSedanRetailAvgGrossProfitTrend( Integer integer )
{
    sedanRetailAvgGrossProfitTrend = integer;
}

public void setSuvPercentOfSales12Week( Integer integer )
{
    suvPercentOfSales12Week = integer;
}

public void setSuvPercentOfSalesTrend( Integer integer )
{
    suvPercentOfSalesTrend = integer;
}

public void setSuvRetailAvgDaysToSale12Week( Integer integer )
{
    suvRetailAvgDaysToSale12Week = integer;
}

public void setSuvRetailAvgDaysToSaleTrend( Integer integer )
{
    suvRetailAvgDaysToSaleTrend = integer;
}

public void setSuvRetailAvgGrossProfit12Week( Integer integer )
{
    suvRetailAvgGrossProfit12Week = integer;
}

public void setSuvRetailAvgGrossProfitTrend( Integer integer )
{
    suvRetailAvgGrossProfitTrend = integer;
}

public void setTruckPercentOfSales12Week( Integer integer )
{
    truckPercentOfSales12Week = integer;
}

public void setTruckPercentOfSalesTrend( Integer integer )
{
    truckPercentOfSalesTrend = integer;
}

public void setTruckRetailAvgDaysToSale12Week( Integer integer )
{
    truckRetailAvgDaysToSale12Week = integer;
}

public void setTruckRetailAvgDaysToSaleTrend( Integer integer )
{
    truckRetailAvgDaysToSaleTrend = integer;
}

public void setTruckRetailAvgGrossProfit12Week( Integer integer )
{
    truckRetailAvgGrossProfit12Week = integer;
}

public void setTruckRetailAvgGrossProfitTrend( Integer integer )
{
    truckRetailAvgGrossProfitTrend = integer;
}

public void setVanPercentOfSales12Week( Integer integer )
{
    vanPercentOfSales12Week = integer;
}

public void setVanPercentOfSalesTrend( Integer integer )
{
    vanPercentOfSalesTrend = integer;
}

public void setVanRetailAvgDaysToSale12Week( Integer integer )
{
    vanRetailAvgDaysToSale12Week = integer;
}

public void setVanRetailAvgDaysToSaleTrend( Integer integer )
{
    vanRetailAvgDaysToSaleTrend = integer;
}

public void setVanRetailAvgGrossProfit12Week( Integer integer )
{
    vanRetailAvgGrossProfit12Week = integer;
}

public void setVanRetailAvgGrossProfitTrend( Integer integer )
{
    vanRetailAvgGrossProfitTrend = integer;
}

}
