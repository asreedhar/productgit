package com.firstlook.entity;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class BusinessUnitRelationship implements Serializable
{

private static final long serialVersionUID = -2048946283024397061L;
private Integer businessUnitId;
private Integer parentId;

public BusinessUnitRelationship()
{
    super();
}

public Integer getBusinessUnitId()
{
    return businessUnitId;
}

public Integer getParentId()
{
    return parentId;
}

public void setBusinessUnitId( Integer i )
{
    businessUnitId = i;
}

public void setParentId( Integer i )
{
    parentId = i;
}

public boolean equals( Object obj )
{
    return EqualsBuilder.reflectionEquals(this, obj);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(13, 5, this);
}

}
