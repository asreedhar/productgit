package com.firstlook.entity;

/**
 * This class represents the dealer preferences for "dealer risk". This is not populated by any automated process, except defaults in the
 * Database.
 */
public class DealerRisk implements IDealerRisk
{

private int businessUnitId;
private int riskLevelNumberOfWeeks;
private int riskLevelDealsThreshold;
private int riskLevelNumberOfContributors;
private int redLightNoSaleThreshold;
private int redLightGrossProfitThreshold;
private int greenLightNoSaleThreshold;
private double greenLightGrossProfitThreshold;
private double greenLightMarginThreshold;
private int greenLightDaysPercentage;
private int riskLevelYearInitialTimePeriod;
private int riskLevelYearSecondaryTimePeriod;
private int riskLevelYearRollOverMonth;
private int redLightTarget;
private int yellowLightTarget;
private int greenLightTarget;
private int excessiveMileageThreshold;
private int highMileageThreshold;
private int highMileagePerYearThreshold;
// new columns
private int highAgeThreshold;
private int excessiveAgeThreshold;
private int excessiveMileagePerYearThreshold;


public DealerRisk()
{
	super();
}

public int getBusinessUnitId()
{
	return businessUnitId;
}

public int getGreenLightDaysPercentage()
{
	return greenLightDaysPercentage;
}

public double getGreenLightGrossProfitThreshold()
{
	return greenLightGrossProfitThreshold;
}

public double getGreenLightMarginThreshold()
{
	return greenLightMarginThreshold;
}

public int getGreenLightNoSaleThreshold()
{
	return greenLightNoSaleThreshold;
}

public int getGreenLightTarget()
{
	return greenLightTarget;
}

public int getRedLightGrossProfitThreshold()
{
	return redLightGrossProfitThreshold;
}

public int getRedLightNoSaleThreshold()
{
	return redLightNoSaleThreshold;
}

public int getRedLightTarget()
{
	return redLightTarget;
}

public int getRiskLevelDealsThreshold()
{
	return riskLevelDealsThreshold;
}

public int getRiskLevelNumberOfContributors()
{
	return riskLevelNumberOfContributors;
}

public int getRiskLevelNumberOfWeeks()
{
	return riskLevelNumberOfWeeks;
}

public int getRiskLevelYearInitialTimePeriod()
{
	return riskLevelYearInitialTimePeriod;
}

public int getRiskLevelYearRollOverMonth()
{
	return riskLevelYearRollOverMonth;
}

public int getRiskLevelYearSecondaryTimePeriod()
{
	return riskLevelYearSecondaryTimePeriod;
}

public int getYellowLightTarget()
{
	return yellowLightTarget;
}

public void setBusinessUnitId( int i )
{
	businessUnitId = i;
}

public void setGreenLightDaysPercentage( int i )
{
	greenLightDaysPercentage = i;
}

public void setGreenLightGrossProfitThreshold( double d )
{
	greenLightGrossProfitThreshold = d;
}

public void setGreenLightMarginThreshold( double d )
{
	greenLightMarginThreshold = d;
}

public void setGreenLightNoSaleThreshold( int i )
{
	greenLightNoSaleThreshold = i;
}

public void setGreenLightTarget( int i )
{
	greenLightTarget = i;
}

public void setRedLightGrossProfitThreshold( int i )
{
	redLightGrossProfitThreshold = i;
}

public void setRedLightNoSaleThreshold( int i )
{
	redLightNoSaleThreshold = i;
}

public void setRedLightTarget( int i )
{
	redLightTarget = i;
}

public void setRiskLevelDealsThreshold( int i )
{
	riskLevelDealsThreshold = i;
}

public void setRiskLevelNumberOfContributors( int i )
{
	riskLevelNumberOfContributors = i;
}

public void setRiskLevelNumberOfWeeks( int i )
{
	riskLevelNumberOfWeeks = i;
}

public void setRiskLevelYearInitialTimePeriod( int i )
{
	riskLevelYearInitialTimePeriod = i;
}

public void setRiskLevelYearRollOverMonth( int i )
{
	riskLevelYearRollOverMonth = i;
}

public void setRiskLevelYearSecondaryTimePeriod( int i )
{
	riskLevelYearSecondaryTimePeriod = i;
}

public void setYellowLightTarget( int i )
{
	yellowLightTarget = i;
}

public int getExcessiveAgeThreshold() {
	return excessiveAgeThreshold;
}

public void setExcessiveAgeThreshold(int excessiveAgeThreshold) {
	this.excessiveAgeThreshold = excessiveAgeThreshold;
}

public int getExcessiveMileagePerYearThreshold() {
	return excessiveMileagePerYearThreshold;
}

public void setExcessiveMileagePerYearThreshold(
		int excessiveMileagePerYearThreshold) {
	this.excessiveMileagePerYearThreshold = excessiveMileagePerYearThreshold;
}

public int getExcessiveMileageThreshold() {
	return excessiveMileageThreshold;
}

public void setExcessiveMileageThreshold(int excessiveMileageThreshold) {
	this.excessiveMileageThreshold = excessiveMileageThreshold;
}

public int getHighAgeThreshold() {
	return highAgeThreshold;
}

public void setHighAgeThreshold(int highAgeThreshold) {
	this.highAgeThreshold = highAgeThreshold;
}

public int getHighMileagePerYearThreshold() {
	return highMileagePerYearThreshold;
}

public void setHighMileagePerYearThreshold(int highMileagePerYearThreshold) {
	this.highMileagePerYearThreshold = highMileagePerYearThreshold;
}

public int getHighMileageThreshold() {
	return highMileageThreshold;
}

public void setHighMileageThreshold(int highMileageThreshold) {
	this.highMileageThreshold = highMileageThreshold;
}

}
