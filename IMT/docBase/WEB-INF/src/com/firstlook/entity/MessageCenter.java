package com.firstlook.entity;

public class MessageCenter
{
private Integer	ordering;
private String	title;
private String	body;
private Boolean hasPriority;

private String	lastUser;
private String	dateCreated;
private String	lastModifiedBy;

	
public MessageCenter()
{
	super();
}

public String getBody()
{
	return body;
}

public void setBody( String body )
{
	this.body = body;
}

public String getDateCreated()
{
	return dateCreated;
}

public void setDateCreated( String dateCreated )
{
	this.dateCreated = dateCreated;
}

public String getLastModifiedBy()
{
	return lastModifiedBy;
}

public void setLastModifiedBy( String lastModifiedBy )
{
	this.lastModifiedBy = lastModifiedBy;
}

public String getLastUser()
{
	return lastUser;
}

public void setLastUser( String lastUser )
{
	this.lastUser = lastUser;
}

public String getTitle()
{
	return title;
}

public void setTitle( String title )
{
	this.title = title;
}

public Boolean getHasPriority()
{
	return hasPriority;
}

public void setHasPriority( Boolean hasPriority )
{
	this.hasPriority = hasPriority;
}

public Integer getOrdering()
{
	return ordering;
}

public void setOrdering( Integer ordering )
{
	this.ordering = ordering;
}

}