package com.firstlook.entity;

import biz.firstlook.commons.gson.GsonExclude;

public class DealerGroupPreference
{

private Integer dealerGroupPreferenceId;
private Integer agingPolicy;
private int agingPolicyAdjustment;
private int inventoryExchangeAgeLimit = 45;
private int pricePointDealsThreshold = 8;
private boolean lithiaStore;
@GsonExclude
private DealerGroup dealerGroup;
private boolean includePerformanceManagementCenter;
private boolean includeTransferPricing;
private String carSearchTitle; 
private Boolean showCarSearch = Boolean.FALSE;
private Boolean excludeWholesaleFromDaysSupply = Boolean.FALSE;

public DealerGroupPreference()
{
    super();
}

public Integer getAgingPolicy()
{
    return agingPolicy;
}

public int getAgingPolicyAdjustment()
{
    return agingPolicyAdjustment;
}

public int getInventoryExchangeAgeLimit()
{
    return inventoryExchangeAgeLimit;
}

public void setAgingPolicy( Integer integer )
{
    agingPolicy = integer;
}

public void setAgingPolicyAdjustment( int i )
{
    agingPolicyAdjustment = i;
}

public void setInventoryExchangeAgeLimit( int i )
{
    inventoryExchangeAgeLimit = i;
}

public Integer getDealerGroupPreferenceId()
{
    return dealerGroupPreferenceId;
}

public void setDealerGroupPreferenceId( Integer i )
{
    dealerGroupPreferenceId = i;
}

public int getPricePointDealsThreshold()
{
    return pricePointDealsThreshold;
}

public void setPricePointDealsThreshold( int i )
{
    pricePointDealsThreshold = i;
}

public DealerGroup getDealerGroup()
{
    return dealerGroup;
}

public void setDealerGroup( DealerGroup group )
{
    dealerGroup = group;
}

public boolean isLithiaStore()
{
	return lithiaStore;
}
public void setLithiaStore( boolean lithiaStore )
{
	this.lithiaStore = lithiaStore;
}

public boolean isIncludePerformanceManagementCenter() {
	return includePerformanceManagementCenter;
}

public void setIncludePerformanceManagementCenter(boolean includePerformanceManagementCenter) {
	this.includePerformanceManagementCenter = includePerformanceManagementCenter;
}

public boolean isIncludeTransferPricing() {
	return includeTransferPricing;
}

public void setIncludeTransferPricing(boolean includeTransferPricing) {
	this.includeTransferPricing = includeTransferPricing;
}

public String getCarSearchTitle()
{
    return carSearchTitle;
}

public void setCarSearchTitle( String carSearchTitle )
{
	this.carSearchTitle = carSearchTitle;
}

public boolean getShowCarSearch()
{
    return showCarSearch;
}

public boolean isShowCarSearch()
{
    return showCarSearch;
}

public void setShowCarSearch( Boolean showCarSearch )
{
	this.showCarSearch = showCarSearch;
}

public boolean getExcludeWholesaleFromDaysSupply()
{
    return excludeWholesaleFromDaysSupply;
}

public boolean isExcludeWholesaleFromDaysSupply()
{
    return excludeWholesaleFromDaysSupply;
}

public void setExcludeWholesaleFromDaysSupply( Boolean excludeWholesaleFromDaysSupply )
{
	this.excludeWholesaleFromDaysSupply = excludeWholesaleFromDaysSupply;
}

}
