package com.firstlook.entity;


public class InventorySalesAggregate
{

private int retailUnitsSold;
private int retailAGP;
private int retailAvgDaysToSale;
private int retailAvgMileage;
private int unitsInStock;
private int noSaleUnits;
private int retailAvgBackEnd;
private int retailTotalBackEnd;
private int retailTotalFrontEnd;
private int retailTotalSalesPrice;
private int retailTotalRevenue;
private int totalInventoryDollars;
private int distinctModelsInStock;
private int avgInventoryAge;
private int daysSupply;
private double sellThroughRate;
private int wholesalePerformance;
private double retailPurchaseProfit;
private double retailTradeProfit;
private int retailPurchaseAGP;
private int retailPurchaseAvgDaysToSale;
private double retailPurchaseSellThrough;
private double retailPurchaseRecommendationsFollowed;
private int retailPurchaseAvgNoSaleLoss;
private int retailTradeAGP;
private int retailTradeAvgDaysToSale;
private double retailTradeSellThrough;
private double retailTradePercentAnalyzed;
private int retailTradeAvgNoSaleLoss;
private int retailTradeAvgFlipLoss;

public int getUnitsInStock()
{
	return unitsInStock;
}
public void setUnitsInStock( int unitsInStock )
{
	this.unitsInStock = unitsInStock;
}
public int getAvgInventoryAge()
{
	return avgInventoryAge;
}
public void setAvgInventoryAge( int avgInventoryAge )
{
	this.avgInventoryAge = avgInventoryAge;
}
public int getDaysSupply()
{
	return daysSupply;
}
public void setDaysSupply( int daysSupply )
{
	this.daysSupply = daysSupply;
}
public int getDistinctModelsInStock()
{
	return distinctModelsInStock;
}
public void setDistinctModelsInStock( int distinctModelsInStock )
{
	this.distinctModelsInStock = distinctModelsInStock;
}
public int getNoSaleUnits()
{
	return noSaleUnits;
}
public void setNoSaleUnits( int noSaleUnits )
{
	this.noSaleUnits = noSaleUnits;
}
public int getRetailAGP()
{
	return retailAGP;
}
public void setRetailAGP( int retailAGP )
{
	this.retailAGP = retailAGP;
}
public int getRetailAvgBackEnd()
{
	return retailAvgBackEnd;
}
public void setRetailAvgBackEnd( int retailAvgBackEnd )
{
	this.retailAvgBackEnd = retailAvgBackEnd;
}
public int getRetailAvgDaysToSale()
{
	return retailAvgDaysToSale;
}
public void setRetailAvgDaysToSale( int retailAvgDaysToSale )
{
	this.retailAvgDaysToSale = retailAvgDaysToSale;
}
public int getRetailAvgMileage()
{
	return retailAvgMileage;
}
public void setRetailAvgMileage( int retailAvgMileage )
{
	this.retailAvgMileage = retailAvgMileage;
}
public int getRetailPurchaseAGP()
{
	return retailPurchaseAGP;
}
public void setRetailPurchaseAGP( int retailPurchaseAGP )
{
	this.retailPurchaseAGP = retailPurchaseAGP;
}
public int getRetailPurchaseAvgDaysToSale()
{
	return retailPurchaseAvgDaysToSale;
}
public void setRetailPurchaseAvgDaysToSale( int retailPurchaseAvgDaysToSale )
{
	this.retailPurchaseAvgDaysToSale = retailPurchaseAvgDaysToSale;
}
public int getRetailPurchaseAvgNoSaleLoss()
{
	return retailPurchaseAvgNoSaleLoss;
}
public void setRetailPurchaseAvgNoSaleLoss( int retailPurchaseAvgNoSaleLoss )
{
	this.retailPurchaseAvgNoSaleLoss = retailPurchaseAvgNoSaleLoss;
}
public double getRetailPurchaseProfit()
{
	return retailPurchaseProfit;
}
public void setRetailPurchaseProfit( double retailPurchaseProfit )
{
	this.retailPurchaseProfit = retailPurchaseProfit;
}
public double getRetailPurchaseRecommendationsFollowed()
{
	return retailPurchaseRecommendationsFollowed;
}
public void setRetailPurchaseRecommendationsFollowed( double retailPurchaseRecommendationsFollowed )
{
	this.retailPurchaseRecommendationsFollowed = retailPurchaseRecommendationsFollowed;
}
public double getRetailPurchaseSellThrough()
{
	return retailPurchaseSellThrough;
}
public void setRetailPurchaseSellThrough( double retailPurchaseSellThrough )
{
	this.retailPurchaseSellThrough = retailPurchaseSellThrough;
}
public int getRetailTotalBackEnd()
{
	return retailTotalBackEnd;
}
public void setRetailTotalBackEnd( int retailTotalBackEnd )
{
	this.retailTotalBackEnd = retailTotalBackEnd;
}
public int getRetailTotalFrontEnd()
{
	return retailTotalFrontEnd;
}
public void setRetailTotalFrontEnd( int retailTotalFrontEnd )
{
	this.retailTotalFrontEnd = retailTotalFrontEnd;
}
public int getRetailTotalRevenue()
{
	return retailTotalRevenue;
}
public void setRetailTotalRevenue( int retailTotalRevenue )
{
	this.retailTotalRevenue = retailTotalRevenue;
}
public int getRetailTotalSalesPrice()
{
	return retailTotalSalesPrice;
}
public void setRetailTotalSalesPrice( int retailTotalSalesPrice )
{
	this.retailTotalSalesPrice = retailTotalSalesPrice;
}
public int getRetailTradeAGP()
{
	return retailTradeAGP;
}
public void setRetailTradeAGP( int retailTradeAGP )
{
	this.retailTradeAGP = retailTradeAGP;
}
public int getRetailTradeAvgDaysToSale()
{
	return retailTradeAvgDaysToSale;
}
public void setRetailTradeAvgDaysToSale( int retailTradeAvgDaysToSale )
{
	this.retailTradeAvgDaysToSale = retailTradeAvgDaysToSale;
}
public int getRetailTradeAvgFlipLoss()
{
	return retailTradeAvgFlipLoss;
}
public void setRetailTradeAvgFlipLoss( int retailTradeAvgFlipLoss )
{
	this.retailTradeAvgFlipLoss = retailTradeAvgFlipLoss;
}
public int getRetailTradeAvgNoSaleLoss()
{
	return retailTradeAvgNoSaleLoss;
}
public void setRetailTradeAvgNoSaleLoss( int retailTradeAvgNoSaleLoss )
{
	this.retailTradeAvgNoSaleLoss = retailTradeAvgNoSaleLoss;
}
public double getRetailTradePercentAnalyzed()
{
	return retailTradePercentAnalyzed;
}
public void setRetailTradePercentAnalyzed( double retailTradePercentAnalyzed )
{
	this.retailTradePercentAnalyzed = retailTradePercentAnalyzed;
}
public double getRetailTradeProfit()
{
	return retailTradeProfit;
}
public void setRetailTradeProfit( double retailTradeProfit )
{
	this.retailTradeProfit = retailTradeProfit;
}
public double getRetailTradeSellThrough()
{
	return retailTradeSellThrough;
}
public void setRetailTradeSellThrough( double retailTradeSellThrough )
{
	this.retailTradeSellThrough = retailTradeSellThrough;
}
public int getRetailUnitsSold()
{
	return retailUnitsSold;
}
public void setRetailUnitsSold( int retailUnitsSold )
{
	this.retailUnitsSold = retailUnitsSold;
}
public double getSellThroughRate()
{
	return sellThroughRate;
}
public void setSellThroughRate( double sellThroughRate )
{
	this.sellThroughRate = sellThroughRate;
}
public int getTotalInventoryDollars()
{
	return totalInventoryDollars;
}
public void setTotalInventoryDollars( int totalInventoryDollars )
{
	this.totalInventoryDollars = totalInventoryDollars;
}
public int getWholesalePerformance()
{
	return wholesalePerformance;
}
public void setWholesalePerformance( int wholesalePerformance )
{
	this.wholesalePerformance = wholesalePerformance;
}

@Override
public int hashCode()
{
	final int PRIME = 31;
	int result = 1;
	result = PRIME * result + avgInventoryAge;
	result = PRIME * result + daysSupply;
	result = PRIME * result + distinctModelsInStock;
	result = PRIME * result + noSaleUnits;
	result = PRIME * result + retailAGP;
	result = PRIME * result + retailAvgBackEnd;
	result = PRIME * result + retailAvgDaysToSale;
	result = PRIME * result + retailAvgMileage;
	result = PRIME * result + retailPurchaseAGP;
	result = PRIME * result + retailPurchaseAvgDaysToSale;
	result = PRIME * result + retailPurchaseAvgNoSaleLoss;
	long temp;
	temp = Double.doubleToLongBits( retailPurchaseProfit );
	result = PRIME * result + (int)( temp ^ ( temp >>> 32 ) );
	temp = Double.doubleToLongBits( retailPurchaseRecommendationsFollowed );
	result = PRIME * result + (int)( temp ^ ( temp >>> 32 ) );
	temp = Double.doubleToLongBits( retailPurchaseSellThrough );
	result = PRIME * result + (int)( temp ^ ( temp >>> 32 ) );
	result = PRIME * result + retailTotalBackEnd;
	result = PRIME * result + retailTotalFrontEnd;
	result = PRIME * result + retailTotalRevenue;
	result = PRIME * result + retailTotalSalesPrice;
	result = PRIME * result + retailTradeAGP;
	result = PRIME * result + retailTradeAvgDaysToSale;
	result = PRIME * result + retailTradeAvgFlipLoss;
	result = PRIME * result + retailTradeAvgNoSaleLoss;
	temp = Double.doubleToLongBits( retailTradePercentAnalyzed );
	result = PRIME * result + (int)( temp ^ ( temp >>> 32 ) );
	temp = Double.doubleToLongBits( retailTradeProfit );
	result = PRIME * result + (int)( temp ^ ( temp >>> 32 ) );
	temp = Double.doubleToLongBits( retailTradeSellThrough );
	result = PRIME * result + (int)( temp ^ ( temp >>> 32 ) );
	result = PRIME * result + retailUnitsSold;
	temp = Double.doubleToLongBits( sellThroughRate );
	result = PRIME * result + (int)( temp ^ ( temp >>> 32 ) );
	result = PRIME * result + totalInventoryDollars;
	result = PRIME * result + unitsInStock;
	result = PRIME * result + wholesalePerformance;
	return result;
}

//Data Transfer Object equals is straight forward!
@Override
public boolean equals( Object obj )
{
	if ( this == obj )
		return true;
	if ( obj == null )
		return false;
	if ( getClass() != obj.getClass() )
		return false;
	final InventorySalesAggregate other = (InventorySalesAggregate)obj;
	if ( avgInventoryAge != other.avgInventoryAge )
		return false;
	if ( daysSupply != other.daysSupply )
		return false;
	if ( distinctModelsInStock != other.distinctModelsInStock )
		return false;
	if ( noSaleUnits != other.noSaleUnits )
		return false;
	if ( retailAGP != other.retailAGP )
		return false;
	if ( retailAvgBackEnd != other.retailAvgBackEnd )
		return false;
	if ( retailAvgDaysToSale != other.retailAvgDaysToSale )
		return false;
	if ( retailAvgMileage != other.retailAvgMileage )
		return false;
	if ( retailPurchaseAGP != other.retailPurchaseAGP )
		return false;
	if ( retailPurchaseAvgDaysToSale != other.retailPurchaseAvgDaysToSale )
		return false;
	if ( retailPurchaseAvgNoSaleLoss != other.retailPurchaseAvgNoSaleLoss )
		return false;
	if ( Double.doubleToLongBits( retailPurchaseProfit ) != Double.doubleToLongBits( other.retailPurchaseProfit ) )
		return false;
	if ( Double.doubleToLongBits( retailPurchaseRecommendationsFollowed ) != Double.doubleToLongBits( other.retailPurchaseRecommendationsFollowed ) )
		return false;
	if ( Double.doubleToLongBits( retailPurchaseSellThrough ) != Double.doubleToLongBits( other.retailPurchaseSellThrough ) )
		return false;
	if ( retailTotalBackEnd != other.retailTotalBackEnd )
		return false;
	if ( retailTotalFrontEnd != other.retailTotalFrontEnd )
		return false;
	if ( retailTotalRevenue != other.retailTotalRevenue )
		return false;
	if ( retailTotalSalesPrice != other.retailTotalSalesPrice )
		return false;
	if ( retailTradeAGP != other.retailTradeAGP )
		return false;
	if ( retailTradeAvgDaysToSale != other.retailTradeAvgDaysToSale )
		return false;
	if ( retailTradeAvgFlipLoss != other.retailTradeAvgFlipLoss )
		return false;
	if ( retailTradeAvgNoSaleLoss != other.retailTradeAvgNoSaleLoss )
		return false;
	if ( Double.doubleToLongBits( retailTradePercentAnalyzed ) != Double.doubleToLongBits( other.retailTradePercentAnalyzed ) )
		return false;
	if ( Double.doubleToLongBits( retailTradeProfit ) != Double.doubleToLongBits( other.retailTradeProfit ) )
		return false;
	if ( Double.doubleToLongBits( retailTradeSellThrough ) != Double.doubleToLongBits( other.retailTradeSellThrough ) )
		return false;
	if ( retailUnitsSold != other.retailUnitsSold )
		return false;
	if ( Double.doubleToLongBits( sellThroughRate ) != Double.doubleToLongBits( other.sellThroughRate ) )
		return false;
	if ( totalInventoryDollars != other.totalInventoryDollars )
		return false;
	if ( unitsInStock != other.unitsInStock )
		return false;
	if ( wholesalePerformance != other.wholesalePerformance )
		return false;
	return true;
}

}