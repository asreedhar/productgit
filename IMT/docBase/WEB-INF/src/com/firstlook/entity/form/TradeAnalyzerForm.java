package com.firstlook.entity.form;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.gson.GsonExclude;
import biz.firstlook.commons.services.vehicleCatalog.VehicleCatalogKey;
import biz.firstlook.commons.util.ColorUtility;
import biz.firstlook.transact.persist.model.DemandDealer;
import biz.firstlook.transact.persist.model.MMRVehicle;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.person.IPerson;
import biz.firstlook.transact.persist.service.appraisal.AppraisalActionType;
import biz.firstlook.transact.persist.service.appraisal.AppraisalTypeEnum;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;

import com.firstlook.display.tradeanalyzer.DisplayGuideBook;
import com.firstlook.util.Formatter;

import biz.firstlook.services.mmr.MMRServiceClient;
import biz.firstlook.services.mmr.entity.MMRArea;
import biz.firstlook.services.mmr.entity.MMRTransactions;
import biz.firstlook.services.mmr.entity.MMRTrim;
import biz.firstlook.services.vehiclehistoryreport.autocheck.AutoCheckReportTO;
import biz.firstlook.services.vehiclehistoryreport.carfax.CarfaxReportTO;

@GsonExclude
public class TradeAnalyzerForm extends VehicleSearchForm {

	private static final long serialVersionUID = -4192451207704027161L;
	// A list of the Catalog Keys
	// The Drop down list on the second page of TA
	// A list of Strings that uniquely identify a vehicle
	@GsonExclude
	private List<VehicleCatalogKey> catalogKeys = new ArrayList<VehicleCatalogKey>();
	private VehicleCatalogKey catalogKey;
	private Integer catalogKeyId;
	private String segment;
	private Integer makeModelGroupingId;
	private Integer groupingDescriptionId;

	private String vin;
	private Integer mileage;
	private Integer appraisalId;
	private AppraisalTypeEnum appraisalType;
	private Double reconditioningCost;
	private Integer targetGrossProfit;
	private String conditionDisclosure;
	private String color;
	private int includeDealerGroup;
	// private int year;
	@GsonExclude
	private Collection<DemandDealer> demandDealers;
	private int actionId;
	private int price;
	private Float transferPrice;
	private boolean transferForRetailOnly = true; // FB:3302 - Per DMG.
	private Integer appraisalValue;
	private int appraisalRequirement;
	private boolean requireNameOnAppraisals;
	private boolean requireEstReconCostOnAppraisals;
	private boolean requireReconNotesOnAppraisals;
	private String appraisalInitials;
	private String appraiserName;
	private String tradeAnalyzerDate;
	@GsonExclude
	private Collection<DisplayGuideBook> displayGuideBooks;
	private boolean highMileageOrOlder;
	private boolean catalogKeyKnown;

	// DealTrack Info - vehicle that dealership is trading 'away'.
	private String dealTrackStockNumber;
	private String dealTrackSalesperson;
	/**
	 * The Person ID as a string in case struts tries to convert the empty
	 * string into an integer value (0).
	 */
	private String dealTrackSalespersonID;
	private Integer dealTrackSalespersonClientID;
	private Integer dealTrackNewOrUsed;
	private List<String> dealTrackSalespeopleDropdownList;
	private Integer buyerId = 0;
	private String buyerName="";

	// Customer info
	private Integer customerId;
	private String customerEmail;
	private String customerFirstName;
	private String customerLastName;
	private String customerGender;
	private String customerPhoneNumber;

	private Double edmundsTMV;
	/**
	 * Used for customer offer on appraisal form
	 */
	private Integer offerEditInputName;

	private boolean smsNotify;
	private String[] managers;
	private int selectedManager;

	// added. -bf.
	private Integer segmentId;

	// added for wireless - nk
	private IAppraisal pendingAppraisal;
	private String notes;

	// added for kbb consumer tool link - jc
	private Boolean hasKBBConsumerTool;

	// added for clientside ping
	private String ownerHandle;
	private String vehicleHandle;

	@GsonExclude
	private List<IPerson> appraisers;
	private Integer selectedAppraiserIndex;

	private TradeAnalyzerContextEnum context = TradeAnalyzerContextEnum.STANDARD;

	// vhr reports
	@GsonExclude
	private CarfaxReportTO carfaxReport;
	@GsonExclude
	private AutoCheckReportTO autocheckReport;

	// MMR
	// private String selectedMmrArea = "NA";
	// private String selectedMmrTrim = "";
	private MMRArea[] mmrAreas = {};
	private MMRTrim[] mmrTrims = {};
	private MMRTransactions mmrTransactions;
	private String mmrErrorMsg = "";
	private MMRVehicle mmrVehicle;

	public TradeAnalyzerForm() {
		super();
		// selectedMmrTrim="-1";
		this.displayGuideBooks = new ArrayList<DisplayGuideBook>();
	}

	public CarfaxReportTO getCarfaxReport() {
		return carfaxReport;
	}

	public void setCarfaxReport(CarfaxReportTO report) {
		carfaxReport = report;
	}

	public AutoCheckReportTO getAutocheckReport() {
		return autocheckReport;
	}

	public void setAutocheckReport(AutoCheckReportTO report) {
		autocheckReport = report;
	}

	public Integer getMileage() {
		if (mileage == null) {
			return new Integer(0);
		}
		return mileage;
	}

	public String getMileageFormatted() {
		return getMileage().toString();
	}

	public String getMileageFormattedWithComma() {
		return Formatter.formatNumberToString(getMileage());
	}

	public void setMileage(Integer newMileage) {
		mileage = newMileage;
	}

	public ActionErrors validate(ActionMapping mapping) {
		ActionErrors errors = new ActionErrors();

		if (!validateMake()) {
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
					"error.dealer.sell.search.inventory"));
		}

		return errors;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public String getConditionDisclosure() {
		return conditionDisclosure;
	}

	public void setConditionDisclosure(String conditionDisclosure) {
		this.conditionDisclosure = conditionDisclosure;
	}

	public Double getReconditioningCost() {
		return reconditioningCost;
	}

	public void setReconditioningCost(Double reconditioningCost) {
		this.reconditioningCost = reconditioningCost;
	}

	public Integer getTargetGrossProfit() {
		return targetGrossProfit;
	}

	public void setTargetGrossProfit(Integer targetGrossProfit) {
		this.targetGrossProfit = targetGrossProfit;
	}

	public VehicleCatalogKey getCatalogKey() {
		return catalogKey;
	}

	/**
	 * This is used by the FE to set the catalog key During TA/TM steps, this
	 * form will have a collection of CatalogKeys During the inital stage of the
	 * vinless Appraisal process, catalogKeys are not present until the
	 * GBSubmitAction
	 */
	public void setCatalogKeyId(Integer catalogKeyId) {
		this.catalogKeyId = catalogKeyId;
		for (VehicleCatalogKey key : catalogKeys) {
			if (key.getVehicleCatalogId().intValue() == catalogKeyId.intValue()) {
				this.catalogKey = key;
			}
		}
	}

	/**
	 * Hacks to get this ID in synch with the objects contained on the form.
	 * 
	 * @return
	 */
	public Integer getCatalogKeyId() {
		if (catalogKey != null) {
			catalogKeyId = catalogKey.getVehicleCatalogId();
		} else if (catalogKeys != null && catalogKeys.size() == 1) {
			VehicleCatalogKey key = catalogKeys.get(0);
			catalogKeyId = key.getVehicleCatalogId();
		}
		return catalogKeyId;
	}

	public void setCatalogKey(VehicleCatalogKey selectedCatalogKey) {
		boolean exists = false;
		// only add a key if its a valid key.
		// Struts will try to add the default 'Please Select a Trim' which has a
		// blank value.
		// This is not a valid selection
		if (selectedCatalogKey != null
				&& selectedCatalogKey.getVehicleCatalogDescription() != null
				&& selectedCatalogKey.getVehicleCatalogDescription().length() > 0) {
			this.catalogKey = selectedCatalogKey;
			for (VehicleCatalogKey key : catalogKeys) {
				if (key.getVehicleCatalogId().intValue() == selectedCatalogKey
						.getVehicleCatalogId().intValue()) {
					exists = true;
					break;
				}
			}
			if (!exists) {
				this.catalogKeys.add(selectedCatalogKey);
			}
		}
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = ColorUtility.formatColor(color);
	}

	public int getIncludeDealerGroup() {
		return includeDealerGroup;
	}

	public String getIncludeDealerGroupStr() {
		return String.valueOf(includeDealerGroup);
	}

	public void setIncludeDealerGroup(int includeDealerGroup) {
		this.includeDealerGroup = includeDealerGroup;
	}

	/*
	 * os - this is available in the parent class public int getYear() { return
	 * year; }
	 * 
	 * public void setYear( int year ) { this.year = year; }
	 */

	public Integer getAppraisalId() {
		return appraisalId;
	}

	public void setAppraisalId(Integer appraisalId) {
		this.appraisalId = appraisalId;
	}

	public Collection<DemandDealer> getDemandDealers() {
		return demandDealers;
	}

	public void setDemandDealers(Collection<DemandDealer> demandDealers) {
		this.demandDealers = demandDealers;
	}

	public int getActionId() {
		return actionId;
	}

	public void setActionId(int actionId) {
		this.actionId = actionId;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Integer getAppraisalValue() {
		return appraisalValue;
	}

	public void setAppraisalValue(Integer appraisalValue) {
		this.appraisalValue = appraisalValue;
	}

	public String getTrimFormatted() {
		if (trim == null || "".equals(trim)) {
			return "N/A";
		} else {
			return trim;
		}
	}

	public String getTradeAnalyzerDate() {
		return tradeAnalyzerDate;
	}

	public void setTradeAnalyzerDate(String tradeAnalyzerDate) {
		this.tradeAnalyzerDate = tradeAnalyzerDate;
	}

	public Collection<DisplayGuideBook> getDisplayGuideBooks() {
		return displayGuideBooks;
	}

	public void setDisplayGuideBooks(
			Collection<DisplayGuideBook> displayGuideBooks) {
		this.displayGuideBooks = displayGuideBooks;
	}

	public void clearGuideBookOptions(int guideBookIndex) {
		int i = 0;
		for (DisplayGuideBook guideBook : displayGuideBooks) {
			if (i == guideBookIndex) {
				guideBook.setSelectedEquipmentOptionKeys(new String[0]);
			}
			i++;
		}
	}

	public void addDisplayGuideBook(DisplayGuideBook displayGuideBook) {
		this.displayGuideBooks.add(displayGuideBook);
	}

	public DisplayGuideBook findDisplayGuideBookByGuideBookId(int guideBookId) {
		if (displayGuideBooks != null) {
			Iterator<DisplayGuideBook> iter = displayGuideBooks.iterator();

			while (iter.hasNext()) {
				DisplayGuideBook displayGuideBook = (DisplayGuideBook) iter
						.next();
				if (displayGuideBook.getGuideBookId() != null
						&& displayGuideBook.getGuideBookId().intValue() == guideBookId) {
					return displayGuideBook;
				}
			}
		}
		return null;
	}

	public void setConditionId(int conditionId) {
		if (displayGuideBooks != null) {
			Iterator<DisplayGuideBook> guideBookIter = displayGuideBooks
					.iterator();
			DisplayGuideBook guideBook;
			while (guideBookIter.hasNext()) {
				guideBook = (DisplayGuideBook) guideBookIter.next();
				if (guideBook.getGuideBookId().intValue() == ThirdPartyDataProvider.GUIDEBOOK_KELLEY_TYPE) {
					guideBook.setConditionId(conditionId);
				}
			}
		}
	}

	public ActionErrors validateMileage(ActionErrors errors) {

		if (!isValidMileage()) {
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
					"error.dealer.tools.vehiclelocatorsearch.mileage"));
		}
		return errors;
	}

	protected boolean isValidMileage() {
		if (!isNullOrEmptyOrWhiteSpace(getMileageFormatted())) {
			try {
				int tempInt = Integer.parseInt(getMileageFormatted());
				if (tempInt < 0) {
					return false;
				} else {
					setMileage(new Integer(tempInt));
				}
			} catch (NumberFormatException nfe) {
				return false;
			}
		}
		return true;
	}

	public ActionErrors validateGuideBook() {
		ActionErrors errors = new ActionErrors();

		errors = validateMileage(errors);
		// errors = validateBodyStyle( errors );
		errors = validateReconditioningCosts(errors);

		return errors;
	}

	private ActionErrors validateReconditioningCosts(ActionErrors errors) {
		if (!isValidReconditioningCosts()) {
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
					"error.dealer.tools.vehiclelocatorsearch.reconcost"));
		}
		return errors;
	}

	boolean isValidReconditioningCosts() {
		try {
			if (reconditioningCost != null) {
				int reconCost = reconditioningCost.intValue();
				if (reconCost < 0) {
					return false;
				}
			}
		} catch (NumberFormatException nfe) {
			return false;
		}

		return true;
	}

	public ActionErrors validateBodyStyle(ActionErrors errors) {
		if (!isValidDescription()) {
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
					"error.dealer.tools.vehiclelocatorsearch.bodystyle"));
		}
		return errors;
	}

	/**
	 * Returns true if the selected description is set
	 * 
	 * @return
	 */
	public boolean isValidDescription() {
		if (catalogKey != null
				&& catalogKey.getVehicleCatalogDescription().length() > 0) {
			return true;
		}
		return false;
	}

	public String getAppraisalInitials() {
		return appraisalInitials;
	}

	public void setAppraisalInitials(String appraisalInitials) {
		this.appraisalInitials = appraisalInitials;
	}

	public boolean isHighMileageOrOlder() {
		return highMileageOrOlder;
	}

	public void setHighMileageOrOlder(boolean highMileageOrOlder) {
		this.highMileageOrOlder = highMileageOrOlder;
	}

	public boolean isCatalogKeyKnown() {
		return catalogKeyKnown;
	}

	public void setCatalogKeyKnown(boolean catalogKeyKnown) {
		this.catalogKeyKnown = catalogKeyKnown;
	}

	public boolean isSmsNotify() {
		return smsNotify;
	}

	public void setSmsNotify(boolean smsNotify) {
		this.smsNotify = smsNotify;
	}

	public String[] getManagers() {
		return managers;
	}

	public void setManagers(String[] managerList) {
		this.managers = managerList;
	}

	public String getDealTrackSalesperson() {
		return dealTrackSalesperson;
	}

	public void setDealTrackSalesperson(String salesPerson) {
		this.dealTrackSalesperson = salesPerson;
	}

	public String getStockNumber() {
		return dealTrackStockNumber;
	}

	public void setStockNumber(String stockNumber) {
		this.dealTrackStockNumber = stockNumber;
	}

	public String getDealTrackStockNumber() {
		return dealTrackStockNumber;
	}

	public void setDealTrackStockNumber(String dealTrackStockNumber) {
		this.dealTrackStockNumber = dealTrackStockNumber;
	}

	public List<String> getDealTrackSalespeopleDropdownList() {

		return dealTrackSalespeopleDropdownList;
	}

	public void setDealTrackSalespeopleDropdownList(
			List<String> dealTrackSalespeopleDropdownList) {
		this.dealTrackSalespeopleDropdownList = dealTrackSalespeopleDropdownList;
	}

	public Integer getDealTrackNewOrUsed() {
		return dealTrackNewOrUsed;
	}

	public void setDealTrackNewOrUsed(Integer dealTrackNewOrUsed) {
		this.dealTrackNewOrUsed = dealTrackNewOrUsed;
	}

	public Integer getSegmentId() {
		return segmentId;
	}

	public void setSegmentId(Integer segmentId) {
		this.segmentId = segmentId;
	}

	public String getCustomerFirstName() {
		return customerFirstName;
	}

	public void setCustomerFirstName(String customerFirstName) {
		this.customerFirstName = customerFirstName;
	}

	public String getCustomerGender() {
		return customerGender;
	}

	public void setCustomerGender(String customerGender) {
		this.customerGender = customerGender;
	}

	public String getCustomerLastName() {
		return customerLastName;
	}

	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}

	public String getCustomerPhoneNumber() {
		return customerPhoneNumber;
	}

	public void setCustomerPhoneNumber(String customerPhoneNumber) {
		this.customerPhoneNumber = customerPhoneNumber;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getDealTrackSalespersonID() {
		return dealTrackSalespersonID;
	}

	public void setDealTrackSalespersonID(String dealTrackSalespersonID) {
		this.dealTrackSalespersonID = dealTrackSalespersonID;
	}

	public void setDealTrackSalesPersonID(Collection<IPerson> people) {
		try {
			int currentPersonId = Integer.parseInt(getDealTrackSalespersonID());
			if (currentPersonId == -1) {
				for (IPerson person : people) {
					if (person.getClientId() != null) {
						if (getDealTrackSalespersonClientID().equals(
								person.getClientId().intValue())) {
							setDealTrackSalespersonID(person.getPersonId()
									.toString());
							break;
						}
					}
				}
			}
		} catch (NumberFormatException nfe) {
			// current id is the empty string
		}
	}

	public Integer getDealTrackSalespersonClientID() {
		return dealTrackSalespersonClientID;
	}

	public void setDealTrackSalespersonClientID(
			Integer dealTrackSalespersonClientID) {
		this.dealTrackSalespersonClientID = dealTrackSalespersonClientID;
	}

	public int getAppraisalRequirement() {
		return appraisalRequirement;
	}

	public void setAppraisalRequirement(int appraisalRequirement) {
		this.appraisalRequirement = appraisalRequirement;
	}

	public boolean isRequireNameOnAppraisals() {
		return requireNameOnAppraisals;
	}

	public void setRequireNameOnAppraisals(boolean requireNameOnAppraisals) {
		this.requireNameOnAppraisals = requireNameOnAppraisals;
	}

	public boolean isRequireEstReconCostOnAppraisals() {
		return requireEstReconCostOnAppraisals;
	}

	public void setRequireEstReconCostOnAppraisals(	boolean requireEstReconCostOnAppraisals ) {
		this.requireEstReconCostOnAppraisals = requireEstReconCostOnAppraisals;
	}

	public boolean isRequireReconNotesOnAppraisals() {
		return requireReconNotesOnAppraisals;
	}

	public void setRequireReconNotesOnAppraisals( boolean requireReconNotesOnAppraisals ) {
		this.requireReconNotesOnAppraisals = requireReconNotesOnAppraisals;
	}

	/**
	 * @return Returns the description.
	 */
	public List<VehicleCatalogKey> getCatalogKeys() {
		return catalogKeys;
	}

	/**
	 * @param catalogKeys
	 *            The description to set.
	 */
	public void setCatalogKeys(List<VehicleCatalogKey> catalogKeys) {
		this.catalogKeys.clear();
		this.catalogKeys.addAll(catalogKeys);
		boolean curKeyExists = false;
		// clear out the holders if they are not in this new set
		if (this.catalogKey != null) {
			for (VehicleCatalogKey key : this.catalogKeys) {
				if (key.getVehicleCatalogId().intValue() == getCatalogKeyId()) {
					curKeyExists = true;
					break;
				}
			}
		}
		if (!curKeyExists) {
			this.catalogKey = null;
			this.catalogKeyId = null;
		}
	}

	/**
	 * @return Returns the segment.
	 */
	public String getSegment() {
		return segment;
	}

	/**
	 * @param segment
	 *            The segment to set.
	 */
	public void setSegment(String segment) {
		this.segment = segment;
	}

	public Integer getMakeModelGroupingId() {
		return makeModelGroupingId;
	}

	public void setMakeModelGroupingId(Integer makeModelGroupingId) {
		this.makeModelGroupingId = makeModelGroupingId;
	}

	public String getAppraiserName() {
		return appraiserName;
	}

	public void setAppraiserName(String appraiserName) {
		this.appraiserName = appraiserName;
	}

	public Integer getOfferEditInputName() {
		return offerEditInputName;
	}

	public void setOfferEditInputName(Integer offerEditInput) {
		if (offerEditInput == 0) {

			offerEditInput = null;
		}
		this.offerEditInputName = offerEditInput;
	}

	public void setPendingAppraisal(IAppraisal appraisal) {
		this.pendingAppraisal = appraisal;
	}

	public IAppraisal getPendingAppraisal() {
		return this.pendingAppraisal;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Boolean getHasKBBConsumerTool() {
		return hasKBBConsumerTool;
	}

	public void setHasKBBConsumerTool(Boolean hasKBBConsumerTool) {
		this.hasKBBConsumerTool = hasKBBConsumerTool;
	}

	public Double getEdmundsTMV() {
		return edmundsTMV;
	}

	public void setEdmundsTMV(Double edmundsTMV) {
		this.edmundsTMV = edmundsTMV;
	}

	public int getSelectedManager() {
		return selectedManager;
	}

	public void setSelectedManager(int selectedManager) {
		this.selectedManager = selectedManager;
	}

	public Integer getGroupingDescriptionId() {
		return groupingDescriptionId;
	}

	public void setGroupingDescriptionId(Integer groupingDescriptionId) {
		this.groupingDescriptionId = groupingDescriptionId;
	}

	public void setAppraisalType(String appraisalType) {
		try {
			this.appraisalType = AppraisalTypeEnum.getType(Integer
					.parseInt(appraisalType));
		} catch (Exception e) {
		}
	}

	public String getAppraisalType() {
		return this.appraisalType.getId().toString();
	}

	public void setAppraisalTypeEnum(AppraisalTypeEnum appraisalType) {
		this.appraisalType = appraisalType;
	}

	public AppraisalTypeEnum getAppraisalTypeEnum() {
		return this.appraisalType;
	}

	public Integer getBuyerId() {
		return buyerId;
	}

	public void setBuyerId(Integer buyerId) {
		this.buyerId = buyerId;
	}

	public String getOwnerHandle() {
		return ownerHandle;
	}

	public void setOwnerHandle(String ownerHandle) {
		this.ownerHandle = ownerHandle;
	}

	public String getVehicleHandle() {
		return vehicleHandle;
	}

	public void setVehicleHandle(String vehicleHandle) {
		this.vehicleHandle = vehicleHandle;
	}

	public List<IPerson> getAppraisers() {
		return appraisers;
	}

	public void setAppraisers(List<IPerson> appraiserList) {
		this.appraisers = appraiserList;
	}

	public Integer getSelectedAppraiserIndex() {
		return this.selectedAppraiserIndex;
	}

	public void setSelectedAppraiserIndex(Integer selectedAppraiserIndex) {
		this.selectedAppraiserIndex = selectedAppraiserIndex;
	}

	public String getContext() {
		return context.getDescription();
	}

	public void setContext(String context) {
		TradeAnalyzerContextEnum newContext = TradeAnalyzerContextEnum
				.get(context);
		if (newContext != null) {
			this.context = newContext;
		}
	}

	public TradeAnalyzerContextEnum getContextEnum() {
		return context;
	}

	public void setContextEnum(TradeAnalyzerContextEnum context) {
		this.context = context;
	}

	public Float getTransferPrice() {
		return transferPrice;
	}

	public void setTransferPrice(Float transferPrice) {
		this.transferPrice = transferPrice;
	}

	public List<AppraisalActionType> getAppraisalActionTypes() {

		List<AppraisalActionType> result = new ArrayList<AppraisalActionType>();

		result.add(AppraisalActionType.DECIDE_LATER);
		result.add(AppraisalActionType.PLACE_IN_RETAIL);

		if (AppraisalTypeEnum.TRADE_IN.equals(appraisalType)) {
			result.add(AppraisalActionType.NOT_TRADED_IN);
			result.add(AppraisalActionType.OFFER_TO_GROUP);
			result.add(AppraisalActionType.WHOLESALE_OR_AUCTION);
		}
		return result;
	}

	/**
	 * Currently used to dennote an 'Appraisal that is being driven from the
	 * s&a.'. These are special appraisals that a) default to an Appraisal Type
	 * of PURCHASE and b) are done in a pop up workflow which should close the
	 * window after clicking 'done' on the last page of the TA.
	 * 
	 * In the future perhaps this context can be renamed to something like
	 * 'popup' workflow, or maybe 'coconut'
	 * 
	 * @author nkeen
	 * 
	 */
	public enum TradeAnalyzerContextEnum {

		STANDARD(1, "standard"), SEARCH_AQUISITION(1, "search");

		private Integer id;
		private String description;

		private TradeAnalyzerContextEnum(Integer id, String description) {
			this.id = id;
			this.description = description;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public static TradeAnalyzerContextEnum get(String description) {
			if (SEARCH_AQUISITION.getDescription()
					.equalsIgnoreCase(description)) {
				return SEARCH_AQUISITION;
			}
			return STANDARD;
		}
	}

	public boolean isTransferForRetailOnly() {
		return transferForRetailOnly;
	}

	public void setTransferForRetailOnly(boolean transferForRetailOnly) {
		this.transferForRetailOnly = transferForRetailOnly;
	}

	public MMRArea[] getMmrAreas() {
		return mmrAreas;
	}

	public MMRTrim[] getMmrTrims() {
		return mmrTrims;
	}

	public MMRTransactions getMmrTransactions() {
		return mmrTransactions;
	}

	public void populateMMRAreas(String basicAuthString) {
		if (mmrAreas.length < 1) {
			try {
				MMRServiceClient mmrServiceClient = new MMRServiceClient(
						basicAuthString);
				mmrAreas = mmrServiceClient.getMMRReference();
			} catch (Exception e) {
				// Exception already logged. Just keep whatever the last area
				// list was (or empty list).
			}
		}
	}

	public void populateMMRTrims(String basicAuthString) {
		try {
			MMRServiceClient mmrServiceClient = new MMRServiceClient(
					basicAuthString);
			MMRVehicle mmrVeh = getSelectedMmrVehicle();
			String area = mmrVeh == null ? "" : mmrVeh.getRegionCode();
			mmrTrims = mmrServiceClient.getMMRTrims(vin, area);

			if (mmrTrims.length == 1) {
				if (mmrVeh == null) {
					mmrVeh = new MMRVehicle(mmrTrims[0].getTrimId(), area);
				}
				mmrVeh.setMid(mmrTrims[0].getTrimId());
				// selectedMmrTrim = mmrTrims[0].getTrimId();
				setSelectedMmrVehicle(mmrVeh);
			}
		} catch (Exception e) {
			setMmrErrorMsg(e.getMessage());
			// Exception already logged. Just return whatever the last trim list
			// was (or empty list).
		}
	}

	public void populateMMRTransactions(String basicAuthString) {
		try {
			MMRVehicle mmrVeh = getSelectedMmrVehicle();
			MMRTrim mmrTrim = getMmrTrimSelected();
			if (mmrTrim != null) {
				String area = mmrVeh == null ? "" : mmrVeh.getRegionCode();
				MMRServiceClient mmrServiceClient = new MMRServiceClient(
						basicAuthString);
				mmrTransactions = mmrServiceClient.getMMRTransactions(mmrTrim
						.getTrimId(), mmrTrim.getYear(), area);
			}
		} catch (Exception e) {
			// Set the mmrTransactions back to null, since there aren't any.
			mmrTransactions = null;
			setMmrErrorMsg(e.getMessage());
			// Exception already logged. Just return whatever the last
			// transactions were (or empty list).
		}
	}

	public MMRVehicle getSelectedMmrVehicle() {
		return mmrVehicle;
	}

	public void setSelectedMmrVehicle(MMRVehicle mmrVehicle) {
		if (this.mmrVehicle != null) {
			if (mmrVehicle != null) {
				/*MMRVehicle tmpvehMmrVehicle= this.mmrVehicle;
				this.mmrVehicle= new MMRVehicle();
				this.mmrVehicle.setAveragePrice((tmpvehMmrVehicle.getAveragePrice()));
				this.mmrVehicle.setDateUpdated(tmpvehMmrVehicle.getDateUpdated());
				this.mmrVehicle.setMmrVehicleId((tmpvehMmrVehicle.getMmrVehicleId()));*/
				this.mmrVehicle.setMid(mmrVehicle.getMid());
				this.mmrVehicle.setRegionCode(mmrVehicle.getRegionCode());
			} else {
				this.mmrVehicle = null;
			}
		} else
			this.mmrVehicle = mmrVehicle;
	}

	public String getSelectedMmrArea() {
		MMRVehicle mv = getSelectedMmrVehicle();
		return mv != null ? mv.getRegionCode() : null;
	}

	public String getSelectedMmrTrim() {
		MMRVehicle mv = getSelectedMmrVehicle();
		return mv != null ? mv.getMid() : null;
	}

	public void setSelectedMmrArea(String s) {
		MMRVehicle mv = getSelectedMmrVehicle();
		mv = (mv==null) ? new MMRVehicle("-1", s) : mv;
		mv.setRegionCode(s);
		setSelectedMmrVehicle(mv);
	}
	public void setSelectedMmrTrim(String s) {
		MMRVehicle mv = getSelectedMmrVehicle();
		mv = (mv==null) ? new MMRVehicle(s, null) : mv;
		mv.setMid(s);
		setSelectedMmrVehicle(mv);
	}

	public MMRTrim getMmrTrimSelected() {
		if (this.getSelectedMmrVehicle() != null) {
			MMRVehicle mmrVeh = this.getSelectedMmrVehicle();
			for (MMRTrim trim : mmrTrims) {
				if (trim.getTrimId().equals(mmrVeh.getMid())) {
					return trim;
				}
			}
		}
		return null;
	}

	public void setMmrErrorMsg(String mmrErrorMsg) {
		this.mmrErrorMsg = mmrErrorMsg;
	}

	public String getMmrErrorMsg() {
		return mmrErrorMsg;
	}

	/**
	 * This form must be reset out when coming from Search and Aquisition
	 * (a.k.a. FLUSAN, a.k.a. Search Engine) upon entry to the
	 * GuideBookDetailSubmitAction (as per FB#4463). If this is not done we see
	 * the data from the previous appraisal.
	 */
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		if (mapping.getPath().contains("GuideBookDetailSubmitAction")) {
			String context = request.getParameter("context");
			if ("search".equals(context)) {
				// group one
				if (catalogKeys != null) {
					catalogKeys.clear();
				}
				catalogKey = null;
				catalogKeyId = null;
				segment = null;
				makeModelGroupingId = null;
				groupingDescriptionId = null;
				// group two
				vin = null;
				mileage = null;
				appraisalId = null;
				appraisalType = null;
				reconditioningCost = null;
				conditionDisclosure = null;
				color = null;
				includeDealerGroup = 0;
				year = 0;
				if (demandDealers != null) {
					demandDealers.clear();
				}
				actionId = 0;
				price = 0;
				transferPrice = null;
				transferForRetailOnly = true; // FB:3302 - Per DMG.
				appraisalValue = null;
				appraisalRequirement = 0;
				requireNameOnAppraisals = false;
				requireEstReconCostOnAppraisals = false;
				requireReconNotesOnAppraisals = false;
				appraisalInitials = null;
				appraiserName = null;
				tradeAnalyzerDate = null;
				if (displayGuideBooks != null) {
					displayGuideBooks.clear();
				}
				highMileageOrOlder = false;
				catalogKeyKnown = false;
				// group three
				dealTrackStockNumber = null;
				dealTrackSalesperson = null;
				dealTrackSalespersonID = null;
				dealTrackSalespersonClientID = null;
				dealTrackNewOrUsed = null;
				if (dealTrackSalespeopleDropdownList != null) {
					dealTrackSalespeopleDropdownList.clear();
				}
				buyerName="";
				buyerId = 0;
				// group four
				customerId = 0;
				customerEmail = null;
				customerFirstName = null;
				customerLastName = null;
				customerGender = null;
				customerPhoneNumber = null;
				// group five
				edmundsTMV = null;
				// group six
				offerEditInputName = null;
				smsNotify = false;
				managers = null;
				selectedManager = 0;
				segmentId = null;
				// group seven
				pendingAppraisal = null;
				notes = null;
				ownerHandle = null;
				vehicleHandle = null;
				context = TradeAnalyzerContextEnum.STANDARD.getDescription();

				mmrAreas = new MMRArea[] {};
				mmrTrims = new MMRTrim[] {};
				mmrTransactions = null;
				// selectedMmrArea = "NA";
				// selectedMmrTrim = "";
			}
		}
		super.reset(mapping, request);
	}

	public String getBuyerName() {
		return buyerName;
	}

	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}

}
