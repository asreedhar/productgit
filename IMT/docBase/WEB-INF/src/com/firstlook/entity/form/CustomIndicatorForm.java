package com.firstlook.entity.form;

import com.firstlook.report.CustomIndicator;

/**
 * Insert the type's description here. Creation date: (2/1/2002 11:31:13 AM)
 * 
 * @author: Extreme Developer
 */
public class CustomIndicatorForm extends com.firstlook.action.BaseActionForm
{
private static final long serialVersionUID = -9106638094877588737L;
private CustomIndicator indicator;
private java.lang.String groupingDescription;

/**
 * CustomIndicatorForm constructor comment.
 */
public CustomIndicatorForm()
{
    super();
}

/**
 * CustomIndicatorForm constructor comment.
 */
public CustomIndicatorForm( CustomIndicator newIndicator )
{
    super();
    indicator = newIndicator;
}

public int getFastestSellerRank()
{

    return indicator.getFastestSellerRank(groupingDescription);
}

/**
 * Insert the method's description here. Creation date: (2/1/2002 11:36:22 AM)
 * 
 * @return java.lang.String
 */
public java.lang.String getGroupingDescription()
{
    return groupingDescription;
}

/**
 * Insert the method's description here. Creation date: (2/1/2002 11:34:48 AM)
 * 
 * @return com.firstlook.report.GroupRanking
 */
public com.firstlook.report.CustomIndicator getIndicator()
{
    return indicator;
}

public int getMostProfitableSellerRank()
{

    return indicator.getMostProfitableSellerRank(groupingDescription);
}

public int getTopSellerRank()
{

    return indicator.getTopSellerRank(groupingDescription);
}

public boolean isGroupRanked()
{
    return indicator.isGroupRanked(groupingDescription);
}

public boolean isInHotlist()
{
    return indicator.isInHotlist(groupingDescription);
}

/**
 * Insert the method's description here. Creation date: (2/1/2002 11:36:22 AM)
 * 
 * @param newGroupingDescription
 *            java.lang.String
 */
public void setGroupingDescription( java.lang.String newGroupingDescription )
{
    groupingDescription = newGroupingDescription;
}

/**
 * Insert the method's description here. Creation date: (2/1/2002 11:34:48 AM)
 * 
 * @param newIndicator
 *            com.firstlook.report.GroupRanking
 */
public void setIndicator( com.firstlook.report.CustomIndicator newIndicator )
{
    indicator = newIndicator;
}
}
