package com.firstlook.entity.form;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import biz.firstlook.transact.persist.service.appraisal.AppraisalValue;

import com.firstlook.action.BaseActionForm;
import com.firstlook.util.Formatter;

public class AppraisalBumpForm extends BaseActionForm
{

private static final long serialVersionUID = -8261989092159297487L;

private AppraisalValue vehicleGuideBookBump;

private int appraisalId;
private double bumpAmount;
private String memberName;

public AppraisalBumpForm()
{
	super();
}

public AppraisalBumpForm( AppraisalValue businessObject )
{
	
	setBusinessObject( businessObject );
	memberName = businessObject.getMemberName();	
	appraisalId = businessObject.getAppraisal().getAppraisalId();
	bumpAmount = businessObject.getValue();
}

public void setBusinessObject( Object businessObject )
{
	vehicleGuideBookBump = (AppraisalValue)businessObject;
	memberName = vehicleGuideBookBump.getMemberName();
	appraisalId = vehicleGuideBookBump.getAppraisal().getAppraisalId();
	bumpAmount = vehicleGuideBookBump.getValue();
}

public AppraisalValue getVehicleGuideBookBump()
{
	return vehicleGuideBookBump;
}

public double getBump()
{
	return bumpAmount;
}

public int getVehicleGuideBookBumpId()
{
	throw new NotImplementedException();
}

public int getAppraisalId()
{
	return appraisalId;
}

public void setBump( double bump )
{
	this.bumpAmount = bump;
}

public void setAppraisalBumpId( int appraisalBumpId )
{
	throw new NotImplementedException();
}

public void setAppraisalId( int appraisalId )
{
	this.appraisalId = appraisalId;
}

public String getBumpFormatted()
{
	return Formatter.toTwoDecimalPrice( getBump() );
}

public String getAppraisalInitials()
{
	return vehicleGuideBookBump.getAppraiserName();
}

public String getAppraiserName()
{
	return vehicleGuideBookBump.getAppraiserName();
}
public String getMemberName(){
	return vehicleGuideBookBump.getMemberName();
}
public void setMemberName(String memberName){
	
}

public String getDateCreated()
{
	if(vehicleGuideBookBump == null)
		return "no date";
	
	if(vehicleGuideBookBump.getDateCreated() == null)
		return "no date";
	
	return vehicleGuideBookBump.getDateCreated().toLocaleString();
}

}
