package com.firstlook.entity.form;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AllTests extends TestCase
{

public AllTests( String name )
{
    super(name);
}

public static Test suite()
{
    TestSuite suite = new TestSuite();
    suite.addTest(new TestSuite(TestCorporationForm.class));
    suite.addTest(new TestSuite(TestDealerDemandForm.class));
    suite.addTest(new TestSuite(TestDealerForm.class));
    suite.addTest(new TestSuite(TestDealerUpgradeFormFactory.class));
    suite.addTest(new TestSuite(TestDealerGroupForm.class));
    suite.addTest(new TestSuite(TestDealerUpgradeForm.class));
    suite.addTest(new TestSuite(TestDisplayValueForm.class));
    suite.addTest(new TestSuite(TestExchangeToPlusForm.class));
    suite.addTest(new TestSuite(TestGuideBookValueForm.class));
    suite.addTest(new TestSuite(TestGuideBookVehicleForm.class));
    suite.addTest(new TestSuite(TestPhoneNumberFormHelper.class));
    suite.addTest(new TestSuite(TestSaleForm.class));
    suite.addTest(new TestSuite(TestSelectVehiclesForm.class));
    suite.addTest(new TestSuite(TestTargetSupplySearchForm.class));
    suite.addTest(new TestSuite(TestUCBPPreferencesForm.class));
    suite.addTest(new TestSuite(TestVehicleForm.class));
    suite.addTest(new TestSuite(TestVehicleOptionsForm.class));

    return suite;
}
}
