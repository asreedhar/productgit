package com.firstlook.entity.form;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import junit.framework.TestCase;

import com.firstlook.entity.DealerUpgrade;
import com.firstlook.entity.DealerUpgradeLookup;

public class TestDealerUpgradeFormFactory extends TestCase
{

private int dealerId = 1;

public TestDealerUpgradeFormFactory( String arg0 )
{
    super(arg0);
}

public void testCreateDealerUpgradeFormAging()
{
    Collection dealerUpgrades = new ArrayList();
    DealerUpgrade dealerUpgrade = new DealerUpgrade();
    dealerUpgrade.setDealerUpgradeCode(DealerUpgradeLookup.AGING_PLAN_CODE);
    dealerUpgrade.setEndDate(new Date());
    dealerUpgrade.setStartDate(new Date());
    dealerUpgrade.setActive(true);
    dealerUpgrade.setDealerId(dealerId);
    dealerUpgrades.add(dealerUpgrade);

    DealerUpgradeForm dealerUpgradeForm = DealerUpgradeFormFactory
            .createDealerUpgradeForm(dealerUpgrades);

    assertEquals(dealerUpgrade.isActive(), dealerUpgradeForm
            .isIncludeAgingPlan());
    assertEquals(DealerUpgradeFormFactory.FORMATTER.format(dealerUpgrade
            .getEndDate()), dealerUpgradeForm.getAgingPlanEndDate());
    assertEquals(DealerUpgradeFormFactory.FORMATTER.format(dealerUpgrade
            .getStartDate()), dealerUpgradeForm.getAgingPlanStartDate());
}

public void testCreateDealerUpgradeFormCIA()
{
    Collection dealerUpgrades = new ArrayList();
    DealerUpgrade dealerUpgrade = new DealerUpgrade();
    dealerUpgrade.setDealerUpgradeCode(DealerUpgradeLookup.CIA_CODE);
    dealerUpgrade.setEndDate(new Date());
    dealerUpgrade.setStartDate(new Date());
    dealerUpgrade.setActive(true);
    dealerUpgrade.setDealerId(dealerId);
    dealerUpgrades.add(dealerUpgrade);

    DealerUpgradeForm dealerUpgradeForm = DealerUpgradeFormFactory
            .createDealerUpgradeForm(dealerUpgrades);

    assertEquals(dealerUpgrade.isActive(), dealerUpgradeForm.isIncludeCIA());
    assertEquals(DealerUpgradeFormFactory.FORMATTER.format(dealerUpgrade
            .getEndDate()), dealerUpgradeForm.getCiaEndDate());
    assertEquals(DealerUpgradeFormFactory.FORMATTER.format(dealerUpgrade
            .getStartDate()), dealerUpgradeForm.getCiaStartDate());
}

public void testCreateDealerUpgradeFormTradeAnalyzer()
{
    Collection dealerUpgrades = new ArrayList();
    DealerUpgrade dealerUpgrade = new DealerUpgrade();
    dealerUpgrade.setDealerUpgradeCode(DealerUpgradeLookup.APPRAISAL_CODE);
    dealerUpgrade.setEndDate(new Date());
    dealerUpgrade.setStartDate(new Date());
    dealerUpgrade.setActive(true);
    dealerUpgrade.setDealerId(dealerId);
    dealerUpgrades.add(dealerUpgrade);

    DealerUpgradeForm dealerUpgradeForm = DealerUpgradeFormFactory
            .createDealerUpgradeForm(dealerUpgrades);

    assertEquals(dealerUpgrade.isActive(), dealerUpgradeForm
            .isIncludeAppraisal());
    assertEquals(DealerUpgradeFormFactory.FORMATTER.format(dealerUpgrade
            .getEndDate()), dealerUpgradeForm.getAppraisalEndDate());
    assertEquals(DealerUpgradeFormFactory.FORMATTER.format(dealerUpgrade
            .getStartDate()), dealerUpgradeForm.getAppraisalStartDate());
}

public void testCreateDealerUpgradeFormRedistribution()
{
    Collection dealerUpgrades = new ArrayList();
    DealerUpgrade dealerUpgrade = new DealerUpgrade();
    dealerUpgrade.setDealerUpgradeCode(DealerUpgradeLookup.REDISTRIBUTION_CODE);
    dealerUpgrade.setEndDate(new Date());
    dealerUpgrade.setStartDate(new Date());
    dealerUpgrade.setActive(true);
    dealerUpgrade.setDealerId(dealerId);
    dealerUpgrades.add(dealerUpgrade);

    DealerUpgradeForm dealerUpgradeForm = DealerUpgradeFormFactory
            .createDealerUpgradeForm(dealerUpgrades);

    assertEquals(dealerUpgrade.isActive(), dealerUpgradeForm
            .isIncludeRedistribution());
    assertEquals(DealerUpgradeFormFactory.FORMATTER.format(dealerUpgrade
            .getEndDate()), dealerUpgradeForm.getRedistributionEndDate());
    assertEquals(DealerUpgradeFormFactory.FORMATTER.format(dealerUpgrade
            .getStartDate()), dealerUpgradeForm.getRedistributionStartDate());
}

public void testCreateDealerUpgradeFormAuctionData()
{
    Collection dealerUpgrades = new ArrayList();
    DealerUpgrade dealerUpgrade = new DealerUpgrade();
    dealerUpgrade.setDealerUpgradeCode(DealerUpgradeLookup.AUCTION_DATA_CODE);
    dealerUpgrade.setEndDate(new Date());
    dealerUpgrade.setStartDate(new Date());
    dealerUpgrade.setActive(true);
    dealerUpgrade.setDealerId(dealerId);
    dealerUpgrades.add(dealerUpgrade);

    DealerUpgradeForm dealerUpgradeForm = DealerUpgradeFormFactory
            .createDealerUpgradeForm(dealerUpgrades);

    assertEquals(dealerUpgrade.isActive(), dealerUpgradeForm
            .isIncludeAuctionData());
    assertEquals(DealerUpgradeFormFactory.FORMATTER.format(dealerUpgrade
            .getEndDate()), dealerUpgradeForm.getAuctionDataEndDate());
    assertEquals(DealerUpgradeFormFactory.FORMATTER.format(dealerUpgrade
            .getStartDate()), dealerUpgradeForm.getAuctionDataStartDate());
}

public void testCreateDealerUpgradeFormMultiple()
{
    Collection dealerUpgrades = new ArrayList();
    DealerUpgrade dealerUpgrade = new DealerUpgrade();
    dealerUpgrade.setDealerUpgradeCode(DealerUpgradeLookup.AGING_PLAN_CODE);
    dealerUpgrade.setEndDate(new Date());
    dealerUpgrade.setStartDate(new Date());
    dealerUpgrade.setActive(true);
    dealerUpgrade.setDealerId(dealerId);

    DealerUpgrade dealerUpgrade2 = new DealerUpgrade();
    dealerUpgrade2
            .setDealerUpgradeCode(DealerUpgradeLookup.REDISTRIBUTION_CODE);
    dealerUpgrade2.setEndDate(new Date());
    dealerUpgrade2.setStartDate(new Date());
    dealerUpgrade2.setActive(false);
    dealerUpgrade2.setDealerId(dealerId);

    dealerUpgrades.add(dealerUpgrade);
    dealerUpgrades.add(dealerUpgrade2);

    DealerUpgradeForm dealerUpgradeForm = DealerUpgradeFormFactory
            .createDealerUpgradeForm(dealerUpgrades);

    assertEquals(dealerUpgrade2.isActive(), dealerUpgradeForm
            .isIncludeRedistribution());
    assertEquals(DealerUpgradeFormFactory.FORMATTER.format(dealerUpgrade2
            .getEndDate()), dealerUpgradeForm.getRedistributionEndDate());
    assertEquals(DealerUpgradeFormFactory.FORMATTER.format(dealerUpgrade2
            .getStartDate()), dealerUpgradeForm.getRedistributionStartDate());

    assertEquals(dealerUpgrade.isActive(), dealerUpgradeForm
            .isIncludeAgingPlan());
    assertEquals(DealerUpgradeFormFactory.FORMATTER.format(dealerUpgrade
            .getEndDate()), dealerUpgradeForm.getAgingPlanEndDate());
    assertEquals(DealerUpgradeFormFactory.FORMATTER.format(dealerUpgrade
            .getStartDate()), dealerUpgradeForm.getAgingPlanStartDate());
}

public void testIsValid()
{
    assertTrue(DealerUpgradeFormFactory.isValid(new Date()));
}

public void testIsValidNull()
{
    assertTrue(!DealerUpgradeFormFactory.isValid(null));
}

public void testIsValidString()
{
    assertTrue(DealerUpgradeFormFactory.isValidString("String"));
}

public void testIsValidStringNull()
{
    assertTrue(!DealerUpgradeFormFactory.isValidString(null));
}

public void testIsValidStringEmpty()
{
    assertTrue(!DealerUpgradeFormFactory.isValidString(""));
}

}
