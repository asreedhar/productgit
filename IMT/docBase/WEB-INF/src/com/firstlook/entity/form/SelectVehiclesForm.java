package com.firstlook.entity.form;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import biz.firstlook.commons.gson.GsonExclude;

import com.firstlook.entity.InventoryEntity;
import com.firstlook.exception.ApplicationException;

@GsonExclude
public class SelectVehiclesForm extends com.firstlook.action.BaseActionForm
{
private static final long serialVersionUID = 5287255624386846267L;
private int[] vehicleIdArray;
private int[] selectedVehicleIdArray;

public SelectVehiclesForm()
{
    super();
}

public SelectVehiclesForm( String[] sVehicleIds )
{
    super();

    if ( sVehicleIds != null )
    {
        vehicleIdArray = new int[sVehicleIds.length];

        for (int i = 0; i < sVehicleIds.length; i++)
        {
            vehicleIdArray[i] = Integer.parseInt(sVehicleIds[i]);
        }
    } else
    {
        vehicleIdArray = new int[0];
    }
}

public List<Integer> getModifiedVehicleIds( int[] candidateIds, int[] idsToReject )
{
    ArrayList<Integer> modifiedVehicleIds = new ArrayList<Integer>();

    if ( idsToReject != null )
    {
        Arrays.sort(idsToReject);
    }

    if ( candidateIds != null )
    {
        for (int i = 0; i < candidateIds.length; i++)
        {
            if ( (idsToReject == null)
                    || (Arrays.binarySearch(idsToReject, candidateIds[i]) < 0) )
            {
                modifiedVehicleIds.add(new Integer(candidateIds[i]));
            }
        }
    }
    return modifiedVehicleIds;
}

public List getNewlyCheckedVehicleIds()
{
    return getModifiedVehicleIds(vehicleIdArray, selectedVehicleIdArray);
}

public List getNewlyUncheckedVehicleIds()
{
    return getModifiedVehicleIds(selectedVehicleIdArray, vehicleIdArray);
}

/**
 * 
 * @return int[]
 */
public int[] getSelectedVehicleIdArray()
{
    return selectedVehicleIdArray;
}

/**
 * Insert the method's description here. Creation date: (12/13/2001 3:31:16 PM)
 * 
 * @return int[]
 */
public int[] getVehicleIdArray()
{
    return vehicleIdArray;
}

/**
 * 
 * @param newSelectedVehicleIdArray
 *            int[]
 */
public void setSelectedVehicleIdArray( int[] newSelectedVehicleIdArray )
{
    selectedVehicleIdArray = newSelectedVehicleIdArray;
}

/**
 * Insert the method's description here. Creation date: (12/13/2001 3:31:16 PM)
 * 
 * @param newVehicleIdArray
 *            int[]
 */
public void setVehicleIdArray( int[] newVehicleIdArray )
{
    vehicleIdArray = newVehicleIdArray;
}

public void setVehicleIdArray( Collection vehicles, String isCheckedMethodName )
        throws ApplicationException
{
    Iterator iterator = vehicles.iterator();
    ArrayList<Integer> vehicleIds = new ArrayList<Integer>();

    while (iterator.hasNext())
    {
        InventoryEntity vehicle = (InventoryEntity) iterator.next();

        vehicleIds.add(vehicle.getInventoryId());
    }

    int[] newVehicleIdArray = new int[vehicleIds.size()];

    for (int i = 0; i < vehicleIds.size(); i++)
    {
        newVehicleIdArray[i] = ((Integer) vehicleIds.get(i)).intValue();
    }

    setVehicleIdArray(newVehicleIdArray);
}
}
