package com.firstlook.entity.form;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import biz.firstlook.commons.enumeration.LightAndCIATypeDescriptor;
import biz.firstlook.transact.persist.model.BookOut;
import biz.firstlook.transact.persist.model.MakeModelGrouping;
import biz.firstlook.transact.persist.service.appraisal.AppraisalSource;
import biz.firstlook.transact.persist.service.appraisal.AppraisalValue;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;

import com.firstlook.action.BaseActionForm;
import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.iterator.SimpleFormIterator;
import com.firstlook.util.Formatter;

public class TradeAnalyzerPlusForm extends BaseActionForm
{
private static final long serialVersionUID = -4576279519561701018L;
public static final String TRIMBODY_DELIMITER = "--";
public static String VIEW_ALL = "viewAll";
private IAppraisal appraisal;
private String[] inGroupDemand;
private int showAll;
private String action;
private Collection bookValues;
private int guideBookId;
private Collection guideBookValuesNoMileage;

private int appraisalId;
private int actionId;
private int light;
private int dealerId;
private int appraisalValue;
private String conditionDescription;
private Double wholesalePrice;
private Double estimatedReconditioningCost;

private boolean isWirelessAppraisal = false;
private boolean isCRMAppraisal = false;
private boolean isMobileAppraisal = false;
private int percentToMarket;
//private Integer appraisalTypeId;

public TradeAnalyzerPlusForm()
{
}

public TradeAnalyzerPlusForm( IAppraisal appraisal )
{
	setBusinessObject( appraisal );
}

public void setBusinessObject( Object obj )
{
	appraisal = (IAppraisal)obj;
	MakeModelGrouping makeModelGrouping = appraisal.getVehicle().getMakeModelGrouping();
	if ( makeModelGrouping != null )
	{
		setActualMake( makeModelGrouping.getMake() );
		setActualModel( makeModelGrouping.getModel() );
	}
	setAppraisalId( appraisal.getAppraisalId().intValue() );
	//setAppraisalTypeId(appraisal.getAppraisalTypeId());
	//setVin(appraisal.getVehicle().getVin());
	//setDateCreated(appraisal.getDateCreated);
	if( appraisal.getAppraisalSource().getAppraisalSourceId().intValue() == AppraisalSource.WAVIS.getAppraisalSourceId().intValue())
	{
		isWirelessAppraisal = true;
	} else if( appraisal.getAppraisalSource().getAppraisalSourceId().intValue() == AppraisalSource.AUTOBASE.getAppraisalSourceId().intValue()) {
		isCRMAppraisal = true;
	}
	else if (appraisal.getAppraisalSource().isMobile())
	{
		isMobileAppraisal = true;
	}
	
	AppraisalValue valueObj = appraisal.getLatestAppraisalValue();
	setAppraisalValue( valueObj == null ? 0 : valueObj.getValue() );
	
	setDealerId( appraisal.getBusinessUnitId() );
	setActionId( appraisal.getAppraisalActionType().getAppraisalActionTypeId() );
	setConditionDisclosure( appraisal.getConditionDescription() );
	this.estimatedReconditioningCost = appraisal.getEstimatedReconditioningCost();
	setPrice( ( appraisal.getWholesalePrice() != null ? appraisal.getWholesalePrice().intValue() : 0 ) );
}
public String getAppraisalSource(){
	return appraisal.getAppraisalSource().getAppraisalSourceId()==7?"FL Mobile":"CRM";
}
public void setAppraisalValue(Integer val){
	appraisalValue = ( val == null ) ? 0 : (int)val;
}

public int getAppraisalValue(){
	return appraisalValue;
}

public int getActionId()
{
	return actionId;
}

public int getBookPrice()
{
	return getLatestBookOut().getBookPrice().intValue();
}

public String getBody()
{
	return appraisal.getVehicle().getBodyType().getBodyType();
}

public String getActualMake()
{
	return appraisal.getVehicle().getMakeModelGrouping().getMake();
}

public String getActualMakeUpper()
{
	if ( getActualMake() != null )
	{
		return getActualMake().toUpperCase();
	}
	else
	{
		return null;
	}
}

public String getActualModelUpper()
{
	if ( getActualModel() != null )
	{
		return getActualModel().toUpperCase();
	}
	else
	{
		return null;
	}
}

public String getActualModel()
{
	return appraisal.getVehicle().getMakeModelGrouping().getModel();
}

public String getActualTrim()
{
	return appraisal.getVehicle().getVehicleTrim();
}

public String getActualTrimUpper()
{	
	return ( appraisal.getVehicle().getVehicleTrim() != null ? appraisal.getVehicle().getVehicleTrim().toUpperCase() : null );
}

public String getColor()
{
	return appraisal.getColor();
}

public boolean isSold()
{
	if ( appraisal.getDealCompleted() != null )
	{
		return appraisal.getDealCompleted().booleanValue();
	}
	return false;
}

public int getDealerId()
{
	return this.dealerId;
}

public String getConditionDisclosure()
{
	return conditionDescription;
}

public int getGroupingId() throws ApplicationException
{
	return appraisal.getVehicle().getMakeModelGrouping().getGroupingDescription().getGroupingDescriptionId().intValue();
}

public String getMake()
{
	return appraisal.getVehicle().getMakeModelGrouping().getMake();
}

public int getMileage()
{
	return appraisal.getMileage();
}

public String getMileageFormatted()
{
	return Formatter.formatNumberToString( getMileage() );
}

public String getPriceFormatted()
{
	return formatIntPriceToString( ( wholesalePrice != null ? wholesalePrice.intValue() : 0 ), Formatter.NOT_AVAILABLE );
}

public String getModel()
{
	return appraisal.getVehicle().getMakeModelGrouping().getModel();
}

public int getPrice()
{
	return ( wholesalePrice != null ? wholesalePrice.intValue() : 0 );
}

public String getReconditioningNotes()
{
	return ( estimatedReconditioningCost != null ? estimatedReconditioningCost.toString() : "" );
}

public String getReconditioningNotesFormatted()
{
	int value = getIntFromString( appraisal.getEstimatedReconditioningCost().toString() );

	return formatIntPriceToString( value, Formatter.NOT_AVAILABLE );
}

public Date getTradeAnalyzerDate()
{
	return appraisal.getDateModified();
}

public String getTradeAnalyzerDateFormatted()
{
	SimpleDateFormat dateFormat = new SimpleDateFormat( "MM/dd/yyyy" );

	if ( appraisal.getDateModified() != null )
	{
		return dateFormat.format( appraisal.getDateModified() );
	}

	return Formatter.NOT_AVAILABLE;
}

public int getAppraisalId()
{
	return appraisalId;
}

public void setAppraisalId( int taPlusId )
{
	appraisalId = taPlusId;
}

public String getTrim()
{
	return appraisal.getVehicle().getVehicleTrim();
}

public String getVin()
{
	return appraisal.getVehicle().getVin();
}

public String getYear()
{
	return appraisal.getVehicle().getVehicleYear().toString();
}

public Boolean getIsWirelessAppraisal()
{
	return isWirelessAppraisal;
}

public Boolean getIsCRMAppraisal()
{
	return isCRMAppraisal;
}

public Boolean getIsMobileAppraisal()
{
	return isMobileAppraisal;
}

public void setActionId( int actionId )
{
	this.actionId = actionId;
}

public void setBookPrice( int bookPrice )
{
	getLatestBookOut().setBookPrice( new Integer( bookPrice ) );
}

public void setActualMake( String actualMake )
{
	appraisal.getVehicle().getMakeModelGrouping().setMake( actualMake );
}

public void setActualModel( String actualModel )
{
	appraisal.getVehicle().getMakeModelGrouping().setModel( actualModel );
}

public void setActualTrim( String actualTrim )
{
	appraisal.getVehicle().setVehicleTrim( actualTrim );
}

public void setColor( String color )
{
	appraisal.setColor( color );
}

public void setDealerId( int dealerId )
{
	this.dealerId = dealerId;
}

public void setConditionDisclosure( String conditionDisclosure )
{
	this.conditionDescription = conditionDisclosure;
}

public void setMake( String make )
{
	appraisal.getVehicle().setMake( make );
}

public void setMileage( int mileage )
{
	appraisal.setMileage( mileage );
}

public void setModel( String model )
{
	appraisal.getVehicle().setModel( model );
}

public void setPrice( int price )
{
	this.wholesalePrice = new Double( price );
}

public void setPriceFormatted( String price )
{
	String newPrice = removeComma( price );
	if ( checkIsNumeric( newPrice ) )
	{
		this.wholesalePrice = ( new Double( Integer.parseInt( newPrice ) ) );
	}
}

public void setReconditioningNotes( String reconditioningNotes )
{
	estimatedReconditioningCost = new Double( reconditioningNotes );
}

public void setTradeAnalyzerDate( Date tradeAnalyzerDate )
{
	//not suppose to do that on a form!  use the service (factory)!
}

public void setTrim( String trim )
{
	appraisal.getVehicle().setVehicleTrim( trim );
}

public void setTrimBody( String s )
{
	if ( s.length() == 0 )
	{
		appraisal.getVehicle().setVehicleTrim( null );
	}
	else
	{
		String sub = null;
		int i = 0;
		int j = s.indexOf( TRIMBODY_DELIMITER );

		sub = s.substring( i, j ).trim();
		appraisal.getVehicle().setVehicleTrim( sub );

		sub = s.substring( j + TRIMBODY_DELIMITER.length(), s.length() ).trim();
		appraisal.getVehicle().getBodyType().setBodyType( sub );
	}
}

public void setVin( String vin )
{
	appraisal.getVehicle().setVin( vin );
}

public void setYear( String year )
{
	int yearInt = Integer.parseInt( year );
	appraisal.getVehicle().setVehicleYear( new Integer( yearInt ) );
}

public IAppraisal getAppraisal()
{
	return appraisal;
}

public String[] getInGroupDemand()
{
	return inGroupDemand;
}

public List getInGroupDemandAsList()
{
	if ( getInGroupDemand() != null )
	{
		return Arrays.asList( getInGroupDemand() );
	}

	return new ArrayList();
}

public void setInGroupDemand( String[] inGroupDemand )
{
	this.inGroupDemand = inGroupDemand;
}

public int getShowAll()
{
	return showAll;
}

public String getTrimBody()
{
	return appraisal.getVehicle().getVehicleTrim()
			+ " " + TRIMBODY_DELIMITER + " " + appraisal.getVehicle().getBodyType().getBodyType();
}

public void setShowAll( int showAll )
{
	this.showAll = showAll;
}

public void setSold( boolean sold )
{
	appraisal.setDealCompleted( sold );
}

public int getCylinderCount()
{
	return appraisal.getVehicle().getCylinderCount().intValue();
}

public int getDoorCount()
{
	return appraisal.getVehicle().getDoorCount().intValue();
}

public String getDriveTrainDescription()
{
	return appraisal.getVehicle().getVehicleDriveTrain();
}

public String getEngineDescription()
{
	return appraisal.getVehicle().getVehicleEngine();
}

public String getFuelType()
{
	return appraisal.getVehicle().getFuelType();
}

public String getInteriorColor()
{
	return appraisal.getVehicle().getInteriorColor();
}

public String getInteriorDescription()
{
	return appraisal.getVehicle().getInteriorDescription();
}

public String getTransmission()
{
	return appraisal.getVehicle().getVehicleTransmission();
}

public void setCylinderCount( int cylinderCount )
{
	appraisal.getVehicle().setCylinderCount( new Integer( cylinderCount ) );
}

public void setDoorCount( int doorCount )
{
	appraisal.getVehicle().setDoorCount( new Integer( doorCount ) );
}

public void setDriveTrainDescription( String driveTrainDescription )
{
	appraisal.getVehicle().setVehicleDriveTrain( driveTrainDescription );
}

public void setEngineDescription( String engineDescription )
{
	appraisal.getVehicle().setVehicleEngine( engineDescription );
}

public void setFuelType( String fuelType )
{
	appraisal.getVehicle().setFuelType( fuelType );
}

public void setInteriorColor( String interiorColor )
{
	appraisal.getVehicle().setInteriorColor( interiorColor );
}

public void setInteriorDescription( String interiorDescription )
{
	appraisal.getVehicle().setInteriorDescription( interiorDescription );
}

public void setTransmission( String transmission )
{
	appraisal.getVehicle().setVehicleTransmission( transmission );
}

public BookOut getLatestBookOut()
{
	return appraisal.getLatestBookOut( guideBookId );
}

public SimpleFormIterator getBookValues() throws DatabaseException, ApplicationException
{
	return new SimpleFormIterator( this.bookValues, TradeAnalyzerPlusBookValuesForm.class );
}

public void setBookValues( Collection bookValues )
{
	this.bookValues = bookValues;
}

public String getGroupingDescription() throws ApplicationException
{
	return appraisal.getVehicle().getMakeModelGrouping().getGroupingDescription().getGroupingDescription();
}

public String getAction()
{
	return action;
}

public void setAction( String string )
{
	action = string;
}

public void setAppraisal( IAppraisal appraisal )
{
	this.appraisal = appraisal;
}

public SimpleFormIterator getGuideBookValuesNoMileage()
{
	return ( new SimpleFormIterator( guideBookValuesNoMileage, TradeAnalyzerPlusBookValuesForm.class ) );
}

public void setGuideBookValuesNoMileage( Collection collection )
{
	guideBookValuesNoMileage = collection;
}

public int getMileageCostAdjustment()
{
	return getLatestBookOut().getMileageCostAdjustment().intValue();
}

public int getLight()
{
	return light;
}

/**
 * see {@link LightAndCIATypeDescriptor}.
 * 1 - red, 2 - yellow, 3 - green.
 * @param light
 */
public void setLight( int light )
{
	this.light = light;
}

public String getLastBookOutDateFormatted()
{
	if ( getLatestBookOut().getDateCreated() != null )
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat( "MM/dd/yyyy" );
		return dateFormat.format( getLatestBookOut().getDateCreated() );
	}
	else
	{
		return "";
	}
}

public int getLastAppraisal() 
{
	return ( appraisal.getLatestAppraisalValue() != null ? appraisal.getLatestAppraisalValue().getValue() : 0 );
}

public String getLastInitials()
{
	return getLastAppraiser();
}

public String getLastAppraiser()
{
	return ( appraisal.getLatestAppraisalValue() != null ? appraisal.getLatestAppraisalValue().getAppraiserName() : "" );
}

public String getLastAppraisalAndInitials()
{
	if( appraisal.getLatestAppraisalValue() != null )
	{
		StringBuilder sb = new StringBuilder();
		sb.append( "$" ).append( appraisal.getLatestAppraisalValue().getValue() );
		sb.append( " (" ).append( appraisal.getLatestAppraisalValue().getAppraiserName() );
		return sb.toString();
	}
	else
	{
		return "-";
	}
	
}
public String getAppraiserOrBuyer(){
	if(appraisal.getAppraisalTypeId()==1)
	{
		return getLastAppraiser();
	}
	else{
		return getBuyer();
	}
}

public String getSalesPerson()
{	
	return ( appraisal.getDealTrackSalesPerson() != null ? appraisal.getDealTrackSalesPerson().getFullName() : null );
}

public String getBuyer()
{	
	return ( appraisal.getSelectedBuyer() != null ? appraisal.getSelectedBuyer().getFullName() : null );
}

public int getGuideBookId()
{
	return guideBookId;
}

public void setGuideBookId( int guideBookId )
{
	this.guideBookId = guideBookId;
}
public String getAppraisalType(){
	return appraisal.getAppraisalTypeId()==1?"Trade":"Purchase";
}
public String getCustomer(){
	if(appraisal.getCustomer()!=null)
	return appraisal.getCustomer().getFirstName()+" "+appraisal.getCustomer().getLastName();
	return "";
}

public int getPercentToMarket() {
	return percentToMarket;
}

public void setPercentToMarket(int percentToMarket) {
	this.percentToMarket = percentToMarket;
}
}