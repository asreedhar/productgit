package com.firstlook.entity.form;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import biz.firstlook.transact.persist.model.Vehicle;

import com.firstlook.entity.Dealer;
import com.firstlook.entity.InventoryEntity;
import com.firstlook.entity.VehicleSaleEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.ObjectMother;
import com.firstlook.util.Formatter;

public class TestSaleForm extends BaseTestCase
{

private SaleForm form;
private InventoryEntity inventory;
private VehicleSaleEntity sale;
private SaleFormFactory factory = new SaleFormFactory();

public TestSaleForm( String arg1 )
{
    super(arg1);
}

public void setup()
{
    inventory = ObjectMother.createInventory();
    Dealer dealer = ObjectMother.createDealer();
    inventory.setDealer(dealer);
    Vehicle vehicle = ObjectMother.createVehicle();
    inventory.setVehicle( vehicle );

    sale = ObjectMother.createVehicleSale();
    sale.setInventory(inventory);
}

public void testGetDaysToSellDealDateNull() throws ApplicationException
{
    sale.setDealDate(null);
    form = factory.buildSaleForm(sale, sale.getInventory().getDealer() );

    assertEquals(0, form.getDaysToSell());
    assertEquals(Formatter.NOT_AVAILABLE, form.getDaysToSellFormatted());
}

public void testGetDaysToSellLaterReceivedDate() throws ApplicationException
{
    GregorianCalendar calendar = new GregorianCalendar();
    calendar.set(0, 0, 10);
    Date dealDate = calendar.getTime();

    calendar.add(Calendar.DATE, 10);

    sale.setDealDate(dealDate);
    form = factory.buildSaleForm(sale, sale.getInventory().getDealer() );

    assertEquals(1, form.getDaysToSell());
    assertEquals("1", form.getDaysToSellFormatted());
}

public void testGetDaysToSellReceivedDateNull() throws ApplicationException
{
    sale.setDealDate(new Date());
    inventory.setInventoryReceivedDt(null);
    sale.setInventory(inventory);
    form = factory.buildSaleForm(sale, sale.getInventory().getDealer() );

    assertEquals(0, form.getDaysToSell());
    assertEquals(Formatter.NOT_AVAILABLE, form.getDaysToSellFormatted());
}

public void testGetDaysToSellValidDates() throws ApplicationException
{
    GregorianCalendar calendar = new GregorianCalendar();
    calendar.set(0, 0, 10);

    calendar.add(Calendar.DATE, 1);
    Date dealDate = calendar.getTime();

    sale.setDealDate(dealDate);
    form = factory.buildSaleForm(sale, sale.getInventory().getDealer() );

    assertEquals(1, form.getDaysToSell());
    assertEquals("1", form.getDaysToSellFormatted());
}

public void testGetGrossProfit() throws ApplicationException
{
    sale.setFrontEndGross(20);
    form = factory.buildSaleForm(sale, sale.getInventory().getDealer() );

    assertEquals("$20", form.getGrossProfit());

    sale.setFrontEndGross(Double.NaN);
    form = factory.buildSaleForm(sale, sale.getInventory().getDealer() );

    assertEquals(Formatter.NOT_AVAILABLE, form.getGrossProfit());
}

public void testUnitCostFormatted() throws ApplicationException
{
    sale.getInventory().setUnitCost(20);
    form = factory.buildSaleForm(sale, sale.getInventory().getDealer() );

    assertEquals("$20", form.getUnitCostFormatted());

    sale.getInventory().setUnitCost(Double.NaN);
    form = factory.buildSaleForm(sale, sale.getInventory().getDealer() );

    assertEquals(Formatter.NOT_AVAILABLE, form.getUnitCostFormatted());
}

public void testGetMileage() throws ApplicationException
{
    sale.setVehicleMileage(Integer.MIN_VALUE);
    form = factory.buildSaleForm(sale, sale.getInventory().getDealer() );

    assertEquals(Formatter.NOT_AVAILABLE, form.getMileageFormatted());

}
}
