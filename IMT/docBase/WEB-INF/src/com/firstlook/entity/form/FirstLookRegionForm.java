package com.firstlook.entity.form;

import com.firstlook.action.BaseActionForm;
import com.firstlook.entity.FirstLookRegion;

public class FirstLookRegionForm extends BaseActionForm
{

private static final long serialVersionUID = 1567929210790812855L;
private FirstLookRegion firstLookRegion;

public FirstLookRegionForm()
{
    setBusinessObject(new FirstLookRegion());
}

public FirstLookRegionForm( FirstLookRegion firstLookRegion )
{
    setBusinessObject(firstLookRegion);
}

public void setBusinessObject( Object bo )
{
    firstLookRegion = (FirstLookRegion) bo;
}

public FirstLookRegion getFirstLookRegion()
{
    return firstLookRegion;
}

public int getFirstLookRegionId()
{
    return firstLookRegion.getFirstLookRegionId();
}

public String getFirstLookRegionName()
{
    if ( firstLookRegion.getFirstLookRegionName() == null )
    {
        return "";
    }

    return firstLookRegion.getFirstLookRegionName();
}

public void setFirstLookRegionId( int firstLookRegionId )
{
    firstLookRegion.setFirstLookRegionId(firstLookRegionId);
}

public void setFirstLookRegionName( String firstLookRegionName )
{
    firstLookRegion.setFirstLookRegionName(firstLookRegionName);
}

}
