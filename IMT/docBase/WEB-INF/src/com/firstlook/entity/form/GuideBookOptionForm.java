package com.firstlook.entity.form;

import com.firstlook.dealer.tools.GuideBookOption;

public class GuideBookOptionForm extends com.firstlook.action.BaseActionForm
{

private static final long serialVersionUID = -3038802255345783273L;
private com.firstlook.dealer.tools.GuideBookOption guideBookOption;

public GuideBookOptionForm()
{
	setBusinessObject( new GuideBookOption() );
}

public GuideBookOptionForm( GuideBookOption option )
{
	setBusinessObject( option );
}

public String getDescription()
{
	return guideBookOption.getDescription();
}

public String getDescriptionKelleyPrintout()
{
	if ( guideBookOption.getValue() < 0 && !guideBookOption.isSelected() ) 
	{
		return "No" + guideBookOption.getDescription();
	}
	return guideBookOption.getDescription();
}

public java.lang.String getKey()
{
	return guideBookOption.getKey();
}

public String getValue()
{
	return String.valueOf( guideBookOption.getValue() );
}

public String getValueKelleyPrintout()
{
	if ( guideBookOption.getValue() < 0 )
		if ( !guideBookOption.isSelected() )
		{
			return "<" + String.valueOf( guideBookOption.getValue() ) + ">";
		}
		else
		{
			return "Included";
		}
	return String.valueOf( guideBookOption.getValue() );
}

public String getWholesaleValueKelleyPrintout()
{
	if ( guideBookOption.getWholesaleValue() < 0 )
	{
		if ( !guideBookOption.isSelected() )
		{
			return "<" + String.valueOf( guideBookOption.getWholesaleValue() ) + ">";
		}
		else
		{
			return "Included";
		}
	}
	return String.valueOf( guideBookOption.getWholesaleValue() );
}

public boolean isIncludedOption()
{
	return getValueKelleyPrintout().equalsIgnoreCase( "Included" );
}

public String getValueFormatted()
{
	return formatPriceToString( guideBookOption.getValue() );
}

public void setBusinessObject( Object o )
{
	guideBookOption = (GuideBookOption)o;
}

public void setKey( java.lang.String newKey )
{
	guideBookOption.setKey( newKey );
}

public int getTabOrder()
{
	return guideBookOption.getTabOrder();
}

public void setTabOrder( int i )
{
	guideBookOption.setTabOrder( i );
}

public boolean isSelected()
{
	return guideBookOption.isSelected();
}

public boolean isStandardOption()
{
	return guideBookOption.isStandardOption();
}

}
