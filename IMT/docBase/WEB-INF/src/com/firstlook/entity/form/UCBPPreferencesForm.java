package com.firstlook.entity.form;

import java.util.Collection;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.validator.ValidatorForm;

public class UCBPPreferencesForm extends ValidatorForm
{

private static final long serialVersionUID = -7245528087583493801L;
private int businessUnitId;
private int riskLevelNumberOfWeeks;
private int riskLevelDealsThreshold;
private int riskLevelNumberOfContributors;
private int redLightNoSaleThreshold;
private int redLightGrossProfitThreshold;
private int greenLightNoSaleThreshold;
private double greenLightGrossProfitThreshold;
private double greenLightMarginThreshold;
private int greenLightDaysPercentage;
private int riskLevelYearInitialTimePeriod;
private int riskLevelYearSecondaryTimePeriod;
private int riskLevelYearRollOverMonth;
private int redLightTarget;
private int yellowLightTarget;
private int greenLightTarget;
private int unitCostBucketCreationThreshold;
private int ciaBasisPeriodForecastDays;
private int ciaBasisPeriodPriorDays;
private int ciaTimePeriodForecastId;
private int ciaTimePeriodPriorId;
private Collection ciaTimePeriods;
private Integer dealerTargetOverride;
private int ciaBucketTimePeriodId1;
private int ciaBucketTimePeriodId2;
private int ciaBucketTimePeriodId3;
private int targetDaysSupply;
private int bucketAllocationMinimumThreshold;
private int marketPerformersDisplayThreshold;
private int marketPerformerInStockThreshold;
private String ciaStoreTargetInventoryBasisPeriodDescription;
private int ciaStoreTargetInventoryBasisPeriodId;
private String ciaCoreModelDeterminationBasisPeriodDescription;
private int ciaCoreModelDeterminationBasisPeriodId;
private String ciaPowerzoneModelTargetInventoryBasisPeriodDescription;
private int ciaPowerzoneModelTargetInventoryBasisPeriodId;
private String ciaCoreModelYearAllocationBasisPeriodDescription;
private int ciaCoreModelYearAllocationBasisPeriodId;
private String gdLightProcessorTimePeriodDescription;
private int gdLightProcessorTimePeriodId;
private int salesHistoryDisplayTimePeriodId;
private String salesHistoryDisplayTimePeriodDescription;
private int excessiveMileageThreshold;
private int highMileageThreshold;
private int highMileagePerYearThreshold;
// new columns
private int highAgeThreshold;
private int excessiveAgeThreshold;
private int excessiveMileagePerYearThreshold;


public UCBPPreferencesForm()
{
}

public int getBusinessUnitId()
{
    return businessUnitId;
}

public int getGreenLightDaysPercentage()
{
    return greenLightDaysPercentage;
}

public double getGreenLightGrossProfitThreshold()
{
    return greenLightGrossProfitThreshold;
}

public double getGreenLightMarginThreshold()
{
    return greenLightMarginThreshold;
}

public int getGreenLightNoSaleThreshold()
{
    return greenLightNoSaleThreshold;
}

public int getGreenLightTarget()
{
    return greenLightTarget;
}

public int getRedLightGrossProfitThreshold()
{
    return redLightGrossProfitThreshold;
}

public int getRedLightNoSaleThreshold()
{
    return redLightNoSaleThreshold;
}

public int getRedLightTarget()
{
    return redLightTarget;
}

public int getRiskLevelDealsThreshold()
{
    return riskLevelDealsThreshold;
}

public int getRiskLevelNumberOfContributors()
{
    return riskLevelNumberOfContributors;
}

public int getRiskLevelNumberOfWeeks()
{
    return riskLevelNumberOfWeeks;
}

public int getRiskLevelYearInitialTimePeriod()
{
    return riskLevelYearInitialTimePeriod;
}

public int getRiskLevelYearRollOverMonth()
{
    return riskLevelYearRollOverMonth;
}

public int getRiskLevelYearSecondaryTimePeriod()
{
    return riskLevelYearSecondaryTimePeriod;
}

public int getYellowLightTarget()
{
    return yellowLightTarget;
}

public void setBusinessUnitId( int i )
{
    businessUnitId = i;
}

public void setGreenLightDaysPercentage( int i )
{
    greenLightDaysPercentage = i;
}

public void setGreenLightGrossProfitThreshold( double d )
{
    greenLightGrossProfitThreshold = d;
}

public void setGreenLightMarginThreshold( double d )
{
    greenLightMarginThreshold = d;
}

public void setGreenLightNoSaleThreshold( int i )
{
    greenLightNoSaleThreshold = i;
}

public void setGreenLightTarget( int i )
{
    greenLightTarget = i;
}

public void setRedLightGrossProfitThreshold( int i )
{
    redLightGrossProfitThreshold = i;
}

public void setRedLightNoSaleThreshold( int i )
{
    redLightNoSaleThreshold = i;
}

public void setRedLightTarget( int i )
{
    redLightTarget = i;
}

public void setRiskLevelDealsThreshold( int i )
{
    riskLevelDealsThreshold = i;
}

public void setRiskLevelNumberOfContributors( int i )
{
    riskLevelNumberOfContributors = i;
}

public void setRiskLevelNumberOfWeeks( int i )
{
    riskLevelNumberOfWeeks = i;
}

public void setRiskLevelYearInitialTimePeriod( int i )
{
    riskLevelYearInitialTimePeriod = i;
}

public void setRiskLevelYearRollOverMonth( int i )
{
    riskLevelYearRollOverMonth = i;
}

public void setRiskLevelYearSecondaryTimePeriod( int i )
{
    riskLevelYearSecondaryTimePeriod = i;
}

public void setYellowLightTarget( int i )
{
    yellowLightTarget = i;
}

public Collection getCiaTimePeriods()
{
    return ciaTimePeriods;
}

public void setCiaTimePeriods( Collection collection )
{
    ciaTimePeriods = collection;
}

public ActionErrors validateDealerRisk()
{
    ActionErrors errors = new ActionErrors();

    validateVehicleLightTarget(errors);
    validateRiskLevelYearThresholds(errors);

    return errors;
}

void validateRiskLevelYearThresholds( ActionErrors errors )
{
    int rollOverMonth = getRiskLevelYearRollOverMonth();

    if ( rollOverMonth > 12 || rollOverMonth < 1 )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.admin.dealer.risklevelyear"));
    }
}

void validateVehicleLightTarget( ActionErrors errors )
{
    int targetTotal = getRedLightTarget() + getGreenLightTarget()
            + getYellowLightTarget();

    if ( targetTotal != 100 )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.admin.dealer.vehiclelighttarget"));
    }
}

public int getUnitCostBucketCreationThreshold()
{
    return unitCostBucketCreationThreshold;
}

public void setUnitCostBucketCreationThreshold( int i )
{
    unitCostBucketCreationThreshold = i;
}

public Integer getDealerTargetOverride()
{
    return dealerTargetOverride;
}

public void setDealerTargetOverride( Integer integer )
{
    dealerTargetOverride = integer;
}

public int getCiaBucketTimePeriodId1()
{
    return ciaBucketTimePeriodId1;
}

public int getCiaBucketTimePeriodId2()
{
    return ciaBucketTimePeriodId2;
}

public int getCiaBucketTimePeriodId3()
{
    return ciaBucketTimePeriodId3;
}

public void setCiaBucketTimePeriodId1( int i )
{
    ciaBucketTimePeriodId1 = i;
}

public void setCiaBucketTimePeriodId2( int i )
{
    ciaBucketTimePeriodId2 = i;
}

public void setCiaBucketTimePeriodId3( int i )
{
    ciaBucketTimePeriodId3 = i;
}

public int getBucketAllocationMinimumThreshold()
{
    return bucketAllocationMinimumThreshold;
}

public void setBucketAllocationMinimumThreshold( int i )
{
    bucketAllocationMinimumThreshold = i;
}

public int getCiaBasisPeriodForecastDays()
{
    return ciaBasisPeriodForecastDays;
}

public int getCiaBasisPeriodPriorDays()
{
    return ciaBasisPeriodPriorDays;
}

public int getCiaTimePeriodForecastId()
{
    return ciaTimePeriodForecastId;
}

public int getCiaTimePeriodPriorId()
{
    return ciaTimePeriodPriorId;
}

public void setCiaBasisPeriodForecastDays( int i )
{
    ciaBasisPeriodForecastDays = i;
}

public void setCiaBasisPeriodPriorDays( int i )
{
    ciaBasisPeriodPriorDays = i;
}

public void setCiaTimePeriodForecastId( int i )
{
    ciaTimePeriodForecastId = i;
}

public void setCiaTimePeriodPriorId( int i )
{
    ciaTimePeriodPriorId = i;
}

public int getTargetDaysSupply()
{
    return targetDaysSupply;
}

public void setTargetDaysSupply( int i )
{
    targetDaysSupply = i;
}

public int getMarketPerformersDisplayThreshold()
{
    return marketPerformersDisplayThreshold;
}

public void setMarketPerformersDisplayThreshold( int i )
{
    marketPerformersDisplayThreshold = i;
}

public int getMarketPerformerInStockThreshold()
{
    return marketPerformerInStockThreshold;
}

public void setMarketPerformerInStockThreshold( int i )
{
    marketPerformerInStockThreshold = i;
}

public String getGdLightProcessorTimePeriodDescription()
{
    return gdLightProcessorTimePeriodDescription;
}

public int getGdLightProcessorTimePeriodId()
{
    return gdLightProcessorTimePeriodId;
}

public void setGdLightProcessorTimePeriodDescription( String string )
{
    gdLightProcessorTimePeriodDescription = string;
}

public void setGdLightProcessorTimePeriodId( int i )
{
    gdLightProcessorTimePeriodId = i;
}

public String getSalesHistoryDisplayTimePeriodDescription()
{
    return salesHistoryDisplayTimePeriodDescription;
}

public void setSalesHistoryDisplayTimePeriodDescription(
        String salesHistoryDisplayTimePeriodDescription )
{
    this.salesHistoryDisplayTimePeriodDescription = salesHistoryDisplayTimePeriodDescription;
}

public int getSalesHistoryDisplayTimePeriodId()
{
    return salesHistoryDisplayTimePeriodId;
}

public void setSalesHistoryDisplayTimePeriodId(
        int salesHistoryDisplayTimePeriodId )
{
    this.salesHistoryDisplayTimePeriodId = salesHistoryDisplayTimePeriodId;
}

public String getCiaCoreModelDeterminationBasisPeriodDescription()
{
	return ciaCoreModelDeterminationBasisPeriodDescription;
}

public void setCiaCoreModelDeterminationBasisPeriodDescription( String ciaCoreModelDeterminationBasisPeriodDescription )
{
	this.ciaCoreModelDeterminationBasisPeriodDescription = ciaCoreModelDeterminationBasisPeriodDescription;
}

public int getCiaCoreModelDeterminationBasisPeriodId()
{
	return ciaCoreModelDeterminationBasisPeriodId;
}

public void setCiaCoreModelDeterminationBasisPeriodId( int ciaCoreModelDeterminationBasisPeriodId )
{
	this.ciaCoreModelDeterminationBasisPeriodId = ciaCoreModelDeterminationBasisPeriodId;
}

public String getCiaCoreModelYearAllocationBasisPeriodDescription()
{
	return ciaCoreModelYearAllocationBasisPeriodDescription;
}

public void setCiaCoreModelYearAllocationBasisPeriodDescription( String ciaCoreModelYearAllocationBasisPeriodDescription )
{
	this.ciaCoreModelYearAllocationBasisPeriodDescription = ciaCoreModelYearAllocationBasisPeriodDescription;
}

public int getCiaCoreModelYearAllocationBasisPeriodId()
{
	return ciaCoreModelYearAllocationBasisPeriodId;
}

public void setCiaCoreModelYearAllocationBasisPeriodId( int ciaCoreModelYearAllocationBasisPeriodId )
{
	this.ciaCoreModelYearAllocationBasisPeriodId = ciaCoreModelYearAllocationBasisPeriodId;
}

public int getCiaPowerzoneModelTargetInventoryBasisPeriodId()
{
	return ciaPowerzoneModelTargetInventoryBasisPeriodId;
}

public void setCiaPowerzoneModelTargetInventoryBasisPeriodId( int ciaPowerzoneModelTargetInventoryBasisPeriod )
{
	this.ciaPowerzoneModelTargetInventoryBasisPeriodId = ciaPowerzoneModelTargetInventoryBasisPeriod;
}

public String getCiaPowerzoneModelTargetInventoryBasisPeriodDescription()
{
	return ciaPowerzoneModelTargetInventoryBasisPeriodDescription;
}

public void setCiaPowerzoneModelTargetInventoryBasisPeriodDescription( String ciaPowerzoneModelTargetInventoryBasisPeriodDescription )
{
	this.ciaPowerzoneModelTargetInventoryBasisPeriodDescription = ciaPowerzoneModelTargetInventoryBasisPeriodDescription;
}

public String getCiaStoreTargetInventoryBasisPeriodDescription()
{
	return ciaStoreTargetInventoryBasisPeriodDescription;
}

public void setCiaStoreTargetInventoryBasisPeriodDescription( String ciaStoreTargetInventoryBasisPeriodDescription )
{
	this.ciaStoreTargetInventoryBasisPeriodDescription = ciaStoreTargetInventoryBasisPeriodDescription;
}

public int getCiaStoreTargetInventoryBasisPeriodId()
{
	return ciaStoreTargetInventoryBasisPeriodId;
}

public void setCiaStoreTargetInventoryBasisPeriodId( int ciaStoreTargetInventoryBasisPeriodId )
{
	this.ciaStoreTargetInventoryBasisPeriodId = ciaStoreTargetInventoryBasisPeriodId;
}

public int getExcessiveAgeThreshold() {
	return excessiveAgeThreshold;
}

public void setExcessiveAgeThreshold(int excessiveAgeThreshold) {
	this.excessiveAgeThreshold = excessiveAgeThreshold;
}

public int getExcessiveMileagePerYearThreshold() {
	return excessiveMileagePerYearThreshold;
}

public void setExcessiveMileagePerYearThreshold(
		int excessiveMileagePerYearThreshold) {
	this.excessiveMileagePerYearThreshold = excessiveMileagePerYearThreshold;
}

public int getExcessiveMileageThreshold() {
	return excessiveMileageThreshold;
}

public void setExcessiveMileageThreshold(int excessiveMileageThreshold) {
	this.excessiveMileageThreshold = excessiveMileageThreshold;
}

public int getHighAgeThreshold() {
	return highAgeThreshold;
}

public void setHighAgeThreshold(int highAgeThreshold) {
	this.highAgeThreshold = highAgeThreshold;
}

public int getHighMileagePerYearThreshold() {
	return highMileagePerYearThreshold;
}

public void setHighMileagePerYearThreshold(int highMileagePerYearThreshold) {
	this.highMileagePerYearThreshold = highMileagePerYearThreshold;
}

public int getHighMileageThreshold() {
	return highMileageThreshold;
}

public void setHighMileageThreshold(int highMileageThreshold) {
	this.highMileageThreshold = highMileageThreshold;
}

}
