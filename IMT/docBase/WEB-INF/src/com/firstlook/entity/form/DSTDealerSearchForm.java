package com.firstlook.entity.form;

public class DSTDealerSearchForm extends com.firstlook.action.BaseActionForm
{

private static final long serialVersionUID = -8072004367295104657L;
private java.lang.String nickname;
private int marketId;
private java.lang.String name;
private int topLevelBusinessUnitId;

public DSTDealerSearchForm()
{
    super();
}

public int getMarketId()
{
    return marketId;
}

public java.lang.String getName()
{
    return name;
}

public java.lang.String getNickname()
{
    return nickname;
}

public void setMarketId( int newMarketId )
{
    marketId = newMarketId;
}

public void setName( java.lang.String newName )
{
    name = newName;
}

public void setNickname( java.lang.String newNickname )
{
    nickname = newNickname;
}

public int getTopLevelBusinessUnitId()
{
    return topLevelBusinessUnitId;
}

public void setTopLevelBusinessUnitId( int i )
{
    topLevelBusinessUnitId = i;
}

}
