package com.firstlook.entity.form;

import biz.firstlook.transact.persist.model.GroupingDescription;

import com.firstlook.exception.ApplicationException;
import com.firstlook.service.groupingdescription.GroupingDescriptionService;

public class ViewDealsForm extends com.firstlook.action.BaseActionForm
{
private static final long serialVersionUID = 5190332999742377639L;
private java.lang.String make;
private java.lang.String model;
private java.lang.String trim;
private int includeDealerGroup;
private int bodyStyleId;
private int mileage;
private int mileageFilter;
private int threshold;

private int lowMileage;
private int highMileage;
private int saleTypeId;
private int mileageRangeId;
private int timePeriodId;
private int trimId;

public final static int REPORT_TYPE_NONE = 0;
public final static int REPORT_TYPE_GROUPING_ONLY = 1;
public final static int REPORT_TYPE_GROUPING_WITH_YEAR_AND_TRIM = 2;
public final static int REPORT_TYPE_NEW_CAR = 3;
public final static int REPORT_TYPE_NEW_CAR_NO_BODY = 4; // KL - only used by the Performance Search page which is deactviated 10-28-04
public final static int REPORT_TYPE_GROUPING_WITH_YEAR_TRIM_MILEAGE = 5;

private int reportType;
private int groupingId;

public ViewDealsForm()
{
    super();
    setReportType(REPORT_TYPE_NONE);
}

public int getGroupingId()
{
    return groupingId;
}

public String getGroupingDescription() throws ApplicationException
{
    GroupingDescriptionService service = new GroupingDescriptionService();
    GroupingDescription grouping = service.retrieveById(new Integer(
            getGroupingId()));

    return grouping.getGroupingDescription();
}

/**
 * 
 * @return int
 */
public int getIncludeDealerGroup()
{
    return includeDealerGroup;
}

/**
 * 
 * @return java.lang.String
 */
public java.lang.String getMake()
{
    return make;
}

/**
 * 
 * @return java.lang.String
 */
public java.lang.String getModel()
{
    return model;
}

/**
 * 
 * @return int
 */
public int getReportType()
{
    return reportType;
}

/**
 * 
 * @return java.lang.String
 */
public java.lang.String getTrim()
{
    return trim;
}

/**
 * 
 * @param newGroupingId
 *            int
 */
public void setGroupingId( int newGroupingId )
{
    groupingId = newGroupingId;
}

/**
 * 
 * @param newIncludeDealerGroup
 *            int
 */
public void setIncludeDealerGroup( int newIncludeDealerGroup )
{
    includeDealerGroup = newIncludeDealerGroup;
}

/**
 * 
 * @param newMake
 *            java.lang.String
 */
public void setMake( java.lang.String newMake )
{
    make = newMake;
}

/**
 * 
 * @param newModel
 *            java.lang.String
 */
public void setModel( java.lang.String newModel )
{
    model = newModel;
}

/**
 * 
 * @param newReportType
 *            int
 */
public void setReportType( int newReportType )
{
    reportType = newReportType;
}

/**
 * 
 * @param newTrim
 *            java.lang.String
 */
public void setTrim( java.lang.String newTrim )
{
    trim = newTrim;
}

public int getBodyStyleId()
{
    return bodyStyleId;
}

public void setBodyStyleId( int style )
{
    bodyStyleId = style;
}

public int getMileage()
{
    return mileage;
}

public int getMileageFilter()
{
    return mileageFilter;
}

public void setMileage( int i )
{
    mileage = i;
}

public void setMileageFilter( int i )
{
    mileageFilter = i;
}

public int getThreshold()
{
    return threshold;
}

public void setThreshold( int i )
{
    threshold = i;
}

public int getHighMileage()
{
	return highMileage;
}

public void setHighMileage( int highMileage )
{
	this.highMileage = highMileage;
}

public int getLowMileage()
{
	return lowMileage;
}

public void setLowMileage( int lowMileage )
{
	this.lowMileage = lowMileage;
}

public int getMileageRangeId()
{
	return mileageRangeId;
}

public void setMileageRangeId( int mileagePeriodId )
{
	this.mileageRangeId = mileagePeriodId;
}

public int getSaleTypeId()
{
	return saleTypeId;
}

public void setSaleTypeId( int saleTypeId )
{
	this.saleTypeId = saleTypeId;
}

public int getTimePeriodId()
{
	return timePeriodId;
}

public void setTimePeriodId( int timePeriodId )
{
	this.timePeriodId = timePeriodId;
}

public int getTrimId()
{
	return trimId;
}

public void setTrimId( int trimId )
{
	this.trimId = trimId;
}

}
