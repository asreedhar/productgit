package com.firstlook.entity.form;

import com.firstlook.action.BaseActionForm;
import com.firstlook.dealer.tools.GuideBookVehicle;

public abstract class GuideBookVehicleForm extends BaseActionForm
{
private GuideBookVehicle vehicle;

public GuideBookVehicleForm()
{
    super();
}

public java.lang.String getKey()
{
    return vehicle.getKey();
}

public GuideBookVehicle getVehicle()
{
    return vehicle;
}

public void setBusinessObject( Object businessObject )
{
    vehicle = (GuideBookVehicle) businessObject;

}

public void setKey( String newKey )
{
    vehicle.setKey(newKey);
}

public void setVehicle( GuideBookVehicle newVehicle )
{
    vehicle = newVehicle;
}

}
