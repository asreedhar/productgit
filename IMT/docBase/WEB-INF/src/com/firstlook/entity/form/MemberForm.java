package com.firstlook.entity.form;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.transact.persist.model.Credential;
import biz.firstlook.transact.persist.model.CredentialType;
import biz.firstlook.transact.persist.model.UserRoleEnum;

import com.firstlook.action.BaseActionForm;
import com.firstlook.action.user.member.SubscriptionDisplayData;
import com.firstlook.entity.Member;
import com.firstlook.entity.Role;
import com.firstlook.service.member.RoleService;

public class MemberForm extends BaseActionForm
{

private static final long serialVersionUID = 5859887070744699456L;

public static final String ENCODED_PASSWORD = "********";

protected Member member;
private String selectedAddressIndex;
private String password;
private String passwordConfirm;
private PhoneNumberFormHelper officePhoneNumber;
private PhoneNumberFormHelper officeFaxNumber;
private PhoneNumberFormHelper mobilePhoneNumber;
private String oldPassword;
private PhoneNumberFormHelper pagerNumber;
private String newPassword;
private String dealerGroupName;
private int[] inventoryStatusCDPresets;
private Integer usedRoleId;
private Integer newRoleId;
private Integer adminRoleId;
private String usedRoleName;
private String newRoleName;
private String adminRoleName;
private Integer defaultDealerGroupId;
private Collection<SubscriptionDisplayData> subscriptionData;

private String gmSmartAuctionUsername;
private String gmSmartAuctionPassword;
private Boolean canUseDealerCarfaxCredentials = Boolean.FALSE;

public MemberForm()
{
	super();
	member = new Member();
	officePhoneNumber = new PhoneNumberFormHelper();
	officeFaxNumber = new PhoneNumberFormHelper();
	mobilePhoneNumber = new PhoneNumberFormHelper();
	pagerNumber = new PhoneNumberFormHelper();
}

public MemberForm( Member newMember )
{
	super();
	member = newMember;
	password = member.getPassword();
	passwordConfirm = password;
	officePhoneNumber = new PhoneNumberFormHelper( member.getOfficePhoneNumber() );
	officeFaxNumber = new PhoneNumberFormHelper( member.getOfficeFaxNumber() );
	mobilePhoneNumber = new PhoneNumberFormHelper( member.getMobilePhoneNumber() );
	pagerNumber = new PhoneNumberFormHelper( member.getPagerNumber() );
	setUsedRoleId( newMember );
	setNewRoleId( newMember );
	setAdminRoleId( newMember );

	Collection<Credential> credentials = newMember.getCredentials();
	if ( credentials != null )
	{
		Iterator<Credential> credentialsIter = credentials.iterator();
		while ( credentialsIter.hasNext() )
		{
			Credential credential = credentialsIter.next();
			int credentialTypeId = credential.getCredentialTypeId().intValue();
			if ( credentialTypeId == CredentialType.GMAC.getId().intValue())
			{
				gmSmartAuctionUsername = credential.getUsername();
				gmSmartAuctionPassword = credential.getPassword();
			}
		}
	}
}

private void setNewRoleId( Member member )
{
	Role newRole = RoleService.getNewRole( member );
	if ( newRole != null )
	{
		newRoleId = newRole.getRoleId();
		newRoleName = newRole.getName();
	}
}

private void setUsedRoleId( Member newMember )
{
	Role usedRole = RoleService.getUsedRole( member );
	if ( usedRole != null )
	{
		usedRoleId = usedRole.getRoleId();
		usedRoleName = usedRole.getName();
	}
}

private void setAdminRoleId( Member newMember )
{
	Role adminRole = RoleService.getAdminRole( member );
	if ( adminRole == null )
	{
		adminRole = Role.ADMIN_NONE;
	}
	adminRoleId = adminRole.getRoleId();
	adminRoleName = adminRole.getName();
}

public java.lang.String getCity()
{
	return member.getCity();
}

public int getDashboardRowDisplay()
{
	return member.getDashboardRowDisplay();
}

public int getDealerGroupId()
{
	return member.getDealerGroupId();
}

public int[] getDealerIdArray()
{
	return member.getDealerIdArray();
}

public java.lang.String getEmailAddress()
{
	return member.getEmailAddress();
}

public String getSmsAddress()
{
	return member.getSmsAddress();
}

public java.lang.String getFirstName()
{
	return member.getFirstName();
}

public String getFullName()
{
	return getFirstName() + " " + getLastName();
}

public java.lang.String getJobTitle()
{
	if ( member.getJobTitle() != null )
	{
		return member.getJobTitle().getName();
	}
	else
	{
		return "";
	}
}

public java.lang.String getLastName()
{
	return member.getLastName();
}

public java.lang.String getLogin()
{
	return member.getLogin();
}

public String getLoginStatusFormatted()
{
	String loginStatusFormatted = "";
	if ( member.getLoginStatus() == Member.MEMBER_LOGIN_STATUS_NEW )
	{
		loginStatusFormatted = "NEW";
	}
	else if ( member.getLoginStatus() == Member.MEMBER_LOGIN_STATUS_OLD )
	{
		loginStatusFormatted = "OLD";
	}

	return loginStatusFormatted;
}

public int getLoginStatus()
{
	return member.getLoginStatus();
}

public void setLoginStatus( int newLoginStatus )
{
	member.setLoginStatus( new Integer( newLoginStatus ) );
}

public Member getMember()
{
	member.setRoles( new HashSet<Role>() );
	if ( usedRoleId != null )
	{
		member.getRoles().add(Role.getRoleById(usedRoleId));
	}
	if ( newRoleId != null )
	{
		member.getRoles().add(Role.getRoleById(newRoleId));
	}
	if ( adminRoleId != null )
	{
		member.getRoles().add(Role.getRoleById(adminRoleId));
	}
	return member;
}

public int getMemberId()
{
	if ( member.getMemberId() != null )
	{
		return member.getMemberId().intValue();
	}
	else
	{
		return 0;
	}
}

public int getMemberType()
{
	return member.getMemberType();
}

public java.lang.String getMiddleInitial()
{
	return member.getMiddleInitial();
}

public String getMobilePhoneNumber()
{
	return mobilePhoneNumber.getPhoneNumber();
}

public String getMobilePhoneNumberAreaCode()
{
	return mobilePhoneNumber.getAreaCode();
}

public String getMobilePhoneNumberPrefix()
{
	return mobilePhoneNumber.getPrefix();
}

public String getMobilePhoneNumberSuffix()
{
	return mobilePhoneNumber.getSuffix();
}

public java.lang.String getNameAndPhone()
{
	String extension = ( getOfficePhoneExtension() == null ) ? "" : " ext." + getOfficePhoneExtension();

	return member.getFirstName() + " " + member.getLastName() + " " + officePhoneNumber.getPhoneNumberFormatted() + extension;
}

public java.lang.String getNotes()
{
	return member.getNotes();
}

public String getOfficeFaxNumber()
{
	return officeFaxNumber.getPhoneNumber();
}

public String getOfficeFaxNumberAreaCode()
{
	return officeFaxNumber.getAreaCode();
}

public String getOfficeFaxNumberPrefix()
{
	return officeFaxNumber.getPrefix();
}

public String getOfficeFaxNumberSuffix()
{
	return officeFaxNumber.getSuffix();
}

public java.lang.String getOfficePhoneExtension()
{
	return member.getOfficePhoneExtension();
}

public java.lang.String getOfficePhoneNumber()
{
	return officePhoneNumber.getPhoneNumber();
}

public java.lang.String getOfficePhoneNumberFormatted()
{
	return officePhoneNumber.getAreaCode() + "-" + officePhoneNumber.getPrefix() + "-" + officePhoneNumber.getSuffix();
}

public String getOfficePhoneNumberAreaCode()
{
	return officePhoneNumber.getAreaCode();
}

public String getOfficePhoneNumberPrefix()
{
	return officePhoneNumber.getPrefix();
}

public String getOfficePhoneNumberSuffix()
{
	return officePhoneNumber.getSuffix();
}

public String getPagerNumber()
{
	return pagerNumber.getPhoneNumber();
}

public String getPagerNumberAreaCode()
{
	return pagerNumber.getAreaCode();
}

public String getPagerNumberPrefix()
{
	return pagerNumber.getPrefix();
}

public String getPagerNumberSuffix()
{
	return pagerNumber.getSuffix();
}

public String getPassword()
{
	return password;
}

public java.lang.String getPasswordConfirm()
{
	return passwordConfirm;
}

public java.lang.String getPreferredFirstName()
{
	return member.getPreferredFirstName();
}

public String getReportMethod()
{
	return member.getReportMethod();
}

public java.lang.String getSalutation()
{
	return member.getSalutation();
}

public java.lang.String getState()
{
	return member.getState();
}

public java.lang.String getStreetAddress1()
{
	return member.getStreetAddress1();
}

public java.lang.String getStreetAddress2()
{
	return member.getStreetAddress2();
}

public java.lang.String getZipcode()
{
	return member.getZipcode();
}

public boolean isAdmin()
{
	return member.isAdmin();
}

public boolean isUser()
{
	return member.isUser();
}

public boolean isAccountRep()
{
	return member.isAccountRep();
}

public boolean isCapPreferred()
{
	return member.hasReportPreference( Member.MEMBER_REPORT_PREFERENCE_CAP );
}

public boolean isCbgPreferred()
{
	return member.hasReportPreference( Member.MEMBER_REPORT_PREFERENCE_CBG );
}

public boolean isDashboardPreferred()
{
	return member.hasReportPreference( Member.MEMBER_REPORT_PREFERENCE_DASHBOARD );
}

public boolean isActiveRedistributionPreferred()
{
	return member.hasReportPreference( Member.MEMBER_REPORT_PREFERENCE_ACTIVE_REDISTRIBUTION );
}

public void setActiveRedistributionPreferred( boolean activeRedistribution )
{
	member.addReportPreference( Member.MEMBER_REPORT_PREFERENCE_ACTIVE_REDISTRIBUTION );
}

public boolean isTapPreferred()
{
	return member.hasReportPreference( Member.MEMBER_REPORT_PREFERENCE_TAP );
}

public boolean isTmgPreferred()
{
	return member.hasReportPreference( Member.MEMBER_REPORT_PREFERENCE_TMG );
}

public void setBusinessObject( Object bo )
{
	member = (Member)bo;
	officePhoneNumber = new PhoneNumberFormHelper( member.getOfficePhoneNumber() );
	officeFaxNumber = new PhoneNumberFormHelper( member.getOfficeFaxNumber() );
	mobilePhoneNumber = new PhoneNumberFormHelper( member.getMobilePhoneNumber() );
	pagerNumber = new PhoneNumberFormHelper( member.getPagerNumber() );

}

public void setCapPreferred( boolean newCapPreffered )
{
	member.addReportPreference( Member.MEMBER_REPORT_PREFERENCE_CAP );
}

public void setCbgPreferred( boolean newCbgPreffered )
{
	member.addReportPreference( Member.MEMBER_REPORT_PREFERENCE_CBG );
}

public void setCity( java.lang.String newCity )
{
	member.setCity( newCity );
}

public void setDashboardPreferred( boolean newDashboardPreffered )
{
	member.addReportPreference( Member.MEMBER_REPORT_PREFERENCE_DASHBOARD );
}

public void setDashboardRowDisplay( int newDashboardRowDisplay )
{
	member.setDashboardRowDisplay( new Integer( newDashboardRowDisplay ) );
}

public void setDealerGroupId( int newDealerGroupId )
{
	member.setDealerGroupId( newDealerGroupId );
}

public void setDealerIdArray( int[] newDealerIdArray )
{
	member.setDealerIdArray( newDealerIdArray );
}

public void setEmailAddress( java.lang.String newEmailAddress )
{
	member.setEmailAddress( newEmailAddress );
}

public void setSmsAddress( String smsAddress )
{
	member.setSmsAddress( smsAddress );
}

public void setFirstName( java.lang.String newFirstName )
{
	member.setFirstName( newFirstName );
}

public void setLastName( java.lang.String newLastName )
{
	member.setLastName( newLastName );
}

public void setLogin( java.lang.String newLogin )
{
	member.setLogin( newLogin );
}

public void setMember( com.firstlook.entity.Member newMember )
{
	member = newMember;
}

public void setMemberId( int newMemberId )
{
	member.setMemberId( new Integer( newMemberId ) );
}

public void setMemberType( int newMemberType )
{
	member.setMemberType( new Integer( newMemberType ) );
}

public void setMiddleInitial( java.lang.String newMiddleInitial )
{
	member.setMiddleInitial( newMiddleInitial );
}

public void setMobilePhoneNumberAreaCode( String newAreaCode )
{
	mobilePhoneNumber.setAreaCode( newAreaCode );
}

public void setMobilePhoneNumberPrefix( String newPrefix )
{
	mobilePhoneNumber.setPrefix( newPrefix );
}

public void setMobilePhoneNumberSuffix( String newSuffix )
{
	mobilePhoneNumber.setSuffix( newSuffix );
}

public void setNotes( java.lang.String newNotes )
{
	member.setNotes( newNotes );
}

public void setOfficeFaxNumberAreaCode( String newAreaCode )
{
	officeFaxNumber.setAreaCode( newAreaCode );
}

public void setOfficeFaxNumber( String newOfficeFaxNumber )
{
	officeFaxNumber = new PhoneNumberFormHelper( newOfficeFaxNumber );
	member.setOfficeFaxNumber( newOfficeFaxNumber );
}

public void setOfficeFaxNumberPrefix( String newPrefix )
{
	officeFaxNumber.setPrefix( newPrefix );
}

public void setOfficeFaxNumberSuffix( String newSuffix )
{
	officeFaxNumber.setSuffix( newSuffix );
}

public void setOfficePhoneNumber( String newOfficePhoneNumber )
{
	officePhoneNumber = new PhoneNumberFormHelper( newOfficePhoneNumber );
	member.setOfficePhoneNumber( newOfficePhoneNumber );
}

public void setOfficePhoneNumberExtension( String newOfficePhoneNumberExtension )
{
	if ( newOfficePhoneNumberExtension.length() > 10 )
	{
		member.setOfficePhoneExtension( newOfficePhoneNumberExtension.substring( 10 ) );
	}
	else
	{
		member.setOfficePhoneExtension( "" );
	}
}

public void setOfficePhoneExtension( String newOfficePhoneExtension )
{
	member.setOfficePhoneExtension( newOfficePhoneExtension );
}

public void setOfficePhoneNumberAreaCode( String newAreaCode )
{
	officePhoneNumber.setAreaCode( newAreaCode );
}

public void setOfficePhoneNumberPrefix( String newPrefix )
{
	officePhoneNumber.setPrefix( newPrefix );
}

public void setOfficePhoneNumberSuffix( String newSuffix )
{
	officePhoneNumber.setSuffix( newSuffix );
}

public void setPagerNumberAreaCode( String newAreaCode )
{
	pagerNumber.setAreaCode( newAreaCode );
}

public void setPagerNumberPrefix( String newPrefix )
{
	pagerNumber.setPrefix( newPrefix );
}

public void setPagerNumberSuffix( String newSuffix )
{
	pagerNumber.setSuffix( newSuffix );
}

public void setPassword( String newPassword )
{
	this.password = newPassword;
}

public void setPasswordConfirm( java.lang.String newPasswordConfirm )
{
	passwordConfirm = newPasswordConfirm;
}

public void setPreferredFirstName( String newPreferredFirstName )
{
	member.setPreferredFirstName( newPreferredFirstName );
}

public void setReportMethod( String newReportMethod )
{
	member.setReportMethod( newReportMethod );
}

public void setSalutation( String newSalutation )
{
	member.setSalutation( newSalutation );
}

public void setState( String newState )
{
	member.setState( newState );
}

public void setStreetAddress1( String newStreetAddress1 )
{
	member.setStreetAddress1( newStreetAddress1 );
}

public void setStreetAddress2( String newStreetAddress2 )
{
	member.setStreetAddress2( newStreetAddress2 );
}

public void setTapPreferred( boolean newTapPreffered )
{
	member.addReportPreference( Member.MEMBER_REPORT_PREFERENCE_TAP );
}

public void setTmgPreferred( boolean newTmgPreffered )
{
	member.addReportPreference( Member.MEMBER_REPORT_PREFERENCE_TMG );
}

public void setZipcode( String newZipcode )
{
	member.setZipcode( newZipcode );
}

public Integer getInventoryOverviewSortOrderType() {
	return member.getInventoryOverviewSortOrderType();
}

public void setInventoryOverviewSortOrderType(Integer inventoryOverviewSortOrderType) {
	member.setInventoryOverviewSortOrderType(inventoryOverviewSortOrderType);
}

public ActionErrors validate( ActionMapping mapping, HttpServletRequest request )
{
	ActionErrors errors = new ActionErrors();

	validateFirstName( errors );
	validateLastName( errors );
	validateLogin( errors );
	validatePassword( errors );
	validateOfficePhoneNumber( errors );
	validateOfficeFaxNumber( errors );
	validateMobilePhoneNumber( errors );
	validatePagerNumber( errors );
	if ( getMemberType() == Member.MEMBER_TYPE_USER )
	{
		validateReportMethod( errors );
	}

	return errors;

}

public void validateCity( ActionErrors errors )
{
	final String CITY_ERROR_MESSAGE_KEY = "error.admin.member.city";
	if ( isNullOrEmptyOrWhiteSpace( getCity() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( CITY_ERROR_MESSAGE_KEY ) );
	}
}

public void validateFirstName( ActionErrors errors )
{
	final String FIRSTNAME_ERROR_MESSAGE_KEY = "error.admin.member.firstname";
	if ( isNullOrEmptyOrWhiteSpace( getFirstName() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( FIRSTNAME_ERROR_MESSAGE_KEY ) );
	}
}

public void validateLastName( ActionErrors errors )
{
	final String LASTNAME_ERROR_MESSAGE_KEY = "error.admin.member.lastname";
	if ( isNullOrEmptyOrWhiteSpace( getLastName() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( LASTNAME_ERROR_MESSAGE_KEY ) );
	}
}

public void validateLogin( ActionErrors errors )
{
	final String LOGIN_ERROR_MESSAGE_KEY = "error.admin.member.login";
	if ( isNullOrEmptyOrWhiteSpace( getLogin() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( LOGIN_ERROR_MESSAGE_KEY ) );
	}
}

public void validateMobilePhoneNumber( ActionErrors errors )
{
	final String MOBILE_PHONE_NUMBER_ERROR_MESSAGE_KEY = "error.admin.member.mobilephonenumber";
	if ( mobilePhoneNumber.getPhoneNumber().length() > 0 )
	{
		mobilePhoneNumber.validate( errors, ActionErrors.GLOBAL_ERROR, new ActionError( MOBILE_PHONE_NUMBER_ERROR_MESSAGE_KEY ) );
	}
}

public void validateOfficeFaxNumber( ActionErrors errors )
{
	final String OFFICEFAX_ERROR_MESSAGE_KEY = "error.admin.member.officefax";
	officeFaxNumber.validate( errors, ActionErrors.GLOBAL_ERROR, new ActionError( OFFICEFAX_ERROR_MESSAGE_KEY ) );
}

public void validateOfficePhoneNumber( ActionErrors errors )
{
	final String OFFICEPHONE_ERROR_MESSAGE_KEY = "error.admin.member.officephone";
	officePhoneNumber.validate( errors, ActionErrors.GLOBAL_ERROR, new ActionError( OFFICEPHONE_ERROR_MESSAGE_KEY ) );
}

public void validatePagerNumber( ActionErrors errors )
{
	final String PAGER_NUMBER_ERROR_MESSAGE_KEY = "error.admin.member.pagernumber";
	if ( pagerNumber.getPhoneNumber().length() > 0 )
	{
		pagerNumber.validate( errors, ActionErrors.GLOBAL_ERROR, new ActionError( PAGER_NUMBER_ERROR_MESSAGE_KEY ) );
	}
}

public void validatePassword( ActionErrors errors )
{

	validateNewOrRegularPassword( errors, getPassword() );
}

public void validateNewPassword( ActionErrors errors )
{
	validateNewOrRegularPassword( errors, getNewPassword() );
}

private void validateNewOrRegularPassword( ActionErrors errors, String password )
{
	final String PASSWORD_ERROR_MESSAGE_KEY = "error.admin.member.password";
	final String PASSWORD_NOT_CONFIRMED_ERROR_MESSAGE_KEY = "error.admin.member.passwordnotconfirmed";
	final String PASSWORDCONFIRM_ERROR_MESSAGE_KEY = "error.admin.member.passwordconfirm";
	final int PASSWORD_MIN_LENGTH = 6;
	final int PASSWORD_MAX_LENGTH = 80;

	if ( isNullOrEmptyOrWhiteSpace( password ) || password.length() < PASSWORD_MIN_LENGTH || password.length() > PASSWORD_MAX_LENGTH )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( PASSWORD_ERROR_MESSAGE_KEY ) );
	}

	if ( isNullOrEmptyOrWhiteSpace( getPasswordConfirm() )
			|| getPasswordConfirm().length() < PASSWORD_MIN_LENGTH || getPasswordConfirm().length() > PASSWORD_MAX_LENGTH )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( PASSWORDCONFIRM_ERROR_MESSAGE_KEY ) );
	}

	if ( password != null && getPasswordConfirm() != null )
	{
		if ( !password.equals( getPasswordConfirm() ) )
		{
			errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( PASSWORD_NOT_CONFIRMED_ERROR_MESSAGE_KEY ) );
		}
	}
}

public void validateOldPassword( ActionErrors errors, String passwordInDatabase )
{
	final String OLD_PASSWORD_DOES_NOT_MATCH_KEY = "error.admin.member.oldpassword";

	if ( !( passwordInDatabase.equals( getOldPassword() ) ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( OLD_PASSWORD_DOES_NOT_MATCH_KEY ) );
	}
}

public void validateOldPasswordNotMatchNew( ActionErrors errors )
{
	final String OLD_PASSWORD_MATCHES_NEW_KEY = "error.admin.member.oldpasswordmatchnew";
	if ( getNewPassword().equals( getOldPassword() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( OLD_PASSWORD_MATCHES_NEW_KEY ) );
	}
}

public void validateReportMethod( ActionErrors errors )
{
	if ( isNullOrEmptyOrWhiteSpace( getReportMethod() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.member.reportmethod" ) );
	}

}

public void validateState( ActionErrors errors )
{
	final String STATE_ERROR_MESSAGE_KEY = "error.admin.member.state";
	if ( isNullOrEmptyOrWhiteSpace( getState() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( STATE_ERROR_MESSAGE_KEY ) );
	}
}

public void validateStreetAddress1( ActionErrors errors )
{
	final String STREETADDRESS1_ERROR_MESSAGE_KEY = "error.admin.member.streetaddress1";
	if ( isNullOrEmptyOrWhiteSpace( getStreetAddress1() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( STREETADDRESS1_ERROR_MESSAGE_KEY ) );
	}
}

public void validateZipcode( ActionErrors errors )
{
	final String ZIPCODE_ERROR_MESSAGE_KEY = "error.admin.member.zipcode";
	final int ZIPCODE_REQUIRED_LENGTH = 5;

	if ( isNullOrEmptyOrWhiteSpace( getZipcode() )
			|| !BaseActionForm.checkIsNumeric( getZipcode() ) || getZipcode().length() != ZIPCODE_REQUIRED_LENGTH )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( ZIPCODE_ERROR_MESSAGE_KEY ) );
	}
}

public String getSelectedAddressIndex()
{
	return selectedAddressIndex;
}

public void setSelectedAddressIndex( String selectedAddressIndex )
{
	this.selectedAddressIndex = selectedAddressIndex;
}

public String getOldPassword()
{
	return oldPassword;
}

public void setOldPassword( String oldPassword )
{
	this.oldPassword = oldPassword;
}

public String getNewPassword()
{
	return newPassword;
}

public void setNewPassword( String newPassword )
{
	this.newPassword = newPassword;
}

public boolean isGroupTradePreferred()
{
	return member.hasReportPreference( Member.MEMBER_REPORT_PREFERENCE_GROUPTRADE );
}

public boolean isStoreTradePreferred()
{
	return member.hasReportPreference( Member.MEMBER_REPORT_PREFERENCE_STORETRADE );
}

public void setGroupTradePreferred( boolean b )
{
	member.addReportPreference( Member.MEMBER_REPORT_PREFERENCE_GROUPTRADE );
}

public void setStoreTradePreferred( boolean b )
{
	member.addReportPreference( Member.MEMBER_REPORT_PREFERENCE_STORETRADE );
}

public int getUserRoleCD()
{
	return member.getUserRoleEnum().getValue();
}

public void setUserRoleCD( int pCD )
{
	member.setUserRoleEnum( UserRoleEnum.getEnum(pCD) );
}

public List<UserRoleEnum> getUserRoleEnums()
{
	return UserRoleEnum.getEnumList();
}

public UserRoleEnum getUserRoleEnum()
{
	return member.getUserRoleEnum();
}

public String getNewPasswordFormatted()
{
	return "";
}

public String getOldPasswordFormatted()
{
	return "";
}

public String getPasswordConfirmFormatted()
{
	return "";
}

public void setNewPasswordFormatted( String password )
{
	setNewPassword( password );
}

public void setOldPasswordFormatted( String password )
{
	setOldPassword( password );
}

public void setPasswordConfirmFormatted( String password )
{
	setPasswordConfirm( password );
}

public String getPasswordFormatted()
{
	return "";
}

public void setPasswordFormatted( String password )
{
	setPassword( password );
}

public String getDealerGroupName()
{
	return dealerGroupName;
}

public void setDealerGroupName( String string )
{
	dealerGroupName = string;
}

public String getGmSmartAuctionPassword() {
	return gmSmartAuctionPassword;
}

public void setGmSmartAuctionPassword(String gmSmartAuctionPassword) {
	this.gmSmartAuctionPassword = gmSmartAuctionPassword;
}

public String getGmSmartAuctionUsername() {
	return gmSmartAuctionUsername;
}

public void setGmSmartAuctionUsername(String gmSmartAuctionUsername) {
	this.gmSmartAuctionUsername = gmSmartAuctionUsername;
}

public int[] getInventoryStatusCDPresets()
{
	return inventoryStatusCDPresets; 
}

public void setInventoryStatusCDPresets( int[] inventoryStatusCDPresets )
{
	this.inventoryStatusCDPresets = inventoryStatusCDPresets;
}

public Integer getNewRoleId()
{
	return newRoleId;
}

public void setNewRoleId( Integer newRoleId )
{
	this.newRoleId = newRoleId;
}

public Integer getUsedRoleId()
{
	return usedRoleId;
}

public void setUsedRoleId( Integer usedRoleId )
{
	this.usedRoleId = usedRoleId;
}

public String getNewRoleName()
{
	return newRoleName;
}

public void setNewRoleName( String newRoleName )
{
	this.newRoleName = newRoleName;
}

public String getUsedRoleName()
{
	return usedRoleName;
}

public void setUsedRoleName( String usedRoleName )
{
	this.usedRoleName = usedRoleName;
}

public Integer getAdminRoleId()
{
	return adminRoleId;
}

public void setAdminRoleId( Integer adminRoleId )
{
	this.adminRoleId = adminRoleId;
}

public String getAdminRoleName()
{
	return adminRoleName;
}

public void setAdminRoleName( String adminRoleName )
{
	this.adminRoleName = adminRoleName;
}

public Integer getDefaultDealerGroupId()
{
	return defaultDealerGroupId;
}

public void setDefaultDealerGroupId( Integer defaultDealerGroupId )
{
	this.defaultDealerGroupId = defaultDealerGroupId;
}

public Collection<SubscriptionDisplayData> getSubscriptionData()
{
	return subscriptionData;
}

public void setSubscriptionData( Collection<SubscriptionDisplayData> subscriptionData )
{
	this.subscriptionData = subscriptionData;
}

public int getJobTitleId()
{
	return this.member.getJobTitleId().intValue();
}

public void setJobTitleId( int jobTitleId )
{
	this.member.setJobTitleId( new Integer( jobTitleId ) );
}

// this reset is called becuase when no check boxes are selected, they are not
// passed back in the form
// this means that a form can't tell you that a check box has been unselected.
// the way to handle this
// is by setting all check box controlled fields false here in the reset, which
// gets called before every
// action, and then only the checked boxes will get set to true. DW - 1/10/06
public void reset( ActionMapping mapping, HttpServletRequest request )
{
	super.reset( mapping, request );
	if ( subscriptionData != null )
	{
		Iterator<SubscriptionDisplayData> iter = subscriptionData.iterator();
		SubscriptionDisplayData subscription;
		while ( iter.hasNext() )
		{
			subscription = iter.next();
			subscription.setSelectedDealershipNames( new String[0] );
			subscription.setSelectedDeliveryFormats( new String[0] );
		}
	}
}

public Boolean getCanUseDealerCarfaxCredentials() {
	return canUseDealerCarfaxCredentials;
}

public void setCanUseDealerCarfaxCredentials(
		Boolean canUseDealerCarfaxCredentials) {
	this.canUseDealerCarfaxCredentials = canUseDealerCarfaxCredentials;
}

}
