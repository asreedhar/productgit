package com.firstlook.entity.form;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import biz.firstlook.commons.util.DateUtilFL;
import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.transact.persist.model.NADARegions;

import com.firstlook.action.BaseActionForm;
import com.firstlook.dealer.tools.GuideBook;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerFranchise;
import com.firstlook.entity.ProgramTypeEnum;
import com.firstlook.exception.ApplicationException;

public class DealerForm extends BaseActionForm
{

private static final long serialVersionUID = -8988179011258702854L;

private static final int DEFAULT_REGIONID = 0;

private Dealer dealer;
private PhoneNumberFormHelper phone;
private PhoneNumberFormHelper fax;
private java.util.Collection markets;
private int unitsInStock;
private String bookOutPreferenceName;
private String bookOutPreferenceSecondName;
private Collection auctionLocations;
private Collection<DealerFranchise> franchises;
private int[] atcAccessGroupIds;
private String guideBook2BookOutPreferenceName;
private String guideBook2SecondBookOutPreferenceName;
private String auctionAreaName;
private String lockoutPassword;
private Collection dealerFormFranchises; // temporary hack for Spring refactoring
private boolean auctionEnabled;
private boolean privateAuctions;
private int daysOut;
private int auctionDistanceFromDealership;
private int auctionAreaId;
private boolean atcEnabled;
private boolean gmacEnabled;
private boolean tfsEnabled;
private boolean bookoutLockoutEnabled;

private boolean checkAppraisalHistoryForIMPPlanning;
private boolean applyDefaultPlanToGreenLightsInIMP;

private final List<Integer> tradeManagerDaysFilterValues;

public final static String DEALER_TYPE_DESC_SELLER = "Dealer";
public final static String DEALER_TYPE_DESC_INSTITUTIONAL = "Institutional";
public final static String DEALER_TYPE_DESC_FIRSTLOOK = "First Look";
public final static String TRENDING_VIEW_TOP_SELLER = "Top Sellers";
public final static String TRENDING_VIEW_FASTEST_SELLER = "Fastest Sellers";
public final static String TRENDING_VIEW_MOST_PROFITABLE = "Most Profitable";

public DealerForm()
{
	setBusinessObject( new Dealer() );
	tradeManagerDaysFilterValues = new ArrayList<Integer>();
	for ( int daysToFilterIndex = 1; daysToFilterIndex < 6; daysToFilterIndex++ )
		tradeManagerDaysFilterValues.add( new Integer( daysToFilterIndex * 5 ) );
	 
}

public DealerForm( Dealer newDealer )
{
	setBusinessObject( newDealer );
	tradeManagerDaysFilterValues = new ArrayList<Integer>();
	for ( int daysToFilterIndex = 1; daysToFilterIndex < 6; daysToFilterIndex++ )
		tradeManagerDaysFilterValues.add( new Integer( daysToFilterIndex * 5 ) );
}

public String getActiveStatus()
{
	return dealer.isActive() ? "Active" : "Inactive";
}

public String getAddress1()
{
	return dealer.getAddress1();
}

public String getAddress2()
{
	return dealer.getAddress2();
}

public java.util.Collection getAuctionLocations()
{
	return auctionLocations;
}

public String getCity()
{
	return dealer.getCity();
}

public String getCustomHomePageMessage()
{
	String message = dealer.getCustomHomePageMessage();

	if ( message == null )
	{
		message = PropertyLoader.getProperty( "dealer.default.message" );
	}

	return message;
}

public int getDashboardColumnDisplayPreference()
{
	return dealer.getDashboardColumnDisplayPreference();
}

public Dealer getDealer()
{
	return dealer;
}

public String getDealerCode()
{
	return dealer.getDealerCode();
}

public int getDealerId()
{
	if ( dealer.getDealerId() != null )
	{
		return dealer.getDealerId().intValue();
	}
	else
	{
		return 0;
	}
}

public String getDealerLogoFileName()
{
	return dealer.getDealerCode() + ".gif";
}

public int getDealerType()
{
	return dealer.getDealerType();
}

public String getDealerTypeDescription()
{
	String desc = "N/A";
	if ( getDealerType() == Dealer.DEALER_TYPE_SELLER )
	{
		desc = DealerForm.DEALER_TYPE_DESC_SELLER;
	}
	if ( getDealerType() == Dealer.DEALER_TYPE_INSTITUTIONAL )
	{
		desc = DealerForm.DEALER_TYPE_DESC_INSTITUTIONAL;
	}
	if ( getDealerType() == Dealer.DEALER_TYPE_FIRSTLOOK )
	{
		desc = DealerForm.DEALER_TYPE_DESC_FIRSTLOOK;
	}

	return desc;
}

public int getDefaultForecastingWeeks()
{
	return dealer.getDefaultForecastingWeeks();
}

public int getDefaultTrendingView()
{
	return dealer.getDefaultTrendingView();
}

public int getDefaultTrendingWeeks()
{
	return dealer.getDefaultTrendingWeeks();
}

public String getFaxAreaCode()
{
	return fax.getAreaCode();
}

public String getFaxPrefix()
{
	return fax.getPrefix();
}

public String getFaxSuffix()
{
	return fax.getSuffix();
}

public int getGuideBookId()
{
	return dealer.getGuideBookId();
}

public int getGuideBook2Id()
{
	return dealer.getGuideBook2Id();
}

public String getGuideBookName()
{
	return GuideBook.getTitle( dealer.getGuideBookId() );
}

public String getGuideBook2Name()
{
	return GuideBook.getTitle( dealer.getGuideBook2Id() );
}

public java.util.Collection getMarkets()
{
	return markets;
}

public int getNadaRegionCode()
{
	return dealer.getNadaRegionCode();
}

public void setNadaRegionCode( int nadaRegionCode )
{
	dealer.getDealerPreference().setNadaRegionCode( nadaRegionCode );
}

public String getNadaRegionCodeString()
{
	return NADARegions.getNADARegionString(getNadaRegionCode());
}

public String getName()
{
	return dealer.getName();
}

public String getNickname()
{
	return dealer.getNickname();
}

public String getNicknameCaps()
{
	return dealer.getNickname().toUpperCase();
}

public String getOfficeFaxNumber()
{
	return fax.getPhoneNumber();
}

public String getOfficeFaxNumberFormatted()
{
	return fax.getPhoneNumberFormatted();
}

public String getOfficePhoneNumber()
{
	return phone.getPhoneNumber();
}

public String getOfficePhoneNumberFormatted()
{
	return phone.getPhoneNumberFormatted();
}

public String getPhoneAreaCode()
{
	return phone.getAreaCode();
}

public int getPackAmount()
{
	return dealer.getPackAmount();
}

public int[] getDealerFranchiseIds()
{
	int size = getFranchises().size();
	int[] ids = new int[size];
	DealerFranchise[] franchises = (DealerFranchise[])getFranchises().toArray( new DealerFranchise[size] );
	for ( int i = 0; i < size; i++ )
	{
		ids[i] = franchises[i].getFranchiseId();
	}
	return ids;

}

public void setPackAmount( int packAmount )
{
	dealer.setPackAmount( packAmount );
}

public String getPhonePrefix()
{
	return phone.getPrefix();
}

public String getPhoneSuffix()
{
	return phone.getSuffix();
}

public String getState()
{
	return dealer.getState();
}

public String getTrendingViewDescription()
{
	String desc = "N/A";

	if ( getDefaultTrendingView() == Dealer.TRENDING_VIEW_FASTEST_SELLER )
	{
		desc = DealerForm.TRENDING_VIEW_FASTEST_SELLER;
	}
	if ( getDefaultTrendingView() == Dealer.TRENDING_VIEW_MOST_PROFITABLE )
	{
		desc = DealerForm.TRENDING_VIEW_MOST_PROFITABLE;
	}
	if ( getDefaultTrendingView() == Dealer.TRENDING_VIEW_TOP_SELLER )
	{
		desc = DealerForm.TRENDING_VIEW_TOP_SELLER;
	}
	return desc;
}

public int getUnitsInStock()
{
	return unitsInStock;
}

public int getUnitsSoldThreshold13Wks()
{
	return dealer.getUnitsSoldThreshold13Wks();
}

public int getUnitsSoldThreshold26Wks()
{
	return dealer.getUnitsSoldThreshold26Wks();
}

public int getUnitsSoldThreshold4Wks()
{
	return dealer.getUnitsSoldThreshold4Wks();
}

public int getUnitsSoldThreshold52Wks()
{
	return dealer.getUnitsSoldThreshold52Wks();
}

public int getUnitsSoldThreshold8Wks()
{
	return dealer.getUnitsSoldThreshold8Wks();
}

public String getZipcode()
{
	return dealer.getZipcode();
}

public boolean isActive()
{
	return dealer.isActive();
}

public boolean isAverageMileageDisplayPreference()
{
	if ( getDashboardColumnDisplayPreference() == Dealer.DASHBOARD_DISPLAY_AVERAGE_MILEAGE )
	{
		return true;
	}
	else
	{
		return false;
	}
}

public boolean isDealerNameMasked()
{
	return dealer.isDealerNameMasked();
}

public boolean isDealerTypeInstitutional()
{
	return dealer.getDealerType() == Dealer.DEALER_TYPE_INSTITUTIONAL;
}

public boolean isUnitsInStockDisplayPreference()
{
	if ( getDashboardColumnDisplayPreference() == Dealer.DASHBOARD_DISPLAY_UNITS_IN_STOCK )
	{
		return true;
	}
	else
	{
		return false;
	}
}

public void setActive( boolean newActive )
{
	dealer.setActive( newActive );
}

public void setAddress1( String newAddress1 )
{
	dealer.setAddress1( newAddress1 );
}

public void setAddress2( String newAddress2 )
{
	dealer.setAddress2( newAddress2 );
}

public void setAuctionLocations( java.util.Collection newAuctionLocations )
{
	auctionLocations = newAuctionLocations;
}

public void setBusinessObject( Object bo )
{
	dealer = (Dealer)bo;
	phone = new PhoneNumberFormHelper( dealer.getOfficePhoneNumber() );
	fax = new PhoneNumberFormHelper( dealer.getOfficeFaxNumber() );
}

public void setCity( String newCity )
{
	dealer.setCity( newCity );
}

public void setCustomHomePageMessage( String newCustomHomePageMessage )
{
	dealer.setCustomHomePageMessage( newCustomHomePageMessage );
}

public void setDashboardColumnDisplayPreference( int newDashboardColumnDisplayPreference )
{
	dealer.setDashboardColumnDisplayPreference( newDashboardColumnDisplayPreference );
}

public void setDealer( Dealer newDealer )
{
	dealer = newDealer;
}

public void setDealerCode( String newDealerCode )
{
	dealer.setDealerCode( newDealerCode );
}

public void setDealerId( int newDealerId )
{
	dealer.setDealerId( new Integer( newDealerId ) );
}

public void setDealerNameMasked( boolean newDealerNameMasked )
{
	dealer.setDealerNameMasked( newDealerNameMasked );
}

public void setDealerType( int newDealerType )
{
	dealer.setDealerType( newDealerType );
}

public void setDefaultForecastingWeeks( int value )
{
	dealer.setDefaultForecastingWeeks( value );
}

public void setDefaultTrendingView( int value )
{
	dealer.setDefaultTrendingView( value );
}

public void setDefaultTrendingWeeks( int value )
{
	dealer.setDefaultTrendingWeeks( value );
}

public void setFaxAreaCode( String newFaxAreaCode )
{
	fax.setAreaCode( newFaxAreaCode );
}

public void setFaxPrefix( String newFaxPrefix )
{
	fax.setPrefix( newFaxPrefix );
}

public void setFaxSuffix( String newFaxSuffix )
{
	fax.setSuffix( newFaxSuffix );
}

public void setGuideBookId( int newGuideBookId )
{
	dealer.setGuideBookId( newGuideBookId );
}

public void setGuideBook2Id( int newGuideBookId )
{

	if( newGuideBookId == 0 )
	{
		dealer.setGuideBook2Id( null );
	}
	else
	{
		dealer.setGuideBook2Id( new Integer( newGuideBookId ) );
	}
}

public void setMarkets( java.util.Collection newMarkets )
{
	markets = newMarkets;
}

public void setName( String newName )
{
	dealer.setName( newName );
}

public void setNickname( String newNickname )
{
	dealer.setNickname( newNickname );
}

public void setOfficeFaxNumber( String newOfficeFaxNumber )
{
	dealer.setOfficeFaxNumber( newOfficeFaxNumber );
}

public void setOfficePhoneNumber( String newOfficePhoneNumber )
{
	dealer.setOfficePhoneNumber( newOfficePhoneNumber );
}

public void setPhoneAreaCode( String newPhoneAreaCode )
{
	phone.setAreaCode( newPhoneAreaCode );
}

public void setPhonePrefix( String newPhonePrefix )
{
	phone.setPrefix( newPhonePrefix );
}

public void setPhoneSuffix( String newPhoneSuffix )
{
	phone.setSuffix( newPhoneSuffix );
}

public void setState( String newState )
{
	dealer.setState( newState );
}

public void setUnitsInStock( int newUnitsInStock )
{
	unitsInStock = newUnitsInStock;
}

public void setUnitsSoldThreshold13Wks( int newUnitsSoldThreshold13Wks )
{
	dealer.setUnitsSoldThreshold13Wks( newUnitsSoldThreshold13Wks );
}

public void setUnitsSoldThreshold26Wks( int newUnitsSoldThreshold26Wks )
{
	dealer.setUnitsSoldThreshold26Wks( newUnitsSoldThreshold26Wks );
}

public void setUnitsSoldThreshold4Wks( int newUnitsSoldThreshold4Wks )
{
	dealer.setUnitsSoldThreshold4Wks( newUnitsSoldThreshold4Wks );
}

public void setUnitsSoldThreshold52Wks( int newUnitsSoldThreshold52Wks )
{
	dealer.setUnitsSoldThreshold52Wks( newUnitsSoldThreshold52Wks );
}

public void setUnitsSoldThreshold8Wks( int newUnitsSoldThreshold8Wks )
{
	dealer.setUnitsSoldThreshold8Wks( newUnitsSoldThreshold8Wks );
}

public void setZipcode( String newZipcode )
{
	dealer.setZipcode( newZipcode );
}

public ActionErrors validate( ActionMapping mapping, HttpServletRequest request )
{
	ActionErrors errors = new ActionErrors();
	if ( isNullOrEmptyOrWhiteSpace( this.getName() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealer.name" ) );
	}
	if ( isNullOrEmptyOrWhiteSpace( this.getNickname() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealer.nickname" ) );
	}
	if ( isNullOrEmptyOrWhiteSpace( this.getAddress1() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealer.address1" ) );
	}
	if ( isNullOrEmptyOrWhiteSpace( this.getCity() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealer.city" ) );
	}
	if ( isNullOrEmptyOrWhiteSpace( this.getState() ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealer.state" ) );
	}
	if ( getInceptionDate() == null )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealer.inceptiondate" ) );
	}
	else if ( getInceptionDate().before( new Date( 0 ) ) )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealer.inceptiondate.before.firstlook" ) );
	}

	validateZipcode( errors, this.getZipcode() );

	fax.validate( errors, ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealer.officefaxnumber" ) );
	phone.validate( errors, ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealer.officephonenumber" ) );

	if ( getFranchises() == null || getFranchises().size() < 1 )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealer.franchise.required" ) );
	}
	return errors;
}

public ActionErrors validatePreferences()
{
	ActionErrors errors = new ActionErrors();

	validateAgeBandTarget( errors );
	validateDaysSupplyWeight( errors );
	validateGuideBookPreferences( errors, getBookOutPreferenceId(), getGuideBookId() );
	validateDefaultAuctionRegion( errors );

	if ( getBookOutPreferenceSecondId() > 0 )
	{
		validateGuideBookPreferences( errors, getBookOutPreferenceSecondId(), getGuideBookId() );
	}

	validateGuideBookPreferenceNotTheSame( errors );

	if ( errors.isEmpty() )
	{
		if ( getGuideBook2Id() > 0 )
		{
			validateGuideBookIdsNotTheSame( errors );
			if ( errors.isEmpty() )
			{
				validateGuideBookPreferences( errors, getGuideBook2BookOutPreferenceId(), getGuideBook2Id() );
				if ( getGuideBook2SecondBookOutPreferenceId() > 0 )
				{
					validateGuideBookPreferences( errors, getGuideBook2SecondBookOutPreferenceId(), getGuideBook2Id() );
				}
			}
		}
	}

	return errors;
}

private void validateDefaultAuctionRegion( ActionErrors errors )
{
	if ( getAuctionAreaId() == DEFAULT_REGIONID )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealer.auctionregionid" ) );
	}
}

void validateGuideBookPreferences( ActionErrors errors, int bookOutPreferenceId, int guideBookId )
{
	HashMap<Integer, int[]> map = com.firstlook.entity.GuideBook.guideBookPreferenceMap();

	int[] guideBookPreference = map.get( Integer.valueOf( guideBookId ) );

	boolean flag = false;
	for ( int i = 0; i < guideBookPreference.length; i++ )
	{
		if ( bookOutPreferenceId == guideBookPreference[i] )
		{
			flag = true;
			break;
		}
	}

	if ( !flag )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealer.guidebookpreferences" ) );
	}
}

void validateGuideBookPreferenceNotTheSame( ActionErrors errors )
{
	if ( getBookOutPreferenceSecondId() > 0 )
	{
		if ( getBookOutPreferenceId() == getBookOutPreferenceSecondId() )
		{
			errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealer.guidebookpreferencessame" ) );
		}
	}
	if ( getGuideBook2SecondBookOutPreferenceId() > 0 )
	{
		if ( getGuideBook2BookOutPreferenceId() == getGuideBook2SecondBookOutPreferenceId() )
		{
			errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealer.guidebookpreferencessame" ) );
		}
	}
}

void validateGuideBookIdsNotTheSame( ActionErrors errors )
{
	if ( getGuideBookId() > 0 && getGuideBook2Id() > 0 )
	{
		if ( getGuideBookId() == getGuideBook2Id() )
		{
			errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealer.guidebookssame" ) );
		}
	}
}

void validateDaysSupplyWeight( ActionErrors errors )
{
	int daysSupplyTotal = getDaysSupply12WeekWeight() + getDaysSupply26WeekWeight();

	if ( daysSupplyTotal != 100 )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealer.dayssupplyweight" ) );
	}
}

void validateAgeBandTarget( ActionErrors errors )
{
	int targetTotal = getAgeBandTarget1()
			+ getAgeBandTarget2() + getAgeBandTarget3() + getAgeBandTarget4() + getAgeBandTarget5() + getAgeBandTarget6();

	if ( targetTotal != 100 )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( "error.admin.dealer.agebandtarget" ) );
	}
}

public void validateZipcode( ActionErrors errors, String zipcode )
{
	final String ZIPCODE_ERROR_MESSAGE_KEY = "error.admin.dealer.zipcode";

	if ( !checkIsNumeric( zipcode ) || zipcode.length() != 5 )
	{
		errors.add( ActionErrors.GLOBAL_ERROR, new ActionError( ZIPCODE_ERROR_MESSAGE_KEY ) );
	}
}

public java.util.Date getInceptionDate()
{
	return dealer.getInceptionDate();
}

public String getInceptionDateFormatted()
{
	if ( dealer.getInceptionDate() != null )
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd" );

		return dateFormat.format( dealer.getInceptionDate() );
	}
	return "";
}

public void setInceptionDate( java.util.Date date )
{
	dealer.setInceptionDate( date );
}

public void setInceptionDateFormatted( String date )
{
	SimpleDateFormat format = new SimpleDateFormat( "yyyy-MM-dd" );

	try
	{
		dealer.setInceptionDate( format.parse( date ) );
	}
	catch ( ParseException e )
	{
	}
}

public boolean isAgingInventoryTrackingDisplayPref()
{
	return dealer.isAgingInventoryTrackingDisplayPref();
}

public void setAgingInventoryTrackingDisplayPref( boolean b )
{
	dealer.setAgingInventoryTrackingDisplayPref( b );
}

public boolean isStockOrVinPreference()
{
	return dealer.isStockOrVinPreference();
}

public void setStockOrVinPreference( boolean stockOrVinPreference )
{
	dealer.setStockOrVinPreference( stockOrVinPreference );
}

public boolean isListPricePreference()
{
	return dealer.isListPricePreference();
}

public void setListPricePreference( boolean unitCostOrListPricePreference )
{
	dealer.setListPricePreference( unitCostOrListPricePreference );
}

public int getAgeBandTarget1()
{
	return dealer.getAgeBandTarget1();
}

public int getAgeBandTarget2()
{
	return dealer.getAgeBandTarget2();
}

public int getAgeBandTarget3()
{
	return dealer.getAgeBandTarget3();
}

public int getAgeBandTarget4()
{
	return dealer.getAgeBandTarget4();
}

public int getAgeBandTarget5()
{
	return dealer.getAgeBandTarget5();
}

public int getAgeBandTarget6()
{
	return dealer.getAgeBandTarget6();
}

public void setAgeBandTarget1( int i )
{
	dealer.setAgeBandTarget1( i );
}

public void setAgeBandTarget2( int i )
{
	dealer.setAgeBandTarget2( i );
}

public void setAgeBandTarget3( int i )
{
	dealer.setAgeBandTarget3( i );
}

public void setAgeBandTarget4( int i )
{
	dealer.setAgeBandTarget4( i );
}

public void setAgeBandTarget5( int i )
{
	dealer.setAgeBandTarget5( i );
}

public void setAgeBandTarget6( int i )
{
	dealer.setAgeBandTarget6( i );
}

public void setDaysSupply12WeekWeight( int i )
{
	dealer.setDaysSupply12WeekWeight( i );
}

public void setDaysSupply26WeekWeight( int i )
{
	dealer.setDaysSupply26WeekWeight( i );
}

public int getDaysSupply12WeekWeight()
{
	return dealer.getDaysSupply12WeekWeight();
}

public int getDaysSupply26WeekWeight()
{
	return dealer.getDaysSupply26WeekWeight();
}

public void setBookOutPreference( boolean bookOut )
{
	dealer.setBookOutPreference( bookOut );
}

public boolean isBookOutPreference()
{
	return dealer.getDealerPreference().isBookOut();
}

public int getBookOutPreferenceId()
{
	return dealer.getBookOutPreferenceId();
}

public void setBookOutPreferenceId( int id )
{
	dealer.setBookOutPreferenceId( id );
}

public int getBookOutPreferenceSecondId()
{
	return dealer.getBookOutPreferenceSecondId();
}

public void setBookOutPreferenceSecondId( int id )
{
	dealer.setBookOutPreferenceSecondId( id );
}

public String getBookOutPreferenceName()
{
	return bookOutPreferenceName;
}

public void setBookOutPreferenceName( String name )
{
	bookOutPreferenceName = name;
}

public String getBookOutPreferenceSecondName()
{
	return bookOutPreferenceSecondName;
}

public Integer getSellThroughRate()
{
	return dealer.getDealerPreference().getSellThroughRate();
}

public void setSellThroughRate( Integer rate )
{
	dealer.getDealerPreference().setSellThroughRate( rate );
}

public Boolean getIncludeBackEndInValuation()
{
	return dealer.getDealerPreference().getIncludeBackEndInValuation();
}

public void setIncludeBackEndInValuation( Boolean include )
{
	dealer.getDealerPreference().setIncludeBackEndInValuation( include );
}

public void setBookOutPreferenceSecondName( String string )
{
	bookOutPreferenceSecondName = string;
}

public PhoneNumberFormHelper getPhone()
{
	return phone;
}

public void setFranchises( Collection<DealerFranchise> collection )
{
	franchises = collection;
}

public void setFranchisesById( String[] franchiseIds )
{
	franchises = new ArrayList<DealerFranchise>();
	int size = franchiseIds == null ? 0 : franchiseIds.length;

	for ( int i = 0; i < size; i++ )
	{
		DealerFranchise dealerFranchise = new DealerFranchise();
		dealerFranchise.setFranchiseId( new Integer( franchiseIds[i] ).intValue() );
		franchises.add( dealerFranchise );
	}
}

public String[] getFranchisesById()
{
	if ( franchises != null && franchises.size() > 0 )
	{
		String[] franchiseIds = new String[franchises.size()];
		Iterator dealerFranchises = franchises.iterator();

		for ( int i = 0; dealerFranchises.hasNext(); i++ )
		{
			DealerFranchise franchise = (DealerFranchise)dealerFranchises.next();
			franchiseIds[i] = Integer.toString( franchise.getFranchiseId() );
		}
		return franchiseIds;
	}
	else
	{
		return null;
	}
}

public void setPhone( PhoneNumberFormHelper helper )
{
	phone = helper;
}

public Collection<DealerFranchise> getFranchises()
{
	return franchises;
}

public Collection getDealerFormFranchises() throws ApplicationException
{
	return dealerFormFranchises;
}

public void setDealerFormFranchises( Collection dealerFormFranchises )
{
	this.dealerFormFranchises = dealerFormFranchises;
}

public int getRunDayOfWeek()
{
	return dealer.getDealerPreference().getRunDayOfWeek();
}

public void setRunDayOfWeek( int runDayOfWeek )
{
	dealer.getDealerPreference().setRunDayOfWeek( runDayOfWeek );
}

public int getUnitCostThresholdLower()
{
	return dealer.getDealerPreference().getUnitCostThresholdLowerAsInt();
}

public void setUnitCostThresholdLower( int unitCostThresholdLower )
{
	dealer.getDealerPreference().setUnitCostThresholdLower( new Integer(unitCostThresholdLower) );
}

public int getUnitCostThresholdUpper()
{
	return dealer.getDealerPreference().getUnitCostThresholdUpperAsInt();
}

public void setUnitCostThresholdUpper( int unitCostThresholdUpper )
{
	dealer.getDealerPreference().setUnitCostThresholdUpper( new Integer(unitCostThresholdUpper) );
}

public void setFeGrossProfitThreshold( int threshold )
{
	dealer.getDealerPreference().setFeGrossProfitThreshold( new Integer(threshold) );
}

public int getFeGrossProfitThreshold()
{
	return dealer.getDealerPreference().getFeGrossProfitThresholdAsInt();
}

public void setMarginPercentile( int percentile )
{
	dealer.getDealerPreference().setMarginPercentile( new Integer(percentile ));
}

public int getMarginPercentile()
{
	return dealer.getDealerPreference().getMarginPercentileAsInt();
}

public void setDaysToSalePercentile( int percentile )
{
	dealer.getDealerPreference().setDaysToSalePercentile( new Integer(percentile ));
}

public int getDaysToSalePercentile()
{
	return dealer.getDealerPreference().getDaysToSalePercentileAsInt();
}

public int getGuideBook2BookOutPreferenceId()
{
	return dealer.getGuideBook2BookOutPreferenceId();
}

public void setGuideBook2BookOutPreferenceId( int id )
{
	dealer.setGuideBook2BookOutPreferenceId( id );
}

public int getGuideBook2SecondBookOutPreferenceId()
{
	return dealer.getGuideBook2SecondBookOutPreferenceId();
}

public void setGuideBook2SecondBookOutPreferenceId( int id )
{
	dealer.setGuideBook2SecondBookOutPreferenceId( id );
}

public String getGuideBook2BookOutPreferenceName()
{
	return guideBook2BookOutPreferenceName;
}

public void setGuideBook2BookOutPreferenceName( String name )
{
	guideBook2BookOutPreferenceName = name;
}

public String getGuideBook2SecondBookOutPreferenceName()
{
	return guideBook2SecondBookOutPreferenceName;
}

public void setGuideBook2SecondBookOutPreferenceName( String string )
{
	guideBook2SecondBookOutPreferenceName = string;
}

public int getProgramTypeCD()
{
	return dealer.getProgramTypeCD();
}

public void setProgramTypeCD( int i )
{
	dealer.setProgramTypeCD( i );
}

public ProgramTypeEnum getProgramTypeEnum()
{
	return dealer.getProgramTypeEnum();
}

public String getAuctionAreaName()
{
	return auctionAreaName;
}


public int getAuctionAreaId()
{
    return auctionAreaId;
}

public void setAuctionAreaId( int i )
{
    this.auctionAreaId = i;
}

public void setAuctionAreaName( String string )
{
	auctionAreaName = string;
}

public void setUnitCostThreshold( int threshold )
{
	dealer.getDealerPreference().setUnitCostThreshold( new Integer( threshold ) );
}

public int getUnitCostThreshold()
{
	if(dealer.getDealerPreference().getUnitCostThreshold() == null)
		return 0;
	
	return dealer.getDealerPreference().getUnitCostThreshold().intValue();
}

public void setUnitCostUpdateOnSale( boolean update )
{
	dealer.getDealerPreference().setUnitCostUpdateOnSale( update );
}

public boolean isUnitCostUpdateOnSale()
{
	return dealer.getDealerPreference().isUnitCostUpdateOnSale();
}

public void setUnwindDaysThreshold( int threshold )
{
	dealer.getDealerPreference().setUnwindDaysThreshold( new Integer( threshold ) );
}

public int getUnwindDaysThreshold()
{
	return dealer.getDealerPreference().getUnwindDaysThreshold().intValue();
}

public boolean isShowLotLocationStatus()
{
	return dealer.getDealerPreference().isShowLotLocationStatus();
}

public void setShowLotLocationStatus( boolean lotLocationStatus )
{
	dealer.getDealerPreference().setShowLotLocationStatus( new Boolean(lotLocationStatus) );
}

public Integer getTradeManagerDaysFilter()
{
	return dealer.getDealerPreference().getTradeManagerDaysFilter();
}

public void setTradeManagerDaysFilter( Integer tradeManagerDaysFilter )
{
	dealer.getDealerPreference().setTradeManagerDaysFilter( tradeManagerDaysFilter );
}

public int getUnitsSoldThresholdInvOverview()
{
	return dealer.getDealerPreference().getUnitsSoldThresholdInvOverviewAsInt();
}

public void setUnitsSoldThresholdInvOverview( int i )
{
	dealer.getDealerPreference().setUnitsSoldThresholdInvOverview( new Integer(i) );
}

public int getAverageInventoryAgeRedThreshold()
{
	return dealer.getDealerPreference().getAverageInventoryAgeRedThresholdAsInt();
}

public void setAverageInventoryAgeRedThreshold( int i )
{
	dealer.getDealerPreference().setAverageInventoryAgeRedThreshold( new Integer(i ));
}

public int getAverageDaysSupplyRedThreshold()
{
	return dealer.getDealerPreference().getAverageDaysSupplyRedThresholdAsInt();
}

public void setAverageDaysSupplyRedThreshold( int i )
{
	dealer.getDealerPreference().setAverageDaysSupplyRedThreshold( new Integer(i) );
}

public double getVehicleSaleThresholdForCoreMarketPenetration()
{
	return dealer.getDealerPreference().getVehicleSaleThresholdForCoreMarketPenetrationAsDouble();
}

public void setVehicleSaleThresholdForCoreMarketPenetration( double d )
{
	dealer.getDealerPreference().setVehicleSaleThresholdForCoreMarketPenetration(new Double( d ));
}

public boolean isAtcEnabled()
{
    return atcEnabled;
}

public void setAtcEnabled( boolean include )
{
    this.atcEnabled = include;
}

public boolean isApplyPriorAgingNotes()
{
	return dealer.getDealerPreference().isApplyPriorAgingNotes();
}

public void setApplyPriorAgingNotes( boolean applyNotes )
{
	dealer.getDealerPreference().setApplyPriorAgingNotes( applyNotes );
}

public int[] getAtcAccessGroupIds()
{
	return atcAccessGroupIds;
}

public void setAtcAccessGroupIds( int[] is )
{
	atcAccessGroupIds = is;
}

public Integer getPurchasingDistanceFromDealer()
{
	return dealer.getDealerPreference().getPurchasingDistanceFromDealer();
}

public void setPurchasingDistanceFromDealer( Integer purchasingDistanceFromDealer )
{
	this.dealer.getDealerPreference().setPurchasingDistanceFromDealer( purchasingDistanceFromDealer );
}

public Integer getLiveAuctionDistanceFromDealer()
{
	return dealer.getDealerPreference().getLiveAuctionDistanceFromDealer();
}

public void setLiveAuctionDistanceFromDealer( Integer liveAuctionDistanceFromDealer )
{
	this.dealer.getDealerPreference().setLiveAuctionDistanceFromDealer(liveAuctionDistanceFromDealer);
}

public boolean isDisplayUnitCostToDealerGroup()
{
	return dealer.getDealerPreference().isDisplayUnitCostToDealerGroup();
}

public void setDisplayUnitCostToDealerGroup( boolean displayUnitCostToDealerGroup )
{
	this.dealer.getDealerPreference().setDisplayUnitCostToDealerGroup( displayUnitCostToDealerGroup );
}

public int getSearchInactiveInventoryDaysBackThreshold()
{
	return dealer.getDealerPreference().getSearchInactiveInventoryDaysBackThreshold();
}

public void setSearchInactiveInventoryDaysBackThreshold( int searchInactiveInventoryDaysBackThreshold )
{
	this.dealer.getDealerPreference().setSearchInactiveInventoryDaysBackThreshold( searchInactiveInventoryDaysBackThreshold );
}

public int getFlashLocatorHideUnitCostDays()
{
	return dealer.getDealerPreference().getFlashLocatorHideUnitCostDays();
}

public void setFlashLocatorHideUnitCostDays( int flashLocatorHideUnitCostDays )
{
	this.dealer.getDealerPreference().setFlashLocatorHideUnitCostDays( flashLocatorHideUnitCostDays );
}

public boolean isAuctionEnabled()
{
	return auctionEnabled;
}

public void setAuctionEnabled( boolean auctionEnabled )
{
	this.auctionEnabled = auctionEnabled;
}

public int getDaysOut()
{
	return daysOut;
}

public String getDaysOutFormatted()
{
	return DateUtilFL.getFormattedDatePeriodBasedOnDays( this.daysOut );
}

public void setDaysOut( int daysOut )
{
	this.daysOut = daysOut;
}

public boolean isPrivateAuctions()
{
	return privateAuctions;
}

public void setPrivateAuctions( boolean privateAuctions )
{
	this.privateAuctions = privateAuctions;
}

public int getAuctionDistanceFromDealership()
{
	return auctionDistanceFromDealership;
}

public void setAuctionDistanceFromDealership( int auctionDistanceFromDealership )
{
	this.auctionDistanceFromDealership = auctionDistanceFromDealership;
}

public boolean isCalculateAverageBookValue()
{
	return dealer.getDealerPreference().isCalculateAverageBookValue();
}

public void setCalculateAverageBookValue( boolean calculateAverageBookValue )
{
	dealer.getDealerPreference().setCalculateAverageBookValue( calculateAverageBookValue );
}

public String getLockoutPassword()
{
	return lockoutPassword;
}

public void setLockoutPassword( String newPass )
{
	this.lockoutPassword = newPass;
}

public boolean isGmacEnabled()
{
	return gmacEnabled;
}

public void setGmacEnabled( boolean gmacEnabled )
{
	this.gmacEnabled = gmacEnabled;
}

public boolean isTfsEnabled()
{
	return tfsEnabled;
}

public void setTfsEnabled( boolean tfsEnabled )
{
	this.tfsEnabled = tfsEnabled;
}

public List getTradeManagerDaysFilterValues()
{
	return tradeManagerDaysFilterValues;
}

public Integer getShowroomDaysFilter()
{
	return dealer.getDealerPreference().getShowroomDaysFilter();
}

public void setShowroomDaysFilter( Integer newShowroomDays )
{
	dealer.getDealerPreference().setShowroomDaysFilter( newShowroomDays );
}

public Integer getAppraisalRequirementLevel()
{
	return dealer.getDealerPreference().getAppraisalRequirementLevel();
}

public void setAppraisalRequirementLevel( Integer newAppraisalRequirementLevel )
{
	dealer.getDealerPreference().setAppraisalRequirementLevel( newAppraisalRequirementLevel );
}

public boolean isRequireNameOnAppraisals() {
	return dealer.getDealerPreference().getRequireNameOnAppraisals();
}

public void setRequireNameOnAppraisals(boolean requireNameOnAppraisals) {
	dealer.getDealerPreference().setRequireNameOnAppraisals(requireNameOnAppraisals);
}

public boolean isRequireEstReconCostOnAppraisals() {
	return dealer.getDealerPreference().getRequireEstReconCostOnAppraisals();
}

public boolean isRequireReconNotesOnAppraisals() {
	return dealer.getDealerPreference().getRequireReconNotesOnAppraisals();
}

public void setRequireEstReconCostOnAppraisals( boolean requireEstReconCostOnAppraisals ) {
	dealer.getDealerPreference().setRequireEstReconCostOnAppraisals( requireEstReconCostOnAppraisals );
}

public void setRequireReconNotesOnAppraisals( boolean requireReconNotesOnAppraisals ) {
	dealer.getDealerPreference().setRequireReconNotesOnAppraisals( requireReconNotesOnAppraisals );
}

public boolean getCheckAppraisalHistoryForIMPPlanning()
{
	return checkAppraisalHistoryForIMPPlanning;
}

public void setCheckAppraisalHistoryForIMPPlanning( boolean checkAppraisalHistoryForIMPPlanning)
{
	this.checkAppraisalHistoryForIMPPlanning = checkAppraisalHistoryForIMPPlanning;
}

public boolean getApplyDefaultPlanToGreenLightsInIMP()
{
	return applyDefaultPlanToGreenLightsInIMP;
}

public void setApplyDefaultPlanToGreenLightsInIMP( boolean applyDefaultPlanToGreenLightsInIMP)
{
	this.applyDefaultPlanToGreenLightsInIMP = applyDefaultPlanToGreenLightsInIMP;
}

public boolean isBookoutLockoutEnabled()
{
	return bookoutLockoutEnabled;
}

public void setBookoutLockoutEnabled( boolean bookoutLockoutEnabled )
{
	this.bookoutLockoutEnabled = bookoutLockoutEnabled;
}

public Integer getRedistributionDealerDistance() {
	return dealer.getRedistributionDealerDistance();
}

public void setRedistributionDealerDistance(Integer redistributionDealerDistance) {
	dealer.setRedistributionDealerDistance(redistributionDealerDistance);
}

public Integer getRedistributionNumTopDealers() {
	return dealer.getRedistributionNumTopDealers();
}

public void setRedistributionNumTopDealers(Integer redistributionNumTopDealers) {
	dealer.setRedistributionNumTopDealers(redistributionNumTopDealers);
}

public Integer getRedistributionROI() {
	return dealer.getRedistributionROI();
}

public void setRedistributionROI(Integer redistributionRoi) {
	dealer.setRedistributionROI(redistributionRoi);
}

public Integer getRedistributionUnderstock() {
	return dealer.getRedistributionUnderstock();
}

public void setRedistributionUnderstock(Integer redistributionUnderstock) {
	dealer.setRedistributionUnderstock(redistributionUnderstock);
}

public String toString() {
	return ReflectionToStringBuilder.reflectionToString( this, ToStringStyle.MULTI_LINE_STYLE );
}

}
