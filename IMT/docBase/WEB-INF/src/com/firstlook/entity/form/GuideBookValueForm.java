package com.firstlook.entity.form;

import com.firstlook.dealer.tools.GuideBookReturnValue;
import com.firstlook.util.Formatter;

public class GuideBookValueForm extends com.firstlook.action.BaseActionForm
{

private static final long serialVersionUID = -1531916799423670068L;
private GuideBookReturnValue guideBookValue;

public GuideBookValueForm()
{
    setBusinessObject(new GuideBookReturnValue());
}

protected GuideBookReturnValue getGuideBookValue()
{
    return guideBookValue;
}

public java.lang.String getTitle()
{
    return guideBookValue.getTitle();
}

public java.lang.Integer getValue()
{
    return guideBookValue.getValue(); // KL - if this getter is called, needs
    // to handle a possible null value
}

public String getValueFormatted()
{

    String valueFormatted = Formatter.NOT_AVAILABLE;

    if ( getValue() != null && getValue().intValue() > 0 )
    {
        valueFormatted = "$"
                + Formatter.formatNumberToString(getValue().intValue());
    }

    return valueFormatted;

}

public void setBusinessObject( Object bo )
{
    guideBookValue = (GuideBookReturnValue) bo;
}
}
