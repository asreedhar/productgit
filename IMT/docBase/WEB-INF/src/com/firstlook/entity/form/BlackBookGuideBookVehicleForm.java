package com.firstlook.entity.form;

import com.firstlook.dealer.tools.BlackBookGuideBook;

public class BlackBookGuideBookVehicleForm extends GuideBookVehicleForm
{

private static final long serialVersionUID = -1293133629778969117L;

public BlackBookGuideBookVehicleForm()
{
    super();
}

public String getDescriptionFormatted()
{
    return new BlackBookGuideBook().getDescriptionFormatted(getVehicle());
}

public String getDescriptionFormattedWithBreak()
{
    return null;
}

}
