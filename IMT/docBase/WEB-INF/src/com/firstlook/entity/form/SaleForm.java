package com.firstlook.entity.form;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.firstlook.entity.TradePurchaseEnum;
import com.firstlook.util.Formatter;

public class SaleForm extends com.firstlook.action.BaseActionForm
{
private static final long serialVersionUID = 2137132850745767094L;
private double salePrice;
private String baseColor;
private long daysToSale;
private Date dealDate;
private String dealerNickname;
private String dealNumber;
private double retailGrossProfit;
private String make;
private String model;
private String trim;
private String bodyStyle;
private int mileage;
private TradePurchaseEnum tradePurchase;
private double unitCost;
private int year;
private double fIGrossProfit;
private int light;
private String stockNumber;

public SaleForm()
{
}

public String getActualCashValueTrade1()
{
    String actualCashValue = formatPriceToString(getSalePrice(),
            Formatter.NOT_AVAILABLE);

    return actualCashValue;
}

public String getBaseColor()
{
    return baseColor;
}

public String getBodyStyle()
{
    return bodyStyle;
}

public String getDaysToSellFormatted()
{
    if ( getDaysToSell() == 0 )
    {
        return Formatter.NOT_AVAILABLE;
    }

    return String.valueOf(getDaysToSell());
}

public long getDaysToSell()
{
    return getDaysToSale();
}

public String getDealDateFormatted()
{
    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
    return dateFormat.format(getDealDate());
}

public Date getDealDate()
{
    return dealDate;
}

public String getDealNumber()
{
    return dealNumber;
}

public String getGrossProfit()
{
    String output = formatPriceToString(getRetailGrossProfit(),
            Formatter.NOT_AVAILABLE);

    return output;
}

public double getGrossMargin()
{
    return getRetailGrossProfit();
}

public String getPrice()
{
    String output = formatPriceToString(getSalePrice(), Formatter.NOT_AVAILABLE);

    return output;

}

public double getPriceAsDouble()
{
    return salePrice;
}

public String getMake()
{
    return make;
}

public String getMileageFormatted()
{
	
    String output = Formatter.formatNumberToString(getMileage());


    if ( output.equals("") || getMileage() == -1 )
    {
        output = Formatter.NOT_AVAILABLE;
    }

    return output;
}

public int getMileage()
{
    return mileage;
}

public String getModel()
{
    return model;
}

public String getTrim()
{
    return trim;
}

public double getUnitCost()
{
    return unitCost;
}

public String getUnitCostFormatted()
{
    return formatPriceToString(getUnitCost(), Formatter.NOT_AVAILABLE);
}

public double getUnitCostAsDouble()
{
    return getUnitCost();
}

public int getYear()
{
    return year;
}

public int getTradeOrPurchase()
{
    return tradePurchase.getValue();
}

public String getTradeOrPurchaseFormatted()
{
    return tradePurchase.getName();
}

public double getSalePrice()
{
    return salePrice;
}

public void setSalePrice( double d )
{
    salePrice = d;
}

public void setBaseColor( String string )
{
    baseColor = string;
}

public void setBodyStyle( String string )
{
    bodyStyle = string;
}

public long getDaysToSale()
{
    return daysToSale;
}

public void setDaysToSale( long l )
{
    daysToSale = l;
}

public void setDealDate( Date date )
{
    dealDate = date;
}

public String getDealerNickname()
{
    return dealerNickname;
}

public void setDealerNickname( String nickName )
{
    dealerNickname = nickName;
}

public void setDealNumber( String number )
{
    dealNumber = number;
}

public double getRetailGrossProfit()
{
    return retailGrossProfit;
}

public void setRetailGrossProfit( double d )
{
    retailGrossProfit = d;
}

public void setMake( String string )
{
    make = string;
}

public void setModel( String string )
{
    model = string;
}

public void setMileage( int i )
{
    mileage = i;
}

public TradePurchaseEnum getTradePurchase()
{
    return tradePurchase;
}

public void setTradePurchase( TradePurchaseEnum tradePurchaseEnum )
{
    tradePurchase = tradePurchaseEnum;
}

public void setTrim( String string )
{
    trim = string;
}

public void setUnitCost( double d )
{
    unitCost = d;
}

public void setYear( int i )
{
    year = i;
}

public double getFIGrossProfit()
{
    return fIGrossProfit;
}

public void setFIGrossProfit( double d )
{
    fIGrossProfit = d;
}

public String getFIGrossProfitFormatted()
{
    String output = formatPriceToString(getFIGrossProfit(),
            Formatter.NOT_AVAILABLE);

    return output;
}

public int getLight()
{
    return light;
}

public void setLight( int i )
{
    light = i;
}

public String getStockNumber()
{
    return stockNumber;
}

public void setStockNumber( String i )
{
    stockNumber = i;
}
}
