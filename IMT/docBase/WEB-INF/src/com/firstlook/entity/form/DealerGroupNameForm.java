package com.firstlook.entity.form;

import java.util.Collection;

import com.firstlook.action.BaseActionForm;

public class DealerGroupNameForm extends BaseActionForm
{

private static final long serialVersionUID = 7874390244379113301L;
private Collection dealerGroups;
private int[] dealerGroupIdArray;

public DealerGroupNameForm()
{
    super();
}

public int[] getDealerGroupIdArray()
{
    return dealerGroupIdArray;
}

public Collection getDealerGroups()
{
    return dealerGroups;
}

public void setDealerGroupIdArray( int[] is )
{
    dealerGroupIdArray = is;
}

public void setDealerGroups( Collection collection )
{
    dealerGroups = collection;
}

}
