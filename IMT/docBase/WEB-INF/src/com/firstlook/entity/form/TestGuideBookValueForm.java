package com.firstlook.entity.form;

import com.firstlook.util.Formatter;

public class TestGuideBookValueForm extends com.firstlook.mock.BaseTestCase
{

private GuideBookValueForm form;

/**
 * TestGuideBookValueForm constructor comment.
 * 
 * @param arg1
 *            java.lang.String
 */
public TestGuideBookValueForm( String arg1 )
{
    super(arg1);
}

public void setup()
{
    form = new GuideBookValueForm();
}

public void testGetValueFormatted()
{

    form.getGuideBookValue().setValue(new Integer(2433));

    assertEquals("$2,433", form.getValueFormatted());

}

public void testGetValueFormatted_NA_LessThanOrEqualToZero()
{
    form.getGuideBookValue().setValue(new Integer(-3));

    assertEquals("negative", Formatter.NOT_AVAILABLE, form.getValueFormatted());

    form.getGuideBookValue().setValue(new Integer(0));

    assertEquals("zero", Formatter.NOT_AVAILABLE, form.getValueFormatted());
}

public void testGetValueFormatted_NA_Null()
{
    form.getGuideBookValue().setValue(null);

    assertEquals(Formatter.NOT_AVAILABLE, form.getValueFormatted());

}
}
