package com.firstlook.entity.form;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class PhotoUploadForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1599920451186054130L;
	FormFile imageFile;
	Integer appraisalId;
	Integer sequenceNumber;
	
	
	
	public Integer getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public FormFile getImageFile() {
		return imageFile;
	}

	public void setImageFile(FormFile imageFile) {
		this.imageFile = imageFile;
	}

	public Integer getAppraisalId() {
		return appraisalId;
	}

	public void setAppraisalId(Integer appraisalId) {
		this.appraisalId = appraisalId;
	}
	
	
	
	
}
