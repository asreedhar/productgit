package com.firstlook.entity.form;

import com.firstlook.dealer.tools.NADAGuideBook;

public class NADAGuideBookVehicleForm extends GuideBookVehicleForm
{
private static final long serialVersionUID = -2157558274292970947L;

/**
 * NADAGuideBookVehicleForm constructor comment.
 */
public NADAGuideBookVehicleForm()
{
    super();
}

public java.lang.String getDescriptionFormatted()
{
    return new NADAGuideBook().getDescriptionFormatted(getVehicle());
}

public String getDescriptionFormattedWithBreak()
{
    return new NADAGuideBook().getDescriptionFormattedWithBreak(getVehicle());
}

}
