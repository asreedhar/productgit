package com.firstlook.entity.form;

import org.apache.struts.action.ActionErrors;

import com.firstlook.entity.Region;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.DummyActionMapping;
import com.firstlook.mock.DummyHttpRequest;
import com.firstlook.mock.ObjectMother;

public class TestRegionForm extends BaseTestCase
{

private com.firstlook.mock.DummyHttpRequest request;
private com.firstlook.mock.DummyActionMapping mapping;
private RegionForm form;
private org.apache.struts.action.ActionErrors errors;

public TestRegionForm( String arg1 )
{
    super(arg1);
}

public void setup()
{
    request = new DummyHttpRequest();
    mapping = new DummyActionMapping();
    form = new RegionForm();
    errors = new ActionErrors();
}

public void testFormMapping()
{
    Region region = ObjectMother.createRegion();
    form.setBusinessObject(region);

    assertEquals("getAddress1", region.getAddress1(), form.getAddress1());
    assertEquals("getAddress2", region.getAddress2(), form.getAddress2());
    assertEquals("getBusinessUnitTypeId", region.getBusinessUnitTypeId(), form
            .getBusinessUnitTypeId());
    assertEquals("getCity", region.getCity(), form.getCity());
    assertEquals("getName", region.getName(), form.getName());
    assertEquals("getNickname", region.getNickname(), form.getNickname());
    assertEquals("getOfficeFax", region.getOfficeFax(), form.getOfficeFax());
    assertEquals("getOfficePhone", region.getOfficePhone(), form
            .getOfficePhone());
    assertEquals("getRegionCode", region.getRegionCode(), form.getRegionCode());
    assertEquals("getState", region.getState(), form.getState());
    assertEquals("getZipCode", region.getZipcode(), form.getZipCode());

}

}
