package com.firstlook.entity.form;

import java.text.SimpleDateFormat;

import org.apache.struts.validator.ValidatorForm;

import com.firstlook.entity.GroupingPromotionPlan;

public class GroupingPromotionDialogForm extends ValidatorForm
{
private static final long serialVersionUID = 7335987840592348271L;
private Integer groupingPromotionPlanId;
private Integer businessUnitId;
private Integer groupingDescriptionId;
private String startDate;
private String endDate;
private String notes;

public GroupingPromotionDialogForm()
{

}

public GroupingPromotionDialogForm( GroupingPromotionPlan promotionPlan )
{
	setGroupingPromotionPlanId( promotionPlan.getGroupingPromotionPlanId() );
	setBusinessUnitId( promotionPlan.getBusinessUnitId() );
	setGroupingDescriptionId( promotionPlan.getGroupingDescriptionId() );
	SimpleDateFormat dateFormat = new SimpleDateFormat( "MM/dd/yyyy" );
	setStartDate( dateFormat.format( promotionPlan.getPromotionStartDate() ) );
	setEndDate( dateFormat.format( promotionPlan.getPromotionEndDate() ) );
	setNotes( promotionPlan.getNotes() );
}

public String getEndDate()
{
	return endDate;
}

public void setEndDate( String endDate )
{
	this.endDate = endDate;
}

public String getNotes()
{
	return notes;
}

public void setNotes( String notes )
{
	this.notes = notes;
}

public String getStartDate()
{
	return startDate;
}

public void setStartDate( String startDate )
{
	this.startDate = startDate;
}

public Integer getGroupingDescriptionId()
{
	return groupingDescriptionId;
}

public void setGroupingDescriptionId( Integer groupingDescriptionId )
{
	this.groupingDescriptionId = groupingDescriptionId;
}

public Integer getGroupingPromotionPlanId()
{
	return groupingPromotionPlanId;
}

public void setGroupingPromotionPlanId( Integer groupingPromotionPlanId )
{
	this.groupingPromotionPlanId = groupingPromotionPlanId;
}

public Integer getBusinessUnitId()
{
	return businessUnitId;
}

public void setBusinessUnitId( Integer businessUnitId )
{
	this.businessUnitId = businessUnitId;
}

}
