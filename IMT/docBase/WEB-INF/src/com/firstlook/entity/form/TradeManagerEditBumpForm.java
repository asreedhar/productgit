package com.firstlook.entity.form;

import java.util.ArrayList;
import java.util.Collection;

import biz.firstlook.commons.gson.GsonExclude;

import com.firstlook.action.BaseActionForm;
import com.firstlook.iterator.ColumnedFormIterator;
import com.firstlook.iterator.SimpleFormIterator;
import com.firstlook.util.Formatter;

public class TradeManagerEditBumpForm extends BaseActionForm
{

private static final long serialVersionUID = 5033057582000339563L;
@GsonExclude
private Collection appraisalBumpCol = new ArrayList();
private double appraisalValue;
private int appraisalId;
private int newBumpValue;
private String appraisalInitials;
private String appraiserName;
private String appraisalBumpDate;
private String memberName;


public TradeManagerEditBumpForm()
{
    super();
}

public String getMemberName() {
	return memberName;
}

public void setMemberName(String memberName) {
	this.memberName = memberName;
}

public double getAppraisalValue()
{
    return appraisalValue;
}

public Collection getVehicleGuideBookBumpCol()
{
    return appraisalBumpCol;
}

public void setAppraisalValue( double i )
{
    appraisalValue = i;
}

public void setVehicleGuideBookBumpCol( Collection collection )
{
    appraisalBumpCol = collection;
}

public SimpleFormIterator getVehicleGuideBookBumpIterator()
{
	SimpleFormIterator bumpIterator = new SimpleFormIterator(getVehicleGuideBookBumpCol(),
	                                                         AppraisalBumpForm.class); 
    return bumpIterator;
}

public ColumnedFormIterator getVehicleGuideBookBumpColumnedIterator()
{
    return new ColumnedFormIterator(3, getVehicleGuideBookBumpCol(),
            AppraisalBumpForm.class);
}

public String getAppraisalValueFormatted()
{
    return Formatter.toTwoDecimalPrice(appraisalValue);
}

public int getNewBumpValue()
{
    return newBumpValue;
}

public int getAppraisalId()
{
    return appraisalId;
}

public void setNewBumpValue( int i )
{
    newBumpValue = i;
}

public void setAppraisalId( int i )
{
    appraisalId = i;
}

public String getAppraisalInitials()
{
    return appraisalInitials;
}

public void setAppraisalInitials( String string )
{
    appraisalInitials = string;
}

public String getAppraisalBumpDate()
{
	return appraisalBumpDate;
}

public void setAppraisalBumpDate( String appraisalBumpDate )
{
	this.appraisalBumpDate = appraisalBumpDate;
}

public String getAppraiserName()
{
	return appraiserName;
}

public void setAppraiserName( String appraiserName )
{
	this.appraiserName = appraiserName;
}

}
