package com.firstlook.entity.form;

/**
 * Insert the type's description here. Creation date: (6/18/2002 2:20:01 PM)
 * 
 * @author: Extreme Developer
 */
public class TestDealerDemandForm extends com.firstlook.mock.BaseTestCase
{
private DealerDemandForm form;

/**
 * TestDealerDemandForm constructor comment.
 * 
 * @param arg1
 *            java.lang.String
 */
public TestDealerDemandForm( String arg1 )
{
    super(arg1);
}

public void setup()
{
    form = new DealerDemandForm();
}

public void testGetRankingsFormatted_All()
{
    int topSellerRank = 9;
    int mostProfitableRank = 8;
    int fastestSellerRank = 7;

    form.setTopSellerRanking(topSellerRank);
    form.setMostProfitableRanking(mostProfitableRank);
    form.setFastestSellerRanking(fastestSellerRank);
    form.setHotList(true);

    String expected = "#" + topSellerRank + " Top Seller, #"
            + mostProfitableRank + " Most Profitable, #" + fastestSellerRank
            + " Fastest Seller, Hot List";

    assertEquals(expected, form.getRankingsFormatted());
}

public void testGetRankingsSeparator_Blank()
{
    assertEquals("", form.getRankingsSeparator(new StringBuffer("")));
}

public void testGetRankingsSeparator_Comma()
{
    assertEquals(", ", form.getRankingsSeparator(new StringBuffer("something")));
}
}
