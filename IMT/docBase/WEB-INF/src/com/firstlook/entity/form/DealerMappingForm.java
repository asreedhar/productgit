package com.firstlook.entity.form;

import com.firstlook.action.BaseActionForm;

public class DealerMappingForm extends BaseActionForm
{

private static final long serialVersionUID = 2399506205747200099L;
private Integer businessUnitId;
private Integer dealerNumber;
private String dealerName;
private String dealerState;
private String dealerCounty;

public DealerMappingForm()
{
    super();
}

public DealerMappingForm( Integer businessUnitId,
        Integer currentDealerRefNumber, String currentDealerRefName,
        String dealerState, String dealerCounty )
{
    this.businessUnitId = businessUnitId;
    this.dealerNumber = currentDealerRefNumber;
    this.dealerName = currentDealerRefName;
    this.dealerState = dealerState;
    this.dealerCounty = dealerCounty;
}

public Integer getBusinessUnitId()
{
    return businessUnitId;
}

public String getDealerCounty()
{
    return dealerCounty;
}

public String getDealerName()
{
    return dealerName;
}

public String getDealerState()
{
    return dealerState;
}

public void setBusinessUnitId( Integer integer )
{
    businessUnitId = integer;
}

public void setDealerCounty( String string )
{
    dealerCounty = string;
}

public void setDealerName( String string )
{
    dealerName = string;
}

public void setDealerState( String string )
{
    dealerState = string;
}

public Integer getDealerNumber()
{
    return dealerNumber;
}

public void setDealerNumber( Integer integer )
{
    dealerNumber = integer;
}

}
