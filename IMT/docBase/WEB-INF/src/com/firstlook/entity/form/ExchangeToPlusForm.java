package com.firstlook.entity.form;

import com.firstlook.action.BaseActionForm;

public class ExchangeToPlusForm extends BaseActionForm
{

private static final long serialVersionUID = -1857800258749675404L;
private String make;
private String model;
private int includeDealerGroup;

public ExchangeToPlusForm()
{
    super();
}

public int getIncludeDealerGroup()
{
    return includeDealerGroup;
}

public String getMake()
{
    return make;
}

public String getMakeCAPS()
{
    return make.toUpperCase();
}

public String getModel()
{
    return model;
}

public String getModelCAPS()
{
    return model.toUpperCase();
}

public void setIncludeDealerGroup( int includeDealerGroup )
{
    this.includeDealerGroup = includeDealerGroup;
}

public void setMake( String make )
{
    this.make = make;
}

public void setModel( String model )
{
    this.model = model;
}

}
