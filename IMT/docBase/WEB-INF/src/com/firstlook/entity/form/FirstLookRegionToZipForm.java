package com.firstlook.entity.form;

import com.firstlook.action.BaseActionForm;
import com.firstlook.entity.FirstLookRegionToZip;

public class FirstLookRegionToZipForm extends BaseActionForm
{

private static final long serialVersionUID = 165247602389293780L;
private FirstLookRegionToZip firstLookRegionToZip;
private String oldZipCode;

public FirstLookRegionToZipForm()
{
    setBusinessObject(new FirstLookRegionToZip());
}

public FirstLookRegionToZipForm( FirstLookRegionToZip firstLookRegionToZip )
{
    setBusinessObject(firstLookRegionToZip);
}

public void setBusinessObject( Object bo )
{
    firstLookRegionToZip = (FirstLookRegionToZip) bo;
}

public int getFirstLookRegionId()
{
    return firstLookRegionToZip.getFirstLookRegionId();
}

public FirstLookRegionToZip getFirstLookRegionToZip()
{
    return firstLookRegionToZip;
}

public String getZipCode()
{
    return firstLookRegionToZip.getZipCode();
}

public void setFirstLookRegionId( int firstLookRegionId )
{
    firstLookRegionToZip.setFirstLookRegionId(firstLookRegionId);
}

public void setZipCode( String zipCode )
{
    firstLookRegionToZip.setZipCode(zipCode);
}

public String getOldZipCode()
{
    return oldZipCode;
}

public void setOldZipCode( String string )
{
    oldZipCode = string;
}

}
