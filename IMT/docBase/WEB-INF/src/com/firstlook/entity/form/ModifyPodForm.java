package com.firstlook.entity.form;

/**
 * Insert the type's description here. Creation date: (2/18/2002 3:26:13 PM)
 * 
 * @author: Extreme Developer
 */
public class ModifyPodForm extends com.firstlook.action.BaseActionForm
{
private static final long serialVersionUID = 5957793573208707299L;
private java.lang.String[] sequenceNumbers;
private int[] ids;
private int newPodId;
private int podId;
private int[] selectedVehicleIds;
// The MOVE_TO_VEHICLE_LIST tells the action to remove the vehicle from a pod.
public final static int MOVE_TO_VEHICLE_LIST = -2;
private int switchToPodId;

/**
 * PodVehicleReorderForm constructor comment.
 */
public ModifyPodForm()
{
    super();
}

/**
 * 
 * @return int[]
 */
public int[] getIds()
{
    return ids;
}

/**
 * 
 * @return int
 */
public int getNewPodId()
{
    return newPodId;
}

/**
 * 
 * @return int
 */
public int getPodId()
{
    return podId;
}

/**
 * 
 * @return int[]
 */
public int[] getSelectedVehicleIds()
{
    return selectedVehicleIds;
}

/**
 * 
 * @return java.lang.String[]
 */
public java.lang.String[] getSequenceNumbers()
{
    return sequenceNumbers;
}

/**
 * Insert the method's description here. Creation date: (2/28/2002 10:40:19 AM)
 * 
 * @return int
 */
public int getSwitchToPodId()
{
    return switchToPodId;
}

/**
 * 
 * @param newIds
 *            int[]
 */
public void setIds( int[] newIds )
{
    ids = newIds;
}

/**
 * 
 * @param newNewPodId
 *            int
 */
public void setNewPodId( int newNewPodId )
{
    newPodId = newNewPodId;
}

/**
 * 
 * @param newPodId
 *            int
 */
public void setPodId( int newPodId )
{
    podId = newPodId;
}

/**
 * 
 * @param newSelectedVehicleIds
 *            int[]
 */
public void setSelectedVehicleIds( int[] newSelectedVehicleIds )
{
    selectedVehicleIds = newSelectedVehicleIds;
}

/**
 * 
 * @param newSequenceNumbers
 *            java.lang.String[]
 */
public void setSequenceNumbers( java.lang.String[] newSequenceNumbers )
{
    sequenceNumbers = newSequenceNumbers;
}

/**
 * Insert the method's description here. Creation date: (2/28/2002 10:40:19 AM)
 * 
 * @param newSwitchToPodId
 *            int
 */
public void setSwitchToPodId( int newSwitchToPodId )
{
    switchToPodId = newSwitchToPodId;
}
}
