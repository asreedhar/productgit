package com.firstlook.entity.form;

import java.util.Collection;

import com.firstlook.action.BaseActionForm;

public class DealerGroupDealerForm extends BaseActionForm
{

private static final long serialVersionUID = 879694213569651476L;
private Collection dealerToDealerGroupHolders;
private int[] dealerIdArray;

public DealerGroupDealerForm()
{
    super();
}

public int[] getDealerIdArray()
{
    return dealerIdArray;
}

public Collection getDealerToDealerGroupHolders()
{
    return dealerToDealerGroupHolders;
}

public void setDealerIdArray( int[] is )
{
    dealerIdArray = is;
}

public void setDealerToDealerGroupHolders( Collection collection )
{
    dealerToDealerGroupHolders = collection;
}

}
