package com.firstlook.entity.form;

import org.apache.struts.action.ActionErrors;

import com.firstlook.mock.BaseTestCase;

public class TestDealerGroupForm extends BaseTestCase
{
private com.firstlook.mock.DummyHttpRequest request;
private DealerGroupForm dealerGroupForm;
private com.firstlook.mock.DummyActionMapping mapping;

public TestDealerGroupForm( String arg1 )
{
    super(arg1);
}

public void setup()
{
    request = new com.firstlook.mock.DummyHttpRequest();
    mapping = new com.firstlook.mock.DummyActionMapping();
    dealerGroupForm = new DealerGroupForm();
}

public void testValidateAddress1()
{
    ActionErrors ae = dealerGroupForm.validate(mapping, request);

    assertTrue(isInActionErrors(ae, "error.admin.dealergroup.address1"));
}

public void testValidateCity()
{
    ActionErrors ae = dealerGroupForm.validate(mapping, request);

    assertTrue(isInActionErrors(ae, "error.admin.dealergroup.city"));
}

public void testValidateName()
{
    ActionErrors ae = dealerGroupForm.validate(mapping, request);

    assertTrue(isInActionErrors(ae, "error.admin.dealergroup.name"));
}

public void testValidateNickName()
{
    ActionErrors ae = dealerGroupForm.validate(mapping, request);

    assertTrue(isInActionErrors(ae, "error.admin.dealergroup.nickname"));
}

public void testValidateOfficeFax()
{
    dealerGroupForm.setFaxAreaCode("777");
    dealerGroupForm.setFaxPrefix("77");
    dealerGroupForm.setFaxSuffix("777");

    ActionErrors ae = dealerGroupForm.validate(mapping, request);

    assertTrue(isInActionErrors(ae, "error.admin.dealergroup.officefaxnumber"));
}

public void testValidateOfficeFaxIsNotNumeric()
{
    dealerGroupForm.setFaxAreaCode("777");
    dealerGroupForm.setFaxPrefix("777");
    dealerGroupForm.setFaxSuffix("AAAA");

    ActionErrors ae = dealerGroupForm.validate(mapping, request);

    assertTrue(isInActionErrors(ae, "error.admin.dealergroup.officefaxnumber"));
}

public void testValidateOfficeFaxPass()
{
    dealerGroupForm.setFaxAreaCode("777");
    dealerGroupForm.setFaxPrefix("777");
    dealerGroupForm.setFaxSuffix("7777");

    ActionErrors ae = dealerGroupForm.validate(mapping, request);

    assertTrue(!isInActionErrors(ae, "error.admin.dealergroup.officefaxnumber"));
}

public void testValidateOfficePhone()
{
    dealerGroupForm.setPhoneAreaCode("555");
    dealerGroupForm.setPhonePrefix("555");
    dealerGroupForm.setPhoneSuffix("555");

    ActionErrors ae = dealerGroupForm.validate(mapping, request);

    assertTrue(isInActionErrors(ae, "error.admin.dealergroup.officephonenumber"));
}

public void testValidateOfficePhoneIsNotNumeric()
{
    dealerGroupForm.setPhoneAreaCode("555");
    dealerGroupForm.setPhonePrefix("555");
    dealerGroupForm.setPhoneSuffix("55AA");

    ActionErrors ae = dealerGroupForm.validate(mapping, request);

    assertTrue(isInActionErrors(ae, "error.admin.dealergroup.officephonenumber"));
}

public void testValidateOfficePhonePass()
{
    dealerGroupForm.setPhoneAreaCode("555");
    dealerGroupForm.setPhonePrefix("555");
    dealerGroupForm.setPhoneSuffix("5555");

    ActionErrors ae = dealerGroupForm.validate(mapping, request);

    assertTrue(!isInActionErrors(ae,
            "error.admin.dealergroup.officephonenumber"));
}

public void testValidateState()
{
    ActionErrors ae = dealerGroupForm.validate(mapping, request);

    assertTrue(isInActionErrors(ae, "error.admin.dealergroup.state"));
}

public void testValidateZipcode()
{
    dealerGroupForm.setZipcode("1234");

    ActionErrors ae = dealerGroupForm.validate(mapping, request);

    assertTrue(isInActionErrors(ae, "error.admin.dealergroup.zipcode"));
}

public void testValidateZipCodeIsNot5Digits()
{
    dealerGroupForm.setZipcode("ABCDE");

    ActionErrors ae = dealerGroupForm.validate(mapping, request);

    assertTrue(isInActionErrors(ae, "error.admin.dealergroup.zipcode"));
}

public void testValidateZipcodePass()
{
    dealerGroupForm.setZipcode("12345");

    ActionErrors ae = dealerGroupForm.validate(mapping, request);

    assertTrue(!isInActionErrors(ae, "error.admin.dealergroup.zipcode"));
}

public void testValidateAgingPolicyEmpty()
{
    dealerGroupForm.setAgingPolicyFormatted("");

    ActionErrors errors = new ActionErrors();

    dealerGroupForm.validateAgingPolicy(errors);

    assertTrue("Aging Policy empty", isInActionErrors(errors,
            "error.admin.dealergroup.agingpolicy.empty"));
}

public void testValidateAgingPolicyNotANumber()
{
    dealerGroupForm.setAgingPolicyFormatted("1-465,3");

    ActionErrors errors = new ActionErrors();

    dealerGroupForm.validateAgingPolicy(errors);

    assertTrue("Aging Policy Not A Number", isInActionErrors(errors,
            "error.admin.dealergroup.agingpolicy.notanumber"));
}

public void testValidateAgingPolicyOutOfRange()
{
    dealerGroupForm.setAgingPolicyFormatted("150");

    ActionErrors errors = new ActionErrors();

    dealerGroupForm.validateAgingPolicy(errors);

    assertTrue("Aging Policy Out of Range", isInActionErrors(errors,
            "error.admin.dealergroup.agingpolicy.outofrange"));
}

public void testValidateAgingPolicyAdjustmentNegative()
{
    dealerGroupForm.setAgingPolicyAdjustment(-5);

    ActionErrors ae = dealerGroupForm.validate(mapping, request);

    assertTrue(isInActionErrors(ae,
            "error.admin.dealergroup.agingpolicyadjustment"));
}

public void testValidateAgingPolicyAdjustmentPositive()
{
    dealerGroupForm.setAgingPolicyAdjustment(5);

    ActionErrors ae = dealerGroupForm.validate(mapping, request);

    assertTrue(!isInActionErrors(ae,
            "error.admin.dealergroup.agingpolicyadjustment"));
}
}
