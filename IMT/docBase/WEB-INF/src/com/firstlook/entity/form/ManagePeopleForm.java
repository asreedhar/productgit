package com.firstlook.entity.form;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

import biz.firstlook.transact.persist.person.IPerson;

public class ManagePeopleForm extends ValidatorForm{

    private static final long serialVersionUID = -6763876106306903595L;
    
    private String position;
    private List<IPerson> people; 
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String sortColumn;
    private String sortDir;
    private boolean editMode;
    private boolean updateMode;
    
    
    public ManagePeopleForm() {
        super();
        people = new ArrayList<IPerson>();
        sortDir = "asc";
        sortColumn = "lastName";
    }
    
    public ActionErrors validate( ActionMapping arg0, HttpServletRequest arg1 )
    {
        ActionErrors errors = super.validate(arg0, arg1);
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
        "error.personManager.invalidFirstName"));
        return errors;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public List<IPerson> getPeople() {
        return people;
    }

    public void setPeople(List<IPerson> peopleList) {
        this.people = peopleList;
    }
    
    public IPerson getPeople(int index) {
        return people.get(index);
    }
    
    public void setPeople(int index, Object person) {
    	
    	
    	this.people.set(index, (IPerson) person);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSortColumn() {
        return sortColumn;
    }

    public void setSortColumn(String sortColumn) {
        this.sortColumn = sortColumn;
    }

    public String getSortDir() {
        return sortDir;
    }

    public void setSortDir(String sortDir) {
        this.sortDir = sortDir;
    }

    public boolean isEditMode() {
        return editMode;
    }

    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    public boolean isUpdateMode() {
        return updateMode;
    }

    public void setUpdateMode(boolean updateMode) {
        this.updateMode = updateMode;
    }

}
