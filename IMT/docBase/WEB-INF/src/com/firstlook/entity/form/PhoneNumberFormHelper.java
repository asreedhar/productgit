package com.firstlook.entity.form;

import java.io.Serializable;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

import com.firstlook.action.BaseActionForm;

public class PhoneNumberFormHelper implements Serializable
{

private static final long serialVersionUID = 8519191638053403998L;
	
private String areaCode = "";
private String prefix = "";
private String suffix = "";

public PhoneNumberFormHelper()
{
    super();

}

public PhoneNumberFormHelper( String phoneNumber )
{
    super();
    if ( phoneNumber != null && phoneNumber.length() == 10 )  {
    	setAreaCode(phoneNumber.substring(0, 3));
        setPrefix(phoneNumber.substring(3, 6));
        setSuffix(phoneNumber.substring(6, 10));
    } else if(phoneNumber == null) {
    	//ignore and do nothing!?  without this, this breaks TestSimpleFormIterator
    } else if(phoneNumber.length() != 10){
    	//also do nothing; MemberForm assumes everything is ok.
        //throw new IllegalArgumentException("phoneNumber should be 10 characters in length.  Input was: " + phoneNumber);
    }
}

/**
 * 
 * @return java.lang.String
 */
public java.lang.String getAreaCode()
{
    return areaCode;
}

/**
 * 
 * @return java.lang.String
 */
public String getPhoneNumber()
{
    return getAreaCode() + getPrefix() + getSuffix();
}

public String getPhoneNumberFormatted()
{
    return getAreaCode() + "-" + getPrefix() + "-" + getSuffix();
}

/**
 * 
 * @return java.lang.String
 */
public java.lang.String getPrefix()
{
    return prefix;
}

/**
 * 
 * @return java.lang.String
 */
public java.lang.String getSuffix()
{
    return suffix;
}

/**
 * 
 * @param newAreaCode
 *            java.lang.String
 */
public void setAreaCode( java.lang.String newAreaCode )
{
    areaCode = newAreaCode;
}

/**
 * 
 * @param newPrefix
 *            java.lang.String
 */
public void setPrefix( java.lang.String newPrefix )
{
    prefix = newPrefix;
}

/**
 * 
 * @param newSuffix
 *            java.lang.String
 */
public void setSuffix( java.lang.String newSuffix )
{
    suffix = newSuffix;
}

public void validate( ActionErrors errors, String errorType, ActionError error )
{
    if ( !BaseActionForm.checkIsNumeric(getPhoneNumber())
            || getPhoneNumber().length() != 10 )
    {
        errors.add(errorType, error);
    }
}
}
