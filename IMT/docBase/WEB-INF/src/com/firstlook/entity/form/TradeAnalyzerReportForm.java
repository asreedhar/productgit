package com.firstlook.entity.form;

import com.firstlook.action.BaseActionForm;

public class TradeAnalyzerReportForm extends BaseActionForm
{

private static final long serialVersionUID = 7731881084521159193L;
private String storeName;
private int oneDayCount;
private int weekCount;
private int inceptionCount;

public TradeAnalyzerReportForm()
{
    super();
}

public int getInceptionCount()
{
    return inceptionCount;
}

public int getOneDayCount()
{
    return oneDayCount;
}

public String getStoreName()
{
    return storeName;
}

public int getWeekCount()
{
    return weekCount;
}

public void setInceptionCount( int i )
{
    inceptionCount = i;
}

public void setOneDayCount( int i )
{
    oneDayCount = i;
}

public void setStoreName( String string )
{
    storeName = string;
}

public void setWeekCount( int i )
{
    weekCount = i;
}

}
