package com.firstlook.entity.form;

import org.apache.struts.action.ActionForm;

public class AuctionDataForm extends ActionForm
{

private static final long serialVersionUID = -710240736995707354L;
private String vic;
private int areaId;
private int timePeriodId;
private int mileage;
private Boolean usingVIC;
private int modelYear;

public AuctionDataForm()
{
}

public int getAreaId()
{
    return areaId;
}

public String getVic()
{
    return vic;
}

public int getTimePeriodId()
{
    return timePeriodId;
}

public void setAreaId( int i )
{
    areaId = i;
}

public void setVic( String i )
{
    vic = i;
}

public void setTimePeriodId( int i )
{
    timePeriodId = i;
}

public int getMileage()
{
    return mileage;
}

public void setMileage( int i )
{
    mileage = i;
}

public Boolean getUsingVIC()
{
	return usingVIC;
}

public void setUsingVIC( Boolean usingVIC )
{
	this.usingVIC = usingVIC;
}

public int getModelYear()
{
	return modelYear;
}

public void setModelYear( int year )
{
	this.modelYear = year;
}

}
