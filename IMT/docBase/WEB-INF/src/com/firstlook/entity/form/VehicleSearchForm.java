package com.firstlook.entity.form;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

import biz.firstlook.commons.gson.GsonExclude;

@GsonExclude
public class VehicleSearchForm extends SelectVehiclesForm
{

private static final long serialVersionUID = 426192035220710194L;
public String make;
public String model;
protected int year = Integer.MIN_VALUE;

public String trim;
public final static String TRIM_DEFAULT = "ALL";
public final static String YEAR_DEFAULT = "ALL";

public VehicleSearchForm()
{
    super();
}

private String convertNullToEmptyString( String string )
{
    if ( string == null )
    {
        string = "";
    }
    return string;
}

public String getFormattedYear()
{
    if ( year <= 0 )
    {
        return "";
    } else
    {
        return Integer.toString(year);
    }
}

public String getYearFormatted()
{
    if ( year <= 0 )
    {
        return YEAR_DEFAULT;
    } else
    {
        return Integer.toString(year);
    }
}

public java.lang.String getMake()
{
    return convertNullToEmptyString(make).toUpperCase().trim();
}

public java.lang.String getModel()
{
    return convertNullToEmptyString(model).toUpperCase().trim();
}

public java.lang.String getTrim()
{
    return convertNullToEmptyString(trim).toUpperCase().trim();
}

public String getTrimFiltered()
{
    String retVal = getTrim();

    if ( retVal == null || TRIM_DEFAULT.equalsIgnoreCase(retVal) )
    {
        retVal = "";
    }

    return retVal;
}

public String getTrimFormatted()
{
    String retVal = getTrim();

    if ( retVal == null || TRIM_DEFAULT.equalsIgnoreCase(retVal)
            || retVal.equals("") )
    {
        retVal = TRIM_DEFAULT;
    }

    return retVal;
}

public int getYear()
{
    if ( validateYear() )
    {
        return year;
    } else
    {
        return Integer.MIN_VALUE;
    }
}

public void setMake( String newMake )
{
    make = newMake;
}

public void setModel( String newModel )
{
    model = newModel;
}

public void setTrim( String newTrim )
{
    trim = newTrim;
}

public void setYear( int newYear )
{
    year = newYear;
}

public ActionErrors validateMakeErrors()
{
    ActionErrors errors = new ActionErrors();

    if ( !validateMake() )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.dealer.sell.search.inventory"));
    }

    return errors;

}

boolean validateMake()
{
    if ( isNullOrEmptyOrWhiteSpace(make) )
    {
        return false;
    }

    return true;
}

boolean validateModel()
{
    if ( isNullOrEmptyOrWhiteSpace(model) )
    {
        return false;
    }

    return true;
}

boolean validateYear()
{
    if ( year == 0 )
    {
        return false;
    }

    return true;
}
}
