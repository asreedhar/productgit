package com.firstlook.entity.form;

import org.apache.struts.action.ActionErrors;

public class TestTargetSupplySearchForm extends com.firstlook.mock.BaseTestCase
{
private TargetSupplySearchForm form;
private ActionErrors actionErrors;

/**
 * TestTargetSupplySearchForm constructor comment.
 * 
 * @param arg1
 *            java.lang.String
 */
public TestTargetSupplySearchForm( String arg1 )
{
    super(arg1);
}

public void setup()
{
    actionErrors = new ActionErrors();
    form = new TargetSupplySearchForm();
}

public void testConstructor()
{
    assertEquals("make", "", form.getMake());
}

public void testValidateMake()
{
    form.setMake("");
    form.validateMake(actionErrors);
    assertTrue("No make error", isInActionErrors(actionErrors,
            "error.admin.supply.search.make"));
}

public void testValidateModel()
{
    form.setModel("");
    form.validateModel(actionErrors);
    assertTrue("No model error", isInActionErrors(actionErrors,
            "error.admin.supply.search.model"));
}
}
