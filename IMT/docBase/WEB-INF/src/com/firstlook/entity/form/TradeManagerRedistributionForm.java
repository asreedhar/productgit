package com.firstlook.entity.form;

import com.firstlook.action.BaseActionForm;

public class TradeManagerRedistributionForm extends BaseActionForm
{
	private static final long serialVersionUID = -2403835547913966760L;
	public static String MAJOR_DELIMETER = "|";
	public static String MINOR_DELIMETER = "&";
	
	String appraisalAndActionPairs;

	public TradeManagerRedistributionForm()
	{
	}

	public String getAppraisalAndActionPairs() 
	{
		return appraisalAndActionPairs;
	}

	public void setAppraisalAndActionPairs(String appraisalAndActionPairs) 
	{
		this.appraisalAndActionPairs = appraisalAndActionPairs;
	}
	
}
