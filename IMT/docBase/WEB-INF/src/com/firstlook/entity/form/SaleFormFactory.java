package com.firstlook.entity.form;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import biz.firstlook.commons.util.DateUtilFL;

import com.firstlook.comparator.SaleComparator;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.VehicleSaleEntity;
import com.firstlook.exception.ApplicationException;
import com.firstlook.persistence.vehiclesale.ViewDealsRow;
import com.firstlook.service.dealer.IDealerService;
import com.firstlook.util.SaleUtility;

public class SaleFormFactory
{

// TODO: it is redundant to pass in VehicleSale and Dealer, as you can just do sale.getInventory().getDealer(),
// but this is a temporary fix for Spring factoring - AB 6/1/05

public SaleForm buildSaleForm( VehicleSaleEntity sale, Dealer dealer ) throws ApplicationException
{
	SaleForm saleForm = new SaleForm();

    saleForm.setSalePrice(sale.getSalePrice());
    saleForm.setBaseColor(sale.getInventory().getVehicle().getBaseColor());
    saleForm.setBodyStyle(sale.getInventory().getVehicle()
            .getBodyType().getBodyType());
    saleForm.setDaysToSale(SaleUtility.calculateDaysToSale(
                                                           sale.getInventory().getInventoryReceivedDt(),
                                                           sale.getDealDate()));
    saleForm.setDealDate(sale.getDealDate());
    saleForm.setDealerNickname( dealer.getNickname());
    saleForm.setDealNumber(sale.getFinanceInsuranceDealNumber());
    saleForm.setRetailGrossProfit(sale.getFrontEndGross());
    saleForm.setFIGrossProfit(sale.getBackEndGross());
    saleForm.setMake(sale.getInventory().getVehicle().getMake());
    saleForm.setModel(sale.getInventory().getVehicle().getModel());
    saleForm.setMileage(sale.getVehicleMileage());
    saleForm.setTradePurchase(sale.getInventory().getTradePurchaseEnum());
    saleForm.setTrim(createTrim(sale));
    saleForm.setUnitCost(sale.getInventory().getUnitCost());
    saleForm.setYear(sale.getInventory().getVehicle().getVehicleYear().intValue());
    saleForm.setLight(sale.getInventory().getInitialVehicleLight().intValue() );
    saleForm.setStockNumber(sale.getInventory().getStockNumber());
    return saleForm;
}


public SaleForm buildSaleForm( ViewDealsRow sale ) throws ApplicationException
{
	SaleForm saleForm = new SaleForm();

    saleForm.setSalePrice(sale.getSalePrice());
    saleForm.setBaseColor(sale.getBaseColor());
    saleForm.setBodyStyle(sale.getBodyStyle());
    saleForm.setDaysToSale(calculateDaysToSale(sale));
    saleForm.setDealDate(sale.getDealDate());
    saleForm.setDealNumber(sale.getDealNumber() );
    saleForm.setRetailGrossProfit(sale.getRetailGrossProfit());
    saleForm.setDealerNickname( sale.getDealerNickname() );
    saleForm.setMake(sale.getMake());
    saleForm.setModel(sale.getModel());
    saleForm.setMileage(sale.getMileage());
    saleForm.setTradePurchase(sale.getTradePurchase());
    saleForm.setTrim(sale.getTrim());
    saleForm.setUnitCost(sale.getUnitCost());
    saleForm.setYear(sale.getYear());
    return saleForm;
}

public List buildSortedSaleFormsUsingViewDealRows( Collection vehicleSales, String orderBy )
throws ApplicationException
{
List<SaleForm> saleForms = new ArrayList<SaleForm>();
Iterator iterator = vehicleSales.iterator();
ViewDealsRow vehicleSale;

while (iterator.hasNext())
{
vehicleSale = (ViewDealsRow) iterator.next();
SaleForm form = buildSaleForm( vehicleSale );
saleForms.add(form);
}
Collections.sort(saleForms, new SaleComparator(orderBy));
return saleForms;
}

public List<SaleForm> buildSortedSaleForms( Collection vehicleSales, String orderBy, IDealerService dealerService )
        throws ApplicationException
{
    List<SaleForm> saleForms = new ArrayList<SaleForm>();
    Iterator iterator = vehicleSales.iterator();
    Dealer dealer;
    VehicleSaleEntity vehicleSale;
    
    while (iterator.hasNext())
    {
    	vehicleSale = (VehicleSaleEntity) iterator.next();
    	dealer = dealerService.retrieveDealer( vehicleSale.getInventory().getDealerId() );
        SaleForm form = buildSaleForm( vehicleSale, dealer );
        saleForms.add(form);
    }
    Collections.sort(saleForms, new SaleComparator(orderBy));
    return saleForms;
}

private long calculateDaysToSale( ViewDealsRow sale )
{
    if ( sale.getInventoryReceivedDate() != null
            && sale.getDealDate() != null )
    {
        double milliDifference = (sale.getDealDate().getTime() - sale
                .getInventoryReceivedDate().getTime());
        double dayDifference = (milliDifference / DateUtilFL.MILLISECONDS_PER_DAY);

        long dayDiff = Math.round(dayDifference);

        if ( dayDiff < 1 )
        {
            return 1;
        } else
        {
            return dayDiff;
        }
    } else
    {
        return 0;
    }
}

private String createTrim( VehicleSaleEntity sale )
{
    String trim = sale.getInventory().getVehicle().getVehicleTrim();
    if ( "N/A".equalsIgnoreCase(trim) )
    {
        return "";
    } else
    {
        return trim;
    }
}
}
