package com.firstlook.entity.form;

import com.firstlook.mock.BaseTestCase;

/**
 * @author cvs
 * 
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates. To enable and disable the creation of type
 * comments go to Window>Preferences>Java>Code Generation.
 */
public class TestExchangeToPlusForm extends BaseTestCase
{

private ExchangeToPlusForm form;

public TestExchangeToPlusForm( String arg1 )
{
    super(arg1);
}

public void setup()
{
    form = new ExchangeToPlusForm();
}

public void testGetMakeCAPS()
{
    String make = "Honda";
    form.setMake(make);

    assertEquals("Make is not CAPS", make.toUpperCase(), form.getMakeCAPS());
}

public void testGetModelCAPS()
{
    String model = "Accord";
    form.setModel(model);

    assertEquals("Model is not CAPS", model.toUpperCase(), form.getModelCAPS());
}

}
