package com.firstlook.entity.form;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import biz.firstlook.transact.persist.exception.SelectedThirdPartyVehicleNotFoundException;
import biz.firstlook.transact.persist.model.BookOut;
import biz.firstlook.transact.persist.model.BookOutSource;
import biz.firstlook.transact.persist.model.ThirdPartyDataProvider;
import biz.firstlook.transact.persist.model.ThirdPartyVehicle;
import biz.firstlook.transact.persist.service.appraisal.IAppraisal;

import com.firstlook.action.BaseActionForm;
import com.firstlook.dealer.tools.GuideBook;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.bookout.BookOutService;
import com.firstlook.util.Formatter;

public class AppraisalForm extends BaseActionForm {

	private static final long serialVersionUID = -6610817423999898437L;

	private String actualMake;

	private String actualModel;

	private String actualTrim;

	private IAppraisal appraisal;

	private int guideBookId;

	private String kelleyDriveTrainCode;

	private String kelleyEngineCode;

	private String kelleyMakeCode;

	private String kelleyModelCode;

	private String kelleyTransmissionCode;

	// removed properties off of Appraisal object onto the form
	// kelleyPublishState
	private String publishState;

	public AppraisalForm(IAppraisal appraisal) {
		super();
		setBusinessObject(appraisal);
		setActualMake(appraisal.getVehicle().getMakeModelGrouping().getMake());
		setActualModel(appraisal.getVehicle().getMakeModelGrouping().getModel());
		setActualTrim(appraisal.getVehicle().getVehicleTrim());
	}

	public AppraisalForm(IAppraisal appraisal, int guideBookId) {
		this(appraisal);
		setGuideBookId(guideBookId);
		BookOut bookout = appraisal.getLatestBookOut(guideBookId);
		ThirdPartyVehicle selectedTPV = null;

		setActualMake("");
		setActualModel("");
		setActualTrim("");

		if (bookout != null) {
			try {
				selectedTPV = bookout.getSelectedThirdPartyVehicle();

				if (StringUtils.isNotBlank(selectedTPV.getMake())) {
					setActualMake(selectedTPV.getMake());
				}
				if (StringUtils.isNotBlank(selectedTPV.getModel())) {
					setActualModel(selectedTPV.getModel());
				}
				if (StringUtils.isNotBlank(selectedTPV.getBody())) {
					setActualTrim(selectedTPV.getBody());
				} else if (guideBookId == ThirdPartyDataProvider.GUIDEBOOK_BLACKBOOK_TYPE) {
					setActualTrim(appraisal.getVehicle().getVehicleTrim());
				}
			} catch (SelectedThirdPartyVehicleNotFoundException e) {

			}
		}
	}

	public void setBusinessObject(Object object) {
		appraisal = (IAppraisal) object;
	}

	public int getActionId() {
		return appraisal.getAppraisalActionType().getAppraisalActionTypeId();
	}

	public String getActualMake() {
		return actualMake;
	}

	public String getActualModel() {
		return actualModel;
	}

	public String getActualTrim() {
		return actualTrim;
	}

	public int getAppraisalValue() {
		return appraisal.getLatestAppraisalValue().getValue();
	}

	public String getBody() {
		return appraisal.getVehicle().getBodyType().getBodyType();
	}

	public BookOut getLatestBookOut() {
		BookOut bookOut = appraisal.getLatestBookOut(guideBookId);
		if (bookOut != null) {
			return bookOut;
		} else {
			return BookOutService.createEmptyBookOutForPrinting(guideBookId,
					BookOutSource.BOOK_OUT_SOURCE_APPRAISAL);
		}
	}

	public int getBookPrice() {
		return getLatestBookOut().getBookPrice().intValue();
	}

	public String getColor() {
		return appraisal.getColor();
	}

	public String getConditionDisclosure() {
		return appraisal.getConditionDescription();
	}

	public int getCylinderCount() {
		return appraisal.getVehicle().getCylinderCount().intValue();
	}

	public int getDealerId() {
		return appraisal.getBusinessUnitId().intValue();
	}

	public int getDoorCount() {
		return appraisal.getVehicle().getDoorCount().intValue();
	}

	public String getDriveTrainDescription() {
		return appraisal.getVehicle().getVehicleDriveTrain();
	}

	public String getEngineDescription() {
		return appraisal.getVehicle().getVehicleEngine();
	}

	public String getFuelType() {
		return appraisal.getVehicle().getFuelType();
	}

	public int getGroupingId() throws ApplicationException {
		return appraisal.getVehicle().getMakeModelGrouping()
				.getGroupingDescription().getGroupingDescriptionId().intValue();
	}

	public String getInteriorColor() {
		return appraisal.getVehicle().getInteriorColor();
	}

	public String getInteriorDescription() {
		return appraisal.getVehicle().getInteriorDescription();
	}

	public String getKelleyDriveTrainCode() {
		return kelleyDriveTrainCode;
	}

	public String getKelleyEngineCode() {
		return kelleyEngineCode;
	}

	public String getKelleyMakeCode() {
		return kelleyMakeCode;
	}

	public String getKelleyModelCode() {
		return kelleyModelCode;
	}

	public String getKelleyTransmissionCode() {
		return kelleyTransmissionCode;
	}

	public String getMake() {
		return appraisal.getVehicle().getMake();
	}

	public int getMemberId() {
		return appraisal.getMemberId().intValue();
	}

	public int getMileage() {
		return appraisal.getMileage();
	}

	public String getMileageFormattedWithComma() {
		return Formatter.formatNumberToString(getMileage());
	}

	public String getModel() {
		return appraisal.getVehicle().getModel();
	}

	public int getPrice() {
		return appraisal.getWholesalePrice().intValue();
	}

	public String getReconditioningNotes() {
		return appraisal.getEstimatedReconditioningCost().toString();
	}
	
	public String getTargetGrossProfit() {
		return appraisal.getTargetGrossProfit().toString();
	}

	public boolean isSold() {
		return appraisal.getDealCompleted().booleanValue();
	}

	public Date getTradeAnalyzerDate() {
		return appraisal.getDateCreated();
	}

	public String getTradeAnalyzerDateFormatted() {

		if (appraisal.getDateCreated() != null) {
			return formatDateToString(appraisal.getDateCreated());
		}

		return Formatter.NOT_AVAILABLE;

	}

	public String getTransmission() {
		return appraisal.getVehicle().getVehicleTransmission();
	}

	public String getTrim() {
		return appraisal.getVehicle().getVehicleTrim();
	}

	public IAppraisal getAppraisal() {
		return appraisal;
	}

	public int getAppraisalId() {
		return appraisal.getAppraisalId().intValue();
	}

	public String getVin() {
		return appraisal.getVehicle().getVin();
	}

	public String getYear() {
		return appraisal.getVehicle().getVehicleYear().toString();
	}

	public int getWeight() {
		if (getLatestBookOut().getWeight() != null) {
			return getLatestBookOut().getWeight().intValue();
		}
		return 0;
	}

	public String getGuideBookTitle() {
		if (getLatestBookOut() != null) {
			String title = getLatestBookOut().getThirdPartyDescription();
			if (title != null && !title.equals(""))
				return title;

		}

		return GuideBook.getTitle(guideBookId);
	}

	public String getGuideBookFooter() {
		if (getLatestBookOut() != null) {
			return getLatestBookOut().getGuideBookDefaultFooter();
		} else {
			return "";
		}
	}

	public int getMileageCostAdjustment() {
		return getLatestBookOut().getMileageCostAdjustment().intValue();
	}

	public int getMsrp() {
		if (getLatestBookOut().getMsrp() != null) {
			return getLatestBookOut().getMsrp().intValue();
		}
		return 0;
	}

	public String getGuideBookDescription() {
		if (getLatestBookOut() != null) {
			return getLatestBookOut().getThirdPartyDescription();
		} else {
			return "AppraisalForm GuideBook Description Error";
		}
	}

	public String getGuideBookRegion() {
		if (getLatestBookOut() != null) {
			return getLatestBookOut().getRegion();
		} else {
			return "";
		}
	}

	public String getLastBookOutDateFormatted() {
		if (getLatestBookOut().getDateCreated() != null) {
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			return dateFormat.format(getLatestBookOut().getDateCreated());
		} else {
			return "";
		}
	}

	public String getKelleyPublishRange() {
		if (getLatestBookOut() != null) {
			String datePublished = getLatestBookOut().getDatePublished();
			if (datePublished != null) {
				datePublished = datePublished.substring(3);
			}
			return datePublished;
		} else {
			return "No publish info";
		}
	}

	public String getPublishInfo() {
		if (getLatestBookOut() != null) {
			return getLatestBookOut().getDatePublished();
		} else {
			return "No publish info";
		}
	}

	public String getPublishMonth() {
		if (getLatestBookOut() != null
				&& getLatestBookOut().getDatePublished() != null) {
			return getLatestBookOut().getDatePublished().substring(4, 11);
		} else {
			return "";
		}
	}

	public String getPublishState() {
		return publishState;
	}

	public void setPublishState(String publishState) {
		this.publishState = publishState;
	}

	public String getPublishYear() {
		if (getLatestBookOut() != null
				&& getLatestBookOut().getDatePublished() != null) {
			return getLatestBookOut().getDatePublished().substring(12, 16);
		} else {
			return "";
		}
	}

	public int getGuideBookId() {
		return guideBookId;
	}

	public void setGuideBookId(int guideBookId) {
		this.guideBookId = guideBookId;
	}

	public void setActualMake(String actualMake) {
		this.actualMake = actualMake;
	}

	public void setActualModel(String actualModel) {
		this.actualModel = actualModel;
	}

	public void setActualTrim(String actualTrim) {
		this.actualTrim = actualTrim;
	}

	public String getBlackBookDescription() {
		BookOut previousBookout = appraisal
				.getLatestBookOut(ThirdPartyDataProvider.BLACKBOOK
						.getThirdPartyId());
		if (previousBookout == null)
			return "";
		try {
			return previousBookout.getSelectedThirdPartyVehicle()
					.getDescription();
		} catch (SelectedThirdPartyVehicleNotFoundException e) {

			// this is just a safety net incase something goes wrong and there
			// isn't a BB vehicle - which shouldn't happen
			return actualModel + " " + actualTrim;
		}
	}

}
