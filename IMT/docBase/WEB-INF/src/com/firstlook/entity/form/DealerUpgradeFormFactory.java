package com.firstlook.entity.form;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import com.firstlook.entity.DealerUpgrade;
import com.firstlook.entity.DealerUpgradeLookup;
import com.firstlook.exception.ApplicationException;

public class DealerUpgradeFormFactory
{
private static final String DATE_FORMAT = "MM/dd/yyyy";
static SimpleDateFormat FORMATTER = new SimpleDateFormat(DATE_FORMAT);

public static DealerUpgradeForm createDealerUpgradeForm(
        Collection dealerUpgrades )
{
    DealerUpgradeForm dealerUpgradeForm = new DealerUpgradeForm();

    Iterator dealerUpgradesIterator = dealerUpgrades.iterator();
    while (dealerUpgradesIterator.hasNext())
    {
        DealerUpgrade dealerUpgrade = (DealerUpgrade) dealerUpgradesIterator
                .next();
        switch (dealerUpgrade.getDealerUpgradeCode())
        {
            case DealerUpgradeLookup.AGING_PLAN_CODE:
            {
                setAgingPlanValuesOnForm(dealerUpgradeForm, dealerUpgrade);
                break;
            }
            case DealerUpgradeLookup.APPRAISAL_CODE:
            {
                setAppraisalValuesOnForm(dealerUpgradeForm, dealerUpgrade);
                break;
            }
            case DealerUpgradeLookup.REDISTRIBUTION_CODE:
            {
                setRedistributionValuesOnForm(dealerUpgradeForm, dealerUpgrade);
                break;
            }
            case DealerUpgradeLookup.CIA_CODE:
            {
                setCIAValuesOnForm(dealerUpgradeForm, dealerUpgrade);
                break;
            }
            case DealerUpgradeLookup.AUCTION_DATA_CODE:
            {
                setAuctionDataValuesOnForm(dealerUpgradeForm, dealerUpgrade);
                break;
            }

            case DealerUpgradeLookup.WINDOWSTICKER_CODE:
            {
                setWindowStickerValuesOnForm(dealerUpgradeForm, dealerUpgrade);
                break;
            }
            case DealerUpgradeLookup.PERFORMANCEDASHBOARD_CODE:
            {
                setPerformanceDashboardValuesOnForm(dealerUpgradeForm,
                        dealerUpgrade);
                break;
            }
            case DealerUpgradeLookup.MARKET_DATA_CODE:
            {
                setMarketDataValuesOnForm(dealerUpgradeForm, dealerUpgrade);
                break;
            }
			case DealerUpgradeLookup.ANNUAL_ROI_CODE:
			{
				setAnnualRoiValuesOnForm( dealerUpgradeForm, dealerUpgrade );
			}
			case DealerUpgradeLookup.EQUITY_ANALYZER_CODE:
			{
				setEquityAnalyzerValuesOnForm( dealerUpgradeForm, dealerUpgrade );
			}
    }
	}
    return dealerUpgradeForm;
}

private static void setPerformanceDashboardValuesOnForm(
        DealerUpgradeForm dealerUpgradeForm, DealerUpgrade dealerUpgrade )
{
    if ( isValid(dealerUpgrade.getEndDate()) )
    {
        dealerUpgradeForm.setPerformanceDashboardEndDate(FORMATTER
                .format(dealerUpgrade.getEndDate()));
    }
    if ( isValid(dealerUpgrade.getStartDate()) )
    {
        dealerUpgradeForm.setPerformanceDashboardStartDate(FORMATTER
                .format(dealerUpgrade.getStartDate()));
    }
    dealerUpgradeForm.setIncludePerformanceDashboard(dealerUpgrade.isActive());
}

private static void setCIAValuesOnForm( DealerUpgradeForm dealerUpgradeForm,
        DealerUpgrade dealerUpgrade )
{
    if ( isValid(dealerUpgrade.getEndDate()) )
    {
        dealerUpgradeForm.setCiaEndDate(FORMATTER.format(dealerUpgrade
                .getEndDate()));
    }
    if ( isValid(dealerUpgrade.getStartDate()) )
    {
        dealerUpgradeForm.setCiaStartDate(FORMATTER.format(dealerUpgrade
                .getStartDate()));
    }

    dealerUpgradeForm.setIncludeCIA(dealerUpgrade.isActive());
}

private static void setMarketDataValuesOnForm(
        DealerUpgradeForm dealerUpgradeForm, DealerUpgrade dealerUpgrade )
{
    if ( isValid(dealerUpgrade.getEndDate()) )
    {
        dealerUpgradeForm.setMarketDataEndDate(FORMATTER.format(dealerUpgrade
                .getEndDate()));
    }
    if ( isValid(dealerUpgrade.getStartDate()) )
    {
        dealerUpgradeForm.setMarketDataStartDate(FORMATTER.format(dealerUpgrade
                .getStartDate()));
    }

    dealerUpgradeForm.setIncludeMarketData(dealerUpgrade.isActive());
}

private static void setAuctionDataValuesOnForm(
        DealerUpgradeForm dealerUpgradeForm, DealerUpgrade dealerUpgrade )
{
    if ( isValid(dealerUpgrade.getEndDate()) )
    {
        dealerUpgradeForm.setAuctionDataEndDate(FORMATTER.format(dealerUpgrade
                .getEndDate()));
    }
    if ( isValid(dealerUpgrade.getStartDate()) )
    {
        dealerUpgradeForm.setAuctionDataStartDate(FORMATTER
                .format(dealerUpgrade.getStartDate()));
    }

    dealerUpgradeForm.setIncludeAuctionData(dealerUpgrade.isActive());
}

private static void setWindowStickerValuesOnForm(
        DealerUpgradeForm dealerUpgradeForm, DealerUpgrade dealerUpgrade )
{
    if ( isValid(dealerUpgrade.getEndDate()) )
    {
        dealerUpgradeForm.setWindowStickerEndDate(FORMATTER
                .format(dealerUpgrade.getEndDate()));
    }
    if ( isValid(dealerUpgrade.getStartDate()) )
    {
        dealerUpgradeForm.setWindowStickerStartDate(FORMATTER
                .format(dealerUpgrade.getStartDate()));
    }

    dealerUpgradeForm.setIncludeWindowSticker(dealerUpgrade.isActive());
}

private static void setRedistributionValuesOnForm(
        DealerUpgradeForm dealerUpgradeForm, DealerUpgrade dealerUpgrade )
{
    if ( isValid(dealerUpgrade.getEndDate()) )
    {
        dealerUpgradeForm.setRedistributionEndDate(FORMATTER
                .format(dealerUpgrade.getEndDate()));
    }
    if ( isValid(dealerUpgrade.getStartDate()) )
    {
        dealerUpgradeForm.setRedistributionStartDate(FORMATTER
                .format(dealerUpgrade.getStartDate()));
    }

    dealerUpgradeForm.setIncludeRedistribution(dealerUpgrade.isActive());
}

private static void setAppraisalValuesOnForm(
        DealerUpgradeForm dealerUpgradeForm, DealerUpgrade dealerUpgrade )
{
    if ( isValid(dealerUpgrade.getEndDate()) )
    {
        dealerUpgradeForm.setAppraisalEndDate(FORMATTER.format(dealerUpgrade
                .getEndDate()));
    }
    if ( isValid(dealerUpgrade.getStartDate()) )
    {
        dealerUpgradeForm.setAppraisalStartDate(FORMATTER.format(dealerUpgrade
                .getStartDate()));
    }
    dealerUpgradeForm.setIncludeAppraisal(dealerUpgrade.isActive());
}

private static void setAgingPlanValuesOnForm(
        DealerUpgradeForm dealerUpgradeForm, DealerUpgrade dealerUpgrade )
{
    if ( isValid(dealerUpgrade.getEndDate()) )
    {
        dealerUpgradeForm.setAgingPlanEndDate(FORMATTER.format(dealerUpgrade
                .getEndDate()));
    }
    if ( isValid(dealerUpgrade.getStartDate()) )
    {
        dealerUpgradeForm.setAgingPlanStartDate(FORMATTER.format(dealerUpgrade
                .getStartDate()));
    }

    dealerUpgradeForm.setIncludeAgingPlan(dealerUpgrade.isActive());
}

private static void setAnnualRoiValuesOnForm( DealerUpgradeForm dealerUpgradeForm, DealerUpgrade dealerUpgrade )
{
	if ( isValid( dealerUpgrade.getEndDate() ) )
	{
		dealerUpgradeForm.setAnnualRoiEndDate( FORMATTER.format( dealerUpgrade.getEndDate() ) );
	}
	if ( isValid( dealerUpgrade.getStartDate() ) )
	{
		dealerUpgradeForm.setAnnualRoiStartDate( FORMATTER.format( dealerUpgrade.getStartDate() ) );
	}

	dealerUpgradeForm.setIncludeAnnualRoi( dealerUpgrade.isActive() );
}

private static void setEquityAnalyzerValuesOnForm( DealerUpgradeForm dealerUpgradeForm, DealerUpgrade dealerUpgrade )
{
	if ( isValid( dealerUpgrade.getEndDate() ) )
	{
		dealerUpgradeForm.setEquityAnalyzerEndDate( FORMATTER.format( dealerUpgrade.getEndDate() ) );
	}
	if ( isValid( dealerUpgrade.getStartDate() ) )
	{
		dealerUpgradeForm.setEquityAnalyzerStartDate( FORMATTER.format( dealerUpgrade.getStartDate() ) );
	}

	dealerUpgradeForm.setIncludeEquityAnalyzer( dealerUpgrade.isActive() );
}

static boolean isValid( Date date )
{
    if ( date != null && !date.equals("") )
    {
        return true;
    } else
    {
        return false;
    }
}

static boolean isValidString( String date )
{
    if ( date != null && !date.equals("") )
    {
        return true;
    } else
    {
        return false;
    }
}

public static Collection createDealerUpgrades(
        DealerUpgradeForm dealerUpgradeForm, int dealerId )
        throws ApplicationException
{
    ArrayList<DealerUpgrade> upgrades = new ArrayList<DealerUpgrade>();
    try
    {
		upgrades.add( createDealerUpgrade( dealerUpgradeForm, dealerId, DealerUpgradeLookup.CIA_CODE ) );
		upgrades.add( createDealerUpgrade( dealerUpgradeForm, dealerId, DealerUpgradeLookup.AGING_PLAN_CODE ) );
		upgrades.add( createDealerUpgrade( dealerUpgradeForm, dealerId, DealerUpgradeLookup.AUCTION_DATA_CODE ) );
		upgrades.add( createDealerUpgrade( dealerUpgradeForm, dealerId, DealerUpgradeLookup.REDISTRIBUTION_CODE ) );
		upgrades.add( createDealerUpgrade( dealerUpgradeForm, dealerId, DealerUpgradeLookup.APPRAISAL_CODE ) );
		upgrades.add( createDealerUpgrade( dealerUpgradeForm, dealerId, DealerUpgradeLookup.WINDOWSTICKER_CODE ) );
		upgrades.add( createDealerUpgrade( dealerUpgradeForm, dealerId, DealerUpgradeLookup.PERFORMANCEDASHBOARD_CODE ) );
		upgrades.add( createDealerUpgrade( dealerUpgradeForm, dealerId, DealerUpgradeLookup.MARKET_DATA_CODE ) );
		upgrades.add( createDealerUpgrade( dealerUpgradeForm, dealerId, DealerUpgradeLookup.ANNUAL_ROI_CODE ) );
		upgrades.add( createDealerUpgrade( dealerUpgradeForm, dealerId, DealerUpgradeLookup.EQUITY_ANALYZER_CODE ) );
	}
	catch ( ParseException e )
    {
		throw new ApplicationException( "Problem parsing date for dealer upgrades", e );
    }
    return upgrades;
}

private static DealerUpgrade createDealerUpgrade(
        DealerUpgradeForm dealerUpgradeForm, int dealerId, int upgradeCode )
        throws ParseException
{
    DealerUpgrade upgrade = new DealerUpgrade();
    populateDealerUpgrade(upgrade, dealerUpgradeForm, upgradeCode);
    upgrade.setDealerId(dealerId);
    return upgrade;
}

private static void populateDealerUpgrade( DealerUpgrade upgrade,
        DealerUpgradeForm dealerUpgradeForm, int upgradeCode )
        throws ParseException
{
    upgrade.setDealerUpgradeCode(upgradeCode);
    switch (upgradeCode)
    {
        case DealerUpgradeLookup.CIA_CODE:
        {
            upgrade.setActive(dealerUpgradeForm.isIncludeCIA());
            if ( isValidString(dealerUpgradeForm.getCiaEndDate()) )
            {
                upgrade.setEndDate(FORMATTER.parse(dealerUpgradeForm
                        .getCiaEndDate()));
            }
            if ( isValidString(dealerUpgradeForm.getCiaStartDate()) )
            {
                upgrade.setStartDate(FORMATTER.parse(dealerUpgradeForm
                        .getCiaStartDate()));
            }
            break;
        }
        case DealerUpgradeLookup.AGING_PLAN_CODE:
        {
            upgrade.setActive(dealerUpgradeForm.isIncludeAgingPlan());
            if ( isValidString(dealerUpgradeForm.getAgingPlanEndDate()) )
            {
                upgrade.setEndDate(FORMATTER.parse(dealerUpgradeForm
                        .getAgingPlanEndDate()));
            }
            if ( isValidString(dealerUpgradeForm.getAgingPlanStartDate()) )
            {
                upgrade.setStartDate(FORMATTER.parse(dealerUpgradeForm
                        .getAgingPlanStartDate()));
            }
            break;
        }
        case DealerUpgradeLookup.AUCTION_DATA_CODE:
        {
            upgrade.setActive(dealerUpgradeForm.isIncludeAuctionData());
            if ( isValidString(dealerUpgradeForm.getAuctionDataEndDate()) )
            {
                upgrade.setEndDate(FORMATTER.parse(dealerUpgradeForm
                        .getAuctionDataEndDate()));
            }
            if ( isValidString(dealerUpgradeForm.getAuctionDataStartDate()) )
            {
                upgrade.setStartDate(FORMATTER.parse(dealerUpgradeForm
                        .getAuctionDataStartDate()));
            }
            break;
        }
        case DealerUpgradeLookup.REDISTRIBUTION_CODE:
        {
            upgrade.setActive(dealerUpgradeForm.isIncludeRedistribution());
            if ( isValidString(dealerUpgradeForm.getRedistributionEndDate()) )
            {
                upgrade.setEndDate(FORMATTER.parse(dealerUpgradeForm
                        .getRedistributionEndDate()));
            }
            if ( isValidString(dealerUpgradeForm.getRedistributionStartDate()) )
            {
                upgrade.setStartDate(FORMATTER.parse(dealerUpgradeForm
                        .getRedistributionStartDate()));
            }
            break;
        }
        case DealerUpgradeLookup.APPRAISAL_CODE:
        {
            upgrade.setActive(dealerUpgradeForm.isIncludeAppraisal());
            if ( isValidString(dealerUpgradeForm.getAppraisalEndDate()) )
            {
                upgrade.setEndDate(FORMATTER.parse(dealerUpgradeForm
                        .getAppraisalEndDate()));
            }
            if ( isValidString(dealerUpgradeForm.getAppraisalStartDate()) )
            {
                upgrade.setStartDate(FORMATTER.parse(dealerUpgradeForm
                        .getAppraisalStartDate()));
            }
            break;
        }
        case DealerUpgradeLookup.WINDOWSTICKER_CODE:
        {
            upgrade.setActive(dealerUpgradeForm.isIncludeWindowSticker());
            if ( isValidString(dealerUpgradeForm.getWindowStickerEndDate()) )
            {
                upgrade.setEndDate(FORMATTER.parse(dealerUpgradeForm
                        .getWindowStickerEndDate()));
            }
            if ( isValidString(dealerUpgradeForm.getWindowStickerStartDate()) )
            {
                upgrade.setStartDate(FORMATTER.parse(dealerUpgradeForm
                        .getWindowStickerStartDate()));
            }
            break;
        }
        case DealerUpgradeLookup.PERFORMANCEDASHBOARD_CODE:
        {
            upgrade
                    .setActive(dealerUpgradeForm
                            .isIncludePerformanceDashboard());
            if ( isValidString(dealerUpgradeForm
                    .getPerformanceDashboardEndDate()) )
            {
                upgrade.setEndDate(FORMATTER.parse(dealerUpgradeForm
                        .getPerformanceDashboardEndDate()));
            }
            if ( isValidString(dealerUpgradeForm
                    .getPerformanceDashboardStartDate()) )
            {
                upgrade.setStartDate(FORMATTER.parse(dealerUpgradeForm
                        .getPerformanceDashboardStartDate()));
            }
            break;
        }
        case DealerUpgradeLookup.MARKET_DATA_CODE:
        {
            upgrade.setActive(dealerUpgradeForm.isIncludeMarketData());
            if ( isValidString(dealerUpgradeForm.getMarketDataEndDate()) )
            {
                upgrade.setEndDate(FORMATTER.parse(dealerUpgradeForm
                        .getMarketDataEndDate()));
            }
            if ( isValidString(dealerUpgradeForm.getMarketDataStartDate()) )
            {
                upgrade.setStartDate(FORMATTER.parse(dealerUpgradeForm
                        .getMarketDataStartDate()));
            }
            break;
        }
		case DealerUpgradeLookup.ANNUAL_ROI_CODE:
		{
			upgrade.setActive( dealerUpgradeForm.isIncludeAnnualRoi() );
			if ( isValidString( dealerUpgradeForm.getAnnualRoiEndDate() ) )
			{
				upgrade.setEndDate( FORMATTER.parse( dealerUpgradeForm.getAnnualRoiEndDate() ) );
			}
			if ( isValidString( dealerUpgradeForm.getAnnualRoiStartDate() ) )
			{
				upgrade.setStartDate( FORMATTER.parse( dealerUpgradeForm.getAnnualRoiStartDate() ) );
			}
			break;
		}
		case DealerUpgradeLookup.EQUITY_ANALYZER_CODE:
		{
			upgrade.setActive( dealerUpgradeForm.isIncludeEquityAnalyzer() );
			if ( isValidString( dealerUpgradeForm.getEquityAnalyzerEndDate() ) )
			{
				upgrade.setEndDate( FORMATTER.parse( dealerUpgradeForm.getEquityAnalyzerEndDate() ) );
			}
			if ( isValidString( dealerUpgradeForm.getEquityAnalyzerStartDate() ) )
			{
				upgrade.setStartDate( FORMATTER.parse( dealerUpgradeForm.getEquityAnalyzerStartDate() ) );
			}
			break;
		}
        default:
            break;
    }
}

}
