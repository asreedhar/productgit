package com.firstlook.entity.form;

import java.text.DecimalFormat;
import java.util.Date;

import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.transact.persist.model.ThirdPartyOptionType;
import biz.firstlook.transact.persist.model.ThirdPartyVehicleOption;

/*
 * 
 * An explanation of what is going on with the KBB specific methods.
 * 
 * With respect to KBBDBRolloutDate and isPostKBBDBBookout:
 * 	    In the old kbb stuff (pre kbb db) we were not saving whether or not an option was standard.
 * 		this meant that when we wanted to display the kbb print outs, we had to have funny logic to
 * 		figure out whether or not to display the options value or the string 'included'.  So when we 
 * 		implemented the new KBB stuff, we couldn't just blow away this logic becuase of the legacy bookouts.
 * 		so we had to have both sets of logic live side by side.  We should be able to delete the logic in the 
 * 		else blocks of the if (isPostKBBDBBookout()) pretty soon after the slated roll out of the kbb db on 4/17/07
 * 
 * With respect to the display logic for the KBB methods:
 * 		In KBB standard options that are selected are displayed as 'included' as opposed to 0.  so when displaying a kbb
 * 		value - first check to see if the option is selected (isStatus == true) and it is standard display 'Included'
 * 		if the option is negative wrap the value with <>'s, otherwise just display it.
 * 
 * 		-DW 4/10/07
 */

public class ThirdPartyVehicleOptionForm extends com.firstlook.action.BaseActionForm
{

private static final long serialVersionUID = 7873488372100691983L;
private ThirdPartyVehicleOption thirdPartyVehicleOption;
private static Date KBBDBRollOutDate = null;
public static final String COMMA_NUMBER = "#,###,###,###,##0";

public ThirdPartyVehicleOptionForm()
{
	setBusinessObject( new ThirdPartyVehicleOption() );
}

public ThirdPartyVehicleOptionForm( ThirdPartyVehicleOption option )
{
	setBusinessObject( option );
}

public String getOptionName()
{
	return thirdPartyVehicleOption.getOption().getOptionName();
}

public String getOptionKey()
{
	return thirdPartyVehicleOption.getOption().getOptionKey();
}

public String getDescription()
{
	return thirdPartyVehicleOption.getOptionName();
}

// see comment at top of the file
public String getDescriptionKelleyPrintout()
{
	if ( isPostKBBDBBookout() )
	{
		if ( thirdPartyVehicleOption.isStandardOption() && !thirdPartyVehicleOption.isStatus() )
		{
			return "No " + thirdPartyVehicleOption.getOptionName();
		}
		return thirdPartyVehicleOption.getOptionName();
	}
	else
	{
		if ( thirdPartyVehicleOption.getRetailValue() < 0 && !thirdPartyVehicleOption.isStatus() )
		{
			return "No " + thirdPartyVehicleOption.getOptionName();
		}
		return thirdPartyVehicleOption.getOptionName();
	}
}

public java.lang.String getKey()
{
	return thirdPartyVehicleOption.getOptionKey();
}

public String getValue()
{
	return String.valueOf( thirdPartyVehicleOption.getValue() );
}

// see comment at top of the file
public String getRetailValueKelleyPrintout()
{
	// don't display 0's as prices for engine/drivetrain/transmission
	if ( (thirdPartyVehicleOption.getOption().getThirdPartyOptionType().equals(ThirdPartyOptionType.EQUIPMENT)) && 
			( thirdPartyVehicleOption.getRetailValue() == 0 ) )
	{
		return "Included";
	}
	
	if ( isPostKBBDBBookout() )
	{
		if ( thirdPartyVehicleOption.isStandardOption() && thirdPartyVehicleOption.isStatus() )
		{
			return "Included";
		}
		
		if ( thirdPartyVehicleOption.getRetailValue() < 0 )
		{
			return "<" + String.valueOf( formatNumberWithComma( Math.abs( thirdPartyVehicleOption.getRetailValue() ) ) ) + ">";
		}
		return String.valueOf( formatNumberWithComma( thirdPartyVehicleOption.getRetailValue() ) );
	}
	else
	{
		if ( thirdPartyVehicleOption.getRetailValue() == 0 )
		{
			return "Included";
		}
		if ( thirdPartyVehicleOption.getRetailValue() < 0 )
		{
			return "<" + String.valueOf( formatNumberWithComma( Math.abs( thirdPartyVehicleOption.getRetailValue() ) ) ) + ">";
		}
		return String.valueOf( formatNumberWithComma( thirdPartyVehicleOption.getRetailValue() ) );
	}
}

//see comment at top of the file
public String getTradeInValueKelleyPrintout()
{
	// don't display 0's as prices for engine/drivetrain/transmission
	if ( (thirdPartyVehicleOption.getOption().getThirdPartyOptionType().equals(ThirdPartyOptionType.EQUIPMENT)) && 
			( thirdPartyVehicleOption.getTradeInValue() == 0 ) )
	{
		return "Included";
	}
	
	if ( isPostKBBDBBookout() )
	{
		if ( thirdPartyVehicleOption.isStandardOption() && thirdPartyVehicleOption.isStatus() )
		{
			return "Included";
		}
		if ( thirdPartyVehicleOption.getTradeInValue() < 0 )
		{
			return "<" + String.valueOf( formatNumberWithComma( Math.abs( thirdPartyVehicleOption.getTradeInValue() ) ) ) + ">";
		}
		return String.valueOf( formatNumberWithComma( thirdPartyVehicleOption.getTradeInValue() ) );
	}
	else
	{
		if ( thirdPartyVehicleOption.getTradeInValue() == 0 )
		{
			return "Included";
		}
		if ( thirdPartyVehicleOption.getTradeInValue() < 0 )
		{
			return "<" + String.valueOf( formatNumberWithComma( Math.abs( thirdPartyVehicleOption.getTradeInValue() ) ) ) + ">";
		}
		return String.valueOf( formatNumberWithComma( thirdPartyVehicleOption.getTradeInValue() ) );
	}
}

//see comment at top of the file
public String getWholesaleValueKelleyPrintout()
{
	// don't display 0's as prices for engine/drivetrain/transmission
	if ( (thirdPartyVehicleOption.getOption().getThirdPartyOptionType().equals(ThirdPartyOptionType.EQUIPMENT)) && 
			( thirdPartyVehicleOption.getWholesaleValue() == 0 ) )
	{
		return "Included";
	}
	
	if ( isPostKBBDBBookout() )
	{
		if (thirdPartyVehicleOption.isStandardOption() && thirdPartyVehicleOption.isStatus())
		{
			return "Included";
		}
		if ( thirdPartyVehicleOption.getWholesaleValue() < 0 )
		{
			return "<" + String.valueOf( formatNumberWithComma( Math.abs( thirdPartyVehicleOption.getWholesaleValue() ) ) ) + ">";
		}
		return String.valueOf( formatNumberWithComma( thirdPartyVehicleOption.getWholesaleValue() ) );
	}
	else
	{
		if ( thirdPartyVehicleOption.getWholesaleValue() == 0 )
		{
			return "Included";
		}
		if ( thirdPartyVehicleOption.getWholesaleValue() < 0 )
		{
			return "<" + String.valueOf( formatNumberWithComma( Math.abs( thirdPartyVehicleOption.getWholesaleValue() ) ) ) + ">";
		}
		return String.valueOf( formatNumberWithComma( thirdPartyVehicleOption.getWholesaleValue() ) );	
	}
}

// SPECIAL METHODS FOR KELLEY EQUIPMENT OPTIONS!!!!!
//see comment at top of the file
public boolean getDisplayEquipmentOptions()
{
	if (thirdPartyVehicleOption == null || thirdPartyVehicleOption.getOption() == null)
	{
		return false;
	}

	if( thirdPartyVehicleOption.getOption().getThirdPartyOptionType().equals(ThirdPartyOptionType.EQUIPMENT) )
	{
		
		if ( isPostKBBDBBookout() )
		{
			if ( thirdPartyVehicleOption.isStandardOption() )
			{
				return true;
			}
			return thirdPartyVehicleOption.isStatus();
		}
		else
		{
			if ( thirdPartyVehicleOption.getRetailValue() >= 0 )
			{
				return thirdPartyVehicleOption.getStatus();
			}
			if ( thirdPartyVehicleOption.getRetailValue() < 0 )
			{
				return true;
			}
		}
	}
	// we don't want to display non-equipment options on this page
	return false;
}

//see comment at top of the file
public String getEquipmentRetailValueKelleyPrintout()
{
	// don't display 0's as prices for engine/drivetrain/transmission
	if ( (thirdPartyVehicleOption.getOption().getThirdPartyOptionType().equals(ThirdPartyOptionType.EQUIPMENT)) && 
			( thirdPartyVehicleOption.getRetailValue() == 0 ) )
	{
		return "Included";
	}
	
	if ( isPostKBBDBBookout() )
	{
		if ( thirdPartyVehicleOption.isStandardOption() && thirdPartyVehicleOption.isStatus())
		{
			return "Included";
		}
		if ( thirdPartyVehicleOption.getRetailValue() < 0 )
		{
			return "<" + String.valueOf( formatNumberWithComma( Math.abs( thirdPartyVehicleOption.getRetailValue() ) ) ) + ">";
		}
		return String.valueOf( formatNumberWithComma( thirdPartyVehicleOption.getRetailValue() ) );
	}
	else
	{
		if ( thirdPartyVehicleOption.getRetailValue() == 0 || 
			( thirdPartyVehicleOption.getRetailValue() < 0 && thirdPartyVehicleOption.getStatus() ) )
		{
			return "Included";
		}
		if ( thirdPartyVehicleOption.getRetailValue() < 0 && !thirdPartyVehicleOption.getStatus() )
		{
			return "<" + String.valueOf( formatNumberWithComma( Math.abs( thirdPartyVehicleOption.getRetailValue() ) ) ) + ">";
		}
		return String.valueOf( formatNumberWithComma( thirdPartyVehicleOption.getRetailValue() ) );	
	}
}

//see comment at top of the file
public String getEquipmentTradeInValueKelleyPrintout()
{
	// don't display 0's as prices for engine/drivetrain/transmission
	if ( (thirdPartyVehicleOption.getOption().getThirdPartyOptionType().equals(ThirdPartyOptionType.EQUIPMENT)) && 
			( thirdPartyVehicleOption.getTradeInValue() == 0 ) )
	{
		return "Included";
	}
	
	if ( isPostKBBDBBookout() )
	{
		if ( thirdPartyVehicleOption.isStandardOption() && thirdPartyVehicleOption.isStatus())
		{
			return "Included";
		}
		if ( thirdPartyVehicleOption.getTradeInValue() < 0 )
		{
			return "<" + String.valueOf( formatNumberWithComma( Math.abs( thirdPartyVehicleOption.getTradeInValue() ) ) ) + ">";
		}
		return String.valueOf( formatNumberWithComma( thirdPartyVehicleOption.getTradeInValue() ) );
	}
	else
	{
		if ( thirdPartyVehicleOption.getTradeInValue() == 0 || 
			( thirdPartyVehicleOption.getTradeInValue() < 0 && thirdPartyVehicleOption.getStatus() ) )
		{
			return "Included";
		}
		if ( thirdPartyVehicleOption.getWholesaleValue() < 0 && !thirdPartyVehicleOption.getStatus() )
		{
			return "<" + String.valueOf( formatNumberWithComma( Math.abs( thirdPartyVehicleOption.getTradeInValue() ) ) ) + ">";
		}
		return String.valueOf( formatNumberWithComma( thirdPartyVehicleOption.getTradeInValue() ) );
	}
}

//see comment at top of the file
public String getEquipmentWholesaleValueKelleyPrintout()
{
	// don't display 0's as prices for engine/drivetrain/transmission
	if ( (thirdPartyVehicleOption.getOption().getThirdPartyOptionType().equals(ThirdPartyOptionType.EQUIPMENT)) && 
			( thirdPartyVehicleOption.getWholesaleValue() == 0 ) )
	{
		return "Included";
	}
	
	if ( isPostKBBDBBookout() )
	{
		if ( thirdPartyVehicleOption.isStandardOption() && thirdPartyVehicleOption.isStatus())
		{
				return "Included";
		}
		if ( thirdPartyVehicleOption.getWholesaleValue() < 0 )
		{
				return "<" + String.valueOf( formatNumberWithComma( Math.abs( thirdPartyVehicleOption.getWholesaleValue() ) ) ) + ">";
		}
		return String.valueOf( formatNumberWithComma( thirdPartyVehicleOption.getWholesaleValue() ) );
	}
	else
	{
		if ( thirdPartyVehicleOption.getWholesaleValue() == 0 || 
			( thirdPartyVehicleOption.getWholesaleValue() < 0 && thirdPartyVehicleOption.getStatus() ) )
		{
			return "Included";
		}
		if ( thirdPartyVehicleOption.getWholesaleValue() < 0 && !thirdPartyVehicleOption.getStatus() )
		{
			return "<" + String.valueOf( formatNumberWithComma( Math.abs( thirdPartyVehicleOption.getWholesaleValue() ) ) ) + ">";
		}
		return String.valueOf( formatNumberWithComma( thirdPartyVehicleOption.getWholesaleValue() ) );
	}
}

//see comment at top of the file
public boolean getIsEquipmentIncludedOption()
{
	return getEquipmentWholesaleValueKelleyPrintout().equalsIgnoreCase( "Included" );
}
// END OF SPECIAL EQUIPMENT OPTIONS METHODS

public boolean isIncludedOption()
{
	return getRetailValueKelleyPrintout().equalsIgnoreCase( "Included" );
}

public boolean getIsIncludedOption()
{
	return getRetailValueKelleyPrintout().equalsIgnoreCase( "Included" );
}

public String getValueFormatted()
{
	return formatPriceToString( thirdPartyVehicleOption.getValue() );
}

public void setBusinessObject( Object o )
{
	thirdPartyVehicleOption = (ThirdPartyVehicleOption)o;
}

public void setKey( java.lang.String newKey )
{
	thirdPartyVehicleOption.getOption().setOptionKey( newKey );
}

public int getTabOrder()
{
	return thirdPartyVehicleOption.getTabOrder();
}

public void setTabOrder( int i )
{
	thirdPartyVehicleOption.setTabOrder( i );
}

public boolean isSelected()
{
	return thirdPartyVehicleOption.isStatus();
}

public boolean isStandardOption()
{
	return thirdPartyVehicleOption.isStandardOption();
}

private String formatNumberWithComma( int number )
{
	DecimalFormat decimalFormat = new DecimalFormat( COMMA_NUMBER );
	return decimalFormat.format( number );
}

//see comment at top of the file
private boolean isPostKBBDBBookout()
{
	if ( KBBDBRollOutDate == null )
	{
		KBBDBRollOutDate = new Date( PropertyLoader.getProperty("firstlook.KBBDBRollOutDate") );
	}
	if ( thirdPartyVehicleOption.getDateCreated() != null )
	{
		if ( thirdPartyVehicleOption.getDateCreated().after( KBBDBRollOutDate ) ) {
			return true;
		} else {
			return false;
		}
	}
	// should never reach here!!!
	return false;
}

}
