package com.firstlook.entity.form;

import java.util.List;

import com.firstlook.action.BaseActionForm;

public class AccessGroupsForm extends BaseActionForm
{

private static final long serialVersionUID = -2415225830957483984L;
private List accessGroups;


public List getAccessGroups()
{
	return accessGroups;
}

public void setAccessGroups( List accessGroups )
{
	this.accessGroups = accessGroups;
}


}
