package com.firstlook.entity.form;

import biz.firstlook.transact.persist.model.BookOutValue;

import com.firstlook.action.BaseActionForm;
import com.firstlook.util.Formatter;

public class TradeAnalyzerPlusBookValuesForm extends BaseActionForm
{
private static final long serialVersionUID = 4451324884783815242L;
private BookOutValue businessObject;

public TradeAnalyzerPlusBookValuesForm()
{
    setBusinessObject(new BookOutValue());
}

public TradeAnalyzerPlusBookValuesForm(
        BookOutValue tradeAnalyzerPlusBookValues )
{
    setBusinessObject(tradeAnalyzerPlusBookValues);
}

/**
 * Returns the title.
 * 
 * @return String
 */
public String getTitle()
{
    return businessObject.getThirdPartyCategory().getThirdPartyCategory().getCategory();
}

/**
 * Returns the tradeAnalyzerPlusBookValuesId.
 * 
 * @return Integer
 */
public Integer getTradeAnalyzerPlusBookValuesId()
{
	   return businessObject.getBookOutValueId();
}

/**
 * Returns the value.
 * 
 * @return Integer
 */
public Integer getValue()
{
    return businessObject.getValue();
}

public String getValueFormatted()
{
    String valueFormatted = Formatter.NOT_AVAILABLE;

    if ( getValue() != null && getValue().intValue() > 0 )
    {
        valueFormatted = "$"
                + Formatter.formatNumberToString(getValue().intValue());
    }

    return valueFormatted;
}


/**
 * Sets the value.
 * 
 * @param value
 *            The value to set
 */
public void setValue( Integer value )
{
    businessObject.setValue(value);
}

public void setBusinessObject( Object obj )
{
    businessObject = (BookOutValue) obj;
}

}
