package com.firstlook.entity.form;

public class TestPhoneNumberFormHelper extends com.firstlook.mock.BaseTestCase
{
/**
 * TestPhoneNumberFormHelper constructor comment.
 */
public TestPhoneNumberFormHelper( String name )
{
    super(name);
}

public void testConstructorWithNull()
{
    PhoneNumberFormHelper form = new PhoneNumberFormHelper(null);
    assertEquals("", form.getAreaCode());
    assertEquals("", form.getPrefix());
    assertEquals("", form.getSuffix());
    assertEquals("", form.getPhoneNumber());
}

public void testConstructorWithString()
{
    PhoneNumberFormHelper pnfh = new PhoneNumberFormHelper("1234567890");
    assertEquals("Area Code", "123", pnfh.getAreaCode());
    assertEquals("Prefix", "456", pnfh.getPrefix());
    assertEquals("Suffix", "7890", pnfh.getSuffix());

}

public void testGetPhoneNumber()
{
    PhoneNumberFormHelper phoneNumber = new PhoneNumberFormHelper();
    phoneNumber.setAreaCode("312");
    phoneNumber.setPrefix("696");
    phoneNumber.setSuffix("5000");

    assertEquals("3126965000", phoneNumber.getPhoneNumber());
}
}
