package com.firstlook.entity.form;

import org.apache.struts.action.ActionForm;

public class VehicleIdentificationForm extends ActionForm {

	private static final long serialVersionUID = 8787511682654712650L;

	private String vin;
	private boolean catalogKeyKnown;
	
	
	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public boolean isCatalogKeyKnown() {
		return catalogKeyKnown;
	}

	public void setCatalogKeyKnown(boolean catalogKeyKnown) {
		this.catalogKeyKnown = catalogKeyKnown;
	}
	
	


}
