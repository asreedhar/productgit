package com.firstlook.entity.form;

import java.util.ArrayList;

import org.apache.struts.action.ActionErrors;

import biz.firstlook.commons.util.MockPropertyFinder;
import biz.firstlook.commons.util.PropertyLoader;
import biz.firstlook.transact.persist.model.ThirdPartyCategory;

import com.firstlook.dealer.tools.GuideBook;
import com.firstlook.entity.Dealer;
import com.firstlook.entity.DealerFranchise;
import com.firstlook.mock.BaseTestCase;
import com.firstlook.mock.DummyActionMapping;
import com.firstlook.mock.DummyHttpRequest;
import com.firstlook.mock.ObjectMother;

public class TestDealerForm extends BaseTestCase
{

private DummyHttpRequest request;
private DummyActionMapping mapping;
private DealerForm form;
private ActionErrors errors;
private MockPropertyFinder propFinder;

public TestDealerForm( String arg1 )
{
    super(arg1);
}

public void setup()
{
    request = new DummyHttpRequest();
    mapping = new DummyActionMapping();
    form = new DealerForm();
    form.setFranchises(new ArrayList());
    errors = new ActionErrors();

    propFinder = new MockPropertyFinder();
    propFinder.setStringProperty("dealer.default.message", "hello");
    ArrayList list = new ArrayList();
    list.add(propFinder);
    PropertyLoader.setPropertyFinders(list);
}

public void tearDown()
{
    PropertyLoader.setPropertyFinders(null);
}

public void testDealerDefaultMessage()
{
    Dealer dealer = new Dealer();
    dealer.setCustomHomePageMessage(null);
    DealerForm df = new DealerForm(dealer);
    String message = df.getCustomHomePageMessage();
    assertNotNull("No default message set", message);
}

public void testDealerFormForCorrectDealer() throws Exception
{
    Dealer dealer = ObjectMother.createDealer();
    dealer.setDealerId(new Integer(0));
    DealerForm df = new DealerForm(dealer);

    assertEquals(dealer, df.getDealer());
}

public void testGetTrendingViewDescription() throws java.lang.Exception
{
    form.setDefaultTrendingView(Dealer.TRENDING_VIEW_FASTEST_SELLER);
    assertEquals("fastest seller", DealerForm.TRENDING_VIEW_FASTEST_SELLER,
            form.getTrendingViewDescription());

    form.setDefaultTrendingView(Dealer.TRENDING_VIEW_MOST_PROFITABLE);
    assertEquals("profit", DealerForm.TRENDING_VIEW_MOST_PROFITABLE, form
            .getTrendingViewDescription());

    form.setDefaultTrendingView(Dealer.TRENDING_VIEW_TOP_SELLER);
    assertEquals("top seller", DealerForm.TRENDING_VIEW_TOP_SELLER, form
            .getTrendingViewDescription());
}

public void testGetNickNameCaps()
{
    form.setNickname("dealer1");
    assertEquals("Nickname is not in caps", "DEALER1", form.getNicknameCaps());
}

public void testValidateAddress1()
{
    errors = form.validate(mapping, request);

    assertTrue(isInActionErrors(errors, "error.admin.dealer.address1"));
}

public void testValidateCity()
{
    errors = form.validate(mapping, request);

    assertTrue(isInActionErrors(errors, "error.admin.dealer.city"));
}

public void testValidateDealerName()
{
    errors = form.validate(mapping, request);

    assertTrue(isInActionErrors(errors, "error.admin.dealer.name"));
}

public void testValidateNickname()
{
    errors = form.validate(mapping, request);

    assertTrue(isInActionErrors(errors, "error.admin.dealer.nickname"));
}

public void testValidateState()
{
    errors = form.validate(mapping, request);

    assertTrue(isInActionErrors(errors, "error.admin.dealer.state"));
}

public void testValidateInceptionDate()
{
    errors = form.validate(mapping, request);

    assertTrue(isInActionErrors(errors, "error.admin.dealer.inceptiondate"));
}

public void testValidateZipcode()
{
    form.validateZipcode(errors, null);

    assertTrue(isInActionErrors(errors, "error.admin.dealer.zipcode"));
}

public void testValidateZipcodeIsNumeric()
{
    form.validateZipcode(errors, "ABCDE");

    assertTrue(isInActionErrors(errors, "error.admin.dealer.zipcode"));
}

public void testValidateZipcodeLessThan5Digits()
{
    form.validateZipcode(errors, "1234");

    assertTrue(isInActionErrors(errors, "error.admin.dealer.zipcode"));
}

public void testValidateZipcodeMoreThan5Digits()
{
    form.validateZipcode(errors, "123456");

    assertTrue(isInActionErrors(errors, "error.admin.dealer.zipcode"));
}

public void testValidateAgeBandTargetsNotEqual()
{
    form.setAgeBandTarget1(50);
    form.setAgeBandTarget2(0);
    form.setAgeBandTarget3(0);
    form.setAgeBandTarget4(0);
    form.setAgeBandTarget5(0);
    form.setAgeBandTarget6(0);
    form.validateAgeBandTarget(errors);

    assertTrue(isInActionErrors(errors, "error.admin.dealer.agebandtarget"));
}

public void testValidateAgeBandTargets()
{
    form.setAgeBandTarget1(50);
    form.setAgeBandTarget2(10);
    form.setAgeBandTarget3(10);
    form.setAgeBandTarget4(10);
    form.setAgeBandTarget5(10);
    form.setAgeBandTarget6(10);
    form.validateAgeBandTarget(errors);

    assertTrue(!isInActionErrors(errors, "error.admin.dealer.agebandtarget"));
}

public void testValidateDaysSupplyWeightLessThan100()
{
    form.setDaysSupply12WeekWeight(10);
    form.setDaysSupply26WeekWeight(10);
    form.validateDaysSupplyWeight(errors);

    assertTrue(isInActionErrors(errors, "error.admin.dealer.dayssupplyweight"));
}

public void testValidateDaysSupplyWeightGreaterThan100()
{
    form.setDaysSupply12WeekWeight(100);
    form.setDaysSupply26WeekWeight(10);
    form.validateDaysSupplyWeight(errors);

    assertTrue(isInActionErrors(errors, "error.admin.dealer.dayssupplyweight"));
}

public void testValidateDaysSupplyWeight()
{
    form.setDaysSupply12WeekWeight(70);
    form.setDaysSupply26WeekWeight(30);
    form.validateDaysSupplyWeight(errors);

    assertTrue(!isInActionErrors(errors, "error.admin.dealer.dayssupplyweight"));
}

public void testValidateGuideBookPreferenceError()
{
    form.setGuideBookId(GuideBook.GUIDE_TYPE_BLACKBOOK);
    form
            .setBookOutPreferenceId(ThirdPartyCategory.KELLEY_RETAIL_TYPE);
    form.validateGuideBookPreferences(errors, form.getBookOutPreferenceId(),
            form.getGuideBookId());

    assertTrue(isInActionErrors(errors,
            "error.admin.dealer.guidebookpreferences"));
}

public void testValidateGuideBookPreferenceNoError()
{
    form.setGuideBookId(GuideBook.GUIDE_TYPE_BLACKBOOK);
    form
            .setBookOutPreferenceId(ThirdPartyCategory.BLACKBOOK_AVERAGE_TYPE);
    form.validateGuideBookPreferences(errors, form.getBookOutPreferenceId(),
            form.getGuideBookId());

    assertTrue(!isInActionErrors(errors,
            "error.admin.dealer.guidebookpreferences"));
}

public void testValidateGuideBookPreferenceNotTheSame()
{
    form.setBookOutPreferenceId(2);
    form.setBookOutPreferenceSecondId(2);
    form.setGuideBook2SecondBookOutPreferenceId(0);

    form.validateGuideBookPreferenceNotTheSame(errors);

    assertTrue(isInActionErrors(errors,
            "error.admin.dealer.guidebookpreferencessame"));
}

public void testValidateGuideBookPreferenceNotTheSameZeros()
{
    form.setBookOutPreferenceId(0);
    form.setBookOutPreferenceSecondId(0);
    form.setGuideBook2SecondBookOutPreferenceId(0);

    form.validateGuideBookPreferenceNotTheSame(errors);

    assertTrue(!isInActionErrors(errors,
            "error.admin.dealer.guidebookpreferencessame"));
}

public void testValidateGuideBookPreferenceNotTheSameDifferent()
{
    form.setBookOutPreferenceId(1);
    form.setBookOutPreferenceSecondId(2);
    form.setGuideBook2SecondBookOutPreferenceId(0);

    form.validateGuideBookPreferenceNotTheSame(errors);

    assertTrue(!isInActionErrors(errors,
            "error.admin.dealer.guidebookpreferencessame"));
}

public void testSetFranchisesByIdSingleId()
{
    String[] franchiseId =
    { "1" };
    form.setFranchisesById(franchiseId);

    DealerFranchise franchise = (DealerFranchise) form.getFranchises()
            .toArray()[0];

    assertEquals(1, franchise.getFranchiseId());
}

public void testSetFranchisesByIdMultipleIds()
{
    String[] franchiseId =
    { "1", "2" };
    form.setFranchisesById(franchiseId);

    DealerFranchise firstFranchise = (DealerFranchise) form.getFranchises()
            .toArray()[0];
    DealerFranchise nextFranchise = (DealerFranchise) form.getFranchises()
            .toArray()[1];

    assertEquals(1, firstFranchise.getFranchiseId());
    assertEquals(2, nextFranchise.getFranchiseId());

}

public void testSetFranchisesByIdNoId()
{
    String[] lackOfFranchiseId = {};
    form.setFranchisesById(lackOfFranchiseId);

    assertEquals(0, form.getFranchises().size());
}

public void testSetFranchisesByIdNull()
{
    String[] lackOfFranchiseId = null;
    form.setFranchisesById(lackOfFranchiseId);

    assertEquals(0, form.getFranchises().size());
}

}
