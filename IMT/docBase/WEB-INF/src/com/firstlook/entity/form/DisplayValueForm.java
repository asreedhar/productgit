package com.firstlook.entity.form;

import com.firstlook.action.BaseActionForm;

public class DisplayValueForm extends BaseActionForm
{

private static final long serialVersionUID = -3692767543993467650L;
private Object value;

public DisplayValueForm()
{
    super();
}

public String getValue()
{
    String returnValue = "";

    if ( value != null )
    {
        returnValue = value.toString();
    }

    return returnValue;
}

public void setValue( String newValue )
{
    value = newValue;
}

public void setBusinessObject( Object newObj )
{
    value = newObj;
}

}
