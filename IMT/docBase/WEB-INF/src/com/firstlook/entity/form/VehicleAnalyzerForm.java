package com.firstlook.entity.form;

import com.firstlook.action.BaseActionForm;

public class VehicleAnalyzerForm extends BaseActionForm
{

private static final long serialVersionUID = 456671420807666751L;
private String bodyStyle;
private String groupingDescription;
private String make;
private String model;
private String trim;
private int bodyStyleId;
private int groupingDescriptionId;
private int includeDealerGroup;

public VehicleAnalyzerForm()
{
    super();
}

public int getIncludeDealerGroup()
{
    return includeDealerGroup;
}

public String getMake()
{
    return make;
}

public String getMakeUpper()
{
    return make.toUpperCase();
}

public String getModel()
{
    return model;
}

public String getModelUpper()
{
    return model.toUpperCase();
}

public String getTrim()
{
    if ( trim != null && !trim.equalsIgnoreCase("null") )
    {
        return trim;
    } else
    {
        return "";
    }
}

public String getTrimUpper()
{
    return trim.toUpperCase();
}

public void setIncludeDealerGroup( int includeDealerGroup )
{
    this.includeDealerGroup = includeDealerGroup;
}

public void setMake( String make )
{
    this.make = make;
}

public void setModel( String model )
{
    this.model = model;
}

public void setTrim( String trim )
{
    this.trim = trim;
}

public int getGroupingDescriptionId()
{
    return groupingDescriptionId;
}

public void setGroupingDescriptionId( int groupingDescriptionId )
{
    this.groupingDescriptionId = groupingDescriptionId;
}

public String getBodyStyle()
{
    return bodyStyle;
}

public void setBodyStyle( String string )
{
    bodyStyle = string;
}

public int getBodyStyleId()
{
    return bodyStyleId;
}

public void setBodyStyleId( int i )
{
    bodyStyleId = i;
}

public String getBody()
{
    return getBodyStyle();
}

public void setBody( String body )
{
    setBodyStyle(body);
}

public String getGroupingDescription()
{
    return groupingDescription;
}

public void setGroupingDescription( String string )
{
    groupingDescription = string;
}

public String getIncludeDealerGroupStr()
{
    return String.valueOf(includeDealerGroup);
}

}