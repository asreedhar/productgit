package com.firstlook.entity.form;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

public class TargetSupplySearchForm extends com.firstlook.action.BaseActionForm
{
private static final long serialVersionUID = -2657665272114015437L;
private int marketId;
private java.lang.String make;
private java.lang.String model;
private int numberOfDays;

public TargetSupplySearchForm()
{
    super();
    setMake("");
}

/**
 * Insert the method's description here. Creation date: (3/8/2002 2:38:15 PM)
 * 
 * @return java.lang.String
 */
public java.lang.String getMake()
{
    return make;
}

/**
 * Insert the method's description here. Creation date: (3/8/2002 2:38:05 PM)
 * 
 * @return int
 */
public int getMarketId()
{
    return marketId;
}

/**
 * Insert the method's description here. Creation date: (3/8/2002 2:38:24 PM)
 * 
 * @return java.lang.String
 */
public java.lang.String getModel()
{
    return model;
}

/**
 * Insert the method's description here. Creation date: (3/8/2002 2:38:39 PM)
 * 
 * @return int
 */
public int getNumberOfDays()
{
    return numberOfDays;
}

/**
 * Insert the method's description here. Creation date: (3/8/2002 2:38:15 PM)
 * 
 * @param newMake
 *            java.lang.String
 */
public void setMake( java.lang.String newMake )
{
    make = newMake;
}

/**
 * Insert the method's description here. Creation date: (3/8/2002 2:38:05 PM)
 * 
 * @param newMarketId
 *            int
 */
public void setMarketId( int newMarketId )
{
    marketId = newMarketId;
}

/**
 * Insert the method's description here. Creation date: (3/8/2002 2:38:24 PM)
 * 
 * @param newModel
 *            java.lang.String
 */
public void setModel( java.lang.String newModel )
{
    model = newModel;
}

/**
 * Insert the method's description here. Creation date: (3/8/2002 2:38:39 PM)
 * 
 * @param newNumberOfDays
 *            int
 */
public void setNumberOfDays( int newNumberOfDays )
{
    numberOfDays = newNumberOfDays;
}

public ActionErrors validate( ActionMapping mapping, HttpServletRequest request )
{
    ActionErrors ae = new ActionErrors();
    validateMake(ae);
    validateModel(ae);

    return ae;
}

void validateMake( ActionErrors errors )
{
    if ( make == null || make.equals("") )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.admin.supply.search.make"));
    }
}

void validateModel( ActionErrors errors )
{
    if ( model == null || model.equals("") )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.admin.supply.search.model"));
    }
}
}
