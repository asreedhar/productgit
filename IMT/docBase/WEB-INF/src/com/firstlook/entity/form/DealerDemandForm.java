package com.firstlook.entity.form;

public class DealerDemandForm extends DealerForm
{
private static final long serialVersionUID = -3025125832597865611L;
private int topSellerRanking;
private int mostProfitableRanking;
private int fastestSellerRanking;
private boolean hotList;

public DealerDemandForm()
{
    super();
}

public DealerDemandForm( com.firstlook.entity.Dealer newDealer )
{
    super(newDealer);
}

public int getFastestSellerRanking()
{
    return fastestSellerRanking;
}

public int getMostProfitableRanking()
{
    return mostProfitableRanking;
}

public String getRankingsFormatted()
{
    StringBuffer rankings = new StringBuffer();

    if ( getTopSellerRanking() != 0 )
    {
        rankings.append(getRankingsSeparator(rankings) + "#"
                + getTopSellerRanking() + " Top Seller");
    }
    if ( getMostProfitableRanking() != 0 )
    {
        rankings.append(getRankingsSeparator(rankings) + "#"
                + getMostProfitableRanking() + " Most Profitable");
    }
    if ( getFastestSellerRanking() != 0 )
    {
        rankings.append(getRankingsSeparator(rankings) + "#"
                + getFastestSellerRanking() + " Fastest Seller");
    }
    if ( isHotList() )
    {
        rankings.append(getRankingsSeparator(rankings) + "Hot List");
    }

    return rankings.toString();
}

protected String getRankingsSeparator( StringBuffer rankings )
{
    if ( rankings == null || rankings.length() == 0 )
    {
        return "";
    } else
    {
        return ", ";
    }
}

public int getTopSellerRanking()
{
    return topSellerRanking;
}

public boolean isHotList()
{
    return hotList;
}

public void setFastestSellerRanking( int newFastestSellerRanking )
{
    fastestSellerRanking = newFastestSellerRanking;
}

public void setHotList( boolean newHotList )
{
    hotList = newHotList;
}

public void setMostProfitableRanking( int newMostProfitableRanking )
{
    mostProfitableRanking = newMostProfitableRanking;
}

public void setTopSellerRanking( int newTopSellerRanking )
{
    topSellerRanking = newTopSellerRanking;
}
}
