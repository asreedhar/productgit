package com.firstlook.entity.form;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import com.firstlook.action.BaseActionForm;
import com.firstlook.entity.Corporation;

public class CorporationForm extends BaseActionForm
{

private static final long serialVersionUID = -776109806070372324L;
private Corporation corporation;
private PhoneNumberFormHelper phone;
private PhoneNumberFormHelper fax;
private Collection regions;

public CorporationForm()
{
    setBusinessObject(new Corporation());
}

public CorporationForm( Corporation corp )
{
    setBusinessObject(corp);

}

public void setBusinessObject( Object bo )
{
    corporation = (Corporation) bo;
    phone = new PhoneNumberFormHelper(corporation.getOfficePhone());
    fax = new PhoneNumberFormHelper(corporation.getOfficeFax());
}

public boolean isActive()
{
    return corporation.isActive();
}

public String getAddress1()
{
    return corporation.getAddress1();
}

public String getAddress2()
{
    return corporation.getAddress2();
}

public String getCity()
{
    return corporation.getCity();
}

public String getCorporationCode()
{
    return corporation.getCorporationCode();
}

public int getCorporationId()
{
    if ( corporation.getCorporationId() != null )
    {
        return corporation.getCorporationId().intValue();
    } else
    {
        return 0;
    }
}

public String getName()
{
    return corporation.getName();
}

public String getNickname()
{
    return corporation.getNickname();
}

public String getPhoneAreaCode()
{
    return phone.getAreaCode();
}

public void setPhoneAreaCode( String newPhoneAreaCode )
{
    phone.setAreaCode(newPhoneAreaCode);
}

public void setPhonePrefix( String newPhonePrefix )
{
    phone.setPrefix(newPhonePrefix);
}

public void setPhoneSuffix( String newPhoneSuffix )
{
    phone.setSuffix(newPhoneSuffix);
}

public void setFaxAreaCode( String newFaxAreaCode )
{
    fax.setAreaCode(newFaxAreaCode);
}

public void setFaxPrefix( String newFaxPrefix )
{
    fax.setPrefix(newFaxPrefix);
}

public void setFaxSuffix( String newFaxSuffix )
{
    fax.setSuffix(newFaxSuffix);
}

public String getOfficeFax()
{
    return fax.getPhoneNumber();
}

public ActionErrors validate( ActionMapping mapping, HttpServletRequest request )
{
    ActionErrors errors = new ActionErrors();
    if ( isNullOrEmptyOrWhiteSpace(this.getName()) )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.admin.corporation.name"));
    }
    if ( isNullOrEmptyOrWhiteSpace(this.getNickname()) )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.admin.corporation.nickname"));
    }
    if ( isNullOrEmptyOrWhiteSpace(this.getAddress1()) )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.admin.corporation.address1"));
    }
    if ( isNullOrEmptyOrWhiteSpace(this.getCity()) )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.admin.corporation.city"));
    }
    if ( isNullOrEmptyOrWhiteSpace(this.getState()) )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                "error.admin.corporation.state"));
    }

    validateZipcode(errors, this.getZipCode());

    fax.validate(errors, ActionErrors.GLOBAL_ERROR, new ActionError(
            "error.admin.corporation.officefaxnumber"));
    phone.validate(errors, ActionErrors.GLOBAL_ERROR, new ActionError(
            "error.admin.corporation.officephonenumber"));

    return errors;
}

public void validateZipcode( ActionErrors errors, String zipcode )
{
    final String ZIPCODE_ERROR_MESSAGE_KEY = "error.admin.corporation.zipcode";

    if ( !checkIsNumeric(zipcode) || zipcode.length() != 5 )
    {
        errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                ZIPCODE_ERROR_MESSAGE_KEY));
    }
}

public String getOfficeFaxFormatted()
{
    return fax.getPhoneNumberFormatted();
}

public java.lang.String getFaxAreaCode()
{
    return fax.getAreaCode();
}

public String getFaxPrefix()
{
    return fax.getPrefix();
}

public String getFaxSuffix()
{
    return fax.getSuffix();
}

public String getPhonePrefix()
{
    return phone.getPrefix();
}

public String getPhoneSuffix()
{
    return phone.getSuffix();
}

public String getOfficePhone()
{
    return phone.getPhoneNumber();
}

public String getOfficePhoneFormatted()
{
    return phone.getPhoneNumberFormatted();
}

public String getState()
{
    return corporation.getState();
}

public String getZipCode()
{
    return corporation.getZipcode();
}

public void setActive( boolean b )
{
    corporation.setActive(b);
}

public void setAddress1( String string )
{
    corporation.setAddress1(string);
}

public void setAddress2( String string )
{
    corporation.setAddress2(string);
}

public void setCity( String string )
{
    corporation.setCity(string);
}

public void setCorporationCode( String string )
{
    corporation.setCorporationCode(string);
}

public void setCorporationId( int i )
{
    corporation.setCorporationId(new Integer(i));
}

public void setName( String string )
{
    corporation.setName(string);
}

public void setNickname( String string )
{
    corporation.setNickname(string);
}

public void setOfficeFax( String string )
{
    corporation.setOfficeFax(string);
}

public void setOfficePhone( String string )
{
    corporation.setOfficePhone(string);
}

public void setState( String string )
{
    corporation.setState(string);
}

public void setZipCode( String string )
{
    corporation.setZipcode(string);
}

public Collection getRegions()
{
    return regions;
}

public Corporation getCorporation()
{
    return corporation;
}

public void setCorporation( Corporation corporation )
{
	this.corporation = corporation;
}

public void setRegions( Collection regions )
{
	this.regions = regions;
}

}
