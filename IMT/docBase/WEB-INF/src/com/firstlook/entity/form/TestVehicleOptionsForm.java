package com.firstlook.entity.form;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.firstlook.entity.OptionDetail;
import com.firstlook.entity.VehicleOption;

public class TestVehicleOptionsForm extends com.firstlook.mock.BaseTestCase
{

public TestVehicleOptionsForm( String arg1 )
{
    super(arg1);
}

public void testVehicleOptionsDescriptionCollection()
{
    Collection vehicleOptions = new ArrayList();

    OptionDetail optionDetail1 = new OptionDetail();
    optionDetail1.setOptionName("Air Conditioning");

    OptionDetail optionDetail2 = new OptionDetail();
    optionDetail2.setOptionName("Power Windows");

    OptionDetail optionDetail3 = new OptionDetail();
    optionDetail3.setOptionName("Power Seats");

    VehicleOption vehicleOption1 = new VehicleOption();
    vehicleOption1.setOptionValue(true);
    vehicleOption1.setOptionDetail(optionDetail1);

    VehicleOption vehicleOption2 = new VehicleOption();
    vehicleOption2.setOptionValue(true);
    vehicleOption2.setOptionDetail(optionDetail2);

    VehicleOption vehicleOption3 = new VehicleOption();
    vehicleOption3.setOptionValue(true);
    vehicleOption3.setOptionDetail(optionDetail3);

    vehicleOptions.add(vehicleOption1);
    vehicleOptions.add(vehicleOption2);
    vehicleOptions.add(vehicleOption3);

    VehicleOptionsForm vof = new VehicleOptionsForm(vehicleOptions);

    Collection optionCollection = vof.getVehicleOptionsDescriptionCollection();

    Iterator optionsIterator = optionCollection.iterator();

    assertEquals("Air Conditioning", (String) optionsIterator.next());
    assertEquals("Power Windows", (String) optionsIterator.next());
    assertEquals("Power Seats", (String) optionsIterator.next());

}
}
