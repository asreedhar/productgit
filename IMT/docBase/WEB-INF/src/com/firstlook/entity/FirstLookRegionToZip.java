package com.firstlook.entity;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class FirstLookRegionToZip implements Serializable
{

private static final long serialVersionUID = -1917602677078180441L;
private int firstLookRegionId;
private String zipCode;

public FirstLookRegionToZip()
{
    super();
}

public int getFirstLookRegionId()
{
    return firstLookRegionId;
}

public String getZipCode()
{
    return zipCode;
}

public void setFirstLookRegionId( int i )
{
    firstLookRegionId = i;
}

public void setZipCode( String zip )
{
    zipCode = zip;
}

public boolean equals( Object obj )
{
    return EqualsBuilder.reflectionEquals(this, obj);
}

public int hashCode()
{
    return HashCodeBuilder.reflectionHashCode(13, 5, this);
}

}
