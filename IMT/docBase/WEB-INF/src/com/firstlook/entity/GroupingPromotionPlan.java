package com.firstlook.entity;

import java.util.Date;

public class GroupingPromotionPlan
{
private Integer groupingPromotionPlanId;
private Integer businessUnitId;
private Integer groupingDescriptionId;
private String groupingDescription;
private Date promotionStartDate;
private Date promotionEndDate;
private String notes;

// private Date lastModifiedDate;

public Integer getGroupingDescriptionId()
{
    return groupingDescriptionId;
}

public void setGroupingDescriptionId( Integer groupingDescriptionId )
{
    this.groupingDescriptionId = groupingDescriptionId;
}

public Integer getGroupingPromotionPlanId()
{
    return groupingPromotionPlanId;
}

public void setGroupingPromotionPlanId( Integer groupingPromotionPlanId )
{
    this.groupingPromotionPlanId = groupingPromotionPlanId;
}

public String getNotes()
{
    return notes;
}

public void setNotes( String notes )
{
    this.notes = notes;
}

public Date getPromotionEndDate()
{
    return promotionEndDate;
}

public void setPromotionEndDate( Date promotionEndDate )
{
    this.promotionEndDate = promotionEndDate;
}

public Date getPromotionStartDate()
{
    return promotionStartDate;
}

public void setPromotionStartDate( Date promotionStartDate )
{
    this.promotionStartDate = promotionStartDate;
}

public String getGroupingDescription()
{
    return groupingDescription;
}

public void setGroupingDescription( String groupingDescription )
{
    this.groupingDescription = groupingDescription;
}

public Integer getBusinessUnitId()
{
	return businessUnitId;
}

public void setBusinessUnitId( Integer businessUnitId )
{
	this.businessUnitId = businessUnitId;
}

}
