package com.firstlook.entity;

import java.util.Date;

public class DealerGroupPotentialEvent
{

private Integer dealerGroupPotentialEventId;
private int businessUnitId;
private int tradeAnalyzerEventId;
private int unitsInStock;
private Date createTimestamp;

public DealerGroupPotentialEvent()
{
    super();
}

public Integer getDealerGroupPotentialEventId()
{
    return dealerGroupPotentialEventId;
}

public int getBusinessUnitId()
{
    return businessUnitId;
}

public Date getCreateTimestamp()
{
    return createTimestamp;
}

public int getTradeAnalyzerEventId()
{
    return tradeAnalyzerEventId;
}

public int getUnitsInStock()
{
    return unitsInStock;
}

public void setDealerGroupPotentialEventId( Integer dealerGroupPotentialEventId )
{
    this.dealerGroupPotentialEventId = dealerGroupPotentialEventId;
}

public void setBusinessUnitId( int dealerId )
{
    this.businessUnitId = dealerId;
}

public void setCreateTimestamp( Date timestamp )
{
    this.createTimestamp = timestamp;
}

public void setTradeAnalyzerEventId( int tradeAnalyzerEventId )
{
    this.tradeAnalyzerEventId = tradeAnalyzerEventId;
}

public void setUnitsInStock( int unitsInStock )
{
    this.unitsInStock = unitsInStock;
}

}
