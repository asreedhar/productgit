package com.firstlook.entity;

import java.util.Date;

public class TradeAnalyzerEvent
{

private Integer tradeAnalyzerEventId;
private Date createTimestamp;
private int businessUnitId;
private boolean viewDealsDealerShipTrim;
private boolean viewDealsDealerShipOverall;
private boolean viewDealsDealerGroupTrim;
private boolean viewDealsDealerGroupOverall;
private boolean showDealerGroupResults;
private int generalAverageDaysToSale;
private int generalAverageGrossProfit;
private int generalAverageMileage;
private int generalUnitsSold;
private int generalUnitsInStock;
private int generalNoSales;
private int specificAverageDaysToSale;
private int specificAverageGrossProfit;
private int specificAverageMileage;
private int specificUnitsSold;
private int specificUnitsInStock;
private int specificNoSales;
private int memberId;
private String vin;
private int mileage;
private String make;
private int guideBookId;
private String model;
private String trim;
private String guideBookMake;
private String guideBookOptions;
private int guideBookYear;
private String guideBookSeries;
private int guideBookValue1;
private boolean highGrossProfit;
private boolean lowGrossProfit;
private boolean fasterDaysToSell;
private boolean avgDaysToSell;
private boolean avgMargin;
private boolean avgGrossProfit;
private boolean slowerDaysToSell;
private boolean frequentNoSales;
private boolean highMargin;
private boolean lowMargin;
private boolean noHistoricalPerformance;
private boolean limitedHistoricalPeformance;
private int guideBookValue2;
private int guideBookValue3;
private int guideBookValue4;

public TradeAnalyzerEvent()
{
    super();
}

public int getBusinessUnitId()
{
    return businessUnitId;
}

public int getGuideBookId()
{
    return guideBookId;
}

public String getGuideBookMake()
{
    return guideBookMake;
}

public String getGuideBookOptions()
{
    return guideBookOptions;
}

public String getGuideBookSeries()
{
    return guideBookSeries;
}

public int getGuideBookValue1()
{
    return guideBookValue1;
}

public int getGuideBookValue2()
{
    return guideBookValue2;
}

public int getGuideBookValue3()
{
    return guideBookValue3;
}

public int getGuideBookValue4()
{
    return guideBookValue4;
}

public int getGuideBookYear()
{
    return guideBookYear;
}

public String getMake()
{
    return make;
}

public int getMemberId()
{
    return memberId;
}

public int getMileage()
{
    return mileage;
}

public String getModel()
{
    return model;
}

public Date getCreateTimestamp()
{
    return createTimestamp;
}

public String getTrim()
{
    return trim;
}

public String getVin()
{
    return vin;
}

public void setBusinessUnitId( int newDealerId )
{
    businessUnitId = newDealerId;
}

public void setGuideBookId( int newGuideBookId )
{
    guideBookId = newGuideBookId;
}

public void setGuideBookMake( String newGuideBookMake )
{
    guideBookMake = newGuideBookMake;
}

public void setGuideBookOptions( String newGuideBookOptions )
{
    guideBookOptions = newGuideBookOptions;
}

public void setGuideBookSeries( String newGuideBookSeries )
{
    guideBookSeries = newGuideBookSeries;
}

public void setGuideBookValue1( int newGuideBookValue1 )
{
    guideBookValue1 = newGuideBookValue1;
}

public void setGuideBookValue2( int newGuideBookValue2 )
{
    guideBookValue2 = newGuideBookValue2;
}

public void setGuideBookValue3( int newGuideBookValue3 )
{
    guideBookValue3 = newGuideBookValue3;
}

public void setGuideBookValue4( int newGuideBookValue4 )
{
    guideBookValue4 = newGuideBookValue4;
}

public void setGuideBookYear( int newGuideBookYear )
{
    guideBookYear = newGuideBookYear;
}

public void setMake( java.lang.String newMake )
{
    make = newMake;
}

public void setMemberId( int newMemberId )
{
    memberId = newMemberId;
}

public void setMileage( int newMileage )
{
    mileage = newMileage;
}

public void setModel( java.lang.String newModel )
{
    model = newModel;
}

public void setCreateTimestamp( Date newTimestamp )
{
    createTimestamp = newTimestamp;
}

public void setTrim( String newTrim )
{
    trim = newTrim;
}

public void setVin( String newVin )
{
    vin = newVin;
}

public int getGeneralAverageDaysToSale()
{
    return generalAverageDaysToSale;
}

public int getGeneralAverageGrossProfit()
{
    return generalAverageGrossProfit;
}

public int getGeneralAverageMileage()
{
    return generalAverageMileage;
}

public int getGeneralNoSales()
{
    return generalNoSales;
}

public int getGeneralUnitsInStock()
{
    return generalUnitsInStock;
}

public int getGeneralUnitsSold()
{
    return generalUnitsSold;
}

public int getSpecificAverageDaysToSale()
{
    return specificAverageDaysToSale;
}

public int getSpecificAverageGrossProfit()
{
    return specificAverageGrossProfit;
}

public int getSpecificAverageMileage()
{
    return specificAverageMileage;
}

public int getSpecificNoSales()
{
    return specificNoSales;
}

public int getSpecificUnitsInStock()
{
    return specificUnitsInStock;
}

public int getSpecificUnitsSold()
{
    return specificUnitsSold;
}

public void setGeneralAverageDaysToSale( int generalAverageDaysToSale )
{
    this.generalAverageDaysToSale = generalAverageDaysToSale;
}

public void setGeneralAverageGrossProfit( int generalAverageGrossProfit )
{
    this.generalAverageGrossProfit = generalAverageGrossProfit;
}

public void setGeneralAverageMileage( int generalAverageMileage )
{
    this.generalAverageMileage = generalAverageMileage;
}

public void setGeneralNoSales( int generalNoSales )
{
    this.generalNoSales = generalNoSales;
}

public void setGeneralUnitsInStock( int generalUnitsInStock )
{
    this.generalUnitsInStock = generalUnitsInStock;
}

public void setGeneralUnitsSold( int generalUnitsSold )
{
    this.generalUnitsSold = generalUnitsSold;
}

public void setSpecificAverageDaysToSale( int specificAverageDaysToSale )
{
    this.specificAverageDaysToSale = specificAverageDaysToSale;
}

public void setSpecificAverageGrossProfit( int specificAverageGrossProfit )
{
    this.specificAverageGrossProfit = specificAverageGrossProfit;
}

public void setSpecificAverageMileage( int specificAverageMileage )
{
    this.specificAverageMileage = specificAverageMileage;
}

public void setSpecificNoSales( int specificNoSales )
{
    this.specificNoSales = specificNoSales;
}

public void setSpecificUnitsInStock( int specificUnitsInStock )
{
    this.specificUnitsInStock = specificUnitsInStock;
}

public void setSpecificUnitsSold( int specificUnitsSold )
{
    this.specificUnitsSold = specificUnitsSold;
}

public boolean isShowDealerGroupResults()
{
    return showDealerGroupResults;
}

public boolean isViewDealsDealerGroupOverall()
{
    return viewDealsDealerGroupOverall;
}

public boolean isViewDealsDealerGroupTrim()
{
    return viewDealsDealerGroupTrim;
}

public boolean isViewDealsDealerShipOverall()
{
    return viewDealsDealerShipOverall;
}

public boolean isViewDealsDealerShipTrim()
{
    return viewDealsDealerShipTrim;
}

public void setShowDealerGroupResults( boolean showDealerGroupResults )
{
    this.showDealerGroupResults = showDealerGroupResults;
}

public void setViewDealsDealerGroupOverall( boolean viewDealsDealerGroupOverall )
{
    this.viewDealsDealerGroupOverall = viewDealsDealerGroupOverall;
}

public void setViewDealsDealerGroupTrim( boolean viewDealsDealerGroupTrim )
{
    this.viewDealsDealerGroupTrim = viewDealsDealerGroupTrim;
}

public void setViewDealsDealerShipOverall( boolean viewDealsDealerShipOverall )
{
    this.viewDealsDealerShipOverall = viewDealsDealerShipOverall;
}

public void setViewDealsDealerShipTrim( boolean viewDealsDealerShipTrim )
{
    this.viewDealsDealerShipTrim = viewDealsDealerShipTrim;
}

public boolean isFasterDaysToSell()
{
    return fasterDaysToSell;
}

public boolean isFrequentNoSales()
{
    return frequentNoSales;
}

public boolean isHighGrossProfit()
{
    return highGrossProfit;
}

public boolean isHighMargin()
{
    return highMargin;
}

public boolean isLimitedHistoricalPeformance()
{
    return limitedHistoricalPeformance;
}

public boolean isLowGrossProfit()
{
    return lowGrossProfit;
}

public boolean isLowMargin()
{
    return lowMargin;
}

public boolean isNoHistoricalPerformance()
{
    return noHistoricalPerformance;
}

public boolean isSlowerDaysToSell()
{
    return slowerDaysToSell;
}

public void setFasterDaysToSell( boolean fasterDaysToSell )
{
    this.fasterDaysToSell = fasterDaysToSell;
}

public void setFrequentNoSales( boolean frequentNoSales )
{
    this.frequentNoSales = frequentNoSales;
}

public void setHighGrossProfit( boolean highGrossProfit )
{
    this.highGrossProfit = highGrossProfit;
}

public void setHighMargin( boolean highMargin )
{
    this.highMargin = highMargin;
}

public void setLimitedHistoricalPeformance( boolean limitedHistoricalPeformance )
{
    this.limitedHistoricalPeformance = limitedHistoricalPeformance;
}

public void setLowGrossProfit( boolean lowGrossProfit )
{
    this.lowGrossProfit = lowGrossProfit;
}

public void setLowMargin( boolean lowMargin )
{
    this.lowMargin = lowMargin;
}

public void setNoHistoricalPerformance( boolean noHistoricalPerformance )
{
    this.noHistoricalPerformance = noHistoricalPerformance;
}

public void setSlowerDaysToSell( boolean slowerDaysToSell )
{
    this.slowerDaysToSell = slowerDaysToSell;
}

public boolean isAvgDaysToSell()
{
    return avgDaysToSell;
}

public boolean isAvgGrossProfit()
{
    return avgGrossProfit;
}

public boolean isAvgMargin()
{
    return avgMargin;
}

public void setAvgDaysToSell( boolean avgDaysToSell )
{
    this.avgDaysToSell = avgDaysToSell;
}

public void setAvgGrossProfit( boolean avgGrossProfit )
{
    this.avgGrossProfit = avgGrossProfit;
}

public void setAvgMargin( boolean avgMargin )
{
    this.avgMargin = avgMargin;
}

public Integer getTradeAnalyzerEventId()
{
    return tradeAnalyzerEventId;
}

public void setTradeAnalyzerEventId( Integer integer )
{
    tradeAnalyzerEventId = integer;
}

}