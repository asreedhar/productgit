package com.firstlook.entity;

public class MarketShare
{

private int modelYear;
private int numDeals;
private double marketShare;

public MarketShare()
{
}

public double getMarketShare()
{
    return marketShare;
}

public int getModelYear()
{
    return modelYear;
}

public int getNumDeals()
{
    return numDeals;
}

public void setMarketShare( double d )
{
    marketShare = d;
}

public void setModelYear( int i )
{
    modelYear = i;
}

public void setNumDeals( int i )
{
    numDeals = i;
}

}
