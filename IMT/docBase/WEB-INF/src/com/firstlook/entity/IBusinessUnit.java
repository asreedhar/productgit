package com.firstlook.entity;

public interface IBusinessUnit
{

public Integer getBusinessUnitId();

public void setBusinessUnitId( Integer newId );

public int getBusinessUnitTypeId();

public void setBusinessUnitTypeId( int newId );

}
