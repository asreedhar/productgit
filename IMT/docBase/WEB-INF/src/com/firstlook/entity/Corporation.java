package com.firstlook.entity;

import java.util.Set;

import com.firstlook.exception.ApplicationException;
import com.firstlook.service.businessunit.BusinessUnitCodeService;

public class Corporation implements IBusinessUnit
{

public static final int CORPORATION_PREFIX_SIZE = 8;

private Integer corporationId;
private int businessUnitTypeId;
private String name;
private String nickname;
private String corporationCode;
private String address1;
private String address2;
private String city;
private String state;
private String zipcode;
private String officePhone;
private Set<Region> regions;
private String officeFax;
private boolean active;

public Corporation()
{
    super();
    setBusinessUnitTypeId(BusinessUnitType.BUSINESS_UNIT_CORPORATE_TYPE);
}

public String createCorporationCode() throws ApplicationException
{
    BusinessUnitCodeService businessUnitCodeService = new BusinessUnitCodeService();

    return businessUnitCodeService.createBusinessUnitCode(getName());
}

public boolean isActive()
{
    return active;
}

public String getAddress1()
{
    return address1;
}

public String getAddress2()
{
    return address2;
}

public int getBusinessUnitTypeId()
{
    return businessUnitTypeId;
}

public String getCity()
{
    return city;
}

public String getCorporationCode()
{
    return corporationCode;
}

public Integer getCorporationId()
{
    return corporationId;
}

public Integer getBusinessUnitId()
{
    return getCorporationId();
}

public void setBusinessUnitId( Integer businessUnitId )
{
    setCorporationId(businessUnitId);
}

public String getName()
{
    return name;
}

public String getNickname()
{
    return nickname;
}

public String getOfficeFax()
{
    return officeFax;
}

public String getOfficePhone()
{
    return officePhone;
}

public String getState()
{
    return state;
}

public String getZipcode()
{
    return zipcode;
}

public void setActive( boolean b )
{
    active = b;
}

public void setAddress1( String string )
{
    address1 = string;
}

public void setAddress2( String string )
{
    address2 = string;
}

public void setBusinessUnitTypeId( int i )
{
    businessUnitTypeId = i;
}

public void setCity( String string )
{
    city = string;
}

public void setCorporationCode( String string )
{
    corporationCode = string;
}

public void setCorporationId( Integer i )
{
    corporationId = i;
}

public void setName( String string )
{
    name = string;
}

public void setNickname( String string )
{
    nickname = string;
}

public void setOfficeFax( String string )
{
    officeFax = string;
}

public void setOfficePhone( String string )
{
    officePhone = string;
}

public void setState( String string )
{
    state = string;
}

public void setZipcode( String string )
{
    zipcode = string;
}

public Set<Region> getRegions()
{
	return regions;
}

public void setRegions( Set<Region> regions )
{
	this.regions = regions;
}

}
