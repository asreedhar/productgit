package com.firstlook.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.firstlook.data.DatabaseException;
import com.firstlook.exception.ApplicationException;
import com.firstlook.service.businessunit.BusinessUnitCodeService;

public class DealerGroup implements IBusinessUnit
{
public final static int DEALERGROUP_PREFIX_SIZE = 4;
public final static boolean DEALERGROUP_ACTIVE = true;
public final static boolean DEALERGROUP_INACTIVE = false;
private Integer dealerGroupId;
private String dealerGroupCode;
private String nickname = ""; //done to keep compatibility with previous revision
private String name = "";  //done to keep compatibility with previous revision
private String address1;
private String address2;
private String city;
private String state;
private String zipcode;
private String officePhoneNumber;
private String officeFaxNumber;
private Set<Dealer> dealers;
boolean active = false;
private int businessUnitTypeId = BusinessUnitType.BUSINESS_UNIT_DEALERGROUP_TYPE;
private DealerGroupPreference dealerGroupPreference;
private Set<DealerGroupPreference> dealerGroupPreferences;
int[] marketIdArray;

public DealerGroup()
{
	super();
}

public String createDealerGroupCode() throws ApplicationException
{
	BusinessUnitCodeService businessUnitCodeService = new BusinessUnitCodeService();

	return businessUnitCodeService.createBusinessUnitCode( getName() );
}

public boolean equals( Object object )
{
	boolean equals = false;
	if ( ( object != null ) && ( object instanceof DealerGroup ) )
	{
		DealerGroup dg = (DealerGroup)object;
		equals = ( dg.getAddress1().equals( this.getAddress1() ) )
				&& ( dg.getAddress2().equals( this.getAddress2() ) ) && ( dg.getCity().equals( this.getCity() ) )
				&& ( dg.getDealerGroupCode().equals( this.getDealerGroupCode() ) )
				&& ( dg.getDealerGroupId().intValue() == this.getDealerGroupId().intValue() ) && ( dg.getName().equals( this.getName() ) )
				&& ( dg.getNickname().equals( this.getNickname() ) ) && ( dg.getOfficeFaxNumber().equals( this.getOfficeFaxNumber() ) )
				&& ( dg.getOfficePhoneNumber().equals( this.getOfficePhoneNumber() ) ) && ( dg.getState().equals( this.getState() ) )
				&& ( dg.getInventoryExchangeAgeLimit() == this.getInventoryExchangeAgeLimit() )
				&& ( dg.getZipcode().equals( this.getZipcode() ) );
	}

	return equals;
}

public java.lang.String getAddress1()
{
	return address1;
}

public java.lang.String getAddress2()
{
	return address2;
}

public java.lang.String getCity()
{
	return city;
}

public java.lang.String getDealerGroupCode()
{
	return dealerGroupCode;
}

public Integer getDealerGroupId()
{
	return dealerGroupId;
}

public int getBusinessUnitIdInt()
{
	if ( getDealerGroupId() != null )
	{
		return getDealerGroupId().intValue();
	}
	else
	{
		return 0;
	}
}
//TODO:  get rid of this method as it is trying to process dealers which may or may not be set on the object - KDL post-Spring
public Collection<Dealer> getActiveDealers() throws DatabaseException, ApplicationException
{
	List<Dealer> dealers = new ArrayList<Dealer>();
	Iterator<Dealer> dealersIterator = getDealers().iterator();

	while ( dealersIterator.hasNext() )
	{
		Dealer dealer = (Dealer)dealersIterator.next();
		if ( dealer.isActive() )
		{
			dealers.add( dealer );
		}
	}

	return dealers;
}

public String getNickname()
{
	return nickname;
}

public String getOfficeFaxNumber()
{
	return officeFaxNumber;
}

public String getOfficePhoneNumber()
{
	return officePhoneNumber;
}

public String getState()
{
	return state;
}

public String getZipcode()
{
	return zipcode;
}

public void setAddress1( String newAddress1 )
{
	address1 = newAddress1;
}

public void setAddress2( String newAddress2 )
{
	address2 = newAddress2;
}

public void setCity( String newCity )
{
	city = newCity;
}

public void setDealerGroupCode( String newDealerGroupCode )
{
	dealerGroupCode = newDealerGroupCode;
}

public void setDealerGroupId( Integer newDealerGroupId )
{
	dealerGroupId = newDealerGroupId;
}

public void setDealers( Collection<Dealer> newDealers )
{
	dealers.clear();
	dealers.addAll(newDealers);
}

public void setMarketIdArray( int[] newMarketIdArray )
{
	marketIdArray = newMarketIdArray;
}

public void setNickname( String newNickname )
{
	nickname = newNickname;
}

public void setOfficeFaxNumber( String newOfficeFaxNumber )
{
	officeFaxNumber = newOfficeFaxNumber;
}

public void setOfficePhoneNumber( String newOfficePhoneNumber )
{
	officePhoneNumber = newOfficePhoneNumber;
}

public void setState( String newState )
{
	state = newState;
}

public void setZipcode( String newZipcode )
{
	zipcode = newZipcode;
}

public Integer getAgingPolicy()
{
	return getDealerGroupPreference().getAgingPolicy();
}

public void setAgingPolicy( Integer agingPolicy )
{
	getDealerGroupPreference().setAgingPolicy( agingPolicy );
}

public int getAgingPolicyAdjustment()
{
	return getDealerGroupPreference().getAgingPolicyAdjustment();
}

public void setAgingPolicyAdjustment( int agingPolicyAdjustment )
{
	getDealerGroupPreference().setAgingPolicyAdjustment( agingPolicyAdjustment );
}

public boolean isLithiaStore()
{
	return getDealerGroupPreference().isLithiaStore();
}

public void setLithiaStore( boolean lithiaStore )
{
	getDealerGroupPreference().setLithiaStore( lithiaStore );
}

public int getInventoryExchangeAgeLimit()
{
	return getDealerGroupPreference().getInventoryExchangeAgeLimit();
}

public void setInventoryExchangeAgeLimit( int inventoryExchangeAgeLimit )
{
	getDealerGroupPreference().setInventoryExchangeAgeLimit( inventoryExchangeAgeLimit );
}

public boolean isActive()
{
	return active;
}

public void setActive( boolean b )
{
	active = b;
}

public int getBusinessUnitTypeId()
{
	return businessUnitTypeId;
}

//Left for hibernate to use reflection
public void setBusinessUnitTypeId( int id ) {
	this.businessUnitTypeId = id;
}

public DealerGroupPreference getDealerGroupPreference()
{
	if ( dealerGroupPreferences != null && !dealerGroupPreferences.isEmpty() )
	{
		return (DealerGroupPreference)dealerGroupPreferences.toArray()[0];
	}
	else
	{
		dealerGroupPreferences = new HashSet<DealerGroupPreference>();
		dealerGroupPreference = new DealerGroupPreference();
		dealerGroupPreference.setDealerGroup( this );
		dealerGroupPreferences.add( dealerGroupPreference );
		return dealerGroupPreference;
	}
}

public void setDealerGroupPreferenceId( int dealerGroupPreferenceId )
{
	getDealerGroupPreference().setDealerGroupPreferenceId( new Integer( dealerGroupPreferenceId ) );
}

public int getDealerGroupPreferenceId()
{
	return getDealerGroupPreference().getDealerGroupPreferenceId().intValue();
}

public String getName()
{
	return name;
}

public void setName( String string )
{
	name = string;
}

public int getPricePointDealsThreshold()
{
	return getDealerGroupPreference().getPricePointDealsThreshold();
}

public void setPricePointDealsThreshold( int i )
{
	getDealerGroupPreference().setPricePointDealsThreshold( i );
}

public Set<DealerGroupPreference> getDealerGroupPreferences()
{
	return dealerGroupPreferences;
}

public void setDealerGroupPreferences( Set<DealerGroupPreference> set )
{
	dealerGroupPreferences = set;
}

public void setBusinessUnitId( Integer newId )
{
	dealerGroupId = newId;
}

public Integer getBusinessUnitId()
{
	return dealerGroupId;
}

public void setDealerGroupPreference( DealerGroupPreference preference )
{
	dealerGroupPreference = preference;
}

public Set<Dealer> getDealers()
{
	return dealers;
}

}
