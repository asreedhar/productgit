package com.firstlook.entity;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.enums.ValuedEnum;

public class ImpactModeEnum extends ValuedEnum
{
private static final long serialVersionUID = 5359922328639420592L;
public static int STANDARD_VAL = 1;
public static int PERCENTAGE_VAL = 2;

public static ImpactModeEnum STANDARD = new ImpactModeEnum("standard",
        STANDARD_VAL);
public static ImpactModeEnum PERCENTAGE = new ImpactModeEnum("percentage",
        PERCENTAGE_VAL);

private ImpactModeEnum( String name, int value )
{
    super(name, value);
}

public static ImpactModeEnum getEnum( String part )
{
    return (ImpactModeEnum) getEnum(ImpactModeEnum.class, part);
}

public static ImpactModeEnum getEnum( int part )
{
    return (ImpactModeEnum) getEnum(ImpactModeEnum.class, part);
}

public static Map getEnumMap()
{
    return getEnumMap(ImpactModeEnum.class);
}

public static List getEnumList()
{
    return getEnumList(ImpactModeEnum.class);
}

public static Iterator iterator()
{
    return iterator(ImpactModeEnum.class);
}

}