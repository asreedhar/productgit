package com.firstlook.entity;

import java.sql.Timestamp;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class ReportCenterSession
{

private Integer reportCenterSessionId;
private String sessionId;
private String rcSessionId;
private Integer businessUnitId;
private Integer memberId;
private Integer doAuthentication = new Integer(1);
private Timestamp accessTime = new Timestamp( System.currentTimeMillis() );

protected ReportCenterSession()
{
	super();
}

public ReportCenterSession( String sessionId,String rcSessionId,Integer businessUnitId, Integer memberId )
{
	super();
	this.sessionId = sessionId;
	this.businessUnitId = businessUnitId;
	this.memberId = memberId;
	this.rcSessionId = rcSessionId;
}


public Integer getBusinessUnitId()
{
	return businessUnitId;
}

public String getRcSessionId()
{
	return rcSessionId;
}

public void setRcSessionId( String rcSessionId )
{
	this.rcSessionId = rcSessionId;
}

public void setBusinessUnitId( Integer businessUnitId )
{
	this.businessUnitId = businessUnitId;
}

public Integer getMemberId()
{
	return memberId;
}

public void setMemberId( Integer memberId )
{
	this.memberId = memberId;
}

public Integer getDoAuthentication() {
	return doAuthentication;
}

public void setDoAuthentication(Integer doAuthentication) {
	this.doAuthentication = doAuthentication;
}

public Integer getReportCenterSessionId()
{
	return reportCenterSessionId;
}

public void setReportCenterSessionId( Integer reportCenterSessionId )
{
	this.reportCenterSessionId = reportCenterSessionId;
}

public String getSessionId()
{
	return sessionId;
}

public void setSessionId( String sessionId )
{
	this.sessionId = sessionId;
}

protected Timestamp getAccessTime()
{
	return accessTime;
}

protected void setAccessTime( Timestamp accessTime )
{
	this.accessTime = accessTime;
}

public String toString()
{
	return ReflectionToStringBuilder.reflectionToString( this, ToStringStyle.SHORT_PREFIX_STYLE );
}

}
